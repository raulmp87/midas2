package mx.com.afirme.midas.danios.reportes.reportepmlhidro;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ReportePMLHidroDTO implements Serializable{
	private static final long serialVersionUID = -7421299112536406685L;

	private Integer claveTipoReporte;
	
	private BigDecimal convenioValorAsegurable;
	
	private Short tipoPrimerRiesgo;
	private BigDecimal montoPrimerRiesgo;
	private BigDecimal porcentajeRetencion;
	private Integer consecPeriodoCobertura;
	private BigDecimal convenioLimiteMaximo;
	
	private BigDecimal convenioDeducible;
	
	private BigDecimal convenioCoaseguro;
	
	private Short primeraLineaMar;
	private Short primeraLineaLago;
	private Short sobreelevacionDesplante;
	private Short rugosidad;
	private Short usoInmueble;
	
	private Short piso;
	private Short tipoCubierta;
	private Short formaCubierta;
	private Short irrePlanta;
	private Short objetosCerca;
	private Short azotea;
	private Short tamanoCristal;
	private Short tipoVentanas;
	private Short tipoDomos;
	private Short soporteVentana;
	private Short porcentajeCristalFachadas;
	private Short porcentajeDomos;
	private Short otrosFachada;
	private Short murosContencion;
	
	private Date fechaCorte;
	private Double tipoCambio;
	
	private Integer idRegistro;
	private String numeroPoliza;
	private Integer numeroRegistro;
	private Date fechaInicio;
	private Date fechaFin;
	private BigDecimal inmValorAsegurable;
	private BigDecimal contValorAsegurable;
	private BigDecimal consecValorAsegurable;
	
	private BigDecimal consecLimiteMaximo;
	private BigDecimal inmDeducible;
	private BigDecimal contDeducible;
	private BigDecimal consecDeducible;
	private BigDecimal inmCoaseguro;
	private BigDecimal contCoaseguro;
	private BigDecimal consecCoaseguro;
	private Integer claveEstado;
	private Integer codigoPostal;
	private BigDecimal longitud;
	private BigDecimal latitud;
	private Short numeroPisos;
	private BigDecimal valorAsegurable;
	private BigDecimal valorRetenido;
	private BigDecimal prima;
	private BigDecimal cedida;
	private BigDecimal retenida;
	private String moneda;
	private String rsrt;
	private String ofiEmi;
	private String inciso;
	private String zonaAmis;
	private Short tipoPoliza;
	private String numCapa;
	private BigDecimal limiteMaximo;
	private BigDecimal coaseguro;
	
//	private BigDecimal regresarNullSiEsCero(BigDecimal valor){
//		BigDecimal valorCalculado = valor;
//		if(valor != null && valor.compareTo(BigDecimal.ZERO)==0)
//			valorCalculado = null;
//		return valorCalculado;
//	}
	
//	private Integer regresarIntNullSiEsCero(Integer valor){
//		Integer valorCalculado = valor;
//		if(valor != null && valor.compareTo(new Integer(0))==0)
//			valorCalculado = null;
//		return valorCalculado;
//	}
	
	public Date getFechaCorte() {
		return fechaCorte;
	}
	public void setFechaCorte(Date fechaCorte) {
		this.fechaCorte = fechaCorte;
	}
	public Double getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public Integer getIdRegistro() {
		return idRegistro;
	}
	public void setIdRegistro(Integer idRegistro) {
		this.idRegistro = idRegistro;
	}
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public Integer getNumeroRegistro() {
		return numeroRegistro;
	}
	public void setNumeroRegistro(Integer numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
	}
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	public BigDecimal getInmValorAsegurable() {
		return inmValorAsegurable;
	}
	public void setInmValorAsegurable(BigDecimal inmValorAsegurable) {
		this.inmValorAsegurable = inmValorAsegurable;
	}
	public BigDecimal getContValorAsegurable() {
		return contValorAsegurable;
	}
	public void setContValorAsegurable(BigDecimal contValorAsegurable) {
		this.contValorAsegurable = contValorAsegurable;
	}
	public BigDecimal getConsecValorAsegurable() {
		return consecValorAsegurable;
	}
	public void setConsecValorAsegurable(BigDecimal consecValorAsegurable) {
		this.consecValorAsegurable = consecValorAsegurable;
	}
	public BigDecimal getConsecLimiteMaximo() {
		return consecLimiteMaximo;
	}
	public void setConsecLimiteMaximo(BigDecimal consecLimiteMaximo) {
		this.consecLimiteMaximo = consecLimiteMaximo;
	}
	public BigDecimal getInmDeducible() {
		return inmDeducible;
	}
	public void setInmDeducible(BigDecimal inmDeducible) {
		this.inmDeducible = inmDeducible;
	}
	public BigDecimal getContDeducible() {
		return contDeducible;
	}
	public void setContDeducible(BigDecimal contDeducible) {
		this.contDeducible = contDeducible;
	}
	public BigDecimal getConsecDeducible() {
		return consecDeducible;
	}
	public void setConsecDeducible(BigDecimal consecDeducible) {
		this.consecDeducible = consecDeducible;
	}
	public BigDecimal getInmCoaseguro() {
		return inmCoaseguro;
	}
	public void setInmCoaseguro(BigDecimal inmCoaseguro) {
		this.inmCoaseguro = inmCoaseguro;
	}
	public BigDecimal getContCoaseguro() {
		return contCoaseguro;
	}
	public void setContCoaseguro(BigDecimal contCoaseguro) {
		this.contCoaseguro = contCoaseguro;
	}
	public BigDecimal getConsecCoaseguro() {
		return consecCoaseguro;
	}
	public void setConsecCoaseguro(BigDecimal consecCoaseguro) {
		this.consecCoaseguro = consecCoaseguro;
	}
	public Integer getClaveEstado() {
		return claveEstado;
	}
	public void setClaveEstado(Integer claveEstado) {
		this.claveEstado = claveEstado;
	}
	public Integer getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(Integer codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public BigDecimal getLongitud() {
		return longitud;
	}
	public void setLongitud(BigDecimal longitud) {
		this.longitud = longitud;
	}
	public BigDecimal getLatitud() {
		return latitud;
	}
	public void setLatitud(BigDecimal latitud) {
		this.latitud = latitud;
	}
	public Short getNumeroPisos() {
		return numeroPisos;
	}
	public void setNumeroPisos(Short numeroPisos) {
		this.numeroPisos = numeroPisos;
	}
	public BigDecimal getValorAsegurable() {
		return valorAsegurable;
	}
	public void setValorAsegurable(BigDecimal valorAsegurable) {
		this.valorAsegurable = valorAsegurable;
	}
	public BigDecimal getValorRetenido() {
		return valorRetenido;
	}
	public void setValorRetenido(BigDecimal valorRetenido) {
		this.valorRetenido = valorRetenido;
	}
	public BigDecimal getPrima() {
		return prima;
	}
	public void setPrima(BigDecimal prima) {
		this.prima = prima;
	}
	public BigDecimal getCedida() {
		return cedida;
	}
	public void setCedida(BigDecimal cedida) {
		this.cedida = cedida;
	}
	public BigDecimal getRetenida() {
		return retenida;
	}
	public void setRetenida(BigDecimal retenida) {
		this.retenida = retenida;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getRsrt() {
		return rsrt;
	}
	public void setRsrt(String rsrt) {
		this.rsrt = rsrt;
	}
	public String getOfiEmi() {
		return ofiEmi;
	}
	public void setOfiEmi(String ofiEmi) {
		this.ofiEmi = ofiEmi;
	}
	public String getInciso() {
		return inciso;
	}
	public void setInciso(String inciso) {
		this.inciso = inciso;
	}
	public String getZonaAmis() {
		return zonaAmis;
	}
	public void setZonaAmis(String zonaAmis) {
		this.zonaAmis = zonaAmis;
	}
	public Short getTipoPoliza() {
		return tipoPoliza;
	}
	public void setTipoPoliza(Short tipoPoliza) {
		this.tipoPoliza = tipoPoliza;
	}
	public String getNumCapa() {
		return numCapa;
	}
	public void setNumCapa(String numCapa) {
		this.numCapa = numCapa;
	}
	public BigDecimal getLimiteMaximo() {
		return limiteMaximo;
	}
	public void setLimiteMaximo(BigDecimal limiteMaximo) {
		this.limiteMaximo = limiteMaximo;
	}
	public BigDecimal getCoaseguro() {
		return coaseguro;
	}
	public void setCoaseguro(BigDecimal coaseguro) {
		this.coaseguro = coaseguro;
	}
	public BigDecimal getConvenioValorAsegurable() {
		return convenioValorAsegurable;
	}
	public void setConvenioValorAsegurable(BigDecimal convenioValorAsegurable) {
		this.convenioValorAsegurable = convenioValorAsegurable;
	}
	public Short getTipoPrimerRiesgo() {
		return tipoPrimerRiesgo;
	}
	public void setTipoPrimerRiesgo(Short tipoPrimerRiesgo) {
		this.tipoPrimerRiesgo = tipoPrimerRiesgo;
	}
	public BigDecimal getMontoPrimerRiesgo() {
		return montoPrimerRiesgo;
	}
	public void setMontoPrimerRiesgo(BigDecimal montoPrimerRiesgo) {
		this.montoPrimerRiesgo = montoPrimerRiesgo;
	}
	public Integer getConsecPeriodoCobertura() {
		return consecPeriodoCobertura;
	}
	public void setConsecPeriodoCobertura(Integer consecPeriodoCobertura) {
		this.consecPeriodoCobertura = consecPeriodoCobertura;
	}
	public BigDecimal getConvenioLimiteMaximo() {
		return convenioLimiteMaximo;
	}
	public void setConvenioLimiteMaximo(BigDecimal convenioLimiteMaximo) {
		this.convenioLimiteMaximo = convenioLimiteMaximo;
	}
	public BigDecimal getConvenioDeducible() {
		return convenioDeducible;
	}
	public void setConvenioDeducible(BigDecimal convenioDeducible) {
		this.convenioDeducible = convenioDeducible;
	}
	public BigDecimal getConvenioCoaseguro() {
		return convenioCoaseguro;
	}
	public void setConvenioCoaseguro(BigDecimal convenioCoaseguro) {
		this.convenioCoaseguro = convenioCoaseguro;
	}
	public Short getPrimeraLineaMar() {
		return primeraLineaMar;
	}
	public void setPrimeraLineaMar(Short primeraLineaMar) {
		this.primeraLineaMar = primeraLineaMar;
	}
	public Short getPrimeraLineaLago() {
		return primeraLineaLago;
	}
	public void setPrimeraLineaLago(Short primeraLineaLago) {
		this.primeraLineaLago = primeraLineaLago;
	}
	public Short getSobreelevacionDesplante() {
		return sobreelevacionDesplante;
	}
	public void setSobreelevacionDesplante(Short sobreelevacionDesplante) {
		this.sobreelevacionDesplante = sobreelevacionDesplante;
	}
	public Short getRugosidad() {
		return rugosidad;
	}
	public void setRugosidad(Short rugosidad) {
		this.rugosidad = rugosidad;
	}
	public Short getUsoInmueble() {
		return usoInmueble;
	}
	public void setUsoInmueble(Short usoInmueble) {
		this.usoInmueble = usoInmueble;
	}
	public Short getPiso() {
		return piso;
	}
	public void setPiso(Short piso) {
		this.piso = piso;
	}
	public Short getTipoCubierta() {
		return tipoCubierta;
	}
	public void setTipoCubierta(Short tipoCubierta) {
		this.tipoCubierta = tipoCubierta;
	}
	public Short getFormaCubierta() {
		return formaCubierta;
	}
	public void setFormaCubierta(Short formaCubierta) {
		this.formaCubierta = formaCubierta;
	}
	public Short getIrrePlanta() {
		return irrePlanta;
	}
	public void setIrrePlanta(Short irrePlanta) {
		this.irrePlanta = irrePlanta;
	}
	public Short getObjetosCerca() {
		return objetosCerca;
	}
	public void setObjetosCerca(Short objetosCerca) {
		this.objetosCerca = objetosCerca;
	}
	public Short getAzotea() {
		return azotea;
	}
	public void setAzotea(Short azotea) {
		this.azotea = azotea;
	}
	public Short getTamanoCristal() {
		return tamanoCristal;
	}
	public void setTamanoCristal(Short tamanoCristal) {
		this.tamanoCristal = tamanoCristal;
	}
	public Short getTipoVentanas() {
		return tipoVentanas;
	}
	public void setTipoVentanas(Short tipoVentanas) {
		this.tipoVentanas = tipoVentanas;
	}
	public Short getTipoDomos() {
		return tipoDomos;
	}
	public void setTipoDomos(Short tipoDomos) {
		this.tipoDomos = tipoDomos;
	}
	public Short getSoporteVentana() {
		return soporteVentana;
	}
	public void setSoporteVentana(Short soporteVentana) {
		this.soporteVentana = soporteVentana;
	}
	public Short getPorcentajeCristalFachadas() {
		return porcentajeCristalFachadas;
	}
	public void setPorcentajeCristalFachadas(Short porcentajeCristalFachadas) {
		this.porcentajeCristalFachadas = porcentajeCristalFachadas;
	}
	public Short getPorcentajeDomos() {
		return porcentajeDomos;
	}
	public void setPorcentajeDomos(Short porcentajeDomos) {
		this.porcentajeDomos = porcentajeDomos;
	}
	public Short getOtrosFachada() {
		return otrosFachada;
	}
	public void setOtrosFachada(Short otrosFachada) {
		this.otrosFachada = otrosFachada;
	}
	public Short getMurosContencion() {
		return murosContencion;
	}
	public void setMurosContencion(Short murosContencion) {
		this.murosContencion = murosContencion;
	}
	public BigDecimal getPorcentajeRetencion() {
		return porcentajeRetencion;
	}
	public void setPorcentajeRetencion(BigDecimal porcentajeRetencion) {
		this.porcentajeRetencion = porcentajeRetencion;
	}
	public Integer getClaveTipoReporte() {
		return claveTipoReporte;
	}
	public void setClaveTipoReporte(Integer claveTipoReporte) {
		this.claveTipoReporte = claveTipoReporte;
	}
}
