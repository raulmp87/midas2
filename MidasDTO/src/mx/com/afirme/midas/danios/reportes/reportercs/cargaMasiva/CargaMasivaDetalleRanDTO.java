package mx.com.afirme.midas.danios.reportes.reportercs.cargaMasiva;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * @author JORGEKNO
 */
@Entity
@Table(name = "CNSF_LIMITE", schema = "MIDAS")

public class CargaMasivaDetalleRanDTO implements java.io.Serializable, Cloneable {

	private static final long serialVersionUID = 1L;
	// Fields
	
	private BigDecimal idLimite;
	@Id
	@Column(name = "ID_LIMITE", nullable = false, precision = 22, scale = 0)
	@SequenceGenerator(name = "ID_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.CNSF_LIMITES_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_SEQ_GENERADOR")
	public BigDecimal getIdLimite() {
		return idLimite;
	}

	public void setIdLimite(BigDecimal id) {
		this.idLimite = id;
	}

	private BigDecimal lim_inf;
	private BigDecimal lim_sup;
	
	// Constructors

	/** default constructor */
	public CargaMasivaDetalleRanDTO() {
	}
	
	@Column(name = "lim_inf", nullable = false, precision = 22, scale = 0)
	public BigDecimal getLim_inf() {
		return lim_inf;
	}

	public void setLim_inf(BigDecimal lim_inf) {
		this.lim_inf = lim_inf;
	}
	
	@Column(name = "lim_sup", nullable = false, precision = 22, scale = 0)
	public BigDecimal getLim_sup() {
		return lim_sup;
	}

	public void setLim_sup(BigDecimal lim_sup) {
		this.lim_sup = lim_sup;
	}

	public CargaMasivaDetalleRanDTO clone()   {

    	CargaMasivaDetalleRanDTO clon = new CargaMasivaDetalleRanDTO(this.idLimite,this.lim_inf, this.lim_sup);

        return clon;

     }

	public CargaMasivaDetalleRanDTO(BigDecimal idLimite, BigDecimal lim_inf,
			BigDecimal lim_sup) {
		super();
		this.idLimite = idLimite;
		this.lim_inf = lim_inf;
		this.lim_sup = lim_sup;
	}

	@Override
	public String toString() {
		return "CargaMasivaDetalleRanDTO [idLimite=" + idLimite + ", lim_inf="
				+ lim_inf + ", lim_sup=" + lim_sup
				+ "]";
	}
	
	
	
}