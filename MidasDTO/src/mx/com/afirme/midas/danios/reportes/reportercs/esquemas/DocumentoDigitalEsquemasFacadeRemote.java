package mx.com.afirme.midas.danios.reportes.reportercs.esquemas;

import java.math.BigDecimal;
import java.util.List;

 
public interface DocumentoDigitalEsquemasFacadeRemote {
	
	public void save(DocumentoDigitalEsquemasDTO entity);

	public void delete(DocumentoDigitalEsquemasDTO entity);

	public DocumentoDigitalEsquemasDTO update(
			DocumentoDigitalEsquemasDTO entity);

	public DocumentoDigitalEsquemasDTO findById(BigDecimal id);

	public List<DocumentoDigitalEsquemasDTO> findByProperty(
			String propertyName, Object value);

	public List<DocumentoDigitalEsquemasDTO> findAll();
}