package mx.com.afirme.midas.danios.reportes.bean.pago;

import java.io.Serializable;
import java.math.BigDecimal;

public class ReciboDTO implements Serializable {

	private static final long serialVersionUID = -4358959415065591280L;

	private BigDecimal primaNeta;
	private BigDecimal recargoPagoFraccionado;
	private BigDecimal derecho;
	private BigDecimal iva;
	private BigDecimal totalRecibo;
	private BigDecimal factorIVA;
	
	public ReciboDTO() {
	}

	public ReciboDTO(BigDecimal primaNeta, BigDecimal recargoPagoFraccionado,
			BigDecimal derecho, BigDecimal iva, BigDecimal totalRecibo) {
		this.primaNeta = primaNeta;
		this.recargoPagoFraccionado = recargoPagoFraccionado;
		this.derecho = derecho;
		this.iva = iva;
		this.totalRecibo = totalRecibo;
	}
	
	public BigDecimal getPrimaNeta() {
		return primaNeta;
	}
	public void setPrimaNeta(BigDecimal primaNeta) {
		this.primaNeta = primaNeta;
	}
	public BigDecimal getRecargoPagoFraccionado() {
		return recargoPagoFraccionado;
	}
	public void setRecargoPagoFraccionado(BigDecimal recargoPagoFraccionado) {
		this.recargoPagoFraccionado = recargoPagoFraccionado;
	}
	public BigDecimal getDerecho() {
		return derecho;
	}
	public void setDerecho(BigDecimal derecho) {
		this.derecho = derecho;
	}
	public BigDecimal getIva() {
		return iva;
	}
	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}
	public BigDecimal getTotalRecibo() {
		return totalRecibo;
	}
	public void setTotalRecibo(BigDecimal totalRecibo) {
		this.totalRecibo = totalRecibo;
	}
	public BigDecimal getFactorIVA() {
		return factorIVA;
	}
	public void setFactorIVA(BigDecimal factorIVA) {
		this.factorIVA = factorIVA;
	}
}
