package mx.com.afirme.midas.danios.reportes.cotizacion;

import java.math.BigDecimal;

import javax.ejb.Remote;

import mx.com.afirme.midas.sistema.SystemException;


public interface ReporteCotizacionServiciosRemote {
	
	public byte[] imprimirCotizacion(BigDecimal idToCotizacion,String nombreUsuario) throws SystemException;

	public byte[] imprimirSolicitud(BigDecimal idToCotizacion,String nombreUsuario) throws SystemException;
	
	public byte[] imprimirEndoso(BigDecimal idToPoliza,Short numeroEndoso,String nombreUsuario) throws SystemException;
	
	public byte[] imprimirPoliza(BigDecimal idToCotizacion,String nombreUsuario) throws SystemException;
}
