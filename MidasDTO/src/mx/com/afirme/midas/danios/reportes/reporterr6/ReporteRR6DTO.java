package mx.com.afirme.midas.danios.reportes.reporterr6;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


/**
 * @author Paulo dos Santos
 */
@Entity
@Table(name="CNSF_RR6_ESTATUS", schema="MIDAS")
public class ReporteRR6DTO implements Serializable {

	private static final long serialVersionUID = 2299753824789845932L;
	
	private Integer idtcontrato;
	private Date fechaInicio;
	private Date fechaFin;
	private Date iniEjecucion;
	private Date finEjecucion;
	private String estatus;
	private String reporte;
	private String registro;
	
	/** default constructor */
    public ReporteRR6DTO() {
    }
    
    @Id
    @SequenceGenerator(name = "IDCONTRATO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.CNSF_RR6_ESTATUS_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDCONTRATO_SEQ_GENERADOR")
    @Column(name="IDCONTRATO", unique=true, nullable=false, precision=22, scale=0)
    public Integer getIdcontrato() {
        return this.idtcontrato;
    }
    
    public void setIdcontrato(Integer idtcontrato) {
        this.idtcontrato = idtcontrato;
    }
    
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAINICIO", nullable=false, length=10)
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	
	@Temporal(TemporalType.DATE)
    @Column(name="FECHAFIN", nullable=false, length=10)
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	@Temporal(TemporalType.DATE)
    @Column(name="INIEJECUCION", nullable=false, length=10)
	public Date getIniEjecucion() {
		return iniEjecucion;
	}
	public void setIniEjecucion(Date iniEjecucion) {
		this.iniEjecucion = iniEjecucion;
	}
	
	@Temporal(TemporalType.DATE)
    @Column(name="FINEJECUCION", nullable=false, length=10)
	public Date getFinEjecucion() {
		return finEjecucion;
	}
	public void setFinEjecucion(Date finEjecucion) {
		this.finEjecucion = finEjecucion;
	}
	
	@Column(name="ESTATUS", nullable=false, length=40)
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	
	@Column(name="REPORTE", nullable=false, length=40)
	public String getReporte() {
		return reporte;
	}
	public void setReporte(String reporte) {
		this.reporte = reporte;
	}
	
	@Transient
	public String getRegistro() {
		return registro;
	}
	public void setRegistro(String registro) {
		this.registro = registro;
	}
}