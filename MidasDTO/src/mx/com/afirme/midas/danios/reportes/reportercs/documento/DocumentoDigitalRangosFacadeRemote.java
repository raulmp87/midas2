package mx.com.afirme.midas.danios.reportes.reportercs.documento;

import java.math.BigDecimal;
import java.util.List;

/**
 * Remote interface for DocumentoDigitalSolicitudDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface DocumentoDigitalRangosFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved
	 * DocumentoDigitalSolicitudDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            DocumentoDigitalSolicitudDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(DocumentoDigitalRangosDTO entity);

	/**
	 * Delete a persistent DocumentoDigitalSolicitudDTO entity.
	 * 
	 * @param entity
	 *            DocumentoDigitalSolicitudDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(DocumentoDigitalRangosDTO entity);

	/**
	 * Persist a previously saved DocumentoDigitalSolicitudDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * DocumentoDigitalSolicitudDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            DocumentoDigitalSolicitudDTO entity to update
	 * @return DocumentoDigitalSolicitudDTO the persisted
	 *         DocumentoDigitalSolicitudDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public DocumentoDigitalRangosDTO update(
			DocumentoDigitalRangosDTO entity);

	public DocumentoDigitalRangosDTO findById(BigDecimal id);

	/**
	 * Find all DocumentoDigitalSolicitudDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the DocumentoDigitalSolicitudDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<DocumentoDigitalSolicitudDTO> found by query
	 */
	public List<DocumentoDigitalRangosDTO> findByProperty(
			String propertyName, Object value);

	/**
	 * Find all DocumentoDigitalSolicitudDTO entities.
	 * 
	 * @return List<DocumentoDigitalSolicitudDTO> all
	 *         DocumentoDigitalSolicitudDTO entities
	 */
	public List<DocumentoDigitalRangosDTO> findAll();
}