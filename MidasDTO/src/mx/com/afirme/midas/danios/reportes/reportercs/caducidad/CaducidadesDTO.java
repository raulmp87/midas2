package mx.com.afirme.midas.danios.reportes.reportercs.caducidad;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "CNSF_CADUCIDADES_AMIS", schema = "MIDAS")
public class CaducidadesDTO implements java.io.Serializable {
	private static final long serialVersionUID = -2203293929348324069L;
	
	private BigDecimal id;
	private BigDecimal anio;
	private String plan;
	private BigDecimal anioVigencia;
	private String tipoMoneda;
	private BigDecimal porcentaje;
	
	public CaducidadesDTO() {
	}
	public CaducidadesDTO( BigDecimal anio, String plan,
			BigDecimal anioVigencia, String tipoMoneda, BigDecimal porcentaje) {
		super();
		this.anio = anio;
		this.plan = plan;
		this.anioVigencia = anioVigencia;
		this.tipoMoneda = tipoMoneda;
		this.porcentaje = porcentaje;
	}
	
	@Id
	@Column(name = "ID", nullable = false, precision = 22, scale = 0)
	@SequenceGenerator(name = "ID_SEQ_GENERADORCADAMIS", allocationSize = 1, sequenceName = "MIDAS.CNSF_CADAMIS_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_SEQ_GENERADORCADAMIS")
	
	public BigDecimal getId() {
		return id;
	}
	
	public void setId(BigDecimal id) {
		this.id = id;
	}

	@Column(name = "ANIO", nullable = true)
	public BigDecimal getAnio() {
		return anio;
	}
	public void setAnio(BigDecimal anio) {
		this.anio = anio;
	}
	@Column(name = "PLAN", nullable = true)
	public String getPlan() {
		return plan;
	}
	public void setPlan(String plan) {
		this.plan = plan;
	}
	@Column(name = "ANIOVIGENCIA", nullable = true)
	public BigDecimal getAnioVigencia() {
		return anioVigencia;
	}
	public void setAnioVigencia(BigDecimal anioVigencia) {
		this.anioVigencia = anioVigencia;
	}
	@Column(name = "TIPOMONEDA", nullable = true)
	public String getTipoMoneda() {
		return tipoMoneda;
	}
	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}
	@Column(name = "PORCENTAJE", nullable = true)
	public BigDecimal getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(BigDecimal porcentaje) {
		this.porcentaje = porcentaje;
	}
	
}