package mx.com.afirme.midas.danios.reportes.cotizacion.generica;

import java.io.Serializable;

public class DatoIncisoCotizacionForm implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7438643867550476126L;
	private String etiqueta;
	private String valor;
	
	public String getEtiqueta() {
		return etiqueta;
	}
	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
}
