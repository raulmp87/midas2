package mx.com.afirme.midas.danios.reportes.reportercs.cargaMasiva;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.catalogos.esquemasreas.EsquemasDTO;
import mx.com.afirme.midas.danios.reportes.reportercs.caducidad.CaducidadesDTO;

public interface CargaMasivaDetalleRanFacadeRemote {
	
	public void save(CargaMasivaRangosRamosDTO entity);
	
	public void save(CargaMasivaDetalleRanDTO entity);
	
	public void save(EsquemasDTO entity);
	
	public void save(CaducidadesDTO entity);

	public void delete(CargaMasivaDetalleRanDTO entity);
	
	public CargaMasivaDetalleRanDTO update(CargaMasivaDetalleRanDTO entity);

	public CargaMasivaDetalleRanDTO findById(CargaMasivaDetalleRanDTO id);
	
	public CargaMasivaRangosRamosDTO findById(CargaMasivaRangosRamosDTO id);


	public List<CargaMasivaRangosRamosDTO> findByProperty(String propertyName,
			Object value);
	
	public List<CargaMasivaDetalleRanDTO> findAll();		
	
	public CargaMasivaDetalleRanDTO agregar2( 
			List<CargaMasivaRangosRamosDTO> direccionesValidas,
			List<CargaMasivaRangosRamosDTO> direccionesInvalidas) ;
	
	public EsquemasDTO agregar(
			List<EsquemasDTO> direccionesValidas) ;
	
	public CaducidadesDTO agregarCad(
			List<CaducidadesDTO> direccionesValidas) ;
	
	public int deleteEsquemas(BigDecimal anio,BigDecimal mes, BigDecimal dia, String cveNegocio) ;
}