package mx.com.afirme.midas.danios.reportes.reportercs;

import java.util.Date;
import java.util.List;
 
import mx.com.afirme.midas.danios.reportes.reportercs.log.SolvenciaLogDTO;

public interface ReporteRCSFacadeRemote {

	public List<ReporteRCSDTO> obtieneReportesRCSGenerico(ReporteRCSDTO movimientoEmision, Double tipoCambio, String nombreSP) 
	throws Exception;
	
	public List<ReporteRCSDTO> obtieneRCSVidaPas(ReporteRCSDTO movimientoEmision) 
	throws Exception;
	
	public List<ReporteRCSDTO> obtieneDetalleRCSVidaCP(ReporteRCSDTO movimientoEmision) 
	throws Exception;
	
	public List<ReporteRCSDTO> obtieneRCSVidaLP(ReporteRCSDTO movimientoEmision) 
	throws Exception;
	
	public List<ReporteRCSDTO> obtieneDetalleRCSVidaLP(ReporteRCSDTO movimientoEmision) 
	throws Exception;
	
	public List<ReporteRCSDTO> obtieneREASRCS(ReporteRCSDTO movimientoEmision, int tipoReporte, Double tipoCambio) 
	throws Exception;
	
	public List<ReporteRCSDTO> obtieneValidacionesRCS( ReporteRCSDTO movimientoEmision) 
	throws Exception;
	
	public List<ReporteRCSDTO> obtieneClaveRamo(String id_ramo,  String modulo) 
	throws Exception;
	
	public boolean actualizaRangos(ReporteRCSDTO movimientoEmision) 
	throws Exception;
	
	public boolean eliminaRangos(ReporteRCSDTO movimientoEmision) 
	throws Exception;
	
	public boolean setRangosDuracionSP(Integer duracionPromedio, Integer duracionRemanente) 
	throws Exception;
		
	public boolean actualizaCalificaciones(int adia,ReporteRCSDTO movimientoEmision,int anio) 
	throws Exception;
	
	public int sp_actualizaNom(String tipo_archivo, String valor) 
	throws Exception;
	
	public String obtieneNomArchivo(String nomArch)
	throws Exception;
	
	public String obtenerNumRangosDuracion(int tipoRango)
	throws Exception;
	
	public int generaInfoRCS(String anio, String mes, String day, String reproceso, String negocio)
	throws Exception;
	
	public int generaInfoRCSVida(String anio, String mes, String day, String reproceso)
	throws Exception;
	
	public List<SolvenciaLogDTO> findByFechaCorte(Date fechaCorte, List<String> negocio);
	
	public Date getFechaUltimoCorteProcesado();
	
	//public void initialize();
}
