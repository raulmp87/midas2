package mx.com.afirme.midas.danios.reportes.reportepml;

public class AtributoEntradaDTO_PML {

	public static final String NUM_POLIZA ="numeroPoliza";
	public static final String NUM_REGISTRO = "numeroRegistro";
	public static final String FECHA_INICIO = "fechaInicio";
	public static final String FECHA_FIN = "fechaFin";
	public static final String INM_VALOR_ASEGURABLE ="inmValorAsegurable";
	public static final String CONT_VALOR_ASEGURABLE = "contValorAsegurable";
	public static final String CONSEC_VALOR_ASEGURABLE = "consecValorAsegurable";
	public static final String CONVENIO_VALOR_ASEGURABLE = "convenioValorAsegurable";
	public static final String PORCENTAJE_RETENCION = "porcentajeRetencion";
	public static final String TIPO_PRIMER_RIESGO = "tipoPrimerRiesgo";
	public static final String MONTO_PRIMER_RIESGO = "montoPrimerRiesgo";
	public static final String CONSEC_LIMITE_MAXIMO = "consecLimiteMaximo";
	public static final String CONSEC_PERIODO_COBERTURA = "consecPeriodoCobertura";
	public static final String CONVENIO_LIMITE_MAXIMO = "convenioLimiteMaximo";
	public static final String INM_DEDUCIBLE = "inmDeducible";
	public static final String CONT_DEDUCIBLE = "contDeducible";
	public static final String CONSEC_DEDUCIBLE = "consecDeducible";
	public static final String CONVENIO_DEDUCIBLE= "convenioDeducible";
	public static final String INM_COASEGURO="inmCoaseguro";
	public static final String CONT_COASEGURO="contCoaseguro";
	public static final String CONSEC_COASEGURO="consecCoaseguro";
	public static final String CONVENIO_COASEGURO="convenioCoaseguro";
	public static final String CLAVE_ESTADO="claveEstado";
	public static final String CODIGO_POSTAL="codigoPostal";
	public static final String LONGITUD="longitud";
	public static final String LATITUD="latitud";
	public static final String PRIMERA_LINEA_MAR="primeraLineaMar";
	public static final String PRIMERA_LINEA_LAGO="primeraLineaLago";
	public static final String SOBREELEVACION_DESPLANTE="sobreelevacionDesplante";
	public static final String RUGOSIDAD="rugosidad";
	public static final String USO_INMUEBLE="usoInmueble";
	public static final String NUM_PISOS="numeroPisos";
	public static final String PISO="piso";
	public static final String TIPO_CUBIERTA="tipoCubierta";
	public static final String FORMA_CUBIERTA="formaCubierta";
	public static final String IRRE_PLANTA="irrePlanta";
	public static final String OBJETOS_CERCA="objetosCerca";
	public static final String AZOTEA="azotea";
	public static final String TAMANO_CRISTAL="tamanoCristal";
	public static final String TIPO_VENTANAS="tipoVentanas";
	public static final String TIPO_DOMOS="tipoDomos";
	public static final String SOPORTE_VENTANA="soporteVentana";
	public static final String PROCENTAJE_CRISTAL_FACHADAS="porcentajeCristalFachadas";
	public static final String PORCENTAJE_DOMOS="porcentajeDomos";
	public static final String OTROS_FACHADA="otrosFachada";
	public static final String MUROS_CONTENCION="murosContencion";
	public static final String VALOR_ASEGURABLE="valorAsegurable";
	public static final String VALOR_RETENIDO="valorRetenido";
	public static final String PRIMA="prima";
	public static final String CEDIDA="cedida";
	public static final String RETENIDA="retenida";
	public static final String MONEDA="moneda";
	public static final String RSR_T="rsrt";
	public static final String OFI_EMI="ofiEmi";
	public static final String INCISO="inciso";
	public static final String ZONA_AMIS="zonaAmis";
	public static final String TIPO_POLIZA="tipoPoliza";
	public static final String NUM_CAPA="numCapa";
	public static final String LIMITE_MAXIMO="limiteMaximo";
	public static final String COASEGURO="coaseguro";
	public static final String INM_PORCENTAJE_RETENCION="inmPorcentajeRetencion";
	public static final String INM_LIMITE_MAXIMO="inmLimiteMaximo";
	public static final String CONT_PORCENTAJE_RETENCION="contPorcentajeRetencion";
	public static final String CONT_LIMITE_MAXIMO="contLimiteMaximo";
	public static final String CONSEC_PORCENTAJE_RETENCION="consecPorcentajeRetencion";
	public static final String OTR_FECHA="otrFecha";
	public static final String OTR_REFORZADA="otrReforzada";
	public static final String OTR_DA_REPARADO="otrDaReparado";
	public static final String OTR_DA_PREVIOS="otrDaPrevios";
	public static final String OTR_HUNDIMIENTOS="otrHundimientos";
	public static final String OTR_IRRE_PLANTA="otrIrrePlanta";
	public static final String OTR_IRRE_ELEVACION="otrIrreElevacion";
	public static final String OTR_ESQUINA="otrEsquina";
	public static final String OTR_GOLPETEO="otrGolpeteo";
	public static final String OTR_SOBREPESO="otrSobrepeso";
	public static final String OTR_COLUMNAS_CORTAS="otrColumnasCortas";
	public static final String EST_CONTRAVENTEO="estContraventeo";
	public static final String EST_MUROS_PRE="estMurosPre";
	public static final String EST_CLAROS="estClaros";
	public static final String EST_CUBIERTA="estCubierta";
	public static final String EST_MUROS="estMuros";
	public static final String EST_TRABES="estTrabes";
	public static final String EST_COLUMNAS="estColumnas";
	public static final String EDI_USO="ediUso";
	public static final String EDI_FECHA_CONSTRUCCION="ediFechaConstruccion";
	public static final String EDI_SUELO="ediSuelo";
	public static final String CLAVE_MUNICIPIO="claveMunicipio";
	public static final String ES_INDUSTRIAL="esIndustrial";
	public static final String ZONA_SISMICA="zonaSismica";
}
