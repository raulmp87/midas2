package mx.com.afirme.midas.danios.reportes.reportepmltev;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ReportePMLTEVDTO implements Serializable{

	private static final long serialVersionUID = -4339052874717963401L;

	private Integer claveTipoReporte;
	
	private BigDecimal porcentajeRetencion;
	private BigDecimal inmPorcentajeRetencion;
	private BigDecimal inmLimiteMaximo;
	private BigDecimal contPorcentajeRetencion;
	private BigDecimal contLimiteMaximo;
	private BigDecimal consecPorcentajeRetencion;
	private String zonaSismica;
	private Short esIndustrial;
	private Integer claveMunicipio;
	private Short ediSuelo;
	private Integer ediFechaConstruccion;
	private Short ediUso;
	private Short estColumnas;
	private Short estTrabes;
	private Short estMuros;
	private Short estCubierta;
	private Short estClaros;
	private Short estMurosPre;
	private Short estContraventeo;
	private Short otrColumnasCortas;
	private Short otrSobrepeso;
	private Short otrGolpeteo;
	private Short otrEsquina;
	private Short otrIrreElevacion;
	private Short otrIrrePlanta;
	private Short otrHundimientos;
	private Short otrDaPrevios;
	private Short otrDaReparado;
	private Short otrReforzada;
	private Integer otrFecha;
	private Date fechaCorte;
	private Double tipoCambio;
	
	private Integer idRegistro;
	private String numeroPoliza;
	private Integer numeroRegistro;
	private Date fechaInicio;
	private Date fechaFin;
	private BigDecimal inmValorAsegurable;
	private BigDecimal contValorAsegurable;
	private BigDecimal consecValorAsegurable;
	
	private BigDecimal consecLimiteMaximo;
	private BigDecimal inmDeducible;
	private BigDecimal contDeducible;
	private BigDecimal consecDeducible;
	private BigDecimal inmCoaseguro;
	private BigDecimal contCoaseguro;
	private BigDecimal consecCoaseguro;
	private Short claveEstado;
	private Integer codigoPostal;
	private BigDecimal longitud;
	private BigDecimal latitud;
	private Short numeroPisos;
	private BigDecimal valorAsegurable;
	private BigDecimal valorRetenido;
	private BigDecimal prima;
	private BigDecimal cedida;
	private BigDecimal retenida;
	private String moneda;
	private String rsrt;
	private String ofiEmi;
	private String inciso;
	private String zonaAmis;
	private String tipoPoliza;
	private String numCapa;
	private BigDecimal limiteMaximo;
	private BigDecimal coaseguro;
	
//	private BigDecimal regresarNullSiEsCero(BigDecimal valor){
//		BigDecimal valorCalculado = valor;
//		if(valor != null && valor.compareTo(BigDecimal.ZERO)==0)
//			valorCalculado = null;
//		return valorCalculado;
//	}
	
	public Date getFechaCorte() {
		return fechaCorte;
	}
	public void setFechaCorte(Date fechaCorte) {
		this.fechaCorte = fechaCorte;
	}
	public Double getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public Integer getIdRegistro() {
		return idRegistro;
	}
	public void setIdRegistro(Integer idRegistro) {
		this.idRegistro = idRegistro;
	}
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public Integer getNumeroRegistro() {
		return numeroRegistro;
	}
	public void setNumeroRegistro(Integer numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
	}
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	public BigDecimal getInmValorAsegurable() {
		return inmValorAsegurable;
	}
	public void setInmValorAsegurable(BigDecimal inmValorAsegurable) {
		this.inmValorAsegurable = inmValorAsegurable;
	}
	public BigDecimal getContValorAsegurable() {
		return contValorAsegurable;
	}
	public void setContValorAsegurable(BigDecimal contValorAsegurable) {
		this.contValorAsegurable = contValorAsegurable;
	}
	public BigDecimal getConsecValorAsegurable() {
		return consecValorAsegurable;
	}
	public void setConsecValorAsegurable(BigDecimal consecValorAsegurable) {
		this.consecValorAsegurable = consecValorAsegurable;
	}
	public BigDecimal getConsecLimiteMaximo() {
		return consecLimiteMaximo;
	}
	public void setConsecLimiteMaximo(BigDecimal consecLimiteMaximo) {
		this.consecLimiteMaximo = consecLimiteMaximo;
	}
	public BigDecimal getInmDeducible() {
		return inmDeducible;
	}
	public void setInmDeducible(BigDecimal inmDeducible) {
		this.inmDeducible = inmDeducible;
	}
	public BigDecimal getContDeducible() {
		return contDeducible;
	}
	public void setContDeducible(BigDecimal contDeducible) {
		this.contDeducible = contDeducible;
	}
	public BigDecimal getConsecCoaseguro() {
		return consecCoaseguro;
	}
	public void setConsecCoaseguro(BigDecimal consecCoaseguro) {
		this.consecCoaseguro = consecCoaseguro;
	}
	public Short getClaveEstado() {
		return claveEstado;
	}
	public void setClaveEstado(Short claveEstado) {
		this.claveEstado = claveEstado;
	}
	public Integer getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(Integer codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public BigDecimal getLongitud() {
		return longitud;
	}
	public void setLongitud(BigDecimal longitud) {
		this.longitud = longitud;
	}
	public BigDecimal getLatitud() {
		return latitud;
	}
	public void setLatitud(BigDecimal latitud) {
		this.latitud = latitud;
	}
	public Short getNumeroPisos() {
		return numeroPisos;
	}
	public void setNumeroPisos(Short numeroPisos) {
		this.numeroPisos = numeroPisos;
	}
	public BigDecimal getValorAsegurable() {
		return valorAsegurable;
	}
	public void setValorAsegurable(BigDecimal valorAsegurable) {
		this.valorAsegurable = valorAsegurable;
	}
	public BigDecimal getValorRetenido() {
		return valorRetenido;
	}
	public void setValorRetenido(BigDecimal valorRetenido) {
		this.valorRetenido = valorRetenido;
	}
	public BigDecimal getPrima() {
		return prima;
	}
	public void setPrima(BigDecimal prima) {
		this.prima = prima;
	}
	public BigDecimal getCedida() {
		return cedida;
	}
	public void setCedida(BigDecimal cedida) {
		this.cedida = cedida;
	}
	public BigDecimal getRetenida() {
		return retenida;
	}
	public void setRetenida(BigDecimal retenida) {
		this.retenida = retenida;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getRsrt() {
		return rsrt;
	}
	public void setRsrt(String rsrt) {
		this.rsrt = rsrt;
	}
	public String getOfiEmi() {
		return ofiEmi;
	}
	public void setOfiEmi(String ofiEmi) {
		this.ofiEmi = ofiEmi;
	}
	public String getInciso() {
		return inciso;
	}
	public void setInciso(String inciso) {
		this.inciso = inciso;
	}
	public String getZonaAmis() {
		return zonaAmis;
	}
	public void setZonaAmis(String zonaAmis) {
		this.zonaAmis = zonaAmis;
	}
	public String getTipoPoliza() {
		return tipoPoliza;
	}
	public void setTipoPoliza(String tipoPoliza) {
		this.tipoPoliza = tipoPoliza;
	}
	public String getNumCapa() {
		return numCapa;
	}
	public void setNumCapa(String numCapa) {
		this.numCapa = numCapa;
	}
	public BigDecimal getLimiteMaximo() {
		return limiteMaximo;
	}
	public void setLimiteMaximo(BigDecimal limiteMaximo) {
		this.limiteMaximo = limiteMaximo;
	}
	public BigDecimal getCoaseguro() {
		return coaseguro;
	}
	public void setCoaseguro(BigDecimal coaseguro) {
		this.coaseguro = coaseguro;
	}
	public BigDecimal getInmPorcentajeRetencion() {
		return inmPorcentajeRetencion;
	}
	public void setInmPorcentajeRetencion(BigDecimal inmPorcentajeRetencion) {
		this.inmPorcentajeRetencion = inmPorcentajeRetencion;
	}
	public BigDecimal getInmLimiteMaximo() {
		return inmLimiteMaximo;
	}
	public void setInmLimiteMaximo(BigDecimal inmLimiteMaximo) {
		this.inmLimiteMaximo = inmLimiteMaximo;
	}
	public BigDecimal getInmCoaseguro() {
		return inmCoaseguro;
	}
	public void setInmCoaseguro(BigDecimal inmCoaseguro) {
		this.inmCoaseguro = inmCoaseguro;
	}
	public BigDecimal getContPorcentajeRetencion() {
		return contPorcentajeRetencion;
	}
	public void setContPorcentajeRetencion(BigDecimal contPorcentajeRetencion) {
		this.contPorcentajeRetencion = contPorcentajeRetencion;
	}
	public BigDecimal getContLimiteMaximo() {
		return contLimiteMaximo;
	}
	public void setContLimiteMaximo(BigDecimal contLimiteMaximo) {
		this.contLimiteMaximo = contLimiteMaximo;
	}
	public BigDecimal getContCoaseguro() {
		return contCoaseguro;
	}
	public void setContCoaseguro(BigDecimal contCoaseguro) {
		this.contCoaseguro = contCoaseguro;
	}
	public BigDecimal getConsecPorcentajeRetencion() {
		return consecPorcentajeRetencion;
	}
	public void setConsecPorcentajeRetencion(BigDecimal consecPorcentajeRetencion) {
		this.consecPorcentajeRetencion = consecPorcentajeRetencion;
	}
	public BigDecimal getConsecDeducible() {
		return consecDeducible;
	}
	public void setConsecDeducible(BigDecimal consecDeducible) {
		this.consecDeducible = consecDeducible;
	}
	public String getZonaSismica() {
		return zonaSismica;
	}
	public void setZonaSismica(String zonaSismica) {
		this.zonaSismica = zonaSismica;
	}
	public Short getEsIndustrial() {
		return esIndustrial;
	}
	public void setEsIndustrial(Short esIndustrial) {
		this.esIndustrial = esIndustrial;
	}
	public Integer getClaveMunicipio() {
		return claveMunicipio;
	}
	public void setClaveMunicipio(Integer claveMunicipio) {
		this.claveMunicipio = claveMunicipio;
	}
	public Short getEdiSuelo() {
		return ediSuelo;
	}
	public void setEdiSuelo(Short ediSuelo) {
		this.ediSuelo = ediSuelo;
	}
	public Integer getEdiFechaConstruccion() {
		return ediFechaConstruccion;
	}
	public void setEdiFechaConstruccion(Integer ediFechaConstruccion) {
		this.ediFechaConstruccion = ediFechaConstruccion;
	}
	public Short getEdiUso() {
		return ediUso;
	}
	public void setEdiUso(Short ediUso) {
		this.ediUso = ediUso;
	}
	public Short getEstColumnas() {
		return estColumnas;
	}
	public void setEstColumnas(Short estColumnas) {
		this.estColumnas = estColumnas;
	}
	public Short getEstTrabes() {
		return estTrabes;
	}
	public void setEstTrabes(Short estTrabes) {
		this.estTrabes = estTrabes;
	}
	public Short getEstMuros() {
		return estMuros;
	}
	public void setEstMuros(Short estMuros) {
		this.estMuros = estMuros;
	}
	public Short getEstCubierta() {
		return estCubierta;
	}
	public void setEstCubierta(Short estCubierta) {
		this.estCubierta = estCubierta;
	}
	public Short getEstClaros() {
		return estClaros;
	}
	public void setEstClaros(Short estClaros) {
		this.estClaros = estClaros;
	}
	public Short getEstMurosPre() {
		return estMurosPre;
	}
	public void setEstMurosPre(Short estMurosPre) {
		this.estMurosPre = estMurosPre;
	}
	public Short getEstContraventeo() {
		return estContraventeo;
	}
	public void setEstContraventeo(Short estContraventeo) {
		this.estContraventeo = estContraventeo;
	}
	public Short getOtrColumnasCortas() {
		return otrColumnasCortas;
	}
	public void setOtrColumnasCortas(Short otrColumnasCortas) {
		this.otrColumnasCortas = otrColumnasCortas;
	}
	public Short getOtrSobrepeso() {
		return otrSobrepeso;
	}
	public void setOtrSobrepeso(Short otrSobrepeso) {
		this.otrSobrepeso = otrSobrepeso;
	}
	public Short getOtrGolpeteo() {
		return otrGolpeteo;
	}
	public void setOtrGolpeteo(Short otrGolpeteo) {
		this.otrGolpeteo = otrGolpeteo;
	}
	public Short getOtrEsquina() {
		return otrEsquina;
	}
	public void setOtrEsquina(Short otrEsquina) {
		this.otrEsquina = otrEsquina;
	}
	public Short getOtrIrreElevacion() {
		return otrIrreElevacion;
	}
	public void setOtrIrreElevacion(Short otrIrreElevacion) {
		this.otrIrreElevacion = otrIrreElevacion;
	}
	public Short getOtrIrrePlanta() {
		return otrIrrePlanta;
	}
	public void setOtrIrrePlanta(Short otrIrrePlanta) {
		this.otrIrrePlanta = otrIrrePlanta;
	}
	public Short getOtrHundimientos() {
		return otrHundimientos;
	}
	public void setOtrHundimientos(Short otrHundimientos) {
		this.otrHundimientos = otrHundimientos;
	}
	public Short getOtrDaPrevios() {
		return otrDaPrevios;
	}
	public void setOtrDaPrevios(Short otrDaPrevios) {
		this.otrDaPrevios = otrDaPrevios;
	}
	public Short getOtrDaReparado() {
		return otrDaReparado;
	}
	public void setOtrDaReparado(Short otrDaReparado) {
		this.otrDaReparado = otrDaReparado;
	}
	public Short getOtrReforzada() {
		return otrReforzada;
	}
	public void setOtrReforzada(Short otrReforzada) {
		this.otrReforzada = otrReforzada;
	}
	public Integer getOtrFecha() {
		return otrFecha;
	}
	public void setOtrFecha(Integer otrFecha) {
		this.otrFecha = otrFecha;
	}
	public BigDecimal getPorcentajeRetencion() {
		return porcentajeRetencion;
	}
	public void setPorcentajeRetencion(BigDecimal porcentajeRetencion) {
		this.porcentajeRetencion = porcentajeRetencion;
	}
	public Integer getClaveTipoReporte() {
		return claveTipoReporte;
	}
	public void setClaveTipoReporte(Integer claveTipoReporte) {
		this.claveTipoReporte = claveTipoReporte;
	}
}
