package mx.com.afirme.midas.danios.reportes.reportercs.calificaciones;

public class CalificacionesREASDTO {

	String rgre;
	String ambest;
	String snp;
	String otras;
	
	int calificacion;

	public CalificacionesREASDTO(String rgre, String ambest, String snp,
			String otras, int calificacion) {
		super();
		this.rgre = rgre;
		this.ambest = ambest;
		this.snp = snp;
		this.otras = otras;
		this.calificacion = calificacion;
	}

	public String getRgre() {
		return rgre;
	}

	public void setRgre(String rgre) {
		this.rgre = rgre;
	}

	public String getAmbest() {
		return ambest;
	}

	public void setAmbest(String ambest) {
		this.ambest = ambest;
	}

	public String getSnp() {
		return snp;
	}

	public void setSnp(String snp) {
		this.snp = snp;
	}

	public String getOtras() {
		return otras;
	}

	public void setOtras(String otras) {
		this.otras = otras;
	}

	public int getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(int calificacion) {
		this.calificacion = calificacion;
	}

	@Override
	public String toString() {
		return "CalificacionesREASDTO [rgre=" + rgre + ", ambest=" + ambest
				+ ", snp=" + snp + ", otras=" + otras + ", calificacion="
				+ calificacion + "]";
	}
	
}
