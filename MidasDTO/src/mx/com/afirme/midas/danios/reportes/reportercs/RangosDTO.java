package mx.com.afirme.midas.danios.reportes.reportercs;

import java.math.BigDecimal;

public class RangosDTO implements java.io.Serializable {
	private static final long serialVersionUID = -2203293929348324069L;
	private BigDecimal tipoRenglon;
	private String valor;
	private String valor2;
	private String valor3;
	private String valor4;
	

	// Constructors

	/** default constructor */
	public RangosDTO() {
	}

	
	public BigDecimal getTipoRenglon() {
		return this.tipoRenglon;
	}

	public void setTipoRenglon(BigDecimal idToDireccion) {
		this.tipoRenglon = idToDireccion;
	}
	
	

	public String getValor() {
		BigDecimal bigDecimal =  new BigDecimal(valor);
		return bigDecimal.intValue()+"";
	}

	public void setValor(String nombreCalle) {
		this.valor = nombreCalle;
	}


	public String getValor2() {
		if(valor2 == null)
			valor2 = "00";
		BigDecimal bigDecimal =  new BigDecimal(valor2);
		return bigDecimal.intValue()+"";
	}


	public void setValor2(String valor2) {
		this.valor2 = valor2;
	}


	public String getValor3() {
		
		if(valor3 == null)
			valor3 = "00";
		
		BigDecimal bigDecimal =  new BigDecimal(valor3);
		return bigDecimal.intValue()+"";
	}


	public void setValor3(String valor3) {
		this.valor3 = valor3;
	}


	public String getValor4() {
		
		if(valor4 == null)
			valor4 = "00";
		BigDecimal bigDecimal =  new BigDecimal(valor4);
		return bigDecimal.intValue()+"";
	}


	public void setValor4(String valor4) {
		this.valor4 = valor4;
	}


	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof RangosDTO))
			return false;
		RangosDTO other = (RangosDTO) obj;
		
		if (valor == null) {
			if (other.valor != null)
				return false;
		} else if (!valor.equals(other.valor))
			return false;
		
		return true;
	}
	
	
}