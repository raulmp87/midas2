package mx.com.afirme.midas.danios.reportes.reporterr6;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Remote;



@Remote
public interface ReporteRR6FacadeRemote {

	List<ReporteRR6DTO> obtenerReporte(String fechaIni, String fechaFinal, BigDecimal tipoCambio, int reporte, String nombreUsuario) throws Exception;
	
	void procesarReporte(String fechaIni, String fechaFinal, BigDecimal tipoCambio, int reporte, String nombreUsuario) throws Exception;
	
	/**
	 * Find all ReporteRR6DTO entities.
	 * 
	 * @return List<ReporteRR6DTO> all ReporteRR6DTO entities
	 */
	List<ReporteRR6DTO> findAll(String fechaCorte);
	
	ReporteRR6DTO findById(Integer id);
	
	/**
	 * Delete a persistent ReporteRR6DTO entity.
	 * 
	 * @param entity
	 *            ReporteRR6DTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	void delete(ReporteRR6DTO entity);
	
	/**
	 * Find all CargaRR6DTO entities.
	 * 
	 * @return List<CargaRR6DTO> all CargaRR6DTO entities
	 */
	List<CargaRR6DTO> findAllCarga(String fechaCorte);
	
	int obtenerRegistros(Date fechaIni, Date fechaFinal, BigDecimal tipoCambio, int reporte, String nombreUsuario);
	
	void actualizarEstatusCarga(int idCarga, int estatusCarga);
	
	List<Object[]> obtenerReporteExcel(Date fechaFinal) throws Exception;
	
	List<String> obtenerArchivos(Date fechaFinal, int tipoArchivo) throws Exception;
	
	int obtenerRegistrosSA(Date fechaFinal);
}


