package mx.com.afirme.midas.danios.reportes.reportebasesemision;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ReporteBasesEmisionDTO implements Serializable {

	private static final long serialVersionUID = 2299753824789845932L;

	private Date fechaInicio;
	private Date fechaFin;
	private Integer idRamo;
	private Integer idSubRamo;
	private Integer idNivelContratoReaseguro;
	private Integer nivelAgrupamiento;
	
	private String oficinaEmision;
	private String descOficinaEmision;
	private String descRamo;
	private String ctaContableRamo;
	private String descSubRamo;
	private String ctaContableSubRamo;
	private String numeroPoliza;
	private String monedaPoliza;
	private String codigoProducto;
	private String descProducto;
	private String codigoTipoNegocio;
	private String descTipoNegocio;
	private Integer codigoUsuarioAsegurado;
	private String nombreUsuarioAsegurado;
	private Integer claveAgente;
	private String nombreAgente;
	private String formaPago;
	private String estatusPoliza;
	private String giroAsociadoPoliza;
	private Integer numeroEndoso;
	private String descTipoEndoso;
	private String descMotivoEndoso;
	private Date fechaEmision;
	private Date fechaInicioVigencia;
	private Date fechaFinVigencia;
	private BigDecimal tipoCambio;
	
	//Campos Agregados 07/04/2010 por CMM
	private BigDecimal numeroInciso;
	private String descSeccion;
	private String descCobertura;
	
	private BigDecimal importePrimaNeta;
	private BigDecimal importeRecargos;
	private BigDecimal importeDerechos;
	private BigDecimal importeComisiones;
	
	private BigDecimal importeBonifCom;
	private BigDecimal importeComisionesRPF;
	private BigDecimal importeBonifComRPF;
	
	private BigDecimal importeIVA;
	private String nivelContratoReaseguro;
	private BigDecimal importePrimaRetenida;
	private BigDecimal importePrimaCedida;
	private BigDecimal importePrimaFacultada;
	private String parteRelacionada;
	
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	public Integer getIdRamo() {
		return idRamo;
	}
	public void setIdRamo(Integer idRamo) {
		this.idRamo = idRamo;
	}
	public Integer getIdSubRamo() {
		return idSubRamo;
	}
	public void setIdSubRamo(Integer idSubRamo) {
		this.idSubRamo = idSubRamo;
	}
	public Integer getIdNivelContratoReaseguro() {
		return idNivelContratoReaseguro;
	}
	public void setIdNivelContratoReaseguro(Integer idNivelContratoReaseguro) {
		this.idNivelContratoReaseguro = idNivelContratoReaseguro;
	}
	public Integer getNivelAgrupamiento() {
		return nivelAgrupamiento;
	}
	public void setNivelAgrupamiento(Integer nivelAgrupamiento) {
		this.nivelAgrupamiento = nivelAgrupamiento;
	}
	public String getOficinaEmision() {
		return oficinaEmision;
	}
	public void setOficinaEmision(String oficinaEmision) {
		this.oficinaEmision = oficinaEmision;
	}
	public String getDescOficinaEmision() {
		return descOficinaEmision;
	}
	public void setDescOficinaEmision(String descOficinaEmision) {
		this.descOficinaEmision = descOficinaEmision;
	}
	public String getDescRamo() {
		return descRamo;
	}
	public void setDescRamo(String descRamo) {
		this.descRamo = descRamo;
	}
	public String getCtaContableRamo() {
		return ctaContableRamo;
	}
	public void setCtaContableRamo(String ctaContableRamo) {
		this.ctaContableRamo = ctaContableRamo;
	}
	public String getDescSubRamo() {
		return descSubRamo;
	}
	public void setDescSubRamo(String descSubRamo) {
		this.descSubRamo = descSubRamo;
	}
	public String getCtaContableSubRamo() {
		return ctaContableSubRamo;
	}
	public void setCtaContableSubRamo(String ctaContableSubRamo) {
		this.ctaContableSubRamo = ctaContableSubRamo;
	}
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public String getMonedaPoliza() {
		return monedaPoliza;
	}
	public void setMonedaPoliza(String monedaPoliza) {
		this.monedaPoliza = monedaPoliza;
	}
	public String getCodigoProducto() {
		return codigoProducto;
	}
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	public String getDescProducto() {
		return descProducto;
	}
	public void setDescProducto(String descProducto) {
		this.descProducto = descProducto;
	}
	public String getCodigoTipoNegocio() {
		return codigoTipoNegocio;
	}
	public void setCodigoTipoNegocio(String codigoTipoNegocio) {
		this.codigoTipoNegocio = codigoTipoNegocio;
	}
	public String getDescTipoNegocio() {
		return descTipoNegocio;
	}
	public void setDescTipoNegocio(String descTipoNegocio) {
		this.descTipoNegocio = descTipoNegocio;
	}
	public Integer getCodigoUsuarioAsegurado() {
		return codigoUsuarioAsegurado;
	}
	public void setCodigoUsuarioAsegurado(Integer codigoUsuarioAsegurado) {
		this.codigoUsuarioAsegurado = codigoUsuarioAsegurado;
	}
	public String getNombreUsuarioAsegurado() {
		return nombreUsuarioAsegurado;
	}
	public void setNombreUsuarioAsegurado(String nombreUsuarioAsegurado) {
		this.nombreUsuarioAsegurado = nombreUsuarioAsegurado;
	}
	public Integer getClaveAgente() {
		return claveAgente;
	}
	public void setClaveAgente(Integer claveAgente) {
		this.claveAgente = claveAgente;
	}
	public String getNombreAgente() {
		return nombreAgente;
	}
	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}
	public String getFormaPago() {
		return formaPago;
	}
	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	public String getEstatusPoliza() {
		return estatusPoliza;
	}
	public void setEstatusPoliza(String estatusPoliza) {
		this.estatusPoliza = estatusPoliza;
	}
	public String getGiroAsociadoPoliza() {
		return giroAsociadoPoliza;
	}
	public void setGiroAsociadoPoliza(String giroAsociadoPoliza) {
		this.giroAsociadoPoliza = giroAsociadoPoliza;
	}
	public Integer getNumeroEndoso() {
		return numeroEndoso;
	}
	public void setNumeroEndoso(Integer numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}
	public String getDescTipoEndoso() {
		return descTipoEndoso;
	}
	public void setDescTipoEndoso(String descTipoEndoso) {
		this.descTipoEndoso = descTipoEndoso;
	}
	public String getDescMotivoEndoso() {
		return descMotivoEndoso;
	}
	public void setDescMotivoEndoso(String descMotivoEndoso) {
		this.descMotivoEndoso = descMotivoEndoso;
	}
	public Date getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}
	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}
	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}
	public BigDecimal getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(BigDecimal tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public BigDecimal getImportePrimaNeta() {
		return importePrimaNeta;
	}
	public void setImportePrimaNeta(BigDecimal importePrimaNeta) {
		this.importePrimaNeta = importePrimaNeta;
	}
	public BigDecimal getImporteRecargos() {
		return importeRecargos;
	}
	public void setImporteRecargos(BigDecimal importeRecargos) {
		this.importeRecargos = importeRecargos;
	}
	public BigDecimal getImporteDerechos() {
		return importeDerechos;
	}
	public void setImporteDerechos(BigDecimal importeDerechos) {
		this.importeDerechos = importeDerechos;
	}
	public BigDecimal getImporteComisiones() {
		return importeComisiones;
	}
	public void setImporteComisiones(BigDecimal importeComisiones) {
		this.importeComisiones = importeComisiones;
	}
	public BigDecimal getImporteBonifCom() {
		return importeBonifCom;
	}
	public void setImporteBonifCom(BigDecimal importeBonifCom) {
		this.importeBonifCom = importeBonifCom;
	}
	public BigDecimal getImporteComisionesRPF() {
		return importeComisionesRPF;
	}
	public void setImporteComisionesRPF(BigDecimal importeComisionesRPF) {
		this.importeComisionesRPF = importeComisionesRPF;
	}
	public BigDecimal getImporteBonifComRPF() {
		return importeBonifComRPF;
	}
	public void setImporteBonifComRPF(BigDecimal importeBonifComRPF) {
		this.importeBonifComRPF = importeBonifComRPF;
	}
	public BigDecimal getImporteIVA() {
		return importeIVA;
	}
	public void setImporteIVA(BigDecimal importeIVA) {
		this.importeIVA = importeIVA;
	}
	public String getNivelContratoReaseguro() {
		return nivelContratoReaseguro;
	}
	public void setNivelContratoReaseguro(String nivelContratoReaseguro) {
		this.nivelContratoReaseguro = nivelContratoReaseguro;
	}
	public BigDecimal getImportePrimaRetenida() {
		return importePrimaRetenida;
	}
	public void setImportePrimaRetenida(BigDecimal importePrimaRetenida) {
		this.importePrimaRetenida = importePrimaRetenida;
	}
	public BigDecimal getImportePrimaCedida() {
		return importePrimaCedida;
	}
	public void setImportePrimaCedida(BigDecimal importePrimaCedida) {
		this.importePrimaCedida = importePrimaCedida;
	}
	public BigDecimal getImportePrimaFacultada() {
		return importePrimaFacultada;
	}
	public void setImportePrimaFacultada(BigDecimal importePrimaFacultada) {
		this.importePrimaFacultada = importePrimaFacultada;
	}
	public void setNumeroInciso(BigDecimal numeroInciso) {
	    this.numeroInciso = numeroInciso;
	}
	public BigDecimal getNumeroInciso() {
	    return numeroInciso;
	}
	public void setDescSeccion(String descSeccion) {
	    this.descSeccion = descSeccion;
	}
	public String getDescSeccion() {
	    return descSeccion;
	}
	public void setDescCobertura(String descCobertura) {
	    this.descCobertura = descCobertura;
	}
	public String getDescCobertura() {
	    return descCobertura;
	}
	public String getParteRelacionada() {
		return parteRelacionada;
	}
	public void setParteRelacionada(String parteRelacionada) {
		this.parteRelacionada = parteRelacionada;
	}
	

	
	
	
	
	
	
}
