package mx.com.afirme.midas.danios.reportes.reportemonitoreosol;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ReporteMonitoreoSolicitudesDTO implements Serializable {

	private static final long serialVersionUID = -1744423841729768412L;
	
	private	BigDecimal	idSOL;
	private	String	fechaSOL;
	private	String	tipoSOL;
	private	String	tipoEndoso;
	private	String	estatusSOL;
	private	String	codigoProductoSOL;
	private	Integer	codigoEjecutivoSOL;
	private	String	nombreEjecutivoSOL;
	private	Integer	codigoAgenteSOL;
	private	String	nombreAgenteSOL;
	private	String	codigoUsuarioSOL;
	private	Integer	horasSOL;
	private	BigDecimal	idODT;
	private	String	fechaODT;
	private	String	codigoUsuarioODT;
	private	Integer	horasODT;
	private	BigDecimal	idCOT;
	private	String	fechaCOT;
	private	String	codigoUsuarioCOT;
	private	Integer	horasCOT;
	private	String	fechaEMI;
	private	String	codigoUsuarioEMI;
	private	Integer	horasEMI;
	private	String	fechaTOT;
	private	String	codigoUsuarioTOT;
	private	BigDecimal	idPOL;
	private	Integer	numeroEndosoPOL;
	private	Integer	horasTOT;
	
	private Date fechaInicio;
	private Date fechaFin;
	private BigDecimal idProductoSol;
	private String nombreSolicitante;
	
	public BigDecimal getIdSOL() {
		return idSOL;
	}
	public void setIdSOL(BigDecimal idSOL) {
		this.idSOL = idSOL;
	}
	public String getFechaSOL() {
		return fechaSOL;
	}
	public void setFechaSOL(String fechaSOL) {
		this.fechaSOL = fechaSOL;
	}
	public String getTipoSOL() {
		return tipoSOL;
	}
	public void setTipoSOL(String tipoSOL) {
		this.tipoSOL = tipoSOL;
	}
	public String getTipoEndoso() {
		return tipoEndoso;
	}
	public void setTipoEndoso(String tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}
	public String getEstatusSOL() {
		return estatusSOL;
	}
	public void setEstatusSOL(String estatusSOL) {
		this.estatusSOL = estatusSOL;
	}
	public String getCodigoProductoSOL() {
		return codigoProductoSOL;
	}
	public void setCodigoProductoSOL(String codigoProductoSOL) {
		this.codigoProductoSOL = codigoProductoSOL;
	}
	public Integer getCodigoEjecutivoSOL() {
		return codigoEjecutivoSOL;
	}
	public void setCodigoEjecutivoSOL(Integer codigoEjecutivoSOL) {
		this.codigoEjecutivoSOL = codigoEjecutivoSOL;
	}
	public String getNombreEjecutivoSOL() {
		return nombreEjecutivoSOL;
	}
	public void setNombreEjecutivoSOL(String nombreEjecutivoSOL) {
		this.nombreEjecutivoSOL = nombreEjecutivoSOL;
	}
	public Integer getCodigoAgenteSOL() {
		return codigoAgenteSOL;
	}
	public void setCodigoAgenteSOL(Integer codigoAgenteSOL) {
		this.codigoAgenteSOL = codigoAgenteSOL;
	}
	public String getNombreAgenteSOL() {
		return nombreAgenteSOL;
	}
	public void setNombreAgenteSOL(String nombreAgenteSOL) {
		this.nombreAgenteSOL = nombreAgenteSOL;
	}
	public String getCodigoUsuarioSOL() {
		return codigoUsuarioSOL;
	}
	public void setCodigoUsuarioSOL(String codigoUsuarioSOL) {
		this.codigoUsuarioSOL = codigoUsuarioSOL;
	}
	public Integer getHorasSOL() {
		return horasSOL;
	}
	public void setHorasSOL(Integer horasSOL) {
		this.horasSOL = horasSOL;
	}
	public BigDecimal getIdODT() {
		return idODT;
	}
	public void setIdODT(BigDecimal idODT) {
		this.idODT = idODT;
	}
	public String getFechaODT() {
		return fechaODT;
	}
	public void setFechaODT(String fechaODT) {
		this.fechaODT = fechaODT;
	}
	public String getCodigoUsuarioODT() {
		return codigoUsuarioODT;
	}
	public void setCodigoUsuarioODT(String codigoUsuarioODT) {
		this.codigoUsuarioODT = codigoUsuarioODT;
	}
	public Integer getHorasODT() {
		return horasODT;
	}
	public void setHorasODT(Integer horasODT) {
		this.horasODT = horasODT;
	}
	public BigDecimal getIdCOT() {
		return idCOT;
	}
	public void setIdCOT(BigDecimal idCOT) {
		this.idCOT = idCOT;
	}
	public String getFechaCOT() {
		return fechaCOT;
	}
	public void setFechaCOT(String fechaCOT) {
		this.fechaCOT = fechaCOT;
	}
	public String getCodigoUsuarioCOT() {
		return codigoUsuarioCOT;
	}
	public void setCodigoUsuarioCOT(String codigoUsuarioCOT) {
		this.codigoUsuarioCOT = codigoUsuarioCOT;
	}
	public Integer getHorasCOT() {
		return horasCOT;
	}
	public void setHorasCOT(Integer horasCOT) {
		this.horasCOT = horasCOT;
	}
	public String getFechaEMI() {
		return fechaEMI;
	}
	public void setFechaEMI(String fechaEMI) {
		this.fechaEMI = fechaEMI;
	}
	public String getCodigoUsuarioEMI() {
		return codigoUsuarioEMI;
	}
	public void setCodigoUsuarioEMI(String codigoUsuarioEMI) {
		this.codigoUsuarioEMI = codigoUsuarioEMI;
	}
	public Integer getHorasEMI() {
		return horasEMI;
	}
	public void setHorasEMI(Integer horasEMI) {
		this.horasEMI = horasEMI;
	}
	public String getFechaTOT() {
		return fechaTOT;
	}
	public void setFechaTOT(String fechaTOT) {
		this.fechaTOT = fechaTOT;
	}
	public String getCodigoUsuarioTOT() {
		return codigoUsuarioTOT;
	}
	public void setCodigoUsuarioTOT(String codigoUsuarioTOT) {
		this.codigoUsuarioTOT = codigoUsuarioTOT;
	}
	public BigDecimal getIdPOL() {
		return idPOL;
	}
	public void setIdPOL(BigDecimal idPOL) {
		this.idPOL = idPOL;
	}
	public Integer getNumeroEndosoPOL() {
		return numeroEndosoPOL;
	}
	public void setNumeroEndosoPOL(Integer numeroEndosoPOL) {
		this.numeroEndosoPOL = numeroEndosoPOL;
	}
	public Integer getHorasTOT() {
		return horasTOT;
	}
	public void setHorasTOT(Integer horasTOT) {
		this.horasTOT = horasTOT;
	}
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	public BigDecimal getIdProductoSol() {
		return idProductoSol;
	}
	public void setIdProductoSol(BigDecimal idProductoSol) {
		this.idProductoSol = idProductoSol;
	}
	public void setNombreSolicitante(String nombreSolicitante) {
	    this.nombreSolicitante = nombreSolicitante;
	}
	public String getNombreSolicitante() {
	    return nombreSolicitante;
	}

}
