package mx.com.afirme.midas.danios.reportes.reportercs;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class ReporteRCSDTO implements Serializable {

	private static final long serialVersionUID = 2299753824789845932L;

	private Calendar fechaInicio;
	private Date fechaFin;
	private Integer idRamo;
	private Integer numeroCortes;
	private int cveNegocio;
	private String registro;
	
	
	public Calendar getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Calendar fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	public Integer getIdRamo() {
		return idRamo;
	}
	public void setIdRamo(Integer idRamo) {
		this.idRamo = idRamo;
	}
	public Integer getNumeroCortes() {
		return numeroCortes;
	}
	public void setNumeroCortes(Integer numeroCortes) {
		this.numeroCortes = numeroCortes;
	}
	public String getRegistro() {
		return registro;
	}
	public void setRegistro(String registro) {
		this.registro = registro;
	}
	public int getCveNegocio() {
		return cveNegocio;
	}
	public void setCveNegocio(int cveNegocio) {
		this.cveNegocio = cveNegocio;
	}
		
}
