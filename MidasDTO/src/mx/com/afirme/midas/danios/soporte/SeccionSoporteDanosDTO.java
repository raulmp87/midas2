/**
 * 
 */
package mx.com.afirme.midas.danios.soporte;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author ERIC
 *
 */
public class SeccionSoporteDanosDTO implements Serializable {

	private static final long serialVersionUID = 8771999933764349560L;
	private BigDecimal idToSeccion;
	private String descripcionSeccion;
	/**
	 * @return the idToSeccion
	 */
	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}
	/**
	 * @param idToSeccion the idToSeccion to set
	 */
	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}
	/**
	 * @return the descripcionSeccion
	 */
	public String getDescripcionSeccion() {
		return descripcionSeccion;
	}
	/**
	 * @param descripcionSeccion the descripcionSeccion to set
	 */
	public void setDescripcionSeccion(String descripcionSeccion) {
		this.descripcionSeccion = descripcionSeccion;
	}
	

}
