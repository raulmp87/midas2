package mx.com.afirme.midas.danios.soporte;

import java.math.BigDecimal;

public class CoberturaSoporteDanosDTO implements java.io.Serializable{

	private static final long serialVersionUID = 6776498232239928784L;
	private BigDecimal idToCobertura;
	private String descripcionCobertura;
	private short claveTipoSumaAsegurada;
	private String descripcionTipoSumaAsegurada;
	private BigDecimal idTcSubRamo;
	private String descripcionSubramo;
	private BigDecimal idTcRamo;
	private String descripcionRamo;
	private BigDecimal codigoRamo;
	private BigDecimal codigoSubRamo;
	
	
	/**
	 * @return the codigoRamo
	 */
	public BigDecimal getCodigoRamo() {
		return codigoRamo;
	}
	/**
	 * @param codigoRamo the codigoRamo to set
	 */
	public void setCodigoRamo(BigDecimal codigoRamo) {
		this.codigoRamo = codigoRamo;
	}
	/**
	 * @return the codigoSubRamo
	 */
	public BigDecimal getCodigoSubRamo() {
		return codigoSubRamo;
	}
	/**
	 * @param codigoSubRamo the codigoSubRamo to set
	 */
	public void setCodigoSubRamo(BigDecimal codigoSubRamo) {
		this.codigoSubRamo = codigoSubRamo;
	}
	/**
	 * @return the idToCobertura
	 */
	public BigDecimal getIdToCobertura() {
		return idToCobertura;
	}
	/**
	 * @param idToCobertura the idToCobertura to set
	 */
	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}
	/**
	 * @return the descripcionCobertura
	 */
	public String getDescripcionCobertura() {
		return descripcionCobertura;
	}
	/**
	 * @param descripcionCobertura the descripcionCobertura to set
	 */
	public void setDescripcionCobertura(String descripcionCobertura) {
		this.descripcionCobertura = descripcionCobertura;
	}
	/**
	 * @return the claveTipoSumaAsegurada
	 */
	public short getClaveTipoSumaAsegurada() {
		return claveTipoSumaAsegurada;
	}
	/**
	 * @param claveTipoSumaAsegurada the claveTipoSumaAsegurada to set
	 */
	public void setClaveTipoSumaAsegurada(short claveTipoSumaAsegurada) {
		this.claveTipoSumaAsegurada = claveTipoSumaAsegurada;
	}
	/**
	 * @return the descripcionTipoSumaAsegurada
	 */
	public String getDescripcionTipoSumaAsegurada() {
		return descripcionTipoSumaAsegurada;
	}
	/**
	 * @param descripcionTipoSumaAsegurada the descripcionTipoSumaAsegurada to set
	 */
	public void setDescripcionTipoSumaAsegurada(String descripcionTipoSumaAsegurada) {
		this.descripcionTipoSumaAsegurada = descripcionTipoSumaAsegurada;
	}
	/**
	 * @return the idTcSubRamo
	 */
	public BigDecimal getIdTcSubRamo() {
		return idTcSubRamo;
	}
	/**
	 * @param idTcSubRamo the idTcSubRamo to set
	 */
	public void setIdTcSubRamo(BigDecimal idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}
	/**
	 * @return the descripcionSubramo
	 */
	public String getDescripcionSubramo() {
		return descripcionSubramo;
	}
	/**
	 * @param descripcionSubramo the descripcionSubramo to set
	 */
	public void setDescripcionSubramo(String descripcionSubramo) {
		this.descripcionSubramo = descripcionSubramo;
	}
	/**
	 * @return the idTcRamo
	 */
	public BigDecimal getIdTcRamo() {
		return idTcRamo;
	}
	/**
	 * @param idTcRamo the idTcRamo to set
	 */
	public void setIdTcRamo(BigDecimal idTcRamo) {
		this.idTcRamo = idTcRamo;
	}
	
	/**
	 * @return the descripcionRamo
	 */
	public String getDescripcionRamo() {
		return descripcionRamo;
	}
	/**
	 * @param descripcionRamo the descripcionRamo to set
	 */
	public void setDescripcionRamo(String descripcionRamo) {
		this.descripcionRamo = descripcionRamo;
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		
		if(idToCobertura != null){
			sb.append("idToCobertura = " + idToCobertura.toString() + '\n');
		}else{
			sb.append("idToCobertura = null " + '\n');
		}
		if(descripcionCobertura != null){
			sb.append("descripcionCobertura = " + descripcionCobertura.toString() + '\n');
		}else{
			sb.append("descripcionCobertura = null " + '\n');
		}	
		
		sb.append("claveTipoSumaAsegurada = " +claveTipoSumaAsegurada + '\n' );

		if(descripcionTipoSumaAsegurada != null){
			sb.append("descripcionTipoSumaAsegurada = " + descripcionTipoSumaAsegurada + '\n' );
		}else{
			sb.append("descripcionTipoSumaAsegurada = null " + '\n' );
		}		
		if(idTcSubRamo != null){
			sb.append("idTcSubRamo = " + idTcSubRamo.toString() + '\n');
		}else{
			sb.append("idTcSubRamo = null "  + '\n');
		}		
		if(descripcionSubramo != null){
			sb.append("descripcionSubramo = " + descripcionSubramo + '\n' );
		}else{
			sb.append("descripcionSubramo = null " + '\n' );
		}		
		if(idTcRamo != null){
			sb.append("idTcRamo = " + idTcRamo.toString() + '\n');
		}else{
			sb.append("idTcRamo = null " + '\n');
		}
		if(descripcionRamo != null){
			sb.append("descripcionRamo = " + descripcionRamo + '\n');
		}else{
			sb.append("descripcionRamo = null " + '\n');
		}
		if(codigoRamo != null){
			sb.append("codigoRamo = " + codigoRamo.toString() + '\n');
		}else{
			sb.append("codigoRamo = null " + '\n');
		}											
		if(codigoSubRamo != null){
			sb.append("codigoSubRamo = " + codigoSubRamo.toString() + '\n');
		}else{
			sb.append("codigoSubRamo = null " + '\n');
		}						
		
		return sb.toString();		
	}	
}
