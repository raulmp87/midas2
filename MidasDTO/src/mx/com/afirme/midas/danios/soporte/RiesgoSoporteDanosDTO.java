package mx.com.afirme.midas.danios.soporte;

import java.io.Serializable;
import java.math.BigDecimal;

public class RiesgoSoporteDanosDTO implements Serializable {

	private static final long serialVersionUID = 7784523966280054640L;
	private BigDecimal idToRiesgo;
	private String descripcionRiesgo;
	/**
	 * @return the idToRiesgo
	 */
	public BigDecimal getIdToRiesgo() {
		return idToRiesgo;
	}
	/**
	 * @param idToRiesgo the idToRiesgo to set
	 */
	public void setIdToRiesgo(BigDecimal idToRiesgo) {
		this.idToRiesgo = idToRiesgo;
	}
	/**
	 * @return the descripcionRiesgo
	 */
	public String getDescripcionRiesgo() {
		return descripcionRiesgo;
	}
	/**
	 * @param descripcionRiesgo the descripcionRiesgo to set
	 */
	public void setDescripcionRiesgo(String descripcionRiesgo) {
		this.descripcionRiesgo = descripcionRiesgo;
	}


}
