package mx.com.afirme.midas.danios.soporte;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PolizaSoporteDanosDTO implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	private BigDecimal idToPoliza;
	private String numeroPoliza;
	
	private BigDecimal idAsegurado;
	private String nombreAsegurado;
	private String calleAsegurado;
	private String cpAsegurado;
	private BigDecimal idEstadoAsegurado;
	private BigDecimal idMunicipioAsegurado;
	private BigDecimal idColoniaAsegurado;
	private String telefonoAsegurado;
	
	private BigDecimal idContratante;
	private String nombreContratante;
	private String calleContratante;
	private String cpContratante;
	private BigDecimal idEstadoContratante;
	private BigDecimal idMunicipioContratante;
	private BigDecimal idColoniaContratante;
	private String telefonoContratante;
	
	private Date fechaEmision;
	private int codigoProducto;
	private String nombreComercialProducto;
	private BigDecimal idMoneda;
	private String descripcionMoneda;
	private Date fechaPago;
	private String nombreOficina;
	private double importePagado;
	private double saldoPendiente;
	private BigDecimal idFormaPago;
	private String descripcionFormaPago;
	private double saldoVencido;
	private	String ultimoReciboPagado;
	private	Date fechaInicioVigencia;
	private Date fechaFinVigencia;
	private BigDecimal idTcTipoNegocio;
	private BigDecimal codigoTipoNegocio;
	private boolean productoTransporte;
	private String descripcionTipoPoliza;
	private List<DetallePolizaSoporteDanosSiniestrosDTO> listaDetallesPolizaSiniestros = new ArrayList<DetallePolizaSoporteDanosSiniestrosDTO>();
	private int numeroUltimoEndoso;
	private BigDecimal codigoAgente;
	private String nombreAgente;
	private Short numeroEndosoVigente;

	public PolizaSoporteDanosDTO(){
	}

	/**
	 * @return the idToPoliza
	 */
	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}

	/**
	 * @param idToPoliza the idToPoliza to set
	 */
	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	/**
	 * @return the numeroPoliza
	 */
	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	/**
	 * @param numeroPoliza the numeroPoliza to set
	 */
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	/**
	 * @return the nombreAsegurado
	 */
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}

	/**
	 * @param nombreAsegurado the nombreAsegurado to set
	 */
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}

	/**
	 * @return the fechaEmision
	 */
	public Date getFechaEmision() {
		return fechaEmision;
	}

	/**
	 * @param fechaEmision the fechaEmision to set
	 */
	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	/**
	 * @return the codigoProducto
	 */
	public int getCodigoProducto() {
		return codigoProducto;
	}

	/**
	 * @param codigoProducto the codigoProducto to set
	 */
	public void setCodigoProducto(int codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	/**
	 * @return the calleAsegurado
	 */
	public String getCalleAsegurado() {
		return calleAsegurado;
	}

	/**
	 * @param calleAsegurado the calleAsegurado to set
	 */
	public void setCalleAsegurado(String calleAsegurado) {
		this.calleAsegurado = calleAsegurado;
	}

	/**
	 * @return the cpAsegurado
	 */
	public String getCpAsegurado() {
		return cpAsegurado;
	}

	/**
	 * @param cpAsegurado the cpAsegurado to set
	 */
	public void setCpAsegurado(String cpAsegurado) {
		this.cpAsegurado = cpAsegurado;
	}

	/**
	 * @return the idEstadoAsegurado
	 */
	public BigDecimal getIdEstadoAsegurado() {
		return idEstadoAsegurado;
	}

	/**
	 * @param idEstadoAsegurado the idEstadoAsegurado to set
	 */
	public void setIdEstadoAsegurado(BigDecimal idEstadoAsegurado) {
		this.idEstadoAsegurado = idEstadoAsegurado;
	}

	/**
	 * @return the idMunicipioAsegurado
	 */
	public BigDecimal getIdMunicipioAsegurado() {
		return idMunicipioAsegurado;
	}

	/**
	 * @param idMunicipioAsegurado the idMunicipioAsegurado to set
	 */
	public void setIdMunicipioAsegurado(BigDecimal idMunicipioAsegurado) {
		this.idMunicipioAsegurado = idMunicipioAsegurado;
	}

	/**
	 * @return the idColoniaAsegurado
	 */
	public BigDecimal getIdColoniaAsegurado() {
		return idColoniaAsegurado;
	}

	/**
	 * @param idColoniaAsegurado the idColoniaAsegurado to set
	 */
	public void setIdColoniaAsegurado(BigDecimal idColoniaAsegurado) {
		this.idColoniaAsegurado = idColoniaAsegurado;
	}

	/**
	 * @return the idMoneda
	 */
	public BigDecimal getIdMoneda() {
		return idMoneda;
	}

	/**
	 * @param idMoneda the idMoneda to set
	 */
	public void setIdMoneda(BigDecimal idMoneda) {
		this.idMoneda = idMoneda;
	}

	/**
	 * @return the descripcionMoneda
	 */
	public String getDescripcionMoneda() {
		return descripcionMoneda;
	}

	/**
	 * @param descripcionMoneda the descripcionMoneda to set
	 */
	public void setDescripcionMoneda(String descripcionMoneda) {
		this.descripcionMoneda = descripcionMoneda;
	}

	/**
	 * @return the fechaPago
	 */
	public Date getFechaPago() {
		return fechaPago;
	}

	/**
	 * @param fechaPago the fechaPago to set
	 */
	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	/**
	 * @return the nombreOficina
	 */
	public String getNombreOficina() {
		return nombreOficina;
	}

	/**
	 * @param nombreOficina the nombreOficina to set
	 */
	public void setNombreOficina(String nombreOficina) {
		this.nombreOficina = nombreOficina;
	}

	/**
	 * @return the importePagado
	 */
	public double getImportePagado() {
		return importePagado;
	}

	/**
	 * @param importePagado the importePagado to set
	 */
	public void setImportePagado(double importePagado) {
		this.importePagado = importePagado;
	}

	/**
	 * @return the saldoPendiente
	 */
	public double getSaldoPendiente() {
		return saldoPendiente;
	}

	/**
	 * @param saldoPendiente the saldoPendiente to set
	 */
	public void setSaldoPendiente(double saldoPendiente) {
		this.saldoPendiente = saldoPendiente;
	}

	/**
	 * @return the idFormaPago
	 */
	public BigDecimal getIdFormaPago() {
		return idFormaPago;
	}

	/**
	 * @param idFormaPago the idFormaPago to set
	 */
	public void setIdFormaPago(BigDecimal idFormaPago) {
		this.idFormaPago = idFormaPago;
	}

	/**
	 * @return the descripcionFormaPago
	 */
	public String getDescripcionFormaPago() {
		return descripcionFormaPago;
	}

	/**
	 * @param descripcionFormaPago the descripcionFormaPago to set
	 */
	public void setDescripcionFormaPago(String descripcionFormaPago) {
		this.descripcionFormaPago = descripcionFormaPago;
	}

	/**
	 * @return the saldoVencido
	 */
	public double getSaldoVencido() {
		return saldoVencido;
	}

	/**
	 * @param saldoVencido the saldoVencido to set
	 */
	public void setSaldoVencido(double saldoVencido) {
		this.saldoVencido = saldoVencido;
	}

	/**
	 * @return the ultimoReciboPagado
	 */
	public String getUltimoReciboPagado() {
		return ultimoReciboPagado;
	}

	/**
	 * @param ultimoReciboPagado the ultimoReciboPagado to set
	 */
	public void setUltimoReciboPagado(String ultimoReciboPagado) {
		this.ultimoReciboPagado = ultimoReciboPagado;
	}

	/**
	 * @return the fechaInicioVigencia
	 */
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	/**
	 * @param fechaInicioVigencia the fechaInicioVigencia to set
	 */
	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	/**
	 * @return the fechaFinVigencia
	 */
	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}

	/**
	 * @param fechaFinVigencia the fechaFinVigencia to set
	 */
	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	/**
	 * @return the idTcTipoNegocio
	 */
	public BigDecimal getIdTcTipoNegocio() {
		return idTcTipoNegocio;
	}

	/**
	 * @param idTcTipoNegocio the idTcTipoNegocio to set
	 */
	public void setIdTcTipoNegocio(BigDecimal idTcTipoNegocio) {
		this.idTcTipoNegocio = idTcTipoNegocio;
	}

	/**
	 * @return the codigoTipoNegocio
	 */
	public BigDecimal getCodigoTipoNegocio() {
		return codigoTipoNegocio;
	}

	/**
	 * @param codigoTipoNegocio the codigoTipoNegocio to set
	 */
	public void setCodigoTipoNegocio(BigDecimal codigoTipoNegocio) {
		this.codigoTipoNegocio = codigoTipoNegocio;
	}

	/**
	 * @return the productoTransporte
	 */
	public boolean isProductoTransporte() {
		return productoTransporte;
	}

	/**
	 * @param productoTransporte the productoTransporte to set
	 */
	public void setProductoTransporte(boolean productoTransporte) {
		this.productoTransporte = productoTransporte;
	}

	/**
	 * @return the descripcionSubRamo
	 */
	public String getDescripcionTipoPoliza() {
		return descripcionTipoPoliza;
	}

	/**
	 * @param descripcionSubRamo the descripcionSubRamo to set
	 */
	public void setDescripcionTipoPoliza(String descripcionTipoPoliza) {
		this.descripcionTipoPoliza = descripcionTipoPoliza;
	}

	/**
	 * @return the listaDetallesPolizaSiniestros
	 */
	public List<DetallePolizaSoporteDanosSiniestrosDTO> getListaDetallesPolizaSiniestros() {
		return listaDetallesPolizaSiniestros;
	}

	/**
	 * @param listaDetallesPolizaSiniestros the listaDetallesPolizaSiniestros to set
	 */
	public void setListaDetallesPolizaSiniestros(
			List<DetallePolizaSoporteDanosSiniestrosDTO> listaDetallesPolizaSiniestros) {
		this.listaDetallesPolizaSiniestros = listaDetallesPolizaSiniestros;
	}

	public void setNumeroUltimoEndoso(int numeroUltimoEndoso) {
		this.numeroUltimoEndoso = numeroUltimoEndoso;
	}

	public int getNumeroUltimoEndoso() {
		return numeroUltimoEndoso;
	}

	public void setCodigoAgente(BigDecimal codigoAgente) {
		this.codigoAgente = codigoAgente;
	}

	public BigDecimal getCodigoAgente() {
		return codigoAgente;
	}

	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}

	public String getNombreAgente() {
		return nombreAgente;
	}

	public void setIdAsegurado(BigDecimal idAsegurado) {
		this.idAsegurado = idAsegurado;
	}

	public BigDecimal getIdAsegurado() {
		return idAsegurado;
	}

	public void setTelefonoAsegurado(String telefonoAsegurado) {
	    this.telefonoAsegurado = telefonoAsegurado;
	}

	public String getTelefonoAsegurado() {
	    return telefonoAsegurado;
	}

	public void setIdContratante(BigDecimal idContratante) {
	    this.idContratante = idContratante;
	}

	public BigDecimal getIdContratante() {
	    return idContratante;
	}

	public void setNombreContratante(String nombreContratante) {
	    this.nombreContratante = nombreContratante;
	}

	public String getNombreContratante() {
	    return nombreContratante;
	}

	public void setCalleContratante(String calleContratante) {
	    this.calleContratante = calleContratante;
	}

	public String getCalleContratante() {
	    return calleContratante;
	}

	public void setCpContratante(String cpContratante) {
	    this.cpContratante = cpContratante;
	}

	public String getCpContratante() {
	    return cpContratante;
	}

	public void setIdEstadoContratante(BigDecimal idEstadoContratante) {
	    this.idEstadoContratante = idEstadoContratante;
	}

	public BigDecimal getIdEstadoContratante() {
	    return idEstadoContratante;
	}

	public void setIdMunicipioContratante(BigDecimal idMunicipioContratante) {
	    this.idMunicipioContratante = idMunicipioContratante;
	}

	public BigDecimal getIdMunicipioContratante() {
	    return idMunicipioContratante;
	}

	public void setIdColoniaContratante(BigDecimal idColoniaContratante) {
	    this.idColoniaContratante = idColoniaContratante;
	}

	public BigDecimal getIdColoniaContratante() {
	    return idColoniaContratante;
	}

	public void setTelefonoContratante(String telefonoContratante) {
	    this.telefonoContratante = telefonoContratante;
	}

	public String getTelefonoContratante() {
	    return telefonoContratante;
	}

	public void setNombreComercialProducto(String nombreComercialProducto) {
	    this.nombreComercialProducto = nombreComercialProducto;
	}

	public String getNombreComercialProducto() {
	    return nombreComercialProducto;
	}

	public void setNumeroEndosoVigente(Short numeroEndosoVigente) {
		this.numeroEndosoVigente = numeroEndosoVigente;
	}

	public Short getNumeroEndosoVigente() {
		return numeroEndosoVigente;
	}
}
