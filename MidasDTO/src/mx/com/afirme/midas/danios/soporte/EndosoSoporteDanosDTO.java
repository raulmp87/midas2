package mx.com.afirme.midas.danios.soporte;

import java.util.Date;

public class EndosoSoporteDanosDTO implements java.io.Serializable{

	private static final long serialVersionUID = 2L;
	private String numeroPoliza;
	private int numeroEndoso;
	private String nombreAsegurado;	
	
	/** Inicio de vigencia para este endoso */
	private Date fechaInicioVigencia;
	/** Fin de vigencia para este endoso */
	private Date fechaFinVigencia;
	
	/**
	 * @return the fechaInicioVigencia
	 */
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	/**
	 * @param fechaInicioVigencia the fechaInicioVigencia to set
	 */
	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	/**
	 * @return the fechaFinVigencia
	 */
	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}

	/**
	 * @param fechaFinVigencia the fechaFinVigencia to set
	 */
	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	/**
	 * @return the numeroPoliza
	 */
	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	/**
	 * @param numeroPoliza the numeroPoliza to set
	 */
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	/**
	 * @return the numeroEndoso
	 */
	public int getNumeroEndoso() {
		return numeroEndoso;
	}

	/**
	 * @param numeroEndoso the numeroEndoso to set
	 */
	public void setNumeroEndoso(int numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}

	/**
	 * @return the nombreAsegurado
	 */
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}

	/**
	 * @param nombreAsegurado the nombreAsegurado to set
	 */
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}
}