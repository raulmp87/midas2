package mx.com.afirme.midas.danios.soporte;

import java.math.BigDecimal;

public class DetallePolizaSoporteDanosSiniestrosDTO implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	private	BigDecimal numeroInciso;	 
	private	BigDecimal idToSeccion;
	private	BigDecimal numeroSubInciso;
	private	BigDecimal idToCobertura;
	private	BigDecimal idToRiesgo;
	private	String descripcionSeccion;
	private	String descripcionSubInciso;
	private	String descripcionCobertura;
	private	String descripcionRiesgo;
	private	Double sumaAsegurada;
	private	int claveTipoSumaAsegurada;
	private	int claveTipoAgrupacion;
	private Double primaNeta;
	private Double deducible;
	private Double coaseguro;

	public DetallePolizaSoporteDanosSiniestrosDTO(){
	}

	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	public BigDecimal getNumeroSubInciso() {
		return numeroSubInciso;
	}

	public void setNumeroSubInciso(BigDecimal numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}

	public BigDecimal getIdToCobertura() {
		return idToCobertura;
	}

	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	public BigDecimal getIdToRiesgo() {
		return idToRiesgo;
	}

	public void setIdToRiesgo(BigDecimal idToRiesgo) {
		this.idToRiesgo = idToRiesgo;
	}

	public String getDescripcionSeccion() {
		return descripcionSeccion;
	}

	public void setDescripcionSeccion(String descripcionSeccion) {
		this.descripcionSeccion = descripcionSeccion;
	}

	public String getDescripcionSubInciso() {
		return descripcionSubInciso;
	}

	public void setDescripcionSubInciso(String descripcionSubInciso) {
		this.descripcionSubInciso = descripcionSubInciso;
	}

	public String getDescripcionCobertura() {
		return descripcionCobertura;
	}

	public void setDescripcionCobertura(String descripcionCobertura) {
		this.descripcionCobertura = descripcionCobertura;
	}

	public String getDescripcionRiesgo() {
		return descripcionRiesgo;
	}

	public void setDescripcionRiesgo(String descripcionRiesgo) {
		this.descripcionRiesgo = descripcionRiesgo;
	}

	public Double getSumaAsegurada() {
		return sumaAsegurada;
	}

	public void setSumaAsegurada(Double sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}

	public int getClaveTipoSumaAsegurada() {
		return claveTipoSumaAsegurada;
	}

	public void setClaveTipoSumaAsegurada(int claveTipoSumaAsegurada) {
		this.claveTipoSumaAsegurada = claveTipoSumaAsegurada;
	}

	public int getClaveTipoAgrupacion() {
		return claveTipoAgrupacion;
	}

	public void setClaveTipoAgrupacion(int claveTipoAgrupacion) {
		this.claveTipoAgrupacion = claveTipoAgrupacion;
	}

	public void setPrimaNeta(Double primaNeta) {
	    this.primaNeta = primaNeta;
	}

	public Double getPrimaNeta() {
	    return primaNeta;
	}

	public void setDeducible(Double deducible) {
	    this.deducible = deducible;
	}

	public Double getDeducible() {
	    return deducible;
	}

	public void setCoaseguro(Double coaseguro) {
	    this.coaseguro = coaseguro;
	}

	public Double getCoaseguro() {
	    return coaseguro;
	}

	
}
