package mx.com.afirme.midas.danios;
// default package

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.cotizacion.reaseguro.ReaseguroCotizacionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;

/**
 * ReaseguroDetalleCoberturaCotizacionDTO entity. @author MyEclipse Persistence
 * Tools
 */
@Entity
@Table(name = "TOREASEGURODETCOBCOT", schema = "MIDAS")
public class ReaseguroDetalleCoberturaCotizacionDTO implements
		java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 7489059445676693212L;
	private ReaseguroDetalleCoberturaCotizacionId id;
	private ReaseguroCotizacionDTO reaseguroCotizacion;
	private CoberturaDTO cobertura;
	private Double valorSumaAsegurada;
	private Double valorPrimaNeta;
	private Double valorPrimaNetaContratos;
	private Double valorPrimaNetaFacultada;

	// Constructors

	/** default constructor */
	public ReaseguroDetalleCoberturaCotizacionDTO() {
		reaseguroCotizacion = new ReaseguroCotizacionDTO();
		cobertura = new CoberturaDTO();
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToCotizacion", column = @Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idTcSubRamo", column = @Column(name = "IDTCSUBRAMO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToCobertura", column = @Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)) })
	public ReaseguroDetalleCoberturaCotizacionId getId() {
		return this.id;
	}

	public void setId(ReaseguroDetalleCoberturaCotizacionId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumns( {
			@JoinColumn(name = "IDTOCOTIZACION", referencedColumnName = "IDTOCOTIZACION", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTCSUBRAMO", referencedColumnName = "IDTCSUBRAMO", nullable = false, insertable = false, updatable = false) })
	public ReaseguroCotizacionDTO getReaseguroCotizacion() {
		return reaseguroCotizacion;
	}

	public void setReaseguroCotizacion(ReaseguroCotizacionDTO reaseguroCotizacion) {
		this.reaseguroCotizacion = reaseguroCotizacion;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOCOBERTURA", nullable = false, insertable = false, updatable = false)
	public CoberturaDTO getCobertura() {
		return cobertura;
	}

	public void setCobertura(CoberturaDTO cobertura) {
		this.cobertura = cobertura;
	}

	@Column(name = "VALORSUMAASEGURADA", nullable = false, precision = 16)
	public Double getValorSumaAsegurada() {
		return this.valorSumaAsegurada;
	}

	public void setValorSumaAsegurada(Double valorSumaAsegurada) {
		this.valorSumaAsegurada = valorSumaAsegurada;
	}

	@Column(name = "VALORPRIMANETA", nullable = false, precision = 16)
	public Double getValorPrimaNeta() {
		return this.valorPrimaNeta;
	}

	public void setValorPrimaNeta(Double valorPrimaNeta) {
		this.valorPrimaNeta = valorPrimaNeta;
	}

	@Column(name = "VALORPRIMANETACONTRATOS", nullable = false, precision = 16)
	public Double getValorPrimaNetaContratos() {
		return valorPrimaNetaContratos;
	}

	public void setValorPrimaNetaContratos(Double valorPrimaNetaContratos) {
		this.valorPrimaNetaContratos = valorPrimaNetaContratos;
	}

	@Column(name = "VALORPRIMANETAFACULTADA", nullable = false, precision = 16)
	public Double getValorPrimaNetaFacultada() {
		return valorPrimaNetaFacultada;
	}

	public void setValorPrimaNetaFacultada(Double valorPrimaNetaFacultada) {
		this.valorPrimaNetaFacultada = valorPrimaNetaFacultada;
	}
}