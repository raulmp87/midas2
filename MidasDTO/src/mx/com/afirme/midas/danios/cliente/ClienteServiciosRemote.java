package mx.com.afirme.midas.danios.cliente;

import java.math.BigDecimal;

import javax.ejb.Remote;

import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;


public interface ClienteServiciosRemote {

	public ClienteDTO findById(BigDecimal idCliente, String nombreUsuario) throws Exception;
	
	public BigDecimal save(ClienteDTO entity, String nombreUsuario) throws Exception;
	
}
