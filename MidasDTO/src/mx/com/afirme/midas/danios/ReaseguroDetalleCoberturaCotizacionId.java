package mx.com.afirme.midas.danios;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * ReaseguroDetalleCoberturaCotizacionDTOId entity. @author MyEclipse
 * Persistence Tools
 */
@Embeddable
public class ReaseguroDetalleCoberturaCotizacionId implements
		java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -6111138411010037456L;
	private BigDecimal idToCotizacion;
	private BigDecimal idTcSubRamo;
	private BigDecimal idToCobertura;

	// Constructors

	/** default constructor */
	public ReaseguroDetalleCoberturaCotizacionId() {
	}

	/** full constructor */
	public ReaseguroDetalleCoberturaCotizacionId(BigDecimal idToCotizacion,
			BigDecimal idTcSubRamo, BigDecimal idToCobertura) {
		this.idToCotizacion = idToCotizacion;
		this.idTcSubRamo = idTcSubRamo;
		this.idToCobertura = idToCobertura;
	}

	// Property accessors

	@Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCotizacion() {
		return this.idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	@Column(name = "IDTCSUBRAMO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcSubRamo() {
		return this.idTcSubRamo;
	}

	public void setIdTcSubRamo(BigDecimal idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}

	@Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCobertura() {
		return this.idToCobertura;
	}

	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ReaseguroDetalleCoberturaCotizacionId))
			return false;
		ReaseguroDetalleCoberturaCotizacionId castOther = (ReaseguroDetalleCoberturaCotizacionId) other;

		return ((this.getIdToCotizacion() == castOther.getIdToCotizacion()) || (this
				.getIdToCotizacion() != null
				&& castOther.getIdToCotizacion() != null && this
				.getIdToCotizacion().equals(castOther.getIdToCotizacion())))
				&& ((this.getIdTcSubRamo() == castOther.getIdTcSubRamo()) || (this
						.getIdTcSubRamo() != null
						&& castOther.getIdTcSubRamo() != null && this
						.getIdTcSubRamo().equals(castOther.getIdTcSubRamo())))
				&& ((this.getIdToCobertura() == castOther.getIdToCobertura()) || (this
						.getIdToCobertura() != null
						&& castOther.getIdToCobertura() != null && this
						.getIdToCobertura()
						.equals(castOther.getIdToCobertura())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToCotizacion() == null ? 0 : this.getIdToCotizacion()
						.hashCode());
		result = 37
				* result
				+ (getIdTcSubRamo() == null ? 0 : this.getIdTcSubRamo()
						.hashCode());
		result = 37
				* result
				+ (getIdToCobertura() == null ? 0 : this.getIdToCobertura()
						.hashCode());
		return result;
	}

}