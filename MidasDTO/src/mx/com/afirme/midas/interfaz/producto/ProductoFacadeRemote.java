/**
 * 
 */
package mx.com.afirme.midas.interfaz.producto;

import javax.ejb.Remote;

import mx.com.afirme.midas.producto.ProductoDTO;

/**
 * @author andres.avalos
 *
 */

public interface ProductoFacadeRemote {

	public void save(ProductoDTO entity, String nombreUsuario) throws Exception;
	
}
