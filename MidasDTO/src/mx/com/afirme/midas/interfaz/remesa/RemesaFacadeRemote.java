package mx.com.afirme.midas.interfaz.remesa;

import java.math.BigDecimal;

import javax.ejb.Remote;


public interface RemesaFacadeRemote {

	
	public BigDecimal registraRemesa (BigDecimal idRemesa, String nombreUsuario) throws Exception;
	
}
