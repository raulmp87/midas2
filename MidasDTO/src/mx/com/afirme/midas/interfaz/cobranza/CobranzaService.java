package mx.com.afirme.midas.interfaz.cobranza;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.dto.MensajeDTO;


public interface CobranzaService {
	public List<CoberturaCobranzaDTO> consultaCoberturasConSaldoPendiente(
			BigDecimal polizaId);

	public List<CoberturaCobranzaDTO> consultaCoberturasConSaldoPendiente(
			BigDecimal polizaId, BigDecimal lineaId);

	public List<CoberturaCobranzaDTO> consultaCoberturasConSaldoPendiente(
			BigDecimal polizaId, BigDecimal lineaId, BigDecimal numeroInciso);

	public List<CoberturaCobranzaDTO> consultaCoberturasSaldoTotal(
			BigDecimal polizaId);

	public List<CoberturaCobranzaDTO> consultaCoberturasSaldoTotal(
			BigDecimal polizaId, BigDecimal lineaId);

	public List<CoberturaCobranzaDTO> consultaCoberturasSaldoTotal(
			BigDecimal polizaId, BigDecimal lineaId, BigDecimal numeroInciso);

	public Double consultaSaldoPendiente(BigDecimal polizaId);

	public Double consultaSaldoTotal(BigDecimal polizaId);
	
	public MensajeDTO stpValidaPrimerRecNoPagado(BigDecimal polizaId, Date fechaProceso);

}
