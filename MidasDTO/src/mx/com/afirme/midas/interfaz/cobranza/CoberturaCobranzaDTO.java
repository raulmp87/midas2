package mx.com.afirme.midas.interfaz.cobranza;

import java.io.Serializable;
import java.math.BigDecimal;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

public class CoberturaCobranzaDTO implements Serializable, Entidad {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String TOTAL="T";
	public static final String SALDO="S";
	
	private BigDecimal polizaId;
	private BigDecimal coberturaId;
	private BigDecimal lineaNegocioId;
	private BigDecimal numeroInciso;

	private Double valorPN;
	private Double valorBonificacionComisionPN;
	private Double valorBonificacionComisionRPF;
	private Double valorBonificacionComision;
	private Double valorDerechos;
	private Integer pctRecargo;
	private Double valorRecargo;
	private Double valorIVA;
	private Double valorPrimaTotal;
	private Double valorComisionPNAgente;
	private Double valorComisionRPFAgente;
	private Double valorComisionAgente;

	public BigDecimal getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(BigDecimal polizaId) {
		this.polizaId = polizaId;
	}

	public BigDecimal getCoberturaId() {
		return coberturaId;
	}

	public void setCoberturaId(BigDecimal coberturaId) {
		this.coberturaId = coberturaId;
	}

	public BigDecimal getLineaNegocioId() {
		return lineaNegocioId;
	}

	public void setLineaNegocioId(BigDecimal lineaNegocioId) {
		this.lineaNegocioId = lineaNegocioId;
	}

	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public Double getValorPN() {
		return valorPN;
	}

	public void setValorPN(Double valorPN) {
		this.valorPN = valorPN;
	}

	public Double getValorBonificacionComisionPN() {
		return valorBonificacionComisionPN;
	}

	public void setValorBonificacionComisionPN(
			Double valorBonificacionComisionPN) {
		this.valorBonificacionComisionPN = valorBonificacionComisionPN;
	}

	public Double getValorBonificacionComisionRPF() {
		return valorBonificacionComisionRPF;
	}

	public void setValorBonificacionComisionRPF(
			Double valorBonificacionComisionRPF) {
		this.valorBonificacionComisionRPF = valorBonificacionComisionRPF;
	}

	public Double getValorBonificacionComision() {
		return valorBonificacionComision;
	}

	public void setValorBonificacionComision(Double valorBonificacionComision) {
		this.valorBonificacionComision = valorBonificacionComision;
	}

	public Double getValorDerechos() {
		return valorDerechos;
	}

	public void setValorDerechos(Double valorDerechos) {
		this.valorDerechos = valorDerechos;
	}

	public Integer getPctRecargo() {
		return pctRecargo;
	}

	public void setPctRecargo(Integer pctRecargo) {
		this.pctRecargo = pctRecargo;
	}

	public Double getValorRecargo() {
		return valorRecargo;
	}

	public void setValorRecargo(Double valorRecargo) {
		this.valorRecargo = valorRecargo;
	}

	public Double getValorIVA() {
		return valorIVA;
	}

	public void setValorIVA(Double valorIVA) {
		this.valorIVA = valorIVA;
	}

	public Double getValorPrimaTotal() {
		return valorPrimaTotal;
	}

	public void setValorPrimaTotal(Double valorPrimaTotal) {
		this.valorPrimaTotal = valorPrimaTotal;
	}

	public Double getValorComisionPNAgente() {
		return valorComisionPNAgente;
	}

	public void setValorComisionPNAgente(Double valorComisionPNAgente) {
		this.valorComisionPNAgente = valorComisionPNAgente;
	}

	public Double getValorComisionRPFAgente() {
		return valorComisionRPFAgente;
	}

	public void setValorComisionRPFAgente(Double valorComisionRPFAgente) {
		this.valorComisionRPFAgente = valorComisionRPFAgente;
	}

	public Double getValorComisionAgente() {
		return valorComisionAgente;
	}

	public void setValorComisionAgente(Double valorComisionAgente) {
		this.valorComisionAgente = valorComisionAgente;
	}

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
}
