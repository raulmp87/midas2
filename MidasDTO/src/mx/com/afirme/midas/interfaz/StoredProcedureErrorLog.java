/**
 * Clase para escribir en un log de Base de Datos las excepciones generadas por los Stored Procedures
 */
package mx.com.afirme.midas.interfaz;

import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.commons.dbutils.DbUtils;

/**
 * @author andres.avalos
 *
 */
public class StoredProcedureErrorLog {

	public static enum TipoAccion {GUARDAR, BORRAR, MODIFICAR, BUSCAR};
	
	/**
	 * Inserta un registro de error de Stored Procedure en la Base de Datos MIDAS
	 * @param nombreUsuario Nombre del usuario logueado que realizo la accion
	 * @param tipoAccion tipo de accion realizada (GUARDAR, BORRAR, MODIFICAR, BUSCAR)
	 * @param nombreStoredProcedure Nombre del Stored Procedure invocado
	 * @param objetoTransporte Objeto del cual se obtienen los par�metros de entrada del Stored Procedure
	 * @param codigoRespuesta Codigo de error del Stored Procedure (Si es manejado)
	 * @param descripcionRespuesta Descripcion de error del Stored Procedure (Si es manejado)
	 */
	@SuppressWarnings("unused")
	public static void doLog(String nombreUsuario, TipoAccion tipoAccion, String nombreStoredProcedure, Object objetoTransporte, 
			Integer codigoRespuesta, String descripcionRespuesta) {
		Connection con = null;
		PreparedStatement  ps = null;
		try {
			Context ctx = new InitialContext();
	        if(ctx == null )
				
					throw new Exception("No Contexto");
				
	        DataSource ds = 
	              (DataSource)ctx.lookup(
	                 "jdbc/MidasDataSource");

	        if (ds != null) {
	          con = ds.getConnection();
	        }
	        
	        String sqlQuery = "INSERT INTO MIDAS.TOINTERFAZERRORLOG (IDTOINTERFAZERRORLOG, FECHA, USUARIO, ACCION, NOMBREOBJETO, POPIEDADESOBJETO, ";
	        sqlQuery += "NOMBRESTOREDPROC) VALUES (MIDAS.idtoInterfazErrorLog_seq.nextval,?,?,?,?,?,?)";
	        
	        ps = con.prepareStatement(sqlQuery);
	        ps.setTimestamp(1, new java.sql.Timestamp(new java.util.Date().getTime()));
	        ps.setString(2, nombreUsuario);
	        ps.setString(3, tipoAccion.toString());
	        ps.setString(4, objetoTransporte.getClass().getName());
	        ps.setString(5, objetoTransporte.toString() + " Cod Err: " + codigoRespuesta + " Desc Err: " + descripcionRespuesta);
	        ps.setString(6, nombreStoredProcedure);
	        ps.execute();    
	        
	        
		} catch (Exception e) {
			e.printStackTrace();
	    } finally{
	    	DbUtils.closeQuietly(con);
	    	DbUtils.closeQuietly(ps);
	    }
	
	}
	 
	@SuppressWarnings("unused")
	public static void doLog2(String nombreUsuario, TipoAccion tipoAccion, String nombreStoredProcedure, String descripcionRespuesta) {
		Connection con = null;
		PreparedStatement  ps = null;
		try {
			Context ctx = new InitialContext();
	        if(ctx == null )
				
					throw new Exception("No Contexto");
				
	        DataSource ds = 
	              (DataSource)ctx.lookup(
	                 "jdbc/MidasDataSource");

	        if (ds != null) {
	          con = ds.getConnection();
	        }
	        
	        String sqlQuery = "INSERT INTO MIDAS.TOINTERFAZERRORLOG (IDTOINTERFAZERRORLOG, FECHA, USUARIO, ACCION, NOMBREOBJETO, POPIEDADESOBJETO, ";
	        sqlQuery += "NOMBRESTOREDPROC) VALUES (MIDAS.idtoInterfazErrorLog_seq.nextval,?,?,?,?,?,?)";
	        
	        ps = con.prepareStatement(sqlQuery);
	        ps.setTimestamp(1, new java.sql.Timestamp(new java.util.Date().getTime()));
	        ps.setString(2, nombreUsuario);
	        ps.setString(3, tipoAccion.toString());
	        ps.setString(4, "LogdeJob");
	        ps.setString(5,descripcionRespuesta);
	        ps.setString(6, nombreStoredProcedure);
	        ps.execute();    
	        
	        
		} catch (Exception e) {
			e.printStackTrace();
	    } finally{
	    	DbUtils.closeQuietly(con);
	    	DbUtils.closeQuietly(ps);
	    }
	
	}	
	
	
}
