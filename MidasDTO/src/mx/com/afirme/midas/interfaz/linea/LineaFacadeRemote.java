/**
 * 
 */
package mx.com.afirme.midas.interfaz.linea;

import javax.ejb.Remote;

import mx.com.afirme.midas.contratos.linea.LineaDTO;

/**
 * @author andres.avalos
 *
 */

public interface LineaFacadeRemote {

	public void save(LineaDTO entity, String nombreUsuario) throws Exception;
	
}
