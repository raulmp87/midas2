/**
 * Ejecuta StoresProcedures de Oracle con los parametros de entrada precediendo al de salida
 */
package mx.com.afirme.midas.interfaz;

import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import mx.com.afirme.midas2.util.ReflectionUtils;

import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.dbutils.DbUtils;
import org.apache.log4j.Logger;



/**
 * @author andres.avalos
 *
 */
public class StoredProcedureHelper {
	
	private String cadenaJNDI = null;
	private Connection conexion = null;
	private CallableStatement cs = null;
	private ResultSet rs= null;
	private String nombre = null;
	private String claseResultado = null;
	private HashMap<String, String> mapaResultados = null;
	private List<StoredProcedureParameter> parametros = null;
	
	private Integer codigoRespuesta = null;
	private String descripcionRespuesta = null;
	private final int numParametrosSalida = 3;
	//private final int numParametrosSalidaNz = 1;
	
	//Constantes para cadenas JNDI de origen de datos para Stored Procedures
	public static final String DATASOURCE_INTERFAZ = "jdbc/InterfazDataSource";
	public static final String DATASOURCE_MIDAS = "jdbc/MidasDataSource";
	public static final String DATASOURCE_MIZAR = "jdbc/MizarDataSource";
	public static final String DATASOURCE_NETEZZA = "jdbc/NetezzaDataSource";
	
	public static final String SEPARADOR_COMA = ",";
	public static final String VOID_CLASS = "void.class";
	public static final String VALUE_NULL = "null";
	public static final String VALUE_STRING = "string";
	
	private enum TipoProcedimiento {SINGLERESULT, RESULTLIST, EXECUTEUPDATE, EXECUTENETEZZA,EXECUTESTRING};
	
	private static Logger logger = Logger.getLogger(StoredProcedureHelper.class);
	
	/**
	 * Constructor minimo
	 * @throws Exception 
	 */
	public StoredProcedureHelper() throws Exception {
		obtieneConexion();
		this.parametros = new ArrayList<StoredProcedureParameter>();
	}
	
	/**
	 * Constructor completo
	 * @param nombre Nombre del stored procedure a ejecutar
	 * @throws Exception 
	 */
	public StoredProcedureHelper(String nombre) throws Exception {
		this.nombre = nombre;
		obtieneConexion();
		this.parametros = new ArrayList<StoredProcedureParameter>();
	}
	
	/**
	 * 
	 * @param nombre Nombre del stored procedure a ejecutar
	 * @param cadenaOrigenDatos Nombre de la cadena JNDI de origen de datos
	 * @throws Exception
	 */
	public StoredProcedureHelper(String nombre, String cadenaOrigenDatos) throws Exception {
		this.nombre = nombre;
		this.cadenaJNDI = cadenaOrigenDatos.trim();
		obtieneConexion();
		this.parametros = new ArrayList<StoredProcedureParameter>();
	}
	
	
	/**
	 * Obtiene la conexion a la Base de datos
	 * @throws Exception 
	 */
	@SuppressWarnings("unused")
	private void obtieneConexion() throws Exception {
		try
        {
			String cadenaConexion = DATASOURCE_INTERFAZ;
			
			if (this.cadenaJNDI != null) {
				cadenaConexion = this.cadenaJNDI;
			}
			
	        Context ctx = new InitialContext();
	        if(ctx == null ) 
	            throw new Exception("No Contexto");
	
	        DataSource ds = 
	              (DataSource)ctx.lookup(
	            		  cadenaConexion);
	
	        if (ds != null) {
	          this.conexion = ds.getConnection();
	        }
	        
        }
	    catch(Exception e) {
	        throw e;
	    } 
	}
	
	/**
	 * Establece un parametro de entrada para el stored procedure
	 * @param nombreParametro Nombre del parametro
	 * @param valorParametro Valor del parametro
	 * 
	 * NOTA: Los parametros se tienen que establecer en el mismo orden en el que 
	 * estan declarados en el Stored Procedure en la Base de Datos
	 */
	public void estableceParametro(String nombreParametro, Object valorParametro) {
	
		//Se hace una validacion si el tipo es Date de java (java.util.Date) se convierta a Date para SQL (java.sql.Timestamp)
		Object valorParametroFinal = null;
		if (valorParametro != null && valorParametro.getClass().getName().equals("java.util.Date")) {
			java.util.Date dateUtil = (java.util.Date) valorParametro;
			valorParametroFinal = new java.sql.Timestamp(dateUtil.getTime());
		} else {
			valorParametroFinal = valorParametro;
		}
		
		this.parametros.add(new StoredProcedureParameter(nombreParametro, valorParametroFinal));
	
	}
	
	/**
	 * Establece el tipo de objeto que devolvera la ejecucion del Stored Procedure
	 * @param claseResultado clase del objeto resultado
	 * @param propiedades propiedades del objeto resultado (separados por coma ',')
	 * @param columnasBaseDatos columnas de la base de datos separados por coma ','. Deben 
	 * ser la misma cantidad y en el mismo orden de que las propiedades del objeto resultado
	 * de tal manera que a cada propiedad le corresponda una columna de base de datos
	 */
	public void estableceMapeoResultados(String claseResultado, String []propiedades, String []columnasBaseDatos) {
		mapaResultados = new HashMap<String, String>();
		this.claseResultado = claseResultado;
		
		if (propiedades.length == columnasBaseDatos.length) {
			for(int i=0;i<propiedades.length;i++){
				mapaResultados.put(propiedades[i], columnasBaseDatos[i]);
			}
		}
		
		
	}
	
	/**
	 * Establece el tipo de objeto que devolvera la ejecucion del Stored Procedure
	 * @param claseResultado clase del objeto resultado
	 * @param propiedades propiedades del objeto resultado (separados por coma ',')
	 * @param columnasBaseDatos columnas de la base de datos separados por coma ','. Deben 
	 * ser la misma cantidad y en el mismo orden de que las propiedades del objeto resultado
	 * de tal manera que a cada propiedad le corresponda una columna de base de datos
	 */
	public void estableceMapeoResultados(String claseResultado, String propiedades, String columnasBaseDatos) {
		mapaResultados = new HashMap<String, String>();
		int i = 0;
		this.claseResultado = claseResultado;
		
		String[] aPropiedades = propiedades.split(SEPARADOR_COMA);
		String[] aColumnas = columnasBaseDatos.split(SEPARADOR_COMA);
		if (aPropiedades.length == aColumnas.length) {
			for (String propiedad : aPropiedades) {
				
				mapaResultados.put(propiedad, aColumnas[i]);
				i++;
			}
		}
		
		
	}
	
	
	/**
	 * Obtiene un objeto del tipo mapeado como resultado de la ejecucion del Stored procedure
	 * @return Un objeto del tipo mapeado como resultado de la ejecucion del Stored procedure
	 * @throws Exception 
	 */
	public Object obtieneResultadoSencillo() throws Exception {
		try
        {
			Object clase = null;
			
			ejecutaStoredProcedure (TipoProcedimiento.SINGLERESULT);
			
	        this.codigoRespuesta = (Integer) this.cs.getObject(parametros.size() + 2);
	        this.descripcionRespuesta = (String) this.cs.getObject(parametros.size() + 3);
	        
	        this.rs = (ResultSet)this.cs.getObject(parametros.size() + 1);
	        
	        
	        while(rs.next()) {
		        clase = obtieneRegistro(this.rs);
		        return clase;
		    }
	        
	    } catch(Exception e) {
	        throw e;
	    }finally{
	    	if (this.rs != null)
	    		DbUtils.closeQuietly(this.rs);
	    	if (this.cs != null)
	    		DbUtils.closeQuietly(this.cs);
	    	if (this.conexion != null)
	    		DbUtils.closeQuietly(this.conexion);	    	
	    }
	    return null;
	}
	
	/**
	 * Obtiene una lista de objetos del tipo mapeado como resultado de la ejecucion del Stored procedure
	 * @return Una lista de objetos del tipo mapeado como resultado de la ejecucion del Stored procedure
	 * @throws Exception 
	 */
	@SuppressWarnings({ "rawtypes" })
	public List obtieneListaResultados() throws Exception {
		try
        {
			Object clase = null;
			List<Object> listaObjetos = new ArrayList<Object>();
			
			ejecutaStoredProcedure (TipoProcedimiento.RESULTLIST);
			
			
			this.codigoRespuesta = (Integer) this.cs.getObject(parametros.size() + 2);
		        this.descripcionRespuesta = (String) this.cs.getObject(parametros.size() + 3);
			
		        this.rs = (ResultSet)this.cs.getObject(parametros.size() + 1);
	        
	        while(this.rs.next()) {
		        clase = obtieneRegistro(this.rs);
		        listaObjetos.add(clase);
		    }
	        
	        return listaObjetos;
	        
	    } catch(Exception e) {
	        throw e;
	    }finally{
	    	if (this.rs != null)
	    		DbUtils.closeQuietly(this.rs);
	    	if (this.cs != null)
	    		DbUtils.closeQuietly(this.cs);
	    	if (this.conexion != null)
	    		DbUtils.closeQuietly(this.conexion);	    	
	    }
	}
	
	/***Soporta herencia******************************************/
	public List obtieneListaResultadosHerencia() throws Exception {
		try
        {
			Object clase = null;
			List<Object> listaObjetos = new ArrayList<Object>();
			
			ejecutaStoredProcedure (TipoProcedimiento.RESULTLIST);
			
			
			this.codigoRespuesta = (Integer) this.cs.getObject(parametros.size() + 2);
		        this.descripcionRespuesta = (String) this.cs.getObject(parametros.size() + 3);
			
		        this.rs = (ResultSet)this.cs.getObject(parametros.size() + 1);
	        
	        while(this.rs.next()) {
		        clase = obtieneRegistroHerencia(this.rs);
		        listaObjetos.add(clase);
		    }
	        
	        return listaObjetos;
	        
	    } catch(Exception e) {
	        throw e;
	    }finally{
	    	if (this.rs != null)
	    		DbUtils.closeQuietly(this.rs);
	    	if (this.cs != null)
	    		DbUtils.closeQuietly(this.cs);
	    	if (this.conexion != null)
	    		DbUtils.closeQuietly(this.conexion);	    	
	    }
	}
	
	/**
	 * Ejecuta un Stored procedure transaccional (INSERT, UPDATE, DELETE)
	 * @return Un entero indicando si la ejecucion fue exitosa o no
	 * @throws Exception 
	 */
	public int ejecutaActualizar() throws Exception {
		try
        {
			BigDecimal res = BigDecimal.ZERO;
			
			ejecutaStoredProcedure (TipoProcedimiento.EXECUTEUPDATE);
			
			if (this.claseResultado == null || 
    				!this.claseResultado.toLowerCase().equals(VOID_CLASS)) {
				res = this.cs.getBigDecimal(parametros.size() + 1);
				
				this.codigoRespuesta = (Integer) this.cs.getObject(parametros.size() + 2);
		        this.descripcionRespuesta = (String) this.cs.getObject(parametros.size() + 3);
		        
		        if (res == null || res.equals(new BigDecimal("-1"))|| codigoRespuesta == null 
		        		|| (!codigoRespuesta.equals(new Integer("0")))) {
		        	String resStr = res != null ? res.toString() : VALUE_NULL;
		        	String codigoRespuestaStr = codigoRespuesta != null ? codigoRespuesta.toString() : VALUE_NULL;
		        	String descripcionRespuestaStr = descripcionRespuesta != null ? descripcionRespuesta.toString() : VALUE_NULL;
		        	
		        	throw new SQLException("Error en la ejecucion del Stored Procedure - " +
		        						   "[res="+resStr+", codigoRespuesta="+codigoRespuestaStr+", " +
		        						   	"descripcionRespuesta="+descripcionRespuestaStr+"]");
		        }
		        
    		} else {
    			res = BigDecimal.ONE;
    		}
			return Integer.parseInt(res.toString());
	       
	    }
	    catch(Exception e) {
	        throw e;
	    } finally{
	    	if (this.rs != null)
	    		DbUtils.closeQuietly(this.rs);
	    	if (this.cs != null)
	    		DbUtils.closeQuietly(this.cs);
	    	if (this.conexion != null)
	    		DbUtils.closeQuietly(this.conexion);	    	
	    }
	}
	public String ejecutaActualizarString() throws Exception {
		try
        {
			String res="";
			
			ejecutaStoredProcedure (TipoProcedimiento.EXECUTESTRING);
			
			if (this.claseResultado == null || this.claseResultado.toLowerCase().equals(VALUE_STRING)) {
				res = this.cs.getString(parametros.size() + 1);
				
				this.codigoRespuesta = (Integer) this.cs.getObject(parametros.size() + 2);
		        this.descripcionRespuesta = (String) this.cs.getObject(parametros.size() + 3);
		        
		        if (res == null || codigoRespuesta == null 
		        		|| (!codigoRespuesta.equals(new Integer("0")))) {
		        	String resStr = res != null ? res.toString() : VALUE_NULL;
		        	String codigoRespuestaStr = codigoRespuesta != null ? codigoRespuesta.toString() : VALUE_NULL;
		        	String descripcionRespuestaStr = descripcionRespuesta != null ? descripcionRespuesta.toString() : VALUE_NULL;
		        	
		        	throw new SQLException("Error en la ejecucion del Stored Procedure - " +
		        						   "[res="+resStr+", codigoRespuesta="+codigoRespuestaStr+", " +
		        						   	"descripcionRespuesta="+descripcionRespuestaStr+"]");
		        }
		        
    		} else {
    			res = null;
    		}
			return res.toString();
	       
	    }
	    catch(Exception e) {
	        throw e;
	    } finally{
	    	if (this.rs != null)
	    		DbUtils.closeQuietly(this.rs);
	    	if (this.cs != null)
	    		DbUtils.closeQuietly(this.cs);
	    	if (this.conexion != null)
	    		DbUtils.closeQuietly(this.conexion);	    	
	    }
	}
	
	/**
	 * Ejecuta un Stored procedure transaccional (INSERT, UPDATE, DELETE)
	 * @return Un entero indicando si la ejecucion fue exitosa o no
	 * @throws Exception 
	 *//*
	public int ejecutaSPNetezza() throws Exception {
		try
        {
			BigDecimal res = new BigDecimal(0);
			
			ejecutaStoredProcedureNz (TipoProcedimiento.EXECUTENETEZZA);
			
			if (this.claseResultado == null || 
    				!this.claseResultado.toLowerCase().equals(VOID_CLASS)) {
				res = this.cs.getBigDecimal(parametros.size() + 1);
				
				this.codigoRespuesta = (Integer) this.cs.getObject(parametros.size() + 2);
		        this.descripcionRespuesta = (String) this.cs.getObject(parametros.size() + 3);
		        
		        if (res == null || res.equals(new BigDecimal("-1"))|| codigoRespuesta == null 
		        		|| (!codigoRespuesta.equals(new Integer("0")))) {
		        	String resStr = res != null ? res.toString() : VALUE_NULL;
		        	String codigoRespuestaStr = codigoRespuesta != null ? codigoRespuesta.toString() : VALUE_NULL;
		        	String descripcionRespuestaStr = descripcionRespuesta != null ? descripcionRespuesta.toString() : VALUE_NULL;
		        	
		        	throw new SQLException("Error en la ejecucion del Stored Procedure - " +
		        						   "[res="+resStr+", codigoRespuesta="+codigoRespuestaStr+", " +
		        						   	"descripcionRespuesta="+descripcionRespuestaStr+"]");
		        }
		        
    		} else {
    			res = BigDecimal.ONE;
    		}
			return Integer.parseInt(res.toString());
	       
	    }
	    catch(Exception e) {
	        throw e;
	    } finally{
	    	if (this.rs != null)
	    		DbUtils.closeQuietly(this.rs);
	    	if (this.cs != null)
	    		DbUtils.closeQuietly(this.cs);
	    	if (this.conexion != null)
	    		DbUtils.closeQuietly(this.conexion);	    	
	    }
	}
	*/
	/**
	 * Ejecuta un Stored procedure transaccional (INSERT, UPDATE, DELETE)
	 * @return Un entero indicando si la ejecucion fue exitosa o no
	 * @throws Exception 
	 */
	public int ejecutaActualizarSQLServer() throws Exception {
		try
        {
			BigDecimal res = BigDecimal.ZERO;
			
			ejecutaStoredProcedureSQLServer (TipoProcedimiento.EXECUTEUPDATE);
			
			if (this.claseResultado == null || 
    				!this.claseResultado.toLowerCase().equals(VOID_CLASS)) {
//				int updatedParams=cs.getUpdateCount();
//				boolean test=cs.getMoreResults(Statement.KEEP_CURRENT_RESULT);//FIx de SQL Server para webshpere
//				System.out.println("---SQL SERVER[Updated:"+updatedParams+"|Results:"+test+"]");
				Integer data=this.cs.getInt(parametros.size()+1);
				res=(data!=null)?new BigDecimal(data):new BigDecimal(1);
				//res = this.cs.getBigDecimal(parametros.size() + 1);
				String data2=this.cs.getString(parametros.size() + 2);
				this.codigoRespuesta = (data2!=null)?Integer.parseInt(data2):null;
		        this.descripcionRespuesta = (String) this.cs.getObject(parametros.size() + 3);
		        
		        if (res == null || res.equals(new BigDecimal("-1"))|| codigoRespuesta == null 
		        		|| (!codigoRespuesta.equals(new Integer("0")))) {
		        	String resStr = res != null ? res.toString() : VALUE_NULL;
		        	String codigoRespuestaStr = codigoRespuesta != null ? codigoRespuesta.toString() : VALUE_NULL;
		        	String descripcionRespuestaStr = descripcionRespuesta != null ? descripcionRespuesta.toString() : VALUE_NULL;
		        	
		        	throw new SQLException("Error en la ejecucion del Stored Procedure - " +
		        						   "[res="+resStr+", codigoRespuesta="+codigoRespuestaStr+", " +
		        						   	"descripcionRespuesta="+descripcionRespuestaStr+"]");
		        }
		        
    		} else {
    			res = BigDecimal.ONE;
    		}
			return Integer.parseInt(res.toString());
	       
	    }
	    catch(Exception e) {
	        throw e;
	    } finally{
	    	if (this.rs != null)
	    		DbUtils.closeQuietly(this.rs);
	    	if (this.cs != null)
	    		DbUtils.closeQuietly(this.cs);
	    	if (this.conexion != null)
	    		DbUtils.closeQuietly(this.conexion);	    	
	    }
	}
	
	/**
	 * Ejecuta el Stored procedure
	 * @param tipoProcedimiento Tipo de procedimiento a ejecutar (SINGLERESULT, RESULTLIST, EXECUTEUPDATE)
	 * @throws Exception 
	 */
	private void ejecutaStoredProcedure(TipoProcedimiento tipoProcedimiento) throws Exception {
		
		try
        {
			
//			  java.util.Map map = this.conexion.getTypeMap();
//		      map.put("mySchemaName.ATHLETES", Class.forName("Athletes"));
//		      this.conexion.setTypeMap(map);
			
	        cs = this.conexion.prepareCall("BEGIN " + this.nombre.trim()+ "(" + numeroParametrosEnQuery() + "); END;");
	        
	        for (StoredProcedureParameter parametro : parametros) {
	        	registraParametro(parametro, parametros.indexOf(parametro) + 1);
	        	
	        }
	        //Se registrara el parametro de salida con la consideracion que en el Stored Procedure
	        //de la BD tiene que ser el ultimo y unico parametro de salida
	        switch (tipoProcedimiento) {
	        
	        case SINGLERESULT:
	        	this.cs.registerOutParameter(this.parametros.size() + 1, oracle.jdbc.driver.OracleTypes.CURSOR);
	        	this.cs.registerOutParameter(this.parametros.size() + 2, oracle.jdbc.driver.OracleTypes.INTEGER);
	        	this.cs.registerOutParameter(this.parametros.size() + 3, oracle.jdbc.driver.OracleTypes.VARCHAR);
	        	break;
	        case RESULTLIST:
	        	this.cs.registerOutParameter(this.parametros.size() + 1, oracle.jdbc.driver.OracleTypes.CURSOR);
	        	this.cs.registerOutParameter(this.parametros.size() + 2, oracle.jdbc.driver.OracleTypes.INTEGER);
	        	this.cs.registerOutParameter(this.parametros.size() + 3, oracle.jdbc.driver.OracleTypes.VARCHAR);
	        	break;
	       case EXECUTEUPDATE:
	        	if (this.claseResultado == null || 
	    				!this.claseResultado.toLowerCase().equals(VOID_CLASS)) {
	        		this.cs.registerOutParameter(this.parametros.size() + 1, java.sql.Types.INTEGER);
	        		this.cs.registerOutParameter(this.parametros.size() + 2, oracle.jdbc.driver.OracleTypes.INTEGER);
//	        		this.cs.registerOutParameter(this.parametros.size() + 2, java.sql.Types.INTEGER);
		        	this.cs.registerOutParameter(this.parametros.size() + 3, oracle.jdbc.driver.OracleTypes.VARCHAR);
//		        	this.cs.registerOutParameter(this.parametros.size() + 3, java.sql.Types.VARCHAR);
		        	
	    		}
	        	break;
	       case EXECUTESTRING:
	        	if (this.claseResultado.toLowerCase().equals(VALUE_STRING)) {
	        		this.cs.registerOutParameter(this.parametros.size() + 1, java.sql.Types.VARCHAR);
	        		this.cs.registerOutParameter(this.parametros.size() + 2, oracle.jdbc.driver.OracleTypes.INTEGER);
		        	this.cs.registerOutParameter(this.parametros.size() + 3, oracle.jdbc.driver.OracleTypes.VARCHAR);
	    		}
	        	break;
	       case EXECUTENETEZZA:
	        	if (this.claseResultado == null || 
	    				!this.claseResultado.toLowerCase().equals(VOID_CLASS)) {
		        	//this.cs.registerOutParameter(this.parametros.size(), oracle.jdbc.driver.OracleTypes.VARCHAR);
	        		
	    		}
	        	break;
	        }
	        
	        cs.execute();
	        
	    }catch(Exception e){
	        throw e;
	    } 
	    
	}
	
	
	/**
	 * Ejecuta el Stored procedure
	 * @param tipoProcedimiento Tipo de procedimiento a ejecutar (SINGLERESULT, RESULTLIST, EXECUTEUPDATE)
	 * @throws Exception 
	 *//*
	private void ejecutaStoredProcedureNz(TipoProcedimiento tipoProcedimiento) throws Exception {
		
		try
        {
	        cs = this.conexion.prepareCall("{call " + this.nombre.trim()+ "(" + numeroParametrosEnQueryNz() + ")}");
	        
	        for (StoredProcedureParameter parametro : parametros) {
	        	registraParametro(parametro, parametros.indexOf(parametro) + 1);
	        	
	        }
	        //Se registrara el parametro de salida con la consideracion que en el Stored Procedure
	        //de la BD tiene que ser el ultimo y unico parametro de salida
	        switch (tipoProcedimiento) {
	        
	        case SINGLERESULT:
	        	this.cs.registerOutParameter(this.parametros.size() + 1, oracle.jdbc.driver.OracleTypes.CURSOR);
	        	this.cs.registerOutParameter(this.parametros.size() + 2, oracle.jdbc.driver.OracleTypes.INTEGER);
	        	this.cs.registerOutParameter(this.parametros.size() + 3, oracle.jdbc.driver.OracleTypes.VARCHAR);
	        	break;
	        case RESULTLIST:
	        	this.cs.registerOutParameter(this.parametros.size() + 1, oracle.jdbc.driver.OracleTypes.CURSOR);
	        	this.cs.registerOutParameter(this.parametros.size() + 2, oracle.jdbc.driver.OracleTypes.INTEGER);
	        	this.cs.registerOutParameter(this.parametros.size() + 3, oracle.jdbc.driver.OracleTypes.VARCHAR);
	        	break;
	       case EXECUTEUPDATE:
	        	if (this.claseResultado == null || 
	    				!this.claseResultado.toLowerCase().equals(VOID_CLASS)) {
	        		this.cs.registerOutParameter(this.parametros.size() + 1, java.sql.Types.INTEGER);
	        		this.cs.registerOutParameter(this.parametros.size() + 2, oracle.jdbc.driver.OracleTypes.INTEGER);
//	        		this.cs.registerOutParameter(this.parametros.size() + 2, java.sql.Types.INTEGER);
		        	this.cs.registerOutParameter(this.parametros.size() + 3, oracle.jdbc.driver.OracleTypes.VARCHAR);
//		        	this.cs.registerOutParameter(this.parametros.size() + 3, java.sql.Types.VARCHAR);
		        	
	    		}
	        	break;
	       case EXECUTENETEZZA:
	        	if (this.claseResultado == null || 
	    				!this.claseResultado.toLowerCase().equals(VOID_CLASS)) {
		        	//this.cs.registerOutParameter(this.parametros.size(), oracle.jdbc.driver.OracleTypes.VARCHAR);
	       
	    		}
	        	break;
	        }
	        
	        cs.execute();
	        
	    }
	    catch(Exception e)
	        {
	        throw e;
	    } 
	    
	}
	*/
	/**
	 * Ejecuta el Stored procedure
	 * @param tipoProcedimiento Tipo de procedimiento a ejecutar (SINGLERESULT, RESULTLIST, EXECUTEUPDATE)
	 * @throws Exception 
	 */
	private void ejecutaStoredProcedureSQLServer(TipoProcedimiento tipoProcedimiento) throws Exception {
		
		try
        {
			
//			  java.util.Map map = this.conexion.getTypeMap();
//		      map.put("mySchemaName.ATHLETES", Class.forName("Athletes"));
//		      this.conexion.setTypeMap(map);

			
	        cs = this.conexion.prepareCall("{call " + this.nombre.trim()+ "(" + numeroParametrosEnQuery() + ")}");
	        
	        for (StoredProcedureParameter parametro : parametros) {
	        	registraParametro(parametro, parametros.indexOf(parametro) + 1);
	        	
	        }
	        //Se registrara el parametro de salida con la consideracion que en el Stored Procedure
	        //de la BD tiene que ser el ultimo y unico parametro de salida
	        switch (tipoProcedimiento) {
	        
	        case SINGLERESULT:
	        	this.cs.registerOutParameter(this.parametros.size() + 1, oracle.jdbc.driver.OracleTypes.CURSOR);
	        	this.cs.registerOutParameter(this.parametros.size() + 2, oracle.jdbc.driver.OracleTypes.INTEGER);
	        	this.cs.registerOutParameter(this.parametros.size() + 3, oracle.jdbc.driver.OracleTypes.VARCHAR);
	        	break;
	        case RESULTLIST:
	        	this.cs.registerOutParameter(this.parametros.size() + 1, oracle.jdbc.driver.OracleTypes.CURSOR);
	        	this.cs.registerOutParameter(this.parametros.size() + 2, oracle.jdbc.driver.OracleTypes.INTEGER);
	        	this.cs.registerOutParameter(this.parametros.size() + 3, oracle.jdbc.driver.OracleTypes.VARCHAR);
	        	break;
	       case EXECUTEUPDATE:
	        	if (this.claseResultado == null || 
	    				!this.claseResultado.toLowerCase().equals(VOID_CLASS)) {
	        		this.cs.registerOutParameter(this.parametros.size() + 1, java.sql.Types.INTEGER);
	        		this.cs.registerOutParameter(this.parametros.size() + 2, java.sql.Types.VARCHAR);
//	        		this.cs.registerOutParameter(this.parametros.size() + 2, java.sql.Types.INTEGER);
		        	this.cs.registerOutParameter(this.parametros.size() + 3, oracle.jdbc.driver.OracleTypes.VARCHAR);
//		        	this.cs.registerOutParameter(this.parametros.size() + 3, java.sql.Types.VARCHAR);
		        	
	    		}
	        	break;
	        }
	        
	        cs.execute();
	        
	    }
	    catch(Exception e)
	        {
	        throw e;
	    } 
	    
	}

	/**
	 * Genera una cadena que indica el numero de parametros necesaria para la invocacion del Stored procedure
	 * @return Una cadena que indica el numero de parametros necesaria para la invocacion del Stored procedure
	 */
	private String numeroParametrosEnQuery() {
		String resultado = "";	
		StringBuilder resul = new StringBuilder("");
		
		int parametroSalida = this.numParametrosSalida;	
		
		if (this.claseResultado != null && 
				this.claseResultado.toLowerCase().equals(VOID_CLASS)) {
			parametroSalida = 0;
		}
		
		for (int i = 0; i < this.parametros.size() + parametroSalida; i++) {
			resul.append("?,");
		}
		resultado = resul.toString();
		if (resultado.length() > 0) {
			resultado = resultado.substring(0, resultado.lastIndexOf(SEPARADOR_COMA));
		}
		
		return resultado;
	}
	
	/**
	 * Genera una cadena que indica el numero de parametros necesaria para la invocacion del Stored procedure
	 * @return Una cadena que indica el numero de parametros necesaria para la invocacion del Stored procedure
	 */
	private String numeroParametrosEnQueryNz() {
		
		StringBuilder resultado = new StringBuilder("");	
		
		int parametroSalida = 0;
		
		if (this.claseResultado != null && 
				this.claseResultado.toLowerCase().equals(VOID_CLASS)) {
			parametroSalida = 0;
		}
		
		for (int i = 0; i < this.parametros.size() + parametroSalida; i++) {
			resultado.append("?,");
		}
		String result=resultado.toString();
		
		if (result.length() > 0) {
			result = result.substring(0, result.lastIndexOf(SEPARADOR_COMA));
		}
		
		return result;
	}
	
	/**
	 * Registra un parametro de entrada del Stored procedure
	 * @param parametro Parametro de entrada del Stored procedure
	 * @param indiceParametro indice del parametro de entrada del Stored procedure
	 * @throws Exception 
	 */
	private void registraParametro(StoredProcedureParameter parametro, int indiceParametro) throws Exception {
		try {

			this.cs.setObject(indiceParametro, parametro.getValor());
				
		} catch (Exception e) {
			throw e;
		}
		
		
	}
	
	/**
	 * Ejecuta el mapeo del resultado del Stored procedure al objeto de salida
	 * @param rs ResultSet del Stored procedure
	 * @return El objeto de salida mapeado
	 * @throws Exception 
	 */
	private Object obtieneRegistro(ResultSet rs) throws Exception {
		
		Object clase = null;
		String nombreColumna;
		Object numberObject = null;
		Object simpleObject = null;
		
		PropertyUtilsBean utilsBean = new PropertyUtilsBean();
		
        try {
			clase = Class.forName(this.claseResultado).newInstance();
		
	        for (Field field : Class.forName(this.claseResultado).getDeclaredFields()) {
	        	nombreColumna = null;
	        	nombreColumna = this.mapaResultados.get(field.getName());
	        	
	        	if (nombreColumna != null) {
	        			simpleObject = rs.getObject(rs.findColumn(nombreColumna));
	        		
	        		if (simpleObject != null) {
		        		
		        		//Si es un BigDecimal
		        		if (simpleObject.getClass().getSimpleName().equals("BigDecimal")) {
		        			
		        			if (field.getType().getSimpleName().equals("Integer")) {
		        				numberObject = Integer.valueOf(simpleObject.toString());
		        			} else if (field.getType().getSimpleName().equals("Short")) {
		        				numberObject = Short.valueOf(simpleObject.toString());
		        			} else if (field.getType().getSimpleName().equals("Long")) {
		        				numberObject = Long.valueOf(simpleObject.toString());
		        			} else if (field.getType().getSimpleName().equals("Float")) {
		        				numberObject = Float.valueOf(simpleObject.toString());
		        			} else if (field.getType().getSimpleName().equals("Double")) {
		        				numberObject = Double.valueOf(simpleObject.toString());		        				
		        			} else {
		        				//BigDecimal
		        				numberObject = simpleObject;
		        			}
		        			
		        			utilsBean.setProperty(clase, field.getName(),  field.getType().cast(numberObject));
		        			
		        		} else{
		        			if(field.getType().getSimpleName().equals("GregorianCalendar")){
			        			String[] arr = simpleObject != null ? simpleObject.toString().split("/")
			        					: null;
			        			GregorianCalendar calendarObject = new GregorianCalendar();
			        			calendarObject.set(Calendar.YEAR, new Integer(arr[2]).intValue());
			        			calendarObject.set(Calendar.MONTH, new Integer(arr[1]).intValue() -1);
			        			calendarObject.set(Calendar.DATE, new Integer(arr[0]).intValue());
		        				utilsBean.setProperty(clase, field.getName(),  field.getType().cast(calendarObject));
		        			}else{
		        				if(simpleObject!=null){
		        					utilsBean.setProperty(clase, field.getName(),  field.getType().cast(simpleObject));
		        				}
		        			}
		        		}
	        		}
	        	}
	        	
	        }       
	        
        }  catch (Exception e) {
			throw e;
		} 
		
		return clase;
	}
	
	/***Soporta herencia********************************************************/
	/**
	 * Ejecuta el mapeo del resultado del Stored procedure al objeto de salida
	 * @param rs ResultSet del Stored procedure
	 * @return El objeto de salida mapeado
	 * @throws Exception 
	 */
	private Object obtieneRegistroHerencia(ResultSet rs) throws Exception {
		
		Object clase = null;
		String nombreColumna;
		Object numberObject = null;
		Object simpleObject = null;
		
		PropertyUtilsBean utilsBean = new PropertyUtilsBean();
		
        try {
			clase = Class.forName(this.claseResultado).newInstance();
			Field[] fields=ReflectionUtils.getAllFields(Class.forName(this.claseResultado));
	        for (Field field : fields) {
	        	nombreColumna = null;
	        	nombreColumna = this.mapaResultados.get(field.getName());
	        	
	        	if (nombreColumna != null) {
	        			simpleObject = rs.getObject(rs.findColumn(nombreColumna));
	        		
	        		if (simpleObject != null) {
		        		
		        		//Si es un BigDecimal
		        		if (simpleObject.getClass().getSimpleName().equals("BigDecimal")) {
		        			
		        			if (field.getType().getSimpleName().equals("Integer")) {
		        				numberObject = Integer.valueOf(simpleObject.toString());
		        			} else if (field.getType().getSimpleName().equals("Short")) {
		        				numberObject = Short.valueOf(simpleObject.toString());
		        			} else if (field.getType().getSimpleName().equals("Long")) {
		        				numberObject = Long.valueOf(simpleObject.toString());
		        			} else if (field.getType().getSimpleName().equals("Float")) {
		        				numberObject = Float.valueOf(simpleObject.toString());
		        			} else if (field.getType().getSimpleName().equals("Double")) {
		        				numberObject = Double.valueOf(simpleObject.toString());		        				
		        			} else {
		        				//BigDecimal
		        				numberObject = simpleObject;
		        			}
		        			
		        			utilsBean.setProperty(clase, field.getName(),  field.getType().cast(numberObject));
		        			
		        		} else{
		        			if(field.getType().getSimpleName().equals("GregorianCalendar")){
			        			String[] arr = simpleObject != null ? simpleObject.toString().split("/")
			        					: null;
			        			GregorianCalendar calendarObject = new GregorianCalendar();
			        			calendarObject.set(Calendar.YEAR, new Integer(arr[2]).intValue());
			        			calendarObject.set(Calendar.MONTH, new Integer(arr[1]).intValue() -1);
			        			calendarObject.set(Calendar.DATE, new Integer(arr[0]).intValue());
		        				utilsBean.setProperty(clase, field.getName(),  field.getType().cast(calendarObject));
		        			}else{
		        				utilsBean.setProperty(clase, field.getName(),  field.getType().cast(simpleObject));
		        			}
		        		}
	        		}
	        	}
	        	
	        }
	       
	        
        }  catch (Exception e) {
			throw e;
		} 
		
		return clase;
	}
	

	/**
	 * Genera un archivo CSV y lo almacena en el servidor partir del ResultSet 
	 * de un cursor devuelto por la ejecucion del Stored procedure
	 * 
	 * @param fullPath  Ruta y nombre del archivo CSV a crear
	 * @return 
	 * @throws Exception
	 */
	public String obtieneArchivoCSV(String fullPath) throws Exception {
		try {
			
			final long ROWS_BLOCK_SIZE = 50000; //TODO definirlo como un parametro config en caso de requerirse
			Long numeroRegistros = 0L;
			File file;
			ResultSetMetaData meta;
			int numeroColumnas;
			
			StringBuilder resultSetCSV = new StringBuilder();

			ejecutaStoredProcedure(TipoProcedimiento.RESULTLIST);

			this.codigoRespuesta = (Integer) this.cs.getObject(parametros
					.size() + 2);
			this.descripcionRespuesta = (String) this.cs.getObject(parametros
					.size() + 3);

			this.rs = (ResultSet) this.cs.getObject(parametros.size() + 1);
			
			meta = rs.getMetaData();
			numeroColumnas = meta.getColumnCount();
			
			// Iterar los encabezados del cursor 
			StringBuilder encabezados = new StringBuilder();
			encabezados.append( "\"").append(meta.getColumnName(1)).append("\"");
			for (int i = 2; i < numeroColumnas + 1; i++) {
				encabezados.append(",\"").append(meta.getColumnName(i)).append("\"");
			}

			resultSetCSV.append(encabezados.toString());
			resultSetCSV.append(System.getProperty("line.separator"));
			
			// Si no trae extension se agrega la correspondiente
			if(!fullPath.contains("."))
			{
				fullPath = fullPath.concat(".csv");
			}
			
			file = new File(fullPath);
			
			// eliminar el archivo si ya existe
			logger.info("Se elimina el archivo existente: " + file.delete());			
			
			file.createNewFile();
			logger.info("Se crea el archivo: " + fullPath);

			while (this.rs.next()) 
			{
				StringBuilder rows = new StringBuilder( "\"");
				rows.append(rs.getString(1)).append("\"");
				for (int i = 2; i < numeroColumnas + 1; i++) {
					rows.append(",\"").append(rs.getString(i)).append("\"");
				}
				resultSetCSV.append(rows.toString());
				resultSetCSV.append(System.getProperty("line.separator"));
				
				// escribir el archivo bloque por bloque evitando 
				// mantener todo el resultSet en memoria
				if (numeroRegistros == ROWS_BLOCK_SIZE) 
				{					
					FileWriter writer = new FileWriter(fullPath, true);

					writer.write(resultSetCSV.toString());
					logger.info("escribiendo bloque ... ");
					writer.flush();
					writer.close();

					numeroRegistros = 0L;
					resultSetCSV = new StringBuilder();					

				} else {
					numeroRegistros ++;
				}
			}

			// escribe los registros restantes
			FileWriter writer = new FileWriter(fullPath, true);
			writer.write(resultSetCSV.toString());
			logger.info("escribiendo ultimo bloque ... ");
			writer.flush();
			writer.close();

			return fullPath;

		} catch (Exception e) {
			throw e;
		} finally {
			if (this.rs != null)
				DbUtils.closeQuietly(this.rs);
			if (this.cs != null)
				DbUtils.closeQuietly(this.cs);
			if (this.conexion != null)
				DbUtils.closeQuietly(this.conexion);
		}
	}
	/**
	 * @return nombre Nombre del Stored procedure
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre Nombre del Stored procedure a ingresar
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the codigoRespuesta
	 */
	public Integer getCodigoRespuesta() {
		return codigoRespuesta;
	}

	/**
	 * @return the descripcionRespuesta
	 */
	public String getDescripcionRespuesta() {
		return descripcionRespuesta;
	}
	
	
}
