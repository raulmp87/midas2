/**
 * 
 */
package mx.com.afirme.midas.interfaz.asientocontable;

import java.util.List;

import javax.ejb.Remote;

/**
 * @author andres.avalos
 *
 */

public interface AsientoContableFacadeRemote {
		
	public List<AsientoContableDTO> contabilizaMovimientos(String idObjetoContable, String claveTransaccionContable, String nombreUsuario) 
		throws Exception;
	
	
}
