package mx.com.afirme.midas.interfaz.asientocontable;

import java.io.Serializable;

public class AsientoContableDTO implements Serializable{

	private static final long serialVersionUID = -3087599921530210126L;
	
	private String idRamo;
	private String idSubRamo;
	private String conceptoPoliza;
	private Short idMoneda;
	private Double tipoCambio;
	private String claveConceptoMovimiento;
	private Long auxiliar;
	private String centroCosto;
	private String tipoPersona;
	private Double importeNeto;
	private Double importeBonificacion;
	private Double importeDescuento;
	private Double importeDerechos;
	private Double importeRecargos;
	private Double importeIva;
	private Double importeComision;
	private Double importeGastos;
	private Double importeOtrosConceptos;
	private Double importeOtrosImpuestos;
	private String conceptoMovimiento;
	private String nombreUsuario;
	
	/**
	 * @return the idRamo
	 */
	public String getIdRamo() {
		return idRamo;
	}
	/**
	 * @param idRamo the idRamo to set
	 */
	public void setIdRamo(String idRamo) {
		this.idRamo = idRamo;
	}
	/**
	 * @return the idSubRamo
	 */
	public String getIdSubRamo() {
		return idSubRamo;
	}
	/**
	 * @param idSubRamo the idSubRamo to set
	 */
	public void setIdSubRamo(String idSubRamo) {
		this.idSubRamo = idSubRamo;
	}
	/**
	 * @return the conceptoPoliza
	 */
	public String getConceptoPoliza() {
		return conceptoPoliza;
	}
	/**
	 * @param conceptoPoliza the conceptoPoliza to set
	 */
	public void setConceptoPoliza(String conceptoPoliza) {
		this.conceptoPoliza = conceptoPoliza;
	}
	/**
	 * @return the idMoneda
	 */
	public Short getIdMoneda() {
		return idMoneda;
	}
	/**
	 * @param idMoneda the idMoneda to set
	 */
	public void setIdMoneda(Short idMoneda) {
		this.idMoneda = idMoneda;
	}
	/**
	 * @return the tipoCambio
	 */
	public Double getTipoCambio() {
		return tipoCambio;
	}
	/**
	 * @param tipoCambio the tipoCambio to set
	 */
	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	/**
	 * @return the claveConceptoMovimiento
	 */
	public String getClaveConceptoMovimiento() {
		return claveConceptoMovimiento;
	}
	/**
	 * @param claveConceptoMovimiento the claveConceptoMovimiento to set
	 */
	public void setClaveConceptoMovimiento(String claveConceptoMovimiento) {
		this.claveConceptoMovimiento = claveConceptoMovimiento;
	}
	/**
	 * @return the auxiliar
	 */
	public Long getAuxiliar() {
		return auxiliar;
	}
	/**
	 * @param auxiliar the auxiliar to set
	 */
	public void setAuxiliar(Long auxiliar) {
		this.auxiliar = auxiliar;
	}
	/**
	 * @return the centroCosto
	 */
	public String getCentroCosto() {
		return centroCosto;
	}
	/**
	 * @param centroCosto the centroCosto to set
	 */
	public void setCentroCosto(String centroCosto) {
		this.centroCosto = centroCosto;
	}
	/**
	 * @return the tipoPersona
	 */
	public String getTipoPersona() {
		return tipoPersona;
	}
	/**
	 * @param tipoPersona the tipoPersona to set
	 */
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	/**
	 * @return the importeNeto
	 */
	public Double getImporteNeto() {
		return importeNeto;
	}
	/**
	 * @param importeNeto the importeNeto to set
	 */
	public void setImporteNeto(Double importeNeto) {
		this.importeNeto = importeNeto;
	}
	/**
	 * @return the importeBonificacion
	 */
	public Double getImporteBonificacion() {
		return importeBonificacion;
	}
	/**
	 * @param importeBonificacion the importeBonificacion to set
	 */
	public void setImporteBonificacion(Double importeBonificacion) {
		this.importeBonificacion = importeBonificacion;
	}
	/**
	 * @return the importeDescuento
	 */
	public Double getImporteDescuento() {
		return importeDescuento;
	}
	/**
	 * @param importeDescuento the importeDescuento to set
	 */
	public void setImporteDescuento(Double importeDescuento) {
		this.importeDescuento = importeDescuento;
	}
	/**
	 * @return the importeDerechos
	 */
	public Double getImporteDerechos() {
		return importeDerechos;
	}
	/**
	 * @param importeDerechos the importeDerechos to set
	 */
	public void setImporteDerechos(Double importeDerechos) {
		this.importeDerechos = importeDerechos;
	}
	/**
	 * @return the importeRecargos
	 */
	public Double getImporteRecargos() {
		return importeRecargos;
	}
	/**
	 * @param importeRecargos the importeRecargos to set
	 */
	public void setImporteRecargos(Double importeRecargos) {
		this.importeRecargos = importeRecargos;
	}
	/**
	 * @return the importeIva
	 */
	public Double getImporteIva() {
		return importeIva;
	}
	/**
	 * @param importeIva the importeIva to set
	 */
	public void setImporteIva(Double importeIva) {
		this.importeIva = importeIva;
	}
	/**
	 * @return the importeComision
	 */
	public Double getImporteComision() {
		return importeComision;
	}
	/**
	 * @param importeComision the importeComision to set
	 */
	public void setImporteComision(Double importeComision) {
		this.importeComision = importeComision;
	}
	/**
	 * @return the importeGastos
	 */
	public Double getImporteGastos() {
		return importeGastos;
	}
	/**
	 * @param importeGastos the importeGastos to set
	 */
	public void setImporteGastos(Double importeGastos) {
		this.importeGastos = importeGastos;
	}
	/**
	 * @return the importeOtrosConceptos
	 */
	public Double getImporteOtrosConceptos() {
		return importeOtrosConceptos;
	}
	/**
	 * @param importeOtrosConceptos the importeOtrosConceptos to set
	 */
	public void setImporteOtrosConceptos(Double importeOtrosConceptos) {
		this.importeOtrosConceptos = importeOtrosConceptos;
	}
	/**
	 * @return the importeOtrosImpuestos
	 */
	public Double getImporteOtrosImpuestos() {
		return importeOtrosImpuestos;
	}
	/**
	 * @param importeOtrosImpuestos the importeOtrosImpuestos to set
	 */
	public void setImporteOtrosImpuestos(Double importeOtrosImpuestos) {
		this.importeOtrosImpuestos = importeOtrosImpuestos;
	}
	/**
	 * @return the conceptoMovimiento
	 */
	public String getConceptoMovimiento() {
		return conceptoMovimiento;
	}
	/**
	 * @param conceptoMovimiento the conceptoMovimiento to set
	 */
	public void setConceptoMovimiento(String conceptoMovimiento) {
		this.conceptoMovimiento = conceptoMovimiento;
	}
	/**
	 * @return the nombreUsuario
	 */
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	/**
	 * @param nombreUsuario the nombreUsuario to set
	 */
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}	
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();									
							
		sb.append("AsientoContableDTO {"  + '\n');
		sb.append("idRamo = " + idRamo + '\n');
		sb.append("idSubRamo = " + idSubRamo + '\n');
		sb.append("conceptoPoliza = " + conceptoPoliza + '\n');
		sb.append("idMoneda = " + idMoneda + '\n');					
		
		if(tipoCambio != null){
			sb.append("tipoCambio = " + tipoCambio.toString() + '\n');
		}else{
			sb.append("tipoCambio = null " + '\n');
		}
		
		sb.append("centroCosto = " + centroCosto + '\n');
		sb.append("claveConceptoMovimiento = " + claveConceptoMovimiento + '\n');
		sb.append("tipoPersona = " + tipoPersona + '\n');		
		sb.append("conceptoMovimiento = " + conceptoMovimiento + '\n');
		sb.append("}" + '\n');
		
		return sb.toString();						
	}	
}
