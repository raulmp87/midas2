package mx.com.afirme.midas.interfaz.moneda;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;


public interface MonedaFacadeRemote {

	public List<MonedaDTO> findAll(String nombreUsuario)
	throws Exception;
	
}
