package mx.com.afirme.midas.interfaz.solicitudcheque;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class SolicitudChequeDTO implements Serializable {


	private static final long serialVersionUID = -1224209741962980156L;
	
	private BigDecimal idSesion;
	private BigDecimal idPago;
	private BigDecimal idSolCheque;
	private String claveTransaccionContable;
	private Short idMoneda;
	private Double tipoCambio;
	private BigDecimal idPrestadorServicio;
	private String nombreBeneficiario;
	private String tipoPago;
	private String conceptoPoliza;
	private String conceptoMov;
	private String situacionPago;
	private BigDecimal importe;
	private String cuentaBancariaEmisora;
	private BigDecimal folio;
	private Date fechaMovimiento;
	private Long idAgente;
	private Long auxiliar;
	private Integer pctIVAAcreditable;
	private Integer pctIVARetenido; 
	private Integer pctISRRetenido;
	private Integer claveTipoOperacion;

	
	
	public BigDecimal getIdSesion() {
		return idSesion;
	}
	public void setIdSesion(BigDecimal idSesion) {
		this.idSesion = idSesion;
	}
	public BigDecimal getIdSolCheque() {
		return idSolCheque;
	}
	public void setIdSolCheque(BigDecimal idSolCheque) {
		this.idSolCheque = idSolCheque;
	}
	/**
	 * @return the idPago
	 */
	public BigDecimal getIdPago() {
		return idPago;
	}
	/**
	 * @param idPago the idPago to set
	 */
	public void setIdPago(BigDecimal idPago) {
		this.idPago = idPago;
	}
	/**
	 * @return the claveTransaccionContable
	 */
	public String getClaveTransaccionContable() {
		return claveTransaccionContable;
	}
	/**
	 * @param claveTransaccionContable the claveTransaccionContable to set
	 */
	public void setClaveTransaccionContable(String claveTransaccionContable) {
		this.claveTransaccionContable = claveTransaccionContable;
	}
	/**
	 * @return the idMoneda
	 */
	public Short getIdMoneda() {
		return idMoneda;
	}
	/**
	 * @param idMoneda the idMoneda to set
	 */
	public void setIdMoneda(Short idMoneda) {
		this.idMoneda = idMoneda;
	}
	/**
	 * @return the tipoCambio
	 */
	public Double getTipoCambio() {
		return tipoCambio;
	}
	/**
	 * @param tipoCambio the tipoCambio to set
	 */
	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	/**
	 * @return the idPrestadorServicio
	 */
	public BigDecimal getIdPrestadorServicio() {
		return idPrestadorServicio;
	}
	/**
	 * @param idPrestadorServicio the idPrestadorServicio to set
	 */
	public void setIdPrestadorServicio(BigDecimal idPrestadorServicio) {
		this.idPrestadorServicio = idPrestadorServicio;
	}
	/**
	 * @return the nombreBeneficiario
	 */
	public String getNombreBeneficiario() {
		return nombreBeneficiario;
	}
	/**
	 * @param nombreBeneficiario the nombreBeneficiario to set
	 */
	public void setNombreBeneficiario(String nombreBeneficiario) {
		this.nombreBeneficiario = nombreBeneficiario;
	}
	/**
	 * @return the tipoPago
	 */
	public String getTipoPago() {
		return tipoPago;
	}
	/**
	 * @param tipoPago the tipoPago to set
	 */
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	/**
	 * @return the conceptoPoliza
	 */
	public String getConceptoPoliza() {
		return conceptoPoliza;
	}
	/**
	 * @param conceptoPoliza the conceptoPoliza to set
	 */
	public void setConceptoPoliza(String conceptoPoliza) {
		this.conceptoPoliza = conceptoPoliza;
	}
	
	public String getConceptoMov() {
		return conceptoMov;
	}
	public void setConceptoMov(String conceptoMov) {
		this.conceptoMov = conceptoMov;
	}
	/**
	 * @return the situacionPago
	 */
	public String getSituacionPago() {
		return situacionPago;
	}
	/**
	 * @param situacionPago the situacionPago to set
	 */
	public void setSituacionPago(String situacionPago) {
		this.situacionPago = situacionPago;
	}
		
	public BigDecimal getImporte() {
		return importe;
	}
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}
	/**
	 * @return the cuentaBancariaEmisora
	 */
	public String getCuentaBancariaEmisora() {
		return cuentaBancariaEmisora;
	}
	/**
	 * @param cuentaBancariaEmisora the cuentaBancariaEmisora to set
	 */
	public void setCuentaBancariaEmisora(String cuentaBancariaEmisora) {
		this.cuentaBancariaEmisora = cuentaBancariaEmisora;
	}
	/**
	 * @return the folio
	 */
	public BigDecimal getFolio() {
		return folio;
	}
	/**
	 * @param folio the folio to set
	 */
	public void setFolio(BigDecimal folio) {
		this.folio = folio;
	}
	/**
	 * @return the fechaMovimiento
	 */
	public Date getFechaMovimiento() {
		return fechaMovimiento;
	}
	/**
	 * @param fechaMovimiento the fechaMovimiento to set
	 */
	public void setFechaMovimiento(Date fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
	}
	
	public Long getIdAgente() {
		return idAgente;
	}
	
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
	
	public Long getAuxiliar() {
		return auxiliar;
	}
	
	public void setAuxiliar(Long auxiliar) {
		this.auxiliar = auxiliar;
	}
	
	public Integer getPctIVAAcreditable() {
		return pctIVAAcreditable;
	}
	
	public void setPctIVAAcreditable(Integer pctIVAAcreditable) {
		this.pctIVAAcreditable = pctIVAAcreditable;
	}
	
	public Integer getPctIVARetenido() {
		return pctIVARetenido;
	}
	
	public void setPctIVARetenido(Integer pctIVARetenido) {
		this.pctIVARetenido = pctIVARetenido;
	}
	
	public Integer getPctISRRetenido() {
		return pctISRRetenido;
	}
	
	public void setPctISRRetenido(Integer pctISRRetenido) {
		this.pctISRRetenido = pctISRRetenido;
	}
	
	public Integer getClaveTipoOperacion() {
		return claveTipoOperacion;
	}
	
	public void setClaveTipoOperacion(Integer claveTipoOperacion) {
		this.claveTipoOperacion = claveTipoOperacion;
	}
	
}
