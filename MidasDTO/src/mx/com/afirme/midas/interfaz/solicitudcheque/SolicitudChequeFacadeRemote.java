package mx.com.afirme.midas.interfaz.solicitudcheque;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas2.dto.calculos.DetalleSolicitudCheque;


public interface SolicitudChequeFacadeRemote {

	public BigDecimal solicitaCheque (SolicitudChequeDTO solicitudCheque, String nombreUsuario)
	throws Exception;
	
	public SolicitudChequeDTO consultaEstatusSolicitudCheque (BigDecimal idPago, String nombreUsuario)
	throws Exception;
	
	public String cancelaSolicitudCheque (BigDecimal idPago, String nombreUsuario)
	throws Exception;
	
	public SolicitudChequeDTO obtenerSolicitudCheque(Long idSolicitudCheque);
	
	public List<DetalleSolicitudCheque> obtenerDetallePorSolicitudCheque(Long idSolicitudCheque);
	
}
