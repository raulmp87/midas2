package mx.com.afirme.midas.interfaz.tipocambio;

import javax.ejb.Remote;
import java.util.Date;


public interface TipoCambioFacadeRemote {

	public Double obtieneTipoCambioPorDia(Date fecha, String nombreUsuario) throws Exception;
	
	public Double obtieneTipoCambioPorMes(Integer mes, Integer anio, String nombreUsuario) throws Exception;
	
}
