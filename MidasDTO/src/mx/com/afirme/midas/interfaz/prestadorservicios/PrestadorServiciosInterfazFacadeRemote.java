package mx.com.afirme.midas.interfaz.prestadorservicios;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;


public interface PrestadorServiciosInterfazFacadeRemote {

	public List<PrestadorServiciosDTO> listarPrestadores(
			PrestadorServiciosDTO prestadorServicios, String nombreUsuario)
			throws Exception;

	public PrestadorServiciosDTO detallePrestador(
			BigDecimal idPrestadorServicios, String nombreUsuario)
			throws Exception;

}
