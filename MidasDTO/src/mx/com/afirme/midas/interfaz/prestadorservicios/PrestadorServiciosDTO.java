package mx.com.afirme.midas.interfaz.prestadorservicios;

import java.io.Serializable;
import java.math.BigDecimal;

public class PrestadorServiciosDTO implements Serializable {

	private static final long serialVersionUID = -504533498761425680L;
	
	private BigDecimal idPrestadorServicios;
	private String nombrePrestador;
	private String tipoPrestador;
	
	public BigDecimal getIdPrestadorServicios() {
		return idPrestadorServicios;
	}
	public void setIdPrestadorServicios(BigDecimal idPrestadorServicios) {
		this.idPrestadorServicios = idPrestadorServicios;
	}
	public String getNombrePrestador() {
		return nombrePrestador;
	}
	public void setNombrePrestador(String nombrePrestador) {
		this.nombrePrestador = nombrePrestador;
	}
	public String getTipoPrestador() {
		return tipoPrestador;
	}
	public void setTipoPrestador(String tipoPrestador) {
		this.tipoPrestador = tipoPrestador;
	}
	
	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof PrestadorServiciosDTO) {
			PrestadorServiciosDTO prestadorServiciosDTO = (PrestadorServiciosDTO) object;
			equal = prestadorServiciosDTO.getIdPrestadorServicios().equals(this.getIdPrestadorServicios());
		} // End of if
		return equal;
	}	
}
