package mx.com.afirme.midas.interfaz.cliente;

import java.math.BigDecimal;
import java.util.Date;
/**
 * Clase para el canal de ventas para clientes
 * @author vmhersil
 *
 */
public class CanalVenta {
	private BigDecimal 	idAgente;
	private	String		claveAgente;
	private	String		nombreAgente;
	private	String		productoEmitido;
	private	Date		fechaUltimaEmision;
	private	Short		idEstatus;
	private	String		estatus;
	public BigDecimal getIdAgente() {
		return idAgente;
	}
	public void setIdAgente(BigDecimal idAgente) {
		this.idAgente = idAgente;
	}
	public String getClaveAgente() {
		return claveAgente;
	}
	public void setClaveAgente(String claveAgente) {
		this.claveAgente = claveAgente;
	}
	public String getNombreAgente() {
		return nombreAgente;
	}
	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}
	public String getProductoEmitido() {
		return productoEmitido;
	}
	public void setProductoEmitido(String productoEmitido) {
		this.productoEmitido = productoEmitido;
	}
	public Date getFechaUltimaEmision() {
		return fechaUltimaEmision;
	}
	public void setFechaUltimaEmision(Date fechaUltimaEmision) {
		this.fechaUltimaEmision = fechaUltimaEmision;
	}
	public Short getIdEstatus() {
		return idEstatus;
	}
	public void setIdEstatus(Short idEstatus) {
		this.idEstatus = idEstatus;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
}
