

package mx.com.afirme.midas.interfaz.cliente;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.dto.ClientBeanValidator;
import mx.com.afirme.midas2.dto.ClientBeanValidator.SectionType;
/**
 * Clase DTO para manejar todo lo relacionado a clientes ya sea persona fisica/moral
 * @author vmhersil
 *
 */
public class ClienteGenericoDTO extends ClienteDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//Esta variable se utiliza para cargar los datos de configuracion de campos requeridos por negocio
	private String 		idClienteString;
	private Long		idClienteCob;
	private String		idToPersonaString;
	private String 		claveTipoPersonaString;
	private String		idEstadoNacimientoString;
	private String 		idToDireccionString;
	private String		idColoniaString;
	private String    	idColoniaStringDom;
	private String 		idMunicipioString;
	private String 		idEstadoString;
	private String 		tipoSituacionString;
	private String		idPaisString;
	private String 		nombreCompleto;
	private Long 		idNegocio;
	private Long 		idDomicilioConsulta;
	//#####Datos Personales################
	//Persona Fisica
	private Short 		tipoSituacion;
	private String 		estadoCivil;
	
	@ClientBeanValidator(mappingLabel="Colonia Diferente",seccion=SectionType.DATOS_GENERALES,modelView="cliente")
	private String 		nombreColoniaDiferente;
	//Persona Moral
	@ClientBeanValidator(mappingLabel="Sector",seccion=SectionType.DATOS_GENERALES,modelView="cliente")
	private String 		claveSectorFinanciero;
	private String		nombreSectorFinanciero;
	
	@ClientBeanValidator(mappingLabel="Razon social",seccion=SectionType.DATOS_GENERALES,modelView="cliente")
	private String 		razonSocial;
	
	@ClientBeanValidator(mappingLabel="Fecha de constitucion",seccion=SectionType.DATOS_GENERALES,modelView="cliente")
	private Date 		fechaConstitucion;
	
	private String 		fechaConstitucionString;
	
	@ClientBeanValidator(mappingLabel="Giro",seccion=SectionType.DATOS_GENERALES,modelView="cliente")
	private String 		idGiro;
	
	@ClientBeanValidator(mappingLabel="Representante Legal",seccion=SectionType.DATOS_GENERALES,modelView="cliente")
	private BigDecimal idRepresentante;
	//#####Datos de Contacto################
	//Persona fisica
	@ClientBeanValidator(mappingLabel="Celular",seccion=SectionType.DATOS_CONTACTO,modelView="cliente")
	private String 		celularContacto;
	
	@ClientBeanValidator(mappingLabel="Telefono oficina",seccion=SectionType.DATOS_CONTACTO,modelView="cliente")
	private String 		telefonoOficinaContacto;
	
	@ClientBeanValidator(mappingLabel="Extension",seccion=SectionType.DATOS_CONTACTO,modelView="cliente")
	private String 		extensionContacto;
	
	@ClientBeanValidator(mappingLabel="Fax",seccion=SectionType.DATOS_CONTACTO,modelView="cliente")
	private String 		faxContacto;
	
	@ClientBeanValidator(mappingLabel="Facebook",seccion=SectionType.DATOS_CONTACTO,modelView="cliente")
	private String 		facebookContacto;
	
	@ClientBeanValidator(mappingLabel="Twitter",seccion=SectionType.DATOS_CONTACTO,modelView="cliente")
	private String		twitterContacto;
	
	@ClientBeanValidator(mappingLabel="Pagina Web",seccion=SectionType.DATOS_CONTACTO,modelView="cliente")
	private String 		paginaWebContacto;
	
	@ClientBeanValidator(mappingLabel="Telefonos adicionales",seccion=SectionType.DATOS_CONTACTO,modelView="cliente")
	private String 		telefonosAdicionalesContacto;
	
	@ClientBeanValidator(mappingLabel="Correos adicionales",seccion=SectionType.DATOS_CONTACTO,modelView="cliente")
	private String 		correosAdicionalesContacto;
	//Persona Moral
	@ClientBeanValidator(mappingLabel="Puesto del contacto",seccion=SectionType.DATOS_CONTACTO,modelView="cliente")
	private String 		puestoContacto;
	
	@ClientBeanValidator(mappingLabel="Nombre del contacto",seccion=SectionType.DATOS_CONTACTO,modelView="cliente")
	private	String 		nombreContacto;
	//#####Datos Fiscales###################
	//Persona Fisica
	@ClientBeanValidator(mappingLabel="Nombre",seccion=SectionType.DATOS_FISCALES,modelView="cliente")
	private String 		nombreFiscal;
	
	@ClientBeanValidator(mappingLabel="Apellido paterno",seccion=SectionType.DATOS_FISCALES,modelView="cliente")
	private	String		apellidoPaternoFiscal;
	
	@ClientBeanValidator(mappingLabel="Apellido materno",seccion=SectionType.DATOS_FISCALES,modelView="cliente")
	private String 		apellidoMaternoFiscal;
	
	@ClientBeanValidator(mappingLabel="Calle y numero Fiscal",seccion=SectionType.DATOS_FISCALES,modelView="cliente")
	private String		nombreCalleFiscal;
	
	@ClientBeanValidator(mappingLabel="Codigo postal Fiscal",seccion=SectionType.DATOS_FISCALES,modelView="cliente")
	private	String		codigoPostalFiscal;
	
	@ClientBeanValidator(mappingLabel="Estado Fiscal",seccion=SectionType.DATOS_FISCALES,modelView="cliente")
	private	String		idEstadoFiscal;
	
	@ClientBeanValidator(mappingLabel="Municipio Fiscal",seccion=SectionType.DATOS_FISCALES,modelView="cliente")
	private String		idMunicipioFiscal;
	
	@ClientBeanValidator(mappingLabel="Colonia",seccion=SectionType.DATOS_FISCALES,modelView="cliente")
	private String		idColoniaFiscal;
	private String		nombreColoniaFiscal;
	
	@ClientBeanValidator(mappingLabel="RFC Fiscal",seccion=SectionType.DATOS_FISCALES,modelView="cliente")
	private String		codigoRFCFiscal;
	
	@ClientBeanValidator(mappingLabel="Telefono Fiscal",seccion=SectionType.DATOS_FISCALES,modelView="cliente")
	private String		telefonoFiscal;
	private String  	idPersonaFiscal;
	
	@ClientBeanValidator(mappingLabel="Colonia Diferente Fiscal",seccion=SectionType.DATOS_FISCALES,modelView="cliente")
	private String 		nombreColoniaDiferenteFiscal;
	//Persona Moral
	@ClientBeanValidator(mappingLabel="Fecha de constitucion",seccion=SectionType.DATOS_FISCALES,modelView="cliente")
	private Date		fechaNacimientoFiscal;
	
	@ClientBeanValidator(mappingLabel="Razon Social",seccion=SectionType.DATOS_GENERALES,modelView="cliente")
	private String		razonSocialFiscal;
	//#####Datos de Cobranza################
	//Persona Fisica y Moral
	@ClientBeanValidator(mappingLabel="Nombre del tarjetahabiente",seccion=SectionType.DATOS_COBRANZA,modelView="cliente")
	private String		nombreTarjetaHabienteCobranza;
	
	@ClientBeanValidator(mappingLabel="RFC Cobranza",seccion=SectionType.DATOS_COBRANZA,modelView="cliente")
	private String		codigoRFCCobranza;
	
	@ClientBeanValidator(mappingLabel="Codigo postal Cobranza",seccion=SectionType.DATOS_COBRANZA,modelView="cliente")
	private	String		codigoPostalCobranza;
	
	@ClientBeanValidator(mappingLabel="Estado Cobranza",seccion=SectionType.DATOS_COBRANZA,modelView="cliente")
	private	String		idEstadoCobranza;
	
	@ClientBeanValidator(mappingLabel="Ciudad Cobranza",seccion=SectionType.DATOS_COBRANZA,modelView="cliente")
	private String		idMunicipioCobranza;
	
	@ClientBeanValidator(mappingLabel="Colonia Cobranza",seccion=SectionType.DATOS_COBRANZA,modelView="cliente")
	private String		idColoniaCobranza;
	private String		nombreColoniaCobranza;
	
	@ClientBeanValidator(mappingLabel="Calle y numero Cobranza",seccion=SectionType.DATOS_COBRANZA,modelView="cliente")
	private String		nombreCalleCobranza;
	
	@ClientBeanValidator(mappingLabel="Telefono Cobranza",seccion=SectionType.DATOS_COBRANZA,modelView="cliente")
	private String		telefonoCobranza;
	
	@ClientBeanValidator(mappingLabel="Correo electronico Cobranza",seccion=SectionType.DATOS_COBRANZA,modelView="cliente")
	private String		emailCobranza;
	
	@ClientBeanValidator(mappingLabel="Institucion bancaria",seccion=SectionType.DATOS_COBRANZA,modelView="cliente")
	private BigDecimal	 	idBancoCobranza;
	
	@ClientBeanValidator(mappingLabel="Tipo de tarjeta",seccion=SectionType.DATOS_COBRANZA,modelView="cliente")
	private String	 	idTipoTarjetaCobranza;
	
	@ClientBeanValidator(mappingLabel="Numero de tarjeta",seccion=SectionType.DATOS_COBRANZA,modelView="cliente")
	private String		numeroTarjetaCobranza;
	
	@ClientBeanValidator(mappingLabel="Codigo de seguridad",seccion=SectionType.DATOS_COBRANZA,modelView="cliente")
	private String 		codigoSeguridadCobranza;
	
	
	private	String		fechaVencimientoTarjetaCobranza;
	
	@ClientBeanValidator(mappingLabel="Fecha de vencimiento mes",seccion=SectionType.DATOS_COBRANZA,modelView="cliente")
	private Integer 	mesFechaVencimiento;
	
	@ClientBeanValidator(mappingLabel="Fecha de vencimiento anio",seccion=SectionType.DATOS_COBRANZA,modelView="cliente")
	private Integer		anioFechaVencimiento;
	
	@ClientBeanValidator(mappingLabel="Dia de pago",seccion=SectionType.DATOS_COBRANZA,modelView="cliente")
	private	BigDecimal	diaPagoTarjetaCobranza;
	
	@ClientBeanValidator(mappingLabel="Tipo de Domiciliacion",seccion=SectionType.DATOS_COBRANZA,modelView="cliente")
	private Integer		idTipoDomiciliacion;
	
	@ClientBeanValidator(mappingLabel="Numero de Cuenta",seccion=SectionType.DATOS_COBRANZA,modelView="cliente")
	private String numeroCuenta;
	
	private Long idConductoCobranza;
	private Integer idVersionConducto;
	private Integer idTipoConductoCobro;
	private String tipoPromocion;
	private String rfcCobranza;
	private String cuentaNoEncriptada;
	
	
	
	//####Datos de Aviso de Siniestros######
	/*
	private	List<ContactoSiniestro> contactosSiniestro;
	private List<CanalVenta>	canalesVenta;
	*/
	@ClientBeanValidator(mappingLabel="Celular",seccion=SectionType.AVISOS_SINIESTROS,modelView="cliente")
	private	String		celularAviso;
	
	@ClientBeanValidator(mappingLabel="Telefono oficina Aviso",seccion=SectionType.AVISOS_SINIESTROS,modelView="cliente")
	private String		telefonoOficinaAviso;
	
	@ClientBeanValidator(mappingLabel="Telefono casa Aviso",seccion=SectionType.AVISOS_SINIESTROS,modelView="cliente")
	private String		telefonoCasaAviso;
	
	@ClientBeanValidator(mappingLabel="Nombre Aviso",seccion=SectionType.AVISOS_SINIESTROS,modelView="cliente")
	private	String		nombreAviso;
	
	@ClientBeanValidator(mappingLabel="Parentesco",seccion=SectionType.AVISOS_SINIESTROS,modelView="cliente")
	private	String		parentescoAviso;
	
	@ClientBeanValidator(mappingLabel="Extension Aviso",seccion=SectionType.AVISOS_SINIESTROS,modelView="cliente")
	private String 		extensionAviso;
	
	@ClientBeanValidator(mappingLabel="Correo electronico Aviso",seccion=SectionType.AVISOS_SINIESTROS,modelView="cliente")
	private String		correoElectronicoAviso;
	
	private List<DireccionCliente> direcciones;
	
	public Short getTipoSituacion() {
		return tipoSituacion;
	}
	public void setTipoSituacion(Short tipoSituacion) {
		this.tipoSituacion = tipoSituacion;
	}
	public String getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public String getNombreFiscal() {
		return nombreFiscal;
	}
	public void setNombreFiscal(String nombreFiscal) {
		this.nombreFiscal = nombreFiscal;
	}
	public String getApellidoPaternoFiscal() {
		return apellidoPaternoFiscal;
	}
	public void setApellidoPaternoFiscal(String apellidoPaternoFiscal) {
		this.apellidoPaternoFiscal = apellidoPaternoFiscal;
	}
	public String getApellidoMaternoFiscal() {
		return apellidoMaternoFiscal;
	}
	public void setApellidoMaternoFiscal(String apellidoMaternoFiscal) {
		this.apellidoMaternoFiscal = apellidoMaternoFiscal;
	}
	public String getNombreCalleFiscal() {
		return nombreCalleFiscal;
	}
	public void setNombreCalleFiscal(String nombreCalleFiscal) {
		this.nombreCalleFiscal = nombreCalleFiscal;
	}
	public String getCodigoPostalFiscal() {
		return codigoPostalFiscal;
	}
	public void setCodigoPostalFiscal(String codigoPostalFiscal) {
		this.codigoPostalFiscal = codigoPostalFiscal;
	}
	public String getCodigoRFCFiscal() {
		return codigoRFCFiscal;
	}
	public void setCodigoRFCFiscal(String codigoRFCFiscal) {
		this.codigoRFCFiscal = codigoRFCFiscal;
	}
	public String getTelefonoFiscal() {
		return telefonoFiscal;
	}
	public void setTelefonoFiscal(String telefonoFiscal) {
		this.telefonoFiscal = telefonoFiscal;
	}
	public Date getFechaNacimientoFiscal() {
		return fechaNacimientoFiscal;
	}
	public void setFechaNacimientoFiscal(Date fechaNacimientoFiscal) {
		this.fechaNacimientoFiscal = fechaNacimientoFiscal;
	}
	public String getCodigoRFCCobranza() {
		return codigoRFCCobranza;
	}
	public void setCodigoRFCCobranza(String codigoRFCCobranza) {
		this.codigoRFCCobranza = codigoRFCCobranza;
	}
	public String getCodigoPostalCobranza() {
		return codigoPostalCobranza;
	}
	public void setCodigoPostalCobranza(String codigoPostalCobranza) {
		this.codigoPostalCobranza = codigoPostalCobranza;
	}
	public String getNombreCalleCobranza() {
		return nombreCalleCobranza;
	}
	public void setNombreCalleCobranza(String nombreCalleCobranza) {
		this.nombreCalleCobranza = nombreCalleCobranza;
	}
	public String getTelefonoCobranza() {
		return telefonoCobranza;
	}
	public void setTelefonoCobranza(String telefonoCobranza) {
		this.telefonoCobranza = telefonoCobranza;
	}
	public String getEmailCobranza() {
		return emailCobranza;
	}
	public void setEmailCobranza(String emailCobranza) {
		this.emailCobranza = emailCobranza;
	}
	public Long getIdNegocio() {
		return idNegocio;
	}
	public void setIdNegocio(Long idNegocio) {
		this.idNegocio = idNegocio;
	}
	public String getCelularContacto() {
		return celularContacto;
	}
	public void setCelularContacto(String celularContacto) {
		this.celularContacto = celularContacto;
	}
	public String getTelefonoOficinaContacto() {
		return telefonoOficinaContacto;
	}
	public void setTelefonoOficinaContacto(String telefonoOficinaContacto) {
		this.telefonoOficinaContacto = telefonoOficinaContacto;
	}
	public String getExtensionContacto() {
		return extensionContacto;
	}
	public void setExtensionContacto(String extensionContacto) {
		this.extensionContacto = extensionContacto;
	}
	public String getFaxContacto() {
		return faxContacto;
	}
	public void setFaxContacto(String faxContacto) {
		this.faxContacto = faxContacto;
	}
	public String getFacebookContacto() {
		return facebookContacto;
	}
	public void setFacebookContacto(String facebookContacto) {
		this.facebookContacto = facebookContacto;
	}
	public String getTwitterContacto() {
		return twitterContacto;
	}
	public void setTwitterContacto(String twitterContacto) {
		this.twitterContacto = twitterContacto;
	}
	public String getPaginaWebContacto() {
		return paginaWebContacto;
	}
	public void setPaginaWebContacto(String paginaWebContacto) {
		this.paginaWebContacto = paginaWebContacto;
	}
	public String getTelefonosAdicionalesContacto() {
		return telefonosAdicionalesContacto;
	}
	public void setTelefonosAdicionalesContacto(String telefonosAdicionalesContacto) {
		this.telefonosAdicionalesContacto = telefonosAdicionalesContacto;
	}
	public String getCorreosAdicionalesContacto() {
		return correosAdicionalesContacto;
	}
	public void setCorreosAdicionalesContacto(String correosAdicionalesContacto) {
		this.correosAdicionalesContacto = correosAdicionalesContacto;
	}
	public String getPuestoContacto() {
		return puestoContacto;
	}
	public void setPuestoContacto(String puestoContacto) {
		this.puestoContacto = puestoContacto;
	}
	public String getNombreContacto() {
		return nombreContacto;
	}
	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}
	public String getNombreTarjetaHabienteCobranza() {
		return nombreTarjetaHabienteCobranza;
	}
	public void setNombreTarjetaHabienteCobranza(
			String nombreTarjetaHabienteCobranza) {
		this.nombreTarjetaHabienteCobranza = nombreTarjetaHabienteCobranza;
	}
	public String getNumeroTarjetaCobranza() {
		return numeroTarjetaCobranza;
	}
	public void setNumeroTarjetaCobranza(String numeroTarjetaCobranza) {
		this.numeroTarjetaCobranza = numeroTarjetaCobranza;
	}
	public String getCodigoSeguridadCobranza() {
		return codigoSeguridadCobranza;
	}
	public void setCodigoSeguridadCobranza(String codigoSeguridadCobranza) {
		this.codigoSeguridadCobranza = codigoSeguridadCobranza;
	}
	public String getFechaVencimientoTarjetaCobranza() {
		return fechaVencimientoTarjetaCobranza;
	}
	public void setFechaVencimientoTarjetaCobranza(
			String fechaVencimientoTarjetaCobranza) {
		this.fechaVencimientoTarjetaCobranza = fechaVencimientoTarjetaCobranza;
	}
	public String getNombreColoniaFiscal() {
		return nombreColoniaFiscal;
	}
	public void setNombreColoniaFiscal(String nombreColoniaFiscal) {
		this.nombreColoniaFiscal = nombreColoniaFiscal;
	}
	public String getClaveSectorFinanciero() {
		return claveSectorFinanciero;
	}
	public void setClaveSectorFinanciero(String claveSectorFinanciero) {
		this.claveSectorFinanciero = claveSectorFinanciero;
	}
	public String getNombreSectorFinanciero() {
		return nombreSectorFinanciero;
	}
	public void setNombreSectorFinanciero(String nombreSectorFinanciero) {
		this.nombreSectorFinanciero = nombreSectorFinanciero;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public String getNombreColoniaCobranza() {
		return nombreColoniaCobranza;
	}
	public void setNombreColoniaCobranza(String nombreColoniaCobranza) {
		this.nombreColoniaCobranza = nombreColoniaCobranza;
	}
	public String getTipoSituacionString() {
		return tipoSituacionString;
	}
	public void setTipoSituacionString(String tipoSituacionString) {
		this.tipoSituacionString = tipoSituacionString;
	}
	public String getClaveTipoPersonaString() {
		return claveTipoPersonaString;
	}
	public void setClaveTipoPersonaString(String claveTipoPersonaString) {
		this.claveTipoPersonaString = claveTipoPersonaString;
	}
	public String getIdClienteString() {
		return idClienteString;
	}
	public void setIdClienteString(String idClienteString) {
		this.idClienteString = idClienteString;
	}
	public String getIdToPersonaString() {
		return idToPersonaString;
	}
	public void setIdToPersonaString(String idToPersonaString) {
		this.idToPersonaString = idToPersonaString;
	}
	public String getIdEstadoNacimientoString() {
		return idEstadoNacimientoString;
	}
	public void setIdEstadoNacimientoString(String idEstadoNacimientoString) {
		this.idEstadoNacimientoString = idEstadoNacimientoString;
	}
	public String getIdToDireccionString() {
		return idToDireccionString;
	}
	public void setIdToDireccionString(String idToDireccionString) {
		this.idToDireccionString = idToDireccionString;
	}
	public String getIdColoniaString() {
		return idColoniaString;
	}
	public void setIdColoniaString(String idColoniaString) {
		this.idColoniaString = idColoniaString;
	}
	public String getIdColoniaStringDom() {
		return idColoniaStringDom;
	}
	public void setIdColoniaStringDom(String idColoniaStringDom) {
		this.idColoniaStringDom = idColoniaStringDom;
	}
	public String getIdMunicipioString() {
		return (getIdMunicipio()!=null)?getIdMunicipio().toPlainString():idMunicipioString;
	}
	public void setIdMunicipioString(String idMunicipioString) {
		this.idMunicipioString = idMunicipioString;
	}
	public String getIdEstadoString() {
		return (getIdEstado()!=null)?getIdEstado().toPlainString():idEstadoString;
	}
	public void setIdEstadoString(String idEstadoString) {
		this.idEstadoString = idEstadoString;
	}
	public String getIdGiro() {
		return idGiro;
	}
	public void setIdGiro(String idGiro) {
		this.idGiro = idGiro;
	}
	public String getIdEstadoFiscal() {
		return idEstadoFiscal;
	}
	public void setIdEstadoFiscal(String idEstadoFiscal) {
		this.idEstadoFiscal = idEstadoFiscal;
	}
	public String getIdMunicipioFiscal() {
		return idMunicipioFiscal;
	}
	public void setIdMunicipioFiscal(String idMunicipioFiscal) {
		this.idMunicipioFiscal = idMunicipioFiscal;
	}
	public String getIdColoniaFiscal() {
		return idColoniaFiscal;
	}
	public void setIdColoniaFiscal(String idColoniaFiscal) {
		this.idColoniaFiscal = idColoniaFiscal;
	}
	public String getIdPersonaFiscal() {
		return idPersonaFiscal;
	}
	public void setIdPersonaFiscal(String idPersonaFiscal) {
		this.idPersonaFiscal = idPersonaFiscal;
	}
	public String getIdEstadoCobranza() {
		return idEstadoCobranza;
	}
	public void setIdEstadoCobranza(String idEstadoCobranza) {
		this.idEstadoCobranza = idEstadoCobranza;
	}
	public String getIdMunicipioCobranza() {
		return idMunicipioCobranza;
	}
	public void setIdMunicipioCobranza(String idMunicipioCobranza) {
		this.idMunicipioCobranza = idMunicipioCobranza;
	}
	public String getIdColoniaCobranza() {
		return idColoniaCobranza;
	}
	public void setIdColoniaCobranza(String idColoniaCobranza) {
		this.idColoniaCobranza = idColoniaCobranza;
	}
	public BigDecimal getIdBancoCobranza() {
		return idBancoCobranza;
	}
	public void setIdBancoCobranza(BigDecimal idBancoCobranza) {
		this.idBancoCobranza = idBancoCobranza;
	}
	public String getIdTipoTarjetaCobranza() {
		return idTipoTarjetaCobranza;
	}
	public void setIdTipoTarjetaCobranza(String idTipoTarjetaCobranza) {
		this.idTipoTarjetaCobranza = idTipoTarjetaCobranza;
	}
	public BigDecimal getDiaPagoTarjetaCobranza() {
		return diaPagoTarjetaCobranza;
	}
	public void setDiaPagoTarjetaCobranza(BigDecimal diaPagoTarjetaCobranza) {
		this.diaPagoTarjetaCobranza = diaPagoTarjetaCobranza;
	}
	public String getIdPaisString() {
		return idPaisString;
	}
	public void setIdPaisString(String idPaisString) {
		this.idPaisString = idPaisString;
	}
	public String getNombreColoniaDiferente() {
		return nombreColoniaDiferente;
	}
	public void setNombreColoniaDiferente(String nombreColoniaDiferente) {
		this.nombreColoniaDiferente = nombreColoniaDiferente;
	}
	public String getNombreColoniaDiferenteFiscal() {
		return nombreColoniaDiferenteFiscal;
	}
	public void setNombreColoniaDiferenteFiscal(String nombreColoniaDiferenteFiscal) {
		this.nombreColoniaDiferenteFiscal = nombreColoniaDiferenteFiscal;
	}
	public Date getFechaConstitucion() {
		return fechaConstitucion;
	}
	public void setFechaConstitucion(Date fechaConstitucion) {
		this.fechaConstitucion = fechaConstitucion;
	}
	public String getRazonSocialFiscal() {
		return razonSocialFiscal;
	}
	public void setRazonSocialFiscal(String razonSocialFiscal) {
		this.razonSocialFiscal = razonSocialFiscal;
	}
	public Integer getMesFechaVencimiento() {
		return mesFechaVencimiento;
	}
	public void setMesFechaVencimiento(Integer mesFechaVencimiento) {
		this.mesFechaVencimiento = mesFechaVencimiento;
	}
	public Integer getAnioFechaVencimiento() {
		return anioFechaVencimiento;
	}
	public void setAnioFechaVencimiento(Integer anioFechaVencimiento) {
		this.anioFechaVencimiento = anioFechaVencimiento;
	}
	public Integer getIdTipoDomiciliacion() {
		return idTipoDomiciliacion;
	}
	public void setIdTipoDomiciliacion(Integer idTipoDomiciliacion) {
		this.idTipoDomiciliacion = idTipoDomiciliacion;
	}
	public String getCelularAviso() {
		return celularAviso;
	}
	public void setCelularAviso(String celularAviso) {
		this.celularAviso = celularAviso;
	}
	public String getTelefonoOficinaAviso() {
		return telefonoOficinaAviso;
	}
	public void setTelefonoOficinaAviso(String telefonoOficinaAviso) {
		this.telefonoOficinaAviso = telefonoOficinaAviso;
	}
	public String getTelefonoCasaAviso() {
		return telefonoCasaAviso;
	}
	public void setTelefonoCasaAviso(String telefonoCasaAviso) {
		this.telefonoCasaAviso = telefonoCasaAviso;
	}
	public String getNombreAviso() {
		return nombreAviso;
	}
	public void setNombreAviso(String nombreAviso) {
		this.nombreAviso = nombreAviso;
	}
	public String getParentescoAviso() {
		return parentescoAviso;
	}
	public void setParentescoAviso(String parentescoAviso) {
		this.parentescoAviso = parentescoAviso;
	}
	public String getExtensionAviso() {
		return extensionAviso;
	}
	public void setExtensionAviso(String extensionAviso) {
		this.extensionAviso = extensionAviso;
	}
	public String getCorreoElectronicoAviso() {
		return correoElectronicoAviso;
	}
	public void setCorreoElectronicoAviso(String correoElectronicoAviso) {
		this.correoElectronicoAviso = correoElectronicoAviso;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	public BigDecimal getIdRepresentante() {
		return idRepresentante;
	}
	public void setIdRepresentante(BigDecimal idRepresentante) {
		this.idRepresentante = idRepresentante;
	}
	public Long getIdDomicilioConsulta() {
		return idDomicilioConsulta;
	}
	public void setIdDomicilioConsulta(Long idDomicilioConsulta) {
		this.idDomicilioConsulta = idDomicilioConsulta;
	}
	public String getFechaConstitucionString() {
		return fechaConstitucionString;
	}
	public void setFechaConstitucionString(String fechaConstitucionString) {
		this.fechaConstitucionString = fechaConstitucionString;
	}
	public Long getIdClienteCob() {
		return idClienteCob;
	}
	public void setIdClienteCob(Long idClienteCob) {
		this.idClienteCob = idClienteCob;
	}
	public Long getIdConductoCobranza() {
		return idConductoCobranza;
	}
	public void setIdConductoCobranza(Long idConductoCobranza) {
		this.idConductoCobranza = idConductoCobranza;
	}
	public Integer getIdVersionConducto() {
		return idVersionConducto;
	}
	public void setIdVersionConducto(Integer idVersionConducto) {
		this.idVersionConducto = idVersionConducto;
	}
	public Integer getIdTipoConductoCobro() {
		return idTipoConductoCobro;
	}
	public void setIdTipoConductoCobro(Integer idTipoConductoCobro) {
		this.idTipoConductoCobro = idTipoConductoCobro;
	}
	public String getTipoPromocion() {
		return tipoPromocion;
	}
	public void setTipoPromocion(String tipoPromocion) {
		this.tipoPromocion = tipoPromocion;
	}
	public String getRfcCobranza() {
		return rfcCobranza;
	}
	public void setRfcCobranza(String rfcCobranza) {
		this.rfcCobranza = rfcCobranza;
	}
	public String getCuentaNoEncriptada() {
		return cuentaNoEncriptada;
	}
	public void setCuentaNoEncriptada(String cuentaNoEncriptada) {
		this.cuentaNoEncriptada = cuentaNoEncriptada;
	}

	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	
	/**
	 * @return the direcciones
	 */
	public List<DireccionCliente> getDirecciones() {
		return direcciones;
	}
	
	/**
	 * @param direcciones the direcciones to set
	 */
	public void setDirecciones(List<DireccionCliente> direcciones) {
		this.direcciones = direcciones;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("idClienteString: " + this.idClienteString + "");
		sb.append("idToPersonaString: " + this.idToPersonaString + "");
		sb.append("idToDireccionString: " + this.idToDireccionString);
		sb.append("]");
		
		return sb.toString();
	}
}
