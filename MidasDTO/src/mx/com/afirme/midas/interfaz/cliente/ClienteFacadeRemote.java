package mx.com.afirme.midas.interfaz.cliente;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


import mx.com.afirme.midas.sistema.ClientSystemException;


public interface ClienteFacadeRemote {
	
	public static final short CLAVE_PERSONA_FISICA = 1;
	public static final short CLAVE_PERSONA_MORAL = 2;

	 BigDecimal save(ClienteDTO entity, String nombreUsuario)
			throws Exception;

	 ClienteDTO saveV2(ClienteDTO entity, String nombreUsuario)
			throws Exception;

	 BigDecimal update(ClienteDTO entity, String nombreUsuario)
			throws Exception;

	 ClienteDTO findById(BigDecimal idCliente, String nombreUsuario)
			throws Exception;

	 List<ClienteDTO> findByProperty(String propertyName, Object value,
			String nombreUsuario) throws Exception;

	 List<ClienteDTO> listarFiltrado(ClienteDTO entity,
			String nombreUsuario) throws Exception;

	 List<ClienteDTO> buscarPorCURP(ClienteDTO entity,
			String nombreUsuario) throws Exception;
	
	 ClienteGenericoDTO saveFullData(ClienteGenericoDTO entity, String nombreUsuario, boolean validaNegocio)
	throws Exception;
	
	 Map<String, Map<String,String>> validateClientFields(ClienteGenericoDTO cliente,boolean validaNegocio) throws ClientSystemException,Exception;
	
	 Long guardarDatosCobranza(ClienteGenericoDTO entity,String nombreUsuario, boolean validaNegocio)throws Exception;
	
	 void guardarDatosAvisoSiniestro(ClienteGenericoDTO entity,String nombreUsuario,boolean validaNegocio)throws Exception;
	
	 List<String> obtenerCamposRequeridos(ClienteGenericoDTO entity)throws Exception;
	
	 List<ClienteGenericoDTO> findByFilters(ClienteGenericoDTO filtro,String usuario) throws Exception;
	
	 List<ClienteGenericoDTO> findByIdRFC(ClienteGenericoDTO filtro,String usuario) throws Exception;
	
	 List<ClienteGenericoDTO> findByFiltersNoAddress(ClienteGenericoDTO filtro,String usuario) throws Exception;
	
	 ClienteGenericoDTO loadById(ClienteGenericoDTO filtro) throws Exception;
	
	 ClienteGenericoDTO loadByIdNoAddress(ClienteGenericoDTO filtro) throws Exception;
	
	 List<ClienteGenericoDTO> loadMediosPagoPorCliente(ClienteGenericoDTO cliente,String usuario) throws Exception;
	
	 ClienteGenericoDTO getConductoCobro(Long idCliente, Long idConductoCobro) throws Exception;
	
	 BigDecimal getDomicilioPorCliente(BigDecimal idCliente);
	
	 ClienteGenericoDTO getRFCPorCliente(BigDecimal idCliente);

	 void obtenerDatosTarjeta(BigDecimal idToPersonaContratante, Integer idTipoConductoCobro, String usuario);
	
}
