/**
 * 
 */
package mx.com.afirme.midas.interfaz.cliente;

import java.io.Serializable;

/**
 * @author simaveras
 *Maneja las direcciones del clientes ya sea persona fisica/moral
 *
 */
public class DireccionCliente implements Serializable{

	private static final long serialVersionUID = 6550475796625435795L;

	private String id;
	private String tipoDireccion;
	private String pais;
	private String estado;
	private String municipio;
	private String colonia;
	private String calleNumero;
	private String codigoPostal;
	
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * @return the tipoDireccion
	 */
	public String getTipoDireccion() {
		return tipoDireccion;
	}
	/**
	 * @param tipoDireccion the tipoDireccion to set
	 */
	public void setTipoDireccion(String tipoDireccion) {
		this.tipoDireccion = tipoDireccion;
	}
	/**
	 * @return the pais
	 */
	public String getPais() {
		return pais;
	}
	/**
	 * @param pais the pais to set
	 */
	public void setPais(String pais) {
		this.pais = pais;
	}
	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/**
	 * @return the municipio
	 */
	public String getMunicipio() {
		return municipio;
	}
	/**
	 * @param municipio the municipio to set
	 */
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	/**
	 * @return the colonia
	 */
	public String getColonia() {
		return colonia;
	}
	/**
	 * @param colonia the colonia to set
	 */
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	/**
	 * @return the calleNumero
	 */
	public String getCalleNumero() {
		return calleNumero;
	}
	/**
	 * @param calleNumero the calleNumero to set
	 */
	public void setCalleNumero(String calleNumero) {
		this.calleNumero = calleNumero;
	}
	/**
	 * @return the codigoPostal
	 */
	public String getCodigoPostal() {
		return codigoPostal;
	}
	/**
	 * @param codigoPostal the codigoPostal to set
	 */
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
}
