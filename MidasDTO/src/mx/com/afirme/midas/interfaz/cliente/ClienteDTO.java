package mx.com.afirme.midas.interfaz.cliente;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas2.dto.ClientBeanValidator;
import mx.com.afirme.midas2.dto.ClientBeanValidator.SectionType;

public class ClienteDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private  BigDecimal idCliente;
	// Datos Persona
	//@ClientBeanValidator(mappingLabel="Tipo de Persona",seccion=SectionType.DATOS_GENERALES,modelView="cliente")
	private  BigDecimal idToPersona;
	@ClientBeanValidator(mappingLabel="Tipo de Persona",seccion=SectionType.DATOS_GENERALES,modelView="cliente")
	private Short claveTipoPersona;
	
	@ClientBeanValidator(mappingLabel="Nombre",seccion=SectionType.DATOS_GENERALES,modelView="cliente")
	private String nombre;
	
	@ClientBeanValidator(mappingLabel="Apellido Paterno",errorMessage="midas.cliente.datosGenerales.error.apellidoPaterno",seccion=SectionType.DATOS_GENERALES,modelView="cliente")
	private String apellidoPaterno;
	
	@ClientBeanValidator(mappingLabel="Apellido Materno",seccion=SectionType.DATOS_GENERALES,modelView="cliente")
	private String apellidoMaterno;
	
	@ClientBeanValidator(mappingLabel="Fecha de nacimiento",seccion=SectionType.DATOS_GENERALES,modelView="cliente")
	private Date fechaNacimiento;
	
	@ClientBeanValidator(mappingLabel="RFC",seccion=SectionType.DATOS_GENERALES,modelView="cliente")
	private String codigoRFC;
	
	@ClientBeanValidator(mappingLabel="Clave Telefono casa",seccion=SectionType.DATOS_CONTACTO,modelView="cliente")
	private String claveTelefono;
	
	@ClientBeanValidator(mappingLabel="Clave2 Telefono casa",seccion=SectionType.DATOS_CONTACTO,modelView="cliente")
	private String clave2Telefono;
	
	@ClientBeanValidator(mappingLabel="Numero Telefono casa",seccion=SectionType.DATOS_CONTACTO,modelView="cliente")
	private String numeroTelefono;
	
	private final String separadorTel = "-";
	
	@ClientBeanValidator(mappingLabel="Telefono casa",seccion=SectionType.DATOS_CONTACTO,modelView="cliente")
	private String telefono;//telefono casa
	
	@ClientBeanValidator(mappingLabel="Correo electronico",seccion=SectionType.DATOS_CONTACTO,modelView="cliente")
	private String email;
	private String estadoNacimiento;
	private  BigDecimal idEstadoNacimiento;
	
	@ClientBeanValidator(mappingLabel="CURP",seccion=SectionType.DATOS_GENERALES,modelView="cliente")
	private String codigoCURP;
	
	@ClientBeanValidator(mappingLabel="Sexo",seccion=SectionType.DATOS_GENERALES,modelView="cliente")
	private String sexo;
	
	/*
	 * Campos usados exclusivamente para el cotizador Casa
	 */
	@ClientBeanValidator(mappingLabel="Nacionalidad",seccion=SectionType.DATOS_GENERALES,modelView="cliente")
	private String claveNacionalidad;//clave del pais de nacimiento "PAMEXI", por default para Midas en el SP
	
	private String claveOcupacion;//ocupacion del cliente, no se usa actualmente en Midas. Se implement� para el cotizador Casa
	//@ClientBeanValidator(mappingLabel="Ocupacion",seccion=SectionType.DATOS_GENERALES,modelView="cliente")
	private String descripcionOcupacion;
	@ClientBeanValidator(mappingLabel="Estado de nacimiento",seccion=SectionType.DATOS_GENERALES,modelView="cliente")
	private String claveEstadoNacimiento;//"id" del estado de nacimiento, representado con una cadena de cinco caracteres, usada para el cotizador Casa
	@ClientBeanValidator(mappingLabel="Ciudad de nacimiento",seccion=SectionType.DATOS_GENERALES,modelView="cliente")
	private String claveCiudadNacimiento;//"id" de la ciudad de nacimiento, representado con una cadena de cinco caracteres, usada para el cotizador Casa
		
	// Datos Direccion
	private  BigDecimal idToDireccion;
	private  BigDecimal idDomicilio;
	
	@ClientBeanValidator(mappingLabel="Calle y numero",seccion=SectionType.DATOS_GENERALES,modelView="cliente")
	private String nombreCalle; //Calle y numero
	private  BigDecimal numeroExterior;//X
	private String numeroInterior;//X
	private String entreCalles;//X
	private  BigDecimal idColonia;
	private String idColoniaString;
	
	@ClientBeanValidator(mappingLabel="Colonia",seccion=SectionType.DATOS_GENERALES,modelView="cliente")
	private String nombreColonia;
	
	private String nombreDelegacion;//Ciudad
	private  BigDecimal idMunicipio;
	
	@ClientBeanValidator(mappingLabel="Municipio",seccion=SectionType.DATOS_GENERALES,modelView="cliente")
	private String idMunicipioString;
	private  BigDecimal idEstado;
	
	@ClientBeanValidator(mappingLabel="Estado",seccion=SectionType.DATOS_GENERALES,modelView="cliente")
	private String idEstadoString;
	
	@ClientBeanValidator(mappingLabel="Codigo postal",seccion=SectionType.DATOS_GENERALES,modelView="cliente")
	private String codigoPostal;
	
	private String descripcionEstado;
	private String direccionCompleta;
	
	//representante Legal
	@ClientBeanValidator(mappingLabel="Representante legal",seccion=SectionType.DATOS_GENERALES,modelView="cliente")
	private String nombreRepresentante;
	
	private String nombreContratante;
	private Date fechaNacimientoRepresentante;
	

	/**
	 * Genera un String que contiene el nombre completo del clienteForm recibido.
	 * El m�todo valida si el cliente es una persona f�sica o moral y devuelve la cadena correcta.
	 * @param clienteForm
	 * @return String nombre Completo del cliente.
	 */
	public String obtenerNombreCliente(){
		StringBuilder nombreCompleto = new StringBuilder("");
		
		if (this != null && this.getClaveTipoPersona()!=null){
			if (this.getClaveTipoPersona().intValue() == 1){
				if(this.getNombre() != null && !this.getNombre().equals("")){
					nombreCompleto.append(this.getNombre() + " ");
				}
				if(this.getApellidoPaterno() != null && !this.getApellidoPaterno().equals("")){
					nombreCompleto.append(this.getApellidoPaterno()+ " ");
				}
				if(this.getApellidoMaterno() != null && !this.getApellidoMaterno().equals("")){
					nombreCompleto.append(this.getApellidoMaterno() + " ");
				}
			}
			else if (this.getClaveTipoPersona().intValue() == 2){
				if(this.getNombre() != null && !this.getNombre().equals("")){
					nombreCompleto.append(this.getNombre() + " ");
				}
			}
		}
		return nombreCompleto.toString().toUpperCase();
	}

	public String getNombreCliente() {
		return obtenerNombreCliente();
	}
	
	/**
	 * Genera un String que contiene la descripci�n de la direcci�n del cliente recibido.
	 * La descripci�n se genera con los campos nombreCalle, descripcionColonia y descripcionEstado.
	 * @param clienteForm
	 * @return
	 */
	public String obtenerDireccionCliente(){
		String descripcionDireccion = "";
		if (this != null){
			descripcionDireccion += (!StringUtil.isEmpty(this.getNombreCalle()))? this.getNombreCalle().toUpperCase()+", " : "";
			descripcionDireccion += (!StringUtil.isEmpty(this.getNombreColonia()))? " COL. "+this.getNombreColonia().toUpperCase()+", " : "";
			descripcionDireccion += (!StringUtil.isEmpty(this.getNombreDelegacion()))? this.getNombreDelegacion().toUpperCase()+", " : "";
			descripcionDireccion += (!StringUtil.isEmpty(this.getDescripcionEstado()))? this.getDescripcionEstado().toUpperCase()+"" : "";
			descripcionDireccion += (!StringUtil.isEmpty(this.getCodigoPostal()))? ", C.P. "+this.getCodigoPostal().toUpperCase()+" " : "";
		}
		return descripcionDireccion;
	}

	public BigDecimal getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(BigDecimal idCliente) {
		this.idCliente = idCliente;
	}

	public BigDecimal getIdToPersona() {
		return idToPersona;
	}

	public void setIdToPersona(BigDecimal idToPersona) {
		this.idToPersona = idToPersona;
	}

	public Short getClaveTipoPersona() {
		return claveTipoPersona;
	}

	public void setClaveTipoPersona(Short claveTipoPersona) {
		this.claveTipoPersona = claveTipoPersona;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getCodigoRFC() {
		return codigoRFC;
	}

	public void setCodigoRFC(String codigoRFC) {
		this.codigoRFC = codigoRFC;
	}

	public String getTelefono() {
		if(this.numeroTelefono != null && this.claveTelefono != null && this.clave2Telefono != null){
			return this.claveTelefono.trim() + this.separadorTel + this.clave2Telefono.trim() + this.separadorTel + this.numeroTelefono.trim();
		}
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public BigDecimal getIdToDireccion() {
		return idToDireccion;
	}

	public void setIdToDireccion(BigDecimal idToDireccion) {
		this.idToDireccion = idToDireccion;
	}

	public String getNombreCalle() {
		return nombreCalle;
	}

	public void setNombreCalle(String nombreCalle) {
		this.nombreCalle = nombreCalle;
	}

	public BigDecimal getNumeroExterior() {
		return numeroExterior;
	}

	public void setNumeroExterior(BigDecimal numeroExterior) {
		this.numeroExterior = numeroExterior;
	}

	public String getNumeroInterior() {
		return numeroInterior;
	}

	public void setNumeroInterior(String numeroInterior) {
		this.numeroInterior = numeroInterior;
	}

	public String getEntreCalles() {
		return entreCalles;
	}

	public void setEntreCalles(String entreCalles) {
		this.entreCalles = entreCalles;
	}

	/**
	 * @return the idColonia
	 */
	public BigDecimal getIdColonia() {
		return idColonia;
	}

	/**
	 * @param idColonia the idColonia to set
	 */
	public void setIdColonia(BigDecimal idColonia) {
		this.idColonia = idColonia;
	}

	public String getNombreColonia() {
		return nombreColonia;
	}

	public void setNombreColonia(String nombreColonia) {
		this.nombreColonia = nombreColonia;
	}

	public String getNombreDelegacion() {
		return nombreDelegacion;
	}

	public void setNombreDelegacion(String nombreDelegacion) {
		this.nombreDelegacion = nombreDelegacion;
	}

	public BigDecimal getIdMunicipio() {
		return idMunicipio;
	}

	public void setIdMunicipio(BigDecimal idMunicipio) {
		this.idMunicipio = idMunicipio;
	}

	public BigDecimal getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(BigDecimal idEstado) {
		this.idEstado = idEstado;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	/**
	 * @return the descripcionEstado
	 */
	public String getDescripcionEstado() {
		return descripcionEstado;
	}

	/**
	 * @param descripcionEstado the descripcionEstado to set
	 */
	public void setDescripcionEstado(String descripcionEstado) {
		this.descripcionEstado = descripcionEstado;
	}

	public String getDireccionCompleta() {
		return direccionCompleta;
	}

	public void setDireccionCompleta(String direccionCompleta) {
		this.direccionCompleta = direccionCompleta;
	}

	public String getEstadoNacimiento() {
		return estadoNacimiento;
	}

	public void setEstadoNacimiento(String estadoNacimiento) {
		this.estadoNacimiento = estadoNacimiento;
	}

	public String getCodigoCURP() {
		return codigoCURP;
	}

	public void setCodigoCURP(String codigoCURP) {
		this.codigoCURP = codigoCURP;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	
	public String getDescripcionSexo(){
		String descripcionSexo = "";
		if(sexo != null){
			if(sexo.equalsIgnoreCase("M")){
				descripcionSexo = "MASCULINO";
			}
			else if (sexo.equalsIgnoreCase("F")){
				descripcionSexo = "FEMENINO";
			}
		}
		return descripcionSexo;
	}

	public String getNombreRepresentante() {
		return nombreRepresentante;
	}

	public void setNombreRepresentante(String nombreRepresentante) {
		this.nombreRepresentante = nombreRepresentante;
	}

	public String getIdColoniaString() {
		return idColoniaString;
	}

	public void setIdColoniaString(String idColoniaString) {
		this.idColoniaString = idColoniaString;
	}

	public String getIdMunicipioString() {
		return idMunicipioString;
	}

	public void setIdMunicipioString(String idMunicipioString) {
		this.idMunicipioString = idMunicipioString;
	}

	public String getIdEstadoString() {
		return idEstadoString;
	}

	public void setIdEstadoString(String idEstadoString) {
		this.idEstadoString = idEstadoString;
	}

	public BigDecimal getIdDomicilio() {
		return idDomicilio;
	}

	public void setIdDomicilio(BigDecimal idDomicilio) {
		this.idDomicilio = idDomicilio;
	}

	public BigDecimal getIdEstadoNacimiento() {
		return idEstadoNacimiento;
	}

	public void setIdEstadoNacimiento(BigDecimal idEstadoNacimiento) {
		this.idEstadoNacimiento = idEstadoNacimiento;
	}

	public String getNombreContratante() {
		return nombreContratante;
	}

	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}

	public Date getFechaNacimientoRepresentante() {
		return fechaNacimientoRepresentante;
	}

	public void setFechaNacimientoRepresentante(Date fechaNacimientoRepresentante) {
		this.fechaNacimientoRepresentante = fechaNacimientoRepresentante;
	}

	public String getClaveNacionalidad() {
		return claveNacionalidad;
	}

	public void setClaveNacionalidad(String claveNacionalidad) {
		this.claveNacionalidad = claveNacionalidad;
	}

	public String getClaveOcupacion() {
		return claveOcupacion;
	}

	public void setClaveOcupacion(String claveOcupacion) {
		this.claveOcupacion = claveOcupacion;
	}

	public String getDescripcionOcupacion() {
		return descripcionOcupacion;
	}

	public void setDescripcionOcupacion(String descripcionOcupacion) {
		this.descripcionOcupacion = descripcionOcupacion;
	}

	public String getClaveCiudadNacimiento() {
		return claveCiudadNacimiento;
	}

	public void setClaveCiudadNacimiento(String claveCiudadNacimiento) {
		this.claveCiudadNacimiento = claveCiudadNacimiento;
	}

	public String getClaveEstadoNacimiento() {
		return claveEstadoNacimiento;
	}
	
	public void setClaveEstadoNacimiento(String claveEstadoNacimiento) {
		this.claveEstadoNacimiento = claveEstadoNacimiento;
	}

	public void setClaveTelefono(String claveTelefono) {
		this.claveTelefono = claveTelefono;
	}

	public String getClaveTelefono() {
		return claveTelefono;
	}

	public void setNumeroTelefono(String numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}

	public String getNumeroTelefono() {
		return numeroTelefono;
	}

	public void setClave2Telefono(String clave2Telefono) {
		this.clave2Telefono = clave2Telefono;
	}

	public String getClave2Telefono() {
		return clave2Telefono;
	}
}
