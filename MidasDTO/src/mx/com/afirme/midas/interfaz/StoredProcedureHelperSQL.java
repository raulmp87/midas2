package mx.com.afirme.midas.interfaz;

import java.lang.reflect.Field;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.dbutils.DbUtils;
import org.springframework.beans.DirectFieldAccessor;

public class StoredProcedureHelperSQL {
	private String cadenaJNDI = null;
	private Connection conexion = null;
	private CallableStatement cs = null;
	private ResultSet rs= null;
	private String nombre = null;
	private String claseResultado = null;
	private HashMap<String, String> mapaResultados = null;
	private List<StoredProcedureParameter> parametros = null;
	private Statement stmt;
	
	private Integer codigoRespuesta = null;
	private String descripcionRespuesta = null;
	private final int numParametrosSalida = 0;
	private Integer posicionOutputParameter = null;
	private Integer tipoOutputParameter = null;
	
	//Constantes para cadenas JNDI de origen de datos para Stored Procedures
	public static final String DATASOURCE_INTERFAZ = "jdbc/InterfazDataSource";
	public static final String DATASOURCE_MIDAS = "jdbc/MidasDataSource";
	public static final String DATASOURCE_MIZAR = "jdbc/MizarDataSource";
		
	private enum TipoProcedimiento {SINGLERESULT, SINGLEOUTPUT, RESULTLIST, EXECUTEUPDATE};
	
	/**
	 * Constructor minimo
	 * @throws Exception 
	 */
	public StoredProcedureHelperSQL() throws Exception {
		obtieneConexion();
		this.parametros = new ArrayList<StoredProcedureParameter>();
	}
	
	/**
	 * Constructor completo
	 * @param nombre Nombre del stored procedure a ejecutar
	 * @throws Exception 
	 */
	public StoredProcedureHelperSQL(String nombre) throws Exception {
		this.nombre = nombre;
		obtieneConexion();
		this.parametros = new ArrayList<StoredProcedureParameter>();
	}
	
	/**
	 * 
	 * @param nombre Nombre del stored procedure a ejecutar
	 * @param cadenaOrigenDatos Nombre de la cadena JNDI de origen de datos
	 * @throws Exception
	 */
	public StoredProcedureHelperSQL(String nombre, String cadenaOrigenDatos) throws Exception {
		this.nombre = nombre;
		this.cadenaJNDI = cadenaOrigenDatos.trim();
		obtieneConexion();
		this.parametros = new ArrayList<StoredProcedureParameter>();
	}
	
	
	/**
	 * Obtiene la conexion a la Base de datos
	 * @throws Exception 
	 */
	@SuppressWarnings("unused")
	private void obtieneConexion() throws Exception {
		try
        {
			String cadenaConexion = DATASOURCE_MIZAR;
			
			if (this.cadenaJNDI != null) {
				cadenaConexion = this.cadenaJNDI;
			}
			
	        Context ctx = new InitialContext();
	        if(ctx == null ) 
	            throw new Exception("No Contexto");
	
	        DataSource ds = 
	              (DataSource)ctx.lookup(
	            		  cadenaConexion);
	
	        if (ds != null) {
	          this.conexion = ds.getConnection();
	        }
	        
        }
	    catch(Exception e) {
	        throw e;
	    } 
	}
	
	/**
	 * Establece un parametro de entrada para el stored procedure
	 * @param nombreParametro Nombre del parametro
	 * @param valorParametro Valor del parametro
	 * 
	 * NOTA: Los parametros se tienen que establecer en el mismo orden en el que 
	 * estan declarados en el Stored Procedure en la Base de Datos
	 */
	public void estableceParametro(String nombreParametro, Object valorParametro) {
		
		//Se hace una validacion si el tipo es Date de java (java.util.Date) se convierta a Date para SQL (java.sql.Timestamp)
		Object valorParametroFinal = null;
		if (valorParametro != null && valorParametro.getClass().getName().equals("java.util.Date")) {
			java.util.Date dateUtil = (java.util.Date) valorParametro;
			valorParametroFinal = new java.sql.Timestamp(dateUtil.getTime());
		} else {
			valorParametroFinal = valorParametro;
		}
		
		this.parametros.add(new StoredProcedureParameter(nombreParametro, valorParametroFinal));
	
	}
	
	/**
	 * Establece el tipo de objeto que devolvera la ejecucion del Stored Procedure
	 * @param claseResultado clase del objeto resultado
	 * @param propiedades propiedades del objeto resultado (separados por coma ',')
	 * @param columnasBaseDatos columnas de la base de datos separados por coma ','. Deben 
	 * ser la misma cantidad y en el mismo orden de que las propiedades del objeto resultado
	 * de tal manera que a cada propiedad le corresponda una columna de base de datos
	 */
	public void estableceMapeoResultados(String claseResultado, String propiedades, String columnasBaseDatos) {
		mapaResultados = new HashMap<String, String>();
		int i = 0;
		this.claseResultado = claseResultado;
		
		if (!propiedades.equals(null) && !columnasBaseDatos.equals(null)) {
		
			String[] aPropiedades = propiedades.split(",");
			String[] aColumnas = columnasBaseDatos.split(",");
			if (aPropiedades.length == aColumnas.length) {
				for (String propiedad : aPropiedades) {
					
					mapaResultados.put(propiedad, aColumnas[i]);
					i++;
				}
			}
		}
	}
	
	
	/**
	 * Obtiene un objeto del tipo mapeado como resultado de la ejecucion del Stored procedure
	 * @return Un objeto del tipo mapeado como resultado de la ejecucion del Stored procedure
	 * @throws Exception 
	 */
	public Object obtieneResultadoSencillo() throws Exception {
		try
        {
			Object clase = null;
			
			this.rs = ejecutaStoredProcedure (TipoProcedimiento.SINGLERESULT);
	        
	        while(this.rs.next()) {
	        	clase = obtieneRegistro(this.rs);
	        	return clase;
		    }
	        
	    } catch(Exception e) {
	        throw e;
	    }finally{
	    	if (this.rs != null)
	    		DbUtils.closeQuietly(this.rs);
	    	if (this.cs != null)
	    		DbUtils.closeQuietly(this.cs);
	    	if (this.conexion != null)
	    		DbUtils.closeQuietly(this.conexion);	    	
	    }	
	    return null;
	}
	
	
	/**
	 * Obtiene el parametro de salida como resultado de la ejecucion del Stored procedure
	 * @param posicionParametroSalida Posicion del parametro de salida a obtener
	 * @param tipoParametroSalida El tipo del parametro de salida a obtener
	 * @return El parametro de salida como resultado de la ejecucion del Stored procedure
	 * @throws Exception 
	 */
	public Object obtieneResultadoOutputSencillo(int posicionParametroSalida, int tipoParametroSalida) throws Exception {
		
		try
        {
			this.posicionOutputParameter = posicionParametroSalida;
			this.tipoOutputParameter = tipoParametroSalida;
			
			ejecutaStoredProcedure (TipoProcedimiento.SINGLEOUTPUT);
			
			return this.cs.getObject(this.posicionOutputParameter);
	        
	    } catch(Exception e) {
	        throw e;
	    }finally{
	    	if (this.rs != null)
	    		DbUtils.closeQuietly(this.rs);
	    	if (this.cs != null)
	    		DbUtils.closeQuietly(this.cs);
	    	if (this.conexion != null)
	    		DbUtils.closeQuietly(this.conexion);	    	
	    }
		
	}
	
	/**
	 * Obtiene una lista de objetos del tipo mapeado como resultado de la ejecucion del Stored procedure
	 * @return Una lista de objetos del tipo mapeado como resultado de la ejecucion del Stored procedure
	 * @throws Exception 
	 */
	@SuppressWarnings({ "rawtypes" })
	public List obtieneListaResultados() throws Exception {
		try
        {
			Object clase = null;
			List<Object> listaObjetos = new ArrayList<Object>();
			
			this.rs = ejecutaStoredProcedure (TipoProcedimiento.RESULTLIST);
	        
	        while(this.rs.next()) {
		        clase = obtieneRegistro(this.rs);
		        listaObjetos.add(clase);
		    }
	        
	        return listaObjetos;
	        
	    } catch(Exception e) {
	        throw e;
	    }finally{
	    	if (this.rs != null)
	    		DbUtils.closeQuietly(this.rs);
	    	if (this.cs != null)
	    		DbUtils.closeQuietly(this.cs);
	    	if (this.conexion != null)
	    		DbUtils.closeQuietly(this.conexion);	    	
	    }
	}
	
	/**
	 * Ejecuta un Stored procedure transaccional (INSERT, UPDATE, DELETE)
	 * @return Un entero indicando si la ejecucion fue exitosa o no
	 * @throws Exception 
	 */
	public int ejecutaActualizar() throws Exception {
		try
        {
			Integer res = new Integer(0);
			
			this.rs = ejecutaStoredProcedure (TipoProcedimiento.EXECUTEUPDATE);
			
			 while(this.rs.next()) {
				 res = this.rs.getInt(1);
			    }
			 
			 return res.intValue();
	       
	    }
	    catch(Exception e) {
	        throw e;
	    } finally{
	    	if (this.rs != null)
	    		DbUtils.closeQuietly(this.rs);
	    	if (this.cs != null)
	    		DbUtils.closeQuietly(this.cs);
	    	if (this.conexion != null)
	    		DbUtils.closeQuietly(this.conexion);	    	
	    }
	}
	
	/**
	 * Ejecuta el Stored procedure
	 * @param tipoProcedimiento Tipo de procedimiento a ejecutar (SINGLERESULT, RESULTLIST, EXECUTEUPDATE, SINGLEOUTPUT)
	 * @throws Exception 
	 */
	private ResultSet ejecutaStoredProcedure(TipoProcedimiento tipoProcedimiento) throws Exception {
		
		try
        {
			
			cs = this.conexion.prepareCall("{call  " + this.nombre.trim()+ "(" + numeroParametrosEnQuery() + ")}");
			int i = 1;
	        for (StoredProcedureParameter parametro : parametros) {
	        	if (this.posicionOutputParameter == null || this.posicionOutputParameter.intValue() != i) {
	        		registraParametro(parametro, parametros.indexOf(parametro) + 1);
	        	}
	        	i++;
	       	}
	        
	        //Se registrara el parametro de salida con la consideracion que en el Stored Procedure
	        //de la BD tiene que ser el unico parametro de salida
	        switch (tipoProcedimiento) {

	        case SINGLEOUTPUT:
	        		
	        	this.cs.registerOutParameter(this.posicionOutputParameter, this.tipoOutputParameter);
	        	if (this.claseResultado == null || 
	    				this.claseResultado.toLowerCase().equals("void.class")) {
	        		cs.execute();
	        		return null;
	        	}
	        	break;
	        }
	        
	        return cs.executeQuery();
	        
	    }
	    catch(Exception e)
	        {
	        throw e;
	    } 
	    
	}

	/**
	 * Genera una cadena que indica el numero de parametros necesaria para la invocacion del Stored procedure
	 * @return Una cadena que indica el numero de parametros necesaria para la invocacion del Stored procedure
	 */
	private String numeroParametrosEnQuery() {
		
		StringBuilder resul = new StringBuilder("");	
		String resultado ="";
		
		int parametroSalida = this.numParametrosSalida;
		
		if (this.claseResultado != null && 
				this.claseResultado.toLowerCase().equals("void.class")) {
			parametroSalida = 0;
		}
		
		for (int i = 0; i < this.parametros.size() + parametroSalida; i++) {
			resul.append("?,");
		}
		resultado=resul.toString();
		if (resultado.length() > 0) {
			resultado = resultado.substring(0, resultado.lastIndexOf(","));
		}
		
		return resultado;
	}
	
	/**
	 * Registra un parametro de entrada del Stored procedure
	 * @param parametro Parametro de entrada del Stored procedure
	 * @param indiceParametro indice del parametro de entrada del Stored procedure
	 * @throws Exception 
	 */
	private void registraParametro(StoredProcedureParameter parametro, int indiceParametro) throws Exception {
		try {
			this.cs.setObject(indiceParametro, parametro.getValor());
		} catch (Exception e) {
			throw e;
		}
		
	}
	
	/**
	 * Ejecuta el mapeo del resultado del Stored procedure al objeto de salida
	 * @param rs ResultSet del Stored procedure
	 * @return El objeto de salida mapeado
	 * @throws Exception 
	 */
	private Object obtieneRegistro(ResultSet rs) throws Exception {
		
		Object clase = null;
		String nombreColumna;
		Object numberObject = null;
		Object simpleObject = null;
		
		PropertyUtilsBean utilsBean = new PropertyUtilsBean();
		
        try {
			clase = Class.forName(this.claseResultado).newInstance();
		
	        for (Field field : Class.forName(this.claseResultado).getDeclaredFields()) {
	        	nombreColumna = null;
	        	nombreColumna = this.mapaResultados.get(field.getName());
	        	if (nombreColumna != null) {
	        		simpleObject = rs.getObject(rs.findColumn(nombreColumna));
	        		
	        		if (simpleObject != null) {
		        		
		        		//Si es un BigDecimal
		        		if (simpleObject.getClass().getSimpleName().equals("BigDecimal")) {
		        			
		        			if (field.getType().getSimpleName().equals("Integer")) {
		        				numberObject = Integer.valueOf(simpleObject.toString());
		        			} else if (field.getType().getSimpleName().equals("Short")) {
		        				numberObject = Short.valueOf(simpleObject.toString());
		        			} else if (field.getType().getSimpleName().equals("Long")) {
		        				numberObject = Long.valueOf(simpleObject.toString());
		        			} else if (field.getType().getSimpleName().equals("Float")) {
		        				numberObject = Float.valueOf(simpleObject.toString());
		        			} else if (field.getType().getSimpleName().equals("Double")) {
		        				numberObject = Double.valueOf(simpleObject.toString());		        				
		        			} else {//BigDecimal
		        				numberObject = simpleObject;
		        			}
		        			
		        			utilsBean.setProperty(clase, field.getName(),  field.getType().cast(numberObject));
		        			
		        		} else {
		        			utilsBean.setProperty(clase, field.getName(),  field.getType().cast(simpleObject));
		        		}
	        		}
	        	}
	        	
	        }
	       
	        
        }  catch (Exception e) {
			throw e;
		} 
		
		return clase;
	}

	/**
	 * @author jmendoza
	 * @param <T>
	 * @param select
	 * @param clazz
	 * @return Lista
	 * @throws Exception
	 */
	public <T> List<T>  executeSelect(String select,Class<T> clazz) throws Exception{
		try
        {
			System.out.println("*******Entrando al metodo executeSelect "+ select);
			this.conexion.setAutoCommit(true);
			List<T> lista = new ArrayList<T>();
		    
			Class tipoObjeto = clazz;
			Field[] fields = tipoObjeto.getDeclaredFields();
	        
			this.stmt = this.conexion.createStatement();
			this.rs = stmt.executeQuery(select);
//			this.conexion.commit();
			while(rs.next()) {
				T elem = clazz.newInstance();
				for(Field f : fields){
					try{
						DirectFieldAccessor myObjectAccessor = new DirectFieldAccessor(elem);
					    myObjectAccessor.setPropertyValue(f.getName(), rs.getObject(f.getName()));
					}catch(SQLException e){
						e.printStackTrace();
					}
		        }
				 lista.add(elem);
		    }
			
	        return lista;
	        
	    } catch(Exception e) {
	    	System.out.println("********************Exception en executeSelect"+e.getMessage());
	        throw e;
	    }
	    finally{
	    	if (this.rs != null)
	    		DbUtils.closeQuietly(this.rs);
	    	if (this.stmt != null)
	    		DbUtils.closeQuietly(this.stmt);
	    	if (this.conexion != null)
	    		DbUtils.closeQuietly(this.conexion);	    	
	    }
	}

	/**
	 * @return nombre Nombre del Stored procedure
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre Nombre del Stored procedure a ingresar
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the codigoRespuesta
	 */
	public Integer getCodigoRespuesta() {
		return codigoRespuesta;
	}

	/**
	 * @return the descripcionRespuesta
	 */
	public String getDescripcionRespuesta() {
		return descripcionRespuesta;
	}
	
	
}
