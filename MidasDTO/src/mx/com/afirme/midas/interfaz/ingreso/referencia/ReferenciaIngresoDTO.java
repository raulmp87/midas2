package mx.com.afirme.midas.interfaz.ingreso.referencia;

import mx.com.afirme.midas.base.CacheableDTO;

public class ReferenciaIngresoDTO extends CacheableDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3529806702160354844L;

	private String idReferenciaExterna;
	private String descripcionReferenciaExterna;

	public ReferenciaIngresoDTO() {
	}
	
	public ReferenciaIngresoDTO(String idReferenciaExterna,String descripcionReferenciaExterna) {
		this.idReferenciaExterna = idReferenciaExterna;
		this.descripcionReferenciaExterna = descripcionReferenciaExterna;
	}

	@Override
	public String getDescription() {
		return getDescripcionReferenciaExterna();
	}

	@Override
	public Object getId() {
		return getIdReferenciaExterna();
	}

	@Override
	public boolean equals(Object object) {
		// TODO Auto-generated method stub
		return false;
	}

	public String getIdReferenciaExterna() {
		return idReferenciaExterna;
	}

	public void setIdReferenciaExterna(String idReferenciaExterna) {
		this.idReferenciaExterna = idReferenciaExterna;
	}

	public String getDescripcionReferenciaExterna() {
		return descripcionReferenciaExterna;
	}

	public void setDescripcionReferenciaExterna(String descripcionReferenciaExterna) {
		this.descripcionReferenciaExterna = descripcionReferenciaExterna;
	}
}
