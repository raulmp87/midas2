package mx.com.afirme.midas.interfaz.ingreso.referencia;

import java.util.List;

import javax.ejb.Remote;


public interface ReferenciaIngresoFacadeRemote {

	public List<ReferenciaIngresoDTO> obtenerReferenciasIngreso (String nombreUsuario) throws Exception;
}
