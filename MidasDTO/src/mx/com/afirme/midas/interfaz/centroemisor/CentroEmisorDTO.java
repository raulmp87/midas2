package mx.com.afirme.midas.interfaz.centroemisor;

import java.io.Serializable;

public class CentroEmisorDTO implements Serializable {

	
	private static final long serialVersionUID = 5458314985965584076L;
	
	private String clave;
	private String nombre;
	
	/**
	 * @return the clave
	 */
	public String getClave() {
		return clave;
	}
	/**
	 * @param clave the clave to set
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}
