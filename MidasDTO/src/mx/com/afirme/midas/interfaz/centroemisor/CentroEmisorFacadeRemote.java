package mx.com.afirme.midas.interfaz.centroemisor;

import java.util.List;

import javax.ejb.Remote;


public interface CentroEmisorFacadeRemote {

	public List<CentroEmisorDTO> listarCentrosEmisores(String nombreUsuario)
	throws Exception;
	
	
}
