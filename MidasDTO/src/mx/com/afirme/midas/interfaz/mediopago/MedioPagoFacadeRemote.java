package mx.com.afirme.midas.interfaz.mediopago;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;


public interface MedioPagoFacadeRemote {

	public List<MedioPagoDTO> findByProperty (MedioPagoDTO medioPago, String nombreUsuario)
	throws Exception;
	
}
