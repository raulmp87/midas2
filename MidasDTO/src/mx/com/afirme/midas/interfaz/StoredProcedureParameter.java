/**
 * Clase de parametro de entrada para Stored procedure
 */
package mx.com.afirme.midas.interfaz;

/**
 * @author andres.avalos
 *
 */
public class StoredProcedureParameter {

	
	private String nombre = null;
	private Object valor = null;
		
	/**
	 * Constructor minimo
	 */
	public StoredProcedureParameter() {
		
	}
	
	/**
	 * Constructor completo
	 * @param nombre Nombre del parametro
	 * @param valor Valor del parametro
	 */
	public StoredProcedureParameter(String nombre, Object valor) {
		
		this.nombre = nombre;
		this.valor = valor;
		
	}
	
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the valor
	 */
	public Object getValor() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(Object valor) {
		this.valor = valor;
	}
	
	
}
