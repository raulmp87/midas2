/**
 * 
 */
package mx.com.afirme.midas.interfaz.agente;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.base.MidasInterfaceBase;
import mx.com.afirme.midas.interfaz.agente.tipocedula.TipoCedulaDTO;

/**
 * @author andres.avalos
 *
 */

public interface AgenteFacadeRemote extends MidasInterfaceBase<AgenteDTO>{

	public AgenteDTO findById(Integer idAgente, String nombreUsuario)
	throws Exception;
	
	public List<AgenteDTO> listarAgentes(String nombreUsuario, String tipoCedula)
	throws Exception;
	
	public List<TipoCedulaDTO> listarTiposCedula (String nombreUsuario)
	throws Exception;
	
	/**
	 * Consulta lista de Ejecutivos
	 * @param nombreUsuario Codigo del usuario que realiza la operacion
	 * @return Lista de Ejecutivos
	 * @throws Exception
	 */
	public List<AgenteDTO> listaEjecutivos(String nombreUsuario)
	throws Exception;
	
	/**
	 * Consulta Agentes de un Ejecutivo 
	 * @param idOficina id de la Oficina del Ejecutivo
	 * @param nombreUsuario Codigo del usuario que realiza la operacion
	 * @return Lista de Agentes de un Ejecutivo
	 * @throws Exception
	 */
	public List<AgenteDTO> listaEjecutivoAgentes(String idOficina, String nombreUsuario)
	throws Exception;
	
}
