package mx.com.afirme.midas.interfaz.agente.tipocedula;

import java.io.Serializable;

public class TipoCedulaDTO implements Serializable {
	
	private static final long serialVersionUID = 2532625675226343673L;
	
	private String cedula;
	private String descripcion;
	
	/**
	 * @return the cedula
	 */
	public String getCedula() {
		return cedula;
	}
	/**
	 * @param cedula the cedula to set
	 */
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
	
	
}
