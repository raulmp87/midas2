package mx.com.afirme.midas.interfaz.codigopostalcolonia;

import java.io.Serializable;

public class CodigoPostalColoniaDTO implements Serializable {

	private static final long serialVersionUID = -5507447482056048683L;

	private String codigoPostal;
	private Short idColonia;
	private String nombreColonia;
	
	
	public CodigoPostalColoniaDTO(String codigoPostal, Short idColonia, String nombreColonia) {
	
		this.codigoPostal = codigoPostal;
		this.idColonia = idColonia;
		this.nombreColonia = nombreColonia;
	}
	
	public CodigoPostalColoniaDTO() {
	
	}
	
	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public Short getIdColonia() {
		return idColonia;
	}
	public void setIdColonia(Short idColonia) {
		this.idColonia = idColonia;
	}
	public String getNombreColonia() {
		return nombreColonia;
	}
	public void setNombreColonia(String nombreColonia) {
		this.nombreColonia = nombreColonia;
	}
	
	
	
	
}
