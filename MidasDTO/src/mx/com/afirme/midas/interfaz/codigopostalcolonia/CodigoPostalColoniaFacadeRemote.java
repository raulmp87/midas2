package mx.com.afirme.midas.interfaz.codigopostalcolonia;

import java.util.List;

import javax.ejb.Remote;


public interface CodigoPostalColoniaFacadeRemote {

	public List<CodigoPostalColoniaDTO> agregaCodigoPostal (CodigoPostalColoniaDTO codigoPostal, String nombreUsuario) throws Exception;
	
	public List<CodigoPostalColoniaDTO> listaColonias (CodigoPostalColoniaDTO codigoPostal, String nombreUsuario) throws Exception;

	public List<CodigoPostalColoniaDTO> findByZipCode(String zipCode) throws Exception;
}
