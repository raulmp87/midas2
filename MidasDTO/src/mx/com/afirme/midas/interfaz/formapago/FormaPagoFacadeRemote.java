package mx.com.afirme.midas.interfaz.formapago;

import java.util.List;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoIDTO;

import javax.ejb.Remote;


public interface FormaPagoFacadeRemote {
	
	public List<FormaPagoIDTO> findByProperty (FormaPagoIDTO formaPago, String nombreUsuario)
	throws Exception;
	
	public List<FormaPagoIDTO> findByProperty (FormaPagoIDTO formaPago, String nombreUsuario, String claveNegocio)
	throws Exception;
	
}
