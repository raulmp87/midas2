package mx.com.afirme.midas.interfaz.formapago;

import java.io.Serializable;
import java.math.BigDecimal;

public class FormaPagoIDTO implements Serializable{

	private static final long serialVersionUID = 6837003601755570779L;
	
	private Integer idFormaPago;
	private String descripcion;
	private BigDecimal porcentajeRecargoPagoFraccionado;
	private Integer numeroRecibosGenerados;
	private Short idMoneda;
	private String derechosProrrateados;
	
	public FormaPagoIDTO(){
		
	}
	public FormaPagoIDTO(Integer idFormaPago, String descripcion, Short idMoneda){
		this.idFormaPago = idFormaPago;
		this.descripcion = descripcion;
		this.idMoneda = idMoneda;
	}
	
	/**
	 * @return the idFormaPago
	 */
	public Integer getIdFormaPago() {
		return idFormaPago;
	}
	/**
	 * @param idFormaPago the idFormaPago to set
	 */
	public void setIdFormaPago(Integer idFormaPago) {
		this.idFormaPago = idFormaPago;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @return the porcentajeRecargoPagoFraccionado
	 */
	public BigDecimal getPorcentajeRecargoPagoFraccionado() {
		return porcentajeRecargoPagoFraccionado;
	}
	/**
	 * @param porcentajeRecargoPagoFraccionado the porcentajeRecargoPagoFraccionado to set
	 */
	public void setPorcentajeRecargoPagoFraccionado(
			BigDecimal porcentajeRecargoPagoFraccionado) {
		this.porcentajeRecargoPagoFraccionado = porcentajeRecargoPagoFraccionado;
	}
	/**
	 * @return the numeroRecibosGenerados
	 */
	public Integer getNumeroRecibosGenerados() {
		return numeroRecibosGenerados;
	}
	/**
	 * @param numeroRecibosGenerados the numeroRecibosGenerados to set
	 */
	public void setNumeroRecibosGenerados(Integer numeroRecibosGenerados) {
		this.numeroRecibosGenerados = numeroRecibosGenerados;
	}
	/**
	 * @return the idMoneda
	 */
	public Short getIdMoneda() {
		return idMoneda;
	}
	/**
	 * @param idMoneda the idMoneda to set
	 */
	public void setIdMoneda(Short idMoneda) {
		this.idMoneda = idMoneda;
	}
	public void setDerechosProrrateados(String derechosProrrateados) {
		this.derechosProrrateados = derechosProrrateados;
	}
	public String getDerechosProrrateados() {
		return derechosProrrateados;
	}
	
	
	
	
	
}
