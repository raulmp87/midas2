package mx.com.afirme.midas.interfaz.endoso;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class EndosoIDTO implements Serializable {

	private static final long serialVersionUID = 3154540453466858424L;

	private Long numeroFolioRecibo; // n�mero de recibo
	private BigDecimal idPoliza; // (No nulo) Identificador de Poliza de Midas
	private Short numeroEndoso; // Numero de Endoso en MIDAS
	private String llaveFiscal; // Llave fiscal del Endoso
	private Short esValido; // Para diferentes validaciones Ej. Al Identificar
	// si la poliza es Cancelable a inicio de vigencia,
	// 0- No cancelable, 1-Cancelable, null-Error
	private String motivoNoCancelable; // Motivo por el cual la poliza no es
	// cancelable
	private BigDecimal referencia; // Identificador de Referencia Ej.
	// identificador de Endoso generado en
	// SEYCOS
	private Short numeroExhibicion; // n�mero de exhibici�n
	private Double importe; // Importe a Pagar
	private Date fechaLimitePago; // Fecha l�mite de pago

	private Date fechaInicioVigencia; // Fecha de Inicio de Vigencia del Endoso
	// de Cancelacion/rehabilitacion
	private Double primaNeta; // PN por devengar al momento de la
	// Cancelacion/rehabilitacion
	private Double bonificacionComision; // Bonificacion de la Comision por
	// devengar al momento de la
	// Cancelacion/rehabilitacion
	private Double bonificacionComisionRPF; // Bonificacion de la Comision por
	// Recargo Pago Fraccionado por
	// devengar al momento de la
	// Cancelacion/rehabilitacion
	private Double derechos; // Derechos por devengar al momento de la
	// Cancelacion/rehabilitacion
	private Double iva; // IVA por devengar al momento de la
	// Cancelacion/rehabilitacion
	private Double primaTotal; // Prima Total por devengar al momento de la
	// Cancelacion/rehabilitacion
	private Double comision; // Comision por devengar al momento de la
	// Cancelacion/rehabilitacion
	private Double comisionRPF; // Comision por Recargo Pago Fraccionado por
	// devengar al momento de la
	// Cancelacion/rehabilitacion
	private Double recargoPF; // Recargo por Pago Fraccionado por devengar al
	// momento de la cancelacion/rehabilitacion
	private String calculo; // indica el tipo de Calculo que se debe realizar al
							// cancelar 'S' SEYCOS, 'M' MIDAS
	private String numeroEndosoString;
	/*
	 * Campos usados para el reporte de endosos cancelables.
	 */
	private String numeroPoliza;
	private String nombreSolicitante;
	private Long idAgente;
	private String nombreAgente;
	private String nombreSupervisoria;
	private String nombreOficina;
	private String nombreGerencia;
	private String claveMovimientoEndoso;
	private Integer diasVencidos;
	
	/*
	 * Validacion en Autos
	 */
	private BigDecimal numeroLinea;
	private Integer numeroInciso;
	private BigDecimal importeNotaCredito;
	private Integer idCobertura;
	private Integer tipoMovimiento;
	
	//SobreComisiones
	private Double sobreComisionAgente;
	private Double sobreComisionProm;
	private Double bonoAgente;
	private Double bonoProm;
	private Double cesionDerechosAgente;
	private Double cesionDerechosProm;
	private Double sobreComisionUDIAgente;
	private Double sobreComisionUDIProm;
	
	private BigDecimal factorDctoIgualacion;
	
	public Long getNumeroFolioRecibo() {
		return numeroFolioRecibo;
	}

	public void setNumeroFolioRecibo(Long numeroFolioRecibo) {
		this.numeroFolioRecibo = numeroFolioRecibo;
	}

	/**
	 * Obtiene el Identificador de la Poliza en Midas
	 * 
	 * @return Identificador de la Poliza en Midas
	 */
	public BigDecimal getIdPoliza() {
		return idPoliza;
	}

	public void setIdPoliza(BigDecimal idPoliza) {
		this.idPoliza = idPoliza;
	}

	/**
	 * Obtiene el Numero de Endoso en MIDAS
	 * 
	 * @return Numero de Endoso en MIDAS
	 */
	public Short getNumeroEndoso() {
		return numeroEndoso;
	}

	public void setNumeroEndoso(Short numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}

	/**
	 * Obtiene la llave fiscal del Endoso
	 * 
	 * @return Llave fiscal del Endoso
	 */
	public String getLlaveFiscal() {
		return llaveFiscal;
	}

	public void setLlaveFiscal(String llaveFiscal) {
		this.llaveFiscal = llaveFiscal;
	}

	/**
	 * Obtiene un numero para diferentes validaciones Ej. Al Identificar si la
	 * poliza es Cancelable a inicio de vigencia, 0- No cancelable, 1-Cancelable
	 * 
	 * @return Numero para diferentes validaciones
	 */
	public Short getEsValido() {
		return esValido;
	}

	public void setEsValido(Short esValido) {
		this.esValido = esValido;
	}

	/**
	 * Obtiene el motivo por el cual la poliza no es cancelable
	 * 
	 * @return Motivo por el cual la poliza no es cancelable
	 */
	public String getMotivoNoCancelable() {
		return motivoNoCancelable;
	}

	public void setMotivoNoCancelable(String motivoNoCancelable) {
		this.motivoNoCancelable = motivoNoCancelable;
	}

	/**
	 * Obtiene un identificador de Referencia Ej. identificador de Endoso
	 * generado en SEYCOS
	 * 
	 * @return Identificador de Referencia
	 */
	public BigDecimal getReferencia() {
		return referencia;
	}

	public void setReferencia(BigDecimal referencia) {
		this.referencia = referencia;
	}

	public Short getNumeroExhibicion() {
		return numeroExhibicion;
	}

	public void setNumeroExhibicion(Short numeroExhibicion) {
		this.numeroExhibicion = numeroExhibicion;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public Date getFechaLimitePago() {
		return fechaLimitePago;
	}

	public void setFechaLimitePago(Date fechaLimitePago) {
		this.fechaLimitePago = fechaLimitePago;
	}

	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	public Double getPrimaNeta() {
		return primaNeta;
	}

	public void setPrimaNeta(Double primaNeta) {
		this.primaNeta = primaNeta;
	}

	public Double getBonificacionComision() {
		return bonificacionComision;
	}

	public void setBonificacionComision(Double bonificacionComision) {
		this.bonificacionComision = bonificacionComision;
	}

	public Double getBonificacionComisionRPF() {
		return bonificacionComisionRPF;
	}

	public void setBonificacionComisionRPF(Double bonificacionComisionRPF) {
		this.bonificacionComisionRPF = bonificacionComisionRPF;
	}

	public Double getDerechos() {
		return derechos;
	}

	public void setDerechos(Double derechos) {
		this.derechos = derechos;
	}

	public Double getIva() {
		return iva;
	}

	public void setIva(Double iva) {
		this.iva = iva;
	}

	public Double getPrimaTotal() {
		return primaTotal;
	}

	public void setPrimaTotal(Double primaTotal) {
		this.primaTotal = primaTotal;
	}

	public Double getComision() {
		return comision;
	}

	public void setComision(Double comision) {
		this.comision = comision;
	}

	public Double getComisionRPF() {
		return comisionRPF;
	}

	public void setComisionRPF(Double comisionRPF) {
		this.comisionRPF = comisionRPF;
	}

	public Double getRecargoPF() {
		return recargoPF;
	}

	public void setRecargoPF(Double recargoPF) {
		this.recargoPF = recargoPF;
	}

	/**
	 * @return the calculo
	 */
	public String getCalculo() {
		return calculo;
	}

	/**
	 * @param calculo
	 *            the calculo to set
	 */
	public void setCalculo(String calculo) {
		this.calculo = calculo;
	}

	public String getNumeroEndosoString() {
		return numeroEndosoString;
	}

	public void setNumeroEndosoString(String numeroEndosoString) {
		this.numeroEndosoString = numeroEndosoString;
	}

	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	public String getNombreSolicitante() {
		return nombreSolicitante;
	}

	public void setNombreSolicitante(String nombreSolicitante) {
		this.nombreSolicitante = nombreSolicitante;
	}

	public Long getIdAgente() {
		return idAgente;
	}

	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}

	public String getNombreAgente() {
		return nombreAgente;
	}

	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}

	public String getNombreSupervisoria() {
		return nombreSupervisoria;
	}

	public void setNombreSupervisoria(String nombreSupervisoria) {
		this.nombreSupervisoria = nombreSupervisoria;
	}

	public String getNombreOficina() {
		return nombreOficina;
	}

	public void setNombreOficina(String nombreOficina) {
		this.nombreOficina = nombreOficina;
	}

	public String getNombreGerencia() {
		return nombreGerencia;
	}

	public void setNombreGerencia(String nombreGerencia) {
		this.nombreGerencia = nombreGerencia;
	}

	public Integer getDiasVencidos() {
		return diasVencidos;
	}

	public String getClaveMovimientoEndoso() {
		return claveMovimientoEndoso;
	}

	public void setClaveMovimientoEndoso(String claveMovimientoEndoso) {
		this.claveMovimientoEndoso = claveMovimientoEndoso;
	}

	public void setDiasVencidos(Integer diasVencidos) {
		this.diasVencidos = diasVencidos;
	}

	public BigDecimal getNumeroLinea() {
		return numeroLinea;
	}

	public void setNumeroLinea(BigDecimal numeroLinea) {
		this.numeroLinea = numeroLinea;
	}

	public Integer getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public BigDecimal getImporteNotaCredito() {
		return importeNotaCredito;
	}

	public void setImporteNotaCredito(BigDecimal importeNotaCredito) {
		this.importeNotaCredito = importeNotaCredito;
	}

	public void setSobreComisionAgente(Double sobreComisionAgente) {
		this.sobreComisionAgente = sobreComisionAgente;
	}

	public Double getSobreComisionAgente() {
		return sobreComisionAgente;
	}

	public void setSobreComisionProm(Double sobreComisionProm) {
		this.sobreComisionProm = sobreComisionProm;
	}

	public Double getSobreComisionProm() {
		return sobreComisionProm;
	}

	public void setBonoAgente(Double bonoAgente) {
		this.bonoAgente = bonoAgente;
	}

	public Double getBonoAgente() {
		return bonoAgente;
	}

	public void setBonoProm(Double bonoProm) {
		this.bonoProm = bonoProm;
	}

	public Double getBonoProm() {
		return bonoProm;
	}

	public void setCesionDerechosAgente(Double cesionDerechosAgente) {
		this.cesionDerechosAgente = cesionDerechosAgente;
	}

	public Double getCesionDerechosAgente() {
		return cesionDerechosAgente;
	}

	public void setCesionDerechosProm(Double cesionDerechosProm) {
		this.cesionDerechosProm = cesionDerechosProm;
	}

	public Double getCesionDerechosProm() {
		return cesionDerechosProm;
	}

	public void setSobreComisionUDIAgente(Double sobreComisionUDIAgente) {
		this.sobreComisionUDIAgente = sobreComisionUDIAgente;
	}

	public Double getSobreComisionUDIAgente() {
		return sobreComisionUDIAgente;
	}

	public void setSobreComisionUDIProm(Double sobreComisionUDIProm) {
		this.sobreComisionUDIProm = sobreComisionUDIProm;
	}

	public Double getSobreComisionUDIProm() {
		return sobreComisionUDIProm;
	}

	public BigDecimal getFactorDctoIgualacion() {
		return factorDctoIgualacion;
	}

	public void setFactorDctoIgualacion(BigDecimal factorDctoIgualacion) {
		this.factorDctoIgualacion = factorDctoIgualacion;
	}	

	public Integer getIdCobertura() {
		return idCobertura;
	}

	public void setIdCobertura(Integer idCobertura) {
		this.idCobertura = idCobertura;
	}

	public Integer getTipoMovimiento() {
		return tipoMovimiento;
	}

	public void setTipoMovimiento(Integer tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}	

	
}
