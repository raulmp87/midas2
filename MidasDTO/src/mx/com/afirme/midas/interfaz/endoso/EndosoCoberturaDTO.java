/**
 * 
 */
package mx.com.afirme.midas.interfaz.endoso;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * @author Jorge.Cano
 * 
 */
public class EndosoCoberturaDTO implements Serializable {

	/**
	 * 
	 */
	private static final NumberFormat fMonto = new DecimalFormat("$#,##0.00");
	private static final long serialVersionUID = 1L;
	private BigDecimal idToSeccion;
	private String nombreComercialSeccion;
	private BigDecimal idTocobertura;
	private String nombreComercialCobertura;
	private Double valorPrimaneta;

	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	public String getNombreComercialSeccion() {
		return nombreComercialSeccion;
	}

	public void setNombreComercialSeccion(String nombreComercialSeccion) {
		this.nombreComercialSeccion = nombreComercialSeccion;
	}

	public BigDecimal getIdTocobertura() {
		return idTocobertura;
	}

	public void setIdTocobertura(BigDecimal idTocobertura) {
		this.idTocobertura = idTocobertura;
	}

	public String getNombreComercialCobertura() {
		return nombreComercialCobertura;
	}

	public void setNombreComercialCobertura(String nombreComercialCobertura) {
		this.nombreComercialCobertura = nombreComercialCobertura;
	}

	public Double getValorPrimaneta() {
		return valorPrimaneta;
	}

	public void setValorPrimaneta(Double valorPrimaneta) {
		this.valorPrimaneta = valorPrimaneta;
	}

	public String getValorPrimanetaString() {
		return fMonto.format(this.valorPrimaneta.doubleValue()) ;
	}
}
