package mx.com.afirme.midas.interfaz.contratofacultativo.pagocobertura;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;


public interface PagoCoberturaFacadeRemote {
	public List<PagoCoberturaDTO> calcularPagos(Date fechaInicialBase, Date fechaFinalBase, int idFormaPagoBase,
												Date fechaInicioVigencia, Date fechaFinVigencia, int formaPago, 
												double montoAPagar, String nombreUsuario) throws Exception;	
}
