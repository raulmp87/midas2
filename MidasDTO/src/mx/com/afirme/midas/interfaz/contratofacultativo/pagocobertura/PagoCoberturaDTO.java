package mx.com.afirme.midas.interfaz.contratofacultativo.pagocobertura;

import java.io.Serializable;
import java.util.Date;

public class PagoCoberturaDTO implements Serializable {	
	private static final long serialVersionUID = -6583750646362576784L;	
	
	private Short numeroExhibicion; 	//número de exhibición
	private Date fechaInicioPago; 		//Fecha del pago
	private Date fechaFinPago; 			//Fecha del pago
	private String periodoCompleto;		//Periodo Completo - S, N	
	private Double factor;				//Factor
	private Double montoPago; 			//Monto a Pagar
	
	public Short getNumeroExhibicion() {
		return numeroExhibicion;
	}
	public void setNumeroExhibicion(Short numeroExhibicion) {
		this.numeroExhibicion = numeroExhibicion;
	}
	public Date getFechaInicioPago() {
		return fechaInicioPago;
	}
	public void setFechaInicioPago(Date fechaInicioPago) {
		this.fechaInicioPago = fechaInicioPago;
	}
	public Date getFechaFinPago() {
		return fechaFinPago;
	}
	public void setFechaFinPago(Date fechaFinPago) {
		this.fechaFinPago = fechaFinPago;
	}
	public String getPeriodoCompleto() {
		return periodoCompleto;
	}
	public void setPeriodoCompleto(String periodoCompleto) {
		this.periodoCompleto = periodoCompleto;
	}
	public Double getFactor() {
		return factor;
	}
	public void setFactor(Double factor) {
		this.factor = factor;
	}
	public Double getMontoPago() {
		return montoPago;
	}
	public void setMontoPago(Double montoPago) {
		this.montoPago = montoPago;
	}
}