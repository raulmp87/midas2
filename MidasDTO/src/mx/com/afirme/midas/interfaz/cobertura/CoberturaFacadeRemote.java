/**
 * 
 */
package mx.com.afirme.midas.interfaz.cobertura;

import javax.ejb.Remote;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;

/**
 * @author andres.avalos
 *
 */

public interface CoberturaFacadeRemote {

	public void save(CoberturaDTO entity, String nombreUsuario) throws Exception;
	
}
