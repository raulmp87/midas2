package mx.com.afirme.midas.interfaz.consultacobranzapoliza;

import java.math.BigDecimal;

import javax.ejb.Remote;


public interface ConsultaCobranzaPolizaFacadeRemote {

	public DatosCobranzaPolizaDTO consultaCobranzaPoliza (BigDecimal idToPoliza,String nombreUsuario)
	throws Exception;
}
