package mx.com.afirme.midas.interfaz.consultacobranzapoliza;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class DatosCobranzaPolizaDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String idToPoliza;
	private String numeroPoliza;
	private String situacion;
	private BigDecimal ultimoReciboPagado;
	private Date fechaVencimiento;
	private Date fechaUltimoPago;
	private BigDecimal montoPagado;
	private BigDecimal montoPorVencer;
	private BigDecimal montoVencido;
	/**
	 * @return the idToPoliza
	 */
	public String getIdToPoliza() {
		return idToPoliza;
	}
	/**
	 * @param idToPoliza the idToPoliza to set
	 */
	public void setIdToPoliza(String idToPoliza) {
		this.idToPoliza = idToPoliza;
	}
	/**
	 * @return the numeroPoliza
	 */
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	/**
	 * @param numeroPoliza the numeroPoliza to set
	 */
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	/**
	 * @return the situacion
	 */
	public String getSituacion() {
		return situacion;
	}
	/**
	 * @param situacion the situacion to set
	 */
	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}
	/**
	 * @return the ultimoReciboPagado
	 */
	public BigDecimal getUltimoReciboPagado() {
		return ultimoReciboPagado;
	}
	/**
	 * @param ultimoReciboPagado the ultimoReciboPagado to set
	 */
	public void setUltimoReciboPagado(BigDecimal ultimoReciboPagado) {
		this.ultimoReciboPagado = ultimoReciboPagado;
	}
	/**
	 * @return the fechaVencimiento
	 */
	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}
	/**
	 * @param fechaVencimiento the fechaVencimiento to set
	 */
	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	/**
	 * @return the fechaUltimoPago
	 */
	public Date getFechaUltimoPago() {
		return fechaUltimoPago;
	}
	/**
	 * @param fechaUltimoPago the fechaUltimoPago to set
	 */
	public void setFechaUltimoPago(Date fechaUltimoPago) {
		this.fechaUltimoPago = fechaUltimoPago;
	}
	/**
	 * @return the montoPagado
	 */
	public BigDecimal getMontoPagado() {
		return montoPagado;
	}
	/**
	 * @param montoPagado the montoPagado to set
	 */
	public void setMontoPagado(BigDecimal montoPagado) {
		this.montoPagado = montoPagado;
	}
	/**
	 * @return the montoPorVencer
	 */
	public BigDecimal getMontoPorVencer() {
		return montoPorVencer;
	}
	/**
	 * @param montoPorVencer the montoPorVencer to set
	 */
	public void setMontoPorVencer(BigDecimal montoPorVencer) {
		this.montoPorVencer = montoPorVencer;
	}
	/**
	 * @return the montoVencido
	 */
	public BigDecimal getMontoVencido() {
		return montoVencido;
	}
	/**
	 * @param montoVencido the montoVencido to set
	 */
	public void setMontoVencido(BigDecimal montoVencido) {
		this.montoVencido = montoVencido;
	}

	
}
