/**
 * 
 */
package mx.com.afirme.midas.interfaz.poliza;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import javax.ejb.Remote;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;

/**
 * @author andres.avalos
 * 
 */

public interface PolizaFacadeRemote {
	
	public static final String CONV_TYPE_LINE = "LINEA";

	public Map<String, String> emitePoliza(CotizacionDTO cotizacionDTO) throws Exception;

	public String consultaDatosPoliza(BigDecimal idCotizacionSeycos,String claveConsulta);
	
	public BigDecimal obtenerIdToCotizacionDePolizaSeycos(BigDecimal idCotizacionSeycos);
	
	public String obtenerFolioPolizaSeycos(BigDecimal idCotizacionSeycos);
	
	public BigDecimal getConvertion(String type,String midasId);
	
	public Date obtenerFechaEmisionPoliza(BigDecimal idCotizacionSeycos);
	
	public boolean esPolizaCasaNueva(BigDecimal idCotizacionSeycos);
		
	public boolean reexpidePoliza(BigDecimal idPoliza, BigDecimal idCotizacion);
	
	
}
