package mx.com.afirme.midas.interfaz.recibo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

public class ReciboDTO implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	private Long numeroFolioRecibo; 		//n�mero de recibo
	private Short numeroExhibicion; 			//n�mero de exhibici�n
	private String llaveFiscal; 			//Identificador para invocar el Webservice y generar el pdf
	private Double importe; 				//Importe a Pagar
	private Date fechaLimitePago; 			//Fecha l�mite de pago
	private Long solicitudCheque;			//Numero de solicitud de cheque devolucion primas
	
	private String origenRecibo;
	private String numeroEndoso;
	private String situacion;
	private Date fechaSituacion;
	private String origenSituacion;
	private Date fechaIncioVigencia;
	private Date fechaFinVigencia;
	private BigDecimal idPoliza; 			//(No nulo) Identificador de Poliza de Midas
	private BigDecimal idCotizacion;
	private BigDecimal idLinea;
	private BigDecimal idInciso;
	private BigDecimal idRecibo;
	private BigDecimal idMoneda;
	private BigDecimal folioFiscal;
	private String serieFiscal;
	
	private String generacionRecibo;
	
	public int getDiasVencido() {
		Calendar cal = Calendar.getInstance();		
		return fechaLimitePago.compareTo(cal.getTime());
	}


	public BigDecimal getFolioFiscal() {
		return folioFiscal;
	}


	public void setFolioFiscal(BigDecimal folioFiscal) {
		this.folioFiscal = folioFiscal;
	}


	public String getSerieFiscal() {
		return serieFiscal;
	}


	public void setSerieFiscal(String serieFiscal) {
		this.serieFiscal = serieFiscal;
	}


	public BigDecimal getIdMoneda() {
		return idMoneda;
	}


	public void setIdMoneda(BigDecimal idMoneda) {
		this.idMoneda = idMoneda;
	}


	public BigDecimal getIdCotizacion() {
		return idCotizacion;
	}


	public void setIdCotizacion(BigDecimal idCotizacion) {
		this.idCotizacion = idCotizacion;
	}


	public BigDecimal getIdLinea() {
		return idLinea;
	}


	public void setIdLinea(BigDecimal idLinea) {
		this.idLinea = idLinea;
	}


	public BigDecimal getIdInciso() {
		return idInciso;
	}


	public void setIdInciso(BigDecimal idInciso) {
		this.idInciso = idInciso;
	}


	public BigDecimal getIdRecibo() {
		return idRecibo;
	}


	public void setIdRecibo(BigDecimal idRecibo) {
		this.idRecibo = idRecibo;
	}


	/**
	 * @return the numeroFolioRecibo
	 */
	public Long getNumeroFolioRecibo() {
		return numeroFolioRecibo;
	}


	/**
	 * @param numeroFolioRecibo the numeroFolioRecibo to set
	 */
	public void setNumeroFolioRecibo(Long numeroFolioRecibo) {
		this.numeroFolioRecibo = numeroFolioRecibo;
	}


	/**
	 * @return the numeroExhibicion
	 */
	public Short getNumeroExhibicion() {
		return numeroExhibicion;
	}


	/**
	 * @param numeroExhibicion the numeroExhibicion to set
	 */
	public void setNumeroExhibicion(Short numeroExhibicion) {
		this.numeroExhibicion = numeroExhibicion;
	}


	/**
	 * @return the llaveFiscal
	 */
	public String getLlaveFiscal() {
		return llaveFiscal;
	}


	/**
	 * @param llaveFiscal the llaveFiscal to set
	 */
	public void setLlaveFiscal(String llaveFiscal) {
		this.llaveFiscal = llaveFiscal;
	}


	/**
	 * @return the importe
	 */
	public Double getImporte() {
		return importe;
	}


	/**
	 * @param importe the importe to set
	 */
	public void setImporte(Double importe) {
		this.importe = importe;
	}


	/**
	 * @return the fechaLimitePago
	 */
	public Date getFechaLimitePago() {
		return fechaLimitePago;
	}


	/**
	 * @param fechaLimitePago the fechaLimitePago to set
	 */
	public void setFechaLimitePago(Date fechaLimitePago) {
		this.fechaLimitePago = fechaLimitePago;
	}


	public Long getSolicitudCheque() {
		return solicitudCheque;
	}


	public void setSolicitudCheque(Long solicitudCheque) {
		this.solicitudCheque = solicitudCheque;
	}


	public String getOrigenRecibo() {
		return origenRecibo;
	}


	public void setOrigenRecibo(String origenRecibo) {
		this.origenRecibo = origenRecibo;
	}


	public String getNumeroEndoso() {
		return numeroEndoso;
	}


	public void setNumeroEndoso(String numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}


	public String getSituacion() {
		return situacion;
	}


	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}


	public Date getFechaSituacion() {
		return fechaSituacion;
	}


	public void setFechaSituacion(Date fechaSituacion) {
		this.fechaSituacion = fechaSituacion;
	}


	public String getOrigenSituacion() {
		return origenSituacion;
	}


	public void setOrigenSituacion(String origenSituacion) {
		this.origenSituacion = origenSituacion;
	}


	/**
	 * @return the idPoliza
	 */
	public BigDecimal getIdPoliza() {
		return idPoliza;
	}


	/**
	 * @param idPoliza the idPoliza to set
	 */
	public void setIdPoliza(BigDecimal idPoliza) {
		this.idPoliza = idPoliza;
	}


	public Date getFechaIncioVigencia() {
		return fechaIncioVigencia;
	}


	public void setFechaIncioVigencia(Date fechaIncioVigencia) {
		this.fechaIncioVigencia = fechaIncioVigencia;
	}


	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}


	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}
	
	public String getGeneracionRecibo() {
		return generacionRecibo;
	}
	
	public void setGeneracionRecibo (String generacionRecibo) {
		this.generacionRecibo = generacionRecibo;
	}
}