package mx.com.afirme.midas.interfaz.recibo;

import java.math.BigDecimal;
import java.util.List;

public interface ReciboFacadeRemote {

	public List<ReciboDTO> emiteRecibosPoliza(BigDecimal idPoliza, String nombreUsuario)
		throws Exception;
	
	/**
	 * Consulta la informaci�n de los Recibos de una P�liza, indicando tambi�n el numero de endoso
	 * @param numPolizaEndoso (Id de la p�liza | Numero del Endoso)
	 * @param nombreUsuario Nombre del Usuario que realiza la operacion
	 * @return Los recibos de la p�liza/endoso
	 * @throws Exception
	 */
	public List<ReciboDTO> consultaRecibos(String numPolizaEndoso, String nombreUsuario)
		throws Exception;
	
	/**
	 * @param insurancePolicy (00000-0000000000-00)
	 * @param subsection Número de inciso
	 * @param siglasRFC Siglas del RFC (3 personas morales, 4 personas fisicas)
	 * @param fechaRFC Fecha del RFC (en formato ddMMyy)
	 * @param homoClaveRFC Homoclave del RFC (3 digitos alfanumericos)
	 * @param filterReceipts filtrar recibos a mostrar por id
	 * @return Listado de Recibos
	 */
	public List<ReciboDTO> findByPolicyAndRFC(String insurancePolicy, String subsection, String siglasRFC, String fechaRFC, String homoClaveRFC) throws Exception;
	public List<ReciboDTO> findByPolicy(String insurancePolicy, String subsection) throws Exception;
	public List<ReciboDTO> findByPolicy(String insurancePolicy) throws Exception;
	/**
	 * @param ids Lista de Ids de los recibos
	 * @return Listado de Recibos
	 */
	public List<ReciboDTO> findById(String insurancePolicy, String subsection, String [] ids) throws Exception;
	public List<ReciboDTO> findById(String insurancePolicy, String [] ids) throws Exception;
}
