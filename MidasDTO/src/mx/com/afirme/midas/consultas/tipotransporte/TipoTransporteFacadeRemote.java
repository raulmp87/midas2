package mx.com.afirme.midas.consultas.tipotransporte;

// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for TipoTransporteFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface TipoTransporteFacadeRemote extends
		MidasInterfaceBase<TipoTransporteDTO> {
	/**
	 * Perform an initial save of a previously unsaved TipoTransporteDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            TipoTransporteDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoTransporteDTO entity);

	/**
	 * Delete a persistent TipoTransporteDTO entity.
	 * 
	 * @param entity
	 *            TipoTransporteDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoTransporteDTO entity);

	/**
	 * Persist a previously saved TipoTransporteDTO entity and return it or a
	 * copy of it to the sender. A copy of the TipoTransporteDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoTransporteDTO entity to update
	 * @return TipoTransporteDTO the persisted TipoTransporteDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoTransporteDTO update(TipoTransporteDTO entity);

	/**
	 * Find all TipoTransporteDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the TipoTransporteDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoTransporteDTO> found by query
	 */
	public List<TipoTransporteDTO> findByProperty(String propertyName,
			Object value);

	public List<TipoTransporteDTO> listarFiltrado(
			TipoTransporteDTO tipoTransporteDTO);
}