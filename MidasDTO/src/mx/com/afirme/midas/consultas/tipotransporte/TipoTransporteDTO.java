package mx.com.afirme.midas.consultas.tipotransporte;

// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.base.CacheableDTO;

/**
 * TipoTransporteDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name = "TipoTransporteDTO")
@Table(name = "TCTIPOTRANSPORTE", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = "CODIGOTIPOTRANSPORTE"))
public class TipoTransporteDTO extends CacheableDTO {

	private static final long serialVersionUID = 5043379657014065408L;

	private BigDecimal idTipoTransporte;
	private BigDecimal codigoTipoTransporte;
	private String descripcionTipoTransporte;

	// Constructors

	/** default constructor */
	public TipoTransporteDTO() {
	}

	/** full constructor */
	public TipoTransporteDTO(BigDecimal idTipoTransporte,
			BigDecimal codigoTipoTransporte, String descripcionTipoTransporte) {
		this.idTipoTransporte = idTipoTransporte;
		this.codigoTipoTransporte = codigoTipoTransporte;
		this.descripcionTipoTransporte = descripcionTipoTransporte;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTCTIPOTRANSPORTE_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCTIPOTRANSPORTE_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCTIPOTRANSPORTE_SEQ_GENERADOR")
	@Column(name = "IDTCTIPOTRANSPORTE", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTipoTransporte() {
		return this.idTipoTransporte;
	}

	public void setIdTipoTransporte(BigDecimal idTipoTransporte) {
		this.idTipoTransporte = idTipoTransporte;
	}

	@Column(name = "CODIGOTIPOTRANSPORTE", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoTipoTransporte() {
		return this.codigoTipoTransporte;
	}

	public void setCodigoTipoTransporte(BigDecimal codigoTipoTransporte) {
		this.codigoTipoTransporte = codigoTipoTransporte;
	}

	@Column(name = "DESCRIPCIONTIPOTRANSPORTE", nullable = false, length = 200)
	public String getDescripcionTipoTransporte() {
		return this.descripcionTipoTransporte;
	}

	public void setDescripcionTipoTransporte(String descripcionTipoTransporte) {
		this.descripcionTipoTransporte = descripcionTipoTransporte;
	}

	@Override
	public String getDescription() {
		return this.descripcionTipoTransporte;
	}

	@Override
	public Object getId() {
		return this.idTipoTransporte;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;

		if (o instanceof TipoTransporteDTO) {
			if (((TipoTransporteDTO) o).getCodigoTipoTransporte().equals(
					this.getCodigoTipoTransporte()))
				return true;
		}
		return false;
	}

}