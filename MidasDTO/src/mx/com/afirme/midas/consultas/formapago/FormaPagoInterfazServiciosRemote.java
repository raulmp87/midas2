package mx.com.afirme.midas.consultas.formapago;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.interfaz.formapago.FormaPagoIDTO;
import mx.com.afirme.midas.sistema.SystemException;


public interface FormaPagoInterfazServiciosRemote {
	
	public List<FormaPagoIDTO> listarTodos(Short idMoneda,String nombreUsuario)	throws SystemException;
	
	public List<FormaPagoIDTO> listarFiltrado(Integer idFormaPago, String descripcion, Short idMoneda,String nombreUsuario) throws SystemException;
	
	public FormaPagoIDTO getPorId(Integer idFormaPago,Short idMoneda,String nombreUsuario) throws SystemException;
	
	public FormaPagoIDTO getPorIdClaveNegocio(Integer idFormaPago,Short idMoneda,String nombreUsuario, String claveNegocio) throws SystemException;
}
