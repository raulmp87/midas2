package mx.com.afirme.midas.consultas.formapago;

// default package

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.CotizacionExpressPaso2Checks;

/**
 * FormaPagoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name = "FormaPagoDTO")
@Table(name = "VNFORMAPAGO", schema = "MIDAS")
public class FormaPagoDTO extends CacheableDTO implements Entidad {

	private static final long serialVersionUID = -7208451564621889932L;
	private Integer idFormaPago;
	private String descripcion;
	private List<ProductoDTO> productos;
	

	// Constructors
	public FormaPagoDTO() {
	}

	public FormaPagoDTO(Integer idFormaPago, String descripcion) {
		this.idFormaPago = idFormaPago;
		this.descripcion = descripcion;
	}

	@Id
	@Column(name = "IDTCFORMAPAGO", nullable = false, precision = 5, scale = 0)
	@NotNull(groups=CotizacionExpressPaso2Checks.class, message="{com.afirme.midas2.requerido}")
	public Integer getIdFormaPago() {
		return idFormaPago;
	}

	public void setIdFormaPago(Integer idFormaPago) {
		this.idFormaPago = idFormaPago;
	}
		
	@Column(name = "DESCRIPCION", nullable = false, length = 30)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	

	@ManyToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinTable(name = "TRFORMAPAGOPRODUCTO", schema = "MIDAS",
			joinColumns = {
				@JoinColumn(name="IDTCFORMAPAGO") 
			},
			inverseJoinColumns = {
				@JoinColumn(name="IDTOPRODUCTO")
			}
	)
	public List<ProductoDTO> getProductos() {
		return productos;
	}

	public void setProductos(List<ProductoDTO> productos) {
		this.productos = productos;
	}

	@Override
	public Object getId() {
		return this.idFormaPago;
	}

	@Override
	public String getDescription() {
		return this.descripcion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((descripcion == null) ? 0 : descripcion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormaPagoDTO other = (FormaPagoDTO) obj;
		if (idFormaPago == null) {
			if (other.idFormaPago != null)
				return false;
		} else if (!idFormaPago.equals(other.idFormaPago))
			return false;
		return true;
	}

	@Override
	public Integer getKey() {	
		return idFormaPago;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return this.descripcion;
	}

	@Override
	public Integer getBusinessKey() {
		// TODO Auto-generated method stub
		return this.idFormaPago;
	}
	
}