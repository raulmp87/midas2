package mx.com.afirme.midas.consultas.formapago;
// default package

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for FormaPagoFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface FormaPagoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved FormaPago entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            FormaPago entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(FormaPagoDTO entity);

	/**
	 * Delete a persistent FormaPagoDTO entity.
	 * 
	 * @param entity
	 *            FormaPagoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(FormaPagoDTO entity);

	/**
	 * Persist a previously saved FormaPagoDTO entity and return it or a copy of it
	 * to the sender. A copy of the FormaPagoDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            FormaPagoDTO entity to update
	 * @return FormaPagoDTO the persisted FormaPagoDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public FormaPagoDTO update(FormaPagoDTO entity);

	public FormaPagoDTO findById(FormaPagoDTO id);

	/**
	 * Find all FormaPagoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the FormaPagoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<FormaPagoDTO> found by query
	 */
	public List<FormaPagoDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all FormaPagoDTO entities.
	 * 
	 * @return List<FormaPagoDTO> all FormaPagoDTO entities
	 */
	public List<FormaPagoDTO> findAll();
	
	public List<FormaPagoDTO> listarFiltrado(FormaPagoDTO formaPagoDTO);
}