package mx.com.afirme.midas.consultas.gastoexpedicion;

// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for GastoExpedicionDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface GastoExpedicionFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved GastoExpedicionDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            GastoExpedicionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(GastoExpedicionDTO entity);

	/**
	 * Delete a persistent GastoExpedicionDTO entity.
	 * 
	 * @param entity
	 *            GastoExpedicionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(GastoExpedicionDTO entity);

	/**
	 * Persist a previously saved GastoExpedicionDTO entity and return it or a
	 * copy of it to the sender. A copy of the GastoExpedicionDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            GastoExpedicionDTO entity to update
	 * @return GastoExpedicionDTO the persisted GastoExpedicionDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public GastoExpedicionDTO update(GastoExpedicionDTO entity);

	public GastoExpedicionDTO findById(GastoExpedicionId id);

	/**
	 * Find all GastoExpedicionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the GastoExpedicionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<GastoExpedicionDTO> found by query
	 */
	public List<GastoExpedicionDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all GastoExpedicionDTO entities.
	 * 
	 * @return List<GastoExpedicionDTO> all GastoExpedicionDTO entities
	 */
	public List<GastoExpedicionDTO> findAll();

	public GastoExpedicionDTO getGastoExpedicion(BigDecimal idMoneda,
			BigDecimal primaNeta);

}