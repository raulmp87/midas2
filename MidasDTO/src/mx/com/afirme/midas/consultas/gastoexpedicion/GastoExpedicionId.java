package mx.com.afirme.midas.consultas.gastoexpedicion;

// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * GastoExpedicionDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class GastoExpedicionId implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idMoneda;
	private BigDecimal primaNetaMinima;
	private BigDecimal primaNetaMaxima;

	// Constructors

	/** default constructor */
	public GastoExpedicionId() {
	}

	/** full constructor */
	public GastoExpedicionId(BigDecimal idMoneda, BigDecimal primaNetaMinima,
			BigDecimal primaNetaMaxima) {
		this.idMoneda = idMoneda;
		this.primaNetaMinima = primaNetaMinima;
		this.primaNetaMaxima = primaNetaMaxima;
	}

	// Property accessors

	@Column(name = "IdMoneda", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdMoneda() {
		return this.idMoneda;
	}

	public void setIdMoneda(BigDecimal idMoneda) {
		this.idMoneda = idMoneda;
	}

	@Column(name = "PrimaNetaMinima", nullable = false, precision = 22, scale = 0)
	public BigDecimal getPrimaNetaMinima() {
		return this.primaNetaMinima;
	}

	public void setPrimaNetaMinima(BigDecimal primaNetaMinima) {
		this.primaNetaMinima = primaNetaMinima;
	}

	@Column(name = "PrimaNetaMaxima", nullable = false, precision = 22, scale = 0)
	public BigDecimal getPrimaNetaMaxima() {
		return this.primaNetaMaxima;
	}

	public void setPrimaNetaMaxima(BigDecimal primaNetaMaxima) {
		this.primaNetaMaxima = primaNetaMaxima;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof GastoExpedicionId))
			return false;
		GastoExpedicionId castOther = (GastoExpedicionId) other;

		return ((this.getIdMoneda() == castOther.getIdMoneda()) || (this
				.getIdMoneda() != null
				&& castOther.getIdMoneda() != null && this.getIdMoneda()
				.equals(castOther.getIdMoneda())))
				&& ((this.getPrimaNetaMinima() == castOther
						.getPrimaNetaMinima()) || (this.getPrimaNetaMinima() != null
						&& castOther.getPrimaNetaMinima() != null && this
						.getPrimaNetaMinima().equals(
								castOther.getPrimaNetaMinima())))
				&& ((this.getPrimaNetaMaxima() == castOther
						.getPrimaNetaMaxima()) || (this.getPrimaNetaMaxima() != null
						&& castOther.getPrimaNetaMaxima() != null && this
						.getPrimaNetaMaxima().equals(
								castOther.getPrimaNetaMaxima())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getIdMoneda() == null ? 0 : this.getIdMoneda().hashCode());
		result = 37
				* result
				+ (getPrimaNetaMinima() == null ? 0 : this.getPrimaNetaMinima()
						.hashCode());
		result = 37
				* result
				+ (getPrimaNetaMaxima() == null ? 0 : this.getPrimaNetaMaxima()
						.hashCode());
		return result;
	}

}