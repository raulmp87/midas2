package mx.com.afirme.midas.consultas.gastoexpedicion;

// default package

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * GastoExpedicionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCGASTOEXPEDICION", schema = "MIDAS")
public class GastoExpedicionDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private GastoExpedicionId id;
	private Double gastoExpedicionPoliza;
	private Double gastoExpedicionEndosoAumento;

	// Constructors

	/** default constructor */
	public GastoExpedicionDTO() {
	}

	/** full constructor */
	public GastoExpedicionDTO(GastoExpedicionId id,
			Double gastoExpedicionPoliza, Double gastoExpedicionEndosoAumento) {
		this.id = id;
		this.gastoExpedicionPoliza = gastoExpedicionPoliza;
		this.gastoExpedicionEndosoAumento = gastoExpedicionEndosoAumento;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idMoneda", column = @Column(name = "IDMONEDA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "primaNetaMinima", column = @Column(name = "PRIMANETAMINIMA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "primaNetaMaxima", column = @Column(name = "PRIMANETAMAXIMA", nullable = false, precision = 22, scale = 0)) })
	public GastoExpedicionId getId() {
		return this.id;
	}

	public void setId(GastoExpedicionId id) {
		this.id = id;
	}

	@Column(name = "GastoExpedicionPoliza", nullable = false, precision = 16)
	public Double getGastoExpedicionPoliza() {
		return this.gastoExpedicionPoliza;
	}

	public void setGastoExpedicionPoliza(Double gastoExpedicionPoliza) {
		this.gastoExpedicionPoliza = gastoExpedicionPoliza;
	}

	@Column(name = "GastoExpedicionEndosoAumento", nullable = false, precision = 16)
	public Double getGastoExpedicionEndosoAumento() {
		return this.gastoExpedicionEndosoAumento;
	}

	public void setGastoExpedicionEndosoAumento(
			Double gastoExpedicionEndosoAumento) {
		this.gastoExpedicionEndosoAumento = gastoExpedicionEndosoAumento;
	}

}