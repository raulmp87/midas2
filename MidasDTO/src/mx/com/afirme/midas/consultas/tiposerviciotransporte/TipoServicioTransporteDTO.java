package mx.com.afirme.midas.consultas.tiposerviciotransporte;

// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.base.CacheableDTO;

/**
 * TipoServicioTransporteDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name = "TipoServicioTransporteDTO")
@Table(name = "TCTIPOSERVICIOTRANSPORTE", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = "CODIGOTIPOSERVICIOTRANSPORTE"))
public class TipoServicioTransporteDTO extends CacheableDTO {

	private static final long serialVersionUID = 5546164401596971117L;

	private BigDecimal idTipoServicioTransporte;
	private BigDecimal codigoTipoServicioTransporte;
	private String descripcionTipoServicioTra;

	// Constructors
	public TipoServicioTransporteDTO() {
	}

	public TipoServicioTransporteDTO(BigDecimal idTipoServicioTransporte,
			BigDecimal codigoTipoServicioTransporte,
			String descripcionTipoServicioTra) {
		this.idTipoServicioTransporte = idTipoServicioTransporte;
		this.codigoTipoServicioTransporte = codigoTipoServicioTransporte;
		this.descripcionTipoServicioTra = descripcionTipoServicioTra;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTCTIPOSERVICIOTRANSP_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCTIPOSERVICIOTRANSP_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCTIPOSERVICIOTRANSP_SEQ_GENERADOR")
	@Column(name = "IDTCTIPOSERVICIOTRANSPORTE", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTipoServicioTransporte() {
		return this.idTipoServicioTransporte;
	}

	public void setIdTipoServicioTransporte(BigDecimal idTipoServicioTransporte) {
		this.idTipoServicioTransporte = idTipoServicioTransporte;
	}

	@Column(name = "CODIGOTIPOSERVICIOTRANSPORTE", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoTipoServicioTransporte() {
		return this.codigoTipoServicioTransporte;
	}

	public void setCodigoTipoServicioTransporte(
			BigDecimal codigoTipoServicioTransporte) {
		this.codigoTipoServicioTransporte = codigoTipoServicioTransporte;
	}

	@Column(name = "DESCRIPCIONTIPOSERVICIOTRA", nullable = false, length = 200)
	public String getDescripcionTipoServicioTra() {
		return this.descripcionTipoServicioTra;
	}

	public void setDescripcionTipoServicioTra(String descripcionTipoServicioTra) {
		this.descripcionTipoServicioTra = descripcionTipoServicioTra;
	}

	@Override
	public String getDescription() {
		return this.descripcionTipoServicioTra;
	}

	@Override
	public Object getId() {
		return this.idTipoServicioTransporte;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;

		if (o instanceof TipoServicioTransporteDTO) {
			if (((TipoServicioTransporteDTO) o)
					.getCodigoTipoServicioTransporte().equals(
							this.getCodigoTipoServicioTransporte()))
				return true;
		}
		return false;
	}

}