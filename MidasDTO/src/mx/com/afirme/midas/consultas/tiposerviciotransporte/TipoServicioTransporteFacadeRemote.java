package mx.com.afirme.midas.consultas.tiposerviciotransporte;

// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for TipoServicioTransporteFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface TipoServicioTransporteFacadeRemote extends
		MidasInterfaceBase<TipoServicioTransporteDTO> {
	/**
	 * Perform an initial save of a previously unsaved TipoServicioTransporteDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            TipoServicioTransporteDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoServicioTransporteDTO entity);

	/**
	 * Delete a persistent TipoServicioTransporteDTO entity.
	 * 
	 * @param entity
	 *            TipoServicioTransporteDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoServicioTransporteDTO entity);

	/**
	 * Persist a previously saved TipoServicioTransporteDTO entity and return it
	 * or a copy of it to the sender. A copy of the TipoServicioTransporteDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoServicioTransporteDTO entity to update
	 * @return TipoServicioTransporteDTO the persisted TipoServicioTransporteDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoServicioTransporteDTO update(TipoServicioTransporteDTO entity);

	/**
	 * Find all TipoServicioTransporteDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the TipoServicioTransporteDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoServicioTransporteDTO> found by query
	 */
	public List<TipoServicioTransporteDTO> findByProperty(String propertyName,
			Object value);

	public List<TipoServicioTransporteDTO> listarFiltrado(
			TipoServicioTransporteDTO tipoServicioTransporteDTO);
}