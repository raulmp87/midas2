package mx.com.afirme.midas.consultas.tipopropietariocamion;
// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for TipoPropietarioCamionFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface TipoPropietarioCamionFacadeRemote extends MidasInterfaceBase<TipoPropietarioCamionDTO> {
	/**
	 * Perform an initial save of a previously unsaved TipoPropietarioCamionDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            TipoPropietarioCamionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoPropietarioCamionDTO entity);

	/**
	 * Delete a persistent TipoPropietarioCamionDTO entity.
	 * 
	 * @param entity
	 *            TipoPropietarioCamionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoPropietarioCamionDTO entity);

	/**
	 * Persist a previously saved TipoPropietarioCamionDTO entity and return it or
	 * a copy of it to the sender. A copy of the TipoPropietarioCamionDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoPropietarioCamionDTO entity to update
	 * @return TipoPropietarioCamionDTO the persisted TipoPropietarioCamionDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoPropietarioCamionDTO update(TipoPropietarioCamionDTO entity);

	/**
	 * Find all TipoPropietarioCamionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the TipoPropietarioCamionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoPropietarioCamionDTO> found by query
	 */
	public List<TipoPropietarioCamionDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all TipoPropietarioCamionDTO entities.
	 * 
	 * @return List<TipoPropietarioCamionDTO> all TipoPropietarioCamionDTO entities
	 */
	public List<TipoPropietarioCamionDTO> findAll();
	
	public List<TipoPropietarioCamionDTO> listarFiltrado(TipoPropietarioCamionDTO tipoPropietarioCamionDTO);
}