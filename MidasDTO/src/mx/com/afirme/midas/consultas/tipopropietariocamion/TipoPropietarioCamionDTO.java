package mx.com.afirme.midas.consultas.tipopropietariocamion;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.base.CacheableDTO;

/**
 * TipoPropietarioCamionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name = "TipoPropietarioCamionDTO")
@Table(name = "TCTIPOPROPIETARIOTRANSPORTE", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = "CODIGOTIPOPROPTRANSPORTE"))
public class TipoPropietarioCamionDTO extends CacheableDTO implements java.io.Serializable {

	private static final long serialVersionUID = 1766214188639298926L;

	private BigDecimal idTipoPropietarioTransporte;
	private BigDecimal codigoTipoPropTransporte;
	private String 	   descripcionTipoPropietarioTra;

	// Constructors
	public TipoPropietarioCamionDTO() {
	}

	public TipoPropietarioCamionDTO(BigDecimal idTipoPropietarioTransporte,
			BigDecimal codigoTipoPropTransporte,
			String descripcionTipoPropietarioTra) {
		this.idTipoPropietarioTransporte   = idTipoPropietarioTransporte;
		this.codigoTipoPropTransporte      = codigoTipoPropTransporte;
		this.descripcionTipoPropietarioTra = descripcionTipoPropietarioTra;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTCTIPOPROPIETARIOTRANSP_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCTIPOPROPIETARIOTRANSP_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCTIPOPROPIETARIOTRANSP_SEQ_GENERADOR")
	@Column(name = "IDTCTIPOPROPIETARIOTRANSPORTE", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTipoPropietarioTransporte() {
		return this.idTipoPropietarioTransporte;
	}

	public void setIdTipoPropietarioTransporte(
			BigDecimal idTipoPropietarioTransporte) {
		this.idTipoPropietarioTransporte = idTipoPropietarioTransporte;
	}

	@Column(name = "CODIGOTIPOPROPTRANSPORTE", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoTipoPropTransporte() {
		return this.codigoTipoPropTransporte;
	}

	public void setCodigoTipoPropTransporte(BigDecimal codigoTipoPropTransporte) {
		this.codigoTipoPropTransporte = codigoTipoPropTransporte;
	}

	@Column(name = "DESCRIPCIONTIPOPROPIETARIOTRA", nullable = false, length = 200)
	public String getDescripcionTipoPropietarioTra() {
		return this.descripcionTipoPropietarioTra;
	}

	public void setDescripcionTipoPropietarioTra(
			String descripcionTipoPropietarioTra) {
		this.descripcionTipoPropietarioTra = descripcionTipoPropietarioTra;
	}

	@Override
	public String getDescription() {
		return this.descripcionTipoPropietarioTra;
	}

	@Override
	public Object getId() {
		return this.idTipoPropietarioTransporte;
	}

	@Override
	public boolean equals(Object o) {
		if(o == null)
			return false;
		
		if(o instanceof TipoPropietarioCamionDTO){
			if(((TipoPropietarioCamionDTO) o) .getCodigoTipoPropTransporte() .equals(this.getCodigoTipoPropTransporte()))
				return true;
		}
		return false;
	}

}