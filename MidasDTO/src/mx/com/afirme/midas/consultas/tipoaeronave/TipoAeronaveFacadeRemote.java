package mx.com.afirme.midas.consultas.tipoaeronave;

// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for TipoAeronaveFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface TipoAeronaveFacadeRemote extends
		MidasInterfaceBase<TipoAeronaveDTO> {
	/**
	 * Perform an initial save of a previously unsaved TipoAeronaveDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            TipoAeronaveDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoAeronaveDTO entity);

	/**
	 * Delete a persistent TipoAeronaveDTO entity.
	 * 
	 * @param entity
	 *            TipoAeronaveDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoAeronaveDTO entity);

	/**
	 * Persist a previously saved TipoAeronaveDTO entity and return it or a copy
	 * of it to the sender. A copy of the TipoAeronaveDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoAeronaveDTO entity to update
	 * @return TipoAeronaveDTO the persisted TipoAeronaveDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoAeronaveDTO update(TipoAeronaveDTO entity);

	/**
	 * Find all TipoAeronaveDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the TipoAeronaveDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoAeronaveDTO> found by query
	 */
	public List<TipoAeronaveDTO> findByProperty(String propertyName,
			Object value);

	public List<TipoAeronaveDTO> listarFiltrado(TipoAeronaveDTO tipoAeronaveDTO);
}