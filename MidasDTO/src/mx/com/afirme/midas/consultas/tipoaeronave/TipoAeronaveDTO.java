package mx.com.afirme.midas.consultas.tipoaeronave;

// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.base.CacheableDTO;

/**
 * TipoAeronaveDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name = "TipoAeronaveDTO")
@Table(name = "TCTIPOAERONAVE", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = "CODIGOTIPOAERONAVE"))
public class TipoAeronaveDTO extends CacheableDTO {

	private static final long serialVersionUID = 7563769454687462210L;

	private BigDecimal idTipoAeronave;
	private BigDecimal codigoTipoAeronave;
	private String descripcionTipoAeronave;

	// Constructors
	public TipoAeronaveDTO() {
	}

	public TipoAeronaveDTO(BigDecimal idTipoAeronave,
			BigDecimal codigoTipoAeronave, String descripcionTipoAeronave) {
		this.idTipoAeronave = idTipoAeronave;
		this.codigoTipoAeronave = codigoTipoAeronave;
		this.descripcionTipoAeronave = descripcionTipoAeronave;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTCTIPOAERONAVE_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCTIPOAERONAVE_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCTIPOAERONAVE_SEQ_GENERADOR")
	@Column(name = "IDTCTIPOAERONAVE", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTipoAeronave() {
		return this.idTipoAeronave;
	}

	public void setIdTipoAeronave(BigDecimal idTipoAeronave) {
		this.idTipoAeronave = idTipoAeronave;
	}

	@Column(name = "CODIGOTIPOAERONAVE", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoTipoAeronave() {
		return this.codigoTipoAeronave;
	}

	public void setCodigoTipoAeronave(BigDecimal codigoTipoAeronave) {
		this.codigoTipoAeronave = codigoTipoAeronave;
	}

	@Column(name = "DESCRIPCIONTIPOAERONAVE", nullable = false, length = 200)
	public String getDescripcionTipoAeronave() {
		return this.descripcionTipoAeronave;
	}

	public void setDescripcionTipoAeronave(String descripcionTipoAeronave) {
		this.descripcionTipoAeronave = descripcionTipoAeronave;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;

		if (o instanceof TipoAeronaveDTO) {
			if (((TipoAeronaveDTO) o).getCodigoTipoAeronave().equals(
					this.getCodigoTipoAeronave()))
				return true;
		}

		return false;
	}

	@Override
	public String getDescription() {
		return this.descripcionTipoAeronave;
	}

	@Override
	public Object getId() {
		return this.idTipoAeronave;
	}
}