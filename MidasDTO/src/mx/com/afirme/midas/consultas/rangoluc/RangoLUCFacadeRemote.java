package mx.com.afirme.midas.consultas.rangoluc;

// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for RangoLUCDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface RangoLUCFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved RangoLUCDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            RangoLUCDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(RangoLUCDTO entity);

	/**
	 * Delete a persistent RangoLUCDTO entity.
	 * 
	 * @param entity
	 *            RangoLUCDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(RangoLUCDTO entity);

	/**
	 * Persist a previously saved RangoLUCDTO entity and return it or a copy of
	 * it to the sender. A copy of the RangoLUCDTO entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            RangoLUCDTO entity to update
	 * @return RangoLUCDTO the persisted RangoLUCDTO entity instance, may not be
	 *         the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public RangoLUCDTO update(RangoLUCDTO entity);

	public RangoLUCDTO findById(BigDecimal id);

	/**
	 * Find all RangoLUCDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the RangoLUCDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<RangoLUCDTO> found by query
	 */
	public List<RangoLUCDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all RangoLUCDTO entities.
	 * 
	 * @return List<RangoLUCDTO> all RangoLUCDTO entities
	 */
	public List<RangoLUCDTO> findAll();
	
	public RangoLUCDTO getLimiteInferior();
}