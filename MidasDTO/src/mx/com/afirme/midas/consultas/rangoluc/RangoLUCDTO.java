package mx.com.afirme.midas.consultas.rangoluc;

// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * RangoLUCDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TORANGOLUC", schema = "MIDAS")
public class RangoLUCDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idtorangoluc;
	private BigDecimal limiteInferior;
	private BigDecimal limiteSuperior;
	private Double porcentajeDeCuota;

	// Constructors

	/** default constructor */
	public RangoLUCDTO() {
	}

	/** full constructor */
	public RangoLUCDTO(BigDecimal idtorangoluc, BigDecimal limiteInferior,
			BigDecimal limiteSuperior, Double porcentajeDeCuota) {
		this.idtorangoluc = idtorangoluc;
		this.limiteInferior = limiteInferior;
		this.limiteSuperior = limiteSuperior;
		this.porcentajeDeCuota = porcentajeDeCuota;
	}

	// Property accessors
	@Id
	@Column(name = "IDTORANGOLUC", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdtorangoluc() {
		return this.idtorangoluc;
	}

	public void setIdtorangoluc(BigDecimal idtorangoluc) {
		this.idtorangoluc = idtorangoluc;
	}

	@Column(name = "LimiteInferior", nullable = false, precision = 22, scale = 0)
	public BigDecimal getLimiteInferior() {
		return this.limiteInferior;
	}

	public void setLimiteInferior(BigDecimal limiteInferior) {
		this.limiteInferior = limiteInferior;
	}

	@Column(name = "LimiteSuperior", nullable = false, precision = 22, scale = 0)
	public BigDecimal getLimiteSuperior() {
		return this.limiteSuperior;
	}

	public void setLimiteSuperior(BigDecimal limiteSuperior) {
		this.limiteSuperior = limiteSuperior;
	}

	@Column(name = "PorcentajeDeCuota", nullable = false, precision = 8, scale = 4)
	public Double getPorcentajeDeCuota() {
		return this.porcentajeDeCuota;
	}

	public void setPorcentajeDeCuota(Double porcentajeDeCuota) {
		this.porcentajeDeCuota = porcentajeDeCuota;
	}

}