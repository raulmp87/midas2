package mx.com.afirme.midas.consultas.tipolicenciapiloto;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.base.CacheableDTO;


/**
 * TipoLicenciaPilotoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name = "TipoLicenciaPilotoDTO")
@Table(name = "TCTIPOLICENCIAPILOTO", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = "CODIGOTIPOLICENCIAPILOTO"))
public class TipoLicenciaPilotoDTO extends CacheableDTO implements java.io.Serializable {

	private static final long serialVersionUID = -6985479079897673812L;

	private BigDecimal idTipoLicenciaPiloto;
	private BigDecimal codigoTipoLicenciaPiloto;
	private String 	   descripcionTipoLicenciaPiloto;

	// Constructors
	public TipoLicenciaPilotoDTO() {
	}

	/** full constructor */
	public TipoLicenciaPilotoDTO(BigDecimal idTipoLicenciaPiloto,
			BigDecimal codigoTipoLicenciaPiloto,
			String descripcionTipoLicenciaPiloto) {
		this.idTipoLicenciaPiloto    	   = idTipoLicenciaPiloto;
		this.codigoTipoLicenciaPiloto 	   = codigoTipoLicenciaPiloto;
		this.descripcionTipoLicenciaPiloto = descripcionTipoLicenciaPiloto;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTCTIPOLICENCIAPILOTO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCTIPOLICENCIAPILOTO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCTIPOLICENCIAPILOTO_SEQ_GENERADOR")
	@Column(name = "IDTCTIPOLICENCIAPILOTO", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTipoLicenciaPiloto() {
		return this.idTipoLicenciaPiloto;
	}

	public void setIdTipoLicenciaPiloto(BigDecimal idTipoLicenciaPiloto) {
		this.idTipoLicenciaPiloto = idTipoLicenciaPiloto;
	}

	@Column(name = "CODIGOTIPOLICENCIAPILOTO", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoTipoLicenciaPiloto() {
		return this.codigoTipoLicenciaPiloto;
	}

	public void setCodigoTipoLicenciaPiloto(BigDecimal codigoTipoLicenciaPiloto) {
		this.codigoTipoLicenciaPiloto = codigoTipoLicenciaPiloto;
	}

	@Column(name = "DESCRIPCIONTIPOLICENCIAPILOTO", nullable = false, length = 200)
	public String getDescripcionTipoLicenciaPiloto() {
		return this.descripcionTipoLicenciaPiloto;
	}

	public void setDescripcionTipoLicenciaPiloto(
			String descripcionTipoLicenciaPiloto) {
		this.descripcionTipoLicenciaPiloto = descripcionTipoLicenciaPiloto;
	}

	
	@Override
	public boolean equals(Object o) {
		if(o == null)
			return false;
		
		if(o instanceof TipoLicenciaPilotoDTO){
			if(((TipoLicenciaPilotoDTO) o).getCodigoTipoLicenciaPiloto() .equals(this.getCodigoTipoLicenciaPiloto()))
				return true;
		}
		return false;
	}
	
	@Override
	public String getDescription() {
		return this.descripcionTipoLicenciaPiloto;
	}

	@Override
	public Object getId() {
		return this.idTipoLicenciaPiloto;
	}
}