package mx.com.afirme.midas.consultas.tipolicenciapiloto;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for TipoLicenciaPilotoFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface TipoLicenciaPilotoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved TipoLicenciaPilotoDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            TipoLicenciaPilotoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoLicenciaPilotoDTO entity);

	/**
	 * Delete a persistent TipoLicenciaPilotoDTO entity.
	 * 
	 * @param entity
	 *            TipoLicenciaPilotoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoLicenciaPilotoDTO entity);

	/**
	 * Persist a previously saved TipoLicenciaPilotoDTO entity and return it or a
	 * copy of it to the sender. A copy of the TipoLicenciaPilotoDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoLicenciaPilotoDTO entity to update
	 * @return TipoLicenciaPilotoDTO the persisted TipoLicenciaPilotoDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoLicenciaPilotoDTO update(TipoLicenciaPilotoDTO entity);

	public TipoLicenciaPilotoDTO findById(BigDecimal id);

	/**
	 * Find all TipoLicenciaPilotoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the TipoLicenciaPilotoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoLicenciaPilotoDTO> found by query
	 */
	public List<TipoLicenciaPilotoDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all TipoLicenciaPilotoDTO entities.
	 * 
	 * @return List<TipoLicenciaPilotoDTO> all TipoLicenciaPilotoDTO entities
	 */
	public List<TipoLicenciaPilotoDTO> findAll();

	public List<TipoLicenciaPilotoDTO> listarFiltrado(TipoLicenciaPilotoDTO tipoLicenciaPilotoDTO);
}