package mx.com.afirme.midas.consultas.mediopago;
// default package

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for MedioPagoFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface MedioPagoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved MedioPago entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            MedioPago entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(MedioPagoDTO entity);

	/**
	 * Delete a persistent MedioPagoDTO entity.
	 * 
	 * @param entity
	 *            MedioPagoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(MedioPagoDTO entity);

	/**
	 * Persist a previously saved MedioPagoDTO entity and return it or a copy of it
	 * to the sender. A copy of the MedioPagoDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            MedioPagoDTO entity to update
	 * @return MedioPagoDTO the persisted MedioPagoDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public MedioPagoDTO update(MedioPagoDTO entity);

	public MedioPagoDTO findById(MedioPagoDTO id);

	/**
	 * Find all MedioPagoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the MedioPagoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<MedioPagoDTO> found by query
	 */
	public List<MedioPagoDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all MedioPagoDTO entities.
	 * 
	 * @return List<MedioPagoDTO> all MedioPagoDTO entities
	 */
	public List<MedioPagoDTO> findAll();
	
	public List<MedioPagoDTO> listarFiltrado(MedioPagoDTO medioPagoDTO);
}