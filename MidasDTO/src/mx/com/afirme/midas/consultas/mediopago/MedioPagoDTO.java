package mx.com.afirme.midas.consultas.mediopago;
// default package

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * MedioPagoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name = "MedioPagoDTO")
@Table(name = "VNMEDIOPAGO", schema = "MIDAS")
public class MedioPagoDTO extends CacheableDTO implements java.io.Serializable, Entidad {

	private static final long serialVersionUID = 4263801717650219661L;
	private Integer idMedioPago;
	private String  descripcion;
	private List<ProductoDTO> productos;

	public static final Integer MEDIO_PAGO_AGENTE = 15;
	public static final Integer MEDIO_PAGO_TARJETA = 4;
	public static final Integer MEDIO_PAGO_DOMICILIADA = 8;
	public static final Integer MEDIO_PAGO_CUENTAAFIRME = 1386;
	public static final String PAGO_TARJETA = "TARJETA DE CREDITO";
	public static final String PAGO_DOMICILIADA = "COBRANZA DOMICILIADA";
	public static final String PAGO_CUENTAAFIRME = "CUENTA AFIRME (CREDITO O DEBITO)";
	
	// Constructors
	public MedioPagoDTO() {
	}

	public MedioPagoDTO(Integer idMedioPago, String descripcion) {
		this.idMedioPago = idMedioPago;
		this.descripcion = descripcion;
	}

	// Property accessors
	@Id
	@Column(name = "IDTCMEDIOPAGO", nullable = false, precision = 5, scale = 0)
	public Integer getIdMedioPago() {
		return this.idMedioPago;
	}

	public void setIdMedioPago(Integer idMedioPago) {
		this.idMedioPago = idMedioPago;
	}

	@Column(name = "DESCRIPCION", nullable = false, length = 40)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String getDescription() {
		return this.descripcion;
	}

	@Override
	public String getId() {
		return this.getIdMedioPago().toString();
	}

	
	@ManyToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinTable(name = "TRMEDIOPAGOPRODUCTO", schema = "MIDAS",
			joinColumns = {
				@JoinColumn(name="IDTCMEDIOPAGO") 
			},
			inverseJoinColumns = {
				@JoinColumn(name="IDTOPRODUCTO")
			}
	)
	public List<ProductoDTO> getProductos() {
		return productos;
	}

	public void setProductos(List<ProductoDTO> productos) {
		this.productos = productos;
	}

	@Override
	public boolean equals(Object o) {
		if(o == null)
			return false;
		
		if(o instanceof MedioPagoDTO){
			if(((MedioPagoDTO) o) .getDescripcion() .equals(this.getDescripcion()))
				return true;
		}
		
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Integer getKey() {
		return this.idMedioPago;
	}

	@Override
	public String getValue() {
		return this.descripcion;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}

}