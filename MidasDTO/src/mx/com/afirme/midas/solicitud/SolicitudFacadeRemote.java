package mx.com.afirme.midas.solicitud;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

/**
 * Remote interface for SolicitudDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SolicitudFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved SolicitudDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SolicitudDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public SolicitudDTO save(SolicitudDTO entity);

	/**
	 * Delete a persistent SolicitudDTO entity.
	 * 
	 * @param entity
	 *            SolicitudDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SolicitudDTO entity);

	/**
	 * Persist a previously saved SolicitudDTO entity and return it or a copy of
	 * it to the sender. A copy of the SolicitudDTO entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            SolicitudDTO entity to update
	 * @return SolicitudDTO the persisted SolicitudDTO entity instance, may not
	 *         be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SolicitudDTO update(SolicitudDTO entity);

	public SolicitudDTO findById(BigDecimal id);

	/**
	 * Find all SolicitudDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SolicitudDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SolicitudDTO> found by query
	 */
	public List<SolicitudDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all SolicitudDTO entities.
	 * 
	 * @return List<SolicitudDTO> all SolicitudDTO entities
	 */
	public List<SolicitudDTO> findAll();

	public List<SolicitudDTO> findByUserId(String id);

	public List<SolicitudDTO> findByEstatus(Short claveEstatus, String userId);
	
	public List<SolicitudDTO> listarFiltrado(SolicitudDTO solicitudDTO);

	public List<SolicitudDTO> listarFiltradoGrid(SolicitudDTO solicitudDTO);
	
	public Long obtenerTotalFiltrado(SolicitudDTO solicitudDTO);
	
	/**
	 * Se registran estadisticas sobre la solicitud para su monitoreo
	 * @param idToSolicitud
	 * @param idToCotizacion
	 * @param idToPoliza
	 * @param numeroEndoso
	 * @param codigoUsuario
	 * @param tipoAccion
	 */
	public  void registraEstadisticas(BigDecimal idToSolicitud,BigDecimal idToCotizacion,
			BigDecimal idToPoliza, BigDecimal numeroEndoso,String codigoUsuario,int tipoAccion);
	
	public List<SolicitudDTO> listarFiltradoSolicitud(SolicitudDTO solicitudDTO, Date fechaInicial, Date fechaFinal);
	
	public List<SolicitudDTO> listarFilradoSolicitudGrid(SolicitudDTO solicitudDTO, Date fechaInicial, Date fechaFinal,Long idToNegocioProducto);
	
	public Long listarFilradoSolicitudGridCount(SolicitudDTO solicitudDTO, Date fechaInicial, Date fechaFinal,Long idToNegocioProducto);
	
	/**
	 * Perform an initial save of a previously unsaved SolicitudDTO entity and return its id value. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SolicitudDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public BigDecimal saveAndGetId(SolicitudDTO entity);
	
	public Long obtenerTotalFiltrado(SolicitudDTO solicitudDTO, Date fechaFinal);
	
	public Long obtenerTotalFiltradoRapido(SolicitudDTO solicitudDTO) ;
}