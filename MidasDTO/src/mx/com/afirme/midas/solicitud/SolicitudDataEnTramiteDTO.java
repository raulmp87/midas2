/**
 * 
 */
package mx.com.afirme.midas.solicitud;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * @author jreyes
 *
 */
@Entity
@Table(name = "TOSOLICITUDDATAENTRAMITE", schema = "MIDAS")
public class SolicitudDataEnTramiteDTO implements Entidad{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1147163369448136530L;
	
	private BigDecimal idToSolicitudDataEnTramite;
    private BigDecimal idToSolicitud;
    private String descripcion;
    private String modelo;
    private String serie;
    private String equipoEspecial;
    private String adaptacion;
    private String observaciones;
    private Date fechaInicioVigencia;
    private Date fechaFinVigencia;
	
    @Id
	@SequenceGenerator(name = "IDTOSOLICITUDDATAENTRAMITE_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOSOLICITUDDATAENTRAMITE_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOSOLICITUDDATAENTRAMITE_SEQ_GENERADOR")
	@Column(name = "IDTOSOLICITUDDATAENTRAMITE", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSolicitudDataEnTramite() {
		return idToSolicitudDataEnTramite;
	}

	public void setIdToSolicitudDataEnTramite(BigDecimal idToSolicitudDataEnTramite) {
		this.idToSolicitudDataEnTramite = idToSolicitudDataEnTramite;
	}
	
	@Column(name = "IDTOSOLICITUD")
	public BigDecimal getIdToSolicitud() {
		return idToSolicitud;
	}

	public void setIdToSolicitud(BigDecimal idToSolicitud) {
		this.idToSolicitud = idToSolicitud;
	}

	@Column(name = "DESCRIPCION")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "MODELO")
	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	@Column(name = "SERIE")
	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}
	
	@Column(name = "EQUIPOESPECIAL")
	public String getEquipoEspecial() {
		return equipoEspecial;
	}

	public void setEquipoEspecial(String equipoEspecial) {
		this.equipoEspecial = equipoEspecial;
	}
	
	@Column(name = "ADAPTACION")
	public String getAdaptacion() {
		return adaptacion;
	}

	public void setAdaptacion(String adaptacion) {
		this.adaptacion = adaptacion;
	}

	@Column(name = "OBSERVACIONES")
	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAINICIOVIGENCIA")
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAFINVIGENCIA")
	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}

	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.idToSolicitudDataEnTramite;
	}

	@Override
	public String getValue() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

}