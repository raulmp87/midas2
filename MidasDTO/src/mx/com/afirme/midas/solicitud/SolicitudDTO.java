package mx.com.afirme.midas.solicitud;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.base.PaginadoDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.comentarios.Comentario;

/**
 * SolicitudDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOSOLICITUD", schema = "MIDAS")
public class SolicitudDTO extends PaginadoDTO implements Entidad {
	public static enum Estatus{
		PROCESO((short)0), ORDEN_DE_TRABAJO((short)1), TERMINADA((short)2), PENDIENTE((short)3), RECHAZADA((short)8), CANCELADA((short)9),PROCESO_INCOMPLETO((short)5);		
		Estatus(Short estatus) {
			this.estatus = estatus;
		}
		private Short estatus;
		public Short getEstatus(){
			return this.estatus;
		}
	};		
	
	public enum ACCION {CREACION,MODIFICACION,ASIGNACION_COORDINADOR,ASIGNACION_SUSCRIPTOR,REASIGNACION_SUSCRIPTOR,CANCELACION,RECHAZO,TERMINAR}
	

	private static final long serialVersionUID = 1L;
	private BigDecimal idToSolicitud;
	private ProductoDTO productoDTO;
	private Short claveTipoSolicitud;
	private BigDecimal idToPolizaAnterior;
	private String telefonoContacto;
	private String emailContactos;
	private BigDecimal codigoAgente;
	private Short claveOpcionEmision;
	private Date fechaCreacion;
	private String codigoUsuarioCreacion;
	private Date fechaModificacion;
	private String codigoUsuarioModificacion;
	private Short claveEstatus;
	private Short claveTipoPersona;
	private String nombrePersona;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String nombreAgente;
	private String nombreOficinaAgente;
	private String nombreOficina;
	private List<DocumentoDigitalSolicitudDTO> documentoDigitalSolicitudDTOs = new ArrayList<DocumentoDigitalSolicitudDTO>();
	private Short esRenovacion;
	private List<Comentario> comentarioList = new ArrayList<Comentario>();
	private Negocio negocio;	
	private ControlArchivoDTO controlArchivo;
	private Agente agente;
	private String nombreNegocio;
	private String tituloCliente;
	private String nombreProducto;
	private BigDecimal folio;
	private String tipodeMovimiento;
	private String nombreDescripcionEstatus;
	private String nombreCompleto;
	private String codigoUsuarioAsignacion;
	private String motivoRechazo;
	private String email;
	
	//Atributos para endosos
	private BigDecimal claveTipoEndoso;
	private Date fechaInicioVigenciaEndoso;
	private BigDecimal idToPolizaEndosada;
	private Short claveMotivoEndoso;

	private BigDecimal codigoEjecutivo;
	private String nombreEjecutivo;
	private BigDecimal idOficina;
	private BigDecimal idCentroEmisor;
	private BigDecimal idPromotoria;
	private Short claveEstatusTransient; 
	private String nombreUsuarioSolicitante;
	private Boolean esBusquedaRapida = false;
	private Short tieneCartaCobertura;
	
	//Contantes Claves Tipos Endoso
	public static final short CVE_TIPO_ENDOSO_INDEFINIDO = 0;
	public static final short CVE_TIPO_ENDOSO_ALTA_INCISO = 5;
	public static final short CVE_TIPO_ENDOSO_BAJA_INCISO = 6;
	public static final short CVE_TIPO_ENDOSO_CAMBIO_DATOS = 7;
	public static final short CVE_TIPO_ENDOSO_CAMBIO_AGENTE = 8;
	public static final short CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO = 9;
	public static final short CVE_TIPO_ENDOSO_CANCELACION_ENDOSO = 10;
	public static final short CVE_TIPO_ENDOSO_CANCELACION_POLIZA = 11;
	public static final short CVE_TIPO_ENDOSO_EXTENSION_VIGENCIA = 12;
	public static final short CVE_TIPO_ENDOSO_INCLUSION_ANEXO = 13;
	public static final short CVE_TIPO_ENDOSO_INCLUSION_TEXTO = 14;
	public static final short CVE_TIPO_ENDOSO_DE_MOVIMIENTOS = 15;
	public static final short CVE_TIPO_ENDOSO_REHABILITACION_INCISOS = 16;
	public static final short CVE_TIPO_ENDOSO_REHABILITACION_ENDOSO = 17;
	public static final short CVE_TIPO_ENDOSO_REHABILITACION_POLIZA = 18;
	public static final short CVE_TIPO_ENDOSO_AJUSTE_PRIMA = 19;
	public static final short CVE_TIPO_ENDOSO_EXCLUSION_TEXTO = 20;
	public static final short CVE_TIPO_ENDOSO_CAMBIO_IVA = 21;
	public static final short CVE_TIPO_ENDOSO_INCLUSION_CONDICIONES  = 22;
	public static final short CVE_TIPO_ENDOSO_BAJA_CONDICIONES  = 23;
	public static final short CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL = 24;
	public static final short CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL = 25;
	public static final short CVE_TIPO_ENDOSO_DESAGRUPACION_RECIBOS = 26;
	public static final short CVE_TIPO_ENDOSO_CAMBIO_CONDUCTO_COBRO = 27;

	public static final short CVE_MOTIVO_ENDOSO_INDEFINIDO = 0;
	public static final short CVE_MOTIVO_ENDOSO_PETICION_ASEGURADO = 1;
	public static final short CVE_MOTIVO_ENDOSO_CANCELACION_AUTOMATICA_FALTA_PAGO = 7;
	
	public static final short CVE_MOTIVO_ENDOSO_POR_COMPETENCIA = 8;
	public static final short CVE_MOTIVO_ENDOSO_VENTA_UNIDAD = 9;
	public static final short CVE_MOTIVO_ENDOSO_PERDIDA_TOTAL = 10;
	public static final short CVE_MOTIVO_ENDOSO_ROBO_TOTAL = 11;
	public static final short CVE_MOTIVO_ENDOSO_MAL_SERVICIO_SINIESTRO = 12;
	public static final short CVE_MOTIVO_ENDOSO_MAL_SERVICIO_COBRANZA = 13;
	public static final short CVE_MOTIVO_ENDOSO_MAL_SERVICIO_VENTAS = 14;
	public static final short CVE_MOTIVO_ENDOSO_CAMBIO_AGENTE = 15;
	public static final short CVE_MOTIVO_ENDOSO_FUE_A_VENTA_DIRECTA = 16;
	public static final short CVE_MOTIVO_ENDOSO_ALTA_SINIESTRALIDAD = 17;
	public static final short CVE_MOTIVO_ENDOSO_INTERES_PERSONAL_DE_CLIENTE = 18;
	public static final short CVE_MOTIVO_ENDOSO_CAMBIO_POLIZA = 19;
	public static final short CVE_MOTIVO_ENDOSO_DUPLICIDAD = 20;
	public static final short CVE_MOTIVO_ENDOSO_FALTA_PAGO = 21;
	public static final short CVE_MOTIVO_ENDOSO_PAGO_DANIOS_SUSTITUCION_PERDIDA_TOTAL = 22;


	
	
	//SIMULACION ROL BORRAR
	public static final String USUARIO_AGENTE = "ADMIN";
	public static final String USUARIO_MESA = "MESA";
	public static final String USUARIO_COORDINADOR = "COORAUT";
	public static final String USUARIO_SUSCRIPTOR = "SUSAUT";
	
	
	// Constructors

	/** default constructor */
	public SolicitudDTO() {
	}

	/** minimal constructor */
	public SolicitudDTO(BigDecimal idToSolicitud, ProductoDTO productoDTO,
			Short claveTipoSolicitud, BigDecimal idToPolizaAnterior,
			BigDecimal codigoAgente, Short claveOpcionEmision,
			Date fechaCreacion, String codigoUsuarioCreacion,
			Date fechaModificacion, String codigoUsuarioModificacion,
			Short claveEstatus, Short claveTipoPersona, String nombrePersona) {
		this.idToSolicitud = idToSolicitud;
		this.productoDTO = productoDTO;
		this.claveTipoSolicitud = claveTipoSolicitud;
		this.idToPolizaAnterior = idToPolizaAnterior;
		this.codigoAgente = codigoAgente;
		this.claveOpcionEmision = claveOpcionEmision;
		this.fechaCreacion = fechaCreacion;
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
		this.fechaModificacion = fechaModificacion;
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
		this.claveEstatus = claveEstatus;
		this.claveTipoPersona = claveTipoPersona;
		this.nombrePersona = nombrePersona;
	}

	/** full constructor */
	public SolicitudDTO(BigDecimal idToSolicitud, ProductoDTO productoDTO,
			Short claveTipoSolicitud, BigDecimal idToPolizaAnterior,
			String telefonoContacto, String emailContactos,
			BigDecimal codigoAgente, Short claveOpcionEmision,
			Date fechaCreacion, String codigoUsuarioCreacion,
			Date fechaModificacion, String codigoUsuarioModificacion,
			Short claveEstatus, Short claveTipoPersona, String nombrePersona,
			String apellidoPaterno, String apellidoMaterno,
			List<DocumentoDigitalSolicitudDTO> documentoDigitalSolicitudDTOs,
			BigDecimal claveTipoEndoso, Date fechaInicioVigenciaEndoso, 
			BigDecimal idToPolizaEnodosada) {
		this.idToSolicitud = idToSolicitud;
		this.productoDTO = productoDTO;
		this.claveTipoSolicitud = claveTipoSolicitud;
		this.idToPolizaAnterior = idToPolizaAnterior;
		this.telefonoContacto = telefonoContacto;
		this.emailContactos = emailContactos;
		this.codigoAgente = codigoAgente;
		this.claveOpcionEmision = claveOpcionEmision;
		this.fechaCreacion = fechaCreacion;
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
		this.fechaModificacion = fechaModificacion;
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
		this.claveEstatus = claveEstatus;
		this.claveTipoPersona = claveTipoPersona;
		this.nombrePersona = nombrePersona;
		this.apellidoPaterno = apellidoPaterno;
		this.apellidoMaterno = apellidoMaterno;
		this.documentoDigitalSolicitudDTOs = documentoDigitalSolicitudDTOs;
		this.claveTipoEndoso= claveTipoEndoso;
		this.fechaInicioVigenciaEndoso= fechaInicioVigenciaEndoso;
		this.idToPolizaEndosada=idToPolizaEnodosada;
		
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTOSOLICITUD_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOSOLICITUD_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOSOLICITUD_SEQ_GENERADOR")
	@Column(name = "IDTOSOLICITUD", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSolicitud() {
		return this.idToSolicitud;
	}

	public void setIdToSolicitud(BigDecimal idToSolicitud) {
		this.idToSolicitud = idToSolicitud;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOPRODUCTO", nullable = false)
	public ProductoDTO getProductoDTO() {
		return this.productoDTO;
	}

	public void setProductoDTO(ProductoDTO productoDTO) {
		this.productoDTO = productoDTO;
	}

	@Column(name = "CLAVETIPOSOLICITUD", nullable = false, precision = 4, scale = 0)
	public Short getClaveTipoSolicitud() {
		return this.claveTipoSolicitud;
	}

	public void setClaveTipoSolicitud(Short claveTipoSolicitud) {
		this.claveTipoSolicitud = claveTipoSolicitud;
	}

	@Column(name = "IDTOPOLIZAANTERIOR", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToPolizaAnterior() {
		return this.idToPolizaAnterior;
	}

	public void setIdToPolizaAnterior(BigDecimal idToPolizaAnterior) {
		this.idToPolizaAnterior = idToPolizaAnterior;
	}

	@Column(name = "TELEFONOCONTACTO", length = 20)
	public String getTelefonoContacto() {
		return this.telefonoContacto;
	}

	public void setTelefonoContacto(String telefonoContacto) {
		this.telefonoContacto = telefonoContacto;
	}

	@Column(name = "EMAILCONTACTOS", length = 2000)
	public String getEmailContactos() {
		return this.emailContactos;
	}

	public void setEmailContactos(String emailContactos) {
		this.emailContactos = emailContactos;
	}

	@Column(name = "CODIGOAGENTE", nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoAgente() {
		return this.codigoAgente;
	}

	public void setCodigoAgente(BigDecimal codigoAgente) {
		this.codigoAgente = codigoAgente;
	}
	
	public void setCodigoAgente(String codigoAgente) {
		if (codigoAgente != null) {
			this.codigoAgente = new BigDecimal(codigoAgente);
		}
	}

	@Column(name = "CLAVEOPCIONEMISION", nullable = false, precision = 4, scale = 0)
	public Short getClaveOpcionEmision() {
		return this.claveOpcionEmision;
	}

	public void setClaveOpcionEmision(Short claveOpcionEmision) {
		this.claveOpcionEmision = claveOpcionEmision;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHACREACION", nullable = false, length = 7)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Column(name = "CODIGOUSUARIOCREACION", nullable = false, length = 8)
	public String getCodigoUsuarioCreacion() {
		return this.codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAMODIFICACION", nullable = false, length = 7)
	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Column(name = "CODIGOUSUARIOMODIFICACION", nullable = false, length = 8)
	public String getCodigoUsuarioModificacion() {
		return this.codigoUsuarioModificacion;
	}

	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}

	@Column(name = "CLAVEESTATUS", nullable = false, precision = 4, scale = 0)
	public Short getClaveEstatus() {
		return this.claveEstatus;
	}

	public void setClaveEstatus(Short claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	@Column(name = "CLAVETIPOPERSONA", nullable = false, precision = 4, scale = 0)
	public Short getClaveTipoPersona() {
		return this.claveTipoPersona;
	}

	public void setClaveTipoPersona(Short claveTipoPersona) {
		this.claveTipoPersona = claveTipoPersona;
	}

	@Column(name = "NOMBREPERSONA", nullable = false, length = 200)
	public String getNombrePersona() {
		return this.nombrePersona;
	}

	public void setNombrePersona(String nombrePersona) {
		this.nombrePersona = nombrePersona;
	}

	@Column(name = "APELLIDOPATERNO", length = 20)
	public String getApellidoPaterno() {
		return this.apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	@Column(name = "APELLIDOMATERNO", length = 20)
	public String getApellidoMaterno() {
		return this.apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "solicitudDTO")
	public List<DocumentoDigitalSolicitudDTO> getDocumentoDigitalSolicitudDTOs() {
		return this.documentoDigitalSolicitudDTOs;
	}

	public void setDocumentoDigitalSolicitudDTOs(
			List<DocumentoDigitalSolicitudDTO> documentoDigitalSolicitudDTOs) {
		this.documentoDigitalSolicitudDTOs = documentoDigitalSolicitudDTOs;
	}

	@Column(name = "NOMBREAGENTE")	
	public String getNombreAgente() {
		return nombreAgente;
	}

	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}

	@Column(name = "NOMBREOFICINAAGENTE")	
	public String getNombreOficinaAgente() {
		return nombreOficinaAgente;
	}

	public void setNombreOficinaAgente(String nombreOficinaAgente) {
		this.nombreOficinaAgente = nombreOficinaAgente;
	}

	public void setClaveTipoEndoso(BigDecimal claveTipoEndoso) {
		this.claveTipoEndoso = claveTipoEndoso;
	}

	@Column(name = "CLAVETIPOENDOSO",precision=22)	
	public BigDecimal getClaveTipoEndoso() {
		return claveTipoEndoso;
	}

	public void setFechaInicioVigenciaEndoso(Date fechaInicioVigenciaEndoso) {
		this.fechaInicioVigenciaEndoso = fechaInicioVigenciaEndoso;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAINICIOVIGENCIAENDOSO",length=7)
	public Date getFechaInicioVigenciaEndoso() {
		return fechaInicioVigenciaEndoso;
	}

	public void setIdToPolizaEndosada(BigDecimal idToPolizaEndosada) {
		this.idToPolizaEndosada = idToPolizaEndosada;
	}


	@Column(name = "IDTOPOLIZAENDOSADA", precision = 22)
	public BigDecimal getIdToPolizaEndosada() {
		return idToPolizaEndosada;
	}

	@Column(name = "CLAVEMOTIVOENDOSO", precision = 4, scale = 0)
	public Short getClaveMotivoEndoso() {
	    return this.claveMotivoEndoso;
	}

	public void setClaveMotivoEndoso(Short claveMotivoEndoso) {
	    this.claveMotivoEndoso = claveMotivoEndoso;
	}

	public void setCodigoEjecutivo(BigDecimal codigoEjecutivo) {
	    this.codigoEjecutivo = codigoEjecutivo;
	}
	@Column(name = "CODIGOEJECUTIVO", precision = 22, scale = 0)
	public BigDecimal getCodigoEjecutivo() {
	    return codigoEjecutivo;
	}
	
	public void setNombreEjecutivo(String nombreEjecutivo) {
	    this.nombreEjecutivo = nombreEjecutivo;
	}
	@Column(name = "NOMBREEJECUTIVO", length = 240)
	public String getNombreEjecutivo() {
	    return nombreEjecutivo;
	}

	public void setIdOficina(BigDecimal idOficina) {
	    this.idOficina = idOficina;
	}
	@Column(name = "IDOFICINA", precision = 22, scale = 0)
	public BigDecimal getIdOficina() {
	    return idOficina;
	}

	@Column(name = "NOMBREOFICINA")	
	public String getNombreOficina() {
		return nombreOficina;
	}

	public void setNombreOficina(String nombreOficina) {
		this.nombreOficina = nombreOficina;
	}

	@Column(name = "ESRENOVACION")	
	public Short getEsRenovacion() {
		return esRenovacion;
	}

	public void setEsRenovacion(Short esRenovacion) {
		this.esRenovacion = esRenovacion;
	}
	
	@Column(name = "TIENECARTACOBERTURA")
	public Short getTieneCartaCobertura() {
		return tieneCartaCobertura;
	}

	public void setTieneCartaCobertura(Short tieneCartaCobertura) {
		this.tieneCartaCobertura = tieneCartaCobertura;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "solicitudDTO")
	public List<Comentario> getComentarioList() {
		return comentarioList;
	}

	public void setComentarioList(List<Comentario> comentarioList) {
		this.comentarioList = comentarioList;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "NEGOCIO_ID", nullable = false)
	public Negocio getNegocio() {
		return negocio;
	}
	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}
	
	@Column(name="Email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.getIdToSolicitud();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	public String getNumeroSolicitud(){
		DecimalFormat f = new DecimalFormat("00000000");
		String prefix = "SOL-";
		return prefix + f.format(this.getIdToSolicitud());
	}
	
	@Transient
	public BigDecimal getIdCentroEmisor() {
		return idCentroEmisor;
	}

	public void setIdCentroEmisor(BigDecimal idCentroEmisor) {
		this.idCentroEmisor = idCentroEmisor;
	}
	@Transient
	public BigDecimal getIdPromotoria() {
		return idPromotoria;
	}

	public void setIdPromotoria(BigDecimal idPromotoria) {
		this.idPromotoria = idPromotoria;
	}

	@Transient
	public String getDescripcionEstatus(){
		String descripcion = null;
		if(this.claveEstatus != null){
			switch(this.claveEstatus){				
				case 0:
					descripcion = "PROCESO";
					break;
				case 1:
					descripcion = "ORDEN DE TRABAJO";
					break;
				case 2:
					descripcion = "TERMINADA";
					break;
				case 3:
					descripcion = "PENDIENTE";
					break;
				case 8:
					descripcion = "RECHAZADA";
					break;
				case 9:
					descripcion = "CANCELADA";
					break;
				case 5:
					descripcion = "PROCESO_INCOMPLETO";
					break;					
				default:
					break;							
			}
		}
		return descripcion;
		
	}
	
	
	
	@Transient
	public String getDescripcionTipoSolicitud(){
		String descripcion = null;
		if(this.claveTipoSolicitud != null){
			switch(this.claveTipoSolicitud){				
				case 0:
					descripcion = "PÓLIZA";
					break;
				case 1:
					descripcion = "RE-EXPEDICIÓN";
					break;
				case 2:
					descripcion = "ENDOSO";
					break;	
				case 3:
					descripcion = "COTIZACION";
					break;	
				case 4:
					descripcion = "RENOVACION";
					break;	
				default:
					break;							
			}
		}
		return descripcion;
		
	}
	
	
	/**
	 * Se usa para poder establecer el estatus
	 * de una cotizacion en lugar del estatus 
	 * de la solicutd
	 * @return
	 */
	@Transient
	public Short getClaveEstatusTransient() {
		return claveEstatusTransient;
	}
	
	public void setClaveEstatusTransient(Short claveEstatusTransient) {
		this.claveEstatusTransient = claveEstatusTransient;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	@Transient
	public Agente getAgente() {
		return agente;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("idToSolicitud: " + this.idToSolicitud + ", ");
		sb.append("productoDTO: " + this.productoDTO + ", ");
		sb.append("idToPolizaEndosada: " + this.getIdToPolizaEndosada());
		sb.append("]");
		
		return sb.toString();
	}
   
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOCONTROLARCHIVO", nullable = false)
	public ControlArchivoDTO getControlArchivoDTO() {
		return controlArchivo;
	}

	public void setControlArchivoDTO(ControlArchivoDTO controlArchivo) {
		this.controlArchivo = controlArchivo;
	}

	@Transient
	public String getNombreNegocio() {
		return nombreNegocio;
	}

	public void setNombreNegocio(String nombreNegocio) {
		this.nombreNegocio = nombreNegocio;
	}

	@Transient
	public String getTituloCliente() {
		return tituloCliente;
	}

	public void setTituloCliente(String tituloCliente) {
		this.tituloCliente = tituloCliente;
	}

	@Transient
	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	@Transient
	public BigDecimal getFolio() {
		return folio;
	}

	public void setFolio(BigDecimal folio) {
		this.folio = folio;
	}

	@Transient
	public String getTipodeMovimiento() {
		return tipodeMovimiento;
	}

	public void setTipodeMovimiento(String tipodeMovimiento) {
		this.tipodeMovimiento = tipodeMovimiento;
	}

	@Transient
	public String getNombreDescripcionEstatus() {
		return nombreDescripcionEstatus;
	}

	public void setNombreDescripcionEstatus(String nombreDescripcionEstatus) {
		this.nombreDescripcionEstatus = nombreDescripcionEstatus;
	}

	@Transient
	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	/**
	 * @return the codigoUsuarioAsignacion
	 */
	@Column(name="CODIGOUSUARIOASIGNACION", length = 8)
	public String getCodigoUsuarioAsignacion() {
		return codigoUsuarioAsignacion;
	}

	/**
	 * @param codigoUsuarioAsignacion the codigoUsuarioAsignacion to set
	 */
	public void setCodigoUsuarioAsignacion(String codigoUsuarioAsignacion) {
		this.codigoUsuarioAsignacion = codigoUsuarioAsignacion;
	}

	public void setNombreUsuarioSolicitante(String nombreUsuarioSolicitante) {
		this.nombreUsuarioSolicitante = nombreUsuarioSolicitante;
	}

	@Transient
	public String getNombreUsuarioSolicitante() {
		return nombreUsuarioSolicitante;
	}

	public void setMotivoRechazo(String motivoRechazo) {
		this.motivoRechazo = motivoRechazo;
	}

	@Transient
	public String getMotivoRechazo() {
		return motivoRechazo;
	}

	public void setEsBusquedaRapida(Boolean esBusquedaRapida) {
		this.esBusquedaRapida = esBusquedaRapida;
	}

	@Transient
	public Boolean getEsBusquedaRapida() {
		return esBusquedaRapida;
	}
	
	@Transient
	public String getDescripcionTipoEndoso(){
		String descripcion = null;
		if(this.claveTipoEndoso != null){
			switch(claveTipoEndoso.shortValue()){
			case CVE_TIPO_ENDOSO_ALTA_INCISO:
				descripcion = "ALTA DE INCISO";
				break;
			case CVE_TIPO_ENDOSO_AJUSTE_PRIMA:
				descripcion = "AJUSTE DE PRIMA";
				break;
			case CVE_TIPO_ENDOSO_BAJA_CONDICIONES:
				descripcion = "BAJA CONDICIONES ESP";
				break;
			case CVE_TIPO_ENDOSO_BAJA_INCISO:
				descripcion = "BAJA DE INCISO";
				break;
			case CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL:
				descripcion = "BAJA DE INCISO PT";
				break;
			case CVE_TIPO_ENDOSO_CAMBIO_AGENTE:
				descripcion = "CAMBIO DE AGENTE";
				break;
			case CVE_TIPO_ENDOSO_CAMBIO_DATOS:
				descripcion = "CAMBIO DE DATOS";
				break;
			case CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO:
				descripcion = "CAMBIO DE FORMA PAGO";
				break;
			case CVE_TIPO_ENDOSO_CAMBIO_IVA:
				descripcion = "CAMBIO DE IVA";
				break;
			case CVE_TIPO_ENDOSO_CANCELACION_ENDOSO:
				descripcion = "CANCELACION DE ENDOSO";
				break;
			case CVE_TIPO_ENDOSO_CANCELACION_POLIZA:
				descripcion = "CANCELACION DE POLIZA";
				break;
			case CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL:
				descripcion = "CANCELACION DE POLIZA PT";
				break;
			case CVE_TIPO_ENDOSO_DE_MOVIMIENTOS:
				descripcion = "DE MOVIMIENTOS";
				break;
			case CVE_TIPO_ENDOSO_DESAGRUPACION_RECIBOS:
				descripcion = "DESAGRUPACION DE RECIBOS";
				break;
			case CVE_TIPO_ENDOSO_EXCLUSION_TEXTO:
				descripcion = "EXCLUSION DE TEXTO";
				break;
			case CVE_TIPO_ENDOSO_EXTENSION_VIGENCIA:
				descripcion = "EXTENSION DE VIGENCIA";
				break;
			case CVE_TIPO_ENDOSO_INCLUSION_ANEXO:
				descripcion = "INCLUSION DE ANEXO";
				break;
			case CVE_TIPO_ENDOSO_INCLUSION_CONDICIONES:
				descripcion = "INCLUSION CONDICIONES ESP";
				break;
			case CVE_TIPO_ENDOSO_INCLUSION_TEXTO:
				descripcion = "INCLUSION DE TEXTO";
				break;
			case CVE_TIPO_ENDOSO_REHABILITACION_ENDOSO:
				descripcion = "REHABILITACION DE ENDOSO";
				break;
			case CVE_TIPO_ENDOSO_REHABILITACION_INCISOS:
				descripcion = "REHABILITACION DE INCISOS";
				break;
			case CVE_TIPO_ENDOSO_REHABILITACION_POLIZA:
				descripcion = "REHABILITACION DE POLIZA";
				break;
			case CVE_TIPO_ENDOSO_CAMBIO_CONDUCTO_COBRO:
				descripcion = "CAMBIO DE CONDUCTO DE COBRO";
				break;
			}
		}
		return descripcion;
	}
	
}