package mx.com.afirme.midas.emision;

import java.math.BigDecimal;

import javax.ejb.Remote;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;


public interface EmisionFacadeRemote {

	public PolizaDTO emitirPolizaCasa (CotizacionDTO cotizacion);
	
	public boolean reexpidePoliza(BigDecimal idPoliza, BigDecimal idCotizacion);
}
