package mx.com.afirme.midas.cotizacion.seccion;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.SeccionInciso;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

/**
 * SeccionCotizacionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOSECCIONCOT", schema = "MIDAS")
public class SeccionCotizacionDTO implements java.io.Serializable, Entidad {

	// Fields
	private static final long serialVersionUID = -5120670542827294116L;

	private SeccionCotizacionDTOId id;
	private IncisoCotizacionDTO incisoCotizacionDTO;
	private SeccionDTO seccionDTO;
	private Double valorPrimaNeta;
	private Double valorSumaAsegurada;
	private Short claveObligatoriedad;
	private Short claveContrato;
	private List<CoberturaCotizacionDTO> coberturaCotizacionLista;
	private List<SubIncisoCotizacionDTO> subIncisoCotizacionLista;
	private Double valorPrimaNetaOriginal;
	private List<CoberturaCotizacionDTO> coberturaCotizacionSinAgrupar;

	// Constructors

	/** default constructor */
	public SeccionCotizacionDTO() {
		id = new SeccionCotizacionDTOId();
		incisoCotizacionDTO = new IncisoCotizacionDTO();
		seccionDTO = new SeccionDTO();
		coberturaCotizacionLista = new ArrayList<CoberturaCotizacionDTO>(0);
		subIncisoCotizacionLista = new ArrayList<SubIncisoCotizacionDTO>(0);
	}
	
	public SeccionCotizacionDTO(SeccionInciso seccionInciso) {
		this.claveContrato = seccionInciso.getClaveContrato();
		this.claveObligatoriedad = seccionInciso.getClaveObligatoriedad();
		this.valorPrimaNeta = seccionInciso.getValorPrimaNeta();
		this.valorSumaAsegurada = seccionInciso.getValorSumaAsegurada();
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToCotizacion", column = @Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroInciso", column = @Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToSeccion", column = @Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)) })
	public SeccionCotizacionDTOId getId() {
		return this.id;
	}

	public void setId(SeccionCotizacionDTOId id) {
		this.id = id;
	}

	//Se quita el cascade ya que esto esta causando que la persistencia genere queries innecesarios y ocasionando problemas de performance.
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumns( {
			@JoinColumn(name = "IDTOCOTIZACION", referencedColumnName = "IDTOCOTIZACION", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "NUMEROINCISO", referencedColumnName = "NUMEROINCISO", nullable = false, insertable = false, updatable = false) })
	public IncisoCotizacionDTO getIncisoCotizacionDTO() {
		return incisoCotizacionDTO;
	}

	public void setIncisoCotizacionDTO(IncisoCotizacionDTO incisoCotizacionDTO) {
		this.incisoCotizacionDTO = incisoCotizacionDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOSECCION", nullable = false, insertable = false, updatable = false)
	public SeccionDTO getSeccionDTO() {
		return this.seccionDTO;
	}

	public void setSeccionDTO(SeccionDTO seccionDTO) {
		this.seccionDTO = seccionDTO;
	}

	@Column(name = "VALORPRIMANETA", nullable = false, precision = 16)
	public Double getValorPrimaNeta() {
		return this.valorPrimaNeta;
	}

	public void setValorPrimaNeta(Double valorPrimaNeta) {
		this.valorPrimaNeta = valorPrimaNeta;
	}

	@Column(name = "VALORSUMAASEGURADA", nullable = false, precision = 16)
	public Double getValorSumaAsegurada() {
		return this.valorSumaAsegurada;
	}

	public void setValorSumaAsegurada(Double valorSumaAsegurada) {
		this.valorSumaAsegurada = valorSumaAsegurada;
	}

	@Column(name = "CLAVEOBLIGATORIEDAD", nullable = false, precision = 4, scale = 0)
	public Short getClaveObligatoriedad() {
		return this.claveObligatoriedad;
	}

	public void setClaveObligatoriedad(Short claveObligatoriedad) {
		this.claveObligatoriedad = claveObligatoriedad;
	}

	@Column(name = "CLAVECONTRATO", nullable = false, precision = 4, scale = 0)
	public Short getClaveContrato() {
		return this.claveContrato;
	}

	public void setClaveContrato(Short claveContrato) {
		this.claveContrato = claveContrato;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "seccionCotizacionDTO")
	public List<CoberturaCotizacionDTO> getCoberturaCotizacionLista() {
		return this.coberturaCotizacionLista;
	}

	public void setCoberturaCotizacionLista(
			List<CoberturaCotizacionDTO> coberturaCotizacionLista) {
		this.coberturaCotizacionLista = coberturaCotizacionLista;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "seccionCotizacionDTO")
	public List<SubIncisoCotizacionDTO> getSubIncisoCotizacionLista() {
		return this.subIncisoCotizacionLista;
	}

	public void setSubIncisoCotizacionLista(
			List<SubIncisoCotizacionDTO> subIncisoCotizacionLista) {
		this.subIncisoCotizacionLista = subIncisoCotizacionLista;
	}

	@Transient
	public Double getValorPrimaNetaOriginal() {
		return valorPrimaNetaOriginal;
	}

	public void setValorPrimaNetaOriginal(Double valorPrimaNetaOriginal) {
		this.valorPrimaNetaOriginal = valorPrimaNetaOriginal;
	}

	@Transient
	public List<CoberturaCotizacionDTO> getCoberturaCotizacionSinAgrupar() {
		return coberturaCotizacionSinAgrupar;
	}

	public void setCoberturaCotizacionSinAgrupar(
			List<CoberturaCotizacionDTO> coberturaCotizacionSinAgrupar) {
		this.coberturaCotizacionSinAgrupar = coberturaCotizacionSinAgrupar;
	}
	
	@Transient
	public CoberturaCotizacionDTO getCoberturaDeLista(final CoberturaCotizacionId id){
		Predicate predicate = new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				CoberturaCotizacionDTO cobertura = (CoberturaCotizacionDTO)arg0;
				if(cobertura.getId().equals(id)){
					return true;
				}
				return false;
			}
		};		
		return (CoberturaCotizacionDTO)CollectionUtils.find(this.coberturaCotizacionLista, predicate);
	}

	@Override
	public SeccionCotizacionDTOId getKey() {		
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SeccionCotizacionDTOId getBusinessKey() {
		return id;
	}
}