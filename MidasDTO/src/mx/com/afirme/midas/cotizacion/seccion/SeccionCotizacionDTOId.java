package mx.com.afirme.midas.cotizacion.seccion;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * SeccionCotizacionDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class SeccionCotizacionDTOId  implements java.io.Serializable {


    // Fields    
	private static final long serialVersionUID = -6476909898658047010L;
	private BigDecimal idToCotizacion;
    private BigDecimal numeroInciso;
    private BigDecimal idToSeccion;

    // Constructors

    /** default constructor */
    public SeccionCotizacionDTOId() {
    }

    
    /** full constructor */
    public SeccionCotizacionDTOId(BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idToSeccion) {
        this.idToCotizacion = idToCotizacion;
        this.numeroInciso = numeroInciso;
        this.idToSeccion = idToSeccion;
    }

   
    // Property accessors

    @Column(name="IDTOCOTIZACION", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToCotizacion() {
        return this.idToCotizacion;
    }
    
    public void setIdToCotizacion(BigDecimal idToCotizacion) {
        this.idToCotizacion = idToCotizacion;
    }

    @Column(name="NUMEROINCISO", nullable=false, precision=22, scale=0)

    public BigDecimal getNumeroInciso() {
        return this.numeroInciso;
    }
    
    public void setNumeroInciso(BigDecimal numeroInciso) {
        this.numeroInciso = numeroInciso;
    }

    @Column(name="IDTOSECCION", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToSeccion() {
        return this.idToSeccion;
    }
    
    public void setIdToSeccion(BigDecimal idToSeccion) {
        this.idToSeccion = idToSeccion;
    }

    
   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof SeccionCotizacionDTOId) ) return false;
		 SeccionCotizacionDTOId castOther = ( SeccionCotizacionDTOId ) other; 
         
		 return ( (this.getIdToCotizacion()==castOther.getIdToCotizacion()) || ( this.getIdToCotizacion()!=null && castOther.getIdToCotizacion()!=null && this.getIdToCotizacion().equals(castOther.getIdToCotizacion()) ) )
 && ( (this.getNumeroInciso()==castOther.getNumeroInciso()) || ( this.getNumeroInciso()!=null && castOther.getNumeroInciso()!=null && this.getNumeroInciso().equals(castOther.getNumeroInciso()) ) )
 && ( (this.getIdToSeccion()==castOther.getIdToSeccion()) || ( this.getIdToSeccion()!=null && castOther.getIdToSeccion()!=null && this.getIdToSeccion().equals(castOther.getIdToSeccion()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdToCotizacion() == null ? 0 : this.getIdToCotizacion().hashCode() );
         result = 37 * result + ( getNumeroInciso() == null ? 0 : this.getNumeroInciso().hashCode() );
         result = 37 * result + ( getIdToSeccion() == null ? 0 : this.getIdToSeccion().hashCode() );
         return result;
   }   

}