package mx.com.afirme.midas.cotizacion.seccion;

// default package

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;

/**
 * Remote interface for SeccionCotizacionDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SeccionCotizacionFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved SeccionCotizacionDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            SeccionCotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SeccionCotizacionDTO entity);

	/**
	 * Delete a persistent SeccionCotizacionDTO entity.
	 * 
	 * @param entity
	 *            SeccionCotizacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SeccionCotizacionDTO entity);

	/**
	 * Persist a previously saved SeccionCotizacionDTO entity and return it or a
	 * copy of it to the sender. A copy of the SeccionCotizacionDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            SeccionCotizacionDTO entity to update
	 * @return SeccionCotizacionDTO the persisted SeccionCotizacionDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SeccionCotizacionDTO update(SeccionCotizacionDTO entity);

	public SeccionCotizacionDTO findById(SeccionCotizacionDTOId id);

	/**
	 * Find all SeccionCotizacionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SeccionCotizacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SeccionCotizacionDTO> found by query
	 */
	public List<SeccionCotizacionDTO> findByProperty(String propertyName,
			Object value);

	public List<SeccionCotizacionDTO> findByValorPrimaNeta(Object valorPrimaNeta);

	public List<SeccionCotizacionDTO> findByValorSumaAsegurada(
			Object valorSumaAsegurada);

	public List<SeccionCotizacionDTO> findByClaveObligatoriedad(
			Object claveObligatoriedad);

	public List<SeccionCotizacionDTO> findByClaveContrato(Object claveContrato);

	/**
	 * Find all SeccionCotizacionDTO entities.
	 * 
	 * @return List<SeccionCotizacionDTO> all SeccionCotizacionDTO entities
	 */
	public List<SeccionCotizacionDTO> findAll();

	/**
	 * Find SeccionCotizacionDTO by listarPorIncisoCotizacionId entities.
	 * 
	 * @return List<SeccionCotizacionDTO> all SeccionCotizacionDTO entities
	 */
	
	public List<SeccionCotizacionDTO> listarPorIncisoCotizacionId(
			IncisoCotizacionId idToInciso);
	
	public List<SeccionCotizacionDTO> listarSeccionesPrimerRiesgo(
			BigDecimal idToCotizacion);

	public List<SeccionCotizacionDTO> listarSeccionesContratadas(
			BigDecimal idToCotizacion);
	
	public List<SeccionCotizacionDTO> listarSeccionesContratadas(BigDecimal idToCotizacion,BigDecimal numeroInciso);

	public List<SeccionCotizacionDTO> listarSeccionesLUC(
			BigDecimal idToCotizacion);	

	/**
	 * M�todo que regresa la lista de entidades SeccionCotizacion relacionadas con una cotizacion en el numero del inciso recibido.
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @return List<SeccionCotizacionDTO>
	 */
	public List<SeccionCotizacionDTO> listarPorCotizacionNumeroInciso(BigDecimal idToCotizacion,BigDecimal numeroInciso);
	
	/**
	 * Regresa una lista con todos las secciones que una cotizacion tiene
	 * @param idToContizacion
	 * @return
	 */
	public List<SeccionCotizacionDTO> listarPorCotizacion(BigDecimal idToContizacion);
	
	/**
	 * M�todo que regresa el total de entidades SeccionCotizacion relacionadas con una cotizacion en el numero del inciso recibido.
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @return
	 */
	public Long obtenerTotalSeccionesContratadas(BigDecimal idToCotizacion,BigDecimal numeroInciso);
	
	public Double getSumatoriaCoberturasBasicas(SeccionCotizacionDTOId id);
	
	public List<SeccionCotizacionDTO> listarSeccionesContratadasPorCotizacion(BigDecimal idToCotizacion);
	
	public List<SeccionCotizacionDTO> listarSeccionesContratadasPorCotizacion(BigDecimal idToCotizacion,Short claveContrato);
	
	public List<SeccionCotizacionDTO> listarSeccionesContratadasPorCotizacion(BigDecimal idToCotizacion,Short claveContrato,boolean aplicarMerge);
	
	public List<SeccionCotizacionDTO> listarSeccionesContratadasIgualacion(BigDecimal idToCotizacion,BigDecimal numeroInciso);
}