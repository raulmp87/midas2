package mx.com.afirme.midas.cotizacion.endoso.movimiento;

// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.base.LogBaseDTO;

/**
 * MovimientoCotizacionEndosoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOMOVIMIENTOCOTEND", schema = "MIDAS")
public class MovimientoCotizacionEndosoDTO extends LogBaseDTO {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToMovimientoCotEnd;
	private BigDecimal idToCotizacion;
	private BigDecimal numeroInciso;
	private BigDecimal idToSeccion;
	private BigDecimal idToCobertura;
	private Short claveTipoMovimiento;
	private BigDecimal valorDiferenciaSumaAsegurada;
	private BigDecimal valorDiferenciaPrimaNeta;
	private BigDecimal valorDiferenciaCuota;
	private String descripcionMovimiento;
	private String descripcionSeccion;
	private BigDecimal numeroSubInciso;
	private Short claveRegeneraRecibos;
	private Short claveAgrupacion;
	private BigDecimal idToControlArchivo;
	// Constructors

	/** default constructor */
	public MovimientoCotizacionEndosoDTO() {
	}

	// Property accessors IDTOMOVIMIENTOCOTEND_SEQ

	@Id
	@SequenceGenerator(name = "IDTOMOVIMIENTOCOTEND_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOMOVIMIENTOCOTEND_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOMOVIMIENTOCOTEND_SEQ_GENERADOR")
	@Column(name = "IDTOMOVIMIENTOCOTEND", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToMovimientoCotEnd() {
		return this.idToMovimientoCotEnd;
	}

	public void setIdToMovimientoCotEnd(BigDecimal idToMovimientoCotEnd) {
		this.idToMovimientoCotEnd = idToMovimientoCotEnd;
	}

	@Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCotizacion() {
		return this.idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	@Column(name = "NUMEROINCISO", precision = 22, scale = 0)
	public BigDecimal getNumeroInciso() {
		return this.numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	@Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSeccion() {
		return this.idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	@Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCobertura() {
		return this.idToCobertura;
	}

	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	@Column(name = "CLAVETIPOMOVIMIENTO", nullable = false, precision = 4, scale = 0)
	public Short getClaveTipoMovimiento() {
		return this.claveTipoMovimiento;
	}

	public void setClaveTipoMovimiento(Short claveTipoMovimiento) {
		this.claveTipoMovimiento = claveTipoMovimiento;
	}

	@Column(name = "VALORDIFERENCIASUMAASEGURADA", nullable = false)
	public BigDecimal getValorDiferenciaSumaAsegurada() {
		return this.valorDiferenciaSumaAsegurada;
	}

	public void setValorDiferenciaSumaAsegurada(
			BigDecimal valorDiferenciaSumaAsegurada) {
		this.valorDiferenciaSumaAsegurada = valorDiferenciaSumaAsegurada;
	}

	@Column(name = "VALORDIFERENCIAPRIMANETA", nullable = false)
	public BigDecimal getValorDiferenciaPrimaNeta() {
		return this.valorDiferenciaPrimaNeta;
	}

	public void setValorDiferenciaPrimaNeta(BigDecimal valorDiferenciaPrimaNeta) {
		this.valorDiferenciaPrimaNeta = valorDiferenciaPrimaNeta;
	}

	@Column(name = "VALORDIFERENCIACUOTA", nullable = false)
	public BigDecimal getValorDiferenciaCuota() {
		return this.valorDiferenciaCuota;
	}

	public void setValorDiferenciaCuota(BigDecimal valorDiferenciaCuota) {
		this.valorDiferenciaCuota = valorDiferenciaCuota;
	}

	@Column(name = "DESCRIPCIONMOVIMIENTO", length = 300)
	public String getDescripcionMovimiento() {
		return this.descripcionMovimiento;
	}

	public void setDescripcionMovimiento(String descripcionMovimiento) {
		this.descripcionMovimiento = descripcionMovimiento;
	}

	@Transient
	public String getDescripcionSeccion() {
		return descripcionSeccion;
	}

	public void setDescripcionSeccion(String descripcionSeccion) {
		this.descripcionSeccion = descripcionSeccion;
	}

	@Column(name="NUMEROSUBINCISO")
	public BigDecimal getNumeroSubInciso() {
		return numeroSubInciso;
	}

	public void setNumeroSubInciso(BigDecimal numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}

	@Column(name="CLAVEREGENERARECIBOS")
	public Short getClaveRegeneraRecibos() {
		return claveRegeneraRecibos;
	}

	public void setClaveRegeneraRecibos(Short claveRegeneraRecibos) {
		this.claveRegeneraRecibos = claveRegeneraRecibos;
	}
	
	@Column(name="CLAVEAGRUPACION")
	public Short getClaveAgrupacion() {
		return claveAgrupacion;
	}

	public void setClaveAgrupacion(Short claveAgrupacion) {
		this.claveAgrupacion = claveAgrupacion;
	}

	@Column(name="IDTOCONTROLARCHIVO")
	public BigDecimal getIdToControlArchivo() {
		return idToControlArchivo;
	}

	public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
		this.idToControlArchivo = idToControlArchivo;
	}

}