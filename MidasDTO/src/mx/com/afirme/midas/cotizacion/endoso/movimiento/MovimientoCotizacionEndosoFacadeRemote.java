package mx.com.afirme.midas.cotizacion.endoso.movimiento;

// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for MovimientoCotizacionEndosoDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface MovimientoCotizacionEndosoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved
	 * MovimientoCotizacionEndosoDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            MovimientoCotizacionEndosoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(MovimientoCotizacionEndosoDTO entity);

	/**
	 * Delete a persistent MovimientoCotizacionEndosoDTO entity.
	 * 
	 * @param entity
	 *            MovimientoCotizacionEndosoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(MovimientoCotizacionEndosoDTO entity);

	/**
	 * Persist a previously saved MovimientoCotizacionEndosoDTO entity and
	 * return it or a copy of it to the sender. A copy of the
	 * MovimientoCotizacionEndosoDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            MovimientoCotizacionEndosoDTO entity to update
	 * @return MovimientoCotizacionEndosoDTO the persisted
	 *         MovimientoCotizacionEndosoDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public MovimientoCotizacionEndosoDTO update(
			MovimientoCotizacionEndosoDTO entity);

	public MovimientoCotizacionEndosoDTO findById(BigDecimal id);

	public void borrarPorIdToCotizacion(BigDecimal id);

	/**
	 * Find all MovimientoCotizacionEndosoDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the MovimientoCotizacionEndosoDTO property to
	 *            query
	 * @param value
	 *            the property value to match
	 * @return List<MovimientoCotizacionEndosoDTO> found by query
	 */
	public List<MovimientoCotizacionEndosoDTO> findByProperty(
			String propertyName, Object value);

	/**
	 * Find all MovimientoCotizacionEndosoDTO entities.
	 * 
	 * @return List<MovimientoCotizacionEndosoDTO> all
	 *         MovimientoCotizacionEndosoDTO entities
	 */
	public List<MovimientoCotizacionEndosoDTO> findAll();

	public List<MovimientoCotizacionEndosoDTO> obtenerMovimientosPorCotizacion(BigDecimal idToCotizacion);

	public List<MovimientoCotizacionEndosoDTO> listarFiltrado(MovimientoCotizacionEndosoDTO entity);
	
	public BigDecimal obtenerDiferenciaPrimaNeta(BigDecimal idToCotizacion);
	
	public List<MovimientoCotizacionEndosoDTO> obtieneMovimientosAEvaluar(BigDecimal idToCotizacion);

	public void registrarMovimientos(List<MovimientoCotizacionEndosoDTO> listaMovimientos);
	
}