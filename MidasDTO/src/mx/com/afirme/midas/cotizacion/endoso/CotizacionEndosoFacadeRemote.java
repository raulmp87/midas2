package mx.com.afirme.midas.cotizacion.endoso;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;


public interface CotizacionEndosoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved CotizacionDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            CotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public CotizacionDTO save(CotizacionDTO entity);

	/**
	 * Delete a persistent CotizacionDTO entity.
	 * 
	 * @param entity
	 *            CotizacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(CotizacionDTO entity);

	/**
	 * Persist a previously saved CotizacionDTO entity and return it or a copy
	 * of it to the sender. A copy of the CotizacionDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            CotizacionDTO entity to update
	 * @return CotizacionDTO the persisted CotizacionDTO entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public CotizacionDTO update(CotizacionDTO entity);

	public CotizacionDTO findById(BigDecimal id);

	/**
	 * Find all CotizacionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the CotizacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<CotizacionDTO> found by query
	 */
	public List<CotizacionDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all CotizacionDTO entities.
	 * 
	 * @return List<CotizacionDTO> all CotizacionDTO entities
	 */
	public List<CotizacionDTO> findAll();

	public List<CotizacionDTO> listarFiltrado(CotizacionDTO cotizacionDTO);
	
	public List<CotizacionDTO> listarCotizaciones();
	
	public Long obtenerTotalFiltrado(CotizacionDTO cotizacionDTO);

}
