package mx.com.afirme.midas.cotizacion;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.base.LogBaseDTO;
import mx.com.afirme.midas.catalogos.tiponegocio.TipoNegocioDTO;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDTO;
import mx.com.afirme.midas.cotizacion.documento.DocumentoAnexoReaseguroCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.DocumentoDigitalCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.direccion.DireccionDTO;
import mx.com.afirme.midas.persona.PersonaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.catalogos.EnumBase;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.Cotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.cobranza.programapago.ToProgPago;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;

import org.apache.openjpa.persistence.ReadOnly;

/**
 * CotizacionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOCOTIZACION", schema = "MIDAS")
public class CotizacionDTO extends LogBaseDTO implements Entidad{
	private static final long serialVersionUID = -2501860420078593766L;
	public enum ACCION {CREACION,MODIFICACION,LIBERACION,ASIGNACION_EMISION,EMISION,CANCELACION,RECHAZO,ASIGNACION_COORDINADOR,ASIGNACION_SUSCRIPTOR}

	private BigDecimal idToCotizacion;
	private TipoPolizaDTO tipoPolizaDTO;
	private SolicitudDTO solicitudDTO;
	private DireccionDTO direccionCobroDTO;
	private BigDecimal idMoneda;
	private BigDecimal idFormaPago;
	private Date fechaInicioVigencia;
	private Date fechaFinVigencia;
	private String nombreEmpresaContratante;
	private String nombreEmpresaAsegurado;
	private String codigoUsuarioOrdenTrabajo;
	private String codigoUsuarioCotizacion;
	private String codigoUsuarioEmision;
	private Date fechaCreacion;
	private String codigoUsuarioCreacion;
	private Date fechaModificacion;
	private String codigoUsuarioModificacion;
	private Short claveEstatus;
	private Double porcentajebonifcomision;
	private BigDecimal idToPersonaContratante;
	private BigDecimal idToPersonaAsegurado;
	private BigDecimal idMedioPago;
	private List<IncisoCotizacionDTO> incisoCotizacionDTOs = new ArrayList<IncisoCotizacionDTO>(1);
	private List<DocumentoDigitalCotizacionDTO> documentoDigitalCotizacionDTOs = new ArrayList<DocumentoDigitalCotizacionDTO>(1);
	private List<DocAnexoCotDTO> documentoAnexoCotizacionDTOs = new ArrayList<DocAnexoCotDTO>(1);
	private List<DocumentoAnexoReaseguroCotizacionDTO> documentoAnexoReaseguroCotizacionDTOs = new ArrayList<DocumentoAnexoReaseguroCotizacionDTO>(1);
	private Short claveAutRetroacDifer;
	private Short claveAutVigenciaMaxMin;
	private Integer diasGracia;
	private String folioPolizaAsociada;
	private TipoNegocioDTO tipoNegocioDTO;
	private Short claveAutoEmisionDocOperIlicitas;
	private String nombreAsegurado;
	private String nombreContratante;
	private String telefonoContratante;
	private String correoContratante;
	private Double tipoCambio;
	private Short claveMotivoEndoso;
	private Double porcentajeIva;
	private Double primaNetaAnual;
	private Double primaNetaCotizacion;
	private Double montoRecargoPagoFraccionado;
	private Double derechosPoliza;
	private Double factorIVA;
	private Double montoIVA;
	private Double primaNetaTotal;
	private Double montoBonificacionComision;
	private Double montoBonificacionComisionPagoFraccionado;
	private Double montoComisionPagoFraccionado;
	private Double porcentajePagoFraccionado;
	private Double diasPorDevengar;
	private List<TexAdicionalCotDTO> texAdicionalCotDTOs = new ArrayList<TexAdicionalCotDTO>(1);
	private List<ArchivosPendientes> archivosPendientes = new ArrayList<CotizacionDTO.ArchivosPendientes>(1);
	private String aclaraciones;
	private BigDecimal idCentroEmisor; 
	private String centroEmisorDescripcion;
	// Agregados para autorizaciones
	private Date fechaSolAutRetroacDifer;
	private Date fechaAutRetroacDifer;
	private String codigoUsuarioAutRetroacDifer;
	private Date fechaSolAutVigenciaMaxMin;
	private Date fechaAutVigenciaMaxMin;
	private String codigoUsuarioAutVigenciaMaxMin;
	private CotizacionContinuity cotizacionContinuity;
	
	// Agregados para modificacin de derechos y recargo por pago fraccionado		
	private Short claveDerechosUsuario;
	private Double valorDerechosUsuario;
	private Short claveRecargoPagoFraccionadoUsuario;
	private Double porcentajeRecargoPagoFraccionadoUsuario;
	private Double valorRecargoPagoFraccionadoUsuario;
	private Short claveAutorizacionDerechos; 	           
	private String codigoUsuarioAutorizacionDerechos;
	private Date fechaAutorizacionDerechos;
	private Short claveAutorizacionRecargoPagoFraccionado;
	private String codigoUsuarioAutorizacionRecargoPagoFraccionado;
	private Date fechaAutorizacionRecargoPagoFraccionado;
	private String descripcionLicitacion;
	private String descripcionLicitacionEndoso;
	private Short claveImpresionSumaAsegurada;
	
	//13/06/2011 Agregados para registro de autorizacion de dias de gracia.
	private String codigoUsuarioSolicitaAutDiasGracia;
	private Date fechaSolicitudAutDiasGracia;
	private Short claveAutorizacionDiasGracia;
	private String codigoUsuarioAutorizaDiasGracia;
	private Date fechaAutorizaDiasGracia;
	
	private Date fechaLiberacion;
	
	private BigDecimal idDomicilioContratante;
	private BigDecimal idDomicilioAsegurado;
	
	private NegocioTipoPoliza negocioTipoPoliza;
	private Long idConductoCobroCliente;
	private String numeroPrimerRecibo;
	private String digitoVerificadorRecibo;
	private Boolean primerReciboPagado;
	private String numeroAutorizacion;
	private Boolean igualacionNivelCotizacion = Boolean.FALSE;
	private Double primaTotalAntesDeIgualacion;
	private Double descuentoIgualacionPrimas;
	private BigDecimal numeroAutoexpedible;
	private BigDecimal tipoImpresionCliente;
	private Date fechaFinVigenciaHasta;
	private SeccionDTO seccionDTO;
	
	 private BigDecimal numeroPoliza;
	 private String estatusPoliza;
	 
	 //Agregados para relacionar la cotizacion con folio y numero de credito de originacion
	 private String folio;
	 private String numeroCredito;
	
	/*
	 * 22/08/2011. JLAB. Dato usado en el cotizador casa, actualmente no se registra beneficiario en Midas
	 */
	private PersonaDTO beneficiario;
	
	private Double porcentajeDescuentoGlobal;
 	private Double descuentoVolumen;   // Identificador descuento Volumen
 	private NegocioDerechoPoliza negocioDerechoPoliza;
 	private List<ComisionCotizacionDTO> comisionCotizacionDTOs = new ArrayList<ComisionCotizacionDTO>(1);
 	private BigDecimal valorPrimaTotal;
 	private BigDecimal valorTotalPrimas;
 
 	private Date fechaSeguimiento;
 	private Boolean conflictoNumeroSerie;
 	private String descripcionMoneda;
 	private Double descuentoOriginacion;
 	
 	//Descuento aplicado
 	private String descuendoAplicadoCotizacion;
 	
 	//agrupacion 20131216
 	private String tipoAgrupacionRecibos;
 	
 	//recuotificacion 20140311
 	private List<ToProgPago> programaPagos;
 	//recuotificacion 20170104
 	private Boolean recuotificadaAutomatica;
 	private Boolean recuotificadaManual;
 	
 	//Condiciones Especiales
 	private List<CondicionEspecial> condicionesEspeciales = new ArrayList<CondicionEspecial>(1);
 	
 	private String tipoCotizacion;

	private Double primaTarifa;
	
	//Valores de prima
	private BigDecimal valorComisionCedida;
	private BigDecimal valorRecargo;
	private BigDecimal valorDerechos;
	private BigDecimal valorIva;
	
	//Carga Masiva Servicio Publico
	private String numPolizaServicioPublico;
	private String numCentroEmisor;
	//Added for AS360 project.
	/** Ejecutivo que coloca la poliza */
	private String ejecutivoColoca;
	
	private Boolean sinCompensacion;

	public enum TIPO_AGRUPACION_RECIBOS implements EnumBase<String> {
		UBICACION("UB", "UBICACION"), AGRUPADO("NE", "AGRUPADO"), FILIAL("FI", "FILIAL");
		private String estatus;
		private String name;

		TIPO_AGRUPACION_RECIBOS(String estatus, String name) {
			this.estatus = estatus;
			this.name = name;
		}

		public String getValue() {
			return this.estatus;
		}

		public String getLabel() {
			return this.name;
		}
		
		public static String findByEstatus(String estatus){
		    for(TIPO_AGRUPACION_RECIBOS tipoAgrupacion : values()){
		        if( tipoAgrupacion.getValue().equals(estatus)){
		            return tipoAgrupacion.getLabel();
		        }
		    }
		    return null;
		}

	}
 	//KEY NAME STRING
	public static final String IDTOCOTIZACION = "idToCotizacion";	
	//Constants
	public static final short ESTATUS_COT_ASIGNADA_EMISION = 15;
	public static final short ESTATUS_COT_EMITIDA = 16;
	public static final short ESTATUS_COT_ASIGNADA = 11;
	public static final short ESTATUS_COT_EN_PROCESO = 10;
	public static final short ESTATUS_COT_TERMINADA = 12;
	public static final short ESTATUS_COT_CANCELADA = 19;
	public static final short ESTATUS_COT_VENCIDA = 18;
	
	//Folio
	public static final String NUEVOCOTIZADOR = "C_N_COTIZADOR";
	
	//Tipocotizacion indica desde que cotizador se creo
	public static final String TIPO_COTIZACION_SERVICIO_PUBLICO = "TCSP";
	public static final String TIPO_COTIZACION_AGENTES = "C_N_COTIZADOR";
	public static final String TIPO_COTIZACION_PORTAL = "TCP";
	public static final String TIPO_COTIZACION_MOVIL = "TCM";
	public static final String TIPO_COTIZACION_AUTOPLAZO = "AUP";
	public static final String TIPO_COTIZACION_AUTOINDIVIDUAL = "AUI";
	public static final String TIPO_COTIZACION_SEGURO_OBLIGATORIO = "TCSO";
	
	public class ArchivosPendientes implements Serializable {
		private static final long serialVersionUID = 1L;
		private ControlArchivoDTO archivoOriginal;
		private ControlArchivoDTO archivoNuevo;

		public void setArchivoOriginal(ControlArchivoDTO archivoOriginal) {
			this.archivoOriginal = archivoOriginal;
		}

		public ControlArchivoDTO getArchivoOriginal() {
			return archivoOriginal;
		}

		public void setArchivoNuevo(ControlArchivoDTO archivoNuevo) {
			this.archivoNuevo = archivoNuevo;
		}

		public ControlArchivoDTO getArchivoNuevo() {
			return archivoNuevo;
		}
	}

	// Constructors

	/** default constructor */
	public CotizacionDTO() {
	}
	
	public CotizacionDTO(Cotizacion cotizacion){
		
		this.aclaraciones = cotizacion.getAclaraciones();
		this.beneficiario = cotizacion.getBeneficiario();
		//cotizacion.getClaveAfectacionPrimas();
		this.claveAutoEmisionDocOperIlicitas = cotizacion.getClaveAutoOmisionDocOperIli();
		this.claveAutorizacionDerechos = cotizacion.getClaveAutorizacionDerechos();
		this.claveAutorizacionDiasGracia = cotizacion.getClaveAutorizacionDiasGracia();
		this.claveAutorizacionRecargoPagoFraccionado = cotizacion.getClaveAutorizacionRecargoPagoFraccionado();
		this.claveAutRetroacDifer = cotizacion.getClaveAutRetroacDifer();
		this.claveAutVigenciaMaxMin = cotizacion.getClaveAutVigenciaMaxMin();
		this.claveDerechosUsuario = cotizacion.getClaveDerechosUsuario();
		this.claveEstatus = cotizacion.getClaveEstatus();
		this.claveImpresionSumaAsegurada = cotizacion.getClaveImpresionSumaAsegurada();
		this.claveMotivoEndoso = cotizacion.getClaveMotivoEndoso();
		this.claveRecargoPagoFraccionadoUsuario = cotizacion.getClaveRecargoPagoFraccionadoUsuario();
		//cotizacion.getCodigoUsuarioAutOmisionDocOperIli();
		this.codigoUsuarioAutorizacionDerechos = cotizacion.getCodigoUsuarioAutorizacionDerechos();
		this.codigoUsuarioAutorizacionRecargoPagoFraccionado = cotizacion.getCodigoUsuarioAutorizacionRecargoPagoFraccionado();
		this.codigoUsuarioAutorizaDiasGracia = cotizacion.getCodigoUsuarioAutorizaDiasGracia();
		this.codigoUsuarioAutRetroacDifer = cotizacion.getCodigoUsuarioAutRetroacDifer();
		this.codigoUsuarioAutVigenciaMaxMin = cotizacion.getCodigoUsuarioAutVigenciaMaxMin();
		//Comentario Integracion
		this.codigoUsuarioCotizacion = cotizacion.getCodigoUsuarioCotizacion();
		this.codigoUsuarioCreacion = cotizacion.getCodigoUsuarioCreacion();
		this.codigoUsuarioEmision = cotizacion.getCodigoUsuarioEmision();
		this.codigoUsuarioModificacion = cotizacion.getCodigoUsuarioModificacion();
		this.codigoUsuarioOrdenTrabajo = cotizacion.getCodigoUsuarioOrdenTrabajo();
		this.codigoUsuarioSolicitaAutDiasGracia = cotizacion.getCodigoUsuarioSolicitaAutDiasGracia();
		this.conflictoNumeroSerie = cotizacion.getConflictoNumeroSerie();
		this.derechosPoliza = cotizacion.getDerechosPoliza();
		this.descripcionLicitacion = cotizacion.getDescripcionLicitacion();
		this.descripcionLicitacionEndoso = cotizacion.getDescripcionLicitacionEndoso();
		this.descuentoIgualacionPrimas = cotizacion.getDescuentoIgualacionPrimas();
		this.descuentoVolumen = cotizacion.getDescuentoVolumen();
		this.diasGracia = cotizacion.getDiasGracia();
		this.diasPorDevengar = cotizacion.getDiasPorDevengar();
		this.direccionCobroDTO = cotizacion.getDireccionCobro();
		this.idDomicilioContratante = cotizacion.getDomicilioContratanteId();
		this.idDomicilioAsegurado = cotizacion.getDoomicilioAseguradoId();
		//cotizacion.getEstatusCobranza();		
		this.factorIVA = cotizacion.getFactorIVA();
		this.fechaAutorizacionDerechos = cotizacion.getFechaAutorizacionDerechos();
		this.fechaAutorizacionRecargoPagoFraccionado = cotizacion.getFechaAutorizacionRecargoPagoFraccionado();
		this.fechaAutorizaDiasGracia = cotizacion.getFechaAutorizaDiasGracia();
		this.fechaAutRetroacDifer = cotizacion.getFechaAutRetroacDifer();
		this.fechaAutVigenciaMaxMin = cotizacion.getFechaAutVigenciaMaxMin();
		this.fechaCreacion = cotizacion.getFechaCreacion();
		this.fechaFinVigencia = cotizacion.getFechaFinVigencia();
		this.fechaInicioVigencia = cotizacion.getFechaInicioVigencia();
		this.fechaLiberacion = cotizacion.getFechaLiberacion();
		this.fechaModificacion = cotizacion.getFechaModificacion();
		this.fechaSeguimiento = cotizacion.getFechaSeguimiento();
		//cotizacion.getFechaSolAutOmisionDocOperIli();
		this.fechaSolAutRetroacDifer = cotizacion.getFechaSolAutRetroacDifer();
		this.fechaSolAutVigenciaMaxMin = cotizacion.getFechaSolAutVigenciaMaxMin();
		this.fechaSolicitudAutDiasGracia = cotizacion.getFechaSolicitudAutDiasGracia();
		this.folioPolizaAsociada = cotizacion.getFolioPolizaAsociada();
		this.igualacionNivelCotizacion = cotizacion.getIgualacionNivelCotizacion();
		// cotizacion.getImporteNotaCredito();
		this.idFormaPago = new BigDecimal(cotizacion.getFormaPago().getIdFormaPago());
		this.idMedioPago = new BigDecimal(cotizacion.getMedioPago().getIdMedioPago());
		this.idMoneda = new BigDecimal(cotizacion.getMoneda().getIdTcMoneda());
		this.montoBonificacionComision = cotizacion.getMontoBonificacionComision();
		this.montoBonificacionComisionPagoFraccionado = cotizacion.getMontoBonificacionComisionPagoFraccionado();
		this.montoComisionPagoFraccionado = cotizacion.getMontoComisionPagoFraccionado();
		this.montoIVA = cotizacion.getMontoIVA();
		this.montoRecargoPagoFraccionado = cotizacion.getMontoRecargoPagoFraccionado();
		this.negocioDerechoPoliza = cotizacion.getNegocioDerechoPoliza();
		this.negocioTipoPoliza = cotizacion.getNegocioTipoPoliza();
		this.nombreAsegurado = cotizacion.getNombreAsegurado();
		this.nombreContratante = cotizacion.getNombreContratante();
		this.nombreEmpresaAsegurado = cotizacion.getNombreEmpresaAsegurado();
		this.nombreEmpresaContratante = cotizacion.getNombreEmpresaContratante();
		this.idToPersonaAsegurado = new BigDecimal(cotizacion.getPersonaAseguradoId()!=null?cotizacion.getPersonaAseguradoId():0);
		this.idToPersonaContratante = new BigDecimal(cotizacion.getPersonaContratanteId()!=null?cotizacion.getPersonaContratanteId():0);
		this.porcentajebonifcomision = cotizacion.getPorcentajebonifcomision();
		this.porcentajeDescuentoGlobal = cotizacion.getPorcentajeDescuentoGlobal();
		this.porcentajeIva = cotizacion.getPorcentajeIva();
		this.porcentajePagoFraccionado = cotizacion.getPorcentajePagoFraccionado();
		this.porcentajeRecargoPagoFraccionadoUsuario = cotizacion.getPorcentajeRecargoPagoFraccionadoUsuario();
		this.primaNetaAnual = cotizacion.getPrimaNetaAnual();
		this.primaNetaCotizacion = cotizacion.getPrimaNetaCotizacion();
		this.primaNetaTotal = cotizacion.getPrimaNetaTotal();
		this.primaTotalAntesDeIgualacion =	cotizacion.getPrimaTotalAntesDeIgualacion();
		this.solicitudDTO = cotizacion.getSolicitud();
		this.tipoCambio = cotizacion.getTipoCambio();
		this.tipoNegocioDTO = cotizacion.getTipoNegocio();
		this.tipoPolizaDTO = cotizacion.getTipoPoliza();
		this.valorDerechosUsuario = cotizacion.getValorDerechosUsuario();
		this.valorPrimaTotal = cotizacion.getValorPrimaTotal();
		this.valorRecargoPagoFraccionadoUsuario = cotizacion.getValorRecargoPagoFraccionadoUsuario();
		this.valorTotalPrimas = cotizacion.getValorTotalPrimas();
		this.idConductoCobroCliente = cotizacion.getIdConductoCobroCliente();
		//this.ejecutivoColoca = cotizacion.getEjecutivoColoca(); // para que no cause conflictos con bitemporalidad
		
	}

	// Property accessors
	@Id
	//@SequenceGenerator(name = "IDTOCOTIZACION_SEQ_GENERADOR", allocationSize = 1, sequenceName = "IDTOCOTIZACION_SEQ")
	//@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOCOTIZACION_SEQ_GENERADOR")
	@Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCotizacion() {
		return this.idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	@Column(name = "PORCENTAJEBONIFCOMISION", nullable = true, precision = 8, scale = 4)
	public Double getPorcentajebonifcomision() {
		return this.porcentajebonifcomision;
	}

	public void setPorcentajebonifcomision(Double porcentajebonifcomision) {
		this.porcentajebonifcomision = porcentajebonifcomision;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOTIPOPOLIZA")
	public TipoPolizaDTO getTipoPolizaDTO() {
		return tipoPolizaDTO;
	}

	public void setTipoPolizaDTO(TipoPolizaDTO tipoPolizaDTO) {
		this.tipoPolizaDTO = tipoPolizaDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOSOLICITUD", nullable = false)
	public SolicitudDTO getSolicitudDTO() {
		return solicitudDTO;
	}

	public void setSolicitudDTO(SolicitudDTO solicitudDTO) {
		this.solicitudDTO = solicitudDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTODIRECCIONCOBRO")
	public DireccionDTO getDireccionCobroDTO() {
		return direccionCobroDTO;
	}

	public void setDireccionCobroDTO(DireccionDTO direccionCobroDTO) {
		this.direccionCobroDTO = direccionCobroDTO;
	}

	@Column(name = "IDMONEDA", precision = 22, scale = 0)
	public BigDecimal getIdMoneda() {
		return this.idMoneda;
	}

	public void setIdMoneda(BigDecimal idMoneda) {
		this.idMoneda = idMoneda;
	}

	@Column(name = "IDFORMAPAGO", precision = 22, scale = 0)
	public BigDecimal getIdFormaPago() {
		return this.idFormaPago;
	}

	public void setIdFormaPago(BigDecimal idFormaPago) {
		if (idFormaPago != new BigDecimal(-1)) {
			this.idFormaPago = idFormaPago;
		}
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAINICIOVIGENCIA", length = 7)
	public Date getFechaInicioVigencia() {
		return this.fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAFINVIGENCIA", length = 7)
	public Date getFechaFinVigencia() {
		return this.fechaFinVigencia;
	}

	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	@Column(name = "NOMBREEMPRESACONTRATANTE", length = 200)
	public String getNombreEmpresaContratante() {
		return this.nombreEmpresaContratante;
	}

	public void setNombreEmpresaContratante(String nombreEmpresaContratante) {
		this.nombreEmpresaContratante = nombreEmpresaContratante;
	}

	@Column(name = "NOMBREEMPRESAASEGURADO", length = 200)
	public String getNombreEmpresaAsegurado() {
		return this.nombreEmpresaAsegurado;
	}

	public void setNombreEmpresaAsegurado(String nombreEmpresaAsegurado) {
		this.nombreEmpresaAsegurado = nombreEmpresaAsegurado;
	}

	@Column(name = "CODIGOUSUARIOORDENTRABAJO", length = 8)
	public String getCodigoUsuarioOrdenTrabajo() {
		return this.codigoUsuarioOrdenTrabajo;
	}

	public void setCodigoUsuarioOrdenTrabajo(String codigoUsuarioOrdenTrabajo) {
		this.codigoUsuarioOrdenTrabajo = codigoUsuarioOrdenTrabajo;
	}

	@Column(name = "CODIGOUSUARIOCOTIZACION", length = 8)
	public String getCodigoUsuarioCotizacion() {
		return this.codigoUsuarioCotizacion;
	}

	public void setCodigoUsuarioCotizacion(String codigoUsuarioCotizacion) {
		this.codigoUsuarioCotizacion = codigoUsuarioCotizacion;
	}

	@Column(name = "CODIGOUSUARIOEMISION", length = 8)
	public String getCodigoUsuarioEmision() {
		return codigoUsuarioEmision;
	}

	public void setCodigoUsuarioEmision(String codigoUsuarioEmision) {
		this.codigoUsuarioEmision = codigoUsuarioEmision;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHACREACION", nullable = false, length = 7)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Column(name = "EJECUTIVO_COLOCA", nullable = true, length = 100)
	public String getEjecutivoColoca() {
		return this.ejecutivoColoca;
	}

	public void setEjecutivoColoca(String ejecutivoColoca) {
		this.ejecutivoColoca = ejecutivoColoca;
	}
	
	@Column(name = "CODIGOUSUARIOCREACION", nullable = false, length = 8)
	public String getCodigoUsuarioCreacion() {
		return this.codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAMODIFICACION", nullable = false, length = 7)
	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Column(name = "CODIGOUSUARIOMODIFICACION", nullable = false, length = 8)
	public String getCodigoUsuarioModificacion() {
		return this.codigoUsuarioModificacion;
	}

	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}

	@Column(name = "IDMEDIOPAGO")
	public BigDecimal getIdMedioPago() {
		return idMedioPago;
	}

	public void setIdMedioPago(BigDecimal idMedioPago) {
		this.idMedioPago = idMedioPago;
	}

	@Column(name = "CLAVEESTATUS", nullable = false, precision = 4, scale = 0)
	public Short getClaveEstatus() {
		return claveEstatus;
	}

	public void setClaveEstatus(Short claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	@Column(name = "IDTOPERSONACONTRATANTE")
	public BigDecimal getIdToPersonaContratante() {
		return idToPersonaContratante;
	}

	public void setIdToPersonaContratante(BigDecimal idToPersonaContratante) {
		this.idToPersonaContratante = idToPersonaContratante;
	}

	@Column(name = "IDTOPERSONAASEGURADO")
	public BigDecimal getIdToPersonaAsegurado() {
		return idToPersonaAsegurado;
	}

	public void setIdToPersonaAsegurado(BigDecimal idToPersonaAsegurado) {
		this.idToPersonaAsegurado = idToPersonaAsegurado;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "cotizacionDTO")
	public List<IncisoCotizacionDTO> getIncisoCotizacionDTOs() {
		return incisoCotizacionDTOs;
	}

	public void setIncisoCotizacionDTOs(
			List<IncisoCotizacionDTO> incisoCotizacionDTOs) {
		this.incisoCotizacionDTOs = incisoCotizacionDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "cotizacionDTO")
	public List<DocumentoAnexoReaseguroCotizacionDTO> getDocumentoAnexoReaseguroCotizacionDTOs() {
		return documentoAnexoReaseguroCotizacionDTOs;
	}

	public void setDocumentoAnexoReaseguroCotizacionDTOs(
			List<DocumentoAnexoReaseguroCotizacionDTO> documentoAnexoReaseguroCotizacionDTOs) {
		this.documentoAnexoReaseguroCotizacionDTOs = documentoAnexoReaseguroCotizacionDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "cotizacionDTO")
	public List<DocumentoDigitalCotizacionDTO> getDocumentoDigitalCotizacionDTOs() {
		return documentoDigitalCotizacionDTOs;
	}

	public void setDocumentoDigitalCotizacionDTOs(
			List<DocumentoDigitalCotizacionDTO> documentoDigitalCotizacionDTOs) {
		this.documentoDigitalCotizacionDTOs = documentoDigitalCotizacionDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "cotizacionDTO")
	public List<DocAnexoCotDTO> getDocumentoAnexoCotizacionDTOs() {
		return documentoAnexoCotizacionDTOs;
	}

	public void setDocumentoAnexoCotizacionDTOs(
			List<DocAnexoCotDTO> documentoAnexoCotizacionDTOs) {
		this.documentoAnexoCotizacionDTOs = documentoAnexoCotizacionDTOs;
	}

	@Column(name = "CLAVEAUTRETROACDIFER")
	public Short getClaveAutRetroacDifer() {
		return claveAutRetroacDifer;
	}

	public void setClaveAutRetroacDifer(Short claveAutRetroacDifer) {
		this.claveAutRetroacDifer = claveAutRetroacDifer;
	}

	@Column(name = "CLAVEAUTVIGENCIAMAXMIN")
	public Short getClaveAutVigenciaMaxMin() {
		return claveAutVigenciaMaxMin;
	}

	public void setClaveAutVigenciaMaxMin(Short claveAutVigenciaMaxMin) {
		this.claveAutVigenciaMaxMin = claveAutVigenciaMaxMin;
	}

	@Column(name = "DIASGRACIA")
	public Integer getDiasGracia() {
		return diasGracia;
	}

	public void setDiasGracia(Integer diasGracia) {
		this.diasGracia = diasGracia;
	}

	@Column(name = "FOLIOPOLIZAASOCIADA", length = 25)
	public String getFolioPolizaAsociada() {
		return folioPolizaAsociada;
	}

	public void setFolioPolizaAsociada(String folioPolizaAsociada) {
		this.folioPolizaAsociada = folioPolizaAsociada;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCTIPONEGOCIO")
	public TipoNegocioDTO getTipoNegocioDTO() {
		return tipoNegocioDTO;
	}

	public void setTipoNegocioDTO(TipoNegocioDTO tipoNegocioDTO) {
		this.tipoNegocioDTO = tipoNegocioDTO;
	}

	public void setClaveAutoEmisionDocOperIlicitas(
			Short claveAutoEmisionDocOperIlicitas) {
		this.claveAutoEmisionDocOperIlicitas = claveAutoEmisionDocOperIlicitas;
	}

	@Column(name = "CLAVEAUTOMISIONDOCOPERILICITAS")
	public Short getClaveAutoEmisionDocOperIlicitas() {
		return claveAutoEmisionDocOperIlicitas;
	}

	@Column(name = "NOMBREASEGURADO", length = 240)
	public String getNombreAsegurado() {
		return this.nombreAsegurado;
	}

	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}

	@Column(name = "NOMBRECONTRATANTE", length = 240)
	public String getNombreContratante() {
		return this.nombreContratante;
	}

	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}

	@Column(name = "TELEFONOCONTRATANTE", length = 15)
	public String getTelefonoContratante() {
		return telefonoContratante;
	}

	public void setTelefonoContratante(String telefonoContratante) {
		this.telefonoContratante = telefonoContratante;
	}

	@Column(name = "CORREOCONTRATANTE", length = 60)
	public String getCorreoContratante() {
		return correoContratante;
	}

	public void setCorreoContratante(String correoContratante) {
		this.correoContratante = correoContratante;
	}

	@Column(name = "TIPOCAMBIO")
	public Double getTipoCambio() {
		return tipoCambio;
	}

	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	@Column(name = "CLAVEMOTIVOENDOSO")
	public Short getClaveMotivoEndoso() {
		return claveMotivoEndoso;
	}

	public void setClaveMotivoEndoso(Short claveMotivoEndoso) {
		this.claveMotivoEndoso = claveMotivoEndoso;
	}

	@Transient
	public Double getPrimaNetaAnual() {
		return primaNetaAnual;
	}

	public void setPrimaNetaAnual(Double primaNetaAnual) {
		this.primaNetaAnual = primaNetaAnual;
	}

	@Transient
	public Double getPrimaNetaCotizacion() {
		return primaNetaCotizacion;
	}

	public void setPrimaNetaCotizacion(Double primaNetaCotizacion) {
		this.primaNetaCotizacion = primaNetaCotizacion;
	}

	@Transient
	public Double getMontoRecargoPagoFraccionado() {
		return montoRecargoPagoFraccionado;
	}

	public void setMontoRecargoPagoFraccionado(
			Double montoRecargoPagoFraccionado) {
		this.montoRecargoPagoFraccionado = montoRecargoPagoFraccionado;
	}

	@Transient
	public Double getDerechosPoliza() {
		return derechosPoliza;
	}

	public void setDerechosPoliza(Double derechosPoliza) {
		this.derechosPoliza = derechosPoliza;
	}

	@Transient
	public Double getFactorIVA() {
		return factorIVA;
	}

	public void setFactorIVA(Double factorIVA) {
		this.factorIVA = factorIVA;
	}

	@Transient
	public Double getMontoIVA() {
		return montoIVA;
	}

	public void setMontoIVA(Double montoIVA) {
		this.montoIVA = montoIVA;
	}

	@Transient
	public Double getPrimaNetaTotal() {
		return primaNetaTotal;
	}

	public void setPrimaNetaTotal(Double primaNetaTotal) {
		this.primaNetaTotal = primaNetaTotal;
	}

	@Transient
	public Double getMontoBonificacionComision() {
		return montoBonificacionComision;
	}

	public void setMontoBonificacionComision(Double montoBonificacionComision) {
		this.montoBonificacionComision = montoBonificacionComision;
	}

	@Transient
	public Double getMontoBonificacionComisionPagoFraccionado() {
		return montoBonificacionComisionPagoFraccionado;
	}

	public void setMontoBonificacionComisionPagoFraccionado(
			Double montoBonificacionComisionPagoFraccionado) {
		this.montoBonificacionComisionPagoFraccionado = montoBonificacionComisionPagoFraccionado;
	}

	@Transient
	public Double getMontoComisionPagoFraccionado() {
		return montoComisionPagoFraccionado;
	}

	public void setMontoComisionPagoFraccionado(Double montoComisionPagoFraccionado) {
		this.montoComisionPagoFraccionado = montoComisionPagoFraccionado;
	}

	@Column(name = "PCTRECARGOPAGOFRACCIONADO")
	public Double getPorcentajePagoFraccionado() {
		return porcentajePagoFraccionado;
	}

	public void setPorcentajePagoFraccionado(Double porcentajePagoFraccionado) {
		this.porcentajePagoFraccionado = porcentajePagoFraccionado;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "cotizacion")
	public List<TexAdicionalCotDTO> getTexAdicionalCotDTOs() {
		return texAdicionalCotDTOs;
	}

	public void setTexAdicionalCotDTOs(
			List<TexAdicionalCotDTO> texAdicionalCotDTOs) {
		this.texAdicionalCotDTOs = texAdicionalCotDTOs;
	}

	@Transient
	public List<ArchivosPendientes> getArchivosPendientes() {
		return archivosPendientes;
	}

	public void addArchivoPendiente(ControlArchivoDTO archivoOriginal,
			ControlArchivoDTO archivoNuevo) {
		if (archivosPendientes == null) {
			archivosPendientes = new ArrayList<ArchivosPendientes>();
		}
		ArchivosPendientes pendiente = new ArchivosPendientes();
		pendiente.setArchivoOriginal(archivoOriginal);
		pendiente.setArchivoNuevo(archivoNuevo);
		archivosPendientes.add(pendiente);
	}

	@Column(name = "PORCENTAJEIVA")
	public Double getPorcentajeIva() {
		return porcentajeIva;
	}

	public void setPorcentajeIva(Double porcentajeIva) {
		this.porcentajeIva = porcentajeIva;
	}

	@Column(name = "ACLARACIONES", length = 2000)
	public String getAclaraciones() {
		return aclaraciones;
	}

	public void setAclaraciones(String aclaraciones) {
		this.aclaraciones = aclaraciones;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHASOLAUTVIGENCIAMAXMIN", length = 7)
	public Date getFechaSolAutVigenciaMaxMin() {
		return this.fechaSolAutVigenciaMaxMin;
	}

	public void setFechaSolAutVigenciaMaxMin(Date fechaSolAutVigenciaMaxMin) {
		this.fechaSolAutVigenciaMaxMin = fechaSolAutVigenciaMaxMin;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAAUTVIGENCIAMAXMIN", length = 7)
	public Date getFechaAutVigenciaMaxMin() {
		return this.fechaAutVigenciaMaxMin;
	}

	public void setFechaAutVigenciaMaxMin(Date fechaAutVigenciaMaxMin) {
		this.fechaAutVigenciaMaxMin = fechaAutVigenciaMaxMin;
	}

	@Column(name = "CODIGOUSUARIOAUTVIGENCIAMAXMIN", length = 8)
	public String getCodigoUsuarioAutVigenciaMaxMin() {
		return this.codigoUsuarioAutVigenciaMaxMin;
	}

	public void setCodigoUsuarioAutVigenciaMaxMin(
			String codigoUsuarioAutVigenciaMaxMin) {
		this.codigoUsuarioAutVigenciaMaxMin = codigoUsuarioAutVigenciaMaxMin;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHASOLAUTRETROACDIFER", length = 7)
	public Date getFechaSolAutRetroacDifer() {
		return this.fechaSolAutRetroacDifer;
	}

	public void setFechaSolAutRetroacDifer(Date fechaSolAutRetroacDifer) {
		this.fechaSolAutRetroacDifer = fechaSolAutRetroacDifer;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAAUTRETROACDIFER", length = 7)
	public Date getFechaAutRetroacDifer() {
		return this.fechaAutRetroacDifer;
	}

	public void setFechaAutRetroacDifer(Date fechaAutRetroacDifer) {
		this.fechaAutRetroacDifer = fechaAutRetroacDifer;
	}

	@Column(name = "CODIGOUSUARIOAUTRETROACDIFER", length = 8)
	public String getCodigoUsuarioAutRetroacDifer() {
		return this.codigoUsuarioAutRetroacDifer;
	}

	public void setCodigoUsuarioAutRetroacDifer(
			String codigoUsuarioAutRetroacDifer) {
		this.codigoUsuarioAutRetroacDifer = codigoUsuarioAutRetroacDifer;
	}

	@Transient
	public Double getDiasPorDevengar() {
		return diasPorDevengar;
	}

	public void setDiasPorDevengar(Double diasPorDevengar) {
		this.diasPorDevengar = diasPorDevengar;
	}
	
	@Column(name = "CLAVEDERECHOSUSUARIO", nullable = true)
	public Short getClaveDerechosUsuario() {
		return claveDerechosUsuario;
	}

	public void setClaveDerechosUsuario(Short claveDerechosUsuario) {
		this.claveDerechosUsuario = claveDerechosUsuario;
	}

	@Column(name = "VALORDERECHOSUSUARIO", nullable = true, precision = 24, scale = 10)
	public Double getValorDerechosUsuario() {
		return valorDerechosUsuario;
	}

	public void setValorDerechosUsuario(Double valorDerechosUsuario) {
		this.valorDerechosUsuario = valorDerechosUsuario;
	}

	@Column(name = "CLAVERECARGOPAGOFRACCUSUARIO", nullable = true)
	public Short getClaveRecargoPagoFraccionadoUsuario() {
		return claveRecargoPagoFraccionadoUsuario;
	}

	public void setClaveRecargoPagoFraccionadoUsuario(
			Short claveRecargoPagoFraccionadoUsuario) {
		this.claveRecargoPagoFraccionadoUsuario = claveRecargoPagoFraccionadoUsuario;
	}	
	
	@Column(name = "PORCENTAJERECPAGOFRACCUSUARIO", nullable = true, precision = 14, scale = 10)
	public Double getPorcentajeRecargoPagoFraccionadoUsuario() {
		return porcentajeRecargoPagoFraccionadoUsuario;
	}

	public void setPorcentajeRecargoPagoFraccionadoUsuario(
			Double porcentajeRecargoPagoFraccionadoUsuario) {
		this.porcentajeRecargoPagoFraccionadoUsuario = porcentajeRecargoPagoFraccionadoUsuario;
	}
	
	@Column(name = "VALORRECARGOPAGOFRACCUSUARIO", nullable = true, precision = 24, scale = 10)
	public Double getValorRecargoPagoFraccionadoUsuario() {
		return valorRecargoPagoFraccionadoUsuario;
	}

	public void setValorRecargoPagoFraccionadoUsuario(
			Double valorRecargoPagoFraccionadoUsuario) {
		this.valorRecargoPagoFraccionadoUsuario = valorRecargoPagoFraccionadoUsuario;
	}

	@Column(name = "CLAVEAUTORIZACIONDERECHOS", nullable = true)
	public Short getClaveAutorizacionDerechos() {
		return claveAutorizacionDerechos;
	}

	public void setClaveAutorizacionDerechos(Short claveAutorizacionDerechos) {
		this.claveAutorizacionDerechos = claveAutorizacionDerechos;
	}

	@Column(name = "CODIGOUSUARIOAUTDERECHOS", nullable = true, length = 8)
	public String getCodigoUsuarioAutorizacionDerechos() {
		return codigoUsuarioAutorizacionDerechos;
	}

	public void setCodigoUsuarioAutorizacionDerechos(
			String codigoUsuarioAutorizacionDerechos) {
		this.codigoUsuarioAutorizacionDerechos = codigoUsuarioAutorizacionDerechos;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAAUTDERECHOS", nullable = true)
	public Date getFechaAutorizacionDerechos() {
		return fechaAutorizacionDerechos;
	}

	public void setFechaAutorizacionDerechos(Date fechaAutorizacionDerechos) {
		this.fechaAutorizacionDerechos = fechaAutorizacionDerechos;
	}

	@Column(name = "CLAVEAUTORIZACIONRPF", nullable = true)
	public Short getClaveAutorizacionRecargoPagoFraccionado() {
		return claveAutorizacionRecargoPagoFraccionado;
	}

	public void setClaveAutorizacionRecargoPagoFraccionado(
			Short claveAutorizacionRecargoPagoFraccionado) {
		this.claveAutorizacionRecargoPagoFraccionado = claveAutorizacionRecargoPagoFraccionado;
	}

	@Column(name = "CODIGOUSUARIOAUTRPF", nullable = true, length = 8)
	public String getCodigoUsuarioAutorizacionRecargoPagoFraccionado() {
		return codigoUsuarioAutorizacionRecargoPagoFraccionado;
	}

	public void setCodigoUsuarioAutorizacionRecargoPagoFraccionado(
			String codigoUsuarioAutorizacionRecargoPagoFraccionado) {
		this.codigoUsuarioAutorizacionRecargoPagoFraccionado = codigoUsuarioAutorizacionRecargoPagoFraccionado;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAAUTRPF", nullable = true)
	public Date getFechaAutorizacionRecargoPagoFraccionado() {
		return fechaAutorizacionRecargoPagoFraccionado;
	}

	public void setFechaAutorizacionRecargoPagoFraccionado(
			Date fechaAutorizacionRecargoPagoFraccionado) {
		this.fechaAutorizacionRecargoPagoFraccionado = fechaAutorizacionRecargoPagoFraccionado;
	}

	@Column(name = "DESCRIPCIONLICITACION", nullable = true, length = 400)
	public String getDescripcionLicitacion() {
		return descripcionLicitacion;
	}

	public void setDescripcionLicitacion(String descripcionLicitacion) {
		this.descripcionLicitacion = descripcionLicitacion;
	}

	@Column(name = "DESCRIPCIONLICITACIONENDOSO", nullable = true, length = 1200)
	public String getDescripcionLicitacionEndoso() {
		return descripcionLicitacionEndoso;
	}

	public void setDescripcionLicitacionEndoso(String descripcionLicitacionEndoso) {
		this.descripcionLicitacionEndoso = descripcionLicitacionEndoso;
	}

	@Column(name = "CLAVEIMPRESIONSUMAASEGURADA", nullable = true)
	public Short getClaveImpresionSumaAsegurada() {
		return claveImpresionSumaAsegurada;
	}

	public void setClaveImpresionSumaAsegurada(Short claveImpresionSumaAsegurada) {
		this.claveImpresionSumaAsegurada = claveImpresionSumaAsegurada;
	}

	@Column(name = "CODIGOUSUARIOSOLAUTDIASGRACIA")
	public String getCodigoUsuarioSolicitaAutDiasGracia() {
		return codigoUsuarioSolicitaAutDiasGracia;
	}

	public void setCodigoUsuarioSolicitaAutDiasGracia(
			String codigoUsuarioSolicitaAutDiasGracia) {
		this.codigoUsuarioSolicitaAutDiasGracia = codigoUsuarioSolicitaAutDiasGracia;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHASOLAUTDIASGRACIA", length = 7)
	public Date getFechaSolicitudAutDiasGracia() {
		return fechaSolicitudAutDiasGracia;
	}

	public void setFechaSolicitudAutDiasGracia(Date fechaSolicitudAutDiasGracia) {
		this.fechaSolicitudAutDiasGracia = fechaSolicitudAutDiasGracia;
	}

	@Column(name = "CLAVEAUTDIASGRACIA")
	public Short getClaveAutorizacionDiasGracia() {
		return claveAutorizacionDiasGracia;
	}

	public void setClaveAutorizacionDiasGracia(Short claveAutorizacionDiasGracia) {
		this.claveAutorizacionDiasGracia = claveAutorizacionDiasGracia;
	}

	@Column(name = "CODIGOUSUARIOAUTDIASGRACIA")
	public String getCodigoUsuarioAutorizaDiasGracia() {
		return codigoUsuarioAutorizaDiasGracia;
	}

	public void setCodigoUsuarioAutorizaDiasGracia(
			String codigoUsuarioAutorizaDiasGracia) {
		this.codigoUsuarioAutorizaDiasGracia = codigoUsuarioAutorizaDiasGracia;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAAUTDIASGRACIA", length = 7)
	public Date getFechaAutorizaDiasGracia() {
		return fechaAutorizaDiasGracia;
	}

	public void setFechaAutorizaDiasGracia(Date fechaAutorizaDiasGracia) {
		this.fechaAutorizaDiasGracia = fechaAutorizaDiasGracia;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHALIBERACION", length = 7)
	public Date getFechaLiberacion() {
		return fechaLiberacion;
	}

	public void setFechaLiberacion(Date fechaLiberacion) {
		this.fechaLiberacion = fechaLiberacion;
	}

	@Column(name = "IDDOMICILIOCONTRATANTE")
	public BigDecimal getIdDomicilioContratante() {
		return idDomicilioContratante;
	}

	public void setIdDomicilioContratante(BigDecimal idDomicilioContratante) {
		this.idDomicilioContratante = idDomicilioContratante;
	}

	@Column(name = "IDDOMICILIOASEGURADO")
	public BigDecimal getIdDomicilioAsegurado() {
		return idDomicilioAsegurado;
	}

	public void setIdDomicilioAsegurado(BigDecimal idDomicilioAsegurado) {
		this.idDomicilioAsegurado = idDomicilioAsegurado;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOPERSONABENEFICIARIO")
	public PersonaDTO getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(PersonaDTO beneficiario) {
		this.beneficiario = beneficiario;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.idToCotizacion;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
	
	public String getNumeroCotizacion(){
		try{
			DecimalFormat f = new DecimalFormat("00000000");
			String prefix = "COT-";
			return prefix + f.format(this.getIdToCotizacion());
		}catch(Exception e){
		}
		return "";
	}
	
	@Transient
	public NegocioTipoPoliza getNegocioTipoPoliza() {
		return negocioTipoPoliza;
	}

	public void setNegocioTipoPoliza(NegocioTipoPoliza negocioTipoPoliza) {
		this.negocioTipoPoliza = negocioTipoPoliza;
	}
	
	@Column(name = "PORCENTAJEDESCUENTOGLOBAL", nullable = true, precision = 14, scale = 10)
	public Double getPorcentajeDescuentoGlobal() {
		if(porcentajeDescuentoGlobal == null) {
			porcentajeDescuentoGlobal = 0d;
		}
		return porcentajeDescuentoGlobal;
	}

	public void setPorcentajeDescuentoGlobal(Double porcentajeDescuentoGlobal) {
		if(porcentajeDescuentoGlobal == null) {
			porcentajeDescuentoGlobal = 0d;
		}
		this.porcentajeDescuentoGlobal = porcentajeDescuentoGlobal;
	}
 
	@Transient
	public Double getDescuentoVolumen() {
		if(descuentoVolumen ==null){
			descuentoVolumen = 0d;
		}
		return descuentoVolumen;
	}

	public void setDescuentoVolumen(Double descuentoVolumen) {
		this.descuentoVolumen = descuentoVolumen;
	}

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTONEGDERECHOPOLIZA", referencedColumnName="IDTONEGDERECHOPOLIZA", nullable=true)
	public NegocioDerechoPoliza getNegocioDerechoPoliza() {
		return negocioDerechoPoliza;
	}

	public void setNegocioDerechoPoliza(NegocioDerechoPoliza negocioDerechoPoliza) {
		this.negocioDerechoPoliza = negocioDerechoPoliza;
	}

	@Transient
	public String getAsegurado(){
		return this.nombreEmpresaAsegurado != null ? this.nombreEmpresaAsegurado : this.nombreAsegurado;
	}
	
	@Transient
	public String getDescripcionEstatus(){		
		String descripcion = null;
		if(this.claveEstatus != null){
			switch(this.claveEstatus){				
				case 0:
					descripcion = "PROCESO";
					break;
				case 1:
					descripcion = "COT ASIGNADA";
					break;
					
				case 8:
					descripcion = "COT RECHAZADA";
					break;
				case 19:
					descripcion = "COT CANCELADA";
					break;
				case 10:
					descripcion = "COT EN PROCESO";
					break;		
				case 11:
					descripcion = "COT ASIGNADA";
					break;
				case 12:
					descripcion = "COT TERMINADA";
					break;
				case 13:
					descripcion = "COT LIBERADA";
					break;	
				case 14:
					descripcion = "COT LISTA PARA EMITIR";
					break;		
				case 15:
					descripcion = "COT ASIGNADA PARA EMISION";
				case 16:
					descripcion = "COT EMITIDA";	
					break;	
				case 17:
					descripcion = "COT PENDIENTE AUTORIZACION";			
					break;								
				default:
					break;							
			}
		}
		return descripcion;
		
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "cotizacionDTO")
	public List<ComisionCotizacionDTO> getComisionCotizacionDTOs() {
		return comisionCotizacionDTOs;
	}
	
	public void setComisionCotizacionDTOs(
			List<ComisionCotizacionDTO> comisionCotizacionDTOs) {
		this.comisionCotizacionDTOs = comisionCotizacionDTOs;
	}

	@Column(name = "VALORPRIMATOTAL", nullable = true, precision = 16, scale = 2)
	public BigDecimal getValorPrimaTotal() {
		return valorPrimaTotal;
	}

	public void setValorPrimaTotal(BigDecimal valorPrimaTotal) {
		this.valorPrimaTotal = valorPrimaTotal;
	}

	public void setFechaSeguimiento(Date fechaSeguimiento) {
		this.fechaSeguimiento = fechaSeguimiento;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHASEGUIMIENTO", length = 7)
	public Date getFechaSeguimiento() {
		return fechaSeguimiento;
	}

	public void setConflictoNumeroSerie(Boolean conflictoNumeroSerie) {
		this.conflictoNumeroSerie = conflictoNumeroSerie;
	}

	@Column(name = "CONFLICTONUMEROSERIE", nullable = false, precision = 4, scale = 0)
	public Boolean getConflictoNumeroSerie() {
		return conflictoNumeroSerie;
	}

	public void setValorTotalPrimas(BigDecimal valorTotalPrimas) {
		this.valorTotalPrimas = valorTotalPrimas;
	}

	@Column(name = "VALORTOTALPRIMAS", nullable = true, precision = 16, scale = 2)
	public BigDecimal getValorTotalPrimas() {
		return valorTotalPrimas;
	}

	public void setFechaFinVigenciaHasta(Date fechaFinVigenciaHasta) {
		this.fechaFinVigenciaHasta = fechaFinVigenciaHasta;
	}
	
	@OneToOne(fetch = FetchType.LAZY)
	@ReadOnly
	@JoinColumn(name="IDTOCOTIZACION", referencedColumnName="numero", nullable = true, insertable=false, updatable=false)
	public CotizacionContinuity getCotizacionContinuity() {
		return cotizacionContinuity;
	}

	public void setCotizacionContinuity(CotizacionContinuity cotizacionContinuity) {
		this.cotizacionContinuity = cotizacionContinuity;
	}

	@Transient
	public Date getFechaFinVigenciaHasta() {
		return fechaFinVigenciaHasta;
	}

	@Column(name = "IDCLIENTECONDUCTOCOBRO", nullable = true,  precision = 16, scale = 2)
	public Long getIdConductoCobroCliente() {
		return idConductoCobroCliente;
	}

	public void setIdConductoCobroCliente(Long idConductoCobroCliente) {
		this.idConductoCobroCliente = idConductoCobroCliente;
	}

	@Column(name = "NUMPRIMERRECIBO", nullable = true)
	public String getNumeroPrimerRecibo() {
		return numeroPrimerRecibo;
	}

	public void setNumeroPrimerRecibo(String numeroPrimerRecibo) {
		this.numeroPrimerRecibo = numeroPrimerRecibo;
	}
		
	@Column(name = "DIGVERIFICADORRECIBO", nullable = true)
	public String getDigitoVerificadorRecibo() {
		return digitoVerificadorRecibo;
	}
	
	public void setDigitoVerificadorRecibo(String digitoVerificadorRecibo) {
		this.digitoVerificadorRecibo = digitoVerificadorRecibo;
	}

	public void setSeccionDTO(SeccionDTO seccionDTO) {
		this.seccionDTO = seccionDTO;
	}

	@Transient
	public SeccionDTO getSeccionDTO() {
		return seccionDTO;
	}
	
	@Column(name = "ESTATUSPAGORECIBO", nullable = true)
	public Boolean getPrimerReciboPagado() {
		return primerReciboPagado;
	}

	public void setPrimerReciboPagado(Boolean primerReciboPagado) {
		this.primerReciboPagado = primerReciboPagado;
	}
	
	
	@Column(name = "IGUALACIONNIVELCOTIZACION")
	public Boolean getIgualacionNivelCotizacion() {
		return igualacionNivelCotizacion;
	}

	public void setIgualacionNivelCotizacion(Boolean igualacionNivelCotizacion) {
		this.igualacionNivelCotizacion = igualacionNivelCotizacion;
	}

	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("IdToCotizacion: " + this.getIdToCotizacion() + ", ");
		sb.append("TipoPolizaDTO: " + this.getTipoPolizaDTO() + ", ");
		sb.append("DescripcionEstatus: " + this.getDescripcionEstatus() + ", ");
		sb.append("FolioPolizaAsociada: " + this.getFolioPolizaAsociada() + ", ");
		sb.append("NumeroCotizacion: " + this.getNumeroCotizacion() + ", ");
		sb.append("PrimaNetaTotal: " + this.getPrimaNetaTotal() + ", ");
		sb.append("ValorPrimaTotal: " + this.getValorPrimaTotal() + ", ");
		sb.append("SolicitudDTO: " + this.getSolicitudDTO() + ", ");
		this.getIncisoCotizacionDTOs().size();
		sb.append("IncisoCotizacionDTOs: " + this.getIncisoCotizacionDTOs());
		sb.append("]");
		
		return sb.toString();
	}

	@Column(name = "NUMEROAUTORIZACION", nullable = true)
	public String getNumeroAutorizacion() {
		return numeroAutorizacion;
	}

	public void setNumeroAutorizacion(String numeroAutorizacion) {
		this.numeroAutorizacion = numeroAutorizacion;
	}

	public void setDescripcionMoneda(String descripcionMoneda) {
		this.descripcionMoneda = descripcionMoneda;
	}

	@Transient
	public String getDescripcionMoneda() {
		return descripcionMoneda;
	}

	public void setNumeroAutoexpedible(BigDecimal numeroAutoexpedible) {
		this.numeroAutoexpedible = numeroAutoexpedible;
	}

	@Column(name = "NUMEROAUTOEXPEDIBLE", nullable = true)
	public BigDecimal getNumeroAutoexpedible() {
		return numeroAutoexpedible;
	}

	@Column(name = "IGUALACIONPRIMAANTERIOR", nullable = true)
	public Double getPrimaTotalAntesDeIgualacion() {
		return primaTotalAntesDeIgualacion;
	}

	public void setPrimaTotalAntesDeIgualacion(Double primaTotalAntesDeIgualacion) {
		this.primaTotalAntesDeIgualacion = primaTotalAntesDeIgualacion;
	}

	@Column(name = "IGUALACIONDESCUENTOGENERADO", nullable = true)
	public Double getDescuentoIgualacionPrimas() {
		return descuentoIgualacionPrimas;
	}

	public void setDescuentoIgualacionPrimas(Double descuentoIgualacionPrimas) {
		this.descuentoIgualacionPrimas = descuentoIgualacionPrimas;
	}
	
	@Column(name="IDTCCENTROEMISOR", nullable=true)
	public BigDecimal getIdCentroEmisor() {
		return idCentroEmisor;
	}

	public void setIdCentroEmisor(BigDecimal idCentroEmisor) {
		this.idCentroEmisor = idCentroEmisor;
	}
	@Transient
	public String getCentroEmisorDescripcion() {
		return centroEmisorDescripcion;
	}

	public void setCentroEmisorDescripcion(String centroEmisorDescripcion) {
		this.centroEmisorDescripcion = centroEmisorDescripcion;
	}
	@Transient
	public BigDecimal getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(BigDecimal numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	@Transient
	public String getEstatusPoliza() {
		return estatusPoliza;
	}

	public void setEstatusPoliza(String estatusPoliza) {
		this.estatusPoliza = estatusPoliza;
	}
	@Column(name = "TIPOIMPRESIONCLIENTE", nullable = false, precision = 1, scale = 0)
	public BigDecimal getTipoImpresionCliente() {
		return tipoImpresionCliente;
	}
	public void setTipoImpresionCliente(BigDecimal tipoImpresionCliente) {
		this.tipoImpresionCliente = tipoImpresionCliente;
	}

	@Column(name = "TIPOAGRUPACIONRECIBOS")
	public String getTipoAgrupacionRecibos() {
		return tipoAgrupacionRecibos;
	}
	
	public void setTipoAgrupacionRecibos(String tipoAgrupacionRecibos) {
		this.tipoAgrupacionRecibos = tipoAgrupacionRecibos;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "cotizacion")
	public List<ToProgPago> getProgramaPagos() {
		return programaPagos;
	}

	public void setProgramaPagos(List<ToProgPago> programaPagos) {
		this.programaPagos = programaPagos;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "TOCONDICIONESPCOTIZACION",  schema = "MIDAS",
			joinColumns = {@JoinColumn(name="COTIZACION_ID", referencedColumnName="IDTOCOTIZACION")},
			inverseJoinColumns = {@JoinColumn(name="CONDICIONESPECIAL_ID", referencedColumnName="ID")}
	)
	public List<CondicionEspecial> getCondicionesEspeciales() {
		return condicionesEspeciales;
	}

	public void setCondicionesEspeciales(
			List<CondicionEspecial> condicionesEspeciales) {
		this.condicionesEspeciales = condicionesEspeciales;
	}
	
	@Column(name = "FOLIO")
	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}
	
	@Column(name = "NUMEROCREDITO")
	public String getNumeroCredito() {
		return numeroCredito;
	}

	public void setNumeroCredito(String numeroCredito) {
		this.numeroCredito = numeroCredito;
	}
	
	@Column(name = "DESCUENTOORIGINACION")
	public Double getDescuentoOriginacion() {
		return descuentoOriginacion;
	}

	public void setDescuentoOriginacion(Double descuentoOriginacion) {
		this.descuentoOriginacion = descuentoOriginacion;
	}

	public void setDescuendoAplicadoCotizacion(
			String descuendoAplicadoCotizacion) {
		this.descuendoAplicadoCotizacion = descuendoAplicadoCotizacion;
	}

	@Transient
	public String getDescuendoAplicadoCotizacion() {
		return descuendoAplicadoCotizacion;
	}

	public String getTipoCotizacion() {
		return tipoCotizacion;
	}

	public void setTipoCotizacion(String tipoCotizacion) {
		this.tipoCotizacion = tipoCotizacion;
	}

	public Double getPrimaTarifa() {
		return primaTarifa;
	}

	public void setPrimaTarifa(Double primaTarifa) {
		this.primaTarifa = primaTarifa;
	}

	public void setValorComisionCedida(BigDecimal valorComisionCedida) {
		this.valorComisionCedida = valorComisionCedida;
	}

	@Column(name = "VALORCOMISIONCEDIDA")
	public BigDecimal getValorComisionCedida() {
		return valorComisionCedida;
	}

	public void setValorRecargo(BigDecimal valorRecargo) {
		this.valorRecargo = valorRecargo;
	}

	@Column(name = "VALORRECARGO")
	public BigDecimal getValorRecargo() {
		return valorRecargo;
	}

	public void setValorDerechos(BigDecimal valorDerechos) {
		this.valorDerechos = valorDerechos;
	}

	@Column(name = "VALORDERECHOS")
	public BigDecimal getValorDerechos() {
		return valorDerechos;
	}

	public void setValorIva(BigDecimal valorIva) {
		this.valorIva = valorIva;
	}

	@Column(name = "VALORIVA")
	public BigDecimal getValorIva() {
		return valorIva;
	}

	@Column(name = "ESRECUOTIFICADAAUTOM")
	public Boolean getRecuotificadaAutomatica() {
		return recuotificadaAutomatica;
	}

	public void setRecuotificadaAutomatica(Boolean recuotificadaAutomatica) {
		this.recuotificadaAutomatica = recuotificadaAutomatica;
	}

	@Column(name = "ESRECUOTIFICADAMANUAL")
	public Boolean getRecuotificadaManual() {
		return recuotificadaManual;
	}

	public void setRecuotificadaManual(Boolean recuotificadaManual) {
		this.recuotificadaManual = recuotificadaManual;
	}


	
	
	@Column(name = "NUMPOLIZA_SERVPUB")
	public String getNumPolizaServicioPublico() {
		return numPolizaServicioPublico;
	}

	public void setNumPolizaServicioPublico(String numPolizaServicioPublico) {
		this.numPolizaServicioPublico = numPolizaServicioPublico;
	}

	@Column(name = "NUMCENTROEMISOR")
	public String getNumCentroEmisor() {
		return numCentroEmisor;
	}

	public void setNumCentroEmisor(String numCentroEmisor) {
		this.numCentroEmisor = numCentroEmisor;
	}

	@Column(name = "SINCOMPENSACION")
	public Boolean getSinCompensacion() {
		return sinCompensacion;
	}

	public void setSinCompensacion(Boolean sinCompensacion) {
		this.sinCompensacion = sinCompensacion;
	}
	
	
	
	
	
}