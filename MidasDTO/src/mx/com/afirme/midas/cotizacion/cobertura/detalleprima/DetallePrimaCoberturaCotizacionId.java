package mx.com.afirme.midas.cotizacion.cobertura.detalleprima;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * DetPrimaCoberturaCotizacionDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class DetallePrimaCoberturaCotizacionId  implements java.io.Serializable {

	private static final long serialVersionUID = 3226362054126048192L;
	private BigDecimal numeroSubInciso;
     private BigDecimal idToCotizacion;
     private BigDecimal numeroInciso;
     private BigDecimal idToSeccion;
     private BigDecimal idToCobertura;


    // Constructors

    /** default constructor */
    public DetallePrimaCoberturaCotizacionId() {
    }
   
    public DetallePrimaCoberturaCotizacionId(BigDecimal numeroSubInciso,
			BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idToSeccion, BigDecimal idToCobertura) {
		super();
		this.numeroSubInciso = numeroSubInciso;
		this.idToCotizacion = idToCotizacion;
		this.numeroInciso = numeroInciso;
		this.idToSeccion = idToSeccion;
		this.idToCobertura = idToCobertura;
	}



	// Property accessors

    @Column(name="NUMEROSUBINCISO", nullable=false, precision=22, scale=0)
    public BigDecimal getNumeroSubInciso() {
        return this.numeroSubInciso;
    }
    
    public void setNumeroSubInciso(BigDecimal numeroSubInciso) {
        this.numeroSubInciso = numeroSubInciso;
    }

    @Column(name="IDTOCOTIZACION", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToCotizacion() {
        return this.idToCotizacion;
    }
    
    public void setIdToCotizacion(BigDecimal idToCotizacion) {
        this.idToCotizacion = idToCotizacion;
    }

    @Column(name="NUMEROINCISO", nullable=false, precision=22, scale=0)

    public BigDecimal getNumeroInciso() {
        return this.numeroInciso;
    }
    
    public void setNumeroInciso(BigDecimal numeroInciso) {
        this.numeroInciso = numeroInciso;
    }

    @Column(name="IDTOSECCION", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToSeccion() {
        return this.idToSeccion;
    }
    
    public void setIdToSeccion(BigDecimal idToSeccion) {
        this.idToSeccion = idToSeccion;
    }

    @Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToCobertura() {
        return this.idToCobertura;
    }
    
    public void setIdToCobertura(BigDecimal idToCobertura) {
        this.idToCobertura = idToCobertura;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof DetallePrimaCoberturaCotizacionId) ) return false;
		 DetallePrimaCoberturaCotizacionId castOther = ( DetallePrimaCoberturaCotizacionId ) other; 
         
		 return ( (this.getNumeroSubInciso()==castOther.getNumeroSubInciso()) || ( this.getNumeroSubInciso()!=null && castOther.getNumeroSubInciso()!=null && this.getNumeroSubInciso().equals(castOther.getNumeroSubInciso()) ) )
 && ( (this.getIdToCotizacion()==castOther.getIdToCotizacion()) || ( this.getIdToCotizacion()!=null && castOther.getIdToCotizacion()!=null && this.getIdToCotizacion().equals(castOther.getIdToCotizacion()) ) )
 && ( (this.getNumeroInciso()==castOther.getNumeroInciso()) || ( this.getNumeroInciso()!=null && castOther.getNumeroInciso()!=null && this.getNumeroInciso().equals(castOther.getNumeroInciso()) ) )
 && ( (this.getIdToSeccion()==castOther.getIdToSeccion()) || ( this.getIdToSeccion()!=null && castOther.getIdToSeccion()!=null && this.getIdToSeccion().equals(castOther.getIdToSeccion()) ) )
 && ( (this.getIdToCobertura()==castOther.getIdToCobertura()) || ( this.getIdToCobertura()!=null && castOther.getIdToCobertura()!=null && this.getIdToCobertura().equals(castOther.getIdToCobertura()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getNumeroSubInciso() == null ? 0 : this.getNumeroSubInciso().hashCode() );
         result = 37 * result + ( getIdToCotizacion() == null ? 0 : this.getIdToCotizacion().hashCode() );
         result = 37 * result + ( getNumeroInciso() == null ? 0 : this.getNumeroInciso().hashCode() );
         result = 37 * result + ( getIdToSeccion() == null ? 0 : this.getIdToSeccion().hashCode() );
         result = 37 * result + ( getIdToCobertura() == null ? 0 : this.getIdToCobertura().hashCode() );
         return result;
   }   





}