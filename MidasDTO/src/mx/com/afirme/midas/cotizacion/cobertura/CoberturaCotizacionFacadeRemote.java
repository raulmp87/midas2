package mx.com.afirme.midas.cotizacion.cobertura;

// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotId;
import mx.com.afirme.midas.cotizacion.riesgo.aumento.AumentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;

/**
 * Remote interface for CoberturaCotDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface CoberturaCotizacionFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved CoberturaCotDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            CoberturaCotDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(CoberturaCotizacionDTO entity);

	/**
	 * Delete a persistent CoberturaCotDTO entity.
	 * 
	 * @param entity
	 *            CoberturaCotDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(CoberturaCotizacionDTO entity);

	/**
	 * Persist a previously saved CoberturaCotDTO entity and return it or a copy
	 * of it to the sender. A copy of the CoberturaCotDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            CoberturaCotDTO entity to update
	 * @return CoberturaCotDTO the persisted CoberturaCotDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public CoberturaCotizacionDTO update(CoberturaCotizacionDTO entity);

	public CoberturaCotizacionDTO findById(CoberturaCotizacionId id);

	/**
	 * Find all CoberturaCotDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the CoberturaCotDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<CoberturaCotDTO> found by query
	 */
	public List<CoberturaCotizacionDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all CoberturaCotDTO entities.
	 * 
	 * @return List<CoberturaCotDTO> all CoberturaCotDTO entities
	 */
	public List<CoberturaCotizacionDTO> findAll();

	/**
	 * Find all CoberturaCotizacionDTO by SeccionId entities.
	 * 
	 * @return List<CoberturaCotDTO> all CoberturaCotizacionDTO entities
	 */
	public List<CoberturaCotizacionDTO> listarPorSeccionCotizacionId(
			SeccionCotizacionDTOId seccionCotizacionDTO,
			Boolean listarSoloContratadas);

	public List<CoberturaCotizacionDTO> listarPorSeccionCotizacionIdSinContratar(
			SeccionCotizacionDTOId seccionCotizacionDTO);

	/**
	 * Find all CoberturaCotizacionDTO by idCoberturaSumaAsegurada entities.
	 * 
	 * @return List<CoberturaCotDTO> all filtered CoberturaCotizacionDTO
	 *         entities
	 */
	public List<CoberturaCotizacionDTO> listarCoberturasDeSumaAsegurada(
			CoberturaCotizacionDTO coberturaCotizacionDTO);

	public List<CoberturaCotizacionDTO> listarCoberturasContratadasPorSeccion(
			BigDecimal idToCotizacion, BigDecimal idToSeccion);

	public List<RecargoRiesgoCotizacionDTO> listarRecargosRiesgoPorCoberturaCotizacion(
			CoberturaCotizacionId id, BigDecimal idRecargoEspecial,Boolean contratado);

	public List<AumentoRiesgoCotizacionDTO> listarAumentosRiesgoPorCoberturaCotizacion(CoberturaCotizacionId id, BigDecimal idAumentoEspecial, Boolean contratado);
	
	public List<DescuentoRiesgoCotizacionDTO> listarDescuentosRiesgoPorCoberturaCotizacion(
			CoberturaCotizacionId id, BigDecimal idDescuentoEspecial,
			Boolean contratado);

	public List<CoberturaCotizacionDTO> listarCoberturasContratadas(
			BigDecimal idToCotizacion);
	
	public List<CoberturaCotizacionDTO> listarCoberturasContratadas(BigDecimal idToCotizacion,BigDecimal numeroInciso);

	public List<CoberturaCotizacionDTO> listarCoberturasContratadas(
			BigDecimal idToCotizacion, boolean aplicarMerge);

	public List<CoberturaCotizacionDTO> listarCoberturasContratadasPorSeccionCobertura(
			CoberturaCotizacionId id);

	public List<CoberturaCotizacionDTO> listarCoberturasCalculadasPrimerRiesgoLUC(
			BigDecimal idToCotizacion, Short numeroAgrupacion);

	public AgrupacionCotDTO getAgrupacionCotizacion(AgrupacionCotId id);

	/**
	 * Lista los registros de CoberturaCotizacionDTO usando como filtro los
	 * atributos del objeto CoberturaCotizacionId recibido. Los atributos usados
	 * son: idToCotizacion, idToCobertura, idToSeccion y numeroInciso.
	 * 
	 * @param CoberturaCotizacionId
	 *            id
	 * @return List<CoberturaCotizacionDTO> lista de entidades encontrada
	 */
	public List<CoberturaCotizacionDTO> listarPorIdFiltrado(
			CoberturaCotizacionId id);

	/**
	 * ListarFiltrado
	 * 
	 * @param CoberturaCotizacionDTO
	 * @return List<CoberturaCotizacionDTO> lista de entidades encontrada
	 */
	public List<CoberturaCotizacionDTO> listarFiltrado(
			CoberturaCotizacionDTO coberturaCotizacion);

	public Double getSumatoriaRiesgosBasicos(CoberturaCotizacionId id);

	public Double obtenerSACoberturasBasicasIncendioPorCotizacion(
			BigDecimal idToCotizacion);

	public List<BigDecimal> listarCoberturasDistintas(
			BigDecimal idToCotizacion, boolean soloContratadas);

	/**
	 * METODO PARA OBTENER LA COBERTURA , SOLAMENTE SI ESTA CONTRATADA Y SI SU
	 * SECCION TAMBIEN LO ESTA
	 * 
	 * @param id
	 * @return
	 */
	public CoberturaCotizacionDTO obtenerCoberturaContratadaPorId(
			CoberturaCotizacionId id);

	public List<CoberturaCotizacionDTO> listarCoberturasContratadasApliquenPrimerRiesgoLUC(
			BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idToCobertura);

	public List<CoberturaCotizacionDTO> listarCoberturasAsociadasPorSeccionCotizacionId(
			SeccionCotizacionDTOId seccionCotizacionDTOId,
			BigDecimal idToCoberturaBasica);

	public Map<BigDecimal, BigDecimal> obtenerCoberturasPrimerRiesgo(
			BigDecimal idToCotizacion);

	public List<CoberturaCotizacionDTO> listarCoberturasPrimerRiesgoSeccion(
			BigDecimal idToCotizacion, BigDecimal idToSeccion);

	public List<CoberturaCotizacionDTO> listarCoberturasContratadasPorSeccionCotizacion(
			SeccionCotizacionDTOId seccionCotizacionDTOId);

	public Long obtenerTotalCoberturasContratadasPorSeccionCotizacion(
			SeccionCotizacionDTOId seccionCotizacionDTOId);

	public List<CoberturaCotizacionDTO> listarCoberturasBasicasContratadas(
			BigDecimal idToCotizacion, BigDecimal numeroInciso);

	/**
	 * METODO OBTENER LA LISTA DE SUBRAMOS INVOLUCRADOS EN EL ENDOSO. SE
	 * INCLUYEN SUBRAMOS DE COBERTURAS CONTRATADAS Y DADAS DE BAJA EN EL ENDOSO.
	 * 
	 * @param idToCotizacion
	 * @return List<SubRamoDTO>
	 */
	public List<SubRamoDTO> listarSubRamosEndoso(BigDecimal idToCotizacion);

	/**
	 * Lista las coberturas contratadas en una secci�n y cuyo id se encuentre
	 * dentro de la lista de id's enviada como par�metro.
	 * 
	 * @param seccion
	 * @param idsCoberturas
	 * @return List<CoberturaCotizacionDTO>
	 */
	public List<CoberturaCotizacionDTO> listarCoberturasContratadasEnConjunto(
			SeccionCotizacionDTOId seccion, List<BigDecimal> idsCoberturas);
	
	
	
	public List<CoberturaCotizacionDTO> listarCoberturasObligatorioParcial(BigDecimal idCotizacion, BigDecimal idSeccion, BigDecimal numeroInciso);
	
	
	/**
	 * Revisa si la cobertura de la cotizacion fue contratada o descontratada, o si cambio su suma asegurada, para recalcularla
	 * @param coberturaCotizacion
	 * @param nombreUsuario
	 * @return La cobertura recalculada
	 */
	public CoberturaCotizacionDTO actualizarCoberturaEnCotizacion(CoberturaCotizacionDTO coberturaCotizacion, String nombreUsuario);
	
	
	
	
	
	
	
	
}