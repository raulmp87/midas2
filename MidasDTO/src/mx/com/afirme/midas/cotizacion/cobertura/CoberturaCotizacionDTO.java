package mx.com.afirme.midas.cotizacion.cobertura;
// default package

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.CoberturaSeccion;
import mx.com.afirme.midas2.domain.cobranza.programapago.ToDesgloseRecibo;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducibles.NegocioDeducibleCob;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.sumaasegurada.NegocioCobSumAse;
import mx.com.afirme.midas2.util.UtileriasWeb;


/**
 * CoberturaCotDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOCOBERTURACOT"
    ,schema="MIDAS"
)
public class CoberturaCotizacionDTO  implements java.io.Serializable,Entidad {

	
    // Fields    
	private static final long serialVersionUID = 1137980858995214872L;
	 private CoberturaCotizacionId id;
	private SeccionCotizacionDTO seccionCotizacionDTO;
     private CoberturaSeccionDTO coberturaSeccionDTO;
     private BigDecimal idTcSubramo;
     private Double valorPrimaNeta;
     private Double valorSumaAsegurada;
     private Double valorCoaseguro;
     private Double valorDeducible;
     //private Short claveAutReaseguro;
     private String codigoUsuarioAutReaseguro;
     private Short claveObligatoriedad;
     private Short claveContrato;
	private Double valorCuota;
	private Double valorCuotaAnterior;
	private Double porcentajeCoaseguro;
	private Short claveAutCoaseguro;
	private String codigoUsuarioAutCoaseguro;
	private Double porcentajeDeducible;
	private Short claveAutDeducible;
	private String codigoUsuarioAutDeducible;
	private Short numeroAgrupacion;
	private Double valorCuotaOriginal;
	private Double valorPrimaNetaOriginal;
	private List<RiesgoCotizacionDTO> riesgoCotizacionLista;
	private List<DetallePrimaCoberturaCotizacionDTO> listaDetallePrimacoberturaCotizacion;
	private Short claveTipoDeducible;
	private Short claveTipoLimiteDeducible;
	private Double valorMinimoLimiteDeducible;
	private Double valorMaximoLimiteDeducible;
	private Short claveFacultativo;
	private Boolean claveCobertura;
	private Boolean claveContratoBoolean;
	private String descripcionDeducible;
	private List<NegocioDeducibleCob> deducibles;
	private Double valorSumaAseguradaMin;
	private Double valorSumaAseguradaMax;
	private Integer idToVersionAgrupadorTarifa = 0;
	private Integer idVersionCarga = 0;
	private Integer idVersionTarifa = 0;
	private Integer idTarifaExt = 0;
	private Integer idVersionTarifaExt = 0;
	private Double valorPrimaDiaria;
	private Integer diasSalarioMinimo;
	private List<ToDesgloseRecibo> desgloseRecibo;
	private Boolean consultaAsegurada;
	private List<NegocioCobSumAse> sumasAseguradas;
	
	/** default constructor */
    public CoberturaCotizacionDTO() {
    	setClaveContrato(Short.valueOf((short) 0));
    	if (id==null)
    		id = new CoberturaCotizacionId();
    }
    
    public CoberturaCotizacionDTO(CoberturaSeccion coberturaSeccion) {
    	this.id = new CoberturaCotizacionId();
    	this.claveAutCoaseguro = coberturaSeccion.getClaveAutCoaseguro();
    	this.claveAutDeducible = coberturaSeccion.getClaveAutDeducible();
    	this.claveContrato = coberturaSeccion.getClaveContrato();
    	this.claveFacultativo = coberturaSeccion.getClaveFacultativo();
    	this.claveObligatoriedad = coberturaSeccion.getClaveObligatoriedad();
    	this.claveTipoDeducible = coberturaSeccion.getClaveTipoDeducible();
    	this.claveTipoLimiteDeducible = coberturaSeccion.getClaveTipoLimiteDeducible();
    	this.coberturaSeccionDTO = coberturaSeccion.getCoberturaSeccionDTO();
    	this.codigoUsuarioAutCoaseguro = coberturaSeccion.getCodigoUsuarioAutCoaseguro();
    	this.codigoUsuarioAutDeducible = coberturaSeccion.getCodigoUsuarioAutDeducible();
    	this.codigoUsuarioAutReaseguro = coberturaSeccion.getCodigoUsuarioAutReaseguro();
    	this.deducibles = coberturaSeccion.getDeducibles();
    	this.descripcionDeducible = coberturaSeccion.getDescripcionDeducible();
    	//coberturaSeccion.getFechaAutCoaSeguro();
    	//coberturaSeccion.getFechaAutDeducible();
    	//coberturaSeccion.getFechaSolAutCoaSeguro();
    	//coberturaSeccion.getFechaSolAutDeducible();
    	this.numeroAgrupacion = coberturaSeccion.getNumeroAgrupacion();
    	this.porcentajeCoaseguro = coberturaSeccion.getPorcentajeCoaseguro();
    	this.porcentajeDeducible = coberturaSeccion.getPorcentajeDeducible();
    	this.idTcSubramo = new BigDecimal(coberturaSeccion.getSubRamoId()!=null?coberturaSeccion.getSubRamoId():0);
    	this.valorCoaseguro = coberturaSeccion.getValorCoaseguro();
    	this.valorCuota = coberturaSeccion.getValorCuota();
    	this.valorCuotaAnterior = coberturaSeccion.getValorCuotaAnterior();
    	this.valorDeducible = coberturaSeccion.getValorDeducible();
    	this.valorMaximoLimiteDeducible = coberturaSeccion.getValorMaximoLimiteDeducible();
    	this.valorMinimoLimiteDeducible = coberturaSeccion.getValorMinimoLimiteDeducible();
    	this.valorPrimaNeta = coberturaSeccion.getValorPrimaNeta();
    	this.valorSumaAsegurada = coberturaSeccion.getValorSumaAsegurada();
    	this.valorSumaAseguradaMax = coberturaSeccion.getValorSumaAseguradaMax();
    	this.valorSumaAseguradaMin = coberturaSeccion.getValorSumaAseguradaMin();
    	this.diasSalarioMinimo=coberturaSeccion.getDiasSalarioMinimo();
    	this.consultaAsegurada=coberturaSeccion.getConsultaAsegurada();
    }

    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idToCotizacion", column=@Column(name="IDTOCOTIZACION", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="numeroInciso", column=@Column(name="NUMEROINCISO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idToSeccion", column=@Column(name="IDTOSECCION", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idToCobertura", column=@Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0) ) } )
        
	public CoberturaCotizacionId getId() {
		return id;
	}

	public void setId(CoberturaCotizacionId id) {
		this.id = id;
	}
    
	//Se quita el cascade ya que esto esta causando que la persistencia genere queries innecesarios por ejemplo cuando se carga
	//una lista que contenga 10 CoberturaCotizacion se lanzan 10 queries para obtener la SeccionCotizacionDTO aun y cuando la seccion ya había
	//sido inicializada anteriormente esto genera problemas de performance. Ademas esta propiedad es readonly porque realmente las columnas forman
	//parte dell id (CoberturaCotizacionId) y si se quieren actualizar deben hacerse a traves de ese id.
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumns( { 
        @JoinColumn(name="IDTOCOTIZACION", referencedColumnName="IDTOCOTIZACION", nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="NUMEROINCISO", referencedColumnName="NUMEROINCISO", nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="IDTOSECCION", referencedColumnName="IDTOSECCION", nullable=false, insertable=false, updatable=false) } )

    public SeccionCotizacionDTO getSeccionCotizacionDTO() {
        return this.seccionCotizacionDTO;
    }
    
    public void setSeccionCotizacionDTO(SeccionCotizacionDTO seccionCotizacionDTO) {
        this.seccionCotizacionDTO = seccionCotizacionDTO;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumns( { 
        @JoinColumn(name="IDTOSECCION", referencedColumnName="IDTOSECCION", nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="IDTOCOBERTURA", referencedColumnName="IDTOCOBERTURA", nullable=false, insertable=false, updatable=false) } )

    public CoberturaSeccionDTO getCoberturaSeccionDTO() {
        return this.coberturaSeccionDTO;
    }
    
    public void setCoberturaSeccionDTO(CoberturaSeccionDTO coberturaSeccionDTO) {
        this.coberturaSeccionDTO = coberturaSeccionDTO;
    }
    
    @Column(name="IDTCSUBRAMO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdTcSubramo() {
        return this.idTcSubramo;
    }
    
    public void setIdTcSubramo(BigDecimal idTcSubramo) {
        this.idTcSubramo = idTcSubramo;
    }
    
    @Column(name="VALORPRIMANETA", nullable=false, precision=16)

    public Double getValorPrimaNeta() {
        return this.valorPrimaNeta;
    }
    
    public void setValorPrimaNeta(Double valorPrimaNeta) {
        this.valorPrimaNeta = valorPrimaNeta;
    }
    public void setValorPrimaNeta(String valorPrimaNeta){
    	NumberFormat fMonto = new DecimalFormat("$#,##0.00");
    	try {
			this.valorPrimaNeta = (Double)fMonto.parse(valorPrimaNeta);
		} catch (ParseException e) {}
    }
    @Column(name="VALORSUMAASEGURADA", nullable=false, precision=16)

    public Double getValorSumaAsegurada() {
        return this.valorSumaAsegurada;
    }
    
    public void setValorSumaAsegurada(Double valorSumaAsegurada) {
        this.valorSumaAsegurada = valorSumaAsegurada;
    }
    
	public void setValorSumaAseguradaStr(String valorSumaAsegurada) {
		this.valorSumaAsegurada = UtileriasWeb.eliminaFormatoMoneda(valorSumaAsegurada);
	}
	
    
    public void setValorSumaAsegurada(String valorSumaAsegurada) {
    	NumberFormat fMonto = new DecimalFormat("$#,##0.00");
        try {
        	if(valorSumaAsegurada.contains("$")){
    			this.valorSumaAsegurada = (Double)fMonto.parse(valorSumaAsegurada);        		
        	}else{
        		this.valorSumaAsegurada = Double.valueOf(valorSumaAsegurada);
        	}
		} catch (ParseException e) {}
    }
    
    @Column(name="VALORCOASEGURO", nullable=false, precision=16)

    public Double getValorCoaseguro() {
        return this.valorCoaseguro;
    }
    
    public void setValorCoaseguro(Double valorCoaseguro) {
        this.valorCoaseguro = valorCoaseguro;
    }
    
    @Column(name="VALORDEDUCIBLE", nullable=false, precision=16)

    public Double getValorDeducible() {
        return this.valorDeducible;
    }
    
    public void setValorDeducible(Double valorDeducible) {
        this.valorDeducible = valorDeducible;
    }
    
    public void setValorDeducible(String valorDeducible){
    	this.valorDeducible = Double.valueOf(valorDeducible);
    }

    @Column(name="CODIGOUSUARIOAUTREASEGURO", length=8)

    public String getCodigoUsuarioAutReaseguro() {
        return this.codigoUsuarioAutReaseguro;
    }
    
    public void setCodigoUsuarioAutReaseguro(String codigoUsuarioAutReaseguro) {
        this.codigoUsuarioAutReaseguro = codigoUsuarioAutReaseguro;
    }
    
    @Column(name="CLAVEOBLIGATORIEDAD", nullable=false, precision=4, scale=0)

    public Short getClaveObligatoriedad() {
        return this.claveObligatoriedad;
    }
    
    public void setClaveObligatoriedad(Short claveObligatoriedad) {
        this.claveObligatoriedad = claveObligatoriedad;
    }
    
    @Column(name="CLAVECONTRATO", precision=4, scale=0)

    public Short getClaveContrato() {
        return this.claveContrato;
    }
    
    public void setClaveContrato(Short claveContrato) {
        this.claveContrato = claveContrato;
		if(this.claveContrato == 1){
			this.claveContratoBoolean = true;
		}else{
			this.claveContratoBoolean = false;
		}
    }
    
    @Column(name="VALORCUOTA", nullable=false, precision=16, scale=0)
    public Double getValorCuota() {
		return valorCuota;
	}

	public void setValorCuota(Double valorCuota) {
		this.valorCuota = valorCuota;
	}

	@Transient
    public Double getValorCuotaAnterior() {
		return valorCuotaAnterior;
	}

	public void setValorCuotaAnterior(Double valorCuotaAnterior) {
		this.valorCuotaAnterior = valorCuotaAnterior;
	}	
	@Column(name="PORCENTAJECOASEGURO", nullable=false)
	public Double getPorcentajeCoaseguro() {
		return porcentajeCoaseguro;
	}

	public void setPorcentajeCoaseguro(Double porcentajeCoaseguro) {
		this.porcentajeCoaseguro = porcentajeCoaseguro;
	}

	@Column(name="CLAVEAUTCOASEGURO", nullable=false, precision=4, scale=0)
	public Short getClaveAutCoaseguro() {
		return claveAutCoaseguro;
	}

	public void setClaveAutCoaseguro(Short claveAutCoaseguro) {
		this.claveAutCoaseguro = claveAutCoaseguro;
	}

	@Column(name="CODIGOUSUARIOAUTCOASEGURO", nullable=false, precision=8, scale=0)
	public String getCodigoUsuarioAutCoaseguro() {
		return codigoUsuarioAutCoaseguro;
	}

	public void setCodigoUsuarioAutCoaseguro(String codigoUsuarioAutCoaseguro) {
		this.codigoUsuarioAutCoaseguro = codigoUsuarioAutCoaseguro;
	}

	@Column(name="PORCENTAJEDEDUCIBLE", nullable=false)
	public Double getPorcentajeDeducible() {
		return porcentajeDeducible;
	}

	public void setPorcentajeDeducible(Double porcentajeDeducible) {
		this.porcentajeDeducible = porcentajeDeducible;
	}

	@Column(name="CLAVEAUTDEDUCIBLE", nullable=false, precision=4, scale=0)
	public Short getClaveAutDeducible() {
		return claveAutDeducible;
	}

	public void setClaveAutDeducible(Short claveAutDeducible) {
		this.claveAutDeducible = claveAutDeducible;
	}

	@Column(name="CODIGOUSUARIOAUTDEDUCIBLE", nullable=false, precision=8, scale=0)
	public String getCodigoUsuarioAutDeducible() {
		return codigoUsuarioAutDeducible;
	}

	public void setCodigoUsuarioAutDeducible(String codigoUsuarioAutDeducible) {
		this.codigoUsuarioAutDeducible = codigoUsuarioAutDeducible;
	}
	
	@Column(name = "NUMEROAGRUPACION", nullable = false, precision = 4, scale = 0)
	public Short getNumeroAgrupacion() {
		return this.numeroAgrupacion;
	}

	public void setNumeroAgrupacion(Short numeroAgrupacion) {
		this.numeroAgrupacion = numeroAgrupacion;
	}

	@Transient
	public Double getValorCuotaOriginal() {
		return valorCuotaOriginal;
	}

	public void setValorCuotaOriginal(Double valorCuotaOriginal) {
		this.valorCuotaOriginal = valorCuotaOriginal;
	}

	@Transient
	public Double getValorPrimaNetaOriginal() {
		return valorPrimaNetaOriginal;
	}	

	public void setValorPrimaNetaOriginal(Double valorPrimaNetaOriginal) {
		this.valorPrimaNetaOriginal = valorPrimaNetaOriginal;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "coberturaCotizacionDTO")
	public List<RiesgoCotizacionDTO> getRiesgoCotizacionLista() {
		return riesgoCotizacionLista;
	}
	public void setRiesgoCotizacionLista(
			List<RiesgoCotizacionDTO> riesgoCotizacionLista) {
		this.riesgoCotizacionLista = riesgoCotizacionLista;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "coberturaCotizacionDTO")
	public List<DetallePrimaCoberturaCotizacionDTO> getListaDetallePrimacoberturaCotizacion() {
		return listaDetallePrimacoberturaCotizacion;
	}
	public void setListaDetallePrimacoberturaCotizacion(
			List<DetallePrimaCoberturaCotizacionDTO> listaDetallePrimacoberturaCotizacion) {
		this.listaDetallePrimacoberturaCotizacion = listaDetallePrimacoberturaCotizacion;
	}

	@Column(name="CLAVETIPODEDUCIBLE")
	public Short getClaveTipoDeducible() {
		return claveTipoDeducible;
	}

	public void setClaveTipoDeducible(Short claveTipoDeducible) {
		this.claveTipoDeducible = claveTipoDeducible;
	}

	@Column(name="CLAVETIPOLIMITEDEDUCIBLE")
	public Short getClaveTipoLimiteDeducible() {
		return claveTipoLimiteDeducible;
	}

	public void setClaveTipoLimiteDeducible(Short claveTipoLimiteDeducible) {
		this.claveTipoLimiteDeducible = claveTipoLimiteDeducible;
	}

	@Column(name="VALORMINIMOLIMITEDEDUCIBLE")
	public Double getValorMinimoLimiteDeducible() {
		return valorMinimoLimiteDeducible;
	}

	public void setValorMinimoLimiteDeducible(Double valorMinimoLimiteDeducible) {
		this.valorMinimoLimiteDeducible = valorMinimoLimiteDeducible;
	}

	@Column(name="VALORMAXIMOLIMITEDEDUCIBLE")
	public Double getValorMaximoLimiteDeducible() {
		return valorMaximoLimiteDeducible;
	}

	public void setValorMaximoLimiteDeducible(Double valorMaximoLimiteDeducible) {
		this.valorMaximoLimiteDeducible = valorMaximoLimiteDeducible;
	}

	@Column(name = "CLAVEFACULTATIVO", nullable = false, precision = 4, scale = 0)
	public Short getClaveFacultativo() {
		return claveFacultativo;
	}

	public void setClaveFacultativo(Short claveFacultativo) {
		this.claveFacultativo = claveFacultativo;
	}

	@Transient
	public Boolean getClaveCobertura() {
		return claveCobertura;
	}

	public void setClaveCobertura(Boolean claveCobertura) {
		this.claveCobertura = claveCobertura;
	}
	
	
	@Transient
	public Boolean getClaveContratoBoolean() {
		return claveContratoBoolean;
	}

	public void setClaveContratoBoolean(Boolean claveContratoBoolean) {
		this.claveContratoBoolean = claveContratoBoolean;
		if(this.claveContratoBoolean){
			this.claveContrato = 1;
		}else{
			this.claveContrato = 0;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CoberturaCotizacionDTO other = (CoberturaCotizacionDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CoberturaCotizacionId getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		return this.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial();
	}

	@SuppressWarnings("unchecked")
	@Override
	public CoberturaCotizacionId getBusinessKey() {
		return this.id;
	}

	@Transient
	public String getDescripcionDeducible() {
		return descripcionDeducible;
	}

	public void setDescripcionDeducible(String descripcionDeducible) {
		this.descripcionDeducible = descripcionDeducible;
	}
	
	@Transient
	public List<NegocioDeducibleCob> getDeducibles() {
		return deducibles;
	}

	public void setDeducibles(List<NegocioDeducibleCob> deducibles) {
		this.deducibles = deducibles;
	}
	
	@Transient
	public Double getValorSumaAseguradaMin() {
		return valorSumaAseguradaMin;
	}

	public void setValorSumaAseguradaMin(Double valorSumaAseguradaMin) {
		this.valorSumaAseguradaMin = valorSumaAseguradaMin;
	}

	@Transient
	public Double getValorSumaAseguradaMax() {
		return valorSumaAseguradaMax;
	}

	public void setValorSumaAseguradaMax(Double valorSumaAseguradaMax) {
		this.valorSumaAseguradaMax = valorSumaAseguradaMax;
	}
	@Column(name="IDTOAGRUPADORTARIFA", nullable = false)
	public Integer getIdToVersionAgrupadorTarifa() {
		return idToVersionAgrupadorTarifa;
	}
	
	public void setIdToVersionAgrupadorTarifa(Integer idVersionAgrupadorTarifa) {
		this.idToVersionAgrupadorTarifa = idVersionAgrupadorTarifa;
	}
	
	@Column(name="IDVERSIONCARGA", nullable = false)
	public Integer getIdVersionCarga() {
		return idVersionCarga;
	}

	public void setIdVersionCarga(Integer idVersionCarga) {
		this.idVersionCarga = idVersionCarga;
	}
	@Column(name="IDVERAGRUPADORTARIFA", nullable = false)
	public Integer getIdVersionTarifa() {
		return idVersionTarifa;
	}

	public void setIdVersionTarifa(Integer idVersionTarifa) {
		this.idVersionTarifa = idVersionTarifa;
	}
	@Column(name="IDTARIFAEXT", nullable = false)
	public Integer getIdTarifaExt() {
		return idTarifaExt;
	}

	public void setIdTarifaExt(Integer idTarifaExt) {
		this.idTarifaExt = idTarifaExt;
	}
	@Column(name="IDVERSIONTARIFAEXT", nullable = false)
	public Integer getIdVersionTarifaExt() {
		return idVersionTarifaExt;
	}

	public void setIdVersionTarifaExt(Integer idVersionTarifaExt) {
		this.idVersionTarifaExt = idVersionTarifaExt;
	}
	
	@Column(name="VALORPRIMADIARIA", nullable = false)
	public Double getValorPrimaDiaria() {
		return valorPrimaDiaria;
	}
	
	public void setValorPrimaDiaria(Double valorPrimaDiaria) {
		this.valorPrimaDiaria = valorPrimaDiaria;
	}
	
	@Column(name="DIASSALARIOMINIMO")
	public Integer getDiasSalarioMinimo() {
		return diasSalarioMinimo;
	}
	
	public void setDiasSalarioMinimo(Integer diasSalarioMinimo) {
		this.diasSalarioMinimo = diasSalarioMinimo;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="cobertura")
	public List<ToDesgloseRecibo> getDesgloseRecibo() {
		return desgloseRecibo;
	}

	public void setDesgloseRecibo(List<ToDesgloseRecibo> desgloseRecibo) {
		this.desgloseRecibo = desgloseRecibo;
	}

	@Column(name="CONSULTAASEGURADA")
	public Boolean getConsultaAsegurada() {
		return consultaAsegurada;
	}

	public void setConsultaAsegurada(Boolean consultaAsegurada) {
		this.consultaAsegurada = consultaAsegurada;
	}

	@Transient
	public List<NegocioCobSumAse> getSumasAseguradas() {
		return sumasAseguradas;
	}

	public void setSumasAseguradas(List<NegocioCobSumAse> sumasAseguradas) {
		this.sumasAseguradas = sumasAseguradas;
	}
	
}