package mx.com.afirme.midas.cotizacion.cobertura.detalleprima;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;

/**
 * Remote interface for DetPrimaCoberturaCotizacionDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface DetallePrimaCoberturaCotizacionFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved DetPrimaCoberturaCotizacionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DetPrimaCoberturaCotizacionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DetallePrimaCoberturaCotizacionDTO entity);
    /**
	 Delete a persistent DetPrimaCoberturaCotizacionDTO entity.
	  @param entity DetPrimaCoberturaCotizacionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DetallePrimaCoberturaCotizacionDTO entity);
   /**
	 Persist a previously saved DetPrimaCoberturaCotizacionDTO entity and return it or a copy of it to the sender. 
	 A copy of the DetPrimaCoberturaCotizacionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DetPrimaCoberturaCotizacionDTO entity to update
	 @return DetPrimaCoberturaCotizacionDTO the persisted DetPrimaCoberturaCotizacionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public DetallePrimaCoberturaCotizacionDTO update(DetallePrimaCoberturaCotizacionDTO entity);
	public DetallePrimaCoberturaCotizacionDTO findById( DetallePrimaCoberturaCotizacionId id);
	 /**
	 * Find all DetPrimaCoberturaCotizacionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DetPrimaCoberturaCotizacionDTO property to query
	  @param value the property value to match
	  	  @return List<DetPrimaCoberturaCotizacionDTO> found by query
	 */
	public List<DetallePrimaCoberturaCotizacionDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all DetPrimaCoberturaCotizacionDTO entities.
	  	  @return List<DetPrimaCoberturaCotizacionDTO> all DetPrimaCoberturaCotizacionDTO entities
	 */
	public List<DetallePrimaCoberturaCotizacionDTO> findAll(
		);
	
	/**
	 * 	findByCoberturaCotizacion. Encuentra la lista de entidades DetallePrimaCoberturaCotizacionDTO que tengan los atributos 
	 * recibidos en el objeto CoberturaCotizacionId. Los atributos usados para la b�squeda son los siguientes: idToCotizacion, 
	 * numeroInciso, idToSeccion, idToCobertura.
	 * @param CoberturaCotizacionId
	 * @return List<DetallePrimaCoberturaCotizacionDTO>
	 */
	public List<DetallePrimaCoberturaCotizacionDTO> findByCoberturaCotizacion(CoberturaCotizacionId coberturaCotId);
	
	/**
	 * 	findBySubIncisoCotizacion. Encuentra la lista de entidades DetallePrimaCoberturaCotizacionDTO que tengan los atributos 
	 * recibidos en el objeto DetallePrimaCoberturaCotizacionId. Los atributos usados para la b�squeda son los siguientes: idToCotizacion, 
	 * numeroInciso, idToSeccion, numeroSubInciso.
	 * @param DetallePrimaCoberturaCotizacionId
	 * @return List<DetallePrimaCoberturaCotizacionDTO>
	 */
	
	public List<DetallePrimaCoberturaCotizacionDTO> findBySubIncisoCotizacion(DetallePrimaCoberturaCotizacionId detallePrimaCoberturaCotizacionId);


	public Double sumPrimaNetaARDTCoberturaCotizacion(CoberturaCotizacionId coberturaCotId);

	public Double sumPrimaNetaCoberturaCotizacion(CoberturaCotizacionId coberturaCotId);

	public void deleteDetallePrimaCoberturaCotizacion(CoberturaCotizacionId coberturaCotId);
	public void borrarFiltrado(DetallePrimaCoberturaCotizacionId detallePrimaCoberturaCotizacionId);
	
	public List<DetallePrimaCoberturaCotizacionDTO> listarPorCotizacion(BigDecimal idToCotizacion);
}