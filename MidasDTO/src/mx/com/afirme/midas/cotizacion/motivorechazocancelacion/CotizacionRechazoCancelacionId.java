package mx.com.afirme.midas.cotizacion.motivorechazocancelacion;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * CotizacionRechazoCancelacionId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class CotizacionRechazoCancelacionId  implements java.io.Serializable {

	private static final long serialVersionUID = 2580159250825214064L;
	private BigDecimal idToCotizacion;
     private BigDecimal idTcRechazoCancelacion;


    // Constructors

    /** default constructor */
    public CotizacionRechazoCancelacionId() {
    }

    
    /** full constructor */
    public CotizacionRechazoCancelacionId(BigDecimal idToCotizacion, BigDecimal idTcRechazoCancelacion) {
        this.idToCotizacion = idToCotizacion;
        this.idTcRechazoCancelacion = idTcRechazoCancelacion;
    }

   
    // Property accessors

    @Column(name="IDTOCOTIZACION", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToCotizacion() {
        return this.idToCotizacion;
    }
    
    public void setIdToCotizacion(BigDecimal idToCotizacion) {
        this.idToCotizacion = idToCotizacion;
    }

    @Column(name="IDTCRECHAZOCANCELACION", nullable=false, precision=22, scale=0)

    public BigDecimal getIdTcRechazoCancelacion() {
        return this.idTcRechazoCancelacion;
    }
    
    public void setIdTcRechazoCancelacion(BigDecimal idTcRechazoCancelacion) {
        this.idTcRechazoCancelacion = idTcRechazoCancelacion;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof CotizacionRechazoCancelacionId) ) return false;
		 CotizacionRechazoCancelacionId castOther = ( CotizacionRechazoCancelacionId ) other; 
         
		 return ( (this.getIdToCotizacion()==castOther.getIdToCotizacion()) || ( this.getIdToCotizacion()!=null && castOther.getIdToCotizacion()!=null && this.getIdToCotizacion().equals(castOther.getIdToCotizacion()) ) )
 && ( (this.getIdTcRechazoCancelacion()==castOther.getIdTcRechazoCancelacion()) || ( this.getIdTcRechazoCancelacion()!=null && castOther.getIdTcRechazoCancelacion()!=null && this.getIdTcRechazoCancelacion().equals(castOther.getIdTcRechazoCancelacion()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdToCotizacion() == null ? 0 : this.getIdToCotizacion().hashCode() );
         result = 37 * result + ( getIdTcRechazoCancelacion() == null ? 0 : this.getIdTcRechazoCancelacion().hashCode() );
         return result;
   }   





}