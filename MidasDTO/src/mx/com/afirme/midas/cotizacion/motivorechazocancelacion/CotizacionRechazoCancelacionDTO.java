package mx.com.afirme.midas.cotizacion.motivorechazocancelacion;
// default package

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.rechazocancelacion.RechazoCancelacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;


/**
 * CotizacionRechazoCancelacion entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TRCOTIZACIONRECHAZOCANCELACION"
    ,schema="MIDAS"
)
public class CotizacionRechazoCancelacionDTO  implements java.io.Serializable {


	private static final long serialVersionUID = 3244293642713656974L;
     private CotizacionRechazoCancelacionId id;
     private CotizacionDTO cotizacionDTO;
     private RechazoCancelacionDTO rechazoCancelacionDTO;


    // Constructors

    /** default constructor */
    public CotizacionRechazoCancelacionDTO() {
    }
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idToCotizacion", column=@Column(name="IDTOCOTIZACION", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idTcRechazoCancelacion", column=@Column(name="IDTCRECHAZOCANCELACION", nullable=false, precision=22, scale=0) ) } )
    public CotizacionRechazoCancelacionId getId() {
        return this.id;
    }
    
    public void setId(CotizacionRechazoCancelacionId id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTOCOTIZACION", nullable=false, insertable=false, updatable=false)
    public CotizacionDTO getCotizacionDTO() {
		return cotizacionDTO;
	}
	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTCRECHAZOCANCELACION", nullable=false, insertable=false, updatable=false)
	public RechazoCancelacionDTO getRechazoCancelacionDTO() {
		return rechazoCancelacionDTO;
	}
	public void setRechazoCancelacionDTO(RechazoCancelacionDTO rechazoCancelacionDTO) {
		this.rechazoCancelacionDTO = rechazoCancelacionDTO;
	}

}