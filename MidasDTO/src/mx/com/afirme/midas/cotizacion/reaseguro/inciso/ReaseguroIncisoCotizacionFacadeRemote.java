package mx.com.afirme.midas.cotizacion.reaseguro.inciso;
// default package

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for ReaseguroIncisoCotizacionDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ReaseguroIncisoCotizacionFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ReaseguroIncisoCotizacionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ReaseguroIncisoCotizacionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ReaseguroIncisoCotizacionDTO entity);
    /**
	 Delete a persistent ReaseguroIncisoCotizacionDTO entity.
	  @param entity ReaseguroIncisoCotizacionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ReaseguroIncisoCotizacionDTO entity);
   /**
	 Persist a previously saved ReaseguroIncisoCotizacionDTO entity and return it or a copy of it to the sender. 
	 A copy of the ReaseguroIncisoCotizacionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ReaseguroIncisoCotizacionDTO entity to update
	 @return ReaseguroIncisoCotizacionDTO the persisted ReaseguroIncisoCotizacionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ReaseguroIncisoCotizacionDTO update(ReaseguroIncisoCotizacionDTO entity);
	public ReaseguroIncisoCotizacionDTO findById( ReaseguroIncisoCotizacionId id);
	 /**
	 * Find all ReaseguroIncisoCotizacionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ReaseguroIncisoCotizacionDTO property to query
	  @param value the property value to match
	  	  @return List<ReaseguroIncisoCotizacionDTO> found by query
	 */
	public List<ReaseguroIncisoCotizacionDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ReaseguroIncisoCotizacionDTO entities.
	  	  @return List<ReaseguroIncisoCotizacionDTO> all ReaseguroIncisoCotizacionDTO entities
	 */
	public List<ReaseguroIncisoCotizacionDTO> findAll(
		);
	
	/**
	 * Find all ReaseguroIncisoCotizacionDTO entities with depends of a filter. 
	 * @param entity the name of the entity ReaseguroIncisoCotizacionDTO
	 * @return ReaseguroIncisoCotizacionDTO found by query
	 */
	public List<ReaseguroIncisoCotizacionDTO> listarFiltrado(ReaseguroIncisoCotizacionDTO entity);
}