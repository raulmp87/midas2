package mx.com.afirme.midas.cotizacion.reaseguro;
// default package

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for ReaseguroCotizacionDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ReaseguroCotizacionFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ReaseguroCotizacionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ReaseguroCotizacionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ReaseguroCotizacionDTO entity);
    /**
	 Delete a persistent ReaseguroCotizacionDTO entity.
	  @param entity ReaseguroCotizacionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ReaseguroCotizacionDTO entity);
   /**
	 Persist a previously saved ReaseguroCotizacionDTO entity and return it or a copy of it to the sender. 
	 A copy of the ReaseguroCotizacionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ReaseguroCotizacionDTO entity to update
	 @return ReaseguroCotizacionDTO the persisted ReaseguroCotizacionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ReaseguroCotizacionDTO update(ReaseguroCotizacionDTO entity);
	public ReaseguroCotizacionDTO findById( ReaseguroCotizacionId id);
	 /**
	 * Find all ReaseguroCotizacionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ReaseguroCotizacionDTO property to query
	  @param value the property value to match
	  	  @return List<ReaseguroCotizacionDTO> found by query
	 */
	public List<ReaseguroCotizacionDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ReaseguroCotizacionDTO entities.
	  	  @return List<ReaseguroCotizacionDTO> all ReaseguroCotizacionDTO entities
	 */
	public List<ReaseguroCotizacionDTO> findAll(
		);
	
	/**
	 * Find all ReaseguroCotizacionDTO entities with depends of a filter. 
	 * @param entity the name of the entity ReaseguroCotizacionDTO
	 * @return ReaseguroCotizacionDTO found by query
	 */
	public List<ReaseguroCotizacionDTO> listarFiltrado(ReaseguroCotizacionDTO entity);
}