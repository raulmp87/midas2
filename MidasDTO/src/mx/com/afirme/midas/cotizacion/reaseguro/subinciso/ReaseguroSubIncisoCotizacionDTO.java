package mx.com.afirme.midas.cotizacion.reaseguro.subinciso;
// default package

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.tiponegocio.TipoNegocioDTO;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTO;


/**
 * ReaseguroSubIncisoCotizacionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOREASEGUROSUBINCISOCOT"
    ,schema="MIDAS"
)

public class ReaseguroSubIncisoCotizacionDTO  implements java.io.Serializable {


    /**
	 * 
	 */
	private static final long serialVersionUID = -3741436020220011958L;
	// Fields    

     private ReaseguroSubIncisoCotizacionId id;
     private SubRamoDTO subRamoDTO;
     private SubIncisoCotizacionDTO subIncisoCotizacionDTO ;
     private Double valorSumaAsegurada;
     private Double valorPrimaNeta;
     private Short claveTipoOrigen;
     private Short claveEstatus;
	private TipoNegocioDTO tipoNegocioDTO;


    // Constructors

    /** default constructor */
    public ReaseguroSubIncisoCotizacionDTO() {
    }

    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idToCotizacion", column=@Column(name="IDTOCOTIZACION", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="numeroInciso", column=@Column(name="NUMEROINCISO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idToSeccion", column=@Column(name="IDTOSECCION", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="numeroSubinciso", column=@Column(name="NUMEROSUBINCISO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idTcSubramo", column=@Column(name="IDTCSUBRAMO", nullable=false, precision=22, scale=0) ) } )

    public ReaseguroSubIncisoCotizacionId getId() {
        return this.id;
    }
    
    public void setId(ReaseguroSubIncisoCotizacionId id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="IDTCSUBRAMO", nullable=false, insertable=false, updatable=false)

    public SubRamoDTO getSubRamoDTO() {
		return subRamoDTO;
	}

	public void setSubRamoDTO(SubRamoDTO subRamoDTO) {
		this.subRamoDTO = subRamoDTO;
	}
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumns( { 
        @JoinColumn(name="IDTOCOTIZACION", referencedColumnName="IDTOCOTIZACION", nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="NUMEROINCISO", referencedColumnName="NUMEROINCISO", nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="IDTOSECCION", referencedColumnName="IDTOSECCION", nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="NUMEROSUBINCISO", referencedColumnName="NUMEROSUBINCISO", nullable=false, insertable=false, updatable=false) } )

    public SubIncisoCotizacionDTO getSubIncisoCotizacionDTO() {
		return subIncisoCotizacionDTO;
	}

	public void setSubIncisoCotizacionDTO(
			SubIncisoCotizacionDTO subIncisoCotizacionDTO) {
		this.subIncisoCotizacionDTO = subIncisoCotizacionDTO;
	}
    
    @Column(name="VALORSUMAASEGURADA", nullable=false, precision=16)

    public Double getValorSumaAsegurada() {
        return this.valorSumaAsegurada;
    }
    
    public void setValorSumaAsegurada(Double valorSumaAsegurada) {
        this.valorSumaAsegurada = valorSumaAsegurada;
    }
    
    @Column(name="VALORPRIMANETA", nullable=false, precision=16)

    public Double getValorPrimaNeta() {
        return this.valorPrimaNeta;
    }
    
    public void setValorPrimaNeta(Double valorPrimaNeta) {
        this.valorPrimaNeta = valorPrimaNeta;
    }
    
    @Column(name="CLAVETIPOORIGEN", nullable=false, precision=4, scale=0)

    public Short getClaveTipoOrigen() {
        return this.claveTipoOrigen;
    }
    
    public void setClaveTipoOrigen(Short claveTipoOrigen) {
        this.claveTipoOrigen = claveTipoOrigen;
    }
    
    //@Column(name="CLAVEESTATUS", nullable=false, precision=4, scale=0)
    @Transient
    public Short getClaveEstatus() {
        return this.claveEstatus;
    }
    
    public void setClaveEstatus(Short claveEstatus) {
        this.claveEstatus = claveEstatus;
    }

    @ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCTIPONEGOCIO")
	public TipoNegocioDTO getTipoNegocioDTO() {
		return tipoNegocioDTO;
	}

	public void setTipoNegocioDTO(TipoNegocioDTO tipoNegocioDTO) {
		this.tipoNegocioDTO = tipoNegocioDTO;
	}
}