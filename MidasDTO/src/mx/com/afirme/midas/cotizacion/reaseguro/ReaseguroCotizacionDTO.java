package mx.com.afirme.midas.cotizacion.reaseguro;
// default package

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.tiponegocio.TipoNegocioDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;


/**
 * ReaseguroCotizacionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOREASEGUROCOT"
    ,schema="MIDAS"
)

public class ReaseguroCotizacionDTO  implements java.io.Serializable {


    /**
	 * 
	 */
	private static final long serialVersionUID = -8661992149373913710L;
	// Fields    

     private ReaseguroCotizacionId id;
     private CotizacionDTO cotizacionDTO;
     private SubRamoDTO subRamoDTO;
     private Double ValorSumaAsegurada;
     private Double ValorPrimaNeta;
     private Short claveTipoOrigen;
     private Short claveEstatus;
	private TipoNegocioDTO tipoNegocioDTO;


    // Constructors

    /** default constructor */
    public ReaseguroCotizacionDTO() {
    }

    // Property accessors
    @EmbeddedId
    @AttributeOverrides( {
        @AttributeOverride(name="idToCotizacion", column=@Column(name="IDTOCOTIZACION", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idTcSubRamo", column=@Column(name="IDTCSUBRAMO", nullable=false, precision=22, scale=0) ) } )
    public ReaseguroCotizacionId getId() {
        return this.id;
    }
    
    public void setId(ReaseguroCotizacionId id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="IDTOCOTIZACION", nullable=false, insertable=false, updatable=false)

    public CotizacionDTO getCotizacionDTO() {
        return this.cotizacionDTO;
    }
    
    public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
        this.cotizacionDTO = cotizacionDTO;
    }
	
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="IDTCSUBRAMO", nullable=false, insertable=false, updatable=false)

    public SubRamoDTO getSubRamoDTO() {
		return subRamoDTO;
	}

	public void setSubRamoDTO(SubRamoDTO subRamoDTO) {
		this.subRamoDTO = subRamoDTO;
	}
    
    @Column(name="VALORSUMAASEGURADA", nullable=false, precision=16)
    public Double getValorSumaAsegurada() {
        return this.ValorSumaAsegurada;
    }
    
    public void setValorSumaAsegurada(Double ValorSumaAsegurada) {
        this.ValorSumaAsegurada = ValorSumaAsegurada;
    }
    
    @Column(name="VALORPRIMANETA", nullable=false, precision=16)

    public Double getValorPrimaNeta() {
        return this.ValorPrimaNeta;
    }
    
    public void setValorPrimaNeta(Double ValorPrimaNeta) {
        this.ValorPrimaNeta = ValorPrimaNeta;
    }
    
    @Column(name="CLAVETIPOORIGEN", nullable=false, precision=4, scale=0)

    public Short getClaveTipoOrigen() {
        return this.claveTipoOrigen;
    }
    
    public void setClaveTipoOrigen(Short claveTipoOrigen) {
        this.claveTipoOrigen = claveTipoOrigen;
    }
    
    //@Column(name="CLAVEESTATUS", nullable=false, precision=4, scale=0)
    @Transient
    public Short getClaveEstatus() {
        return this.claveEstatus;
    }
    
    public void setClaveEstatus(Short claveEstatus) {
        this.claveEstatus = claveEstatus;
    }

    @ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCTIPONEGOCIO")
	public TipoNegocioDTO getTipoNegocioDTO() {
		return tipoNegocioDTO;
	}

	public void setTipoNegocioDTO(TipoNegocioDTO tipoNegocioDTO) {
		this.tipoNegocioDTO = tipoNegocioDTO;
	}
}