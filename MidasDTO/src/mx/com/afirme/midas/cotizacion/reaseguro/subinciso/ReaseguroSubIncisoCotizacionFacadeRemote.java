package mx.com.afirme.midas.cotizacion.reaseguro.subinciso;
// default package

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for ReaseguroSubIncisoCotizacionDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ReaseguroSubIncisoCotizacionFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ReaseguroSubIncisoCotizacionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ReaseguroSubIncisoCotizacionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ReaseguroSubIncisoCotizacionDTO entity);
    /**
	 Delete a persistent ReaseguroSubIncisoCotizacionDTO entity.
	  @param entity ReaseguroSubIncisoCotizacionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ReaseguroSubIncisoCotizacionDTO entity);
   /**
	 Persist a previously saved ReaseguroSubIncisoCotizacionDTO entity and return it or a copy of it to the sender. 
	 A copy of the ReaseguroSubIncisoCotizacionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ReaseguroSubIncisoCotizacionDTO entity to update
	 @return ReaseguroSubIncisoCotizacionDTO the persisted ReaseguroSubIncisoCotizacionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ReaseguroSubIncisoCotizacionDTO update(ReaseguroSubIncisoCotizacionDTO entity);
	public ReaseguroSubIncisoCotizacionDTO findById( ReaseguroSubIncisoCotizacionId id);
	 /**
	 * Find all ReaseguroSubIncisoCotizacionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ReaseguroSubIncisoCotizacionDTO property to query
	  @param value the property value to match
	  	  @return List<ReaseguroSubIncisoCotizacionDTO> found by query
	 */
	public List<ReaseguroSubIncisoCotizacionDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ReaseguroSubIncisoCotizacionDTO entities.
	  	  @return List<ReaseguroSubIncisoCotizacionDTO> all ReaseguroSubIncisoCotizacionDTO entities
	 */
	public List<ReaseguroSubIncisoCotizacionDTO> findAll(
		);
	
	/**
	 * Find all ReaseguroSubIncisoCotizacionDTO entities with depends of a filter. 
	 * @param entity the name of the entity ReaseguroSubIncisoCotizacionDTO
	 * @return ReaseguroSubIncisoCotizacionDTO found by query
	 */
	public List<ReaseguroSubIncisoCotizacionDTO> listarFiltrado(ReaseguroSubIncisoCotizacionDTO entity);
}