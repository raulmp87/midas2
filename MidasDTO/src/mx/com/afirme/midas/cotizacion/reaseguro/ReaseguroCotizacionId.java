package mx.com.afirme.midas.cotizacion.reaseguro;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * ReaseguroCotizacionDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class ReaseguroCotizacionId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1894264587032335012L;
	private BigDecimal idToCotizacion;
     private BigDecimal idTcSubRamo;


    // Constructors

    /** default constructor */
    public ReaseguroCotizacionId() {
    }

    
    /** full constructor */
    public ReaseguroCotizacionId(BigDecimal idToCotizacion, BigDecimal idTcSubRamo) {
        this.idToCotizacion = idToCotizacion;
        this.idTcSubRamo = idTcSubRamo;
    }

   
    // Property accessors

    @Column(name="IDTOCOTIZACION", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToCotizacion() {
        return this.idToCotizacion;
    }
    
    public void setIdToCotizacion(BigDecimal idToCotizacion) {
        this.idToCotizacion = idToCotizacion;
    }

    @Column(name="IDTCSUBRAMO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdTcSubRamo() {
        return this.idTcSubRamo;
    }
    
    public void setIdTcSubRamo(BigDecimal idTcSubRamo) {
        this.idTcSubRamo = idTcSubRamo;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof ReaseguroCotizacionId) ) return false;
		 ReaseguroCotizacionId castOther = ( ReaseguroCotizacionId ) other; 
         
		 return ( (this.getIdToCotizacion()==castOther.getIdToCotizacion()) || ( this.getIdToCotizacion()!=null && castOther.getIdToCotizacion()!=null && this.getIdToCotizacion().equals(castOther.getIdToCotizacion()) ) )
 && ( (this.getIdTcSubRamo()==castOther.getIdTcSubRamo()) || ( this.getIdTcSubRamo()!=null && castOther.getIdTcSubRamo()!=null && this.getIdTcSubRamo().equals(castOther.getIdTcSubRamo()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdToCotizacion() == null ? 0 : this.getIdToCotizacion().hashCode() );
         result = 37 * result + ( getIdTcSubRamo() == null ? 0 : this.getIdTcSubRamo().hashCode() );
         return result;
   }   





}