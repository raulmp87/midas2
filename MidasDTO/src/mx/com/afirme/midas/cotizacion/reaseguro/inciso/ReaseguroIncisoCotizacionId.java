package mx.com.afirme.midas.cotizacion.reaseguro.inciso;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * ReaseguroIncisoCotizacionDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class ReaseguroIncisoCotizacionId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 5052607805068840911L;
	private BigDecimal idToCotizacion;
     private BigDecimal numeroInciso;
     private BigDecimal idTcSubRamo;


    // Constructors

    /** default constructor */
    public ReaseguroIncisoCotizacionId() {
    }

    
    /** full constructor */
    public ReaseguroIncisoCotizacionId(BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idTcSubRamo) {
        this.idToCotizacion = idToCotizacion;
        this.numeroInciso = numeroInciso;
        this.idTcSubRamo = idTcSubRamo;
    }

   
    // Property accessors

    @Column(name="IDTOCOTIZACION", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToCotizacion() {
        return this.idToCotizacion;
    }
    
    public void setIdToCotizacion(BigDecimal idToCotizacion) {
        this.idToCotizacion = idToCotizacion;
    }

    @Column(name="NUMEROINCISO", nullable=false, precision=22, scale=0)

    public BigDecimal getNumeroInciso() {
        return this.numeroInciso;
    }
    
    public void setNumeroInciso(BigDecimal numeroInciso) {
        this.numeroInciso = numeroInciso;
    }

    @Column(name="IDTCSUBRAMO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdTcSubRamo() {
        return this.idTcSubRamo;
    }
    
    public void setIdTcSubRamo(BigDecimal idTcSubRamo) {
        this.idTcSubRamo = idTcSubRamo;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof ReaseguroIncisoCotizacionId) ) return false;
		 ReaseguroIncisoCotizacionId castOther = ( ReaseguroIncisoCotizacionId ) other; 
         
		 return ( (this.getIdToCotizacion()==castOther.getIdToCotizacion()) || ( this.getIdToCotizacion()!=null && castOther.getIdToCotizacion()!=null && this.getIdToCotizacion().equals(castOther.getIdToCotizacion()) ) )
 && ( (this.getNumeroInciso()==castOther.getNumeroInciso()) || ( this.getNumeroInciso()!=null && castOther.getNumeroInciso()!=null && this.getNumeroInciso().equals(castOther.getNumeroInciso()) ) )
 && ( (this.getIdTcSubRamo()==castOther.getIdTcSubRamo()) || ( this.getIdTcSubRamo()!=null && castOther.getIdTcSubRamo()!=null && this.getIdTcSubRamo().equals(castOther.getIdTcSubRamo()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdToCotizacion() == null ? 0 : this.getIdToCotizacion().hashCode() );
         result = 37 * result + ( getNumeroInciso() == null ? 0 : this.getNumeroInciso().hashCode() );
         result = 37 * result + ( getIdTcSubRamo() == null ? 0 : this.getIdTcSubRamo().hashCode() );
         return result;
   }   





}