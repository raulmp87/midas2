package mx.com.afirme.midas.cotizacion.reaseguro.subinciso;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * ReaseguroSubIncisoCotizacionDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class ReaseguroSubIncisoCotizacionId  implements java.io.Serializable {

	private static final long serialVersionUID = 5937462894190489537L;
	// Fields    

     private BigDecimal idToCotizacion;
     private BigDecimal numeroInciso;
     private BigDecimal idToSeccion;
     private BigDecimal numeroSubinciso;
     private BigDecimal idTcSubramo;


    // Constructors

    /** default constructor */
    public ReaseguroSubIncisoCotizacionId() {
    }

    
    /** full constructor */
    public ReaseguroSubIncisoCotizacionId(BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idToSeccion, BigDecimal numeroSubinciso, BigDecimal idTcSubramo) {
        this.idToCotizacion = idToCotizacion;
        this.numeroInciso = numeroInciso;
        this.idToSeccion = idToSeccion;
        this.numeroSubinciso = numeroSubinciso;
        this.idTcSubramo = idTcSubramo;
    }

   
    // Property accessors

    @Column(name="IDTOCOTIZACION", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToCotizacion() {
        return this.idToCotizacion;
    }
    
    public void setIdToCotizacion(BigDecimal idToCotizacion) {
        this.idToCotizacion = idToCotizacion;
    }

    @Column(name="NUMEROINCISO", nullable=false, precision=22, scale=0)

    public BigDecimal getNumeroInciso() {
        return this.numeroInciso;
    }
    
    public void setNumeroInciso(BigDecimal numeroInciso) {
        this.numeroInciso = numeroInciso;
    }

    @Column(name="IDTOSECCION", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToSeccion() {
        return this.idToSeccion;
    }
    
    public void setIdToSeccion(BigDecimal idToSeccion) {
        this.idToSeccion = idToSeccion;
    }

    @Column(name="NUMEROSUBINCISO", nullable=false, precision=22, scale=0)

    public BigDecimal getNumeroSubinciso() {
        return this.numeroSubinciso;
    }
    
    public void setNumeroSubinciso(BigDecimal numeroSubinciso) {
        this.numeroSubinciso = numeroSubinciso;
    }

    @Column(name="IDTCSUBRAMO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdTcSubramo() {
        return this.idTcSubramo;
    }
    
    public void setIdTcSubramo(BigDecimal idTcSubramo) {
        this.idTcSubramo = idTcSubramo;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof ReaseguroSubIncisoCotizacionId) ) return false;
		 ReaseguroSubIncisoCotizacionId castOther = ( ReaseguroSubIncisoCotizacionId ) other; 
         
		 return ( (this.getIdToCotizacion()==castOther.getIdToCotizacion()) || ( this.getIdToCotizacion()!=null && castOther.getIdToCotizacion()!=null && this.getIdToCotizacion().equals(castOther.getIdToCotizacion()) ) )
 && ( (this.getNumeroInciso()==castOther.getNumeroInciso()) || ( this.getNumeroInciso()!=null && castOther.getNumeroInciso()!=null && this.getNumeroInciso().equals(castOther.getNumeroInciso()) ) )
 && ( (this.getIdToSeccion()==castOther.getIdToSeccion()) || ( this.getIdToSeccion()!=null && castOther.getIdToSeccion()!=null && this.getIdToSeccion().equals(castOther.getIdToSeccion()) ) )
 && ( (this.getNumeroSubinciso()==castOther.getNumeroSubinciso()) || ( this.getNumeroSubinciso()!=null && castOther.getNumeroSubinciso()!=null && this.getNumeroSubinciso().equals(castOther.getNumeroSubinciso()) ) )
 && ( (this.getIdTcSubramo()==castOther.getIdTcSubramo()) || ( this.getIdTcSubramo()!=null && castOther.getIdTcSubramo()!=null && this.getIdTcSubramo().equals(castOther.getIdTcSubramo()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdToCotizacion() == null ? 0 : this.getIdToCotizacion().hashCode() );
         result = 37 * result + ( getNumeroInciso() == null ? 0 : this.getNumeroInciso().hashCode() );
         result = 37 * result + ( getIdToSeccion() == null ? 0 : this.getIdToSeccion().hashCode() );
         result = 37 * result + ( getNumeroSubinciso() == null ? 0 : this.getNumeroSubinciso().hashCode() );
         result = 37 * result + ( getIdTcSubramo() == null ? 0 : this.getIdTcSubramo().hashCode() );
         return result;
   }   





}