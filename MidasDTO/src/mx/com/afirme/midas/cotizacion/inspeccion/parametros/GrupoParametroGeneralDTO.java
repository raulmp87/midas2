package mx.com.afirme.midas.cotizacion.inspeccion.parametros;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;


/**
 * GrupoParametroGeneralDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOGRUPOPARAMETROGENERAL",schema="MIDAS")
@Cache(type=CacheType.FULL, alwaysRefresh = false, shared = true)	
public class GrupoParametroGeneralDTO  implements java.io.Serializable {

	private static final long serialVersionUID = 293154738821699448L;

     private BigDecimal idToGrupoParametroGeneral;
     private String descripcionGrupoParametroGral;
     private List<ParametroGeneralDTO> parametroGeneralList;

     public static final BigDecimal GRUPO_SP_INICIALIZA_CALCULO_RIESGO_POR_PRODUCTO = new BigDecimal("7");
     public static final BigDecimal GRUPO_SP_CALCULO_RIESGO_POR_PRODUCTO = new BigDecimal("8");
     public static final BigDecimal GRUPO_HABILITAR_PROCESOS = new BigDecimal("40");
     
     /**
      * <p>Constante que contiene la descripci&oacute;n del grupo
      * para la conexi&oacute;n al servidor <b>SFTP</b></p> 
      */
     public static final String GRUPO_SERVIDOR_SFTP = "SFTP SERVER";

    // Constructors

    /** default constructor */
    public GrupoParametroGeneralDTO() {
    	parametroGeneralList = new ArrayList<ParametroGeneralDTO>();
    }

    // Property accessors
    @Id 
    @SequenceGenerator(name = "IDTOGRUPOPARAMETROGENERAL_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOGRUPOPARAMETROGENERAL_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOGRUPOPARAMETROGENERAL_SEQ_GENERADOR")
    @Column(name="IDTOGRUPOPARAMETROGENERAL", nullable=false, precision=22, scale=0)
    public BigDecimal getIdToGrupoParametroGeneral() {
        return this.idToGrupoParametroGeneral;
    }
    
    public void setIdToGrupoParametroGeneral(BigDecimal idToGrupoParametroGeneral) {
        this.idToGrupoParametroGeneral = idToGrupoParametroGeneral;
    }
    
    @Column(name="DESCRIPCIONGRUPOPARAMETROGRAL", nullable=false, length=200)
    public String getDescripcionGrupoParametroGral() {
        return this.descripcionGrupoParametroGral;
    }
    
    public void setDescripcionGrupoParametroGral(String descripcionGrupoParametroGral) {
        this.descripcionGrupoParametroGral = descripcionGrupoParametroGral;
    }

    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="grupoParametroGeneral")
    public List<ParametroGeneralDTO> getParametroGeneralList() {
        return this.parametroGeneralList;
    }
    
    public void setParametroGeneralList(List<ParametroGeneralDTO> parametroGeneralList) {
        this.parametroGeneralList = parametroGeneralList;
    }

}