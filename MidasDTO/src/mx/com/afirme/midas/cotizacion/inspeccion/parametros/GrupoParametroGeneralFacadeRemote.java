package mx.com.afirme.midas.cotizacion.inspeccion.parametros;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for GrupoParametroGeneralDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface GrupoParametroGeneralFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved GrupoParametroGeneralDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity GrupoParametroGeneralDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(GrupoParametroGeneralDTO entity);
    /**
	 Delete a persistent GrupoParametroGeneralDTO entity.
	  @param entity GrupoParametroGeneralDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(GrupoParametroGeneralDTO entity);
   /**
	 Persist a previously saved GrupoParametroGeneralDTO entity and return it or a copy of it to the sender. 
	 A copy of the GrupoParametroGeneralDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity GrupoParametroGeneralDTO entity to update
	 @return GrupoParametroGeneralDTO the persisted GrupoParametroGeneralDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public GrupoParametroGeneralDTO update(GrupoParametroGeneralDTO entity);
	public GrupoParametroGeneralDTO findById( BigDecimal id);
	 /**
	 * Find all GrupoParametroGeneralDTO entities with a specific property value.  
	 
	  @param propertyName the name of the GrupoParametroGeneralDTO property to query
	  @param value the property value to match
	  	  @return List<GrupoParametroGeneralDTO> found by query
	 */
	public List<GrupoParametroGeneralDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all GrupoParametroGeneralDTO entities.
	  	  @return List<GrupoParametroGeneralDTO> all GrupoParametroGeneralDTO entities
	 */
	public List<GrupoParametroGeneralDTO> findAll(
		);	
}