package mx.com.afirme.midas.cotizacion.inspeccion;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for ToinspeccionincisocotFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface InspeccionIncisoCotizacionFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved Toinspeccionincisocot
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            Toinspeccionincisocot entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public InspeccionIncisoCotizacionDTO save(InspeccionIncisoCotizacionDTO entity);

	/**
	 * Delete a persistent Toinspeccionincisocot entity.
	 * 
	 * @param entity
	 *            Toinspeccionincisocot entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(InspeccionIncisoCotizacionDTO entity);

	/**
	 * Persist a previously saved Toinspeccionincisocot entity and return it or
	 * a copy of it to the sender. A copy of the Toinspeccionincisocot entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            Toinspeccionincisocot entity to update
	 * @return Toinspeccionincisocot the persisted Toinspeccionincisocot entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public InspeccionIncisoCotizacionDTO update(InspeccionIncisoCotizacionDTO entity);

	public InspeccionIncisoCotizacionDTO findById(BigDecimal id);

	/**
	 * Find all Toinspeccionincisocot entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the Toinspeccionincisocot property to query
	 * @param value
	 *            the property value to match
	 * @return List<Toinspeccionincisocot> found by query
	 */
	public List<InspeccionIncisoCotizacionDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all Toinspeccionincisocot entities.
	 * 
	 * @return List<Toinspeccionincisocot> all Toinspeccionincisocot entities
	 */
	public List<InspeccionIncisoCotizacionDTO> findAll();
	
}