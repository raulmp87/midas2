package mx.com.afirme.midas.cotizacion.inspeccion.proveedor;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.InspeccionIncisoCotizacionDTO;
import mx.com.afirme.midas.direccion.DireccionDTO;
import mx.com.afirme.midas.persona.PersonaDTO;


/**
 * ProveedorInspeccionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOPROVEEDORINSPECCION",schema="MIDAS")
public class ProveedorInspeccionDTO  extends CacheableDTO{


    // Fields    

	private static final long serialVersionUID = -8393766164258507141L;
	private BigDecimal idToProveedorInspeccion;
	private PersonaDTO personaDTO;
    private DireccionDTO direccionDTO;
    /*private String nombreProveedor;
    private String telefonoProveedor;
    private String rfcProveedor;*/
    private List<InspeccionIncisoCotizacionDTO> inspeccionIncisoCotizacionList;


    // Constructors

    /** default constructor */
    public ProveedorInspeccionDTO() {
    	inspeccionIncisoCotizacionList = new ArrayList<InspeccionIncisoCotizacionDTO>();
    }
   
    // Property accessors
    @Id
    @SequenceGenerator(name = "IDTOPROVEEDORINSPECCION_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOPROVEEDORINSPECCION_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOPROVEEDORINSPECCION_SEQ_GENERADOR")
    @Column(name="IDTOPROVEEDORINSPECCION", nullable=false, precision=22, scale=0)
    public BigDecimal getIdToProveedorInspeccion() {
        return this.idToProveedorInspeccion;
    }
    
    public void setIdToProveedorInspeccion(BigDecimal idToProveedorInspeccion) {
        this.idToProveedorInspeccion = idToProveedorInspeccion;
    }
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTODIRECCIONPROVEEDOR", nullable=false)
    public DireccionDTO getDireccionDTO() {
        return this.direccionDTO;
    }
   
    public void setDireccionDTO(DireccionDTO direccionDTO) {
        this.direccionDTO = direccionDTO;
    }
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTOPERSONAPROVEEDOR", nullable=false)
    public PersonaDTO getPersonaDTO() {
		return personaDTO;
	}

	public void setPersonaDTO(PersonaDTO personaDTO) {
		this.personaDTO = personaDTO;
	}
    
    /*@Column(name="NOMBREPROVEEDOR", nullable=false, length=200)
    public String getNombreProveedor() {
        return this.nombreProveedor;
    }
    
    public void setNombreProveedor(String nombreProveedor) {
        this.nombreProveedor = nombreProveedor;
    }
    
    @Column(name="TELEFONOPROVEEDOR", nullable=false, length=20)
    public String getTelefonoProveedor() {
        return this.telefonoProveedor;
    }
    
    public void setTelefonoProveedor(String telefonoProveedor) {
        this.telefonoProveedor = telefonoProveedor;
    }
    
    @Column(name="RFCPROVEEDOR", length=20)
    public String getRfcProveedor() {
        return this.rfcProveedor;
    }
    
    public void setRfcProveedor(String rfcProveedor) {
        this.rfcProveedor = rfcProveedor;
    }*/

	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="proveedorInspeccion")
    public List<InspeccionIncisoCotizacionDTO> getInspeccionIncisoCotizacionList() {
        return this.inspeccionIncisoCotizacionList;
    }
    
    public void setInspeccionIncisoCotizacionList(List<InspeccionIncisoCotizacionDTO> inspeccionIncisoCotizacionList) {
        this.inspeccionIncisoCotizacionList = inspeccionIncisoCotizacionList;
    }

	@Override
	public String getDescription() {
		String nombre = "NO DISPONIBLE";
		if (this.personaDTO != null){
			if (personaDTO.getClaveTipoPersona() == 1){
				if (personaDTO.getNombre() != null)
					nombre = personaDTO.getNombre();
				if (personaDTO.getApellidoPaterno() != null){
					if (!nombre.equals("NODISPONIBLE"))
						nombre += " " + personaDTO.getApellidoPaterno();
					else
						nombre = personaDTO.getApellidoPaterno();
				}
				if (personaDTO.getApellidoMaterno() != null){
					if (!nombre.equals("NODISPONIBLE"))
						nombre += " " + personaDTO.getApellidoMaterno();
					else
						nombre = personaDTO.getApellidoMaterno();
				}
			}
			else{
				if (personaDTO.getNombre() != null)
					nombre = personaDTO.getNombre();
			}
			
		}
		return nombre;
	}

	@Override
	public Object getId() {
		return this.idToProveedorInspeccion;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof ProveedorInspeccionDTO) {
			ProveedorInspeccionDTO entidad = (ProveedorInspeccionDTO) object;
			equal = entidad.getIdToProveedorInspeccion().equals(this.idToProveedorInspeccion);
		} // End of if
		return equal;
	}
   
    
}