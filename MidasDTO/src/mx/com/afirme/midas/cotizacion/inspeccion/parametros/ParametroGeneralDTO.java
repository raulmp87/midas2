package mx.com.afirme.midas.cotizacion.inspeccion.parametros;
// default package

import java.math.BigDecimal;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;


/**
 * ParametroGeneralDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOPARAMETROGENERAL",schema="MIDAS")
@Cache(type=CacheType.FULL, alwaysRefresh = false, shared = true)	
public class ParametroGeneralDTO  implements java.io.Serializable, Entidad {
	
	public static final BigDecimal GRUPO_PARAM_GENERAL_GENERALES = new BigDecimal(16);
	public static final BigDecimal GRUPO_PARAM_GENERAL_TRACKING = new BigDecimal(31);
	public static final BigDecimal GRUPO_PARAM_GENERAL_ENLACE_APNS = new BigDecimal(32);
	public static final BigDecimal GRUPO_PARAM_GENERAL_REPORTE_AGENTES = new BigDecimal(33);
	public static final BigDecimal GRUPO_PARAM_GENERAL_FORTIMAX = new BigDecimal(20);
	public static final BigDecimal GRUPO_PARAMETRO_ESTRUCTURA_FORTIMAX = new BigDecimal(25);
	public static final BigDecimal GRUPO_PARAM_GENERAL_GENERAL = new BigDecimal(13);
	public static final BigDecimal GRUPO_PARAM_GENERAL_HABILITAR_PROCESO = new BigDecimal(40);
	public static final BigDecimal GRUPO_PARAM_GENERAL_CLIENTE_SUITE = new BigDecimal(34);
	public static final BigDecimal GRUPO_PARAM_GENERAL_MOVIL_SO = new BigDecimal(84);
	public static final BigDecimal GRUPO_PARAM_GENERAL_TELEFONO_SO_SPV = new BigDecimal(85);
	public static final BigDecimal GRUPO_PARAM_COMPONENETE_PROGRAMA_PAGO = new BigDecimal(22);
	
	public static final BigDecimal CODIGO_PARAM_GENERAL_RESTRICCION_ENDOSOS_FLOTILLAS_MIGRADAS = new BigDecimal(160030);
	public static final BigDecimal CODIGO_PARAM_GENERAL_COTIZACION_PAQUETE_BASE =new BigDecimal(162010);
	public static final BigDecimal CODIGO_PARAM_GENERAL_COTIZACION_NEGOCIO_BASE =new BigDecimal(162020);
	public static final BigDecimal CODIGO_PARAM_GENERAL_COTIZACION_PRODUCTO_BASE =new BigDecimal(162030);
	public static final BigDecimal CODIGO_PARAM_GENERAL_COTIZACION_POLIZA_BASE =new BigDecimal(162040);
	public static final BigDecimal CODIGO_PARAM_GENERAL_COTIZACION_NEGOCIOSECCION_BASE =new BigDecimal(162050);
	public static final BigDecimal CODIGO_PARAM_GENERAL_COTIZACION_TIPOUSO_BASE =new BigDecimal(162060);
	public static final BigDecimal CODIGO_PARAM_GENERAL_COTIZACION_NEGOCIO_BASE_SERVICIOPUBLICO = new BigDecimal(162070);
	public static final BigDecimal CODIGO_PARAM_GENERAL_TRACKING_ID =new BigDecimal(310010);
	public static final BigDecimal CODIGO_PARAM_GENERAL_TRACKING_USER =new BigDecimal(310020);
	public static final BigDecimal CODIGO_PARAM_GENERAL_TRAKING_PASSWORD =new BigDecimal(310030);
	public static final BigDecimal CODIGO_PARAM_GENERAL_TRACKING_ENDPOINT =new BigDecimal(310040);
	public static final BigDecimal CODIGO_PARAM_GENERAL_ENLACE_APNS_PW_CERTIFICADO = new BigDecimal(320010);
	public static final BigDecimal CODIGO_PARAM_GENERAL_ENLACE_APNS_NOMBRE_CERTIFICADO = new BigDecimal(320020);
	public static final BigDecimal CODIGO_PARAM_GENERAL_ENLACE_APNS_ENVIRONMENT = new BigDecimal(320030);
	public static final BigDecimal CODIGO_PARAM_GENERAL_ENLACE_APNS_LONGITUD_REGISTRATIONID_IOS = new BigDecimal(320040);
	public static final BigDecimal CODIGO_PARAM_GENERAL_REPORTE_AGENTES_ID_ROL_AGENTE = new BigDecimal(330010);
	public static final BigDecimal CODIGO_PARAM_GENERAL_REPORTE_AGENTES_ID_ROL_PROMOTOR = new BigDecimal(330020);
	public static final BigDecimal CODIGO_PARAM_GENERAL_REPORTE_AGENTE_DETALLE_PRIMAS = new BigDecimal(330030);
	public static final BigDecimal CODIGO_PARAM_GENERAL_CENA_GAVETA = new BigDecimal(200120);
	public static final BigDecimal CODIGO_PARAM_GENERAL_CENA_GAVETA_NEGOCIO_AUTOS = new BigDecimal(25036);
	public static final BigDecimal CODIGO_PARAM_GENERAL_CENA_CARPETA = new BigDecimal(200130);
	public static final BigDecimal CODIGO_PARAM_GENERAL_KBB_ENDPOINT = new BigDecimal(131095);
	public static final BigDecimal CODIGO_PARAM_GENERAL_KBB_ACTIVA = new BigDecimal(131091);
	public static final BigDecimal CODIGO_PARAM_GENERAL_KBB_USER = new BigDecimal(131092);	
	public static final BigDecimal CODIGO_PARAM_GENERAL_HABILITA_FIX_BITEMPORAL = new BigDecimal(400010);
	public static final BigDecimal CODIGO_PARAM_GENERAL_RESTRICCION_ENDOSOS_PEND_MIGRACION_SEYCOS = new BigDecimal(160035);

	public static final BigDecimal CODIGO_PARAM_GENERAL_CLIENTE_SUITE_PW_CERTIFICADO = new BigDecimal(340010);
	public static final BigDecimal CODIGO_PARAM_GENERAL_CLIENTE_SUITE_NOMBRE_CERTIFICADO = new BigDecimal(340020);
	public static final BigDecimal CODIGO_PARAM_GENERAL_CLIENTE_SUITE_ENVIRONMENT = new BigDecimal(340030);
	public static final BigDecimal CODIGO_PARAM_GENERAL_RUTA_CERTIFICADO = new BigDecimal(340040);
	
	public static final BigDecimal CODIGO_PARAM_GENERAL_FRONTERIZO_MARCA = new BigDecimal(131093);
	public static final BigDecimal CODIGO_PARAM_GENERAL_FRONTERIZO_ESTILO = new BigDecimal(131094);
	public static final BigDecimal CODIGO_PARAM_GENERAL_FRONTERIZO_CONDICION = new BigDecimal(131096);
	
	public static final BigDecimal CODIGO_PARAM_GENERAL_CESVI_ENDPOINT_IND = new BigDecimal(131055);
	
	public static final BigDecimal CODIGO_PARAM_GENERAL_MEDIOSPAGO_SO = new BigDecimal(840010);
	public static final BigDecimal CODIGO_PARAM_GENERAL_ID_NEGOCIO_BASE = new BigDecimal(840020);
	public static final BigDecimal CODIGO_PARAM_GENERAL_ID_LINEA_NEGOCIO_BASE = new BigDecimal(840030);
	
	public static final BigDecimal CODIGO_PARAM_GENERAL_TELEFONO_SO_REPUBLICA_SPV = new BigDecimal(858010);
	public static final BigDecimal CODIGO_PARAM_GENERAL_NEGOCIOS_SO_SPV = new BigDecimal(858020);
	public static final BigDecimal CODIGO_PARAM_GENERAL_TELEFONO_SO_DF_SPV = new BigDecimal(858030);
	public static final BigDecimal CODIGO_PARAM_GENERAL_MODO_CALCULO_RECIBOS_AUTOS = new BigDecimal(22030);
	public static final BigDecimal CODIGO_PARAM_GENERAL_VALIDAR_PREVIO_RECIBOS_AUTOS = new BigDecimal(22040);
	
	public static final BigDecimal CODIGO_PARAM_GENERAL_VALIDAR_NUEVO_DESC_AUTOPLAZO = new BigDecimal(136010);
	
	public static final BigDecimal GRUPO_PARAM_GENERAL_COBERTURA_OBLIGATORIA_SERVICIO_PUBLICO = new BigDecimal(90);
	public static final BigDecimal CODIGO_PARAM_GENERAL_COBERTURA_OBLIGATORIA_SERVICIO_PUBLICO = new BigDecimal(900010);
	public static final BigDecimal CODIGO_PARAM_GENERAL_LINEA_AUTOBUSES_SERVICIO_PUBLICO = new BigDecimal(900020);
	
	public static final BigDecimal GRUPO_PARAM_GENERAL_COBERTURA_RC_USA = new BigDecimal(92);
	public static final BigDecimal CODIGO_PARAM_GENERAL_COBERTURA_RC_USA = new BigDecimal(920010);
	
	public static final BigDecimal GRUPO_CONFIGURACION_GENERAL_COBERTURAS = new BigDecimal(93);
	public static final BigDecimal CODIGO_FECHA_INICIO_UMA = new BigDecimal(930010);
	
	public static final BigDecimal GRUPO_CONFIGURACION_SINIESTRO_MOVIL = new BigDecimal(94);
	public static final BigDecimal CODIGO_CONFIGURACION_ESTADOS_SINIESTRO_MOVIL = new BigDecimal(940010);
	
	public static final BigDecimal CODIGO_PARAM_GENERAL_MERCHANT = new BigDecimal(136020);
	public static final BigDecimal CODIGO_PARAM_GENERAL_URL_BACK = new BigDecimal(136030);
	
	public static final BigDecimal CODIGO_PARAM_GENERAL_ACTIVA_DESACTIVA_CANCELACION_CFDI = new BigDecimal(162300);

	public static final String GRUPO_RECUOTIFICACION_AUTOS = "MODULO_RECUOTIFICACION_AUTOS";
	public static final String PARAMETRO_RECUOTIFICACION_AUTOS_MODULO_ACTIVO = "ENCENDIDO_MODULO";
	
	public static final BigDecimal PARAMETRO_CORREO_ORIGEN = new BigDecimal(102001);
	public static final BigDecimal PARAMETRO_FORMATO_CORREO = new BigDecimal(102002);
	
	public static final BigDecimal GRUPO_EJECUTVO_COLOCA = new BigDecimal(110);
	public static final BigDecimal CODIGO_EJECUTVO_COLOCA_CNE = new BigDecimal(970100);

	
	/**
	 * <p>Constante de la descripci&oacute;n del parametro que contiene
	 * la informaci&oacute;n de conexi&oacute;n al servidor
	 * <b>SFTP</b></p> 
	 */
	public static final String PARAMETRO_SERVIDOR_SFTP_INFO = "INFORMACION CONEXION SERVIDOR SFTP";
	
	private static final long serialVersionUID = -4080864286165168306L;
	private ParametroGeneralId id;
    private GrupoParametroGeneralDTO grupoParametroGeneral;
    private String descripcionParametroGeneral;
    private Short claveTipoValor;
    private String valor;


    // Constructor

    /** default constructor */
    public ParametroGeneralDTO() {
    }

    
    // Property accessors
    @EmbeddedId
    @AttributeOverrides( {
        @AttributeOverride(name="idToGrupoParametroGeneral", column=@Column(name="IDTOGRUPOPARAMETROGENERAL", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="codigoParametroGeneral", column=@Column(name="CODIGOPARAMETROGENERAL", nullable=false, precision=22, scale=0) ) } )
    public ParametroGeneralId getId() {
        return this.id;
    }
    
    public void setId(ParametroGeneralId id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTOGRUPOPARAMETROGENERAL", nullable=false, insertable=false, updatable=false)
    public GrupoParametroGeneralDTO getGrupoParametroGeneral() {
        return this.grupoParametroGeneral;
    }
    
    public void setGrupoParametroGeneral(GrupoParametroGeneralDTO grupoParametroGeneral) {
        this.grupoParametroGeneral = grupoParametroGeneral;
    }
    
    @Column(name="DESCRIPCIONPARAMETROGENERAL", nullable=false, length=200)
    public String getDescripcionParametroGeneral() {
        return this.descripcionParametroGeneral;
    }
    
    public void setDescripcionParametroGeneral(String descripcionParametroGeneral) {
        this.descripcionParametroGeneral = descripcionParametroGeneral;
    }
    
    @Column(name="CLAVETIPOVALOR", nullable=false, precision=4, scale=0)
    public Short getClaveTipoValor() {
        return this.claveTipoValor;
    }
    
    public void setClaveTipoValor(Short claveTipoValor) {
        this.claveTipoValor = claveTipoValor;
    }
    
    @Column(name="VALOR" )
    public String getValor() {
        return this.valor;
    }
    
    public void setValor(String valor) {
        this.valor = valor;
    }


	@SuppressWarnings("unchecked")
	@Override
	public ParametroGeneralId getKey() {
		
		return this.id;
	}


	@Override
	public String getValue() {
		
		return this.valor;
	}


	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}