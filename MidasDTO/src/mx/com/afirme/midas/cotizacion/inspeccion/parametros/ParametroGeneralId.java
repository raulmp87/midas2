package mx.com.afirme.midas.cotizacion.inspeccion.parametros;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * ParametroGeneralId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class ParametroGeneralId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 4353916411061141755L;
	private BigDecimal idToGrupoParametroGeneral;
    private BigDecimal codigoParametroGeneral;


    // Constructors

    /** default constructor */
    public ParametroGeneralId() {
    	super();
    }
    
    public ParametroGeneralId(BigDecimal idToGrupoParametroGeneral, BigDecimal codigoParametroGeneral) {
    	this();
    	this.idToGrupoParametroGeneral = idToGrupoParametroGeneral;
    	this.codigoParametroGeneral = codigoParametroGeneral;
    }
    
    // Property accessors

    @Column(name="IDTOGRUPOPARAMETROGENERAL", nullable=false, precision=22, scale=0)
    public BigDecimal getIdToGrupoParametroGeneral() {
        return this.idToGrupoParametroGeneral;
    }
    
    public void setIdToGrupoParametroGeneral(BigDecimal idToGrupoParametroGeneral) {
        this.idToGrupoParametroGeneral = idToGrupoParametroGeneral;
    }

    @Column(name="CODIGOPARAMETROGENERAL", nullable=false, precision=22, scale=0)
    public BigDecimal getCodigoParametroGeneral() {
        return this.codigoParametroGeneral;
    }
    
    public void setCodigoParametroGeneral(BigDecimal codigoParametroGeneral) {
        this.codigoParametroGeneral = codigoParametroGeneral;
    }


    public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof ParametroGeneralId) ) return false;
		 ParametroGeneralId castOther = ( ParametroGeneralId ) other; 
         
		 return ( (this.getIdToGrupoParametroGeneral()==castOther.getIdToGrupoParametroGeneral()) || ( this.getIdToGrupoParametroGeneral()!=null && castOther.getIdToGrupoParametroGeneral()!=null && this.getIdToGrupoParametroGeneral().equals(castOther.getIdToGrupoParametroGeneral()) ) )
 && ( (this.getCodigoParametroGeneral()==castOther.getCodigoParametroGeneral()) || ( this.getCodigoParametroGeneral()!=null && castOther.getCodigoParametroGeneral()!=null && this.getCodigoParametroGeneral().equals(castOther.getCodigoParametroGeneral()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdToGrupoParametroGeneral() == null ? 0 : this.getIdToGrupoParametroGeneral().hashCode() );
         result = 37 * result + ( getCodigoParametroGeneral() == null ? 0 : this.getCodigoParametroGeneral().hashCode() );
         return result;
   }   

}