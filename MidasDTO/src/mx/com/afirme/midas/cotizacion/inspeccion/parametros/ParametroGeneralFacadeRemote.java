package mx.com.afirme.midas.cotizacion.inspeccion.parametros;
// default package

import java.math.BigDecimal;
import java.util.List;

/**
 * Remote interface for ParametroGeneralDTOFacade.
 * @author MyEclipse Persistence Tools
 */
public interface ParametroGeneralFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ParametroGeneralDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ParametroGeneralDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ParametroGeneralDTO entity);
    /**
	 Delete a persistent ParametroGeneralDTO entity.
	  @param entity ParametroGeneralDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ParametroGeneralDTO entity);
   /**
	 Persist a previously saved ParametroGeneralDTO entity and return it or a copy of it to the sender. 
	 A copy of the ParametroGeneralDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ParametroGeneralDTO entity to update
	 @return ParametroGeneralDTO the persisted ParametroGeneralDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ParametroGeneralDTO update(ParametroGeneralDTO entity);
	public ParametroGeneralDTO findById( ParametroGeneralId id);
	public ParametroGeneralDTO findById(ParametroGeneralId id, boolean refresh); 
	 /**
	 * Find all ParametroGeneralDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ParametroGeneralDTO property to query
	  @param value the property value to match
	  	  @return List<ParametroGeneralDTO> found by query
	 */
	public List<ParametroGeneralDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ParametroGeneralDTO entities.
	  	  @return List<ParametroGeneralDTO> all ParametroGeneralDTO entities
	 */
	public List<ParametroGeneralDTO> findAll(
		);
	
	public List<ParametroGeneralDTO> listarFiltrado(ParametroGeneralDTO entity);
	
	public String getValueByCode (BigDecimal code);
	
}