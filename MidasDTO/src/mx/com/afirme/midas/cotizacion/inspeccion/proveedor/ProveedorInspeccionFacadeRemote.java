package mx.com.afirme.midas.cotizacion.inspeccion.proveedor;
// default package

import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for ProveedorInspeccionDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ProveedorInspeccionFacadeRemote extends MidasInterfaceBase<ProveedorInspeccionDTO>{
		/**
	 Perform an initial save of a previously unsaved ProveedorInspeccionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ProveedorInspeccionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public ProveedorInspeccionDTO save(ProveedorInspeccionDTO entity);
    /**
	 Delete a persistent ProveedorInspeccionDTO entity.
	  @param entity ProveedorInspeccionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ProveedorInspeccionDTO entity);
   /**
	 Persist a previously saved ProveedorInspeccionDTO entity and return it or a copy of it to the sender. 
	 A copy of the ProveedorInspeccionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ProveedorInspeccionDTO entity to update
	 @return ProveedorInspeccionDTO the persisted ProveedorInspeccionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ProveedorInspeccionDTO update(ProveedorInspeccionDTO entity);
	
	 /**
	 * Find all ProveedorInspeccionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ProveedorInspeccionDTO property to query
	  @param value the property value to match
	  	  @return List<ProveedorInspeccionDTO> found by query
	 */
	public List<ProveedorInspeccionDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ProveedorInspeccionDTO entities.
	  	  @return List<ProveedorInspeccionDTO> all ProveedorInspeccionDTO entities
	 */
	public List<ProveedorInspeccionDTO> findAll();	
}