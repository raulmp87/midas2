package mx.com.afirme.midas.cotizacion.inspeccion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.documento.DocumentoAnexoInspeccionIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.proveedor.ProveedorInspeccionDTO;

/**
 * Toinspeccionincisocot entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOINSPECCIONINCISOCOT", schema = "MIDAS")
public class InspeccionIncisoCotizacionDTO implements java.io.Serializable {

	// Fields

	private static final long serialVersionUID = -9018565036404134359L;
	private BigDecimal idToInspeccionIncisoCotizacion;
	private IncisoCotizacionDTO incisoCotizacion;
	private ProveedorInspeccionDTO proveedorInspeccion;
	private BigDecimal numeroReporte;
	private String nombrePersonaEntrevistada;
	private Date fechaInspeccion;
	private Date fechaReporteInspeccion;
	private Short claveResultadoInspeccion;
	private String comentariosReporte;
	private List<DocumentoAnexoInspeccionIncisoCotizacionDTO> documentoAnexoInspeccionIncisoCotizacionList;

	// Constructors

	/** default constructor */
	public InspeccionIncisoCotizacionDTO() {
		documentoAnexoInspeccionIncisoCotizacionList = new ArrayList<DocumentoAnexoInspeccionIncisoCotizacionDTO>();
	}


	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTOINSPECCIONINCISOCOT_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOINSPECCIONINCISOCOT_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOINSPECCIONINCISOCOT_SEQ_GENERADOR")	
	@Column(name = "IDTOINSPECCIONINCISOCOT", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToInspeccionIncisoCotizacion() {
		return this.idToInspeccionIncisoCotizacion;
	}

	public void setIdToInspeccionIncisoCotizacion(BigDecimal idToInspeccionIncisoCotizacion) {
		this.idToInspeccionIncisoCotizacion = idToInspeccionIncisoCotizacion;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( {
			@JoinColumn(name = "IDTOCOTIZACION", referencedColumnName = "IDTOCOTIZACION", nullable = false),
			@JoinColumn(name = "NUMEROINCISO", referencedColumnName = "NUMEROINCISO", nullable = false) })
	public IncisoCotizacionDTO getIncisoCotizacion() {
		return this.incisoCotizacion;
	}

	public void setIncisoCotizacion(IncisoCotizacionDTO incisoCotizacion) {
		this.incisoCotizacion = incisoCotizacion;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOPROVEEDORINSPECCION", nullable = false)
	public ProveedorInspeccionDTO getProveedorInspeccion() {
		return this.proveedorInspeccion;
	}

	public void setProveedorInspeccion(
			ProveedorInspeccionDTO proveedorInspeccion) {
		this.proveedorInspeccion = proveedorInspeccion;
	}

	@Column(name = "NUMEROREPORTE", nullable = false, precision = 22, scale = 0)
	public BigDecimal getNumeroReporte() {
		return this.numeroReporte;
	}

	public void setNumeroReporte(BigDecimal numeroReporte) {
		this.numeroReporte = numeroReporte;
	}

	@Column(name = "NOMBREPERSONAENTREVISTADA", nullable = false, length = 100)
	public String getNombrePersonaEntrevistada() {
		return this.nombrePersonaEntrevistada;
	}

	public void setNombrePersonaEntrevistada(String nombrePersonaEntrevistada) {
		this.nombrePersonaEntrevistada = nombrePersonaEntrevistada;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAINSPECCION", nullable = false, length = 7)
	public Date getFechaInspeccion() {
		return this.fechaInspeccion;
	}

	public void setFechaInspeccion(Date fechaInspeccion) {
		this.fechaInspeccion = fechaInspeccion;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAREPORTEINSPECCION", nullable = false, length = 7)
	public Date getFechaReporteInspeccion() {
		return this.fechaReporteInspeccion;
	}

	public void setFechaReporteInspeccion(Date fechaReporteInspeccion) {
		this.fechaReporteInspeccion = fechaReporteInspeccion;
	}

	@Column(name = "CLAVERESULTADOINSPECCION", nullable = false, precision = 4, scale = 0)
	public Short getClaveResultadoInspeccion() {
		return this.claveResultadoInspeccion;
	}

	public void setClaveResultadoInspeccion(Short claveResultadoInspeccion) {
		this.claveResultadoInspeccion = claveResultadoInspeccion;
	}

	@Column(name = "COMENTARIOSREPORTE", length = 4000)
	public String getComentariosReporte() {
		return this.comentariosReporte;
	}

	public void setComentariosReporte(String comentariosReporte) {
		this.comentariosReporte = comentariosReporte;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "inspeccionIncisoCotizacion")
	public List<DocumentoAnexoInspeccionIncisoCotizacionDTO> getDocumentoAnexoInspeccionIncisoCotizacionList() {
		return this.documentoAnexoInspeccionIncisoCotizacionList;
	}

	public void setDocumentoAnexoInspeccionIncisoCotizacionList(List<DocumentoAnexoInspeccionIncisoCotizacionDTO> documentoAnexoInspeccionIncisoCotizacionList) {
		this.documentoAnexoInspeccionIncisoCotizacionList = documentoAnexoInspeccionIncisoCotizacionList;
	}
	
}