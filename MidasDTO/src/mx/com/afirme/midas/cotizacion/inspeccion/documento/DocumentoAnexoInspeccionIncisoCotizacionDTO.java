package mx.com.afirme.midas.cotizacion.inspeccion.documento;
// default package

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.cotizacion.inspeccion.InspeccionIncisoCotizacionDTO;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;


/**
 * DocumentoAnexoInspeccionIncisoCotizacionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TODOCANEXOINSPINCISOCOT",schema="MIDAS")
public class DocumentoAnexoInspeccionIncisoCotizacionDTO  implements java.io.Serializable {
	
	// Fields    
	
	 private static final long serialVersionUID = -8982220644752313757L;
     private BigDecimal idToDocumentoAnexoInspeccionIncisoCotizacion;
     private InspeccionIncisoCotizacionDTO inspeccionIncisoCotizacion;
     private BigDecimal idToControlArchivo;
     private ControlArchivoDTO controlArchivo;
     private Date fechaCreacion;
     private String codigoUsuarioCreacion;
     private String nombreUsuarioCreacion;
     private Date fechaModificacion;
     private String codigoUsuarioModificacion;
     private String nombreUsuarioModificacion;


    // Constructors

    /** default constructor */
    public DocumentoAnexoInspeccionIncisoCotizacionDTO() {
    }

    // Property accessors
    @Id
    @SequenceGenerator(name = "IDTODOCANEXOINSINCCOT_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTODOCANEXOINSINCCOT_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTODOCANEXOINSINCCOT_SEQ_GENERADOR")	
    @Column(name="IDTODOCANEXOINSINCCOT", nullable=false, precision=22, scale=0)
    public BigDecimal getIdToDocumentoAnexoInspeccionIncisoCotizacion() {
        return this.idToDocumentoAnexoInspeccionIncisoCotizacion;
    }
    
    public void setIdToDocumentoAnexoInspeccionIncisoCotizacion(BigDecimal idToDocumentoAnexoInspeccionIncisoCotizacion) {
        this.idToDocumentoAnexoInspeccionIncisoCotizacion = idToDocumentoAnexoInspeccionIncisoCotizacion;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTOINSPECCIONINCISOCOT", nullable=false)
    public InspeccionIncisoCotizacionDTO getInspeccionIncisoCotizacion() {
        return this.inspeccionIncisoCotizacion;
    }
    
    public void setInspeccionIncisoCotizacion(InspeccionIncisoCotizacionDTO inspeccionIncisoCotizacion) {
        this.inspeccionIncisoCotizacion = inspeccionIncisoCotizacion;
    }
    
    @Column(name="IDTOCONTROLARCHIVO", nullable=false, precision=22, scale=0)
    public BigDecimal getIdToControlArchivo() {
        return this.idToControlArchivo;
    }
    
    public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
        this.idToControlArchivo = idToControlArchivo;
    }
  
    
    @OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOCONTROLARCHIVO", nullable = false, insertable = false, updatable = false)
	public ControlArchivoDTO getControlArchivo() {
		return controlArchivo;
	}

	public void setControlArchivo(ControlArchivoDTO controlArchivoDTO) {
		this.controlArchivo = controlArchivoDTO;
	}
    
    @Temporal(TemporalType.DATE)
    @Column(name="FECHACREACION", nullable=false, length=7)
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }
    
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    
    @Column(name="CODIGOUSUARIOCREACION", nullable=false, length=8)
    public String getCodigoUsuarioCreacion() {
        return this.codigoUsuarioCreacion;
    }
    
    public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
        this.codigoUsuarioCreacion = codigoUsuarioCreacion;
    }
    
    @Column(name="NOMBREUSUARIOCREACION", nullable=false, length=200)
    public String getNombreUsuarioCreacion() {
        return this.nombreUsuarioCreacion;
    }
    
    public void setNombreUsuarioCreacion(String nombreUsuarioCreacion) {
        this.nombreUsuarioCreacion = nombreUsuarioCreacion;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAMODIFICACION", length=7)
    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }
    
    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
    
    @Column(name="CODIGOUSUARIOMODIFICACION", length=8)

    public String getCodigoUsuarioModificacion() {
        return this.codigoUsuarioModificacion;
    }
    
    public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
        this.codigoUsuarioModificacion = codigoUsuarioModificacion;
    }
    
    @Column(name="NOMBREUSUARIOMODIFICACION", length=200)
    public String getNombreUsuarioModificacion() {
        return this.nombreUsuarioModificacion;
    }
    
    public void setNombreUsuarioModificacion(String nombreUsuarioModificacion) {
        this.nombreUsuarioModificacion = nombreUsuarioModificacion;
    }

}