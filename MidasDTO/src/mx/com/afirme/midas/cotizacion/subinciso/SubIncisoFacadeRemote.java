package mx.com.afirme.midas.cotizacion.subinciso;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for SubIncisoDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface SubIncisoFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved SubIncisoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity SubIncisoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public SubIncisoDTO save(SubIncisoDTO entity);
    /**
	 Delete a persistent SubIncisoDTO entity.
	  @param entity SubIncisoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(SubIncisoDTO entity);
   /**
	 Persist a previously saved SubIncisoDTO entity and return it or a copy of it to the sender. 
	 A copy of the SubIncisoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity SubIncisoDTO entity to update
	 @return SubIncisoDTO the persisted SubIncisoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public SubIncisoDTO update(SubIncisoDTO entity);
	public SubIncisoDTO findById( BigDecimal id);
	 /**
	 * Find all SubIncisoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the SubIncisoDTO property to query
	  @param value the property value to match
	  	  @return List<SubIncisoDTO> found by query
	 */
	public List<SubIncisoDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all SubIncisoDTO entities.
	  	  @return List<SubIncisoDTO> all SubIncisoDTO entities
	 */
	public List<SubIncisoDTO> findAll(
		);	
}