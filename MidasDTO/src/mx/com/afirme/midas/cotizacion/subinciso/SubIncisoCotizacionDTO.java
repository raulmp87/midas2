package mx.com.afirme.midas.cotizacion.subinciso;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import mx.com.afirme.midas.cotizacion.reaseguro.subinciso.ReaseguroSubIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;

/**
 * SubIncisoCotizacionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOSUBINCISOCOT", schema = "MIDAS")
public class SubIncisoCotizacionDTO implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = 4134683120594432319L;
	private SubIncisoCotizacionDTOId id;
	private SeccionCotizacionDTO seccionCotizacionDTO;
	private Short claveAutReaseguro;
	private String codigoUsuarioAutReaseguro;
	private String descripcionSubInciso;
	private Double valorSumaAsegurada;
	private List<ReaseguroSubIncisoCotizacionDTO> reaseguroSubIncisoCotizacionDTOs = new ArrayList<ReaseguroSubIncisoCotizacionDTO>();
	private Short claveEstatusDeclaracion;

	// Constructors

	/** default constructor */
	public SubIncisoCotizacionDTO() {
		if (id == null)
			id = new SubIncisoCotizacionDTOId();
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToCotizacion", column = @Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroInciso", column = @Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToSeccion", column = @Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroSubInciso", column = @Column(name = "NUMEROSUBINCISO", nullable = false, precision = 22, scale = 0)) })
	public SubIncisoCotizacionDTOId getId() {
		return this.id;
	}

	public void setId(SubIncisoCotizacionDTOId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumns( {
			@JoinColumn(name = "IDTOCOTIZACION", referencedColumnName = "IDTOCOTIZACION", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "NUMEROINCISO", referencedColumnName = "NUMEROINCISO", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTOSECCION", referencedColumnName = "IDTOSECCION", nullable = false, insertable = false, updatable = false) })
	public SeccionCotizacionDTO getSeccionCotizacionDTO() {
		return this.seccionCotizacionDTO;
	}

	public void setSeccionCotizacionDTO(
			SeccionCotizacionDTO seccionCotizacionDTO) {
		this.seccionCotizacionDTO = seccionCotizacionDTO;
	}

	@Column(name = "CLAVEAUTREASEGURO", nullable = false, precision = 4, scale = 0)
	public Short getClaveAutReaseguro() {
		return this.claveAutReaseguro;
	}

	public void setClaveAutReaseguro(Short claveAutReaseguro) {
		this.claveAutReaseguro = claveAutReaseguro;
	}

	@Column(name = "CODIGOUSUARIOAUTREASEGURO", length = 8)
	public String getCodigoUsuarioAutReaseguro() {
		return this.codigoUsuarioAutReaseguro;
	}

	public void setCodigoUsuarioAutReaseguro(String codigoUsuarioAutReaseguro) {
		this.codigoUsuarioAutReaseguro = codigoUsuarioAutReaseguro;
	}

	@Column(name = "DESCRIPCIONSUBINCISO", nullable = false, length = 200)
	public String getDescripcionSubInciso() {
		return this.descripcionSubInciso;
	}

	public void setDescripcionSubInciso(String descripcionSubInciso) {
		this.descripcionSubInciso = descripcionSubInciso;
	}

	@Column(name = "VALORSUMAASEGURADA", nullable = false, precision = 16)
	public Double getValorSumaAsegurada() {
		return this.valorSumaAsegurada;
	}

	public void setValorSumaAsegurada(Double valorSumaAsegurada) {
		this.valorSumaAsegurada = valorSumaAsegurada;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "subIncisoCotizacionDTO")
	public List<ReaseguroSubIncisoCotizacionDTO> getReaseguroSubIncisoCotizacionDTOs() {
		return this.reaseguroSubIncisoCotizacionDTOs;
	}

	public void setReaseguroSubIncisoCotizacionDTOs(
			List<ReaseguroSubIncisoCotizacionDTO> reaseguroSubIncisoCotizacionDTOs) {
		this.reaseguroSubIncisoCotizacionDTOs = reaseguroSubIncisoCotizacionDTOs;
	}

	@Column(name = "CLAVEESTATUSDECLARACION")
	public Short getClaveEstatusDeclaracion() {
		return claveEstatusDeclaracion;
	}

	public void setClaveEstatusDeclaracion(Short claveEstatusDeclaracion) {
		this.claveEstatusDeclaracion = claveEstatusDeclaracion;
	}
}