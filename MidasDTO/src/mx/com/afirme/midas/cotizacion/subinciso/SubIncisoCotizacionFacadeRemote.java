package mx.com.afirme.midas.cotizacion.subinciso;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;

/**
 * Remote interface for SubIncisoCotizacionDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface SubIncisoCotizacionFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved SubIncisoCotizacionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity SubIncisoCotizacionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public SubIncisoCotizacionDTO save(SubIncisoCotizacionDTO entity);
    /**
	 Delete a persistent SubIncisoCotizacionDTO entity.
	  @param entity SubIncisoCotizacionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(SubIncisoCotizacionDTO entity);
   /**
	 Persist a previously saved SubIncisoCotizacionDTO entity and return it or a copy of it to the sender. 
	 A copy of the SubIncisoCotizacionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity SubIncisoCotizacionDTO entity to update
	 @return SubIncisoCotizacionDTO the persisted SubIncisoCotizacionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public SubIncisoCotizacionDTO update(SubIncisoCotizacionDTO entity);
	public SubIncisoCotizacionDTO findById( SubIncisoCotizacionDTOId id);
	 /**
	 * Find all SubIncisoCotizacionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the SubIncisoCotizacionDTO property to query
	  @param value the property value to match
	  	  @return List<SubIncisoCotizacionDTO> found by query
	 */
	public List<SubIncisoCotizacionDTO> findByProperty(String propertyName, Object value
		);
	public List<SubIncisoCotizacionDTO> findByClaveAutReaseguro(Object claveAutReaseguro
		);
	public List<SubIncisoCotizacionDTO> findByCodigoUsuarioAutReaseguro(Object codigoUsuarioAutReaseguro
		);
	/**
	 * Find all SubIncisoCotizacionDTO entities.
	  	  @return List<SubIncisoCotizacionDTO> all SubIncisoCotizacionDTO entities
	 */
	public List<SubIncisoCotizacionDTO> findAll();
	
	/**
	 * Find SubIncisoCotizacionDTO entities that match with the given attributes.
	  	  @return List<SubIncisoCotizacionDTO> filtered list of SubIncisoCotizacionDTO entities
	 */
	public List<SubIncisoCotizacionDTO> listarFiltrado(SubIncisoCotizacionDTO subIncisoCotizacionDTO);
	
	public List<SubIncisoCotizacionDTO> listarFiltrado(SubIncisoCotizacionDTO subIncisoCotizacionDTO,boolean aplicarMerge);

	public BigDecimal maxSubIncisos(BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idToSeccion);
	
	public BigDecimal primaNetaSubInciso(SubIncisoCotizacionDTOId id);

	public void borrarSubincisosSeccionCotizacion(SeccionCotizacionDTOId id);
	
	public List<SubIncisoCotizacionDTO> listarSubIncisosPorCotizacion(BigDecimal idToCotizacion);
}