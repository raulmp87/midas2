package mx.com.afirme.midas.cotizacion.subinciso;
// default package

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * SubIncisoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOSUBINCISO",schema="MIDAS")
public class SubIncisoDTO  implements java.io.Serializable {


    // Fields    
	 private static final long serialVersionUID = -676657930698502207L;
	 private BigDecimal idToSubInciso;
     private String descripcionSubInciso;
     private Double sumaAsegurada;
     private Short tipoEquipo;
     private Short anioFabricacion;
     private String marca;
     private String modelo;
     private String noSerie;


    // Constructors

    /** default constructor */
    public SubIncisoDTO() {
    }

   
    // Property accessors
    @Id
    @SequenceGenerator(name = "IDTOSUBINCISO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOSUBINCISO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOSUBINCISO_SEQ_GENERADOR")	  	
    @Column(name="IDTOSUBINCISO", nullable=false, precision=22, scale=0)
    public BigDecimal getIdToSubInciso() {
        return this.idToSubInciso;
    }
    
    public void setIdToSubInciso(BigDecimal idToSubInciso) {
        this.idToSubInciso = idToSubInciso;
    }
    
    @Column(name="DESCRIPCIONSUBINCISO", nullable=false)

    public String getDescripcionSubInciso() {
        return this.descripcionSubInciso;
    }
    
    public void setDescripcionSubInciso(String descripcionSubInciso) {
        this.descripcionSubInciso = descripcionSubInciso;
    }
    
    @Column(name="SUMAASEGURADA", precision=12, scale=4)

    public Double getSumaAsegurada() {
        return this.sumaAsegurada;
    }
    
    public void setSumaAsegurada(Double sumaAsegurada) {
        this.sumaAsegurada = sumaAsegurada;
    }
    
    @Column(name="TIPOEQUIPO", precision=4, scale=0)

    public Short getTipoEquipo() {
        return this.tipoEquipo;
    }
    
    public void setTipoEquipo(Short tipoEquipo) {
        this.tipoEquipo = tipoEquipo;
    }
    
    @Column(name="ANIOFABRICACION", precision=4, scale=0)

    public Short getAnioFabricacion() {
        return this.anioFabricacion;
    }
    
    public void setAnioFabricacion(Short anioFabricacion) {
        this.anioFabricacion = anioFabricacion;
    }
    
    @Column(name="MARCA", length=50)

    public String getMarca() {
        return this.marca;
    }
    
    public void setMarca(String marca) {
        this.marca = marca;
    }
    
    @Column(name="MODELO", length=50)

    public String getModelo() {
        return this.modelo;
    }
    
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
    
    @Column(name="NOSERIE", length=100)

    public String getNoSerie() {
        return this.noSerie;
    }
    
    public void setNoSerie(String noSerie) {
        this.noSerie = noSerie;
    }

}