package mx.com.afirme.midas.cotizacion.subinciso;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * SubIncisoCotizacionDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class SubIncisoCotizacionDTOId  implements java.io.Serializable {


    // Fields    

	private static final long serialVersionUID = 6883640423313245170L;
	
	private BigDecimal idToCotizacion;
    private BigDecimal numeroInciso;
    private BigDecimal idToSeccion;
    private BigDecimal numeroSubInciso;


    // Constructors

    /** default constructor */
    public SubIncisoCotizacionDTOId() {
    }

    
    /** full constructor */
    public SubIncisoCotizacionDTOId(BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idToSeccion, BigDecimal numeroSubInciso) {
        this.idToCotizacion = idToCotizacion;
        this.numeroInciso = numeroInciso;
        this.idToSeccion = idToSeccion;
        this.numeroSubInciso = numeroSubInciso;
    }

   
    // Property accessors

    @Column(name="IDTOCOTIZACION", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToCotizacion() {
        return this.idToCotizacion;
    }
    
    public void setIdToCotizacion(BigDecimal idToCotizacion) {
        this.idToCotizacion = idToCotizacion;
    }

    @Column(name="NUMEROINCISO", nullable=false, precision=22, scale=0)

    public BigDecimal getNumeroInciso() {
        return this.numeroInciso;
    }
    
    public void setNumeroInciso(BigDecimal numeroinciso) {
        this.numeroInciso = numeroinciso;
    }

    @Column(name="IDTOSECCION", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToSeccion() {
        return this.idToSeccion;
    }
    
    public void setIdToSeccion(BigDecimal idToSeccion) {
        this.idToSeccion = idToSeccion;
    }

    @Column(name="NUMEROSUBINCISO", nullable=false, precision=22, scale=0)

    public BigDecimal getNumeroSubInciso() {
        return this.numeroSubInciso;
    }
    
    public void setNumeroSubInciso(BigDecimal numeroSubInciso) {
        this.numeroSubInciso = numeroSubInciso;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof SubIncisoCotizacionDTOId) ) return false;
		 SubIncisoCotizacionDTOId castOther = ( SubIncisoCotizacionDTOId ) other;
         
		 return ( (this.getIdToCotizacion().equals(castOther.getIdToCotizacion())) || ( this.getIdToCotizacion()!=null && castOther.getIdToCotizacion()!=null && this.getIdToCotizacion().equals(castOther.getIdToCotizacion()) ) )
 && ( (this.getNumeroInciso().equals(castOther.getNumeroInciso())) || ( this.getNumeroInciso()!=null && castOther.getNumeroInciso()!=null && this.getNumeroInciso().equals(castOther.getNumeroInciso()) ) )
 && ( (this.getIdToSeccion().equals(castOther.getIdToSeccion())) || ( this.getIdToSeccion()!=null && castOther.getIdToSeccion()!=null && this.getIdToSeccion().equals(castOther.getIdToSeccion()) ) )
 && ( (this.getNumeroSubInciso().equals(castOther.getNumeroSubInciso())) || ( this.getNumeroSubInciso()!=null && castOther.getNumeroSubInciso()!=null && this.getNumeroSubInciso().equals(castOther.getNumeroSubInciso()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdToCotizacion() == null ? 0 : this.getIdToCotizacion().hashCode() );
         result = 37 * result + ( getNumeroInciso() == null ? 0 : this.getNumeroInciso().hashCode() );
         result = 37 * result + ( getIdToSeccion() == null ? 0 : this.getIdToSeccion().hashCode() );
         result = 37 * result + ( getNumeroSubInciso() == null ? 0 : this.getNumeroSubInciso().hashCode() );
         return result;
   }   





}