package mx.com.afirme.midas.cotizacion.calculo;

import java.io.Serializable;
import java.math.BigDecimal;

public class CalculoLUCDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToCotizacion;
	private BigDecimal idToSeccion;
	private Double sumaAsegurada;
	private Double primaNetaLUC;
	private Double cuotaLUC;

	public Double getSumaAsegurada() {
		return sumaAsegurada;
	}

	public void setSumaAsegurada(Double sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}

	public Double getPrimaNetaLUC() {
		return primaNetaLUC;
	}

	public void setPrimaNetaLUC(Double primaNetaLUC) {
		this.primaNetaLUC = primaNetaLUC;
	}

	public Double getCuotaLUC() {
		return cuotaLUC;
	}

	public void setCuotaLUC(Double cuotaLUC) {
		this.cuotaLUC = cuotaLUC;
	}

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

}
