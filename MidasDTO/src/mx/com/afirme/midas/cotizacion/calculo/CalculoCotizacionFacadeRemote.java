package mx.com.afirme.midas.cotizacion.calculo;

import java.math.BigDecimal;

import javax.ejb.Remote;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;


public interface CalculoCotizacionFacadeRemote {

	public CalculoRiesgoDTO calcularRiesgo(CalculoRiesgoDTO calculoRiesgoDTO,
			String nombreUsuario) throws Exception;

	public void inicializarCalculoRiesgo(BigDecimal idToCotizacion,String nombreUsuario) throws Exception;

	public CalculoLUCDTO calcularLUC(CalculoLUCDTO calculoLUCDTO,
			String nombreUsuario) throws Exception;

	public void calcularRiesgosHidro(BigDecimal idToCotizacion,
			BigDecimal numeroInciso, String nombreUsuario) throws Exception;
		
	public CalculoRiesgoDTO calcularRiesgoEmbarque(
			CalculoRiesgoDTO calculoRiesgoDTO, String nombreUsuario)
			throws Exception;
	
	public CalculoRiesgoDTO eliminarRiesgoEmbarque(
			CalculoRiesgoDTO calculoRiesgoDTO, String nombreUsuario)
			throws Exception;	
	
	
	public CoberturaCotizacionDTO calcularCobertura(
			CoberturaCotizacionDTO coberturaCotizacionDTO, String nombreUsuario, boolean seContratoCobertura) throws Exception;
	
	public CoberturaCotizacionDTO calcularCoberturaCasa(
			CoberturaCotizacionDTO coberturaCotizacionDTO, String nombreUsuario, boolean seContratoCobertura) throws Exception;
	
	
	public void actualizaAnexosDeCoberturasContratadas(BigDecimal idToCotizacion);
	
}
