package mx.com.afirme.midas.cotizacion.calculo;

import java.io.Serializable;
import java.math.BigDecimal;

public class CalculoRiesgoDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private BigDecimal idToCotizacion;
	private BigDecimal numeroInciso;
	private BigDecimal idToSeccion;
	private BigDecimal idToCobertura;
	private BigDecimal idToRiesgo;
	private BigDecimal numeroSubInciso;
	private Double sumaAsegurada;
	private Double primaNetaB;
	private Double primaNetaARDT;
	private Double primaNetaARDV;

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	public BigDecimal getIdToCobertura() {
		return idToCobertura;
	}

	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	public BigDecimal getIdToRiesgo() {
		return idToRiesgo;
	}

	public void setIdToRiesgo(BigDecimal idToRiesgo) {
		this.idToRiesgo = idToRiesgo;
	}

	public BigDecimal getNumeroSubInciso() {
		return numeroSubInciso;
	}

	public void setNumeroSubInciso(BigDecimal numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}

	public Double getSumaAsegurada() {
		return sumaAsegurada;
	}

	public void setSumaAsegurada(Double sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}

	public Double getPrimaNetaB() {
		return primaNetaB;
	}

	public void setPrimaNetaB(Double primaNetaB) {
		this.primaNetaB = primaNetaB;
	}

	public Double getPrimaNetaARDT() {
		return primaNetaARDT;
	}

	public void setPrimaNetaARDT(Double primaNetaARDT) {
		this.primaNetaARDT = primaNetaARDT;
	}

	public Double getPrimaNetaARDV() {
		return primaNetaARDV;
	}

	public void setPrimaNetaARDV(Double primaNetaARDV) {
		this.primaNetaARDV = primaNetaARDV;
	}

}
