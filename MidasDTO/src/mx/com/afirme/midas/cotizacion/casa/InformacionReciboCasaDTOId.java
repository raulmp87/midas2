package mx.com.afirme.midas.cotizacion.casa;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;


/**
 * InformacionReciboCasaDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class InformacionReciboCasaDTOId  implements java.io.Serializable {


    // Fields    

     private static final long serialVersionUID = -7062521866792560426L;
	 private String idToProducto;
     private BigDecimal idToPoliza;
     private BigDecimal numeroRenovacion;
     private BigDecimal numeroEndoso;
     private BigDecimal idToCotizacion;
     private BigDecimal idTipo;
     private String idTcRamo;
     private String idTcSubRamo;
     private BigDecimal numeroInciso;
     private String idToCobertura;
     private BigDecimal idToSeccion;


    // Constructors

    /** default constructor */
    public InformacionReciboCasaDTOId() {
    }

    
    /** full constructor */
    public InformacionReciboCasaDTOId(String idToProducto, BigDecimal idToPoliza, BigDecimal numeroRenovacion, BigDecimal numeroEndoso, BigDecimal idToCotizacion, BigDecimal idTipo, String idTcRamo, String idTcSubRamo, BigDecimal numeroInciso, String idToCobertura) {
        this.idToProducto = idToProducto;
        this.idToPoliza = idToPoliza;
        this.numeroRenovacion = numeroRenovacion;
        this.numeroEndoso = numeroEndoso;
        this.idToCotizacion = idToCotizacion;
        this.idTipo = idTipo;
        this.idTcRamo = idTcRamo;
        this.idTcSubRamo = idTcSubRamo;
        this.numeroInciso = numeroInciso;
        this.idToCobertura = idToCobertura;
    }

   
    // Property accessors

    @Column(name="IDTOPRODUCTO", nullable=false, length=10)

    public String getIdToProducto() {
        return this.idToProducto;
    }
    
    public void setIdToProducto(String idToProducto) {
        this.idToProducto = idToProducto;
    }

    @Column(name="IDTOPOLIZA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToPoliza() {
        return this.idToPoliza;
    }
    
    public void setIdToPoliza(BigDecimal idToPoliza) {
        this.idToPoliza = idToPoliza;
    }

    @Column(name="NUMRENOVACION", nullable=false, precision=22, scale=0)

    public BigDecimal getNumeroRenovacion() {
        return this.numeroRenovacion;
    }
    
    public void setNumeroRenovacion(BigDecimal numeroRenovacion) {
        this.numeroRenovacion = numeroRenovacion;
    }

    @Column(name="NUMEROENDOSO", nullable=false, precision=22, scale=0)

    public BigDecimal getNumeroEndoso() {
        return this.numeroEndoso;
    }
    
    public void setNumeroEndoso(BigDecimal numeroEndoso) {
        this.numeroEndoso = numeroEndoso;
    }

    @Column(name="IDTOCOTIZACION", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToCotizacion() {
        return this.idToCotizacion;
    }
    
    public void setIdToCotizacion(BigDecimal idToCotizacion) {
        this.idToCotizacion = idToCotizacion;
    }

    @Column(name="IDTIPO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdTipo() {
        return this.idTipo;
    }
    
    public void setIdTipo(BigDecimal idTipo) {
        this.idTipo = idTipo;
    }

    @Column(name="IDTCRAMO", nullable=false, length=2)

    public String getIdTcRamo() {
        return this.idTcRamo;
    }
    
    public void setIdTcRamo(String idTcRamo) {
        this.idTcRamo = idTcRamo;
    }

    @Column(name="IDTCSUBRAMO", nullable=false, length=2)

    public String getIdTcSubRamo() {
        return this.idTcSubRamo;
    }
    
    public void setIdTcSubRamo(String idTcSubRamo) {
        this.idTcSubRamo = idTcSubRamo;
    }

    @Column(name="NUMEROINCISO", nullable=false, precision=22, scale=0)

    public BigDecimal getNumeroInciso() {
        return this.numeroInciso;
    }
    
    public void setNumeroInciso(BigDecimal numeroInciso) {
        this.numeroInciso = numeroInciso;
    }

    @Column(name="IDTOCOBERTURA", nullable=false, length=10)

    public String getIdToCobertura() {
        return this.idToCobertura;
    }
    
    public void setIdToCobertura(String idToCobertura) {
        this.idToCobertura = idToCobertura;
    }
   


   @Transient
   public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}


	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}


public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof InformacionReciboCasaDTOId) ) return false;
		 InformacionReciboCasaDTOId castOther = ( InformacionReciboCasaDTOId ) other; 
         
		 return ( (this.getIdToProducto()==castOther.getIdToProducto()) || ( this.getIdToProducto()!=null && castOther.getIdToProducto()!=null && this.getIdToProducto().equals(castOther.getIdToProducto()) ) )
 && ( (this.getIdToPoliza()==castOther.getIdToPoliza()) || ( this.getIdToPoliza()!=null && castOther.getIdToPoliza()!=null && this.getIdToPoliza().equals(castOther.getIdToPoliza()) ) )
 && ( (this.getNumeroRenovacion()==castOther.getNumeroRenovacion()) || ( this.getNumeroRenovacion()!=null && castOther.getNumeroRenovacion()!=null && this.getNumeroRenovacion().equals(castOther.getNumeroRenovacion()) ) )
 && ( (this.getNumeroEndoso()==castOther.getNumeroEndoso()) || ( this.getNumeroEndoso()!=null && castOther.getNumeroEndoso()!=null && this.getNumeroEndoso().equals(castOther.getNumeroEndoso()) ) )
 && ( (this.getIdToCotizacion()==castOther.getIdToCotizacion()) || ( this.getIdToCotizacion()!=null && castOther.getIdToCotizacion()!=null && this.getIdToCotizacion().equals(castOther.getIdToCotizacion()) ) )
 && ( (this.getIdTipo()==castOther.getIdTipo()) || ( this.getIdTipo()!=null && castOther.getIdTipo()!=null && this.getIdTipo().equals(castOther.getIdTipo()) ) )
 && ( (this.getIdTcRamo()==castOther.getIdTcRamo()) || ( this.getIdTcRamo()!=null && castOther.getIdTcRamo()!=null && this.getIdTcRamo().equals(castOther.getIdTcRamo()) ) )
 && ( (this.getIdTcSubRamo()==castOther.getIdTcSubRamo()) || ( this.getIdTcSubRamo()!=null && castOther.getIdTcSubRamo()!=null && this.getIdTcSubRamo().equals(castOther.getIdTcSubRamo()) ) )
 && ( (this.getNumeroInciso()==castOther.getNumeroInciso()) || ( this.getNumeroInciso()!=null && castOther.getNumeroInciso()!=null && this.getNumeroInciso().equals(castOther.getNumeroInciso()) ) )
 && ( (this.getIdToCobertura()==castOther.getIdToCobertura()) || ( this.getIdToCobertura()!=null && castOther.getIdToCobertura()!=null && this.getIdToCobertura().equals(castOther.getIdToCobertura()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdToProducto() == null ? 0 : this.getIdToProducto().hashCode() );
         result = 37 * result + ( getIdToPoliza() == null ? 0 : this.getIdToPoliza().hashCode() );
         result = 37 * result + ( getNumeroRenovacion() == null ? 0 : this.getNumeroRenovacion().hashCode() );
         result = 37 * result + ( getNumeroEndoso() == null ? 0 : this.getNumeroEndoso().hashCode() );
         result = 37 * result + ( getIdToCotizacion() == null ? 0 : this.getIdToCotizacion().hashCode() );
         result = 37 * result + ( getIdTipo() == null ? 0 : this.getIdTipo().hashCode() );
         result = 37 * result + ( getIdTcRamo() == null ? 0 : this.getIdTcRamo().hashCode() );
         result = 37 * result + ( getIdTcSubRamo() == null ? 0 : this.getIdTcSubRamo().hashCode() );
         result = 37 * result + ( getNumeroInciso() == null ? 0 : this.getNumeroInciso().hashCode() );
         result = 37 * result + ( getIdToCobertura() == null ? 0 : this.getIdToCobertura().hashCode() );
         return result;
   }   





}