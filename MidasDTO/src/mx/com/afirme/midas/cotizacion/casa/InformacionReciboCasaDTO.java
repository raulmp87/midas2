package mx.com.afirme.midas.cotizacion.casa;
// default package

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * InformacionReciboCasaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOINFORMACIONRECIBOSCASA"
    ,schema="MIDAS"
)


public class InformacionReciboCasaDTO  implements java.io.Serializable {


    // Fields    

     private static final long serialVersionUID = -2270435567725814035L;
	 private InformacionReciboCasaDTOId id;
     private String descripcionProducto;
     private BigDecimal idCentroEmisor;
     private BigDecimal numeroPoliza;
     private BigDecimal claveTipoEndoso;
     private String motivoCancelacion;
     private BigDecimal idContratante;
     private BigDecimal idAgente;
     private BigDecimal idConductoCobro;
     private BigDecimal idTcFormaPago;
     private BigDecimal idMoneda;
     private Double tipoCambio;
     private String codigoUsuario;
     private Date fechaInicioVigencia;
     private Date fechaFinVigencia;
     private BigDecimal numeroCoberturasContratadas;
     private String descripcionCobertura;
     private Integer auxiliar;
     private String centroCosto;
     private Double valorPrimaNeta;
     private Double valorBonificacionComision;
     private Double valorBonificacionComisionRPF;
     private Double valorDerechos;
     private Double valorRecargos;
     private Double valorIva;
     private Double valorPrimaTotal;
     private Double comisionPrimaNetaAgente;
     private Double comisionRecargoAgente;
     private Double comisionPrimaNetaSupervisor;
     private Double comisionRecargoSupervisor;


    // Constructors

    /** default constructor */
    public InformacionReciboCasaDTO() {
    }

	/** minimal constructor */
    public InformacionReciboCasaDTO(InformacionReciboCasaDTOId id) {
        this.id = id;
    }
    
    /** full constructor */
    public InformacionReciboCasaDTO(InformacionReciboCasaDTOId id, String descripcionProducto, BigDecimal idCentroEmisor, BigDecimal numeroPoliza, BigDecimal claveTipoEndoso, String motivoCancelacion, BigDecimal idContratante, BigDecimal idAgente, BigDecimal idConductoCobro, BigDecimal idTcFormaPago, BigDecimal idMoneda, Double tipoCambio, String codigoUsuario, Date fechaInicioVigencia, Date fechaFinVigencia, BigDecimal numeroCoberturasContratadas, String descripcionCobertura, Integer auxiliar, String centroCosto, Double valorPrimaNeta, Double valorBonificacionComision, Double valorBonificacionComisionRPF, Double valorDerechos, Double valorRecargos, Double valorIva, Double valorPrimaTotal, Double comisionPrimaNetaAgente, Double comisionRecargoAgente, Double comisionPrimaNetaSupervisor, Double comisionRecargoSupervisor) {
        this.id = id;
        this.descripcionProducto = descripcionProducto;
        this.idCentroEmisor = idCentroEmisor;
        this.numeroPoliza = numeroPoliza;
        this.claveTipoEndoso = claveTipoEndoso;
        this.motivoCancelacion = motivoCancelacion;
        this.idContratante = idContratante;
        this.idAgente = idAgente;
        this.idConductoCobro = idConductoCobro;
        this.idTcFormaPago = idTcFormaPago;
        this.idMoneda = idMoneda;
        this.tipoCambio = tipoCambio;
        this.codigoUsuario = codigoUsuario;
        this.fechaInicioVigencia = fechaInicioVigencia;
        this.fechaFinVigencia = fechaFinVigencia;
        this.numeroCoberturasContratadas = numeroCoberturasContratadas;
        this.descripcionCobertura = descripcionCobertura;
        this.auxiliar = auxiliar;
        this.centroCosto = centroCosto;
        this.valorPrimaNeta = valorPrimaNeta;
        this.valorBonificacionComision = valorBonificacionComision;
        this.valorBonificacionComisionRPF = valorBonificacionComisionRPF;
        this.valorDerechos = valorDerechos;
        this.valorRecargos = valorRecargos;
        this.valorIva = valorIva;
        this.valorPrimaTotal = valorPrimaTotal;
        this.comisionPrimaNetaAgente = comisionPrimaNetaAgente;
        this.comisionRecargoAgente = comisionRecargoAgente;
        this.comisionPrimaNetaSupervisor = comisionPrimaNetaSupervisor;
        this.comisionRecargoSupervisor = comisionRecargoSupervisor;
    }

   
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idToProducto", column=@Column(name="IDTOPRODUCTO", nullable=false, length=10) ), 
        @AttributeOverride(name="idToPoliza", column=@Column(name="IDTOPOLIZA", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="numeroRenovacion", column=@Column(name="NUMRENOVACION", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="numeroEndoso", column=@Column(name="NUMEROENDOSO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idToCotizacion", column=@Column(name="IDTOCOTIZACION", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idTipo", column=@Column(name="IDTIPO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idTcRamo", column=@Column(name="IDTCRAMO", nullable=false, length=2) ), 
        @AttributeOverride(name="idTcSubRamo", column=@Column(name="IDTCSUBRAMO", nullable=false, length=2) ), 
        @AttributeOverride(name="numeroInciso", column=@Column(name="NUMEROINCISO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idToCobertura", column=@Column(name="IDTOCOBERTURA", nullable=false, length=10) ) } )

    public InformacionReciboCasaDTOId getId() {
        return this.id;
    }
    
    public void setId(InformacionReciboCasaDTOId id) {
        this.id = id;
    }
    
    @Column(name="DESCRIPCIONPRODUCTO", length=200)

    public String getDescripcionProducto() {
        return this.descripcionProducto;
    }
    
    public void setDescripcionProducto(String descripcionProducto) {
        this.descripcionProducto = descripcionProducto;
    }
    
    @Column(name="IDCENTROEMISOR", precision=22, scale=0)

    public BigDecimal getIdCentroEmisor() {
        return this.idCentroEmisor;
    }
    
    public void setIdCentroEmisor(BigDecimal idCentroEmisor) {
        this.idCentroEmisor = idCentroEmisor;
    }
    
    @Column(name="NUMPOLIZA", precision=22, scale=0)

    public BigDecimal getNumeroPoliza() {
        return this.numeroPoliza;
    }
    
    public void setNumeroPoliza(BigDecimal numeroPoliza) {
        this.numeroPoliza = numeroPoliza;
    }
    
    @Column(name="CLAVETIPOENDOSO", precision=22, scale=0)

    public BigDecimal getClaveTipoEndoso() {
        return this.claveTipoEndoso;
    }
    
    public void setClaveTipoEndoso(BigDecimal claveTipoEndoso) {
        this.claveTipoEndoso = claveTipoEndoso;
    }
    
    @Column(name="MOTIVOCANCELACION", length=200)

    public String getMotivoCancelacion() {
        return this.motivoCancelacion;
    }
    
    public void setMotivoCancelacion(String motivoCancelacion) {
        this.motivoCancelacion = motivoCancelacion;
    }
    
    @Column(name="IDCONTRATANTE", precision=22, scale=0)

    public BigDecimal getIdContratante() {
        return this.idContratante;
    }
    
    public void setIdContratante(BigDecimal idContratante) {
        this.idContratante = idContratante;
    }
    
    @Column(name="IDAGENTE", precision=22, scale=0)

    public BigDecimal getIdAgente() {
        return this.idAgente;
    }
    
    public void setIdAgente(BigDecimal idAgente) {
        this.idAgente = idAgente;
    }
    
    @Column(name="IDCONDUCTOCOBRO", precision=22, scale=0)

    public BigDecimal getIdConductoCobro() {
        return this.idConductoCobro;
    }
    
    public void setIdConductoCobro(BigDecimal idConductoCobro) {
        this.idConductoCobro = idConductoCobro;
    }
    
    @Column(name="IDTCFORMAPAGO", precision=22, scale=0)

    public BigDecimal getIdTcFormaPago() {
        return this.idTcFormaPago;
    }
    
    public void setIdTcFormaPago(BigDecimal idTcFormaPago) {
        this.idTcFormaPago = idTcFormaPago;
    }
    
    @Column(name="IDMONEDA", precision=22, scale=0)

    public BigDecimal getIdMoneda() {
        return this.idMoneda;
    }
    
    public void setIdMoneda(BigDecimal idMoneda) {
        this.idMoneda = idMoneda;
    }
    
    @Column(name="TIPOCAMBIO", precision=16, scale=4)

    public Double getTipoCambio() {
        return this.tipoCambio;
    }
    
    public void setTipoCambio(Double tipoCambio) {
        this.tipoCambio = tipoCambio;
    }
    
    @Column(name="CODIGOUSUARIO", length=8)

    public String getCodigoUsuario() {
        return this.codigoUsuario;
    }
    
    public void setCodigoUsuario(String codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAINICIOVIGENCIA", length=7)

    public Date getFechaInicioVigencia() {
        return this.fechaInicioVigencia;
    }
    
    public void setFechaInicioVigencia(Date fechaInicioVigencia) {
        this.fechaInicioVigencia = fechaInicioVigencia;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAFINVIGENCIA", length=7)

    public Date getFechaFinVigencia() {
        return this.fechaFinVigencia;
    }
    
    public void setFechaFinVigencia(Date fechaFinVigencia) {
        this.fechaFinVigencia = fechaFinVigencia;
    }
    
    @Column(name="NUMCOBCONTRATADAS", precision=22, scale=0)

    public BigDecimal getNumeroCoberturasContratadas() {
        return this.numeroCoberturasContratadas;
    }
    
    public void setNumeroCoberturasContratadas(BigDecimal numeroCoberturasContratadas) {
        this.numeroCoberturasContratadas = numeroCoberturasContratadas;
    }
    
    @Column(name="DESCRIPCIONCOBERTURA", length=200)

    public String getDescripcionCobertura() {
        return this.descripcionCobertura;
    }
    
    public void setDescripcionCobertura(String descripcionCobertura) {
        this.descripcionCobertura = descripcionCobertura;
    }
    
    @Column(name="AUXILIAR", precision=5, scale=0)

    public Integer getAuxiliar() {
        return this.auxiliar;
    }
    
    public void setAuxiliar(Integer auxiliar) {
        this.auxiliar = auxiliar;
    }
    
    @Column(name="CENTROCOSTO", length=8)

    public String getCentroCosto() {
        return this.centroCosto;
    }
    
    public void setCentroCosto(String centroCosto) {
        this.centroCosto = centroCosto;
    }
    
    @Column(name="VALORPRIMANETA", precision=16)

    public Double getValorPrimaNeta() {
        return this.valorPrimaNeta;
    }
    
    public void setValorPrimaNeta(Double valorPrimaNeta) {
        this.valorPrimaNeta = valorPrimaNeta;
    }
    
    @Column(name="VALORBONIFCOMISION", precision=16)

    public Double getValorBonificacionComision() {
        return this.valorBonificacionComision;
    }
    
    public void setValorBonificacionComision(Double valorBonificacionComision) {
        this.valorBonificacionComision = valorBonificacionComision;
    }
    
    @Column(name="VALORBONIFCOMRECPAGOFRAC", precision=16)

    public Double getValorBonificacionComisionRPF() {
        return this.valorBonificacionComisionRPF;
    }
    
    public void setValorBonificacionComisionRPF(Double valorBonificacionComisionRPF) {
        this.valorBonificacionComisionRPF = valorBonificacionComisionRPF;
    }
    
    @Column(name="VALORDERECHOS", precision=16)

    public Double getValorDerechos() {
        return this.valorDerechos;
    }
    
    public void setValorDerechos(Double valorDerechos) {
        this.valorDerechos = valorDerechos;
    }
    
    @Column(name="VALORRECARGOS", precision=16)

    public Double getValorRecargos() {
        return this.valorRecargos;
    }
    
    public void setValorRecargos(Double valorRecargos) {
        this.valorRecargos = valorRecargos;
    }
    
    @Column(name="VALORIVA", precision=16)

    public Double getValorIva() {
        return this.valorIva;
    }
    
    public void setValorIva(Double valorIva) {
        this.valorIva = valorIva;
    }
    
    @Column(name="VALORPRIMATOTAL", precision=16)

    public Double getValorPrimaTotal() {
        return this.valorPrimaTotal;
    }
    
    public void setValorPrimaTotal(Double valorPrimaTotal) {
        this.valorPrimaTotal = valorPrimaTotal;
    }
    
    @Column(name="COMISIONPNAGT", precision=16)

    public Double getComisionPrimaNetaAgente() {
        return this.comisionPrimaNetaAgente;
    }
    
    public void setComisionPrimaNetaAgente(Double comisionPrimaNetaAgente) {
        this.comisionPrimaNetaAgente = comisionPrimaNetaAgente;
    }
    
    @Column(name="COMISIONRCGAGT", precision=16)

    public Double getComisionRecargoAgente() {
        return this.comisionRecargoAgente;
    }
    
    public void setComisionRecargoAgente(Double comisionRecargoAgente) {
        this.comisionRecargoAgente = comisionRecargoAgente;
    }
    
    @Column(name="COMISIONPNSUP", precision=16)

    public Double getComisionPrimaNetaSupervisor() {
        return this.comisionPrimaNetaSupervisor;
    }
    
    public void setComisionPrimaNetaSupervisor(Double comisionPrimaNetaSupervisor) {
        this.comisionPrimaNetaSupervisor = comisionPrimaNetaSupervisor;
    }
    
    @Column(name="COMISIONRCGSUP", precision=16)

    public Double getComisionRecargoSupervisor() {
        return this.comisionRecargoSupervisor;
    }
    
    public void setComisionRecargoSupervisor(Double comisionRecargoSupervisor) {
        this.comisionRecargoSupervisor = comisionRecargoSupervisor;
    }
   








}