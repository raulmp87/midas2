package mx.com.afirme.midas.cotizacion.casa;
// default package

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for InformacionReciboCasaDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface InformacionReciboCasaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved InformacionReciboCasaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity InformacionReciboCasaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(InformacionReciboCasaDTO entity);
    /**
	 Delete a persistent InformacionReciboCasaDTO entity.
	  @param entity InformacionReciboCasaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(InformacionReciboCasaDTO entity);
   /**
	 Persist a previously saved InformacionReciboCasaDTO entity and return it or a copy of it to the sender. 
	 A copy of the InformacionReciboCasaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity InformacionReciboCasaDTO entity to update
	 @return InformacionReciboCasaDTO the persisted InformacionReciboCasaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public InformacionReciboCasaDTO update(InformacionReciboCasaDTO entity);
	public InformacionReciboCasaDTO findById( InformacionReciboCasaDTOId id);
	 /**
	 * Find all InformacionReciboCasaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the InformacionReciboCasaDTO property to query
	  @param value the property value to match
	  	  @return List<InformacionReciboCasaDTO> found by query
	 */
	public List<InformacionReciboCasaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all InformacionReciboCasaDTO entities.
	  	  @return List<InformacionReciboCasaDTO> all InformacionReciboCasaDTO entities
	 */
	public List<InformacionReciboCasaDTO> findAll(
		);	
}