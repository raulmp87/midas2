package mx.com.afirme.midas.cotizacion.casa;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.ConfiguracionDatoIncisoCotizacionDTO;
import mx.com.afirme.midas.direccion.DireccionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;


public interface CotizacionCasaFacadeRemote {

	
	public CotizacionDTO crearActualizarCotizacionCasa (ClienteDTO contratante, ClienteDTO asegurado, DireccionDTO direccionBien, 
			FormaPagoDTO formaPago,	Double porcentajeIva, String nombreBeneficiario, AgenteDTO agente, 
			List<ConfiguracionDatoIncisoCotizacionDTO> datosInciso, String[] datos, BigDecimal idCotizacion);
		
	
	public void prepararEmisionCasa(CotizacionDTO cotizacion);
	
	
}
