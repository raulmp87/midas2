package mx.com.afirme.midas.cotizacion.diasgracia;

import javax.ejb.Remote;


public interface DiasGraciaServicio {

	public Integer obtenerDiasGraciaDefault();
}
