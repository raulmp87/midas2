package mx.com.afirme.midas.cotizacion.primerriesgoluc;

// default package

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;

/**
 * Remote interface for AgrupacionCotDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface AgrupacionCotFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved AgrupacionCotDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            AgrupacionCotDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(AgrupacionCotDTO entity);

	/**
	 * Delete a persistent AgrupacionCotDTO entity.
	 * 
	 * @param entity
	 *            AgrupacionCotDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(AgrupacionCotDTO entity);

	/**
	 * Persist a previously saved AgrupacionCotDTO entity and return it or a
	 * copy of it to the sender. A copy of the AgrupacionCotDTO entity parameter
	 * is returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            AgrupacionCotDTO entity to update
	 * @return AgrupacionCotDTO the persisted AgrupacionCotDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public AgrupacionCotDTO update(AgrupacionCotDTO entity);

	public AgrupacionCotDTO findById(AgrupacionCotId id);

	/**
	 * Find all AgrupacionCotDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the AgrupacionCotDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<AgrupacionCotDTO> found by query
	 */
	public List<AgrupacionCotDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all AgrupacionCotDTO entities.
	 * 
	 * @return List<AgrupacionCotDTO> all AgrupacionCotDTO entities
	 */
	public List<AgrupacionCotDTO> findAll();

	public Short maxAgrupacion(BigDecimal idToCotizacion);

	public AgrupacionCotDTO buscarPorCotizacion(BigDecimal idToCotizacion,
			Short claveTipoAgrupacion);

	public List<AgrupacionCotDTO> buscarPorCotizacionAgrupacion(BigDecimal idToCotizacion,
			Short claveTipoAgrupacion);

	public AgrupacionCotDTO buscarPorCotizacionSeccion(
			BigDecimal idToCotizacion, Short claveTipoAgrupacion,
			BigDecimal idToSeccion);

	public void borrarAgrupaciones(BigDecimal idToCotizacion);

	public List<AgrupacionCotDTO> listarFiltrado(AgrupacionCotDTO dto);	
	
	public void borrarAgrupacion(BigDecimal idToCotizacion, BigDecimal numeroAgrupacion,  BigDecimal idToSeccion);
	
	public List<SeccionDTO> listarSeccionesConAgrupacionPrimerRiesgo(BigDecimal idToCotizacion);
	
	/**
	 * Lista las agrupaciones de tipo LUC de una cotizaci�n que est�n mal registradas en al menos una cobertura. 
	 * (coberturas que tengan numeroAgrupacion distinto al de la agrupaci�n que le corresponda) 
	 * @param idToCotizacion
	 * @return List<AgrupacionCotDTO> entidades AgrupacionCotDTO encontradas por el query.
	 */	
	public List<AgrupacionCotDTO> listarAgrupacionesLUCMalRegistradas(BigDecimal idToCotizacion);
	
	/**
	 * Lista las agrupaciones existentes en una cotizaci�n (idToCotizacionAgrExistente), pero que no se encuentran en otra (idToCotizacionAgrInexistente).  
	 * @param idToCotizacionAgrExistente
     * @param idToCotizacionAgrInexistente 
	 * @return List<AgrupacionCotDTO> entidades AgrupacionCotDTO encontradas por el query.
	 */	
	public List<AgrupacionCotDTO> listarAgrupacionesExcluyentes(BigDecimal idToCotizacionAgrExistente, BigDecimal idToCotizacionAgrInexistente);
	
	public List<AgrupacionCotDTO> listarPorCotizacion(BigDecimal idToCotizacion);
}