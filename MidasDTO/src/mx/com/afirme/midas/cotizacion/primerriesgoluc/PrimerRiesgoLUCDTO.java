package mx.com.afirme.midas.cotizacion.primerriesgoluc;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;

public class PrimerRiesgoLUCDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idCotizacion;
	private Double totalSecciones;
	private Double totalSeccionesPrimerRiesgo;
	private Double sumaAseguradaPrimerRiesgo;
	private Integer size;
	private List<SeccionCotizacionDTO> seccionesPrimerRiesgo = new ArrayList<SeccionCotizacionDTO>();
	private List<SeccionCotizacionDTO> seccionesLUC = new ArrayList<SeccionCotizacionDTO>();

	public BigDecimal getIdCotizacion() {
		return idCotizacion;
	}

	public void setIdCotizacion(BigDecimal idCotizacion) {
		this.idCotizacion = idCotizacion;
	}

	public Double getTotalSecciones() {
		return totalSecciones;
	}

	public void setTotalSecciones(Double totalSecciones) {
		this.totalSecciones = totalSecciones;
	}

	public Double getTotalSeccionesPrimerRiesgo() {
		return totalSeccionesPrimerRiesgo;
	}

	public void setTotalSeccionesPrimerRiesgo(Double totalSeccionesPrimerRiesgo) {
		this.totalSeccionesPrimerRiesgo = totalSeccionesPrimerRiesgo;
	}

	public Double getSumaAseguradaPrimerRiesgo() {
		return sumaAseguradaPrimerRiesgo;
	}

	public void setSumaAseguradaPrimerRiesgo(Double sumaAseguradaPrimerRiesgo) {
		this.sumaAseguradaPrimerRiesgo = sumaAseguradaPrimerRiesgo;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public List<SeccionCotizacionDTO> getSeccionesPrimerRiesgo() {
		return seccionesPrimerRiesgo;
	}

	public void setSeccionesPrimerRiesgo(
			List<SeccionCotizacionDTO> seccionesPrimerRiesgo) {
		this.seccionesPrimerRiesgo = seccionesPrimerRiesgo;
	}

	public List<SeccionCotizacionDTO> getSeccionesLUC() {
		return seccionesLUC;
	}

	public void setSeccionesLUC(List<SeccionCotizacionDTO> seccionesLUC) {
		this.seccionesLUC = seccionesLUC;
	}
}
