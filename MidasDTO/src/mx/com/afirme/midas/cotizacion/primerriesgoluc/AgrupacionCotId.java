package mx.com.afirme.midas.cotizacion.primerriesgoluc;

// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * AgrupacionCotDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class AgrupacionCotId implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields

	private BigDecimal idToCotizacion;
	private Short numeroAgrupacion;

	// Constructors

	/** default constructor */
	public AgrupacionCotId() {
	}

	// Property accessors

	@Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCotizacion() {
		return this.idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	@Column(name = "NumeroAgrupacion", nullable = false, precision = 4, scale = 0)
	public Short getNumeroAgrupacion() {
		return this.numeroAgrupacion;
	}

	public void setNumeroAgrupacion(Short numeroAgrupacion) {
		this.numeroAgrupacion = numeroAgrupacion;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof AgrupacionCotId))
			return false;
		AgrupacionCotId castOther = (AgrupacionCotId) other;

		return ((this.getIdToCotizacion() == castOther.getIdToCotizacion()) || (this
				.getIdToCotizacion() != null
				&& castOther.getIdToCotizacion() != null && this
				.getIdToCotizacion().equals(castOther.getIdToCotizacion())))
				&& ((this.getNumeroAgrupacion() == castOther
						.getNumeroAgrupacion()) || (this.getNumeroAgrupacion() != null
						&& castOther.getNumeroAgrupacion() != null && this
						.getNumeroAgrupacion().equals(
								castOther.getNumeroAgrupacion())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToCotizacion() == null ? 0 : this.getIdToCotizacion()
						.hashCode());
		result = 37
				* result
				+ (getNumeroAgrupacion() == null ? 0 : this
						.getNumeroAgrupacion().hashCode());
		return result;
	}

}