package mx.com.afirme.midas.cotizacion.primerriesgoluc;

// default package

import java.math.BigDecimal;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;

/**
 * AgrupacionCotDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOAGRUPACIONCOT", schema = "MIDAS")
public class AgrupacionCotDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private AgrupacionCotId id;
	private CotizacionDTO cotizacionDTO;
	private Short claveTipoAgrupacion = 1;
	private BigDecimal idToSeccion;
	private Double valorSumaAsegurada = 0D;
	private Double valorCuota = 0D;
	private Double valorPrimaneta = 0D;
	private String descripcionSeccion;

	// Constructors

	/** default constructor */
	public AgrupacionCotDTO() {
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToCotizacion", column = @Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroAgrupacion", column = @Column(name = "NUMEROAGRUPACION", nullable = false, precision = 4, scale = 0)) })
	public AgrupacionCotId getId() {
		return this.id;
	}

	public void setId(AgrupacionCotId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTOCOTIZACION", nullable = false, insertable = false, updatable = false)
	public CotizacionDTO getCotizacionDTO() {
		return this.cotizacionDTO;
	}

	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}

	@Column(name = "ClaveTipoAgrupacion", nullable = false, precision = 4, scale = 0)
	public Short getClaveTipoAgrupacion() {
		return this.claveTipoAgrupacion;
	}

	public void setClaveTipoAgrupacion(Short claveTipoAgrupacion) {
		this.claveTipoAgrupacion = claveTipoAgrupacion;
	}

	@Column(name = "IdToSeccion", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSeccion() {
		return this.idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	@Column(name = "ValorSumaAsegurada", nullable = false, precision = 16)
	public Double getValorSumaAsegurada() {
		return this.valorSumaAsegurada;
	}

	public void setValorSumaAsegurada(Double valorSumaAsegurada) {
		this.valorSumaAsegurada = valorSumaAsegurada;
	}

	@Column(name = "ValorCuota", nullable = false, precision = 16, scale = 10)
	public Double getValorCuota() {
		return this.valorCuota;
	}

	public void setValorCuota(Double valorCuota) {
		this.valorCuota = valorCuota;
	}

	@Column(name = "ValorPrimaneta", nullable = false, precision = 16)
	public Double getValorPrimaneta() {
		return this.valorPrimaneta;
	}

	public void setValorPrimaneta(Double valorPrimaneta) {
		this.valorPrimaneta = valorPrimaneta;
	}

	@Transient
	public String getDescripcionSeccion() {
		return descripcionSeccion;
	}

	public void setDescripcionSeccion(String descripcionSeccion) {
		this.descripcionSeccion = descripcionSeccion;
	}

}