package mx.com.afirme.midas.cotizacion.inciso.cargamasiva;

// default package

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author JORGEKNO
 */
@Entity
@Table(name = "TOCARGAMASIVADETALLECOT", schema = "MIDAS")
public class CargaMasivaDetalleCotDTO implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	// Fields

	private CargaMasivaDetalleCotId id;
	private CargaMasivaCotDTO cargaMasivaCotDTO;
	private String nombreCalle;
	private String numeroExterior;
	private String numeroInterior;
	private String codigoPostal;
	private String nombreEstadoOriginal;
	private String nombreEstadoValido;
	private String nombreMunicipioOriginal;
	private String nombreMunicipioValido;
	private String nombreColoniaOriginal;
	private String nombreColoniaValida;
	private Short claveEstatus;
	private String mensajeError;
	private String sumaAseguradaSecRgo;
	private String idColonia;

	// Constructors

	/** default constructor */
	public CargaMasivaDetalleCotDTO() {
	}

	/** minimal constructor */
	public CargaMasivaDetalleCotDTO(CargaMasivaDetalleCotId id,
			String nombreCalle, String numeroExterior, String codigoPostal,
			String nombreEstadoOriginal, String nombreEstadoValido,
			String nombreMunicipioOriginal, String nombreMunicipioValido,
			String nombreColoniaOriginal, String nombreColoniaValida,
			Short claveEstatus, String mensajeError, String sumaAseguradaSecRgo) {
		this.id = id;
		this.nombreCalle = nombreCalle;
		this.numeroExterior = numeroExterior;
		this.codigoPostal = codigoPostal;
		this.nombreEstadoOriginal = nombreEstadoOriginal;
		this.nombreEstadoValido = nombreEstadoValido;
		this.nombreMunicipioOriginal = nombreMunicipioOriginal;
		this.nombreMunicipioValido = nombreMunicipioValido;
		this.nombreColoniaOriginal = nombreColoniaOriginal;
		this.nombreColoniaValida = nombreColoniaValida;
		this.claveEstatus = claveEstatus;
		this.mensajeError = mensajeError;
		this.sumaAseguradaSecRgo = sumaAseguradaSecRgo;
	}

	/** full constructor */
	public CargaMasivaDetalleCotDTO(CargaMasivaDetalleCotId id,
			String nombreCalle, String numeroExterior, String numeroInterior,
			String codigoPostal, String nombreEstadoOriginal,
			String nombreEstadoValido, String nombreMunicipioOriginal,
			String nombreMunicipioValido, String nombreColoniaOriginal,
			String nombreColoniaValida, Short claveEstatus,
			String mensajeError, String sumaAseguradaSecRgo) {
		this.id = id;
		this.nombreCalle = nombreCalle;
		this.numeroExterior = numeroExterior;
		this.numeroInterior = numeroInterior;
		this.codigoPostal = codigoPostal;
		this.nombreEstadoOriginal = nombreEstadoOriginal;
		this.nombreEstadoValido = nombreEstadoValido;
		this.nombreMunicipioOriginal = nombreMunicipioOriginal;
		this.nombreMunicipioValido = nombreMunicipioValido;
		this.nombreColoniaOriginal = nombreColoniaOriginal;
		this.nombreColoniaValida = nombreColoniaValida;
		this.claveEstatus = claveEstatus;
		this.mensajeError = mensajeError;
		this.sumaAseguradaSecRgo = sumaAseguradaSecRgo;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToCotizacion", column = @Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroInciso", column = @Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)) })
	public CargaMasivaDetalleCotId getId() {
		return this.id;
	}

	public void setId(CargaMasivaDetalleCotId id) {
		this.id = id;
	}

	@Column(name = "NOMBRECALLE", nullable = false, length = 100)
	public String getNombreCalle() {
		return this.nombreCalle;
	}

	public void setNombreCalle(String nombreCalle) {
		this.nombreCalle = nombreCalle;
	}

	@Column(name = "NUMEROEXTERIOR", nullable = false, length = 10)
	public String getNumeroExterior() {
		return this.numeroExterior;
	}

	public void setNumeroExterior(String numeroExterior) {
		this.numeroExterior = numeroExterior;
	}

	@Column(name = "NUMEROINTERIOR", length = 10)
	public String getNumeroInterior() {
		return this.numeroInterior;
	}

	public void setNumeroInterior(String numeroInterior) {
		this.numeroInterior = numeroInterior;
	}

	@Column(name = "CODIGOPOSTAL", nullable = false, precision = 22, scale = 0)
	public String getCodigoPostal() {
		return this.codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	@Column(name = "NOMBREESTADOORIGINAL", nullable = false, length = 100)
	public String getNombreEstadoOriginal() {
		return this.nombreEstadoOriginal;
	}

	public void setNombreEstadoOriginal(String nombreEstadoOriginal) {
		this.nombreEstadoOriginal = nombreEstadoOriginal;
	}

	@Column(name = "NOMBREESTADOVALIDO", nullable = false, length = 100)
	public String getNombreEstadoValido() {
		return this.nombreEstadoValido;
	}

	public void setNombreEstadoValido(String nombreEstadoValido) {
		this.nombreEstadoValido = nombreEstadoValido;
	}

	@Column(name = "NOMBREMUNICIPIOORIGINAL", nullable = false, length = 100)
	public String getNombreMunicipioOriginal() {
		return this.nombreMunicipioOriginal;
	}

	public void setNombreMunicipioOriginal(String nombreMunicipioOriginal) {
		this.nombreMunicipioOriginal = nombreMunicipioOriginal;
	}

	@Column(name = "NOMBREMUNICIPIOVALIDO", nullable = false, length = 100)
	public String getNombreMunicipioValido() {
		return this.nombreMunicipioValido;
	}

	public void setNombreMunicipioValido(String nombreMunicipioValido) {
		this.nombreMunicipioValido = nombreMunicipioValido;
	}

	@Column(name = "NOMBRECOLONIAORIGINAL", nullable = false, length = 100)
	public String getNombreColoniaOriginal() {
		return this.nombreColoniaOriginal;
	}

	public void setNombreColoniaOriginal(String nombreColoniaOriginal) {
		this.nombreColoniaOriginal = nombreColoniaOriginal;
	}

	@Column(name = "NOMBRECOLONIAVALIDA", nullable = false, length = 100)
	public String getNombreColoniaValida() {
		return this.nombreColoniaValida;
	}

	public void setNombreColoniaValida(String nombreColoniaValida) {
		this.nombreColoniaValida = nombreColoniaValida;
	}

	@Column(name = "CLAVEESTATUS", nullable = false, precision = 4, scale = 0)
	public Short getClaveEstatus() {
		return this.claveEstatus;
	}

	public void setClaveEstatus(Short claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	@Column(name = "MENSAJEERROR", nullable = false, length = 100)
	public String getMensajeError() {
		return this.mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	@Column(name = "SUMAASEGURADASECRGO", nullable = false, length = 2000)
	public String getSumaAseguradaSecRgo() {
		return this.sumaAseguradaSecRgo;
	}

	public void setSumaAseguradaSecRgo(String sumaAseguradaSecRgo) {
		this.sumaAseguradaSecRgo = sumaAseguradaSecRgo;
	}

	@Column(name = "IDCOLONIA")
	public String getIdColonia() {
		return idColonia;
	}

	public void setIdColonia(String idColonia) {
		this.idColonia = idColonia;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTOCOTIZACION", nullable = false, insertable = false, updatable = false)
	public CargaMasivaCotDTO getCargaMasivaCotDTO() {
		return cargaMasivaCotDTO;
	}

	public void setCargaMasivaCotDTO(CargaMasivaCotDTO cargaMasivaCotDTO) {
		this.cargaMasivaCotDTO = cargaMasivaCotDTO;
	}

}