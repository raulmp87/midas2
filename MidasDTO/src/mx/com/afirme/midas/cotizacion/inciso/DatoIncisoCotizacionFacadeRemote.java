package mx.com.afirme.midas.cotizacion.inciso;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.catalogos.equipoelectronico.SubtipoEquipoElectronicoDTO;
import mx.com.afirme.midas.catalogos.giro.SubGiroDTO;
import mx.com.afirme.midas.catalogos.girorc.SubGiroRCDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.tipoequipocontratista.SubtipoEquipoContratistaDTO;
import mx.com.afirme.midas.catalogos.tipomaquinaria.SubTipoMaquinariaDTO;
import mx.com.afirme.midas.catalogos.tipomontajemaquina.SubtipoMontajeMaquinaDTO;
import mx.com.afirme.midas.catalogos.tipomuro.TipoMuroDTO;
import mx.com.afirme.midas.catalogos.tipoobracivil.TipoObraCivilDTO;
import mx.com.afirme.midas.catalogos.tipotecho.TipoTechoDTO;
import mx.com.afirme.midas.sistema.SystemException;

/**
 * Remote interface for DatoIncisoCotizacionDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface DatoIncisoCotizacionFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved DatoIncisoCotizacionDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            DatoIncisoCotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(DatoIncisoCotizacionDTO entity);

	/**
	 * Delete a persistent DatoIncisoCotizacionDTO entity.
	 * 
	 * @param entity
	 *            DatoIncisoCotizacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(DatoIncisoCotizacionDTO entity);

	/**
	 * Persist a previously saved DatoIncisoCotizacionDTO entity and return it
	 * or a copy of it to the sender. A copy of the DatoIncisoCotizacionDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            DatoIncisoCotizacionDTO entity to update
	 * @return DatoIncisoCotizacionDTO the persisted DatoIncisoCotizacionDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public DatoIncisoCotizacionDTO update(DatoIncisoCotizacionDTO entity);

	public DatoIncisoCotizacionDTO findById(DatoIncisoCotizacionId id);

	/**
	 * Find all DatoIncisoCotizacionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the DatoIncisoCotizacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<DatoIncisoCotizacionDTO> found by query
	 */
	public List<DatoIncisoCotizacionDTO> findByProperty(String propertyName,
			Object value);

	public List<DatoIncisoCotizacionDTO> findByValor(Object valor);

	/**
	 * Find all DatoIncisoCotizacionDTO entities.
	 * 
	 * @return List<DatoIncisoCotizacionDTO> all DatoIncisoCotizacionDTO
	 *         entities
	 */
	public List<DatoIncisoCotizacionDTO> findAll();

	public void deleteAll(BigDecimal idToCotizacion, BigDecimal numeroInciso);
	
	/**
	 * Filtra los registros de DatoIncisoCotizacionDTO en base a los atributos recibidos en el objeto DatoIncisoCotizacionId.
	 * Los atributos enviados en la consulta son: idToCotizacion, numeroInciso, idToSeccion, idToCobertura, idToRiesgo,
	 * numeroSubinciso, idTcRamo, idTcSubramo, claveDetalle, idDato. Los atributos que se reciben con valor de null son ignorados.
	 * @param DatoIncisoCotizacionId idDatoInciso
	 */
	public List<DatoIncisoCotizacionDTO> listarPorIdFiltrado(DatoIncisoCotizacionId idDatoInciso);
	
	public List<DatoIncisoCotizacionDTO> getDatosRamoInciso(BigDecimal idToCotizacion, BigDecimal numeroInciso);
	
	public List<DatoIncisoCotizacionDTO> getDatosRiesgoInciso(BigDecimal idToCotizacion, BigDecimal numeroInciso);
	
	/**
	 * Obtiene el registro de la tabla DatoIncisoCotizacion que contiene el n�mero de pisos del inciso cuyos atributos se reciben.
	 * El n�mero de pisos se recupera del campo "valor" de la tabla DatoIncisoCotizacion
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @return String numero de pisos del inciso, si no se encuentra el registro, regresa "No disponible";
	 */
	public String obtenerNumeroPisos(BigDecimal idToCotizacion,BigDecimal numeroInciso);
	
	/**
	 * Obtiene el objeto TipoMuro correspondiente a un inciso, utilizando la configuraci�n adecuada para la tabla DatoIncisoCotizacion
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @return TipoMuroDTO. Entidad tipoMuroDTO correspondiente al inciso, en base al valor del registro DatoInciso encontrado. Si no se encuentra el 
	 * registro, devuelve null.
	 */
	public TipoMuroDTO obtenerTipoMuro(BigDecimal idToCotizacion,BigDecimal numeroInciso);
	
	/**
	 * Obtiene el objeto TipoTecho correspondiente a un inciso, utilizando la configuraci�n adecuada para la tabla DatoIncisoCotizacion
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @return TipoTechoDTO. Entidad tipoTechoDTO correspondiente al inciso, en base al valor del registro DatoInciso encontrado. Si no se encuentra el 
	 * registro, devuelve null.
	 */
	public TipoTechoDTO obtenerTipoTecho(BigDecimal idToCotizacion,BigDecimal numeroInciso);
	
	public List<DatoIncisoCotizacionDTO> listarPorIdToCotizacion(BigDecimal idToCotizacion);
	
	/**
	 * Regresa el subtipo correspondiente al SubRamoDTO recibido. los Subtipos considerados son los siguientes: subGiro, subGiroRC, 
	 * SubTipoMaquinaria, SubTipoMontajeMaquina, SubTipoEquipoElectronico, SubTipoEquipoContratista y tipoObraCivil 
	 * @return Object el objeto de los datos de riesgo correspondiente al subramo recibido de la cotizaci�n e inciso recibidos. null en caso de error
	 * @throws SystemException. En caso de no encontrar el datoIncisoCotizacion, o de no encontrar la entidad a la que hace referencia el DatoIncisoCotizacion 
	 */
	public Object obtenerDTOSubTipoCorrespondienteAlSubRamo(SubRamoDTO subRamoDTO,BigDecimal idToCotizacion, BigDecimal numeroInciso) throws SystemException;
	
	/**
	 * Regresa el SubGiroDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubGiroDTO. El sub giro del inciso, null en caso de no encontrar el registro.
	 */
	public SubGiroDTO obtenerSubgiro(BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idTcRamo) throws SystemException;
	
	/**
	 * Regresa el SubGiroRCDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubGiroRCDTO. El SubGiroRCDTO del inciso, null en caso de no encontrar el registro.
	 */
	public SubGiroRCDTO obtenerSubGiroRC(BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idTcRamo) throws SystemException;
	
	/**
	 * Regresa el SubTipoMaquinariaDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubTipoMaquinariaDTO. El SubTipoMaquinariaDTO del inciso, null en caso de no encontrar el registro.
	 */
	public SubTipoMaquinariaDTO obtenerSubTipoMaquinaria(BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idTcRamo) throws SystemException;
	
	/**
	 * Regresa el SubTipoMaquinariaDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubTipoMaquinariaDTO. El SubTipoMaquinariaDTO del inciso, null en caso de no encontrar el registro.
	 */
	public SubTipoMaquinariaDTO obtenerSubTipoMaquinaria(BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idTcRamo,
			BigDecimal numeroSubInciso,BigDecimal idToSeccion) throws SystemException;
			
	/**
	 * Regresa el SubtipoMontajeMaquinaDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubtipoMontajeMaquinaDTO. El SubtipoMontajeMaquinaDTO del inciso, null en caso de no encontrar el registro.
	 */
	public SubtipoMontajeMaquinaDTO obtenerSubTipoMontajeMaquina (BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idTcRamo) throws SystemException;
	
	/**
	 * Regresa el SubtipoEquipoElectronicoDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubtipoEquipoElectronicoDTO. El SubtipoEquipoElectronicoDTO del inciso, null en caso de no encontrar el registro.
	 */
	public SubtipoEquipoElectronicoDTO obtenerSubtipoEquipoElectronico (BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idTcRamo) throws SystemException;
	
	/**
	 * Regresa el SubtipoEquipoElectronicoDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubtipoEquipoElectronicoDTO. El SubtipoEquipoElectronicoDTO del inciso, null en caso de no encontrar el registro.
	 */
	public SubtipoEquipoElectronicoDTO obtenerSubtipoEquipoElectronico (BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idTcRamo,
			BigDecimal numeroSubInciso,BigDecimal idToSeccion) throws SystemException;
	
	/**
	 * Regresa el SubtipoEquipoContratistaDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubtipoEquipoContratistaDTO. El SubtipoEquipoContratistaDTO del inciso, null en caso de no encontrar el registro.
	 */
	public SubtipoEquipoContratistaDTO obtenerSubtipoEquipoContratista (BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idTcRamo) throws SystemException;
	
	/**
	 * Regresa el SubtipoEquipoContratistaDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return SubtipoEquipoContratistaDTO. El SubtipoEquipoContratistaDTO del inciso, null en caso de no encontrar el registro.
	 */
	public SubtipoEquipoContratistaDTO obtenerSubtipoEquipoContratista (BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idTcRamo,
			BigDecimal numeroSubInciso,BigDecimal idToSeccion) throws SystemException;
	
	/**
	 * Regresa el TipoObraCivilDTO correspondiente al inciso cuyos datos se reciben. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idTcRamo
	 * @return TipoObraCivilDTO. El TipoObraCivilDTO del inciso, null en caso de no encontrar el registro.
	 */
	public TipoObraCivilDTO obtenerTipoObraCivil (BigDecimal idToCotizacion, BigDecimal numeroInciso,
			BigDecimal idTcRamo)throws SystemException;
}