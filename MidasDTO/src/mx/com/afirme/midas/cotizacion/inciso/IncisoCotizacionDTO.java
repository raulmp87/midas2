package mx.com.afirme.midas.cotizacion.inciso;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.base.PaginadoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.direccion.DireccionDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.Inciso;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;

/**
 * IncisoCotizacionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOINCISOCOT", schema = "MIDAS")
public class IncisoCotizacionDTO extends PaginadoDTO implements java.io.Serializable, Entidad {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -2822812277501578961L;
	private IncisoCotizacionId id;
	private CotizacionDTO cotizacionDTO = new CotizacionDTO();
	private DireccionDTO direccionDTO;	
	private Short claveEstatusInspeccion;
	private Short claveTipoOrigenInspeccion;
	private Short claveMensajeInspeccion;
	private Short claveAutInspeccion;
	private String codigoUsuarioEstInspeccion;
	private Double valorPrimaNeta;
	private Double valorPrimaTotal;
	private Boolean igualacionNivelInciso = Boolean.FALSE;
	private Double primaTotalAntesDeIgualacion;
	private Double descuentoIgualacionPrimas;
	
	private BigDecimal idClienteCob;
	private BigDecimal idMedioPago;
	private Long idConductoCobroCliente;
	private String numeroTarjetaCobranza;
	private String descripcionMedioPago;
	
	private Boolean primerReciboPagado;
	private String numeroAutorizacion;
	private String numeroPrimerRecibo;
	private String digitoVerificadorRecibo;
	
	private String emailContacto;
	
	private String conductoCobro;
	private String institucionBancaria;
	private String tipoTarjeta;
	private String numeroTarjetaClave;
	private String codigoSeguridad;
	private String fechaVencimiento;
	

	private List<SeccionCotizacionDTO> seccionCotizacionList = new ArrayList<SeccionCotizacionDTO>(1);

	private String descripcionGiroAsegurado;
	
	private Long numeroSecuencia;
	private IncisoAutoCot incisoAutoCot = new IncisoAutoCot();

	private BigDecimal idToSeccion;// Transient 
	
	private Boolean soloCotizados;
	
	private BigDecimal idToPoliza;
	
	//CondicionesEspeciales
	private List<CondicionEspecial> condicionesEspeciales = new ArrayList<CondicionEspecial>(1);
	
	// Constructors
	/** default constructor */
	public IncisoCotizacionDTO() {
	}
	
	public IncisoCotizacionDTO(Inciso inciso) {
		this.id = new IncisoCotizacionId();
		if(inciso.getAutoInciso() != null){
			this.incisoAutoCot = new IncisoAutoCot(inciso.getAutoInciso());
		}
		this.claveAutInspeccion = inciso.getClaveAutInspeccion();
		this.claveEstatusInspeccion = inciso.getClaveEstatusInspeccion();
		this.claveMensajeInspeccion = inciso.getClaveMensajeInspeccion();
		this.claveTipoOrigenInspeccion = inciso.getClaveTipoOrigenInspeccion();
		this.codigoUsuarioEstInspeccion = inciso.getCodigoUsuarioEstInspeccion();
		this.descripcionGiroAsegurado = inciso.getDescripcionGiroAsegurado();
		this.direccionDTO = inciso.getDireccion();
		//inciso.getFechaAutInspeccion();
		//inciso.getFechaEstatusInspeccion();
		//inciso.getFechaSolAutInspeccion();
		this.numeroSecuencia = inciso.getNumeroSecuencia().longValue();
		this.valorPrimaNeta = inciso.getValorPrimaNeta();
		this.idClienteCob = inciso.getIdClienteCob();
		this.idConductoCobroCliente = inciso.getIdConductoCobroCliente();
		this.idMedioPago = inciso.getIdMedioPago();
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToCotizacion", column = @Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroInciso", column = @Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)) })
	public IncisoCotizacionId getId() {
		return this.id;
	}

	public void setId(IncisoCotizacionId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOCOTIZACION", nullable = false, insertable = false, updatable = false)
	public CotizacionDTO getCotizacionDTO() {
		return this.cotizacionDTO;
	}

	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}

	@Column(name = "CLAVEESTATUSINSPECCION", nullable = false, precision = 4, scale = 0)
	public Short getClaveEstatusInspeccion() {
		return this.claveEstatusInspeccion;
	}

	public void setClaveEstatusInspeccion(Short claveEstatusInspeccion) {
		this.claveEstatusInspeccion = claveEstatusInspeccion;
	}

	@Column(name = "CLAVEMENSAJEINSPECCION", nullable = false, precision = 4, scale = 0)
	public Short getClaveMensajeInspeccion() {
		return this.claveMensajeInspeccion;
	}

	public void setClaveMensajeInspeccion(Short claveMensajeInspeccion) {
		this.claveMensajeInspeccion = claveMensajeInspeccion;
	}

	@Column(name = "CLAVEAUTINSPECCION", nullable = false, precision = 4, scale = 0)
	public Short getClaveAutInspeccion() {
		return this.claveAutInspeccion;
	}

	public void setClaveAutInspeccion(Short claveAutInspeccion) {
		this.claveAutInspeccion = claveAutInspeccion;
	}

	@Column(name = "CLAVETIPOORIGENINSPECCION", nullable = false, precision = 4, scale = 0)
	public Short getClaveTipoOrigenInspeccion() {
		return this.claveTipoOrigenInspeccion;
	}

	public void setClaveTipoOrigenInspeccion(Short claveTipoOrigenInspeccion) {
		this.claveTipoOrigenInspeccion = claveTipoOrigenInspeccion;
	}

	@Column(name = "CODIGOUSUARIOESTINSPECCION", length = 8)
	public String getCodigoUsuarioEstInspeccion() {
		return this.codigoUsuarioEstInspeccion;
	}

	public void setCodigoUsuarioEstInspeccion(String codigoUsuarioEstInspeccion) {
		this.codigoUsuarioEstInspeccion = codigoUsuarioEstInspeccion;
	}

	@Column(name = "VALORPRIMANETA", nullable = false, precision = 16)
	public Double getValorPrimaNeta() {
		return this.valorPrimaNeta;
	}

	public void setValorPrimaNeta(Double valorPrimaNeta) {
		this.valorPrimaNeta = valorPrimaNeta;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTODIRECCIONINCISO")
	public DireccionDTO getDireccionDTO() {
		return direccionDTO;
	}

	public void setDireccionDTO(DireccionDTO direccionDTO) {
		this.direccionDTO = direccionDTO;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "incisoCotizacionDTO")
	public List<SeccionCotizacionDTO> getSeccionCotizacionList() {
		return seccionCotizacionList;
	}

	public void setSeccionCotizacionList(
			List<SeccionCotizacionDTO> seccionCotizacionList) {
		this.seccionCotizacionList = seccionCotizacionList;
	}

	@Column(name = "DESCRIPCIONGIROASEGURADO", length = 250)
	public String getDescripcionGiroAsegurado() {
		return descripcionGiroAsegurado;
	}

	public void setDescripcionGiroAsegurado(String descripcionGiroAsegurado) {
		this.descripcionGiroAsegurado = descripcionGiroAsegurado;
	}

	@Column(name = "NUMEROSECUENCIA", precision = 5, scale = 0)
	public Long getNumeroSecuencia() {
		return numeroSecuencia;
	}

	public void setNumeroSecuencia(Long numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}

	@OneToOne(fetch = FetchType.EAGER, mappedBy = "incisoCotizacionDTO", cascade = CascadeType.ALL)
	public IncisoAutoCot getIncisoAutoCot() {
		return incisoAutoCot;
	}

	public void setIncisoAutoCot(IncisoAutoCot incisoAutoCot) {
		this.incisoAutoCot = incisoAutoCot;
	}

	@SuppressWarnings("unchecked")
	@Override
	public IncisoCotizacionId getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Transient
	public SeccionCotizacionDTO getSeccionCotizacion(){
		SeccionCotizacionDTO seccion = null;
		if(!this.getSeccionCotizacionList().isEmpty()){
			seccion = this.getSeccionCotizacionList().get(0);
		}
		return seccion;
	}

	@Transient
	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}
		
	@Transient
	public Boolean getSoloCotizados() {
		return soloCotizados;
	}

	public void setSoloCotizados(Boolean soloCotizados) {
		this.soloCotizados = soloCotizados;
	}

	/**
	 * @return the valorPrimaTotal
	 */
	@Transient
	public Double getValorPrimaTotal() {

		return valorPrimaTotal;
	}

	/**
	 * @param valorPrimaTotal the valorPrimaTotal to set
	 */
	public void setValorPrimaTotal(Double valorPrimaTotal) {
		this.valorPrimaTotal = valorPrimaTotal;
	}

	@Column(name = "IGUALACIONNIVELINCISO", nullable = true)
	public Boolean getIgualacionNivelInciso() {
		return igualacionNivelInciso;
	}

	public void setIgualacionNivelInciso(Boolean igualacionNivelInciso) {
		this.igualacionNivelInciso = igualacionNivelInciso;
	}

	@Column(name = "IGUALACIONPRIMAANTERIOR", nullable = true)
	public Double getPrimaTotalAntesDeIgualacion() {
		return primaTotalAntesDeIgualacion;
	}

	public void setPrimaTotalAntesDeIgualacion(Double primaTotalAntesDeIgualacion) {
		this.primaTotalAntesDeIgualacion = primaTotalAntesDeIgualacion;
	}

	@Column(name = "IGUALACIONDESCUENTOGENERADO", nullable = true)
	public Double getDescuentoIgualacionPrimas() {
		return descuentoIgualacionPrimas;
	}

	public void setDescuentoIgualacionPrimas(Double descuentoIgualacionPrimas) {
		this.descuentoIgualacionPrimas = descuentoIgualacionPrimas;
	}

	public void setIdConductoCobroCliente(Long idConductoCobroCliente) {
		this.idConductoCobroCliente = idConductoCobroCliente;
	}

	@Column(name = "IDCLIENTECONDUCTOCOBRO", nullable = true)
	public Long getIdConductoCobroCliente() {
		return idConductoCobroCliente;
	}

	public void setIdClienteCob(BigDecimal idClienteCob) {
		this.idClienteCob = idClienteCob;
	}

	@Column(name = "IDCLIENTECOB", nullable = true)
	public BigDecimal getIdClienteCob() {
		return idClienteCob;
	}

	public void setIdMedioPago(BigDecimal idMedioPago) {
		this.idMedioPago = idMedioPago;
	}

	@Column(name = "IDMEDIOPAGO", nullable = true)
	public BigDecimal getIdMedioPago() {
		return idMedioPago;
	}

	public void setNumeroTarjetaCobranza(String numeroTarjetaCobranza) {
		this.numeroTarjetaCobranza = numeroTarjetaCobranza;
	}

	@Transient
	public String getNumeroTarjetaCobranza() {
		return numeroTarjetaCobranza;
	}

	public void setDescripcionMedioPago(String descripcionMedioPago) {
		this.descripcionMedioPago = descripcionMedioPago;
	}

	@Transient
	public String getDescripcionMedioPago() {
		return descripcionMedioPago;
	}

	public void setPrimerReciboPagado(Boolean primerReciboPagado) {
		this.primerReciboPagado = primerReciboPagado;
	}

	@Column(name = "ESTATUSPAGORECIBO", nullable = true)
	public Boolean getPrimerReciboPagado() {
		return primerReciboPagado;
	}

	public void setNumeroAutorizacion(String numeroAutorizacion) {
		this.numeroAutorizacion = numeroAutorizacion;
	}

	@Column(name = "NUMEROAUTORIZACION", nullable = true)
	public String getNumeroAutorizacion() {
		return numeroAutorizacion;
	}

	public void setNumeroPrimerRecibo(String numeroPrimerRecibo) {
		this.numeroPrimerRecibo = numeroPrimerRecibo;
	}

	@Column(name = "NUMPRIMERRECIBO", nullable = true)
	public String getNumeroPrimerRecibo() {
		return numeroPrimerRecibo;
	}

	public void setDigitoVerificadorRecibo(String digitoVerificadorRecibo) {
		this.digitoVerificadorRecibo = digitoVerificadorRecibo;
	}

	@Column(name = "DIGVERIFICADORRECIBO", nullable = true)
	public String getDigitoVerificadorRecibo() {
		return digitoVerificadorRecibo;
	}

	public void setEmailContacto(String emailContacto) {
		this.emailContacto = emailContacto;
	}

	@Column(name = "EMAILCONTACTO ", nullable = true)
	public String getEmailContacto() {
		return emailContacto;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "TOCONDICIONESPINCISO", schema = "MIDAS",
			joinColumns = {@JoinColumn(name="COTIZACION_ID", referencedColumnName="IDTOCOTIZACION"),
						   @JoinColumn(name="NUMEROINCISO", referencedColumnName="NUMEROINCISO")}, 
			inverseJoinColumns = {@JoinColumn(name="CONDICIONESPECIAL_ID", referencedColumnName="ID")}
	)
	public List<CondicionEspecial> getCondicionesEspeciales() {
		return condicionesEspeciales;
	}

	public void setCondicionesEspeciales(
			List<CondicionEspecial> condicionesEspeciales) {
		this.condicionesEspeciales = condicionesEspeciales;
	}
	
	@Transient
	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	@Column(name = "CONDUCTOCOBRO")
	public String getConductoCobro() {
		return conductoCobro;
	}

	public void setConductoCobro(String conductoCobro) {
		this.conductoCobro = conductoCobro;
	}
	
	@Column(name = "INSTITUCIONBANCARIA")
	public String getInstitucionBancaria() {
		return institucionBancaria;
	}

	public void setInstitucionBancaria(String institucionBancaria) {
		this.institucionBancaria = institucionBancaria;
	}

	@Column(name = "TIPOTARJETA")
	public String getTipoTarjeta() {
		return tipoTarjeta;
	}

	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}
	
	@Column(name = "NUMEROTARJETACLAVE")
	public String getNumeroTarjetaClave() {
		return numeroTarjetaClave;
	}

	public void setNumeroTarjetaClave(String numeroTarjetaClave) {
		this.numeroTarjetaClave = numeroTarjetaClave;
	}

	@Column(name = "CODIGOSEGURIDAD")
	public String getCodigoSeguridad() {
		return codigoSeguridad;
	}

	public void setCodigoSeguridad(String codigoSeguridad) {
		this.codigoSeguridad = codigoSeguridad;
	}

	@Column(name = "FECHAVENCIMIENTO")
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
}