package mx.com.afirme.midas.cotizacion.inciso.cargamasiva;


import java.util.List;

import mx.com.afirme.midas.catalogos.esquemasreas.EsquemasDTO;



public interface CargaMasivaDetalleCotFacadeRemote {
	
	public void save(CargaMasivaDetalleCotDTO entity);
	
	public void save(EsquemasDTO entity);

	public void delete(CargaMasivaDetalleCotDTO entity);

	
	public CargaMasivaDetalleCotDTO update(CargaMasivaDetalleCotDTO entity);

	public CargaMasivaDetalleCotDTO findById(CargaMasivaDetalleCotId id);

	
	public List<CargaMasivaDetalleCotDTO> findByProperty(String propertyName,
			Object value);

	
	public List<CargaMasivaDetalleCotDTO> findAll();
}