package mx.com.afirme.midas.cotizacion.inciso;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * ConfiguracionDatoIncisoCotizacionId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class ConfiguracionDatoIncisoCotizacionId implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private BigDecimal idTcRamo;
	private BigDecimal idTcSubramo;
	private BigDecimal idToRiesgo;
	private Short claveDetalle;
	private BigDecimal idDato;

	// Constructors

	/** default constructor */
	public ConfiguracionDatoIncisoCotizacionId() {
	}

	/** full constructor */
	public ConfiguracionDatoIncisoCotizacionId(BigDecimal idTcRamo,
			BigDecimal idTcSubramo, BigDecimal idToRiesgo, Short claveDetalle,
			BigDecimal idDato) {
		this.idTcRamo = idTcRamo;
		this.idTcSubramo = idTcSubramo;
		this.idToRiesgo = idToRiesgo;
		this.claveDetalle = claveDetalle;
		this.idDato = idDato;
	}

	// Property accessors

	@Column(name = "IDTCRAMO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcRamo() {
		return this.idTcRamo;
	}

	public void setIdTcRamo(BigDecimal idTcRamo) {
		this.idTcRamo = idTcRamo;
	}

	@Column(name = "IDTCSUBRAMO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcSubramo() {
		return this.idTcSubramo;
	}

	public void setIdTcSubramo(BigDecimal idTcSubramo) {
		this.idTcSubramo = idTcSubramo;
	}

	@Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToRiesgo() {
		return this.idToRiesgo;
	}

	public void setIdToRiesgo(BigDecimal idToRiesgo) {
		this.idToRiesgo = idToRiesgo;
	}

	@Column(name = "CLAVEDETALLE", nullable = false, precision = 4, scale = 0)
	public Short getClaveDetalle() {
		return this.claveDetalle;
	}

	public void setClaveDetalle(Short claveDetalle) {
		this.claveDetalle = claveDetalle;
	}

	@Column(name = "IDDATO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdDato() {
		return this.idDato;
	}

	public void setIdDato(BigDecimal idDato) {
		this.idDato = idDato;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ConfiguracionDatoIncisoCotizacionId))
			return false;
		ConfiguracionDatoIncisoCotizacionId castOther = (ConfiguracionDatoIncisoCotizacionId) other;

		return ((this.getIdTcRamo() == castOther.getIdTcRamo()) || (this
				.getIdTcRamo() != null
				&& castOther.getIdTcRamo() != null && this.getIdTcRamo()
				.equals(castOther.getIdTcRamo())))
				&& ((this.getIdTcSubramo() == castOther.getIdTcSubramo()) || (this
						.getIdTcSubramo() != null
						&& castOther.getIdTcSubramo() != null && this
						.getIdTcSubramo().equals(castOther.getIdTcSubramo())))
				&& ((this.getIdToRiesgo() == castOther.getIdToRiesgo()) || (this
						.getIdToRiesgo() != null
						&& castOther.getIdToRiesgo() != null && this
						.getIdToRiesgo().equals(castOther.getIdToRiesgo())))
				&& ((this.getClaveDetalle() == castOther.getClaveDetalle()) || (this
						.getClaveDetalle() != null
						&& castOther.getClaveDetalle() != null && this
						.getClaveDetalle().equals(castOther.getClaveDetalle())))
				&& ((this.getIdDato() == castOther.getIdDato()) || (this
						.getIdDato() != null
						&& castOther.getIdDato() != null && this.getIdDato()
						.equals(castOther.getIdDato())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getIdTcRamo() == null ? 0 : this.getIdTcRamo().hashCode());
		result = 37
				* result
				+ (getIdTcSubramo() == null ? 0 : this.getIdTcSubramo()
						.hashCode());
		result = 37
				* result
				+ (getIdToRiesgo() == null ? 0 : this.getIdToRiesgo()
						.hashCode());
		result = 37
				* result
				+ (getClaveDetalle() == null ? 0 : this.getClaveDetalle()
						.hashCode());
		result = 37 * result
				+ (getIdDato() == null ? 0 : this.getIdDato().hashCode());
		return result;
	}
}