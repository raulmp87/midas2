package mx.com.afirme.midas.cotizacion.inciso;

// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.catalogos.descuento.DescuentoDTO;
import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.direccion.DireccionDTO;

/**
 * Remote interface for IncisoCotizacionDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */
public interface IncisoCotizacionFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved IncisoCotizacionDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            IncisoCotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public IncisoCotizacionDTO save(IncisoCotizacionDTO entity);

	/**
	 * Delete a persistent IncisoCotizacionDTO entity.
	 * 
	 * @param entity
	 *            IncisoCotizacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(IncisoCotizacionDTO entity);

	/**
	 * Persist a previously saved IncisoCotizacionDTO entity and return it or a
	 * copy of it to the sender. A copy of the IncisoCotizacionDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            IncisoCotizacionDTO entity to update
	 * @return IncisoCotizacionDTO the persisted IncisoCotizacionDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public IncisoCotizacionDTO update(IncisoCotizacionDTO entity);

	public IncisoCotizacionDTO findById(IncisoCotizacionId id);

	/**
	 * Find all IncisoCotizacionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the IncisoCotizacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<IncisoCotizacionDTO> found by query
	 */
	public List<IncisoCotizacionDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all IncisoCotizacionDTO entities.
	 * 
	 * @return List<IncisoCotizacionDTO> all IncisoCotizacionDTO entities
	 */
	public List<IncisoCotizacionDTO> findAll();

	/**
	 * Find all IncisoCotizacionDTO entities related with the given Cotizacion
	 * ID.
	 * 
	 * @param BigDecimal
	 *            idToCotizacion
	 * @return List<IncisoCotizacionDTO> all IncisoCotizacionDTO entities
	 */
	public List<IncisoCotizacionDTO> findByCotizacionId(
			BigDecimal idToCotizacion);
	/**
	 * Obtiene el inciso con cotizacion y numero de inciso
	 * si el numero de inciso es null retorna todos los incisos de una cotizacion
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @return List<IncisoCotizacionDTO>
	 */
	public List<IncisoCotizacionDTO> findByCotizacionId(
			BigDecimal idToCotizacion,BigDecimal numeroInciso);
	
	/**
	 * Lista los incisos filtrados por los siguientes parametros
	 * @param idToCotizacion (requerido)
	 * @param numeroSecuencia opcional
	 * @param idToSeccion opcional
	 * @param descripcion opcional
	 * @param paqueteId opcional
	 * @return List<IncisoCotizacionDTO>
	 * @author martin
	 */
	public List<IncisoCotizacionDTO> listarIncisosConFiltro(IncisoCotizacionDTO filtro);
	/**
	 * Obtiene el total de incisos de una cotizacion
	 * @param idToCotizacion
	 * @param numeroSecuencia
	 * @param idToSeccion
	 * @param descripcion
	 * @param paqueteId
	 * @return
	 */
	public Long listarIncisosConFiltroCount(IncisoCotizacionDTO filtro);
	
	public BigDecimal maxIncisos(BigDecimal idToCotizacion);
	
	public Long maxSecuencia(BigDecimal idToCotizacion);
	
	public int contarIncisosPorCotizacion(BigDecimal idToCotizacion);

	/**
	 * Find IncisoCotizacionDTO entities with claveAutInspeccion = 1:Pendiente
	 * Autorizacion or 8:Rechazada
	 * 
	 * @return List<IncisoCotizacionDTO> all IncisoCotizacionDTO entities
	 */
	public List<IncisoCotizacionDTO> listarAutorizacionesPendientesYRechazadasPorCotizacion(
			BigDecimal idToCotizacion);

	public Double getSumaAseguradaMayorPorUbicacion(BigDecimal idToCotizacion,
			String seccionesConComas);

	public BigDecimal primaNetaPorCotizacionPrimerRiesgo(
			BigDecimal idToCotizacion, String seccionesConComas);

	public BigDecimal sumaAseguradaPrimerRiesgoPorCotizacion(
			BigDecimal idToCotizacion);

	/**
	 * Lista los incisos de una cotizacion que tengan las coberturas recibidas
	 * con la claveContrato recibido.
	 * 
	 * @param BigDecimal
	 *            idToCotizacion @param List<CoberturaCotizacionDTO> Lista de
	 *            coberturas con la claveContrato recibida
	 * @param claveContrato
	 * @return List<IncisoCotizacionDTO> entidades IncisoCotizacionDTO
	 *         encontradas pro el query
	 */
	public List<IncisoCotizacionDTO> listarIncisosPorCoberturaContratada(
			BigDecimal idToCotizacion, List<CoberturaCotizacionDTO> coberturas,
			Short claveContrato);

	public Map<String, String> agregarInciso(CotizacionDTO cotizacionDTO,
			BigDecimal idToDireccion, BigDecimal numeroInciso,
			List<ConfiguracionDatoIncisoCotizacionDTO> datosInciso,
			String[] datos, List<RecargoVarioDTO> recargosEspeciales,
			List<DescuentoDTO> descuentosEspeciales);

	public Map<String, String> borrarInciso(
			IncisoCotizacionDTO incisoCotizacionDTO);
	
	public void actualizaNumeroSecuencia(BigDecimal idToCotizacion,Long numeroSecuencia);
	
	public Boolean copiarInciso(IncisoCotizacionDTO incisoNuevo,
			IncisoCotizacionDTO incisoOriginal, CotizacionDTO cotizacionDTO,
			DireccionDTO direccionNueva,
			List<DatoIncisoCotizacionDTO> listaDatosInciso);

	public Map<String, String> separarInciso(CotizacionDTO cotizacionDTO,
			BigDecimal numeroInciso);
	
	public List<IncisoCotizacionDTO> obtenerIncisosPorIdCotizacionDireccion(BigDecimal idToCotizacion, BigDecimal idToDireccionInciso);

	public BigDecimal copiarInciso(BigDecimal idToCotizacion, BigDecimal numeroIncisoBase, BigDecimal idToDireccion);
	
	/**
	 * Lista los incisos de una cotizacion que tengan al menos una cobertura contratada con el subramo especificado. 
	 * @param idToCotizacion, idTcSubRamo
	 * @return List<IncisoCotizacionDTO> entidades IncisoCotizacionDTO encontradas por el query.
	 */
	public List<IncisoCotizacionDTO> listarIncisosPorSubRamo(BigDecimal idToCotizacion, BigDecimal idTcSubRamo);
	
	/**
	 * Lista los incisos dados de baja en una cotizacion de endoso. 
	 * @param idToCotizacionEndoso
	 * @return List<IncisoCotizacionDTO> entidades IncisoCotizacionDTO encontradas por el query.
	 */	
	public List<IncisoCotizacionDTO> listarBajaIncisos(BigDecimal idToCotizacionEndosoActual, BigDecimal idToCotizacionEndosoAnterior);
	
	/**
	 * Actualiza los datos de un inciso.
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idToDireccion
	 * @param datosInciso
	 * @param datos
	 * @return
	 */
	public IncisoCotizacionDTO actualizarIncisoCotizacion(BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idToDireccion,
			List<ConfiguracionDatoIncisoCotizacionDTO> datosInciso, String[] datos);
	
	/**
	 * Perform an initial save of a previously unsaved IncisoCotizacionDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            IncisoCotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public IncisoCotizacionId saveAndGetId(IncisoCotizacionDTO entity);
	
	public List<IncisoCotizacionDTO> getIncisosPorLineaPaquete(
			BigDecimal cotizacionId, BigDecimal lineaId, Long paqueteId);

	public List<IncisoCotizacionDTO> getIncisosPorLinea(
			BigDecimal cotizacionId, BigDecimal lineaId);
	/**
	 * Obtener seccion cotizacion de un inciso
	 * @param idToCotizacion
	 * @param idInciso
	 * @return
	 */
	public SeccionCotizacionDTO getSeccionCotizacionDTO(BigDecimal idToCotizacion, BigDecimal idInciso);
	
	public List<IncisoCotizacionDTO> findNoAsignadosByCotizacionId(BigDecimal idToCotizacion);
	
	public BigDecimal copiarInciso(BigDecimal idToCotizacion, BigDecimal numeroIncisoBase, BigDecimal numeroInciso, BigDecimal idToDireccion);
	
}