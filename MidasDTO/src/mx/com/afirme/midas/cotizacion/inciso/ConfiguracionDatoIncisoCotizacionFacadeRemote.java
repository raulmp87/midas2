package mx.com.afirme.midas.cotizacion.inciso;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.sistema.SystemException;

/**
 * Remote interface for ConfiguracionDatoIncisoDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ConfiguracionDatoIncisoCotizacionFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved
	 * ConfiguracionDatoIncisoCotizacionDTO entity. All subsequent persist actions of this
	 * entity should use the #update() method.
	 * 
	 * @param entity
	 *            ConfiguracionDatoIncisoCotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ConfiguracionDatoIncisoCotizacionDTO entity);

	/**
	 * Delete a persistent ConfiguracionDatoIncisoCotizacionDTO entity.
	 * 
	 * @param entity
	 *            ConfiguracionDatoIncisoCotizacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ConfiguracionDatoIncisoCotizacionDTO entity);

	/**
	 * Persist a previously saved ConfiguracionDatoIncisoCotizacionDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * ConfiguracionDatoIncisoCotizacionDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            ConfiguracionDatoIncisoCotizacionDTO entity to update
	 * @return ConfiguracionDatoIncisoCotizacionDTO the persisted
	 *         ConfiguracionDatoIncisoCotizacionDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ConfiguracionDatoIncisoCotizacionDTO update(ConfiguracionDatoIncisoCotizacionDTO entity);

	public ConfiguracionDatoIncisoCotizacionDTO findById(ConfiguracionDatoIncisoCotizacionId id);

	/**
	 * Find all ConfiguracionDatoIncisoCotizacionDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the ConfiguracionDatoIncisoCotizacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<ConfiguracionDatoIncisoCotizacionDTO> found by query
	 */
	public List<ConfiguracionDatoIncisoCotizacionDTO> findByProperty(String propertyName,
			Object value);

	public List<ConfiguracionDatoIncisoCotizacionDTO> findByClaveTipoControl(
			Object claveTipoControl);

	public List<ConfiguracionDatoIncisoCotizacionDTO> findByClaveTipoValidacion(
			Object claveTipoValidacion);

	public List<ConfiguracionDatoIncisoCotizacionDTO> findByDescripcionEtiqueta(
			Object descripcionEtiqueta);

	public List<ConfiguracionDatoIncisoCotizacionDTO> findByDescripcionClaseRemota(
			Object descripcionClaseRemota);

	/**
	 * Find all ConfiguracionDatoIncisoCotizacionDTO entities.
	 * 
	 * @return List<ConfiguracionDatoIncisoCotizacionDTO> all ConfiguracionDatoIncisoCotizacionDTO
	 *         entities
	 */
	public List<ConfiguracionDatoIncisoCotizacionDTO> findAll();

	public List<ConfiguracionDatoIncisoCotizacionDTO> getDatosRamoInciso(BigDecimal idTcRamo);

	public List<ConfiguracionDatoIncisoCotizacionDTO> getDatosRiesgoInciso(BigDecimal idToRiesgo);

	public List<ConfiguracionDatoIncisoCotizacionDTO> getDatosRiesgoSubInciso(BigDecimal idToRiesgo);
	
	public List<ConfiguracionDatoIncisoCotizacionDTO> getDatosRiesgoImpresionPoliza(BigDecimal idTcRamo, BigDecimal idTcSubRamo);
	
	public List<ConfiguracionDatoIncisoCotizacionDTO> listarFiltrado(ConfiguracionDatoIncisoCotizacionDTO entity);
	
	public List<ConfiguracionDatoIncisoCotizacionDTO> getDatosRiesgoInciso(BigDecimal idToTipoPoliza, BigDecimal idTcRamo);


	/**
	 * Obtiene la etiqueta y valor real de un datoIncisoCotizacionDTO.
	 * @param datoIncisoCotizacionDTO
	 * @return String[] arreglo de String que contiene en la posici�n 0 la etiqueta, en la posici�n [1] el valor y en la posici�n [2] el valor registrado en datoIncisoCot.
	 * @throws SystemException
	 */
	public String[] obtenerDescripcionDatoInciso(DatoIncisoCotizacionDTO datoIncisoCotizacionDTO,ConfiguracionDatoIncisoCotizacionDTO configuracionDatoIncisoCotizacionDTO);

}