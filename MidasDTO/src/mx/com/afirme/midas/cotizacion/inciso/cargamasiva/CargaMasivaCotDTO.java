package mx.com.afirme.midas.cotizacion.inciso.cargamasiva;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author JORGEKNO
 */
@Entity
@Table(name = "TOCARGAMASIVACOT", schema = "MIDAS")
public class CargaMasivaCotDTO implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private BigDecimal idToCotizacion;
	private BigDecimal numeroIncisoBase;
	private String claveSeccionRiesgo;
	private List<CargaMasivaDetalleCotDTO> cargaMasivaDetalleCotDTO = new ArrayList<CargaMasivaDetalleCotDTO>();

	// Constructors

	/** default constructor */
	public CargaMasivaCotDTO() {
	}

	/** minimal constructor */
	public CargaMasivaCotDTO(BigDecimal idToCotizacion,
			BigDecimal numeroIncisoBase, String claveSeccionRiesgo) {
		this.idToCotizacion = idToCotizacion;
		this.numeroIncisoBase = numeroIncisoBase;
		this.claveSeccionRiesgo = claveSeccionRiesgo;
	}

	/** full constructor */
	public CargaMasivaCotDTO(BigDecimal idToCotizacion,
			BigDecimal numeroIncisoBase, String claveSeccionRiesgo,
			List<CargaMasivaDetalleCotDTO> cargaMasivaDetalleCotDTO) {
		this.idToCotizacion = idToCotizacion;
		this.numeroIncisoBase = numeroIncisoBase;
		this.claveSeccionRiesgo = claveSeccionRiesgo;
		this.cargaMasivaDetalleCotDTO = cargaMasivaDetalleCotDTO;
	}

	// Property accessors
	@Id
	@Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCotizacion() {
		return this.idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	@Column(name = "NUMEROINCISOBASE", nullable = false, precision = 22, scale = 0)
	public BigDecimal getNumeroIncisoBase() {
		return this.numeroIncisoBase;
	}

	public void setNumeroIncisoBase(BigDecimal numeroIncisoBase) {
		this.numeroIncisoBase = numeroIncisoBase;
	}

	@Column(name = "CLAVESECCIONRIESGO", nullable = false, length = 2000)
	public String getClaveSeccionRiesgo() {
		return this.claveSeccionRiesgo;
	}

	public void setClaveSeccionRiesgo(String claveSeccionRiesgo) {
		this.claveSeccionRiesgo = claveSeccionRiesgo;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "cargaMasivaCotDTO")
	public List<CargaMasivaDetalleCotDTO> getCargaMasivaDetalleCotDTO() {
		return this.cargaMasivaDetalleCotDTO;
	}

	public void setCargaMasivaDetalleCotDTO(
			List<CargaMasivaDetalleCotDTO> cargaMasivaDetalleCotDTO) {
		this.cargaMasivaDetalleCotDTO = cargaMasivaDetalleCotDTO;
	}

}