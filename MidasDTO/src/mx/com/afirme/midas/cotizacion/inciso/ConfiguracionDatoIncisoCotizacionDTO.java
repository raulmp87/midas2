package mx.com.afirme.midas.cotizacion.inciso;

import java.math.BigDecimal;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * ConfiguracionDatoIncisoCotizacionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCCONFIGDATOINCISOCOT", schema = "MIDAS")
public class ConfiguracionDatoIncisoCotizacionDTO implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private ConfiguracionDatoIncisoCotizacionId id;
	private Short claveTipoControl;
	private Short claveTipoValidacion;
	private String descripcionEtiqueta;
	private String descripcionClaseRemota;
	private BigDecimal idGrupo;
	private Short claveimpresionpoliza;
	private Short codigoFormato;
	
	/*
	 * Valores que por el momento solo utiliza el cotizador casa y no MIDAS.
	 * Tal vez mas adelante tendra que ser necesario manejar algun tipo de
	 * applicationId para tener mas control sobre estos campos.
	 */
	private Integer codigoCampo;
	private Integer visibleCasa;
	private String valorDefaultCasa;
	private Integer validacionCasa;
	private String dependencia;
	private Integer tipoDependencia;
	
	// Constructors

	public ConfiguracionDatoIncisoCotizacionDTO(Short claveDetalle,BigDecimal idDato,BigDecimal idTcRamo,BigDecimal idTcSubRamo,BigDecimal idToRiesgo){
		this(new ConfiguracionDatoIncisoCotizacionId(),null,null,null,null,null);
		getId().setClaveDetalle(claveDetalle);
		getId().setIdDato(idDato);
		getId().setIdTcRamo(idTcRamo);
		getId().setIdTcSubramo(idTcSubRamo);
		getId().setIdToRiesgo(idToRiesgo);
	}
	
	/** default constructor */
	public ConfiguracionDatoIncisoCotizacionDTO() {
	}

	/** minimal constructor */
	public ConfiguracionDatoIncisoCotizacionDTO(ConfiguracionDatoIncisoCotizacionId id,
			Short claveTipoControl, Short claveTipoValidacion,
			String descripcionEtiqueta, BigDecimal idGrupo, 
			Short claveimpresionpoliza) {
		this.id = id;
		this.claveTipoControl = claveTipoControl;
		this.claveTipoValidacion = claveTipoValidacion;
		this.descripcionEtiqueta = descripcionEtiqueta;
		this.idGrupo = idGrupo;
		this.claveimpresionpoliza = claveimpresionpoliza;
	}

	/** full constructor */
	public ConfiguracionDatoIncisoCotizacionDTO(ConfiguracionDatoIncisoCotizacionId id,
			Short claveTipoControl, Short claveTipoValidacion,
			String descripcionEtiqueta, String descripcionClaseRemota,
			BigDecimal idGrupo, Short claveimpresionpoliza) {
		this.id = id;
		this.claveTipoControl = claveTipoControl;
		this.claveTipoValidacion = claveTipoValidacion;
		this.descripcionEtiqueta = descripcionEtiqueta;
		this.descripcionClaseRemota = descripcionClaseRemota;
		this.idGrupo = idGrupo;
		this.claveimpresionpoliza = claveimpresionpoliza;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idTcRamo", column = @Column(name = "IDTCRAMO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idTcSubramo", column = @Column(name = "IDTCSUBRAMO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToRiesgo", column = @Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "claveDetalle", column = @Column(name = "CLAVEDETALLE", nullable = false, precision = 4, scale = 0)),
			@AttributeOverride(name = "idDato", column = @Column(name = "IDDATO", nullable = false, precision = 22, scale = 0)) })
	public ConfiguracionDatoIncisoCotizacionId getId() {
		return this.id;
	}

	public void setId(ConfiguracionDatoIncisoCotizacionId id) {
		this.id = id;
	}

	@Column(name = "CLAVETIPOCONTROL", nullable = false, precision = 4, scale = 0)
	public Short getClaveTipoControl() {
		return this.claveTipoControl;
	}

	public void setClaveTipoControl(Short claveTipoControl) {
		this.claveTipoControl = claveTipoControl;
	}

	@Column(name = "CLAVETIPOVALIDACION", nullable = false, precision = 4, scale = 0)
	public Short getClaveTipoValidacion() {
		return this.claveTipoValidacion;
	}

	public void setClaveTipoValidacion(Short claveTipoValidacion) {
		this.claveTipoValidacion = claveTipoValidacion;
	}

	@Column(name = "DESCRIPCIONETIQUETA", nullable = false, length = 200)
	public String getDescripcionEtiqueta() {
		return this.descripcionEtiqueta;
	}

	public void setDescripcionEtiqueta(String descripcionEtiqueta) {
		this.descripcionEtiqueta = descripcionEtiqueta;
	}

	@Column(name = "DESCRIPCIONCLASEREMOTA", length = 200)
	public String getDescripcionClaseRemota() {
		return this.descripcionClaseRemota;
	}

	public void setDescripcionClaseRemota(String descripcionClaseRemota) {
		this.descripcionClaseRemota = descripcionClaseRemota;
	}

	@Column(name = "IDGRUPO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdGrupo() {
		return this.idGrupo;
	}

	public void setIdGrupo(BigDecimal idGrupo) {
		this.idGrupo = idGrupo;
	}
	
	@Column(name = "CLAVEIMPRESIONPOLIZA", nullable = false, precision = 4, scale = 0)
	public Short getClaveimpresionpoliza() {
		return this.claveimpresionpoliza;
	}

	public void setClaveimpresionpoliza(Short claveimpresionpoliza) {
		this.claveimpresionpoliza = claveimpresionpoliza;
	}

	@Column(name = "CODIGOFORMATO", nullable = false, precision = 4, scale = 0)
	public Short getCodigoFormato() {
		return codigoFormato;
	}

	public void setCodigoFormato(Short codigoFormato) {
		this.codigoFormato = codigoFormato;
	}

	@Column
	public Integer getCodigoCampo() {
		return codigoCampo;
	}

	public void setCodigoCampo(Integer codigoCampo) {
		this.codigoCampo = codigoCampo;
	}

	@Column
	public Integer getVisibleCasa() {
		return visibleCasa;
	}

	public void setVisibleCasa(Integer visibleCasa) {
		this.visibleCasa = visibleCasa;
	}

	@Column
	public String getValorDefaultCasa() {
		return valorDefaultCasa;
	}

	public void setValorDefaultCasa(String valorDefaultCasa) {
		this.valorDefaultCasa = valorDefaultCasa;
	}

	@Column
	public Integer getValidacionCasa() {
		return validacionCasa;
	}

	public void setValidacionCasa(Integer validacionCasa) {
		this.validacionCasa = validacionCasa;
	}

	@Column
	public String getDependencia() {
		return dependencia;
	}

	public void setDependencia(String dependencia) {
		this.dependencia = dependencia;
	}

	@Column
	public Integer getTipoDependencia() {
		return tipoDependencia;
	}

	public void setTipoDependencia(Integer tipoDependencia) {
		this.tipoDependencia = tipoDependencia;
	}
		
}