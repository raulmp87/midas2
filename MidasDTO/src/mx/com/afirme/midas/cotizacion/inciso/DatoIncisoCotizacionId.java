package mx.com.afirme.midas.cotizacion.inciso;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * DatoIncisoCotizacionId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class DatoIncisoCotizacionId implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private BigDecimal idToCotizacion;
	private BigDecimal numeroInciso;
	private BigDecimal idToSeccion;
	private BigDecimal idToCobertura;
	private BigDecimal idToRiesgo;
	private BigDecimal numeroSubinciso;
	private BigDecimal idTcRamo;
	private BigDecimal idTcSubramo;
	private Short claveDetalle;
	private BigDecimal idDato;

	// Constructors

	/** default constructor */
	public DatoIncisoCotizacionId() {
	}

	/** full constructor */
	public DatoIncisoCotizacionId(BigDecimal idToCotizacion,
			BigDecimal numeroInciso, BigDecimal idToSeccion,
			BigDecimal idToCobertura, BigDecimal idToRiesgo,
			BigDecimal numeroSubinciso, BigDecimal idTcRamo,
			BigDecimal idTcSubramo, Short claveDetalle, BigDecimal idDato) {
		this.idToCotizacion = idToCotizacion;
		this.numeroInciso = numeroInciso;
		this.idToSeccion = idToSeccion;
		this.idToCobertura = idToCobertura;
		this.idToRiesgo = idToRiesgo;
		this.numeroSubinciso = numeroSubinciso;
		this.idTcRamo = idTcRamo;
		this.idTcSubramo = idTcSubramo;
		this.claveDetalle = claveDetalle;
		this.idDato = idDato;
	}

	// Property accessors

	@Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCotizacion() {
		return this.idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	@Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getNumeroInciso() {
		return this.numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	@Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSeccion() {
		return this.idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	@Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCobertura() {
		return this.idToCobertura;
	}

	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	@Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToRiesgo() {
		return this.idToRiesgo;
	}

	public void setIdToRiesgo(BigDecimal idToRiesgo) {
		this.idToRiesgo = idToRiesgo;
	}

	@Column(name = "NUMEROSUBINCISO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getNumeroSubinciso() {
		return this.numeroSubinciso;
	}

	public void setNumeroSubinciso(BigDecimal numeroSubinciso) {
		this.numeroSubinciso = numeroSubinciso;
	}

	@Column(name = "IDTCRAMO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcRamo() {
		return this.idTcRamo;
	}

	public void setIdTcRamo(BigDecimal idTcRamo) {
		this.idTcRamo = idTcRamo;
	}

	@Column(name = "IDTCSUBRAMO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcSubramo() {
		return this.idTcSubramo;
	}

	public void setIdTcSubramo(BigDecimal idTcSubramo) {
		this.idTcSubramo = idTcSubramo;
	}

	@Column(name = "CLAVEDETALLE", nullable = false, precision = 4, scale = 0)
	public Short getClaveDetalle() {
		return this.claveDetalle;
	}

	public void setClaveDetalle(Short claveDetalle) {
		this.claveDetalle = claveDetalle;
	}

	@Column(name = "IDDATO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdDato() {
		return this.idDato;
	}

	public void setIdDato(BigDecimal idDato) {
		this.idDato = idDato;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof DatoIncisoCotizacionId))
			return false;
		DatoIncisoCotizacionId castOther = (DatoIncisoCotizacionId) other;

		return ((this.getIdToCotizacion() == castOther.getIdToCotizacion()) || (this
				.getIdToCotizacion() != null
				&& castOther.getIdToCotizacion() != null && this
				.getIdToCotizacion().equals(castOther.getIdToCotizacion())))
				&& ((this.getNumeroInciso() == castOther.getNumeroInciso()) || (this
						.getNumeroInciso() != null
						&& castOther.getNumeroInciso() != null && this
						.getNumeroInciso().equals(castOther.getNumeroInciso())))
				&& ((this.getIdToSeccion() == castOther.getIdToSeccion()) || (this
						.getIdToSeccion() != null
						&& castOther.getIdToSeccion() != null && this
						.getIdToSeccion().equals(castOther.getIdToSeccion())))
				&& ((this.getIdToCobertura() == castOther.getIdToCobertura()) || (this
						.getIdToCobertura() != null
						&& castOther.getIdToCobertura() != null && this
						.getIdToCobertura()
						.equals(castOther.getIdToCobertura())))
				&& ((this.getIdToRiesgo() == castOther.getIdToRiesgo()) || (this
						.getIdToRiesgo() != null
						&& castOther.getIdToRiesgo() != null && this
						.getIdToRiesgo().equals(castOther.getIdToRiesgo())))
				&& ((this.getNumeroSubinciso() == castOther
						.getNumeroSubinciso()) || (this.getNumeroSubinciso() != null
						&& castOther.getNumeroSubinciso() != null && this
						.getNumeroSubinciso().equals(
								castOther.getNumeroSubinciso())))
				&& ((this.getIdTcRamo() == castOther.getIdTcRamo()) || (this
						.getIdTcRamo() != null
						&& castOther.getIdTcRamo() != null && this
						.getIdTcRamo().equals(castOther.getIdTcRamo())))
				&& ((this.getIdTcSubramo() == castOther.getIdTcSubramo()) || (this
						.getIdTcSubramo() != null
						&& castOther.getIdTcSubramo() != null && this
						.getIdTcSubramo().equals(castOther.getIdTcSubramo())))
				&& ((this.getClaveDetalle() == castOther.getClaveDetalle()) || (this
						.getClaveDetalle() != null
						&& castOther.getClaveDetalle() != null && this
						.getClaveDetalle().equals(castOther.getClaveDetalle())))
				&& ((this.getIdDato() == castOther.getIdDato()) || (this
						.getIdDato() != null
						&& castOther.getIdDato() != null && this.getIdDato()
						.equals(castOther.getIdDato())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToCotizacion() == null ? 0 : this.getIdToCotizacion()
						.hashCode());
		result = 37
				* result
				+ (getNumeroInciso() == null ? 0 : this.getNumeroInciso()
						.hashCode());
		result = 37
				* result
				+ (getIdToSeccion() == null ? 0 : this.getIdToSeccion()
						.hashCode());
		result = 37
				* result
				+ (getIdToCobertura() == null ? 0 : this.getIdToCobertura()
						.hashCode());
		result = 37
				* result
				+ (getIdToRiesgo() == null ? 0 : this.getIdToRiesgo()
						.hashCode());
		result = 37
				* result
				+ (getNumeroSubinciso() == null ? 0 : this.getNumeroSubinciso()
						.hashCode());
		result = 37 * result
				+ (getIdTcRamo() == null ? 0 : this.getIdTcRamo().hashCode());
		result = 37
				* result
				+ (getIdTcSubramo() == null ? 0 : this.getIdTcSubramo()
						.hashCode());
		result = 37
				* result
				+ (getClaveDetalle() == null ? 0 : this.getClaveDetalle()
						.hashCode());
		result = 37 * result
				+ (getIdDato() == null ? 0 : this.getIdDato().hashCode());
		return result;
	}
}