package mx.com.afirme.midas.cotizacion.inciso.cargamasiva;

// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.direccion.DireccionDTO;

/**
 * Remote interface for CargaMasivaCotDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface CargaMasivaCotFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved CargaMasivaCotDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            CargaMasivaCotDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(CargaMasivaCotDTO entity);

	/**
	 * Delete a persistent CargaMasivaCotDTO entity.
	 * 
	 * @param entity
	 *            CargaMasivaCotDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(CargaMasivaCotDTO entity);

	/**
	 * Persist a previously saved CargaMasivaCotDTO entity and return it or a
	 * copy of it to the sender. A copy of the CargaMasivaCotDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            CargaMasivaCotDTO entity to update
	 * @return CargaMasivaCotDTO the persisted CargaMasivaCotDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public CargaMasivaCotDTO update(CargaMasivaCotDTO entity);

	public CargaMasivaCotDTO findById(BigDecimal id);

	/**
	 * Find all CargaMasivaCotDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the CargaMasivaCotDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<CargaMasivaCotDTO> found by query
	 */
	public List<CargaMasivaCotDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all CargaMasivaCotDTO entities.
	 * 
	 * @return List<CargaMasivaCotDTO> all CargaMasivaCotDTO entities
	 */
	public List<CargaMasivaCotDTO> findAll();

	public CargaMasivaCotDTO agregar(CargaMasivaCotDTO cargaMasivaCotDTO,
			List<DireccionDTO> direccionesValidas,
			List<DireccionDTO> direccionesInvalidas);

	public void insertarDetalleCargaMasiva(CargaMasivaCotDTO cargaMasivaCotDTO,
			List<DireccionDTO> direcciones, boolean direccionesValidas);
	
	public CargaMasivaCotDTO getCargaMasiva(BigDecimal idToCotizacion);
	
	public void actualizaDetalle(CargaMasivaDetalleCotDTO detalle);
	
	public DireccionDTO poblarDireccion(CargaMasivaDetalleCotDTO detalle);
}