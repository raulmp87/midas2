package mx.com.afirme.midas.cotizacion.inciso;

import java.math.BigDecimal;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * DatoIncisoCotizacionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TODATOINCISOCOT", schema = "MIDAS")
public class DatoIncisoCotizacionDTO implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private DatoIncisoCotizacionId id;
	private String valor;

	// Constructors

	/** default constructor */
	public DatoIncisoCotizacionDTO() {
	}

	/** full constructor */
	public DatoIncisoCotizacionDTO(DatoIncisoCotizacionId id, String valor) {
		this.id = id;
		this.valor = valor;
	}

	/**
	 * Instancia un objeto DatoIncisoCotizacionDTO estableciando todos los atributos en cero.
	 * @return DatoIncisoCotizacionDTO instancia
	 */
	public static DatoIncisoCotizacionDTO instanciaDatoIncisoCotizacion(){
		DatoIncisoCotizacionDTO datoInciso;
		datoInciso = new DatoIncisoCotizacionDTO();
		datoInciso.setId(new DatoIncisoCotizacionId());
		datoInciso.getId().setIdToCotizacion(new BigDecimal(0d));
		datoInciso.getId().setNumeroInciso(new BigDecimal(0d));
		datoInciso.getId().setIdTcRamo(new BigDecimal(0d));
		datoInciso.getId().setIdDato(new BigDecimal(0d));
		datoInciso.getId().setIdToSeccion(new BigDecimal(0d));
		datoInciso.getId().setIdToCobertura(new BigDecimal(0d));
		datoInciso.getId().setNumeroSubinciso(new BigDecimal(0d));
		datoInciso.getId().setIdTcSubramo(new BigDecimal(0d));
		datoInciso.getId().setClaveDetalle(new Short((short)0));
		datoInciso.getId().setIdToRiesgo(new BigDecimal(0d));
		return datoInciso;
	}
	
	/**
	 * Instancia un objeto DatoIncisoCotizacionDTO estableciando todos los atributos en cero,
	 * además del idToCotizacion y numeroInciso recibidos. 
	 * @return DatoIncisoCotizacionDTO instancia
	 */
	public static DatoIncisoCotizacionDTO instanciaDatoIncisoCotizacion(BigDecimal idToCotizacion,BigDecimal numeroInciso){
		DatoIncisoCotizacionDTO datoInciso;
		datoInciso = new DatoIncisoCotizacionDTO();
		datoInciso.setId(new DatoIncisoCotizacionId());
		datoInciso.getId().setIdToCotizacion(idToCotizacion);
		datoInciso.getId().setNumeroInciso(numeroInciso);
		datoInciso.getId().setIdTcRamo(new BigDecimal(0d));
		datoInciso.getId().setIdDato(new BigDecimal(0d));
		datoInciso.getId().setIdToSeccion(new BigDecimal(0d));
		datoInciso.getId().setIdToCobertura(new BigDecimal(0d));
		datoInciso.getId().setNumeroSubinciso(new BigDecimal(0d));
		datoInciso.getId().setIdTcSubramo(new BigDecimal(0d));
		datoInciso.getId().setClaveDetalle(new Short((short)0));
		datoInciso.getId().setIdToRiesgo(new BigDecimal(0d));
		return datoInciso;
	}
	
	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToCotizacion", column = @Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroInciso", column = @Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToSeccion", column = @Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToCobertura", column = @Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToRiesgo", column = @Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroSubinciso", column = @Column(name = "NUMEROSUBINCISO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idTcRamo", column = @Column(name = "IDTCRAMO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idTcSubramo", column = @Column(name = "IDTCSUBRAMO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "claveDetalle", column = @Column(name = "CLAVEDETALLE", nullable = false, precision = 4, scale = 0)),
			@AttributeOverride(name = "idDato", column = @Column(name = "IDDATO", nullable = false, precision = 22, scale = 0)) })
	public DatoIncisoCotizacionId getId() {
		return this.id;
	}

	public void setId(DatoIncisoCotizacionId id) {
		this.id = id;
	}

	@Column(name = "VALOR", nullable = false, length = 4000)
	public String getValor() {
		return this.valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
}