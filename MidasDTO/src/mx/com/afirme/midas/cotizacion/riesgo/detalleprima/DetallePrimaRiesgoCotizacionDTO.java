package mx.com.afirme.midas.cotizacion.riesgo.detalleprima;
// default package

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDTO;


/**
 * DetPrimaRiesgoCotizacionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TODETPRIMARIESGOCOT"
    ,schema="MIDAS"
)
public class DetallePrimaRiesgoCotizacionDTO  implements java.io.Serializable {
	private static final long serialVersionUID = 9052234293753376361L;
     private DetallePrimaRiesgoCotizacionId id;
     private RiesgoCotizacionDTO riesgoCotizacionDTO;
     private Double valorSumaAsegurada;
     private Double valorPrimaNetaB;
     private Double valorPrimaNetaARDT;
     private Double valorPrimaNeta;
     private Double valorCuotaB;
     private Double valorCuotaARDT;
     private Double valorCuotaARDV;
     private Double valorCuota;


    // Constructors

    /** default constructor */
    public DetallePrimaRiesgoCotizacionDTO() {
    }
   
    // Property accessors
    @EmbeddedId
    @AttributeOverrides( {
        @AttributeOverride(name="numeroSubInciso", column=@Column(name="NUMEROSUBINCISO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idToCotizacion", column=@Column(name="IDTOCOTIZACION", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="numeroInciso", column=@Column(name="NUMEROINCISO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idToSeccion", column=@Column(name="IDTOSECCION", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idToCobertura", column=@Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idToRiesgo", column=@Column(name="IDTORIESGO", nullable=false, precision=22, scale=0) ) } )

    public DetallePrimaRiesgoCotizacionId getId() {
        return this.id;
    }
    
    public void setId(DetallePrimaRiesgoCotizacionId id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumns( { 
        @JoinColumn(name="IDTOCOTIZACION", referencedColumnName="IDTOCOTIZACION", nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="NUMEROINCISO", referencedColumnName="NUMEROINCISO", nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="IDTOSECCION", referencedColumnName="IDTOSECCION", nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="IDTOCOBERTURA", referencedColumnName="IDTOCOBERTURA", nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="IDTORIESGO", referencedColumnName="IDTORIESGO", nullable=false, insertable=false, updatable=false) } )

    public RiesgoCotizacionDTO getRiesgoCotizacionDTO() {
		return riesgoCotizacionDTO;
	}

	public void setRiesgoCotizacionDTO(RiesgoCotizacionDTO riesgoCotizacionDTO) {
		this.riesgoCotizacionDTO = riesgoCotizacionDTO;
	}
    
    @Column(name="VALORSUMAASEGURADA", nullable=false, precision=16)
    public Double getValorSumaAsegurada() {
        return this.valorSumaAsegurada;
    }
    
    public void setValorSumaAsegurada(Double valorSumaAsegurada) {
        this.valorSumaAsegurada = valorSumaAsegurada;
    }
    
    @Column(name="VALORPRIMANETAB", nullable=false, precision=16)
    public Double getValorPrimaNetaB() {
        return this.valorPrimaNetaB;
    }
    
    public void setValorPrimaNetaB(Double valorPrimaNetaB) {
        this.valorPrimaNetaB = valorPrimaNetaB;
    }
    
    @Column(name="VALORPRIMANETAARDT", nullable=false, precision=16)
    public Double getValorPrimaNetaARDT() {
        return this.valorPrimaNetaARDT;
    }
    
    public void setValorPrimaNetaARDT(Double valorPrimaNetaARDT) {
        this.valorPrimaNetaARDT = valorPrimaNetaARDT;
    }
    
    @Column(name="VALORPRIMANETA", nullable=false, precision=16)
    public Double getValorPrimaNeta() {
        return this.valorPrimaNeta;
    }
    
    public void setValorPrimaNeta(Double valorPrimaNeta) {
        this.valorPrimaNeta = valorPrimaNeta;
    }
    
    @Column(name="VALORCUOTAB", nullable=false, precision=16, scale=10)
    public Double getValorCuotaB() {
        return this.valorCuotaB;
    }
    
    public void setValorCuotaB(Double valorCuotaB) {
        this.valorCuotaB = valorCuotaB;
    }
    
    @Column(name="VALORCUOTAARDT", nullable=false, precision=16, scale=10)
    public Double getValorCuotaARDT() {
        return this.valorCuotaARDT;
    }
    
    public void setValorCuotaARDT(Double valorCuotaARDT) {
        this.valorCuotaARDT = valorCuotaARDT;
    }
    
    @Column(name="VALORCUOTAARDV", nullable=false, precision=16, scale=10)
    public Double getValorCuotaARDV() {
        return this.valorCuotaARDV;
    }
    
    public void setValorCuota(Double valorCuotaARDV) {
        this.valorCuotaARDV = valorCuotaARDV;
    }
    @Column(name="VALORCUOTA", nullable=false, precision=16, scale=10)
    public Double getValorCuota() {
        return this.valorCuota;
    }
    
    public void setValorCuotaARDV(Double valorCuota) {
        this.valorCuota = valorCuota;
    }
}