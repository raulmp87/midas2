package mx.com.afirme.midas.cotizacion.riesgo.dependencia;

// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * RiesgoDependenciaDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class RiesgoDependenciaId implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields

	private BigDecimal idToRiesgo;
	private BigDecimal idToRiesgoDependeDe;

	// Constructors

	/** default constructor */
	public RiesgoDependenciaId() {
	}

	/** full constructor */
	public RiesgoDependenciaId(BigDecimal idToRiesgo,
			BigDecimal idToRiesgoDependeDe) {
		this.idToRiesgo = idToRiesgo;
		this.idToRiesgoDependeDe = idToRiesgoDependeDe;
	}

	// Property accessors

	@Column(name = "IdToRiesgo", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToRiesgo() {
		return this.idToRiesgo;
	}

	public void setIdToRiesgo(BigDecimal idToRiesgo) {
		this.idToRiesgo = idToRiesgo;
	}

	@Column(name = "IdToRiesgoDependeDe", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToRiesgoDependeDe() {
		return this.idToRiesgoDependeDe;
	}

	public void setIdToRiesgoDependeDe(BigDecimal idToRiesgoDependeDe) {
		this.idToRiesgoDependeDe = idToRiesgoDependeDe;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof RiesgoDependenciaId))
			return false;
		RiesgoDependenciaId castOther = (RiesgoDependenciaId) other;

		return ((this.getIdToRiesgo() == castOther.getIdToRiesgo()) || (this
				.getIdToRiesgo() != null
				&& castOther.getIdToRiesgo() != null && this.getIdToRiesgo()
				.equals(castOther.getIdToRiesgo())))
				&& ((this.getIdToRiesgoDependeDe() == castOther
						.getIdToRiesgoDependeDe()) || (this
						.getIdToRiesgoDependeDe() != null
						&& castOther.getIdToRiesgoDependeDe() != null && this
						.getIdToRiesgoDependeDe().equals(
								castOther.getIdToRiesgoDependeDe())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToRiesgo() == null ? 0 : this.getIdToRiesgo()
						.hashCode());
		result = 37
				* result
				+ (getIdToRiesgoDependeDe() == null ? 0 : this
						.getIdToRiesgoDependeDe().hashCode());
		return result;
	}

}