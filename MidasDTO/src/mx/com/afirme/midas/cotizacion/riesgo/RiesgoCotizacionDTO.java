package mx.com.afirme.midas.cotizacion.riesgo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.aumento.AumentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.detalleprima.DetallePrimaRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaDTO;

/**
 * RiesgoCotizacionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TORIESGOCOT", schema = "MIDAS")
public class RiesgoCotizacionDTO implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private RiesgoCotizacionId id;
	private RiesgoCoberturaDTO riesgoCoberturaDTO;
	private CoberturaCotizacionDTO coberturaCotizacionDTO;
	private BigDecimal idTcSubramo;
	private Double valorPrimaNeta;
	private Double valorSumaAsegurada;
	private Double valorCoaseguro;
	private Double valorDeducible;
	private Double valorDescuento;
	private Double valorRecargo;
	private Double valorAumento;
	private Double porcentajeCoaseguro;
	private Short claveAutCoaseguro;
	private String codigoUsuarioAutCoaseguro;
	private Double porcentajeDeducible;
	private Short claveAutDeducible;
	private String codigoUsuarioAutDeducible;
	private Short claveObligatoriedad;
	private Short claveContrato;
	private List<DescuentoRiesgoCotizacionDTO> descuentoRiesgoCotizacionDTOs = new ArrayList<DescuentoRiesgoCotizacionDTO>();
	private List<AumentoRiesgoCotizacionDTO> aumentoRiesgoCotizacionDTOs = new ArrayList<AumentoRiesgoCotizacionDTO>();
	private List<RecargoRiesgoCotizacionDTO> recargoRiesgoCotizacionDTOs = new ArrayList<RecargoRiesgoCotizacionDTO>();
	private Double valorCuotaB;
	private Double valorCuotaARDT;
	private Double valorPrimaNetaARDT;
	private Double valorPrimaNetaB;
	
	private List<DetallePrimaRiesgoCotizacionDTO> detallePrimaRiesgoCotizacionList;
	private Short claveTipoDeducible;
	private Short claveTipoLimiteDeducible;
	private Double valorMinimoLimiteDeducible;
	private Double valorMaximoLimiteDeducible;

	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToCotizacion", column = @Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroInciso", column = @Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToSeccion", column = @Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToCobertura", column = @Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToRiesgo", column = @Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)) })
	public RiesgoCotizacionId getId() {
		return this.id;
	}

	public void setId(RiesgoCotizacionId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumns( {
			@JoinColumn(name = "IDTOSECCION", referencedColumnName = "IDTOSECCION", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTOCOBERTURA", referencedColumnName = "IDTOCOBERTURA", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTORIESGO", referencedColumnName = "IDTORIESGO", nullable = false, insertable = false, updatable = false) })
	public RiesgoCoberturaDTO getRiesgoCoberturaDTO() {
		return this.riesgoCoberturaDTO;
	}

	public void setRiesgoCoberturaDTO(RiesgoCoberturaDTO riesgoCoberturaDTO) {
		this.riesgoCoberturaDTO = riesgoCoberturaDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumns( {
			@JoinColumn(name = "IDTOCOTIZACION", referencedColumnName = "IDTOCOTIZACION", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "NUMEROINCISO", referencedColumnName = "NUMEROINCISO", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTOSECCION", referencedColumnName = "IDTOSECCION", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTOCOBERTURA", referencedColumnName = "IDTOCOBERTURA", nullable = false, insertable = false, updatable = false) })
	public CoberturaCotizacionDTO getCoberturaCotizacionDTO() {
		return this.coberturaCotizacionDTO;
	}

	public void setCoberturaCotizacionDTO(
			CoberturaCotizacionDTO coberturaCotizacionDTO) {
		this.coberturaCotizacionDTO = coberturaCotizacionDTO;
	}

	@Column(name = "IDTCSUBRAMO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcSubramo() {
		return this.idTcSubramo;
	}

	public void setIdTcSubramo(BigDecimal idTcSubramo) {
		this.idTcSubramo = idTcSubramo;
	}

	@Column(name = "VALORPRIMANETA", nullable = false, precision = 16)
	public Double getValorPrimaNeta() {
		return this.valorPrimaNeta;
	}

	public void setValorPrimaNeta(Double valorPrimaNeta) {
		this.valorPrimaNeta = valorPrimaNeta;
	}

	@Column(name = "VALORSUMAASEGURADA", nullable = false, precision = 16)
	public Double getValorSumaAsegurada() {
		return this.valorSumaAsegurada;
	}

	public void setValorSumaAsegurada(Double valorSumaAsegurada) {
		this.valorSumaAsegurada = valorSumaAsegurada;
	}

	@Column(name = "VALORCOASEGURO", nullable = false, precision = 16)
	public Double getValorCoaseguro() {
		return this.valorCoaseguro;
	}

	public void setValorCoaseguro(Double valorCoaseguro) {
		this.valorCoaseguro = valorCoaseguro;
	}

	@Column(name = "VALORDEDUCIBLE", nullable = false, precision = 16)
	public Double getValorDeducible() {
		return this.valorDeducible;
	}

	public void setValorDeducible(Double valorDeducible) {
		this.valorDeducible = valorDeducible;
	}

	@Column(name = "VALORDESCUENTO", nullable = false, precision = 16)
	public Double getValorDescuento() {
		return this.valorDescuento;
	}

	public void setValorDescuento(Double valorDescuento) {
		this.valorDescuento = valorDescuento;
	}

	@Column(name = "VALORRECARGO", nullable = false, precision = 16)
	public Double getValorRecargo() {
		return this.valorRecargo;
	}

	public void setValorRecargo(Double valorRecargo) {
		this.valorRecargo = valorRecargo;
	}

	@Column(name = "VALORAUMENTO", nullable = false, precision = 16)
	public Double getValorAumento() {
		return this.valorAumento;
	}

	public void setValorAumento(Double valorAumento) {
		this.valorAumento = valorAumento;
	}

	@Column(name = "PORCENTAJECOASEGURO", nullable = false)
	public Double getPorcentajeCoaseguro() {
		return this.porcentajeCoaseguro;
	}

	public void setPorcentajeCoaseguro(Double porcentajeCoaseguro) {
		this.porcentajeCoaseguro = porcentajeCoaseguro;
	}

	@Column(name = "CLAVEAUTCOASEGURO", nullable = false, precision = 4, scale = 0)
	public Short getClaveAutCoaseguro() {
		return this.claveAutCoaseguro;
	}

	public void setClaveAutCoaseguro(Short claveAutCoaseguro) {
		this.claveAutCoaseguro = claveAutCoaseguro;
	}

	@Column(name = "CODIGOUSUARIOAUTCOASEGURO", length = 8)
	public String getCodigoUsuarioAutCoaseguro() {
		return this.codigoUsuarioAutCoaseguro;
	}

	public void setCodigoUsuarioAutCoaseguro(String codigoUsuarioAutCoaseguro) {
		this.codigoUsuarioAutCoaseguro = codigoUsuarioAutCoaseguro;
	}

	@Column(name = "PORCENTAJEDEDUCIBLE", nullable = false)
	public Double getPorcentajeDeducible() {
		return this.porcentajeDeducible;
	}

	public void setPorcentajeDeducible(Double porcentajeDeducible) {
		this.porcentajeDeducible = porcentajeDeducible;
	}

	@Column(name = "CLAVEAUTDEDUCIBLE", nullable = false, precision = 4, scale = 0)
	public Short getClaveAutDeducible() {
		return this.claveAutDeducible;
	}

	public void setClaveAutDeducible(Short claveAutDeducible) {
		this.claveAutDeducible = claveAutDeducible;
	}

	@Column(name = "CODIGOUSUARIOAUTDEDUCIBLE", length = 8)
	public String getCodigoUsuarioAutDeducible() {
		return this.codigoUsuarioAutDeducible;
	}

	public void setCodigoUsuarioAutDeducible(String codigoUsuarioAutDeducible) {
		this.codigoUsuarioAutDeducible = codigoUsuarioAutDeducible;
	}

	@Column(name = "CLAVEOBLIGATORIEDAD", nullable = false, precision = 4, scale = 0)
	public Short getClaveObligatoriedad() {
		return this.claveObligatoriedad;
	}

	public void setClaveObligatoriedad(Short claveObligatoriedad) {
		this.claveObligatoriedad = claveObligatoriedad;
	}

	@Column(name = "CLAVECONTRATO", nullable = false, precision = 4, scale = 0)
	public Short getClaveContrato() {
		return this.claveContrato;
	}

	public void setClaveContrato(Short claveContrato) {
		this.claveContrato = claveContrato;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "riesgoCotizacionDTO")
	public List<DescuentoRiesgoCotizacionDTO> getDescuentoRiesgoCotizacionDTOs() {
		return this.descuentoRiesgoCotizacionDTOs;
	}

	public void setDescuentoRiesgoCotizacionDTOs(
			List<DescuentoRiesgoCotizacionDTO> descuentoRiesgoCotizacionDTOs) {
		this.descuentoRiesgoCotizacionDTOs = descuentoRiesgoCotizacionDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "riesgoCotizacionDTO")
	public List<AumentoRiesgoCotizacionDTO> getAumentoRiesgoCotizacionDTOs() {
		return this.aumentoRiesgoCotizacionDTOs;
	}

	public void setAumentoRiesgoCotizacionDTOs(
			List<AumentoRiesgoCotizacionDTO> aumentoRiesgoCotizacionDTOs) {
		this.aumentoRiesgoCotizacionDTOs = aumentoRiesgoCotizacionDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "riesgoCotizacionDTO")
	public List<RecargoRiesgoCotizacionDTO> getRecargoRiesgoCotizacionDTOs() {
		return this.recargoRiesgoCotizacionDTOs;
	}

	public void setRecargoRiesgoCotizacionDTOs(
			List<RecargoRiesgoCotizacionDTO> recargoRiesgoCotizacionDTOs) {
		this.recargoRiesgoCotizacionDTOs = recargoRiesgoCotizacionDTOs;
	}
	
	@Column(name = "VALORCUOTAB", nullable = false, precision = 16, scale = 10)
	public Double getValorCuotaB() {
		return this.valorCuotaB;
	}

	public void setValorCuotaB(Double valorCuotaB) {
		this.valorCuotaB = valorCuotaB;
	}

	@Column(name = "VALORCUOTAARDT", nullable = false, precision = 16, scale = 10)
	public Double getValorCuotaARDT() {
		return this.valorCuotaARDT;
	}

	public void setValorCuotaARDT(Double valorCuotaARDT) {
		this.valorCuotaARDT = valorCuotaARDT;
	}

	@Column(name = "VALORPRIMANETAARDT", nullable = false, precision = 16)
	public Double getValorPrimaNetaARDT() {
		return this.valorPrimaNetaARDT;
	}

	public void setValorPrimaNetaARDT(Double valorPrimaNetaARDT) {
		this.valorPrimaNetaARDT = valorPrimaNetaARDT;
	}

	@Column(name = "VALORPRIMANETAB", nullable = false, precision = 16)
	public Double getValorPrimaNetaB() {
		return this.valorPrimaNetaB;
	}

	public void setValorPrimaNetaB(Double valorPrimaNetaB) {
		this.valorPrimaNetaB = valorPrimaNetaB;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "riesgoCotizacionDTO")
	public List<DetallePrimaRiesgoCotizacionDTO> getDetallePrimaRiesgoCotizacionList() {
		return detallePrimaRiesgoCotizacionList;
	}
	public void setDetallePrimaRiesgoCotizacionList(
			List<DetallePrimaRiesgoCotizacionDTO> detallePrimaRiesgoCotizacionList) {
		this.detallePrimaRiesgoCotizacionList = detallePrimaRiesgoCotizacionList;
	}

	@Column(name="CLAVETIPODEDUCIBLE")
	public Short getClaveTipoDeducible() {
		return claveTipoDeducible;
	}

	public void setClaveTipoDeducible(Short claveTipoDeducible) {
		this.claveTipoDeducible = claveTipoDeducible;
	}

	@Column(name="CLAVETIPOLIMITEDEDUCIBLE")
	public Short getClaveTipoLimiteDeducible() {
		return claveTipoLimiteDeducible;
	}

	public void setClaveTipoLimiteDeducible(Short claveTipoLimiteDeducible) {
		this.claveTipoLimiteDeducible = claveTipoLimiteDeducible;
	}

	@Column(name="VALORMINIMOLIMITEDEDUCIBLE")
	public Double getValorMinimoLimiteDeducible() {
		return valorMinimoLimiteDeducible;
	}

	public void setValorMinimoLimiteDeducible(Double valorMinimoLimiteDeducible) {
		this.valorMinimoLimiteDeducible = valorMinimoLimiteDeducible;
	}

	@Column(name="VALORMAXIMOLIMITEDEDUCIBLE")
	public Double getValorMaximoLimiteDeducible() {
		return valorMaximoLimiteDeducible;
	}

	public void setValorMaximoLimiteDeducible(Double valorMaximoLimiteDeducible) {
		this.valorMaximoLimiteDeducible = valorMaximoLimiteDeducible;
	}
}