package mx.com.afirme.midas.cotizacion.riesgo.recargo;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDTO;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDTO;

/**
 * RecargoRiesgoCotizacionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TORECARGORIESGOCOT", schema = "MIDAS")
public class RecargoRiesgoCotizacionDTO implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private RecargoRiesgoCotizacionId id;
	private RiesgoCotizacionDTO riesgoCotizacionDTO;
	private RecargoVarioDTO recargoVarioDTO;
	private Short claveAutorizacion;
	private String codigoUsuarioAutorizacion;
	private Double valorRecargo;
	private Short claveObligatoriedad;
	private Short claveContrato;
	private Short claveTipo;
	private Short claveComercialTecnico;
	private Short claveNivel;

	// Constructors

	/** default constructor */
	public RecargoRiesgoCotizacionDTO() {
	}

	/** minimal constructor */
	public RecargoRiesgoCotizacionDTO(RecargoRiesgoCotizacionId id,
			RiesgoCotizacionDTO riesgoCotizacionDTO,
			RecargoVarioDTO recargoVarioDTO, Short claveAutorizacion,
			Double valorRecargo, Short claveObligatoriedad, Short claveContrato) {
		this.id = id;
		this.riesgoCotizacionDTO = riesgoCotizacionDTO;
		this.recargoVarioDTO = recargoVarioDTO;
		this.claveAutorizacion = claveAutorizacion;
		this.valorRecargo = valorRecargo;
		this.claveObligatoriedad = claveObligatoriedad;
		this.claveContrato = claveContrato;
	}

	/** full constructor */
	public RecargoRiesgoCotizacionDTO(RecargoRiesgoCotizacionId id,
			RiesgoCotizacionDTO riesgoCotizacionDTO,
			RecargoVarioDTO recargoVarioDTO, Short claveAutorizacion,
			String codigoUsuarioAutorizacion, Double valorRecargo,
			Short claveObligatoriedad, Short claveContrato) {
		this.id = id;
		this.riesgoCotizacionDTO = riesgoCotizacionDTO;
		this.recargoVarioDTO = recargoVarioDTO;
		this.claveAutorizacion = claveAutorizacion;
		this.codigoUsuarioAutorizacion = codigoUsuarioAutorizacion;
		this.valorRecargo = valorRecargo;
		this.claveObligatoriedad = claveObligatoriedad;
		this.claveContrato = claveContrato;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToCotizacion", column = @Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroInciso", column = @Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToSeccion", column = @Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToCobertura", column = @Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToRiesgo", column = @Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToRecargoVario", column = @Column(name = "IDTORECARGOVARIO", nullable = false, precision = 22, scale = 0)) })
	public RecargoRiesgoCotizacionId getId() {
		return this.id;
	}

	public void setId(RecargoRiesgoCotizacionId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( {
			@JoinColumn(name = "IDTOCOTIZACION", referencedColumnName = "IDTOCOTIZACION", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "NUMEROINCISO", referencedColumnName = "NUMEROINCISO", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTOSECCION", referencedColumnName = "IDTOSECCION", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTOCOBERTURA", referencedColumnName = "IDTOCOBERTURA", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTORIESGO", referencedColumnName = "IDTORIESGO", nullable = false, insertable = false, updatable = false) })
	public RiesgoCotizacionDTO getRiesgoCotizacionDTO() {
		return this.riesgoCotizacionDTO;
	}

	public void setRiesgoCotizacionDTO(RiesgoCotizacionDTO riesgoCotizacionDTO) {
		this.riesgoCotizacionDTO = riesgoCotizacionDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTORECARGOVARIO", nullable = false, insertable = false, updatable = false)
	public RecargoVarioDTO getRecargoVarioDTO() {
		return this.recargoVarioDTO;
	}

	public void setRecargoVarioDTO(RecargoVarioDTO recargoVarioDTO) {
		this.recargoVarioDTO = recargoVarioDTO;
	}

	@Column(name = "CLAVEAUTORIZACION", nullable = false, precision = 4, scale = 0)
	public Short getClaveAutorizacion() {
		return this.claveAutorizacion;
	}

	public void setClaveAutorizacion(Short claveAutorizacion) {
		this.claveAutorizacion = claveAutorizacion;
	}

	@Column(name = "CODIGOUSUARIOAUTORIZACION", length = 8)
	public String getCodigoUsuarioAutorizacion() {
		return this.codigoUsuarioAutorizacion;
	}

	public void setCodigoUsuarioAutorizacion(String codigoUsuarioAutorizacion) {
		this.codigoUsuarioAutorizacion = codigoUsuarioAutorizacion;
	}

	@Column(name = "VALORRECARGO", nullable = false)
	public Double getValorRecargo() {
		return this.valorRecargo;
	}

	public void setValorRecargo(Double valorRecargo) {
		this.valorRecargo = valorRecargo;
	}

	@Column(name = "CLAVEOBLIGATORIEDAD", nullable = false, precision = 4, scale = 0)
	public Short getClaveObligatoriedad() {
		return this.claveObligatoriedad;
	}

	public void setClaveObligatoriedad(Short claveObligatoriedad) {
		this.claveObligatoriedad = claveObligatoriedad;
	}

	@Column(name = "CLAVECONTRATO", nullable = false, precision = 4, scale = 0)
	public Short getClaveContrato() {
		return this.claveContrato;
	}

	public void setClaveContrato(Short claveContrato) {
		this.claveContrato = claveContrato;
	}

	@Column(name = "CLAVECOMERCIALTECNICO", nullable = false, precision = 4, scale = 0)
	public Short getClaveComercialTecnico() {
		return claveComercialTecnico;
	}

	public void setClaveComercialTecnico(Short claveComercialTecnico) {
		this.claveComercialTecnico = claveComercialTecnico;
	}

	@Column(name = "CLAVENIVEL", nullable = false, precision = 4, scale = 0)
	public Short getClaveNivel() {
		return claveNivel;
	}

	public void setClaveNivel(Short claveNivel) {
		this.claveNivel = claveNivel;
	}

	@Transient
	public Short getClaveTipo() {
		return claveTipo;
	}

	public void setClaveTipo(Short claveTipo) {
		this.claveTipo = claveTipo;
	}
}