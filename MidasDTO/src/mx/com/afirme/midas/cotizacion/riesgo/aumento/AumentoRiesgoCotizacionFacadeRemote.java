package mx.com.afirme.midas.cotizacion.riesgo.aumento;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;

/**
 * Remote interface for AumentoRiesgoCotizacionDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface AumentoRiesgoCotizacionFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved
	 * AumentoRiesgoCotizacionDTO entity. All subsequent persist actions of this
	 * entity should use the #update() method.
	 * 
	 * @param entity
	 *            AumentoRiesgoCotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(AumentoRiesgoCotizacionDTO entity);

	/**
	 * Delete a persistent AumentoRiesgoCotizacionDTO entity.
	 * 
	 * @param entity
	 *            AumentoRiesgoCotizacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(AumentoRiesgoCotizacionDTO entity);

	/**
	 * Persist a previously saved AumentoRiesgoCotizacionDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * AumentoRiesgoCotizacionDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            AumentoRiesgoCotizacionDTO entity to update
	 * @return AumentoRiesgoCotizacionDTO the persisted
	 *         AumentoRiesgoCotizacionDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public AumentoRiesgoCotizacionDTO update(AumentoRiesgoCotizacionDTO entity);

	public AumentoRiesgoCotizacionDTO findById(AumentoRiesgoCotizacionId id);

	/**
	 * Find all AumentoRiesgoCotizacionDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the AumentoRiesgoCotizacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<AumentoRiesgoCotizacionDTO> found by query
	 */
	public List<AumentoRiesgoCotizacionDTO> findByProperty(String propertyName,
			Object value);

	public List<AumentoRiesgoCotizacionDTO> findByClaveAutorizacion(
			Object claveAutorizacion);

	public List<AumentoRiesgoCotizacionDTO> findByCodigoUsuarioAutorizacion(
			Object codigoUsuarioAutorizacion);

	public List<AumentoRiesgoCotizacionDTO> findByValorAumento(
			Object valorAumento);

	public List<AumentoRiesgoCotizacionDTO> findByClaveObligatoriedad(
			Object claveObligatoriedad);

	public List<AumentoRiesgoCotizacionDTO> findByClaveContrato(
			Object claveContrato);

	/**
	 * Find all AumentoRiesgoCotizacionDTO entities.
	 * 
	 * @return List<AumentoRiesgoCotizacionDTO> all AumentoRiesgoCotizacionDTO
	 *         entities
	 */
	public List<AumentoRiesgoCotizacionDTO> findAll();

	/**
	 * findByRiesgoCotizacion. Encuentra la lista de entidades
	 * AumentoRiesgoCotizacionDTO que tengan los atributos recibidos en el
	 * objeto riesgoCotId. Los atributos usados para la b�squeda son los
	 * siguientes: idToCotizacion, numeroInciso, idToSeccion, idToCobertura,
	 * idToRiesgo.
	 * 
	 * @param RiesgoCotizacionId
	 * @return List<AumentoRiesgoCotizacionDTO>
	 */
	public List<AumentoRiesgoCotizacionDTO> findByRiesgoCotizacion(
			RiesgoCotizacionId riesgoCotId);

	public List<AumentoRiesgoCotizacionDTO> findByCoberturaCotizacion(
			RiesgoCotizacionId riesgoCotId);

	public List<AumentoRiesgoCotizacionDTO> listarAumentosPendientesAutorizacion(BigDecimal idToCotizacion);
}