package mx.com.afirme.midas.cotizacion.riesgo.detalleprima;
// default package

import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTOId;

/**
 * Remote interface for DetPrimaRiesgoCotizacionDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface DetallePrimaRiesgoCotizacionFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved DetPrimaRiesgoCotizacionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DetPrimaRiesgoCotizacionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DetallePrimaRiesgoCotizacionDTO entity);
    /**
	 Delete a persistent DetPrimaRiesgoCotizacionDTO entity.
	  @param entity DetPrimaRiesgoCotizacionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DetallePrimaRiesgoCotizacionDTO entity);
   /**
	 Persist a previously saved DetPrimaRiesgoCotizacionDTO entity and return it or a copy of it to the sender. 
	 A copy of the DetPrimaRiesgoCotizacionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DetPrimaRiesgoCotizacionDTO entity to update
	 @return DetPrimaRiesgoCotizacionDTO the persisted DetPrimaRiesgoCotizacionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public DetallePrimaRiesgoCotizacionDTO update(DetallePrimaRiesgoCotizacionDTO entity);
	public DetallePrimaRiesgoCotizacionDTO findById( DetallePrimaRiesgoCotizacionId id);
	 /**
	 * Find all DetPrimaRiesgoCotizacionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DetPrimaRiesgoCotizacionDTO property to query
	  @param value the property value to match
	  	  @return List<DetPrimaRiesgoCotizacionDTO> found by query
	 */
	public List<DetallePrimaRiesgoCotizacionDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all DetPrimaRiesgoCotizacionDTO entities.
	  	  @return List<DetPrimaRiesgoCotizacionDTO> all DetPrimaRiesgoCotizacionDTO entities
	 */
	public List<DetallePrimaRiesgoCotizacionDTO> findAll(
		);
	
	/**
	 * 	findByRiesgoCotizacion. Encuentra la lista de entidades DetallePrimaRiesgoCotizacionDTO que tengan los atributos 
	 * recibidos en el objeto riesgoCotizacionId. Los atributos usados para la b�squeda son los siguientes: idToCotizacion, 
	 * numeroInciso, idToSeccion, idToCobertura, idToRiesgo.
	 * @param RiesgoCotizacionId  
	 * @return List<DetallePrimaRiesgoCotizacionDTO>
	 */
	public List<DetallePrimaRiesgoCotizacionDTO> findByRiesgoCotizacion(RiesgoCotizacionId riesgoCotId);

	public List<DetallePrimaRiesgoCotizacionDTO> findBySubInciso(SubIncisoCotizacionDTOId id);

	public List<DetallePrimaRiesgoCotizacionDTO> listarFiltado(DetallePrimaRiesgoCotizacionId detallePrimaRiesgoCotizacionId);

	public void deleteDetallePrimaRiesgoCotizacion(RiesgoCotizacionId riesgoCotId);
	public void borrarFiltrado(DetallePrimaRiesgoCotizacionId detallePrimaRiesgoCotizacionId);
}