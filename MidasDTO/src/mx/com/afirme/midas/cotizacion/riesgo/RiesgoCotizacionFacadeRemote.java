package mx.com.afirme.midas.cotizacion.riesgo;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;
import mx.com.afirme.midas.producto.configuracion.aumento.AumentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.descuento.DescuentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.recargo.RecargoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento.AumentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento.DescuentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.RecargoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.AumentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.DescuentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.RecargoVarioCoberturaDTO;
import mx.com.afirme.midas.sistema.SystemException;

/**
 * Remote interface for RiesgoCotizacionDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface RiesgoCotizacionFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved RiesgoCotizacionDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            RiesgoCotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(RiesgoCotizacionDTO entity);

	/**
	 * Delete a persistent RiesgoCotizacionDTO entity.
	 * 
	 * @param entity
	 *            RiesgoCotizacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(RiesgoCotizacionDTO entity);

	/**
	 * Persist a previously saved RiesgoCotizacionDTO entity and return it or a
	 * copy of it to the sender. A copy of the RiesgoCotizacionDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            RiesgoCotizacionDTO entity to update
	 * @return RiesgoCotizacionDTO the persisted RiesgoCotizacionDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public RiesgoCotizacionDTO update(RiesgoCotizacionDTO entity);

	public RiesgoCotizacionDTO findById(RiesgoCotizacionId id);

	/**
	 * Find all RiesgoCotizacionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the RiesgoCotizacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<RiesgoCotizacionDTO> found by query
	 */
	public List<RiesgoCotizacionDTO> findByProperty(String propertyName,
			Object value);

	public List<RiesgoCotizacionDTO> findByValorPrimaNeta(Object valorPrimaNeta);

	public List<RiesgoCotizacionDTO> findByValorSumaAsegurada(
			Object valorSumaAsegurada);

	public List<RiesgoCotizacionDTO> findByValorCoaseguro(Object valorCoaseguro);

	public List<RiesgoCotizacionDTO> findByValorDeducible(Object valorDeducible);

	public List<RiesgoCotizacionDTO> findByValorDescuento(Object valorDescuento);

	public List<RiesgoCotizacionDTO> findByValorRecargo(Object valorRecargo);

	public List<RiesgoCotizacionDTO> findByValorAumento(Object valorAumento);

	public List<RiesgoCotizacionDTO> findByPorcentajeCoaseguro(
			Object porcentajeCoaseguro);

	public List<RiesgoCotizacionDTO> findByClaveAutCoaseguro(
			Object claveAutCoaseguro);

	public List<RiesgoCotizacionDTO> findByCodigoUsuarioAutCoaseguro(
			Object codigoUsuarioAutCoaseguro);

	public List<RiesgoCotizacionDTO> findByPorcentajeDeducible(
			Object porcentajeDeducible);

	public List<RiesgoCotizacionDTO> findByClaveAutDeducible(
			Object claveAutDeducible);

	public List<RiesgoCotizacionDTO> findByCodigoUsuarioAutDeducible(
			Object codigoUsuarioAutDeducible);

	public List<RiesgoCotizacionDTO> findByClaveObligatoriedad(
			Object claveObligatoriedad);

	public List<RiesgoCotizacionDTO> findByClaveContrato(Object claveContrato);

	/**
	 * Find all RiesgoCotizacionDTO entities.
	 * 
	 * @return List<RiesgoCotizacionDTO> all RiesgoCotizacionDTO entities
	 */
	public List<RiesgoCotizacionDTO> findAll();

	public List<DescuentoVarioCoberturaDTO> getDescuentosCobertura(BigDecimal idToCobertura, BigDecimal idToRiesgo);

	public List<AumentoVarioCoberturaDTO> getAumentosCobertura(BigDecimal idToCobertura, BigDecimal idToRiesgo);

	public List<RecargoVarioCoberturaDTO> getRecargosCobertura(BigDecimal idToCobertura, BigDecimal idToRiesgo);

	public List<DescuentoVarioTipoPolizaDTO> getDescuentosTipoPoliza(BigDecimal idToTipoPoliza, BigDecimal idToCobertura);

	public List<AumentoVarioTipoPolizaDTO> getAumentosTipoPoliza(BigDecimal idToTipoPoliza, BigDecimal idToCobertura);

	public List<RecargoVarioTipoPolizaDTO> getRecargosTipoPoliza(BigDecimal idToTipoPoliza, BigDecimal idToCobertura);

	public List<DescuentoVarioProductoDTO> getDescuentosProducto(BigDecimal idToProducto, BigDecimal idToTipoPoliza);

	public List<AumentoVarioProductoDTO> getAumentosProducto(BigDecimal idToProducto, BigDecimal idToTipoPoliza);

	public List<RecargoVarioProductoDTO> getRecargosProducto(BigDecimal idToProducto, BigDecimal idToTipoPoliza);

	public List<RiesgoCotizacionDTO> listarRiesgoContratados(CoberturaCotizacionId id);

	public List<RiesgoCotizacionDTO> listarFiltrado(RiesgoCotizacionDTO entity);

	/**
	 * lista las entidades de RiesgoCotizacionDTO que tengan lo atributos recibidos en el objeto RiesgoCotizacionId.
	 * Los atributos utilizados como filtro son: idToCotizacion, idToCobertura, idToSeccion, idToRiesgo y numeroInciso.
	 * @param id
	 * @return List<RiesgoCotizacionDTO>
	 */
	public List<RiesgoCotizacionDTO> listarPorIdFiltrado(RiesgoCotizacionId id);
	public List<RiesgoCotizacionDTO> listarRiesgoContratadosPorCobertura(CoberturaCotizacionId id);

	public Long obtenerTotalRiesgoContratadosPorCobertura(CoberturaCotizacionId id);
	
	public List<RiesgoCotizacionDTO> listarRiesgoContratadosPorSeccion(SeccionCotizacionDTOId id);

	public void insertarARD(BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idToProducto, BigDecimal idToTipoPoliza);

	public List<BigDecimal> obtenerRiesgosContratadosCotizacion(BigDecimal idToCotizacion);
	
	public List<RiesgoCotizacionDTO> listarRiesgoContratadosPorCotizacion(BigDecimal idToCotizacion);

	public List<RiesgoCotizacionDTO> listarRiesgosConCoasegurosDeduciblesPendientes(BigDecimal idToCotizacion);
	
	/**
	 * Method listarRiesgosContratadosPorCotizacion()
	 * @param idToCotizacion
	 * @return List<RiesgoCotizacionDTO>
	 * @throws SystemException
	 */
	public List<RiesgoCotizacionDTO> listarRiesgosContratadosPorCotizacion(BigDecimal idToCotizacion);
}