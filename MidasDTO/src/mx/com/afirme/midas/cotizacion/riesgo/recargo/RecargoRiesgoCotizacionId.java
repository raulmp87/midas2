package mx.com.afirme.midas.cotizacion.riesgo.recargo;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * RecargoRiesgoCotizacionId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class RecargoRiesgoCotizacionId implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private BigDecimal idToCotizacion;
	private BigDecimal numeroInciso;
	private BigDecimal idToSeccion;
	private BigDecimal idToCobertura;
	private BigDecimal idToRiesgo;
	private BigDecimal idToRecargoVario;

	// Constructors

	/** default constructor */
	public RecargoRiesgoCotizacionId() {
	}

	/** full constructor */
	public RecargoRiesgoCotizacionId(BigDecimal idToCotizacion,
			BigDecimal numeroInciso, BigDecimal idToSeccion,
			BigDecimal idToCobertura, BigDecimal idToRiesgo,
			BigDecimal idToRecargoVario) {
		this.idToCotizacion = idToCotizacion;
		this.numeroInciso = numeroInciso;
		this.idToSeccion = idToSeccion;
		this.idToCobertura = idToCobertura;
		this.idToRiesgo = idToRiesgo;
		this.idToRecargoVario = idToRecargoVario;
	}

	// Property accessors

	@Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCotizacion() {
		return this.idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	@Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getNumeroInciso() {
		return this.numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	@Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSeccion() {
		return this.idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	@Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCobertura() {
		return this.idToCobertura;
	}

	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	@Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToRiesgo() {
		return this.idToRiesgo;
	}

	public void setIdToRiesgo(BigDecimal idToRiesgo) {
		this.idToRiesgo = idToRiesgo;
	}

	@Column(name = "IDTORECARGOVARIO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToRecargoVario() {
		return this.idToRecargoVario;
	}

	public void setIdToRecargoVario(BigDecimal idToRecargoVario) {
		this.idToRecargoVario = idToRecargoVario;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof RecargoRiesgoCotizacionId))
			return false;
		RecargoRiesgoCotizacionId castOther = (RecargoRiesgoCotizacionId) other;

		return ((this.getIdToCotizacion() == castOther.getIdToCotizacion()) || (this
				.getIdToCotizacion() != null
				&& castOther.getIdToCotizacion() != null && this
				.getIdToCotizacion().equals(castOther.getIdToCotizacion())))
				&& ((this.getNumeroInciso() == castOther.getNumeroInciso()) || (this
						.getNumeroInciso() != null
						&& castOther.getNumeroInciso() != null && this
						.getNumeroInciso().equals(castOther.getNumeroInciso())))
				&& ((this.getIdToSeccion() == castOther.getIdToSeccion()) || (this
						.getIdToSeccion() != null
						&& castOther.getIdToSeccion() != null && this
						.getIdToSeccion().equals(castOther.getIdToSeccion())))
				&& ((this.getIdToCobertura() == castOther.getIdToCobertura()) || (this
						.getIdToCobertura() != null
						&& castOther.getIdToCobertura() != null && this
						.getIdToCobertura()
						.equals(castOther.getIdToCobertura())))
				&& ((this.getIdToRiesgo() == castOther.getIdToRiesgo()) || (this
						.getIdToRiesgo() != null
						&& castOther.getIdToRiesgo() != null && this
						.getIdToRiesgo().equals(castOther.getIdToRiesgo())))
				&& ((this.getIdToRecargoVario() == castOther
						.getIdToRecargoVario()) || (this.getIdToRecargoVario() != null
						&& castOther.getIdToRecargoVario() != null && this
						.getIdToRecargoVario().equals(
								castOther.getIdToRecargoVario())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToCotizacion() == null ? 0 : this.getIdToCotizacion()
						.hashCode());
		result = 37
				* result
				+ (getNumeroInciso() == null ? 0 : this.getNumeroInciso()
						.hashCode());
		result = 37
				* result
				+ (getIdToSeccion() == null ? 0 : this.getIdToSeccion()
						.hashCode());
		result = 37
				* result
				+ (getIdToCobertura() == null ? 0 : this.getIdToCobertura()
						.hashCode());
		result = 37
				* result
				+ (getIdToRiesgo() == null ? 0 : this.getIdToRiesgo()
						.hashCode());
		result = 37
				* result
				+ (getIdToRecargoVario() == null ? 0 : this
						.getIdToRecargoVario().hashCode());
		return result;
	}

}