package mx.com.afirme.midas.cotizacion.riesgo.recargo;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;

/**
 * Remote interface for RecargoRiesgoCotizacionDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface RecargoRiesgoCotizacionFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved
	 * RecargoRiesgoCotizacionDTO entity. All subsequent persist actions of this
	 * entity should use the #update() method.
	 * 
	 * @param entity
	 *            RecargoRiesgoCotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(RecargoRiesgoCotizacionDTO entity);

	/**
	 * Delete a persistent RecargoRiesgoCotizacionDTO entity.
	 * 
	 * @param entity
	 *            RecargoRiesgoCotizacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(RecargoRiesgoCotizacionDTO entity);

	/**
	 * Persist a previously saved RecargoRiesgoCotizacionDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * RecargoRiesgoCotizacionDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            RecargoRiesgoCotizacionDTO entity to update
	 * @return RecargoRiesgoCotizacionDTO the persisted
	 *         RecargoRiesgoCotizacionDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public RecargoRiesgoCotizacionDTO update(RecargoRiesgoCotizacionDTO entity);

	public RecargoRiesgoCotizacionDTO findById(RecargoRiesgoCotizacionId id);

	/**
	 * Find all RecargoRiesgoCotizacionDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the RecargoRiesgoCotizacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<RecargoRiesgoCotizacionDTO> found by query
	 */
	public List<RecargoRiesgoCotizacionDTO> findByProperty(String propertyName,
			Object value);

	public List<RecargoRiesgoCotizacionDTO> findByClaveAutorizacion(
			Object claveAutorizacion);

	public List<RecargoRiesgoCotizacionDTO> findByCodigoUsuarioAutorizacion(
			Object codigoUsuarioAutorizacion);

	public List<RecargoRiesgoCotizacionDTO> findByValorRecargo(
			Object valorRecargo);

	public List<RecargoRiesgoCotizacionDTO> findByClaveObligatoriedad(
			Object claveObligatoriedad);

	public List<RecargoRiesgoCotizacionDTO> findByClaveContrato(
			Object claveContrato);

	/**
	 * Find all RecargoRiesgoCotizacionDTO entities.
	 * 
	 * @return List<RecargoRiesgoCotizacionDTO> all RecargoRiesgoCotizacionDTO
	 *         entities
	 */
	public List<RecargoRiesgoCotizacionDTO> findAll();

	/**
	 * findByRiesgoCotizacion. Encuentra la lista de entidades
	 * RecargoRiesgoCotizacionDTO que tengan los atributos recibidos en el
	 * objeto riesgoCotId. Los atributos usados para la b�squeda son los
	 * siguientes: idToCotizacion, numeroInciso, idToSeccion, idToCobertura,
	 * idToRiesgo.
	 * 
	 * @param RiesgoCotizacionId
	 * @return List<RecargoRiesgoCotizacionDTO>
	 */
	public List<RecargoRiesgoCotizacionDTO> findByRiesgoCotizacion(
			RiesgoCotizacionId riesgoCotId);

	public List<RecargoRiesgoCotizacionDTO> findByCoberturaCotizacion(
			RiesgoCotizacionId riesgoCotId);

	public List<RecargoRiesgoCotizacionDTO> listarRecargosPendientesAutorizacion(BigDecimal idToCotizacion);
}