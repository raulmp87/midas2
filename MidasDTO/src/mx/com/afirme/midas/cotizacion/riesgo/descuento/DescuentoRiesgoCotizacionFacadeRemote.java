package mx.com.afirme.midas.cotizacion.riesgo.descuento;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;

/**
 * Remote interface for DescuentoRiesgoCotizacionDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface DescuentoRiesgoCotizacionFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved
	 * DescuentoRiesgoCotizacionDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            DescuentoRiesgoCotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(DescuentoRiesgoCotizacionDTO entity);

	/**
	 * Delete a persistent DescuentoRiesgoCotizacionDTO entity.
	 * 
	 * @param entity
	 *            DescuentoRiesgoCotizacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(DescuentoRiesgoCotizacionDTO entity);

	/**
	 * Persist a previously saved DescuentoRiesgoCotizacionDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * DescuentoRiesgoCotizacionDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            DescuentoRiesgoCotizacionDTO entity to update
	 * @return DescuentoRiesgoCotizacionDTO the persisted
	 *         DescuentoRiesgoCotizacionDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public DescuentoRiesgoCotizacionDTO update(
			DescuentoRiesgoCotizacionDTO entity);

	public DescuentoRiesgoCotizacionDTO findById(DescuentoRiesgoCotizacionId id);

	/**
	 * Find all DescuentoRiesgoCotizacionDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the DescuentoRiesgoCotizacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<DescuentoRiesgoCotizacionDTO> found by query
	 */
	public List<DescuentoRiesgoCotizacionDTO> findByProperty(
			String propertyName, Object value);

	public List<DescuentoRiesgoCotizacionDTO> findByClaveAutorizacion(
			Object claveAutorizacion);

	public List<DescuentoRiesgoCotizacionDTO> findByCodigoUsuarioAutorizacion(
			Object codigoUsuarioAutorizacion);

	public List<DescuentoRiesgoCotizacionDTO> findByValorDescuento(
			Object valorDescuento);

	public List<DescuentoRiesgoCotizacionDTO> findByClaveObligatoriedad(
			Object claveObligatoriedad);

	public List<DescuentoRiesgoCotizacionDTO> findByClaveContrato(
			Object claveContrato);

	/**
	 * Find all DescuentoRiesgoCotizacionDTO entities.
	 * 
	 * @return List<DescuentoRiesgoCotizacionDTO> all
	 *         DescuentoRiesgoCotizacionDTO entities
	 */
	public List<DescuentoRiesgoCotizacionDTO> findAll();

	/**
	 * findByRiesgoCotizacion. Encuentra la lista de entidades
	 * DescuentoRiesgoCotizacionDTO que tengan los atributos recibidos en el
	 * objeto riesgoCotId. Los atributos usados para la b�squeda son los
	 * siguientes: idToCotizacion, numeroInciso, idToSeccion, idToCobertura,
	 * idToRiesgo.
	 * 
	 * @param RiesgoCotizacionId
	 * @return List<DescuentoRiesgoCotizacionDTO>
	 */
	public List<DescuentoRiesgoCotizacionDTO> findByRiesgoCotizacion(
			RiesgoCotizacionId riesgoCotId);

	public List<DescuentoRiesgoCotizacionDTO> findByCoberturaCotizacion(
			RiesgoCotizacionId riesgoCotId);

	public List<DescuentoRiesgoCotizacionDTO> listarDescuentosPendientesAutorizacion(BigDecimal idToCotizacion);
}