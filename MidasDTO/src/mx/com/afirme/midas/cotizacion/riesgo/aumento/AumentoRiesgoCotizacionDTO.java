package mx.com.afirme.midas.cotizacion.riesgo.aumento;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.aumentovario.AumentoVarioDTO;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDTO;

/**
 * AumentoRiesgoCotizacionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOAUMENTORIESGOCOT", schema = "MIDAS")
public class AumentoRiesgoCotizacionDTO implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private AumentoRiesgoCotizacionId id;
	private RiesgoCotizacionDTO riesgoCotizacionDTO;
	private AumentoVarioDTO aumentoVarioDTO;
	private Short claveAutorizacion;
	private String codigoUsuarioAutorizacion;
	private Double valorAumento;
	private Short claveObligatoriedad;
	private Short claveContrato;
	private Short claveComercialTecnico;
	private Short claveNivel;
//	private Short claveTipo;

	// Constructors

	/** default constructor */
	public AumentoRiesgoCotizacionDTO() {
	}

	/** minimal constructor */
	public AumentoRiesgoCotizacionDTO(AumentoRiesgoCotizacionId id,
			RiesgoCotizacionDTO riesgoCotizacionDTO,
			AumentoVarioDTO aumentoVarioDTO, Short claveAutorizacion,
			Double valorAumento, Short claveObligatoriedad, Short claveContrato) {
		this.id = id;
		this.riesgoCotizacionDTO = riesgoCotizacionDTO;
		this.aumentoVarioDTO = aumentoVarioDTO;
		this.claveAutorizacion = claveAutorizacion;
		this.valorAumento = valorAumento;
		this.claveObligatoriedad = claveObligatoriedad;
		this.claveContrato = claveContrato;
	}

	/** full constructor */
	public AumentoRiesgoCotizacionDTO(AumentoRiesgoCotizacionId id,
			RiesgoCotizacionDTO riesgoCotizacionDTO,
			AumentoVarioDTO aumentoVarioDTO, Short claveAutorizacion,
			String codigoUsuarioAutorizacion, Double valorAumento,
			Short claveObligatoriedad, Short claveContrato) {
		this.id = id;
		this.riesgoCotizacionDTO = riesgoCotizacionDTO;
		this.aumentoVarioDTO = aumentoVarioDTO;
		this.claveAutorizacion = claveAutorizacion;
		this.codigoUsuarioAutorizacion = codigoUsuarioAutorizacion;
		this.valorAumento = valorAumento;
		this.claveObligatoriedad = claveObligatoriedad;
		this.claveContrato = claveContrato;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToCotizacion", column = @Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroInciso", column = @Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToSeccion", column = @Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToCobertura", column = @Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToRiesgo", column = @Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToAumentoVario", column = @Column(name = "IDTOAUMENTOVARIO", nullable = false, precision = 22, scale = 0)) })
	public AumentoRiesgoCotizacionId getId() {
		return this.id;
	}

	public void setId(AumentoRiesgoCotizacionId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( {
			@JoinColumn(name = "IDTOCOTIZACION", referencedColumnName = "IDTOCOTIZACION", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "NUMEROINCISO", referencedColumnName = "NUMEROINCISO", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTOSECCION", referencedColumnName = "IDTOSECCION", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTOCOBERTURA", referencedColumnName = "IDTOCOBERTURA", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTORIESGO", referencedColumnName = "IDTORIESGO", nullable = false, insertable = false, updatable = false) })
	public RiesgoCotizacionDTO getRiesgoCotizacionDTO() {
		return this.riesgoCotizacionDTO;
	}

	public void setRiesgoCotizacionDTO(RiesgoCotizacionDTO riesgoCotizacionDTO) {
		this.riesgoCotizacionDTO = riesgoCotizacionDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOAUMENTOVARIO", nullable = false, insertable = false, updatable = false)
	public AumentoVarioDTO getAumentoVarioDTO() {
		return this.aumentoVarioDTO;
	}

	public void setAumentoVarioDTO(AumentoVarioDTO aumentoVarioDTO) {
		this.aumentoVarioDTO = aumentoVarioDTO;
	}

	@Column(name = "CLAVEAUTORIZACION", nullable = false, precision = 4, scale = 0)
	public Short getClaveAutorizacion() {
		return this.claveAutorizacion;
	}

	public void setClaveAutorizacion(Short claveAutorizacion) {
		this.claveAutorizacion = claveAutorizacion;
	}

	@Column(name = "CODIGOUSUARIOAUTORIZACION", length = 8)
	public String getCodigoUsuarioAutorizacion() {
		return this.codigoUsuarioAutorizacion;
	}

	public void setCodigoUsuarioAutorizacion(String codigoUsuarioAutorizacion) {
		this.codigoUsuarioAutorizacion = codigoUsuarioAutorizacion;
	}

	@Column(name = "VALORAUMENTO", nullable = false, precision = 16)
	public Double getValorAumento() {
		return this.valorAumento;
	}

	public void setValorAumento(Double valorAumento) {
		this.valorAumento = valorAumento;
	}

	@Column(name = "CLAVEOBLIGATORIEDAD", nullable = false, precision = 4, scale = 0)
	public Short getClaveObligatoriedad() {
		return this.claveObligatoriedad;
	}

	public void setClaveObligatoriedad(Short claveObligatoriedad) {
		this.claveObligatoriedad = claveObligatoriedad;
	}

	@Column(name = "CLAVECONTRATO", nullable = false, precision = 4, scale = 0)
	public Short getClaveContrato() {
		return this.claveContrato;
	}

	public void setClaveContrato(Short claveContrato) {
		this.claveContrato = claveContrato;
	}

	@Column(name = "CLAVECOMERCIALTECNICO", nullable = false, precision = 4, scale = 0)
	public Short getClaveComercialTecnico() {
		return claveComercialTecnico;
	}

	public void setClaveComercialTecnico(Short claveComercialTecnico) {
		this.claveComercialTecnico = claveComercialTecnico;
	}

	@Column(name = "CLAVENIVEL", nullable = false, precision = 4, scale = 0)
	public Short getClaveNivel() {
		return claveNivel;
	}

	public void setClaveNivel(Short claveNivel) {
		this.claveNivel = claveNivel;
	}

/*	public void setClaveTipo(Short claveTipo) {
		this.claveTipo = claveTipo;
	}

	public Short getClaveTipo() {
		return claveTipo;
	}*/
}