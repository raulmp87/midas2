package mx.com.afirme.midas.cotizacion.riesgo.dependencia;

// default package

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for RiesgoDependenciaDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface RiesgoDependenciaFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved RiesgoDependenciaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            RiesgoDependenciaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(RiesgoDependenciaDTO entity);

	/**
	 * Delete a persistent RiesgoDependenciaDTO entity.
	 * 
	 * @param entity
	 *            RiesgoDependenciaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(RiesgoDependenciaDTO entity);

	/**
	 * Persist a previously saved RiesgoDependenciaDTO entity and return it or a
	 * copy of it to the sender. A copy of the RiesgoDependenciaDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            RiesgoDependenciaDTO entity to update
	 * @return RiesgoDependenciaDTO the persisted RiesgoDependenciaDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public RiesgoDependenciaDTO update(RiesgoDependenciaDTO entity);

	public RiesgoDependenciaDTO findById(RiesgoDependenciaId id);

	/**
	 * Find all RiesgoDependenciaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the RiesgoDependenciaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<RiesgoDependenciaDTO> found by query
	 */
	public List<RiesgoDependenciaDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all RiesgoDependenciaDTO entities.
	 * 
	 * @return List<RiesgoDependenciaDTO> all RiesgoDependenciaDTO entities
	 */
	public List<RiesgoDependenciaDTO> findAll();
}