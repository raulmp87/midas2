package mx.com.afirme.midas.cotizacion.riesgo.dependencia;

// default package

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDTO;

/**
 * RiesgoDependenciaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TRRIESGODEPENDENCIA", schema = "MIDAS")
public class RiesgoDependenciaDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private RiesgoDependenciaId id;
	private RiesgoDTO riesgoDependeDeDTO;
	private RiesgoDTO riesgoDTO;
	private Short claveNivelDependencia;

	// Constructors

	/** default constructor */
	public RiesgoDependenciaDTO() {
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToRiesgo", column = @Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToRiesgoDependeDe", column = @Column(name = "IDTORIESGODEPENDEDE", nullable = false, precision = 22, scale = 0)) })
	public RiesgoDependenciaId getId() {
		return this.id;
	}

	public void setId(RiesgoDependenciaId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTORIESGODEPENDEDE", nullable = false, insertable = false, updatable = false)
	public RiesgoDTO getRiesgoDependeDeDTO() {
		return this.riesgoDependeDeDTO;
	}

	public void setRiesgoDependeDeDTO(RiesgoDTO riesgoDependeDeDTO) {
		this.riesgoDependeDeDTO = riesgoDependeDeDTO;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTORIESGO", nullable = false, insertable = false, updatable = false)
	public RiesgoDTO getRiesgoDTO() {
		return this.riesgoDTO;
	}

	public void setRiesgoDTO(RiesgoDTO riesgoDTO) {
		this.riesgoDTO = riesgoDTO;
	}

	@Column(name = "CLAVENIVELDEPENDENCIA", nullable = false, precision = 4, scale = 0)
	public Short getClaveNivelDependencia() {
		return this.claveNivelDependencia;
	}

	public void setClaveNivelDependencia(Short claveNivelDependencia) {
		this.claveNivelDependencia = claveNivelDependencia;
	}

}