package mx.com.afirme.midas.cotizacion.documento;
// default package

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * DocPrevencionOperIlicitasDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TODOCPREVENCIONOPERILICITAS"
    ,schema="MIDAS"
)
public class DocPrevencionOperIlicitasDTO  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToControlArchivo;
	private BigDecimal idToCotizacion;
     private Short claveTipoDocumento;

    /** default constructor */
    public DocPrevencionOperIlicitasDTO() {
    }
   
    // Property accessors
    @Id 
    @Column(name="IDTOCONTROLARCHIVO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToControlArchivo() {
        return this.idToControlArchivo;
    }
    
    public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
        this.idToControlArchivo = idToControlArchivo;
    }
    
    public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

    @Column(name="IDTOCOTIZACION", nullable=false)
	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	@Column(name="CLAVETIPODOCUMENTO", nullable=false, precision=4, scale=0)

    public Short getClaveTipoDocumento() {
        return this.claveTipoDocumento;
    }
    
    public void setClaveTipoDocumento(Short claveTipoDocumento) {
        this.claveTipoDocumento = claveTipoDocumento;
    }

}