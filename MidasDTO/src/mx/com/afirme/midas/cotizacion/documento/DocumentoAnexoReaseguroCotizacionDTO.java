package mx.com.afirme.midas.cotizacion.documento;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;

@Entity
@Table(name = "TODOCANEXOREASEGUROCOT", schema = "MIDAS")
public class DocumentoAnexoReaseguroCotizacionDTO implements
		java.io.Serializable {

	// Fields

	private static final long serialVersionUID = -902311103032515500L;
	private DocumentoAnexoReaseguroCotizacionId id;
	private CotizacionDTO cotizacionDTO;
	private BigDecimal numeroSecuencia;
	private String descripcionDocumentoAnexo;
	private Date fechaCreacion;
	private String codigoUsuarioCreacion;
	private String nombreUsuarioCreacion;
	private Date fechaModificacion;
	private String codigoUsuarioModificacion;
	private String nombreUsuarioModificacion;

	// Constructors

	/** default constructor */
	public DocumentoAnexoReaseguroCotizacionDTO() {
	}

	/** minimal constructor */
	public DocumentoAnexoReaseguroCotizacionDTO(
			DocumentoAnexoReaseguroCotizacionId id, Date fechaCreacion,
			String codigoUsuarioCreacion, String nombreUsuarioCreacion) {
		this.id = id;
		this.fechaCreacion = fechaCreacion;
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
		this.nombreUsuarioCreacion = nombreUsuarioCreacion;
	}

	/** full constructor */
	public DocumentoAnexoReaseguroCotizacionDTO(
			DocumentoAnexoReaseguroCotizacionId id, BigDecimal numeroSecuencia,
			String descripcionDocumentoAnexo, Date fechaCreacion,
			String codigoUsuarioCreacion, String nombreUsuarioCreacion,
			Date fechaModificacion, String codigoUsuarioModificacion,
			String nombreUsuarioModificacion) {
		this.id = id;
		this.numeroSecuencia = numeroSecuencia;
		this.descripcionDocumentoAnexo = descripcionDocumentoAnexo;
		this.fechaCreacion = fechaCreacion;
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
		this.nombreUsuarioCreacion = nombreUsuarioCreacion;
		this.fechaModificacion = fechaModificacion;
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
		this.nombreUsuarioModificacion = nombreUsuarioModificacion;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToCotizacion", column = @Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToControlArchivo", column = @Column(name = "IDTOCONTROLARCHIVO", nullable = false, precision = 22, scale = 0)) })
	public DocumentoAnexoReaseguroCotizacionId getId() {
		return this.id;
	}

	public void setId(DocumentoAnexoReaseguroCotizacionId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOCOTIZACION", nullable = false, insertable = false, updatable = false)
	public CotizacionDTO getCotizacionDTO() {
		return cotizacionDTO;
	}

	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}

	@Column(name = "NUMEROSECUENCIA", precision = 22, scale = 0)
	public BigDecimal getNumeroSecuencia() {
		return this.numeroSecuencia;
	}

	public void setNumeroSecuencia(BigDecimal numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}

	@Column(name = "DESCRIPCIONDOCUMENTOANEXO", length = 200)
	public String getDescripcionDocumentoAnexo() {
		return this.descripcionDocumentoAnexo;
	}

	public void setDescripcionDocumentoAnexo(String descripcionDocumentoAnexo) {
		this.descripcionDocumentoAnexo = descripcionDocumentoAnexo;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHACREACION", nullable = false, length = 7)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Column(name = "CODIGOUSUARIOCREACION", nullable = false, length = 8)
	public String getCodigoUsuarioCreacion() {
		return this.codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	@Column(name = "NOMBREUSUARIOCREACION", nullable = false, length = 200)
	public String getNombreUsuarioCreacion() {
		return this.nombreUsuarioCreacion;
	}

	public void setNombreUsuarioCreacion(String nombreUsuarioCreacion) {
		this.nombreUsuarioCreacion = nombreUsuarioCreacion;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAMODIFICACION", length = 7)
	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Column(name = "CODIGOUSUARIOMODIFICACION", length = 8)
	public String getCodigoUsuarioModificacion() {
		return this.codigoUsuarioModificacion;
	}

	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}

	@Column(name = "NOMBREUSUARIOMODIFICACION", length = 200)
	public String getNombreUsuarioModificacion() {
		return this.nombreUsuarioModificacion;
	}

	public void setNombreUsuarioModificacion(String nombreUsuarioModificacion) {
		this.nombreUsuarioModificacion = nombreUsuarioModificacion;
	}

}