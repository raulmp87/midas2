package mx.com.afirme.midas.cotizacion.documento;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;

/**
 * Todocdigitalcot entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TODOCDIGITALCOT", schema = "MIDAS")
public class DocumentoDigitalCotizacionDTO implements java.io.Serializable {

	// Fields

	private static final long serialVersionUID = -503480907842094464L;
	private BigDecimal idDocumentoDigitalCotizacion;
	private CotizacionDTO cotizacionDTO;
	private BigDecimal idControlArchivo;
	private ControlArchivoDTO controlArchivo;
	private Date fechaCreacion;
	private String codigoUsuarioCreacion;
	private String nombreUsuarioCreacion;
	private Date fechaModificacion;
	private String codigoUsuarioModificacion;
	private String nombreUsuarioModificacion;

	// Constructors

	/** default constructor */
	public DocumentoDigitalCotizacionDTO() {
		this.cotizacionDTO = new CotizacionDTO();
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTODOCDIGITALCOT_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTODOCDIGITALCOT_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTODOCDIGITALCOT_SEQ_GENERADOR")	  	
	@Column(name = "IDTODOCDIGITALCOT", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdDocumentoDigitalCotizacion() {
		return this.idDocumentoDigitalCotizacion;
	}

	public void setIdDocumentoDigitalCotizacion(BigDecimal idDocumentoDigitalCotizacion) {
		this.idDocumentoDigitalCotizacion = idDocumentoDigitalCotizacion;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOCOTIZACION", nullable = false)
	public CotizacionDTO getCotizacionDTO() {
		return this.cotizacionDTO;
	}
	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}

	@Column(name = "IDTOCONTROLARCHIVO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdControlArchivo() {
		return this.idControlArchivo;
	}

	public void setIdControlArchivo(BigDecimal idControlArchivo) {
		this.idControlArchivo = idControlArchivo;
	}
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOCONTROLARCHIVO", nullable = false, insertable = false, updatable = false)
	public ControlArchivoDTO getControlArchivo() {
		return controlArchivo;
	}

	public void setControlArchivo(ControlArchivoDTO controlArchivoDTO) {
		this.controlArchivo = controlArchivoDTO;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHACREACION", nullable = false, length = 7)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Column(name = "CODIGOUSUARIOCREACION", nullable = false, length = 8)
	public String getCodigoUsuarioCreacion() {
		return this.codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	@Column(name = "NOMBREUSUARIOCREACION", nullable = false, length = 200)
	public String getNombreUsuarioCreacion() {
		return this.nombreUsuarioCreacion;
	}

	public void setNombreUsuarioCreacion(String nombreUsuarioCreacion) {
		this.nombreUsuarioCreacion = nombreUsuarioCreacion;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAMODIFICACION", length = 7)
	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Column(name = "CODIGOUSUARIOMODIFICACION", length = 8)
	public String getCodigoUsuarioModificacion() {
		return this.codigoUsuarioModificacion;
	}

	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}

	@Column(name = "NOMBREUSUARIOMODIFICACION", length = 200)
	public String getNombreUsuarioModificacion() {
		return this.nombreUsuarioModificacion;
	}

	public void setNombreUsuarioModificacion(String nombreUsuarioModificacion) {
		this.nombreUsuarioModificacion = nombreUsuarioModificacion;
	}

}