package mx.com.afirme.midas.cotizacion.documento;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for DocumentoDigitalCotizacionFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface DocumentoDigitalCotizacionFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved DocumentoDigitalCotizacionDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            DocumentoDigitalCotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(DocumentoDigitalCotizacionDTO entity);

	/**
	 * Delete a persistent DocumentoDigitalCotizacionDTO entity.
	 * 
	 * @param entity
	 *            DocumentoDigitalCotizacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(DocumentoDigitalCotizacionDTO entity);

	/**
	 * Persist a previously saved DocumentoDigitalCotizacionDTO entity and return it or a copy
	 * of it to the sender. A copy of the DocumentoDigitalCotizacionDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            DocumentoDigitalCotizacionDTO entity to update
	 * @return DocumentoDigitalCotizacionDTO the persisted DocumentoDigitalCotizacionDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public DocumentoDigitalCotizacionDTO update(DocumentoDigitalCotizacionDTO entity);

	public DocumentoDigitalCotizacionDTO findById(BigDecimal id);

	/**
	 * Find all DocumentoDigitalCotizacionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the DocumentoDigitalCotizacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<DocumentoDigitalCotizacionDTO> found by query
	 */
	public List<DocumentoDigitalCotizacionDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all DocumentoDigitalCotizacionDTO entities.
	 * 
	 * @return List<DocumentoDigitalCotizacionDTO> all DocumentoDigitalCotizacionDTO entities
	 */
	public List<DocumentoDigitalCotizacionDTO> findAll();
	
	public void copiarDocumentos(BigDecimal idCotizacionBase,
			BigDecimal idCotizacionCopia);
}