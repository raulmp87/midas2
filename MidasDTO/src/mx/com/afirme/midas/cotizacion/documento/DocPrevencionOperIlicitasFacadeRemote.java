package mx.com.afirme.midas.cotizacion.documento;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for DocPrevencionOperIlicitasDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface DocPrevencionOperIlicitasFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved DocPrevencionOperIlicitasDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DocPrevencionOperIlicitasDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DocPrevencionOperIlicitasDTO entity);
    /**
	 Delete a persistent DocPrevencionOperIlicitasDTO entity.
	  @param entity DocPrevencionOperIlicitasDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DocPrevencionOperIlicitasDTO entity);
   /**
	 Persist a previously saved DocPrevencionOperIlicitasDTO entity and return it or a copy of it to the sender. 
	 A copy of the DocPrevencionOperIlicitasDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DocPrevencionOperIlicitasDTO entity to update
	 @return DocPrevencionOperIlicitasDTO the persisted DocPrevencionOperIlicitasDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public DocPrevencionOperIlicitasDTO update(DocPrevencionOperIlicitasDTO entity);
	public DocPrevencionOperIlicitasDTO findById( BigDecimal id);
	 /**
	 * Find all DocPrevencionOperIlicitasDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DocPrevencionOperIlicitasDTO property to query
	  @param value the property value to match
	  	  @return List<DocPrevencionOperIlicitasDTO> found by query
	 */
	public List<DocPrevencionOperIlicitasDTO> findByProperty(String propertyName, Object value
		);
	public List<DocPrevencionOperIlicitasDTO> findByClaveTipoDocumento(Object claveTipoDocumento
		);
	/**
	 * Find all DocPrevencionOperIlicitasDTO entities.
	  	  @return List<DocPrevencionOperIlicitasDTO> all DocPrevencionOperIlicitasDTO entities
	 */
	public List<DocPrevencionOperIlicitasDTO> findAll(
		);	
}