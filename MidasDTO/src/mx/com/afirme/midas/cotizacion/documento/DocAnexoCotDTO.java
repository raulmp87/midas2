package mx.com.afirme.midas.cotizacion.documento;
// default package

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;


/**
 * DocAnexoCotDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TODOCANEXOCOT"
    ,schema="MIDAS"
)

public class DocAnexoCotDTO  implements java.io.Serializable, Entidad {


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
     private DocAnexoCotId id;
     private CotizacionDTO cotizacionDTO;
     private Date fechaCreacion;
     private String codigoUsuarioCreacion;
     private String nombreUsuarioCreacion;
     private Date fechaModificacion;
     private String codigoUsuarioModificacion;
     private String nombreUsuarioModificacion;
     private String descripcionDocumentoAnexo;
     private Short claveObligatoriedad;
     private Short claveSeleccion;
     private BigDecimal orden;
     private BigDecimal claveTipo;
     private BigDecimal idToCobertura;


    // Constructors

    /** default constructor */
    public DocAnexoCotDTO() {
    }

   
    // Property accessors
    @EmbeddedId
    @AttributeOverrides( {
	    @AttributeOverride(name="idToCotizacion", column=@Column(name="IDTOCOTIZACION", nullable=false, precision=22, scale=0) ), 
	    @AttributeOverride(name="idToControlArchivo", column=@Column(name="IDTOCONTROLARCHIVO", nullable=false, precision=22, scale=0) ) } )
    public DocAnexoCotId getId() {
        return this.id;
    }
    
    public void setId(DocAnexoCotId id) {
        this.id = id;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTOCOTIZACION", nullable=false, insertable=false, updatable=false)
    public CotizacionDTO getCotizacionDTO() {
		return cotizacionDTO;
	}

	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}
	
    @Temporal(TemporalType.DATE)
    @Column(name="FECHACREACION", nullable=false, length=7)
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }
    
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    
    @Column(name="CODIGOUSUARIOCREACION", nullable=false, length=8)
    public String getCodigoUsuarioCreacion() {
        return this.codigoUsuarioCreacion;
    }
    
    public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
        this.codigoUsuarioCreacion = codigoUsuarioCreacion;
    }
    
    @Column(name="NOMBREUSUARIOCREACION", nullable=false, length=200)
    public String getNombreUsuarioCreacion() {
        return this.nombreUsuarioCreacion;
    }
    
    public void setNombreUsuarioCreacion(String nombreUsuarioCreacion) {
        this.nombreUsuarioCreacion = nombreUsuarioCreacion;
    }
    
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAMODIFICACION", length=7)
    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }
    
    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
    
    @Column(name="CODIGOUSUARIOMODIFICACION", length=8)
    public String getCodigoUsuarioModificacion() {
        return this.codigoUsuarioModificacion;
    }
    
    public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
        this.codigoUsuarioModificacion = codigoUsuarioModificacion;
    }
    
    @Column(name="NOMBREUSUARIOMODIFICACION", length=200)
    public String getNombreUsuarioModificacion() {
        return this.nombreUsuarioModificacion;
    }
    
    public void setNombreUsuarioModificacion(String nombreUsuarioModificacion) {
        this.nombreUsuarioModificacion = nombreUsuarioModificacion;
    }
    
    @Column(name="DESCRIPCIONDOCUMENTOANEXO", nullable=false, length=200)
    public String getDescripcionDocumentoAnexo() {
        return this.descripcionDocumentoAnexo;
    }
    
    public void setDescripcionDocumentoAnexo(String descripcionDocumentoAnexo) {
        this.descripcionDocumentoAnexo = descripcionDocumentoAnexo;
    }
    
    @Column(name="CLAVEOBLIGATORIEDAD", nullable=false, precision=4, scale=0)
    public Short getClaveObligatoriedad() {
        return this.claveObligatoriedad;
    }
    
    public void setClaveObligatoriedad(Short claveObligatoriedad) {
        this.claveObligatoriedad = claveObligatoriedad;
    }
    
    @Column(name="CLAVESELECCION", nullable=false, precision=4, scale=0)

    public Short getClaveSeleccion() {
        return this.claveSeleccion;
    }
    
    public void setClaveSeleccion(Short claveSeleccion) {
        this.claveSeleccion = claveSeleccion;
    }
    
    @Column(name="ORDEN", precision=22, scale=0)

    public BigDecimal getOrden() {
        return this.orden;
    }
    
    public void setOrden(BigDecimal orden) {
        this.orden = orden;
    }
    
    @Column(name="CLAVETIPO", nullable=false, precision=22, scale=0)

    public BigDecimal getClaveTipo() {
        return this.claveTipo;
    }
    
    public void setClaveTipo(BigDecimal claveTipo) {
        this.claveTipo = claveTipo;
    }

    @Column(name="IDTOCOBERTURA")
    
	public BigDecimal getIdToCobertura() {
		return idToCobertura;
	}


	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}


	@SuppressWarnings("unchecked")
	@Override
	public DocAnexoCotId getKey() {
		return this.id;
	}


	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
}