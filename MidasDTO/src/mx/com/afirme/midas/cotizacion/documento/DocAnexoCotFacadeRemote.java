package mx.com.afirme.midas.cotizacion.documento;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for DocAnexoCotDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface DocAnexoCotFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved DocAnexoCotDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DocAnexoCotDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DocAnexoCotDTO entity);
    /**
	 Delete a persistent DocAnexoCotDTO entity.
	  @param entity DocAnexoCotDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DocAnexoCotDTO entity);
   /**
	 Persist a previously saved DocAnexoCotDTO entity and return it or a copy of it to the sender. 
	 A copy of the DocAnexoCotDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DocAnexoCotDTO entity to update
	 @return DocAnexoCotDTO the persisted DocAnexoCotDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public DocAnexoCotDTO update(DocAnexoCotDTO entity);
	public DocAnexoCotDTO findById( DocAnexoCotId id);
	 /**
	 * Find all DocAnexoCotDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DocAnexoCotDTO property to query
	  @param value the property value to match
  	  @return List<DocAnexoCotDTO> found by query
	 */
	public List<DocAnexoCotDTO> findByProperty(String propertyName, Object value);
	/**
	 * Find all DocAnexoCotDTO entities.
	  	  @return List<DocAnexoCotDTO> all DocAnexoCotDTO entities
	 */
	public List<DocAnexoCotDTO> findAll();
	
	public List<DocAnexoCotDTO> listarFiltrado(DocAnexoCotDTO docAnexoCotDTO);
	
	public void copiarDocumentos(BigDecimal idCotizacionBase, BigDecimal idCotizacionCopia);
	
	/**
	 * Generate documents for one quotation
	 * @param idToCotizacion
	 * @autor martin
	 */
	public void generateDocumentsByCotizacion(BigDecimal idToCotizacion);
	/**
	 * Generate coberturas documents for one quotation
	 * @param idToCotizacion
	 * @author martin
	 */
	public void generateCobDocumentsByCotizacion(BigDecimal idToCotizacion);
}