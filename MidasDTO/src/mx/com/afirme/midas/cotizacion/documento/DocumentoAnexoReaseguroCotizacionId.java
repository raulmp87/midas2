package mx.com.afirme.midas.cotizacion.documento;


import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * DocumentoAnexoReaseguroCotizacionId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class DocumentoAnexoReaseguroCotizacionId  implements java.io.Serializable {


    // Fields    
    private static final long serialVersionUID = -2820636850197169618L;
	private BigDecimal idToCotizacion;
    private BigDecimal idToControlArchivo;


    // Constructors

    /** default constructor */
    public DocumentoAnexoReaseguroCotizacionId() {
    }

    
    /** full constructor */
    public DocumentoAnexoReaseguroCotizacionId(BigDecimal idToCotizacion, BigDecimal idToControlArchivo) {
        this.idToCotizacion = idToCotizacion;
        this.idToControlArchivo = idToControlArchivo;
    }

   
    // Property accessors

    @Column(name="IDTOCOTIZACION", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToCotizacion() {
        return this.idToCotizacion;
    }
    
    public void setIdToCotizacion(BigDecimal idToCotizacion) {
        this.idToCotizacion = idToCotizacion;
    }

    @Column(name="IDTOCONTROLARCHIVO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToControlArchivo() {
        return this.idToControlArchivo;
    }
    
    public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
        this.idToControlArchivo = idToControlArchivo;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof DocumentoAnexoReaseguroCotizacionId) ) return false;
		 DocumentoAnexoReaseguroCotizacionId castOther = ( DocumentoAnexoReaseguroCotizacionId ) other; 
         
		 return ( (this.getIdToCotizacion()==castOther.getIdToCotizacion()) || ( this.getIdToCotizacion()!=null && castOther.getIdToCotizacion()!=null && this.getIdToCotizacion().equals(castOther.getIdToCotizacion()) ) )
 && ( (this.getIdToControlArchivo()==castOther.getIdToControlArchivo()) || ( this.getIdToControlArchivo()!=null && castOther.getIdToControlArchivo()!=null && this.getIdToControlArchivo().equals(castOther.getIdToControlArchivo()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdToCotizacion() == null ? 0 : this.getIdToCotizacion().hashCode() );
         result = 37 * result + ( getIdToControlArchivo() == null ? 0 : this.getIdToControlArchivo().hashCode() );
         return result;
   }   





}