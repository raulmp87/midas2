package mx.com.afirme.midas.cotizacion.documento;
// default package

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.texto.TexAdicionalCot;


/**
 * TexAdicionalCotDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOTEXADICIONALCOT"
    ,schema="MIDAS"
)
public class TexAdicionalCotDTO  implements java.io.Serializable,Entidad {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 8291497584535697147L;
	private BigDecimal idToTexAdicionalCot;
     private CotizacionDTO cotizacion;
     private String descripcionTexto;
     private Short claveAutorizacion;
     private String codigoUsuarioAutorizacion;
     private Date fechaCreacion;
     private String codigoUsuarioCreacion;
     private String nombreUsuarioCreacion;
     private Date fechaModificacion;
     private String codigoUsuarioModificacion;
     private String nombreUsuarioModificacion;
     private BigDecimal numeroSecuencia;


    // Constructors

	/** default constructor */
    public TexAdicionalCotDTO() {
    	cotizacion = new CotizacionDTO();
    }
    
    public TexAdicionalCotDTO(TexAdicionalCot texAdicionalCot){
    	this.claveAutorizacion = texAdicionalCot.getClaveAutorizacion();
    	this.codigoUsuarioAutorizacion = texAdicionalCot.getCodigoUsuarioAutorizacion();
    	this.codigoUsuarioCreacion = texAdicionalCot.getCodigoUsuarioCreacion();
    	this.codigoUsuarioModificacion = texAdicionalCot.getCodigoUsuarioModificacion();
    	this.descripcionTexto = texAdicionalCot.getDescripcionTexto();
    	this.fechaCreacion = texAdicionalCot.getFechaCreacion();
    	this.fechaModificacion = texAdicionalCot.getFechaModificacion();
    	this.nombreUsuarioCreacion = texAdicionalCot.getNombreUsuarioCreacion();
    	this.nombreUsuarioModificacion = texAdicionalCot.getNombreUsuarioModificacion();
    	this.numeroSecuencia = new BigDecimal(texAdicionalCot.getNumeroSecuencia());
    }
   
    // Property accessors
    @Id     
    @SequenceGenerator(name = "IDTOTEXADICIONALCOT_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOTEXADICIONALCOT_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOTEXADICIONALCOT_SEQ_GENERADOR")
    @Column(name="IDTOTEXADICIONALCOT", nullable=false, precision=22, scale=0)
    public BigDecimal getIdToTexAdicionalCot() {
		return idToTexAdicionalCot;
	}

	public void setIdToTexAdicionalCot(BigDecimal idToTexAdicionalCot) {
		this.idToTexAdicionalCot = idToTexAdicionalCot;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTOCOTIZACION", nullable=false)

    public CotizacionDTO getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(CotizacionDTO cotizacion) {
		this.cotizacion = cotizacion;
	}
    
    @Column(name="DESCRIPCIONTEXTO", length=4000)

    public String getDescripcionTexto() {
		return descripcionTexto;
	}

	public void setDescripcionTexto(String descripcionTexto) {
		this.descripcionTexto = descripcionTexto;
	}
    
    @Column(name="CLAVEAUTORIZACION", nullable=false, precision=4, scale=0)

    public Short getClaveAutorizacion() {
		return claveAutorizacion;
	}

	public void setClaveAutorizacion(Short claveAutorizacion) {
		this.claveAutorizacion = claveAutorizacion;
	}
	
    @Column(name="CODIGOUSUARIOAUTORIZACION", length=8)

    public String getCodigoUsuarioAutorizacion() {
		return codigoUsuarioAutorizacion;
	}

	public void setCodigoUsuarioAutorizacion(String codigoUsuarioAutorizacion) {
		this.codigoUsuarioAutorizacion = codigoUsuarioAutorizacion;
	}
     
    @Temporal(TemporalType.DATE)
    @Column(name="FECHACREACION", nullable=false, length=7)

    public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
       
    @Column(name="CODIGOUSUARIOCREACION", nullable=false, length=8)

    public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
      
    @Column(name="NOMBREUSUARIOCREACION", nullable=false, length=200)

   public String getNombreUsuarioCreacion() {
		return nombreUsuarioCreacion;
	}

	public void setNombreUsuarioCreacion(String nombreUsuarioCreacion) {
		this.nombreUsuarioCreacion = nombreUsuarioCreacion;
	}
       
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAMODIFICACION", length=7)

   public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
     
    @Column(name="CODIGOUSUARIOMODIFICACION", length=8)

    public String getCodigoUsuarioModificacion() {
		return codigoUsuarioModificacion;
	}

	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}
       
    @Column(name="NOMBREUSUARIOMODIFICACION", length=200)

   public String getNombreUsuarioModificacion() {
		return nombreUsuarioModificacion;
	}

	public void setNombreUsuarioModificacion(String nombreUsuarioModificacion) {
		this.nombreUsuarioModificacion = nombreUsuarioModificacion;
	}
       
    
    @Column(name="NUMEROSECUENCIA", precision=22, scale=0)
    public BigDecimal getNumeroSecuencia() {
		return numeroSecuencia;
	}

	public void setNumeroSecuencia(BigDecimal numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}
	
	public String getDescripcionEstatusAut(){		
		String descripcionEstatusAut = "";
		if (this.claveAutorizacion != null) {
			switch(this.claveAutorizacion){				
				case TexAdicionalCot.CLAVEAUTORIZACION_EN_PROCESO:
					descripcionEstatusAut = "EN PROCESO";
					break;
				case TexAdicionalCot.CLAVEAUTORIZACION_AUTORIZADA:
					descripcionEstatusAut = "AUTORIZADA";
					break;					
				case TexAdicionalCot.CLAVEAUTORIZACION_RECHAZADA:
					descripcionEstatusAut = "RECHAZADA";
					break;
				case TexAdicionalCot.CLAVEAUTORIZACION_CANCELADA:
					descripcionEstatusAut = "CANCELADA";
					break;									
				default:
					break;							
			}
		}
		return descripcionEstatusAut;
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return idToTexAdicionalCot;
	}

	@Override
	public String getValue() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

}