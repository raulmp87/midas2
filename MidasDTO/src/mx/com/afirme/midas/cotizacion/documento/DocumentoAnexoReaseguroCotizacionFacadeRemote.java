package mx.com.afirme.midas.cotizacion.documento;


import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

/**
 * Remote interface for DocumentoAnexoReaseguroCotizacionDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface DocumentoAnexoReaseguroCotizacionFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved DocumentoAnexoReaseguroCotizacionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DocumentoAnexoReaseguroCotizacionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DocumentoAnexoReaseguroCotizacionDTO entity);
    /**
	 Delete a persistent DocumentoAnexoReaseguroCotizacionDTO entity.
	  @param entity DocumentoAnexoReaseguroCotizacionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DocumentoAnexoReaseguroCotizacionDTO entity);
   /**
	 Persist a previously saved DocumentoAnexoReaseguroCotizacionDTO entity and return it or a copy of it to the sender. 
	 A copy of the DocumentoAnexoReaseguroCotizacionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DocumentoAnexoReaseguroCotizacionDTO entity to update
	 @return DocumentoAnexoReaseguroCotizacionDTO the persisted DocumentoAnexoReaseguroCotizacionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public DocumentoAnexoReaseguroCotizacionDTO update(DocumentoAnexoReaseguroCotizacionDTO entity);
	public DocumentoAnexoReaseguroCotizacionDTO findById( DocumentoAnexoReaseguroCotizacionId id);
	 /**
	 * Find all DocumentoAnexoReaseguroCotizacionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DocumentoAnexoReaseguroCotizacionDTO property to query
	  @param value the property value to match
	  	  @return List<DocumentoAnexoReaseguroCotizacionDTO> found by query
	 */
	public List<DocumentoAnexoReaseguroCotizacionDTO> findByProperty(String propertyName, Object value
		);
	public List<DocumentoAnexoReaseguroCotizacionDTO> findByDescripcionDocumentoAnexo(Object descripcionDocumentoAnexo
		);
	public List<DocumentoAnexoReaseguroCotizacionDTO> findByCodigoUsuarioCreacion(Object codigoUsuarioCreacion
		);
	public List<DocumentoAnexoReaseguroCotizacionDTO> findByNombreUsuarioCreacion(Object nombreUsuarioCreacion
		);
	public List<DocumentoAnexoReaseguroCotizacionDTO> findByCodigoUsuarioModificacion(Object codigoUsuarioModificacion
		);
	public List<DocumentoAnexoReaseguroCotizacionDTO> findByNombreUsuarioModificacion(Object nombreUsuarioModificacion
		);
	/**
	 * Find all DocumentoAnexoReaseguroCotizacionDTO entities.
	  	  @return List<DocumentoAnexoReaseguroCotizacionDTO> all DocumentoAnexoReaseguroCotizacionDTO entities
	 */
	public List<DocumentoAnexoReaseguroCotizacionDTO> findAll(
		);	
	public void copiarDocumentos(BigDecimal idCotizacionBase, BigDecimal idCotizacionCopia);
}