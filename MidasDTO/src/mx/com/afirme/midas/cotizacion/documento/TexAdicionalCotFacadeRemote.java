package mx.com.afirme.midas.cotizacion.documento;
// default package

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

/**
 * Remote interface for TexAdicionalCotDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface TexAdicionalCotFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved TexAdicionalCotDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity TexAdicionalCotDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public TexAdicionalCotDTO save(TexAdicionalCotDTO entity);
    /**
	 Delete a persistent TexAdicionalCotDTO entity.
	  @param entity TexAdicionalCotDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(TexAdicionalCotDTO entity);
   /**
	 Persist a previously saved TexAdicionalCotDTO entity and return it or a copy of it to the sender. 
	 A copy of the TexAdicionalCotDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity TexAdicionalCotDTO entity to update
	 @return TexAdicionalCotDTO the persisted TexAdicionalCotDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public TexAdicionalCotDTO update(TexAdicionalCotDTO entity);
	public TexAdicionalCotDTO findById( BigDecimal id);
	 /**
	 * Find all TexAdicionalCotDTO entities with a specific property value.  
	 
	  @param propertyName the name of the TexAdicionalCotDTO property to query
	  @param value the property value to match
	  	  @return List<TexAdicionalCotDTO> found by query
	 */
	public List<TexAdicionalCotDTO> findByProperty(String propertyName, Object value
		);
	
	/**
	 * Find all TexAdicionalCotDTO entities.
	  	  @return List<TexAdicionalCotDTO> all TexAdicionalCotDTO entities
	 */
	public List<TexAdicionalCotDTO> findAll(
		);	
	
	/**
	 * Find all TexAdicionalCotDTO distint entities filtered by cotization.
	  	  @return List<TexAdicionalCotDTO> all TexAdicionalCotDTO entities
	 */
	public List<TexAdicionalCotDTO> listarFiltrado(TexAdicionalCotDTO texAdicionalCotDTO
		);
	
	/**
	 * Find all TexAdicionalCotDTO distint entities filtered by cotization and state.
	  	  @return List<TexAdicionalCotDTO> all TexAdicionalCotDTO entities
	 */
	public List<TexAdicionalCotDTO> listarFiltradoPorAutorizar(TexAdicionalCotDTO texAdicionalCotDTO
		);
	
	/**
	 * update claveAutorizacion from TexAdicionalCotDTO for ids
	  	  @return true if the update operation was realized
	 */
	public boolean cambiarEstatusTextosAdicionales(String ids, TexAdicionalCotDTO texAdicionalCotDTO);
	
	/**
	 * Find all TexAdicionalCotDTO distint entities filtered by cotization and will be pendig to be acepted
	  	  @return List<TexAdicionalCotDTO> all TexAdicionalCotDTO entities
	 */
	public List<TexAdicionalCotDTO> buscarPorClaveAutorizacionPorCotizacion(BigDecimal idToCotizacion, Short claveAutorizacion);
	
	public void copiarTextosAdicionales(BigDecimal idCotizacionBase,
			BigDecimal idCotizacionCopia);
}