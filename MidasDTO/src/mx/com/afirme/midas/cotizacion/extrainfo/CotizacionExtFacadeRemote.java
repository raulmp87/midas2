package mx.com.afirme.midas.cotizacion.extrainfo;

// default package

import java.math.BigDecimal;
import java.util.List;

/**
 * Remote interface for CotizacionExtDTOFacade.
 * @author MyEclipse Persistence Tools
 */

public interface CotizacionExtFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved CotizacionExtDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity  CotizacionExtDTO entity to persist
	 * @throws RuntimeException  when the operation fails
	 */
	public CotizacionExtDTO save(CotizacionExtDTO entity);

	/**
	 * Delete a persistent CotizacionExtDTO entity.
	 * 
	 * @param entity CotizacionExtDTO entity to delete
	 * @throws RuntimeException when the operation fails
	 */
	public void delete(CotizacionExtDTO entity);

	/**
	 * Persist a previously saved CotizacionExtDTO entity and return it or a copy
	 * of it to the sender. A copy of the CotizacionExtDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity CotizacionExtDTO entity to update
	 * @return CotizacionExtDTO the persisted CotizacionExtDTO entity instance, may
	 *         not be the same
	 * @throws RuntimeException if the operation fails
	 */
	public CotizacionExtDTO update(CotizacionExtDTO entity);
	/**
	 * @param id
	 * @return
	 */
	public CotizacionExtDTO findById(BigDecimal id);

	/**
	 * Find all CotizacionExtDTO entities with a specific property value.
	 * 
	 * @param propertyName the name of the CotizacionExtDTO property to query
	 * @param value the property value to match
	 * @return List<CotizacionExtDTO> found by query
	 */
	public List<CotizacionExtDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all CotizacionExtDTO entities.
	 * @return List<CotizacionExtDTO> all CotizacionExtDTO entities
	 */
	public List<CotizacionExtDTO> findAll();
	/**
	 * @param cotizacionExtDTO
	 * @return
	 */
	public List<CotizacionExtDTO> listarFiltrado(CotizacionExtDTO cotizacionExtDTO);

}