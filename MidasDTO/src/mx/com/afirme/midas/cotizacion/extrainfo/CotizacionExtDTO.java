package mx.com.afirme.midas.cotizacion.extrainfo;

import java.math.BigDecimal;

import mx.com.afirme.midas.base.LogBaseDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Id;

@Entity
@Table(name = "TOCOTIZACION_EXT", schema = "MIDAS")
public class CotizacionExtDTO extends LogBaseDTO implements Entidad{

	private static final long serialVersionUID = 3094621597503094433L;
	//KEY NAME STRING
	public static final String IDTOCOTIZACION = "idToCotizacion";
	
	private BigDecimal idToCotizacion;
	private Integer statusException;
	
	public CotizacionExtDTO () {
		super();
	}
	public CotizacionExtDTO (CotizacionExtDTO cotizacion) {
		super();
		this.idToCotizacion = cotizacion.getIdToCotizacion();
		this.statusException = cotizacion.getStatusException();
	}
	
	
	@Id
	//@SequenceGenerator(name = "IDTOCOTIZACION_SEQ_GENERADOR", allocationSize = 1, sequenceName = "IDTOCOTIZACION_SEQ")
	//@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOCOTIZACION_SEQ_GENERADOR")
	@Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCotizacion() {
		return this.idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	@Column(name = "STATUSEXCEPTION")
	public Integer getStatusException() {
		return statusException;
	}
	
	public boolean isStatusException () {
		return (statusException > 0);
	}

	public void setStatusException(Integer statusException) {
		this.statusException = statusException;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.idToCotizacion;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
	
}
