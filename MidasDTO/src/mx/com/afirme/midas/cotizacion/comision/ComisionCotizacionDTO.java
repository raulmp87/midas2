package mx.com.afirme.midas.cotizacion.comision;

import java.math.BigDecimal;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * ComisionCotizacionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOCOMISIONCOT", schema = "MIDAS")
public class ComisionCotizacionDTO implements java.io.Serializable, Entidad {

	private static final long serialVersionUID = 1L;
	private ComisionCotizacionId id;
	private CotizacionDTO cotizacionDTO;
	private SubRamoDTO subRamoDTO;
	private BigDecimal valorComisionCotizacion;
	private Short claveAutComision;
	private String codigoUsuarioAutComision;
	private BigDecimal PorcentajeComisionDefault;
	private BigDecimal PorcentajeComisionCotizacion;

	// Constructors

	/** default constructor */
	public ComisionCotizacionDTO() {
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToCotizacion", column = @Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idTcSubramo", column = @Column(name = "IDTCSUBRAMO", nullable = false, precision = 22, scale = 0)) })
	public ComisionCotizacionId getId() {
		return this.id;
	}

	public void setId(ComisionCotizacionId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTOCOTIZACION", nullable = false, insertable = false, updatable = false)
	public CotizacionDTO getCotizacionDTO() {
		return this.cotizacionDTO;
	}

	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTCSUBRAMO", nullable = false, insertable = false, updatable = false)
	public SubRamoDTO getSubRamoDTO() {
		return this.subRamoDTO;
	}

	public void setSubRamoDTO(SubRamoDTO subRamoDTO) {
		this.subRamoDTO = subRamoDTO;
	}

	@Column(name = "VALORCOMISIONCOTIZACION", nullable = false, precision = 16)
	public BigDecimal getValorComisionCotizacion() {
		return this.valorComisionCotizacion;
	}

	public void setValorComisionCotizacion(BigDecimal valorComisionCotizacion) {
		this.valorComisionCotizacion = valorComisionCotizacion;
	}

	@Column(name = "CLAVEAUTCOMISION", nullable = false, precision = 4, scale = 0)
	public Short getClaveAutComision() {
		return this.claveAutComision;
	}

	public void setClaveAutComision(Short claveAutComision) {
		this.claveAutComision = claveAutComision;
	}

	@Column(name = "CODIGOUSUARIOAUTCOMISION", length = 8)
	public String getCodigoUsuarioAutComision() {
		return this.codigoUsuarioAutComision;
	}

	public void setCodigoUsuarioAutComision(String codigoUsuarioAutComision) {
		this.codigoUsuarioAutComision = codigoUsuarioAutComision;
	}

	@Column(name = "PORCENTAJECOMISIONDEFAULT")
	public BigDecimal getPorcentajeComisionDefault() {
		return PorcentajeComisionDefault;
	}

	public void setPorcentajeComisionDefault(BigDecimal porcentajeComisionDefault) {
		PorcentajeComisionDefault = porcentajeComisionDefault;
	}

	@Column(name = "PORCENTAJECOMISIONCOTIZACION")
	public BigDecimal getPorcentajeComisionCotizacion() {
		return PorcentajeComisionCotizacion;
	}

	public void setPorcentajeComisionCotizacion(
			BigDecimal porcentajeComisionCotizacion) {
		PorcentajeComisionCotizacion = porcentajeComisionCotizacion;
	}
	
	@Override
	public ComisionCotizacionId getKey() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
}