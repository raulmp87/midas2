package mx.com.afirme.midas.cotizacion.comision;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * ComisionCotizacionId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class ComisionCotizacionId implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private BigDecimal idToCotizacion;
	private BigDecimal idTcSubramo;
	private Short tipoPorcentajeComision;

	// Constructors

	/** default constructor */
	public ComisionCotizacionId() {
	}

	/** full constructor */
	public ComisionCotizacionId(BigDecimal idToCotizacion,
			BigDecimal idTcSubramo) {
		this.idToCotizacion = idToCotizacion;
		this.idTcSubramo = idTcSubramo;
	}

	// Property accessors

	@Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCotizacion() {
		return this.idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	@Column(name = "IDTCSUBRAMO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcSubramo() {
		return this.idTcSubramo;
	}

	public void setIdTcSubramo(BigDecimal idTcSubramo) {
		this.idTcSubramo = idTcSubramo;
	}

	@Column(name = "CLAVETIPOPORCENTAJECOMISION")
	public Short getTipoPorcentajeComision() {
		return tipoPorcentajeComision;
	}

	public void setTipoPorcentajeComision(Short tipoPorcentajeComision) {
		this.tipoPorcentajeComision = tipoPorcentajeComision;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ComisionCotizacionId))
			return false;
		ComisionCotizacionId castOther = (ComisionCotizacionId) other;

		return ((this.getIdToCotizacion() == castOther.getIdToCotizacion()) || (this.getIdToCotizacion() != null
				&& castOther.getIdToCotizacion() != null && this.getIdToCotizacion().equals(castOther.getIdToCotizacion())))
				&& ((this.getIdTcSubramo() == castOther.getIdTcSubramo()) || (this.getIdTcSubramo() != null
						&& castOther.getIdTcSubramo() != null && this.getIdTcSubramo().equals(castOther.getIdTcSubramo())))
				&& ((this.getTipoPorcentajeComision() == castOther.getTipoPorcentajeComision()) || (this.getTipoPorcentajeComision() != null
						&& castOther.getTipoPorcentajeComision() != null && this.getTipoPorcentajeComision().equals(castOther.getTipoPorcentajeComision())));
	}

	public int hashCode() {
		int result = 17;
		result = 37 * result + (getIdToCotizacion() == null ? 0 : this.getIdToCotizacion().hashCode());
		result = 37 * result + (getIdTcSubramo() == null ? 0 : this.getIdTcSubramo().hashCode());
		result = 37 * result + (getTipoPorcentajeComision() == null ? 0 : this.getTipoPorcentajeComision().hashCode());
		return result;
	}
}