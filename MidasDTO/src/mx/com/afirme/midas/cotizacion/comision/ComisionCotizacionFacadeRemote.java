package mx.com.afirme.midas.cotizacion.comision;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

/**
 * Remote interface for ComisionCotizacionDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ComisionCotizacionFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved ComisionCotizacionDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            ComisionCotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ComisionCotizacionDTO entity);

	/**
	 * Delete a persistent ComisionCotizacionDTO entity.
	 * 
	 * @param entity
	 *            ComisionCotizacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ComisionCotizacionDTO entity);

	/**
	 * Persist a previously saved ComisionCotizacionDTO entity and return it or
	 * a copy of it to the sender. A copy of the ComisionCotizacionDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            ComisionCotizacionDTO entity to update
	 * @return ComisionCotizacionDTO the persisted ComisionCotizacionDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ComisionCotizacionDTO update(ComisionCotizacionDTO entity);

	public ComisionCotizacionDTO findById(ComisionCotizacionId id);

	/**
	 * Find all ComisionCotizacionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ComisionCotizacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<ComisionCotizacionDTO> found by query
	 */
	public List<ComisionCotizacionDTO> findByProperty(String propertyName,
			Object value);

	public List<ComisionCotizacionDTO> findByPorcentajeComisionDefault(
			Object porcentajeComisionDefault);

	public List<ComisionCotizacionDTO> findByPorcentajeComisionCotizacion(
			Object porcentajeComisionCotizacion);

	public List<ComisionCotizacionDTO> findByValorComisionCotizacion(
			Object valorComisionCotizacion);

	public List<ComisionCotizacionDTO> findByClaveAutComision(
			Object claveAutComision);

	public List<ComisionCotizacionDTO> findByCodigoUsuarioAutComision(
			Object codigoUsuarioAutComision);

	/**
	 * Find all ComisionCotizacionDTO entities.
	 * 
	 * @return List<ComisionCotizacionDTO> all ComisionCotizacionDTO entities
	 */
	public List<ComisionCotizacionDTO> findAll();
	
	/**
	 * Encuentra los registros de ComisionCotizacionDTO correspondientes al idToCotizacion recibido
	 * y que ademas tienen clave de Autorizacion recibida.
	 * @param idToCotizacion
	 * @param claveAutorizacion
	 * @return List<ComisionCotizacionDTO> found by query
	 */
	public List<ComisionCotizacionDTO> encontrarPorClaveAutirizacionPorCotizacion(BigDecimal idToCotizacion,Short claveAutorizacion);
	
	/**
	 * Encuentra los registros de ComisionCotizacionDTO correspondientes al idToCotizacion recibido
	 * y que ademas pueden ser autorizados.
	 * @param idToCotizacion
	 * @param claveAutorizacion
	 * @return List<ComisionCotizacionDTO> found by query
	 */
	public List<ComisionCotizacionDTO> buscarComisionesPorAutorizar(BigDecimal idToCotizacion);
}