package mx.com.afirme.midas.cotizacion.resumen;

import java.io.Serializable;

import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionId;

public class SoporteResumen implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Double montoComision;
	private String descripcionRamo;
	private Double porcentajeComision;
	private Double montoComisionCedida;
	private ComisionCotizacionId id;

	public Double getMontoComision() {
		return montoComision;
	}

	public void setMontoComision(Double montoComision) {
		this.montoComision = montoComision;
	}

	public String getDescripcionRamo() {
		return descripcionRamo;
	}

	public void setDescripcionRamo(String descripcionRamo) {
		this.descripcionRamo = descripcionRamo;
	}

	public Double getPorcentajeComision() {
		return porcentajeComision;
	}

	public void setPorcentajeComision(Double porcentajeComision) {
		this.porcentajeComision = porcentajeComision;
	}

	public Double getMontoComisionCedida() {
		return montoComisionCedida;
	}

	public void setMontoComisionCedida(Double montoComisionCedida) {
		this.montoComisionCedida = montoComisionCedida;
	}

	public ComisionCotizacionId getId() {
		return id;
	}

	public void setId(ComisionCotizacionId id) {
		this.id = id;
	}

}
