package mx.com.afirme.midas.cotizacion.resumen;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.bean.pago.EsquemaPagosDTO;
import mx.com.afirme.midas.sistema.SystemException;


public interface ResumenCotizacionFacadeRemote {

	public CotizacionDTO calcularTotalesResumenCotizacion(CotizacionDTO cotizacionDTO, List<CoberturaCotizacionDTO> coberturas) throws SystemException;
	
	public CotizacionDTO setTotalesResumenCambioFormaPagoImpresion(CotizacionDTO cotizacionEndosoActual) throws SystemException;
	
	public CotizacionDTO setTotalesResumenCotizacion(CotizacionDTO cotizacionDTO, List<SoporteResumen> resumenComisiones) throws SystemException;

	public EsquemaPagosDTO obtenerTotalesResumenCotizacion(BigDecimal idToCotizacion);
	
	public EsquemaPagosDTO calcularRecibosCotizacion(
			Integer cantidadRecibosAnual,BigDecimal totalPrimaNeta,
			BigDecimal totalRecargo,BigDecimal totalDerecho,
			BigDecimal totalIva,BigDecimal factorIVA);
	
	public EsquemaPagosDTO calcularRecibosCotizacion(Integer cantidadRecibosAnual,Date fechaInicioVigencia,Date fechaFinVigencia,
			BigDecimal totalPrimaNeta,
			BigDecimal totalRecargo,BigDecimal totalDerecho,
			BigDecimal totalIva,BigDecimal factorIVA);
}
