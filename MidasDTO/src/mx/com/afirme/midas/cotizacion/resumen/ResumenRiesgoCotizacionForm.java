package mx.com.afirme.midas.cotizacion.resumen;

import java.io.Serializable;

public class ResumenRiesgoCotizacionForm implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3256769716887416985L;
	private String numeroRiesgo;
	private String descripcionRiesgo;
	private String sumaAsegurada;
	public String getNumeroRiesgo() {
		return numeroRiesgo;
	}
	public void setNumeroRiesgo(String numeroRiesgo) {
		this.numeroRiesgo = numeroRiesgo;
	}
	public String getDescripcionRiesgo() {
		return descripcionRiesgo;
	}
	public void setDescripcionRiesgo(String descripcionRiesgo) {
		this.descripcionRiesgo = descripcionRiesgo;
	}
	public String getSumaAsegurada() {
		return sumaAsegurada;
	}
	public void setSumaAsegurada(String sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
}
