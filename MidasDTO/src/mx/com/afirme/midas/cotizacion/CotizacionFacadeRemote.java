package mx.com.afirme.midas.cotizacion;

// default package

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.catalogos.giro.SubGiroDTO;
import mx.com.afirme.midas.catalogos.plenoreaseguro.PlenoReaseguroDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.solicitud.SolicitudDTO;

/**
 * Remote interface for CotizacionDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface CotizacionFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved CotizacionDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            CotizacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public CotizacionDTO save(CotizacionDTO entity);

	/**
	 * Delete a persistent CotizacionDTO entity.
	 * 
	 * @param entity
	 *            CotizacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(CotizacionDTO entity);

	/**
	 * Persist a previously saved CotizacionDTO entity and return it or a copy
	 * of it to the sender. A copy of the CotizacionDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            CotizacionDTO entity to update
	 * @return CotizacionDTO the persisted CotizacionDTO entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public CotizacionDTO update(CotizacionDTO entity);

	public CotizacionDTO findById(BigDecimal id);

	/**
	 * Find all CotizacionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the CotizacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<CotizacionDTO> found by query
	 */
	public List<CotizacionDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all CotizacionDTO entities.
	 * 
	 * @return List<CotizacionDTO> all CotizacionDTO entities
	 */
	public List<CotizacionDTO> findAll();

	public List<CotizacionDTO> listarFiltrado(CotizacionDTO cotizacionDTO);

	public Long obtenerTotalFiltrado(CotizacionDTO cotizacionDTO);
	
	public CotizacionDTO generarCotizacionASolicitud(BigDecimal idCotizacion,
			SolicitudDTO solicitudEndoso, String nombreUsuario,
			String nombreUsuarioCotizacion);

	public CotizacionDTO getCotizacionFull(BigDecimal idToCotizacion);

	public Double getPrimaNetaCotizacion(BigDecimal idToCotizacion);

	public List<CotizacionDTO> listarCotizaciones();
	public List<CotizacionDTO> listarCotizacionesFiltrado(CotizacionDTO cotizacionDTO);
	public Long obtenerTotalCotizacionesFiltrado(CotizacionDTO cotizacionDTO);

	public List<CotizacionDTO> obtenerCotizacionesFacultadas(Date fechaInicial,Date fechaFinal);
	
	public Double obtenerPorcentajeRecargoPagoFraccionadoCotizacion(CotizacionDTO cotizacionDTO) throws SystemException;
	
	public CotizacionDTO modificarRecargoPagoFraccionado(BigDecimal idToCotizacion, Short claveRecargoPagoFraccionadoUsuario,
			Double valorRecargoPagoFraccionado, boolean calculoPoliza, boolean sinAutorizacion) throws SystemException;
	
	public Double calcularDerechosCotizacion(CotizacionDTO cotizacionDTO, boolean calculoPoliza);
	
	public CotizacionDTO modificarDerechos(BigDecimal idToCotizacion, Short claveDerechosUsuario, Double valorDerechos, boolean calculoPoliza, boolean sinAutorizacion);
	
	public CotizacionDTO recalcularCotizacion(CotizacionDTO cotizacion,String nombreUsuario);
	
	public BigDecimal obtenerIdToTipoPoliza(String codigoProducto,String codigoTipoPoliza);
	public PlenoReaseguroDTO obtenerPlenoReaseguro(SubGiroDTO subGiroDTO);
	public List<CotizacionDTO> listarFiltradoCotizacion(CotizacionDTO cotizacionDTO, Date fechaInicial, Date fechaFinal, String agentes, String gerencias, BigDecimal valorPrimaTotalFin,Double descuento,Double descuentoFin);
	
	/**
	 Perform an initial save of a previously unsaved CotizacionDTO entity and return its id value. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CotizacionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
	 public BigDecimal saveAndGetId(CotizacionDTO cotizacionDTO);
	 
	 public Long obtenerTotalFiltradoCotizacion(CotizacionDTO cotizacionDTO, Date fechaInicial, Date fechaFinal, String agentes, String gerencias, BigDecimal valorPrimaTotalFin,Double descuento,Double descuentoFin);
	 
	 public List<CotizacionDTO> cotizacionPorDescripcionVehiculo(IncisoCotizacionDTO filtroInicso,List<CotizacionDTO> cotizacionesFiltradas);
}