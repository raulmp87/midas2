package mx.com.afirme.midas.contratos.contratoprimerexcedente;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.base.LogBaseDTO;
import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDTO;


/**
 * ContratoPrimerExcedenteDTO entity. @author MyEclipse Persistence Tools
 */

@Entity
@Table(name="TMCONTRATOPRIMEREXCEDENTE"
    ,schema="MIDAS"
)

public class ContratoPrimerExcedenteDTO extends LogBaseDTO {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idTmContratoPrimerExcedente;
     private String folioContrato;
     private Date fechaInicial;
     private Date fechaFinal;
     private BigDecimal limiteMaximo;
     private BigDecimal numeroPlenos;
     private Double montoPleno;
     private BigDecimal idMoneda;
     private BigDecimal usuarioAutorizo;
     private Date fechaAutorizacion;
     private BigDecimal estatus;
     private BigDecimal formaPago;

     
	private List<ParticipacionDTO> participacionDTOList;
    private List<LineaDTO> lineaDTOs;


    // Constructors

    /** default constructor */
    public ContratoPrimerExcedenteDTO() {
    	if (participacionDTOList == null)
    		participacionDTOList = new ArrayList<ParticipacionDTO>(0);
    	if (lineaDTOs == null)
    		lineaDTOs = new ArrayList<LineaDTO>(0);
    }
   
    // Property accessors
    @Id 
    @SequenceGenerator(name = "IDTMCONPRIMEREXCEDENTE_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTMCONPRIMEREXCEDENTE_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTMCONPRIMEREXCEDENTE_SEQ_GENERADOR")
    @Column(name="IDTMCONTRATOPRIMEREXCEDENTE", unique=true, nullable=false, precision=22, scale=0)
    public BigDecimal getIdTmContratoPrimerExcedente() {
        return this.idTmContratoPrimerExcedente;
    }
    
    public void setIdTmContratoPrimerExcedente(BigDecimal idTmContratoPrimerExcedente) {
        this.idTmContratoPrimerExcedente = idTmContratoPrimerExcedente;
   }
    
    @Column(name="FOLIOCONTRATO", nullable=false, length=7)

    public String getFolioContrato() {
        return this.folioContrato;
    }
    
    public void setFolioContrato(String folioContrato) {
        this.folioContrato = folioContrato;
    }
@Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHAINICIAL", nullable=false, length=7)

    public Date getFechaInicial() {
        return this.fechaInicial;
    }
    
    public void setFechaInicial(Date fechaInicial) {
        this.fechaInicial = fechaInicial;
    }
@Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHAFINAL", nullable=false, length=7)

    public Date getFechaFinal() {
        return this.fechaFinal;
    }
    
    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }
    
    @Column(name="LIMITEMAXIMO", nullable=false, precision=20, scale=8)

    public BigDecimal getLimiteMaximo() {
        return this.limiteMaximo;
    }
    
    public void setLimiteMaximo(BigDecimal limiteMaximo) {
        this.limiteMaximo = limiteMaximo;
    }
    
    @Column(name="NUMEROPLENOS", nullable=false, precision=22, scale=0)

    public BigDecimal getNumeroPlenos() {
        return this.numeroPlenos;
    }
    
    public void setNumeroPlenos(BigDecimal numeroPlenos) {
        this.numeroPlenos = numeroPlenos;
    }
    
    @Column(name="ESTATUS", nullable=false, precision=22, scale=0)
    
    public BigDecimal getEstatus() {
		return estatus;
	}

	public void setEstatus(BigDecimal estatus) {
		this.estatus = estatus;
	}

    @Column(name="MONTOPLENO", nullable=false, precision=20, scale=8)

    public Double getMontoPleno() {
        return this.montoPleno;
    }
    
    public void setMontoPleno(Double montoPleno) {
        this.montoPleno = montoPleno;
    }
    
    @Column(name="IDTCMONEDA", precision=22, scale=0)
    public BigDecimal getIdMoneda() {
        return this.idMoneda;
    }
    
    public void setIdMoneda(BigDecimal idMoneda) {
        this.idMoneda = idMoneda;
    }
    
    @Column(name="USUARIOAUTORIZO", precision=22, scale=0)

    public BigDecimal getUsuarioAutorizo() {
        return this.usuarioAutorizo;
    }
    
    public void setUsuarioAutorizo(BigDecimal usuarioAutorizo) {
        this.usuarioAutorizo = usuarioAutorizo;
    }
@Temporal(TemporalType.DATE)
    @Column(name="FECHAAUTORIZACION", length=7)

    public Date getFechaAutorizacion() {
        return this.fechaAutorizacion;
    }
    
    public void setFechaAutorizacion(Date fechaAutorizacion) {
        this.fechaAutorizacion = fechaAutorizacion;
    }
    
    @Column(name="FORMAPAGO", nullable=false, precision=22)
    public BigDecimal getFormaPago() {
    	return formaPago;
    }

    public void setFormaPago(BigDecimal formaPago) {
    	this.formaPago = formaPago;
    }
    
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="contratoPrimerExcedente")

    public List<ParticipacionDTO> getParticipacionDTOList() {
        return this.participacionDTOList;
    }
    
    public void setParticipacionDTOList(List<ParticipacionDTO> participacionDTOList) {
        this.participacionDTOList = participacionDTOList;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="contratoPrimerExcedente")

    public List<LineaDTO> getLineas() {
        return this.lineaDTOs;
    }
    
    public void setLineas(List<LineaDTO> lineaDTOs) {
        this.lineaDTOs = lineaDTOs;
    }

}