package mx.com.afirme.midas.contratos.contratoprimerexcedente;
// default package

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDTO;

/**
 * Remote interface for ContratoPrimerExcedenteFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ContratoPrimerExcedenteFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ContratoPrimerExcedenteDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ContratoPrimerExcedenteDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public ContratoPrimerExcedenteDTO save(ContratoPrimerExcedenteDTO entity);
    /**
	 Delete a persistent ContratoPrimerExcedenteDTO entity.
	  @param entity ContratoPrimerExcedenteDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ContratoPrimerExcedenteDTO entity);
   /**
	 Persist a previously saved ContratoPrimerExcedenteDTO entity and return it or a copy of it to the sender. 
	 A copy of the ContratoPrimerExcedenteDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ContratoPrimerExcedenteDTO entity to update
	 @return ContratoPrimerExcedenteDTO the persisted ContratoPrimerExcedenteDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ContratoPrimerExcedenteDTO update(ContratoPrimerExcedenteDTO entity);
	public ContratoPrimerExcedenteDTO findById( BigDecimal id);
	 /**
	 * Find all ContratoPrimerExcedenteDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ContratoPrimerExcedenteDTO property to query
	  @param value the property value to match
	  	  @return List<ContratoPrimerExcedenteDTO> found by query
	 */
	public List<ContratoPrimerExcedenteDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ContratoPrimerExcedenteDTO entities.
	  	  @return List<ContratoPrimerExcedenteDTO> all ContratoPrimerExcedenteDTO entities
	 */
	public List<ContratoPrimerExcedenteDTO> findAll(
		);
	
	/**
	 * Find filtered ContratoPrimerExcedenteDTO entities.
	  	  @return List<ContratoPrimerExcedenteDTO> filtered ContratoPrimerExcedenteDTO entities
	 */
	public List<ContratoPrimerExcedenteDTO> listarFiltrado(ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO);
	
	/**
	 * Find filtered ContratoPrimerExcedenteDTO entities.
	  	  @return List<ContratoPrimerExcedenteDTO> filtered ContratoPrimerExcedenteDTO entities
	 */
	public List<ContratoPrimerExcedenteDTO> buscarPorFechaInicialFinal(Date fInicial, Date fFinal);
	
	/**
	 * Update ContratoPrimerExcedenteDTO entities by dates using as a list of ids as filter(using this formart 'id,id,id') .
	 *   	  @return ContratoPrimerExcedenteDTO the persisted ContratoPrimerExcedenteDTO entity instance, may not be the same
	 */
	public boolean actualizaFechasLineasPorIdLineas(String IdLineas,Date fInicial,Date fFinal);
	
	/**
	 *Consulta el contrato primer excedente del ejercicio anterior que tenga los mismos atributos que el estado de cuenta recibido 
	 *(reasegurador, corredor, moneda, ramo, subramo y forma de pago).
	 * @param estadoCuentaDTO
	 * @return ContratoPrimerExcedenteDTO del ejercicio anterior, null si no existe.
	 */
	public ContratoPrimerExcedenteDTO obtenerContratoPrimerExcedenteEjercicioAnterior(EstadoCuentaDTO estadoCuentaDTO);
	
	/**
	 *Consulta los contratos primer excedente de ejercicios anteriores que tengan los mismos atributos que el estado de cuenta recibido 
	 *(reasegurador, corredor, moneda, ramo, subramo y forma de pago).
	 * @param estadoCuentaDTO
	 * @return ContratoPrimerExcedenteDTO del ejercicio anterior, null si no existe.
	 */
	public List<ContratoPrimerExcedenteDTO> obtenerListaContratoPrimerExcedenteEjercicioAnterior(EstadoCuentaDTO estadoCuentaDTO);
}