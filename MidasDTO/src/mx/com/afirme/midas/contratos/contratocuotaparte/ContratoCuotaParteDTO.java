package mx.com.afirme.midas.contratos.contratocuotaparte;
// default package

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.base.LogBaseDTO;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDTO;
import mx.com.afirme.midas.contratos.linea.LineaDTO;

/**
 * ContratoCuotaParteDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TMCONTRATOCUOTAPARTE"
    ,schema="MIDAS"
)

public class ContratoCuotaParteDTO extends LogBaseDTO {
	private static final long serialVersionUID = 1L;

    // Fields    

     private BigDecimal idTmContratoCuotaParte;
     private Date fechaInicial;
     private Date fechaFinal;
     private Double porcentajeRetencion;
     private Double porcentajeCesion;
     private BigDecimal idTcMoneda;
     private String folioContrato;
     private BigDecimal estatus;
     private BigDecimal usuarioAutorizo;
     private Date fechaAutorizacion;
     private BigDecimal formaPago;
     private List<ParticipacionDTO> participaciones;
     private List<LineaDTO> lineas;

    // Constructors

    /** default constructor */
    public ContratoCuotaParteDTO() {
    	if(participaciones == null)
    		participaciones = new ArrayList<ParticipacionDTO>();
    	
    	if(lineas == null)
    		lineas = new ArrayList<LineaDTO>();
    }


   
    // Property accessors
    @Id 
    @SequenceGenerator(name = "IDTMCONTRATOCUOTAPARTE_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTMCONTRATOCUOTAPARTE_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTMCONTRATOCUOTAPARTE_SEQ_GENERADOR")	  	
    @Column(name="IDTMCONTRATOCUOTAPARTE", unique=true, nullable=false, precision=22, scale=0)
    public BigDecimal getIdTmContratoCuotaParte() {
        return this.idTmContratoCuotaParte;
    }
    
    public void setIdTmContratoCuotaParte(BigDecimal idTmContratoCuotaParte) {
        this.idTmContratoCuotaParte = idTmContratoCuotaParte;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHAINICIAL", nullable=false, length=7)

    public Date getFechaInicial() {
        return this.fechaInicial;
    }
    
    public void setFechaInicial(Date fechaInicial) {
        this.fechaInicial = fechaInicial;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHAFINAL", nullable=false, length=7)

    public Date getFechaFinal() {
        return this.fechaFinal;
    }
    
    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }
    
    @Column(name="PORCENTAJERETENCION", nullable=false, precision=5)

    public Double getPorcentajeRetencion() {
        return this.porcentajeRetencion;
    }
    
    public void setPorcentajeRetencion(Double porcentajeRetencion) {
        this.porcentajeRetencion = porcentajeRetencion;
    }
    
    @Column(name="PORCENTAJECESION", nullable=false, precision=5)

    public Double getPorcentajeCesion() {
        return this.porcentajeCesion;
    }
    
    public void setPorcentajeCesion(Double porcentajeCesion) {
        this.porcentajeCesion = porcentajeCesion;
    }
    
    @Column(name="IDTCMONEDA", precision=22, scale=0)

    public BigDecimal getIdTcMoneda() {
        return this.idTcMoneda;
    }
    
    public void setIdTcMoneda(BigDecimal idTcMoneda) {
        this.idTcMoneda = idTcMoneda;
    }
    
    @Column(name="FOLIOCONTRATO", nullable=false, length=7)

    public String getFolioContrato() {
        return this.folioContrato;
    }
    
    public void setFolioContrato(String folioContrato) {
        this.folioContrato = folioContrato;
    }
    
    @Column(name="ESTATUS", nullable=false, precision=22, scale=0)

    public BigDecimal getEstatus() {
        return this.estatus;
    }
    
    public void setEstatus(BigDecimal estatus) {
        this.estatus = estatus;
    }
    
    @Column(name="USUARIOAUTORIZO", precision=22, scale=0)

    public BigDecimal getUsuarioAutorizo() {
        return this.usuarioAutorizo;
    }
    
    public void setUsuarioAutorizo(BigDecimal usuarioAutorizo) {
        this.usuarioAutorizo = usuarioAutorizo;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAAUTORIZACION", length=7)

    public Date getFechaAutorizacion() {
        return this.fechaAutorizacion;
    }
    
    public void setFechaAutorizacion(Date fechaAutorizacion) {
        this.fechaAutorizacion = fechaAutorizacion;
    }
    
    @Column(name="FORMAPAGO", nullable=false, precision=22, scale=0)

    public BigDecimal getFormaPago() {
        return this.formaPago;
    }
    
    public void setFormaPago(BigDecimal formaPago) {
        this.formaPago = formaPago;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="contratoCuotaParte")

    public List<ParticipacionDTO> getParticipaciones() {
        return this.participaciones;
    }
    
    public void setParticipaciones(List<ParticipacionDTO> participaciones) {
        this.participaciones = participaciones;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="contratoCuotaParte")

    public List<LineaDTO> getLineas() {
        return this.lineas;
    }
    
    public void setLineas(List<LineaDTO> lineas) {
        this.lineas = lineas;
    }


}