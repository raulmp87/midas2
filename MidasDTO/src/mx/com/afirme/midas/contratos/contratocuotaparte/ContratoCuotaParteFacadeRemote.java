package mx.com.afirme.midas.contratos.contratocuotaparte;
// default package

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDTO;

/**
 * Remote interface for ContratoCuotaParteFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ContratoCuotaParteFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ContratoCuotaParteDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ContratoCuotaParteDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public ContratoCuotaParteDTO save(ContratoCuotaParteDTO entity);
    /**
	 Delete a persistent ContratoCuotaParteDTO entity.
	  @param entity ContratoCuotaParteDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ContratoCuotaParteDTO entity);
   /**
	 Persist a previously saved ContratoCuotaParteDTO entity and return it or a copy of it to the sender. 
	 A copy of the ContratoCuotaParteDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ContratoCuotaParteDTO entity to update
	 @return ContratoCuotaParteDTO the persisted ContratoCuotaParteDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ContratoCuotaParteDTO update(ContratoCuotaParteDTO entity);
	public ContratoCuotaParteDTO findById( BigDecimal id);
	 /**
	 * Find all ContratoCuotaParteDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ContratoCuotaParteDTO property to query
	  @param value the property value to match
	  	  @return List<ContratoCuotaParteDTO> found by query
	 */
	public List<ContratoCuotaParteDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ContratoCuotaParteDTO entities.
	  	  @return List<ContratoCuotaParteDTO> all ContratoCuotaParteDTO entities
	 */
	public List<ContratoCuotaParteDTO> findAll(
		);
	
	/**
	 * Find filtered ContratoCuotaParteDTO entities.
	  	  @return List<ContratoCuotaParteDTO> filtered ContratoCuotaParteDTO entities
	 */
	public List<ContratoCuotaParteDTO> listarFiltrado(ContratoCuotaParteDTO contratoCuotaParteDTO);
	
	/**
	 * Find filtered ContratoCuotaParteDTO entities.
	  	  @return List<ContratoCuotaParteDTO> filtered ContratoCuotaParteDTO entities
	 */
	public List<ContratoCuotaParteDTO> buscarPorFechaInicialFinal(Date initialDate, Date finalDate);
	
	/**
	 * Update ContratoCuotaParteDTO entities by dates using as a list of ids as filter(using this formart 'id,id,id') .
	 *   	  @return ContratoCuotaParteDTO the persisted LineaDTO entity instance, may not be the same
	 */
	public boolean actualizaFechasLineasPorIdLineas(String IdLineas,Date fInicial,Date fFinal);
	
	/**
	 * Cancel ContratoCuotaParteDTO with the corresponding IdTmContratoCuotaParte.
	 *   	  @return ContratoCuotaParteDTO cancelled.
	 */
	public ContratoCuotaParteDTO cancelarContratoCuotaParte(ContratoCuotaParteDTO contratoCuotaParteDTO);
	
	/**
	 *Consulta el contrato Cuota Parte del ejercicio anterior que tenga los mismos atributos que el estado de cuenta recibido 
	 *(reasegurador, corredor, moneda, ramo, subramo y forma de pago).
	 * @param estadoCuentaDTO
	 * @return ContratoCuotaParteDTO del ejercicio anterior, null si no existe.
	 */
	public ContratoCuotaParteDTO obtenerContratoCuotaParteEjercicioAnterior(EstadoCuentaDTO estadoCuentaDTO);
	
	/**
	 *Consulta los contratos Cuota Parte de ejercicios anteriores que tengan los mismos atributos que el estado de cuenta recibido 
	 *(reasegurador, corredor, moneda, ramo, subramo y forma de pago).
	 * @param estadoCuentaDTO
	 * @return ContratoCuotaParteDTO del ejercicio anterior, null si no existe.
	 */
	public List<ContratoCuotaParteDTO> obtenerListaContratoCuotaParteEjercicioAnterior(EstadoCuentaDTO estadoCuentaDTO);
}