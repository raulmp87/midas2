package mx.com.afirme.midas.contratos.linea;
// default package

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.contratos.participacion.ParticipacionDTO;

/**
 * Remote interface for LineaFacade.
 * @author MyEclipse Persistence Tools
 */


public interface LineaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved LineaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity LineaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public LineaDTO save(LineaDTO entity);
    /**
	 Delete a persistent LineaDTO entity.
	  @param entity LineaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(LineaDTO entity);
   /**
	 Persist a previously saved LineaDTO entity and return it or a copy of it to the sender. 
	 A copy of the LineaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity LineaDTO entity to update
	 @return LineaDTO the persisted LineaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public LineaDTO update(LineaDTO entity);
	public LineaDTO findById( BigDecimal id);
	 /**
	 * Find all LineaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the LineaDTO property to query
	  @param value the property value to match
	  	  @return List<LineaDTO> found by query
	 */
	public List<LineaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all LineaDTO entities.
	 * 	  	  @return List<LineaDTO> all LineaDTO entities
	 */
	public List<LineaDTO> findAll(
		);
	
	/**
	 * Find filtered LineaDTO entities.
	 * 	  	  @return List<LineaDTO> filtered LineaDTO entities
	 */
	public List<LineaDTO> buscarPorFechaInicialFinal(Date fInicial, Date fFinal);
	
	/**
	 * Update LineaDTO entities by dates using as a list of ids as filter(using this format 'id,id,id') .
	 *   	  @return LineaDTO the persisted LineaDTO entity instance, may not be the same
	 */
	public boolean actualizaFechasLineasPorIdLineas(String ids,String idsCCP,String idsCPE, Date fInicial, Date fFinal);
	
	/**
	 * Duplicate LineaDTO entity.
	 *   	  @return if if was possible to duplicate the selected line
	 */
	public LineaDTO duplicarLinea(LineaDTO lineaDTO, List<ParticipacionDTO> pCCP, List<ParticipacionDTO> pCPE);
	
	
	/**
	 * validateExercise for specific LineaDTO entity.
	 *   	  @return if if was valid return true
	 */
	public boolean validaEjercicio(LineaDTO lineaDTO); 
	
	/**
	 * obtenerEjercicios retrieve a list of EjercicioDTO with all the excercises (represented by the years: e.g. "05-06", "07-08" .. etc) from LineaDTO.FechaInicial 
	 * @return List<EjercicioDTO>, list of EjercicioDTO 
	 */
	public List<EjercicioDTO> obtenerEjercicios();
	
	/**
	 * obtenerSuscripcionesPorLinea retrieve a list of SuscripcionDTO found using SubRamo, Ejercicio and Tipo Reaseguro 
	 * @return List<SuscripcionDTO>, list of SuscripcionDTO 
	 */
	public List<SuscripcionDTO> obtenerSuscripcionesPorLinea(BigDecimal idTcSubRamo,int ejercicio ,int tipoReaseguro);
	
	/**
	 * Delete the association between LineaDTO and a ContratoCuotaParteDTO, in this process the commissions are deleted too
	 * @param lineaDTO
	 */
	public void desasociarContratoCPLinea(LineaDTO lineaDTO);
	
	/**
	 * Delete the association between LineaDTO and a ContratoPrimerExcedenteDTO, in this process the commissions are deleted too
	 * @param lineaDTO
	 */
	public void desasociarContratoPELinea(LineaDTO lineaDTO);
	
	/**
	 * Obtains the current excercise based on existing lines (LineaDTO's)
	 * @return
	 */
	public int obtenerEjercicioActual();

	public BigDecimal obtenerTipoDistribucion (BigDecimal idToPoliza,BigDecimal idToCobertura);
}