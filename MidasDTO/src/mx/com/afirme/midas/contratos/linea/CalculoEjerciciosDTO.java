package mx.com.afirme.midas.contratos.linea;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class CalculoEjerciciosDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 270451254403856909L;

	private Date fechaInicial;
	private Date fechaFin;
	private List<EjercicioDTO> ejercicioDTOList;
	
	
	// Getters & Setters
	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	public List<EjercicioDTO> obtenerListaPeriodos(){
		ejercicioDTOList = new ArrayList<EjercicioDTO>();
		int anioInicial;
		int anioFinal;
		if (fechaInicial != null && fechaFin != null){
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(fechaInicial);
			anioInicial = calendar.get(Calendar.YEAR);
			calendar.setTime(fechaFin);
			anioFinal = calendar.get(Calendar.YEAR);
			if (anioInicial == anioFinal){
				EjercicioDTO ejercicioDTO = new EjercicioDTO();
				ejercicioDTO.setActual(true);
				ejercicioDTO.setAnio(anioFinal);
				ejercicioDTO.setClave(anioInicial);
				ejercicioDTOList.add(ejercicioDTO);
			}else				
				for (int anioEnTurno = anioInicial; anioEnTurno <= anioFinal; anioEnTurno++){
					EjercicioDTO ejercicioDTO = new EjercicioDTO();
					
					if (anioEnTurno == anioFinal)
						ejercicioDTO.setActual(true);
					else
						ejercicioDTO.setActual(false);
					ejercicioDTO.setAnio(anioEnTurno);
					ejercicioDTO.setClave(anioEnTurno);
					ejercicioDTOList.add(ejercicioDTO);
				}
		}
		
		return ejercicioDTOList;
	}

	public List<EjercicioDTO> getEjercicioDTOList() {
		return this.obtenerListaPeriodos();
	}
	
	public void setEjercicioDTOList(List<EjercicioDTO> ejercicioDTOList) {
		
	}
	
	
	/**
	 * 
	 * @param fechaInicial
	 * @return
	 */
	public static int obtenerEjercicio(Date fechaInicial){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fechaInicial);
		int anioInicial = calendar.get(Calendar.YEAR);
		return anioInicial;
	}
	
}
