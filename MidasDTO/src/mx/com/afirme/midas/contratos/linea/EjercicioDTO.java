package mx.com.afirme.midas.contratos.linea;

import java.io.Serializable;

public class EjercicioDTO implements Serializable{
	
	
	/**
	 * This DTO is not mapped with the data base, instead, this DTO is used only as a cache for the data used in combo 'Ejercicios' from Estados de Cuenta por Tipo Reaseguro
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer anio;
	private String descripcion;
	private boolean actual;

	
	// Getters & Setters
	public Integer getAnio() {
		return anio;
	}
	
	public void setAnio(Integer anio) {
		this.anio = anio;
	}

	public void setClave(Integer anio) {
		this.anio = anio;
	}

	public String getDescripcion() {
		String anioInicioStr;
		String anioFinStr;
		Integer anioFin;
		
		if(this.anio != null){
			//anioInicioStr = anio.toString();
			anioInicioStr = CalculoSuscripcionesDTO.procesaAnio(anio);
			
			anioFin = anio + 1;
			
			//anioFinStr = anioFin.toString();
			anioFinStr = CalculoSuscripcionesDTO.procesaAnio(anioFin);
			
			anioInicioStr = anioInicioStr.substring(2);
			anioFinStr = anioFinStr.substring(2);
			this.descripcion = anioInicioStr + "-" + anioFinStr;
		}
		
		return this.descripcion;
	}

	public boolean isActual() {
		return actual;
	}

	public void setActual(boolean actual) {
		this.actual = actual;
	}


}
