package mx.com.afirme.midas.contratos.linea;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class SuscripcionDTO implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1519389401732747421L;

	private int idSuscripcion;
	
	private String descripcion;
	
	private Date fechaInicial;
	
	private Date fechaFinal;
	
	private int ejercicio;
	

	public int getIdSuscripcion() {
		return this.idSuscripcion;
	}

	public void setIdSuscripcion(int idSuscripcion) {
		this.idSuscripcion = idSuscripcion;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFechaInicial() {
		return this.fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public int getEjercicio() {
		return this.ejercicio;
	}

	public void setEjercicio(int ejercicio) {
		this.ejercicio = ejercicio;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public List<Date> obtenerMesesIncluidos(){
		List<Date> listaMeses = new ArrayList<Date>();
		
		if(fechaFinal != null && fechaInicial != null && ejercicio > 0){
			
			Calendar calendarInicial = Calendar.getInstance();
			Calendar calendarFinal = Calendar.getInstance();
			calendarInicial.setTime(fechaInicial);
			calendarFinal.setTime(fechaFinal);
			
			int diferenciaMeses = calendarFinal.get(Calendar.MONTH) - calendarInicial.get(Calendar.MONTH);
			
			int anio = ejercicio;
			
			if(ejercicio != calendarInicial.get(Calendar.YEAR))
				anio = calendarInicial.get(Calendar.YEAR);
			
			if(diferenciaMeses > 0){
				for(int i=0;i<=diferenciaMeses;i++){
					Calendar calendarTMP = Calendar.getInstance();
					calendarTMP.set(Calendar.DATE, 1);
					
					int mes = calendarInicial.get(Calendar.MONTH) + i;
					
					if(mes > 12){
						mes = mes - 12;
						anio = anio+1;
					}
					
					calendarTMP.set(Calendar.MONTH,mes);
					calendarTMP.set(Calendar.YEAR,anio);
					
					listaMeses.add(calendarTMP.getTime());
					
				}
			}
			
		}
		
		return listaMeses;
	}

}
