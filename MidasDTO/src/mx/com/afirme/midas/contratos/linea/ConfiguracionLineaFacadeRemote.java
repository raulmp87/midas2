package mx.com.afirme.midas.contratos.linea;
// default package

import java.util.List;
import java.math.BigDecimal;
import javax.ejb.Remote;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;

/**
 * Remote interface for ConfiguracionLineaFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ConfiguracionLineaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ConfiguracionLineaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ConfiguracionLineaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ConfiguracionLineaDTO entity);
    /**
	 Delete a persistent ConfiguracionLineaDTO entity.
	  @param entity ConfiguracionLineaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ConfiguracionLineaDTO entity);
   /**
	 Persist a previously saved ConfiguracionLineaDTO entity and return it or a copy of it to the sender. 
	 A copy of the ConfiguracionLineaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ConfiguracionLineaDTO entity to update
	 @return ConfiguracionLineaDTO the persisted ConfiguracionLineaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ConfiguracionLineaDTO update(ConfiguracionLineaDTO entity);
	public ConfiguracionLineaDTO findById( BigDecimal id);
	 /**
	 * Find all ConfiguracionLineaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ConfiguracionLineaDTO property to query
	  @param value the property value to match
	  	  @return List<ConfiguracionLineaDTO> found by query
	 */
	public List<ConfiguracionLineaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ConfiguracionLineaDTO entities.
	  	  @return List<ConfiguracionLineaDTO> all ConfiguracionLineaDTO entities
	 */
	public List<ConfiguracionLineaDTO> findAll(
		);	
	
	/**
	 * Find filtered ConfiguracionLineaDTO entities.
	  	  @return List<ConfiguracionLineaDTO> filtered ConfiguracionLineaDTO entities
	 */
	public List<ConfiguracionLineaDTO> listarFiltrado(ConfiguracionLineaDTO configuracionLineaDTO);
	
	/**
	 * Find filtered ConfiguracionLineaDTO entities.
	  	  @return List<ConfiguracionLineaDTO> filtered ConfiguracionLineaDTO entities
	 */
	public List<ConfiguracionLineaDTO> listarLineaFiltrado(ConfiguracionLineaDTO configuracionLineaDTO);
	
	/**
	 * Find filtered ConfiguracionLineaDTO entities.
	  	  @return List<ConfiguracionLineaDTO> filtered ConfiguracionLineaDTO entities
	 */
	public List<ConfiguracionLineaDTO> listarFiltradoVigencia(ConfiguracionLineaDTO configuracionLineaDTO);
	
	
	/**
	 * Find filtered ConfiguracionLineaDTO entities.
	  	  @return List<ConfiguracionLineaDTO> filtered ConfiguracionLineaDTO entities
	 */
	public List<ConfiguracionLineaDTO> listarFiltradoContrato(ConfiguracionLineaDTO configuracionLineaDTO);
	/**
	 * Get Configuration Line by Contracts witch belongs to a configuration line
	  	  @return List<ConfiguracionLineaDTO> filtered ConfiguracionLineaDTO entities
	 */
	public List<ConfiguracionLineaDTO> getCLineasPorContratosDeCLinea(ConfiguracionLineaDTO configuracionLineaDTO);

	/**
	 * Get ConfiguracionLineaDTO entities Line by id LineaDTO
	  	  @return List<ConfiguracionLineaDTO> filtered ConfiguracionLineaDTO entities
	 */
	public List<ConfiguracionLineaDTO> getConfiguracionLineaPorIDLineas(List<String> idLineas);
	
	/**
	 * Find all ConfiguracionDatoIncisoCotizacionDTO entities.
	 * 
	 * @return List<SubRamoDTO> all SubRamoDTO
	 *         entities not in ids
	 */
	public List<SubRamoDTO> getSubRamosRestantes(String ids, BigDecimal idTcRamo);
}