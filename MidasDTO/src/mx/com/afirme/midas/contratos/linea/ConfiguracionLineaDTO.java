package mx.com.afirme.midas.contratos.linea;
// default package

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



@Entity
@Table(name="VNCONFIGURACIONLINEA", schema="MIDAS")

public class ConfiguracionLineaDTO  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 6249722122000998354L;
	private BigDecimal idTmLinea;
     private Date fechaInicial;
     private Date fechaFinal;
     private String ramo;
     private String subRamo;
     private String modoDistribucion;
     private String contratoCuotaParte;
     private BigDecimal porcentajeDeCesion;
     private BigDecimal cesionCuotaParte;
     private Double maximo;
     private String contratoPrimerExcedente;
     private BigDecimal montoDelPleno;
     private BigDecimal numeroDePlenos;
     private BigDecimal cesionPrimerExcedente;
     private BigDecimal capacidadMaximaLinea;
     private BigDecimal totalRetencion;
     private BigDecimal totalCedido;
     private BigDecimal idTcRamo;
     private BigDecimal idTcSubRamo;
     private BigDecimal idTmContratoCuotaParte;
     private BigDecimal idTmContratoPrimerExcedente;
     private BigDecimal estatus;


    // Constructors

    /** default constructor */
    public ConfiguracionLineaDTO() {
    }

       
    // Property accessors
    @Id
    @Column(name="IDTMLINEA", nullable=false, precision=22, scale=0)
    public BigDecimal getIdTmLinea() {
        return this.idTmLinea;
    }
    
    public void setIdTmLinea(BigDecimal idTmLinea) {
        this.idTmLinea = idTmLinea;
    }
@Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHAINICIAL", nullable=false, length=7)

    public Date getFechaInicial() {
        return this.fechaInicial;
    }
    
    public void setFechaInicial(Date fechaInicial) {
        this.fechaInicial = fechaInicial;
    }
@Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHAFINAL", nullable=false, length=7)

    public Date getFechaFinal() {
        return this.fechaFinal;
    }
    
    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    @Column(name="RAMO", nullable=false, length=50)

    public String getRamo() {
        return this.ramo;
    }
    
    public void setRamo(String ramo) {
        this.ramo = ramo;
    }

    @Column(name="SUBRAMO", length=50)

    public String getSubRamo() {
        return this.subRamo;
    }
    
    public void setSubRamo(String subRamo) {
        this.subRamo = subRamo;
    }

    @Column(name="MODODISTRIBUCION", length=9)

    public String getModoDistribucion() {
        return this.modoDistribucion;
    }
    
    public void setModoDistribucion(String modoDistribucion) {
        this.modoDistribucion = modoDistribucion;
    }

    @Column(name="CONTRATOCUOTAPARTE", length=7)

    public String getContratoCuotaParte() {
        return this.contratoCuotaParte;
    }
    
    public void setContratoCuotaParte(String contratoCuotaParte) {
        this.contratoCuotaParte = contratoCuotaParte;
    }

    @Column(name="PORCENTAJEDECESION", precision=22, scale=0)

    public BigDecimal getPorcentajeDeCesion() {
        return this.porcentajeDeCesion;
    }
    
    public void setPorcentajeDeCesion(BigDecimal porcentajeDeCesion) {
        this.porcentajeDeCesion = porcentajeDeCesion;
    }

    @Column(name="CESIONCUOTAPARTE", precision=22, scale=0)

    public BigDecimal getCesionCuotaParte() {
        return this.cesionCuotaParte;
    }
    
    public void setCesionCuotaParte(BigDecimal cesionCuotaParte) {
        this.cesionCuotaParte = cesionCuotaParte;
    }

    @Column(name="MAXIMO", nullable=false, precision=20, scale=8)

    public Double getMaximo() {
        return this.maximo;
    }
    
    public void setMaximo(Double maximo) {
        this.maximo = maximo;
    }

    @Column(name="CONTRATOPRIMEREXCEDENTE", length=7)

    public String getContratoPrimerExcedente() {
        return this.contratoPrimerExcedente;
    }
    
    public void setContratoPrimerExcedente(String contratoPrimerExcedente) {
        this.contratoPrimerExcedente = contratoPrimerExcedente;
    }

    @Column(name="MONTODELPLENO", precision=22, scale=0)

    public BigDecimal getMontoDelPleno() {
        return this.montoDelPleno;
    }
    
    public void setMontoDelPleno(BigDecimal montoDelPleno) {
        this.montoDelPleno = montoDelPleno;
    }

    @Column(name="NUMERODEPLENOS", precision=22, scale=0)

    public BigDecimal getNumeroDePlenos() {
        return this.numeroDePlenos;
    }
    
    public void setNumeroDePlenos(BigDecimal numeroDePlenos) {
        this.numeroDePlenos = numeroDePlenos;
    }

    @Column(name="CESIONPRIMEREXCEDENTE", precision=22, scale=0)

    public BigDecimal getCesionPrimerExcedente() {
        return this.cesionPrimerExcedente;
    }
    
    public void setCesionPrimerExcedente(BigDecimal cesionPrimerExcedente) {
        this.cesionPrimerExcedente = cesionPrimerExcedente;
    }

    @Column(name="CAPACIDADMAXIMALINEA", precision=22, scale=0)

    public BigDecimal getCapacidadMaximaLinea() {
        return this.capacidadMaximaLinea;
    }
    
    public void setCapacidadMaximaLinea(BigDecimal capacidadMaximaLinea) {
        this.capacidadMaximaLinea = capacidadMaximaLinea;
    }

    @Column(name="TOTALRETENCION", precision=22, scale=0)

    public BigDecimal getTotalRetencion() {
        return this.totalRetencion;
    }
    
    public void setTotalRetencion(BigDecimal totalRetencion) {
        this.totalRetencion = totalRetencion;
    }

    @Column(name="TOTALCEDIDO", precision=22, scale=0)

    public BigDecimal getTotalCedido() {
        return this.totalCedido;
    }
    
    public void setTotalCedido(BigDecimal totalCedido) {
        this.totalCedido = totalCedido;
    }

    @Column(name="IDTCRAMO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdTcRamo() {
        return this.idTcRamo;
    }
    
    public void setIdTcRamo(BigDecimal idTcRamo) {
        this.idTcRamo = idTcRamo;
    }
    
    @Column(name="IDTCSUBRAMO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdTcSubRamo() {
        return this.idTcSubRamo;
    }
    
    public void setIdTcSubRamo(BigDecimal idTcSubRamo) {
        this.idTcSubRamo = idTcSubRamo;
    }

    @Column(name="IDTMCONTRATOCUOTAPARTE", nullable=false, precision=22, scale=0)

    public BigDecimal getIdTmContratoCuotaParte() {
        return this.idTmContratoCuotaParte;
    }
    
    public void setIdTmContratoCuotaParte(BigDecimal idTmContratoCuotaParte) {
        this.idTmContratoCuotaParte = idTmContratoCuotaParte;
    }

    @Column(name="IDTMCONTRATOPRIMEREXCEDENTE", nullable=false, precision=22, scale=0)

    public BigDecimal getIdTmContratoPrimerExcedente() {
        return this.idTmContratoPrimerExcedente;
    }
    
    public void setIdTmContratoPrimerExcedente(BigDecimal idTmContratoPrimerExcedente) {
        this.idTmContratoPrimerExcedente = idTmContratoPrimerExcedente;
    }
   
    @Column(name="ESTATUS", nullable=false, precision=22, scale=0)
    public BigDecimal getEstatus() {
        return this.estatus;
    }
    
    public void setEstatus(BigDecimal estatus) {
        this.estatus = estatus;
    }
}