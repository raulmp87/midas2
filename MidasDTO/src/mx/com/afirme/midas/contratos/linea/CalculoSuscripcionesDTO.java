package mx.com.afirme.midas.contratos.linea;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDecoradoDTO;

public class CalculoSuscripcionesDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int FORMA_PAGO_MENSUAL = 0;
	public static final int FORMA_PAGO_TRIMESTRAL = 1;
	public static final int FORMA_PAGO_SEMESTRAL = 2;
	public static final int FORMA_PAGO_ANUAL = 3;
	
	private Date fechaInicial;
	
	private Date fechaFinal;
	
	private BigDecimal formaPago;
	
	private int ejercicio;
	
	public CalculoSuscripcionesDTO(){
		
	}
	
	public CalculoSuscripcionesDTO(int ejercicio,Date fechaInicial,Date fechaFinal,BigDecimal formaPago){
		this.ejercicio = ejercicio;
		this.fechaInicial = fechaInicial;
		this.fechaFinal = fechaFinal;
		this.formaPago = formaPago;
	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public BigDecimal getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(BigDecimal formaPago) {
		this.formaPago = formaPago;
	}

	public int getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(int ejercicio) {
		this.ejercicio = ejercicio;
	}
	
	public List<SuscripcionDTO> getListaSuscripciones(){
		Calendar calendarFechaInicial = Calendar.getInstance();
		Calendar calendarFechaFinal = Calendar.getInstance();
		List<SuscripcionDTO> suscripcionDTOList = new ArrayList<SuscripcionDTO>();
		if (this.formaPago != null && this.fechaInicial != null && this.fechaFinal != null && ejercicio > 0){
			calendarFechaInicial.setTime(fechaInicial);
			int mesInicial = calendarFechaInicial.get(Calendar.MONTH);
			int anioInicial = calendarFechaInicial.get(Calendar.YEAR);
			calendarFechaFinal.setTime(fechaFinal);
			int mesFinal = calendarFechaFinal.get(Calendar.MONTH);
			int anioFinal = calendarFechaFinal.get(Calendar.YEAR);
			int diferenciaMeses = 0;
			int diferenciaAnios = anioFinal - anioInicial;
			EjercicioDTO ejercicioDTO = new EjercicioDTO();
			ejercicioDTO.setAnio(ejercicio);
			SuscripcionDTO suscripcionDTO;
			String descripcion;
			String nombreMeses[]={"ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE"};
			ArrayList<String> trimestre = new ArrayList<String>();
			trimestre.add("1er");
			trimestre.add("2do");
			trimestre.add("3er");
			trimestre.add("4to");
			
			if (!calendarFechaFinal.before(fechaInicial)){
				if (diferenciaAnios >= 0){
					if (diferenciaAnios > 0){
						diferenciaMeses = 12 - mesInicial; //rango cerrado
						diferenciaMeses = diferenciaMeses + mesFinal;
						diferenciaMeses = diferenciaMeses + ((diferenciaAnios-1) * 12);
					}else
						diferenciaMeses = mesFinal - mesInicial;
					
					if (diferenciaMeses >= 0){
						int numTrimestres = diferenciaMeses/3+5;
						for (int i=5; i < numTrimestres;i++){
							trimestre.add(i+"º");
						}
						ejercicioDTO = new EjercicioDTO();
						ejercicioDTO.setAnio(ejercicio);
						if (formaPago.intValue() == CalculoSuscripcionesDTO.FORMA_PAGO_MENSUAL){
							int mesEnTurno = mesInicial;
							int anioEnTurno = anioInicial;
							calendarFechaFinal.setTime(fechaInicial);
							for(int i=0; i < diferenciaMeses; i++,mesEnTurno++){
								if (mesEnTurno > 11){
									mesEnTurno = 0;
									anioEnTurno = anioEnTurno + 1;
								}
								suscripcionDTO = new SuscripcionDTO();
								suscripcionDTO.setEjercicio(ejercicio);
								suscripcionDTO.setFechaInicial(calendarFechaInicial.getTime());
								calendarFechaFinal.setTime(calendarFechaInicial.getTime());
								calendarFechaFinal.add(Calendar.MONTH, 1); // Se suma un mes
								calendarFechaFinal.add(Calendar.DAY_OF_MONTH, -1); // Se resta un día (Para obtener algo como "25/10/09 - 24/11/09" entre fecha inicial y fecha final
								suscripcionDTO.setFechaFinal(calendarFechaFinal.getTime());
								suscripcionDTO.setIdSuscripcion(i+1);
								descripcion = nombreMeses[mesEnTurno] + " " + anioEnTurno + " CTTO " + ejercicioDTO.getDescripcion();
								suscripcionDTO.setDescripcion(descripcion);
								suscripcionDTOList.add(suscripcionDTO);
								calendarFechaFinal.add(Calendar.DAY_OF_MONTH, 1);
								calendarFechaInicial.setTime(calendarFechaFinal.getTime());
							}
						}else{
							if (formaPago.intValue() == CalculoSuscripcionesDTO.FORMA_PAGO_TRIMESTRAL){
								int trimestreEnTurno = 0;
								int idSuscripcion = 0;
								calendarFechaFinal.setTime(fechaInicial);
								for(int i=0; i < diferenciaMeses; i+=3,trimestreEnTurno++,idSuscripcion++){
									suscripcionDTO = new SuscripcionDTO();
									suscripcionDTO.setEjercicio(ejercicio);
									suscripcionDTO.setFechaInicial(calendarFechaInicial.getTime());
									calendarFechaFinal.add(Calendar.MONTH, 3); // Se suman tres meses
									calendarFechaFinal.add(Calendar.DAY_OF_MONTH, -1); // Se resta un día (Para obtener algo como "25/08/09 - 24/11/09" entre fecha inicial y fecha final
									suscripcionDTO.setFechaFinal(calendarFechaFinal.getTime());
									suscripcionDTO.setIdSuscripcion(idSuscripcion+1);									
									descripcion = trimestre.get(trimestreEnTurno) + " TRIM CTTO " + ejercicioDTO.getDescripcion();
									suscripcionDTO.setDescripcion(descripcion);
									suscripcionDTOList.add(suscripcionDTO);
									calendarFechaFinal.add(Calendar.DAY_OF_MONTH, 1);
									calendarFechaInicial.setTime(calendarFechaFinal.getTime());
								}
							}else{
								if (formaPago.intValue() == CalculoSuscripcionesDTO.FORMA_PAGO_SEMESTRAL){
									int semestreEnTurno = 0;
									int idSuscripcion = 0;
									calendarFechaFinal.setTime(fechaInicial);
									for(int i=0; i < diferenciaMeses; i+=6,semestreEnTurno++,idSuscripcion++){
										suscripcionDTO = new SuscripcionDTO();
										suscripcionDTO.setEjercicio(ejercicio);
										suscripcionDTO.setFechaInicial(calendarFechaInicial.getTime());
										calendarFechaFinal.add(Calendar.MONTH, 6); // Se suman seis meses
										calendarFechaFinal.add(Calendar.DAY_OF_MONTH, -1); // Se resta un día (Para obtener algo como "25/05/09 - 24/11/09" entre fecha inicial y fecha final
										suscripcionDTO.setFechaFinal(calendarFechaFinal.getTime());
										suscripcionDTO.setIdSuscripcion(idSuscripcion+1);					
										descripcion = trimestre.get(semestreEnTurno) + " SEM CTTO " + ejercicioDTO.getDescripcion();
										suscripcionDTO.setDescripcion(descripcion);
										suscripcionDTOList.add(suscripcionDTO);
										calendarFechaFinal.add(Calendar.DAY_OF_MONTH, 1);
										calendarFechaInicial.setTime(calendarFechaFinal.getTime());
									}
								}else{
									if (formaPago.intValue() == CalculoSuscripcionesDTO.FORMA_PAGO_ANUAL){
										int idSuscripcion = 0;
										calendarFechaFinal.setTime(fechaInicial);
										suscripcionDTO = new SuscripcionDTO();
										suscripcionDTO.setEjercicio(ejercicio);
										suscripcionDTO.setFechaInicial(calendarFechaInicial.getTime());
										calendarFechaFinal.add(Calendar.MONTH, 12); // Se suman doce meses
										calendarFechaFinal.add(Calendar.DAY_OF_MONTH, -1); // Se resta un día (Para obtener algo como "25/05/09 - 24/05/10" entre fecha inicial y fecha final
										suscripcionDTO.setFechaFinal(calendarFechaFinal.getTime());
										suscripcionDTO.setIdSuscripcion(idSuscripcion+1);									
										descripcion = "CTTO " + ejercicioDTO.getDescripcion();
										suscripcionDTO.setDescripcion(descripcion);
										suscripcionDTOList.add(suscripcionDTO);
									}
								}
							}
						}
					}
				}
			}
		}
		
		return suscripcionDTOList;
	}
	
	/**
	 * 
	 * 
	 * @param fecha
	 * @return
	 */
	public SuscripcionDTO obtenerPeriodo(Date fecha){
		SuscripcionDTO  suscripcion = null;
		List<SuscripcionDTO> lista = getListaSuscripciones();
		
 		if (lista != null && lista.size() > 0){
	        for(SuscripcionDTO suscripcionDTO : lista){
	        	if ((fecha.compareTo(suscripcionDTO.getFechaInicial())  >= 0) && (fecha.compareTo(suscripcionDTO.getFechaFinal()) <= 0)){
	        		suscripcion =  suscripcionDTO; 
	        		break;
	        	}
	         }
 	 	}		
		
		return suscripcion;
	}
	
	/**
	 * 
	 * @param fechaInicial
	 * @param fechaFinal
	 * @param fechaEnCurso
	 * @param formaPago
	 * @return
	 */
	public static SuscripcionDTO obtenerSuscripcion(
								Date fechaInicial, 
								Date fechaFinal, 
								Date fechaEnCurso,
								BigDecimal formaPago){
		CalculoSuscripcionesDTO calculo = new CalculoSuscripcionesDTO();
		calculo.setFechaInicial(fechaInicial);
		calculo.setFechaFinal(fechaFinal);
		calculo.setFormaPago(formaPago);
		calculo.setEjercicio(CalculoEjerciciosDTO.obtenerEjercicio(fechaInicial));
		
		SuscripcionDTO suscripcion = calculo.obtenerPeriodo(fechaEnCurso);
		
		return suscripcion;
	}
	
	/**
	 * 
	 * @param fechaInicial
	 * @param fechaFinal
	 * @param formaPago
	 * @return
	 */
	public static List<SuscripcionDTO> obtenerSuscripciones(
								Date fechaInicial, 
								Date fechaFinal, 
								BigDecimal formaPago){
		CalculoSuscripcionesDTO calculo = new CalculoSuscripcionesDTO();
		calculo.setFechaInicial(fechaInicial);
		calculo.setFechaFinal(fechaFinal);
		calculo.setFormaPago(formaPago);
		calculo.setEjercicio(CalculoEjerciciosDTO.obtenerEjercicio(fechaInicial));
		
		List<SuscripcionDTO>  suscripciones = calculo.getListaSuscripciones();
		
		return suscripciones;
	}
	
	public static String procesaAnio(Integer anio){
		String anioProc = anio.toString();
		
		if(anio != null){
			if(anioProc.trim().length() < 4){
				switch(anioProc.length()){
				case 1:
					anioProc = "200" + anioProc;
					break;
				case 2:
					anioProc = "20" + anioProc;
					break;
				case 3:
					anioProc = "2" + anioProc;
				}
			} 
		}
		
		
		return anioProc;
		
	}
	
	public SuscripcionDTO getSuscripcion(EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO, Integer suscripcion){
		SuscripcionDTO suscripcionDTO;
		int diferenciaMesesSuscripcion = 0;
		this.ejercicio = estadoCuentaDecoradoDTO.getEjercicio();
		this.fechaInicial = estadoCuentaDecoradoDTO.getFechaInicial();
		
		//Si el estado de cuenta tiene forma de pago = null, quiere decir que se trata de un contrato facultativo
		//en el cual se definieron exhibiciones a nivel cobertura. Por defecto, se coloca forma de pago = ANUAL
		this.formaPago = estadoCuentaDecoradoDTO.getFormaPago() != null ? estadoCuentaDecoradoDTO.getFormaPago() : 
																		  new BigDecimal(CalculoSuscripcionesDTO.FORMA_PAGO_ANUAL);
		if (suscripcion == null){
			suscripcion = estadoCuentaDecoradoDTO.getSuscripcion();
		}
		
		if(this.formaPago.toBigInteger().intValue() == FORMA_PAGO_MENSUAL){
			diferenciaMesesSuscripcion = suscripcion;
		}else{
			if(this.formaPago.toBigInteger().intValue() == FORMA_PAGO_TRIMESTRAL){
				diferenciaMesesSuscripcion = suscripcion * 3;
			}else{
				if(this.formaPago.toBigInteger().intValue() == FORMA_PAGO_SEMESTRAL){
					diferenciaMesesSuscripcion = suscripcion * 6;
				}else{
					if(this.formaPago.toBigInteger().intValue() == FORMA_PAGO_ANUAL){
						diferenciaMesesSuscripcion = suscripcion * 12;
					}
				}
			}
		}
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(this.getFechaInicial());
		calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + diferenciaMesesSuscripcion);
		
		this.fechaFinal = calendar.getTime();
		
		List<SuscripcionDTO> suscripcionDTOList = this.getListaSuscripciones();
		if (suscripcionDTOList.size() <= suscripcion.intValue()){
			suscripcion = suscripcion - 1; // Ajuste al índice de la lista, debido a que éste empieza en 0. Es decir, para la suscripción 1 se accede al elemento 0 de la lista.
			suscripcionDTO = suscripcionDTOList.get(suscripcion);
		}else{
			suscripcionDTO = null;
		}
		return suscripcionDTO;
	}

}
