package mx.com.afirme.midas.contratos.linea;
// default package

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDTO;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteDTO;
import mx.com.afirme.midas.contratos.lineaparticipacion.LineaParticipacionDTO;


/**
 * LineaDTO entity. @author MyEclipse Persistence Tools
 */

@Entity
@Table(name="TMLINEA", schema="MIDAS")

public class LineaDTO  implements java.io.Serializable {
	
	public static final int FORMA_PAGO_MENSUAL = 0;
	public static final int FORMA_PAGO_TRIMESTRAL = 1;
	public static final int FORMA_PAGO_SEMESTRAL = 2;
	public static final int FORMA_PAGO_ANUAL = 3;
	
	public static final int FORMA_PAGO_RETENCION_PURA = FORMA_PAGO_SEMESTRAL ;


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = -5832808917129337062L;
	private BigDecimal idTmLinea;
     private SubRamoDTO subRamo;
     private ContratoCuotaParteDTO contratoCuotaParte;
     private ContratoPrimerExcedenteDTO contratoPrimerExcedente;
     private Date fechaInicial;
     private Date fechaFinal;
     private BigDecimal tipoDistribucion;
     private Double maximo;
     private BigDecimal estatus;
     private BigDecimal usuarioAutorizo;
     private Date fechaAutorizacion;
     private int estadosCuentaCreados;     
     private List<LineaParticipacionDTO> lineaParticipaciones;


    // Constructors

    /** default constructor */
    public LineaDTO() {
    	if(lineaParticipaciones==null)
    		lineaParticipaciones = new ArrayList<LineaParticipacionDTO>();
    	
    	if(contratoCuotaParte == null)
    		contratoCuotaParte = new ContratoCuotaParteDTO();
    	
    	if(contratoPrimerExcedente == null)
    		contratoPrimerExcedente = new ContratoPrimerExcedenteDTO();
    	
    	if(subRamo == null)
    		subRamo = new SubRamoDTO();
    }
   
    // Property accessors
    @Id 
    @SequenceGenerator(name = "IDTMLINEA_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTMLINEA_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTMLINEA_SEQ_GENERADOR")	  	
    @Column(name="IDTMLINEA", unique=true, nullable=false, precision=22, scale=0)

    public BigDecimal getIdTmLinea() {
        return this.idTmLinea;
    }
    
    public void setIdTmLinea(BigDecimal idTmLinea) {
        this.idTmLinea = idTmLinea;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTCSUBRAMO", nullable=false, insertable=false)   
    public SubRamoDTO getSubRamo() {
		return subRamo;
	}
    public void setSubRamo(SubRamoDTO subRamo) {
		this.subRamo = subRamo;
	}
        
	@ManyToOne(cascade=CascadeType.REFRESH,fetch=FetchType.EAGER)
        @JoinColumn(name="IDTMCONTRATOCUOTAPARTE")
    public ContratoCuotaParteDTO getContratoCuotaParte() {
        return this.contratoCuotaParte;
    }
    
	public void setContratoCuotaParte(ContratoCuotaParteDTO contratoCuotaParteDTO) {
        this.contratoCuotaParte = contratoCuotaParteDTO;
    }

	@ManyToOne(cascade=CascadeType.REFRESH,fetch=FetchType.EAGER)
        @JoinColumn(name="IDTMCONTRATOPRIMEREXCEDENTE")

    public ContratoPrimerExcedenteDTO getContratoPrimerExcedente() {
        return this.contratoPrimerExcedente;
    }
    
    public void setContratoPrimerExcedente(ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO) {
        this.contratoPrimerExcedente = contratoPrimerExcedenteDTO;
    }
@Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHAINICIAL", nullable=false, length=7)

    public Date getFechaInicial() {
        return this.fechaInicial;
    }
    
    public void setFechaInicial(Date fechaInicial) {
        this.fechaInicial = fechaInicial;
    }
@Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHAFINAL", nullable=false, length=7)

    public Date getFechaFinal() {
        return this.fechaFinal;
    }
    
    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }
    
    @Column(name="TIPODISTRIBUCION", nullable=false, precision=22, scale=0)

    public BigDecimal getTipoDistribucion() {
        return this.tipoDistribucion;
    }
    
    public void setTipoDistribucion(BigDecimal tipoDistribucion) {
        this.tipoDistribucion = tipoDistribucion;
    }
    
    @Column(name="MAXIMO", nullable=false, precision=20, scale=8)

    public Double getMaximo() {
        return this.maximo;
    }
    
    public void setMaximo(Double maximo) {
        this.maximo = maximo;
    }
    
    @Column(name="ESTATUS", nullable=false, precision=22, scale=0)

    public BigDecimal getEstatus() {
        return this.estatus;
    }
    
    public void setEstatus(BigDecimal estatus) {
        this.estatus = estatus;
    }
    
    @Column(name="USUARIOAUTORIZO", precision=22, scale=0)

    public BigDecimal getUsuarioAutorizo() {
        return this.usuarioAutorizo;
    }
    
    public void setUsuarioAutorizo(BigDecimal usuarioAutorizo) {
        this.usuarioAutorizo = usuarioAutorizo;
    }
@Temporal(TemporalType.DATE)
    @Column(name="FECHAAUTORIZACION", length=7)

    public Date getFechaAutorizacion() {
        return this.fechaAutorizacion;
    }
    
    public void setFechaAutorizacion(Date fechaAutorizacion) {
        this.fechaAutorizacion = fechaAutorizacion;
    }
   
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="linea")

    public List<LineaParticipacionDTO> getLineaParticipaciones() {
        return this.lineaParticipaciones;
    }
    
    public void setLineaParticipaciones(List<LineaParticipacionDTO> lineaParticipaciones) {
        this.lineaParticipaciones = lineaParticipaciones;
    }

	@Column(name = "ESTADOSCUENTACREADOS", precision = 1, scale = 0)
	public int getEstadosCuentaCreados() {
		return this.estadosCuentaCreados;
	}

	public void setEstadosCuentaCreados(int estadosCuentaCreados) {
		this.estadosCuentaCreados = estadosCuentaCreados;
	} 

	@Override
	public boolean equals(Object obj) {
		if(obj == null) return false;
		if(!(obj instanceof LineaDTO))	return false;
		LineaDTO castObj = (LineaDTO) obj;
		if(castObj.getIdTmLinea() == null) return false;
		if(this.idTmLinea == null) return false;
        return (castObj.getIdTmLinea().equals(this.idTmLinea));
	}
	
	@Override
	public int hashCode() {
        int result = 17;
        result = 37 * result + ( getIdTmLinea() == null ? 0 : this.getIdTmLinea().hashCode() );
        return result;
	}
}