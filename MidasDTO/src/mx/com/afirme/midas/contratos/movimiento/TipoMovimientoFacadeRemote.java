package mx.com.afirme.midas.contratos.movimiento;
// default package

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for TipoMovimientoDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface TipoMovimientoFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved TipoMovimientoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity TipoMovimientoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(TipoMovimientoDTO entity);
    /**
	 Delete a persistent TipoMovimientoDTO entity.
	  @param entity TipoMovimientoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(TipoMovimientoDTO entity);
   /**
	 Persist a previously saved TipoMovimientoDTO entity and return it or a copy of it to the sender. 
	 A copy of the TipoMovimientoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity TipoMovimientoDTO entity to update
	 @return TipoMovimientoDTO the persisted TipoMovimientoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public TipoMovimientoDTO update(TipoMovimientoDTO entity);
	public TipoMovimientoDTO findById( int id);
	 /**
	 * Find all TipoMovimientoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the TipoMovimientoDTO property to query
	  @param value the property value to match
	  	  @return List<TipoMovimientoDTO> found by query
	 */
	public List<TipoMovimientoDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all TipoMovimientoDTO entities.
	  	  @return List<TipoMovimientoDTO> all TipoMovimientoDTO entities
	 */
	public List<TipoMovimientoDTO> findAll(
		);	
}