package mx.com.afirme.midas.contratos.movimiento;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import mx.com.afirme.midas.contratos.estadocuenta.AcumuladorDTO;
import mx.com.afirme.midas.contratos.estadocuenta.ConceptoAcumuladorDTO;
import mx.com.afirme.midas.contratos.estadocuenta.presentacion.DetalleConfiguracionEstadoCuentaDTO;


/**
 * ConceptoMovimientoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCCONCEPTOMOVIMIENTO",schema="MIDAS")
public class ConceptoMovimientoDTO  implements java.io.Serializable {
	
	public static final int SINIESTROS = 7;
	public static final int GASTOS_AJUSTE = 8;
	public static final int SALVAMENTO = 9;
	public static final int SALDO_ANTERIOR = 1;
	public static final int SALDO_TECNICO = 12;
	public static final int TIPO_NATURALEZA_HABER = 1;
	public static final int TIPO_NATURALEZA_DEBE = 2;
	public static final int TIPO_NATURALEZA_DEBE_HABER = 3;

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields    

     private int idConceptoMovimiento;
     private String descripcion;
     private int naturaleza;     
     private Set<AcumuladorDTO> acumuladorDTOs = new HashSet<AcumuladorDTO>(0);
     private Set<ConceptoAcumuladorDTO> conceptoAcumuladorDTOs = new HashSet<ConceptoAcumuladorDTO>(0);
     private Set<ConceptoMovimientoDetalleDTO> conceptoMovimientoDetalleDTOs = new HashSet<ConceptoMovimientoDetalleDTO>(0);
     private List<DetalleConfiguracionEstadoCuentaDTO> detalleConfiguracionEstadoCuentaList;

     //private Set<MovimientoReaseguroDTO> movimientoReaseguroDTOs = new HashSet<MovimientoReaseguroDTO>(0);


    // Constructors

    /** default constructor */
    public ConceptoMovimientoDTO() {
    }
   
    // Property accessors
    @Id 
    
    @Column(name="IDTCCONCEPTOMOVIMIENTO", unique=true, nullable=false, precision=2, scale=0)

    public int getIdConceptoMovimiento() {
        return this.idConceptoMovimiento;
    }
    
    public void setIdConceptoMovimiento(int idConceptoMovimiento) {
        this.idConceptoMovimiento = idConceptoMovimiento;
    }
    
    @Column(name="DESCRIPCION", length=50)

    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="conceptoMovimientoDTO")

    public Set<AcumuladorDTO> getAcumuladorDTOs() {
        return this.acumuladorDTOs;
    }
    
    public void setAcumuladorDTOs(Set<AcumuladorDTO> acumuladorDTOs) {
        this.acumuladorDTOs = acumuladorDTOs;
    }
    
    /*
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="conceptoMovimientoDTO")

    public Set<MovimientoReaseguroDTO> getMovimientoReaseguroDTOs() {
        return this.movimientoReaseguroDTOs;
    }
    
    public void setMovimientoReaseguroDTOs(Set<MovimientoReaseguroDTO> movimientoReaseguroDTOs) {
        this.movimientoReaseguroDTOs = movimientoReaseguroDTOs;
    }
   */


    
    @Column(name="NATURALEZA", nullable=false, precision=1, scale=0)
    public int getNaturaleza() {
        return this.naturaleza;
    }
    
    public void setNaturaleza(int naturaleza) {
        this.naturaleza = naturaleza;
    }    

    
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="conceptoMovimientoDTO")

    public Set<ConceptoAcumuladorDTO> getConceptoAcumuladorDTOs() {
        return this.conceptoAcumuladorDTOs;
    }
    
    public void setConceptoAcumuladorDTOs(Set<ConceptoAcumuladorDTO> conceptoAcumuladorDTOs) {
        this.conceptoAcumuladorDTOs = conceptoAcumuladorDTOs;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="conceptoMovimientoDTO")

    public Set<ConceptoMovimientoDetalleDTO> getConceptoMovimientoDetalleDTOs() {
        return this.conceptoMovimientoDetalleDTOs;
    }
    
    public void setConceptoMovimientoDetalleDTOs(Set<ConceptoMovimientoDetalleDTO> conceptoMovimientoDetalleDTOs) {
        this.conceptoMovimientoDetalleDTOs = conceptoMovimientoDetalleDTOs;
    }

    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="conceptoMovimientoDTO")
	public List<DetalleConfiguracionEstadoCuentaDTO> getDetalleConfiguracionEstadoCuentaList() {
		return detalleConfiguracionEstadoCuentaList;
	}

	public void setDetalleConfiguracionEstadoCuentaList(
			List<DetalleConfiguracionEstadoCuentaDTO> detalleConfiguracionEstadoCuentaList) {
		this.detalleConfiguracionEstadoCuentaList = detalleConfiguracionEstadoCuentaList;
	}
}