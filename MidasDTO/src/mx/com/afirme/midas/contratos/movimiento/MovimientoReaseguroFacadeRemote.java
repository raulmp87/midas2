package mx.com.afirme.midas.contratos.movimiento;
// default package

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for MovimientoReaseguroDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface MovimientoReaseguroFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved MovimientoReaseguroDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity MovimientoReaseguroDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public MovimientoReaseguroDTO save(MovimientoReaseguroDTO entity);
    /**
	 Delete a persistent MovimientoReaseguroDTO entity.
	  @param entity MovimientoReaseguroDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(MovimientoReaseguroDTO entity);
   /**
	 Persist a previously saved MovimientoReaseguroDTO entity and return it or a copy of it to the sender. 
	 A copy of the MovimientoReaseguroDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity MovimientoReaseguroDTO entity to update
	 @return MovimientoReaseguroDTO the persisted MovimientoReaseguroDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public MovimientoReaseguroDTO update(MovimientoReaseguroDTO entity);
	public MovimientoReaseguroDTO findById( BigDecimal id);
	 /**
	 * Find all MovimientoReaseguroDTO entities with a specific property value.  
	 
	  @param propertyName the name of the MovimientoReaseguroDTO property to query
	  @param value the property value to match
	  	  @return List<MovimientoReaseguroDTO> found by query
	 */
	public List<MovimientoReaseguroDTO> findByProperty(String propertyName, Object value);
	/**
	 * Find all MovimientoReaseguroDTO entities.
	  	  @return List<MovimientoReaseguroDTO> all MovimientoReaseguroDTO entities
	 */
	public List<MovimientoReaseguroDTO> findAll();
	
	public void notificarMovimientosContabilidadReaseguro(BigDecimal idToPoliza,int numeroEndoso,Date fechaContabilizado,String comodin);
	
	public BigDecimal obtenerCantidadMovimientosReaseguro(BigDecimal idToPoliza,int numeroEndoso,String propiedadTabla,String valor);
	
}