package mx.com.afirme.midas.contratos.movimiento;
// default package

import java.util.List;
import javax.ejb.Remote;


/**
 * Remote interface for ConceptoMovimientoDetalleFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ConceptoMovimientoDetalleFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ConceptoMovimientoDetalleDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ConceptoMovimientoDetalleDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ConceptoMovimientoDetalleDTO entity);
    /**
	 Delete a persistent ConceptoMovimientoDetalleDTO entity.
	  @param entity ConceptoMovimientoDetalleDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ConceptoMovimientoDetalleDTO entity);
   /**
	 Persist a previously saved ConceptoMovimientoDetalleDTO entity and return it or a copy of it to the sender. 
	 A copy of the ConceptoMovimientoDetalleDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ConceptoMovimientoDetalleDTO entity to update
	 @return ConceptoMovimientoDetalleDTO the persisted ConceptoMovimientoDetalleDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ConceptoMovimientoDetalleDTO update(ConceptoMovimientoDetalleDTO entity);
	public ConceptoMovimientoDetalleDTO findById( int id);
	 /**
	 * Find all ConceptoMovimientoDetalleDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ConceptoMovimientoDetalleDTO property to query
	  @param value the property value to match
	  	  @return List<ConceptoMovimientoDetalleDTO> found by query
	 */
	public List<ConceptoMovimientoDetalleDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ConceptoMovimientoDetalleDTO entities.
	  	  @return List<ConceptoMovimientoDetalleDTO> all ConceptoMovimientoDetalleDTO entities
	 */
	public List<ConceptoMovimientoDetalleDTO> findAll(
		);	
}