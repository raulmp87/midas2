package mx.com.afirme.midas.contratos.movimiento;
// default package

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * ConceptoMovimientoDetalleDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCCONCEPTOMOVIMIENTODETALLE",schema="MIDAS")
public class ConceptoMovimientoDetalleDTO  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	//Constants
	public static final int PRIMA_CEDIDA = 1;
	public static final int COMISION_PRIMA_CEDIDA = 3;
	public static final int PRIMA_NO_DEVENGADA = 2;
	public static final int COMISION_PRIMA_NO_DEVENGADA = 4;
	public static final int CARGO_DE_REMESAS_SALDOS = 17;
	public static final int PRIMA_ADICIONAL = 40;
	public static final int CANCELACION_PRIMA_ADICIONAL = 41;
	public static final int INGRESO_PRIMA_NO_DEVENGADA = 32;
	public static final int PRIMA_NO_DEVENGADA_INGRESO = 39;
	public static final int CANCELACION_INGRESO_PRIMA_NO_DEVENGADA = 33;
	public static final int INGRESO_BONO_POR_NO_SINIESTRO = 42;
	public static final int CANCELACION_INGRESO_BONO_POR_NO_SINIESTRO = 43;
	public static final int CARGOS_REMESAS_SALDOS = 17;
	public static final int PAGOS_PRIMA = 34;
	public static final int CANCELAR_PAGO_PRIMA = 35;
	public static final int RETENCION_IMPUESTOS = 44;
	public static final int CANCELACION_RETENCION_IMPUESTOS = 45;
	public static final int INDEMNIZACION = 5;
	public static final int CANCELACION_INDEMNIZACION = 6;
	public static final int GASTOS_DE_AJUSTE = 9;
	public static final int CANCELACION_GASTOS_DE_AJUSTE = 24;
	public static final int DEDUCIBLE = 7;
	public static final int CANCELACION_DEDUCIBLE = 8;
	public static final int COASEGURO = 25;
	public static final int CANCELACION_COASEGURO = 26;
	public static final int SALVAMENTO = 10;
	public static final int CANCELACION_SALVAMENTO = 11;
	public static final int RECUPERACION = 12;
	public static final int CANCELACION_RECUPERACION = 13;
	public static final int INGRESO_POR_SINIESTRO = 30;
	public static final int ABONOS_DE_REMESAS_SALDOS = 18;
	public static final int CANCELACION_INGRESO_POR_SINIESTRO = 31;
	
	// Fields    
     private Integer idConceptoDetalle;
     private ConceptoMovimientoDTO conceptoMovimientoDTO;
     private String descripcion;
     private Set<MovimientoReaseguroDTO> movimientoReaseguroDTOs = new HashSet<MovimientoReaseguroDTO>(0);
     private boolean banderaAcumulaEdoCta;
     private boolean banderaAplicaTerminacionSiniestros;


    // Constructors

    /** default constructor */
    public ConceptoMovimientoDetalleDTO() {
    }

   
    // Property accessors
    @Id 
    @Column(name="IDTCCONCEPTODETALLE", unique=true, nullable=false, precision=2, scale=0)
    public Integer getIdConceptoDetalle() {
        return this.idConceptoDetalle;
    }
    
    public void setIdConceptoDetalle(Integer idConceptoDetalle) {
        this.idConceptoDetalle = idConceptoDetalle;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTCCONCEPTOMOVIMIENTO", nullable=false)
    public ConceptoMovimientoDTO getConceptoMovimientoDTO() {
        return this.conceptoMovimientoDTO;
    }
    
    public void setConceptoMovimientoDTO(ConceptoMovimientoDTO conceptoMovimientoDTO) {
        this.conceptoMovimientoDTO = conceptoMovimientoDTO;
    }
    
    @Column(name="DESCRIPCION", length=50)
    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="conceptoMovimientoDetalleDTO")
    public Set<MovimientoReaseguroDTO> getMovimientoReaseguroDTOs() {
        return this.movimientoReaseguroDTOs;
    }
    
    public void setMovimientoReaseguroDTOs(Set<MovimientoReaseguroDTO> movimientoReaseguroDTOs) {
        this.movimientoReaseguroDTOs = movimientoReaseguroDTOs;
    }

    @Column(name="ACUMULAEDOCTA", precision=1)
	public boolean getBanderaAcumulaEdoCta() {
		return banderaAcumulaEdoCta;
	}

	public void setBanderaAcumulaEdoCta(boolean banderaAcumulaEdoCta) {
		this.banderaAcumulaEdoCta = banderaAcumulaEdoCta;
	}

	@Column(name="APLICATERMINACIONSINIESTROS", precision=1)
	public boolean getBanderaAplicaTerminacionSiniestros() {
		return banderaAplicaTerminacionSiniestros;
	}

	public void setBanderaAplicaTerminacionSiniestros(boolean banderaAplicaTerminacionSiniestros) {
		this.banderaAplicaTerminacionSiniestros = banderaAplicaTerminacionSiniestros;
	}
   
}