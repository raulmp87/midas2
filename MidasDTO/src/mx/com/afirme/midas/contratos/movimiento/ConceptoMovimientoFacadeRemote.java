package mx.com.afirme.midas.contratos.movimiento;
// default package

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for ConceptoMovimientoDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ConceptoMovimientoFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ConceptoMovimientoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ConceptoMovimientoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ConceptoMovimientoDTO entity);
    /**
	 Delete a persistent ConceptoMovimientoDTO entity.
	  @param entity ConceptoMovimientoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ConceptoMovimientoDTO entity);
   /**
	 Persist a previously saved ConceptoMovimientoDTO entity and return it or a copy of it to the sender. 
	 A copy of the ConceptoMovimientoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ConceptoMovimientoDTO entity to update
	 @return ConceptoMovimientoDTO the persisted ConceptoMovimientoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ConceptoMovimientoDTO update(ConceptoMovimientoDTO entity);
	public ConceptoMovimientoDTO findById( int id);
	 /**
	 * Find all ConceptoMovimientoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ConceptoMovimientoDTO property to query
	  @param value the property value to match
	  	  @return List<ConceptoMovimientoDTO> found by query
	 */
	public List<ConceptoMovimientoDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ConceptoMovimientoDTO entities.
	  	  @return List<ConceptoMovimientoDTO> all ConceptoMovimientoDTO entities
	 */
	public List<ConceptoMovimientoDTO> findAll(
		);	
}