package mx.com.afirme.midas.contratos.movimiento;

import java.util.List;

import mx.com.afirme.midas.reaseguro.soporte.SoporteReaseguroDTO;

public class MovimientoDistribucionDTO {
	
	public SoporteReaseguroDTO getSoporteReaseguro() {
		return soporteReaseguro;
	}

	public void setSoporteReaseguro(SoporteReaseguroDTO soporteReaseguro) {
		this.soporteReaseguro = soporteReaseguro;
	}

	public List<MovimientoReaseguroDTO> getListaMovimientos() {
		return listaMovimientos;
	}

	public void setListaMovimientos(List<MovimientoReaseguroDTO> listaMovimientos) {
		this.listaMovimientos = listaMovimientos;
	}

	private SoporteReaseguroDTO soporteReaseguro;
	
	private List<MovimientoReaseguroDTO> listaMovimientos;
	

}
