package mx.com.afirme.midas.contratos.movimiento;
// default package

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * TipoMovimientoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCTIPOMOVIMIENTO"
    ,schema="MIDAS"
)

public class TipoMovimientoDTO  implements java.io.Serializable {
	
	public static final int CUENTAS_POR_PAGAR = 1;
	public static final int CUENTAS_POR_COBRAR = 2;
	public static final int PAGO = 3;
	public static final int COBRO = 4;
	
	
	public static final int NATURALEZA_ABONO = 1;
	public static final int NATURALEZA_CARGO = 2;
	

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields    

     private int idTipoMovimiento;
     private String descripcion;
     private int naturaleza;     
     //private Set<MovimientoReaseguroDTO> movimientoReaseguroDTOs = new HashSet<MovimientoReaseguroDTO>(0);


    // Constructors

    /** default constructor */
    public TipoMovimientoDTO() {
    }

  
    // Property accessors
    @Id 
    
    @Column(name="IDTCTIPOMOVIMIENTO", unique=true, nullable=false, precision=2, scale=0)

    public int getIdTipoMovimiento() {
        return this.idTipoMovimiento;
    }
    
    public void setIdTipoMovimiento(int idTipoMovimiento) {
        this.idTipoMovimiento = idTipoMovimiento;
    }
    
    @Column(name="DESCRIPCION", length=20)

    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    /*
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="tipoMovimientoDTO")

    public Set<MovimientoReaseguroDTO> getMovimientoReaseguroDTOs() {
        return this.movimientoReaseguroDTOs;
    }
    
    public void setMovimientoReaseguroDTOs(Set<MovimientoReaseguroDTO> movimientoReaseguroDTOs) {
        this.movimientoReaseguroDTOs = movimientoReaseguroDTOs;
    }*/
   

    @Column(name="NATURALEZA", nullable=false, precision=1, scale=0)
    public int getNaturaleza() {
        return this.naturaleza;
    }
    
    public void setNaturaleza(int naturaleza) {
        this.naturaleza = naturaleza;
    }

}