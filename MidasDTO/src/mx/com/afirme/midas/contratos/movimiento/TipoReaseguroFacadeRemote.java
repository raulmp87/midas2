package mx.com.afirme.midas.contratos.movimiento;
// default package

import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for TipoReaseguroDTOFacade.
 * @author MyEclipse Persistence Tools
 */

public interface TipoReaseguroFacadeRemote extends MidasInterfaceBase<TipoReaseguroDTO> {
		/**
	 Perform an initial save of a previously unsaved TipoReaseguroDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity TipoReaseguroDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(TipoReaseguroDTO entity);
    /**
	 Delete a persistent TipoReaseguroDTO entity.
	  @param entity TipoReaseguroDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(TipoReaseguroDTO entity);
   /**
	 Persist a previously saved TipoReaseguroDTO entity and return it or a copy of it to the sender. 
	 A copy of the TipoReaseguroDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity TipoReaseguroDTO entity to update
	 @return TipoReaseguroDTO the persisted TipoReaseguroDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public TipoReaseguroDTO update(TipoReaseguroDTO entity);

	 /**
	 * Find all TipoReaseguroDTO entities with a specific property value.  
	 
	  @param propertyName the name of the TipoReaseguroDTO property to query
	  @param value the property value to match
	  	  @return List<TipoReaseguroDTO> found by query
	 */
	public List<TipoReaseguroDTO> findByProperty(String propertyName, Object value
		);
}