package mx.com.afirme.midas.contratos.movimiento;
// default package

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;


/**
 * TipoReaseguroDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCTIPOREASEGURO"
    ,schema="MIDAS"
)

public class TipoReaseguroDTO extends CacheableDTO {
	
	public static final int TIPO_RETENCION = 4;
	public static final int TIPO_CONTRATO_CUOTA_PARTE = 1;
	public static final int TIPO_CONTRATO_PRIMER_EXCEDENTE = 2;
	public static final int TIPO_CONTRATO_FACULTATIVO = 3;
	
	
	public static final int ID_RETENCION = -1;


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields    

     private int tipoReaseguro;
     private String descripcion;
     //private Set<MovimientoReaseguroDTO> movimientoReaseguroDTOs = new HashSet<MovimientoReaseguroDTO>(0);


    // Constructors

    /** default constructor */
    public TipoReaseguroDTO() {
    }


   
    // Property accessors
    @Id 
    
    @Column(name="IDTCTIPOREASEGURO", unique=true, nullable=false, precision=2, scale=0)

    public int getTipoReaseguro() {
        return this.tipoReaseguro;
    }
    
    public void setTipoReaseguro(int tipoReaseguro) {
        this.tipoReaseguro = tipoReaseguro;
    }
    
    @Column(name="DESCRIPCION", length=50)

    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

	@Override
	public String getDescription() {
		return this.descripcion;
	}

	@Override
	public Object getId() {
		return this.tipoReaseguro;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof TipoReaseguroDTO) {
			TipoReaseguroDTO tipoReaseguroDTO = (TipoReaseguroDTO) object;
			equal = tipoReaseguroDTO.getTipoReaseguro() == this.tipoReaseguro;
		} // End of if
		return equal;
	}
    
    /*
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="tipoReaseguroDTO")

    public Set<MovimientoReaseguroDTO> getMovimientoReaseguroDTOs() {
        return this.movimientoReaseguroDTOs;
    }
    
    public void setMovimientoReaseguroDTOs(Set<MovimientoReaseguroDTO> movimientoReaseguroDTOs) {
        this.movimientoReaseguroDTOs = movimientoReaseguroDTOs;
    }*/
}