package mx.com.afirme.midas.contratos.movimiento;
// default package

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDTO;
import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDTO;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteDTO;
import mx.com.afirme.midas.contratos.linea.LineaDTO;




/**
 * MovimientoReaseguroDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOMOVIMIENTOREASEGURO"
    ,schema="MIDAS"
)

public class MovimientoReaseguroDTO  implements java.io.Serializable {


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields    

     private BigDecimal idMovimiento;
     private SubRamoDTO subRamo;
     
     private TipoReaseguroDTO tipoReaseguroDTO;
     private int tipoReaseguro;
     
     private ReaseguradorCorredorDTO reaseguradorCorredor;
     private ReaseguradorCorredorDTO corredor;     
     
     private TipoMovimientoDTO tipoMovimientoDTO;
     private int tipoMovimiento;
     
     //private ConceptoMovimientoDTO conceptoMovimientoDTO;
     private ConceptoMovimientoDetalleDTO conceptoMovimientoDetalleDTO;    
     private int conceptoMovimiento;
     
     /*private BigDecimal idContratoFacultativo;
     private BigDecimal idContratoPrimerExcedente;
     private BigDecimal idContratoCuotaParte;  */   
     private BigDecimal idPoliza;
     private BigDecimal idToMovimientoSiniestro;
     private Integer numeroEndoso;
     private BigDecimal idCobertura;
     private BigDecimal idInciso;     
     private BigDecimal idSubInciso;
     private BigDecimal cantidad;
     private int idMoneda;
     private String descripcion;
     private Date fechaRegistro;
     private int acumulado;
     private int ejercicio;     
     private ContratoFacultativoDTO contratoFacultativoDTO;
     private ContratoCuotaParteDTO contratoCuotaParteDTO;
     private ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO; 
     private LineaDTO lineaDTO;  
     
     private BigDecimal idToReporteSiniestro;
     //private ReporteSiniestroDTO reporteSiniestroDTO;
     
     private BigDecimal suscripcion;
     
 	private BigDecimal idSeccion;
	private int contabilizado;
	private Double tipoCambio;
	private Date fechaContabilizado;
	private String usuario;  
	private String comodin;	


    // Constructors
   
	/** default constructor */
    public MovimientoReaseguroDTO() {
    }



   
    // Property accessors
    @Id 
	@SequenceGenerator(name = "IDTOMOVIMIENTO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOMOVIMIENTO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOMOVIMIENTO_SEQ_GENERADOR")	    
    @Column(name="IDTOMOVIMIENTO", unique=true, nullable=false, precision=22, scale=0)
    public BigDecimal getIdMovimiento() {
        return this.idMovimiento;
    }
    
    public void setIdMovimiento(BigDecimal idMovimiento) {
        this.idMovimiento = idMovimiento;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTCSUBRAMO", nullable=false)
    public SubRamoDTO getSubRamo() {
        return this.subRamo;
    }
    
    public void setSubRamo(SubRamoDTO subramo) {
        this.subRamo = subramo;
    }
    
    @ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTCTIPOREASEGURO",updatable=false, insertable=false)
    public TipoReaseguroDTO getTipoReaseguroDTO() {
        return this.tipoReaseguroDTO;
    }
    
    public void setTipoReaseguroDTO(TipoReaseguroDTO tipoReaseguroDTO) {
        this.tipoReaseguroDTO = tipoReaseguroDTO;
    }

	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTCREASEGURADORCORREDOR")
    public ReaseguradorCorredorDTO getReaseguradorCorredor() {
        return this.reaseguradorCorredor;
    }
    
    public void setReaseguradorCorredor(ReaseguradorCorredorDTO tcreaseguradorcorredor) {
        this.reaseguradorCorredor = tcreaseguradorcorredor;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTCTIPOMOVIMIENTO", nullable=false,  updatable=false, insertable=false)
    public TipoMovimientoDTO getTipoMovimientoDTO() {
        return this.tipoMovimientoDTO;
    }
    
    public void setTipoMovimientoDTO(TipoMovimientoDTO tipoMovimientoDTO) {
        this.tipoMovimientoDTO = tipoMovimientoDTO;
    }
    
    
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTCCONCEPTOMOVIMIENTO", nullable=false, updatable=false, insertable=false)
	public ConceptoMovimientoDetalleDTO getConceptoMovimientoDetalleDTO() {
	    return this.conceptoMovimientoDetalleDTO;
	}
	
	public void setConceptoMovimientoDetalleDTO(ConceptoMovimientoDetalleDTO conceptoMovimientoDetalleDTO) {
	    this.conceptoMovimientoDetalleDTO = conceptoMovimientoDetalleDTO;
	}  
    
    /*
    @Column(name="IDTMCONTRATOFACULTATIVO", precision=22, scale=0)
    public BigDecimal getIdContratoFacultativo() {
        return this.idContratoFacultativo;
    }
    
    public void setIdContratoFacultativo(BigDecimal idContratoFacultativo) {
        this.idContratoFacultativo = idContratoFacultativo;
    }
    
    @Column(name="IDTMCONTRATOPRIMEREXCEDENTE", precision=22, scale=0)

    public BigDecimal getIdContratoPrimerExcedente() {
        return this.idContratoPrimerExcedente;
    }
    
    public void setIdContratoPrimerExcedente(BigDecimal idContratoPrimerExcedente) {
        this.idContratoPrimerExcedente = idContratoPrimerExcedente;
    }*/
    
    @Column(name="IDTOPOLIZA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdPoliza() {
        return this.idPoliza;
    }
    
    public void setIdPoliza(BigDecimal idPoliza) {
        this.idPoliza = idPoliza;
    }
    
    @Column(name="NUMEROENDOSO", nullable=false, precision=4, scale=0)

    public Integer getNumeroEndoso() {
        return this.numeroEndoso;
    }
    
    public void setNumeroEndoso(Integer numeroEndoso) {
        this.numeroEndoso = numeroEndoso;
    }
    
    @Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdCobertura() {
        return this.idCobertura;
    }
    
    public void setIdCobertura(BigDecimal idCobertura) {
        this.idCobertura = idCobertura;
    }
    
    @Column(name="IDTOSUBINCISO", precision=22, scale=0)

    public BigDecimal getIdSubInciso() {
        return this.idSubInciso;
    }
    
    public void setIdSubInciso(BigDecimal idSubInciso) {
        this.idSubInciso = idSubInciso;
    }
    

    
    /*
    @Column(name="IDTMCONTRATOCUOTAPARTE", precision=22, scale=0)
    public BigDecimal getIdContratoCuotaParte() {
        return this.idContratoCuotaParte;
    }
    
    public void setIdContratoCuotaParte(BigDecimal idContratoCuotaParte) {
        this.idContratoCuotaParte = idContratoCuotaParte;
    }*/
    
    @Column(name="CANTIDAD", nullable=false, precision=10)
    public BigDecimal getCantidad() {
        return this.cantidad;
    }
    
    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }
    
    @Column(name="IDMONEDA", nullable=false, precision=2, scale=0)
    public int getIdMoneda() {
        return this.idMoneda;
    }
    
    public void setIdMoneda(int idMoneda) {
        this.idMoneda = idMoneda;
    }
    
    @Column(name="DESCRIPCION", length=300)

    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    

@Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHAREGISTRO", nullable=false, length=7)

    public Date getFechaRegistro() {
        return this.fechaRegistro;
    }
    
    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
    
    @Column(name="ACUMULADO", precision=2, scale=0)
    public int getAcumulado() {
        return this.acumulado;
    }
    
    public void setAcumulado(int acumulado) {
        this.acumulado = acumulado;
    }



    @Column(name="IDTCTIPOREASEGURO", nullable=false, precision=2, scale=0)
	public int getTipoReaseguro() {
		return tipoReaseguro;
	}




	public void setTipoReaseguro(int tipoReaseguro) {
		this.tipoReaseguro = tipoReaseguro;
	}



    @Column(name="IDTCTIPOMOVIMIENTO", nullable=false, precision=2, scale=0)
	public int getTipoMovimiento() {
		return tipoMovimiento;
	}




	public void setTipoMovimiento(int tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}



	   @Column(name="IDTCCONCEPTOMOVIMIENTO", nullable=false, precision=2, scale=0)
	public int getConceptoMovimiento() {
		return conceptoMovimiento;
	}




	public void setConceptoMovimiento(int conceptoMovimiento) {
		this.conceptoMovimiento = conceptoMovimiento;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTMCONTRATOFACULTATIVO")
	public ContratoFacultativoDTO getContratoFacultativoDTO() {
	    return this.contratoFacultativoDTO;
	}

	public void setContratoFacultativoDTO(ContratoFacultativoDTO contratoFacultativoDTO) {
	    this.contratoFacultativoDTO = contratoFacultativoDTO;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	    @JoinColumn(name="IDTMCONTRATOCUOTAPARTE")
	public ContratoCuotaParteDTO getContratoCuotaParteDTO() {
	    return this.contratoCuotaParteDTO;
	}

	public void setContratoCuotaParteDTO(ContratoCuotaParteDTO contratoCuotaParteDTO) {
	    this.contratoCuotaParteDTO = contratoCuotaParteDTO;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTMCONTRATOPRIMEREXCEDENTE")
	public ContratoPrimerExcedenteDTO getContratoPrimerExcedenteDTO() {
	    return this.contratoPrimerExcedenteDTO;
	}
	
	public void setContratoPrimerExcedenteDTO(ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO) {
	    this.contratoPrimerExcedenteDTO = contratoPrimerExcedenteDTO;
	}	

    @Column(name="IDTOINCISO", precision=22, scale=0)
    public BigDecimal getIdInciso() {
        return this.idInciso;
    }
    
    public void setIdInciso(BigDecimal idInciso) {
        this.idInciso = idInciso;
    }
 
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDLINEA")
	public LineaDTO getLineaDTO() {
	    return this.lineaDTO;
	}
	
	public void setLineaDTO(LineaDTO lineaDTO) {
	    this.lineaDTO = lineaDTO;
	}    
	
	/*
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="IDTOREPORTESINIESTRO")
	public ReporteSiniestroDTO getReporteSiniestroDTO() {
	    return this.reporteSiniestroDTO;
	}

	public void setReporteSiniestroDTO(ReporteSiniestroDTO reporteSiniestroDTO) {
	    this.reporteSiniestroDTO = reporteSiniestroDTO;
	}
	*/
		
	
    @Column(name="EJERCICIO", nullable=false, precision=4, scale=0)
    public int getEjercicio() {
        return this.ejercicio;
    }
    
    public void setEjercicio(int ejercicio) {
        this.ejercicio = ejercicio;
    }	
	
    @Transient 
 	public BigDecimal getSuscripcion() {
		return suscripcion;
	}

	public void setSuscripcion(BigDecimal suscripcion) {
		this.suscripcion = suscripcion;
	} 
	
	@Column(name = "IDTOSECCION", precision = 22, scale = 0)
	public BigDecimal getIdSeccion() {
		return this.idSeccion;
	}

	public void setIdSeccion(BigDecimal idSeccion) {
		this.idSeccion = idSeccion;
	}

	@Column(name = "CONTABILIZADO", precision = 1, scale = 0)
	public int getContabilizado() {
		return this.contabilizado;
	}

	public void setContabilizado(int contabilizado) {
		this.contabilizado = contabilizado;
	}

	@Column(name = "TIPOCAMBIO", precision = 16)
	public Double getTipoCambio() {
		return this.tipoCambio;
	}

	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHACONTABILIZADO", length = 7)
	public Date getFechaContabilizado() {
		return this.fechaContabilizado;
	}

	public void setFechaContabilizado(Date fechaContabilizado) {
		this.fechaContabilizado = fechaContabilizado;
	}

	@Column(name = "USUARIO", length = 50)
	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCCORREDOR")
	public ReaseguradorCorredorDTO getCorredor() {
		return this.corredor;
	}

	public void setCorredor(
			ReaseguradorCorredorDTO corredor) {
		this.corredor = corredor;
	}	
	
	@Column(name = "COMODIN", length = 50)
	public String getComodin() {
		return this.comodin;
	}

	public void setComodin(String comodin) {
		this.comodin = comodin;
	}

    @Column(name="IDTOREPORTESINIESTRO", nullable=true, precision=22, scale=0)
	public BigDecimal getIdToReporteSiniestro() {
		return idToReporteSiniestro;
	}

	public void setIdToReporteSiniestro(BigDecimal idToReporteSiniestro) {
		this.idToReporteSiniestro = idToReporteSiniestro;
	}
	
	public void setIdToMovimientoSiniestro(BigDecimal idToMovimientoSiniestro) {
		this.idToMovimientoSiniestro = idToMovimientoSiniestro;
	}
	
	@Column(name="IDTOMOVIMIENTOSINIESTRO",  nullable=true, precision=22, scale=0)
	public BigDecimal getIdToMovimientoSiniestro() {
		return idToMovimientoSiniestro;
	}	
	
}