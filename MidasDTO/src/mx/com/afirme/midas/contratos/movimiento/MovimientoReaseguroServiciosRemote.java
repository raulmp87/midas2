package mx.com.afirme.midas.contratos.movimiento;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.contratos.estadocuenta.AcumuladorDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDTO;
import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.interfaz.asientocontable.AsientoContableDTO;


public interface MovimientoReaseguroServiciosRemote {
	
	/**
	 * 
	 * @param movimiento
	 * @return
	 */
	MovimientoReaseguroDTO registrarMovimiento(MovimientoReaseguroDTO movimiento,Boolean restarMovimiento);
	
	/**
	 * Registra un conjunto de movimientos reaseguro en una sola transacci�n.
	 * @param movimientos
	 */
	List<MovimientoReaseguroDTO> registrarMovimientos(List<MovimientoReaseguroDTO> movimientos);	
	
	/**
	 * This method is pretty similar to the method MovimientoReaseguroDTO registrarMovimiento(MovimientoReaseguroDTO movimiento), the difference consist
	 * on the origin of the estadoCuenta entity, in this implementation that entity is provided, in the prior method the entity must be retrieved/created.
	 * @param movimiento
	 * @param estadoCuenta, Receives the EstadoCuentaDTO that will be used to persist the MovimientoReaseguroDTO
	 * @return MovimientoReaseguroDTO
	 */
	MovimientoReaseguroDTO registrarMovimiento(MovimientoReaseguroDTO movimiento, EstadoCuentaDTO estadoCuenta);
	

	/**
	 * 
	 * @param movimientos
	 */
	//void marcarComoContabilizados(List<MovimientoReaseguroDTO> movimientos);
	
	/**
	 * 
	 * @param identificador
	 * @param claveModulo
	 * @param listaMovimientos
	 */
	void registrarContabilidad(String identificador, String claveModulo, List<MovimientoReaseguroDTO> listaMovimientos);
	
	public List<AsientoContableDTO> registrarContabilidad(String identificador, String claveModulo);
	
	/**
	 * 
	 * @return
	 */
	List<String> obtenerMovimientosIngresosPendientesPorContabilizar();
	
	/***
	 * 
	 * @param listaClaveRemesas
	 * @param usuario
	 */
	void registrarContabilidad(List<String> listaClaveRemesas, String usuario);	

	/**
	 *  Service for Undoing all the {@link MovimientoReaseguroDTO} related to a Poliza
	 * @param idToPoliza
	 */
	public void deshacerMovimientosPorPoliza(PolizaSoporteDanosDTO polizaSoporteDanosDTO);
	
	/**
	 * This service must calculate the suscription corresponding to a certain movimientoReaseguro entity
	 * @param movimientoReaseguroDTO
	 */
	public BigDecimal calcularSuscripcion(MovimientoReaseguroDTO movimientoReaseguroDTO);
	
	/**
	 * Return the maximum id in MovimientoReaseguroDTO
	 */
	public int getMaxId(Integer mes,Integer anio);
	
	/**
	 * Returns a list of {@link MovimientoReaseguroDTO} that are in a range of ids
	 * @param limiteInferior
	 * @param limiteSuperior
	 * @return
	 */
	public List<MovimientoReaseguroDTO> listarPorRangoIds(Integer mes,Integer anio,int limiteInferior, int limiteSuperior);
	
	public List<MovimientoReaseguroDTO> listarPorRangoIds(List<BigDecimal> listaIdToMovimientos,int limiteInferior, int limiteSuperior);
	
	/**
	 * Apply to AcumuladorDTO amounts defined in each {@link MovimientoReaseguroDTO} related to a given {@link ReporteSiniestroDTO}
	 * @param idToReporteSiniestro, Id from the {@link ReporteSiniestroDTO}
	 */
	public void acumularMovimientosTerminacionSiniestros(BigDecimal idToReporteSiniestro);
	
	/**
	 * Create an {@link EstadoCuentaDTO} based on data of a single {@link MovimientoReaseguroDTO}.
	 * @param movimientoReaseguroDTO
	 * @return
	 */
	public EstadoCuentaDTO generaEstadoCuenta(MovimientoReaseguroDTO movimientoReaseguroDTO);
	
	/**
	 * Create an {@link AcumuladorDTO} based on data of a single {@link MovimientoReaseguroDTO} and {@link EstadoCuentaDTO}.
	 * @param movimientoReaseguroDTO
	 * @param estadoCuentaDTO
	 * @return
	 */
	public AcumuladorDTO generaAcumulador(MovimientoReaseguroDTO movimientoReaseguroDTO, EstadoCuentaDTO estadoCuentaDTO);

	/**
	 * Consulta la cantidad de movimientos de reaseguro que le corresponden a un siniestro y no deben ser acumulados, debido a la bandera
	 * del concepto movimiento (acumulaEstadoDeCuenta)
	 */
	public int obtenerCantidadMovimientosSiniestrosNoAcumulables();
	
	/**
	 * Consulta la cantidad de movimientos de reaseguro que le corresponden a un siniestro y tienen la fecha de registro incorrecta (fecha de emision)
	 */
	public int obtenerCantidadMovimientosSiniestrosConFechaIncorrecta();
	
	/**
	 * Consulta los movimientos de reaseguro que le corresponden a un siniestro y no deben ser acumulados, debido a la bandera
	 * del concepto movimiento (acumulaEstadoDeCuenta).
	 * Los parametros limiteInferior y limiteSuperior son opcionales.
	 * @parameter limiteInferior. El menor registro por consultar.
	 * @parameter limiteSuperior. El m�ximo registro por consultar
	 */
	public List<MovimientoReaseguroDTO> listarMovimientosSiniestrosNoAcumulables(Integer limiteInferior, Integer limiteSuperior);
	
	/**
	 * Consulta los movimientos de reaseguro que le corresponden a un siniestro y tienen la fecha de registro incorrecta (fecha de emision)
	 */
	public List<MovimientoReaseguroDTO> listarMovimientosSiniestrosConFechaIncorrecta(Integer limiteInferior, Integer limiteSuperior);
		
	/**
	 * Clase que registra Ingresos de reaseguro
	 */
	public List<AsientoContableDTO> contabilizaMovimientosReaIn(String idObjetoContable, String claveTransaccionContable,  String usuario);
	
	public boolean recalcularAcumulador(BigDecimal idToAcumulador);
	
	public List<BigDecimal> obtenerIdReporteSiniestrosTerminados();
	
	public BigDecimal obtenerSaldoSiniestro(BigDecimal idToReporteSiniestro,int idConceptoMovimiento,int naturaleza);
	
	public List<SubRamoDTO> obtenerSubRamosAfectadosPorSiniestro(BigDecimal idToReporteSiniestro,boolean incluirSoloSubRamosFacultados);
}
