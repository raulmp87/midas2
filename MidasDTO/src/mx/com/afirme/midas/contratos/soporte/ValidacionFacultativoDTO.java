package mx.com.afirme.midas.contratos.soporte;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;

public class ValidacionFacultativoDTO {


	private Map<SubRamoDTO, Integer> mapaTipoDistribuciones;
	private Map<SubRamoDTO, CumuloDTO> mapaCumulos;

	public ValidacionFacultativoDTO() {
		mapaTipoDistribuciones = new HashMap<SubRamoDTO, Integer>();
		mapaCumulos = new HashMap<SubRamoDTO, CumuloDTO>();
	}

	/**
	 * 
	 * @param subRamo
	 */
	public void agregarSubRamo(SubRamoDTO subRamo){
		mapaTipoDistribuciones.put(subRamo, null);
	}

	/**
	 * 
	 * @param subRamo
	 * @param tipoDistribucion
	 */
	public void asignarTipoDistribucion(SubRamoDTO subRamo, Integer tipoDistribucion){
		mapaTipoDistribuciones.put(subRamo, tipoDistribucion);
	}

	/**
	 * 
	 * @param subRamo
	 */
	public int obtenerTipoDistribucion(SubRamoDTO subRamo){
		return mapaTipoDistribuciones.get(subRamo);
	}

	public Set<SubRamoDTO> obtenerSubRamos(){
		return mapaTipoDistribuciones.keySet();
	}

	/**
	 * 
	 * @param subRamo
	 * @param cumulo
	 */
	public void agregarCumulo(SubRamoDTO subRamo, CumuloDTO cumulo){
		mapaCumulos.put(subRamo, cumulo);
	}

	/**
	 * 
	 * @param subRamo
	 */
	public CumuloDTO obtenerCumulo(SubRamoDTO subRamo){
		return mapaCumulos.get(subRamo);
	}
	
	/**
	 * 
	 * @return
	 */
	public List<CumuloDTO> obtenerCumulos(){
		return (List<CumuloDTO>)mapaCumulos.values();
	}
	
	public boolean esFacultativoRequerido(SubRamoDTO subRamo){
		boolean esRequerido = false;
		
		if(this.mapaCumulos.get(subRamo) != null){
			esRequerido = this.mapaCumulos.get(subRamo).isFacultativoRequerido();
		}		
		return esRequerido;
	}
}
