package mx.com.afirme.midas.contratos.soporte;

import javax.ejb.Remote;


public interface SoporteContratosReaseguroServiciosRemote {
	
	ValidacionFacultativoDTO obtenerTipoDistribucion(ValidacionFacultativoDTO datosValidacion);
	
	ValidacionFacultativoDTO validarSoporteFacultativo(ValidacionFacultativoDTO datosValidacion);
	
	/**
	 * Por definir firma del servicio
	 * @param datosValidacion
	 */
	ValidacionFacultativoDTO solicitarFacultativo(ValidacionFacultativoDTO datosValidacion);

}
