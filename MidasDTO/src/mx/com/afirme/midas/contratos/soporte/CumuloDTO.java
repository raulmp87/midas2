package mx.com.afirme.midas.contratos.soporte;

import java.io.Serializable;
import java.math.BigDecimal;

public class CumuloDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long idCumulo;	
	private int idSubramo;
	private long idCotizacion;
	private long numeroInciso; 
	private long idSeccion;
	private long numeroSubInciso; 
	private BigDecimal sumaAsegurada;
	private BigDecimal primaNeta;
	private int tipoDistribucion;
	private float porcentajePleno;
	private boolean aplicaPrimerRiesgo;
	
	private float porcentajeRetencion;
	private float porcentajeCuotaParte;
	private float porcentajePrimerExcedente;
	private float porcentajeFacultativo;
	
	
	public int getIdSubramo() {
		return idSubramo;
	}
	public void setIdSubramo(int idSubramo) {
		this.idSubramo = idSubramo;
	}
	public long getIdCotizacion() {
		return idCotizacion;
	}
	public void setIdCotizacion(long idCotizacion) {
		this.idCotizacion = idCotizacion;
	}
	public long getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(long numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public long getIdSeccion() {
		return idSeccion;
	}
	public void setIdSeccion(long idSeccion) {
		this.idSeccion = idSeccion;
	}
	public long getNumeroSubInciso() {
		return numeroSubInciso;
	}
	public void setNumeroSubInciso(long numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}
	public BigDecimal getSumaAsegurada() {
		return sumaAsegurada;
	}
	public void setSumaAsegurada(BigDecimal sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
	public int getTipoDistribucion() {
		return tipoDistribucion;
	}
	public void setTipoDistribucion(int tipoDistribucion) {
		this.tipoDistribucion = tipoDistribucion;
	}
	public float getPorcentajePleno() {
		return porcentajePleno;
	}
	public void setPorcentajePleno(float porcentajePleno) {
		this.porcentajePleno = porcentajePleno;
	}
	public boolean isAplicaPrimerRiesgo() {
		return aplicaPrimerRiesgo;
	}
	public void setAplicaPrimerRiesgo(boolean aplicaPrimerRiesgo) {
		this.aplicaPrimerRiesgo = aplicaPrimerRiesgo;
	}
	public float getPorcentajeRetencion() {
		return porcentajeRetencion;
	}
	public void setPorcentajeRetencion(float porcentajeRetencion) {
		this.porcentajeRetencion = porcentajeRetencion;
	}
	public float getPorcentajeCuotaParte() {
		return porcentajeCuotaParte;
	}
	public void setPorcentajeCuotaParte(float porcentajeCuotaParte) {
		this.porcentajeCuotaParte = porcentajeCuotaParte;
	}
	public float getPorcentajePrimerExcedente() {
		return porcentajePrimerExcedente;
	}
	public void setPorcentajePrimerExcedente(float porcentajePrimerExcedente) {
		this.porcentajePrimerExcedente = porcentajePrimerExcedente;
	}
	public float getPorcentajeFacultativo() {
		return porcentajeFacultativo;
	}
	public void setPorcentajeFacultativo(float porcentajeFacultativo) {
		this.porcentajeFacultativo = porcentajeFacultativo;
	}
	public boolean isFacultativoRequerido() {
		return porcentajeFacultativo == 0.0 ? false : true;
	}
	public long getIdCumulo() {
		return idCumulo;
	}
	public void setIdCumulo(long idCumulo) {
		this.idCumulo = idCumulo;
	}
	public BigDecimal getPrimaNeta() {
		return primaNeta;
	}
	public void setPrimaNeta(BigDecimal primaNeta) {
		this.primaNeta = primaNeta;
	}
	

}
