package mx.com.afirme.midas.contratos.lineaparticipacion;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * LineaParticipacionId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class LineaParticipacionId implements java.io.Serializable {
	private static final long serialVersionUID = -5832808917119337062L;
	// Fields

	private BigDecimal idTdParticipacion;
	private BigDecimal idTmLinea;

	// Constructors

	/** default constructor */
	public LineaParticipacionId() {
	}

	/** full constructor */
	public LineaParticipacionId(BigDecimal idTdParticipacion,
			BigDecimal idTmLinea) {
		this.idTdParticipacion = idTdParticipacion;
		this.idTmLinea = idTmLinea;
	}

	// Property accessors

	@Column(name = "IDTDPARTICIPACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTdParticipacion() {
		return this.idTdParticipacion;
	}

	public void setIdTdParticipacion(BigDecimal idTdParticipacion) {
		this.idTdParticipacion = idTdParticipacion;
	}

	@Column(name = "IDTMLINEA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTmLinea() {
		return this.idTmLinea;
	}

	public void setIdTmLinea(BigDecimal idTmLinea) {
		this.idTmLinea = idTmLinea;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof LineaParticipacionId))
			return false;
		LineaParticipacionId castOther = (LineaParticipacionId) other;

		return ((this.getIdTdParticipacion() == castOther
				.getIdTdParticipacion()) || (this.getIdTdParticipacion() != null
				&& castOther.getIdTdParticipacion() != null && this
				.getIdTdParticipacion()
				.equals(castOther.getIdTdParticipacion())))
				&& ((this.getIdTmLinea() == castOther.getIdTmLinea()) || (this
						.getIdTmLinea() != null
						&& castOther.getIdTmLinea() != null && this
						.getIdTmLinea().equals(castOther.getIdTmLinea())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdTdParticipacion() == null ? 0 : this
						.getIdTdParticipacion().hashCode());
		result = 37 * result
				+ (getIdTmLinea() == null ? 0 : this.getIdTmLinea().hashCode());
		return result;
	}

}