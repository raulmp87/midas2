package mx.com.afirme.midas.contratos.lineaparticipacion;
// default package

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for LineaParticipacionFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface LineaParticipacionFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved LineaParticipacionDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            LineaParticipacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(LineaParticipacionDTO entity);

	/**
	 * Delete a persistent LineaParticipacionDTO entity.
	 * 
	 * @param entity
	 *            LineaParticipacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(LineaParticipacionDTO entity);

	/**
	 * Persist a previously saved LineaParticipacionDTO entity and return it or a
	 * copy of it to the sender. A copy of the LineaParticipacionDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            LineaParticipacionDTO entity to update
	 * @return LineaParticipacionDTO the persisted LineaParticipacionDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public LineaParticipacionDTO update(LineaParticipacionDTO entity);

	/**
	 * Find  LineaParticipacionDTO by id property.
	 * 
	 * @return LineaParticipacionDTO found by id
	 */
	public LineaParticipacionDTO findById(LineaParticipacionId id);

	/**
	 * Find all LineaParticipacionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the LineaParticipacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<LineaParticipacionDTO> found by query
	 */
	public List<LineaParticipacionDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all LineaParticipacionDTO entities.
	 * 
	 * @return List<LineaParticipacionDTO> all LineaParticipacionDTO entities
	 */
	public List<LineaParticipacionDTO> findAll();
}