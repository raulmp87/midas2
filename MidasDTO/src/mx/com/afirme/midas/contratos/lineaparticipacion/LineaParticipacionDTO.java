package mx.com.afirme.midas.contratos.lineaparticipacion;
// default package

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDTO;
import mx.com.afirme.midas.contratos.linea.LineaDTO;

/**
 * LineaParticipacionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TRTMLINEATDPARTICIPACION", schema = "MIDAS")
public class LineaParticipacionDTO implements java.io.Serializable {
	private static final long serialVersionUID = -5832808917129337032L;
	// Fields

	private LineaParticipacionId id;
	private ParticipacionDTO participacion;
	private LineaDTO linea;
	private Double comision;
	private Double comisionEnCombinacion;

	// Constructors

	/** default constructor */
	public LineaParticipacionDTO() {
		if (participacion==null)
		participacion = new ParticipacionDTO();
	}	

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idTdParticipacion", column = @Column(name = "IDTDPARTICIPACION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idTmLinea", column = @Column(name = "IDTMLINEA", nullable = false, precision = 22, scale = 0)) })
	public LineaParticipacionId getId() {
		return this.id;
	}

	public void setId(LineaParticipacionId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTDPARTICIPACION", nullable = false, insertable = false, updatable = false)
	public ParticipacionDTO getParticipacion() {
		return this.participacion;
	}

	public void setParticipacion(ParticipacionDTO participacion) {
		this.participacion = participacion;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTMLINEA", nullable = false, insertable = false, updatable = false)
	public LineaDTO getLinea() {
		return this.linea;
	}

	public void setLinea(LineaDTO linea) {
		this.linea = linea;
	}

	@Column(name = "COMISION", nullable = false, precision = 5)
	public Double getComision() {
		return this.comision;
	}

	public void setComision(Double comision) {
		this.comision = comision;
	}

	public void setComisionEnCombinacion(Double comisionEnCombinacion) {
		this.comisionEnCombinacion = comisionEnCombinacion;
	}

	@Column(name = "COMISIONENCOMBINACION", precision = 5)
	public Double getComisionEnCombinacion() {
		return comisionEnCombinacion;
	}

}