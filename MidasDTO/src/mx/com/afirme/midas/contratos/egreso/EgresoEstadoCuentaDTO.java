package mx.com.afirme.midas.contratos.egreso;
// default package

import java.math.BigDecimal;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDTO;


/**
 * EgresoEstadoCuentaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TREGRESOESTADOCUENTA"
    ,schema="MIDAS"
)
public class EgresoEstadoCuentaDTO  implements java.io.Serializable {


    // Fields    

	
	
	/**
	 * 
	 */
	 private static final long serialVersionUID = 1L;
	 
	 private EgresoEstadoCuentaId id;
     private EgresoReaseguroDTO egresoReaseguroDTO;
     private EstadoCuentaDTO estadoCuentaDTO;
 	 private BigDecimal monto;
 	 private BigDecimal idCheque;
 	 private Integer idEstatus;
 	 private String cuentaAfirme;
 	 private BigDecimal impuesto;

    // Constructors

    /** default constructor */
    public EgresoEstadoCuentaDTO() {
    }

   
    // Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idEgresoReaseguro", column = @Column(name = "IDTOEGRESOREASEGURO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idEstadoCuenta", column = @Column(name = "IDTOESTADOCUENTA", nullable = false, precision = 22, scale = 0)) })        
    public EgresoEstadoCuentaId getId() {
        return this.id;
    }
    
    public void setId(EgresoEstadoCuentaId id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTOEGRESOREASEGURO", nullable=false, insertable=false, updatable=false)

    public EgresoReaseguroDTO getEgresoReaseguro() {
        return this.egresoReaseguroDTO;
    }
    
    public void setEgresoReaseguro(EgresoReaseguroDTO egresoReaseguroDTO) {
        this.egresoReaseguroDTO = egresoReaseguroDTO;
    }

	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTOESTADOCUENTA", nullable=false, insertable=false, updatable=false)
    public EstadoCuentaDTO getEstadoCuentaDTO() {
        return this.estadoCuentaDTO;
    }
    
    public void setEstadoCuentaDTO(EstadoCuentaDTO estadoCuentaDTO) {
        this.estadoCuentaDTO = estadoCuentaDTO;
    }
   
	@Column(name = "MONTO", nullable = false, precision = 16)
	public BigDecimal getMonto() {
		return this.monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	@Column(name = "IDCHEQUE", precision = 22, scale = 0)
	public BigDecimal getIdCheque() {
		return this.idCheque;
	}

	public void setIdCheque(BigDecimal idCheque) {
		this.idCheque = idCheque;
	}
	
	//@Transient
	@Column(name = "ESTATUS", precision = 22, scale = 0)
	public Integer getidEstatus() {
		return this.idEstatus;
	}

	public void setidEstatus(Integer idEstatus) {
		this.idEstatus = idEstatus;
	}
	
	@Column(name = "CUENTAAFIRME", precision = 22, scale = 0)
	public String getCuentaAfirme() {
		return this.cuentaAfirme;
	}

	public void setCuentaAfirme(String cuentaAfirme) {
		this.cuentaAfirme = cuentaAfirme;
	}
	
    @Column(name="IMPUESTO", nullable=true, precision=24, scale=10)
    public BigDecimal getImpuesto() {
		return impuesto;
	}

	public void setImpuesto(BigDecimal impuesto) {
		this.impuesto = impuesto;
	}
	
	
}