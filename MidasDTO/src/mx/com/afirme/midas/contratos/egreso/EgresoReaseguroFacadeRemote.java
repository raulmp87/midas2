package mx.com.afirme.midas.contratos.egreso;
// default package

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.contratofacultativo.pagocobertura.PagoCoberturaReaseguradorDTO;
import mx.com.afirme.midas.contratofacultativo.participacionFacultativa.ParticipacionReaseguradorContratoFacultativoDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDecoradoDTO;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDecoradoDTO;
import mx.com.afirme.midas.contratos.participacioncorredor.ParticipacionCorredorDecoradoDTO;

/**
 * Remote interface for EgresoReaseguroFacade.
 * @author MyEclipse Persistence Tools
 */


public interface EgresoReaseguroFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved EgresoReaseguroDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity EgresoReaseguroDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(EgresoReaseguroDTO entity);
    /**
	 Delete a persistent EgresoReaseguroDTO entity.
	  @param entity EgresoReaseguroDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(EgresoReaseguroDTO entity);
   /**
	 Persist a previously saved EgresoReaseguroDTO entity and return it or a copy of it to the sender. 
	 A copy of the EgresoReaseguroDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity EgresoReaseguroDTO entity to update
	 @return EgresoReaseguroDTO the persisted EgresoReaseguroDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public EgresoReaseguroDTO update(EgresoReaseguroDTO entity);
	public EgresoReaseguroDTO findById( BigDecimal id);
	 /**
	 * Find all EgresoReaseguroDTO entities with a specific property value.  
	 
	  @param propertyName the name of the EgresoReaseguroDTO property to query
	  @param value the property value to match
	  	  @return List<EgresoReaseguroDTO> found by query
	 */
	public List<EgresoEstadoCuentaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all EgresoReaseguroDTO entities.
	  	  @return List<EgresoReaseguroDTO> all EgresoReaseguroDTO entities
	 */
	public List<EgresoReaseguroDTO> findAll();
	
	/**
	 * Returns a list of EgresoReaseguroDTO that are not associated to a EstadoCuentaDTO
	 * @return
	 */
	public List<EgresoReaseguroDTO> obtenerEgresosPendientes();
	
	/**
	 * Returns a list of EgresoReaseguroDTO that are not associated to a EstadoCuentaDTO
	 * @return
	 */
	public List<EgresoReaseguroDTO> obtenerEgresosPendientesCancel();
	
	/**
	 * 
	 * @param egresoReaseguro
	 * @param egresoEstadosCuenta
	 */
	public EgresoReaseguroDTO agregarEgreso(EgresoReaseguroDTO egresoReaseguro, 
											List<EgresoEstadoCuentaDTO> egresoEstadosCuenta,
											List<PagoCoberturaReaseguradorDTO> exhibiciones);
	
	/**
	 * Change the status of an {@link EgresoReaseguroDTO}
	 * @param egreso
	 */
	public void confirmarPago(EgresoReaseguroDTO egreso);
	
	/**
	 * Obtains all the EgresoEstadoCuentaDTO given its Id
	 * @param idToEgresoReaseguro
	 * @return a list of EgresoEstadoCuentaDTO
	 */
	public List<EgresoEstadoCuentaDTO> obtenerEstadosCuentaEgreso(BigDecimal idToEgresoReaseguro);
	
	/**
	 * Obtains all the ParticipacionDecoradoDTO obtained from the ParticipacionDTO entities related to a single {@link EstadoCuentaDTO}
	 * @param estadoCuenta
	 * @return a list of ParticipacionDecoradoDTO
	 */
	public List<ParticipacionDecoradoDTO> obtenerParticipaciones(EstadoCuentaDecoradoDTO estadoCuenta);
	
	/**
	 * 
	 * @param participacion
	 * @return
	 */
	public List<ParticipacionCorredorDecoradoDTO> obtenerParticipaciones(ParticipacionDecoradoDTO participacion, BigDecimal idContratoFacultativo, BigDecimal idEgresoReaseguro);
	
	/**
	 * Retrieve a list of {@link EgresoEstadoCuentaDTO} related to a specific {@link EstadoCuentaDTO}
	 * @param estadoCuenta
	 * @return a list of EgresoEstadoCuentaDTO
	 */
	public List<EgresoEstadoCuentaDTO> obtenerEgresosPorEstadoCuenta(EstadoCuentaDTO estadoCuenta, BigDecimal idEgresoReaseguro);
	
	/*
	public List<EgresoEstadoCuentaDTO> borrarEgresoDTO(EgresoReaseguroDTO egresoReaseguroId, BigDecimal idEstadoCuenta);
	
	public List<EgresoEstadoCuentaDTO> borrarEgresoCheque(EgresoReaseguroDTO egresoReaseguroId, BigDecimal idEstadoCuenta);
	*/	
	public List<EgresoEstadoCuentaDTO> eliminarEgresoDTOEstadoCuenta(EgresoReaseguroDTO egresoReaseguroId);
	
	public Object ultimoEgresoDTO();
	
	public void actualizaEgresoReaseguro(BigDecimal egresoReaseguro, Integer estatus, BigDecimal noCheque);
	
	public void actualizaEgresoReaseguroBorrar(BigDecimal egresoReaseguro, Integer estatus, BigDecimal noCheque);
	
	public List<EgresoEstadoCuentaDTO> obtenerEstadosCuentaEgresoEstatus();
	
	public List<EgresoEstadoCuentaDTO> obtenerEstadosCuentaEgresoCancel();
	
	public EgresoReaseguroDTO obtenerEgresosEstadoCuentaCheque(BigDecimal idEstadoCuenta, BigDecimal idEgresoReaseguro);

	public ParticipacionReaseguradorContratoFacultativoDTO obtenerParticipacionesEgresoContratoFacultativo(EstadoCuentaDecoradoDTO estadoCuenta);
	
	public List<ParticipacionReaseguradorContratoFacultativoDTO> obtenerConfiguracionPagosEgresoContratoFacultativo(BigDecimal idToEgresoReaseguro);
	
	public void eliminarEgreso(BigDecimal idEgresoReaseguro);
	
	public List<EgresoReaseguroDTO> obtenerEgresosCancelacionPendiente();
	
	public List<ParticipacionReaseguradorContratoFacultativoDTO> obtenerPagosPendientes(Date fechaCorte);
	
	public List<ParticipacionReaseguradorContratoFacultativoDTO> obtenerAutorizacionesPagosPendientes(short tipoAutorizacionPendiente);
	
	public List<EstadoCuentaDecoradoDTO> obtenerAutorizacionesPendientesPagoFacultativo();
	
	public boolean autorizarPagoCoberturaReasegurador(Long idToPlanPagosCobertura,Short numeroExhibicion,BigDecimal idReasegurador,
			boolean autorizacion,String claveUsuario,short tipoAutorizacionPendiente);
	
	public boolean autorizarPagoFacultativoPorEstadoCuenta(BigDecimal idToEstadoCuenta,boolean autorizacion,String claveUsuario);
}