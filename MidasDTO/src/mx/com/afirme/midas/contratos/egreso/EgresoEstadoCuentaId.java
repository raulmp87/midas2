package mx.com.afirme.midas.contratos.egreso;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * EgresoEstadoCuentaId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class EgresoEstadoCuentaId implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idEgresoReaseguro;
	private BigDecimal idEstadoCuenta;

	// Constructors

	/** default constructor */
	public EgresoEstadoCuentaId() {
	}

	/** full constructor */
	public EgresoEstadoCuentaId(BigDecimal idtoegresoreaseguro,
			BigDecimal idtoestadocuenta) {
		this.idEgresoReaseguro = idtoegresoreaseguro;
		this.idEstadoCuenta = idtoestadocuenta;
	}

	// Property accessors

	@Column(name = "IDTOEGRESOREASEGURO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdEgresoReaseguro() {
		return this.idEgresoReaseguro;
	}

	public void setIdEgresoReaseguro(BigDecimal idtoegresoreaseguro) {
		this.idEgresoReaseguro = idtoegresoreaseguro;
	}

	@Column(name = "IDTOESTADOCUENTA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdEstadoCuenta() {
		return this.idEstadoCuenta;
	}

	public void setIdEstadoCuenta(BigDecimal idtoestadocuenta) {
		this.idEstadoCuenta = idtoestadocuenta;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof EgresoEstadoCuentaId))
			return false;
		EgresoEstadoCuentaId castOther = (EgresoEstadoCuentaId) other;

		return ((this.getIdEgresoReaseguro() == castOther
				.getIdEgresoReaseguro()) || (this.getIdEgresoReaseguro() != null
				&& castOther.getIdEgresoReaseguro() != null && this
				.getIdEgresoReaseguro().equals(
						castOther.getIdEgresoReaseguro())))
				&& ((this.getIdEstadoCuenta() == castOther
						.getIdEstadoCuenta()) || (this.getIdEstadoCuenta() != null
						&& castOther.getIdEstadoCuenta() != null && this
						.getIdEstadoCuenta().equals(
								castOther.getIdEstadoCuenta())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdEgresoReaseguro() == null ? 0 : this
						.getIdEgresoReaseguro().hashCode());
		result = 37
				* result
				+ (getIdEstadoCuenta() == null ? 0 : this
						.getIdEstadoCuenta().hashCode());
		return result;
	}

}