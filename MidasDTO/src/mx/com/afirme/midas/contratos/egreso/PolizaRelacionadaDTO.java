package mx.com.afirme.midas.contratos.egreso;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.danios.soporte.EndosoSoporteDanosDTO;

public class PolizaRelacionadaDTO {
	
	private EndosoSoporteDanosDTO endosoDTO;
	
	private SubRamoDTO subRamoDTO;	
	
	public PolizaRelacionadaDTO(EndosoSoporteDanosDTO endosoDTO,
			SubRamoDTO subRamoDTO) {
		super();
		this.endosoDTO = endosoDTO;
		this.subRamoDTO = subRamoDTO;
	}
	
	public String getNumeroPoliza(){
		return endosoDTO.getNumeroPoliza();
	}
	
	public int getNumeroEndoso(){
		return endosoDTO.getNumeroEndoso();
	}
	
	public String getSubRamo(){
		return subRamoDTO.getDescripcionSubRamo();
	}

	

}
