package mx.com.afirme.midas.contratos.egreso;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PagoDTO implements Serializable{
	public static final String LIQUIDADO = "Liquidado";
	public static final String NO_LIQUIDADO = "Pendiente";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String numeroPago;
	
	private BigDecimal monto;
	
	private Date fechaPago;
	
	private String suscripcion;
	
	private EgresoEstadoCuentaDTO egresoEstadoCuentaDTO;
	

	public PagoDTO(EgresoEstadoCuentaDTO egresoEstadoCuentaDTO) {
		this.egresoEstadoCuentaDTO = egresoEstadoCuentaDTO;
	}	

	public String getNumeroPago() {
		return numeroPago;
	}

	public void setNumeroPago(String numeroPago) {
		this.numeroPago = numeroPago;
	}

	public BigDecimal getMonto() {
		return monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	public Date getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	public String getEstatus() {
		String desc = null;
		
		if(egresoEstadoCuentaDTO == null || egresoEstadoCuentaDTO.getEgresoReaseguro() == null){
			return null;
		}
		
		int estatus = egresoEstadoCuentaDTO.getEgresoReaseguro().getEstatus();
		
		if(estatus == EgresoReaseguroDTO.PENDIENTE){
			desc = NO_LIQUIDADO;
		}else{
			desc = LIQUIDADO;
		}
		
		return desc;
	}


	public String getSuscripcion() {
		return suscripcion;
	}

	public void setSuscripcion(String suscripcion) {
		this.suscripcion = suscripcion;
	}

	public EgresoEstadoCuentaDTO getEgresoEstadoCuentaDTO() {
		return egresoEstadoCuentaDTO;
	}

	public void setEgresoEstadoCuentaDTO(EgresoEstadoCuentaDTO egresoEstadoCuentaDTO) {
		this.egresoEstadoCuentaDTO = egresoEstadoCuentaDTO;
	}
	

}
