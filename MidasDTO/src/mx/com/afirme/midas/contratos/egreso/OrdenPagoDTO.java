package mx.com.afirme.midas.contratos.egreso;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.catalogos.cuentabanco.CuentaBancoDTO;
import mx.com.afirme.midas.contratofacultativo.participacionFacultativa.ParticipacionReaseguradorContratoFacultativoDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDecoradoDTO;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDecoradoDTO;
import mx.com.afirme.midas.contratos.participacioncorredor.ParticipacionCorredorDecoradoDTO;

public class OrdenPagoDTO implements Serializable{

	/** serialVersionUID **/
	private static final long serialVersionUID = 2L;
	
	private PolizaRelacionadaDTO polizaRelacionadaDTO;
	private EstadoCuentaDecoradoDTO estadoCuenta;
	private List<ParticipacionDecoradoDTO> listaParticipacion;
	private List<ParticipacionCorredorDecoradoDTO> listaParticipacionCorredor;
	private List<PagoDTO> listaPagos;
	private CuentaBancoDTO cuentaBanco;
	private EgresoEstadoCuentaDTO egresoEstadoCuenta;
	private EgresoEstadoCuentaDTO egresoEstadoCuentaDTO;
	private List<ParticipacionReaseguradorContratoFacultativoDTO> listaParticipacionFacultativo;
	/** Fecha de la orden de pago */
	private Date fechaEgreso;
	/** Estatus de la orden de pago */
	private int estatus;
	
	/**
	 * @return the polizaRelacionadaDTO
	 */
	public PolizaRelacionadaDTO getPolizaRelacionadaDTO() {
		return polizaRelacionadaDTO;
	}
	/**
	 * @param polizaRelacionadaDTO the polizaRelacionadaDTO to set
	 */
	public void setPolizaRelacionadaDTO(PolizaRelacionadaDTO polizaRelacionadaDTO) {
		this.polizaRelacionadaDTO = polizaRelacionadaDTO;
	}
	/**
	 * @return the estadoCuenta
	 */
	public EstadoCuentaDecoradoDTO getEstadoCuenta() {
		return estadoCuenta;
	}
	/**
	 * @param estadoCuenta the estadoCuenta to set
	 */
	public void setEstadoCuenta(EstadoCuentaDecoradoDTO estadoCuenta) {
		this.estadoCuenta = estadoCuenta;
	}
	/**
	 * @return the listaParticipacion
	 */
	public List<ParticipacionDecoradoDTO> getListaParticipacion() {
		return listaParticipacion;
	}
	/**
	 * @param listaParticipacion the listaParticipacion to set
	 */
	public void setListaParticipacion(List<ParticipacionDecoradoDTO> listaParticipacion) {
		this.listaParticipacion = listaParticipacion;
	}
	/**
	 * @return the listaParticipacionCorredor
	 */
	public List<ParticipacionCorredorDecoradoDTO> getListaParticipacionCorredor() {
		return listaParticipacionCorredor;
	}
	/**
	 * @param listaParticipacionCorredor the listaParticipacionCorredor to set
	 */
	public void setListaParticipacionCorredor(List<ParticipacionCorredorDecoradoDTO> listaParticipacionCorredor) {
		this.listaParticipacionCorredor = listaParticipacionCorredor;
	}
	/**
	 * @return the listaPagos
	 */
	public List<PagoDTO> getListaPagos() {
		return listaPagos;
	}
	/**
	 * @param listaPagos the listaPagos to set
	 */
	public void setListaPagos(List<PagoDTO> listaPagos) {
		this.listaPagos = listaPagos;
	}
	/**
	 * @return the cuentaBanco
	 */
	public CuentaBancoDTO getCuentaBanco() {
		return cuentaBanco;
	}
	/**
	 * @param cuentaBanco the cuentaBanco to set
	 */
	public void setCuentaBanco(CuentaBancoDTO cuentaBanco) {
		this.cuentaBanco = cuentaBanco;
	}
	/**
	 * @return the egresoEstadoCuenta
	 */
	public EgresoEstadoCuentaDTO getEgresoEstadoCuenta() {
		return egresoEstadoCuenta;
	}
	/**
	 * @param egresoEstadoCuenta the egresoEstadoCuenta to set
	 */
	public void setEgresoEstadoCuenta(EgresoEstadoCuentaDTO egresoEstadoCuenta) {
		this.egresoEstadoCuenta = egresoEstadoCuenta;
	}
	/**
	 * @return the egresoEstadoCuentaDTO
	 */
	public EgresoEstadoCuentaDTO getEgresoEstadoCuentaDTO() {
		return egresoEstadoCuentaDTO;
	}
	/**
	 * @param egresoEstadoCuentaDTO the egresoEstadoCuentaDTO to set
	 */
	public void setEgresoEstadoCuentaDTO(EgresoEstadoCuentaDTO egresoEstadoCuentaDTO) {
		this.egresoEstadoCuentaDTO = egresoEstadoCuentaDTO;
	}
	/**
	 * @return the listaParticipacionFacultativo
	 */
	public List<ParticipacionReaseguradorContratoFacultativoDTO> getListaParticipacionFacultativo() {
		return listaParticipacionFacultativo;
	}
	/**
	 * @param listaParticipacionFacultativo the listaParticipacionFacultativo to set
	 */
	public void setListaParticipacionFacultativo(
			List<ParticipacionReaseguradorContratoFacultativoDTO> listaParticipacionFacultativo) {
		this.listaParticipacionFacultativo = listaParticipacionFacultativo;
	}
	/**
	 * @return the fechaEgreso
	 */
	public Date getFechaEgreso() {
		return fechaEgreso;
	}
	/**
	 * @param fechaEgreso the fechaEgreso to set
	 */
	public void setFechaEgreso(Date fechaEgreso) {
		this.fechaEgreso = fechaEgreso;
	}
	/**
	 * @return the estatus
	 */
	public int getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}	
}