package mx.com.afirme.midas.contratos.egreso;
// default package

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;


/**
 * EgresoReaseguroDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOEGRESOREASEGURO"
    ,schema="MIDAS"
)
public class EgresoReaseguroDTO  implements java.io.Serializable {
	
	public static final int PENDIENTE = 0;
	public static final int CONFIRMADO = 1;
	
	public static final short ESTATUS_PENDIENTE = 0;
	public static final short ESTATUS_PAGADO = 1;
	public static final short ESTATUS_CANCELADO = 2;
	public static final short ESTATUS_EN_SOLICITUD_MIZAR = 3;
	public static final short ESTATUS_EN_PROCESO_DE_CANCELACION = 4;		

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields    


	private BigDecimal idEgresoReaseguro;
     private Double monto;
     private int idMoneda;
     private Double tipoCambio;
     private String referencia;
     private Date fechaEgreso;
     private int estatus;
     private Double impuesto;
     private boolean registrarEgresoEdoCtaAutomaticos;
     private boolean registrarEgresoEdoCtaFacultativos;
     private String cuentaAfirme;
     private Double factorTipoCambio;
     private ReaseguradorCorredorDTO reasegurador;
     private ReaseguradorCorredorDTO corredor;
     
     
     private Set<EgresoEstadoCuentaDTO> egresoEstadoCuentaDTOs = new HashSet<EgresoEstadoCuentaDTO>(0);


    // Constructors

    /** default constructor */
    public EgresoReaseguroDTO() {
    }
   
    // Property accessors
    @Id 
	@SequenceGenerator(name = "IDTOEGRESOREASEGURO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOEGRESOREASEGURO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOEGRESOREASEGURO_SEQ_GENERADOR")    
    @Column(name="IDTOEGRESOREASEGURO", unique=true, nullable=false, precision=22, scale=0)
    public BigDecimal getIdEgresoReaseguro() {
        return this.idEgresoReaseguro;
    }
    
    public void setIdEgresoReaseguro(BigDecimal idEgresoReaseguro) {
        this.idEgresoReaseguro = idEgresoReaseguro;
    }
    
    @Column(name="MONTO", nullable=false, precision=24, scale=10)

    public Double getMonto() {
        return this.monto;
    }
    
    public void setMonto(Double monto) {
        this.monto = monto;
    }
    
    @Column(name="IDTCMONEDA", nullable=false, precision=3, scale=0)

    public int getIdMoneda() {
        return this.idMoneda;
    }
    
    public void setIdMoneda(int idMoneda) {
        this.idMoneda = idMoneda;
    }
    
    @Column(name="TIPOCAMBIO", precision=16)

    public Double getTipoCambio() {
        return this.tipoCambio;
    }
    
    public void setTipoCambio(Double tipoCambio) {
        this.tipoCambio = tipoCambio;
    }
    
    @Column(name="REFERENCIA", length=100)

    public String getReferencia() {
        return this.referencia;
    }
    
    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }
@Temporal(TemporalType.DATE)
    @Column(name="FECHAEGRESO", length=7)

    public Date getFechaEgreso() {
        return this.fechaEgreso;
    }
    
    public void setFechaEgreso(Date fechaEgreso) {
        this.fechaEgreso = fechaEgreso;
    }
    
    @Column(name="ESTATUS", nullable=false, precision=1, scale=0)

    public int getEstatus() {
        return this.estatus;
    }
    
    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }
    
    @Column(name="IMPUESTO", nullable=true, precision=24, scale=10)
    public Double getImpuesto() {
		return impuesto;
	}

	public void setImpuesto(Double impuesto) {
		this.impuesto = impuesto;
	}

	@Transient
	public boolean getRegistrarEgresoEdoCtaAutomaticos() {
		return registrarEgresoEdoCtaAutomaticos;
	}

	public void setRegistrarEgresoEdoCtaAutomaticos(
			boolean registrarEgresoEdoCtaAutomaticos) {
		this.registrarEgresoEdoCtaAutomaticos = registrarEgresoEdoCtaAutomaticos;
	}

	@Transient
	public boolean getRegistrarEgresoEdoCtaFacultativos() {
		return registrarEgresoEdoCtaFacultativos;
	}

	public void setRegistrarEgresoEdoCtaFacultativos(
			boolean registrarEgresoEdoCtaFacultativos) {
		this.registrarEgresoEdoCtaFacultativos = registrarEgresoEdoCtaFacultativos;
	}

	@Transient
	public String getCuentaAfirme() {
		return cuentaAfirme;
	}

	public void setCuentaAfirme(String cuentaAfirme) {
		this.cuentaAfirme = cuentaAfirme;
	}

	@Transient
	public Double getFactorTipoCambio() {
		return factorTipoCambio;
	}

	public void setFactorTipoCambio(Double factorTipoCambio) {
		this.factorTipoCambio = factorTipoCambio;
	}

	@Transient
	public ReaseguradorCorredorDTO getReasegurador() {
		return reasegurador;
	}

	public void setReasegurador(ReaseguradorCorredorDTO reasegurador) {
		this.reasegurador = reasegurador;
	}

	@Transient
	public ReaseguradorCorredorDTO getCorredor() {
		return corredor;
	}

	public void setCorredor(ReaseguradorCorredorDTO corredor) {
		this.corredor = corredor;
	}

@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="egresoReaseguro")

    public Set<EgresoEstadoCuentaDTO> getEgresoEstadoCuentas() {
        return this.egresoEstadoCuentaDTOs;
    }
    
    public void setEgresoEstadoCuentas(Set<EgresoEstadoCuentaDTO> egresoEstadoCuentaDTOs) {
        this.egresoEstadoCuentaDTOs = egresoEstadoCuentaDTOs;
    }
}