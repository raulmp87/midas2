package mx.com.afirme.midas.contratos.estadocuenta.presentacion;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * ConfiguracionConceptoEstadoCuentaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCCONFIGURACIONCONCEPTOEDOCTA", schema="MIDAS")
public class ConfiguracionConceptoEstadoCuentaDTO  implements java.io.Serializable,java.lang.Comparable<ConfiguracionConceptoEstadoCuentaDTO> {
	//constantes para configuraciones de conceptos
	public static final Integer ID_CONFIG_CONCEPTO_REMESA_SALDO_AUTOMATICOS_PESOS = 83;
	public static final Integer ID_CONFIG_CONCEPTO_REMESA_SALDO_AUTOMATICOS_DOLARES = 93;

	private static final long serialVersionUID = 4640570511921258086L;
	private Integer idTcConfiguracionConcepto;
    private Short orden;
    private String nombreConcepto;
    private Integer naturaleza;
    private AgrupacionEstadoCuentaDTO agrupacionEstadoCuentaDTO;
    private Integer aplicaDesglocePorEjercicio;
	private List<DetalleConfiguracionEstadoCuentaDTO> detalleConfiguracionEstadoCuentaDTOs = new ArrayList<DetalleConfiguracionEstadoCuentaDTO>();

    // Constructors

    /** default constructor */
    public ConfiguracionConceptoEstadoCuentaDTO() {
    }

	/** minimal constructor */
    public ConfiguracionConceptoEstadoCuentaDTO(Integer idTcConfiguracionConcepto) {
        this.idTcConfiguracionConcepto = idTcConfiguracionConcepto;
    }
    
    /** full constructor */
    public ConfiguracionConceptoEstadoCuentaDTO(Integer idTcConfiguracionConcepto, Short orden, String nombreConcepto, Integer naturaleza, List<DetalleConfiguracionEstadoCuentaDTO> detalleConfiguracionEstadoCuentaDTOs) {
        this.idTcConfiguracionConcepto = idTcConfiguracionConcepto;
        this.orden = orden;
        this.nombreConcepto = nombreConcepto;
        this.naturaleza = naturaleza;
        this.detalleConfiguracionEstadoCuentaDTOs = detalleConfiguracionEstadoCuentaDTOs;
    }

   
    // Property accessors
    @Id
    @Column(name="IDTCCONFIGCONCEPTO", nullable=false, precision=8, scale=0)
    public Integer getIdTcConfiguracionConcepto() {
		return idTcConfiguracionConcepto;
	}

	public void setIdTcConfiguracionConcepto(Integer idTcConfiguracionConcepto) {
		this.idTcConfiguracionConcepto = idTcConfiguracionConcepto;
	}
    
    @Column(name="ORDEN", precision=4, scale=0)
    public Short getOrden() {
        return this.orden;
    }
    
    public void setOrden(Short orden) {
        this.orden = orden;
    }
    
    @Column(name="NOMBRECONCEPTO", length=100)
    public String getNombreConcepto() {
        return this.nombreConcepto;
    }
    
    public void setNombreConcepto(String nombreConcepto) {
        this.nombreConcepto = nombreConcepto;
    }
    
    @Column(name="NATURALEZA", precision=1, scale=0)
    public Integer getNaturaleza() {
        return this.naturaleza;
    }
    
    public void setNaturaleza(Integer naturaleza) {
        this.naturaleza = naturaleza;
    }
    
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="configuracionConceptoEstadoCuentaDTO")
    public List<DetalleConfiguracionEstadoCuentaDTO> getDetalleConfiguracionEstadoCuentaDTOs() {
        return this.detalleConfiguracionEstadoCuentaDTOs;
    }
    
    public void setDetalleConfiguracionEstadoCuentaDTOs(List<DetalleConfiguracionEstadoCuentaDTO> detalleConfiguracionEstadoCuentaDTOs) {
        this.detalleConfiguracionEstadoCuentaDTOs = detalleConfiguracionEstadoCuentaDTOs;
    }

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="IDTCAGRUPACIONEDOCTA", nullable=false, insertable=false, updatable=false)
	public AgrupacionEstadoCuentaDTO getAgrupacionEstadoCuentaDTO() {
		return agrupacionEstadoCuentaDTO;
	}

	public void setAgrupacionEstadoCuentaDTO(AgrupacionEstadoCuentaDTO agrupacionEstadoCuentaDTO) {
		this.agrupacionEstadoCuentaDTO = agrupacionEstadoCuentaDTO;
	}

	public int compareTo(ConfiguracionConceptoEstadoCuentaDTO o) {
		return this.orden.compareTo(o.getOrden());
	}

	@Column(name="APLICADESGLOSEPOREJERCICIO", precision=1, scale=0)
	public Integer getAplicaDesglocePorEjercicio() {
		return aplicaDesglocePorEjercicio;
	}

	public void setAplicaDesglocePorEjercicio(Integer aplicaDesglocePorEjercicio) {
		this.aplicaDesglocePorEjercicio = aplicaDesglocePorEjercicio;
	}
}