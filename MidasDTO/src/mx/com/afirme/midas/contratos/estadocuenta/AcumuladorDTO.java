package mx.com.afirme.midas.contratos.estadocuenta;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.contratos.movimiento.ConceptoMovimientoDTO;


/**
 * AcumuladorDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOACUMULADOR", schema="MIDAS")
public class AcumuladorDTO  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	// Fields    

     private BigDecimal idAcumulador;
     private ConceptoMovimientoDTO conceptoMovimientoDTO;
     private EstadoCuentaDTO estadoCuentaDTO;
     private int mes;
     private int anio;
     private BigDecimal cargo;
     private BigDecimal abono;


    // Constructors

    /** default constructor */
    public AcumuladorDTO() {}

   
    // Property accessors
    @Id 
	@SequenceGenerator(name = "IDTOACUMULADOR_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOACUMULADOR_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOACUMULADOR_SEQ_GENERADOR")    
    @Column(name="IDTOACUMULADOR", unique=true, nullable=false, precision=22, scale=0)
    public BigDecimal getIdAcumulador() {
        return this.idAcumulador;
    }
    
    public void setIdAcumulador(BigDecimal idAcumulador) {
        this.idAcumulador = idAcumulador;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTCCONCEPTOMOVIMIENTO", nullable=false)
    public ConceptoMovimientoDTO getConceptoMovimientoDTO() {
        return this.conceptoMovimientoDTO;
    }
    
    public void setConceptoMovimientoDTO(ConceptoMovimientoDTO conceptoMovimientoDTO) {
        this.conceptoMovimientoDTO = conceptoMovimientoDTO;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTOESTADOCUENTA")
    public EstadoCuentaDTO getEstadoCuentaDTO() {
        return this.estadoCuentaDTO;
    }
    
    public void setEstadoCuentaDTO(EstadoCuentaDTO estadoCuentaDTO) {
        this.estadoCuentaDTO = estadoCuentaDTO;
    }
    
    @Column(name="MES", nullable=false, precision=2, scale=0)

    public int getMes() {
        return this.mes;
    }
    
    public void setMes(int mes) {
        this.mes = mes;
    }
    
    @Column(name="ANIO", nullable=false, precision=4, scale=0)

    public int getAnio() {
        return this.anio;
    }
    
    public void setAnio(int anio) {
        this.anio = anio;
    }
    
    @Column(name="CARGO", precision=10)

    public BigDecimal getCargo() {
        return this.cargo;
    }
    
    public void setCargo(BigDecimal cargo) {
        this.cargo = cargo;
    }
    
    @Column(name="ABONO", precision=10)

    public BigDecimal getAbono() {
        return this.abono;
    }
    
    public void setAbono(BigDecimal abono) {
        this.abono = abono;
    }

}