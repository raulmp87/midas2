package mx.com.afirme.midas.contratos.estadocuenta;
// default package

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.contratos.movimiento.MovimientoReaseguroDTO;

/**
 * Remote interface for AcumuladorDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface AcumuladorFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved AcumuladorDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity AcumuladorDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(AcumuladorDTO entity);
    /**
	 Delete a persistent AcumuladorDTO entity.
	  @param entity AcumuladorDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(AcumuladorDTO entity);
   /**
	 Persist a previously saved AcumuladorDTO entity and return it or a copy of it to the sender. 
	 A copy of the AcumuladorDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity AcumuladorDTO entity to update
	 @return AcumuladorDTO the persisted AcumuladorDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public AcumuladorDTO update(AcumuladorDTO entity);
	public AcumuladorDTO findById( BigDecimal id);
	 /**
	 * Find all AcumuladorDTO entities with a specific property value.  
	 
	  @param propertyName the name of the AcumuladorDTO property to query
	  @param value the property value to match
	  	  @return List<AcumuladorDTO> found by query
	 */
	public List<AcumuladorDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all AcumuladorDTO entities.
	  	  @return List<AcumuladorDTO> all AcumuladorDTO entities
	 */
	public List<AcumuladorDTO> findAll(
		);	
	
	/**
	 * Obtener el acumulador basado en el movimiento a acumular y el estado de cuenta al cu�l se asocia.
	 * Si el acumulador no es encontrado regresar null 
	 * @param movimiento
	 * @param estadoCuenta
	 * @return AcumuladorDTO
	 */
	AcumuladorDTO obtenerAcumuladorPorMovimientoYEstadoCuenta(
							MovimientoReaseguroDTO movimiento, 
							EstadoCuentaDTO estadoCuenta);
	
	/**
	 * This method set all AcumuladorDTO entities to Cargo = 0 and Abono = 0
	 */
	public void reestablecerAcumuladores(Integer mes,Integer anio);
	
	/**
	 * This method regenerate all the acumuladorDTO for each movientoReaseguroDTO passed in the list
	 */
	public String procesarLoteMovimientosARegenerar(List<MovimientoReaseguroDTO> movimientoReaseguroDTOList,Boolean restarCantidad);
	
	public List<BigDecimal> obtenerIdsAcumuladores(Integer mes,Integer anio);
}