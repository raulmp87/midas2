package mx.com.afirme.midas.contratos.estadocuenta.presentacion;
// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDTO;

/**
 * Remote interface for AgrupacionEstadoCuentaDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface AgrupacionEstadoCuentaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved AgrupacionEstadoCuentaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity AgrupacionEstadoCuentaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(AgrupacionEstadoCuentaDTO entity);
    /**
	 Delete a persistent AgrupacionEstadoCuentaDTO entity.
	  @param entity AgrupacionEstadoCuentaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(AgrupacionEstadoCuentaDTO entity);
   /**
	 Persist a previously saved AgrupacionEstadoCuentaDTO entity and return it or a copy of it to the sender. 
	 A copy of the AgrupacionEstadoCuentaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity AgrupacionEstadoCuentaDTO entity to update
	 @return AgrupacionEstadoCuentaDTO the persisted AgrupacionEstadoCuentaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public AgrupacionEstadoCuentaDTO update(AgrupacionEstadoCuentaDTO entity);
	public AgrupacionEstadoCuentaDTO findById( Integer id);
	 /**
	 * Find all AgrupacionEstadoCuentaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the AgrupacionEstadoCuentaDTO property to query
	  @param value the property value to match
	  	  @return List<AgrupacionEstadoCuentaDTO> found by query
	 */
	public List<AgrupacionEstadoCuentaDTO> findByProperty(String propertyName, Object value);
	/**
	 * Find all AgrupacionEstadoCuentaDTO entities.
	  	  @return List<AgrupacionEstadoCuentaDTO> all AgrupacionEstadoCuentaDTO entities
	 */
	public List<AgrupacionEstadoCuentaDTO> findAll(
		);
	
	public List<AgrupacionEstadoCuentaDTO> listarFiltrado(AgrupacionEstadoCuentaDTO agrupacionEstadoCuentaDTO);
	
	public List<AgrupacionEstadoCuentaDTO> obtenerAgrupacionesPorEstadoCuenta(EstadoCuentaDTO estadoCuentaDTO,Short claveFormatoEstadoCuenta);
	
	public List<AgrupacionEstadoCuentaDTO> obtenerAgrupacionesFacultativo(short claveFormatoEstadoCuenta,short idMoneda);
	
	public List<AgrupacionEstadoCuentaDTO> obtenerAgrupaciones(int tipoReaseguro,int idMoneda,Short claveFormatoEstadoCuenta);
}