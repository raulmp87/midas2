package mx.com.afirme.midas.contratos.estadocuenta.reportes;

import java.io.Serializable;
import java.math.BigDecimal;

public class ReporteSaldoPorNegocioFacultativoDTO implements Serializable {
	private static final long serialVersionUID = 9145007111654991055L;

	private BigDecimal idTcSubRamo;
	private String descripcionSubRamo;
	private String descripcionCobertura;
	private BigDecimal idToPoliza;
	private Integer numeroEndoso;
	private String numeroPoliza;
	private String descripcionTipoEndoso;
	private Integer idMoneda;
	private String descripcionMoneda;
	private String nombreAsegurado;
	private String numeroAceptacion;
	private BigDecimal montoPrimaFacultada;
	private BigDecimal porcentajeParticipacion;
	private BigDecimal porcentajeComision;
	private BigDecimal montoPrimaParticipacion;
	private BigDecimal montoComision;
	private BigDecimal montoPrimaAPagar;
	
	public ReporteSaldoPorNegocioFacultativoDTO(){
		numeroAceptacion = "";
	}
	
	public BigDecimal getIdTcSubRamo() {
		return idTcSubRamo;
	}
	public void setIdTcSubRamo(BigDecimal idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}
	public String getDescripcionSubRamo() {
		return descripcionSubRamo;
	}
	public void setDescripcionSubRamo(String descripcionSubRamo) {
		this.descripcionSubRamo = descripcionSubRamo;
	}
	public String getDescripcionCobertura() {
		return descripcionCobertura;
	}
	public void setDescripcionCobertura(String descripcionCobertura) {
		this.descripcionCobertura = descripcionCobertura;
	}
	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}
	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}
	public Integer getNumeroEndoso() {
		return numeroEndoso;
	}
	public void setNumeroEndoso(Integer numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public Integer getIdMoneda() {
		return idMoneda;
	}
	public void setIdMoneda(Integer idMoneda) {
		this.idMoneda = idMoneda;
	}
	public String getDescripcionMoneda() {
		return descripcionMoneda;
	}
	public void setDescripcionMoneda(String descripcionMoneda) {
		this.descripcionMoneda = descripcionMoneda;
	}
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}
	public String getNumeroAceptacion() {
		return numeroAceptacion;
	}
	public void setNumeroAceptacion(String numeroAceptacion) {
		this.numeroAceptacion = numeroAceptacion;
	}
	public BigDecimal getMontoPrimaFacultada() {
		return montoPrimaFacultada;
	}
	public void setMontoPrimaFacultada(BigDecimal montoPrimaFacultada) {
		this.montoPrimaFacultada = montoPrimaFacultada;
	}
	public BigDecimal getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}
	public void setPorcentajeParticipacion(BigDecimal porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}
	public BigDecimal getPorcentajeComision() {
		return porcentajeComision;
	}
	public void setPorcentajeComision(BigDecimal porcentajeComision) {
		this.porcentajeComision = porcentajeComision;
	}
	public BigDecimal getMontoPrimaParticipacion() {
		return montoPrimaParticipacion;
	}
	public void setMontoPrimaParticipacion(BigDecimal montoPrimaParticipacion) {
		this.montoPrimaParticipacion = montoPrimaParticipacion;
	}
	public BigDecimal getMontoComision() {
		return montoComision;
	}
	public void setMontoComision(BigDecimal montoComision) {
		this.montoComision = montoComision;
	}
	public BigDecimal getMontoPrimaAPagar() {
		return montoPrimaAPagar;
	}
	public void setMontoPrimaAPagar(BigDecimal montoPrimaAPagar) {
		this.montoPrimaAPagar = montoPrimaAPagar;
	}
	public String getDescripcionTipoEndoso() {
		return descripcionTipoEndoso;
	}
	public void setDescripcionTipoEndoso(String descripcionTipoEndoso) {
		this.descripcionTipoEndoso = descripcionTipoEndoso;
	}
}
