package mx.com.afirme.midas.contratos.estadocuenta.presentacion;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDecoradoDTO;


public interface PresentacionEstadoCuentaServiciosRemote {
	
//	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaConSaldosCalculados(EstadoCuentaDTO estadoCuentaDTO,
//			short claveFormatoEstadoCuenta,List<AgrupacionEstadoCuentaDTO> listaAgrupaciones,boolean incluirFiltroAcumuladores,Integer mes,Integer anio,Boolean mostrarSaldoAcumuladoPorAgrupacion);
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaConSaldosCalculados(BigDecimal idtoEstadoCuenta,short claveFormatoEstadoCuenta);
	
	public List<EstadoCuentaDecoradoDTO> obtenerEstadoCuentaCombinadoConSaldosCalculados(BigDecimal idToPoliza,short claveFormatoEstadoCuenta);
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaCombinadoConSaldosCalculados(BigDecimal idToPoliza,BigDecimal idTcReasegurador,BigDecimal idMoneda,BigDecimal idTcSubRamo,
			boolean incluirFiltroPorMes,List<Date> mesesPorIncluir, boolean incluirFiltradoHasta,Integer mes,Integer anio,boolean separarConceptosPorReasegurador,short claveFormatoEstadoCuenta);
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaCombinadoConSaldosCalculados(BigDecimal idToPoliza,BigDecimal[] idTcSubRamo,
			BigDecimal[] idTmContratoFacultativo,Integer mes,Integer anio,
			boolean separarConceptosPorReasegurador,short claveFormatoEstadoCuenta);
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaPorSiniestro(BigDecimal idToReporteSiniestro,BigDecimal idMoneda,BigDecimal[] idTcSubRamos,Integer mes,Integer anio,boolean separarConceptosPorReasegurador,Short claveFormatoEstadoCuenta);
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaPorReasegurador(BigDecimal idTcReasegurador,BigDecimal idMoneda,BigDecimal idToSiniestro,Integer mes,Integer anio,BigDecimal idTcSubRamo,
			Short claveFormatoEstadoCuenta);
	
	/**
	 * Consulta un estado de cuenta, correspondiente a una suscripci�n, y los estados de cuenta de suscripciones anteriores, adem�s consulta el estado 
	 * de cuenta de los contratos cuota parte � primer excedente de a�os contractuales anteriores y combina los saldos en un estado de cuenta �nico.
	 * @param idToEstadoCuenta
	 * @return
	 */
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaHistoricoCombinado(BigDecimal idToEstadoCuenta,short claveFormatoEstadoCuenta);
	
//	/**
//	 * Consulta un estado de cuenta, correspondiente a una suscripci�n, subramo, reasegurador, moneda, ejercicio y tipo de reaseguro, as� como los estados 
//	 * de cuenta de suscripciones anteriores, adem�s consulta los saldos de los contratos cuota parte � primer excedente de a�os contractuales anteriores 
//	 * y combina los saldos en un estado de cuenta �nico.
//	 * @param idTcSubRamo
//	 * @param idTcReasegurador
//	 * @param idMoneda
//	 * @param idTcTipoReaseguro
//	 * @param ejercicio
//	 * @param suscripcion
//	 * @return
//	 */
//	public EstadoCuentaDecoradoDTO obtenerEstadosCuentaHistoricosCombinado(BigDecimal idTcSubRamo, BigDecimal idTcReasegurador,
//			BigDecimal idMoneda,int idTcTipoReaseguro,int ejercicio, int suscripcion,short claveFormatoEstadoCuenta,boolean separarSaldoPorMeses);
	
	public List<EstadoCuentaDecoradoDTO> obtenerEstadosCuentaSeparados(BigDecimal idTcSubRamo, BigDecimal idTcReasegurador,
			BigDecimal idMoneda,int idTcTipoReaseguro,int ejercicio, int suscripcion,short claveFormatoEstadoCuenta,boolean desglosarSaldoPorMes,short claveFormatoEstadoCuentaDesglosadoPorMes);
	
	public List<EstadoCuentaDecoradoDTO> obtenerEstadoCuentaFacultativoPorSuscripcion(BigDecimal idTcReasegurador,BigDecimal idMoneda,int ejercicio, int suscripcion);
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaFacultativoPorSuscripcion(BigDecimal idTcReasegurador,BigDecimal idMoneda,BigDecimal idTcSubRamo,int ejercicio, int suscripcion);
}
