package mx.com.afirme.midas.contratos.estadocuenta;

import java.io.Serializable;
import java.math.BigDecimal;

public class SaldoEgresoDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private BigDecimal monto;
	
	private BigDecimal idestadocuenta;
	
	

	public BigDecimal getMonto() {
		return monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	public BigDecimal getIdestadocuenta() {
		return idestadocuenta;
	}

	public void setIdestadocuenta(BigDecimal idestadocuenta) {
		this.idestadocuenta = idestadocuenta;
	}	
}