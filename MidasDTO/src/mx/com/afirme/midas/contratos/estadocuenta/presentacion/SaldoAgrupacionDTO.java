package mx.com.afirme.midas.contratos.estadocuenta.presentacion;

import java.math.BigDecimal;
import java.text.NumberFormat;

import mx.com.afirme.midas.contratos.estadocuenta.SaldoConceptoDTO;

public class SaldoAgrupacionDTO extends SaldoConceptoDTO {
	private static final long serialVersionUID = -6575626151300291898L;
	
	private BigDecimal totalDebe;
	private BigDecimal totalHaber;
	private BigDecimal totalSaldo;
	
	public SaldoAgrupacionDTO() {
	}
	
	public SaldoAgrupacionDTO(String nombre, String fechaTrimestre, BigDecimal saldoAcumulado, BigDecimal saldo,
			BigDecimal totalDebe, BigDecimal totalHaber, BigDecimal totalSaldo) {
		super(nombre, fechaTrimestre,null, BigDecimal.ZERO, BigDecimal.ZERO, saldoAcumulado, saldo);
		this.totalDebe = totalDebe;
		this.totalHaber = totalHaber;
		this.totalSaldo = totalSaldo;
	}
	public BigDecimal getTotalDebe() {
		return totalDebe;
	}
	public void setTotalDebe(BigDecimal totalDebe) {
		this.totalDebe = totalDebe;
	}
	public BigDecimal getTotalHaber() {
		return totalHaber;
	}
	public void setTotalHaber(BigDecimal totalHaber) {
		this.totalHaber = totalHaber;
	}

	public BigDecimal getTotalSaldo() {
		return totalSaldo;
	}

	public void setTotalSaldo(BigDecimal totalSaldo) {
		this.totalSaldo = totalSaldo;
	}
	
	public String getEtiquetaDebe() {
		String resultado = "";
		if(getTotalDebe() != null && getTotalDebe().compareTo(BigDecimal.ZERO) != 0){
			NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
			resultado = currencyFormatter.format(getTotalDebe());
		}
		return resultado;
	}

	public void setEtiquetaDebe(String etiquetaDebe) {
		
	}

	public String getEtiquetaHaber() {
		String resultado = "";
		if(getTotalHaber() != null && getTotalHaber().compareTo(BigDecimal.ZERO) != 0){
			NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
			resultado = currencyFormatter.format(getTotalHaber());
		}
		return resultado;
	}

	public void setEtiquetaHaber(String etiquetaHaber) {
		
	}

	public String getEtiquetaSaldo() {
		String resultado = "";
		if(getTotalSaldo() != null){
			NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
			resultado = currencyFormatter.format(getTotalSaldo());
		}
		return resultado;
	}

	public void setEtiquetaSaldo(String etiquetaSaldo) {
		
	}
}
