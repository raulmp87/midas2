package mx.com.afirme.midas.contratos.estadocuenta.presentacion;
// default package

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.contratos.movimiento.ConceptoMovimientoDTO;


/**
 * DetalleConfiguracionEstadoCuentaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCDETALLECONFIGESTADOCUENTA", schema="MIDAS")
public class DetalleConfiguracionEstadoCuentaDTO  implements java.io.Serializable {
	public static final int CLAVE_OPERACION_SUMA = 1;
	public static final int CLAVE_OPERACION_RESTA = 2;
	private static final long serialVersionUID = -8852488003011311862L;
	private Integer idTcDetalleConfigEstadoCuenta;
    private ConfiguracionConceptoEstadoCuentaDTO configuracionConceptoEstadoCuentaDTO;
    private Integer claveTipoOperacion;
    private ConceptoMovimientoDTO conceptoMovimientoDTO;
    private Integer claveAplicaConceptoDetalle;
    private BigDecimal idTcConceptoDetalle;

    // Constructors

    /** default constructor */
    public DetalleConfiguracionEstadoCuentaDTO() {
    }

    
    /** full constructor */
    public DetalleConfiguracionEstadoCuentaDTO(Integer idTcDetalleConfigEstadoCuenta, ConfiguracionConceptoEstadoCuentaDTO configuracionConceptoEstadoCuentaDTO, Integer claveTipoOperacion) {
        this.idTcDetalleConfigEstadoCuenta = idTcDetalleConfigEstadoCuenta;
        this.configuracionConceptoEstadoCuentaDTO = configuracionConceptoEstadoCuentaDTO;
        this.claveTipoOperacion = claveTipoOperacion;
    }

   
    // Property accessors
    @Id     
    @Column(name="IDTCDETALLECONFIGEDOCTA", nullable=false, precision=8, scale=0)
    public Integer getIdTcDetalleConfigEstadoCuenta() {
		return idTcDetalleConfigEstadoCuenta;
	}

	public void setIdTcDetalleConfigEstadoCuenta(Integer idTcDetalleConfigEstadoCuenta) {
		this.idTcDetalleConfigEstadoCuenta = idTcDetalleConfigEstadoCuenta;
	}
    
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="IDTCCONFIGCONCEPTO", nullable=false)
    public ConfiguracionConceptoEstadoCuentaDTO getConfiguracionConceptoEstadoCuentaDTO() {
        return this.configuracionConceptoEstadoCuentaDTO;
    }
    
    public void setConfiguracionConceptoEstadoCuentaDTO(ConfiguracionConceptoEstadoCuentaDTO configuracionConceptoEstadoCuentaDTO) {
        this.configuracionConceptoEstadoCuentaDTO = configuracionConceptoEstadoCuentaDTO;
    }
    
    @Column(name="CLAVETIPOOPERACION", nullable=false, precision=1, scale=0)
    public Integer getClaveTipoOperacion() {
		return claveTipoOperacion;
	}

	public void setClaveTipoOperacion(Integer claveTipoOperacion) {
		this.claveTipoOperacion = claveTipoOperacion;
	}
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="IDTCCONCEPTOMOVIMIENTO", nullable=false, insertable=false, updatable=false)
    public ConceptoMovimientoDTO getConceptoMovimientoDTO() {
        return this.conceptoMovimientoDTO;
    }
    
    public void setConceptoMovimientoDTO(ConceptoMovimientoDTO conceptoMovimientoDTO) {
        this.conceptoMovimientoDTO = conceptoMovimientoDTO;
    }

    @Column(name="CLAVEAPLICACONCEPTODETALLE", nullable=false, precision=1, scale=0)
	public Integer getClaveAplicaConceptoDetalle() {
		return claveAplicaConceptoDetalle;
	}

	public void setClaveAplicaConceptoDetalle(Integer claveAplicaConceptoDetalle) {
		this.claveAplicaConceptoDetalle = claveAplicaConceptoDetalle;
	}

	@Column(name="IDTCCONCEPTODETALLE", nullable=true, precision=2, scale=0)
	public BigDecimal getIdTcConceptoDetalle() {
		return idTcConceptoDetalle;
	}

	public void setIdTcConceptoDetalle(BigDecimal idTcConceptoDetalle) {
		this.idTcConceptoDetalle = idTcConceptoDetalle;
	}
}