package mx.com.afirme.midas.contratos.estadocuenta.presentacion;
// default package

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * AgrupacionEstadoCuentaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCAGRUPACIONEDOCTA", schema="MIDAS")
public class AgrupacionEstadoCuentaDTO  implements java.io.Serializable {

	//Constantes
	public static final Short TIPO_FACULTATIVO = 1;
	public static final Short TIPO_AUTOMATICO = 2;
	public static final Short MOSTRAR_ENCABEZADO = 1;
	public static final Short OCULTAR_ENCABEZADO= 0;
	public static final Short MOSTRAR_SUBTOTAL = 1;
	public static final Short OCULTAR_SUBTOTAL = 0;
	public static final Short CLAVE_FORMATO_DEFAULT_FACULTATIVO = (short)1;
	public static final Short CLAVE_FORMATO_DEFAULT_AUTOMATICO = (short)1;
	public static final Short CLAVE_FORMATO_FACULTATIVO_POR_SINIESTRO = (short)3;
	public static final Short CLAVE_FORMATO_FACULTATIVO_POR_POLIZA = (short)2;
	public static final Short CLAVE_FORMATO_FACULTATIVO_CONSULTA_DESDE_AUTOMATICOS_PRIMA_COMISION_SINIESTRO = (short)4;
	public static final Short CLAVE_FORMATO_FACULTATIVO_CONSULTA_DESDE_AUTOMATICOS_COMPLETO = (short)5;
	public static final Short CLAVE_FORMATO_AUTOMATICOS_CONSULTA_PRIMA_COMISION_SINIESTRO = (short)2;
	public static final Short CLAVE_FORMATO_AUTOMATICOS_COMPLETO = (short)3;
	
	//Variables de instancia
	private static final long serialVersionUID = 4044257700644598903L;
	private Integer idTcAgrupacionEstadoCuenta;
	private Short claveTipoEstadoCuenta;
	private Short claveFormatoEstadoCuenta;
	private Short idMoneda;
	private String nombreAgrupacion;
	private Short orden;
	private Short claveMostrarEncabezado;
	private Short claveMostrarSubTotal;
	private List<ConfiguracionConceptoEstadoCuentaDTO> configuracionConceptoEstadoCuentaList = new ArrayList<ConfiguracionConceptoEstadoCuentaDTO>();
	
	public static enum TipoEstadoCuenta{
		Facultativo(1),Automatico(2);
	
		private int valor;
		
		TipoEstadoCuenta(int valor){
			this.valor = valor;
		}
		
		public int getValor(){return valor;}
	};
	
	public static enum FormatoEstadoCuenta{
		FacultativoReaseguro((short)1),AutomaticoReaseguro((short)2),FacultativoContabilidad((short)3);
		
		private short valor;
		
		FormatoEstadoCuenta(short valor){
			this.valor = valor;
		}
		
		public short getValor(){return valor;}
	};
	

    /** default constructor */
    public AgrupacionEstadoCuentaDTO() {
    }

	/** minimal constructor */
    public AgrupacionEstadoCuentaDTO(Integer idTcAgrupacionEstadoCuenta, Short claveTipoEstadoCuenta, Short claveFormatoEstadoCuenta, Short idMoneda, Short orden, Short claveMostrarEncabezado, Short claveMostrarSubTotal) {
        this.idTcAgrupacionEstadoCuenta = idTcAgrupacionEstadoCuenta;
        this.claveTipoEstadoCuenta = claveTipoEstadoCuenta;
        this.claveFormatoEstadoCuenta = claveFormatoEstadoCuenta;
        this.idMoneda = idMoneda;
        this.orden = orden;
        this.claveMostrarEncabezado = claveMostrarEncabezado;
        this.claveMostrarSubTotal = claveMostrarSubTotal;
    }
    
    /** full constructor */
    public AgrupacionEstadoCuentaDTO(Integer idTcAgrupacionEstadoCuenta, Short claveTipoEstadoCuenta, Short claveFormatoEstadoCuenta, Short idMoneda, String nombreAgrupacion, Short orden, Short claveMostrarEncabezado, Short claveMostrarSubTotal) {
        this.idTcAgrupacionEstadoCuenta = idTcAgrupacionEstadoCuenta;
        this.claveTipoEstadoCuenta = claveTipoEstadoCuenta;
        this.claveFormatoEstadoCuenta = claveFormatoEstadoCuenta;
        this.idMoneda = idMoneda;
        this.nombreAgrupacion = nombreAgrupacion;
        this.orden = orden;
        this.claveMostrarEncabezado = claveMostrarEncabezado;
        this.claveMostrarSubTotal = claveMostrarSubTotal;
    }

   
    // Property accessors
    @Id     
    @Column(name="IDTCAGRUPACIONEDOCTA", nullable=false, precision=8, scale=0)
    public Integer getIdTcAgrupacionEstadoCuenta() {
        return this.idTcAgrupacionEstadoCuenta;
    }
    
    public void setIdTcAgrupacionEstadoCuenta(Integer idTcAgrupacionEstadoCuenta) {
        this.idTcAgrupacionEstadoCuenta = idTcAgrupacionEstadoCuenta;
    }
    
    @Column(name="CLAVETIPOESTADOCUENTA", nullable=false, precision=4, scale=0)
    public Short getClaveTipoEstadoCuenta() {
        return this.claveTipoEstadoCuenta;
    }
    
    public void setClaveTipoEstadoCuenta(Short claveTipoEstadoCuenta) {
        this.claveTipoEstadoCuenta = claveTipoEstadoCuenta;
    }
    
    @Column(name="CLAVEFORMATOEDOCTA", nullable=false, precision=4, scale=0)
    public Short getClaveFormatoEstadoCuenta() {
        return this.claveFormatoEstadoCuenta;
    }
    
    public void setClaveFormatoEstadoCuenta(Short claveFormatoEstadoCuenta) {
        this.claveFormatoEstadoCuenta = claveFormatoEstadoCuenta;
    }
    
    @Column(name="IDMONEDA", nullable=false, precision=3, scale=0)
    public Short getIdMoneda() {
        return this.idMoneda;
    }
    
    public void setIdMoneda(Short idMoneda) {
        this.idMoneda = idMoneda;
    }
    
    @Column(name="NOMBREAGRUPACION", length=100)
    public String getNombreAgrupacion() {
        return this.nombreAgrupacion;
    }
    
    public void setNombreAgrupacion(String nombreAgrupacion) {
        this.nombreAgrupacion = nombreAgrupacion;
    }
    
    @Column(name="ORDEN", nullable=false, precision=4, scale=0)
    public Short getOrden() {
        return this.orden;
    }
    
    public void setOrden(Short orden) {
        this.orden = orden;
    }
    
    @Column(name="CLAVEMOSTRARENCABEZADO", nullable=false, precision=1, scale=0)
    public Short getClaveMostrarEncabezado() {
        return this.claveMostrarEncabezado;
    }
    
    public void setClaveMostrarEncabezado(Short claveMostrarEncabezado) {
        this.claveMostrarEncabezado = claveMostrarEncabezado;
    }
    
    @Column(name="CLAVEMOSTRARSUBTOTAL", nullable=false, precision=1, scale=0)
    public Short getClaveMostrarSubTotal() {
        return this.claveMostrarSubTotal;
    }
    
    public void setClaveMostrarSubTotal(Short claveMostrarSubTotal) {
        this.claveMostrarSubTotal = claveMostrarSubTotal;
    }

    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="agrupacionEstadoCuentaDTO")
	public List<ConfiguracionConceptoEstadoCuentaDTO> getConfiguracionConceptoEstadoCuentaList() {
		return configuracionConceptoEstadoCuentaList;
	}

	public void setConfiguracionConceptoEstadoCuentaList(List<ConfiguracionConceptoEstadoCuentaDTO> configuracionConceptoEstadoCuentaList) {
		this.configuracionConceptoEstadoCuentaList = configuracionConceptoEstadoCuentaList;
	}
}