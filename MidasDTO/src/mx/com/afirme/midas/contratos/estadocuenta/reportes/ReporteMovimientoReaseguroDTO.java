package mx.com.afirme.midas.contratos.estadocuenta.reportes;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;

public class ReporteMovimientoReaseguroDTO implements Serializable {
	private static final long serialVersionUID = -2795541908846949836L;

	private String nombreReasegurador;
	private String nombreCorredor;
	private String numeroPoliza;
	private Integer numeroEndoso;
	private String tipoEndoso;
	private Date fechaEmision;
	private String fechaEmisionFormateada;
	private Date fechaInicioVigencia;
	private Date fechaFinVigencia;
	private String nombreAsegurado;
	private Date fechaMovimiento;
	private String codigoSubRamo;
	private BigDecimal primaCedida;
	private BigDecimal comisiones;
	private BigDecimal primaMenosComision;
	private BigDecimal ingresosDevolucionDePrima;
	private BigDecimal ingresosBonoPorNoSiniestro;
	private BigDecimal pagos;
	private BigDecimal impuestos;
	private BigDecimal saldoAPagar;
	private BigDecimal tipoCambio;
	private Integer idMoneda;
	private String mesMovimiento;
	private String descripcionMoneda;
	private String numeroSiniestro;
	private Date fechaOcurrenciaSiniestro;
	private String estatusSiniestro;
	private String tipoContrato;
	private String identificador;
	private String descripcionCobertura;
	private String descripcionSeccion;
	private Integer numeroInciso;
	private Integer numeroSubInciso;
	//Saldos de la cuenta por cobrar(siniestros)
	private BigDecimal cesionSiniestro;
	private BigDecimal cobros;
	private BigDecimal saldoPorCobrar;
	private String descripcionConcepto;
	
	
	public String getNombreReasegurador() {
		return nombreReasegurador;
	}
	public void setNombreReasegurador(String nombreReasegurador) {
		this.nombreReasegurador = nombreReasegurador;
	}
	public String getNombreCorredor() {
		return nombreCorredor;
	}
	public void setNombreCorredor(String nombreCorredor) {
		this.nombreCorredor = nombreCorredor;
	}
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public Integer getNumeroEndoso() {
		return numeroEndoso;
	}
	public void setNumeroEndoso(Integer numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}
	public String getTipoEndoso() {
		return tipoEndoso;
	}
	public void setTipoEndoso(String tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}
	public Date getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public String getFechaEmisionFormateada() {
		return fechaEmisionFormateada;
	}
	public void setFechaEmisionFormateada(String fechaEmisionFormateada) {
		this.fechaEmisionFormateada = fechaEmisionFormateada;
	}
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}
	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}
	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}
	public Date getFechaMovimiento() {
		return fechaMovimiento;
	}
	public void setFechaMovimiento(Date fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
	}
	public String getCodigoSubRamo() {
		return codigoSubRamo;
	}
	public void setCodigoSubRamo(String codigoSubRamo) {
		this.codigoSubRamo = codigoSubRamo;
	}
	public BigDecimal getPrimaCedida() {
		return primaCedida;
	}
	public void setPrimaCedida(BigDecimal primaCedida) {
		this.primaCedida = primaCedida;
		calcularPrimaMenosComision();
	}
	public BigDecimal getComisiones() {
		return comisiones;
	}
	public void setComisiones(BigDecimal comisiones) {
		this.comisiones = comisiones;
		calcularPrimaMenosComision();
	}
	public BigDecimal getPrimaMenosComision() {
		return primaMenosComision;
	}
	public void setPrimaMenosComision(BigDecimal primaMenosComision) {
		this.primaMenosComision = primaMenosComision;
	}
	public BigDecimal getIngresosDevolucionDePrima() {
		return ingresosDevolucionDePrima;
	}
	public void setIngresosDevolucionDePrima(BigDecimal ingresosDevolucionDePrima) {
		this.ingresosDevolucionDePrima = ingresosDevolucionDePrima;
	}
	public BigDecimal getIngresosBonoPorNoSiniestro() {
		return ingresosBonoPorNoSiniestro;
	}
	public void setIngresosBonoPorNoSiniestro(BigDecimal ingresosBonoPorNoSiniestro) {
		this.ingresosBonoPorNoSiniestro = ingresosBonoPorNoSiniestro;
	}
	public BigDecimal getPagos() {
		return pagos;
	}
	public void setPagos(BigDecimal pagos) {
		this.pagos = pagos;
	}
	public BigDecimal getImpuestos() {
		return impuestos;
	}
	public void setImpuestos(BigDecimal impuestos) {
		this.impuestos = impuestos;
	}
	public BigDecimal getSaldoAPagar() {
		return saldoAPagar;
	}
	public void setSaldoAPagar(BigDecimal saldoAPagar) {
		this.saldoAPagar = saldoAPagar;
	}
	public BigDecimal getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(BigDecimal tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public Integer getIdMoneda() {
		return idMoneda;
	}
	public void setIdMoneda(Integer idMoneda) {
		this.idMoneda = idMoneda;
		if(idMoneda != null){
			if(idMoneda == MonedaDTO.MONEDA_DOLARES)
				descripcionMoneda = "USD";
			else if(idMoneda == MonedaDTO.MONEDA_PESOS)
				descripcionMoneda = "Pesos";
		}
	}
	public String getMesMovimiento() {
		if(fechaMovimiento != null && mesMovimiento == null){
			String[] meses = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String mes = "";
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(fechaMovimiento);
			int indiceMes = calendar.get(Calendar.MONTH);
			mes = meses[indiceMes];
			mesMovimiento = mes;
		}
		return mesMovimiento;
	}
	public void setMesMovimiento(String mesMovimiento) {
		this.mesMovimiento = mesMovimiento;
	}
	private void calcularPrimaMenosComision(){
		if(primaCedida != null && comisiones != null){
			this.primaMenosComision = primaCedida.subtract(comisiones);
		}
	}
	public String getDescripcionMoneda() {
		return descripcionMoneda;
	}
	public void setDescripcionMoneda(String descripcionMoneda) {
		this.descripcionMoneda = descripcionMoneda;
	}
	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}
	public Date getFechaOcurrenciaSiniestro() {
		return fechaOcurrenciaSiniestro;
	}
	public void setFechaOcurrenciaSiniestro(Date fechaOcurrenciaSiniestro) {
		this.fechaOcurrenciaSiniestro = fechaOcurrenciaSiniestro;
	}
	public String getEstatusSiniestro() {
		return estatusSiniestro;
	}
	public void setEstatusSiniestro(String estatusSiniestro) {
		this.estatusSiniestro = estatusSiniestro;
	}
	public BigDecimal getCesionSiniestro() {
		return cesionSiniestro;
	}
	public void setCesionSiniestro(BigDecimal cesionSiniestro) {
		this.cesionSiniestro = cesionSiniestro;
		calcularSaldoPorCobrar();
	}
	public BigDecimal getCobros() {
		return cobros;
	}
	public void setCobros(BigDecimal cobros) {
		this.cobros = cobros;
		calcularSaldoPorCobrar();
	}
	private void calcularSaldoPorCobrar(){
		if(this.cobros != null && this.cesionSiniestro != null)
			this.saldoPorCobrar = this.cesionSiniestro.subtract(this.cobros);
	}
	public BigDecimal getSaldoPorCobrar() {
		return saldoPorCobrar;
	}
	public void setSaldoPorCobrar(BigDecimal saldoPorCobrar) {
		this.saldoPorCobrar = saldoPorCobrar;
	}
	public String getDescripcionConcepto() {
		return descripcionConcepto;
	}
	public void setDescripcionConcepto(String descripcionConcepto) {
		this.descripcionConcepto = descripcionConcepto;
	}
	public String getTipoContrato() {
		return tipoContrato;
	}
	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public String getDescripcionCobertura() {
		return descripcionCobertura;
	}
	public void setDescripcionCobertura(String descripcionCobertura) {
		this.descripcionCobertura = descripcionCobertura;
	}
	public String getDescripcionSeccion() {
		return descripcionSeccion;
	}
	public void setDescripcionSeccion(String descripcionSeccion) {
		this.descripcionSeccion = descripcionSeccion;
	}
	public Integer getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public Integer getNumeroSubInciso() {
		return numeroSubInciso;
	}
	public void setNumeroSubInciso(Integer numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}
	
}
