package mx.com.afirme.midas.contratos.estadocuenta;
// default package

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.catalogos.cuentabanco.CuentaBancoDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDTO;
import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.contratos.movimiento.MovimientoReaseguroDTO;
import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;

/**
 * Remote interface for EstadoCuentaDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface EstadoCuentaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved EstadoCuentaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity EstadoCuentaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(EstadoCuentaDTO entity);
    /**
	 Delete a persistent EstadoCuentaDTO entity.
	  @param entity EstadoCuentaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(EstadoCuentaDTO entity);
   /**
	 Persist a previously saved EstadoCuentaDTO entity and return it or a copy of it to the sender. 
	 A copy of the EstadoCuentaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity EstadoCuentaDTO entity to update
	 @return EstadoCuentaDTO the persisted EstadoCuentaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public EstadoCuentaDTO update(EstadoCuentaDTO entity);
	public EstadoCuentaDTO findById( BigDecimal id);
	 /**
	 * Find all EstadoCuentaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the EstadoCuentaDTO property to query
	  @param value the property value to match
	  	  @return List<EstadoCuentaDTO> found by query
	 */
	public List<EstadoCuentaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all EstadoCuentaDTO entities.
	  	  @return List<EstadoCuentaDTO> all EstadoCuentaDTO entities
	 */
	public List<EstadoCuentaDTO> findAll(
		);	
	
	
	/**
	 * Determina el estado de cuenta asociado para el movimiento de reaseguro indicado.
	 * En caso de no exitir un estado de cuenta asociado regresar null
	 * @param movimiento
	 * @return EstadoCuentaDTO
	 */
	EstadoCuentaDTO obtenerEstadoCuentaPorMovimiento(MovimientoReaseguroDTO movimiento);
	
	/**
	 * 
	 * @param reasegurador
	 * @return
	 */
	List<EstadoCuentaDTO> obtenerEstadosCuenta(ReaseguradorCorredorDTO reasegurador);
	
	/**
	 * 
	 * @param reasegurador
	 * @return
	 */
	List<EstadoCuentaDTO> obtenerEstadosCuenta(ReaseguradorCorredorDTO reasegurador, Date fechaInicio, Date fechaFin);	
	
	
	/**
	 * 
	 * @param EstadoCuentaFiltroDTO
	 * @return List<EstadoCuentaDTO>
	 */
	public List<EstadoCuentaDTO> obtenerEstadosCuenta(EstadoCuentaFiltroDTO  filtro);
	
	/**
	 * This method obtains the proper details of a given EstadoCuentaDTO 
	 * @param EstadoCuentaFiltroDTO
	 * @param EstadoCuentaFiltroDTO Map<BigDecimal,List<ConceptoAcumuladorDTO>> mapaConceptosPorMoneda
	 * @return List<EstadoCuentaDTO>
	 */
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaDetalle(BigDecimal idEstadoCuenta,List<ConceptoAcumuladorDTO> listaConceptosPorMoneda,List<AcumuladorDTO> acumuladorDTOList);
	
	/**
	 * This method retrieve a list of accumulated balances for a determined EstadoCuentaDTO
	 * @param EstadoCuentaFiltroDTO
	 * @param EstadoCuentaFiltroDTO Map<BigDecimal,List<ConceptoAcumuladorDTO>> mapaConceptosPorMoneda
	 * @return List<EstadoCuentaDTO>
	 */
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaConSaldosCalculados(EstadoCuentaDTO estadoCuentaDTO,List<ConceptoAcumuladorDTO> listaConceptosPorMoneda,List<AcumuladorDTO> listaAcumuladoresEdoCta);
	

	/**
	 * This method receive a list of PolizaDummyDTO and returns only the PolizaDummyDTO's 
	 * entities which are referenced in SoporteReaseguroDTO
	 * @param EstadoCuentaFiltroDTO
	 * @return List<PolizaDummyDTO>
	 */
	public List<PolizaSoporteDanosDTO> filtrarPolizasFacultativas(List<PolizaSoporteDanosDTO> polizas);
	
	/**
	 * This method returns a filtered list of {@link EstadoCuentaDecoradoDTO} for a {@link PolizaSoporteDanosDTO}
	 * @param {@link EstadoCuentaDecoradoDTO}
	 * @return List<{@link EstadoCuentaDecoradoDTO}>
	 */
	public List<EstadoCuentaDTO> buscarEstadosCuentaFacultativos(PolizaSoporteDanosDTO poliza);
	

	/**
	 * Crea los estados de cuenta correspondientes a las l�neas autorizadas
	 */
	public void crearEstadosCuentaContratosAutomaticos();
	
	
	/**
	 * Crea los estados de cuenta correspondientes a contratos facultativos autorizados.
	 */
	public void crearEstadosCuentaContratosFacultativos();	
	

	/**
	 * Crea los estados de cuenta correspondiente a una l�nea autorizada.
	 * @param linea
	 */
	public void crearEstadosCuentaLinea(LineaDTO linea);
	
	/**
	 * Crea los estados de cuenta correspondiente a un contrato facultativo autorizado.
	 * @param contrato
	 */
	public void crearEstadosCuentaFacultativo(ContratoFacultativoDTO contrato);
	
	/**
	 * Obtiene la lista de estados de cuenta que conforman la serie por ejercicio y suscripci�n para el estado de cuenta indicado.
	 * @param estadoCuenta
	 * @return
	 */
	public List<EstadoCuentaDTO> obtenerSerieEstadosCuenta(EstadoCuentaDTO estadoCuenta);
	
	/**
	 * Obtiene las cuentas de banco relacionadas a un EstadoCuentaDTO
	 * @param estadoCuenta
	 * @return
	 */
	public CuentaBancoDTO obtenerCuentaBanco(EstadoCuentaDecoradoDTO estadoCuenta,Integer idMoneda);
	
	
	/**
	 * obtiene las lineas que entran en vigencia 
	 * @param fecha
	 */
	public List<LineaDTO> obtenerLineasAutorizadas(Date fecha);

	/**
	 * obtiene los contratos vencidos de facultativo 
	 * @param fecha
	 */
	public List<ContratoFacultativoDTO> obtenerContratoAutorizadasFacultativo(Date fecha);
	
	List<ConceptoAcumuladorDTO> obtenerConceptosMovimientos(BigDecimal idMoneda, List<ConceptoAcumuladorDTO> listaConceptosPorMoneda);
	
	List<AcumuladorDTO> obtenerAcumuladoresPorIdEstadoCta(BigDecimal idEstadoCuenta);
	
	public SaldoEgresoDTO obtenerEgresoPorIdEstadoCta(BigDecimal idEstadoCuenta);
	
	public EstadoCuentaDTO obtenerEstadoCuentaHistorico(BigDecimal idEstadoCuenta);
	
	public List<SaldoConceptoDTO> obtenerSaldosIndividuales(EstadoCuentaDTO estadoCuentaDTO,List<ConceptoAcumuladorDTO> listaConceptosPorMoneda,List<AcumuladorDTO> listaAcumuladoresEdoCta,boolean consultaAcumuladores);
	
	public SaldoConceptoDTO obtenerSaldosAcumulados(List<SaldoConceptoDTO> listaSaldosConceptos, SaldoConceptoDTO saldoAnterior);
	
	public List<BigDecimal> filtrarPolizasFacultativasPorReaseguradorMoneda(BigDecimal idTcReasegurador,BigDecimal idMoneda);
	
	public List<BigDecimal> filtrarSiniestrosFacultativosPorReaseguradorMoneda(BigDecimal idTcReasegurador,BigDecimal idMoneda);
	
	public List<EstadoCuentaDTO> obtenerEstadosCuentaPorIdsTipoReaseguro(String[] idsEdosCta, int[] tiposReaseguro);
}