package mx.com.afirme.midas.contratos.estadocuenta;
// default package


import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.contratos.movimiento.ConceptoMovimientoDTO;


/**
 * ConceptoAcumuladorDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TDCONCEPTOACUMULADOR"
    ,schema="MIDAS"
)
public class ConceptoAcumuladorDTO  implements java.io.Serializable {


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields    

     private ConceptoAcumuladorDTOId id;
     private ConceptoMovimientoDTO conceptoMovimientoDTO;
     private int orden;


    // Constructors

    /** default constructor */
    public ConceptoAcumuladorDTO() {
    }

   
    // Property accessors
    @EmbeddedId
    @AttributeOverrides( {
        @AttributeOverride(name="idConceptoMovimiento", column=@Column(name="IDTCCONCEPTOMOVIMIENTO", nullable=false, precision=2, scale=0) ), 
        @AttributeOverride(name="idMoneda", column=@Column(name="IDMONEDA", nullable=false, precision=3, scale=0) ) } )
    public ConceptoAcumuladorDTOId getId() {
        return this.id;
    }
    
    public void setId(ConceptoAcumuladorDTOId id) {
        this.id = id;
    }
	
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="IDTCCONCEPTOMOVIMIENTO", nullable=false, insertable=false, updatable=false)
    public ConceptoMovimientoDTO getConceptoMovimientoDTO() {
        return this.conceptoMovimientoDTO;
    }
    
    public void setConceptoMovimientoDTO(ConceptoMovimientoDTO conceptoMovimientoDTO) {
        this.conceptoMovimientoDTO = conceptoMovimientoDTO;
    }
    
    @Column(name="ORDEN", nullable=false, precision=2, scale=0)
    public int getOrden() {
        return this.orden;
    }
    
    public void setOrden(int orden) {
        this.orden = orden;
    }
   








}