package mx.com.afirme.midas.contratos.estadocuenta.reportes;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;


public interface ReporteEstadoCuentaServiciosRemote {

	List<ReporteReaseguradorEstadoCuentaDTO> obtenerMovimientosReaseguroCuentasPorPagar(BigDecimal idToPoliza,
			BigDecimal idTcReasegurador,BigDecimal idMoneda,BigDecimal[] idTcSubRamo,Integer mes,Integer anio,Date fechaInicio,Date fechaFin,boolean incluirSoloFacultativo,boolean incluirSoloAutomaticos);

	List<ReporteReaseguradorEstadoCuentaDTO> obtenerMovimientosReaseguroCuentasPorCobrar(BigDecimal idToReporteSiniestro,
			BigDecimal idTcReasegurador,BigDecimal idMoneda,BigDecimal[] idTcSubRamo,Integer mes,Integer anio,Date fechaInicio,Date fechaFin,boolean incluirSoloFacultativo,boolean incluirSoloAutomaticos);
	
	public ReporteSaldoTrimestralDTO obtenerSaldosPorReasegurador(BigDecimal idTcReasegurador,Integer[] idTcTipoReaseguro,Integer ejercicio,Integer suscripcion,
			boolean incluirEjerciciosAnteriores,BigDecimal idTcMoneda,boolean excluirSaldoCorredor);
	
	public ReporteReaseguradorNegociosFacultativosDTO obtenerReporteMovimientosFacultativosTrimestre(BigDecimal idTcReasegurador,Integer ejercicio,Integer suscripcion,boolean excluirSaldoCorredor);
	
	public List<ReporteSaldoPorSubRamoDTO> obtenerSubRamosAfectados(ReaseguradorCorredorDTO reaseguradorDTO,Integer[] idTcTipoReaseguro,BigDecimal idTcMoneda,boolean excluirSaldoCorredor);
}
