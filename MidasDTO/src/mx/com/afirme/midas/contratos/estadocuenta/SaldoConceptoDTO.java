package mx.com.afirme.midas.contratos.estadocuenta;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Calendar;

public class SaldoConceptoDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private String descripcionConcepto;
	private BigDecimal debe;
	private BigDecimal haber;
	private BigDecimal saldo;
	private BigDecimal saldoAcumulado;
	private String fechaTrimestre;
	private boolean ocultarSaldos;
	private boolean resaltarConcepto;
	private boolean mostrarSaldoAcumulado;
	private Calendar mesPertenceSaldo;
	private Integer idTcConfiguracionSaldoConcepto;
	private boolean aplicaDesglosePorEjercicio;

	public SaldoConceptoDTO(){
		aplicaDesglosePorEjercicio = false;
	}
	
	public SaldoConceptoDTO(String nombre,String fechaTrimestre,Integer idTcConfiguracionSaldoConcepto,BigDecimal debe,BigDecimal haber,BigDecimal saldoAcumulado,BigDecimal saldo){
		this.descripcionConcepto = nombre;
		this.debe = debe;
		this.haber = haber;
		this.fechaTrimestre = fechaTrimestre;
		this.saldoAcumulado = saldoAcumulado;
		this.saldo = saldo;
		this.idTcConfiguracionSaldoConcepto = idTcConfiguracionSaldoConcepto;
		aplicaDesglosePorEjercicio = false;
	}

	public BigDecimal getDebe() {
		return debe;
	}

	public void setDebe(BigDecimal debe) {
		this.debe = debe;
	}

	public BigDecimal getHaber() {
		return haber;
	}

	public void setHaber(BigDecimal haber) {
		this.haber = haber;
	}

	public BigDecimal getSaldo() {
		return saldo;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

	public BigDecimal getSaldoAcumulado() {
		return saldoAcumulado;
	}

	public void setSaldoAcumulado(BigDecimal saldoAcumulado) {
		this.saldoAcumulado = saldoAcumulado;
	}

	public String getDescripcionConcepto() {
		return descripcionConcepto;
	}

	public void setDescripcionConcepto(String descripcionConcepto) {
		this.descripcionConcepto = descripcionConcepto;
	}

	public String getFechaTrimestre() {
		return fechaTrimestre;
	}

	public void setFechaTrimestre(String fechaTrimestre) {
		this.fechaTrimestre = fechaTrimestre;
	}

	public boolean isOcultarSaldos() {
		return ocultarSaldos;
	}

	public void setOcultarSaldos(boolean ocultarSaldos) {
		this.ocultarSaldos = ocultarSaldos;
	}

	public boolean isResaltarConcepto() {
		return resaltarConcepto;
	}

	public void setResaltarConcepto(boolean resaltarConcepto) {
		this.resaltarConcepto = resaltarConcepto;
	}

	public boolean isMostrarSaldoAcumulado() {
		return mostrarSaldoAcumulado;
	}

	public void setMostrarSaldoAcumulado(boolean mostrarSaldoAcumulado) {
		this.mostrarSaldoAcumulado = mostrarSaldoAcumulado;
	}

	public String getEtiquetaDebe() {
		String resultado = "";
		if(!isOcultarSaldos()){
			if(this.debe != null && this.debe.compareTo(BigDecimal.ZERO) != 0){
				NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
				resultado = currencyFormatter.format(this.debe);
			}
		}
		return resultado;
	}

	public void setEtiquetaDebe(String etiquetaDebe) {
		
	}

	public String getEtiquetaHaber() {
		String resultado = "";
		if(!isOcultarSaldos()){
			if(this.haber != null && this.haber.compareTo(BigDecimal.ZERO) != 0){
				NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
				resultado = currencyFormatter.format(this.haber);
			}
		}
		return resultado;
	}

	public void setEtiquetaHaber(String etiquetaHaber) {
		
	}

	public String getEtiquetaSaldo() {
		String resultado = "";
		if(!isOcultarSaldos()){
			NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
			if(isMostrarSaldoAcumulado()){
				resultado = currencyFormatter.format(getSaldoAcumulado().doubleValue());
			}else{
				resultado = currencyFormatter.format(getSaldo().doubleValue());
			}
		}
		return resultado;
	}

	public void setEtiquetaSaldo(String etiquetaSaldo) {
		
	}

	public Calendar getMesPertenceSaldo() {
		return mesPertenceSaldo;
	}

	public void setMesPertenceSaldo(Calendar mesPertenceSaldo) {
		this.mesPertenceSaldo = mesPertenceSaldo;
	}

	public Integer getIdTcConfiguracionSaldoConcepto() {
		return idTcConfiguracionSaldoConcepto;
	}

	public void setIdTcConfiguracionSaldoConcepto(Integer idTcConfiguracionSaldoConcepto) {
		this.idTcConfiguracionSaldoConcepto = idTcConfiguracionSaldoConcepto;
	}

	public boolean isAplicaDesglosePorEjercicio() {
		return aplicaDesglosePorEjercicio;
	}

	public void setAplicaDesglosePorEjercicio(boolean aplicaDesglosePorEjercicio) {
		this.aplicaDesglosePorEjercicio = aplicaDesglosePorEjercicio;
	}
	
	public SaldoConceptoDTO duplicate(){
		SaldoConceptoDTO saldoConceptoCopia = new SaldoConceptoDTO();
		saldoConceptoCopia.setAplicaDesglosePorEjercicio(aplicaDesglosePorEjercicio);
		saldoConceptoCopia.setDebe(debe);
		saldoConceptoCopia.setDescripcionConcepto(descripcionConcepto);
		saldoConceptoCopia.setFechaTrimestre(fechaTrimestre);
		saldoConceptoCopia.setHaber(haber);
		saldoConceptoCopia.setIdTcConfiguracionSaldoConcepto(idTcConfiguracionSaldoConcepto);
		saldoConceptoCopia.setMesPertenceSaldo(mesPertenceSaldo);
		saldoConceptoCopia.setMostrarSaldoAcumulado(mostrarSaldoAcumulado);
		saldoConceptoCopia.setOcultarSaldos(ocultarSaldos);
		saldoConceptoCopia.setResaltarConcepto(resaltarConcepto);
		saldoConceptoCopia.setSaldo(saldo);
		saldoConceptoCopia.setSaldoAcumulado(saldoAcumulado);
		return saldoConceptoCopia;
	}
}