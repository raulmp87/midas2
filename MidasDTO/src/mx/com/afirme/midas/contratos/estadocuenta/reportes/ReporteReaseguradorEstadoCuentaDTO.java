package mx.com.afirme.midas.contratos.estadocuenta.reportes;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;


public class ReporteReaseguradorEstadoCuentaDTO implements Serializable {
	private static final long serialVersionUID = 5061759946795945352L;

	private String nombreReasegurador;
	private BigDecimal totalPrimaMenosComision;
	private BigDecimal totalIngresosPorDevolucionPrima;
	private BigDecimal totalIngresosBonoPorNoSiniestro;
	private BigDecimal totalPagos;
	private BigDecimal totalImpuestos;
	private BigDecimal totalSaldoAPagar;
	//Totales para el estado de cuenta por cobrar (siniestros)
	private BigDecimal totalCesionSiniestro;
	private BigDecimal totalCobros;
	private BigDecimal totalSaldoPorCobrar;
	private List<ReporteMovimientoReaseguroDTO> listaMovimientosReaseguro;
	private Object dataSourceSubReporte;
	public String getNombreReasegurador() {
		return nombreReasegurador;
	}
	public void setNombreReasegurador(String nombreReasegurador) {
		this.nombreReasegurador = nombreReasegurador;
	}
	public BigDecimal getTotalPrimaMenosComision() {
		return totalPrimaMenosComision;
	}
	public void setTotalPrimaMenosComision(BigDecimal totalPrimaMenosComision) {
		this.totalPrimaMenosComision = totalPrimaMenosComision;
	}
	public BigDecimal getTotalIngresosPorDevolucionPrima() {
		return totalIngresosPorDevolucionPrima;
	}
	public void setTotalIngresosPorDevolucionPrima(
			BigDecimal totalIngresosPorDevolucionPrima) {
		this.totalIngresosPorDevolucionPrima = totalIngresosPorDevolucionPrima;
	}
	public BigDecimal getTotalIngresosBonoPorNoSiniestro() {
		return totalIngresosBonoPorNoSiniestro;
	}
	public void setTotalIngresosBonoPorNoSiniestro(
			BigDecimal totalIngresosBonoPorNoSiniestro) {
		this.totalIngresosBonoPorNoSiniestro = totalIngresosBonoPorNoSiniestro;
	}
	public BigDecimal getTotalPagos() {
		return totalPagos;
	}
	public void setTotalPagos(BigDecimal totalPagos) {
		this.totalPagos = totalPagos;
	}
	public BigDecimal getTotalImpuestos() {
		return totalImpuestos;
	}
	public void setTotalImpuestos(BigDecimal totalImpuestos) {
		this.totalImpuestos = totalImpuestos;
	}
	public BigDecimal getTotalSaldoAPagar() {
		return totalSaldoAPagar;
	}
	public void setTotalSaldoAPagar(BigDecimal totalSaldoAPagar) {
		this.totalSaldoAPagar = totalSaldoAPagar;
	}
	public List<ReporteMovimientoReaseguroDTO> getListaMovimientosReaseguro() {
		return listaMovimientosReaseguro;
	}
	public void setListaMovimientosReaseguro(List<ReporteMovimientoReaseguroDTO> listaMovimientosReaseguro) {
		this.listaMovimientosReaseguro = listaMovimientosReaseguro;
	}
	public Object getDataSourceSubReporte() {
		return dataSourceSubReporte;
	}
	public void setDataSourceSubReporte(Object dataSourceSubReporte) {
		this.dataSourceSubReporte = dataSourceSubReporte;
	}
	public BigDecimal getTotalCesionSiniestro() {
		return totalCesionSiniestro;
	}
	public void setTotalCesionSiniestro(BigDecimal totalCesionSiniestro) {
		this.totalCesionSiniestro = totalCesionSiniestro;
	}
	public BigDecimal getTotalCobros() {
		return totalCobros;
	}
	public void setTotalCobros(BigDecimal totalCobros) {
		this.totalCobros = totalCobros;
	}
	public BigDecimal getTotalSaldoPorCobrar() {
		return totalSaldoPorCobrar;
	}
	public void setTotalSaldoPorCobrar(BigDecimal totalSaldoPorCobrar) {
		this.totalSaldoPorCobrar = totalSaldoPorCobrar;
	}
}
