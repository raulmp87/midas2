package mx.com.afirme.midas.contratos.estadocuenta.reportes;

import java.io.Serializable;
import java.math.BigDecimal;

import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;

public class ReporteSaldoPorSubRamoDTO implements Serializable,Comparable<ReporteSaldoPorSubRamoDTO> {

	private static final long serialVersionUID = -8559197319494737863L;
	
	private BigDecimal idTcSubRamo;
	private String descripcionSubRamo;
	private String codigoSubRamo;
	private Integer idTcTipoReaseguro;
	private String descripcionTipoReaseguro;
	private BigDecimal idTcReasegurador;
	private String nombreReasegurador;
	private BigDecimal idTcCorredor;
	private String nombreCorredor;
	private BigDecimal idMoneda;
	private BigDecimal descripcionMoneda;
	private BigDecimal montoPrimaMes1;
	private BigDecimal montoPrimaMes2;
	private BigDecimal montoPrimaMes3;
	private BigDecimal montoComisionMes1;
	private BigDecimal montoComisionMes2;
	private BigDecimal montoComisionMes3;
	private BigDecimal montoSiniestroMes1;
	private BigDecimal montoSiniestroMes2;
	private BigDecimal montoSiniestroMes3;
	private BigDecimal montoPrimaFacultativoMes1;
	private BigDecimal montoPrimaFacultativoMes2;
	private BigDecimal montoPrimaFacultativoMes3;
	private BigDecimal montoComisionFacultativoMes1;
	private BigDecimal montoComisionFacultativoMes2;
	private BigDecimal montoComisionFacultativoMes3;
	private BigDecimal montoSiniestroFacultativoMes1;
	private BigDecimal montoSiniestroFacultativoMes2;
	private BigDecimal montoSiniestroFacultativoMes3;
	private BigDecimal saldoAnterior;
	private BigDecimal saldoMes1;
	private BigDecimal saldoMes2;
	private BigDecimal saldoMes3;
	private BigDecimal montoTotalPrima;
	private BigDecimal montoTotalComision;
	private BigDecimal montoTotalSiniestro;
	private BigDecimal saldoTotalTrimestre;
	private BigDecimal saldoFacultativoMes1;
	private BigDecimal saldoFacultativoMes2;
	private BigDecimal saldoFacultativoMes3;
	private BigDecimal montoTotalPrimaFacultativo;
	private BigDecimal montoTotalComisionFacultativo;
	private BigDecimal montoTotalSiniestroFacultativo;
	private BigDecimal saldoTotalTrimestreFacultativo;
	private BigDecimal saldoProporcionalesMasFacultativo;
	
	private BigDecimal montoRemesasSaldos;
	private BigDecimal montoArrastreSaldos;
	
	private Integer ejercicio;
	
	public ReporteSaldoPorSubRamoDTO() {
		montoPrimaMes1 = montoPrimaMes2 = montoPrimaMes3 = BigDecimal.ZERO;
		saldoFacultativoMes1 = saldoFacultativoMes2 = saldoFacultativoMes3 = BigDecimal.ZERO;
		montoComisionMes1 = montoComisionMes2 = montoComisionMes3 = BigDecimal.ZERO;
		montoSiniestroMes1 = montoSiniestroMes2 = montoSiniestroMes3 = BigDecimal.ZERO;
		montoPrimaFacultativoMes1 = montoPrimaFacultativoMes2 = montoPrimaFacultativoMes3 = BigDecimal.ZERO;
		montoComisionFacultativoMes1 = montoComisionFacultativoMes2 = montoComisionFacultativoMes3 = BigDecimal.ZERO;
		montoSiniestroFacultativoMes1 = montoSiniestroFacultativoMes2 = montoSiniestroFacultativoMes3 = BigDecimal.ZERO;
		saldoAnterior = BigDecimal.ZERO;
		saldoMes1 = saldoMes2 = saldoMes3 = BigDecimal.ZERO;
		montoTotalPrima = montoTotalComision = montoTotalSiniestro = BigDecimal.ZERO;
		saldoTotalTrimestre = BigDecimal.ZERO;
		saldoFacultativoMes1 = saldoFacultativoMes2 = saldoFacultativoMes3 = BigDecimal.ZERO;
		montoTotalPrimaFacultativo = montoTotalComisionFacultativo = montoTotalSiniestroFacultativo = saldoTotalTrimestreFacultativo = BigDecimal.ZERO;
		saldoProporcionalesMasFacultativo = BigDecimal.ZERO;
		montoRemesasSaldos = montoArrastreSaldos = BigDecimal.ZERO;
	}

	private void calcularSaldosMes(){
		montoTotalPrima = montoPrimaMes1.add(montoPrimaMes2).add(montoPrimaMes3);
		montoTotalComision = montoComisionMes1.add(montoComisionMes2).add(montoComisionMes3);
		montoTotalSiniestro = montoSiniestroMes1.add(montoSiniestroMes2).add(montoSiniestroMes3);
	}
	
	private void calcularSaldosFacultativos(){
		montoTotalPrimaFacultativo = montoPrimaFacultativoMes1.add(montoPrimaFacultativoMes2).add(montoPrimaFacultativoMes3);
		montoTotalComisionFacultativo = montoComisionFacultativoMes1.add(montoComisionFacultativoMes2).add(montoComisionFacultativoMes3);
		montoTotalSiniestroFacultativo = montoSiniestroFacultativoMes1.add(montoSiniestroFacultativoMes2).add(montoSiniestroFacultativoMes3);
	}
	
	private void calcularSaldoTotal(){
		saldoTotalTrimestre = saldoMes1.add(saldoMes2).add(saldoMes3).add(saldoAnterior);
	}
	
	private void calcularSaldoTotalFacultativo(){
		saldoTotalTrimestreFacultativo = saldoMes1.add(saldoMes2).add(saldoMes3).add(saldoAnterior);
	}

	public BigDecimal getIdTcSubRamo() {
		return idTcSubRamo;
	}
	public void setIdTcSubRamo(BigDecimal idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}
	public String getDescripcionSubRamo() {
		return descripcionSubRamo;
	}
	public void setDescripcionSubRamo(String descripcionSubRamo) {
		this.descripcionSubRamo = descripcionSubRamo;
	}
	public String getCodigoSubRamo() {
		return codigoSubRamo;
	}
	public void setCodigoSubRamo(String codigoSubRamo) {
		this.codigoSubRamo = codigoSubRamo;
	}
	public Integer getIdTcTipoReaseguro() {
		return idTcTipoReaseguro;
	}
	public void setIdTcTipoReaseguro(Integer idTcTipoReaseguro) {
		this.idTcTipoReaseguro = idTcTipoReaseguro;
		if(idTcTipoReaseguro != null && idTcTipoReaseguro == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
			descripcionTipoReaseguro = "Couta parte";
		}else if(idTcTipoReaseguro != null && idTcTipoReaseguro == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
			descripcionTipoReaseguro = "Primer excedente";
		}else if(idTcTipoReaseguro != null && idTcTipoReaseguro == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
			descripcionTipoReaseguro = "Facultativo";
		}if(idTcTipoReaseguro != null && idTcTipoReaseguro == TipoReaseguroDTO.TIPO_RETENCION){
			descripcionTipoReaseguro = "Retención";
		}
	}
	public String getDescripcionTipoReaseguro() {
		return descripcionTipoReaseguro;
	}
	public void setDescripcionTipoReaseguro(String descripcionTipoReaseguro) {
		this.descripcionTipoReaseguro = descripcionTipoReaseguro;
	}
	public BigDecimal getMontoPrimaMes1() {
		return montoPrimaMes1;
	}
	public void setMontoPrimaMes1(BigDecimal montoPrimaMes1) {
		if(montoPrimaMes1 != null){
			this.montoPrimaMes1 = montoPrimaMes1;
			calcularSaldosMes();
		}
	}
	public BigDecimal getMontoPrimaMes2() {
		return montoPrimaMes2;
	}
	public void setMontoPrimaMes2(BigDecimal montoPrimaMes2) {
		if(montoPrimaMes2 != null){
			this.montoPrimaMes2 = montoPrimaMes2;
			calcularSaldosMes();
		}
	}
	public BigDecimal getMontoPrimaMes3() {
		return montoPrimaMes3;
	}
	public void setMontoPrimaMes3(BigDecimal montoPrimaMes3) {
		if(montoPrimaMes3 != null){
			this.montoPrimaMes3 = montoPrimaMes3;
			calcularSaldosMes();
		}
	}
	public BigDecimal getMontoComisionMes1() {
		return montoComisionMes1;
	}
	public void setMontoComisionMes1(BigDecimal montoComisionMes1) {
		if(montoComisionMes1 != null){
			this.montoComisionMes1 = montoComisionMes1;
			calcularSaldosMes();
		}
	}
	public BigDecimal getMontoComisionMes2() {
		return montoComisionMes2;
	}
	public void setMontoComisionMes2(BigDecimal montoComisionMes2) {
		if(montoComisionMes2 != null){
			this.montoComisionMes2 = montoComisionMes2;
			calcularSaldosMes();
		}
	}
	public BigDecimal getMontoComisionMes3() {
		return montoComisionMes3;
	}
	public void setMontoComisionMes3(BigDecimal montoComisionMes3) {
		if(montoComisionMes3 != null){
			this.montoComisionMes3 = montoComisionMes3;
			calcularSaldosMes();
		}
	}
	public BigDecimal getMontoSiniestroMes1() {
		return montoSiniestroMes1;
	}
	public void setMontoSiniestroMes1(BigDecimal montoSiniestroMes1) {
		if(montoSiniestroMes1 != null){
			this.montoSiniestroMes1 = montoSiniestroMes1;
			calcularSaldosMes();
		}
	}
	public BigDecimal getMontoSiniestroMes2() {
		return montoSiniestroMes2;
	}
	public void setMontoSiniestroMes2(BigDecimal montoSiniestroMes2) {
		if(montoSiniestroMes2 != null){
			this.montoSiniestroMes2 = montoSiniestroMes2;
			calcularSaldosMes();
		}
	}
	public BigDecimal getMontoSiniestroMes3() {
		return montoSiniestroMes3;
	}
	public void setMontoSiniestroMes3(BigDecimal montoSiniestroMes3) {
		if(montoSiniestroMes3 != null){
			this.montoSiniestroMes3 = montoSiniestroMes3;
			calcularSaldosMes();
		}
	}
	public BigDecimal getMontoPrimaFacultativoMes1() {
		return montoPrimaFacultativoMes1;
	}
	public void setMontoPrimaFacultativoMes1(BigDecimal montoPrimaFacultativoMes1) {
		if(montoPrimaFacultativoMes1 != null){
			this.montoPrimaFacultativoMes1 = montoPrimaFacultativoMes1;
			calcularSaldosFacultativos();
		}
	}
	public BigDecimal getMontoPrimaFacultativoMes2() {
		return montoPrimaFacultativoMes2;
	}
	public void setMontoPrimaFacultativoMes2(BigDecimal montoPrimaFacultativoMes2) {
		if(montoPrimaFacultativoMes2 != null){
			this.montoPrimaFacultativoMes2 = montoPrimaFacultativoMes2;
			calcularSaldosFacultativos();
		}
	}
	public BigDecimal getMontoPrimaFacultativoMes3() {
		return montoPrimaFacultativoMes3;
	}
	public void setMontoPrimaFacultativoMes3(BigDecimal montoPrimaFacultativoMes3) {
		if(montoPrimaFacultativoMes3 != null){
			this.montoPrimaFacultativoMes3 = montoPrimaFacultativoMes3;
			calcularSaldosFacultativos();
		}
	}
	public BigDecimal getMontoComisionFacultativoMes1() {
		return montoComisionFacultativoMes1;
	}
	public void setMontoComisionFacultativoMes1(BigDecimal montoComisionFacultativoMes1) {
		if(montoComisionFacultativoMes1 != null){
			this.montoComisionFacultativoMes1 = montoComisionFacultativoMes1;
			calcularSaldosFacultativos();
		}
	}
	public BigDecimal getMontoComisionFacultativoMes2() {
		return montoComisionFacultativoMes2;
	}
	public void setMontoComisionFacultativoMes2(BigDecimal montoComisionFacultativoMes2) {
		if(montoComisionFacultativoMes2 != null){
			this.montoComisionFacultativoMes2 = montoComisionFacultativoMes2;
			calcularSaldosFacultativos();
		}
	}
	public BigDecimal getMontoComisionFacultativoMes3() {
		return montoComisionFacultativoMes3;
	}
	public void setMontoComisionFacultativoMes3(BigDecimal montoComisionFacultativoMes3) {
		if(montoComisionFacultativoMes3 != null){
			this.montoComisionFacultativoMes3 = montoComisionFacultativoMes3;
			calcularSaldosFacultativos();
		}
	}
	public BigDecimal getSaldoMes1() {
		return saldoMes1;
	}
	public void setSaldoMes1(BigDecimal saldoMes1) {
		this.saldoMes1 = saldoMes1;
		calcularSaldoTotal();
	}
	public BigDecimal getSaldoMes2() {
		return saldoMes2;
	}
	public void setSaldoMes2(BigDecimal saldoMes2) {
		this.saldoMes2 = saldoMes2;
		calcularSaldoTotal();
	}
	public BigDecimal getSaldoMes3() {
		return saldoMes3;
	}
	public void setSaldoMes3(BigDecimal saldoMes3) {
		this.saldoMes3 = saldoMes3;
		calcularSaldoTotal();
	}
	public BigDecimal getSaldoTotalTrimestre() {
		return saldoTotalTrimestre;
	}
	public void setSaldoTotalTrimestre(BigDecimal saldoTotalTrimestre) {
		this.saldoTotalTrimestre = saldoTotalTrimestre;
	}
	public BigDecimal getIdTcReasegurador() {
		return idTcReasegurador;
	}
	public void setIdTcReasegurador(BigDecimal idTcReasegurador) {
		this.idTcReasegurador = idTcReasegurador;
	}
	public String getNombreReasegurador() {
		return nombreReasegurador;
	}
	public void setNombreReasegurador(String nombreReasegurador) {
		this.nombreReasegurador = nombreReasegurador;
	}
	public BigDecimal getIdTcCorredor() {
		return idTcCorredor;
	}
	public void setIdTcCorredor(BigDecimal idTcCorredor) {
		this.idTcCorredor = idTcCorredor;
	}
	public String getNombreCorredor() {
		return nombreCorredor;
	}
	public void setNombreCorredor(String nombreCorredor) {
		this.nombreCorredor = nombreCorredor;
	}
	public BigDecimal getSaldoAnterior() {
		return saldoAnterior;
	}
	public void setSaldoAnterior(BigDecimal saldoAnterior) {
		if(saldoAnterior != null){
			this.saldoAnterior = saldoAnterior;
			calcularSaldosMes();
		}
	}
	public BigDecimal getIdMoneda() {
		return idMoneda;
	}
	public void setIdMoneda(BigDecimal idMoneda) {
		this.idMoneda = idMoneda;
	}
	public BigDecimal getDescripcionMoneda() {
		return descripcionMoneda;
	}
	public void setDescripcionMoneda(BigDecimal descripcionMoneda) {
		this.descripcionMoneda = descripcionMoneda;
	}

	public int compareTo(ReporteSaldoPorSubRamoDTO o) {
		int value = 0;
		if(this.idTcSubRamo != null && o.getIdTcSubRamo() != null){
			value = this.idTcSubRamo.compareTo(o.getIdTcSubRamo());
		}
		return value;
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public ReporteSaldoPorSubRamoDTO duplicate(){
		ReporteSaldoPorSubRamoDTO reporteSaldoNuevo = new ReporteSaldoPorSubRamoDTO();
		reporteSaldoNuevo.setCodigoSubRamo(getCodigoSubRamo());
		reporteSaldoNuevo.setDescripcionMoneda(descripcionMoneda);
		reporteSaldoNuevo.setDescripcionSubRamo(descripcionSubRamo);
		reporteSaldoNuevo.setDescripcionTipoReaseguro(descripcionTipoReaseguro);
		reporteSaldoNuevo.setIdMoneda(idMoneda);
		reporteSaldoNuevo.setIdTcCorredor(idTcCorredor);
		reporteSaldoNuevo.setIdTcReasegurador(idTcReasegurador);
		reporteSaldoNuevo.setIdTcSubRamo(idTcSubRamo);
		reporteSaldoNuevo.setIdTcTipoReaseguro(idTcTipoReaseguro);
		reporteSaldoNuevo.setMontoComisionFacultativoMes1(montoComisionFacultativoMes1);
		reporteSaldoNuevo.setMontoComisionFacultativoMes2(montoComisionFacultativoMes2);
		reporteSaldoNuevo.setMontoComisionFacultativoMes3(montoComisionFacultativoMes3);
		reporteSaldoNuevo.setMontoComisionMes1(montoComisionMes1);
		reporteSaldoNuevo.setMontoComisionMes2(montoComisionMes2);
		reporteSaldoNuevo.setMontoComisionMes3(montoComisionMes3);
		reporteSaldoNuevo.setMontoPrimaFacultativoMes1(montoPrimaFacultativoMes1);
		reporteSaldoNuevo.setMontoPrimaFacultativoMes2(montoPrimaFacultativoMes2);
		reporteSaldoNuevo.setMontoPrimaFacultativoMes3(montoPrimaFacultativoMes3);
		reporteSaldoNuevo.setMontoPrimaMes1(montoPrimaMes1);
		reporteSaldoNuevo.setMontoPrimaMes2(montoPrimaMes2);
		reporteSaldoNuevo.setMontoPrimaMes3(montoPrimaMes3);
		reporteSaldoNuevo.setMontoSiniestroMes1(montoSiniestroMes1);
		reporteSaldoNuevo.setMontoSiniestroMes2(montoSiniestroMes2);
		reporteSaldoNuevo.setMontoSiniestroMes3(montoSiniestroMes3);
		reporteSaldoNuevo.setNombreCorredor(nombreCorredor);
		reporteSaldoNuevo.setNombreReasegurador(nombreReasegurador);
		reporteSaldoNuevo.setSaldoAnterior(saldoAnterior);
		reporteSaldoNuevo.setSaldoMes1(saldoMes1);
		reporteSaldoNuevo.setSaldoMes2(saldoMes2);
		reporteSaldoNuevo.setSaldoMes3(saldoMes3);
		reporteSaldoNuevo.setSaldoTotalTrimestre(saldoTotalTrimestre);
		return reporteSaldoNuevo;
	}
	
	public BigDecimal getMontoTotalPrima() {
		return montoTotalPrima;
	}
	public void setMontoTotalPrima(BigDecimal montoTotalPrima) {
		this.montoTotalPrima = montoTotalPrima;
	}
	public BigDecimal getMontoTotalComision() {
		return montoTotalComision;
	}
	public void setMontoTotalComision(BigDecimal montoTotalComision) {
		this.montoTotalComision = montoTotalComision;
	}
	public BigDecimal getMontoTotalSiniestro() {
		return montoTotalSiniestro;
	}
	public void setMontoTotalSiniestro(BigDecimal montoTotalSiniestro) {
		this.montoTotalSiniestro = montoTotalSiniestro;
	}
	public BigDecimal getMontoSiniestroFacultativoMes1() {
		return montoSiniestroFacultativoMes1;
	}
	public void setMontoSiniestroFacultativoMes1(BigDecimal montoSiniestroFacultativoMes1) {
		if(montoSiniestroFacultativoMes1 != null){
			this.montoSiniestroFacultativoMes1 = montoSiniestroFacultativoMes1;
			calcularSaldosFacultativos();
		}
	}
	public BigDecimal getMontoSiniestroFacultativoMes2() {
		return montoSiniestroFacultativoMes2;
	}
	public void setMontoSiniestroFacultativoMes2(BigDecimal montoSiniestroFacultativoMes2) {
		if(montoSiniestroFacultativoMes2 != null){
			this.montoSiniestroFacultativoMes2 = montoSiniestroFacultativoMes2;
			calcularSaldosFacultativos();
		}
	}
	public BigDecimal getMontoSiniestroFacultativoMes3() {
		return montoSiniestroFacultativoMes3;
	}
	public void setMontoSiniestroFacultativoMes3(BigDecimal montoSiniestroFacultativoMes3) {
		if(montoSiniestroFacultativoMes3 != null){
			this.montoSiniestroFacultativoMes3 = montoSiniestroFacultativoMes3;
			calcularSaldosFacultativos();
		}
	}
	public BigDecimal getMontoTotalPrimaFacultativo() {
		return montoTotalPrimaFacultativo;
	}
	public void setMontoTotalPrimaFacultativo(BigDecimal montoTotalPrimaFacultativo) {
		this.montoTotalPrimaFacultativo = montoTotalPrimaFacultativo;
	}
	public BigDecimal getMontoTotalComisionFacultativo() {
		return montoTotalComisionFacultativo;
	}
	public void setMontoTotalComisionFacultativo(BigDecimal montoTotalComisionFacultativo) {
		this.montoTotalComisionFacultativo = montoTotalComisionFacultativo;
	}
	public BigDecimal getMontoTotalSiniestroFacultativo() {
		return montoTotalSiniestroFacultativo;
	}
	public void setMontoTotalSiniestroFacultativo(BigDecimal montoTotalSiniestroFacultativo) {
		this.montoTotalSiniestroFacultativo = montoTotalSiniestroFacultativo;
	}
	public BigDecimal getSaldoTotalTrimestreFacultativo() {
		return saldoTotalTrimestreFacultativo;
	}
	public void setSaldoTotalTrimestreFacultativo(BigDecimal saldoTotalTrimestreFacultativo) {
		this.saldoTotalTrimestreFacultativo = saldoTotalTrimestreFacultativo;
	}
	public BigDecimal getSaldoProporcionalesMasFacultativo() {
		if(saldoProporcionalesMasFacultativo == null || saldoProporcionalesMasFacultativo.compareTo(BigDecimal.ZERO) == 0)
			saldoProporcionalesMasFacultativo = saldoTotalTrimestre.add(saldoTotalTrimestreFacultativo);
		return saldoProporcionalesMasFacultativo;
	}
	public void setSaldoProporcionalesMasFacultativo(BigDecimal saldoProporcionalesMasFacultativo) {
		this.saldoProporcionalesMasFacultativo = saldoProporcionalesMasFacultativo;
	}
	public BigDecimal getSaldoFacultativoMes1() {
		return saldoFacultativoMes1;
	}
	public void setSaldoFacultativoMes1(BigDecimal saldoFacultativoMes1) {
		if(saldoFacultativoMes1 != null){
			this.saldoFacultativoMes1 = saldoFacultativoMes1;
			calcularSaldoTotalFacultativo();
		}
	}
	public BigDecimal getSaldoFacultativoMes2() {
		return saldoFacultativoMes2;
	}
	public void setSaldoFacultativoMes2(BigDecimal saldoFacultativoMes2) {
		if(saldoFacultativoMes2 != null){
			this.saldoFacultativoMes2 = saldoFacultativoMes2;
			calcularSaldoTotalFacultativo();
		}
	}
	public BigDecimal getSaldoFacultativoMes3() {
		return saldoFacultativoMes3;
	}
	public void setSaldoFacultativoMes3(BigDecimal saldoFacultativoMes3) {
		if(saldoFacultativoMes3 != null){
			this.saldoFacultativoMes3 = saldoFacultativoMes3;
			calcularSaldoTotalFacultativo();
		}
	}
	public BigDecimal getMontoRemesasSaldos() {
		return montoRemesasSaldos;
	}
	public void setMontoRemesasSaldos(BigDecimal montoRemesasSaldos) {
		if(montoRemesasSaldos != null){
			this.montoRemesasSaldos = montoRemesasSaldos;
			this.montoArrastreSaldos = saldoTotalTrimestre.subtract(montoRemesasSaldos);
		}
	}
	public BigDecimal getMontoArrastreSaldos() {
		return montoArrastreSaldos;
	}
	public void setMontoArrastreSaldos(BigDecimal montoArrastreSaldos) {
		this.montoArrastreSaldos = montoArrastreSaldos;
	}
}
