package mx.com.afirme.midas.contratos.estadocuenta;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDTO;
import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDTO;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteDTO;
import mx.com.afirme.midas.contratos.linea.CalculoSuscripcionesDTO;
import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.contratos.linea.SuscripcionDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;
import mx.com.afirme.midas.danios.soporte.EndosoSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;

public class EstadoCuentaDecoradoDTO extends EstadoCuentaDTO {

	private static final long serialVersionUID = 1L;
	
	private EstadoCuentaDTO estadoCuentaDTO;
	private List<SaldoConceptoDTO> saldosAcumulados;
	private SaldoConceptoDTO saldoTecnico;
	private CalculoSuscripcionesDTO calculoSuscripcionesDTO;
	private PolizaDTO polizaDTO;
	PolizaSoporteDanosDTO polizaSoporteDanosDTO;
	private EndosoSoporteDanosDTO endosoSoporteDanosDTO;
	private BigDecimal montoIngreso;
	private BigDecimal montoEgreso;
	private String cuentaAfirme;
	private SaldoConceptoDTO saldoTecnicoAnterior;
	private EndosoDTO endosoDTO;
	
	private List<List<SaldoConceptoDTO>> listaSaldosAcumuladosPorMes;
	private List<SaldoConceptoDTO> saldosAcumuladosPorMes;
	
	private SuscripcionDTO suscripcionDTOCalculada;
	
	//Constructor
	public EstadoCuentaDecoradoDTO(EstadoCuentaDTO estadoCuentaDTO){
        this.estadoCuentaDTO = estadoCuentaDTO;        
	}
	
	@Override
	public BigDecimal getIdEstadoCuenta() {
        return this.estadoCuentaDTO.getIdEstadoCuenta();
    }
    
	@Override
    public void setIdEstadoCuenta(BigDecimal idEstadoCuenta) {
        this.estadoCuentaDTO.setIdEstadoCuenta(idEstadoCuenta);
    }
	
	@Override
    public ContratoFacultativoDTO getContratoFacultativoDTO() {
        return this.estadoCuentaDTO.getContratoFacultativoDTO();
    }
    
	@Override
    public void setContratoFacultativoDTO(ContratoFacultativoDTO contratoFacultativoDTO) {
        this.estadoCuentaDTO.setContratoFacultativoDTO(contratoFacultativoDTO);
    }

	@Override
    public ContratoCuotaParteDTO getContratoCuotaParteDTO() {
		return this.estadoCuentaDTO.getContratoCuotaParteDTO();
	}

	@Override
	public void setContratoCuotaParteDTO(ContratoCuotaParteDTO contratoCuotaParteDTO) {
		this.estadoCuentaDTO.setContratoCuotaParteDTO(contratoCuotaParteDTO);
	}

	@Override
    public ContratoPrimerExcedenteDTO getContratoPrimerExcedenteDTO() {
		return this.estadoCuentaDTO.getContratoPrimerExcedenteDTO();
	}

	@Override
	public void setContratoPrimerExcedenteDTO(
			ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO) {
		this.estadoCuentaDTO.setContratoPrimerExcedenteDTO(contratoPrimerExcedenteDTO);
	}

	@Override
    public int getIdMoneda() {
        return this.estadoCuentaDTO.getIdMoneda();
    }
    
	@Override
    public void setIdMoneda(int idMoneda) {
        this.estadoCuentaDTO.setIdMoneda(idMoneda);
    }
    
	@Override
    public BigDecimal getIdPoliza() {
        return this.estadoCuentaDTO.getIdPoliza();
    }
    
	@Override
    public void setIdPoliza(BigDecimal idPoliza) {
        this.estadoCuentaDTO.setIdPoliza(idPoliza);
    }
    
	@Override
    public Integer getNumeroEndoso() {
        return this.estadoCuentaDTO.getNumeroEndoso();
    }
    
	@Override
    public void setNumeroEndoso(Integer numeroEndoso) {
        this.estadoCuentaDTO.setNumeroEndoso(numeroEndoso);
    }

	@Override
    public String getObservaciones() {
        return this.estadoCuentaDTO.getObservaciones();
    }
    
	@Override
    public void setObservaciones(String observaciones) {
        this.estadoCuentaDTO.setObservaciones(observaciones);
    }
    
	@Override
    public List<AcumuladorDTO> getAcumuladorDTOs() {
        return this.estadoCuentaDTO.getAcumuladorDTOs();
    }
    
	@Override
    public void setAcumuladorDTOs(List<AcumuladorDTO> acumuladorDTOs) {
        this.estadoCuentaDTO.setAcumuladorDTOs(acumuladorDTOs);
    }

	@Override
	public SubRamoDTO getSubRamoDTO() {
	    return this.estadoCuentaDTO.getSubRamoDTO();
	}
	
	@Override
	public void setSubRamoDTO(SubRamoDTO subRamoDTO) {
	    this.estadoCuentaDTO.setSubRamoDTO(subRamoDTO);
	}
	
	@Override
	public LineaDTO getLineaDTO() {
	    return this.estadoCuentaDTO.getLineaDTO();
	}
	
	@Override
	public void setLineaDTO(LineaDTO lineaDTO) {
	    this.estadoCuentaDTO.setLineaDTO(lineaDTO);
	}	

	@Override
	public ReaseguradorCorredorDTO getReaseguradorCorredorDTO() {
	    return this.estadoCuentaDTO.getReaseguradorCorredorDTO();
	}
	
	@Override
	public void setReaseguradorCorredorDTO(ReaseguradorCorredorDTO reaseguradorCorredorDTO) {
	    this.estadoCuentaDTO.setReaseguradorCorredorDTO(reaseguradorCorredorDTO);
	}

	@Override
	public int getTipoReaseguro() {
		return  this.estadoCuentaDTO.getTipoReaseguro();
	}

	@Override
	public void setTipoReaseguro(int tipoReaseguro) {
		this.estadoCuentaDTO.setTipoReaseguro(tipoReaseguro);
	}

	@Override
    public int getEjercicio() {
        return this.estadoCuentaDTO.getEjercicio();
    }
    
	@Override
    public void setEjercicio(int ejercicio) {
        this.estadoCuentaDTO.setEjercicio(ejercicio);
    }
    
	@Override
    public int getSuscripcion() {
        return this.estadoCuentaDTO.getSuscripcion();
    }
    
	@Override
    public void setSuscripcion(int suscripcion) {
        this.estadoCuentaDTO.setSuscripcion(suscripcion);
    }	
	
	public String getDescripcionSuscripcion(){
		String descripcion = "";
		if (calculoSuscripcionesDTO == null){
			calculoSuscripcionesDTO = new CalculoSuscripcionesDTO();
		}
		SuscripcionDTO suscripcionDTO = calculoSuscripcionesDTO.getSuscripcion(new EstadoCuentaDecoradoDTO(estadoCuentaDTO), null);
		if (suscripcionDTO != null){
			descripcion = suscripcionDTO.getDescripcion();
		}
//		calcularSuscripciones();
//		List<SuscripcionDTO> suscripcionDTOList = calculoSuscripcionesDTO.getListaSuscripciones();
//		int idSuscripcion = estadoCuentaDTO.getSuscripcion();
//		String descripcion = "No Disponible";
//		if ((idSuscripcion-1) < suscripcionDTOList.size()){
//			descripcion = suscripcionDTOList.get(idSuscripcion - 1).getDescripcion();
//		}else{
//			Calendar calendar = Calendar.getInstance();
//			calendar.setTime(this.getFechaFinal());
//			calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) + 20);
//			calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1);
//			calculoSuscripcionesDTO.setFechaFinal(calendar.getTime());
//			suscripcionDTOList = calculoSuscripcionesDTO.getListaSuscripciones();
//			descripcion = suscripcionDTOList.get(idSuscripcion - 1).getDescripcion();
//		}
		return descripcion;
	}
	
	public SuscripcionDTO getSuscripcionDTO(){
		if (calculoSuscripcionesDTO == null){
			calculoSuscripcionesDTO = new CalculoSuscripcionesDTO();
		}
		SuscripcionDTO suscripcionDTO = calculoSuscripcionesDTO.getSuscripcion(new EstadoCuentaDecoradoDTO(estadoCuentaDTO), null);
		return suscripcionDTO;
	}
	
	public String getDescripcionTipoReaseguroReporte(){
		String descripcion = null;
		if (estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE)
			descripcion = "Automáticos";
		else
			if (estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE)
				descripcion = "Automáticos";
			else
				if (estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO)
					descripcion = "Contrato Facultativo";
				else
					if (estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_RETENCION)
						descripcion = "Automáticos";
		
		return descripcion;
	}
	
	public String getDescripcionTipoReaseguro(){
		String descripcion = null;
		if (estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE)
			descripcion = "Contrato Cuota Parte";
		else
			if (estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE)
				descripcion = "Contrato Primer Excedente";
			else
				if (estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO)
					descripcion = "Contrato Facultativo";
				else
					if (estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_RETENCION)
						descripcion = "Retención";
		
		return descripcion;
	}
	
	public String getNombreLargoReasegurador() {
		String nombreReasegurador = null;
		if (estadoCuentaDTO != null){
			if(estadoCuentaDTO.getReaseguradorCorredorDTO() != null)
				nombreReasegurador = estadoCuentaDTO.getReaseguradorCorredorDTO().getNombre();
			else
				nombreReasegurador = "Retención";
		}
				
	    return nombreReasegurador;
	}
	public String getNombreLargoCorredor(){
		String nombreCorredor = "N/A";
		if (estadoCuentaDTO != null && estadoCuentaDTO.getCorredorDTO() != null){
			nombreCorredor = estadoCuentaDTO.getCorredorDTO().getNombre();
		}
		
		return nombreCorredor;
	}
	
	public String getNombreReasegurador() {
		String nombreReasegurador = null;
		if (estadoCuentaDTO != null){
			if(estadoCuentaDTO.getReaseguradorCorredorDTO() != null)
				nombreReasegurador = estadoCuentaDTO.getReaseguradorCorredorDTO().getNombrecorto();
			else
				nombreReasegurador = "Retención";
		}
				
	    return nombreReasegurador;
	}
	public String getNombreCorredor(){
		String nombreCorredor = "N/A";
		if (estadoCuentaDTO != null && estadoCuentaDTO.getCorredorDTO() != null){
			nombreCorredor = estadoCuentaDTO.getCorredorDTO().getNombrecorto();
		}
		
		return nombreCorredor;
	}
	
	public String getRfc(){
		String rfc = null;
		if (estadoCuentaDTO != null && estadoCuentaDTO.getCorredorDTO() != null){
			rfc = estadoCuentaDTO.getCorredorDTO().getRfc();			
		}			
	    return rfc;		
	}
	
	public String getEstatus() {
		return null;
	}
	
	public Date getFechaFinalPeriodo() {
//		calcularSuscripciones();
//		List<SuscripcionDTO> suscripcionDTOList = calculoSuscripcionesDTO.getListaSuscripciones();
		int idSuscripcion = estadoCuentaDTO.getSuscripcion();
		if (calculoSuscripcionesDTO == null){
			calculoSuscripcionesDTO = new CalculoSuscripcionesDTO();
		}
		SuscripcionDTO suscripcionDTO = calculoSuscripcionesDTO.getSuscripcion(new EstadoCuentaDecoradoDTO(estadoCuentaDTO), new Integer(idSuscripcion));
		Date fechaFinal = null;
		if (suscripcionDTO != null){
			fechaFinal = suscripcionDTO.getFechaFinal();
		}
		
//		if ((idSuscripcion-1) < suscripcionDTOList.size()){
//			fechaFinal = suscripcionDTOList.get(idSuscripcion - 1).getFechaFinal();
//		}else{
//			Calendar calendar = Calendar.getInstance();
//			calendar.setTime(this.getFechaFinal());
//			calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) + 20);
//			calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1);
//			calculoSuscripcionesDTO.setFechaFinal(calendar.getTime());
//			suscripcionDTOList = calculoSuscripcionesDTO.getListaSuscripciones();
//			fechaFinal = suscripcionDTOList.get(idSuscripcion - 1).getFechaFinal();
//		}
	    return fechaFinal;
	}
	
	public Date getFechaInicialPeriodo() {
//		calcularSuscripciones();
//		List<SuscripcionDTO> suscripcionDTOList = calculoSuscripcionesDTO.getListaSuscripciones();
		int idSuscripcion = estadoCuentaDTO.getSuscripcion();
		if (calculoSuscripcionesDTO == null){
			calculoSuscripcionesDTO = new CalculoSuscripcionesDTO();
		}
		SuscripcionDTO suscripcionDTO = calculoSuscripcionesDTO.getSuscripcion(new EstadoCuentaDecoradoDTO(estadoCuentaDTO), new Integer(idSuscripcion));
		Date fechaInicial = null;
		if (suscripcionDTO != null){
			fechaInicial = suscripcionDTO.getFechaInicial();
		}
//		if ((idSuscripcion-1) < suscripcionDTOList.size()){
//			fechaInicial = suscripcionDTOList.get(idSuscripcion - 1).getFechaInicial();
//		}else{
//			Calendar calendar = Calendar.getInstance();
//			calendar.setTime(this.getFechaFinal());
//			calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) + 20);
//			calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1);
//			calculoSuscripcionesDTO.setFechaFinal(calendar.getTime());
//			suscripcionDTOList = calculoSuscripcionesDTO.getListaSuscripciones();
//			fechaInicial = suscripcionDTOList.get(idSuscripcion - 1).getFechaInicial();
//		}
	    return fechaInicial;
	}

	public List<SaldoConceptoDTO> getSaldosAcumulados() {
		return saldosAcumulados;
	}

	public void setSaldosAcumulados(List<SaldoConceptoDTO> saldosAcumulados) {
		this.saldosAcumulados = saldosAcumulados;
	}

	public SaldoConceptoDTO getSaldoTecnico() {
		return saldoTecnico;
	}

	public void setSaldoTecnico(SaldoConceptoDTO saldoTecnico) {
		this.saldoTecnico = saldoTecnico;
		if(saldoTecnico != null && !saldoTecnico.isMostrarSaldoAcumulado()){
			this.saldoTecnico.setMostrarSaldoAcumulado(true);
		}
	}
	
	public BigDecimal getFormaPago(){
		BigDecimal formaPago = new BigDecimal(0);
		int tipoReaseguro = this.getTipoReaseguro();
		
		switch(tipoReaseguro){
		case TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE: 
			formaPago = this.getContratoCuotaParteDTO().getFormaPago();
			break;
		case TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE:
			formaPago = this.getContratoPrimerExcedenteDTO().getFormaPago();
			break;
		case TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO:
			formaPago = this.getContratoFacultativoDTO().getIdFormaDePago();
			break;
		case TipoReaseguroDTO.TIPO_RETENCION:
			formaPago = new BigDecimal(LineaDTO.FORMA_PAGO_RETENCION_PURA);
		}
		
		return formaPago;
	}
	
	public String getDescripcionFormaPago(){
		String descripcion = null;
		int formaPago = 0;
		BigDecimal formaPagoWrapper = null;
		int tipoReaseguro = this.getTipoReaseguro();
		
		switch(tipoReaseguro){
		case TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE: 
				formaPagoWrapper = this.getContratoCuotaParteDTO().getFormaPago();
				break;
		case TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE:
				formaPagoWrapper = this.getContratoPrimerExcedenteDTO().getFormaPago();
				break;
		case TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO:
			formaPagoWrapper = this.getContratoFacultativoDTO().getIdFormaDePago();
			break;
		case TipoReaseguroDTO.TIPO_RETENCION:
			formaPago = LineaDTO.FORMA_PAGO_RETENCION_PURA;
		}
		
		if(formaPagoWrapper != null){
			formaPago = formaPagoWrapper.intValue();			
		}
		
		switch(formaPago){
		case LineaDTO.FORMA_PAGO_MENSUAL: descripcion = "Mensual"; break;			
		case LineaDTO.FORMA_PAGO_TRIMESTRAL: descripcion = "Trimestral"; break;				
		case LineaDTO.FORMA_PAGO_SEMESTRAL: descripcion = "Semestral"; break;	
		case LineaDTO.FORMA_PAGO_ANUAL:     descripcion = "Anual";		
		}
	
		return descripcion;	
		
	}
	
//	private void calcularSuscripciones(){
//		BigDecimal formaPago = null;
//		
//		if(calculoSuscripcionesDTO == null)
//			calculoSuscripcionesDTO = new CalculoSuscripcionesDTO();
//		else
//			return;
//		
//		if (estadoCuentaDTO != null){
//			calculoSuscripcionesDTO.setEjercicio(estadoCuentaDTO.getEjercicio());
//			if(estadoCuentaDTO.getTipoReaseguro() != TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
//				//if (estadoCuentaDTO.getLineaDTO() != null){
//					if(estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE && estadoCuentaDTO.getContratoCuotaParteDTO() != null){
//						formaPago = estadoCuentaDTO.getContratoCuotaParteDTO().getFormaPago();
//						calculoSuscripcionesDTO.setFechaInicial(estadoCuentaDTO.getContratoCuotaParteDTO().getFechaInicial());
//						calculoSuscripcionesDTO.setFechaFinal(estadoCuentaDTO.getContratoCuotaParteDTO().getFechaFinal());
//					}else
//						if(estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE && estadoCuentaDTO.getContratoPrimerExcedenteDTO() != null){
//							formaPago = estadoCuentaDTO.getContratoPrimerExcedenteDTO().getFormaPago();
//							calculoSuscripcionesDTO.setFechaInicial(estadoCuentaDTO.getContratoPrimerExcedenteDTO().getFechaInicial());
//							calculoSuscripcionesDTO.setFechaFinal(estadoCuentaDTO.getContratoPrimerExcedenteDTO().getFechaFinal());
//						}else
//							if(estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_RETENCION){
//								formaPago = new BigDecimal(LineaDTO.FORMA_PAGO_RETENCION_PURA);
//								calculoSuscripcionesDTO.setFechaInicial(estadoCuentaDTO.getLineaDTO().getFechaInicial());
//								calculoSuscripcionesDTO.setFechaFinal(estadoCuentaDTO.getLineaDTO().getFechaFinal());
//							}
//			//	}
//			}else{
//				if(estadoCuentaDTO.getContratoFacultativoDTO() != null){
//					calculoSuscripcionesDTO.setFechaInicial(estadoCuentaDTO.getContratoFacultativoDTO().getFechaInicial());
//					calculoSuscripcionesDTO.setFechaFinal(estadoCuentaDTO.getContratoFacultativoDTO().getFechaFinal());
//					formaPago = estadoCuentaDTO.getContratoFacultativoDTO().getIdFormaDePago();
//				}
//			}
//			if (formaPago != null){
//				calculoSuscripcionesDTO.setFormaPago(formaPago);
//				calculoSuscripcionesDTO.setEjercicio(estadoCuentaDTO.getEjercicio());
//
//			}
//		}
//	}

	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	public EstadoCuentaDTO getEstadoCuentaDTO() {
		return estadoCuentaDTO;
	}

	public void setEstadoCuentaDTO(EstadoCuentaDTO estadoCuentaDTO) {
		this.estadoCuentaDTO = estadoCuentaDTO;
	}

	public BigDecimal getMontoIngreso() {
		return montoIngreso;
	}

	public void setMontoIngreso(BigDecimal montoIngreso) {
		this.montoIngreso = montoIngreso;
	}
	
	public String toString(){
		StringBuffer buffer = new StringBuffer();
		
		buffer.append(this.getDescripcionTipoReaseguro());
		buffer.append("-");
		buffer.append(this.getDescripcionSuscripcion());
		buffer.append("-");
		buffer.append(this.getNombreReasegurador());
		buffer.append("-");
		buffer.append(this.getSubRamoDTO().getDescripcionSubRamo());
		buffer.append("-");
		buffer.append(this.getDescripcionMoneda());
		
		if(this.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
			buffer.append(this.getPolizaSoporteDanosDTO().getNumeroPoliza());
			buffer.append("-");
			buffer.append(this.getNumeroEndoso());
		}

		return buffer.toString();
		
	}
	
	public String getDescripcionMoneda(){
		String descripcion = null;
		int idMoneda = this.getIdMoneda();
		
		switch(idMoneda){
		case 840: descripcion = "Dolares"; break;
		case 484: descripcion = "Moneda Nacional";
		}

		
		return descripcion;
	}
	
	public PolizaSoporteDanosDTO getPolizaSoporteDanosDTO() {
		return polizaSoporteDanosDTO;
	}

	public void setPolizaSoporteDanosDTO(PolizaSoporteDanosDTO polizaSoporteDanosDTO) {
		this.polizaSoporteDanosDTO = polizaSoporteDanosDTO;
	}

	public EndosoSoporteDanosDTO getEndosoSoporteDanosDTO() {
		return endosoSoporteDanosDTO;
	}

	public void setEndosoSoporteDanosDTO(EndosoSoporteDanosDTO endosoSoporteDanosDTO) {
		this.endosoSoporteDanosDTO = endosoSoporteDanosDTO;
	}

	public BigDecimal getMontoEgreso() {
		return montoEgreso;
	}

	public void setMontoEgreso(BigDecimal montoEgreso) {
		this.montoEgreso = montoEgreso;
	}

	public Date getFechaInicial() {
		Date fechaInicial = null;
		if (this.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
			fechaInicial = this.getContratoCuotaParteDTO().getFechaInicial();
		}else{
			if (this.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
				fechaInicial = this.getContratoPrimerExcedenteDTO().getFechaInicial();
			}else{
				if (this.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
					fechaInicial = this.getContratoFacultativoDTO().getFechaInicial();
				}else{
					if (this.getTipoReaseguro() == TipoReaseguroDTO.TIPO_RETENCION){
						fechaInicial = this.getLineaDTO().getFechaInicial();
					}
				}
			}
		}
		
		return fechaInicial;
	}

	public Date getFechaFinal() {
		Date fechaFinal = null;
		
		if (this.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
			fechaFinal = this.getContratoCuotaParteDTO().getFechaFinal();
		}else{
			if (this.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
				fechaFinal = this.getContratoPrimerExcedenteDTO().getFechaFinal();
			}else{
				if (this.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
					fechaFinal = this.getContratoFacultativoDTO().getFechaFinal();
				}else{
					if (this.getTipoReaseguro() == TipoReaseguroDTO.TIPO_RETENCION){
						fechaFinal = this.getLineaDTO().getFechaFinal();
					}
				}
			}
		}
			
		return fechaFinal;
	}

	public String getFolioContrato() {
		String folioContrato = null;
		
		if (this.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
			folioContrato = this.getContratoCuotaParteDTO().getFolioContrato();
		}else{
			if (this.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
				folioContrato = this.getContratoPrimerExcedenteDTO().getFolioContrato();
			}else{
				if (this.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
					folioContrato = null; //TODO Falta definir el campo Folio Contrato en ContratoFacultativoDTO, temporalmente se regresa un null
				}
			}
		}
		
		return folioContrato;
	}	

	public String getCuentaAfirme() {
        return cuentaAfirme;
    }
    
	public void setCuentaAfirme(String cuentaAfirme) {
        this.cuentaAfirme = cuentaAfirme;
    }	

	@Override
	public ReaseguradorCorredorDTO getCorredorDTO() {
		if(this.estadoCuentaDTO != null)
			return this.estadoCuentaDTO.getCorredorDTO();
		else return null;
	}

	public List<List<SaldoConceptoDTO>> getListaSaldosAcumuladosPorMes() {
		return listaSaldosAcumuladosPorMes;
	}

	public void setListaSaldosAcumuladosPorMes(List<List<SaldoConceptoDTO>> listaSaldosAcumuladosPorMes) {
		this.listaSaldosAcumuladosPorMes = listaSaldosAcumuladosPorMes;
	}

	public List<SaldoConceptoDTO> getSaldosAcumuladosPorMes() {
		return saldosAcumuladosPorMes;
	}

	public void setSaldosAcumuladosPorMes(List<SaldoConceptoDTO> saldosAcumuladosPorMes) {
		this.saldosAcumuladosPorMes = saldosAcumuladosPorMes;
	}

	public SaldoConceptoDTO getSaldoTecnicoAnterior() {
		return saldoTecnicoAnterior;
	}

	public void setSaldoTecnicoAnterior(SaldoConceptoDTO saldoTecnicoAnterior) {
		this.saldoTecnicoAnterior = saldoTecnicoAnterior;
	}

	public SuscripcionDTO getSuscripcionDTOCalculada() {
		return suscripcionDTOCalculada;
	}

	public void setSuscripcionDTOCalculada(SuscripcionDTO suscripcionDTOCalculada) {
		this.suscripcionDTOCalculada = suscripcionDTOCalculada;
	}
	   
	@Override
    public Short getEstatusAutorizacionPagoFacultativoPorEstadoCuenta() {
		return estadoCuentaDTO.getEstatusAutorizacionPagoFacultativoPorEstadoCuenta();
	}

	@Override
	public void setEstatusAutorizacionPagoFacultativoPorEstadoCuenta(
			Short estatusAutorizacionPagoFacultativoPorEstadoCuenta) {
		this.estadoCuentaDTO.setEstatusAutorizacionPagoFacultativoPorEstadoCuenta(estatusAutorizacionPagoFacultativoPorEstadoCuenta);
	}
		
	@Override
	public Date getFechaSolicitudAutorizacionPagoFacultativoPorEstadoCuenta() {
		return this.estadoCuentaDTO.getFechaSolicitudAutorizacionPagoFacultativoPorEstadoCuenta();
	}

	@Override
	public void setFechaSolicitudAutorizacionPagoFacultativoPorEstadoCuenta(
			Date fechaSolicitudAutorizacionPagoFacultativoPorEstadoCuenta) {
		this.estadoCuentaDTO.setFechaSolicitudAutorizacionPagoFacultativoPorEstadoCuenta(fechaSolicitudAutorizacionPagoFacultativoPorEstadoCuenta);
	}
	
	@Override
	public String getUsuarioSolicitudAutorizacionPagoFacultativoPorEstadoCuenta() {
		return this.estadoCuentaDTO.getUsuarioSolicitudAutorizacionPagoFacultativoPorEstadoCuenta();
	}

	@Override
	public void setUsuarioSolicitudAutorizacionPagoFacultativoPorEstadoCuenta(
			String usuarioSolicitudAutorizacionPagoFacultativoPorEstadoCuenta) {
		this.estadoCuentaDTO.setUsuarioSolicitudAutorizacionPagoFacultativoPorEstadoCuenta(usuarioSolicitudAutorizacionPagoFacultativoPorEstadoCuenta);
	}
	
	@Override
	public Date getFechaAutorizacionPagoFacultativoPorEstadoCuenta() {
		return this.estadoCuentaDTO.getFechaAutorizacionPagoFacultativoPorEstadoCuenta();
	}

	@Override
	public void setFechaAutorizacionPagoFacultativoPorEstadoCuenta(
			Date fechaAutorizacionPagoFacultativoPorEstadoCuenta) {
		this.estadoCuentaDTO.setFechaAutorizacionPagoFacultativoPorEstadoCuenta(fechaAutorizacionPagoFacultativoPorEstadoCuenta);
	}
	
	@Override
	public String getUsuarioAutorizacionPagoFacultativoPorEstadoCuenta() {
		return this.estadoCuentaDTO.getUsuarioAutorizacionPagoFacultativoPorEstadoCuenta();
	}

	@Override
	public void setUsuarioAutorizacionPagoFacultativoPorEstadoCuenta(
			String usuarioAutorizacionPagoFacultativoPorEstadoCuenta) {
		this.estadoCuentaDTO.setUsuarioAutorizacionPagoFacultativoPorEstadoCuenta(usuarioAutorizacionPagoFacultativoPorEstadoCuenta);
	}

	public EndosoDTO getEndosoDTO() {
		return endosoDTO;
	}

	public void setEndosoDTO(EndosoDTO endosoDTO) {
		this.endosoDTO = endosoDTO;
	}
}