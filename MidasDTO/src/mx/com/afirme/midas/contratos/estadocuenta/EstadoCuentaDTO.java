package mx.com.afirme.midas.contratos.estadocuenta;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDTO;
import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDTO;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteDTO;
import mx.com.afirme.midas.contratos.linea.LineaDTO;

/**
 * EstadoCuentaDTO entity. @author MyEclipse Persistence Tools
 */

//Mapeo de campos para ejecutar queries nativos sobre la tabla ToEstadoCuenta
@SqlResultSetMapping(name="estadoCuentaMapping", entities={
	    @EntityResult(entityClass=EstadoCuentaDTO.class, fields = {	    	
	    	@FieldResult(name="idEstadoCuenta", column="IDTOESTADOCUENTA"),	
	    	@FieldResult(name="subRamoDTO.idTcSubRamo", column="IDTCSUBRAMO"),	
	    	@FieldResult(name="idMoneda", column="IDMONEDA"),	
	    	@FieldResult(name="reaseguradorCorredorDTO.idtcreaseguradorcorredor", column="IDTCREASEGURADOR"),	
	    	@FieldResult(name="idPoliza", column="IDTOPOLIZA"),	
	    	@FieldResult(name="contratoCuotaParteDTO.idTmContratoCuotaParte", column="IDTMCONTRATOCUOTAPARTE"),	
	    	@FieldResult(name="contratoPrimerExcedenteDTO.idTmContratoPrimerExcedente", column="IDTMCONTRATOPRIMEREXCEDENTE"),	
	    	@FieldResult(name="contratoFacultativoDTO.idTmContratoFacultativo", column="IDTMCONTRATOFACULTATIVO"),	
	    	@FieldResult(name="observaciones", column="OBSERVACIONES"),	
	    	@FieldResult(name="tipoReaseguro", column="IDTCTIPOREASEGURO"),	
	    	@FieldResult(name="lineaDTO.idTmLinea", column="IDLINEA"),		    	
	    	@FieldResult(name="ejercicio", column="EJERCICIO"),	
	    	@FieldResult(name="suscripcion", column="SUSCRIPCION"),	
	    	@FieldResult(name="numeroEndoso", column="NUMEROENDOSO"),		    	
	    	@FieldResult(name="corredorDTO.idtcreaseguradorcorredor", column="IDTCCORREDOR"),	
	    	@FieldResult(name="estatusAutorizacionPagoFacultativoPorEstadoCuenta", column="ESTATUSAUTPAGOFACEDOCUENTA"),	
	    	@FieldResult(name="fechaSolicitudAutorizacionPagoFacultativoPorEstadoCuenta", column="FECHASOLAUTPAGOFACEDOCUENTA"),	
	    	@FieldResult(name="usuarioSolicitudAutorizacionPagoFacultativoPorEstadoCuenta", column="USUARIOSOLAUTPAGOFACEDOCUENTA"),	
	    	@FieldResult(name="fechaAutorizacionPagoFacultativoPorEstadoCuenta", column="FECHAAUTPAGOFACEDOCUENTA"),	
	    	@FieldResult(name="usuarioAutorizacionPagoFacultativoPorEstadoCuenta", column="USUARIOAUTPAGOFACEDOCUENTA")	        
	    })
	}
)

@Entity
@Table(name="TOESTADOCUENTA", schema="MIDAS")
public class EstadoCuentaDTO  implements java.io.Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	 // Fields    
     private BigDecimal idEstadoCuenta;
     private ContratoFacultativoDTO contratoFacultativoDTO;
	 private ContratoCuotaParteDTO contratoCuotaParteDTO;
     private ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO;
     private LineaDTO lineaDTO;
     //private BigDecimal idSubRamo;
     private SubRamoDTO subRamoDTO;
     private int idMoneda;
     private ReaseguradorCorredorDTO reaseguradorCorredorDTO;
     private ReaseguradorCorredorDTO corredorDTO;
     //private BigDecimal idReasegurador;
     private BigDecimal idPoliza;
     private Integer numeroEndoso;
     private String observaciones;
     
     private int tipoReaseguro;
     
     private int ejercicio;
     private int suscripcion;   
     
     private Short estatusAutorizacionPagoFacultativoPorEstadoCuenta; 
     private Date fechaSolicitudAutorizacionPagoFacultativoPorEstadoCuenta;
     private String usuarioSolicitudAutorizacionPagoFacultativoPorEstadoCuenta;
     private Date fechaAutorizacionPagoFacultativoPorEstadoCuenta;
     private String usuarioAutorizacionPagoFacultativoPorEstadoCuenta;

     private List<AcumuladorDTO> acumuladorDTOs = new ArrayList<AcumuladorDTO>(0);
     
     private EstadoCuentaDTO estadoCuentaSuscripcionAnterior;


    // Constructors

    /** default constructor */
    public EstadoCuentaDTO() {
    }

    public EstadoCuentaDTO duplicate(){
    	EstadoCuentaDTO estadoCuentaDuplicado = new EstadoCuentaDTO();
    	estadoCuentaDuplicado.setAcumuladorDTOs(getAcumuladorDTOs());
    	estadoCuentaDuplicado.setContratoCuotaParteDTO(getContratoCuotaParteDTO());
    	estadoCuentaDuplicado.setContratoFacultativoDTO(getContratoFacultativoDTO());
    	estadoCuentaDuplicado.setContratoPrimerExcedenteDTO(getContratoPrimerExcedenteDTO());
    	estadoCuentaDuplicado.setCorredorDTO(getCorredorDTO());
    	estadoCuentaDuplicado.setEjercicio(getEjercicio());
    	estadoCuentaDuplicado.setEstadoCuentaSuscripcionAnterior(getEstadoCuentaSuscripcionAnterior());
    	estadoCuentaDuplicado.setIdEstadoCuenta(getIdEstadoCuenta());
    	estadoCuentaDuplicado.setIdMoneda(getIdMoneda());
    	estadoCuentaDuplicado.setIdPoliza(getIdPoliza());
    	estadoCuentaDuplicado.setLineaDTO(getLineaDTO());
    	estadoCuentaDuplicado.setNumeroEndoso(getNumeroEndoso());
    	estadoCuentaDuplicado.setObservaciones(getObservaciones());
    	estadoCuentaDuplicado.setReaseguradorCorredorDTO(getReaseguradorCorredorDTO());
    	estadoCuentaDuplicado.setSubRamoDTO(getSubRamoDTO());
    	estadoCuentaDuplicado.setSuscripcion(getSuscripcion());
    	estadoCuentaDuplicado.setTipoReaseguro(getTipoReaseguro());
    	return estadoCuentaDuplicado;
    }
   
    // Property accessors
    @Id 
	@SequenceGenerator(name = "IDTOESTADOCUENTA_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOESTADOCUENTA_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOESTADOCUENTA_SEQ_GENERADOR")	     
    @Column(name="IDTOESTADOCUENTA", unique=true, nullable=false, precision=22, scale=0)
    public BigDecimal getIdEstadoCuenta() {
        return this.idEstadoCuenta;
    }
    
    public void setIdEstadoCuenta(BigDecimal idEstadoCuenta) {
        this.idEstadoCuenta = idEstadoCuenta;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTMCONTRATOFACULTATIVO")

    public ContratoFacultativoDTO getContratoFacultativoDTO() {
        return this.contratoFacultativoDTO;
    }
    
    public void setContratoFacultativoDTO(ContratoFacultativoDTO contratoFacultativoDTO) {
        this.contratoFacultativoDTO = contratoFacultativoDTO;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTMCONTRATOCUOTAPARTE")

     public ContratoCuotaParteDTO getContratoCuotaParteDTO() {
		return contratoCuotaParteDTO;
	}


	public void setContratoCuotaParteDTO(ContratoCuotaParteDTO contratoCuotaParteDTO) {
		this.contratoCuotaParteDTO = contratoCuotaParteDTO;
	}

	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTMCONTRATOPRIMEREXCEDENTE")

    public ContratoPrimerExcedenteDTO getContratoPrimerExcedenteDTO() {
		return contratoPrimerExcedenteDTO;
	}


	public void setContratoPrimerExcedenteDTO(
			ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO) {
		this.contratoPrimerExcedenteDTO = contratoPrimerExcedenteDTO;
	}
   
    /*@Column(name="IDTCSUBRAMO", nullable=false, precision=22, scale=0)
    public BigDecimal getIdSubRamo() {
        return this.idSubRamo;
    }
    
    public void setIdSubRamo(BigDecimal idSubRamo) {
        this.idSubRamo = idSubRamo;
    }*/
    
    
    @Column(name="IDMONEDA", nullable=false, precision=2, scale=0)

    public int getIdMoneda() {
        return this.idMoneda;
    }
    
    public void setIdMoneda(int idMoneda) {
        this.idMoneda = idMoneda;
    }
    
    /*
    @Column(name="IDTCREASEGURADOR", nullable=false, precision=22, scale=0)
    public BigDecimal getIdReasegurador() {
        return this.idReasegurador;
    }
    
    public void setIdReasegurador(BigDecimal idReasegurador) {
        this.idReasegurador = idReasegurador;
    }*/
    
    @Column(name="IDTOPOLIZA", precision=22, scale=0)
    public BigDecimal getIdPoliza() {
        return this.idPoliza;
    }
    
    public void setIdPoliza(BigDecimal idPoliza) {
        this.idPoliza = idPoliza;
    }
    
    @Column(name="NUMEROENDOSO", precision=4, scale=0)

    public Integer getNumeroEndoso() {
        return this.numeroEndoso;
    }
    
    public void setNumeroEndoso(Integer numeroEndoso) {
        this.numeroEndoso = numeroEndoso;
    }
    
    @Column(name="OBSERVACIONES", length=300)

    public String getObservaciones() {
        return this.observaciones;
    }
    
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
    

    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="estadoCuentaDTO")
    public List<AcumuladorDTO> getAcumuladorDTOs() {
        return this.acumuladorDTOs;
    }
    
    public void setAcumuladorDTOs(List<AcumuladorDTO> acumuladorDTOs) {
        this.acumuladorDTOs = acumuladorDTOs;
    }
   

	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTCSUBRAMO", nullable=false)
	public SubRamoDTO getSubRamoDTO() {
	    return this.subRamoDTO;
	}
	
	public void setSubRamoDTO(SubRamoDTO subRamoDTO) {
	    this.subRamoDTO = subRamoDTO;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDLINEA")
	public LineaDTO getLineaDTO() {
	    return this.lineaDTO;
	}
	
	public void setLineaDTO(LineaDTO lineaDTO) {
	    this.lineaDTO = lineaDTO;
	}	


	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTCREASEGURADOR", nullable=false)
	public ReaseguradorCorredorDTO getReaseguradorCorredorDTO() {
	    return this.reaseguradorCorredorDTO;
	}
	
	
	public void setReaseguradorCorredorDTO(ReaseguradorCorredorDTO reaseguradorCorredorDTO) {
	    this.reaseguradorCorredorDTO = reaseguradorCorredorDTO;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTCCORREDOR", nullable=true)
    public ReaseguradorCorredorDTO getCorredorDTO() {
		return corredorDTO;
	}

	public void setCorredorDTO(ReaseguradorCorredorDTO corredorDTO) {
		this.corredorDTO = corredorDTO;
	}

	@Column(name="IDTCTIPOREASEGURO", nullable=false, precision=2, scale=0)
	public int getTipoReaseguro() {
		return tipoReaseguro;
	}

	public void setTipoReaseguro(int tipoReaseguro) {
		this.tipoReaseguro = tipoReaseguro;
	}

    @Column(name="EJERCICIO", nullable=false, precision=4, scale=0)
    public int getEjercicio() {
        return this.ejercicio;
    }
    
    public void setEjercicio(int ejercicio) {
        this.ejercicio = ejercicio;
    }
    
    @Column(name="SUSCRIPCION", nullable=false, precision=4, scale=0)
    public int getSuscripcion() {
        return this.suscripcion;
    }
    
    public void setSuscripcion(int suscripcion) {
        this.suscripcion = suscripcion;
    }   

    @Column(name="ESTATUSAUTPAGOFACEDOCUENTA", nullable=true, precision=2, scale=0)
    public Short getEstatusAutorizacionPagoFacultativoPorEstadoCuenta() {
		return estatusAutorizacionPagoFacultativoPorEstadoCuenta;
	}

	public void setEstatusAutorizacionPagoFacultativoPorEstadoCuenta(
			Short estatusAutorizacionPagoFacultativoPorEstadoCuenta) {
		this.estatusAutorizacionPagoFacultativoPorEstadoCuenta = estatusAutorizacionPagoFacultativoPorEstadoCuenta;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHASOLAUTPAGOFACEDOCUENTA", nullable=true)	
	public Date getFechaSolicitudAutorizacionPagoFacultativoPorEstadoCuenta() {
		return fechaSolicitudAutorizacionPagoFacultativoPorEstadoCuenta;
	}

	public void setFechaSolicitudAutorizacionPagoFacultativoPorEstadoCuenta(
			Date fechaSolicitudAutorizacionPagoFacultativoPorEstadoCuenta) {
		this.fechaSolicitudAutorizacionPagoFacultativoPorEstadoCuenta = fechaSolicitudAutorizacionPagoFacultativoPorEstadoCuenta;
	}

	@Column(name="USUARIOSOLAUTPAGOFACEDOCUENTA", nullable=true)
	public String getUsuarioSolicitudAutorizacionPagoFacultativoPorEstadoCuenta() {
		return usuarioSolicitudAutorizacionPagoFacultativoPorEstadoCuenta;
	}

	public void setUsuarioSolicitudAutorizacionPagoFacultativoPorEstadoCuenta(
			String usuarioSolicitudAutorizacionPagoFacultativoPorEstadoCuenta) {
		this.usuarioSolicitudAutorizacionPagoFacultativoPorEstadoCuenta = usuarioSolicitudAutorizacionPagoFacultativoPorEstadoCuenta;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHAAUTPAGOFACEDOCUENTA", nullable=true)
	public Date getFechaAutorizacionPagoFacultativoPorEstadoCuenta() {
		return fechaAutorizacionPagoFacultativoPorEstadoCuenta;
	}

	public void setFechaAutorizacionPagoFacultativoPorEstadoCuenta(
			Date fechaAutorizacionPagoFacultativoPorEstadoCuenta) {
		this.fechaAutorizacionPagoFacultativoPorEstadoCuenta = fechaAutorizacionPagoFacultativoPorEstadoCuenta;
	}

	@Column(name="USUARIOAUTPAGOFACEDOCUENTA", nullable=true)
	public String getUsuarioAutorizacionPagoFacultativoPorEstadoCuenta() {
		return usuarioAutorizacionPagoFacultativoPorEstadoCuenta;
	}

	public void setUsuarioAutorizacionPagoFacultativoPorEstadoCuenta(
			String usuarioAutorizacionPagoFacultativoPorEstadoCuenta) {
		this.usuarioAutorizacionPagoFacultativoPorEstadoCuenta = usuarioAutorizacionPagoFacultativoPorEstadoCuenta;
	}

	@Transient
	public EstadoCuentaDTO getEstadoCuentaSuscripcionAnterior() {
		return estadoCuentaSuscripcionAnterior;
	}

	public void setEstadoCuentaSuscripcionAnterior(EstadoCuentaDTO estadoCuentaSuscripcionAnterior) {
		this.estadoCuentaSuscripcionAnterior = estadoCuentaSuscripcionAnterior;
	}
	
	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object != null && object instanceof EstadoCuentaDTO) {
			EstadoCuentaDTO estadoCuentaDTO = (EstadoCuentaDTO) object;
			if(estadoCuentaDTO.getIdEstadoCuenta() != null && this.getIdEstadoCuenta() != null)
				equal = estadoCuentaDTO.getIdEstadoCuenta().compareTo(this.getIdEstadoCuenta())==0;
		} // End of if
		return equal;
	}
	
	@Override
	public int hashCode(){
		if(this.idEstadoCuenta != null)
			return this.idEstadoCuenta.hashCode();
		else
			return super.hashCode();
	}
}