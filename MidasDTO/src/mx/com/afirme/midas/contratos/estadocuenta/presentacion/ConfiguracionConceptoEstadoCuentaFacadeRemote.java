package mx.com.afirme.midas.contratos.estadocuenta.presentacion;
// default package

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for ConfiguracionConceptoEstadoCuentaDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ConfiguracionConceptoEstadoCuentaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ConfiguracionConceptoEstadoCuentaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ConfiguracionConceptoEstadoCuentaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ConfiguracionConceptoEstadoCuentaDTO entity);
    /**
	 Delete a persistent ConfiguracionConceptoEstadoCuentaDTO entity.
	  @param entity ConfiguracionConceptoEstadoCuentaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ConfiguracionConceptoEstadoCuentaDTO entity);
   /**
	 Persist a previously saved ConfiguracionConceptoEstadoCuentaDTO entity and return it or a copy of it to the sender. 
	 A copy of the ConfiguracionConceptoEstadoCuentaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ConfiguracionConceptoEstadoCuentaDTO entity to update
	 @return ConfiguracionConceptoEstadoCuentaDTO the persisted ConfiguracionConceptoEstadoCuentaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ConfiguracionConceptoEstadoCuentaDTO update(ConfiguracionConceptoEstadoCuentaDTO entity);
	public ConfiguracionConceptoEstadoCuentaDTO findById( Integer id);
	 /**
	 * Find all ConfiguracionConceptoEstadoCuentaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ConfiguracionConceptoEstadoCuentaDTO property to query
	  @param value the property value to match
	  	  @return List<ConfiguracionConceptoEstadoCuentaDTO> found by query
	 */
	public List<ConfiguracionConceptoEstadoCuentaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ConfiguracionConceptoEstadoCuentaDTO entities.
	  	  @return List<ConfiguracionConceptoEstadoCuentaDTO> all ConfiguracionConceptoEstadoCuentaDTO entities
	 */
	public List<ConfiguracionConceptoEstadoCuentaDTO> findAll(
		);	
}