package mx.com.afirme.midas.contratos.estadocuenta.reportes;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.contratos.linea.SuscripcionDTO;

public class ReporteSaldoTrimestralDTO implements Serializable{
	private static final long serialVersionUID = -7933345690914609866L;

	private BigDecimal idTcReasegurador;
	private String nombreReasegurador;
	private String idTcMoneda;
	private String descripcionMoneda;
	private BigDecimal montoTotalSaldoAnterior;
	private BigDecimal montoTotalPrimaMes1;
	private BigDecimal montoTotalPrimaMes2;
	private BigDecimal montoTotalPrimaMes3;
	private BigDecimal montoTotalComisionMes1;
	private BigDecimal montoTotalComisionMes2;
	private BigDecimal montoTotalComisionMes3;
	private BigDecimal montoTotalSiniestroMes1;
	private BigDecimal montoTotalSiniestroMes2;
	private BigDecimal montoTotalSiniestroMes3;
	private BigDecimal montoTotalPrimaTrimestre;
	private BigDecimal montoTotalComisionTrimestre;
	private BigDecimal montoTotalSiniestroTrimestre;
	private BigDecimal montoTotalSaldoTecnicoTrimestre;
	private BigDecimal montoTotalPrimaFacultativoMes1;
	private BigDecimal montoTotalPrimaFacultativoMes2;
	private BigDecimal montoTotalPrimaFacultativoMes3;
	private BigDecimal montoTotalComisionFacultativoMes1;
	private BigDecimal montoTotalComisionFacultativoMes2;
	private BigDecimal montoTotalComisionFacultativoMes3;
	private BigDecimal montoTotalSiniestroFacultativoMes1;
	private BigDecimal montoTotalSiniestroFacultativoMes2;
	private BigDecimal montoTotalSiniestroFacultativoMes3;
	private BigDecimal montoTotalPrimaFacultativoTrimestre;
	private BigDecimal montoTotalComisionFacultativoTrimestre;
	private BigDecimal montoTotalSiniestroFacultativoTrimestre;
	private BigDecimal montoTotalSaldoTecnicoFacultativoTrimestre;
	private BigDecimal montoTotalSaldoTecnicoProporcionalMasFacultativoTrimestre;
	
	private BigDecimal montoTotalRemesaSaldo;
	private BigDecimal montoTotalArrastreSaldo;
	
	private SuscripcionDTO suscripcionDTO;
	private SuscripcionDTO suscripcionAnteriorDTO;
	
	private List<ReporteSaldoPorSubRamoDTO> listaSaldosSubRamo;

	public ReporteSaldoTrimestralDTO(){
		montoTotalSaldoAnterior = BigDecimal.ZERO;
		montoTotalPrimaMes1 = montoTotalPrimaMes2 = montoTotalPrimaMes3 = BigDecimal.ZERO;
		montoTotalComisionMes1 = montoTotalComisionMes2 = montoTotalComisionMes3 = BigDecimal.ZERO;
		montoTotalSiniestroMes1 = montoTotalSiniestroMes2 = montoTotalSiniestroMes3 = BigDecimal.ZERO;
		montoTotalSaldoTecnicoTrimestre = BigDecimal.ZERO;
		montoTotalPrimaFacultativoMes1 = montoTotalPrimaFacultativoMes2 = montoTotalPrimaFacultativoMes3 = BigDecimal.ZERO;
		montoTotalComisionFacultativoMes1 = montoTotalComisionFacultativoMes2 = montoTotalComisionFacultativoMes3 = BigDecimal.ZERO;
		montoTotalSiniestroFacultativoMes1= montoTotalSiniestroFacultativoMes2 = montoTotalSiniestroFacultativoMes3 = BigDecimal.ZERO;
		montoTotalPrimaFacultativoTrimestre = montoTotalComisionFacultativoTrimestre = montoTotalSiniestroFacultativoTrimestre = BigDecimal.ZERO;
		montoTotalSaldoTecnicoFacultativoTrimestre = BigDecimal.ZERO;
		montoTotalPrimaTrimestre = montoTotalComisionTrimestre = montoTotalSiniestroTrimestre = BigDecimal.ZERO;
		listaSaldosSubRamo = new ArrayList<ReporteSaldoPorSubRamoDTO>();
		montoTotalRemesaSaldo = montoTotalArrastreSaldo = BigDecimal.ZERO;
	}
	
	public BigDecimal getIdTcReasegurador() {
		return idTcReasegurador;
	}

	public void setIdTcReasegurador(BigDecimal idTcReasegurador) {
		this.idTcReasegurador = idTcReasegurador;
	}

	public String getNombreReasegurador() {
		return nombreReasegurador;
	}

	public void setNombreReasegurador(String nombreReasegurador) {
		this.nombreReasegurador = nombreReasegurador;
	}

	public String getIdTcMoneda() {
		return idTcMoneda;
	}

	public void setIdTcMoneda(String idTcMoneda) {
		this.idTcMoneda = idTcMoneda;
	}

	public String getDescripcionMoneda() {
		return descripcionMoneda;
	}

	public void setDescripcionMoneda(String descripcionMoneda) {
		this.descripcionMoneda = descripcionMoneda;
	}

	public BigDecimal getMontoTotalSaldoAnterior() {
		return montoTotalSaldoAnterior;
	}

	public void setMontoTotalSaldoAnterior(BigDecimal montoTotalSaldoAnterior) {
		this.montoTotalSaldoAnterior = montoTotalSaldoAnterior;
	}

	public BigDecimal getMontoTotalPrimaMes1() {
		return montoTotalPrimaMes1;
	}

	public void setMontoTotalPrimaMes1(BigDecimal montoTotalPrimaMes1) {
		this.montoTotalPrimaMes1 = montoTotalPrimaMes1;
	}

	public BigDecimal getMontoTotalPrimaMes2() {
		return montoTotalPrimaMes2;
	}

	public void setMontoTotalPrimaMes2(BigDecimal montoTotalPrimaMes2) {
		this.montoTotalPrimaMes2 = montoTotalPrimaMes2;
	}

	public BigDecimal getMontoTotalPrimaMes3() {
		return montoTotalPrimaMes3;
	}

	public void setMontoTotalPrimaMes3(BigDecimal montoTotalPrimaMes3) {
		this.montoTotalPrimaMes3 = montoTotalPrimaMes3;
	}

	public BigDecimal getMontoTotalComisionMes1() {
		return montoTotalComisionMes1;
	}

	public void setMontoTotalComisionMes1(BigDecimal montoTotalComisionMes1) {
		this.montoTotalComisionMes1 = montoTotalComisionMes1;
	}

	public BigDecimal getMontoTotalComisionMes2() {
		return montoTotalComisionMes2;
	}

	public void setMontoTotalComisionMes2(BigDecimal montoTotalComisionMes2) {
		this.montoTotalComisionMes2 = montoTotalComisionMes2;
	}

	public BigDecimal getMontoTotalComisionMes3() {
		return montoTotalComisionMes3;
	}

	public void setMontoTotalComisionMes3(BigDecimal montoTotalComisionMes3) {
		this.montoTotalComisionMes3 = montoTotalComisionMes3;
	}

	public BigDecimal getMontoTotalSiniestroMes1() {
		return montoTotalSiniestroMes1;
	}

	public void setMontoTotalSiniestroMes1(BigDecimal montoTotalSiniestroMes1) {
		this.montoTotalSiniestroMes1 = montoTotalSiniestroMes1;
	}

	public BigDecimal getMontoTotalSiniestroMes2() {
		return montoTotalSiniestroMes2;
	}

	public void setMontoTotalSiniestroMes2(BigDecimal montoTotalSiniestroMes2) {
		this.montoTotalSiniestroMes2 = montoTotalSiniestroMes2;
	}

	public BigDecimal getMontoTotalSiniestroMes3() {
		return montoTotalSiniestroMes3;
	}

	public void setMontoTotalSiniestroMes3(BigDecimal montoTotalSiniestroMes3) {
		this.montoTotalSiniestroMes3 = montoTotalSiniestroMes3;
	}

	public BigDecimal getMontoTotalPrimaFacultativoMes1() {
		return montoTotalPrimaFacultativoMes1;
	}

	public void setMontoTotalPrimaFacultativoMes1(
			BigDecimal montoTotalPrimaFacultativoMes1) {
		this.montoTotalPrimaFacultativoMes1 = montoTotalPrimaFacultativoMes1;
	}

	public BigDecimal getMontoTotalPrimaFacultativoMes2() {
		return montoTotalPrimaFacultativoMes2;
	}

	public void setMontoTotalPrimaFacultativoMes2(
			BigDecimal montoTotalPrimaFacultativoMes2) {
		this.montoTotalPrimaFacultativoMes2 = montoTotalPrimaFacultativoMes2;
	}

	public BigDecimal getMontoTotalPrimaFacultativoMes3() {
		return montoTotalPrimaFacultativoMes3;
	}

	public void setMontoTotalPrimaFacultativoMes3(
			BigDecimal montoTotalPrimaFacultativoMes3) {
		this.montoTotalPrimaFacultativoMes3 = montoTotalPrimaFacultativoMes3;
	}

	public BigDecimal getMontoTotalComisionFacultativoMes1() {
		return montoTotalComisionFacultativoMes1;
	}

	public void setMontoTotalComisionFacultativoMes1(
			BigDecimal montoTotalComisionFacultativoMes1) {
		this.montoTotalComisionFacultativoMes1 = montoTotalComisionFacultativoMes1;
	}

	public BigDecimal getMontoTotalComisionFacultativoMes2() {
		return montoTotalComisionFacultativoMes2;
	}

	public void setMontoTotalComisionFacultativoMes2(
			BigDecimal montoTotalComisionFacultativoMes2) {
		this.montoTotalComisionFacultativoMes2 = montoTotalComisionFacultativoMes2;
	}

	public BigDecimal getMontoTotalComisionFacultativoMes3() {
		return montoTotalComisionFacultativoMes3;
	}

	public void setMontoTotalComisionFacultativoMes3(
			BigDecimal montoTotalComisionFacultativoMes3) {
		this.montoTotalComisionFacultativoMes3 = montoTotalComisionFacultativoMes3;
	}

	public BigDecimal getMontoTotalPrimaTrimestre() {
		return montoTotalPrimaTrimestre;
	}

	public void setMontoTotalPrimaTrimestre(BigDecimal montoTotalPrimaTrimestre) {
		this.montoTotalPrimaTrimestre = montoTotalPrimaTrimestre;
	}

	public BigDecimal getMontoTotalComisionTrimestre() {
		return montoTotalComisionTrimestre;
	}

	public void setMontoTotalComisionTrimestre(
			BigDecimal montoTotalComisionTrimestre) {
		this.montoTotalComisionTrimestre = montoTotalComisionTrimestre;
	}

	public BigDecimal getMontoTotalSiniestroTrimestre() {
		return montoTotalSiniestroTrimestre;
	}

	public void setMontoTotalSiniestroTrimestre(
			BigDecimal montoTotalSiniestroTrimestre) {
		this.montoTotalSiniestroTrimestre = montoTotalSiniestroTrimestre;
	}

	public BigDecimal getMontoTotalSaldoTecnicoTrimestre() {
		return montoTotalSaldoTecnicoTrimestre;
	}

	public void setMontoTotalSaldoTecnicoTrimestre(
			BigDecimal montoTotalSaldoTecnicoTrimestre) {
		this.montoTotalSaldoTecnicoTrimestre = montoTotalSaldoTecnicoTrimestre;
	}

	public List<ReporteSaldoPorSubRamoDTO> getListaSaldosSubRamo() {
		return listaSaldosSubRamo;
	}

	public void setListaSaldosSubRamo(
			List<ReporteSaldoPorSubRamoDTO> listaSaldosSubRamo) {
		this.listaSaldosSubRamo = listaSaldosSubRamo;
	}
	public SuscripcionDTO getSuscripcionDTO() {
		return suscripcionDTO;
	}
	public void setSuscripcionDTO(SuscripcionDTO suscripcionDTO) {
		this.suscripcionDTO = suscripcionDTO;
	}
	public String obtenerDescripcionSuscripcionAnterior(){
		String descripcion = "";
		if(suscripcionAnteriorDTO != null && suscripcionAnteriorDTO.getDescripcion() != null){
			descripcion = suscripcionAnteriorDTO.getDescripcion();
		}
		return descripcion;
	}

	public SuscripcionDTO getSuscripcionAnteriorDTO() {
		return suscripcionAnteriorDTO;
	}

	public void setSuscripcionAnteriorDTO(SuscripcionDTO suscripcionAnteriorDTO) {
		this.suscripcionAnteriorDTO = suscripcionAnteriorDTO;
	}

	public BigDecimal getMontoTotalSiniestroFacultativoMes1() {
		return montoTotalSiniestroFacultativoMes1;
	}

	public void setMontoTotalSiniestroFacultativoMes1(BigDecimal montoTotalSiniestroFacultativoMes1) {
		this.montoTotalSiniestroFacultativoMes1 = montoTotalSiniestroFacultativoMes1;
	}

	public BigDecimal getMontoTotalSiniestroFacultativoMes2() {
		return montoTotalSiniestroFacultativoMes2;
	}

	public void setMontoTotalSiniestroFacultativoMes2(BigDecimal montoTotalSiniestroFacultativoMes2) {
		this.montoTotalSiniestroFacultativoMes2 = montoTotalSiniestroFacultativoMes2;
	}

	public BigDecimal getMontoTotalSiniestroFacultativoMes3() {
		return montoTotalSiniestroFacultativoMes3;
	}

	public void setMontoTotalSiniestroFacultativoMes3(BigDecimal montoTotalSiniestroFacultativoMes3) {
		this.montoTotalSiniestroFacultativoMes3 = montoTotalSiniestroFacultativoMes3;
	}

	public BigDecimal getMontoTotalPrimaFacultativoTrimestre() {
		return montoTotalPrimaFacultativoTrimestre;
	}

	public void setMontoTotalPrimaFacultativoTrimestre(BigDecimal montoTotalPrimaFacultativoTrimestre) {
		this.montoTotalPrimaFacultativoTrimestre = montoTotalPrimaFacultativoTrimestre;
	}

	public BigDecimal getMontoTotalComisionFacultativoTrimestre() {
		return montoTotalComisionFacultativoTrimestre;
	}

	public void setMontoTotalComisionFacultativoTrimestre(BigDecimal montoTotalComisionFacultativoTrimestre) {
		this.montoTotalComisionFacultativoTrimestre = montoTotalComisionFacultativoTrimestre;
	}

	public BigDecimal getMontoTotalSiniestroFacultativoTrimestre() {
		return montoTotalSiniestroFacultativoTrimestre;
	}

	public void setMontoTotalSiniestroFacultativoTrimestre(BigDecimal montoTotalSiniestroFacultativoTrimestre) {
		this.montoTotalSiniestroFacultativoTrimestre = montoTotalSiniestroFacultativoTrimestre;
	}
	
	public BigDecimal getMontoTotalSaldoTecnicoFacultativoTrimestre() {
		return montoTotalSaldoTecnicoFacultativoTrimestre;
	}
	public void setMontoTotalSaldoTecnicoFacultativoTrimestre(BigDecimal montoTotalSaldoTecnicoFacultativoTrimestre) {
		this.montoTotalSaldoTecnicoFacultativoTrimestre = montoTotalSaldoTecnicoFacultativoTrimestre;
	}
	public BigDecimal getMontoTotalSaldoTecnicoProporcionalMasFacultativoTrimestre() {
		if(montoTotalSaldoTecnicoProporcionalMasFacultativoTrimestre== null || montoTotalSaldoTecnicoProporcionalMasFacultativoTrimestre.compareTo(BigDecimal.ZERO) == 0)
			montoTotalSaldoTecnicoProporcionalMasFacultativoTrimestre = montoTotalSaldoTecnicoFacultativoTrimestre.add(montoTotalSaldoTecnicoTrimestre);
		return montoTotalSaldoTecnicoProporcionalMasFacultativoTrimestre;
	}
	public void setMontoTotalSaldoTecnicoProporcionalMasFacultativoTrimestre(BigDecimal montoTotalSaldoTecnicoProporcionalMasFacultativoTrimestre) {
		this.montoTotalSaldoTecnicoProporcionalMasFacultativoTrimestre = montoTotalSaldoTecnicoProporcionalMasFacultativoTrimestre;
	}
	public BigDecimal getMontoTotalRemesaSaldo() {
		return montoTotalRemesaSaldo;
	}
	public void setMontoTotalRemesaSaldo(BigDecimal montoTotalRemesaSaldo) {
		this.montoTotalRemesaSaldo = montoTotalRemesaSaldo;
	}
	public BigDecimal getMontoTotalArrastreSaldo() {
		return montoTotalArrastreSaldo;
	}
	public void setMontoTotalArrastreSaldo(BigDecimal montoTotalArrastreSaldo) {
		this.montoTotalArrastreSaldo = montoTotalArrastreSaldo;
	}
}
