package mx.com.afirme.midas.contratos.estadocuenta;
// default package

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for ConceptoAcumuladorFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ConceptoAcumuladorFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ConceptoAcumuladorDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ConceptoAcumuladorDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ConceptoAcumuladorDTO entity);
    /**
	 Delete a persistent ConceptoAcumuladorDTO entity.
	  @param entity ConceptoAcumuladorDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ConceptoAcumuladorDTO entity);
   /**
	 Persist a previously saved ConceptoAcumuladorDTO entity and return it or a copy of it to the sender. 
	 A copy of the ConceptoAcumuladorDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ConceptoAcumuladorDTO entity to update
	 @return ConceptoAcumuladorDTO the persisted ConceptoAcumuladorDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ConceptoAcumuladorDTO update(ConceptoAcumuladorDTO entity);
	public ConceptoAcumuladorDTO findById( ConceptoAcumuladorDTOId id);
	 /**
	 * Find all ConceptoAcumuladorDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ConceptoAcumuladorDTO property to query
	  @param value the property value to match
	  	  @return List<ConceptoAcumuladorDTO> found by query
	 */
	public List<ConceptoAcumuladorDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ConceptoAcumuladorDTO entities.
	  	  @return List<ConceptoAcumuladorDTO> all ConceptoAcumuladorDTO entities
	 */
	public List<ConceptoAcumuladorDTO> findAll(
		);	
}