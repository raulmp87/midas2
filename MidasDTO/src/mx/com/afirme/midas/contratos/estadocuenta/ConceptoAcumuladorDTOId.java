package mx.com.afirme.midas.contratos.estadocuenta;
// default package

import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * ConceptoAcumuladorDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class ConceptoAcumuladorDTOId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int idConceptoMovimiento;
     private int idMoneda;


    // Constructors

    /** default constructor */
    public ConceptoAcumuladorDTOId() {
    }

    
    /** full constructor */
    public ConceptoAcumuladorDTOId(int idConceptoMovimiento, int idMoneda) {
        this.idConceptoMovimiento = idConceptoMovimiento;
        this.idMoneda = idMoneda;
    }

   
    // Property accessors

    @Column(name="IDTCCONCEPTOMOVIMIENTO", nullable=false, precision=2, scale=0)

    public int getIdConceptoMovimiento() {
        return this.idConceptoMovimiento;
    }
    
    public void setIdConceptoMovimiento(int idConceptoMovimiento) {
        this.idConceptoMovimiento = idConceptoMovimiento;
    }

    @Column(name="IDMONEDA", nullable=false, precision=3, scale=0)

    public int getIdMoneda() {
        return this.idMoneda;
    }
    
    public void setIdMoneda(int idMoneda) {
        this.idMoneda = idMoneda;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof ConceptoAcumuladorDTOId) ) return false;
		 ConceptoAcumuladorDTOId castOther = ( ConceptoAcumuladorDTOId ) other; 
         
		 return (this.getIdConceptoMovimiento()==castOther.getIdConceptoMovimiento())
 && (this.getIdMoneda()==castOther.getIdMoneda());
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + this.getIdConceptoMovimiento();
         result = 37 * result + this.getIdMoneda();
         return result;
   }   





}