package mx.com.afirme.midas.contratos.estadocuenta;

import java.io.Serializable;
import java.math.BigDecimal;

public class EstadoCuentaFiltroDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal idTcRamo;	
	
	private BigDecimal idTcSubRamo;
	
	private Integer tipoReaseguro;
	
	private BigDecimal idtcreaseguradorcorredor;
	
	private Integer ejercicio;
	
	private Integer suscripcion;
	
	private Integer idMoneda;
	private BigDecimal idToPoliza;
	private BigDecimal[] idToEstadoCuenta;
	private BigDecimal idTmContratoCuotaParte;
	private BigDecimal idTmContratoPrimerExcedente;
	private BigDecimal idTmContratoFacultativo;
	
	private Boolean incluirValidacionBonoPorNoSiniestro;


	public BigDecimal getIdTcRamo() {
		return idTcRamo;
	}

	public void setIdTcRamo(BigDecimal idTcRamo) {
		this.idTcRamo = idTcRamo;
	}

	public BigDecimal getIdTcSubRamo() {
		return idTcSubRamo;
	}

	public void setIdTcSubRamo(BigDecimal idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}

	public BigDecimal getIdtcreaseguradorcorredor() {
		return idtcreaseguradorcorredor;
	}

	public void setIdtcreaseguradorcorredor(BigDecimal idtcreaseguradorcorredor) {
		this.idtcreaseguradorcorredor = idtcreaseguradorcorredor;
	}

	public int getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(int ejercicio) {
		this.ejercicio = ejercicio;
	}

	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	public BigDecimal[] getIdToEstadoCuenta() {
		return idToEstadoCuenta;
	}

	public void setIdToEstadoCuenta(BigDecimal[] idToEstadoCuenta) {
		this.idToEstadoCuenta = idToEstadoCuenta;
	}

	public Integer getTipoReaseguro() {
		return tipoReaseguro;
	}

	public void setTipoReaseguro(Integer tipoReaseguro) {
		this.tipoReaseguro = tipoReaseguro;
	}

	public Integer getSuscripcion() {
		return suscripcion;
	}

	public void setSuscripcion(Integer suscripcion) {
		this.suscripcion = suscripcion;
	}

	public Integer getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(Integer idMoneda) {
		this.idMoneda = idMoneda;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public BigDecimal getIdTmContratoCuotaParte() {
		return idTmContratoCuotaParte;
	}

	public void setIdTmContratoCuotaParte(BigDecimal idTmContratoCuotaParte) {
		this.idTmContratoCuotaParte = idTmContratoCuotaParte;
	}

	public BigDecimal getIdTmContratoPrimerExcedente() {
		return idTmContratoPrimerExcedente;
	}

	public void setIdTmContratoPrimerExcedente(
			BigDecimal idTmContratoPrimerExcedente) {
		this.idTmContratoPrimerExcedente = idTmContratoPrimerExcedente;
	}

	public BigDecimal getIdTmContratoFacultativo() {
		return idTmContratoFacultativo;
	}

	public void setIdTmContratoFacultativo(BigDecimal idTmContratoFacultativo) {
		this.idTmContratoFacultativo = idTmContratoFacultativo;
	}

	public Boolean getIncluirValidacionBonoPorNoSiniestro() {
		return incluirValidacionBonoPorNoSiniestro;
	}

	public void setIncluirValidacionBonoPorNoSiniestro(Boolean incluirValidacionBonoPorNoSiniestro) {
		this.incluirValidacionBonoPorNoSiniestro = incluirValidacionBonoPorNoSiniestro;
	}
}
