package mx.com.afirme.midas.contratos.estadocuenta.presentacion;
// default package

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for DetalleConfiguracionEstadoCuentaDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface DetalleConfiguracionEstadoCuentaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved DetalleConfiguracionEstadoCuentaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DetalleConfiguracionEstadoCuentaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DetalleConfiguracionEstadoCuentaDTO entity);
    /**
	 Delete a persistent DetalleConfiguracionEstadoCuentaDTO entity.
	  @param entity DetalleConfiguracionEstadoCuentaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DetalleConfiguracionEstadoCuentaDTO entity);
   /**
	 Persist a previously saved DetalleConfiguracionEstadoCuentaDTO entity and return it or a copy of it to the sender. 
	 A copy of the DetalleConfiguracionEstadoCuentaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DetalleConfiguracionEstadoCuentaDTO entity to update
	 @return DetalleConfiguracionEstadoCuentaDTO the persisted DetalleConfiguracionEstadoCuentaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public DetalleConfiguracionEstadoCuentaDTO update(DetalleConfiguracionEstadoCuentaDTO entity);
	public DetalleConfiguracionEstadoCuentaDTO findById( Integer id);
	 /**
	 * Find all DetalleConfiguracionEstadoCuentaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DetalleConfiguracionEstadoCuentaDTO property to query
	  @param value the property value to match
	  	  @return List<DetalleConfiguracionEstadoCuentaDTO> found by query
	 */
	public List<DetalleConfiguracionEstadoCuentaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all DetalleConfiguracionEstadoCuentaDTO entities.
	  	  @return List<DetalleConfiguracionEstadoCuentaDTO> all DetalleConfiguracionEstadoCuentaDTO entities
	 */
	public List<DetalleConfiguracionEstadoCuentaDTO> findAll(
		);	
}