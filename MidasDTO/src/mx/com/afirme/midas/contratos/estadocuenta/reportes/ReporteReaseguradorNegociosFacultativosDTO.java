package mx.com.afirme.midas.contratos.estadocuenta.reportes;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.contratos.linea.SuscripcionDTO;

public class ReporteReaseguradorNegociosFacultativosDTO implements Serializable {
	private static final long serialVersionUID = -80803737365914082L;

	private BigDecimal idTcReasegurador;
	private String nombreReasegurador;
	private SuscripcionDTO suscripcionDTO;
	private List<ReporteSaldoPorNegocioFacultativoDTO> listaSaldosNegociosFacultativos;
	
	public ReporteReaseguradorNegociosFacultativosDTO(){
		listaSaldosNegociosFacultativos = new ArrayList<ReporteSaldoPorNegocioFacultativoDTO>();
	}
	
	public BigDecimal getIdTcReasegurador() {
		return idTcReasegurador;
	}
	public void setIdTcReasegurador(BigDecimal idTcReasegurador) {
		this.idTcReasegurador = idTcReasegurador;
	}
	public String getNombreReasegurador() {
		return nombreReasegurador;
	}
	public void setNombreReasegurador(String nombreReasegurador) {
		this.nombreReasegurador = nombreReasegurador;
	}
	public SuscripcionDTO getSuscripcionDTO() {
		return suscripcionDTO;
	}
	public void setSuscripcionDTO(SuscripcionDTO suscripcionDTO) {
		this.suscripcionDTO = suscripcionDTO;
	}
	public List<ReporteSaldoPorNegocioFacultativoDTO> getListaSaldosNegociosFacultativos() {
		return listaSaldosNegociosFacultativos;
	}
	public void setListaSaldosNegociosFacultativos(
			List<ReporteSaldoPorNegocioFacultativoDTO> listaSaldosNegociosFacultativos) {
		this.listaSaldosNegociosFacultativos = listaSaldosNegociosFacultativos;
	}
	
	
}
