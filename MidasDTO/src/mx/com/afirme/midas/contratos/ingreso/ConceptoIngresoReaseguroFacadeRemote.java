package mx.com.afirme.midas.contratos.ingreso;
// default package

import java.util.List;
import javax.ejb.Remote;


/**
 * Remote interface for TcconceptoingresoreaseguroFacade.
 * 
 * @author MyEclipse Persistence Tools
 */
  
public interface ConceptoIngresoReaseguroFacadeRemote {

	public ConceptoIngresoReaseguroDTO findById(Byte id);

	/**
	 * Find all ConceptoIngresoReaseguroDTO entities.
	 * 
	 * @return List<ConceptoIngresoReaseguroDTO> all ConceptoIngresoReaseguroDTO
	 *         entities
	 */
	public List<ConceptoIngresoReaseguroDTO> findAll();
}