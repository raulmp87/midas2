package mx.com.afirme.midas.contratos.ingreso;
// default package

import java.math.BigDecimal;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDTO;


/**
 * IngresoEstadoCuentaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TRINGRESOESTADOCUENTA"
    ,schema="MIDAS"
)
public class IngresoEstadoCuentaDTO  implements java.io.Serializable {


    // Fields    


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private IngresoEstadoCuentaId id;
     private EstadoCuentaDTO estadoCuentaDTO;
     private IngresoReaseguroDTO ingresoReaseguroDTO;
     private BigDecimal monto;


    // Constructors

    /** default constructor */
    public IngresoEstadoCuentaDTO() {
    }

    
   
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idIngresoReaseguro", column=@Column(name="IDTOINGRESOREASEGURO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idEstadoCuenta", column=@Column(name="IDTOESTADOCUENTA", nullable=false, precision=22, scale=0) ) } )

    public IngresoEstadoCuentaId getId() {
        return this.id;
    }
    
    public void setId(IngresoEstadoCuentaId id) {
        this.id = id;
    }

	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="IDTOESTADOCUENTA", nullable=false, insertable=false, updatable=false)
    public EstadoCuentaDTO getEstadoCuentaDTO() {
        return this.estadoCuentaDTO;
    }
    
    public void setEstadoCuentaDTO(EstadoCuentaDTO estadoCuentaDTO) {
        this.estadoCuentaDTO = estadoCuentaDTO;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="IDTOINGRESOREASEGURO", nullable=false, insertable=false, updatable=false)

    public IngresoReaseguroDTO getIngresoReaseguro() {
        return this.ingresoReaseguroDTO;
    }
    
    public void setIngresoReaseguro(IngresoReaseguroDTO ingresoReaseguroDTO) {
        this.ingresoReaseguroDTO = ingresoReaseguroDTO;
    }
    
    @Column(name="MONTO", nullable=false, precision=16)

    public BigDecimal getMonto() {
        return this.monto;
    }
    
    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }
   








}