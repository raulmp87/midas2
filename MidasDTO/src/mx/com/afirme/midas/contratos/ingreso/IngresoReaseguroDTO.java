package mx.com.afirme.midas.contratos.ingreso;
// default package

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



/**
 * IngresoReaseguroDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOINGRESOREASEGURO"
    ,schema="MIDAS"
)
public class IngresoReaseguroDTO  implements java.io.Serializable {


    // Fields    


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idIngresoReaseguro;
     private Double monto;
     private int idMoneda;
     private Double tipoCambio;
     private BigDecimal numeroCuenta;
     private String referencia;
 	 private ConceptoIngresoReaseguroDTO conceptoIngresoReaseguroDTO;     
     private Date fechaIngreso;
     private int estatus;
     private Set<IngresoEstadoCuentaDTO> ingresoEstadoCuentaDTOs = new HashSet<IngresoEstadoCuentaDTO>(0);
     
     private Double montoTotal;
     private Double montoVida;
     private Double montoOtros;
     private Double montoNoProporcionales;     
     private Integer contabiliza;
     private String idReferenciaExterna;
     


    // Constructors

    /** default constructor */
    public IngresoReaseguroDTO() {
    }
    

   
    // Property accessors
    @Id 
	@SequenceGenerator(name = "IDTOINGRESOREASEGURO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOINGRESOREASEGURO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOINGRESOREASEGURO_SEQ_GENERADOR")	    
    @Column(name="IDTOINGRESOREASEGURO", unique=true, nullable=false, precision=22, scale=0)
    public BigDecimal getIdIngresoReaseguro() {
        return this.idIngresoReaseguro;
    }
    
    public void setIdIngresoReaseguro(BigDecimal idIngresoReaseguro) {
        this.idIngresoReaseguro = idIngresoReaseguro;
    }
    
    @Column(name="MONTO", nullable=false, precision=18, scale=4)

    public Double getMonto() {
        return this.monto;
    }
    
    public void setMonto(Double monto) {
        this.monto = monto;
    }
    
    @Column(name="IDTCMONEDA", nullable=false, precision=3, scale=0)

    public int getIdMoneda() {
        return this.idMoneda;
    }
    
    public void setIdMoneda(int idMoneda) {
        this.idMoneda = idMoneda;
    }
    
    @Column(name="TIPOCAMBIO", precision=16)

    public Double getTipoCambio() {
        return this.tipoCambio;
    }
    
    public void setTipoCambio(Double tipoCambio) {
        this.tipoCambio = tipoCambio;
    }
    
    @Column(name="NUMEROCUENTA", precision=22, scale=0)

    public BigDecimal getNumeroCuenta() {
        return this.numeroCuenta;
    }
    
    public void setNumeroCuenta(BigDecimal numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }
    
    @Column(name="REFERENCIA", length=100)

    public String getReferencia() {
        return this.referencia;
    }
    
    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }
    

@Temporal(TemporalType.DATE)
    @Column(name="FECHAINGRESO", length=7)

    public Date getFechaIngreso() {
        return this.fechaIngreso;
    }
    
    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }
    
    @Column(name="ESTATUS", nullable=false, precision=22, scale=0)

    public int getEstatus() {
        return this.estatus;
    }
    
    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="ingresoReaseguro")

    public Set<IngresoEstadoCuentaDTO> getIngresoEstadoCuentas() {
        return this.ingresoEstadoCuentaDTOs;
    }
    
    public void setIngresoEstadoCuentas(Set<IngresoEstadoCuentaDTO> ingresoEstadoCuentaDTOs) {
        this.ingresoEstadoCuentaDTOs = ingresoEstadoCuentaDTOs;
    }

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCCONCEPTO", nullable = false)
	public ConceptoIngresoReaseguroDTO getConceptoIngresoReaseguro() {
		return this.conceptoIngresoReaseguroDTO;
	}

	public void setConceptoIngresoReaseguro(
			ConceptoIngresoReaseguroDTO conceptoIngresoReaseguroDTO) {
		this.conceptoIngresoReaseguroDTO = conceptoIngresoReaseguroDTO;
	}
	

    @Column(name = "MONTOTOTAL", precision = 18, scale = 4)
    public Double getMontoTotal() {
        return this.montoTotal;
    }

    public void setMontoTotal(Double montoTotal) {
        this.montoTotal = montoTotal;
    }

    @Column(name = "MONTOVIDA", precision = 18, scale = 4)
    public Double getMontoVida() {
        return this.montoVida;
    }

    public void setMontoVida(Double montoVida) {
        this.montoVida = montoVida;
    }

    @Column(name = "MONTOOTROS", precision = 18, scale = 4)
    public Double getMontoOtros() {
        return this.montoOtros;
    }

    public void setMontoOtros(Double montoOtros) {
        this.montoOtros = montoOtros;
    }

    @Column(name = "MONTONOPROPORCIONALES", precision = 18, scale = 4)
    public Double getMontoNoProporcionales() {
        return this.montoNoProporcionales;
    }

    public void setMontoNoProporcionales(Double montoNoProporcionales) {
        this.montoNoProporcionales = montoNoProporcionales;
    }	

    @Column(name = "CONTABILIZA", precision = 3, scale = 4)
    public Integer getContabiliza() {
        return this.contabiliza;
    }

    public void setContabiliza(Integer contabiliza) {
        this.contabiliza = contabiliza;
    }
    
    @Column(name = "IDREFERENCIAEXTERNA")
	public String getIdReferenciaExterna() {
		return idReferenciaExterna;
	}

	public void setIdReferenciaExterna(String idReferenciaExterna) {
		this.idReferenciaExterna = idReferenciaExterna;
	}
}