package mx.com.afirme.midas.contratos.ingreso;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * IngresoEstadoCuentaId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class IngresoEstadoCuentaId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private BigDecimal idIngresoReaseguro;
     private BigDecimal idEstadoCuenta;


    // Constructors

    /** default constructor */
    public IngresoEstadoCuentaId() {
    }

    
    /** full constructor */
    public IngresoEstadoCuentaId(BigDecimal idIngresoReaseguro, BigDecimal idEstadoCuenta) {
        this.idIngresoReaseguro = idIngresoReaseguro;
        this.idEstadoCuenta = idEstadoCuenta;
    }

   
    // Property accessors

    @Column(name="IDTOINGRESOREASEGURO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdIngresoReaseguro() {
        return this.idIngresoReaseguro;
    }
    
    public void setIdIngresoReaseguro(BigDecimal idIngresoReaseguro) {
        this.idIngresoReaseguro = idIngresoReaseguro;
    }

    @Column(name="IDTOESTADOCUENTA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdEstadoCuenta() {
        return this.idEstadoCuenta;
    }
    
    public void setIdEstadoCuenta(BigDecimal idEstadoCuenta) {
        this.idEstadoCuenta = idEstadoCuenta;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof IngresoEstadoCuentaId) ) return false;
		 IngresoEstadoCuentaId castOther = ( IngresoEstadoCuentaId ) other; 
         
		 return ( (this.getIdIngresoReaseguro()==castOther.getIdIngresoReaseguro()) || ( this.getIdIngresoReaseguro()!=null && castOther.getIdIngresoReaseguro()!=null && this.getIdIngresoReaseguro().equals(castOther.getIdIngresoReaseguro()) ) )
 && ( (this.getIdEstadoCuenta()==castOther.getIdEstadoCuenta()) || ( this.getIdEstadoCuenta()!=null && castOther.getIdEstadoCuenta()!=null && this.getIdEstadoCuenta().equals(castOther.getIdEstadoCuenta()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdIngresoReaseguro() == null ? 0 : this.getIdIngresoReaseguro().hashCode() );
         result = 37 * result + ( getIdEstadoCuenta() == null ? 0 : this.getIdEstadoCuenta().hashCode() );
         return result;
   }   





}