package mx.com.afirme.midas.contratos.ingreso;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDecoradoDTO;

/**
 * Remote interface for IngresoReaseguroFacade.
 * @author MyEclipse Persistence Tools
 */


public interface IngresoReaseguroFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved IngresoReaseguroDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity IngresoReaseguroDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(IngresoReaseguroDTO entity);
    /**
	 Delete a persistent IngresoReaseguroDTO entity.
	  @param entity IngresoReaseguroDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(IngresoReaseguroDTO entity);
   /**
	 Persist a previously saved IngresoReaseguroDTO entity and return it or a copy of it to the sender. 
	 A copy of the IngresoReaseguroDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity IngresoReaseguroDTO entity to update
	 @return IngresoReaseguroDTO the persisted IngresoReaseguroDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public IngresoReaseguroDTO update(IngresoReaseguroDTO entity);
	public IngresoReaseguroDTO findById( BigDecimal id);
	 /**
	 * Find all IngresoReaseguroDTO entities with a specific property value.  
	 
	  @param propertyName the name of the IngresoReaseguroDTO property to query
	  @param value the property value to match
	  	  @return List<IngresoReaseguroDTO> found by query
	 */
	public List<IngresoReaseguroDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all IngresoReaseguroDTO entities.
	  	  @return List<IngresoReaseguroDTO> all IngresoReaseguroDTO entities
	 */
	public List<IngresoReaseguroDTO> findAll();
	
	/**
	 * Find all IngresoReaseguroDTO entities that are still pending of association.
	  	  @return List<IngresoReaseguroDTO> all IngresoReaseguroDTO entities
	 */
	public List<IngresoReaseguroDTO> obtenerIngresosPendientes();
	
	/**
	 *	Returns true if all the EstadoCuentaDecoradoDTO's entities belongs to the same ReaseguradorCorredorDTO.
	 *		@return List<EstadoCuentaDecoradoDTO> all IngresoReaseguroDTO entities
	 */
	public boolean mismoReaseguradorCorredor(List<EstadoCuentaDecoradoDTO> listaEstadosCuenta);
	
	public void relacionarIngreso(IngresoReaseguroDTO ingresoReaseguro, List<EstadoCuentaDecoradoDTO> lista);
}