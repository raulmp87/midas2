package mx.com.afirme.midas.contratos.ingreso;
// default package


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



/**
 * ConceptoIngresoReaseguroDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCCONCEPTOINGRESOREASEGURO", schema = "MIDAS")
public class ConceptoIngresoReaseguroDTO implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields

	private Byte idConcepto;
	private String descripcion;

	// Constructors


	// Property accessors
	@Id
	@Column(name = "IDTCCONCEPTO", unique = true, nullable = false, precision = 2, scale = 0)
	public Byte getIdConcepto() {
		return this.idConcepto;
	}

	public void setIdConcepto(Byte idConcepto) {
		this.idConcepto = idConcepto;
	}

	@Column(name = "DESCRIPCION", nullable = false, length = 50)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


}