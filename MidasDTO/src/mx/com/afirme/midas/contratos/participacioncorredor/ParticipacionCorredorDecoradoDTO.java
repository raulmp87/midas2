package mx.com.afirme.midas.contratos.participacioncorredor;

import java.io.Serializable;
import java.math.BigDecimal;

import mx.com.afirme.midas.contratofacultativo.participacionCorredorFacultativo.ParticipacionCorredorFacultativoDTO;

public class ParticipacionCorredorDecoradoDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ParticipacionCorredorDTO participacionCorredorDTO;
	
	private ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoDTO;
	
	private String nombre;
	
	private String procedencia;
	
	private String cnfs;
	
	private BigDecimal porcentajeParticipacion;
	
	private BigDecimal montoAsignado;
	
	
	public ParticipacionCorredorDecoradoDTO(
			ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoDTO) {
		super();
		this.participacionCorredorFacultativoDTO = participacionCorredorFacultativoDTO;
	}

	public ParticipacionCorredorDecoradoDTO(
			ParticipacionCorredorDTO participacionCorredorDTO) {
		super();
		this.participacionCorredorDTO = participacionCorredorDTO;
	}

	public ParticipacionCorredorDTO getParticipacionCorredorDTO() {
		return participacionCorredorDTO;
	}

	public void setParticipacionCorredorDTO(
			ParticipacionCorredorDTO participacionCorredorDTO) {
		this.participacionCorredorDTO = participacionCorredorDTO;
	}

	public ParticipacionCorredorFacultativoDTO getParticipacionCorredorFacultativoDTO() {
		return participacionCorredorFacultativoDTO;
	}

	public void setParticipacionCorredorFacultativoDTO(
			ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoDTO) {
		this.participacionCorredorFacultativoDTO = participacionCorredorFacultativoDTO;
	}

	public String getNombre() {
		if (participacionCorredorDTO != null && participacionCorredorDTO.getReaseguradorCorredor() != null)
			nombre = participacionCorredorDTO.getReaseguradorCorredor().getNombre();
		else
			if (participacionCorredorFacultativoDTO != null && participacionCorredorFacultativoDTO.getReaseguradorCorredorDTO() != null)
				nombre = participacionCorredorFacultativoDTO.getReaseguradorCorredorDTO().getNombre();
			else
				nombre = "";
		
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getProcedencia() {
		
		if (participacionCorredorDTO != null && participacionCorredorDTO.getReaseguradorCorredor() != null){
			if (participacionCorredorDTO.getReaseguradorCorredor().getProcedencia().equals("0"))
				procedencia = "Local";
			else
				procedencia = "Extranjero";
		}else
			if (participacionCorredorFacultativoDTO != null && participacionCorredorFacultativoDTO.getReaseguradorCorredorDTO() != null){
				if (participacionCorredorFacultativoDTO.getReaseguradorCorredorDTO().getProcedencia().equals("0"))
					procedencia = "Local";
				else
					procedencia = "Extranjero";
			}else
				procedencia = "";
		
		return procedencia;
	}

	public void setProcedencia(String procedencia) {
		this.procedencia = procedencia;
	}

	public String getCnfs() {
		
		if (participacionCorredorDTO != null && participacionCorredorDTO.getReaseguradorCorredor() != null)
			cnfs = participacionCorredorDTO.getReaseguradorCorredor().getCnfs();
		else
			if (participacionCorredorFacultativoDTO != null && participacionCorredorFacultativoDTO.getReaseguradorCorredorDTO() != null)
				cnfs = participacionCorredorFacultativoDTO.getReaseguradorCorredorDTO().getCnfs();
			else
				cnfs = "";
		
		return cnfs;
	}

	public void setCnfs(String cnfs) {
		this.cnfs = cnfs;
	}

	public BigDecimal getPorcentajeParticipacion() {

		if (participacionCorredorDTO != null)
			porcentajeParticipacion = new BigDecimal(participacionCorredorDTO.getPorcentajeParticipacion());
		else
			if (participacionCorredorFacultativoDTO != null)
				porcentajeParticipacion =participacionCorredorFacultativoDTO.getPorcentajeParticipacion();
			else
				porcentajeParticipacion = null;
		
		return porcentajeParticipacion;
	}

	public void setPorcentajeParticipacion(BigDecimal porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}

	public BigDecimal getMontoAsignado() {
		return montoAsignado;
	}

	public void setMontoAsignado(BigDecimal montoAsignado) {
		this.montoAsignado = montoAsignado;
	}	

}
