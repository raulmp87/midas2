package mx.com.afirme.midas.contratos.participacioncorredor;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDTO;


/**
 * ParticipacionCorredorDTO entity. @author MyEclipse Persistence Tools
 */

@Entity
@Table(name="TDPARTICIPACIONCORREDOR", schema="MIDAS")

public class ParticipacionCorredorDTO  implements java.io.Serializable {


    // Fields

     /**
	 * 
	 */
	private static final long serialVersionUID = -1807479227938434522L;
	private BigDecimal idTdParticipacionCorredor;
     private ParticipacionDTO participacion;
     private ReaseguradorCorredorDTO reaseguradorCorredor;
     private Double porcentajeParticipacion;


    // Constructors

    /** default constructor */
    public ParticipacionCorredorDTO() {
    	 if (participacion==null)
    		 participacion = new ParticipacionDTO();
    	 
    	 if (reaseguradorCorredor==null)
    		 reaseguradorCorredor = new ReaseguradorCorredorDTO();
    }   
   
    // Property accessors
    @Id 
    @SequenceGenerator(name = "IDTDPARTICIPACIONCORREDOR_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTDPARTICIPACIONCORREDOR_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTDPARTICIPACIONCORREDOR_SEQ_GENERADOR")	 
    @Column(name="IDTDPARTICIPACIONCORREDOR", unique=true, nullable=false, precision=22, scale=0)

    public BigDecimal getIdTdParticipacionCorredor() {
        return this.idTdParticipacionCorredor;
    }
    
    public void setIdTdParticipacionCorredor(BigDecimal idTdParticipacionCorredor) {
        this.idTdParticipacionCorredor = idTdParticipacionCorredor;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTDPARTICIPACION", nullable=false)

    public ParticipacionDTO getParticipacion() {
        return this.participacion;
    }
    
    public void setParticipacion(ParticipacionDTO participacion) {
        this.participacion = participacion;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTCREASEGURADORCORREDOR", nullable=false)

    public ReaseguradorCorredorDTO getReaseguradorCorredor() {
        return this.reaseguradorCorredor;
    }
    
    public void setReaseguradorCorredor(ReaseguradorCorredorDTO reaseguradorCorredor) {
        this.reaseguradorCorredor = reaseguradorCorredor;
    }
    
    @Column(name="PORCENTAJEPARTICIPACION", nullable=false, precision=5)

    public Double getPorcentajeParticipacion() {
        return this.porcentajeParticipacion;
    }
    
    public void setPorcentajeParticipacion(Double porcentajeParticipacion) {
        this.porcentajeParticipacion = porcentajeParticipacion;
    }
   








}