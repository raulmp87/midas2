package mx.com.afirme.midas.contratos.participacioncorredor;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for ParticipacionCorredorFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ParticipacionCorredorFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ParticipacionCorredorDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ParticipacionCorredorDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ParticipacionCorredorDTO entity);
    /**
	 Delete a persistent ParticipacionCorredorDTO entity.
	  @param entity ParticipacionCorredorDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ParticipacionCorredorDTO entity);
   /**
	 Persist a previously saved ParticipacionCorredorDTO entity and return it or a copy of it to the sender. 
	 A copy of the ParticipacionCorredorDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ParticipacionCorredorDTO entity to update
	 @return ParticipacionCorredorDTO the persisted ParticipacionCorredorDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ParticipacionCorredorDTO update(ParticipacionCorredorDTO entity);
	
	/**
	 * Find  ParticipacionDTO by idParticipacionDTO property.
	 * 
	 * @return ParticipacionDTO found by id
	 */
	public ParticipacionCorredorDTO findById( BigDecimal id);
	 /**
	 * Find all ParticipacionCorredorDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ParticipacionCorredorDTO property to query
	  @param value the property value to match
	  	  @return List<ParticipacionCorredorDTO> found by query
	 */
	public List<ParticipacionCorredorDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ParticipacionCorredorDTO entities.
	  	  @return List<ParticipacionCorredorDTO> all ParticipacionCorredorDTO entities
	 */
	public List<ParticipacionCorredorDTO> findAll(
		);	
	
	/**
	 * Find filtered ParticipacionCorredorDTO entities.
	  	  @return List<ParticipacionCorredorDTO> filtered ParticipacionCorredorDTO entities
	 */
	public List<ParticipacionCorredorDTO> listarFiltrado(ParticipacionCorredorDTO participacionCorredorDTO);
}