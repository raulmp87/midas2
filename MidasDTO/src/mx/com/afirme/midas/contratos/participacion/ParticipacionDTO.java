package mx.com.afirme.midas.contratos.participacion;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.ArrayList;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.contacto.ContactoDTO;
import mx.com.afirme.midas.catalogos.cuentabanco.CuentaBancoDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDTO;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteDTO;
import mx.com.afirme.midas.contratos.lineaparticipacion.LineaParticipacionDTO;
import mx.com.afirme.midas.contratos.participacioncorredor.ParticipacionCorredorDTO;


/**
 * ParticipacionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TDPARTICIPACION", schema="MIDAS")

public class ParticipacionDTO  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = -6026999999757055516L;
	private BigDecimal idTdParticipacion;
     private CuentaBancoDTO cuentaBancoDolares;
     private ContratoCuotaParteDTO contratoCuotaParte;
     private CuentaBancoDTO cuentaBancoPesos;
     private ReaseguradorCorredorDTO reaseguradorCorredor;
     private ContratoPrimerExcedenteDTO contratoPrimerExcedente;
     private ContactoDTO contacto;
     private Double porcentajeParticipacion;     
     private BigDecimal tipo;
     private List<ParticipacionCorredorDTO> participacionCorredorDTOs;
     private List<LineaParticipacionDTO> lineaParticipaciones;


    // Constructors

    /** default constructor */
    public ParticipacionDTO() {
    	if(participacionCorredorDTOs!=null)
    		participacionCorredorDTOs = new ArrayList<ParticipacionCorredorDTO>();
    	
    	if(lineaParticipaciones==null)
    		lineaParticipaciones = new ArrayList<LineaParticipacionDTO>();
    	
    	if (contratoCuotaParte==null)
    		contratoCuotaParte = new ContratoCuotaParteDTO();
    	
    	if (cuentaBancoDolares==null)
    		cuentaBancoDolares = new CuentaBancoDTO();
    	
    	if (cuentaBancoPesos==null)
    		cuentaBancoPesos = new CuentaBancoDTO();
    	
    	if (reaseguradorCorredor==null)
    		reaseguradorCorredor = new ReaseguradorCorredorDTO();        
        
    	if (contratoPrimerExcedente==null)
    		contratoPrimerExcedente = new ContratoPrimerExcedenteDTO();
        
    	if (contacto==null)
    		contacto = new ContactoDTO();        
    }	     
   
    // Property accessors
    @Id 
    @SequenceGenerator(name = "IDTDPARTICIPACION_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTDPARTICIPACION_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTDPARTICIPACION_SEQ_GENERADOR")	 
    @Column(name="IDTDPARTICIPACION", unique=true, nullable=false, precision=22, scale=0)

    public BigDecimal getIdTdParticipacion() {
        return this.idTdParticipacion;
    }
    
    public void setIdTdParticipacion(BigDecimal idTdParticipacion) {
        this.idTdParticipacion = idTdParticipacion;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTCCUENTABANCODOLARES", nullable=true)

    public CuentaBancoDTO getCuentaBancoDolares() {
        return this.cuentaBancoDolares;
    }
    
    public void setCuentaBancoDolares(CuentaBancoDTO cuentaBancoDolares) {
        this.cuentaBancoDolares = cuentaBancoDolares;
    }
	@ManyToOne(cascade=CascadeType.REFRESH,fetch=FetchType.EAGER)
        @JoinColumn(name="IDTMCONTRATOCUOTAPARTE")

    public ContratoCuotaParteDTO getContratoCuotaParte() {
        return this.contratoCuotaParte;
    }
    
    public void setContratoCuotaParte(ContratoCuotaParteDTO contratoCuotaParte) {
        this.contratoCuotaParte = contratoCuotaParte;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTCCUENTABANCOPESOS", nullable=true)

    public CuentaBancoDTO getCuentaBancoPesos() {
        return this.cuentaBancoPesos;
    }
    
    public void setCuentaBancoPesos(CuentaBancoDTO cuentaBancoPesos) {
        this.cuentaBancoPesos = cuentaBancoPesos;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTCREASEGURADORCORREDOR", nullable=false)

    public ReaseguradorCorredorDTO getReaseguradorCorredor() {
        return this.reaseguradorCorredor;
    }
    
    public void setReaseguradorCorredor(ReaseguradorCorredorDTO reaseguradorCorredor) {
        this.reaseguradorCorredor = reaseguradorCorredor;
    }
	@ManyToOne(cascade=CascadeType.REFRESH,fetch=FetchType.EAGER)
        @JoinColumn(name="IDTMCONTRATOPRIMEREXCEDENTE")

    public ContratoPrimerExcedenteDTO getContratoPrimerExcedente() {
        return this.contratoPrimerExcedente;
    }
    
    public void setContratoPrimerExcedente(ContratoPrimerExcedenteDTO contratoPrimerExcedente) {
        this.contratoPrimerExcedente = contratoPrimerExcedente;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTCCONTACTO")

    public ContactoDTO getContacto() {
        return this.contacto;
    }
    
    public void setContacto(ContactoDTO contacto) {
        this.contacto = contacto;
    }
    
    @Column(name="PORCENTAJEPARTICIPACION", nullable=false, precision=5)

    public Double getPorcentajeParticipacion() {
        return this.porcentajeParticipacion;
    }
    
    public void setPorcentajeParticipacion(Double porcentajeParticipacion) {
        this.porcentajeParticipacion = porcentajeParticipacion;
    }                
    
    @Column(name="TIPO", nullable=false, precision=22, scale=0)

    public BigDecimal getTipo() {
        return this.tipo;
    }
    
    public void setTipo(BigDecimal tipo) {
        this.tipo = tipo;
    }
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="participacion")
    public List<ParticipacionCorredorDTO> getParticipacionCorredores() {
        return this.participacionCorredorDTOs;
    }
    
    public void setParticipacionCorredores(List<ParticipacionCorredorDTO> participacionCorredorDTOs) {
        this.participacionCorredorDTOs = participacionCorredorDTOs;
    }
   
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="participacion")
    public List<LineaParticipacionDTO> getLineaParticipaciones() {
        return this.lineaParticipaciones;
    }
    
    public void setLineaParticipaciones(List<LineaParticipacionDTO> lineaParticipaciones) {
        this.lineaParticipaciones = lineaParticipaciones;
    }
}