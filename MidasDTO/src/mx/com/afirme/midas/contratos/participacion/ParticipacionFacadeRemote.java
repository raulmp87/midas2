package mx.com.afirme.midas.contratos.participacion;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDTO;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteDTO;
import mx.com.afirme.midas.contratos.linea.LineaDTO;

/**
 * Remote interface for ParticipacionFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ParticipacionFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ParticipacionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ParticipacionDTO entity to persist
	  @throws RuntimeException when the operation fails
	  @return the new participation entity
	 */
    public ParticipacionDTO save(ParticipacionDTO entity);
    /**
	 Delete a persistent ParticipacionDTO entity.
	  @param entity ParticipacionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ParticipacionDTO entity);
   /**
	 Persist a previously saved ParticipacionDTO entity and return it or a copy of it to the sender. 
	 A copy of the ParticipacionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ParticipacionDTO entity to update
	 @return ParticipacionDTO the persisted ParticipacionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ParticipacionDTO update(ParticipacionDTO entity);
	
	/**
	 * Find  ParticipacionDTO by id property.
	 * 
	 * @return ParticipacionDTO found by id
	 */
	public ParticipacionDTO findById( BigDecimal id);
	 /**
	 * Find all ParticipacionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ParticipacionDTO property to query
	  @param value the property value to match
	  	  @return List<ParticipacionDTO> found by query
	 */
	public List<ParticipacionDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ParticipacionDTO entities.
	  	  @return List<ParticipacionDTO> all ParticipacionDTO entities
	 */
	public List<ParticipacionDTO> findAll(
		);
	
	/**
	 * Find filtered ParticipacionDTO entities.
	  	  @return List<ParticipacionDTO> filtered ParticipacionDTO entities
	 */
	public List<ParticipacionDTO> listarFiltrado(ParticipacionDTO participacionDTO);
	
	/**
	 * Find filtered ParticipacionDTO entities.
	  	  @return List<ParticipacionDTO> filtered ParticipacionDTO entities
	 */
	public List<ParticipacionDTO> buscarPorLineaCCP(ContratoCuotaParteDTO contratoCuotaParteDTO,LineaDTO lineaDTO);
	
	/**
	 * Find filtered ParticipacionDTO entities.
	  	  @return List<ParticipacionDTO> filtered ParticipacionDTO entities
	 */
	public List<ParticipacionDTO> buscarPorLineaCPE(ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO,LineaDTO lineaDTO);
}