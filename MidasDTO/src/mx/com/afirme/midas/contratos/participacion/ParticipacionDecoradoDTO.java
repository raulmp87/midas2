package mx.com.afirme.midas.contratos.participacion;

import java.io.Serializable;
import java.math.BigDecimal;

import mx.com.afirme.midas.contratofacultativo.participacionFacultativa.ParticipacionFacultativoDTO;
import mx.com.afirme.midas.contratos.participacioncorredor.ParticipacionCorredorDTO;

public class ParticipacionDecoradoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ParticipacionDTO participacionDTO;
	private ParticipacionCorredorDTO participacionCorredorDTO;

	private ParticipacionFacultativoDTO participacionFacultativoDTO;
	
	private BigDecimal primaNeta;
	
	private BigDecimal primaReaseguro;
	
	private BigDecimal porcentajeParticipacion;
	
	private BigDecimal participacion;
	
	private BigDecimal porcentajeComision;
	
	private BigDecimal comision;
	
	private BigDecimal primaNetaReaseguro;
	
	private BigDecimal sumaAsegurada;
	
	private int tipoReaseguro;
	
	
	public ParticipacionDecoradoDTO(ParticipacionFacultativoDTO participacionFacultativoDTO) {
		super();
		this.participacionFacultativoDTO = participacionFacultativoDTO;
	}

	public ParticipacionDecoradoDTO(ParticipacionDTO participacionDTO) {
		super();
		this.participacionDTO = participacionDTO;
	}

	public ParticipacionDecoradoDTO(ParticipacionCorredorDTO participacionCorredorDTO){
		this.participacionCorredorDTO = participacionCorredorDTO;
		this.participacionDTO = participacionCorredorDTO.getParticipacion();
	}


	public ParticipacionDTO getParticipacionDTO() {
		return participacionDTO;
	}

	public void setParticipacionDTO(ParticipacionDTO participacionDTO) {
		this.participacionDTO = participacionDTO;
	}

	public ParticipacionFacultativoDTO getParticipacionFacultativoDTO() {
		return participacionFacultativoDTO;
	}

	public void setParticipacionFacultativoDTO(ParticipacionFacultativoDTO participacionFacultativoDTO) {
		this.participacionFacultativoDTO = participacionFacultativoDTO;
	}

	public String getNombreCorredor() {
		String nombre = "NA";
		if (participacionCorredorDTO != null){
			nombre = participacionCorredorDTO.getParticipacion().getReaseguradorCorredor().getNombre();
		}
		return nombre;
	}
	public String getNombreReasegurador() {
		String nombre = "";
		if (participacionCorredorDTO != null){
			nombre = participacionCorredorDTO.getReaseguradorCorredor().getNombre();
		}
		else if (participacionDTO != null){
			nombre = participacionDTO.getReaseguradorCorredor().getNombre();
		}
		else if (participacionFacultativoDTO != null){
			nombre = participacionFacultativoDTO.getReaseguradorCorredorDTO().getNombre();
		}
		
		return nombre;
	}

	public String getProcedencia() {
		String procedencia = "";
		if (participacionCorredorDTO != null && participacionCorredorDTO.getReaseguradorCorredor() != null){
			if (participacionCorredorDTO.getReaseguradorCorredor().getProcedencia().equals("0"))
				procedencia = "Local";
			else
				procedencia = "Extranjero";
		}
		else if (participacionDTO != null && participacionDTO.getReaseguradorCorredor() != null){
			if (participacionDTO.getReaseguradorCorredor().getProcedencia().equals("0"))
				procedencia = "Local";
			else
				procedencia = "Extranjero";
		}else 
			if (participacionFacultativoDTO != null  && participacionFacultativoDTO.getReaseguradorCorredorDTO() != null){
				if (participacionFacultativoDTO.getReaseguradorCorredorDTO().getProcedencia()!= null){
					if (participacionFacultativoDTO.getReaseguradorCorredorDTO().getProcedencia().equals("0"))
						procedencia = "Local";
					else
						procedencia = "Extranjero";
				}else{
					procedencia = "Local";
				}	
			}
		
		return procedencia;
	}

	public String getCnfs() {
		String cnfs = "";
		if(participacionCorredorDTO != null){
			cnfs = participacionCorredorDTO.getReaseguradorCorredor().getCnfs();
		}
		else if (participacionDTO != null)
			cnfs = participacionDTO.getReaseguradorCorredor().getCnfs();
		else if (participacionFacultativoDTO != null)
				cnfs = participacionFacultativoDTO.getReaseguradorCorredorDTO().getCnfs();
		
		return cnfs;
	}

	public BigDecimal getPorcentajeParticipacion() {
		BigDecimal porcentajeParticipacion = null;
		if(participacionCorredorDTO != null){
			if(participacionCorredorDTO.getPorcentajeParticipacion() !=null){
				porcentajeParticipacion = new BigDecimal(participacionCorredorDTO.getPorcentajeParticipacion());
			}else{
				porcentajeParticipacion = new BigDecimal("0.0");
			}
		}
		else if (participacionDTO != null){
			if(participacionDTO.getPorcentajeParticipacion()!=null){
				porcentajeParticipacion = new BigDecimal(participacionDTO.getPorcentajeParticipacion().doubleValue());
			}else{
				porcentajeParticipacion = new BigDecimal("0.0");
			}			
		}else{ 
			if (participacionFacultativoDTO != null ){
				if (participacionFacultativoDTO.getPorcentajeParticipacion()!=null) {
					porcentajeParticipacion =new BigDecimal(participacionFacultativoDTO.getPorcentajeParticipacion().doubleValue());
				}
				else{
					porcentajeParticipacion = new BigDecimal("0.0");
				}
						
			}
		}
		return porcentajeParticipacion;
	}

	public BigDecimal getPrimaNeta() {
		return primaNeta;
	}

	public void setPrimaNeta(BigDecimal primaNeta) {
		this.primaNeta = primaNeta;
	}

	public BigDecimal getPrimaReaseguro() {
		return primaReaseguro;
	}

	public void setPrimaReaseguro(BigDecimal primaReaseguro) {
		this.primaReaseguro = primaReaseguro;
	}
	
	public BigDecimal getPorcentajePaticipacion() {
		return porcentajeParticipacion;
	}

	public void setPorcentajeParticipacion(BigDecimal porcentajeParticipacion) { 
		this.porcentajeParticipacion = porcentajeParticipacion;
	}

	public BigDecimal getParticipacion() {
		return participacion;
	}

	public void setParticipacion(BigDecimal participacion) {
		this.participacion = participacion;
	}

	public BigDecimal getPorcentajeComision() {
		return porcentajeComision;
	}

	public void setPorcentajeComision(BigDecimal porcentajeComision) {
		this.porcentajeComision = porcentajeComision;
	}

	public BigDecimal getComision() {
		return comision;
	}

	public void setComision(BigDecimal comision) {
		this.comision = comision;
	}

	public BigDecimal getPrimaNetaReaseguro() {
		return primaNetaReaseguro;
	}

	public void setPrimaNetaReaseguro(BigDecimal primaNetaReaseguro) {
		this.primaNetaReaseguro = primaNetaReaseguro;
	}

	public BigDecimal getSumaAsegurada() {
		return sumaAsegurada;
	}

	public void setSumaAsegurada(BigDecimal sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}

	public int getTipoReaseguro() {
		return tipoReaseguro;
	}

	public void setTipoReaseguro(int tipoReaseguro) {
		this.tipoReaseguro = tipoReaseguro;
	}

	public BigDecimal getTipo() {
		BigDecimal tipo = null;
		if (this.getParticipacionDTO() != null)
			tipo = this.getParticipacionDTO().getTipo();
		else
			if (this.getParticipacionFacultativoDTO() != null)
				tipo = new BigDecimal(this.getParticipacionFacultativoDTO().getTipo());
			else  if(this.participacionCorredorDTO != null && participacionCorredorDTO.getParticipacion() != null){
				tipo = participacionCorredorDTO.getParticipacion().getTipo();
			}
			else
			  	throw new RuntimeException("Debe instanciarse el objeto ParticipacionDecoradoDTO con alguno de los dos objetos siguientes: ParticipacionDTO y ParticipacionFacultativoDTO");
		
		return tipo;
	}	

}
