package mx.com.afirme.midas.direccion;
// default package

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

/**
 * Remote interface for DireccionDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface DireccionFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved DireccionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DireccionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public DireccionDTO save(DireccionDTO entity);
    /**
	 Delete a persistent DireccionDTO entity.
	  @param entity DireccionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DireccionDTO entity);
   /**
	 Persist a previously saved DireccionDTO entity and return it or a copy of it to the sender. 
	 A copy of the DireccionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DireccionDTO entity to update
	 @return DireccionDTO the persisted DireccionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public DireccionDTO update(DireccionDTO entity);
	public DireccionDTO findById( BigDecimal id);
	 /**
	 * Find all DireccionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DireccionDTO property to query
	  @param value the property value to match
	  	  @return List<DireccionDTO> found by query
	 */
	public List<DireccionDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all DireccionDTO entities.
	  	  @return List<DireccionDTO> all DireccionDTO entities
	 */
	public List<DireccionDTO> findAll(
		);
	
	/**
	 * Find a DireccionDTO entity related with a specific CotizacionDTO.
	 * CotizacionDTO contains three direccionDTO records. The found record is indicated by
	 * the param int direccionOrden in the next order:
	 * 1 - Direccion contratante.
	 * 2 - Direccion cobro.
	 * 3 - Direccion asegurado.    
	 * @param int direccionOrden
	 * @param BigDecimal idToCotizacion
	  	  @return List<DireccionDTO> found by query
	 */
	public DireccionDTO findByIdCotizacion(BigDecimal idToCotizacion,int direccionOrden);
	
	/**
	 * Find a DireccionDTO entity related with a specific ProveedorInspeccionDTO.
	 * @param BigDecimal idToProveedorInspeccion
	  	  @return DireccionDTO found by query
	 */
	public DireccionDTO findByIdProveedorInspeccion(BigDecimal idToProveedorInspeccion);
	
	public List<BigDecimal> listarIdToDirecciones(DireccionDTO direccionDTO);
	
	/**
	 * Consulta los nombres de estado, ciudad y colonia y los almacena en los atributos estado, ciudad y colonia respectivamente.
	 * @param direccionDTO con los id's de estado, ciudad y colonia.
	 * @return direccionDTO con los nombres de estado, ciudad y colonia.
	 */
	public DireccionDTO poblarDatosDireccion(DireccionDTO direccionDTO);
}