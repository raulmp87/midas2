package mx.com.afirme.midas.direccion;

import java.math.BigDecimal;

public class SeccionSubIncisoSoporteDanosSiniestrosDTO {
    
    private BigDecimal idToSeccion;
    private String nombreComercialSeccion;
    private BigDecimal numeroSubInciso;
    private String descripcionSubInciso;
    
    
    
   
    public void setIdToSeccion(BigDecimal idToSeccion) {
	this.idToSeccion = idToSeccion;
    }
    public BigDecimal getIdToSeccion() {
	return idToSeccion;
    }
    public void setNumeroSubInciso(BigDecimal numeroSubInciso) {
	this.numeroSubInciso = numeroSubInciso;
    }
    public BigDecimal getNumeroSubInciso() {
	return numeroSubInciso;
    }
    public void setDescripcionSubInciso(String descripcionSubInciso) {
	this.descripcionSubInciso = descripcionSubInciso;
    }
    public String getDescripcionSubInciso() {
	return descripcionSubInciso;
    }
    public void setNombreComercialSeccion(String nombreComercialSeccion) {
	this.nombreComercialSeccion = nombreComercialSeccion;
    }
    public String getNombreComercialSeccion() {
	return nombreComercialSeccion;
    }
    
    

}
