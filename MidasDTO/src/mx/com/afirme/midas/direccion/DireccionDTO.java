package mx.com.afirme.midas.direccion;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.base.StringUtil;

/**
 * DireccionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TODIRECCION", schema = "MIDAS")
public class DireccionDTO implements java.io.Serializable {
	private static final long serialVersionUID = -2203293929348324069L;
	private BigDecimal idToDireccion;
	private String nombreCalle;
	private String numeroExterior;
	private String numeroInterior;
	private String entreCalles;
	private String nombreColonia;
	private String nombreDelegacion;
	private BigDecimal idMunicipio;
	private BigDecimal idEstado;
	private BigDecimal codigoPostal;
	private String sumasAseguradas;
	private String mensajeError;

	private String municipio;
    private String estado;
    private String colonia;
	// Constructors

	/** default constructor */
	public DireccionDTO() {
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTODIRECCION_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTODIRECCION_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTODIRECCION_SEQ_GENERADOR")
	@Column(name = "IDTODIRECCION")
	public BigDecimal getIdToDireccion() {
		return this.idToDireccion;
	}

	public void setIdToDireccion(BigDecimal idToDireccion) {
		this.idToDireccion = idToDireccion;
	}
	
//	public void setIdToDireccion(String idToDireccion) {
//		if (idToDireccion != null && !idToDireccion.equals("")) {
//			this.idToDireccion = new BigDecimal(idToDireccion);
//		}
//	}
	

	@Column(name = "NOMBRECALLE", nullable = false, length = 100)
	public String getNombreCalle() {
		return this.nombreCalle;
	}

	public void setNombreCalle(String nombreCalle) {
		this.nombreCalle = nombreCalle;
	}

	@Column(name = "NUMEROEXTERIOR", nullable = false)
	public String getNumeroExterior() {
		return this.numeroExterior;
	}

	public void setNumeroExterior(String numeroExterior) {
		this.numeroExterior = numeroExterior;
	}

	@Column(name = "NUMEROINTERIOR", length = 10)
	public String getNumeroInterior() {
		return this.numeroInterior;
	}

	public void setNumeroInterior(String numeroInterior) {
		this.numeroInterior = numeroInterior;
	}

	@Column(name = "ENTRECALLES", length = 100)
	public String getEntreCalles() {
		return this.entreCalles;
	}

	public void setEntreCalles(String entreCalles) {
		this.entreCalles = entreCalles;
	}

	@Column(name = "NOMBRECOLONIA", length = 100)
	public String getNombreColonia() {
		return this.nombreColonia;
	}

	public void setNombreColonia(String nombreColonia) {
		this.nombreColonia = nombreColonia;
	}

	@Column(name = "NOMBREDELEGACION", length = 100)
	public String getNombreDelegacion() {
		return this.nombreDelegacion;
	}

	public void setNombreDelegacion(String nombreDelegacion) {
		this.nombreDelegacion = nombreDelegacion;
	}

	@Column(name = "IDMUNICIPIO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdMunicipio() {
		return this.idMunicipio;
	}

	public void setIdMunicipio(BigDecimal idMunicipio) {
		this.idMunicipio = idMunicipio;
	}

	@Column(name = "IDESTADO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdEstado() {
		return this.idEstado;
	}

	public void setIdEstado(BigDecimal idEstado) {
		this.idEstado = idEstado;
	}

	@Column(name = "CODIGOPOSTAL", nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoPostal() {
		return this.codigoPostal;
	}

	public void setCodigoPostal(BigDecimal codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	@Transient
	public String getSumasAseguradas() {
		return sumasAseguradas;
	}

	public void setSumasAseguradas(String sumasAseguradas) {
		this.sumasAseguradas = sumasAseguradas;
	}

	@Transient
	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	
	@Transient
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Transient
	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	@Transient
	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	
	public String toString() {
		String direccion = "";
		direccion += StringUtil.isEmpty(nombreCalle) ? "" : nombreCalle + ", ";
		direccion += StringUtil.isEmpty(numeroExterior) ? "" : "No. "+ numeroExterior + ", ";
		direccion += StringUtil.isEmpty(this.numeroInterior) ? "" : "int. " + numeroInterior + ", ";
		direccion += StringUtil.isEmpty(colonia) ? "" : "Col. " + colonia + ", ";
		direccion += StringUtil.isEmpty(municipio) ? "" : municipio + ", ";
		direccion += StringUtil.isEmpty(estado) ? "" : estado + ", ";
		direccion += (codigoPostal == null) ? "" : "C.P. " + this.codigoPostal;
		return direccion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((codigoPostal == null) ? 0 : codigoPostal.hashCode());
		result = prime * result
				+ ((entreCalles == null) ? 0 : entreCalles.hashCode());
		result = prime * result
				+ ((idEstado == null) ? 0 : idEstado.hashCode());
		result = prime * result
				+ ((idMunicipio == null) ? 0 : idMunicipio.hashCode());
		result = prime * result
				+ ((nombreCalle == null) ? 0 : nombreCalle.hashCode());
		result = prime * result
				+ ((nombreColonia == null) ? 0 : nombreColonia.hashCode());
		result = prime
				* result
				+ ((nombreDelegacion == null) ? 0 : nombreDelegacion.hashCode());
		result = prime * result
				+ ((numeroExterior == null) ? 0 : numeroExterior.hashCode());
		result = prime * result
				+ ((numeroInterior == null) ? 0 : numeroInterior.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof DireccionDTO))
			return false;
		DireccionDTO other = (DireccionDTO) obj;
		if (codigoPostal == null) {
			if (other.codigoPostal != null)
				return false;
		} else if (!codigoPostal.equals(other.codigoPostal))
			return false;
		if (entreCalles == null) {
			if (other.entreCalles != null)
				return false;
		} else if (!entreCalles.equals(other.entreCalles))
			return false;
		if (idEstado == null) {
			if (other.idEstado != null)
				return false;
		} else if (!idEstado.equals(other.idEstado))
			return false;
		if (idMunicipio == null) {
			if (other.idMunicipio != null)
				return false;
		} else if (!idMunicipio.equals(other.idMunicipio))
			return false;
		if (nombreCalle == null) {
			if (other.nombreCalle != null)
				return false;
		} else if (!nombreCalle.equals(other.nombreCalle))
			return false;
		if (nombreColonia == null) {
			if (other.nombreColonia != null)
				return false;
		} else if (!nombreColonia.equals(other.nombreColonia))
			return false;
		if (nombreDelegacion == null) {
			if (other.nombreDelegacion != null)
				return false;
		} else if (!nombreDelegacion.equals(other.nombreDelegacion))
			return false;
		if (numeroExterior == null) {
			if (other.numeroExterior != null)
				return false;
		} else if (!numeroExterior.equals(other.numeroExterior))
			return false;
		if (numeroInterior == null) {
			if (other.numeroInterior != null)
				return false;
		} else if (!numeroInterior.equals(other.numeroInterior))
			return false;
		return true;
	}
	
	
}