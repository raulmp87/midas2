/**
 * 
 */
package mx.com.afirme.midas.sistema.excepcion;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author andres.avalos
 *
 */
public class ExcepcionDeLogicaNegocio extends Exception {

	private static final long serialVersionUID = 1L;

	private String claseOrigen;
	private String descripcion;
	private Throwable ex;	
	
	public ExcepcionDeLogicaNegocio(String claseOrigen, String descripcion) {
		this.claseOrigen = claseOrigen;
		this.descripcion = descripcion;
		
		Throwable ex = null;
		Logger.getLogger("MidasPU").log(Level.SEVERE, "Excepcion de lógica de negocio en la capa de persistencia en " + this.claseOrigen.trim() + " : " + 
				this.descripcion , ex);
	}
	
	public ExcepcionDeLogicaNegocio(String claseOrigen, Throwable ex) {
		this.claseOrigen = claseOrigen;
		this.ex = ex;
		Logger.getLogger("MidasPU").log(Level.SEVERE, "Excepcion de lógica de negocio en la capa de persistencia en " + this.claseOrigen.trim() + " de tipo " + 
				this.ex.getClass().getCanonicalName(), this.ex);
		this.ex.printStackTrace();
	}
	
	public ExcepcionDeLogicaNegocio(String claseOrigen, Throwable ex, String descripcion) {
		this.claseOrigen = claseOrigen;
		this.ex = ex;
		this.descripcion = descripcion;
		Logger.getLogger("MidasPU").log(Level.SEVERE, "Excepcion de lógica de negocio en la capa de persistencia en " + this.claseOrigen.trim() + " de tipo " + 
				this.ex.getClass().getCanonicalName(), this.ex);
		this.ex.printStackTrace();
	}

	/**
	 * @return the serialVersionUID
	 */
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	/**
	 * @return the claseOrigen
	 */
	public String getClaseOrigen() {
		return claseOrigen;
	}

	/**
	 * @return the ex
	 */
	public Throwable getEx() {
		return ex;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
}
