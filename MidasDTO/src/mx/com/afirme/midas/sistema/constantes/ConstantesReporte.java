package mx.com.afirme.midas.sistema.constantes;

public class ConstantesReporte {
	
	//Constantes para tipos de documentos
	public static final String TIPO_PDF = "application/pdf";
	public static final String TIPO_XLS = "application/vnd.ms-excel";
	public static final String TIPO_CSV = "text/csv";
	public static final String TIPO_TXT = "text/plain";
	public static final String TIPO_TXT_UTF8 = "text/plain; charset=UTF-8";
	public static final String TIPO_DOC = "application/vnd.ms-word";
	public static final String TIPO_XLSX ="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	public static final String TIPO_TXT_MOVIL = "textMovil/plain";
	
	public static final String LOGOTIPO_SEGUROS_AFIRME = "/mx/com/afirme/midas/danios/reportes/comun/imagenes/logo_AS.jpg";
	public static final String FIRMA_FUNCIONARIO1 = "/mx/com/afirme/midas/danios/reportes/comun/imagenes/sign1.jpg";
	public static final String FIRMA_FUNCIONARIO2 = "/mx/com/afirme/midas/danios/reportes/comun/imagenes/sign2.jpg";
	public static final String FONDO_PLANTILLA_COTIZACION = "/mx/com/afirme/midas/danios/reportes/comun/imagenes/cotizacion1.gif";
	
	//Constantes para tipo de reporte Poliza
	public static final int REPORTE_SEGURO_EMPRESARIAL = 1;
	public static final int REPORTE_TRANSPORTES = 2;
	public static final int REPORTE_GENERICA = 3;
	public static final int REPORTE_ENDOSO = 4;
	public static final int REPORTE_SEGURO_FAMILIAR = 5;
	public static final int REPORTE_ENDOSO_CANCELACION = 6;
	public static final int REPORTE_ENDOSO_REHABILITACION=7;
	public static final int REPORTE_RC_FUNCIONARIOS=8;
	public static final int REPORTE_RC_MEDICOS=9;
	public static final int REPORTE_RC_PROFESIONAL_MEDICOS=10;

	public static final String SUFFIX_PDF = "PDF";
}
