package mx.com.afirme.midas.sistema.constantes;

public class ConstantesCotizacion {

	public static final String CLAVE_SUMA_ASEGURADA_BASICA="1";
	public static final String CLAVE_SUMA_ASEGURADA_AMPARADA="2";
	public static final String CLAVE_SUMA_ASEGURADA_SUBLIMITE="3";
	
	// Constantes de endosos para tipo de solicitud;
	public static final int SOLICITUD_ENDOSO_DE_CANCELACION = 1;
	public static final int SOLICITUD_ENDOSO_DE_REHABILITACION = 2;
	public static final int SOLICITUD_ENDOSO_DE_MODIFICACION = 3;
	public static final int SOLICITUD_ENDOSO_DE_DECLARACION = 4;
	public static final int SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO = 5;
	
	// Constantes de tipo de endoso;
	public static final short TIPO_ENDOSO_CANCELACION = 4;
	public static final short TIPO_ENDOSO_REHABILITACION = 5;
	public static final short TIPO_ENDOSO_AUMENTO = 2;
	public static final short TIPO_ENDOSO_DISMINUCION = 3;
	public static final short TIPO_ENDOSO_CAMBIO_DATOS = 1;
	public static final short TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO = 6;
	public static final short TIPO_ENDOSO_CE = 7;
	public static final short TIPO_ENDOSO_RE = 8;
	
	//Constantes para el tipo de Calculo
	public static final String TIPO_CALCULO_SEYCOS = "S";
	public static final String TIPO_CALCULO_MIDAS = "M";
	
	//Constantes para tipos de agrupoacion
	public static final short TIPO_PRIMER_RIESGO = 1;
	public static final short TIPO_LUC = 2;
	
	//Constantes para modificación de derechos y recargo por pago fraccionado
	public static final Short CLAVE_DERECHOS_SISTEMA = 0;
	public static final  Short CLAVE_DERECHOS_USUARIO = 1;
	public static final  Short CLAVE_RPF_SISTEMA = 0;
	public static final  Short CLAVE_RPF_USUARIO_MONTO = 1;
	public static final  Short CLAVE_RPF_USUARIO_PORCENTAJE = 2;
	
	//Constantes para Estatus
	public static final short AUTORIZACION_NO_REQUERIDA = 0;
	public static final short AUTORIZACION_REQUERIDA = 1;
	public static final String CLAVES_AUTORIZADA_RECHAZADA = "78";
	public static final short AUTORIZADA = 7;
	public static final short RECHAZADA = 8;
	public static final short ESTATUS_ODT_ASIGNADA = 1;
	public static final short ESTATUS_ODT_ENPROCESO = 0;
	public static final short ESTATUS_ODT_TERMINADA = 2;
	public static final short ESTATUS_ODT_RECHAZADA = 8;
	public static final short ESTATUS_ODT_CANCELADA = 9;
	public static final short ESTATUS_COT_ENPROCESO = 10;	
	public static final short ESTATUS_COT_ASIGNADA = 11;
	public static final short ESTATUS_COT_LIBERADA = 13;
	public static final short ESTATUS_COT_LISTA_EMITIR = 14;
	public static final short ESTATUS_COT_ASIGNADA_EMISION = 15;
	public static final short ESTATUS_COT_EMITIDA = 16;
	public static final short ESTATUS_COT_PENDIENTE_AUTORIZACION = 17;
	public static final short ESTATUS_SOL_TERMINADA = 2;
	public static final short ESTATUS_SOL_ASIGNADA = 1;
	public static final Short ESTATUS_SOL_EN_PROCESO = (short)0;
	
	//Constantes para Tipo de ARD
	public static final short TIPO_COMERCIAL = 1;
	public static final short TIPO_TECNICO = 2;
	
	//Constantes para Tipo vALOR de ARD
	public static final short TIPO_PORCENTAJE = 1;
	public static final short TIPO_VALOR = 2;		
	
	//Constantes para NIVEL de ARD
	public static final short NIVEL_PRODUCTO = 1;
	public static final short NIVEL_TIPO_POLIZA = 2;		
	public static final short NIVEL_COBERTURA = 3;

	//Constantes para Obligatoriedad
	public static final short DEFAULT = 0;
	public static final short OPCIONAL = 1;		
	public static final short OBLIGATORIO_PARCIAL = 2;	
	public static final short OBLIGATORIO = 3;
	
	//status de recibos 
	public static final String RECIBO_EMITIDO = "EMITIDO";
	public static final String RECIBO_PAGADO = "PAGADO";
	public static final String RECIBO_CANCELADO = "CANCELADO";
	
	//Constantes para tipos de comision
	public static final short TIPO_RO = 1;
	public static final short TIPO_RCI = 2;
	public static final short TIPO_PRR = 3;
	
	//Constantes para contratado y no contratado
	public static final short CONTRATADO = 1;
	public static final short NO_CONTRATADO = 0;
	
	//Contantes para tipos de movimientos de endoso
	public static final short TIPO_MOV_ALTA_COBERTURA = 1;
	public static final short TIPO_MOV_BAJA_COBERTURA = 2;
	public static final short TIPO_MOV_MODIFICACION_COBERTURA = 3;
	public static final short TIPO_MOV_ALTA_SECCION = 4;
	public static final short TIPO_MOV_BAJA_SECCION = 5;	
	public static final short TIPO_MOV_MODIFICACION_SECCION = 6;
	public static final short TIPO_MOV_ALTA_INCISO = 7;
	public static final short TIPO_MOV_BAJA_INCISO = 8;
	public static final short TIPO_MOV_MODIFICACION_INCISO = 9;
	public static final short TIPO_MOV_MODIFICACION_APP_GRAL = 10;
	public static final short TIPO_MOV_ALTA_SUBINCISO = 11;
	public static final short TIPO_MOV_BAJA_SUBINCISO = 12;
	public static final short TIPO_MOV_MODIFICACION_SUBINCISO = 13;
	public static final short TIPO_MOV_ALTA_1ER_RIESGO_LUC = 14;
	public static final short TIPO_MOV_BAJA_1ER_RIESGO_LUC = 15;
	public static final short TIPO_MOV_MODIFICACION_1ER_RIESGO_LUC = 16;
	public static final short TIPO_MOV_MODIFICACION_PRIMA_NETA = 17;
	public static final short TIPO_MOV_MODIFICACION_COASEGURO = 18;
	public static final short TIPO_MOV_MODIFICACION_DEDUCIBLE = 19;
	
	public static final String CLAVE_TIPO_PERSONA_FISICA="1";
	public static final String CLAVE_TIPO_PERSONA_MORAL="2";
	
	//Constantes para CASA
	public static final String CODIGO_PRODUCTO_CASA = "06"; 
	public static final String CODIGO_TIPO_POLIZA_CASA = "03";
	
}
