package mx.com.afirme.midas.sistema;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
/**
 * Excepcion para el catalogo de clientes, requiere guardar la lista de errores en un mapa
 * @author vmhersil
 *
 */
public class ClientSystemException extends Exception{
	private static final long serialVersionUID = -8683201524609678111L;
	private List<Map<String,String>> errorList;//field, and error message
	private Integer idCliente;
	public ClientSystemException(){
		super();
	}
	
	public ClientSystemException(String msg){
		super(msg);
	}
	
	public ClientSystemException(Throwable e){
		super(e);
	}
	
	public ClientSystemException(String msg,Integer idCliente){
		super(msg);
		this.idCliente=idCliente;
	}
	
	public ClientSystemException(Map<String,String> errorMap){
		errorList=new ArrayList<Map<String,String>>();
		errorList.add(errorMap);
	}
	
	public ClientSystemException(List<Map<String,String>> errorList){
		this.errorList=errorList;
	}
	
	public ClientSystemException(List<Map<String,String>> errorList,Integer idCliente){
		this.errorList=errorList;
		this.idCliente=idCliente;
	}

	public List<Map<String, String>> getErrorList() {
		return errorList;
	}

	public void setErrorList(List<Map<String, String>> errorList) {
		this.errorList = errorList;
	}

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}
}
