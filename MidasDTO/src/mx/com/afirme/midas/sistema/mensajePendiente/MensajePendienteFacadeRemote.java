package mx.com.afirme.midas.sistema.mensajePendiente;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for MensajePendienteDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface MensajePendienteFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved MensajePendienteDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            MensajePendienteDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(MensajePendienteDTO entity);

	/**
	 * Delete a persistent MensajePendienteDTO entity.
	 * 
	 * @param entity
	 *            MensajePendienteDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(MensajePendienteDTO entity);

	/**
	 * Persist a previously saved MensajePendienteDTO entity and return it or a
	 * copy of it to the sender. A copy of the MensajePendienteDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            MensajePendienteDTO entity to update
	 * @return MensajePendienteDTO the persisted MensajePendienteDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public MensajePendienteDTO update(MensajePendienteDTO entity);

	public MensajePendienteDTO findById(BigDecimal id);

	/**
	 * Find all MensajePendienteDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the MensajePendienteDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<MensajePendienteDTO> found by query
	 */
	public List<MensajePendienteDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all MensajePendienteDTO entities.
	 * 
	 * @return List<MensajePendienteDTO> all MensajePendienteDTO entities
	 */
	public List<MensajePendienteDTO> findAll();

	public List<MensajePendienteDTO> listarPendientesDanios(String idsRoles);

	public List<MensajePendienteDTO> listarPendientesReaseguro(String idsRoles);

	public List<MensajePendienteDTO> listarPendientesSiniestros(String idsRoles);
}