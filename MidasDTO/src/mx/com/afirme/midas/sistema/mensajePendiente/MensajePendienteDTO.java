package mx.com.afirme.midas.sistema.mensajePendiente;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * MensajePendienteDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOMENSAJEPENDIENTE", schema = "MIDAS")
public class MensajePendienteDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToMensajePendiente;
	private Date fechaCreacion;
	private BigDecimal idRol;
	private String descripcionMensaje;
	private BigDecimal estatus;
	private BigDecimal modulo;
	private String parametro1;
	private String parametro2;
	private String parametro3;
	private String url;
	private String siguienteFuncion;

	// Constructors

	/** default constructor */
	public MensajePendienteDTO() {
	}

	/** minimal constructor */
	public MensajePendienteDTO(BigDecimal idToMensajePendiente,
			Date fechaCreacion, BigDecimal idRol, String descripcionMensaje,
			BigDecimal estatus, BigDecimal modulo, String url) {
		this.idToMensajePendiente = idToMensajePendiente;
		this.fechaCreacion = fechaCreacion;
		this.idRol = idRol;
		this.descripcionMensaje = descripcionMensaje;
		this.estatus = estatus;
		this.modulo = modulo;
		this.url = url;
	}

	/** full constructor */
	public MensajePendienteDTO(BigDecimal idToMensajePendiente,
			Date fechaCreacion, BigDecimal idRol, String descripcionMensaje,
			BigDecimal estatus, BigDecimal modulo, String parametro1,
			String parametro2, String parametro3, String url, String siguienteFuncion) {
		this.idToMensajePendiente = idToMensajePendiente;
		this.fechaCreacion = fechaCreacion;
		this.idRol = idRol;
		this.descripcionMensaje = descripcionMensaje;
		this.estatus = estatus;
		this.modulo = modulo;
		this.parametro1 = parametro1;
		this.parametro2 = parametro2;
		this.parametro3 = parametro3;
		this.url = url;
		this.siguienteFuncion = siguienteFuncion;
	}

	// Property accessors
	@Id
	@Column(name = "IDTOMENSAJEPENDIENTE", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToMensajePendiente() {
		return this.idToMensajePendiente;
	}

	public void setIdToMensajePendiente(BigDecimal idToMensajePendiente) {
		this.idToMensajePendiente = idToMensajePendiente;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHACREACION", nullable = false, length = 7)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Column(name = "IDROL", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdRol() {
		return this.idRol;
	}

	public void setIdRol(BigDecimal idRol) {
		this.idRol = idRol;
	}

	@Column(name = "DESCRIPCIONMENSAJE", nullable = false, length = 250)
	public String getDescripcionMensaje() {
		return this.descripcionMensaje;
	}

	public void setDescripcionMensaje(String descripcionMensaje) {
		this.descripcionMensaje = descripcionMensaje;
	}

	@Column(name = "ESTATUS", nullable = false, precision = 22, scale = 0)
	public BigDecimal getEstatus() {
		return this.estatus;
	}

	public void setEstatus(BigDecimal estatus) {
		this.estatus = estatus;
	}

	@Column(name = "MODULO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getModulo() {
		return this.modulo;
	}

	public void setModulo(BigDecimal modulo) {
		this.modulo = modulo;
	}

	@Column(name = "PARAMETRO1", length = 240)
	public String getParametro1() {
		return this.parametro1;
	}

	public void setParametro1(String parametro1) {
		this.parametro1 = parametro1;
	}

	@Column(name = "PARAMETRO2", length = 240)
	public String getParametro2() {
		return this.parametro2;
	}

	public void setParametro2(String parametro2) {
		this.parametro2 = parametro2;
	}

	@Column(name = "PARAMETRO3", length = 240)
	public String getParametro3() {
		return this.parametro3;
	}

	public void setParametro3(String parametro3) {
		this.parametro3 = parametro3;
	}

	@Column(name = "URL", nullable = false, length = 1000)
	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name = "SIGUIENTEFUNCION", length = 240)
	public String getSiguienteFuncion() {
		return siguienteFuncion;
	}

	public void setSiguienteFuncion(String siguienteFuncion) {
		this.siguienteFuncion = siguienteFuncion;
	}
}