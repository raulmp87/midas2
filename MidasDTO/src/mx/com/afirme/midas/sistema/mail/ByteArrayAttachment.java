package mx.com.afirme.midas.sistema.mail;

//import java.io.File;
//import java.io.FileInputStream;
//import java.io.IOException;
import java.io.Serializable;

//import mx.com.afirme.midas.sistema.SystemException;
//import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
//import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;

public class ByteArrayAttachment implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String nombreArchivo;
	
	private TipoArchivo tipoArchivo;
	
	private byte[] contenidoArchivo;
	
	public static enum TipoArchivo {PDF, IMAGEN_JPG, TEXTO, HTML, DESCONOCIDO,XLS,XML};
	
	public ByteArrayAttachment() {

	}
	
	/**
	 * Constructor
	 * @param nombreArchivo Nombre del archivo a adjuntar
	 * @param tipoArchivo Tipo del archivo a adjuntar
	 * @param contenidoArchivo El contenido del archivo en un arreglo de bytes
	 */
	public ByteArrayAttachment(String nombreArchivo, TipoArchivo tipoArchivo, byte[] contenidoArchivo) {
		this.nombreArchivo = nombreArchivo;
		this.tipoArchivo = tipoArchivo;
		this.contenidoArchivo = contenidoArchivo;
	}
	
	/**
	 * @return the nombreArchivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}

	/**
	 * @param nombreArchivo the nombreArchivo to set
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	
	/**
	 * @return the tipoArchivo
	 */
	public TipoArchivo getTipoArchivo() {
		return tipoArchivo;
	}

	/**
	 * @param tipoArchivo the tipoArchivo to set
	 */
	public void setTipoArchivo(TipoArchivo tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}

	/**
	 * @return the contenidoArchivo
	 */
	public byte[] getContenidoArchivo() {
		return contenidoArchivo;
	}

	/**
	 * @param contenidoArchivo the contenidoArchivo to set
	 */
	public void setContenidoArchivo(byte[] contenidoArchivo) {
		this.contenidoArchivo = contenidoArchivo;
	}
	
	
}
