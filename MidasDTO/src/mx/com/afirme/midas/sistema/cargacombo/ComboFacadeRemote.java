package mx.com.afirme.midas.sistema.cargacombo;

import javax.ejb.Remote;



/**
 * Remote interface for ComboFacadeRemote.
 * 
 * @author MyEclipse Persistence Tools
 */


public interface ComboFacadeRemote{

	/**
	 * build id and description of catalog.
	 * 
	 * @param id
	 *           id of catalog
	 * @param desc
	 *           description of catalog
	 * @return build string with id,description of catalog
	 */
	public String buildIdDescription(String id,String desc,String objec);

	
}





