package mx.com.afirme.midas.sistema.log.historico;

import javax.ejb.Remote;

/**
 * Remote interface for LogOperacionHistFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface LogOperacionHistFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved LogOperacionHistDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            LogOperacionHistDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(LogOperacionHistDTO entity);

	
}