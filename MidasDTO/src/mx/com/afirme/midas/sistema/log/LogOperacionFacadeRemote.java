package mx.com.afirme.midas.sistema.log;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Remote;

/**
 * Remote interface for LogOperacionFacade.
 * 
 * @author MyEclipse Persistence Tools
 */
@Local
public interface LogOperacionFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved LogOperacionDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            LogOperacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(LogOperacionDTO entity);

	public void delete(LogOperacionDTO entity);
	
	/**
	 * Encuentra todos los registros de log que deben ser 
	 * transferidos al historico.
	 * @param maximoRegistros Numero maximo de registros que deben permanecer en el log. Si se sobrepasa este numero, se tienen que 
	 * respaldar en el historico
	 * @return List<LogOperacionDTO> Los registros de log que deben ser
	 *  transferidos al historico
	 */
	public List<LogOperacionDTO> buscarRegistrosParaRespaldo(String maximoRegistros);
	
}