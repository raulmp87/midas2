package mx.com.afirme.midas.sistema.log.historico;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * LogOperacionHistDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOOPERACIONLOGHIST", schema = "MIDAS")
public class LogOperacionHistDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idtooperacionloghist;
	private String tipoaccion;
	private BigDecimal idregistro;
	private String tipoentidad;
	private String nombreentidad;
	private String nombretablaentidad;
	private String nombreusuario;
	private Timestamp fecha;

	// Constructors

	/** default constructor */
	public LogOperacionHistDTO() {
	}

	/** minimal constructor */
	public LogOperacionHistDTO(BigDecimal idtooperacionloghist) {
		this.idtooperacionloghist = idtooperacionloghist;
	}

	/** full constructor */
	public LogOperacionHistDTO(BigDecimal idtooperacionloghist,
			String tipoaccion, BigDecimal idregistro, String tipoentidad,
			String nombreentidad, String nombretablaentidad,
			String nombreusuario, Timestamp fecha) {
		this.idtooperacionloghist = idtooperacionloghist;
		this.tipoaccion = tipoaccion;
		this.idregistro = idregistro;
		this.tipoentidad = tipoentidad;
		this.nombreentidad = nombreentidad;
		this.nombretablaentidad = nombretablaentidad;
		this.nombreusuario = nombreusuario;
		this.fecha = fecha;
	}

	// Property accessors
	@Id
	@Column(name = "IDTOOPERACIONLOGHIST", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdtooperacionloghist() {
		return this.idtooperacionloghist;
	}

	public void setIdtooperacionloghist(BigDecimal idtooperacionloghist) {
		this.idtooperacionloghist = idtooperacionloghist;
	}

	@Column(name = "TIPOACCION")
	public String getTipoaccion() {
		return this.tipoaccion;
	}

	public void setTipoaccion(String tipoaccion) {
		this.tipoaccion = tipoaccion;
	}

	@Column(name = "IDREGISTRO", precision = 22, scale = 0)
	public BigDecimal getIdregistro() {
		return this.idregistro;
	}

	public void setIdregistro(BigDecimal idregistro) {
		this.idregistro = idregistro;
	}

	@Column(name = "TIPOENTIDAD")
	public String getTipoentidad() {
		return this.tipoentidad;
	}

	public void setTipoentidad(String tipoentidad) {
		this.tipoentidad = tipoentidad;
	}

	@Column(name = "NOMBREENTIDAD")
	public String getNombreentidad() {
		return this.nombreentidad;
	}

	public void setNombreentidad(String nombreentidad) {
		this.nombreentidad = nombreentidad;
	}

	@Column(name = "NOMBRETABLAENTIDAD")
	public String getNombretablaentidad() {
		return this.nombretablaentidad;
	}

	public void setNombretablaentidad(String nombretablaentidad) {
		this.nombretablaentidad = nombretablaentidad;
	}

	@Column(name = "NOMBREUSUARIO")
	public String getNombreusuario() {
		return this.nombreusuario;
	}

	public void setNombreusuario(String nombreusuario) {
		this.nombreusuario = nombreusuario;
	}

	@Column(name = "FECHA", length = 11)
	public Timestamp getFecha() {
		return this.fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

}