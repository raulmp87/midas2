package mx.com.afirme.midas.sistema.log;

import java.math.BigDecimal;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * LogOperacionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOOPERACIONLOG", schema = "MIDAS")
public class LogOperacionDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idtooperacionlog;
	private String tipoaccion;
	private BigDecimal idregistro;
	private String tipoentidad;
	private String nombreentidad;
	private String nombretablaentidad;
	private String nombreusuario;
	private Timestamp fecha;

	// Constructors

	/** default constructor */
	public LogOperacionDTO() {
	}

	/** minimal constructor */
	public LogOperacionDTO(BigDecimal idtooperacionlog) {
		this.idtooperacionlog = idtooperacionlog;
	}

	/** full constructor */
	public LogOperacionDTO(BigDecimal idtooperacionlog, String tipoaccion,
			BigDecimal idregistro, String tipoentidad, String nombreentidad,
			String nombretablaentidad, String nombreusuario, Timestamp fecha) {
		this.idtooperacionlog = idtooperacionlog;
		this.tipoaccion = tipoaccion;
		this.idregistro = idregistro;
		this.tipoentidad = tipoentidad;
		this.nombreentidad = nombreentidad;
		this.nombretablaentidad = nombretablaentidad;
		this.nombreusuario = nombreusuario;
		this.fecha = fecha;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTOOPERACIONLOG_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOOPERACIONLOG_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOOPERACIONLOG_SEQ_GENERADOR")	  	
	@Column(name = "IDTOOPERACIONLOG", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdtooperacionlog() {
		return this.idtooperacionlog;
	}

	public void setIdtooperacionlog(BigDecimal idtooperacionlog) {
		this.idtooperacionlog = idtooperacionlog;
	}

	@Column(name = "TIPOACCION")
	public String getTipoaccion() {
		return this.tipoaccion;
	}

	public void setTipoaccion(String tipoaccion) {
		this.tipoaccion = tipoaccion;
	}

	@Column(name = "IDREGISTRO", precision = 22, scale = 0)
	public BigDecimal getIdregistro() {
		return this.idregistro;
	}

	public void setIdregistro(BigDecimal idregistro) {
		this.idregistro = idregistro;
	}

	@Column(name = "TIPOENTIDAD")
	public String getTipoentidad() {
		return this.tipoentidad;
	}

	public void setTipoentidad(String tipoentidad) {
		this.tipoentidad = tipoentidad;
	}

	@Column(name = "NOMBREENTIDAD")
	public String getNombreentidad() {
		return this.nombreentidad;
	}

	public void setNombreentidad(String nombreentidad) {
		this.nombreentidad = nombreentidad;
	}

	@Column(name = "NOMBRETABLAENTIDAD")
	public String getNombretablaentidad() {
		return this.nombretablaentidad;
	}

	public void setNombretablaentidad(String nombretablaentidad) {
		this.nombretablaentidad = nombretablaentidad;
	}

	@Column(name = "NOMBREUSUARIO")
	public String getNombreusuario() {
		return this.nombreusuario;
	}

	public void setNombreusuario(String nombreusuario) {
		this.nombreusuario = nombreusuario;
	}

	@Column(name = "FECHA", length = 11)
	public Timestamp getFecha() {
		return this.fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

}