package mx.com.afirme.midas.sistema.controlArchivo;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * ControlArchivoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOCONTROLARCHIVO", schema = "MIDAS")
public class ControlArchivoDTO implements java.io.Serializable,Entidad {

	// Fields
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToControlArchivo;
	private String claveTipo;
	private String nombreArchivoOriginal;
	private String codigoExterno;
	private String extension;
	private BigDecimal idArchivo;
	//KEY NAME STRING
	public static final String IDTOCONTROLARCHIVO = "idToControlArchivo";
	
	public static final Byte ARCHIVO_DOCUMENTO_SINIESTRO = 14;
	// Constructors

	/** default constructor */
	public ControlArchivoDTO() {
	}

	public String obtenerNombreArchivoFisico() {
		String fileName = getNombreArchivoOriginal();
		String extension = null;
		if(fileName != null)
			extension = (fileName.lastIndexOf(".") == -1) ? "" : fileName.substring(fileName.lastIndexOf("."), fileName.length());
		String nombre = "";
		if(getIdToControlArchivo() != null)
			nombre = getIdToControlArchivo().toString() + extension;
		return nombre;
	}
	
	/** full constructor */
	public ControlArchivoDTO(BigDecimal idToControlArchivo, String claveTipo,
			String nombreArchivoOriginal) {
		this.idToControlArchivo = idToControlArchivo;
		this.claveTipo = claveTipo;
		this.nombreArchivoOriginal = nombreArchivoOriginal;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTOCONTROLARCHIVO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOCONTROLARCHIVO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOCONTROLARCHIVO_SEQ_GENERADOR")
	@Column(name = "IDTOCONTROLARCHIVO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToControlArchivo() {
		return this.idToControlArchivo;
	}

	public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
		this.idToControlArchivo = idToControlArchivo;
	}

	@Column(name = "CLAVETIPO", nullable = false, length = 10)
	public String getClaveTipo() {
		return this.claveTipo;
	}

	public void setClaveTipo(String claveTipo) {
		this.claveTipo = claveTipo;
	}

	@Column(name = "NOMBREARCHIVOORIGINAL", nullable = false, length = 200)
	public String getNombreArchivoOriginal() {
		return this.nombreArchivoOriginal;
	}

	public void setNombreArchivoOriginal(String nombreArchivoOriginal) {
		this.nombreArchivoOriginal = nombreArchivoOriginal;
	}
	
	@Column(name = "CODIGOEXTERNO", nullable = true, length = 200)
	public String getCodigoExterno() {
		return codigoExterno;
	}

	public void setCodigoExterno(String codigoExterno) {
		this.codigoExterno = codigoExterno;
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public  BigDecimal getKey() {
		return this.getIdToControlArchivo();
	}

    @Transient	
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}

   @Transient
	public BigDecimal getIdArchivo() {
		return idArchivo;
	}

	public void setIdArchivo(BigDecimal idArchivo) {
		this.idArchivo = idArchivo;
	}

	@Override
	public String getValue() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}
}