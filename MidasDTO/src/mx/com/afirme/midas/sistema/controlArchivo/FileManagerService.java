package mx.com.afirme.midas.sistema.controlArchivo;

import java.math.BigDecimal;
import java.util.Map;

import javax.ejb.Local;

@Local
public interface FileManagerService {
	public static final BigDecimal GRUPO_PARAMETRO_FORTIMAX = new BigDecimal("20");
	public static final BigDecimal GRUPO_PARAMETRO_ESTRUCTURA_FORTIMAX = new BigDecimal("25");
	public static final BigDecimal PARAMETRO_URL = new BigDecimal("200010");
	public static final BigDecimal PARAMETRO_GAVETA = new BigDecimal("200020");
	public static final BigDecimal PARAMETRO_GAVETA_AUTOS = new BigDecimal("200120");
	public static final BigDecimal PARAMETRO_CARPETA = new BigDecimal("200030");
	public static final BigDecimal PARAMETRO_CARPETA_AUTOS = new BigDecimal("200130");
	public static final BigDecimal PARAMETRO_USUARIO = new BigDecimal("200040");
	public static final BigDecimal PARAMETRO_PASSWORD = new BigDecimal("200050");
	public static final BigDecimal PARAMETRO_EXPEDIENTE = new BigDecimal("200060");
	public static final BigDecimal PARAMETRO_FIELD_NAME = new BigDecimal("200070");
	public static final BigDecimal PARAMETRO_GAVETA_CG = new BigDecimal("200080");
	public static final BigDecimal PARAMETRO_CARPETA_DANOS = new BigDecimal("200090");
	public static final BigDecimal PARAMETRO_FIELDNAME_DANOS = new BigDecimal("200100");
	public static final BigDecimal PARAMETRO_EXPEDIENTE_DANOS = new BigDecimal("200110");
	public static final BigDecimal PARAMETRO_CREA_FILESYSTEM = new BigDecimal("200160");
	public static final BigDecimal PARAMETRO_SWITCH_FORTIMAX = new BigDecimal("200170");
	public static final BigDecimal PARAMETRO_SWITCH_MIGRACION = new BigDecimal("200180");
	public static final BigDecimal PARAMETRO_TIPO_ESTRUCTURA = new BigDecimal("200204");
	public static final BigDecimal PARAMETRO_CAMPOS_ESTRUCTURA = new BigDecimal("200205");

	public static final String WSDL_DOCUMENTO = "/DocumentoService?wsdl";
	public static final String WSDL_LINK = "/LinkService?wsdl";
	public static final String WSDL_EXPEDIENTE = "/ExpedienteService?wsdl";
	public static final String WSDL_LOGIN = "/LoginService?wsdl";
	public static final String WSDL_DOWNLOAD = "/DownloadService?wsdl";
	public static final String WSDL_MAKEZIP = "/MakeZipService?wsdl";
	public static final String WSDL_BUSQUEDA = "/BusquedaService?wsdl";
	
	public static final String CLAVE_DOCUMENTO = "D";
	public static final String CLAVE_CARPETA = "C";
	
	public FileManagerResponse uploadFileFortimax(String fileName, String idControlArchivo, byte[] fileData, Map<String,Object> parametros);
	public boolean uploadFile(String fileName, String idControlArchivo, byte[] file);
	public boolean uploadFile(String fileName, String idControlArchivo, byte[] fileData, boolean highAvailability);
	public byte[] downloadFile(String fileName, String idControlArchivo);
	public byte[] downloadFileFromDisk(String fileName);
	public String getFileName(ControlArchivoDTO controlArchivoDTO);
	public boolean uploadFileCG(String fileName, byte[] fileData, boolean highAvailability);
	public byte[] downloadFileCG(String fileName);
	public String traerParametroGral(BigDecimal grupo, BigDecimal param);
	public boolean uploadFileToDisk(String fileName, byte[] fileData, boolean migrar , String idControlArchivo);
	
}
