package mx.com.afirme.midas.sistema.controlArchivo.migracion;

// default package

import java.util.List;


import mx.com.afirme.midas.sistema.controlArchivo.migracion.MigracionControlArchivoAutoDTO.Estatus;

/**
 * Remote interface for MigracionControlArchivoAutoDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface MigracionControlArchivoAutoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved
	 * MigracionControlArchivoAutoDTO entity. All subsequent persist actions of this
	 * entity should use the #update() method.
	 * 
	 * @param entity
	 *            MigracionControlArchivoAutoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(MigracionControlArchivoAutoDTO entity);

	/**
	 * Delete a persistent MigracionControlArchivoAutoDTO entity.
	 * 
	 * @param entity
	 *            MigracionControlArchivoAutoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(MigracionControlArchivoAutoDTO entity);

	/**
	 * Persist a previously saved MigracionControlArchivoAutoDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * MigracionControlArchivoAutoDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            MigracionControlArchivoAutoDTO entity to update
	 * @return MigracionControlArchivoAutoDTO the persisted
	 *         MigracionControlArchivoAutoDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public MigracionControlArchivoAutoDTO update(MigracionControlArchivoAutoDTO entity);

	public MigracionControlArchivoAutoDTO findById(Long id);

	/**
	 * Find all MigracionControlArchivoAutoDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the MigracionControlArchivoAutoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<MigracionControlArchivoAutoDTO> found by query
	 */
	public List<MigracionControlArchivoAutoDTO> findByProperty(String propertyName,
			Object value);

	public List<MigracionControlArchivoAutoDTO> findByEstatus(Estatus estatus);

	/**
	 * Find all MigracionControlArchivoAutoDTO entities.
	 * 
	 * @return List<MigracionControlArchivoAutoDTO> all MigracionControlArchivoAutoDTO
	 *         entities
	 */
	public List<MigracionControlArchivoAutoDTO> findAll();
	
	
}