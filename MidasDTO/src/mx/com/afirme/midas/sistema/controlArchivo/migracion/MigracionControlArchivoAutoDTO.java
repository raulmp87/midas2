package mx.com.afirme.midas.sistema.controlArchivo.migracion;
// default package

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;

/**
 * MigracionControlArchivoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOMIGRACIONCONTROLARCHIVOAUTO", schema = "MIDAS", 
		uniqueConstraints = @UniqueConstraint(columnNames = "IDTOCONTROLARCHIVO"))
public class MigracionControlArchivoAutoDTO implements java.io.Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 3046851367483383273L;
	private Long id;
	private ControlArchivoDTO controlArchivo;
	private String estatus;
	private String detalleEstatus;
	private Date fechaEstatus;
	
	public enum Estatus {
		PENDIENTE("PENDIENTE"), MIGRADO("MIGRADO"), ERROR("ERROR");
		private String valor;

		private Estatus(String valor) {
			this.valor = valor;
		}

		public String getValor() {
			return this.valor;
		}
	}

	// Constructors

	/** default constructor */
	public MigracionControlArchivoAutoDTO() {
	}


	/**
	 * Constructor minimo
	 * @param tocontrolarchivo
	 * @param estatus
	 */
	public MigracionControlArchivoAutoDTO(ControlArchivoDTO tocontrolarchivo, Estatus estatus) {
		this.controlArchivo = tocontrolarchivo;
		this.estatus = estatus.getValor();
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "TOMIGRACIONCONTROLARCHAUT_SEQ", allocationSize = 1, sequenceName = "MIDAS.TOMIGRACIONCONTROLARCHAUT_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TOMIGRACIONCONTROLARCHAUT_SEQ")
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="idToControlArchivo")
	public ControlArchivoDTO getControlArchivo() {
		return this.controlArchivo;
	}

	public void setControlArchivo(ControlArchivoDTO controlArchivo) {
		this.controlArchivo = controlArchivo;
	}

	@Column(name = "ESTATUS", nullable = false, length = 50)
	public String getEstatus() {
		return this.estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	@Column(name = "DETALLEESTATUS")
	public String getDetalleEstatus() {
		return this.detalleEstatus;
	}

	public void setDetalleEstatus(String detalleEstatus) {
		this.detalleEstatus = detalleEstatus;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAESTATUS", nullable = false, length = 11)
	public Date getFechaEstatus() {
		return this.fechaEstatus;
	}

	public void setFechaEstatus(Date fechaEstatus) {
		this.fechaEstatus = fechaEstatus;
	}

}