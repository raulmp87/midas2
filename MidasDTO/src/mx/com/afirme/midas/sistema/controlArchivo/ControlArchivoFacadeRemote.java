package mx.com.afirme.midas.sistema.controlArchivo;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for ControlArchivoDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ControlArchivoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved ControlArchivoDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            ControlArchivoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public ControlArchivoDTO save(ControlArchivoDTO entity);

	/**
	 * Delete a persistent ControlArchivoDTO entity.
	 * 
	 * @param entity
	 *            ControlArchivoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ControlArchivoDTO entity);

	/**
	 * Persist a previously saved ControlArchivoDTO entity and return it or a
	 * copy of it to the sender. A copy of the ControlArchivoDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            ControlArchivoDTO entity to update
	 * @return ControlArchivoDTO the persisted ControlArchivoDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ControlArchivoDTO update(ControlArchivoDTO entity);

	public ControlArchivoDTO findById(BigDecimal id);

	/**
	 * Find all ControlArchivoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ControlArchivoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<ControlArchivoDTO> found by query
	 */
	public List<ControlArchivoDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all ControlArchivoDTO entities.
	 * 
	 * @return List<ControlArchivoDTO> all ControlArchivoDTO entities
	 */
	public List<ControlArchivoDTO> findAll();
	
	public void copy(ControlArchivoDTO in, ControlArchivoDTO out);
}