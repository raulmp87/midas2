package mx.com.afirme.midas.sistema.controlArchivo;

public class FileManagerResponse {


	private static final long serialVersionUID = 1L;
	
	private Boolean success;
	private String nodo;
	
	public FileManagerResponse( Boolean success, String nodo){
		this.success = success;
		this.nodo = nodo;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getNodo() {
		return nodo;
	}

	public void setNodo(String nodo) {
		this.nodo = nodo;
	}
	
	@Override
	public String toString(){
		
		return "success="+ this.success + "  nodo=" + this.nodo; 
		
	}
	
}
