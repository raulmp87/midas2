/**
 * 
 */
package mx.com.afirme.midas.sistema.temporizador;

import java.math.BigDecimal;

import javax.ejb.Remote;

import mx.com.afirme.midas.reaseguro.soporte.SoporteReaseguroDTO;

/**
 * @author alfredo.osorio
 *
 */

public interface TemporizadorDistribuirPolizaFacadeRemote {

	/**
	 * Inicia el temporizador
	 * @param tiempoIniciar Tiempo para que se inicie el temporizador (en milisegundos)
	 * @param soporteReaseguroDTO Este parametro es el que se le va a mandar el timer para
	 * como parametro info se espera que este objeto tenga establecido estos dos valores:
	 * idToPoliza y numeroEndoso.
	 */
	public void iniciarTemporizador(long tiempoIniciar, SoporteReaseguroDTO soporteReaseguroDTO);

	/**
	 * Inicia el temporizador
	 * @param tiempoIniciar Tiempo para que se inicie el temporizador (en milisegundos)
	 * @param idToSoporteReaseguro Este parametro es el id del soporte que se distribuir�.
	 */
	public void iniciarTemporizador(long tiempoIniciar, BigDecimal idToSoporteReaseguro);
	
	/**
	 * Detiene los temporizadores de distribuci�n de p�lizas.
	 */
	public void detenerTemporizadoresDistribucionPolizas();
}
