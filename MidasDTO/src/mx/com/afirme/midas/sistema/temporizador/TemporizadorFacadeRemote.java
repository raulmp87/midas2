/**
 * 
 */
package mx.com.afirme.midas.sistema.temporizador;

import javax.ejb.Remote;

/**
 * @author andres.avalos
 *
 */

public interface TemporizadorFacadeRemote {

	/**
	 * Inicia el temporizador
	 * @param tiempoIniciar Tiempo para que se inicie el temporizador (en milisegundos)
	 * @param tiempoIntervalo Intervalo de tiempo entre cada ejecucion del evento TimeOut del temporizador (en milisegundos)
	 * @param maximoRegistros Numero maximo de registros que deben permanecer en el log. Si se sobrepasa este numero, se tienen que 
	 * respaldar en el historico
	 */
	public void iniciarTemporizador(long tiempoIniciar, long tiempoIntervalo, String maximoRegistros);
	
	/**
	 * Detiene el temporizador
	 */
	public void detenerTemporizador();
	
	
}
