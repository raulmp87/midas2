/**
 * 
 */
package mx.com.afirme.midas.sistema.temporizador;

import javax.ejb.Remote;

/**
 * @author alfredo.osorio
 *
 */

public interface TemporizadorDistribuirPrimaFacadeRemote {

	/**
	 * Inicia el temporizador
	 * @param tiempoIniciar Tiempo para que se inicie el temporizador (en milisegundos)
	 * @param tiempoIntervalo Intervalo de tiempo entre cada ejecucion del evento TimeOut del temporizador (en milisegundos)
	 */
	public void iniciarTemporizador(long tiempoIniciar);
	
	/**
	 * Detiene el temporizador
	 */
	public void detenerTemporizador();
	
	
}
