/**
 * 
 */
package mx.com.afirme.midas.sistema.temporizador;

import java.math.BigDecimal;
import java.util.Date;

import javax.ejb.Remote;

/**
 * @author alfredo.osorio
 *
 */

public interface TemporizadorDistribuirSiniestroFacadeRemote {

	/**
	 * Inicia el temporizador
	 * @param tiempoIniciar Tiempo para que se inicie el temporizador (en milisegundos)
	 * @param soporteReaseguroDTO Este parametro es el que se le va a mandar el timer para
	 * como parametro info se espera que este objeto tenga establecido estos dos valores:
	 * idToPoliza y numeroEndoso.
	 */
	public void iniciarTemporizador(long tiempoIniciar, 
			BigDecimal idToPoliza, Integer numeroEndoso,
			BigDecimal idToSeccion, BigDecimal idToCobertura,
			Integer numeroInciso, Integer numeroSubInciso,
			Integer conceptoMovimiento,	BigDecimal idToMoneda, 
			Date fechaMovimiento, BigDecimal idMovimientoSiniestro,
			BigDecimal montoMovimiento, Integer tipoMovimiento,
			BigDecimal idToReporteSiniestro, 
			BigDecimal idtoDistribucionMovSiniestro);
	
}
