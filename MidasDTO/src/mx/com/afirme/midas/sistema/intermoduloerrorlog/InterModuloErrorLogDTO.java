package mx.com.afirme.midas.sistema.intermoduloerrorlog;

import java.math.BigDecimal;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Tointermoderrorlog entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOINTERMODERRORLOG", schema = "MIDAS")
public class InterModuloErrorLogDTO implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private BigDecimal idToInterModuloErrorLog;
	private Timestamp fechaHora;
	private String nombreUsuario;
	private Short moduloCapturaError;
	private String objetoCapturaError;
	private String metodoCapturaError;
	private Short moduloOrigenError;
	private String objetoOrigenError;
	private String metodoOrigenError;
	private String paramMetodoOrigenError;
	private String retornoMetodoorigenError;
	private String excepcion;
	private String descripcionError;
	private String comentariosAdicionales;

	// Constructors

	/** default constructor */
	public InterModuloErrorLogDTO() {
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTOINTERMODERRORLOG_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOINTERMODERRORLOG_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOINTERMODERRORLOG_SEQ_GENERADOR")
	@Column(name = "IDTOINTERMODERRORLOG", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToInterModuloErrorLog() {
		return idToInterModuloErrorLog;
	}
	public void setIdToInterModuloErrorLog(BigDecimal idToInterModuloErrorLog) {
		this.idToInterModuloErrorLog = idToInterModuloErrorLog;
	}

	@Column(name = "FECHAHORA", nullable = false, length = 11)
	public Timestamp getFechaHora() {
		return fechaHora;
	}
	public void setFechaHora(Timestamp fechaHora) {
		this.fechaHora = fechaHora;
	}

	@Column(name = "NOMBREUSUARIO", nullable = false, length = 50)
	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	@Column(name = "MODULOCAPTURAERROR", nullable = false, precision = 4, scale = 0)
	public Short getModuloCapturaError() {
		return moduloCapturaError;
	}
	public void setModuloCapturaError(Short moduloCapturaError) {
		this.moduloCapturaError = moduloCapturaError;
	}

	@Column(name = "OBJETOCAPTURAERROR", nullable = false, length = 200)
	public String getObjetoCapturaError() {
		return objetoCapturaError;
	}
	public void setObjetoCapturaError(String objetoCapturaError) {
		this.objetoCapturaError = objetoCapturaError;
	}

	@Column(name = "METODOCAPTURAERROR", nullable = false, length = 100)
	public String getMetodoCapturaError() {
		return metodoCapturaError;
	}

	public void setMetodoCapturaError(String metodoCapturaError) {
		this.metodoCapturaError = metodoCapturaError;
	}

	@Column(name = "MODULOORIGENERROR", nullable = false, precision = 4, scale = 0)
	public Short getModuloOrigenError() {
		return moduloOrigenError;
	}
	public void setModuloOrigenError(Short moduloOrigenError) {
		this.moduloOrigenError = moduloOrigenError;
	}

	@Column(name = "OBJETOORIGENERROR", nullable = false, length = 200)
	public String getObjetoOrigenError() {
		return objetoOrigenError;
	}
	public void setObjetoOrigenError(String objetoOrigenError) {
		this.objetoOrigenError = objetoOrigenError;
	}

	@Column(name = "METODOORIGENERROR", nullable = false, length = 100)
	public String getMetodoOrigenError() {
		return metodoOrigenError;
	}
	public void setMetodoOrigenError(String metodoOrigenError) {
		this.metodoOrigenError = metodoOrigenError;
	}

	@Column(name = "PARAMMETODOORIGENERROR", nullable = false, length = 2000)
	public String getParamMetodoOrigenError() {
		return paramMetodoOrigenError;
	}
	public void setParamMetodoOrigenError(String paramMetodoOrigenError) {
		this.paramMetodoOrigenError = paramMetodoOrigenError;
	}

	@Column(name = "RETORNOMETODOORIGENERROR", nullable = false, length = 200)
	public String getRetornoMetodoorigenError() {
		return retornoMetodoorigenError;
	}
	public void setRetornoMetodoorigenError(String retornoMetodoorigenError) {
		this.retornoMetodoorigenError = retornoMetodoorigenError;
	}

	@Column(name = "EXCEPCION", nullable = false, length = 500)
	public String getExcepcion() {
		return this.excepcion;
	}
	public void setExcepcion(String excepcion) {
		this.excepcion = excepcion;
	}

	@Column(name = "DESCRIPCIONERROR", nullable = false, length = 500)
	public String getDescripcionError() {
		return descripcionError;
	}
	public void setDescripcionError(String descripcionError) {
		this.descripcionError = descripcionError;
	}

	@Column(name = "COMENTARIOSADICIONALES", nullable = false, length = 500)
	public String getComentariosAdicionales() {
		return comentariosAdicionales;
	}
	public void setComentariosAdicionales(String comentariosAdicionales) {
		this.comentariosAdicionales = comentariosAdicionales;
	}
}