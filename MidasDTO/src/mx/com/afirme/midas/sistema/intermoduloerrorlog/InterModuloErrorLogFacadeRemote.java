package mx.com.afirme.midas.sistema.intermoduloerrorlog;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for TointermoderrorlogFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface InterModuloErrorLogFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved Tointermoderrorlog
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            Tointermoderrorlog entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(InterModuloErrorLogDTO entity);

	/**
	 * Delete a persistent Tointermoderrorlog entity.
	 * 
	 * @param entity
	 *            Tointermoderrorlog entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(InterModuloErrorLogDTO entity);

	/**
	 * Persist a previously saved Tointermoderrorlog entity and return it or a
	 * copy of it to the sender. A copy of the Tointermoderrorlog entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            Tointermoderrorlog entity to update
	 * @return Tointermoderrorlog the persisted Tointermoderrorlog entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public InterModuloErrorLogDTO update(InterModuloErrorLogDTO entity);

	public InterModuloErrorLogDTO findById(BigDecimal id);

	/**
	 * Find all Tointermoderrorlog entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the Tointermoderrorlog property to query
	 * @param value
	 *            the property value to match
	 * @return List<Tointermoderrorlog> found by query
	 */
	public List<InterModuloErrorLogDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all Tointermoderrorlog entities.
	 * 
	 * @return List<Tointermoderrorlog> all Tointermoderrorlog entities
	 */
	public List<InterModuloErrorLogDTO> findAll();
	
	public List<InterModuloErrorLogDTO> listarFiltrado(InterModuloErrorLogDTO entity);
}