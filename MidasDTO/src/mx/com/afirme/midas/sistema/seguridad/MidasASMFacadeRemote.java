package mx.com.afirme.midas.sistema.seguridad;

import java.util.List;

import javax.ejb.Remote;

import com.asm.dto.PageConsentDTO;
import com.asm.dto.UserDTO;

/**
 * @deprecated desde Midas 2.0. Utilizar {@link mx.com.afirme.midas2.service.seguridad.UsuarioService}
 */
@Deprecated

public interface MidasASMFacadeRemote {
	/**
	 * Metodo que hace logout en ASM a un usuario especifico
	 * 
	 * @param nombreUsuario
	 *            nombre del usuario que se desea hacer logout
	 * @param idAccesoUsuario
	 *            id que determina si la session del usuario que desea consultar
	 *            es valida o no.
	 * @return true si fue exitoso, false si no fue exitoso
	 * 
	 */
	public boolean logOutUsuario(String nombreUsuario, int idAccesoUsuario);

	/**
	 * Metodo que bloquea un usuario deacuerdo al IdUsuario
	 * 
	 * @param idUsuario
	 *            idUsuario que se pretende bloquear
	 */
	public void bloqueaAcceso(int idUsuario);

	/**
	 * Metodo que obtiene una lista de Consentimientos por pagina de acuerdo a
	 * un idRole especifico
	 * 
	 * @param idRol
	 *            id del rol que se desea consultar
	 * @return List<PageConsentDTO> Lista de consentimientos por pagina que
	 *         corresponden al id del rol solicitado
	 */
	public List<PageConsentDTO> obtieneListaConsentimientos(int idRole);

	/**
	 * Metodo que obtiene un Usuario Registrado por nombre de usuario especifico
	 * 
	 * @param nombreUsuario
	 *            nombre del usuario que se desea consultar
	 * @param idAccesoUsuario
	 *            id que determina si la session del usuario que desea consultar
	 *            es valida o no.
	 * @return UserDTO usuario solicitado
	 * 
	 */
	public UserDTO obtieneUsuarioRegistrado(String nombreUsuario,
			int idAccesoUsuario);

	/**
	 * Metodo que obtiene un Usuario sin roles por un id de usuario especifico
	 * 
	 * @param idUsuario
	 *            id del usuario que se desea consultar
	 * @param idAccesoUsuario
	 *            id que determina si la session del usuario que desea consultar
	 *            es valida o no.
	 * @return UserDTO usuario solicitado
	 * 
	 */
	public UserDTO obtieneUsuarioSinRolesPorIdUsuario(int idUsuario,
			int idAccesoUsuario);

	/**
	 * Metodo que obtiene un Usuario sin roles por un nombre de usuario
	 * especifico
	 * 
	 * @param nombreUsuario
	 *            nombre del usuario que se desea consultar
	 * @param idAccesoUsuario
	 *            id que determina si la session del usuario que desea consultar
	 *            es valida o no.
	 * @return UserDTO usuario solicitado
	 * 
	 */
	public UserDTO obtieneUsuarioSinRolesPorNombreUsuario(String nombreUsuario,
			int idAccesoUsuario);

	/**
	 * Metodo que obtiene una lista de Usuarios sin roles por un id de rol
	 * especifico
	 * 
	 * @param idRol
	 *            id del rol que se desea consultar
	 * @param nombreUsuario
	 *            nombre del usuario que desea hacer la peticion
	 * @param idAccesoUsuario
	 *            id que determina si la session del usuario que desea consultar
	 *            es valida o no.
	 * @return List<UserDTO> Lista de usuarios que corresponden al id del rol
	 *         solicitado
	 */
	public List<UserDTO> obtieneUsuariosSinRolesPorIdRol(int idRol,
			String nombreUsuario, int idAccesoUsuario);

	/**
	 * Metodo que obtiene una lista de Usuarios sin roles por un nombre de rol
	 * especifico
	 * 
	 * @param nombreRol
	 *            nombre del rol que se desea consultar
	 * @param nombreUsuario
	 *            nombre del usuario que desea hacer la peticion
	 * @param idAccesoUsuario
	 *            id que determina si la session del usuario que desea consultar
	 *            es valida o no.
	 * @return List<UserDTO> Lista de usuarios que corresponden al nombre del
	 *         rol solicitados
	 */
	public List<UserDTO> obtieneUsuariosSinRolesPorNombreRol(String nombreRol,
			String nombreUsuario, int idAccesoUsuario);
	/**
	 * Metodo que obtiene un usuario especifico
	 * 
	 * @param nombreUsuario
	 *            nombre del usuario que desea hacer la peticion
	 * @return UserDTO usuario que corresponden al nombre solicitado
	 */	
	public UserDTO obtieneUsuario(String nombreUsuario);
	
	/**
	 * Metodo que obtiene un usuario especifico
	 * 
	 * @param idUsuario
	 *            id del usuario que desea hacer la peticion
	 * @return UserDTO usuario que corresponden al nombre solicitado
	 */	
	public UserDTO obtieneUsuario(int idUsuario);
}
