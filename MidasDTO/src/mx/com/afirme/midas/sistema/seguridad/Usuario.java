/**
 * Clase Usuario de la aplicaci�n MIDAS
 */
package mx.com.afirme.midas.sistema.seguridad;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.domain.siniestro.Cabina;

/**
 * @author andres.avalos
 *
 */
public class Usuario implements java.io.Serializable {

	
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String nombreUsuario;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private byte sexo;
	private Date fechaNacimiento;
	private byte estadoCivil;
	private String email;
	private String telefonoCasa;
	private String telefonoCelular;
	private String telefonoOficina;
	private String inicialesRFC;
	private String fechaRFC;
	private String homoclaveRFC;
	private String curp;
	private Integer personaId;
	private String idSesionUsuario;
	private String token;
	private boolean activo = false;
	private List<Rol> roles;
	private ArrayList<AtributoUsuario> listaAtributos= new ArrayList<AtributoUsuario>();
	private boolean isOutsider;
	private String promoCode;
	private String deviceUuid;
	private List<Menu> menus;
	private List<Pagina> pages;
	
	//Tipos de Usuarios
	public static final int USUARIO_INTERNO = 1;
	public static final int USUARIO_EXTERNO = 2;
	
	public Integer getTipoUsuario()
	{
		if(!this.isOutsider)
		{
			return USUARIO_INTERNO;			
		}else
		{
			return USUARIO_EXTERNO;			
		}
	}
	
	/**
	 * La cabina que tiene asociado este usuario.
	 */
	private Cabina cabina;
	
	public Usuario(){
		cabina = new Cabina();
		cabina.setId(1l);
		cabina.setNombre("MONTERREY");
	}
	public String getNombreCompleto(){
		StringBuilder fullName = new StringBuilder("");
		if(this.nombre != null){
			fullName.append(this.nombre + " ");
		}
		if(this.apellidoPaterno != null){
			fullName.append(this.apellidoPaterno + " ");
		}
		if(this.apellidoMaterno != null){
			fullName.append(this.apellidoMaterno + " ");
		}
		
		return fullName.toString();
	}
	public Usuario(Integer id, String nombre, String nombreUsuario){
		this.id = id;
		this.nombre = nombre;
		this.nombreUsuario = nombreUsuario;
		this.roles = new ArrayList<Rol>();
		this.menus = new ArrayList<Menu>();
		this.pages = new ArrayList<Pagina>();
	}
	
	public Usuario(Integer id, String nombre, String nombreUsuario,String email){
		this.id = id;
		this.nombre = nombre;
		this.nombreUsuario = nombreUsuario;
		this.roles = new ArrayList<Rol>();
		this.email=email;
		this.menus = new ArrayList<Menu>();
		this.pages = new ArrayList<Pagina>();
	}
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the nombreUsuario
	 */
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	/**
	 * @param nombreUsuario the nombreUsuario to set
	 */
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the apellidoPaterno
	 */
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	/**
	 * @param apellidoPaterno the apellidoPaterno to set
	 */
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	/**
	 * @return the apellidoMaterno
	 */
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	/**
	 * @param apellidoMaterno the apellidoMaterno to set
	 */
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	/**
	 * @return the sexo
	 */
	public byte getSexo() {
		return sexo;
	}

	/**
	 * @param sexo the sexo to set
	 */
	public void setSexo(byte sexo) {
		this.sexo = sexo;
	}

	/**
	 * @return the fechaNacimiento
	 */
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	/**
	 * @param fechaNacimiento the fechaNacimiento to set
	 */
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	

	/**
	 * @return the estadoCivil
	 */
	public byte getEstadoCivil() {
		return estadoCivil;
	}

	/**
	 * @param estadoCivil the estadoCivil to set
	 */
	public void setEstadoCivil(byte estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the telefonoCasa
	 */
	public String getTelefonoCasa() {
		return telefonoCasa;
	}

	/**
	 * @param telefonoCasa the telefonoCasa to set
	 */
	public void setTelefonoCasa(String telefonoCasa) {
		this.telefonoCasa = telefonoCasa;
	}

	/**
	 * @return the telefonoCelular
	 */
	public String getTelefonoCelular() {
		return telefonoCelular;
	}

	/**
	 * @param telefonoCelular the telefonoCelular to set
	 */
	public void setTelefonoCelular(String telefonoCelular) {
		this.telefonoCelular = telefonoCelular;
	}

	/**
	 * @return the telefonoOficina
	 */
	public String getTelefonoOficina() {
		return telefonoOficina;
	}

	/**
	 * @param telefonoOficina the telefonoOficina to set
	 */
	public void setTelefonoOficina(String telefonoOficina) {
		this.telefonoOficina = telefonoOficina;
	}

	/**
	 * @return the inicialesRFC
	 */
	public String getInicialesRFC() {
		return inicialesRFC;
	}

	/**
	 * @param inicialesRFC the inicialesRFC to set
	 */
	public void setInicialesRFC(String inicialesRFC) {
		this.inicialesRFC = inicialesRFC;
	}

	/**
	 * @return the fechaRFC
	 */
	public String getFechaRFC() {
		return fechaRFC;
	}

	/**
	 * @param fechaRFC the fechaRFC to set
	 */
	public void setFechaRFC(String fechaRFC) {
		this.fechaRFC = fechaRFC;
	}

	/**
	 * @return the homoclaveRFC
	 */
	public String getHomoclaveRFC() {
		return homoclaveRFC;
	}

	/**
	 * @param homoclaveRFC the homoclaveRFC to set
	 */
	public void setHomoclaveRFC(String homoclaveRFC) {
		this.homoclaveRFC = homoclaveRFC;
	}

	/**
	 * @return the curp
	 */
	public String getCurp() {
		return curp;
	}

	/**
	 * @param curp the curp to set
	 */
	public void setCurp(String curp) {
		this.curp = curp;
	}

	/**
	 * @return the personaId
	 */
	public Integer getPersonaId() {
		return personaId;
	}

	/**
	 * @param personaId the personaId to set
	 */
	public void setPersonaId(Integer personaId) {
		this.personaId = personaId;
	}

	public String getIdSesionUsuario() {
		return idSesionUsuario;
	}

	public void setIdSesionUsuario(String idSesionUsuario) {
		this.idSesionUsuario = idSesionUsuario;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	/**
	 * @return the activo
	 */
	public boolean isActivo() {
		return activo;
	}

	/**
	 * @param activo the activo to set
	 */
	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	/**
	 * @return the roles
	 */
	public List<Rol> getRoles() {
		return roles;
	}
	/**
	 * @param roles the roles to set
	 */
	public void setRoles(List<Rol> roles) {
		this.roles = roles;
	}
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof Usuario) {
			Usuario usuario = (Usuario) object;
			if(usuario.getId()!= null){
				equal = usuario.getId().equals(this.getId());
			}
		} // End of if
		return equal;
	}

	public ArrayList<AtributoUsuario> getListaAtributos() {
		return listaAtributos;
	}

	public void setListaAtributos(ArrayList<AtributoUsuario> listaAtributos) {
		this.listaAtributos = listaAtributos;
	}
	
	public void agregarAtributo(AtributoUsuario atributo){
		if(listaAtributos == null)
			listaAtributos = new ArrayList<AtributoUsuario>();
		listaAtributos.add(atributo);
	}

	public ArrayList<AtributoUsuario> obtenerAtributos() {
		return listaAtributos;
	}
	
	public AtributoUsuario obtenerAtributo(String claveAtributo){
		AtributoUsuario atributo = null;
		if(claveAtributo != null){
			for(AtributoUsuario atributoTMP : listaAtributos){
				if(atributoTMP.getNombre().compareTo(claveAtributo) == 0){
					atributo = atributoTMP;
					break;
				}
			}
		}
		return atributo;
	}
	
	public boolean contieneAtributo(String claveAtributo){
		AtributoUsuario atributo = new AtributoUsuario();
		atributo.setNombre(claveAtributo);
		return listaAtributos.contains(atributo);
	}
	
	/**
	 * @return isOutsider attribute
	 */
	public boolean isOutsider() {
		return isOutsider;
	}
	public void setOutsider(boolean isOutsider) {
		this.isOutsider = isOutsider;
	}
	
	public Cabina getCabina() {
		return cabina;
	}
	
	public void setCabina(Cabina cabina) {
		this.cabina = cabina;
	}

	public String getPromoCode() {
		return promoCode;
	}
	
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}	
	
	public String getDeviceUuid() {
		return deviceUuid;
	}
	
	public void setDeviceUuid(String deviceUuid) {
		this.deviceUuid = deviceUuid;
	}
	public List<Menu> getMenus() {
		return menus;
	}
	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}
	public List<Pagina> getPages() {
		return pages;
	}
	public void setPages(List<Pagina> pages) {
		this.pages = pages;
	}
	
	
}
