/**
 * Clase Pagina de la aplicación MIDAS
 */
package mx.com.afirme.midas.sistema.seguridad;

/**
 * @author andres.avalos
 *
 */
public class Pagina implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String nombre;
	private String nombreAccionDo;
	private String descripcion;
	
	public Pagina() {
		
	}
		
	public Pagina(Integer id, String nombre, String nombreAccionDo, String descripcion) { 
		this.id = id;
		this.nombre = nombre;
		this.nombreAccionDo = nombreAccionDo;
		this.descripcion = descripcion;
	}
		
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the nombreAccionDo
	 */
	public String getNombreAccionDo() {
		return nombreAccionDo;
	}
	/**
	 * @param nombreAccionDo the nombreAccionDo to set
	 */
	public void setNombreAccionDo(String nombreAccionDo) {
		this.nombreAccionDo = nombreAccionDo;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
	
	
}
