package mx.com.afirme.midas.sistema.seguridad;

import java.io.Serializable;

public class AtributoUsuario implements Serializable {
	private static final long serialVersionUID = -1474088264424860445L;

	private String nombre;
	private Serializable valor;
	private boolean activo;
	
	public AtributoUsuario(){
		
	}
	
	public AtributoUsuario(String nombre, Serializable valor, boolean activo) {
		super();
		this.nombre = nombre;
		this.valor = valor;
		this.activo = activo;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Serializable getValor() {
		return valor;
	}
	public void setValor(Serializable valor) {
		this.valor = valor;
	}
	public boolean isActivo() {
		return activo;
	}
	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	
	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof AtributoUsuario) {
			AtributoUsuario atributo = (AtributoUsuario) object;
			equal = atributo.getNombre().equals(this.nombre);
		} // End of if
		return equal;
	}
	
	@Override
	public int hashCode(){
		return this.nombre.hashCode();
	}
}
