/**
 * Clase PaginaPermiso de la aplicación MIDAS
 */
package mx.com.afirme.midas.sistema.seguridad;

import java.util.ArrayList;
import java.util.List;

/**
 * @author andres.avalos
 *
 */
public class PaginaPermiso implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Pagina pagina;
	private List<Permiso> permisos;
	
	public PaginaPermiso() {
		
		this.permisos = new ArrayList<Permiso>();
	}
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the pagina
	 */
	public Pagina getPagina() {
		return pagina;
	}
	/**
	 * @param pagina the pagina to set
	 */
	public void setPagina(Pagina pagina) {
		this.pagina = pagina;
	}
	/**
	 * @return the permisos
	 */
	public List<Permiso> getPermisos() {
		return permisos;
	}
	/**
	 * @param permisos the permisos to set
	 */
	public void setPermisos(List<Permiso> permisos) {
		this.permisos = permisos;
	}
	
	
	
}
