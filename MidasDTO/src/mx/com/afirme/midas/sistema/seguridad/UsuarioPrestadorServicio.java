package mx.com.afirme.midas.sistema.seguridad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;

@Entity(name = "UsuarioPrestadorServicio")
@Table(name = "TOUSUARIOPRESTADORSERVICIO", schema = "MIDAS")
public class UsuarioPrestadorServicio  implements Entidad{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOUSUARIOPRESTADORSERVICIO_ID_GENERATOR")
	@SequenceGenerator(name="TOUSUARIOPRESTADORSERVICIO_ID_GENERATOR", schema="MIDAS", sequenceName="TOUSUARIOPRESTADORSERV_SEQ",allocationSize=1)
	@Column(name="ID")	
	private	Long id;
	
	@Column(name="CODIGO_USUARIO", nullable = false)
	private String codigoUsuario;
	
	@OneToOne
	@JoinColumn(name = "PRESTADORSERVICIO_ID", nullable = false)
	private PrestadorServicio prestadorServicio;
	
	public UsuarioPrestadorServicio(){
		super();
	}
	
	public UsuarioPrestadorServicio(String codigoUsuario,
			PrestadorServicio prestadorServicio) {
		this();
		this.codigoUsuario = codigoUsuario;
		this.prestadorServicio = prestadorServicio;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return codigoUsuario.concat("-").concat(prestadorServicio.getPersonaMidas().getNombre());
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigoUsuario() {
		return codigoUsuario;
	}

	public void setCodigoUsuario(String codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}

	public PrestadorServicio getPrestadorServicio() {
		return prestadorServicio;
	}

	public void setPrestadorServicio(PrestadorServicio prestadorServicio) {
		this.prestadorServicio = prestadorServicio;
	}

}
