package mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.exclusion;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * ExclusionRecargoVarioTipoPolizaId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class ExclusionRecargoVarioTipoPolizaId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 6137164827829410077L;
	private BigDecimal idtotipopoliza;
     private BigDecimal idtorecargovario;
     private BigDecimal idtocobertura;


    // Constructors

    /** default constructor */
    public ExclusionRecargoVarioTipoPolizaId() {
    }

    
    /** full constructor */
    public ExclusionRecargoVarioTipoPolizaId(BigDecimal idtotipopoliza, BigDecimal idtorecargovario, BigDecimal idtocobertura) {
        this.idtotipopoliza = idtotipopoliza;
        this.idtorecargovario = idtorecargovario;
        this.idtocobertura = idtocobertura;
    }

   
    // Property accessors

    @Column(name="IDTOTIPOPOLIZA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtotipopoliza() {
        return this.idtotipopoliza;
    }
    
    public void setIdtotipopoliza(BigDecimal idtotipopoliza) {
        this.idtotipopoliza = idtotipopoliza;
    }

    @Column(name="IDTORECARGOVARIO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtorecargovario() {
        return this.idtorecargovario;
    }
    
    public void setIdtorecargovario(BigDecimal idtorecargovario) {
        this.idtorecargovario = idtorecargovario;
    }

    @Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtocobertura() {
        return this.idtocobertura;
    }
    
    public void setIdtocobertura(BigDecimal idtocobertura) {
        this.idtocobertura = idtocobertura;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof ExclusionRecargoVarioTipoPolizaId) ) return false;
		 ExclusionRecargoVarioTipoPolizaId castOther = ( ExclusionRecargoVarioTipoPolizaId ) other; 
         
		 return ( (this.getIdtotipopoliza()==castOther.getIdtotipopoliza()) || ( this.getIdtotipopoliza()!=null && castOther.getIdtotipopoliza()!=null && this.getIdtotipopoliza().equals(castOther.getIdtotipopoliza()) ) )
 && ( (this.getIdtorecargovario()==castOther.getIdtorecargovario()) || ( this.getIdtorecargovario()!=null && castOther.getIdtorecargovario()!=null && this.getIdtorecargovario().equals(castOther.getIdtorecargovario()) ) )
 && ( (this.getIdtocobertura()==castOther.getIdtocobertura()) || ( this.getIdtocobertura()!=null && castOther.getIdtocobertura()!=null && this.getIdtocobertura().equals(castOther.getIdtocobertura()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdtotipopoliza() == null ? 0 : this.getIdtotipopoliza().hashCode() );
         result = 37 * result + ( getIdtorecargovario() == null ? 0 : this.getIdtorecargovario().hashCode() );
         result = 37 * result + ( getIdtocobertura() == null ? 0 : this.getIdtocobertura().hashCode() );
         return result;
   }   





}