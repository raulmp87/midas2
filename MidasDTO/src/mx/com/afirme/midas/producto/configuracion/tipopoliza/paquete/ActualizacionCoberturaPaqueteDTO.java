package mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.seccion.cobertura.CoberturaSeccionPaqueteDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;

public class ActualizacionCoberturaPaqueteDTO implements Serializable {
	
	public static final Short CLAVE_EXITOSO = (short)1;
	public static final Short CLAVE_COBERTURA_BASICA_CONTRATADA = (short)2;
	public static final Short CLAVE_COBERTURAS_AMPARADAS_DESCONTRATADAS = (short)3;
	public static final Short CLAVE_COBERTURAS_SUBLIMITE_DESCONTRATADAS = (short)4;
	public static final Short CLAVE_COBERTURAS_AMPARADAS_Y_SUBLIMITE_DESCONTRATADAS = (short)5;
	public static final Short CLAVE_SA_COBERTURAS_SUBLIMITE_MODIFICADA = (short)6;
	public static final Short CLAVE_NO_EXITOSO_REGISTRO_SA_COBERTURA_AMPARADA= (short)7;
	public static final Short CLAVE_NO_EXITOSO_REGISTRO_SA_COBERTURA_SUBLIMITE= (short)8;
	public static final Short CLAVE_NO_EXITOSO_DESCONTRATO_COBERTURA_OBLIGATORIA= (short)9;
	public static final Short CLAVE_NO_EXITOSO_DESCONTRATO_COBERTURA_OBLIGATORIA_PARCIAL= (short)10;
	public static final Short CLAVE_NO_EXITOSO_DESCONTRATO_COBERTURA_AMPARADA_OBLIGATORIA= (short)11;
	public static final Short CLAVE_NO_EXITOSO_DESCONTRATO_COBERTURA_SUBLIMITE_OBLIGATORIA= (short)12;
	public static final Short CLAVE_NO_EXITOSO_COBERTURA_BASICA_NO_CONTRATADA= (short)15;
	public static final Short CLAVE_NO_EXITOSO_REGISTRO_SA_COBERTURA_BASICA_RANGOS= (short)16;
	
	//Constantes usadas para liberar un paquete
	public static final Short LIBERAR_PAQUETE_NO_EXITOSO_COBERTURA_BASICA_SIN_SA= (short)13;
	
	//Constantes usadas para liberar un paquete
	public static final Short ELIMINAR_PAQUETE_NO_EXITOSO= (short)14;
	
	public static final String CLAVE_MENSAJE_EXITO = "30";
	public static final String CLAVE_MENSAJE_INFORMACION = "20";
	public static final String CLAVE_MENSAJE_ERROR = "10";
	
	private static final long serialVersionUID = 755006460055551688L;

	private CoberturaSeccionDTO coberturaSeccion;
	private PaquetePolizaDTO paquetePoliza;
	private CoberturaSeccionPaqueteDTO coberturaPaqueteDTO;
	
	private Boolean operacionExitosa;
	private Short claveResultado;
	private String claveMensaje;
	private String mensajeError;
	
	private List<CoberturaSeccionDTO> coberturasAmparadasDependientes;
	private List<CoberturaSeccionDTO> coberturasSubLimiteDependientes;
	private CoberturaSeccionPaqueteDTO coberturaSeccionBasica;
	
	private BigDecimal valorMinimoSumaAseguradaSublimite;
	private BigDecimal valorMaximoSumaAseguradaSublimite;
	
	private ActualizacionCoberturaPaqueteDTO operacionLimitante;
	
	
	public CoberturaSeccionDTO getCoberturaSeccion() {
		return coberturaSeccion;
	}
	public void setCoberturaSeccion(CoberturaSeccionDTO coberturaSeccion) {
		this.coberturaSeccion = coberturaSeccion;
	}
	public PaquetePolizaDTO getPaquetePoliza() {
		return paquetePoliza;
	}
	public void setPaquetePoliza(PaquetePolizaDTO paquetePoliza) {
		this.paquetePoliza = paquetePoliza;
	}
	public Short getClaveResultado() {
		return claveResultado;
	}
	public void setClaveResultado(Short claveResultado) {
		this.claveResultado = claveResultado;
		if(claveResultado != null){
			if(claveResultado.equals(CLAVE_EXITOSO)){
				this.claveMensaje = CLAVE_MENSAJE_EXITO;
			}
			else if(claveResultado.equals(CLAVE_COBERTURA_BASICA_CONTRATADA) || claveResultado.equals(CLAVE_SA_COBERTURAS_SUBLIMITE_MODIFICADA) ||
					claveResultado.equals(CLAVE_COBERTURAS_AMPARADAS_Y_SUBLIMITE_DESCONTRATADAS) || claveResultado.equals(CLAVE_COBERTURAS_SUBLIMITE_DESCONTRATADAS) ||
					claveResultado.equals(CLAVE_COBERTURAS_AMPARADAS_DESCONTRATADAS)){
				this.claveMensaje = CLAVE_MENSAJE_INFORMACION;
			}
			else{//cualqueirt otra clave se interpreta como error
				this.claveMensaje = CLAVE_MENSAJE_ERROR;
			}
		}
	}
	public List<CoberturaSeccionDTO> getCoberturasAmparadasDependientes() {
		return coberturasAmparadasDependientes;
	}
	public void setCoberturasAmparadasDependientes(
			List<CoberturaSeccionDTO> coberturasAmparadasDependientes) {
		this.coberturasAmparadasDependientes = coberturasAmparadasDependientes;
	}
	public List<CoberturaSeccionDTO> getCoberturasSubLimiteDependientes() {
		return coberturasSubLimiteDependientes;
	}
	public void setCoberturasSubLimiteDependientes(
			List<CoberturaSeccionDTO> coberturasSubLimiteDependientes) {
		this.coberturasSubLimiteDependientes = coberturasSubLimiteDependientes;
	}
	public CoberturaSeccionPaqueteDTO getCoberturaSeccionBasica() {
		return coberturaSeccionBasica;
	}
	public void setCoberturaSeccionBasica(CoberturaSeccionPaqueteDTO coberturaSeccionBasica) {
		this.coberturaSeccionBasica = coberturaSeccionBasica;
	}
	public CoberturaSeccionPaqueteDTO getCoberturaPaqueteDTO() {
		return coberturaPaqueteDTO;
	}
	public void setCoberturaPaqueteDTO(CoberturaSeccionPaqueteDTO coberturaPaqueteDTO) {
		this.coberturaPaqueteDTO = coberturaPaqueteDTO;
	}
	public BigDecimal getValorMaximoSumaAseguradaSublimite() {
		return valorMaximoSumaAseguradaSublimite;
	}
	public BigDecimal getValorMinimoSumaAseguradaSublimite() {
		return valorMinimoSumaAseguradaSublimite;
	}
	public void setValorMinimoSumaAseguradaSublimite(BigDecimal valorMinimoSumaAseguradaSublimite) {
		this.valorMinimoSumaAseguradaSublimite = valorMinimoSumaAseguradaSublimite;
	}
	public Boolean getOperacionExitosa() {
		return operacionExitosa;
	}
	public void setOperacionExitosa(Boolean operacionExitosa) {
		this.operacionExitosa = operacionExitosa;
	}
	public void setValorMaximoSumaAseguradaSublimite(BigDecimal valorMaximoSumaAseguradaSublimite) {
		this.valorMaximoSumaAseguradaSublimite = valorMaximoSumaAseguradaSublimite;
	}
	public ActualizacionCoberturaPaqueteDTO getOperacionLimitante() {
		return operacionLimitante;
	}
	public String getClaveMensaje() {
		return claveMensaje;
	}
	public void setClaveMensaje(String claveMensaje) {
		this.claveMensaje = claveMensaje;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	public void setOperacionLimitante(ActualizacionCoberturaPaqueteDTO operacionLimitante) {
		this.operacionLimitante = operacionLimitante;
	}
}
