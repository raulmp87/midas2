package mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.seccion.cobertura;
// default package

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.PaquetePolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;


/**
 * CoberturaSeccionPaqueteDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOCOBERTURASECCIONPAQUETE"
    ,schema="MIDAS"
, uniqueConstraints = @UniqueConstraint(columnNames={"IDTOPAQUETEPOLIZA", "IDTOCOBERTURA", "IDTOSECCION"})
)

public class CoberturaSeccionPaqueteDTO  implements java.io.Serializable {

	private static final long serialVersionUID = 3253266222426940858L;
	private BigDecimal idToCoberturaSeccionPaquete;
     private PaquetePolizaDTO paquetePolizaDTO;
     private CoberturaSeccionDTO coberturaSeccionDTO;
     private Short claveContrato;
     private BigDecimal valorSumaAsegurada;


    // Constructors

    /** default constructor */
    public CoberturaSeccionPaqueteDTO() {
    }

	/** minimal constructor */
    public CoberturaSeccionPaqueteDTO(BigDecimal idToCoberturaSeccionPaquete, PaquetePolizaDTO paquetePolizaDTO, Short claveContrato) {
        this.idToCoberturaSeccionPaquete = idToCoberturaSeccionPaquete;
        this.paquetePolizaDTO = paquetePolizaDTO;
        this.claveContrato = claveContrato;
    }
    
    /** full constructor */
    public CoberturaSeccionPaqueteDTO(BigDecimal idToCoberturaSeccionPaquete, PaquetePolizaDTO paquetePolizaDTO, Short claveContrato, BigDecimal valorSumaAsegurada) {
        this.idToCoberturaSeccionPaquete = idToCoberturaSeccionPaquete;
        this.paquetePolizaDTO = paquetePolizaDTO;
        this.claveContrato = claveContrato;
        this.valorSumaAsegurada = valorSumaAsegurada;
    }
    
    @Id
	@SequenceGenerator(name = "TOCOBERTURASE_IDTOCOBERTUR_SEQ", allocationSize = 1, sequenceName = "MIDAS.TOCOBERTURASE_IDTOCOBERTUR_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TOCOBERTURASE_IDTOCOBERTUR_SEQ")
	@Column(name = "IDTOCOBERTURASECCIONPAQ")
    public BigDecimal getIdToCoberturaSeccionPaquete() {
        return this.idToCoberturaSeccionPaquete;
    }
    
    public void setIdToCoberturaSeccionPaquete(BigDecimal idToCoberturaSeccionPaquete) {
        this.idToCoberturaSeccionPaquete = idToCoberturaSeccionPaquete;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="IDTOPAQUETEPOLIZA", nullable=false)
    public PaquetePolizaDTO getPaquetePolizaDTO() {
        return this.paquetePolizaDTO;
    }
    
    public void setPaquetePolizaDTO(PaquetePolizaDTO paquetePolizaDTO) {
        this.paquetePolizaDTO = paquetePolizaDTO;
    }
    
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumns( { 
    	@JoinColumn(name="IDTOSECCION", referencedColumnName="IDTOSECCION", nullable=false ), 
    	@JoinColumn(name="IDTOCOBERTURA", referencedColumnName="IDTOCOBERTURA", nullable=false) } )
    public CoberturaSeccionDTO getCoberturaSeccionDTO() {
		return coberturaSeccionDTO;
	}

	public void setCoberturaSeccionDTO(CoberturaSeccionDTO coberturaSeccionDTO) {
		this.coberturaSeccionDTO = coberturaSeccionDTO;
	}

	@Column(name="CLAVECONTRATO", nullable=false, precision=2, scale=0)

    public Short getClaveContrato() {
        return this.claveContrato;
    }
    
    public void setClaveContrato(Short claveContrato) {
        this.claveContrato = claveContrato;
    }
    
    @Column(name="VALORSUMAASEGURADA", precision=16)

    public BigDecimal getValorSumaAsegurada() {
        return this.valorSumaAsegurada;
    }
    
    public void setValorSumaAsegurada(BigDecimal valorSumaAsegurada) {
        this.valorSumaAsegurada = valorSumaAsegurada;
    }
   
    public boolean isCoberturaDefaultPaquete(){
    	boolean resultado = false;
    	
    	if(this.getPaquetePolizaDTO() != null && this.getPaquetePolizaDTO().getTipoPolizaDTO() != null &&
    			this.getPaquetePolizaDTO().getTipoPolizaDTO().getIdToPaqueteDefault() != null){
    		if(this.getIdToCoberturaSeccionPaquete() != null){
    			resultado = this.getPaquetePolizaDTO().getTipoPolizaDTO().getIdToPaqueteDefault().equals(getIdToCoberturaSeccionPaquete());
    		}
    	}
    	
    	return resultado;
    }
}