package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * RiesgoCoberturaId entity. @author José Luis Arellano
 */
@Embeddable
public class RiesgoCoberturaId implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idtoseccion;
	private BigDecimal idtocobertura;
	private BigDecimal idtoriesgo;

	// Constructors

	/** default constructor */
	public RiesgoCoberturaId() {
	}

	/** full constructor */
	public RiesgoCoberturaId(BigDecimal idtoseccion, BigDecimal idtocobertura, BigDecimal idtoriesgo) {
		this.idtoseccion = idtoseccion;
		this.idtocobertura = idtocobertura;
		this.idtoriesgo = idtoriesgo;
	}

	@Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdtoseccion() {
		return this.idtoseccion;
	}

	public void setIdtoseccion(BigDecimal idtoseccion) {
		this.idtoseccion = idtoseccion;
	}

	@Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdtocobertura() {
		return this.idtocobertura;
	}

	public void setIdtocobertura(BigDecimal idtocobertura) {
		this.idtocobertura = idtocobertura;
	}

	@Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdtoriesgo() {
		return this.idtoriesgo;
	}

	public void setIdtoriesgo(BigDecimal idtoriesgo) {
		this.idtoriesgo = idtoriesgo;
	}
}
