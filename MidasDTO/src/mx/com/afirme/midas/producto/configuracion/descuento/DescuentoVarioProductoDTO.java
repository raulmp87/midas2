package mx.com.afirme.midas.producto.configuracion.descuento;

// default package

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.descuento.DescuentoDTO;
import mx.com.afirme.midas.producto.ProductoDTO;

/**
 * DescuentoPorProductoDTO entity. @author Jorge.Cano
 */
@Entity(name = "DescuentoVarioProductoDTO")
@Table(name = "TODESCUENTOVARIOPRODUCTO", schema = "MIDAS")
public class DescuentoVarioProductoDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DescuentoVarioProductoId id;
	private ProductoDTO productoDTO;
	private DescuentoDTO descuentoDTO;
	private String claveoObligatoriedad;
	private String claveComercialTecnico;
	private String claveAplicaReaseguro;
	private Double valor;

	// Constructors

	/** default constructor */
	public DescuentoVarioProductoDTO() {
		if (this.productoDTO == null) {
			this.productoDTO = new ProductoDTO();
		}
		if (this.descuentoDTO == null) {
			this.descuentoDTO = new DescuentoDTO();
		}
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToProducto", column = @Column(name = "IDTOPRODUCTO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToDescuentoVario", column = @Column(name = "IDTODESCUENTOVARIO", nullable = false, precision = 22, scale = 0)) })
	public DescuentoVarioProductoId getId() {
		return this.id;
	}

	public void setId(DescuentoVarioProductoId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTOPRODUCTO", nullable = false, insertable = false, updatable = false)
	public ProductoDTO getProductoDTO() {
		return this.productoDTO;
	}

	public void setProductoDTO(ProductoDTO productoDTO) {
		this.productoDTO = productoDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTODESCUENTOVARIO", nullable = false, insertable = false, updatable = false)
	public DescuentoDTO getDescuentoDTO() {
		return this.descuentoDTO;
	}

	public void setDescuentoDTO(DescuentoDTO descuentoDTO) {
		this.descuentoDTO = descuentoDTO;
	}

	@Column(name = "CLAVEOBLIGATORIEDAD", nullable = false, precision = 4, scale = 0)
	public String getClaveObligatoriedad() {
		return this.claveoObligatoriedad;
	}

	public void setClaveObligatoriedad(String claveoObligatoriedad) {
		this.claveoObligatoriedad = claveoObligatoriedad;
	}

	@Column(name = "CLAVECOMERCIALTECNICO", nullable = false, precision = 4, scale = 0)
	public String getClaveComercialTecnico() {
		return this.claveComercialTecnico;
	}

	public void setClaveComercialTecnico(String claveComercialTecnico) {
		this.claveComercialTecnico = claveComercialTecnico;
	}

	@Column(name = "CLAVEAPLICAREASEGURO", nullable = false, precision = 4, scale = 0)
	public String getClaveAplicaReaseguro() {
		return this.claveAplicaReaseguro;
	}

	public void setClaveAplicaReaseguro(String claveAplicaReaseguro) {
		this.claveAplicaReaseguro = claveAplicaReaseguro;
	}

	@Column(name = "VALOR", nullable = false, precision = 16)
	public Double getValor() {
		return this.valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

}