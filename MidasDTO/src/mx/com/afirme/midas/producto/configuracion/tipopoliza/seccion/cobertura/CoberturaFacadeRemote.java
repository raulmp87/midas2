package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura;

// default package

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

/**
 * Remote interface for CoberturaDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface CoberturaFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved CoberturaDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            CoberturaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(CoberturaDTO entity);

	/**
	 * Delete a persistent CoberturaDTO entity.
	 * 
	 * @param entity
	 *            CoberturaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(CoberturaDTO entity);

	/**
	 * Persist a previously saved CoberturaDTO entity and return it or a copy of
	 * it to the sender. A copy of the CoberturaDTO entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            CoberturaDTO entity to update
	 * @return CoberturaDTO the persisted CoberturaDTO entity instance, may not
	 *         be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public CoberturaDTO update(CoberturaDTO entity);

	public CoberturaDTO findById(BigDecimal id);

	/**
	 * Find all CoberturaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the CoberturaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<CoberturaDTO> found by query
	 */
	public List<CoberturaDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all CoberturaDTO entities.
	 * 
	 * @return List<CoberturaDTO> all CoberturaDTO entities
	 */
	public List<CoberturaDTO> findAll();

	/**
	 * Busca todos las entidades ProductoDTO que cumplan con el filtro de
	 * busqueda
	 * 
	 * @return List<CoberturaDTO> all CoberturaDTO entities
	 */
	public List<CoberturaDTO> listarFiltrado(CoberturaDTO entity, Boolean mostrarInactivos);
	
	/**
	 * Borra l�gicamente un registro de Cobertura, estableciendo en 0 los campos 
	 * CLAVEACTIVO y CLAVEESTATUS
	 * 
	 * @param entity
	 *            CoberturaDTO entity el registro Cobertura a borrar
	 * @return CoberturaDTO the persisted CoberturaDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             si la operacion falla
	 */
	public CoberturaDTO borradoLogico(CoberturaDTO entity);
	
	/**
	 * Busca los registros de Cobertura que no hayan sido borrados logicamente
	 * 
	 * @return List<CoberturaDTO> registros de Cobertura que no han sido borrados logicamente
	 */
	public List<CoberturaDTO> listarVigentes();
	
	public List<CoberturaDTO> listarCoberturasPorExcluir(BigDecimal idToCobertura, BigDecimal idToSeccion);

	public List<CoberturaDTO> listarCoberturasNoRequeridas(BigDecimal idToCobertura, BigDecimal idToSeccion);
	
	public List<CoberturaDTO> listarCoberturasPorAsociarSeccion(BigDecimal idToSeccion);
	
	/**
	 * Busca los registros de Cobertura que no hayan sido borrados logicamente y 
	 * que est�n relacionados con las secciones activas del TipoP�liza cuyo ID se recibe 
	 * @param idToTipoPoliza
	 * @return List<CoberturaDTO> registros de Cobertura que no han sido borrados logicamente
	 */
	public List<CoberturaDTO> listarVigentesPorTipoPoliza(BigDecimal idToTipoPoliza);
}