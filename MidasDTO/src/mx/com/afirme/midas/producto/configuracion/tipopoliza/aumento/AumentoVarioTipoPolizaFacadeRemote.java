package mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento;
// default package

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for AumentoVarioTipoPolizaFacade.
 * @author MyEclipse Persistence Tools
 */


public interface AumentoVarioTipoPolizaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved AumentoVarioTipoPolizaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity AumentoVarioTipoPolizaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(AumentoVarioTipoPolizaDTO entity);
    /**
	 Delete a persistent AumentoVarioTipoPolizaDTO entity.
	  @param entity AumentoVarioTipoPolizaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(AumentoVarioTipoPolizaDTO entity);
   /**
	 Persist a previously saved AumentoVarioTipoPolizaDTO entity and return it or a copy of it to the sender. 
	 A copy of the AumentoVarioTipoPolizaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity AumentoVarioTipoPolizaDTO entity to update
	 @return AumentoVarioTipoPolizaDTO the persisted AumentoVarioTipoPolizaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public AumentoVarioTipoPolizaDTO update(AumentoVarioTipoPolizaDTO entity);
	public AumentoVarioTipoPolizaDTO findById( AumentoVarioTipoPolizaId id);
	 /**
	 * Find all AumentoVarioTipoPolizaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the AumentoVarioTipoPolizaDTO property to query
	  @param value the property value to match
	  	  @return List<AumentoVarioTipoPolizaDTO> found by query
	 */
	public List<AumentoVarioTipoPolizaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all AumentoVarioTipoPolizaDTO entities.
	  	  @return List<AumentoVarioTipoPolizaDTO> all AumentoVarioTipoPolizaDTO entities
	 */
	public List<AumentoVarioTipoPolizaDTO> findAll(
		);	
}