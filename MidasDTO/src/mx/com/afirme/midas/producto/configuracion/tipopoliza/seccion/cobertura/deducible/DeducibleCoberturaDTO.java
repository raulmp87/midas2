package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.deducible;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * DeducibleCoberturaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TODEDUCIBLECOBERTURA", schema = "MIDAS")
public class DeducibleCoberturaDTO implements java.io.Serializable, Entidad {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DeducibleCoberturaId id;
	private CoberturaDTO coberturaDTO;
	private Double valor;
	private Short claveDefault;

	// Constructors

	/** default constructor */
	public DeducibleCoberturaDTO() {
	}

	/** full constructor */
	public DeducibleCoberturaDTO(DeducibleCoberturaId id,
			CoberturaDTO coberturaDTO, Double valor, Short claveDefault) {
		this.id = id;
		this.coberturaDTO = coberturaDTO;
		this.valor = valor;
		this.claveDefault = claveDefault;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToCobertura", column = @Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroSecuencia", column = @Column(name = "NUMEROSECUENCIA", nullable = false, precision = 22, scale = 0)) })
	public DeducibleCoberturaId getId() {
		return this.id;
	}

	public void setId(DeducibleCoberturaId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTOCOBERTURA", nullable = false, insertable = false, updatable = false)
	public CoberturaDTO getCoberturaDTO() {
		return this.coberturaDTO;
	}

	public void setCoberturaDTO(CoberturaDTO coberturaDTO) {
		this.coberturaDTO = coberturaDTO;
	}

	@Column(name = "VALOR", nullable = false, precision = 16)
	public Double getValor() {
		return this.valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	@Column(name = "CLAVEDEFAULT", nullable = false, precision = 4, scale = 0)
	public Short getClaveDefault() {
		return this.claveDefault;
	}

	public void setClaveDefault(Short claveDefault) {
		this.claveDefault = claveDefault;
	}

	@SuppressWarnings("unchecked")
	@Override
	public DeducibleCoberturaId getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		return this.valor.toString();
	}

	@SuppressWarnings("unchecked")
	public DeducibleCoberturaId getBusinessKey() {
		return this.id;
	}

}