package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo;

// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;

/**
 * Remote interface for RiesgoDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface RiesgoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved RiesgoDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            RiesgoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(RiesgoDTO entity);

	/**
	 * Delete a persistent RiesgoDTO entity.
	 * 
	 * @param entity
	 *            RiesgoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(RiesgoDTO entity);

	/**
	 * Persist a previously saved RiesgoDTO entity and return it or a copy of it
	 * to the sender. A copy of the RiesgoDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            RiesgoDTO entity to update
	 * @return RiesgoDTO the persisted RiesgoDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public RiesgoDTO update(RiesgoDTO entity);

	public RiesgoDTO findById(BigDecimal id);

	/**
	 * Find all RiesgoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the RiesgoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<RiesgoDTO> found by query
	 */
	public List<RiesgoDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all RiesgoDTO entities.
	 * 
	 * @return List<RiesgoDTO> all RiesgoDTO entities
	 */
	public List<RiesgoDTO> findAll();
	
	/**
	 * Busca todos las entidades RiesgoDTO que cumplan con el filtro de
	 * busqueda
	 * 
	 * @return List<RiesgoDTO> all RiesgoDTO entities
	 */
	public List<RiesgoDTO> listarFiltrado(RiesgoDTO entity, Boolean mostrarInactivos);
	
	/**
	 * Borra l�gicamente un registro de Riesgo, estableciendo en 0 los campos 
	 * CLAVEACTIVO y CLAVEESTATUS
	 * 
	 * @param entity
	 *            RiesgoDTO entity el registro Riesgo a borrar
	 * @return RiesgoDTO the persisted RiesgoDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             si la operacion falla
	 */
	public RiesgoDTO borradoLogico(RiesgoDTO entity);
	
	/**
	 * Busca los registros de Riesgo que no hayan sido borrados logicamente
	 * 
	 * @return List<RiesgoDTO> registros de Riesgo que no han sido borrados logicamente
	 */
	public List<RiesgoDTO> listarVigentes();
	
	/**M�todo listarRiesgosPorAsociarCobertura.
	 * Busca los registros de Riesgo que no est�n asociados a la cobertura cuyo ID se recibe.
	 * 
	 * @param BigDecimal idToCobertura. El id de la cobertura
	 * @return List<RiesgoDTO> registros de Riesgo que est�n asociados a la cobertura.
	 */
	public List<RiesgoDTO> listarRiesgosPorAsociarCobertura(CoberturaDTO coberturaDTO,BigDecimal idToSeccion);
	
	/**M�todo listarRiesgosVigentesPorCoberturaSeccion.
	 * Busca los registros de Riesgo que est�n asociados a la coberturaSeccion cuyos ID se reciben.
	 * @param BigDecimal idToCobertura. El id de la cobertura
	 * @param BigDecimal idToSeccion
	 * @return List<RiesgoDTO> registros de Riesgo que est�n asociados a la cobertura.
	 */
	public List<RiesgoDTO> listarRiesgosVigentesPorCoberturaSeccion(BigDecimal idToCobertura,BigDecimal idToSeccion);

	public Long countRiesgoCobertura(BigDecimal idToRiesgo);

	public Long countRiesgoTarifa(BigDecimal idToRiesgo);
}