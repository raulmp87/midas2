package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.exclusion;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;

/**
 * CoberturaExcluidaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TRCOBERTURAEXCLUIDA", schema = "MIDAS")
public class CoberturaExcluidaDTO implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields

	private CoberturaExcluidaId id;
	private CoberturaDTO coberturaExcluidaDTO;
	private CoberturaDTO coberturaBaseDTO;

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToCobertura", column = @Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToCoberturaExcluida", column = @Column(name = "IDCOBERTURAEXCLUIDA", nullable = false, precision = 22, scale = 0)) })
	public CoberturaExcluidaId getId() {
		return this.id;
	}

	public void setId(CoberturaExcluidaId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDCOBERTURAEXCLUIDA", nullable = false, insertable = false, updatable = false)
	public CoberturaDTO getCoberturaExcluidaDTO() {
		return this.coberturaExcluidaDTO;
	}

	public void setCoberturaExcluidaDTO(CoberturaDTO coberturaExcluidaDTO) {
		this.coberturaExcluidaDTO = coberturaExcluidaDTO;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTOCOBERTURA", nullable = false, insertable = false, updatable = false)
	public CoberturaDTO getCoberturaBaseDTO() {
		return this.coberturaBaseDTO;
	}

	public void setCoberturaBaseDTO(CoberturaDTO coberturaBaseDTO) {
		this.coberturaBaseDTO = coberturaBaseDTO;
	}
}