package mx.com.afirme.midas.producto.configuracion.aumento.exclusion;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.aumentovario.AumentoVarioDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;


/**
 * ExclusionAumentoVarioProductoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TREXCAUMENTOVARPRODUCTO",schema="MIDAS")
public class ExclusionAumentoVarioProductoDTO  implements java.io.Serializable {


     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ExclusionAumentoVarioProductoId id;
    private AumentoVarioDTO aumentoVarioDTO;
    private TipoPolizaDTO tipoPolizaDTO;
    private ProductoDTO productoDTO;


    // Constructors

    /** default constructor */
    public ExclusionAumentoVarioProductoDTO() {
    }

    
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idtoproducto", column=@Column(name="IDTOPRODUCTO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idtoaumentovario", column=@Column(name="IDTOAUMENTOVARIO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idtotipopoliza", column=@Column(name="IDTOTIPOPOLIZA", nullable=false, precision=22, scale=0) ) } )

    public ExclusionAumentoVarioProductoId getId() {
        return this.id;
    }
    
    public void setId(ExclusionAumentoVarioProductoId id) {
        this.id = id;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTOAUMENTOVARIO", nullable=false, insertable=false, updatable=false)
    public AumentoVarioDTO getAumentoVarioDTO() {
		return aumentoVarioDTO;
	}


	public void setAumentoVarioDTO(AumentoVarioDTO aumentoVarioDTO) {
		this.aumentoVarioDTO = aumentoVarioDTO;
	}

	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTOTIPOPOLIZA", nullable=false, insertable=false, updatable=false)
    public TipoPolizaDTO getTipoPolizaDTO() {
		return tipoPolizaDTO;
	}


	public void setTipoPolizaDTO(TipoPolizaDTO tipoPolizaDTO) {
		this.tipoPolizaDTO = tipoPolizaDTO;
	}
   
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTOPRODUCTO", nullable=false, insertable=false, updatable=false)
    public ProductoDTO getProductoDTO() {
		return productoDTO;
	}


	public void setProductoDTO(ProductoDTO productoDTO) {
		this.productoDTO = productoDTO;
	}

	public boolean equals(Object o) {
		if (o instanceof ExclusionAumentoVarioProductoDTO) {
			ExclusionAumentoVarioProductoDTO temp = (ExclusionAumentoVarioProductoDTO) o;
			if (temp.getId().getIdtoproducto().intValue() == this.getId().getIdtoproducto().intValue()
					&& temp.getId().getIdtoaumentovario().intValue() == this.getId().getIdtoaumentovario().intValue()
					&& temp.getId().getIdtotipopoliza().intValue() == this.getId().getIdtotipopoliza().intValue()) {
				return true;
			}
		}
		return false;
	}

	public int hashCode() {
		int hash = 1;
	    hash = hash * 31 + this.getId().getIdtoproducto().hashCode();
	    hash = hash * 31 + this.getId().getIdtoaumentovario().hashCode();
	    hash = hash * 31 + this.getId().getIdtotipopoliza().hashCode();
	    return hash;
	}
	
}