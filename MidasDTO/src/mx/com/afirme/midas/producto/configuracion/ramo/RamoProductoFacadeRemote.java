package mx.com.afirme.midas.producto.configuracion.ramo;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

/**
 * Remote interface for RamoProductoDTOFacade.
 * @author Jos� Luis Arellano
 */

public interface RamoProductoFacadeRemote {
	/**
	 Perform an initial save of a previously unsaved RamoProductoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity RamoProductoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
   public void save(RamoProductoDTO entity);
   /**
	 Delete a persistent RamoProductoDTO entity.
	  @param entity RamoProductoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
   public void delete(RamoProductoDTO entity);
  /**
	 Persist a previously saved RamoProductoDTO entity and return it or a copy of it to the sender. 
	 A copy of the RamoProductoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity RamoProductoDTO entity to update
	 @return RamoProductoDTO the persisted RamoProductoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public RamoProductoDTO update(RamoProductoDTO entity);
	
	public RamoProductoDTO findById( RamoProductoId id);
	 /**
	 * Find all RamoProductoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the RamoProductoDTO property to query
	  @param value the property value to match
	  	  @return List<RamoProductoDTO> found by query
	 */
	public List<RamoProductoDTO> findByProperty(String propertyName, Object value);
	
	/**
	 * Find all RamoProductoDTO entities.
	  	  @return List<RamoProductoDTO> all RamoProductoDTO entities
	 */
	public List<RamoProductoDTO> findAll();
	
	/**
	 * Lista todos los ramos que no est�n asociados a un producto determinado 
	 * por el id que recibe el m�todo
	 * @param BigDecimal idToProducto el id del producto.
	 * @return List<RamoProductoDTO> registros RamoProductoDTO que no est�n asociados al producto.
	 */
	public List<RamoProductoDTO> obtenerRamosSinAsociar(BigDecimal idToProducto);
}
