package mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * RamoTipoPolizaDTOId entity. @author José Luis Arellano
 */
@Embeddable
public class RamoTipoPolizaId implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idtotipopoliza;
    private BigDecimal idtcramo;

    @Column(name="IDTOTIPOPOLIZA", nullable=false, precision=22, scale=0)
    public BigDecimal getIdtotipopoliza() {
        return this.idtotipopoliza;
    }
    
    public void setIdtotipopoliza(BigDecimal idtotipopoliza) {
        this.idtotipopoliza = idtotipopoliza;
    }

    @Column(name="IDTCRAMO", nullable=false, precision=22, scale=0)
    public BigDecimal getIdtcramo() {
        return this.idtcramo;
    }
    
    public void setIdtcramo(BigDecimal idtcramo) {
        this.idtcramo = idtcramo;
    }
}
