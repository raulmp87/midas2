package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.tipovehiculo;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class SeccionTipoVehiculoId implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToSeccion;
	private BigDecimal idTcTipoVehiculo;
	
	
	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}
	
	@Column(name="IDTOSECCION", nullable=false, precision=22, scale=0)
	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}
	
	public void setIdTcTipoVehiculo(BigDecimal idTcTipoVehiculo) {
		this.idTcTipoVehiculo = idTcTipoVehiculo;
	}
	
	@Column(name="IDTCTIPOVEHICULO", unique=true, nullable=false, precision=22, scale=0)
	public BigDecimal getIdTcTipoVehiculo() {
		return idTcTipoVehiculo;
	}
	
	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof SeccionTipoVehiculoId))
			return false;
		SeccionTipoVehiculoId castOther = (SeccionTipoVehiculoId) other;

		return ((this.getIdToSeccion() == castOther.getIdToSeccion()) || (this
				.getIdToSeccion() != null && castOther.getIdToSeccion() != null && this
				.getIdToSeccion().equals(castOther.getIdToSeccion())))
				&& ((this.getIdTcTipoVehiculo() == castOther
						.getIdTcTipoVehiculo()) || (this.getIdTcTipoVehiculo() != null
						&& castOther.getIdTcTipoVehiculo() != null && this
						.getIdTcTipoVehiculo().equals(
								castOther.getIdTcTipoVehiculo())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToSeccion() == null ? 0 : this.getIdToSeccion()
						.hashCode());
		result = 37
				* result
				+ (getIdTcTipoVehiculo() == null ? 0 : this
						.getIdTcTipoVehiculo().hashCode());
		return result;
	}

}
