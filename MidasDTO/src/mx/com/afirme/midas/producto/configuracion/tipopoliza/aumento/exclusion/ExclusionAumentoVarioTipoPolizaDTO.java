package mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento.exclusion;


import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.aumentovario.AumentoVarioDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;


/**
 * ExclusionAumentoVarioTipoPolizaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TREXCAUMENTOVARTIPOPOLIZA"
    ,schema="MIDAS"
)
public class ExclusionAumentoVarioTipoPolizaDTO  implements java.io.Serializable {


	private static final long serialVersionUID = 1L;
	private ExclusionAumentoVarioTipoPolizaId id;
    private AumentoVarioDTO aumentoVarioDTO;
    private TipoPolizaDTO  tipoPolizaDTO;
    private CoberturaDTO coberturaDTO;



    /** default constructor */
    public ExclusionAumentoVarioTipoPolizaDTO() {
    }

    
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idtotipopoliza", column=@Column(name="IDTOTIPOPOLIZA", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idtoaumentovario", column=@Column(name="IDTOAUMENTOVARIO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idtocobertura", column=@Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0) ) } )

    public ExclusionAumentoVarioTipoPolizaId getId() {
        return this.id;
    }
    
    public void setId(ExclusionAumentoVarioTipoPolizaId id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTOAUMENTOVARIO", nullable=false, insertable=false, updatable=false)

    public AumentoVarioDTO getAumentoVarioDTO() {
		return aumentoVarioDTO;
	}


	public void setAumentoVarioDTO(AumentoVarioDTO aumentoVarioDTO) {
		this.aumentoVarioDTO = aumentoVarioDTO;
	}


	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTOTIPOPOLIZA", nullable=false, insertable=false, updatable=false)
    public TipoPolizaDTO getTipoPolizaDTO() {
		return tipoPolizaDTO;
	}


	public void setTipoPolizaDTO(TipoPolizaDTO tipoPolizaDTO) {
		this.tipoPolizaDTO = tipoPolizaDTO;
	}	
  
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTOCOBERTURA", nullable=false, insertable=false, updatable=false)
	public CoberturaDTO getCoberturaDTO() {
		return coberturaDTO;
	}


	public void setCoberturaDTO(CoberturaDTO coberturaDTO) {
		this.coberturaDTO = coberturaDTO;
	}

	public boolean equals(Object o) {
		if (o instanceof ExclusionAumentoVarioTipoPolizaDTO) {
			ExclusionAumentoVarioTipoPolizaDTO temp = (ExclusionAumentoVarioTipoPolizaDTO) o;
			if (temp.getId().getIdtotipopoliza().intValue() == this.getId().getIdtotipopoliza().intValue()
					&& temp.getId().getIdtoaumentovario().intValue() == this.getId().getIdtoaumentovario().intValue()
					&& temp.getId().getIdtocobertura().intValue() == this.getId().getIdtocobertura().intValue()) {
				return true;
			}
		}
		return false;
	}

	public int hashCode() {
		int hash = 1;
	    hash = hash * 31 + this.getId().getIdtotipopoliza().hashCode();
	    hash = hash * 31 + this.getId().getIdtoaumentovario().hashCode();
	    hash = hash * 31 + this.getId().getIdtotipopoliza().hashCode();
	    return hash;
	}

}