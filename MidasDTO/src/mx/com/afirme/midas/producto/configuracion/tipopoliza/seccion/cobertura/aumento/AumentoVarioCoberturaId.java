package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * AumentoVarioCoberturaId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class AumentoVarioCoberturaId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idtocobertura;
     private BigDecimal idtoaumentovario;


    // Constructors

    /** default constructor */
    public AumentoVarioCoberturaId() {
    }

    
    /** full constructor */
    public AumentoVarioCoberturaId(BigDecimal idtocobertura, BigDecimal idtoaumentovario) {
        this.idtocobertura = idtocobertura;
        this.idtoaumentovario = idtoaumentovario;
    }

   
    // Property accessors

    @Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtocobertura() {
        return this.idtocobertura;
    }
    
    public void setIdtocobertura(BigDecimal idtocobertura) {
        this.idtocobertura = idtocobertura;
    }

    @Column(name="IDTOAUMENTOVARIO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtoaumentovario() {
        return this.idtoaumentovario;
    }
    
    public void setIdtoaumentovario(BigDecimal idtoaumentovario) {
        this.idtoaumentovario = idtoaumentovario;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof AumentoVarioCoberturaId) ) return false;
		 AumentoVarioCoberturaId castOther = ( AumentoVarioCoberturaId ) other; 
         
		 return ( (this.getIdtocobertura()==castOther.getIdtocobertura()) || ( this.getIdtocobertura()!=null && castOther.getIdtocobertura()!=null && this.getIdtocobertura().equals(castOther.getIdtocobertura()) ) )
 && ( (this.getIdtoaumentovario()==castOther.getIdtoaumentovario()) || ( this.getIdtoaumentovario()!=null && castOther.getIdtoaumentovario()!=null && this.getIdtoaumentovario().equals(castOther.getIdtoaumentovario()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdtocobertura() == null ? 0 : this.getIdtocobertura().hashCode() );
         result = 37 * result + ( getIdtoaumentovario() == null ? 0 : this.getIdtoaumentovario().hashCode() );
         return result;
   }   





}