package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.ramo;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * RamoSeccionDTOId entity. @author José Luis Arellano
 */
@Embeddable
public class RamoSeccionId implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idtoseccion;
    private BigDecimal idtcramo;

    @Column(name="IDTOSECCION", nullable=false, precision=22, scale=0)
    public BigDecimal getIdtoseccion() {
        return this.idtoseccion;
    }
    
    public void setIdtoseccion(BigDecimal idtoseccion) {
        this.idtoseccion = idtoseccion;
    }

    @Column(name="IDTCRAMO", nullable=false, precision=22, scale=0)
    public BigDecimal getIdtcramo() {
        return this.idtcramo;
    }
    
    public void setIdtcramo(BigDecimal idtcramo) {
        this.idtcramo = idtcramo;
    }
}
