package mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * RecargoVarioTipoPolizaId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class RecargoVarioTipoPolizaId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idtotipopoliza;
     private BigDecimal idtorecargovario;


    // Constructors

    /** default constructor */
    public RecargoVarioTipoPolizaId() {
    }

    
    /** full constructor */
    public RecargoVarioTipoPolizaId(BigDecimal idtotipopoliza, BigDecimal idtorecargovario) {
        this.idtotipopoliza = idtotipopoliza;
        this.idtorecargovario = idtorecargovario;
    }

   
    // Property accessors

    @Column(name="IDTOTIPOPOLIZA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtotipopoliza() {
        return this.idtotipopoliza;
    }
    
    public void setIdtotipopoliza(BigDecimal idtotipopoliza) {
        this.idtotipopoliza = idtotipopoliza;
    }

    @Column(name="IDTORECARGOVARIO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtorecargovario() {
        return this.idtorecargovario;
    }
    
    public void setIdtorecargovario(BigDecimal idtorecargovario) {
        this.idtorecargovario = idtorecargovario;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof RecargoVarioTipoPolizaId) ) return false;
		 RecargoVarioTipoPolizaId castOther = ( RecargoVarioTipoPolizaId ) other; 
         
		 return ( (this.getIdtotipopoliza()==castOther.getIdtotipopoliza()) || ( this.getIdtotipopoliza()!=null && castOther.getIdtotipopoliza()!=null && this.getIdtotipopoliza().equals(castOther.getIdtotipopoliza()) ) )
 && ( (this.getIdtorecargovario()==castOther.getIdtorecargovario()) || ( this.getIdtorecargovario()!=null && castOther.getIdtorecargovario()!=null && this.getIdtorecargovario().equals(castOther.getIdtorecargovario()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdtotipopoliza() == null ? 0 : this.getIdtotipopoliza().hashCode() );
         result = 37 * result + ( getIdtorecargovario() == null ? 0 : this.getIdtorecargovario().hashCode() );
         return result;
   }   





}