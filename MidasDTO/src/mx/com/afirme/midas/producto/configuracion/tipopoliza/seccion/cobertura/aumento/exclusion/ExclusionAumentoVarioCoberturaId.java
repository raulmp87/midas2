package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.exclusion;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * ExclusionAumentoVarioCoberturaId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class ExclusionAumentoVarioCoberturaId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idtocobertura;
     private BigDecimal idtoaumentovario;
     private BigDecimal idtoriesgo;


    // Constructors

    /** default constructor */
    public ExclusionAumentoVarioCoberturaId() {
    }

    
    /** full constructor */
    public ExclusionAumentoVarioCoberturaId(BigDecimal idtocobertura, BigDecimal idtoaumentovario, BigDecimal idtoriesgo) {
        this.idtocobertura = idtocobertura;
        this.idtoaumentovario = idtoaumentovario;
        this.idtoriesgo = idtoriesgo;
    }

   
    // Property accessors

    @Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtocobertura() {
        return this.idtocobertura;
    }
    
    public void setIdtocobertura(BigDecimal idtocobertura) {
        this.idtocobertura = idtocobertura;
    }

    @Column(name="IDTOAUMENTOVARIO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtoaumentovario() {
        return this.idtoaumentovario;
    }
    
    public void setIdtoaumentovario(BigDecimal idtoaumentovario) {
        this.idtoaumentovario = idtoaumentovario;
    }

    @Column(name="IDTORIESGO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtoriesgo() {
        return this.idtoriesgo;
    }
    
    public void setIdtoriesgo(BigDecimal idtoriesgo) {
        this.idtoriesgo = idtoriesgo;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof ExclusionAumentoVarioCoberturaId) ) return false;
		 ExclusionAumentoVarioCoberturaId castOther = ( ExclusionAumentoVarioCoberturaId ) other; 
         
		 return ( (this.getIdtocobertura()==castOther.getIdtocobertura()) || ( this.getIdtocobertura()!=null && castOther.getIdtocobertura()!=null && this.getIdtocobertura().equals(castOther.getIdtocobertura()) ) )
 && ( (this.getIdtoaumentovario()==castOther.getIdtoaumentovario()) || ( this.getIdtoaumentovario()!=null && castOther.getIdtoaumentovario()!=null && this.getIdtoaumentovario().equals(castOther.getIdtoaumentovario()) ) )
 && ( (this.getIdtoriesgo()==castOther.getIdtoriesgo()) || ( this.getIdtoriesgo()!=null && castOther.getIdtoriesgo()!=null && this.getIdtoriesgo().equals(castOther.getIdtoriesgo()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdtocobertura() == null ? 0 : this.getIdtocobertura().hashCode() );
         result = 37 * result + ( getIdtoaumentovario() == null ? 0 : this.getIdtoaumentovario().hashCode() );
         result = 37 * result + ( getIdtoriesgo() == null ? 0 : this.getIdtoriesgo().hashCode() );
         return result;
   }   





}