package mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento.exclusion;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

/**
 * Remote interface for ExclusionDescuentoVarioTipoPolizaFacade.
 * @author Jos� Luis Arellano
 */

public interface ExclusionDescuentoVarioTipoPolizaFacadeRemote {
	/**
	 Perform an initial save of a previously unsaved ExclusionDescuentoVarioTipoPolizaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ExclusionDescuentoVarioTipoPolizaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
   public void save(ExclusionDescuentoVarioTipoPolizaDTO entity);
   /**
	 Delete a persistent ExclusionDescuentoVarioTipoPolizaDTO entity.
	  @param entity ExclusionDescuentoVarioTipoPolizaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
   public void delete(ExclusionDescuentoVarioTipoPolizaDTO entity);
  /**
	 Persist a previously saved ExclusionDescuentoVarioTipoPolizaDTO entity and return it or a copy of it to the sender. 
	 A copy of the ExclusionDescuentoVarioTipoPolizaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ExclusionDescuentoVarioTipoPolizaDTO entity to update
	 @return ExclusionDescuentoVarioTipoPolizaDTO the persisted ExclusionDescuentoVarioTipoPolizaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ExclusionDescuentoVarioTipoPolizaDTO update(ExclusionDescuentoVarioTipoPolizaDTO entity);
	public ExclusionDescuentoVarioTipoPolizaDTO findById( ExclusionDescuentoVarioTipoPolizaId id);
	 /**
	 * Find all ExclusionDescuentoVarioTipoPolizaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ExclusionDescuentoVarioTipoPolizaDTO property to query
	  @param value the property value to match
	  	  @return List<ExclusionDescuentoVarioTipoPolizaDTO> found by query
	 */
	public List<ExclusionDescuentoVarioTipoPolizaDTO> findByProperty(String propertyName, Object value);
	/**
	 * Find all ExclusionDescuentoVarioTipoPolizaDTO entities.
	  	  @return List<ExclusionDescuentoVarioTipoPolizaDTO> all ExclusionDescuentoVarioTipoPolizaDTO entities
	 */
	public List<ExclusionDescuentoVarioTipoPolizaDTO> findAll();
	
	/**
	 * Find a ExclusionDescuentoVarioTipoPolizaDTO entity with the specific
	 * received id�s.
	 * 
	 * @param BigDecimal idToTipoPoliza.
	 * @param BigDecimal idToDescuentoVario.
	 * @param BigDecimal idToCobertura.
	 * 
	 * @return List<ExclusionRecargoVarioTipoPolizaDTO> found by query
	 */
	public List<ExclusionDescuentoVarioTipoPolizaDTO> findByIDs(BigDecimal idToTipoPoliza, BigDecimal idToDescuentoVario, BigDecimal idToCobertura);
	
	/**
	 * Encuentra los registros de ExclusionDescuentoVarioTipoPolizaDTO relacionados con el TipoPolizaDTO cuyo ID se recibe y que adem�s est�n 
	 * relacionados s�lo con las coberturas que no hayan sido borradas l�gicamente.
	  @param BigDecimal idToTipoPoliza. El ID del tipoPoliza
	  @return List<ExclusionDescuentoVarioTipoPolizaDTO> encontrados por el query formado.
	 */
    public List<ExclusionDescuentoVarioTipoPolizaDTO> getVigentesPorIdTipoPoliza(BigDecimal idToTipoPoliza);
}
