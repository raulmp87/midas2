package mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento.exclusion;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * ExclusionDescuentoVarioTipoPolizaId entity. @author José Luis Arellano
 */
@Embeddable
public class ExclusionDescuentoVarioTipoPolizaId implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private BigDecimal idtotipopoliza;
     private BigDecimal idtodescuentovario;
     private BigDecimal idtocobertura;
    
     
     public ExclusionDescuentoVarioTipoPolizaId() {
     }
     
    public ExclusionDescuentoVarioTipoPolizaId(BigDecimal idtotipopoliza, BigDecimal idtodescuentovario, BigDecimal idtocobertura) {
        this.idtotipopoliza = idtotipopoliza;
        this.idtodescuentovario = idtodescuentovario;
        this.idtocobertura = idtocobertura;
    }

    @Column(name="IDTOTIPOPOLIZA", nullable=false, precision=22, scale=0)
    public BigDecimal getIdtotipopoliza() {
        return this.idtotipopoliza;
    }
    
    public void setIdtotipopoliza(BigDecimal idtotipopoliza) {
        this.idtotipopoliza = idtotipopoliza;
    }

    @Column(name="IDTODESCUENTOVARIO", nullable=false, precision=22, scale=0)
    public BigDecimal getIdtodescuentovario() {
        return this.idtodescuentovario;
    }
    
    public void setIdtodescuentovario(BigDecimal idtodescuentovario) {
        this.idtodescuentovario = idtodescuentovario;
    }

    @Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0)
    public BigDecimal getIdtocobertura() {
        return this.idtocobertura;
    }
    
    public void setIdtocobertura(BigDecimal idtocobertura) {
        this.idtocobertura = idtocobertura;
    }

}