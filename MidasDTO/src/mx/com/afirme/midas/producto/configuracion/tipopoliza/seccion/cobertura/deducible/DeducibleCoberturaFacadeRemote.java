package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.deducible;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for DeducibleCoberturaFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface DeducibleCoberturaFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved DeducibleCoberturaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            DeducibleCoberturaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(DeducibleCoberturaDTO entity);

	/**
	 * Delete a persistent DeducibleCoberturaDTO entity.
	 * 
	 * @param entity
	 *            DeducibleCoberturaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(DeducibleCoberturaDTO entity);

	/**
	 * Persist a previously saved DeducibleCoberturaDTO entity and return it or
	 * a copy of it to the sender. A copy of the DeducibleCoberturaDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            DeducibleCoberturaDTO entity to update
	 * @return DeducibleCoberturaDTO the persisted DeducibleCoberturaDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public DeducibleCoberturaDTO update(DeducibleCoberturaDTO entity);

	public DeducibleCoberturaDTO findById(DeducibleCoberturaId id);

	/**
	 * Find all DeducibleCoberturaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the DeducibleCoberturaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<DeducibleCoberturaDTO> found by query
	 */
	public List<DeducibleCoberturaDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all DeducibleCoberturaDTO entities.
	 * 
	 * @return List<DeducibleCoberturaDTO> all DeducibleCoberturaDTO entities
	 */
	public List<DeducibleCoberturaDTO> findAll();

	public List<DeducibleCoberturaDTO> listarDeducibles(BigDecimal idToCobertura);

	public List<DeducibleCoberturaDTO> listarDeduciblesPorCobertura(BigDecimal idToCobertura);
	
	public BigDecimal nextNumeroSecuencia(BigDecimal idToCobertura);
}