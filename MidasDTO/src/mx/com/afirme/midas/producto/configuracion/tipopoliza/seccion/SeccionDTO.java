package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.ramo.RamoSeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.seccionrequerida.SeccionRequeridaDTO;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgCondiciones;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.domain.subordinados.DerechoEndosoSeccion;
import mx.com.afirme.midas2.domain.subordinados.DerechoPolizaSeccion;

/**
 * SeccionDTO entity. @author Jorge Cano
 */
@Entity(name = "SeccionDTO")
@Table(name = "TOSECCION", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = {
		"CODIGOSECCION", "VERSIONSECCION" }))
public class SeccionDTO implements java.io.Serializable, Entidad {

	// Fields
	private static final long serialVersionUID = 1L;
	private BigDecimal idToSeccion;
	private TipoPolizaDTO tipoPolizaDTO;
	private String codigo;
	private Integer version;
	private String descripcion;
	private String nombreComercial;
	private String claveImpresionEncabezado;
	private String claveBienSeccion;
	private String claveImpresionSumaAsegurada;
	private String claveImpresionPrimaNeta;
	private BigDecimal numeroSecuencia;
	private String claveObligatoriedad;
	/*private String claveActivoConfiguracion;
	private String claveActivoProduccion;*/
	private Short claveSubIncisos;
	private Short claveEstatus;
	private Short claveActivo;
	private Date fechaCreacion;
	private Date fechaModificacion;
    private String codigoUsuarioCreacion;
	private String codigoUsuarioModificacion;
	private List<CoberturaSeccionDTO> coberturas;
	private List<RamoSeccionDTO> ramoSeccionDTOList;
	private List<SeccionRequeridaDTO> seccionesRequeridas;
	private String claveLuc;
	private String clavePrimerRiesgo;
	private String claveDependenciaOtrasPr;
	private Short claveEdificioContenido;
	private Date fechaInicioVigencia;
	private Integer diasGracia;
	private Integer diasGraciaSubsecuentes;
	private Integer diasRetroactividad;
	private Integer diasDiferimiento;
	private BigDecimal porcentajeMaximoBonificacion;
	private BigDecimal porcentajeMaximoComision;
	private BigDecimal porcentajeMaximoSobrecomision;
	private TipoVehiculoDTO tipoVehiculo;
	private List<DerechoPolizaSeccion> derechosPoliza = new ArrayList<DerechoPolizaSeccion>();
	private List<DerechoEndosoSeccion> derechosEndoso = new ArrayList<DerechoEndosoSeccion>();
	private List<CgCondiciones> cgCondiciones = new ArrayList<CgCondiciones>();
	private List<Paquete> paquetes;
	private short servicioPublico; 
	private Short tipoValidacionNumSerie = 1;
	
	//AHERACA 05102016
	private String claveTipoBien;
	
	public static final Short TIPO_VALIDACION_CESVI = 1;
	public static final Short TIPO_VALIDACION_KBB = 2;

	public static final BigDecimal LINEA_NEGOCIO_AUT_INDIVIDUALES = new BigDecimal(1601);
	public static final BigDecimal LINEA_NEGOCIO_CAM_INDIVIDUALES = new BigDecimal(1602);
	
	public static final BigDecimal LINEA_NEGOCIO_AUTOPLAZO_AUTOS = new BigDecimal(1609);
	public static final BigDecimal LINEA_NEGOCIO_AUTOPLAZO_CAMIONES = new BigDecimal(1610);
	
	public static final BigDecimal LINEA_NEG_AUT_SERVICIO_PUBLICO = new BigDecimal(1611);
	public static final BigDecimal LINEA_NEG_BUS_SERVICIO_PUBLICO = new BigDecimal(1612);
	public static final BigDecimal LINEA_NEG_AUT_SERVICIO_PUBLICO_MTY = new BigDecimal(1623);
	
	
	private static final String[] CODIGOS_SECCION_SERVICIO_PUBLICO = {"SA1238", "SA1252", "SA1269"};

	private Date fechaRegistro;
    private String numeroRegistro;
	
	// Constructors

	/** default constructor */
	public SeccionDTO() {
		if (this.tipoPolizaDTO == null){
			this.tipoPolizaDTO = new TipoPolizaDTO();
		}
		this.claveEstatus=new Short("1");
		this.claveActivo= new Short("1");
		this.fechaCreacion=new Date();
		this.fechaModificacion=new Date();
		this.codigoUsuarioCreacion="enDB";
		this.codigoUsuarioModificacion="enDB";
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTOSECCION_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOSECCION_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOSECCION_SEQ_GENERADOR")	
	@Column(name = "IDTOSECCION", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSeccion() {
		return this.idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOTIPOPOLIZA", nullable = false)
	public TipoPolizaDTO getTipoPolizaDTO() {
		return this.tipoPolizaDTO;
	}

	public void setTipoPolizaDTO(TipoPolizaDTO tipoPolizaDTO) {
		this.tipoPolizaDTO = tipoPolizaDTO;
	}

	@Column(name = "CODIGOSECCION", nullable = false, length = 8)
	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Column(name = "VERSIONSECCION", nullable = false)
	public Integer getVersion() {
		return this.version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	@Column(name = "DESCRIPCIONSECCION", nullable = false, length = 200)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "NOMBRECOMERCIALSECCION", nullable = false, length = 100)
	public String getNombreComercial() {
		return this.nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}

	@Column(name = "CLAVEIMPRESIONENCABEZADO", nullable = false, precision = 4, scale = 0)
	public String getClaveImpresionEncabezado() {
		return this.claveImpresionEncabezado;
	}

	public void setClaveImpresionEncabezado(String claveImpresionEncabezado) {
		this.claveImpresionEncabezado = claveImpresionEncabezado;
	}

	@Column(name = "CLAVEBIENSECCION", nullable = false, precision = 4, scale = 0)
	public String getClaveBienSeccion() {
		return this.claveBienSeccion;
	}

	public void setClaveBienSeccion(String claveBienSeccion) {
		this.claveBienSeccion = claveBienSeccion;
	}

	@Column(name = "CLAVEIMPRESIONSUMAASEGURADA", nullable = false, precision = 4, scale = 0)
	public String getClaveImpresionSumaAsegurada() {
		return this.claveImpresionSumaAsegurada;
	}

	public void setClaveImpresionSumaAsegurada(String claveImpresionSumaAsegurada) {
		this.claveImpresionSumaAsegurada = claveImpresionSumaAsegurada;
	}

	@Column(name = "CLAVEIMPRESIONPRIMANETA", nullable = false, precision = 4, scale = 0)
	public String getClaveImpresionPrimaNeta() {
		return this.claveImpresionPrimaNeta;
	}

	public void setClaveImpresionPrimaNeta(String claveImpresionPrimaNeta) {
		this.claveImpresionPrimaNeta = claveImpresionPrimaNeta;
	}

	@Column(name = "NUMEROSECUENCIA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getNumeroSecuencia() {
		return this.numeroSecuencia;
	}

	public void setNumeroSecuencia(BigDecimal numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}

	@Column(name = "CLAVEOBLIGATORIEDAD", nullable = false, precision = 4, scale = 0)
	public String getClaveObligatoriedad() {
		return this.claveObligatoriedad;
	}

	public void setClaveObligatoriedad(String claveObligatoriedad) {
		this.claveObligatoriedad = claveObligatoriedad;
	}

	/*@Column(name = "CLAVEACTIVOCONFIGURACION", nullable = false, precision = 4, scale = 0)
	public String getClaveActivoConfiguracion() {
		return this.claveActivoConfiguracion;
	}

	public void setClaveActivoConfiguracion(String claveActivoConfiguracion) {
		this.claveActivoConfiguracion = claveActivoConfiguracion;
	}*/

	/*@Column(name = "CLAVEACTIVOPRODUCCION", nullable = false, precision = 4, scale = 0)
	public String getClaveActivoProduccion() {
		return this.claveActivoProduccion;
	}

	public void setClaveActivoProduccion(String claveActivoProduccion) {
		this.claveActivoProduccion = claveActivoProduccion;
	}*/
	
	@Column(name = "CLAVESUBINCISOS ")
	public Short getClaveSubIncisos() {
		return claveSubIncisos;
	}

	public void setClaveSubIncisos(Short claveSubIncisos) {
		this.claveSubIncisos = claveSubIncisos;
	}
	
	@Column(name = "CLAVEESTATUS", nullable = false, precision = 4, scale = 0)
	public Short getClaveEstatus() {
		return claveEstatus;
	}

	public void setClaveEstatus(Short claveEstatus) {
		this.claveEstatus = claveEstatus;
	}
	
	@Column(name = "CLAVEACTIVO", nullable = false, precision = 4, scale = 0)
	public Short getClaveActivo() {
		return claveActivo;
	}
	
	@Temporal(TemporalType.DATE)
    @Column(name="FECHACREACION", length=7, nullable=false )
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Temporal(TemporalType.DATE)
    @Column(name="FECHAMODIFICACION", length=7, nullable=false)
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Column(name="CODIGOUSUARIOCREACION", nullable=false, length=8)
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	
	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
	
	@Column(name="CODIGOUSUARIOMODIFICACION", length=8, nullable=false)
	public String getCodigoUsuarioModificacion() {
		return codigoUsuarioModificacion;
	}	
	
	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}

	public void setClaveActivo(Short claveActivo) {
		this.claveActivo = claveActivo;
	}	

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "seccionDTO")
	public List<CoberturaSeccionDTO> getCoberturas() {
		return coberturas;
	}

	public void setCoberturas(List<CoberturaSeccionDTO> coberturas) {
		this.coberturas = coberturas;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "seccionDTO")
	public List<RamoSeccionDTO> getRamoSeccionDTOList() {
		return ramoSeccionDTOList;
	}

	public void setRamoSeccionDTOList(List<RamoSeccionDTO> ramoSeccionDTOList) {
		this.ramoSeccionDTOList = ramoSeccionDTOList;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "seccionBaseDTO")
	public List<SeccionRequeridaDTO> getSeccionesRequeridas() {
		return seccionesRequeridas;
	}

	public void setSeccionesRequeridas(List<SeccionRequeridaDTO> seccionesRequeridas) {
		this.seccionesRequeridas = seccionesRequeridas;
	}
	
	@Column(name = "CLAVELUC")
	public String getClaveLuc() {
		return this.claveLuc;
	}

	public void setClaveLuc(String claveLuc) {
		this.claveLuc = claveLuc;
	}

	@Column(name = "CLAVEPRIMERRIESGO")
	public String getClavePrimerRiesgo() {
		return this.clavePrimerRiesgo;
	}

	public void setClavePrimerRiesgo(String clavePrimerRiesgo) {
		this.clavePrimerRiesgo = clavePrimerRiesgo;
	}
	
	@Column(name = "CLAVEDEPENDENCIAOTRASPR")
	public String getClaveDependenciaOtrasPr() {
		return this.claveDependenciaOtrasPr;
	}

	public void setClaveDependenciaOtrasPr(String claveDependenciaOtrasPr) {
		this.claveDependenciaOtrasPr = claveDependenciaOtrasPr;
	}

	@Column(name = "CLAVEEDIFICIOCONTENIDO", nullable = true, precision = 4, scale = 0)
	public Short getClaveEdificioContenido() {
		return claveEdificioContenido;
	}

	public void setClaveEdificioContenido(Short claveEdificioContenido) {
		this.claveEdificioContenido = claveEdificioContenido;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAINICIOVIGENCIA", nullable = false, length = 7)
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	@Column(name = "DIASGRACIA")
	public Integer getDiasGracia() {
		return diasGracia;
	}

	public void setDiasGracia(Integer diasGracia) {
		this.diasGracia = diasGracia;
	}

	@Column(name = "DIASGRACIASUBSECUENTES")
	public Integer getDiasGraciaSubsecuentes() {
		return diasGraciaSubsecuentes;
	}

	public void setDiasGraciaSubsecuentes(Integer diasGraciaSubsecuentes) {
		this.diasGraciaSubsecuentes = diasGraciaSubsecuentes;
	}

	@Column(name = "DIASRETROACTIVIDAD")
	public Integer getDiasRetroactividad() {
		return diasRetroactividad;
	}

	public void setDiasRetroactividad(Integer diasRetroactividad) {
		this.diasRetroactividad = diasRetroactividad;
	}

	@Column(name = "DIASDIFERIMIENTO")
	public Integer getDiasDiferimiento() {
		return diasDiferimiento;
	}

	public void setDiasDiferimiento(Integer diasDiferimiento) {
		this.diasDiferimiento = diasDiferimiento;
	}

	@Column(name = "PCTMAXIMOBONIFICACION", nullable = false, precision = 8, scale = 4)
	public BigDecimal getPorcentajeMaximoBonificacion() {
		return porcentajeMaximoBonificacion;
	}

	public void setPorcentajeMaximoBonificacion(
			BigDecimal porcentajeMaximoBonificacion) {
		this.porcentajeMaximoBonificacion = porcentajeMaximoBonificacion;
	}

	@Column(name = "PCTMAXIMOCOMISION", nullable = false, precision = 8, scale = 4)
	public BigDecimal getPorcentajeMaximoComision() {
		return porcentajeMaximoComision;
	}

	public void setPorcentajeMaximoComision(BigDecimal porcentajeMaximoComision) {
		this.porcentajeMaximoComision = porcentajeMaximoComision;
	}

	@Column(name = "PCTMAXIMOSOBRECOMISION", nullable = false, precision = 8, scale = 4)
	public BigDecimal getPorcentajeMaximoSobrecomision() {
		return porcentajeMaximoSobrecomision;
	}

	public void setPorcentajeMaximoSobrecomision(
			BigDecimal porcentajeMaximoSobrecomision) {
		this.porcentajeMaximoSobrecomision = porcentajeMaximoSobrecomision;
	}

	@ManyToOne(fetch = FetchType.EAGER)	
	@JoinColumn(name = "IDTCTIPOVEHICULO", referencedColumnName = "IDTCTIPOVEHICULO", nullable = false, insertable = true, updatable = true)			
	public TipoVehiculoDTO getTipoVehiculo() {
		return tipoVehiculo;
	}

	public void setTipoVehiculo(TipoVehiculoDTO tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "seccion", orphanRemoval=true)
	public List<DerechoPolizaSeccion> getDerechosPoliza() {
		return derechosPoliza;
	}

	public void setDerechosPoliza(List<DerechoPolizaSeccion> derechosPoliza) {
		this.derechosPoliza = derechosPoliza;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "seccion", orphanRemoval=true)
	public List<DerechoEndosoSeccion> getDerechosEndoso() {
		return derechosEndoso;
	}

	public void setDerechosEndoso(List<DerechoEndosoSeccion> derechosEndoso) {
		this.derechosEndoso = derechosEndoso;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "seccionDTO")
	public List<CgCondiciones> getCgCondiciones() {
		return this.cgCondiciones;
	}

	public void setCgCondiciones(List<CgCondiciones> cgCondiciones) {
		this.cgCondiciones = cgCondiciones;
	}

	@ManyToMany(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name = "TRPAQUETESECCION", schema = "MIDAS",
			joinColumns = {
				@JoinColumn(name="IDTOSECCION")
			},
			inverseJoinColumns = {
				@JoinColumn(name="IDPAQUETE")
			}
	)
	public List<Paquete> getPaquetes() {
		return paquetes;
	}

	public void setPaquetes(List<Paquete> paquetes) {
		this.paquetes = paquetes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SeccionDTO other = (SeccionDTO) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return idToSeccion;
	}

	@Override
	public String getValue() {
		//  Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getBusinessKey() {
		return idToSeccion;
	}

	public String toString(){
	  StringBuilder sb = new StringBuilder();
	  sb.append("[");
	  sb.append("idToSeccion: " + this.idToSeccion + ", ");
	  sb.append("descripcion: " + this.descripcion + ", ");
	  sb.append("]");
	  
	  return sb.toString();
	}
	
	/**
	 * Indica si es una sección de servicio público.
	 * @return
	 */
	@Transient
	public boolean isServicioPublico() {
		if (codigo == null) {
			return false;
		}
		
		for (String codigoSeccionServicioPublico: CODIGOS_SECCION_SERVICIO_PUBLICO) {
			if (codigo.equals(codigoSeccionServicioPublico)) {
				return true;
			}
		}
		
		return false;
	}

	@Column(name = "SERVICIOPUBLICO", nullable = false)
	public short getServicioPublico() {
		return servicioPublico;
	}

	public void setServicioPublico(short servicioPublico) {
		this.servicioPublico = servicioPublico;
	}

	/**
	 * @return el fechaRegistro
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAREGISTRO")
	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	/**
	 * @param fechaRegistro el fechaRegistro a establecer
	 */
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	/**
	 * @return el numeroRegistro
	 */
	@Column(name = "NUMEROREGISTRO")
	public String getNumeroRegistro() {
		return numeroRegistro;
	}

	/**
	 * @param numeroRegistro el numeroRegistro a establecer
	 */
	public void setNumeroRegistro(String numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
	}

	@Column(name = "TIPOVALIDACIONNUMSERIE")
	public Short getTipoValidacionNumSerie() {
		return tipoValidacionNumSerie;
	}

	public void setTipoValidacionNumSerie(Short tipoValidacionNumSerie) {
		this.tipoValidacionNumSerie = tipoValidacionNumSerie;
	}

	@Column(name = "CLAVETIPOBIEN")
	public String getClaveTipoBien() {
		return claveTipoBien;
	}

	public void setClaveTipoBien(String claveTipoBien) {
		this.claveTipoBien = claveTipoBien;
	}
}