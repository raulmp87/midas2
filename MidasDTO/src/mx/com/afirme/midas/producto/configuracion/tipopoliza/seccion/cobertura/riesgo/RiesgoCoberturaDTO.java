package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.producto.configuracion.aumento.AumentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.descuento.DescuentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.recargo.RecargoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento.AumentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento.DescuentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.RecargoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.AumentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.DescuentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.RecargoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.coaseguro.CoaseguroRiesgoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.deducible.DeducibleRiesgoCoberturaDTO;

/**
 * RiesgoCoberturaDTO entity. @author José Luis Arellano
 */
@Entity
@Table(name = "TORIESGOCOBERTURA", schema = "MIDAS")
public class RiesgoCoberturaDTO implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	private RiesgoCoberturaId id;
	private RiesgoDTO riesgoDTO;
	private CoberturaSeccionDTO coberturaSeccionDTO;
	private String descripcionRiesgoCobertura;
	private Short claveObligatoriedad;
	private Short claveTipoCoaseguro;
	private Double valorMinimoCoaseguro;
	private Double valorMaximoCoaseguro;
	private Short claveTipoLimiteCoaseguro;
	private Double valorMinimoLimiteCoaseguro;
	private Double valorMaximoLimiteCoaseguro;
	private Short claveTipoDeducible;
	private Double valorMinimoDeducible;
	private Double valorMaximoDeducible;
	private Short claveTipoLimiteDeducible;
	private Double valorMinimoLimiteDeducible;
	private Double valorMaximoLimiteDeducible;
	private Short claveTipoSumaAsegurada;
	private Short claveAutorizacion;
	private Short claveImporteCero;
	private List<CoaseguroRiesgoCoberturaDTO> coaseguros = new ArrayList<CoaseguroRiesgoCoberturaDTO>();
	private List<DeducibleRiesgoCoberturaDTO> deducibles = new ArrayList<DeducibleRiesgoCoberturaDTO>();
	
	private List<AumentoVarioCoberturaDTO> aumentosCobertura;
	private List<DescuentoVarioCoberturaDTO> descuentosCobertura;
	private List<RecargoVarioCoberturaDTO> recargosCobertura;
	private List<AumentoVarioTipoPolizaDTO> aumentosTipoPoliza;
	private List<DescuentoVarioTipoPolizaDTO> descuentosTipoPoliza;
	private List<RecargoVarioTipoPolizaDTO> recargosTipoPoliza;
	private List<AumentoVarioProductoDTO> aumentosProducto;
	private List<DescuentoVarioProductoDTO> descuentosProducto;
	private List<RecargoVarioProductoDTO> recargosProducto;
	
	@EmbeddedId
	@AttributeOverrides( {
		@AttributeOverride(name = "idtoseccion", column = @Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)),
		@AttributeOverride(name = "idtocobertura", column = @Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)),
		@AttributeOverride(name = "idtoriesgo", column = @Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)) })
	public RiesgoCoberturaId getId() {
		return this.id;
	}

	public void setId(RiesgoCoberturaId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumns( {
		@JoinColumn(name = "IDTOSECCION", referencedColumnName = "IDTOSECCION", nullable = false, insertable = false, updatable = false),
		@JoinColumn(name = "IDTOCOBERTURA", referencedColumnName = "IDTOCOBERTURA", nullable = false, insertable = false, updatable = false) })
	public CoberturaSeccionDTO getCoberturaSeccionDTO() {
		return coberturaSeccionDTO;
	}

	public void setCoberturaSeccionDTO(CoberturaSeccionDTO coberturaSeccionDTO) {
		this.coberturaSeccionDTO = coberturaSeccionDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTORIESGO", nullable = false, insertable = false, updatable = false)
	public RiesgoDTO getRiesgoDTO() {
		return this.riesgoDTO;
	}

	public void setRiesgoDTO(RiesgoDTO riesgoDTO) {
		this.riesgoDTO = riesgoDTO;
	}

	@Column(name = "DESCRIPCIONRIESGOCOBERTURA", nullable = false, length = 200)
	public String getDescripcionRiesgoCobertura() {
		return this.descripcionRiesgoCobertura;
	}

	public void setDescripcionRiesgoCobertura(String descripcionRiesgoCobertura) {
		this.descripcionRiesgoCobertura = descripcionRiesgoCobertura;
	}

	@Column(name = "CLAVEOBLIGATORIEDAD", nullable = false, precision = 4, scale = 0)
	public Short getClaveObligatoriedad() {
		return this.claveObligatoriedad;
	}

	public void setClaveObligatoriedad(Short claveObligatoriedad) {
		this.claveObligatoriedad = claveObligatoriedad;
	}
	
	@Column(name = "CLAVETIPOCOASEGURO", nullable = false, precision = 4, scale = 0)
	public Short getClaveTipoCoaseguro() {
		return this.claveTipoCoaseguro;
	}

	public void setClaveTipoCoaseguro(Short claveTipoCoaseguro) {
		this.claveTipoCoaseguro = claveTipoCoaseguro;
	}

	@Column(name = "VALORMINIMOCOASEGURO", nullable = false, precision = 16, scale = 4)
	public Double getValorMinimoCoaseguro() {
		return this.valorMinimoCoaseguro;
	}

	public void setValorMinimoCoaseguro(Double valorMinimoCoaseguro) {
		this.valorMinimoCoaseguro = valorMinimoCoaseguro;
	}

	@Column(name = "VALORMAXIMOCOASEGURO", nullable = false, precision = 16, scale = 4)
	public Double getValorMaximoCoaseguro() {
		return this.valorMaximoCoaseguro;
	}

	public void setValorMaximoCoaseguro(Double valorMaximoCoaseguro) {
		this.valorMaximoCoaseguro = valorMaximoCoaseguro;
	}

	@Column(name = "CLAVETIPOLIMITECOASEGURO", nullable = false, precision = 4, scale = 0)
	public Short getClaveTipoLimiteCoaseguro() {
		return this.claveTipoLimiteCoaseguro;
	}

	public void setClaveTipoLimiteCoaseguro(Short claveTipoLimiteCoaseguro) {
		this.claveTipoLimiteCoaseguro = claveTipoLimiteCoaseguro;
	}

	@Column(name = "VALORMINIMOLIMITECOASEGURO", nullable = false, precision = 16, scale = 4)
	public Double getValorMinimoLimiteCoaseguro() {
		return this.valorMinimoLimiteCoaseguro;
	}

	public void setValorMinimoLimiteCoaseguro(Double valorMinimoLimiteCoaseguro) {
		this.valorMinimoLimiteCoaseguro = valorMinimoLimiteCoaseguro;
	}

	@Column(name = "VALORMAXIMOLIMITECOASEGURO", nullable = false, precision = 16, scale = 4)
	public Double getValorMaximoLimiteCoaseguro() {
		return this.valorMaximoLimiteCoaseguro;
	}

	public void setValorMaximoLimiteCoaseguro(Double valorMaximoLimiteCoaseguro) {
		this.valorMaximoLimiteCoaseguro = valorMaximoLimiteCoaseguro;
	}

	@Column(name = "CLAVETIPODEDUCIBLE", nullable = false, precision = 4, scale = 0)
	public Short getClaveTipoDeducible() {
		return this.claveTipoDeducible;
	}

	public void setClaveTipoDeducible(Short claveTipoDeducible) {
		this.claveTipoDeducible = claveTipoDeducible;
	}

	@Column(name = "VALORMINIMODEDUCIBLE", nullable = false, precision = 16, scale = 4)
	public Double getValorMinimoDeducible() {
		return this.valorMinimoDeducible;
	}

	public void setValorMinimoDeducible(Double valorMinimoDeducible) {
		this.valorMinimoDeducible = valorMinimoDeducible;
	}

	@Column(name = "VALORMAXIMODEDUCIBLE", nullable = false, precision = 16, scale = 4)
	public Double getValorMaximoDeducible() {
		return this.valorMaximoDeducible;
	}

	public void setValorMaximoDeducible(Double valorMaximoDeducible) {
		this.valorMaximoDeducible = valorMaximoDeducible;
	}

	@Column(name = "CLAVETIPOLIMITEDEDUCIBLE", nullable = false, precision = 4, scale = 0)
	public Short getClaveTipoLimiteDeducible() {
		return this.claveTipoLimiteDeducible;
	}

	public void setClaveTipoLimiteDeducible(Short claveTipoLimiteDeducible) {
		this.claveTipoLimiteDeducible = claveTipoLimiteDeducible;
	}

	@Column(name = "VALORMINIMOLIMITEDEDUCIBLE", nullable = false, precision = 16, scale = 4)
	public Double getValorMinimoLimiteDeducible() {
		return this.valorMinimoLimiteDeducible;
	}

	public void setValorMinimoLimiteDeducible(Double valorMinimoLimiteDeducible) {
		this.valorMinimoLimiteDeducible = valorMinimoLimiteDeducible;
	}

	@Column(name = "VALORMAXIMOLIMITEDEDUCIBLE", nullable = false, precision = 16, scale = 4)
	public Double getValorMaximoLimiteDeducible() {
		return this.valorMaximoLimiteDeducible;
	}

	public void setValorMaximoLimiteDeducible(Double valorMaximoLimiteDeducible) {
		this.valorMaximoLimiteDeducible = valorMaximoLimiteDeducible;
	}

	@Column(name = "CLAVETIPOSUMAASEGURADA", nullable = false, precision = 4, scale = 0)
	public Short getClaveTipoSumaAsegurada() {
		return this.claveTipoSumaAsegurada;
	}

	public void setClaveTipoSumaAsegurada(Short claveTipoSumaAsegurada) {
		this.claveTipoSumaAsegurada = claveTipoSumaAsegurada;
	}

	@Column(name = "CLAVEAUTORIZACION", nullable = false, precision = 4, scale = 0)
	public Short getClaveAutorizacion() {
		return this.claveAutorizacion;
	}

	public void setClaveAutorizacion(Short claveAutorizacion) {
		this.claveAutorizacion = claveAutorizacion;
	}

	@Column(name = "CLAVEIMPORTECERO", nullable = false, precision = 4, scale = 0)
	public Short getClaveImporteCero() {
		return this.claveImporteCero;
	}

	public void setClaveImporteCero(Short claveImporteCero) {
		this.claveImporteCero = claveImporteCero;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "riesgoCoberturaDTO")
	public List<CoaseguroRiesgoCoberturaDTO> getCoaseguros() {
		return coaseguros;
	}

	public void setCoaseguros(List<CoaseguroRiesgoCoberturaDTO> coaseguros) {
		this.coaseguros = coaseguros;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "riesgoCoberturaDTO")
	public List<DeducibleRiesgoCoberturaDTO> getDeducibles() {
		return deducibles;
	}

	public void setDeducibles(List<DeducibleRiesgoCoberturaDTO> deducibles) {
		this.deducibles = deducibles;
	}

	@Transient
	public List<AumentoVarioCoberturaDTO> getAumentosCobertura() {
		return aumentosCobertura;
	}
	public void setAumentosCobertura(
			List<AumentoVarioCoberturaDTO> aumentosCobertura) {
		this.aumentosCobertura = aumentosCobertura;
	}

	@Transient
	public List<AumentoVarioTipoPolizaDTO> getAumentosTipoPoliza() {
		return aumentosTipoPoliza;
	}
	public void setAumentosTipoPoliza(
			List<AumentoVarioTipoPolizaDTO> aumentosTipoPoliza) {
		this.aumentosTipoPoliza = aumentosTipoPoliza;
	}

	@Transient
	public List<AumentoVarioProductoDTO> getAumentosProducto() {
		return aumentosProducto;
	}
	public void setAumentosProducto(List<AumentoVarioProductoDTO> aumentosProducto) {
		this.aumentosProducto = aumentosProducto;
	}

	@Transient
	public List<DescuentoVarioCoberturaDTO> getDescuentosCobertura() {
		return descuentosCobertura;
	}
	public void setDescuentosCobertura(
			List<DescuentoVarioCoberturaDTO> descuentosCobertura) {
		this.descuentosCobertura = descuentosCobertura;
	}

	@Transient
	public List<RecargoVarioCoberturaDTO> getRecargosCobertura() {
		return recargosCobertura;
	}
	public void setRecargosCobertura(List<RecargoVarioCoberturaDTO> recargosCobertura) {
		this.recargosCobertura = recargosCobertura;
	}

	@Transient
	public List<DescuentoVarioTipoPolizaDTO> getDescuentosTipoPoliza() {
		return descuentosTipoPoliza;
	}
	public void setDescuentosTipoPoliza(List<DescuentoVarioTipoPolizaDTO> descuentosTipoPoliza) {
		this.descuentosTipoPoliza = descuentosTipoPoliza;
	}

	@Transient
	public List<RecargoVarioTipoPolizaDTO> getRecargosTipoPoliza() {
		return recargosTipoPoliza;
	}
	public void setRecargosTipoPoliza(List<RecargoVarioTipoPolizaDTO> recargosTipoPoliza) {
		this.recargosTipoPoliza = recargosTipoPoliza;
	}

	@Transient
	public List<DescuentoVarioProductoDTO> getDescuentosProducto() {
		return descuentosProducto;
	}
	public void setDescuentosProducto(List<DescuentoVarioProductoDTO> descuentosProducto) {
		this.descuentosProducto = descuentosProducto;
	}

	@Transient
	public List<RecargoVarioProductoDTO> getRecargosProducto() {
		return recargosProducto;
	}
	public void setRecargosProducto(List<RecargoVarioProductoDTO> recargosProducto) {
		this.recargosProducto = recargosProducto;
	}
}
