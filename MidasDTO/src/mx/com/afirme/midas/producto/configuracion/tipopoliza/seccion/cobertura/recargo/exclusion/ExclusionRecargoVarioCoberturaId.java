package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.exclusion;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * ExclusionRecargoVarioCoberturaId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class ExclusionRecargoVarioCoberturaId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idtocobertura;
     private BigDecimal idtorecargovario;
     private BigDecimal idtoriesgo;


    // Constructors

    /** default constructor */
    public ExclusionRecargoVarioCoberturaId() {
    }

    
    /** full constructor */
    public ExclusionRecargoVarioCoberturaId(BigDecimal idtocobertura, BigDecimal idtorecargovario, BigDecimal idtoriesgo) {
        this.idtocobertura = idtocobertura;
        this.idtorecargovario = idtorecargovario;
        this.idtoriesgo = idtoriesgo;
    }

   
    // Property accessors

    @Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtocobertura() {
        return this.idtocobertura;
    }
    
    public void setIdtocobertura(BigDecimal idtocobertura) {
        this.idtocobertura = idtocobertura;
    }

    @Column(name="IDTORECARGOVARIO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtorecargovario() {
        return this.idtorecargovario;
    }
    
    public void setIdtorecargovario(BigDecimal idtorecargovario) {
        this.idtorecargovario = idtorecargovario;
    }

    @Column(name="IDTORIESGO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtoriesgo() {
        return this.idtoriesgo;
    }
    
    public void setIdtoriesgo(BigDecimal idtoriesgo) {
        this.idtoriesgo = idtoriesgo;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof ExclusionRecargoVarioCoberturaId) ) return false;
		 ExclusionRecargoVarioCoberturaId castOther = ( ExclusionRecargoVarioCoberturaId ) other; 
         
		 return ( (this.getIdtocobertura()==castOther.getIdtocobertura()) || ( this.getIdtocobertura()!=null && castOther.getIdtocobertura()!=null && this.getIdtocobertura().equals(castOther.getIdtocobertura()) ) )
 && ( (this.getIdtorecargovario()==castOther.getIdtorecargovario()) || ( this.getIdtorecargovario()!=null && castOther.getIdtorecargovario()!=null && this.getIdtorecargovario().equals(castOther.getIdtorecargovario()) ) )
 && ( (this.getIdtoriesgo()==castOther.getIdtoriesgo()) || ( this.getIdtoriesgo()!=null && castOther.getIdtoriesgo()!=null && this.getIdtoriesgo().equals(castOther.getIdtoriesgo()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdtocobertura() == null ? 0 : this.getIdtocobertura().hashCode() );
         result = 37 * result + ( getIdtorecargovario() == null ? 0 : this.getIdtorecargovario().hashCode() );
         result = 37 * result + ( getIdtoriesgo() == null ? 0 : this.getIdtoriesgo().hashCode() );
         return result;
   }   





}