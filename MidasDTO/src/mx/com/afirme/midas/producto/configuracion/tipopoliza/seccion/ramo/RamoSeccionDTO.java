package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.ramo;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;

/**
 * RamoSeccionDTO entity. @author José Luis Arellano
 */
@Entity
@Table(name = "TRRAMOSECCION", schema = "MIDAS")
public class RamoSeccionDTO implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	// Fields
	private SeccionDTO seccionDTO;
	private RamoDTO ramoDTO;
	private RamoSeccionId id;

	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idtoseccion", column = @Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idtcramo", column = @Column(name = "IDTCRAMO", nullable = false, precision = 22, scale = 0)) })
	public RamoSeccionId getId() {
		return id;
	}

	public void setId(RamoSeccionId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOSECCION", nullable = false, insertable = false, updatable = false)
	public SeccionDTO getSeccionDTO() {
		return seccionDTO;
	}

	public void setSeccionDTO(SeccionDTO seccionDTO) {
		this.seccionDTO = seccionDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCRAMO", nullable = false, insertable = false, updatable = false)
	public RamoDTO getRamoDTO() {
		return ramoDTO;
	}

	public void setRamoDTO(RamoDTO ramoDTO) {
		this.ramoDTO = ramoDTO;
	}
}
