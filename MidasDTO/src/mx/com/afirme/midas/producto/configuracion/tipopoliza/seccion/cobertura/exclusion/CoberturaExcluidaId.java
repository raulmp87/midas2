package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.exclusion;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * CoberturaExcluidaId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class CoberturaExcluidaId implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToCobertura;
	private BigDecimal idToCoberturaExcluida;

	// Constructors

	/** default constructor */
	public CoberturaExcluidaId() {
	}

	/** full constructor */
	public CoberturaExcluidaId(BigDecimal idToCobertura,
			BigDecimal idToCoberturaExcluida) {
		this.idToCobertura = idToCobertura;
		this.idToCoberturaExcluida = idToCoberturaExcluida;
	}

	// Property accessors

	@Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCobertura() {
		return this.idToCobertura;
	}

	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	@Column(name = "IDCOBERTURAEXCLUIDA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCoberturaExcluida() {
		return this.idToCoberturaExcluida;
	}

	public void setIdToCoberturaExcluida(BigDecimal idToCoberturaExcluida) {
		this.idToCoberturaExcluida = idToCoberturaExcluida;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof CoberturaExcluidaId))
			return false;
		CoberturaExcluidaId castOther = (CoberturaExcluidaId) other;

		return ((this.getIdToCobertura() == castOther.getIdToCobertura()) || (this
				.getIdToCobertura() != null
				&& castOther.getIdToCobertura() != null && this
				.getIdToCobertura().equals(castOther.getIdToCobertura())))
				&& ((this.getIdToCoberturaExcluida() == castOther
						.getIdToCoberturaExcluida()) || (this
						.getIdToCoberturaExcluida() != null
						&& castOther.getIdToCoberturaExcluida() != null && this
						.getIdToCoberturaExcluida().equals(
								castOther.getIdToCoberturaExcluida())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToCobertura() == null ? 0 : this.getIdToCobertura()
						.hashCode());
		result = 37
				* result
				+ (getIdToCoberturaExcluida() == null ? 0 : this
						.getIdToCoberturaExcluida().hashCode());
		return result;
	}

}