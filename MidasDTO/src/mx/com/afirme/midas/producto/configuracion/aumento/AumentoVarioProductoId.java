package mx.com.afirme.midas.producto.configuracion.aumento;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * AumentoVarioProductoId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class AumentoVarioProductoId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idtoproducto;
     private BigDecimal idtoaumentovario;


    // Constructors

    /** default constructor */
    public AumentoVarioProductoId() {
    }

    
    /** full constructor */
    public AumentoVarioProductoId(BigDecimal idtoproducto, BigDecimal idtoaumentovario) {
        this.idtoproducto = idtoproducto;
        this.idtoaumentovario = idtoaumentovario;
    }

   
    // Property accessors

    @Column(name="IDTOPRODUCTO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtoproducto() {
        return this.idtoproducto;
    }
    
    public void setIdtoproducto(BigDecimal idtoproducto) {
        this.idtoproducto = idtoproducto;
    }

    @Column(name="IDTOAUMENTOVARIO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtoaumentovario() {
        return this.idtoaumentovario;
    }
    
    public void setIdtoaumentovario(BigDecimal idtoaumentovario) {
        this.idtoaumentovario = idtoaumentovario;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof AumentoVarioProductoId) ) return false;
		 AumentoVarioProductoId castOther = ( AumentoVarioProductoId ) other; 
         
		 return ( (this.getIdtoproducto()==castOther.getIdtoproducto()) || ( this.getIdtoproducto()!=null && castOther.getIdtoproducto()!=null && this.getIdtoproducto().equals(castOther.getIdtoproducto()) ) )
 && ( (this.getIdtoaumentovario()==castOther.getIdtoaumentovario()) || ( this.getIdtoaumentovario()!=null && castOther.getIdtoaumentovario()!=null && this.getIdtoaumentovario().equals(castOther.getIdtoaumentovario()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdtoproducto() == null ? 0 : this.getIdtoproducto().hashCode() );
         result = 37 * result + ( getIdtoaumentovario() == null ? 0 : this.getIdtoaumentovario().hashCode() );
         return result;
   }   





}