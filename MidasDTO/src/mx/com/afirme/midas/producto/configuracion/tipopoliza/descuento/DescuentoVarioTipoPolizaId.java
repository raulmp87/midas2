package mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * DescuentoVarioTipoPolizaId entity. @author José Luis Arellano
 */
@Embeddable
public class DescuentoVarioTipoPolizaId implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idtotipopoliza;
    private BigDecimal idtodescuentovario;

    public DescuentoVarioTipoPolizaId() {
    }
    
    public DescuentoVarioTipoPolizaId(BigDecimal idtotipopoliza, BigDecimal idtodescuentovario) {
        this.idtotipopoliza = idtotipopoliza;
        this.idtodescuentovario = idtodescuentovario;
    }

   
    // Property accessors

    @Column(name="IDTOTIPOPOLIZA", nullable=false, precision=22, scale=0)
    public BigDecimal getIdtotipopoliza() {
        return this.idtotipopoliza;
    }
    
    public void setIdtotipopoliza(BigDecimal idtotipopoliza) {
        this.idtotipopoliza = idtotipopoliza;
    }

    @Column(name="IDTODESCUENTOVARIO", nullable=false, precision=22, scale=0)
    public BigDecimal getIdtodescuentovario() {
		return idtodescuentovario;
	}

	public void setIdtodescuentovario(BigDecimal idtodescuentovario) {
		this.idtodescuentovario = idtodescuentovario;
	}
}
