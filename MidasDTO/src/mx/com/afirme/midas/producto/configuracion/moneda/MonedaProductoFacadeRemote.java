package mx.com.afirme.midas.producto.configuracion.moneda;
// default package

import java.util.List;
import javax.ejb.Remote;


/**
 * Remote interface for MonedaProductoDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface MonedaProductoFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved MonedaProductoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity MonedaProductoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(MonedaProductoDTO entity);
    /**
	 Delete a persistent MonedaProductoDTO entity.
	  @param entity MonedaProductoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(MonedaProductoDTO entity);
   /**
	 Persist a previously saved MonedaProductoDTO entity and return it or a copy of it to the sender. 
	 A copy of the MonedaProductoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity MonedaProductoDTO entity to update
	 @return MonedaProductoDTO the persisted MonedaProductoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public MonedaProductoDTO update(MonedaProductoDTO entity);
	public MonedaProductoDTO findById( MonedaProductoId id);
	 /**
	 * Find all MonedaProductoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the MonedaProductoDTO property to query
	  @param value the property value to match
	  	  @return List<MonedaProductoDTO> found by query
	 */
	public List<MonedaProductoDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all MonedaProductoDTO entities.
	  	  @return List<MonedaProductoDTO> all MonedaProductoDTO entities
	 */
	public List<MonedaProductoDTO> findAll(
		);	
}