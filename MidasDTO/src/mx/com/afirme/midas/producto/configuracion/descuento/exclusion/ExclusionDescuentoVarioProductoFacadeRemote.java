package mx.com.afirme.midas.producto.configuracion.descuento.exclusion;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

/**
 * Remote interface for ExclusionDescuentoVarioProductoFacade.
 * @author Jos� Luis Arellano
 */

public interface ExclusionDescuentoVarioProductoFacadeRemote {
	/**
	 Perform an initial save of a previously unsaved ExclusionDescuentoVarioProductoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
 	@param entity ExclusionDescuentoVarioProductoDTO entity to persist
	@throws RuntimeException when the operation fails
	*/
   public void save(ExclusionDescuentoVarioProductoDTO entity);
   /**
	 Delete a persistent ExclusionDescuentoVarioProductoDTO entity.
	  @param entity ExclusionDescuentoVarioProductoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
   public void delete(ExclusionDescuentoVarioProductoDTO entity);
  /**
	 Persist a previously saved ExclusionDescuentoVarioProductoDTO entity and return it or a copy of it to the sender. 
	 A copy of the ExclusionDescuentoVarioProductoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ExclusionDescuentoVarioProductoDTO entity to update
	 @return ExclusionDescuentoVarioProductoDTO the persisted ExclusionDescuentoVarioProductoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ExclusionDescuentoVarioProductoDTO update(ExclusionDescuentoVarioProductoDTO entity);
	
	public ExclusionDescuentoVarioProductoDTO findById( ExclusionDescuentoVarioProductoId id);
	 /**
	 * Find all ExclusionDescuentoVarioProductoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ExclusionDescuentoVarioProductoDTO property to query
	  @param value the property value to match
	  @return List<ExclusionDescuentoVarioProductoDTO> found by query
	 */
	public List<ExclusionDescuentoVarioProductoDTO> findByProperty(String propertyName, Object value);
	/**
	 * Find all ExclusionDescuentoVarioProductoDTO entities.
	  	  @return List<ExclusionDescuentoVarioProductoDTO> all ExclusionDescuentoVarioProductoDTO entities
	 */
	public List<ExclusionDescuentoVarioProductoDTO> findAll();
	
	/**
	 * Encuentra los registros de ExclusionDescuentoVarioProductoDTO relacionados con el ProductoDTO cuyo ID se recibe y que adem�s est�n 
	 * relacionados s�lo con Tipos de p�liza que no hayan sido borradas l�gicamente.
	  @param BigDecimal idToProducto. El ID del producto
	  @return List<ExclusionDescuentoVarioProductoDTO> encontrados por el query formado.
	 */
    public List<ExclusionDescuentoVarioProductoDTO> getVigentesPorIdProducto(BigDecimal idToProducto);
}
