package mx.com.afirme.midas.producto.configuracion.moneda;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * MonedaProductoDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class MonedaProductoId implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToProducto;
	private BigDecimal idMoneda;

	// Constructors

	/** default constructor */
	public MonedaProductoId() {
	}

	// Property accessors

	@Column(name = "IDTOPRODUCTO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToProducto() {
		return this.idToProducto;
	}

	public void setIdToProducto(BigDecimal idToProducto) {
		this.idToProducto = idToProducto;
	}

	@Column(name = "IDMONEDA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdMoneda() {
		return this.idMoneda;
	}

	public void setIdMoneda(BigDecimal idMoneda) {
		this.idMoneda = idMoneda;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof MonedaProductoId))
			return false;
		MonedaProductoId castOther = (MonedaProductoId) other;

		return ((this.getIdToProducto() == castOther.getIdToProducto()) || (this
				.getIdToProducto() != null
				&& castOther.getIdToProducto() != null && this
				.getIdToProducto().equals(castOther.getIdToProducto())))
				&& ((this.getIdMoneda() == castOther.getIdMoneda()) || (this
						.getIdMoneda() != null
						&& castOther.getIdMoneda() != null && this
						.getIdMoneda().equals(castOther.getIdMoneda())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToProducto() == null ? 0 : this.getIdToProducto()
						.hashCode());
		result = 37 * result
				+ (getIdMoneda() == null ? 0 : this.getIdMoneda().hashCode());
		return result;
	}

}