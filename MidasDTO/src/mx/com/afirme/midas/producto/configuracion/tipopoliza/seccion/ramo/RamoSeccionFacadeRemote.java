package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.ramo;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for RamoSeccionDTOFacade.
 * @author Jos� Luis Arellano
 */

public interface RamoSeccionFacadeRemote {
	/**
	 Perform an initial save of a previously unsaved RamoSeccionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity RamoSeccionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
  public void save(RamoSeccionDTO entity);
  /**
	 Delete a persistent RamoSeccionDTO entity.
	  @param entity RamoSeccionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
  public void delete(RamoSeccionDTO entity);
 /**
	 Persist a previously saved RamoSeccionDTO entity and return it or a copy of it to the sender. 
	 A copy of the RamoSeccionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity RamoSeccionDTO entity to update
	 @return RamoSeccionDTO the persisted RamoSeccionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public RamoSeccionDTO update(RamoSeccionDTO entity);
	
	public RamoSeccionDTO findById( RamoSeccionId id);
	 /**
	 * Find all RamoSeccionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the RamoSeccionDTO property to query
	  @param value the property value to match
	  	  @return List<RamoSeccionDTO> found by query
	 */
	public List<RamoSeccionDTO> findByProperty(String propertyName, Object value);
	
	/**
	 * Find all RamoSeccionDTO entities.
	  	  @return List<RamoSeccionDTO> all RamoSeccionDTO entities
	 */
	public List<RamoSeccionDTO> findAll();
	
	/**
	 * Lista todos los ramos que no est�n asociadas a una seccion determinada 
	 * por el id que recibe el m�todo
	 * @param BigDecimal idToSeccion el id de la seccion.
	 * @return List<RamoSeccionDTO> registros RamoSeccion que no est�n asociados a la seccion.
	 */
	public List<RamoSeccionDTO> obtenerRamosSinAsociar(BigDecimal idToSeccion);
	
}
