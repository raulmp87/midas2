package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for RecargoVarioCoberturaFacade.
 * @author MyEclipse Persistence Tools
 */


public interface RecargoVarioCoberturaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved RecargoVarioCoberturaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity RecargoVarioCoberturaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(RecargoVarioCoberturaDTO entity);
    /**
	 Delete a persistent RecargoVarioCoberturaDTO entity.
	  @param entity RecargoVarioCoberturaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(RecargoVarioCoberturaDTO entity);
   /**
	 Persist a previously saved RecargoVarioCoberturaDTO entity and return it or a copy of it to the sender. 
	 A copy of the RecargoVarioCoberturaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity RecargoVarioCoberturaDTO entity to update
	 @return RecargoVarioCoberturaDTO the persisted RecargoVarioCoberturaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public RecargoVarioCoberturaDTO update(RecargoVarioCoberturaDTO entity);
	public RecargoVarioCoberturaDTO findById( RecargoVarioCoberturaId id);
	 /**
	 * Find all RecargoVarioCoberturaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the RecargoVarioCoberturaDTO property to query
	  @param value the property value to match
	  	  @return List<RecargoVarioCoberturaDTO> found by query
	 */
	public List<RecargoVarioCoberturaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all RecargoVarioCoberturaDTO entities.
	  	  @return List<RecargoVarioCoberturaDTO> all RecargoVarioCoberturaDTO entities
	 */
	public List<RecargoVarioCoberturaDTO> findAll(
		);

	public List<RecargoVarioCoberturaDTO> buscarRecargoCobertura(BigDecimal idToCobertura, BigDecimal idToRecargoVario);
}