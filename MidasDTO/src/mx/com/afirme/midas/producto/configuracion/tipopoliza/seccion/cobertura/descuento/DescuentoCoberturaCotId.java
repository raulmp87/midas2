package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DescuentoCoberturaCotId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7519338435515922075L;
	
	private Long idToCotizacion;
	private Long numeroInciso;
	private Long idToSeccion;
	private Long idToCobertura;
	private Long idToDescuentoVario;
	
	@Column(name = "IDTOCOTIZACION", nullable = false)
	public Long getIdToCotizacion() {
		return idToCotizacion;
	}
	public void setIdToCotizacion(Long idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	
	@Column(name = "NUMEROINCISO", nullable = false)
	public Long getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(Long numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	
	@Column(name = "IDTOSECCION", nullable = false)
	public Long getIdToSeccion() {
		return idToSeccion;
	}
	public void setIdToSeccion(Long idToSeccion) {
		this.idToSeccion = idToSeccion;
	}
	
	@Column(name = "IDTOCOBERTURA", nullable = false)
	public Long getIdToCobertura() {
		return idToCobertura;
	}
	public void setIdToCobertura(Long idToCobertura) {
		this.idToCobertura = idToCobertura;
	}
	
	@Column(name = "IDTODESCUENTOVARIO", nullable = false)
	public Long getIdToDescuentoVario() {
		return idToDescuentoVario;
	}
	public void setIdToDescuentoVario(Long idToDescuentoVario) {
		this.idToDescuentoVario = idToDescuentoVario;
	}
	
	
   public boolean equals(Object other) {
	   if ( (this == other ) ) return true;
	   if ( (other == null ) ) return false;
	   if ( !(other instanceof DescuentoCoberturaCotId) ) return false;
	   DescuentoCoberturaCotId castOther = ( DescuentoCoberturaCotId ) other; 
     
	   return ( (this.getIdToCobertura()==castOther.getIdToCobertura()) || ( this.getIdToCobertura()!=null && castOther.getIdToCobertura()!=null && this.getIdToCobertura().equals(castOther.getIdToCobertura()) ) )
	   && ((this.getNumeroInciso()==castOther.getNumeroInciso()) || ( this.getNumeroInciso()!=null && castOther.getNumeroInciso()!=null && this.getNumeroInciso().equals(castOther.getNumeroInciso()))) 
	   && ((this.getIdToSeccion()==castOther.getIdToSeccion()) || ( this.getIdToSeccion()!=null && castOther.getIdToSeccion()!=null && this.getIdToSeccion().equals(castOther.getIdToSeccion()))) 
	   && ((this.getIdToCotizacion()==castOther.getIdToCotizacion()) || ( this.getIdToCotizacion()!=null && castOther.getIdToCotizacion()!=null && this.getIdToCotizacion().equals(castOther.getIdToCotizacion()))) 
	   && ((this.getIdToDescuentoVario()==castOther.getIdToDescuentoVario()) || ( this.getIdToDescuentoVario()!=null && castOther.getIdToDescuentoVario()!=null && this.getIdToDescuentoVario().equals(castOther.getIdToDescuentoVario()) ) );
   }

}
