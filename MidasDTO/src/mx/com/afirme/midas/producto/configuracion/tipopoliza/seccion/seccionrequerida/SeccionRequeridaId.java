package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.seccionrequerida;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * SeccionRequeridaId entity. @author José Luis Arellano
 */
@Embeddable
public class SeccionRequeridaId  implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
     private BigDecimal idToSeccion;
     private BigDecimal idSeccionRequerida;
     
    /** default constructor */
    public SeccionRequeridaId() {
    }

    /** full constructor */
    public SeccionRequeridaId(BigDecimal idToSeccion,BigDecimal idSeccionRequerida) {
		this.idToSeccion = idToSeccion;
		this.idSeccionRequerida = idSeccionRequerida;
	}

    @Column(name="IDTOSECCION", nullable=false, precision=22, scale=0)
    public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

    @Column(name="IDSECCIONREQUERIDA", nullable=false, precision=22, scale=0)
    public BigDecimal getIdSeccionRequerida() {
		return idSeccionRequerida;
	}

	public void setIdSeccionRequerida(BigDecimal idSeccionRequerida) {
		this.idSeccionRequerida = idSeccionRequerida;
	}

   public boolean equals(Object other) {
     if ( (this == other ) ) return true;
	 if ( (other == null ) ) return false;
	 if ( !(other instanceof SeccionRequeridaId) ) return false;
	 SeccionRequeridaId castOther = ( SeccionRequeridaId ) other; 
     
	 return ( (this.getIdToSeccion()==castOther.getIdToSeccion()) || ( this.getIdToSeccion()!=null && castOther.getIdToSeccion()!=null && this.getIdToSeccion().equals(castOther.getIdToSeccion()) ) )&& ( (this.getIdSeccionRequerida()==castOther.getIdSeccionRequerida()) || ( this.getIdSeccionRequerida()!=null && castOther.getIdSeccionRequerida()!=null && this.getIdSeccionRequerida().equals(castOther.getIdSeccionRequerida()) ) );
   }
   public int hashCode() {
     int result = 17;
     
     result = 37 * result + ( getIdToSeccion() == null ? 0 : this.getIdToSeccion().hashCode() );
     result = 37 * result + ( getIdSeccionRequerida() == null ? 0 : this.getIdSeccionRequerida().hashCode() );
     return result;
   }
}