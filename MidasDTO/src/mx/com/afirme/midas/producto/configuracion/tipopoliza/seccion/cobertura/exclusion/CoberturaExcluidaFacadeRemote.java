package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.exclusion;

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for CoberturaExcluidaDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface CoberturaExcluidaFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved CoberturaExcluidaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            CoberturaExcluidaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(CoberturaExcluidaDTO entity);

	/**
	 * Delete a persistent CoberturaExcluidaDTO entity.
	 * 
	 * @param entity
	 *            CoberturaExcluidaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(CoberturaExcluidaDTO entity);

	/**
	 * Persist a previously saved CoberturaExcluidaDTO entity and return it or a
	 * copy of it to the sender. A copy of the CoberturaExcluidaDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            CoberturaExcluidaDTO entity to update
	 * @return CoberturaExcluidaDTO the persisted CoberturaExcluidaDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public CoberturaExcluidaDTO update(CoberturaExcluidaDTO entity);

	public CoberturaExcluidaDTO findById(CoberturaExcluidaId id);

	/**
	 * Find all CoberturaExcluidaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the CoberturaExcluidaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<CoberturaExcluidaDTO> found by query
	 */
	public List<CoberturaExcluidaDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all CoberturaExcluidaDTO entities.
	 * 
	 * @return List<CoberturaExcluidaDTO> all CoberturaExcluidaDTO entities
	 */
	public List<CoberturaExcluidaDTO> findAll();
}