package mx.com.afirme.midas.producto.configuracion.descuento;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * DescuentoPorProductoDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class DescuentoVarioProductoId implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToProducto;
	private BigDecimal idToDescuentoVario;

	// Constructors

	/** default constructor */
	public DescuentoVarioProductoId() {
	}

	// Property accessors

	@Column(name = "IDTOPRODUCTO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToProducto() {
		return this.idToProducto;
	}

	public void setIdToProducto(BigDecimal idToProducto) {
		this.idToProducto = idToProducto;
	}

	@Column(name = "IDTODESCUENTOVARIO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToDescuentoVario() {
		return this.idToDescuentoVario;
	}

	public void setIdToDescuentoVario(BigDecimal idToDescuentoVario) {
		this.idToDescuentoVario = idToDescuentoVario;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof DescuentoVarioProductoId))
			return false;
		DescuentoVarioProductoId castOther = (DescuentoVarioProductoId) other;

		return ((this.getIdToProducto() == castOther.getIdToProducto()) || (this
				.getIdToProducto() != null
				&& castOther.getIdToProducto() != null && this
				.getIdToProducto().equals(castOther.getIdToProducto())))
				&& ((this.getIdToDescuentoVario() == castOther
						.getIdToDescuentoVario()) || (this
						.getIdToDescuentoVario() != null
						&& castOther.getIdToDescuentoVario() != null && this
						.getIdToDescuentoVario().equals(
								castOther.getIdToDescuentoVario())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToProducto() == null ? 0 : this.getIdToProducto()
						.hashCode());
		result = 37
				* result
				+ (getIdToDescuentoVario() == null ? 0 : this
						.getIdToDescuentoVario().hashCode());
		return result;
	}

}