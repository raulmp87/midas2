package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.tipovehiculo;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name = "SeccionTipoVehiculoDTO")
@Table(name = "TOSECCIONTIPOVEHICULO", schema = "MIDAS")
public class SeccionTipoVehiculoDTO implements java.io.Serializable, Entidad {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SeccionTipoVehiculoId id;
	
	public void setId(SeccionTipoVehiculoId id) {
		this.id = id;
	}
	
	@EmbeddedId
	public SeccionTipoVehiculoId getId() {
		return id;
	}
	@SuppressWarnings("unchecked")
	@Override
	public SeccionTipoVehiculoId getKey() {
		return this.getId();
	}
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}
	@SuppressWarnings("unchecked")
	@Override
	public SeccionTipoVehiculoId getBusinessKey() {
		return this.getId();
	}

}
