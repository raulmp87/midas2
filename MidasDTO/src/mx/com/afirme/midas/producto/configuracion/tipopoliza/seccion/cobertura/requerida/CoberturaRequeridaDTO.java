package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.requerida;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;

/**
 * CoberturaRequeridaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TRCOBERTURAREQUERIDA", schema = "MIDAS")
public class CoberturaRequeridaDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CoberturaRequeridaId id;
	private CoberturaDTO coberturaBaseDTO;
	private CoberturaDTO coberturaRequeridaDTO;

	// Constructors

	/** default constructor */
	public CoberturaRequeridaDTO() {
	}

	/** full constructor */
	public CoberturaRequeridaDTO(CoberturaRequeridaId id,
			CoberturaDTO coberturaBaseDTO, CoberturaDTO coberturaRequeridaDTO) {
		this.id = id;
		this.coberturaBaseDTO = coberturaBaseDTO;
		this.coberturaRequeridaDTO = coberturaRequeridaDTO;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToCobertura", column = @Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToCoberturaRequerida", column = @Column(name = "IDCOBERTURAREQUERIDA", nullable = false, precision = 22, scale = 0)) })
	public CoberturaRequeridaId getId() {
		return this.id;
	}

	public void setId(CoberturaRequeridaId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTOCOBERTURA", nullable = false, insertable = false, updatable = false)
	public CoberturaDTO getCoberturaBaseDTO() {
		return this.coberturaBaseDTO;
	}

	public void setCoberturaBaseDTO(CoberturaDTO coberturaBaseDTO) {
		this.coberturaBaseDTO = coberturaBaseDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDCOBERTURAREQUERIDA", nullable = false, insertable = false, updatable = false)
	public CoberturaDTO getCoberturaRequeridaDTO() {
		return this.coberturaRequeridaDTO;
	}

	public void setCoberturaRequeridaDTO(CoberturaDTO coberturaRequeridaDTO) {
		this.coberturaRequeridaDTO = coberturaRequeridaDTO;
	}

}