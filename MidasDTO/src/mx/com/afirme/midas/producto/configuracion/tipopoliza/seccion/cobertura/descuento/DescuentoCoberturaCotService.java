package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento;

import java.util.List;

import javax.ejb.Local;

@Local
public interface DescuentoCoberturaCotService {

	/**
	 * Bulk deletes <code>DescuentoCoberturaCot</code>. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idToSeccion
	 * @param idToDescuentoVario
	 */
	public void bulkDelete(Long idToCotizacion, Long numeroInciso, Long idToSeccion, Long idToDescuentoVario);
	
	/**
	 * Bulk deletes <code>DescuentoCoberturaCot</code>. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idToSeccion
	 * @param idToDescuentoVarios
	 */
	public void bulkDelete(Long idToCotizacion, Long numeroInciso, Long idToSeccion, List<Long> idToDescuentoVarios);
	
	
}
