package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.exclusion;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.aumentovario.AumentoVarioDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDTO;


/**
 * ExclusionAumentoVarioCoberturaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TREXCAUMENTOVARCOBERTURA"
    ,schema="MIDAS"
)
public class ExclusionAumentoVarioCoberturaDTO  implements java.io.Serializable {

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ExclusionAumentoVarioCoberturaId id;
    private AumentoVarioDTO aumentoVarioDTO;
    private RiesgoDTO riesgoDTO;
    private CoberturaDTO coberturaDTO;



    /** default constructor */
    public ExclusionAumentoVarioCoberturaDTO() {
    }

    
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idtocobertura", column=@Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idtoaumentovario", column=@Column(name="IDTOAUMENTOVARIO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idtoriesgo", column=@Column(name="IDTORIESGO", nullable=false, precision=22, scale=0) ) } )

    public ExclusionAumentoVarioCoberturaId getId() {
        return this.id;
    }
    
    public void setId(ExclusionAumentoVarioCoberturaId id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTOAUMENTOVARIO", nullable=false, insertable=false, updatable=false)

    public AumentoVarioDTO getAumentoVarioDTO() {
		return aumentoVarioDTO;
	}


	public void setAumentoVarioDTO(AumentoVarioDTO aumentoVarioDTO) {
		this.aumentoVarioDTO = aumentoVarioDTO;
	}
    
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTORIESGO", nullable=false, insertable=false, updatable=false)
    public RiesgoDTO getRiesgoDTO() {
		return riesgoDTO;
	}


	public void setRiesgoDTO(RiesgoDTO riesgoDTO) {
		this.riesgoDTO = riesgoDTO;
	}
   
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTOCOBERTURA", nullable=false, insertable=false, updatable=false)

    public CoberturaDTO getCoberturaDTO() {
		return coberturaDTO;
	}


	public void setCoberturaDTO(CoberturaDTO coberturaDTO) {
		this.coberturaDTO = coberturaDTO;
	}

	public boolean equals(Object o) {
		if (o instanceof ExclusionAumentoVarioCoberturaDTO) {
			ExclusionAumentoVarioCoberturaDTO temp = (ExclusionAumentoVarioCoberturaDTO) o;
			if (temp.getId().getIdtocobertura().intValue() == this.getId().getIdtocobertura().intValue()
					&& temp.getId().getIdtoaumentovario().intValue() == this.getId().getIdtoaumentovario().intValue()
					&& temp.getId().getIdtoriesgo().intValue() == this.getId().getIdtoriesgo().intValue()) {
				return true;
			}
		}
		return false;
	}

	public int hashCode() {
		int hash = 1;
	    hash = hash * 31 + this.getId().getIdtocobertura().hashCode();
	    hash = hash * 31 + this.getId().getIdtoaumentovario().hashCode();
	    hash = hash * 31 + this.getId().getIdtoriesgo().hashCode();
	    return hash;
	}
}