package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo;
// default package

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;


/**
 * RecargoVarioCoberturaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TORECARGOVARIOCOBERTURA"
    ,schema="MIDAS"
)
public class RecargoVarioCoberturaDTO  implements java.io.Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private RecargoVarioCoberturaId id;
    private RecargoVarioDTO recargoVarioDTO;
    private CoberturaDTO coberturaDTO;
    private Short claveobligatoriedad;
    private Short clavecomercialtecnico;
    private Short claveaplicareaseguro;
    private Double valor;


    // Constructors

    /** default constructor */
    public RecargoVarioCoberturaDTO() {
    }

   
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idtocobertura", column=@Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idtorecargovario", column=@Column(name="IDTORECARGOVARIO", nullable=false, precision=22, scale=0) ) } )

    public RecargoVarioCoberturaId getId() {
        return this.id;
    }
    
    public void setId(RecargoVarioCoberturaId id) {
        this.id = id;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTORECARGOVARIO", nullable=false, insertable=false, updatable=false)
    public RecargoVarioDTO getRecargoVarioDTO() {
        return this.recargoVarioDTO;
    }
    
    public void setRecargoVarioDTO(RecargoVarioDTO recargoVarioDTO) {
        this.recargoVarioDTO = recargoVarioDTO;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTOCOBERTURA", nullable=false, insertable=false, updatable=false)
     public CoberturaDTO getCoberturaDTO() {
		return coberturaDTO;
	}


	public void setCoberturaDTO(CoberturaDTO coberturaDTO) {
		this.coberturaDTO = coberturaDTO;
	}
   
    
    @Column(name="CLAVEOBLIGATORIEDAD", nullable=false, precision=4, scale=0)

    public Short getClaveobligatoriedad() {
        return this.claveobligatoriedad;
    }
 

	public void setClaveobligatoriedad(Short claveobligatoriedad) {
        this.claveobligatoriedad = claveobligatoriedad;
    }
    
    @Column(name="CLAVECOMERCIALTECNICO", nullable=false, precision=4, scale=0)

    public Short getClavecomercialtecnico() {
        return this.clavecomercialtecnico;
    }
    
    public void setClavecomercialtecnico(Short clavecomercialtecnico) {
        this.clavecomercialtecnico = clavecomercialtecnico;
    }
    
    @Column(name="CLAVEAPLICAREASEGURO", nullable=false, precision=4, scale=0)

    public Short getClaveaplicareaseguro() {
        return this.claveaplicareaseguro;
    }
    
    public void setClaveaplicareaseguro(Short claveaplicareaseguro) {
        this.claveaplicareaseguro = claveaplicareaseguro;
    }
    
    @Column(name="VALOR", nullable=false, precision=16)

    public Double getValor() {
        return this.valor;
    }
    
    public void setValor(Double valor) {
        this.valor = valor;
    }
   








}