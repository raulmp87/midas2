package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.exclusion;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.descuento.DescuentoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDTO;


/**
 * ExclusionDescuentoVarioCoberturaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TREXCDESCUENTOVARCOBERTURA",schema="MIDAS")
public class ExclusionDescuentoVarioCoberturaDTO  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private ExclusionDescuentoVarioCoberturaId id;
    private DescuentoDTO descuentoDTO;
    private RiesgoDTO riesgoDTO;
    private CoberturaDTO coberturaDTO;



    /** default constructor */
    public ExclusionDescuentoVarioCoberturaDTO() {
    }

    
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idtocobertura", column=@Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idtodescuentovario", column=@Column(name="IDTODESCUENTOVARIO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idtoriesgo", column=@Column(name="IDTORIESGO", nullable=false, precision=22, scale=0) ) } )

    public ExclusionDescuentoVarioCoberturaId getId() {
        return this.id;
    }
    
    public void setId(ExclusionDescuentoVarioCoberturaId id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTODESCUENTOVARIO", nullable=false, insertable=false, updatable=false)
    public DescuentoDTO getDescuentoDTO() {
		return descuentoDTO;
	}

	public void setDescuentoDTO(DescuentoDTO descuentoDTO) {
		this.descuentoDTO = descuentoDTO;
	}
    
    
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTORIESGO", nullable=false, insertable=false, updatable=false)
    public RiesgoDTO getRiesgoDTO() {
		return riesgoDTO;
	}

	public void setRiesgoDTO(RiesgoDTO riesgoDTO) {
		this.riesgoDTO = riesgoDTO;
	}
   
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTOCOBERTURA", nullable=false, insertable=false, updatable=false)
    public CoberturaDTO getCoberturaDTO() {
		return coberturaDTO;
	}


	public void setCoberturaDTO(CoberturaDTO coberturaDTO) {
		this.coberturaDTO = coberturaDTO;
	}

	public boolean equals(Object o) {
		if (o instanceof ExclusionDescuentoVarioCoberturaDTO) {
			ExclusionDescuentoVarioCoberturaDTO temp = (ExclusionDescuentoVarioCoberturaDTO) o;
			if (temp.getId().getIdtocobertura().intValue() == this.getId().getIdtocobertura().intValue()
					&& temp.getId().getIdtodescuentovario().intValue() == this.getId().getIdtodescuentovario().intValue()
					&& temp.getId().getIdtoriesgo().intValue() == this.getId().getIdtoriesgo().intValue()) {
				return true;
			}
		}
		return false;
	}

	public int hashCode() {
		int hash = 1;
	    hash = hash * 31 + this.getId().getIdtocobertura().hashCode();
	    hash = hash * 31 + this.getId().getIdtodescuentovario().hashCode();
	    hash = hash * 31 + this.getId().getIdtoriesgo().hashCode();
	    return hash;
	}
}