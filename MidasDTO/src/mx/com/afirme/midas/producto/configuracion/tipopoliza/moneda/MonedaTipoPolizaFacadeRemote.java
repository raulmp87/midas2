package mx.com.afirme.midas.producto.configuracion.tipopoliza.moneda;
// default package

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

/**
 * Remote interface for MonedaTipoPolizaDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface MonedaTipoPolizaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved MonedaTipoPolizaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity MonedaTipoPolizaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(MonedaTipoPolizaDTO entity);
    /**
	 Delete a persistent MonedaTipoPolizaDTO entity.
	  @param entity MonedaTipoPolizaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(MonedaTipoPolizaDTO entity);
   /**
	 Persist a previously saved MonedaTipoPolizaDTO entity and return it or a copy of it to the sender. 
	 A copy of the MonedaTipoPolizaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity MonedaTipoPolizaDTO entity to update
	 @return MonedaTipoPolizaDTO the persisted MonedaTipoPolizaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public MonedaTipoPolizaDTO update(MonedaTipoPolizaDTO entity);
	public MonedaTipoPolizaDTO findById( MonedaTipoPolizaId id);
	 /**
	 * Find all MonedaTipoPolizaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the MonedaTipoPolizaDTO property to query
	  @param value the property value to match
	  	  @return List<MonedaTipoPolizaDTO> found by query
	 */
	public List<MonedaTipoPolizaDTO> findByProperty(String propertyName, Object value
		);
	
	public List<MonedaTipoPolizaDTO> findByTipoPolizaId(BigDecimal idToTipoPoliza);
	
	/**
	 * Find all MonedaTipoPolizaDTO entities.
	  	  @return List<MonedaTipoPolizaDTO> all MonedaTipoPolizaDTO entities
	 */
	public List<MonedaTipoPolizaDTO> findAll(
		);
	
}