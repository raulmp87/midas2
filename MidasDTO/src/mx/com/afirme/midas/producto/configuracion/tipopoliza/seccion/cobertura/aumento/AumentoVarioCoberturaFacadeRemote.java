package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento;
// default package

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for AumentoVarioCoberturaFacade.
 * @author MyEclipse Persistence Tools
 */


public interface AumentoVarioCoberturaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved AumentoVarioCoberturaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity AumentoVarioCoberturaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(AumentoVarioCoberturaDTO entity);
    /**
	 Delete a persistent AumentoVarioCoberturaDTO entity.
	  @param entity AumentoVarioCoberturaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(AumentoVarioCoberturaDTO entity);
   /**
	 Persist a previously saved AumentoVarioCoberturaDTO entity and return it or a copy of it to the sender. 
	 A copy of the AumentoVarioCoberturaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity AumentoVarioCoberturaDTO entity to update
	 @return AumentoVarioCoberturaDTO the persisted AumentoVarioCoberturaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public AumentoVarioCoberturaDTO update(AumentoVarioCoberturaDTO entity);
	public AumentoVarioCoberturaDTO findById( AumentoVarioCoberturaId id);
	 /**
	 * Find all AumentoVarioCoberturaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the AumentoVarioCoberturaDTO property to query
	  @param value the property value to match
	  	  @return List<AumentoVarioCoberturaDTO> found by query
	 */
	public List<AumentoVarioCoberturaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all AumentoVarioCoberturaDTO entities.
	  	  @return List<AumentoVarioCoberturaDTO> all AumentoVarioCoberturaDTO entities
	 */
	public List<AumentoVarioCoberturaDTO> findAll(
		);	
}