package mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.exclusion;
// default package

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

/**
 * Remote interface for ExclusionRecargoVarioTipoPolizaFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ExclusionRecargoVarioTipoPolizaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ExclusionRecargoVarioTipoPolizaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ExclusionRecargoVarioTipoPolizaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ExclusionRecargoVarioTipoPolizaDTO entity);
    /**
	 Delete a persistent ExclusionRecargoVarioTipoPolizaDTO entity.
	  @param entity ExclusionRecargoVarioTipoPolizaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ExclusionRecargoVarioTipoPolizaDTO entity);
   /**
	 Persist a previously saved ExclusionRecargoVarioTipoPolizaDTO entity and return it or a copy of it to the sender. 
	 A copy of the ExclusionRecargoVarioTipoPolizaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ExclusionRecargoVarioTipoPolizaDTO entity to update
	 @return ExclusionRecargoVarioTipoPolizaDTO the persisted ExclusionRecargoVarioTipoPolizaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ExclusionRecargoVarioTipoPolizaDTO update(ExclusionRecargoVarioTipoPolizaDTO entity);
	public ExclusionRecargoVarioTipoPolizaDTO findById( ExclusionRecargoVarioTipoPolizaId id);
	 /**
	 * Find all ExclusionRecargoVarioTipoPolizaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ExclusionRecargoVarioTipoPolizaDTO property to query
	  @param value the property value to match
	  	  @return List<ExclusionRecargoVarioTipoPolizaDTO> found by query
	 */
	public List<ExclusionRecargoVarioTipoPolizaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ExclusionRecargoVarioTipoPolizaDTO entities.
	  	  @return List<ExclusionRecargoVarioTipoPolizaDTO> all ExclusionRecargoVarioTipoPolizaDTO entities
	 */
	public List<ExclusionRecargoVarioTipoPolizaDTO> findAll(
		);	
	
	/**
	 * Find a ExclusionRecargoVarioTipoPolizaDTO entity with the specific
	 * received id�s.
	 * 
	 * @param BigDecimal idToTipoPoliza.
	 * @param BigDecimal idToRecargoVario.
	 * @param BigDecimal idToCobertura.
	 * 
	 * @return List<ExclusionRecargoVarioTipoPolizaDTO> found by query
	 */
	public List<ExclusionRecargoVarioTipoPolizaDTO> findByIDs(BigDecimal idToTipoPoliza, BigDecimal idToRecargoVario, BigDecimal idToCobertura);
	
	/**
	 * Encuentra los registros de ExclusionRecargoVarioTipoPolizaDTO relacionados con el TipoPolizaDTO cuyo ID se recibe y que adem�s est�n 
	 * relacionados s�lo con las coberturas que no hayan sido borradas l�gicamente.
	  @param BigDecimal idToTipoPoliza. El ID del tipoPoliza
	  @return List<ExclusionRecargoVarioTipoPolizaDTO> encontrados por el query formado.
	 */
    public List<ExclusionRecargoVarioTipoPolizaDTO> getVigentesPorIdTipoPoliza(BigDecimal idToTipoPoliza);
}