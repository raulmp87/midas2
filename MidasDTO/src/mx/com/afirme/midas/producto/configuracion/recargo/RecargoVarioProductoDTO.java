package mx.com.afirme.midas.producto.configuracion.recargo;
// default packageTorecargovario

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDTO;
import mx.com.afirme.midas.producto.ProductoDTO;

/**
 * RecargoPorProductoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity (name = "RecargoVarioProductoDTO")
@Table(name="TORECARGOVARIOPRODUCTO"
    ,schema="MIDAS"
)
public class RecargoVarioProductoDTO  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private RecargoVarioProductoId id;
     private ProductoDTO productoDTO;
     private RecargoVarioDTO recargoVarioDTO;
     private String claveObligatoriedad;
     private String claveComercialTecnico;
     private String claveAplicaReaseguro;
     private Double valor;


    // Constructors

    /** default constructor */
    public RecargoVarioProductoDTO() {
    }
   
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idToProducto", column=@Column(name="IDTOPRODUCTO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idToRecargoVario", column=@Column(name="IDTORECARGOVARIO", nullable=false, precision=22, scale=0) ) } )

    public RecargoVarioProductoId getId() {
        return this.id;
    }

    public void setId(RecargoVarioProductoId id) {
        this.id = id;
    }
        
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="IDTOPRODUCTO", nullable=false, insertable=false, updatable=false)

    public ProductoDTO getProductoDTO() {
        return this.productoDTO;
    }
    
    public void setProductoDTO(ProductoDTO productoDTO) {
        this.productoDTO = productoDTO;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTORECARGOVARIO", nullable=false, insertable=false, updatable=false)

    public RecargoVarioDTO getRecargoVarioDTO() {
        return this.recargoVarioDTO;
    }
    
    public void setRecargoVarioDTO(RecargoVarioDTO recargoVarioDTO) {
        this.recargoVarioDTO = recargoVarioDTO;
    }
    
    @Column(name="CLAVEOBLIGATORIEDAD", nullable=false, precision=4, scale=0)

    public String getClaveObligatoriedad() {
        return this.claveObligatoriedad;
    }
    
    public void setClaveObligatoriedad(String claveObligatoriedad) {
        this.claveObligatoriedad = claveObligatoriedad;
    }
    
    @Column(name="CLAVECOMERCIALTECNICO", nullable=false, precision=4, scale=0)

    public String getClaveComercialTecnico() {
        return this.claveComercialTecnico;
    }
    
    public void setClaveComercialTecnico(String claveComercialTecnico) {
        this.claveComercialTecnico = claveComercialTecnico;
    }
    
    @Column(name="CLAVEAPLICAREASEGURO", nullable=false, precision=4, scale=0)

    public String getClaveAplicaReaseguro() {
        return this.claveAplicaReaseguro;
    }
    
    public void setClaveAplicaReaseguro(String claveAplicaReaseguro) {
        this.claveAplicaReaseguro = claveAplicaReaseguro;
    }
    
    @Column(name="VALOR", nullable=false, precision=16)

    public Double getValor() {
        return this.valor;
    }
    
    public void setValor(Double valor) {
        this.valor = valor;
    }
}