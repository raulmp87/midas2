package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.coaseguro;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for CoaseguroCoberturaFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface CoaseguroCoberturaFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved CoaseguroCoberturaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            CoaseguroCoberturaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(CoaseguroCoberturaDTO entity);

	/**
	 * Delete a persistent CoaseguroCoberturaDTO entity.
	 * 
	 * @param entity
	 *            CoaseguroCoberturaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(CoaseguroCoberturaDTO entity);

	/**
	 * Persist a previously saved CoaseguroCoberturaDTO entity and return it or
	 * a copy of it to the sender. A copy of the CoaseguroCoberturaDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            CoaseguroCoberturaDTO entity to update
	 * @return CoaseguroCoberturaDTO the persisted CoaseguroCoberturaDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public CoaseguroCoberturaDTO update(CoaseguroCoberturaDTO entity);

	public CoaseguroCoberturaDTO findById(CoaseguroCoberturaId id);

	/**
	 * Find all CoaseguroCoberturaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the CoaseguroCoberturaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<CoaseguroCoberturaDTO> found by query
	 */
	public List<CoaseguroCoberturaDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all CoaseguroCoberturaDTO entities.
	 * 
	 * @return List<CoaseguroCoberturaDTO> all CoaseguroCoberturaDTO entities
	 */
	public List<CoaseguroCoberturaDTO> findAll();

	public List<CoaseguroCoberturaDTO> listarCoaseguros(BigDecimal idToCobertura);

	public List<CoaseguroCoberturaDTO> listarCoasegurosPorCobertura(BigDecimal idToCobertura);
	
	public BigDecimal nextNumeroSecuencia(BigDecimal idToCobertura);
}