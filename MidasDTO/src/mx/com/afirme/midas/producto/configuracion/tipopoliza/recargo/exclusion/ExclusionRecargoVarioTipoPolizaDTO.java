package mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.exclusion;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;


/**
 * ExclusionRecargoVarioTipoPolizaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TREXCRECARGOVARTIPOPOLIZA"
    ,schema="MIDAS"
)
public class ExclusionRecargoVarioTipoPolizaDTO  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ExclusionRecargoVarioTipoPolizaId id;
    private RecargoVarioDTO recargoVarioDTO;
    private TipoPolizaDTO tipoPolizaDTO;
    private CoberturaDTO coberturaDTO;


    // Constructors

    /** default constructor */
    public ExclusionRecargoVarioTipoPolizaDTO() {
    }

    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idtotipopoliza", column=@Column(name="IDTOTIPOPOLIZA", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idtorecargovario", column=@Column(name="IDTORECARGOVARIO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idtocobertura", column=@Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0) ) } )

    public ExclusionRecargoVarioTipoPolizaId getId() {
        return this.id;
    }
    
    public void setId(ExclusionRecargoVarioTipoPolizaId id) {
        this.id = id;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTORECARGOVARIO", nullable=false, insertable=false, updatable=false)
    public RecargoVarioDTO getRecargoVarioDTO() {
        return this.recargoVarioDTO;
    }
    
    public void setRecargoVarioDTO(RecargoVarioDTO recargoVarioDTO) {
        this.recargoVarioDTO = recargoVarioDTO;
    }
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTOTIPOPOLIZA", nullable=false, insertable=false, updatable=false)
	public TipoPolizaDTO getTipoPolizaDTO() {
		return tipoPolizaDTO;
	}

	public void setTipoPolizaDTO(TipoPolizaDTO tipoPolizaDTO) {
		this.tipoPolizaDTO = tipoPolizaDTO;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTOCOBERTURA", nullable=false, insertable=false, updatable=false)
	public CoberturaDTO getCoberturaDTO() {
		return coberturaDTO;
	}

	public void setCoberturaDTO(CoberturaDTO coberturaDTO) {
		this.coberturaDTO = coberturaDTO;
	}
	
	public boolean equals(Object o) {
		if (o instanceof ExclusionRecargoVarioTipoPolizaDTO) {
			ExclusionRecargoVarioTipoPolizaDTO temp = (ExclusionRecargoVarioTipoPolizaDTO) o;
			if (temp.getId().getIdtotipopoliza().intValue() == this.getId().getIdtotipopoliza().intValue()
					&& temp.getId().getIdtorecargovario().intValue() == this.getId().getIdtorecargovario().intValue()
					&& temp.getId().getIdtocobertura().intValue() == this.getId().getIdtocobertura().intValue()) {
				return true;
			}
		}
		return false;
	}

	public int hashCode() {
		int hash = 1;
	    hash = hash * 31 + this.getId().getIdtotipopoliza().hashCode();
	    hash = hash * 31 + this.getId().getIdtorecargovario().hashCode();
	    hash = hash * 31 + this.getId().getIdtotipopoliza().hashCode();
	    return hash;
	}
}