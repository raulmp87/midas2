package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura;

// default package

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.GrupoVariablesModificacionPrima;

/**
 * CoberturaSeccionDTO entity. @author Jos� Luis Arellano
 */
@Entity
@Table(name = "TOCOBERTURASECCION", schema = "MIDAS")
public class CoberturaSeccionDTO implements java.io.Serializable, Entidad {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields

	private CoberturaSeccionDTOId id;
	private CoberturaDTO coberturaDTO;
	private SeccionDTO seccionDTO;
	private BigDecimal claveObligatoriedad;
	
	private List<RiesgoCoberturaDTO> riesgos;
	private List <GrupoVariablesModificacionPrima> gruposVariablesModificacionPrimas;

	// Constructors

	/** default constructor */
	public CoberturaSeccionDTO() {
	}
	
	public CoberturaSeccionDTO(CoberturaSeccionDTOId id,
			CoberturaDTO coberturaDTO, SeccionDTO seccionDTO,
			BigDecimal claveObligatoriedad) {
		super();
		this.id = id;
		this.coberturaDTO = coberturaDTO;
		this.seccionDTO = seccionDTO;
		this.claveObligatoriedad = claveObligatoriedad;
	}



	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idtoseccion", column = @Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idtocobertura", column = @Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)) })
	public CoberturaSeccionDTOId getId() {
		return this.id;
	}

	public void setId(CoberturaSeccionDTOId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOCOBERTURA", nullable = false, insertable = false, updatable = false)
	public CoberturaDTO getCoberturaDTO() {
		return this.coberturaDTO;
	}

	public void setCoberturaDTO(CoberturaDTO coberturaDTO) {
		this.coberturaDTO = coberturaDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOSECCION", nullable = false, insertable = false, updatable = false)
	public SeccionDTO getSeccionDTO() {
		return this.seccionDTO;
	}

	public void setSeccionDTO(SeccionDTO seccionDTO) {
		this.seccionDTO = seccionDTO;
	}

	@Column(name = "CLAVEOBLIGATORIEDAD", nullable = false, precision = 4, scale = 0)
	public BigDecimal getClaveObligatoriedad() {
		return this.claveObligatoriedad;
	}

	public void setClaveObligatoriedad(BigDecimal claveObligatoriedad) {
		this.claveObligatoriedad = claveObligatoriedad;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "coberturaSeccionDTO")
	public List<RiesgoCoberturaDTO> getRiesgos() {
		return riesgos;
	}

	public void setRiesgos(List<RiesgoCoberturaDTO> riesgos) {
		this.riesgos = riesgos;
	}
	
	
	@ManyToMany(cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinTable(name = "TRVARMODIFPRIMACOBERTURA", schema = "MIDAS",
		joinColumns = {
			@JoinColumn(name="IDTOCOBERTURA", referencedColumnName="IDTOCOBERTURA"),
			@JoinColumn(name="IDTOSECCION", referencedColumnName="IDTOSECCION")	
		},
		inverseJoinColumns = {
			@JoinColumn(name="IDTCGRUPOVARMODIFPRIMA", referencedColumnName="IDTCGRUPOVARMODIFPRIMA")
		}
	)
	public List<GrupoVariablesModificacionPrima> getGruposVariablesModificacionPrimas() {
		return gruposVariablesModificacionPrimas;
	}


	public void setGruposVariablesModificacionPrimas(
			List<GrupoVariablesModificacionPrima> gruposVariablesModificacionPrimas) {
		this.gruposVariablesModificacionPrimas = gruposVariablesModificacionPrimas;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CoberturaSeccionDTOId getKey() {		
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CoberturaSeccionDTOId getBusinessKey() {
		return id;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("id: " + this.id + ", ");
		sb.append("seccionDTO: " + this.seccionDTO + ", ");
		sb.append("coberturaDTO: " + this.coberturaDTO + ", ");
		sb.append("]");
		
		return sb.toString();
	}
}