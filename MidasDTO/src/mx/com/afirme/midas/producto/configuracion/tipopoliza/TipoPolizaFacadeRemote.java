package mx.com.afirme.midas.producto.configuracion.tipopoliza;

// default package

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

/**
 * Remote interface for TipoPolizaDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface TipoPolizaFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved TipoPolizaDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            TipoPolizaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoPolizaDTO entity);

	/**
	 * Delete a persistent TipoPolizaDTO entity.
	 * 
	 * @param entity
	 *            TipoPolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoPolizaDTO entity);

	/**
	 * Persist a previously saved TipoPolizaDTO entity and return it or a copy
	 * of it to the sender. A copy of the TipoPolizaDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoPolizaDTO entity to update
	 * @return TipoPolizaDTO the persisted TipoPolizaDTO entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoPolizaDTO update(TipoPolizaDTO entity);

	public TipoPolizaDTO findById(BigDecimal id);

	/**
	 * Find all TipoPolizaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the TipoPolizaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoPolizaDTO> found by query
	 */
	public List<TipoPolizaDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all TipoPolizaDTO entities.
	 * 
	 * @return List<TipoPolizaDTO> all TipoPolizaDTO entities
	 */
	public List<TipoPolizaDTO> findAll();

	/**
	 * Find ProductoDTO entities with a child id value.
	 * 
	 * @param id
	 *            the child id
	 * @return ProductoDTO found by query
	 */
	public TipoPolizaDTO findByChildId(BigDecimal id);
	
	/**
	 * Borra l�gicamente un registro de TipoPoliza, estableciendo en 0 los campos 
	 * CLAVEACTIVO y CLAVEESTATUS
	 * 
	 * @param entity
	 *            TipoPolizaDTO entity el registro Riesgo a borrar
	 * @return TipoPolizaDTO the persisted TipoPolizaDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             si la operacion falla
	 */
	public TipoPolizaDTO borradoLogico(TipoPolizaDTO entity);
	
	/**
	 * Busca los registros de TipoPoliza que no hayan sido borrados logicamente
	 * 
	 * @return List<TipoPolizaDTO> registros de TipoPoliza que no han sido borrados logicamente
	 */
	public List<TipoPolizaDTO> listarVigentes();
	
	/**
	 * Busca los registros de TipoPoliza que no hayan sido borrados logicamente
	 * y que pertenezcan a un producto.
	 * 
	 * @return List<TipoPolizaDTO> registros de TipoPoliza que no han sido borrados logicamente
	 * 		pertenecientes al producto cuyo ID se recibe.
	 * @param 
	 */
	public List<TipoPolizaDTO> listarVigentesPorIdProducto(BigDecimal idToProducto);
	
	
	public List<TipoPolizaDTO> listarPorIdProducto(BigDecimal idToProducto, Boolean verInactivos, Boolean soloActivos);
	
	/**
	 * Find a tipoPolizaDTO entity wich is related with the given cotizacionDTO id.
	 * @param idToCotizacion CotizacionDTO entity id
	 * @return TipoPolizaDTO entity
	 */
	public TipoPolizaDTO findTipoPolizaByCotizacion(BigDecimal idToCotizacion);

	/**
	 * Busca los registros de TipoPoliza (activas e inactivas) que pertenezcan a un Producto.
	 * 
	 * @param idToProducto identificador del Producto
	 * @return lista de TipoPoliza (activas e inactivas)
	 */
	public List<TipoPolizaDTO> listarPorIdProducto(BigDecimal idToProducto);
}