package mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * AumentoVarioTipoPolizaId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class AumentoVarioTipoPolizaId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idtotipopoliza;
     private BigDecimal idtoaumentovario;


    // Constructors

    /** default constructor */
    public AumentoVarioTipoPolizaId() {
    }

    
    /** full constructor */
    public AumentoVarioTipoPolizaId(BigDecimal idtotipopoliza, BigDecimal idtoaumentovario) {
        this.idtotipopoliza = idtotipopoliza;
        this.idtoaumentovario = idtoaumentovario;
    }

   
    // Property accessors

    @Column(name="IDTOTIPOPOLIZA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtotipopoliza() {
        return this.idtotipopoliza;
    }
    
    public void setIdtotipopoliza(BigDecimal idtotipopoliza) {
        this.idtotipopoliza = idtotipopoliza;
    }

    @Column(name="IDTOAUMENTOVARIO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtoaumentovario() {
        return this.idtoaumentovario;
    }
    
    public void setIdtoaumentovario(BigDecimal idtoaumentovario) {
        this.idtoaumentovario = idtoaumentovario;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof AumentoVarioTipoPolizaId) ) return false;
		 AumentoVarioTipoPolizaId castOther = ( AumentoVarioTipoPolizaId ) other; 
         
		 return ( (this.getIdtotipopoliza()==castOther.getIdtotipopoliza()) || ( this.getIdtotipopoliza()!=null && castOther.getIdtotipopoliza()!=null && this.getIdtotipopoliza().equals(castOther.getIdtotipopoliza()) ) )
 && ( (this.getIdtoaumentovario()==castOther.getIdtoaumentovario()) || ( this.getIdtoaumentovario()!=null && castOther.getIdtoaumentovario()!=null && this.getIdtoaumentovario().equals(castOther.getIdtoaumentovario()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdtotipopoliza() == null ? 0 : this.getIdtotipopoliza().hashCode() );
         result = 37 * result + ( getIdtoaumentovario() == null ? 0 : this.getIdtoaumentovario().hashCode() );
         return result;
   }   





}