package mx.com.afirme.midas.producto.configuracion.recargo.exclusion;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;

/**
 * ExcRecargoVarioPorProducto entity. @author José Luis Arellano
 */
@Entity
@Table(name="TREXCRECARGOVARPRODUCTO",schema="MIDAS")
public class ExclusionRecargoVarioProductoDTO implements java.io.Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ExclusionRecargoVarioProductoId id;
    private ProductoDTO productoDTO;
    private TipoPolizaDTO tipoPolizaDTO;
    private RecargoVarioDTO RecargoVarioDTO;
    
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idtoproducto", column=@Column(name="IDTOPRODUCTO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idtorecargovario", column=@Column(name="IDTORECARGOVARIO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idtotipopoliza", column=@Column(name="IDTOTIPOPOLIZA", nullable=false, precision=22, scale=0) ) } )

    public ExclusionRecargoVarioProductoId getId() {
        return this.id;
    }
    
    public void setId(ExclusionRecargoVarioProductoId id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTOPRODUCTO", nullable=false, insertable=false, updatable=false)
    public ProductoDTO getProductoDTO() {
		return productoDTO;
	}

	public void setProductoDTO(ProductoDTO productoDTO) {
		this.productoDTO = productoDTO;
	}
    
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTOTIPOPOLIZA", nullable=false, insertable=false, updatable=false)
    public TipoPolizaDTO getTipoPolizaDTO() {
		return tipoPolizaDTO;
	}

	public void setTipoPolizaDTO(TipoPolizaDTO tipoPolizaDTO) {
		this.tipoPolizaDTO = tipoPolizaDTO;
	}
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTORECARGOVARIO", nullable=false, insertable=false, updatable=false)
	public RecargoVarioDTO getRecargoVarioDTO() {
		return RecargoVarioDTO;
	}

	public void setRecargoVarioDTO(RecargoVarioDTO recargoVarioDTO) {
		RecargoVarioDTO = recargoVarioDTO;
	}
	
	public boolean equals(Object o) {
		if (o instanceof ExclusionRecargoVarioProductoDTO) {
			ExclusionRecargoVarioProductoDTO temp = (ExclusionRecargoVarioProductoDTO) o;
			if (temp.getId().getIdtoproducto().intValue() == this.getId().getIdtoproducto().intValue()
					&& temp.getId().getIdtorecargovario().intValue() == this.getId().getIdtorecargovario().intValue()
					&& temp.getId().getIdtotipopoliza().intValue() == this.getId().getIdtotipopoliza().intValue()) {
				return true;
			}
		}
		return false;
	}

	public int hashCode() {
		int hash = 1;
	    hash = hash * 31 + this.getId().getIdtoproducto().hashCode();
	    hash = hash * 31 + this.getId().getIdtorecargovario().hashCode();
	    hash = hash * 31 + this.getId().getIdtotipopoliza().hashCode();
	    return hash;
	}
}
