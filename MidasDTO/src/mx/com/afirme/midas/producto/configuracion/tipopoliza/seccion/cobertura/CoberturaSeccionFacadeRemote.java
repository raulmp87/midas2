package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for CoberturaSeccionDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface CoberturaSeccionFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved CoberturaSeccionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CoberturaSeccionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CoberturaSeccionDTO entity);
    /**
	 Delete a persistent CoberturaSeccionDTO entity.
	  @param entity CoberturaSeccionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CoberturaSeccionDTO entity);
   /**
	 Persist a previously saved CoberturaSeccionDTO entity and return it or a copy of it to the sender. 
	 A copy of the CoberturaSeccionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CoberturaSeccionDTO entity to update
	 @return CoberturaSeccionDTO the persisted CoberturaSeccionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CoberturaSeccionDTO update(CoberturaSeccionDTO entity);
	public CoberturaSeccionDTO findById( CoberturaSeccionDTOId id);
	 /**
	 * Find all CoberturaSeccionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the CoberturaSeccionDTO property to query
	  @param value the property value to match
	  	  @return List<CoberturaSeccionDTO> found by query
	 */
	public List<CoberturaSeccionDTO> findByProperty(String propertyName, Object value);
	public List<CoberturaSeccionDTO> findByClaveobligatoriedad(Object claveobligatoriedad);
	/**
	 * Find all CoberturaSeccionDTO entities.
	  	  @return List<CoberturaSeccionDTO> all CoberturaSeccionDTO entities
	 */
	public List<CoberturaSeccionDTO> findAll();
	
	/**
	 * Lista todas las coberturas que no est�n asociadas una secci�n determinada 
	 * por el id que recibe el m�todo
	 * @param BigDecimal idToSeccion el id de la seccion.
	 * @return List<CoberturaSeccionDTO> registros CoberturaSeccion que no est�n asociados a la seccion.
	 */
	public List<CoberturaSeccionDTO> obtenerCoberturasSinAsociar(BigDecimal idToSeccion);
	
	/**
	 * Encuentra todas las entidades de CoberturaSeccionDTO relacionadas con la Seccion cuyo ID se recibe, y que adem�s est�n relacionadas
	 * s�lo con coberturas que no han sido borradas l�gicamente.  
	  @param idToSeccion
  	  @return List<CoberturaSeccionDTO> lista de registros encontrados por el query formado.
	 */
    public List<CoberturaSeccionDTO> getVigentesPorSeccion(BigDecimal idToSeccion);

    public Long countCobeturaSeccion(BigDecimal idToCobertura);
    
    
    /**
     * Este metodo regresa todas las CoberturaSeccion que por suma asegurada requieren a la actual idCobertura, idSeccion.
     * Es decir todas las coberturas que necesitan que una CoberturaSeccion este presente (Dependientes).
     * @return
     */
    public List<CoberturaSeccionDTO> listCoberturaSumaAseguradaDependientes(BigDecimal idCoberturaSumaAsegurada, BigDecimal idSeccion);
    
    /**
     * Consulta las entidades CoberturaSeccionDTO que coincidan con el objeto filtro recibido.
     * @param entity
     * @return
     */
    public List<CoberturaSeccionDTO> listarFiltrado(CoberturaSeccionDTO entity);
}