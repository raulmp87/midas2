package mx.com.afirme.midas.producto.configuracion.tipopoliza;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.configuracion.aumento.exclusion.ExclusionAumentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.recargo.exclusion.ExclusionRecargoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento.AumentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento.exclusion.ExclusionAumentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento.DescuentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.moneda.MonedaTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.PaquetePolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo.RamoTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.RecargoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.exclusion.ExclusionRecargoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * TipoPolizaDTO entity. @author Jorge Cano
 */
@Entity(name = "TipoPolizaDTO")
@Table(name = "TOTIPOPOLIZA", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = {
		"CODIGOTIPOPOLIZA", "VERSIONTIPOPOLIZA" }))
public class TipoPolizaDTO implements java.io.Serializable, Entidad {

	
	public static final BigDecimal ID_CASA_AFIRME_PLUS_AGENTES = new BigDecimal("1310");
	public static final BigDecimal ID_CASA_AFIRME_PLUS_BANCA = new BigDecimal("1311");
	public static final BigDecimal ID_CASA_AFIRME_PLUS_EMPLEADOS = new BigDecimal("1317");
	public static final BigDecimal ID_CASA_AFIRME_PLUS = new BigDecimal("1308");
	
	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToTipoPoliza;
	private ProductoDTO productoDTO;
	private String codigo;
	private Integer version;
	private String descripcion;
	private String nombreComercial;
	private Integer claveRenovable;
	private Integer clavePagoInmediato;
	private Integer diasRetroactividad;
	private Integer diasDiferimiento;
	private Integer diasGracia;
	/*private Integer claveActivoConfiguracion;
	private Integer claveActivoProduccion;*/
	private Integer idTipoCalculoEmision;
	private Integer idTipoCalculoCancelacion;
	// private String descripcionRegistroCNSF;
	// private Integer idControlArchivoRegistroCNSF;
	private String claveProdServ;
	
	private Short claveEstatus;
	private Short claveActivo;
	private Date fechaCreacion;
	private Date fechaModificacion;
    private String codigoUsuarioCreacion;
	private String codigoUsuarioModificacion;
	
	private BigDecimal idToPaqueteDefault;

	private List<SeccionDTO> secciones = new ArrayList<SeccionDTO>();
	private List<ExclusionRecargoVarioProductoDTO> exclusionRecargoVarioProductoDTOs = new ArrayList<ExclusionRecargoVarioProductoDTO>();
	private List<ExclusionRecargoVarioTipoPolizaDTO> exclusionRecargoVarioTipoPolizaDTOs = new ArrayList<ExclusionRecargoVarioTipoPolizaDTO>();
	private List<RecargoVarioTipoPolizaDTO> recargoVarioTipoPolizaDTO = new ArrayList<RecargoVarioTipoPolizaDTO>();
	private List<ExclusionAumentoVarioTipoPolizaDTO> exclusionAumentoVarioTipoPolizaDTOs = new ArrayList<ExclusionAumentoVarioTipoPolizaDTO>();
	private List<ExclusionAumentoVarioProductoDTO> exclusionAumentoVarioProductoDTOs = new ArrayList<ExclusionAumentoVarioProductoDTO>();
	private List<AumentoVarioTipoPolizaDTO> aumentoVarioTipoPolizaDTOs = new ArrayList<AumentoVarioTipoPolizaDTO>();

	private List<RamoTipoPolizaDTO> ramoTipoPolizaList = new ArrayList<RamoTipoPolizaDTO>();
	private List<DescuentoVarioTipoPolizaDTO> descuentos = new ArrayList<DescuentoVarioTipoPolizaDTO>();
	private List<MonedaTipoPolizaDTO> monedas = new ArrayList<MonedaTipoPolizaDTO>();
	
	private List<PaquetePolizaDTO> paquetesPolizaDTO;
	
	private Integer diasGraciaSubsecuentes;
	
	private Integer claveAplicaFlotillas;
	private Integer claveAplicaAutoexpedible;
	
	private Date fechaInicioVigencia;

	// Constructors

	/** default constructor */
	public TipoPolizaDTO() {
		if (this.productoDTO == null) {
			this.productoDTO = new ProductoDTO();
		}
		this.claveEstatus=new Short("1");
		this.claveActivo= new Short("1");
		this.fechaCreacion=new Date();
		this.fechaModificacion=new Date();
		this.codigoUsuarioCreacion="enDB";
		this.codigoUsuarioModificacion="enDB";
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTOTIPOPOLIZA_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOTIPOPOLIZA_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOTIPOPOLIZA_SEQ_GENERADOR")
	@Column(name = "IDTOTIPOPOLIZA", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToTipoPoliza() {
		return this.idToTipoPoliza;
	}

	public void setIdToTipoPoliza(BigDecimal idToTipoPoliza) {
		this.idToTipoPoliza = idToTipoPoliza;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOPRODUCTO", nullable = false)
	public ProductoDTO getProductoDTO() {
		return this.productoDTO;
	}

	public void setProductoDTO(ProductoDTO productoDTO) {
		this.productoDTO = productoDTO;
	}

	@Column(name = "CODIGOTIPOPOLIZA", nullable = false, length = 8)
	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Column(name = "VERSIONTIPOPOLIZA", nullable = false)
	public Integer getVersion() {
		return this.version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	@Column(name = "DESCRIPCIONTIPOPOLIZA", nullable = false, length = 200)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "NOMBRECOMERCIALTIPOPOLIZA", nullable = false, length = 100)
	public String getNombreComercial() {
		return this.nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}

	@Column(name = "CLAVERENOVABLE", nullable = false, precision = 4, scale = 0)
	public Integer getClaveRenovable() {
		return claveRenovable;
	}

	public void setClaveRenovable(Integer claveRenovable) {
		this.claveRenovable = claveRenovable;
	}

	@Column(name = "CLAVEPAGOINMEDIATO", nullable = false, precision = 4, scale = 0)
	public Integer getClavePagoInmediato() {
		return this.clavePagoInmediato;
	}

	public void setClavePagoInmediato(Integer clavePagoInmediato) {
		this.clavePagoInmediato = clavePagoInmediato;
	}

	@Column(name = "DIASRETROACTIVIDAD", nullable = false, precision = 22, scale = 0)
	public Integer getDiasRetroactividad() {
		return this.diasRetroactividad;
	}

	public void setDiasRetroactividad(Integer diasRetroactividad) {
		this.diasRetroactividad = diasRetroactividad;
	}

	@Column(name = "DIASDIFERIMIENTO", nullable = false, precision = 22, scale = 0)
	public Integer getDiasDiferimiento() {
		return this.diasDiferimiento;
	}

	public void setDiasDiferimiento(Integer diasDiferimiento) {
		this.diasDiferimiento = diasDiferimiento;
	}

	@Column(name = "DIASGRACIA", nullable = false, precision = 22, scale = 0)
	public Integer getDiasGracia() {
		return this.diasGracia;
	}

	public void setDiasGracia(Integer diasGracia) {
		this.diasGracia = diasGracia;
	}

	/*@Column(name = "CLAVEACTIVOCONFIGURACION", nullable = false, precision = 4, scale = 0)
	public Integer getClaveActivoConfiguracion() {
		return this.claveActivoConfiguracion;
	}

	public void setClaveActivoConfiguracion(Integer claveActivoConfiguracion) {
		this.claveActivoConfiguracion = claveActivoConfiguracion;
	}*/

	/*@Column(name = "CLAVEACTIVOPRODUCCION", nullable = false, precision = 4, scale = 0)
	public Integer getClaveActivoProduccion() {
		return this.claveActivoProduccion;
	}

	public void setClaveActivoProduccion(Integer claveActivoProduccion) {
		this.claveActivoProduccion = claveActivoProduccion;
	}*/

	@Column(name = "CLAVEESTATUS", nullable = false, precision = 4, scale = 0)
	public Short getClaveEstatus() {
		return claveEstatus;
	}

	public void setClaveEstatus(Short claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	@Column(name = "CLAVEACTIVO", nullable = false, precision = 4, scale = 0)
	public Short getClaveActivo() {
		return claveActivo;
	}

	public void setClaveActivo(Short claveActivo) {
		this.claveActivo = claveActivo;
	}

	@Temporal(TemporalType.DATE)
    @Column(name="FECHACREACION", length=7)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Temporal(TemporalType.DATE)
    @Column(name="FECHAMODIFICACION", length=7)
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Column(name="CODIGOUSUARIOCREACION", nullable=false, length=8)
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	
	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
	
	@Column(name="CODIGOUSUARIOMODIFICACION", length=8)
	public String getCodigoUsuarioModificacion() {
		return codigoUsuarioModificacion;
	}	
	
	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tipoPolizaDTO")
	public List<SeccionDTO> getSecciones() {
		return this.secciones;
	}

	public void setSecciones(List<SeccionDTO> secciones) {
		this.secciones = secciones;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tipoPolizaDTO")
	public List<ExclusionRecargoVarioProductoDTO> getExclusionRecargoVarioProductoDTOs() {
		return exclusionRecargoVarioProductoDTOs;
	}

	public void setExclusionRecargoVarioProductoDTOs(
			List<ExclusionRecargoVarioProductoDTO> exclusionRecargoVarioProductoDTOs) {
		this.exclusionRecargoVarioProductoDTOs = exclusionRecargoVarioProductoDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tipoPolizaDTO")
	public List<ExclusionRecargoVarioTipoPolizaDTO> getExclusionRecargoVarioTipoPolizaDTOs() {
		return exclusionRecargoVarioTipoPolizaDTOs;
	}

	public void setExclusionRecargoVarioTipoPolizaDTOs(
			List<ExclusionRecargoVarioTipoPolizaDTO> exclusionRecargoVarioTipoPolizaDTOs) {
		this.exclusionRecargoVarioTipoPolizaDTOs = exclusionRecargoVarioTipoPolizaDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tipoPolizaDTO")
	public List<RecargoVarioTipoPolizaDTO> getRecargoVarioTipoPolizaDTO() {
		return recargoVarioTipoPolizaDTO;
	}

	public void setRecargoVarioTipoPolizaDTO(
			List<RecargoVarioTipoPolizaDTO> recargoVarioTipoPolizaDTO) {
		this.recargoVarioTipoPolizaDTO = recargoVarioTipoPolizaDTO;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tipoPolizaDTO")
	public List<ExclusionAumentoVarioTipoPolizaDTO> getExclusionAumentoVarioTipoPolizaDTOs() {
		return exclusionAumentoVarioTipoPolizaDTOs;
	}

	public void setExclusionAumentoVarioTipoPolizaDTOs(
			List<ExclusionAumentoVarioTipoPolizaDTO> exclusionAumentoVarioTipoPolizaDTOs) {
		this.exclusionAumentoVarioTipoPolizaDTOs = exclusionAumentoVarioTipoPolizaDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tipoPolizaDTO")
	public List<ExclusionAumentoVarioProductoDTO> getExclusionAumentoVarioProductoDTOs() {
		return exclusionAumentoVarioProductoDTOs;
	}

	public void setExclusionAumentoVarioProductoDTOs(
			List<ExclusionAumentoVarioProductoDTO> exclusionAumentoVarioProductoDTOs) {
		this.exclusionAumentoVarioProductoDTOs = exclusionAumentoVarioProductoDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tipoPolizaDTO")
	public List<AumentoVarioTipoPolizaDTO> getAumentoVarioTipoPolizaDTOs() {
		return aumentoVarioTipoPolizaDTOs;
	}

	public void setAumentoVarioTipoPolizaDTOs(
			List<AumentoVarioTipoPolizaDTO> aumentoVarioTipoPolizaDTOs) {
		this.aumentoVarioTipoPolizaDTOs = aumentoVarioTipoPolizaDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tipoPolizaDTO")
	public List<RamoTipoPolizaDTO> getRamoTipoPolizaList() {
		return ramoTipoPolizaList;
	}

	public void setRamoTipoPolizaList(List<RamoTipoPolizaDTO> ramoTipoPolizaList) {
		this.ramoTipoPolizaList = ramoTipoPolizaList;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tipoPolizaDTO")
	public List<DescuentoVarioTipoPolizaDTO> getDescuentos() {
		return descuentos;
	}

	public void setDescuentos(List<DescuentoVarioTipoPolizaDTO> descuentos) {
		this.descuentos = descuentos;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tipoPolizaDTO")
	public List<MonedaTipoPolizaDTO> getMonedas() {
		return monedas;
	}

	public void setMonedas(List<MonedaTipoPolizaDTO> monedas) {
		this.monedas = monedas;
	}

	@Column(name = "IDTIPOCALCULOEMISION")
	public Integer getIdTipoCalculoEmision() {
		return idTipoCalculoEmision;
	}

	public void setIdTipoCalculoEmision(Integer idTipoCalculoEmision) {
		this.idTipoCalculoEmision = idTipoCalculoEmision;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tipoPolizaDTO")
	public List<PaquetePolizaDTO> getPaquetesPolizaDTO() {
		return paquetesPolizaDTO;
	}

	public void setPaquetesPolizaDTO(List<PaquetePolizaDTO> paquetesPolizaDTO) {
		this.paquetesPolizaDTO = paquetesPolizaDTO;
	}

	@Column(name = "IDTIPOCALCULOCANCELACION")
	public Integer getIdTipoCalculoCancelacion() {
		return idTipoCalculoCancelacion;
	}

	public void setIdTipoCalculoCancelacion(Integer idTipoCalculoCancelacion) {
		this.idTipoCalculoCancelacion = idTipoCalculoCancelacion;
	}

	@Column(name = "IDTOPAQUETEDEFAULT")
	public BigDecimal getIdToPaqueteDefault() {
		return idToPaqueteDefault;
	}

	public void setIdToPaqueteDefault(BigDecimal idToPaqueteDefault) {
		this.idToPaqueteDefault = idToPaqueteDefault;
	}

	@Column(name = "DIASGRACIASUBSECUENTES")
	public Integer getDiasGraciaSubsecuentes() {
		return diasGraciaSubsecuentes;
	}

	public void setDiasGraciaSubsecuentes(Integer diasGraciaSubsecuentes) {
		this.diasGraciaSubsecuentes = diasGraciaSubsecuentes;
	}

	@Column(name = "CLAVEAPLICAFLOTILLAS")
	public Integer getClaveAplicaFlotillas() {
		return claveAplicaFlotillas;
	}

	public void setClaveAplicaFlotillas(Integer claveAplicaFlotillas) {
		this.claveAplicaFlotillas = claveAplicaFlotillas;
	}

	@Column(name = "CLAVEAPLICAAUTOEXPEDIBLE")
	public Integer getClaveAplicaAutoexpedible() {
		return claveAplicaAutoexpedible;
	}

	public void setClaveAplicaAutoexpedible(Integer claveAplicaAutoexpedible) {
		this.claveAplicaAutoexpedible = claveAplicaAutoexpedible;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAINICIOVIGENCIA", nullable = false, length = 7)
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	
	/**
	 * @return the claveProdServ
	 */
	@Column(name = "CLAVEPRODSERV")
	public String getClaveProdServ() {
		return claveProdServ;
	}

	/**
	 * @param claveProdServ the claveProdServ to set
	 */
	public void setClaveProdServ(String claveProdServ) {
		this.claveProdServ = claveProdServ;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.getIdToTipoPoliza();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoPolizaDTO other = (TipoPolizaDTO) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	/*
	 * @Column(name = "DESCRIPCIONREGISTROCNSF") public String
	 * getDescripcionRegistroCNSF() { return descripcionRegistroCNSF; }
	 * 
	 * public void setDescripcionRegistroCNSF(String descripcionRegistroCNSF) {
	 * this.descripcionRegistroCNSF = descripcionRegistroCNSF; }
	 * 
	 * @Column(name = "IDCTRLARCHREGISTROCNSF") public Integer
	 * getIdControlArchivoRegistroCNSF() { return idControlArchivoRegistroCNSF;
	 * } public void setIdControlArchivoRegistroCNSF(Integer
	 * idControlArchivoRegistroCNSF) { this.idControlArchivoRegistroCNSF =
	 * idControlArchivoRegistroCNSF; }
	 */
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("IdToTipoPoliza: " + this.getIdToTipoPoliza() + ", ");
		sb.append("ProductoDTO: " + this.getProductoDTO());
		sb.append("]");
		
		return sb.toString();
	}
}