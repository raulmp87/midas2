package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.exclusion;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDTO;


/**
 * ExclusionRecargoVarioCoberturaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TREXCRECARGOVARCOBERTURA"
    ,schema="MIDAS"
)
public class ExclusionRecargoVarioCoberturaDTO  implements java.io.Serializable {


     /**
	 * @author Christian Ceballos
	 */
	private static final long serialVersionUID = 1L;
	private ExclusionRecargoVarioCoberturaId id;
    private RecargoVarioDTO recargoVarioDTO;
    private RiesgoDTO riesgoDTO;
    private CoberturaDTO coberturaDTO;


   

	/** default constructor */
    public ExclusionRecargoVarioCoberturaDTO() {
    }

    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idtocobertura", column=@Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idtorecargovario", column=@Column(name="IDTORECARGOVARIO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idtoriesgo", column=@Column(name="IDTORIESGO", nullable=false, precision=22, scale=0) ) } )

    public ExclusionRecargoVarioCoberturaId getId() {
        return this.id;
    }
    
    public void setId(ExclusionRecargoVarioCoberturaId id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTORECARGOVARIO", nullable=false, insertable=false, updatable=false)

    public RecargoVarioDTO getRecargoVarioDTO() {
        return this.recargoVarioDTO;
    }
    
    public void setRecargoVarioDTO(RecargoVarioDTO recargoVarioDTO) {
        this.recargoVarioDTO = recargoVarioDTO;
    }
	
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTORIESGO", nullable=false, insertable=false, updatable=false)
    public RiesgoDTO getRiesgoDTO() {
		return riesgoDTO;
	}

	public void setRiesgoDTO(RiesgoDTO riesgoDTO) {
		this.riesgoDTO = riesgoDTO;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTOCOBERTURA", nullable=false, insertable=false, updatable=false)
	public CoberturaDTO getCoberturaDTO() {
		return coberturaDTO;
	}

	public void setCoberturaDTO(CoberturaDTO coberturaDTO) {
		this.coberturaDTO = coberturaDTO;
	}

	public boolean equals(Object o) {
		if (o instanceof ExclusionRecargoVarioCoberturaDTO) {
			ExclusionRecargoVarioCoberturaDTO temp = (ExclusionRecargoVarioCoberturaDTO) o;
			if (temp.getId().getIdtocobertura().intValue() == this.getId().getIdtocobertura().intValue()
					&& temp.getId().getIdtorecargovario().intValue() == this.getId().getIdtorecargovario().intValue()
					&& temp.getId().getIdtoriesgo().intValue() == this.getId().getIdtoriesgo().intValue()) {
				return true;
			}
		}
		return false;
	}

	public int hashCode() {
		int hash = 1;
	    hash = hash * 31 + this.getId().getIdtocobertura().hashCode();
	    hash = hash * 31 + this.getId().getIdtorecargovario().hashCode();
	    hash = hash * 31 + this.getId().getIdtoriesgo().hashCode();
	    return hash;
	}
}