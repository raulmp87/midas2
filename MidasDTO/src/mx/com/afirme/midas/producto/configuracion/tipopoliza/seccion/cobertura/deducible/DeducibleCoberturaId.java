package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.deducible;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * DeducibleCoberturaId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class DeducibleCoberturaId implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToCobertura;
	private BigDecimal numeroSecuencia;

	// Constructors

	/** default constructor */
	public DeducibleCoberturaId() {
	}

	/** full constructor */
	public DeducibleCoberturaId(BigDecimal idToCobertura,
			BigDecimal numeroSecuencia) {
		this.idToCobertura = idToCobertura;
		this.numeroSecuencia = numeroSecuencia;
	}

	// Property accessors

	@Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCobertura() {
		return this.idToCobertura;
	}

	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	@Column(name = "NUMEROSECUENCIA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getNumeroSecuencia() {
		return this.numeroSecuencia;
	}

	public void setNumeroSecuencia(BigDecimal numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof DeducibleCoberturaId))
			return false;
		DeducibleCoberturaId castOther = (DeducibleCoberturaId) other;

		return ((this.getIdToCobertura() == castOther.getIdToCobertura()) || (this
				.getIdToCobertura() != null
				&& castOther.getIdToCobertura() != null && this
				.getIdToCobertura().equals(castOther.getIdToCobertura())))
				&& ((this.getNumeroSecuencia() == castOther
						.getNumeroSecuencia()) || (this.getNumeroSecuencia() != null
						&& castOther.getNumeroSecuencia() != null && this
						.getNumeroSecuencia().equals(
								castOther.getNumeroSecuencia())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToCobertura() == null ? 0 : this.getIdToCobertura()
						.hashCode());
		result = 37
				* result
				+ (getNumeroSecuencia() == null ? 0 : this.getNumeroSecuencia()
						.hashCode());
		return result;
	}

}