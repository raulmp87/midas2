package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * RecargoVarioCoberturaId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class RecargoVarioCoberturaId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idtocobertura;
     private BigDecimal idtorecargovario;


    // Constructors

    /** default constructor */
    public RecargoVarioCoberturaId() {
    }

    
    /** full constructor */
    public RecargoVarioCoberturaId(BigDecimal idtocobertura, BigDecimal idtorecargovario) {
        this.idtocobertura = idtocobertura;
        this.idtorecargovario = idtorecargovario;
    }

   
    // Property accessors

    @Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtocobertura() {
        return this.idtocobertura;
    }
    
    public void setIdtocobertura(BigDecimal idtocobertura) {
        this.idtocobertura = idtocobertura;
    }

    @Column(name="IDTORECARGOVARIO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtorecargovario() {
        return this.idtorecargovario;
    }
    
    public void setIdtorecargovario(BigDecimal idtorecargovario) {
        this.idtorecargovario = idtorecargovario;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof RecargoVarioCoberturaId) ) return false;
		 RecargoVarioCoberturaId castOther = ( RecargoVarioCoberturaId ) other; 
         
		 return ( (this.getIdtocobertura()==castOther.getIdtocobertura()) || ( this.getIdtocobertura()!=null && castOther.getIdtocobertura()!=null && this.getIdtocobertura().equals(castOther.getIdtocobertura()) ) )
 && ( (this.getIdtorecargovario()==castOther.getIdtorecargovario()) || ( this.getIdtorecargovario()!=null && castOther.getIdtorecargovario()!=null && this.getIdtorecargovario().equals(castOther.getIdtorecargovario()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdtocobertura() == null ? 0 : this.getIdtocobertura().hashCode() );
         result = 37 * result + ( getIdtorecargovario() == null ? 0 : this.getIdtorecargovario().hashCode() );
         return result;
   }   





}