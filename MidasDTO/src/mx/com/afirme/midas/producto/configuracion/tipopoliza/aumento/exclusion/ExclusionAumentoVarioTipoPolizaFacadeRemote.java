package mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento.exclusion;


import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;


/**
 * Remote interface for ExclusionAumentoVarioTipoPolizaFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ExclusionAumentoVarioTipoPolizaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ExclusionAumentoVarioTipoPolizaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ExclusionAumentoVarioTipoPolizaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ExclusionAumentoVarioTipoPolizaDTO entity);
    /**
	 Delete a persistent ExclusionAumentoVarioTipoPolizaDTO entity.
	  @param entity ExclusionAumentoVarioTipoPolizaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ExclusionAumentoVarioTipoPolizaDTO entity);
   /**
	 Persist a previously saved ExclusionAumentoVarioTipoPolizaDTO entity and return it or a copy of it to the sender. 
	 A copy of the ExclusionAumentoVarioTipoPolizaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ExclusionAumentoVarioTipoPolizaDTO entity to update
	 @return ExclusionAumentoVarioTipoPolizaDTO the persisted ExclusionAumentoVarioTipoPolizaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ExclusionAumentoVarioTipoPolizaDTO update(ExclusionAumentoVarioTipoPolizaDTO entity);
	public ExclusionAumentoVarioTipoPolizaDTO findById( ExclusionAumentoVarioTipoPolizaId id);
	 /**
	 * Find all ExclusionAumentoVarioTipoPolizaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ExclusionAumentoVarioTipoPolizaDTO property to query
	  @param value the property value to match
	  	  @return List<ExclusionAumentoVarioTipoPolizaDTO> found by query
	 */
	public List<ExclusionAumentoVarioTipoPolizaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ExclusionAumentoVarioTipoPolizaDTO entities.
	  	  @return List<ExclusionAumentoVarioTipoPolizaDTO> all ExclusionAumentoVarioTipoPolizaDTO entities
	 */
	public List<ExclusionAumentoVarioTipoPolizaDTO> findAll(
		);	
	
	/**
	 * Find a ExclusionAumentoVarioTipoPolizaDTO entity with the specific
	 * received id�s.
	 * 
	 * @param BigDecimal idToTipoPoliza.
	 * @param BigDecimal idToAumentoVario.
	 * @param BigDecimal idToCobertura.
	 * 
	 * @return List<ExclusionAumentoVarioTipoPolizaDTO> found by query
	 */
	public List<ExclusionAumentoVarioTipoPolizaDTO> findByIDs(BigDecimal idToTipoPoliza, BigDecimal idToAumentoVario, BigDecimal idToCobertura);
	
	/**
	 * Encuentra los registros de ExclusionAumentoVarioTipoPolizaDTO relacionados con el TipoPolizaDTO cuyo ID se recibe y que adem�s est�n 
	 * relacionados s�lo con las coberturas que no hayan sido borradas l�gicamente.
	  @param BigDecimal idToTipoPoliza. El ID del tipoPoliza
	  @return List<ExclusionAumentoVarioTipoPolizaDTO> encontrados por el query formado.
	 */
    public List<ExclusionAumentoVarioTipoPolizaDTO> getVigentesPorIdTipoPoliza(BigDecimal idToTipoPoliza);
}