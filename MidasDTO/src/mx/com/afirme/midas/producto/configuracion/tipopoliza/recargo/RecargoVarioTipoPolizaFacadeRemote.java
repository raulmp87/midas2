package mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo;
// default package

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for RecargoVarioTipoPolizaFacade.
 * @author MyEclipse Persistence Tools
 */


public interface RecargoVarioTipoPolizaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved RecargoVarioTipoPolizaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity RecargoVarioTipoPolizaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(RecargoVarioTipoPolizaDTO entity);
    /**
	 Delete a persistent RecargoVarioTipoPolizaDTO entity.
	  @param entity RecargoVarioTipoPolizaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(RecargoVarioTipoPolizaDTO entity);
   /**
	 Persist a previously saved RecargoVarioTipoPolizaDTO entity and return it or a copy of it to the sender. 
	 A copy of the RecargoVarioTipoPolizaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity RecargoVarioTipoPolizaDTO entity to update
	 @return RecargoVarioTipoPolizaDTO the persisted RecargoVarioTipoPolizaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public RecargoVarioTipoPolizaDTO update(RecargoVarioTipoPolizaDTO entity);
	public RecargoVarioTipoPolizaDTO findById( RecargoVarioTipoPolizaId id);
	 /**
	 * Find all RecargoVarioTipoPolizaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the RecargoVarioTipoPolizaDTO property to query
	  @param value the property value to match
	  	  @return List<RecargoVarioTipoPolizaDTO> found by query
	 */
	public List<RecargoVarioTipoPolizaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all RecargoVarioTipoPolizaDTO entities.
	  	  @return List<RecargoVarioTipoPolizaDTO> all RecargoVarioTipoPolizaDTO entities
	 */
	public List<RecargoVarioTipoPolizaDTO> findAll(
		);	
}