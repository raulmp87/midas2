package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Remote interface for RiesgoCoberturaFacade.
 * 
 * @author Jos� Luis Arellano
 */
public interface RiesgoCoberturaFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved RiesgoCoberturaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            RiesgoCoberturaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(RiesgoCoberturaDTO entity);

	/**
	 * Delete a persistent RiesgoCoberturaDTO entity.
	 * 
	 * @param entity
	 *            RiesgoCoberturaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(RiesgoCoberturaDTO entity);

	/**
	 * Persist a previously saved RiesgoCoberturaDTO entity and return it or a
	 * copy of it to the sender. A copy of the RiesgoCoberturaDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            RiesgoCoberturaDTO entity to update
	 * @return RiesgoCoberturaDTO the persisted RiesgoCoberturaDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public RiesgoCoberturaDTO update(RiesgoCoberturaDTO entity);

	public RiesgoCoberturaDTO findById(RiesgoCoberturaId id);

	/**
	 * Find all RiesgoCoberturaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the RiesgoCoberturaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<RiesgoCoberturaDTO> found by query
	 */
	public List<RiesgoCoberturaDTO> findByProperty(String propertyName,Object value);

	/**
	 * Find all RiesgoCoberturaDTO entities.
	 * 
	 * @return List<RiesgoCoberturaDTO> all RiesgoCoberturaDTO entities
	 */
	public List<RiesgoCoberturaDTO> findAll();
	
	public List<RiesgoCoberturaDTO> findByCoberturaSeccion(BigDecimal idToCobertura, BigDecimal idToSeccion);
	
	public List<RiesgoCoberturaDTO> findByCoberturaSeccionId(BigDecimal idToCobertura, BigDecimal idToSeccion);
	/**
	 * listarVigentesPorCoberturaSeccion. lista los registros de CoberturaSeccionDTO que contengan la cobertura y la seccion 
	 * cuyos ID�s se reciben, y que adem�s est�n relacionados s�lo con riesgos vigentes.
	 * @param idToCobertura @param idToSeccion
	 * @return List<RiesgoCoberturaDTO> encontrados por el query.
	 */
	public List<RiesgoCoberturaDTO> listarVigentesPorCoberturaSeccion(BigDecimal idToCobertura, BigDecimal idToSeccion);

	public List<BigDecimal> obtenerRiesgosPorTipoPoliza(BigDecimal idToTipoPoliza);

	public List<BigDecimal> obtenerRiesgosPorSeccionCotizacion(BigDecimal idToCotizacion, BigDecimal numeroInciso, BigDecimal idToSeccion);

	public void borrarARDs(RiesgoCoberturaDTO riesgoCoberturaDTO);
	
	/**
	 * Obtiene un mapa con los <code>RiesgoCoberturaDTO</code> para una seccion agrupada por <code>idToCobertura</code>, es decir la llave del mapa es <code>idToCobertura</code>.
	 * Este mapa es util ya que en lugar de hacer llamadas individuales para obtener cada uno de los riesgos por cobertura se obtienen todos estos en una sola llamada
	 * y despues el cliente puede obtener cada uno de estos a traves del mapa proporcionando el <code>idToCobertura</code>.
	 * @param idToCobertura
	 * @return
	 */
	public Map<BigDecimal, List<RiesgoCoberturaDTO>> getMapCoberturaRiesgoCoberturaVigentes(BigDecimal idToSeccion);

}
