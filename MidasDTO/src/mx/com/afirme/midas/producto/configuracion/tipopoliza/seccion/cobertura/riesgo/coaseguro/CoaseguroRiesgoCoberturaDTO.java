package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.coaseguro;
// default package

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaDTO;


/**
 * CoaseguroRiesgoCoberturaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOCOASEGURORIESGOCOBERTURA",schema="MIDAS")
public class CoaseguroRiesgoCoberturaDTO  implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
     private CoaseguroRiesgoCoberturaId id;
     private RiesgoCoberturaDTO riesgoCoberturaDTO;
     private Double valor;
     private Short claveDefault;

    @EmbeddedId
    @AttributeOverrides( {
        @AttributeOverride(name="idToSeccion", column=@Column(name="IDTOSECCION", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idToCobertura", column=@Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idToRiesgo", column=@Column(name="IDTORIESGO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="numeroSecuencia", column=@Column(name="NUMEROSECUENCIA", nullable=false, precision=22, scale=0) ) } )
    public CoaseguroRiesgoCoberturaId getId() {
		return id;
	}

	public void setId(CoaseguroRiesgoCoberturaId id) {
		this.id = id;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumns( { 
        @JoinColumn(name="IDTOSECCION", referencedColumnName="IDTOSECCION", nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="IDTOCOBERTURA", referencedColumnName="IDTOCOBERTURA", nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="IDTORIESGO", referencedColumnName="IDTORIESGO", nullable=false, insertable=false, updatable=false) } )
    public RiesgoCoberturaDTO getRiesgoCoberturaDTO() {
		return riesgoCoberturaDTO;
	}

	public void setRiesgoCoberturaDTO(RiesgoCoberturaDTO riesgoCoberturaDTO) {
		this.riesgoCoberturaDTO = riesgoCoberturaDTO;
	}
    
    @Column(name="VALOR", nullable=false, precision=16)

    public Double getValor() {
        return this.valor;
    }
    
    public void setValor(Double valor) {
        this.valor = valor;
    }
    
    @Column(name="CLAVEDEFAULT", nullable=false, precision=4, scale=0)

    public Short getClaveDefault() {
        return this.claveDefault;
    }
    
    public void setClaveDefault(Short claveDefault) {
        this.claveDefault = claveDefault;
    }
}