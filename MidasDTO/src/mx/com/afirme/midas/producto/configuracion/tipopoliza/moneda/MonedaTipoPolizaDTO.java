package mx.com.afirme.midas.producto.configuracion.tipopoliza.moneda;

// default package

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;

/**
 * MonedaTipoPolizaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TRMONEDATIPOPOLIZA", schema = "MIDAS")
public class MonedaTipoPolizaDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MonedaTipoPolizaId id;
	private TipoPolizaDTO tipoPolizaDTO;

	// Constructors

	/** default constructor */
	public MonedaTipoPolizaDTO() {
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToTipoPoliza", column = @Column(name = "IDTOTIPOPOLIZA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idMoneda", column = @Column(name = "IDMONEDA", nullable = false, precision = 22, scale = 0)) })
	public MonedaTipoPolizaId getId() {
		return this.id;
	}

	public void setId(MonedaTipoPolizaId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOTIPOPOLIZA", nullable = false, insertable = false, updatable = false)
	public TipoPolizaDTO getTipoPolizaDTO() {
		return this.tipoPolizaDTO;
	}

	public void setTipoPolizaDTO(TipoPolizaDTO tipoPolizaDTO) {
		this.tipoPolizaDTO = tipoPolizaDTO;
	}

}