package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.seccionrequerida;
// default package

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;


/**
 * Remote interface for SeccionRequeridaDTOFacade.
 * @author MyEclipse Persistence Tools
 */

public interface SeccionRequeridaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved SeccionRequeridaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity SeccionRequeridaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(SeccionRequeridaDTO entity);
    /**
	 Delete a persistent SeccionRequeridaDTO entity.
	  @param entity SeccionRequeridaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(SeccionRequeridaDTO entity);
   /**
	 Persist a previously saved SeccionRequeridaDTO entity and return it or a copy of it to the sender. 
	 A copy of the SeccionRequeridaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity SeccionRequeridaDTO entity to update
	 @return SeccionRequeridaDTO the persisted SeccionRequeridaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public SeccionRequeridaDTO update(SeccionRequeridaDTO entity);
	public SeccionRequeridaDTO findById( SeccionRequeridaId id);
	 /**
	 * Find all SeccionRequeridaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the SeccionRequeridaDTO property to query
	  @param value the property value to match
	  	  @return List<SeccionRequeridaDTO> found by query
	 */
	public List<SeccionRequeridaDTO> findByProperty(String propertyName, Object value);
	/**
	 * Find all SeccionRequeridaDTO entities.
	  	  @return List<SeccionRequeridaDTO> all SeccionRequeridaDTO entities
	 */
	public List<SeccionRequeridaDTO> findAll();
	
	/**
	 * Find all SeccionRequeridaDTO entities related with SeccionDTO�d id recieved.
  	 * @return List<SeccionRequeridaDTO> all SeccionRequeridaDTO entities related with SeccionDTO�d id recieved.
	 */
	public List<SeccionRequeridaDTO> findRequiredSecctions(BigDecimal idToSeccion);
}