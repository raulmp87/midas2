package mx.com.afirme.midas.producto.configuracion.descuento.exclusion;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * ExclusionAumentoVarioProductoId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class ExclusionDescuentoVarioProductoId implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private BigDecimal idtoproducto;
    private BigDecimal idtodescuentovario;
    private BigDecimal idtotipopoliza;
    
    @Column(name="IDTOPRODUCTO", nullable=false, precision=22, scale=0)
    public BigDecimal getIdtoproducto() {
        return this.idtoproducto;
    }
    
    public void setIdtoproducto(BigDecimal idtoproducto) {
        this.idtoproducto = idtoproducto;
    }

    @Column(name="IDTODESCUENTOVARIO", nullable=false, precision=22, scale=0)
    public BigDecimal getIdtodescuentovario() {
		return idtodescuentovario;
	}
    
    public void setIdtodescuentovario(BigDecimal idtodescuentovario) {
		this.idtodescuentovario = idtodescuentovario;
	}

    @Column(name="IDTOTIPOPOLIZA", nullable=false, precision=22, scale=0)
    public BigDecimal getIdtotipopoliza() {
        return this.idtotipopoliza;
    }

	public void setIdtotipopoliza(BigDecimal idtotipopoliza) {
        this.idtotipopoliza = idtotipopoliza;
    }
}
