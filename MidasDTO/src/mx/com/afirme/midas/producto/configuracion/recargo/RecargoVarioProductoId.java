package mx.com.afirme.midas.producto.configuracion.recargo;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * RecargoPorProductoDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class RecargoVarioProductoId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToProducto;
     private BigDecimal idToRecargoVario;


    // Constructors

    /** default constructor */
    public RecargoVarioProductoId() {
    }

    
    /** full constructor */
    public RecargoVarioProductoId(BigDecimal idToProducto, BigDecimal idToRecargoVario) {
        this.idToProducto = idToProducto;
        this.idToRecargoVario = idToRecargoVario;
    }

   
    // Property accessors

    @Column(name="IDTOPRODUCTO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToProducto() {
        return this.idToProducto;
    }
    
    public void setIdToProducto(BigDecimal idToProducto) {
        this.idToProducto = idToProducto;
    }

    @Column(name="IDTORECARGOVARIO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToRecargoVario() {
        return this.idToRecargoVario;
    }
    
    public void setIdToRecargoVario(BigDecimal idToRecargoVario) {
        this.idToRecargoVario = idToRecargoVario;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof RecargoVarioProductoId) ) return false;
		 RecargoVarioProductoId castOther = ( RecargoVarioProductoId ) other; 
         
		 return ( (this.getIdToProducto()==castOther.getIdToProducto()) || ( this.getIdToProducto()!=null && castOther.getIdToProducto()!=null && this.getIdToProducto().equals(castOther.getIdToProducto()) ) )
 && ( (this.getIdToRecargoVario()==castOther.getIdToRecargoVario()) || ( this.getIdToRecargoVario()!=null && castOther.getIdToRecargoVario()!=null && this.getIdToRecargoVario().equals(castOther.getIdToRecargoVario()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdToProducto() == null ? 0 : this.getIdToProducto().hashCode() );
         result = 37 * result + ( getIdToRecargoVario() == null ? 0 : this.getIdToRecargoVario().hashCode() );
         return result;
   }   





}