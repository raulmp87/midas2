package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.exclusion;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * ExclusionDescuentoVarioCoberturaId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class ExclusionDescuentoVarioCoberturaId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idtocobertura;
     private BigDecimal idtodescuentovario;
     private BigDecimal idtoriesgo;

    /** default constructor */
    public ExclusionDescuentoVarioCoberturaId() {
    }

    /** full constructor */
    public ExclusionDescuentoVarioCoberturaId(BigDecimal idtocobertura, BigDecimal idtodescuentovario, BigDecimal idtoriesgo) {
        this.idtocobertura = idtocobertura;
        this.idtodescuentovario = idtodescuentovario;
        this.idtoriesgo = idtoriesgo;
    }

    @Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0)
    public BigDecimal getIdtocobertura() {
        return this.idtocobertura;
    }
    
    public void setIdtocobertura(BigDecimal idtocobertura) {
        this.idtocobertura = idtocobertura;
    }
    
    @Column(name="IDTOAUMENTOVARIO", nullable=false, precision=22, scale=0)
    public BigDecimal getIdtodescuentovario() {
        return this.idtodescuentovario;
    }
    
    public void setIdtodescuentovario(BigDecimal idtodescuentovario) {
        this.idtodescuentovario = idtodescuentovario;
    }

    @Column(name="IDTORIESGO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtoriesgo() {
        return this.idtoriesgo;
    }
    
    public void setIdtoriesgo(BigDecimal idtoriesgo) {
        this.idtoriesgo = idtoriesgo;
    }
   
}