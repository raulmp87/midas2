package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.exclusion;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for ExclusionRecargoVarioCoberturaFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ExclusionRecargoVarioCoberturaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ExclusionRecargoVarioCoberturaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ExclusionRecargoVarioCoberturaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ExclusionRecargoVarioCoberturaDTO entity);
    /**
	 Delete a persistent ExclusionRecargoVarioCoberturaDTO entity.
	  @param entity ExclusionRecargoVarioCoberturaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ExclusionRecargoVarioCoberturaDTO entity);
   /**
	 Persist a previously saved ExclusionRecargoVarioCoberturaDTO entity and return it or a copy of it to the sender. 
	 A copy of the ExclusionRecargoVarioCoberturaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ExclusionRecargoVarioCoberturaDTO entity to update
	 @return ExclusionRecargoVarioCoberturaDTO the persisted ExclusionRecargoVarioCoberturaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ExclusionRecargoVarioCoberturaDTO update(ExclusionRecargoVarioCoberturaDTO entity);
	public ExclusionRecargoVarioCoberturaDTO findById( ExclusionRecargoVarioCoberturaId id);
	 /**
	 * Find all ExclusionRecargoVarioCoberturaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ExclusionRecargoVarioCoberturaDTO property to query
	  @param value the property value to match
	  	  @return List<ExclusionRecargoVarioCoberturaDTO> found by query
	 */
	public List<ExclusionRecargoVarioCoberturaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ExclusionRecargoVarioCoberturaDTO entities.
	  	  @return List<ExclusionRecargoVarioCoberturaDTO> all ExclusionRecargoVarioCoberturaDTO entities
	 */
	public List<ExclusionRecargoVarioCoberturaDTO> findAll(
		);
	
	/**
	 * Encuentra los registros de ExclusionRecargoVarioCoberturaDTO relacionados con la CoberturaSeccionDTO cuyos ID�s se reciben y que adem�s est�n 
	 * relacionados s�lo con Riesgos que no hayan sido borradas l�gicamente.
	  @param BigDecimal idToCobertura. El ID de la cobertura.
	  @param BigDecimal idToSeccion. El ID de la seccion.
	  @return List<ExclusionRecargoVarioCoberturaDTO> encontrados por el query formado.
	 */
    public List<ExclusionRecargoVarioCoberturaDTO> getVigentesPorIdCoberturaIdSeccion(BigDecimal idToCobertura,BigDecimal idToSeccion);
}