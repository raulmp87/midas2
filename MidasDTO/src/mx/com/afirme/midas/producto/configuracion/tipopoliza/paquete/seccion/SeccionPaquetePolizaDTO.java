package mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.seccion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.seccion.cobertura.CoberturaSeccionPaqueteDTO;

public class SeccionPaquetePolizaDTO implements Serializable {
	private static final long serialVersionUID = -5060653551718001595L;

	private BigDecimal idToSeccion;
	private String nombreSeccion;
	private String descripcionSeccion;
	
	private List<CoberturaSeccionPaqueteDTO> listaCoberturas;

	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	public String getNombreSeccion() {
		return nombreSeccion;
	}

	public void setNombreSeccion(String nombreSeccion) {
		this.nombreSeccion = nombreSeccion;
	}

	public String getDescripcionSeccion() {
		return descripcionSeccion;
	}

	public void setDescripcionSeccion(String descripcionSeccion) {
		this.descripcionSeccion = descripcionSeccion;
	}

	public List<CoberturaSeccionPaqueteDTO> getListaCoberturas() {
		return listaCoberturas;
	}

	public void setListaCoberturas(List<CoberturaSeccionPaqueteDTO> listaCoberturas) {
		this.listaCoberturas = listaCoberturas;
	}
	
}
