package mx.com.afirme.midas.producto.configuracion.recargo.exclusion;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for EXCRecargoPorProductoDTO.
 * @author Jos� Luis Arellano
 */

public interface ExclusionRecargoVarioProductoFacadeRemote {
	/**
	 Perform an initial save of a previously unsaved ExcRecargoVarioProducto entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ExcRecargoVarioProducto entity to persist
	  @throws RuntimeException when the operation fails
	 */
	public void save(ExclusionRecargoVarioProductoDTO entity);
   /**
	 Delete a persistent ExcRecargoPorProductoDTO entity.
	  @param entity ExcRecargoPorProductoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
	public void delete(ExclusionRecargoVarioProductoDTO entity);
  /**
	 Persist a previously saved ExcRecargoPorProductoDTO entity and return it or a copy of it to the sender. 
	 A copy of the ExcRecargoPorProductoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ExcRecargoPorProductoDTO entity to update
	 @return ExcRecargoPorProductoDTO the persisted ExcRecargoPorProductoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ExclusionRecargoVarioProductoDTO update(ExclusionRecargoVarioProductoDTO entity);

	public ExclusionRecargoVarioProductoDTO findById( ExclusionRecargoVarioProductoId id);
	 /**
	 * Find all ExcRecargoPorProductoDTO entities with a specific property value.  	 
	  @param propertyName the name of the ExcRecargoPorProductoDTO property to query
	  @param value the property value to match
	  	  @return List<ExcRecargoPorProductoDTO> found by query
	 */
	public List<ExclusionRecargoVarioProductoDTO> findByProperty(String propertyName, Object value);
	/**
	 * Find all ExcRecargoPorProductoDTO entities.
	  @return List<ExcRecargoPorProductoDTO> all ExcRecargoPorProductoDTO entities
	 */
	public List<ExclusionRecargoVarioProductoDTO> findAll();
	
	/**
	 * Encuentra los registros de ExclusionRecargoVarioProductoDTO relacionados con el ProductoDTO cuyo ID se recibe y que adem�s est�n 
	 * relacionados s�lo con Tipos de p�liza que no hayan sido borradas l�gicamente.
	  @param BigDecimal idToProducto. El ID del producto
	  @return List<ExclusionRecargoVarioProductoDTO> encontrados por el query formado.
	 */
    public List<ExclusionRecargoVarioProductoDTO> getVigentesPorIdProducto(BigDecimal idToProducto);
}
