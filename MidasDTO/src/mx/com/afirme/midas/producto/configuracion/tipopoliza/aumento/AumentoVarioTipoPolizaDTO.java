package mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.aumentovario.AumentoVarioDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;


/**
 * AumentoVarioTipoPolizaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOAUMENTOVARIOTIPOPOLIZA"
    ,schema="MIDAS"
)
public class AumentoVarioTipoPolizaDTO  implements java.io.Serializable {

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private AumentoVarioTipoPolizaId id;
    private AumentoVarioDTO aumentoVarioDTO;
    private TipoPolizaDTO tipoPolizaDTO;
    private Short claveobligatoriedad;
    private Short clavecomercialtecnico;
    private Short claveaplicareaseguro;
    private Double valor;



    /** default constructor */
    public AumentoVarioTipoPolizaDTO() {
    }
   
    
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idtotipopoliza", column=@Column(name="IDTOTIPOPOLIZA", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idtoaumentovario", column=@Column(name="IDTOAUMENTOVARIO", nullable=false, precision=22, scale=0) ) } )

    public AumentoVarioTipoPolizaId getId() {
        return this.id;
    }
    
    public void setId(AumentoVarioTipoPolizaId id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTOAUMENTOVARIO", nullable=false, insertable=false, updatable=false)
     public AumentoVarioDTO getAumentoVarioDTO() {
		return aumentoVarioDTO;
	}


	public void setAumentoVarioDTO(AumentoVarioDTO aumentoVarioDTO) {
		this.aumentoVarioDTO = aumentoVarioDTO;
	}
  
    
    @Column(name="CLAVEOBLIGATORIEDAD", nullable=false, precision=4, scale=0)

    public Short getClaveobligatoriedad() {
        return this.claveobligatoriedad;
    }
    
    public void setClaveobligatoriedad(Short claveobligatoriedad) {
        this.claveobligatoriedad = claveobligatoriedad;
    }
    
    @Column(name="CLAVECOMERCIALTECNICO", nullable=false, precision=4, scale=0)

    public Short getClavecomercialtecnico() {
        return this.clavecomercialtecnico;
    }
    
    public void setClavecomercialtecnico(Short clavecomercialtecnico) {
        this.clavecomercialtecnico = clavecomercialtecnico;
    }
    
    @Column(name="CLAVEAPLICAREASEGURO", nullable=false, precision=4, scale=0)

    public Short getClaveaplicareaseguro() {
        return this.claveaplicareaseguro;
    }
    
    public void setClaveaplicareaseguro(Short claveaplicareaseguro) {
        this.claveaplicareaseguro = claveaplicareaseguro;
    }
    
    @Column(name="VALOR", nullable=false, precision=16)

    public Double getValor() {
        return this.valor;
    }
    
    public void setValor(Double valor) {
        this.valor = valor;
    }

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTOTIPOPOLIZA", nullable=false, insertable=false, updatable=false)
	public TipoPolizaDTO getTipoPolizaDTO() {
		return tipoPolizaDTO;
	}


	public void setTipoPolizaDTO(TipoPolizaDTO tipoPolizaDTO) {
		this.tipoPolizaDTO = tipoPolizaDTO;
	}


	
   








}