package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * CoberturaSeccionDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class CoberturaSeccionDTOId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idtoseccion;
     private BigDecimal idtocobertura;


    // Constructors

    /** default constructor */
    public CoberturaSeccionDTOId() {
    }

    
    /** full constructor */
    public CoberturaSeccionDTOId(BigDecimal idtoseccion, BigDecimal idtocobertura) {
        this.idtoseccion = idtoseccion;
        this.idtocobertura = idtocobertura;
    }

   
    // Property accessors

    @Column(name="IDTOSECCION", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtoseccion() {
        return this.idtoseccion;
    }
    
    public void setIdtoseccion(BigDecimal idtoseccion) {
        this.idtoseccion = idtoseccion;
    }

    @Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtocobertura() {
        return this.idtocobertura;
    }
    
    public void setIdtocobertura(BigDecimal idtocobertura) {
        this.idtocobertura = idtocobertura;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof CoberturaSeccionDTOId) ) return false;
		 CoberturaSeccionDTOId castOther = ( CoberturaSeccionDTOId ) other; 
         
		 return ( (this.getIdtoseccion()==castOther.getIdtoseccion()) || ( this.getIdtoseccion()!=null && castOther.getIdtoseccion()!=null && this.getIdtoseccion().equals(castOther.getIdtoseccion()) ) )
 && ( (this.getIdtocobertura()==castOther.getIdtocobertura()) || ( this.getIdtocobertura()!=null && castOther.getIdtocobertura()!=null && this.getIdtocobertura().equals(castOther.getIdtocobertura()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdtoseccion() == null ? 0 : this.getIdtoseccion().hashCode() );
         result = 37 * result + ( getIdtocobertura() == null ? 0 : this.getIdtocobertura().hashCode() );
         return result;
   }   





}