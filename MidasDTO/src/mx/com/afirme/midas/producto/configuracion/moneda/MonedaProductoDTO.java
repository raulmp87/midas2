package mx.com.afirme.midas.producto.configuracion.moneda;

// default package

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.producto.ProductoDTO;

/**
 * MonedaProductoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name = "MonedaProductoDTO")
@Table(name = "TRMONEDAPRODUCTO", schema = "MIDAS")
public class MonedaProductoDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MonedaProductoId id;
	private ProductoDTO productoDTO;

	// Constructors

	/** default constructor */
	public MonedaProductoDTO() {
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToProducto", column = @Column(name = "IDTOPRODUCTO")),
			@AttributeOverride(name = "idMoneda", column = @Column(name = "IDMONEDA")) })
	public MonedaProductoId getId() {
		return this.id;
	}

	public void setId(MonedaProductoId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOPRODUCTO", nullable=false, insertable=false, updatable=false)
	public ProductoDTO getProductoDTO() {
		return this.productoDTO;
	}

	public void setProductoDTO(ProductoDTO productoDTO) {
		this.productoDTO = productoDTO;
	}

}