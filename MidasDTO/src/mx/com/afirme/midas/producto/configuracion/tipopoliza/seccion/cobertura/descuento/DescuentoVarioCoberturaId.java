package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * DescuentoVarioCoberturaId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class DescuentoVarioCoberturaId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idtocobertura;
    private BigDecimal idtodescuentovario;
    
    public DescuentoVarioCoberturaId() {
    }

    public DescuentoVarioCoberturaId(BigDecimal idtocobertura, BigDecimal idtodescuentovario) {
        this.idtocobertura = idtocobertura;
        this.idtodescuentovario = idtodescuentovario;
    }

    @Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0)
    public BigDecimal getIdtocobertura() {
        return this.idtocobertura;
    }
    
    public void setIdtocobertura(BigDecimal idtocobertura) {
        this.idtocobertura = idtocobertura;
    }

    @Column(name="IDTODESCUENTOVARIO", nullable=false, precision=22, scale=0)
    public BigDecimal getIdtodescuentovario() {
        return this.idtodescuentovario;
    }
    
    public void setIdtodescuentovario(BigDecimal idtodescuentovario) {
        this.idtodescuentovario = idtodescuentovario;
    }
   
}