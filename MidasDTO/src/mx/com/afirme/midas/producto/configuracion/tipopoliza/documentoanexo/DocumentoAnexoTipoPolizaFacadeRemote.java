package mx.com.afirme.midas.producto.configuracion.tipopoliza.documentoanexo;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for DocumentoAnexoTipoPolizaDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface DocumentoAnexoTipoPolizaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved DocumentoAnexoTipoPolizaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DocumentoAnexoTipoPolizaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DocumentoAnexoTipoPolizaDTO entity);
    /**
	 Delete a persistent DocumentoAnexoTipoPolizaDTO entity.
	  @param entity DocumentoAnexoTipoPolizaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DocumentoAnexoTipoPolizaDTO entity);
   /**
	 Persist a previously saved DocumentoAnexoTipoPolizaDTO entity and return it or a copy of it to the sender. 
	 A copy of the DocumentoAnexoTipoPolizaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DocumentoAnexoTipoPolizaDTO entity to update
	 @return DocumentoAnexoTipoPolizaDTO the persisted DocumentoAnexoTipoPolizaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public DocumentoAnexoTipoPolizaDTO update(DocumentoAnexoTipoPolizaDTO entity);
	public DocumentoAnexoTipoPolizaDTO findById( BigDecimal id);
	 /**
	 * Find all DocumentoAnexoTipoPolizaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DocumentoAnexoTipoPolizaDTO property to query
	  @param value the property value to match
	  	  @return List<DocumentoAnexoTipoPolizaDTO> found by query
	 */
	public List<DocumentoAnexoTipoPolizaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all DocumentoAnexoTipoPolizaDTO entities.
	  	  @return List<DocumentoAnexoTipoPolizaDTO> all DocumentoAnexoTipoPolizaDTO entities
	 */
	public List<DocumentoAnexoTipoPolizaDTO> findAll(
		);	
}