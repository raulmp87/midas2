package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.coaseguro;
// default package

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

/**
 * Remote interface for CoaseguroRiesgoCoberturaDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface CoaseguroRiesgoCoberturaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved CoaseguroRiesgoCoberturaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CoaseguroRiesgoCoberturaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CoaseguroRiesgoCoberturaDTO entity);
    /**
	 Delete a persistent CoaseguroRiesgoCoberturaDTO entity.
	  @param entity CoaseguroRiesgoCoberturaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CoaseguroRiesgoCoberturaDTO entity);
   /**
	 Persist a previously saved CoaseguroRiesgoCoberturaDTO entity and return it or a copy of it to the sender. 
	 A copy of the CoaseguroRiesgoCoberturaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CoaseguroRiesgoCoberturaDTO entity to update
	 @return CoaseguroRiesgoCoberturaDTO the persisted CoaseguroRiesgoCoberturaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CoaseguroRiesgoCoberturaDTO update(CoaseguroRiesgoCoberturaDTO entity);
	public CoaseguroRiesgoCoberturaDTO findById( CoaseguroRiesgoCoberturaId id);
	 /**
	 * Find all CoaseguroRiesgoCoberturaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the CoaseguroRiesgoCoberturaDTO property to query
	  @param value the property value to match
	  	  @return List<CoaseguroRiesgoCoberturaDTO> found by query
	 */
	public List<CoaseguroRiesgoCoberturaDTO> findByProperty(String propertyName, Object value
		);
	public List<CoaseguroRiesgoCoberturaDTO> findByValor(Object valor
		);
	public List<CoaseguroRiesgoCoberturaDTO> findByClaveDefault(Object claveDefault
		);
	/**
	 * Find all CoaseguroRiesgoCoberturaDTO entities.
	  	  @return List<CoaseguroRiesgoCoberturaDTO> all CoaseguroRiesgoCoberturaDTO entities
	 */
	public List<CoaseguroRiesgoCoberturaDTO> findAll();
	
	public List<CoaseguroRiesgoCoberturaDTO> findByRiesgoCoberturaSeccion(BigDecimal idToRiesgo,BigDecimal idToCobertura,BigDecimal idToSeccion);

	public void synchronizeCoasegurosRiesgoCobertura(BigDecimal idToRiesgo, BigDecimal idToCobertura, BigDecimal idToSeccion);

	public List<CoaseguroRiesgoCoberturaDTO> listarFiltrado(CoaseguroRiesgoCoberturaDTO entity);
}