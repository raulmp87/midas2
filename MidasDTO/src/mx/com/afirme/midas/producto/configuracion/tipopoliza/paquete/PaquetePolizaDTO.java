package mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.seccion.cobertura.CoberturaSeccionPaqueteDTO;


/**
 * PaquetePolizaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOPAQUETEPOLIZA"
    ,schema="MIDAS"
, uniqueConstraints = @UniqueConstraint(columnNames="NOMBRE")
)
public class PaquetePolizaDTO  implements java.io.Serializable {
	
	public static final Short ESTATUS_CONFIGURANDO = (short)0;
	public static final Short ESTATUS_LIBERADO = (short)1;

	private static final long serialVersionUID = -4737149495802411992L;
	private BigDecimal idToPaquetePoliza;
	private TipoPolizaDTO tipoPolizaDTO = new TipoPolizaDTO();
	private String nombre;
	private String descripcion;
	private Short orden;
	private Short estatus;
     
     private List<CoberturaSeccionPaqueteDTO> coberturaSeccionPaqueteDTOs = new ArrayList<CoberturaSeccionPaqueteDTO>();


    // Constructors

    /** default constructor */
    public PaquetePolizaDTO() {
    }

	/** minimal constructor */
    public PaquetePolizaDTO(BigDecimal idToPaquetePoliza, String nombre, Short orden) {
        this.idToPaquetePoliza = idToPaquetePoliza;
        this.nombre = nombre;
        this.orden = orden;
    }
    
    /** full constructor */
    public PaquetePolizaDTO(BigDecimal idToPaquetePoliza, String nombre, String descripcion, Short orden, List<CoberturaSeccionPaqueteDTO> coberturaSeccionPaqueteDTOs) {
        this.idToPaquetePoliza = idToPaquetePoliza;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.orden = orden;
        this.coberturaSeccionPaqueteDTOs = coberturaSeccionPaqueteDTOs;
    }

    @Id
	@SequenceGenerator(name = "TOPAQUETEPOLI_IDTOPAQUETEP_SEQ", allocationSize = 1, sequenceName = "MIDAS.TOPAQUETEPOLI_IDTOPAQUETEP_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TOPAQUETEPOLI_IDTOPAQUETEP_SEQ")
	@Column(name = "IDTOPAQUETEPOLIZA")
    public BigDecimal getIdToPaquetePoliza() {
        return this.idToPaquetePoliza;
    }
    
    public void setIdToPaquetePoliza(BigDecimal idToPaquetePoliza) {
        this.idToPaquetePoliza = idToPaquetePoliza;
    }
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "IDTOTIPOPOLIZA", nullable = false)
   	public TipoPolizaDTO getTipoPolizaDTO() {
		return tipoPolizaDTO;
	}

	public void setTipoPolizaDTO(TipoPolizaDTO tipoPolizaDTO) {
		this.tipoPolizaDTO = tipoPolizaDTO;
	}
    
    @Column(name="NOMBRE", unique=true, nullable=false)

    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    @Column(name="DESCRIPCION", length=200)

    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    @Column(name="ORDEN", nullable=false, precision=4, scale=0)
    public Short getOrden() {
        return this.orden;
    }
    
    public void setOrden(Short orden) {
        this.orden = orden;
    }
    
    @Column(name="ESTATUS", nullable=false, precision=1, scale=0)
    public Short getEstatus() {
		return estatus;
	}

	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}

@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="paquetePolizaDTO")
    public List<CoberturaSeccionPaqueteDTO> getCoberturaSeccionPaqueteDTOs() {
        return this.coberturaSeccionPaqueteDTOs;
    }
    
    public void setCoberturaSeccionPaqueteDTOs(List<CoberturaSeccionPaqueteDTO> coberturaSeccionPaqueteDTOs) {
        this.coberturaSeccionPaqueteDTOs = coberturaSeccionPaqueteDTOs;
    }
}