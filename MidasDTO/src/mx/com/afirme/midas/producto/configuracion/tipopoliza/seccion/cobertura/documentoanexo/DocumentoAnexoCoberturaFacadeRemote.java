package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.documentoanexo;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for DocumentoAnexoCoberturaDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface DocumentoAnexoCoberturaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved DocumentoAnexoCoberturaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DocumentoAnexoCoberturaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DocumentoAnexoCoberturaDTO entity);
    /**
	 Delete a persistent DocumentoAnexoCoberturaDTO entity.
	  @param entity DocumentoAnexoCoberturaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DocumentoAnexoCoberturaDTO entity);
   /**
	 Persist a previously saved DocumentoAnexoCoberturaDTO entity and return it or a copy of it to the sender. 
	 A copy of the DocumentoAnexoCoberturaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DocumentoAnexoCoberturaDTO entity to update
	 @return DocumentoAnexoCoberturaDTO the persisted DocumentoAnexoCoberturaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public DocumentoAnexoCoberturaDTO update(DocumentoAnexoCoberturaDTO entity);
	public DocumentoAnexoCoberturaDTO findById( BigDecimal id);
	 /**
	 * Find all DocumentoAnexoCoberturaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DocumentoAnexoCoberturaDTO property to query
	  @param value the property value to match
	  	  @return List<DocumentoAnexoCoberturaDTO> found by query
	 */
	public List<DocumentoAnexoCoberturaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all DocumentoAnexoCoberturaDTO entities.
	  	  @return List<DocumentoAnexoCoberturaDTO> all DocumentoAnexoCoberturaDTO entities
	 */
	public List<DocumentoAnexoCoberturaDTO> findAll(
		);

	public List<DocumentoAnexoCoberturaDTO> listarAnexosPorCotizacion(BigDecimal idToCotizacion);
	
	public List<DocumentoAnexoCoberturaDTO> listarAnexosPorCoberturas(List<BigDecimal> lstCoberturasDto);
}