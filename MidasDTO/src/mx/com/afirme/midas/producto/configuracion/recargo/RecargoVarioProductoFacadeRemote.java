package mx.com.afirme.midas.producto.configuracion.recargo;
// default package

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for RecargoPorProductoDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface RecargoVarioProductoFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved RecargoPorProductoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity RecargoPorProductoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(RecargoVarioProductoDTO entity);
    /**
	 Delete a persistent RecargoPorProductoDTO entity.
	  @param entity RecargoPorProductoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(RecargoVarioProductoDTO entity);
   /**
	 Persist a previously saved RecargoPorProductoDTO entity and return it or a copy of it to the sender. 
	 A copy of the RecargoPorProductoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity RecargoPorProductoDTO entity to update
	 @return RecargoPorProductoDTO the persisted RecargoPorProductoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public RecargoVarioProductoDTO update(RecargoVarioProductoDTO entity);
	public RecargoVarioProductoDTO findById( RecargoVarioProductoId id);
	 /**
	 * Find all RecargoPorProductoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the RecargoPorProductoDTO property to query
	  @param value the property value to match
	  	  @return List<RecargoPorProductoDTO> found by query
	 */
	public List<RecargoVarioProductoDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all RecargoPorProductoDTO entities.
	  	  @return List<RecargoPorProductoDTO> all RecargoPorProductoDTO entities
	 */
	public List<RecargoVarioProductoDTO> findAll(
		);	
}