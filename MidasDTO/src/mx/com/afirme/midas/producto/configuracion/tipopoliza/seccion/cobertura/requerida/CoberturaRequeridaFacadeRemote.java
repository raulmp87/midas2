package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.requerida;

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for CoberturaRequeridaFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface CoberturaRequeridaFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved CoberturaRequeridaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            CoberturaRequeridaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(CoberturaRequeridaDTO entity);

	/**
	 * Delete a persistent CoberturaRequeridaDTO entity.
	 * 
	 * @param entity
	 *            CoberturaRequeridaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(CoberturaRequeridaDTO entity);

	/**
	 * Persist a previously saved CoberturaRequeridaDTO entity and return it or
	 * a copy of it to the sender. A copy of the CoberturaRequeridaDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            CoberturaRequeridaDTO entity to update
	 * @return CoberturaRequeridaDTO the persisted CoberturaRequeridaDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public CoberturaRequeridaDTO update(CoberturaRequeridaDTO entity);

	public CoberturaRequeridaDTO findById(CoberturaRequeridaId id);

	/**
	 * Find all CoberturaRequeridaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the CoberturaRequeridaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<CoberturaRequeridaDTO> found by query
	 */
	public List<CoberturaRequeridaDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all CoberturaRequeridaDTO entities.
	 * 
	 * @return List<CoberturaRequeridaDTO> all CoberturaRequeridaDTO entities
	 */
	public List<CoberturaRequeridaDTO> findAll();
}