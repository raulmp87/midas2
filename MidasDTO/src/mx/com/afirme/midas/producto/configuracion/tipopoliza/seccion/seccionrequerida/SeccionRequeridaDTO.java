package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.seccionrequerida;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;


/**
 * SeccionRequeridaDTO entity. @author José Luis Arellano
 */
@Entity
@Table(name="TRSECCIONREQUERIDA",schema="MIDAS")
public class SeccionRequeridaDTO  implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private SeccionRequeridaId id;
	private SeccionDTO seccionBaseDTO;
	private SeccionDTO seccionRequeridaDTO;

    
	@EmbeddedId
    @AttributeOverrides( {
    	@AttributeOverride(name="idToSeccion", column=@Column(name="IDTOSECCION", nullable=false, precision=22, scale=0) ), 
    	@AttributeOverride(name="idSeccionRequerida", column=@Column(name="IDSECCIONREQUERIDA", nullable=false, precision=22, scale=0) ) } )
    public SeccionRequeridaId getId() {
        return this.id;
    }

    public void setId(SeccionRequeridaId id) {
        this.id = id;
    }
	
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="IDTOSECCION", nullable=false, insertable=false, updatable=false)
    public SeccionDTO getSeccionBaseDTO() {
		return seccionBaseDTO;
	}

	public void setSeccionBaseDTO(SeccionDTO seccionBaseDTO) {
		this.seccionBaseDTO = seccionBaseDTO;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="IDSECCIONREQUERIDA", nullable=false, insertable=false, updatable=false)
    public SeccionDTO getSeccionRequeridaDTO() {
		return seccionRequeridaDTO;
	}

	public void setSeccionRequeridaDTO(SeccionDTO seccionRequeridaDTO) {
		this.seccionRequeridaDTO = seccionRequeridaDTO;
	}
}