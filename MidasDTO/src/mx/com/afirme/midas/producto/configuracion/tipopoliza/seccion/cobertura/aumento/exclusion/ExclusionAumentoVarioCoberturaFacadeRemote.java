package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.exclusion;
// default package

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

/**
 * Remote interface for ExclusionAumentoVarioCoberturaFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ExclusionAumentoVarioCoberturaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ExclusionAumentoVarioCoberturaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ExclusionAumentoVarioCoberturaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ExclusionAumentoVarioCoberturaDTO entity);
    /**
	 Delete a persistent ExclusionAumentoVarioCoberturaDTO entity.
	  @param entity ExclusionAumentoVarioCoberturaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ExclusionAumentoVarioCoberturaDTO entity);
   /**
	 Persist a previously saved ExclusionAumentoVarioCoberturaDTO entity and return it or a copy of it to the sender. 
	 A copy of the ExclusionAumentoVarioCoberturaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ExclusionAumentoVarioCoberturaDTO entity to update
	 @return ExclusionAumentoVarioCoberturaDTO the persisted ExclusionAumentoVarioCoberturaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ExclusionAumentoVarioCoberturaDTO update(ExclusionAumentoVarioCoberturaDTO entity);
	public ExclusionAumentoVarioCoberturaDTO findById( ExclusionAumentoVarioCoberturaId id);
	 /**
	 * Find all ExclusionAumentoVarioCoberturaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ExclusionAumentoVarioCoberturaDTO property to query
	  @param value the property value to match
	  	  @return List<ExclusionAumentoVarioCoberturaDTO> found by query
	 */
	public List<ExclusionAumentoVarioCoberturaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ExclusionAumentoVarioCoberturaDTO entities.
	  	  @return List<ExclusionAumentoVarioCoberturaDTO> all ExclusionAumentoVarioCoberturaDTO entities
	 */
	public List<ExclusionAumentoVarioCoberturaDTO> findAll(
		);
	/**
	 * Find a ExclusionAumentoVarioCoberturaDTO entity with the specific
	 * received id�s.
	 * 
	 * @param BigDecimal idToCobertura.
	 * @param BigDecimal idToAumentoVario.
	 * @param BigDecimal idToRiesgo.
	 * 
	 * @return List<ExclusionAumentoVarioCoberturaDTO> found by query
	 */
	public List<ExclusionAumentoVarioCoberturaDTO> findByIDs(BigDecimal idToCobertura, BigDecimal idToAumentoVario, BigDecimal idToRiesgo);
	
	/**
	 * Encuentra los registros de ExclusionAumentoVarioCoberturaDTO relacionados con la CoberturaSeccionDTO cuyos ID�s se reciben y que adem�s est�n 
	 * relacionados s�lo con Riesgos que no hayan sido borradas l�gicamente.
	  @param BigDecimal idToCobertura. El ID de la cobertura.
	  @param BigDecimal idToSeccion. El ID de la seccion.
	  @return List<ExclusionAumentoVarioCoberturaDTO> encontrados por el query formado.
	 */
    public List<ExclusionAumentoVarioCoberturaDTO> getVigentesPorIdCoberturaIdSeccion(BigDecimal idToCobertura,BigDecimal idToSeccion);
}