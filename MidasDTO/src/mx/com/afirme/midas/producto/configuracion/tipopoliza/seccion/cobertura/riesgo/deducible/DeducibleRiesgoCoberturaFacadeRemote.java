package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.deducible;
// default package

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

/**
 * Remote interface for DeducibleRiesgoCoberturaDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface DeducibleRiesgoCoberturaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved DeducibleRiesgoCoberturaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DeducibleRiesgoCoberturaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DeducibleRiesgoCoberturaDTO entity);
    /**
	 Delete a persistent DeducibleRiesgoCoberturaDTO entity.
	  @param entity DeducibleRiesgoCoberturaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DeducibleRiesgoCoberturaDTO entity);
   /**
	 Persist a previously saved DeducibleRiesgoCoberturaDTO entity and return it or a copy of it to the sender. 
	 A copy of the DeducibleRiesgoCoberturaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DeducibleRiesgoCoberturaDTO entity to update
	 @return DeducibleRiesgoCoberturaDTO the persisted DeducibleRiesgoCoberturaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public DeducibleRiesgoCoberturaDTO update(DeducibleRiesgoCoberturaDTO entity);
	public DeducibleRiesgoCoberturaDTO findById( DeducibleRiesgoCoberturaId id);
	 /**
	 * Find all DeducibleRiesgoCoberturaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DeducibleRiesgoCoberturaDTO property to query
	  @param value the property value to match
	  	  @return List<DeducibleRiesgoCoberturaDTO> found by query
	 */
	public List<DeducibleRiesgoCoberturaDTO> findByProperty(String propertyName, Object value
		);
	public List<DeducibleRiesgoCoberturaDTO> findByValor(Object valor
		);
	public List<DeducibleRiesgoCoberturaDTO> findByClaveDefault(Object claveDefault
		);
	/**
	 * Find all DeducibleRiesgoCoberturaDTO entities.
	  	  @return List<DeducibleRiesgoCoberturaDTO> all DeducibleRiesgoCoberturaDTO entities
	 */
	public List<DeducibleRiesgoCoberturaDTO> findAll(
		);
	
	public List<DeducibleRiesgoCoberturaDTO> findByRiesgoCoberturaSeccion(BigDecimal idToRiesgo,BigDecimal idToCobertura,BigDecimal idToSeccion);

	public void synchronizeDeduciblesRiesgoCobertura(BigDecimal idToRiesgo, BigDecimal idToCobertura, BigDecimal idToSeccion);

	public List<DeducibleRiesgoCoberturaDTO> listarFiltrado(DeducibleRiesgoCoberturaDTO entity);
}