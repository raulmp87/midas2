package mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;

/**
 * RamoTipoPolizaDTO entity. @author José Luis Arellano
 */
@Entity
@Table(name="TRRAMOTIPOPOLIZA",schema="MIDAS")
public class RamoTipoPolizaDTO implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	// Fields
    private TipoPolizaDTO tipoPolizaDTO;
    private RamoDTO ramoDTO;
    private RamoTipoPolizaId id;
	
    @EmbeddedId
    @AttributeOverrides( {
        @AttributeOverride(name="idtotipopoliza", column=@Column(name="IDTOTIPOPOLIZA", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idtcramo", column=@Column(name="IDTCRAMO", nullable=false, precision=22, scale=0) ) } )
    public RamoTipoPolizaId getId() {
		return id;
	}
	public void setId(RamoTipoPolizaId id) {
		this.id = id;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTOTIPOPOLIZA", nullable=false, insertable=false, updatable=false)
	public TipoPolizaDTO getTipoPolizaDTO() {
		return tipoPolizaDTO;
	}
	public void setTipoPolizaDTO(TipoPolizaDTO tipoPolizaDTO) {
		this.tipoPolizaDTO = tipoPolizaDTO;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="IDTCRAMO", nullable=false, insertable=false, updatable=false)
	public RamoDTO getRamoDTO() {
		return ramoDTO;
	}
	public void setRamoDTO(RamoDTO ramoDTO) {
		this.ramoDTO = ramoDTO;
	}
}
