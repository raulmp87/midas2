package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.requerida;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * CoberturaRequeridaId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class CoberturaRequeridaId implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToCobertura;
	private BigDecimal idToCoberturaRequerida;

	// Constructors

	/** default constructor */
	public CoberturaRequeridaId() {
	}

	/** full constructor */
	public CoberturaRequeridaId(BigDecimal idToCobertura,
			BigDecimal idToCoberturaRequerida) {
		this.idToCobertura = idToCobertura;
		this.idToCoberturaRequerida = idToCoberturaRequerida;
	}

	// Property accessors

	@Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCobertura() {
		return this.idToCobertura;
	}

	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	@Column(name = "IDCOBERTURAREQUERIDA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCoberturaRequerida() {
		return this.idToCoberturaRequerida;
	}

	public void setIdToCoberturaRequerida(BigDecimal idToCoberturaRequerida) {
		this.idToCoberturaRequerida = idToCoberturaRequerida;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof CoberturaRequeridaId))
			return false;
		CoberturaRequeridaId castOther = (CoberturaRequeridaId) other;

		return ((this.getIdToCobertura() == castOther.getIdToCobertura()) || (this
				.getIdToCobertura() != null
				&& castOther.getIdToCobertura() != null && this
				.getIdToCobertura().equals(castOther.getIdToCobertura())))
				&& ((this.getIdToCoberturaRequerida() == castOther
						.getIdToCoberturaRequerida()) || (this
						.getIdToCoberturaRequerida() != null
						&& castOther.getIdToCoberturaRequerida() != null && this
						.getIdToCoberturaRequerida().equals(
								castOther.getIdToCoberturaRequerida())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToCobertura() == null ? 0 : this.getIdToCobertura()
						.hashCode());
		result = 37
				* result
				+ (getIdToCoberturaRequerida() == null ? 0 : this
						.getIdToCoberturaRequerida().hashCode());
		return result;
	}

}