package mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento;

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for DescuentoVarioTipoPolizaFacade.
 * @author MyEclipse Persistence Tools
 */

public interface DescuentoVarioTipoPolizaFacadeRemote {
	/**
	 Perform an initial save of a previously unsaved DescuentoVarioTipoPolizaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DescuentoVarioTipoPolizaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
   public void save(DescuentoVarioTipoPolizaDTO entity);
   /**
	 Delete a persistent DescuentoVarioTipoPolizaDTO entity.
	  @param entity DescuentoVarioTipoPolizaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
   public void delete(DescuentoVarioTipoPolizaDTO entity);
  /**
	 Persist a previously saved DescuentoVarioTipoPolizaDTO entity and return it or a copy of it to the sender. 
	 A copy of the DescuentoVarioTipoPolizaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DescuentoVarioTipoPolizaDTO entity to update
	 @return DescuentoVarioTipoPolizaDTO the persisted DescuentoVarioTipoPolizaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public DescuentoVarioTipoPolizaDTO update(DescuentoVarioTipoPolizaDTO entity);
	public DescuentoVarioTipoPolizaDTO findById( DescuentoVarioTipoPolizaId id);
	 /**
	 * Find all DescuentoVarioTipoPolizaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DescuentoVarioTipoPolizaDTO property to query
	  @param value the property value to match
	  	  @return List<DescuentoVarioTipoPolizaDTO> found by query
	 */
	public List<DescuentoVarioTipoPolizaDTO> findByProperty(String propertyName, Object value);
	/**
	 * Find all DescuentoVarioTipoPolizaDTO entities.
	  	  @return List<DescuentoVarioTipoPolizaDTO> all DescuentoVarioTipoPolizaDTO entities
	 */
	public List<DescuentoVarioTipoPolizaDTO> findAll();
}
