package mx.com.afirme.midas.producto.configuracion.aumento.exclusion;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * ExclusionAumentoVarioProductoId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class ExclusionAumentoVarioProductoId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idtoproducto;
     private BigDecimal idtoaumentovario;
     private BigDecimal idtotipopoliza;


    // Constructors

    /** default constructor */
    public ExclusionAumentoVarioProductoId() {
    }

    
    /** full constructor */
    public ExclusionAumentoVarioProductoId(BigDecimal idtoproducto, BigDecimal idtoaumentovario, BigDecimal idtotipopoliza) {
        this.idtoproducto = idtoproducto;
        this.idtoaumentovario = idtoaumentovario;
        this.idtotipopoliza = idtotipopoliza;
    }

   
    // Property accessors

    @Column(name="IDTOPRODUCTO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtoproducto() {
        return this.idtoproducto;
    }
    
    public void setIdtoproducto(BigDecimal idtoproducto) {
        this.idtoproducto = idtoproducto;
    }

    @Column(name="IDTOAUMENTOVARIO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtoaumentovario() {
        return this.idtoaumentovario;
    }
    
    public void setIdtoaumentovario(BigDecimal idtoaumentovario) {
        this.idtoaumentovario = idtoaumentovario;
    }

    @Column(name="IDTOTIPOPOLIZA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtotipopoliza() {
        return this.idtotipopoliza;
    }
    
    public void setIdtotipopoliza(BigDecimal idtotipopoliza) {
        this.idtotipopoliza = idtotipopoliza;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof ExclusionAumentoVarioProductoId) ) return false;
		 ExclusionAumentoVarioProductoId castOther = ( ExclusionAumentoVarioProductoId ) other; 
         
		 return ( (this.getIdtoproducto()==castOther.getIdtoproducto()) || ( this.getIdtoproducto()!=null && castOther.getIdtoproducto()!=null && this.getIdtoproducto().equals(castOther.getIdtoproducto()) ) )
 && ( (this.getIdtoaumentovario()==castOther.getIdtoaumentovario()) || ( this.getIdtoaumentovario()!=null && castOther.getIdtoaumentovario()!=null && this.getIdtoaumentovario().equals(castOther.getIdtoaumentovario()) ) )
 && ( (this.getIdtotipopoliza()==castOther.getIdtotipopoliza()) || ( this.getIdtotipopoliza()!=null && castOther.getIdtotipopoliza()!=null && this.getIdtotipopoliza().equals(castOther.getIdtotipopoliza()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdtoproducto() == null ? 0 : this.getIdtoproducto().hashCode() );
         result = 37 * result + ( getIdtoaumentovario() == null ? 0 : this.getIdtoaumentovario().hashCode() );
         result = 37 * result + ( getIdtotipopoliza() == null ? 0 : this.getIdtotipopoliza().hashCode() );
         return result;
   }   





}