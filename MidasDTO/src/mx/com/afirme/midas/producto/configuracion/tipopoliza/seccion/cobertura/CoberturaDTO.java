package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento.exclusion.ExclusionAumentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.exclusion.ExclusionRecargoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.AumentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.exclusion.ExclusionAumentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.coaseguro.CoaseguroCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.deducible.DeducibleCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.DescuentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.exclusion.CoberturaExcluidaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.RecargoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.exclusion.ExclusionRecargoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.requerida.CoberturaRequeridaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.sumaaseguradaadicional.SumaAseguradaAdicionalCoberturaDTO;
import mx.com.afirme.midas.sistema.constantes.ConstantesCotizacion;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducibles.NegocioDeducibleCob;

/**
 * CoberturaDTO entity. @author Jorge Cano
 */
@Entity(name = "CoberturaDTO")
@Table(name = "TOCOBERTURA", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = {
		"CODIGOCOBERTURA", "VERSIONCOBERTURA" }))
public class CoberturaDTO implements java.io.Serializable, Entidad {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToCobertura;
	private SubRamoDTO subRamoDTO;
	private String codigo;
	private Integer version;
	private String descripcion;
	private String nombreComercial;
	private String claveTipoSumaAsegurada;
	private String claveTipoCoaseguro;
	private Double valorMinimoCoaseguro;
	private Double valorMaximoCoaseguro;
	private String claveTipoLimiteCoaseguro;
	private Double valorMinimoLimiteCoaseguro;
	private Double valorMaximoLimiteCoaseguro;
	private String claveTipoDeducible;
	private Double valorMinimoDeducible;
	private Double valorMaximoDeducible;
	private String claveTipoLimiteDeducible;
	private Double valorMinimoLimiteDeducible;
	private Double valorMaximoLimiteDeducible;
	private String claveDesglosaRiesgos;
	private String claveMostrarPanCobertura;
	private String claveMostrarPanDescripcion;
	private String claveMostrarPanSumaAsegurada;
	private String claveMostrarPanCoaseguro;
	private String claveMostrarPanDeducible;
	private String claveMostrarPanPrimaNeta;
	private String claveMostrarCotCobertura;
	private String claveMostrarCotDescripcion;
	private String claveMostrarCotSumaAsegurada;
	private String claveMostrarCotCoaseguro;
	private String claveMostrarCotDeducible;
	private String claveMostrarCotPrimaNeta;
	private String claveMostrarPolCobertura;
	private String claveMostrarPolDescripcion;
	private String claveMostrarPolSumaAsegurada;
	private String claveMostrarPolCoaseguro;
	private String claveMostrarPolDeducible;
	private String claveMostrarPolPrimaNeta;
	private BigDecimal numeroSecuencia;
	private String claveAutorizacion;
	private String claveImporteCero;
	/*private String claveActivoConfiguracion;
	private String claveActivoProduccion;*/
	private String claveTipoCalculo;
	
	private Short claveEstatus;
	private Short claveActivo;
	private Date fechaCreacion;
	private Date fechaModificacion;
    private String codigoUsuarioCreacion;
	private String codigoUsuarioModificacion;
	private Short claveIgualacion;
	private Short clavePrimerRiesgo;
	
	private List<ExclusionRecargoVarioTipoPolizaDTO> exclusionRecargoVarioTipoPolizaDTOs = new  ArrayList<ExclusionRecargoVarioTipoPolizaDTO> (1);
	private List<ExclusionRecargoVarioCoberturaDTO> exclusionRecargoVarioCoberturaDTOs = new ArrayList<ExclusionRecargoVarioCoberturaDTO>(1);
	private List<RecargoVarioCoberturaDTO> recargoVarioCoberturaDTOs = new ArrayList<RecargoVarioCoberturaDTO>(1);
	
	private List<ExclusionAumentoVarioTipoPolizaDTO> exclusionAumentoVarioTipoPolizaDTOs = new ArrayList<ExclusionAumentoVarioTipoPolizaDTO>(1);
	private List<ExclusionAumentoVarioCoberturaDTO> exclusionAumentoVarioCoberturaDTOs = new ArrayList<ExclusionAumentoVarioCoberturaDTO>(1);
	private List<AumentoVarioCoberturaDTO> aumentoVarioCoberturaDTOs = new ArrayList<AumentoVarioCoberturaDTO>(1);
	
	private List<CoberturaSeccionDTO> secciones = new ArrayList<CoberturaSeccionDTO>(1);
	private List<DescuentoVarioCoberturaDTO> descuentos = new ArrayList<DescuentoVarioCoberturaDTO>(1);
	private List<AumentoVarioCoberturaDTO> aumentos = new ArrayList<AumentoVarioCoberturaDTO>(1);
	private List<RecargoVarioCoberturaDTO> recargos = new ArrayList<RecargoVarioCoberturaDTO>(1);
	private List<CoberturaExcluidaDTO> coberturasExcluidas = new ArrayList<CoberturaExcluidaDTO>(1);
	private List<CoberturaRequeridaDTO> coberturasRequeridas = new ArrayList<CoberturaRequeridaDTO>(1);
	private List<DeducibleCoberturaDTO> deducibles = new ArrayList<DeducibleCoberturaDTO>(1);
	private List<CoaseguroCoberturaDTO> coaseguros = new ArrayList<CoaseguroCoberturaDTO>(1);
	private String descripcionRegistroCNSF;
	private BigDecimal idControlArchivoRegistroCNSF;
	private BigDecimal idCoberturaSumaAsegurada;
	private String claveFuenteSumaAsegurada;
	private String coberturaEsPropia;
	private Double valMinPrimaCobertura;
	private String coberturaAplicaDescuentoRecargo;
//	private String fechaInicioVigencia;
	private List<NegocioDeducibleCob> negocioDeduciblesPP = new ArrayList<NegocioDeducibleCob>(1);
	
	private BigDecimal idTcRamo;
	
	private Date fechaRegistro;
    private String numeroRegistro;
	
	/**
	 * La leyenda a mostrar. Por ejemplo: "Amparada". El default si no se especifica es segun el tipo de suma
	 * asegurada que se seleccione por ejemplo. Si se seleeciona tipo de suma asegurada "Amparada" entonces la leyenda
	 * seria "Amparada" a pesar de que no se especifique. 
	 * (CASA)
	 */
	private String leyenda;
	/**
	 * El tipo de valor de la suma asegurada. 
	 * 1 = Monetario (Default)
	 * 2 = DSMVDF
	 * 3 = Derivada ( tipo sublimite )
	 * (CASA)
	 */
	private Integer claveTipoValorSumaAsegurada;
	/**
	 * Indica si la suma asegurada es editable. 
	 * Esto se utiliza para el cotizador casa. 
	 * (CASA)
	 */
	private Boolean editable;
	
	/**
	 * Especifica el factor minimo con el cual se definen los rangos
	 * de una cobertura de tipo "Sublimite" 
	 * (CASA)
	 */
	private BigDecimal factorMinSumaAsegurada;
	/**
	 * Especifica el factor maximo con el cual se definen los rangos
	 * de una cobertura de tipo "Sublimite" 
	 * (CASA)
	 */
	private BigDecimal factorMaxSumaAsegurada;
	/**
	 * Especifica el factor con el cual se calcula la suma asegurada para
	 * una cobertura sublimite. Esto aplica para centro de negocios. (CASA)
	 */
	private BigDecimal factorSumaAsegurada;
	
	/**
	 * Especifica la suma asegurada minima para las coberturas basicas.
	 * No es necesario especificar ambos. Se puede especificar solo minimo
	 * o solo maximo.
	 * (CASA)
	 */
	private BigDecimal minSumaAsegurada;
	
	/**
	 * Especifica la suma asegurada maxima para las coberturas basicas.
	 * No es necesario especificar ambos. Se puede especificar solo minimo
	 * o solo maximo.
	 * (CASA)
	 */
	private BigDecimal maxSumaAsegurada;
	
	private String tipoConfiguracion;
	
	private String reinstalable;
	
	private String seguroObligatorio;
	
	private List<SumaAseguradaAdicionalCoberturaDTO> sumasAseguradasAdicionales = new ArrayList<SumaAseguradaAdicionalCoberturaDTO>(1);
		
	private BigDecimal factorRecargo; 
	
	//Constantes
	
	public static final int TIPO_VALOR_SA_MONETARIO = 1;
	public static final int TIPO_VALOR_SA_DSMGVDF = 2;
	public static final int TIPO_VALOR_SA_SUBLIMITE = 3;
	
	public static final short OBLIGATORIO = 0;
	public static final short OPCIONAL_DEFAULT = 1;		
	public static final short OPCIONAL = 2;	
	
	public static final String CLAVE_FUENTE_SA_VALOR_PROPORCIONADO = "0";
	public static final String CLAVE_FUENTE_SA_VALOR_COMERCIAL = "1";
	public static final String CLAVE_FUENTE_SA_VALOR_FACTURA = "2";
	public static final String CLAVE_FUENTE_SA_VALOR_CONVENIDO = "9";
	public static final String CLAVE_FUENTE_SA_AMPARADA = "3";
	public static final String CLAVE_FUENTE_SA_VALOR_CARATULA = "10";

	public static final int VALOR_PROPORCIONADO = 0;
	public static final int VALOR_COMERCIAL = 1;
	public static final int VALOR_FACTURA = 2;
	public static final int VALOR_CONVENIDO = 9;
	public static final int VALOR_AMPARADA = 3;
	public static final int VALOR_CARATULA = 10;
	
	public static final String RESPONSABILIDAD_CIVIL_AUTOS_NOMBRE_COMERCIAL = "Responsabilidad Civil";
	public static final String DANOS_OCASIONADOS_CARGA_NOMBRE_COMERCIAL = "Daños Ocasionados por la Carga";
	public static final String DANOS_MATERIALES_NOMBRE_COMERCIAL = "Da\u00F1os Materiales";
	public static final String NOMBRE_LIMITE_RC_VIAJERO = "RESPONSABILIDAD CIVIL VIAJERO";
	public static final String NOMBRE_RT_CONVENIDO = "ROBO TOTAL VALOR CONVENIDO";
	public static final String NOMBRE_DM_CONVENIDO = "DA\u00d1OS MATERIALES VALOR CONVENIDO";
	public static final String NOMBRE_RT = "ROBO TOTAL";
	public static final String NOMBRE_DM = "DA\u00d1OS MATERIALES";
	public static final String GASTOS_MEDICOS_NOMBRE_COMERCIAL = "GASTOS M\u00C9DICOS";
	
	public static final BigDecimal IDTOCOBERTURA_DANIOSOCASIONADOSPORLACARGA = new BigDecimal(2840);
	public static final BigDecimal IDTOCOBERTURA_RESPONSABILIDAD_CIVIL_USA_CANADA_AUT_FRO = new BigDecimal(3840);
	public static final BigDecimal IDTOCOBERTURA_RESPONSABILIDAD_CIVIL_USA_CANADA_CAM_RES = new BigDecimal(2850);
	public static final BigDecimal IDTOCOBERTURA_RESPONSABILIDAD_CIVIL_USA_CANADA_AUT_RES = new BigDecimal(2640);
	public static final BigDecimal IDTOCOBERTURA_RESPONSABILIDAD_CIVIL_AUT_RES = new BigDecimal(2530);
	public static final BigDecimal IDTOCOBERTURA_RESPONSABILIDAD_CIVIL_AUT_RES_PICKUP = new BigDecimal(2730);
	public static final BigDecimal IDTOCOBERTURA_DANIOS_MATERIALES = new BigDecimal(2510);
	public static final BigDecimal IDTOCOBERTURA_ROBO_TOTAL = new BigDecimal(2520);
	public static final BigDecimal IDTOCOBERTURA_DANIOS_MATERIALES_VALOR_CONVENIDO = new BigDecimal(4816);
	public static final BigDecimal IDTOCOBERTURA_ROBO_TOTAL_VALOR_CONVENIDO = new BigDecimal(4817);
	public static final BigDecimal IDTOCOBERTURA_DANIOS_MATERIALES_VF = new BigDecimal(4818);
	public static final BigDecimal IDTOCOBERTURA_ROBO_VF = new BigDecimal(4819);
	public static final BigDecimal IDTOCOBERTURA_DANIOS_MATERIALES_CAMIONES = new BigDecimal(2710);
	public static final BigDecimal IDTOCOBERTURA_ROBO_CAMIONES = new BigDecimal(2720);
	

	public static final String CLAVE_TIPO_SUMA_ASEGURADA_AMPARADA = "2";
	public static final String CLAVE_TIPO_SUMA_ASEGURADA_LUC = "5";
	public static final String DESCRIPCION_TIPO_SUMA_ASEGURADA_AMPARADA = "Amparada";
	public static final String DESCRIPCION_TIPO_SA_VALOR_COMERCIAL =  "Valor Comercial";
	
	public static final int CLAVE_TIPO_DEDUCIBLE_PCTE = 1;
	
	public static final String CLAVE_CALCULO_DANOS_MATERIALES = "DM";
	public static final String CLAVE_CALCULO_ROBO_TOTAL = "RT";
	
	// Constructors

	/** default constructor */
	public CoberturaDTO() {
		if (this.subRamoDTO == null) {
			this.subRamoDTO = new SubRamoDTO();
		}
		this.claveEstatus=new Short("1");
		this.claveActivo= new Short("1");
		this.fechaCreacion=new Date();
		this.fechaModificacion=new Date();
		this.codigoUsuarioCreacion="enDB";
		this.codigoUsuarioModificacion="enDB";
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTOCOBERTURA_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOCOBERTURA_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOCOBERTURA_SEQ_GENERADOR")
	@Column(name = "IDTOCOBERTURA", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCobertura() {
		return this.idToCobertura;
	}

	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCSUBRAMO", nullable = false)
	public SubRamoDTO getSubRamoDTO() {
		return this.subRamoDTO;
	}

	public void setSubRamoDTO(SubRamoDTO subRamoDTO) {
		this.subRamoDTO = subRamoDTO;
	}

	@Column(name = "CODIGOCOBERTURA", nullable = false, length = 8)
	public String getCodigo() {
		return this.codigo!=null?this.codigo.toUpperCase():this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
		
	@Column(name = "VERSIONCOBERTURA", nullable = false)
	public Integer getVersion() {
		return this.version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	@Column(name = "DESCRIPCIONCOBERTURA", nullable = false, length = 200)
	public String getDescripcion() {
		return this.descripcion!=null?this.descripcion.toUpperCase():this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "NOMBRECOMERCIALCOBERTURA", nullable = false, length = 100)
	public String getNombreComercial() {
		return this.nombreComercial!=null?this.nombreComercial.toUpperCase():this.nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}

	@Column(name = "CLAVETIPOSUMAASEGURADA", nullable = false, precision = 4, scale = 0)
	public String getClaveTipoSumaAsegurada() {
		return this.claveTipoSumaAsegurada;
	}

	public void setClaveTipoSumaAsegurada(String claveTipoSumaAsegurada) {
		this.claveTipoSumaAsegurada = claveTipoSumaAsegurada;
	}

	@Column(name = "CLAVETIPOCOASEGURO", nullable = false, precision = 4, scale = 0)
	public String getClaveTipoCoaseguro() {
		return this.claveTipoCoaseguro;
	}

	public void setClaveTipoCoaseguro(String claveTipoCoaseguro) {
		this.claveTipoCoaseguro = claveTipoCoaseguro;
	}

	@Column(name = "VALORMINIMOCOASEGURO", nullable = false, precision = 16)
	public Double getValorMinimoCoaseguro() {
		return this.valorMinimoCoaseguro;
	}

	public void setValorMinimoCoaseguro(Double valorMinimoCoaseguro) {
		this.valorMinimoCoaseguro = valorMinimoCoaseguro;
	}

	@Column(name = "VALORMAXIMOCOASEGURO", nullable = false, precision = 16)
	public Double getValorMaximoCoaseguro() {
		return this.valorMaximoCoaseguro;
	}

	public void setValorMaximoCoaseguro(Double valorMaximoCoaseguro) {
		this.valorMaximoCoaseguro = valorMaximoCoaseguro;
	}

	@Column(name = "CLAVETIPOLIMITECOASEGURO", nullable = false, precision = 4, scale = 0)
	public String getClaveTipoLimiteCoaseguro() {
		return this.claveTipoLimiteCoaseguro;
	}

	public void setClaveTipoLimiteCoaseguro(String claveTipoLimiteCoaseguro) {
		this.claveTipoLimiteCoaseguro = claveTipoLimiteCoaseguro;
	}

	@Column(name = "VALORMINIMOLIMITECOASEGURO", nullable = false, precision = 16)
	public Double getValorMinimoLimiteCoaseguro() {
		return this.valorMinimoLimiteCoaseguro;
	}

	public void setValorMinimoLimiteCoaseguro(Double valorMinimoLimiteCoaseguro) {
		this.valorMinimoLimiteCoaseguro = valorMinimoLimiteCoaseguro;
	}

	@Column(name = "VALORMAXIMOLIMITECOASEGURO", nullable = false, precision = 16)
	public Double getValorMaximoLimiteCoaseguro() {
		return this.valorMaximoLimiteCoaseguro;
	}

	public void setValorMaximoLimiteCoaseguro(Double valorMaximoLimiteCoaseguro) {
		this.valorMaximoLimiteCoaseguro = valorMaximoLimiteCoaseguro;
	}

	@Column(name = "CLAVETIPODEDUCIBLE", nullable = false, precision = 4, scale = 0)
	public String getClaveTipoDeducible() {
		return this.claveTipoDeducible;
	}

	public void setClaveTipoDeducible(String claveTipoDeducible) {
		this.claveTipoDeducible = claveTipoDeducible;
	}

	@Column(name = "VALORMINIMODEDUCIBLE", nullable = false, precision = 16)
	public Double getValorMinimoDeducible() {
		return this.valorMinimoDeducible;
	}

	public void setValorMinimoDeducible(Double valorMinimoDeducible) {
		this.valorMinimoDeducible = valorMinimoDeducible;
	}

	@Column(name = "VALORMAXIMODEDUCIBLE", nullable = false, precision = 16)
	public Double getValorMaximoDeducible() {
		return this.valorMaximoDeducible;
	}

	public void setValorMaximoDeducible(Double valorMaximoDeducible) {
		this.valorMaximoDeducible = valorMaximoDeducible;
	}

	@Column(name = "CLAVETIPOLIMITEDEDUCIBLE", nullable = false, precision = 4, scale = 0)
	public String getClaveTipoLimiteDeducible() {
		return this.claveTipoLimiteDeducible;
	}

	public void setClaveTipoLimiteDeducible(String claveTipoLimiteDeducible) {
		this.claveTipoLimiteDeducible = claveTipoLimiteDeducible;
	}

	@Column(name = "VALORMINIMOLIMITEDEDUCIBLE", nullable = false, precision = 16)
	public Double getValorMinimoLimiteDeducible() {
		return this.valorMinimoLimiteDeducible;
	}

	public void setValorMinimoLimiteDeducible(Double valorMinimoLimiteDeducible) {
		this.valorMinimoLimiteDeducible = valorMinimoLimiteDeducible;
	}

	@Column(name = "VALORMAXIMOLIMITEDEDUCIBLE", nullable = false, precision = 16)
	public Double getValorMaximoLimiteDeducible() {
		return this.valorMaximoLimiteDeducible;
	}

	public void setValorMaximoLimiteDeducible(Double valorMaximoLimiteDeducible) {
		this.valorMaximoLimiteDeducible = valorMaximoLimiteDeducible;
	}

	@Column(name = "CLAVEDESGLOSARIESGOS", nullable = false, precision = 4, scale = 0)
	public String getClaveDesglosaRiesgos() {
		return this.claveDesglosaRiesgos;
	}

	public void setClaveDesglosaRiesgos(String claveDesglosaRiesgos) {
		this.claveDesglosaRiesgos = claveDesglosaRiesgos;
	}

	@Column(name = "CLAVEMOSTRARPANCOBERTURA", nullable = false, precision = 4, scale = 0)
	public String getClaveMostrarPanCobertura() {
		return this.claveMostrarPanCobertura;
	}

	public void setClaveMostrarPanCobertura(String claveMostrarPanCobertura) {
		this.claveMostrarPanCobertura = claveMostrarPanCobertura;
	}

	@Column(name = "CLAVEMOSTRARPANDESCRIPCION", nullable = false, precision = 4, scale = 0)
	public String getClaveMostrarPanDescripcion() {
		return this.claveMostrarPanDescripcion;
	}

	public void setClaveMostrarPanDescripcion(String claveMostrarPanDescripcion) {
		this.claveMostrarPanDescripcion = claveMostrarPanDescripcion;
	}

	@Column(name = "CLAVEMOSTRARPANSUMAASEGURADA", nullable = false, precision = 4, scale = 0)
	public String getClaveMostrarPanSumaAsegurada() {
		return this.claveMostrarPanSumaAsegurada;
	}

	public void setClaveMostrarPanSumaAsegurada(
			String claveMostrarPanSumaAsegurada) {
		this.claveMostrarPanSumaAsegurada = claveMostrarPanSumaAsegurada;
	}

	@Column(name = "CLAVEMOSTRARPANCOASEGURO", nullable = false, precision = 4, scale = 0)
	public String getClaveMostrarPanCoaseguro() {
		return this.claveMostrarPanCoaseguro;
	}

	public void setClaveMostrarPanCoaseguro(String claveMostrarPanCoaseguro) {
		this.claveMostrarPanCoaseguro = claveMostrarPanCoaseguro;
	}

	@Column(name = "CLAVEMOSTRARPANDEDUCIBLE", nullable = false, precision = 4, scale = 0)
	public String getClaveMostrarPanDeducible() {
		return this.claveMostrarPanDeducible;
	}

	public void setClaveMostrarPanDeducible(String claveMostrarPanDeducible) {
		this.claveMostrarPanDeducible = claveMostrarPanDeducible;
	}

	@Column(name = "CLAVEMOSTRARPANPRIMANETA", nullable = false, precision = 4, scale = 0)
	public String getClaveMostrarPanPrimaNeta() {
		return this.claveMostrarPanPrimaNeta;
	}

	public void setClaveMostrarPanPrimaNeta(String claveMostrarPanPrimaNeta) {
		this.claveMostrarPanPrimaNeta = claveMostrarPanPrimaNeta;
	}

	@Column(name = "CLAVEMOSTRARCOTCOBERTURA", nullable = false, precision = 4, scale = 0)
	public String getClaveMostrarCotCobertura() {
		return this.claveMostrarCotCobertura;
	}

	public void setClaveMostrarCotCobertura(String claveMostrarCotCobertura) {
		this.claveMostrarCotCobertura = claveMostrarCotCobertura;
	}

	@Column(name = "CLAVEMOSTRARCOTDESCRIPCION", nullable = false, precision = 4, scale = 0)
	public String getClaveMostrarCotDescripcion() {
		return this.claveMostrarCotDescripcion;
	}

	public void setClaveMostrarCotDescripcion(String claveMostrarCotDescripcion) {
		this.claveMostrarCotDescripcion = claveMostrarCotDescripcion;
	}

	@Column(name = "CLAVEMOSTRARCOTSUMAASEGURADA", nullable = false, precision = 4, scale = 0)
	public String getClaveMostrarCotSumaAsegurada() {
		return this.claveMostrarCotSumaAsegurada;
	}

	public void setClaveMostrarCotSumaAsegurada(
			String claveMostrarCotSumaAsegurada) {
		this.claveMostrarCotSumaAsegurada = claveMostrarCotSumaAsegurada;
	}

	@Column(name = "CLAVEMOSTRARCOTCOASEGURO", nullable = false, precision = 4, scale = 0)
	public String getClaveMostrarCotCoaseguro() {
		return this.claveMostrarCotCoaseguro;
	}

	public void setClaveMostrarCotCoaseguro(String claveMostrarCotCoaseguro) {
		this.claveMostrarCotCoaseguro = claveMostrarCotCoaseguro;
	}

	@Column(name = "CLAVEMOSTRARCOTDEDUCIBLE", nullable = false, precision = 4, scale = 0)
	public String getClaveMostrarCotDeducible() {
		return this.claveMostrarCotDeducible;
	}

	public void setClaveMostrarCotDeducible(String claveMostrarCotDeducible) {
		this.claveMostrarCotDeducible = claveMostrarCotDeducible;
	}

	@Column(name = "CLAVEMOSTRARCOTPRIMANETA", nullable = false, precision = 4, scale = 0)
	public String getClaveMostrarCotPrimaNeta() {
		return this.claveMostrarCotPrimaNeta;
	}

	public void setClaveMostrarCotPrimaNeta(String claveMostrarCotPrimaNeta) {
		this.claveMostrarCotPrimaNeta = claveMostrarCotPrimaNeta;
	}

	@Column(name = "CLAVEMOSTRARPOLCOBERTURA", nullable = false, precision = 4, scale = 0)
	public String getClaveMostrarPolCobertura() {
		return this.claveMostrarPolCobertura;
	}

	public void setClaveMostrarPolCobertura(String claveMostrarPolCobertura) {
		this.claveMostrarPolCobertura = claveMostrarPolCobertura;
	}

	@Column(name = "CLAVEMOSTRARPOLDESCRIPCION", nullable = false, precision = 4, scale = 0)
	public String getClaveMostrarPolDescripcion() {
		return this.claveMostrarPolDescripcion;
	}

	public void setClaveMostrarPolDescripcion(String claveMostrarPolDescripcion) {
		this.claveMostrarPolDescripcion = claveMostrarPolDescripcion;
	}

	@Column(name = "CLAVEMOSTRARPOLSUMAASEGURADA", nullable = false, precision = 4, scale = 0)
	public String getClaveMostrarPolSumaAsegurada() {
		return this.claveMostrarPolSumaAsegurada;
	}

	public void setClaveMostrarPolSumaAsegurada(
			String claveMostrarPolSumaAsegurada) {
		this.claveMostrarPolSumaAsegurada = claveMostrarPolSumaAsegurada;
	}

	@Column(name = "CLAVEMOSTRARPOLCOASEGURO", nullable = false, precision = 4, scale = 0)
	public String getClaveMostrarPolCoaseguro() {
		return this.claveMostrarPolCoaseguro;
	}

	public void setClaveMostrarPolCoaseguro(String claveMostrarPolCoaseguro) {
		this.claveMostrarPolCoaseguro = claveMostrarPolCoaseguro;
	}

	@Column(name = "CLAVEMOSTRARPOLDEDUCIBLE", nullable = false, precision = 4, scale = 0)
	public String getClaveMostrarPolDeducible() {
		return this.claveMostrarPolDeducible;
	}

	public void setClaveMostrarPolDeducible(String claveMostrarPolDeducible) {
		this.claveMostrarPolDeducible = claveMostrarPolDeducible;
	}

	@Column(name = "CLAVEMOSTRARPOLPRIMANETA", nullable = false, precision = 4, scale = 0)
	public String getClaveMostrarPolPrimaNeta() {
		return this.claveMostrarPolPrimaNeta;
	}

	public void setClaveMostrarPolPrimaNeta(String claveMostrarPolPrimaNeta) {
		this.claveMostrarPolPrimaNeta = claveMostrarPolPrimaNeta;
	}

	@Column(name = "NUMEROSECUENCIA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getNumeroSecuencia() {
		return this.numeroSecuencia;
	}

	public void setNumeroSecuencia(BigDecimal numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}

	@Column(name = "CLAVEAUTORIZACION", nullable = false, precision = 4, scale = 0)
	public String getClaveAutorizacion() {
		return this.claveAutorizacion;
	}

	public void setClaveAutorizacion(String claveAutorizacion) {
		this.claveAutorizacion = claveAutorizacion;
	}

	@Column(name = "CLAVEIMPORTECERO", nullable = false, precision = 4, scale = 0)
	public String getClaveImporteCero() {
		return this.claveImporteCero;
	}

	public void setClaveImporteCero(String claveImporteCero) {
		this.claveImporteCero = claveImporteCero;
	}
	
	/*@Column(name = "CLAVEACTIVOCONFIGURACION", nullable = false, precision = 4, scale = 0)
	public String getClaveActivoConfiguracion() {
		return this.claveActivoConfiguracion;
	}
	public void setClaveActivoConfiguracion(String claveActivoConfiguracion) {
		this.claveActivoConfiguracion = claveActivoConfiguracion;
	}*/

	/*@Column(name = "CLAVEACTIVOPRODUCCION", nullable = false, precision = 4, scale = 0)
	public String getClaveActivoProduccion() {
		return this.claveActivoProduccion;
	}
	public void setClaveActivoProduccion(String claveActivoProduccion) {
		this.claveActivoProduccion = claveActivoProduccion;
	}*/
	
	
	@Column(name = "CLAVEESTATUS", nullable = false, precision = 4, scale = 0)
	public Short getClaveEstatus() {
		return claveEstatus;
	}

	public void setClaveEstatus(Short claveEstatus) {
		this.claveEstatus = claveEstatus;
	}
	
	@Column(name = "CLAVEACTIVO", nullable = false, precision = 4, scale = 0)
	public Short getClaveActivo() {
		return claveActivo;
	}

	public void setClaveActivo(Short claveActivo) {
		this.claveActivo = claveActivo;
	}	
	
	@Temporal(TemporalType.DATE)
    @Column(name="FECHACREACION", length=7, nullable=false)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Temporal(TemporalType.DATE)
    @Column(name="FECHAMODIFICACION", length=7, nullable=false)
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Column(name="CODIGOUSUARIOCREACION", nullable=false, length=8)
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	
	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
	
	
	 @Column(name = "DESCRIPCIONREGISTROCNSF") 
	 public String getDescripcionRegistroCNSF() { 
		 return descripcionRegistroCNSF; 
	}
	
	 public void setDescripcionRegistroCNSF(String descripcionRegistroCNSF) {
		 this.descripcionRegistroCNSF = descripcionRegistroCNSF; 
	 }
	
	@Column(name="CODIGOUSUARIOMODIFICACION", nullable=false, length=8)
	public String getCodigoUsuarioModificacion() {
		return codigoUsuarioModificacion;
	}	
	
	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}	
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "coberturaDTO")
	public List<ExclusionRecargoVarioTipoPolizaDTO> getExclusionRecargoVarioTipoPolizaDTOs() {
		return exclusionRecargoVarioTipoPolizaDTOs;
	}

	public void setExclusionRecargoVarioTipoPolizaDTOs(
			List<ExclusionRecargoVarioTipoPolizaDTO> exclusionRecargoVarioTipoPolizaDTOs) {
		this.exclusionRecargoVarioTipoPolizaDTOs = exclusionRecargoVarioTipoPolizaDTOs;
	}
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "coberturaDTO")
	public List<ExclusionRecargoVarioCoberturaDTO> getExclusionRecargoVarioCoberturaDTOs() {
		return exclusionRecargoVarioCoberturaDTOs;
	}

	public void setExclusionRecargoVarioCoberturaDTOs(
			List<ExclusionRecargoVarioCoberturaDTO> exclusionRecargoVarioCoberturaDTOs) {
		this.exclusionRecargoVarioCoberturaDTOs = exclusionRecargoVarioCoberturaDTOs;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "coberturaDTO")
	public List<RecargoVarioCoberturaDTO> getRecargoVarioCoberturaDTOs() {
		return recargoVarioCoberturaDTOs;
	}

	public void setRecargoVarioCoberturaDTOs(
			List<RecargoVarioCoberturaDTO> recargoVarioCoberturaDTOs) {
		this.recargoVarioCoberturaDTOs = recargoVarioCoberturaDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "coberturaDTO")
	public List<ExclusionAumentoVarioTipoPolizaDTO> getExclusionAumentoVarioTipoPolizaDTOs() {
		return exclusionAumentoVarioTipoPolizaDTOs;
	}

	public void setExclusionAumentoVarioTipoPolizaDTOs(
			List<ExclusionAumentoVarioTipoPolizaDTO> exclusionAumentoVarioTipoPolizaDTOs) {
		this.exclusionAumentoVarioTipoPolizaDTOs = exclusionAumentoVarioTipoPolizaDTOs;
	}
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "coberturaDTO")
	public List<ExclusionAumentoVarioCoberturaDTO> getExclusionAumentoVarioCoberturaDTOs() {
		return exclusionAumentoVarioCoberturaDTOs;
	}

	public void setExclusionAumentoVarioCoberturaDTOs(
			List<ExclusionAumentoVarioCoberturaDTO> exclusionAumentoVarioCoberturaDTOs) {
		this.exclusionAumentoVarioCoberturaDTOs = exclusionAumentoVarioCoberturaDTOs;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "coberturaDTO")
	public List<AumentoVarioCoberturaDTO> getAumentoVarioCoberturaDTOs() {
		return aumentoVarioCoberturaDTOs;
	}

	public void setAumentoVarioCoberturaDTOs(
			List<AumentoVarioCoberturaDTO> aumentoVarioCoberturaDTOs) {
		this.aumentoVarioCoberturaDTOs = aumentoVarioCoberturaDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "coberturaDTO")
	public List<CoberturaSeccionDTO> getSecciones() {
		return secciones;
	}

	public void setSecciones(List<CoberturaSeccionDTO> secciones) {
		this.secciones = secciones;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "coberturaDTO")
	public List<DescuentoVarioCoberturaDTO> getDescuentos() {
		return descuentos;
	}

	public void setDescuentos(List<DescuentoVarioCoberturaDTO> descuentos) {
		this.descuentos = descuentos;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "coberturaDTO")
	public List<AumentoVarioCoberturaDTO> getAumentos() {
		return aumentos;
	}

	public void setAumentos(List<AumentoVarioCoberturaDTO> aumentos) {
		this.aumentos = aumentos;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "coberturaDTO")
	public List<RecargoVarioCoberturaDTO> getRecargos() {
		return recargos;
	}

	public void setRecargos(List<RecargoVarioCoberturaDTO> recargos) {
		this.recargos = recargos;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "coberturaBaseDTO")
	public List<CoberturaExcluidaDTO> getCoberturasExcluidas() {
		return coberturasExcluidas;
	}

	public void setCoberturasExcluidas(List<CoberturaExcluidaDTO> coberturasExcluidas) {
		this.coberturasExcluidas = coberturasExcluidas;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "coberturaBaseDTO")
	public List<CoberturaRequeridaDTO> getCoberturasRequeridas() {
		return coberturasRequeridas;
	}

	public void setCoberturasRequeridas(List<CoberturaRequeridaDTO> coberturasRequeridas) {
		this.coberturasRequeridas = coberturasRequeridas;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "coberturaDTO")
	public List<DeducibleCoberturaDTO> getDeducibles() {
		return deducibles;
	}

	public void setDeducibles(List<DeducibleCoberturaDTO> deducibles) {
		this.deducibles = deducibles;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "coberturaDTO")
	public List<CoaseguroCoberturaDTO> getCoaseguros() {
		return coaseguros;
	}

	public void setCoaseguros(List<CoaseguroCoberturaDTO> coaseguros) {
		this.coaseguros = coaseguros;
	}

	@Column(name = "IDCTRLARCHREGISTROCNSF")
	public BigDecimal getIdControlArchivoRegistroCNSF() {
		return idControlArchivoRegistroCNSF;
	}

	public void setIdControlArchivoRegistroCNSF(BigDecimal idControlArchivoRegistroCNSF) {
		this.idControlArchivoRegistroCNSF = idControlArchivoRegistroCNSF;
	}
	
	@Column(name = "CLAVEIGUALACION", nullable = false, precision = 4, scale = 0)
	public Short getClaveIgualacion() {
		return this.claveIgualacion;
	}

	public void setClaveIgualacion(Short claveIgualacion) {
		this.claveIgualacion = claveIgualacion;
	}

	@Column(name = "CLAVEPRIMERRIESGO", nullable = false, precision = 4, scale = 0)
	public Short getClavePrimerRiesgo() {
		return this.clavePrimerRiesgo;
	}

	public void setClavePrimerRiesgo(Short clavePrimerRiesgo) {
		this.clavePrimerRiesgo = clavePrimerRiesgo;
	}

	@Column(name = "IDCOBERTURASUMAASEGURADA", nullable = false)
	public BigDecimal getIdCoberturaSumaAsegurada() {
		return idCoberturaSumaAsegurada;
	}

	public void setIdCoberturaSumaAsegurada(BigDecimal idCoberturaSumaAsegurada) {
		this.idCoberturaSumaAsegurada = idCoberturaSumaAsegurada;
	}
	
	@Column(name = "LEYENDA")
	public String getLeyenda() {
		return leyenda;
	}
	
	public void setLeyenda(String leyenda) {
		this.leyenda = leyenda;
	}
	
	@Column(name="CLAVETIPOVALORSUMAASEGURADA")
	public Integer getClaveTipoValorSumaAsegurada() {
		return claveTipoValorSumaAsegurada;
	}
	
	public void setClaveTipoValorSumaAsegurada(
			Integer claveTipoValorSumaAsegurada) {
		this.claveTipoValorSumaAsegurada = claveTipoValorSumaAsegurada;
	}
	
	@Column(name="EDITABLE")
	public Boolean getEditable() {
		return editable;
	}
	
	public void setEditable(Boolean editable) {
		this.editable = editable;
	}

	@Column(name="FACTORMINSUMAASEGURADA", precision=16, scale=4)
	public BigDecimal getFactorMinSumaAsegurada() {
		return factorMinSumaAsegurada;
	}

	public void setFactorMinSumaAsegurada(BigDecimal factorMinSumaAsegurada) {
		this.factorMinSumaAsegurada = factorMinSumaAsegurada;
	}

	@Column(name="FACTORMAXSUMAASEGURADA", precision=16, scale=4)
	public BigDecimal getFactorMaxSumaAsegurada() {
		return factorMaxSumaAsegurada;
	}

	public void setFactorMaxSumaAsegurada(BigDecimal factorMaxSumaAsegurada) {
		this.factorMaxSumaAsegurada = factorMaxSumaAsegurada;
	}

	@Column(name="FACTORSUMAASEGURADA", precision=16, scale=4)
	public BigDecimal getFactorSumaAsegurada() {
		return factorSumaAsegurada;
	}

	public void setFactorSumaAsegurada(BigDecimal factorSumaAsegurada) {
		this.factorSumaAsegurada = factorSumaAsegurada;
	}
	
	@Column(name="MINSUMAASEGURADA", precision=16, scale=4)
	public BigDecimal getMinSumaAsegurada() {
		return minSumaAsegurada;
	}
	
	public void setMinSumaAsegurada(BigDecimal minSumaAsegurada) {
		this.minSumaAsegurada = minSumaAsegurada;
	}
	
	@Column(name="MAXSUMAASEGURADA", precision=16, scale=4)
	public BigDecimal getMaxSumaAsegurada() {
		return maxSumaAsegurada;
	}
	
	public void setMaxSumaAsegurada(BigDecimal maxSumaAsegurada) {
		this.maxSumaAsegurada = maxSumaAsegurada;
	}

	@Column(name = "CLAVECOBERTURAPROPIA", nullable = false, precision = 38, scale = 0)
	public String getCoberturaEsPropia() {
		return coberturaEsPropia;
	}

	public void setCoberturaEsPropia(String coberturaEsPropia) {
		this.coberturaEsPropia = coberturaEsPropia;
	}

	@Column(name = "VALORMINIMOPRIMA", nullable = false, precision = 16, scale = 2) 
	public Double getValMinPrimaCobertura() {
		return valMinPrimaCobertura;
	}

	public void setValMinPrimaCobertura(Double valMinPrimaCobertura) {
		this.valMinPrimaCobertura = valMinPrimaCobertura;
	}

	@Column(name = "CLAVEAPLICADCTORCGOMODIFDESC", nullable = false, precision = 4, scale = 0)
	public String getCoberturaAplicaDescuentoRecargo() {
		return coberturaAplicaDescuentoRecargo;
	}

	public void setCoberturaAplicaDescuentoRecargo(String coberturaAplicaDescuentoRecargo) {
		this.coberturaAplicaDescuentoRecargo = coberturaAplicaDescuentoRecargo;
	}

	@Column(name = "CLAVEFUENTESUMAASEGURADA", nullable = false, precision = 38, scale = 0)
	public String getClaveFuenteSumaAsegurada() {
		return claveFuenteSumaAsegurada;
	}

	public void setClaveFuenteSumaAsegurada(String claveFuenteSumaAsegurada) {
		this.claveFuenteSumaAsegurada = claveFuenteSumaAsegurada;
	}

	@Transient
	public List<NegocioDeducibleCob> getNegocioDeduciblesPP() {
		return negocioDeduciblesPP;
	}

	public void setNegocioDeduciblesPP(
			List<NegocioDeducibleCob> negocioDeduciblesPP) {
		this.negocioDeduciblesPP = negocioDeduciblesPP;
	}
	
	@Transient
	public BigDecimal getIdTcRamo() {
		return idTcRamo;
	}

	public void setIdTcRamo(BigDecimal idTcRamo) {
		this.idTcRamo = idTcRamo;
	}

	
	@Column(name = "TIPO_CONFIGURACION", length = 3)
	public String getTipoConfiguracion() {
		return tipoConfiguracion;
	}

	public void setTipoConfiguracion(String tipoConfiguracion) {
		this.tipoConfiguracion = tipoConfiguracion;
	}

	/**
	 * Define si la cobertura contiene la estructura y los datos necesarios para validar la suma asegurada en 
	 * funci�n de otra cobertura, la validaci�n considera lo siguiente: 
	 * 1. La cobertura es subl�mite o su claveTipoValor de suma asegurada es 3:CLAVE_SUMA_ASEGURADA_SUBLIMITE
	 * 2. Si se cumple lo anterior, validar si la cobertura tiene registrado un idCoberturaSumaAsegurada y 
	 * los factores m�nimo y m�ximo de suma asegurada.
	 * @return true, si la cobertura pasa las validaciones, de lo contrario false.
	 */
	public boolean aplicaValidacionSumaAseguradaSubLimite(){
		
		boolean aplicaSubLimite = (claveTipoSumaAsegurada != null && claveTipoSumaAsegurada.equals(ConstantesCotizacion.CLAVE_SUMA_ASEGURADA_SUBLIMITE) ||
				(claveTipoValorSumaAsegurada != null && claveTipoValorSumaAsegurada.intValue() == CoberturaDTO.TIPO_VALOR_SA_SUBLIMITE) );
		
		//validar si existen los campos necesarios para encontrar la cobertura basica
		
		if(aplicaSubLimite){
			
			aplicaSubLimite = (idCoberturaSumaAsegurada != null && factorMinSumaAsegurada != null && factorMaxSumaAsegurada != null);
		}
		
		return aplicaSubLimite;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
				
		if (obj instanceof BitemporalCoberturaSeccion) {
			BitemporalCoberturaSeccion bitemporalCoberturaSeccion = (BitemporalCoberturaSeccion) obj;
			return this.equals(bitemporalCoberturaSeccion.getEntidadContinuity().getCoberturaDTO());
		}
		
		if (getClass() != obj.getClass())
			return false;
		
		CoberturaDTO other = (CoberturaDTO) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.getIdToCobertura();
	}

	@Override
	public String getValue() {
		//  Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		//  Auto-generated method stub
		return null;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("idToCobertura: " + this.idToCobertura + ", ");
		sb.append("descripcion: " + this.descripcion);
		sb.append("]");
		
		return sb.toString();
	}

	public void setClaveTipoCalculo(String claveTipoCalculo) {
		this.claveTipoCalculo = claveTipoCalculo;
	}

	@Column(name = "CLAVETIPOCALCULO", nullable = false, precision = 6, scale = 0)
	public String getClaveTipoCalculo() {
		return claveTipoCalculo;
	}	
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "coberturaDTO")
	public List<SumaAseguradaAdicionalCoberturaDTO> getSumasAseguradasAdicionales() {
		return sumasAseguradasAdicionales;
	}

	public void setSumasAseguradasAdicionales(
			List<SumaAseguradaAdicionalCoberturaDTO> sumasAseguradasAdicionales) {
		this.sumasAseguradasAdicionales = sumasAseguradasAdicionales;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAREGISTRO")
	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	@Column(name = "NUMEROREGISTRO")
	public String getNumeroRegistro() {
		return numeroRegistro;
	}

	/**
	 * @param numeroRegistro el numeroRegistro a establecer
	 */
	public void setNumeroRegistro(String numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
	}
	
	@Column(name = "REINSTALABLE")
	public String getReinstalable() {
		return reinstalable;
	}

	public void setReinstalable(String reinstalable) {
		this.reinstalable = reinstalable;
	}

	
	public void setSeguroObligatorio(String seguroObligatorio) {
		this.seguroObligatorio = seguroObligatorio;
	}
	
	@Column(name = "SEGUROOBLIGATORIO")
	public String getSeguroObligatorio() {
		return seguroObligatorio;
	}
	
	@Column(name = "FACTORRECARGO")
	public BigDecimal getFactorRecargo() {
		return factorRecargo;
	}

	public void setFactorRecargo(BigDecimal factorRecargo) {
		this.factorRecargo = factorRecargo;
	}
	
}