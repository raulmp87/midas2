package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.exclusion;
// default package

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

/**
 * Remote interface for ExclusionDescuentoVarioCoberturaFacade.
 * @author MyEclipse Persistence Tools
 */

public interface ExclusionDescuentoVarioCoberturaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ExclusionDescuentoVarioCoberturaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ExclusionDescuentoVarioCoberturaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ExclusionDescuentoVarioCoberturaDTO entity);
    /**
	 Delete a persistent ExclusionDescuentoVarioCoberturaDTO entity.
	  @param entity ExclusionDescuentoVarioCoberturaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ExclusionDescuentoVarioCoberturaDTO entity);
   /**
	 Persist a previously saved ExclusionDescuentoVarioCoberturaDTO entity and return it or a copy of it to the sender. 
	 A copy of the ExclusionDescuentoVarioCoberturaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ExclusionDescuentoVarioCoberturaDTO entity to update
	 @return ExclusionDescuentoVarioCoberturaDTO the persisted ExclusionDescuentoVarioCoberturaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ExclusionDescuentoVarioCoberturaDTO update(ExclusionDescuentoVarioCoberturaDTO entity);
	public ExclusionDescuentoVarioCoberturaDTO findById( ExclusionDescuentoVarioCoberturaId id);
	 /**
	 * Find all ExclusionDescuentoVarioCoberturaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ExclusionDescuentoVarioCoberturaDTO property to query
	  @param value the property value to match
	  	  @return List<ExclusionDescuentoVarioCoberturaDTO> found by query
	 */
	public List<ExclusionDescuentoVarioCoberturaDTO> findByProperty(String propertyName, Object value);
	/**
	 * Find all ExclusionDescuentoVarioCoberturaDTO entities.
	  	  @return List<ExclusionDescuentoVarioCoberturaDTO> all ExclusionDescuentoVarioCoberturaDTO entities
	 */
	public List<ExclusionDescuentoVarioCoberturaDTO> findAll();
	
	/**
	 * Find a ExclusionDescuentoVarioCoberturaDTO entity with the specific
	 * received id�s.
	 * 
	 * @param BigDecimal idToCobertura.
	 * @param BigDecimal idToDescuentoVario.
	 * @param BigDecimal idToRiesgo.
	 * 
	 * @return List<ExclusionAumentoVarioCoberturaDTO> found by query
	 */
	public List<ExclusionDescuentoVarioCoberturaDTO> findByIDs(BigDecimal idToCobertura, BigDecimal idToDescuento, BigDecimal idToRiesgo);
	
	/**
	 * Encuentra los registros de ExclusionDescuentoVarioCoberturaDTO relacionados con la CoberturaSeccionDTO cuyos ID�s se reciben y que adem�s est�n 
	 * relacionados s�lo con Riesgos que no hayan sido borradas l�gicamente.
	  @param BigDecimal idToCobertura. El ID de la cobertura.
	  @param BigDecimal idToSeccion. El ID de la seccion.
	  @return List<ExclusionDescuentoVarioCoberturaDTO> encontrados por el query formado.
	 */
    public List<ExclusionDescuentoVarioCoberturaDTO> getVigentesPorIdCoberturaIdSeccion(BigDecimal idToCobertura,BigDecimal idToSeccion);
}