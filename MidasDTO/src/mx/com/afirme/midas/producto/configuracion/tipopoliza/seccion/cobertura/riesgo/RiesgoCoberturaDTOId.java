package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * RiesgoCoberturaDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class RiesgoCoberturaDTOId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToSeccion;
     private BigDecimal idToCobertura;
     private BigDecimal idToRiesgo;


    // Constructors

    /** default constructor */
    public RiesgoCoberturaDTOId() {
    }

    
    /** full constructor */
    public RiesgoCoberturaDTOId(BigDecimal idToSeccion, BigDecimal idToCobertura, BigDecimal idToRiesgo) {
        this.idToSeccion = idToSeccion;
        this.idToCobertura = idToCobertura;
        this.idToRiesgo = idToRiesgo;
    }

   
    // Property accessors

    @Column(name="IDTOSECCION", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToSeccion() {
        return this.idToSeccion;
    }
    
    public void setIdToSeccion(BigDecimal idToSeccion) {
        this.idToSeccion = idToSeccion;
    }

    @Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToCobertura() {
        return this.idToCobertura;
    }
    
    public void setIdToCobertura(BigDecimal idToCobertura) {
        this.idToCobertura = idToCobertura;
    }

    @Column(name="IDTORIESGO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToRiesgo() {
        return this.idToRiesgo;
    }
    
    public void setIdToRiesgo(BigDecimal idToRiesgo) {
        this.idToRiesgo = idToRiesgo;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof RiesgoCoberturaDTOId) ) return false;
		 RiesgoCoberturaDTOId castOther = ( RiesgoCoberturaDTOId ) other; 
         
		 return ( (this.getIdToSeccion()==castOther.getIdToSeccion()) || ( this.getIdToSeccion()!=null && castOther.getIdToSeccion()!=null && this.getIdToSeccion().equals(castOther.getIdToSeccion()) ) )
 && ( (this.getIdToCobertura()==castOther.getIdToCobertura()) || ( this.getIdToCobertura()!=null && castOther.getIdToCobertura()!=null && this.getIdToCobertura().equals(castOther.getIdToCobertura()) ) )
 && ( (this.getIdToRiesgo()==castOther.getIdToRiesgo()) || ( this.getIdToRiesgo()!=null && castOther.getIdToRiesgo()!=null && this.getIdToRiesgo().equals(castOther.getIdToRiesgo()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdToSeccion() == null ? 0 : this.getIdToSeccion().hashCode() );
         result = 37 * result + ( getIdToCobertura() == null ? 0 : this.getIdToCobertura().hashCode() );
         result = 37 * result + ( getIdToRiesgo() == null ? 0 : this.getIdToRiesgo().hashCode() );
         return result;
   }   





}