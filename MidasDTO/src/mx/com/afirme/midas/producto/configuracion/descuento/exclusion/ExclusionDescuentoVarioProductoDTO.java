package mx.com.afirme.midas.producto.configuracion.descuento.exclusion;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.descuento.DescuentoDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;

/**
 * ExclusionDescuentoVarioProductoDTO entity. @author José Luis Arellano
 */
@Entity
@Table(name="TREXCDESCUENTOVARPRODUCTO",schema="MIDAS")
public class ExclusionDescuentoVarioProductoDTO implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private ExclusionDescuentoVarioProductoId id;
    private DescuentoDTO descuentoDTO;
    private TipoPolizaDTO tipoPolizaDTO;
    private ProductoDTO productoDTO;
    
    @EmbeddedId
    @AttributeOverrides( {
        @AttributeOverride(name="idtoproducto", column=@Column(name="IDTOPRODUCTO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idtodescuentovario", column=@Column(name="IDTODESCUENTOVARIO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idtotipopoliza", column=@Column(name="IDTOTIPOPOLIZA", nullable=false, precision=22, scale=0) ) } )
    public ExclusionDescuentoVarioProductoId getId() {
        return this.id;
    }
    
    public void setId(ExclusionDescuentoVarioProductoId id) {
        this.id = id;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTODESCUENTOVARIO", nullable=false, insertable=false, updatable=false)
    public DescuentoDTO getDescuentoDTO() {
		return descuentoDTO;
	}


	public void setDescuentoDTO(DescuentoDTO descuentoDTO) {
		this.descuentoDTO = descuentoDTO;
	}

	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTOTIPOPOLIZA", nullable=false, insertable=false, updatable=false)
    public TipoPolizaDTO getTipoPolizaDTO() {
		return tipoPolizaDTO;
	}


	public void setTipoPolizaDTO(TipoPolizaDTO tipoPolizaDTO) {
		this.tipoPolizaDTO = tipoPolizaDTO;
	}
   
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTOPRODUCTO", nullable=false, insertable=false, updatable=false)
    public ProductoDTO getProductoDTO() {
		return productoDTO;
	}

	public void setProductoDTO(ProductoDTO productoDTO) {
		this.productoDTO = productoDTO;
	}
	
	public boolean equals(Object o) {
		if (o instanceof ExclusionDescuentoVarioProductoDTO) {
			ExclusionDescuentoVarioProductoDTO temp = (ExclusionDescuentoVarioProductoDTO) o;
			if (temp.getId().getIdtoproducto().intValue() == this.getId().getIdtoproducto().intValue()
					&& temp.getId().getIdtodescuentovario().intValue() == this.getId().getIdtodescuentovario().intValue()
					&& temp.getId().getIdtotipopoliza().intValue() == this.getId().getIdtotipopoliza().intValue()) {
				return true;
			}
		}
		return false;
	}

	public int hashCode() {
		int hash = 1;
	    hash = hash * 31 + this.getId().getIdtoproducto().hashCode();
	    hash = hash * 31 + this.getId().getIdtodescuentovario().hashCode();
	    hash = hash * 31 + this.getId().getIdtotipopoliza().hashCode();
	    return hash;
	}
}
