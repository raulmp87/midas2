package mx.com.afirme.midas.producto.configuracion.descuento;

// default package

import java.util.List;

import javax.ejb.Remote;

/**
 * Remote interface for DescuentoPorProductoDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface DescuentoVarioProductoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved DescuentoPorProductoDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            DescuentoPorProductoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(DescuentoVarioProductoDTO entity);

	/**
	 * Delete a persistent DescuentoPorProductoDTO entity.
	 * 
	 * @param entity
	 *            DescuentoPorProductoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(DescuentoVarioProductoDTO entity);

	/**
	 * Persist a previously saved DescuentoPorProductoDTO entity and return it
	 * or a copy of it to the sender. A copy of the DescuentoPorProductoDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            DescuentoPorProductoDTO entity to update
	 * @return DescuentoPorProductoDTO the persisted DescuentoPorProductoDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public DescuentoVarioProductoDTO update(DescuentoVarioProductoDTO entity);

	public DescuentoVarioProductoDTO findById(DescuentoVarioProductoId id);

	/**
	 * Find all DescuentoPorProductoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the DescuentoPorProductoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<DescuentoPorProductoDTO> found by query
	 */
	public List<DescuentoVarioProductoDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all DescuentoPorProductoDTO entities.
	 * 
	 * @return List<DescuentoPorProductoDTO> all DescuentoPorProductoDTO
	 *         entities
	 */
	public List<DescuentoVarioProductoDTO> findAll();
}