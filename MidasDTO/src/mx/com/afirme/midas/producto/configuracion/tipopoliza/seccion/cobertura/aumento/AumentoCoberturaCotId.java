package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class AumentoCoberturaCotId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1839623319062112420L;
	
	private Long idToCotizacion;
	private Long numeroInciso;
	private Long idToSeccion;
	private Long idToCobertura;
	private Long idToAumentoVario;
	
	@Column(name = "IDTOCOTIZACION", nullable = false)
	public Long getIdToCotizacion() {
		return idToCotizacion;
	}
	public void setIdToCotizacion(Long idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	
	@Column(name = "NUMEROINCISO", nullable = false)
	public Long getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(Long numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	
	@Column(name = "IDTOSECCION", nullable = false)
	public Long getIdToSeccion() {
		return idToSeccion;
	}
	public void setIdToSeccion(Long idToSeccion) {
		this.idToSeccion = idToSeccion;
	}
	
	@Column(name = "IDTOCOBERTURA", nullable = false)
	public Long getIdToCobertura() {
		return idToCobertura;
	}
	public void setIdToCobertura(Long idToCobertura) {
		this.idToCobertura = idToCobertura;
	}
	
	@Column(name = "IDTOAUMENTOVARIO", nullable = false)
	public Long getIdToAumentoVario() {
		return idToAumentoVario;
	}
	public void setIdToAumentoVario(Long idToAumentoVario) {
		this.idToAumentoVario = idToAumentoVario;
	}
	
	
   public boolean equals(Object other) {
	   if ( (this == other ) ) return true;
	   if ( (other == null ) ) return false;
	   if ( !(other instanceof AumentoCoberturaCotId) ) return false;
	   AumentoCoberturaCotId castOther = ( AumentoCoberturaCotId ) other; 
     
	   return ( (this.getIdToCobertura()==castOther.getIdToCobertura()) || ( this.getIdToCobertura()!=null && castOther.getIdToCobertura()!=null && this.getIdToCobertura().equals(castOther.getIdToCobertura()) ) )
	   && ((this.getNumeroInciso()==castOther.getNumeroInciso()) || ( this.getNumeroInciso()!=null && castOther.getNumeroInciso()!=null && this.getNumeroInciso().equals(castOther.getNumeroInciso()))) 
	   && ((this.getIdToSeccion()==castOther.getIdToSeccion()) || ( this.getIdToSeccion()!=null && castOther.getIdToSeccion()!=null && this.getIdToSeccion().equals(castOther.getIdToSeccion()))) 
	   && ((this.getIdToCotizacion()==castOther.getIdToCotizacion()) || ( this.getIdToCotizacion()!=null && castOther.getIdToCotizacion()!=null && this.getIdToCotizacion().equals(castOther.getIdToCotizacion()))) 
	   && ((this.getIdToAumentoVario()==castOther.getIdToAumentoVario()) || ( this.getIdToAumentoVario()!=null && castOther.getIdToAumentoVario()!=null && this.getIdToAumentoVario().equals(castOther.getIdToAumentoVario()) ) );
   }
	     
}
