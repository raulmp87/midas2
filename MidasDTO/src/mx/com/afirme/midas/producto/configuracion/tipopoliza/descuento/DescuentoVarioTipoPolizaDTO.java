package mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.descuento.DescuentoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;

/**
 * DescuentoVarioTipoPolizaDTO entity. @author José Luis Arellano
 */
@Entity
@Table(name="TODESCUENTOVARIOTIPOPOLIZA",schema="MIDAS")
public class DescuentoVarioTipoPolizaDTO implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DescuentoVarioTipoPolizaId id;
    private DescuentoDTO descuentoDTO;
    private TipoPolizaDTO tipoPolizaDTO;
    private Short claveobligatoriedad;
    private Short clavecomercialtecnico;
    private Short claveaplicareaseguro;
    private Double valor;
    
    @EmbeddedId
    @AttributeOverrides( {
      @AttributeOverride(name="idtotipopoliza", column=@Column(name="IDTOTIPOPOLIZA", nullable=false, precision=22, scale=0) ), 
      @AttributeOverride(name="idtodescuentovario", column=@Column(name="IDTODESCUENTOVARIO", nullable=false, precision=22, scale=0) ) } )
    public DescuentoVarioTipoPolizaId getId() {
        return this.id;
    }
    
    public void setId(DescuentoVarioTipoPolizaId id) {
        this.id = id;
    }
	
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTODESCUENTOVARIO", nullable=false, insertable=false, updatable=false)
    public DescuentoDTO getDescuentoDTO() {
		return descuentoDTO;
	}

	public void setDescuentoDTO(DescuentoDTO descuentoDTO) {
		this.descuentoDTO = descuentoDTO;
	}
  
    
    @Column(name="CLAVEOBLIGATORIEDAD", nullable=false, precision=4, scale=0)
    public Short getClaveobligatoriedad() {
        return this.claveobligatoriedad;
    }

	public void setClaveobligatoriedad(Short claveobligatoriedad) {
        this.claveobligatoriedad = claveobligatoriedad;
    }
    
    @Column(name="CLAVECOMERCIALTECNICO", nullable=false, precision=4, scale=0)
    public Short getClavecomercialtecnico() {
        return this.clavecomercialtecnico;
    }
    
    public void setClavecomercialtecnico(Short clavecomercialtecnico) {
        this.clavecomercialtecnico = clavecomercialtecnico;
    }
    
    @Column(name="CLAVEAPLICAREASEGURO", nullable=false, precision=4, scale=0)
    public Short getClaveaplicareaseguro() {
        return this.claveaplicareaseguro;
    }
    
    public void setClaveaplicareaseguro(Short claveaplicareaseguro) {
        this.claveaplicareaseguro = claveaplicareaseguro;
    }
    
    @Column(name="VALOR", nullable=false, precision=16)
    public Double getValor() {
        return this.valor;
    }
    
    public void setValor(Double valor) {
        this.valor = valor;
    }

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTOTIPOPOLIZA", nullable=false, insertable=false, updatable=false)
	public TipoPolizaDTO getTipoPolizaDTO() {
		return tipoPolizaDTO;
	}


	public void setTipoPolizaDTO(TipoPolizaDTO tipoPolizaDTO) {
		this.tipoPolizaDTO = tipoPolizaDTO;
	}
}
