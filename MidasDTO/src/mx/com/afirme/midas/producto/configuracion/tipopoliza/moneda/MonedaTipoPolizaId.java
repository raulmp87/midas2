package mx.com.afirme.midas.producto.configuracion.tipopoliza.moneda;

// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * MonedaTipoPolizaDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class MonedaTipoPolizaId implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToTipoPoliza;
	private BigDecimal idMoneda;

	// Constructors

	/** default constructor */
	public MonedaTipoPolizaId() {
	}

	// Property accessors

	@Column(name = "IDTOTIPOPOLIZA")
	public BigDecimal getIdToTipoPoliza() {
		return this.idToTipoPoliza;
	}

	public void setIdToTipoPoliza(BigDecimal idToTipoPoliza) {
		this.idToTipoPoliza = idToTipoPoliza;
	}

	@Column(name = "IDMONEDA")
	public BigDecimal getIdMoneda() {
		return this.idMoneda;
	}

	public void setIdMoneda(BigDecimal idMoneda) {
		this.idMoneda = idMoneda;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof MonedaTipoPolizaId))
			return false;
		MonedaTipoPolizaId castOther = (MonedaTipoPolizaId) other;

		return ((this.getIdToTipoPoliza() == castOther.getIdToTipoPoliza()) || (this
				.getIdToTipoPoliza() != null
				&& castOther.getIdToTipoPoliza() != null && this
				.getIdToTipoPoliza().equals(castOther.getIdToTipoPoliza())))
				&& ((this.getIdMoneda() == castOther.getIdMoneda()) || (this
						.getIdMoneda() != null
						&& castOther.getIdMoneda() != null && this
						.getIdMoneda().equals(castOther.getIdMoneda())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToTipoPoliza() == null ? 0 : this.getIdToTipoPoliza()
						.hashCode());
		result = 37 * result
				+ (getIdMoneda() == null ? 0 : this.getIdMoneda().hashCode());
		return result;
	}

}