package mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento.exclusion;


import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * ExclusionAumentoVarioTipoPolizaId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class ExclusionAumentoVarioTipoPolizaId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idtotipopoliza;
     private BigDecimal idtoaumentovario;
     private BigDecimal idtocobertura;


    // Constructors

    /** default constructor */
    public ExclusionAumentoVarioTipoPolizaId() {
    }

    
    /** full constructor */
    public ExclusionAumentoVarioTipoPolizaId(BigDecimal idtotipopoliza, BigDecimal idtoaumentovario, BigDecimal idtocobertura) {
        this.idtotipopoliza = idtotipopoliza;
        this.idtoaumentovario = idtoaumentovario;
        this.idtocobertura = idtocobertura;
    }

   
    // Property accessors

    @Column(name="IDTOTIPOPOLIZA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtotipopoliza() {
        return this.idtotipopoliza;
    }
    
    public void setIdtotipopoliza(BigDecimal idtotipopoliza) {
        this.idtotipopoliza = idtotipopoliza;
    }

    @Column(name="IDTOAUMENTOVARIO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtoaumentovario() {
        return this.idtoaumentovario;
    }
    
    public void setIdtoaumentovario(BigDecimal idtoaumentovario) {
        this.idtoaumentovario = idtoaumentovario;
    }

    @Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtocobertura() {
        return this.idtocobertura;
    }
    
    public void setIdtocobertura(BigDecimal idtocobertura) {
        this.idtocobertura = idtocobertura;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof ExclusionAumentoVarioTipoPolizaId) ) return false;
		 ExclusionAumentoVarioTipoPolizaId castOther = ( ExclusionAumentoVarioTipoPolizaId ) other; 
         
		 return ( (this.getIdtotipopoliza()==castOther.getIdtotipopoliza()) || ( this.getIdtotipopoliza()!=null && castOther.getIdtotipopoliza()!=null && this.getIdtotipopoliza().equals(castOther.getIdtotipopoliza()) ) )
 && ( (this.getIdtoaumentovario()==castOther.getIdtoaumentovario()) || ( this.getIdtoaumentovario()!=null && castOther.getIdtoaumentovario()!=null && this.getIdtoaumentovario().equals(castOther.getIdtoaumentovario()) ) )
 && ( (this.getIdtocobertura()==castOther.getIdtocobertura()) || ( this.getIdtocobertura()!=null && castOther.getIdtocobertura()!=null && this.getIdtocobertura().equals(castOther.getIdtocobertura()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdtotipopoliza() == null ? 0 : this.getIdtotipopoliza().hashCode() );
         result = 37 * result + ( getIdtoaumentovario() == null ? 0 : this.getIdtoaumentovario().hashCode() );
         result = 37 * result + ( getIdtocobertura() == null ? 0 : this.getIdtocobertura().hashCode() );
         return result;
   }   





}