package mx.com.afirme.midas.producto.configuracion.ramo;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * RamoProductoDTOId entity. @author José Luis Arellano
 */
@Embeddable
public class RamoProductoId implements java.io.Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idtoproducto;
    private BigDecimal idtcramo;

    @Column(name="IDTOPRODUCTO", nullable=false, precision=22, scale=0)
    public BigDecimal getIdtoproducto() {
        return this.idtoproducto;
    }
    
    public void setIdtoproducto(BigDecimal idtoproducto) {
        this.idtoproducto = idtoproducto;
    }

    @Column(name="IDTCRAMO", nullable=false, precision=22, scale=0)
    public BigDecimal getIdtcramo() {
        return this.idtcramo;
    }
    
    public void setIdtcramo(BigDecimal idtcramo) {
        this.idtcramo = idtcramo;
    }
}
