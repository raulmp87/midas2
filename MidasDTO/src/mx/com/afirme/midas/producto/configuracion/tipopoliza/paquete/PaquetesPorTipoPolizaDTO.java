package mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.seccion.SeccionPaquetePolizaDTO;

/**
 * 
 * @author jose luis arellano
 * @since 15/04/2011
 */
public class PaquetesPorTipoPolizaDTO implements Serializable {
	private static final long serialVersionUID = 2970825026535011528L;
	
	private PaquetePolizaDTO paquetePolizaDTO;

	private List<SeccionPaquetePolizaDTO> listaSeccionesPaquetePoliza;

	public PaquetePolizaDTO getPaquetePolizaDTO() {
		return paquetePolizaDTO;
	}

	public void setPaquetePolizaDTO(PaquetePolizaDTO paquetePolizaDTO) {
		this.paquetePolizaDTO = paquetePolizaDTO;
	}

	public List<SeccionPaquetePolizaDTO> getListaSeccionesPaquetePoliza() {
		return listaSeccionesPaquetePoliza;
	}

	public void setListaSeccionesPaquetePoliza(List<SeccionPaquetePolizaDTO> listaSeccionesPaquetePoliza) {
		this.listaSeccionesPaquetePoliza = listaSeccionesPaquetePoliza;
	}
	
}
