package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo;

import java.util.List;

import javax.ejb.Local;

@Local
public interface RecargoCoberturaCotService {

	/**
	 * Bulk deletes <code>DescuentoCoberturaCot</code>. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idToSeccion
	 * @param idToRecargoVario
	 */
	public void bulkDelete(Long idToCotizacion, Long numeroInciso, Long idToSeccion, Long idToRecargoVario);

	/**
	 * Bulk deletes <code>DescuentoCoberturaCot</code>. 
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param idToSeccion
	 * @param idToRecargoVarios
	 */
	public void bulkDelete(Long idToCotizacion, Long numeroInciso, Long idToSeccion, List<Long> idToRecargoVarios);

}
