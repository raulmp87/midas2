package mx.com.afirme.midas.producto.configuracion.ramo;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.producto.ProductoDTO;

/**
 * RamoProductoDTO entity. @author José Luis Arellano
 */
@Entity
@Table(name="TRRAMOPRODUCTO",schema="MIDAS")
public class RamoProductoDTO implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// Fields
	private RamoProductoId id;
    private ProductoDTO productoDTO;
    private RamoDTO ramoDTO;
	
    @EmbeddedId
    @AttributeOverrides( {
        @AttributeOverride(name="idtoproducto", column=@Column(name="IDTOPRODUCTO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idtcramo", column=@Column(name="IDTCRAMO", nullable=false, precision=22, scale=0) ) } )
    public RamoProductoId getId() {
		return id;
	}
	public void setId(RamoProductoId id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTOPRODUCTO", nullable=false, insertable=false, updatable=false)
	public ProductoDTO getProductoDTO() {
		return productoDTO;
	}
	public void setProductoDTO(ProductoDTO productoDTO) {
		this.productoDTO = productoDTO;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="IDTCRAMO", nullable=false, insertable=false, updatable=false)
	public RamoDTO getRamoDTO() {
		return ramoDTO;
	}
	public void setRamoDTO(RamoDTO ramoDTO) {
		this.ramoDTO = ramoDTO;
	}
}
