package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.exclusion.ExclusionAumentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.exclusion.ExclusionRecargoVarioCoberturaDTO;

/**
 * RiesgoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity (name = "RiesgoDTO")
@Table(name = "TORIESGO", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = {
		"CODIGORIESGO", "VERSIONRIESGO" }))
public class RiesgoDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToRiesgo;
	private String codigo;
	private Integer version;
	private String descripcion;
	private String nombreComercial;
	private SubRamoDTO subRamoDTO;
	/*private String claveActivoConfiguracion;
	private String claveActivoProduccion;*/
	
	private Short claveEstatus;
	private Short claveActivo;
	private Date fechaCreacion;
	private Date fechaModificacion;
    private String codigoUsuarioCreacion;
	private String codigoUsuarioModificacion;
	private List<ExclusionRecargoVarioCoberturaDTO> exclusionRecargoVarioCoberturaDTOs = new ArrayList<ExclusionRecargoVarioCoberturaDTO>();
	private List<ExclusionAumentoVarioCoberturaDTO> exclusionAumentoVarioCoberturaDTOs = new ArrayList<ExclusionAumentoVarioCoberturaDTO>();
	private List<RiesgoCoberturaDTO> riesgoCoberturaDTOList = new ArrayList<RiesgoCoberturaDTO>();
	private BigDecimal valorMinimoPrima;
	private Short claveAplicaPerdidaTotal;
	
	private BigDecimal idTcRamo;
	
	
	/** default constructor */
	public RiesgoDTO() {
		if (subRamoDTO == null) {
			subRamoDTO = new SubRamoDTO();
		}
		this.claveEstatus=new Short("0");
		this.claveActivo= new Short("0");
		this.fechaCreacion=new Date();
		this.fechaModificacion=new Date();
		this.codigoUsuarioCreacion="enDB";
		this.codigoUsuarioModificacion="enDB";
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTORIESGO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTORIESGO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTORIESGO_SEQ_GENERADOR")
	@Column(name = "IDTORIESGO", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToRiesgo() {
		return this.idToRiesgo;
	}

	public void setIdToRiesgo(BigDecimal idToRiesgo) {
		this.idToRiesgo = idToRiesgo;
	}

	@Column(name = "CODIGORIESGO", nullable = false, length = 8)
	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Column(name = "VERSIONRIESGO", nullable = false, precision = 4, scale = 0)
	public Integer getVersion() {
		return this.version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	@Column(name = "DESCRIPCIONRIESGO", nullable = false, length = 200)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "NOMBRECOMERCIALRIESGO", nullable = false, length = 100)
	public String getNombreComercial() {
		return this.nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}

	/*@Column(name = "CLAVEACTIVOCONFIGURACION", nullable = false, precision = 4, scale = 0)
	public String getClaveActivoConfiguracion() {
		return this.claveActivoConfiguracion;
	}

	public void setClaveActivoConfiguracion(String claveActivoConfiguracion) {
		this.claveActivoConfiguracion = claveActivoConfiguracion;
	}*/

	/*@Column(name = "CLAVEACTIVOPRODUCCION", nullable = false, precision = 4, scale = 0)
	public String getClaveActivoProduccion() {
		return this.claveActivoProduccion;
	}

	public void setClaveActivoProduccion(String claveActivoProduccion) {
		this.claveActivoProduccion = claveActivoProduccion;
	}*/
	
	@Column(name = "CLAVEESTATUS", nullable = false, precision = 4, scale = 0)
	public Short getClaveEstatus() {
		return claveEstatus;
	}

	public void setClaveEstatus(Short claveEstatus) {
		this.claveEstatus = claveEstatus;
	}
	
	@Column(name = "CLAVEACTIVO", nullable = false, precision = 4, scale = 0)
	public Short getClaveActivo() {
		return claveActivo;
	}

	public void setClaveActivo(Short claveActivo) {
		this.claveActivo = claveActivo;
	}	
	
	@Temporal(TemporalType.DATE)
    @Column(name="FECHACREACION", length=7, nullable=false)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Temporal(TemporalType.DATE)
    @Column(name="FECHAMODIFICACION", length=7, nullable=false)
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Column(name="CODIGOUSUARIOCREACION", nullable=false, length=8)
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	
	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
	
	@Column(name="CODIGOUSUARIOMODIFICACION", length=8, nullable=false)
	public String getCodigoUsuarioModificacion() {
		return codigoUsuarioModificacion;
	}	
	
	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="riesgoDTO")
	public List<ExclusionRecargoVarioCoberturaDTO> getExclusionRecargoVarioCoberturaDTOs() {
		return exclusionRecargoVarioCoberturaDTOs;
	}

	public void setExclusionRecargoVarioCoberturaDTOs(
			List<ExclusionRecargoVarioCoberturaDTO> exclusionRecargoVarioCoberturaDTOs) {
		this.exclusionRecargoVarioCoberturaDTOs = exclusionRecargoVarioCoberturaDTOs;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCSUBRAMO", nullable = false)
	public SubRamoDTO getSubRamoDTO() {
		return subRamoDTO;
	}
	public void setSubRamoDTO(SubRamoDTO subRamoDTO) {
		this.subRamoDTO = subRamoDTO;
	}
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="riesgoDTO")
	public List<ExclusionAumentoVarioCoberturaDTO> getExclusionAumentoVarioCoberturaDTOs() {
		return exclusionAumentoVarioCoberturaDTOs;
	}

	public void setExclusionAumentoVarioCoberturaDTOs(
			List<ExclusionAumentoVarioCoberturaDTO> exclusionAumentoVarioCoberturaDTOs) {
		this.exclusionAumentoVarioCoberturaDTOs = exclusionAumentoVarioCoberturaDTOs;
	}

	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="riesgoDTO")
	public List<RiesgoCoberturaDTO> getRiesgoCoberturaDTOList() {
		return riesgoCoberturaDTOList;
	}

	public void setRiesgoCoberturaDTOList(
			List<RiesgoCoberturaDTO> riesgoCoberturaDTOList) {
		this.riesgoCoberturaDTOList = riesgoCoberturaDTOList;
	}

	@Column(name = "VALORMINIMOPRIMA", nullable = false, precision = 16, scale = 2)
	public BigDecimal getValorMinimoPrima() {
		return valorMinimoPrima;
	}

	public void setValorMinimoPrima(BigDecimal valorMinimoPrima) {
		this.valorMinimoPrima = valorMinimoPrima;
	}

	@Column(name = "CLAVEAPLICAPERDIDATOTAL", nullable = false, precision = 4)
	public Short getClaveAplicaPerdidaTotal() {
		return claveAplicaPerdidaTotal;
	}

	public void setClaveAplicaPerdidaTotal(Short claveAplicaPerdidaTotal) {
		this.claveAplicaPerdidaTotal = claveAplicaPerdidaTotal;
	}
	
	@Transient
	public BigDecimal getIdTcRamo() {
		return idTcRamo;
	}

	public void setIdTcRamo(BigDecimal idTcRamo) {
		this.idTcRamo = idTcRamo;
	}
	
}