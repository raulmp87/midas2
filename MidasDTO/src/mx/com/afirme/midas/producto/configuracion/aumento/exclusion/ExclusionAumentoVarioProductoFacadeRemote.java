package mx.com.afirme.midas.producto.configuracion.aumento.exclusion;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for ExclusionAumentoVarioProductoFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ExclusionAumentoVarioProductoFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ExclusionAumentoVarioProductoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ExclusionAumentoVarioProductoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ExclusionAumentoVarioProductoDTO entity);
    /**
	 Delete a persistent ExclusionAumentoVarioProductoDTO entity.
	  @param entity ExclusionAumentoVarioProductoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ExclusionAumentoVarioProductoDTO entity);
   /**
	 Persist a previously saved ExclusionAumentoVarioProductoDTO entity and return it or a copy of it to the sender. 
	 A copy of the ExclusionAumentoVarioProductoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ExclusionAumentoVarioProductoDTO entity to update
	 @return ExclusionAumentoVarioProductoDTO the persisted ExclusionAumentoVarioProductoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ExclusionAumentoVarioProductoDTO update(ExclusionAumentoVarioProductoDTO entity);
	public ExclusionAumentoVarioProductoDTO findById( ExclusionAumentoVarioProductoId id);
	 /**
	 * Find all ExclusionAumentoVarioProductoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ExclusionAumentoVarioProductoDTO property to query
	  @param value the property value to match
	  	  @return List<ExclusionAumentoVarioProductoDTO> found by query
	 */
	public List<ExclusionAumentoVarioProductoDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ExclusionAumentoVarioProductoDTO entities.
	  	  @return List<ExclusionAumentoVarioProductoDTO> all ExclusionAumentoVarioProductoDTO entities
	 */
	public List<ExclusionAumentoVarioProductoDTO> findAll(
		);
	
	/**
	 * Encuentra los registros de ExclusionAumentoVarioProductoDTO relacionados con el ProductoDTO cuyo ID se recibe y que adem�s est�n 
	 * relacionados s�lo con Tipos de p�liza que no hayan sido borradas l�gicamente.
	  @param BigDecimal idToProducto. El ID del producto
	  @return List<ExclusionAumentoVarioProductoDTO> encontrados por el query formado.
	 */
    public List<ExclusionAumentoVarioProductoDTO> getVigentesPorIdProducto(BigDecimal idToProducto);
}