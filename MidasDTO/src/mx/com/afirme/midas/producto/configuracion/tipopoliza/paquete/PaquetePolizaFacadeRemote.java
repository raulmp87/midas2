package mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete;
// default package

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.seccion.cobertura.CoberturaSeccionPaqueteDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;

/**
 * Remote interface for PaquetePolizaDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface PaquetePolizaFacadeRemote {
	
	/**
	 * Genera el registro PaquetePolizaDTO y la estructura por default de las coberturas del paquete.
	 * La estructura por default incluye las coberturas obligatorias y obligatorias parciales b�sicas, amparadas y subl�mite
	 * incluyendo sus dependencias.
	 * @param entity
	 * @return
	 */
    public PaquetePolizaDTO save(PaquetePolizaDTO entity);
//    /**
//	 Delete a persistent PaquetePolizaDTO entity.
//	  @param entity PaquetePolizaDTO entity to delete
//	 @throws RuntimeException when the operation fails
//	 */
//    public void delete(PaquetePolizaDTO entity);
   /**
	 Persist a previously saved PaquetePolizaDTO entity and return it or a copy of it to the sender. 
	 A copy of the PaquetePolizaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity PaquetePolizaDTO entity to update
	 @return PaquetePolizaDTO the persisted PaquetePolizaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public PaquetePolizaDTO update(PaquetePolizaDTO entity);
	public PaquetePolizaDTO findById( BigDecimal id);
	 /**
	 * Find all PaquetePolizaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the PaquetePolizaDTO property to query
	  @param value the property value to match
	  	  @return List<PaquetePolizaDTO> found by query
	 */
	public List<PaquetePolizaDTO> findByProperty(String propertyName, Object value,boolean poblarCoberturas);
	
	/**
     * Consulta la cobertura b&aacute;sica de la cual depende la cobertura amparada o subl&iacute;mite recibida.
     * La cobertura b&aacute;sica se busca primero en la misma secci&oacte;n de la cobertura recibida y despu&eacute;s
     * en la cobertura de "CONTENIDOS".
     * @param coberturaDependiente
     * @return
     */
	public CoberturaSeccionDTO consultarCoberturaBasica(CoberturaSeccionDTO coberturaDependiente);
	
	/**
	 * Consulta las entidades CoberturaSeccionDTO disponibles para registrar al paquete cuyo ID se recibe.
	 * @param idToPaquetePoliza
	 * @return
	 */
	public List<CoberturaSeccionDTO> obtenerCoberturasDisponibles(BigDecimal idToPaquetePoliza);
	
	/**
	 * Consulta las entidades CoberturaSeccionPaqueteDTO registradas a un paquete cuyo id se recibe.
	 * @param idToPaquetePoliza
	 * @return
	 */
	public List<CoberturaSeccionPaqueteDTO> obtenerCoberturasRegistradas(BigDecimal idToPaquetePoliza);

	public CoberturaSeccionPaqueteDTO consultarCoberturaSeccionPaquetePorId( BigDecimal id);
	
	/**
     * Ejecuta la modificacion de una cobertura en el paquete. Las modificaciones posibles son: 
     * contratar o descontratar una cobertura y cambiar la suma asegurada de una cobertrua. La 
     * operacion se deduce en base a los parametros recibidos.
     * @param idToPaquetePoliza. Paquete sobre el cual se realizar�n las modificaciones.
     * @param idToCoberturaSeccionPaquete. cobertura a modificar, debe pertenecer al paquete cuyo ID se recibe.
     * 			Puede ser nulo, solo cuando la operacion es "agregar una cobertura".
     * @param idToSeccion. Seccion que se agrega al paquete. Puede ser nula, cuando ya existe la cobertura-seccion,
     * 			especificando un idToCoberturaSeccionPaquete.
     * @param idToCobertura. Cobertura que se agrega al paquete. Puede ser nula, cuando ya existe la cobertura-seccion,
     * 			especificando un idToCoberturaSeccionPaquete.
     * @param montoSumaAsegurada. Suma asegurada a registrar en caso de que la operacion sea "cambio de suma asegurada".
     * @param claveContrato. Usado para espeficar si la cobertura se agrega o se elimina del paquete, 
     * 			valores permitidos: 1 (CONTRATADO) y 0 (DESCONTRATADO).
     */
	public ActualizacionCoberturaPaqueteDTO registrarCoberturaPaquete(BigDecimal idToPaquetePoliza,BigDecimal idToCoberturaSeccionPaquete,
			BigDecimal idToSeccion,BigDecimal idToCobertura,BigDecimal montoSumaAsegurada,Short claveContrato);
	
	public List<PaquetePolizaDTO> listarPorTipoPoliza(BigDecimal idToTipoPoliza,boolean poblarCoberturas,Short estatus);
	
	/**
     * Valida las sumas aseguradas de las coberturas b&aacute;sicas registradas en el paquete.
     * Si las coberturas son v�lidas, se actualiza el estatus del paquete a "liberado" (1),
     * de lo contrario env�a mansaje de error en el objeto ActualizacionCoberturaPaqueteDTO
     * @param idToPaquete. Identificador del paquete a procesar.
     * @return ActualizacionCoberturaPaqueteDTO objeto que contiene la informaci�n del resultado de la operaci&oacute;n
     */
	public ActualizacionCoberturaPaqueteDTO liberarPaquete(BigDecimal idToPaquete);
	
	/**
     * Elimina el paquete cuyo identificador se recibe.
     * Son eliminadas en cascada las coberturas del paquete (CoberturaSeccionPaqueteDTO).
     * @param idToPaquete. Identificador del paquete a procesar.
     * @return ActualizacionCoberturaPaqueteDTO objeto que contiene la informaci�n del resultado de la operaci&oacute;n
     */
    public ActualizacionCoberturaPaqueteDTO eliminarPaquete(BigDecimal idToPaquete);
    
    public void aplicarPaqueteCotizacion(BigDecimal idToPaquete,BigDecimal idToCotizacion,BigDecimal numeroInciso);
    
    /**
     * Establece el paquete por dafault para un tipo de p�liza.
     * El m�todo valida el estatus del paquete, si no es liberado lanza un RunTimeException.
     * @param idToPaquetePoliza
     */
    public PaquetePolizaDTO establecerPaqueteDefault(BigDecimal idToPaquetePoliza);
}