package mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for RamoTipoPolizaDTOFacade.
 * @author Jos� Luis Arellano
 */

public interface RamoTipoPolizaFacadeRemote {
	/**
	 Perform an initial save of a previously unsaved RamoTipoPolizaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity RamoTipoPolizaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
 public void save(RamoTipoPolizaDTO entity);
 /**
	 Delete a persistent RamoTipoPolizaDTO entity.
	  @param entity RamoTipoPolizaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
 public void delete(RamoTipoPolizaDTO entity);
/**
	 Persist a previously saved RamoTipoPolizaDTO entity and return it or a copy of it to the sender. 
	 A copy of the RamoTipoPolizaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity RamoTipoPolizaDTO entity to update
	 @return RamoTipoPolizaDTO the persisted RamoTipoPolizaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public RamoTipoPolizaDTO update(RamoTipoPolizaDTO entity);
	
	public RamoTipoPolizaDTO findById( RamoTipoPolizaId id);
	 /**
	 * Find all RamoTipoPolizaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the RamoTipoPolizaDTO property to query
	  @param value the property value to match
	  	  @return List<RamoTipoPolizaDTO> found by query
	 */
	public List<RamoTipoPolizaDTO> findByProperty(String propertyName, Object value);
	
	/**
	 * Find all RamoTipoPolizaDTO entities.
	  	  @return List<RamoTipoPolizaDTO> all RamoTipoPolizaDTO entities
	 */
	public List<RamoTipoPolizaDTO> findAll();
	
	/**
	 * Lista todos los ramos que no est�n asociados a un TipoPoliza determinado 
	 * por el id que recibe el m�todo
	 * @param BigDecimal idToTipoPoliza el id del TipoPoliza.
	 * @return List<RamoTipoPolizaDTO> registros RamoTipoPolizaDTO que no est�n asociados al TipoPoliza.
	 */
	public List<RamoTipoPolizaDTO> obtenerRamosSinAsociar(BigDecimal idToTipoPoliza);
}
