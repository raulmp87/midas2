package mx.com.afirme.midas.producto.configuracion.tipopoliza.descuento.exclusion;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.descuento.DescuentoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;

/**
 * ExclusionDescuentoVarioTipoPolizaDTO entity. @author José Luis Arellano
 */
@Entity
@Table(name="TREXCDESCUENTOVARTIPOPOLIZA",schema="MIDAS")
public class ExclusionDescuentoVarioTipoPolizaDTO implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private ExclusionDescuentoVarioTipoPolizaId id;
    private DescuentoDTO descuentoDTO;
    private TipoPolizaDTO  tipoPolizaDTO;
    private CoberturaDTO coberturaDTO;

    @EmbeddedId
    @AttributeOverrides( {
        @AttributeOverride(name="idtotipopoliza", column=@Column(name="IDTOTIPOPOLIZA", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idtodescuentovario", column=@Column(name="IDTODESCUENTOVARIO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idtocobertura", column=@Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0) ) } )
    public ExclusionDescuentoVarioTipoPolizaId getId() {
        return this.id;
    }
    
    public void setId(ExclusionDescuentoVarioTipoPolizaId id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="IDTODESCUENTOVARIO", nullable=false, insertable=false, updatable=false)
    public DescuentoDTO getDescuentoDTO() {
		return descuentoDTO;
	}


	public void setDescuentoDTO(DescuentoDTO descuentoDTO) {
		this.descuentoDTO = descuentoDTO;
	}

	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTOTIPOPOLIZA", nullable=false, insertable=false, updatable=false)
    public TipoPolizaDTO getTipoPolizaDTO() {
		return tipoPolizaDTO;
	}

	public void setTipoPolizaDTO(TipoPolizaDTO tipoPolizaDTO) {
		this.tipoPolizaDTO = tipoPolizaDTO;
	}	
  
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTOCOBERTURA", nullable=false, insertable=false, updatable=false)
	public CoberturaDTO getCoberturaDTO() {
		return coberturaDTO;
	}

	public void setCoberturaDTO(CoberturaDTO coberturaDTO) {
		this.coberturaDTO = coberturaDTO;
	}
	
	public boolean equals(Object o) {
		if (o instanceof ExclusionDescuentoVarioTipoPolizaDTO) {
			ExclusionDescuentoVarioTipoPolizaDTO temp = (ExclusionDescuentoVarioTipoPolizaDTO) o;
			if (temp.getId().getIdtotipopoliza().intValue() == this.getId().getIdtotipopoliza().intValue()
					&& temp.getId().getIdtodescuentovario().intValue() == this.getId().getIdtodescuentovario().intValue()
					&& temp.getId().getIdtocobertura().intValue() == this.getId().getIdtocobertura().intValue()) {
				return true;
			}
		}
		return false;
	}

	public int hashCode() {
		int hash = 1;
	    hash = hash * 31 + this.getId().getIdtotipopoliza().hashCode();
	    hash = hash * 31 + this.getId().getIdtodescuentovario().hashCode();
	    hash = hash * 31 + this.getId().getIdtotipopoliza().hashCode();
	    return hash;
	}
}