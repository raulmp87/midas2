package mx.com.afirme.midas.producto.configuracion.aumento;
// default package

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for AumentoVarioProductoFacade.
 * @author MyEclipse Persistence Tools
 */


public interface AumentoVarioProductoFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved AumentoVarioProductoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity AumentoVarioProductoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(AumentoVarioProductoDTO entity);
    /**
	 Delete a persistent AumentoVarioProductoDTO entity.
	  @param entity AumentoVarioProductoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(AumentoVarioProductoDTO entity);
   /**
	 Persist a previously saved AumentoVarioProductoDTO entity and return it or a copy of it to the sender. 
	 A copy of the AumentoVarioProductoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity AumentoVarioProductoDTO entity to update
	 @return AumentoVarioProductoDTO the persisted AumentoVarioProductoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public AumentoVarioProductoDTO update(AumentoVarioProductoDTO entity);
	public AumentoVarioProductoDTO findById( AumentoVarioProductoId id);
	 /**
	 * Find all AumentoVarioProductoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the AumentoVarioProductoDTO property to query
	  @param value the property value to match
	  	  @return List<AumentoVarioProductoDTO> found by query
	 */
	public List<AumentoVarioProductoDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all AumentoVarioProductoDTO entities.
	  	  @return List<AumentoVarioProductoDTO> all AumentoVarioProductoDTO entities
	 */
	public List<AumentoVarioProductoDTO> findAll(
		);	
}