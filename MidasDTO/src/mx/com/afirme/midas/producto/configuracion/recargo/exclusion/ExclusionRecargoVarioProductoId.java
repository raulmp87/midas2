package mx.com.afirme.midas.producto.configuracion.recargo.exclusion;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * ExcRecargoVarioProductoId entity. @author José Luis Arellano
 */
@Embeddable
public class ExclusionRecargoVarioProductoId implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idtoproducto;
    private BigDecimal idtorecargovario;
    private BigDecimal idtotipopoliza;
    
    // Property accessors

    @Column(name="IDTOPRODUCTO", nullable=false, precision=22, scale=0)
    public BigDecimal getIdtoproducto() {
        return this.idtoproducto;
    }
    
    public void setIdtoproducto(BigDecimal idtoproducto) {
        this.idtoproducto = idtoproducto;
    }

    @Column(name="IDTORECARGOVARIO", nullable=false, precision=22, scale=0)
    public BigDecimal getIdtorecargovario() {
        return this.idtorecargovario;
    }
    
    public void setIdtorecargovario(BigDecimal idtorecargovario) {
        this.idtorecargovario = idtorecargovario;
    }

    @Column(name="IDTOTIPOPOLIZA", nullable=false, precision=22, scale=0)
    public BigDecimal getIdtotipopoliza() {
        return this.idtotipopoliza;
    }
    
    public void setIdtotipopoliza(BigDecimal idtotipopoliza) {
        this.idtotipopoliza = idtotipopoliza;
    }
}
