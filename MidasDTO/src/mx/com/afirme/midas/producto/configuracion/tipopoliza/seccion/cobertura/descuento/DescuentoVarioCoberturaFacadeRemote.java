package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento;
// default package

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for DescuentoVarioCoberturaFacade.
 * @author MyEclipse Persistence Tools
 */

public interface DescuentoVarioCoberturaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved DescuentoVarioCoberturaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DescuentoVarioCoberturaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DescuentoVarioCoberturaDTO entity);
    /**
	 Delete a persistent DescuentoVarioCoberturaDTO entity.
	  @param entity DescuentoVarioCoberturaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DescuentoVarioCoberturaDTO entity);
   /**
	 Persist a previously saved DescuentoVarioCoberturaDTO entity and return it or a copy of it to the sender. 
	 A copy of the DescuentoVarioCoberturaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DescuentoVarioCoberturaDTO entity to update
	 @return DescuentoVarioCoberturaDTO the persisted DescuentoVarioCoberturaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public DescuentoVarioCoberturaDTO update(DescuentoVarioCoberturaDTO entity);
	public DescuentoVarioCoberturaDTO findById( DescuentoVarioCoberturaId id);
	 /**
	 * Find all DescuentoVarioCoberturaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DescuentoVarioCoberturaDTO property to query
	  @param value the property value to match
	  	  @return List<DescuentoVarioCoberturaDTO> found by query
	 */
	public List<DescuentoVarioCoberturaDTO> findByProperty(String propertyName, Object value);
	/**
	 * Find all DescuentoVarioCoberturaDTO entities.
	  	  @return List<DescuentoVarioCoberturaDTO> all DescuentoVarioCoberturaDTO entities
	 */
	public List<DescuentoVarioCoberturaDTO> findAll();	
}