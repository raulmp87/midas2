package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class RecargoCoberturaCotId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 430768092033263371L;

	private Long idToCotizacion;
	private Long numeroInciso;
	private Long idToSeccion;
	private Long idToCobertura;
	private Long idToRecargoVario;
	
	@Column(name = "IDTOCOTIZACION", nullable = false)
	public Long getIdToCotizacion() {
		return idToCotizacion;
	}
	public void setIdToCotizacion(Long idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	
	@Column(name = "NUMEROINCISO", nullable = false)
	public Long getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(Long numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	
	@Column(name = "IDTOSECCION", nullable = false)
	public Long getIdToSeccion() {
		return idToSeccion;
	}
	public void setIdToSeccion(Long idToSeccion) {
		this.idToSeccion = idToSeccion;
	}
	
	@Column(name = "IDTOCOBERTURA", nullable = false)
	public Long getIdToCobertura() {
		return idToCobertura;
	}
	public void setIdToCobertura(Long idToCobertura) {
		this.idToCobertura = idToCobertura;
	}
	
	@Column(name = "IDTORECARGOVARIO", nullable = false)
	public Long getIdToRecargoVario() {
		return idToRecargoVario;
	}
	public void setIdToRecargoVario(Long idToRecargoVario) {
		this.idToRecargoVario = idToRecargoVario;
	}
	
	
   public boolean equals(Object other) {
	   if ( (this == other ) ) return true;
	   if ( (other == null ) ) return false;
	   if ( !(other instanceof RecargoCoberturaCotId) ) return false;
	   RecargoCoberturaCotId castOther = ( RecargoCoberturaCotId ) other; 
     
	   return ( (this.getIdToCobertura()==castOther.getIdToCobertura()) || ( this.getIdToCobertura()!=null && castOther.getIdToCobertura()!=null && this.getIdToCobertura().equals(castOther.getIdToCobertura()) ) )
	   && ((this.getNumeroInciso()==castOther.getNumeroInciso()) || ( this.getNumeroInciso()!=null && castOther.getNumeroInciso()!=null && this.getNumeroInciso().equals(castOther.getNumeroInciso()))) 
	   && ((this.getIdToSeccion()==castOther.getIdToSeccion()) || ( this.getIdToSeccion()!=null && castOther.getIdToSeccion()!=null && this.getIdToSeccion().equals(castOther.getIdToSeccion()))) 
	   && ((this.getIdToCotizacion()==castOther.getIdToCotizacion()) || ( this.getIdToCotizacion()!=null && castOther.getIdToCotizacion()!=null && this.getIdToCotizacion().equals(castOther.getIdToCotizacion()))) 
	   && ((this.getIdToRecargoVario()==castOther.getIdToRecargoVario()) || ( this.getIdToRecargoVario()!=null && castOther.getIdToRecargoVario()!=null && this.getIdToRecargoVario().equals(castOther.getIdToRecargoVario()) ) );
   }
	   

}
