package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.sumaaseguradaadicional;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name = "TOSUMAADICIONALCOBERTURA", schema = "MIDAS")
public class SumaAseguradaAdicionalCoberturaDTO implements java.io.Serializable, Entidad{
	private static final long serialVersionUID = 1L;
	private BigDecimal id;
	private CoberturaDTO coberturaDTO;
	private Double valor;

	// Constructors

	/** default constructor */
	public SumaAseguradaAdicionalCoberturaDTO() {
	}

	/** full constructor */
	public SumaAseguradaAdicionalCoberturaDTO(BigDecimal id,
			CoberturaDTO coberturaDTO, Double valor) {
		this.id = id;
		this.coberturaDTO = coberturaDTO;
		this.valor = valor;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "TOSUMAADICIONALCOB_ID_GENERATOR", sequenceName = "IDTOSUMAADICIONALCOB_SEQ", allocationSize=1, schema="MIDAS")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TTOSUMAADICIONALCOB_ID_GENERATOR")
	@Column(name = "ID")
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTOCOBERTURA", nullable = false, insertable = false, updatable = false)
	public CoberturaDTO getCoberturaDTO() {
		return this.coberturaDTO;
	}

	public void setCoberturaDTO(CoberturaDTO coberturaDTO) {
		this.coberturaDTO = coberturaDTO;
	}

	@Column(name = "VALOR", nullable = false, precision = 16)
	public Double getValor() {
		return this.valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		return this.valor.toString();
	}

	@Override
	public Double getBusinessKey() {
		// TODO Apéndice de método generado automáticamente
		return this.valor;
	}
}
