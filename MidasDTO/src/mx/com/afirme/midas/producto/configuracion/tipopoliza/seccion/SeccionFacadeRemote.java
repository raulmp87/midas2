package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;

/**
 * Remote interface for SeccionDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface SeccionFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved SeccionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity SeccionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(SeccionDTO entity);
    /**
	 Delete a persistent SeccionDTO entity.
	  @param entity SeccionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(SeccionDTO entity);
   /**
	 Persist a previously saved SeccionDTO entity and return it or a copy of it to the sender. 
	 A copy of the SeccionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity SeccionDTO entity to update
	 @return SeccionDTO the persisted SeccionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public SeccionDTO update(SeccionDTO entity);
	public SeccionDTO findById( BigDecimal id);
	 /**
	 * Find all SeccionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the SeccionDTO property to query
	  @param value the property value to match
	  	  @return List<SeccionDTO> found by query
	 */
	public List<SeccionDTO> findByProperty(String propertyName, Object value);
	
	/**
	 * Find all SeccionDTO entities.
	  	  @return List<SeccionDTO> all SeccionDTO entities
	 */
	public List<SeccionDTO> findAll();
	
	/**
	 * Borra l�gicamente un registro de Seccion, estableciendo en 0 los campos 
	 * CLAVEACTIVO y CLAVEESTATUS
	 * 
	 * @param entity
	 *            SeccionDTO entity el registro Seccion a borrar
	 * @return RiesgoDTO the persisted RiesgoDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             si la operacion falla
	 */
	public SeccionDTO borradoLogico(SeccionDTO entity);
	
	/**
	 * Busca los registros de Seccion que no hayan sido borrados logicamente
	 * 
	 * @return List<SeccionDTO> registros de Seccion que no han sido borrados logicamente
	 */
	public List<SeccionDTO> listarVigentes();
	
	/**
	 * Busca los registros de Seccion que no hayan sido borrados logicamente
	 * pertenecientes al TipoPoliza cuyo ID se recibe.
	 * @param BigDecimal idToProducto el id del producto al que pertenecen 
	 *		los registros TipoPoliza
	 * @return List<SeccionDTO> registros de Seccion que no han sido borrados logicamente
	 */
	public List<SeccionDTO> listarVigentes(BigDecimal idToTipoPoliza);
	
	public List<SeccionDTO> listarPorIdTipoPoliza(BigDecimal idToTipoPoliza, Boolean verInactivos, Boolean soloActivos);
	
	/**
	 * Busca los registros de Seccion que no sean requeridos para la secci�n cuyo id se recibe.
	 * @param BigDecimal idToSeccion el id de la seccion
	 * @param BigDecimal idTiTipoPoliza el id del TipoPoliza al que pertenece la seccion
	 * @return List<SeccionDTO> registros de Seccion que no est�n relacionados con la seccion.
	 */
	public List<SeccionDTO> listarSeccionesNoRequeridas(BigDecimal idToSeccion,BigDecimal idToTipoPoliza);
	
	/**
	 * Busca los registros de Seccion que sean requeridos para la secci�n cuyo id se recibe.
	 * @param BigDecimal idToSeccion el id de la seccion
	 * @return List<SeccionDTO> registros de Seccion que est�n relacionados con la seccion.
	 */
	public List<SeccionDTO> listarSeccionesRequeridas(BigDecimal idToSeccion);
	

	/**
	 * Busca los registros de Seccion vigentes que dependan de un tipo poliza, que a su vez dependa de un producto cuya claveNegocio sea A.
	 * @return List<SeccionDTO> registros de Seccion que est�n relacionados con la seccion.
	 */
	public List<SeccionDTO> listarVigentesAutos();
	public List<SeccionDTO> listarVigentesAutosUsables();
	/**
	 * Busca los registros de Seccion vigentes que dependan de un tipo poliza, que a su vez dependa de un producto de acuerdo a la claveNegocio.
	 * @return List<SeccionDTO> registros de Seccion que est�n relacionados con la seccion.
	 */
	public List<SeccionDTO> listarVigentesPorNegocio(String claveNegocio);
	
	public List<ValorCatalogoAgentes> listarCedulasAsociadasPorSeccion(BigDecimal idToSeccion);
	
	public List<ValorCatalogoAgentes> listarCedulasNoAsociadasPorSeccion(BigDecimal idToSeccion);
	
	public Integer guardarCedulasAsociadasPorSeccion(BigDecimal idToSeccion, String idsCedulas);
	
	public BigDecimal countCedulasAsociadasPorSeccion(Long idTipoCedula, BigDecimal idSeccion);
	
	public BigDecimal countCedulasAsociadasPorSeccionesEnTipoPoliza(Long idTipoCedula, BigDecimal idTipoPoliza);
	
	public  Map<String, String> generarNuevaVersion(BigDecimal idToSeccion) throws Exception;
	
}