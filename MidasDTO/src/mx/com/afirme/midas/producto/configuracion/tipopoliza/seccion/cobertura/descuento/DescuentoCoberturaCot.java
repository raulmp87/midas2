package mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.catalogos.descuento.DescuentoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="toDescuentoCoberturaCot", schema="midas" )
public class DescuentoCoberturaCot implements Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3064768958668552139L;

	private DescuentoCoberturaCotId id;
	private DescuentoDTO descuentoDTO;
	private CoberturaDTO coberturaDTO;
	private Long claveAutorizacion;
	private String codigoUsuarioAutorizacion;
	private BigDecimal valorDescuento;
	private Short claveObligatoriedad;
	private Short claveContrato;
	private Short claveComercialTecnico;
	private Short claveNivel;
	private Date fechaSolicitudAutorizacion;
	private Date fechaAutorizacion;

	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToCotizacion", column = @Column(name = "IDTOCOTIZACION", nullable = false)),
			@AttributeOverride(name = "numeroInciso", column = @Column(name = "NUMEROINCISO", nullable = false)),
			@AttributeOverride(name = "idToSeccion", column = @Column(name = "IDTOSECCION", nullable = false)),
			@AttributeOverride(name = "idToCobertura", column = @Column(name = "IDTOCOBERTURA", nullable = false)),
			@AttributeOverride(name = "idToDescuentoVario", column = @Column(name = "IDTODESCUENTOVARIO", nullable = false)) })
	public DescuentoCoberturaCotId getId() {
		return id;
	}

	public void setId(DescuentoCoberturaCotId id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTODESCUENTOVARIO", nullable=false, insertable=false, updatable=false)
	public DescuentoDTO getDescuentoDTO() {
		return descuentoDTO;
	}

	public void setDescuentoDTO(DescuentoDTO descuentoDTO) {
		this.descuentoDTO = descuentoDTO;
	}

	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTOCOBERTURA", nullable=false, insertable=false, updatable=false)
	public CoberturaDTO getCoberturaDTO() {
		return coberturaDTO;
	}

	public void setCoberturaDTO(CoberturaDTO coberturaDTO) {
		this.coberturaDTO = coberturaDTO;
	}

	@Column(name="CLAVEAUTORIZACION")
	public Long getClaveAutorizacion() {
		return claveAutorizacion;
	}

	public void setClaveAutorizacion(Long claveAutorizacion) {
		this.claveAutorizacion = claveAutorizacion;
	}

	@Column(name="CODIGOUSUARIOAUTORIZACION")
	public String getCodigoUsuarioAutorizacion() {
		return codigoUsuarioAutorizacion;
	}

	public void setCodigoUsuarioAutorizacion(String codigoUsuarioAutorizacion) {
		this.codigoUsuarioAutorizacion = codigoUsuarioAutorizacion;
	}

	@Column(name="VALORDESCUENTO")
	public BigDecimal getValorDescuento() {
		return valorDescuento;
	}

	public void setValorDescuento(BigDecimal valorDescuento) {
		this.valorDescuento = valorDescuento;
	}

	@Column(name="CLAVEOBLIGATORIEDAD")
	public Short getClaveObligatoriedad() {
		return claveObligatoriedad;
	}

	public void setClaveObligatoriedad(Short claveObligatoriedad) {
		this.claveObligatoriedad = claveObligatoriedad;
	}

	@Column(name="CLAVECONTRATO")
	public Short getClaveContrato() {
		return claveContrato;
	}

	public void setClaveContrato(Short claveContrato) {
		this.claveContrato = claveContrato;
	}

	@Column(name="CLAVECOMERCIALTECNICO")
	public Short getClaveComercialTecnico() {
		return claveComercialTecnico;
	}

	public void setClaveComercialTecnico(Short claveComercialTecnico) {
		this.claveComercialTecnico = claveComercialTecnico;
	}

	@Column(name="CLAVENIVEL")
	public Short getClaveNivel() {
		return claveNivel;
	}

	public void setClaveNivel(Short claveNivel) {
		this.claveNivel = claveNivel;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHASOLICITUDAUTORIZACION")
	public Date getFechaSolicitudAutorizacion() {
		return fechaSolicitudAutorizacion;
	}

	public void setFechaSolicitudAutorizacion(Date fechaSolicitudAutorizacion) {
		this.fechaSolicitudAutorizacion = fechaSolicitudAutorizacion;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHAAUTORIZACION")
	public Date getFechaAutorizacion() {
		return fechaAutorizacion;
	}

	public void setFechaAutorizacion(Date fechaAutorizacion) {
		this.fechaAutorizacion = fechaAutorizacion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public DescuentoCoberturaCotId getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}


}
