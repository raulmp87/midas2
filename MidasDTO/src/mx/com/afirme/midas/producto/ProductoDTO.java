package mx.com.afirme.midas.producto;

// default package

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.producto.configuracion.aumento.AumentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.aumento.exclusion.ExclusionAumentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.descuento.DescuentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.moneda.MonedaProductoDTO;
import mx.com.afirme.midas.producto.configuracion.ramo.RamoProductoDTO;
import mx.com.afirme.midas.producto.configuracion.recargo.RecargoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.recargo.exclusion.ExclusionRecargoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dto.fuerzaventa.GenericaAgentesView;

/**
 * ProductoDTO entity. @author Jorge Cano
 */
@Entity(name = "ProductoDTO")
@Table(name = "TOPRODUCTO", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = {
		"CODIGOPRODUCTO", "VERSIONPRODUCTO" }))
public class ProductoDTO extends CacheableDTO implements Serializable, Entidad {

	
	public static final String AUTOMOVILES_FLOTILLA = "32";
	public static final BigDecimal AUTOMOVILES_INDIVIDUALES = new BigDecimal (191);
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1000352971598431697L;
	private BigDecimal idToProducto;
	private String codigo;
	private Integer version;
	private String descripcion;
	//private String descripcionCorta;
	private String nombreComercial;
	private String claveUnidadVigencia;
	private BigDecimal valorMinimoUnidadVigencia;
	private BigDecimal valorMaximoUnidadVigencia;
	private int valorDefaultUnidadVigencia;
	private String claveRenovable;
	private String clavePagoInmediato;
	private BigDecimal diasRetroactividad;
	private BigDecimal diasDiferimiento;
	private BigDecimal diasGracia;
	//private String claveActivoConfiguracion;
	//private String claveActivoProduccion;
	//private String registroCNSF;
	//private String tipoCalculoEmision;
	//private String tipoCalculoCancelacion;
	private List<TipoPolizaDTO> tiposPoliza = new ArrayList<TipoPolizaDTO>();
	private List<ExclusionRecargoVarioProductoDTO> exclusionRecargoVarioProductoDTOs = new ArrayList<ExclusionRecargoVarioProductoDTO>();
	private List<RecargoVarioProductoDTO> recargoVarioProductoDTO = new ArrayList<RecargoVarioProductoDTO>();
	private List<DescuentoVarioProductoDTO> descuentos = new ArrayList<DescuentoVarioProductoDTO>();
	private List<RecargoVarioProductoDTO> recargos = new ArrayList<RecargoVarioProductoDTO>();
	private List<AumentoVarioProductoDTO> aumentos = new ArrayList<AumentoVarioProductoDTO>();
	private List<ExclusionAumentoVarioProductoDTO> exclusionAumentoVarioProductoDTOs =  new ArrayList<ExclusionAumentoVarioProductoDTO>();
	private List<AumentoVarioProductoDTO> aumentoVarioProductoDTOs = new ArrayList<AumentoVarioProductoDTO> ();
	
	private List<RamoProductoDTO> ramoProductoDTOList = new ArrayList<RamoProductoDTO>();
	private List<ExclusionRecargoVarioProductoDTO> excRecargoPorProductoDTO = new ArrayList<ExclusionRecargoVarioProductoDTO>();
	private List<MonedaProductoDTO> monedas = new ArrayList<MonedaProductoDTO>();
	
	//private Integer idTipoCalculo;
	private String descripcionRegistroCNFS;
	private Integer idControlArchivoRegistroCNSF;
	private Integer idControlArchivoCaratulaPoliza;
	private Integer idControlArchivoCondicionesProducto;
	private Integer idControlArchivoNotaTecnica;
	private Integer idControlArchivoAnalisisCongruencia;
	private Integer idControlArchivoDictamenJuridico;
	
	private Short claveEstatus; 
	private Short claveActivo;
	private Date fechaCreacion;
	private Date fechaModificacion;
    private String codigoUsuarioCreacion;
	private String codigoUsuarioModificacion;
	
	private Short claveAjusteVigencia;
	
	private String claveNegocio;
	
	private Integer diasGraciaSubsecuentes;
	
	private Date fechaInicioVigencia;
	
	private List<FormaPagoDTO> formasPago;
	
	private List<MedioPagoDTO> mediosPago;
	
	/** default constructor */
	public ProductoDTO() {
		this.claveEstatus=new Short("0");
		this.claveActivo= new Short("0");
		this.fechaCreacion=new Date();
		this.fechaModificacion=new Date();
		this.codigoUsuarioCreacion="enDB";
		this.codigoUsuarioModificacion="enDB";
	}
	
	public ProductoDTO(GenericaAgentesView view){
		this.idToProducto=(view.getId()!=null)?new BigDecimal(view.getId()):null;
		this.descripcion=view.getValor();
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTOPRODUCTO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOPRODUCTO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOPRODUCTO_SEQ_GENERADOR")
	@Column(name = "IDTOPRODUCTO")
	public BigDecimal getIdToProducto() {
		return this.idToProducto;
	}

	public void setIdToProducto(BigDecimal idToProducto) {
		this.idToProducto = idToProducto;
	}

	@Column(name = "CODIGOPRODUCTO")
	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Column(name = "VERSIONPRODUCTO")
	public Integer getVersion() {
		return this.version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	@Column(name = "DESCRIPCIONPRODUCTO")
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "NOMBRECOMERCIALPRODUCTO")
	public String getNombreComercial() {
		return this.nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}

	@Column(name = "CLAVEUNIDADVIGENCIA")
	public String getClaveUnidadVigencia() {
		return this.claveUnidadVigencia;
	}

	public void setClaveUnidadVigencia(String claveUnidadVigencia) {
		this.claveUnidadVigencia = claveUnidadVigencia;
	}

	@Column(name = "VALORMINIMOUNIDADVIGENCIA")
	public BigDecimal getValorMinimoUnidadVigencia() {
		return this.valorMinimoUnidadVigencia;
	}

	public void setValorMinimoUnidadVigencia(
			BigDecimal valorMinimoUnidadVigencia) {
		this.valorMinimoUnidadVigencia = valorMinimoUnidadVigencia;
	}

	@Column(name = "VALORMAXIMOUNIDADVIGENCIA")
	public BigDecimal getValorMaximoUnidadVigencia() {
		return this.valorMaximoUnidadVigencia;
	}

	public void setValorMaximoUnidadVigencia(
			BigDecimal valorMaximoUnidadVigencia) {
		this.valorMaximoUnidadVigencia = valorMaximoUnidadVigencia;
	}

	@Column(name = "CLAVERENOVABLE")
	public String getClaveRenovable() {
		return this.claveRenovable;
	}

	public void setClaveRenovable(String claveRenovable) {
		this.claveRenovable = claveRenovable;
	}

	@Column(name = "CLAVEPAGOINMEDIATO")
	public String getClavePagoInmediato() {
		return this.clavePagoInmediato;
	}

	public void setClavePagoInmediato(String clavePagoInmediato) {
		this.clavePagoInmediato = clavePagoInmediato;
	}

	@Column(name = "DIASRETROACTIVIDAD")
	public BigDecimal getDiasRetroactividad() {
		return this.diasRetroactividad;
	}

	public void setDiasRetroactividad(BigDecimal diasRetroactividad) {
		this.diasRetroactividad = diasRetroactividad;
	}

	@Column(name = "DIASDIFERIMIENTO")
	public BigDecimal getDiasDiferimiento() {
		return this.diasDiferimiento;
	}

	public void setDiasDiferimiento(BigDecimal diasDiferimiento) {
		this.diasDiferimiento = diasDiferimiento;
	}

	@Column(name = "DIASGRACIA")
	public BigDecimal getDiasGracia() {
		return this.diasGracia;
	}

	public void setDiasGracia(BigDecimal diasGracia) {
		this.diasGracia = diasGracia;
	}

	/*
	@Column(name = "CLAVEACTIVOCONFIGURACION")
	public String getClaveActivoConfiguracion() {
		return this.claveActivoConfiguracion;
	}

	public void setClaveActivoConfiguracion(String claveActivoConfiguracion) {
		this.claveActivoConfiguracion = claveActivoConfiguracion;
	}

	@Column(name = "CLAVEACTIVOPRODUCCION")
	public String getClaveActivoProduccion() {
		return this.claveActivoProduccion;
	}

	public void setClaveActivoProduccion(String claveActivoProduccion) {
		this.claveActivoProduccion = claveActivoProduccion;
	}
	*/
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "productoDTO")
	public List<TipoPolizaDTO> getTiposPoliza() {
		return this.tiposPoliza;
	}

	public void setTiposPoliza(List<TipoPolizaDTO> tiposPoliza) {
		this.tiposPoliza = tiposPoliza;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "productoDTO")
	public List<RecargoVarioProductoDTO> getRecargoVarioProductoDTO() {
		return recargoVarioProductoDTO;
	}

	public void setRecargoVarioProductoDTO(
			List<RecargoVarioProductoDTO> recargoVarioProductoDTO) {
		this.recargoVarioProductoDTO = recargoVarioProductoDTO;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "productoDTO")
	public List<ExclusionRecargoVarioProductoDTO> getExclusionRecargoVarioProductoDTOs() {
		return exclusionRecargoVarioProductoDTOs;
	}

	public void setExclusionRecargoVarioProductoDTOs(
			List<ExclusionRecargoVarioProductoDTO> exclusionRecargoVarioProductoDTOs) {
		this.exclusionRecargoVarioProductoDTOs = exclusionRecargoVarioProductoDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "productoDTO")
	public List<DescuentoVarioProductoDTO> getDescuentos() {
		return descuentos;
	}

	public void setDescuentos(List<DescuentoVarioProductoDTO> descuentos) {
		this.descuentos = descuentos;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "productoDTO")
	public List<ExclusionAumentoVarioProductoDTO> getExclusionAumentoVarioProductoDTOs() {
		return exclusionAumentoVarioProductoDTOs;
	}

	public void setExclusionAumentoVarioProductoDTOs(
			List<ExclusionAumentoVarioProductoDTO> exclusionAumentoVarioProductoDTOs) {
		this.exclusionAumentoVarioProductoDTOs = exclusionAumentoVarioProductoDTOs;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "productoDTO")
	public List<AumentoVarioProductoDTO> getAumentoVarioProductoDTOs() {
		return aumentoVarioProductoDTOs;
	}

	public void setAumentoVarioProductoDTOs(
			List<AumentoVarioProductoDTO> aumentoVarioProductoDTOs) {
		this.aumentoVarioProductoDTOs = aumentoVarioProductoDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "productoDTO")
	public List<RecargoVarioProductoDTO> getRecargos() {
		return recargos;
	}

	public void setRecargos(List<RecargoVarioProductoDTO> recargos) {
		this.recargos = recargos;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "productoDTO")
	public List<RamoProductoDTO> getRamoProductoDTOList() {
		return ramoProductoDTOList;
	}

	public void setRamoProductoDTOList(List<RamoProductoDTO> ramoProductoDTOList) {
		this.ramoProductoDTOList = ramoProductoDTOList;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "productoDTO")
	public List<ExclusionRecargoVarioProductoDTO> getExcRecargoPorProductoDTO() {
		return excRecargoPorProductoDTO;
	}

	public void setExcRecargoPorProductoDTO(
			List<ExclusionRecargoVarioProductoDTO> excRecargoPorProductoDTO) {
		this.excRecargoPorProductoDTO = excRecargoPorProductoDTO;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "productoDTO")
	public List<AumentoVarioProductoDTO> getAumentos() {
		return aumentos;
	}

	public void setAumentos(List<AumentoVarioProductoDTO> aumentos) {
		this.aumentos = aumentos;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "productoDTO")
	public List<MonedaProductoDTO> getMonedas() {
		return monedas;
	}

	public void setMonedas(List<MonedaProductoDTO> monedas) {
		this.monedas = monedas;
	}

	@Override
	public String getDescription() {
		return this.nombreComercial;
	}

	@Override
	public Object getId() {
		return this.idToProducto;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof ProductoDTO) {
			ProductoDTO productoDTO = (ProductoDTO) object;
			equal = productoDTO.getIdToProducto().equals(this.idToProducto);
		} // End of if
		return equal;
	}

	@Column(name = "VALORDEFAULTUNIDADVIGENCIA")
	public int getValorDefaultUnidadVigencia() {
		return valorDefaultUnidadVigencia;
	}

	public void setValorDefaultUnidadVigencia(int valorDefaultUnidadVigencia) {
		this.valorDefaultUnidadVigencia = valorDefaultUnidadVigencia;
	}

	/*
	//@Column(name = "IDTIPOCALCULO")
	public Integer getIdTipoCalculo() {
		return idTipoCalculo;
	}

	public void setIdTipoCalculo(Integer idTipoCalculo) {
		this.idTipoCalculo = idTipoCalculo;
	}
	*/
	/*@Column(name = "DESCRIPCIONREGISTROCNSF")
	public String getDescripcionRegistroCNFS() {
		return descripcionRegistroCNFS;
	}

	public void setDescripcionRegistroCNFS(String descripcionRegistroCNFS) {
		this.descripcionRegistroCNFS = descripcionRegistroCNFS;
	}
	*/
	@Column(name = "IDCTRLARCHREGISTROCNSF")
	public Integer getIdControlArchivoRegistroCNSF() {
		return idControlArchivoRegistroCNSF;
	}
	public void setIdControlArchivoRegistroCNSF(Integer idControlArchivoRegistroCNSF) {
		this.idControlArchivoRegistroCNSF = idControlArchivoRegistroCNSF;
	}

	@Column(name = "IDCTRLARCHCARATULAPOLIZA")
	public Integer getIdControlArchivoCaratulaPoliza() {
		return idControlArchivoCaratulaPoliza;
	}
	public void setIdControlArchivoCaratulaPoliza(Integer idControlArchivoCaratulaPoliza) {
		this.idControlArchivoCaratulaPoliza = idControlArchivoCaratulaPoliza;
	}

	@Column(name = "IDCTRLARCHCONDICIONESPRODUCTO")
	public Integer getIdControlArchivoCondicionesProducto() {
		return idControlArchivoCondicionesProducto;
	}
	public void setIdControlArchivoCondicionesProducto(Integer idControlArchivoCondicionesProducto) {
		this.idControlArchivoCondicionesProducto = idControlArchivoCondicionesProducto;
	}

	@Column(name = "IDCTRLARCHNOTATECNICA ")
	public Integer getIdControlArchivoNotaTecnica() {
		return idControlArchivoNotaTecnica;
	}
	public void setIdControlArchivoNotaTecnica(Integer idControlArchivoNotaTecnica) {
		this.idControlArchivoNotaTecnica = idControlArchivoNotaTecnica;
	}

	@Column(name = "IDCTRLARCHANALISISCONGRUENCIA")
	public Integer getIdControlArchivoAnalisisCongruencia() {
		return idControlArchivoAnalisisCongruencia;
	}
	public void setIdControlArchivoAnalisisCongruencia(Integer idControlArchivoAnalisisCongruencia) {
		this.idControlArchivoAnalisisCongruencia = idControlArchivoAnalisisCongruencia;
	}

	@Column(name = "IDCTRLARCHDICTAMENJURIDICO")
	public Integer getIdControlArchivoDictamenJuridico() {
		return idControlArchivoDictamenJuridico;
	}

	public void setIdControlArchivoDictamenJuridico(Integer idControlArchivoDictamenJuridico) {
		this.idControlArchivoDictamenJuridico = idControlArchivoDictamenJuridico;
	}
	
	@Column(name="CLAVEESTATUS", nullable=false, precision=4, scale=0)
	public Short getClaveEstatus() {
		return claveEstatus;
	}

	public void setClaveEstatus(Short claveEstatus) {
		this.claveEstatus = claveEstatus;
	}
	
	@Column(name="CLAVEACTIVO", nullable=false, precision=4, scale=0)
	public Short getClaveActivo() {
		return claveActivo;
	}

	public void setClaveActivo(Short claveActivo) {
		this.claveActivo = claveActivo;
	}
	
	@Temporal(TemporalType.DATE)
    @Column(name="FECHACREACION", length=7, nullable=false)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Temporal(TemporalType.DATE)
    @Column(name="FECHAMODIFICACION", length=7, nullable=false)
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Column(name="CODIGOUSUARIOCREACION", nullable=false, length=8)
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}
	
	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
	
	@Column(name="CODIGOUSUARIOMODIFICACION", length=8, nullable=false)
	public String getCodigoUsuarioModificacion() {
		return codigoUsuarioModificacion;
	}	
	
	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}
	
	
	@Column(name = "DESCRIPCIONREGISTROCNSF")
	public String getDescripcionRegistroCNFS() {
		return descripcionRegistroCNFS;
	}

	public void setDescripcionRegistroCNFS(String descripcionRegistroCNFS) {
		this.descripcionRegistroCNFS = descripcionRegistroCNFS;
	}

	@Column(name = "CLAVEAJUSTEVIGENCIA")
	public Short getClaveAjusteVigencia() {
		return claveAjusteVigencia;
	}

	public void setClaveAjusteVigencia(Short claveAjusteVigencia) {
		this.claveAjusteVigencia = claveAjusteVigencia;
	}

	@Column(name = "CLAVENEGOCIO")
	public String getClaveNegocio() {
		return claveNegocio;
	}

	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}

	@Column(name = "DIASGRACIASUBSECUENTES")
	public Integer getDiasGraciaSubsecuentes() {
		return diasGraciaSubsecuentes;
	}

	public void setDiasGraciaSubsecuentes(Integer diasGraciaSubsecuentes) {
		this.diasGraciaSubsecuentes = diasGraciaSubsecuentes;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAINICIOVIGENCIA", nullable = false, length = 7)
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	@ManyToMany(cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinTable(name = "TRFORMAPAGOPRODUCTO", schema = "MIDAS",
			joinColumns = {
				@JoinColumn(name="IDTOPRODUCTO")
			},
			inverseJoinColumns = {
				@JoinColumn(name="IDTCFORMAPAGO")
			}
	)
	public List<FormaPagoDTO> getFormasPago() {
		return formasPago;
	}

	public void setFormasPago(List<FormaPagoDTO> formasPago) {
		this.formasPago = formasPago;
	}

	@ManyToMany(cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinTable(name = "TRMEDIOPAGOPRODUCTO", schema = "MIDAS",
			joinColumns = {
				@JoinColumn(name="IDTOPRODUCTO")
			},
			inverseJoinColumns = {
				@JoinColumn(name="IDTCMEDIOPAGO")
			}
	)
	public List<MedioPagoDTO> getMediosPago() {
		return mediosPago;
	}

	public void setMediosPago(List<MedioPagoDTO> mediosPago) {
		this.mediosPago = mediosPago;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.getIdToProducto();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("IdToProducto: " + this.getIdToProducto() + ", ");
		sb.append("NombreComercial: " + this.getNombreComercial());
		sb.append("]");
		
		return sb.toString();
	}
}