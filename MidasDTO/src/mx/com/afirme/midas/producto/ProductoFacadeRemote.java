package mx.com.afirme.midas.producto;

// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for ProductoDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ProductoFacadeRemote extends MidasInterfaceBase<ProductoDTO> {
	/**
	 * Perform an initial save of a previously unsaved ProductoDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            ProductoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ProductoDTO entity);

	/**
	 * Delete a persistent ProductoDTO entity.
	 * 
	 * @param entity
	 *            ProductoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ProductoDTO entity);

	/**
	 * Persist a previously saved ProductoDTO entity and return it or a copy of
	 * it to the sender. A copy of the ProductoDTO entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            ProductoDTO entity to update
	 * @return ProductoDTO the persisted ProductoDTO entity instance, may not be
	 *         the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ProductoDTO update(ProductoDTO entity);

	/**
	 * Find all ProductoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ProductoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<ProductoDTO> found by query
	 */
	public List<ProductoDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find ProductoDTO entities with a child id value.
	 * 
	 * @param id
	 *            the child id
	 * @return ProductoDTO found by query
	 */
	public ProductoDTO findByChildId(BigDecimal id);

	/**
	 * Find all ProductoDTO entities
	 * 
	 * @return List<ProductoDTO> all ProductoDTO entities
	 */
	public List<ProductoDTO> listarFiltrado(ProductoDTO entity, Boolean mostrarInactivos);
	
	/**
	 * Lista los productos disponibles
	 * agrupados por descripcion y codigoproducto
	 * @return
	 * @autor martin
	 */
	public List<ProductoDTO> listarProductosGroupBy(Long idToNegocio);
	
	/**
	 * Obtiene una lista con las versiones
	 * de un producto
	 * @param codigo
	 * @return
	 * @autor martin
	 */
	public Map<BigDecimal,Integer> getListaVersionesPorCodigoProducto(String codigo);
	
	/**
	 * Borra l�gicamente un registro de Producto, estableciendo en 0 los campos
	 * claveEstatus y CLAVEACTIVOPRODUCCION
	 * 
	 * @param entity
	 *            ProductoDTO entity el registro Producto a borrar
	 * @return ProductoDTO the persisted ProductoDTO entity instance, may not be
	 *         the same
	 * @throws RuntimeException
	 *             si la operacion falla
	 */
	public ProductoDTO borradoLogico(ProductoDTO entity);

	/**
	 * Busca los registros de Producto que no hayan sido borrados logicamente
	 * 
	 * @return List<ProductoDTO> registros de Producto que no han sido borrados
	 *         logicamente
	 */
	public List<ProductoDTO> listarVigentes();
	
	/**
	 * Find a productoDTO entity wich is related with the given solicitudDTO entity.
	 * @param idToSolicitud SolicitudDTO entity id
	 * @return ProductoDTO entity
	 */
	public ProductoDTO findProductoBySolicitud(BigDecimal idToSolicitud);
	
	public  Map<String, String> generarNuevaVersion(BigDecimal idProducto) throws Exception;
	
}


