package mx.com.afirme.midas.producto;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TCNEGOCIOSEGUROS", schema="MIDAS")
public class NegocioSeguros implements Serializable {
	
	private static final long serialVersionUID = -1901176621168692902L;

	public static final String CLAVE_AUTOS = "A";
	public static final String CLAVE_CASA = "C";
	public static final String CLAVE_DANOS = "D";
	public static final String CLAVE_VIDA = "V";
	
	private String clave;
	private String nombre;
	
	@Id
	public String getClave() {
		return clave;
	}
	
	public void setClave(String clave) {
		this.clave = clave;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}
