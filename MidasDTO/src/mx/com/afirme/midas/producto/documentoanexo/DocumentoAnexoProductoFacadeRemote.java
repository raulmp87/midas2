package mx.com.afirme.midas.producto.documentoanexo;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for DocumentoAnexoProductoDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface DocumentoAnexoProductoFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved DocumentoAnexoProductoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DocumentoAnexoProductoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DocumentoAnexoProductoDTO entity);
    /**
	 Delete a persistent DocumentoAnexoProductoDTO entity.
	  @param entity DocumentoAnexoProductoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DocumentoAnexoProductoDTO entity);
   /**
	 Persist a previously saved DocumentoAnexoProductoDTO entity and return it or a copy of it to the sender. 
	 A copy of the DocumentoAnexoProductoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DocumentoAnexoProductoDTO entity to update
	 @return DocumentoAnexoProductoDTO the persisted DocumentoAnexoProductoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public DocumentoAnexoProductoDTO update(DocumentoAnexoProductoDTO entity);
	public DocumentoAnexoProductoDTO findById( BigDecimal id);
	 /**
	 * Find all DocumentoAnexoProductoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DocumentoAnexoProductoDTO property to query
	  @param value the property value to match
	  	  @return List<DocumentoAnexoProductoDTO> found by query
	 */
	public List<DocumentoAnexoProductoDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all DocumentoAnexoProductoDTO entities.
	  	  @return List<DocumentoAnexoProductoDTO> all DocumentoAnexoProductoDTO entities
	 */
	public List<DocumentoAnexoProductoDTO> findAll(
		);	
}