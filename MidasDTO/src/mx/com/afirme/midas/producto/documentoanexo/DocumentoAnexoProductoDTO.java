package mx.com.afirme.midas.producto.documentoanexo;
// default package

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.producto.ProductoDTO;


/**
 * DocumentoAnexoProductoDTO entity. @author José Luis Arellano
 */
@Entity
@Table(name="TODOCUMENTOANEXOPRODUCTO",schema="MIDAS")
public class DocumentoAnexoProductoDTO  implements java.io.Serializable {
	private static final long serialVersionUID = -886765693786722035L;
	private BigDecimal idToDocumentoAnexoProducto;
     private ProductoDTO productoDTO;
     private Short claveObligatoriedad;
     private BigDecimal numeroSecuencia;
     private BigDecimal idToControlArchivo;
     private String descripcion;
     private Date fechaCreacion;
     private String codigoUsuarioCreacion;
     private String nombreUsuarioCreacion;
     private Date fechaModificacion;
     private String codigoUsuarioModificacion;
     private String nombreUsuarioModificacion;


    // Constructors

    /** default constructor */
    public DocumentoAnexoProductoDTO() {
    }

    @Id     
    @SequenceGenerator(name = "IDTODOCUMENTOANEXOPRODUCTO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTODOCANEXOPRODUCTO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTODOCUMENTOANEXOPRODUCTO_SEQ_GENERADOR")
    @Column(name="IDTODOCANEXOPRODUCTO", nullable=false, precision=22, scale=0)
    public BigDecimal getIdToDocumentoAnexoProducto() {
        return this.idToDocumentoAnexoProducto;
    }
    public void setIdToDocumentoAnexoProducto(BigDecimal idToDocumentoAnexoProducto) {
        this.idToDocumentoAnexoProducto = idToDocumentoAnexoProducto;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTOPRODUCTO", nullable=false)
    public ProductoDTO getProductoDTO() {
		return productoDTO;
	}
	public void setProductoDTO(ProductoDTO productoDTO) {
		this.productoDTO = productoDTO;
	}
	
    @Column(name="CLAVEOBLIGATORIEDAD", nullable=false, precision=4, scale=0)
    public Short getClaveObligatoriedad() {
        return this.claveObligatoriedad;
    }

	public void setClaveObligatoriedad(Short claveObligatoriedad) {
        this.claveObligatoriedad = claveObligatoriedad;
    }
    
    @Column(name="NUMEROSECUENCIA", nullable=false, precision=22, scale=0)
    public BigDecimal getNumeroSecuencia() {
        return this.numeroSecuencia;
    }
    public void setNumeroSecuencia(BigDecimal numeroSecuencia) {
        this.numeroSecuencia = numeroSecuencia;
    }
    
    @Column(name="IDTOCONTROLARCHIVO", nullable=false, precision=22, scale=0)
    public BigDecimal getIdToControlArchivo() {
        return this.idToControlArchivo;
    }
    public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
        this.idToControlArchivo = idToControlArchivo;
    }
    
    @Column(name="DESCRIPCIONDOCUMENTOANEXO", nullable=false, length=200)
    public String getDescripcion() {
        return this.descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    @Temporal(TemporalType.DATE)
    @Column(name="FECHACREACION", nullable=false, length=7)
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    
    @Column(name="CODIGOUSUARIOCREACION", nullable=false, length=8)
    public String getCodigoUsuarioCreacion() {
        return this.codigoUsuarioCreacion;
    }
    public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
        this.codigoUsuarioCreacion = codigoUsuarioCreacion;
    }
    
    @Column(name="NOMBREUSUARIOCREACION", nullable=false, length=200)
    public String getNombreUsuarioCreacion() {
        return this.nombreUsuarioCreacion;
    }
    public void setNombreUsuarioCreacion(String nombreUsuarioCreacion) {
        this.nombreUsuarioCreacion = nombreUsuarioCreacion;
    }
    
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAMODIFICACION", length=7)
    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }
    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
    
    @Column(name="CODIGOUSUARIOMODIFICACION", length=8)
    public String getCodigoUsuarioModificacion() {
        return this.codigoUsuarioModificacion;
    }
    public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
        this.codigoUsuarioModificacion = codigoUsuarioModificacion;
    }
    
    @Column(name="NOMBREUSUARIOMODIFICACION", length=200)
    public String getNombreUsuarioModificacion() {
        return this.nombreUsuarioModificacion;
    }
    public void setNombreUsuarioModificacion(String nombreUsuarioModificacion) {
        this.nombreUsuarioModificacion = nombreUsuarioModificacion;
    }
   }