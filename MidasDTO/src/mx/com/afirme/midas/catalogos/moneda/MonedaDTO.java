package mx.com.afirme.midas.catalogos.moneda;
// default package

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * MonedaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "VNMONEDA", schema = "MIDAS")
public class MonedaDTO extends CacheableDTO implements java.io.Serializable, Entidad {
	private static final long serialVersionUID = 2L;

	public static final int MONEDA_PESOS = 484;
	public static final int MONEDA_DOLARES = 840;
	
	// Fields

	private Short idTcMoneda;
	private String descripcion;

	// Constructors

	/** default constructor */
	public MonedaDTO() {
	}

	// Property accessors
	@Id
	@Column(name = "IDTCMONEDA", nullable = false, precision = 3, scale = 0)
	public Short getIdTcMoneda() {
		return this.idTcMoneda;
	}

	public void setIdTcMoneda(Short idTcMoneda) {
		this.idTcMoneda = idTcMoneda;
	}

	@Column(name = "DESCRIPCION", nullable = false, length = 20)
	public String getDescripcion() {
		return this.descripcion!=null?this.descripcion.toUpperCase():this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof MonedaDTO))
			return false;
		MonedaDTO castOther = (MonedaDTO) other;

		return ((this.getIdTcMoneda() == castOther.getIdTcMoneda()) || (this
				.getIdTcMoneda() != null
				&& castOther.getIdTcMoneda() != null && this.getIdTcMoneda()
				.equals(castOther.getIdTcMoneda())))
				&& ((this.getDescripcion() == castOther.getDescripcion()) || (this
						.getDescripcion() != null
						&& castOther.getDescripcion() != null && this
						.getDescripcion().equals(castOther.getDescripcion())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdTcMoneda() == null ? 0 : this.getIdTcMoneda()
						.hashCode());
		result = 37
				* result
				+ (getDescripcion() == null ? 0 : this.getDescripcion()
						.hashCode());
		return result;
	}

	@Override
	public String getDescription() {
		return this.descripcion;
	}

	@Override
	public Object getId() {
		return this.idTcMoneda;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Short getKey() {
		return this.idTcMoneda;
	}

	@Override
	public String getValue() {
		return descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Short getBusinessKey() {
		return this.idTcMoneda;
	}
}