package mx.com.afirme.midas.catalogos.moneda;
// default package

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for MonedaFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface MonedaFacadeRemote extends MidasInterfaceBase<MonedaDTO> {
	/**
	 * Perform an initial save of a previously unsaved MonedaDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            MonedaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(MonedaDTO entity);

	/**
	 * Delete a persistent MonedaDTO entity.
	 * 
	 * @param entity
	 *            MonedaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(MonedaDTO entity);

	/**
	 * Persist a previously saved MonedaDTO entity and return it or a copy of it to
	 * the sender. A copy of the MonedaDTO entity parameter is returned when the
	 * JPA persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            MonedaDTO entity to update
	 * @return MonedaDTO the persisted MonedaDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public MonedaDTO update(MonedaDTO entity);

	public MonedaDTO findById(Short id);

	/**
	 * Find all MonedaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the MonedaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<MonedaDTO> found by query
	 */
	public List<MonedaDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all MonedaDTO entities.
	 * 
	 * @return List<MonedaDTO> all MonedaDTO entities
	 */
	public List<MonedaDTO> findAll();

	public List<MonedaDTO> listarMonedasAsociadas(BigDecimal idToProducto);
	public List<MonedaDTO> listarMonedasPorAsociar(BigDecimal idToProducto);
	
	/**
	 * Find all MonedaDTO entities not related with a TipoPoliza.
	 * @param BigDecimal idToTipoPoliza
	 * 
	 * @return List<MonedaDTO> all MonedaDTO entities not related with the tipoPoliza entity
	 */
	public List<MonedaDTO> findNotRelatedTipoPoliza(BigDecimal idToTipoPoliza,BigDecimal idToProducto);
	
	/**
	 * Find all MonedaDTO entities related with a TipoPoliza.
	 * @param BigDecimal idToTipoPoliza
	 * @return List<MonedaDTO> all MonedaDTO entities not related with the tipoPoliza entity
	 */
	public List<MonedaDTO> findRelatedTipoPoliza(BigDecimal idToTipoPoliza);
	
	/**
	 * Find all MonedaDTO entities related with a TipoPoliza Entity.
	 * @param BigDecimal idToTipoPoliza
	 * @return List<MonedaDTO> all MonedaDTO entities not related with the tipoPoliza entity
	 */
	public List<MonedaDTO> listarMonedasAsociadasTipoPoliza(BigDecimal idToTipoPoliza);
}