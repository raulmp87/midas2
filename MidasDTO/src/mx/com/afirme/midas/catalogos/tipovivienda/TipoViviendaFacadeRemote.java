package mx.com.afirme.midas.catalogos.tipovivienda;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;


public interface TipoViviendaFacadeRemote extends MidasInterfaceBase<TipoViviendaDTO> {

}
