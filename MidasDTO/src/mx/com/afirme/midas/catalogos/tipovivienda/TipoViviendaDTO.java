package mx.com.afirme.midas.catalogos.tipovivienda;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.base.PermitidoCotizarCasa;
import mx.com.afirme.midas.base.TipoHorizontal;

@Entity
@Table(schema="MIDAS", name="TCTIPOVIVIENDA")
public class TipoViviendaDTO extends CacheableDTO implements TipoHorizontal, PermitidoCotizarCasa {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -1227408776175247215L;

	private BigDecimal idTipoVivienda;

	private String descripcionTipoVivienda;
	private BigDecimal codigoTipoVivienda;
	private Boolean permitidoCasa;
	private Boolean tipoHorizontal;
	
	public TipoViviendaDTO() {
		
	}

	@Id
	@SequenceGenerator(name = "IDTCTIPOVIVIENDA_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCTIPOVIVIENDA_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCTIPOVIVIENDA_SEQ_GENERADOR")
	@Column(name = "IDTCTIPOVIVIENDA", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTipoVivienda() {
		return idTipoVivienda;
	}

	public void setIdTipoVivienda(BigDecimal idTipoVivienda) {
		this.idTipoVivienda = idTipoVivienda;
	}

	@Column
	public String getDescripcionTipoVivienda() {
		return descripcionTipoVivienda;
	}

	public void setDescripcionTipoVivienda(String descripcionTipoVivienda) {
		this.descripcionTipoVivienda = descripcionTipoVivienda;
	}

	@Column(name = "CODIGOTIPOVIVIENDA", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoTipoVivienda() {
		return codigoTipoVivienda;
	}

	public void setCodigoTipoVivienda(BigDecimal codigoTipoVivienda) {
		this.codigoTipoVivienda = codigoTipoVivienda;
	}

	@Column
	public Boolean getPermitidoCasa() {
		return permitidoCasa;
	}

	public void setPermitidoCasa(Boolean permitidoCasa) {
		this.permitidoCasa = permitidoCasa;
	}

	@Column
	public Boolean getTipoHorizontal() {
		return tipoHorizontal;
	}

	public void setTipoHorizontal(Boolean tipoHorizontal) {
		this.tipoHorizontal = tipoHorizontal;
	}

	@Override
	public String getDescription() {
		return getDescripcionTipoVivienda();
	}

	@Override
	public Object getId() {
		return getIdTipoVivienda();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((descripcionTipoVivienda == null) ? 0
						: descripcionTipoVivienda.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof TipoViviendaDTO) {
			TipoViviendaDTO dto = (TipoViviendaDTO) object;
			equal = dto.getDescripcionTipoVivienda().equals(this.descripcionTipoVivienda);
		}
		return equal;
	}
	
}
