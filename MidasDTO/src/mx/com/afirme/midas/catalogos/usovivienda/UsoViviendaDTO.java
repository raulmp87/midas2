package mx.com.afirme.midas.catalogos.usovivienda;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.base.PermitidoCotizarCasa;

@Entity
@Table(schema="MIDAS", name="TCUSOVIVIENDA")
public class UsoViviendaDTO extends CacheableDTO implements PermitidoCotizarCasa {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6634429230568624126L;

	private BigDecimal idUsoVivienda;

	private String descripcionUsoVivienda;
	private BigDecimal codigoUsoVivienda;
	private Boolean permitidoCasa;
	
	public UsoViviendaDTO() {
		
	}

	@Id
	@SequenceGenerator(name = "IDTCUSOIVIENDA_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCUSOVIVIENDA_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCUSOVIVIENDA_SEQ_GENERADOR")
	@Column(name = "IDTCUSOVIVIENDA", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdUsoVivienda() {
		return idUsoVivienda;
	}

	public void setIdUsoVivienda(BigDecimal idUsoVivienda) {
		this.idUsoVivienda = idUsoVivienda;
	}

	@Column
	public String getDescripcionUsoVivienda() {
		return descripcionUsoVivienda;
	}

	public void setDescripcionUsoVivienda(String descripcionUsoVivienda) {
		this.descripcionUsoVivienda = descripcionUsoVivienda;
	}

	@Column
	public BigDecimal getCodigoUsoVivienda() {
		return codigoUsoVivienda;
	}

	public void setCodigoUsoVivienda(BigDecimal codigoUsoVivienda) {
		this.codigoUsoVivienda = codigoUsoVivienda;
	}

	@Column
	public Boolean getPermitidoCasa() {
		return permitidoCasa;
	}

	public void setPermitidoCasa(Boolean permitidoCasa) {
		this.permitidoCasa = permitidoCasa;
	}

	@Override
	public String getDescription() {
		return getDescripcionUsoVivienda();
	}

	@Override
	public Object getId() {
		return getIdUsoVivienda();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((descripcionUsoVivienda == null) ? 0
						: descripcionUsoVivienda.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof UsoViviendaDTO) {
			UsoViviendaDTO dto = (UsoViviendaDTO) object;
			equal = dto.getDescripcionUsoVivienda().equals(this.descripcionUsoVivienda);
		} 
		return equal;
	}
	
}
