package mx.com.afirme.midas.catalogos.usovivienda;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;


public interface UsoViviendaFacadeRemote extends MidasInterfaceBase<UsoViviendaDTO> {

}
