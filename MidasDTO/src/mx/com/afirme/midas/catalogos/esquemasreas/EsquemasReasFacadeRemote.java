package mx.com.afirme.midas.catalogos.esquemasreas;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.base.MidasInterfaceBase;

public interface EsquemasReasFacadeRemote extends MidasInterfaceBase<EsquemasDTO> {
	
    public void save(EsquemasDTO entity);
    
    public void delete(EsquemasDTO entity);
 
	public int update(EsquemasDTO entity);

	public List<EsquemasDTO> findByPropertyID(BigDecimal value);
	
	public List<EsquemasDTO> findAll();
	
	public List<EsquemasDTO> listarFiltrado(EsquemasDTO contactoDTO);
}