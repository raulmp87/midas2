package mx.com.afirme.midas.catalogos.esquemasreas;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;


@Entity
@Table(name = "CNSF_REAS_ESQ", schema = "MIDAS")
public class EsquemasDTO extends CacheableDTO implements java.io.Serializable {
	private static final long serialVersionUID = -2203293929348324069L;
	
	private BigDecimal id;
	private String idContrato;
	private BigDecimal anio;
	private BigDecimal cer;
	private BigDecimal tipoCobertura;
	private BigDecimal nivel;
	private BigDecimal ordenEntrada;
	
	private BigDecimal retencion;
	private BigDecimal capLim;
	private BigDecimal retAdic;
	private BigDecimal reinstalaciones;
	private BigDecimal llaveD;
	private BigDecimal llaveRet;
	private String claveREAS;
	private BigDecimal partREAS;
	private BigDecimal calificacion;
	private String combinacion;
	private BigDecimal aut;
	private BigDecimal cveNegocio;
	private BigDecimal dia;
	private BigDecimal mes;
	private BigDecimal tipoMoneda;
	private String claveReasAlter;
	
	
	/** default constructor */
	public EsquemasDTO() {
		aut = new BigDecimal(0);
		dia = new BigDecimal(1);
		mes = new BigDecimal(4);
		tipoMoneda = new BigDecimal(1);
		cveNegocio = new BigDecimal(2);
	}
	@Id
	@Column(name = "ID", nullable = false, precision = 22, scale = 0)
	@SequenceGenerator(name = "ID_SEQ_GENERADORESQ", allocationSize = 1, sequenceName = "MIDAS.CNSF_ESQREAS_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_SEQ_GENERADORESQ")
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	@Column(name = "ANIO", nullable = true)
	public BigDecimal getAnio() {
		return anio;
	}

	public void setAnio(BigDecimal anio) {
		this.anio = anio;
	}
	@Column(name = "CER", nullable = true)
	public BigDecimal getCer() {
		return cer;
	}

	public void setCer(BigDecimal cer) {
		this.cer = cer;
	}
	@Column(name = "TIPO_COBERTURA", nullable = true)
	public BigDecimal getTipoCobertura() {
		return tipoCobertura;
	}

	public void setTipoCobertura(BigDecimal tipoCobertura) {
		this.tipoCobertura = tipoCobertura;
	}
	
	@Column(name = "ID_CONTRATO", nullable = true)
	public String getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(String idContrato) {
		this.idContrato = idContrato;
	}
	@Column(name = "ORDEN_ENTRADA", nullable = true)
	public BigDecimal getOrdenEntrada() {
		return ordenEntrada;
	}

	public void setOrdenEntrada(BigDecimal ordenEntrada) {
		this.ordenEntrada = ordenEntrada;
	}
	
	@Column(name = "CVE_REASEGURADORA", nullable = true)
	public String getClaveREAS() {
		return claveREAS;
	}

	public void setClaveREAS(String claveREAS) {
		this.claveREAS = claveREAS;
	}
	
	@Column(name = "CVE_REAS_ALTER", nullable = true)
	public String getClaveReasAlter() {
		return claveReasAlter;
	}
	public void setClaveReasAlter(String claveReasAlter) {
		this.claveReasAlter = claveReasAlter;
	}
	@Column(name = "PART_REASEGURADOR", nullable = true)
	public BigDecimal getPartREAS() {
		return partREAS;
	}

	public void setPartREAS(BigDecimal partREAS) {
		this.partREAS = partREAS;
	}
	@Column(name = "CALIFICACION", nullable = true)
	public BigDecimal getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(BigDecimal calificacion) {
		this.calificacion = calificacion;
	}
	@Column(name = "COMBINACION", nullable = true)
	public String getCombinacion() {
		return combinacion;
	}

	public void setCombinacion(String combinacion) {
		this.combinacion = combinacion;
	}
	@Column(name = "AUT", nullable = true)
	public BigDecimal getAut() {
		return aut;
	}
	public void setAut(BigDecimal aut) {
		this.aut = aut;
	}
	@Column(name = "CVE_NEGOCIO", nullable = true)
	public BigDecimal getCveNegocio() {
		return cveNegocio;
	}
	public void setCveNegocio(BigDecimal cveNegocio) {
		this.cveNegocio = cveNegocio;
	}
	@Column(name = "NIVEL", nullable = true)
	public BigDecimal getNivel() {
		return nivel;
	}
	public void setNivel(BigDecimal nivel) {
		this.nivel = nivel;
	}
	@Column(name = "RETENCION", nullable = true)
	public BigDecimal getRetencion() {
		return retencion;
	}
	public void setRetencion(BigDecimal retencion) {
		this.retencion = retencion;
	}
	@Column(name = "CAP_LIMITE", nullable = true)
	public BigDecimal getCapLim() {
		return capLim;
	}
	public void setCapLim(BigDecimal capLim) {
		this.capLim = capLim;
	}
	@Column(name = "RETENCION_AD", nullable = true)
	public BigDecimal getRetAdic() {
		return retAdic;
	}
	public void setRetAdic(BigDecimal retAdic) {
		this.retAdic = retAdic;
	}
	@Column(name = "REINSTALACIONES", nullable = true)
	public BigDecimal getReinstalaciones() {
		return reinstalaciones;
	}
	public void setReinstalaciones(BigDecimal reinstalaciones) {
		this.reinstalaciones = reinstalaciones;
	}
	@Column(name = "LLAVE_DIRECTA", nullable = true)
	public BigDecimal getLlaveD() {
		return llaveD;
	}
	public void setLlaveD(BigDecimal llaveD) {
		this.llaveD = llaveD;
	}
	@Column(name = "LLAVE_RET", nullable = true)
	public BigDecimal getLlaveRet() {
		return llaveRet;
	}
	public void setLlaveRet(BigDecimal llaveRet) {
		this.llaveRet = llaveRet;
	}
	@Column(name = "DIA", nullable = true)
	public BigDecimal getDia() {
		return dia;
	}
	public void setDia(BigDecimal dia) {
		this.dia = dia;
	}
	@Column(name = "MES", nullable = true)
	public BigDecimal getMes() {
		return mes;
	}
	public void setMes(BigDecimal mes) {
		this.mes = mes;
	}
	@Column(name = "TIPO_MONEDA", nullable = true)
	public BigDecimal getTipoMoneda() {
		return tipoMoneda;
	}
	public void setTipoMoneda(BigDecimal tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}
	@Override
	public String getDescription() {
		return toString();
	}
	@Override
	public boolean equals(Object object) {
		return false;
	}
	@Override
	public String toString() {
		return "EsquemasDTO [id=" + id + ", idContrato=" + idContrato
				+ ", anio=" + anio + ", cer=" + cer + ", tipoCobertura="
				+ tipoCobertura + ", nivel=" + nivel + ", ordenEntrada="
				+ ordenEntrada + ", retencion=" + retencion + ", capLim="
				+ capLim + ", retAdic=" + retAdic + ", reinstalaciones="
				+ reinstalaciones + ", llaveD=" + llaveD + ", llaveRet="
				+ llaveRet + ", claveREAS=" + claveREAS + ", partREAS="
				+ partREAS + ", calificacion=" + calificacion
				+ ", combinacion=" + combinacion + ", aut=" + aut
				+ ", cve_negocio=" + cveNegocio + "]";
	}
	
}