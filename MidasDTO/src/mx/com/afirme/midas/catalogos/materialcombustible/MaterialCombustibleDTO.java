package mx.com.afirme.midas.catalogos.materialcombustible;
// default package

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.base.CacheableDTO;


/**
 * MaterialCombustibleDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCMATERIALCOMBUSTIBLE"
    ,schema="MIDAS"
, uniqueConstraints = @UniqueConstraint(columnNames="IDTCMATERIALCOMBUSTIBLE")
)

public class MaterialCombustibleDTO  extends CacheableDTO {


	private static final long serialVersionUID = 6303055558309138840L;
	private BigDecimal idMaterialCombustible;
    private BigDecimal codigoMaterialCombustible;
    private String descripcionMaterialCombustible;


    // Constructors

    /** default constructor */
    public MaterialCombustibleDTO() {
    }

    @Id 
    @SequenceGenerator(name = "IDTCMATERIALCOMBUSTIBLE_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCMATERIALCOMBUSTIBLE_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCMATERIALCOMBUSTIBLE_SEQ_GENERADOR")
    @Column(name="IDTCMATERIALCOMBUSTIBLE", unique=true, nullable=false, precision=22, scale=0)
	public BigDecimal getIdMaterialCombustible() {
		return idMaterialCombustible;
	}


	public void setIdMaterialCombustible(BigDecimal idMaterialCombustible) {
		this.idMaterialCombustible = idMaterialCombustible;
	}

	@Column(name="CODIGOMATERIALCOMBUSTIBLE", unique=true, nullable=false, precision=22, scale=0)
	public BigDecimal getCodigoMaterialCombustible() {
		return codigoMaterialCombustible;
	}


	public void setCodigoMaterialCombustible(BigDecimal codigoMaterialCombustible) {
		this.codigoMaterialCombustible = codigoMaterialCombustible;
	}

	@Column(name="DESCRIPCIONMATERIALCOMBUSTIBLE", nullable=false, length=200)
	public String getDescripcionMaterialCombustible() {
		return descripcionMaterialCombustible;
	}


	public void setDescripcionMaterialCombustible(
			String descripcionMaterialCombustible) {
		this.descripcionMaterialCombustible = descripcionMaterialCombustible;
	}

	@Override
	public String getDescription() {
		return this.descripcionMaterialCombustible;
	}

	@Override
	public Object getId() {
		return this.idMaterialCombustible;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof MaterialCombustibleDTO) {
			MaterialCombustibleDTO materialCombustibleDTO = (MaterialCombustibleDTO) object;
			equal = materialCombustibleDTO.getIdMaterialCombustible().equals(this.idMaterialCombustible);
		} // End of if
		return equal;
	}
}