package mx.com.afirme.midas.catalogos.materialcombustible;
// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for MaterialCombustibleFacade.
 * @author MyEclipse Persistence Tools
 */


public interface MaterialCombustibleFacadeRemote extends MidasInterfaceBase<MaterialCombustibleDTO> {
		/**
	 Perform an initial save of a previously unsaved MaterialCombustibleDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity MaterialCombustibleDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(MaterialCombustibleDTO entity);
    /**
	 Delete a persistent MaterialCombustibleDTO entity.
	  @param entity MaterialCombustibleDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(MaterialCombustibleDTO entity);
   /**
	 Persist a previously saved MaterialCombustibleDTO entity and return it or a copy of it to the sender. 
	 A copy of the MaterialCombustibleDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity MaterialCombustibleDTO entity to update
	 @return MaterialCombustibleDTO the persisted MaterialCombustibleDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public MaterialCombustibleDTO update(MaterialCombustibleDTO entity);

	 /**
	 * Find all MaterialCombustibleDTO entities with a specific property value.  
	 
	  @param propertyName the name of the MaterialCombustibleDTO property to query
	  @param value the property value to match
	  	  @return List<MaterialCombustibleDTO> found by query
	 */
	public List<MaterialCombustibleDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all MaterialCombustibleDTO entities.
	  	  @return List<MaterialCombustibleDTO> all MaterialCombustibleDTO entities
	 */
	public List<MaterialCombustibleDTO> findAll(
		);	
	
	public List<MaterialCombustibleDTO> listarFiltrado(MaterialCombustibleDTO materialCombustibleDTO);
}