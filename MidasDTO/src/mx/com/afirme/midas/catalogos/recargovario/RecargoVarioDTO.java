package mx.com.afirme.midas.catalogos.recargovario;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.producto.configuracion.recargo.RecargoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.recargo.exclusion.ExclusionRecargoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.RecargoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.recargo.exclusion.ExclusionRecargoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.RecargoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.exclusion.ExclusionRecargoVarioCoberturaDTO;


/**
 * RecargoVarioDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TORECARGOVARIO"
    ,schema="MIDAS"
)

public class RecargoVarioDTO  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 5810506040738507732L;
	private BigDecimal idtorecargovario;
     private String descripcionrecargo;
     private Short clavetiporecargo;
     private List<RecargoVarioProductoDTO> recargoVarioProductoDTOs = new ArrayList<RecargoVarioProductoDTO>();
     private List<RecargoVarioCoberturaDTO> recargoVarioCoberturaDTOs = new ArrayList<RecargoVarioCoberturaDTO>();
     private List<RecargoVarioTipoPolizaDTO> recargoVarioTipoPolizaDTOs = new ArrayList<RecargoVarioTipoPolizaDTO>();
     private List<ExclusionRecargoVarioProductoDTO> exclusionRecargoVarioProductoDTOs = new ArrayList<ExclusionRecargoVarioProductoDTO>();
     private List<ExclusionRecargoVarioTipoPolizaDTO> exclusionRecargoVarioTipoPolizaDTOs = new ArrayList<ExclusionRecargoVarioTipoPolizaDTO>();
     private List<ExclusionRecargoVarioCoberturaDTO> exclusionRecargoVarioCoberturaDTOs = new ArrayList<ExclusionRecargoVarioCoberturaDTO>();


    // Constructors

    /** default constructor */
    public RecargoVarioDTO() {
    }
    
    // Property accessors
    @Id 
    @SequenceGenerator(name = "IDTORECARGOVARIO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTORECARGOVARIO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTORECARGOVARIO_SEQ_GENERADOR")
    @Column(name="IDTORECARGOVARIO", unique=true, nullable=false, precision=22, scale=0)

    public BigDecimal getIdtorecargovario() {
        return this.idtorecargovario;
    }
    
    public void setIdtorecargovario(BigDecimal idtorecargovario) {
        this.idtorecargovario = idtorecargovario;
    }
    
    @Column(name="DESCRIPCIONRECARGO", nullable=false, length=200)

    public String getDescripcionrecargo() {
        return this.descripcionrecargo;
    }
    
    public void setDescripcionrecargo(String descripcionrecargo) {
        this.descripcionrecargo = descripcionrecargo;
    }
    
    @Column(name="CLAVETIPORECARGO", nullable=false, precision=4, scale=0)

    public Short getClavetiporecargo() {
        return this.clavetiporecargo;
    }
    
    public void setClavetiporecargo(Short clavetiporecargo) {
        this.clavetiporecargo = clavetiporecargo;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="recargoVarioDTO")
	public List<ExclusionRecargoVarioProductoDTO> getExclusionRecargoVarioProductoDTOs() {
		return exclusionRecargoVarioProductoDTOs;
	}

	public void setExclusionRecargoVarioProductoDTOs(
		List<ExclusionRecargoVarioProductoDTO> exclusionRecargoVarioProductoDTOs) {
		this.exclusionRecargoVarioProductoDTOs = exclusionRecargoVarioProductoDTOs;
	}
   
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="recargoVarioDTO")
	public List<RecargoVarioProductoDTO> getRecargoVarioProductoDTOs() {
		return recargoVarioProductoDTOs;
	}
	
	public void setRecargoVarioProductoDTOs(
			List<RecargoVarioProductoDTO> recargoVarioProductoDTOs) {
		this.recargoVarioProductoDTOs = recargoVarioProductoDTOs;
	}
   
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="recargoVarioDTO")
	public List<RecargoVarioCoberturaDTO> getRecargoVarioCoberturaDTOs() {
		return recargoVarioCoberturaDTOs;
	}

	public void setRecargoVarioCoberturaDTOs(
			List<RecargoVarioCoberturaDTO> recargoVarioCoberturaDTOs) {
		this.recargoVarioCoberturaDTOs = recargoVarioCoberturaDTOs;
	}
	
	
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="recargoVarioDTO")
	public List<ExclusionRecargoVarioTipoPolizaDTO> getExclusionRecargoVarioTipoPolizaDTOs() {
		return exclusionRecargoVarioTipoPolizaDTOs;
	}
	
	public void setExclusionRecargoVarioTipoPolizaDTOs(
			List<ExclusionRecargoVarioTipoPolizaDTO> exclusionRecargoVarioTipoPolizaDTOs) {
		this.exclusionRecargoVarioTipoPolizaDTOs = exclusionRecargoVarioTipoPolizaDTOs;
	}

	
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="recargoVarioDTO")
	public List<ExclusionRecargoVarioCoberturaDTO> getExclusionRecargoVarioCoberturaDTOs() {
		return exclusionRecargoVarioCoberturaDTOs;
	}

	public void setExclusionRecargoVarioCoberturaDTOs(
			List<ExclusionRecargoVarioCoberturaDTO> exclusionRecargoVarioCoberturaDTOs) {
		this.exclusionRecargoVarioCoberturaDTOs = exclusionRecargoVarioCoberturaDTOs;
	}

@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="recargoVarioDTO")
	public List<RecargoVarioTipoPolizaDTO> getRecargoVarioTipoPolizaDTOs() {
		return recargoVarioTipoPolizaDTOs;
	}

	public void setRecargoVarioTipoPolizaDTOs(
			List<RecargoVarioTipoPolizaDTO> recargoVarioTipoPolizaDTOs) {
		this.recargoVarioTipoPolizaDTOs = recargoVarioTipoPolizaDTOs;
	}
   

	








}