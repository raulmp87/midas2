package mx.com.afirme.midas.catalogos.recargovario;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDTO;

/**
 * Remote interface for RecargoVarioFacade.
 * @author MyEclipse Persistence Tools
 */


public interface RecargoVarioFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved RecargoVarioDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity RecargoVarioDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(RecargoVarioDTO entity);
    /**
	 Delete a persistent RecargoVarioDTO entity.
	  @param entity RecargoVarioDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(RecargoVarioDTO entity);
   /**
	 Persist a previously saved RecargoVarioDTO entity and return it or a copy of it to the sender. 
	 A copy of the RecargoVarioDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity RecargoVarioDTO entity to update
	 @return RecargoVarioDTO the persisted RecargoVarioDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public RecargoVarioDTO update(RecargoVarioDTO entity);
	public RecargoVarioDTO findById( BigDecimal id);
	 /**
	 * Find all RecargoVarioDTO entities with a specific property value.  
	 
	  @param propertyName the name of the RecargoVarioDTO property to query
	  @param value the property value to match
	  	  @return List<RecargoVarioDTO> found by query
	 */
	public List<RecargoVarioDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all RecargoVarioDTO entities.
	  	  @return List<RecargoVarioDTO> all RecargoVarioDTO entities
	 */
	public List<RecargoVarioDTO> findAll(
		);	
	
	public List<RecargoVarioDTO> listarFiltrado(RecargoVarioDTO recargoVarioDTO);

	public List<RecargoVarioDTO> listarRecargosPorAsociar(BigDecimal idProducto);
	
	public List<RecargoVarioDTO> listarRecargosPorAsociarTipoPoliza(BigDecimal idToTipoPoliza);

	public List<RecargoVarioDTO> listarRecargosPorAsociarCobertura(BigDecimal idToCobertura);

	public List<RecargoVarioDTO> listarRecargosEspeciales();
}