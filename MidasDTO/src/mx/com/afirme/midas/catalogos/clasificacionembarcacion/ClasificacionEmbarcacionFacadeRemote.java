package mx.com.afirme.midas.catalogos.clasificacionembarcacion;
// default package

import java.util.List;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for ClasificacionEmbarcacionFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ClasificacionEmbarcacionFacadeRemote extends MidasInterfaceBase<ClasificacionEmbarcacionDTO> {
	/**
	 * Perform an initial save of a previously unsaved ClasificacionEmbarcacionDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            ClasificacionEmbarcacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ClasificacionEmbarcacionDTO entity);

	/**
	 * Delete a persistent ClasificacionEmbarcacionDTO entity.
	 * 
	 * @param entity
	 *            ClasificacionEmbarcacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ClasificacionEmbarcacionDTO entity);

	/**
	 * Persist a previously saved ClasificacionEmbarcacionDTO entity and return it
	 * or a copy of it to the sender. A copy of the ClasificacionEmbarcacionDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            ClasificacionEmbarcacionDTO entity to update
	 * @return ClasificacionEmbarcacionDTO the persisted ClasificacionEmbarcacionDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ClasificacionEmbarcacionDTO update(ClasificacionEmbarcacionDTO entity);

	/**
	 * Find all ClasificacionEmbarcacionDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the ClasificacionEmbarcacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<ClasificacionEmbarcacionDTO> found by query
	 */
	public List<ClasificacionEmbarcacionDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all ClasificacionEmbarcacionDTO entities.
	 * 
	 * @return List<ClasificacionEmbarcacionDTO> all ClasificacionEmbarcacionDTO
	 *         entities
	 */
	public List<ClasificacionEmbarcacionDTO> findAll();
	
	/**
	 * Find filtered ClasificacionEmbarcacionDTO entities.
	 * 
	 * @return List<ClasificacionEmbarcacionDTO> filtered ClasificacionEmbarcacionDTO entities
	 */
	public List<ClasificacionEmbarcacionDTO> listarFiltrado(ClasificacionEmbarcacionDTO clasificacionEmbarcacionDTO);
}