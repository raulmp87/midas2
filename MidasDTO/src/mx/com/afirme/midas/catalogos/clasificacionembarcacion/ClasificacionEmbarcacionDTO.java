package mx.com.afirme.midas.catalogos.clasificacionembarcacion;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.base.CacheableDTO;

/**
 * ClasificacionEmbarcacionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCCLASIFICACIONEMBARCACION", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = "CODIGOCLASIFICACIONEMBARCACION"))
public class ClasificacionEmbarcacionDTO extends CacheableDTO implements java.io.Serializable {
	private static final long serialVersionUID = -2593021066860551746L;
	// Fields

	private BigDecimal idTcClasificacionEmbarcacion;
	private BigDecimal codigoClasificacionEmbarcacion;
	private String descripcionClasifEmbarcacion;

	// Constructors

	/** default constructor */
	public ClasificacionEmbarcacionDTO() {
	}

	/** full constructor */
	public ClasificacionEmbarcacionDTO(BigDecimal idTcClasificacionEmbarcacion,
			BigDecimal codigoClasificacionEmbarcacion,
			String descripcionClasifEmbarcacion) {
		this.idTcClasificacionEmbarcacion = idTcClasificacionEmbarcacion;
		this.codigoClasificacionEmbarcacion = codigoClasificacionEmbarcacion;
		this.descripcionClasifEmbarcacion = descripcionClasifEmbarcacion;
	}

	// Property accessors
	@Id
	@Column(name = "IDTCCLASIFICACIONEMBARCACION", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcClasificacionEmbarcacion() {
		return this.idTcClasificacionEmbarcacion;
	}

	public void setIdTcClasificacionEmbarcacion(
			BigDecimal idTcClasificacionEmbarcacion) {
		this.idTcClasificacionEmbarcacion = idTcClasificacionEmbarcacion;
	}

	@Column(name = "CODIGOCLASIFICACIONEMBARCACION", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoClasificacionEmbarcacion() {
		return this.codigoClasificacionEmbarcacion;
	}

	public void setCodigoClasificacionEmbarcacion(
			BigDecimal codigoClasificacionEmbarcacion) {
		this.codigoClasificacionEmbarcacion = codigoClasificacionEmbarcacion;
	}

	@Column(name = "DESCRIPCIONCLASIFEMBARCACION", nullable = false, length = 200)
	public String getDescripcionClasifEmbarcacion() {
		return this.descripcionClasifEmbarcacion;
	}

	public void setDescripcionClasifEmbarcacion(
			String descripcionClasifEmbarcacion) {
		this.descripcionClasifEmbarcacion = descripcionClasifEmbarcacion;
	}
	
	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof ClasificacionEmbarcacionDTO) {
			ClasificacionEmbarcacionDTO clasificacionEmbarcacionDTO = (ClasificacionEmbarcacionDTO) object;
			equal = clasificacionEmbarcacionDTO.getIdTcClasificacionEmbarcacion().equals(this.getIdTcClasificacionEmbarcacion());
		} // End of if
		return equal;
	}
	
	@Override
	public String getDescription() {
		return this.descripcionClasifEmbarcacion;
	}
	
	@Override
	public Object getId() {
		return this.idTcClasificacionEmbarcacion;
	}

}