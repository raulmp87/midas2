package mx.com.afirme.midas.catalogos.descuento;


import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity(name = "DescuentoDTO")
@Table(name = "TODESCUENTOVARIO", schema = "MIDAS")
public class DescuentoDTO implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8543965022075635714L;
	private BigDecimal idToDescuentoVario;
	private Short claveTipo;
	private String descripcion;

	// Constructors

	/** default constructor */
	public DescuentoDTO() {
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTODESCUENTOVARIO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTODESCUENTOVARIO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTODESCUENTOVARIO_SEQ_GENERADOR")
	@Column(name = "IDTODESCUENTOVARIO", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToDescuentoVario() {
		return this.idToDescuentoVario;
	}

	public void setIdToDescuentoVario(BigDecimal idToDescuentoVario) {
		this.idToDescuentoVario = idToDescuentoVario;
	}

	@Column(name = "CLAVETIPODESCUENTO", nullable = false, length = 4)
	public Short getClaveTipo() {
		return this.claveTipo;
	}

	public void setClaveTipo(Short claveTipo) {
		this.claveTipo = claveTipo;
	}

	@Column(name = "DESCRIPCIONDESCUENTO", length = 200)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}