package mx.com.afirme.midas.catalogos.descuento;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.catalogos.descuento.DescuentoDTO;

/**
 * Remote interface for DescuentoFacade.
 * @author MyEclipse Persistence Tools
 */


public interface DescuentoFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved DescuentoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DescuentoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DescuentoDTO entity);
    /**
	 Delete a persistent DescuentoDTO entity.
	  @param entity DescuentoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DescuentoDTO entity);
   /**
	 Persist a previously saved DescuentoDTO entity and return it or a copy of it to the sender. 
	 A copy of the DescuentoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DescuentoDTO entity to update
	 @return DescuentoDTO the persisted DescuentoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public DescuentoDTO update(DescuentoDTO entity);
	public DescuentoDTO findById( BigDecimal id);
	 /**
	 * Find all DescuentoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DescuentoDTO property to query
	  @param value the property value to match
	  	  @return List<DescuentoDTO> found by query
	 */
	public List<DescuentoDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all DescuentoDTO entities.
	  	  @return List<DescuentoDTO> all DescuentoDTO entities
	 */
	public List<DescuentoDTO> findAll();

	public List<DescuentoDTO> listarFiltrado(DescuentoDTO descuentoDTO);

	public List<DescuentoDTO> listarDescuentosPorAsociar(BigDecimal idProducto);
	
	public List<DescuentoDTO> listarDescuentosPorAsociarTipoPoliza(BigDecimal idToTipoPoliza);

	public List<DescuentoDTO> listarDescuentosPorAsociarCobertura(BigDecimal idToCobertura);

	public List<DescuentoDTO> listarDescuentosEspeciales();
}