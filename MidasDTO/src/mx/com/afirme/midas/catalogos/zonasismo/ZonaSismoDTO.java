package mx.com.afirme.midas.catalogos.zonasismo;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.catalogos.codigopostalzonasismo.CodigoPostalZonaSismoDTO;

/**
 * ZonaSismoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name = "ZonaSismoDTO")
@Table(name = "TCZONASISMO", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = "CODIGOZONASISMO"))
public class ZonaSismoDTO extends CacheableDTO {

	private static final long serialVersionUID = -7652276030748993887L;
	
	private BigDecimal idZonaSismo;
	private String descripcionZonaSismo;
	private String codigoZonaSismo;
	private BigDecimal valorDefaultCoaseguro;
	private BigDecimal valorDefaultCoaseguroROCH;
	private BigDecimal valorDefaultDeducible;
	private BigDecimal valorDefaultDeducibleROCH;
	
	
	private List<CodigoPostalZonaSismoDTO> codigoPostalZonaSismoDTOs;

	// Constructors
	public ZonaSismoDTO() {
		if (codigoPostalZonaSismoDTOs == null)
			codigoPostalZonaSismoDTOs = new ArrayList<CodigoPostalZonaSismoDTO>();
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTCZONASISMO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCZONASISMO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCZONASISMO_SEQ_GENERADOR")
	@Column(name = "IDTCZONASISMO", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdZonaSismo() {
		return this.idZonaSismo;
	}

	public void setIdZonaSismo(BigDecimal idZonaSismo) {
		this.idZonaSismo = idZonaSismo;
	}

	@Column(name = "DESCRIPCIONZONASISMO", nullable = false, length = 200)
	public String getDescripcionZonaSismo() {
		return this.descripcionZonaSismo;
	}

	public void setDescripcionZonaSismo(String descripcionZonaSismo) {
		this.descripcionZonaSismo = descripcionZonaSismo;
	}

	@Column(name = "CODIGOZONASISMO")
	public String getCodigoZonaSismo() {
		return this.codigoZonaSismo;
	}

	public void setCodigoZonaSismo(String codigoZonaSismo) {
		this.codigoZonaSismo = codigoZonaSismo;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "zonaSismoDTO")
	public List<CodigoPostalZonaSismoDTO> getCodigoPostalZonaSismos() {
		return this.codigoPostalZonaSismoDTOs;
	}

	public void setCodigoPostalZonaSismos(
			List<CodigoPostalZonaSismoDTO> codigoPostalZonaSismoDTOs) {
		this.codigoPostalZonaSismoDTOs = codigoPostalZonaSismoDTOs;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof ZonaSismoDTO) {
			ZonaSismoDTO zonaSismoDTO = (ZonaSismoDTO) object;
			equal = zonaSismoDTO.getIdZonaSismo().equals(this.getIdZonaSismo());
		} // End of if
		return equal;
	}
	@Column(name = "VALORDEFAULTCOASEGURO", precision = 16, scale = 4)
	public BigDecimal getValorDefaultCoaseguro() {
		return this.valorDefaultCoaseguro;
	}

	public void setValorDefaultCoaseguro(BigDecimal valorDefaultCoaseguro) {
		this.valorDefaultCoaseguro = valorDefaultCoaseguro;
	}

	@Column(name = "VALORDEFAULTCOASEGUROROCH", precision = 16, scale = 4)
	public BigDecimal getValorDefaultCoaseguroROCH() {
		return this.valorDefaultCoaseguroROCH;
	}

	public void setValorDefaultCoaseguroROCH(
			BigDecimal valorDefaultCoaseguroROCH) {
		this.valorDefaultCoaseguroROCH = valorDefaultCoaseguroROCH;
	}

	@Column(name = "VALORDEFAULTDEDUCIBLE", precision = 16, scale = 4)
	public BigDecimal getValorDefaultDeducible() {
		return this.valorDefaultDeducible;
	}

	public void setValorDefaultDeducible(BigDecimal valorDefaultDeducible) {
		this.valorDefaultDeducible = valorDefaultDeducible;
	}

	@Column(name = "VALORDEFAULTDEDUCIBLEROCH", precision = 16, scale = 4)
	public BigDecimal getValorDefaultDeducibleROCH() {
		return this.valorDefaultDeducibleROCH;
	}

	public void setValorDefaultDeducibleROCH(
			BigDecimal valorDefaultDeducibleROCH) {
		this.valorDefaultDeducibleROCH = valorDefaultDeducibleROCH;
	}	

	@Override
	public String getDescription() {
		return this.descripcionZonaSismo;
	}

	@Override
	public Object getId() {
		return this.idZonaSismo;
	}	
}