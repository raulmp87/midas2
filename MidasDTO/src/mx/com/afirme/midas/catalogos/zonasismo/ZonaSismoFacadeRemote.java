package mx.com.afirme.midas.catalogos.zonasismo;
// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for ZonaSismoFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ZonaSismoFacadeRemote extends MidasInterfaceBase<ZonaSismoDTO>{
	/**
	 * Perform an initial save of a previously unsaved ZonaSismoDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            ZonaSismoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ZonaSismoDTO entity);

	/**
	 * Delete a persistent ZonaSismoDTO entity.
	 * 
	 * @param entity
	 *            ZonaSismoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ZonaSismoDTO entity);

	/**
	 * Persist a previously saved ZonaSismoDTO entity and return it or a copy of it
	 * to the sender. A copy of the ZonaSismoDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            ZonaSismoDTO entity to update
	 * @return ZonaSismoDTO the persisted ZonaSismoDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ZonaSismoDTO update(ZonaSismoDTO entity);

	/**
	 * Find all ZonaSismoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ZonaSismoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<ZonaSismoDTO> found by query
	 */
	public List<ZonaSismoDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all ZonaSismoDTO entities.
	 * 
	 * @return List<ZonaSismoDTO> all ZonaSismoDTO entities
	 */
	public List<ZonaSismoDTO> findAll();
	
	public List<ZonaSismoDTO> listarFiltrado(ZonaSismoDTO zonaSismoDTO);
}