package mx.com.afirme.midas.catalogos.aumentovario;
// default package

import java.math.BigDecimal;
import java.util.List;

/**
 * Remote interface for AumentoVarioFacade.
 * @author MyEclipse Persistence Tools
 */


public interface AumentoVarioFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved AumentoVarioDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity AumentoVarioDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(AumentoVarioDTO entity);
    /**
	 Delete a persistent AumentoVarioDTO entity.
	  @param entity AumentoVarioDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(AumentoVarioDTO entity);
   /**
	 Persist a previously saved AumentoVarioDTO entity and return it or a copy of it to the sender. 
	 A copy of the AumentoVarioDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity AumentoVarioDTO entity to update
	 @return AumentoVarioDTO the persisted AumentoVarioDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public AumentoVarioDTO update(AumentoVarioDTO entity);
	public AumentoVarioDTO findById( BigDecimal id);
	 /**
	 * Find all AumentoVarioDTO entities with a specific property value.  
	 
	  @param propertyName the name of the AumentoVarioDTO property to query
	  @param value the property value to match
	  	  @return List<AumentoVarioDTO> found by query
	 */
	public List<AumentoVarioDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all AumentoVarioDTO entities.
	  	  @return List<AumentoVarioDTO> all AumentoVarioDTO entities
	 */
	public List<AumentoVarioDTO> findAll(
		);	
	
	public List<AumentoVarioDTO> listarFiltrado(AumentoVarioDTO aumentoVarioDTO);

	public List<AumentoVarioDTO> listarAumentosPorAsociar(BigDecimal idProducto);

	public List<AumentoVarioDTO> listarAumentosPorAsociarTipoPoliza(BigDecimal idToTipoPoliza);

	public List<AumentoVarioDTO> listarAumentosPorAsociarCobetura(BigDecimal idToCobertura);
}