package mx.com.afirme.midas.catalogos.aumentovario;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.producto.configuracion.aumento.AumentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.aumento.exclusion.ExclusionAumentoVarioProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento.AumentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.aumento.exclusion.ExclusionAumentoVarioTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.AumentoVarioCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.aumento.exclusion.ExclusionAumentoVarioCoberturaDTO;


/**
 * AumentoVarioDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOAUMENTOVARIO"
    ,schema="MIDAS"
)

public class AumentoVarioDTO  implements java.io.Serializable {



     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idAumentoVario;
    private String descripcionAumento;
    private Short claveTipoAumento;
    private List<AumentoVarioTipoPolizaDTO> aumentoVarioTipoPolizaDTOs = new ArrayList<AumentoVarioTipoPolizaDTO>();
    private List<AumentoVarioProductoDTO> aumentoVarioProductoDTOs = new ArrayList<AumentoVarioProductoDTO>();
    private List<ExclusionAumentoVarioCoberturaDTO> exclusionAumentoVarioCoberturaDTOs = new ArrayList<ExclusionAumentoVarioCoberturaDTO>();
    private List<ExclusionAumentoVarioProductoDTO> exclusionAumentoVarioProductoDTOs = new ArrayList<ExclusionAumentoVarioProductoDTO>();
    private List<AumentoVarioCoberturaDTO> aumentoVarioCoberturaDTOs = new ArrayList<AumentoVarioCoberturaDTO>();
    private List<ExclusionAumentoVarioTipoPolizaDTO> exclusionAumentoVarioTipoPolizaDTOs = new ArrayList<ExclusionAumentoVarioTipoPolizaDTO>();



    /** default constructor */
    public AumentoVarioDTO() {
    }


    @Id 
    @SequenceGenerator(name = "IDTOAUMENTOVARIO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOAUMENTOVARIO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOAUMENTOVARIO_SEQ_GENERADOR")
    @Column(name="IDTOAUMENTOVARIO", unique=true, nullable=false, precision=22, scale=0)
	public BigDecimal getIdAumentoVario() {
		return idAumentoVario;
	}



	public void setIdAumentoVario(BigDecimal idAumentoVario) {
		this.idAumentoVario = idAumentoVario;
	}


	@Column(name="DESCRIPCIONAUMENTO", nullable=false, length=200)
	public String getDescripcionAumento() {
		return descripcionAumento;
	}



	public void setDescripcionAumento(String descripcionAumento) {
		this.descripcionAumento = descripcionAumento;
	}


	@Column(name="CLAVETIPOAUMENTO", nullable=false, precision=4, scale=0)
	public Short getClaveTipoAumento() {
		return claveTipoAumento;
	}



	public void setClaveTipoAumento(Short claveTipoAumento) {
		this.claveTipoAumento = claveTipoAumento;
	}


	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="aumentoVarioDTO")
	public List<AumentoVarioTipoPolizaDTO> getAumentoVarioTipoPolizaDTOs() {
		return aumentoVarioTipoPolizaDTOs;
	}



	public void setAumentoVarioTipoPolizaDTOs(
			List<AumentoVarioTipoPolizaDTO> aumentoVarioTipoPolizaDTOs) {
		this.aumentoVarioTipoPolizaDTOs = aumentoVarioTipoPolizaDTOs;
	}


	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="aumentoVarioDTO")
	public List<AumentoVarioProductoDTO> getAumentoVarioProductoDTOs() {
		return aumentoVarioProductoDTOs;
	}



	public void setAumentoVarioProductoDTOs(
			List<AumentoVarioProductoDTO> aumentoVarioProductoDTOs) {
		this.aumentoVarioProductoDTOs = aumentoVarioProductoDTOs;
	}


	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="aumentoVarioDTO")
	public List<ExclusionAumentoVarioCoberturaDTO> getExclusionAumentoVarioCoberturaDTOs() {
		return exclusionAumentoVarioCoberturaDTOs;
	}



	public void setExclusionAumentoVarioCoberturaDTOs(
			List<ExclusionAumentoVarioCoberturaDTO> exclusionAumentoVarioCoberturaDTOs) {
		this.exclusionAumentoVarioCoberturaDTOs = exclusionAumentoVarioCoberturaDTOs;
	}


	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="aumentoVarioDTO")
	public List<ExclusionAumentoVarioProductoDTO> getExclusionAumentoVarioProductoDTOs() {
		return exclusionAumentoVarioProductoDTOs;
	}



	public void setExclusionAumentoVarioProductoDTOs(
			List<ExclusionAumentoVarioProductoDTO> exclusionAumentoVarioProductoDTOs) {
		this.exclusionAumentoVarioProductoDTOs = exclusionAumentoVarioProductoDTOs;
	}


	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="aumentoVarioDTO")
	public List<AumentoVarioCoberturaDTO> getAumentoVarioCoberturaDTOs() {
		return aumentoVarioCoberturaDTOs;
	}



	public void setAumentoVarioCoberturaDTOs(
			List<AumentoVarioCoberturaDTO> aumentoVarioCoberturaDTOs) {
		this.aumentoVarioCoberturaDTOs = aumentoVarioCoberturaDTOs;
	}


	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="aumentoVarioDTO")
	public List<ExclusionAumentoVarioTipoPolizaDTO> getExclusionAumentoVarioTipoPolizaDTOs() {
		return exclusionAumentoVarioTipoPolizaDTOs;
	}



	public void setExclusionAumentoVarioTipoPolizaDTOs(
			List<ExclusionAumentoVarioTipoPolizaDTO> exclusionAumentoVarioTipoPolizaDTOs) {
		this.exclusionAumentoVarioTipoPolizaDTOs = exclusionAumentoVarioTipoPolizaDTOs;
	}

   
}