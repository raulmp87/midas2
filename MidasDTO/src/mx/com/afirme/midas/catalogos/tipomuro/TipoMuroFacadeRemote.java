package mx.com.afirme.midas.catalogos.tipomuro;
// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for TipoMuroFacade.
 * @author MyEclipse Persistence Tools
 */


public interface TipoMuroFacadeRemote extends MidasInterfaceBase<TipoMuroDTO> {
		/**
	 Perform an initial save of a previously unsaved TipoMuroDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity TipoMuroDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(TipoMuroDTO entity);
    /**
	 Delete a persistent TipoMuroDTO entity.
	  @param entity TipoMuroDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(TipoMuroDTO entity);
   /**
	 Persist a previously saved TipoMuroDTO entity and return it or a copy of it to the sender. 
	 A copy of the TipoMuroDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity TipoMuroDTO entity to update
	 @return TipoMuroDTO the persisted TipoMuroDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public TipoMuroDTO update(TipoMuroDTO entity);
	
	 /**
	 * Find all TipoMuroDTO entities with a specific property value.  
	 
	  @param propertyName the name of the TipoMuroDTO property to query
	  @param value the property value to match
	  	  @return List<TipoMuroDTO> found by query
	 */
	public List<TipoMuroDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all TipoMuroDTO entities.
	  	  @return List<TipoMuroDTO> all TipoMuroDTO entities
	 */
	public List<TipoMuroDTO> findAll(
		);	
	
	public List<TipoMuroDTO> listarFiltrado(TipoMuroDTO tipoMuroDTO);
}