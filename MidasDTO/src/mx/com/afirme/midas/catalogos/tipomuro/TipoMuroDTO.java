package mx.com.afirme.midas.catalogos.tipomuro;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.base.PermitidoCotizarCasa;


/**
 * TipoMuroDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCTIPOMURO"
    ,schema="MIDAS"
, uniqueConstraints = @UniqueConstraint(columnNames="IDTCTIPOMURO")
)
@Cache(
  type=CacheType.SOFT,
  size=1000,
  expiry=36000000
)
public class TipoMuroDTO  extends CacheableDTO implements PermitidoCotizarCasa {


    /**
	 * 
	 */
	private static final long serialVersionUID = -2593021066160550846L;
	// Fields    

     private BigDecimal idTipoMuro;
     private BigDecimal codigoTipoMuro;
     private String descripcionTipoMuro;
     private Boolean permitidoCasa;

    // Constructors

    /** default constructor */
    public TipoMuroDTO() {
    }

    // Property accessors
    @Id 
    @SequenceGenerator(name = "IDTCTIPOMURO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCTIPOMURO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCTIPOMURO_SEQ_GENERADOR")
    @Column(name="IDTCTIPOMURO", unique=true, nullable=false, precision=22, scale=0)
	public BigDecimal getIdTipoMuro() {
		return idTipoMuro;
	}


	public void setIdTipoMuro(BigDecimal idTipoMuro) {
		this.idTipoMuro = idTipoMuro;
	}

	@Column(name="CODIGOTIPOMURO", unique=true, nullable=false, precision=22, scale=0)
	public BigDecimal getCodigoTipoMuro() {
		return codigoTipoMuro;
	}


	public void setCodigoTipoMuro(BigDecimal codigoTipoMuro) {
		this.codigoTipoMuro = codigoTipoMuro;
	}

	@Column(name="DESCRIPCIONTIPOMURO", nullable=false, length=200)
	public String getDescripcionTipoMuro() {
		return descripcionTipoMuro;
	}


	public void setDescripcionTipoMuro(String descripcionTipoMuro) {
		this.descripcionTipoMuro = descripcionTipoMuro;
	}

	@Override
	public String getDescription() {
		return this.descripcionTipoMuro;
	}

	@Override
	public Object getId() {
		return this.idTipoMuro;
	}

	@Column
	public Boolean getPermitidoCasa() {
		return permitidoCasa;
	}

	public void setPermitidoCasa(Boolean permitidoCasa) {
		this.permitidoCasa = permitidoCasa;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof TipoMuroDTO) {
			TipoMuroDTO dto = (TipoMuroDTO) object;
			equal = dto.getIdTipoMuro().equals(this.idTipoMuro);
		} // End of if
		return equal;
	}
	
}