package mx.com.afirme.midas.catalogos.reaseguradorcorredor;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for TcreaseguradorcorredorFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ReaseguradorCorredorFacadeRemote extends MidasInterfaceBase<ReaseguradorCorredorDTO> {
	/**
	 * Perform an initial save of a previously unsaved Tcreaseguradorcorredor
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            Tcreaseguradorcorredor entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ReaseguradorCorredorDTO entity);

	/**
	 * Delete a persistent Tcreaseguradorcorredor entity.
	 * 
	 * @param entity
	 *            Tcreaseguradorcorredor entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ReaseguradorCorredorDTO entity);

	/**
	 * Persist a previously saved Tcreaseguradorcorredor entity and return it or
	 * a copy of it to the sender. A copy of the Tcreaseguradorcorredor entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            Tcreaseguradorcorredor entity to update
	 * @return Tcreaseguradorcorredor the persisted Tcreaseguradorcorredor
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ReaseguradorCorredorDTO update(ReaseguradorCorredorDTO entity);

	/**
	 * Find all Tcreaseguradorcorredor entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the Tcreaseguradorcorredor property to query
	 * @param value
	 *            the property value to match
	 * @return List<Tcreaseguradorcorredor> found by query
	 */
	public List<ReaseguradorCorredorDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all Tcreaseguradorcorredor entities.
	 * 
	 * @return List<Tcreaseguradorcorredor> all Tcreaseguradorcorredor entities
	 */
	public List<ReaseguradorCorredorDTO> findAll();
	
	
	public List<ReaseguradorCorredorDTO> listarFiltrado(ReaseguradorCorredorDTO reaseguradorCorredorDTO);
}