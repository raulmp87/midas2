package mx.com.afirme.midas.catalogos.impuestoresidenciafiscal;
// default package

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;


/**
 * ImpuestoResidenciaFiscalDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCIMPUESTORESIDENCIAFISCAL"
    ,schema="MIDAS"
)

public class ImpuestoResidenciaFiscalDTO  implements java.io.Serializable {    
	private static final long serialVersionUID = 1757539260572564300L;

	// Fields    
     private Long idTcImpuestoResidenciaFiscal;
     private String descripcion;
     private Double porcentaje;
     
     private Set<ReaseguradorCorredorDTO> reaseguradores = new HashSet<ReaseguradorCorredorDTO>(0);


    // Constructors

    /** default constructor */
    public ImpuestoResidenciaFiscalDTO() {
    }

	/** minimal constructor */
    public ImpuestoResidenciaFiscalDTO(Long idTcImpuestoResidenciaFiscal, String descripcion, Double porcentaje) {
        this.idTcImpuestoResidenciaFiscal = idTcImpuestoResidenciaFiscal;
        this.descripcion = descripcion;
        this.porcentaje = porcentaje;
    }
    
    /** full constructor */
    public ImpuestoResidenciaFiscalDTO(Long idTcImpuestoResidenciaFiscal, String descripcion, Double porcentaje, Set<ReaseguradorCorredorDTO> reaseguradores) {
        this.idTcImpuestoResidenciaFiscal = idTcImpuestoResidenciaFiscal;
        this.descripcion = descripcion;
        this.porcentaje = porcentaje;
        this.reaseguradores = reaseguradores;
    }

   
    // Property accessors
    @Id     
    @SequenceGenerator(name = "IDTCIMPUESTORESIDFISCAL_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCIMPUESTORESIDFISCAL_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCIMPUESTORESIDFISCAL_SEQ_GENERADOR")
    @Column(name="IDTCIMPUESTORESIDENCIAFISCAL", unique=true, nullable=false, precision=10, scale=0)
    public Long getIdTcImpuestoResidenciaFiscal() {
        return this.idTcImpuestoResidenciaFiscal;
    }
    
    public void setIdTcImpuestoResidenciaFiscal(Long idTcImpuestoResidenciaFiscal) {
        this.idTcImpuestoResidenciaFiscal = idTcImpuestoResidenciaFiscal;
    }
    
    @Column(name="DESCRIPCION", nullable=false, length=50)

    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    @Column(name="PORCENTAJE", nullable=false, precision=8, scale=4)

    public Double getPorcentaje() {
        return this.porcentaje;
    }
    
    public void setPorcentaje(Double porcentaje) {
        this.porcentaje = porcentaje;
    }
    
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="impuestoResidenciaFiscal")																		
    public Set<ReaseguradorCorredorDTO> getReaseguradores() {
        return this.reaseguradores;
    }
    
    public void setReaseguradores(Set<ReaseguradorCorredorDTO> reaseguradores) {
        this.reaseguradores = reaseguradores;
    }
    

}