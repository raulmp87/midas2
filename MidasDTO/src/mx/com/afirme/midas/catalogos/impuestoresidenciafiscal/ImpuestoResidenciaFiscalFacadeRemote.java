package mx.com.afirme.midas.catalogos.impuestoresidenciafiscal;
// default package

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for ImpuestoResidenciaFiscalFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ImpuestoResidenciaFiscalFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ImpuestoResidenciaFiscalDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ImpuestoResidenciaFiscalDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ImpuestoResidenciaFiscalDTO entity);
    /**
	 Delete a persistent ImpuestoResidenciaFiscalDTO entity.
	  @param entity ImpuestoResidenciaFiscalDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ImpuestoResidenciaFiscalDTO entity);
   /**
	 Persist a previously saved ImpuestoResidenciaFiscalDTO entity and return it or a copy of it to the sender. 
	 A copy of the ImpuestoResidenciaFiscalDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ImpuestoResidenciaFiscalDTO entity to update
	 @return ImpuestoResidenciaFiscalDTO the persisted ImpuestoResidenciaFiscalDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ImpuestoResidenciaFiscalDTO update(ImpuestoResidenciaFiscalDTO entity);
	public ImpuestoResidenciaFiscalDTO findById( Long id);
	 /**
	 * Find all ImpuestoResidenciaFiscalDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ImpuestoResidenciaFiscalDTO property to query
	  @param value the property value to match
	  	  @return List<ImpuestoResidenciaFiscalDTO> found by query
	 */
	public List<ImpuestoResidenciaFiscalDTO> findByProperty(String propertyName, Object value
		);
	public List<ImpuestoResidenciaFiscalDTO> findByDescripcion(Object descripcion
		);
	public List<ImpuestoResidenciaFiscalDTO> findByPorcentaje(Object porcentaje
		);
	/**
	 * Find all ImpuestoResidenciaFiscalDTO entities.
	  	  @return List<ImpuestoResidenciaFiscalDTO> all ImpuestoResidenciaFiscalDTO entities
	 */
	public List<ImpuestoResidenciaFiscalDTO> findAll(
		);	
}