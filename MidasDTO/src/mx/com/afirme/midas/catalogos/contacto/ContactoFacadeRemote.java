package mx.com.afirme.midas.catalogos.contacto;
// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for ContactoFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ContactoFacadeRemote extends MidasInterfaceBase<ContactoDTO> {
		/**
	 Perform an initial save of a previously unsaved ContactoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ContactoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ContactoDTO entity);
    /**
	 Delete a persistent ContactoDTO entity.
	  @param entity ContactoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ContactoDTO entity);
   /**
	 Persist a previously saved ContactoDTO entity and return it or a copy of it to the sender. 
	 A copy of the ContactoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ContactoDTO entity to update
	 @return ContactoDTO the persisted ContactoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ContactoDTO update(ContactoDTO entity);

	 /**
	 * Find all ContactoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ContactoDTO property to query
	  @param value the property value to match
	  	  @return List<ContactoDTO> found by query
	 */
	public List<ContactoDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ContactoDTO entities.
	  	  @return List<ContactoDTO> all ContactoDTO entities
	 */
	public List<ContactoDTO> findAll(
		);
	
	/**
	 * Find filtered ContactoDTO entities.
	  	  @return List<ContactoDTO> filtered ContactoDTO entities
	 */
	public List<ContactoDTO> listarFiltrado(ContactoDTO contactoDTO);
}