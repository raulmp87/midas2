package mx.com.afirme.midas.catalogos.contacto;
// default package

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDTO;

@Entity
@Table(name="TCCONTACTO", schema="MIDAS")

public class ContactoDTO extends CacheableDTO implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 7052386591022420642L;
	private BigDecimal idtccontacto;
     private ReaseguradorCorredorDTO reaseguradorCorredor;
     private String nombre;
     private BigDecimal telefono;
     private Set<ParticipacionDTO> participacionDTOs = new HashSet<ParticipacionDTO>(0);


    // Constructors

    /** default constructor */
    public ContactoDTO() {
    }

    // Property accessors
    @Id
    @SequenceGenerator(name = "IDTCCONTACTO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCCONTACTO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCCONTACTO_SEQ_GENERADOR")
    @Column(name="IDTCCONTACTO", unique=true, nullable=false, precision=22, scale=0)
    public BigDecimal getIdtccontacto() {
        return this.idtccontacto;
    }
    
    public void setIdtccontacto(BigDecimal idtccontacto) {
        this.idtccontacto = idtccontacto;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTCREASEGURADORCORREDOR", nullable=false)

    public ReaseguradorCorredorDTO getReaseguradorCorredor() {
        return this.reaseguradorCorredor;
    }
    
    public void setReaseguradorCorredor(ReaseguradorCorredorDTO reaseguradorCorredor) {
        this.reaseguradorCorredor = reaseguradorCorredor;
    }
    
    @Column(name="NOMBRE", nullable=false, length=250)

    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    @Column(name="TELEFONO", nullable=false, precision=22, scale=0)

    public BigDecimal getTelefono() {
        return this.telefono;
    }
    
    public void setTelefono(BigDecimal telefono) {
        this.telefono = telefono;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="contacto")

    public Set<ParticipacionDTO> getParticipacions() {
        return this.participacionDTOs;
    }
    
    public void setParticipacions(Set<ParticipacionDTO> participacionDTOs) {
        this.participacionDTOs = participacionDTOs;
    }
    
    @Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof ContactoDTO) {
			ContactoDTO contactoDTO = (ContactoDTO) object;
			equal = contactoDTO.getIdtccontacto().equals(this.getIdtccontacto());
		} // End of if
		return equal;
	}
	
	@Override
	public String getDescription() {
		return this.getNombre();
	}
	
	@Override
	public Object getId() {
		return this.getIdtccontacto();
	}
}