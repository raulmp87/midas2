package mx.com.afirme.midas.catalogos.ajustador;
// default package

import java.util.List;

import mx.com.afirme.midas.base.MidasInterfaceBase;


/**
 * Remote interface for AjustadorFacade.
 * @author MyEclipse Persistence Tools
 */

public interface AjustadorFacadeRemote extends MidasInterfaceBase<AjustadorDTO> {
		/**
	 Perform an initial save of a previously unsaved AjustadorDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity AjustadorDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(AjustadorDTO entity);
    /**
	 Delete a persistent AjustadorDTO entity.
	  @param entity AjustadorDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(AjustadorDTO entity);
   /**
	 Persist a previously saved AjustadorDTO entity and return it or a copy of it to the sender. 
	 A copy of the AjustadorDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity AjustadorDTO entity to update
	 @return AjustadorDTO the persisted AjustadorDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public AjustadorDTO update(AjustadorDTO entity);

	 /**
	 * Find all AjustadorDTO entities with a specific property value.  
	 
	  @param propertyName the name of the AjustadorDTO property to query
	  @param value the property value to match
	  	  @return List<AjustadorDTO> found by query
	 */
	public List<AjustadorDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all AjustadorDTO entities.
	  	  @return List<AjustadorDTO> all AjustadorDTO entities
	 */
	public List<AjustadorDTO> findAll(
		);	
	
	public List<AjustadorDTO> listarFiltrado(AjustadorDTO ajustadorDTO);	
	
	public List<AjustadorDTO> listarPorOrdenAsignacion();
	
	public List<AjustadorDTO> ajustadoresPorEstatus(Short estatus);
}