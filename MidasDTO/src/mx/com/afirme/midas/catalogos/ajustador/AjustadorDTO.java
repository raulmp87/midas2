package mx.com.afirme.midas.catalogos.ajustador;
// default package



import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;



/**
 * AjustadorDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name = "AjustadorDTO")
@Table(name="TCAJUSTADOR"
    ,schema="MIDAS"
)
public class AjustadorDTO extends CacheableDTO {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idTcAjustador;
     private String razonSocial;
     private String rfc;
     private String nombre;
     private String calleYNumero;
     private Integer codigoPostal;
     private String estado;
     private String ciudad;
     private String colonia;
     private String telefono;
     private String telefonoCelular;
     private String radiolocalizador;
     private String ramo;
     private String tipoDeContribuyente;
     private String tipoDeProveedor;
     private String municipio;
     private String email;
	 private String usuarioNombre;
     private Date fechaModificacion;
     private Long asignaciones;
     private Integer distancia; // No persistida
     private Set<ReporteSiniestroDTO> reporteSiniestroDTOs = new HashSet<ReporteSiniestroDTO>(0);
     private Short estatus;
     public static final Short ACTIVO = 1;
     public static final Short INACTIVO = 0;
    // Constructors

    /** default constructor */
    public AjustadorDTO() {
    }


   
    // Property accessors
    @Id 
	@SequenceGenerator(name = "IDTCAJUSTADOR_SEQ_GENERADOR",allocationSize = 1, sequenceName = "MIDAS.IDTCAJUSTADOR_SEQ")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "IDTCAJUSTADOR_SEQ_GENERADOR")
    @Column(name="IDTCAJUSTADOR", unique=true, nullable=false, precision=22, scale=0)
    public BigDecimal getIdTcAjustador() {
        return this.idTcAjustador;
    }
    
    public void setIdTcAjustador(BigDecimal idTcAjustador) {
        this.idTcAjustador = idTcAjustador;
    }
    
    @Column(name="RAZONSOCIAL", nullable=false, length=200)

    public String getRazonSocial() {
        return this.razonSocial;
    }
    
    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }
    
    @Column(name="RFC", nullable=false, length=20)

    public String getRfc() {
        return this.rfc;
    }
    
    public void setRfc(String rfc) {
        this.rfc = rfc;
    }
    
    @Column(name="NOMBRE", nullable=false, length=150)

    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    @Column(name="CALLEYNUMERO", nullable=false, length=100)

    public String getCalleYNumero() {
        return this.calleYNumero;
    }
    
    public void setCalleYNumero(String calle) {
        this.calleYNumero = calle;
    }
    
    @Column(name="CODIGOPOSTAL", nullable=false, precision=6, scale=0)

    public Integer getCodigoPostal() {
        return this.codigoPostal;
    }
    
    public void setCodigoPostal(Integer codigoPostal) {
        this.codigoPostal = codigoPostal;
    }
    
    @Column(name="ESTADO", nullable=false, length=100)

    public String getEstado() {
        return this.estado;
    }
    
    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    @Column(name="CIUDAD", nullable=false, length=100)

    public String getCiudad() {
        return this.ciudad;
    }
    
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }
    
    @Column(name="COLONIA", length=150)

    public String getColonia() {
        return this.colonia;
    }
    
    public void setColonia(String colonia) {
        this.colonia = colonia;
    }
    
    @Column(name="TELEFONO", nullable=false, length=10)

    public String getTelefono() {
        return this.telefono;
    }
    
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    
    @Column(name="TELEFONOCELULAR", nullable=false, length=20)

    public String getTelefonoCelular() {
        return this.telefonoCelular;
    }
    
    public void setTelefonoCelular(String telefonoCelular) {
        this.telefonoCelular = telefonoCelular;
    }
    
    @Column(name="RADIOLOCALIZADOR", nullable=false, length=20)

    public String getRadiolocalizador() {
        return this.radiolocalizador;
    }
    
    public void setRadiolocalizador(String radiolocalizador) {
        this.radiolocalizador = radiolocalizador;
    }
    
    @Column(name="RAMO", length=150)

    public String getRamo() {
        return this.ramo;
    }
    
    public void setRamo(String ramo) {
        this.ramo = ramo;
    }
    
    @Column(name="TIPODECONTRIBUYENTE", nullable=false, length=50)

    public String getTipoDeContribuyente() {
        return this.tipoDeContribuyente;
    }
    
    public void setTipoDeContribuyente(String tipoContribuyente) {
        this.tipoDeContribuyente = tipoContribuyente;
    }
    
    @Column(name="TIPODEPROVEEDOR", nullable=false, length=100)

    public String getTipoDeProveedor() {
        return this.tipoDeProveedor;
    }
    
    public void setTipoDeProveedor(String tipoProveedor) {
        this.tipoDeProveedor = tipoProveedor;
    }
    
    @Column(name="MUNICIPIO", length=100)

    public String getMunicipio() {
        return this.municipio;
    }
    
    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }
    
    @Column(name="EMAIL", length=240)

    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
/**
	 * @return the usuarioNombre
	 */
    @Column(name="USUARIONOMBRE", length=50)
    public String getUsuarioNombre() {
		return usuarioNombre;
	}

	/**
	 * @param usuarioNombre the usuarioNombre to set
	 */
	public void setUsuarioNombre(String usuarioNombre) {
		this.usuarioNombre = usuarioNombre;
	}

	/**
	 * @return the fechaModificacion
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAMODIFICACION")
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	/**
	 * @param fechaModificacion the fechaModificacion to set
	 */
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="ajustador")

    public Set<ReporteSiniestroDTO> getReporteSiniestroDTOs() {
        return this.reporteSiniestroDTOs;
    }
    
    public void setReporteSiniestroDTOs(Set<ReporteSiniestroDTO> reporteSiniestroDTOs) {
        this.reporteSiniestroDTOs = reporteSiniestroDTOs;
    }


    @Column(name="ASIGNACIONES", nullable=false, precision=10, scale=0)
	public Long getAsignaciones() {
		return asignaciones;
	}

	public void setAsignaciones(Long asignaciones) {
		this.asignaciones = asignaciones;
	}
	
	@Column(name = "ESTATUS", nullable = false, precision = 22, scale = 0)
	public Short getEstatus() {
		return estatus;
	}

	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}


	/*
	 * No persistida
	 */
	@Transient
	public Integer getDistancia() {
		return distancia;
	}



	public void setDistancia(Integer distancia) {
		this.distancia = distancia;
	}

	@Override
	public String getDescription() {
		return this.nombre;
	}

	@Override
	public Object getId() {
		return this.idTcAjustador;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof AjustadorDTO) {
			AjustadorDTO ajustadorDTO = (AjustadorDTO) object;
			equal = ajustadorDTO.getIdTcAjustador().equals(this.getIdTcAjustador());
		} // End of if
		return equal;
	}
}