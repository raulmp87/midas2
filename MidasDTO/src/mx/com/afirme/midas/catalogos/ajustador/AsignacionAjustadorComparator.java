package mx.com.afirme.midas.catalogos.ajustador;

import java.util.Comparator;

public class AsignacionAjustadorComparator implements Comparator<AjustadorDTO> {

	public int compare(AjustadorDTO o1, AjustadorDTO o2) {
	    final int BEFORE = -1;
	    final int EQUAL = 0;
	    final int AFTER = 1;
	    
	    if(o1 == o2) return EQUAL;
	    
	    if(o1.getAsignaciones() < o2.getAsignaciones()) 
	    	return BEFORE;
	    else if(o1.getAsignaciones() > o2.getAsignaciones()) return AFTER;
	    
	    else if(o1.getAsignaciones() == o2.getAsignaciones()){
	    	if(o1.getIdTcAjustador().compareTo(o2.getIdTcAjustador()) == BEFORE)
	    	   return BEFORE;
	    	else if(o1.getIdTcAjustador().compareTo(o2.getIdTcAjustador()) == AFTER)
	    		return AFTER;
	    	else
	    		return EQUAL;
	    }
	    
	    return 0;		

	}

}
