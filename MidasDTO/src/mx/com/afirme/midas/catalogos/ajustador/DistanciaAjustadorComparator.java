package mx.com.afirme.midas.catalogos.ajustador;

import java.util.Comparator;

public class DistanciaAjustadorComparator implements Comparator<AjustadorDTO> {

	public int compare(AjustadorDTO o1, AjustadorDTO o2) {
	    final int BEFORE = -1;
	    final int EQUAL = 0;
	    final int AFTER = 1;
	    
	    if(o1 == o2) return EQUAL;
	    
	    if(o1.getDistancia() < o2.getDistancia()) 
	    	return BEFORE;
	    else if(o1.getDistancia() > o2.getDistancia()) return AFTER;
	    
	    else if(o1.getDistancia() == o2.getDistancia()) return EQUAL;	   
	    
	    return 0;
	}

}
