package mx.com.afirme.midas.catalogos.tipomontajemaquina;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for SubtipoMontajeMaquinaFacade.
 * @author MyEclipse Persistence Tools
 */


public interface SubtipoMontajeMaquinaFacadeRemote extends MidasInterfaceBase<SubtipoMontajeMaquinaDTO> {
		/**
	 Perform an initial save of a previously unsaved SubtipoMontajeMaquinaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity SubtipoMontajeMaquinaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(SubtipoMontajeMaquinaDTO entity);
    /**
	 Delete a persistent SubtipoMontajeMaquinaDTO entity.
	  @param entity SubtipoMontajeMaquinaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(SubtipoMontajeMaquinaDTO entity);
   /**
	 Persist a previously saved SubtipoMontajeMaquinaDTO entity and return it or a copy of it to the sender. 
	 A copy of the SubtipoMontajeMaquinaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity SubtipoMontajeMaquinaDTO entity to update
	 @return SubtipoMontajeMaquinaDTO the persisted SubtipoMontajeMaquinaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public SubtipoMontajeMaquinaDTO update(SubtipoMontajeMaquinaDTO entity);

	 /**
	 * Find all SubtipoMontajeMaquinaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the SubtipoMontajeMaquinaDTO property to query
	  @param value the property value to match
	  	  @return List<SubtipoMontajeMaquinaDTO> found by query
	 */
	public List<SubtipoMontajeMaquinaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all SubtipoMontajeMaquinaDTO entities.
	  	  @return List<SubtipoMontajeMaquinaDTO> all SubtipoMontajeMaquinaDTO entities
	 */
	public List<SubtipoMontajeMaquinaDTO> findAll(
		);	
	
	public List<SubtipoMontajeMaquinaDTO> listarFiltrado(SubtipoMontajeMaquinaDTO subtipoMontajeMaquinaDTO);
}