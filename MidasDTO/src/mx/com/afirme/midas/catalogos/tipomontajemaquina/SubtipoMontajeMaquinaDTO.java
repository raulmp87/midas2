package mx.com.afirme.midas.catalogos.tipomontajemaquina;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

import mx.com.afirme.midas.catalogos.SubTipoGenerico;


/**
 * SubtipoMontajeMaquinaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCSUBTIPOMONTAJEMAQ"
    ,schema="MIDAS"
, uniqueConstraints = @UniqueConstraint(columnNames={"IDTCTIPOMONTAJEMAQ", "CODIGOSUBTIPOMONTAJEMAQ"})
)
@Cache(
  type=CacheType.SOFT,
  size=1000,
  expiry=36000000
)
public class SubtipoMontajeMaquinaDTO extends SubTipoGenerico{


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 8076576528788021140L;
	private BigDecimal idtcsubtipomontajemaq;
     private TipoMontajeMaquinaDTO tipoMontajeMaquinaDTO;
     private BigDecimal codigosubtipomontajemaq;
     private String descripcionsubtipomontajemaq;
     private Short claveAutorizacion;


    // Constructors

    /** default constructor */
    public SubtipoMontajeMaquinaDTO() {
    }

    // Property accessors
    @Id 
    @SequenceGenerator(name = "IDTCSUBTIPOMONTAJEMAQ_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCSUBTIPOMONTAJEMAQ_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCSUBTIPOMONTAJEMAQ_SEQ_GENERADOR")
    @Column(name="IDTCSUBTIPOMONTAJEMAQ", unique=true, nullable=false, precision=22, scale=0)

    public BigDecimal getIdtcsubtipomontajemaq() {
        return this.idtcsubtipomontajemaq;
    }
    
    public void setIdtcsubtipomontajemaq(BigDecimal idtcsubtipomontajemaq) {
        this.idtcsubtipomontajemaq = idtcsubtipomontajemaq;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTCTIPOMONTAJEMAQ", nullable=false)

    public TipoMontajeMaquinaDTO getTipoMontajeMaquinaDTO() {
        return this.tipoMontajeMaquinaDTO;
    }
    
    public void setTipoMontajeMaquinaDTO(TipoMontajeMaquinaDTO tipoMontajeMaquinaDTO) {
        this.tipoMontajeMaquinaDTO = tipoMontajeMaquinaDTO;
    }
    
    @Column(name="CODIGOSUBTIPOMONTAJEMAQ", nullable=false, precision=22, scale=0)

    public BigDecimal getCodigosubtipomontajemaq() {
        return this.codigosubtipomontajemaq;
    }
    
    public void setCodigosubtipomontajemaq(BigDecimal codigosubtipomontajemaq) {
        this.codigosubtipomontajemaq = codigosubtipomontajemaq;
    }
    
    @Column(name="DESCRIPCIONSUBTIPOMONTAJEMAQ", nullable=false, precision=22, scale=0)

    public String getDescripcionsubtipomontajemaq() {
        return this.descripcionsubtipomontajemaq;
    }
    
    public void setDescripcionsubtipomontajemaq(String descripcionsubtipomontajemaq) {
        this.descripcionsubtipomontajemaq = descripcionsubtipomontajemaq;
    }
    
    @Column(name="CLAVEAUTORIZACION", nullable=false, precision=4, scale=0)
    public Short getClaveAutorizacion() {
		return claveAutorizacion;
	}

	public void setClaveAutorizacion(Short claveAutorizacion) {
		this.claveAutorizacion = claveAutorizacion;
	}

	@Override
	public String getDescription() {
		return this.descripcionsubtipomontajemaq;
	}

	@Override
	public Object getId() {
		return this.idtcsubtipomontajemaq;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof SubtipoMontajeMaquinaDTO) {
			SubtipoMontajeMaquinaDTO subtipoMontajeMaquinaDTO = (SubtipoMontajeMaquinaDTO) object;
			equal = subtipoMontajeMaquinaDTO.getIdtcsubtipomontajemaq().equals(this.idtcsubtipomontajemaq);
		} // End of if
		return equal;
	}
}