package mx.com.afirme.midas.catalogos.tipomontajemaquina;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.base.CacheableDTO;


/**
 * TipoMontajeMaquinaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCTIPOMONTAJEMAQ"
    ,schema="MIDAS"
, uniqueConstraints = @UniqueConstraint(columnNames="CODIGOTIPOMONTAJEMAQ")
)

public class TipoMontajeMaquinaDTO  extends CacheableDTO {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = -3950721411274445044L;
	private BigDecimal idtctipomontajemaq;
    private BigDecimal codigotipomontajemaq;
    private String descripciontipomontajemaq;
    private List<SubtipoMontajeMaquinaDTO> subtipoMontajeMaquinasDTO = new ArrayList<SubtipoMontajeMaquinaDTO> ();


    // Constructors

    /** default constructor */
    public TipoMontajeMaquinaDTO() {
    }
   
    // Property accessors
    @Id 
    @SequenceGenerator(name = "IDTCTIPOMONTAJEMAQ_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCTIPOMONTAJEMAQ_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCTIPOMONTAJEMAQ_SEQ_GENERADOR")
    @Column(name="IDTCTIPOMONTAJEMAQ", unique=true, nullable=false, precision=22, scale=0)

    public BigDecimal getIdtctipomontajemaq() {
        return this.idtctipomontajemaq;
    }
    
    public void setIdtctipomontajemaq(BigDecimal idtctipomontajemaq) {
        this.idtctipomontajemaq = idtctipomontajemaq;
    }
    
    @Column(name="CODIGOTIPOMONTAJEMAQ", unique=true, nullable=false, precision=22, scale=0)

    public BigDecimal getCodigotipomontajemaq() {
        return this.codigotipomontajemaq;
    }
    
    public void setCodigotipomontajemaq(BigDecimal codigotipomontajemaq) {
        this.codigotipomontajemaq = codigotipomontajemaq;
    }
    
    @Column(name="DESCRIPCIONTIPOMONTAJEMAQ", nullable=false, length=200)

    public String getDescripciontipomontajemaq() {
        return this.descripciontipomontajemaq;
    }
    
    public void setDescripciontipomontajemaq(String descripciontipomontajemaq) {
        this.descripciontipomontajemaq = descripciontipomontajemaq;
    }
   
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="tipoMontajeMaquinaDTO")

    public List<SubtipoMontajeMaquinaDTO> getSubtipoMontajeMaquinasDTO() {
        return this.subtipoMontajeMaquinasDTO;
    }
    
    public void setSubtipoMontajeMaquinasDTO(List<SubtipoMontajeMaquinaDTO> subtipoMontajeMaquinas) {
        this.subtipoMontajeMaquinasDTO = subtipoMontajeMaquinas;
    }

	@Override
	public String getDescription() {
		return this.descripciontipomontajemaq;
	}

	@Override
	public Object getId() {
		return this.idtctipomontajemaq;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof TipoMontajeMaquinaDTO) {
			TipoMontajeMaquinaDTO tipoMontajeMaquinaDTO = (TipoMontajeMaquinaDTO) object;
			equal = tipoMontajeMaquinaDTO.getIdtctipomontajemaq().equals(this.idtctipomontajemaq);
		}
		return equal;
	}







}