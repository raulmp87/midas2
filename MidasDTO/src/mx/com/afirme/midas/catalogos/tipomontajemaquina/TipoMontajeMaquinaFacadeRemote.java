package mx.com.afirme.midas.catalogos.tipomontajemaquina;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for TipoMontajeMaquinaFacade.
 * @author MyEclipse Persistence Tools
 */


public interface TipoMontajeMaquinaFacadeRemote extends MidasInterfaceBase<TipoMontajeMaquinaDTO> {
		/**
	 Perform an initial save of a previously unsaved TipoMontajeMaquinaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity TipoMontajeMaquinaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(TipoMontajeMaquinaDTO entity);
    /**
	 Delete a persistent TipoMontajeMaquinaDTO entity.
	  @param entity TipoMontajeMaquinaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(TipoMontajeMaquinaDTO entity);
   /**
	 Persist a previously saved TipoMontajeMaquinaDTO entity and return it or a copy of it to the sender. 
	 A copy of the TipoMontajeMaquinaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity TipoMontajeMaquinaDTO entity to update
	 @return TipoMontajeMaquinaDTO the persisted TipoMontajeMaquinaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public TipoMontajeMaquinaDTO update(TipoMontajeMaquinaDTO entity);

	 /**
	 * Find all TipoMontajeMaquinaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the TipoMontajeMaquinaDTO property to query
	  @param value the property value to match
	  	  @return List<TipoMontajeMaquinaDTO> found by query
	 */
	public List<TipoMontajeMaquinaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all TipoMontajeMaquinaDTO entities.
	  	  @return List<TipoMontajeMaquinaDTO> all TipoMontajeMaquinaDTO entities
	 */
	public List<TipoMontajeMaquinaDTO> findAll(
		);	
	
	public List<TipoMontajeMaquinaDTO> listarFiltrado(TipoMontajeMaquinaDTO tipoMontajeMaquinaDTO);
}