package mx.com.afirme.midas.catalogos.productoscnsf;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
/**
 * 
 * @author Gustavo Rodriguez
 *
 */
@Entity
@Table(name = "CAT_CLAVEPRODSERV", schema = "AFIFACTELEC")
public class ProductoCnsfDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private BigDecimal id;
	private String claveProdServ;
	private String descripcionProd;
	private String ivaTrasladado;
	private String iepsTrasladado;
	private String complementoProd;
	
	//Constructors
	public ProductoCnsfDTO(){
		
	}
	
	public ProductoCnsfDTO(String claveProdServi, String descripcionProd){
		this.claveProdServ = claveProdServi;
		this.descripcionProd = descripcionProd;
	}
	
	//Property Accessors
	@Id
	@SequenceGenerator(name = "CAT_CLAVEPRODSERV_ID_SEQ", allocationSize = 1, sequenceName = "AFIFACTELEC.CAT_CLAVEPRODSERV_ID_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CAT_CLAVEPRODSERV_ID_SEQ")	
	@Column(name = "IDCLAVEPROD", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	@Column(name = "CLAVEPRODSERV", nullable = false)
	public String getClaveProdServ() {
		return claveProdServ;
	}

	public void setClaveProdServ(String claveProdServ) {
		this.claveProdServ = claveProdServ;
	}

	@Column(name = "DESCRIPCIONPROD", nullable = false, length = 250)
	public String getDescripcionProd() {
		return descripcionProd;
	}

	public void setDescripcionProd(String descripcionProd) {
		this.descripcionProd = descripcionProd;
	}

	@Column(name = "IVATRASLADADO")
	public String getIvaTrasladado() {
		return ivaTrasladado;
	}

	public void setIvaTrasladado(String ivaTrasladado) {
		this.ivaTrasladado = ivaTrasladado;
	}

	@Column(name = "IEPSTRASLADADO")
	public String getIepsTrasladado() {
		return iepsTrasladado;
	}

	public void setIepsTrasladado(String iepsTrasladado) {
		this.iepsTrasladado = iepsTrasladado;
	}

	@Column(name = "COMPLEMENTOPROD", length = 150)
	public String getComplementoProd() {
		return complementoProd;
	}

	public void setComplementoProd(String complementoProd) {
		this.complementoProd = complementoProd;
	}
	
}
