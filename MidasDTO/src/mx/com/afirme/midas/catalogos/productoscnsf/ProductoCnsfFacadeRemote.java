package mx.com.afirme.midas.catalogos.productoscnsf;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;
import javax.persistence.EntityTransaction;

import mx.com.afirme.midas.catalogos.productoscnsf.ProductoCnsfDTO;;

@Remote
public interface ProductoCnsfFacadeRemote {

	public void save(ProductoCnsfDTO entity);

	public void delete(ProductoCnsfDTO entity);

	public ProductoCnsfDTO update(ProductoCnsfDTO entity);

	public List<ProductoCnsfDTO> findByProperty(String propertyName, Object value);
	
	public List<ProductoCnsfDTO> findAll();
	
	public List<ProductoCnsfDTO> listarFiltrado(ProductoCnsfDTO ProductoCnsfDTO);
	
	public ProductoCnsfDTO findById(BigDecimal id);
	
	public EntityTransaction getTransaction();
	
	public Long getCount(ProductoCnsfDTO filtro);
	
	public List<ProductoCnsfDTO> findByFilters(ProductoCnsfDTO filtro,int primerRegistroACargar,int numeroMaximoRegistrosACargar);
}
