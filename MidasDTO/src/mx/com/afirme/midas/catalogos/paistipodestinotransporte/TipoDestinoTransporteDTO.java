package mx.com.afirme.midas.catalogos.paistipodestinotransporte;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;

/**
 * Tctipodestinotransporte entity. @author MyEclipse Persistence Tools
 */

@Entity
@Table(name = "TCTIPODESTINOTRANSPORTE", schema = "MIDAS")
public class TipoDestinoTransporteDTO extends CacheableDTO implements java.io.Serializable {

	
	private static final long serialVersionUID = -5708444756513336888L;
	
	// Fields

	private BigDecimal idTipoDestinoTransporte;
	private BigDecimal codigoTipoDestinoTransporte;
	private String descripcionTipoDestinoTra;

	// Constructors

	/** default constructor */
	public TipoDestinoTransporteDTO() {
	}

	/** minimal constructor */
	public TipoDestinoTransporteDTO(BigDecimal idTipoDestinoTransporte,
			BigDecimal codigoTipoDestinoTransporte,
			String descripcionTipoDestinoTra) {
		
		this.idTipoDestinoTransporte = idTipoDestinoTransporte;
		this.codigoTipoDestinoTransporte = codigoTipoDestinoTransporte;
		this.descripcionTipoDestinoTra = descripcionTipoDestinoTra;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTCTIPODESTINOTRANSPORTE_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCTIPODESTINOTRANSPORTE_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCTIPODESTINOTRANSPORTE_SEQ_GENERADOR")
	@Column(name = "IDTCTIPODESTINOTRANSPORTE", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTipoDestinoTransporte() {
		return this.idTipoDestinoTransporte;
	}

	public void setIdTipoDestinoTransporte(
			BigDecimal idTipoDestinoTransporte) {
		this.idTipoDestinoTransporte = idTipoDestinoTransporte;
	}

	@Column(name = "CODIGOTIPODESTINOTRANSPORTE", nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoTipoDestinoTransporte() {
		return this.codigoTipoDestinoTransporte;
	}

	public void setCodigoTipoDestinoTransporte(
			BigDecimal codigoTipoDestinoTransporte) {
		this.codigoTipoDestinoTransporte = codigoTipoDestinoTransporte;
	}

	@Column(name = "DESCRIPCIONTIPODESTINOTRA", nullable = false, length = 200)
	public String getDescripcionTipoDestinoTransporte() {
		return this.descripcionTipoDestinoTra;
	}

	public void setDescripcionTipoDestinoTransporte(String descripcionTipoDestinoTransporte) {
		this.descripcionTipoDestinoTra = descripcionTipoDestinoTransporte;
	}

	@Override
	public String getDescription() {
		return this.descripcionTipoDestinoTra;
	}

	@Override
	public BigDecimal getId() {
		return this.idTipoDestinoTransporte;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof TipoDestinoTransporteDTO) {
			TipoDestinoTransporteDTO tipoDestinoTransporteDTO = (TipoDestinoTransporteDTO) object;
			equal = tipoDestinoTransporteDTO.getId().equals(this.getId());
		} // End of if
		return equal;
	}

}