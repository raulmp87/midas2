package mx.com.afirme.midas.catalogos.paistipodestinotransporte;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for TctipodestinotransporteFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface TipoDestinoTransporteFacadeRemote extends
		MidasInterfaceBase<TipoDestinoTransporteDTO> {
	/**
	 * Perform an initial save of a previously unsaved Tctipodestinotransporte
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            Tctipodestinotransporte entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoDestinoTransporteDTO entity);

	/**
	 * Delete a persistent Tctipodestinotransporte entity.
	 * 
	 * @param entity
	 *            Tctipodestinotransporte entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoDestinoTransporteDTO entity);

	/**
	 * Persist a previously saved Tctipodestinotransporte entity and return it
	 * or a copy of it to the sender. A copy of the Tctipodestinotransporte
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            Tctipodestinotransporte entity to update
	 * @return Tctipodestinotransporte the persisted Tctipodestinotransporte
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoDestinoTransporteDTO update(TipoDestinoTransporteDTO entity);

	/**
	 * Find all Tctipodestinotransporte entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the Tctipodestinotransporte property to query
	 * @param value
	 *            the property value to match
	 * @return List<Tctipodestinotransporte> found by query
	 */
	public List<TipoDestinoTransporteDTO> findByProperty(String propertyName,
			Object value);

	public List<TipoDestinoTransporteDTO> findByDescripcionTipoDestinoTransporte(
			Object descripcionTipoDestinoTransporte);

	public List<TipoDestinoTransporteDTO> listarFiltrado(
			TipoDestinoTransporteDTO tipoDestinoTransporteDTO);
}