package mx.com.afirme.midas.catalogos.paistipodestinotransporte;


import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.PaisDTO;

/**
 * PaisTipoDestinoTransporteDTO entity. @author MyEclipse Persistence Tools
 */

@Entity(name="PaisTipoDestinoTransporteDTO")
@Table(name = "TCPAISTIPODESTINOTRANSPORTE", schema = "MIDAS")
public class PaisTipoDestinoTransporteDTO extends CacheableDTO {

	private static final long serialVersionUID = -5708444756517776888L;
	
	// Fields

	private String idPais;
	private BigDecimal idTipoDestinoTransporte;
	
	private TipoDestinoTransporteDTO tipoDestinoTransporteDTO;
	private PaisDTO paisDTO;

	// Constructors

	public PaisTipoDestinoTransporteDTO() {
		if (this.tipoDestinoTransporteDTO == null)
		this.tipoDestinoTransporteDTO = new TipoDestinoTransporteDTO();
	}

	// Property accessors
	
	@Id
	@Column(name="IDPAIS", nullable=false, precision = 6, scale = 0)
	public String getIdPais() {
		return idPais;
	}

	public void setIdPais(String idPais) {
		this.idPais = idPais;
	}
	
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDPAIS", nullable = false, insertable = false, updatable = false)
	public PaisDTO getPais() {
		return this.paisDTO;
	}
	
	public void setPais(PaisDTO paisDTO) {
		this.paisDTO = paisDTO;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCTIPODESTINOTRANSPORTE", nullable = false, insertable = false, updatable = false)
	public TipoDestinoTransporteDTO getTipoDestinoTransporte() {
		return this.tipoDestinoTransporteDTO;
	}
	
	public void setTipoDestinoTransporte(TipoDestinoTransporteDTO tipoDestinoTransporteDTO) {
		this.tipoDestinoTransporteDTO = tipoDestinoTransporteDTO;
	}
	
	@Column(name="IDTCTIPODESTINOTRANSPORTE", nullable=false, precision = 22, scale = 0)
	public BigDecimal getIdTipoDestinoTransporte() {
		return idTipoDestinoTransporte;
	}

	public void setIdTipoDestinoTransporte(BigDecimal idTipoDestinoTransporte) {
		this.idTipoDestinoTransporte = idTipoDestinoTransporte;
	}

	@Override
	public String getDescription() {
		return null;
	}

	@Override
	public Object getId() {
		return this.idPais;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof PaisTipoDestinoTransporteDTO) {
			PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO = (PaisTipoDestinoTransporteDTO) object;
			equal = paisTipoDestinoTransporteDTO.getIdPais().equals(this.idPais);
		} // End of if
		return equal;
	}
}