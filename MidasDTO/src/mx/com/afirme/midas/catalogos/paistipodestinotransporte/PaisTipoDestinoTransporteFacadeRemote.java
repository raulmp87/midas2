package mx.com.afirme.midas.catalogos.paistipodestinotransporte;

import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for PaisTipoDestinoTransporteFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface PaisTipoDestinoTransporteFacadeRemote extends MidasInterfaceBase<PaisTipoDestinoTransporteDTO> {
	/**
	 * Perform an initial save of a previously unsaved
	 * PaisTipoDestinoTransporteDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            PaisTipoDestinoTransporteDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(PaisTipoDestinoTransporteDTO entity);

	/**
	 * Delete a persistent PaisTipoDestinoTransporteDTO entity.
	 * 
	 * @param entity
	 *            PaisTipoDestinoTransporteDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(PaisTipoDestinoTransporteDTO entity);

	/**
	 * Persist a previously saved PaisTipoDestinoTransporteDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * PaisTipoDestinoTransporteDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            PaisTipoDestinoTransporteDTO entity to update
	 * @return PaisTipoDestinoTransporteDTO the persisted
	 *         PaisTipoDestinoTransporteDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public PaisTipoDestinoTransporteDTO update(PaisTipoDestinoTransporteDTO entity);
	
	public PaisTipoDestinoTransporteDTO findById(PaisTipoDestinoTransporteDTO id);

	/**
	 * Find all PaisTipoDestinoTransporte entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the PaisTipoDestinoTransporteDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<PaisTipoDestinoTransporteDTO> found by query
	 */
	public List<PaisTipoDestinoTransporteDTO> findByProperty(
			String propertyName, Object value);

	/**
	 * Find all PaisTipoDestinoTransporteDTO entities.
	 * 
	 * @return List<PaisTipoDestinoTransporteDTO> all PaisTipoDestinoTransporteDTO
	 *         entities
	 */
	public List<PaisTipoDestinoTransporteDTO> findAll();
	
	public List<PaisTipoDestinoTransporteDTO> listarFiltrado(PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO);
	
}