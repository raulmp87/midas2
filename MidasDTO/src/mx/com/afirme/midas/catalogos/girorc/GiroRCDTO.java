package mx.com.afirme.midas.catalogos.girorc;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.base.CacheableDTO;

/**
 * GiroRC entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCGIRORC", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = "CODIGOGIRORC"))
public class GiroRCDTO extends CacheableDTO{
	private static final long serialVersionUID = -1042005177069902221L;

	private BigDecimal idTcGiroRC;
	private BigDecimal codigoGiroRC;
	private String descripcionGiroRC;
	private Set<SubGiroRCDTO> subGiroRCs = new HashSet<SubGiroRCDTO>(0);

	public GiroRCDTO() {
	}

	/** full constructor */
	public GiroRCDTO(BigDecimal idTcGiroRC, BigDecimal codigoGiroRC,
			String descripcionGiroRC, Set<SubGiroRCDTO> subGiroRCs) {
		this.idTcGiroRC = idTcGiroRC;
		this.codigoGiroRC = codigoGiroRC;
		this.descripcionGiroRC = descripcionGiroRC;
		this.subGiroRCs = subGiroRCs;
	}

	@Id
	@SequenceGenerator(name = "IDTCGIRORC_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCGIRORC_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCGIRORC_SEQ_GENERADOR")	
	@Column(name = "idTcGiroRC", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcGiroRC() {
		return this.idTcGiroRC;
	}

	public void setIdTcGiroRC(BigDecimal idTcGiroRC) {
		this.idTcGiroRC = idTcGiroRC;
	}

	@Column(name = "CODIGOGIRORC", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoGiroRC() {
		return this.codigoGiroRC;
	}

	public void setCodigoGiroRC(BigDecimal codigoGiroRC) {
		this.codigoGiroRC = codigoGiroRC;
	}

	@Column(name = "DESCRIPCIONGIRORC", nullable = false, length = 200)
	public String getDescripcionGiroRC() {
		return this.descripcionGiroRC;
	}

	public void setDescripcionGiroRC(String descripcionGiroRC) {
		this.descripcionGiroRC = descripcionGiroRC;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "giroRC")
	public Set<SubGiroRCDTO> getSubGiroRCs() {
		return this.subGiroRCs;
	}

	public void setSubGiroRCs(Set<SubGiroRCDTO> subGiroRCs) {
		this.subGiroRCs = subGiroRCs;
	}

	@Override
	public String getDescription() {
		return this.descripcionGiroRC;
	}

	@Override
	public Object getId() {
		return this.idTcGiroRC;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o instanceof GiroRCDTO) {
			if (((GiroRCDTO) o).getCodigoGiroRC().equals(this.getCodigoGiroRC())) {
				return true;
			}
		}
		return false;
	}
}