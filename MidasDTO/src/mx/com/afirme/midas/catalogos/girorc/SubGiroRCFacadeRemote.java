package mx.com.afirme.midas.catalogos.girorc;
// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;


/**
 * Remote interface for SubGiroRCFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SubGiroRCFacadeRemote extends MidasInterfaceBase<SubGiroRCDTO> {
	/**
	 * Perform an initial save of a previously unsaved SubGiroRC entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SubGiroRC entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SubGiroRCDTO entity);

	/**
	 * Delete a persistent SubGiroRC entity.
	 * 
	 * @param entity
	 *            SubGiroRC entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SubGiroRCDTO entity);

	/**
	 * Persist a previously saved SubGiroRC entity and return it or a copy of it
	 * to the sender. A copy of the SubGiroRC entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            SubGiroRC entity to update
	 * @return SubGiroRC the persisted SubGiroRC entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SubGiroRCDTO update(SubGiroRCDTO entity);

	/**
	 * Find all SubGiroRC entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SubGiroRC property to query
	 * @param value
	 *            the property value to match
	 * @return List<SubGiroRC> found by query
	 */
	public List<SubGiroRCDTO> findByProperty(String propertyName, Object value);

	public List<SubGiroRCDTO> findByDescripcionsubgirorc(
			Object descripcionsubgirorc);

	public List<SubGiroRCDTO> findByClavetiporiesgo(Object clavetiporiesgo);

	public List<SubGiroRCDTO> findByClaveinspeccion(Object claveinspeccion);

	/**
	 * Find all SubGiroRC entities.
	 * 
	 * @return List<SubGiroRC> all SubGiroRC entities
	 */
	public List<SubGiroRCDTO> findAll();

	public List<SubGiroRCDTO> listarFiltrado(SubGiroRCDTO subGiroRCDTO);

}