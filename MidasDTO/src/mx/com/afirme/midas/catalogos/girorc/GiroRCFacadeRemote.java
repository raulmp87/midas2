package mx.com.afirme.midas.catalogos.girorc;
// default package

import java.util.List;

import javax.ejb.Remote;
import javax.persistence.EntityTransaction;

import mx.com.afirme.midas.base.MidasInterfaceBase;


/**
 * Remote interface for GiroRCFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface GiroRCFacadeRemote extends MidasInterfaceBase<GiroRCDTO> {
	/**
	 * Perform an initial save of a previously unsaved GiroRC entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            GiroRC entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(GiroRCDTO entity);

	/**
	 * Delete a persistent GiroRC entity.
	 * 
	 * @param entity
	 *            GiroRC entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(GiroRCDTO entity);

	/**
	 * Persist a previously saved GiroRC entity and return it or a copy of it to
	 * the sender. A copy of the GiroRC entity parameter is returned when the
	 * JPA persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            GiroRC entity to update
	 * @return GiroRC the persisted GiroRC entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public GiroRCDTO update(GiroRCDTO entity);

	/**
	 * Find all GiroRC entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the GiroRC property to query
	 * @param value
	 *            the property value to match
	 * @return List<GiroRC> found by query
	 */
	public List<GiroRCDTO> findByProperty(String propertyName, Object value);

	public List<GiroRCDTO> findByDescripciongirorc(Object descripciongirorc);

	/**
	 * Find all GiroRC entities.
	 * 
	 * @return List<GiroRC> all GiroRC entities
	 */
	public List<GiroRCDTO> findAll();
	
	public List<GiroRCDTO> listarFiltrado(GiroRCDTO giroRCDTO);
	
	public EntityTransaction getTransaction();
}