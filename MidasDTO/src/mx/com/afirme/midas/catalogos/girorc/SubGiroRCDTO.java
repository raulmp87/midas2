package mx.com.afirme.midas.catalogos.girorc;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

import mx.com.afirme.midas.catalogos.SubTipoGenerico;

/**
 * SubGiroRC entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCSUBGIRORC", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = {
		"IDTCGIRORC", "CODIGOSUBGIRORC" }))
@Cache(
  type=CacheType.SOFT,
  size=1000,
  expiry=36000000
)
public class SubGiroRCDTO extends SubTipoGenerico {
	private static final long serialVersionUID = -3923916665953728440L;

	private BigDecimal idTcSubGiroRC;
	private GiroRCDTO giroRC;
	private BigDecimal codigoSubGiroRC;
	private String descripcionSubGiroRC;
	private Short claveTipoRiesgo;
	private Short claveAutorizacion;

	public SubGiroRCDTO() {
	}

	@Id
	@SequenceGenerator(name = "IDTCGIRORC_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCGIRORC_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCGIRORC_SEQ_GENERADOR")
	@Column(name = "IDTCSUBGIRORC", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcSubGiroRC() {
		return this.idTcSubGiroRC;
	}

	public void setIdTcSubGiroRC(BigDecimal idTcSubGiroRC) {
		this.idTcSubGiroRC = idTcSubGiroRC;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCGIRORC", nullable = false)
	public GiroRCDTO getGiroRC() {
		return this.giroRC;
	}

	public void setGiroRC(GiroRCDTO giroRC) {
		this.giroRC = giroRC;
	}

	@Column(name = "CODIGOSUBGIRORC", nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoSubGiroRC() {
		return this.codigoSubGiroRC;
	}

	public void setCodigoSubGiroRC(BigDecimal codigoSubGiroRC) {
		this.codigoSubGiroRC = codigoSubGiroRC;
	}

	@Column(name = "DESCRIPCIONSUBGIRORC", nullable = false, length = 200)
	public String getDescripcionSubGiroRC() {
		return this.descripcionSubGiroRC;
	}

	public void setDescripcionSubGiroRC(String descripcionSubGiroRC) {
		this.descripcionSubGiroRC = descripcionSubGiroRC;
	}

	@Column(name = "CLAVETIPORIESGO", nullable = false, precision = 4, scale = 0)
	public Short getClaveTipoRiesgo() {
		return this.claveTipoRiesgo;
	}

	public void setClaveTipoRiesgo(Short claveTipoRiesgo) {
		this.claveTipoRiesgo = claveTipoRiesgo;
	}

	@Column(name = "CLAVEAUTORIZACION", nullable = false, precision = 4, scale = 0)
	public Short getClaveAutorizacion() {
		return claveAutorizacion;
	}

	public void setClaveAutorizacion(Short claveAutorizacion) {
		this.claveAutorizacion = claveAutorizacion;
	}

	@Override
	public String getDescription() {
		return this.descripcionSubGiroRC;
	}

	@Override
	public Object getId() {
		return this.idTcSubGiroRC;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof SubGiroRCDTO) {
			SubGiroRCDTO dto = (SubGiroRCDTO) object;
			equal = dto.getIdTcSubGiroRC().equals(this.idTcSubGiroRC);
		} // End of if
		return equal;
	}
}