package mx.com.afirme.midas.catalogos.tiponegocio;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for TipoNegocioDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface TipoNegocioFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved TipoNegocioDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            TipoNegocioDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoNegocioDTO entity);

	/**
	 * Delete a persistent TipoNegocioDTO entity.
	 * 
	 * @param entity
	 *            TipoNegocioDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoNegocioDTO entity);

	/**
	 * Persist a previously saved TipoNegocioDTO entity and return it or a copy
	 * of it to the sender. A copy of the TipoNegocioDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoNegocioDTO entity to update
	 * @return TipoNegocioDTO the persisted TipoNegocioDTO entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoNegocioDTO update(TipoNegocioDTO entity);

	public TipoNegocioDTO findById(BigDecimal id);

	/**
	 * Find all TipoNegocioDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the TipoNegocioDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoNegocioDTO> found by query
	 */
	public List<TipoNegocioDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all TipoNegocioDTO entities.
	 * 
	 * @return List<TipoNegocioDTO> all TipoNegocioDTO entities
	 */
	public List<TipoNegocioDTO> findAll();
}