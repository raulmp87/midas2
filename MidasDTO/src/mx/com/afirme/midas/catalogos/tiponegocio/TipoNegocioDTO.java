package mx.com.afirme.midas.catalogos.tiponegocio;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.siniestro.cabina.configcorreo.ConfigCorreoTipoNegociooDTO;

/**
 * TipoNegocioDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCTIPONEGOCIO", schema = "MIDAS")
public class TipoNegocioDTO extends CacheableDTO implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private BigDecimal idTcTipoNegocio;
	private String descripcionTipoNegocio;
	private BigDecimal codigoTipoNegocio;
	private List<CotizacionDTO> cotizacionDTOs = new ArrayList<CotizacionDTO>();
	private List<ConfigCorreoTipoNegociooDTO> configCorreoTipoNegocioDTOs = new ArrayList<ConfigCorreoTipoNegociooDTO>();
	
	// Property accessors
	@Id
	@Column(name = "IDTCTIPONEGOCIO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcTipoNegocio() {
		return this.idTcTipoNegocio;
	}

	public void setIdTcTipoNegocio(BigDecimal idTcTipoNegocio) {
		this.idTcTipoNegocio = idTcTipoNegocio;
	}

	@Column(name = "DESCRIPCIONTIPONEGOCIO", nullable = false, length = 100)
	public String getDescripcionTipoNegocio() {
		return this.descripcionTipoNegocio;
	}

	public void setDescripcionTipoNegocio(String descripcionTipoNegocio) {
		this.descripcionTipoNegocio = descripcionTipoNegocio;
	}

	@Column(name = "CODIGOTIPONEGOCIO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoTipoNegocio() {
		return this.codigoTipoNegocio;
	}

	public void setCodigoTipoNegocio(BigDecimal codigoTipoNegocio) {
		this.codigoTipoNegocio = codigoTipoNegocio;
	}

	public void setCotizacionDTOs(List<CotizacionDTO> cotizacionDTOs) {
		this.cotizacionDTOs = cotizacionDTOs;
	}

	@OneToMany(mappedBy="tipoNegocioDTO")
	public List<CotizacionDTO> getCotizacionDTOs() {
		return cotizacionDTOs;
	}
	
	public void setConfigCorreoTipoNegocioDTOs(List<ConfigCorreoTipoNegociooDTO> configCorreoTipoNegocioDTOs) {
		this.configCorreoTipoNegocioDTOs = configCorreoTipoNegocioDTOs;
	}
	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name="IDTCTIPONEGOCIO")
	public List<ConfigCorreoTipoNegociooDTO> getConfigCorreoTipoNegocioDTOs() {
		return configCorreoTipoNegocioDTOs;
	}

	@Override
	public String getDescription() {
		
		return this.getCodigoTipoNegocio() + " - " + this.getDescripcionTipoNegocio();
	}

	@Override
	public Object getId() {
		
		return this.getIdTcTipoNegocio();
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof TipoNegocioDTO) {
			TipoNegocioDTO tipoNegocioDTO = (TipoNegocioDTO) object;
			equal = tipoNegocioDTO.getIdTcTipoNegocio().equals(this.idTcTipoNegocio);
		} // End of if
		return equal;
	}
}