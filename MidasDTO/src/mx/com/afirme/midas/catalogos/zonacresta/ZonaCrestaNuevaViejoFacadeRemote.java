package mx.com.afirme.midas.catalogos.zonacresta;
// default package

import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for ZonaCrestaNuevaViejoFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ZonaCrestaNuevaViejoFacadeRemote extends MidasInterfaceBase<ZonaCrestaNuevaViejoDTO> {
		/**
	 Perform an initial save of a previously unsaved ZonaCrestaNuevaViejoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ZonaCrestaNuevaViejoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ZonaCrestaNuevaViejoDTO entity);
    /**
	 Delete a persistent ZonaCrestaNuevaViejoDTO entity.
	  @param entity ZonaCrestaNuevaViejoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ZonaCrestaNuevaViejoDTO entity);
   /**
	 Persist a previously saved ZonaCrestaNuevaViejoDTO entity and return it or a copy of it to the sender. 
	 A copy of the ZonaCrestaNuevaViejoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ZonaCrestaNuevaViejoDTO entity to update
	 @return ZonaCrestaNuevaViejoDTO the persisted ZonaCrestaNuevaViejoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ZonaCrestaNuevaViejoDTO update(ZonaCrestaNuevaViejoDTO entity);
	public ZonaCrestaNuevaViejoDTO findById( ZonaCrestaNuevaViejoId id);
	 /**
	 * Find all ZonaCrestaNuevaViejoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ZonaCrestaNuevaViejoDTO property to query
	  @param value the property value to match
	  	  @return List<ZonaCrestaNuevaViejoDTO> found by query
	 */
	public List<ZonaCrestaNuevaViejoDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ZonaCrestaNuevaViejoDTO entities.
	  	  @return List<ZonaCrestaNuevaViejoDTO> all ZonaCrestaNuevaViejoDTO entities
	 */
	public List<ZonaCrestaNuevaViejoDTO> findAll(
		);	
}