package mx.com.afirme.midas.catalogos.zonacresta;
// default package

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;


/**
 * ZonaCrestaViejoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name ="ZonaCrestaViejoDTO")
@Table(name="TCZONACRESTAVIEJO"
    ,schema="MIDAS"
)

public class ZonaCrestaViejoDTO extends CacheableDTO {


    // Fields
	
	private static final long serialVersionUID = 1L;
	private BigDecimal idtczonacrestaviejo;
     private String geocodigoviejo;
     private String nombreareaviejo;
     private BigDecimal numeroareaviejo;
     private BigDecimal tipozona;
     private Set<ZonaCrestaNuevaViejoDTO> zonaCrestaNuevaViejoDTOs = new HashSet<ZonaCrestaNuevaViejoDTO>(0);


    // Constructors

    /** default constructor */
    public ZonaCrestaViejoDTO() {
    }

	/** minimal constructor */
    public ZonaCrestaViejoDTO(BigDecimal idtczonacrestaviejo, String geocodigoviejo, String nombreareaviejo, BigDecimal numeroareaviejo, BigDecimal tipozona) {
        this.idtczonacrestaviejo = idtczonacrestaviejo;
        this.geocodigoviejo = geocodigoviejo;
        this.nombreareaviejo = nombreareaviejo;
        this.numeroareaviejo = numeroareaviejo;
        this.tipozona = tipozona;
    }
    
    /** full constructor */
    public ZonaCrestaViejoDTO(BigDecimal idtczonacrestaviejo, String geocodigoviejo, String nombreareaviejo, BigDecimal numeroareaviejo, BigDecimal tipozona, Set<ZonaCrestaNuevaViejoDTO> zonaCrestaNuevaViejoDTOs) {
        this.idtczonacrestaviejo = idtczonacrestaviejo;
        this.geocodigoviejo = geocodigoviejo;
        this.nombreareaviejo = nombreareaviejo;
        this.numeroareaviejo = numeroareaviejo;
        this.tipozona = tipozona;
        this.zonaCrestaNuevaViejoDTOs = zonaCrestaNuevaViejoDTOs;
    }

   
    // Property accessors
    @Id 
    @SequenceGenerator(name = "IDTCZONACRESTAVIEJO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCZONACRESTAVIEJO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCZONACRESTAVIEJO_SEQ_GENERADOR")
    @Column(name="IDTCZONACRESTAVIEJO", unique=true, nullable=false, precision=22, scale=0)

    public BigDecimal getIdtczonacrestaviejo() {
        return this.idtczonacrestaviejo;
    }
    
    public void setIdtczonacrestaviejo(BigDecimal idtczonacrestaviejo) {
        this.idtczonacrestaviejo = idtczonacrestaviejo;
    }
    
    @Column(name="GEOCODIGOVIEJO", nullable=false, length=50)

    public String getGeocodigoviejo() {
        return this.geocodigoviejo;
    }
    
    public void setGeocodigoviejo(String geocodigoviejo) {
        this.geocodigoviejo = geocodigoviejo;
    }
    
    @Column(name="NOMBREAREAVIEJO", nullable=false, length=50)

    public String getNombreareaviejo() {
        return this.nombreareaviejo;
    }
    
    public void setNombreareaviejo(String nombreareaviejo) {
        this.nombreareaviejo = nombreareaviejo;
    }
    
    @Column(name="NUMEROAREAVIEJO", nullable=false, precision=22, scale=0)

    public BigDecimal getNumeroareaviejo() {
        return this.numeroareaviejo;
    }
    
    public void setNumeroareaviejo(BigDecimal numeroareaviejo) {
        this.numeroareaviejo = numeroareaviejo;
    }
    
    @Column(name="TIPOZONA", nullable=false, precision=22, scale=0)

    public BigDecimal getTipozona() {
        return this.tipozona;
    }
    
    public void setTipozona(BigDecimal tipozona) {
        this.tipozona = tipozona;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="zonaCrestaViejo")

    public Set<ZonaCrestaNuevaViejoDTO> getZonaCrestaNuevaViejos() {
        return this.zonaCrestaNuevaViejoDTOs;
    }
    
    public void setZonaCrestaNuevaViejos(Set<ZonaCrestaNuevaViejoDTO> zonaCrestaNuevaViejoDTOs) {
        this.zonaCrestaNuevaViejoDTOs = zonaCrestaNuevaViejoDTOs;
    }

    public boolean equals(Object other) {
		boolean equal = (other == this);
		if (!equal && other instanceof ZonaCrestaViejoDTO) {
			ZonaCrestaViejoDTO zonaCrestaViejo = (ZonaCrestaViejoDTO) other;
			equal = zonaCrestaViejo.getIdtczonacrestaviejo().equals(this.idtczonacrestaviejo);
		} // End of if
		return equal;
    }

	@Override
	public String getDescription() {
		return this.nombreareaviejo;
	}

	@Override
	public Object getId() {
		return this.idtczonacrestaviejo;
	}
}