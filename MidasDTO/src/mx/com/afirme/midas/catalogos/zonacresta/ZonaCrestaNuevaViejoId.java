package mx.com.afirme.midas.catalogos.zonacresta;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * ZonaCrestaNuevaViejoId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class ZonaCrestaNuevaViejoId  implements java.io.Serializable {


    // Fields
	
	private static final long serialVersionUID = 1L;
	private BigDecimal idtczonacresta;
     private BigDecimal idtczonacrestaviejo;


    // Constructors

    /** default constructor */
    public ZonaCrestaNuevaViejoId() {
    }

    
    /** full constructor */
    public ZonaCrestaNuevaViejoId(BigDecimal idtczonacresta, BigDecimal idtczonacrestaviejo) {
        this.idtczonacresta = idtczonacresta;
        this.idtczonacrestaviejo = idtczonacrestaviejo;
    }

   
    // Property accessors

    @Column(name="IDTCZONACRESTA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtczonacresta() {
        return this.idtczonacresta;
    }
    
    public void setIdtczonacresta(BigDecimal idtczonacresta) {
        this.idtczonacresta = idtczonacresta;
    }

    @Column(name="IDTCZONACRESTAVIEJO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdtczonacrestaviejo() {
        return this.idtczonacrestaviejo;
    }
    
    public void setIdtczonacrestaviejo(BigDecimal idtczonacrestaviejo) {
        this.idtczonacrestaviejo = idtczonacrestaviejo;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof ZonaCrestaNuevaViejoId) ) return false;
		 ZonaCrestaNuevaViejoId castOther = ( ZonaCrestaNuevaViejoId ) other; 
         
		 return ( (this.getIdtczonacresta()==castOther.getIdtczonacresta()) || ( this.getIdtczonacresta()!=null && castOther.getIdtczonacresta()!=null && this.getIdtczonacresta().equals(castOther.getIdtczonacresta()) ) )
 && ( (this.getIdtczonacrestaviejo()==castOther.getIdtczonacrestaviejo()) || ( this.getIdtczonacrestaviejo()!=null && castOther.getIdtczonacrestaviejo()!=null && this.getIdtczonacrestaviejo().equals(castOther.getIdtczonacrestaviejo()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdtczonacresta() == null ? 0 : this.getIdtczonacresta().hashCode() );
         result = 37 * result + ( getIdtczonacrestaviejo() == null ? 0 : this.getIdtczonacrestaviejo().hashCode() );
         return result;
   }   





}