package mx.com.afirme.midas.catalogos.zonacresta;
// default package

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;


/**
 * ZonaCrestaNuevaViejoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity (name ="ZonaCrestaNuevaViejoDTO")
@Table(name="TRZONACRESTANUEVAVIEJO"
    ,schema="MIDAS"
)

public class ZonaCrestaNuevaViejoDTO extends CacheableDTO {


    // Fields    
	private static final long serialVersionUID = 1L;
	private ZonaCrestaNuevaViejoId id;
     private ZonaCrestaViejoDTO zonaCrestaViejoDTO;
     private ZonaCrestaDTO zonaCrestaDTO;


    // Constructors

    /** default constructor */
    public ZonaCrestaNuevaViejoDTO() {
    }

    
    /** full constructor */
    public ZonaCrestaNuevaViejoDTO(ZonaCrestaNuevaViejoId id, ZonaCrestaViejoDTO zonaCrestaViejoDTO, ZonaCrestaDTO zonaCrestaDTO) {
        this.id = id;
        this.zonaCrestaViejoDTO = zonaCrestaViejoDTO;
        this.zonaCrestaDTO = zonaCrestaDTO;
    }

   
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idtczonacresta", column=@Column(name="IDTCZONACRESTA", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idtczonacrestaviejo", column=@Column(name="IDTCZONACRESTAVIEJO", nullable=false, precision=22, scale=0) ) } )

    public ZonaCrestaNuevaViejoId getId() {
        return this.id;
    }
    
    public void setId(ZonaCrestaNuevaViejoId id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="IDTCZONACRESTA", nullable=false, insertable=false, updatable=false)

    public ZonaCrestaViejoDTO getZonaCrestaViejo() {
        return this.zonaCrestaViejoDTO;
    }
    
    public void setZonaCrestaViejo(ZonaCrestaViejoDTO zonaCrestaViejoDTO) {
        this.zonaCrestaViejoDTO = zonaCrestaViejoDTO;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="IDTCZONACRESTA", nullable=false, insertable=false, updatable=false)

    public ZonaCrestaDTO getZonaCresta() {
        return this.zonaCrestaDTO;
    }
    
    public void setZonaCresta(ZonaCrestaDTO zonaCrestaDTO) {
        this.zonaCrestaDTO = zonaCrestaDTO;
    }
   

    public boolean equals(Object other) {
		boolean equal = (other == this);
		if (!equal && other instanceof ZonaCrestaNuevaViejoDTO) {
			ZonaCrestaNuevaViejoDTO trZonaCresta = (ZonaCrestaNuevaViejoDTO) other;
			equal = (trZonaCresta.getId().getIdtczonacresta().equals(this.id.getIdtczonacresta()) && trZonaCresta.getId().getIdtczonacrestaviejo().equals(this.id.getIdtczonacrestaviejo()));
		} // End of if
		return equal;
    }


	@Override
	public String getDescription() {
		return null;
	}
}