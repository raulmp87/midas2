package mx.com.afirme.midas.catalogos.zonacresta;
// default package

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for ZonaCrestaViejoFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ZonaCrestaViejoFacadeRemote extends MidasInterfaceBase<ZonaCrestaViejoDTO> {
		/**
	 Perform an initial save of a previously unsaved ZonaCrestaViejoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ZonaCrestaViejoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ZonaCrestaViejoDTO entity);
    /**
	 Delete a persistent ZonaCrestaViejoDTO entity.
	  @param entity ZonaCrestaViejoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ZonaCrestaViejoDTO entity);
   /**
	 Persist a previously saved ZonaCrestaViejoDTO entity and return it or a copy of it to the sender. 
	 A copy of the ZonaCrestaViejoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ZonaCrestaViejoDTO entity to update
	 @return ZonaCrestaViejoDTO the persisted ZonaCrestaViejoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ZonaCrestaViejoDTO update(ZonaCrestaViejoDTO entity);

	 /**
	 * Find all ZonaCrestaViejoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ZonaCrestaViejoDTO property to query
	  @param value the property value to match
	  	  @return List<ZonaCrestaViejoDTO> found by query
	 */
	public List<ZonaCrestaViejoDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ZonaCrestaViejoDTO entities.
	  	  @return List<ZonaCrestaViejoDTO> all ZonaCrestaViejoDTO entities
	 */
	public List<ZonaCrestaViejoDTO> findAll(
		);	
	
	public List<ZonaCrestaViejoDTO> buscarFiltradoSegunIdNuevo(BigDecimal idNuevo);
	
	public List<ZonaCrestaViejoDTO> listarFiltrado(ZonaCrestaViejoDTO zonaCrestaViejoDTO);
}