package mx.com.afirme.midas.catalogos.zonacresta;
// default package

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;

/**
 * ZonaCrestaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name="ZonaCrestaDTO")
@Table(name="TCZONACRESTA"
    ,schema="MIDAS"
)


public class ZonaCrestaDTO extends CacheableDTO {


    // Fields
	private static final long serialVersionUID = 1L;
	private BigDecimal idtczonacresta;
     private String nombrearea;
     private BigDecimal numeroarea;
     private String geocodigo;
     private String idtcmunicipio;
     //private Set<ZonaCrestaNuevaViejoDTO> zonaCrestaNuevaViejoDTOs = new HashSet<ZonaCrestaNuevaViejoDTO>(0);


    // Constructors

    /** default constructor */
    public ZonaCrestaDTO() {
    }

	/** minimal constructor */
    public ZonaCrestaDTO(BigDecimal idtczonacresta, String nombrearea, BigDecimal numeroarea, String geocodigo, String idtcmunicipio) {
        this.idtczonacresta = idtczonacresta;
        this.nombrearea = nombrearea;
        this.numeroarea = numeroarea;
        this.geocodigo = geocodigo;
        this.idtcmunicipio = idtcmunicipio;
    }
    
    /** full constructor */
    public ZonaCrestaDTO(BigDecimal idtczonacresta, String nombrearea, BigDecimal numeroarea, String geocodigo, String idtcmunicipio, Set<ZonaCrestaNuevaViejoDTO> zonaCrestaNuevaViejoDTOs) {
        this.idtczonacresta = idtczonacresta;
        this.nombrearea = nombrearea;
        this.numeroarea = numeroarea;
        this.geocodigo = geocodigo;
        this.idtcmunicipio = idtcmunicipio;
        //this.zonaCrestaNuevaViejoDTOs = zonaCrestaNuevaViejoDTOs;
    }

    
    
    
    
    
    
    
   
    // Property accessors
    @Id 
    @SequenceGenerator(name = "IDTCZONACRESTA_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCZONACRESTA_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCZONACRESTA_SEQ_GENERADOR")
    @Column(name="IDTCZONACRESTA", unique=true, nullable=false, precision=22, scale=0)

    public BigDecimal getIdtczonacresta() {
        return this.idtczonacresta;
    }
    
    public void setIdtczonacresta(BigDecimal idtczonacresta) {
        this.idtczonacresta = idtczonacresta;
    }
    
    @Column(name="NOMBREAREA", nullable=false, length=50)

    public String getNombrearea() {
        return this.nombrearea;
    }
    
    public void setNombrearea(String nombrearea) {
        this.nombrearea = nombrearea;
    }
    
    @Column(name="NUMEROAREA", nullable=false, precision=22, scale=0)

    public BigDecimal getNumeroarea() {
        return this.numeroarea;
    }
    
    public void setNumeroarea(BigDecimal numeroarea) {
        this.numeroarea = numeroarea;
    }
    
    @Column(name="GEOCODIGO", nullable=false, length=50)

    public String getGeocodigo() {
        return this.geocodigo;
    }
    
    public void setGeocodigo(String geocodigo) {
        this.geocodigo = geocodigo;
    }
    
    @Column(name="IDTCMUNICIPIO", nullable=false, precision=22, scale=0)

    public String getIdtcmunicipio() {
        return this.idtcmunicipio;
    }
    
    public void setIdtcmunicipio(String idtcmunicipio) {
        this.idtcmunicipio = idtcmunicipio;
    }

    public boolean equals(Object other) {
		boolean equal = (other == this);
		if (!equal && other instanceof ZonaCrestaDTO) {
			ZonaCrestaDTO zonaCresta = (ZonaCrestaDTO) other;
			equal = zonaCresta.getIdtczonacresta().equals(this.idtczonacresta);
		} // End of if
		return equal;
    }

	@Override
	public String getDescription() {
		return this.nombrearea;
	}

	@Override
	public Object getId() {
		return this.idtczonacresta;
	}   
}