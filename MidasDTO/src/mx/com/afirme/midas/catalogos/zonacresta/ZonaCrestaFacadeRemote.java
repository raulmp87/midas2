package mx.com.afirme.midas.catalogos.zonacresta;
// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for ZonaCrestaFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ZonaCrestaFacadeRemote extends MidasInterfaceBase<ZonaCrestaDTO> {
		/**
	 Perform an initial save of a previously unsaved ZonaCrestaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ZonaCrestaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ZonaCrestaDTO entity);
    /**
	 Delete a persistent ZonaCrestaDTO entity.
	  @param entity ZonaCrestaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ZonaCrestaDTO entity);
   /**
	 Persist a previously saved ZonaCrestaDTO entity and return it or a copy of it to the sender. 
	 A copy of the ZonaCrestaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ZonaCrestaDTO entity to update
	 @return ZonaCrestaDTO the persisted ZonaCrestaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ZonaCrestaDTO update(ZonaCrestaDTO entity);

	 /**
	 * Find all ZonaCrestaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ZonaCrestaDTO property to query
	  @param value the property value to match
	  	  @return List<ZonaCrestaDTO> found by query
	 */
	public List<ZonaCrestaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ZonaCrestaDTO entities.
	  	  @return List<ZonaCrestaDTO> all ZonaCrestaDTO entities
	 */
	public List<ZonaCrestaDTO> findAll(
		);	
	
	public List<ZonaCrestaDTO> listarFiltrado(ZonaCrestaDTO zonaCrestaDTO);
		
}