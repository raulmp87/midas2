package mx.com.afirme.midas.catalogos.marcavehiculo;
// default package

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipobienautos.TipoBienAutosDTO;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.CotizacionExpressPaso1Checks;


/**
 * MarcaVehiculoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCMARCAVEHICULO"
    ,schema="MIDAS"
)

public class MarcaVehiculoDTO  extends CacheableDTO implements Entidad {


    /**
	 * 
	 */
	private static final long serialVersionUID = -7041563372554075088L;
	// Fields    

     private BigDecimal idTcMarcaVehiculo;
     private TipoBienAutosDTO tipoBienAutosDTO=new TipoBienAutosDTO();
     private String codigoMarcaVehiculo;
     private String descripcionMarcaVehiculo;
     private List<EstiloVehiculoDTO> estiloVehiculoDTOs = new java.util.ArrayList<EstiloVehiculoDTO>();
     private TipoVehiculoDTO tipoVehiculoDTO = new TipoVehiculoDTO();


    // Constructors

    /** default constructor */
    public MarcaVehiculoDTO() {
    }

	/** minimal constructor */
    public MarcaVehiculoDTO(BigDecimal idTcMarcaVehiculo, TipoBienAutosDTO tipoBienAutosDTO, String codigoMarcaVehiculo) {
        this.idTcMarcaVehiculo = idTcMarcaVehiculo;
        this.tipoBienAutosDTO = tipoBienAutosDTO;
        this.codigoMarcaVehiculo = codigoMarcaVehiculo;
    }
    
    /** full constructor */
    public MarcaVehiculoDTO(BigDecimal idTcMarcaVehiculo, TipoBienAutosDTO tipoBienAutosDTO, String codigoMarcaVehiculo, String descripcionMarcaVehiculo, List<EstiloVehiculoDTO> estiloVehiculoDTOs) {
        this.idTcMarcaVehiculo = idTcMarcaVehiculo;
        this.tipoBienAutosDTO = tipoBienAutosDTO;
        this.codigoMarcaVehiculo = codigoMarcaVehiculo;
        this.descripcionMarcaVehiculo = descripcionMarcaVehiculo;
        this.estiloVehiculoDTOs = estiloVehiculoDTOs;
    }

   
    // Property accessors
    @Id 
    @SequenceGenerator(name = "idTcMarcaVehiculo_seq_GEN", allocationSize = 1, sequenceName = "MIDAS.idTcMarcaVehiculo_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idTcMarcaVehiculo_seq_GEN")
    @Column(name="IDTCMARCAVEHICULO", unique=true, nullable=false, precision=22, scale=0)
    @NotNull(groups=CotizacionExpressPaso1Checks.class, message="{com.afirme.midas2.requerido}")
    public BigDecimal getIdTcMarcaVehiculo() {
        return this.idTcMarcaVehiculo;
    }
    
    public void setIdTcMarcaVehiculo(BigDecimal idTcMarcaVehiculo) {
        this.idTcMarcaVehiculo = idTcMarcaVehiculo;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="CLAVETIPOBIEN", nullable=false)

    public TipoBienAutosDTO getTipoBienAutosDTO() {
        return this.tipoBienAutosDTO;
    }
    
    public void setTipoBienAutosDTO(TipoBienAutosDTO tipoBienAutosDTO) {
        this.tipoBienAutosDTO = tipoBienAutosDTO;
    }
    
    @Column(name="CODIGOMARCAVEHICULO", nullable=false, length=5)

    public String getCodigoMarcaVehiculo() {
        return this.codigoMarcaVehiculo!=null?this.codigoMarcaVehiculo.toUpperCase():this.codigoMarcaVehiculo;
    }
    
    public void setCodigoMarcaVehiculo(String codigoMarcaVehiculo) {
        this.codigoMarcaVehiculo = codigoMarcaVehiculo;
    }
    
    @Column(name="DESCRIPCIONMARCAVEHICULO", length=100)

    public String getDescripcionMarcaVehiculo() {
        return this.descripcionMarcaVehiculo!=null?this.descripcionMarcaVehiculo.toUpperCase():this.descripcionMarcaVehiculo;
    }
    
    public void setDescripcionMarcaVehiculo(String descripcionMarcaVehiculo) {
        this.descripcionMarcaVehiculo = descripcionMarcaVehiculo;
    }
    
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="marcaVehiculoDTO")
    public List<EstiloVehiculoDTO> getEstiloVehiculoDTOs() {
        return this.estiloVehiculoDTOs;
    }
    
    public void setEstiloVehiculoDTOs(List<EstiloVehiculoDTO> estiloVehiculoDTOs) {
        this.estiloVehiculoDTOs = estiloVehiculoDTOs;
    }

	@Override
	public String getDescription() {
		return getDescripcionMarcaVehiculo();
	}

	@Override
	public Object getId() {
		return getIdTcMarcaVehiculo();
	}
	
	
	@Transient
	public TipoVehiculoDTO getTipoVehiculoDTO() {
		return tipoVehiculoDTO;
	}

	public void setTipoVehiculoDTO(TipoVehiculoDTO tipoVehiculoDTO) {
		this.tipoVehiculoDTO = tipoVehiculoDTO;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof MarcaVehiculoDTO) {
			MarcaVehiculoDTO dto = (MarcaVehiculoDTO) object;
			equal = dto.getIdTcMarcaVehiculo().equals(this.idTcMarcaVehiculo);
		} // End of if
		return equal;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return idTcMarcaVehiculo;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getBusinessKey() {
		return idTcMarcaVehiculo;
	}



	@Override
	public String getValue() {
		return descripcionMarcaVehiculo;
	}

}