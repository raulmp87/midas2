package mx.com.afirme.midas.catalogos.marcavehiculo;
// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for MarcaVehiculoDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface MarcaVehiculoFacadeRemote extends MidasInterfaceBase<MarcaVehiculoDTO>{
		/**
	 Perform an initial save of a previously unsaved MarcaVehiculoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity MarcaVehiculoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(MarcaVehiculoDTO entity);
    /**
	 Delete a persistent MarcaVehiculoDTO entity.
	  @param entity MarcaVehiculoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(MarcaVehiculoDTO entity);
   /**
	 Persist a previously saved MarcaVehiculoDTO entity and return it or a copy of it to the sender. 
	 A copy of the MarcaVehiculoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity MarcaVehiculoDTO entity to update
	 @return MarcaVehiculoDTO the persisted MarcaVehiculoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public MarcaVehiculoDTO update(MarcaVehiculoDTO entity);
	
	 /**
	 * Find all MarcaVehiculoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the MarcaVehiculoDTO property to query
	  @param value the property value to match
	  	  @return List<MarcaVehiculoDTO> found by query
	 */
	public List<MarcaVehiculoDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all MarcaVehiculoDTO entities.
	  	  @return List<MarcaVehiculoDTO> all MarcaVehiculoDTO entities
	 */
	public List<MarcaVehiculoDTO> findAll(
		);
	
	public List<MarcaVehiculoDTO> listarFiltrado(MarcaVehiculoDTO marcaVehiculoDTO);
}