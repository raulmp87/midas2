package mx.com.afirme.midas.catalogos.marcavehiculo;
// default package

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.catalogos.tipobienautos.TipoBienAutosDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.CotizacionExpressPaso1Checks;


/**
 * MarcaVehiculoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCSUBMARCAVEHICULO"
    ,schema="MIDAS"
)

public class SubMarcaVehiculoDTO  extends CacheableDTO implements Entidad {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idSubTcMarcaVehiculo;
	private MarcaVehiculoDTO marcaVehiculoDTO = new MarcaVehiculoDTO();    
	 /** default constructor */
    public SubMarcaVehiculoDTO() {
    }
	private String descripcionSubMarcaVehiculo;
	 public SubMarcaVehiculoDTO(BigDecimal idSubTcMarcaVehiculo, MarcaVehiculoDTO marcaVehiculoDTO, String descripcionSubMarcaVehiculo) {
	        this.idSubTcMarcaVehiculo = idSubTcMarcaVehiculo;
	        this.marcaVehiculoDTO = marcaVehiculoDTO;
	        this.descripcionSubMarcaVehiculo = descripcionSubMarcaVehiculo;
	    }

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return idSubTcMarcaVehiculo;
	}
	
	@Override
	public String getValue() {
		return descripcionSubMarcaVehiculo;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getBusinessKey() {
		return idSubTcMarcaVehiculo;
	}

	@Override
	public Object getId() {
		return getIdSubTcMarcaVehiculo();
	}
	@Override
	public String getDescription() {
		return getDescripcionSubMarcaVehiculo();
	}
	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof SubMarcaVehiculoDTO) {
			SubMarcaVehiculoDTO dto = (SubMarcaVehiculoDTO) object;
			equal = dto.getIdSubTcMarcaVehiculo().equals(this.idSubTcMarcaVehiculo);
		} // End of if
		return equal;
	}
    @Id 
    @SequenceGenerator(name = "TCSUBMARCAVEHICULO_SEQ_GEN", allocationSize = 1, sequenceName = "MIDAS.TCSUBMARCAVEHICULO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TCSUBMARCAVEHICULO_SEQ_GEN")
    @Column(name="IDTCSUBMARCAVEHICULO", unique=true, nullable=false, precision=22, scale=0)
    @NotNull(groups=CotizacionExpressPaso1Checks.class, message="{com.afirme.midas2.requerido}")
	public BigDecimal getIdSubTcMarcaVehiculo() {
		return idSubTcMarcaVehiculo;
	}

	public void setIdSubTcMarcaVehiculo(BigDecimal idSubTcMarcaVehiculo) {
		this.idSubTcMarcaVehiculo = idSubTcMarcaVehiculo;
	}

	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTCMARCAVEHICULO", nullable=false)
	public MarcaVehiculoDTO getMarcaVehiculoDTO() {
	    return this.marcaVehiculoDTO;
	}
	
	public void setMarcaVehiculoDTO(MarcaVehiculoDTO marcaVehiculoDTO) {
	    this.marcaVehiculoDTO = marcaVehiculoDTO;
	}
	@Column(name="DESCRIPCION", length=100)
	public String getDescripcionSubMarcaVehiculo() {
		return descripcionSubMarcaVehiculo;
	}

	public void setDescripcionSubMarcaVehiculo(String descripcionSubMarcaVehiculo) {
		this.descripcionSubMarcaVehiculo = descripcionSubMarcaVehiculo;
	}
	
	
	
    
    
}