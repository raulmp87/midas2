package mx.com.afirme.midas.catalogos.marcavehiculo;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.CotizacionExpressPaso1Checks;

@Entity
@Table(name = "TCMARCAVEHICULOEQUIVALENCIA", schema = "MIDAS")
public class MarcaVehiculoEquivalenciaDTO extends CacheableDTO implements
		Entidad {

	private static final long serialVersionUID = -3928271519695235662L;
	private BigDecimal idTcMarcaVehiculoEq;
	private String codigoMarcaVehiculo;
	private String descripcionMarcaVehiculo;

	public MarcaVehiculoEquivalenciaDTO() {
	}

	public MarcaVehiculoEquivalenciaDTO(BigDecimal idTcMarcaVehiculoEq,
			String codigoMarcaVehiculo) {
		this.idTcMarcaVehiculoEq = idTcMarcaVehiculoEq;
		this.codigoMarcaVehiculo = codigoMarcaVehiculo;
	}

	public MarcaVehiculoEquivalenciaDTO(BigDecimal idTcMarcaVehiculoEq,
			String codigoMarcaVehiculo, String descripcionMarcaVehiculo) {
		this.idTcMarcaVehiculoEq = idTcMarcaVehiculoEq;
		this.codigoMarcaVehiculo = codigoMarcaVehiculo;
		this.descripcionMarcaVehiculo = descripcionMarcaVehiculo;
	}

	@Id
	@SequenceGenerator(name = "IDTCMARCAVEHICULOEQ_SEQ_GEN", allocationSize = 1, sequenceName = "MIDAS.IDTCMARCAVEHICULOEQ_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCMARCAVEHICULOEQ_SEQ_GEN")
	@Column(name = "IDTCMARCAVEHICULOEQ", unique = true, nullable = false, precision = 22, scale = 0)
	@NotNull(groups = CotizacionExpressPaso1Checks.class, message = "{com.afirme.midas2.requerido}")
	public BigDecimal getIdTcMarcaVehiculoEq() {
		return this.idTcMarcaVehiculoEq;
	}

	public void setIdTcMarcaVehiculoEq(BigDecimal idTcMarcaVehiculoEq) {
		this.idTcMarcaVehiculoEq = idTcMarcaVehiculoEq;
	}

	@Column(name = "CODIGOMARCAVEHICULO", nullable = false, length = 5)
	public String getCodigoMarcaVehiculo() {
		return this.codigoMarcaVehiculo != null ? this.codigoMarcaVehiculo
				.toUpperCase() : this.codigoMarcaVehiculo;
	}

	public void setCodigoMarcaVehiculo(String codigoMarcaVehiculo) {
		this.codigoMarcaVehiculo = codigoMarcaVehiculo;
	}

	@Column(name = "DESCRIPCIONMARCAVEHICULO", length = 100)
	public String getDescripcionMarcaVehiculo() {
		return this.descripcionMarcaVehiculo != null ? this.descripcionMarcaVehiculo
				.toUpperCase() : this.descripcionMarcaVehiculo;
	}

	public void setDescripcionMarcaVehiculo(String descripcionMarcaVehiculo) {
		this.descripcionMarcaVehiculo = descripcionMarcaVehiculo;
	}

	@Override
	public String getDescription() {
		return getDescripcionMarcaVehiculo();
	}

	@Override
	public Object getId() {
		return getIdTcMarcaVehiculoEq();
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof MarcaVehiculoEquivalenciaDTO) {
			MarcaVehiculoEquivalenciaDTO dto = (MarcaVehiculoEquivalenciaDTO) object;
			equal = dto.getIdTcMarcaVehiculoEq().equals(
					this.idTcMarcaVehiculoEq);
		}
		return equal;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return idTcMarcaVehiculoEq;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getBusinessKey() {
		return idTcMarcaVehiculoEq;
	}

	@Override
	public String getValue() {
		return descripcionMarcaVehiculo;
	}
}