package mx.com.afirme.midas.catalogos.marcavehiculo;
//default package

import java.util.List;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
* Remote interface for SubMarcaVehiculoDTOFacade.
* @author MyEclipse Persistence Tools
*/


public interface SubMarcaVehiculoFacadeRemote extends MidasInterfaceBase<SubMarcaVehiculoDTO>{
		/**
	 Perform an initial save of a previously unsaved SubMarcaVehiculoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity SubMarcaVehiculoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
 public void save(SubMarcaVehiculoDTO entity);
 /**
	 Delete a persistent SubMarcaVehiculoDTO entity.
	  @param entity SubMarcaVehiculoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
 public void delete(SubMarcaVehiculoDTO entity);
/**
	 Persist a previously saved SubMarcaVehiculoDTO entity and return it or a copy of it to the sender. 
	 A copy of the SubMarcaVehiculoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity SubMarcaVehiculoDTO entity to update
	 @return SubMarcaVehiculoDTO the persisted SubMarcaVehiculoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public SubMarcaVehiculoDTO update(SubMarcaVehiculoDTO entity);
	
	 /**
	 * Find all SubMarcaVehiculoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the SubMarcaVehiculoDTO property to query
	  @param value the property value to match
	  	  @return List<SubMarcaVehiculoDTO> found by query
	 */
	public List<SubMarcaVehiculoDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all SubMarcaVehiculoDTO entities.
	  	  @return List<SubMarcaVehiculoDTO> all SubMarcaVehiculoDTO entities
	 */
	public List<SubMarcaVehiculoDTO> findAll(
		);
	
	public List<SubMarcaVehiculoDTO> listarFiltrado(SubMarcaVehiculoDTO subMarcaVehiculoDTO);
}