package mx.com.afirme.midas.catalogos.institucionbancaria;


import mx.com.afirme.midas.base.MidasInterfaceBase;

public interface BancoEmisorFacadeRemote extends MidasInterfaceBase<BancoEmisorDTO>{
	public BancoEmisorDTO findById(Integer idBanco);
	
	public BancoEmisorDTO findByName(String nombreBanco);
	
	public String encriptaDatos(String encriptaDato);
	
	public String desEncriptaDatos(String encriptaDato);
}
