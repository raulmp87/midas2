package mx.com.afirme.midas.catalogos.institucionbancaria;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import mx.com.afirme.midas.base.CacheableDTO;
/**
 * Entidad que representa el catalogo de bancos
 * @author vmhersil
 *
 */
@Entity(name="BancoDTO")
@Table(name="VW_BANCO",schema="MIDAS")
public class BancoDTO  extends CacheableDTO implements Comparable<BancoDTO>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final Integer AMERICAN_EXPRESS = 60;
	public static final String BANCO_AFIRME = "AFIRME";
	public static final String BANCO_AMERICAN_EXPRESS = "AMERICAN EXPRESS";
	private Integer idBanco;
	private String	nombreBanco;
	@Id
	@Column(name="idBanco",nullable=false,length=4)
	public Integer getIdBanco() {
		return idBanco;
	}

	public void setIdBanco(Integer idBanco) {
		this.idBanco = idBanco;
	}
	@Column(name="nombreBanco",nullable=false,length=150)
	public String getNombreBanco() {
		return nombreBanco;
	}

	public void setNombreBanco(String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}

	@Override
	public int compareTo(BancoDTO o) {
		return this.idBanco.compareTo(o.getIdBanco());
	}

	@Override
	public Object getId() {
		return this.idBanco;
	}

	@Override
	public String getDescription() {
		return this.nombreBanco;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal=(object==this);
		if(!equal && (object instanceof BancoDTO)){
			BancoDTO instance=(BancoDTO)object;
			equal=instance.getIdBanco().equals(this.idBanco);
		}
		return equal;
	}

}
