package mx.com.afirme.midas.catalogos.institucionbancaria;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import mx.com.afirme.midas.base.CacheableDTO;
/**
 * Entidad que representa el catalogo de bancos para cobranza
 * @author lpervil
 *
 */
@Entity(name="BancoEmisorDTO")
@Table(name="VW_BANCO_EMISOR",schema="MIDAS")
public class BancoEmisorDTO  extends CacheableDTO implements Comparable<BancoEmisorDTO>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idBanco;
	private String	nombreBanco;
	@Id
	@Column(name="idBanco",nullable=false,length=4)
	public Integer getIdBanco() {
		return idBanco;
	}

	public void setIdBanco(Integer idBanco) {
		this.idBanco = idBanco;
	}
	@Column(name="nombreBanco",nullable=false,length=150)
	public String getNombreBanco() {
		return nombreBanco;
	}

	public void setNombreBanco(String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}

	@Override
	public int compareTo(BancoEmisorDTO o) {
		return this.idBanco.compareTo(o.getIdBanco());
	}

	@Override
	public Object getId() {
		return this.idBanco;
	}

	@Override
	public String getDescription() {
		return this.nombreBanco;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal=(object==this);
		if(!equal && (object instanceof BancoEmisorDTO)){
			BancoEmisorDTO instance=(BancoEmisorDTO)object;
			equal=instance.getIdBanco().equals(this.idBanco);
		}
		return equal;
	}

}
