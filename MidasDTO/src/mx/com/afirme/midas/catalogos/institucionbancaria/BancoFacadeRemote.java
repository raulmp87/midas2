package mx.com.afirme.midas.catalogos.institucionbancaria;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

public interface BancoFacadeRemote extends MidasInterfaceBase<BancoDTO>{
	public BancoDTO findById(Integer idBanco);
	
	public BancoDTO findByName(String nombreBanco);
}
