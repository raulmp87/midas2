package mx.com.afirme.midas.catalogos.tiposerviciovehiculo;
// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for TipoServicioVehiculoDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface TipoServicioVehiculoFacadeRemote extends MidasInterfaceBase<TipoServicioVehiculoDTO>{
		/**
	 Perform an initial save of a previously unsaved TipoServicioVehiculoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity TipoServicioVehiculoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(TipoServicioVehiculoDTO entity);
    /**
	 Delete a persistent TipoServicioVehiculoDTO entity.
	  @param entity TipoServicioVehiculoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(TipoServicioVehiculoDTO entity);
   /**
	 Persist a previously saved TipoServicioVehiculoDTO entity and return it or a copy of it to the sender. 
	 A copy of the TipoServicioVehiculoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity TipoServicioVehiculoDTO entity to update
	 @return TipoServicioVehiculoDTO the persisted TipoServicioVehiculoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public TipoServicioVehiculoDTO update(TipoServicioVehiculoDTO entity);
	
	 /**
	 * Find all TipoServicioVehiculoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the TipoServicioVehiculoDTO property to query
	  @param value the property value to match
	  	  @return List<TipoServicioVehiculoDTO> found by query
	 */
	public List<TipoServicioVehiculoDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Filtered Find TipoServicioVehiculoDTO entities.
	  	  @return List<TipoServicioVehiculoDTO> TipoServicioVehiculoDTO entities
	 */
	public List<TipoServicioVehiculoDTO> listarFiltrado(TipoServicioVehiculoDTO tipoServicioVehiculoDTO);
}