package mx.com.afirme.midas.catalogos.tiposerviciovehiculo;
// default package

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;




/**
 * TipoServicioVehiculoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCTIPOSERVICIOVEHICULO"
    ,schema="MIDAS"
)

public class TipoServicioVehiculoDTO extends CacheableDTO implements java.io.Serializable, Entidad {

	private static final long serialVersionUID = 164410180349044822L;
	private BigDecimal idTcTipoServicioVehiculo;
	private BigDecimal idTcTipoVehiculo;
     private String codigoTipoServicioVehiculo;
     private String descripcionTipoServVehiculo;
     private TipoVehiculoDTO tipoVehiculoDTO = new TipoVehiculoDTO();


    // Constructors

    /** default constructor */
    public TipoServicioVehiculoDTO() {
    }

	/** minimal constructor */
    public TipoServicioVehiculoDTO(BigDecimal idTcTipoServicioVehiculo, BigDecimal idTcTipoVehiculo, String codigoTipoServicioVehiculo) {
        this.idTcTipoServicioVehiculo = idTcTipoServicioVehiculo;
        this.idTcTipoVehiculo = idTcTipoVehiculo;
        this.codigoTipoServicioVehiculo = codigoTipoServicioVehiculo;
    }
    
    /** full constructor */
    public TipoServicioVehiculoDTO(BigDecimal idTcTipoServicioVehiculo, BigDecimal idTcTipoVehiculo, String codigoTipoServicioVehiculo, String descripcionTipoServVehiculo) {
        this.idTcTipoServicioVehiculo = idTcTipoServicioVehiculo;
        this.idTcTipoVehiculo = idTcTipoVehiculo;
        this.codigoTipoServicioVehiculo = codigoTipoServicioVehiculo;
        this.descripcionTipoServVehiculo = descripcionTipoServVehiculo;
    }

   
    // Property accessors
    @Id 
    @SequenceGenerator(name = "idTcTipoServV_seq_GEN", allocationSize = 1, sequenceName = "MIDAS.idTcTipoServicioVehiculo_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idTcTipoServV_seq_GEN")
    @Column(name="IDTCTIPOSERVICIOVEHICULO", unique=true, nullable=false, precision=22, scale=0)

    public BigDecimal getIdTcTipoServicioVehiculo() {
        return this.idTcTipoServicioVehiculo;
    }
    
    public void setIdTcTipoServicioVehiculo(BigDecimal idTcTipoServicioVehiculo) {
        this.idTcTipoServicioVehiculo = idTcTipoServicioVehiculo;
    }
    
    @Column(name="IDTCTIPOVEHICULO", nullable=false, length=5)

    public BigDecimal getIdTcTipoVehiculo() {
        return this.idTcTipoVehiculo;
    }
    
    public void setIdTcTipoVehiculo(BigDecimal idTcTipoVehiculo) {
        this.idTcTipoVehiculo = idTcTipoVehiculo;
    }
    
    
    @Column(name="CODIGOTIPOSERVICIOVEHICULO", nullable=false, length=5)

    public String getCodigoTipoServicioVehiculo() {
        return this.codigoTipoServicioVehiculo!=null?this.codigoTipoServicioVehiculo.toUpperCase():this.codigoTipoServicioVehiculo;
    }
    
    public void setCodigoTipoServicioVehiculo(String codigoTipoServicioVehiculo) {
        this.codigoTipoServicioVehiculo = codigoTipoServicioVehiculo;
    }
    
    @Column(name="DESCRIPCIONTIPOSERVVEHICULO", length=100)

    public String getDescripcionTipoServVehiculo() {
        return this.descripcionTipoServVehiculo!=null?this.descripcionTipoServVehiculo.toUpperCase():this.descripcionTipoServVehiculo;
    }
    
    public void setDescripcionTipoServVehiculo(String descripcionTipoServVehiculo) {
        this.descripcionTipoServVehiculo = descripcionTipoServVehiculo;
    }

	@Override
	public Object getId() {
		return codigoTipoServicioVehiculo;
	}

	@Override
	public String getDescription() {
		return descripcionTipoServVehiculo;
	}

	@Transient
	public TipoVehiculoDTO getTipoVehiculoDTO() {
		return tipoVehiculoDTO;
	}

	public void setTipoVehiculoDTO(TipoVehiculoDTO tipoVehiculoDTO) {
		this.tipoVehiculoDTO = tipoVehiculoDTO;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((codigoTipoServicioVehiculo == null) ? 0
						: codigoTipoServicioVehiculo.hashCode());
		result = prime
				* result
				+ ((descripcionTipoServVehiculo == null) ? 0
						: descripcionTipoServVehiculo.hashCode());
		result = prime
				* result
				+ ((idTcTipoServicioVehiculo == null) ? 0
						: idTcTipoServicioVehiculo.hashCode());
		result = prime
				* result
				+ ((idTcTipoVehiculo == null) ? 0 : idTcTipoVehiculo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoServicioVehiculoDTO other = (TipoServicioVehiculoDTO) obj;
		if (codigoTipoServicioVehiculo == null) {
			if (other.codigoTipoServicioVehiculo != null)
				return false;
		} else if (!codigoTipoServicioVehiculo
				.equals(other.codigoTipoServicioVehiculo))
			return false;
		if (descripcionTipoServVehiculo == null) {
			if (other.descripcionTipoServVehiculo != null)
				return false;
		} else if (!descripcionTipoServVehiculo
				.equals(other.descripcionTipoServVehiculo))
			return false;
		if (idTcTipoServicioVehiculo == null) {
			if (other.idTcTipoServicioVehiculo != null)
				return false;
		} else if (!idTcTipoServicioVehiculo
				.equals(other.idTcTipoServicioVehiculo))
			return false;
		if (idTcTipoVehiculo == null) {
			if (other.idTcTipoVehiculo != null)
				return false;
		} else if (!idTcTipoVehiculo.equals(other.idTcTipoVehiculo))
			return false;
		return true;
	}

	@Override
	public BigDecimal getKey() {		
		return idTcTipoServicioVehiculo;
	}

	@Override
	public String getValue() {		
		return descripcionTipoServVehiculo;
	}

	@Override
	public BigDecimal getBusinessKey() {		
		return idTcTipoServicioVehiculo;
	}

}