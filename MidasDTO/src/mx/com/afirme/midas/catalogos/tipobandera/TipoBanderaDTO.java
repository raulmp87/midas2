package mx.com.afirme.midas.catalogos.tipobandera;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.base.CacheableDTO;

/**
 * TipoBanderaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCTIPOBANDERA", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = "CODIGOTIPOBANDERA"))
public class TipoBanderaDTO extends CacheableDTO implements java.io.Serializable {
	private static final long serialVersionUID = -2593021066160551746L;
	// Fields

	private BigDecimal idTcTipoBandera;
	private BigDecimal codigoTipoBandera;
	private String descripcionTipoBandera;

	// Constructors

	/** default constructor */
	public TipoBanderaDTO() {
	}

	// Property accessors
	@Id
	@Column(name = "IDTCTIPOBANDERA", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcTipoBandera() {
		return this.idTcTipoBandera;
	}

	public void setIdTcTipoBandera(BigDecimal idTcTipoBandera) {
		this.idTcTipoBandera = idTcTipoBandera;
	}

	@Column(name = "CODIGOTIPOBANDERA", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoTipoBandera() {
		return this.codigoTipoBandera;
	}

	public void setCodigoTipoBandera(BigDecimal codigoTipoBandera) {
		this.codigoTipoBandera = codigoTipoBandera;
	}

	@Column(name = "DESCRIPCIONTIPOBANDERA", nullable = false, length = 200)
	public String getDescripcionTipoBandera() {
		return this.descripcionTipoBandera;
	}

	public void setDescripcionTipoBandera(String descripcionTipoBandera) {
		this.descripcionTipoBandera = descripcionTipoBandera;
	}
	
	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof TipoBanderaDTO) {
			TipoBanderaDTO tipoBanderaDTO = (TipoBanderaDTO) object;
			equal = tipoBanderaDTO.getIdTcTipoBandera().equals(this.getIdTcTipoBandera());
		} // End of if
		return equal;
	}
	
	@Override
	public String getDescription() {
		return this.descripcionTipoBandera;
	}
	
	@Override
	public Object getId() {
		return this.idTcTipoBandera;
	}

}