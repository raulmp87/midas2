package mx.com.afirme.midas.catalogos.tipobandera;
// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for TipoBanderaFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface TipoBanderaFacadeRemote extends MidasInterfaceBase<TipoBanderaDTO> {
	/**
	 * Perform an initial save of a previously unsaved TipoBanderaDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            TipoBanderaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoBanderaDTO entity);

	/**
	 * Delete a persistent TipoBanderaDTO entity.
	 * 
	 * @param entity
	 *            TipoBanderaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoBanderaDTO entity);

	/**
	 * Persist a previously saved TipoBanderaDTO entity and return it or a copy of
	 * it to the sender. A copy of the TipoBanderaDTO entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            TipoBanderaDTO entity to update
	 * @return TipoBanderaDTO the persisted TipoBanderaDTO entity instance, may not be
	 *         the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoBanderaDTO update(TipoBanderaDTO entity);

	/**
	 * Find all TipoBanderaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the TipoBanderaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoBanderaDTO> found by query
	 */
	public List<TipoBanderaDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all TipoBanderaDTO entities.
	 * 
	 * @return List<TipoBanderaDTO> all TipoBanderaDTO entities
	 */
	public List<TipoBanderaDTO> findAll();
	
	/**
	 * Find filtered TipoBanderaDTO entities.
	 * 
	 * @return List<TipoBanderaDTO> filtered TipoBanderaDTO entities
	 */
	public List<TipoBanderaDTO> listarFiltrado(TipoBanderaDTO tipoBanderaDTO);
}