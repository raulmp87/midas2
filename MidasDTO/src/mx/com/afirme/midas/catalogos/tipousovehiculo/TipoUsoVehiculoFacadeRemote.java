package mx.com.afirme.midas.catalogos.tipousovehiculo;
// default package

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;

/**
 * Remote interface for TipoUsoVehiculoDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface TipoUsoVehiculoFacadeRemote extends MidasInterfaceBase<TipoUsoVehiculoDTO>{
		/**
	 Perform an initial save of a previously unsaved TipoUsoVehiculoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity TipoUsoVehiculoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(TipoUsoVehiculoDTO entity);
    /**
	 Delete a persistent TipoUsoVehiculoDTO entity.
	  @param entity TipoUsoVehiculoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(TipoUsoVehiculoDTO entity);
   /**
	 Persist a previously saved TipoUsoVehiculoDTO entity and return it or a copy of it to the sender. 
	 A copy of the TipoUsoVehiculoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity TipoUsoVehiculoDTO entity to update
	 @return TipoUsoVehiculoDTO the persisted TipoUsoVehiculoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public TipoUsoVehiculoDTO update(TipoUsoVehiculoDTO entity);
	 /**
	 * Find all TipoUsoVehiculoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the TipoUsoVehiculoDTO property to query
	  @param value the property value to match
	  	  @return List<TipoUsoVehiculoDTO> found by query
	 */
	public List<TipoUsoVehiculoDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all TipoUsoVehiculoDTO entities.
	  	  @return List<TipoUsoVehiculoDTO> all TipoUsoVehiculoDTO entities
	 */
	public List<TipoUsoVehiculoDTO> findAll();
	
	public List<TipoUsoVehiculoDTO> listarFiltrado(TipoUsoVehiculoDTO tipoUsoVehiculoDTO);
	
	public List<TipoUsoVehiculoDTO> findByClaveTipoBien(String claveTipoBien);
	public List<TipoUsoVehiculoDTO> findByClaveTipoBienBySeccion(BigDecimal idToSeccion);
	public TipoUsoVehiculoDTO findByTipoVehiculoAndDescripcion(TipoVehiculoDTO tipoVehiculoDTO, String usoDescripcion);
	
}