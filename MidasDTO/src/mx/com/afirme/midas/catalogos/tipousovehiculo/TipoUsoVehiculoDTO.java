package mx.com.afirme.midas.catalogos.tipousovehiculo;
// default package

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;


/**
 * TipoUsoVehiculoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCTIPOUSOVEHICULO"
    ,schema="MIDAS"
)

public class TipoUsoVehiculoDTO extends CacheableDTO implements java.io.Serializable, Entidad {
	private static final long serialVersionUID = 7412680195863047107L;
	private BigDecimal idTcTipoUsoVehiculo;
	private BigDecimal idTcTipoVehiculo;
    private String codigoTipoUsoVehiculo;
    private String descripcionTipoUsoVehiculo;
    private TipoVehiculoDTO tipoVehiculoDTO = new TipoVehiculoDTO();
    private BigDecimal claveAmisTipoServicio;
    
    // Constructors

    /** default constructor */
    public TipoUsoVehiculoDTO() {
    }

	/** minimal constructor */
    public TipoUsoVehiculoDTO(BigDecimal idTcTipoUsoVehiculo, String codigoTipoUsoVehiculo) {
        this.idTcTipoUsoVehiculo = idTcTipoUsoVehiculo;
        this.codigoTipoUsoVehiculo = codigoTipoUsoVehiculo;
    }
    
    /** full constructor */
    public TipoUsoVehiculoDTO(BigDecimal idTcTipoUsoVehiculo, BigDecimal idTcTipoVehiculo, String codigoTipoUsoVehiculo, String descripcionTipoUsoVehiculo) {
        this.idTcTipoUsoVehiculo = idTcTipoUsoVehiculo;
        this.idTcTipoVehiculo = idTcTipoVehiculo;
        this.codigoTipoUsoVehiculo = codigoTipoUsoVehiculo;
        this.descripcionTipoUsoVehiculo = descripcionTipoUsoVehiculo;
        this.claveAmisTipoServicio = claveAmisTipoServicio;
    }

   
    // Property accessors
    @Id 
    @SequenceGenerator(name = "idTcTipoUsoV_seq_GEN", allocationSize = 1, sequenceName = "MIDAS.idTcTipoUsoVehiculo_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idTcTipoUsoV_seq_GEN")
    @Column(name="IDTCTIPOUSOVEHICULO", unique=true, nullable=false, precision=22, scale=0)

    public BigDecimal getIdTcTipoUsoVehiculo() {
        return this.idTcTipoUsoVehiculo;
    }
    
    public void setIdTcTipoUsoVehiculo(BigDecimal idTcTipoUsoVehiculo) {
        this.idTcTipoUsoVehiculo = idTcTipoUsoVehiculo;
    }
    
    @Column(name="IDTCTIPOVEHICULO", nullable=false, length=5)

    public BigDecimal getIdTcTipoVehiculo() {
        return this.idTcTipoVehiculo;
    }
    
    public void setIdTcTipoVehiculo(BigDecimal idTcTipoVehiculo) {
        this.idTcTipoVehiculo = idTcTipoVehiculo;
    }
    
    @Column(name="CODIGOTIPOUSOVEHICULO", nullable=false, length=5)

    public String getCodigoTipoUsoVehiculo() {
        return this.codigoTipoUsoVehiculo.toUpperCase();
    }
    
    public void setCodigoTipoUsoVehiculo(String codigoTipoUsoVehiculo) {
        this.codigoTipoUsoVehiculo = codigoTipoUsoVehiculo;
    }
    
    @Column(name="DESCRIPCIONTIPOUSOVEHICULO", length=100)

    public String getDescripcionTipoUsoVehiculo() {
        return this.descripcionTipoUsoVehiculo.toUpperCase();
    }
    
    public void setDescripcionTipoUsoVehiculo(String descripcionTipoUsoVehiculo) {
        this.descripcionTipoUsoVehiculo = descripcionTipoUsoVehiculo;
    }
    
    @Transient
    public TipoVehiculoDTO getTipoVehiculoDTO() {
		return tipoVehiculoDTO;
	}

	public void setTipoVehiculoDTO(TipoVehiculoDTO tipoVehiculoDTO) {
		this.tipoVehiculoDTO = tipoVehiculoDTO;
	}

	@Override
	public Object getId() {
		return idTcTipoUsoVehiculo;
	}

	@Override
	public String getDescription() {
		return descripcionTipoUsoVehiculo;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoUsoVehiculoDTO other = (TipoUsoVehiculoDTO) obj;
		if (idTcTipoUsoVehiculo == null) {
			if (other.idTcTipoUsoVehiculo != null)
				return false;
		} else if (!idTcTipoUsoVehiculo.equals(other.idTcTipoUsoVehiculo))
			return false;
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.idTcTipoUsoVehiculo;
	}

	@Override
	public String getValue() {
		return this.descripcionTipoUsoVehiculo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getBusinessKey() {
		return this.idTcTipoUsoVehiculo;
	}

    @Column(name="CVEAMIS_TIPOSERVICIO", nullable=false, length=100)
    public BigDecimal getClaveAmisTipoServicio() {
        return claveAmisTipoServicio;
    }
    
    public void setClaveAmisTipoServicio(BigDecimal claveAmisTipoServicio) {
        this.claveAmisTipoServicio = claveAmisTipoServicio;
    }
}