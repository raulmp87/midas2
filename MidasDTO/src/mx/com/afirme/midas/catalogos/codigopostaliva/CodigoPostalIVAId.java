package mx.com.afirme.midas.catalogos.codigopostaliva;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * CodigoPostalIVADTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class CodigoPostalIVAId implements java.io.Serializable {

    // Fields

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private BigDecimal codigoPostal;
    private String nombreColonia;

    // Constructors

    /** default constructor */
    public CodigoPostalIVAId() {
    }

    /** full constructor */
    public CodigoPostalIVAId(BigDecimal codigoPostal, String nombreColonia) {
	this.codigoPostal = codigoPostal;
	this.nombreColonia = nombreColonia;
    }

    // Property accessors

    @Column(name = "CODIGOPOSTAL", nullable = false, precision = 22, scale = 0)
    public BigDecimal getCodigoPostal() {
	return this.codigoPostal;
    }

    public void setCodigoPostal(BigDecimal codigoPostal) {
	this.codigoPostal = codigoPostal;
    }

    @Column(name = "NOMBRECOLONIA", nullable = false, length = 100)
    public String getNombreColonia() {
	return this.nombreColonia;
    }

    public void setNombreColonia(String nombreColonia) {
	this.nombreColonia = nombreColonia;
    }

    public boolean equals(Object other) {
	if ((this == other))
	    return true;
	if ((other == null))
	    return false;
	if (!(other instanceof CodigoPostalIVAId))
	    return false;
	CodigoPostalIVAId castOther = (CodigoPostalIVAId) other;

	return ((this.getCodigoPostal() == castOther.getCodigoPostal()) || (this
		.getCodigoPostal() != null
		&& castOther.getCodigoPostal() != null && this
		.getCodigoPostal().equals(castOther.getCodigoPostal())))
		&& ((this.getNombreColonia() == castOther.getNombreColonia()) || (this
			.getNombreColonia() != null
			&& castOther.getNombreColonia() != null && this
			.getNombreColonia()
			.equals(castOther.getNombreColonia())));
    }

    public int hashCode() {
	int result = 17;

	result = 37
		* result
		+ (getCodigoPostal() == null ? 0 : this.getCodigoPostal()
			.hashCode());
	result = 37
		* result
		+ (getNombreColonia() == null ? 0 : this.getNombreColonia()
			.hashCode());
	return result;
    }

}