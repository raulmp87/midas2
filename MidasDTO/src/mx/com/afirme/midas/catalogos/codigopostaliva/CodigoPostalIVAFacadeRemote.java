package mx.com.afirme.midas.catalogos.codigopostaliva;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.sistema.SystemException;

/**
 * Remote interface for CodigoPostalIVADTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface CodigoPostalIVAFacadeRemote {
    /**
     * Perform an initial save of a previously unsaved CodigoPostalIVADTO
     * entity. All subsequent persist actions of this entity should use the
     * #update() method.
     * 
     * @param entity
     *            CodigoPostalIVADTO entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(CodigoPostalIVADTO entity);

    /**
     * Delete a persistent CodigoPostalIVADTO entity.
     * 
     * @param entity
     *            CodigoPostalIVADTO entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(CodigoPostalIVADTO entity);

    /**
     * Persist a previously saved CodigoPostalIVADTO entity and return it or a
     * copy of it to the sender. A copy of the CodigoPostalIVADTO entity
     * parameter is returned when the JPA persistence mechanism has not
     * previously been tracking the updated entity.
     * 
     * @param entity
     *            CodigoPostalIVADTO entity to update
     * @return CodigoPostalIVADTO the persisted CodigoPostalIVADTO entity
     *         instance, may not be the same
     * @throws RuntimeException
     *             if the operation fails
     */
    public CodigoPostalIVADTO update(CodigoPostalIVADTO entity);

    public CodigoPostalIVADTO findById(CodigoPostalIVAId id);

    /**
     * Find all CodigoPostalIVADTO entities with a specific property value.
     * 
     * @param propertyName
     *            the name of the CodigoPostalIVADTO property to query
     * @param value
     *            the property value to match
     * @return List<CodigoPostalIVADTO> found by query
     */
    public List<CodigoPostalIVADTO> findByProperty(String propertyName,
	    Object value);

    /**
     * Find all CodigoPostalIVADTO entities.
     * 
     * @return List<CodigoPostalIVADTO> all CodigoPostalIVADTO entities
     */
    public List<CodigoPostalIVADTO> findAll();
   
    public List<CodigoPostalIVADTO> findByCodigoPostal(BigDecimal codigoPostal);
    
    public Double getIVAPorIdCotizacion(BigDecimal idToCotizacion, String nombreUsuario) throws SystemException;
}