package mx.com.afirme.midas.catalogos.codigopostaliva;
// default package

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * CodigoPostalIVADTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCCODIGOPOSTALIVA", schema = "MIDAS")
public class CodigoPostalIVADTO implements java.io.Serializable {

    // Fields

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private CodigoPostalIVAId id;
    private Double valorIVA;

    // Constructors

    /** default constructor */
    public CodigoPostalIVADTO() {
    }

    /** full constructor */
    public CodigoPostalIVADTO(CodigoPostalIVAId id, Double valorIVA) {
	this.id = id;
	this.valorIVA = valorIVA;
    }

    // Property accessors
    @EmbeddedId
    @AttributeOverrides( {
	    @AttributeOverride(name = "codigoPostal", column = @Column(name = "CODIGOPOSTAL", nullable = false, precision = 22, scale = 0)),
	    @AttributeOverride(name = "nombreColonia", column = @Column(name = "NOMBRECOLONIA", nullable = false, length = 100)) })
    public CodigoPostalIVAId getId() {
	return this.id;
    }

    public void setId(CodigoPostalIVAId id) {
	this.id = id;
    }

    @Column(name = "VALORIVA", nullable = false, precision = 6)
    public Double getValorIVA() {
	return this.valorIVA;
    }

    public void setValorIVA(Double valorIVA) {
	this.valorIVA = valorIVA;
    }

}