package mx.com.afirme.midas.catalogos.eventocatastrofico;
// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for EventoCatastroficoDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface EventoCatastroficoFacadeRemote extends MidasInterfaceBase<EventoCatastroficoDTO> {
		/**
	 Perform an initial save of a previously unsaved EventoCatastroficoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity EventoCatastroficoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(EventoCatastroficoDTO entity);
    /**
	 Delete a persistent EventoCatastroficoDTO entity.
	  @param entity EventoCatastroficoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(EventoCatastroficoDTO entity);
   /**
	 Persist a previously saved EventoCatastroficoDTO entity and return it or a copy of it to the sender. 
	 A copy of the EventoCatastroficoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity EventoCatastroficoDTO entity to update
	 @return EventoCatastroficoDTO the persisted EventoCatastroficoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public EventoCatastroficoDTO update(EventoCatastroficoDTO entity);

	 /**
	 * Find all EventoCatastroficoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the EventoCatastroficoDTO property to query
	  @param value the property value to match
	  	  @return List<EventoCatastroficoDTO> found by query
	 */
	public List<EventoCatastroficoDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all EventoCatastroficoDTO entities.
	  	  @return List<EventoCatastroficoDTO> all EventoCatastroficoDTO entities
	 */
	public List<EventoCatastroficoDTO> findAll(
		);	
	
	public List<EventoCatastroficoDTO> listarFiltrado(EventoCatastroficoDTO eventoCatastroficoDTO);
}