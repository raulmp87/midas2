package mx.com.afirme.midas.catalogos.eventocatastrofico;
// default package

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;


/**
 * EventoCatastroficoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCEVENTOCATASTROFICO"
    ,schema="MIDAS"
)
public class EventoCatastroficoDTO extends CacheableDTO implements java.io.Serializable {


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields    

     private BigDecimal idTcEventoCatastrofico;
     private BigDecimal codigoEvento;
     private String descripcionEvento;
//     private Set<ReporteSiniestroDTO> reporteSiniestroDTOs = new HashSet<ReporteSiniestroDTO>(0);


    // Constructors

    /** default constructor */
    public EventoCatastroficoDTO() {
    }

   
    // Property accessors
    @Id 
    @SequenceGenerator(name = "IDTCEVENTOCATASTROFICO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCEVENTOCATASTROFICO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCEVENTOCATASTROFICO_SEQ_GENERADOR")
    @Column(name="IDTCEVENTOCATASTROFICO", unique=true, nullable=false, precision=22, scale=0)
    public BigDecimal getIdTcEventoCatastrofico() {
        return this.idTcEventoCatastrofico;
    }
    
    public void setIdTcEventoCatastrofico(BigDecimal idTcEventoCatastrofico) {
        this.idTcEventoCatastrofico = idTcEventoCatastrofico;
    }
    
    @Column(name="CODIGOEVENTO", nullable=false, precision=22, scale=0)
    public BigDecimal getCodigoEvento() {
        return this.codigoEvento;
    }
    
    public void setCodigoEvento(BigDecimal codigoEvento) {
        this.codigoEvento = codigoEvento;
    }
    
    @Column(name="DESCRIPCIONEVENTO", nullable=false, length=200)

    public String getDescripcionEvento() {
        return this.descripcionEvento;
    }
    
    public void setDescripcionEvento(String descripcionEvento) {
        this.descripcionEvento = descripcionEvento;
    }
    
//    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="eventoCatastrofico")
//    public Set<ReporteSiniestroDTO> getReporteSiniestroDTOs() {
//        return this.reporteSiniestroDTOs;
//    }
//    
//    public void setReporteSiniestroDTOs(Set<ReporteSiniestroDTO> reporteSiniestroDTOs) {
//        this.reporteSiniestroDTOs = reporteSiniestroDTOs;
//    }

    @Override
	public BigDecimal getId() {
		return this.idTcEventoCatastrofico;
	}
	@Override
	public String getDescription() {
		return this.descripcionEvento;
	}
	
	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof EventoCatastroficoDTO) {
			EventoCatastroficoDTO eventoCatastroficoDTO = (EventoCatastroficoDTO) object;
			equal = eventoCatastroficoDTO.getIdTcEventoCatastrofico().equals(this.getIdTcEventoCatastrofico());
		} // End of if
		return equal;
	}

}