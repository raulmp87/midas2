package mx.com.afirme.midas.catalogos.modelovehiculo;
// default package

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas2.annotation.Exportable;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.CotizacionExpressPaso1Checks;


/**
 * ModeloVehiculoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCMODELOVEHICULO"
    ,schema="MIDAS"
)

public class ModeloVehiculoDTO  implements java.io.Serializable, Entidad {

	private static final long serialVersionUID = -5111813582029341728L;
	private ModeloVehiculoId id=new ModeloVehiculoId();
     private Integer valorNuevo;
     private Integer valorComercial;
     private BigDecimal valorCaratula;
     private String claveCondRiesgo;
     private Date fechaCreacion;
     private String codigoUsuarioCreacion;

     private EstiloVehiculoDTO estiloVehiculoDTO;

     private MonedaDTO monedaDTO;
    // Constructors

    /** default constructor */
    public ModeloVehiculoDTO() {
    }

	/** minimal constructor */
    public ModeloVehiculoDTO(ModeloVehiculoId id, Date fechaCreacion, String codigoUsuarioCreacion) {
        this.id = id;
        this.fechaCreacion = fechaCreacion;
        this.codigoUsuarioCreacion = codigoUsuarioCreacion;
    }
    
    /** full constructor */
    public ModeloVehiculoDTO(ModeloVehiculoId id, Integer valorNuevo, Integer valorComercial, String claveCondRiesgo, Date fechaCreacion, String codigoUsuarioCreacion) {
        this.id = id;
        this.valorNuevo = valorNuevo;
        this.valorComercial = valorComercial;
        this.claveCondRiesgo = claveCondRiesgo;
        this.fechaCreacion = fechaCreacion;
        this.codigoUsuarioCreacion = codigoUsuarioCreacion;
    }

   
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="claveTipoBien", column=@Column(name="CLAVETIPOBIEN", nullable=false, length=5) ), 
        @AttributeOverride(name="claveEstilo", column=@Column(name="CLAVEESTILO", nullable=false, length=8) ), 
        @AttributeOverride(name="modeloVehiculo", column=@Column(name="MODELOVEHICULO", nullable=false, precision=4, scale=0) ), 
        @AttributeOverride(name="idMoneda", column=@Column(name="IDMONEDA", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idVersionCarga", column=@Column(name="IDVERSIONCARGA", nullable=false, precision=22, scale=0) ) } )

    @NotNull(groups=CotizacionExpressPaso1Checks.class, message="{com.afirme.midas2.requerido}")
    @Valid
    public ModeloVehiculoId getId() {
        return this.id;
    }
    
    public void setId(ModeloVehiculoId id) {
        this.id = id;
    }
    
    @Column(name="VALORNUEVO", precision=8, scale=0)
    public Integer getValorNuevo() {
        return this.valorNuevo;
    }
    
    public void setValorNuevo(Integer valorNuevo) {
        this.valorNuevo = valorNuevo;
    }
    
    @Column(name="VALORCOMERCIAL", precision=8, scale=0)
    public Integer getValorComercial() {
        return this.valorComercial;
    }
    
    public void setValorComercial(Integer valorComercial) {
        this.valorComercial = valorComercial;
    }
    
    @Column(name="CLAVECONDRIESGO", length=4)

    public String getClaveCondRiesgo() {
        return this.claveCondRiesgo!=null?this.claveCondRiesgo.toUpperCase():this.claveCondRiesgo;
    }
    
    public void setClaveCondRiesgo(String claveCondRiesgo) {
        this.claveCondRiesgo = claveCondRiesgo;
    }
    
    @Temporal(TemporalType.DATE)
	@Column(name = "FECHACREACION", length = 7)
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }
    
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    
    @Column(name="CODIGOUSUARIOCREACION", nullable=false, length=8)
    public String getCodigoUsuarioCreacion() {
        return this.codigoUsuarioCreacion!=null?this.codigoUsuarioCreacion.toUpperCase():this.codigoUsuarioCreacion;
    }
    
    public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
        this.codigoUsuarioCreacion = codigoUsuarioCreacion;
    }

    @Transient
	public EstiloVehiculoDTO getEstiloVehiculoDTO() {
		return estiloVehiculoDTO;
	}

	public void setEstiloVehiculoDTO(EstiloVehiculoDTO estiloVehiculoDTO) {
		this.estiloVehiculoDTO = estiloVehiculoDTO;
	}

	@Transient
	public MonedaDTO getMonedaDTO() {
		return monedaDTO;
	}

	public void setMonedaDTO(MonedaDTO monedaDTO) {
		this.monedaDTO = monedaDTO;
	}

    @Column(name="VALORCARATULA")
    @Exportable(columnName="Valor_Caratula", columnOrder=5)
	public BigDecimal getValorCaratula() {
		return valorCaratula;
	}

	public void setValorCaratula(BigDecimal valorCaratula) {
		this.valorCaratula = valorCaratula;
	}

    @Transient
	@Exportable(columnName="Clave_Tipo_Bien", columnOrder=1)
    public String getClaveTipoBien() {
        return this.id.getClaveTipoBien();
    }

    @Transient
    @Exportable(columnName="Clave_Estilo", columnOrder=2)
    public String getClaveEstilo() {
        return this.id.getClaveEstilo();
    }

    @Transient
	@Exportable(columnName="Modelo", columnOrder=3)
    public Short getModeloVehiculo() {
        return this.id.getModeloVehiculo();
    }

    @Transient
	@Exportable(columnName="Moneda", columnOrder=4)
    public BigDecimal getIdMoneda() {
        return this.id.getIdMoneda();
    }

    @Transient
	@Exportable(columnName="Version_Carga", columnOrder=0)
    public BigDecimal getIdVersionCarga() {
        return this.id.getIdVersionCarga();
    }

	@SuppressWarnings("unchecked")
	@Override
	public ModeloVehiculoId getKey() {
		return id;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ModeloVehiculoId getBusinessKey() {
		return id;
	}


	@Override
	public String getValue() {
		return null;
	}
}