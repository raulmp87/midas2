package mx.com.afirme.midas.catalogos.modelovehiculo;
// default package

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.tipovehiculo.SeccionTipoVehiculoDTO;

/**
 * Remote interface for ModeloVehiculoDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ModeloVehiculoFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ModeloVehiculoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ModeloVehiculoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ModeloVehiculoDTO entity);
    /**
	 Delete a persistent ModeloVehiculoDTO entity.
	  @param entity ModeloVehiculoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ModeloVehiculoDTO entity);
   /**
	 Persist a previously saved ModeloVehiculoDTO entity and return it or a copy of it to the sender. 
	 A copy of the ModeloVehiculoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ModeloVehiculoDTO entity to update
	 @return ModeloVehiculoDTO the persisted ModeloVehiculoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ModeloVehiculoDTO update(ModeloVehiculoDTO entity);
	public ModeloVehiculoDTO findById( ModeloVehiculoId id);
	 /**
	 * Find all ModeloVehiculoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ModeloVehiculoDTO property to query
	  @param value the property value to match
	  	  @return List<ModeloVehiculoDTO> found by query
	 */
	public List<ModeloVehiculoDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ModeloVehiculoDTO entities.
	  	  @return List<ModeloVehiculoDTO> all ModeloVehiculoDTO entities
	 */
	public List<ModeloVehiculoDTO> findAll();
	
	public List<ModeloVehiculoDTO> listarFiltrado(ModeloVehiculoDTO modeloVehiculoDTO);
	
	public List<ModeloVehiculoDTO> buscarPorMarcaMonedaSeccionVersionCarga(BigDecimal idMoneda, 
			BigDecimal idMarca, String claveTipoBien, Long idVersionCarga, List<SeccionTipoVehiculoDTO> seccionTipoVehiculoList);
	
	public String actualizarMasivo(String idToControlArchivo);
	
	
}