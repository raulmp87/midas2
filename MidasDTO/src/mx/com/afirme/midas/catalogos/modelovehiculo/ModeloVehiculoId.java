package mx.com.afirme.midas.catalogos.modelovehiculo;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.validator.group.CotizacionExpressPaso1Checks;


/**
 * ModeloVehiculoDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class ModeloVehiculoId  implements java.io.Serializable {
	private static final long serialVersionUID = -5894243908968648526L;
	private String claveTipoBien;
     private String claveEstilo;
     private Short modeloVehiculo;
     private BigDecimal idMoneda;
     private BigDecimal idVersionCarga;


    // Constructors

    /** default constructor */
    public ModeloVehiculoId() {
    }

    
    /** full constructor */
    public ModeloVehiculoId(String claveTipoBien, String claveEstilo, Short modeloVehiculo, BigDecimal idMoneda, BigDecimal idVersionCarga) {
        this.claveTipoBien = claveTipoBien;
        this.claveEstilo = claveEstilo;
        this.modeloVehiculo = modeloVehiculo;
        this.idMoneda = idMoneda;
        this.idVersionCarga = idVersionCarga;
    }

   
    // Property accessors

    @Column(name="CLAVETIPOBIEN", nullable=false, length=5)

    public String getClaveTipoBien() {
        return this.claveTipoBien!=null?this.claveTipoBien:this.claveTipoBien;
    }
    
    public void setClaveTipoBien(String claveTipoBien) {
        this.claveTipoBien = claveTipoBien;
    }

    @Column(name="CLAVEESTILO", nullable=false, length=8)

    public String getClaveEstilo() {
        return this.claveEstilo!=null?this.claveEstilo.toUpperCase():this.claveEstilo;
    }
    
    public void setClaveEstilo(String claveEstilo) {
        this.claveEstilo = claveEstilo;
    }

    @Column(name="MODELOVEHICULO", nullable=false, precision=4, scale=0)
    @NotNull(groups=CotizacionExpressPaso1Checks.class, message="{com.afirme.midas2.requerido}")
    public Short getModeloVehiculo() {
        return this.modeloVehiculo;
    }
    
    public void setModeloVehiculo(Short modeloVehiculo) {
        this.modeloVehiculo = modeloVehiculo;
    }

    @Column(name="IDMONEDA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdMoneda() {
        return this.idMoneda;
    }
    
    public void setIdMoneda(BigDecimal idMoneda) {
        this.idMoneda = idMoneda;
    }

    @Column(name="IDVERSIONCARGA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdVersionCarga() {
        return this.idVersionCarga;
    }
    
    public void setIdVersionCarga(BigDecimal idVersionCarga) {
        this.idVersionCarga = idVersionCarga;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof ModeloVehiculoId) ) return false;
		 ModeloVehiculoId castOther = ( ModeloVehiculoId ) other; 
         
		 return ( (this.getClaveTipoBien()==castOther.getClaveTipoBien()) || ( this.getClaveTipoBien()!=null && castOther.getClaveTipoBien()!=null && this.getClaveTipoBien().equals(castOther.getClaveTipoBien()) ) )
 && ( (this.getClaveEstilo()==castOther.getClaveEstilo()) || ( this.getClaveEstilo()!=null && castOther.getClaveEstilo()!=null && this.getClaveEstilo().equals(castOther.getClaveEstilo()) ) )
 && ( (this.getModeloVehiculo()==castOther.getModeloVehiculo()) || ( this.getModeloVehiculo()!=null && castOther.getModeloVehiculo()!=null && this.getModeloVehiculo().equals(castOther.getModeloVehiculo()) ) )
 && ( (this.getIdMoneda()==castOther.getIdMoneda()) || ( this.getIdMoneda()!=null && castOther.getIdMoneda()!=null && this.getIdMoneda().equals(castOther.getIdMoneda()) ) )
 && ( (this.getIdVersionCarga()==castOther.getIdVersionCarga()) || ( this.getIdVersionCarga()!=null && castOther.getIdVersionCarga()!=null && this.getIdVersionCarga().equals(castOther.getIdVersionCarga()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getClaveTipoBien() == null ? 0 : this.getClaveTipoBien().hashCode() );
         result = 37 * result + ( getClaveEstilo() == null ? 0 : this.getClaveEstilo().hashCode() );
         result = 37 * result + ( getModeloVehiculo() == null ? 0 : this.getModeloVehiculo().hashCode() );
         result = 37 * result + ( getIdMoneda() == null ? 0 : this.getIdMoneda().hashCode() );
         result = 37 * result + ( getIdVersionCarga() == null ? 0 : this.getIdVersionCarga().hashCode() );
         return result;
   }   





}