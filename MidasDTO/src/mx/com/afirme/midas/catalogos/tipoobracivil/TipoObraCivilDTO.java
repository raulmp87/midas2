package mx.com.afirme.midas.catalogos.tipoobracivil;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

import mx.com.afirme.midas.catalogos.SubTipoGenerico;
import mx.com.afirme.midas.catalogos.giro.GiroDTO;

/**
 * TipoObraCivilDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCTIPOOBRACIVIL", schema = "MIDAS")
@Cache(
  type=CacheType.SOFT,
  size=1000,
  expiry=36000000
)
public class TipoObraCivilDTO extends SubTipoGenerico{

	// Fields

	/**
	 * @author Christian Ceballos
	 * @since 28/07/09
	 */
	private static final long serialVersionUID = 5052865533400148565L;
	private BigDecimal idTipoObraCivil;
	private BigDecimal codigoTipoObraCivil;
	private String descripcionTipoObraCivil;
	private Short claveAutorizacion;
	private BigDecimal idPeriodoNormalConstruccion;

	/** default constructor */
	public TipoObraCivilDTO() {
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTCTIPOOBRACIVIL_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCTIPOOBRACIVIL_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCTIPOOBRACIVIL_SEQ_GENERADOR")
	@Column(name = "IDTCTIPOOBRACIVIL", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTipoObraCivil() {
		return idTipoObraCivil;
	}

	public void setIdTipoObraCivil(BigDecimal idTipoObraCivil) {
		this.idTipoObraCivil = idTipoObraCivil;
	}

	@Column(name = "CODIGOTIPOOBRACIVIL", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoTipoObraCivil() {
		return codigoTipoObraCivil;
	}

	public void setCodigoTipoObraCivil(BigDecimal codigoTipoObraCivil) {
		this.codigoTipoObraCivil = codigoTipoObraCivil;
	}

	@Column(name = "DESCRIPCIONTIPOOBRACIVIL", nullable = false, length = 200)
	public String getDescripcionTipoObraCivil() {
		return descripcionTipoObraCivil;
	}

	public void setDescripcionTipoObraCivil(String descripcionTipoObraCivil) {
		this.descripcionTipoObraCivil = descripcionTipoObraCivil;
	}

	@Column(name = "CLAVEAUTORIZACION", nullable = false, precision = 4, scale = 0)
	public Short getClaveAutorizacion() {
		return claveAutorizacion;
	}

	public void setClaveAutorizacion(Short claveAutorizacion) {
		this.claveAutorizacion = claveAutorizacion;
	}

	@Override
	public String getDescription() {
		return this.descripcionTipoObraCivil;
	}

	@Override
	public Object getId() {
		return this.idTipoObraCivil;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof GiroDTO) {
			TipoObraCivilDTO tipoObraCivilDTO = (TipoObraCivilDTO) object;
			equal = tipoObraCivilDTO.getIdTipoObraCivil().equals(
					this.idTipoObraCivil);
		} // End of if
		return equal;
	}

	@Column(name = "IDPERIODONORMALCONSTRUCCION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdPeriodoNormalConstruccion() {
		return idPeriodoNormalConstruccion;
	}

	public void setIdPeriodoNormalConstruccion(
			BigDecimal idPeriodoNormalConstruccion) {
		this.idPeriodoNormalConstruccion = idPeriodoNormalConstruccion;
	}
}