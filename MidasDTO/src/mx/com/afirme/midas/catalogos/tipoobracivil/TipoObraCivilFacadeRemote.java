package mx.com.afirme.midas.catalogos.tipoobracivil;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for TipoObraCivilFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface TipoObraCivilFacadeRemote extends
		MidasInterfaceBase<TipoObraCivilDTO> {
	/**
	 * Perform an initial save of a previously unsaved TipoObraCivilDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            TipoObraCivilDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoObraCivilDTO entity);

	/**
	 * Delete a persistent TipoObraCivilDTO entity.
	 * 
	 * @param entity
	 *            TipoObraCivilDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoObraCivilDTO entity);

	/**
	 * Persist a previously saved TipoObraCivilDTO entity and return it or a
	 * copy of it to the sender. A copy of the TipoObraCivilDTO entity parameter
	 * is returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoObraCivilDTO entity to update
	 * @return TipoObraCivilDTO the persisted TipoObraCivilDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoObraCivilDTO update(TipoObraCivilDTO entity);

	/**
	 * Find all TipoObraCivilDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the TipoObraCivilDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoObraCivilDTO> found by query
	 */
	public List<TipoObraCivilDTO> findByProperty(String propertyName,
			Object value);

	public List<TipoObraCivilDTO> listarFiltrado(
			TipoObraCivilDTO tipoObraCivilDTO);
}