package mx.com.afirme.midas.catalogos.estadocivil;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import mx.com.afirme.midas.base.CacheableDTO;
/**
 * Entidad para el catalogo de Estado Civil
 * @author vmhersil
 *
 */
@Entity(name="EstadoCivilDTO")
@Table(name="VW_ESTADO_CIVIL",schema="MIDAS")
public class EstadoCivilDTO extends CacheableDTO implements Comparable<EstadoCivilDTO>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String idEstadoCivil;
	private String nombreEstadoCivil;
	
	@Id
	@Column(name="idEstadoCivil",nullable=false,length=2)
	public String getIdEstadoCivil() {
		return idEstadoCivil;
	}

	public void setIdEstadoCivil(String idEstadoCivil) {
		this.idEstadoCivil = idEstadoCivil;
	}
	
	@Column(name="nombreEstadoCivil",nullable=false,length=20)
	public String getNombreEstadoCivil() {
		return nombreEstadoCivil;
	}

	public void setNombreEstadoCivil(String nombreEstadoCivil) {
		this.nombreEstadoCivil = nombreEstadoCivil;
	}

	@Override
	public int compareTo(EstadoCivilDTO o) {
		return this.idEstadoCivil.compareTo(o.idEstadoCivil);
	}

	@Override
	public Object getId() {
		return this.idEstadoCivil;
	}

	@Override
	public String getDescription() {
		return this.nombreEstadoCivil;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal=(object==this);
		if(!equal && (object instanceof EstadoCivilDTO)){
			EstadoCivilDTO instance=(EstadoCivilDTO)object;
			equal=instance.getIdEstadoCivil().equals(this.idEstadoCivil);
		}
		return equal;
	}

}
