package mx.com.afirme.midas.catalogos.estadocivil;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;
/**
 * 
 * @author vmhersil
 *
 */

public interface EstadoCivilFacadeRemote extends MidasInterfaceBase<EstadoCivilDTO>{
	public EstadoCivilDTO findById(String idEstadoCivil);
	
	public EstadoCivilDTO findByName(String nombreEstadoCivil);
}
