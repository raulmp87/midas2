package mx.com.afirme.midas.catalogos.giro;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.catalogos.ramo.RamoDTO;

/**
 * SubGiroDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCSUBGIRO", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = {
		"IDTCGIRO", "CODIGOSUBGIRO" }))
@Cache(
  type=CacheType.SOFT,
  size=1000,
  expiry=36000000
)
public class SubGiroDTO extends CacheableDTO {
	private static final long serialVersionUID = 4826843560937185572L;
	
	private BigDecimal idTcSubGiro;
	private GiroDTO giroDTO;
	private BigDecimal codigoSubGiro;
	private String descripcionSubGiro;
	private BigDecimal idGrupoRobo;
	private BigDecimal idGrupoDineroValores;
	private BigDecimal idGrupoCristales;
	private BigDecimal idGrupoPlenos;
	private Short claveInspeccion;

	public SubGiroDTO() {
	}

	@Id
	@SequenceGenerator(name = "IDTCSUBGIRO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCSUBGIRO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCSUBGIRO_SEQ_GENERADOR")
	@Column(name = "IDTCSUBGIRO", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcSubGiro() {
		return this.idTcSubGiro;
	}

	public void setIdTcSubGiro(BigDecimal idTcSubGiro) {
		this.idTcSubGiro = idTcSubGiro;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCGIRO", nullable = false)
	public GiroDTO getGiroDTO() {
		return giroDTO;
	}

	public void setGiroDTO(GiroDTO giroDTO) {
		this.giroDTO = giroDTO;
	}

	@Column(name = "CODIGOSUBGIRO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoSubGiro() {
		return this.codigoSubGiro;
	}

	public void setCodigoSubGiro(BigDecimal codigoSubGiro) {
		this.codigoSubGiro = codigoSubGiro;
	}

	@Column(name = "DESCRIPCIONSUBGIRO", nullable = false, length = 200)
	public String getDescripcionSubGiro() {
		return this.descripcionSubGiro;
	}

	public void setDescripcionSubGiro(String descripcionSubGiro) {
		this.descripcionSubGiro = descripcionSubGiro;
	}

	@Column(name = "IDGRUPOROBO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdGrupoRobo() {
		return this.idGrupoRobo;
	}

	public void setIdGrupoRobo(BigDecimal idGrupoRobo) {
		this.idGrupoRobo = idGrupoRobo;
	}

	@Column(name = "IDGRUPODINEROVALORES", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdGrupoDineroValores() {
		return this.idGrupoDineroValores;
	}

	public void setIdGrupoDineroValores(BigDecimal idGrupoDineroValores) {
		this.idGrupoDineroValores = idGrupoDineroValores;
	}

	@Column(name = "IDGRUPOCRISTALES", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdGrupoCristales() {
		return this.idGrupoCristales;
	}

	public void setIdGrupoCristales(BigDecimal idGrupoCristales) {
		this.idGrupoCristales = idGrupoCristales;
	}

	@Column(name = "IDGRUPOPLENOS", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdGrupoPlenos() {
		return this.idGrupoPlenos;
	}

	public void setIdGrupoPlenos(BigDecimal idGrupoPlenos) {
		this.idGrupoPlenos = idGrupoPlenos;
	}

	@Column(name = "CLAVEINSPECCION", nullable = false, precision = 4, scale = 0)
	public Short getClaveInspeccion() {
		return this.claveInspeccion;
	}

	public void setClaveInspeccion(Short claveInspeccion) {
		this.claveInspeccion = claveInspeccion;
	}

	@Override
	public String getDescription() {
		return this.descripcionSubGiro;
	}

	@Override
	public Object getId() {
		return this.idTcSubGiro;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof RamoDTO) {
			SubGiroDTO subGiroDTO = (SubGiroDTO) object;
			equal = subGiroDTO.getIdTcSubGiro().equals(this.idTcSubGiro);
		} // End of if
		return equal;
	}
}