package mx.com.afirme.midas.catalogos.giro;

// default package

import java.util.List;

import javax.ejb.Remote;
import javax.persistence.EntityTransaction;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for GiroFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface GiroFacadeRemote extends MidasInterfaceBase<GiroDTO> {
	/**
	 * Perform an initial save of a previously unsaved GiroDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            GiroDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(GiroDTO entity);

	/**
	 * Delete a persistent GiroDTO entity.
	 * 
	 * @param entity
	 *            GiroDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(GiroDTO entity);

	/**
	 * Persist a previously saved GiroDTO entity and return it or a copy of it
	 * to the sender. A copy of the GiroDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            GiroDTO entity to update
	 * @return GiroDTO the persisted GiroDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public GiroDTO update(GiroDTO entity);


	/**
	 * Find all GiroDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the GiroDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<GiroDTO> found by query
	 */
	public List<GiroDTO> findByProperty(String propertyName, Object value);

	public List<GiroDTO> findByDescripcionGiro(Object descripcionGiro);

	public List<GiroDTO> listarFiltrado(GiroDTO giroDTO);

	public EntityTransaction getTransaction();

}