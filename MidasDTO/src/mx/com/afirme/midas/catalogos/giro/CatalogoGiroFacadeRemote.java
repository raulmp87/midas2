package mx.com.afirme.midas.catalogos.giro;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;
/**
 * Interfaz para catalogo de giro
 * @author vmhersil
 *
 */

public interface CatalogoGiroFacadeRemote  extends MidasInterfaceBase<CatalogoGiroDTO> {
	
	public CatalogoGiroDTO findById(Integer idGiro);
	
	public CatalogoGiroDTO findByName(String nombreGiro);
}
