package mx.com.afirme.midas.catalogos.giro;
// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for SubGiroFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SubGiroFacadeRemote extends MidasInterfaceBase<SubGiroDTO> {
	/**
	 * Perform an initial save of a previously unsaved SubGiroDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SubGiroDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SubGiroDTO entity);

	/**
	 * Delete a persistent SubGiroDTO entity.
	 * 
	 * @param entity
	 *            SubGiroDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SubGiroDTO entity);

	/**
	 * Persist a previously saved SubGiroDTO entity and return it or a copy of it
	 * to the sender. A copy of the SubGiroDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            SubGiroDTO entity to update
	 * @return SubGiroDTO the persisted SubGiroDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SubGiroDTO update(SubGiroDTO entity);


	/**
	 * Find all SubGiroDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SubGiroDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SubGiroDTO> found by query
	 */
	public List<SubGiroDTO> findByProperty(String propertyName, Object value);

	public List<SubGiroDTO> findByDescripcionSubGiro(Object descripcionSubGiro);

	public List<SubGiroDTO> findByClaveInspeccion(Object claveInspeccion);

	/**
	 * Find all SubGiroDTO entities.
	 * 
	 * @return List<SubGiroDTO> all SubGiroDTO entities
	 */
	public List<SubGiroDTO> findAll();
	
	public List<SubGiroDTO> listarFiltrado(SubGiroDTO subGiroDTO);
}