package mx.com.afirme.midas.catalogos.giro;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;
/**
 * Entidad para el catalogo de Giro
 * @author vmhersil
 *
 */
@Entity(name="CatalogoGiroDTO")
@Table(name="VW_GIRO",schema="MIDAS")
public class CatalogoGiroDTO extends CacheableDTO implements Comparable<CatalogoGiroDTO>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idGiro;
	private String nombreGiro;
	
	@Id
	@Column(name="idGiro",nullable=false,length=8)
	public Integer getIdGiro() {
		return idGiro;
	}

	public void setIdGiro(Integer idGiro) {
		this.idGiro = idGiro;
	}

	@Column(name="nombreGiro",nullable=false,length=150)
	public String getNombreGiro() {
		return nombreGiro;
	}

	public void setNombreGiro(String nombreGiro) {
		this.nombreGiro = nombreGiro;
	}

	@Override
	public int compareTo(CatalogoGiroDTO o) {
		return this.idGiro.compareTo(o.getIdGiro());
	}

	@Override
	public Object getId() {
		return this.idGiro;
	}

	@Override
	public String getDescription() {
		return this.nombreGiro;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal=(object==this);
		if(!equal && (object instanceof CatalogoGiroDTO)){
			CatalogoGiroDTO instance=(CatalogoGiroDTO)object;
			equal=instance.getIdGiro().equals(this.idGiro);
		}
		return equal;
	}

}
