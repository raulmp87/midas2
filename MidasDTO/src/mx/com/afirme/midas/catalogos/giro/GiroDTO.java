package mx.com.afirme.midas.catalogos.giro;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.base.CacheableDTO;

/**
 * GiroDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCGIRO", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = "CODIGOGIRO"))
public class GiroDTO extends CacheableDTO {
	private static final long serialVersionUID = -5161740158301782616L;
	private BigDecimal idTcGiro;
	private BigDecimal codigoGiro;
	private String descripcionGiro;
	private List<SubGiroDTO> subGiroDTOs = new ArrayList<SubGiroDTO>();

	// Constructors

	/** default constructor */
	public GiroDTO() {
	}

	/** minimal constructor */
	public GiroDTO(BigDecimal idTcGiro, BigDecimal codigoGiro,
			String descripcionGiro) {
		this.idTcGiro = idTcGiro;
		this.codigoGiro = codigoGiro;
		this.descripcionGiro = descripcionGiro;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTCGIRO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCGIRO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCGIRO_SEQ_GENERADOR")	
	@Column(name = "idTcGiro", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcGiro() {
		return this.idTcGiro;
	}

	public void setIdTcGiro(BigDecimal idTcGiro) {
		this.idTcGiro = idTcGiro;
	}

	@Column(name = "CODIGOGIRO", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoGiro() {
		return this.codigoGiro;
	}

	public void setCodigoGiro(BigDecimal codigoGiro) {
		this.codigoGiro = codigoGiro;
	}

	@Column(name = "DESCRIPCIONGIRO", nullable = false, length = 200)
	public String getDescripcionGiro() {
		return this.descripcionGiro;
	}

	public void setDescripcionGiro(String descripcionGiro) {
		this.descripcionGiro = descripcionGiro;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "giroDTO")
	public List<SubGiroDTO> getSubGiroDTOs() {
		return subGiroDTOs;
	}

	public void setSubGiroDTOs(List<SubGiroDTO> subGiroDTOs) {
		this.subGiroDTOs = subGiroDTOs;
	}

	@Override
	public String getDescription() {
		return this.descripcionGiro;
	}

	@Override
	public Object getId() {
		return this.idTcGiro;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof GiroDTO) {
			GiroDTO giroDTO = (GiroDTO) object;
			equal = giroDTO.getIdTcGiro().equals(this.idTcGiro);
		} // End of if
		return equal;
	}

}