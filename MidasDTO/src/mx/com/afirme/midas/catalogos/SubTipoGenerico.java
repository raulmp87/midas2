package mx.com.afirme.midas.catalogos;

import java.io.Serializable;

import mx.com.afirme.midas.base.CacheableDTO;

public abstract class SubTipoGenerico extends CacheableDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	public abstract Short getClaveAutorizacion();
}
