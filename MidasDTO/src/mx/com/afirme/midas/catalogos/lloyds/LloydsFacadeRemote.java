package mx.com.afirme.midas.catalogos.lloyds;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.catalogos.lloyds.LloydsDTO;

/**
 * Remote interface for LloydsDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface LloydsFacadeRemote {
	public LloydsDTO findById( BigDecimal id);
	 /**
	 * Find all LloydsDTO entities with a specific property value.  
	 
	  @param propertyName the name of the LloydsDTO property to query
	  @param value the property value to match
	  	  @return List<LloydsDTO> found by query
	 */
	public List<LloydsDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all LloydsDTO entities.
	  	  @return List<LloydsDTO> all LloydsDTO entities
	 */
	public List<LloydsDTO> findAll(
		);
	
	public List<LloydsDTO> findAllOrderedBy(String...fieldNames);
}