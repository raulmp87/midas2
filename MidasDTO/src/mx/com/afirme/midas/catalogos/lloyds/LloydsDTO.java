package mx.com.afirme.midas.catalogos.lloyds;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * LloydsDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCLLOYDS"
    ,schema="MIDAS"
)

public class LloydsDTO  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	 private static final long serialVersionUID = 1L;
	 private BigDecimal idTcLloyds;
     private double porcentajePrimerRiesgoAbsoluto;
     private double porcentajeFactor;
     private double porcentajeRecargoEquivalente;
     private double porcentajeAhorro;
     private double porcentajeAhorroVsRiesgoA;


    // Constructors

    /** default constructor */
    public LloydsDTO() {
    }
   
    // Property accessors
    @Id 
    
    @Column(name="IDTCLLOYDS", nullable=false, precision=22, scale=0)

    public BigDecimal getIdTcLloyds() {
        return this.idTcLloyds;
    }
    
    public void setIdTcLloyds(BigDecimal idTcLloyds) {
        this.idTcLloyds = idTcLloyds;
    }
    
    @Column(name="PORCENTAJEPRIMERRIESGOABSOLUTO", nullable=false, precision=8, scale=4)

    public double getPorcentajePrimerRiesgoAbsoluto() {
        return this.porcentajePrimerRiesgoAbsoluto;
    }
    
    public void setPorcentajePrimerRiesgoAbsoluto(double porcentajePrimerRiesgoAbsoluto) {
        this.porcentajePrimerRiesgoAbsoluto = porcentajePrimerRiesgoAbsoluto;
    }
    
    @Column(name="PORCENTAJEFACTOR", nullable=false, precision=8, scale=4)

    public double getPorcentajeFactor() {
        return this.porcentajeFactor;
    }
    
    public void setPorcentajeFactor(double porcentajeFactor) {
        this.porcentajeFactor = porcentajeFactor;
    }
    
    @Column(name="PORCENTAJERECARGOEQUIVALENTE", nullable=false, precision=8, scale=4)

    public double getPorcentajeRecargoEquivalente() {
        return this.porcentajeRecargoEquivalente;
    }
    
    public void setPorcentajeRecargoEquivalente(double porcentajeRecargoEquivalente) {
        this.porcentajeRecargoEquivalente = porcentajeRecargoEquivalente;
    }
    
    @Column(name="PORCENTAJEAHORRO", nullable=false, precision=8, scale=4)

    public double getPorcentajeAhorro() {
        return this.porcentajeAhorro;
    }
    
    public void setPorcentajeAhorro(double porcentajeAhorro) {
        this.porcentajeAhorro = porcentajeAhorro;
    }
    
    @Column(name="PORCENTAJEAHORROVSRIESGOA", nullable=false, precision=8, scale=4)

    public double getPorcentajeAhorroVsRiesgoA() {
        return this.porcentajeAhorroVsRiesgoA;
    }
    
    public void setPorcentajeAhorroVsRiesgoA(double porcentajeAhorroVsRiesgoA) {
        this.porcentajeAhorroVsRiesgoA = porcentajeAhorroVsRiesgoA;
    }
   








}