package mx.com.afirme.midas.catalogos.tiponavegacion;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.catalogos.tipobandera.TipoBanderaDTO;

/**
 * TipoNavegacionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCTIPONAVEGACION", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = "CODIGOTIPONAVEGACION"))
public class TipoNavegacionDTO extends CacheableDTO implements java.io.Serializable {
	private static final long serialVersionUID = -2593021066160550746L;
	// Fields

	private BigDecimal idTcTipoNavegacion;
	private BigDecimal codigoTipoNavegacion;
	private String descripcionTipoNavegacion;

	// Constructors

	/** default constructor */
	public TipoNavegacionDTO() {
	}

	// Property accessors
	@Id
	@Column(name = "IDTCTIPONAVEGACION", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcTipoNavegacion() {
		return this.idTcTipoNavegacion;
	}

	public void setIdTcTipoNavegacion(BigDecimal idTcTipoNavegacion) {
		this.idTcTipoNavegacion = idTcTipoNavegacion;
	}

	@Column(name = "CODIGOTIPONAVEGACION", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoTipoNavegacion() {
		return this.codigoTipoNavegacion;
	}

	public void setCodigoTipoNavegacion(BigDecimal codigoTipoNavegacion) {
		this.codigoTipoNavegacion = codigoTipoNavegacion;
	}

	@Column(name = "DESCRIPCIONTIPONAVEGACION", nullable = false, length = 200)
	public String getDescripcionTipoNavegacion() {
		return this.descripcionTipoNavegacion;
	}

	public void setDescripcionTipoNavegacion(String descripcionTipoNavegacion) {
		this.descripcionTipoNavegacion = descripcionTipoNavegacion;
	}
	
	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof TipoBanderaDTO) {
			TipoNavegacionDTO tipoNavegacionDTO = (TipoNavegacionDTO) object;
			equal = tipoNavegacionDTO.getIdTcTipoNavegacion().equals(this.getIdTcTipoNavegacion());
		} // End of if
		return equal;
	}
	
	@Override
	public String getDescription() {
		return this.descripcionTipoNavegacion;
	}
	
	@Override
	public Object getId() {
		return this.idTcTipoNavegacion;
	}

}