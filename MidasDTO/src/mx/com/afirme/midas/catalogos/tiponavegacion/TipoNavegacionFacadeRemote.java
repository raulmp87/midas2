package mx.com.afirme.midas.catalogos.tiponavegacion;
// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for TipoNavegacionFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface TipoNavegacionFacadeRemote extends MidasInterfaceBase<TipoNavegacionDTO> {
	/**
	 * Perform an initial save of a previously unsaved TipoNavegacionDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            TipoNavegacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoNavegacionDTO entity);

	/**
	 * Delete a persistent TipoNavegacionDTO entity.
	 * 
	 * @param entity
	 *            TipoNavegacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoNavegacionDTO entity);

	/**
	 * Persist a previously saved TipoNavegacionDTO entity and return it or a copy
	 * of it to the sender. A copy of the TipoNavegacionDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoNavegacionDTO entity to update
	 * @return TipoNavegacionDTO the persisted TipoNavegacionDTO entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoNavegacionDTO update(TipoNavegacionDTO entity);

	/**
	 * Find all TipoNavegacionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the TipoNavegacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoNavegacionDTO> found by query
	 */
	public List<TipoNavegacionDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all TipoNavegacionDTO entities.
	 * 
	 * @return List<TipoNavegacionDTO> all TipoNavegacionDTO entities
	 */
	public List<TipoNavegacionDTO> findAll();
	
	/**
	 * Find filtered TipoNavegacionDTO entities.
	 * 
	 * @return List<TipoNavegacionDTO> filtered TipoNavegacionDTO entities
	 */
	public List<TipoNavegacionDTO> listarFiltrado(TipoNavegacionDTO tipoNavegacionDTO);
}