package mx.com.afirme.midas.catalogos.codigo.postal;

// default package

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.base.CacheableDTO;

/**
 * ColoniaDTO entity. @author MyEclipse Persistence Tools
 */

@Entity(name = "ColoniaDTO")
@Table(name = "VW_COLONY", schema = "MIDAS")
public class ColoniaDTO extends CacheableDTO implements Comparable<ColoniaDTO> {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String colonyId;
	private String zipCode;
	private Integer zipCodeUserId;
	private String colonyName;
	private String cityId;
	private Integer noDiferencias = 0;

	@Id
	@Column(name = "COLONY_ID")
	public String getColonyId() {
		return colonyId;
	}

	public void setColonyId(String colonyId) {
		this.colonyId = colonyId;
	}

	@Column(name = "ZIP_CODE", unique = false, nullable = false, insertable = true, updatable = true, length = 6)
	public String getZipCode() {
		return this.zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	@Column(name = "ZIP_CODE_USER_ID")
	public Integer getZipCodeUserId() {
		return this.zipCodeUserId;
	}

	public void setZipCodeUserId(Integer zipCodeUserId) {
		this.zipCodeUserId = zipCodeUserId;
	}

	@Column(name = "COLONY_NAME", unique = false, nullable = false, insertable = true, updatable = true, length = 100)
	public String getColonyName() {
		return this.colonyName;
	}

	public void setColonyName(String colonyName) {
		this.colonyName = colonyName;
	}

	@Column(name = "CITY_ID", unique = false, nullable = false, insertable = true, updatable = true, length = 6)
	public String getCityId() {
		return this.cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getDescription() {

		return this.colonyName;
	}

	public Object getId() {
		return this.colonyId;
	}

	@Transient
	public Integer getNoDiferencias() {
		return noDiferencias;
	}

	public void setNoDiferencias(Integer noDiferencias) {
		this.noDiferencias = noDiferencias;
	}

	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof ColoniaDTO) {
			ColoniaDTO colonia = (ColoniaDTO) object;
			equal = colonia.getColonyId().equals(this.colonyId);
		} // End of if
		return equal;
	}

	public int compareTo(ColoniaDTO o) {
		return this.noDiferencias.compareTo(o.getNoDiferencias());
	}
}