package mx.com.afirme.midas.catalogos.codigo.postal;

// default package

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for ColoniaDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ColoniaFacadeRemote extends MidasInterfaceBase<ColoniaDTO> {
	/**
	 * Perform an initial save of a previously unsaved ColoniaDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            ColoniaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ColoniaDTO entity);

	/**
	 * Delete a persistent ColoniaDTO entity.
	 * 
	 * @param entity
	 *            ColoniaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ColoniaDTO entity);

	/**
	 * Persist a previously saved ColoniaDTO entity and return it or a copy of
	 * it to the sender. A copy of the ColoniaDTO entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            ColoniaDTO entity to update
	 * @returns ColoniaDTO the persisted ColoniaDTO entity instance, may not be
	 *          the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ColoniaDTO update(ColoniaDTO entity);

	public ColoniaDTO findById(String id);

	/**
	 * Find all ColoniaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ColoniaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<ColoniaDTO> found by query
	 */
	public List<ColoniaDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all ColoniaDTO entities.
	 * 
	 * @return List<ColoniaDTO> all ColoniaDTO entities
	 */
	public List<ColoniaDTO> findAll();
	
	public List<ColoniaDTO> listarFiltrado(ColoniaDTO coloniaDTO);
	
	public Long obtenerTotalFiltrado(ColoniaDTO coloniaDTO);

	public List<ColoniaDTO> findByColonyName(BigDecimal cityId, BigDecimal zipCode, String colonyName);
	
	public List<ColoniaDTO> findByColonyName(BigDecimal cityId, String colonyName);
	
	public List<ColoniaDTO> getColonyByZipCode(String zipCode);
}