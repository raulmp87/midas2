package mx.com.afirme.midas.catalogos.codigo.postal;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.CotizacionExpressPaso1Checks;

import org.apache.bval.constraints.NotEmpty;


/**
 * MunicipioDTO entity. @author MyEclipse Persistence Tools
 */
@Entity (name="MunicipioDTO")
@Table(name="VW_MUNICIPALITY"
    ,schema="MIDAS"
)

public class MunicipioDTO extends CacheableDTO implements java.io.Serializable, Entidad {


    // Fields    

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String municipalityId;
    private String municipalityName;
    private String stateId;
    private Date creationDate;
    private String creatorUserId;
    private Date lastUpdateDate;
    private String lastModifiedByUserId;

    // Constructors

    /** default constructor */
    public MunicipioDTO() {
    }

	/** minimal constructor */
    public MunicipioDTO(String municipalityId, String municipalityName) {
        this.municipalityId = municipalityId;
        this.municipalityName = municipalityName;
    }
    
    /** full constructor */
    public MunicipioDTO(String municipalityId, String municipalityName, String stateId, Date creationDate, String creatorUserId, Date lastUpdateDate, String lastModifiedByUserId) {
        this.municipalityId = municipalityId;
        this.municipalityName = municipalityName;
        this.stateId = stateId;
        this.creationDate = creationDate;
        this.creatorUserId = creatorUserId;
        this.lastUpdateDate = lastUpdateDate;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
   
    // Property accessors
    @Id 
    @Column(name="MUNICIPALITY_ID", nullable=false, length=6)
    @NotEmpty(groups=CotizacionExpressPaso1Checks.class, message="{com.afirme.midas2.requerido}")
    public String getMunicipalityId() {
        return this.municipalityId;
    }
    
    public void setMunicipalityId(String municipalityId) {
        this.municipalityId = municipalityId;
    }

    @Column(name="MUNICIPALITYNAME", nullable=false, length=40)

    public String getMunicipalityName() {
        return this.municipalityName;
    }
    
    public void setMunicipalityName(String municipalityName) {
        this.municipalityName = municipalityName;
    }

    @Column(name="STATE_ID", length=6)

    public String getStateId() {
        return this.stateId;
    }
    
    public void setStateId(String stateId) {
        this.stateId = stateId;
    }
@Temporal(TemporalType.DATE)
    @Column(name="CREATION_DATE", length=8)

    public Date getCreationDate() {
        return this.creationDate;
    }
    
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Column(name="CREATOR_USER_ID", length=8)

    public String getCreatorUserId() {
        return this.creatorUserId;
    }
    
    public void setCreatorUserId(String creatorUserId) {
        this.creatorUserId = creatorUserId;
    }
@Temporal(TemporalType.DATE)
    @Column(name="LAST_UPDATE_DATE", length=8)

    public Date getLastUpdateDate() {
        return this.lastUpdateDate;
    }
    
    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    @Column(name="LAST_MODIFIED_BY_USER_ID", length=8)

    public String getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }
    
    public void setLastModifiedByUserId(String lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }

    public boolean equals(Object other) {
    	boolean equal = (other == this);
		if (!equal && other instanceof MunicipioDTO) {
			MunicipioDTO municipio = (MunicipioDTO) other;
			equal = municipio.getId().equals(this.municipalityId);
		} // End of if
		return equal;
    }
    
    public Object getId() {
		return this.getMunicipalityId();
	}
	
	public String getDescription() {
		return this.municipalityName;
	}

	@Override
	public String getKey() {
		
		return municipalityId;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getBusinessKey() {
		// TODO Auto-generated method stub
		return municipalityId;
	}
    
    
    
}