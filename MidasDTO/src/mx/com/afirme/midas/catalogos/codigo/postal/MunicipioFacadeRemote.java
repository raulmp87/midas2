package mx.com.afirme.midas.catalogos.codigo.postal;
// default package

import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for MunicipioFacade.
 * @author MyEclipse Persistence Tools
 */


public interface MunicipioFacadeRemote extends MidasInterfaceBase<MunicipioDTO> {
		/**
	 Perform an initial save of a previously unsaved MunicipioDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity MunicipioDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(MunicipioDTO entity);
    /**
	 Delete a persistent MunicipioDTO entity.
	  @param entity MunicipioDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(MunicipioDTO entity);
   /**
	 Persist a previously saved MunicipioDTO entity and return it or a copy of it to the sender. 
	 A copy of the MunicipioDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity MunicipioDTO entity to update
	 @return MunicipioDTO the persisted MunicipioDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public MunicipioDTO update(MunicipioDTO entity);
	public MunicipioDTO findById( String id);
	 /**
	 * Find all MunicipioDTO entities with a specific property value.  
	 
	  @param propertyName the name of the MunicipioDTO property to query
	  @param value the property value to match
	  	  @return List<MunicipioDTO> found by query
	 */
	public List<MunicipioDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all MunicipioDTO entities.
	  	  @return List<MunicipioDTO> all MunicipioDTO entities
	 */
	public List<MunicipioDTO> findAll(
		);	
}