package mx.com.afirme.midas.catalogos.codigo.postal;
// default package

import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for PaisDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface PaisFacadeRemote extends MidasInterfaceBase<PaisDTO> {
		/**
	 Perform an initial save of a previously unsaved PaisDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity PaisDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(PaisDTO entity);
    /**
	 Delete a persistent PaisDTO entity.
	  @param entity PaisDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(PaisDTO entity);
   /**
	 Persist a previously saved PaisDTO entity and return it or a copy of it to the sender. 
	 A copy of the PaisDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity PaisDTO entity to update
	 @returns PaisDTO the persisted PaisDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public PaisDTO update(PaisDTO entity);
	public PaisDTO findById( String id);
	 /**
	 * Find all PaisDTO entities with a specific property value.  
	 
	  @param propertyName the name of the PaisDTO property to query
	  @param value the property value to match
	  	  @return List<PaisDTO> found by query
	 */
	public List<PaisDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all PaisDTO entities.
	  	  @return List<PaisDTO> all PaisDTO entities
	 */
	public List<PaisDTO> findAll();
	
	public List<PaisDTO> getPaisDTONotAssociatedToTipoDestino();
}