package mx.com.afirme.midas.catalogos.codigo.postal;
// default package

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;


/**
 * PaisDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name = "RecargosDescuentosCPDTO")
@Table(name = "CF_FACTORDESCUENTORECARGO", schema = "MIDAS")
public class RecargosDescuentosCPDTO extends CacheableDTO implements java.io.Serializable, Entidad {

	private static final long serialVersionUID = 1L;
	private Long idRecargoDescuento;
	private Integer codigoPostal;
	private String tipoValor;
	private BigDecimal valor;
	private Date fechaVencimiento;
	private Integer idToCobertura;
	private Integer idToSeccion;
	public static final Long TIPO_VALOR_PORCENTAJE = 1L;
	public static final Long TIPO_VALOR_MONTO = 2L;
	public static final String TIPO_VALOR_PORCENTAJE_STR = "Porcentaje";
	public static final String TIPO_VALOR_MONTO_STR = "Monto";
	
    @Id
    @Column(name="ID", unique=false, nullable=false, insertable=true, updatable=true)
    public Long getIdRecargoDescuento() {
 	   return idRecargoDescuento;
    }
    public void setIdRecargoDescuento(Long idRecargoDescuento) {
 	   this.idRecargoDescuento = idRecargoDescuento;
    }
	
    @Column(name="CODIGO_POSTAL", unique=false, nullable=false, insertable=true, updatable=true)
    public Integer getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(Integer codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

    @Column(name="TIPO_VALOR", unique=false, nullable=true, insertable=true, updatable=true)
	public String getTipoValor() {
		return tipoValor;
	}

	public void setTipoValor(String tipoValor) {
		this.tipoValor = tipoValor;
	}
	
	@Column(name="VALOR", unique=false, nullable=true, insertable=true, updatable=true)
	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
	@Temporal(TemporalType.DATE)
    @Column(name="FECHA_VENCIM", unique=false, nullable=true, insertable=true, updatable=true)
	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	
	@Column(name="TOCOBERTURA_ID", unique=false, nullable=true, insertable=true, updatable=true)
	public Integer getIdToCobertura() {
		return idToCobertura;
	}

	public void setIdToCobertura(Integer idToCobertura) {
		this.idToCobertura = idToCobertura;
	}
	
	@Column(name="TOSECCION_ID", unique=false, nullable=true, insertable=true, updatable=true)
	public Integer getIdToSeccion() {
		return idToSeccion;
	}

	public void setIdToSeccion(Integer idToSeccion) {
		this.idToSeccion = idToSeccion;
	}
	
	public Object getId() {
		return this.getIdRecargoDescuento();
	}
	
	public String getDescription() {
		return this.tipoValor;
	}
	
	public boolean equals(Object other) {
		boolean equal = (other == this);
		if (!equal && other instanceof PaisDTO) {
			RecargosDescuentosCPDTO recDesc = (RecargosDescuentosCPDTO) other;
			equal = recDesc.getId().equals(this.idRecargoDescuento);
		}
		return equal;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getIdRecargoDescuento();
	}
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
}