package mx.com.afirme.midas.catalogos.codigo.postal;

// default package

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * CiudadDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name = "CiudadDTO")
@Table(name = "VW_CITY", schema = "MIDAS")
public class CiudadDTO extends CacheableDTO implements java.io.Serializable, Entidad {

	private static final long serialVersionUID = 1L;
	private String cityId;
	private String cityName;
	private String stateId;
	private Date creationDate;
	private String creatorUserId;
	private Date lastUpdateDate;
	private String lastModifiedByUserId;

	// Property accessors
	@Id
	@Column(name = "CITY_ID", unique = false, nullable = false, insertable = true, updatable = true, length = 6)
	public String getCityId() {
		return this.cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	@Column(name = "CITY_NAME", unique = false, nullable = false, insertable = true, updatable = true, length = 40)
	public String getCityName() {
		return this.cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	@Column(name = "STATE_ID", unique = false, nullable = true, insertable = true, updatable = true, length = 6)
	public String getStateId() {
		return this.stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "CREATION_DATE", unique = false, nullable = true, insertable = true, updatable = true, length = 8)
	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Column(name = "CREATOR_USER_ID", unique = false, nullable = true, insertable = true, updatable = true, length = 8)
	public String getCreatorUserId() {
		return this.creatorUserId;
	}

	public void setCreatorUserId(String creatorUserId) {
		this.creatorUserId = creatorUserId;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "LAST_UPDATE_DATE", unique = false, nullable = true, insertable = true, updatable = true, length = 8)
	public Date getLastUpdateDate() {
		return this.lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	@Column(name = "LAST_MODIFIED_BY_USER_ID", unique = false, nullable = true, insertable = true, updatable = true, length = 8)
	public String getLastModifiedByUserId() {
		return this.lastModifiedByUserId;
	}

	public void setLastModifiedByUserId(String lastModifiedByUserId) {
		this.lastModifiedByUserId = lastModifiedByUserId;
	}

	public String getDescription() {
		return this.cityName;
	}

	public Object getId() {
		return this.getCityId();
	}

	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof CiudadDTO) {
			CiudadDTO ciudad = (CiudadDTO) object;
			equal = ciudad.getCityId().equals(this.cityId);
		} // End of if
		return equal;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getKey() {
		return this.getCityId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}