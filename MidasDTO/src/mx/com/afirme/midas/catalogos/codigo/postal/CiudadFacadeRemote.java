package mx.com.afirme.midas.catalogos.codigo.postal;

// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for CiudadDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface CiudadFacadeRemote extends MidasInterfaceBase<CiudadDTO> {
	/**
	 * Perform an initial save of a previously unsaved CiudadDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            CiudadDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	void save(CiudadDTO entity);

	/**
	 * Delete a persistent CiudadDTO entity.
	 * 
	 * @param entity
	 *            CiudadDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	void delete(CiudadDTO entity);

	/**
	 * Persist a previously saved CiudadDTO entity and return it or a copy of it
	 * to the sender. A copy of the CiudadDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            CiudadDTO entity to update
	 * @returns CiudadDTO the persisted CiudadDTO entity instance, may not be
	 *          the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	CiudadDTO update(CiudadDTO entity);

	CiudadDTO findById(String id);

	/**
	 * Find all CiudadDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the CiudadDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<CiudadDTO> found by query
	 */
	List<CiudadDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all CiudadDTO entities.
	 * 
	 * @return List<CiudadDTO> all CiudadDTO entities
	 */
	List<CiudadDTO> findAll();

	List<CiudadDTO> findByCityName(BigDecimal stateId, String cityName);
	
	String getCityIdByZipCode(String zipCode);
	
	CiudadDTO getCityByZipCode(String zipCode);
}