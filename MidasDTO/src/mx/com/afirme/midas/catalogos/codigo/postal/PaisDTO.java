package mx.com.afirme.midas.catalogos.codigo.postal;
// default package

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;


/**
 * PaisDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name = "PaisDTO")
@Table(name = "VW_COUNTRY", schema="MIDAS")
public class PaisDTO extends CacheableDTO implements java.io.Serializable, Entidad {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String countryId;
    private String countryName;
    private Date creationDate;
    private String creatorUserId;
    private Date lastUpdateDate;
    private String lastModifiedByUserId;
    @Id
    @Column(name="COUNTRY_ID", unique=false, nullable=false, insertable=true, updatable=true, length=6)
	public String getCountryId() {
		return countryId;
	}
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	
    @Column(name="COUNTRY_NAME", unique=false, nullable=false, insertable=true, updatable=true, length=40)
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	
	@Temporal(TemporalType.DATE)
    @Column(name="CREATION_DATE", unique=false, nullable=true, insertable=true, updatable=true, length=8)
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	@Column(name="CREATOR_USER_ID", unique=false, nullable=true, insertable=true, updatable=true, length=8)
	public String getCreatorUserId() {
		return creatorUserId;
	}
	public void setCreatorUserId(String creatorUserId) {
		this.creatorUserId = creatorUserId;
	}
	
	@Temporal(TemporalType.DATE)
    @Column(name="LAST_UPDATE_DATE", unique=false, nullable=true, insertable=true, updatable=true, length=8)
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	
	@Column(name="LAST_MODIFIED_BY_USER_ID", unique=false, nullable=true, insertable=true, updatable=true, length=8)
	public String getLastModifiedByUserId() {
		return lastModifiedByUserId;
	}
	public void setLastModifiedByUserId(String lastModifiedByUserId) {
		this.lastModifiedByUserId = lastModifiedByUserId;
	}
	
	public Object getId() {
		return this.getCountryId();
	}
	
	public String getDescription() {
		return this.countryName;
	}
    
	public boolean equals(Object other) {
		boolean equal = (other == this);
		if (!equal && other instanceof PaisDTO) {
			PaisDTO pais = (PaisDTO) other;
			equal = pais.getCountryId().equals(this.countryId);
		} // End of if
		return equal;
  }
	@SuppressWarnings("unchecked")
	@Override
	public String getKey() {
		return this.getCountryId();
	}
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}   

}