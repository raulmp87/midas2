package mx.com.afirme.midas.catalogos.codigo.postal;

// default package

import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for EstadoDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface EstadoFacadeRemote extends MidasInterfaceBase<EstadoDTO> {
	/**
	 * Perform an initial save of a previously unsaved EstadoDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            EstadoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(EstadoDTO entity);
	
	public void save(RecargosDescuentosCPDTO entity);

	/**
	 * Delete a persistent EstadoDTO entity.
	 * 
	 * @param entity
	 *            EstadoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(EstadoDTO entity);
	
	public void delete(RecargosDescuentosCPDTO entity);

	/**
	 * Persist a previously saved EstadoDTO entity and return it or a copy of it
	 * to the sender. A copy of the EstadoDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            EstadoDTO entity to update
	 * @returns EstadoDTO the persisted EstadoDTO entity instance, may not be
	 *          the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public EstadoDTO update(EstadoDTO entity);
	
	public RecargosDescuentosCPDTO update(RecargosDescuentosCPDTO entity);

	public EstadoDTO findById(String id);

	/**
	 * Find all EstadoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the EstadoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<EstadoDTO> found by query
	 */
	public List<EstadoDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all EstadoDTO entities.
	 * 
	 * @return List<EstadoDTO> all EstadoDTO entities
	 */
	public List<EstadoDTO> findAll();
	
	public String getStateIdByZipCode(String zipCode);
	
	/**
	 * Indica si el estado es fronterizo.
	 * @param idEstado
	 * @return
	 */
	public Boolean isEstadoFronterizo(String idEstado);
	public String validarCodigoPostal(String codigoPostal,int idEstado);
	public List<RecargosDescuentosCPDTO> getRegargosCP(String idCobertura,String idSeccion);
	
}