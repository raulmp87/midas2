package mx.com.afirme.midas.catalogos.codigo.postal;

// default package

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.CotizacionExpressPaso1Checks;

import org.apache.bval.constraints.NotEmpty;

/**
 * EstadoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name = "EstadoDTO")
@Table(name = "VW_STATE", schema = "MIDAS")
public class EstadoDTO extends CacheableDTO implements java.io.Serializable, Entidad {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String stateId;
	private String stateName;
	private String countryId;
	private Date creationDate;
	private String creatorUserId;
	private Date lastUpdateDate;
	private String lastModifiedByUserId;

	// Property accessors
	@Id
	@Column(name = "STATE_ID", unique = false, nullable = false, insertable = true, updatable = true, length = 6)
	@NotEmpty(groups=CotizacionExpressPaso1Checks.class, message="{com.afirme.midas2.requerido}")
	public String getStateId() {
		return this.stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	@Column(name = "STATE_NAME", unique = false, nullable = false, insertable = true, updatable = true, length = 40)
	public String getStateName() {
		return this.stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	@Column(name = "COUNTRY_ID", unique = false, nullable = true, insertable = true, updatable = true, length = 6)
	public String getCountryId() {
		return this.countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "CREATION_DATE", unique = false, nullable = true, insertable = true, updatable = true, length = 8)
	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Column(name = "CREATOR_USER_ID", unique = false, nullable = true, insertable = true, updatable = true, length = 8)
	public String getCreatorUserId() {
		return this.creatorUserId;
	}

	public void setCreatorUserId(String creatorUserId) {
		this.creatorUserId = creatorUserId;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "LAST_UPDATE_DATE", unique = false, nullable = true, insertable = true, updatable = true, length = 8)
	public Date getLastUpdateDate() {
		return this.lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	@Column(name = "LAST_MODIFIED_BY_USER_ID", unique = false, nullable = true, insertable = true, updatable = true, length = 8)
	public String getLastModifiedByUserId() {
		return this.lastModifiedByUserId;
	}

	public void setLastModifiedByUserId(String lastModifiedByUserId) {
		this.lastModifiedByUserId = lastModifiedByUserId;
	}

	public String getDescription() {
		return this.stateName;
	}

	public Object getId() {
		return this.getStateId();
	}

	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof EstadoDTO) {
			EstadoDTO estado = (EstadoDTO) object;
			equal = estado.getStateId().equals(this.stateId);
		} // End of if
		return equal;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getKey() {
		return this.getStateId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}