package mx.com.afirme.midas.catalogos.reaseguradorcnsf;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.catalogos.calificacionesreas.CalificacionAgenciaDTO;

@Entity(name = "ReaseguradorCNSFMovDTO")
@Table(name="CNSF_REASEGURADORES_MOV"
    ,schema="MIDAS"
)
public class ReaseguradorCNSFMovDTO {
	
	private BigDecimal idReasMov;
	private Date corte;
	private String calificacion;
	private BigDecimal idagencia;
	
	private CalificacionAgenciaDTO calificacionAgenciaDTO;
	private ReaseguradorCnsfDTO ReaseguradorCnsfDTO;
	
	@Id
    @SequenceGenerator(name = "CNSF_REASEGURADORES_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.CNSF_REASEGURADORES_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CNSF_REASEGURADORES_SEQ_GENERADOR")
    @Column(name="ID_REAS_MOV", unique=true, nullable=false, precision=22, scale=0)
    	public BigDecimal getIdReasMov() {
		return idReasMov;
	}
	public void setIdReasMov(BigDecimal idReasMov) {
		this.idReasMov = idReasMov;
	}
	@Temporal(TemporalType.DATE)
	@Column(name="CORTE", nullable=false)
	public Date getCorte() {
		return corte;
	}
	public void setCorte(Date corte) {
		this.corte = corte;
	}
	@Column(name="CALIFICACION", nullable=false)
	public String getCalificacion() {
		return calificacion;
	}
	public void setCalificacion(String calificacion) {
		this.calificacion = calificacion;
	}
	@Column(name="IDAGENCIA", nullable=false)
	public BigDecimal getIdagencia() {
		return idagencia;
	}
	public void setIdagencia(BigDecimal idagencia) {
		this.idagencia = idagencia;
	}
	
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID", nullable = false, insertable = false, updatable = false)
	public CalificacionAgenciaDTO getCalificacionAgenciaDTO() {
		return calificacionAgenciaDTO;
	}
	public void setCalificacionAgenciaDTO(
			CalificacionAgenciaDTO calificacionAgenciaDTO) {
		this.calificacionAgenciaDTO = calificacionAgenciaDTO;
	}
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDREASEGURADOR", nullable = false, insertable = false, updatable = false)
	public ReaseguradorCnsfDTO getReaseguradorCnsfDTO() {
		return ReaseguradorCnsfDTO;
	}
	public void setReaseguradorCnsfDTO(ReaseguradorCnsfDTO ReaseguradorCnsfDTO) {
		this.ReaseguradorCnsfDTO = ReaseguradorCnsfDTO;
	}
	
	
}
