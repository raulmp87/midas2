package mx.com.afirme.midas.catalogos.reaseguradorcnsf;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.catalogos.agenciacalificadora.AgenciaCalificadoraDTO;
import mx.com.afirme.midas.catalogos.calificacionesreas.CalificacionAgenciaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name = "ReaseguradorCNSFBitacoraDTO")
@Table(name="CNSF_REASEGURADORES_BITACORA"
    ,schema="MIDAS"
) 

public class ReaseguradorCNSFBitacoraDTO extends CacheableDTO implements java.io.Serializable, Entidad, Comparable<ReaseguradorCNSFBitacoraDTO> {
   
	 private static final long serialVersionUID = 1L;
	 
	 private BigDecimal idReasBit;
	 private Date fechaMov;
	 private String usuario;
	 private String cambio;
	 
	@Id
	@Column(name="ID_REAS_BIT", unique=true, nullable=false, precision=22, scale=0)
	public BigDecimal getIdReasBit() {
		return idReasBit;
	}
	public void setIdReasBit(BigDecimal idReasBit) {
		this.idReasBit = idReasBit;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_MOV", nullable=false)
	public Date getFechaMov() {

	    return fechaMov;
	}
	public void setFechaMov(Date fechaMov) {
	
		this.fechaMov = fechaMov;
		    
	}
	
	@Column(name="USUARIO", nullable=false)
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	@Column(name="CAMBIO", nullable=false)
	public String getCambio() {
		return cambio;
	}
	public void setCambio(String cambio) {
		this.cambio = cambio;
	}
	 

	@Override
	public int compareTo(ReaseguradorCNSFBitacoraDTO arg0) {
		return this.fechaMov.compareTo(arg0.fechaMov);
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getKey() {
		return this.getIdReasBit()+"";
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}

	@Override
	public Object getId() {
		return this.getIdReasBit();
	}

	@Override
	public String getDescription() {
		return this.getCambio();
	}

	@Override
	public boolean equals(Object other) {
		boolean equal = other == this;
		if (!equal && other instanceof ReaseguradorCNSFBitacoraDTO) {
			ReaseguradorCNSFBitacoraDTO pais = (ReaseguradorCNSFBitacoraDTO) other;
			equal = pais.getIdReasBit().equals(this.idReasBit);
		}
		return equal;
	}

}