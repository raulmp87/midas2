package mx.com.afirme.midas.catalogos.reaseguradorcnsf;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.catalogos.agenciacalificadora.AgenciaCalificadoraDTO;
import mx.com.afirme.midas.catalogos.calificacionesreas.CalificacionAgenciaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;


@Entity(name = "ReaseguradorCnsfDTO")
@Table(name="CNSF_REASEGURADORES"
    ,schema="MIDAS"
) 

public class ReaseguradorCnsfDTO extends CacheableDTO implements java.io.Serializable, Entidad, Comparable<ReaseguradorCnsfDTO> {
	   
	 private static final long serialVersionUID = 1L;
	 
	 private BigDecimal idReasegurador;
	 private String nombreReas;
	 private String claveCnsf;
	 private String claveCnsfAnt;
	 private BigDecimal idagencia;
	 private String calificacion;
	 private String usuario;
	 private BigDecimal idTcReasegurador;
	 private AgenciaCalificadoraDTO agenciaCalificadoraDTO; 
	 private CalificacionAgenciaDTO calificacionAgenciaDTO;
	 
    @Id
    @SequenceGenerator(name = "CNSF_REASEGURADORES_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.CNSF_REASEGURADORES_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CNSF_REASEGURADORES_SEQ_GENERADOR")
    @Column(name="ID_REASEGURADORES", unique=true, nullable=false, precision=22, scale=0)
	public BigDecimal getIdReasegurador() {
		return idReasegurador;
	}

	public void setIdReasegurador(BigDecimal idReasegurador) {
		this.idReasegurador = idReasegurador;
	}

	@Column(name="NOMBRE_REASEGURADOR", nullable=false)
	public String getNombreReas() {
		return nombreReas;
	}
	
	public void setNombreReas(String nombreReas) {
		this.nombreReas = nombreReas;
	}

	@Column(name="CNSF", nullable=false)
	public String getClaveCnsf() {
		return claveCnsf;
	}

	public void setClaveCnsf(String claveCnsf) {
		this.claveCnsf = claveCnsf;
	}

	@Column(name="CNSF_ANTERIOR", nullable=false)
	public String getClaveCnsfAnt() {
		return claveCnsfAnt;
	}

	public void setClaveCnsfAnt(String claveCnsfAnt) {
		this.claveCnsfAnt = claveCnsfAnt;
	}
	@Column(name="AGENCIA_CALIFICADORA", nullable=false)
	public BigDecimal getIdagencia() {
		return idagencia;
	}

	public void setIdagencia(BigDecimal idagencia) {
		this.idagencia = idagencia;
	}

	@Column(name="CALIFICACION", nullable=false)
	public String getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(String calificacion) {
		this.calificacion = calificacion;
	}
	
	@Column(name="USUARIO", nullable=false)
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	@Column(name="IDTCREASEGURADORCORREDOR", nullable=false)
	public BigDecimal getIdTcReasegurador() {
		return idTcReasegurador;
	}

	public void setIdTcReasegurador(BigDecimal idTcReasegurador) {
		this.idTcReasegurador = idTcReasegurador;
	}

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "AGENCIA_CALIFICADORA", nullable = false, insertable = false, updatable = false)
	public AgenciaCalificadoraDTO getAgenciaCalificadoraDTO() {
		return agenciaCalificadoraDTO;
	}
	
	public void setAgenciaCalificadoraDTO(
			AgenciaCalificadoraDTO agenciaCalificadoraDTO) {
		this.agenciaCalificadoraDTO = agenciaCalificadoraDTO;
	}
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CALIFICACION", nullable = false, insertable = false, updatable = false)
	public CalificacionAgenciaDTO getCalificacionAgenciaDTO() {
		return calificacionAgenciaDTO;
	}

	public void setCalificacionAgenciaDTO(
			CalificacionAgenciaDTO calificacionAgenciaDTO) {
		this.calificacionAgenciaDTO = calificacionAgenciaDTO;
	}
	
	@Override
	public int compareTo(ReaseguradorCnsfDTO arg0) {
		return this.claveCnsf.compareTo(arg0.claveCnsf);
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getKey() {
		return this.getIdReasegurador()+"";
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}

	@Override
	public Object getId() {
		return this.getIdReasegurador();
	}

	@Override
	public String getDescription() {
		return this.claveCnsf;
	}

	@Override
	public boolean equals(Object other) {
		boolean equal = other == this;
		if (!equal && other instanceof ReaseguradorCnsfDTO) {
			ReaseguradorCnsfDTO pais = (ReaseguradorCnsfDTO) other;
			equal = pais.getIdReasegurador().equals(this.idReasegurador);
		}
		return equal;
	}
}