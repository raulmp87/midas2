package mx.com.afirme.midas.catalogos.reaseguradorcnsf;

import java.math.BigDecimal;

public class ReaseguradorCargaCNSF {
	
	private BigDecimal idReaseguradorCargaCNSF;
	private String claveReasegurador;
	private String nombreReasegurador;
	private String tipoRiesgo;
	private String fechaAlta;
	private String fechaBaja;
	
	public BigDecimal getIdReaseguradorCargaCNSF() {
		return idReaseguradorCargaCNSF;
	}
	public void setIdReaseguradorCargaCNSF(BigDecimal idReaseguradorCargaCNSF) {
		this.idReaseguradorCargaCNSF = idReaseguradorCargaCNSF;
	}
	public String getClaveReasegurador() {
		return claveReasegurador;
	}
	public void setClaveReasegurador(String claveReasegurador) {
		this.claveReasegurador = claveReasegurador;
	}
	public String getNombreReasegurador() {
		return nombreReasegurador;
	}
	public void setNombreReasegurador(String nombreReasegurador) {
		this.nombreReasegurador = nombreReasegurador;
	}
	public String getTipoRiesgo() {
		return tipoRiesgo;
	}
	public void setTipoRiesgo(String tipoRiesgo) {
		this.tipoRiesgo = tipoRiesgo;
	}
	public String getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	public String getFechaBaja() {
		return fechaBaja;
	}
	public void setFechaBaja(String fechaBaja) {
		this.fechaBaja = fechaBaja;
	}
	
	
}
