package mx.com.afirme.midas.catalogos.reaseguradorcnsf;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.base.MidasInterfaceBase;

public interface ReaseguradorCnsfFacadeRemote extends MidasInterfaceBase<ReaseguradorCnsfDTO> {
	
	public void save(ReaseguradorCnsfDTO entity);
	
    public void delete(ReaseguradorCnsfDTO entity);
 
	public ReaseguradorCnsfDTO update(ReaseguradorCnsfDTO entity);

	public List<ReaseguradorCnsfDTO> findByProperty(String propertyName, Object value);
	
	public List<ReaseguradorCnsfDTO> findByPropertyID(BigDecimal value);
	
	public List<ReaseguradorCnsfDTO> findAll();
		
	public List<ReaseguradorCnsfDTO> listarFiltrado(ReaseguradorCnsfDTO contactoDTO);
	
	public ReaseguradorCnsfDTO findById(String id);
	public List<ReaseguradorCNSFBitacoraDTO> findAllBitacora();

}
