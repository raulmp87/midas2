package mx.com.afirme.midas.catalogos.tipobienautos.cargavaloresamis;
// default package

import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.catalogos.tipobienautos.TipoBienAutosDTO;
import mx.com.afirme.midas.catalogos.tipobienautos.cargavaloresamis.CargaValoresAmisId;;


/**
 * CargaValoresAmisDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOCARGAVALORESAMIS"
    ,schema="MIDAS"
)

public class CargaValoresAmisDTO  extends CacheableDTO{


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CargaValoresAmisId id;
     private TipoBienAutosDTO tipoBienAutosDTO;
     private Date fechaInicioVigencia;
     private Date fechaFinVigencia;
     private Short claveEstatus;
     private Date fechaCreacion;
     private String codigoUsuarioCreacion;
     private Date fechaActivacion;
     private String codigoUsuarioActivacion;


    // Constructors

    /** default constructor */
    public CargaValoresAmisDTO() {
    }

	/** minimal constructor */
    public CargaValoresAmisDTO(CargaValoresAmisId id, TipoBienAutosDTO tipoBienAutosDTO, Date fechaInicioVigencia, Date fechaFinVigencia, Short claveEstatus, Date fechaCreacion, String codigoUsuarioCreacion) {
        this.id = id;
        this.tipoBienAutosDTO = tipoBienAutosDTO;
        this.fechaInicioVigencia = fechaInicioVigencia;
        this.fechaFinVigencia = fechaFinVigencia;
        this.claveEstatus = claveEstatus;
        this.fechaCreacion = fechaCreacion;
        this.codigoUsuarioCreacion = codigoUsuarioCreacion;
    }
    
    /** full constructor */
    public CargaValoresAmisDTO(CargaValoresAmisId id, TipoBienAutosDTO tipoBienAutosDTO, Date fechaInicioVigencia, Date fechaFinVigencia, Short claveEstatus, Date fechaCreacion, String codigoUsuarioCreacion, Date fechaActivacion, String codigoUsuarioActivacion) {
        this.id = id;
        this.tipoBienAutosDTO = tipoBienAutosDTO;
        this.fechaInicioVigencia = fechaInicioVigencia;
        this.fechaFinVigencia = fechaFinVigencia;
        this.claveEstatus = claveEstatus;
        this.fechaCreacion = fechaCreacion;
        this.codigoUsuarioCreacion = codigoUsuarioCreacion;
        this.fechaActivacion = fechaActivacion;
        this.codigoUsuarioActivacion = codigoUsuarioActivacion;
    }

   
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="claveTipoBien", column=@Column(name="CLAVETIPOBIEN", nullable=false, length=5) ), 
        @AttributeOverride(name="idMoneda", column=@Column(name="IDMONEDA", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idVersionCarga", column=@Column(name="IDVERSIONCARGA", nullable=false, precision=22, scale=0) ) } )

    public CargaValoresAmisId getId() {
        return this.id;
    }
    
    public void setId(CargaValoresAmisId id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="CLAVETIPOBIEN", nullable=false, insertable=false, updatable=false)

    public TipoBienAutosDTO getTipoBienAutosDTO() {
        return this.tipoBienAutosDTO;
    }
    
    public void setTipoBienAutosDTO(TipoBienAutosDTO tipoBienAutosDTO) {
        this.tipoBienAutosDTO = tipoBienAutosDTO;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="fechaInicioVigencia", nullable=false, length=7)

    public Date getFechaInicioVigencia() {
        return this.fechaInicioVigencia;
    }
    
    public void setFechaInicioVigencia(Date fechaInicioVigencia) {
        this.fechaInicioVigencia = fechaInicioVigencia;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="fechaFinVigencia", nullable=false, length=7)

    public Date getFechaFinVigencia() {
        return this.fechaFinVigencia;
    }
    
    public void setFechaFinVigencia(Date fechaFinVigencia) {
        this.fechaFinVigencia = fechaFinVigencia;
    }
    
    @Column(name="claveEstatus", nullable=false, precision=4, scale=0)

    public Short getClaveEstatus() {
        return this.claveEstatus;
    }
    
    public void setClaveEstatus(Short claveEstatus) {
        this.claveEstatus = claveEstatus;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="fechaCreacion", nullable=false, length=7)

    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }
    
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    
    @Column(name="codigoUsuarioCreacion", nullable=false, length=8)

    public String getCodigoUsuarioCreacion() {
        return this.codigoUsuarioCreacion;
    }
    
    public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
        this.codigoUsuarioCreacion = codigoUsuarioCreacion;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="fechaActivacion", length=7)

    public Date getFechaActivacion() {
        return this.fechaActivacion;
    }
    
    public void setFechaActivacion(Date fechaActivacion) {
        this.fechaActivacion = fechaActivacion;
    }
    
    @Column(name="codigoUsuarioActivacion", length=8)

    public String getcodigoUsuarioActivacion() {
        return this.codigoUsuarioActivacion;
    }
    
    public void setcodigoUsuarioActivacion(String codigoUsuarioActivacion) {
        this.codigoUsuarioActivacion = codigoUsuarioActivacion;
    }

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		CargaValoresAmisDTO other = (CargaValoresAmisDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
   








}