package mx.com.afirme.midas.catalogos.tipobienautos;
// default package

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;
import  mx.com.afirme.midas.catalogos.tipobienautos.cargavaloresamis.CargaValoresAmisDTO;


/**
 * TipoBienAutosDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCTIPOBIENAUTOS"
    ,schema="MIDAS"
)

public class TipoBienAutosDTO  extends CacheableDTO{


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 6903874911654678637L;
	private String claveTipoBien;
    private String descripcionTipoBien;
    private List<TipoVehiculoDTO> tipoVehiculoDTOs = new ArrayList<TipoVehiculoDTO>(0);
    private List<EstiloVehiculoDTO> estiloVehiculoDTOs = new ArrayList<EstiloVehiculoDTO>(0);
    private List<CargaValoresAmisDTO> cargaValoresAmisDTOs = new ArrayList<CargaValoresAmisDTO>(0);
    private List<MarcaVehiculoDTO> marcaVehiculoDTOs = new ArrayList<MarcaVehiculoDTO>(0);


    // Constructors

    /** default constructor */
    public TipoBienAutosDTO() {
    }

	/** minimal constructor */
    public TipoBienAutosDTO(String claveTipoBien) {
        this.claveTipoBien = claveTipoBien;
    }
    
    /** full constructor */
    public TipoBienAutosDTO(String claveTipoBien, String descripcionTipoBien, List<TipoVehiculoDTO> tipoVehiculoDTOs, List<EstiloVehiculoDTO> estiloVehiculoDTOs, List<CargaValoresAmisDTO> cargaValoresAmisDTOs, List<MarcaVehiculoDTO> marcaVehiculoDTOs) {
        this.claveTipoBien = claveTipoBien;
        this.descripcionTipoBien = descripcionTipoBien;
        this.tipoVehiculoDTOs = tipoVehiculoDTOs;
        this.estiloVehiculoDTOs = estiloVehiculoDTOs;
        this.cargaValoresAmisDTOs = cargaValoresAmisDTOs;
        this.marcaVehiculoDTOs = marcaVehiculoDTOs;
    }

   
    // Property accessors
    @Id 
    
    @Column(name="claveTipoBien", unique=true, nullable=false, length=5)

    public String getClaveTipoBien() {
    	if(this.claveTipoBien != null){
    		this.claveTipoBien.toUpperCase();
    	}
        return this.claveTipoBien;
    }
    
    public void setClaveTipoBien(String claveTipoBien) {
        this.claveTipoBien = claveTipoBien;
    }
    
    @Column(name="descripcionTipoBien", length=100)

    public String getDescripcionTipoBien() {
        return this.descripcionTipoBien!=null?this.descripcionTipoBien.toUpperCase():this.descripcionTipoBien;
    }
    
    public void setDescripcionTipoBien(String descripcionTipoBien) {
        this.descripcionTipoBien = descripcionTipoBien;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="tipoBienAutosDTO")

    public List<TipoVehiculoDTO> getTipoVehiculoDTOs() {
        return this.tipoVehiculoDTOs;
    }
    
    public void setTipoVehiculoDTOs(List<TipoVehiculoDTO> tipoVehiculoDTOs) {
        this.tipoVehiculoDTOs = tipoVehiculoDTOs;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="tipoBienAutosDTO")

    public List<EstiloVehiculoDTO> getEstiloVehiculoDTOs() {
        return this.estiloVehiculoDTOs;
    }
    
    public void setEstiloVehiculoDTOs(List<EstiloVehiculoDTO> estiloVehiculoDTOs) {
        this.estiloVehiculoDTOs = estiloVehiculoDTOs;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="tipoBienAutosDTO")

    public List<CargaValoresAmisDTO> getCargaValoresAmisDTOs() {
        return this.cargaValoresAmisDTOs;
    }
    
    public void setCargaValoresAmisDTOs(List<CargaValoresAmisDTO> cargaValoresAmisDTOs) {
        this.cargaValoresAmisDTOs = cargaValoresAmisDTOs;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="tipoBienAutosDTO")

    public List<MarcaVehiculoDTO> getMarcaVehiculoDTOs() {
        return this.marcaVehiculoDTOs;
    }
    
    public void setMarcaVehiculoDTOs(List<MarcaVehiculoDTO> marcaVehiculoDTOs) {
        this.marcaVehiculoDTOs = marcaVehiculoDTOs;
    }

    @Override
	public String getDescription() {
		return getDescripcionTipoBien();
	}

	@Override
	public Object getId() {
		return getClaveTipoBien();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((claveTipoBien == null) ? 0 : claveTipoBien.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		TipoBienAutosDTO other = (TipoBienAutosDTO) obj;
		if (claveTipoBien == null) {
			if (other.claveTipoBien != null)
				return false;
		} else if (!claveTipoBien.equals(other.claveTipoBien))
			return false;
		return true;
	}
   








}