package mx.com.afirme.midas.catalogos.tipobienautos.cargavaloresamis;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * CargaValoresAmisDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class CargaValoresAmisId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 5613574732186993480L;
	private String claveTipoBien;
	private BigDecimal idMoneda;
	private BigDecimal idVersionCarga;


    // Constructors

    /** default constructor */
    public CargaValoresAmisId() {
    }

    
    /** full constructor */
    public CargaValoresAmisId(String claveTipoBien, BigDecimal idMoneda, BigDecimal idVersionCarga) {
        this.claveTipoBien = claveTipoBien;
        this.idMoneda = idMoneda;
        this.idVersionCarga = idVersionCarga;
    }

   
    // Property accessors

    @Column(name="claveTipoBien", nullable=false, length=5)

    public String getClaveTipoBien() {
        return this.claveTipoBien;
    }
    
    public void setClaveTipoBien(String claveTipoBien) {
        this.claveTipoBien = claveTipoBien;
    }

    @Column(name="idMoneda", nullable=false, precision=22, scale=0)

    public BigDecimal getIdMoneda() {
        return this.idMoneda;
    }
    
    public void setIdMoneda(BigDecimal idMoneda) {
        this.idMoneda = idMoneda;
    }

    @Column(name="idVersionCarga", nullable=false, precision=22, scale=0)

    public BigDecimal getIdVersionCarga() {
        return this.idVersionCarga;
    }
    
    public void setIdVersionCarga(BigDecimal idVersionCarga) {
        this.idVersionCarga = idVersionCarga;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof CargaValoresAmisId) ) return false;
		 CargaValoresAmisId castOther = ( CargaValoresAmisId ) other; 
         
		 return ( (this.getClaveTipoBien()==castOther.getClaveTipoBien()) || ( this.getClaveTipoBien()!=null && castOther.getClaveTipoBien()!=null && this.getClaveTipoBien().equals(castOther.getClaveTipoBien()) ) )
 && ( (this.getIdMoneda()==castOther.getIdMoneda()) || ( this.getIdMoneda()!=null && castOther.getIdMoneda()!=null && this.getIdMoneda().equals(castOther.getIdMoneda()) ) )
 && ( (this.getIdVersionCarga()==castOther.getIdVersionCarga()) || ( this.getIdVersionCarga()!=null && castOther.getIdVersionCarga()!=null && this.getIdVersionCarga().equals(castOther.getIdVersionCarga()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getClaveTipoBien() == null ? 0 : this.getClaveTipoBien().hashCode() );
         result = 37 * result + ( getIdMoneda() == null ? 0 : this.getIdMoneda().hashCode() );
         result = 37 * result + ( getIdVersionCarga() == null ? 0 : this.getIdVersionCarga().hashCode() );
         return result;
   }   





}