package mx.com.afirme.midas.catalogos.tipobienautos;
// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for TipoBienAutosDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface TipoBienAutosFacadeRemote extends MidasInterfaceBase<TipoBienAutosDTO>{
		/**
	 Perform an initial save of a previously unsaved TipoBienAutosDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity TipoBienAutosDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(TipoBienAutosDTO entity);
    /**
	 Delete a persistent TipoBienAutosDTO entity.
	  @param entity TipoBienAutosDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(TipoBienAutosDTO entity);
   /**
	 Persist a previously saved TipoBienAutosDTO entity and return it or a copy of it to the sender. 
	 A copy of the TipoBienAutosDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity TipoBienAutosDTO entity to update
	 @return TipoBienAutosDTO the persisted TipoBienAutosDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public TipoBienAutosDTO update(TipoBienAutosDTO entity);
	public TipoBienAutosDTO findById( String id);
	 /**
	 * Find all TipoBienAutosDTO entities with a specific property value.  
	 
	  @param propertyName the name of the TipoBienAutosDTO property to query
	  @param value the property value to match
	  	  @return List<TipoBienAutosDTO> found by query
	 */
	public List<TipoBienAutosDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all TipoBienAutosDTO entities.
	  	  @return List<TipoBienAutosDTO> all TipoBienAutosDTO entities
	 */
	public List<TipoBienAutosDTO> findAll();
	
	public List<TipoBienAutosDTO> listarFiltrado(TipoBienAutosDTO tipoBienAutosDTO);
}