package mx.com.afirme.midas.catalogos.tipobienautos.cargavaloresamis;

import java.util.List;
import javax.ejb.Remote;
import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for CargaValoresAmisDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface CargaValoresAmisFacadeRemote extends MidasInterfaceBase<CargaValoresAmisDTO>{
		/**
	 Perform an initial save of a previously unsaved CargaValoresAmisDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CargaValoresAmisDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CargaValoresAmisDTO entity);
    /**
	 Delete a persistent CargaValoresAmisDTO entity.
	  @param entity CargaValoresAmisDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CargaValoresAmisDTO entity);
   /**
	 Persist a previously saved CargaValoresAmisDTO entity and return it or a copy of it to the sender. 
	 A copy of the CargaValoresAmisDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CargaValoresAmisDTO entity to update
	 @return CargaValoresAmisDTO the persisted CargaValoresAmisDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CargaValoresAmisDTO update(CargaValoresAmisDTO entity);
	public CargaValoresAmisDTO findById( CargaValoresAmisId id);
	 /**
	 * Find all CargaValoresAmisDTO entities with a specific property value.  
	 
	  @param propertyName the name of the CargaValoresAmisDTO property to query
	  @param value the property value to match
	  	  @return List<CargaValoresAmisDTO> found by query
	 */
	public List<CargaValoresAmisDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all CargaValoresAmisDTO entities.
	  	  @return List<CargaValoresAmisDTO> all CargaValoresAmisDTO entities
	 */
	public List<CargaValoresAmisDTO> findAll(
		);	
}