package mx.com.afirme.midas.catalogos.tipocambio;
// default package

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Tctipocambiomensual entity. @author MyEclipse Persistence Tools
 */
@Entity(name = "TipoCambioMensualDTO")
@Table(name = "TCTIPOCAMBIOMENSUAL", schema = "MIDAS")
public class TipoCambioMensualDTO implements Serializable{
	

	private static final long serialVersionUID = -5854427199784998710L;
	private Short idtctipocambiomensual;
	private Short mes;
	private Short anio;
	private Double tipoCambio;

	// Constructors

	/** default constructor */
	public TipoCambioMensualDTO() {
	}

	/** full constructor */
	public TipoCambioMensualDTO(Short idtctipocambiomensual, Short mes,
			Short anio, Double tipoCambio) {
		this.idtctipocambiomensual = idtctipocambiomensual;
		this.mes = mes;
		this.anio = anio;
		this.tipoCambio = tipoCambio;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTCTCMENSUAL_GENERATOR", allocationSize = 1, sequenceName = "MIDAS.IDTCTIPOCAMBIOSEQUENCE")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCTCMENSUAL_GENERATOR")
	@Column(name = "IDTCTIPOCAMBIOMENSUAL", unique = true, nullable = false, precision = 4, scale = 0)
	public Short getIdtctipocambiomensual() {
		return this.idtctipocambiomensual;
	}

	public void setIdtctipocambiomensual(Short idtctipocambiomensual) {
		this.idtctipocambiomensual = idtctipocambiomensual;
	}

	@Column(name = "MES", nullable = false, precision = 2, scale = 0)
	public Short getMes() {
		return this.mes;
	}

	public void setMes(Short mes) {
		this.mes = mes;
	}

	@Column(name = "ANIO", nullable = false, precision = 4, scale = 0)
	public Short getAnio() {
		return this.anio;
	}

	public void setAnio(Short anio) {
		this.anio = anio;
	}

	@Column(name = "TIPOCAMBIO", nullable = false, precision = 9, scale = 6)
	public Double getTipoCambio() {
		return this.tipoCambio;
	}

	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

}