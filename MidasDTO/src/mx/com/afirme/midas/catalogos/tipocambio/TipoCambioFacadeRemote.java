package mx.com.afirme.midas.catalogos.tipocambio;
// default package

import java.util.Date;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for TctipocambiomensualFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface TipoCambioFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved Tctipocambiomensual
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            Tctipocambiomensual entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoCambioMensualDTO entity);

	/**
	 * Delete a persistent Tctipocambiomensual entity.
	 * 
	 * @param entity
	 *            Tctipocambiomensual entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoCambioMensualDTO entity);

	/**
	 * Persist a previously saved Tctipocambiomensual entity and return it or a
	 * copy of it to the sender. A copy of the Tctipocambiomensual entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            Tctipocambiomensual entity to update
	 * @return Tctipocambiomensual the persisted Tctipocambiomensual entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoCambioMensualDTO update(TipoCambioMensualDTO entity);

	public TipoCambioMensualDTO findById(Short id);

	/**
	 * Find all Tctipocambiomensual entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the Tctipocambiomensual property to query
	 * @param value
	 *            the property value to match
	 * @return List<Tctipocambiomensual> found by query
	 */
	public List<TipoCambioMensualDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all Tctipocambiomensual entities.
	 * 
	 * @return List<Tctipocambiomensual> all Tctipocambiomensual entities
	 */
	public List<TipoCambioMensualDTO> findAll();
	
	public void actualizarTipoCambioMensual(Date fechaInicial,Date fechaFinal,String nombreUsuario);
}