package mx.com.afirme.midas.catalogos.tipomaquinaria;
// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for SubTipoMaquinariaFacade.
 * @author MyEclipse Persistence Tools
 */


public interface SubTipoMaquinariaFacadeRemote extends MidasInterfaceBase<SubTipoMaquinariaDTO>{
		/**
	 Perform an initial save of a previously unsaved SubTipoMaquinaria entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity SubTipoMaquinaria entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(SubTipoMaquinariaDTO entity);
    /**
	 Delete a persistent SubTipoMaquinaria entity.
	  @param entity SubTipoMaquinaria entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(SubTipoMaquinariaDTO entity);
   /**
	 Persist a previously saved SubTipoMaquinaria entity and return it or a copy of it to the sender. 
	 A copy of the SubTipoMaquinaria entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity SubTipoMaquinaria entity to update
	 @return SubTipoMaquinaria the persisted SubTipoMaquinaria entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public SubTipoMaquinariaDTO update(SubTipoMaquinariaDTO entity);
	
	 /**
	 * Find all SubTipoMaquinaria entities with a specific property value.  
	 
	  @param propertyName the name of the SubTipoMaquinaria property to query
	  @param value the property value to match
	  	  @return List<SubTipoMaquinaria> found by query
	 */
	public List<SubTipoMaquinariaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all SubTipoMaquinaria entities.
	  	  @return List<SubTipoMaquinaria> all SubTipoMaquinaria entities
	 */
	public List<SubTipoMaquinariaDTO> findAll();	
}