package mx.com.afirme.midas.catalogos.tipomaquinaria;
// default package

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

import mx.com.afirme.midas.catalogos.SubTipoGenerico;


/**
 * SubTipoMaquinaria entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCSUBTIPOMAQUINARIA"
    ,schema="MIDAS"
)
@Cache(
  type=CacheType.SOFT,
  size=1000,
  expiry=36000000
)
public class SubTipoMaquinariaDTO  extends SubTipoGenerico{


    /**
	 * 
	 */
	private static final long serialVersionUID = -617478492044874353L;
	// Fields    

     private BigDecimal idTcSubTipoMaquinaria;
     private TipoMaquinariaDTO tipoMaquinaria;
     private BigDecimal codigoSubTipoMaquinaria;
     private String descripcionSubTipoMaquinaria;
     private Short claveAutorizacion;


    // Constructors

    /** default constructor */
    public SubTipoMaquinariaDTO() {
    }

    // Property accessors
    @Id 
    
    @Column(name="IDTCSUBTIPOMAQUINARIA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdTcSubTipoMaquinaria() {
        return this.idTcSubTipoMaquinaria;
    }
    
    public void setIdTcSubTipoMaquinaria(BigDecimal idTcSubTipoMaquinaria) {
        this.idTcSubTipoMaquinaria = idTcSubTipoMaquinaria;
    }
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTCTIPOMAQUINARIA", nullable=false)

    public TipoMaquinariaDTO getTipoMaquinaria() {
        return this.tipoMaquinaria;
    }
    
    public void setTipoMaquinaria(TipoMaquinariaDTO tipoMaquinaria) {
        this.tipoMaquinaria = tipoMaquinaria;
    }
    
    @Column(name="CODIGOSUBTIPOMAQUINARIA", nullable=false, precision=22, scale=0)

    public BigDecimal getCodigoSubTipoMaquinaria() {
        return this.codigoSubTipoMaquinaria;
    }
    
    public void setCodigoSubTipoMaquinaria(BigDecimal codigoSubTipoMaquinaria) {
        this.codigoSubTipoMaquinaria = codigoSubTipoMaquinaria;
    }
    
    @Column(name="DESCRIPCIONSUBTIPOMAQUINARIA", nullable=false, length=200)

    public String getDescripcionSubTipoMaquinaria() {
        return this.descripcionSubTipoMaquinaria;
    }
    
    public void setDescripcionSubTipoMaquinaria(String descripcionSubTipoMaquinaria) {
        this.descripcionSubTipoMaquinaria = descripcionSubTipoMaquinaria;
    }
    
    @Column(name="CLAVEAUTORIZACION", nullable=false, precision=4, scale=0)

    public Short getClaveAutorizacion() {
        return this.claveAutorizacion;
    }
    
    public void setClaveAutorizacion(Short claveAutorizacion) {
        this.claveAutorizacion = claveAutorizacion;
    }

	@Override
	public String getDescription() {
		return this.descripcionSubTipoMaquinaria;
	}

	@Override
	public Object getId() {
		return this.idTcSubTipoMaquinaria;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof SubTipoMaquinariaDTO) {
			SubTipoMaquinariaDTO dto = (SubTipoMaquinariaDTO) object;
			equal = dto.getIdTcSubTipoMaquinaria().equals(this.idTcSubTipoMaquinaria);
		} // End of if
		return equal;
	}
   
}