package mx.com.afirme.midas.catalogos.tipomaquinaria;
// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for TipoMaquinariaFacade.
 * @author MyEclipse Persistence Tools
 */


public interface TipoMaquinariaFacadeRemote extends MidasInterfaceBase<TipoMaquinariaDTO>{
		/**
	 Perform an initial save of a previously unsaved TipoMaquinaria entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity TipoMaquinaria entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(TipoMaquinariaDTO entity);
    /**
	 Delete a persistent TipoMaquinaria entity.
	  @param entity TipoMaquinaria entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(TipoMaquinariaDTO entity);
   /**
	 Persist a previously saved TipoMaquinaria entity and return it or a copy of it to the sender. 
	 A copy of the TipoMaquinaria entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity TipoMaquinaria entity to update
	 @return TipoMaquinaria the persisted TipoMaquinaria entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public TipoMaquinariaDTO update(TipoMaquinariaDTO entity);
	
	 /**
	 * Find all TipoMaquinaria entities with a specific property value.  
	 
	  @param propertyName the name of the TipoMaquinaria property to query
	  @param value the property value to match
	  	  @return List<TipoMaquinaria> found by query
	 */
	public List<TipoMaquinariaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all TipoMaquinaria entities.
	  	  @return List<TipoMaquinaria> all TipoMaquinaria entities
	 */
	public List<TipoMaquinariaDTO> findAll(
		);	
}