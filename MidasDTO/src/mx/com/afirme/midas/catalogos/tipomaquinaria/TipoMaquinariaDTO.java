package mx.com.afirme.midas.catalogos.tipomaquinaria;
// default package

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;


/**
 * TipoMaquinaria entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCTIPOMAQUINARIA"
    ,schema="MIDAS"
)

public class TipoMaquinariaDTO  extends CacheableDTO {
	private static final long serialVersionUID = -1165683674679930219L;
     private BigDecimal idTcTipoMaquinaria;
     private BigDecimal codigoTipoMaquinaria;
     private String descripcionTipoMaquinaria;


    // Constructors

    /** default constructor */
    public TipoMaquinariaDTO() {
    }
   
    // Property accessors
    @Id 
    @Column(name="IDTCTIPOMAQUINARIA", nullable=false, precision=22, scale=0)
    public BigDecimal getIdTcTipoMaquinaria() {
        return this.idTcTipoMaquinaria;
    }
    
    public void setIdTcTipoMaquinaria(BigDecimal idTcTipoMaquinaria) {
        this.idTcTipoMaquinaria = idTcTipoMaquinaria;
    }
    
    @Column(name="CODIGOTIPOMAQUINARIA", nullable=false, precision=22, scale=0)

    public BigDecimal getCodigoTipoMaquinaria() {
        return this.codigoTipoMaquinaria;
    }
    
    public void setCodigoTipoMaquinaria(BigDecimal codigoTipoMaquinaria) {
        this.codigoTipoMaquinaria = codigoTipoMaquinaria;
    }
    
    @Column(name="DESCRIPCIONTIPOMAQUINARIA", nullable=false, length=200)

    public String getDescripcionTipoMaquinaria() {
        return this.descripcionTipoMaquinaria;
    }
    
    public void setDescripcionTipoMaquinaria(String descripcionTipoMaquinaria) {
        this.descripcionTipoMaquinaria = descripcionTipoMaquinaria;
    }

	@Override
	public String getDescription() {
		return this.descripcionTipoMaquinaria;
	}

	@Override
	public Object getId() {
		return this.idTcTipoMaquinaria;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof TipoMaquinariaDTO) {
			TipoMaquinariaDTO dto = (TipoMaquinariaDTO) object;
			equal = dto.getIdTcTipoMaquinaria().equals(this.idTcTipoMaquinaria);
		} // End of if
		return equal;
	}

}