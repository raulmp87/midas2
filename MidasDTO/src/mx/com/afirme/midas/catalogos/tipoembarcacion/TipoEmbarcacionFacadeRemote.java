package mx.com.afirme.midas.catalogos.tipoembarcacion;
// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for TipoEmbarcacionFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface TipoEmbarcacionFacadeRemote extends MidasInterfaceBase<TipoEmbarcacionDTO> {
	/**
	 * Perform an initial save of a previously unsaved TipoEmbarcacionDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            TipoEmbarcacionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoEmbarcacionDTO entity);

	/**
	 * Delete a persistent TipoEmbarcacionDTO entity.
	 * 
	 * @param entity
	 *            TipoEmbarcacionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoEmbarcacionDTO entity);

	/**
	 * Persist a previously saved TipoEmbarcacionDTO entity and return it or a copy
	 * of it to the sender. A copy of the TipoEmbarcacionDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoEmbarcacionDTO entity to update
	 * @return TipoEmbarcacionDTO the persisted TipoEmbarcacionDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoEmbarcacionDTO update(TipoEmbarcacionDTO entity);

	/**
	 * Find all TipoEmbarcacionDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the TipoEmbarcacionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoEmbarcacionDTO> found by query
	 */
	public List<TipoEmbarcacionDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all TipoEmbarcacionDTO entities.
	 * 
	 * @return List<TipoEmbarcacionDTO> all TipoEmbarcacionDTO entities
	 */
	public List<TipoEmbarcacionDTO> findAll();
	
	/**
	 * Find filtered TipoEmbarcacionDTO entities.
	 * 
	 * @return List<TipoEmbarcacionDTO> filtered TipoEmbarcacionDTO entities
	 */
	public List<TipoEmbarcacionDTO> listarFiltrado(TipoEmbarcacionDTO tipoEmbarcacionDTO);
}