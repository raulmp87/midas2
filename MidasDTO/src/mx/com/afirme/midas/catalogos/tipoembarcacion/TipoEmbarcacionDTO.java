package mx.com.afirme.midas.catalogos.tipoembarcacion;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.base.CacheableDTO;

/**
 * TipoEmbarcacionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCTIPOEMBARCACION", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = "CODIGOTIPOEMBARCACION"))
public class TipoEmbarcacionDTO extends CacheableDTO implements java.io.Serializable {
	private static final long serialVersionUID = -2593021566160551746L;
	// Fields

	private BigDecimal idTcTipoEmbarcacion;
	private BigDecimal codigoTipoEmbarcacion;
	private String descripcionTipoEmbarcacion;

	// Constructors

	/** default constructor */
	public TipoEmbarcacionDTO() {
	}

	/** full constructor */
	public TipoEmbarcacionDTO(BigDecimal idTcTipoEmbarcacion,
			BigDecimal codigoTipoEmbarcacion, String descripcionTipoEmbarcacion) {
		this.idTcTipoEmbarcacion = idTcTipoEmbarcacion;
		this.codigoTipoEmbarcacion = codigoTipoEmbarcacion;
		this.descripcionTipoEmbarcacion = descripcionTipoEmbarcacion;
	}

	// Property accessors
	@Id
	@Column(name = "IDTCTIPOEMBARCACION", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcTipoEmbarcacion() {
		return this.idTcTipoEmbarcacion;
	}

	public void setIdTcTipoEmbarcacion(BigDecimal idTcTipoEmbarcacion) {
		this.idTcTipoEmbarcacion = idTcTipoEmbarcacion;
	}

	@Column(name = "CODIGOTIPOEMBARCACION", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoTipoEmbarcacion() {
		return this.codigoTipoEmbarcacion;
	}

	public void setCodigoTipoEmbarcacion(BigDecimal codigoTipoEmbarcacion) {
		this.codigoTipoEmbarcacion = codigoTipoEmbarcacion;
	}

	@Column(name = "DESCRIPCIONTIPOEMBARCACION", nullable = false, length = 200)
	public String getDescripcionTipoEmbarcacion() {
		return this.descripcionTipoEmbarcacion;
	}

	public void setDescripcionTipoEmbarcacion(String descripcionTipoEmbarcacion) {
		this.descripcionTipoEmbarcacion = descripcionTipoEmbarcacion;
	}
	
	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof TipoEmbarcacionDTO) {
			TipoEmbarcacionDTO tipoEmbarcacionDTO = (TipoEmbarcacionDTO) object;
			equal = tipoEmbarcacionDTO.getIdTcTipoEmbarcacion().equals(this.getIdTcTipoEmbarcacion());
		} // End of if
		return equal;
	}
	
	@Override
	public String getDescription() {
		return this.descripcionTipoEmbarcacion;
	}
	
	@Override
	public Object getId() {
		return this.idTcTipoEmbarcacion;
	}

}