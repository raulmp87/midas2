package mx.com.afirme.midas.catalogos.tipoequipocontratista;

// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for SubtipoEquipoContratistaFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SubtipoEquipoContratistaFacadeRemote extends
		MidasInterfaceBase<SubtipoEquipoContratistaDTO> {
	/**
	 * Perform an initial save of a previously unsaved
	 * SubtipoEquipoContratistaDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            SubtipoEquipoContratistaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SubtipoEquipoContratistaDTO entity);

	/**
	 * Delete a persistent SubtipoEquipoContratistaDTO entity.
	 * 
	 * @param entity
	 *            SubtipoEquipoContratistaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SubtipoEquipoContratistaDTO entity);

	/**
	 * Persist a previously saved SubtipoEquipoContratistaDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * SubtipoEquipoContratistaDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            SubtipoEquipoContratistaDTO entity to update
	 * @return SubtipoEquipoContratistaDTO the persisted
	 *         SubtipoEquipoContratistaDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SubtipoEquipoContratistaDTO update(SubtipoEquipoContratistaDTO entity);

	/**
	 * Find all SubtipoEquipoContratistaDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the SubtipoEquipoContratistaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SubtipoEquipoContratistaDTO> found by query
	 */
	public List<SubtipoEquipoContratistaDTO> findByProperty(
			String propertyName, Object value);

	public List<SubtipoEquipoContratistaDTO> listarFiltrado(
			SubtipoEquipoContratistaDTO subtipoEquipoContratistaDTO);
}