package mx.com.afirme.midas.catalogos.tipoequipocontratista;

// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for TipoEquipoContratistaFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface TipoEquipoContratistaFacadeRemote extends
		MidasInterfaceBase<TipoEquipoContratistaDTO> {
	/**
	 * Perform an initial save of a previously unsaved TipoEquipoContratistaDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            TipoEquipoContratistaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoEquipoContratistaDTO entity);

	/**
	 * Delete a persistent TipoEquipoContratistaDTO entity.
	 * 
	 * @param entity
	 *            TipoEquipoContratistaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoEquipoContratistaDTO entity);

	/**
	 * Persist a previously saved TipoEquipoContratistaDTO entity and return it
	 * or a copy of it to the sender. A copy of the TipoEquipoContratistaDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoEquipoContratistaDTO entity to update
	 * @return TipoEquipoContratistaDTO the persisted TipoEquipoContratistaDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoEquipoContratistaDTO update(TipoEquipoContratistaDTO entity);

	/**
	 * Find all TipoEquipoContratistaDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the TipoEquipoContratistaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoEquipoContratistaDTO> found by query
	 */
	public List<TipoEquipoContratistaDTO> findByProperty(String propertyName,
			Object value);

	public List<TipoEquipoContratistaDTO> listarFiltrado(
			TipoEquipoContratistaDTO tipoEquipoContratistaDTO);
}