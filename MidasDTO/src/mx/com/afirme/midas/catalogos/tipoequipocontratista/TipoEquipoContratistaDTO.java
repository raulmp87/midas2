package mx.com.afirme.midas.catalogos.tipoequipocontratista;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.base.CacheableDTO;

/**
 * TipoEquipoContratistaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name = "TipoEquipoContratistaDTO")
@Table(name = "TCTIPOEQUIPOCONTRATISTA", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = "CODIGOTIPOEQUIPOCONTRATISTA"))
public class TipoEquipoContratistaDTO extends CacheableDTO {

	private static final long serialVersionUID = 8217679150617563215L;

	private BigDecimal idTipoEquipoContratista;
	private BigDecimal codigoTipoEquipoContratista;
	private String descripcionTipoEqContr;
	private List<SubtipoEquipoContratistaDTO> subtipoEquipoContratistaDTOs = new ArrayList<SubtipoEquipoContratistaDTO>();

	// Constructors
	public TipoEquipoContratistaDTO() {
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTCTIPOEQUIPOCONTRAT_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCTIPOEQUIPOCONTRAT_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCTIPOEQUIPOCONTRAT_SEQ_GENERADOR")
	@Column(name = "IDTCTIPOEQUIPOCONTRATISTA", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTipoEquipoContratista() {
		return this.idTipoEquipoContratista;
	}

	public void setIdTipoEquipoContratista(BigDecimal idTipoEquipoContratista) {
		this.idTipoEquipoContratista = idTipoEquipoContratista;
	}

	@Column(name = "CODIGOTIPOEQUIPOCONTRATISTA", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoTipoEquipoContratista() {
		return this.codigoTipoEquipoContratista;
	}

	public void setCodigoTipoEquipoContratista(
			BigDecimal codigoTipoEquipoContratista) {
		this.codigoTipoEquipoContratista = codigoTipoEquipoContratista;
	}

	@Column(name = "DESCRIPCIONTIPOEQCONTR", nullable = false, length = 200)
	public String getDescripcionTipoEqContr() {
		return this.descripcionTipoEqContr;
	}

	public void setDescripcionTipoEqContr(String descripcionTipoEqContr) {
		this.descripcionTipoEqContr = descripcionTipoEqContr;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tipoEquipoContratistaDTO")
	public List<SubtipoEquipoContratistaDTO> getSubtipoEquipoContratistaDTOs() {
		return subtipoEquipoContratistaDTOs;
	}

	public void setSubtipoEquipoContratistaDTOs(
			List<SubtipoEquipoContratistaDTO> subtipoEquipoContratistaDTOs) {
		this.subtipoEquipoContratistaDTOs = subtipoEquipoContratistaDTOs;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o instanceof TipoEquipoContratistaDTO) {
			if (((TipoEquipoContratistaDTO) o).getCodigoTipoEquipoContratista()
					.equals(this.getCodigoTipoEquipoContratista())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String getDescription() {
		return this.descripcionTipoEqContr;
	}

	@Override
	public String getId() {
		return this.idTipoEquipoContratista.toString();
	}
	
}