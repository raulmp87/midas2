package mx.com.afirme.midas.catalogos.tipoequipocontratista;

// default package

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

import mx.com.afirme.midas.catalogos.SubTipoGenerico;

/**
 * SubtipoEquipoContratistaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name = "SubtipoEquipoContratistaDTO")
@Table(name = "TCSUBTIPOEQUIPOCONTRATISTA", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = {
		"IDTCTIPOEQUIPOCONTRATISTA", "CODIGOSUBTIPOEQUIPOCONTRATISTA" }))
@Cache(
  type=CacheType.SOFT,
  size=1000,
  expiry=36000000
)
public class SubtipoEquipoContratistaDTO extends SubTipoGenerico{

	private static final long serialVersionUID = -2547749751120696503L;

	private BigDecimal idSubtipoEquipoContratista;
	private TipoEquipoContratistaDTO tipoEquipoContratistaDTO;
	private BigDecimal codigoSubtipoEquipoContratista;
	private String descripcionSubtipoEqCont;
	private Short claveAutorizacion;

	// Constructors

	/** default constructor */
	public SubtipoEquipoContratistaDTO() {
		if (this.tipoEquipoContratistaDTO == null) {
			this.tipoEquipoContratistaDTO = new TipoEquipoContratistaDTO();
		}
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTCSUBTIPOEQUIPOCONTRAT_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCSUBTIPOEQUIPOCONTRAT_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCSUBTIPOEQUIPOCONTRAT_SEQ_GENERADOR")
	@Column(name = "IDTCSUBTIPOEQUIPOCONTRATISTA", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdSubtipoEquipoContratista() {
		return this.idSubtipoEquipoContratista;
	}

	public void setIdSubtipoEquipoContratista(
			BigDecimal idSubtipoEquipoContratista) {
		this.idSubtipoEquipoContratista = idSubtipoEquipoContratista;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCTIPOEQUIPOCONTRATISTA", nullable = false)
	public TipoEquipoContratistaDTO getTipoEquipoContratistaDTO() {
		return tipoEquipoContratistaDTO;
	}

	public void setTipoEquipoContratistaDTO(
			TipoEquipoContratistaDTO tipoEquipoContratistaDTO) {
		this.tipoEquipoContratistaDTO = tipoEquipoContratistaDTO;
	}

	@Column(name = "CODIGOSUBTIPOEQUIPOCONTRATISTA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoSubtipoEquipoContratista() {
		return this.codigoSubtipoEquipoContratista;
	}

	public void setCodigoSubtipoEquipoContratista(
			BigDecimal codigoSubtipoEquipoContratista) {
		this.codigoSubtipoEquipoContratista = codigoSubtipoEquipoContratista;
	}

	@Column(name = "DESCRIPCIONSUBTIPOEQCONT", nullable = false, length = 200)
	public String getDescripcionSubtipoEqCont() {
		return this.descripcionSubtipoEqCont;
	}

	public void setDescripcionSubtipoEqCont(String descripcionSubtipoEqCont) {
		this.descripcionSubtipoEqCont = descripcionSubtipoEqCont;
	}

	@Column(name = "CLAVEAUTORIZACION", nullable = false, precision = 4, scale = 0)
	public Short getClaveAutorizacion() {
		return this.claveAutorizacion;
	}

	public void setClaveAutorizacion(Short claveAutorizacion) {
		this.claveAutorizacion = claveAutorizacion;
	}

	@Override
	public String getDescription() {
		return this.descripcionSubtipoEqCont;
	}

	@Override
	public Object getId() {
		return this.idSubtipoEquipoContratista;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof SubtipoEquipoContratistaDTO) {
			SubtipoEquipoContratistaDTO subtipoEquipoContratistaDTO = (SubtipoEquipoContratistaDTO) object;
			equal = subtipoEquipoContratistaDTO.getIdSubtipoEquipoContratista()
					.equals(this.idSubtipoEquipoContratista);
		} // End of if
		return equal;
	}
}