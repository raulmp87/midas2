package mx.com.afirme.midas.catalogos.coasegurador;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.catalogos.impuestoresidenciafiscal.ImpuestoResidenciaFiscalDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

import org.eclipse.persistence.annotations.Customizer;

@Entity(name = "CoaseguradorHistorico")
@Table(name="TCCOASEGURADOR_HIST", schema="MIDAS")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class CoaseguradorHistorico implements java.io.Serializable, Entidad {
	// Fields    
	 private static final long serialVersionUID = 1L;
	 public static final BigDecimal CORREDOR = new BigDecimal("0");
	 public static final BigDecimal REASEGURADOR = new BigDecimal("1");
    private BigDecimal id;
    private BigDecimal idOrigen;
    private String nombre;
    private String ubicacion;
    private String correoelectronico;
    private String cnfs;
    private String tipo;
    private BigDecimal estatus;
    private String nombrecorto;
    private String procedencia;
    private String ciudad;
    private String estado;
    private String pais;
    private String telefonofijo;
    private String telefonomovil;
	 private BigDecimal idContable;    
	 private String Rfc;
	 private ImpuestoResidenciaFiscalDTO impuestoResidenciaFiscal;
	 private Short tieneConstanciaResidenciaFiscal;
	 private Date fechaInicioVigenciaConstanciaResidenciaFiscal;
	 private Date fechaFinVigenciaConstanciaResidenciaFiscal;
    private String calle;
    private String numeroExterior;
    private String numeroInterior;
    private String colonia;
    private String descripcionCiudad;
    private String descripcionEstado;
    private String descripcionPais;
    private String codigoPostal;
    private Date fechaInicioRegistro;
	private Date fechaFinRegistro;


   // Constructors

   /** default constructor */
   public CoaseguradorHistorico() {
   }

	
   // Property accessors
   @Id 
   @SequenceGenerator(name = "COAS_HIST_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.TCREASCOAS_HIST_SEQ")
   @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COAS_HIST_SEQ_GENERADOR")	
   @Column(name="ID", unique=true, nullable=false, precision=22, scale=0)
   public BigDecimal getId() {
       return this.id;
   }
   
   public void setId(BigDecimal id) {
       this.id = id;
   }
   
   @Column(name="IDTCCOASEGURADOR", nullable=false, precision=22, scale=0)
   public BigDecimal getIdOrigen() {
	   return idOrigen;
   }
   
   public void setIdOrigen(BigDecimal idOrigen) {
	   this.idOrigen = idOrigen;
   }
   
   @Column(name="NOMBRE", nullable=false, length=100)

   public String getNombre() {
       return this.nombre;
   }
   
   public void setNombre(String nombre) {
       this.nombre = nombre;
   }
   
   @Column(name="UBICACION", nullable=false, length=250)

   public String getUbicacion() {
       return this.ubicacion;
   }
   
   public void setUbicacion(String ubicacion) {
       this.ubicacion = ubicacion;
   }
   
   @Column(name="CORREOELECTRONICO", nullable=false, length=250)

   public String getCorreoelectronico() {
       return this.correoelectronico;
   }
   
   public void setCorreoelectronico(String correoelectronico) {
       this.correoelectronico = correoelectronico;
   }
   
   @Column(name="CNSF", nullable=false, length=50)

   public String getCnfs() {
       return this.cnfs;
   }
   
   public void setCnfs(String cnfs) {
       this.cnfs = cnfs;
   }
   
   @Column(name="TIPO", nullable=false, length=1)

   public String getTipo() {
       return this.tipo;
   }
   
   public void setTipo(String tipo) {
       this.tipo = tipo;
   }
   
   @Column(name="ESTATUS", nullable=false, precision=22, scale=0)

   public BigDecimal getEstatus() {
       return this.estatus;
   }
   
   public void setEstatus(BigDecimal estatus) {
       this.estatus = estatus;
   }
   
   @Column(name="NOMBRECORTO", nullable=false, length=30)

   public String getNombrecorto() {
       return this.nombrecorto;
   }
   
   public void setNombrecorto(String nombrecorto) {
       this.nombrecorto = nombrecorto;
   }
   
   @Column(name="PROCEDENCIA", nullable=false, length=1)

   public String getProcedencia() {
       return this.procedencia;
   }
   
   public void setProcedencia(String procedencia) {
       this.procedencia = procedencia;
   }
   
   @Column(name="CIUDAD", nullable=false, length=50)

   public String getCiudad() {
       return this.ciudad;
   }
   
   public void setCiudad(String ciudad) {
       this.ciudad = ciudad;
   }
   
   @Column(name="ESTADO", nullable=false, length=50)

   public String getEstado() {
       return this.estado;
   }
   
   public void setEstado(String estado) {
       this.estado = estado;
   }
   
   @Column(name="PAIS", nullable=false, length=50)

   public String getPais() {
       return this.pais;
   }
   
   public void setPais(String pais) {
       this.pais = pais;
   }
   
   @Column(name="TELEFONOFIJO", nullable=false, length=15)

   public String getTelefonofijo() {
       return this.telefonofijo;
   }
   
   public void setTelefonofijo(String telefonofijo) {
       this.telefonofijo = telefonofijo;
   }
   
   @Column(name="TELEFONOMOVIL", length=15)

   public String getTelefonomovil() {
       return this.telefonomovil;
   }
   
   public void setTelefonomovil(String telefonomovil) {
       this.telefonomovil = telefonomovil;
   }
   //--------------------------
   @Column(name="RFC", length=13)

   public String getRfc() {
       return this.Rfc;
   }
   
   public void setRfc(String rfc) {
       this.Rfc = rfc;
   }
   //------------------------
   
   
   @Column(name="CALLE", nullable=false, length=100)
   public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	@Column(name="NOEXTERIOR", nullable=false, length=10)
	public String getNumeroExterior() {
		return numeroExterior;
	}

	public void setNumeroExterior(String numeroExterior) {
		this.numeroExterior = numeroExterior;
	}

	@Column(name="NOINTERIOR", nullable=false, length=10)
	public String getNumeroInterior() {
		return numeroInterior;
	}

	public void setNumeroInterior(String numeroInterior) {
		this.numeroInterior = numeroInterior;
	}

	@Column(name="COLONIA", nullable=false, length=100)
	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	@Column(name="CIUDAD_DESC", nullable=false, length=100)
	public String getDescripcionCiudad() {
		return descripcionCiudad;
	}

	public void setDescripcionCiudad(String descripcionCiudad) {
		this.descripcionCiudad = descripcionCiudad;
	}

	@Column(name="ESTADO_DESC", nullable=false, length=100)
	public String getDescripcionEstado() {
		return descripcionEstado;
	}

	public void setDescripcionEstado(String descripcionEstado) {
		this.descripcionEstado = descripcionEstado;
	}

	@Column(name="PAIS_DESC", nullable=false, length=100)
	public String getDescripcionPais() {
		return descripcionPais;
	}

	public void setDescripcionPais(String descripcionPais) {
		this.descripcionPais = descripcionPais;
	}

	@Column(name="CODIGOPOSTAL", nullable=false, length=10)
	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof CoaseguradorHistorico) {
			CoaseguradorHistorico coaseguradorHistorico = (CoaseguradorHistorico) object;
			equal = coaseguradorHistorico.getId().equals(this.getId());
		} // End of if
		return equal;
	}
		
	@Column(name = "IDCONTABLE", precision = 22, scale = 0)
	public BigDecimal getIdContable() {
		return this.idContable;
	}

	public void setIdContable(BigDecimal idContable) {
		this.idContable = idContable;
	}

	@ManyToOne(fetch=FetchType.EAGER)
   	@JoinColumn(name="IDTCIMPUESTORESIDENCIAFISCAL", nullable=true)
	public ImpuestoResidenciaFiscalDTO getImpuestoResidenciaFiscal() {
		return impuestoResidenciaFiscal;
	}

	public void setImpuestoResidenciaFiscal(ImpuestoResidenciaFiscalDTO impuestoResidenciaFiscal) {
		this.impuestoResidenciaFiscal = impuestoResidenciaFiscal;
	}

	@Column(name = "TIENECONSTANCIARESIDFISCAL", precision = 1, scale = 0, nullable=true)
	public Short getTieneConstanciaResidenciaFiscal() {
		return tieneConstanciaResidenciaFiscal;
	}

	public void setTieneConstanciaResidenciaFiscal(
			Short tieneConstanciaResidenciaFiscal) {
		this.tieneConstanciaResidenciaFiscal = tieneConstanciaResidenciaFiscal;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAINICIOVIGCONSTRESFISCAL", nullable=true)
	public Date getFechaInicioVigenciaConstanciaResidenciaFiscal() {
		return fechaInicioVigenciaConstanciaResidenciaFiscal;
	}

	public void setFechaInicioVigenciaConstanciaResidenciaFiscal(
			Date fechaInicioVigenciaConstanciaResidenciaFiscal) {
		this.fechaInicioVigenciaConstanciaResidenciaFiscal = fechaInicioVigenciaConstanciaResidenciaFiscal;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAFINVIGCONSTRESFISCAL", nullable=true)
	public Date getFechaFinVigenciaConstanciaResidenciaFiscal() {
		return fechaFinVigenciaConstanciaResidenciaFiscal;
	}

	public void setFechaFinVigenciaConstanciaResidenciaFiscal(
			Date fechaFinVigenciaConstanciaResidenciaFiscal) {
		this.fechaFinVigenciaConstanciaResidenciaFiscal = fechaFinVigenciaConstanciaResidenciaFiscal;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "H_START_DATE", nullable = false, length = 7)
	public Date getFechaInicioRegistro() {
		return fechaInicioRegistro;
	}

	public void setFechaInicioRegistro(Date fechaInicioRegistro) {
		this.fechaInicioRegistro = fechaInicioRegistro;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "H_END_DATE", nullable = false, length = 7)
	public Date getFechaFinRegistro() {
		return fechaFinRegistro;
	}

	public void setFechaFinRegistro(Date fechaFinRegistro) {
		this.fechaFinRegistro = fechaFinRegistro;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return nombre + " (" + cnfs + ")";
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getBusinessKey() {
		return cnfs;
	}	
}
