package mx.com.afirme.midas.catalogos.estilovehiculo;
// default package

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;

import mx.com.afirme.midas2.validator.group.CotizacionExpressPaso1Checks;

import org.apache.bval.constraints.NotEmpty;


/**
 * EstiloVehiculoDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class EstiloVehiculoId  implements java.io.Serializable {

	private static final long serialVersionUID = 5935377619654299926L;
	private String claveTipoBien;
     private String claveEstilo;
     private BigDecimal idVersionCarga;


    // Constructors

    /** default constructor */
    public EstiloVehiculoId() {
    }

    
    /** full constructor */
    public EstiloVehiculoId(String claveTipoBien, String claveEstilo, BigDecimal idVersionCarga) {
        this.claveTipoBien = claveTipoBien;
        this.claveEstilo = claveEstilo;
        this.idVersionCarga = idVersionCarga;
    }

   
    // Property accessors

    @Column(name="CLAVETIPOBIEN", nullable=false, length=5)

    public String getClaveTipoBien() {
        return this.claveTipoBien!=null?this.claveTipoBien.toUpperCase():this.claveTipoBien;
    }
    
    public void setClaveTipoBien(String claveTipoBien) {
        this.claveTipoBien = claveTipoBien;
    }

    @Column(name="CLAVEESTILO", nullable=false, length=8)
    public String getClaveEstilo() {
        return this.claveEstilo!=null?this.claveEstilo.toUpperCase():this.claveEstilo;
    }
    
    public void setClaveEstilo(String claveEstilo) {
        this.claveEstilo = claveEstilo;
    }

    @Column(name="IDVERSIONCARGA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdVersionCarga() {
        return this.idVersionCarga;
    }
    
    public void setIdVersionCarga(BigDecimal idVersionCarga) {
        this.idVersionCarga = idVersionCarga;
    }
   

    @Override
    public String toString(){
    	if (claveTipoBien != null && claveEstilo != null && idVersionCarga != null) {
        	return ""+claveTipoBien+"_"+claveEstilo+"_"+idVersionCarga;
    	}
    	//Se esta regresando vacio para evitar que algo se haya desarrollado esperando este comportamiento falle. Lo normal seria que regresara nulo.
    	return ""; 
    }
    
    public void valueOf(String idString){
    	if(idString!= null && !idString.trim().equals("")){
	    	String[] ids = idString.split("_");
	    	if(ids.length == 3){
	    		claveTipoBien = ids[0];
	    		claveEstilo = ids[1];
	    		idVersionCarga = new BigDecimal(ids[2]);
	    	}
    	}
    }
    
    @NotEmpty(groups=CotizacionExpressPaso1Checks.class, message="{com.afirme.midas2.requerido}")
    @Transient
    public String getStrId() {
    	return toString();
    }

    /**
     * Metodo de conveniencia que asigna el Id en base a un <code>String</code>. Se utiliza convenciones JavaBeans para que pueda ser asignado desde el
     * web framework.
     */
    public void setStrId(String strId) {
    	valueOf(strId);
    }
    
   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof EstiloVehiculoId) ) return false;
		 EstiloVehiculoId castOther = ( EstiloVehiculoId ) other; 
         
		 return ( (this.getClaveTipoBien()==castOther.getClaveTipoBien()) || ( this.getClaveTipoBien()!=null && castOther.getClaveTipoBien()!=null && this.getClaveTipoBien().equals(castOther.getClaveTipoBien()) ) )
 && ( (this.getClaveEstilo()==castOther.getClaveEstilo()) || ( this.getClaveEstilo()!=null && castOther.getClaveEstilo()!=null && this.getClaveEstilo().equals(castOther.getClaveEstilo()) ) )
 && ( (this.getIdVersionCarga()==castOther.getIdVersionCarga()) || ( this.getIdVersionCarga()!=null && castOther.getIdVersionCarga()!=null && this.getIdVersionCarga().equals(castOther.getIdVersionCarga()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getClaveTipoBien() == null ? 0 : this.getClaveTipoBien().hashCode() );
         result = 37 * result + ( getClaveEstilo() == null ? 0 : this.getClaveEstilo().hashCode() );
         result = 37 * result + ( getIdVersionCarga() == null ? 0 : this.getIdVersionCarga().hashCode() );
         return result;
   }   





}