package mx.com.afirme.midas.catalogos.estilovehiculo.grupo;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * EstiloVehiculoGruposDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class EstiloVehiculoGrupoId  implements java.io.Serializable {

	private static final long serialVersionUID = -4089847320068752023L;
	private String claveTipoBien;
     private String claveEstilo;
     private BigDecimal idVersionCarga;


    // Constructors

    /** default constructor */
    public EstiloVehiculoGrupoId() {
    }

    
    /** full constructor */
    public EstiloVehiculoGrupoId(String claveTipoBien, String claveEstilo, BigDecimal idVersionCarga) {
        this.claveTipoBien = claveTipoBien;
        this.claveEstilo = claveEstilo;
        this.idVersionCarga = idVersionCarga;
    }

   
    // Property accessors

    @Column(name="CLAVETIPOBIEN", nullable=false, length=5)

    public String getClaveTipoBien() {
        return this.claveTipoBien;
    }
    
    public void setClaveTipoBien(String claveTipoBien) {
        this.claveTipoBien = claveTipoBien;
    }

    @Column(name="CLAVEESTILO", nullable=false, length=8)

    public String getClaveEstilo() {
        return this.claveEstilo;
    }
    
    public void setClaveEstilo(String claveEstilo) {
        this.claveEstilo = claveEstilo;
    }

    @Column(name="IDVERSIONCARGA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdVersionCarga() {
        return this.idVersionCarga;
    }
    
    public void setIdVersionCarga(BigDecimal idVersionCarga) {
        this.idVersionCarga = idVersionCarga;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof EstiloVehiculoGrupoId) ) return false;
		 EstiloVehiculoGrupoId castOther = ( EstiloVehiculoGrupoId ) other; 
         
		 return ( (this.getClaveTipoBien()==castOther.getClaveTipoBien()) || ( this.getClaveTipoBien()!=null && castOther.getClaveTipoBien()!=null && this.getClaveTipoBien().equals(castOther.getClaveTipoBien()) ) )
 && ( (this.getClaveEstilo()==castOther.getClaveEstilo()) || ( this.getClaveEstilo()!=null && castOther.getClaveEstilo()!=null && this.getClaveEstilo().equals(castOther.getClaveEstilo()) ) )
 && ( (this.getIdVersionCarga()==castOther.getIdVersionCarga()) || ( this.getIdVersionCarga()!=null && castOther.getIdVersionCarga()!=null && this.getIdVersionCarga().equals(castOther.getIdVersionCarga()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getClaveTipoBien() == null ? 0 : this.getClaveTipoBien().hashCode() );
         result = 37 * result + ( getClaveEstilo() == null ? 0 : this.getClaveEstilo().hashCode() );
         result = 37 * result + ( getIdVersionCarga() == null ? 0 : this.getIdVersionCarga().hashCode() );
         return result;
   }   





}