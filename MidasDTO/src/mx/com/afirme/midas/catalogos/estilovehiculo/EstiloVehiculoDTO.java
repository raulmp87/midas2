package mx.com.afirme.midas.catalogos.estilovehiculo;
// default package

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.marcavehiculo.SubMarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipobienautos.TipoBienAutosDTO;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;
import mx.com.afirme.midas2.annotation.Exportable;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.estilovehiculo.NegocioEstiloVehiculo;


/**
 * EstiloVehiculoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCESTILOVEHICULO"
    ,schema="MIDAS"
)

public class EstiloVehiculoDTO  extends CacheableDTO implements Entidad{

	private static final long serialVersionUID = -2134265149309048573L;
	private EstiloVehiculoId id = new EstiloVehiculoId();
     private MarcaVehiculoDTO marcaVehiculoDTO = new MarcaVehiculoDTO();
     private SubMarcaVehiculoDTO subMarcaVehiculoDTO = new SubMarcaVehiculoDTO();
     private BigDecimal idTcTipoVehiculo;
     private TipoBienAutosDTO tipoBienAutosDTO = new TipoBienAutosDTO();
     private Date fechaInicioVigencia;
     private Date fechaFinVigencia;
     private String descripcionEstilo;
     private Short numeroAsientos;
     private Short numeroPuertas;
     private Double capacidadToneladas;
     private String claveUnidadMedida;
     private String claveCatalogoFabricante;
     private Date fechaCreacion;
     private String codigoUsuarioCreacion;
     private String claveTransmision;
     private String claveCilindros;
     private String claveBolsasAire;
     private String claveSistemaElectrico;
     private String claveVestidura;
     private String claveQuemacocos;
     private String claveVersion;
     private String claveFrenos;
     private String claveSonido;
     private String claveAireAcondicionado;
     private String claveTipoCombustible;
     private String claveAlarmaFabricante;
     private String claveTipoInyeccion;
     private String descripcionBase;
     private TipoVehiculoDTO tipoVehiculoDTO = new TipoVehiculoDTO();
     private List<NegocioEstiloVehiculo> negocioEstiloVehiculos;
     private Boolean altoRiesgo;
     private Boolean altaFrecuencia;
     private String claveSesas;
     private String claveAmis;

     //Transient
     private Short modeloVehiculo;
     private Boolean tipoSubMarcaVacio;

     
     
    /* 
     private BigDecimal idVersionCarga;*/
     
     @Transient
     @Exportable(columnName="Clave_Estilo", columnOrder=0)
    public String getClaveEstilo() {
    	 return this.id.getClaveEstilo();
	}
     
    @Transient
    @Exportable(columnName="Clave_Tipo_Bien", columnOrder=1)
    public String getClaveTipoBien() {
    	 return this.id.getClaveTipoBien();
	}
    
    @Transient
    @Exportable(columnName="Version_Carga", columnOrder=2)
    public BigDecimal getIdVersionCarga() {
    	 return this.id.getIdVersionCarga();
	}

	/*@Transient
	@Exportable(columnName="Numero_Marca", columnOrder=4)
	public BigDecimal getNumeoMarca() {
		if(this.marcaVehiculoDTO!=null && this.marcaVehiculoDTO.getIdTcMarcaVehiculo()!=null ){
			return this.marcaVehiculoDTO.getIdTcMarcaVehiculo();
		}else{
			return null;
		}
		
	}
	@Transient
	@Exportable(columnName="Codigo_Marca", columnOrder=5)
	public String getCodigoMarca() {
		if(this.marcaVehiculoDTO!=null  && !StringUtil.isEmpty(this.marcaVehiculoDTO.getCodigoMarcaVehiculo())){
			return this.marcaVehiculoDTO.getCodigoMarcaVehiculo();
		}else{
			return null;
		}
		
	}*/
	
	@Transient
	@Exportable(columnName="Marca", columnOrder=4)
	public String getMarca() {
		if(this.marcaVehiculoDTO!=null && !StringUtil.isEmpty(this.marcaVehiculoDTO.getDescripcionMarcaVehiculo())){
			return this.marcaVehiculoDTO.getDescripcionMarcaVehiculo();
		}else{
			return null;
		}
		
	}


	@Transient
	@Exportable(columnName="Tipo", columnOrder=5)
	public String getSubmarca() {
		if(this.subMarcaVehiculoDTO!=null && subMarcaVehiculoDTO.getIdSubTcMarcaVehiculo()!=null  ){
			return this.subMarcaVehiculoDTO.getDescripcionSubMarcaVehiculo();
		}else{
			return null;
		}
	}


	  // Constructors


	/** default constructor */
    public EstiloVehiculoDTO() {
    }

	/** minimal constructor */
    public EstiloVehiculoDTO(EstiloVehiculoId id, MarcaVehiculoDTO marcaVehiculoDTO, 
    		BigDecimal idTcTipoVehiculo, TipoBienAutosDTO tipoBienAutosDTO, 
    		Date fechaInicioVigencia, Date fechaFinVigencia, Short numeroPuertas, 
    		Date fechaCreacion, String codigoUsuarioCreacion,SubMarcaVehiculoDTO subMarcaVehiculoDTO ) {
        this.id = id;
        this.marcaVehiculoDTO = marcaVehiculoDTO;
        this.subMarcaVehiculoDTO=subMarcaVehiculoDTO;
        this.idTcTipoVehiculo = idTcTipoVehiculo;
        this.tipoBienAutosDTO = tipoBienAutosDTO;
        this.fechaInicioVigencia = fechaInicioVigencia;
        this.fechaFinVigencia = fechaFinVigencia;
        this.numeroPuertas = numeroPuertas;
        this.fechaCreacion = fechaCreacion;
        this.codigoUsuarioCreacion = codigoUsuarioCreacion;
    }
    
    /** full constructor */
    public EstiloVehiculoDTO(EstiloVehiculoId id, MarcaVehiculoDTO marcaVehiculoDTO, 
    		BigDecimal idTcTipoVehiculo, TipoBienAutosDTO tipoBienAutosDTO, 
    		Date fechaInicioVigencia, Date fechaFinVigencia, String descripcionEstilo, 
    		Short numeroAsientos, Short numeroPuertas, Double capacidadToneladas, 
    		String claveUnidadMedida, String claveCatalogoFabricante, Date fechaCreacion, 
    		String codigoUsuarioCreacion, String claveTransmision, String claveCilindros, 
    		String claveBolsasAire, String claveSistemaElectrico, 
    		String claveVestidura, String claveQuemacocos, String claveVersion, 
    		String claveFrenos, String claveSonido, String claveAireAcondicionado, 
    		String claveTipoCombustible, String claveAlarmaFabricante, String claveTipoInyeccion,SubMarcaVehiculoDTO subMarcaVehiculoDTO) {
        this.id = id;
        this.marcaVehiculoDTO = marcaVehiculoDTO;
        this.idTcTipoVehiculo = idTcTipoVehiculo;
        this.tipoBienAutosDTO = tipoBienAutosDTO;
        this.fechaInicioVigencia = fechaInicioVigencia;
        this.fechaFinVigencia = fechaFinVigencia;
        this.descripcionEstilo = descripcionEstilo;
        this.numeroAsientos = numeroAsientos;
        this.numeroPuertas = numeroPuertas;
        this.capacidadToneladas = capacidadToneladas;
        this.claveUnidadMedida = claveUnidadMedida;
        this.claveCatalogoFabricante = claveCatalogoFabricante;
        this.fechaCreacion = fechaCreacion;
        this.codigoUsuarioCreacion = codigoUsuarioCreacion;
        this.claveTransmision = claveTransmision;
        this.claveCilindros = claveCilindros;
        this.claveBolsasAire = claveBolsasAire;
        this.claveSistemaElectrico = claveSistemaElectrico;
        this.claveVestidura = claveVestidura;
        this.claveQuemacocos = claveQuemacocos;
        this.claveVersion = claveVersion;
        this.claveFrenos = claveFrenos;
        this.claveSonido = claveSonido;
        this.claveAireAcondicionado = claveAireAcondicionado;
        this.claveTipoCombustible = claveTipoCombustible;
        this.claveAlarmaFabricante = claveAlarmaFabricante;
        this.claveTipoInyeccion = claveTipoInyeccion;
        this.subMarcaVehiculoDTO= subMarcaVehiculoDTO;
    }

   
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="claveTipoBien", column=@Column(name="CLAVETIPOBIEN", nullable=false, length=5) ), 
        @AttributeOverride(name="claveEstilo", column=@Column(name="CLAVEESTILO", nullable=false, length=8) ), 
        @AttributeOverride(name="idVersionCarga", column=@Column(name="IDVERSIONCARGA", nullable=false, precision=22, scale=0) ) } )

    public EstiloVehiculoId getId() {
        return this.id;
    }
    
    public void setId(EstiloVehiculoId id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTCMARCAVEHICULO", nullable=false)

    public MarcaVehiculoDTO getMarcaVehiculoDTO() {
        return this.marcaVehiculoDTO;
    }
    
    public void setMarcaVehiculoDTO(MarcaVehiculoDTO marcaVehiculoDTO) {
        this.marcaVehiculoDTO = marcaVehiculoDTO;
    }
   
    
    
    
    
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTCSUBMARCAVEHICULO", nullable=true)
    public SubMarcaVehiculoDTO getSubMarcaVehiculoDTO() {
		return subMarcaVehiculoDTO;
	}

	public void setSubMarcaVehiculoDTO(SubMarcaVehiculoDTO subMarcaVehiculoDTO) {
		this.subMarcaVehiculoDTO = subMarcaVehiculoDTO;
	}

	@Column(name="IDTCTIPOVEHICULO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdTcTipoVehiculo() {
        return this.idTcTipoVehiculo;
    }
    
    public void setIdTcTipoVehiculo(BigDecimal idTcTipoVehiculo) {
        this.idTcTipoVehiculo = idTcTipoVehiculo;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="CLAVETIPOBIEN", nullable=false, insertable=false, updatable=false)

    public TipoBienAutosDTO getTipoBienAutosDTO() {
        return this.tipoBienAutosDTO;
    }
    
    public void setTipoBienAutosDTO(TipoBienAutosDTO tipoBienAutosDTO) {
        this.tipoBienAutosDTO = tipoBienAutosDTO;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="fechaInicioVigencia", nullable=false, length=7)

    public Date getFechaInicioVigencia() {
        return this.fechaInicioVigencia;
    }
    
    public void setFechaInicioVigencia(Date fechaInicioVigencia) {
        this.fechaInicioVigencia = fechaInicioVigencia;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="fechaFinVigencia", nullable=false, length=7)

    public Date getFechaFinVigencia() {
        return this.fechaFinVigencia;
    }
    
    public void setFechaFinVigencia(Date fechaFinVigencia) {
        this.fechaFinVigencia = fechaFinVigencia;
    }
    
    @Column(name="descripcionEstilo")
    @Exportable(columnName="Descripcion", columnOrder=3)
    public String getDescripcionEstilo() {
        return this.descripcionEstilo!=null?this.descripcionEstilo.toUpperCase():this.descripcionEstilo;
    }
    
    public void setDescripcionEstilo(String descripcionEstilo) {
        this.descripcionEstilo = descripcionEstilo;
    }
    
    @Column(name="numeroAsientos", precision=4, scale=0)

    public Short getNumeroAsientos() {
        return this.numeroAsientos;
    }
    
    public void setNumeroAsientos(Short numeroAsientos) {
        this.numeroAsientos = numeroAsientos;
    }
    
    @Column(name="numeroPuertas", nullable=false, precision=2, scale=0)

    public Short getNumeroPuertas() {
        return this.numeroPuertas;
    }
    
    public void setNumeroPuertas(Short numeroPuertas) {
        this.numeroPuertas = numeroPuertas;
    }
    
    @Column(name="capacidadToneladas", precision=8)

    public Double getCapacidadToneladas() {
        return this.capacidadToneladas;
    }
    
    public void setCapacidadToneladas(Double capacidadToneladas) {
        this.capacidadToneladas = capacidadToneladas;
    }
    
    @Column(name="claveUnidadMedida", length=4)

    public String getClaveUnidadMedida() {
        return this.claveUnidadMedida!=null?this.claveUnidadMedida.toUpperCase():this.claveUnidadMedida;
    }
    
    public void setClaveUnidadMedida(String claveUnidadMedida) {
        this.claveUnidadMedida = claveUnidadMedida;
    }
    
    @Column(name="claveCatalogoFabricante", length=12)

    public String getClaveCatalogoFabricante() {
        return this.claveCatalogoFabricante!=null?this.claveCatalogoFabricante.toUpperCase():this.claveCatalogoFabricante;
    }
    
    public void setClaveCatalogoFabricante(String claveCatalogoFabricante) {
        this.claveCatalogoFabricante = claveCatalogoFabricante;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="fechaCreacion", nullable=false, length=7)

    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }
    
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    
    @Column(name="codigoUsuarioCreacion", nullable=false, length=8)

    public String getCodigoUsuarioCreacion() {
        return this.codigoUsuarioCreacion!=null?this.codigoUsuarioCreacion.toUpperCase():this.codigoUsuarioCreacion;
    }
    
    public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
        this.codigoUsuarioCreacion = codigoUsuarioCreacion;
    }
    
    @Column(name="claveTransmision", length=6)

    public String getClaveTransmision() {
        return this.claveTransmision;
    }
    
    public void setClaveTransmision(String claveTransmision) {
        this.claveTransmision = claveTransmision;
    }
    
    @Column(name="claveCilindros", precision=6, scale=0)

    public String getClaveCilindros() {
        return this.claveCilindros!=null?this.claveCilindros.toUpperCase():this.claveCilindros;
    }
    
    public void setClaveCilindros(String claveCilindros) {
        this.claveCilindros = claveCilindros;
    }
    
    @Column(name="claveBolsasAire", precision=6, scale=0)

    public String getClaveBolsasAire() {
        return this.claveBolsasAire!=null?this.claveBolsasAire.toUpperCase():this.claveBolsasAire;
    }
    
    public void setClaveBolsasAire(String claveBolsasAire) {
        this.claveBolsasAire = claveBolsasAire;
    }
    
    @Column(name="claveSistemaElectrico", length=6)

    public String getClaveSistemaElectrico() {
        return this.claveSistemaElectrico!=null?this.claveSistemaElectrico.toUpperCase():this.claveSistemaElectrico;
    }
    
    public void setClaveSistemaElectrico(String claveSistemaElectrico) {
        this.claveSistemaElectrico = claveSistemaElectrico;
    }
    
    @Column(name="claveVestidura", length=6)

    public String getClaveVestidura() {
        return this.claveVestidura!=null?this.claveVestidura.toUpperCase():this.claveVestidura;
    }
    
    public void setClaveVestidura(String claveVestidura) {
        this.claveVestidura = claveVestidura;
    }
    
    @Column(name="claveQuemacocos", precision=6, scale=0)

    public String getClaveQuemaCocos() {
        return this.claveQuemacocos!=null?this.claveQuemacocos.toUpperCase():this.claveQuemacocos;
    }
    
    public void setClaveQuemaCocos(String claveQuemacocos) {
        this.claveQuemacocos = claveQuemacocos;
    }
    
    @Column(name="claveVersion", length=6)

    public String getClaveVersion() {
        return this.claveVersion!=null?this.claveVersion.toUpperCase():this.claveVersion;
    }
    
    public void setClaveVersion(String claveVersion) {
        this.claveVersion = claveVersion;
    }
    
    @Column(name="claveFrenos", length=6)

    public String getClaveFrenos() {
        return this.claveFrenos!=null?this.claveFrenos.toUpperCase():this.claveFrenos;
    }
    
    public void setClaveFrenos(String claveFrenos) {
        this.claveFrenos = claveFrenos;
    }
    
    @Column(name="claveSonido", length=6)

    public String getClaveSonido() {
        return this.claveSonido!=null?this.claveSonido.toUpperCase():this.claveSonido;
    }
    
    public void setClaveSonido(String claveSonido) {
        this.claveSonido = claveSonido;
    }
    
    @Column(name="claveAireAcondicionado", precision=6, scale=0)

    public String getClaveAireAcondicionado() {
        return this.claveAireAcondicionado!=null?this.claveAireAcondicionado.toUpperCase():this.claveAireAcondicionado;
    }
    
    public void setClaveAireAcondicionado(String claveAireAcondicionado) {
        this.claveAireAcondicionado = claveAireAcondicionado;
    }
    
    @Column(name="claveTipoCombustible", length=6)

    public String getClaveTipoCombustible() {
        return this.claveTipoCombustible!=null?this.claveTipoCombustible.toUpperCase():this.claveTipoCombustible;
    }
    
    public void setClaveTipoCombustible(String claveTipoCombustible) {
        this.claveTipoCombustible = claveTipoCombustible;
    }
    
    @Column(name="claveAlarmaFabricante", precision=6, scale=0)

    public String getClaveAlarmaFabricante() {
        return this.claveAlarmaFabricante!=null?this.claveAlarmaFabricante.toUpperCase():this.claveAlarmaFabricante;
    }
    
    public void setClaveAlarmaFabricante(String claveAlarmaFabricante) {
        this.claveAlarmaFabricante = claveAlarmaFabricante;
    }
    
    @Column(name="claveTipoInyeccion", length=6)

    public String getClaveTipoInyeccion() {
        return this.claveTipoInyeccion!=null?this.claveTipoInyeccion.toUpperCase():this.claveTipoInyeccion;
    }
    
    public void setClaveTipoInyeccion(String claveTipoInyeccion) {
        this.claveTipoInyeccion = claveTipoInyeccion;
    }
    
    @Transient
    public TipoVehiculoDTO getTipoVehiculoDTO() {
		return tipoVehiculoDTO;
	}

	public void setTipoVehiculoDTO(TipoVehiculoDTO tipoVehiculoDTO) {
		this.tipoVehiculoDTO = tipoVehiculoDTO;
	}

	@Override
	public String getDescription() {
		return getDescripcionEstilo();
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof EstiloVehiculoDTO) {
			EstiloVehiculoDTO dto = (EstiloVehiculoDTO) object;
			equal = dto.getId().equals(this.id);
		} // End of if
		return equal;
	}

	@SuppressWarnings("unchecked")
	@Override
	public EstiloVehiculoId getKey() {
		return id;
	}


	@Override
	public String getValue() {
		return null;
	}


	@SuppressWarnings("unchecked")
	@Override
	public EstiloVehiculoId getBusinessKey() {
		return id;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "estiloVehiculoDTO")
	public List<NegocioEstiloVehiculo> getNegocioEstiloVehiculos() {
		return negocioEstiloVehiculos;
	}

	public void setNegocioEstiloVehiculos(
			List<NegocioEstiloVehiculo> negocioEstiloVehiculos) {
		this.negocioEstiloVehiculos = negocioEstiloVehiculos;
	}



	@Column(name="DESCRIPCIONBASE", length=6)
	public String getDescripcionBase() {
		return descripcionBase;
	}
	
	public void setDescripcionBase(String descripcionBase) {
		this.descripcionBase = descripcionBase;
	}
	
	@Column(name = "ALTORIESGO", nullable = true, precision = 4, scale = 0)
	public Boolean getAltoRiesgo() {
		return altoRiesgo;
	}

	public void setAltoRiesgo(Boolean altoRiesgo) {
		this.altoRiesgo = altoRiesgo;
	}
	@Column(name = "ALTAFRECUENCIA", nullable = true, precision = 4, scale = 0)
	public Boolean getAltaFrecuencia() {
		return altaFrecuencia;
	}

	public void setAltaFrecuencia(Boolean altaFrecuencia) {
		this.altaFrecuencia = altaFrecuencia;
	}

	public void setModeloVehiculo(Short modeloVehiculo) {
		this.modeloVehiculo = modeloVehiculo;
	}

	@Transient
	public Short getModeloVehiculo() {
		return modeloVehiculo;
	}
	
	@Transient
	public Boolean getTipoSubMarcaVacio() {
		return tipoSubMarcaVacio;
	}

	public void setTipoSubMarcaVacio(Boolean tipoSubMarcaVacio) {
		this.tipoSubMarcaVacio = tipoSubMarcaVacio;
	}

	@Column(name="CLAVESESAS")
	public String getClaveSesas() {
		return claveSesas;
	}

	public void setClaveSesas(String claveSesas) {
		this.claveSesas = claveSesas;
	}

	@Column(name="CLAVEAMIS")
	public String getClaveAmis() {
		return claveAmis;
	}

	public void setClaveAmis(String claveAmis) {
		this.claveAmis = claveAmis;
	}	
}