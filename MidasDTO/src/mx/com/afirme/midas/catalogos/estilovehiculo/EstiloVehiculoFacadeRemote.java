package mx.com.afirme.midas.catalogos.estilovehiculo;
// default package

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.base.MidasInterfaceBase;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.tipovehiculo.SeccionTipoVehiculoDTO;
import mx.com.afirme.midas2.domain.catalogos.VarModifDescripcion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccion;
import mx.com.afirme.midas2.dto.wrapper.VersionCargaComboDTO;

/**
 * Remote interface for EstiloVehiculoDTOFacade.
 * @author MyEclipse Persistence Tools
 */
public interface EstiloVehiculoFacadeRemote extends MidasInterfaceBase<EstiloVehiculoDTO>{
		/**
	 Perform an initial save of a previously unsaved EstiloVehiculoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity EstiloVehiculoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(EstiloVehiculoDTO entity);
    /**
	 Delete a persistent EstiloVehiculoDTO entity.
	  @param entity EstiloVehiculoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(EstiloVehiculoDTO entity);
   /**
	 Persist a previously saved EstiloVehiculoDTO entity and return it or a copy of it to the sender. 
	 A copy of the EstiloVehiculoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity EstiloVehiculoDTO entity to update
	 @return EstiloVehiculoDTO the persisted EstiloVehiculoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public EstiloVehiculoDTO update(EstiloVehiculoDTO entity);
	public EstiloVehiculoDTO findById( EstiloVehiculoId id);
	 /**
	 * Find all EstiloVehiculoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the EstiloVehiculoDTO property to query
	  @param value the property value to match
	  	  @return List<EstiloVehiculoDTO> found by query
	 */
	public List<EstiloVehiculoDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all EstiloVehiculoDTO entities.
	  	  @return List<EstiloVehiculoDTO> all EstiloVehiculoDTO entities
	 */
	public List<EstiloVehiculoDTO> findAll();
	
	public List<EstiloVehiculoDTO> listarFiltrado(EstiloVehiculoDTO estiloVehiculoDTO);
	/**
	 * Lista los distintos estilos agrupados por la descripcion, los datos que
	 * se obtiene de esta consulta son: descripcionestilo, claveTipoBien, 
	 * claveEstilo,idTcMarcaVehiculo,idTcTipoVehiculo
	 * ordenados por la descripcion
	 * @param estiloVehiculoDTO
	 * @return List<EstiloVehiculoDTO>
	 */
	public List<EstiloVehiculoDTO> listarDistintosFiltrado(EstiloVehiculoDTO estiloVehiculoDTO);

	/**
	 * Lista los distintos estilos agrupados por la descripcion, los datos que
	 * se obtiene de esta consulta son: descripcionestilo, claveTipoBien, 
	 * claveEstilo,idTcMarcaVehiculo,idTcTipoVehiculo
	 * ordenados por la descripcion
	 * Es necesario enviar la descripcion y modelo del vehiculo
	 * esta consulto es especial para el autocomplete
	 * @param estiloVehiculoDTO
	 * @return List<EstiloVehiculoDTO>
	 */
	public List<EstiloVehiculoDTO> listarDistintosFiltrado(String claveTipoBien,Integer modelo, String descripcion, BigDecimal idToNegSeccion);
	
	/**
	 * Lista los distintos estilos agrupados 
	 * la version estada dada por NegocioAgrupadorTarifaSeccion 
	 * @param estiloVehiculoDTO
	 * @return List<EstiloVehiculoDTO>
	 */
	public List<EstiloVehiculoDTO> listarDistintosFiltradoVersionAgrupador(EstiloVehiculoDTO estiloVehiculoDTO, NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion, BigDecimal idToNegSeccion);
	
	public List<EstiloVehiculoDTO> listarDistintosFiltradoVersionAgrupadorTodo(EstiloVehiculoDTO estiloVehiculoDTO, NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion, 
			List<SeccionTipoVehiculoDTO> seccionTipoVehiculoList );
	
	public List<VersionCargaComboDTO> listarVersionCarga(EstiloVehiculoDTO estiloVehiculoDTO);
	
	public VersionCargaComboDTO getVersionCarga(EstiloVehiculoDTO estiloVehiculoDTO);
	
	public List<VarModifDescripcion> findByFiltersModifDescripcion(VarModifDescripcion varModifDescripcion);
	
	public String actualizarMasivo(String idToControlArchivo);
}