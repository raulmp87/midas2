package mx.com.afirme.midas.catalogos.estilovehiculo.grupo;
// default package

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;


/**
 * EstiloVehiculoGruposDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCESTILOVEHICULOGRUPOS"
    ,schema="MIDAS"
)

public class EstiloVehiculoGrupoDTO  implements java.io.Serializable {
	private static final long serialVersionUID = -6299460910237286268L;

    // Fields    
	private EstiloVehiculoGrupoId id=new EstiloVehiculoGrupoId();
     private String claveAMIS;
     private Date fechaInicioVigencia;
     private Date fechaFinVigencia;
     private BigDecimal idGrupoDM;
     private BigDecimal idGrupoRT;
     private BigDecimal idGrupoRC;
     private Date fechaCreacion;
     private String codigoUsuarioCreacion;

     private EstiloVehiculoDTO estiloVehiculoDTO;

    // Constructors

    /** default constructor */
    public EstiloVehiculoGrupoDTO() {
    }

    
    /** full constructor */
    public EstiloVehiculoGrupoDTO(EstiloVehiculoGrupoId id, String claveAMIS, Date fechaInicioVigencia, Date fechaFinVigencia, BigDecimal idGrupoDM, BigDecimal idGrupoRT, BigDecimal idGrupoRC, Date fechaCreacion, String codigoUsuarioCreacion) {
        this.id = id;
        this.claveAMIS = claveAMIS;
        this.fechaInicioVigencia = fechaInicioVigencia;
        this.fechaFinVigencia = fechaFinVigencia;
        this.idGrupoDM = idGrupoDM;
        this.idGrupoRT = idGrupoRT;
        this.idGrupoRC = idGrupoRC;
        this.fechaCreacion = fechaCreacion;
        this.codigoUsuarioCreacion = codigoUsuarioCreacion;
    }

   
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="claveTipoBien", column=@Column(name="CLAVETIPOBIEN", nullable=false, length=5) ), 
        @AttributeOverride(name="claveEstilo", column=@Column(name="CLAVEESTILO", nullable=false, length=8) ), 
        @AttributeOverride(name="idVersionCarga", column=@Column(name="IDVERSIONCARGA", nullable=false, precision=22, scale=0) ) } )

    public EstiloVehiculoGrupoId getId() {
        return this.id;
    }
    
    public void setId(EstiloVehiculoGrupoId id) {
        this.id = id;
    }
    
    @Column(name="CLAVEAMIS", nullable=false, length=8)

    public String getClaveAMIS() {
        return this.claveAMIS;
    }
    
    public void setClaveAMIS(String claveAMIS) {
        this.claveAMIS = claveAMIS;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAINICIOVIGENCIA", nullable=false, length=7)

    public Date getFechaInicioVigencia() {
        return this.fechaInicioVigencia;
    }
    
    public void setFechaInicioVigencia(Date fechaInicioVigencia) {
        this.fechaInicioVigencia = fechaInicioVigencia;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAFINVIGENCIA", nullable=false, length=7)

    public Date getFechaFinVigencia() {
        return this.fechaFinVigencia;
    }
    
    public void setFechaFinVigencia(Date fechaFinVigencia) {
        this.fechaFinVigencia = fechaFinVigencia;
    }
    
    @Column(name="IDGRUPODM", nullable=false, precision=22, scale=0)

    public BigDecimal getIdGrupoDM() {
        return this.idGrupoDM;
    }
    
    public void setIdGrupoDM(BigDecimal idGrupoDM) {
        this.idGrupoDM = idGrupoDM;
    }
    
    @Column(name="IDGRUPORT", nullable=false, precision=22, scale=0)

    public BigDecimal getIdGrupoRT() {
        return this.idGrupoRT;
    }
    
    public void setIdGrupoRT(BigDecimal idGrupoRT) {
        this.idGrupoRT = idGrupoRT;
    }
    
    @Column(name="IDGRUPORC", nullable=false, precision=22, scale=0)

    public BigDecimal getIdGrupoRC() {
        return this.idGrupoRC;
    }
    
    public void setIdGrupoRC(BigDecimal idGrupoRC) {
        this.idGrupoRC = idGrupoRC;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHACREACION", nullable=false, length=7)

    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }
    
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    
    @Column(name="CODIGOUSUARIOCREACION", nullable=false, length=8)

    public String getCodigoUsuarioCreacion() {
        return this.codigoUsuarioCreacion;
    }
    
    public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
        this.codigoUsuarioCreacion = codigoUsuarioCreacion;
    }

    @Transient
	public EstiloVehiculoDTO getEstiloVehiculoDTO() {
		return estiloVehiculoDTO;
	}

	public void setEstiloVehiculoDTO(EstiloVehiculoDTO estiloVehiculoDTO) {
		this.estiloVehiculoDTO = estiloVehiculoDTO;
	}  
}