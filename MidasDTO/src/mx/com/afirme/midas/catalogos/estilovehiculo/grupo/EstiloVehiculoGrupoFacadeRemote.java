package mx.com.afirme.midas.catalogos.estilovehiculo.grupo;
// default package

import java.util.List;
/**
 * Remote interface for EstiloVehiculoGruposDTOFacade.
 * @author MyEclipse Persistence Tools
 */
public interface EstiloVehiculoGrupoFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved EstiloVehiculoGruposDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity EstiloVehiculoGruposDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(EstiloVehiculoGrupoDTO entity);
    /**
	 Delete a persistent EstiloVehiculoGruposDTO entity.
	  @param entity EstiloVehiculoGruposDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(EstiloVehiculoGrupoDTO entity);
   /**
	 Persist a previously saved EstiloVehiculoGruposDTO entity and return it or a copy of it to the sender. 
	 A copy of the EstiloVehiculoGruposDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity EstiloVehiculoGruposDTO entity to update
	 @return EstiloVehiculoGruposDTO the persisted EstiloVehiculoGruposDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public EstiloVehiculoGrupoDTO update(EstiloVehiculoGrupoDTO entity);
	public EstiloVehiculoGrupoDTO findById( EstiloVehiculoGrupoId id);
	 /**
	 * Find all EstiloVehiculoGruposDTO entities with a specific property value.  
	 
	  @param propertyName the name of the EstiloVehiculoGruposDTO property to query
	  @param value the property value to match
	  	  @return List<EstiloVehiculoGruposDTO> found by query
	 */
	public List<EstiloVehiculoGrupoDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all EstiloVehiculoGruposDTO entities.
	  	  @return List<EstiloVehiculoGruposDTO> all EstiloVehiculoGruposDTO entities
	 */
	public List<EstiloVehiculoGrupoDTO> findAll();
	
	public List<EstiloVehiculoGrupoDTO> listarFiltrado(EstiloVehiculoGrupoDTO estiloVehiculoGrupoDTO);
}