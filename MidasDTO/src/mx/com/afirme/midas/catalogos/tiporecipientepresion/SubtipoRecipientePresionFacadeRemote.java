package mx.com.afirme.midas.catalogos.tiporecipientepresion;

// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for SubtipoRecipientePresionFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SubtipoRecipientePresionFacadeRemote extends
		MidasInterfaceBase<SubtipoRecipientePresionDTO> {
	/**
	 * Perform an initial save of a previously unsaved
	 * SubtipoRecipientePresionDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            SubtipoRecipientePresionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SubtipoRecipientePresionDTO entity);

	/**
	 * Delete a persistent SubtipoRecipientePresionDTO entity.
	 * 
	 * @param entity
	 *            SubtipoRecipientePresionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SubtipoRecipientePresionDTO entity);

	/**
	 * Persist a previously saved SubtipoRecipientePresionDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * SubtipoRecipientePresionDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            SubtipoRecipientePresionDTO entity to update
	 * @return SubtipoRecipientePresionDTO the persisted
	 *         SubtipoRecipientePresionDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SubtipoRecipientePresionDTO update(SubtipoRecipientePresionDTO entity);

	/**
	 * Find all SubtipoRecipientePresionDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the SubtipoRecipientePresionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SubtipoRecipientePresionDTO> found by query
	 */
	public List<SubtipoRecipientePresionDTO> findByProperty(
			String propertyName, Object value);

	public List<SubtipoRecipientePresionDTO> listarFiltrado(
			SubtipoRecipientePresionDTO subTipoRecPresionDTO);
}