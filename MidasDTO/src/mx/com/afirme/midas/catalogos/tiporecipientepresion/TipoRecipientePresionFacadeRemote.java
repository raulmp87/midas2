package mx.com.afirme.midas.catalogos.tiporecipientepresion;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for TipoRecipientePresionFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface TipoRecipientePresionFacadeRemote extends
		MidasInterfaceBase<TipoRecipientePresionDTO> {
	/**
	 * Perform an initial save of a previously unsaved TipoRecipientePresionDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            TipoRecipientePresionDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoRecipientePresionDTO entity);

	/**
	 * Delete a persistent TipoRecipientePresionDTO entity.
	 * 
	 * @param entity
	 *            TipoRecipientePresionDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoRecipientePresionDTO entity);

	/**
	 * Persist a previously saved TipoRecipientePresionDTO entity and return it
	 * or a copy of it to the sender. A copy of the TipoRecipientePresionDTO
	 * entity parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoRecipientePresionDTO entity to update
	 * @return TipoRecipientePresionDTO the persisted TipoRecipientePresionDTO
	 *         entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoRecipientePresionDTO update(TipoRecipientePresionDTO entity);

	/**
	 * Find all TipoRecipientePresionDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the TipoRecipientePresionDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoRecipientePresionDTO> found by query
	 */
	public List<TipoRecipientePresionDTO> findByProperty(String propertyName,
			Object value);

	public List<TipoRecipientePresionDTO> listarFiltrado(
			TipoRecipientePresionDTO tipoRecipientePresionDTO);
}