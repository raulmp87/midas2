package mx.com.afirme.midas.catalogos.tiporecipientepresion;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.base.CacheableDTO;

/**
 * TipoRecipientePresionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name = "TipoRecipientePresionDTO")
@Table(name = "TCTIPORECIPIENTEPRESION", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = "CODIGOTIPORECIPIENTEPRESION"))
public class TipoRecipientePresionDTO extends CacheableDTO {

	private static final long serialVersionUID = 5732163587508841496L;

	private BigDecimal idTipoRecipientePresion;
	private String descripcionTipoRecPresion;
	private BigDecimal codigoTipoRecipientePresion;
	private List<SubtipoRecipientePresionDTO> subtipoRecipientePresionDTOs = new ArrayList<SubtipoRecipientePresionDTO>();

	// constructor
	public TipoRecipientePresionDTO() {
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTCTIPORECIPIENTEPRES_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCTIPORECIPIENTEPRES_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCTIPORECIPIENTEPRES_SEQ_GENERADOR")
	@Column(name = "IDTCTIPORECIPIENTEPRESION", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTipoRecipientePresion() {
		return this.idTipoRecipientePresion;
	}

	public void setIdTipoRecipientePresion(BigDecimal idTipoRecipientePresion) {
		this.idTipoRecipientePresion = idTipoRecipientePresion;
	}

	@Column(name = "DESCRIPCIONTIPORECPRESION", nullable = false, length = 200)
	public String getDescripcionTipoRecPresion() {
		return this.descripcionTipoRecPresion;
	}

	public void setDescripcionTipoRecPresion(String descripcionTipoRecPresion) {
		this.descripcionTipoRecPresion = descripcionTipoRecPresion;
	}

	@Column(name = "CODIGOTIPORECIPIENTEPRESION", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoTipoRecipientePresion() {
		return this.codigoTipoRecipientePresion;
	}

	public void setCodigoTipoRecipientePresion(
			BigDecimal codigoTipoRecipientePresion) {
		this.codigoTipoRecipientePresion = codigoTipoRecipientePresion;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tipoRecipientePresionDTO")
	public List<SubtipoRecipientePresionDTO> getSubtipoRecipientePresionDTOs() {
		return subtipoRecipientePresionDTOs;
	}

	public void setSubtipoRecipientePresionDTOs(List<SubtipoRecipientePresionDTO> subtipoRecipientePresionDTOs) {
		this.subtipoRecipientePresionDTOs = subtipoRecipientePresionDTOs;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o instanceof TipoRecipientePresionDTO) {
			if (((TipoRecipientePresionDTO) o).getCodigoTipoRecipientePresion()
					.equals(this.getCodigoTipoRecipientePresion())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String getDescription() {
		return this.descripcionTipoRecPresion;
	}

	@Override
	public Object getId() {
		return this.idTipoRecipientePresion;
	}
}