package mx.com.afirme.midas.catalogos.tiporecipientepresion;

// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.catalogos.giro.GiroDTO;

/**
 * SubtipoRecipientePresionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name = "SubtipoRecipientePresionDTO")
@Table(name = "TCSUBTIPORECIPIENTEPRESION", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = {
		"IDTCTIPORECIPIENTEPRESION", "CODIGOSUBTIPORECIPIENTEPRESION" }))
public class SubtipoRecipientePresionDTO extends CacheableDTO {

	// Fields

	private static final long serialVersionUID = -87182720360738299L;

	private BigDecimal idSubtipoRecipientePresion;
	private TipoRecipientePresionDTO tipoRecipientePresionDTO;
	private String descripcionSubtipoRecPresion;
	private BigDecimal codigoSubtipoRecipientePresion;

	// Constructors

	/** default constructor */
	public SubtipoRecipientePresionDTO() {
		if (this.tipoRecipientePresionDTO == null) {
			this.tipoRecipientePresionDTO = new TipoRecipientePresionDTO();
		}
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTCSUBTIPORECIPIENTEPRES_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCSUBTIPORECIPIENTEPRES_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCSUBTIPORECIPIENTEPRES_SEQ_GENERADOR")
	@Column(name = "IDTCSUBTIPORECIPIENTEPRESION", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdSubtipoRecipientePresion() {
		return this.idSubtipoRecipientePresion;
	}

	public void setIdSubtipoRecipientePresion(
			BigDecimal idSubtipoRecipientePresion) {
		this.idSubtipoRecipientePresion = idSubtipoRecipientePresion;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCTIPORECIPIENTEPRESION", nullable = false)
	public TipoRecipientePresionDTO getTipoRecipientePresionDTO() {
		return tipoRecipientePresionDTO;
	}

	public void setTipoRecipientePresionDTO(
			TipoRecipientePresionDTO tipoRecipientePresionDTO) {
		this.tipoRecipientePresionDTO = tipoRecipientePresionDTO;
	}

	@Column(name = "DESCRIPCIONSUBTIPORECPRESION", nullable = false, length = 200)
	public String getDescripcionSubtipoRecPresion() {
		return this.descripcionSubtipoRecPresion;
	}

	public void setDescripcionSubtipoRecPresion(
			String descripcionSubtipoRecPresion) {
		this.descripcionSubtipoRecPresion = descripcionSubtipoRecPresion;
	}

	@Column(name = "CODIGOSUBTIPORECIPIENTEPRESION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoSubtipoRecipientePresion() {
		return this.codigoSubtipoRecipientePresion;
	}

	public void setCodigoSubtipoRecipientePresion(
			BigDecimal codigoSubtipoRecipientePresion) {
		this.codigoSubtipoRecipientePresion = codigoSubtipoRecipientePresion;
	}

	@Override
	public String getDescription() {
		return this.descripcionSubtipoRecPresion;
	}

	@Override
	public Object getId() {
		return this.idSubtipoRecipientePresion;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof GiroDTO) {
			SubtipoRecipientePresionDTO subtipoRecipientePresionDTO = (SubtipoRecipientePresionDTO) object;
			equal = subtipoRecipientePresionDTO.getIdSubtipoRecipientePresion()
					.equals(this.idSubtipoRecipientePresion);
		} // End of if
		return equal;
	}

}