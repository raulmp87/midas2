package mx.com.afirme.midas.catalogos.calificacionesreas;


import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.catalogos.agenciacalificadora.AgenciaCalificadoraDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
@Entity
@Table(name="CNSF_CALIFICACIONES_REAS", schema="MIDAS")

public class CalificacionAgenciaDTO extends CacheableDTO implements java.io.Serializable, Entidad {
	
	private static final long serialVersionUID = 7052386591022420642L;
	private BigDecimal id;
    private AgenciaCalificadoraDTO agencia;
    private String calificacion;
    private BigDecimal valor;
    
    @Id
    @SequenceGenerator(name = "CNSF_CALIF_REAS_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.CNSF_CALIF_REAS_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CNSF_CALIF_REAS_SEQ_GENERADOR")
    @Column(name="ID", unique=true, nullable=false, precision=22, scale=0)
    public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}
	
	@ManyToOne(fetch=FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinColumn(name="AGENCIA", nullable=false)
	public AgenciaCalificadoraDTO getAgencia() {
		return agencia;
	}

	public void setAgencia(AgenciaCalificadoraDTO agencia) {
		this.agencia = agencia;
	}
	@Column(name="CALIFICACION", nullable=false)
	public String getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(String calificacion) {
		this.calificacion = calificacion;
	}
	@Column(name="VALOR", nullable=false, precision=22, scale=0)
	public BigDecimal getValor() {
		return valor;
	} 

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	@Override
	public String getDescription() {
		return calificacion;
	}

	@Override
	public boolean equals(Object other) {
		boolean equal = other == this;
		if (!equal && other instanceof CalificacionAgenciaDTO) {
			CalificacionAgenciaDTO pais = (CalificacionAgenciaDTO) other;
			equal = pais.getId().equals(this.id);
		} 
		return equal;
	}

	@Override
	public String getKey() {
		return this.getId()+"";
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}