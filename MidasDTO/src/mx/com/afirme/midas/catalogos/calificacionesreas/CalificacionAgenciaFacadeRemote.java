package mx.com.afirme.midas.catalogos.calificacionesreas;


import java.math.BigDecimal;
import java.util.List;


import mx.com.afirme.midas.base.MidasInterfaceBase;

public interface CalificacionAgenciaFacadeRemote extends MidasInterfaceBase<CalificacionAgenciaDTO> {
	
    public void save(CalificacionAgenciaDTO entity);
    
    public void delete(CalificacionAgenciaDTO entity);
 
	public CalificacionAgenciaDTO update(CalificacionAgenciaDTO entity);

	public List<CalificacionAgenciaDTO> findByProperty(String propertyName, Object value);
	
	public List<CalificacionAgenciaDTO> findByPropertyID(BigDecimal value);
	
	public List<CalificacionAgenciaDTO> findAll();
	
	public List<CalificacionAgenciaDTO> listarFiltrado(CalificacionAgenciaDTO contactoDTO);
	
	public CalificacionAgenciaDTO findById(String id);
}