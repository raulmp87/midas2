package mx.com.afirme.midas.catalogos.equipoelectronico;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.base.CacheableDTO;

/**
 * EquipoElectronicoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCTIPOEQUIPOELECTRONICO", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = "CODIGOTIPOEQUIPOELECTRONICO"))
public class EquipoElectronicoDTO extends CacheableDTO {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1024235715165587453L;
	private BigDecimal idtctipoequipoelectronico;
	private BigDecimal codigotipoequipoelectronico;
	private String descripciontipoeqelectro;
	private List<SubtipoEquipoElectronicoDTO> subtipoEquipoElectronicoDTOs = new ArrayList<SubtipoEquipoElectronicoDTO>();

	// Constructors

	/** default constructor */
	public EquipoElectronicoDTO() {
	}

	// Property accessors
	@Id
	@Column(name = "IDTCTIPOEQUIPOELECTRONICO", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdtctipoequipoelectronico() {
		return this.idtctipoequipoelectronico;
	}

	public void setIdtctipoequipoelectronico(
			BigDecimal idtctipoequipoelectronico) {
		this.idtctipoequipoelectronico = idtctipoequipoelectronico;
	}

	@Column(name = "CODIGOTIPOEQUIPOELECTRONICO", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigotipoequipoelectronico() {
		return this.codigotipoequipoelectronico;
	}

	public void setCodigotipoequipoelectronico(
			BigDecimal codigotipoequipoelectronico) {
		this.codigotipoequipoelectronico = codigotipoequipoelectronico;
	}

	@Column(name = "DESCRIPCIONTIPOEQELECTRO", nullable = false, length = 200)
	public String getDescripciontipoeqelectro() {
		return this.descripciontipoeqelectro;
	}

	public void setDescripciontipoeqelectro(String descripciontipoeqelectro) {
		this.descripciontipoeqelectro = descripciontipoeqelectro;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "equipoElectronicoDTO")
	public List<SubtipoEquipoElectronicoDTO> getSubtipoEquipoElectronicoDTOs() {
		return subtipoEquipoElectronicoDTOs;
	}

	public void setSubtipoEquipoElectronicoDTOs(
			List<SubtipoEquipoElectronicoDTO> subtipoEquipoElectronicoDTOs) {
		this.subtipoEquipoElectronicoDTOs = subtipoEquipoElectronicoDTOs;
	}

	@Override
	public String getDescription() {
		return this.descripciontipoeqelectro;
	}

	@Override
	public Object getId() {
		return this.idtctipoequipoelectronico;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof EquipoElectronicoDTO) {
			EquipoElectronicoDTO equipoElectronicoDTO = (EquipoElectronicoDTO) object;
			equal = equipoElectronicoDTO.getIdtctipoequipoelectronico().equals(
					this.idtctipoequipoelectronico);
		}
		return equal;
	}
}