package mx.com.afirme.midas.catalogos.equipoelectronico;

// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for SubtipoEquipoElectronicoFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SubtipoEquipoElectronicoFacadeRemote extends
		MidasInterfaceBase<SubtipoEquipoElectronicoDTO> {
	/**
	 * Perform an initial save of a previously unsaved
	 * SubtipoEquipoElectronicoDTO entity. All subsequent persist actions of
	 * this entity should use the #update() method.
	 * 
	 * @param entity
	 *            SubtipoEquipoElectronicoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SubtipoEquipoElectronicoDTO entity);

	/**
	 * Delete a persistent SubtipoEquipoElectronicoDTO entity.
	 * 
	 * @param entity
	 *            SubtipoEquipoElectronicoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SubtipoEquipoElectronicoDTO entity);

	/**
	 * Persist a previously saved SubtipoEquipoElectronicoDTO entity and return
	 * it or a copy of it to the sender. A copy of the
	 * SubtipoEquipoElectronicoDTO entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            SubtipoEquipoElectronicoDTO entity to update
	 * @return SubtipoEquipoElectronicoDTO the persisted
	 *         SubtipoEquipoElectronicoDTO entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SubtipoEquipoElectronicoDTO update(SubtipoEquipoElectronicoDTO entity);

	/**
	 * Find all SubtipoEquipoElectronicoDTO entities with a specific property
	 * value.
	 * 
	 * @param propertyName
	 *            the name of the SubtipoEquipoElectronicoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SubtipoEquipoElectronicoDTO> found by query
	 */
	public List<SubtipoEquipoElectronicoDTO> findByProperty(
			String propertyName, Object value);

}