package mx.com.afirme.midas.catalogos.equipoelectronico;

// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for EquipoElectronicoFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface EquipoElectronicoFacadeRemote extends
		MidasInterfaceBase<EquipoElectronicoDTO> {
	/**
	 * Perform an initial save of a previously unsaved EquipoElectronicoDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            EquipoElectronicoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(EquipoElectronicoDTO entity);

	/**
	 * Delete a persistent EquipoElectronicoDTO entity.
	 * 
	 * @param entity
	 *            EquipoElectronicoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(EquipoElectronicoDTO entity);

	/**
	 * Persist a previously saved EquipoElectronicoDTO entity and return it or a
	 * copy of it to the sender. A copy of the EquipoElectronicoDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            EquipoElectronicoDTO entity to update
	 * @return EquipoElectronicoDTO the persisted EquipoElectronicoDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public EquipoElectronicoDTO update(EquipoElectronicoDTO entity);

	/**
	 * Find all EquipoElectronicoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the EquipoElectronicoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<EquipoElectronicoDTO> found by query
	 */
	public List<EquipoElectronicoDTO> findByProperty(String propertyName,
			Object value);

}