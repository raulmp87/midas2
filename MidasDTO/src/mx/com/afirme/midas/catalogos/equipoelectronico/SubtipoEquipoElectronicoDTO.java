package mx.com.afirme.midas.catalogos.equipoelectronico;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

import mx.com.afirme.midas.catalogos.SubTipoGenerico;
import mx.com.afirme.midas.catalogos.ramo.RamoDTO;

/**
 * SubtipoEquipoElectronicoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCSUBTIPOEQUIPOELECTRONICO", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = {
		"IDTCTIPOEQUIPOELECTRONICO", "CODIGOSUBTIPOEQUIPOELECTRONICO" }))
@Cache(
  type=CacheType.SOFT,
  size=1000,
  expiry=36000000
)
public class SubtipoEquipoElectronicoDTO extends SubTipoGenerico{

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -6792244994834436101L;
	private BigDecimal idtcsubtipoequipoelectronico;
	private EquipoElectronicoDTO equipoElectronicoDTO;
	private BigDecimal codigosubtipoequipoelectronico;
	private String descripcionsubtipoeqelectro;
	private Short claveAutorizacion;

	// Constructors

	/** default constructor */
	public SubtipoEquipoElectronicoDTO() {
	}

	// Property accessors
	@Id
	@Column(name = "IDTCSUBTIPOEQUIPOELECTRONICO", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdtcsubtipoequipoelectronico() {
		return this.idtcsubtipoequipoelectronico;
	}

	public void setIdtcsubtipoequipoelectronico(
			BigDecimal idtcsubtipoequipoelectronico) {
		this.idtcsubtipoequipoelectronico = idtcsubtipoequipoelectronico;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTCTIPOEQUIPOELECTRONICO", nullable = false)
	public EquipoElectronicoDTO getEquipoElectronicoDTO() {
		return equipoElectronicoDTO;
	}

	public void setEquipoElectronicoDTO(EquipoElectronicoDTO equipoElectronicoDTO) {
		this.equipoElectronicoDTO = equipoElectronicoDTO;
	}

	@Column(name = "CODIGOSUBTIPOEQUIPOELECTRONICO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigosubtipoequipoelectronico() {
		return this.codigosubtipoequipoelectronico;
	}

	public void setCodigosubtipoequipoelectronico(
			BigDecimal codigosubtipoequipoelectronico) {
		this.codigosubtipoequipoelectronico = codigosubtipoequipoelectronico;
	}

	@Column(name = "DESCRIPCIONSUBTIPOEQELECTRO", nullable = false, length = 200)
	public String getDescripcionsubtipoeqelectro() {
		return this.descripcionsubtipoeqelectro;
	}

	public void setDescripcionsubtipoeqelectro(
			String descripcionsubtipoeqelectro) {
		this.descripcionsubtipoeqelectro = descripcionsubtipoeqelectro;
	}

	@Column(name = "CLAVEAUTORIZACION", nullable = false, precision = 4, scale = 0)
	public Short getClaveAutorizacion() {
		return claveAutorizacion;
	}

	public void setClaveAutorizacion(Short claveAutorizacion) {
		this.claveAutorizacion = claveAutorizacion;
	}

	@Override
	public String getDescription() {
		return this.descripcionsubtipoeqelectro;
	}

	@Override
	public Object getId() {
		return this.idtcsubtipoequipoelectronico;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof RamoDTO) {
			SubtipoEquipoElectronicoDTO subtipoEquipoElectronicoDTO = (SubtipoEquipoElectronicoDTO) object;
			equal = subtipoEquipoElectronicoDTO
					.getIdtcsubtipoequipoelectronico().equals(
							this.idtcsubtipoequipoelectronico);
		}
		return equal;
	}
}