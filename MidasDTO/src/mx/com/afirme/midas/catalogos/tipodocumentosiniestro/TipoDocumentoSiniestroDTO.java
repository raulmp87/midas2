package mx.com.afirme.midas.catalogos.tipodocumentosiniestro;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * TipoDocumentoSiniestro entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCTIPODOCUMENTOSINIESTRO"
    ,schema="MIDAS"
)

public class TipoDocumentoSiniestroDTO  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

    // Fields    

     private BigDecimal idTcTipoDocumentoSiniestro;
     private String descripcionTipoDocumento;
     
     public static final Byte CARTA_RECHAZO = 7;

    // Constructors

    /** default constructor */
    public TipoDocumentoSiniestroDTO() {
    }

    // Property accessors
    @Id 
    
    @Column(name="IDTCTIPODOCUMENTOSINIESTRO", unique=true, nullable=false, precision=22, scale=0)

    public BigDecimal getIdTcTipoDocumentoSiniestro() {
        return this.idTcTipoDocumentoSiniestro;
    }
    
    public void setIdTcTipoDocumentoSiniestro(BigDecimal idTcTipoDocumentoSiniestro) {
        this.idTcTipoDocumentoSiniestro = idTcTipoDocumentoSiniestro;
    }
    
    @Column(name="DESCRIPCIONTIPODOCUMENTO", nullable=false, length=50)

    public String getDescripcionTipoDocumento() {
        return this.descripcionTipoDocumento;
    }
    
    public void setDescripcionTipoDocumento(String descripcionTipoDocumento) {
        this.descripcionTipoDocumento = descripcionTipoDocumento;
    }
   
    public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof TipoDocumentoSiniestroDTO) {
			TipoDocumentoSiniestroDTO tipoDocumentoSiniestro = (TipoDocumentoSiniestroDTO) object;
			equal = tipoDocumentoSiniestro.getIdTcTipoDocumentoSiniestro().equals(this.idTcTipoDocumentoSiniestro);
		} // End of if
		return equal;
	}

}