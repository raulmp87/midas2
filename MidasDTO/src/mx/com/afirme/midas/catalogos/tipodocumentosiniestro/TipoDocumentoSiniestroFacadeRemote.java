package mx.com.afirme.midas.catalogos.tipodocumentosiniestro;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for TipoDocumentoSiniestroFacade.
 * @author MyEclipse Persistence Tools
 */


public interface TipoDocumentoSiniestroFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved TipoDocumentoSiniestro entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity TipoDocumentoSiniestro entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(TipoDocumentoSiniestroDTO entity);
    /**
	 Delete a persistent TipoDocumentoSiniestro entity.
	  @param entity TipoDocumentoSiniestro entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(TipoDocumentoSiniestroDTO entity);
   /**
	 Persist a previously saved TipoDocumentoSiniestro entity and return it or a copy of it to the sender. 
	 A copy of the TipoDocumentoSiniestro entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity TipoDocumentoSiniestro entity to update
	 @return TipoDocumentoSiniestro the persisted TipoDocumentoSiniestro entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public TipoDocumentoSiniestroDTO update(TipoDocumentoSiniestroDTO entity);
	public TipoDocumentoSiniestroDTO findById( BigDecimal id);
	 /**
	 * Find all TipoDocumentoSiniestro entities with a specific property value.  
	 
	  @param propertyName the name of the TipoDocumentoSiniestro property to query
	  @param value the property value to match
	  	  @return List<TipoDocumentoSiniestro> found by query
	 */
	public List<TipoDocumentoSiniestroDTO> findByProperty(String propertyName, Object value
		);
	
	/**
	 * Find all TipoDocumentoSiniestro entities.
	  	  @return List<TipoDocumentoSiniestro> all TipoDocumentoSiniestro entities
	 */
	public List<TipoDocumentoSiniestroDTO> findAll(
		);	
}