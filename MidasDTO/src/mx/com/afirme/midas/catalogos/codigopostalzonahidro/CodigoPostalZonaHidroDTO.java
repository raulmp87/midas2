package mx.com.afirme.midas.catalogos.codigopostalzonahidro;

// default package

import java.math.BigDecimal;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.base.PaginadoDTO;
import mx.com.afirme.midas.catalogos.zonahidro.ZonaHidroDTO;

/**
 * CodigoPostalZonaHidroDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCCODIGOPOSTALZONAHIDRO", schema = "MIDAS")
public class CodigoPostalZonaHidroDTO extends PaginadoDTO {
	
	private static final long serialVersionUID = 3535912870453426306L;
	
	// Fields

	private CodigoPostalZonaHidroId id;
	private ZonaHidroDTO zonaHidro;

	// Constructors

	/** default constructor */
	public CodigoPostalZonaHidroDTO() {
		if (zonaHidro == null)
			zonaHidro = new ZonaHidroDTO();
		if(id==null)
		    id= new CodigoPostalZonaHidroId();
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "codigoPostal", column = @Column(name = "CODIGOPOSTAL", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "nombreColonia", column = @Column(name = "NOMBRECOLONIA")) })
	public CodigoPostalZonaHidroId getId() {
		return this.id;
	}

	public void setId(CodigoPostalZonaHidroId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCZONAHIDRO", nullable = false, insertable = true, updatable = true)
	public ZonaHidroDTO getZonaHidro() {
		return this.zonaHidro;
	}

	public void setZonaHidro(ZonaHidroDTO zonaHidro) {
		this.zonaHidro = zonaHidro;
	}

	public String getNombreColonia() {
		return id.getNombreColonia();
	}

	public BigDecimal getCodigoPostal() {
		return id.getCodigoPostal();
	}

	public String getCodigoZonaHidro() {
		return this.zonaHidro.getCodigoZonaHidro();
	}

	public String getDescripcionZonaHidro() {
		return this.zonaHidro.getDescripcionZonaHidro();
	}

	
	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof CodigoPostalZonaHidroDTO) {
			CodigoPostalZonaHidroDTO codigoPostalZonaHidroDTO = (CodigoPostalZonaHidroDTO) object;
			equal = codigoPostalZonaHidroDTO.getId().equals(this.id);
		} // End of if
		return equal;
	}
}