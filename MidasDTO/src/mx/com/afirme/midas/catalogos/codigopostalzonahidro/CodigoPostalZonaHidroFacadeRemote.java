package mx.com.afirme.midas.catalogos.codigopostalzonahidro;
// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.catalogos.codigopostalzonasismo.CodigoPostalZonaSismoDTO;


/**
 * Remote interface for CodigoPostalZonaHidroFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface CodigoPostalZonaHidroFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved CodigoPostalZonaHidroDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            CodigoPostalZonaHidroDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(CodigoPostalZonaHidroDTO entity);

	/**
	 * Delete a persistent CodigoPostalZonaHidroDTO entity.
	 * 
	 * @param entity
	 *            CodigoPostalZonaHidroDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(CodigoPostalZonaHidroDTO entity);

	/**
	 * Persist a previously saved CodigoPostalZonaHidroDTO entity and return it or
	 * a copy of it to the sender. A copy of the CodigoPostalZonaHidroDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            CodigoPostalZonaHidroDTO entity to update
	 * @return CodigoPostalZonaHidroDTO the persisted CodigoPostalZonaHidroDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public CodigoPostalZonaHidroDTO update(CodigoPostalZonaHidroDTO entity);

	public CodigoPostalZonaHidroDTO findById(CodigoPostalZonaHidroId id);

	/**
	 * Find all CodigoPostalZonaHidroDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the CodigoPostalZonaHidroDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<CodigoPostalZonaHidroDTO> found by query
	 */
	public List<CodigoPostalZonaHidroDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all CodigoPostalZonaHidroDTO entities.
	 * 
	 * @return List<CodigoPostalZonaHidroDTO> all CodigoPostalZonaHidroDTO entities
	 */
	public List<CodigoPostalZonaHidroDTO> findAll();
	
	/**
	 * Find filtered CodigoPostalZonaHidroDTO entities.
	  	  @return List<CodigoPostalZonaHidroDTO> filtered CodigoPostalZonaHidroDTO entities
	 */
	public List<CodigoPostalZonaHidroDTO> listarFiltrado(CodigoPostalZonaHidroDTO codigoPostalZonaHidroDTO);
	
	public Long obtenerTotalFiltrado(CodigoPostalZonaHidroDTO codigoPostalZonaHidroDTO);
	
	public void agregarCodigoPostalZonasHidroSismo(CodigoPostalZonaHidroDTO codigoPostalZonaHidroDTO, 
		CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO);
	
	public void modificarCodigoPostalZonasHidroSismo(CodigoPostalZonaHidroDTO codigoPostalZonaHidroDTO, 
		CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO);
}