package mx.com.afirme.midas.catalogos.tipoempaque;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for TipoEmpaqueFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface TipoEmpaqueFacadeRemote extends
		MidasInterfaceBase<TipoEmpaqueDTO> {
	/**
	 * Perform an initial save of a previously unsaved TipoEmpaqueDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            TipoEmpaqueDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoEmpaqueDTO entity);

	/**
	 * Delete a persistent TipoEmpaqueDTO entity.
	 * 
	 * @param entity
	 *            TipoEmpaqueDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoEmpaqueDTO entity);

	/**
	 * Persist a previously saved TipoEmpaqueDTO entity and return it or a copy
	 * of it to the sender. A copy of the TipoEmpaqueDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            TipoEmpaqueDTO entity to update
	 * @return TipoEmpaqueDTO the persisted TipoEmpaqueDTO entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoEmpaqueDTO update(TipoEmpaqueDTO entity);

	/**
	 * Find all TipoEmpaqueDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the TipoEmpaqueDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<TipoEmpaqueDTO> found by query
	 */
	public List<TipoEmpaqueDTO> findByProperty(String propertyName, Object value);

	public List<TipoEmpaqueDTO> listarFiltrado(TipoEmpaqueDTO tipoEmpaqueDTO);
}