package mx.com.afirme.midas.catalogos.tipoempaque;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.catalogos.giro.GiroDTO;

/**
 * TipoEmpaqueDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCTIPOEMPAQUE", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = "CODIGOTIPOEMPAQUE"))
public class TipoEmpaqueDTO extends CacheableDTO {

	private static final long serialVersionUID = 419221784748062934L;
	private BigDecimal idTipoEmpaque;
	private BigDecimal codigoTipoEmpaque;
	private String descripcionTipoEmpaque;

	/** default constructor */
	public TipoEmpaqueDTO() {
	}

	@Id
	@SequenceGenerator(name = "IDTCTIPOEMPAQUE_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCTIPOEMPAQUE_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCTIPOEMPAQUE_SEQ_GENERADOR")
	@Column(name = "IDTCTIPOEMPAQUE", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTipoEmpaque() {
		return idTipoEmpaque;
	}

	public void setIdTipoEmpaque(BigDecimal idTipoEmpaque) {
		this.idTipoEmpaque = idTipoEmpaque;
	}

	@Column(name = "CODIGOTIPOEMPAQUE", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoTipoEmpaque() {
		return codigoTipoEmpaque;
	}

	public void setCodigoTipoEmpaque(BigDecimal codigoTipoEmpaque) {
		this.codigoTipoEmpaque = codigoTipoEmpaque;
	}

	@Column(name = "DESCRIPCIONTIPOEMPAQUE", nullable = false, length = 200)
	public String getDescripcionTipoEmpaque() {
		return descripcionTipoEmpaque;
	}

	public void setDescripcionTipoEmpaque(String descripcionTipoEmpaque) {
		this.descripcionTipoEmpaque = descripcionTipoEmpaque;
	}

	@Override
	public String getDescription() {
		return this.descripcionTipoEmpaque;
	}

	@Override
	public Object getId() {
		return this.idTipoEmpaque;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof GiroDTO) {
			TipoEmpaqueDTO tipoEmpaqueDTO = (TipoEmpaqueDTO) object;
			equal = tipoEmpaqueDTO.getIdTipoEmpaque()
					.equals(this.idTipoEmpaque);
		} // End of if
		return equal;
	}

}