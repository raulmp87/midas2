package mx.com.afirme.midas.catalogos.distanciaciudad;
// default package

import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for DistanciaCiudadFacade.
 * @author MyEclipse Persistence Tools
 */


public interface DistanciaCiudadFacadeRemote extends MidasInterfaceBase<DistanciaCiudadDTO> {
		/**
	 Perform an initial save of a previously unsaved DistanciaCiudadDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DistanciaCiudadDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DistanciaCiudadDTO entity);
    /**
	 Delete a persistent DistanciaCiudadDTO entity.
	  @param entity DistanciaCiudadDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DistanciaCiudadDTO entity);
   /**
	 Persist a previously saved DistanciaCiudadDTO entity and return it or a copy of it to the sender. 
	 A copy of the DistanciaCiudadDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DistanciaCiudadDTO entity to update
	 @return DistanciaCiudadDTO the persisted DistanciaCiudadDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public DistanciaCiudadDTO update(DistanciaCiudadDTO entity);
	public DistanciaCiudadDTO findById( Long id);
	 /**
	 * Find all DistanciaCiudadDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DistanciaCiudadDTO property to query
	  @param value the property value to match
	  	  @return List<DistanciaCiudadDTO> found by query
	 */
	public List<DistanciaCiudadDTO> findByProperty(String propertyName, Object value
		);
	public List<DistanciaCiudadDTO> findByIdciudadorigen(Object idciudadorigen
		);
	public List<DistanciaCiudadDTO> findByIdciudaddestino(Object idciudaddestino
		);
	public List<DistanciaCiudadDTO> findByDistancia(Object distancia
		);
	public List<DistanciaCiudadDTO> findByIdestadoorigen(Object idestadoorigen
		);
	public List<DistanciaCiudadDTO> findByIdestadodestino(Object idestadodestino
		);
	/**
	 * Find all DistanciaCiudadDTO entities.
	  	  @return List<DistanciaCiudadDTO> all DistanciaCiudadDTO entities
	 */
	public List<DistanciaCiudadDTO> findAll(
		);
	
	public List<DistanciaCiudadDTO> listarFiltrado(DistanciaCiudadDTO distanciaCiudadDTO);
}