package mx.com.afirme.midas.catalogos.distanciaciudad;
// default package

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.base.CacheableDTO;


/**
 * DistanciaCiudadDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TRDISTANCIACIUDAD"
    ,schema="MIDAS"
, uniqueConstraints = @UniqueConstraint(columnNames={"IDCIUDADORIGEN", "IDCIUDADDESTINO"})
)

public class DistanciaCiudadDTO extends CacheableDTO {


    // Fields    

     private Long idDistanciaCiudad;
     private String idCiudadOrigen;
     private String idCiudadDestino;
     private Long distancia;
     private String idEstadoOrigen;
     private String idEstadoDestino;
     
     private static final long serialVersionUID = 1L;


    // Constructors

    /** default constructor */
    public DistanciaCiudadDTO() {
    }

    
    /** full constructor */
    public DistanciaCiudadDTO(Long idDistanciaCiudad, String idCiudadOrigen, String idCiudadDestino, Long distancia, String idEstadoOrigen, String idEstadoDestino) {
        this.idDistanciaCiudad = idDistanciaCiudad;
        this.idCiudadOrigen = idCiudadOrigen;
        this.idCiudadDestino = idCiudadDestino;
        this.distancia = distancia;
        this.idEstadoOrigen = idEstadoOrigen;
        this.idEstadoDestino = idEstadoDestino;
    }

   
    // Property accessors
    @Id 
    @SequenceGenerator(name = "IDTRDISTANCIACIUDAD_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTRDISTANCIACIUDAD_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTRDISTANCIACIUDAD_SEQ_GENERADOR")
    @Column(name="IDTRDISTANCIACIUDAD", unique=true, nullable=false, precision=10, scale=0)
    public Long getIdDistanciaCiudad() {
        return this.idDistanciaCiudad;
    }
    
    public void setIdDistanciaCiudad(Long idDistanciaCiudad) {
        this.idDistanciaCiudad = idDistanciaCiudad;
    }
    
    @Column(name="IDCIUDADORIGEN", nullable=false, precision=10, scale=0)

    public String getIdCiudadOrigen() {
        return this.idCiudadOrigen;
    }
    
    public void setIdCiudadOrigen(String idCiudadOrigen) {
        this.idCiudadOrigen = idCiudadOrigen;
    }
    
    @Column(name="IDCIUDADDESTINO", nullable=false, precision=10, scale=0)

    public String getIdCiudadDestino() {
        return this.idCiudadDestino;
    }
    
    public void setIdCiudadDestino(String idCiudadDestino) {
        this.idCiudadDestino = idCiudadDestino;
    }
    
    @Column(name="DISTANCIA", nullable=false, precision=10, scale=0)

    public Long getDistancia() {
        return this.distancia;
    }
    
    public void setDistancia(Long distancia) {
        this.distancia = distancia;
    }
    
    @Column(name="IDESTADOORIGEN", nullable=false, precision=10, scale=0)

    public String getIdEstadoOrigen() {
        return this.idEstadoOrigen;
    }
    
    public void setIdEstadoOrigen(String idEstadoOrigen) {
        this.idEstadoOrigen = idEstadoOrigen;
    }
    
    @Column(name="IDESTADODESTINO", nullable=false, precision=10, scale=0)

    public String getIdEstadoDestino() {
        return this.idEstadoDestino;
    }
    
    public void setIdEstadoDestino(String idEstadoDestino) {
        this.idEstadoDestino = idEstadoDestino;
    }


	@Override
	public String getDescription() {
		return null;
	}


	@Override
	public Object getId() {
		return this.idDistanciaCiudad;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof DistanciaCiudadDTO) {
			DistanciaCiudadDTO distanciaCiudadDTO = (DistanciaCiudadDTO) object;
			equal = distanciaCiudadDTO.getIdDistanciaCiudad().equals(this.idDistanciaCiudad);
		} // End of if
		return equal;
	}
}