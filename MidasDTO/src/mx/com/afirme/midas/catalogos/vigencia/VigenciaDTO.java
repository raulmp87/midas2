package mx.com.afirme.midas.catalogos.vigencia;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="TCVIGENCIA"
    ,schema="MIDAS"
)

public class VigenciaDTO extends CacheableDTO implements java.io.Serializable, Entidad{

	/**
	 * 
	 */
	private static final long serialVersionUID = 418221514091221653L;
	private BigDecimal idTcVigencia;
    private String descripcion;

    // Constructors

    /** default constructor */
    public VigenciaDTO() {
    }

	/** minimal constructor */
    public VigenciaDTO(BigDecimal idTcVigencia, String descripcion) {
        this.idTcVigencia = idTcVigencia;
        this.descripcion = descripcion;
    }

	// Property accessors
    @Id 
    @SequenceGenerator(name = "IDTCVIGENCIA_SEQ_GEN", allocationSize = 1, sequenceName = "MIDAS.IDTCVIGENCIA_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCVIGENCIA_SEQ_GEN")
    @Column(name="IDTCVIGENCIA", unique=true, nullable=false, precision=22, scale=0)
    public BigDecimal getIdTcVigencia() {
		return idTcVigencia;
	}

	public void setIdTcVigencia(BigDecimal idTcVigencia) {
		this.idTcVigencia = idTcVigencia;
	}
    
    @Column(name="DESCRIPCION", length=100)

    public String getDescripcion() {
        return this.descripcion.toUpperCase();
    }    

	public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VigenciaDTO other = (VigenciaDTO) obj;
		if (idTcVigencia == null) {
			if (other.idTcVigencia != null)
				return false;
		} else if (!idTcVigencia.equals(other.idTcVigencia))
			return false;
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.idTcVigencia;
	}

	@Override
	public String getValue() {
		return this.descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getBusinessKey() {
		return this.idTcVigencia;
	}

	@Override
	public String getDescription() {
		// TODO Apéndice de método generado automáticamente
		return descripcion;
	}

	@Override
	public Object getId() {
		// TODO Apéndice de método generado automáticamente
		return idTcVigencia;
	}
}
