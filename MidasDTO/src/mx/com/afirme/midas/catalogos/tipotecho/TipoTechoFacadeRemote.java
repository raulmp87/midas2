package mx.com.afirme.midas.catalogos.tipotecho;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for TctipotechoFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface TipoTechoFacadeRemote extends MidasInterfaceBase<TipoTechoDTO> {
	/**
	 * Perform an initial save of a previously unsaved Tctipotecho entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            Tctipotecho entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TipoTechoDTO entity);

	/**
	 * Delete a persistent Tctipotecho entity.
	 * 
	 * @param entity
	 *            Tctipotecho entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TipoTechoDTO entity);

	/**
	 * Persist a previously saved Tctipotecho entity and return it or a copy of
	 * it to the sender. A copy of the Tctipotecho entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            Tctipotecho entity to update
	 * @return Tctipotecho the persisted Tctipotecho entity instance, may not be
	 *         the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TipoTechoDTO update(TipoTechoDTO entity);

	/**
	 * Find all Tctipotecho entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the Tctipotecho property to query
	 * @param value
	 *            the property value to match
	 * @return List<Tctipotecho> found by query
	 */
	public List<TipoTechoDTO> findByProperty(String propertyName, Object value);

	public List<TipoTechoDTO> findByDescripciontipotecho(
			Object descripciontipotecho);

	/**
	 * Find all Tctipotecho entities.
	 * 
	 * @return List<Tctipotecho> all Tctipotecho entities
	 */
	public List<TipoTechoDTO> findAll();
	
	public List<TipoTechoDTO> listarFiltrado(TipoTechoDTO tipoTechoDTO);
}