package mx.com.afirme.midas.catalogos.tipotecho;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.base.PermitidoCotizarCasa;

/**
 * Tctipotecho entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCTIPOTECHO", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = "IDTCTIPOTECHO"))
@Cache(
  type=CacheType.SOFT,
  size=1000,
  expiry=36000000
)
public class TipoTechoDTO extends CacheableDTO implements PermitidoCotizarCasa {
	/**
	 * 
	 */
	private static final long serialVersionUID = -496022045167309923L;
	
	private BigDecimal codigoTipoTecho;
	private BigDecimal idTipoTecho;
	private String descripcionTipoTecho;
    private Boolean permitidoCasa;


	/** default constructor */
	public TipoTechoDTO() {
	}
	
	@Id
	@SequenceGenerator(name = "IDTCTIPOTECHO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCTIPOTECHO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCTIPOTECHO_SEQ_GENERADOR")
	@Column(name = "IDTCTIPOTECHO", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTipoTecho() {
		return idTipoTecho;
	}

	public void setIdTipoTecho(BigDecimal idTipoTecho) {
		this.idTipoTecho = idTipoTecho;
	}
	
	@Column(name = "CODIGOTIPOTECHO", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoTipoTecho() {
		return codigoTipoTecho;
	}

	public void setCodigoTipoTecho(BigDecimal codigoTipoTecho) {
		this.codigoTipoTecho = codigoTipoTecho;
	}

	
	@Column(name = "DESCRIPCIONTIPOTECHO", nullable = false, length = 200)
	public String getDescripcionTipoTecho() {
		return descripcionTipoTecho;
	}

	public void setDescripcionTipoTecho(String descripcionTipoTecho) {
		this.descripcionTipoTecho = descripcionTipoTecho;
	}

	@Override
	public String getDescription() {
		return this.descripcionTipoTecho;
	}

	@Override
	public Object getId() {
		return this.idTipoTecho;
	}

	@Column
	public Boolean getPermitidoCasa() {
		return permitidoCasa;
	}

	public void setPermitidoCasa(Boolean permitidoCasa) {
		this.permitidoCasa = permitidoCasa;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof TipoTechoDTO) {
			TipoTechoDTO dto = (TipoTechoDTO) object;
			equal = dto.getIdTipoTecho().equals(this.idTipoTecho);
		} // End of if
		return equal;
	}
	
}