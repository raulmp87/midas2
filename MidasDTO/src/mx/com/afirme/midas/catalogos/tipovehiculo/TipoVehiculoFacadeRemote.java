package mx.com.afirme.midas.catalogos.tipovehiculo;
// default package

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for TipoVehiculoDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface TipoVehiculoFacadeRemote extends MidasInterfaceBase<TipoVehiculoDTO>{
		/**
	 Perform an initial save of a previously unsaved TipoVehiculoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity TipoVehiculoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(TipoVehiculoDTO entity);
    /**
	 Delete a persistent TipoVehiculoDTO entity.
	  @param entity TipoVehiculoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(TipoVehiculoDTO entity);
   /**
	 Persist a previously saved TipoVehiculoDTO entity and return it or a copy of it to the sender. 
	 A copy of the TipoVehiculoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity TipoVehiculoDTO entity to update
	 @return TipoVehiculoDTO the persisted TipoVehiculoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public TipoVehiculoDTO update(TipoVehiculoDTO entity);
	 /**
	 * Find all TipoVehiculoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the TipoVehiculoDTO property to query
	  @param value the property value to match
	  	  @return List<TipoVehiculoDTO> found by query
	 */
	public List<TipoVehiculoDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all TipoVehiculoDTO entities.
	  	  @return List<TipoVehiculoDTO> all TipoVehiculoDTO entities
	 */
	public List<TipoVehiculoDTO> findAll();
	
	public List<TipoVehiculoDTO> listarFiltrado(TipoVehiculoDTO tipoVehiculoDTO);
}