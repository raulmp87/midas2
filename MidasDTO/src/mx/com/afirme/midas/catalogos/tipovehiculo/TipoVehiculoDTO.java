package mx.com.afirme.midas.catalogos.tipovehiculo;
// default package


import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.catalogos.tipobienautos.TipoBienAutosDTO;

/**
* TipoVehiculoDTO entity. @author MyEclipse Persistence Tools
*/
@Entity
@Table(name="TCTIPOVEHICULO"
    ,schema="MIDAS"
)
public class TipoVehiculoDTO extends CacheableDTO{


    // Fields    

     /**
    * 
     */
     private static final long serialVersionUID = 4594543680401952752L;
     private BigDecimal idTcTipoVehiculo;
     private TipoBienAutosDTO tipoBienAutosDTO = new TipoBienAutosDTO();
     private String codigoTipoVehiculo;
     private String descripcionTipoVehiculo;
     

    // Constructors

    /** default constructor */
    public TipoVehiculoDTO() {
    }

    /** minimal constructor */
    public TipoVehiculoDTO(BigDecimal idTcTipoVehiculo, TipoBienAutosDTO tipoBienAutosDTO, String codigoTipoVehiculo) {
        this.idTcTipoVehiculo = idTcTipoVehiculo;
        this.tipoBienAutosDTO = tipoBienAutosDTO;
        this.codigoTipoVehiculo = codigoTipoVehiculo;
    }
    
    /** full constructor */
    public TipoVehiculoDTO(BigDecimal idTcTipoVehiculo,  TipoBienAutosDTO tipoBienAutosDTO, String codigoTipoVehiculo, String descripcionTipoVehiculo) {
        this.idTcTipoVehiculo = idTcTipoVehiculo;
        this.tipoBienAutosDTO = tipoBienAutosDTO;
        this.codigoTipoVehiculo = codigoTipoVehiculo;
        this.descripcionTipoVehiculo = descripcionTipoVehiculo;
    }

   
    // Property accessors
    @Id 
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTCTIPOVEHICULO_SEQ")
    @SequenceGenerator(name="IDTCTIPOVEHICULO_SEQ", sequenceName="MIDAS.IDTCTIPOVEHICULO_SEQ", allocationSize=1) 
    @Column(name="IDTCTIPOVEHICULO", unique=true, nullable=false, precision=22, scale=0)
    public BigDecimal getIdTcTipoVehiculo() {
        return this.idTcTipoVehiculo;
    }
    
    public void setIdTcTipoVehiculo(BigDecimal idTcTipoVehiculo) {
        this.idTcTipoVehiculo = idTcTipoVehiculo;
    }
    
                @ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="CLAVETIPOBIEN", nullable=false, insertable=true, updatable=true)

    public TipoBienAutosDTO getTipoBienAutosDTO() {
        return this.tipoBienAutosDTO;
    }
    
    public void setTipoBienAutosDTO(TipoBienAutosDTO tipoBienAutosDTO) {
        this.tipoBienAutosDTO = tipoBienAutosDTO;
    }
    
    @Column(name="codigoTipoVehiculo", nullable=false, length=5)

    public String getCodigoTipoVehiculo() {
        return this.codigoTipoVehiculo!=null?this.codigoTipoVehiculo.toUpperCase():this.codigoTipoVehiculo;
    }
    
    public void setCodigoTipoVehiculo(String codigoTipoVehiculo) {
        this.codigoTipoVehiculo = codigoTipoVehiculo;
    }
    
    @Column(name="descripcionTipoVehiculo", length=100)

    public String getDescripcionTipoVehiculo() {
        return this.descripcionTipoVehiculo!=null?this.descripcionTipoVehiculo.toUpperCase():this.descripcionTipoVehiculo;
    }
    
    public void setDescripcionTipoVehiculo(String descripcionTipoVehiculo) {
        this.descripcionTipoVehiculo = descripcionTipoVehiculo;
    }

    @Override
    public String getDescription() {
                   return getDescripcionTipoVehiculo();
    }

    

    @Override
    public int hashCode() {
                   final int prime = 31;
                   int result = 1;
                   result = prime * result + ((idTcTipoVehiculo == null) ? 0 : idTcTipoVehiculo.hashCode());
                   return result;
    }

    @Override
    public boolean equals(Object obj) {
                   if (this == obj)
                                   return true;
                   if (getClass() != obj.getClass())
                                   return false;
                   TipoVehiculoDTO other = (TipoVehiculoDTO) obj;
                   if (idTcTipoVehiculo == null) {
                                   if (other.idTcTipoVehiculo != null)
                                                   return false;
                   } else if (!idTcTipoVehiculo.equals(other.idTcTipoVehiculo))
                                   return false;
                   return true;
    }

	@Override
	public BigDecimal getId() {
		return idTcTipoVehiculo;
	}
}
