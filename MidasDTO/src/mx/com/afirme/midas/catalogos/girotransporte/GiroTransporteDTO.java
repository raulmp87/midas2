package mx.com.afirme.midas.catalogos.girotransporte;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;

/**
 * GiroTransporte entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table (name = "TCGIROTRANSPORTE"
	, schema = "MIDAS")	
public class GiroTransporteDTO extends CacheableDTO {

	private static final long serialVersionUID = 5483782731056397941L;
	
	// Fields
	private BigDecimal idGiroTransporte;
	private BigDecimal codigoGiroTransporte;
	private BigDecimal idGrupoROT;
	private String descripcionGiroTransporte;

	// Constructors

	/** default constructor */
	public GiroTransporteDTO() {
	}

	// Property accessors
	
	@Id 
    @SequenceGenerator(name = "IDTCGIROTRANSPORTE_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCGIROTRANSPORTE_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCGIROTRANSPORTE_SEQ_GENERADOR")
    @Column(name = "IDTCGIROTRANSPORTE", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdGiroTransporte() {
		return this.idGiroTransporte;
	}

	public void setIdGiroTransporte(BigDecimal idGiroTransporte) {
		this.idGiroTransporte = idGiroTransporte;
	}

	@Column(name = "CODIGOGIROTRANSPORTE", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoGiroTransporte() {
		return this.codigoGiroTransporte;
	}

	public void setCodigoGiroTransporte(BigDecimal codigoGiroTransporte) {
		this.codigoGiroTransporte = codigoGiroTransporte;
	}

	@Column(name = "IDGRUPOROT", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdGrupoROT() {
		return this.idGrupoROT;
	}

	public void setIdGrupoROT(BigDecimal idGrupoROT) {
		this.idGrupoROT = idGrupoROT;
	}

	@Column(name = "DESCRIPCIONGIROTRANSPORTE", nullable = false, length = 200)
	public String getDescripcionGiroTransporte() {
		return this.descripcionGiroTransporte;
	}

	public void setDescripcionGiroTransporte(String descripcionGiroTransporte) {
		this.descripcionGiroTransporte = descripcionGiroTransporte;
	}

	@Override
	public String getDescription() {
		return this.descripcionGiroTransporte;
	}

	@Override
	public Object getId() {
		return this.idGiroTransporte;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof GiroTransporteDTO) {
			GiroTransporteDTO giroTransporteDTO = (GiroTransporteDTO) object;
			equal = giroTransporteDTO.getIdGiroTransporte().equals(
					this.idGiroTransporte);
		}
		return equal;
	}
}