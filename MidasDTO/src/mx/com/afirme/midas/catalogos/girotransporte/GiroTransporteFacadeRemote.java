package mx.com.afirme.midas.catalogos.girotransporte;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for GiroTransporteFacade.
 * 
 * @author MyEclipse Persistence Tools
 */


public interface GiroTransporteFacadeRemote extends
		MidasInterfaceBase<GiroTransporteDTO> {
	/**
	 * Perform an initial save of a previously unsaved GiroTransporte entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            GiroTransporte entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(GiroTransporteDTO entity);

	/**
	 * Delete a persistent GiroTransporte entity.
	 * 
	 * @param entity
	 *            GiroTransporte entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(GiroTransporteDTO entity);

	/**
	 * Persist a previously saved GiroTransporte entity and return it or a copy
	 * of it to the sender. A copy of the GiroTransporte entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            GiroTransporte entity to update
	 * @return GiroTransporte the persisted GiroTransporte entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public GiroTransporteDTO update(GiroTransporteDTO entity);

	/**
	 * Find all GiroTransporte entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the GiroTransporte property to query
	 * @param value
	 *            the property value to match
	 * @return List<GiroTransporte> found by query
	 */
	public List<GiroTransporteDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all GiroTransporte entities.
	 * 
	 * @return List<GiroTransporte> all GiroTransporte entities
	 */
	public List<GiroTransporteDTO> findAll();

	/**
	 * Find GiroTransporte entities filtering them by a some parameter(s).
	 * 
	 * @return List<GiroTransporte> all GiroTransporte entities
	 */
	public List<GiroTransporteDTO> listarFiltrado(
			GiroTransporteDTO giroTransporteDTO);

}