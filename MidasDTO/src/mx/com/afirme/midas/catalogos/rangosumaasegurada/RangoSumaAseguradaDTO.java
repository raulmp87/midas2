package mx.com.afirme.midas.catalogos.rangosumaasegurada;
// default package

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * RangoSumaAseguradaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCRANGOSUMAASEGURADA"
    ,schema="MIDAS"
)

public class RangoSumaAseguradaDTO  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int idTcRangoSumaAsegurada;
     private Double rango;
     private int posicion;


    // Constructors

    /** default constructor */
    public RangoSumaAseguradaDTO() {
    }
   
    // Property accessors
    @Id 
	@SequenceGenerator(name = "IDTCRANGOSUMAASEGURADA_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCRANGOSUMAASEGURADA_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCRANGOSUMAASEGURADA_SEQ_GENERADOR")	       
    @Column(name="IDTCRANGOSUMAASEGURADA", unique=true, nullable=false, precision=22, scale=0)
    public int getIdTcRangoSumaAsegurada() {
        return this.idTcRangoSumaAsegurada;
    }
    
    public void setIdTcRangoSumaAsegurada(int idTcRangoSumaAsegurada) {
        this.idTcRangoSumaAsegurada = idTcRangoSumaAsegurada;
    }
    
    @Column(name="RANGO", precision=16)

    public Double getRango() {
        return this.rango;
    }
    
    public void setRango(Double rango) {
        this.rango = rango;
    }
    
    @Column(name="POSICION", precision=3, scale=0)

    public int getPosicion() {
        return this.posicion;
    }
    
    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }
   








}