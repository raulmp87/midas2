package mx.com.afirme.midas.catalogos.rangosumaasegurada;
// default package

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for RangoSumaAseguradaFacade.
 * @author MyEclipse Persistence Tools
 */


public interface RangoSumaAseguradaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved RangoSumaAseguradaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity RangoSumaAseguradaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(RangoSumaAseguradaDTO entity);
    /**
	 Delete a persistent RangoSumaAseguradaDTO entity.
	  @param entity RangoSumaAseguradaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(RangoSumaAseguradaDTO entity);
   /**
	 Persist a previously saved RangoSumaAseguradaDTO entity and return it or a copy of it to the sender. 
	 A copy of the RangoSumaAseguradaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity RangoSumaAseguradaDTO entity to update
	 @return RangoSumaAseguradaDTO the persisted RangoSumaAseguradaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public RangoSumaAseguradaDTO update(RangoSumaAseguradaDTO entity);
	public RangoSumaAseguradaDTO findById( int id);
	 /**
	 * Find all RangoSumaAseguradaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the RangoSumaAseguradaDTO property to query
	  @param value the property value to match
	  	  @return List<RangoSumaAseguradaDTO> found by query
	 */
	public List<RangoSumaAseguradaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all RangoSumaAseguradaDTO entities.
	  	  @return List<RangoSumaAseguradaDTO> all RangoSumaAseguradaDTO entities
	 */
	public List<RangoSumaAseguradaDTO> findAll(
		);	
}