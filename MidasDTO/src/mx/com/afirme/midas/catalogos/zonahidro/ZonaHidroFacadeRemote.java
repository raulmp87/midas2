package mx.com.afirme.midas.catalogos.zonahidro;
// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;


/**
 * Remote interface for ZonaHidroFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ZonaHidroFacadeRemote extends MidasInterfaceBase<ZonaHidroDTO>{
	/**
	 * Perform an initial save of a previously unsaved ZonaHidroDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            ZonaHidroDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ZonaHidroDTO entity);

	/**
	 * Delete a persistent ZonaHidroDTO entity.
	 * 
	 * @param entity
	 *            ZonaHidroDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ZonaHidroDTO entity);

	/**
	 * Persist a previously saved ZonaHidroDTO entity and return it or a copy of it
	 * to the sender. A copy of the ZonaHidroDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            ZonaHidroDTO entity to update
	 * @return ZonaHidroDTO the persisted ZonaHidroDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ZonaHidroDTO update(ZonaHidroDTO entity);

	/**
	 * Find all ZonaHidroDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ZonaHidroDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<ZonaHidroDTO> found by query
	 */
	public List<ZonaHidroDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all ZonaHidroDTO entities.
	 * 
	 * @return List<ZonaHidroDTO> all ZonaHidroDTO entities
	 */
	public List<ZonaHidroDTO> findAll();
	
	/**
	 * Find filtered ZonaHidroDTO entities.
	  	  @return List<ZonaHidroDTO> filtered ZonaHidroDTO entities
	 */
	public List<ZonaHidroDTO> listarFiltrado(ZonaHidroDTO zonaHidroDTO);

}