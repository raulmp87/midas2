package mx.com.afirme.midas.catalogos.zonahidro;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.catalogos.codigopostalzonahidro.CodigoPostalZonaHidroDTO;

/**
 * ZonaHidroDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCZONAHIDRO", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = "CODIGOZONAHIDRO"))
public class ZonaHidroDTO extends CacheableDTO  {
		
	private static final long serialVersionUID = 2212079138070376922L;
	// Fields

	private BigDecimal idTcZonaHidro;
	private String codigoZonaHidro;
	private String descripcionZonaHidro;
	private BigDecimal valorDefaultCoaseguro;
	private BigDecimal valorDefaultCoaseguroBMCE;
	private BigDecimal valorDefaultDeducible;
	private BigDecimal valorDefaultDeducibleBMCE;
	private List<CodigoPostalZonaHidroDTO> tcCodigoPostalZonaHidros;

	// Constructors

	/** default constructor */
	public ZonaHidroDTO() {
		if (tcCodigoPostalZonaHidros == null)
			tcCodigoPostalZonaHidros = new ArrayList<CodigoPostalZonaHidroDTO>();
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTCZONAHIDRO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCZONAHIDRO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCZONAHIDRO_SEQ_GENERADOR")
	@Column(name = "IDTCZONAHIDRO", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcZonaHidro() {
		return this.idTcZonaHidro;
	}

	public void setIdTcZonaHidro(BigDecimal idTcZonaHidro) {
		this.idTcZonaHidro = idTcZonaHidro;
	}

	@Column(name = "CODIGOZONAHIDRO")
	public String getCodigoZonaHidro() {
		return this.codigoZonaHidro;
	}

	public void setCodigoZonaHidro(String codigoZonaHidro) {
		this.codigoZonaHidro = codigoZonaHidro;
	}

	@Column(name = "DESCRIPCIONZONAHIDRO", nullable = false, length = 200)
	public String getDescripcionZonaHidro() {
		return this.descripcionZonaHidro;
	}

	public void setDescripcionZonaHidro(String descripcionZonaHidro) {
		this.descripcionZonaHidro = descripcionZonaHidro;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "zonaHidro")
	public List<CodigoPostalZonaHidroDTO> getTcCodigoPostalZonaHidros() {
		return this.tcCodigoPostalZonaHidros;
	}

	public void setTcCodigoPostalZonaHidros(
			List<CodigoPostalZonaHidroDTO> tcCodigoPostalZonaHidros) {
		this.tcCodigoPostalZonaHidros = tcCodigoPostalZonaHidros;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof ZonaHidroDTO) {
			ZonaHidroDTO zonaHidroDTO = (ZonaHidroDTO) object;
			equal = zonaHidroDTO.getIdTcZonaHidro().equals(
					this.getIdTcZonaHidro());
		} // End of if
		return equal;
	}
	@Column(name = "VALORDEFAULTCOASEGURO", precision = 16, scale = 4)
	public BigDecimal getValorDefaultCoaseguro() {
		return this.valorDefaultCoaseguro;
	}

	public void setValorDefaultCoaseguro(BigDecimal valorDefaultCoaseguro) {
		this.valorDefaultCoaseguro = valorDefaultCoaseguro;
	}

	@Column(name = "VALORDEFAULTCOASEGUROBMCE", precision = 16, scale = 4)
	public BigDecimal getValorDefaultCoaseguroBMCE() {
		return this.valorDefaultCoaseguroBMCE;
	}

	public void setValorDefaultCoaseguroBMCE(
			BigDecimal valorDefaultCoaseguroBMCE) {
		this.valorDefaultCoaseguroBMCE = valorDefaultCoaseguroBMCE;
	}

	@Column(name = "VALORDEFAULTDEDUCIBLE", precision = 16, scale = 4)
	public BigDecimal getValorDefaultDeducible() {
		return this.valorDefaultDeducible;
	}

	public void setValorDefaultDeducible(BigDecimal valorDefaultDeducible) {
		this.valorDefaultDeducible = valorDefaultDeducible;
	}

	@Column(name = "VALORDEFAULTDEDUCIBLEBMCE", precision = 16, scale = 4)
	public BigDecimal getValorDefaultDeducibleBMCE() {
		return this.valorDefaultDeducibleBMCE;
	}

	public void setValorDefaultDeducibleBMCE(
			BigDecimal valorDefaultDeducibleBMCE) {
		this.valorDefaultDeducibleBMCE = valorDefaultDeducibleBMCE;
	}	
	
	@Override
	public String getDescription() {
		return this.descripcionZonaHidro;
	}

	@Override
	public Object getId() {
		return this.idTcZonaHidro;
	}	
}