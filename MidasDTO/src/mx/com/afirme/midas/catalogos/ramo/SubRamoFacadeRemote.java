package mx.com.afirme.midas.catalogos.ramo;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for SubRamoFacade.
 * @author MyEclipse Persistence Tools
 */


public interface SubRamoFacadeRemote extends MidasInterfaceBase<SubRamoDTO> {
		/**
	 Perform an initial save of a previously unsaved SubRamoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity SubRamoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(SubRamoDTO entity);
    /**
	 Delete a persistent SubRamoDTO entity.
	  @param entity SubRamoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(SubRamoDTO entity);
   /**
	 Persist a previously saved SubRamoDTO entity and return it or a copy of it to the sender. 
	 A copy of the SubRamoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity SubRamoDTO entity to update
	 @return SubRamoDTO the persisted SubRamoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public SubRamoDTO update(SubRamoDTO entity);

	 /**
	 * Find all SubRamoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the SubRamoDTO property to query
	  @param value the property value to match
	  	  @return List<SubRamoDTO> found by query
	 */
	public List<SubRamoDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all SubRamoDTO entities.
	  	  @return List<SubRamoDTO> all SubRamoDTO entities
	 */
	public List<SubRamoDTO> findAll();	
	
	/**
	 * Obtiene todos los subRamos que pertenecen al ramo especificado.
	 * @param idTcRamo
	 * @return
	 */
	public List<SubRamoDTO> findAll(BigDecimal idTcRamo);
	
	public List<SubRamoDTO> listarFiltrado(SubRamoDTO subRamoDTO);
	
	public List<SubRamoDTO> listarFiltradoLike(SubRamoDTO subRamoDTO);
	
	public Long obtenerTotalFiltrado(SubRamoDTO subRamoDTO);

	public List<SubRamoDTO> getSubRamosInCotizacion(BigDecimal idToCotizacion);
	
	public List<SubRamoDTO> getSubRamosPorIncisoCotizacion(
			BigDecimal idToCotizacion, BigDecimal numeroInciso);
	
	public List<SubRamoDTO> getSubRamosPorTipoPoliza(BigDecimal idToTipoPoliza);
	
}