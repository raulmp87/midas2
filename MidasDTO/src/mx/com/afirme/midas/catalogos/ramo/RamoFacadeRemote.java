package mx.com.afirme.midas.catalogos.ramo;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;
import javax.persistence.EntityTransaction;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for TcramoFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface RamoFacadeRemote extends MidasInterfaceBase<RamoDTO> {
	/**
	 * Perform an initial save of a previously unsaved Tcramo entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            Tcramo entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(RamoDTO entity);

	/**
	 * Delete a persistent Tcramo entity.
	 * 
	 * @param entity
	 *            Tcramo entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(RamoDTO entity);

	/**
	 * Persist a previously saved Tcramo entity and return it or a copy of it to
	 * the sender. A copy of the Tcramo entity parameter is returned when the
	 * JPA persistence mechanism has not previously been tracking the updated
	 * entity.
	 * 
	 * @param entity
	 *            Tcramo entity to update
	 * @return Tcramo the persisted Tcramo entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public RamoDTO update(RamoDTO entity);

	/**
	 * Find all Tcramo entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the Tcramo property to query
	 * @param value
	 *            the property value to match
	 * @return List<Tcramo> found by query
	 */
	public List<RamoDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all Tcramo entities.
	 * 
	 * @return List<Tcramo> all Tcramo entities
	 */
	public List<RamoDTO> findAll();
	
	public List<RamoDTO> listarFiltrado(RamoDTO ramoDTO);
	
	public List<RamoDTO> listarFiltradoLike(RamoDTO ramoDTO);	
	
	public EntityTransaction getTransaction();
	
	/**
	 * Lista todos los Ramos que no est�n asociadas una secci�n determinada 
	 * por el id que recibe el m�todo
	 * @param BigDecimal idToSeccion el id de la seccion.
	 * @return List<RamoDTO> registros Ramo que no est�n asociados a la seccion.
	 */
	public List<RamoDTO> obtenerRamosSinAsociarSeccion(BigDecimal idToSeccion, BigDecimal idToTipoPoliza);
	
	/**
	 * Lista todos los Ramos que no est�n asociadas un producto determinado 
	 * por el id que recibe el m�todo
	 * @param BigDecimal idToProducto el id del producto.
	 * @return List<RamoDTO> registros Ramo que no est�n asociados al producto.
	 */
	public List<RamoDTO> obtenerRamosSinAsociarProducto(BigDecimal idToProducto);
	
	/**
	 * Lista todos los Ramos que no est�n asociadas un TipoPoliza determinado 
	 * por el id que recibe el m�todo
	 * @param BigDecimal idToTipoPoliza el id del tipoPoliza.
	 * @return List<RamoDTO> registros Ramo que no est�n asociados al TipoPoliza.
	 */
	public List<RamoDTO> obtenerRamosSinAsociarTipoPoliza(BigDecimal idToTipoPoliza,BigDecimal idToProducto);
	
	/**
	 * Lista todos los ramos que no se puedan desasociar a un producto.
	 * Los ramos que no se pueden desasociar del producto son aquellos que se encuentran asociados a alguna P�liza VIGENTE que le pertenece al producto. 
	 * @param BigDecimal idToProducto el id del Producto.
	 * @return List<RamoDTO> registros RamoProducto que no se deban desasociar al producto.
	 */
	public List<RamoDTO> obtenerRamosObligatoriosPorProducto(BigDecimal idToProducto);
	
	/**
	 * Lista todos los ramos que no se puedan desasociar a un TipoPoliza.
	 * Los ramos que no se pueden desasociar del tipoPoliza son aquellos que se encuentran asociados a alguna seccion VIGENTE asociada al tipoPoliza. 
	 * @param BigDecimal idToTipoPoliza el id del TipoPoliza.
	 * @return List<RamoDTO> registros Ramo que no se deban desasociar al producto.
	 */
	public List<RamoDTO> obtenerRamosObligatoriosPorTipoPoliza(BigDecimal idToTipoPoliza);
	
	/**
	 * Establece la clave negocio que se establece por dafult para todas las busquedas.
	 * @param claveNegocioDefault
	 */
	public void setClaveNegocioDefault(String claveNegocioDefault);
	/**
	 * Obtiene la clave negocio que se establece por dafult para todas las busquedas.
	 * @param claveNegocioDefault
	 */
	public String getClaveNegocioDefault() ;
}