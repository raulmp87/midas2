package mx.com.afirme.midas.catalogos.ramo;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.List;
import java.util.ArrayList;


import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.producto.configuracion.ramo.RamoProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo.RamoTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.ramo.RamoSeccionDTO;

@Entity(name = "RamoRCSDTO")
@Table(name = "CNSF_RAMO", schema = "MIDAS")
public class RamoRCSDTO extends CacheableDTO {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal id_ramo;
	private BigDecimal modulo;
	private String descripcion;
	private String clave;
	
	// Constructors

	/** default constructor */
	public RamoRCSDTO() {
	}

	/** minimal constructor */
	public RamoRCSDTO(BigDecimal idRamo, String descripcion) {
		this.id_ramo = idRamo;
		this.descripcion = descripcion;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTCRAMO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCRAMO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCRAMO_SEQ_GENERADOR")
	@Column(name = "ID_RAMO", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getId_ramo() {
		return id_ramo;
	}

	public void setId_ramo(BigDecimal id_ramo) {
		this.id_ramo = id_ramo;
	}

	@Column(name = "DESCRIPCION", nullable = false, length = 200)
	public String getDescripcion() {
		return this.descripcion;
	}
	

	public void setDescripcion(String descripcion) {
		this.
		descripcion = descripcion;
	}
	
	@Column(name = "CLAVE", nullable = false, length = 1)
	public String getClaveNegocio() {
		return clave;
	}
	public void setClaveNegocio(String claveNegocio) {
		this.clave = claveNegocio;
	}

	@Override
	public String getDescription() {
		return this.descripcion;
	}

	@Override
	public Object getId() {
		return this.id_ramo;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof RamoRCSDTO) {
			RamoRCSDTO ramoDTO = (RamoRCSDTO) object;
			equal = ramoDTO.getId_ramo().equals(this.id_ramo);
		} // End of if
		return equal;
	}

	@Column (name = "MODULO", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getModulo() {
		return modulo;
	}

	public void setModulo(BigDecimal modulo) {
		this.modulo = modulo;
	}

	
}