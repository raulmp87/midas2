package mx.com.afirme.midas.catalogos.ramo;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.List;
import java.util.ArrayList;


import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.producto.configuracion.ramo.RamoProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo.RamoTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.ramo.RamoSeccionDTO;

@Entity(name = "RamoDTO")
@Table(name = "TCRAMO", schema = "MIDAS")
public class RamoDTO extends CacheableDTO {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idTcRamo;
	private BigDecimal codigo;
	private String descripcion;
	private String claveNegocio;
	private List<SubRamoDTO> tcSubRamos = new ArrayList<SubRamoDTO>();
	private List<RamoSeccionDTO> ramoSeccionList = new ArrayList<RamoSeccionDTO>();
	private List<RamoTipoPolizaDTO> ramoTipoPolizaList = new ArrayList<RamoTipoPolizaDTO>();
	private List<RamoProductoDTO> ramoProductoList = new ArrayList<RamoProductoDTO>();
	// Constructors

	/** default constructor */
	public RamoDTO() {
	}

	/** minimal constructor */
	public RamoDTO(BigDecimal idTcRamo, String descripcion) {
		this.idTcRamo = idTcRamo;
		this.descripcion = descripcion;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTCRAMO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCRAMO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCRAMO_SEQ_GENERADOR")
	@Column(name = "IDTCRAMO", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcRamo() {
		return this.idTcRamo;
	}

	public void setIdTcRamo(BigDecimal idTcRamo) {
		this.idTcRamo = idTcRamo;
	}

	@Column(name = "DESCRIPCIONRAMO", nullable = false, length = 200)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.
		descripcion = descripcion;
	}
	
	@Column(name = "CLAVENEGOCIO", nullable = false, length = 1)
	public String getClaveNegocio() {
		return claveNegocio;
	}
	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "ramoDTO")
	public List<SubRamoDTO> getTcSubRamos() {
		return this.tcSubRamos;
	}

	public void setTcSubRamos(List<SubRamoDTO> tcSubRamos) {
		this.tcSubRamos = tcSubRamos;
	}

	@Override
	public String getDescription() {
		return this.descripcion;
	}

	@Override
	public Object getId() {
		return this.idTcRamo;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof RamoDTO) {
			RamoDTO ramoDTO = (RamoDTO) object;
			equal = ramoDTO.getIdTcRamo().equals(this.idTcRamo);
		} // End of if
		return equal;
	}

	@Column (name = "CODIGORAMO", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigo() {
		return codigo;
	}

	public void setCodigo(BigDecimal codigo) {
		this.codigo = codigo;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "ramoDTO")
	public List<RamoSeccionDTO> getRamoSeccionList() {
		return ramoSeccionList;
	}

	public void setRamoSeccionList(List<RamoSeccionDTO> ramoSeccionList) {
		this.ramoSeccionList = ramoSeccionList;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "ramoDTO")
	public List<RamoTipoPolizaDTO> getRamoTipoPolizaList() {
		return ramoTipoPolizaList;
	}

	public void setRamoTipoPolizaList(List<RamoTipoPolizaDTO> ramoTipoPolizaList) {
		this.ramoTipoPolizaList = ramoTipoPolizaList;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "ramoDTO")
	public List<RamoProductoDTO> getRamoProductoList() {
		return ramoProductoList;
	}

	public void setRamoProductoList(List<RamoProductoDTO> ramoProductoList) {
		this.ramoProductoList = ramoProductoList;
	}
}