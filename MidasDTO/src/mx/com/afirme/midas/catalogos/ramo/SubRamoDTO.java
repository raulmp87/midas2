package mx.com.afirme.midas.catalogos.ramo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.reaseguro.ReaseguroCotizacionDTO;
import mx.com.afirme.midas.cotizacion.reaseguro.inciso.ReaseguroIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.reaseguro.subinciso.ReaseguroSubIncisoCotizacionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

/**
 * SubRamoDTO entity. @author MyEclipse Persistence Tools
 */

@Entity(name = "SubRamoDTO")
@Table(name = "TCSUBRAMO", schema = "MIDAS")
@Cache(
  type=CacheType.SOFT,
  size=1000,
  expiry=36000000
)
public class SubRamoDTO extends CacheableDTO implements Entidad {

	private static final long serialVersionUID = 1L;
	private BigDecimal idTcSubRamo;
	private RamoDTO ramoDTO;
	private BigDecimal codigoSubRamo;
	private String descripcionSubRamo;
	private Double porcentajeMaximoComisionRO;
	private Double porcentajeMaximoComisionRCI;
	private Double porcentajeMaximoComisionPRR;
	private BigDecimal idCuentaContable;
	private BigDecimal idSubCuentaContable;
	private List<RiesgoDTO> riesgoDTOs = new ArrayList<RiesgoDTO>();
	private List<ReaseguroCotizacionDTO> reaseguroCotizacionDTOs = new ArrayList<ReaseguroCotizacionDTO>();
	private List<ComisionCotizacionDTO> comisionCotizacionDTOs = new ArrayList<ComisionCotizacionDTO>();
	private List<CoberturaDTO> coberturaDTOs = new ArrayList<CoberturaDTO>();
	private List<ReaseguroSubIncisoCotizacionDTO> reaseguroSubIncisoCotizacionDTOs = new ArrayList<ReaseguroSubIncisoCotizacionDTO>();
	private List<LineaDTO> lineaDTOs = new ArrayList<LineaDTO>();
	private List<ReaseguroIncisoCotizacionDTO> reaseguroIncisoCotizacionDTOs = new ArrayList<ReaseguroIncisoCotizacionDTO>();
	private String codigoSubRamoMovsManuales;

	//Constantes
	public static final BigDecimal SUBRAMO_INCENDIO = new BigDecimal(1d);
	public static final BigDecimal SUBRAMO_OBRA_CIVIL_EN_CONSTRUCCION = new BigDecimal(16d);
	public static final BigDecimal SUBRAMO_EQUIPO_CONTRATISTA_Y_MAQUINARIA_PESADA = new BigDecimal(14d);
	public static final BigDecimal SUBRAMO_EQUIPO_ELECTRONICO = new BigDecimal(13d);
	public static final BigDecimal SUBRAMO_MONTAJE_MAQUINA = new BigDecimal(17d);
	public static final BigDecimal SUBRAMO_ROTURA_MAQUINARIA = new BigDecimal(12d);
	public static final BigDecimal SUBRAMO_RESPONSABILIDAD_CIVIL_GENERAL = new BigDecimal(31d);
	public static final BigDecimal SUBRAMO_HIDRO=new BigDecimal(41d);
	public static final BigDecimal SUBRAMO_TERREMOTO=new BigDecimal(37d);
	
	// Constructors

	/** default constructor */
	public SubRamoDTO() {
		if (this.ramoDTO == null) {
			this.ramoDTO = new RamoDTO();
		}
	}

	/** minimal constructor */
	public SubRamoDTO(BigDecimal idTcSubRamo, RamoDTO ramoDTO) {
		this.idTcSubRamo = idTcSubRamo;
		this.ramoDTO = ramoDTO;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTCSUBRAMO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCSUBRAMO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCSUBRAMO_SEQ_GENERADOR")
	@Column(name = "IDTCSUBRAMO", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcSubRamo() {
		return this.idTcSubRamo;
	}

	public void setIdTcSubRamo(BigDecimal idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCRAMO", nullable = false)
	public RamoDTO getRamoDTO() {
		return this.ramoDTO;
	}

	public void setRamoDTO(RamoDTO ramoDTO) {
		this.ramoDTO = ramoDTO;
	}

	@Column(name = "CODIGOSUBRAMO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoSubRamo() {
		return this.codigoSubRamo;
	}

	public void setCodigoSubRamo(BigDecimal codigoSubRamo) {
		this.codigoSubRamo = codigoSubRamo;
	}

	@Column(name = "DESCRIPCIONSUBRAMO", nullable = false, length = 200)
	public String getDescripcionSubRamo() {
		return this.descripcionSubRamo;
	}

	public void setDescripcionSubRamo(String descripcionSubRamo) {
		this.descripcionSubRamo = descripcionSubRamo;
	}

	@Column(name = "PORCENTAJEMAXCOMISIONRO", nullable = false, precision = 8, scale = 4)
	public Double getPorcentajeMaximoComisionRO() {
		return this.porcentajeMaximoComisionRO;
	}

	public void setPorcentajeMaximoComisionRO(Double porcentajeMaximoComisionRO) {
		this.porcentajeMaximoComisionRO = porcentajeMaximoComisionRO;
	}

	@Column(name = "PORCENTAJEMAXCOMISIONRCI", nullable = false, precision = 8, scale = 4)
	public Double getPorcentajeMaximoComisionRCI() {
		return this.porcentajeMaximoComisionRCI;
	}

	public void setPorcentajeMaximoComisionRCI(Double porcentajeMaximoComisionRCI) {
		this.porcentajeMaximoComisionRCI = porcentajeMaximoComisionRCI;
	}

	@Column(name = "PORCENTAJEMAXCOMISIONPRR", nullable = false, precision = 8, scale = 4)
	public Double getPorcentajeMaximoComisionPRR() {
		return this.porcentajeMaximoComisionPRR;
	}

	public void setPorcentajeMaximoComisionPRR(Double porcentajeMaximoComisionPRR) {
		this.porcentajeMaximoComisionPRR = porcentajeMaximoComisionPRR;
	}

	@Column(name = "IDCUENTACONTABLE", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdCuentaContable() {
		return this.idCuentaContable;
	}

	public void setIdCuentaContable(BigDecimal idCuentaContable) {
		this.idCuentaContable = idCuentaContable;
	}

	@Column(name = "IDSUBCUENTACONTABLE", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdSubCuentaContable() {
		return this.idSubCuentaContable;
	}

	public void setIdSubCuentaContable(BigDecimal idSubCuentaContable) {
		this.idSubCuentaContable = idSubCuentaContable;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "subRamoDTO")
	public List<RiesgoDTO> getRiesgoDTOs() {
		return this.riesgoDTOs;
	}

	public void setRiesgoDTOs(List<RiesgoDTO> riesgoDTOs) {
		this.riesgoDTOs = riesgoDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "subRamoDTO")
	public List<ReaseguroCotizacionDTO> getReaseguroCotizacionDTOs() {
		return this.reaseguroCotizacionDTOs;
	}

	public void setReaseguroCotizacionDTOs(List<ReaseguroCotizacionDTO> reaseguroCotizacionDTOs) {
		this.reaseguroCotizacionDTOs = reaseguroCotizacionDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "subRamoDTO")
	public List<ComisionCotizacionDTO> getComisionCotizacionDTOs() {
		return this.comisionCotizacionDTOs;
	}

	public void setComisionCotizacionDTOs(
			List<ComisionCotizacionDTO> comisionCotizacionDTOs) {
		this.comisionCotizacionDTOs = comisionCotizacionDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "subRamoDTO")
	public List<CoberturaDTO> getCoberturaDTOs() {
		return this.coberturaDTOs;
	}

	public void setCoberturaDTOs(List<CoberturaDTO> coberturaDTOs) {
		this.coberturaDTOs = coberturaDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "subRamoDTO")
	public List<ReaseguroSubIncisoCotizacionDTO> getReaseguroSubIncisoCotizacionDTOs() {
		return this.reaseguroSubIncisoCotizacionDTOs;
	}

	public void setReaseguroSubIncisoCotizacionDTOs(
			List<ReaseguroSubIncisoCotizacionDTO> reaseguroSubIncisoCotizacionDTOs) {
		this.reaseguroSubIncisoCotizacionDTOs = reaseguroSubIncisoCotizacionDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "subRamo")
	public List<LineaDTO> getLineaDTOs() {
		return this.lineaDTOs;
	}

	public void setLineaDTOs(List<LineaDTO> lineaDTOs) {
		this.lineaDTOs = lineaDTOs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "subRamoDTO")
	public List<ReaseguroIncisoCotizacionDTO> getReaseguroIncisoCotizacionDTOs() {
		return this.reaseguroIncisoCotizacionDTOs;
	}

	public void setReaseguroIncisoCotizacionDTOs(
			List<ReaseguroIncisoCotizacionDTO> reaseguroIncisoCotizacionDTOs) {
		this.reaseguroIncisoCotizacionDTOs = reaseguroIncisoCotizacionDTOs;
	}

	@Override
	public String getDescription() {
		return this.descripcionSubRamo;
	}

	@Override
	public Object getId() {
		return this.idTcSubRamo;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof SubRamoDTO) {
			SubRamoDTO subRamoDTO = (SubRamoDTO) object;
			equal = subRamoDTO.getIdTcSubRamo().equals(this.idTcSubRamo);
		} // End of if
		return equal;
	}
	
	@Override
	public int hashCode(){
		return this.idTcSubRamo.hashCode();
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.idTcSubRamo;
	}

	@Override
	public String getValue() {
		return this.descripcionSubRamo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getBusinessKey() {
		return this.idTcSubRamo;
	}
	
	@Transient
	  public String getCodigoSubRamoMovsManuales() {
	    this.codigoSubRamoMovsManuales = this.codigoSubRamo.toString().substring(this.ramoDTO.getCodigo().toString().length());
	    return this.codigoSubRamoMovsManuales;
	  }

	 public void setCodigoSubRamoMovsManuales(String codigoSubRamoMovsManuales) {
	    this.codigoSubRamoMovsManuales = codigoSubRamoMovsManuales;
	 }
}