package mx.com.afirme.midas.catalogos.cuentabanco;
// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;


/**
 * Remote interface for CuentaBancoFacade.
 * @author MyEclipse Persistence Tools
 */


public interface CuentaBancoFacadeRemote extends MidasInterfaceBase<CuentaBancoDTO> {
		/**
	 Perform an initial save of a previously unsaved CuentaBancoDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CuentaBancoDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CuentaBancoDTO entity);
    /**
	 Delete a persistent CuentaBancoDTO entity.
	  @param entity CuentaBancoDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CuentaBancoDTO entity);
   /**
	 Persist a previously saved CuentaBancoDTO entity and return it or a copy of it to the sender. 
	 A copy of the CuentaBancoDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CuentaBancoDTO entity to update
	 @return CuentaBancoDTO the persisted CuentaBancoDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CuentaBancoDTO update(CuentaBancoDTO entity);

	 /**
	 * Find all CuentaBancoDTO entities with a specific property value.  
	 
	  @param propertyName the name of the CuentaBancoDTO property to query
	  @param value the property value to match
	  	  @return List<CuentaBancoDTO> found by query
	 */
	public List<CuentaBancoDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all CuentaBancoDTO entities.
	  	  @return List<CuentaBancoDTO> all CuentaBancoDTO entities
	 */
	public List<CuentaBancoDTO> findAll(
		);
	
	/**
	 * Find filtered CuentaBancoDTO entities.
	  	  @return List<CuentaBancoDTO> filtered CuentaBancoDTO entities
	 */
	public List<CuentaBancoDTO> listarFiltrado(CuentaBancoDTO cuentaBancoDTO);
}