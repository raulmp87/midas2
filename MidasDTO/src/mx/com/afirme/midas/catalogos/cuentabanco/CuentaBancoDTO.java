package mx.com.afirme.midas.catalogos.cuentabanco;
// default package

//import ParticipacionDTO;
import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDTO;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;
//import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table(name="TCCUENTABANCO",schema="MIDAS")

public class CuentaBancoDTO extends CacheableDTO implements java.io.Serializable {


    // Fields    

	private static final long serialVersionUID = 1L;
	private BigDecimal idtccuentabanco;
     private ReaseguradorCorredorDTO reaseguradorCorredor;
     private String nombrebanco;
     private String numerocuenta;
     private String numeroaba;
     private String numeroswift;
     private String beneficiariocuenta;
     private String numeroclabe;
     private String direccionbanco;
     private BigDecimal estatus;
     private String pais;
     private String ffc;
     private String cuentaffc;
     private BigDecimal idTcMoneda;
     private Set<ParticipacionDTO> participacionsForIdtccuentabancopesos = new HashSet<ParticipacionDTO>(0);
     private Set<ParticipacionDTO> participacionsForIdtccuentabancodolares = new HashSet<ParticipacionDTO>(0);


    // Constructors

    /** default constructor */
    public CuentaBancoDTO() {
    }	
   
    // Property accessors
    @Id 
    @SequenceGenerator(name = "IDTCCUENTABANCO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCCUENTABANCO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCCUENTABANCO_SEQ_GENERADOR")
    @Column(name="IDTCCUENTABANCO", unique=true, nullable=false, precision=22, scale=0)

    public BigDecimal getIdtccuentabanco() {
        return this.idtccuentabanco;
    }
    
    public void setIdtccuentabanco(BigDecimal idtccuentabanco) {
        this.idtccuentabanco = idtccuentabanco;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTCREASEGURADORCORREDOR", nullable=false)

    public ReaseguradorCorredorDTO getReaseguradorCorredor() {
        return this.reaseguradorCorredor;
    }
    
    public void setReaseguradorCorredor(ReaseguradorCorredorDTO reaseguradorCorredor) {
        this.reaseguradorCorredor = reaseguradorCorredor;
    }
    
    @Column(name="NOMBREBANCO", nullable=false, length=50)

    public String getNombrebanco() {
        return this.nombrebanco;
    }
    
    public void setNombrebanco(String nombrebanco) {
        this.nombrebanco = nombrebanco;
    }
    
    @Column(name="NUMEROCUENTA", length = 100)

    public String getNumerocuenta() {
        return this.numerocuenta;
    }
    
    public void setNumerocuenta(String numerocuenta) {
        this.numerocuenta = numerocuenta;
    }
    
    @Column(name="NUMEROABA", length = 100)

    public String getNumeroaba() {
        return this.numeroaba;
    }
    
    public void setNumeroaba(String numeroaba) {
        this.numeroaba = numeroaba;
    }
    
    @Column(name="NUMEROSWIFT", length = 100)

    public String getNumeroswift() {
        return this.numeroswift;
    }
    
    public void setNumeroswift(String numeroswift) {
        this.numeroswift = numeroswift;
    }
    
    @Column(name="BENEFICIARIOCUENTA", length=50)

    public String getBeneficiariocuenta() {
        return this.beneficiariocuenta;
    }
    
    public void setBeneficiariocuenta(String beneficiariocuenta) {
        this.beneficiariocuenta = beneficiariocuenta;
    }
    
    @Column(name="NUMEROCLABE", length = 100)

    public String getNumeroclabe() {
        return this.numeroclabe;
    }
    
    public void setNumeroclabe(String numeroclabe) {
        this.numeroclabe = numeroclabe;
    }
    
    @Column(name="DIRECCIONBANCO", length=250)

    public String getDireccionbanco() {
        return this.direccionbanco;
    }
    
    public void setDireccionbanco(String direccionbanco) {
        this.direccionbanco = direccionbanco;
    }
    
    @Column(name="ESTATUS", precision=22, scale=0)

    public BigDecimal getEstatus() {
        return this.estatus;
    }
    
    public void setEstatus(BigDecimal estatus) {
        this.estatus = estatus;
    }
    
    @Column(name="PAIS", nullable=false, length=50)

    public String getPais() {
        return this.pais;
    }
    
    public void setPais(String pais) {
        this.pais = pais;
    }
    
    @Column(name="FFC", length=50)

    public String getFfc() {
        return this.ffc;
    }
    
    public void setFfc(String ffc) {
        this.ffc = ffc;
    }
    
    @Column(name="CUENTAFFC", length=50)

    public String getCuentaffc() {
        return this.cuentaffc;
    }
    
    public void setCuentaffc(String cuentaffc) {
        this.cuentaffc = cuentaffc;
    }
    
    @Column(name="IDTCMONEDA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdTcMoneda() {
        return this.idTcMoneda;
    }
    
    public void setIdTcMoneda(BigDecimal idTcMoneda) {
        this.idTcMoneda = idTcMoneda;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="cuentaBancoPesos")

    public Set<ParticipacionDTO> getParticipacionsForIdtccuentabancopesos() {
        return this.participacionsForIdtccuentabancopesos;
    }
    
    public void setParticipacionsForIdtccuentabancopesos(Set<ParticipacionDTO> participacionsForIdtccuentabancopesos) {
        this.participacionsForIdtccuentabancopesos = participacionsForIdtccuentabancopesos;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="cuentaBancoDolares")

    public Set<ParticipacionDTO> getParticipacionsForIdtccuentabancodolares() {
        return this.participacionsForIdtccuentabancodolares;
    }
    
    public void setParticipacionsForIdtccuentabancodolares(Set<ParticipacionDTO> participacionsForIdtccuentabancodolares) {
        this.participacionsForIdtccuentabancodolares = participacionsForIdtccuentabancodolares;
    }
    
	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof CuentaBancoDTO) {
			CuentaBancoDTO cuentaBancoDTO = (CuentaBancoDTO) object;
			equal = cuentaBancoDTO.getIdtccuentabanco().equals(this.getIdtccuentabanco());
		} // End of if
		return equal;
	}
	
	@Override
	public String getDescription() {
		return this.getNumerocuenta().toString();
	}
	
	@Override
	public Object getId() {
		return this.getIdtccuentabanco();
	}
	
	@Transient
	public String getDescripcionMoneda(){
		String descripcion = null;
		int idMoneda = 0;
		if(getIdTcMoneda() != null){
			idMoneda = getIdTcMoneda().intValue();
		}
		switch(idMoneda){
		case 840: descripcion = "Dolares"; break;
		case 484: descripcion = "Moneda Nacional";
		}
		
		return descripcion;
	}
}