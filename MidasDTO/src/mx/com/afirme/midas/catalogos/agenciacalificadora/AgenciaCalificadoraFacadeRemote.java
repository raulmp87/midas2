package mx.com.afirme.midas.catalogos.agenciacalificadora;

import java.util.List;

import mx.com.afirme.midas.base.MidasInterfaceBase;

public interface AgenciaCalificadoraFacadeRemote extends MidasInterfaceBase<AgenciaCalificadoraDTO> {
	
	public List<AgenciaCalificadoraDTO> findAll();
	
	public AgenciaCalificadoraDTO findById(String id);
	
	public List<AgenciaCalificadoraDTO> findByProperty(String property, String value);

}