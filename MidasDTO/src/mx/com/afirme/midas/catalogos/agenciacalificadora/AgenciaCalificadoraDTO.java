package mx.com.afirme.midas.catalogos.agenciacalificadora;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name = "AgenciaCalificadoraDTO")
@Table(name="TCAGENCIACALIFICADORA"
    ,schema="MIDAS"
)

public class AgenciaCalificadoraDTO extends CacheableDTO implements java.io.Serializable, Entidad, Comparable<AgenciaCalificadoraDTO> {
   
	 private static final long serialVersionUID = 1L;
	 private BigDecimal idagencia;
	 private String nombreAgencia;


    public AgenciaCalificadoraDTO() {
    }

    @Id 
    @Column(name="IDAGENCIA", unique=true, nullable=false, precision=22, scale=0)
    public BigDecimal getIdagencia() {
		return idagencia;
	}

	public void setIdagencia(BigDecimal idagencia) {
		this.idagencia = idagencia;
	}

	@Column(name="NOMBREAGENCIA", nullable=false, length=100)
	public String getNombreAgencia() {
		return nombreAgencia;
	}

	public void setNombreAgencia(String nombreAgencia) {
		this.nombreAgencia = nombreAgencia;
	}

	@Override
	public int compareTo(AgenciaCalificadoraDTO arg0) {
		return this.nombreAgencia.compareTo(arg0.nombreAgencia);
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getKey() {
		return this.getIdagencia()+"";
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}

	@Override
	public Object getId() {
		return this.getIdagencia();
	}

	@Override
	public String getDescription() {
		return this.nombreAgencia;
	}

	@Override
	public boolean equals(Object other) {
		boolean equal = other == this;
		if (!equal && other instanceof AgenciaCalificadoraDTO) {
			AgenciaCalificadoraDTO pais = (AgenciaCalificadoraDTO) other;
			equal = pais.getIdagencia().equals(this.idagencia);
		}
		return equal;
	}

	
}