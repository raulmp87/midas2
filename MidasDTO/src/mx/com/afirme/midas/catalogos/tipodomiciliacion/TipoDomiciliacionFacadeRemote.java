package mx.com.afirme.midas.catalogos.tipodomiciliacion;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

public interface TipoDomiciliacionFacadeRemote extends MidasInterfaceBase<TipoDomiciliacionDTO>{
	public TipoDomiciliacionDTO findById(Integer id);
	
	public TipoDomiciliacionDTO findByName(String valor);
}
