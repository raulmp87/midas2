package mx.com.afirme.midas.catalogos.tipodomiciliacion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;

@Entity(name="TipoDomiciliacionDTO")
@Table(name="VW_TIPODOMICILIACION",schema="MIDAS")
public class TipoDomiciliacionDTO extends CacheableDTO implements Comparable<TipoDomiciliacionDTO>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String	valor;
	
	@Id
	@Column(nullable=false,length=3,name="id")
	@Override
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="valor",length=25,nullable=false)
	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	@Override
	public String getDescription() {
		return valor;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal=(object==this);
		if(!equal && (object instanceof TipoDomiciliacionDTO)){
			TipoDomiciliacionDTO instance=(TipoDomiciliacionDTO)object;
			equal=instance.getId().equals(this.id);
		}
		return equal;
	}

	@Override
	public int compareTo(TipoDomiciliacionDTO o) {
		return this.id.compareTo(o.getId());
	}

}
