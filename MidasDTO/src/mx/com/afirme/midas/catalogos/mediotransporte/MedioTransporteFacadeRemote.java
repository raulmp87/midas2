package mx.com.afirme.midas.catalogos.mediotransporte;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for MedioTransporteFacade.
 * @author MyEclipse Persistence Tools
 */


public interface MedioTransporteFacadeRemote extends MidasInterfaceBase<MedioTransporteDTO> {
		/**
	 Perform an initial save of a previously unsaved MedioTransporteDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity MedioTransporteDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(MedioTransporteDTO entity);
    /**
	 Delete a persistent MedioTransporteDTO entity.
	  @param entity MedioTransporteDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(MedioTransporteDTO entity);
   /**
	 Persist a previously saved MedioTransporteDTO entity and return it or a copy of it to the sender. 
	 A copy of the MedioTransporteDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity MedioTransporteDTO entity to update
	 @return MedioTransporteDTO the persisted MedioTransporteDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public MedioTransporteDTO update(MedioTransporteDTO entity);

	 /**
	 * Find all MedioTransporteDTO entities with a specific property value.  
	 
	  @param propertyName the name of the MedioTransporteDTO property to query
	  @param value the property value to match
	  	  @return List<MedioTransporteDTO> found by query
	 */
	public List<MedioTransporteDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all MedioTransporteDTO entities.
	  	  @return List<MedioTransporteDTO> all MedioTransporteDTO entities
	 */
	public List<MedioTransporteDTO> findAll(
		);	
	
	public List<MedioTransporteDTO> listarFiltrado(MedioTransporteDTO medioTransporteDTO);
}