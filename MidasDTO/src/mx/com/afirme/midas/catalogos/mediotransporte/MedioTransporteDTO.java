package mx.com.afirme.midas.catalogos.mediotransporte;
// default package

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.base.CacheableDTO;


/**
 * MedioTransporteDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCMEDIOTRANSPORTE"
    ,schema="MIDAS"
, uniqueConstraints = @UniqueConstraint(columnNames="IDTCMEDIOTRANSPORTE")
)

public class MedioTransporteDTO extends CacheableDTO {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1874290867161168948L;
	private BigDecimal codigoMedioTransporte;
     private BigDecimal idMedioTransporte;
     private String descripcionMedioTransporte;


    // Constructors

    /** default constructor */
    public MedioTransporteDTO() {
    }
    @Id 
    @SequenceGenerator(name = "IDTCMEDIOTRANSPORTE_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCMEDIOTRANSPORTE_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCMEDIOTRANSPORTE_SEQ_GENERADOR")
    @Column(name="IDTCMEDIOTRANSPORTE", unique=true, nullable=false, precision=22, scale=0)
    public BigDecimal getIdMedioTransporte() {
		return idMedioTransporte;
	}

	public void setIdMedioTransporte(BigDecimal idMedioTransporte) {
		this.idMedioTransporte = idMedioTransporte;
	}
	
    @Column(name="CODIGOMEDIOTRANSPORTE", unique=true, nullable=false, precision=22, scale=0)
	public BigDecimal getCodigoMedioTransporte() {
		return codigoMedioTransporte;
	}
    
	public void setCodigoMedioTransporte(BigDecimal codigoMedioTransporte) {
		this.codigoMedioTransporte = codigoMedioTransporte;
	}

	@Column(name="DESCRIPCIONMEDIOTRANSPORTE", nullable=false, length=200)
	public String getDescripcionMedioTransporte() {
		return descripcionMedioTransporte;
	}


	public void setDescripcionMedioTransporte(String descripcionMedioTransporte) {
		this.descripcionMedioTransporte = descripcionMedioTransporte;
	}
	@Override
	public String getDescription() {
		return this.descripcionMedioTransporte;
	}
	@Override
	public Object getId() {
		return this.idMedioTransporte;
	}
	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof MedioTransporteDTO) {
			MedioTransporteDTO medioTransporteDTO = (MedioTransporteDTO) object;
			equal = medioTransporteDTO.getIdMedioTransporte().equals(
					this.idMedioTransporte);
		}
		return equal;
	}
}