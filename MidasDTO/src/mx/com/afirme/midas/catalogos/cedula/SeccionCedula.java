package mx.com.afirme.midas.catalogos.cedula;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;





import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name = "SeccionCedula")
@Table(name = "TOSECCION_TIPOCEDULA", schema = "MIDAS")
public class SeccionCedula implements java.io.Serializable, Entidad {

	private static final long serialVersionUID = 1L;
	private BigDecimal id;
	private BigDecimal idToSeccion;
	private BigDecimal idTipoCedula;
	
	@Id
	@Column(name = "ID", nullable = false, precision = 38, scale = 0)
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}
	
	@Column(name = "TOSECCION_ID", nullable = false, precision = 38, scale = 0)
	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}
	
	@Column(name = "TIPOCEDULA_ID", nullable = false, precision = 38, scale = 0)
	public BigDecimal getIdTipoCedula() {
		return idTipoCedula;
	}
	
	public void setIdTipoCedula(BigDecimal idTipoCedula) {
		this.idTipoCedula = idTipoCedula;
	}

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
		
}
