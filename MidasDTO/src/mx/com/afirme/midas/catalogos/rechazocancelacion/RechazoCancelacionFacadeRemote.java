package mx.com.afirme.midas.catalogos.rechazocancelacion;
// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for RechazoCancelacionFacade.
 * @author MyEclipse Persistence Tools
 */


public interface RechazoCancelacionFacadeRemote extends MidasInterfaceBase<RechazoCancelacionDTO>{
		/**
	 Perform an initial save of a previously unsaved RechazoCancelacion entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity RechazoCancelacion entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public RechazoCancelacionDTO save(RechazoCancelacionDTO entity);
    /**
	 Delete a persistent RechazoCancelacion entity.
	  @param entity RechazoCancelacion entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(RechazoCancelacionDTO entity);
   /**
	 Persist a previously saved RechazoCancelacion entity and return it or a copy of it to the sender. 
	 A copy of the RechazoCancelacion entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity RechazoCancelacion entity to update
	 @return RechazoCancelacion the persisted RechazoCancelacion entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public RechazoCancelacionDTO update(RechazoCancelacionDTO entity);

	 /**
	 * Find all RechazoCancelacion entities with a specific property value.  
	 
	  @param propertyName the name of the RechazoCancelacion property to query
	  @param value the property value to match
	  	  @return List<RechazoCancelacion> found by query
	 */
	public List<RechazoCancelacionDTO> findByProperty(String propertyName, Object value);
}