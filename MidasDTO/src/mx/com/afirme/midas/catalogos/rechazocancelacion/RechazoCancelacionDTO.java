package mx.com.afirme.midas.catalogos.rechazocancelacion;
// default package


import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;

/**
 * RechazoCancelacion entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCRECHAZOCANCELACION",schema="MIDAS")
public class RechazoCancelacionDTO  extends CacheableDTO {

	private static final long serialVersionUID = 3381347150766479173L;
	private BigDecimal idTcRechazoCancelacion;
    private Short claveTipoMotivo;
    private String descripcionMotivo;


    // Constructors

    /** default constructor */
    public RechazoCancelacionDTO() {
    }
    // Property accessors
    @Id 
    @SequenceGenerator(name = "IDTORECHAZOCANCELACION_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTCRECHAZOCANCELACION_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTORECHAZOCANCELACION_SEQ_GENERADOR")
    @Column(name="IDTCRECHAZOCANCELACION", nullable=false, precision=22, scale=0)
    public BigDecimal getIdTcRechazoCancelacion() {
        return this.idTcRechazoCancelacion;
    }
    
    public void setIdTcRechazoCancelacion(BigDecimal idTcRechazoCancelacion) {
        this.idTcRechazoCancelacion = idTcRechazoCancelacion;
    }
    
    @Column(name="CLAVETIPOMOTIVO", nullable=false, precision=4, scale=0)
    public Short getClaveTipoMotivo() {
        return this.claveTipoMotivo;
    }
    
    public void setClaveTipoMotivo(Short claveTipoMotivo) {
        this.claveTipoMotivo = claveTipoMotivo;
    }
    
    @Column(name="DESCRIPCIONMOTIVO", nullable=false, length=200)

    public String getDescripcionMotivo() {
        return this.descripcionMotivo;
    }
    
    public void setDescripcionMotivo(String descripcionMotivo) {
        this.descripcionMotivo = descripcionMotivo;
    }

	@Override
	public String getDescription() {
		return this.descripcionMotivo;
	}

	@Override
	public Object getId() {
		return this.idTcRechazoCancelacion;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof RechazoCancelacionDTO) {
			RechazoCancelacionDTO entity = (RechazoCancelacionDTO) object;
			equal = entity.getIdTcRechazoCancelacion().equals(this.idTcRechazoCancelacion);
		} // End of if
		return equal;
	}

}