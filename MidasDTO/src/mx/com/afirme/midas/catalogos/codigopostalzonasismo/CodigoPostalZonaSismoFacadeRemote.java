package mx.com.afirme.midas.catalogos.codigopostalzonasismo;
// default package

import java.util.List;

import javax.ejb.Remote;

/**
 * Remote interface for CodigoPostalZonaSismoFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface CodigoPostalZonaSismoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved CodigoPostalZonaSismoDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            CodigoPostalZonaSismoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(CodigoPostalZonaSismoDTO entity);

	/**
	 * Delete a persistent CodigoPostalZonaSismoDTO entity.
	 * 
	 * @param entity
	 *            CodigoPostalZonaSismoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(CodigoPostalZonaSismoDTO entity);

	/**
	 * Persist a previously saved CodigoPostalZonaSismoDTO entity and return it or
	 * a copy of it to the sender. A copy of the CodigoPostalZonaSismoDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            CodigoPostalZonaSismoDTO entity to update
	 * @return CodigoPostalZonaSismoDTO the persisted CodigoPostalZonaSismoDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public CodigoPostalZonaSismoDTO update(CodigoPostalZonaSismoDTO entity);

	public CodigoPostalZonaSismoDTO findById(CodigoPostalZonaSismoId id);

	/**
	 * Find all CodigoPostalZonaSismoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the CodigoPostalZonaSismoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<CodigoPostalZonaSismoDTO> found by query
	 */
	public List<CodigoPostalZonaSismoDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all CodigoPostalZonaSismoDTO entities.
	 * 
	 * @return List<CodigoPostalZonaSismoDTO> all CodigoPostalZonaSismoDTO entities
	 */
	public List<CodigoPostalZonaSismoDTO> findAll();
	
	public List<CodigoPostalZonaSismoDTO> listarFiltrado(CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO);
	
	public Long obtenerTotalFiltrado(CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO);
}