package mx.com.afirme.midas.catalogos.codigopostalzonasismo;

// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * CodigoPostalZonaSismoId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class CodigoPostalZonaSismoId implements java.io.Serializable {

	private static final long serialVersionUID = 6755103045345163676L;

	private String nombreColonia;
	private BigDecimal codigoPostal;

	// Constructors
	public CodigoPostalZonaSismoId() {
	}

	/** full constructor */
	public CodigoPostalZonaSismoId(String nombreColonia, BigDecimal codigoPostal) {
		this.nombreColonia = nombreColonia;
		this.codigoPostal = codigoPostal;
	}

	@Column(name = "NOMBRECOLONIA")
	public String getNombreColonia() {
		return nombreColonia;
	}

	public void setNombreColonia(String nombreColonia) {
		this.nombreColonia = nombreColonia;
	}

	@Column(name = "CODIGOPOSTAL", nullable = false, precision = 22, scale = 0)
	public BigDecimal getCodigoPostal() {
		return this.codigoPostal;
	}

	public void setCodigoPostal(BigDecimal codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof CodigoPostalZonaSismoId))
			return false;
		CodigoPostalZonaSismoId castOther = (CodigoPostalZonaSismoId) other;

		return ((this.getNombreColonia() == castOther.getNombreColonia()) || (this
				.getNombreColonia() != null
				&& castOther.getNombreColonia() != null && this
				.getNombreColonia().equals(castOther.getNombreColonia())))
				&& ((this.getCodigoPostal() == castOther.getCodigoPostal()) || (this
						.getCodigoPostal() != null
						&& castOther.getCodigoPostal() != null && this
						.getCodigoPostal().equals(castOther.getCodigoPostal())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getNombreColonia() == null ? 0 : this.getNombreColonia()
						.hashCode());
		result = 37
				* result
				+ (getCodigoPostal() == null ? 0 : this.getCodigoPostal()
						.hashCode());
		return result;
	}

}