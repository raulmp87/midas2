package mx.com.afirme.midas.catalogos.codigopostalzonasismo;

// default package

import java.math.BigDecimal;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.base.PaginadoDTO;
import mx.com.afirme.midas.catalogos.zonasismo.ZonaSismoDTO;

/**
 * CodigoPostalZonaSismoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name = "CodigoPostalZonaSismoDTO")
@Table(name = "TCCODIGOPOSTALZONASISMO", schema = "MIDAS")
public class CodigoPostalZonaSismoDTO extends PaginadoDTO {

	private static final long serialVersionUID = 9151004504003810362L;

	private CodigoPostalZonaSismoId id;
	private ZonaSismoDTO zonaSismoDTO;

	// Constructors
	public CodigoPostalZonaSismoDTO() {
		if (zonaSismoDTO == null)
			zonaSismoDTO = new ZonaSismoDTO();
		if (id==null)
	    		id = new CodigoPostalZonaSismoId();
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "nombreColonia", column = @Column(name = "NOMBRECOLONIA")),
			@AttributeOverride(name = "codigoPostal", column = @Column(name = "CODIGOPOSTAL", nullable = false, precision = 22, scale = 0)) })
	public CodigoPostalZonaSismoId getId() {
		return this.id;
	}

	public void setId(CodigoPostalZonaSismoId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCZONASISMO", nullable = false, insertable = true, updatable = true)
	public ZonaSismoDTO getZonaSismoDTO() {
		return this.zonaSismoDTO;
	}

	public void setZonaSismoDTO(ZonaSismoDTO zonaSismoDTO) {
		this.zonaSismoDTO = zonaSismoDTO;
	}

	public String getNombreColonia() {
		return id.getNombreColonia();
	}

	public BigDecimal getCodigoPostal() {
		return id.getCodigoPostal();
	}

	public String getCodigoZonaSismo() {
		return this.zonaSismoDTO.getCodigoZonaSismo();
	}

	public String getDescripcionZonaSismo() {
		return this.zonaSismoDTO.getDescripcionZonaSismo();
	}

	

	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof CodigoPostalZonaSismoDTO) {
			CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO = (CodigoPostalZonaSismoDTO) object;
			equal = codigoPostalZonaSismoDTO.getId().equals(this.id);
		} // End of if
		return equal;
	}
}