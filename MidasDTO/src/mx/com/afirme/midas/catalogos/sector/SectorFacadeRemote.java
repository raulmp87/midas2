package mx.com.afirme.midas.catalogos.sector;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

public interface SectorFacadeRemote extends MidasInterfaceBase<SectorDTO> {
	/**
	 * Busca un sector por su id
	 * @param idSector
	 * @return
	 */
	public SectorDTO findById(String idSector);
	
	/**
	 * Busca un sector por su descripcion
	 * @param nombreSector
	 * @return
	 */
	public SectorDTO findByName(String nombreSector);
	
	/**
	 * Obtiene la lista de todos los sectores.
	 */
	public List<SectorDTO> findAll();
}
