package mx.com.afirme.midas.catalogos.sector;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;
/**
 * Entidad para el catalogo de sector
 * @author vmhersil
 *
 */
@Entity(name="SectorDTO")
@Table(name="VW_SECTOR",schema="MIDAS")
public class SectorDTO  extends CacheableDTO implements Comparable<SectorDTO>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idSector;
	private String nombreSector;
	
	@Id
	@Column(name="SECTOR_ID",length=2)
	public String getIdSector() {
		return idSector;
	}

	public void setIdSector(String idSector) {
		this.idSector = idSector;
	}
	
	@Column(name="SECTOR_NAME",nullable=false,length=50)
	public String getNombreSector() {
		return nombreSector;
	}

	public void setNombreSector(String nombreSector) {
		this.nombreSector = nombreSector;
	}

	@Override
	public int compareTo(SectorDTO o) {
		return this.idSector.compareTo(o.getIdSector());
	}

	@Override
	public Object getId() {
		return this.idSector;
	}

	@Override
	public String getDescription() {
		return this.nombreSector;
	}

	@Override
	public boolean equals(Object object) {
		boolean equal=(object==this);
		if(!equal && (object instanceof SectorDTO)){
			SectorDTO sector=(SectorDTO)object;
			equal=sector.getIdSector().equals(this.idSector);
		}
		return equal;
	}

}
