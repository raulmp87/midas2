package mx.com.afirme.midas.catalogos.plenoreaseguro;
// default package

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBase;

/**
 * Remote interface for PlenoReaseguroDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface PlenoReaseguroFacadeRemote extends MidasInterfaceBase<PlenoReaseguroDTO> {
		/**
	 Perform an initial save of a previously unsaved PlenoReaseguroDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity PlenoReaseguroDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(PlenoReaseguroDTO entity);
    /**
	 Delete a persistent PlenoReaseguroDTO entity.
	  @param entity PlenoReaseguroDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(PlenoReaseguroDTO entity);
   /**
	 Persist a previously saved PlenoReaseguroDTO entity and return it or a copy of it to the sender. 
	 A copy of the PlenoReaseguroDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity PlenoReaseguroDTO entity to update
	 @return PlenoReaseguroDTO the persisted PlenoReaseguroDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public PlenoReaseguroDTO update(PlenoReaseguroDTO entity);
	
	 /**
	 * Find all PlenoReaseguroDTO entities with a specific property value.  
	 
	  @param propertyName the name of the PlenoReaseguroDTO property to query
	  @param value the property value to match
	  	  @return List<PlenoReaseguroDTO> found by query
	 */
	public List<PlenoReaseguroDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all PlenoReaseguroDTO entities.
	  	  @return List<PlenoReaseguroDTO> all PlenoReaseguroDTO entities
	 */
	public List<PlenoReaseguroDTO> findAll(
		);	
}