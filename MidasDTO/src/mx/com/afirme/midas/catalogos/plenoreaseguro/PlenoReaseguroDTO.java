package mx.com.afirme.midas.catalogos.plenoreaseguro;
// default package

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;


/**
 * PlenoReaseguroDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCPLENOREASEGURO",schema="MIDAS")

public class PlenoReaseguroDTO  extends CacheableDTO{

	private static final long serialVersionUID = 5400088056858099424L;
	private BigDecimal idTcPlenoReaseguro;
     private Double porcentajeMaximo;


    // Constructors

    /** default constructor */
    public PlenoReaseguroDTO() {
    }

    
    /** full constructor */
    public PlenoReaseguroDTO(BigDecimal idTcPlenoReaseguro, Double porcentajeMaximo) {
        this.idTcPlenoReaseguro = idTcPlenoReaseguro;
        this.porcentajeMaximo = porcentajeMaximo;
    }

   
    // Property accessors
    @Id 
    @Column(name="IDTCPLENOREASEGURO", nullable=false, precision=22, scale=0)
    public BigDecimal getIdTcPlenoReaseguro() {
        return this.idTcPlenoReaseguro;
    }
    
    public void setIdTcPlenoReaseguro(BigDecimal idTcPlenoReaseguro) {
        this.idTcPlenoReaseguro = idTcPlenoReaseguro;
    }
    
    @Column(name="PORCENTAJEMAXIMO", nullable=false, precision=8, scale=4)
    public Double getPorcentajeMaximo() {
        return this.porcentajeMaximo;
    }
    
    public void setPorcentajeMaximo(Double porcentajeMaximo) {
        this.porcentajeMaximo = porcentajeMaximo;
    }


	@Override
	public String getDescription() {
		return this.porcentajeMaximo.toString();
	}


	@Override
	public Object getId() {
		return this.idTcPlenoReaseguro;
	}


	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if(object == null)
			return false;
		if (!equal && object instanceof PlenoReaseguroDTO) {
			PlenoReaseguroDTO plenoReaseguroDTO = (PlenoReaseguroDTO) object;
			equal = plenoReaseguroDTO.getIdTcPlenoReaseguro().equals(this.idTcPlenoReaseguro);
		}
		return equal;
	}
   

}