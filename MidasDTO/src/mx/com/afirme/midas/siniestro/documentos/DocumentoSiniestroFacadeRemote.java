package mx.com.afirme.midas.siniestro.documentos;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;
/**
 * Remote interface for DocumentoSiniestroDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface DocumentoSiniestroFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved DocumentoSiniestroDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DocumentoSiniestroDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DocumentoSiniestroDTO entity);
    /**
	 Delete a persistent DocumentoSiniestroDTO entity.
	  @param entity DocumentoSiniestroDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DocumentoSiniestroDTO entity);
   /**
	 Persist a previously saved DocumentoSiniestroDTO entity and return it or a copy of it to the sender. 
	 A copy of the DocumentoSiniestroDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DocumentoSiniestroDTO entity to update
	 @return DocumentoSiniestroDTO the persisted DocumentoSiniestroDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public DocumentoSiniestroDTO update(DocumentoSiniestroDTO entity);
	public DocumentoSiniestroDTO findById( BigDecimal id);
	 /**
	 * Find all DocumentoSiniestroDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DocumentoSiniestroDTO property to query
	  @param value the property value to match
	  	  @return List<DocumentoSiniestroDTO> found by query
	 */
	public List<DocumentoSiniestroDTO> findByProperty(String propertyName, Object value
		);

	/**
	 * Find all DocumentoSiniestroDTO entities.
	  	  @return List<DocumentoSiniestroDTO> all DocumentoSiniestroDTO entities
	 */
	public List<DocumentoSiniestroDTO> findAll(
		);
	
	public DocumentoSiniestroDTO obtenDocumentoSiniestro(BigDecimal idReporteSiniestro, BigDecimal idTipoDocumentoSiniestro);
}