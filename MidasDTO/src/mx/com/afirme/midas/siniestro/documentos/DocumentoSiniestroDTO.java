package mx.com.afirme.midas.siniestro.documentos;

import java.math.BigDecimal;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.tipodocumentosiniestro.TipoDocumentoSiniestroDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;


/**
 * DocumentoSiniestroDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TODOCUMENTOSINIESTRO"
    ,schema="MIDAS"
)

public class DocumentoSiniestroDTO  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

    // Fields    

     private BigDecimal idToDocumentoSiniestro;
     private TipoDocumentoSiniestroDTO tipoDocumentoSiniestroDTO;
     private ReporteSiniestroDTO reporteSiniestroDTO;
     private String url;
     private String nombreDocumento;
     private Boolean requiereAprobacion;
     private Boolean aprobado;
     private Timestamp fechaCreacion;
     private BigDecimal idTcUsuarioCreacion;
     private Timestamp fechaModificacion;
     private BigDecimal idTcUsuarioModificacion;
     private BigDecimal idToControArchivo;


    // Constructors

    /** default constructor */
    public DocumentoSiniestroDTO() {
    }

    // Property accessors
    @Id 
    @SequenceGenerator(name = "IDTODOCUMENTOSINIESTRO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTODOCUMENTOSINIESTRO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTODOCUMENTOSINIESTRO_SEQ_GENERADOR")	
    @Column(name="IDTODOCUMENTOSINIESTRO", unique=true, nullable=false, precision=22, scale=0)

    public BigDecimal getIdToDocumentoSiniestro() {
        return this.idToDocumentoSiniestro;
    }
    
    public void setIdToDocumentoSiniestro(BigDecimal idToDocumentoSiniestro) {
        this.idToDocumentoSiniestro = idToDocumentoSiniestro;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTCTIPODOCUMENTO", nullable=false)

    public TipoDocumentoSiniestroDTO getTipoDocumentoSiniestroDTO() {
        return this.tipoDocumentoSiniestroDTO;
    }
    
    public void setTipoDocumentoSiniestroDTO(TipoDocumentoSiniestroDTO tipoDocumentoSiniestroDTO) {
        this.tipoDocumentoSiniestroDTO = tipoDocumentoSiniestroDTO;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTOREPORTESINIESTRO", nullable=false)

    public ReporteSiniestroDTO getReporteSiniestroDTO() {
        return this.reporteSiniestroDTO;
    }
    
    public void setReporteSiniestroDTO(ReporteSiniestroDTO reporteSiniestroDTO) {
        this.reporteSiniestroDTO = reporteSiniestroDTO;
    }
    
    @Column(name="URL", length=240)

    public String getUrl() {
        return this.url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }
    
    @Column(name="NOMBREDOCUMENTO", nullable=false, length=50)

    public String getNombreDocumento() {
        return this.nombreDocumento;
    }
    
    public void setNombreDocumento(String nombreDocumento) {
        this.nombreDocumento = nombreDocumento;
    }
    
    @Column(name="REQUIEREAPROBACION", nullable=false, precision=1, scale=0)

    public Boolean getRequiereAprobacion() {
        return this.requiereAprobacion;
    }
    
    public void setRequiereAprobacion(Boolean requiereAprobacion) {
        this.requiereAprobacion = requiereAprobacion;
    }
    
    @Column(name="APROBADO", precision=1, scale=0)

    public Boolean getAprobado() {
        return this.aprobado;
    }
    
    public void setAprobado(Boolean aprobado) {
        this.aprobado = aprobado;
    }
    
    @Column(name="FECHACREACION", nullable=false, length=7)

    public Timestamp getFechaCreacion() {
        return this.fechaCreacion;
    }
    
    public void setFechaCreacion(Timestamp fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    
    @Column(name="IDTCUSUARIOCREACION", nullable=false, precision=22, scale=0)

    public BigDecimal getIdTcUsuarioCreacion() {
        return this.idTcUsuarioCreacion;
    }
    
    public void setIdTcUsuarioCreacion(BigDecimal idTcUsuarioCreacion) {
        this.idTcUsuarioCreacion = idTcUsuarioCreacion;
    }
    
    @Column(name="FECHAMODIFICACION", length=7)

    public Timestamp getFechaModificacion() {
        return this.fechaModificacion;
    }
    
    public void setFechaModificacion(Timestamp fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
    
    @Column(name="IDTCUSUARIOMODIFICACION", precision=22, scale=0)

    public BigDecimal getIdTcUsuarioModificacion() {
        return this.idTcUsuarioModificacion;
    }
    
    public void setIdTcUsuarioModificacion(BigDecimal idTcUsuarioModificacion) {
        this.idTcUsuarioModificacion = idTcUsuarioModificacion;
    }
    
    @Column(name="IDTOCONTROARCHIVO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToControArchivo() {
        return this.idToControArchivo;
    }
    
    public void setIdToControArchivo(BigDecimal idToControArchivo) {
        this.idToControArchivo = idToControArchivo;
    }
   
}