package mx.com.afirme.midas.siniestro.servicios;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.interfaz.asientocontable.AsientoContableDTO;
import mx.com.afirme.midas.interfaz.solicitudcheque.SolicitudChequeDTO;


public interface SinietroServiciosRemote {

	public List<AsientoContableDTO> contabilizaMovimientos(String idObjetoContable, String claveTransaccionContable,String nombreUsuario) throws Exception;;
	
	public BigDecimal solicitaCheque (SolicitudChequeDTO solicitudCheque, String nombreUsuario) throws Exception;;
	
	public SolicitudChequeDTO consultaEstatusSolicitudCheque (BigDecimal idPago, String nombreUsuario) throws Exception;;
	
	public String cancelaSolicitudCheque (BigDecimal idPago, String nombreUsuario) throws Exception;;
}
