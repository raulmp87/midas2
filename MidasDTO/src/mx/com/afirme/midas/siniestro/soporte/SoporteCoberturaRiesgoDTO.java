package mx.com.afirme.midas.siniestro.soporte;

import java.math.BigDecimal;

public class SoporteCoberturaRiesgoDTO {
	
	private static final long serialVersionUID = 1L;
	
	private BigDecimal idToSeccion;
	private BigDecimal numeroSubInciso;
	private BigDecimal idTocobertura;
	private BigDecimal idToRiesgo;
	
	
	public SoporteCoberturaRiesgoDTO (){
	}


	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}


	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}


	public BigDecimal getNumeroSubInciso() {
		return numeroSubInciso;
	}


	public void setNumeroSubInciso(BigDecimal numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}


	public BigDecimal getIdTocobertura() {
		return idTocobertura;
	}


	public void setIdTocobertura(BigDecimal idTocobertura) {
		this.idTocobertura = idTocobertura;
	}


	public BigDecimal getIdToRiesgo() {
		return idToRiesgo;
	}


	public void setIdToRiesgo(BigDecimal idToRiesgo) {
		this.idToRiesgo = idToRiesgo;
	}
	
	
}
