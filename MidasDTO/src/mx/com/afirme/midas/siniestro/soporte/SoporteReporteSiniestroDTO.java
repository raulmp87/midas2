package mx.com.afirme.midas.siniestro.soporte;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SoporteReporteSiniestroDTO implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	
	private Date fechaSiniestro;
	private String numeroSiniestro;
	private String descripcionSiniestro;
	private BigDecimal numeroInciso;
	private BigDecimal estatus;
	private String descripcionEstatus;
	private Double estimacion;
	private Double totalIndemnizaciones;
	
	private List<SoporteCoberturaRiesgoDTO> listaCoberturaRiesgo = new ArrayList<SoporteCoberturaRiesgoDTO>();
	
	
	
	public SoporteReporteSiniestroDTO(){
		
	}


	public Date getFechaSiniestro() {
		return fechaSiniestro;
	}


	public void setFechaSiniestro(Date fechaSiniestro) {
		this.fechaSiniestro = fechaSiniestro;
	}


	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}


	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}


	public String getDescripcionSiniestro() {
		return descripcionSiniestro;
	}


	public void setDescripcionSiniestro(String descripcionSiniestro) {
		this.descripcionSiniestro = descripcionSiniestro;
	}


	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}


	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}


	public BigDecimal getEstatus() {
		return estatus;
	}


	public void setEstatus(BigDecimal estatus) {
		this.estatus = estatus;
	}


	public String getDescripcionEstatus() {
		return descripcionEstatus;
	}


	public void setDescripcionEstatus(String descripcionEstatus) {
		this.descripcionEstatus = descripcionEstatus;
	}


	public Double getEstimacion() {
		return estimacion;
	}


	public void setEstimacion(Double estimacion) {
		this.estimacion = estimacion;
	}


	public Double getTotalIndemnizaciones() {
		return totalIndemnizaciones;
	}


	public void setTotalIndemnizaciones(Double totalIndemnizaciones) {
		this.totalIndemnizaciones = totalIndemnizaciones;
	}


	public List<SoporteCoberturaRiesgoDTO> getListaCoberturaRiesgo() {
		return listaCoberturaRiesgo;
	}


	public void setListaCoberturaRiesgo(
			List<SoporteCoberturaRiesgoDTO> listaCoberturaRiesgo) {
		this.listaCoberturaRiesgo = listaCoberturaRiesgo;
	}
	
	
}
