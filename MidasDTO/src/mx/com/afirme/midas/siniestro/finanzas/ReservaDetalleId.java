package mx.com.afirme.midas.siniestro.finanzas;
// default package

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * ReservaDetalleId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class ReservaDetalleId implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idtoreservaestimada;
	private BigDecimal idtoreportesiniestro;
	private BigDecimal idtopoliza;
	private BigDecimal numeroinciso;
	private BigDecimal idtoriesgo;
	private BigDecimal idtocobertura;
	private BigDecimal idtoseccion;
	private BigDecimal numerosubinciso;

	// Constructors

	/** default constructor */
	public ReservaDetalleId() {
	}

	// Property accessors

	@Column(name = "IDTORESERVAESTIMADA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdtoreservaestimada() {
		return this.idtoreservaestimada;
	}

	public void setIdtoreservaestimada(BigDecimal idtoreservaestimada) {
		this.idtoreservaestimada = idtoreservaestimada;
	}

	@Column(name = "IDTOREPORTESINIESTRO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdtoreportesiniestro() {
		return this.idtoreportesiniestro;
	}

	public void setIdtoreportesiniestro(BigDecimal idtoreportesiniestro) {
		this.idtoreportesiniestro = idtoreportesiniestro;
	}

	@Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdtopoliza() {
		return this.idtopoliza;
	}

	public void setIdtopoliza(BigDecimal idtopoliza) {
		this.idtopoliza = idtopoliza;
	}

	@Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getNumeroinciso() {
		return this.numeroinciso;
	}

	public void setNumeroinciso(BigDecimal numeroinciso) {
		this.numeroinciso = numeroinciso;
	}

	@Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdtoriesgo() {
		return this.idtoriesgo;
	}

	public void setIdtoriesgo(BigDecimal idtoriesgo) {
		this.idtoriesgo = idtoriesgo;
	}

	@Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdtocobertura() {
		return this.idtocobertura;
	}

	public void setIdtocobertura(BigDecimal idtocobertura) {
		this.idtocobertura = idtocobertura;
	}

	@Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdtoseccion() {
		return this.idtoseccion;
	}

	public void setIdtoseccion(BigDecimal idtoseccion) {
		this.idtoseccion = idtoseccion;
	}

	@Column(name = "NUMEROSUBINCISO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getNumerosubinciso() {
		return this.numerosubinciso;
	}

	public void setNumerosubinciso(BigDecimal numerosubinciso) {
		this.numerosubinciso = numerosubinciso;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ReservaDetalleId))
			return false;
		ReservaDetalleId castOther = (ReservaDetalleId) other;

		return ((this.getIdtoreservaestimada() == castOther
				.getIdtoreservaestimada()) || (this.getIdtoreservaestimada() != null
				&& castOther.getIdtoreservaestimada() != null && this
				.getIdtoreservaestimada().equals(
						castOther.getIdtoreservaestimada())))
				&& ((this.getIdtoreportesiniestro() == castOther
						.getIdtoreportesiniestro()) || (this
						.getIdtoreportesiniestro() != null
						&& castOther.getIdtoreportesiniestro() != null && this
						.getIdtoreportesiniestro().equals(
								castOther.getIdtoreportesiniestro())))
				&& ((this.getIdtopoliza() == castOther.getIdtopoliza()) || (this
						.getIdtopoliza() != null
						&& castOther.getIdtopoliza() != null && this
						.getIdtopoliza().equals(castOther.getIdtopoliza())))
				&& ((this.getNumeroinciso() == castOther.getNumeroinciso()) || (this
						.getNumeroinciso() != null
						&& castOther.getNumeroinciso() != null && this
						.getNumeroinciso().equals(castOther.getNumeroinciso())))
				&& ((this.getIdtoriesgo() == castOther.getIdtoriesgo()) || (this
						.getIdtoriesgo() != null
						&& castOther.getIdtoriesgo() != null && this
						.getIdtoriesgo().equals(castOther.getIdtoriesgo())))
				&& ((this.getIdtocobertura() == castOther.getIdtocobertura()) || (this
						.getIdtocobertura() != null
						&& castOther.getIdtocobertura() != null && this
						.getIdtocobertura()
						.equals(castOther.getIdtocobertura())))
				&& ((this.getIdtoseccion() == castOther.getIdtoseccion()) || (this
						.getIdtoseccion() != null
						&& castOther.getIdtoseccion() != null && this
						.getIdtoseccion().equals(castOther.getIdtoseccion())))
				&& ((this.getNumerosubinciso() == castOther
						.getNumerosubinciso()) || (this.getNumerosubinciso() != null
						&& castOther.getNumerosubinciso() != null && this
						.getNumerosubinciso().equals(
								castOther.getNumerosubinciso())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdtoreservaestimada() == null ? 0 : this
						.getIdtoreservaestimada().hashCode());
		result = 37
				* result
				+ (getIdtoreportesiniestro() == null ? 0 : this
						.getIdtoreportesiniestro().hashCode());
		result = 37
				* result
				+ (getIdtopoliza() == null ? 0 : this.getIdtopoliza()
						.hashCode());
		result = 37
				* result
				+ (getNumeroinciso() == null ? 0 : this.getNumeroinciso()
						.hashCode());
		result = 37
				* result
				+ (getIdtoriesgo() == null ? 0 : this.getIdtoriesgo()
						.hashCode());
		result = 37
				* result
				+ (getIdtocobertura() == null ? 0 : this.getIdtocobertura()
						.hashCode());
		result = 37
				* result
				+ (getIdtoseccion() == null ? 0 : this.getIdtoseccion()
						.hashCode());
		result = 37
				* result
				+ (getNumerosubinciso() == null ? 0 : this.getNumerosubinciso()
						.hashCode());
		return result;
	}

}