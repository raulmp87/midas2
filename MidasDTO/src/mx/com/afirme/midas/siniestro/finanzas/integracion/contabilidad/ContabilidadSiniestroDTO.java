package mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad;

// default package

import java.math.BigDecimal;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * ContabilidadSiniestro entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOCONTABILIDADSINIESTRO", schema = "MIDAS")
public class ContabilidadSiniestroDTO implements java.io.Serializable {
	public static final int ESTATUS_CONTABILIZADO = 1;
	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ContabilidadSiniestroId id;
	private BigDecimal idPolizaContable;
	private BigDecimal idRamo;
	private String conceptoPol;
	private BigDecimal idMoneda;
	private Double tipoCambio;
	private Integer auxiliar;
	private String cCosto;
	private String tipoPersona;
	private Double impNeto;
	private Double impBonificacion;
	private Double impDescuento;
	private Double impDerechos;
	private Double impRecargos;
	private Double impIva;
	private Double impComision;
	private Double impGastos;
	private Double impOtrosConc;
	private Double impOtrosImp;
	private String conceptoMov;
	private String usuario;
	private BigDecimal idAgente;
	private Double impIvaRet;
	private Double impIsrRet;

	// Constructors

	/** default constructor */
	public ContabilidadSiniestroDTO() {
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToReporteSiniestro", column = @Column(name = "IDTOREPORTESINIESTRO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idTcMovimientoSiniestro", column = @Column(name = "IDTCMOVIMIENTOSINIESTRO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idRegistro", column = @Column(name = "IDREGISTRO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idSubr", column = @Column(name = "ID_SUBR", nullable = false, precision = 22, scale = 0)), 
			@AttributeOverride(name = "cveConceptoMov", column = @Column(name = "CVE_CONCEPTO_MOV", nullable = false, length = 2)) })
	public ContabilidadSiniestroId getId() {
		return this.id;
	}

	public void setId(ContabilidadSiniestroId id) {
		this.id = id;
	}

	@Column(name = "IDPOLIZACONTABLE", precision = 22, scale = 0)
	public BigDecimal getIdPolizaContable() {
		return this.idPolizaContable;
	}

	public void setIdPolizaContable(BigDecimal idPolizaContable) {
		this.idPolizaContable = idPolizaContable;
	}

	@Column(name = "ID_RAMO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdRamo() {
		return this.idRamo;
	}

	public void setIdRamo(BigDecimal idRamo) {
		this.idRamo = idRamo;
	}

	@Column(name = "CONCEPTO_POL", nullable = false, length = 240)
	public String getConceptoPol() {
		return this.conceptoPol;
	}

	public void setConceptoPol(String conceptoPol) {
		this.conceptoPol = conceptoPol;
	}

	@Column(name = "ID_MONEDA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdMoneda() {
		return this.idMoneda;
	}

	public void setIdMoneda(BigDecimal idMoneda) {
		this.idMoneda = idMoneda;
	}

	@Column(name = "TIPO_CAMBIO", nullable = false, precision = 14)
	public Double getTipoCambio() {
		return this.tipoCambio;
	}

	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	@Column(name = "AUXILIAR", nullable = false, precision = 5, scale = 0)
	public Integer getAuxiliar() {
		return this.auxiliar;
	}

	public void setAuxiliar(Integer auxiliar) {
		this.auxiliar = auxiliar;
	}

	@Column(name = "CCOSTO", nullable = false, length = 5)
	public String getCCosto() {
		return this.cCosto;
	}

	public void setCCosto(String cCosto) {
		this.cCosto = cCosto;
	}

	@Column(name = "TIPOPERSONA", nullable = false, length = 1)
	public String getTipoPersona() {
		return this.tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	@Column(name = "IMP_NETO", nullable = false, precision = 14)
	public Double getImpNeto() {
		return this.impNeto;
	}

	public void setImpNeto(Double impNeto) {
		this.impNeto = impNeto;
	}

	@Column(name = "IMP_BONIFICACION", nullable = false, precision = 14)
	public Double getImpBonificacion() {
		return this.impBonificacion;
	}

	public void setImpBonificacion(Double impBonificacion) {
		this.impBonificacion = impBonificacion;
	}

	@Column(name = "IMP_DESCUENTO", nullable = false, precision = 14)
	public Double getImpDescuento() {
		return this.impDescuento;
	}

	public void setImpDescuento(Double impDescuento) {
		this.impDescuento = impDescuento;
	}

	@Column(name = "IMP_DERECHOS", nullable = false, precision = 14)
	public Double getImpDerechos() {
		return this.impDerechos;
	}

	public void setImpDerechos(Double impDerechos) {
		this.impDerechos = impDerechos;
	}

	@Column(name = "IMP_RECARGOS", nullable = false, precision = 14)
	public Double getImpRecargos() {
		return this.impRecargos;
	}

	public void setImpRecargos(Double impRecargos) {
		this.impRecargos = impRecargos;
	}

	@Column(name = "IMP_IVA", nullable = false, precision = 14)
	public Double getImpIva() {
		return this.impIva;
	}

	public void setImpIva(Double impIva) {
		this.impIva = impIva;
	}

	@Column(name = "IMP_COMISION", nullable = false, precision = 14)
	public Double getImpComision() {
		return this.impComision;
	}

	public void setImpComision(Double impComision) {
		this.impComision = impComision;
	}

	@Column(name = "IMP_GASTOS", nullable = false, precision = 14)
	public Double getImpGastos() {
		return this.impGastos;
	}

	public void setImpGastos(Double impGastos) {
		this.impGastos = impGastos;
	}

	@Column(name = "IMP_OTROS_CONC", nullable = false, precision = 14)
	public Double getImpOtrosConc() {
		return this.impOtrosConc;
	}

	public void setImpOtrosConc(Double impOtrosConc) {
		this.impOtrosConc = impOtrosConc;
	}

	@Column(name = "IMP_OTROS_IMP", nullable = false, precision = 14)
	public Double getImpOtrosImp() {
		return this.impOtrosImp;
	}

	public void setImpOtrosImp(Double impOtrosImp) {
		this.impOtrosImp = impOtrosImp;
	}

	@Column(name = "CONCEPTO_MOV", nullable = false, length = 80)
	public String getConceptoMov() {
		return this.conceptoMov;
	}

	public void setConceptoMov(String conceptoMov) {
		this.conceptoMov = conceptoMov;
	}

	@Column(name = "USUARIO", nullable = false, length = 8)
	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
    @Column(name="IDAGENTE", precision=22, scale=0)
    public BigDecimal getIdAgente() {
        return this.idAgente;
    }
    
    public void setIdAgente(BigDecimal idAgente) {
        this.idAgente = idAgente;
    }
    
    @Column(name = "IMP_IVA_RET", nullable = false, precision = 14)
	public Double getImpIvaRet() {
		return this.impIvaRet;
	}

	public void setImpIvaRet(Double impIvaRet) {
		this.impIvaRet = impIvaRet;
	}

	@Column(name = "IMP_ISR_RET", nullable = false, precision = 14)
	public Double getImpIsrRet() {
		return this.impIsrRet;
	}

	public void setImpIsrRet(Double impIsrRet) {
		this.impIsrRet = impIsrRet;
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();									
		
		sb.append("");
		sb.append("ContabilidadSiniestroDTO { " + '\n');
		sb.append(id.toString());
		
		if(idRamo != null){
			sb.append("idRamo = " + idRamo.toString() + '\n');
		}else{
			sb.append("idRamo = null " + '\n');
		}
		
		sb.append("conceptoPol = " + conceptoPol + '\n');
		
		if(idMoneda != null){
			sb.append("idMoneda = " + idMoneda.toString() + '\n');
		}else{
			sb.append("idMoneda = null " + '\n');
		}
		
		sb.append("cCosto = " + cCosto + '\n');
		sb.append("tipoPersona = " + tipoPersona + '\n');
		sb.append("conceptoMov = " + conceptoMov+ '\n');
		
		if(idAgente != null){
			sb.append("idAgente = " + idAgente.toString() + '\n');
		}else{
			sb.append("idAgente = null " + '\n');
		}
		
		if(tipoCambio != null){
			sb.append("tipoCambio = " + tipoCambio.toString() + '\n');
		}else{
			sb.append("tipoCambio = null " + '\n');
		}
		
		if(auxiliar != null){
			sb.append("auxiliar = " + auxiliar.toString() + '\n');
		}else{
			sb.append("auxiliar = null " + '\n');
		}

		if(impNeto != null){
			sb.append("impNeto = " + impNeto.toString() + '\n');
		}else{
			sb.append("impNeto = null " + '\n');
		}

		if(impBonificacion != null){
			sb.append("impBonificacion = " + impBonificacion.toString() + '\n');
		}else{
			sb.append("impBonificacion = null " + '\n');
		}

		if(impDescuento != null){
			sb.append("impDescuento = " + impDescuento.toString() + '\n');
		}else{
			sb.append("impDescuento = null " + '\n');
		}
		
		if(impDerechos != null){
			sb.append("impDerechos = " + impDerechos.toString() + '\n');
		}else{
			sb.append("impDerechos = null " + '\n');
		}

		if(impRecargos != null){
			sb.append("impRecargos = " + impRecargos.toString() + '\n');
		}else{
			sb.append("impRecargos = null " + '\n');
		}

		if(impIva != null){
			sb.append("impIva = " + impIva.toString() + '\n');
		}else{
			sb.append("impIva = null " + '\n');
		}
		
		if(impComision != null){
			sb.append("impComision = " + impComision.toString() + '\n');
		}else{
			sb.append("impComision = null " + '\n');
		}

		if(impGastos != null){
			sb.append("impGastos = " + impGastos.toString() + '\n');
		}else{
			sb.append(" = null " + '\n');
		}

		if(impOtrosConc != null){
			sb.append("impOtrosConc = " + impOtrosConc.toString() + '\n');
		}else{
			sb.append("impOtrosConc = null " + '\n');
		}
		
		if(impOtrosImp != null){
			sb.append("impOtrosImp = " + impOtrosImp.toString() + '\n');
		}else{
			sb.append("impOtrosImp = null " + '\n');
		}	
		if(impIvaRet != null){
			sb.append("impIvaRet = " + impIvaRet.toString() + '\n');
		}else{
			sb.append("impIvaRet = null " + '\n');
		}
		if(impIsrRet != null){
			sb.append("impIsrRet = " + impIsrRet.toString() + '\n');
		}else{
			sb.append("impIsrRet = null " + '\n');
		}
		
		sb.append("}" + '\n');
		
		return sb.toString();				
	}
}