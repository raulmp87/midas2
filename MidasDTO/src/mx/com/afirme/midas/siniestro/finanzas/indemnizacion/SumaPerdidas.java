package mx.com.afirme.midas.siniestro.finanzas.indemnizacion;

public class SumaPerdidas {

	@SuppressWarnings("unused")
	private Double montoPago;
    @SuppressWarnings("unused")
	private Double deducible;
    @SuppressWarnings("unused")
	private Double coaseguro;
    private Double suma;
    
    public SumaPerdidas(Double montoPago,Double deducible,Double coaseguro){
    	suma = montoPago + deducible + coaseguro;
    }
    
    public Double getSuma() {
		return suma;
	}

	public void setSuma(Double suma) {
		this.suma = suma;
	}
    
	public static Double sumame(Double montoPago,Double deducible,Double coaseguro){
		return montoPago + deducible + coaseguro;
	}
}

	
