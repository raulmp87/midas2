package mx.com.afirme.midas.siniestro.finanzas;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for ReservaDetalleFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ReservaDetalleFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved ReservaDetalleDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            ReservaDetalleDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ReservaDetalleDTO entity);

	/**
	 * Delete a persistent ReservaDetalleDTO entity.
	 * 
	 * @param entity
	 *            ReservaDetalleDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ReservaDetalleDTO entity);

	/**
	 * Persist a previously saved ReservaDetalleDTO entity and return it or a copy
	 * of it to the sender. A copy of the ReservaDetalleDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            ReservaDetalleDTO entity to update
	 * @return ReservaDetalleDTO the persisted ReservaDetalleDTO entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ReservaDetalleDTO update(ReservaDetalleDTO entity);

	public ReservaDetalleDTO findById(ReservaDetalleId id);

	/**
	 * Find all ReservaDetalleDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ReservaDetalleDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<ReservaDetalleDTO> found by query
	 */
	public List<ReservaDetalleDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all ReservaDetalleDTO entities.
	 * 
	 * @return List<ReservaDetalleDTO> all ReservaDetalleDTO entities
	 */
	public List<ReservaDetalleDTO> findAll();
	
	public List<ReservaDetalleDTO> listarDetalleReserva(BigDecimal idReserva);
	
	public Double sumaReserva(BigDecimal idReserva);
	
	public List<ReservaDetalleDTO> listarReservaDetalle(BigDecimal idReserva);
	
	public Double diferenciaDeReserva(BigDecimal idToReporteSiniestro);
	
	public Double obtenerSumaReservaInicial(BigDecimal idToReporteSiniestro);
	
}