package mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad;

import java.util.List;
import javax.ejb.Remote;

/**
* Remote interface for ContabilidadOrdenPagoFacade.
* 
* @author MyEclipse Persistence Tools
*/

public interface ContabilidadOrdenPagoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved ContabilidadOrdenPago
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            ContabilidadOrdenPago entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ContabilidadOrdenPagoDTO entity);

	/**
	 * Delete a persistent ContabilidadOrdenPago entity.
	 * 
	 * @param entity
	 *            ContabilidadOrdenPago entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ContabilidadOrdenPagoDTO entity);

	/**
	 * Persist a previously saved ContabilidadOrdenPago entity and return it or
	 * a copy of it to the sender. A copy of the ContabilidadOrdenPago entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            ContabilidadOrdenPago entity to update
	 * @return ContabilidadOrdenPago the persisted ContabilidadOrdenPago entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ContabilidadOrdenPagoDTO update(ContabilidadOrdenPagoDTO entity);

	public ContabilidadOrdenPagoDTO findById(ContabilidadOrdenPagoId id);

	/**
	 * Find all ContabilidadOrdenPago entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ContabilidadOrdenPago property to query
	 * @param value
	 *            the property value to match
	 * @return List<ContabilidadOrdenPago> found by query
	 */
	public List<ContabilidadOrdenPagoDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all ContabilidadOrdenPago entities.
	 * 
	 * @return List<ContabilidadOrdenPago> all ContabilidadOrdenPago entities
	 */
	public List<ContabilidadOrdenPagoDTO> findAll();
}