package mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.gasto.GastoSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.IngresoSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDTO;

/**
 * AutorizacionTecnica entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOAUTORIZACIONTECNICA" ,schema="MIDAS")
public class AutorizacionTecnicaDTO  implements java.io.Serializable {
	public static final short ESTATUS_PENDIENTE_AUTORIZAR = 0;
	public static final short ESTATUS_AUTORIZADA = 1;
	public static final short ESTATUS_CANCELADA = 2;	

	public static final String DESCRIPCION_ESTATUS_PENDIENTE_AUTORIZAR = "Pendiente de Autorizar";
	public static final String DESCRIPCION_ESTATUS_AUTORIZADA = "Autorizada";
	public static final String DESCRIPCION_ESTATUS_CANCELADA = "Cancelada";
	
	public static final String DESCRIPCION_TIPO_AUTORIZACION_INGRESO = "Ingreso";
	public static final String DESCRIPCION_TIPO_AUTORIZACION_INDEMNIZACION = "Indemnizacion";
	public static final String DESCRIPCION_TIPO_AUTORIZACION_GASTO = "Gasto";
	
	public static final int ID_TIPO_AUTORIZACION_GASTO = 1;
	public static final int ID_TIPO_AUTORIZACION_INGRESO = 2;
	public static final int ID_TIPO_AUTORIZACION_INDEMNIZACION = 3;
	
    /**
	 *  
	 */
	private static final long serialVersionUID = 1L;
	// Fields    
     private BigDecimal idToAutorizacionTecnica;     
     private GastoSiniestroDTO gastoSiniestroDTO;
     private IngresoSiniestroDTO ingresoSiniestroDTO;
     private IndemnizacionDTO indemnizacionDTO;          
     private Date fecha;
     private Short estatus;
     private ReporteSiniestroDTO reporteSiniestroDTO;
     private Double montoNeto;
     private String descripcionEstatus;
     private BigDecimal idToOrdenPago;
     private String tipoAutorizacionTecnica;
     private int idTipoAutorizacionTecnica;
     private BigDecimal idSolicitudCheque;
     private String codigoUsuarioCreacion;
     private Date fechaCreacion;
     private String codigoUsuarioAutoriza;
     private Date fechaAutorizacion;
     private BigDecimal idConcepto;
     private Object objectTipoAutorizacionTecnica;
     private String claveSeleccion;
    /** default constructor */
    public AutorizacionTecnicaDTO() {
    }
   
    
	// Property accessors
    @Id
    @SequenceGenerator(name = "IDTOAUTORIZACIONTECNICA_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOAUTORIZACIONTECNICA_SEQ")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "IDTOAUTORIZACIONTECNICA_SEQ_GENERADOR")
    @Column(name="IDTOAUTORIZACIONTECNICA", unique=true, nullable=false, precision=22, scale=0)
    public BigDecimal getIdToAutorizacionTecnica() {
        return this.idToAutorizacionTecnica;
    }    	
	    
    
    public void setIdToAutorizacionTecnica(BigDecimal idToAutorizacionTecnica) {
        this.idToAutorizacionTecnica = idToAutorizacionTecnica;
    }
	
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTOGASTOSINIESTRO")
    public GastoSiniestroDTO getGastoSiniestroDTO() {
        return this.gastoSiniestroDTO;
    }
    
    public void setGastoSiniestroDTO(GastoSiniestroDTO gastoSiniestroDTO) {
        this.gastoSiniestroDTO = gastoSiniestroDTO;
    }
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="IDTOINDEMNIZACION")
    public IndemnizacionDTO getIndemnizacionDTO() {
        return this.indemnizacionDTO;
    }
    
    public void setIndemnizacionDTO(IndemnizacionDTO indemnizacionDTO) {
        this.indemnizacionDTO = indemnizacionDTO;
    }
	
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTOINGRESOSINIESTRO")
    public IngresoSiniestroDTO getIngresoSiniestroDTO() {
        return this.ingresoSiniestroDTO;
    }
    
    public void setIngresoSiniestroDTO(IngresoSiniestroDTO ingresoSiniestroDTO) {
        this.ingresoSiniestroDTO = ingresoSiniestroDTO;
    }
    
    @Temporal(TemporalType.DATE)
    @Column(name="FECHA", nullable=false, length=7)
    public Date getFecha() {
        return this.fecha;
    }
    
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
    @Column(name="ESTATUS", nullable=false, precision=22, scale=0)
    public Short getEstatus() {
        return this.estatus;
    }
    
    public void setEstatus(Short estatus) {
        this.estatus = estatus;
    }
    
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOREPORTESINIESTRO", nullable = false)
	public ReporteSiniestroDTO getReporteSiniestroDTO() {
		return this.reporteSiniestroDTO;
	}

	public void setReporteSiniestroDTO(ReporteSiniestroDTO toReporteSiniestro) {
		this.reporteSiniestroDTO = toReporteSiniestro;
	}
	
    @Column(name="MONTONETO", nullable=false, precision=16)
    public Double getMontoNeto() {
        return this.montoNeto;
    }
    
    public void setMontoNeto(Double montoNeto) {
        this.montoNeto = montoNeto;
    }

    @Transient
	public String getDescripcionEstatus() {
		return descripcionEstatus;
	}

    public void setDescripcionEstatus(String descripcionEstatus) {
		this.descripcionEstatus = descripcionEstatus;
	}

	@Transient
	public BigDecimal getIdToOrdenPago() {
		return idToOrdenPago;
	}

	public void setIdToOrdenPago(BigDecimal idToOrdenPago) {
		this.idToOrdenPago = idToOrdenPago;
	}
	
	@Transient
	public String getTipoAutorizacionTecnica() {
		return tipoAutorizacionTecnica;
	}		

	public void setTipoAutorizacionTecnica(String tipoAutorizacionTecnica) {
		this.tipoAutorizacionTecnica = tipoAutorizacionTecnica;
	}
	
	@Transient
	public int getIdTipoAutorizacionTecnica() {		
		
		if(gastoSiniestroDTO != null){
			idTipoAutorizacionTecnica = ID_TIPO_AUTORIZACION_GASTO;
		}else if(ingresoSiniestroDTO != null){
			idTipoAutorizacionTecnica = ID_TIPO_AUTORIZACION_INGRESO;
		}else{
			idTipoAutorizacionTecnica = ID_TIPO_AUTORIZACION_INDEMNIZACION;
		}
					
		return idTipoAutorizacionTecnica;
	}
	
	@Transient
	public BigDecimal getIdSolicitudCheque() {
		return idSolicitudCheque;
	}

	public void setIdSolicitudCheque(BigDecimal idSolicitudCheque) {
		this.idSolicitudCheque = idSolicitudCheque;
	}
	
	@Column(name="CODIGOUSUARIOCREACION", nullable=false, length=8)
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	@Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHACREACION", nullable=false, length=7)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	@Column(name="CODIGOUSUARIOAUTORIZA", nullable=false, length=8)
	public String getCodigoUsuarioAutoriza() {
		return codigoUsuarioAutoriza;
	}

	public void setCodigoUsuarioAutoriza(String codigoUsuarioAutoriza) {
		this.codigoUsuarioAutoriza = codigoUsuarioAutoriza;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHAAUTORIZACION", nullable=false, length=7)
	public Date getFechaAutorizacion() {
		return fechaAutorizacion;
	}

	public void setFechaAutorizacion(Date fechaAutorizacion) {
		this.fechaAutorizacion = fechaAutorizacion;
	}
	
	@Transient
	public BigDecimal getIdConcepto() {				
		if(gastoSiniestroDTO != null){
			idConcepto = gastoSiniestroDTO.getIdToGastoSiniestro();
		}else if(ingresoSiniestroDTO != null){
			idConcepto = ingresoSiniestroDTO.getIdToIngresoSiniestro();
		}else{
			idConcepto = indemnizacionDTO.getIdToIndemnizacion();
		}
					
		return idConcepto;
	}	
	
	@Transient
	public Object getObjectTipoAutorizacionTecnica() {		
		
		if(gastoSiniestroDTO != null){
			objectTipoAutorizacionTecnica = gastoSiniestroDTO;
		}else if(ingresoSiniestroDTO != null){
			objectTipoAutorizacionTecnica = ingresoSiniestroDTO;
		}else{
			objectTipoAutorizacionTecnica = indemnizacionDTO;
		}
					
		return objectTipoAutorizacionTecnica;
	}

	@Transient
	public String getClaveSeleccion() {
		return claveSeleccion;
	}


	public void setClaveSeleccion(String claveSeleccion) {
		this.claveSeleccion = claveSeleccion;
	}
}