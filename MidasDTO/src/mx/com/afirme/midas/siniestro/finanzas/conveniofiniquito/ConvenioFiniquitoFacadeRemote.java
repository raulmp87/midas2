package mx.com.afirme.midas.siniestro.finanzas.conveniofiniquito;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for ConvenioFiniquitoFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ConvenioFiniquitoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved ConvenioFiniquito entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            ConvenioFiniquito entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public ConvenioFiniquitoDTO save(ConvenioFiniquitoDTO entity);

	/**
	 * Delete a persistent ConvenioFiniquito entity.
	 * 
	 * @param entity
	 *            ConvenioFiniquito entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ConvenioFiniquitoDTO entity);

	/**
	 * Persist a previously saved ConvenioFiniquito entity and return it or a
	 * copy of it to the sender. A copy of the ConvenioFiniquito entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            ConvenioFiniquito entity to update
	 * @return ConvenioFiniquito the persisted ConvenioFiniquito entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ConvenioFiniquitoDTO update(ConvenioFiniquitoDTO entity);

	public ConvenioFiniquitoDTO findById(BigDecimal id);

	/**
	 * Find all ConvenioFiniquito entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ConvenioFiniquito property to query
	 * @param value
	 *            the property value to match
	 * @return List<ConvenioFiniquito> found by query
	 */
	public List<ConvenioFiniquitoDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all ConvenioFiniquito entities.
	 * @return List<ConvenioFiniquito> all ConvenioFiniquito entities
	 */
	public List<ConvenioFiniquitoDTO> findAll();
	
	public ConvenioFiniquitoDTO obtenerConvenioCancelar(BigDecimal idToReporteSiniestro, BigDecimal idToIndemnizacion,Short estatus);
}