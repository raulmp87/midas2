package mx.com.afirme.midas.siniestro.finanzas.ingreso;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.EstatusFinanzasDTO;

/**
 * IngresoSiniestro entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOINGRESOSINIESTRO", schema = "MIDAS")
public class IngresoSiniestroDTO implements java.io.Serializable {

	public static final String ALTA_INGRESO = "A";
	public static final String CANCELACION_INGRESO = "C";
	public static final String INGRESO_SALVAMENTO = "IS";
	public static final String INGRESO_RECUPERACION = "IR";
	public static final String INGRESO_DEDUCIBLE = "ID";
	public static final String INGRESO_COASEGURO = "IC";
	public static final String CANCELACION_INGRESO_SALVAMENTO = "CIS";
	public static final String CANCELACION_INGRESO_RECUPERACION = "CIR";
	public static final String CANCELACION_INGRESO_DEDUCIBLE = "CID";
	public static final String CANCELACION_INGRESO_COASEGURO = "CIC";
	public static final String ALTA_SALVAMENTO = "AS";
	public static final String ALTA_RECUPERACION = "AR";
	public static final String CANCELACION_SALVAMENTO = "CS";
	public static final String CANCELACION_RECUPERACION = "CR";

	private static final long serialVersionUID = 1L;
	private BigDecimal idToIngresoSiniestro;
	private String descripcion;
	private Double monto;
	private Double iva;
	private Date fechaCobro;
	private BigDecimal fichaDeposito;
	private BigDecimal transferencia;

	private Double porcentajeIVA;
	private Double porcentajeIVARetencion;
	private Double porcentajeISR;
	private Double porcentajeISRRetencion;
	private Double porcentajeOtros;
	private Double montoIVA;
	private Double montoIVARetencion;
	private Double montoISR;
	private Double montoISRRetencion;
	private Double montoOtros;
	private Date fechaCreacion;
	private BigDecimal idTcUsuarioCreacion;
	private Date fechaModificacion;
	private BigDecimal idTcUsuarioModificacion;
	private EstatusFinanzasDTO estatusFinanzas;
	
	
	private ReporteSiniestroDTO reporteSiniestroDTO;
	private ConceptoIngresoDTO conceptoIngreso;

	// Constructors

	/** default constructor */
	public IngresoSiniestroDTO() {
	}

	// Property accessors 
	@Id
	@SequenceGenerator(name = "IDTOINGRESOSINIESTRO_SEQ_GENERADOR",allocationSize = 1, sequenceName = "MIDAS.IDTOINGRESOSINIESTRO_SEQ")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "IDTOINGRESOSINIESTRO_SEQ_GENERADOR")
	@Column(name = "IDTOINGRESOSINIESTRO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToIngresoSiniestro() {
		return this.idToIngresoSiniestro;
	}

	public void setIdToIngresoSiniestro(BigDecimal idToIngresoSiniestro) {
		this.idToIngresoSiniestro = idToIngresoSiniestro;
	}

	@Column(name = "DESCRIPCION", nullable = false, length = 240)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "MONTO", nullable = false, precision = 16)
	public Double getMonto() {
		return this.monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}

	@Column(name = "IVA", precision = 16)
	public Double getIva() {
		return this.iva;
	}

	public void setIva(Double iva) {
		this.iva = iva;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHACOBRO", nullable = false, length = 7)
	public Date getFechaCobro() {
		return this.fechaCobro;
	}

	public void setFechaCobro(Date fechaCobro) {
		this.fechaCobro = fechaCobro;
	}

	@Column(name = "FICHADEPOSITO", precision = 22, scale = 0)
	public BigDecimal getFichaDeposito() {
		return this.fichaDeposito;
	}

	public void setFichaDeposito(BigDecimal fichaDeposito) {
		this.fichaDeposito = fichaDeposito;
	}

	@Column(name = "TRANSFERENCIA", precision = 22, scale = 0)
	public BigDecimal getTransferencia() {
		return this.transferencia;
	}

	public void setTransferencia(BigDecimal transferencia) {
		this.transferencia = transferencia;
	}
	
	@Column(name = "PORCENTAJEIVA", precision = 5)
	public Double getPorcentajeIVA() {
		return porcentajeIVA;
	}

	public void setPorcentajeIVA(Double porcentajeIVA) {
		this.porcentajeIVA = porcentajeIVA;
	}
	
	@Column(name = "PORCENTAJEIVARETENCION", precision = 5)
	public Double getPorcentajeIVARetencion() {
		return porcentajeIVARetencion;
	}

	public void setPorcentajeIVARetencion(Double porcentajeIVARetencion) {
		this.porcentajeIVARetencion = porcentajeIVARetencion;
	}
	
	@Column(name = "PORCENTAJEISR", precision = 5)
	public Double getPorcentajeISR() {
		return porcentajeISR;
	}

	public void setPorcentajeISR(Double porcentajeISR) {
		this.porcentajeISR = porcentajeISR;
	}
	
	@Column(name = "PORCENTAJEISRRETENCION", precision = 5)
	public Double getPorcentajeISRRetencion() {
		return porcentajeISRRetencion;
	}

	public void setPorcentajeISRRetencion(Double porcentajeISRRetencion) {
		this.porcentajeISRRetencion = porcentajeISRRetencion;
	}
	
	@Column(name = "PORCENTAJEOTROS", precision = 5)
	public Double getPorcentajeOtros() {
		return porcentajeOtros;
	}

	public void setPorcentajeOtros(Double porcentajeOtros) {
		this.porcentajeOtros = porcentajeOtros;
	}

	@Column(name = "MONTOIVA", precision = 16)
	public Double getMontoIVA() {
		return this.montoIVA;
	}

	public void setMontoIVA(Double montoIVA) {
		this.montoIVA = montoIVA;
	}

	@Column(name = "MONTOIVARETENCION", precision = 16)
	public Double getMontoIVARetencion() {
		return this.montoIVARetencion;
	}

	public void setMontoIVARetencion(Double montoIVARetencion) {
		this.montoIVARetencion = montoIVARetencion;
	}

	@Column(name = "MONTOISR", precision = 16)
	public Double getMontoISR() {
		return this.montoISR;
	}

	public void setMontoISR(Double montoISR) {
		this.montoISR = montoISR;
	}

	@Column(name = "MONTOISRRETENCION", precision = 16)
	public Double getMontoISRRetencion() {
		return this.montoISRRetencion;
	}

	public void setMontoISRRetencion(Double montoISRRetencion) {
		this.montoISRRetencion = montoISRRetencion;
	}

	@Column(name = "MONTOOTROS", precision = 16)
	public Double getMontoOtros() {
		return this.montoOtros;
	}

	public void setMontoOtros(Double montoOtros) {
		this.montoOtros = montoOtros;
	}


	@Temporal(TemporalType.DATE)
	@Column(name = "FECHACREACION", nullable = false, length = 7)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Column(name = "IDTCUSUARIOCREACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcUsuarioCreacion() {
		return this.idTcUsuarioCreacion;
	}

	public void setIdTcUsuarioCreacion(BigDecimal idTcUsuarioCreacion) {
		this.idTcUsuarioCreacion = idTcUsuarioCreacion;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAMODIFICACION", length = 7)
	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Column(name = "IDTCUSUARIOMODIFICACION", precision = 22, scale = 0)
	public BigDecimal getIdTcUsuarioModificacion() {
		return this.idTcUsuarioModificacion;
	}

	public void setIdTcUsuarioModificacion(BigDecimal idTcUsuarioModificacion) {
		this.idTcUsuarioModificacion = idTcUsuarioModificacion;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTCESTATUS")
	public EstatusFinanzasDTO getEstatusFinanzas() {
		return estatusFinanzas;
	}

	public void setEstatusFinanzas(EstatusFinanzasDTO estatusFinanzas) {
		this.estatusFinanzas = estatusFinanzas;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOREPORTESINIESTRO", nullable = false)
	public ReporteSiniestroDTO getReporteSiniestroDTO() {
		return this.reporteSiniestroDTO;
	}

	public void setReporteSiniestroDTO(ReporteSiniestroDTO toreportesiniestro) {
		this.reporteSiniestroDTO = toreportesiniestro;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTCCONCEPTOINGRESO")
    public ConceptoIngresoDTO getConceptoIngreso(){
		return this.conceptoIngreso;
	}
	
	public void setConceptoIngreso(ConceptoIngresoDTO conceptoIngreso){
		this.conceptoIngreso = conceptoIngreso;
	}	
}
