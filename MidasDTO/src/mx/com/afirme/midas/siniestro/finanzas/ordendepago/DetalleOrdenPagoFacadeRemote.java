// default package
package mx.com.afirme.midas.siniestro.finanzas.ordendepago;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for DetalleOrdenPagoDTOFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface DetalleOrdenPagoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved DetalleOrdenPagoDTO
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            DetalleOrdenPagoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(DetalleOrdenPagoDTO entity);

	/**
	 * Delete a persistent DetalleOrdenPagoDTO entity.
	 * 
	 * @param entity
	 *            DetalleOrdenPagoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(DetalleOrdenPagoDTO entity);

	/**
	 * Persist a previously saved DetalleOrdenPagoDTO entity and return it or a
	 * copy of it to the sender. A copy of the DetalleOrdenPagoDTO entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            DetalleOrdenPagoDTO entity to update
	 * @return DetalleOrdenPagoDTO the persisted DetalleOrdenPagoDTO entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public DetalleOrdenPagoDTO update(DetalleOrdenPagoDTO entity);

	public DetalleOrdenPagoDTO findById(BigDecimal id);

	/**
	 * Find all DetalleOrdenPagoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the DetalleOrdenPagoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<DetalleOrdenPagoDTO> found by query
	 */
	public List<DetalleOrdenPagoDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all DetalleOrdenPagoDTO entities.
	 * 
	 * @return List<DetalleOrdenPagoDTO> all DetalleOrdenPagoDTO entities
	 */
	public List<DetalleOrdenPagoDTO> findAll();
}