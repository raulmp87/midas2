package mx.com.afirme.midas.siniestro.finanzas;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;

/**
 * ReservaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TORESERVAESTIMADA", schema = "MIDAS")
public class ReservaDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idtoreservaestimada;
	private ReporteSiniestroDTO reporteSiniestroDTO;
	private String descripcion;
	private BigDecimal tipoajuste;
	private Date fechaestimacion;
	private BigDecimal idtousuariocreacion;
	private Boolean autorizada;
	private Date fechaautorizacion;
	private BigDecimal idtousuarioautoriza;
	private List <ReservaDetalleDTO> reservaDetalleDTOs = new ArrayList<ReservaDetalleDTO>();

	public static final Boolean AUTORIZADA = true;
	public static final Boolean NO_AUTORIZADA = false;
	// Constructors

	/** default constructor */
	public ReservaDTO() {
	}

	

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTORESERVAESTIMADA_SEQ_GENERADOR",allocationSize = 1, sequenceName = "MIDAS.IDTORESERVAESTIMADA_SEQ")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "IDTORESERVAESTIMADA_SEQ_GENERADOR")
	@Column(name = "IDTORESERVAESTIMADA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdtoreservaestimada() {
		return this.idtoreservaestimada;
	}

	public void setIdtoreservaestimada(BigDecimal idtoreservaestimada) {
		this.idtoreservaestimada = idtoreservaestimada;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOREPORTESINIESTRO", nullable = false)
	public ReporteSiniestroDTO getReporteSiniestroDTO() {
		return this.reporteSiniestroDTO;
	}

	public void setReporteSiniestroDTO(ReporteSiniestroDTO toreportesiniestro) {
		this.reporteSiniestroDTO = toreportesiniestro;
	}

	@Column(name = "DESCRIPCION", length = 240)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "TIPOAJUSTE", nullable = false, precision = 22, scale = 0)
	public BigDecimal getTipoajuste() {
		return this.tipoajuste;
	}

	public void setTipoajuste(BigDecimal tipoajuste) {
		this.tipoajuste = tipoajuste;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAESTIMACION", nullable = false, length = 7)
	public Date getFechaestimacion() {
		return this.fechaestimacion;
	}

	public void setFechaestimacion(Date fechaestimacion) {
		this.fechaestimacion = fechaestimacion;
	}

	@Column(name = "IDTOUSUARIOCREACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdtousuariocreacion() {
		return this.idtousuariocreacion;
	}

	public void setIdtousuariocreacion(BigDecimal idtousuariocreacion) {
		this.idtousuariocreacion = idtousuariocreacion;
	}

	@Column(name = "AUTORIZADA", nullable = false, precision = 1, scale = 0)
	public Boolean getAutorizada() {
		return this.autorizada;
	}

	public void setAutorizada(Boolean autorizada) {
		this.autorizada = autorizada;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAAUTORIZACION", length = 7)
	public Date getFechaautorizacion() {
		return this.fechaautorizacion;
	}

	public void setFechaautorizacion(Date fechaautorizacion) {
		this.fechaautorizacion = fechaautorizacion;
	}

	@Column(name = "IDTOUSUARIOAUTORIZA", precision = 22, scale = 0)
	public BigDecimal getIdtousuarioautoriza() {
		return this.idtousuarioautoriza;
	}

	public void setIdtousuarioautoriza(BigDecimal idtousuarioautoriza) {
		this.idtousuarioautoriza = idtousuarioautoriza;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "reservaDTO")
	public List<ReservaDetalleDTO> getReservaDetalleDTOs() {
		return reservaDetalleDTOs;
	}



	public void setReservaDetalleDTOs(List<ReservaDetalleDTO> reservaDetalleDTOs) {
		this.reservaDetalleDTOs = reservaDetalleDTOs;
	}

	
	

}