package mx.com.afirme.midas.siniestro.finanzas.integracion;

import java.io.Serializable;
import java.math.BigDecimal;

import mx.com.afirme.midas.danios.soporte.CoberturaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SeccionSoporteDanosDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;


public class SoporteDistribucionReaseguroDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal numeroInciso;
	private BigDecimal numeroSubInciso;
	private CoberturaSoporteDanosDTO coberturaDTO;
	private SeccionSoporteDanosDTO seccionDTO;
	private LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO;
	private double porcentajeSumaAsegurada;
	private double monto;
	private double montoRetenido;
	private double montoCuotaParte;
	private double montoPrimerExcedente;
	private double montoFacultativo;
	
	public SoporteDistribucionReaseguroDTO(){
		
	}
	
	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public double getPorcentajeSumaAsegurada() {
		return porcentajeSumaAsegurada;
	}

	public void setPorcentajeSumaAsegurada(double porcentajeSumaAsegurada) {
		this.porcentajeSumaAsegurada = porcentajeSumaAsegurada;
	}

	
	public BigDecimal getNumeroSubInciso() {
		return numeroSubInciso;
	}

	public void setNumeroSubInciso(BigDecimal numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}

	public SeccionSoporteDanosDTO getSeccionDTO() {
		return seccionDTO;
	}

	public void setSeccionDTO(SeccionSoporteDanosDTO seccionDTO) {
		this.seccionDTO = seccionDTO;
	}

	public void setCoberturaDTO(CoberturaSoporteDanosDTO coberturaDTO) {
		this.coberturaDTO = coberturaDTO;
	}

	public CoberturaSoporteDanosDTO getCoberturaDTO() {
		return coberturaDTO;
	}

	public void setLineaSoporteReaseguroDTO(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO) {
		this.lineaSoporteReaseguroDTO = lineaSoporteReaseguroDTO;
	}

	public LineaSoporteReaseguroDTO getLineaSoporteReaseguroDTO() {
		return lineaSoporteReaseguroDTO;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public double getMonto() {
		return monto;
	}

	public void setMontoRetenido(double montoRetenido) {
		this.montoRetenido = montoRetenido;
	}

	public double getMontoRetenido() {
		return montoRetenido;
	}

	public void setMontoCuotaParte(double montoCuotaParte) {
		this.montoCuotaParte = montoCuotaParte;
	}

	public double getMontoCuotaParte() {
		return montoCuotaParte;
	}

	public void setMontoPrimerExcedente(double montoPrimerExcedente) {
		this.montoPrimerExcedente = montoPrimerExcedente;
	}

	public double getMontoPrimerExcedente() {
		return montoPrimerExcedente;
	}

	public void setMontoFacultativo(double montoFacultativo) {
		this.montoFacultativo = montoFacultativo;
	}

	public double getMontoFacultativo() {
		return montoFacultativo;
	} 		
}
