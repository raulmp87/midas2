package mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad;

//default package

import java.math.BigDecimal;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
* ContabilidadOrdenPago entity. @author MyEclipse Persistence Tools
*/
@Entity
@Table(name = "TOCONTABILIDADORDENPAGO", schema = "MIDAS")
public class ContabilidadOrdenPagoDTO implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ContabilidadOrdenPagoId id;
	private BigDecimal idRamo;
	private BigDecimal idPrestadorServicio;
	private String conceptoPol;
	private BigDecimal idMoneda;
	private Double tipoCambio;	
	private Integer auxiliar;
	private String ccosto;
	private String tipoPago;
	private Double impNeto;
	private Double impIva;
	private Double impIvaRet;
	private Double impIsrRet;	
	private Double impOtrosImpuestos;	
	private String usuario;	
	private BigDecimal estatus;	
    private String pTransCont;
    private String pBeneficiario;
    private Short cptoPago;
    private BigDecimal idAgente;
    private Double impDeducible;
    private Double impCoaseguro;
    private Double impSalvamento;
    private Double impPrimasCob;
    
	
	/** default constructor */
	public ContabilidadOrdenPagoDTO() {
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToOrdenPago", column = @Column(name = "IDTOORDENPAGO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idSubr", column = @Column(name = "ID_SUBR", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToAutorizacionTecnica", column = @Column(name = "IDTOAUTORIZACIONTECNICA", nullable = false, precision = 22, scale = 0))})
			
			
	public ContabilidadOrdenPagoId getId() {
		return this.id;
	}

	public void setId(ContabilidadOrdenPagoId id) {
		this.id = id;
	}

	@Column(name = "ID_RAMO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdRamo() {
		return this.idRamo;
	}

	public void setIdRamo(BigDecimal idRamo) {
		this.idRamo = idRamo;
	}

	@Column(name = "IDPRESTADORSERVICIO", precision = 22, scale = 0)
	public BigDecimal getIdPrestadorServicio() {
		return this.idPrestadorServicio;
	}

	public void setIdPrestadorServicio(BigDecimal idPrestadorServicio) {
		this.idPrestadorServicio = idPrestadorServicio;
	}

	@Column(name = "CONCEPTO_POL", nullable = false, length = 240)
	public String getConceptoPol() {
		return this.conceptoPol;
	}

	public void setConceptoPol(String conceptoPol) {
		this.conceptoPol = conceptoPol;
	}

	@Column(name = "ID_MONEDA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdMoneda() {
		return this.idMoneda;
	}

	public void setIdMoneda(BigDecimal idMoneda) {
		this.idMoneda = idMoneda;
	}

	@Column(name = "TIPO_CAMBIO", nullable = false, precision = 14)
	public Double getTipoCambio() {
		return this.tipoCambio;
	}

	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	
	@Column(name = "AUXILIAR", nullable = false, precision = 5, scale = 0)
	public Integer getAuxiliar() {
		return this.auxiliar;
	}

	public void setAuxiliar(Integer auxiliar) {
		this.auxiliar = auxiliar;
	}

	@Column(name = "CCOSTO", nullable = false, length = 5)
	public String getCcosto() {
		return this.ccosto;
	}

	public void setCcosto(String ccosto) {
		this.ccosto = ccosto;
	}

	@Column(name = "TIPO_PAGO", length = 2)
	public String getTipoPago() {
		return this.tipoPago;
	}

	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}

	@Column(name = "IMP_NETO", nullable = false, precision = 14)
	public Double getImpNeto() {
		return this.impNeto;
	}

	public void setImpNeto(Double impNeto) {
		this.impNeto = impNeto;
	}

	@Column(name = "IMP_IVA", nullable = false, precision = 14)
	public Double getImpIva() {
		return this.impIva;
	}

	public void setImpIva(Double impIva) {
		this.impIva = impIva;
	}

	@Column(name = "IMP_IVA_RET", nullable = false, precision = 14)
	public Double getImpIvaRet() {
		return this.impIvaRet;
	}

	public void setImpIvaRet(Double impIvaRet) {
		this.impIvaRet = impIvaRet;
	}

	@Column(name = "IMP_ISR_RET", nullable = false, precision = 14)
	public Double getImpIsrRet() {
		return this.impIsrRet;
	}

	public void setImpIsrRet(Double impIsrRet) {
		this.impIsrRet = impIsrRet;
	}

	@Column(name = "IMP_OTROS_IMPUESTOS", nullable = false, precision = 14)
	public Double getImpOtrosImpuestos() {
		return this.impOtrosImpuestos;
	}

	public void setImpOtrosImpuestos(Double impOtrosImpuestos) {
		this.impOtrosImpuestos = impOtrosImpuestos;
	}

	@Column(name = "USUARIO", nullable = false, length = 8)
	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	@Column(name = "ESTATUS", nullable = false, precision = 22, scale = 0)
	public BigDecimal getEstatus() {
		return this.estatus;
	}

	public void setEstatus(BigDecimal estatus) {
		this.estatus = estatus;
	}
	
    @Column(name="PTRANS_CONT", nullable=false, length=30)
    public String getPtransCont() {
        return this.pTransCont;
    }
    
    public void setPtransCont(String pTransCont) {
        this.pTransCont = pTransCont;
    }
	
    @Column(name="PBENEFICIARIO", length=30)
    public String getPBeneficiario() {
        return this.pBeneficiario;
    }
    
    public void setPBeneficiario(String pBeneficiario) {
        this.pBeneficiario = pBeneficiario;
    }    	
    
    @Column(name="CPTO_PAGO", precision=4, scale=0)
    public Short getCptoPago() {
        return this.cptoPago;
    }
    
    public void setCptoPago(Short cptoPago) {
        this.cptoPago = cptoPago;
    }
    
    @Column(name="IDAGENTE", precision=22, scale=0)
    public BigDecimal getIdAgente() {
        return this.idAgente;
    }
    
    public void setIdAgente(BigDecimal idAgente) {
        this.idAgente = idAgente;
    }    
    
    @Column(name = "IMP_DEDUCIBLE", nullable = false, precision = 14)
    public Double getImpDeducible() {
		return impDeducible;
	}

    public void setImpDeducible(Double impDeducible) {
		this.impDeducible = impDeducible;
	}    
    
    @Column(name = "IMP_COASEGURO", nullable = false, precision = 14)   
    public Double getImpCoaseguro() {
		return impCoaseguro;
	}

    public void setImpCoaseguro(Double impCoaseguro) {
		this.impCoaseguro = impCoaseguro;
	}   
    
    @Column(name = "IMP_SALVAMENTO", nullable = false, precision = 14)
    public Double getImpSalvamento() {
		return impSalvamento;
	}

    public void setImpSalvamento(Double impSalvamento) {
		this.impSalvamento = impSalvamento;
	}    
    
    @Column(name = "IMP_PRIMAS_COB", nullable = false, precision = 14)
    public Double getImpPrimasCob() {
		return impPrimasCob;
	}
    
    public void setImpPrimasCob(Double impPrimasCob) {
		this.impPrimasCob = impPrimasCob;
	}    
    
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("");
		sb.append("ContabilidadOrdenPagoDTO { " + '\n');
		sb.append(id.toString());
		
		if (idRamo != null) {
			sb.append("idRamo = " + idRamo.toString() + '\n');
		} else {
			sb.append("idRamo = null " + '\n');
		}

		if (idPrestadorServicio != null) {
			sb.append("idPrestadorServicio = " + idPrestadorServicio.toString()	+ '\n');
		} else {
			sb.append("idPrestadorServicio = null " + '\n');
		}

		sb.append("conceptoPol = " + conceptoPol + '\n');

		if (idMoneda != null) {
			sb.append("idMoneda = " + idMoneda.toString() + '\n');
		} else {
			sb.append("idMoneda = null " + '\n');
		}

		if (tipoCambio != null) {
			sb.append("tipoCambio = " + tipoCambio.toString() + '\n');
		} else {
			sb.append("tipoCambio = null " + '\n');
		}		

		if (auxiliar != null) {
			sb.append("auxiliar = " + auxiliar.toString() + '\n');
		} else {
			sb.append("auxiliar = null " + '\n');
		}

		sb.append("ccosto = " + ccosto + '\n');		
		sb.append("tipoPago = " + tipoPago + '\n');

		if (impNeto != null) {
			sb.append("impNeto = " + impNeto.toString() + '\n');
		} else {
			sb.append("impNeto = null " + '\n');
		}
		
		if (impIva != null) {
			sb.append("impIva = " + impIva.toString() + '\n');
		} else {
			sb.append("impIva = null " + '\n');
		}
		
		if (impIvaRet != null) {
			sb.append("impIvaRet = " + impIvaRet.toString() + '\n');
		} else {
			sb.append("impIvaRet = null " + '\n');
		}
		
		if (impIsrRet != null) {
			sb.append("impIsrRet = " + impIsrRet.toString() + '\n');
		} else {
			sb.append("impIsrRet = null " + '\n');
		}
		
		if (impOtrosImpuestos != null) {
			sb.append("impOtrosImpuestos = " + impOtrosImpuestos.toString()
					+ '\n');
		} else {
			sb.append("impOtrosImpuestos = null " + '\n');
		}

		sb.append("usuario = " + usuario + '\n');
		
		if (estatus != null) {
			sb.append("estatus = " + estatus.toString() + '\n');
		} else {
			sb.append("estatus = null " + '\n');
		}

		sb.append("pTransCont = " + pTransCont + '\n');
		sb.append("pBeneficiario = " + pBeneficiario + '\n');
		sb.append("cptoPago = " + cptoPago + '\n');
		
		if (idAgente != null) {
			sb.append("idAgente = " + idAgente.toString() + '\n');
		} else {
			sb.append("idAgente = null " + '\n');
		}
		
		if (impDeducible != null) {
			sb.append("impDeducible = " + impDeducible.toString() + '\n');
		} else {
			sb.append("impDeducible = null " + '\n');
		}
		
		if (impCoaseguro != null) {
			sb.append("impCoaseguro = " + impCoaseguro.toString() + '\n');
		} else {
			sb.append("impCoaseguro = null " + '\n');
		}

		if (impSalvamento != null) {
			sb.append("impSalvamento = " + impSalvamento.toString() + '\n');
		} else {
			sb.append("impSalvamento = null " + '\n');
		}		
		
		if (impPrimasCob != null) {
			sb.append("impPrimasCob = " + impPrimasCob.toString() + '\n');
		} else {
			sb.append("impPrimasCob = null " + '\n');
		}		
		
		sb.append("}" + '\n');

		return sb.toString();
	}
}