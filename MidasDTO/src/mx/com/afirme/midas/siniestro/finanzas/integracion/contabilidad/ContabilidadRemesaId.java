package mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * ContabilidadRemesaId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class ContabilidadRemesaId implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields

	private BigDecimal idSubr;
	private String cveConceptoMov;
	private BigDecimal idToAplicacionIngreso;

	// Constructors

	/** default constructor */
	public ContabilidadRemesaId() {
	}


	@Column(name = "ID_SUBR", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdSubr() {
		return this.idSubr;
	}

	public void setIdSubr(BigDecimal idSubr) {
		this.idSubr = idSubr;
	}

	@Column(name = "CVE_CONCEPTO_MOV", nullable = false, length = 2)
	public String getCveConceptoMov() {
		return this.cveConceptoMov;
	}

	public void setCveConceptoMov(String cveConceptoMov) {
		this.cveConceptoMov = cveConceptoMov;
	}

	@Column(name = "IDTOAPLICACIONINGRESO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToAplicacionIngreso() {
		return this.idToAplicacionIngreso;
	}

	public void setIdToAplicacionIngreso(BigDecimal idToAplicacionIngreso) {
		this.idToAplicacionIngreso = idToAplicacionIngreso;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ContabilidadRemesaId))
			return false;
		ContabilidadRemesaId castOther = (ContabilidadRemesaId) other;

		return ((this.getIdSubr() == castOther.getIdSubr()) || (this
				.getIdSubr() != null
				&& castOther.getIdSubr() != null && this.getIdSubr().equals(
				castOther.getIdSubr())))
				&& ((this.getCveConceptoMov() == castOther.getCveConceptoMov()) || (this
						.getCveConceptoMov() != null
						&& castOther.getCveConceptoMov() != null && this
						.getCveConceptoMov().equals(
								castOther.getCveConceptoMov())))
				&& ((this.getIdToAplicacionIngreso() == castOther
						.getIdToAplicacionIngreso()) || (this
						.getIdToAplicacionIngreso() != null
						&& castOther.getIdToAplicacionIngreso() != null && this
						.getIdToAplicacionIngreso().equals(
								castOther.getIdToAplicacionIngreso())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getIdSubr() == null ? 0 : this.getIdSubr().hashCode());
		result = 37
				* result
				+ (getCveConceptoMov() == null ? 0 : this.getCveConceptoMov()
						.hashCode());
		result = 37
				* result
				+ (getIdToAplicacionIngreso() == null ? 0 : this
						.getIdToAplicacionIngreso().hashCode());
		return result;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		if(cveConceptoMov != null){
			sb.append("cveConceptoMov = " + cveConceptoMov.toString() + '\n');
		}else{
			sb.append("cveConceptoMov = null " + '\n');
		}
		
		if(idSubr != null){
			sb.append("idSubr = " + idSubr.toString() + '\n');
		}else{
			sb.append("idSubr = null " + '\n');
		}
		
		if(idToAplicacionIngreso != null){
			sb.append("idtoaplicacioningreso = " + idToAplicacionIngreso.toString() + '\n');
		}else{
			sb.append("idtoaplicacioningreso = null " + '\n');
		}
		
		return sb.toString();
	}	
}