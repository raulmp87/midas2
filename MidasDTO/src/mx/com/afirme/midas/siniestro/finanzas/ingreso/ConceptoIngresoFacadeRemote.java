package mx.com.afirme.midas.siniestro.finanzas.ingreso;

import java.util.List;
import java.math.BigDecimal;
import javax.ejb.Remote;

/**
 * Remote interface for ConceptoIngresoFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ConceptoIngresoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved ConceptoIngreso entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            ConceptoIngreso entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ConceptoIngresoDTO entity);

	/**
	 * Delete a persistent ConceptoIngreso entity.
	 * 
	 * @param entity
	 *            ConceptoIngreso entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ConceptoIngresoDTO entity);

	/**
	 * Persist a previously saved ConceptoIngreso entity and return it or a copy
	 * of it to the sender. A copy of the ConceptoIngreso entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            ConceptoIngreso entity to update
	 * @return ConceptoIngreso the persisted ConceptoIngreso entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ConceptoIngresoDTO update(ConceptoIngresoDTO entity);

	public ConceptoIngresoDTO findById(BigDecimal id);

	/**
	 * Find all ConceptoIngreso entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ConceptoIngreso property to query
	 * @param value
	 *            the property value to match
	 * @return List<ConceptoIngreso> found by query
	 */
	public List<ConceptoIngresoDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all ConceptoIngreso entities.
	 * 
	 * @return List<ConceptoIngreso> all ConceptoIngreso entities
	 */
	public List<ConceptoIngresoDTO> findAll();
}