package mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad;

//default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * ContabilidadOrdenPagoId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class ContabilidadOrdenPagoId implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigDecimal idToOrdenPago;
	private BigDecimal idSubr;
	private BigDecimal idToAutorizacionTecnica;

	// Constructors

	/** default constructor */
	public ContabilidadOrdenPagoId() {
	}

	@Column(name = "IDTOORDENPAGO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToOrdenPago() {
		return this.idToOrdenPago;
	}

	public void setIdToOrdenPago(BigDecimal idToOrdenPago) {
		this.idToOrdenPago = idToOrdenPago;
	}

	@Column(name = "ID_SUBR", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdSubr() {
		return this.idSubr;
	}

	public void setIdSubr(BigDecimal idSubr) {
		this.idSubr = idSubr;
	}

	@Column(name = "IDTOAUTORIZACIONTECNICA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToAutorizacionTecnica() {
		return this.idToAutorizacionTecnica;
	}

	public void setIdToAutorizacionTecnica(BigDecimal idToAutorizacionTecnica) {
		this.idToAutorizacionTecnica = idToAutorizacionTecnica;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ContabilidadOrdenPagoId))
			return false;
		ContabilidadOrdenPagoId castOther = (ContabilidadOrdenPagoId) other;

		return ((this.getIdToOrdenPago() == castOther.getIdToOrdenPago()) || (this
				.getIdToOrdenPago() != null
				&& castOther.getIdToOrdenPago() != null && this
				.getIdToOrdenPago().equals(castOther.getIdToOrdenPago())))
				&& ((this.getIdSubr() == castOther.getIdSubr()) || (this
						.getIdSubr() != null
						&& castOther.getIdSubr() != null && this.getIdSubr()
						.equals(castOther.getIdSubr())))
				&& ((this.getIdToAutorizacionTecnica() == castOther
						.getIdToAutorizacionTecnica()) || (this
						.getIdToAutorizacionTecnica() != null
						&& castOther.getIdToAutorizacionTecnica() != null && this
						.getIdToAutorizacionTecnica().equals(
								castOther.getIdToAutorizacionTecnica())));

	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToOrdenPago() == null ? 0 : this.getIdToOrdenPago()
						.hashCode());

		result = 37 * result
				+ (getIdSubr() == null ? 0 : this.getIdSubr().hashCode());

		result = 37
				* result
				+ (getIdToAutorizacionTecnica() == null ? 0 : this
						.getIdToAutorizacionTecnica().hashCode());
		return result;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		if (idToOrdenPago != null) {
			sb.append("idToOrdenPago = " + idToOrdenPago.toString() + '\n');
		} else {
			sb.append("idToOrdenPago = null " + '\n');
		}

		if (idSubr != null) {
			sb.append("idSubr = " + idSubr.toString() + '\n');
		} else {
			sb.append("idSubr = null " + '\n');
		}

		if (idToAutorizacionTecnica != null) {
			sb.append("idToAutorizacionTecnica = "
					+ idToAutorizacionTecnica.toString() + '\n');
		} else {
			sb.append("idToAutorizacionTecnica = null " + '\n');
		}

		return sb.toString();
	}
}