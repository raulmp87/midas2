package mx.com.afirme.midas.siniestro.finanzas;
// default package

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;

/**
 * RiesgoAfectadoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TORIESGOAFECTADO", schema = "MIDAS")
public class RiesgoAfectadoDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private RiesgoAfectadoId id;
	private ReporteSiniestroDTO reporteSiniestroDTO;
	private Date fechaAfectacion;
	private Double sumaAsegurada;
	private List<ReservaDetalleDTO> reservaDetalleDTOs = new ArrayList<ReservaDetalleDTO>();
	private Byte estatus;
	
	private String descripcionTipoSA;

	public static final Byte ACTIVO = 1;
	public static final Byte INACTIVO = 0;
	/** default constructor */
	public RiesgoAfectadoDTO() {
	}

		// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idtoreportesiniestro", column = @Column(name = "IDTOREPORTESINIESTRO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idtopoliza", column = @Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroinciso", column = @Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idtoseccion", column = @Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idtocobertura", column = @Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idtoriesgo", column = @Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numerosubinciso", column = @Column(name = "NUMEROSUBINCISO", nullable = false, precision = 22, scale = 0)) })
	public RiesgoAfectadoId getId() {
		return this.id;
	}

	public void setId(RiesgoAfectadoId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOREPORTESINIESTRO", nullable = false, insertable = false, updatable = false)
	public ReporteSiniestroDTO getReporteSiniestroDTO() {
//	public ReporteSiniestroDTO getToReporteSiniestro() {
		return this.reporteSiniestroDTO;
	}

	public void setReporteSiniestroDTO(ReporteSiniestroDTO toReporteSiniestro) {
//	public void setToReporteSiniestro(ReporteSiniestroDTO toReporteSiniestro) {
		this.reporteSiniestroDTO = toReporteSiniestro;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAAFECTACION", nullable = false, length = 7)
	public Date getFechaAfectacion() {
		return this.fechaAfectacion;
	}

	public void setFechaAfectacion(Date fechaAfectacion) {
		this.fechaAfectacion = fechaAfectacion;
	}

	@Column(name = "SUMAASEGURADA", nullable = false, precision = 16)
	public Double getSumaAsegurada() {
		return this.sumaAsegurada;
	}

	public void setSumaAsegurada(Double sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "riesgoAfectadoDTO")
	public List<ReservaDetalleDTO> getReservaDetalleDTOs() {
//	public List<ReservaDetalleDTO> getReservaDetalles() {
		return this.reservaDetalleDTOs;
	}

	public void setReservaDetalleDTOs(List<ReservaDetalleDTO> reservaDetalleDTOs) {
//	public void setReservaDetalles(List<ReservaDetalleDTO> reservaDetalleDTOs) {
		this.reservaDetalleDTOs = reservaDetalleDTOs;
	}

	@Column(name="ESTATUS", nullable=false, precision=22, scale=0)
    public Byte getEstatus() {
        return this.estatus;
    }
    
    public void setEstatus(Byte estatus) {
        this.estatus = estatus;
    }

	@Transient
	public String getDescripcionTipoSA() {
		return descripcionTipoSA;
	}

	public void setDescripcionTipoSA(String descripcionTipoSA) {
		this.descripcionTipoSA = descripcionTipoSA;
	}
	
	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof RiesgoAfectadoDTO) {
			RiesgoAfectadoDTO riesgoAfectadoDTO = (RiesgoAfectadoDTO) object;
			equal = riesgoAfectadoDTO.getId().getIdtoreportesiniestro().equals(this.getId().getIdtoreportesiniestro())
			&& riesgoAfectadoDTO.getId().getIdtopoliza().equals(this.getId().getIdtopoliza())
			&& riesgoAfectadoDTO.getId().getNumeroinciso().equals(this.getId().getNumeroinciso()) 
			&& riesgoAfectadoDTO.getId().getNumerosubinciso().equals(this.getId().getNumerosubinciso())
			&& riesgoAfectadoDTO.getId().getIdtoseccion().equals(this.getId().getIdtoseccion())
			&& riesgoAfectadoDTO.getId().getIdtocobertura().equals(this.getId().getIdtocobertura())
			&& riesgoAfectadoDTO.getId().getIdtoriesgo().equals(this.getId().getIdtoriesgo());
		} // End of if
		return equal;
	}

	
}