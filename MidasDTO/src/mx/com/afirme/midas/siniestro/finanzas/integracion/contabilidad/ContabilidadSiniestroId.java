package mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad;

// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * ContabilidadSiniestroId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class ContabilidadSiniestroId implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToReporteSiniestro;
	private BigDecimal idTcMovimientoSiniestro;
	private BigDecimal idRegistro;
	private BigDecimal idSubr;
	private String cveConceptoMov;

	// Constructors

	/** default constructor */
	public ContabilidadSiniestroId() {
	}

	/** full constructor */
	public ContabilidadSiniestroId(BigDecimal idToReporteSiniestro,
			BigDecimal idTcMovimientoSiniestro, BigDecimal idRegistro,
			BigDecimal idSubr, String cveConceptoMov) {
		this.idToReporteSiniestro = idToReporteSiniestro;
		this.idTcMovimientoSiniestro = idTcMovimientoSiniestro;
		this.idRegistro = idRegistro;
		this.idSubr = idSubr;
		this.cveConceptoMov = cveConceptoMov;
	}

	// Property accessors
	@Column(name = "IDTOREPORTESINIESTRO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToReporteSiniestro() {
		return this.idToReporteSiniestro;
	}

	public void setIdToReporteSiniestro(BigDecimal idToReporteSiniestro) {
		this.idToReporteSiniestro = idToReporteSiniestro;
	}

	@Column(name = "IDTCMOVIMIENTOSINIESTRO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcMovimientoSiniestro() {
		return this.idTcMovimientoSiniestro;
	}

	public void setIdTcMovimientoSiniestro(BigDecimal idTcMovimientoSiniestro) {
		this.idTcMovimientoSiniestro = idTcMovimientoSiniestro;
	}

	@Column(name = "IDREGISTRO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdRegistro() {
		return this.idRegistro;
	}

	public void setIdRegistro(BigDecimal idRegistro) {
		this.idRegistro = idRegistro;
	}

	@Column(name = "ID_SUBR", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdSubr() {
		return this.idSubr;
	}

	public void setIdSubr(BigDecimal idSubr) {
		this.idSubr = idSubr;
	}
	
	@Column(name = "CVE_CONCEPTO_MOV", nullable = false, length = 2)
	public String getCveConceptoMov() {
		return this.cveConceptoMov;
	}

	public void setCveConceptoMov(String cveConceptoMov) {
		this.cveConceptoMov = cveConceptoMov;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ContabilidadSiniestroId))
			return false;
		ContabilidadSiniestroId castOther = (ContabilidadSiniestroId) other;

		return ((this.getIdToReporteSiniestro() == castOther
				.getIdToReporteSiniestro()) || (this.getIdToReporteSiniestro() != null
				&& castOther.getIdToReporteSiniestro() != null && this
				.getIdToReporteSiniestro().equals(
						castOther.getIdToReporteSiniestro())))
				&& ((this.getIdTcMovimientoSiniestro() == castOther
						.getIdTcMovimientoSiniestro()) || (this
						.getIdTcMovimientoSiniestro() != null
						&& castOther.getIdTcMovimientoSiniestro() != null && this
						.getIdTcMovimientoSiniestro().equals(
								castOther.getIdTcMovimientoSiniestro())))
				&& ((this.getIdRegistro() == castOther.getIdRegistro()) || (this
						.getIdRegistro() != null
						&& castOther.getIdRegistro() != null && this
						.getIdRegistro().equals(castOther.getIdRegistro())))
				&& ((this.getIdSubr() == castOther.getIdSubr()) || (this
						.getIdSubr() != null
						&& castOther.getIdSubr() != null && this.getIdSubr()
						.equals(castOther.getIdSubr()))
				&& ((this.getCveConceptoMov() == castOther.getCveConceptoMov()) || (this
						.getCveConceptoMov() != null
						&& castOther.getCveConceptoMov() != null && this
						.getCveConceptoMov().equals(
								castOther.getCveConceptoMov()))));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToReporteSiniestro() == null ? 0 : this
						.getIdToReporteSiniestro().hashCode());
		result = 37
				* result
				+ (getIdTcMovimientoSiniestro() == null ? 0 : this
						.getIdTcMovimientoSiniestro().hashCode());
		result = 37
				* result
				+ (getIdRegistro() == null ? 0 : this.getIdRegistro()
						.hashCode());
		result = 37 * result
				+ (getIdSubr() == null ? 0 : this.getIdSubr().hashCode());
		
		result = 37	* result
			+ (getCveConceptoMov() == null ? 0 : this.getCveConceptoMov().hashCode());
		return result;
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();

		if(idToReporteSiniestro != null){
			sb.append("idToReporteSiniestro = " + idToReporteSiniestro.toString() + '\n');
		}else{
			sb.append("idToReporteSiniestro = null " + '\n');
		}
		if(idTcMovimientoSiniestro != null){
			sb.append("idTcMovimientoSiniestro = " + idTcMovimientoSiniestro.toString() + '\n');
		}else{
			sb.append("idTcMovimientoSiniestro = null " + '\n');
		}
		if(idRegistro != null){
			sb.append("idRegistro = " + idRegistro.toString() + '\n');
		}else{
			sb.append("idRegistro = null " + '\n');
		}
		if(idSubr != null){
			sb.append("idSubr = " + idSubr.toString() + '\n');
		}else{
			sb.append("idSubr = null " + '\n');
		}	
		if(cveConceptoMov != null){
			sb.append("cveConceptoMov = " + cveConceptoMov.toString() + '\n');
		}else{
			sb.append("cveConceptoMov = null " + '\n');
		}	
		
		return sb.toString();
	}
	
}