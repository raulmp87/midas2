package mx.com.afirme.midas.siniestro.finanzas;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TCESTATUSFINANZAS", schema = "MIDAS")
public class EstatusFinanzasDTO implements java.io.Serializable{

	// Fields
	private static final long serialVersionUID = 1L;
	private Short idTcEstatusfinanzas;
	private String descripcion;
	
	public static final Short ABIERTO = 1;
	public static final Short POR_AUTORIZAR = 2;
	public static final Short AUTORIZADO = 3;
	public static final Short CANCELADO = 4;
	public static final Short ORDEN_PAGO = 5;
	public static final Short PAGADO = 6;

	// Constructors

	/** default constructor */
	public EstatusFinanzasDTO() {
	}


	// Property accessors
	@Id
	@Column(name = "IDTCESTATUSFINANZAS", nullable = false, precision = 22, scale = 0)
	public Short getIdTcEstatusfinanzas() {
		return this.idTcEstatusfinanzas;
	}

	public void setIdTcEstatusfinanzas(Short idTcEstatusfinanzas) {
		this.idTcEstatusfinanzas = idTcEstatusfinanzas;
	}

	@Column(name = "DESCRIPCION", nullable = false, length = 50)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
