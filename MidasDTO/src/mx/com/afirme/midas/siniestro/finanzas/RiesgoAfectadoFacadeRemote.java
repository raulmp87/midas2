package mx.com.afirme.midas.siniestro.finanzas;
// default package

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;

/**
 * Remote interface for RiesgoAfectadoFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface RiesgoAfectadoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved RiesgoAfectadoDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            RiesgoAfectadoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(RiesgoAfectadoDTO entity);

	/**
	 * Delete a persistent RiesgoAfectadoDTO entity.
	 * 
	 * @param entity
	 *            RiesgoAfectadoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(RiesgoAfectadoDTO entity);

	/**
	 * Persist a previously saved RiesgoAfectadoDTO entity and return it or a copy
	 * of it to the sender. A copy of the RiesgoAfectadoDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            RiesgoAfectadoDTO entity to update
	 * @return RiesgoAfectadoDTO the persisted RiesgoAfectadoDTO entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public RiesgoAfectadoDTO update(RiesgoAfectadoDTO entity);

	public RiesgoAfectadoDTO findById(RiesgoAfectadoId id);

	/**
	 * Find all RiesgoAfectadoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the RiesgoAfectadoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<RiesgoAfectadoDTO> found by query
	 */
	public List<RiesgoAfectadoDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all RiesgoAfectadoDTO entities.
	 * 
	 * @return List<RiesgoAfectadoDTO> all RiesgoAfectadoDTO entities
	 */
	public List<RiesgoAfectadoDTO> findAll();
	
	public List<RiesgoAfectadoDTO> listarFiltrado(RiesgoAfectadoId id);
	
	public Double sumaAseguradaPorReporte(BigDecimal idReporteSiniestro);
	
	public List<SubRamoDTO> getSubramosFromReservaEstimada(BigDecimal idToReservaEstimada);
	
	public Double getSumaAseguradaPorReporteYSubramo(BigDecimal idReporteSiniestro, BigDecimal idSubramo);
	
	public Double getPorcentajeSumaAseguradaPorReporteYSubramo(BigDecimal idReporteSiniestro, BigDecimal idSubramo);
	
	public BigDecimal getIncisoPorReportePoliza(BigDecimal idToReporteSiniestro, BigDecimal idToPoliza);
	
	public List<RiesgoAfectadoId> getSubIncisoSeccionPorReportePoliza(BigDecimal idToReporteSiniestro, BigDecimal idToPoliza,BigDecimal numeroInciso);
	
	public List<RiesgoAfectadoDTO> getCoberturasRiesgoPorReporte(BigDecimal idToReporteSiniestro);
	
	public List<RiesgoAfectadoDTO> getCoberturasRiesgoPorReporte(BigDecimal idToReporteSiniestro, Byte estatus);
}