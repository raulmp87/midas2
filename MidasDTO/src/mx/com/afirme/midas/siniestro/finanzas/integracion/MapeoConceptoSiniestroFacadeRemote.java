package mx.com.afirme.midas.siniestro.finanzas.integracion;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for MapeoConceptoSiniestroFacade.
 * @author MyEclipse Persistence Tools
 */


public interface MapeoConceptoSiniestroFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved MapeoConceptoSiniestro entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity MapeoConceptoSiniestro entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(MapeoConceptoSiniestroDTO entity);
    /**
	 Delete a persistent MapeoConceptoSiniestro entity.
	  @param entity MapeoConceptoSiniestro entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(MapeoConceptoSiniestroDTO entity);
   /**
	 Persist a previously saved MapeoConceptoSiniestro entity and return it or a copy of it to the sender. 
	 A copy of the MapeoConceptoSiniestro entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity MapeoConceptoSiniestro entity to update
	 @return MapeoConceptoSiniestro the persisted MapeoConceptoSiniestro entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public MapeoConceptoSiniestroDTO update(MapeoConceptoSiniestroDTO entity);
	public MapeoConceptoSiniestroDTO findById( MapeoConceptoSiniestroId id);
	 /**
	 * Find all MapeoConceptoSiniestro entities with a specific property value.  
	 
	  @param propertyName the name of the MapeoConceptoSiniestro property to query
	  @param value the property value to match
	  	  @return List<MapeoConceptoSiniestro> found by query
	 */
	public List<MapeoConceptoSiniestroDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all MapeoConceptoSiniestro entities.
	  	  @return List<MapeoConceptoSiniestro> all MapeoConceptoSiniestro entities
	 */
	public List<MapeoConceptoSiniestroDTO> findAll();
	
	/**
	 * Find a MapeoConceptoSiniestro entity which matches idConcepto and conceptos's values
	  	  @return MapeoConceptoSiniestro MapeoConceptoSiniestro entity
	 */	
	public MapeoConceptoSiniestroDTO getMapeoByIdConcepto(BigDecimal idConcepto, BigDecimal concepto);
}