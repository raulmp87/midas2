package mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad;

import java.math.BigDecimal;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.siniestro.finanzas.ingreso.AplicacionIngresoDTO;

/**
 * ContabilidadRemesa entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOCONTABILIDADREMESA", schema = "MIDAS")
public class ContabilidadRemesaDTO implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = 1L;
	private ContabilidadRemesaId id;
	private BigDecimal idToReporteSiniestro;
	private BigDecimal idRamo;
	private String conceptoPol;
	private BigDecimal idMoneda;
	private Double tipoCambio;
	private Integer auxiliar;
	private String cCosto;
	private Double impNeto;
	private String conceptoMov;
	private Double impIva;
	private Double impIvaRet;
	private Double impIsrRet;
	private Double impOtrosImpuestos;
	private Double impIsr;
	private String usuario;
	private BigDecimal idPolizaContable;
	
	private AplicacionIngresoDTO aplicacionIngresoDTO;

	// Constructors

	/** default constructor */
	public ContabilidadRemesaDTO() {
	}
	
	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
		@AttributeOverride(name = "idSubr", column = @Column(name = "ID_SUBR", nullable = false, precision = 22, scale = 0)),
		@AttributeOverride(name = "cveConceptoMov", column = @Column(name = "CVE_CONCEPTO_MOV", nullable = false, length = 2)),
		@AttributeOverride(name = "idToAplicacionIngreso", column = @Column(name = "IDTOAPLICACIONINGRESO", nullable = false, precision = 22, scale = 0)) })
	public ContabilidadRemesaId getId() {
		return this.id;
	}

	public void setId(ContabilidadRemesaId id) {
		this.id = id;
	}

	@Column(name = "IDTOREPORTESINIESTRO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToReporteSiniestro() {
		return this.idToReporteSiniestro;
	}

	public void setIdToReporteSiniestro(BigDecimal idToReporteSiniestro) {
		this.idToReporteSiniestro = idToReporteSiniestro;
	}

	@Column(name = "ID_RAMO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdRamo() {
		return this.idRamo;
	}

	public void setIdRamo(BigDecimal idRamo) {
		this.idRamo = idRamo;
	}

	@Column(name = "CONCEPTO_POL", nullable = false, length = 100)
	public String getConceptoPol() {
		return this.conceptoPol;
	}

	public void setConceptoPol(String conceptoPol) {
		this.conceptoPol = conceptoPol;
	}

	@Column(name = "ID_MONEDA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdMoneda() {
		return this.idMoneda;
	}

	public void setIdMoneda(BigDecimal idMoneda) {
		this.idMoneda = idMoneda;
	}

	@Column(name = "TIPO_CAMBIO", nullable = false, precision = 14)
	public Double getTipoCambio() {
		return this.tipoCambio;
	}

	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	@Column(name = "AUXILIAR", nullable = false, precision = 5, scale = 0)
	public Integer getAuxiliar() {
		return this.auxiliar;
	}

	public void setAuxiliar(Integer auxiliar) {
		this.auxiliar = auxiliar;
	}

	@Column(name = "CCOSTO", nullable = false, length = 5)
	public String getCcosto() {
		return this.cCosto;
	}

	public void setCcosto(String ccosto) {
		this.cCosto = ccosto;
	}

	@Column(name = "IMP_NETO", nullable = false, precision = 14)
	public Double getImpNeto() {
		return this.impNeto;
	}

	public void setImpNeto(Double impNeto) {
		this.impNeto = impNeto;
	}

	@Column(name = "CONCEPTO_MOV", nullable = false, length = 100)
	public String getConceptoMov() {
		return this.conceptoMov;
	}

	public void setConceptoMov(String conceptoMov) {
		this.conceptoMov = conceptoMov;
	}

	@Column(name = "IMP_IVA", nullable = false, precision = 14)
	public Double getImpIva() {
		return this.impIva;
	}

	public void setImpIva(Double impIva) {
		this.impIva = impIva;
	}

	@Column(name = "IMP_IVA_RET", nullable = false, precision = 14)
	public Double getImpIvaRet() {
		return this.impIvaRet;
	}

	public void setImpIvaRet(Double impIvaRet) {
		this.impIvaRet = impIvaRet;
	}

	@Column(name = "IMP_ISR_RET", nullable = false, precision = 14)
	public Double getImpIsrRet() {
		return this.impIsrRet;
	}

	public void setImpIsrRet(Double impIsrRet) {
		this.impIsrRet = impIsrRet;
	}

	@Column(name = "IMP_OTROS_IMPUESTOS", nullable = false, precision = 14)
	public Double getImpOtrosImpuestos() {
		return this.impOtrosImpuestos;
	}

	public void setImpOtrosImpuestos(Double impOtrosImpuestos) {
		this.impOtrosImpuestos = impOtrosImpuestos;
	}

	@Column(name = "IMP_ISR", nullable = false, precision = 14)
	public Double getImpIsr() {
		return this.impIsr;
	}

	public void setImpIsr(Double impIsr) {
		this.impIsr = impIsr;
	}

	@Column(name = "USUARIO", nullable = false, length = 8)
	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	
	@Column(name = "IDPOLIZACONTABLE", precision = 22, scale = 0)
	public BigDecimal getIdPolizaContable() {
		return this.idPolizaContable;
	}

	public void setIdPolizaContable(BigDecimal idPolizaContable) {
		this.idPolizaContable = idPolizaContable;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOAPLICACIONINGRESO", nullable = false, insertable = false, updatable = false)
	public AplicacionIngresoDTO getAplicacionIngresoDTO() {
		return this.aplicacionIngresoDTO;
	}

	public void setAplicacionIngresoDTO(AplicacionIngresoDTO aplicacionIngresoDTO) {
		this.aplicacionIngresoDTO = aplicacionIngresoDTO;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("");
		sb.append("ContabilidadRemesaDTO { " + '\n');
		sb.append(id.toString());
		
		if (idToReporteSiniestro != null) {
			sb.append("idToReporteSiniestro = " + idToReporteSiniestro.toString() + '\n');
		} else {
			sb.append("idToReporteSiniestro = null " + '\n');
		}
		
		if (idRamo != null) {
			sb.append("idRamo = " + idRamo.toString()	+ '\n');
		} else {
			sb.append("idRamo = null " + '\n');
		}

		sb.append("conceptoPol = " + conceptoPol + '\n');

		if (idMoneda != null) {
			sb.append("idMoneda = " + idMoneda.toString() + '\n');
		} else {
			sb.append("idMoneda = null " + '\n');
		}

		if (tipoCambio != null) {
			sb.append("tipoCambio = " + tipoCambio.toString() + '\n');
		} else {
			sb.append("tipoCambio = null " + '\n');
		}		

		if (auxiliar != null) {
			sb.append("auxiliar = " + auxiliar.toString() + '\n');
		} else {
			sb.append("auxiliar = null " + '\n');
		}
			
		sb.append("cveConceptoMov = " + id.getCveConceptoMov() + '\n');
		sb.append("cCosto = " + cCosto + '\n');					
		
		if (impNeto != null) {
			sb.append("impNeto = " + impNeto.toString() + '\n');
		} else {
			sb.append("impNeto = null " + '\n');
		}
		
		sb.append("conceptoMov = " + conceptoMov + '\n');					
		
		if (impIva != null) {
			sb.append("impIva = " + impIva.toString() + '\n');
		} else {
			sb.append("impIva = null " + '\n');
		}
		
		if (impIvaRet != null) {
			sb.append("impIvaRet = " + impIvaRet.toString() + '\n');
		} else {
			sb.append("impIvaRet = null " + '\n');
		}				
		
		if (impIsrRet != null) {
			sb.append("impIsrRet = " + impIsrRet.toString() + '\n');
		} else {
			sb.append("impIsrRet = null " + '\n');
		}
		
		if (impOtrosImpuestos != null) {
			sb.append("impOtrosImpuestos = " + impOtrosImpuestos.toString()	+ '\n');
		} else {
			sb.append("impOtrosImpuestos = null " + '\n');
		}		

		if (impIsr != null) {
			sb.append("impIsr = " + impIsr.toString() + '\n');
		} else {
			sb.append("impIsr = null " + '\n');
		}
		
		sb.append("usuario = " + usuario + '\n');		

		sb.append("}" + '\n');

		return sb.toString();
	}	
}