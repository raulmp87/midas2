package mx.com.afirme.midas.siniestro.finanzas.gasto;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.EstatusFinanzasDTO;

/**
 * GastoSiniestro entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOGASTOSINIESTRO", schema = "MIDAS")
public class GastoSiniestroDTO implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToGastoSiniestro;
	private String descripcion;
	private Double montoGasto;
	private Double montoISR;
	private Double montoIVA;
	private Double montoIVARetencion;
	private Double montoISRRetencion;
	private Double montoOtros;
	private Date fechaGasto;
	private Date fechaEstimacionPago;
	private Date fechaRecepcion;
	private Date fechaEntrega;
	private BigDecimal numeroCheque;
	private BigDecimal numeroTransferencia;
	private String lugarEnvio;
    private Double porcentajeIVA;
    private Double porcentajeIVARetencion;
    private Double porcentajeISR;
    private Double porcentajeISRRetencion;
    private Double porcentajeOtros;		
	private Date fechaCreacion;
	private BigDecimal idTcUsuarioCreacion;
	private Date fechaModificacion;
	private BigDecimal idTcUsuarioModificacion;
	private ReporteSiniestroDTO reporteSiniestroDTO;
	private ConceptoGastoDTO conceptoGasto;
	private PrestadorServiciosDTO prestadorServicios;
	private BigDecimal idTcPrestadorServicios;
	private EstatusFinanzasDTO estatusFinanzas;
	private String nombrePrestadorServicios;

	// Constructors

	/** default constructor */
	public GastoSiniestroDTO() {
	}

	// Property accessors
	@Id
    @SequenceGenerator(name = "IDTOGASTOSINIESTRO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOGASTOSINIESTRO_SEQ")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "IDTOGASTOSINIESTRO_SEQ_GENERADOR")	
	@Column(name = "IDTOGASTOSINIESTRO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToGastoSiniestro() {
		return this.idToGastoSiniestro;
	}

	public void setIdToGastoSiniestro(BigDecimal idToGastoSiniestro) {
		this.idToGastoSiniestro = idToGastoSiniestro;
	}

	@Column(name = "DESCRIPCION", nullable = false, length = 240)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "MONTOGASTO", nullable = false, precision = 16)
	public Double getMontoGasto() {
		return this.montoGasto;
	}

	public void setMontoGasto(Double montoGasto) {
		this.montoGasto = montoGasto;
	}

	@Column(name = "MONTOISR", precision = 16)
	public Double getMontoISR() {
		return this.montoISR;
	}

	public void setMontoISR(Double montoISR) {
		this.montoISR = montoISR;
	}

	@Column(name = "MONTOIVA", precision = 16)
	public Double getMontoIVA() {
		return this.montoIVA;
	}

	public void setMontoIVA(Double montoIVA) {
		this.montoIVA = montoIVA;
	}

	@Column(name = "MONTOIVARETENCION", precision = 16)
	public Double getMontoIVARetencion() {
		return this.montoIVARetencion;
	}

	public void setMontoIVARetencion(Double montoIVARetencion) {
		this.montoIVARetencion = montoIVARetencion;
	}

	@Column(name = "MONTOISRRETENCION", precision = 16)
	public Double getMontoISRRetencion() {
		return this.montoISRRetencion;
	}

	public void setMontoISRRetencion(Double montoISRRetencion) {
		this.montoISRRetencion = montoISRRetencion;
	}

	@Column(name = "MONTOOTROS", precision = 16)
	public Double getMontoOtros() {
		return this.montoOtros;
	}

	public void setMontoOtros(Double montoOtros) {
		this.montoOtros = montoOtros;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAGASTO", nullable = false, length = 7)
	public Date getFechaGasto() {
		return this.fechaGasto;
	}

	public void setFechaGasto(Date fechaGasto) {
		this.fechaGasto = fechaGasto;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAESTIMACIONPAGO", length = 7)
	public Date getFechaEstimacionPago() {
		return this.fechaEstimacionPago;
	}

	public void setFechaEstimacionPago(Date fechaEstimacionPago) {
		this.fechaEstimacionPago = fechaEstimacionPago;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHARECEPCION", length = 7)
	public Date getFechaRecepcion() {
		return this.fechaRecepcion;
	}

	public void setFechaRecepcion(Date fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAENTREGA", length = 7)
	public Date getFechaEntrega() {
		return this.fechaEntrega;
	}

	public void setFechaEntrega(Date fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}

	@Column(name = "NUMEROCHEQUE", precision = 22, scale = 0)
	public BigDecimal getNumeroCheque() {
		return this.numeroCheque;
	}

	public void setNumeroCheque(BigDecimal numeroCheque) {
		this.numeroCheque = numeroCheque;
	}

	@Column(name = "NUMEROTRANSFERENCIA", precision = 22, scale = 0)
	public BigDecimal getNumeroTransferencia() {
		return this.numeroTransferencia;
	}

	public void setNumeroTransferencia(BigDecimal numeroTransferencia) {
		this.numeroTransferencia = numeroTransferencia;
	}

	@Column(name = "LUGARENVIO", length = 240)
	public String getLugarEnvio() {
		return this.lugarEnvio;
	}

	public void setLugarEnvio(String lugarEnvio) {
		this.lugarEnvio = lugarEnvio;
	}

	
    @Column(name="PORCENTAJEIVA", precision=5)
    public Double getPorcentajeIVA() {
        return this.porcentajeIVA;
    }
    
    public void setPorcentajeIVA(Double porcentajeIVA) {
        this.porcentajeIVA = porcentajeIVA;
    }
    
    @Column(name="PORCENTAJEIVARETENCION", precision=5)
    public Double getPorcentajeIVARetencion() {
        return this.porcentajeIVARetencion;
    }
    
    public void setPorcentajeIVARetencion(Double porcentajeIVARetencion) {
        this.porcentajeIVARetencion = porcentajeIVARetencion;
    }
    
    @Column(name="PORCENTAJEISR", precision=5)
    public Double getPorcentajeISR() {
        return this.porcentajeISR;
    }
    
    public void setPorcentajeISR(Double porcentajeISR) {
        this.porcentajeISR = porcentajeISR;
    }
    
    @Column(name="PORCENTAJEISRRETENCION", precision=5)
    public Double getPorcentajeISRRetencion() {
        return this.porcentajeISRRetencion;
    }
    
    public void setPorcentajeISRRetencion(Double porcentajeISRRetencion) {
        this.porcentajeISRRetencion = porcentajeISRRetencion;
    }
    
    @Column(name="PORCENTAJEOTROS", precision=5)
    public Double getPorcentajeOtros() {
        return this.porcentajeOtros;
    }
    
    public void setPorcentajeOtros(Double porcentajeOtros) {
        this.porcentajeOtros = porcentajeOtros;
    }	
			
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHACREACION", nullable = false, length = 7)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Column(name = "IDTCUSUARIOCREACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcUsuarioCreacion() {
		return this.idTcUsuarioCreacion;
	}

	public void setIdTcUsuarioCreacion(BigDecimal idTcUsuarioCreacion) {
		this.idTcUsuarioCreacion = idTcUsuarioCreacion;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAMODIFICACION", length = 7)
	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Column(name = "IDTCUSUARIOMODIFICACION", precision = 22, scale = 0)
	public BigDecimal getIdTcUsuarioModificacion() {
		return this.idTcUsuarioModificacion;
	}

	public void setIdTcUsuarioModificacion(BigDecimal idTcUsuarioModificacion) {
		this.idTcUsuarioModificacion = idTcUsuarioModificacion;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOREPORTESINIESTRO", nullable = false)
	public ReporteSiniestroDTO getReporteSiniestroDTO() {
		return this.reporteSiniestroDTO;
	}

	public void setReporteSiniestroDTO(ReporteSiniestroDTO toreportesiniestro) {
		this.reporteSiniestroDTO = toreportesiniestro;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTCCONCEPTOGASTO")
    public ConceptoGastoDTO getConceptoGasto(){
		return this.conceptoGasto;
	}
	
	public void setConceptoGasto(ConceptoGastoDTO conceptoGasto){
		this.conceptoGasto = conceptoGasto;
	}
		
	@Column(name="IDTCPRESTADORSERVICIOS", nullable=false, precision=22, scale=0)
	public BigDecimal getIdTcPrestadorServicios() {
		return idTcPrestadorServicios;
	}    

	public void setIdTcPrestadorServicios(BigDecimal idTcPrestadorServicios) {
		this.idTcPrestadorServicios = idTcPrestadorServicios;
	}
	
	@Transient
    public PrestadorServiciosDTO getPrestadorServicios(){
		return this.prestadorServicios;
	}
	
	public void setPrestadorServicios(PrestadorServiciosDTO prestadorServicios){
		this.prestadorServicios= prestadorServicios;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTCESTATUS")
	public EstatusFinanzasDTO getEstatusFinanzas() {
		return estatusFinanzas;
	}

	public void setEstatusFinanzas(EstatusFinanzasDTO estatusFinanzas) {
		this.estatusFinanzas = estatusFinanzas;
	}

	@Transient
	public String getNombrePrestadorServicios() {
		return nombrePrestadorServicios;
	}

	public void setNombrePrestadorServicios(String nombrePrestadorServicios) {
		this.nombrePrestadorServicios = nombrePrestadorServicios;
	}
}