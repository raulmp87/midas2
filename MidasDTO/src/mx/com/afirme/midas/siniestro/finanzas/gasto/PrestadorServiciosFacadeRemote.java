package mx.com.afirme.midas.siniestro.finanzas.gasto;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for PrestadorServiciosFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface PrestadorServiciosFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved PrestadorServicios
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            PrestadorServicios entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(PrestadorServiciosDTO entity);

	/**
	 * Delete a persistent PrestadorServicios entity.
	 * 
	 * @param entity
	 *            PrestadorServicios entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(PrestadorServiciosDTO entity);

	/**
	 * Persist a previously saved PrestadorServicios entity and return it or a
	 * copy of it to the sender. A copy of the PrestadorServicios entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            PrestadorServicios entity to update
	 * @return PrestadorServicios the persisted PrestadorServicios entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public PrestadorServiciosDTO update(PrestadorServiciosDTO entity);

	public PrestadorServiciosDTO findById(BigDecimal id);

	/**
	 * Find all PrestadorServicios entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the PrestadorServicios property to query
	 * @param value
	 *            the property value to match
	 * @return List<PrestadorServicios> found by query
	 */
	public List<PrestadorServiciosDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all PrestadorServicios entities.
	 * 
	 * @return List<PrestadorServicios> all PrestadorServicios entities
	 */
	public List<PrestadorServiciosDTO> findAll();
}