package mx.com.afirme.midas.siniestro.finanzas.integracion;

public final class ConceptoMovimientoSiniestro {

	public static final int INDEMNIZACION 				   		= 5;
	public static final int CANCELACION_DE_INDEMNIZACION   		= 6;
	public static final int DEDUCIBLE					   		= 7;
	public static final int CANCELACION_DE_DEDUCIBLE 	   		= 8;
	public static final int GASTOS_DE_AJUSTE               		= 9;
	public static final int SALVAMENTO                     		= 10;
	public static final int CANCELAR_SALVAMENTO            		= 11;
	public static final int RECUPERACION_LEGAL             		= 12;
	public static final int CANCELAR_RECUPERACION_LEGAL    		= 13;
	public static final int RESERVA_INICIAL                		= 21;                           
	public static final int AJUSTE_DE_MAS_EN_RESERVA       		= 22;            
	public static final int AJUSTE_DE_MENOS_EN_RESERVA     		= 23;
	public static final int CANCELACION_DE_GASTO_DE_AJUSTE 		= 24;
	public static final int INGRESO_COASEGURO					= 25;
	public static final int CANCELACION_DE_INGRESO_COASEGURO	= 26;
	
	private ConceptoMovimientoSiniestro(){		
	} 	
}
