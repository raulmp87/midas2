package mx.com.afirme.midas.siniestro.finanzas.ingreso;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDTO;

@Entity
@Table(name = "TOAPLICACIONINGRESO", schema = "MIDAS")
public class AplicacionIngresoDTO implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final Short PENDIENTE = 0;
	public static final Short CONTABILIZADO = 1;
	public static final Short CANCELADO = 2;
	// Fields

	private BigDecimal idToAplicacionIngreso;
	private AutorizacionTecnicaDTO autorizacionTecnicaDTO;
	private Short estatus;
	private String observacionAplicacion;
	private String referenciasAplicacion;
	private BigDecimal idTcUsuarioAplicoIngreso;
	private Date fechaAplicacoIngreso;
	private BigDecimal idTcUsuarioCancelacion;
	private Date fechaCancelacion;
	private String idReferenciaExterna;

	// Constructors

	/** default constructor */
	public AplicacionIngresoDTO() {
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTOAPLICACIONINGRESO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOAPLICACIONINGRESO_SEQ")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "IDTOAPLICACIONINGRESO_SEQ_GENERADOR")
	@Column(name = "IDTOAPLICACIONINGRESO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToAplicacionIngreso() {
		return this.idToAplicacionIngreso;
	}

	public void setIdToAplicacionIngreso(BigDecimal idToAplicacionIngreso) {
		this.idToAplicacionIngreso = idToAplicacionIngreso;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOAUTORIZACIONTECNICA", nullable = false)
	public AutorizacionTecnicaDTO getAutorizacionTecnicaDTO() {
		return this.autorizacionTecnicaDTO;
	}

	public void setAutorizacionTecnicaDTO(AutorizacionTecnicaDTO autorizacionTecnicaDTO) {
		this.autorizacionTecnicaDTO = autorizacionTecnicaDTO;
	}

	@Column(name = "ESTATUS", nullable = false, precision = 22, scale = 0)
	public Short getEstatus() {
		return this.estatus;
	}

	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}

	@Column(name = "OBSERVACIONAPLICACION")
	public String getObservacionAplicacion() {
		return this.observacionAplicacion;
	}

	public void setObservacionAplicacion(String observacionAplicacion) {
		this.observacionAplicacion = observacionAplicacion;
	}

	@Column(name = "REFERENCIASAPLICACION", length = 250)
	public String getReferenciasAplicacion() {
		return this.referenciasAplicacion;
	}

	public void setReferenciasAplicacion(String referenciasAplicacion) {
		this.referenciasAplicacion = referenciasAplicacion;
	}

	@Column(name = "IDTCUSUARIOAPLICOINGRESO", precision = 22, scale = 0)
	public BigDecimal getIdTcUsuarioAplicoIngreso() {
		return this.idTcUsuarioAplicoIngreso;
	}

	public void setIdTcUsuarioAplicoIngreso(BigDecimal idTcUsuarioAplicoIngreso) {
		this.idTcUsuarioAplicoIngreso = idTcUsuarioAplicoIngreso;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAAPLICACOINGRESO", length = 7)
	public Date getFechaAplicacoIngreso() {
		return this.fechaAplicacoIngreso;
	}

	public void setFechaAplicacoIngreso(Date fechaAplicacoIngreso) {
		this.fechaAplicacoIngreso = fechaAplicacoIngreso;
	}

	@Column(name = "IDTCUSUARIOCANCELACION", precision = 22, scale = 0)
	public BigDecimal getIdTcUsuarioCancelacion() {
		return this.idTcUsuarioCancelacion;
	}

	public void setIdTcUsuarioCancelacion(BigDecimal idTcUsuarioCancelacion) {
		this.idTcUsuarioCancelacion = idTcUsuarioCancelacion;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHACANCELACION", length = 7)
	public Date getFechaCancelacion() {
		return this.fechaCancelacion;
	}

	public void setFechaCancelacion(Date fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}

	@Column(name = "IDREFERENCIAEXTERNA")
	public String getIdReferenciaExterna() {
		return idReferenciaExterna;
	}

	public void setIdReferenciaExterna(String idReferenciaExterna) {
		this.idReferenciaExterna = idReferenciaExterna;
	}

}