package mx.com.afirme.midas.siniestro.finanzas.integracion;

import java.io.Serializable;

import java.math.BigDecimal;
import java.util.Date;

public class IntegracionReaseguroDTO implements Serializable{
	/*
	 * 04/03/2011. Se agregan las constantes DISTRIBUCION_DE_AUTORIZACION_POR_INGRESO  y DISTRIBUCION_DE_CANCELACION_POR_INGRESO
	 * Usadas para la aplicación de ingresos, ya que Midas debe indicar a seycos de dónde proviene un deducible, coaseguro, 
	 * salvamento ó recuperación: de un ingreso o de una retención. Las constantes originales se dejan igual en los lugares donde
	 * se venían usando.
	 */
	public static final int DISTRIBUCION_DE_AUTORIZACION_POR_RETENCION = 0;
	public static final int DISTRIBUCION_DE_CANCELACION_POR_RETENCION  = 1;
	public static final int DISTRIBUCION_DE_AUTORIZACION_POR_INGRESO = 2;
	public static final int DISTRIBUCION_DE_CANCELACION_POR_INGRESO  = 3;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal idToPoliza;
	private Integer numeroEndoso; 
	private BigDecimal idToCobertura;
	private BigDecimal idToSeccion;
	private BigDecimal numeroInciso;
	private BigDecimal numeroSubInciso;
	private BigDecimal idMoneda;
	private BigDecimal idToReporteSiniestro;
	private Integer conceptoMovimiento;
	private Date fechaRegistroMovimiento; 
	private Integer tipoMovimiento;
	private BigDecimal idMovimientoSiniestro;
	private Double montoSiniestro;
	private Double ajusteDeMas;
	private Double ajusteDeMenos;
	private Double porcentajeSumaAsegurada;
	
	public IntegracionReaseguroDTO(){	
	}
	
	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}
	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}
	public void setNumeroEndoso(Integer numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}
	public Integer getNumeroEndoso() {
		return numeroEndoso;
	}
	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}
	public BigDecimal getIdToCobertura() {
		return idToCobertura;
	}
	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroSubInciso(BigDecimal numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}

	public BigDecimal getNumeroSubInciso() {
		return numeroSubInciso;
	}

	public void setIdMoneda(BigDecimal idMoneda) {
		this.idMoneda = idMoneda;
	}

	public BigDecimal getIdMoneda() {
		return idMoneda;
	}

	public void setIdToReporteSiniestro(BigDecimal idToReporteSiniestro) {
		this.idToReporteSiniestro = idToReporteSiniestro;
	}
	public BigDecimal getIdToReporteSiniestro() {
		return idToReporteSiniestro;
	}
	public void setConceptoMovimiento(Integer conceptoMovimiento) {
		this.conceptoMovimiento = conceptoMovimiento;
	}
	public Integer getConceptoMovimiento() {
		return conceptoMovimiento;
	}
	public void setFechaRegistroMovimiento(Date fechaRegistroMovimiento) {
		this.fechaRegistroMovimiento = fechaRegistroMovimiento;
	}
	public Date getFechaRegistroMovimiento() {
		return fechaRegistroMovimiento;
	}
	public void setTipoMovimiento(Integer tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}
	public Integer getTipoMovimiento() {
		return tipoMovimiento;
	}
	public void setIdMovimientoSiniestro(BigDecimal idMovimientoSiniestro) {
		this.idMovimientoSiniestro = idMovimientoSiniestro;
	}
	public BigDecimal getIdMovimientoSiniestro() {
		return idMovimientoSiniestro;
	}
	public void setMontoSiniestro(Double montoSiniestro) {
		this.montoSiniestro = montoSiniestro;
	}
	public Double getMontoSiniestro() {
		return montoSiniestro;
	}
	public void setAjusteDeMas(Double ajusteDeMas) {
		this.ajusteDeMas = ajusteDeMas;
	}
	public Double getAjusteDeMas() {
		return ajusteDeMas;
	}
	public void setAjusteDeMenos(Double ajusteDeMenos) {
		this.ajusteDeMenos = ajusteDeMenos;
	}
	public Double getAjusteDeMenos() {
		return ajusteDeMenos;
	}
	
	public void setPorcentajeSumaAsegurada(Double porcentajeSumaAsegurada) {
		this.porcentajeSumaAsegurada = porcentajeSumaAsegurada;
	}

	public Double getPorcentajeSumaAsegurada() {
		return porcentajeSumaAsegurada;
	}

	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();				
		
		if(idToPoliza != null){
			sb.append("idToPoliza = " + idToPoliza.toString() + "\n");
		}
		if(numeroEndoso != null){
			sb.append("numeroEndoso = " +  numeroEndoso.toString() + "\n");
		}
		if(idToCobertura != null){
			sb.append("idToCobertura = " + idToCobertura.toString() + "\n");
		}
		if(idToSeccion != null){
			sb.append("idToSeccion = " + idToSeccion.toString() + "\n");
		}
		if(numeroInciso != null){
			sb.append("numeroInciso = " + numeroInciso.toString() + "\n");
		}
		if(numeroSubInciso != null){
			sb.append("numeroSubInciso = " + numeroSubInciso.toString() + "\n");
		}
		if(idMoneda != null){
			sb.append("idMoneda = " + idMoneda.toString() + "\n");
		}
		if(idToReporteSiniestro != null){
			sb.append("idToReporteSiniestro = " + idToReporteSiniestro.toString() + "\n");
		}
		if(conceptoMovimiento != null){
			sb.append("conceptoMovimiento = " + conceptoMovimiento.toString() + "\n");
		}
		if(fechaRegistroMovimiento != null){
			sb.append("fechaRegistroMovimiento = " + fechaRegistroMovimiento.toString() + "\n");
		}
		if(tipoMovimiento != null){
			sb.append("tipoMovimiento = " + tipoMovimiento.toString() + "\n");
		}
		if(idMovimientoSiniestro != null){
			sb.append("idMovimientoSiniestro = " + idMovimientoSiniestro.toString() + "\n");
		}
		if(montoSiniestro != null){
			sb.append("montoSiniestro = " + montoSiniestro.toString() + "\n");
		}
		if(ajusteDeMas != null){
			sb.append("ajusteDeMas = " + ajusteDeMas.toString() + "\n");
		}
		if(ajusteDeMenos != null){
			sb.append("ajusteDeMenos = " + ajusteDeMenos.toString() + "\n");
		}
		if(porcentajeSumaAsegurada != null){
			sb.append("porcentajeSumaAsegurada = " + porcentajeSumaAsegurada.toString() + "\n");
		}												
		
		return sb.toString();
	}
}
