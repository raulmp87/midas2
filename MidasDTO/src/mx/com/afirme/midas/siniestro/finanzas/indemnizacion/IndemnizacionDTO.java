package mx.com.afirme.midas.siniestro.finanzas.indemnizacion;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.EstatusFinanzasDTO;

/**
 * IndemnizacionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOINDEMNIZACION"
    ,schema="MIDAS"
)

public class IndemnizacionDTO  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

    // Fields    

     private BigDecimal idToIndemnizacion;
     private EstatusFinanzasDTO estatusFinanzasDTO;
     private ReporteSiniestroDTO reporteSiniestroDTO;
     private Boolean variosPagos;
     private Double totalPago;
     private Timestamp fechaDelPago;
     private String comentarios;
     private String beneficiario;
     private Double porcentajeIva;
     private Double porcentajeIvaRetencion;
     private Double porcentajeIsr;
     private Double porcentajeIsrRetencion;
     private Double porcentajeOtros;
     private Double montoIva;
     private Double montoIvaRetencion;
     private Double montoIsr;
     private Double montoIsrRetencion;
     private Double montoOtros;
     private Boolean ultimoPago;
     private Date fechacreacion;
 	 private BigDecimal idtcusuariocreacion;
 	 private Date fechamodificacion;
 	 private BigDecimal idtcusuariomodificacion;
     
     @SuppressWarnings("unused")
	private List<IndemnizacionRiesgoCoberturaDTO> indemnizacionRiesgoCoberturaDTOs = new ArrayList<IndemnizacionRiesgoCoberturaDTO>();

    // Constructors

    /** default constructor */
    public IndemnizacionDTO() {
    }

    // Property accessors
    @Id 
    @SequenceGenerator(name = "IDTOINDEMNIZACION_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOINDEMNIZACION_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOINDEMNIZACION_SEQ_GENERADOR")	
    @Column(name="IDTOINDEMNIZACION", unique=true, nullable=false, precision=22, scale=0)

    public BigDecimal getIdToIndemnizacion() {
        return this.idToIndemnizacion;
    }
    
    public void setIdToIndemnizacion(BigDecimal idToIndemnizacion) {
        this.idToIndemnizacion = idToIndemnizacion;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTCESTATUS", nullable=false)

    public EstatusFinanzasDTO getEstatusFinanzasDTO() {
        return this.estatusFinanzasDTO;
    }
    
    public void setEstatusFinanzasDTO(EstatusFinanzasDTO estatusFinanzasDTO) {
        this.estatusFinanzasDTO = estatusFinanzasDTO;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDTOREPORTESINIESTRO", nullable=false)

    public ReporteSiniestroDTO getReporteSiniestroDTO() {
        return this.reporteSiniestroDTO;
    }
    
    public void setReporteSiniestroDTO(ReporteSiniestroDTO reporteSiniestroDTO) {
        this.reporteSiniestroDTO = reporteSiniestroDTO;
    }
    
    @Column(name="VARIOSPAGOS", nullable=false, precision=1, scale=0)

    public Boolean getVariosPagos() {
        return this.variosPagos;
    }
    
    public void setVariosPagos(Boolean variosPagos) {
        this.variosPagos = variosPagos;
    }
    
    @Column(name="TOTALPAGO", nullable=false, precision=16)

    public Double getTotalPago() {
        return this.totalPago;
    }
    
    public void setTotalPago(Double totalPago) {
        this.totalPago = totalPago;
    }
    
    @Column(name="FECHADELPAGO", nullable=false, length=7)

    public Timestamp getFechaDelPago() {
        return this.fechaDelPago;
    }
    
    public void setFechaDelPago(Timestamp fechaDelPago) {
        this.fechaDelPago = fechaDelPago;
    }
    
    @Column(name="COMENTARIOS", length=250)

    public String getComentarios() {
        return this.comentarios;
    }
    
    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }
    
    @Column(name="BENEFICIARIO", length=150)

    public String getBeneficiario() {
        return this.beneficiario;
    }
    
    public void setBeneficiario(String beneficiario) {
        this.beneficiario = beneficiario;
    }
    
    @Column(name="PORCENTAJEIVA", precision=5)

    public Double getPorcentajeIva() {
        return this.porcentajeIva;
    }
    
    public void setPorcentajeIva(Double porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }
    
    @Column(name="PORCENTAJEIVARETENCION", precision=5)

    public Double getPorcentajeIvaRetencion() {
        return this.porcentajeIvaRetencion;
    }
    
    public void setPorcentajeIvaRetencion(Double porcentajeIvaRetencion) {
        this.porcentajeIvaRetencion = porcentajeIvaRetencion;
    }
    
    @Column(name="PORCENTAJEISR", precision=5)

    public Double getPorcentajeIsr() {
        return this.porcentajeIsr;
    }
    
    public void setPorcentajeIsr(Double porcentajeIsr) {
        this.porcentajeIsr = porcentajeIsr;
    }
    
    @Column(name="PORCENTAJEISRRETENCION", precision=5)

    public Double getPorcentajeIsrRetencion() {
        return this.porcentajeIsrRetencion;
    }
    
    public void setPorcentajeIsrRetencion(Double porcentajeIsrRetencion) {
        this.porcentajeIsrRetencion = porcentajeIsrRetencion;
    }
    
    @Column(name="PORCENTAJEOTROS", precision=5)

    public Double getPorcentajeOtros() {
        return this.porcentajeOtros;
    }
    
    public void setPorcentajeOtros(Double porcentajeOtros) {
        this.porcentajeOtros = porcentajeOtros;
    }
    
    @Column(name="MONTOIVA", precision=16)

    public Double getMontoIva() {
        return this.montoIva;
    }
    
    public void setMontoIva(Double montoIva) {
        this.montoIva = montoIva;
    }
    
    @Column(name="MONTOIVARETENCION", precision=16)

    public Double getMontoIvaRetencion() {
        return this.montoIvaRetencion;
    }
    
    public void setMontoIvaRetencion(Double montoIvaRetencion) {
        this.montoIvaRetencion = montoIvaRetencion;
    }
    
    @Column(name="MONTOISR", precision=16)

    public Double getMontoIsr() {
        return this.montoIsr;
    }
    
    public void setMontoIsr(Double montoIsr) {
        this.montoIsr = montoIsr;
    }
    
    @Column(name="MONTOISRRETENCION", precision=16)

    public Double getMontoIsrRetencion() {
        return this.montoIsrRetencion;
    }
    
    public void setMontoIsrRetencion(Double montoIsrRetencion) {
        this.montoIsrRetencion = montoIsrRetencion;
    }
    
    @Column(name="MONTOOTROS", precision=16)

    public Double getMontoOtros() {
        return this.montoOtros;
    }
    
    public void setMontoOtros(Double montoOtros) {
        this.montoOtros = montoOtros;
    }
    
    @Column(name="ULTIMOPAGO", nullable=false, precision=1, scale=0)

    public Boolean getUltimoPago() {
        return this.ultimoPago;
    }
    
    public void setUltimoPago(Boolean ultimoPago) {
        this.ultimoPago = ultimoPago;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHACREACION", nullable = false, length = 7)
	public Date getFechacreacion() {
		return this.fechacreacion;
	}

	public void setFechacreacion(Date fechacreacion) {
		this.fechacreacion = fechacreacion;
	}

	@Column(name = "IDTCUSUARIOCREACION", precision = 22, scale = 0)
	public BigDecimal getIdtcusuariocreacion() {
		return this.idtcusuariocreacion;
	}

	public void setIdtcusuariocreacion(BigDecimal idtcusuariocreacion) {
		this.idtcusuariocreacion = idtcusuariocreacion;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAMODIFICACION", length = 7)
	public Date getFechamodificacion() {
		return this.fechamodificacion;
	}

	public void setFechamodificacion(Date fechamodificacion) {
		this.fechamodificacion = fechamodificacion;
	}

	@Column(name = "IDTCUSUARIOMODIFICACION", precision = 22, scale = 0)
	public BigDecimal getIdtcusuariomodificacion() {
		return this.idtcusuariomodificacion;
	}

	public void setIdtcusuariomodificacion(BigDecimal idtcusuariomodificacion) {
		this.idtcusuariomodificacion = idtcusuariomodificacion;
	}
    
}