package mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.siniestro.finanzas.pagos.SoportePagosDTO;

/**
 * Remote interface for AutorizacionTecnicaFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface AutorizacionTecnicaFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved AutorizacionTecnica
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            AutorizacionTecnica entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(AutorizacionTecnicaDTO entity);

	/**
	 * Delete a persistent AutorizacionTecnica entity.
	 * 
	 * @param entity
	 *            AutorizacionTecnica entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(AutorizacionTecnicaDTO entity);

	/**
	 * Persist a previously saved AutorizacionTecnica entity and return it or a
	 * copy of it to the sender. A copy of the AutorizacionTecnica entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            AutorizacionTecnica entity to update
	 * @return AutorizacionTecnica the persisted AutorizacionTecnica entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public AutorizacionTecnicaDTO update(AutorizacionTecnicaDTO entity);

	public AutorizacionTecnicaDTO findById(BigDecimal id);

	/**
	 * Find all AutorizacionTecnica entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the AutorizacionTecnica property to query
	 * @param value
	 *            the property value to match
	 * @return List<AutorizacionTecnica> found by query
	 */
	public List<AutorizacionTecnicaDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all AutorizacionTecnica entities.
	 * 
	 * @return List<AutorizacionTecnica> all AutorizacionTecnica entities
	 */
	public List<AutorizacionTecnicaDTO> findAll();

	public List<AutorizacionTecnicaDTO> listarAutorizacionesGastosFiltrado(
			SoportePagosDTO soportePagosDTO);

	public List<AutorizacionTecnicaDTO> listarAutorizacionesPorEstatus(
			BigDecimal idToReporteSiniestro, short estatus);

	public List<AutorizacionTecnicaDTO> listarAutorizacionesPorCancelar(
			BigDecimal idToReporteSiniestro);

	public AutorizacionTecnicaDTO obtenerAutorizacionIndemnizacion(
			BigDecimal idToReporteSiniestro, BigDecimal idToIndemnizacion);

	public AutorizacionTecnicaDTO obtenerAutorizacionIngreso(
			BigDecimal idToReporteSiniestro, BigDecimal idToIngresoSiniestro);

	public AutorizacionTecnicaDTO obtenerAutorizacionGasto(
			BigDecimal idToReporteSiniestro, BigDecimal idToGastoSiniestro);
}
