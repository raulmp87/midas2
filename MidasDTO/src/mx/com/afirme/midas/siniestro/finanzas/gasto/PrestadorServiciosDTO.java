package mx.com.afirme.midas.siniestro.finanzas.gasto;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;

/**
 * PrestadorServicios entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCPRESTADORSERVICIOS", schema = "MIDAS")
public class PrestadorServiciosDTO extends CacheableDTO  implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigDecimal idTcPrestadorServicios;
	private String nombrePrestador;

	// Constructors

	/** default constructor */
	public PrestadorServiciosDTO() {
	}

	// Property accessors
	@Id
	@Column(name = "IDTCPRESTADORSERVICIOS", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcPrestadorServicios() {
		return this.idTcPrestadorServicios;
	}

	public void setIdTcPrestadorServicios(BigDecimal idTcPrestadorServicios) {
		this.idTcPrestadorServicios = idTcPrestadorServicios;
	}

	@Column(name = "NOMBREPRESTADOR", length = 240)
	public String getNombrePrestador() {
		return this.nombrePrestador;
	}

	public void setNombrePrestador(String nombrePrestador) {
		this.nombrePrestador = nombrePrestador;
	}
	
	@Override
	public BigDecimal getId() {
		return this.idTcPrestadorServicios;
	}
	@Override
	public String getDescription() {
		return this.nombrePrestador;
	}
	
	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof PrestadorServiciosDTO) {
			PrestadorServiciosDTO prestadorServiciosDTO = (PrestadorServiciosDTO) object;
			equal = prestadorServiciosDTO.getIdTcPrestadorServicios().equals(this.getIdTcPrestadorServicios());
		} // End of if
		return equal;
	}

}