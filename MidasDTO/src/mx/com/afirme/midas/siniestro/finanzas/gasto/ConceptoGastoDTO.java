package mx.com.afirme.midas.siniestro.finanzas.gasto;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;

/**
 * ConceptoGasto entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCCONCEPTOGASTO", schema = "MIDAS")
public class ConceptoGastoDTO extends CacheableDTO implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idTcConceptoGasto;
	private String descripcion;
    private Short cuenta;
    private Short subCuenta;	

	// Constructors

	/** default constructor */
	public ConceptoGastoDTO() {
	}

	// Property accessors
	@Id
	@Column(name = "IDTCCONCEPTOGASTO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcConceptoGasto() {
		return this.idTcConceptoGasto;
	}

	public void setIdTcConceptoGasto(BigDecimal idTcConceptoGasto) {
		this.idTcConceptoGasto = idTcConceptoGasto;
	}

	@Column(name = "DESCRIPCION", length = 20)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Override
	public BigDecimal getId() {
		return this.idTcConceptoGasto;
	}
	@Override
	public String getDescription() {
		return this.descripcion;
	}
	
    @Column(name="CUENTA", nullable=false, precision=4, scale=0)
    public Short getCuenta() {
        return this.cuenta;
    }
    
    public void setCuenta(Short cuenta) {
        this.cuenta = cuenta;
    }
    
    @Column(name="SUBCUENTA", nullable=false, precision=4, scale=0)
    public Short getSubCuenta() {
        return this.subCuenta;
    }
    
    public void setSubCuenta(Short subCuenta) {
        this.subCuenta = subCuenta;
    }	
	
	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof ConceptoGastoDTO) {
			ConceptoGastoDTO conceptoGastoDTO = (ConceptoGastoDTO) object;
			equal = conceptoGastoDTO.getIdTcConceptoGasto().equals(this.getIdTcConceptoGasto());
		} // End of if
		return equal;
	}

	
}