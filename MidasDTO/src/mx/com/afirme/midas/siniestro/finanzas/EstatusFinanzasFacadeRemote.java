package mx.com.afirme.midas.siniestro.finanzas;

import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for EstatusFinanzasFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface EstatusFinanzasFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved EstatusFinanzasDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            EstatusFinanzasDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(EstatusFinanzasDTO entity);

	/**
	 * Delete a persistent EstatusFinanzasDTO entity.
	 * 
	 * @param entity
	 *            EstatusFinanzasDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(EstatusFinanzasDTO entity);

	/**
	 * Persist a previously saved EstatusFinanzasDTO entity and return it or a copy
	 * of it to the sender. A copy of the EstatusFinanzasDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            EstatusFinanzasDTO entity to update
	 * @return EstatusFinanzasDTO the persisted EstatusFinanzasDTO entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public EstatusFinanzasDTO update(EstatusFinanzasDTO entity);

	public EstatusFinanzasDTO findById(Short id);

	/**
	 * Find all EstatusFinanzasDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the EstatusFinanzasDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<EstatusFinanzasDTO> found by query
	 */
	public List<EstatusFinanzasDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all EstatusFinanzasDTO entities.
	 * 
	 * @return List<EstatusFinanzasDTO> all EstatusFinanzasDTO entities
	 */
	public List<EstatusFinanzasDTO> findAll();
}