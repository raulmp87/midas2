package mx.com.afirme.midas.siniestro.finanzas.pagos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import mx.com.afirme.midas2.annotation.Exportable;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation.OperationType;
import mx.com.afirme.midas2.util.DateUtils;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation;

import org.springframework.stereotype.Component;

@Component
public class FacturaPteComplMonitor implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long noLiquidacion;
	private String tipoLiquidacion;
	private Long noPrestadorServicio;
	private String prestadorServicio;
	private String factura;
	private String  noSolCheque;
	private BigDecimal noCheque;
	private String uuidFactura;
	private Date fechaPago;	
	private String uuidComplementoPago;
	private BigDecimal importePagado;
	private BigDecimal importeRestante;

		
	public FacturaPteComplMonitor() {
		super();
	}


	public FacturaPteComplMonitor(Long noLiquidacion,
			String tipoLiquidacion, Long noPrestadorServicio, String prestadorServicio,
			String factura, String noSolCheque, BigDecimal noCheque, Date fechaPago,
			String uuidFactura, String uuidComplementoPago,
			BigDecimal importePagado, BigDecimal importeRestante) {
		super();
		this.noLiquidacion = noLiquidacion;
		this.tipoLiquidacion = tipoLiquidacion;
		this.noPrestadorServicio = noPrestadorServicio;
		this.prestadorServicio = prestadorServicio;
		this.factura = factura;
		this.noSolCheque = noSolCheque;
		this.noCheque = noCheque;
		this.fechaPago = fechaPago;
		this.uuidFactura = uuidFactura;
		this.uuidComplementoPago = uuidComplementoPago;
		this.importePagado = importePagado;
		this.importeRestante = importeRestante;
	}


	@Override
	public String toString() {
		return "FacturaPteComplMonitor [noLiquidacion=" + noLiquidacion + ", tipoLiquidacion="
				+ tipoLiquidacion + ", noPrestadorServicio=" + noPrestadorServicio
				+ ", prestadorServicio=" + prestadorServicio + ", factura=" + factura
				+ ", noSolCheque=" + noSolCheque + ", noCheque=" + noCheque
				+ ", fechaPago=" + fechaPago + ", uuidFactura=" + uuidFactura
				+ ", uuidComplementoPago=" + uuidComplementoPago
				+ ", importePagado=" + importePagado
				+ ", importeRestante ="+  importeRestante+ "]";
	}

    @Exportable(columnName="NO. LIQUIDACION", columnOrder=0)
	@FilterPersistenceAnnotation(persistenceName="NO_LIQUIDACION")
	public Long getNoLiquidacion() {
		return noLiquidacion;
	}


	public void setNoLiquidacion(Long noLiquidacion) {
		this.noLiquidacion = noLiquidacion;
	}

    @Exportable(columnName="TIPO LIQUIDACION", columnOrder=1)
	@FilterPersistenceAnnotation(persistenceName="TIPO_LIQUIDACION")
	public String getTipoLiquidacion() {
		return tipoLiquidacion;
	}


	public void setTipoLiquidacion(String tipoLiquidacion) {
		this.tipoLiquidacion = tipoLiquidacion;
	}

    @Exportable(columnName="NO. PRESTADOR SERVICIO", columnOrder=2)
	@FilterPersistenceAnnotation(persistenceName="ID_PROVEEDOR")
	public Long getNoPrestadorServicio() {
    	return noPrestadorServicio;

	}


	public void setNoPrestadorServicio(Long noPrestadorServicio) {
		this.noPrestadorServicio = noPrestadorServicio;
	}

    @Exportable(columnName="PRESTADOR SERVICIO", columnOrder=3)
	@FilterPersistenceAnnotation(persistenceName="NOMBRE_PROVEEDOR")
	public String getPrestadorServicio() {
    	return prestadorServicio;
	}


	public void setPrestadorServicio(String prestadorServicio) {
		this.prestadorServicio = prestadorServicio;
	}

	@Exportable(columnName="FACTURA", columnOrder=4)
	@FilterPersistenceAnnotation(persistenceName="FOLIO_FACTURA")
	public String getFactura() {
		return factura;
	}


	public void setFactura(String factura) {
		this.factura = factura;
	}

    @Exportable(columnName="NO. SOLICITUD CHEQUE", columnOrder=5)
	@FilterPersistenceAnnotation(persistenceName="NO_SOLICITUD_CHEQUE")
	public String getNoSolCheque() {
		return noSolCheque;
	}


	public void setNoSolCheque(String noSolCheque) {
		this.noSolCheque = noSolCheque;
	}

    @Exportable(columnName="NO. CHEQUE", columnOrder=6)
	@FilterPersistenceAnnotation(persistenceName="NUMERO_CHEQUE")
	public BigDecimal getNoCheque() {
		return noCheque;
	}


	public void setNoCheque(BigDecimal noCheque) {
		this.noCheque = noCheque;
	}


    @Exportable(columnName="FECHA PAGO", columnOrder=7, format= "dd/MM/yyyy")
	@FilterPersistenceAnnotation(persistenceName="FECHA_PAGO", truncateDate=true)
	public Date getFechaPago() {
		return fechaPago;
	}


	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}


    @Exportable(columnName="UUID FACTURA", columnOrder=8)
	@FilterPersistenceAnnotation(persistenceName="UUID_FACTURA")
	public String getUuidFactura() {
		return uuidFactura;
	}

	public void setUuidFactura(String uuidFactura) {
		this.uuidFactura = uuidFactura;
	}

	@FilterPersistenceAnnotation(persistenceName="UUID_COMPLEMENTO")
	public String getUuidComplementoPago() {
		return uuidComplementoPago;
	}


	public void setUuidComplementoPago(String uuidComplementoPago) {
		this.uuidComplementoPago = uuidComplementoPago;
	}

	@FilterPersistenceAnnotation(persistenceName="IMPORTE_PAGADO")
	public BigDecimal getImportePagado() {
		return importePagado;
	}


	public void setImportePagado(BigDecimal importePagado) {
		this.importePagado = importePagado;
	}
	
    @Exportable(columnName="MONTO RESTANTE", columnOrder=9)
	@FilterPersistenceAnnotation(persistenceName="MONTO_RESTANTE")
	public BigDecimal getImporteRestante() {
		return importeRestante;
	}


	public void setImporteRestante(BigDecimal importeRestante) {
		this.importeRestante = importeRestante;
	}


	public String getFechaPagoString() {
		DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		return this.getFechaPago() != null ? f.format(this.getFechaPago()) :null;
	}

	public void setFechaPagoString(String stringDate) throws ParseException {
		DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		if(stringDate != null && !stringDate.equals("")){
			Date date = new Date();
			date = f.parse(stringDate);
			this.setFechaPago(date);
		}
	}		
}
