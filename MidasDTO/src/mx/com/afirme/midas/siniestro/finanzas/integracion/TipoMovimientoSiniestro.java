package mx.com.afirme.midas.siniestro.finanzas.integracion;

public class TipoMovimientoSiniestro {
	public static final int CUENTA_POR_PAGAR  = 1;
	public static final int CUENTA_POR_COBRAR = 2;	
	
	public TipoMovimientoSiniestro(){
		
	}
}
