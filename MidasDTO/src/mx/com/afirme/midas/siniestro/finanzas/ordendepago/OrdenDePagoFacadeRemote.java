package mx.com.afirme.midas.siniestro.finanzas.ordendepago;

// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import mx.com.afirme.midas.siniestro.finanzas.pagos.SoportePagosDTO;

/**
 * Remote interface for OrdenPagoFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface OrdenDePagoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved OrdenDePagoDTO entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            OrdenDePagoDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public OrdenDePagoDTO save(OrdenDePagoDTO entity);

	/**
	 * Delete a persistent OrdenDePagoDTO entity.
	 * 
	 * @param entity
	 *            OrdenDePagoDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(OrdenDePagoDTO entity);

	/**
	 * Persist a previously saved OrdenDePagoDTO entity and return it or a copy
	 * of it to the sender. A copy of the OrdenDePagoDTO entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            OrdenDePagoDTO entity to update
	 * @return OrdenDePagoDTO the persisted OrdenDePagoDTO entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public OrdenDePagoDTO update(OrdenDePagoDTO entity);

	public OrdenDePagoDTO findById(BigDecimal id);

	/**
	 * Find all OrdenDePagoDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the OrdenDePagoDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<OrdenDePagoDTO> found by query
	 */
	public List<OrdenDePagoDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all OrdenDePagoDTO entities.
	 * 
	 * @return List<OrdenDePagoDTO> all OrdenDePagoDTO entities
	 */
	public List<OrdenDePagoDTO> findAll();

	public List<OrdenDePagoDTO> obtenerOrdenesDePagoXReporteSiniestro(
			BigDecimal idToReporteSiniestro);
	
	/**
	 * Obtiene las ordenes de pago individuales, esto es las que no son ordenes
	 * de pago agrupadas
	 * 
	 * @return List<OrdenDePagoDTO> OrdenDePagoDTO entities
	 */
	public List<OrdenDePagoDTO> obtenerOrdenesDePagoXReporteSiniestroIndividual(BigDecimal idAutorizacionTecnica);


	public OrdenDePagoDTO obtenerOrdenPorIdAutorizacionTecnica(
			BigDecimal idAutorizacionTecnica);

	// public void actualizaOrdenCheque(BigDecimal idOrdenPago, String
	// estatusSolicitud) throws Exception;

	public OrdenDePagoDTO obtenerOrdenNoCanceladaPorIdAutorizacionTecnica(
			BigDecimal idAutorizacionTecnica);

	public OrdenDePagoDTO buscaPrimerRegistro();

	public Map<String, String> generarOrdenDePago(String usuario,
			BigDecimal... autorizaciones);

	public List<DetalleOrdenPagoDTO> listarOrdenesDePagoFiltrado(
			SoportePagosDTO soportePagosDTO);

	public List<DetalleOrdenPagoDTO> buscarDetallePorAutorizacionTecnica(
			BigDecimal idToAutorizacionTecnica);

	public void agregarDetalleOrdenDePago(DetalleOrdenPagoDTO detalle);
}