package mx.com.afirme.midas.siniestro.finanzas.ordendepago;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDTO;

/**
 * OrdenDePagoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOORDENDEPAGO", schema = "MIDAS")
public class OrdenDePagoDTO implements java.io.Serializable {

	public static final short ESTATUS_PENDIENTE = 0;
	public static final short ESTATUS_PAGADA = 1;
	public static final short ESTATUS_CANCELADA = 2;

	private static final long serialVersionUID = 1L;
	// Fields
	private BigDecimal idToOrdenPago;
	private String numeroOrdenPago;
	private AutorizacionTecnicaDTO autorizacionTecnica;
	private Short estatus;
	private BigDecimal idSolicitudCheque;

	private String cuentaBancariaEmisora;
	private String tipo;
	private BigDecimal folio;
	private Date fechaPago;
	private List<DetalleOrdenPagoDTO> detalleOrdenDePago = new ArrayList<DetalleOrdenPagoDTO>();
	private String descripcionEstatus;
	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTOORDENDEPAGO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOORDENDEPAGO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOORDENDEPAGO_SEQ_GENERADOR")
	@Column(name = "IDTOORDENPAGO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToOrdenPago() {
		return this.idToOrdenPago;
	}

	public void setIdToOrdenPago(BigDecimal idToOrdenPago) {
		this.idToOrdenPago = idToOrdenPago;
	}

	@Transient
	public String getNumeroOrdenPago() {
		return numeroOrdenPago;
	}

	public void setNumeroOrdenPago(String numeroOrdenPago) {
		this.numeroOrdenPago = numeroOrdenPago;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOAUTORIZACIONTECNICA", nullable = true)
	public AutorizacionTecnicaDTO getAutorizacionTecnica() {
		return this.autorizacionTecnica;
	}

	public void setAutorizacionTecnica(
			AutorizacionTecnicaDTO autorizacionTecnica) {
		this.autorizacionTecnica = autorizacionTecnica;
	}

	@Column(name = "ESTATUS", nullable = false, precision = 1, scale = 0)
	public Short getEstatus() {
		return this.estatus;
	}

	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}

	@Column(name = "IDSOLICITUDCHEQUE", precision = 22, scale = 0)
	public BigDecimal getIdSolicitudCheque() {
		return this.idSolicitudCheque;
	}

	public void setIdSolicitudCheque(BigDecimal idSolicitudCheque) {
		this.idSolicitudCheque = idSolicitudCheque;
	}

	@Column(name = "CUENTABANCARIAEMISORA", length = 30)
	public String getCuentaBancariaEmisora() {
		return this.cuentaBancariaEmisora;
	}

	public void setCuentaBancariaEmisora(String cuentaBancariaEmisora) {
		this.cuentaBancariaEmisora = cuentaBancariaEmisora;
	}

	@Column(name = "TIPO", length = 2)
	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	@Column(name = "FOLIO", precision = 22, scale = 0)
	public BigDecimal getFolio() {
		return this.folio;
	}

	public void setFolio(BigDecimal folio) {
		this.folio = folio;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAPAGO", length = 7)
	public Date getFechaPago() {
		return this.fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "ordenDePagoDTO")
	public List<DetalleOrdenPagoDTO> getDetalleOrdenDePago() {
		return detalleOrdenDePago;
	}

	public void setDetalleOrdenDePago(
			List<DetalleOrdenPagoDTO> detalleOrdenDePago) {
		this.detalleOrdenDePago = detalleOrdenDePago;
	}
	@Transient
	public String getDescripcionEstatus(){
		if(this.getEstatus() == ESTATUS_PENDIENTE){
			this.descripcionEstatus = "PENDIENTE";
		}else if (this.getEstatus() == ESTATUS_PAGADA){
			this.descripcionEstatus = "PAGADO";
		}else if (this.getEstatus() == ESTATUS_CANCELADA){
			this.descripcionEstatus = "CANCELADA";
		}else{
			this.descripcionEstatus = "NO DISPONIBLE";
		}
		return this.descripcionEstatus;
	}
}