package mx.com.afirme.midas.siniestro.finanzas.indemnizacion;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for IndemnizacionRiesgoCoberturaDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface IndemnizacionRiesgoCoberturaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved IndemnizacionRiesgoCoberturaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity IndemnizacionRiesgoCoberturaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(IndemnizacionRiesgoCoberturaDTO entity);
    /**
	 Delete a persistent IndemnizacionRiesgoCoberturaDTO entity.
	  @param entity IndemnizacionRiesgoCoberturaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(IndemnizacionRiesgoCoberturaDTO entity);
   /**
	 Persist a previously saved IndemnizacionRiesgoCoberturaDTO entity and return it or a copy of it to the sender. 
	 A copy of the IndemnizacionRiesgoCoberturaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity IndemnizacionRiesgoCoberturaDTO entity to update
	 @return IndemnizacionRiesgoCoberturaDTO the persisted IndemnizacionRiesgoCoberturaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public IndemnizacionRiesgoCoberturaDTO update(IndemnizacionRiesgoCoberturaDTO entity);
	public IndemnizacionRiesgoCoberturaDTO findById( IndemnizacionRiesgoCoberturaId id);
	 /**
	 * Find all IndemnizacionRiesgoCoberturaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the IndemnizacionRiesgoCoberturaDTO property to query
	  @param value the property value to match
	  	  @return List<IndemnizacionRiesgoCoberturaDTO> found by query
	 */
	public List<IndemnizacionRiesgoCoberturaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all IndemnizacionRiesgoCoberturaDTO entities.
	  	  @return List<IndemnizacionRiesgoCoberturaDTO> all IndemnizacionRiesgoCoberturaDTO entities
	 */
	public List<IndemnizacionRiesgoCoberturaDTO> findAll(
		);
	
	public Double getTotalCoaseguroPorIdAutorizacionTecnica(BigDecimal idToIndemnizacion);
	
	public Double getTotalDeduciblePorIdAutorizacionTecnica(BigDecimal idToIndemnizacion);
	
	public Double obtenerMontoTotalIndeminizacionCobertura(BigDecimal idToPoliza, BigDecimal numeroInciso,
			BigDecimal numeroSubInciso, BigDecimal idToSeccion, BigDecimal idToCobertura);
	
	public Double[] sumaPagosParcialesAgregar(BigDecimal idToReporteSiniestro);
		
	public Double[] sumaPagosParcialesModificar(BigDecimal idToReporteSiniestro,BigDecimal idToIndemnizacion);
	
	public Double sumaPerdidaDeterminadaPorReporte(BigDecimal idToReporteSiniestro);
	
}