// default package
package mx.com.afirme.midas.siniestro.finanzas.ordendepago;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDTO;

/**
 * DetalleOrdenPagoDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TODETALLEORDENDEPAGO", schema = "MIDAS")
public class DetalleOrdenPagoDTO implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields

	private BigDecimal idToDetalleOrdenPago;
	private AutorizacionTecnicaDTO autorizacionTecnicaDTO;
	private OrdenDePagoDTO ordenDePagoDTO;

	// Constructors

	/** default constructor */
	public DetalleOrdenPagoDTO() {
	}

	/** full constructor */
	public DetalleOrdenPagoDTO(BigDecimal idToDetalleOrdenPago,
			AutorizacionTecnicaDTO autorizacionTecnicaDTO,
			OrdenDePagoDTO ordenDePagoDTO) {
		this.idToDetalleOrdenPago = idToDetalleOrdenPago;
		this.autorizacionTecnicaDTO = autorizacionTecnicaDTO;
		this.ordenDePagoDTO = ordenDePagoDTO;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTODETALLEORDENDEPAGO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTODETALLEORDENDEPAGO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTODETALLEORDENDEPAGO_SEQ_GENERADOR")
	@Column(name = "IDTODETALLEORDENDEPAGO", unique = true, nullable = false, precision = 22, scale = 0)	
	public BigDecimal getIdToDetalleOrdenPago() {
		return this.idToDetalleOrdenPago;
	}

	public void setIdToDetalleOrdenPago(BigDecimal idToDetalleOrdenPago) {
		this.idToDetalleOrdenPago = idToDetalleOrdenPago;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOAUTORIZACIONTECNICA", nullable = false)
	public AutorizacionTecnicaDTO getAutorizacionTecnicaDTO() {
		return this.autorizacionTecnicaDTO;
	}

	public void setAutorizacionTecnicaDTO(
			AutorizacionTecnicaDTO autorizacionTecnicaDTO) {
		this.autorizacionTecnicaDTO = autorizacionTecnicaDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOORDENPAGO", nullable = false) 
	public OrdenDePagoDTO getOrdenDePagoDTO() {
		return this.ordenDePagoDTO;
	}

	public void setOrdenDePagoDTO(OrdenDePagoDTO ordenDePagoDTO) {
		this.ordenDePagoDTO = ordenDePagoDTO;
	}

}