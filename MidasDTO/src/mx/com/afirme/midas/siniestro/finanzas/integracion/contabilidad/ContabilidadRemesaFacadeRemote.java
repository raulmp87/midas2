package mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for ContabilidadRemesaFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ContabilidadRemesaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ContabilidadRemesa entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ContabilidadRemesa entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ContabilidadRemesaDTO entity);
    /**
	 Delete a persistent ContabilidadRemesa entity.
	  @param entity ContabilidadRemesa entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ContabilidadRemesaDTO entity);
   /**
	 Persist a previously saved ContabilidadRemesa entity and return it or a copy of it to the sender. 
	 A copy of the ContabilidadRemesa entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ContabilidadRemesa entity to update
	 @return ContabilidadRemesa the persisted ContabilidadRemesa entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ContabilidadRemesaDTO update(ContabilidadRemesaDTO entity);
	
	public ContabilidadRemesaDTO findById( ContabilidadRemesaId id);
	 /**
	 * Find all ContabilidadRemesa entities with a specific property value.  
	 
	  @param propertyName the name of the ContabilidadRemesa property to query
	  @param value the property value to match
	  	  @return List<ContabilidadRemesa> found by query
	 */
	public List<ContabilidadRemesaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ContabilidadRemesa entities.
	  	  @return List<ContabilidadRemesa> all ContabilidadRemesa entities
	 */
	public List<ContabilidadRemesaDTO> findAll();
	
	public List<ContabilidadRemesaDTO> buscarMovimientosPendientesActualizar(BigDecimal idToAplicacionIngreso);
}