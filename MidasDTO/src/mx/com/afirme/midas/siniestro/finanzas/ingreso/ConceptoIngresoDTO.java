package mx.com.afirme.midas.siniestro.finanzas.ingreso;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas.base.CacheableDTO;

/**
 * ConceptoIngreso entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCCONCEPTOINGRESO", schema = "MIDAS")
public class ConceptoIngresoDTO extends CacheableDTO implements java.io.Serializable {

	public static final short DEDUCIBLE = 1;
	public static final short COASEGURO = 2;
	public static final short RECUPERACION = 3;
	public static final short SALVAMENTO = 4;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idTcConceptoIngreso;
	private String descripcion;

	// Constructors

	/** default constructor */
	public ConceptoIngresoDTO() {
	}

	// Property accessors
	@Id
	@Column(name = "IDTCCONCEPTOINGRESO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcConceptoIngreso() {
		return this.idTcConceptoIngreso;
	}

	public void setIdTcConceptoIngreso(BigDecimal idTcConceptoIngreso) {
		this.idTcConceptoIngreso = idTcConceptoIngreso;
	}

	@Column(name = "DESCRIPCION", length = 20)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Override
	public BigDecimal getId() {
		return this.idTcConceptoIngreso;
	}
	@Override
	public String getDescription() {
		return this.descripcion;
	}
	
	@Override
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof ConceptoIngresoDTO) {
			ConceptoIngresoDTO conceptoIngresoDTO = (ConceptoIngresoDTO) object;
			equal = conceptoIngresoDTO.getIdTcConceptoIngreso().equals(this.getIdTcConceptoIngreso());
		} // End of if
		return equal;
	}
}