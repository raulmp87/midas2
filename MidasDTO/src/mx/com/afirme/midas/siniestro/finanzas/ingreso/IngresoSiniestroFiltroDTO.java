package mx.com.afirme.midas.siniestro.finanzas.ingreso;

import java.io.Serializable;
import java.math.BigDecimal;

public class IngresoSiniestroFiltroDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private String numeroReporte;
	private String fechaIngreso;
	private Double monto;
	private BigDecimal idConceptoIngreso;
	
	public IngresoSiniestroFiltroDTO(){
		this.numeroReporte = null;
		this.fechaIngreso = null;
		this.monto = null;
		this.idConceptoIngreso = null;		
	}

	public void setNumeroReporte(String numeroReporte) {
		this.numeroReporte = numeroReporte;
	}

	public String getNumeroReporte() {
		return numeroReporte;
	}

	public void setFechaIngreso(String fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getFechaIngreso() {
		return fechaIngreso;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}

	public Double getMonto() {
		return monto;
	}

	public void setIdConceptoIngreso(BigDecimal idConceptoIngreso) {
		this.idConceptoIngreso = idConceptoIngreso;
	}

	public BigDecimal getIdConceptoIngreso() {
		return idConceptoIngreso;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("");
		sb.append("IngresoSiniestroFiltroDTO { " + '\n');		
		sb.append("numeroReporte = " + numeroReporte + '\n');
		
		if (fechaIngreso != null) {
			sb.append("fechaIngreso = " + fechaIngreso.toString() + '\n');
		} else {
			sb.append("fechaIngreso = null " + '\n');
		}
		
		if (monto != null) {
			sb.append("monto = " + monto.toString()	+ '\n');
		} else {
			sb.append("monto = null " + '\n');
		}
		
		if (idConceptoIngreso != null) {
			sb.append("idConceptoIngreso = " + idConceptoIngreso.toString() + '\n');
		} else {
			sb.append("idConceptoIngreso = null " + '\n');
		}		
		
		sb.append("}" + '\n');

		return sb.toString();
	}	
}
