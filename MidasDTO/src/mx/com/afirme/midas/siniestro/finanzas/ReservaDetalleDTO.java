package mx.com.afirme.midas.siniestro.finanzas;
// default package

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * ReservaDetalleDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TORESERVAESTIMADADETALLE", schema = "MIDAS")
public class ReservaDetalleDTO implements java.io.Serializable {
	public static final Byte ACTIVO   = 1;
	public static final Byte INACTIVO = 0;

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ReservaDetalleId id;
	private RiesgoAfectadoDTO riesgoAfectadoDTO;
	private ReservaDTO reservaDTO;
	private Double estimacion;
	private Double ajusteMas;
	private Double ajusteMenos;
	private Byte estatus;

	// Constructors

	/** default constructor */
	public ReservaDetalleDTO() {
	}


	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idtoreservaestimada", column = @Column(name = "IDTORESERVAESTIMADA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idtoreportesiniestro", column = @Column(name = "IDTOREPORTESINIESTRO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idtopoliza", column = @Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numeroinciso", column = @Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idtoriesgo", column = @Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idtocobertura", column = @Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idtoseccion", column = @Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "numerosubinciso", column = @Column(name = "NUMEROSUBINCISO", nullable = false, precision = 22, scale = 0)) })
	public ReservaDetalleId getId() {
		return this.id;
	}

	public void setId(ReservaDetalleId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumns( {
			@JoinColumn(name = "IDTOREPORTESINIESTRO", referencedColumnName = "IDTOREPORTESINIESTRO", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTOPOLIZA", referencedColumnName = "IDTOPOLIZA", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "NUMEROINCISO", referencedColumnName = "NUMEROINCISO", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTOSECCION", referencedColumnName = "IDTOSECCION", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTOCOBERTURA", referencedColumnName = "IDTOCOBERTURA", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "IDTORIESGO", referencedColumnName = "IDTORIESGO", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "NUMEROSUBINCISO", referencedColumnName = "NUMEROSUBINCISO", nullable = false, insertable = false, updatable = false) })
	public RiesgoAfectadoDTO getRiesgoAfectadoDTO() {
		return this.riesgoAfectadoDTO;
	}

	public void setRiesgoAfectadoDTO(RiesgoAfectadoDTO riesgoAfectadoDTO) {
		this.riesgoAfectadoDTO = riesgoAfectadoDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTORESERVAESTIMADA", nullable = false, insertable = false, updatable = false)
	public ReservaDTO getReservaDTO() {
		return this.reservaDTO;
	}

	public void setReservaDTO(ReservaDTO reservaDTO) {
		this.reservaDTO = reservaDTO;
	}

	@Column(name = "ESTIMACION", nullable = false, precision = 16)
	public Double getEstimacion() {
		return this.estimacion;
	}

	public void setEstimacion(Double estimacion) {
		this.estimacion = estimacion;
	}
	@Column(name = "AJUSTEMAS", nullable = false, precision = 16)
	public Double getAjusteMas() {
		return this.ajusteMas;
	}

	public void setAjusteMas(Double ajusteMas) {
		this.ajusteMas = ajusteMas;
	}
	@Column(name = "AJUSTEMENOS", nullable = false, precision = 16)
	public Double getAjusteMenos() {
		return this.ajusteMenos;
	}

	public void setAjusteMenos(Double ajusteMenos) {
		this.ajusteMenos = ajusteMenos;
	}

	@Column(name="ESTATUS", nullable=false, precision=22, scale=0)
    public Byte getEstatus() {
        return this.estatus;
    }
    
    public void setEstatus(Byte estatus) {
        this.estatus = estatus;
    }
}