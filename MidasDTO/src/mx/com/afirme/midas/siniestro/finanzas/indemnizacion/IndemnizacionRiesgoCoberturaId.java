package mx.com.afirme.midas.siniestro.finanzas.indemnizacion;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * IndemnizacionRiesgoCoberturaDTOId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class IndemnizacionRiesgoCoberturaId  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
    // Fields    

     private BigDecimal idToIndemnizacion;
     private BigDecimal numeroInciso;
     private BigDecimal idToReporteSiniestro;
     private BigDecimal idToSeccion;
     private BigDecimal numeroSubinciso;
     private BigDecimal idToCobertura;
     private BigDecimal idToRiesgo;
     private BigDecimal idToPoliza;


    // Constructors

    /** default constructor */
    public IndemnizacionRiesgoCoberturaId() {
    }

    // Property accessors

    @Column(name="IDTOINDEMNIZACION", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToIndemnizacion() {
        return this.idToIndemnizacion;
    }
    
    public void setIdToIndemnizacion(BigDecimal idToIndemnizacion) {
        this.idToIndemnizacion = idToIndemnizacion;
    }

    @Column(name="NUMEROINCISO", nullable=false, precision=22, scale=0)

    public BigDecimal getNumeroInciso() {
        return this.numeroInciso;
    }
    
    public void setNumeroInciso(BigDecimal numeroInciso) {
        this.numeroInciso = numeroInciso;
    }

    @Column(name="IDTOREPORTESINIESTRO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToReporteSiniestro() {
        return this.idToReporteSiniestro;
    }
    
    public void setIdToReporteSiniestro(BigDecimal idToReporteSiniestro) {
        this.idToReporteSiniestro = idToReporteSiniestro;
    }

    @Column(name="IDTOSECCION", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToSeccion() {
        return this.idToSeccion;
    }
    
    public void setIdToSeccion(BigDecimal idToSeccion) {
        this.idToSeccion = idToSeccion;
    }

    @Column(name="NUMEROSUBINCISO", nullable=false, precision=22, scale=0)

    public BigDecimal getNumeroSubinciso() {
        return this.numeroSubinciso;
    }
    
    public void setNumeroSubinciso(BigDecimal numeroSubinciso) {
        this.numeroSubinciso = numeroSubinciso;
    }

    @Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToCobertura() {
        return this.idToCobertura;
    }
    
    public void setIdToCobertura(BigDecimal idToCobertura) {
        this.idToCobertura = idToCobertura;
    }

    @Column(name="IDTORIESGO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToRiesgo() {
        return this.idToRiesgo;
    }
    
    public void setIdToRiesgo(BigDecimal idToRiesgo) {
        this.idToRiesgo = idToRiesgo;
    }

    @Column(name="IDTOPOLIZA", nullable=false, precision=22, scale=0)

    public BigDecimal getIdToPoliza() {
        return this.idToPoliza;
    }
    
    public void setIdToPoliza(BigDecimal idToPoliza) {
        this.idToPoliza = idToPoliza;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof IndemnizacionRiesgoCoberturaId) ) return false;
		 IndemnizacionRiesgoCoberturaId castOther = ( IndemnizacionRiesgoCoberturaId ) other; 
         
		 return ( (this.getIdToIndemnizacion()==castOther.getIdToIndemnizacion()) || ( this.getIdToIndemnizacion()!=null && castOther.getIdToIndemnizacion()!=null && this.getIdToIndemnizacion().equals(castOther.getIdToIndemnizacion()) ) )
 && ( (this.getNumeroInciso()==castOther.getNumeroInciso()) || ( this.getNumeroInciso()!=null && castOther.getNumeroInciso()!=null && this.getNumeroInciso().equals(castOther.getNumeroInciso()) ) )
 && ( (this.getIdToReporteSiniestro()==castOther.getIdToReporteSiniestro()) || ( this.getIdToReporteSiniestro()!=null && castOther.getIdToReporteSiniestro()!=null && this.getIdToReporteSiniestro().equals(castOther.getIdToReporteSiniestro()) ) )
 && ( (this.getIdToSeccion()==castOther.getIdToSeccion()) || ( this.getIdToSeccion()!=null && castOther.getIdToSeccion()!=null && this.getIdToSeccion().equals(castOther.getIdToSeccion()) ) )
 && ( (this.getNumeroSubinciso()==castOther.getNumeroSubinciso()) || ( this.getNumeroSubinciso()!=null && castOther.getNumeroSubinciso()!=null && this.getNumeroSubinciso().equals(castOther.getNumeroSubinciso()) ) )
 && ( (this.getIdToCobertura()==castOther.getIdToCobertura()) || ( this.getIdToCobertura()!=null && castOther.getIdToCobertura()!=null && this.getIdToCobertura().equals(castOther.getIdToCobertura()) ) )
 && ( (this.getIdToRiesgo()==castOther.getIdToRiesgo()) || ( this.getIdToRiesgo()!=null && castOther.getIdToRiesgo()!=null && this.getIdToRiesgo().equals(castOther.getIdToRiesgo()) ) )
 && ( (this.getIdToPoliza()==castOther.getIdToPoliza()) || ( this.getIdToPoliza()!=null && castOther.getIdToPoliza()!=null && this.getIdToPoliza().equals(castOther.getIdToPoliza()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdToIndemnizacion() == null ? 0 : this.getIdToIndemnizacion().hashCode() );
         result = 37 * result + ( getNumeroInciso() == null ? 0 : this.getNumeroInciso().hashCode() );
         result = 37 * result + ( getIdToReporteSiniestro() == null ? 0 : this.getIdToReporteSiniestro().hashCode() );
         result = 37 * result + ( getIdToSeccion() == null ? 0 : this.getIdToSeccion().hashCode() );
         result = 37 * result + ( getNumeroSubinciso() == null ? 0 : this.getNumeroSubinciso().hashCode() );
         result = 37 * result + ( getIdToCobertura() == null ? 0 : this.getIdToCobertura().hashCode() );
         result = 37 * result + ( getIdToRiesgo() == null ? 0 : this.getIdToRiesgo().hashCode() );
         result = 37 * result + ( getIdToPoliza() == null ? 0 : this.getIdToPoliza().hashCode() );
         return result;
   }   

}