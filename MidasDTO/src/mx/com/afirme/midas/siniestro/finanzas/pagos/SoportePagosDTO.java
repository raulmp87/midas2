package mx.com.afirme.midas.siniestro.finanzas.pagos;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SoportePagosDTO implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String numeroReporte;
	private String nombreAsegurado;
	private String nombreProveedor;
	private String numeroOrdenDePago;
	private Date fechaDesde;
	private Date fechaHasta;
	private String idMoneda;
	private String idPrestadorServicios;
	private String idsAutorizaciones;
	private String estatus;

	public String getNumeroReporte() {
		return numeroReporte;
	}

	public void setNumeroReporte(String numeroReporte) {
		this.numeroReporte = numeroReporte;
	}

	public String getNombreAsegurado() {
		return nombreAsegurado;
	}

	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}

	public String getNombreProveedor() {
		return nombreProveedor;
	}

	public void setNombreProveedor(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}

	public Date getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Date getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public String getFechaHastaString() {
		DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		return this.getFechaHasta() != null ? f.format(this.getFechaHasta()) : null;
	}

	public void setFechaHastaString(String stringDate) throws ParseException {
		DateFormat f = new SimpleDateFormat("dd/MM/yyyy");		
		if(stringDate != null && !stringDate.equals("")){
			Date date = new Date();
			date = f.parse(stringDate);
			this.setFechaHasta(date);
		}
	}

	public String getFechaDesdeString() {
		DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		return this.getFechaDesde() != null ? f.format(this.getFechaDesde()) :null;
	}

	public void setFechaDesdeString(String stringDate) throws ParseException {
		DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		if(stringDate != null && !stringDate.equals("")){
			Date date = new Date();
			date = f.parse(stringDate);
			this.setFechaDesde(date);
		}
	}	
	
	public String getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(String idMoneda) {
		this.idMoneda = idMoneda;
	}

	public String getIdPrestadorServicios() {
		return idPrestadorServicios;
	}

	public void setIdPrestadorServicios(String idPrestadorServicios) {
		this.idPrestadorServicios = idPrestadorServicios;
	}

	public String getNumeroOrdenDePago() {
		return numeroOrdenDePago;
	}

	public void setNumeroOrdenDePago(String numeroOrdenDePago) {
		this.numeroOrdenDePago = numeroOrdenDePago;
	}

	public String getIdsAutorizaciones() {
		return idsAutorizaciones;
	}

	public void setIdsAutorizaciones(String idsAutorizaciones) {
		this.idsAutorizaciones = idsAutorizaciones;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

}
