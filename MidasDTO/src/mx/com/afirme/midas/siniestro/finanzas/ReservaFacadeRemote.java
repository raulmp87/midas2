package mx.com.afirme.midas.siniestro.finanzas;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionRiesgoCoberturaDTO;

/**
 * Remote interface for ReservaFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ReservaFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved ReservaDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            ReservaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public ReservaDTO save(ReservaDTO entity);
	

	/**
	 * Delete a persistent ReservaDTO entity.
	 * 
	 * @param entity
	 *            ReservaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ReservaDTO entity);

	/**
	 * Persist a previously saved ReservaDTO entity and return it or a copy of it
	 * to the sender. A copy of the ReservaDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            ReservaDTO entity to update
	 * @return ReservaDTO the persisted ReservaDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ReservaDTO update(ReservaDTO entity);

	public ReservaDTO findById(BigDecimal id);

	/**
	 * Find all ReservaDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ReservaDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<ReservaDTO> found by query
	 */
	public List<ReservaDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all ReservaDTO entities.
	 * 
	 * @return List<ReservaDTO> all ReservaDTO entities
	 */
	public List<ReservaDTO> findAll();
	
	public List<ReservaDTO> listarPorFechaEstimacion(BigDecimal idReporteSiniestro);
	
	/**
	 * Obtiene el registro de la reserva estimana mas actual de un reporte de siniestro.
	 * 
	 * @param idToReporteSiniestro
	 * 			Represnta el numero de reporte del siniestro
	 * @param autorizada
	 * 			Indica si recupera la reserva que ya ha sido autorizada (True)
	 * @return ReservaDTO 
	 */	
	public List<ReservaDTO> obtenerUltimaReserva(BigDecimal idToReporteSiniestro, Boolean autorizada);
	
	public ReservaDTO obtenerUltimaReservaCreada(BigDecimal idToReporteSiniestro);
	
	public ReservaDTO obtenReservaEstimacionInicial(BigDecimal idReporteSiniestro);
	
	public ReservaDTO ajustarReservayRiesgosAfectados(List<RiesgoAfectadoDTO> riesgosAfectados, ReservaDTO reserva, List<ReservaDetalleDTO> reservaDetalle);
	
	public boolean ajustarEstimacionInicialyRiesgosAfectados(List<RiesgoAfectadoDTO> riesgosAfectados, List<ReservaDetalleDTO> reservaDetalle);
	
	public ReservaDTO ajustarReservaeIndemnizacion(IndemnizacionDTO indemnizacionDTO, List<IndemnizacionRiesgoCoberturaDTO> listaDetalleIndemnizacion, ReservaDTO reserva, List<ReservaDetalleDTO> reservaDetalle);
	
	public ReservaDTO ajustarReservaeModificacionIndemnizacion(IndemnizacionDTO indemnizacionDTO, List<IndemnizacionRiesgoCoberturaDTO> listaDetalleIndemnizacion, ReservaDTO reserva, List<ReservaDetalleDTO> reservaDetalle);
	
	public ReservaDTO ajustarReservaCancelarIndemnizacion(IndemnizacionDTO indemnizacionDTO, ReservaDTO reserva, List<ReservaDetalleDTO> reservaDetalle);
	
	public ReservaDTO ajustarReservaEliminarIndemnizacion(IndemnizacionDTO indemnizacionDTO, ReservaDTO reserva, List<ReservaDetalleDTO> reservaDetalle);
}