package mx.com.afirme.midas.siniestro.finanzas.indemnizacion;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.siniestro.finanzas.RiesgoAfectadoDTO;

/**
 * IndemnizacionRiesgoCoberturaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOINDEMNIZACIONRIESGOCOBERTURA"
    ,schema="MIDAS"
)

public class IndemnizacionRiesgoCoberturaDTO  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
    // Fields    

     private IndemnizacionRiesgoCoberturaId id;
     private RiesgoAfectadoDTO riesgoAfectadoDTO;
     private IndemnizacionDTO indemnizacionDTO;
     private Double montoPago;
     private Double deducible;
     private Double coaseguro;
    // Constructors

    /** default constructor */
    public IndemnizacionRiesgoCoberturaDTO() {
    }
   
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idToIndemnizacion", column=@Column(name="IDTOINDEMNIZACION", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="numeroInciso", column=@Column(name="NUMEROINCISO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idToReporteSiniestro", column=@Column(name="IDTOREPORTESINIESTRO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idToSeccion", column=@Column(name="IDTOSECCION", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="numeroSubinciso", column=@Column(name="NUMEROSUBINCISO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idToCobertura", column=@Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idToRiesgo", column=@Column(name="IDTORIESGO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idToPoliza", column=@Column(name="IDTOPOLIZA", nullable=false, precision=22, scale=0) ) } )

    public IndemnizacionRiesgoCoberturaId getId() {
        return this.id;
    }
    
    public void setId(IndemnizacionRiesgoCoberturaId id) {
        this.id = id;
    }
    
    @ManyToOne(fetch = FetchType.EAGER)
        @JoinColumns( { 
        @JoinColumn(name="IDTOREPORTESINIESTRO", referencedColumnName="IDTOREPORTESINIESTRO", nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="IDTOPOLIZA", referencedColumnName="IDTOPOLIZA", nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="NUMEROINCISO", referencedColumnName="NUMEROINCISO", nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="IDTOSECCION", referencedColumnName="IDTOSECCION", nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="IDTOCOBERTURA", referencedColumnName="IDTOCOBERTURA", nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="IDTORIESGO", referencedColumnName="IDTORIESGO", nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="NUMEROSUBINCISO", referencedColumnName="NUMEROSUBINCISO", nullable=false, insertable=false, updatable=false) } )
    
    public RiesgoAfectadoDTO getRiesgoAfectadoDTO() {
		return this.riesgoAfectadoDTO;
	}

	public void setRiesgoAfectadoDTO(RiesgoAfectadoDTO riesgoAfectadoDTO) {
		this.riesgoAfectadoDTO = riesgoAfectadoDTO;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="IDTOINDEMNIZACION", nullable=false, insertable=false, updatable=false)

    public IndemnizacionDTO getIndemnizacionDTO() {
        return this.indemnizacionDTO;
    }
    
    public void setIndemnizacionDTO(IndemnizacionDTO indemnizacionDTO) {
        this.indemnizacionDTO = indemnizacionDTO;
    }
    
    @Column(name="MONTOPAGO", nullable=false, precision=16)

    public Double getMontoPago() {
        return this.montoPago;
    }
    
    public void setMontoPago(Double montoPago) {
        this.montoPago = montoPago;
    }
    
    @Column(name="DEDUCIBLE", precision=16)

    public Double getDeducible() {
        return this.deducible;
    }
    
    public void setDeducible(Double deducible) {
        this.deducible = deducible;
    }
    
    @Column(name="COASEGURO", precision=16)

    public Double getCoaseguro() {
        return this.coaseguro;
    }
    
    public void setCoaseguro(Double coaseguro) {
        this.coaseguro = coaseguro;
    }
    
 
}