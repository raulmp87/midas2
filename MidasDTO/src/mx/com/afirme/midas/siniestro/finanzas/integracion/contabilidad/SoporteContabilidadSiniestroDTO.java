package mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad;

import java.io.Serializable;
import java.math.BigDecimal;

public class SoporteContabilidadSiniestroDTO implements Serializable{
	public static final String CLAVE_MOVIMIENTO_ESTIMACION_ORIGINAL = "EO";
	public static final String CLAVE_MOVIMIENTO_AJUSTE_AUMENTO = "AA";
	public static final String CLAVE_MOVIMIENTO_AJUSTE_DISMINUCION = "AD";
	
	public static final String CONCEPTO_MOVIMIENTO_ESTIMACION_ORIGINAL = "Estimacion Original";
	public static final String CONCEPTO_MOVIMIENTO_AJUSTE_AUMENTO = "Ajuste Aumento";
	public static final String CONCEPTO_MOVIMIENTO_AJUSTE_DISMINUCION = "Ajuste Disminucion";
			
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToReporteSiniestro;
	private BigDecimal idTcMovimientoSiniestro;
	private BigDecimal idRegistro;
	private BigDecimal idTcSubRamo;
	private BigDecimal idTcRamo;
	private BigDecimal codigoRamo;
	private BigDecimal codigoSubRamo;
	private BigDecimal idToReservaEstimada;
	private String cveConceptoMov;
	private String conceptoMov; 
	private Double estimacion;	
	
	public SoporteContabilidadSiniestroDTO(){	
	}

	public void setIdToReporteSiniestro(BigDecimal idToReporteSiniestro) {
		this.idToReporteSiniestro = idToReporteSiniestro;
	}

	public BigDecimal getIdToReporteSiniestro() {
		return idToReporteSiniestro;
	}

	public void setIdTcMovimientoSiniestro(BigDecimal idTcMovimientoSiniestro) {
		this.idTcMovimientoSiniestro = idTcMovimientoSiniestro;
	}

	public BigDecimal getIdTcMovimientoSiniestro() {
		return idTcMovimientoSiniestro;
	}

	public void setIdRegistro(BigDecimal idRegistro) {
		this.idRegistro = idRegistro;
	}

	public BigDecimal getIdRegistro() {
		return idRegistro;
	}

	public void setIdTcSubRamo(BigDecimal idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}

	public BigDecimal getIdTcSubRamo() {
		return idTcSubRamo;
	}

	public void setIdTcRamo(BigDecimal idTcRamo) {
		this.idTcRamo = idTcRamo;
	}

	public BigDecimal getIdTcRamo() {
		return idTcRamo;
	}

	public void setCodigoRamo(BigDecimal codigoRamo) {
		this.codigoRamo = codigoRamo;
	}

	public BigDecimal getCodigoRamo() {
		return codigoRamo;
	}

	public void setCodigoSubRamo(BigDecimal codigoSubRamo) {
		this.codigoSubRamo = codigoSubRamo;
	}

	public BigDecimal getCodigoSubRamo() {
		return codigoSubRamo;
	}

	public void setIdToReservaEstimada(BigDecimal idToReservaEstimada) {
		this.idToReservaEstimada = idToReservaEstimada;
	}

	public BigDecimal getIdToReservaEstimada() {
		return idToReservaEstimada;
	}

	public void setCveConceptoMov(String cveConceptoMov) {
		this.cveConceptoMov = cveConceptoMov;
	}

	public String getCveConceptoMov() {
		return cveConceptoMov;
	}

	public void setConceptoMov(String conceptoMov) {
		this.conceptoMov = conceptoMov;
	}

	public String getConceptoMov() {
		return conceptoMov;
	}

	public void setEstimacion(Double estimacion) {
		this.estimacion = estimacion;
	}

	public Double getEstimacion() {
		return estimacion;
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();									
		
		sb.append("");
		sb.append("SoporteContabilidadSiniestroDTO {"  + '\n');
		
		if(idToReporteSiniestro != null){
			sb.append("idToReporteSiniestro = " + idToReporteSiniestro.toString() + '\n');
		}else{
			sb.append("idToReporteSiniestro = null " + '\n');
		}
		if(idTcMovimientoSiniestro != null){
			sb.append("idTcMovimientoSiniestro = " + idTcMovimientoSiniestro.toString() + '\n');
		}else{
			sb.append("idTcMovimientoSiniestro = null"  + '\n');
		}									
		if(idRegistro != null){
			sb.append("idRegistro = " + idRegistro.toString() + '\n' );
		}else{
			sb.append("idRegistro = null " + '\n' );
		}		
		if(idTcSubRamo != null){
			sb.append("idTcSubRamo = " + idTcSubRamo.toString() + '\n');
		}else{
			sb.append("idTcSubRamo = null " + '\n');
		}		
		if(idTcRamo != null){
			sb.append("idTcRamo = " + idTcRamo + '\n' );
		}else{
			sb.append("idTcRamo = null " + '\n' );
		}				
		if(codigoRamo != null){
			sb.append("codigoRamo = " + codigoRamo.toString() + '\n');
		}else{
			sb.append("codigoRamo = " + codigoRamo + '\n');
		}		
		if(codigoSubRamo != null){
			sb.append("codigoSubRamo = " + codigoSubRamo + '\n');
		}else{
			sb.append("codigoSubRamo = null " + '\n');
		}		
		if(idToReservaEstimada != null){
			sb.append("idToReservaEstimada = " + idToReservaEstimada.toString() + '\n');
		}else{
			sb.append("idToReservaEstimada = null " + '\n');
		}											
		if(cveConceptoMov != null){
			sb.append("cveConceptoMov = " + cveConceptoMov.toString() + '\n');
		}else{
			sb.append("cveConceptoMov = null "+ '\n');
		}				
		if(conceptoMov != null){
			sb.append("conceptoMov = " + conceptoMov.toString() + '\n');
		}else{
			sb.append("conceptoMov = null "+ '\n');
		}							
		if(estimacion != null){
			sb.append("estimacion = " + estimacion.toString() + '\n');
		}else{
			sb.append("estimacion = null "+ '\n');
		}								
		
		sb.append("}" + '\n');
		
		return sb.toString();		
	}
}
