package mx.com.afirme.midas.siniestro.finanzas.integracion;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;


public interface IntegracionReaseguroFacadeRemote {
	public List<IntegracionReaseguroDTO> getReservaDetalleInicial(BigDecimal idToReservaEstimada);
	public List<IntegracionReaseguroDTO> getReservaDetalle(BigDecimal idToReservaEstimada);
	public Double getTotalSumaAseguradaPorReporte(BigDecimal idToReporteSiniestro); 
	public List<IntegracionReaseguroDTO> getDistribucionReaseguro(BigDecimal idToReporteSiniestro);
	public List<IntegracionReaseguroDTO> getDistribucionDeIndemnizacion(BigDecimal idToIndemnizacion);
	public List<IntegracionReaseguroDTO> getDistribucionDeDeducibleIndemnizacion(BigDecimal idToIndemnizacion);
	public List<IntegracionReaseguroDTO> getDistribucionDeCoaseguroIndemnizacion(BigDecimal idToIndemnizacion);
}
