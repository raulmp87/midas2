package mx.com.afirme.midas.siniestro.finanzas.indemnizacion;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;

/**
 * Remote interface for IndemnizacionDTOFacade.
 * @author MyEclipse Persistence Tools
 */


public interface IndemnizacionFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved IndemnizacionDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity IndemnizacionDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public IndemnizacionDTO save(IndemnizacionDTO entity);
    /**
	 Delete a persistent IndemnizacionDTO entity.
	  @param entity IndemnizacionDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(IndemnizacionDTO entity);
   /**
	 Persist a previously saved IndemnizacionDTO entity and return it or a copy of it to the sender. 
	 A copy of the IndemnizacionDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity IndemnizacionDTO entity to update
	 @return IndemnizacionDTO the persisted IndemnizacionDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public IndemnizacionDTO update(IndemnizacionDTO entity);
	public IndemnizacionDTO findById( BigDecimal id);
	 /**
	 * Find all IndemnizacionDTO entities with a specific property value.  
	 
	  @param propertyName the name of the IndemnizacionDTO property to query
	  @param value the property value to match
	  	  @return List<IndemnizacionDTO> found by query
	 */
	public List<IndemnizacionDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all IndemnizacionDTO entities.
	  	  @return List<IndemnizacionDTO> all IndemnizacionDTO entities
	 */
	public List<IndemnizacionDTO> findAll(
		);
	
	public IndemnizacionDTO obtenIndemnizacionPorAutorizar(BigDecimal idReporteSiniestro);
	
	public List<IndemnizacionDTO> buscaPagosParciales(BigDecimal idReporteSiniestro);
	
	public boolean tienePagosParciales(BigDecimal idReporteSiniestro);
	
	/**
	 * Find findLastPaymentByReporteSiniestro entity.
	  	  @return IndemnizacionDTO entity
	 */
	public IndemnizacionDTO findLastPaymentByReporteSiniestro(ReporteSiniestroDTO reporteSiniestroDTO);
	
	public List<IndemnizacionDTO> obtenIndemnizacionPorEstatusNOCancelado(ReporteSiniestroDTO reporteSiniestroDTO);
	
	public IndemnizacionDTO obtenerUltimoPagoUnicoPago(BigDecimal idReporteSiniestro,Short statusUnicoPago,Short statusUltimoPago);
	
	public List<IndemnizacionDTO> obtenerVariosPagos();
	
	public List<IndemnizacionDTO> getIndemnizacionPorReporteYEstatus(BigDecimal idReporteSiniestro,Object... params);
	public IndemnizacionDTO obtenerUltimoPago(BigDecimal idToReporteSiniestro);
	public IndemnizacionDTO obtenerUltimoPagoGenerado(BigDecimal idToReporteSiniestro);
	
	public Double getMontoReclamado(BigDecimal idToPoliza);
}