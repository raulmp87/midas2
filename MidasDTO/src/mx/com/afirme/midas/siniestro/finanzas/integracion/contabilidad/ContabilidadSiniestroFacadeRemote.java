package mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad;
// default package


import java.util.List;

import javax.ejb.Remote;

/**
 * Remote interface for ContabilidadSiniestroFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ContabilidadSiniestroFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ContabilidadSiniestro entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ContabilidadSiniestro entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ContabilidadSiniestroDTO entity);
    /**
	 Delete a persistent ContabilidadSiniestro entity.
	  @param entity ContabilidadSiniestro entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ContabilidadSiniestroDTO entity);
   /**
	 Persist a previously saved ContabilidadSiniestro entity and return it or a copy of it to the sender. 
	 A copy of the ContabilidadSiniestro entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ContabilidadSiniestro entity to update
	 @return ContabilidadSiniestro the persisted ContabilidadSiniestro entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ContabilidadSiniestroDTO update(ContabilidadSiniestroDTO entity);
	public ContabilidadSiniestroDTO findById( ContabilidadSiniestroId id);
	 /**
	 * Find all ContabilidadSiniestro entities with a specific property value.  
	 
	  @param propertyName the name of the ContabilidadSiniestro property to query
	  @param value the property value to match
	  	  @return List<ContabilidadSiniestro> found by query
	 */
	public List<ContabilidadSiniestroDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ContabilidadSiniestro entities.
	  	  @return List<ContabilidadSiniestro> all ContabilidadSiniestro entities
	 */
	public List<ContabilidadSiniestroDTO> findAll();	
	
	/**
	 * Devuelve una lista con los ids de los movimientos que aun no han sido registrados
	 * en la contabilidad de Seycos
	 * @return Una lista de ids de movimientos no registrados en la contabilidad de Seycos
	 */
	public List<ContabilidadSiniestroId> buscarMovimientosNoContabilizados();
	
	/**
	 * Actualiza los movimientos de siniestro como contabilizados
	 * @param contabilidadSiniestroId Id del movimiento de siniestro cuyos registros se desean marcar como contabilizados
	 * @param estatusContabilizado Estatus de movimiento contabilizado
	 */
	public void actualizarMovimientosContabilizados(ContabilidadSiniestroId contabilidadSiniestroId, int estatusContabilizado);
}
