package mx.com.afirme.midas.siniestro.finanzas.integracion;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * MapeoConceptoSiniestroId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class MapeoConceptoSiniestroId  implements java.io.Serializable {

    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal idConcepto;
    private BigDecimal valorMapeo;
    private BigDecimal concepto;

    // Constructors

    /** default constructor */
    public MapeoConceptoSiniestroId() {
    }

    
    /** full constructor */
    public MapeoConceptoSiniestroId(BigDecimal idConcepto, BigDecimal valorMapeo, BigDecimal concepto) {
        this.idConcepto = idConcepto;
        this.valorMapeo = valorMapeo;
        this.concepto = concepto;
    }

   
    // Property accessors
    @Column(name="IDCONCEPTO", nullable=false, precision=22, scale=0)
    public BigDecimal getIdConcepto() {
        return this.idConcepto;
    }
    
    public void setIdConcepto(BigDecimal idConcepto) {
        this.idConcepto = idConcepto;
    }

    @Column(name="VALORMAPEO", nullable=false, precision=22, scale=0)
    public BigDecimal getValorMapeo() {
        return this.valorMapeo;
    }
    
    public void setValorMapeo(BigDecimal valormapeo) {
        this.valorMapeo = valormapeo;
    }

    @Column(name="CONCEPTO", nullable=false, length=240)
    public BigDecimal getConcepto() {
        return this.concepto;
    }
    
    public void setConcepto(BigDecimal concepto) {
        this.concepto = concepto;
    }

   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof MapeoConceptoSiniestroId) ) return false;
		 MapeoConceptoSiniestroId castOther = ( MapeoConceptoSiniestroId ) other; 
         
		 return ((this.getIdConcepto()==castOther.getIdConcepto()) || ( this.getIdConcepto()!=null && castOther.getIdConcepto()!=null && this.getIdConcepto().equals(castOther.getIdConcepto()) ) )
		 	&& ( (this.getValorMapeo()==castOther.getValorMapeo()) || ( this.getValorMapeo()!=null && castOther.getValorMapeo()!=null && this.getValorMapeo().equals(castOther.getValorMapeo()) ) )
		 	&& ( (this.getConcepto()==castOther.getConcepto()) || ( this.getConcepto()!=null && castOther.getConcepto()!=null && this.getConcepto().equals(castOther.getConcepto()) ) );
   }
   
   public int hashCode() {
         int result = 17;         
         result = 37 * result + ( getIdConcepto() == null ? 0 : this.getIdConcepto().hashCode() );
         result = 37 * result + ( getValorMapeo() == null ? 0 : this.getValorMapeo().hashCode() );
         result = 37 * result + ( getConcepto() == null ? 0 : this.getConcepto().hashCode() );
         return result;
   }   

}