package mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad;

import java.math.BigDecimal;

public class SoporteContabilidadOrdenPagoDTO {
	private BigDecimal idRamo;
	private BigDecimal idSubRamo;
	private BigDecimal codigoRamo;
	private BigDecimal codigoSubRamo;
	private BigDecimal idPoliza;
	private double montoSumaAsegurada;
	private double montoIndemnizacion;
	private double montoDeducible;
	private double montoCoaseguro;
		
	public SoporteContabilidadOrdenPagoDTO(){		
		this.montoSumaAsegurada = 0.00;
		this.montoIndemnizacion = 0.00;
		this.montoDeducible = 0.00;
		this.montoCoaseguro = 0.00;		
	}

	public void setIdRamo(BigDecimal idRamo) {
		this.idRamo = idRamo;
	}

	public BigDecimal getIdRamo() {
		return idRamo;
	}

	public void setIdSubRamo(BigDecimal idSubRamo) {
		this.idSubRamo = idSubRamo;
	}

	public BigDecimal getIdSubRamo() {
		return idSubRamo;
	}

	public void setCodigoRamo(BigDecimal codigoRamo) {
		this.codigoRamo = codigoRamo;
	}

	public BigDecimal getCodigoRamo() {
		return codigoRamo;
	}

	public void setCodigoSubRamo(BigDecimal codigoSubRamo) {
		this.codigoSubRamo = codigoSubRamo;
	}

	public BigDecimal getCodigoSubRamo() {
		return codigoSubRamo;
	}
	
	public BigDecimal getIdPoliza(){
		return this.idPoliza;
	}

	public void setIdPoliza(BigDecimal idPoliza){
		this.idPoliza = idPoliza;
	}
	
	public void setMontoSumaAsegurada(double montoSumaAsegurada) {
		this.montoSumaAsegurada = montoSumaAsegurada;
	}

	public double getMontoSumaAsegurada() {
		return montoSumaAsegurada;
	}
	
	public void setMontoIndemnizacion(double montoIndemnizacion) {
		this.montoIndemnizacion = montoIndemnizacion;
	}

	public double getMontoIndemnizacion() {
		return montoIndemnizacion;
	}

	public void setMontoDeducible(double montoDeducible) {
		this.montoDeducible = montoDeducible;
	}

	public double getMontoDeducible() {
		return montoDeducible;
	}

	public void setMontoCoaseguro(double montoCoaseguro) {
		this.montoCoaseguro = montoCoaseguro;
	}

	public double getMontoCoaseguro() {
		return montoCoaseguro;
	}	
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		
		sb.append("");
		sb.append("SoporteContabilidadOrdenPagoDTO{" + '\n');				
		sb.append("idRamo = " + idRamo + '\n');
		sb.append("idSubRamo = " + idSubRamo+ '\n');
		sb.append("codigoRamo = " + codigoRamo+ '\n');
		sb.append("codigoSubRamo = " + codigoSubRamo + '\n');
		sb.append("idPoliza = " + idPoliza + '\n');
		sb.append("montoSumaAsegurada = " + montoSumaAsegurada + '\n');					
		sb.append("montoIndemnizacion = " + montoIndemnizacion + '\n');
		sb.append("montoDeducible = " + montoDeducible + '\n');
		sb.append("montoCoaseguro = " + montoCoaseguro + '\n');
		sb.append("}" + '\n');
		
		return sb.toString();		
	}
}
