package mx.com.afirme.midas.siniestro.finanzas.gasto;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for GastoSiniestroFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface GastoSiniestroFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved GastoSiniestro entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            GastoSiniestro entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(GastoSiniestroDTO entity);

	/**
	 * Delete a persistent GastoSiniestro entity.
	 * 
	 * @param entity
	 *            GastoSiniestro entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(GastoSiniestroDTO entity);

	/**
	 * Persist a previously saved GastoSiniestro entity and return it or a copy
	 * of it to the sender. A copy of the GastoSiniestro entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            GastoSiniestro entity to update
	 * @return GastoSiniestro the persisted GastoSiniestro entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public GastoSiniestroDTO update(GastoSiniestroDTO entity);

	public GastoSiniestroDTO findById(BigDecimal id);

	/**
	 * Find all GastoSiniestro entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the GastoSiniestro property to query
	 * @param value
	 *            the property value to match
	 * @return List<GastoSiniestro> found by query
	 */
	public List<GastoSiniestroDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all GastoSiniestro entities.
	 * 
	 * @return List<GastoSiniestro> all GastoSiniestro entities
	 */
	public List<GastoSiniestroDTO> findAll();

	/**
	 * Find all GastoSiniestro entities which are part of a Reporte Siniestro.
	 * 
	 * @return List<GastoSiniestro> all GastoSiniestro entities
	 */	
	public List<GastoSiniestroDTO> findByIdReporteSiniestro(BigDecimal idReporteSiniestro);
	
	public List<GastoSiniestroDTO> listarGastosPendientes();
	
	public List<GastoSiniestroDTO> getGastosPorReporteYEstatus(BigDecimal idReporteSiniestro,Object... params);
	
	public List<GastoSiniestroDTO> getGastosNoCancelados(BigDecimal idReporteSiniestro);
}