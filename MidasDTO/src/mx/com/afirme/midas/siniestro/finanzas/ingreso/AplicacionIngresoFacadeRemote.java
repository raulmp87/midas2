package mx.com.afirme.midas.siniestro.finanzas.ingreso;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;


/**
 * Remote interface for ToaplicacioningresoFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface AplicacionIngresoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved Toaplicacioningreso
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            Toaplicacioningreso entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public AplicacionIngresoDTO save(AplicacionIngresoDTO entity);

	/**
	 * Delete a persistent Toaplicacioningreso entity.
	 * 
	 * @param entity
	 *            Toaplicacioningreso entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(AplicacionIngresoDTO entity);

	/**
	 * Persist a previously saved Toaplicacioningreso entity and return it or a
	 * copy of it to the sender. A copy of the Toaplicacioningreso entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            Toaplicacioningreso entity to update
	 * @return Toaplicacioningreso the persisted Toaplicacioningreso entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public AplicacionIngresoDTO update(AplicacionIngresoDTO entity);

	public AplicacionIngresoDTO findById(BigDecimal id);

	/**
	 * Find all Toaplicacioningreso entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the Toaplicacioningreso property to query
	 * @param value
	 *            the property value to match
	 * @return List<Toaplicacioningreso> found by query
	 */
	public List<AplicacionIngresoDTO> findByProperty(String propertyName, Object value);

	public List<AplicacionIngresoDTO> findByObservacionaplicacion(Object observacionaplicacion);

	public List<AplicacionIngresoDTO> findByReferenciasaplicacion(Object referenciasaplicacion);

	/**
	 * Find all Toaplicacioningreso entities.
	 * 
	 * @return List<Toaplicacioningreso> all Toaplicacioningreso entities
	 */
	public List<AplicacionIngresoDTO> findAll();
	
	public AplicacionIngresoDTO buscarAplicarIngresoPorAT(BigDecimal idToAutorizacionTecnica,Short estatus);
	
	public List<AplicacionIngresoDTO> buscarAplicarIngresoPorEstatus(Short estatus);
	
	public List<AplicacionIngresoDTO> buscarAplicarIngresoPendientes();
}