package mx.com.afirme.midas.siniestro.finanzas.gasto;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for ConceptoGastoFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ConceptoGastoFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved ConceptoGasto entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            ConceptoGasto entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ConceptoGastoDTO entity);

	/**
	 * Delete a persistent ConceptoGasto entity.
	 * 
	 * @param entity
	 *            ConceptoGasto entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ConceptoGastoDTO entity);

	/**
	 * Persist a previously saved ConceptoGasto entity and return it or a copy
	 * of it to the sender. A copy of the ConceptoGasto entity parameter is
	 * returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            ConceptoGasto entity to update
	 * @return ConceptoGasto the persisted ConceptoGasto entity instance, may
	 *         not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ConceptoGastoDTO update(ConceptoGastoDTO entity);

	public ConceptoGastoDTO findById(BigDecimal id);

	/**
	 * Find all ConceptoGasto entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the ConceptoGasto property to query
	 * @param value
	 *            the property value to match
	 * @return List<ConceptoGasto> found by query
	 */
	public List<ConceptoGastoDTO> findByProperty(String propertyName, Object value);

	/**
	 * Find all ConceptoGasto entities.
	 * 
	 * @return List<ConceptoGasto> all ConceptoGasto entities
	 */
	public List<ConceptoGastoDTO> findAll();
}