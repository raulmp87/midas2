package mx.com.afirme.midas.siniestro.finanzas.pagos;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation.OperationType;
import mx.com.afirme.midas2.util.DateUtils;

import org.springframework.stereotype.Component;
@Component
public class FacturaDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String numeroFactura;
	private Date fechaFactura;
	private BigDecimal importeSubTotal;
	private BigDecimal importeIva;
	private BigDecimal importeIvaRet;
	private BigDecimal importeIsr;
	private BigDecimal importeTotal;
	private String estatus;
	private String estatusDesc;
	private BigDecimal importeRestante;
	private String uuidCfdi;
	private Integer idProveedor;

	
	public FacturaDTO(){
		super();
	}
	
	
	
	public FacturaDTO(String numeroFactura,Date fechaFactura, BigDecimal importeSubTotal,
			BigDecimal importeIva,BigDecimal importeIvaRet,BigDecimal importeIsr,
			BigDecimal importeTotal,String estatus,BigDecimal importeRestante,
			String uuidCfdi,Integer idProveedor) {
		this();
		this.numeroFactura = numeroFactura;
		this.fechaFactura = fechaFactura;
		this.importeSubTotal = importeSubTotal;
		this.importeIva = importeIva;
		this.importeIvaRet = importeIvaRet;
		this.importeIsr = importeIsr;
		this.importeTotal = importeTotal;
		this.estatus = estatus;
		this.importeRestante = importeRestante;
		this.uuidCfdi = uuidCfdi;
		this.idProveedor = idProveedor;
	}

	@FilterPersistenceAnnotation(persistenceName="NUMERO_FACTURA")
	public String getNumeroFactura() {
		return numeroFactura;
	}

	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	@FilterPersistenceAnnotation(persistenceName="FECHA_FACTURA", truncateDate=true )
	public Date getFechaFactura() {
		return fechaFactura;
	}

	public void setFechaFactura(Date fechaFactura) {
		this.fechaFactura = fechaFactura;
	}

	@FilterPersistenceAnnotation(persistenceName="SUBTOTAL")
	public BigDecimal getImporteSubTotal() {
		return importeSubTotal;
	}

	public void setImporteSubTotal(BigDecimal importeSubTotal) {
		this.importeSubTotal = importeSubTotal;
	}

	@FilterPersistenceAnnotation(persistenceName="IVA")
	public BigDecimal getImporteIva() {
		return importeIva;
	}

	public void setImporteIva(BigDecimal importeIva) {
		this.importeIva = importeIva;
	}

	@FilterPersistenceAnnotation(persistenceName="IVA_RETENIDO")
	public BigDecimal getImporteIvaRet() {
		return importeIvaRet;
	}

	public void setImporteIvaRet(BigDecimal importeIvaRet) {
		this.importeIvaRet = importeIvaRet;
	}

	@FilterPersistenceAnnotation(persistenceName="ISR")
	public BigDecimal getImporteIsr() {
		return importeIsr;
	}

	public void setImporteIsr(BigDecimal importeIsr) {
		this.importeIsr = importeIsr;
	}

	@FilterPersistenceAnnotation(persistenceName="MONTO_TOTAL")
	public BigDecimal getImporteTotal() {
		return importeTotal;
	}

	public void setImporteTotal(BigDecimal importeTotal) {
		this.importeTotal = importeTotal;
	}

	@FilterPersistenceAnnotation(persistenceName="ESTATUS")
	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	@FilterPersistenceAnnotation(excluded=true)
	public String getEstatusDesc() {
		if(estatus.equals("P")){
			estatusDesc = "PAGADA";
		}else if(estatus.equals("PR")){
			estatusDesc = "PROCESADA";
		}else if(estatus.equals("D")){
			estatusDesc = "DEVUELTA";
		}else if(estatus.equals("CANC")){
			estatusDesc = "CANCELADA";
		}else if(estatus.equals("E")){
			estatusDesc = "ERROR";
		}else if(estatus.equals("ASOC")){
			estatusDesc = "ASOCIADA";
		}else{
			estatusDesc = "INDEFINIDA";
		}			
		return estatusDesc;
	}

	@FilterPersistenceAnnotation(persistenceName="MONTO_RESTANTE")
	public BigDecimal getImporteRestante() {
		return importeRestante;
	}

	public void setImporteRestante(BigDecimal importeRestante) {
		this.importeRestante = importeRestante;
	}

	@FilterPersistenceAnnotation(persistenceName="UUID_CFDI")
	public String getUuidCfdi() {
		return uuidCfdi;
	}

	public void setUuidCfdi(String uuidCfdi) {
		this.uuidCfdi = uuidCfdi;
	}

	@FilterPersistenceAnnotation(persistenceName="ID_PROVEEDOR")
	public Integer getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(Integer idProveedor) {
		this.idProveedor = idProveedor;
	}

	public String getFechaFacturaString() {
		DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		return this.getFechaFactura() != null ? f.format(this.getFechaFactura()) :null;
	}

	public void setFechaFacturaString(String stringDate) throws ParseException {
		DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		if(stringDate != null && !stringDate.equals("")){
			Date date = new Date();
			date = f.parse(stringDate);
			this.setFechaFactura(date);
		}
	}		

	public void finalize() throws Throwable {

	}
}
