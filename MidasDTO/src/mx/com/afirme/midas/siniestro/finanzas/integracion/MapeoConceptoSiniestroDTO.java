package mx.com.afirme.midas.siniestro.finanzas.integracion;
// default package


import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * MapeoConceptoSiniestro entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOMAPEOCONCEPTOSINIESTRO" ,schema="MIDAS")
public class MapeoConceptoSiniestroDTO  implements java.io.Serializable {

    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MapeoConceptoSiniestroId id;

    // Constructors

    /** default constructor */
    public MapeoConceptoSiniestroDTO() {
    }
    
    /** full constructor */
    public MapeoConceptoSiniestroDTO(MapeoConceptoSiniestroId id) {
        this.id = id;
    }
   
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idConcepto", column=@Column(name="IDCONCEPTO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="valorMapeo", column=@Column(name="VALORMAPEO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="concepto", column=@Column(name="CONCEPTO", nullable=false, length=1) ) } )

    public MapeoConceptoSiniestroId getId() {
        return this.id;
    }
    
    public void setId(MapeoConceptoSiniestroId id) {
        this.id = id;
    }   
}