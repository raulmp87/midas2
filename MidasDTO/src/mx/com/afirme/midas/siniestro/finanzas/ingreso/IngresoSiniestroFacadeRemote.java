package mx.com.afirme.midas.siniestro.finanzas.ingreso;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for IngresoSiniestroFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface IngresoSiniestroFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved IngresoSiniestro entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            IngresoSiniestro entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public IngresoSiniestroDTO save(IngresoSiniestroDTO entity);

	/**
	 * Delete a persistent IngresoSiniestro entity.
	 * 
	 * @param entity
	 *            IngresoSiniestro entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(IngresoSiniestroDTO entity);

	/**
	 * Persist a previously saved IngresoSiniestro entity and return it or a
	 * copy of it to the sender. A copy of the IngresoSiniestro entity parameter
	 * is returned when the JPA persistence mechanism has not previously been
	 * tracking the updated entity.
	 * 
	 * @param entity
	 *            IngresoSiniestro entity to update
	 * @return IngresoSiniestro the persisted IngresoSiniestro entity instance,
	 *         may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public IngresoSiniestroDTO update(IngresoSiniestroDTO entity);

	public IngresoSiniestroDTO findById(BigDecimal id);

	/**
	 * Find all IngresoSiniestro entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the IngresoSiniestro property to query
	 * @param value
	 *            the property value to match
	 * @return List<IngresoSiniestro> found by query
	 */
	public List<IngresoSiniestroDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all IngresoSiniestro entities.
	 * 
	 * @return List<IngresoSiniestro> all IngresoSiniestro entities
	 */
	public List<IngresoSiniestroDTO> findAll();
	/**
	 * Find all IngresoSiniestro entities which are part of a Reporte Siniestro.
	 * 
	 * @return List<IngresoSiniestro> all IngresoSiniestro entities
	 */	
	public List<IngresoSiniestroDTO> findByIdReporteSiniestro(BigDecimal idReporteSiniestro);
	
	public List<IngresoSiniestroDTO> listarIngresosPendientes();
	
	public List<IngresoSiniestroDTO> getIngresosPorReporteYEstatus(BigDecimal idReporteSiniestro,Object... params);
	
	public List<IngresoSiniestroDTO> listarIngresosPorAplicar(IngresoSiniestroFiltroDTO criteriosDeFiltrado);
	
	public List<IngresoSiniestroDTO> getIngresosNoCancelados(BigDecimal idReporteSiniestro);
}