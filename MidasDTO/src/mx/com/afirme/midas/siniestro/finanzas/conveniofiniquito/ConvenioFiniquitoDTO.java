package mx.com.afirme.midas.siniestro.finanzas.conveniofiniquito;
// default package

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDTO;

/**
 * ConvenioFiniquito entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOCONVENIOFINIQUITO", schema = "MIDAS")
public class ConvenioFiniquitoDTO implements java.io.Serializable {
	public static final int TIPO_ESTADO_CUENTA = 0;
	public static final int TIPO_CLABE = 1;
	public static final int ESTATUS_CREADO = 0;
	public static final int ESTATUS_RECIBIDO = 1;
	public static final int ESTATUS_CANCELADO = 2;
	
	
	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -8007403136841228298L;
	private IndemnizacionDTO indemnizacion;
	private ReporteSiniestroDTO reporteSiniestro;
	private BigDecimal idToConvenioFiniquito;	
	private String descripcionDanios;
	private Double montoIndemnizacion;
	private String numeroCuenta;
	private int tipoCuenta;
	private Date fechaConvenio;
	private String bancoReceptor;
	private String nombreBeneficiario;
	private String lugarConvenio;
	private int estatus;
	private BigDecimal pagoconcheque;

	// Constructors

	
	/** default constructor */
	public ConvenioFiniquitoDTO() {
	}
	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTOCONVENIOFINIQUITO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOCONVENIOFINIQUITO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOCONVENIOFINIQUITO_SEQ_GENERADOR")
	@Column(name = "IDTOCONVENIOFINIQUITO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToConvenioFiniquito() {
		return this.idToConvenioFiniquito;
	}

	public void setIdToConvenioFiniquito(BigDecimal idToConvenioFiniquito) {
		this.idToConvenioFiniquito = idToConvenioFiniquito;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOINDEMNIZACION", nullable = false)
	public IndemnizacionDTO getIndemnizacion() {
		return this.indemnizacion;
	}

	public void setIndemnizacion(IndemnizacionDTO indemnizacion) {
		this.indemnizacion = indemnizacion;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOREPORTESINIESTRO", nullable = false, insertable = false)
	public ReporteSiniestroDTO getReporteSiniestro() {
		return reporteSiniestro;
	}
	public void setReporteSiniestro(ReporteSiniestroDTO reporteSiniestro) {
		this.reporteSiniestro = reporteSiniestro;
	}

	@Column(name = "DESCRIPCIONDANIOS", nullable = false)
	public String getDescripcionDanios() {
		return this.descripcionDanios;
	}

	public void setDescripcionDanios(String descripcionDanios) {
		this.descripcionDanios = descripcionDanios;
	}

	@Column(name = "MONTOINDEMINZACION", nullable = false, precision = 16)
	public Double getMontoIndemnizacion() {
		return this.montoIndemnizacion;
	}

	public void setMontoIndemnizacion(Double montoIndemnizacion) {
		this.montoIndemnizacion = montoIndemnizacion;
	}

	@Column(name = "NUMEROCUENTA", length = 30)
	public String getNumeroCuenta() {
		return this.numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	@Column(name = "TIPOCUENTA", precision = 1, scale = 0)
	public int getTipoCuenta() {
		return this.tipoCuenta;
	}

	public void setTipoCuenta(int tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHACONVENIO", nullable = false, length = 7)
	public Date getFechaConvenio() {
		return this.fechaConvenio;
	}

	public void setFechaConvenio(Date fechaConvenio) {
		this.fechaConvenio = fechaConvenio;
	}

	@Column(name = "BANCORECEPTOR", length = 100)
	public String getBancoReceptor() {
		return this.bancoReceptor;
	}

	public void setBancoReceptor(String bancoReceptor) {
		this.bancoReceptor = bancoReceptor;
	}

	@Column(name = "NOMBREBENEFICIARIO", length = 100)
	public String getNombreBeneficiario() {
		return this.nombreBeneficiario;
	}

	public void setNombreBeneficiario(String nombreBeneficiario) {
		this.nombreBeneficiario = nombreBeneficiario;
	}

	@Column(name = "LUGARCONVENIO", length = 100)
	public String getLugarConvenio() {
		return this.lugarConvenio;
	}

	public void setLugarConvenio(String lugarConvenio) {
		this.lugarConvenio = lugarConvenio;
	}

	@Column(name = "ESTATUS", nullable = false, precision = 1, scale = 0)
	public int getEstatus() {
		return estatus;
	}
	
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
	
	@Column(name = "PAGOCONCHEQUE", nullable = false, precision = 22, scale = 0)
	public BigDecimal getPagoconcheque() {
		return this.pagoconcheque;
	}

	public void setPagoconcheque(BigDecimal pagoconcheque) {
		this.pagoconcheque = pagoconcheque;
	}
}