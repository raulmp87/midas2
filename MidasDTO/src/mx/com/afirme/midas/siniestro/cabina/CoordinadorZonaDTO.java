package mx.com.afirme.midas.siniestro.cabina;
// default package




import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * CoordinadorZonaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name="CoordinadorZonaDTO")
@Table(name="TOCOORDINADORZONA"
    ,schema="MIDAS"
)

public class CoordinadorZonaDTO  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CoordinadorZonaId id;
     private Integer idCoordinador;


    // Constructors

    /** default constructor */
    public CoordinadorZonaDTO() {
    }

   
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idEstado", column=@Column(name="IDESTADO", nullable=false, length=240) ), 
        @AttributeOverride(name="idContry", column=@Column(name="IDCONTRY", nullable=false, length=240) ) } )

    public CoordinadorZonaId getId() {
        return this.id;
    }
    
    public void setId(CoordinadorZonaId id) {
        this.id = id;
    }
    
    @Column(name="IDCOORDINADOR", nullable=false, precision=22, scale=0)

    public Integer getIdCoordinador() {
        return this.idCoordinador;
    }
    
    public void setIdCoordinador(Integer idCoordinador) {
        this.idCoordinador = idCoordinador;
    }
   








}