package mx.com.afirme.midas.siniestro.cabina;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;


public class PolizaDummyDTO extends CotizacionDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private BigDecimal numeroPoliza;
	private String claveOficina;
	private String formaPago;
	private String ultimoReciboPagado;
	private Date fechaPago;
	private BigDecimal importePagado;
	private BigDecimal saldoPendiente;
	private BigDecimal saldoVencido;
	private Date fechaEmision;
	private BigDecimal tipoNegocio;
	private String descripcionProducto;
	private String descripcionSubRamo;
	private String nombreAsegurado;
	/**
	 * @return the numeroPoliza
	 */
	public BigDecimal getNumeroPoliza() {
		return numeroPoliza;
	}
	/**
	 * @param numeroPoliza the numeroPoliza to set
	 */
	public void setNumeroPoliza(BigDecimal numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	/**
	 * @return the claveOficina
	 */
	public String getClaveOficina() {
		return claveOficina;
	}
	/**
	 * @param claveOficina the claveOficina to set
	 */
	public void setClaveOficina(String claveOficina) {
		this.claveOficina = claveOficina;
	}
	/**
	 * @return the formaPago
	 */
	public String getFormaPago() {
		return formaPago;
	}
	/**
	 * @param formaPago the formaPago to set
	 */
	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	/**
	 * @return the ultimoReciboPagado
	 */
	public String getUltimoReciboPagado() {
		return ultimoReciboPagado;
	}
	/**
	 * @param ultimoReciboPagado the ultimoReciboPagado to set
	 */
	public void setUltimoReciboPagado(String ultimoReciboPagado) {
		this.ultimoReciboPagado = ultimoReciboPagado;
	}
	/**
	 * @return the fechaPago
	 */
	public Date getFechaPago() {
		return fechaPago;
	}
	/**
	 * @param fechaPago the fechaPago to set
	 */
	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}
	/**
	 * @return the importePagado
	 */
	public BigDecimal getImportePagado() {
		return importePagado;
	}
	/**
	 * @param importePagado the importePagado to set
	 */
	public void setImportePagado(BigDecimal importePagado) {
		this.importePagado = importePagado;
	}
	/**
	 * @return the saldoPendiente
	 */
	public BigDecimal getSaldoPendiente() {
		return saldoPendiente;
	}
	/**
	 * @param saldoPendiente the saldoPendiente to set
	 */
	public void setSaldoPendiente(BigDecimal saldoPendiente) {
		this.saldoPendiente = saldoPendiente;
	}
	/**
	 * @return the saldoVencido
	 */
	public BigDecimal getSaldoVencido() {
		return saldoVencido;
	}
	/**
	 * @param saldoVencido the saldoVencido to set
	 */
	public void setSaldoVencido(BigDecimal saldoVencido) {
		this.saldoVencido = saldoVencido;
	}
	/**
	 * @return the fechaEmision
	 */
	public Date getFechaEmision() {
		return fechaEmision;
	}
	/**
	 * @param fechaEmision the fechaEmision to set
	 */
	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	/**
	 * @return the tipoNegocio
	 */
	public BigDecimal getTipoNegocio() {
		return tipoNegocio;
	}
	/**
	 * @param tipoNegocio the tipoNegocio to set
	 */
	public void setTipoNegocio(BigDecimal tipoNegocio) {
		this.tipoNegocio = tipoNegocio;
	}
	/**
	 * @return the descripcionProducto
	 */
	public String getDescripcionProducto() {
		return descripcionProducto;
	}
	/**
	 * @param descripcionProducto the descripcionProducto to set
	 */
	public void setDescripcionProducto(String descripcionProducto) {
		this.descripcionProducto = descripcionProducto;
	}
	/**
	 * @return the descripcionSubRamo
	 */
	public String getDescripcionSubRamo() {
		return descripcionSubRamo;
	}
	/**
	 * @param descripcionSubRamo the descripcionSubRamo to set
	 */
	public void setDescripcionSubRamo(String descripcionSubRamo) {
		this.descripcionSubRamo = descripcionSubRamo;
	}
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}

	
	
	

}
