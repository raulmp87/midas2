package mx.com.afirme.midas.siniestro.cabina;
// default package

import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * CoordinadorZonaId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class CoordinadorZonaId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idEstado;
     private String idContry;


    // Constructors

    /** default constructor */
    public CoordinadorZonaId() {
    }

    
    /** full constructor */
    public CoordinadorZonaId(String idEstado, String idContry) {
        this.idEstado = idEstado;
        this.idContry = idContry;
    }

   
    // Property accessors

    @Column(name="IDESTADO", nullable=false, length=240)

    public String getIdEstado() {
        return this.idEstado;
    }
    
    public void setIdEstado(String idEstado) {
        this.idEstado = idEstado;
    }

    @Column(name="IDCONTRY", nullable=false, length=240)

    public String getIdContry() {
        return this.idContry;
    }
    
    public void setIdContry(String idContry) {
        this.idContry = idContry;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof CoordinadorZonaId) ) return false;
		 CoordinadorZonaId castOther = ( CoordinadorZonaId ) other; 
         
		 return ( (this.getIdEstado()==castOther.getIdEstado()) || ( this.getIdEstado()!=null && castOther.getIdEstado()!=null && this.getIdEstado().equals(castOther.getIdEstado()) ) )
 && ( (this.getIdContry()==castOther.getIdContry()) || ( this.getIdContry()!=null && castOther.getIdContry()!=null && this.getIdContry().equals(castOther.getIdContry()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdEstado() == null ? 0 : this.getIdEstado().hashCode() );
         result = 37 * result + ( getIdContry() == null ? 0 : this.getIdContry().hashCode() );
         return result;
   }   





}