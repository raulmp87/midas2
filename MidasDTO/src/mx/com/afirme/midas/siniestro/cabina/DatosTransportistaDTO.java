package mx.com.afirme.midas.siniestro.cabina;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;




/**
 * DatosTransportistaDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name="DatosTransportistaDTO")
@Table(name="TODATOSTRANSPORTISTA"
    ,schema="MIDAS"
)
public class DatosTransportistaDTO  implements java.io.Serializable {


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields    

     private BigDecimal idToReporteSiniestro;
     private ReporteSiniestroDTO reporteSiniestroDTO;
     private String razonSocial;
     private String tipoPersona;
     private String calle;
     private String colonia;
     private BigDecimal codigoPostal;
     private String idMunicipio;
     private String idEstado;
     private String telefono;
     private String rfc;
     private String numeroUnidad;


    // Constructors

    /** default constructor */
    public DatosTransportistaDTO() {
    }
   
    // Property accessors
    @Id 
    
    @Column(name="IDTOREPORTESINIESTRO", unique=true, nullable=false, precision=22, scale=0)

    public BigDecimal getIdToReporteSiniestro() {
        return this.idToReporteSiniestro;
    }
    
    public void setIdToReporteSiniestro(BigDecimal idToReporteSiniestro) {
        this.idToReporteSiniestro = idToReporteSiniestro;
    }
	
    /*@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="IDTOREPORTESINIESTRO", unique=true, nullable=false, insertable=false, updatable=false)

    public ReporteSiniestroDTO getReporteSiniestroDTO() {
        return this.reporteSiniestroDTO;
    }*/
    
    @OneToOne
    	@JoinColumn(name="IDTOREPORTESINIESTRO", nullable=false, insertable=false, updatable=false)
    public ReporteSiniestroDTO getReporteSiniestroDTO() {
        return this.reporteSiniestroDTO;
    }    
    
    public void setReporteSiniestroDTO(ReporteSiniestroDTO reporteSiniestroDTO) {
        this.reporteSiniestroDTO = reporteSiniestroDTO;
    }
    
    @Column(name="RAZONSOCIAL", length=240)

    public String getRazonSocial() {
        return this.razonSocial;
    }
    
    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }
    
    @Column(name="TIPOPERSONA", length=1)

    public String getTipoPersona() {
        return this.tipoPersona;
    }
    
    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }
    
    @Column(name="CALLE", length=240)

    public String getCalle() {
        return this.calle;
    }
    
    public void setCalle(String calle) {
        this.calle = calle;
    }
    
    @Column(name="COLONIA", length=240)

    public String getColonia() {
        return this.colonia;
    }
    
    public void setColonia(String colonia) {
        this.colonia = colonia;
    }
    
    @Column(name="CODIGOPOSTAL", precision=22, scale=0)

    public BigDecimal getCodigoPostal() {
        return this.codigoPostal;
    }
    
    public void setCodigoPostal(BigDecimal codigoPostal) {
        this.codigoPostal = codigoPostal;
    }
    
    @Column(name="IDMUNICIPIO", length=240)

    public String getIdMunicipio() {
        return this.idMunicipio;
    }
    
    public void setIdMunicipio(String idMunicipio) {
        this.idMunicipio = idMunicipio;
    }
    
    @Column(name="IDESTADO", length=240)

    public String getIdEstado() {
        return this.idEstado;
    }
    
    public void setIdEstado(String idEstado) {
        this.idEstado = idEstado;
    }
    
    @Column(name="TELEFONO", length=50)

    public String getTelefono() {
        return this.telefono;
    }
    
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    
    @Column(name="RFC", length=30)

    public String getRfc() {
        return this.rfc;
    }
    
    public void setRfc(String rfc) {
        this.rfc = rfc;
    }
    
    @Column(name="NUMEROUNIDAD", length=240)

    public String getNumeroUnidad() {
        return this.numeroUnidad;
    }
    
    public void setNumeroUnidad(String numeroUnidad) {
        this.numeroUnidad = numeroUnidad;
    }
   








}