package mx.com.afirme.midas.siniestro.cabina;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * EstatusSiniestroDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCESTATUSSINIESTRO", schema = "MIDAS")
public class EstatusSiniestroDTO implements java.io.Serializable {
	public static final int ESTATUS_PENDIENTE = 1;
	public static final int ESTATUS_LEGAL = 2;
	public static final int ESTATUS_CERRADO = 3;
	
	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1008562312642630284L;
	private BigDecimal idTcEstatusSiniestro;
	private String descripcion;
	private List<ReporteSiniestroDTO> reportesSiniestro = new ArrayList<ReporteSiniestroDTO>(0);

	// Constructors

	/** default constructor */
	public EstatusSiniestroDTO() {
	}

	// Property accessors
	@Id
	@Column(name = "IDTCESTATUSSINIESTRO", nullable = false, precision = 1, scale = 0)
	public BigDecimal getIdTcEstatusSiniestro() {
		return this.idTcEstatusSiniestro;
	}

	public void setIdTcEstatusSiniestro(BigDecimal idTcEstatusSiniestro) {
		this.idTcEstatusSiniestro = idTcEstatusSiniestro;
	}

	@Column(name = "DESCRIPCION", length = 50)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "estatusSiniestro")
	public List<ReporteSiniestroDTO> getReportesSiniestro() {
		return this.reportesSiniestro;
	}

	public void setReportesSiniestro(
			List<ReporteSiniestroDTO> reportesSiniestro) {
		this.reportesSiniestro = reportesSiniestro;
	}

}