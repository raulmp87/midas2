package mx.com.afirme.midas.siniestro.cabina;


import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;


/**
 * Remote interface for DatosTransportistaFacade.
 * @author MyEclipse Persistence Tools
 */


public interface DatosTransportistaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved DatosTransportistaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity DatosTransportistaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(DatosTransportistaDTO entity);
    /**
	 Delete a persistent DatosTransportistaDTO entity.
	  @param entity DatosTransportistaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(DatosTransportistaDTO entity);
   /**
	 Persist a previously saved DatosTransportistaDTO entity and return it or a copy of it to the sender. 
	 A copy of the DatosTransportistaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity DatosTransportistaDTO entity to update
	 @return DatosTransportistaDTO the persisted DatosTransportistaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public DatosTransportistaDTO update(DatosTransportistaDTO entity);
	public DatosTransportistaDTO findById( BigDecimal id);
	 /**
	 * Find all DatosTransportistaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the DatosTransportistaDTO property to query
	  @param value the property value to match
	  	  @return List<DatosTransportistaDTO> found by query
	 */
	public List<DatosTransportistaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all DatosTransportistaDTO entities.
	  	  @return List<DatosTransportistaDTO> all DatosTransportistaDTO entities
	 */
	public List<DatosTransportistaDTO> findAll(
		);	
}