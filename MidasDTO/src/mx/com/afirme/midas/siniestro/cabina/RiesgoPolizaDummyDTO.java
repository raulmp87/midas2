package mx.com.afirme.midas.siniestro.cabina;

import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDTO;

public class RiesgoPolizaDummyDTO extends RiesgoCotizacionDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String subInciso = "Por Definir";
	
	private boolean riesgoAfectado;

	public String getSubInciso() {
		return subInciso;
	}

	public void setSubInciso(String subInciso) {
		this.subInciso = subInciso;
	}

	public boolean isRiesgoAfectado() {
		return riesgoAfectado;
	}

	public void setRiesgoAfectado(boolean riesgoAfectado) {
		this.riesgoAfectado = riesgoAfectado;
	}



}
