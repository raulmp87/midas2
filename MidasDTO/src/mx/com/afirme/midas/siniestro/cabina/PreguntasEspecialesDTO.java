package mx.com.afirme.midas.siniestro.cabina;
// default package

import java.math.BigDecimal;
import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;




/**
 * PreguntasEspecialesDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name="PreguntasEspecialesDTO")
@Table(name="TOPREGUNTASESPECIALES"
    ,schema="MIDAS"
)
public class PreguntasEspecialesDTO  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToReporteSiniestro;
     private ReporteSiniestroDTO reporteSiniestroDTO;
     private String numeroCertificado;
     private String telefonoFuncionario;
     private String email;
     private String dependencia;
     private String cargo;
     private String tipoCobertura;
     private Date fechaReclamacion;
     private String tipoDocumento;
     private String tipoAvisoDetalle;
     private String numeroExpediente;
     private String numeroOficio;
     private String lugarOficio;
     private String nombreDependencia;
     private String personaRealizoOficio;
     private String descripcionPuesto;
     private String causaReclamacion;
     private String plazoReclamacion;
     private String siniestroCubierto;
     private String tienePlazoReclamacion;
     private String comentariosReclamante;
     private String serviciosProfesionales;
     private String nombrefuncionario;
     private String calle;
     private BigDecimal codigopostal;
     private String idcolonia;
     private String idciudad;
     private String idestado;

    // Constructors

    /** default constructor */
    public PreguntasEspecialesDTO() {
    }

	/** minimal constructor */
   
    // Property accessors
    @Id 
    @Column(name="IDTOREPORTESINIESTRO", unique=true, precision=22, scale=0)
    public BigDecimal getIdToReporteSiniestro() {
        return this.idToReporteSiniestro;
    }
    
    public void setIdToReporteSiniestro(BigDecimal idToReporteSiniestro) {
        this.idToReporteSiniestro = idToReporteSiniestro;
    }
    
	/*@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="IDTOREPORTESINIESTRO", unique=true, nullable=false, insertable=false, updatable=false)

    public ReporteSiniestroDTO getReporteSiniestroDTO() {
        return this.reporteSiniestroDTO;
    }*/
    
    @OneToOne
       @JoinColumn(name="IDTOREPORTESINIESTRO",updatable=false, nullable=false, insertable=false)
    public ReporteSiniestroDTO getReporteSiniestroDTO() {
        return this.reporteSiniestroDTO;
    }    
    
    public void setReporteSiniestroDTO(ReporteSiniestroDTO reporteSiniestroDTO) {
        this.reporteSiniestroDTO = reporteSiniestroDTO;
    }
    
    @Column(name="NUMEROCERTIFICADO", length=50)

    public String getNumeroCertificado() {
        return this.numeroCertificado;
    }
    
    public void setNumeroCertificado(String numeroCertificado) {
        this.numeroCertificado = numeroCertificado;
    }
    
    @Column(name="TELEFONOFUNCIONARIO", length=50)

    public String getTelefonoFuncionario() {
        return this.telefonoFuncionario;
    }
    
    public void setTelefonoFuncionario(String telefonoFuncionario) {
        this.telefonoFuncionario = telefonoFuncionario;
    }
    
    @Column(name="EMAIL", length=50)

    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    @Column(name="DEPENDENCIA", length=240)

    public String getDependencia() {
        return this.dependencia;
    }
    
    public void setDependencia(String dependencia) {
        this.dependencia = dependencia;
    }
    
    @Column(name="CARGO", length=240)

    public String getCargo() {
        return this.cargo;
    }
    
    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
    
    @Column(name="TIPOCOBERTURA", length=1)

    public String getTipoCobertura() {
        return this.tipoCobertura;
    }
    
    public void setTipoCobertura(String tipoCobertura) {
        this.tipoCobertura = tipoCobertura;
    }
@Temporal(TemporalType.DATE)
    @Column(name="FECHARECLAMACION", length=7)

    public Date getFechaReclamacion() {
        return this.fechaReclamacion;
    }
    
    public void setFechaReclamacion(Date fechaReclamacion) {
        this.fechaReclamacion = fechaReclamacion;
    }
    
    @Column(name="TIPODOCUMENTO", length=1)

    public String getTipoDocumento() {
        return this.tipoDocumento;
    }
    
    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }
    
    @Column(name="TIPOAVISODETALLE", length=240)

    public String getTipoAvisoDetalle() {
        return this.tipoAvisoDetalle;
    }
    
    public void setTipoAvisoDetalle(String tipoAvisoDetalle) {
        this.tipoAvisoDetalle = tipoAvisoDetalle;
    }
    
    @Column(name="NUMEROEXPEDIENTE", length=240)

    public String getNumeroExpediente() {
        return this.numeroExpediente;
    }
    
    public void setNumeroExpediente(String numeroExpediente) {
        this.numeroExpediente = numeroExpediente;
    }
    
    @Column(name="NUMEROOFICIO", length=240)

    public String getNumeroOficio() {
        return this.numeroOficio;
    }
    
    public void setNumeroOficio(String numeroOficio) {
        this.numeroOficio = numeroOficio;
    }
    
    @Column(name="LUGAROFICIO", length=240)

    public String getLugarOficio() {
        return this.lugarOficio;
    }
    
    public void setLugarOficio(String lugarOficio) {
        this.lugarOficio = lugarOficio;
    }
    
    @Column(name="NOMBREDEPENDENCIA", length=240)

    public String getNombreDependencia() {
        return this.nombreDependencia;
    }
    
    public void setNombreDependencia(String nombreDependencia) {
        this.nombreDependencia = nombreDependencia;
    }
    
    @Column(name="PERSONAREALIZOOFICIO", length=240)

    public String getPersonaRealizoOficio() {
        return this.personaRealizoOficio;
    }
    
    public void setPersonaRealizoOficio(String personaRealizoOficio) {
        this.personaRealizoOficio = personaRealizoOficio;
    }
    
    @Column(name="DESCRIPCIONPUESTO", length=240)

    public String getDescripcionPuesto() {
        return this.descripcionPuesto;
    }
    
    public void setDescripcionPuesto(String descripcionPuesto) {
        this.descripcionPuesto = descripcionPuesto;
    }
    
    @Column(name="CAUSASRECLAMACION", length=240)

    public String getCausaReclamacion() {
        return this.causaReclamacion;
    }
    
    public void setCausaReclamacion(String causaReclamacion) {
        this.causaReclamacion = causaReclamacion;
    }
    
    @Column(name="TIENEPLAZORECLAMACION", precision=1, scale=0)

    public String getTienePlazoReclamacion() {
        return this.tienePlazoReclamacion;
    }
    
    public void setTienePlazoReclamacion(String tienePlazoReclamacion) {
        this.tienePlazoReclamacion = tienePlazoReclamacion;
    }
    
    @Column(name="PLAZORECLAMACION", length=240)

    public String getPlazoReclamacion() {
        return this.plazoReclamacion;
    }
    
    public void setPlazoReclamacion(String plazoReclamacion) {
        this.plazoReclamacion = plazoReclamacion;
    }
    
    @Column(name="SINIESTROCUBIERTO", precision=1, scale=0)

    public String getSiniestroCubierto() {
        return this.siniestroCubierto;
    }
    
    public void setSiniestroCubierto(String siniestroCubierto) {
        this.siniestroCubierto = siniestroCubierto;
    }
    
    @Column(name="COMENTARIOSRECLAMANTE", length=240)

    public String getComentariosReclamante() {
        return this.comentariosReclamante;
    }
    
    public void setComentariosReclamante(String comentariosReclamante) {
        this.comentariosReclamante = comentariosReclamante;
    }
    
    @Column(name="SERVICIOSPROFESIONALES", length=1)

    public String getServiciosProfesionales() {
        return this.serviciosProfesionales;
    }
    
    public void setServiciosProfesionales(String serviciosProfesionales) {
        this.serviciosProfesionales = serviciosProfesionales;
    }
   
    @Column(name = "NOMBREFUNCIONARIO", length = 150)
	public String getNombrefuncionario() {
		return this.nombrefuncionario;
	}

	public void setNombrefuncionario(String nombrefuncionario) {
		this.nombrefuncionario = nombrefuncionario;
	}

	@Column(name = "CALLE", length = 50)
	public String getCalle() {
		return this.calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	@Column(name = "CODIGOPOSTAL", precision = 22, scale = 0)
	public BigDecimal getCodigopostal() {
		return this.codigopostal;
	}

	public void setCodigopostal(BigDecimal codigopostal) {
		this.codigopostal = codigopostal;
	}

	@Column(name = "IDCOLONIA", length = 50)
	public String getIdcolonia() {
		return this.idcolonia;
	}

	public void setIdcolonia(String idcolonia) {
		this.idcolonia = idcolonia;
	}

	@Column(name = "IDCIUDAD", length = 10)
	public String getIdciudad() {
		return this.idciudad;
	}

	public void setIdciudad(String idciudad) {
		this.idciudad = idciudad;
	}

	@Column(name = "IDESTADO", length = 10)
	public String getIdestado() {
		return this.idestado;
	}

	public void setIdestado(String idestado) {
		this.idestado = idestado;
	}

}