package mx.com.afirme.midas.siniestro.cabina;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;


/**
 * Remote interface for PreguntasEspecialesFacade.
 * @author MyEclipse Persistence Tools
 */


public interface PreguntasEspecialesFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved PreguntasEspecialesDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity PreguntasEspecialesDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(PreguntasEspecialesDTO entity);
    /**
	 Delete a persistent PreguntasEspecialesDTO entity.
	  @param entity PreguntasEspecialesDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(PreguntasEspecialesDTO entity);
   /**
	 Persist a previously saved PreguntasEspecialesDTO entity and return it or a copy of it to the sender. 
	 A copy of the PreguntasEspecialesDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity PreguntasEspecialesDTO entity to update
	 @return PreguntasEspecialesDTO the persisted PreguntasEspecialesDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public PreguntasEspecialesDTO update(PreguntasEspecialesDTO entity);
	public PreguntasEspecialesDTO findById( BigDecimal id);
	 /**
	 * Find all PreguntasEspecialesDTO entities with a specific property value.  
	 
	  @param propertyName the name of the PreguntasEspecialesDTO property to query
	  @param value the property value to match
	  	  @return List<PreguntasEspecialesDTO> found by query
	 */
	public List<PreguntasEspecialesDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all PreguntasEspecialesDTO entities.
	  	  @return List<PreguntasEspecialesDTO> all PreguntasEspecialesDTO entities
	 */
	public List<PreguntasEspecialesDTO> findAll(
		);	
}