package mx.com.afirme.midas.siniestro.cabina;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.base.LogBaseDTO;
import mx.com.afirme.midas.catalogos.ajustador.AjustadorDTO;
import mx.com.afirme.midas.catalogos.eventocatastrofico.EventoCatastroficoDTO;
import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;





/**
 * ReporteSiniestroDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name="ReporteSiniestroDTO")
@Table(name="TOREPORTESINIESTRO"
    ,schema="MIDAS"
)
public class ReporteSiniestroDTO extends LogBaseDTO {
	private static final long serialVersionUID = 1L;
	// Fields    

	 private String numeroPolizaCliente;
	 private String accion;
	 
     private BigDecimal idToReporteSiniestro;
     private ReporteEstatusDTO reporteEstatus;
     private AjustadorDTO ajustador;
     private EventoCatastroficoDTO eventoCatastrofico;
     private String numeroReporte;
     private Date fechaHoraReporte;
     private String nombrePersonaReporta;
     private BigDecimal telefonoPersonaReporta;
     private BigDecimal idCabinero;
     private BigDecimal numeroPoliza;
     private String calle;
     private String colonia;
     private String idCiudad;
     private String idEstado;
     private Date fechaSiniestro;
     private String descripcionSiniestro;
     //private String descripcionEventoCatastrofico;
     private String nombreContacto;
     private BigDecimal telefonoContacto;
     private String observaciones;
     private BigDecimal idCoordinador;
     private String idColonia;
     private Date fechaCerradoSiniestro;
     private Date fechaContactoAjustador;
     private Date fechaDocumento;
     private Date fechaInformeFinal;
     private Date fechaInspeccionAjustador;
     private Date fechaAsignacionAjustador;
     private Date fechaLlegadaAjustador;
     private Date fechaPreliminar;
     private Date fechaTerminadoSiniestro;
     private BigDecimal codigoPostal;
     private String comoOcurrio;
     private Date fechaModificacion;
     private BigDecimal idTcUsuarioModifica;
     private Boolean informePreliminarValido;
     private String comentarios;
     private BigDecimal analisisInterno;
     private Boolean informeFinalValido;
     private String descripcionTerminoAjuste;
     private BigDecimal numeroEndoso;
     private BigDecimal idAgente;
     private String nombreAgente;
     private String nombreOficina;
     private String nombreAsegurado;
     private BigDecimal idCliente;
	 private Date fechaEnvioCorreo;
     
     private Set<ReporteSiniestroLogDTO> reporteSiniestroLogs = new HashSet<ReporteSiniestroLogDTO>(0);


     private PreguntasEspecialesDTO preguntasEspeciales;
     private DatosTransportistaDTO datosTransportista;
     
     private PolizaSoporteDanosDTO poliza;
     private SubEstatusSiniestroDTO subEstatusSiniestro;
     private EstatusSiniestroDTO estatusSiniestro;
     private TerminoAjusteDTO terminoAjuste;
     
     private Byte notificacion;
     private PolizaDTO polizaDTO;
         
	public static final Byte PENDIENTE_ASIGNAR_AJUSTADOR = 1;
     public static final Byte PENDIENTE_CONFIRMAR_AJUSTADOR = 2;
     public static final Byte PENDIENTE_VALIDAR_INFORME_PRELIMINAR = 3;
     public static final Byte PENDIENTE_ESTIMAR_RESERVA = 4;
     public static final Byte PENDIENTE_TERMINAR_REPORTE_NO_INDEMNIZADO = 5;
     public static final Byte PENDIENTE_AUTORIZAR_RESERVA = 6;
     public static final Byte PENDIENTE_EVALUACION_INFORME_FINAL = 7;
     public static final Byte PENDIENTE_ASOCIAR_CARTA_RECHAZO = 9;
     public static final Byte PENDIENTE_GENERAR_CONVENIO_FINIQUITO = 10;               
     public static final Byte PENDIENTE_APROBAR_CARTA_RECHAZO = 11;
     public static final Byte PENDIENTE_RECEPCION_CONVENIO_FIRMADO = 12;
     public static final Byte PENDIENTE_IMPRIMIR_CARTA_RECHAZO = 13;
     public static final Byte PENDIENTE_MODIFICAR_CARTA_RECHAZO = 14;
     public static final Byte PENDIENTE_AUTORIZAR_AUTORIZACION_TECNICA = 15;
     public static final Byte PENDIENTE_EMITIR_CHEQUE = 16;     
     public static final Byte PENDIENTE_GENERAR_AUTORIZACION_TECNICA = 17;
     public static final Byte PENDIENTE_AGREGAR_PAGO_PARCIAL_INDEMNIZACION = 18;
     public static final Byte PENDIENTE_AUTORIZAR_INDEMNIZACION = 19;
     public static final Byte PENDIENTE_MODIFICAR_RESERVA = 20;
     public static final Byte PENDIENTE_GENERAR_ORDEN_DE_PAGO = 21;
     public static final Byte PENDIENTE_PROCESO_INDEMNIZACION = 22;
     public static final Byte REPORTE_TERMINADO = 24;
     public static final Byte PENDIENTE_CERRADO = 25;     
     public static final Byte PENDIENTE_AGREGAR_INDEMNIZACION = 8;
     
     public static final Byte NOTIFICACION_NO_ENVIADA = 0;
     public static final Byte NOTIFICACION_ENVIADA = 1;
     
    // Constructors

    /** default constructor */
    public ReporteSiniestroDTO() {
    }

   
    // Property accessors
    @Id 
    @SequenceGenerator(name = "IDTOREPORTESINIESTRO_SEQ_GENERADOR",allocationSize = 1, sequenceName = "MIDAS.IDTOREPORTESINIESTRO_SEQ")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "IDTOREPORTESINIESTRO_SEQ_GENERADOR")
    @Column(name="IDTOREPORTESINIESTRO", unique=true, nullable=false, precision=22, scale=0)

    public BigDecimal getIdToReporteSiniestro() {
        return this.idToReporteSiniestro;
    }
    
    public void setIdToReporteSiniestro(BigDecimal idToReporteSiniestro) {
        this.idToReporteSiniestro = idToReporteSiniestro;
    }
	@ManyToOne(cascade=CascadeType.REFRESH, fetch=FetchType.EAGER)
        @JoinColumn(name="IDTCREPORTEESTATUS")

    public ReporteEstatusDTO getReporteEstatus() {
        return this.reporteEstatus;
    }
    
    public void setReporteEstatus(ReporteEstatusDTO reporteEstatus) {
        this.reporteEstatus = reporteEstatus;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="IDAJUSTADOR")

    public AjustadorDTO getAjustador() {
        return this.ajustador;
    }
    
    public void setAjustador(AjustadorDTO ajustador) {
        this.ajustador = ajustador;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTCEVENTOCATASTROFICO")

	public EventoCatastroficoDTO getEventoCatastrofico() {
	    return this.eventoCatastrofico;
	}
	
	public void setEventoCatastrofico(EventoCatastroficoDTO eventoCatastroficoDTO) {
	    this.eventoCatastrofico = eventoCatastroficoDTO;
	}    
    
    @Column(name="NUMEROREPORTE", nullable=false, length=50)

    public String getNumeroReporte() {
        return this.numeroReporte;
    }
    
    public void setNumeroReporte(String numeroReporte) {
        this.numeroReporte = numeroReporte;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHAHORAREPORTE", nullable=false, length=7)

    public Date getFechaHoraReporte() {
        return this.fechaHoraReporte;
    }
    
    public void setFechaHoraReporte(Date fechaHoraReporte) {
        this.fechaHoraReporte = fechaHoraReporte;
    }
    
    @Column(name="NOMBREPERSONAREPORTA", nullable=false, length=50)

    public String getNombrePersonaReporta() {
        return this.nombrePersonaReporta;
    }
    
    public void setNombrePersonaReporta(String nombrePersonaReporta) {
        this.nombrePersonaReporta = nombrePersonaReporta;
    }
    
    @Column(name="TELEFONOPERSONAREPORTA", nullable=false, precision=20, scale=0)

    public BigDecimal getTelefonoPersonaReporta() {
        return this.telefonoPersonaReporta;
    }
    
    public void setTelefonoPersonaReporta(BigDecimal telefonoPersonaReporta) {
        this.telefonoPersonaReporta = telefonoPersonaReporta;
    }
    
    @Column(name="IDCABINERO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdCabinero() {
        return this.idCabinero;
    }
    
    public void setIdCabinero(BigDecimal idCabinero) {
        this.idCabinero = idCabinero;
    }
    
    @Column(name="NUMEROPOLIZA", precision=22, scale=0)

    public BigDecimal getNumeroPoliza() {
        return this.numeroPoliza;
    }
    
    public void setNumeroPoliza(BigDecimal numeroPoliza) {
        this.numeroPoliza = numeroPoliza;
    }
    
    @Column(name="CALLE", nullable=false, length=50)

    public String getCalle() {
        return this.calle;
    }
    
    public void setCalle(String calle) {
        this.calle = calle;
    }
    
    @Column(name="COLONIA", length=50)

    public String getColonia() {
        return this.colonia;
    }
    
    public void setColonia(String colonia) {
        this.colonia = colonia;
    }
    
    @Column(name="IDCIUDAD", length=10)

    public String getIdCiudad() {
        return this.idCiudad;
    }
    
    public void setIdCiudad(String idCiudad) {
        this.idCiudad = idCiudad;
    }
    
    @Column(name="IDESTADO", length=10)

    public String getIdEstado() {
        return this.idEstado;
    }
    
    public void setIdEstado(String idEstado) {
        this.idEstado = idEstado;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHASINIESTRO", nullable=false, length=7)

    public Date getFechaSiniestro() {
        return this.fechaSiniestro;
    }
    
    public void setFechaSiniestro(Date fechaSiniestro) {
        this.fechaSiniestro = fechaSiniestro;
    }
    
    @Column(name="DESCRIPCIONSINIESTRO", nullable=false, length=200)

    public String getDescripcionSiniestro() {
        return this.descripcionSiniestro;
    }
    
    public void setDescripcionSiniestro(String descripcionSiniestro) {
        this.descripcionSiniestro = descripcionSiniestro;
    }
    
    @Column(name="NOMBRECONTACTO", nullable=false, length=50)

    public String getNombreContacto() {
        return this.nombreContacto;
    }
    
    public void setNombreContacto(String nombreContacto) {
        this.nombreContacto = nombreContacto;
    }
    
    @Column(name="TELEFONOCONTACTO", nullable=false, precision=20, scale=0)

    public BigDecimal getTelefonoContacto() {
        return this.telefonoContacto;
    }
    
    public void setTelefonoContacto(BigDecimal telefonoContacto) {
        this.telefonoContacto = telefonoContacto;
    }
    
    @Column(name="OBSERVACIONES", length=200)

    public String getObservaciones() {
        return this.observaciones;
    }
    
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
    
    @Column(name="IDCOORDINADOR", nullable=false, precision=22, scale=0)

    public BigDecimal getIdCoordinador() {
        return this.idCoordinador;
    }
    
    public void setIdCoordinador(BigDecimal idCoordinador) {
        this.idCoordinador = idCoordinador;
    }
    
    @Column(name="IDCOLONIA", length=50)

    public String getIdColonia() {
        return this.idColonia;
    }
    
    public void setIdColonia(String idColonia) {
        this.idColonia = idColonia;
    }
@Temporal(TemporalType.DATE)
    @Column(name="FECHACERRADOSINIESTRO", length=7)

    public Date getFechaCerradoSiniestro() {
        return this.fechaCerradoSiniestro;
    }
    
    public void setFechaCerradoSiniestro(Date fechaCerradoSiniestro) {
        this.fechaCerradoSiniestro = fechaCerradoSiniestro;
    }
@Temporal(TemporalType.DATE)
    @Column(name="FECHACONTACTOAJUSTADOR", length=7)

    public Date getFechaContactoAjustador() {
        return this.fechaContactoAjustador;
    }
    
    public void setFechaContactoAjustador(Date fechaContactoAjustador) {
        this.fechaContactoAjustador = fechaContactoAjustador;
    }
@Temporal(TemporalType.DATE)
    @Column(name="FECHADOCUMENTO", length=7)

    public Date getFechaDocumento() {
        return this.fechaDocumento;
    }
    
    public void setFechaDocumento(Date fechaDocumento) {
        this.fechaDocumento = fechaDocumento;
    }
@Temporal(TemporalType.DATE)
    @Column(name="FECHAINFORMEFINAL", length=7)

    public Date getFechaInformeFinal() {
        return this.fechaInformeFinal;
    }
    
    public void setFechaInformeFinal(Date fechaInformeFinal) {
        this.fechaInformeFinal = fechaInformeFinal;
    }
@Temporal(TemporalType.DATE)
    @Column(name="FECHAINSPECCIONAJUSTADOR", length=7)

    public Date getFechaInspeccionAjustador() {
        return this.fechaInspeccionAjustador;
    }
    
    public void setFechaInspeccionAjustador(Date fechaInspeccionAjustador) {
        this.fechaInspeccionAjustador = fechaInspeccionAjustador;
    }
@Temporal(TemporalType.DATE)
    @Column(name="FECHAASIGNACIONAJUSTADOR", length=7)

    public Date getFechaAsignacionAjustador() {
        return this.fechaAsignacionAjustador;
    }
    
    public void setFechaAsignacionAjustador(Date fechaAsignacionAjustador) {
        this.fechaAsignacionAjustador = fechaAsignacionAjustador;
    }
@Temporal(TemporalType.DATE)
    @Column(name="FECHALLEGADAAJUSTADOR", length=7)

    public Date getFechaLlegadaAjustador() {
        return this.fechaLlegadaAjustador;
    }
    
    public void setFechaLlegadaAjustador(Date fechaLlegadaAjustador) {
        this.fechaLlegadaAjustador = fechaLlegadaAjustador;
    }
@Temporal(TemporalType.DATE)
    @Column(name="FECHAPRELIMINAR", length=7)

    public Date getFechaPreliminar() {
        return this.fechaPreliminar;
    }
    
    public void setFechaPreliminar(Date fechaPreliminar) {
        this.fechaPreliminar = fechaPreliminar;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHATERMINADOSINIESTRO", length=7)

    public Date getFechaTerminadoSiniestro() {
        return this.fechaTerminadoSiniestro;
    }
    
    public void setFechaTerminadoSiniestro(Date fechaTerminadoSiniestro) {
        this.fechaTerminadoSiniestro = fechaTerminadoSiniestro;
    }
    
    @Column(name="CODIGOPOSTAL", nullable=false, precision=22, scale=0)

    public BigDecimal getCodigoPostal() {
        return this.codigoPostal;
    }
    
    public void setCodigoPostal(BigDecimal codigoPostal) {
        this.codigoPostal = codigoPostal;
    }
    
    @Column(name="COMOOCURRIO", length=240)

    public String getComoOcurrio() {
        return this.comoOcurrio;
    }
    
    public void setComoOcurrio(String comoOcurrio) {
        this.comoOcurrio = comoOcurrio;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHAMODIFICACION", length=7)

    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }
    
    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
    
    @Column(name="IDTCUSUARIOMODIFICA", precision=22, scale=0)

    public BigDecimal getIdTcUsuarioModifica() {
        return this.idTcUsuarioModifica;
    }
    
    public void setIdTcUsuarioModifica(BigDecimal idTcUsuarioModifica) {
        this.idTcUsuarioModifica = idTcUsuarioModifica;
    }
    
    @Column(name="INFORMEPRELIMINARVALIDO", precision=1, scale=0)

    public Boolean getInformePreliminarValido() {
        return this.informePreliminarValido;
    }
    
    public void setInformePreliminarValido(Boolean informePreliminarValido) {
        this.informePreliminarValido = informePreliminarValido;
    }
    
    @Column(name="COMENTARIOS", length=240)

    public String getComentarios() {
        return this.comentarios;
    }
    
    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="reporteSiniestroDTO")

    public Set<ReporteSiniestroLogDTO> getReporteSiniestroLogs() {
        return this.reporteSiniestroLogs;
    }
    
    public void setReporteSiniestroLogs(Set<ReporteSiniestroLogDTO> reporteSiniestroLogs) {
        this.reporteSiniestroLogs = reporteSiniestroLogs;
    }
    

    @OneToOne(mappedBy="reporteSiniestroDTO",fetch=FetchType.EAGER)
	public PreguntasEspecialesDTO getPreguntasEspeciales() {
		return preguntasEspeciales;
	}


	public void setPreguntasEspeciales(PreguntasEspecialesDTO preguntasEspeciales) {
		this.preguntasEspeciales = preguntasEspeciales;
	}

	@OneToOne(mappedBy="reporteSiniestroDTO",fetch=FetchType.EAGER)
	public DatosTransportistaDTO getDatosTransportista() {
		return datosTransportista;
	}


	public void setDatosTransportista(DatosTransportistaDTO datosTransportista) {
		this.datosTransportista = datosTransportista;
	}
	


	@Transient
	public PolizaSoporteDanosDTO getPoliza() {
		return poliza;
	}
	
	public void setPoliza(PolizaSoporteDanosDTO poliza) {
		this.poliza = poliza;
	}

	@Transient
	public String getAccion() {
		return accion;
	}
	public void setAccion(String accion) {
		this.accion = accion;
	}


	@Column(name="ANALISISINTERNO", precision=1, scale=0)
	public BigDecimal getAnalisisInterno() {
		return analisisInterno;
	}

	public void setAnalisisInterno(BigDecimal analisisInterno) {
		this.analisisInterno = analisisInterno;
	}

	@Column(name="INFORMEFINALVALIDO", precision=1, scale=0)
	public Boolean getInformeFinalValido() {
		return informeFinalValido;
	}
	
	public void setInformeFinalValido(Boolean informeFinalValido) {
		this.informeFinalValido = informeFinalValido;
	}
	
	@Column(name="DESCRIPCIONTERMINOAJUSTE", length=100)
	public String getDescripcionTerminoAjuste() {
		return descripcionTerminoAjuste;
	}
	
	public void setDescripcionTerminoAjuste(String descripcionTerminoAjuste) {
		this.descripcionTerminoAjuste = descripcionTerminoAjuste;
	}

	 @Column(name="NUMEROENDOSO", precision=22, scale=0)
	public BigDecimal getNumeroEndoso() {
		return numeroEndoso;
	}


	public void setNumeroEndoso(BigDecimal numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}


	@Column(name="IDAGENTE", precision=22, scale=0)
	public BigDecimal getIdAgente() {
		return idAgente;
	}


	public void setIdAgente(BigDecimal idAgente) {
		this.idAgente = idAgente;
	}

	@Column(name="NOMBREAGENTE", length=150)
	public String getNombreAgente() {
		return nombreAgente;
	}


	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}

	@Column(name="NOMBREOFICINA", length=100)
	public String getNombreOficina() {
		return nombreOficina;
	}


	public void setNombreOficina(String nombreOficina) {
		this.nombreOficina = nombreOficina;
	}

	@Column(name="NOMBREASEGURADO", length=250)
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}


	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}

	@Column(name="IDCLIENTE", precision=22, scale=0)
	public BigDecimal getIdCliente() {
		return idCliente;
	}


	public void setIdCliente(BigDecimal idCliente) {
		this.idCliente = idCliente;
	}
	
	/**
	 * @return the fechaEnvioCorreo
	 */
	@Temporal(TemporalType.DATE)
    @Column(name="FECHAENVIOCORREO", length=7)
	public Date getFechaEnvioCorreo() {
		return fechaEnvioCorreo;
	}

	/**
	 * @param fechaEnvioCorreo the fechaEnvioCorreo to set
	 */
	public void setFechaEnvioCorreo(Date fechaEnvioCorreo) {
		this.fechaEnvioCorreo = fechaEnvioCorreo;
	}
	
	@ManyToOne(cascade=CascadeType.REFRESH,fetch=FetchType.EAGER)
    	@JoinColumn(name="IDTCESTATUSSINIESTRO")
	public EstatusSiniestroDTO getEstatusSiniestro() {
		return estatusSiniestro;
	}

	public void setEstatusSiniestro(EstatusSiniestroDTO estatusSiniestro) {
		this.estatusSiniestro = estatusSiniestro;
	}
	
	@ManyToOne(cascade=CascadeType.REFRESH,fetch=FetchType.EAGER)
		@JoinColumn(name="IDTCSUBESTATUSSINIESTRO")
	public SubEstatusSiniestroDTO getSubEstatusSiniestro() {
		return subEstatusSiniestro;
	}
	
	public void setSubEstatusSiniestro(SubEstatusSiniestroDTO subEstatusSiniestro) {
		this.subEstatusSiniestro = subEstatusSiniestro;
	}

	@ManyToOne(cascade=CascadeType.REFRESH,fetch=FetchType.EAGER)
		@JoinColumn(name="IDTERMINOAJUSTE")
	public TerminoAjusteDTO getTerminoAjuste() {
		return this.terminoAjuste;
	}
	
	public void setTerminoAjuste(TerminoAjusteDTO terminoAjuste) {
		this.terminoAjuste = terminoAjuste;
	}

	@Transient
	public String getNumeroPolizaCliente() {
		return numeroPolizaCliente;
	}

	public void setNumeroPolizaCliente(String numeroPolizaCliente) {
		this.numeroPolizaCliente = numeroPolizaCliente;
	}
	
	@Column(name = "NOTIFICACION", length = 1)
	public Byte getNotificacion() {
		return this.notificacion;
	}

	public void setNotificacion(Byte notificacion) {
		this.notificacion = notificacion;
	}

	@Transient
	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}


	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}













	
	
}