package mx.com.afirme.midas.siniestro.cabina;
// default package


import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;




/**
 * ReporteEstatusDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name="ReporteEstatusDTO")
@Table(name="TCREPORTEESTATUS"
    ,schema="MIDAS"
)
public class ReporteEstatusDTO  implements java.io.Serializable {


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields    

     private Byte idTcReporteEstatus;
     private String descripcion;
     private Set<ReporteSiniestroDTO> reporteSiniestroDTOs = new HashSet<ReporteSiniestroDTO>(0);
     private Set<ReporteSiniestroLogDTO> reporteSiniestroLogs = new HashSet<ReporteSiniestroLogDTO>(0);


    // Constructors

    /** default constructor */
    public ReporteEstatusDTO() {
    }

  
    // Property accessors
    @Id 
    
    @Column(name="IDTCREPORTEESTATUS", unique=true, nullable=false, precision=2, scale=0)

    public Byte getIdTcReporteEstatus() {
        return this.idTcReporteEstatus;
    }
    
    public void setIdTcReporteEstatus(Byte idTcReporteEstatus) {
        this.idTcReporteEstatus = idTcReporteEstatus;
    }
    
    @Column(name="DESCRIPCION", nullable=false, length=100)

    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="reporteEstatus")

    public Set<ReporteSiniestroDTO> getReporteSiniestroDTOs() {
        return this.reporteSiniestroDTOs;
    }
    
    public void setReporteSiniestroDTOs(Set<ReporteSiniestroDTO> reporteSiniestroDTOs) {
        this.reporteSiniestroDTOs = reporteSiniestroDTOs;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="reporteEstatusDTO")

    public Set<ReporteSiniestroLogDTO> getReporteSiniestroLogs() {
        return this.reporteSiniestroLogs;
    }
    
    public void setReporteSiniestroLogs(Set<ReporteSiniestroLogDTO> reporteSiniestroLogs) {
        this.reporteSiniestroLogs = reporteSiniestroLogs;
    }
   








}