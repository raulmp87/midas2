/**
 * 
 */
package mx.com.afirme.midas.siniestro.cabina.configcorreo;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;
import mx.com.afirme.midas.catalogos.tiponegocio.TipoNegocioDTO;
import mx.com.afirme.midas.siniestro.cabina.configcorreo.ConfigCorreoTipoNegociooDTO;

/**
 * @author smvr
 *
 */
@Remote
public interface ConfigCorreoTipoNegocioFacadeRemote {

	/**
	 * Perform an initial save of a previously unsaved ConfigCorreoTipoNegociooDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            ConfigCorreoTipoNegociooDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ConfigCorreoTipoNegociooDTO entity);

	/**
	 * Delete a persistent ConfigCorreoTipoNegociooDTO entity.
	 * 
	 * @param entity
	 *            ConfigCorreoTipoNegociooDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ConfigCorreoTipoNegociooDTO entity);

	/**
	 * Persist a previously saved ConfigCorreoTipoNegociooDTO entity and return it or a copy of it
	 * to the sender. A copy of the ConfigCorreoTipoNegociooDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            ConfigCorreoTipoNegociooDTO entity to update
	 * @return ConfigCorreoTipoNegociooDTO the persisted ConfigCorreoTipoNegociooDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ConfigCorreoTipoNegociooDTO update(ConfigCorreoTipoNegociooDTO entity);

	/**
	 * Find all ConfigCorreoTipoNegociooDTO entities with a specific property value.
	 * 
	 * @param idConfigTipoNegocio
	 *            the name of the idConfigTipoNegocio property to query
	 * @return ConfigCorreoTipoNegociooDTO found by query
	 */
	public ConfigCorreoTipoNegociooDTO findById(BigDecimal idConfigTipoNegocio);
	
	/**
	 * Find all ConfigCorreoTipoNegociooDTO entities with a specific property value.
	 * 
	 * @param idTcTipoNegocio
	 *            the name of the idConfigTipoNegocio property to query
	 * @return List<ConfigCorreoTipoNegociooDTO> found by query
	 */
	public List<ConfigCorreoTipoNegociooDTO> findByTipoNegocio(BigDecimal idTcTipoNegocio);

	/**
	 * Find all TipoNegocioDTO entities.
	 * 
	 * @return List<TipoNegocioDTO> all TipoNegocioDTO entities
	 */
	public List<TipoNegocioDTO> findAll();
	
	/**
	 * Find all TipoNegocioDTO entities by id
	 * @param tipoNegocioDTO
	 * @return List<TipoNegocioDTO> all TipoNegocioDTO entities
	 */
	public List<TipoNegocioDTO> listarFiltrado(TipoNegocioDTO tipoNegocioDTO);
}