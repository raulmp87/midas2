package mx.com.afirme.midas.siniestro.cabina;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * SalvamentoEstatus entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCTERMINOAJUSTE", schema = "MIDAS")
public class TerminoAjusteDTO implements java.io.Serializable {
	// Fields
	public static final int ESTATUS_RECHAZO = 5;
	public static final int ESTATUS_REPORTE_CANCELADO = 6;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1008562312642630284L;
	private BigDecimal idTcTerminoAjuste;
	private String descripcion;
	private List<ReporteSiniestroDTO> reportesSiniestro = new ArrayList<ReporteSiniestroDTO>(0);

	// Constructors

	/** default constructor */
	public TerminoAjusteDTO() {
	}

	// Property accessors
	@Id
	@Column(name = "IDTCTERMINOAJUSTE", nullable = false, precision = 1, scale = 0)
	public BigDecimal getIdTcTerminoAjuste() {
		return this.idTcTerminoAjuste;
	}

	public void setIdTcTerminoAjuste(BigDecimal idTcTerminoAjuste) {
		this.idTcTerminoAjuste = idTcTerminoAjuste;
	}

	@Column(name = "DESCRIPCION", length = 50)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "terminoAjuste")
	public List<ReporteSiniestroDTO> getReportesSiniestro() {
		return this.reportesSiniestro;
	}

	public void setReportesSiniestro(
			List<ReporteSiniestroDTO> reportesSiniestro) {
		this.reportesSiniestro = reportesSiniestro;
	}

}