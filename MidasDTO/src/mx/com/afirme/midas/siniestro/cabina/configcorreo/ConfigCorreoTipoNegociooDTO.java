/**
 * 
 */
package mx.com.afirme.midas.siniestro.cabina.configcorreo;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.catalogos.tiponegocio.TipoNegocioDTO;

/**
 * ConfigCorreoTipoNegociooDTO entity. @author smvr
 *
 */
@Entity
@Table(name = "TOCONFIGCORREOTIPONEGOCIO", schema = "MIDAS")
public class ConfigCorreoTipoNegociooDTO implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	private BigDecimal idTcTipoNegocio;
	private BigDecimal idConfigTipoNegocio;
	private String correo;
	private String usuarioNombre;
	private Date fechaModificacion;
//	TipoNegocioDTO tipoNegocioDTO;
	
	/** default constructor */
	public ConfigCorreoTipoNegociooDTO() {
	}

	/** full constructor */
	public ConfigCorreoTipoNegociooDTO( BigDecimal idTcTipoNegocio,
									   BigDecimal IdConfigTipoNegocio,
									   String correo,
									   String usuarioNombre,
									   Date fechaModificacion 
									  ) {
		this.idTcTipoNegocio     = idTcTipoNegocio;
		this.idConfigTipoNegocio = IdConfigTipoNegocio;
		this.correo              = correo;
		this.usuarioNombre       = usuarioNombre;
		this.fechaModificacion   = fechaModificacion;
	}
	
	// Property accessors

	/**
	 * @return the idTcTipoNegocio
	 */
	@Column(name = "IDTCTIPONEGOCIO")
	public BigDecimal getIdTcTipoNegocio() {
		return idTcTipoNegocio;
	}

	/**
	 * @param idTcTipoNegocio the idTcTipoNegocio to set
	 */
	public void setIdTcTipoNegocio(BigDecimal idTcTipoNegocio) {
		this.idTcTipoNegocio = idTcTipoNegocio;
	}

	/**
	 * @return the idConfigTipoNegocio
	 */
	@Id
	@SequenceGenerator(name = "IDCONFIGTIPONEGOCIO_SEQ_GENERADOR",allocationSize = 1, sequenceName = "MIDAS.TOCONFIGCORREOTIPONEGOCIO_SEQ")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "IDCONFIGTIPONEGOCIO_SEQ_GENERADOR")
	@Column(name = "IDCONFIGTIPONEGOCIO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdConfigTipoNegocio() {
		return this.idConfigTipoNegocio;
	}

	/**
	 * @param idConfigTipoNegocio the idConfigTipoNegocio to set
	 */
	public void setIdConfigTipoNegocio(BigDecimal idConfigTipoNegocio) {
		this.idConfigTipoNegocio = idConfigTipoNegocio;
	}

	@Column(name = "CORREO", length = 240)
	public String getCorreo() {
		return this.correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	@Column(name = "USUARIONOMBRE", length = 50)
	public String getUsuarioNombre() {
		return this.usuarioNombre;
	}

	public void setUsuarioNombre(String usuarioNombre) {
		this.usuarioNombre = usuarioNombre;
	}
	
	/**
	 * @return the fechaModificacion
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAMODIFICACION")
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	/**
	 * @param fechaModificacion the fechaModificacion to set
	 */
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

}
