package mx.com.afirme.midas.siniestro.cabina;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;


/**
 * Remote interface for ReporteSiniestroFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ReporteSiniestroFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ReporteSiniestroDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ReporteSiniestroDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public ReporteSiniestroDTO save(ReporteSiniestroDTO entity);
    /**
	 Delete a persistent ReporteSiniestroDTO entity.
	  @param entity ReporteSiniestroDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ReporteSiniestroDTO entity);
   /**
	 Persist a previously saved ReporteSiniestroDTO entity and return it or a copy of it to the sender. 
	 A copy of the ReporteSiniestroDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ReporteSiniestroDTO entity to update
	 @return ReporteSiniestroDTO the persisted ReporteSiniestroDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ReporteSiniestroDTO update(ReporteSiniestroDTO entity);
	public ReporteSiniestroDTO findById( BigDecimal id);
	 /**
	 * Find all ReporteSiniestroDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ReporteSiniestroDTO property to query
	  @param value the property value to match
	  	  @return List<ReporteSiniestroDTO> found by query
	 */
	public List<ReporteSiniestroDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ReporteSiniestroDTO entities.
	  	  @return List<ReporteSiniestroDTO> all ReporteSiniestroDTO entities
	 */
	public List<ReporteSiniestroDTO> findAll(
		);	
	
	public List<ReporteSiniestroDTO> listarFiltrado(ReporteSiniestroDTO reporteSiniestroDTO);
	
	/**
	 * A utilizarse para el caso de uso de Listar Reportes de Siniestro
	 * @param filtro
	 * @return
	 */
	public List<ReporteSiniestroDTO> listarFiltrado(ReporteSiniestroFiltroDTO filtro);
	
	public List<ReporteSiniestroDTO> listarReportesAbiertos();
	
	/**
	 * A utilizarse para el caso de uso de Listar Reportes de Siniestro
	 * @param listarFiltradoPorNumPoliza
	 * @return
	 */
	public List<ReporteSiniestroDTO> listarFiltradoPorNumPoliza(String idPolizas);
	
	public List<ReporteSiniestroDTO> reportesSinInformePreliminar();
}