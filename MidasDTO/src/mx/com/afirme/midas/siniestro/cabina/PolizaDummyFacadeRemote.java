package mx.com.afirme.midas.siniestro.cabina;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;


import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;

/**
 * Remote interface for PolizaDummyFacade.
 * @author MyEclipse Persistence Tools
 */

public interface PolizaDummyFacadeRemote extends CotizacionFacadeRemote {
	
	List<PolizaDummyDTO> listarFiltrado(PolizaDummyDTO polizaDTO);
	
	PolizaDummyDTO obtenerPoliza(BigDecimal id);
	
	List<RiesgoPolizaDummyDTO> listarRiesgosPoliza(PolizaDummyDTO poliza);

}
