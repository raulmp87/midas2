package mx.com.afirme.midas.siniestro.cabina;
// default package


import java.util.List;
import javax.ejb.Remote;


/**
 * Remote interface for ReporteSiniestroLogFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ReporteSiniestroLogFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ReporteSiniestroLogDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ReporteSiniestroLogDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ReporteSiniestroLogDTO entity);
    /**
	 Delete a persistent ReporteSiniestroLogDTO entity.
	  @param entity ReporteSiniestroLogDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ReporteSiniestroLogDTO entity);
   /**
	 Persist a previously saved ReporteSiniestroLogDTO entity and return it or a copy of it to the sender. 
	 A copy of the ReporteSiniestroLogDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ReporteSiniestroLogDTO entity to update
	 @return ReporteSiniestroLogDTO the persisted ReporteSiniestroLogDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ReporteSiniestroLogDTO update(ReporteSiniestroLogDTO entity);
	public ReporteSiniestroLogDTO findById( ReporteSiniestroLogId id);
	 /**
	 * Find all ReporteSiniestroLogDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ReporteSiniestroLogDTO property to query
	  @param value the property value to match
	  	  @return List<ReporteSiniestroLogDTO> found by query
	 */
	public List<ReporteSiniestroLogDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ReporteSiniestroLogDTO entities.
	  	  @return List<ReporteSiniestroLogDTO> all ReporteSiniestroLogDTO entities
	 */
	public List<ReporteSiniestroLogDTO> findAll(
		);	
}