package mx.com.afirme.midas.siniestro.cabina;
// default package

import java.util.List;
import javax.ejb.Remote;


/**
 * Remote interface for ReporteEstatusFacade.
 * @author MyEclipse Persistence Tools
 */


public interface ReporteEstatusFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved ReporteEstatusDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity ReporteEstatusDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(ReporteEstatusDTO entity);
    /**
	 Delete a persistent ReporteEstatusDTO entity.
	  @param entity ReporteEstatusDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(ReporteEstatusDTO entity);
   /**
	 Persist a previously saved ReporteEstatusDTO entity and return it or a copy of it to the sender. 
	 A copy of the ReporteEstatusDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity ReporteEstatusDTO entity to update
	 @return ReporteEstatusDTO the persisted ReporteEstatusDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public ReporteEstatusDTO update(ReporteEstatusDTO entity);
	public ReporteEstatusDTO findById( Byte id);
	 /**
	 * Find all ReporteEstatusDTO entities with a specific property value.  
	 
	  @param propertyName the name of the ReporteEstatusDTO property to query
	  @param value the property value to match
	  	  @return List<ReporteEstatusDTO> found by query
	 */
	public List<ReporteEstatusDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all ReporteEstatusDTO entities.
	  	  @return List<ReporteEstatusDTO> all ReporteEstatusDTO entities
	 */
	public List<ReporteEstatusDTO> findAll(
		);	
}