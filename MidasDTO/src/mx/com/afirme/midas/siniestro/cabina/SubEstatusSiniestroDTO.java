package mx.com.afirme.midas.siniestro.cabina;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * EstatusSiniestroDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCSUBESTATUSSINIESTRO", schema = "MIDAS")
public class SubEstatusSiniestroDTO implements java.io.Serializable {

	
	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1008562312642630284L;
	private BigDecimal idTcSubEstatusSiniestro;
	private String descripcion;
	private List<ReporteSiniestroDTO> reportesSiniestro = new ArrayList<ReporteSiniestroDTO>(0);

	// Constructors

	/** default constructor */
	public SubEstatusSiniestroDTO() {
	}

	// Property accessors
	@Id
	@Column(name = "IDTCSUBESTATUSSINIESTRO", nullable = false, precision = 1, scale = 0)
	public BigDecimal getIdTcSubEstatusSiniestro() {
		return this.idTcSubEstatusSiniestro;
	}

	public void setIdTcSubEstatusSiniestro(BigDecimal idTcSubEstatusSiniestro) {
		this.idTcSubEstatusSiniestro = idTcSubEstatusSiniestro;
	}

	@Column(name = "DESCRIPCION", length = 50)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "subEstatusSiniestro")
	public List<ReporteSiniestroDTO> getReportesSiniestro() {
		return this.reportesSiniestro;
	}

	public void setReportesSiniestro(
			List<ReporteSiniestroDTO> reportesSiniestro) {
		this.reportesSiniestro = reportesSiniestro;
	}

}