package mx.com.afirme.midas.siniestro.cabina;

// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * ReporteSiniestroLogIdDTO entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class ReporteSiniestroLogIdDTO implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToReporteSiniestro;
	private Short idTcReporteEstatus;

	// Constructors

	/** default constructor */
	public ReporteSiniestroLogIdDTO() {
	}

	/** full constructor */
	public ReporteSiniestroLogIdDTO(BigDecimal idToReporteSiniestro,
			Short idTcReporteEstatus) {
		this.idToReporteSiniestro = idToReporteSiniestro;
		this.idTcReporteEstatus = idTcReporteEstatus;
	}

	// Property accessors

	@Column(name = "IDTOREPORTESINIESTRO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToReporteSiniestro() {
		return this.idToReporteSiniestro;
	}

	public void setIdToReporteSiniestro(BigDecimal idToReporteSiniestro) {
		this.idToReporteSiniestro = idToReporteSiniestro;
	}

	@Column(name = "IDTCREPORTEESTATUS", nullable = false, precision = 2, scale = 0)
	public Short getIdTcReporteEstatus() {
		return this.idTcReporteEstatus;
	}

	public void setIdTcReporteEstatus(Short idTcReporteEstatus) {
		this.idTcReporteEstatus = idTcReporteEstatus;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ReporteSiniestroLogIdDTO))
			return false;
		ReporteSiniestroLogIdDTO castOther = (ReporteSiniestroLogIdDTO) other;

		return ((this.getIdToReporteSiniestro() == castOther
				.getIdToReporteSiniestro()) || (this.getIdToReporteSiniestro() != null
				&& castOther.getIdToReporteSiniestro() != null && this
				.getIdToReporteSiniestro().equals(
						castOther.getIdToReporteSiniestro())))
				&& ((this.getIdTcReporteEstatus() == castOther
						.getIdTcReporteEstatus()) || (this
						.getIdTcReporteEstatus() != null
						&& castOther.getIdTcReporteEstatus() != null && this
						.getIdTcReporteEstatus().equals(
								castOther.getIdTcReporteEstatus())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToReporteSiniestro() == null ? 0 : this
						.getIdToReporteSiniestro().hashCode());
		result = 37
				* result
				+ (getIdTcReporteEstatus() == null ? 0 : this
						.getIdTcReporteEstatus().hashCode());
		return result;
	}

}