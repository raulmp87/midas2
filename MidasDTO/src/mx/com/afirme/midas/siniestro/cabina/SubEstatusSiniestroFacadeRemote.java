package mx.com.afirme.midas.siniestro.cabina;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for SubEstatusSiniestroFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SubEstatusSiniestroFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved SubEstatusSiniestro entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SubEstatusSiniestro entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SubEstatusSiniestroDTO entity);

	/**
	 * Delete a persistent SubEstatusSiniestro entity.
	 * 
	 * @param entity
	 *            SubEstatusSiniestro entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SubEstatusSiniestroDTO entity);

	/**
	 * Persist a previously saved SubEstatusSiniestro entity and return it or a
	 * copy of it to the sender. A copy of the SubEstatusSiniestro entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            SubEstatusSiniestro entity to update
	 * @return SubEstatusSiniestro the persisted SubEstatusSiniestro entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SubEstatusSiniestroDTO update(SubEstatusSiniestroDTO entity);

	public SubEstatusSiniestroDTO findById(BigDecimal id);

	/**
	 * Find all SubEstatusSiniestro entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SubEstatusSiniestro property to query
	 * @param value
	 *            the property value to match
	 * 
	 * @return List<SubEstatusSiniestro> found by query
	 */
	public List<SubEstatusSiniestroDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all SubEstatusSiniestro entities.
	 * 
	 * @param 
	 *         
	 * @return List<SubEstatusSiniestro> all SubEstatusSiniestro entities
	 */
	public List<SubEstatusSiniestroDTO> findAll();
}