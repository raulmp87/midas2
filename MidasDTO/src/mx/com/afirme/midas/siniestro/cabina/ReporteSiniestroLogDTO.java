package mx.com.afirme.midas.siniestro.cabina;
// default package


import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;




/**
 * ReporteSiniestroLogDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name="ReporteSiniestroLogDTO")
@Table(name="TOREPORTESINIESTROLOG"
    ,schema="MIDAS"
)
public class ReporteSiniestroLogDTO  implements java.io.Serializable {


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields    

     private ReporteSiniestroLogId id;
     private ReporteEstatusDTO reporteEstatusDTO;
     private ReporteSiniestroDTO reporteSiniestroDTO;
     private Date fechaEvento;
     private BigDecimal idTcUsuario;


    // Constructors

    /** default constructor */
    public ReporteSiniestroLogDTO() {
    }

   
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idToReporteSiniestro", column=@Column(name="IDTOREPORTESINIESTRO", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idTcReporteEstatus", column=@Column(name="IDTCREPORTEESTATUS", nullable=false, precision=2, scale=0) ) } )

    public ReporteSiniestroLogId getId() {
        return this.id;
    }
    
    public void setId(ReporteSiniestroLogId id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="IDTCREPORTEESTATUS", nullable=false, insertable=false, updatable=false)

    public ReporteEstatusDTO getReporteEstatusDTO() {
        return this.reporteEstatusDTO;
    }
    
    public void setReporteEstatusDTO(ReporteEstatusDTO reporteEstatusDTO) {
        this.reporteEstatusDTO = reporteEstatusDTO;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="IDTOREPORTESINIESTRO", nullable=false, insertable=false, updatable=false)

    public ReporteSiniestroDTO getReporteSiniestroDTO() {
        return this.reporteSiniestroDTO;
    }
    
    public void setReporteSiniestroDTO(ReporteSiniestroDTO reporteSiniestroDTO) {
        this.reporteSiniestroDTO = reporteSiniestroDTO;
    }
@Temporal(TemporalType.DATE)
    @Column(name="FECHAEVENTO", nullable=false, length=7)

    public Date getFechaEvento() {
        return this.fechaEvento;
    }
    
    public void setFechaEvento(Date fechaEvento) {
        this.fechaEvento = fechaEvento;
    }
    
    @Column(name="IDTCUSUARIO", nullable=false, precision=22, scale=0)

    public BigDecimal getIdTcUsuario() {
        return this.idTcUsuario;
    }
    
    public void setIdTcUsuario(BigDecimal idTcUsuario) {
        this.idTcUsuario = idTcUsuario;
    }
   








}