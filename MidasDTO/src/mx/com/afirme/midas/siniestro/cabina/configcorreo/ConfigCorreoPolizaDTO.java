/**
 * 
 */
package mx.com.afirme.midas.siniestro.cabina.configcorreo;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.base.CacheableDTO;

/**
 * ConfigCorreoPolizaDTO entity. @author smvr
 *
 */
@Entity
@Table(name = "TOCONFIGCORREOPOLIZA", schema = "MIDAS")
public class ConfigCorreoPolizaDTO extends CacheableDTO implements java.io.Serializable{

	private static final long serialVersionUID = 936753841L;
	
	private BigDecimal idConfigPoliza;
	private String codigoProducto;
	private String codigoTipoPoliza;
	private Integer numeroPoliza;
	private Integer numeroRenovacion;
	
	private String correo;
	private String usuarioNombre;
	private Date fechaModificacion;
	
	/** default constructor */
	public ConfigCorreoPolizaDTO() {
	}

	/** full constructor */
	public ConfigCorreoPolizaDTO( BigDecimal idConfigPoliza,
								  String codigoProducto,
								  String codigoTipoPoliza,
								  Integer numeroPoliza,
								  Integer numeroRenovacion,
								  String correo,
								  String usuarioNombre,
								  Date fechaModificacion 
								) {
		this.idConfigPoliza    = idConfigPoliza;
		this.codigoProducto    = codigoProducto;
		this.codigoTipoPoliza  = codigoTipoPoliza;
		this.numeroPoliza      = numeroPoliza;
		this.numeroRenovacion  = numeroRenovacion;
		this.correo            = correo;
		this.usuarioNombre     = usuarioNombre;
		this.fechaModificacion = fechaModificacion;
	}
	
	// Property accessors

	/**
	 * @return the idConfigPoliza
	 */
	@Id
	@SequenceGenerator(name = "IDCONFIGPOLIZA_SEQ_GENERADOR",allocationSize = 1, sequenceName = "MIDAS.TOCONFIGCORREOPOLIZA_SEQ")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "IDCONFIGPOLIZA_SEQ_GENERADOR")
	@Column(name = "IDCONFIGPOLIZA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdConfigPoliza() {
		return this.idConfigPoliza;
	}

	
	
	/**
	 * @param idConfigPoliza the idConfigPoliza to set
	 */
	public void setIdConfigPoliza(BigDecimal idConfigPoliza) {
		this.idConfigPoliza = idConfigPoliza;
	}

	@Column(name = "CODIGOPRODUCTO", nullable = false, length = 8)
	public String getCodigoProducto() {
		return this.codigoProducto;
	}

	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	@Column(name = "CODIGOTIPOPOLIZA", nullable = false, length = 8)
	public String getCodigoTipoPoliza() {
		return this.codigoTipoPoliza;
	}

	public void setCodigoTipoPoliza(String codigoTipoPoliza) {
		this.codigoTipoPoliza = codigoTipoPoliza;
	}

	@Column(name = "NUMEROPOLIZA", nullable = false, precision = 8, scale = 0)
	public Integer getNumeroPoliza() {
		return this.numeroPoliza;
	}

	public void setNumeroPoliza(Integer numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	@Column(name = "NUMERORENOVACION", nullable = false, precision = 8, scale = 0)
	public Integer getNumeroRenovacion() {
		return this.numeroRenovacion;
	}

	public void setNumeroRenovacion(Integer numeroRenovacion) {
		this.numeroRenovacion = numeroRenovacion;
	}

	
	@Column(name = "CORREO", length = 240)
	public String getCorreo() {
		return this.correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	@Column(name = "USUARIONOMBRE", length = 50)
	public String getUsuarioNombre() {
		return this.usuarioNombre;
	}

	public void setUsuarioNombre(String usuarioNombre) {
		this.usuarioNombre = usuarioNombre;
	}
	
	/**
	 * @return the fechaModificacion
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAMODIFICACION")
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	/**
	 * @param fechaModificacion the fechaModificacion to set
	 */
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Override
	public String getDescription() {
		return this.codigoProducto + this.codigoTipoPoliza + "-" + this.numeroPoliza + "-" + this.numeroRenovacion;
	}

	@Override
	public String getId() {
		return this.idConfigPoliza.toString();
	}

	@Override
	public boolean equals(Object object) {
		// TODO Auto-generated method stub
		return false;
	}
}
