/**
 * 
 */
package mx.com.afirme.midas.siniestro.cabina.configcorreo;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.siniestro.cabina.configcorreo.ConfigCorreoPolizaDTO;

/**
 * @author smvr
 *
 */
@Remote
public interface ConfigCorreoPolizaFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved ConfigCorreoPolizaDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            ConfigCorreoPolizaDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(ConfigCorreoPolizaDTO entity);

	/**
	 * Delete a persistent ConfigCorreoPolizaDTO entity.
	 * 
	 * @param entity
	 *            ConfigCorreoPolizaDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(ConfigCorreoPolizaDTO entity);

	/**
	 * Persist a previously saved ConfigCorreoPolizaDTO entity and return it or a copy of it
	 * to the sender. A copy of the ConfigCorreoPolizaDTO entity parameter is returned when
	 * the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            ConfigCorreoPolizaDTO entity to update
	 * @return ConfigCorreoPolizaDTO the persisted ConfigCorreoPolizaDTO entity instance, may not be the
	 *         same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public ConfigCorreoPolizaDTO update(ConfigCorreoPolizaDTO entity);


	/**
	 * Find all ConfigCorreoPolizaDTO entities with a specific property value.
	 * 
	 * @param idConfigPoliza
	 *            the name of the idConfigPoliza property to query
	 * @return List<ConfigCorreoPolizaDTO> found by query
	 */
	public ConfigCorreoPolizaDTO findById(BigDecimal idConfigPoliza);

	/**
	 * Find all ConfigCorreoPolizaDTO entities.
	 * 
	 * @return List<ConfigCorreoPolizaDTO> all ConfigCorreoPolizaDTO entities
	 */
	public List<ConfigCorreoPolizaDTO> findAll();
	
	/**
	 * Find all ConfigCorreoPolizaDTO entities by id
	 * @param configCorreoPolizaDTO
	 * @return List<ConfigCorreoPolizaDTO> all ConfigCorreoPolizaDTO entities
	 */
	public List<ConfigCorreoPolizaDTO> listarFiltrado(String numeroPoliza);
}
