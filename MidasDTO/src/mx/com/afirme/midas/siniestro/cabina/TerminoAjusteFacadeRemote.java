package mx.com.afirme.midas.siniestro.cabina;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for SalvamentoEstatusFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface TerminoAjusteFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved SalvamentoEstatus entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SalvamentoEstatus entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(TerminoAjusteDTO entity);

	/**
	 * Delete a persistent SalvamentoEstatus entity.
	 * 
	 * @param entity
	 *            SalvamentoEstatus entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(TerminoAjusteDTO entity);

	/**
	 * Persist a previously saved SalvamentoEstatus entity and return it or a
	 * copy of it to the sender. A copy of the SalvamentoEstatus entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            SalvamentoEstatus entity to update
	 * @return SalvamentoEstatus the persisted SalvamentoEstatus entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public TerminoAjusteDTO update(TerminoAjusteDTO entity);

	public TerminoAjusteDTO findById(BigDecimal id);

	/**
	 * Find all SalvamentoEstatus entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SalvamentoEstatus property to query
	 * @param value
	 *            the property value to match
	 * 
	 * @return List<SalvamentoEstatus> found by query
	 */
	public List<TerminoAjusteDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all SalvamentoEstatus entities.
	 * 
	 * @param 
	 *         
	 * @return List<SalvamentoEstatus> all SalvamentoEstatus entities
	 */
	public List<TerminoAjusteDTO> findAll();
}