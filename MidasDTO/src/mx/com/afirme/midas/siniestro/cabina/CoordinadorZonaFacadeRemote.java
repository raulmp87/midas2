package mx.com.afirme.midas.siniestro.cabina;
// default package


import java.util.List;
import javax.ejb.Remote;


/**
 * Remote interface for CoordinadorZonaFacade.
 * @author MyEclipse Persistence Tools
 */


public interface CoordinadorZonaFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved CoordinadorZonaDTO entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CoordinadorZonaDTO entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CoordinadorZonaDTO entity);
    /**
	 Delete a persistent CoordinadorZonaDTO entity.
	  @param entity CoordinadorZonaDTO entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CoordinadorZonaDTO entity);
   /**
	 Persist a previously saved CoordinadorZonaDTO entity and return it or a copy of it to the sender. 
	 A copy of the CoordinadorZonaDTO entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CoordinadorZonaDTO entity to update
	 @return CoordinadorZonaDTO the persisted CoordinadorZonaDTO entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CoordinadorZonaDTO update(CoordinadorZonaDTO entity);
	public CoordinadorZonaDTO findById( CoordinadorZonaId id);
	 /**
	 * Find all CoordinadorZonaDTO entities with a specific property value.  
	 
	  @param propertyName the name of the CoordinadorZonaDTO property to query
	  @param value the property value to match
	  	  @return List<CoordinadorZonaDTO> found by query
	 */
	public List<CoordinadorZonaDTO> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all CoordinadorZonaDTO entities.
	  	  @return List<CoordinadorZonaDTO> all CoordinadorZonaDTO entities
	 */
	public List<CoordinadorZonaDTO> findAll(
		);	
}