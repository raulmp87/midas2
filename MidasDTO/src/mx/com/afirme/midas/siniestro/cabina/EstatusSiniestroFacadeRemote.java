package mx.com.afirme.midas.siniestro.cabina;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
 * Remote interface for EstatusSiniestroFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface EstatusSiniestroFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved EstatusSiniestro entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            EstatusSiniestro entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(EstatusSiniestroDTO entity);

	/**
	 * Delete a persistent EstatusSiniestro entity.
	 * 
	 * @param entity
	 *            EstatusSiniestro entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(EstatusSiniestroDTO entity);

	/**
	 * Persist a previously saved EstatusSiniestro entity and return it or a
	 * copy of it to the sender. A copy of the EstatusSiniestro entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            EstatusSiniestro entity to update
	 * @return EstatusSiniestro the persisted EstatusSiniestro entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public EstatusSiniestroDTO update(EstatusSiniestroDTO entity);

	public EstatusSiniestroDTO findById(BigDecimal id);

	/**
	 * Find all EstatusSiniestro entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the EstatusSiniestro property to query
	 * @param value
	 *            the property value to match
	 * 
	 * @return List<EstatusSiniestro> found by query
	 */
	public List<EstatusSiniestroDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all EstatusSiniestro entities.
	 * 
	 * @param 
	 *         
	 * @return List<EstatusSiniestro> all EstatusSiniestro entities
	 */
	public List<EstatusSiniestroDTO> findAll();
}