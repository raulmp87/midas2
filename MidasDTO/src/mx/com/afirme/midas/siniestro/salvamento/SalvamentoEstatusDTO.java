package mx.com.afirme.midas.siniestro.salvamento;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * SalvamentoEstatus entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCSALVAMENTOESTATUS", schema = "MIDAS")
public class SalvamentoEstatusDTO implements java.io.Serializable {
	public static final int ESTATUS_ABANDONO = 1;
	public static final int ESTATUS_BAJA_DEPURACION = 2;
	public static final int ESTATUS_INCOSTEABLE = 3;
	public static final int ESTATUS_LEGAL = 4;
	public static final int ESTATUS_NO_EXISTE= 5;
	public static final int ESTATUS_OTROS = 6;
	public static final int ESTATUS_POR_VENDER = 7;
	public static final int ESTATUS_VENDIDO= 8;
	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1008562312642630284L;
	private BigDecimal idTcSalvamentoEstatus;
	private String descripcion;
	private List<SalvamentoSiniestroDTO> salvamentosSiniestro = new ArrayList<SalvamentoSiniestroDTO>(0);

	// Constructors

	/** default constructor */
	public SalvamentoEstatusDTO() {
	}

	// Property accessors
	@Id
	@Column(name = "IDTCSALVAMENTOESTATUS", nullable = false, precision = 1, scale = 0)
	public BigDecimal getIdTcSalvamentoEstatus() {
		return this.idTcSalvamentoEstatus;
	}

	public void setIdTcSalvamentoEstatus(BigDecimal idTcSalvamentoEstatus) {
		this.idTcSalvamentoEstatus = idTcSalvamentoEstatus;
	}

	@Column(name = "DESCRIPCION", length = 50)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "salvamentoEstatus")
	public List<SalvamentoSiniestroDTO> getSalvamentosSiniestro() {
		return this.salvamentosSiniestro;
	}

	public void setSalvamentosSiniestro(
			List<SalvamentoSiniestroDTO> salvamentosSiniestro) {
		this.salvamentosSiniestro = salvamentosSiniestro;
	}

}