package mx.com.afirme.midas.siniestro.salvamento;
// default package

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.IngresoSiniestroDTO;

/**
 * SalvamentoSiniestro entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOSALVAMENTOSINIESTRO", schema = "MIDAS")
public class SalvamentoSiniestroDTO implements java.io.Serializable {
	public static final int ESTATUS_ACTIVO = 0;
	public static final int ESTATUS_VENDIDO = 1;
	public static final int ESTATUS_ELIMINADO = 2;
	
	// Field
	/**
	 * 
	 */
	private static final long serialVersionUID = -820673863220878901L;
	private BigDecimal idToSalvamentoSiniestro;
	private ReporteSiniestroDTO reporteSiniestro;
	private IngresoSiniestroDTO ingresoSiniestro;
	private String nombreItem;
	private String descripcionItem;
	private Double valorEstimado;
	private Double valorReal;
	private String ubicacion;
	private Boolean inspeccion;
	private Date fechaProvision;
	private String observaciones;
	private BigDecimal numeroFactura;
	private String nombreComprador;
	private Boolean salvamentoReaseguro;
	private Boolean salvamentoPerecedero;
	private Date fechaCreacion;
	private BigDecimal idTcUsuarioCreacion;
	private Date fechaModificacion;
	private BigDecimal idTcUsuarioModificacion;
	private BigDecimal cantidad;
	private Double porcentajeIVA;
	private Double porcentajeIVARetencion;
	private Double porcentajeISR;
	private Double porcentajeISRRetencion;
	private Double porcentajeOtros;
	private String unidadMedida;
	private int estatusInterno;
	private SalvamentoEstatusDTO salvamentoEstatus;
	
	// Constructors

	/** default constructor */
	public SalvamentoSiniestroDTO() {
	}
	
	// Property accessors	

	@Id
	@SequenceGenerator(name = "IDTOSALVAMENTOSINIESTRO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOSALVAMENTOSINIESTRO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOSALVAMENTOSINIESTRO_SEQ_GENERADOR")
	@Column(name = "IDTOSALVAMENTOSINIESTRO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSalvamentoSiniestro() {
		return this.idToSalvamentoSiniestro;
	}

	public void setIdToSalvamentoSiniestro(BigDecimal idToSalvamentoSiniestro) {
		this.idToSalvamentoSiniestro = idToSalvamentoSiniestro;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOREPORTESINIESTRO", nullable = false)
	public ReporteSiniestroDTO getReporteSiniestro() {
		return this.reporteSiniestro;
	}

	public void setReporteSiniestro(ReporteSiniestroDTO reporteSiniestro) {
		this.reporteSiniestro = reporteSiniestro;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTOINGRESOSINIESTRO")
	public IngresoSiniestroDTO getIngresoSiniestro() {
		return this.ingresoSiniestro;
	}

	public void setIngresoSiniestro(IngresoSiniestroDTO ingresoSiniestro) {
		this.ingresoSiniestro = ingresoSiniestro;
	}

	@Column(name = "NOMBREITEM", nullable = false, length = 100)
	public String getNombreItem() {
		return this.nombreItem;
	}

	public void setNombreItem(String nombreItem) {
		this.nombreItem = nombreItem;
	}

	@Column(name = "DESCRIPCIONITEM", length = 240)
	public String getDescripcionItem() {
		return this.descripcionItem;
	}

	public void setDescripcionItem(String descripcionItem) {
		this.descripcionItem = descripcionItem;
	}

	@Column(name = "VALORESTIMADO", nullable = false, precision = 16)
	public Double getValorEstimado() {
		return this.valorEstimado;
	}

	public void setValorEstimado(Double valorEstimado) {
		this.valorEstimado = valorEstimado;
	}

	@Column(name = "VALORREAL", precision = 16)
	public Double getValorReal() {
		return this.valorReal;
	}

	public void setValorReal(Double valorReal) {
		this.valorReal = valorReal;
	}

	@Column(name = "UBICACION", nullable = false, length = 500)
	public String getUbicacion() {
		return this.ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	@Column(name = "INSPECCION", precision = 1, scale = 0)
	public Boolean getInspeccion() {
		return this.inspeccion;
	}

	public void setInspeccion(Boolean inspeccion) {
		this.inspeccion = inspeccion;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAPROVISION", length = 7)
	public Date getFechaProvision() {
		return this.fechaProvision;
	}

	public void setFechaProvision(Date fechaProvision) {
		this.fechaProvision = fechaProvision;
	}

	@Column(name = "OBSERVACIONES", length = 240)
	public String getObservaciones() {
		return this.observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	@Column(name = "NUMEROFACTURA", precision = 22, scale = 0)
	public BigDecimal getNumeroFactura() {
		return this.numeroFactura;
	}

	public void setNumeroFactura(BigDecimal numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	@Column(name = "NOMBRECOMPRADOR", length = 100)
	public String getNombreComprador() {
		return this.nombreComprador;
	}

	public void setNombreComprador(String nombreComprador) {
		this.nombreComprador = nombreComprador;
	}

	@Column(name = "SALVAMENTOREASEGURO", precision = 1, scale = 0)
	public Boolean getSalvamentoReaseguro() {
		return this.salvamentoReaseguro;
	}

	public void setSalvamentoReaseguro(Boolean salvamentoReaseguro) {
		this.salvamentoReaseguro = salvamentoReaseguro;
	}

	@Column(name = "SALVAMENTOPERECEDERO", precision = 1, scale = 0)
	public Boolean getSalvamentoPerecedero() {
		return this.salvamentoPerecedero;
	}

	public void setSalvamentoPerecedero(Boolean salvamentoPerecedero) {
		this.salvamentoPerecedero = salvamentoPerecedero;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHACREACION", nullable = false, length = 7)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Column(name = "IDTCUSUARIOCREACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcUsuarioCreacion() {
		return this.idTcUsuarioCreacion;
	}

	public void setIdTcUsuarioCreacion(BigDecimal idTcUsuarioCreacion) {
		this.idTcUsuarioCreacion = idTcUsuarioCreacion;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAMODIFICACION", length = 7)
	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Column(name = "IDTCUSUARIOMODIFICACION", precision = 22, scale = 0)
	public BigDecimal getIdTcUsuarioModificacion() {
		return this.idTcUsuarioModificacion;
	}

	public void setIdTcUsuarioModificacion(BigDecimal idTcUsuarioModificacion) {
		this.idTcUsuarioModificacion = idTcUsuarioModificacion;
	}

	@Column(name = "CANTIDAD", nullable = false, precision = 22, scale = 0)
	public BigDecimal getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	@Column(name = "PORCENTAJEIVA", precision = 5)
	public Double getPorcentajeIVA() {
		return this.porcentajeIVA;
	}

	public void setPorcentajeIVA(Double porcentajeIVA) {
		this.porcentajeIVA = porcentajeIVA;
	}

	@Column(name = "PORCENTAJEIVARETENCION", precision = 5)
	public Double getPorcentajeIVARetencion() {
		return this.porcentajeIVARetencion;
	}

	public void setPorcentajeIVARetencion(Double porcentajeIVARetencion) {
		this.porcentajeIVARetencion = porcentajeIVARetencion;
	}

	@Column(name = "PORCENTAJEISR", precision = 5)
	public Double getPorcentajeISR() {
		return this.porcentajeISR;
	}

	public void setPorcentajeISR(Double porcentajeISR) {
		this.porcentajeISR = porcentajeISR;
	}

	@Column(name = "PORCENTAJEISRRETENCION", precision = 5)
	public Double getPorcentajeISRRetencion() {
		return this.porcentajeISRRetencion;
	}

	public void setPorcentajeISRRetencion(Double porcentajeISRRetencion) {
		this.porcentajeISRRetencion = porcentajeISRRetencion;
	}

	@Column(name = "PORCENTAJEOTROS", precision = 5)
	public Double getPorcentajeOtros() {
		return this.porcentajeOtros;
	}

	public void setPorcentajeOtros(Double porcentajeOtros) {
		this.porcentajeOtros = porcentajeOtros;
	}

	@Column(name = "UNIDADMEDIDA", length = 100)
	public String getUnidadMedida() {
		return this.unidadMedida;
	}

	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}
	
	@Column(name = "ESTATUSINTERNO", nullable = false, precision = 1, scale = 0)
	public int getEstatusInterno() {
		return estatusInterno;
	}

	public void setEstatusInterno(int estatusInterno) {
		this.estatusInterno = estatusInterno;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCSALVAMENTOESTATUS", nullable = false)
	public SalvamentoEstatusDTO getSalvamentoEstatus() {
		return this.salvamentoEstatus;
	}

	public void setSalvamentoEstatus(SalvamentoEstatusDTO salvamentoEstatus) {
		this.salvamentoEstatus = salvamentoEstatus;
	}
}