package mx.com.afirme.midas.siniestro.salvamento;
// default package

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;

/**
 * Remote interface for SalvamentoSiniestroFacade.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface SalvamentoSiniestroFacadeRemote {
	/**
	 * Perform an initial save of a previously unsaved SalvamentoSiniestro
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            SalvamentoSiniestro entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(SalvamentoSiniestroDTO entity);

	/**
	 * Delete a persistent SalvamentoSiniestro entity.
	 * 
	 * @param entity
	 *            SalvamentoSiniestro entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SalvamentoSiniestroDTO entity);

	/**
	 * Persist a previously saved SalvamentoSiniestro entity and return it or a
	 * copy of it to the sender. A copy of the SalvamentoSiniestro entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            SalvamentoSiniestro entity to update
	 * @return SalvamentoSiniestro the persisted SalvamentoSiniestro entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SalvamentoSiniestroDTO update(SalvamentoSiniestroDTO entity);

	public SalvamentoSiniestroDTO findById(BigDecimal id);

	/**
	 * Find all SalvamentoSiniestro entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SalvamentoSiniestro property to query
	 * @param value
	 *            the property value to match
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<SalvamentoSiniestro> found by query
	 */
	public List<SalvamentoSiniestroDTO> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all SalvamentoSiniestro entities.
	 * 
	 * @param rowStartIdxAndCount
	 *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
	 *            row index in the query result-set to begin collecting the
	 *            results. rowStartIdxAndCount[1] specifies the the maximum
	 *            count of results to return.
	 * @return List<SalvamentoSiniestro> all SalvamentoSiniestro entities
	 */
	public List<SalvamentoSiniestroDTO> findAll();
	
	
	/**
	 * Find all not deleted SalvamentoSiniestro entities.
	 * 
	 * @param ReporteSiniestroDTO
	 * @return List<SalvamentoSiniestro> all not deleted SalvamentoSiniestro entities
	 */
	public List<SalvamentoSiniestroDTO> findByNotDeletedStatus(ReporteSiniestroDTO reporteSiniestroDTO);
	
	/**
	 * Find salvamentDTO with status pending entities.
	 * 
	 * @param ReporteSiniestroDTO
	 * @return List<SalvamentoSiniestro> salvamentDTO with status pending entities
	 */
	public List<SalvamentoSiniestroDTO> getSalvamentosPorReporteYEstatus(ReporteSiniestroDTO reporteSiniestroDTO,Object... params);
}