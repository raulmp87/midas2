package mx.com.afirme.midas.base;

public abstract class CacheableDTO extends MidasAbstractoDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public abstract Object getId();

	public abstract String getDescription();
}
