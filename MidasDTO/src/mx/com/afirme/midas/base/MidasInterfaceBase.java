package mx.com.afirme.midas.base;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

public interface MidasInterfaceBase<T extends CacheableDTO> {

	public List<T> findAll();
	
	public List<T> listRelated(Object id);
	
	public T findById(BigDecimal id);
	
	public T findById(CatalogoValorFijoId id);
	
	public T findById(double id);
}
