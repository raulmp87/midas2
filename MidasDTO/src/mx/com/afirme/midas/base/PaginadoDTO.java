package mx.com.afirme.midas.base;

import java.io.Serializable;

import javax.persistence.Transient;

public abstract class PaginadoDTO implements Serializable {

	
	private static final long serialVersionUID = 4615840953573663042L;
	
	@Transient
	private Integer primerRegistroACargar;
	
	@Transient
	private Integer numeroMaximoRegistrosACargar;

	public Integer getPrimerRegistroACargar() {
		return primerRegistroACargar;
	}

	public void setPrimerRegistroACargar(Integer primerRegistroACargar) {
		this.primerRegistroACargar = primerRegistroACargar;
	}

	public Integer getNumeroMaximoRegistrosACargar() {
		return numeroMaximoRegistrosACargar;
	}

	public void setNumeroMaximoRegistrosACargar(Integer numeroMaximoRegistrosACargar) {
		this.numeroMaximoRegistrosACargar = numeroMaximoRegistrosACargar;
	}

	
}
