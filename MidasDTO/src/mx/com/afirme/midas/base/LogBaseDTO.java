package mx.com.afirme.midas.base;

import javax.persistence.Transient;


public class LogBaseDTO extends PaginadoDTO {
	
	private static final long serialVersionUID = 1L;

	@Transient
	private String nombreUsuarioLog;
		
	/**
	 * @return the nombreUsuarioLog
	 */
	public String getNombreUsuarioLog() {
		return nombreUsuarioLog;
	}

	/**
	 * @param nombreUsuarioLog the nombreUsuarioLog to set
	 */
	public void setNombreUsuarioLog(String nombreUsuarioLog) {
		this.nombreUsuarioLog = nombreUsuarioLog;
	}
	
	
}
