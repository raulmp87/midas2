package mx.com.afirme.midas.base;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;

public interface MidasInterfaceBaseAuto<T extends CacheableDTO> extends MidasInterfaceBase{
	public List<T> findAll(BigDecimal... ids);
}