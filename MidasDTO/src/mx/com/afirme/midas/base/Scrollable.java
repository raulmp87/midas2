package mx.com.afirme.midas.base;

import java.io.Serializable;

import javax.persistence.Transient;

public abstract class Scrollable implements Serializable {

	private static final long serialVersionUID = -8028360314912164109L;

	//El total de registros a devolver en una consulta
	private Integer total;
	
	//La posicion del registro inicial en el set de datos
	private Integer posStart;
	
    //Numero de registros a cargar en el set de datos
	private Integer count;
	
	//Indice de la columna por la cual se va a ordenar la consulta
	private String orderBy;
	
	//ASC o DESC
	private String direct;
	
	@Transient
	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	@Transient
	public Integer getPosStart() {
		return posStart;
	}

	public void setPosStart(Integer posStart) {
		this.posStart = posStart;
	}

	@Transient
	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}


	@Transient
	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	@Transient
	public String getDirect() {
		return direct;
	}

	public void setDirect(String direct) {
		this.direct = direct;
	}
	
	
}
