package mx.com.afirme.midas.base;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public abstract class MidasAbstractoDTO extends PaginadoDTO {

	/**
	 * The serialVersionUID
	 */
	private static final long serialVersionUID = 6191457987744441774L;
	/**
	 * The instance's creation date.
	 */
	@Column(name = "CREATION_DATE")
	@Temporal(value = TemporalType.TIMESTAMP)
	protected Date creationDate = null;

	/**
	 * The instance's creator unique identifier.
	 */
	@Column(name = "CREATOR_USER_ID")
	protected String creatorUserId = null;

	/**
	 * The instance's last update date.
	 */
	@Column(name = "LAST_UPDATE_DATE")
	@Temporal(value = TemporalType.TIMESTAMP)
	protected Date lastUpdateDate = null;

	/**
	 * The isntance's last modification date.
	 */
	@Column(name = "LAST_MODIFIED_BY_USER_ID")
	protected String lastModifiedByUserId = null;

	/**
	 * Creates a new instance of <code>AbstractDTO</code> and initializes the
	 * <code>creationDate</code> and <code>lastUpdateDate</code> attributes.
	 */
	public MidasAbstractoDTO() {
		this.creationDate = new Date();
		this.lastUpdateDate = new Date();
	}

	/**
	 * Retrieves the <code>java.util.Date</code> in which the instance was
	 * created.
	 * 
	 * @return The <code>java.util.Date</code> in which the instance was
	 *         created.
	 */
	public Date getCreationDate() {
		return this.creationDate;
	}

	/**
	 * Sets the <code>java.util.Date</code> in which the instance was created.
	 * 
	 * @param creationDate
	 *            The <code>java.util.Date</code> in which the instance was
	 *            created.
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Retrieves the user's unique identifier of the user that created this
	 * instance.
	 * 
	 * @return The user's unique identifier of the user that created this
	 *         instance.
	 */
	public String getCreatorUserId() {
		return this.creatorUserId;
	}

	/**
	 * Sets the user's unique identifier of the user that created this instance.
	 * 
	 * @param creatorUserId
	 *            The user's unique identifier of the user that created this
	 *            instance.
	 */
	public void setCreatorUserId(String creatorUserId) {
		this.creatorUserId = creatorUserId;
		if (StringUtil.isEmpty(this.lastModifiedByUserId)) {
			this.lastModifiedByUserId = this.creatorUserId;
		} // End of if
	}

	/**
	 * Retrieves the <code>java.util.Date</code> in which the instance was last
	 * modified.
	 * 
	 * @return The <code>java.util.Date</code> in which the instance was last
	 *         modified.
	 */
	public Date getLastUpdateDate() {
		return this.lastUpdateDate;
	}

	/**
	 * Sets the <code>java.util.Date</code> in which the instance was last
	 * modified.
	 * 
	 * @param lastUpdateDate
	 *            The <code>java.util.Date</code> in which the instance was last
	 *            modified.
	 */
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	/**
	 * Retrieves the user's unique identifier of the user that modified this
	 * instance.
	 * 
	 * @return The user's unique identifier of the user that modified this
	 *         instance.
	 */
	public String getLastModifiedByUserId() {
		return this.lastModifiedByUserId;
	}

	/**
	 * Sets the user's unique identifier of the user that modified this
	 * instance.
	 * 
	 * @param lastModifiedByUserId
	 *            The user's unique identifier of the user that modified this
	 *            instance.
	 */
	public void setLastModifiedByUserId(String lastModifiedByUserId) {
		this.lastModifiedByUserId = lastModifiedByUserId;
	}

	/**
	 * Indicates whether some other object is "equal to" this one.
	 * 
	 * @return <code>true</code> if this object is the same as the object
	 *         argument; <code>false</code> otherwise.
	 */
	public abstract boolean equals(Object object);

	/**
	 * Retrieves all the getter methods within this instance.
	 * 
	 * @return All the getter methods within this instance.
	 */
	private List<Method> getGetterMethods() {
		List<Method> getters = new ArrayList<Method>();
		Method[] allMethods = this.getClass().getMethods();
		for (int i = 0; i < allMethods.length; i++) {
			String methodName = allMethods[i].getName();
			if (methodName.startsWith("get")) {
				getters.add(allMethods[i]);
			} // End of if
		} // End of for()
		return getters;
	}

	/**
	 * Retrieves all the setter methods within this instance.
	 * 
	 * @return All the setter methods within this instance.
	 */
	private String getAttributeName(Method getterMethod) {
		StringBuilder attribute = new StringBuilder();
		String methodName = getterMethod.getName();
		char firstLetter = methodName.charAt("get".length());
		attribute.append(Character.toLowerCase(firstLetter));
		attribute.append(methodName.substring("get".length() + 1));
		return attribute.toString();
	}

	/**
	 * Returns a string representation of the instance. It will rertrieve a
	 * description of all its attributes and its values.
	 * 
	 * @return a string representation of this DTO.
	 */
	public String toString() {
		StringBuilder buffer = new StringBuilder();
		buffer.append("[");
		List<Method> getterMethodList = this.getGetterMethods();
		for (int i = 0; i < getterMethodList.size(); i++) {
			Method method = (Method) getterMethodList.get(i);
			String attribute = this.getAttributeName(method);
			try {
				Object value = method.invoke(this, (Object[]) null);
				if (!(value instanceof MidasAbstractoDTO)
						&& !(value instanceof Collection<?>)) {
					buffer.append(attribute);
					buffer.append("=\"");
					buffer.append(value);
					buffer.append("\"");
					if (getterMethodList.size() != (i + 1)) {
						buffer.append(";");
					} // End of if
				} // End of if
			} catch (Exception exception) {
				// No problem. Continue Working.
			} // End of try/catch
		} // End of for
		buffer.append("]");
		return buffer.toString();
	}

}
