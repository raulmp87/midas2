package mx.com.afirme.vida.dao.asistencia;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.vida.domain.asistencia.AsistenciaVida;
import mx.com.afirme.vida.dto.asistencia.AsistenciaVidaFiltro;

@Local
public interface AsistenciaVidaDao {

	public List<AsistenciaVida> buscarEnvioProveedor(AsistenciaVidaFiltro filtro);
}
