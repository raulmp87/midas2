package mx.com.afirme.vida.service.tarifa;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

import mx.com.afirme.vida.domain.movil.cotizador.TarifasMovilVidaDTO;

/**
 * Local interface for TarifaMovilVidaService.
 * 
 * @author 
 */
public interface TarifaMovilVidaService {

	public TarifasMovilVidaDTO save(TarifasMovilVidaDTO entity);

	public void delete(TarifasMovilVidaDTO entity);

	public String update(TarifasMovilVidaDTO entity);

	public TarifasMovilVidaDTO findById(Long id);

	public List<TarifasMovilVidaDTO> findByProperty(String propertyName,
			Object value);

	public List<TarifasMovilVidaDTO> findAll();
	
	public List<TarifasMovilVidaDTO> findByFilters(TarifasMovilVidaDTO filter);
	
	public List<String> cargaMasiva(String url) throws Exception;
	
	public boolean existeTarifaMovilVida(String property,Object value);
	
	public List<TarifasMovilVidaDTO> findByFiltersDiferentID(TarifasMovilVidaDTO filter);
	
	public List<TarifasMovilVidaDTO> findByPropertyActivos(String propertyName,
			final Object value);
	
	public TarifasMovilVidaDTO findFechaNacimientoTarifaMovilVida(Date FechaNacimiento);
	
}