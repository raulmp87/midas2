package mx.com.afirme.vida.service.asistencia;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.vida.domain.asistencia.AsistenciaVida;
import mx.com.afirme.vida.dto.asistencia.AsistenciaVidaFiltro;

@Local
public interface AsistenciaVidaService {

	public AsistenciaVida save(AsistenciaVida entity);

	public void update(AsistenciaVida entity);

	public AsistenciaVida findById(Long id);

	public List<AsistenciaVida> findByProperty(String propertyName, Object value);

	public List<AsistenciaVida> findAll();

	public List<AsistenciaVida> findByFilters(AsistenciaVidaFiltro filter);

	public List<AsistenciaVida> findBatchesPendientes();

	public String guardaArchivo(AsistenciaVida entity);	

	public AsistenciaVida setEnviado(AsistenciaVida entity);
	
	public void initialize();
}