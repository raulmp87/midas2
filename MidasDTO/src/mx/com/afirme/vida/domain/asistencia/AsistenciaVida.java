package mx.com.afirme.vida.domain.asistencia;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas2.annotation.Exportable;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name = "TOASISTENCIA_VIDA", schema = "MIDAS")
public class AsistenciaVida extends CacheableDTO implements Serializable, Entidad {

	/**
	 * 
	 */
	public enum AsistenciaVidaEstatus{
		INICIADO,
		RECIBIDO,
		POR_ENVIAR,
		ENVIADO;
	};
	
	
	private static final long serialVersionUID = -1030890630384995587L;
	private Long id;
	private Date fRecepcion;
	private AsistenciaVidaEstatus estatus;
	private String usuarioRegistro;
	private Integer cantRecibidos;
	private Integer cantAceptados;
	private String cantRechazados;
	private Date fIniTrans;
	private Date fFinTrans;
	private List<AsistenciaVidaDetalle> asegurados  = new ArrayList<AsistenciaVidaDetalle>();
	
	@Id
	@Column(name = "ID")
	@Exportable(columnName = "ID", columnOrder = 0)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "F_RECEPCION")	
	public Date getfRecepcion() {
		return fRecepcion;
	}

	public void setfRecepcion(Date fRecepcion) {
		this.fRecepcion = fRecepcion;
	}

	@Column(name = "ESTATUS")
	@Enumerated(EnumType.ORDINAL)
	public AsistenciaVidaEstatus getEstatus() {
		return estatus;
	}

	public void setEstatus(AsistenciaVidaEstatus estatus) {
		this.estatus = estatus;
	}

	@Exportable(columnName = "ESTATUS", columnOrder = 2)
	public String getEstatusDetalle() {
		return this.estatus.toString();
	}

	@Column(name = "USUARIO_REGISTRO")
	@Exportable(columnName = "USUARIO_REGISTRO", columnOrder = 3)
	public String getUsuarioRegistro() {
		return usuarioRegistro;
	}

	public void setUsuarioRegistro(String usuarioRegistro) {
		this.usuarioRegistro = usuarioRegistro;
	}

	@Column(name = "CANT_RECIBIDOS")
	@Exportable(columnName = "CANT_RECIBIDOS", columnOrder = 4)	
	public Integer getCantRecibidos() {
		return cantRecibidos;
	}

	public void setCantRecibidos(Integer cantRecibidos) {
		this.cantRecibidos = cantRecibidos;
	}

	@Column(name = "CANT_ACEPTADOS")
	@Exportable(columnName = "CANT_ACEPTADOS", columnOrder = 5)
	public Integer getCantAceptados() {
		return cantAceptados;
	}

	public void setCantAceptados(Integer cantAceptados) {
		this.cantAceptados = cantAceptados;
	}

	@Column(name = "CANT_RECHAZADOS")
	@Exportable(columnName = "CANT_RECHAZADOS", columnOrder = 6)
	public String getCantRechazados() {
		return cantRechazados;
	}

	public void setCantRechazados(String cantRechazados) {
		this.cantRechazados = cantRechazados;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "F_INI_TRANS")
	public Date getfIniTrans() {
		return fIniTrans;
	}

	public void setfIniTrans(Date fIniTrans) {
		this.fIniTrans = fIniTrans;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "F_FIN_TRANS")
	public Date getfFinTrans() {
		return fFinTrans;
	}

	public void setfFinTrans(Date fFinTrans) {
		this.fFinTrans = fFinTrans;
	}
	
	@OneToMany(fetch=FetchType.EAGER,  mappedBy = "asistenciaVida", cascade = CascadeType.ALL)
	public List<AsistenciaVidaDetalle> getAsegurados() {
		return asegurados;
	}

	public void setAsegurados(List<AsistenciaVidaDetalle> asegurados) {
		this.asegurados = asegurados;
	}
	
	@Transient
	public StringBuffer getRecordHeader(){
		char separator = '|';
		StringBuffer record = new StringBuffer();
		record.append("numPoliza"); record.append(separator);
		record.append("tipoAsistencia"); record.append(separator);
		record.append("idCliente"); record.append(separator);
		record.append("fechaInicio"); record.append(separator);
		record.append("nombre"); record.append(separator);
		record.append("apellidoPaterno"); record.append(separator);
		record.append("apellidoMaterno"); record.append(separator);
		record.append("cveSexo"); record.append(separator);
		record.append("fechaNacimiento"); record.append(separator);
		record.append("cveEntidadFed"); record.append(separator);
		record.append("plazoPagado"); record.append(separator);
		record.append("numCredito"); record.append(separator);
		record.append("plazoCredito"); record.append(separator);
		record.append("ciudad"); record.append(separator);
		record.append("municipio"); record.append(separator);
		record.append("codigoPostal");
		
		return record;
	}
	
	@Transient
	public String getNombreArchivo(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		return "AsistenciaVida" + this.id + "_" + sdf.format(this.fRecepcion);
	}

	@Transient
	@Exportable(columnName = "FECHA_RECEPCION", columnOrder = 1)
	public String getFechaRecepcion(){
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(this.fRecepcion);
	}	
	
	@Override
	public <K> K getKey() {
		return (K) this.id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return (K) this.id;
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object object) {
		// TODO Auto-generated method stub
		return false;
	} 

}
