package mx.com.afirme.vida.domain.asistencia;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas2.annotation.Exportable;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.vida.domain.asistencia.AsistenciaVida.AsistenciaVidaEstatus;

@Entity
@Table(name = "TOASISTENCIA_VIDA_DETA", schema = "MIDAS")
public class AsistenciaVidaDetalle extends CacheableDTO implements
		Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 50130745598898224L;
	private String id;
	private String cveTAsistencia;
	private String numPoliza;
	private Date fInicio;
	private Integer idCliente;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private Date fNacimiento;
	private String cveSexo;
	private String cveMovto;
	private String cveEntidadFed;
	private Integer plazoPagado;
	private Integer numCredito;
	private Integer plazoCredito;
	private String ciudad;
	private String municipio;
	private String codigoPostal;
	private AsistenciaVidaEstatus estatus;
	private Date fhEnvio;
	private Integer proveedorId;
	private Date fhRegistro;
	private AsistenciaVida asistenciaVida;

	@Id
	@Column(name = "ID")	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "CVE_T_ASISTENCIA")
	public String getCveTAsistencia() {
		return cveTAsistencia;
	}

	public void setCveTAsistencia(String cveTAsistencia) {
		this.cveTAsistencia = cveTAsistencia;
	}

	@Column(name = "NUM_POLIZA")	
	@Exportable(columnName = "NUM_POLIZA", columnOrder = 0)
	public String getNumPoliza() {
		return numPoliza;
	}

	public void setNumPoliza(String numPoliza) {
		this.numPoliza = numPoliza;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "F_INICIO")
	@Exportable(columnName = "FECHA_INICIO", columnOrder = 4)
	public Date getfInicio() {
		return fInicio;
	}

	public void setfInicio(Date fInicio) {
		this.fInicio = fInicio;
	}

	@Column(name = "ID_CLIENTE")
	@Exportable(columnName = "ID_CLIENTE", columnOrder = 3)
	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	@Column(name = "NOMBRE")
	@Exportable(columnName = "NOMBRE", columnOrder = 5)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "APELLIDO_PATERNO")
	@Exportable(columnName = "APELLIDO_PATERNO", columnOrder = 6)
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	@Column(name = "APELLIDO_MATERNO")
	@Exportable(columnName = "APELLIDO_MATERNO", columnOrder = 7)
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "F_NACIMIENTO")
	@Exportable(columnName = "FECHA_NACIMIENTO", columnOrder = 9)
	public Date getfNacimiento() {
		return fNacimiento;
	}

	public void setfNacimiento(Date fNacimiento) {
		this.fNacimiento = fNacimiento;
	}

	@Column(name = "CVE_SEXO")
	@Exportable(columnName = "SEXO", columnOrder = 8)
	public String getCveSexo() {
		return cveSexo;
	}

	public void setCveSexo(String cveSexo) {
		this.cveSexo = cveSexo;
	}

	@Column(name = "CVE_MOVTO")
	public String getCveMovto() {
		return cveMovto;
	}

	public void setCveMovto(String cveMovto) {
		this.cveMovto = cveMovto;
	}

	@Column(name = "CVE_ENTIDAD_FED")
	public String getCveEntidadFed() {
		return cveEntidadFed;
	}

	public void setCveEntidadFed(String cveEntidadFed) {
		this.cveEntidadFed = cveEntidadFed;
	}

	@Column(name = "PLAZO_PAGADO")
	@Exportable(columnName = "PLAZO_PAGADO", columnOrder = 11)
	public Integer getPlazoPagado() {
		return plazoPagado;
	}

	public void setPlazoPagado(Integer plazoPagado) {
		this.plazoPagado = plazoPagado;
	}

	@Column(name = "NUM_CREDITO")
	@Exportable(columnName = "NUM_CREDITO", columnOrder = 12)
	public Integer getNumCredito() {
		return numCredito;
	}

	public void setNumCredito(Integer numCredito) {
		this.numCredito = numCredito;
	}

	@Column(name = "PLAZO_CREDITO")
	@Exportable(columnName = "PLAZO_CREDITO", columnOrder = 13)
	public Integer getPlazoCredito() {
		return plazoCredito;
	}

	public void setPlazoCredito(Integer plazoCredito) {
		this.plazoCredito = plazoCredito;
	}

	@Column(name = "CIUDAD")
	@Exportable(columnName = "CIUDAD", columnOrder = 14)
	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	@Column(name = "MUNICIPIO")
	@Exportable(columnName = "MUNICIPIO_DELEGACION", columnOrder = 15)
	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	@Column(name = "CODIGO_POSTAL")
	@Exportable(columnName = "CP", columnOrder = 16)
	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	@Column(name = "ESTATUS")
	@Enumerated(EnumType.ORDINAL)
	public AsistenciaVidaEstatus getEstatus() {
		return estatus;
	}

	public void setEstatus(AsistenciaVidaEstatus estatus) {
		this.estatus = estatus;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FH_ENVIO")
	public Date getFhEnvio() {
		return fhEnvio;
	}

	public void setFhEnvio(Date fhEnvio) {
		this.fhEnvio = fhEnvio;
	}

	@Column(name = "PROVEEDOR_ID")
	public Integer getProveedorId() {
		return proveedorId;
	}

	public void setProveedorId(Integer proveedorId) {
		this.proveedorId = proveedorId;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FH_REGISTRO")
	public Date getFhRegistro() {
		return fhRegistro;
	}

	public void setFhRegistro(Date fhRegistro) {
		this.fhRegistro = fhRegistro;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "TOASISTENCIA_VIDA_ID", referencedColumnName = "ID")
	public AsistenciaVida getAsistenciaVida() {
		return asistenciaVida;
	}

	public void setAsistenciaVida(AsistenciaVida asistenciaVida) {
		this.asistenciaVida = asistenciaVida;
	}
	
	@Transient
	public StringBuffer getRecord(){
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		char separator = '|';
		StringBuffer record = new StringBuffer();
		record.append(this.numPoliza); record.append(separator);
		record.append(getTipoAsistencia()); record.append(separator);
		record.append(this.idCliente); record.append(separator);
		record.append(sdf.format(this.fInicio)); record.append(separator);
		record.append(this.nombre); record.append(separator);
		record.append(this.apellidoPaterno); record.append(separator);
		record.append(this.apellidoMaterno); record.append(separator);
		record.append(this.cveSexo); record.append(separator);
		record.append(sdf.format(this.fNacimiento)); record.append(separator);
		record.append(this.cveEntidadFed); record.append(separator);
		record.append(this.plazoPagado); record.append(separator);
		record.append(this.numCredito); record.append(separator);
		record.append(this.plazoCredito); record.append(separator);
		record.append(this.ciudad); record.append(separator);
		record.append(this.municipio); record.append(separator);
		record.append(this.codigoPostal);
		
		return record;
	}
	
	@Transient
	@Exportable(columnName = "ENTIDAD_FEDERATIVA", columnOrder = 10)
	public String getEntidadFederativa() {
		String val = new String();
		switch(Integer.valueOf(cveEntidadFed)){
			case 1: val = "AGUASCALIENTES"; break;
			case 2: val = "BAJA CALIFORNIA"; break;
			case 3: val = "BAJA CALIFORNIA SUR"; break;
			case 4: val = "CAMPECHE"; break;
			case 5: val = "COAHUILA"; break;
			case 6: val = "COLIMA"; break;
			case 7: val = "CHIAPAS"; break;
			case 8: val = "CHIHUAHUA"; break;
			case 9: val = "CIUDAD DE MÉXICO"; break;
			case 10: val = "DURANGO"; break;
			case 11: val = "GUANAJUATO"; break;
			case 12: val = "GUERRERO"; break;
			case 13: val = "HIDALGO"; break;
			case 14: val = "JALISCO"; break;
			case 15: val = "MÉXICO"; break;
			case 16: val = "MICHOACÁN"; break;
			case 17: val = "MORELOS"; break;
			case 18: val = "NAYARIT"; break;
			case 19: val = "NUEVO LEÓN"; break;
			case 20: val = "OAXACA"; break;
			case 21: val = "PUEBLA"; break;
			case 22: val = "QUERÉTARO"; break;
			case 23: val = "QUINTANA ROO"; break;
			case 24: val = "SAN LUIS POTOSÍ"; break;
			case 25: val = "SINALOA"; break;
			case 26: val = "SONORA"; break;
			case 27: val = "TABASCO"; break;
			case 28: val = "TAMAULIPAS"; break;
			case 29: val = "TLAXCALA"; break;
			case 30: val = "VERACRUZ"; break;
			case 31: val = "YUCATÁN"; break;
			case 32: val = "ZACATECAS"; break;
		}
		return val;
	}
	
	@Transient
	@Exportable(columnName = "MOVIMIENTO", columnOrder = 2)
	public String getMovimiento() {
		if(this.cveMovto.equals("A")){
			return "ALTA";
		}else if(this.cveMovto.equals("M")){
			return "MODIFICACIÓN";
		}else if(this.cveMovto.equals("B")){
			return "BAJA";
		}
		return null;
	}	

	@Transient
	@Exportable(columnName = "TIPO_ASISTENCIA", columnOrder = 1)
	public String getTipoAsistencia() {
		if(this.cveTAsistencia.equals("M")){
			return "MÉDICA";
		}else if(this.cveMovto.equals("F")){
			return "FUNERARIA";
		}
		return null;
	}		
	
	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object object) {
		// TODO Auto-generated method stub
		return false;
	}

}
