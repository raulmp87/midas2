package mx.com.afirme.vida.domain.movil.cotizador;

import java.io.Serializable;



public class RFC
    implements Serializable {
  
  private static final long serialVersionUID = -6207674637965129756L;

  private String acronym = null;
	
	private String date = null;
	
	private String homoclave = null;
	
	public RFC() {
	  // Nothing to do
	}
	
	public RFC(String acronym, String date, String homoclave) {
		this.acronym = acronym;
		this.date = date;
		this.homoclave = homoclave;
	}
	
	public String getAcronym() {
		return acronym;
	}
	
	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}
	
	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getHomoclave() {
		return homoclave;
	}

	public void setHomoclave(String homoclave) {
		this.homoclave = homoclave;
	}

	public boolean equals(Object object) {
	  return (this==object);
	}
	
}
