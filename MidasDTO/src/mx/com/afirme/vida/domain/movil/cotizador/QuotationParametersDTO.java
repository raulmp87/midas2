package mx.com.afirme.vida.domain.movil.cotizador;

public class QuotationParametersDTO extends QuotationDTO  implements java.io.Serializable{  
 
  private static final long serialVersionUID = 1L;

  private String receiptKey;
  
  private String asignationDate;
  
  private String limitReceiptDate;
  
  private String hasAccountCharge;
  
  private boolean hasMoreThanOneInsuredPerson;
  
  private boolean hasCoveragePlatino;
  
  private boolean hasAppendix;
  
  private boolean setAllSubsequentReceipts = false;
  
  private String policySituation = null;
  
  private int renovationNumber = 0;
  
  
  public void setHasCoveragePlatino(boolean hasCoveragePlatino) {
	this.hasCoveragePlatino = hasCoveragePlatino;
  }

  public boolean getHasCoveragePlatino() {
	return hasCoveragePlatino;
  }

  public boolean getHasMoreThanOneInsuredPerson() {
	return hasMoreThanOneInsuredPerson;
  }
	
  public void setHasMoreThanOneInsuredPerson(boolean hasMoreThanOneInsuredPerson) {
		this.hasMoreThanOneInsuredPerson = hasMoreThanOneInsuredPerson;
  }
	
  public String getHasAccountCharge() {
    return hasAccountCharge;
  }

  public void setHasAccountCharge(String hasAccountCharge) {
    this.hasAccountCharge = hasAccountCharge;
  }

  public String getLimitReceiptDate() {
    return limitReceiptDate;
  }

  public void setLimitReceiptDate(String limitReceiptDate) {
    this.limitReceiptDate = limitReceiptDate;
  }

  public String getReceiptKey() {
    return receiptKey;
  }

  public void setReceiptKey(String receiptKey) {
    this.receiptKey = receiptKey;
  }

  public String getAsignationDate() {
    return asignationDate;
  }

  public void setAsignationDate(String asignationDate) {
    this.asignationDate = asignationDate;
  }

  public boolean isSetAllSubsequentReceipts() {
    return setAllSubsequentReceipts;
  }

  public void setSetAllSubsequentReceipts(boolean setAllSubsequentReceipts) {
    this.setAllSubsequentReceipts = setAllSubsequentReceipts;
  }

  public boolean isHasAppendix() {
    return hasAppendix;
  }

  public void setHasAppendix(boolean hasAppendix) {
    this.hasAppendix = hasAppendix;
  }

  public String getPolicySituation() {
    return policySituation;
  }

  public void setPolicySituation(String policySituation) {
    this.policySituation = policySituation;
  }

  public int getRenovationNumber() {
    return renovationNumber;
  }

  public void setRenovationNumber(int renovationNumber) {
    this.renovationNumber = renovationNumber;
  }
  
}
