package mx.com.afirme.vida.domain.movil.cotizador;

public class MensajesCotizacion  implements java.io.Serializable{
		String plan;
		String descripcion;
		String formaPago;
		
		public String getPlan() {
			return plan;
		}
		public void setPlan(String plan) {
			this.plan = plan;
		}
		public String getDescripcion() {
			return descripcion;
		}
		public void setDescripcion(String descripcion) {
			this.descripcion = descripcion;
		}
		public String getFormaPago() {
			return formaPago;
		}
		public void setFormaPago(String formaPago) {
			this.formaPago = formaPago;
		}
		
		
	
}
