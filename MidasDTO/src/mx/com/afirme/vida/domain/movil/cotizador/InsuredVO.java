package mx.com.afirme.vida.domain.movil.cotizador;

public class InsuredVO
    extends CustomerVO {

  private static final long serialVersionUID = -2535693514935675989L;

  private Boolean afirmeClient;
	
	private InsuredTypeVO insuredType;
	
	private CoverageVO coverage;
	
	private boolean oiiApproved = false;
	
	private ProfileVO profile = null;
	
	private Double assuredAmount = null;
	
	private QuotationVO quotation = null;
	
	private Integer businessLineId = null;
	
	private Integer insuredVersionId = null;
	
	private String preferredBeneficiary = null;

	public InsuredVO() {
		this.insuredType = new InsuredTypeVO();	
		this.coverage = new CoverageVO();
		this.profile = new ProfileVO();
		this.quotation = new QuotationVO();
	}

	public Boolean getAfirmeClient() {
		return afirmeClient;
	}

	public Boolean isAfirmeClient() {
		return afirmeClient;
	}

	public void setAfirmeClient(Boolean afirmeClient) {
		this.afirmeClient = afirmeClient;
	}

	public InsuredTypeVO getInsuredType() {
		return insuredType;
	}

	public void setInsuredType(InsuredTypeVO insuredType) {
		this.insuredType = insuredType;
	}

	public CoverageVO getCoverage() {
		return coverage;
	}

	public void setCoverage(CoverageVO coverage) {
		this.coverage = coverage;
	}

  public boolean isOiiApproved() {
    return oiiApproved;
  }

  public void setOiiApproved(boolean oiiApproved) {
    this.oiiApproved = oiiApproved;
  }

  public Double getAssuredAmount() {
    return assuredAmount;
  }

  public void setAssuredAmount(Double assuredAmount) {
    this.assuredAmount = assuredAmount;
  }

  public ProfileVO getProfile() {
    return profile;
  }

  public void setProfile(ProfileVO profile) {
    this.profile = profile;
  }
  
  public Integer getQuotationId() {
    return this.quotation == null ? null : this.quotation.getQuotationId();
  }

  public QuotationVO getQuotation() {
    return quotation;
  }

  public void setQuotation(QuotationVO quotation) {
    this.quotation = quotation;
  }

  public Integer getBusinessLineId() {
    return businessLineId;
  }

  public void setBusinessLineId(Integer businessLineId) {
    this.businessLineId = businessLineId;
  }

  public Integer getInsuredVersionId() {
    return insuredVersionId;
  }

  public void setInsuredVersionId(Integer insuredVersionId) {
    this.insuredVersionId = insuredVersionId;
  }

public String getPreferredBeneficiary() {
	return preferredBeneficiary;
}

public void setPreferredBeneficiary(String preferredBeneficiary) {
	this.preferredBeneficiary = preferredBeneficiary;
}
  
}
