package mx.com.afirme.vida.domain.movil.cotizador;

public class AddressDTO implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private Integer addressId = null;

	private String countryCode = null;

	private String stateCode = null;

	private String cityCode = null;

	private String streetAndNumber = null;

	private String colony = null;

	private String otherColony = null;

	private String postalCode = null;

	private String colonyCode = null;

	private String references = null;

	public String getColonyCode() {
		return colonyCode;
	}

	public void setColonyCode(String colonyCode) {
		this.colonyCode = colonyCode;
	}

	public Integer getAddressId() {
		return addressId;
	}

	public void setAddressId(Integer addressId) {
		this.addressId = addressId;
	}

	public String getStreetAndNumber() {
		return streetAndNumber;
	}

	public void setStreetAndNumber(String streetAndNumber) {
		this.streetAndNumber = streetAndNumber;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getColony() {
		return colony;
	}

	public void setColony(String colony) {
		this.colony = colony;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getOtherColony() {
		return otherColony;
	}

	public void setOtherColony(String otherColony) {
		this.otherColony = otherColony;
	}

	public boolean equals(Object object) {
		boolean isEqual = false;
		if (object instanceof AddressDTO) {
			AddressDTO addressDTO = (AddressDTO) object;
			isEqual = addressDTO.getAddressId().equals(this.addressId);
		} // End of if
		return isEqual;
	}

	public String getReferences() {
		return references;
	}

	public void setReferences(String references) {
		this.references = references;
	}

}
