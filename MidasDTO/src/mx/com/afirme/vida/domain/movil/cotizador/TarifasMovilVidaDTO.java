package mx.com.afirme.vida.domain.movil.cotizador;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
/**
 * Tctarifasmovilvida entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCTARIFAMOVILVIDA", schema="MIDAS")
public class TarifasMovilVidaDTO implements java.io.Serializable {
	private static final long serialVersionUID = 5727322721900628417L;
	// Fields
	private Long idTarifaMovilVida;
	private Short edadMinima;
	private Short edadMaxima;
	private BigDecimal sumaAsegurada;
	private BigDecimal tarifaBasica;
	private BigDecimal tarifaPlatino;
	private Date fechaRegistro;
	private Short bajalogica;
	
	
	@Id
	@Column(name = "IDTCTARIFAMOVILVIDA")
	@SequenceGenerator(name="TCTARIFAMOVILVIDA_ID_GENERATOR", sequenceName="MIDAS.TCTARIFAMOVILVIDA_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TCTARIFAMOVILVIDA_ID_GENERATOR")
	public Long getIdTarifaMovilVida() {
		return idTarifaMovilVida;
	}
	public void setIdTarifaMovilVida(Long idTarifaMovilVida) {
		this.idTarifaMovilVida = idTarifaMovilVida;
	}
	@Column(name = "EDADMIN")
	public Short getEdadMinima() {
		return edadMinima;
	}
	public void setEdadMinima(Short edadMinima) {
		this.edadMinima = edadMinima;
	}
	@Column(name = "EDADMAX")
	public Short getEdadMaxima() {
		return edadMaxima;
	}
	public void setEdadMaxima(Short edadMaxima) {
		this.edadMaxima = edadMaxima;
	}
	@Column(name = "SA")
	public BigDecimal getSumaAsegurada() {
		return sumaAsegurada;
	}
	public void setSumaAsegurada(BigDecimal sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
	@Column(name = "TARIFABASICA")
	public BigDecimal getTarifaBasica() {
		return tarifaBasica;
	}
	public void setTarifaBasica(BigDecimal tarifaBasica) {
		this.tarifaBasica = tarifaBasica;
	}
	@Column(name = "TARIFAPLATINO")
	public BigDecimal getTarifaPlatino() {
		return tarifaPlatino;
	}
	public void setTarifaPlatino(BigDecimal tarifaPlatino) {
		this.tarifaPlatino = tarifaPlatino;
	}
	
	@Column(name = "FECHAREGISTRO")
	@Temporal(TemporalType.DATE)
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	@Column(name = "BAJALOGICA", nullable = false, precision = 4, scale = 0)
	public Short getBajalogica() {
		return this.bajalogica;
	}

	public void setBajalogica(Short bajalogica) {
		this.bajalogica = bajalogica;
	}
}