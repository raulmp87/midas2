package mx.com.afirme.vida.domain.movil.cotizador;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import mx.com.afirme.midas2.util.StringUtil;

public class QuotationDTO implements java.io.Serializable {
  
  private static final long serialVersionUID = -9160556690985556041L;
  
  private List vehiclesList = null;

	private Boolean isFamilyInsurance = null;

	private String hasRCECoverage = null;

	private Integer quotationId = null;

	private Integer quotationNumber = null;

	private String notes = null;

	//private VehicleDTO vehicleDTO = null;

	private Integer methodOfPaymentId = null;

	private PaymentDTO paymentDTO = null;

	private Integer packageId = null;

	private List coverageList = null;

	private String description = null;

	private List results = null;

	private Double discount = null;

	private Double amountDiscount = null;

	private Double cededComission = null;

	private Double totalPremium = null;

	private Integer seycosCode = null;

	private String seycosCodeDescription = null;

	private Integer stage = null;

	private String policyNumber = null;

	private String iniRegistryDate = null;

	private String registryDate = null;

	private String afiliation = null;

	private String contractNumber = null;

	private Integer personId = null;

	private Integer clientId = null;
	
	private Integer addressId = null;

	private Boolean isNaturalPerson = null;

	private Double pctAnnualPayment = null;

	private Double pctSemesterPayment = null;

	private Double pctQuarterlyPayment = null;

	private Double pctMonthlyPayment = null;

	private Double pctSelected = null;

	private Double pctIVA = null;

	private Double pctComission = null;

	private Double netPremium = null;

	private String conduitPaymentId = null;

	private Double rightsAmount = null;

	private Double ivaAmount = null;

	private Double commisionAmount = null;

	private Double paymentAmount = null;

	private Double agentCommisionAmount = null;

	private String status = null;

	private Date forceDate = null;

	private Date forceEndDate = null;
	
	private Date emisionDate = null;

	private Double totalPremiums = null;

	private Double maxDiscount = null;

	private Integer businessLineId = null;
	
	private Integer sectionId = null;

	private String quotationOrigin = null;

	private String agentUserId = null;

	private Integer periodType = null;
	
	private String creatorUsername = null;
	
	private Integer quotationVersionId = null;
	
	private Integer policyRenovationNumber = null;
	
	private String clientName = null;
	
	private Integer policyType;
	
	private Double discountVolumenPercentage;

	private String saleExecutive = null;
	
	private Boolean allowSmallCashPayment = false;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Double getNetPremium() {
		return netPremium;
	}

	public void setNetPremium(Double netPremium) {
		this.netPremium = netPremium;
	}

	public String getAfiliation() {
		return afiliation;
	}

	public void setAfiliation(String afiliation) {
		this.afiliation = afiliation;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(String registryDate) {
		this.registryDate = registryDate;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public Integer getQuotationId() {
		return quotationId;
	}

	public void setQuotationId(Integer quotationId) {
		this.quotationId = quotationId;
	}

	public Integer getQuotationNumber() {
		return quotationNumber;
	}

	public void setQuotationNumber(Integer quotationNumber) {
		this.quotationNumber = quotationNumber;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}


	public Integer getMethodOfPaymentId() {
		return methodOfPaymentId;
	}

	public void setMethodOfPaymentId(Integer methodOfPaymentId) {
		this.methodOfPaymentId = methodOfPaymentId;
	}

	public PaymentDTO getPaymentDTO() {
		return paymentDTO;
	}

	public void setPaymentDTO(PaymentDTO paymentDTO) {
		this.paymentDTO = paymentDTO;
	}

	public Integer getPackageId() {
		return packageId;
	}

	public void setPackageId(Integer packageId) {
		this.packageId = packageId;
	}

	public List getCoverageList() {
		return coverageList;
	}

	public void setCoverageList(List coverageList) {
		this.coverageList = coverageList;
		Iterator coverageIterator = this.coverageList.iterator();
		while(coverageIterator.hasNext()) {
		  //CoverageDTO nextCoverageDTO = (CoverageDTO)coverageIterator.next();
		  //nextCoverageDTO.setQuotationDTO(this);
		} // End of while
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List getResults() {
		return results;
	}

	public void setResults(List results) {
		this.results = results;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getCededComission() {
		return cededComission;
	}

	public void setCededComission(Double cededComission) {
		this.cededComission = cededComission;
	}

	public Double getTotalPremium() {
		return totalPremium;
	}

	public void setTotalPremium(Double totalPremium) {
		this.totalPremium = totalPremium;
	}

	public Integer getSeycosCode() {
		return seycosCode;
	}

	public void setSeycosCode(Integer seycosCode) {
		this.seycosCode = seycosCode;
	}

	public String getSeycosCodeDescription() {
		return seycosCodeDescription;
	}

	public void setSeycosCodeDescription(String seycosCodeDescription) {
		this.seycosCodeDescription = seycosCodeDescription;
	}

	public Integer getStage() {
		return stage;
	}

	public void setStage(Integer stage) {
		this.stage = stage;
	}

	public String getIniRegistryDate() {
		return iniRegistryDate;
	}

	public void setIniRegistryDate(String iniRegistryDate) {
		this.iniRegistryDate = iniRegistryDate;
	}

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public Integer getClientId() {
		return clientId;
	}

	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}

	public Boolean getIsNaturalPerson() {
		return isNaturalPerson;
	}

	public void setIsNaturalPerson(Boolean isNaturalPerson) {
		this.isNaturalPerson = isNaturalPerson;
	}

	public Double getPctAnnualPayment() {
		return pctAnnualPayment;
	}

	public void setPctAnnualPayment(Double pctAnnualPayment) {
		this.pctAnnualPayment = pctAnnualPayment;
	}

	public Double getPctComission() {
		return pctComission;
	}

	public void setPctComission(Double pctComission) {
		this.pctComission = pctComission;
	}

	public Double getPctIVA() {
		return pctIVA;
	}

	public void setPctIVA(Double pctIVA) {
		this.pctIVA = pctIVA;
	}

	public Double getPctMonthlyPayment() {
		return pctMonthlyPayment;
	}

	public void setPctMonthlyPayment(Double pctMonthlyPayment) {
		this.pctMonthlyPayment = pctMonthlyPayment;
	}

	public Double getPctQuarterlyPayment() {
		return pctQuarterlyPayment;
	}

	public void setPctQuarterlyPayment(Double pctQuarterlyPayment) {
		this.pctQuarterlyPayment = pctQuarterlyPayment;
	}

	public Double getPctSemesterPayment() {
		return pctSemesterPayment;
	}

	public void setPctSemesterPayment(Double pctSemesterPayment) {
		this.pctSemesterPayment = pctSemesterPayment;
	}

	public String getConduitPaymentId() {
		return conduitPaymentId;
	}

	public void setConduitPaymentId(String conduitPaymentId) {
		this.conduitPaymentId = conduitPaymentId;
	}

	public Double getRightsAmount() {
		return rightsAmount;
	}

	public void setRightsAmount(Double rightsAmount) {
		this.rightsAmount = rightsAmount;
	}

	public Double getAgentCommisionAmount() {
		return agentCommisionAmount;
	}

	public void setAgentCommisionAmount(Double agentCommisionAmount) {
		this.agentCommisionAmount = agentCommisionAmount;
	}

	public Double getCommisionAmount() {
		return commisionAmount;
	}

	public void setCommisionAmount(Double commisionAmount) {
		this.commisionAmount = commisionAmount;
	}

	public Double getIvaAmount() {
		return ivaAmount;
	}

	public void setIvaAmount(Double ivaAmount) {
		this.ivaAmount = ivaAmount;
	}

	public Double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(Double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public Double getPctSelected() {
		return pctSelected;
	}

	public void setPctSelected(Double pctSelected) {
		this.pctSelected = pctSelected;
	}

	public Date getForceDate() {
		return forceDate;
	}

	public void setForceDate(Date forceDate) {
		this.forceDate = forceDate;
	}
	
	public String getEmisionDateString() {
    DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
    return this.getEmisionDate() != null ? 
        f.format(this.getEmisionDate()) : null;
  }

  public void setEmisionDateString(String stringDate) throws ParseException {
    DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
    Date date = !StringUtil.isEmpty(stringDate) ? f.parse(stringDate)
        : new Date();
    this.setEmisionDate(date);
  }

	public String getForceDateString() {
		DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		return this.getForceDate() != null ? f.format(this.getForceDate()) : f
				.format(new Date());
	}

	public void setForceDateString(String stringDate) throws ParseException {
		DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		Date date = !StringUtil.isEmpty(stringDate) ? f.parse(stringDate)
				: new Date();
		this.setForceDate(date);
	}

	public Double getAmountDiscount() {
		return amountDiscount;
	}

	public void setAmountDiscount(Double amountDiscount) {
		this.amountDiscount = amountDiscount;
	}

	public Double getTotalPremiums() {
		return totalPremiums;
	}

	public void setTotalPremiums(Double totalPremiums) {
		this.totalPremiums = totalPremiums;
	}

	public Double getMaxDiscount() {
		return maxDiscount;
	}

	public void setMaxDiscount(Double maxDiscount) {
		this.maxDiscount = maxDiscount;
	}

	public List getVehiclesList() {
		return vehiclesList;
	}

	public void setVehiclesList(List vehiclesList) {
		this.vehiclesList = vehiclesList;
	}

	public Boolean getIsFamilyInsurance() {
		return isFamilyInsurance;
	}

	public void setIsFamilyInsurance(Boolean isFamilyInsurance) {
		this.isFamilyInsurance = isFamilyInsurance;
	}

	public String getHasRCECoverage() {
		return hasRCECoverage;
	}

	public void setHasRCECoverage(String hasRCECoverage) {
		this.hasRCECoverage = hasRCECoverage;
	}

	public Integer getBusinessLineId() {
		return businessLineId;
	}

	public void setBusinessLineId(Integer businessLineId) {
		this.businessLineId = businessLineId;
	}

	public Integer getSectionId() {
    return sectionId;
  }

  public void setSectionId(Integer sectionId) {
    this.sectionId = sectionId;
  }

  public String getQuotationOrigin() {
		return quotationOrigin;
	}

	public void setQuotationOrigin(String quotationOrigin) {
		this.quotationOrigin = quotationOrigin;
	}

	public Date getForceEndDate() {
		return forceEndDate;
	}

	public void setForceEndDate(Date forceEndDate) {
		this.forceEndDate = forceEndDate;
	}

	public String getForceEndDateString() {
		DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		return this.getForceEndDate() != null ? f
				.format(this.getForceEndDate()) : f.format(new Date());
	}

	public void setForceEndDateString(String stringDate) throws ParseException {
		DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		Date date = !StringUtil.isEmpty(stringDate) ? f.parse(stringDate)
				: new Date();
		this.setForceEndDate(date);
	}

	public String getAgentUserId() {
		return agentUserId;
	}

	public void setAgentUserId(String agentUserId) {
		this.agentUserId = agentUserId;
	}
	
	public Integer getPeriodType() {
		return periodType;
	}

	public void setPeriodType(Integer periodType) {
		this.periodType = periodType;
	}

  public String getCreatorUsername() {
    return creatorUsername;
  }

  public void setCreatorUsername(String creatorUsername) {
    this.creatorUsername = creatorUsername;
  }
    
  public Date getEmisionDate() {
    return emisionDate;
  }

  public void setEmisionDate(Date emisionDate) {
    this.emisionDate = emisionDate;
  }    

  public Integer getAddressId() {
    return addressId;
  }

  public void setAddressId(Integer addressId) {
    this.addressId = addressId;
  }  
  
  public Integer getQuotationVersionId() {
    return quotationVersionId;
  }

  public void setQuotationVersionId(Integer quotationVersionId) {
    this.quotationVersionId = quotationVersionId;
  }

  public Integer getPolicyRenovationNumber() {
    return policyRenovationNumber;
  }

  public void setPolicyRenovationNumber(Integer policyRenovationNumber) {
    this.policyRenovationNumber = policyRenovationNumber;
  }

  public String getClientName() {
    return clientName;
  }

  public void setClientName(String clientName) {
    this.clientName = clientName;
  }
  
  public Integer getPolicyType() {
    return policyType;
  }

  public void setPolicyType(Integer policyType) {
    this.policyType = policyType;
  }

  public boolean equals(Object object) {
    boolean isEqual = (object == this);
    if (!isEqual && (object instanceof QuotationDTO)) {
      QuotationDTO quotationDTO = (QuotationDTO) object;
      isEqual = quotationDTO.getQuotationId().equals(this.quotationId);
    } // End of if
    return isEqual;
  }

public void setDiscountVolumenPercentage(Double discountVolumenPercentage) {
	this.discountVolumenPercentage = discountVolumenPercentage;
}

public Double getDiscountVolumenPercentage() {
	return discountVolumenPercentage;
}  


public String getSaleExecutive() {
	return saleExecutive;
}

public void setSaleExecutive(String saleExecutive) {
	this.saleExecutive = saleExecutive;
}

public Boolean getAllowSmallCashPayment() {
	return allowSmallCashPayment;
}

public void setAllowSmallCashPayment(Boolean allowSmallCashPayment) {
	this.allowSmallCashPayment = allowSmallCashPayment;
}  
   
}