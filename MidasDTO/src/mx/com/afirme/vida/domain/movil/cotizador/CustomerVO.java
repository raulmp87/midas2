package mx.com.afirme.vida.domain.movil.cotizador;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.util.StringUtil;


public class CustomerVO implements java.io.Serializable {
  
  private static final long serialVersionUID = -1296623975552493796L;

  private Integer id;
	
	private Boolean isAfirmeClient;
	
	private String name;
	
	private String fatherLastName;
	
	private String motherLastName;
	
	private String sexId;
	
	private Date birthDate;
	
	private Double weight;
	
	private Double height;

	private String maritalStatusId;
	
	private String nationality;
	
	private Integer birthStateId;
	
	private String curp;
	
	private String occupationId;
	
	private String occupationDetail;
	
	private RFC RFC = null;
	
	private String streetAndNumber;
	
	private String birthCityId;
	
	private String rfc;
	
	private String zipCode;
	
	private String stateId;
	
	private String cityId;
	
	private String colonyId;

	private String homePhoneNumber;
	
	private ProductVO product;
	
	public CustomerVO() {
	  this.RFC = new RFC();
	  this.product = new ProductVO();
	}
	
	public String getCompleteName() {
    StringBuilder personName = new StringBuilder();
    List<String> invalidValues = Arrays.asList(new String[] { ".", ",", "0" });
    boolean isEmpty = true;
    if (!StringUtil.isEmpty(this.name)
        && !invalidValues.contains(this.name)) {
      personName.append(this.name);
      isEmpty = false;
    } // End of if(this.firstName)
    if (!StringUtil.isEmpty(this.fatherLastName)
        && !invalidValues.contains(this.fatherLastName)) {
      if (!isEmpty) {
        personName.append(" ");
      } // End of if
      personName.append(this.fatherLastName);
      isEmpty = false;
    } // End of if(this.firstName)
    if (!StringUtil.isEmpty(this.motherLastName)
        && !invalidValues.contains(this.motherLastName)) {
      if (!isEmpty) {
        personName.append(" ");
      } // End of if
      personName.append(this.motherLastName);
      isEmpty = false;
    } // End of if(this.firstName)
    return String.valueOf(personName);
  }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFatherLastName() {
		return fatherLastName;
	}

	public void setFatherLastName(String fatherLastName) {
		this.fatherLastName = fatherLastName;
	}

	public String getMotherLastName() {
		return motherLastName;
	}

	public void setMotherLastName(String motherLastName) {
		this.motherLastName = motherLastName;
	}

	public String getSexId() {
    return sexId;
  }

  public void setSexId(String sexId) {
    this.sexId = sexId;
  }

  public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getIsAfirmeClient() {
		return isAfirmeClient;
	}

	public void setIsAfirmeClient(Boolean isAfirmeClient) {
		this.isAfirmeClient = isAfirmeClient;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Integer getAge() {
		if (this.birthDate == null) {
			return null;
		}
		Calendar birtday = Calendar.getInstance();
		Calendar now = Calendar.getInstance();
		
    birtday.setTime(this.birthDate);
		int age = now.get(Calendar.YEAR) - birtday.get(Calendar.YEAR);
		if ((birtday.get(Calendar.MONTH) > now.get(Calendar.MONTH)) ||
		    (birtday.get(Calendar.MONTH) == now.get(Calendar.MONTH) &&
		        birtday.get(Calendar.DAY_OF_MONTH) > now.get(Calendar.DAY_OF_MONTH))) {
			age = age - 1;
		} // End of if
		return Integer.valueOf(age);
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getHeight() {
		return height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public String getMaritalStatusId() {
    return maritalStatusId;
  }

  public void setMaritalStatusId(String maritalStatusId) {
    this.maritalStatusId = maritalStatusId;
  }

  public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public Integer getBirthStateId() {
		return birthStateId;
	}

	public void setBirthStateId(Integer birthStateId) {
		this.birthStateId = birthStateId;
	}

	public RFC getRFC() {
		return RFC;
	}

	public void setRFC(RFC RFC) {
		this.RFC = RFC;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}
	
	public String getOccupationId() {
    return occupationId;
  }

  public void setOccupationId(String occupationId) {
    this.occupationId = occupationId;
  }

  public String getOccupationDetail() {
    return occupationDetail;
  }

  public void setOccupationDetail(String occupationDetail) {
    this.occupationDetail = occupationDetail;
  }
  
  public String getStreetAndNumber() {
		return streetAndNumber;
	}

	public void setStreetAndNumber(String streetAndNumber) {
		this.streetAndNumber = streetAndNumber;
	}

	public String getBirthCityId() {
		return birthCityId;
	}

	public void setBirthCityId(String birthCityId) {
		this.birthCityId = birthCityId;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getColonyId() {
		return colonyId;
	}

	public void setColonyId(String colonyId) {
		this.colonyId = colonyId;
	}

	public String getHomePhoneNumber() {
		return homePhoneNumber;
	}

	public void setHomePhoneNumber(String homePhoneNumber) {
		this.homePhoneNumber = homePhoneNumber;
	}
	
	public ProductVO getProduct() {
    return product;
  }

  public void setProduct(ProductVO product) {
    this.product = product;
  }

  public boolean equals(Object object) {
	  return this == object;
	}
	
}
