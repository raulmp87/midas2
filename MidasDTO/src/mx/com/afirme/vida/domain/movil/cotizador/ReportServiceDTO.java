package mx.com.afirme.vida.domain.movil.cotizador;

import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JasperReport;

public class ReportServiceDTO  extends ReportDTO implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String jSessionId = null;

	private int applicationId = 0;

	private String reportType = null;

	private String reportName = null;

	private String reportFormat = null;

	private Map parameters = null;

	private String agentId = null;

	private Integer personId = null;

	private String idReport = null;

	private String initialDate = null;

	private String finalDate = null;

	private String policyHolderNumber = null;

	private String conductId = null;

	private String lineId = null;

	private String packageId = null;

	private String quotationId = null;

	private JasperReport report;
	
	private String duplicate = null;
	
	private String policyNumber = null;
	
	private String receiptNumber = null;

	private String receiptKey = null;
	
	private List reports = null;
	
	private String printReceipt = null;
	
	private String printAccountCharge = null;
	
	private String printApplication = null;
	
	private String origin = null;
	
	private String mainPageOnly = null;
	
	private QuotationParametersDTO quotationParameters = null;
	
	public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public String getPrintAccountCharge() {
    return printAccountCharge;
  }

  public void setPrintAccountCharge(String printAccountCharge) {
    this.printAccountCharge = printAccountCharge;
  }
  
  public String getPrintApplication() {
	return printApplication;
  }
  
  public void setPrintApplication(String printApplication) {
	this.printApplication = printApplication;
  }

  public String getPrintReceipt() {
    return printReceipt;
  }

  public void setPrintReceipt(String printReceipt) {
    this.printReceipt = printReceipt;
  }

  public List getReports() {
    return reports;
  }

  public void setReports(List reports) {
    this.reports = reports;
  }

  public String getReceiptKey() {
    return receiptKey;
  }

  public void setReceiptKey(String receiptKey) {
    this.receiptKey = receiptKey;
  }


  public String getReceiptNumber() {
    return receiptNumber;
  }

  public void setReceiptNumber(String receiptNumber) {
    this.receiptNumber = receiptNumber;
  }

  public String getJSessionId() {
		return jSessionId;
	}

	public void setJSessionId(String sessionId) {
		jSessionId = sessionId;
	}

	public int getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}

	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getReportFormat() {
		return reportFormat;
	}

	public void setReportFormat(String reportFormat) {
		this.reportFormat = reportFormat;
	}

	public Map getParameters() {
		return parameters;
	}

	public void setParameters(Map parameters) {
		this.parameters = parameters;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public String getIdReport() {
		return idReport;
	}

	public void setIdReport(String idReport) {
		this.idReport = idReport;
	}

	public String getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(String initialDate) {
		this.initialDate = initialDate;
	}

	public String getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(String finalDate) {
		this.finalDate = finalDate;
	}

	public String getPolicyHolderNumber() {
		return policyHolderNumber;
	}

	public void setPolicyHolderNumber(String policyHolderNumber) {
		this.policyHolderNumber = policyHolderNumber;
	}

	public String getConductId() {
		return conductId;
	}

	public void setConductId(String conductId) {
		this.conductId = conductId;
	}

	public String getLineId() {
		return lineId;
	}

	public void setLineId(String lineId) {
		this.lineId = lineId;
	}

	public String getPackageId() {
		return packageId;
	}

	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}

	public String getQuotationId() {
		return quotationId;
	}

	public void setQuotationId(String quotationId) {
		this.quotationId = quotationId;
	}

	public JasperReport getReport() {
		return report;
	}

	public void setReport(JasperReport report) {
		this.report = report;
	}

	public String getDuplicate() {
		return duplicate;
	}

	public void setDuplicate(String duplicate) {
		this.duplicate = duplicate;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

  public QuotationParametersDTO getQuotationParameters() {
    return quotationParameters;
  }

  public void setQuotationParameters(QuotationParametersDTO quotationParameters) {
    this.quotationParameters = quotationParameters;
  }

  public String getMainPageOnly() {
    return mainPageOnly;
  }

  public void setMainPageOnly(String mainPageOnly) {
    this.mainPageOnly = mainPageOnly;
  }

}
