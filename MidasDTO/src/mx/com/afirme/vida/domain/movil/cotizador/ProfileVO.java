package mx.com.afirme.vida.domain.movil.cotizador;

import java.io.Serializable;


public class ProfileVO implements Serializable {

  private static final long serialVersionUID = 7532870635037908971L;

  private boolean approved = false;
  
  private boolean profileComplete = false;
  
  private String rejectReason = null;
  
  public boolean isApproved() {
    return approved;
  }

  public void setApproved(boolean approved) {
    this.approved = approved;
  }

  public String getRejectReason() {
    return rejectReason;
  }

  public void setRejectReason(String rejectReason) {
    this.rejectReason = rejectReason;
  }

  public boolean isProfileComplete() {
    return profileComplete;
  }

  public void setProfileComplete(boolean profileComplete) {
    this.profileComplete = profileComplete;
  }

  public boolean equals(Object object) {
    return (this==object);
  }

}
