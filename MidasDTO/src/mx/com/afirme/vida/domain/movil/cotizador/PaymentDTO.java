package mx.com.afirme.vida.domain.movil.cotizador;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.vida.domain.movil.cotizador.AddressDTO;
public class PaymentDTO  implements java.io.Serializable {

  private static final long serialVersionUID = 4054395096402805006L;

  private Integer paymentConduitId;

	private String issuerId;

	private String cardHolder;

	private String cardType;	

	private String cardNumber;

	private String securityCode;

	private String dueDate;

	private Double amount;

	private String clabe;

	private String accountNumber;

	private String authorization;

	private String creditCardType;

	private Integer paymentDay;
	
	//DATOS TITULAR TARJETA / CUENTA
	
	private Integer paymentConduitType;
	
  private AddressDTO addressDTO;	

  private String phone;
	
	private String email;
	
	private String comments;
  
  private String personType = "PF";

  private String rfc;
	// AGREGADOS DESDE EL FORM
	private String actualMonth;

	private String actualYear;

	private String creditCardMonth;

	private String creditCardYear;

	private Integer registeredPaymentMethod;

	// AGREGADOS DE CHARGEVO
	private Integer paymentAttempt;

	private String errorCode;

	private String errorDesc;

	private String authorizationNumber;

	// Agregados para confimacion de PROSA

	private String prosaAffiliation;

	private String prosaAuthorization;

	private String prosaErrorDesc;

	private String prosaPaymentDate;

	private String prosaErrorNum;

	private String prosaTerminal;

	// complementario para registrar el pago de tc en seycos
	private String receiptNumber;
	
	private String receiptDigit;

	// variables para domiciliacion.	

  private List paymentScheduled;
  
  private boolean success;
  
  private int ibsPaymentOrigin;
  
  private String policyReference;
  
  private String promotionCode;
  
  public PaymentDTO() {
    this.paymentScheduled = new ArrayList();
    this.addressDTO = new AddressDTO();
  }
  
  public AddressDTO getAddressDTO() {
    return addressDTO;
  }

  public void setAddressDTO(AddressDTO addressDTO) {
    this.addressDTO = addressDTO;
  }

  public String getPhone() {
    return phone;
  }

  public String getPersonType() {
    return personType;
  }

  public void setPersonType(String personType) {
    this.personType = personType;
  }
  
  public void setPhone(String phone) {
    this.phone = phone;
  }
 
  public boolean isACreditCardPayment(){
    return  paymentConduitType != null && 
      paymentConduitType.intValue() == SeycosPortalConstants.PAYMENT_TYPE_CC ? 
        true : false;
  }
  
  public boolean isAnAccountPayment(){
    return  paymentConduitType != null && 
     paymentConduitType.intValue() == SeycosPortalConstants.PAYMENT_TYPE_ACCOUNT ? 
        true : false;
  }
  
  public boolean isACashPayment(){
    return  paymentConduitType != null && 
      paymentConduitType.intValue() == SeycosPortalConstants.PAYMENT_TYPE_CASH ? 
        true : false;
  }
  
  public boolean isAnAfirmePayment(){
    return  paymentConduitType != null && 
      paymentConduitType.intValue() == SeycosPortalConstants.PAYMENT_TYPE_AFIRME ? 
        true : false;
  }
  
  public void setAsCreditCardPayment(){
    this.paymentConduitType = new Integer(SeycosPortalConstants.PAYMENT_TYPE_CC);
  }
  
  public void setAsCashPayment(){
    this.paymentConduitType = new Integer(
        SeycosPortalConstants.PAYMENT_TYPE_CASH);
  }
  
  public void setAsAccountPayment(){
    this.paymentConduitType = new Integer(
        SeycosPortalConstants.PAYMENT_TYPE_ACCOUNT);
  }
  
  public void setAsAfirmePayment(){
    this.paymentConduitType = new Integer(
        SeycosPortalConstants.PAYMENT_TYPE_AFIRME);
  }
  
  
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }
  
	public boolean isSuccess() {
    return success;
  }

  public void setSuccess(boolean success) {
    this.success = success;
  }
 
	public String getReceiptDigit() {
    return receiptDigit;
  }

  public void setReceiptDigit(String receiptDigit) {
    this.receiptDigit = receiptDigit;
  }

	public void setPaymentScheduled(List paymentScheduled) {
		this.paymentScheduled.addAll(paymentScheduled);
	}

	public void cleanPaymentScheduledList() {
		this.paymentScheduled.removeAll(this.paymentScheduled);
	}

	public Integer getPaymentDay() {
		return paymentDay;
	}

	public void setPaymentDay(Integer paymentDay) {
		this.paymentDay = paymentDay;
	}

	public String getCreditCardType() {
		return creditCardType;
	}

	public void setCreditCardType(String creditCardType) {
		this.creditCardType = creditCardType;
	}

	public String getAuthorization() {
		return authorization;
	}

	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}

	public Integer getPaymentConduitId() {
		return paymentConduitId;
	}

	public void setPaymentConduitId(Integer paymentConduitId) {
		this.paymentConduitId = paymentConduitId;
	}

	public String getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(String issuerId) {
		this.issuerId = issuerId;
	}

	public String getCardHolder() {
		return cardHolder;
	}

	public void setCardHolder(String cardHolder) {
		this.cardHolder = cardHolder;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getClabe() {
		return clabe;
	}

	public void setClabe(String clabe) {
		this.clabe = clabe;
	}

	public String getActualMonth() {
		return actualMonth;
	}

	public void setActualMonth(String actualMonth) {
		this.actualMonth = actualMonth;
	}

	public String getActualYear() {
		return actualYear;
	}

	public void setActualYear(String actualYear) {
		this.actualYear = actualYear;
	}

	public String getCreditCardMonth() {
		return creditCardMonth;
	}

	public void setCreditCardMonth(String creditCardMonth) {
		this.creditCardMonth = creditCardMonth;
	}

	public String getCreditCardYear() {
		return creditCardYear;
	}

	public void setCreditCardYear(String creditCardYear) {
		this.creditCardYear = creditCardYear;
	}

	public Integer getRegisteredPaymentMethod() {
		return registeredPaymentMethod;
	}

	public void setRegisteredPaymentMethod(Integer registeredPaymentMethod) {
		this.registeredPaymentMethod = registeredPaymentMethod;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDesc() {
		return errorDesc;
	}

	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}

	public Integer getPaymentAttempt() {
		return paymentAttempt;
	}

	public void setPaymentAttempt(Integer paymentAttempt) {
		this.paymentAttempt = paymentAttempt;
	}

	public String getAuthorizationNumber() {
		return authorizationNumber;
	}

	public void setAuthorizationNumber(String authorizationNumber) {
		this.authorizationNumber = authorizationNumber;
	}

	public String getProsaAffiliation() {
		return prosaAffiliation;
	}

	public void setProsaAffiliation(String prosaAffiliation) {
		this.prosaAffiliation = prosaAffiliation;
	}

	public String getProsaAuthorization() {
		return prosaAuthorization;
	}

	public void setProsaAuthorization(String prosaAuthorization) {
		this.prosaAuthorization = prosaAuthorization;
	}

	public String getProsaErrorDesc() {
		return prosaErrorDesc;
	}

	public void setProsaErrorDesc(String prosaErrorDesc) {
		this.prosaErrorDesc = prosaErrorDesc;
	}

	public String getProsaErrorNum() {
		return prosaErrorNum;
	}

	public void setProsaErrorNum(String prosaErrorNum) {
		this.prosaErrorNum = prosaErrorNum;
	}

	public String getProsaPaymentDate() {
		return prosaPaymentDate;
	}

	public void setProsaPaymentDate(String prosaPaymentDate) {
		this.prosaPaymentDate = prosaPaymentDate;
	}

	public String getProsaTerminal() {
		return prosaTerminal;
	}

	public void setProsaTerminal(String prosaTerminal) {
		this.prosaTerminal = prosaTerminal;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	
	public Integer getPaymentConduitType() {
	  return paymentConduitType;
	}

	public void setPaymentConduitType(Integer paymentConduitType) {
	  this.paymentConduitType = paymentConduitType;
	}

	public String getRfc() {
    return rfc;
  }

  public void setRfc(String rfc) {
    this.rfc = rfc;
  }
  
  public String getCardAccountNumber() {
    return this.getCardNumber();
  }

  public void setCardAccountNumber(String cardAccountNumber) {
    this.setCardNumber(cardAccountNumber);
  }
  
  public boolean equals(Object object) {
    return (this == object);
  }

  /**
   * Gets is selected movement type is valid for selected bank
   * @return
   */
  public boolean isAValidAccountType(){
    boolean isValid = true;    
    if(this.getPaymentConduitType().intValue() == 
        SeycosPortalConstants.PAYMENT_TYPE_ACCOUNT || 
          this.getPaymentConduitType().intValue() == 
              SeycosPortalConstants.PAYMENT_TYPE_AFIRME){
      if(!this.getIssuerId().trim().equals(SeycosPortalConstants.
          PAYMENT_BANK_AFIRME) && this.getCardType().equals(
              SeycosPortalConstants.PAYMENT_ACCOUNT)){
        isValid = false;
      }
    }
    return isValid;
  }
  
  /**
   * Gets correct payment conduit type depending on the selectec bank and 
   * movement type
   * @return Afirme Conduit Type if bank is Afirme and movement is debit or 
   * account. 
   */
  public int getCorrectPaymentConduitType(){
    //reset payment conduit type
    if(this.getPaymentConduitType().intValue() == 
      SeycosPortalConstants.PAYMENT_TYPE_AFIRME){
      this.setPaymentConduitType(Integer.valueOf(
          SeycosPortalConstants.PAYMENT_TYPE_ACCOUNT));
    }
    
    //check if Afirme conduit type is going to be used    
    if(this.getPaymentConduitType().intValue() == SeycosPortalConstants.
        PAYMENT_TYPE_ACCOUNT && this.getIssuerId().trim().equals(
        SeycosPortalConstants.PAYMENT_BANK_AFIRME) && 
        (this.getCardType().equals(SeycosPortalConstants.PAYMENT_ACCOUNT) || 
            this.getCardType().equals(SeycosPortalConstants.PAYMENT_DEBIT))){
      return SeycosPortalConstants.PAYMENT_TYPE_AFIRME;
    }else{
      return this.getPaymentConduitType().intValue();
    }    
  }

  public int getIbsPaymentOrigin() {
    return ibsPaymentOrigin;
  }
  
  public void setProductAsLifeInsurance(){
    this.setIbsPaymentOrigin(2);
  }
  
  public void setProductAsHouseInsurance(){
    this.setIbsPaymentOrigin(6);
  }
  
  public void setProductAsAutoInsurance(){
    this.setIbsPaymentOrigin(1);
  }
  
  /**
   * 1 � AUTO
   * 2 � VIDA FAMILIAR          
   * 3 � VIDA MODULO
   * 4 � DA�OS
   * 5 � OTROS
   * 6 - CASA
   * @param ibsPaymentOrigin
   */
  public void setIbsPaymentOrigin(int ibsPaymentOrigin) {
    this.ibsPaymentOrigin = ibsPaymentOrigin;
  }

  public String getPolicyReference() {
    return policyReference;
  }

  public void setPolicyReference(String policyReference) {
    this.policyReference = policyReference;
  }

public String getPromotionCode() {
	return promotionCode;
}

public void setPromotionCode(String promotionCode) {
	this.promotionCode = promotionCode;
}
  
  
  
  
}