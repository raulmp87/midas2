package mx.com.afirme.vida.domain.movil.cotizador;


public class SumaAseguradaDTO implements java.io.Serializable {
	
	private Long idSumaAsegurada;
	private String sumaAsegurada;
	
	public Long getIdSumaAsegurada() {
		return idSumaAsegurada;
	}
	public void setIdSumaAsegurada(Long idSumaAsegurada) {
		this.idSumaAsegurada = idSumaAsegurada;
	}
	public String getSumaAsegurada() {
		return sumaAsegurada;
	}
	public void setSumaAsegurada(String sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
	

	
}
