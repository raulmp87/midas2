package mx.com.afirme.vida.domain.movil.cotizador;


public class QuotationVO  implements java.io.Serializable {

  private static final long serialVersionUID = -5270848246704786450L;

  private Integer quotationId = null;
  
  private Integer quotationNumber = null;

  private String userId = null;
  
  private String homePhoneNumber = null;
  
  private String businessPhone = null;
  
  private String cellPhone = null;
  
  private String faxNumber = null;
  
  private String mail = null;
  
  private Integer methodOfPaymentId = null;
  
  private Integer paymentTypeId = null;
  
  private Double amountNetPremium = null;
  
  private Double amountDiscount = null;
  
  private Double amountIva = null;
  
  private Double amountRights = null;
  
  private Double amountPaymentSurcharge = null;
  
  private Double amountPaymentSurchargePct = null;
  
  private Double amountTotalPremium = null;
  
  private Double partialTotalPremium = null;

  private AddressDTO address = null;
  
  private String quotationStatus = null;
  
  private Integer agentId = null;
  
  private Integer productId = null;
  
  public Double getPartialTotalPremium() {
    return partialTotalPremium;
  }

  public void setPartialTotalPremium(Double partialTotalPremium) {
    this.partialTotalPremium = partialTotalPremium;
  }
  
  public Double getAmountPaymentSurchargePct() {
    return amountPaymentSurchargePct;
  }

  public void setAmountPaymentSurchargePct(Double amountPaymentSurchargePct) {
    this.amountPaymentSurchargePct = amountPaymentSurchargePct;
  }

  public QuotationVO() {
    this.address = new AddressDTO();
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String string) {
    userId = string;
  }

  public Integer getQuotationId() {
    return quotationId;
  }

  public void setQuotationId(Integer quotationId) {
    this.quotationId = quotationId;
  }
  
  public Integer getQuotationNumber() {
    return quotationNumber;
  }

  public void setQuotationNumber(Integer quotationNumber) {
    this.quotationNumber = quotationNumber;
  }
  
  public String getHomePhoneNumber() {
    return homePhoneNumber;
  }

  public void setHomePhoneNumber(String homePhoneNumber) {
    this.homePhoneNumber = homePhoneNumber;
  }

  public String getBusinessPhone() {
    return businessPhone;
  }

  public void setBusinessPhone(String businessPhone) {
    this.businessPhone = businessPhone;
  }

  public String getFaxNumber() {
    return faxNumber;
  }

  public void setFaxNumber(String faxNumber) {
    this.faxNumber = faxNumber;
  }

  public String getMail() {
    return mail;
  }

  public void setMail(String mail) {
    this.mail = mail;
  }

  public Integer getMethodOfPaymentId() {
    return methodOfPaymentId;
  }

  public void setMethodOfPaymentId(Integer methodOfPaymentId) {
    this.methodOfPaymentId = methodOfPaymentId;
  }

  public Integer getPaymentTypeId() {
    return paymentTypeId;
  }

  public void setPaymentTypeId(Integer paymentTypeId) {
    this.paymentTypeId = paymentTypeId;
  }

  public Double getAmountDiscount() {
    return amountDiscount;
  }

  public void setAmountDiscount(Double amountDiscount) {
    this.amountDiscount = amountDiscount;
  }

  public Double getAmountNetPremium() {
    return amountNetPremium;
  }

  public void setAmountNetPremium(Double amountNetPremium) {
    this.amountNetPremium = amountNetPremium;
  }

  public Double getAmountIva() {
    return amountIva;
  }

  public void setAmountIva(Double amountIva) {
    this.amountIva = amountIva;
  }

  public Double getAmountRights() {
    return amountRights;
  }

  public void setAmountRights(Double amountRights) {
    this.amountRights = amountRights;
  }

  public Double getAmountPaymentSurcharge() {
    return amountPaymentSurcharge;
  }

  public void setAmountPaymentSurcharge(Double amountPaymentSurcharge) {
    this.amountPaymentSurcharge = amountPaymentSurcharge;
  }

  public Double getAmountTotalPremium() {
    return amountTotalPremium;
  }

  public void setAmountTotalPremium(Double amountTotalPremium) {
    this.amountTotalPremium = amountTotalPremium;
  }

  public AddressDTO getAddress() {
    return address;
  }

  public void setAddress(AddressDTO address) {
    this.address = address;
  }
  
  public String getCellPhone() {
    return cellPhone;
  }

  public void setCellPhone(String cellPhone) {
    this.cellPhone = cellPhone;
  }   

  public String getQuotationStatus() {
    return quotationStatus;
  }

  public void setQuotationStatus(String quotationStatus) {
    this.quotationStatus = quotationStatus;
  }
  
  public Integer getAgentId() {
    return agentId;
  }

  public void setAgentId(Integer agentId) {
    this.agentId = agentId;
  }

  public Integer getProductId() {
    return productId;
  }

  public void setProductId(Integer productId) {
    this.productId = productId;
  }

  public boolean equals(Object object) {
    boolean equal = (this == object);
    if (!equal && (object instanceof QuotationVO)) {
      QuotationVO quotationVO = (QuotationVO) object;
      equal = quotationVO.getQuotationId().equals(this.quotationId);
    } // End of if
    return equal;
  }

}
