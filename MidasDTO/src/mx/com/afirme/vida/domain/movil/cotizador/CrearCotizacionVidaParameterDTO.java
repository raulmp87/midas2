package mx.com.afirme.vida.domain.movil.cotizador;

import java.util.Date;
import java.util.List;

public class CrearCotizacionVidaParameterDTO implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2211187571993375127L;
	private Integer idCotizacion;
	private Long idToCotizacionMovil;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String idEstado;
	private String idMunicipio;
	private String calleNumero;
	private String codigoPostal;
	private Integer idUsrCodPos;
	private String colonia;
	private String telefOficina;
	private String teleFax;
	private String telCasa;
	private String mail;
	private String curp;
	private String cveSexo;
	private Date FNacimiento;
	private String cveEdoCivil;
	private String siglasRFC;
	private String fRFC;
	private String homoclaveRFC;
	private String cveTAsegura;
	private Integer idCobertura;
	private String cobertura;
	private Double sA_amparada;
	private Integer idLugarNac;
	private Integer idDomicilio;
	private Double estatura;
	private Double peso;
	private String idGiroOcup;
	private String detOcupa;
	private Integer clienteAfirme;
	private Integer idProducto;
	private String preferredBeneficiary;
	private Long idSumaAsegurada;
	private Double sumaAsegurada;
	private String os;
	private String uuid;
	private String claveagente;
	private String telefono;
	private String email;
	private List<MensajesCotizacion> mensajesList;
	private String tipoCotizacion;
	private Boolean contratar = true;
	private Boolean procesada = false;
	private Integer idFormaPago;
	private String formaPago;


	public static final String TIPO_COTIZACION_PORTAL = "TCP";
	public static final String TIPO_COTIZACION_MOVIL = "TCM";
	public static final String PORTALPDF = "Cotizacion Portal Vida.pdf";

	public Integer getIdCotizacion() {
		return idCotizacion;
	}

	public void setIdCotizacion(Integer idCotizacion) {
		this.idCotizacion = idCotizacion;
	}

	public Long getIdToCotizacionMovil() {
		return idToCotizacionMovil;
	}

	public void setIdToCotizacionMovil(Long idToCotizacionMovil) {
		this.idToCotizacionMovil = idToCotizacionMovil;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}

	public String getIdMunicipio() {
		return idMunicipio;
	}

	public void setIdMunicipio(String idMunicipio) {
		this.idMunicipio = idMunicipio;
	}

	public String getCalleNumero() {
		return calleNumero;
	}

	public void setCalleNumero(String calleNumero) {
		this.calleNumero = calleNumero;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public Integer getIdUsrCodPos() {
		return idUsrCodPos;
	}

	public void setIdUsrCodPos(Integer idUsrCodPos) {
		this.idUsrCodPos = idUsrCodPos;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getTelefOficina() {
		return telefOficina;
	}

	public void setTelefOficina(String telefOficina) {
		this.telefOficina = telefOficina;
	}

	public String getTeleFax() {
		return teleFax;
	}

	public void setTeleFax(String teleFax) {
		this.teleFax = teleFax;
	}

	public String getTelCasa() {
		return telCasa;
	}

	public void setTelCasa(String telCasa) {
		this.telCasa = telCasa;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getClaveagente() {
		return claveagente;
	}

	public void setClaveagente(String claveagente) {
		this.claveagente = claveagente;
	}

	public Double getSumaAsegurada() {
		return sumaAsegurada;
	}

	public void setSumaAsegurada(Double sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public String getCveSexo() {
		return cveSexo;
	}

	public void setCveSexo(String cveSexo) {
		this.cveSexo = cveSexo;
	}

	public Date getFNacimiento() {
		return FNacimiento;
	}

	public void setFNacimiento(Date fNacimiento) {
		FNacimiento = fNacimiento;
	}

	public String getCveEdoCivil() {
		return cveEdoCivil;
	}

	public void setCveEdoCivil(String cveEdoCivil) {
		this.cveEdoCivil = cveEdoCivil;
	}

	public String getSiglasRFC() {
		return siglasRFC;
	}

	public void setSiglasRFC(String siglasRFC) {
		this.siglasRFC = siglasRFC;
	}

	public String getfRFC() {
		return fRFC;
	}

	public void setfRFC(String fRFC) {
		this.fRFC = fRFC;
	}

	public String getHomoclaveRFC() {
		return homoclaveRFC;
	}

	public void setHomoclaveRFC(String homoclaveRFC) {
		this.homoclaveRFC = homoclaveRFC;
	}

	public String getCveTAsegura() {
		return cveTAsegura;
	}

	public void setCveTAsegura(String cveTAsegura) {
		this.cveTAsegura = cveTAsegura;
	}

	public Integer getIdCobertura() {
		return idCobertura;
	}

	public void setIdCobertura(Integer idCobertura) {
		this.idCobertura = idCobertura;
	}

	public String getCobertura() {
		return cobertura;
	}

	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}

	public Double getsA_amparada() {
		return sA_amparada;
	}

	public void setsA_amparada(Double sA_amparada) {
		this.sA_amparada = sA_amparada;
	}

	public Integer getIdLugarNac() {
		return idLugarNac;
	}

	public void setIdLugarNac(Integer idLugarNac) {
		this.idLugarNac = idLugarNac;
	}

	public Integer getIdDomicilio() {
		return idDomicilio;
	}

	public void setIdDomicilio(Integer idDomicilio) {
		this.idDomicilio = idDomicilio;
	}

	public Double getEstatura() {
		return estatura;
	}

	public void setEstatura(Double estatura) {
		this.estatura = estatura;
	}

	public Double getPeso() {
		return peso;
	}

	public void setPeso(Double peso) {
		this.peso = peso;
	}

	public String getIdGiroOcup() {
		return idGiroOcup;
	}

	public void setIdGiroOcup(String idGiroOcup) {
		this.idGiroOcup = idGiroOcup;
	}

	public String getDetOcupa() {
		return detOcupa;
	}

	public void setDetOcupa(String detOcupa) {
		this.detOcupa = detOcupa;
	}

	public Integer getClienteAfirme() {
		return clienteAfirme;
	}

	public void setClienteAfirme(Integer clienteAfirme) {
		this.clienteAfirme = clienteAfirme;
	}

	public Integer getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}

	public String getPreferredBeneficiary() {
		return preferredBeneficiary;
	}

	public void setPreferredBeneficiary(String preferredBeneficiary) {
		this.preferredBeneficiary = preferredBeneficiary;
	}

	public List<MensajesCotizacion> getMensajesList() {
		return mensajesList;
	}

	public void setMensajesList(List<MensajesCotizacion> mensajesList) {
		this.mensajesList = mensajesList;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getTipoCotizacion() {
		return tipoCotizacion;
	}

	public void setTipoCotizacion(String tipoCotizacion) {
		this.tipoCotizacion = tipoCotizacion;
	}

	public Boolean getContratar() {
		return contratar;
	}

	public void setContratar(Boolean contratar) {
		this.contratar = contratar;
	}

	public Long getIdSumaAsegurada() {
		return idSumaAsegurada;
	}

	public void setIdSumaAsegurada(Long idSumaAsegurada) {
		this.idSumaAsegurada = idSumaAsegurada;
	}

	public Boolean getProcesada() {
		return procesada;
	}

	public void setProcesada(Boolean procesada) {
		this.procesada = procesada;
	}

	public Integer getIdFormaPago() {
		return idFormaPago;
	}

	public void setIdFormaPago(Integer idFormaPago) {
		this.idFormaPago = idFormaPago;
	}

	public String getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}	
}