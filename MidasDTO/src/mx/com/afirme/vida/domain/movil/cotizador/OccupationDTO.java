package mx.com.afirme.vida.domain.movil.cotizador;


public class OccupationDTO implements java.io.Serializable {
	
	private Long idGiroOcupacion;
	private String giroOcupacion;
	
	public Long getIdGiroOcupacion() {
		return idGiroOcupacion;
	}
	
	public void setIdGiroOcupacion(Long idGiroOcupacion) {
		this.idGiroOcupacion = idGiroOcupacion;
	}

	public String getGiroOcupacion() {
		return giroOcupacion;
	}

	public void setGiroOcupacion(String giroOcupacion) {
		this.giroOcupacion = giroOcupacion;
	}
	
	
	
}
