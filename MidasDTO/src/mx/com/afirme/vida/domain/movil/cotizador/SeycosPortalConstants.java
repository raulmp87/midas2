package mx.com.afirme.vida.domain.movil.cotizador;

import java.math.BigDecimal;

public interface SeycosPortalConstants {

	/*
	 * Product Codes
	 */
	public int INDIVIDUAL_VEHICLES_PRODUCT_ID = 1;

	public int LIFE_INSURANCE_PRODUCT_ID = 1405;

	public int HOUSE_PRODUCT_ID = 1434;

	public int IMMIGRANT_LIFE_INSURANCE_PRODUCT_ID = 1455;

	/**
	 * General Application Constants
	 */
	public int SEYCOS_PORTAL_APPLICATION_ID = 4;

	public int MIDAS_APPLICATION_ID = 5;

	public String APPLICATION_PATH = "application.path";

	public String USER = "seycosPortalUser";

	public String ISSUE_DATE_FROM = "issueDateFrom";

	public String ISSUE_DATE_TO = "issueDateTo";

	public String PRODUCT_CODE = "productCode";

	public String INSURANCE_SERVICES_ENDPOINT = "ed.insurance.services.endpoint";

	public static final String WFACTURA_SERVICE_ENDPOINT = "ed.wfactura.services.endpoint";

	public String WS_CURP_VALIDATOR_END_POINT = "helper.ws.curp.validationEndPoint";

	public String EIBS_SERVICES_ENDPOINT = "ed.eibs.services.endpoint";

	public String EIBS_ENVIRONMENT = "ed.eibs.environment";

	public String NATIONAL_UNITY_SERVICES_ENDPOINT = 
		"ed.national.unity.services.endpoint";

	public String NATIONAL_UNITY_WEBSERVICES_ENDPOINT = 
		"ed.national.unity.webservices.endpoint";

	public double IVA = 0.16;
	/**
	 * EJB Constants
	 */
	public String AGENT_EJB = "ejb/SeycosAgent";

	public String LIFE_INSURANCE_SERVICE_EJB = "ejb/LifeInsuranceService";

	public String QUOTATION_INBOX_EJB = "ejb/QuotationInbox";

	public static final String BANDEJA_COTIZACION_MIDAS_EJB = "ejb/BandejaCotizacionService";
	public static final String BANDEJA_COTIZACION_MIDAS_DAO = "dao/BandejaCotizacionDAO";
	public static final String ASIGNACION_AGENTES_TIPOPOLIZA_DAO = "dao/AsignacionAgentesTipoPolizaDAO";

	public String ENDORSEMENT_INBOX_EJB = "ejb/EndorsementInbox";

	public String POLICY_INBOX_EJB = "ejb/PolicyInbox";

	public String CABIN_POLICY_INBOX_EJB = "ejb/CabinPolicyInbox";

	public String SUBSEQUENT_RECEIPTS_INBOX_EJB = "ejb/SubsequentReceiptsInbox";

	public String CLAIM_INBOX_EJB = "ejb/ClaimInbox";

	public String CABIN_MONITOR_INBOX_EJB = "ejb/CabinMonitorInbox";

	public String MENU_EJB = "ejb/SeycosMenu";

	public String ADMINISTRATION_EJB = "ejb/SeycosAdministration";

	public String DOWNLOADS_EJB = "ejb/Downloads";

	public String PROPERTY_EJB = "ejb/SeycosProperty";

	public String ADDRESS_EJB = "ejb/SeycosAddress";

	public String SEYCOS_PORTAL_USER_EJB = "ejb/SeycosUser";

	public String POLICY_HOLDER_EJB = "ejb/PolicyHolder";

	public String SIMPLE_CATALOG_EJB = "ejb/SimpleCatalog";

	public String BANK_EJB = "ejb/Bank";

	public String CITY_EJB = "ejb/City";

	public String STATE_EJB = "ejb/State";

	public String COLONY_EJB = "ejb/Colony";

	public String BRANCHLEP_EJB = "ejb/BranchLEP";  

	public String SUB_BRANCHLEP_EJB = "ejb/SubBranchLEP";

	public String MAKE_EJB = "ejb/Make"; 

	public String MODEL_EJB = "ejb/Model";

	public String YEAR_EJB = "ejb/Year";

	public String COVERAGE_EJB = "ejb/Coverage";

	public String PACKAGE_EJB = "ejb/Package";

	public String PRODUCT_EJB = "ejb/Product";

	public String ACTIVITYNP_EJB = "ejb/ActivityNP";

	public String REPORT_EJB = "ejb/SeycosReport";

	public String QUOTATION_EJB = "ejb/Quotation";

	public String CONDUCT_EJB = "ejb/Conduct";

	public String WELCOME_INBOX_EJB = "ejb/WelcomeInbox";

	public String PAYMENT_EJB = "ejb/Payment";

	public String BUSINESS_LINE_EJB ="ejb/BusinessLine";

	public String SUPERVISORIA_EJB = "ejb/Supervisoria";

	public String MASSIVE_EMISSION_EJB = "ejb/MassiveEmission";

	public String VALIDATION_IMPLMEMENTATION_EJB = "ejb/ValidationImplementation";

	public String PROMOTION_EJB =
		"ejb/com/afirme/eibs/payments/service/AfirmePaymentsServiceHome";

	public String PROMOTION_EJB_HOME = "promotionEJBHome";

	public String ENDORSEMENT_EJB = "ejb/Endorsement";

	public String ENDORSEMENT_FLEETS_EJB = "ejb/endorsement/EndorsementFleets";
	public String SINISTER_EJB = "ejb/Sinister";  

	public String ASSOCIATE_SERVICE_EJB = "ejb/AssociateService";  

	public String THIRD_AFFECTED_EJB = "ejb/ThridAffected";  

	public String USE_CODE_EJB = "ejb/UseCode";

	public String PAY_REPORT_EJB = "ejb/PayReport";

	public String ENDORSEMENT_TYPE_EJB = "ejb/EndorsementType";

	public String COVERAGE_CONFIGURATION_EJB = "ejb/CoverageConfiguration";

	public String HOUSE_REPORT_EJB = "ejb/HouseReport";

	public String AVAILABLE_REPORTS_EJB = "ejb/AvailableReports";

	public String CERTIFIED_SERVICE_EJB = "ejb/CertifiedService";

	public String CLIENT_CURP_SERVICE_EJB = "ejb/ClientCurpService";

	public String MODEL_PACKAGE_RANGE_EJB = "ejb/ModelPackageRange";

	public String EMPLOYEE_RELATIONSHIP_EJB = "ejb/EmployeeRelationship";

	public String FLEET_COMMENT_EJB = "ejb/FleetComment";

	public String FLEET_QUOTATION_EJB = "ejb/FleetQuotation";  

	public String FLEET_SUBSECTIONS_EJB = "ejb/SubSectionsFleets";

	public String FLEET_SOLICITUDE_AUTHORIZATION_EJB = "ejb/SolicitudeAuthorization";

	public String FLEET_QUOTATION_SEYCOS_EJB = "ejb/FleetQuotationSeycos";

	public String MASSIVE_UPLOAD_FLEET_EJB = "ejb/MassiveUploadFleets";
	public String SINISTER_CAUSE_EJB = "ejb/SinisterCause";

	public String AUTHORIZATION_EJB = "ejb/Authorization";

	public String AUTHORIZATION_INBOX_EJB = "ejb/AuthorizationInbox";

	public String MASSIVE_USER_EJB = "ejb/MassiveUser";

	public String DEDUCTIBLE_CONFIGURATION_EJB = "ejb/DeductibleConfiguration";

	public String DEDUCTIBLE_CATALOG_EJB = "ejb/DeductibleCatalog";

	public String SERVICE_PASS_TYPE_CATALOG_EJB = "ejb/ServicePassType";

	public String DISCOUNT_CONFIGURATION_EJB = "ejb/DiscountConfiguration";

	public String POLICY_RIGHTS_CONFIGURATION_EJB = "ejb/PolicyRightsConfiguration";

	public String ASSURED_AMOUNT_CONFIGURATION_EJB = "ejb/AssuredAmountConfiguration";

	public String ASSURED_AMOUNT_CATALOG_EJB = "ejb/AssuredAmountCatalog";

	public String COVERAGE_SPECIAL_CONFIGURATION_EJB = "ejb/CoverageSpecialConfiguration";

	public String ADJUSTMENT_CONFIGURATION_EJB = "ejb/AdjustmentConfigurationReport";

	public String POLICY_WORKORDER_EJB = "ejb/PolicyWorkOrder";

	public String MAIL_OPERATION_CONFIGURATION_EJB = "ejb/MailOperationConfiguration";

	public String MENU_BY_ATTRIBUTE_EJB = "ejb/MenuByAttribute";

	public String POLIZA_PAGOVIRTUAL_EJB="ejb/PolizaPagoVirtual";

	public String POLIZA_LINEAVIRTUAL_EJB="ejb/LineasPagoVirtual";

	public String POLIZA_RECIBOVIRTUAL_EJB="ejb/RecibosPagoVirtual";

	public String PAGO_VIRTUAL_EJB="ejb/PagoVirtual";

	public String BUSCA_PAGO_VIRTUAL_EJB="ejb/BuscaPagoVirtual";  

	public String PERIOD_TYPE_EJB = "ejb/PeriodType";
	
	public String USER_MENU_EXCLUSION_EJB = "ejb/UserMenuExclusion";
	
	/**
	 * DAO Constants
	 */
	public String AGENT_DAO = "dao/AgentDAO";

	public String LIFE_INSURANCE_INSURED_DAO = "dao/LifeInsuranceInsuredDAO";

	public String LIFE_INSURANCE_QUOTATION_DAO = "dao/LifeInsuranceQuotationDAO";

	public String LIFE_INSURANCE_BENEFICIARY_DAO = "dao/LifeInsuranceBeneficiaryDAO";

	public String LIFE_INSURANCE_QUESTION_DAO = "dao/LifeInsuranceQuestionDAO";

	public String LIFE_INSURANCE_EVALUATION_DAO = "dao/LifeInsuranceEvaluationDAO";

	public String LIFE_INSURANCE_PROFILE_DAO = "dao/LifeInsuranceProfileDAO";

	public String LIFE_INSURANCE_COVERAGE_DAO = "dao/LifeInsuranceCoverageDAO";

	public String LIFE_INSURANCE_CATALOG_DAO = "dao/LifeInsuranceCatalogDAO";

	public String LIFE_INSURANCE_OCCUPATION_DAO = "dao/LifeInsuranceOccupationDAO";

	public String LIFE_INSURANCE_SEARCH_OCCUPATION_DAO = "dao/LifeInsuranceSearchOccupationDAO";

	public String LIFE_INSURANCE_INSURED_TYPE_DAO = "dao/LifeInsuranceInsuredTypeDAO";

	public String LIFE_INSURANCE_POLICY_DAO = "dao/LifeInsruancePolicyDAO";

	public String LIFE_INSURANCE_BENEFICIARY_RELATIONSHIP_DAO =
		"dao/LifeInsuranceRelationshipDAO";

	public String LIFE_INSURANCE_CUSTOMER_DAO = "dao/LifeInsuranceCustomerDAO";

	public String ADJUSTER_DAO = "dao/AdjusterDAO";

	public String QUOTATION_INBOX_DAO = "dao/QuotationInboxDAO";

	public String POLICY_INBOX_DAO = "dao/PolicyInboxDAO";

	public String CABIN_POLICY_INBOX_DAO = "dao/CabinPolicyInboxDAO";

	public String SUBSEQUENT_RECEIPTS_INBOX_DAO = "dao/SubsequentReceiptsInboxDAO";

	public String CLAIM_INBOX_DAO = "dao/ClaimInboxDAO";

	public String CABIN_MONITOR_INBOX_DAO = "dao/CabinMonitorInboxDAO";

	public String ENDORSEMENT_INBOX_DAO = "dao/EndorsementInboxDAO";

	public String ADMINISTRATION_DAO = "dao/AdministrationDAO";

	public String ADDRESS_DAO = "dao/SeycosAddressDAO";

	public String SEYCOS_PORTAL_USER_DAO = "dao/SeycosUserDAO";

	public String SEYCOS_ROLE_DAO = "dao/SeycosRoleDAO";

	public String NATURAL_POLICY_HOLDER_DAO = "dao/NaturalPolicyHolderDAO";

	public String LEGAL_ENTITY_POLICY_HOLDER_DAO =
		"dao/LegalEntityPolicyHolderDAO";

	public String BANK_DAO = "dao/BankDAO";

	public String CITY_DAO = "dao/CityDAO";

	public String STATE_DAO = "dao/StateDAO";

	public String COLONY_DAO = "dao/ColonyDAO";

	public String BRANCHLEP_DAO = "dao/BranchLEPDAO";  

	public String SUB_BRANCHLEP_DAO = "dao/SubBranchLEPDAO";

	public String MAKE_DAO = "dao/MakeDAO";

	public String MODEL_DAO = "dao/ModelDAO";

	public String YEAR_DAO = "dao/YearDAO";

	public String COVERAGE_DAO = "dao/CoverageDAO";

	public String PACKAGE_DAO = "dao/PackageDAO";

	public String PRODUCT_DAO = "dao/ProductDAO";

	public String ACTIVITYNP_DAO = "dao/ActivityNPDAO";  

	public String CLIENT_DAO = "dao/ClientDAO";

	public String QUOTATION_DAO = "dao/QuotationDAO";

	public String REPORT_DAO = "dao/ReportDAO";

	public String CONDUCT_DAO = "dao/ConductDAO";

	public String WELCOME_INBOX_DAO = "dao/WelcomeInboxDAO";  

	public String PAYMENT_DAO = "dao/PaymentDAO";

	public String BUSINESS_LINE_DAO = "dao/BusinessLineDAO";

	public String SUPERVISORIA_DAO = "dao/SupervisoriaDAO";

	public String MASSIVE_EMISSION_DAO = "dao/MassiveEmissionDAO";

	public String VALIDATION_IMPLEMENTATION_DAO = "dao/ValidationImplementationDAO";

	public String ENDORSEMENT_DAO = "dao/EndorsementDAO";

	public String ENDORSEMENT_FLEETS_DAO = "dao/endorsement/EndorsementFleetsDAO";
	public String SINISTER_DAO = "dao/SinisterDAO"; 

	public String ASSOCIATE_SERVICE_DAO = "dao/AssociateServiceDAO";   

	public String THIRD_AFFECTED_DAO = "dao/ThridAffectedDAO";

	public String USE_CODE_DAO = "dao/UseCodeDAO";

	public String PAY_REPORT_DAO = "dao/PayReportDAO";

	public String PROMOTORES_DAO = "dao/PromotoresDAO";

	public String ENDORSEMENT_TYPE_DAO = "dao/EndorsementTypeDAO";

	public String COVERAGE_CONFIGURATION_DAO = "dao/CoverageConfigurationDAO";

	public String AVAILABLE_REPORTS_DAO = "dao/AvailableReportsDAO";

	public String CERTIFIED_DAO = "dao/CertifiedDAO";

	public String CLIENT_CURP_DAO = "dao/ClientCurpDAO";

	public String MODEL_PACKAGE_RANGE_DAO = "dao/ModelPackageRange";

	public String EMPLOYEE_RELATIONSHIP_DAO = "dao/EmployeeRelationshipDAO";

	public String SINISTER_CAUSE_DAO = "dao/SinisterCauseDAO";

	public String AUTHORIZATION_DAO = "dao/AuthorizationDAO";

	public String AUTHORIZATION_INBOX_DAO = "dao/AuthorizationInboxDAO";

	public String FLEET_COMMENT_DAO =	"dao/FleetCommentDAO";

	public String FLEET_QUOTATION_DAO =	"dao/FleetQuotationDAO";

	public String FLEET_SUBSECTIONS_DAO =	"dao/FleetSubSectionsDAO";

	public String FLEET_SOLICITUDE_DISCOUNT_DAO =	"dao/SolicitudeDiscountDAO";

	public String FLEET_SOLICITUDE_STATUS_DAO =	"dao/SolicitudeStatusDAO";

	public String FLEET_QUOTATION_SEYCOS_DAO =	"dao/FleetQuotationSeycosDAO";

	public String MASSIVE_UPLOAD_FLEET_DAO =  "dao/MassiveUploadFleetsDAO";
	public String FLEET_POLICY_INBOX_DAO = "dao/FleetPolicyInboxDAO";

	public String MASSIVE_USER_DAO = "dao/MassiveUserDAO";

	public String DEDUCTIBLE_CONFIGURATION_DAO = "dao/DeductibleConfigurationDAO";

	public String DEDUCTIBLE_CATALOG_DAO = "dao/DeductibleCatalogDAO";

	public String SERVICE_PASS_TYPE_DAO = "dao/ServicePassTypeDAO";

	public String DISCOUNT_CONFIGURATION_DAO = "dao/DiscountConfigurationDAO";

	public String POLICY_RIGHTS_CONFIGURATION_DAO = "dao/PolicyRightsConfigurationDAO";

	public String ASSURED_AMOUNT_CONFIGURATION_DAO = "dao/AssuredAmountConfigurationDAO";

	public String ASSURED_AMOUNT_CATALOG_DAO = "dao/AssuredAmountCatalogDAO";

	public String COVERAGE_SPECIAL_CONFIGURATION_DAO = "dao/CoverageSpecialConfigurationDAO";

	public String ADJUSTMENT_CONFIGURATION_REPORT_DAO = "dao/AdjustmentConfigurationReportDAO";

	public String POLICY_WORKORDER_DAO = "dao/PolicyWorkOrderDAO";

	public String VEHICLE_DESCRIPTION_DAO = "dao/VehicleDescriptionCatalogDAO";

	public String MAIL_OPERATION_CONFIGURATION_DAO = "dao/MailOperationConfigurationDAO";

	public String POLIZA_PAGOVIRTUAL_DAO="dao/PolizaPagoVirtualDAO";

	public String POLIZA_LINEAVIRTUAL_DAO="dao/LineasPagoVirtualDAO";

	public String POLIZA_RECIBOVIRTUAL_DAO="dao/RecibosPagoVirtualDAO";

	public String PAGO_VIRTUAL_DAO ="dao/PagoVirtualDAO";

	public String BUSCA_PAGO_VIRTUAL_DAO ="dao/BuscaPagoVirtualDAO"; 
	
	public String RFC_DAO = "dao/RfcDAO"; 

	public String USER_MENU_EXCLUSION_DAO = "dao/UserMenuExclusionDAO";

	/**
	 * GENERAL CONSTANTS   
	 */
	public int ROLE_AGENT_ID = 1;

	public int ROLE_CONDUCT_ID = 2;

	public int ROLE_EMPLOYEE_ID = 8;

	public int ROLE_HR_ID = 9;

	public int ROLE_ENTERPRISE_ID = 10;

	public int ROLE_IMMIGRANT_ADMIN = 15;

	public int ROLE_PAYER_OFFICE = 16;

	public int ROLE_EMISSION_MANAGER = 17;

	public String NACIONALITY_MEXICAN = "PAMEXI";

	public String PROPERTY_MAX_ROWS = "inbox.max.rows";

	public String PROPERTY_MAX_ROWS_PER_PAGE = "inbox.rows.per.page";

	public String MAX_PREVIOUS_YEARS_HISTORY = "report.filter.maxPreviousYearsHistory";

	public String VALID_ASSURED_AMOUNT_LIFE_ADDRESSING = 
		"valid.assured.amount.life.addressing";

	public String ADMINISTRATOR_MAIL = "administration.mail";

	public String SUBSCRIBER_PRODUCT_1 = "user.authorization.product.1.subscriber";

	/**
	 * ORACLE TYPES
	 */  
	public String ORACLE_DESCRIPTOR_STRARRAY = "STRARRAY";

	/**
	 * Product Business Lines
	 */
	public int PUBLIC_SERVICE_BUSINESS_LINE_ID = 1238;

	public int BUS_SERVICE_BUSINESS_LINE_ID = 1252;

	public int IMMIGRANT_BUSINESS_LINE_ID = 1268;

	public int PUBLIC_SERVICE_MTY_BUSINESS_LINE_ID = 1269;

	/**
	 * SECURITY CONSTANTS
	 */
	public int RECOVER_PASSWORD_MAIL_TEMPLATE_ID = 1;

	public String PASSPHRASE_XAMPLITUDE = "passphrase.xamplitude";

	public String PASSPHRASE_XWAVELENGTH = "passphrase.xwavelength";

	public String PASSPHRASE_YAMPLITUDE = "passphrase.yamplitude";

	public String PASSPHRASE_YWAVELENGTH = "passphrase.ywavelength";

	public int NO_USER_PASSWORD_DEFINED = 10;

	public int USER_PASSWORD_ABOUT_TO_EXPIRE = 15;

	public int GIVEN_PASSWORD_NOT_VALID = 20;

	public int USER_IS_NOT_ACTIVE = 30;

	public int USER_IS_NOT_PRESENT = 40;

	public int USER_PASSWORD_IS_EXPIRED = 50;

	public int USER_MAX_PASSWORD_INCORRECT = 60;

	public int USER_NEVER_LOGGED_IN = 70;

	public int USER_ONE_REMAINING_FAILURE = 80;

	public int PASSWORD_REPEATS = 90;

	public int USER_HAS_NO_EMAIL = 100;

	public int GIVEN_MAIL_DOES_NOT_MATCH = 110;

	public int AGENT_INVALID = 120;

	public int USER_HAS_NO_AGENT_ID = 11;

	public int SEYCOS_EXCEPTION = -9999;

	public int PROSA_PAYMENT_EXCEPTION = -10000; 
	public int PROSA_PAYMENT_DIFFERENT_AMOUNT = -10001;  
	public int SEYCOS_PAYMENT_EXCEPTION = -20000;  
	public int EMISSION_PAYMENT_EXCEPTION = -20001;

	public int EMISSION_EXCEPTION = -30000;
	public int ERROR_INVALID_SERIAL_NUMBER = -30001;
	public int ERROR_NO_EMPLOYEE_RELATIONSHIP = -30002;

	public int ERROR_INVALID_PAYMENT_CONDUIT = -30010;  
	public int ERROR_PAYMENT_NO_HOLDER = -30012;  
	public int ERROR_PAYMENT_NO_BANK = -30013;
	public int ERROR_PAYMENT_NO_ACCOUNT_TYPE = -30014;
	public int ERROR_PAYMENT_NO_ACCOUNT = -30015;
	public int ERROR_PAYMENT_NO_CVV = -30016;
	public int ERROR_PAYMENT_NO_DUE_DATE = -30017; 
	public int ERROR_PAYMENT_NO_PHONE = -30018;
	public int ERROR_PAYMENT_NO_PERSON_TYPE = -30026;
	public int ERROR_PAYMENT_NO_CONDUIT_ID = -30027;
	public int ERROR_PAYMENT_INVALID_VIGENCY = -30028;
	public int ERROR_PAYMENT_WRONG_ACCOUNT = -30029;
	public int ERROR_PAYMENT_DEACTIVATING_CONDUIT = -30033;  
	public int ERROR_PAYMENT_INSERTING_ADDRESS = -30032;  
	public int ERROR_PAYMENT_NO_INFO_FOUND = -30030;
	public int ERROR_PAYMENT_GRABAR_DATOS = -30031;
	public int ERROR_PAYMENT_NO_CONDUIT_ALLOWED = -30034;
	public int ERROR_PAYMENT_MISSING_CONDUIT_INFO = -30035;
	public int ERROR_PAYMENT_CANNOT_SELECT_ACOUNT = -30036;
	public int ERROR_PAYMENT_TYPE_CC = -30037;
	public int ERROR_PAYMENT_TYPE_AFIRME = -30038;
	public int ERROR_PAYMENT_ACCOUNT_BALANCE_INQUIRY = -30040;
	public int ERROR_PAYMENT_NO_RECEIPTS = -30050;
	public int ERROR_APPLYING_PAYMENT = -30051;  

	public int ERROR_PRINTING_NO_SELECTED = -50000;
	public int ERROR_PRINTING_INVALID_PRINT_OPTION = -50001;
	public int ERROR_PRINTING_TIME_EXCEPTION = -50002;
	public int ERROR_PRINTING_NULL_DOC = -50003;
	public int ERROR_AXIS_SERVICE_EXCEPTION = -50004;

	public int ERROR_FORCE_DATE_LESS_THAN_TODAY = -60000;
	public int ERROR_FORCE_DATE_GREATER_THAN_TODAY = -60001;

	public int ERROR_ENDORSEMENT_NO_EMISSION_CENTER = -70000;
	public int ERROR_ENDORSEMENT_REQUEST_PENDING = -70001;
	public int ERROR_ENDORSEMENT_INVALID_DATE_CANCELATION = -70002;
	public int ERROR_ENDORSEMENT_GENERIC_EXCEPTION = -70003;
	public int ERROR_ENDORSEMENT_INVALID_SERIAL = -70004;
	public int ERROR_ENDORSEMENT_INVALID_POLICY_VIGENCY = -70005;
	public int ERROR_ENDORSEMENT_INVALID_ENDORSEMENT_VIGENCY = -70006;
	public int ERROR_ENDORSEMENT_INVALID_REQUEST_CANCELATION = -70007;
	public int ERROR_ENDORSEMENT_INVALID_ENDORSEMENT_CANCELATION = -70008;
	public int ERROR_ENDORSEMENT_INVALID_NEW_SERIAL_VS_ORIGINAL = -70009;

	public int ERROR_INVALID_DATE_FILTERS = -90000;

	public int ERROR_SERIAL_NUMBER_DUPLICATE = -100000;  
	public int ERROR_SERIAL_NUMBER_MALFORMED = -100001;  
	public int ERROR_SERIAL_NUMBER_INVALID_VIN = -100002;
	public int ERROR_SERIAL_NUMBER_INVALID_LENGHT = -100003;
	public int ERROR_SERIAL_NUMBER_DUPLICATE_END = -100004;
	public int ERROR_SERIAL_NUMBER_INVALID_VIN_END = -100005;

	public int ERROR_NEED_EMISSION_AUTHORIZATION = -100020;
	public int ERROR_INVALID_STATUS_FOR_PERSON = -100021;
	public int ERROR_MAX_LIMIT_EXCEEDED = -100022;
	public int ERROR_INVALID_VEHICLE = -100023;
	public int ERROR_INVALID_PREMIUM = -100024;

	public String INTERNET_AGENT_USER_ID = "public.agent.user.id";

	public String PASSWORD_CANNOT_REPEAT_USED = "password.cannot.repeat.used";

	public String USER_JSESSION_ID = "userJSessionId";

	public String USER_IP_ADDRESS = "userIPAddress";

	public String PASSWORD_EXPIRES_PROPERTY = "password.expires";

	public String DOWNLOAD_APPLICATION_AUTO = "download.application.auto";

	public String FILE_MANAGEMENT_APPLICATION_WEB_ROOT = "filemanagement.applicationWebRoot";

	public String FILE_MANAGEMENT_USER = "filemanagement.user";

	public String FILE_MANAGEMENT_PASSWORD = "filemanagement.password";

	public String PASSWORD_EXPIRES_ALERT_PROPERTY = "password.expires.alert";

	public String PASSWORD_MAX_INCORRECT = "password.max.incorrect";

	public String USER_TOTAL_ENTRIES = "totalEntries";

	public String USER_HAS_PASSWORD = "hasPassword";

	public String USER_REMAINING_FAILURE_LOGINS = "remainingFailureLogins";

	public String USER_TOTAL_LOGINS = "totalLogins";

	public String USER_INFO_USER_ID = "userId";

	public String USER_INFO_USERNAME = "username";

	public String USER_INFO_ROLE = "role";

	public String USER_INFO_ACTIVE = "active";

	public String USER_INFO_APPLICATION_ID = "applicationId";

	public String USER_INFO_FIRST_NAME = "firstname";

	public String USER_INFO_FATHER_LAST_NAME = "fatherLastName";

	public String USER_INFO_MOTHER_LAST_NAME = "motherLastName";

	public String USER_INFO_EMAIL = "email";

	public String VALIDATE_USER = "validateUser";

	public String REGISTER_USER_ACCESS = "registerUserAccess";

	public String MENU_BUSINESS_LINES = "businessLines";
	
	public String INTERNET_AGENT_ID = "intAgentId";

	/**
	 * QUOTATION CONSTANTS
	 */
	public int QUOTATION_MAIL_TEMPLATE_ID = 2;

	public int POLICY_MAIL_TEMPLATE_ID = 6;

	public Integer EMPTY_ID = new Integer(0);

	public int NOTIFICATION_TEMPLATE_ID = 3;

	public String BARCODE_SERVLET = "barcode.servlet";

	public int GENERAL_NOTIFICATION_TEMPLATE_ID = 4;

	public int RECEIPT_MAIL_TEMPLATE_ID = 5;

	public int SUBSCRIBER_NOTIFICATION_TEMPLATE_ID = 7;

	public int EMPLOYEE_TO_SUBS_AUTH_NOTIFICATION_TEMPLATE_ID = 8;

	public int EMPLOYEE_TO_SUBS_EMI_NOTIFICATION_TEMPLATE_ID = 9;

	public int VIN_NOTIFICATION_TEMPLATE = 10;

	public int AUTHORIZATION_NOTIFICATION_TEMPLATE = 11;

	public int WORKORDER_NOTIFICATION_TEMPLATE = 12;

	/**
	 * REPORTS PATHS.JRXML
	 */
	public String QUOTATION_INBOX =
		"/com/bsd/seycos/report/jrxml/Cotizaciones.jrxml";

	public String POLICY_INBOX =
		"/com/bsd/seycos/report/jrxml/Polizas.jrxml";

	public String PRODUCTION_REPORT =
		"/com/bsd/seycos/report/jrxml/VRP_003.jrxml";

	public String PROMOTION_PRODUCTION_REPORT =
		"/com/bsd/seycos/report/jrxml/VRP_PROMOTORES.jrxml";

	public String MAIN_ACCOUNT_STATEMENT_REPORT =
		"/com/bsd/seycos/report/jrxml/Estado_cuenta.jrxml";

	public String DETAIL_ACCOUNT_STATEMENT =
		"/com/bsd/seycos/report/jrxml/Desglose_movimientos.jrxml";

	public String DETAIL_PREMIUM_PAID_REPORT =
		"/com/bsd/seycos/report/jrxml/DetailPremiumPaid.jrxml";

	public String RENEWAL_REPORT =
		"/com/bsd/seycos/report/jrxml/Renewal.jrxml";  

	public String PORTFOLIO_STATUS  =
		"/com/bsd/seycos/report/jrxml/CalidadCartera.jrxml";

	public String SUB_REPORT_PORTFOLIO_1  =
		"/com/bsd/seycos/report/jrxml/sub_Poliza.jrxml";

	public String SUB_REPORT_PORTFOLIO_2  =
		"/com/bsd/seycos/report/jrxml/sub_EstadisticasxPoliza.jrxml";

	public String SUB_REPORT_PORTFOLIO_3  =
		"/com/bsd/seycos/report/jrxml/sub_VA_tipo_vehiculo.jrxml";

	public String SUB_REPORT_PORTFOLIO_4  =
		"/com/bsd/seycos/report/jrxml/sub_VA_tipo_paquete.jrxml";

	public String SUB_REPORT_PORTFOLIO_5  =
		"/com/bsd/seycos/report/jrxml/sub_Siniestros1.jrxml";

	public String SUB_REPORT_PORTFOLIO_6  =
		"/com/bsd/seycos/report/jrxml/sub_Siniestros2.jrxml";

	public String SUB_REPORT_PORTFOLIO_7  =
		"/com/bsd/seycos/report/jrxml/sub_Siniestros3.jrxml";

	public String SUB_REPORT_PORTFOLIO_8  =
		"/com/bsd/seycos/report/jrxml/DesgloseSiniestros.jrxml";

	/**
	 * REPORTS IMAGES
	 */  
	public String IMAGE_SEYCOS =
		"/com/bsd/seycos/report/jrxml/images/logo_seguros_11.gif";

	public String IMAGE_ACCOUNT_STATEMENT_1 =
		"/com/bsd/seycos/report/jrxml/images/cuadror.JPG";

	public String IMAGE_ACCOUNT_STATEMENT_2 =
		"/com/bsd/seycos/report/jrxml/images/cuadror2.JPG";

	public String IMAGE_ACCOUNT_STATEMENT_3 =
		"/com/bsd/seycos/report/jrxml/images/cuadror3.gif";

	public String IMAGE_ACCOUNT_STATEMENT_4 =
		"/com/bsd/seycos/report/jrxml/images/cuadror4.gif";

	public String IMAGE_HEADER  =
		"/com/bsd/seycos/report/jrxml/images/logo_AS.jpg";

	/**
	 * PAGOS VIRTUAL
	 */
	public String IMAGE_PAGO_VIRTUAL  =
		"/com/bsd/seycos/report/jrxml/images/Cintillo_Seguros.gif";

	public String IMAGE_RUP  =
		"/com/bsd/seycos/report/jrxml/images/logoGS1.jpg";

	public String IMAGE_OXXO  =
		"/com/bsd/seycos/report/jrxml/images/oxxo.jpg";

	public String IMAGE_MARCA_AGUA  =
		"/com/bsd/seycos/report/jrxml/images/sinvalor_fiscal.jpg";


	/**
	 * Web Marketing Constants
	 */
	public String PUBLIC_CLIENT_DEFAULT_ADDRESS_ID =
		"public.client.default.addressid";

	/**
	 * Agent Constants
	 */
	public String AGENT_LIST = "agentList";

	public String USER_ATTRIBUTE_EMPLOYEE_PAYROLL_NUMBER = "employee.payroll.number";

	public String USER_ATTRIBUTE_EMPLOYEE_PAYMENT_ID = "employee.paymentId";

	public String USER_ATTRIBUTE_AGENT_ID = "agentId";

	public String USER_ATTRIBUTE_AGENT_USERNAME = "agentUsername";

	public String USER_ATTRIBUTE_AGENT_EMISSION_CENTER_ID = "agentEmissionCenterId";

	public String USER_ATTRIBUTE_AGENT_OFFICE_ID = "agentOfficeId";

	public String USER_ATTRIBUTE_BUSINESS_LINE_LIST = "businessLineList";

	public String USER_ATTRIBUTE_VEHICLE_LIST = "userVehicleList";

	public String USER_ATTRIBUTE_AGENT_DISCOUNT = "agentDiscount";

	public String USER_ATTRIBUTE_AGENT_PAY_METHODS = "agentPayMethods";

	public String USER_ATTRIBUTE_AGENT_VEHICLE_ZONE = "agentVehicleZone";

	public String USER_ATTRIBUTE_AGENT_VEHICLE_ZONE_DESC = "agentVehicleZoneDesc";

	public String USER_ATTRIBUTE_MAX_VEHICLES = "policy.check.maxVehicles";

	public String USER_ATTRIBUTE_CHECK_PAYMENT_CHARGE = "massiveEmission.payment.charge";

	public String USER_ATTRIBUTE_CASH_CONF = "cashPayment";

	public String USER_ATTRIBUTE_CREDIT_CONF = "creditPayment";

	public String USER_ATTRIBUTE_ACCOUNT_CONF = "accountPayment";

	public String USER_ATTRIBUTE_CASH_FAM_CONF = "cashPaymentFamily";

	public String USER_ATTRIBUTE_CREDIT_FAM_CONF = "creditPaymentFamily";

	public String USER_ATTRIBUTE_ACCOUNT_FAM_CONF = "accountPaymentFamily";

	public String USER_ATTRIBUTE_EMISSION_MSG = "emission.agreement.message";

	public String USER_ATTRIBUTE_EMISSION_MSG_ARG0 = "emission.agreement.message.arg0";

	public String USER_ATTRIBUTE_EMISSION_MSG_ARG1 = "emission.agreement.message.arg1";

	public String USER_ATTRIBUTE_EMISSION_MSG_ARG2 = "emission.agreement.message.arg2";

	public String USER_ATTRIBUTE_MAX_INDIVIDUAL_RECEIPTS = "wfactura.individual.max.receipts";

	public String USER_ATTRIBUTE_MAX_ZIP_RECEIPTS = "wfactura.zip.max.receipts";

	public String USER_ATTRIBUTE_MAX_ZIP_FILE_HOUR = "wfactura.zip.max.hour";

	public String USER_ATTRIBUTE_MIN_ZIP_FILE_HOUR = "wfactura.zip.min.hour";

	public String USER_ATTRIBUTE_POLICY_TYPE = "employee.policy.type";

	public String USER_ATTRIBUTE_SUPERVISOR_ID = "supervisorId";

	public String AGENT_ATTRIBUTE_CASH_PAYMENT_METHODS = "cash.payment.methods";

	public String AGENT_ATTRIBUTE_TC_PAYMENT_METHODS = "tc.payment.methods";

	public String AGENT_ATTRIBUTE_ADDRESSING_PAYMENT_METHODS = "addressing.payment.methods";

	public String AGENT_ATTRIBUTE_EMISSION_AUTH_PATH = "emissionAuthorizationPath";

	public String AGENT_ATTRIBUTE_INVALID_REPORT_IDS = "invalidReportIdsForAgent";

	public String AGENT_ATTRIBUTE_ADD_CONDUCT_PREFIX = "add.conduct.prefix";

	public String AGENT_ATTRIBUTE_ADD_PAYMENT_ATTRIBUTE = "user.attribute.payment.check";

	public String AGENT_ATTRIBUTE_END_FORCE_DATE = "massiveEmission.policy.endForceDate";

	public String AGENT_ATTRIBUTE_EXCLUDE_ANNUAL_COST = "employee.excludeAnnualCost";

	public String AGENT_EMPLOYEE_BEHAVIOR = "employee.behavior";

	public String DRIVER_MIN_AGE = "driver.min.age";

	public String DRIVER_MAX_AGE = "driver.max.age";

	public String EMISSION_SUPPORT_EMAIL = "emission.support.email";

	/**
	 * ORIGIN CONSTANTS
	 */
	public String ORIGIN_SYSTEM = "ED";

	public String ORIGIN_WEB_MARKETING = "WM";

	/**
	 * MAIL NOTIFICATIONS 
	 */
	public int AUTHORIZE_NOTIFICATION = 1;

	public int DECLINE_NOTIFICATION = 2;

	/* PAYMENT TYPES */
	public int CREDIT_CARD_PAYMENT = 1;

	public int ADDRESSING_PAYMENT = 2;

	public int CASH_PAYMENT = 3;


	/**
	 * AUTHORIZATION REQUEST TYPES 
	 */

	public int AUTHORIZATION_TYPE = 1;

	public int REJECTION_TYPE = 2;


	/**
	 * PAYMENT TYPES
	 */

	public static final String ANUAL_MONTHS_WITHOUT_INTEREST_CODE = "98";

	public String PAYMENT_DEBIT = "DEBITO";

	public String PAYMENT_CLABE = "CUENTA CLABE";

	public String PAYMENT_ACCOUNT = "CUENTA BANCARIA";

	public int PAYMENT_TYPE_CASH = 15;

	public int PAYMENT_TYPE_CC = 4;

	public int PAYMENT_TYPE_ACCOUNT = 8;

	public int PAYMENT_TYPE_AFIRME = 1386;

	public String PAYMENT_BANK_AFIRME = "51";

	public String EIBS_CODE_NO_FUNDS = "1616";

	public String PAYMENT_CC_PAYMENT_DAY_AS_FORCE_DATE = 
		"policy.creditCard.setPaymentDayAsVigency";


	/**
	 * APPLICATION ORIGIN CODES FOR SERVICES
	 */

	public static final String APP_ORIGIN_ED = "EDAUT";

	public static final String APP_ORIGIN_EIBS = "BNAUT";

	public static final String APP_ORIGIN_SEYCOS = "SEYCOS";

	public static final String APP_ORIGIN_AUTOPLAZO = "AUTOPZ";

	public static final String APP_ORIGIN_OTHER = "OTHER";

	/**
	 * Active Manager's cookie.
	 */
	public String I_PLANET_COOKIE = "iPlanetDirectoryPro";

	/**
	 * Elements types
	 */
	public String ELEMENT_TYPE_LABEL = "LABEL";

	public String ELEMENT_TYPE_PROPERTY_LABEL = "PROPERTY_LABEL";

	public String ELEMENT_TYPE_COMBO = "COMBO";

	public String ELEMENT_TYPE_RANGE = "RANGE";



	/**
	 * LIFE Modules Constants
	 */

	public String CERTIFIED_CONSENT = "CertifiedConsent";

	public String LIFE_MOD_CONF = "LifeModuleConf";

	public String LIFE_MOD_PKG = "PORTAL.PKG_LIFE_MODULES";

	public BigDecimal INSURED_RESERVE_LIMIT = new BigDecimal("500000");

	/**
	 * Consultas Midas
	 */
	public static final String PKG_CONSULTAS_MIDAS = "PORTAL.PKG_CONSULTA_MIDAS";

	/*
	 * Asignacion de agentes
	 */
	
	public static final String PKG_ASIGNACION_AGENTES = "PORTAL.PKG_CONSULTA_TIPO_POLIZA";
	
	/**
	 * SPUP CABINA
	 * */
	public static final int SIPAC_COVERAGE_RC_VEHICLE = 1563;

	public static final int SIPAC_COVERAGE_RC_ASSET = 1564;

	public static final int SIPAC_COVERAGE_RC_PERSON = 1562;

	public static final int SIPAC_COVERAGE_DM = 1;

	public static final int SIPAC_COVERAGE_GMO = 7;

	public static final int SIPAC_COVERAGE_RC_TRAVELER = 1553;

	public static final String HELPER_CURP_URL_RENAPO = "helper.curp.url.renapo";

	public static final String HELPER_CURP_DUMMY_LIFEBUSINESSLINE = "helper.curp.dummy.lifeBusinessLine";

	public static final String HELPER_CURP_TAXIBUSINESSLINES = "helper.curp.taxiBusinessLines";

	public static final String HELPER_FILES_URL_WS = "helper.files.url.ws";

	public static final String AVISO_PRIVACIDAD_AUTOS = "AP_Autos.pdf";

	public static final String AVISO_PRIVACIDAD_DANIOS = "AP_Danios.pdf";

	public static final String AVISO_PRIVACIDAD_VIDA = "AP_Vida.pdf";

	/**
	 * VMHS-DES-SEYCOS-PERFORMANCE-015: Compresor de javascript Performance Login  
	 * Variable agregada para el archivo de compresion de javascripts, indica 
	 * cuales son los archivos que se consideraran a comprimir.
	 * El archivo se ubica en SeycosPortalWeb\WebRoot
	 */
	public static final String JAVASCRIPT_COMPRESSOR_CONFIG_FILE="compressorConfiguration.properties";
	//Indica el nombre del archivo compreso de javascript
	public static final String JAVASCRIPT_COMPRESSED_FILE="emisionDelegada.min";
	//Indica la carpeta en donde se generar� el archivo comprimido de javascript
	public static final String JAVASCRIPT_COMPRESSED_FILE_FOLDER="scripts";
	//Arreglo de los nombres de las carpetas que se escanearan dentro de WebRoot, ej: SeycosPortalWeb/js,SeycosPortalWeb/scripts
	//y que seran consideradas para buscar los archivos js dentro de estas carpetas, con el fin de no escanear carpetas como 
	//img, images, css,jsp,resources,styles,etc...
	public static final String[] JAVASCRIPT_COMPRESSED_FOLDERS_TO_SCAN={"js","scripts"};


	/**
	 * JERC-DES-ED-0XX: Endpoint servicio de derivaci�n HGS
	 */

	public static final String HGS_SINISTER_URL_EFILE = "hgs.sinister.url.efile";

	/**
	 * JERC-DES-ED-008: Condiciones para activar promociones a meses sin intereses en pagos con TC.
	 */
	public static final String MONTHS_WITHOUT_INTEREST_DISCOUNT_MIN_LIMIT = "monthsWithoutInterest.discountMinLimit";
	public static final String MONTHS_WITHOUT_INTEREST_DISCOUNT_MAX_LIMIT = "monthsWithoutInterest.discountMaxLimit";
	public static final String MONTHS_WITHOUT_INTEREST_EXCLUDED_AGENTS = "monthsWithoutInterest.excludedAgents";
	public static final String MONTHS_WITHOUT_INTEREST_EXCLUDED_BUSINESS_LINES = "monthsWithoutInterest.excludedBLines";
	public static final String MONTHS_WITHOUT_INTEREST_DISCOUNT_MAX_RANGE = "monthsWithoutInterest.discountMaxRange";

	public static final String ANUAL_MONTHS_WITHOUT_INTEREST_INCLUDED_BUSINESS_LINES = "anualMonthsWithoutInterest.includeBLines";

	/**
	 * Propiedad para determinar si un agente tiene permisos para aplicar descuentos sobre el paquete b�sico
	 * JERC 18/04/2012
	 */
	public static final String AGENT_ATTRIBUTE_APPLY_DISCOUNT_BASIC_PACKAGE = "applyDiscountBasicPackage";

	/**
	 * Coberturas de gastos médicos
	 */
	public static final String MEDICAL_COVERAGES = "medical.coverages";
	/**
	 * Coberturas de adaptaciones y conversiones
	 */
	public static final String ADJUSTMENT_COVERAGES = "adjustment.coverages";
	/**
	 * Valor por defecto a usar en la ocupaci�n para personas f�sicas
	 * (Esto por el m�dulo de emisi�n masiva)
	 */
	public static final String NATURAL_POLICY_HOLDER_DEFAULT_OCCUPATION = "9552";
	/**
	 * JERC-DES-WS-001: Propiedades para el cotizador por Web Service 
	 */
	public static final String WEB_SERVICE_PAY_METHODS = "webService.payMethods";
	public static final String WEB_SERVICE_BUSINESS_LINES = "webService.businessLines";
	/**
	 * Fin propiedades para el cotizador por Web Service
	 */

	/**
	 * JERC-DES-ED-021: Propiedades Midas Autos
	 */
	public static final String MIDAS_AUTOS_BASE_PATH = "midasAuto.basePath";
	public static final String MIDAS_AUTOS_INITIAL_PATH = "midasAuto.initialPath";
	/**
	 * Fin propiedades Midas Autos
	 */

	public static final String EXCLUDED_ROLES_VALIDATE_PHONE = "excluded.roles.validatePhone";

	/**
	 * HJC-DES-ED-XXX: Tipos de carga para cargas masivas (p�lizas o endosos)
	 */
	public static final String MASSIVE_EMISSION_ENDORSEMENT_LOAD_TYPE = "END";
	public static final String MASSIVE_EMISSION_POLICY_LOAD_TYPE = "POL";

	/**
	 * Nombre de la propiedad en <code>UMGT.TBL_PROPERTY</code> que contienen los ids, separados por ,
	 * de los agentes exluidos para la propiedad solo lectura de deducibles en cotizaciones.
	 */
	public static final String EXCLUDED_AGENT_READONLY_PROPERTY = "excluded.agent.readonly.property";

	//PYMES
	public static final String LOOKUP_COPIACOT_PROVIDER_URL= "ed.copiacot.lookup.icontext.providerurl";

	/**
	 * Nombre de la propiedad en <code>UMGT.TBL_PROPERTY</code> que contiene el
	 * id de contratante en vida modulo
	 */
	public static final String CERTIFIED_CLIENT_PROPERTY = "life.certified.client";

	/**
	 * Nombre de la propiedad en <code>UMGT.TBL_PROPERTY</code> la fecha minima
	 * en formato yyyyMM para obtener los reportes de
	 */
	public static final String AGENTS_REPORTS_MIN_DATE = "agents.reports.min.date";

	public String AGENTS_REPORTS_ENDPOINT = "agents.reports.endpoint";
	
	//Informacion Vehicular
	public static final String INFORMACION_VEHICULAR_END_POINT_PROPERTY = "ed.informacionVehic.services.endpoint";
	public static final String SEPARADOR_VEHICULO_INICIO = "<inicio>";
	public static final String SEPARADOR_VEHICULO_FIN = "<fin>";
	public static final String SEPARADOR_VEHICULO_CARACTERISTICAS = "\\|";
	public static final int ERROR_VIN_ALTERADO = 2;
	public static final int ERROR_VIN_NO_REGISTRADO = 3;
	public static final int ERROR_VIN_ERRONEO = 4;
	public static final int ERROR_VIN_INVALIDO = 5;	
	public static final int ERROR_VIN_NO_ENCONTRADO = 6;

}