package mx.com.afirme.vida.domain.movil.cotizador;

import java.util.Map;

import net.sf.jasperreports.engine.JasperReport;

public class ReportConfigurationDTO implements java.io.Serializable {

  private JasperReport report;
  
  private Map parameters;
  
  private String reportType;

  private String reportName;

  private String reportFormat;
  
  private String contentType;
  
  private int numOfCopies = 1;
  
  private String reportPath;  
  
  private String digitalKey;
  
  public String getReportPath() {
	return reportPath;
  }

  public void setReportPath(String reportPath) {
	this.reportPath = reportPath;
  }

  public int getNumOfCopies() {
    return numOfCopies;
  }

  public void setNumOfCopies(int numOfCopies) {
    this.numOfCopies = numOfCopies;
  }

  public JasperReport getReport() {
    return report;
  }

  public void setReport(JasperReport report) {
    this.report = report;
  }

  public Map getParameters() {
    return parameters;
  }

  public void setParameters(Map parameters) {
    this.parameters = parameters;
  }

  public String getReportType() {
    return reportType;
  }

  public void setReportType(String reportType) {
    this.reportType = reportType;
  }

  public String getReportName() {
    return reportName;
  }

  public void setReportName(String reportName) {
    this.reportName = reportName;
  }

  public String getReportFormat() {
    return reportFormat;
  }

  public void setReportFormat(String reportFormat) {
    this.reportFormat = reportFormat;
  }

  public String getContentType() {
    return contentType;
  }

  public void setContentType(String contentType) {
    this.contentType = contentType;
  }

  public String getDigitalKey() {
    return digitalKey;
  }

  public void setDigitalKey(String digitalKey) {
    this.digitalKey = digitalKey;
  }

}
