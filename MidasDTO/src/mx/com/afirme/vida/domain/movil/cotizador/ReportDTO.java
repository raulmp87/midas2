package mx.com.afirme.vida.domain.movil.cotizador;

import com.js.dto.AbstractDTO;


public class ReportDTO
    extends AbstractDTO implements java.io.Serializable {

  private static final long serialVersionUID = 1L;
  
  public static final String PROCEDURE_NAME = "procedureName";
  
  public static final String SOURCE_CONNECTION = "sourceConnection";
  
  public static final String SOURCE_DATASOURCE = "sourceDataSource";
  
  public static final String SOURCE_RESULT_SET = "sourceResultSet";


  private byte[] byteArray = null;

  private String errorMessage = null;

  private String contentTypeReport = null;
  
  private String source = ReportDTO.SOURCE_CONNECTION;

  public String getContentTypeReport() {
    return this.contentTypeReport;
  }

  public void setContentTypeReport(String contentTypeReport) {
    this.contentTypeReport = contentTypeReport;
  }

  public byte[] getByteArray() {
    return this.byteArray;
  }

  public void setByteArray(byte[] byteArray) {
    this.byteArray = byteArray;
  }

  public String getErrorMessage() {
    return this.errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

 

  public boolean existError() {
    return (this.byteArray == null && this.errorMessage != null);
  }
  
  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public boolean equals(Object object) {
    return this==object;
  }

}
