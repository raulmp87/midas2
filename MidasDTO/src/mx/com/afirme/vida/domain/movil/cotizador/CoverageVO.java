package mx.com.afirme.vida.domain.movil.cotizador;

import java.io.Serializable;

public class CoverageVO
    implements Serializable {

  private static final long serialVersionUID = 1915631474592123600L;

  private Integer id;
	
	private String name;
	
	private Integer product;

	public Integer getProduct() {
    return product;
  }

  public void setProduct(Integer product) {
    this.product = product;
  }

  public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}
	
  public String getDescription() {
    return this.name;
  }

  public boolean equals(Object object) {
    boolean equal = (this==object);
    if(!equal && (object instanceof CoverageVO)) {
      CoverageVO coverage = (CoverageVO)object;
      equal = coverage.getId().equals(this.id);
    } // End of if
    return equal;
  }
  
}
