package mx.com.afirme.vida.domain.movil.cotizador;

import java.io.Serializable;


public class ProductVO
    implements Serializable {

  private static final long serialVersionUID = 7593496845220592018L;
  
  private Integer productId = null;

	public Integer getProductId() {
    return productId;
  }

  public void setProductId(Integer productId) {
    this.productId = productId;
  }

  public boolean equals(Object object) {
    boolean equal = (this==object);
    if(!equal && (object instanceof ProductVO)) {
      ProductVO productVO = (ProductVO)object;
      equal = productVO.getProductId().equals(this.productId);
    } // End of if
    return equal;
	}

}
