package mx.com.afirme.vida.domain.movil.cotizador;

import java.io.Serializable;

public class InsuredTypeVO implements Serializable{
  
  private static final long serialVersionUID = 1083249409055735259L;

  private String id;
	
	private String type;

	public void setId(String id) {
		this.id = id;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public String getType() {
		return type;
	}
	
  public String getDescription() {
    return this.type;
  }

	public boolean equals(Object object) {
	  boolean equal = (this==object);
	  if(!equal && (object instanceof InsuredTypeVO)) {
	    InsuredTypeVO insuredType = (InsuredTypeVO)object;
	    equal = insuredType.getId().equals(this.id);
	  } // End of if
		return equal;
	}
	
}
