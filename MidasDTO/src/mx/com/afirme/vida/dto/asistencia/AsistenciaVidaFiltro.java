package mx.com.afirme.vida.dto.asistencia;

import java.util.Date;

import mx.com.afirme.vida.domain.asistencia.AsistenciaVida.AsistenciaVidaEstatus;

public class AsistenciaVidaFiltro{
	private Long idBatch;
	private AsistenciaVidaEstatus estatus;
	private Date fechaRecepcionDesde;
	private Date fechaRecepcionHasta;
	private String tipoAsistencia;
	private Date fechaInicioDesde;
	private Date fechaInicioHasta;
	private String numPoliza;
	private String nombreCompleto;
	private String idProveedor;
	
	public Long getIdBatch() {
		return idBatch;
	}
	public void setIdBatch(Long idBatch) {
		this.idBatch = idBatch;
	}
	public AsistenciaVidaEstatus getEstatus() {
		return estatus;
	}
	public void setEstatus(AsistenciaVidaEstatus estatus) {
		this.estatus = estatus;
	}
	public Date getFechaRecepcionDesde() {
		return fechaRecepcionDesde;
	}
	public void setFechaRecepcionDesde(Date fechaRecepcionDesde) {
		this.fechaRecepcionDesde = fechaRecepcionDesde;
	}
	public Date getFechaRecepcionHasta() {
		return fechaRecepcionHasta;
	}
	public void setFechaRecepcionHasta(Date fechaRecepcionHasta) {
		this.fechaRecepcionHasta = fechaRecepcionHasta;
	}
	public String getTipoAsistencia() {
		return tipoAsistencia;
	}
	public void setTipoAsistencia(String tipoAsistencia) {
		this.tipoAsistencia = tipoAsistencia;
	}
	public Date getFechaInicioDesde() {
		return fechaInicioDesde;
	}
	public void setFechaInicioDesde(Date fechaInicioDesde) {
		this.fechaInicioDesde = fechaInicioDesde;
	}
	public Date getFechaInicioHasta() {
		return fechaInicioHasta;
	}
	public void setFechaInicioHasta(Date fechaInicioHasta) {
		this.fechaInicioHasta = fechaInicioHasta;
	}
	public String getNumPoliza() {
		return numPoliza;
	}
	public void setNumPoliza(String numPoliza) {
		this.numPoliza = numPoliza;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	public String getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(String idProveedor) {
		this.idProveedor = idProveedor;
	}
	
	
}

