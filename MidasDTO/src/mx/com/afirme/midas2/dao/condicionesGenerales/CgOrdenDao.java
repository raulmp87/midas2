package mx.com.afirme.midas2.dao.condicionesGenerales;

import java.sql.SQLException;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgOrden;

@Local
public interface CgOrdenDao  extends Dao<Long, CgOrden> {

	List<CgOrden> findByFilter(CgOrden cgOrden) throws SQLException, Exception;
	List<CgOrden> findByFilterQuery( String usuario ) throws SQLException, Exception;
}
