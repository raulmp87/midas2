package mx.com.afirme.midas2.dao.condicionesGenerales;

import java.sql.SQLException;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgCentro;

@Local
public interface CgCentroDao  extends Dao<Long, CgCentro> {

	List<CgCentro> findByFilter(CgCentro cgCentro) throws SQLException, Exception;
}
