package mx.com.afirme.midas2.dao.condicionesGenerales;

import java.sql.BatchUpdateException;
import java.sql.SQLException;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgAgente;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgAgenteView;

@Local
public interface CgAgenteDao extends Dao<Long, CgAgente> {

	List<CgAgenteView> findByFilter(CgAgente cgAgente) throws SQLException, Exception;
	CgAgenteView findCgAgenteViewById(Long id) throws SQLException, Exception;
	CgAgente updateNew( CgAgente entity ) throws BatchUpdateException, Exception;
}
