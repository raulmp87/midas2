package mx.com.afirme.midas2.dao.condicionesGenerales;

import java.sql.SQLException;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgProveedor;

@Local
public interface CgProveedorDao extends Dao<Long, CgProveedor>{
	List<CgProveedor> findByName( String nameParam ) throws SQLException, Exception;
}
