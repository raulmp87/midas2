package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla;

public class ZonaCirculacion {
	private String estado;
	private String muncipio;
	private String codigoPostal;
	
	
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getMuncipio() {
		return muncipio;
	}
	public void setMuncipio(String muncipio) {
		this.muncipio = muncipio;
	}
	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	
	
}
