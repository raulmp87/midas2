package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.inciso.configuracion;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoInciso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoId;

@Local
public interface ConfiguracionDatoIncisoDao extends
		Dao<ConfiguracionDatoIncisoId, ConfiguracionDatoInciso> {

	public List<ConfiguracionDatoInciso> getDatosRamoInciso(BigDecimal idTcRamo,Short claveFiltroRIesgo);

	public List<ConfiguracionDatoInciso> getDatosSubRamoInciso(
			BigDecimal idTcRamo, BigDecimal idTcSubRamo,Short claveFiltroRIesgo);

	public List<ConfiguracionDatoInciso> getDatosCoberturaInciso(
			BigDecimal idTcRamo, BigDecimal idTcSubRamo,
			BigDecimal idToCobertura,Short claveFiltroRIesgo);
	
	public List<ConfiguracionDatoInciso> getConfiguracionDatosIncisoCotAuto(Short claveFiltroRIesgo);
}
