package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.cargaMasivaServicioPublico;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico.CargaMasivaServicioPublico;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico.CargaMasivaServicioPublicoDetalle;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.CargaMasivaSPDetalleDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.CargaMasivaServicioPublicoDTO;

@Local
public interface CargaMasivaServicioPublicoDao {

	/**
	 * Método de interfaz de DAO para la consulta de archivos de carga masiva almacenados
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @param {@link CargaMasivaServicioPublicoDTO} archivoAdjunto
	 * @return {@link CargaMasivaServicioPublico}
	 */
	public List<CargaMasivaServicioPublico> consultaArchivosCargaMasivaDao(CargaMasivaServicioPublicoDTO archivoAdjunto);
	
	/**
	 * Método de interfaz de DAO para verifiar si un MD5 ya existe en la tabla y en que estatus esta
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @param String md5CheckSum
	 * @return Lista de {@link CargaMasivaServicioPublico}
	 */
	public List<CargaMasivaServicioPublico> getMd5ArchivoDao(String md5CheckSum);
	
	/**
	 * Método de interfaz de DAO para almacenar el archivo en la base de datos
	 * @author SOFTNET - ISCJJBV 
	 *
	 * @param {@link CargaMasivaServicioPublico} archivoCargaMasiva
	 * @return {@link CargaMasivaServicioPublico}
	 */
	public CargaMasivaServicioPublico saveArchivoDao(CargaMasivaServicioPublico archivoCargaMasiva);
	
	/**
	 * Método de interfaz de DAO para almacenar el detalle del archivo en la base de datos
	 * @author SOFTNET - ISCJJBV 
	 *
	 * @param {@link CargaMasivaServicioPublicoDetalle} detalleArchivoCargaMasiva
	 * @return {@link CargaMasivaServicioPublicoDetalle}
	 */
	public CargaMasivaServicioPublicoDetalle saveDetalleArchivoDao(CargaMasivaServicioPublicoDetalle detalleArchivoCargaMasiva);
	
	/**
	 * Método de interfaz de DAO para contar los registros de la tabla de archivos
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @param {@link CargaMasivaServicioPublicoDTO} archivoAdjunto
	 * @return Long
	 */
	public Long conteoBusquedaArchivosDao(CargaMasivaServicioPublicoDTO  archivoAdjunto);
	
	/**
	 * Método de interfaz de DAO para actualizar los estatus de los registros de la tabla de detalle de archivos
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @param {@link CargaMasivaServicioPublicoDetalle} registroDetalle
	 */
	public void updateEstatusDetalleDao(CargaMasivaServicioPublicoDetalle  registroDetalle);
	
	/**
	 * Método de interfaz de DAO para actualizar los estatus de los archivos de la tabla de archivos
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @param {@link CargaMasivaServicioPublico} registroArchivo
	 */
	public void updateEstatusArchivoDao(CargaMasivaServicioPublico registroArchivo);
	
	/**
	 * Método de interfaz de DAO para hacer traducción de id's SEYCOS a id's MIDAS
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @param String tipoConversion
	 * @param String identificadorSeycos
	 * @return BigDecimal
	 */
	public BigDecimal obtieneConversionSeycosMidas(String tipoConversion, Long identificadorSeycos);

	
	/**
	 * Método de interfaz de DAO para contar los registros detalle
	 * @author SOFTNET - ISCJAM 
	 * 
	 */
	
	
	public Long contarDetalle(Long archivoId, Integer count,Integer posStart);
	
	/**
	 * Método de interfaz de DAO para busqueda de los registros detalle
	 * @author SOFTNET - ISCJAM
	 * 
	 */

	public List<CargaMasivaSPDetalleDTO> busquedaDetalle(
			Long archivoId, Integer count,Integer posStart);
 
	/**
	 * Metodo que valida en seycos si ya esta en uso el numero de poliza 
	 * @param numToPoliza
	 * @param idCentroEmisor
	 * @return
	 */
	public Boolean existePolizaSeycos(Long numToPoliza, Long idCentroEmisor);
	
	/**
	 * Invoca el procedure de seycos que calcula la fecha de fin de vigencia para crear la cotizacion
	 * @param numToPoliza
	 * @param idCentroEmisor
	 * @return
	 */
	public Date obtenerFinVigenciaPoliza(Date fechaInicioVigencia, Long vigencia);
}
