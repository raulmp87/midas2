package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws;

import java.io.Serializable;
import java.math.BigDecimal;

public class CotizacionCoberturaView implements Serializable {
	
	/**
	 * Objeto con los parámetros para obtener las coberturas
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long IdNegocio;
	private Long idProducto;
	private BigDecimal idToProducto;	
	private BigDecimal idToNegTipoPoliza;
	private BigDecimal idToTipoPoliza;
	private BigDecimal idTipoPoliza;
	private BigDecimal idLineaNegocio;
	private BigDecimal idToSeccion; 
	private Long idPaquete;
	private String idEstadoCirculacion;
	private String idMunicipioCirculacion;
	private Short idMoneda;
	private String estilo;
	private Long modelo;
	
	public Long getIdNegocio() {
		return IdNegocio;
	}
	public void setIdNegocio(Long idNegocio) {
		IdNegocio = idNegocio;
	}
	public Long getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}
	public BigDecimal getIdToProducto() {
		return idToProducto;
	}
	public void setIdToProducto(BigDecimal idToProducto) {
		this.idToProducto = idToProducto;
	}
	public BigDecimal getIdToNegTipoPoliza() {
		return idToNegTipoPoliza;
	}
	public void setIdToNegTipoPoliza(BigDecimal idToNegTipoPoliza) {
		this.idToNegTipoPoliza = idToNegTipoPoliza;
	}
	public BigDecimal getIdToTipoPoliza() {
		return idToTipoPoliza;
	}
	public void setIdToTipoPoliza(BigDecimal idToTipoPoliza) {
		this.idToTipoPoliza = idToTipoPoliza;
	}
	public BigDecimal getIdTipoPoliza() {
		return idTipoPoliza;
	}
	public void setIdTipoPoliza(BigDecimal idTipoPoliza) {
		this.idTipoPoliza = idTipoPoliza;
	}
	public BigDecimal getIdLineaNegocio() {
		return idLineaNegocio;
	}
	public void setIdLineaNegocio(BigDecimal idLineaNegocio) {
		this.idLineaNegocio = idLineaNegocio;
	}
	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}
	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}
	public Long getIdPaquete() {
		return idPaquete;
	}
	public void setIdPaquete(Long idPaquete) {
		this.idPaquete = idPaquete;
	}
	public String getIdEstadoCirculacion() {
		return idEstadoCirculacion;
	}
	public void setIdEstadoCirculacion(String idEstadoCirculacion) {
		this.idEstadoCirculacion = idEstadoCirculacion;
	}
	public String getIdMunicipioCirculacion() {
		return idMunicipioCirculacion;
	}
	public void setIdMunicipioCirculacion(String idMunicipioCirculacion) {
		this.idMunicipioCirculacion = idMunicipioCirculacion;
	}
	public Short getIdMoneda() {
		return idMoneda;
	}
	public void setIdMoneda(Short idMoneda) {
		this.idMoneda = idMoneda;
	}
	public String getEstilo() {
		return estilo;
	}
	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}
	public Long getModelo() {
		return modelo;
	}
	public void setModelo(Long modelo) {
		this.modelo = modelo;
	}
}
