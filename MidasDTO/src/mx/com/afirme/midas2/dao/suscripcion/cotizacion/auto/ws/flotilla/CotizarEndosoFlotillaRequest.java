package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla;

import java.util.Date;
import java.util.List;

public class CotizarEndosoFlotillaRequest extends CotizarEndosoFlotillaBajaIncisoRequest  {
	private short tipoEndoso;
	private Date fechaInicioVigencia;
	private List<Inciso> lstInciso;
	private String numeroPoliza;
	private boolean forzar;
	
	
	
	public short getTipoEndoso() {
		return tipoEndoso;
	}
	public void setTipoEndoso(short tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}
	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	public List<Inciso> getLstInciso() {
		return lstInciso;
	}
	public void setLstInciso(List<Inciso> lstInciso) {
		this.lstInciso = lstInciso;
	}
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public boolean isForzar() {
		return forzar;
	}
	public void setForzar(boolean forzar) {
		this.forzar = forzar;
	}
	

}
