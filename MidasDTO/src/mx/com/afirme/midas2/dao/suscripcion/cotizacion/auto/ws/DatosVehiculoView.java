package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws;

import java.io.Serializable;
import java.math.BigDecimal;

public class DatosVehiculoView implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal idLineaNegocio;
	private String numeroSerieVin;
	private BigDecimal idTipoUso;
	private BigDecimal idMarca;
	private BigDecimal modelo;
	private BigDecimal idEstilo;
	private String numeroMotor;
	
	public BigDecimal getIdLineaNegocio() {
		return idLineaNegocio;
	}
	public void setIdLineaNegocio(BigDecimal idLineaNegocio) {
		this.idLineaNegocio = idLineaNegocio;
	}
	public String getNumeroSerieVin() {
		return numeroSerieVin;
	}
	public void setNumeroSerieVin(String numeroSerieVin) {
		this.numeroSerieVin = numeroSerieVin;
	}
	public BigDecimal getIdTipoUso() {
		return idTipoUso;
	}
	public void setIdTipoUso(BigDecimal idTipoUso) {
		this.idTipoUso = idTipoUso;
	}
	public BigDecimal getIdMarca() {
		return idMarca;
	}
	public void setIdMarca(BigDecimal idMarca) {
		this.idMarca = idMarca;
	}
	public BigDecimal getModelo() {
		return modelo;
	}
	public void setModelo(BigDecimal modelo) {
		this.modelo = modelo;
	}
	public BigDecimal getIdEstilo() {
		return idEstilo;
	}
	public void setIdEstilo(BigDecimal idEstilo) {
		this.idEstilo = idEstilo;
	}
	public String getNumeroMotor() {
		return numeroMotor;
	}
	public void setNumeroMotor(String numeroMotor) {
		this.numeroMotor = numeroMotor;
	}
	@Override
	public String toString() {
		return "DatosVehiculoView [idLineaNegocio=" + idLineaNegocio
				+ ", numeroSerieVin=" + numeroSerieVin + ", idTipoUso="
				+ idTipoUso + ", idMarca=" + idMarca + ", modelo=" + modelo
				+ ", idEstilo=" + idEstilo + ", numeroMotor=" + numeroMotor
				+ "]";
	}
	
}
