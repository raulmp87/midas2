package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla;

import java.math.BigDecimal;

public class Inciso {
	
	private ZonaCirculacion zonaCirculacion;
	private InformacionDelVehiculo informacionDelVehiculo;
	private DetalleDeCoberturas detalleDeCoberturas;
	private String paquete;
	
	private Integer numeroInciso;
	private BigDecimal primaNeta;
	private BigDecimal primaTotal;
	private Short motivoEndoso;
	
	
	
	
	
	public ZonaCirculacion getZonaCirculacion() {
		return zonaCirculacion;
	}
	public void setZonaCirculacion(ZonaCirculacion zonaCirculacion) {
		this.zonaCirculacion = zonaCirculacion;
	}
	public InformacionDelVehiculo getInformacionDelVehiculo() {
		return informacionDelVehiculo;
	}
	public void setInformacionDelVehiculo(InformacionDelVehiculo informacionDelVehiculo) {
		this.informacionDelVehiculo = informacionDelVehiculo;
	}
	public DetalleDeCoberturas getDetalleDeCoberturas() {
		return detalleDeCoberturas;
	}
	public void setDetalleDeCoberturas(DetalleDeCoberturas detalleDeCoberturas) {
		this.detalleDeCoberturas = detalleDeCoberturas;
	}
	public String getPaquete() {
		return paquete;
	}
	public void setPaquete(String paquete) {
		this.paquete = paquete;
	}
	public Integer getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public BigDecimal getPrimaNeta() {
		return primaNeta;
	}
	public void setPrimaNeta(BigDecimal primaNeta) {
		this.primaNeta = primaNeta;
	}
	public BigDecimal getPrimaTotal() {
		return primaTotal;
	}
	public void setPrimaTotal(BigDecimal primaTotal) {
		this.primaTotal = primaTotal;
	}
	public Short getMotivoEndoso() {
		return motivoEndoso;
	}
	public void setMotivoEndoso(Short motivoEndoso) {
		this.motivoEndoso = motivoEndoso;
	}
}
