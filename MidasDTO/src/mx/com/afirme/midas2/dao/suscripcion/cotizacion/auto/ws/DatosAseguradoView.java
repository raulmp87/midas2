package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws;

/**
 * Se utiliza para las opciones de los datos del Asegurado
 * en el Endoso de cambio de datos por WS.
 * 
 * Posibles valores para el campo opcionAsegurado:
 * 1) Los datos del asegurado son iguales a los del contratante
 * 2) Asignar solo el nombre. Si se asigna este valor se deberá asignar
 * también nombreAsegurado.
 * 3) Asignar un cliente ya existente. En esta opcion se debe enviar
 * el clienteAseguradoId tambien. Para ello se utiliza el servicio por WS
 * buscarCliente.
 * 
 * NOTA. Las opciones corresponden a los valores de Complementar Datos Asegurado
 * en el Endoso de cambio de datos de la parte web
 * 
 * @author jochoa
 *
 */
public class DatosAseguradoView {
	
	int opcionAsegurado;
	String nombreAsegurado;
	Long clienteAseguradoId;
	Long clienteContratanteId;
	
	public int getOpcionAsegurado() {
		return opcionAsegurado;
	}
	public void setOpcionAsegurado(int opcionAsegurado) {
		this.opcionAsegurado = opcionAsegurado;
	}
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}
	public Long getClienteAseguradoId() {
		return clienteAseguradoId;
	}
	public void setClienteAseguradoId(Long clienteAseguradoId) {
		this.clienteAseguradoId = clienteAseguradoId;
	}
	public Long getClienteContratanteId() {
		return clienteContratanteId;
	}
	public void setClienteContratanteId(Long clienteContratanteId) {
		this.clienteContratanteId = clienteContratanteId;
	}
}
