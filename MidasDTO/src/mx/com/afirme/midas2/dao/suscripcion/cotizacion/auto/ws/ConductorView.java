package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws;

import java.util.Date;

public class ConductorView {
	private String nombreConductor;
	private String apellidoPaternoConductor;
	private String apellidoMaternoConductor;
	private String fechaNacimiento;
	private String numeroLicencia;
	private String ocupacion;
	
	public String getNombreConductor() {
		return nombreConductor;
	}
	public void setNombreConductor(String nombreConductor) {
		this.nombreConductor = nombreConductor;
	}
	public String getApellidoPaternoConductor() {
		return apellidoPaternoConductor;
	}
	public void setApellidoPaternoConductor(String apellidoPaternoConductor) {
		this.apellidoPaternoConductor = apellidoPaternoConductor;
	}
	public String getApellidoMaternoConductor() {
		return apellidoMaternoConductor;
	}
	public void setApellidoMaternoConductor(String apellidoMaternoConductor) {
		this.apellidoMaternoConductor = apellidoMaternoConductor;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getNumeroLicencia() {
		return numeroLicencia;
	}
	public void setNumeroLicencia(String numeroLicencia) {
		this.numeroLicencia = numeroLicencia;
	}
	public String getOcupacion() {
		return ocupacion;
	}
	public void setOcupacion(String ocupacion) {
		this.ocupacion = ocupacion;
	}
}
