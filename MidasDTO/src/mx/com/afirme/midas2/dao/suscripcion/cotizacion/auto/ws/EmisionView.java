package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws;

import java.math.BigDecimal;

import mx.com.afirme.midas2.dto.endoso.ConductoCobroDTO;

public class EmisionView {
	private ParametrosAutoIncisoView autoInciso;
	private DatosPaqueteView paquete;
	private ConductoCobroDTO conductoCobro;
	private DatosAseguradoView asegurado;
	private String observaciones;
	private BigDecimal idCotizacion;
	private boolean documentacionCompleta;
	private ContratanteView contratante;
	private boolean actualizarContratante;
	private boolean actualizarConductor;
	
	public ParametrosAutoIncisoView getAutoInciso() {
		return autoInciso;
	}
	
	public void setAutoInciso(ParametrosAutoIncisoView autoInciso) {
		this.autoInciso = autoInciso;
	}
	
	public DatosPaqueteView getPaquete() {
		return paquete;
	}
	
	public void setPaquete(DatosPaqueteView paquete) {
		this.paquete = paquete;
	}
	
	public ConductoCobroDTO getConductoCobro() {
		return conductoCobro;
	}
	
	public void setConductoCobro(ConductoCobroDTO conductoCobro) {
		this.conductoCobro = conductoCobro;
	}
	
	public DatosAseguradoView getAsegurado() {
		return asegurado;
	}
	
	public void setAsegurado(DatosAseguradoView asegurado) {
		this.asegurado = asegurado;
	}
	
	public String getObservaciones() {
		return observaciones;
	}
	
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	public BigDecimal getIdCotizacion() {
		return idCotizacion;
	}
	
	public void setIdCotizacion(BigDecimal idCotizacion) {
		this.idCotizacion = idCotizacion;
	}
	
	public boolean isDocumentacionCompleta() {
		return documentacionCompleta;
	}
	
	public void setDocumentacionCompleta(boolean documentacionCompleta) {
		this.documentacionCompleta = documentacionCompleta;
	}
	
	public ContratanteView getContratante() {
		return contratante;
	}
	
	public void setContratante(ContratanteView contratante) {
		this.contratante = contratante;
	}

	public boolean isActualizarContratante() {
		return actualizarContratante;
	}

	public void setActualizarContratante(boolean actualizarContratante) {
		this.actualizarContratante = actualizarContratante;
	}

	public boolean isActualizarConductor() {
		return actualizarConductor;
	}

	public void setActualizarConductor(boolean actualizarConductor) {
		this.actualizarConductor = actualizarConductor;
	}
}