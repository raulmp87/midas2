package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla;

public class EmitirEndosoFlotillaBajaIncisoRequest {
	
	private Short  motivoEndoso;

	public Short getMotivoEndoso() {
		return motivoEndoso;
	}

	public void setMotivoEndoso(Short motivoEndoso) {
		this.motivoEndoso = motivoEndoso;
	}

}
