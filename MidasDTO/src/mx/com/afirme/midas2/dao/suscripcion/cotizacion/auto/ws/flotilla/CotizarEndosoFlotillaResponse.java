package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla;

import java.util.List;

import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasiva.LogErroresCargaMasivaDTO;

public class CotizarEndosoFlotillaResponse {
	
	private String numeroPoliza;
	private List<LogErroresCargaMasivaDTO> listErrores;
	private String estatus;
	private String mensaje;
	private ResumenCostosDTO resumenCostos;
	private List<Inciso> lstInciso;
	
	
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public List<LogErroresCargaMasivaDTO> getListErrores() {
		return listErrores;
	}
	public void setListErrores(List<LogErroresCargaMasivaDTO> listErrores) {
		this.listErrores = listErrores;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public ResumenCostosDTO getResumenCostos() {
		return resumenCostos;
	}
	public void setResumenCostos(ResumenCostosDTO resumenCostos) {
		this.resumenCostos = resumenCostos;
	}
	public List<Inciso> getLstInciso() {
		return lstInciso;
	}
	public void setLstInciso(List<Inciso> lstInciso) {
		this.lstInciso = lstInciso;
	}
	
	
}
