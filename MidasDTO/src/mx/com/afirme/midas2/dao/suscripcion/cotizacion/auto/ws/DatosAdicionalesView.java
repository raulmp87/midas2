package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws;

import java.io.Serializable;
/*
 * Entidad creada para el metodo cotizacion de AutoPlazo que recibe un String(Object Json)
 * */
public class DatosAdicionalesView implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Integer idSucursal;
	private String claveUsuario;
	private Double valorFactura;
	Boolean generaCotizacion;
	
	public Integer getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(Integer idSucursal) {
		this.idSucursal = idSucursal;
	}
	public String getClaveUsuario() {
		return claveUsuario;
	}
	public void setClaveUsuario(String claveUsuario) {
		this.claveUsuario = claveUsuario;
	}
	public Double getValorFactura() {
		return valorFactura;
	}
	public void setValorFactura(Double valorFactura) {
		this.valorFactura = valorFactura;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Boolean getGeneraCotizacion() {
		return generaCotizacion;
	}
	public void setGeneraCotizacion(Boolean generaCotizacion) {
		this.generaCotizacion = generaCotizacion;
	}
	@Override
	public String toString() {
		return "DatosAdicionalesView [idSucursal=" + idSucursal
				+ ", claveUsuario=" + claveUsuario + ", valorFactura="
				+ valorFactura + ", generaCotizacion=" + generaCotizacion + "]";
	}
}
