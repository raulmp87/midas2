package mx.com.afirme.midas2.dao.suscripcion.pago;

import java.math.BigDecimal;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.excepcion.ExcepcionSuscripcion;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO;
import mx.com.afirme.midas2.dto.pago.FolioReciboDTO;

//TODO CAMBIAR ExcepcionSuscripcion
@Local
public interface PolizaPagoDao extends Dao<Long, ExcepcionSuscripcion> {
	
	
	/**
	 * Obtener un recibo que contiene la informaci�n del n�mero de recibo y el digito verificador 
	 * generado por seycos para ser utilizado posteriormente en el cobro y emision
	 * @return
	 */
	public FolioReciboDTO obtenerInformacionPrimerRecibo() throws Exception;
	
	/**
	 * Obtener 
	 * @param idToCotizacion
	 * @param idToSeccion
	 * @param idSeccion
	 * @return
	 */
	public CuentaPagoDTO obtenerInformacionConductoCobro(BigDecimal idToCotizacion, BigDecimal idToSeccion, BigDecimal idSeccion);

}
