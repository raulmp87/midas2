package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class DatosPolizaView implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ProspectoView prospecto;
	private Date inicioVigencia;
	private Date finVigencia;
	private String inicioVigenciaPoliza;
	private String finVigenciaPoliza;
	private Long idNegocio;
	private BigDecimal idProducto; //idToNegProducto
	private BigDecimal idTipoPoliza;
	private Double porcentajePagoFraccionado;
	
	public ProspectoView getProspecto() {
		return prospecto;
	}
	public void setProspecto(ProspectoView prospecto) {
		this.prospecto = prospecto;
	}
	public Date getInicioVigencia() {
		return inicioVigencia;
	}
	public void setInicioVigencia(Date inicioVigencia) {
		this.inicioVigencia = inicioVigencia;
	}
	public Date getFinVigencia() {
		return finVigencia;
	}
	public void setFinVigencia(Date finVigencia) {
		this.finVigencia = finVigencia;
	}
	public Long getIdNegocio() {
		return idNegocio;
	}
	public void setIdNegocio(Long idNegocio) {
		this.idNegocio = idNegocio;
	}
	public BigDecimal getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(BigDecimal idProducto) {
		this.idProducto = idProducto;
	}
	public BigDecimal getIdTipoPoliza() {
		return idTipoPoliza;
	}
	public void setIdTipoPoliza(BigDecimal idTipoPoliza) {
		this.idTipoPoliza = idTipoPoliza;
	}
	public Double getPorcentajePagoFraccionado() {
		return porcentajePagoFraccionado;
	}
	public void setPorcentajePagoFraccionado(Double porcentajePagoFraccionado) {
		this.porcentajePagoFraccionado = porcentajePagoFraccionado;
	}
	public String getInicioVigenciaPoliza() {
		return inicioVigenciaPoliza;
	}
	public void setInicioVigenciaPoliza(String inicioVigenciaPoliza) {
		this.inicioVigenciaPoliza = inicioVigenciaPoliza;
	}
	public String getFinVigenciaPoliza() {
		return finVigenciaPoliza;
	}
	public void setFinVigenciaPoliza(String finVigenciaPoliza) {
		this.finVigenciaPoliza = finVigenciaPoliza;
	}
	
	@Override
	public String toString() {
		return "DatosPolizaView [prospecto=" + prospecto + ", inicioVigencia="
				+ inicioVigencia + ", finVigencia=" + finVigencia
				+ ", idNegocio=" + idNegocio + ", idProducto=" + idProducto
				+ ", idTipoPoliza=" + idTipoPoliza
				+ ", porcentajePagoFraccionado=" + porcentajePagoFraccionado
				+ "]";
	}

}
