package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws;

import java.io.Serializable;

/**
 * 
 * 
 *
 */
public class DatosPaqueteView implements Serializable{

		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String adicionales;
	private Long idPaquete;
	private int idDerecho;
	private int idFormaPago;
	private int comision;
	private int porcentajeIva;
	private double pctDescuentoEstado;
	private double pctePagoFraccionado;
	private String observaciones;
	
	
	public String getAdicionales() {
		return adicionales;
	}
	public void setAdicionales(String adicionales) {
		this.adicionales = adicionales;
	}
	public Long getIdPaquete() {
		return idPaquete;
	}
	public void setIdPaquete(Long idPaquete) {
		this.idPaquete = idPaquete;
	}
	public int getIdDerecho() {
		return idDerecho;
	}
	public void setIdDerecho(int idDerecho) {
		this.idDerecho = idDerecho;
	}
	public int getIdFormaPago() {
		return idFormaPago;
	}
	public void setIdFormaPago(int idFormaPago) {
		this.idFormaPago = idFormaPago;
	}
	public int getComision() {
		return comision;
	}
	public void setComision(int comision) {
		this.comision = comision;
	}
	public int getPorcentajeIva() {
		return porcentajeIva;
	}
	public void setPorcentajeIva(int porcentajeIva) {
		this.porcentajeIva = porcentajeIva;
	}
	public double getPctDescuentoEstado() {
		return pctDescuentoEstado;
	}
	public void setPctDescuentoEstado(double pctDescuentoEstado) {
		this.pctDescuentoEstado = pctDescuentoEstado;
	}
	public double getPctePagoFraccionado() {
		return pctePagoFraccionado;
	}
	public void setPctePagoFraccionado(double pctePagoFraccionado) {
		this.pctePagoFraccionado = pctePagoFraccionado;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	@Override
	public String toString() {
		return "PaqueteTransporte [adicionales=" + adicionales + ", idPaquete="
				+ idPaquete + ", idDerecho=" + idDerecho + ", idFormaPago="
				+ idFormaPago + ", comision=" + comision + ", porcentajeIva="
				+ porcentajeIva + ", pctDescuentoEstado=" + pctDescuentoEstado
				+ ", observaciones=" + observaciones + "]";
	}
	

}
