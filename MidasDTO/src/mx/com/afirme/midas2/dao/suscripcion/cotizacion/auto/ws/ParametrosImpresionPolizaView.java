package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Creado para la impresión de póliza por webservice para saber que elementos de
 * la póliza imprimir
 * 
 * @author rmonper
 *
 */
public class ParametrosImpresionPolizaView {

	private boolean caratula;
	private boolean recibo;
	private boolean todosLosIncisos;
	private int[] rangoDeIncisos;
	private boolean certificadoNUPorInciso;
	private boolean anexos;
	private boolean condicionesEspeciales;
	private boolean situacionActual;
	private Date fechaValidez;
	private String numeroPoliza;
	private BigDecimal idPoliza;
	private boolean incluirReferenciasBancarias;
	private boolean aviso;
	private boolean derechos;
	private String noOrder;

	public ParametrosImpresionPolizaView() {
		this.caratula = true;
		this.recibo = true;
		this.todosLosIncisos = true;
		this.certificadoNUPorInciso = true;
		this.aviso = false;
		this.derechos = false;
	}

	public boolean getCaratula() {
		return caratula;
	}

	public void setCaratula(boolean caratula) {
		this.caratula = caratula;
	}

	public boolean getRecibo() {
		return recibo;
	}

	public void setRecibo(boolean recibo) {
		this.recibo = recibo;
	}

	public boolean getTodosLosIncisos() {
		return todosLosIncisos;
	}

	public void setTodosLosIncisos(boolean todosLosIncisos) {
		this.todosLosIncisos = todosLosIncisos;
	}

	public int[] getRangoDeIncisos() {
		return rangoDeIncisos;
	}

	public void setRangoDeIncisos(int[] rangoDeIncisos) {
		this.rangoDeIncisos = rangoDeIncisos;
	}

	public boolean getCertificadoNUPorInciso() {
		return certificadoNUPorInciso;
	}

	public void setCertificadoNUPorInciso(boolean certificadoNUPorInciso) {
		this.certificadoNUPorInciso = certificadoNUPorInciso;
	}

	public boolean getAnexos() {
		return anexos;
	}

	public void setAnexos(boolean anexos) {
		this.anexos = anexos;
	}

	public boolean getCondicionesEspeciales() {
		return condicionesEspeciales;
	}

	public void setCondicionesEspeciales(boolean condicionesEspeciales) {
		this.condicionesEspeciales = condicionesEspeciales;
	}

	public boolean getSituacionActual() {
		return situacionActual;
	}

	public void setSituacionActual(boolean situacionActual) {
		this.situacionActual = situacionActual;
	}

	public Date getFechaValidez() {
		return fechaValidez;
	}

	public void setFechaValidez(Date fechaValidez) {
		this.fechaValidez = fechaValidez;
	}

	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	public BigDecimal getIdPoliza() {
		return idPoliza;
	}

	public void setIdPoliza(BigDecimal idPoliza) {
		this.idPoliza = idPoliza;
	}

	public boolean isIncluirReferenciasBancarias() {
		return incluirReferenciasBancarias;
	}

	public void setIncluirReferenciasBancarias(boolean incluirReferenciasBancarias) {
		this.incluirReferenciasBancarias = incluirReferenciasBancarias;
	}

	public boolean isAviso() {
		return aviso;
	}

	public void setAviso(boolean aviso) {
		this.aviso = aviso;
	}

	public boolean isDerechos() {
		return derechos;
	}

	public void setDerechos(boolean derechos) {
		this.derechos = derechos;
	}

	public String getNoOrder() {
		return noOrder;
	}

	public void setNoOrder(String noOrder) {
		this.noOrder = noOrder;
	}
	
	

}
