package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws;

import java.io.Serializable;
import java.math.BigDecimal;

public class VehiculoView implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal idToNegSeccion;
	private String numeroSerie;
	private BigDecimal idTipoUso;
	private BigDecimal idMarca;
	private BigDecimal modelo;
	private BigDecimal idEstilo;
	
	
	public BigDecimal getIdToNegSeccion() {
		return idToNegSeccion;
	}
	public void setIdToNegSeccion(BigDecimal idToNegSeccion) {
		this.idToNegSeccion = idToNegSeccion;
	}
	public String getNumeroSerie() {
		return numeroSerie;
	}
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	public BigDecimal getIdTipoUso() {
		return idTipoUso;
	}
	public void setIdTipoUso(BigDecimal idTipoUso) {
		this.idTipoUso = idTipoUso;
	}
	public BigDecimal getIdMarca() {
		return idMarca;
	}
	public void setIdMarca(BigDecimal idMarca) {
		this.idMarca = idMarca;
	}
	public BigDecimal getModelo() {
		return modelo;
	}
	public void setModelo(BigDecimal modelo) {
		this.modelo = modelo;
	}
	public BigDecimal getIdEstilo() {
		return idEstilo;
	}
	public void setIdEstilo(BigDecimal idEstilo) {
		this.idEstilo = idEstilo;
	}
	@Override
	public String toString() {
		return "VehiculoView [idToNegSeccion=" + idToNegSeccion
				+ ", numeroSerie=" + numeroSerie + ", idTipoUso=" + idTipoUso
				+ ", idMarca=" + idMarca + ", modelo=" + modelo + ", idEstilo="
				+ idEstilo + "]";
	}
}
