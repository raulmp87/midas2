package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla;

import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;

public class EmitirEndosoFlotillaResponse {

	private String estatus;
	private String mensaje;
	private List<ExcepcionSuscripcionReporteDTO> exepcionesList;
	private Short numeroEndoso;
	
	
	
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public List<ExcepcionSuscripcionReporteDTO> getExepcionesList() {
		return exepcionesList;
	}
	public void setExepcionesList(List<ExcepcionSuscripcionReporteDTO> exepcionesList) {
		this.exepcionesList = exepcionesList;
	}
	public Short getNumeroEndoso() {
		return numeroEndoso;
	}
	public void setNumeroEndoso(Short numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}
}
