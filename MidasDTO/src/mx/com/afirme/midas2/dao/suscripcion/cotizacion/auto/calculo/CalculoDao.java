package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.calculo;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.ContenedorPrimas;

@Local
public interface CalculoDao{
	/**
	 * Metodo que permite calcular  a las condiciones que se encuentran almacenadas
	 * para una cobertura que le corresponden a un inciso
	 * de acuerdo a una cotizacion, moneda, negocio,
	 * producto, numero de inciso, seccion, agrupador tarifa, version del agrupador, 
	 * version tarifa(version de carga),estilo, modelo, zona circulacion, tipo de uso,
	 * tipo de servicio, paquete, modificadores de prima, modificadores de descripcio,
	 * la cobertura a calcular.
	 * 
	 * @param idCotizacion
	 * @param idMoneda
	 * @param IdNegocio
	 * @param idToProducto
	 * @param numeroInciso
	 * @param idToSeccion
	 * @param idToAgrupadorTarifa
	 * @param idVerAgrupadorTarifa
	 * @param idVertarifa
	 * @param claveEstilo
	 * @param modeloVehiculo
	 * @param claveZonaCircualcion
	 * @param tipoUso
	 * @param tipoServicio
	 * @param idPaquete
	 * @param modificadoresPrima
	 * @param modificadoresDescripcion
	 * @param cobertura
	 * @return CoberturaCotizacionDTO
	 */
	CoberturaCotizacionDTO executeStoreProcedure(BigDecimal idCotizacion, Short idMoneda, Long IdNegocio, BigDecimal idToProducto,
			Long numeroInciso, BigDecimal idToSeccion, Long idToAgrupadorTarifa, Long idVerAgrupadorTarifa, Long idVertarifa,
			String claveEstilo, Long modeloVehiculo, String claveZonaCircualcion, BigDecimal tipoUso, BigDecimal tipoServicio,
			Long idPaquete,String modificadoresPrima, String modificadoresDescripcion,String claveFuenteSuemaAsegurada, int diasVigencia,  
			CoberturaCotizacionDTO cobertura);
	
	/**
	 * 
	 * Metodo que permite calcular  a las condiciones que se encuentran almacenadas
	 * para una cobertura que le corresponden a un inciso
	 * de acuerdo a una cotizacion, moneda, negocio,
	 * producto, numero de inciso, seccion, agrupador tarifa, version del agrupador, 
	 * version tarifa(version de carga),estilo, modelo, zona circulacion, tipo de uso,
	 * tipo de servicio, paquete, modificadores de prima, modificadores de descripcio,
	 * la cobertura a calcular.
	 * 
	 * @param idCotizacionB
	 * @param idIncisoB
	 * @param idIncisoSeccionB
	 * @param idCoberturaSeccionB
	 * @param diasVigencia
	 * @param idMoneda
	 * @param IdNegocio
	 * @param idToProducto
	 * @param numeroInciso
	 * @param idToSeccion
	 * @param idToAgrupadorTarifa
	 * @param idVerAgrupadorTarifa
	 * @param idVertarifa
	 * @param claveEstilo
	 * @param modeloVehiculo
	 * @param claveZonaCircualcion
	 * @param tipoUso
	 * @param tipoServicio
	 * @param idPaquete
	 * @param modificadoresPrima
	 * @param modificadoresDescripcion
	 * @param claveFuenteSuemaAsegurada
	 * @param cobertura
	 * @return BitemporalCoberturaSeccion
	 */
	public BitemporalCoberturaSeccion calculaCoberturaBitemporal(BigDecimal idCotizacionB, BigDecimal idIncisoB, BigDecimal idIncisoSeccionB, 
			BigDecimal idCoberturaSeccionB, Integer diasVigencia, Short idMoneda, Long IdNegocio, BigDecimal idToProducto,
			Long numeroInciso, BigDecimal idToSeccion, Long idToAgrupadorTarifa, Long idVerAgrupadorTarifa, Long idVertarifa,
			String claveEstilo, Long modeloVehiculo, String claveZonaCircualcion, BigDecimal tipoUso, BigDecimal tipoServicio,
			Long idPaquete,String modificadoresPrima, String modificadoresDescripcion, String claveFuenteSuemaAsegurada, BitemporalCoberturaSeccion cobertura);
	
	
	/**
	 * Metodo que permite calcular la zona de circulacion dado 
	 * un estado y un municipio
	 * IdSeccion define si es autoplazo o no
	 * 
	 * @param idEstado
	 * @param idMunicipio
	 * @return String
	 */
	String getZonaCirculacion(BigDecimal IdSeccion, Integer idEstado, Integer idMunicipio, Long idToNegocio);
	
	
	/**
	 * Metodo que permite obtener el total de Siniestros
	 * 
	 * @param idToPoliza
	 * @return poliza con numeroSiniestros
	 */
	public List<PolizaDTO> getCantidadSiniestro(BigDecimal idToPoliza);
	
	/**
	 * Metodo que permite obtener la configuracion inicial del descuento maximo para 
	 * un estado
	 * 
	 * @param idEstado
	 * @return Double
	 */
	Double getDescuentoMaximoPorEstado(Integer idEstado);
	
	public ContenedorPrimas getPrimaNetaCoberturas(BigDecimal idToCotizacion, BigDecimal numeroInciso);
	
	public ResumenCostosDTO resumenCostosEmitidos(BigDecimal idToPoliza, Short numeroEndoso, Integer numeroInciso);
	
	public void ajusteIgualacionCotizacion(BigDecimal idToCotizacion, BigDecimal numeroInciso, Double primaTotal);
}
