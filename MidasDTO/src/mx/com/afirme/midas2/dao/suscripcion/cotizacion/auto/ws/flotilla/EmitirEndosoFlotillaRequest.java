package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla;

import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoInciso;


public class EmitirEndosoFlotillaRequest extends EmitirEndosoFlotillaBajaIncisoRequest {
	private Long idCotizacion;
	private List<AutoInciso> lstInciso;
	private Date fechaInicioVigencia;
	private String numeroPoliza;
	private List<Inciso> lstIncisoCot;
	private short tipoEndoso;
	
	public Long getIdCotizacion() {
		return idCotizacion;
	}
	public void setIdCotizacion(Long idCotizacion) {
		this.idCotizacion = idCotizacion;
	}
	public List<AutoInciso> getLstInciso() {
		return lstInciso;
	}
	public void setLstInciso(List<AutoInciso> lstInciso) {
		this.lstInciso = lstInciso;
	}
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}
	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public List<Inciso> getLstIncisoCot() {
		return lstIncisoCot;
	}
	public void setLstIncisoCot(List<Inciso> lstIncisoCot) {
		this.lstIncisoCot = lstIncisoCot;
	}
	public short getTipoEndoso() {
		return tipoEndoso;
	}
	public void setTipoEndoso(short tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}
	
}
