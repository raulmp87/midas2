package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.cambiosglobales;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.IncisoNumeroSerieDTO;

@Local
public interface IncisoAutoDao extends Dao<Long, IncisoAutoCot>{
	public List<Paquete> getPaquete(BigDecimal idToCotizacion);
	
	public List<IncisoNumeroSerieDTO> numerosSerieRepetidos(BigDecimal idToCotizacion, BigDecimal numeroInciso, String numeroSerie,
			Date fechaIniVigencia, Date fechaFinVigencia) throws Exception;
	
	public List<IncisoNumeroSerieDTO> numerosSerieRepetidosPoliza(BigDecimal idToCotizacion, BigDecimal numeroInciso, String numeroSerie) throws Exception;
}
