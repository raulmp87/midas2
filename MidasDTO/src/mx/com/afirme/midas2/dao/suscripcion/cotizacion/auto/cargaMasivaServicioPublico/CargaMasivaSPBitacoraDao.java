package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.cargaMasivaServicioPublico;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.BitacoraCargaMasivaSPFiltradoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.BitacoraCargaMasivaSPResultadoDTO;

@Local
public interface CargaMasivaSPBitacoraDao {

	public Long contarBitacora(BitacoraCargaMasivaSPFiltradoDTO filtroBitacora);

	public List<BitacoraCargaMasivaSPResultadoDTO> buscarBitacora(
			BitacoraCargaMasivaSPFiltradoDTO filtroBitacora);

}
