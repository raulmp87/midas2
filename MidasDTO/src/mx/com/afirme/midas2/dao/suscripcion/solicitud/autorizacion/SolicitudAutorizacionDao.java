package mx.com.afirme.midas2.dao.suscripcion.solicitud.autorizacion;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.autorizacion.SolicitudExcepcionCotizacion;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.autorizacion.SolicitudExcepcionDetalle;

@Local
public interface SolicitudAutorizacionDao extends Dao<Long, SolicitudExcepcionCotizacion>{

	public List<SolicitudExcepcionCotizacion> listarSolicitudes(
			SolicitudExcepcionCotizacion filtro, boolean listarHistorico);
	
	public List<SolicitudExcepcionDetalle> listarDetalleSolicitudesPorInciso(
			SolicitudExcepcionCotizacion filtro, boolean listarHistorico, BigDecimal idInciso);
	
	public List<SolicitudExcepcionCotizacion> listarSolicitudes(SolicitudExcepcionCotizacion filtro);
	
	public List<SolicitudExcepcionDetalle> listarDetalle(SolicitudExcepcionDetalle filtro);
	
	public List<SolicitudExcepcionCotizacion> listarSolicitudesEstatusInvalido(BigDecimal idToCotizacion);
	
	public List<SolicitudExcepcionDetalle> listarDetalle(Long id);
	
	public Long listarSolicitudesCount(SolicitudExcepcionCotizacion filtro, boolean listarHistorico);
}
