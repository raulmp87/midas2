package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.cobertura;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;

@Local
public interface CoberturaDao {

	public List<CoberturaCotizacionDTO> listarCoberturasSeccionCotizacion(
			SeccionCotizacionDTOId id, boolean soloContratadas);
	
	
	public List<CoberturaCotizacionDTO> listarCoberturasCotizacion(BigDecimal idToCotizacion, boolean soloContratadas);
	
	public List<CoberturaCotizacionDTO> listarCoberturasCotizacionDistinct(
			BigDecimal idToCotizacion, boolean soloContratadas); 
	
	public List<CoberturaCotizacionDTO> listarCoberturasSeccionCotizacionPrimaCero(
			BigDecimal idToCotizacion, boolean soloContratadas);

}
