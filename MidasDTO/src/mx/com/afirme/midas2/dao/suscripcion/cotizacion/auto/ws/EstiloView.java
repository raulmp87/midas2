package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws;

import java.math.BigDecimal;

public class EstiloView {
	
	private BigDecimal idMarca;
	private Short modelo;
	private BigDecimal idLineaNegocio; 
	private String descripcion;
	
	public BigDecimal getIdMarca() {
		return idMarca;
	}
	public void setIdMarca(BigDecimal idMarca) {
		this.idMarca = idMarca;
	}
	public Short getModelo() {
		return modelo;
	}
	public void setModelo(Short modelo) {
		this.modelo = modelo;
	}
	public BigDecimal getIdLineaNegocio() {
		return idLineaNegocio;
	}
	public void setIdLineaNegocio(BigDecimal idLineaNegocio) {
		this.idLineaNegocio = idLineaNegocio;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}	
	
}
