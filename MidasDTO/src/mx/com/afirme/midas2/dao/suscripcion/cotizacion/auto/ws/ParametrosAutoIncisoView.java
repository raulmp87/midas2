package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws;

/**
 * Lleva los parametros de entrada (datos complementarios del vehiculo)
 * para el endoso de cambio de datos por webservice
 * 
 * 
 * @author jochoa
 *
 */
public class ParametrosAutoIncisoView {

	private ConductorView conductor;
	
	private String numeroMotor;
	private String numeroSerie;
	private String placa;
	private String descripcionFinal;
	private String estiloId;
	private String repuve;
	private String emailContacto;
	private String rutaCirculacion;
	private String observacionesInciso;	
	
	private String nombreAsegurado;

	public ConductorView getConductor() {
		return conductor;
	}

	public void setConductor(ConductorView conductor) {
		this.conductor = conductor;
	}

	public String getNumeroMotor() {
		return numeroMotor;
	}

	public void setNumeroMotor(String numeroMotor) {
		this.numeroMotor = numeroMotor;
	}

	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getDescripcionFinal() {
		return descripcionFinal;
	}

	public void setDescripcionFinal(String descripcionFinal) {
		this.descripcionFinal = descripcionFinal;
	}

	public String getEstiloId() {
		return estiloId;
	}

	public void setEstiloId(String estiloId) {
		this.estiloId = estiloId;
	}

	public String getRepuve() {
		return repuve;
	}

	public void setRepuve(String repuve) {
		this.repuve = repuve;
	}

	public String getEmailContacto() {
		return emailContacto;
	}

	public void setEmailContacto(String emailContacto) {
		this.emailContacto = emailContacto;
	}

	public String getRutaCirculacion() {
		return rutaCirculacion;
	}

	public void setRutaCirculacion(String rutaCirculacion) {
		this.rutaCirculacion = rutaCirculacion;
	}

	public String getObservacionesInciso() {
		return observacionesInciso;
	}

	public void setObservacionesInciso(String observacionesInciso) {
		this.observacionesInciso = observacionesInciso;
	}

	public String getNombreAsegurado() {
		return nombreAsegurado;
	}

	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}
}
