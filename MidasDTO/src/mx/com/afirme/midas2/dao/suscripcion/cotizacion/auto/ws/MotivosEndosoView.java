package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws;

import java.math.BigDecimal;

/**
 * Lleva los parametros de entrada para traer la lista
 * de los motivos de endoso por webservice
 * 
 * 
 * @author jochoa
 *
 */
public class MotivosEndosoView {

	private String numeroPoliza;
	private BigDecimal idPoliza;
	private Long tipoEndoso;
	
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public BigDecimal getIdPoliza() {
		return idPoliza;
	}
	public void setIdPoliza(BigDecimal idPoliza) {
		this.idPoliza = idPoliza;
	}
	public Long getTipoEndoso() {
		return tipoEndoso;
	}
	public void setTipoEndoso(Long tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}
}
