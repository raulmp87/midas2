package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla;

import java.math.BigDecimal;

public class InformacionDelVehiculo {
	private Long idNegocioSeccion;
	private Long idTipoDeUso;
	private String descripcion;
	private String claveAmis;
	private String modelo;

	private String marca;
	private String vin;
	private String estilo;
	private BigDecimal descuentoPorEstado;
	
	
	public Long getIdNegocioSeccion() {
		return idNegocioSeccion;
	}
	public void setIdNegocioSeccion(Long idNegocioSeccion) {
		this.idNegocioSeccion = idNegocioSeccion;
	}
	public Long getIdTipoDeUso() {
		return idTipoDeUso;
	}
	public void setIdTipoDeUso(Long idTipoDeUso) {
		this.idTipoDeUso = idTipoDeUso;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getClaveAmis() {
		return claveAmis;
	}
	public void setClaveAmis(String claveAmis) {
		this.claveAmis = claveAmis;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getVin() {
		return vin;
	}
	public void setVin(String vin) {
		this.vin = vin;
	}
	public String getEstilo() {
		return estilo;
	}
	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}
	public BigDecimal getDescuentoPorEstado() {
		return descuentoPorEstado;
	}
	public void setDescuentoPorEstado(BigDecimal descuentoPorEstado) {
		this.descuentoPorEstado = descuentoPorEstado;
	}

}
