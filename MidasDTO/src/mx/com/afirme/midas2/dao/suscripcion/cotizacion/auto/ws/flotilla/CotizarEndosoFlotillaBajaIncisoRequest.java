package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla;

import com.lowagie.text.List;

public class CotizarEndosoFlotillaBajaIncisoRequest{
	private Short  motivoEndoso;
	private int[] numeroIncisos;
	
	
	
	public Short  getMotivoEndoso() {
		return motivoEndoso;
	}
	public void setMotivoEndoso(Short  motivoEndoso) {
		this.motivoEndoso = motivoEndoso;
	}
	public int[]  getNumeroInciso() {
		return numeroIncisos;
	}
	public void setNumeroInciso(int[]  numeroInciso) {
		this.numeroIncisos = numeroInciso;
	}
	
	
}
