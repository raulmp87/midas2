package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.agentes;

import java.math.BigDecimal;

public class VehiculoView {


	private BigDecimal idNegocioSeccion;
	private String negocioName;
	private BigDecimal idMarcaVehiculo;
	private String marcaName;
	private String idEstiloVehiculo;
	private String estiloName;
	private Short idModeloVehiculo;
	private BigDecimal idTipoUso;
	private String tipoUsoName;
	private String numeroSerie;
	private String idMoneda;
	private String idEstado;
	private BigDecimal idToCotizacion;
	private Boolean vinValido;
	private Boolean coincideEstilo;
	private String observacionesSesa;
	private String claveSesa;
	private String claveAmis;
	
	public BigDecimal getIdNegocioSeccion() {
		return idNegocioSeccion;
	}
	public void setIdNegocioSeccion(BigDecimal idNegocioSeccion) {
		this.idNegocioSeccion = idNegocioSeccion;
	}
	public String getNegocioName() {
		return negocioName;
	}
	public void setNegocioName(String negocioName) {
		this.negocioName = negocioName;
	}
	public BigDecimal getIdMarcaVehiculo() {
		return idMarcaVehiculo;
	}
	public void setIdMarcaVehiculo(BigDecimal idMarcaVehiculo) {
		this.idMarcaVehiculo = idMarcaVehiculo;
	}
	public String getMarcaName() {
		return marcaName;
	}
	public void setMarcaName(String marcaName) {
		this.marcaName = marcaName;
	}
	public String getIdEstiloVehiculo() {
		return idEstiloVehiculo;
	}
	public void setIdEstiloVehiculo(String idEstiloVehiculo) {
		this.idEstiloVehiculo = idEstiloVehiculo;
	}
	public String getEstiloName() {
		return estiloName;
	}
	public void setEstiloName(String estiloName) {
		this.estiloName = estiloName;
	}
	public Short getIdModeloVehiculo() {
		return idModeloVehiculo;
	}
	public void setIdModeloVehiculo(Short idModeloVehiculo) {
		this.idModeloVehiculo = idModeloVehiculo;
	}
	public BigDecimal getIdTipoUso() {
		return idTipoUso;
	}
	public void setIdTipoUso(BigDecimal idTipoUso) {
		this.idTipoUso = idTipoUso;
	}
	public String getTipoUsoName() {
		return tipoUsoName;
	}
	public void setTipoUsoName(String tipoUsoName) {
		this.tipoUsoName = tipoUsoName;
	}
	public String getNumeroSerie() {
		return numeroSerie;
	}
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	public String getIdMoneda() {
		return idMoneda;
	}
	public void setIdMoneda(String idMoneda) {
		this.idMoneda = idMoneda;
	}
	public String getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}
	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}
	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	public Boolean getVinValido() {
		return vinValido;
	}
	public void setVinValido(Boolean vinValido) {
		this.vinValido = vinValido;
	}
	public String getObservacionesSesa() {
		return observacionesSesa;
	}
	public void setObservacionesSesa(String observacionesSesa) {
		this.observacionesSesa = observacionesSesa;
	}
	public Boolean getCoincideEstilo() {
		return coincideEstilo;
	}
	public void setCoincideEstilo(Boolean coincideEstilo) {
		this.coincideEstilo = coincideEstilo;
	}
	public String getClaveSesa() {
		return claveSesa;
	}
	public void setClaveSesa(String claveSesa) {
		this.claveSesa = claveSesa;
	}
	public String getClaveAmis() {
		return claveAmis;
	}
	public void setClaveAmis(String claveAmis) {
		this.claveAmis = claveAmis;
	}
	
}
