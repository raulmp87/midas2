package mx.com.afirme.midas2.dao.suscripcion.endoso;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.MovimientoEndoso;

@Local
public interface MovimientoEndosoDao {

	/**
	 * Realiza el borrado de los objetos por controlEndosoCotId.
	 * @param controlEndosoCotId
	 */
	public void remove(Long controlEndosoCotId);
	
	/***
	 * Método que obtendrá los movimientos del endoso que afecten a un Inciso en particular
	 */
	public List<MovimientoEndoso> movimientosEndosoInciso( ControlEndosoCot controlEndosoCot, BitemporalInciso biInciso  );
	
	public BigDecimal obtienePrimaDerechosProrrata(BigDecimal idToPoliza, Integer numeroInciso, Date fechaIniVigencia);
}
