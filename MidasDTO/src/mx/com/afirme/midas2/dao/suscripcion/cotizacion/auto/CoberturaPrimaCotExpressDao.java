package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.CoberturaPrimaCotExpress;

@Local
public interface CoberturaPrimaCotExpressDao extends Dao<Long, CoberturaPrimaCotExpress>{

	
	/**
	 * Utiliza solo los campos necesarios para obtener todas las coberturas para una combinacion dada.
	 * La combinacion es tomada a partir de los campos que estan en el objeto <code>coberturaPrimaCotExpress</code>.
	 * @param coberturaPrimaCotExpress
	 * @return
	 */
	public List<CoberturaPrimaCotExpress> buscarPorCombinacion(CoberturaPrimaCotExpress coberturaPrimaCotExpress);
	
}
