package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.emision;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.impresiones.DatosBasesEmisionDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosBasesEmisionDTO.DatosBasesEmisionParametrosDTO;

@Local
public interface BasesEmisionDao {
	/**
	 * Obtiene la lista de bases de emisión.
	 * @param parametros
	 * @return
	 * @throws Exception
	 */
	public List<DatosBasesEmisionDTO> obtenerBasesEmision(DatosBasesEmisionParametrosDTO parametros)  throws Exception ;
}
