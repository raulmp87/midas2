package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws;

import java.io.Serializable;
import java.math.BigDecimal;

public class CoberturaView implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idCobertura;
	private Short obligatoriedad;
	private Boolean contratada;
	private String descripcion;
	private String sumaAsegurada;
	private String sumaAseguradaMin;
	private String sumaAseguradaMax;
	private String claveTipoDeducible;
	private Double deducible;
	private int claveFuenteSumaAsegurada;
	
	public BigDecimal getIdCobertura() {
		return idCobertura;
	}

	public void setIdCobertura(BigDecimal idCobertura) {
		this.idCobertura = idCobertura;
	}

	public Short getObligatoriedad() {
		return obligatoriedad;
	}

	public void setObligatoriedad(Short obligatoriedad) {
		this.obligatoriedad = obligatoriedad;
	}

	public Boolean getContratada() {
		return contratada;
	}

	public void setContratada(Boolean contratada) {
		this.contratada = contratada;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getSumaAsegurada() {
		return sumaAsegurada;
	}

	public void setSumaAsegurada(String sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}

	public String getSumaAseguradaMin() {
		return sumaAseguradaMin;
	}

	public void setSumaAseguradaMin(String sumaAseguradaMin) {
		this.sumaAseguradaMin = sumaAseguradaMin;
	}

	public String getSumaAseguradaMax() {
		return sumaAseguradaMax;
	}

	public void setSumaAseguradaMax(String sumaAseguradaMax) {
		this.sumaAseguradaMax = sumaAseguradaMax;
	}

	public String getClaveTipoDeducible() {
		return claveTipoDeducible;
	}

	public void setClaveTipoDeducible(String claveTipoDeducible) {
		this.claveTipoDeducible = claveTipoDeducible;
	}

	public Double getDeducible() {
		return deducible;
	}

	public void setDeducible(Double deducible) {
		this.deducible = deducible;
	}

	public int getClaveFuenteSumaAsegurada() {
		return claveFuenteSumaAsegurada;
	}

	public void setClaveFuenteSumaAsegurada(int claveFuenteSumaAsegurada) {
		this.claveFuenteSumaAsegurada = claveFuenteSumaAsegurada;
	}

	@Override
	public String toString() {
		return "CoberturaView [idCobertura=" + idCobertura
				+ ", obligatoriedad=" + obligatoriedad + ", contratada="
				+ contratada + ", descripcion=" + descripcion
				+ ", sumaAsegurada=" + sumaAsegurada + ", claveTipoDeducible="
				+ claveTipoDeducible + ", deducible=" + deducible + ", "
				+ "claveFuenteSumaAsegurada=" + claveFuenteSumaAsegurada +"]";
	}	
}
