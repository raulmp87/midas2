package mx.com.afirme.midas2.dao.suscripcion.impresion;

import java.math.BigDecimal;
import java.util.Date;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.suscripcion.impresion.EdicionEndoso;
import mx.com.afirme.midas2.domain.suscripcion.impresion.EdicionInciso;
import mx.com.afirme.midas2.domain.suscripcion.impresion.EdicionPoliza;

import org.joda.time.DateTime;

@Local
public interface EdicionImpresionDAO {

	/**
	 * Busca la ultima version de la edicion de la impresion de la poliza
	 * en caso de no existir edicion, devuelve null
	 * @param idToPoliza
	 * @param validOn
	 * @param recordFrom
	 * @param claveTipoEndoso
	 * @param esSituacionActual
	 * @return
	 */
	public EdicionPoliza buscarEdicionPoliza(BigDecimal idToPoliza, DateTime validOn, 
			DateTime recordFrom, Short claveTipoEndoso, Boolean esSituacionActual);
	
	/**
	 * Devuelve la siguiente secuencia disponible para la version de la edicion de la poliza
	 * @param idToPoliza
	 * @param validOn
	 * @param recordFrom
	 * @param claveTipoEndoso
	 * @param esSituacionActual
	 * @return
	 */
	public Long obtenerSecuenciaEdicionPoliza(BigDecimal idToPoliza, DateTime validOn, 
			DateTime recordFrom, Short claveTipoEndoso, Boolean esSituacionActual);
	
	/**
	 * Busca la ultima version de la edicion de la impresion de los incisos
	 * en caso de no existir edicion, devuelve null
	 * @param idToPoliza
	 * @param validOn
	 * @param recordFrom
	 * @param claveTipoEndoso
	 * @return
	 */
	public EdicionInciso buscarEdicionInciso(BigDecimal idToPoliza, DateTime validOn, 
			DateTime recordFrom, Short claveTipoEndoso);
	
	/**
	 * Devuelve la siguiente secuencia disponible para la version de la edicion del inciso
	 * @param idToPoliza
	 * @param validOn
	 * @param recordFrom
	 * @param claveTipoEndoso
	 * @return
	 */
	public Long obtenerSecuenciaEdicionInciso(BigDecimal idToPoliza, DateTime validOn, 
			DateTime recordFrom, Short claveTipoEndoso);
	
	/**
	 * Busca la ultima version de la edicion de la impresion de los endosos
	 * en caso de no existir, devuelve null;
	 * @param idToCotizacion
	 * @param recordFrom
	 * @param claveTipoEndoso
	 * @return
	 */
	public EdicionEndoso buscarEdicionEndoso(BigDecimal idToCotizacion, Date recordFrom, Short claveTipoEndoso);
	
	/**
	 * Devuelve la siguiente secuencia disponible para la version de la edicion del endoso
	 * @param idToCotizacion
	 * @param recordFrom
	 * @param claveTipoEndoso
	 * @return
	 */
	public Long obtenerSecuenciaEdicionEndoso(BigDecimal idToCotizacion, Date recordFrom, Short claveTipoEndoso);
	
}
