package mx.com.afirme.midas2.dao.suscripcion.cambiosglobales;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.suscripcion.cambiosglobales.ConfiguracionPlantillaNeg;
import mx.com.afirme.midas2.domain.suscripcion.cambiosglobales.ConfiguracionPlantillaNegCobertura;

@Local
public interface ConfiguracionPlantillaDao extends Dao<BigDecimal, ConfiguracionPlantillaNeg> {

	/**
	 * Obtiene el listado de coberturas para una plantilla
	 * @param idPlantilla
	 * @return
	 */
	public List<ConfiguracionPlantillaNegCobertura> listarCoberturas(Long idPlantilla);

	/**
	 * Buscar por filtros
	 * @param filtro
	 * @return
	 */
	public List<ConfiguracionPlantillaNeg> findByFilters(ConfiguracionPlantillaNeg filtro);
	
	public ConfiguracionPlantillaNeg findByCotizacionLineaPaquete(BigDecimal cotizacionId, BigDecimal lineaId, BigDecimal paqueteId);
	
}
