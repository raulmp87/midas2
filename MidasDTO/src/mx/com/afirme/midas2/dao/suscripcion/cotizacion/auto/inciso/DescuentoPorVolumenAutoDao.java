package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.inciso;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.DescuentoPorVolumenAuto;

@Local
public interface DescuentoPorVolumenAutoDao extends Dao<Long, DescuentoPorVolumenAuto>{

	/**
	 * Busca el <code>DescuentoPorVolumenAuto</code> que se encuentre dentro del rango
	 * <code>minTotalSubsections</code> y <code>minTotalSubsections</code>.
	 * @param totalIncisos
	 * @return
	 */
	public DescuentoPorVolumenAuto findByTotalIncisoDentroDeMinMaxTotalSubsections(int totalIncisos);
	
}
