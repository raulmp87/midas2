package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws;

import java.util.List;


public class CotizacionView {
	
	private Long idAgente;
	private DatosPolizaView datosPoliza;
	private ZonaView zonaCirculacion;
	private DatosVehiculoView vehiculo;
	private DatosPaqueteView paquete;
	private List<CoberturaView> coberturas;
	private ContratanteView contratante;
	private ConductorView conductor;
	private DatosAseguradoView asegurado;
	private String folio;
	private DatosAdicionalesView datosAdicionales;
	
	public Long getIdAgente() {
		return idAgente;
	}

	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}

	public DatosPolizaView getDatosPoliza() {
		return datosPoliza;
	}

	public void setDatosPoliza(DatosPolizaView datosPoliza) {
		this.datosPoliza = datosPoliza;
	}

	public ZonaView getZonaCirculacion() {
		return zonaCirculacion;
	}

	public void setZonaCirculacion(ZonaView zonaCirculacion) {
		this.zonaCirculacion = zonaCirculacion;
	}

	public DatosVehiculoView getVehiculo() {
		return vehiculo;
	}

	public void setVehiculo(DatosVehiculoView vehiculo) {
		this.vehiculo = vehiculo;
	}

	public DatosPaqueteView getPaquete() {
		return paquete;
	}

	public void setPaquete(DatosPaqueteView paquete) {
		this.paquete = paquete;
	}

	public List<CoberturaView> getCoberturas() {
		return coberturas;
	}

	public void setCoberturas(List<CoberturaView> coberturas) {
		this.coberturas = coberturas;
	}

	public ContratanteView getContratante() {
		return contratante;
	}

	public void setContratante(ContratanteView contratante) {
		this.contratante = contratante;
	}

	public ConductorView getConductor() {
		return conductor;
	}

	public void setConductor(ConductorView conductor) {
		this.conductor = conductor;
	}

	public DatosAseguradoView getAsegurado() {
		return asegurado;
	}

	public void setAsegurado(DatosAseguradoView asegurado) {
		this.asegurado = asegurado;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public DatosAdicionalesView getDatosAdicionales() {
		return datosAdicionales;
	}

	public void setDatosAdicionales(DatosAdicionalesView datosAdicionales) {
		this.datosAdicionales = datosAdicionales;
	}

	@Override
	public String toString() {
		return "CotizacionView [idAgente=" + idAgente + ", datosPoliza="
				+ datosPoliza + ", zonaCirculacion=" + zonaCirculacion
				+ ", vehiculo=" + vehiculo + ", paquete=" + paquete
				+ ", coberturas=" + coberturas + ", contratante=" + contratante
				+ ", conductor=" + conductor + ", asegurado=" + asegurado
				+ ", folio=" + folio + ", datosAdicionales=" + datosAdicionales
				+ "]";
	}
	
}
