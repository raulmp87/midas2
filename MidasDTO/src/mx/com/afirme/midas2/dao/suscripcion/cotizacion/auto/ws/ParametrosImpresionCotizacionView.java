package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws;

import java.math.BigDecimal;

public class ParametrosImpresionCotizacionView {
	private BigDecimal idToCotizacion;
	private boolean caratula;
	private int incisoInicial;
	private int incisoFinal;
	private boolean todosLosIncisos;
	
	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}
	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	public boolean isCaratula() {
		return caratula;
	}
	public void setCaratula(boolean caratula) {
		this.caratula = caratula;
	}
	public int getIncisoInicial() {
		return incisoInicial;
	}
	public void setIncisoInicial(int incisoInicial) {
		this.incisoInicial = incisoInicial;
	}
	public int getIncisoFinal() {
		return incisoFinal;
	}
	public void setIncisoFinal(int incisoFinal) {
		this.incisoFinal = incisoFinal;
	}
	public boolean isTodosLosIncisos() {
		return todosLosIncisos;
	}
	public void setTodosLosIncisos(boolean todosLosIncisos) {
		this.todosLosIncisos = todosLosIncisos;
	}
}
