package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws;

import java.util.Date;

public class ContratanteView {
	private int idContratante;
	private String nombreContratante;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String rfc;
	private char claveSexo;
	private char claveEstadoCivil;
	private String idEstadoNacimiento;
	private String idMunicipioNacimiento;
	private String fechaNacimiento;
	private Date fechaDeNacimiento;
	private String codigoPostal;
	private String idCiudad;
	private String idEstado;
	private int idColonia;
	private String claveColonia;
	private String nombreColonia;
	private String calle;
	private String numero;
	private String telefonoCasa;
	private String telefonoOficina;
	private String email;
	
	//datos para persona moral
	private String nombreRazonSocial;
	private String representanteLegal;
	private String fechaConstitucion;
	private Date fechaDeConstitucion;
	
	public int getIdContratante() {
		return idContratante;
	}
	public void setIdContratante(int idContratante) {
		this.idContratante = idContratante;
	}
	public String getNombreContratante() {
		return nombreContratante;
	}
	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public char getClaveSexo() {
		return claveSexo;
	}
	public void setClaveSexo(char claveSexo) {
		this.claveSexo = claveSexo;
	}
	public char getClaveEstadoCivil() {
		return claveEstadoCivil;
	}
	public void setClaveEstadoCivil(char claveEstadoCivil) {
		this.claveEstadoCivil = claveEstadoCivil;
	}
	public String getIdEstadoNacimiento() {
		return idEstadoNacimiento;
	}
	public void setIdEstadoNacimiento(String idEstadoNacimiento) {
		this.idEstadoNacimiento = idEstadoNacimiento;
	}
	public String getIdMunicipioNacimiento() {
		return idMunicipioNacimiento;
	}
	public void setIdMunicipioNacimiento(String idMunicipioNacimiento) {
		this.idMunicipioNacimiento = idMunicipioNacimiento;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public Date getFechaDeNacimiento() {
		return fechaDeNacimiento;
	}
	public void setFechaDeNacimiento(Date fechaDeNacimiento) {
		this.fechaDeNacimiento = fechaDeNacimiento;
	}
	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public String getIdCiudad() {
		return idCiudad;
	}
	public void setIdCiudad(String idCiudad) {
		this.idCiudad = idCiudad;
	}
	public String getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}
	public int getIdColonia() {
		return idColonia;
	}
	public void setIdColonia(int idColonia) {
		this.idColonia = idColonia;
	}
	public String getClaveColonia() {
		return claveColonia;
	}
	public void setClaveColonia(String claveColonia) {
		this.claveColonia = claveColonia;
	}
	public String getNombreColonia() {
		return nombreColonia;
	}
	public void setNombreColonia(String nombreColonia) {
		this.nombreColonia = nombreColonia;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getTelefonoCasa() {
		return telefonoCasa;
	}
	public void setTelefonoCasa(String telefonoCasa) {
		this.telefonoCasa = telefonoCasa;
	}
	public String getTelefonoOficina() {
		return telefonoOficina;
	}
	public void setTelefonoOficina(String telefonoOficina) {
		this.telefonoOficina = telefonoOficina;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNombreRazonSocial() {
		return nombreRazonSocial;
	}
	public void setNombreRazonSocial(String nombreRazonSocial) {
		this.nombreRazonSocial = nombreRazonSocial;
	}
	public String getRepresentanteLegal() {
		return representanteLegal;
	}
	public void setRepresentanteLegal(String representanteLegal) {
		this.representanteLegal = representanteLegal;
	}
	public String getFechaConstitucion() {
		return fechaConstitucion;
	}
	public void setFechaConstitucion(String fechaConstitucion) {
		this.fechaConstitucion = fechaConstitucion;
	}
	public Date getFechaDeConstitucion() {
		return fechaDeConstitucion;
	}
	public void setFechaDeConstitucion(Date fechaDeConstitucion) {
		this.fechaDeConstitucion = fechaDeConstitucion;
	}
}
