package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla;

import java.math.BigDecimal;

public class DetalleDeCoberturas {
	private Integer pctDanosMateriales;
	private Integer pctRoboTotal;
	private BigDecimal limiteRCTerceros;
	private BigDecimal limiteGastosMedicos;
	private Boolean asistenciaJuridica;
	private Boolean asistenciaEnViajes;
	private Boolean exencionDeducibleDanosMateriales;
	private BigDecimal limiteAccidenteConductor;
	private Boolean extencionRC;
	private BigDecimal limiteAdaptacionesConversiones;
	private BigDecimal limiteEquipoEspecial;
	private Integer pctDeducibleEquipoEspecial;
	private Boolean exencionDeducibleRoboTotal;
	private Boolean responsabilidadCivilUsaCanada;
	private BigDecimal igualacion;
	private Integer numeroRemolques;
	private String tipoCarga;
	private BigDecimal limiteMuerte;
	private BigDecimal sumaAseguradaDanosMateriales;
	private BigDecimal sumaAseguradaRoboTotal;
	private Integer diasSalarioMinimo;
	
	
	public Integer getPctDanosMateriales() {
		return pctDanosMateriales;
	}
	public void setPctDanosMateriales(Integer pctDanosMateriales) {
		this.pctDanosMateriales = pctDanosMateriales;
	}
	public Integer getPctRoboTotal() {
		return pctRoboTotal;
	}
	public void setPctRoboTotal(Integer pctRoboTotal) {
		this.pctRoboTotal = pctRoboTotal;
	}
	public BigDecimal getLimiteRCTerceros() {
		return limiteRCTerceros;
	}
	public void setLimiteRCTerceros(BigDecimal limiteRCTerceros) {
		this.limiteRCTerceros = limiteRCTerceros;
	}
	public BigDecimal getLimiteGastosMedicos() {
		return limiteGastosMedicos;
	}
	public void setLimiteGastosMedicos(BigDecimal limiteGastosMedicos) {
		this.limiteGastosMedicos = limiteGastosMedicos;
	}
	public Boolean getAsistenciaJuridica() {
		return asistenciaJuridica;
	}
	public void setAsistenciaJuridica(Boolean asistenciaJuridica) {
		this.asistenciaJuridica = asistenciaJuridica;
	}
	public Boolean getAsistenciaEnViajes() {
		return asistenciaEnViajes;
	}
	public void setAsistenciaEnViajes(Boolean asistenciaEnViajes) {
		this.asistenciaEnViajes = asistenciaEnViajes;
	}
	public Boolean getExencionDeducibleDanosMateriales() {
		return exencionDeducibleDanosMateriales;
	}
	public void setExencionDeducibleDanosMateriales(Boolean exencionDeducibleDanosMateriales) {
		this.exencionDeducibleDanosMateriales = exencionDeducibleDanosMateriales;
	}
	public BigDecimal getLimiteAccidenteConductor() {
		return limiteAccidenteConductor;
	}
	public void setLimiteAccidenteConductor(BigDecimal limiteAccidenteConductor) {
		this.limiteAccidenteConductor = limiteAccidenteConductor;
	}
	public Boolean getExtencionRC() {
		return extencionRC;
	}
	public void setExtencionRC(Boolean extencionRC) {
		this.extencionRC = extencionRC;
	}
	public BigDecimal getLimiteAdaptacionesConversiones() {
		return limiteAdaptacionesConversiones;
	}
	public void setLimiteAdaptacionesConversiones(BigDecimal limiteAdaptacionesConversiones) {
		this.limiteAdaptacionesConversiones = limiteAdaptacionesConversiones;
	}
	public BigDecimal getLimiteEquipoEspecial() {
		return limiteEquipoEspecial;
	}
	public void setLimiteEquipoEspecial(BigDecimal limiteEquipoEspecial) {
		this.limiteEquipoEspecial = limiteEquipoEspecial;
	}
	public Integer getPctDeducibleEquipoEspecial() {
		return pctDeducibleEquipoEspecial;
	}
	public void setPctDeducibleEquipoEspecial(Integer pctDeducibleEquipoEspecial) {
		this.pctDeducibleEquipoEspecial = pctDeducibleEquipoEspecial;
	}
	public Boolean getExencionDeducibleRoboTotal() {
		return exencionDeducibleRoboTotal;
	}
	public void setExencionDeducibleRoboTotal(Boolean exencionDeducibleRoboTotal) {
		this.exencionDeducibleRoboTotal = exencionDeducibleRoboTotal;
	}
	public Boolean getResponsabilidadCivilUsaCanada() {
		return responsabilidadCivilUsaCanada;
	}
	public void setResponsabilidadCivilUsaCanada(Boolean responsabilidadCivilUsaCanada) {
		this.responsabilidadCivilUsaCanada = responsabilidadCivilUsaCanada;
	}
	public BigDecimal getIgualacion() {
		return igualacion;
	}
	public void setIgualacion(BigDecimal igualacion) {
		this.igualacion = igualacion;
	}
	public Integer getNumeroRemolques() {
		return numeroRemolques;
	}
	public void setNumeroRemolques(Integer numeroRemolques) {
		this.numeroRemolques = numeroRemolques;
	}
	public String getTipoCarga() {
		return tipoCarga;
	}
	public void setTipoCarga(String tipoCarga) {
		this.tipoCarga = tipoCarga;
	}
	public BigDecimal getLimiteMuerte() {
		return limiteMuerte;
	}
	public void setLimiteMuerte(BigDecimal limiteMuerte) {
		this.limiteMuerte = limiteMuerte;
	}
	public BigDecimal getSumaAseguradaDanosMateriales() {
		return sumaAseguradaDanosMateriales;
	}
	public void setSumaAseguradaDanosMateriales(BigDecimal sumaAseguradaDanosMateriales) {
		this.sumaAseguradaDanosMateriales = sumaAseguradaDanosMateriales;
	}
	public BigDecimal getSumaAseguradaRoboTotal() {
		return sumaAseguradaRoboTotal;
	}
	public void setSumaAseguradaRoboTotal(BigDecimal sumaAseguradaRoboTotal) {
		this.sumaAseguradaRoboTotal = sumaAseguradaRoboTotal;
	}
	public Integer getDiasSalarioMinimo() {
		return diasSalarioMinimo;
	}
	public void setDiasSalarioMinimo(Integer diasSalarioMinimo) {
		this.diasSalarioMinimo = diasSalarioMinimo;
	}

}
