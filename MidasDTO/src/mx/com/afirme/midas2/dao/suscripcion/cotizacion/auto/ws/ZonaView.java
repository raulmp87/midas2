package mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws;

import java.io.Serializable;

public class ZonaView implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String idEstadoCirculacion;
	private String idMunicipioCirculacion;
	private String codigoPostalCirculacion;
	
	
	public String getIdEstadoCirculacion() {
		return idEstadoCirculacion;
	}
	public void setIdEstadoCirculacion(String idEstadoCirculacion) {
		this.idEstadoCirculacion = idEstadoCirculacion;
	}
	public String getIdMunicipioCirculacion() {
		return idMunicipioCirculacion;
	}
	public void setIdMunicipioCirculacion(String idMunicipioCirculacion) {
		this.idMunicipioCirculacion = idMunicipioCirculacion;
	}
	public String getCodigoPostalCirculacion() {
		return codigoPostalCirculacion;
	}
	public void setCodigoPostalCirculacion(String codigoPostalCirculacion) {
		this.codigoPostalCirculacion = codigoPostalCirculacion;
	}
	@Override
	public String toString() {
		return "ZonaView [idEstadoCirculacion=" + idEstadoCirculacion
				+ ", idMunicipioCirculacion=" + idMunicipioCirculacion
				+ ", codigoPostalCirculacion=" + codigoPostalCirculacion + "]";
	}
}
