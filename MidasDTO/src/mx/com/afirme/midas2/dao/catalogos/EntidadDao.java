package mx.com.afirme.midas2.dao.catalogos;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;


@Local
public interface EntidadDao {
	
	public <E extends Entidad> Object persistAndReturnId(E entity);

	public <E extends Entidad> void persist(E entity);

	public <E extends Entidad> E update(E entity);

	public <E extends Entidad> void remove(E entity);
	
	public <E extends Entidad> void detach(E entity);

	public <E extends Entidad,K> E findById(Class<E> entityClass,K id);
	
	/**
	 * Ensures that the retrieved entity is not retireved from the second level cache by
	 * evicting before finding.
	 * @param entityClass
	 * @param id
	 * @return
	 */
	public <E extends Entidad,K> E evictAndFindById(Class<E> entityClass,K id);

	public <E extends Entidad> List<E> findAll(Class<E> entityClass);
	
	public <E extends Entidad> List<E> findAll(Class<E> entityClass, int maxrows);

	public <E extends Entidad,K> E getReference(Class<E> entityClass,K id);
	
	public Object executeNativeQuerySimpleResult(String query);
	
	public <E extends Entidad> List<E> findByProperty(Class<E> entityClass, String propertyName,
			final Object value);
	
	public <E extends Entidad> List<E> findByProperties(Class<E> entityClass,Map<String,Object> params);
	/**
	 * Obtiene el total de elementos
	 * encontrados en la busqueda
	 * @author martin
	 */
	public <E extends Entidad> Long findByPropertiesCount(Class<E> entityClass,Map<String,Object> params);
	/**
	 * Busca una entidad por columnas y propiedades
	 * @param <E>
	 * @param entityClass
	 * @param params
	 * @param queryParams mapa con los parametros del query numero de registros a cargar y primer resultado a cargar
	 * @param colums Un string con las columnas que se quieren seleccionar separadas por comas.
	 * Ej. String columns = "idTocotizacion,solicitudDTO";
	 * Las fechas se agregan en el queryWhere con el siguiente formato yyyy-mm-dd
	 * Ej. queryWhere.append(" and model.fechaCreacion > '"+ fechaConformato + "'");
	 * @param queryOrder stringBuiler describiendo el ordenamiento, no es obligatorio.
	 * @param queryWhere parametro que lleva otros filtros puestos manualmente tiene que iniciar con un "and"
	 * @return Lista con todos los resultados encontrados
	 * @author martin
	 */
	public <E extends Entidad> List<E> findByColumnsAndProperties(
			Class<E> entityClass,String columns,Map<String, Object> params,
			Map<String,Object> queryParams,StringBuilder queryWhere,StringBuilder queryOrder);
	
	public <E extends Entidad> Long findByColumnsAndPropertiesCount(
			Class<E> entityClass,Map<String, Object> params,
			Map<String,Object> queryParams,StringBuilder queryWhere,StringBuilder queryOrder);
	
	public <E extends Entidad> List<E> findByPropertiesWithOrder(Class<E> entityClass,Map<String, Object> params,String...orderByAttributes);
	
	@SuppressWarnings("rawtypes")
	public List executeNativeQueryMultipleResult(String query);

	@SuppressWarnings("rawtypes")
	public List executeNativeQueryMultipleResult(String query, Map<String, Object> parameters);
	
	public Object executeQuerySimpleResult(String query, Map<String,Object> parameters);
	
	@SuppressWarnings("rawtypes")
	public List executeQueryMultipleResult(String query, Map<String,Object> parameters);
	
	@SuppressWarnings("rawtypes")
	public List executeQueryMultipleResultWithRefresh(String query, Map<String,Object> parameters);
	
	public <E extends Entidad> void executeActionGrid(String accion, E entityClass);
	
	public <E extends Entidad> void refresh(E entity);
	
	public void updateWithQuery(String query,Map<String, Object> parameters); 	
	
	/**
	 * Sirve para buscar un listado de una Entidad a través de un objeto filtro.
	 * En caso de que el objeto filtro no pueda mapearse directamente sobre la entidad
	 * se hace uso de <code>FilterPersistanceAnnotation</code> sobre los métodos del filtro
	 * para definir en que forma se mapearán sobre la entidad.
	 * @param <E>
	 * @param <K>
	 * @param entityClass
	 * @param filter
	 * @return
	 */
	public <E extends Entidad, T> List<E> findByFilterObject(
			Class<E> entityClass, T filter);
	
	public <E extends Entidad, T> List<E> findByFilterObject(
			Class<E> entityClass, T filter, String orderBy);
	/**
	 * Gets next value for number property. Useful when entity has an autonumber column as well as an autogenerated id
	 * @param <E>
	 * @param entityClass
	 * @param property
	 * @return
	 */
	public  <E extends Entidad, T> Long getIncrementedProperty(Class<E> entityClass, String property, T filter);
	
	
	/**
	 * Gets next value for a property. Useful when entity has an autonumber column as well as an autogenerated id or getting the max actual
	 * value for some filters
	 * @param <E>
	 * @param entityClass
	 * @param property
	 * @return
	 */
	public  <E extends Entidad, T> Object getIncrementedProperty(Class<E> entityClass, String property, Map<String, Object> filter);
	
	/**
	 * Gets next value for a named sequence. Useful when there is a need for a sequenced value
	 * @param <E>
	 * @param entityClass
	 * @param property
	 * @return
	 */
	public Long getIncrementedSequence(String sequenceName);
}
