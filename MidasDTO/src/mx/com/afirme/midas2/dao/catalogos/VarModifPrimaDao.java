package mx.com.afirme.midas2.dao.catalogos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.catalogos.VarModifPrima;

@Local
public interface VarModifPrimaDao extends Dao<Long, VarModifPrima> {
	public List<VarModifPrima> findByFilters(VarModifPrima filtroTcVarModifPrima);
	
	public List<VarModifPrima> findByGrupoModifPrima(Long idGrupo);

}
