package mx.com.afirme.midas2.dao.catalogos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.catalogos.Paquete;

@Local
public interface PaqueteDao extends Dao<Long, Paquete> {
	
	public List<Paquete> findByFilters(Paquete filtroPaquete);

}
