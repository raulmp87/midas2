package mx.com.afirme.midas2.dao.catalogos.estiloVehiculo;

import javax.ejb.Local;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas2.dao.Dao;

@Local
public interface EstiloVehiculoDao extends Dao<EstiloVehiculoId, EstiloVehiculoDTO>{
	/**
	 * Retorna la descripcion de un estilo (AMIS)
	 * @param claveEstilo (AMIS)
	 * @return String
	 * @author martin
	 */
	public String getDescripcionPorClaveAmis(String claveEstilo);
}
