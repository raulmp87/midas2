package mx.com.afirme.midas2.dao.catalogos;

import java.io.Serializable;


public interface Entidad extends Serializable {
	public <K> K getKey();
	public String getValue();
	public <K> K getBusinessKey();
}
