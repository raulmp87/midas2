package mx.com.afirme.midas2.dao.catalogos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.catalogos.GrupoVariablesModificacionPrima;

@Local
public interface GrupoVariablesModificacionPrimaDao extends Dao<Long, GrupoVariablesModificacionPrima> {

	public List<GrupoVariablesModificacionPrima> findByFilters(GrupoVariablesModificacionPrima filtroGrupoVariablesModificacionPrima);
	public List<GrupoVariablesModificacionPrima> listRelated(Long id);

}
