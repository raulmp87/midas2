package mx.com.afirme.midas2.dao.catalogos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.catalogos.CobPaquetesSeccion;
import mx.com.afirme.midas2.domain.catalogos.CobPaquetesSeccionId;

@Local
public interface CobPaquetesSeccionDao extends Dao<CobPaquetesSeccionId, CobPaquetesSeccion> {
	public List<CobPaquetesSeccion> findByFilters(CobPaquetesSeccion cobPaquetesSeccion);
	public List<CobPaquetesSeccion> getPorSeccionPaquete(CobPaquetesSeccion cobPaquetesSeccion);
	public List<CobPaquetesSeccion> getPorSeccionPaquete(Long idToSeccion,Long idPaquete);
}
