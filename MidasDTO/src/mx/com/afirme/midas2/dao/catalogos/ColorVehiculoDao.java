package mx.com.afirme.midas2.dao.catalogos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.catalogos.ColorVehiculo;

@Local
public interface ColorVehiculoDao extends EntidadDao{

	public List<ColorVehiculo> findByFilters(ColorVehiculo filtroColorVehiculo);

}
