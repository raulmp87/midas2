package mx.com.afirme.midas2.dao.catalogos.riesgo;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDTO;
import mx.com.afirme.midas2.dao.Dao;

@Local
public interface RiesgoDTODao extends Dao<BigDecimal,RiesgoDTO>{

	public List<RiesgoDTO> getListarFiltrado(RiesgoDTO riesgoDTO,Boolean mostrarInactivos);
	
	public List<RiesgoDTO> listarVigentesAutos();
}
