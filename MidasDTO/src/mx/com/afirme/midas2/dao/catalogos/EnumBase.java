package mx.com.afirme.midas2.dao.catalogos;

public interface EnumBase<K> {

	public K getValue();
	
	public String getLabel();

}
