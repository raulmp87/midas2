package mx.com.afirme.midas2.dao.catalogos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.catalogos.VarModifDescripcion;

@Local
public interface VarModifDescripcionDao extends Dao<Long, VarModifDescripcion> {

	public List<VarModifDescripcion> findByFilters(VarModifDescripcion varModifDescripcion);
	
	public List<VarModifDescripcion> findByGroup(Integer group);
}
