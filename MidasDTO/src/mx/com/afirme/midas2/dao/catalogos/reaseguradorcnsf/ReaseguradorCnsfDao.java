package mx.com.afirme.midas2.dao.catalogos.reaseguradorcnsf;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.catalogos.reaseguradorcnsf.ReaseguradorCnsfMov;

@Local
public interface ReaseguradorCnsfDao extends EntidadDao{ 
	
	public List<ReaseguradorCnsfMov> findByFilters(ReaseguradorCnsfMov filtro);
	
	public int deleteReaseguradorMovs(BigDecimal idAgencia);
	
	public void saveReaseguradorMovs(ReaseguradorCnsfMov reaseguradorCnsfMov);
	
	public Date getFechaUltimoCorteProcesado();
}
