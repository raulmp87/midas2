package mx.com.afirme.midas2.dao.catalogos;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ActualizacionAgente;
@Local
public interface EntidadHistoricoDao extends EntidadDao{
	public static enum TipoOperacionHistorial{
		CENTRO_OPERACION(10L),GERENCIA(20L),EJECUTIVO(30L),PROMOTORIA(40L),AGENTE(50L),CONFIGURACION_COMISIIONES(60L),CONFIGURACION_BONOS(70L),AFIANZADORA(80L),PERSONA(90L),DOMICILIO(100L),HERENCIA_CLIENTES(110L), AUTORIZACION_AGENTES(120L), CARGOS_AGENTES(130L);
		private Long value;
		private TipoOperacionHistorial(Long value){
			this.value=value;
		}
		public Long getValue(){
			return value;
		}
	}
	/**
	 * Guarda en el historial el evento o cambio de un registro de acuerdo al tipo de operacion, especificando el usuario que realiza el cambio
	 * @param operationType
	 * @param idRecord
	 * @param comments
	 * @param userName Usuario que ejecuta el cambio
	 * @return
	 */
	public ActualizacionAgente saveHistory(TipoOperacionHistorial tipoOperacion,Long idRegistro, String comentarios, String usuario,String tipoMovimiento);
	/**
	 * Obtiene el historial de  un registro
	 * @param operationType
	 * @param idRecord
	 * @return
	 * @throws SystemException
	 */
	public List<ActualizacionAgente> getHistory(TipoOperacionHistorial operationType,Long idRecord)throws SystemException;
	/**
	 * Obtiene el ultimo cambio.
	 * @param operationType
	 * @param idRecord
	 * @return
	 * @throws SystemException
	 */
	public ActualizacionAgente getLastUpdate(TipoOperacionHistorial operationType,Long idRecord)throws SystemException;
	
	public ActualizacionAgente getHistoryRecord(TipoOperacionHistorial operationType,Long idRecord, String historyDate)throws SystemException;
	
	public <E extends Entidad, K> E findById(Class<E> entityClass, String keyPropertyName, K key, Date historyDate);
	
	public <E extends Entidad> List<E> findAll(Class<E> entityClass, Date historyDate);
	
	public <E extends Entidad> List<E> findByProperty(Class<E> entityClass, String propertyName,
			final Object value, Date historyDate);
	
	public <E extends Entidad> List<E> findByProperties(Class<E> entityClass,Map<String,Object> params, Date historyDate);
	
	public <E extends Entidad> List<E> findByPropertiesWithOrder(Class<E> entityClass,Map<String, Object> params, Date historyDate, String...orderByAttributes);
	
	public <E extends Entidad> E updateForHistory(E entity);
	
	public <E extends Entidad> List<E> executeHistoryQuery(Date historyDate, Query query);
	
	
}
