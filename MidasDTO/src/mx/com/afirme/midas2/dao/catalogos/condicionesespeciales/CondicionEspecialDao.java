package mx.com.afirme.midas2.dao.catalogos.condicionesespeciales;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import org.joda.time.DateTime;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.condicionesespeciales.AreaCondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.CoberturaCondicionEsp;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.VarAjusteCondicionEsp;
import mx.com.afirme.midas2.domain.condicionesespeciales.VariableAjuste;
import mx.com.afirme.midas2.dto.CondicionEspecialDTO;

@Local
public interface CondicionEspecialDao  extends Dao<Long, CondicionEspecial>{

	public List<CondicionEspecial> buscarCondicionFiltro(CondicionEspecialDTO condicionFiltro);
	
	public List<CondicionEspecial> buscarCondicionCodigoNombre(String param);
	
	public List<CondicionEspecial> buscarCondicionRankingFiltro(CondicionEspecialDTO condicionFiltro);
	
	public Long generarCodigo();
	
	public List<CondicionEspecial> obtenerCondicionEspecialPorLinea( BigDecimal idSeccion );
	
	public List<CoberturaCondicionEsp> obtenerCoberturasCondicionEspecial(Long idCondicionEspecial);
	
	public List<VariableAjuste> obtenerVariablesDisponibles(Long idCondicionEspecial, String nombre);
	
	public List<AreaCondicionEspecial> obtenerAreasImpactoLigadas(Long idCondicionEspecial , Boolean readOnly);
	
	public List<SeccionDTO> obtenerNegociosLigados(Long idCondicionEspecial , Boolean readOnly);
	
	public List<VarAjusteCondicionEsp> obtenerVarAjusteCondicionEspecial(Long idCondicionEspecial);
	
	public List<CondicionEspecial> obtenerCondicionesCotizacionValidas(Long cotizacionContinuityId, DateTime validOn, DateTime knownOn);
	
	public List<CondicionEspecial> obtenerCondicionesIncisoValidas(Long cotizacionContinuityId, DateTime validOn, DateTime knownOn,
			int incisoInicial, int incisoFinal);
	
	public List<CondicionEspecial> obtenerCondicionesIncisoValidasInciso(Long incisoContinuityId, DateTime validOn, DateTime knownOn);
}
