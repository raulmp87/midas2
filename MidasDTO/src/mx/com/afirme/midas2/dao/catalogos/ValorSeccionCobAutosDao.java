package mx.com.afirme.midas2.dao.catalogos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.catalogos.ValorSeccionCobAutos;
import mx.com.afirme.midas2.domain.catalogos.ValorSeccionCobAutosId;


@Local
public interface ValorSeccionCobAutosDao  extends Dao<ValorSeccionCobAutosId, ValorSeccionCobAutos>  { 
	
	List<ValorSeccionCobAutos> getPorIdToSeccion(Long idToSeccion);
	
}
