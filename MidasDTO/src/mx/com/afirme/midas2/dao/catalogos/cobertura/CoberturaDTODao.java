package mx.com.afirme.midas2.dao.catalogos.cobertura;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.dao.Dao;

@Local
public interface CoberturaDTODao extends Dao<BigDecimal,CoberturaDTO>{

	public List<CoberturaDTO> getListarFiltrado(CoberturaDTO coberturaDTO,Boolean mostrarInactivos);
	
	public List<CoberturaDTO> listarVigentesAutos();
}
