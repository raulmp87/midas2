package mx.com.afirme.midas2.dao.catalogos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifaSeccion;

@Local
public interface AgrupadorTarifaSeccionDao extends EntidadDao{

	public List<AgrupadorTarifaSeccion> findByFilters(AgrupadorTarifaSeccion filter);
	
	public List<AgrupadorTarifaSeccion> findBySeccionMoneda(AgrupadorTarifaSeccion filter);
	
	public List<AgrupadorTarifaSeccion> findByBusiness(String claveNegocio,AgrupadorTarifaSeccion filter);
	
}
