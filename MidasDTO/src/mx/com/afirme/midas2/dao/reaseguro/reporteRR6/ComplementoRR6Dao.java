package mx.com.afirme.midas2.dao.reaseguro.reporteRR6;

import java.math.BigDecimal;
import java.sql.Date;

import javax.ejb.Local;

import mx.com.afirme.midas.danios.reportes.reporterr6.CargaRR6DTO;

@Local
public interface ComplementoRR6Dao {
	
	void insertaReporteRTRE(String clave,String negCubiertos,Integer claveEstrategia,Integer ordenCobertura,String identificador, 
			Date fCorte,String negocio);
	
	void insertaReporteRTRC(String identificador,String clave,String negCubiertos,String moneda,String captura,String modificados,String fechainicial,
			String fechafinal,Integer tipocontrato,String capas,BigDecimal porcionCedida,BigDecimal retencionPrioridad,String rentencionFianzas,
			BigDecimal capacidadMaxima,String capMaxFianzas,BigDecimal importe,String comision,String utilidades,String reasInscrito,String tipoReas,
			String reasNacional,String noInscrito,String participacion,String tipointermediario,String id_Intermediario,String intermNoAutorizado, 
			String suscriptor,String aclaracion,Date fCorte,String negocio,Integer orden);
	
	void insertaReporteRTRF(String identificador, String clave, String negCubiertos, String asegurado, String sumaAsegurada, Integer moneda, 
			BigDecimal primaEmitida, BigDecimal primaFacultada, BigDecimal primaCedProporc, BigDecimal primaRetenida, 
			String fechaInicial, String fechaFinal, String tipoContrato, String capas, BigDecimal retPrioridad, String retpriorfian, 
			BigDecimal capacidadMaxReas, String capacidadMaxFian, BigDecimal comision, String partUtilidades, String reasInscrito, 
			String tipoReas, String clvReasNac, String reasNoInscrito, BigDecimal participacionReas, String tipoIntermediario, 
			String clvIntermediario, String intNoAutorizado, String suscriptor, String aclaraciones, String entidades, String municipio, 
			String sector, Date fechaFinCorte, String negocio);
	
	void insertaReporteRTRR(String identificador, Integer tipoContrato, String anio, String clave, String iniciovigencia, BigDecimal comisiones, 
			BigDecimal utilidades, BigDecimal sinReclamaFac, BigDecimal sinReclamaNoProp, BigDecimal ingresos, BigDecimal otrosIngresos, 
			BigDecimal montoPrima, BigDecimal costoCobertura, BigDecimal partSalvamento, BigDecimal recursosRetenidos, BigDecimal reasFinanciero, 
			BigDecimal castigo, BigDecimal egresos, BigDecimal gastosReas, String aclaraciones, Date fechaCorte, String negocio);
	
	void insertaReporteRTRS(Integer consecutivo, Integer siniestro, String claveNegocio, String siniestroReclamacion, String asegurado, String fechaReclamacion, 
			BigDecimal importeReclamacion, BigDecimal importeRecSiniestro, String reasInscrito, String tipoReasNac, 
			String claveReasNac, String reasNoInscrito, BigDecimal importeProporcional, BigDecimal importeNoProporcional, BigDecimal importeFacultativo, 
			String entidades, String municipio, String sector, Date fechaCorte, String negocio);
	
	void insertaReporteRARN(String mes, String concepto, String ramo, String rgre, String tipo, String reasNacional, 
			String noInscrito, BigDecimal primaCedida, BigDecimal coberturaXL, BigDecimal sumaAsegurada,
			BigDecimal sumaAseguradaRetenida, Date fechaIniCorte, Date fechaFinCorte, String negocio);
	
	void insertaReporteRTRI(String idcontrato, BigDecimal corretajeContratos, BigDecimal corretajeFacultativos,Date fechaFinCorte, String negocio);
	
	int getRegistros(java.util.Date fechaCorte, String tabla, String fecha, String negocio);
	
	void insertaEstatusCarga(CargaRR6DTO cargaDTO);
	
	void actualizaEstatusCarga(CargaRR6DTO cargaDTO);
	
	void borraRegistros(java.util.Date fCorte, String tabla, String fecha, String negocio);
	
	int estatusCarga(java.util.Date fechaCorte, String negocio);
}
