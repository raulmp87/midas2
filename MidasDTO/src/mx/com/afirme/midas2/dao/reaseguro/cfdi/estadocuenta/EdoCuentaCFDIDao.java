package mx.com.afirme.midas2.dao.reaseguro.cfdi.estadocuenta;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.reaseguro.cfdi.Contrato;
import mx.com.afirme.midas2.domain.reaseguro.cfdi.Folio;
import mx.com.afirme.midas2.domain.reaseguro.cfdi.estadocuenta.EstadoCuenta;

@Local
public interface EdoCuentaCFDIDao {
	
	public void insertaComprobanteEC(String serie, BigDecimal folio, String descripcionContrato, String descripcionSuscripcion, String descripcionRamo
			, Date fechaInicioPeriodo, Date fechaFinPeriodo, String descripcionMoneda, BigDecimal idReasCoas, BigDecimal total);
		
	public void insertaConceptoEC(String serie, Folio folio, Integer secuencia, String textoPeriodoAnterior, String descripcionConcepto
			, BigDecimal debe, BigDecimal haber, BigDecimal saldo);
	
	public void insertaConceptoFiscal(String serie, Folio folio, String descripcionConcepto, BigDecimal valor);
	
	public void generaSolicitudCancelacionCFDI (String llaveComprobante);
	
	
	public List<EstadoCuenta> filtrar(Contrato contrato, EstadoCuenta estadoCuenta, BigDecimal folio);
		
	public Integer validaFolio (BigDecimal estadoCuentaId, Integer esEgreso, BigDecimal folio);
	
	public EstadoCuenta guardaEstadoCuenta (EstadoCuenta estadoCuenta);
	
}
