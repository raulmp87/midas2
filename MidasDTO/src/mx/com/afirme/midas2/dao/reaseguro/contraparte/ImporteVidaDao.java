package mx.com.afirme.midas2.dao.reaseguro.contraparte;

import java.math.BigDecimal;
import java.util.Date;

import javax.ejb.Local;

import mx.com.afirme.midas.reaseguro.reportes.reportercscontraparte.CargaContraParteDTO;

@Local
public interface ImporteVidaDao {
	
	void insertaSiniestrosPendientes(String poliza, Integer moneda, Integer anio_siniestro, Integer num_siniestro, BigDecimal reserva,
			BigDecimal costo_siniestro, BigDecimal afirme, BigDecimal hannover_life, BigDecimal mafre, String status, Date fCorte, Date inicio_vigencia, String cobertura);
	
	void insertaPolizasFacultadas(String poliza, Integer moneda, Integer anio_siniestro, Integer num_siniestro, BigDecimal reserva,
			BigDecimal costo_siniestro, BigDecimal afirme, BigDecimal monto_distribucion, String status, String rgre, String reasegurador, Date fCorte);

	void insertaContratosAutomaticos(String contrato, String reaseguradores, String rgre, BigDecimal participacion, Date vigencia_ini, Date vigencia_fin, Date fCorte, String cobertura);
	
	void insertaDistribucionSise(Integer siniestro, String reasegurador, String rgre, BigDecimal reserva, String moneda, BigDecimal tipoCambio, Date fCorte);
	
	void insertaAjustes(Integer siniestro, String reasegurador, String cnsf, String moneda, BigDecimal reserva, String anio, String sistema, BigDecimal tipoCambio, Date fCorte);
	
	int getRegistros(Date fechaCorte, String reporte);
	
	int estatusCarga(Date fechaCorte, String reporte);
	
	void borraRegistros(Date fechaCorte, String reporte);
	
	void insertaEstatusCarga(CargaContraParteDTO cargaDTO);
	
	void actualizaEstatusCarga(CargaContraParteDTO cargaDTO);
}
