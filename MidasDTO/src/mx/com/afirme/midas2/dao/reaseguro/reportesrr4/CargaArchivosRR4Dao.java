package mx.com.afirme.midas2.dao.reaseguro.reportesrr4;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.catalogos.esquemasreas.EsquemasDTO;
import mx.com.afirme.midas2.domain.catalogos.reaseguradorcnsf.ReaseguradorCnsf;
import mx.com.afirme.midas2.domain.catalogos.reaseguradorcnsf.ReaseguradorCnsfMov;

@Local
public interface CargaArchivosRR4Dao {
	
	public void save(EsquemasDTO entity);
	
	public int deleteEsquemas(int anio, int mes, int dia , int negocio);
	
	public void save(ReaseguradorCnsfMov entity);
	
	public void save(ReaseguradorCnsf entity);
	
	public int deleteReaseguradorMovs(Date fechacorte);
	
	public List<ReaseguradorCnsf> findByCNSF(String value);
	
	public List<BigDecimal> getMaxCER(String propertyName,
			final Object value,String propertyName2,
			final Object value2);
	
	public ReaseguradorCnsfMov findByIdReaseguradorCnsf(BigDecimal value, Date fechaCorte);
	
	public ReaseguradorCnsfMov findByCveReaseguradorCnsf(String cveCnsf, Date corte); 
	
}
