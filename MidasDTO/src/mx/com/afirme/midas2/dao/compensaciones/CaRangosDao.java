/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaRangos;

@Local
public interface CaRangosDao {
	public void save(CaRangos entity);
	   public void delete(CaRangos entity);
		public CaRangos update(CaRangos entity);
		public CaRangos findById( Long id);
		public List<CaRangos> findByProperty(String propertyName, Object value);
		public List<CaRangos> findByNombre(Object nombre);
		public List<CaRangos> findByValorminimo(Object valorminimo);
		public List<CaRangos> findByValormaximo(Object valormaximo);
		public List<CaRangos> findByUsuario(Object usuario);
		public List<CaRangos> findByBorradologico(Object borradologico);
		public List<CaRangos> findByValorcompensacion(Object valorcompensacion);
		public List<CaRangos> findAll();
		public List<CaRangos> findAllbyParametrosGralesId(Long parametrosGralesId);
}
