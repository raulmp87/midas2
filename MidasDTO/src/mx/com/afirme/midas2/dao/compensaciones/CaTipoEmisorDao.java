/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaTipoEmisor;

@Local
public interface CaTipoEmisorDao {
    
	public void save(CaTipoEmisor entity);
    public void delete(CaTipoEmisor entity);
	public CaTipoEmisor update(CaTipoEmisor entity);
	public CaTipoEmisor findById( Long id);
	public List<CaTipoEmisor> findByProperty(String propertyName, Object value);
	public List<CaTipoEmisor> findByNombre(Object nombre);
	public List<CaTipoEmisor> findByValor(Object valor);
	public List<CaTipoEmisor> findByUsuario(Object usuario);
	public List<CaTipoEmisor> findByBorradologico(Object borradologico);
	public List<CaTipoEmisor> findAll();	
	
}
