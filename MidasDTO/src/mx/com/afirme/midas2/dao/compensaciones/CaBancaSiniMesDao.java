package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;
import javax.ejb.Local;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaSiniMes;

@Local
public interface CaBancaSiniMesDao {

	public void save(CaBancaSiniMes entity);
	public void delete(CaBancaSiniMes entity);
	public CaBancaSiniMes update(CaBancaSiniMes entity);
	public CaBancaSiniMes findById(Long id);
	public List<CaBancaSiniMes> findByProperty(String propertyName,final Object value);
	public int deleteRecordsByProperty(String propertyName, Object value);

}
