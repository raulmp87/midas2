/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaBaseCalculo;

@Local
public interface CaBaseCalculoDao {
	public void save(CaBaseCalculo entity);
	public void delete(CaBaseCalculo entity);
	public CaBaseCalculo update(CaBaseCalculo entity);
	public CaBaseCalculo findById( Long id);
	public List<CaBaseCalculo> findByProperty(String propertyName, Object value);
	public List<CaBaseCalculo> findByNombre(Object nombre);
	public List<CaBaseCalculo> findByValor(Object valor);
	public List<CaBaseCalculo> findByUsuario(Object usuario);
	public List<CaBaseCalculo> findByBorradologico(Object borradologico);
	public List<CaBaseCalculo> findAll();
	public List<CaBaseCalculo> findByProperties(String propertyName, int ...isValues);
}
