/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Marzo 2016
 * @author Mario Dominguez
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaConfiguracionBanca;

@Local
public interface CaConfiguracionBancaDao {
	public void save(CaConfiguracionBanca entity);
	public void delete(CaConfiguracionBanca entity);
	public CaConfiguracionBanca update(CaConfiguracionBanca entity);
	public CaConfiguracionBanca findById( Long id);
	public List<CaConfiguracionBanca> findByProperty(String propertyName, Object value);
	public List<CaConfiguracionBanca> findAll();
}
