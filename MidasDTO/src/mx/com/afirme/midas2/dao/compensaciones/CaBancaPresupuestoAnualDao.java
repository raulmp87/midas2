package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaBancaPresupuestoAnual;

@Local
public interface CaBancaPresupuestoAnualDao {	
	public void save(CaBancaPresupuestoAnual entity);
	public void delete(CaBancaPresupuestoAnual entity);
	public CaBancaPresupuestoAnual update(CaBancaPresupuestoAnual entity);
	public CaBancaPresupuestoAnual findById(Long id);
	public List<CaBancaPresupuestoAnual> findByProperty(String propertyName,Object value);
	public List<CaBancaPresupuestoAnual> findAll();
	public int deleteRecordsByProperty(String propertyName, Object value);
}
