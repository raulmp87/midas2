/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.negociovida.DatosSeycos;

@Local
public interface NegocioVidaDao {	
	public List<DatosSeycos> findAgenteSeycosConPromotor(DatosSeycos datosSeycos);
}