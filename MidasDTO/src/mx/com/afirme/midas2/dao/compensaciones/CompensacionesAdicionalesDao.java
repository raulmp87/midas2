package mx.com.afirme.midas2.dao.compensaciones;

import javax.ejb.Local;
import java.util.List;
import mx.com.afirme.midas2.domain.compensaciones.CaCompensacion;
import mx.com.afirme.midas2.domain.compensaciones.CaConfiguracionCompensacionLista;
import mx.com.afirme.midas2.domain.compensaciones.CaEntidadPersona;
import mx.com.afirme.midas2.domain.compensaciones.CaParametros;
import mx.com.afirme.midas2.dto.compensaciones.Respuesta;
import mx.com.afirme.midas2.dto.compensaciones.FiltroCompensacion;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;

@Local
public interface CompensacionesAdicionalesDao {

	public Respuesta guardarCompensacionAdicional(CaCompensacion compensacionAdicional);
	
	public Long totalCompensaciones(FiltroCompensacion filtro);
	
	public List<CaCompensacion> buscarCompensaciones(FiltroCompensacion filtro);
	
	public List<CaConfiguracionCompensacionLista> buscarEntidadesPersona(FiltroCompensacion filtro);
	
	public List<CaParametros> buscarParametrosGenerales(FiltroCompensacion filtro);
	
	
}
