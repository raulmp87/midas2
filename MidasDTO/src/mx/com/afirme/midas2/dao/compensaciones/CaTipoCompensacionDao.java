/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaTipoCompensacion;

@Local
public interface CaTipoCompensacionDao {
	
	public void save(CaTipoCompensacion entity);
    public void delete(CaTipoCompensacion entity);
	public CaTipoCompensacion update(CaTipoCompensacion entity);
	public CaTipoCompensacion findById( Long id);
	public List<CaTipoCompensacion> findByProperty(String propertyName, Object value);
	public List<CaTipoCompensacion> findByNombre(Object nombre);
	public List<CaTipoCompensacion> findByValor(Object valor);
	public List<CaTipoCompensacion> findByUsuario(Object usuario);
	public List<CaTipoCompensacion> findByBorradologico(Object borradologico);
	public List<CaTipoCompensacion> findAll();
	
}
