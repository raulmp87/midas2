/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaLineaNegocio;

@Local
public interface CaLineaNegocioDao {
	
	public void save(CaLineaNegocio entity);
    public void delete(CaLineaNegocio entity);
	public CaLineaNegocio update(CaLineaNegocio entity);
	public CaLineaNegocio findById( Long id);
	public List<CaLineaNegocio> findByProperty(String propertyName, Object value);
	public List<CaLineaNegocio> findByContraprestacion(Object contraprestacion);
	public List<CaLineaNegocio> findByNegocioId(Object negocioId);
	public List<CaLineaNegocio> findByValorporcentaje(Object valorporcentaje);
	public List<CaLineaNegocio> findByUsuario(Object usuario);
	public List<CaLineaNegocio> findByBorradologico(Object borradologico);
	public List<CaLineaNegocio> findByValorid(Object valorid);
	public List<CaLineaNegocio> findByValordescripcion(Object valordescripcion);
	public CaLineaNegocio findByPropertyUniqueResult(String propertyName, Object value);
	public CaLineaNegocio findByNegocioIdValorId(Long idNegocio, String idValor);
	public List<CaLineaNegocio> findAll();
	public List<CaLineaNegocio> findByNegocioIdandContraprestacion(Long negocioId, boolean contraprestacion);
    
}
