/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaTipoContrato;

@Local
public interface CaTipoContratoDao {
	
	public void save(CaTipoContrato entity);
    public void delete(CaTipoContrato entity);
	public CaTipoContrato update(CaTipoContrato entity);
	public CaTipoContrato findById( Long id);
	public List<CaTipoContrato> findByProperty(String propertyName, Object value);
	public List<CaTipoContrato> findByNombre(Object nombre);
	public List<CaTipoContrato> findByValor(Object valor);
	public List<CaTipoContrato> findByUsuario(Object usuario);
	public List<CaTipoContrato> findByBorradologico(Object borradologico);
	public List<CaTipoContrato> findAll();
	
}
