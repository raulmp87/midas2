/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaTipoMoneda;

@Local
public interface CaTipoMonedaDao {
	public void save(CaTipoMoneda entity);
    public void delete(CaTipoMoneda entity);
	public CaTipoMoneda update(CaTipoMoneda entity);
	public CaTipoMoneda findById( Long id);
	public List<CaTipoMoneda> findByProperty(String propertyName, Object value);
	public List<CaTipoMoneda> findByNombre(Object nombre);
	public List<CaTipoMoneda> findByValor(Object valor);
	public List<CaTipoMoneda> findByUsuario(Object usuario);
	public List<CaTipoMoneda> findByBorradologico(Object borradologico);
	public List<CaTipoMoneda> findAll();	
}
