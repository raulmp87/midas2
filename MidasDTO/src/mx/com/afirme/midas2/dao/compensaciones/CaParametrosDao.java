/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaCompensacion;
import mx.com.afirme.midas2.domain.compensaciones.CaEntidadPersona;
import mx.com.afirme.midas2.domain.compensaciones.CaParametros;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoCompensacion;
import mx.com.afirme.midas2.dto.impresiones.ReporteNegCompensacionesDTO;

@Local
public interface CaParametrosDao {

	public List<CaParametros> findAllbyCompensadicioId(BigDecimal compensAdicioId);
	public void save(CaParametros entity);
	public void delete(CaParametros entity);
	public CaParametros update(CaParametros entity);
	public CaParametros findById(Long id);
	public List<CaParametros> findByProperty(String propertyName,Object value);
	public List<CaParametros> findByMontoutilidades(Object montoutilidades);
	public List<CaParametros> findByPorcentajepago(Object porcentajepago);
	public List<CaParametros> findByMontopago(Object montopago);
	public List<CaParametros> findByPorcentajeagente(Object porcentajeagente);
	public List<CaParametros> findByPorcentajepromotor(Object porcentajepromotor);
	public List<CaParametros> findByEmisioninterna(Object emisioninterna);
	public List<CaParametros> findByEmisionexterna(Object emisionexterna);
	public List<CaParametros> findByTopemaximo(Object topemaximo);
	public List<CaParametros> findByUsuario(Object usuario);
	public List<CaParametros> findByBorradologico(Object borradologico);
	public List<CaParametros> findByParametroactivo(Object parametroactivo);
	public List<CaParametros> findAll();
	public CaParametros findByCompensAdicioIdTipoCompensId(Long compensAdicioId, Long tipoCompensId);
	public CaParametros findByTipocompeidCompeIdPersoId(CaTipoCompensacion tipoCompensacion,CaCompensacion compensacion,CaEntidadPersona entidadPersona);
	public List<CaParametros> findAllbyCompensacionEntidadpersona(CaCompensacion compensacion,CaEntidadPersona entidadPersona);
	public  List<ReporteNegCompensacionesDTO > findByIdNegocio(Long negocioId);
	public  List<ReporteNegCompensacionesDTO > findByIdNegocionBS(Long negocioId);
}
