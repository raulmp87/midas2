/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaTipoRango;

@Local
public interface CaTipoRangoDao {
	public void save(CaTipoRango entity);
    public void delete(CaTipoRango entity);
	public CaTipoRango update(CaTipoRango entity);
	public CaTipoRango findById( Long id);
	public List<CaTipoRango> findByProperty(String propertyName, Object value);
	public List<CaTipoRango> findByNombre(Object nombre);	
	public List<CaTipoRango> findByValor(Object valor);	
	public List<CaTipoRango> findByUsuario(Object usuario);
	public List<CaTipoRango> findByBorradologico(Object borradologico);
	public List<CaTipoRango> findAll();
}
