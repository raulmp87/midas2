/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaTipoAutorizacion;

@Local
public interface CaTipoAutorizacionDao{
	
	public void save(CaTipoAutorizacion entity);
    public void delete(CaTipoAutorizacion entity);
	public CaTipoAutorizacion update(CaTipoAutorizacion entity);
	public CaTipoAutorizacion findById( Long id);
	public List<CaTipoAutorizacion> findByProperty(String propertyName, Object value);
	public List<CaTipoAutorizacion> findByNombre(Object nombre);
	public List<CaTipoAutorizacion> findByValor(Object valor);
	public List<CaTipoAutorizacion> findByUsuario(Object usuario);
	public List<CaTipoAutorizacion> findByBorradologico(Object borradologico);
	public List<CaTipoAutorizacion> findAll();	
	
}
