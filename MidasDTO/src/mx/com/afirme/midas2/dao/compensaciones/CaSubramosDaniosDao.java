/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaSubramosDanios;
import mx.com.afirme.midas2.dto.compensaciones.CompensacionesDTO;
import mx.com.afirme.midas2.dto.compensaciones.SubRamosDaniosViewDTO;

@Local
public interface CaSubramosDaniosDao {
	
	public void save(CaSubramosDanios entity);
    public void delete(CaSubramosDanios entity);
	public CaSubramosDanios update(CaSubramosDanios entity);
	public CaSubramosDanios findById( Long id);
	public List<CaSubramosDanios> findByProperty(String propertyName, Object value);
	public List<CaSubramosDanios> findByPorcentajecompensacion(Object porcentajecompensacion);
	public List<CaSubramosDanios> findByUsuario(Object usuario);
	public List<CaSubramosDanios> findByBorradologico(Object borradologico);
	public List<CaSubramosDanios> findAll();
	public void calcularProvisionDanios(SubRamosDaniosViewDTO subDaniosViewDTO, CompensacionesDTO compensacionesDTO);
	public void  getViewSubRamos(SubRamosDaniosViewDTO subDaniosViewDTO, CompensacionesDTO compensacionesDTO);
	public int deleteRecordsSubRamosDanios(Long cotizacionId, Long entidadPersonaId, Long compensacionId);
}
