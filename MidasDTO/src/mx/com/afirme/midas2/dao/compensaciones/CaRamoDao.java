/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaRamo;

@Local
public interface CaRamoDao {
    
	public void save(CaRamo entity);    
    public void delete(CaRamo entity);
	public CaRamo update(CaRamo entity);
	public CaRamo findById( Long id);
	public List<CaRamo> findByProperty(String propertyName, Object value);
	public List<CaRamo> findByNombre(Object nombre);
	public List<CaRamo> findByIdentificador(Object identificador);
	public List<CaRamo> findByValor(Object valor);
	public List<CaRamo> findByUsuario(Object usu);
	public List<CaRamo> findByBorradologico(Object borradologico);
	public List<CaRamo> findAll();
	
}
