/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaTipoProvision;

@Local
public interface CaTipoProvisionDao {
	public void save(CaTipoProvision entity);
    public void delete(CaTipoProvision entity);
	public CaTipoProvision update(CaTipoProvision entity);
	public CaTipoProvision findById( Long id);
	public List<CaTipoProvision> findByProperty(String propertyName, Object value);
	public List<CaTipoProvision> findByNombre(Object nombre);
	public List<CaTipoProvision> findByValor(Object valor);
	public List<CaTipoProvision> findByUsuario(Object usuario);
	public List<CaTipoProvision> findByBorradologico(Object borradologico);
	public List<CaTipoProvision> findAll();	
}
