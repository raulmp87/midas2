/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaTipoEntidad;

@Local
public interface CaTipoEntidadDao {
	
	public void save(CaTipoEntidad entity);
    public void delete(CaTipoEntidad entity);
	public CaTipoEntidad update(CaTipoEntidad entity);
	public CaTipoEntidad findById( Long id);
	public List<CaTipoEntidad> findByProperty(String propertyName, Object value);
	public List<CaTipoEntidad> findByNombre(Object nombre);
	public List<CaTipoEntidad> findByValor(Object valor);
	public List<CaTipoEntidad> findByUsuario(Object usuario);
	public List<CaTipoEntidad> findByBorradologico(Object borradologico);
	public List<CaTipoEntidad> findAll();
    
}
