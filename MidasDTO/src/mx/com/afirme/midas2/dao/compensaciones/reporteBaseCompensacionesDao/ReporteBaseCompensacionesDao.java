package mx.com.afirme.midas2.dao.compensaciones.reporteBaseCompensacionesDao;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaPagosSaldosDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesCompDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaSegurosDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesPagoCompContra;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesCompDTO.DatosReporteCompParametrosDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesDTO.DatosReporteEstadoCuentaParametrosDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesDerPolDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesDerPolDTO.DatosRepDerPolParametrosDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesPagDevengarDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesPagDevengarDTO.DatosReportePagDevDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesPagoCompContra.DatosReportePagoCompContraParametrosDTO;

@Local
public interface ReporteBaseCompensacionesDao {
	/**
	 * Obtiene la lista de bases de emisión.
	 * @param parametros
	 * @return
	 * @throws Exception
	 */
	public List<CaReportesCompDTO> obtenerRepCompensaciones(DatosReporteCompParametrosDTO parametros)  throws Exception;
	
	public List<CaReportesDerPolDTO> obtenerRepDerechoPoliza(DatosRepDerPolParametrosDTO parametros)throws Exception;
	
	public List<CaReportesPagDevengarDTO> obtenerRepPagadasDevengar(DatosReportePagDevDTO parametros)throws Exception;
	
	public List<CaReportesDTO> obtenerRepEstadoCuenta(DatosReporteEstadoCuentaParametrosDTO parametros) throws Exception;
	
	public List<CaReportesPagoCompContra> obtenerRepPagoCompContra(DatosReportePagoCompContraParametrosDTO parametros) throws Exception;
	
	public List<CaBancaSegurosDTO> obtenerRepBancaSeguros(Map<String,Object> parametros) throws Exception;
	
	public List<CaPagosSaldosDTO> obtenerRepPagosSaldos(Map<String,Object> parametros) throws Exception;
   
}
