/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.compensaciones.CaEntidadPersona;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.DatosSeycos;

@Local
public interface CaEntidadPersonaDao {
	public void save(CaEntidadPersona entity);   
    public void delete(CaEntidadPersona entity);
	public CaEntidadPersona update(CaEntidadPersona entity);
	public CaEntidadPersona findById( Long id);
	public List<CaEntidadPersona> findByProperty(String propertyName, Object value);
	public CaEntidadPersona findByPropertySingleResult(String propertyName, final Object value);
	public List<CaEntidadPersona> findByCompensAdicioId(Long compensAdicioId);
	public List<CaEntidadPersona> findByUsuario(Object usuario);
	public List<CaEntidadPersona> findByNombres(Object nombres);
	public List<CaEntidadPersona> findByParametrosId(Object parametrosId);
	public List<CaEntidadPersona> findByApaterno(Object apaterno);
	public List<CaEntidadPersona> findByAmaterno(Object amaterno);
	public List<CaEntidadPersona> findByBorradologico(Object borradologico);
	public List<CaEntidadPersona> findAll();
	public CaEntidadPersona findByCompensacionAndTipoEntidadAndClave(Long compensAdicioId, String clave, Long valorClave,Long tipoEntidad);
	public CaEntidadPersona findByCompensacionAndTipoEntidadAndClave(Long compensAdicioId, DatosSeycos datosSeycos, Long tipoEntidad);
	public List<CaEntidadPersona> findByAgente(Agente agente);
}
