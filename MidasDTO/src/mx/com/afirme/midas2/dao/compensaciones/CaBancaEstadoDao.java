/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Abril 2016
 * @author Mario Dominguez
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaBancaEstado;

@Local
public interface CaBancaEstadoDao {
	public void save(CaBancaEstado entity);
	public void delete(CaBancaEstado entity);
	public CaBancaEstado update(CaBancaEstado entity);
	public CaBancaEstado findById( Long id);
	public List<CaBancaEstado> findByProperty(String propertyName, Object value);
	public List<CaBancaEstado> findAll();
}
