/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaCompensacion;
import mx.com.afirme.midas2.dto.compensaciones.CompensacionesDTO;
import mx.com.afirme.midas2.dto.compensaciones.ProductoVidaDTO;

@Local
public interface CaCompensacionDao {
	public CaCompensacion save(CaCompensacion entity);
    public void delete(CaCompensacion entity);
	public CaCompensacion update(CaCompensacion entity);
	public CaCompensacion findById( Long id);
	public List<CaCompensacion> findByProperty(String propertyName, Object value);
	public List<CaCompensacion> findByPolizaId(Object polizaId);
	public List<CaCompensacion> findByNegocioId(Object negocioId);
	public List<CaCompensacion> findByCotizacionId(Object cotizacionId);
	public List<CaCompensacion> findByContraprestacion(Object contraprestacion);
	public List<CaCompensacion> findByCuadernoconcurso(Object cuadernoconcurso);
	public List<CaCompensacion> findByConvenioespecial(Object convenioespecial);
	public List<CaCompensacion> findByRetroactivo(Object retroactivo);
	public List<CaCompensacion> findByModificable(Object modificable);
	public List<CaCompensacion> findByExcepciondocumentos(Object excepciondocumentos);
	public List<CaCompensacion> findByCompensacionporsubramos(Object compensacionporsubramos);
	public List<CaCompensacion> findByUsuario(Object usuario);
	public List<CaCompensacion> findByBorradologico(Object borradologico);
	public BigDecimal findIdCompensAdicioNext();
	public List<CaCompensacion> findAll();
	public CaCompensacion findByNegocioIdandContraprestacion(Long idNegocio, boolean contraprestacion);
	public CaCompensacion findByRamoAndFieldAndContraprestacion(Long idRamo,  String field, Object valueField, boolean contraprestacion);
	public List<CaCompensacion> listCompensaciones(Long idRamo,  String field, Object valueField);
	public List<CompensacionesDTO> getlistRecibosPendientesPagar(Long idPoliza);
	public void setConfiguracionByIdRecibo(int idRecibo);
	public void setConfiguracionByIdCompensacionAut(int compensacionId);
	public List<ProductoVidaDTO> getProductosVida();
}
