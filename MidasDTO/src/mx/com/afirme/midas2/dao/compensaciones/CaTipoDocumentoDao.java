/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaTipoDocumento;

@Local
public interface CaTipoDocumentoDao {
	
	public void save(CaTipoDocumento entity);
    public void delete(CaTipoDocumento entity);
	public CaTipoDocumento update(CaTipoDocumento entity);
	public CaTipoDocumento findById( Long id);
	public List<CaTipoDocumento> findByProperty(String propertyName, Object value);
	public List<CaTipoDocumento> findByNombre(Object nombre);
	public List<CaTipoDocumento> findByValor(Object valor);
	public List<CaTipoDocumento> findByUsuario(Object usuario);
	public List<CaTipoDocumento> findByBorradologico(Object borradologico);
	public List<CaTipoDocumento> findAll();	
    
}
