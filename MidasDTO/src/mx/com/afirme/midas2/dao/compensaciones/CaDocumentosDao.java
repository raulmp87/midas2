/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaDocumentos;

@Local
public interface CaDocumentosDao {
	public void save(CaDocumentos entity);
    public void delete(CaDocumentos entity);
	public CaDocumentos update(CaDocumentos entity);
	public CaDocumentos findById( Long id);
	public List<CaDocumentos> findByProperty(String propertyName, Object value);
	public List<CaDocumentos> findByNombre(Object nombre);
	public List<CaDocumentos> findByValor(Object valor);
	public List<CaDocumentos> findByUsuario(Object usuario);
	public List<CaDocumentos> findByBorradologico(Object borradologico);
	public List<CaDocumentos> findAll();	
}
