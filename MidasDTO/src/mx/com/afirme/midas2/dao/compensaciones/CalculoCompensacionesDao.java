package mx.com.afirme.midas2.dao.compensaciones;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;

import mx.com.afirme.midas2.dto.compensaciones.CalculoCompensacionesDTO;

/**
 * 
 * @author Luis Ibarra
 * @since 30/Dic/2015
 *
 */
@Local
public interface CalculoCompensacionesDao {
	
	/**
	 * @author Luis Ibarra
	 * @param idPoliza Id de poliza
	 * @param tipoEndoso Tipo del endoso
	 * @return Calculo de compensaciones
	 */
	public List<CalculoCompensacionesDTO> calcularCompensacion(long idToPoliza);
	
    public int provisionarPorUtilidadCompensacion(long idToPoliza, long entidadPersonaId, BigDecimal montoPago,BigDecimal montoTotal);
	
	public int generarOrdenUtilidad(long idToPoliza, long entidadPersonaId, BigDecimal montoPago);
	
}
