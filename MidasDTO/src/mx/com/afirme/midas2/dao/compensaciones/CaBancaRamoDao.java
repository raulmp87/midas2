/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Abril 2016
 * @author Mario Dominguez
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaBancaRamo;

@Local
public interface CaBancaRamoDao {
	public void save(CaBancaRamo entity);
	public void delete(CaBancaRamo entity);
	public CaBancaRamo update(CaBancaRamo entity);
	public CaBancaRamo findById( Long id);
	public List<CaBancaRamo> findByProperty(String propertyName, Object value);
	public List<CaBancaRamo> findAll();
}
