/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaBitacora;

@Local
public interface CaBitacoraDao {
	public void save(CaBitacora entity);
    public void delete(CaBitacora entity);
	public CaBitacora update(CaBitacora entity);
	public CaBitacora findById( Long id);
	public List<CaBitacora> findByProperty(String propertyName, Object value);
	public List<CaBitacora> findByUsuario(Object usuario);
	public List<CaBitacora> findByHora(Object hora);
	public List<CaBitacora> findByMovimiento(Object movimiento);
	public List<CaBitacora> findByValorAnterior(Object valorAnterior);
	public List<CaBitacora> findByValorPosterior(Object valorPosterior);
	public List<CaBitacora> findAll();		
}
