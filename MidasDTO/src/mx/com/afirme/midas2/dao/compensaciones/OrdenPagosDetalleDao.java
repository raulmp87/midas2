package mx.com.afirme.midas2.dao.compensaciones;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.OrdenPagosDetalle;

/**
 * Local interface for OrdenPagosDetalleFacade.
 * 
 * @author MyEclipse Persistence Tools
 */
@Local
public interface OrdenPagosDetalleDao {
	/**
	 * Perform an initial save of a previously unsaved OrdenPagosDetalle
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            OrdenPagosDetalle entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save( OrdenPagosDetalle entity);

	/**
	 * Delete a persistent OrdenPagosDetalle entity.
	 * 
	 * @param entity
	 *            OrdenPagosDetalle entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(OrdenPagosDetalle entity);

	/**
	 * Persist a previously saved OrdenPagosDetalle entity and return it or a
	 * copy of it to the sender. A copy of the OrdenPagosDetalle entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            OrdenPagosDetalle entity to update
	 * @return OrdenPagosDetalle the persisted OrdenPagosDetalle entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public OrdenPagosDetalle update(OrdenPagosDetalle entity);

	public OrdenPagosDetalle findById(Long id);

	/**
	 * Find all OrdenPagosDetalle entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the OrdenPagosDetalle property to query
	 * @param value
	 *            the property value to match
	 * @return List<OrdenPagosDetalle> found by query
	 */
	public List<OrdenPagosDetalle> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all OrdenPagosDetalle entities.
	 * 
	 * @return List<OrdenPagosDetalle> all OrdenPagosDetalle entities
	 */
	public List<OrdenPagosDetalle> findAll();
	
	public  List<OrdenPagosDetalle >  obtenerOrdenPagosDetalleProcesoPorId(Long liquidacionId);
	
	public  List<OrdenPagosDetalle >  obtenerOrdenPagosDetalleGeneradoPorId(Long ordenPagoId);

	public Boolean excluirRecibosOrdenPagosDetallePorId(Long ordenPagoId ,Long reciboId );
	
	public Boolean actualizarEstatusOrdenPagosDetalle(Long ordenPagoId);
	
	public Boolean incluirRecibosOrdenPagosDetallePorId(Long ordenPagoId ,Long reciboId );
	
	public Boolean actualizarEstatusOrdenPagosDetalleAGenerado(Long ordenPagoId);
	
	public List<OrdenPagosDetalle> obtenerHonorariosOrdenPagos(Long idAgente, String estatusRecibo);

	public BigDecimal consultarPagoPorUtilidad(Long compensacionId, Long entidadPersonaId);
	
	public BigDecimal consultarProvisionPorUtilidad(Long compensacionId, Long entidadPersonaId);
	
	public String guardarSiniestralidad(String datos);
}