/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Marzo 2016
 * @author Mario Dominguez
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaBancaGerencia;

@Local
public interface CaBancaGerenciaDao {
	public void save(CaBancaGerencia entity);
	public void delete(CaBancaGerencia entity);
	public CaBancaGerencia update(CaBancaGerencia entity);
	public CaBancaGerencia findById( Long id);
	public List<CaBancaGerencia> findByProperty(String propertyName, Object value);
	public List<CaBancaGerencia> findAll();
}