/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Marzo 2016
 * @author Mario Dominguez
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaFrecuenciaPagos;

@Local
public interface CaFrecuenciaPagosDao {
	public void save(CaFrecuenciaPagos entity);
	public void delete(CaFrecuenciaPagos entity);
	public CaFrecuenciaPagos update(CaFrecuenciaPagos entity);
	public CaFrecuenciaPagos findById( Long id);
	public List<CaFrecuenciaPagos> findByProperty(String propertyName, Object value);
	public List<CaFrecuenciaPagos> findAll();
}
