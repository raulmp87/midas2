package mx.com.afirme.midas2.dao.compensaciones;

import javax.ejb.Local;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaPrimPagCP;

@Local
public interface CaBancaPrimPagCPDao {

	public void save(CaBancaPrimPagCP entity);
	public void delete(CaBancaPrimPagCP entity);
	public CaBancaPrimPagCP update(CaBancaPrimPagCP entity);
	public CaBancaPrimPagCP findById( Long id);

}
