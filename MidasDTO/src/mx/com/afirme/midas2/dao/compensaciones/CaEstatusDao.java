/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaEstatus;

@Local
public interface CaEstatusDao {
	public void save(CaEstatus entity);
    public void delete(CaEstatus entity);
	public CaEstatus update(CaEstatus entity);
	public CaEstatus findById( Long id);
	public List<CaEstatus> findByProperty(String propertyName, Object value);
	public List<CaEstatus> findByNombre(Object nombre);
	public List<CaEstatus> findByValor(Object valor);
	public List<CaEstatus> findByUsuario(Object usu);
	public List<CaEstatus> findByBorradologico(Object borradologico);
	public List<CaEstatus> findAll();	
}
