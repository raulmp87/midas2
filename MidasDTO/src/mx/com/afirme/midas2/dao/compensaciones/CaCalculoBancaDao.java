/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaCalculoBanca;

@Local
public interface CaCalculoBancaDao {
	public void save(CaCalculoBanca entity);
    public void delete(CaCalculoBanca entity);
	public CaCalculoBanca update(CaCalculoBanca entity);
	public CaCalculoBanca findById( Long id);
	public List<CaCalculoBanca> findByProperty(String propertyName, Object value);
	public List<CaCalculoBanca> findByLineanegocioId(Object lineanegocioId);
	public List<CaCalculoBanca> findByMonto(Object monto);
	public List<CaCalculoBanca> findByPorcentaje(Object porcentaje);
	public List<CaCalculoBanca> findByUsuario(Object usuario);
	public List<CaCalculoBanca> findByBorradologico(Object borradologico);
	public List<CaCalculoBanca> findAll();
}
