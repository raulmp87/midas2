/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaTipoCondicionCalculo;

@Local
public interface CaTipoCondicionCalculoDao {
	
	public void save(CaTipoCondicionCalculo entity);
    public void delete(CaTipoCondicionCalculo entity);
	public CaTipoCondicionCalculo update(CaTipoCondicionCalculo entity);
	public CaTipoCondicionCalculo findById( Long id);
	public List<CaTipoCondicionCalculo> findByProperty(String propertyName, Object value);
	public List<CaTipoCondicionCalculo> findByNombre(Object nombre);
	public List<CaTipoCondicionCalculo> findByValor(Object valor);
	public List<CaTipoCondicionCalculo> findByUsuario(Object usuario);
	public List<CaTipoCondicionCalculo> findByBorradologico(Object borradologico);
	public List<CaTipoCondicionCalculo> findAll();
	
}
