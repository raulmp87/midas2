/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaAutorizacion;

@Local
public interface CaAutorizacionDao {
    public void save(CaAutorizacion entity);
    public void delete(CaAutorizacion entity);
	public CaAutorizacion update(CaAutorizacion entity);
	public CaAutorizacion findById( Long id);
	public List<CaAutorizacion> findByProperty(String propertyName, Object value);
	public List<CaAutorizacion> findByValor(Object valor);
	public List<CaAutorizacion> findByUsuario(Object usuario);
	public List<CaAutorizacion> findByBorradologico(Object borradologico);
	public List<CaAutorizacion> findAll();
	public List<CaAutorizacion> findByCompensacionid(Long idCompensacion);
	public List<CaAutorizacion> finByCompensidAndTipoautoid(Long compensId, Long tipoAutoId);
	public List<CaAutorizacion> finByCompensidTiposautosid(Long compensId, Long ...tipos);
}
