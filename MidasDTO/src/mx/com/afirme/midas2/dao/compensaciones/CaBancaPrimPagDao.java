package mx.com.afirme.midas2.dao.compensaciones;

import java.util.List;
import javax.ejb.Local;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaPrimPag;

@Local
public interface CaBancaPrimPagDao {

	public void save(CaBancaPrimPag entity);
	public void delete(CaBancaPrimPag entity);
	public CaBancaPrimPag update(CaBancaPrimPag entity);
	public CaBancaPrimPag findById(Long id);
	public List<CaBancaPrimPag> findByProperty(String propertyName,final Object value);
	public int deleteRecordsByProperty(String propertyName, Object value);

}
