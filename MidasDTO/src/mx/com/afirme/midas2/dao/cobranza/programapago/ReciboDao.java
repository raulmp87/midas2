package mx.com.afirme.midas2.dao.cobranza.programapago;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.cobranza.programapago.ToRecibo;

public interface ReciboDao extends Dao<Long, ToRecibo>{ 

}
