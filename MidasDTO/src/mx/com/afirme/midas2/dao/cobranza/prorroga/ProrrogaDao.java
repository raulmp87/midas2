package mx.com.afirme.midas2.dao.cobranza.prorroga;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.cobranza.prorroga.ToProrroga;

@Local
public interface ProrrogaDao extends Dao<Long, ToProrroga>{
	
	public ToProrroga findById(Long idProrroga);

}
