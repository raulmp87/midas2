package mx.com.afirme.midas2.dao.cobranza;

import java.util.List;
import javax.ejb.Local;

import mx.com.afirme.midas2.domain.cobranza.FiltroRecibos;
import mx.com.afirme.midas2.domain.cobranza.Recibos;


public interface NotificaFacturaReciboDAO {

	public Long obtenerTotalRecibos();
	
	public List<Recibos> buscarRecibos(FiltroRecibos filtro);
	
	public String registrarproceso(String descripcion);
}
