package mx.com.afirme.midas2.dao.cobranza.reportes;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.cobranza.reportes.CalendarioGonherDTO;
import mx.com.afirme.midas2.domain.cobranza.reportes.CargoAutoDomiTC;

@Local
public interface ReportesCobranzaDao extends EntidadDao{
	
	public List<CargoAutoDomiTC> getReporteCargoAutoDomiTC();
	
	public CalendarioGonherDTO getFechaSiguienteProgramacion();
	
	public void saveConfrimProgramacion(CalendarioGonherDTO calendarioGonherDTO);
}
