package mx.com.afirme.midas2.dao.cobranza.programapago;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.cobranza.programapago.ToDesgloseRecibo;

public interface DesgloseDao extends Dao<Long, ToDesgloseRecibo>{ 

}
