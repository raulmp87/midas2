package mx.com.afirme.midas2.dao.cobranza.recibos.consolidar;


import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.cobranza.recibos.consolidar.Consolidado;
import mx.com.afirme.midas2.domain.cobranza.recibos.consolidar.RecibosConsolidar;
import mx.com.afirme.midas2.domain.cobranza.recibos.consolidar.RelRecibosConsolidado;


@Local
public interface ConsolidarRecibosDao extends EntidadDao {

	public List<RecibosConsolidar> obtenerListadoRecibos(RecibosConsolidar reciboConsolidar );
	
	public List<Consolidado> obtenerRecibosConsolidados(Consolidado consolidado);
		
	public List<Consolidado> cancelarRecibosSeleccionados(RecibosConsolidar reciboConsolidar);
	
	public List<RelRecibosConsolidado> consolidarRecibosSeleccionados(RecibosConsolidar reciboConsolidar);
	
	public Object correrCFDConsolidado(Consolidado consolidado);

}




























