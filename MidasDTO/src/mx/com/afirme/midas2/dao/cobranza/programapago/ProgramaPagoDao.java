package mx.com.afirme.midas2.dao.cobranza.programapago;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.cobranza.programapago.ToProgPago;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.service.cobranza.programapago.ProgramaPagoService.Saldo;

@Local
public interface ProgramaPagoDao extends Dao<Long, ToProgPago>{
	
	public List<IncisoAutoCot> obtenerAseguradosCot(BigDecimal idCotizacion);
	
	public Map<String, String> simulaEmitePoliza(CotizacionDTO cotizacionDTO,
			String canalVenta, Integer recuotificaFlag);
	
	public Saldo obtenerSaldoOriginal(CotizacionDTO cotizacionDTO, BigDecimal idProgPago, Long idReferencia );
	
	Saldo obtenerSaldoOriginalPorInciso(CotizacionDTO cotizacion, BigDecimal idProgPago,Long numeroInciso);
	
	List<ClienteGenericoDTO> obtenerTodosLosAsegurados(BigDecimal idToCotizacion);
}
