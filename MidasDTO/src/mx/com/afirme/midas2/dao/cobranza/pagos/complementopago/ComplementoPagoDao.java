package mx.com.afirme.midas2.dao.cobranza.pagos.complementopago;

import java.util.List;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.cobranza.pagos.complementopago.ComplementoPago;
import mx.com.afirme.midas2.domain.cobranza.pagos.complementopago.IngresosAplicar;
import javax.ejb.Local;

@Local
public interface ComplementoPagoDao extends EntidadDao{

	public List<ComplementoPago> obtenerComplementosPago(ComplementoPago complementopago);
	public List<ComplementoPago> listarErrores(ComplementoPago complementoPago) throws Exception;
	public List<ComplementoPago> findByFilters(ComplementoPago complementoPago) throws Exception;
	public List<IngresosAplicar> consultaIngresosAplicar();
}
