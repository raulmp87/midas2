package mx.com.afirme.midas2.dao.operacionessapamis;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasCesvi;

@Local
public interface SapAlertasCesviDao {
	
	public void guardarAlertasCesvi(List<SapAlertasCesvi> alertasCesvi);
	
}
