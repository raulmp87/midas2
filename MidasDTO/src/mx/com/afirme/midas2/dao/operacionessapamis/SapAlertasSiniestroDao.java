package mx.com.afirme.midas2.dao.operacionessapamis;

import java.util.List;

import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasSiniestro;

import javax.ejb.Local;

@Local
public interface SapAlertasSiniestroDao {
	public void guardarAlertasSiniestros(List<SapAlertasSiniestro> alertasSiniestros);
}
