package mx.com.afirme.midas2.dao.operacionessapamis;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasOcra;

@Local
public interface SapAlertasOcraDao {
	public void guardarAlertasOcra(List<SapAlertasOcra> alertasOcra);
}
