package mx.com.afirme.midas2.dao.operacionessapamis;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.sapamis.otros.SapAmisControlEmision;

@Local
public interface SapAmisControlEmisionDao {
	
	public List<SapAmisControlEmision> findAllControlEmision();
	
	public void limpiarRegistrosControlEnvios(List<SapAmisControlEmision> listaEmisioneEliminar);
	
}
