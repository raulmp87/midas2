package mx.com.afirme.midas2.dao.operacionessapamis;

import java.util.List;

import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasScd;

import javax.ejb.Local;

@Local
public interface SapAlertasScdDao {
	public void guardarAlertasScd(List<SapAlertasScd> alertasScd);
}