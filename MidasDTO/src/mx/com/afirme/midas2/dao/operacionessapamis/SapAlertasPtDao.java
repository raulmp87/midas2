package mx.com.afirme.midas2.dao.operacionessapamis;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasPt;

@Local
public interface SapAlertasPtDao {
	public void guardarAlertasPt(List<SapAlertasPt> alertasPt);
}
