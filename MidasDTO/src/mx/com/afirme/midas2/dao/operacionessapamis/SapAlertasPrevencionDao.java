package mx.com.afirme.midas2.dao.operacionessapamis;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasPrevencion;

@Local
public interface SapAlertasPrevencionDao {
	public void guardarAlertasPrevencion(List<SapAlertasPrevencion> alertasPrevencion);
}
