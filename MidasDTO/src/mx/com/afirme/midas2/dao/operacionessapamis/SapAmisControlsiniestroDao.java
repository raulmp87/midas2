package mx.com.afirme.midas2.dao.operacionessapamis;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.sapamis.otros.SapAmisControlSiniestros;

@Local
public interface SapAmisControlsiniestroDao {
	
	public List<SapAmisControlSiniestros> findAllControlSiniestro();
	
	public void limpiarRegistrosControlEnvios(List<SapAmisControlSiniestros> listaSiniestrosEliminar);
	
}
