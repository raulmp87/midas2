package mx.com.afirme.midas2.dao.operacionessapamis;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasistemasEnvio;

@Local
public interface SapAlertasSistemasEnvioDao {

	public SapAlertasistemasEnvio guardarEncabezadoAlertas(SapAlertasistemasEnvio alertasGuardar );
	
	public SapAlertasistemasEnvio obtenerAlertas(String alertaBuscar);
	
}
