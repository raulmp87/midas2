package mx.com.afirme.midas2.dao.operacionessapamis;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasValuacion;


@Local
public interface SapAlertasValuacionDao {
	public void guardarAlertasValuacion(List<SapAlertasValuacion> alertasValuacion);
}
