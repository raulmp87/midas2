package mx.com.afirme.midas2.dao.operacionessapamis;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasCii;

@Local
public interface SapAlertasCiiDao {
	
	public void guardarAlertasCii(List<SapAlertasCii> alertasCii);
}
