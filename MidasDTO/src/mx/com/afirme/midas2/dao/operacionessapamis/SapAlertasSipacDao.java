package mx.com.afirme.midas2.dao.operacionessapamis;

import java.util.List;

import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasSipac;

import javax.ejb.Local;

@Local
public interface SapAlertasSipacDao {
	public void guardarAlertasSipac(List<SapAlertasSipac> alertasSipac);
}
