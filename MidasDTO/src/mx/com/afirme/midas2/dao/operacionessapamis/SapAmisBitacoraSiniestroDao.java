package mx.com.afirme.midas2.dao.operacionessapamis;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.sapamis.otros.SapAmisBitacoraSiniestros;

@Local
public interface SapAmisBitacoraSiniestroDao {
	public void guardarEnBitacoraSiniestro(SapAmisBitacoraSiniestros siniestro);
	
	public List<SapAmisBitacoraSiniestros> obtenerBitacoraSiniestrosFiltrada (String bitacoraPoliza ,String bitacoraVin ,String bitacoraFechaEnvio,
			String estatusEnvio,String cesvi,String cii,String emision,String ocra,String prevencion,String pt,String csd,
			String siniestro, String sipac , String valuacion);
}
