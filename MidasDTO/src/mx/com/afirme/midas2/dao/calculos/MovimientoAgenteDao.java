package mx.com.afirme.midas2.dao.calculos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.calculos.MovimientoAgente;
import mx.com.afirme.midas2.dto.calculos.DetalleCalculoComisionesView;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.util.MidasException;
@Local
public interface MovimientoAgenteDao extends EntidadDao{
	public List<MovimientoAgente> findByFilters(MovimientoAgente filtro);
	
	public List<DetalleCalculoComisionesView> obtenerMovimientosPorAgentes(Long idConfig,List<AgenteView> listadoAgentes) throws MidasException;
	
}
