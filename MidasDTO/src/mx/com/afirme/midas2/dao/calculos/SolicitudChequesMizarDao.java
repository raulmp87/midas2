package mx.com.afirme.midas2.dao.calculos;

import java.util.Date;

import javax.ejb.Local;

import mx.com.afirme.midas.interfaz.solicitudcheque.SolicitudChequeDTO;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dto.calculos.DetalleSolicitudCheque;
import mx.com.afirme.midas2.util.MidasException;

@Local
public interface SolicitudChequesMizarDao extends EntidadDao{
	
	public enum ClavesTransaccionesContables{
		COMISIONES("COMISIONES"),BONOS("BONOS"),PRESTAMOS("PRESTAMOS");
		public String value;
		private ClavesTransaccionesContables(String value){
			this.value=value;
		}
	};
	
	public Long iniciarCalculoComisiones(Long idConfiguracionComisiones,Long idCalculoComisiones,Long idCalculoTemporal,String queryAgentes) throws MidasException;
	
	public Long iniciarCalculoBonos(Long pIdCalculoIn,Long pIdProgramacion,Long pIdConfigBono,Long pIdModoEjec,Date pFechaAlta,Date pFechaEjecucion,String pHora,Long pPeriodoEjecucion,Long pTipoConfiguracion,String pUsuarioAlta,Long pTipoBono,Long pClaveEstatus,Integer pActivo,Integer pEsNegocio) throws MidasException;
	
	public Long iniciarCalculoProvisiones(Long pIdCalculoIn,Long pIdProgramacion,Long pIdConfigBono,Long pIdModoEjec,Date pFechaAlta,Date pFechaEjecucion,String pHora,Long pPeriodoEjecucion,Long pTipoConfiguracion,String pUsuarioAlta,Long pTipoBono,Long pClaveEstatus,Integer pActivo,Integer pEsNegocio) throws MidasException;
	
	/**
	 * Metodo para contabilizar movimientos y afectar cuentas contables de Mizar.
	 * @param identificador Es el identificador para obtener el cursor
	 * @param claveTransaccionContable dependiendo de la clave contable
	 * @throws MidasException
	 */
	public void contabilizarMovimientos(String identificador,ClavesTransaccionesContables claveTransaccionContable) throws MidasException;
		
	/**
	 * Agrega el encabezado de una solicitud de cheque en MIZAR a partir de una solicitud de cheque de SEYCOS
	 * @param solicitudCheque Solicitud de cheque de SEYCOS
	 */
	public void agregarEncabezadoSolicitudChequeMIZAR(SolicitudChequeDTO solicitudCheque) throws Exception ;
	
	/**
	 * Agrega en MIZAR el detalle de una solicitud de cheque de SEYCOS
	 * @param detalleSolicitudCheque Detalle de la solicitud de cheque de SEYCOS
	 * @param solicitudCheque Solicitud de cheque de SEYCOS
	 */
	public void agregarDetalleSolicitudChequeMIZAR(DetalleSolicitudCheque detalleSolicitudCheque, SolicitudChequeDTO solicitudCheque) throws Exception;
	
	/**
	 * Procesa en MIZAR las solicitudes de cheque recien importadas desde SEYCOS
	 * @param idSesion Es el identificador de la sesión con la que están relacionadas todas las solicitudes de cheque que serán procesadas
	 */
	public void procesarSolicitudesChequeEnMIZAR(Long idSesion) throws Exception ;
	
}
