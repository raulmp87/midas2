package mx.com.afirme.midas2.dao.calculos;

import java.util.List;
import javax.ejb.Local;

import mx.com.afirme.midas2.dao.calculos.CalculoComisionesDao.EstatusCalculoComisiones;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.calculos.CalculoComisiones;
import mx.com.afirme.midas2.domain.calculos.DetalleCalculoComisiones;
import mx.com.afirme.midas2.dto.calculos.DetalleCalculoComisionesView;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.util.MidasException;
/**
 * Interface para gestionar la informacion detallada del preview del calculo a nivel agente/promotoria
 * @author vmhersil
 *
 */
@Local
public interface DetalleCalculoComisionesDao extends EntidadDao{
	/**
	 * Metodo para guardar el detalle de un calculo para su posterior consulta
	 * @return
	 * @throws MidasException
	 */
	public Long saveDetalleCalculoComisiones(DetalleCalculoComisiones detalleCalculo) throws MidasException;
	/**
	 * Guarda el detalle de calculo 
	 * @param detalleCalculo
	 * @return
	 * @throws MidasException
	 */
	public Long saveListDetalleCalculoComisiones(List<DetalleCalculoComisiones> listaDetalleCalculo) throws MidasException;
	/**
	 * Inserta en la tabla de detalle de calculo de comisiones de una lista de agentes de acuerdo a la configuracion.
	 * @param idConfigComisiones
	 * @param agentes
	 * @param idCalculoTemporal
	 * @throws MidasException
	 */
	public void generarDetalleCalculoComisiones(Long idConfigComisiones,List<AgenteView> agentes,Long idCalculoTemporal,Long idCalculo) throws MidasException;
	/**
	 * Por medio de la configuracion y la lista de los agentes a considerar se hace una consulta de los movimientos de los agentes listados
	 * y se obtienen los registros de los movimientos por agente
	 * @param idConfig
	 * @param agentesAConsiderarCalculo
	 * @return
	 */
	public List<DetalleCalculoComisiones> obtenerDetalleCalculoPorConfiguracion(Long idConfig,List<AgenteView> agentesAConsiderarCalculo);
	/**
	 * Carga la informacion del detalle de un registro del calculo donde se mostrara el agente y/o promotoria
	 * a quien se les paga la comision
	 * @param idDetalleCalculo Llave primaria del detalle del calculo toDetalleCalculoComisiones.id
	 * @return
	 * @throws MidasException
	 */
	public DetalleCalculoComisiones loadById(Long idDetalleCalculo) throws MidasException;
	/**
	 * Carga la informacion del detalle de un registro del calculo donde se mostrara el agentes y/o promotoria
	 * a quien se les paga la comision
	 * @param detalleCalculo
	 * @return
	 * @throws MidasException
	 */
	public DetalleCalculoComisiones loadById(DetalleCalculoComisiones detalleCalculo) throws MidasException;
	/**
	 * Carga la informacion detallada de un calculo por medio del id.Se mostraran la lista de agentes y/o 
	 * promotorias involucradas en el preview de ese calculo a consultar.
	 * @param idCalculo
	 * @return
	 * @throws MidasException
	 */
	public List<DetalleCalculoComisiones> listarDetalleDeCalculo(Long idCalculo) throws MidasException;
	/**
	 * Carga la informacion detallada de un calculo por medio del id.Se mostraran la lista de agentes y/o 
	 * promotorias involucradas en el preview de ese calculo a consultar.
	 * @param calculo
	 * @return
	 * @throws MidasException
	 */
	public List<DetalleCalculoComisiones> listarDetalleDeCalculo(CalculoComisiones calculo) throws MidasException;
	
	/**
	 * Carga la informacion detallada de un calculo por medio del id.Se mostraran la lista de agentes y/o 
	 * promotorias involucradas en el preview de ese calculo a consultar siempre y cuando no tengan el estatus de AUTORIZADO.
	 * @param idCalculo
	 * @return Listado de detalles de calculo con estatus diferente a AUTORIZADO
	 */	
	public List<DetalleCalculoComisiones> listarDetalleDeCalculoNoAutorizado(Long idCalculo);
	
	/**
	 * Busca los detalles bajo ciertas condiciones
	 * @param filtro
	 * @return
	 */
	public List<DetalleCalculoComisiones> findByFilters(DetalleCalculoComisiones filtro);
	/**
	 * Elimina logicamente todos los registros de detalle de un calculo
	 * @param idCalculo
	 * @throws MidasException
	 */
	public void deleteDetallePorCalculo(Long idCalculo) throws MidasException;
	/**
	 * Elimina logicamente todos los registros de detalle de un calculo
	 * @param calculo
	 * @throws MidasException
	 */
	public void deleteDetallePorCalculo(CalculoComisiones calculo) throws MidasException;
	/**
	 * Se vuelve a rehabilitar todos los registros de detalle de un calculo eliminado
	 * @param idCalculo
	 * @throws MidasException
	 */
	public void rehabilitarDetallePorCalculo(Long idCalculo) throws MidasException;
	/**
	 * Se vuelve a rehabilitar todos los registros de detalle de un calculo eliminado
	 * @param calculo
	 * @throws MidasException
	 */
	public void rehabilitarDetallePorCalculo(CalculoComisiones calculo) throws MidasException;
	/**
	 * Obtiene el registro de detalle de calculo de comisiones por medio de la solicitud de cheque
	 * @param idSolicitudCheque
	 * @throws MidasException
	 * @return
	 */
	public DetalleCalculoComisiones obtenerDetallePorSolicitudCheque(Long idSolicitudCheque) throws MidasException;
	/**
	 * Actualiza el estatus de todos los registros del detalle de un calculo
	 * @param idCalculo
	 * @param estatus
	 * @throws MidasException
	 */
	public void actualizarDetallePorCalculo(Long idCalculo,EstatusCalculoComisiones estatus) throws MidasException;
	/**
	 * Actualiza el estatus de todos los registros del detalle de un calculo
	 * @param calculo
	 * @param estatus
	 * @throws MidasException
	 */
	public void actualizarDetallePorCalculo(CalculoComisiones calculo,EstatusCalculoComisiones estatus) throws MidasException;
	/**
	 * Obtiene el detalle de un calculo de comisiones para comparar por parte de midas.
	 * @param idCalculo
	 * @return
	 * @throws MidasException
	 */
	public List<DetalleCalculoComisiones> listarDetalleCalculoMidasDiferencia(Long idCalculo) throws MidasException;
	/**
	 * Obtiene el detalle de un calculo de comisiones para comparar por parte de mizar.
	 * @param idCalculo
	 * @return
	 * @throws MidasException
	 */
	public List<DetalleCalculoComisiones> listarDetalleCalculoMizarDiferencia(Long idCalculo) throws MidasException;
	/**
	 * Obtiene las diferencias de importes entre Midas y Mizar.
	 * @param idCalculo
	 * @return
	 * @throws MidasException
	 */
	public List<DetalleCalculoComisiones> listarDiferenciasMidasMizar(Long idCalculo) throws MidasException;
	
	public List<DetalleCalculoComisionesView> listarReporteComisionAgente(Long idCalculo) throws MidasException;
}
