package mx.com.afirme.midas2.dao.calculos;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.bonos.CalculoBono;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.bonos.DetalleCalculoBono;
import mx.com.afirme.midas2.domain.bonos.ProgramacionBono;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HonorariosAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.dto.bonos.DetalleBonoPolizaView;
import mx.com.afirme.midas2.dto.bonos.DetalleBonoRamoSubramoView;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.PreviewCalculoBonoView;
import mx.com.afirme.midas2.util.MidasException;

@Local
public interface CalculoBonosDao extends EntidadDao{
	
	public static enum EstatusCalculoBonos{
		PENDIENTE_AUTORIZAR("PENDIENTE POR AUTORIZAR"),AUTORIZADO("AUTORIZADO"),APLICADO("APLICADO"),PAGADO("PAGADO"),CANCELADO("CANCELADO"),ERROR_PROCESAR("ERROR AL PROCESAR")
		, PROCESO_AUTORIZACION("EN PROCESO DE AUTORIZACION"), PENDIENTE_RECALCULAR("PENDIENTE-RECALCULAR");
		private String value;
		private EstatusCalculoBonos(String value){
			this.value=value;
		}
		public String getValue(){
			return value;
		}
	}
	
	public List<CalculoBono>findByFiltersCalculo(CalculoBono calculoBono);
	
	public List<PreviewCalculoBonoView>findByFiltersCalculoView(CalculoBono calculoBono);
	
	public CalculoBono loadByIdCalculo(Long idCalculo);
	
	public List<DetalleCalculoBono>findByFiltersDetalleCalculo(DetalleCalculoBono calculoBono);
	
	public DetalleCalculoBono loadByIdDetalleCalculo(Long idCalculo);
	
	public List<BigDecimal> getCentrosOperacionByConfiguracion(Long idConfig)throws MidasException;
	
	public List<BigDecimal> getGerenciasByConfiguracion(Long idConfig)throws MidasException;
	
	public List<BigDecimal> getEjecutivosByConfiguracion(Long idConfig)throws MidasException;
	
	public List<BigDecimal> getPromotoriasByConfiguracion(Long idConfig)throws MidasException;	
	
	public List<BigDecimal> getPrioridadesByConfiguracion(Long idConfig)throws MidasException;
	
	public List<BigDecimal> getSituacionesByConfiguracion(Long idConfig)throws MidasException;
	
	public List<BigDecimal> getTiposAgentesByConfiguracion(Long idConfig)throws MidasException;
	
	public List<BigDecimal> getTiposPromotoriasByConfiguracion(Long idConfig)throws MidasException;
	
	public List<AgenteView> obtenerAgentesPorConfiguracion(ConfigBonos configuracion) throws Exception;
	
	public Long generarCalculo(Long idConfigBono,List<AgenteView> agentesDelCalculo) throws MidasException;
	
	public void validarAgentesEnCalculosPendientes(Long idCalculoTemporal)throws MidasException;
	
	public Long ejecutarCalculo(ProgramacionBono progBono, CalculoBono calculoBono, Long modoEjecucion, Integer isNegocio, String conceptoEjecucionAutomatica) throws MidasException;
	
	public Long saveCalculoBono(CalculoBono calculo);
	
	public Long saveListDetalleCalculoBonos(List<DetalleCalculoBono> lista) throws MidasException;
	
	public Long saveDetalleCalculoBonos(DetalleCalculoBono detalleCalculo)throws MidasException;

	/**
	 * Metodo que obtiene las programaciones de bonos activas automaticas por ejecutar.
	 * conceptoEjecucionAutomatica = parametro para identificar si se ejecuto para Provisiones o Bonos
	 * @return
	 * @throws MidasException
	 */
	public List<ProgramacionBono> obtenerConfiguracionesAutomaticasActivas(String conceptoEjecucionAutomatica) throws MidasException;
	/**
	 * Permite ejecutar bono automatico
	 * @param progBono
	 * @param calculoBono
	 * @param isNegocio
	 * @return
	 * @throws MidasException
	 */
	public void ejecutarCalculoAutomatico(ProgramacionBono progBono, CalculoBono calculoBono,String conceptoEjecucionAutomatica) throws MidasException;
	/**
	 * Ejecuta el preview de los calculos de bonos automaticos
	 * @param lista
	 * @return
	 * @throws MidasException
	 */
	public void ejecutarCalculosAutomaticos(List<ProgramacionBono> lista,String conceptoEjecucionAutomatica) throws MidasException;
	
	/**
	 * servicio para eliminar el calculo de bonos solo si aun no esta autorizado
	 * @param calculoBono
	 * @return
	 * @throws MidasException
	 */
	public CalculoBono eliminaCalculoBonos(CalculoBono calculoBono) throws MidasException;
	
	/**
	 * Ejecuta el preview de las polizas emitidas y pagadas para relizar el calculo de bonos y provisiones
	 * @param lista
	 * @return
	 * @throws MidasException
	 */
//	public void ejecutarDetalleMensualPolizasEmitidasYPagadasLog() throws MidasException;
	
	/**
	 * servicio para obtener el detalle del calculo de bono a nivel poliza
	 * @param calculoBono
	 * @return
	 * @throws
	 */
	public List<DetalleBonoPolizaView> obtenerDetalleBonoPoliza(CalculoBono calculoBono);
	
	public List<DetalleBonoPolizaView> obtenerDetalleBonoNivelCobertura(CalculoBono calculoBono);
	
	public List<DetalleBonoRamoSubramoView> obtenerDetalleBonoRamoSubramo(CalculoBono calculoBono);
	
	public void enviarAListadoEjecucionCalculoBonoManual(ProgramacionBono programacionBono, List<Negocio> negociosEspecialesList, List<ConfigBonos> configuracionBonoList,
			Usuario usuario, Boolean isManual) throws MidasException;
	
	public List<DetalleBonoPolizaView> getDetalleBonosNivelAmisExcel(CalculoBono calculoBono);
	
	public void generarSolicitudesDeChequePorCalculo(Long idCalculo);
	
	public List<DetalleCalculoBono> listarDetalleDeCalculoPorAutorizar(Long idCalculo);
	
	public List<HonorariosAgente> obtenerHonorariosFacturasPendientes (Long idAgente, Integer periodo);
	
	public ValorCatalogoAgentes obtenerClaveEstatusCalculo(EstatusCalculoBonos estatus) throws Exception;
	
	public Integer obtenerNumeroCalculosBonoPrevios (Long idCalculo);
	
}
