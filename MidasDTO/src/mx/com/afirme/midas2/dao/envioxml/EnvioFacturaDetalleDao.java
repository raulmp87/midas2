package mx.com.afirme.midas2.dao.envioxml;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.envioxml.EnvioFacturaDet;


@Local
public interface EnvioFacturaDetalleDao extends EntidadDao {
	
	EnvioFacturaDet guardarDetalleEnvio (EnvioFacturaDet envioDet);
	void guardarDetalleEnvio (List<EnvioFacturaDet> envioDetalles);
	
}
