package mx.com.afirme.midas2.dao.envioxml;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.envioxml.EnvioFactura;


@Local
public interface EnvioFacturaDao extends EntidadDao {
	
	EnvioFactura guardaEnvio(EnvioFactura envio);
	List<EnvioFactura> listarFilradoSolicitudGrid(EnvioFactura envioFactura, Date fechaInicial, Date fechaFinal);
}
