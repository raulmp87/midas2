package mx.com.afirme.midas2.dao.general;

import java.util.List;

import mx.com.afirme.midas2.dto.general.Email;

public interface EmailDao{
	public List<Email> findAll();
	public Email findById(Long id);
	public Email findByDesc(String desc);
	public List<Email> findByStatus(boolean status);
	public List<Email> findByProperty(String propertyName, Object property);
	public Email saveObject(Email object);
}