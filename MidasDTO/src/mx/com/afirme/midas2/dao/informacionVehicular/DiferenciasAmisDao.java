package mx.com.afirme.midas2.dao.informacionVehicular;

import java.sql.SQLException;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.poliza.diferenciasamis.DiferenciasAmis;

@Local
public interface DiferenciasAmisDao extends Dao<Long, DiferenciasAmis>{

	public List<DiferenciasAmis> findAllIncisos(Long idProrroga)  throws SQLException, Exception;
	
	public List<DiferenciasAmis> findAllPolizas(Long idProrroga)  throws SQLException, Exception;
	
}
