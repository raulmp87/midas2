package mx.com.afirme.midas2.dao.impuestosestatales;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.catalogos.retencionimpuestos.RetencionCedularesView;
import mx.com.afirme.midas2.dto.impuestosestatales.RetencionImpuestosDTO;
import mx.com.afirme.midas2.dto.reportesAgente.RetencionImpuestosReporteView;

@Local
public interface RetencionImpuestosDao { 
	
	public enum TipoAccionConfiguracion {
		ALTA(0L, "ALT"), ACTUALIZACION(1L, "ACTU"), ACTIVACION(2L, "ACTIV"), INACTIVACION(3L, "INACTIV"), VENCIMIENTO(4L, "VENC");
		private Long value;
		private String clave;
		
		private TipoAccionConfiguracion(Long value, String clave) {
			this.value = value;
			this.clave = clave;
		}
		
		public Long getValue() {
			return this.value;
		}
		public String getClave() {
			return this.clave;
		}
	}
	
	public enum TipoEjecucionCalculo {
		 JOB(0L, "JOB"), USUARIO(1L, "MAN");
		private Long value;
		private String clave;
		
		private TipoEjecucionCalculo(Long value, String clave) {
			this.value = value;
			this.clave = clave;
		}
		
		public Long getValue() {
			return this.value;
		}
		public String getClave() {
			return this.clave;
		}
	}
	
	public enum TipoEstatusEjecucionCalculo {
		POR_INICIAR("INIC"), EN_PROCESO("PROC"), ERROR("ERR"), TERMINADO("TERM");
		private String clave;
		
		private TipoEstatusEjecucionCalculo(String clave) {
			this.clave = clave;
		}
		
		public String getClave() {
			return this.clave;
		}
	}

	public Long setMonitorAvance(RetencionCedularesView monitor);
	
	public void ejecutaProcesoRetencion(RetencionImpuestosDTO configuracion, RetencionCedularesView monitor, TipoEjecucionCalculo modoEjecucion);

	public List<RetencionImpuestosDTO> findByFiltersView(RetencionImpuestosDTO filtro);
	
	public List<RetencionImpuestosReporteView>llenarDatosDetallePrimaRetencionImpuestos(RetencionImpuestosReporteView filtro);
	
	public List<RetencionImpuestosDTO> listarHistorico(RetencionImpuestosDTO filtro);
	
	public List<RetencionCedularesView> listarMonitorRetencionImpuestos();
	
}