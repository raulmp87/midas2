package mx.com.afirme.midas2.dao.sapamis.procesos;

import java.io.Serializable;
import java.util.Date;

public interface SapAmisEjecucionesSTPDao  extends Serializable{
	public void emisionPoblado(Date fechaInicial, Date fechaFinal);
	public void siniestrosPoblado(Date fechaInicial, Date fechaFinal);
	public void rechazosPoblado(Date fechaInicial, Date fechaFinal);
	public void prevencionesPoblado(Date fechaInicial, Date fechaFinal);
	public void pttPoblado(Date fechaInicial, Date fechaFinal);
	public void salvamentoPoblado(Date fechaInicial, Date fechaFinal);
	public void roboPobladoREPUVE(Date fechaInicial, Date fechaFinal);
	public void recuperacionPobladoREPUVE(Date fechaInicial, Date fechaFinal);
}