package mx.com.afirme.midas2.dao.sapamis.sistemas;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisSalvamento;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;

/*******************************************************************************
 * Nombre Interface: 	SapAmisSalvamentoDao.
 * 
 * Descripcion: 		Se utiliza para el manejo de Transacciones con la tabla
 * 						SAPAMISSALVAMENTOS propia para el proceso de emisiones.
 * 						
 * 						Se utiliza el objeto SapAmisSalvamento para el manejo
 * 						de las transacciones.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/

public interface SapAmisSalvamentoDao extends Serializable{
	public List<SapAmisSalvamento> obtenerPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina);
}