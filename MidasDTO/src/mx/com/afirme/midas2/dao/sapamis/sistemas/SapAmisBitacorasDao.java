package mx.com.afirme.midas2.dao.sapamis.sistemas;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisBitacoras;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;

/*******************************************************************************
 * Nombre Interface: 	SapAmisBitacorasDao.
 * 
 * Descripcion: 		Se utiliza para el manejo de Transacciones de las 
 * 						Bitacoras de procesos con la Base de Datos.
 * 						
 * 						Se utiliza el objeto SapAmisBitacoras para el manejo
 * 						de las transacciones.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/

public interface SapAmisBitacorasDao extends Serializable{
	public List<SapAmisBitacoras> obtenerCifras(ParametrosConsulta parametrosConsulta);
}