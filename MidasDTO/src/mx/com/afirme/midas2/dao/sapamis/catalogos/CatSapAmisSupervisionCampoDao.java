package mx.com.afirme.midas2.dao.sapamis.catalogos;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.dto.sapamis.catalogos.CatSapAmisSupervisionCampo;

public interface CatSapAmisSupervisionCampoDao extends Serializable {
	public List<CatSapAmisSupervisionCampo> findByFilters(CatSapAmisSupervisionCampo catSapAmisSupervisionCampo);
}
