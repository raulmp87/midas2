package mx.com.afirme.midas2.dao.sapamis.sistemas;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisValuacion;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;

/*******************************************************************************
 * Nombre Interface: 	SapAmisValuacionDao.
 * 
 * Descripcion: 		Se utiliza para el manejo de Transacciones con la tabla
 * 						SAPAMISVALUACION propia para el proceso de Valuacion.
 * 						
 * 						Se utiliza el objeto SapAmisValuacion para el manejo
 * 						de las transacciones.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/

public interface SapAmisValuacionDao extends Serializable{
	public List<SapAmisValuacion> obtenerPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina);
}