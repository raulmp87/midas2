package mx.com.afirme.midas2.dao.sapamis.sistemas;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisPTT;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;

/*******************************************************************************
 * Nombre Interface: 	SapAmisPTTDao.
 * 
 * Descripcion: 		Se utiliza para el manejo de Transacciones con la tabla
 * 						SAPAMISPTT propia para el proceso de Perdidas Totales.
 * 						
 * 						Se utiliza el objeto SapAmisPTT para el manejo
 * 						de las transacciones.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/

public interface SapAmisPTTDao extends Serializable{
	public List<SapAmisPTT> obtenerPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina);
}