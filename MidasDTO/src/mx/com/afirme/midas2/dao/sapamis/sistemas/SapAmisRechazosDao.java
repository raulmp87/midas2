package mx.com.afirme.midas2.dao.sapamis.sistemas;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisRechazos;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;

/*******************************************************************************
 * Nombre Interface: 	SapAmisRechazosDao.
 * 
 * Descripcion: 		Se utiliza para el manejo de Transacciones con la tabla
 * 						SAPAMISRECHAZOS propia para el proceso de Rechazos.
 * 						
 * 						Se utiliza el objeto SapAmisRechazos para el manejo
 * 						de las transacciones.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/

public interface SapAmisRechazosDao extends Serializable{
	public List<SapAmisRechazos> obtenerPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina);
}