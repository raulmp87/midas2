package mx.com.afirme.midas2.dao.sapamis.sistemas;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisPrevenciones;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;

/*******************************************************************************
 * Nombre Interface: 	SapAmisPrevencionesDao.
 * 
 * Descripcion: 		Se utiliza para el manejo de Transacciones con la tabla
 * 						SAPAMISEMISION propia para el proceso de emisiones.
 * 						
 * 						Se utiliza el objeto SapAmisPrevenciones para el manejo
 * 						de las transacciones.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/

public interface SapAmisPrevencionesDao extends Serializable{
	public List<SapAmisPrevenciones> obtenerPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina);
}