package mx.com.afirme.midas2.dao.poliza.renovacionmasiva;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.OrdenRenovacionMasiva;

@Local
public interface OrdenRenovacionMasivaDao extends EntidadDao {

	public List<OrdenRenovacionMasiva> listByFilter(OrdenRenovacionMasiva ordenRenovacionMasiva);
	
	public Long listByFilterCount(OrdenRenovacionMasiva ordenRenovacion);
}
