package mx.com.afirme.midas2.dao;

import java.util.List;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

public interface Dao<K, E> {
    public void persist(E entity);
    public E update(E entity);
    public void remove(E entity);
    public E findById(K id);
    public List<E> findAll();
    public E getReference(K id);
    public List<E> findByFilters(E filters);
    public List<E> findByProperty(String propertyName, Object value);
    
    /**
	 * Sirve para buscar un listado de una Entidad a través de un objeto filtro.
	 * En caso de que el objeto filtro no pueda mapearse directamente sobre la entidad
	 * se hace uso de <code>FilterPersistanceAnnotation</code> sobre los métodos del filtro
	 * para definir en que forma se mapearán sobre la entidad.
	 * @param <E> entityClass
	 * @param <K> filter
	 * @return
	 */
	@SuppressWarnings("hiding")
	public <E extends Entidad, T> List<E> findByFilterObject(Class<E> entityClass, T filter);
	
	 /**
	 * Sirve para buscar un listado ordenado de una Entidad a través de un objeto filtro.
	 * En caso de que el objeto filtro no pueda mapearse directamente sobre la entidad
	 * se hace uso de <code>FilterPersistanceAnnotation</code> sobre los métodos del filtro
	 * para definir en que forma se mapearán sobre la entidad.
	 * @param <E> entityClass
	 * @param <K> filter
	 * @return
	 */
	@SuppressWarnings("hiding")
	public <E extends Entidad, T> List<E> findByFilterObject(
			Class<E> entityClass, T filter, String orderBy);
}

