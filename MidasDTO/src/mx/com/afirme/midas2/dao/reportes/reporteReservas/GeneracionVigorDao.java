package mx.com.afirme.midas2.dao.reportes.reporteReservas;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.reportes.reporteReservas.GeneracionVigorDTO;

@Local
public interface GeneracionVigorDao extends Dao<Long, GeneracionVigorDTO> {
	
	public GeneracionVigorDTO getLast() throws SQLException;
	
	public List<GeneracionVigorDTO> getListTareas(BigDecimal tipoArchivo) throws SQLException;

}
