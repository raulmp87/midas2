package mx.com.afirme.midas2.dao.reportes.vitro;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.dto.reportes.vitro.ReportesVitroDTO;

import com.afirme.nomina.model.*;

/**
 * Clase que contiene la l\u00f3gica para realizar las consultas 
 * hacia los reportes que se tendr\u00e1n que generar para el negocio de vitro
 * @author AFIRME
 * 
 * @since 20160518
 * 
 * @version 1.0
 */
@Local
public interface ReportesVitroDAO{

	/**
	 * M\u00e9todo que realiza la consulta de los grupos de vitro 
	 * que se encuentran dentro de las tablas de DPN.
	 * 
	 * @return Lista que contiene los grupos existentes.
	 */
	public List<Dpngrupo> consultarGruposVitro();
	
	/**
	 * M\u00e9todo que realiza la consulta del calendario de pagos
	 * de las p\u00f3lizas que se encuentran para ese grupo.
	 *  
	 * @param idGrupo Identificador del grupo al que pertenecen las p\u00f3lizas
	 * 
	 * @return lista con las fechas con las que se va a generar el reporte.
	 */
	public List<Date> consultarCalendarioGrupo(Long idGrupo);
	
	/**
	 * M\u00e9todo que realiza la consulta del id del cliente de dpn apartir de su nombre.
	 * 
	 * @param nombreClienteDPN Nombre del cliente de DPN
	 * 
	 * @param idNegocio Identificador del negocio que al que pertenece el cliente.
	 * 
	 * @return Identificador del cliente;
	 */
	public List<Dpncliente> consultarCliente(String nombreClienteDPN, String idNegocio);
	
	/**
	 * M\u00e9todo que realiza la consulta de las p\u00f3lizas de vitro
	 * que son quincenales.
	 * 
	 * @param fechaCorte fecha en la que se genera el reporte.
	 * 
	 * @param tipoNomina Tipo de nomina de la que se va a generar el reporte.
	 * 
	 * @param idNegocio Tipo denegocio del cual se van a consultar las p\u00f3lizas.
	 * 
	 * @param idFormaPago Tipo de forma de pago de las polizas.
	 * 
	 * @param numeroDivide N\u00famero de dias que componen la quincenal o la semanal
	 * 
	 * @param nombreGrupo Nombre del grupo al que pertenecen las p\u00f3lizas
	 * 
	 * @return lista de p\u00f3lizas que se van a pagar.
	 */
	public List<ReportesVitroDTO> consultarPolizasVitro(Date fechaCorte, String tipoNomina, Long idNegocio, Long idFormaPago, Long numeroDivide, String nombreGrupo);
	
	/**
	 * M\u00e9todo que consulta los negocios pertenecientes a un cliente y que pueda ser vendido por el agente. 
	 * 
	 * @param rfcClientes Lista de rfc de los clientes disponibles dentro de DPN.
	 * 
	 * @return Lista de negocios que pueden ser vendidos por el agente.
	 */
	public List<Negocio> consultarNegocios(Long idAgente);
}