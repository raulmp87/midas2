package mx.com.afirme.midas2.dao.reportes.historial.cobranza;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.reportesAgente.ReporteHistorialCobranzaDTO;


/**
 * 
 * @author jochoa
 *
 */

@Local
public interface ReporteHistorialCobranzaDAO {
	
	public static final String CONSULTA_HISTORIAL_COBRANZA_POR_AGENTE =  "SEYCOS.PKGSIS_GRAL_REPORTES.REPORTE_HISTORIAL_COBRANZA";
	
	public static final String [] ATRIBUTOS_CLASE_HISTORIAL_REPORTE = {
		"id_cotizacion","poliza","nom_cond_cobro","tipo_cuenta","tipo_cobro","num_cuenta","nom_t_tarjeta","nom_solicitante","id_agente",
		"nom_agente","f_contab","sit_poliza","f_ini_vigencia","f_fin_vigencia","id_lin_negocio","id_inciso","desc_vehic","cve_amis",
		"id_modelo_auto","num_serie","placa","num_motor","imp_prima_neta","imp_derechos","imp_rcgos_pagofr","imp_iva","imp_prima_total",
		"nom_paquete","nom_titular","num_folio_rbo","num_exhibicion","f_cubre_desde","f_cubre_hasta","sit_recibo","imp_prima_total_rec",
		"id_usuario_creac","nom_usuario"};
	
	public static final String [] CAMPOS_CURSOR_HISTORIAL = {"ID_COTIZACION", "POLIZA", "NOM_COND_COBRO", "TIPO_CUENTA", "TIPO_COBRO",
		"NUM_CUENTA", "NOM_T_TARJETA", "NOM_SOLICITANTE", "ID_AGENTE", "NOM_AGENTE", "F_CONTAB", "SIT_POLIZA","F_INI_VIGENCIA",
		"F_FIN_VIGENCIA", "ID_LIN_NEGOCIO", "ID_INCISO", "DESC_VEHIC", "CVE_AMIS", "ID_MODELO_AUTO", "NUM_SERIE", "PLACA", "NUM_MOTOR",
		"IMP_PRIMA_NETA", "IMP_DERECHOS", "IMP_RCGOS_PAGOFR", "IMP_IVA", "IMP_PRIMA_TOTAL", "NOM_PAQUETE", "NOM_TITULAR", "NUM_FOLIO_RBO",
		"NUM_EXHIBICION", "F_CUBRE_DESDE", "F_CUBRE_HASTA", "SIT_RECIBO", "IMP_PRIMA_TOTAL_REC", "ID_USUARIO_CREAC", "NOM_USUARIO"};
	
	/**
	 * 
	 * @param seycos_IdAgente
	 * @return
	 */
	public List<ReporteHistorialCobranzaDTO> consultarHistPorAgente(Long seycos_IdAgente);
}
