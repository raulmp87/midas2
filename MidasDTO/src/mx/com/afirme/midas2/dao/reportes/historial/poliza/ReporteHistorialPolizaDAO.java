package mx.com.afirme.midas2.dao.reportes.historial.poliza;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.reportesAgente.ReporteHistorialPolizaDTO;

/**
 * Clase que contiene la l\u00f3gica para generar las consultas
 * para generar el reporte de Historial de pólzias agente
 * 
 * @author AFIRME
 * 
 * @since 28072016
 * 
 * @version 1.0
 *
 */
@Local
public interface ReporteHistorialPolizaDAO {
	
	/**
	 * M\u00fe9todo que realiza la llamada al stored del cual genera la consulta correspondiente para obtener el historico
	 * de las polizas generadas por un agente en particular.
	 * 
	 * @param seycosIdAgente identificador del agente dentro de la tabla agente del esquema de seycos.
	 * 
	 * @return Retorna la lista de objetos que contienen la informaci\u00f3n correspondiente del reporte.
	 */
	public List<ReporteHistorialPolizaDTO> consultarHistorialPolizaPorAgente(Long seycosIdAgente);
	
	/**
	 * M\u00e9todo que realiza la consulta del historico de una p\u00feliza en particular.
	 * 
	 * @param numeroPoliza N\u00famero de p\u00f3liza del cual se realiza la consulta.
	 * 
	 * @return Retorna la lista de objetos que contienen la informaci\u00f3n correspondiente del reporte.
	 */
	public List<ReporteHistorialPolizaDTO> consultarHistorialPorNumeroPoliza(Long numeroPoliza);
}
