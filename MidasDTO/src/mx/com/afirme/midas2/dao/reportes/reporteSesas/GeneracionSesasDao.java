package mx.com.afirme.midas2.dao.reportes.reporteSesas;

import java.sql.SQLException;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.reportes.reporteSesas.GeneracionSesasDTO;

@Local
public interface GeneracionSesasDao extends Dao<Long, GeneracionSesasDTO> {
	
	public GeneracionSesasDTO getLast() throws SQLException, Exception;
	
	public List<GeneracionSesasDTO> getListTareas(String claveNeg) throws SQLException, Exception;

}
