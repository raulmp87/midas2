package mx.com.afirme.midas2.dao.folioReexpedible;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.folioReexpedible.FiltroFolioReexpedible;
import mx.com.afirme.midas2.domain.folioReexpedible.ToFolioReexpedible;


/**
 * Clase que contiene la l\u00f3gica de persistencia para 
 * los folios reexpedibles
 * 
 * @since 10022016
 * 
 * @author AFIRME
 *
 */
@Local
public interface ToFolioReexpedibleDAO extends EntidadDao{
	
	public static final String[] ATRIBUTOS_FOLIO_REEXPEDIBLE = {"idFolioReexpedible", "idTipoMoneda","negprod","negocio","negTpoliza", "vigencia", 
		"agenteFacultativo", "fechaValidesHasta", "comentarios", "numeroFolio", 
		"tipoFolio","fechaCreacion", "activo","agenteVinculado", "folioFin"};

	public static final String MODEL=" model.";
	public static final String AND = " AND ";
	public static final String IGUALPARAMETRO = " = ";
	public static final String BETWEEN = " BETWEEN ";
	public static final String DOSPUNTOS = ":";
	public static final String ID_NEGOCIO = " NEG.IDTONEGOCIO ";
	public static final String ID_NEG_PROD = " NEGPROD.IDTONEGPRODUCTO ";
	public static final String ID_NEG_T_POLIZA = " TIPOPOL.IDTONEGTIPOPOLIZA ";
	public static final String ID_TIPO_FOLIO = " TFOLIO.ID_TIPO_FOLIO ";
	public static final String ID_VIGENCIA = " VIG.IDTCVIGENCIA ";
	public static final String NUMERO_FOLIO = " FOLIO.NUMERO_FOLIO ";
	public static final String FECHA_VALIDEZ = " FOLIO.FECHA_VALIDEZ_HASTA ";
	public static final String ID_MONEDA = " MONEDA.IDTCMONEDA ";
	public static final String INTERROGACION= "?";

	public static final String SQL_QUERY ="SELECT TFOLIO.CLAVE_TIPO_FOLIO, NEG.DESCRIPCIONNEGOCIO, PROD.DESCRIPCIONPRODUCTO, "
			+ " TPOLIZA.DESCRIPCIONTIPOPOLIZA, VIG.DESCRIPCION, FOLIO.NUMERO_FOLIO, FOLIO.FECHA_VALIDEZ_HASTA, MONEDA.DESCRIPCION, PERS.NOMBRE, NVL(FOLIO.COMENTARIOS, 'SIN COMENTARIOS') "
			+ " FROM MIDAS.TOFOLIOREEXPEDIBLE FOLIO "
			+ " INNER JOIN MIDAS.TONEGOCIO NEG "
			+ " ON FOLIO.ID_NEGOCIO = NEG.IDTONEGOCIO "
			+ " INNER JOIN MIDAS.TONEGPRODUCTO NEGPROD "
			+ " ON  FOLIO.ID_NEGOCIOPRODUCTO = NEGPROD.IDTONEGPRODUCTO "
			+ " INNER JOIN MIDAS.TOPRODUCTO PROD "
			+ " ON PROD.IDTOPRODUCTO = NEGPROD.IDTOPRODUCTO "
			+ " INNER JOIN MIDAS.TONEGTIPOPOLIZA TIPOPOL "
			+ " ON FOLIO.ID_NEG_TIPO_POLIZA = TIPOPOL.IDTONEGTIPOPOLIZA "
			+ " INNER JOIN MIDAS.TOTIPOPOLIZA TPOLIZA "
			+ " ON TIPOPOL.IDTOTIPOPOLIZA = TPOLIZA.IDTOTIPOPOLIZA "
			+ " INNER JOIN MIDAS.TCVIGENCIA VIG "
			+ " ON FOLIO.ID_VIGENCIA = VIG.IDTCVIGENCIA "
			+ " INNER JOIN MIDAS.TCTIPOFOLIO TFOLIO "
			+ " ON FOLIO.ID_TIPO_FOLIO = TFOLIO.ID_TIPO_FOLIO "
			+ " INNER JOIN MIDAS.VNMONEDA MONEDA "
			+ " ON FOLIO.ID_TIPO_MONEDA = MONEDA.IDTCMONEDA "
			+ " INNER JOIN MIDAS.TOAGENTE M2_AGT "
		    + " ON M2_AGT.ID = FOLIO.ID_AGENTEVINCULADO "
		    + " LEFT JOIN SEYCOS.AGENTE AGT " 
		    + " ON AGT.ID_AGENTE = M2_AGT.IDAGENTE "
		    + " AND B_ULT_MOD = 'V' "
			+ " LEFT JOIN SEYCOS.PERSONA PERS " 
			+ " ON PERS.ID_PERSONA = AGT.ID_PERSONA " 
			+ " WHERE ";

	public static final String SQL_COUNT_REGISTROS = "SELECT  COUNT(FOLIO.NUMERO_FOLIO) "
			+ " FROM MIDAS.TOFOLIOREEXPEDIBLE FOLIO "
			+ " INNER JOIN MIDAS.TONEGOCIO NEG "
			+ " ON FOLIO.ID_NEGOCIO = NEG.IDTONEGOCIO "
			+ " INNER JOIN MIDAS.TONEGPRODUCTO NEGPROD "
			+ " ON  FOLIO.ID_NEGOCIOPRODUCTO = NEGPROD.IDTONEGPRODUCTO "
			+ " INNER JOIN MIDAS.TOPRODUCTO PROD "
			+ " ON PROD.IDTOPRODUCTO = NEGPROD.IDTOPRODUCTO "
			+ " INNER JOIN MIDAS.TONEGTIPOPOLIZA TIPOPOL "
			+ " ON FOLIO.ID_NEG_TIPO_POLIZA = TIPOPOL.IDTONEGTIPOPOLIZA "
			+ " INNER JOIN MIDAS.TOTIPOPOLIZA TPOLIZA "
			+ " ON TIPOPOL.IDTOTIPOPOLIZA = TPOLIZA.IDTOTIPOPOLIZA "
			+ " INNER JOIN MIDAS.TCVIGENCIA VIG "
			+ " ON FOLIO.ID_VIGENCIA = VIG.IDTCVIGENCIA "
			+ " INNER JOIN MIDAS.TCTIPOFOLIO TFOLIO "
			+ " ON FOLIO.ID_TIPO_FOLIO = TFOLIO.ID_TIPO_FOLIO "
			+ " INNER JOIN MIDAS.VNMONEDA MONEDA "
			+ " ON FOLIO.ID_TIPO_MONEDA = MONEDA.IDTCMONEDA "
			+ " INNER JOIN MIDAS.TOAGENTE M2_AGT "
		    + " ON M2_AGT.ID = FOLIO.ID_AGENTEVINCULADO "
		    + " LEFT JOIN SEYCOS.AGENTE AGT " 
		    + " ON AGT.ID_AGENTE = M2_AGT.IDAGENTE "
		    + " AND B_ULT_MOD = 'V' "
			+ " LEFT JOIN SEYCOS.PERSONA PERS " 
			+ " ON PERS.ID_PERSONA = AGT.ID_PERSONA "
			+ " WHERE ";

	public static final String SQL_ORDER = " ORDER BY FOLIO.FECHA_CREACION DESC, FOLIO.NUMERO_FOLIO DESC";

	
	/**
	 * M\u00e9todo que consulta los tipos de folios existentes dentro de base.
	 * 
	 * @return Lista que contiene todos los tipos de folios existentes dentro de base.
	 */
	public List<ToFolioReexpedible> findAll();
	
	/**
	 * M\u00e9todo que realiza la b\u00fasqueda de los elementos que 
	 * cumplan con una propiedad. 
	 * 
	 * @param propertyName Nombre de la propiedad a buscar
	 * 
	 * @param value Valor de dicha propiedad.
	 * 
	 * @return Lista de elementos que cumple con el filtro indicado.
	 */
	public List<ToFolioReexpedible> findByProperty(String propertyName, Object value);
	
	/**
	 * M\u00e9todo que permite persistir el tipo de folio nuevo 
	 * dentro de la base de datos.
	 * 
	 * @param entity entidad que contiene la informaci\u00f3n a persistir.
	 * 				
	 */
	public void persist(ToFolioReexpedible entity);
	
	/**
	 * M\u00f9todo que realiza el update para actualizar el tipo 
	 * de folio.
	 * 
	 * @param entity entidad a ser actualizada.
	 * 
	 * @return endidad actualizada.
	 */
	public ToFolioReexpedible update(ToFolioReexpedible entity);
	
	/**
	 * M\u00e9todo que actualiza multiples elementos del catalogo.
	 * 
	 * @param entities Lista de entidades a actualizar.
	 * 
	 * @return Lista de entidades actualizadas.
	 */
	public List<ToFolioReexpedible> updateAll(List<ToFolioReexpedible> entities);
	
	/**
	 * M\u00e9todo que persiste multiples elementos del catalogo 
	 * al mismo tiempo.
	 * 
	 * @param entities Entidades a persistir.
	 * 
	 * @return Lista de entidades persistidas.
	 */
	public List<ToFolioReexpedible> saveAll(List<ToFolioReexpedible> entities);
	
	/**
	 * Metodo que hace la baja l\u00f3gica de un cat\u00e1logo
	 * en especifico.
	 * 
	 * @param entity Entidad a eliminar
	 */
	public void eliminarToFolioReexpedible(ToFolioReexpedible entity);
	
	/**
	 * M\u00e9todo que realiza la baja l\u00f3gica de varios elementos al mismo tiempo.
	 * 
	 * @param entities entidades a dar de baja l\u00f3gica.

	 */
	public void eliminarVarios(List<ToFolioReexpedible> entities);
	
	/**
	 * M\u00e9todo que obtiene una entidad ToFolioReexpedible 
	 * a partir de la base de datos.
	 * 
	 * @param id Identificador del objeto dentro de base de datos.
	 * 
	 * @return Objeto que corresponde con dicho id.
	 */	
	public ToFolioReexpedible findById(Long id);
	
	/**
	 * M\u00e9todo que realiza una consulta de folios 
	 * dependiendo de diferentes propiedades que sirven de filtros.
	 * 
	 * @param folioFiltro objeto que contiene los filtros necesarios para realizar
	 * el filtro dentro de la base de datos.
	 * 
	 * @return Lista de elementos que cumplieron con los filtros aplicados.
	 */
	public List<ToFolioReexpedible> findByProperties(FiltroFolioReexpedible folioFiltro);
	
	/**
	 * M\ueee9todo que realiza la consulta de folios de manera optimizada.
	 * 
	 * @param filtro To que contiene el filtro de los folios.
	 * 
	 * @return Lista de los folios.
	 */
	public List<FiltroFolioReexpedible> consultarFolioReexpediblesSQL(FiltroFolioReexpedible filtro);
	
	/**
	 * M\u00e9todo que realiza la consulta del n\u00famero de registros que va a devolber la consulta.
	 * 
	 * @param filtro Objeto que contiene los filtros de la consulta.
	 * 
	 * @return Cantidad de registros que devolver\u00e1 la consulta.
	 */
	public Long contarRegistrosFolios(FiltroFolioReexpedible filtro);
}
