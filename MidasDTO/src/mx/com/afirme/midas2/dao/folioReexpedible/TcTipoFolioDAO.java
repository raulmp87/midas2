package mx.com.afirme.midas2.dao.folioReexpedible;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.folioReexpedible.TcTipoFolio;


/**
 * Clase que contiene la l\u00f3gica de persistencia y de llamada a 
 * datos para el cat\u00e1logo de TCTIPOFOLIO.
 * 
 * @since 09022016
 * 
 * @author AFIRME
 *
 */
@Local
public interface TcTipoFolioDAO extends EntidadDao {
	
	/**
	 * M\u00e9todo que consulta los tipos de folios existentes dentro de base.
	 * 
	 * @return Lista que contiene todos los tipos de folios existentes dentro de base.
	 */
	public List<TcTipoFolio> findAll();
	
	/**
	 * M\u00e9todo que realiza la b\u00fasqueda de los elementos que 
	 * cumplan con una propiedad. 
	 * 
	 * @param propertyName Nombre de la propiedad a buscar
	 * 
	 * @param value Valor de dicha propiedad.
	 * 
	 * @return Lista de elementos que cumple con el filtro indicado.
	 */
	public List<TcTipoFolio> findByProperty(String propertyName, Object value);
	
	/**
	 * M\u00e9todo que permite persistir el tipo de folio nuevo 
	 * dentro de la base de datos.
	 * 
	 * @param entity entidad que contiene la informaci\u00f3n a persistir.
	 * 				
	 */
	public void persist(TcTipoFolio entity);
	
	/**
	 * M\u00f9todo que realiza el update para actualizar el tipo 
	 * de folio.
	 * 
	 * @param entity entidad a ser actualizada.
	 * 
	 * @return endidad actualizada.
	 */
	public TcTipoFolio update(TcTipoFolio entity);
	
	/**
	 * M\u00e9todo que actualiza multiples elementos del catalogo.
	 * 
	 * @param entities Lista de entidades a actualizar.
	 * 
	 * @return Lista de entidades actualizadas.
	 */
	public List<TcTipoFolio> updateAll(List<TcTipoFolio> entities);
	
	/**
	 * M\u00e9todo que persiste multiples elementos del catalogo 
	 * al mismo tiempo.
	 * 
	 * @param entities Entidades a persistir.
	 * 
	 * @return Lista de entidades persistidas.
	 */
	public List<TcTipoFolio> saveAll(List<TcTipoFolio> entities);
	
	/**
	 * Metodo que hace la baja l\u00f3gica de un cat\u00e1logo
	 * en especifico.
	 * 
	 * @param entity Entidad a eliminar
	 * 
	 */
	public void eliminarTcTipoFolio(TcTipoFolio entity);
	
	/**
	 * M\u00e9todo que realiza la baja l\u00f3gica de varios elementos al mismo tiempo.
	 * 
	 * @param entities entidades a dar de baja l\u00f3gica.
	 */
	public void eliminarVarios(List<TcTipoFolio> entities);
	
	/**
	 * M\u00e9todo que obtiene una entidad TcTipoFolio 
	 * a partir de la base de datos.
	 * 
	 * @param id Identificador del objeto dentro de base de datos.
	 * 
	 * @return Objeto que corresponde con dicho id.
	 */
	public TcTipoFolio findById(Long id);
}
