package mx.com.afirme.midas2.dao.endoso.cotizacion.auto;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.MovimientoEndoso;

@Local
public interface EndosoWSMidasAutosDao {

	public MovimientoEndoso findByControlEndosoCotId(Long controlEndosoCotId);
	
	public List<MovimientoEndoso>  findByProperty(String propertyName, Object propertyValue);
	
	public MovimientoEndoso actualizaObservaciones(Long controlEndosoCotId,String observaciones) throws Exception;
	
}