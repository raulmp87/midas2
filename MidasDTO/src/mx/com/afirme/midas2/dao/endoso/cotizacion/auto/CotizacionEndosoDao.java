package mx.com.afirme.midas2.dao.endoso.cotizacion.auto;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.dto.endoso.cotizacion.auto.CotizacionEndosoDTO;

@Local
public interface CotizacionEndosoDao {
	
	/**
	 * Listado de Cotizaciones de Endoso
	 * Desde la Bandeja de Cotizaciones: Autos/Cotizaciones/Endosos
	 */
	public List<Object[]> buscarCotizacionFiltrado(CotizacionEndosoDTO cotizacionEndosoDTO);
	
	/**
	 * Total de Cotizaciones de Endoso
	 * Desde la Bandeja de Cotizaciones: Autos/Cotizaciones/Endosos
	 */
	public Long obtenerTotalPaginacionCotizaciones(CotizacionEndosoDTO cotizacionEndosoDTO);
	
	/**
	 * Listado de Cotizaciones de Endoso de una poliza
	 * Desde Listado de Cotizaciones de Poliza: Autos/Emision/Consulta Por Poliza
	 * @param cotizacionEndosoDTO objeto con filtro Poliza y Cotizacion
	 */
	public List<ControlEndosoCot> buscarCotizacionesEndosoPoliza(CotizacionEndosoDTO cotizacionEndosoDTO);
	
	/**
	 * Total de Cotizaciones de Endoso de una poliza
	 * Desde Listado de Cotizaciones de Poliza: Autos/Emision/Consulta Por Poliza
	 * @param cotizacionEndosoDTO objeto con filtro Poliza y Cotizacion
	 */
	public Long obtenerTotalPaginacionCotizacionesEndosoPoliza(CotizacionEndosoDTO cotizacionEndosoDTO);
	
	public List<Object[]> getEndososParaCancelacion(Long cotizacionId);
	
	public List<Object[]> getEndososParaRehabilitacion(Long cotizacionId);
	
	public ControlEndosoCot getControlEndosoCE(Long controlEndosoCanceladoId);
	
	public List<Object[]> getSumarizadoMovimientosCoberturaEndososAFuturo(ControlEndosoCot controlEndosoCot, String strIncisosBaja, boolean groupInciso);
	
	public List<ControlEndosoCot> getControlEndosoCotEndososEAPAnteriores(ControlEndosoCot controlEndosoCot);
	
	public List<Object[]> getEndososParaAjustar(Long cotizacionId);
	
	/**
	 * Método que regresa el listado de Endosos que han afectado un inciso en particular
	 */
	public List<ControlEndosoCot> obtenerEndosoInciso(Long incisoContinuityId, Long cotizacionId, Date fechaBusquedaIncial,Date fechaBusquedaFinal, List<String> usuarios);
	
}
