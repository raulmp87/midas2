package mx.com.afirme.midas2.dao.endoso.cotizacion.auto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.endoso.EndosoDTO;

@Local
public interface EndosoDao {
	public List<EndosoDTO> getEndososParaCalculoService(BigDecimal idToCotizacion,Date recordFrom);
	public Boolean getExistenCambiosEnCoberturas(Long cotizacionContinuityId);
	public Boolean getExistenCambiosEnInciso(Long cotizacionContinuityId);
	public Boolean getExistenCambiosEnDatosRiesgo(Long cotizacionContinuityId);
	public Boolean validarRecibosDesagrupados(BigDecimal idToPoliza, Integer numeroInciso);
	public BigDecimal validarPrimasPendientesPagoInciso(BigDecimal idToPoliza, Integer numeroInciso, Date fechaValidacion);
}
