package mx.com.afirme.midas2.dao.excepcion;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.excepcion.CondicionExcepcion;
import mx.com.afirme.midas2.domain.excepcion.ExcepcionSuscripcion;
import mx.com.afirme.midas2.domain.excepcion.ValorExcepcion;

@Local
public interface ExcepcionSuscripcionDao extends Dao<Long, ExcepcionSuscripcion> {
		
	public List<ExcepcionSuscripcion> listarExcepciones(String cveNegocio, Short nivelEvaluacion);
	
	public List<ExcepcionSuscripcion> listarExcepciones(ExcepcionSuscripcion filtro);
	
	public List<CondicionExcepcion> obtenerCondiciones(Long idExcepcion);
	
	public CondicionExcepcion obtenerCondicion(Long idExcepcion, Integer tipoCondicion); 
	
	public List<ValorExcepcion> obtenerValoresDeCondicion(Long idToCondicion); 
}
