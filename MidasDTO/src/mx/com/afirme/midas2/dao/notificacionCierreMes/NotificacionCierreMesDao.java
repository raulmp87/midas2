package mx.com.afirme.midas2.dao.notificacionCierreMes;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.movil.cliente.CorreosCosultaMovil;

@Local
public interface NotificacionCierreMesDao {
	/**
	 * Obtiene la lista de catalogos segun los filtros que se proporcionen
	 * 
	 * @param filtro
	 * @return
	 * @throws Exception
	 */

	public List<CorreosCosultaMovil> getCorreosLista();

}
