package mx.com.afirme.midas2.dao.zonaCirculacion;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioEstado;

import java.util.List;

@Local
public interface NegocioEstadoDao extends EntidadDao {
	/**
	 * Obtiene un listado de los estados relacionados con un negocio
	 * 
	 * @param idToNegocio
	 * @return List<NegocioEstado>
	 * @author martin
	 */
	public List<NegocioEstado> obtenerEstadosPorNegocioId(Long idToNegocio);
}
