package mx.com.afirme.midas2.dao.zonaCirculacion;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioDTO;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioMunicipio;
@Local
public interface NegocioMunicipioDao extends EntidadDao {
	/**
	 * Obtiene los municipios de un estado (El id no es el id del NegocioEstado)
	 * 
	 * @param idToEstado
	 * @return
	 * @autor martin
	 */
	public List<NegocioMunicipio> obtenerMunicipiosPorEstadoId(
			Long idToNegocio, String idToEstado);

	public NegocioMunicipio findByNegocioAndEstadoAndMunicipio(Long idToNegocio, String stateId, String municipalityId);
	public NegocioMunicipio findByNegocioAndEstadoAndMunicipio(Negocio negocio, EstadoDTO estado, MunicipioDTO municipio);
}
