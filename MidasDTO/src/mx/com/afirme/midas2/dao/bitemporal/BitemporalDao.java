package mx.com.afirme.midas2.dao.bitemporal;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import org.joda.time.DateTime;

import com.anasoft.os.daofusion.bitemporal.Bitemporal;
import com.anasoft.os.daofusion.bitemporal.Continuity;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;

@Local
public interface BitemporalDao {

	public <E extends EntidadBitemporal> Object persistAndReturnId(E entity);

	public <E extends EntidadBitemporal> void persist(E entity);

	public <E extends EntidadBitemporal> E update(E entity);

	public <E extends EntidadBitemporal> void remove(E entity);

	public <E extends EntidadBitemporal, K> E findById(Class<E> entityClass,
			K id);

	public <E extends EntidadBitemporal> List<E> findAll(Class<E> entityClass);

	public <E extends EntidadBitemporal, K> E getReference(
			Class<E> entityClass, K id);

	public Object executeNativeQuerySimpleResult(String query);

	public <E extends EntidadBitemporal> List<E> findByProperty(
			Class<E> entityClass, String propertyName, final Object value,
			DateTime validOn);

	public <E extends EntidadBitemporal> List<E> findByProperties(
			Class<E> entityClass, Map<String, Object> params, DateTime validOn);

	public <E extends EntidadBitemporal> List<E> findByPropertiesWithOrder(
			Class<E> entityClass, Map<String, Object> params, DateTime validOn,
			String... orderByAttributes);

	public <E extends EntidadBitemporal> List<E> findByPropertyInProcess(
			Class<E> entityClass, String propertyName, final Object value,
			DateTime validOn);

	public <E extends EntidadBitemporal> List<E> findByPropertiesInProcess(
			Class<E> entityClass, Map<String, Object> params, DateTime validOn);

	public <E extends EntidadBitemporal> List<E> findByPropertiesWithOrderInProcess(
			Class<E> entityClass, Map<String, Object> params, DateTime validOn,
			String... orderByAttributes);

	@SuppressWarnings("rawtypes")
	public List executeNativeQueryMultipleResult(String query);

	public Object executeQuerySimpleResult(String query,
			Map<String, Object> parameters);

	@SuppressWarnings("rawtypes")
	public List executeQueryMultipleResult(String query,
			Map<String, Object> parameters);

	public <E extends EntidadBitemporal> void executeActionGrid(String accion,
			E entityClass);

	public <E extends EntidadBitemporal> void refresh(E entity);

	public void updateWithQuery(String query, Map<String, Object> parameters);

	public <T extends Continuity<?, ?>> List<T> getContinuitiesInProcess(Class<T> continuityClass, CotizacionContinuity cotizacionContinuity);
	
}
