/**
 * 
 */
package mx.com.afirme.midas2.dao.bitemporal.siniestros;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.solicitud.SolicitudDataEnTramiteDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.dto.siniestros.IncisoPolizaDTO;
import mx.com.afirme.midas2.dto.siniestros.IncisoSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.ProgramaPagoDTO;
import mx.com.afirme.midas2.dto.siniestros.ReciboProgramaPagoDTO;


/**
 * @author admin
 *
 */
@Local
public interface PolizaSiniestroDao {
	
	public List<IncisoPolizaDTO> buscarIncisosPoliza(String numeroPoliza, String nombreAsegurado, String numeroSerie, 
			Integer numeroInciso, Date fechaOcurridoSiniestro, Short tipoPoliza) ;
	
	public List<IncisoSiniestroDTO> buscarIncisosPolizaFiltro(IncisoSiniestroDTO filtro,  Date validoEn, Date recordFrom);
		
	public List<ProgramaPagoDTO> obtenerProgramasPago(Long idToPoliza, Integer numeroInciso, Date fechaReporteSiniestro);
	
	public List<ReciboProgramaPagoDTO> obtenerRecibosPrograma(Long idToPoliza, Long numeroPrograma, Date fechaReporteSiniestro);
	
	public String obtenerMotivoCobranza(Long idToPoliza, BigDecimal idToSeccion, Integer numeroInciso, Date fechaReporteSiniestro);
	
	/**
	 * Método que regresara el listado de Bitemporales Inciso en caso de existir que coincidan con el numero de solicitud 
	 * y numero de serie del vehículo
	 * @param idToCotizacion
	 * @param numeroSerie
	 * @return
	 */
	public List<BitemporalInciso> obtenerIncisoCartaCobertura( Long idToCotizacion, String numeroSerie );
	
	/*Obtiene la fecha real del inciso. By joksrc*/
	public Date obtenerFechaVigenciaInicioRealPorInciso (Long idIncisoContinuity);
	/*obtiene la fecha de emisión por inciso*/
	public Date obtenerFechaEmisionPorInciso (Long idIncisoContinuity);
	/**
	 * Método que obtiene el listado de IncisoReporteCabina de reportes que no esten cancelados que cuenten con una 
	 * solicitud asociada pero no tengas una poliza asociada.
	 * @return
	 */
	public List<ReporteCabina> obtenerSiniestrosSinPoliza();

	
	public List<BitemporalInciso> obtenerUltimoBitemporalInciso(Integer idToCotizacion, String numeroSerie);
	
	public List<SolicitudDataEnTramiteDTO> buscarSolicitudesPoliza (String numeroSerie, BigDecimal idSolicitud);
	
	public String obtenerDescripcionMotivoCobranza(Long idToPoliza, BigDecimal idToSeccion, Integer numeroInciso, Date fechaReporteSiniestro);
	
	public BigDecimal getIdSeccionFromBitemporalTablesSeccion (Long idContinuidad);
}
