package mx.com.afirme.midas2.dao.bitemporal.endoso.inciso;

import java.math.BigDecimal;

import javax.ejb.Local;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;

import org.joda.time.DateTime;

@Local
public interface IncisoDao {

	public Integer getMaxIncisos(Long idCotizacion, DateTime validoEn);
	
	public Integer getMaxNumeroSecuencia(Long idCotizacion, DateTime validoEn);
	
	public BigDecimal getTotalIncisosCotizacion(BigDecimal idToCotizacion);
	
	public void guardarConductoCobroCotizacionAInciso(CotizacionDTO cotizacionDTO);
	
}
