package mx.com.afirme.midas2.dao.bitemporal;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import org.joda.time.DateTime;

@Local
@SuppressWarnings("rawtypes")
public interface EntidadContinuityDao {

	public Object executeNativeQuerySimpleResult(String query);

	public List executeNativeQueryMultipleResult(String query);

	public Object executeQuerySimpleResult(String query,
			Map<String, Object> parameters);

	public List executeQueryMultipleResult(String query,
			Map<String, Object> parameters);

	public <E extends Entidad> Object persistAndReturnKey(E entity);

	public <E extends Entidad> void persist(E entity);
	
	public <E extends Entidad> void remove(E entity);

	public <C extends EntidadContinuity> C update(C entity);

	public <C extends EntidadContinuity, K> C findByKey(
			Class<C> continuityEntityClass, K key);

	public <C extends EntidadContinuity, K> C getReference(
			Class<C> continuityEntityClass, K key);

	public <C extends EntidadContinuity> Collection<C> findByProperty(
			Class<C> continuityEntityClass, String propertyName,
			final Object value);

	public <E extends Entidad> void refresh(E entity);

	public <B extends EntidadBitemporal> Collection<B> listarBitemporalsFiltrado(
			Class<B> bitemporalEntityClass, Map<String, Object> params,
			DateTime validoEn, DateTime conocidoEn, boolean enProceso, String... orderByAttributes);

	public <B extends EntidadBitemporal> Collection<B> obtenerCancelados(Class<B> bitemporalEntityClass, Long continuitiId, DateTime validoEn, DateTime fechaDeCancelacion);
	
	public <B extends EntidadBitemporal> Collection<B> obtenerCancelados(Class<B> bitemporalEntityClass, 
			DateTime validoEn, DateTime fechaDeCancelacion, Map<String, Object> params);
	
	public void commitContinuity(Long continuityId, Date recordFrom);

	public void rollBackContinuity(Long continuityId);
	
	public void rollBackCoberturas(Long seccionIncisoContinuityId);
	
	public void rollbackCondicionEspecial (Long continuityId , Integer tipo);
	
	public <B extends EntidadBitemporal> void updateBitemporal(Class<B> bitemporalEntityClass, Map<String,Object> params, Long id);
	
}
