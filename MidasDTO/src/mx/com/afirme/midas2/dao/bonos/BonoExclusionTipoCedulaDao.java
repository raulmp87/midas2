package mx.com.afirme.midas2.dao.bonos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.bonos.BonoExclusionTipoCedula;
import mx.com.afirme.midas2.util.MidasException;
/**
 * 
 * @author sams
 *
 */
@Local
public interface BonoExclusionTipoCedulaDao extends EntidadDao {
	public List<BonoExclusionTipoCedula> findById(Long id) throws MidasException;
}
