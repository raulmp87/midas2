package mx.com.afirme.midas2.dao.bonos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.bonos.BonoExclusionesTipoBono;
import mx.com.afirme.midas2.util.MidasException;
/**
 * 
 * @author sams
 *
 */
@Local
public interface BonoExclusionesTipoBonoDao extends EntidadDao{
	public List<BonoExclusionesTipoBono> loadByConfiguration(Long idBono) throws MidasException;
}
