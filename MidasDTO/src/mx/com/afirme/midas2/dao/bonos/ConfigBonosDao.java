package mx.com.afirme.midas2.dao.bonos;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.bonos.ConfigBonoAplicaPromotoria;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoCentroOperacion;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoCobertura;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoEjecutivo;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoGerencia;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoLineaVenta;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoPrioridad;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoProducto;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoPromotoria;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoRamo;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoRangoAplica;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoRangoClaveAmis;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoSeccion;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoSituacion;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoSubramo;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoTipoAgente;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoTipoPromotoria;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.dto.bonos.ConfigBonosDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.CentroOperacionView;
import mx.com.afirme.midas2.dto.fuerzaventa.ConfigBonosNegView;
import mx.com.afirme.midas2.dto.fuerzaventa.EjecutivoView;
import mx.com.afirme.midas2.dto.fuerzaventa.ExcepcionesPolizaView;
import mx.com.afirme.midas2.dto.fuerzaventa.GenericaAgentesView;
import mx.com.afirme.midas2.dto.fuerzaventa.GerenciaView;
import mx.com.afirme.midas2.dto.fuerzaventa.PromotoriaView;
/**
 * 
 * @author vmhersil
 *
 */
@Local
public interface ConfigBonosDao {
	
	public List<CentroOperacionView> getCentroOperacionList()throws Exception;
	
	public List<GerenciaView> getGerenciaList()throws Exception;
	
	public List<EjecutivoView> getEjecutivoList()throws Exception;
	
	public List<PromotoriaView> getPromotoriaList() throws Exception;
	
	public List<ValorCatalogoAgentes> getTipoPromotoriaList() throws Exception;
	
	public List<ValorCatalogoAgentes> getTipoDeAgenteList() throws Exception;
	
	public List<ValorCatalogoAgentes> getPrioridadList() throws Exception;
	
	public List<ValorCatalogoAgentes> getSituacionList() throws Exception;
	
	public List<ValorCatalogoAgentes> getCatalogoPeriodo() throws Exception;
	
	public List<ValorCatalogoAgentes> getCatalogoModoEjecucion() throws Exception;
	
	public List<ValorCatalogoAgentes> getLineasVenta() throws Exception;
	
	public List<GenericaAgentesView> getProductos() throws Exception;
	
	public List<GenericaAgentesView> getRamos() throws Exception;
	
	public List<GenericaAgentesView> getProductosPorLineaVenta(String lineasVenta) throws Exception;
	
	public List<GenericaAgentesView> getRamosPorProductos(List<ConfigBonoProducto> productos)throws Exception;
	
	public List<GenericaAgentesView> getSubramosPorRamos(List<ConfigBonoRamo> ramos)throws Exception;
	
	public List<GenericaAgentesView> getLineasNegocioPorRamos(List<ConfigBonoRamo> ramos) throws Exception;
	
	public List<GenericaAgentesView> getLineasNegocioPorProducto(List<ConfigBonoProducto> productos) throws Exception;
	
	public List<GenericaAgentesView> getCoberturasPorLineasNegocio(List<ConfigBonoSeccion> lineasNegocio) throws Exception;
	
	public List<ConfigBonos> findByFilters(ConfigBonos filtro) throws Exception;
	
	public List<ConfigBonosDTO> findByFiltersView(ConfigBonosDTO filtro);
	
	public ConfigBonos loadById(ConfigBonos config) throws Exception;
	
	/*TODO Servicio Temporal en lo que se hace un refactor para corregir el modelo de configuracion de Bonos (Si estuviera bien esto no seria necesario)*/
	public ConfigBonos findById(Long configBonosId, Date asOf) throws Exception;
	
	public void deleteConfiguration(ConfigBonos configuration) throws Exception;
	
	public List<ConfigBonoCentroOperacion> getCentrosOperacionPorConfiguracion(ConfigBonos config) throws Exception;
	
	public List<ConfigBonoGerencia> getGerenciasPorConfiguracion(ConfigBonos config) throws Exception;
	
	public List<ConfigBonoEjecutivo> getEjecutivosPorConfiguracion(ConfigBonos config) throws Exception;
	
	public List<ConfigBonoPromotoria> getPromotoriasPorConfiguracion(ConfigBonos config) throws Exception;
	
	public List<ConfigBonoPrioridad> getPrioridadesPorConfiguracion(ConfigBonos config) throws Exception;
	
	public List<ConfigBonoTipoPromotoria> getTiposPromotoriaPorConfiguracion(ConfigBonos config) throws Exception;
	
	public List<ConfigBonoTipoAgente> getTiposAgentePorConfiguracion(ConfigBonos config) throws Exception;
	
	public List<ConfigBonoSituacion> getSituacionesPorConfiguracion(ConfigBonos config) throws Exception;
	
	public List<GenericaAgentesView> getProductosPorLineaVentaView(String lineasVenta) throws Exception;
	
	public List<GenericaAgentesView> getRamosPorProductosView(String productos) throws Exception;
	
	public List<GenericaAgentesView> getSubramosPorRamoView(String ramos) throws Exception;
	
	public List<GenericaAgentesView> getLineaNegocioPorRamoView(String ramos) throws Exception;

	public List<GenericaAgentesView> getLineasNegocioPorProductoView(String productos) throws Exception;
	
	public List<GenericaAgentesView> getCoberturasPorLineasNegocioView(String lineasNegocio) throws Exception;
	
	public List<GerenciaView> getGerenciasConCentrosOperacionExcluyentes(List<Long> configGerencias,List<Long> configCentrosOperacion);
	
	public List<EjecutivoView> getEjecutivosConGerenciasExcluyentes(List<Long> configEjecutivos,List<Long> configGerencias,List<Long> configCentros);
	
	public List<PromotoriaView> getPromotoriasConEjecutivosExcluyentes(List<Long> configPromotorias,List<Long> configEjecutivos,List<Long> configGerencias,List<Long> configCentros);

	public List<ConfigBonosNegView> getListNegociosDesasociado(ConfigBonosNegView filtro);
	
	public List <ExcepcionesPolizaView> getPolizaExcepsiones(ExcepcionesPolizaView idPoliza,  ConfigBonos configuracion);
	
	public ExcepcionesPolizaView getInfoPoliza(ExcepcionesPolizaView poliza) throws Exception;
	
	public List<ConfigBonoLineaVenta> getLineaVentaPorConfiguracion(ConfigBonos config) throws Exception;
	
	public List<ConfigBonoProducto> getProductosPorConfiguracion(ConfigBonos config) throws Exception;
	
	public List<ConfigBonoRamo> getRamosPorConfiguracion(ConfigBonos config) throws Exception;
	
	public List<ConfigBonoSubramo> getSubRamosPorConfiguracion(ConfigBonos config) throws Exception;
	
	public List<ConfigBonoSeccion> getSeccionesPorConfiguracion(ConfigBonos config) throws Exception;
	
	public List<ConfigBonoCobertura> getCoberturasPorConfiguracion(ConfigBonos config) throws Exception;
	
	public List<ValorCatalogoAgentes> getLineasVentasRelacionadas(ConfigBonos config) throws Exception;
	
	public List<GenericaAgentesView> getProductoRelacionados(ConfigBonos config) throws Exception;
	
	public List<GenericaAgentesView> getRamoRelacionados(ConfigBonos config) throws Exception;
	
	public List<GenericaAgentesView> getSubRamoRelacionados(ConfigBonos config) throws Exception;
	
	public List<GenericaAgentesView> getLineaNegociosRelacionados(ConfigBonos config) throws Exception;
	
	public List<GenericaAgentesView> getCoberturasRelacionados(ConfigBonos config) throws Exception;
	
	public List<GenericaAgentesView> getProductosPorClaveLinea(String clave) throws Exception;

	//***************************************
	public List<ExcepcionesPolizaView>getPolizaByIDConfigBono(Long id) throws Exception;
	
	public List<Agente>getAgentesByIdConfigBono(Long id) throws Exception;
	
	public List<ConfigBonoAplicaPromotoria>getAplicaPromotoriasByIdConfigBono(Long id)throws Exception;
	
	public List<Agente>getAplicaAgentesByIdConfigBono(Long id)throws Exception;
	
	
	public ConfigBonos saveExclusiones(ConfigBonos exclusiones) throws Exception;
	
	public List<AgenteView> getAgentePorConfiguracion(ConfigBonos configuracion) throws Exception;
	
	/**
	 * Obtiene los agentes resultantes de acuerdo a al configuracion de bonos seleccionada.
	 * @param configuracion
	 * @return
	 * @throws Exception
	 */
	public List<AgenteView> obtenerAgentesPorConfiguracionCapturada(ConfigBonos configuracion) throws Exception;
	/**
	 * Metodo para obtener el listado de agentes por lista de centros de operacion seleccionados, ejecutivos, promotorias, etc...
	 * @param centroOperacionList
	 * @param ejecutivoList
	 * @param gerenciaList
	 * @param promotoriaList
	 * @param situacionesList
	 * @param tiposAgenteList
	 * @param tiposPromotoriaList
	 * @param agenteList
	 * @param prioridadesList
	 * @return
	 * @throws Exception
	 */
	public List<AgenteView> obtenerAgentesPorConfiguracionCapturada(List<CentroOperacion> centroOperacionList,
			List<Ejecutivo> ejecutivoList,
			List<Gerencia> gerenciaList,
			List<Promotoria> promotoriaList,
			List<ValorCatalogoAgentes> situacionesList,
			List<ValorCatalogoAgentes> tiposAgenteList,
			List<ValorCatalogoAgentes> tiposPromotoriaList,
			List<Agente> agenteList,
			List<ValorCatalogoAgentes> prioridadesList) throws Exception;
	
	public List<PromotoriaView> obtenerPromotoriasPorConfiguracionCapturada(List<CentroOperacion> centroOperacionList,
			List<Ejecutivo> ejecutivoList,
			List<Gerencia> gerenciaList,
			List<Promotoria> promotoriaList,			
			List<ValorCatalogoAgentes> tiposPromotoriaList) throws Exception;
	
	public List<ConfigBonoRangoAplica> getRangosPorConfiguracion(ConfigBonos config) throws Exception;
	
	public List<GenericaAgentesView> getLineasNegocioPorRamoProductoView(String productos,String ramos) throws Exception;
	
	
	public List<ConfigBonoRangoClaveAmis> getListaBonoRangosClaveAmis(ConfigBonos config) throws Exception;

	public List<AgenteView> obtenerAgentesPorFuerzaVenta(Long centroOperacion,
			Long ejecutivo,
			Long gerencia,
			Long promotoria,
			Long clasificacionAgente,
			Long agente) throws Exception;
	
	public void actualizarClasificacionConfigBono(Long idConfigBono)throws Exception;
	
}
