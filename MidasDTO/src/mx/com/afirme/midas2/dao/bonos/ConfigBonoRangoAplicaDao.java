package mx.com.afirme.midas2.dao.bonos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoRangoAplica;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;

@Local
public interface ConfigBonoRangoAplicaDao extends EntidadDao{

	public List<ConfigBonoRangoAplica> loadByConfigBono(ConfigBonos configBono) throws Exception;
	public void  validarRangofactores(List<ConfigBonoRangoAplica> configBonoRangoAplica) throws Exception;
}
