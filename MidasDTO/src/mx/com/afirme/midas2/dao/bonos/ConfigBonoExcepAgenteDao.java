package mx.com.afirme.midas2.dao.bonos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoExcepAgente;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;

@Local
public interface ConfigBonoExcepAgenteDao extends EntidadDao{
	public List<ConfigBonoExcepAgente> loadByConfigBono(ConfigBonos configBono) throws Exception;
}
