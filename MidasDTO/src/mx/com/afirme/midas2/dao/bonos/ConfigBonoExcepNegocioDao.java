package mx.com.afirme.midas2.dao.bonos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoExcepNegocio;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
/**
 * 
 * @author cavalos
 *
 */

@Local
public interface ConfigBonoExcepNegocioDao extends EntidadDao{
	public List<ConfigBonoExcepNegocio> loadByConfigBono(ConfigBonos configBono) throws Exception;
}
