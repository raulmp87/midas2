package mx.com.afirme.midas2.dao.bonos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.bonos.ProgramacionBono;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.dto.fuerzaventa.CalculoBonoEjecucionesView;
import mx.com.afirme.midas2.dto.fuerzaventa.GenericaAgentesView;
import mx.com.afirme.midas2.util.MidasException;

@Local
public interface ProgramacionBonoDao extends EntidadDao {
	
	public List<GenericaAgentesView> getNegocioEspeciales() throws Exception;
	
	public List<GenericaAgentesView> getConfiguracionBonos(Long idTipoBono) throws Exception;
	
	public List<PolizaDTO> getProduccionDePolizas(ConfigBonos config);
	
	public List<PolizaDTO> getProduccionDePolizasNegocioEspecial(Negocio negocio);
	
	public ProgramacionBono loadById(Long id) throws MidasException;
	
	public ProgramacionBono loadById(ProgramacionBono programacion) throws MidasException;
	
	public String eliminarBonosCero(String ids) throws MidasException;
	
	public  List<ConfigBonos> bonoIsVigente(List<ConfigBonos> configuracionBonoList) throws MidasException;
	
	public  List<ConfigBonos> excluirBonoNoVigentes(List<ConfigBonos> listaBonosAExcluir, List<ConfigBonos> configuracionBonoList) throws MidasException;
	
	public List<CalculoBonoEjecucionesView> listaCalculoBonosMonitorEjecucion() throws MidasException;
}
