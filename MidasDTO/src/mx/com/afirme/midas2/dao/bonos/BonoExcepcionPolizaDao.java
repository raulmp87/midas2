package mx.com.afirme.midas2.dao.bonos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.bonos.BonoExcepcionPoliza;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;

/**
 * 
 * @author sams
 *
 */

@Local
public interface BonoExcepcionPolizaDao extends EntidadDao {
	public List<BonoExcepcionPoliza> loadByConfigBono(ConfigBonos configBono) throws Exception;
}
