package mx.com.afirme.midas2.dao.componente.scrollable;

import java.util.List;

import javax.ejb.Local;
import javax.persistence.Query;

import mx.com.afirme.midas.base.Scrollable;

@Local
public interface ScrollableNavigationDao {
	
	public <T extends Scrollable> List<T> getResultList (Query query, T scrollableFilter);
	
}
