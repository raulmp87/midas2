package mx.com.afirme.midas2.dao.componente.incisos;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.ejb.Local;

import org.joda.time.DateTime;

import com.anasoft.os.daofusion.bitemporal.RecordStatus;

import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.dto.impresiones.DatosLineasDeNegocioDTO;
import mx.com.afirme.midas2.dto.impresiones.ReferenciaBancariaDTO;
import mx.com.afirme.midas2.service.componente.incisos.TipoQueryListadoIncisos;

@Local
public interface ListadoIncisosDinamicoDao {
	
	List<BitemporalInciso> buscarIncisosFiltrado(IncisoCotizacionDTO filtros, Long cotizacionContinuityId, RecordStatus recordStatusFilter, boolean getCancelados);
	
	List<BitemporalInciso> buscarIncisosFiltrado(IncisoCotizacionDTO filtros, Long cotizacionContinuityId, Date validoEn, RecordStatus recordStatusFilter, boolean getCancelados);
	
	List<BitemporalInciso> buscarIncisosFiltrado(IncisoCotizacionDTO filtros, Long cotizacionContinuityId, Date validoEn, Date recordFrom, RecordStatus recordStatusFilter, boolean getCancelados);
	
	List<BitemporalInciso> buscarIncisosFiltrado(IncisoCotizacionDTO filtros, Long cotizacionContinuityId, Date validoEn, Date recordFrom, RecordStatus recordStatusFilter, boolean getCancelados, boolean esConsultaPoliza);
	
	public List<BitemporalInciso> buscarIncisosFiltradoPerdidaTotal(IncisoCotizacionDTO filtros, Long cotizacionContinuityId);
	
	public Long obtenerTotalPaginacionIncisos(IncisoCotizacionDTO filtros, Long cotizacionContinuityId, RecordStatus recordStatusFilter, boolean getCancelados);
    
    public Long obtenerTotalPaginacionIncisos(IncisoCotizacionDTO filtros, Long cotizacionContinuityId, 
    		Date validoEn, RecordStatus recordStatusFilter, boolean getCancelados, TipoQueryListadoIncisos tipoQuery);
    
    public List<BitemporalInciso> buscarIncisosFiltradoDesagrupacionRecibos(IncisoCotizacionDTO filtros, 
    		Date validoEn, Date recordFrom, Long cotizacionContinuityId);
    
	public List<NegocioSeccion> getSeccionList(Long cotizacionContinuityId, Date validoEn, RecordStatus recordStatus, boolean getCancelados);
	
	public Set<String> listarIncisosDescripcion(Long cotizacionContinuityId, Date validoEn, RecordStatus recordStatus, boolean getCancelados);
	
	public Long getNumeroIncisos(Long cotizacionContinuityId, DateTime validFromDT, DateTime recordFromDT, boolean getCancelados);
	
	/**
	 * Obtiene un resumen de las lineas de negocio con datos como Número de
	 * incisos y prima neta de una póliza a cierta fecha. Se creó para optimizar
	 * el tiempo de respuesta al momento de imprimir una póliza con muchos
	 * incisos, antes se calculaba todo en java.
	 * 
	 * @param idToPoliza
	 * @param numeroEndoso
	 * @return
	 */
	public List<DatosLineasDeNegocioDTO> getDatosLineaNegocio(BigDecimal idToPoliza, Short numeroEndoso);
	/**
	 * Obtiene las referencias Bancarias
	 * @param idToPoliza
	 * @param numeroEndoso
	 * @return
	 */
	public List<ReferenciaBancariaDTO> getReferenciasBancarias(BigDecimal idToPoliza);
}
