package mx.com.afirme.midas2.dao.tarea;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

/**
 * <p>Clase que contiene definida
 * los metodos para acceso a datos para
 * las tareas programadas migradas de oracle
 * al ambiente de <b>MIDAS II</b></p>
 * 
 * @author SEGUROS AFIRME
 * 
 * @since 05042017
 * 
 * @version 1.0
 * 
 * @category Data Acces Object DAO
 *
 */
@Local
public interface EscribeArchivoTextoDao {
	
	/**
	 * M&eacute;todo que realiza la ejecucion de un proceso
	 * para poder obtener la informacion necesaria para
	 * poder escibir el archivo de SELCOR
	 * 
	 * @return Lista que contiene los renglones del archivo a esribir
	 */
	public List<String> obtenerInformacionSelcor();
	
	/**
	 * M&eacute;todo que realiza la ejecuci&oacute;n de un proceso
	 * para recuperar la informaci&oacute;n  requerida
	 * para poder escribir el arcivo de CYBER.
	 * 
	 * @return
	 */
	public List<String> obtenerInformacionCYBER();
	
	/**
	 * M&eacute;todo que ejecuta el un prceso
	 * para obtener la informaci&oacute;n necesaria
	 * para escribir el archivo correspondiente.
	 * 
	 * @param fechaProceso fecha en la que se ejecuta el proceso.
	 * 
	 * @return
	 */
	public StringBuffer obtenerWinstonData(Date fechaProceso);
	
	/**
	 * M6eacute;todo que obtiene la informacion de WD para autos
	 * de una cotizacion en especifico.
	 * 
	 * @param idCotizacion identificaci&oacute;n de la póliza.
	 * 
	 * @return 
	 */
	public List<StringBuffer> obtenerWinstonDataAutos(Long idCotizacion);
	
	/**
	 * Metodo que obtiene la informacion correspondiente
	 * para escribir el archivo de WD de vida.
	 * 
	 * @param idCotizacion id de la cotizacion de la 
	 * que se va a escribir el archivo.
	 * 
	 * @return
	 */
	public StringBuffer obtenerWinstonDataVida(Long idCotizacion);
	
	/**
	 * M&eacute;todo que obtiene la informacion 
	 * necesaria para escribir el archivo de 
	 * WD director.
	 * 
	 * @param idCotizacion id de la cotizacion.
	 * 
	 * @return
	 */
	public List<StringBuffer> obtenerWinstonDataDir(Long idCotizacion);
	
	/**
	 * M&eacute;todo que realiza la consulta de la informaci&oacute;n
	 * necesaria para escribir el archio de CAJERO.
	 * 
	 * @return
	 */
	public StringBuffer obtenerInformacionCajero();
	
	/**
	 * M&eacute;todo que realiza la consulta de la 
	 * informaci&oacute;n para generar el reporte
	 * de primas en deposito.
	 * 
	 * @param fechaProceso fecha sobre la que se
	 * va a generar el reporte.
	 * 
	 * @return
	 */
	public StringBuffer obtenerPrimasDepositoWinstonData(Date fechaProceso);
	
	/**
	 * M&eacute;todo que genera un reporte de las pólizas
	 * canceladas.
	 * 
	 * @param fechaProceso fecha en laque se ejecuta el proceso.
	 * 
	 * @return
	 */
	public List<StringBuffer> obtenerResultadoCancelacion(Date fechaProceso);
	
	/**
	 * M&eacute;todo que obtiene la informaci&oacute;n del cliente
	 * para escribir el reporte de clietne.
	 * 
	 * @return
	 */
	public List<String> obtenerInformacionCliente();
	
	/**
	 * <p>M&eacute;todo que actualiza la informaci&oacute;n de
	 * la tabla <b>SEYCOS.PERSONA_EXT</b> en la columna ID_CIENTEBANCA</p>
	 * 
	 * @param idCliente Identificador del ID_CLIENTEBANCA
	 * 
	 * @param idCotizacion Identificaci&oacute;n de la cotizacion a que est&aacute;
	 * asociado este cliente, dentro de seguros.
	 */
	public void actualizaInformacionClienteBanca(Long idCliente, Long idCotizacion);
}
