package mx.com.afirme.midas2.dao.tarea;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.bitacora.pago.programado.ibs.BitacoraPagoProgIBSTO;

/**
 * <p>Clase que contiene la definici&oacute;n de los m&eacute;todos para
 * acceso a datos para la tarea programada de <code>LeePagoProgIBSService</code></p>
 * 
 * @author SEGUROS AFIRME
 * 
 * @category Data Acces Object DAO
 * 
 * @since 05042017
 * 
 * @version 1.0
 *
 */
@Local
public interface LeePagoProgIBSDao{
	
	/**
	 * <p>M&eacute;todo que realiza la persistencia 
	 * de los objetos que contienen la informaci&oacute;n recuperada
	 * del archivo de PagoProg.txt</p>
	 * <p>Para su posterior actualizacion de esta informacion.</p>
	 * 
	 * @param entities Lista de objetos que contienen la informaci&oacute;n de ibs
	 */
	public void saveBitacoraPagoProgIBS(List<BitacoraPagoProgIBSTO> entities);
	
	/**
	 * <p>M&eacute;todo que ejecuta el proceso de MIDAS que complementa
	 * la informaci&oacute;n de la bitacora antes de ser procesada del lado 
	 * de SEYCOS.</p>
	 */
	public void complementarBitacoraPagoProgIBS();
	
	/**
	 * M&eacute;todo que realiza la ejecuci&oacute;n del proceso
	 * de migraci&oacute;n del contenido de la tabla BitacoraPagoPrgIBS
	 * a su contraparte del lado de SEYCOS.
	 */
	public void migrarInformacionPagoProgIBS();
}
