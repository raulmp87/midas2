package mx.com.afirme.midas2.dao.tarea;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.notificacion.estratega.NotificacionEstrategaDTO;

/**
 * Clase que contiene la definici&oacute;n de las
 * ejecuciones de accesos a datos hechas por el objeto <code>ProgramacionNotificaEstrategas</code>
 * 
 * @author SEGUROS AFIRME
 * 
 * @since 05042017
 * 
 * @category Data Access Object DAO
 * 
 * @version 1.0
 *
 */
@Local
public interface NotificaEstrategasDao{
	
	/**
	 * M&eacute;todo que obtiene la informaci&oacute;n 
	 * de las notificaciones que se realizan a los estrategas.
	 * 
	 * @return Lista de objetos que contiene la informaci&oacute;n
	 * que se mandar&aacute; a los estrategas.
	 */
	public List<NotificacionEstrategaDTO> obtenerNotificaciones();
	
	/**
	 * M&eacute;todo que obtiene los correos de los estrategas
	 * a los cuales se les notificar&aacute; esta informaci&oacute;n.
	 * 
	 * @return lista que contiene los correos.
	 */
	public List<String> obtenerEstrategasDestinatarios();
	
	/**
	 * M&eacute;todo que obtiene el titulo y cuerpo del correo.
	 * 
	 * @return Lista que contiene el titulo y el cuerpo del correo.
	 */
	public List<String> obtenerTituloyCuerpoMensaje();
}
