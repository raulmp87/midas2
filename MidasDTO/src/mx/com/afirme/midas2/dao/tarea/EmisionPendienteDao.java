package mx.com.afirme.midas2.dao.tarea;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.tarea.EmisionPendiente;

@Local
public interface EmisionPendienteDao {
	
	public List<EmisionPendiente> buscarEmisionesPendientes(EmisionPendiente filtro);

	public Long obtenerTotalPaginacionCancelacionesPendientesAutos(EmisionPendiente filtro);
	
	public List<EmisionPendiente> buscarCancelacionesPendientesAutos(EmisionPendiente filtro);
	
	public List<Object[]> monitorearEmisionPendiente(EmisionPendiente filtro);
}

