package mx.com.afirme.midas2.dao.fortimax;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoAplicacionFortimax;
import mx.com.afirme.midas2.util.MidasException;
/**
 * 
 * @author vmhersil
 *
 */
@Local
public interface CatalogoAplicacionFortimaxDao extends EntidadDao{
	/**
	 * Obtiene los datos de la aplicacion por medio del nombre
	 * @param nombreAplicacion
	 * @return
	 * @throws MidasException
	 */
	public CatalogoAplicacionFortimax getAplicacionPorNombre(String nombreAplicacion) throws MidasException;
	/**
	 * Carga los datos de la aplicacion por el id de la aplicacion
	 * @param idAplicacion
	 * @return
	 * @throws MidasException
	 */
	public CatalogoAplicacionFortimax loadById(Long idAplicacion) throws MidasException;
	
	public List<CatalogoAplicacionFortimax> findByFilters(CatalogoAplicacionFortimax aplicacion);
	
	public Long save(CatalogoAplicacionFortimax aplicacion) throws MidasException;
	
	public void delete(Long idAplicacion) throws MidasException;
	
	public Long delete(CatalogoAplicacionFortimax aplicacion) throws MidasException;
	
	public String[] obtenerParametrosAplicacion(String nombreAplicacion) throws MidasException;
	
	public String obtenerParametroLlavePorAplicacion(String nombreAplicacion) throws MidasException;
}
