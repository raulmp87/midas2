package mx.com.afirme.midas2.dao.fortimax;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoDocumentoFortimax;
import mx.com.afirme.midas2.util.MidasException;
/**
 * 
 * @author vmhersil
 *
 */
@Local
public interface DocumentoCarpetaFortimaxDao extends EntidadDao{
	public List<CatalogoDocumentoFortimax> obtenerDocumentosPorAplicacion(String nombreAplicacion) throws MidasException;
	
	public List<CatalogoDocumentoFortimax> obtenerDocumentosPorAplicacionTipoPersona(String nombreAplicacion,Long idTipoPersona) throws MidasException;
	/**
	 * Obtiene documentos de una carpeta o seccion de una aplicacion
	 * @param nombreAplicacion
	 * @param nombreCarpeta
	 * @return
	 * @throws MidasException
	 */
	public List<CatalogoDocumentoFortimax> obtenerDocumentosPorCarpeta(String nombreAplicacion,String nombreCarpeta) throws MidasException;
	/**
	 * Obtiene los documentos de una carpeta o seccion por aplicacion y unicamente los que son requeridos
	 * @param nombreAplicacion
	 * @param nombreCarpeta
	 * @return
	 * @throws MidasException
	 */
	public List<CatalogoDocumentoFortimax> obtenerDocumentosRequeridosPorCarpeta(String nombreAplicacion,String nombreCarpeta) throws MidasException;
	/**
	 * Obtiene los documentos de una carpeta considerando los que son de una tipo de persona especifica
	 * @param nombreAplicacion
	 * @param nombreCarpeta
	 * @param idTipoPersona
	 * @return
	 * @throws MidasException
	 */
	public List<CatalogoDocumentoFortimax> obtenerDocumentosPorCarpetaConTipoPersona(String nombreAplicacion,String nombreCarpeta,Long idTipoPersona) throws MidasException;
	/**
	 * Obtiene los documentos requeridos de una carpeta considerando los que son de una tipo de persona especifica
	 * @param nombreAplicacion
	 * @param nombreCarpeta
	 * @param idTipoPersona
	 * @return
	 * @throws MidasException
	 */
	public List<CatalogoDocumentoFortimax> obtenerDocumentosRequeridosPorCarpetaConTipoPersona(String nombreAplicacion,String nombreCarpeta,Long idTipoPersona) throws MidasException;
	
	public Long save(CatalogoDocumentoFortimax documento) throws MidasException;
	
	public void delete(Long idDocumento) throws MidasException;
	
	public Long delete(CatalogoDocumentoFortimax documento) throws MidasException;
}
