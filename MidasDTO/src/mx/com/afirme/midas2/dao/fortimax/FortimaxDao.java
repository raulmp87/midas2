package mx.com.afirme.midas2.dao.fortimax;

import javax.ejb.Local;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;

@Local
public interface FortimaxDao extends EntidadDao{
	
	public String[] generateExpedient(String tituloAplicacion, String[] fieldValues)throws Exception;

	public String[] generateDocument(Long id, String tituloAplicacion, String documentName,String folderName)throws Exception;
	
	public String[] generateLinkToDocument(Long id, String tituloAplicacion, String nombreDocumento) throws Exception;
	
	public String[] getDocumentFortimax(Long id,String tituloAplicacion) throws Exception;
	
	public String[] uploadFile(String fileName,  TransporteImpresionDTO transporteImpresionDTO, String tituloAplicacion, String[] expediente, String carpeta) throws Exception;
}
