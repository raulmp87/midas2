package mx.com.afirme.midas2.dao.fortimax;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoAplicacionFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.ParametroAplicacionFortimax;
import mx.com.afirme.midas2.util.MidasException;
/**
 * 
 * @author vmhersil
 *
 */
@Local
public interface ParametroAplicacionFortimaxDao extends EntidadDao{
	/**
	 * Obtiene los parametros de X aplicacion
	 * @param nombreAplicacion Nombre de la aplicacion, NO el de Fortimax
	 * @return
	 * @throws MidasException
	 */
	public List<ParametroAplicacionFortimax> obtenerParametrosPorAplicacion(String nombreAplicacion) throws MidasException;
	/**
	 * Obtiene los parametros de X aplicacion
	 * @param aplicacion
	 * @return
	 * @throws MidasException
	 */
	public List<ParametroAplicacionFortimax> obtenerParametrosPorAplicacion(CatalogoAplicacionFortimax aplicacion) throws MidasException;
	/**
	 * Obtiene el campo llave de una aplicacion de la lista de los parametros de la misma aplicacion
	 * @param nombreAplicacion
	 * @return
	 * @throws MidasException
	 */
	public ParametroAplicacionFortimax obtenerParametroLlavePorAplicacion(String nombreAplicacion) throws MidasException;
	/**
	 * Obtiene los parametros de X aplicacion
	 * @param idAplicacion
	 * @return
	 * @throws MidasException
	 */
	public List<ParametroAplicacionFortimax> obtenerParametrosPorAplicacion(Long idAplicacion) throws MidasException;
	
	public Long save(ParametroAplicacionFortimax parametro) throws MidasException;
	
	public void delete(Long idParametro) throws MidasException;
	
	public Long delete(ParametroAplicacionFortimax parametro) throws MidasException;
}
