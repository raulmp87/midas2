package mx.com.afirme.midas2.dao.fortimax;

import java.util.Map;
import javax.ejb.Local;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.ProcesosFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.ProcesosFortimax.PROCESO_FORTIMAX;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
@Local
public interface FortimaxV2Dao extends EntidadDao{
	public static final String WSDL_DOCUMENTO = "/DocumentoService?wsdl";
	public static final String WSDL_LINK = "/LinkService?wsdl";
	public static final String WSDL_EXPEDIENTE = "/ExpedienteService?wsdl";
	public static final String WSDL_LOGIN = "/LoginService?wsdl";
	public static final String WSDL_DOWNLOAD = "/DownloadService?wsdl";
	public static final String WSDL_MAKEZIP = "/MakeZipService?wsdl";
	public static final String WSDL_BUSQUEDA = "/BusquedaService?wsdl";
	
	public static final String CLAVE_DOCUMENTO = "D";
	public static final String CLAVE_CARPETA = "C";
	
	
	/**
	 * Genera Expediente 
	 * @param tituloAplicacion
	 * @param fieldValues
	 * @return String[]
	 */
	public String[] generateExpedient(String tituloAplicacion, String[] fieldValues)throws Exception;
	/**
	 * Genera documento en Gaveta 
	 * @param id
	 * @param tituloAplicacion
	 * @param documentName
	 * @param folderName
	 * @return String[]
	 */
	public String[] generateDocument(Long id, String tituloAplicacion, String documentName,String folderName)throws Exception;
	/**
	 * Genera Liga a Ifimax 
	 * @param id
	 * @param tituloAplicacion
	 * @param nombreDocumento
	 * @param user
	 * @param password
	 * @return String
	 */
	public String generateLinkToDocument(Long id, String tituloAplicacion, String nombreDocumento, String user, String password, String nodo) throws Exception;
	/**
	 * obtiene documentos que han sido anexados al expediente 
	 * @param id
	 * @param tituloAplicacion
	 * @return String[]
	 */
	public String[] getDocumentFortimax(Long id,String tituloAplicacion) throws Exception;
	/**
	 * Sube Archivo a Gaveta 
	 * @param fileName
	 * @param transporteImpresionDTO
	 * @param tituloAplicacion
	 * @param expediente
	 * @param carpeta
	 * @return String[]
	 */
	public String[] uploadFile(String fileName,  TransporteImpresionDTO transporteImpresionDTO, String tituloAplicacion, String[] expediente, String carpeta) throws Exception;
	/**
	 * Genera Token de acceso 
	 * @param duration
	 * @return String
	 */
	public String getToken(long duration) throws Exception;
	/**
	 * Obtiene nodo de localziacion de documento
	 * @param id
	 * @param tituloAplicacion
	 * @param nombreElemento
	 * @param tipoElemento
	 * @param nodoOnly
	 * @return String[]
	 */
	public String getNodo(Long id,String tituloAplicacion, String nombreElemento, String tipoElemento,boolean nodoOnly )throws Exception ;
	/**
	 * Descarga Archivo
	 * @param fileName
	 * @param tipoElemento
	 * @param tipoElemento
	 * @param id
	 * @return  byte[]
	 */
	public byte[] downloadFile (String fileName,String tipoElemento , String tituloAplicacion,Long id ) throws Exception ;
	/**
	 * Descarga Archivo de Fortimax
	 * @param fileName
	 * @param tipoElemento
	 * @param tituloAplicacion
	 * @param id
	 * @return byte[]
	 */
	public byte[] downloadFileBP (String fileName, String tipoElemento, String tituloAplicacion, Long id) throws Exception ;

	/**
	 * Obtiene lista de archivos por proceso 
	 * @param proceso
	 * @return Map<String, String> 
	 */
	public  Map<String, String> getListaArchivosByProceso (PROCESO_FORTIMAX proceso) throws Exception;
	
	/**
	 *  Obtiene lista de archivos almacenados por proceso 
	 * @param proceso
	 * @param fortimaxID
	 * @param carpeta
	 * @return Map<String, String>
	 */
	public Map<String, String> getDocumentosAlmacenadosByProceso(
			PROCESO_FORTIMAX proceso, Long fortimaxID,String carpeta) throws Exception ;
	

	/**
	 * Obtiene entidad proceso 
	 * @param proceso
	 * @return ProcesosFortimax
	 */
	public ProcesosFortimax getProceso  (PROCESO_FORTIMAX proceso)  throws Exception;
	/**
	 * Genera liga a Ifimax por proceso 
	 * @param id
	 * @param aplicacion
	 * @param nombreDocumento
	 * @param defaultUser
	 * @param proceso
	 * @return String
	 */
	public String generateLinkToDocumentByProceso(Long id, String aplicacion, String nombreDocumento,boolean defaultUser,PROCESO_FORTIMAX proceso, String nodo)	throws Exception;
}
