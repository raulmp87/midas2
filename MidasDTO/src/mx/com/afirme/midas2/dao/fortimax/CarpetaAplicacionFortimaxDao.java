package mx.com.afirme.midas2.dao.fortimax;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CarpetaAplicacionFortimax;
import mx.com.afirme.midas2.util.MidasException;
/**
 * 
 * @author vmhersil
 *
 */
@Local
public interface CarpetaAplicacionFortimaxDao extends EntidadDao{
	public CarpetaAplicacionFortimax obtenerCarpetaEspecificaPorAplicacion(String nombreAplicacion,String nombreCarpeta) throws MidasException;
	
	public List<CarpetaAplicacionFortimax> obtenerCarpetasPorAplicacion(Long idAplicacion) throws MidasException;
	
	public List<CarpetaAplicacionFortimax> obtenerCarpetasPorAplicacion(String nombreAplicacion) throws MidasException;
	
	public Long save(CarpetaAplicacionFortimax carpeta) throws MidasException;
	
	public void delete(Long idCarpeta) throws MidasException;
	
	public Long delete(CarpetaAplicacionFortimax carpeta) throws MidasException;
	
}