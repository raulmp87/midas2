package mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.agrupadortarifa;

import java.math.BigDecimal;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccion;

@Local
public interface NegocioAgrupadorTarifaSeccionDao extends Dao<BigDecimal, NegocioAgrupadorTarifaSeccion> {

	public NegocioAgrupadorTarifaSeccion buscarPorNegocioSeccionYMoneda(NegocioSeccion negocioSeccion, BigDecimal idMoneda);
	
}
