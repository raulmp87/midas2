package mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.catalogos.CobPaquetesSeccion;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
@Local
public interface NegocioSeccionDao extends Dao<BigDecimal, NegocioSeccion>{

	public List<NegocioSeccion> listarNegSeccionPorIdNegTipoPoliza(BigDecimal idToNegTipoPoliza);
	
	public List<NegocioSeccion> listarNegSeccionPorCotizacion(BigDecimal idToCotizacion);
	
	/**
	 * Metodo que permite obtener un listado de coberturas
	 * de acuerdo a las relacion Negocio-Cobertura-Paquete-Seccion
	 * que no se encuentran asociadas al Negocio-Seccion, pero
	 * estan disponibles en la entidad CobPaquetesSeccion
	 * @param negocioSeccion
	 * @return
	 */
	public List<CobPaquetesSeccion> listarCoberturasPosible(NegocioPaqueteSeccion negocioPaqueteSeccion);
	
	public NegocioSeccion getPorIdNegTipoPolizaIdToSeccion(BigDecimal idToNegTipoPoliza, BigDecimal idToSeccion);

	public List<NegocioSeccion> listarNegocioSeccionPorProductoNegocioTipoPoliza(ProductoDTO producto, Negocio negocio, TipoPolizaDTO tipoPoliza);
	
	public NegocioSeccion getPorProductoNegocioTipoPolizaSeccionDescripcion(ProductoDTO producto, Negocio negocio, TipoPolizaDTO tipoPoliza, String seccionDescripcion);
	
	public List<SeccionDTO> listarSeccionNegocioByClaveNegocio(String claveNegocio);
	
	public List<NegocioSeccion> listarNegocioSeccionPorIdProductoNegocioTipoPoliza(BigDecimal idToProducto, Long idToNegocio, BigDecimal idToTipoPoliza);
	

	/**
	 * MSN - Obtener todas las secciones ligadas al negocio que sean distintas 
	 * @param idToNegocio
	 * @return
	 */
	public List<SeccionDTO> listarSeccionNegocioByIdNegocio(BigDecimal idToNegocio);
	
	public NegocioSeccion getPorProductoNegocioTipoPolizaSeccionDescripcion(
			ProductoDTO producto, Negocio negocio, TipoPolizaDTO tipoPoliza, SeccionDTO seccion);
	
	public List<NegocioSeccion> listarNegSeccionByIdNegocio(Long idToNegocio);

}
