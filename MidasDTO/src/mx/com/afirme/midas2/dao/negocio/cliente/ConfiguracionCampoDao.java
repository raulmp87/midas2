package mx.com.afirme.midas2.dao.negocio.cliente;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.negocio.cliente.ConfiguracionCampo;

@Local
public interface ConfiguracionCampoDao extends Dao<Long, ConfiguracionCampo>{

}
