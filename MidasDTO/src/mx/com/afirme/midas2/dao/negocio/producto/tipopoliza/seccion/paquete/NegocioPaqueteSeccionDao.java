package mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.paquete;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;

@Local
public interface NegocioPaqueteSeccionDao extends Dao<Long, NegocioPaqueteSeccion> {
	public List<NegocioCobPaqSeccion> listaCoberturasPorPaqueteSeccion(
			BigDecimal idToSeccion, Long idPaquete);
	
	public List<NegocioPaqueteSeccion> listarPaquetesSecccionPorCotizacionInciso(BigDecimal idToCotizacion, BigDecimal idNegocioSeccion);
	
	public NegocioPaqueteSeccion getPorIdNegSeccionIdPaquete(BigDecimal idToNegSeccion, Long id);
	
	public NegocioPaqueteSeccion findByNegocioProductoTipoPolizaSeccionPaquete(
			Long idToNegocio, BigDecimal idToProducto, BigDecimal idToTipoPoliza,
			BigDecimal idToSeccion, Long idPaquete);
	
	public NegocioPaqueteSeccion findByNegocioSeccionAndPaqueteDescripcion(NegocioSeccion negocioSeccion, String paqueteDescripcion);
	
}
