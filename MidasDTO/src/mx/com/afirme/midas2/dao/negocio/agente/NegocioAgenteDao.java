package mx.com.afirme.midas2.dao.negocio.agente;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.agente.NegocioAgente;

@Local
public interface NegocioAgenteDao extends Dao<BigDecimal, NegocioAgente> {
	
	public List<NegocioAgente> listarAgentesAsociados(Long idToNegocio);
	
	public List<Negocio> listarNegociosPorAgente(Integer idToAgente);
	//Comentario integracion
	public List<Negocio> listarNegociosPorAgente(Integer idToAgente, String cveTipoNegocio, Integer status, Boolean esExterno);

	public List<Negocio> listarNegociosPorAgenteClaveNegocio(Integer idTcAgente, String claveNegocio);
	
	/**
	 * Obtiene el total de agentes que tiene asignado el <code>Negocio</code>.
	 * @param negocio
	 * @return
	 */
	public long getTotalNegocioAgente(Negocio negocio);
	
	
	/**
	 * Obtiene el <code>NegocioAgente</code> usando la llave de negocio.
	 * @param negocio
	 * @param agente
	 * @return
	 */
	public NegocioAgente findByNegocioAndAgente(Negocio negocio, Agente agente);
	
}
