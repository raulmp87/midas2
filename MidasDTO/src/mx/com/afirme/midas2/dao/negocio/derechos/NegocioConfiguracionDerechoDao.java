package mx.com.afirme.midas2.dao.negocio.derechos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.negocio.derechos.NegocioConfiguracionDerecho;
import mx.com.afirme.midas2.dto.negocio.derechos.NegocioConfiguracionDerechoDTO;

@Local
public interface NegocioConfiguracionDerechoDao {
	
	public List<NegocioConfiguracionDerechoDTO> buscarConfiguracionesDerecho(NegocioConfiguracionDerecho	filtroConfigDerecho);
	
	public Long conteoBusquedaConfiguracionesDerecho(NegocioConfiguracionDerecho negocioConfiguracionDerecho);
	
}
