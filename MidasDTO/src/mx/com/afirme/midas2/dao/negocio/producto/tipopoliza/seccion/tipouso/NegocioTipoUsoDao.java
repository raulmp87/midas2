package mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.tipouso;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUso;

@Local
public interface NegocioTipoUsoDao extends Dao<BigDecimal, NegocioTipoUso> {

	public NegocioTipoUso buscarDefault(NegocioSeccion negocioSeccion);
	
	public List<TipoUsoVehiculoDTO> getTipoUsoVehiculoDTOByNegSeccionEstiloVehiculo(BigDecimal idToNegSeccion, BigDecimal idTcTipoVehiculo);
	
	public NegocioTipoUso findByNegocioSeccionAndTipoUsoDescripcion(NegocioSeccion negocioSeccion, String tipoUsoDescripcion);
	
}
