package mx.com.afirme.midas2.dao.negocio.producto;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;

@Local
public interface NegocioProductoDao extends Dao<Long, NegocioProducto> {

	public List<NegocioTipoPoliza> listarNegTipoPolizaPorIdToNegProducto(Long idToNegProducto);
	
	public List<NegocioProducto> listarNegocioProductoPorNegocio(Long idToNegocio, Integer activo);
	
	public NegocioProducto getNegocioProductoByIdNegocioIdProducto(Long idToNegocio, BigDecimal idToProducto);
	
	public List<NegocioProducto> listarNegocioProductoUsablesPorNegocio(Long idToNegocio);
}
