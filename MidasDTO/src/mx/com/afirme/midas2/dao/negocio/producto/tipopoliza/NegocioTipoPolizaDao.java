package mx.com.afirme.midas2.dao.negocio.producto.tipopoliza;

import java.math.BigDecimal;

import javax.ejb.Remote;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;


public interface NegocioTipoPolizaDao extends Dao<Long, NegocioTipoPoliza> {

	NegocioTipoPoliza getPorIdNegocioProductoIdToTipoPoliza(Long idToNegProducto, BigDecimal idToTipoPoliza);
	
	NegocioTipoPoliza getPorIdNegocioProductoIdToNegTipoPoliza(Long idToNegProducto, BigDecimal idToNegTipoPoliza);
}
