package mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.paquete.cobertura;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;

@Local
public interface NegocioCobPaqSeccionDAO {

	public List<NegocioCobPaqSeccion> getNegocioCobPaqSeccionAsociados(
			NegocioCobPaqSeccion negocioCobPaqSeccion);

	public List<NegocioCobPaqSeccion> getCoberturas(Long IdNegocio,
			BigDecimal idToProducto, BigDecimal idTipoPoliza,
			BigDecimal idToSeccion, Long idPaquete, String idEstado,
			String idMunicipio, Short idMoneda);
	
	public NegocioCobPaqSeccion getLimitesSumaAseguradaPorCobertura(BigDecimal idToCobertura,String stateId,String cityId, BigDecimal idTcMoneda,Long idToNegPaqueteSeccion);

	public List<NegocioCobPaqSeccion> getNegocioCobPaqSeccionAsociados(BigDecimal idToCobertura,String stateId,String cityId, Short idTcMoneda,Long idToNegPaqueteSeccion, BigDecimal IdTcTipoUsoVehiculo, Long agenteId, Boolean esRenovacion);
}
