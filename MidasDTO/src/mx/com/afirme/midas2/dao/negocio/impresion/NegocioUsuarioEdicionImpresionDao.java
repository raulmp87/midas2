package mx.com.afirme.midas2.dao.negocio.impresion;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.negocio.impresion.NegocioUsuarioEdicionImpresion;

@Local
public interface NegocioUsuarioEdicionImpresionDao {

	public List<NegocioUsuarioEdicionImpresion> obtenerUsuariosDisponibles(
			Long idToNegocio);
	
}
