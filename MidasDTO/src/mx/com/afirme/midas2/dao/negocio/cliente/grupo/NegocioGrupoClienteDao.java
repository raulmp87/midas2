package mx.com.afirme.midas2.dao.negocio.cliente.grupo;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.negocio.cliente.grupo.NegocioGrupoCliente;

@Local
public interface NegocioGrupoClienteDao extends Dao<Long, NegocioGrupoCliente> {

}
