package mx.com.afirme.midas2.dao.negocio.emailsapamis;

import java.util.List;

import mx.com.afirme.midas2.dto.negocio.emailsapamis.EmailNegocioConfig;

public interface EmailNegocioLogDao{
	public List<EmailNegocioConfig> findAll();
	public EmailNegocioConfig findById(Long id);
	public EmailNegocioConfig findByDesc(String desc);
	public List<EmailNegocioConfig> findByStatus(boolean status);
	public List<EmailNegocioConfig> findByProperty(String propertyName, Object property);
	public EmailNegocioConfig saveObject(EmailNegocioConfig object);
}
