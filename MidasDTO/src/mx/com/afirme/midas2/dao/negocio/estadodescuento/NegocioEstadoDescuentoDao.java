package mx.com.afirme.midas2.dao.negocio.estadodescuento;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.negocio.estadodescuento.NegocioEstadoDescuento;

@Local
public interface NegocioEstadoDescuentoDao extends EntidadDao{
	public List<NegocioEstadoDescuento> obtenerEstadosPorNegocioId(Long idToNegocio);
	
	public NegocioEstadoDescuento findByNegocioAndEstado(Long idToNegocio, String idToEstado);
	
	public List<NegocioEstadoDescuento> findByNegocioAndEstadosAsociados(Long idToNegocio);
	
	public String obtenerZipCode (Long idToNegocio, String stateId);
}
