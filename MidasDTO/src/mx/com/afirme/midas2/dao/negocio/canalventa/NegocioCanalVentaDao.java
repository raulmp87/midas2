package mx.com.afirme.midas2.dao.negocio.canalventa;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;

@Local
public interface NegocioCanalVentaDao {
	
	public int sincronizarCamposCatCanalVta(TIPO_CATALOGO tipo, Long idNegocio, String usuarioCreacion, String usuarioModificacion);

	public int saveAllCamposNegocio(Boolean activo, Long idNegocio);

	public int saveSectionCamposNegocio(Boolean visible, Long idNegocio, String section);

}
