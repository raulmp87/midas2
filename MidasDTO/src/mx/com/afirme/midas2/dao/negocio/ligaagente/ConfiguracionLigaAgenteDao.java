package mx.com.afirme.midas2.dao.negocio.ligaagente;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.negocio.ligaagente.ConfigLigaAgenteDTO;
import mx.com.afirme.midas2.dto.negocio.ligaagente.MonitoreoLigaAgenteDTO;

@Local
public interface ConfiguracionLigaAgenteDao {

	/**
	 * busca las Configuraciones de Ligas de Agente que cumplan con las condiciones que se establecen en el filtro que recibe
	 * para la pantalla del listado de configuraciones de ligas de agente
	 * @param cfgLigaFiltro
	 * @return
	 */
	public List<ConfigLigaAgenteDTO> buscarConfiguracionesLigasAgentes(ConfigLigaAgenteDTO cfgLigaFiltro);
	
	/**
	 * busca las Configuraciones de Ligas de Agente que cumplan con las condiciones que se establecen en el filtro que recibe
	 * para la pantalla del listado de monitoreo de ligas de agente
	 * @param cfgLigaFiltro
	 * @return
	 */
	public List<MonitoreoLigaAgenteDTO> buscarMonitoreoLigasAgentes(MonitoreoLigaAgenteDTO monitorLigaFiltro);
	
	/**
	 * busca las Configuraciones de Ligas de Agente que cumplan con las condiciones que se establecen en el filtro que recibe
	 * para la pantalla del listado de monitoreo de ligas de agente
	 * @param cfgLigaFiltro
	 * @return
	 */
	public List<MonitoreoLigaAgenteDTO> buscarMonitoreoLigasAgentesPaginado(MonitoreoLigaAgenteDTO monitorLigaFiltro);
	
	/**
	 * cuenta el numero total de registros y el numero de polizas de las cotizaciones para el filtro que se le pasa como parametro. 
	 * @param monitorLigaFiltro
	 * @return
	 */
	public MonitoreoLigaAgenteDTO buscarMonitoreoLigasAgentesContador(MonitoreoLigaAgenteDTO monitorLigaFiltro);
	
}