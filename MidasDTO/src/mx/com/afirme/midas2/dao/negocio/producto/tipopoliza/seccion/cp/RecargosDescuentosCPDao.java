package mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.cp;

import java.math.BigDecimal;

import javax.ejb.Local;

import mx.com.afirme.midas.catalogos.codigo.postal.RecargosDescuentosCPDTO;
import mx.com.afirme.midas2.dao.Dao;

@Local
public interface RecargosDescuentosCPDao extends Dao<BigDecimal, RecargosDescuentosCPDTO> {

}