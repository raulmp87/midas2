package mx.com.afirme.midas2.dao.negocio.cliente;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.negocio.cliente.ConfiguracionGrupoPorNegocio;

@Local
public interface ConfiguracionGrupoPorNegocioDao extends Dao<Long, ConfiguracionGrupoPorNegocio>{

}
