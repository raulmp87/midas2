package mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.tiposervicio;

import java.math.BigDecimal;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tiposervicio.NegocioTipoServicio;

@Local
public interface NegocioTipoServicioDao extends Dao<BigDecimal, NegocioTipoServicio> {

	public NegocioTipoServicio buscarDefault(NegocioSeccion negocioSeccion);
	
	public NegocioTipoServicio getDefaultByNegSeccionEstiloVehiculo(
			BigDecimal idToNegSeccion, BigDecimal idTcTipoVehiculo);
}
