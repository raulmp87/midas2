package mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.documentos.anexos;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="TODOCUMENTOANEXOSECCION", schema="MIDAS")
public class DocumentoAnexoSeccion  implements java.io.Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public DocumentoAnexoSeccion(){
		
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTODOCUMENTOANEXOSECCION_ID_GENERATOR")	
	@SequenceGenerator(name="IDTODOCUMENTOANEXOSECCION_ID_GENERATOR", sequenceName="MIDAS.IDTODOCUMENTOANEXOSECCION_SEQ", allocationSize=1)
	@Column(name="TODOCUMENTOANEXOSECCIONID")
	private Long id;
	@Column(name="CLAVEOBLIGATORIEDAD")
	private Short claveObligatoriedad;
	@Column(name="NUMEROSECUENCIA")
	private Long numeroSecuencia;
	@Column(name="IDTOCONTROLARCHIVO")
	private Long idControlArchivo;
	@Column(name="DESCRIPCION")
	private String descripcion;
	@Temporal(TemporalType.DATE)
	@Column(name="FECHACREACION")
	private Date fechaCreacion;
	@Column(name="USUARIOID")
	private String usuarioId;
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAMODIFICACION")
	private Date fechaModificacion;
	@Column(name="USUARIOMODIFICACIONID")
	private String usuarioModificacionId;
	@Column(name="NOMBRE")
	private String nombre;
	@Column(name="FMX_ID")
	private String fmxId;
	@Column(name="FMX_NOMBRE")
	private String fmxNombre;
	@Column(name="FMX_GAVETA")
	private String fmxGaveta;
	@Column(name="FMX_CARPETA")
	private String fmxCarpeta;
	@Column(name="IDTOSECCION")
	private Long idToSeccion;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "IDTOSECCION", nullable = false, insertable = false, updatable = false)
	private SeccionDTO seccionDTO;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Short getClaveObligatoriedad() {
		return claveObligatoriedad;
	}

	public void setClaveObligatoriedad(Short claveObligatoriedad) {
		this.claveObligatoriedad = claveObligatoriedad;
	}

	public Long getNumeroSecuencia() {
		return numeroSecuencia;
	}

	public void setNumeroSecuencia(Long numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}

	public Long getIdControlArchivo() {
		return idControlArchivo;
	}

	public void setIdControlArchivo(Long idControlArchivo) {
		this.idControlArchivo = idControlArchivo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(String usuarioId) {
		this.usuarioId = usuarioId;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getUsuarioModificacionId() {
		return usuarioModificacionId;
	}

	public void setUsuarioModificacionId(String usuarioModificacionId) {
		this.usuarioModificacionId = usuarioModificacionId;
	}	

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getFmxId() {
		return fmxId;
	}

	public void setFmxId(String fmxId) {
		this.fmxId = fmxId;
	}

	public String getFmxNombre() {
		return fmxNombre;
	}

	public void setFmxNombre(String fmxNombre) {
		this.fmxNombre = fmxNombre;
	}

	public String getFmxGaveta() {
		return fmxGaveta;
	}

	public void setFmxGaveta(String fmxGaveta) {
		this.fmxGaveta = fmxGaveta;
	}

	public String getFmxCarpeta() {
		return fmxCarpeta;
	}

	public void setFmxCarpeta(String fmxCarpeta) {
		this.fmxCarpeta = fmxCarpeta;
	}

	public Long getIdToSeccion() {
		return idToSeccion;
	}

	public void setIdToSeccion(Long idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	public SeccionDTO getSeccionDTO() {
		return seccionDTO;
	}

	public void setSeccionDTO(SeccionDTO seccionDTO) {
		this.seccionDTO = seccionDTO;
	}

	@Override
	public <K> K getKey() {
		return null;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}

}
