package mx.com.afirme.midas2.dao.negocio.cliente;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.negocio.cliente.ConfiguracionSeccionPorNegocio;

@Local
public interface ConfiguracionSeccionPorNegocioDao extends Dao<Long, ConfiguracionSeccionPorNegocio>{

	List<ConfiguracionSeccionPorNegocio> findByNegocio(Long idNegocio);
}
