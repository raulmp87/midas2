package mx.com.afirme.midas2.dao.negocio.bonocomision;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.bonocomision.NegocioBonoComision;

public interface NegocioBonoComisionDao  extends Dao<Long, NegocioBonoComision> {

	public NegocioBonoComision findByNegocio(Negocio negocio);
	
}
