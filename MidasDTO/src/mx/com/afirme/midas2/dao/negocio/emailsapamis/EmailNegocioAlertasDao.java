package mx.com.afirme.midas2.dao.negocio.emailsapamis;

import java.util.List;

import mx.com.afirme.midas2.dto.negocio.emailsapamis.EmailNegocioAlertas;

public interface EmailNegocioAlertasDao{
	public List<EmailNegocioAlertas> findAll();
	public EmailNegocioAlertas findById(Long id);
	public EmailNegocioAlertas findByDesc(String desc);
	public List<EmailNegocioAlertas> findByStatus(boolean status);
	public List<EmailNegocioAlertas> findByProperty(String propertyName, Object property);
	public EmailNegocioAlertas saveObject(EmailNegocioAlertas object);
}