package mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.estilovehiculo;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.estilovehiculo.NegocioEstiloVehiculo;

@Local
public interface NegocioEstiloVehiculoDao extends
		Dao<Long, NegocioEstiloVehiculo> {
	public List<NegocioEstiloVehiculo> getEstilosVehiculo(
			BigDecimal idToNegSeccion);

	public List<NegocioEstiloVehiculo> getEstilosVehiculo(
			BigDecimal idToNegSeccion, BigDecimal idTcMarcaVehiculo,
			BigDecimal tipoVehiculo);

}
