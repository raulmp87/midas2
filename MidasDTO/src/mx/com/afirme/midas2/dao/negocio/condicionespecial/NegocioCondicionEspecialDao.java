package mx.com.afirme.midas2.dao.negocio.condicionespecial;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial.NivelAplicacion;
import mx.com.afirme.midas2.domain.negocio.condicionespecial.NegocioCondicionEspecial;

@Local
public interface NegocioCondicionEspecialDao extends Dao<Long, NegocioCondicionEspecial>{

	
	/**
	 * MSN - Obtener todas las condiciones especiales ligadas al negocio que sean distintas 
	 * @param idToNegocio
	 * @return List<CondicionEspecial>
	 */
	public List<CondicionEspecial> listarCondicionEspecialByIdNegocio(Long idToNegocio);
	
	/**
	 * MSN - Obtener todas las condiciones especiales del negocio
	 * @param idToNegocio
	 * @param nivelAplicacion
	 * @return List<NegocioCondicionEspecial>
	 */
	public List<NegocioCondicionEspecial> obtenerCondicionesNegocio(Long idToNegocio,NivelAplicacion nivelAplicacion);
	
	/**
	 * MSN - Obtener todas las condicones especiales del negocio.
	 * @param idToNegocio
	 * @param nivelAplicacion
	 * @param obligatoriedad Si es nulo no se toma en cuenta.
	 * @return
	 */
	public List<NegocioCondicionEspecial> obtenerCondicionesNegocio(Long idToNegocio, NivelAplicacion nivelAplicacion, Boolean obligatoriedad);
	
	/**
	 * Lista las condiciones especiales que NO esten en el negocio, en base al id de negocio y al id de seccion 
	 * @param idToNegocio
	 * @param idToSeccion
	 * @return
	 */
	public List<CondicionEspecial> obtenerCondicionesEspecialesDisponibles(	BigDecimal idToNegocio, BigDecimal idToSeccion);
	
	/**
	 * Lista las condiciones especiales que NO esten en el negocio, en base al id de negocio
	 * @param idToNegocio
	 * @return
	 */
	public List<CondicionEspecial> obtenerCondicionesEspecialesDisponibles(BigDecimal idToNegocio);
	
	
	public List<CondicionEspecial> buscarCondicionCodigoNombre(Long idToNegocio, String param, NivelAplicacion nivel);
	
}
