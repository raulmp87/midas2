package mx.com.afirme.midas2.dao.negocio.recuotificacion;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacionUsuario;

@Local
public interface NegocioRecuotificacionDao extends Serializable {

	public List<NegocioRecuotificacionUsuario> obtenerUsuariosDisponibles(
			Long idToNegocio);
	
	public List<NegocioRecuotificacionUsuario> obtenerUsuariosAsociados(
			Long idToNegocio);
	
}
