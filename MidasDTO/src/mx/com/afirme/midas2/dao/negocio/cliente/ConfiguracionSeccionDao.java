package mx.com.afirme.midas2.dao.negocio.cliente;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.negocio.cliente.ConfiguracionSeccion;

@Local
public interface ConfiguracionSeccionDao extends Dao<Long, ConfiguracionSeccion>{
	
}
