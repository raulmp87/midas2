package mx.com.afirme.midas2.dao.negocio;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.agente.NegocioAgente;
@Local
public interface NegocioDao extends Dao<Long, Negocio>{

	/**
	 * Obtiene el listado de negocios por los filtros de clave negocio y estatus negocio.
	 */
	public List<Negocio> findByFilters(Negocio filtroNegocio);
	public List<Negocio> listarNegociosNoBorrados(String cveNegocio);
	public List<Negocio> listRelated(Long id);
	public List<NegocioAgente> listarNegocioAgentes(Long idToNegocio);
	public List<Negocio> listarNegocioSinAgentes(String claveNegocio, Boolean esExterno);
	public List<Negocio> listarNegociosPorDescripcion(String descripcion, Boolean esExterno);
	//Comentario integracion
	public List<Negocio> listarNegocioAplicaUsuario(String claveNegocio, boolean isOutsider);
}
