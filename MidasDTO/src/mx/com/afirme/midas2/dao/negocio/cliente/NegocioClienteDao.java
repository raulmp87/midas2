package mx.com.afirme.midas2.dao.negocio.cliente;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.negocio.cliente.NegocioCliente;

@Local
public interface NegocioClienteDao extends Dao<Long, NegocioCliente>{

}
