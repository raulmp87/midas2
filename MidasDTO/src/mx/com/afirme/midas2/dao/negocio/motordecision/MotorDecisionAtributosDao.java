package mx.com.afirme.midas2.dao.negocio.motordecision;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.negocio.motordecision.MotorDecisionDTO;
import mx.com.afirme.midas2.dto.negocio.motordecision.ResultadoMotorDecisionAtributosDTO;

@Local
public interface MotorDecisionAtributosDao {

	public List<ResultadoMotorDecisionAtributosDTO> consultaValoresDao(MotorDecisionDTO.TipoConsulta tipoConsulta, Long idToNegocio, List<MotorDecisionDTO.FiltrosMotorDecisionDTO> filtrosReqList, List<MotorDecisionDTO.FiltrosMotorDecisionDTO> filtrosNoReqList);
}
