package mx.com.afirme.midas2.dao.negocio.cliente;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.negocio.cliente.ClienteAsociadoJPA;

@Local
public interface ClienteAsociadoJPADao extends Dao<Long, ClienteAsociadoJPA>{

}
