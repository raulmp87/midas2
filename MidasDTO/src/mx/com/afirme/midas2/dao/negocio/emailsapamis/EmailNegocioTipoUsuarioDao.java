package mx.com.afirme.midas2.dao.negocio.emailsapamis;

import java.util.List;

import mx.com.afirme.midas2.dto.negocio.emailsapamis.EmailNegocioTipoUsuario;

public interface EmailNegocioTipoUsuarioDao{
	public List<EmailNegocioTipoUsuario> findAll();
	public EmailNegocioTipoUsuario findById(Long id);
	public EmailNegocioTipoUsuario findByDesc(String desc);
	public List<EmailNegocioTipoUsuario> findByStatus(boolean status);
	public List<EmailNegocioTipoUsuario> findByProperty(String propertyName, Object property);
	public EmailNegocioTipoUsuario saveObject(EmailNegocioTipoUsuario object);
}
