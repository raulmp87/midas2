package mx.com.afirme.midas2.dao.tarifa;

import java.util.List;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersionId;

public interface TarifaDao<K,E> extends Dao<K, E>{
	/**
	 * Metodo que regresar� una lista de objetos de la entidad por medio de tarifa version id
	 * @param tarifaVersionId
	 * @return
	 */
	public List<E> findByTarifaVersionId(TarifaVersionId tarifaVersionId);
}
