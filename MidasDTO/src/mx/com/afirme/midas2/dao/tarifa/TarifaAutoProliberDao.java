package mx.com.afirme.midas2.dao.tarifa;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.tarifa.TarifaAutoProliber;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoProliberId;

@Local
public interface TarifaAutoProliberDao extends TarifaDao<TarifaAutoProliberId, TarifaAutoProliber> {
	
	public List<TarifaAutoProliber> findByFilters(TarifaAutoProliber tarifaAutoProliber);

}
