package mx.com.afirme.midas2.dao.tarifa;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifa;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifaSeccion;
import mx.com.afirme.midas2.domain.tarifa.TarifaAgrupadorTarifa;
import mx.com.afirme.midas2.domain.tarifa.TarifaAgrupadorTarifaId;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersion;

@Local
public interface TarifaAgrupadorTarifaDao extends Dao<TarifaAgrupadorTarifaId, TarifaAgrupadorTarifa> {
	
	public List<AgrupadorTarifa> getListAgrupadorTarifaPorNegocio(String claveNegocio);
	
	public List<AgrupadorTarifaSeccion> getListLineaNegocioPorAgrupadorTarifa(String idToAgrupadorTarifaFromString);
	
	public List<AgrupadorTarifaSeccion> getListMomendaPorAgrupadorTarifaLineaNegocio(String idToAgrupadorTarifaFromString, BigDecimal idToSeccion);
	
	public List<TarifaVersion> getTarifaVersionPorLineaNegocio(String claveNegocio);
	
	public List<TarifaAgrupadorTarifa> getTarifaAgrupadorTarifaPorAgrupadorTarifaLineaNegocio(TarifaAgrupadorTarifa tarifaAgrupadorTarifa, String claveNegocio);
	
	public List<MonedaDTO> getListMonedaPorAgrupadorTarifa(String idToAgrupadorTarifaFromString);
}
