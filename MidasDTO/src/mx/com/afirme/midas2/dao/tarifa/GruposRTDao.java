package mx.com.afirme.midas2.dao.tarifa;

import javax.ejb.Local;

import mx.com.afirme.midas.base.MidasInterfaceBase;
import mx.com.afirme.midas2.dto.ClaveGrupoComboDTO;

@Local
public interface GruposRTDao extends MidasInterfaceBase<ClaveGrupoComboDTO> {

}
