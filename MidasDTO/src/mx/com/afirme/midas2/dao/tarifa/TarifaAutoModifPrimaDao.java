package mx.com.afirme.midas2.dao.tarifa;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.tarifa.TarifaAutoModifPrima;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoModifPrimaId;
/**
 * Interface con los metodos que tendrá nuestro servicio de catalogo de Tarifa Auto Modif Prima
 * mas los que debe de tener por default el catalogo, que son los que hereda de la otra interface de CatalogoDao
 * @author vmhersil
 *
 */
@Local
public interface TarifaAutoModifPrimaDao extends TarifaDao<TarifaAutoModifPrimaId,TarifaAutoModifPrima>{
	public List<TarifaAutoModifPrima> findByFilters(TarifaAutoModifPrima filtroTarifaAutoModifPrima);
}
