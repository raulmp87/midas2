package mx.com.afirme.midas2.dao.tarifa;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.tarifa.TarifaAuto;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoId;

@Local
public interface TarifaAutoDao extends TarifaDao<TarifaAutoId, TarifaAuto> {

}
