package mx.com.afirme.midas2.dao.tarifa;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifa;

@Local
public interface AgrupadorTarifaDao extends EntidadDao{
	public List<AgrupadorTarifa> findByFilters(
			AgrupadorTarifa filtroAgrupadorTarifa);

	public Long createNewVersion(BigDecimal id,BigDecimal version);
	
	public List<AgrupadorTarifa> findByNegocio(String claveNegocio, String filtro);
	
	public AgrupadorTarifa consultaVersionActiva(BigDecimal id);
	
	public AgrupadorTarifa consultaVersionActiva(Long id);
}
