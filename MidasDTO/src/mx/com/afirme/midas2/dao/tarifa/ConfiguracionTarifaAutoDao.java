package mx.com.afirme.midas2.dao.tarifa;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.tarifa.ConfiguracionTarifaAuto;
import mx.com.afirme.midas2.domain.tarifa.ConfiguracionTarifaAutoId;
import mx.com.afirme.midas2.domain.tarifa.TarifaAuto;

@Local
public interface ConfiguracionTarifaAutoDao extends Dao<ConfiguracionTarifaAutoId, ConfiguracionTarifaAuto> {

	/**
	 * Consulta la lista de configuraciones correspondientes a la tarifa recibida.
	 * La lista generada se ordena por idDato, por lo que el primer elemento corresponde al IDBase1,
	 * el segundo al IDBase2, y así sucesivamente.
	 * @param tarifaAuto
	 * @return
	 */
	public List<ConfiguracionTarifaAuto> getConfiguracionTarifaAuto(TarifaAuto tarifaAuto);
}
