package mx.com.afirme.midas2.dao.tarifa;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersion;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersionId;

@Local
public interface TarifaVersionDao extends Dao<TarifaVersionId, TarifaVersion> {

	public List<Long> cargarVersiones(TarifaVersionId id);
	
	public Long generarNuevaVersion(TarifaVersionId id, String negocio, Short tipoTarifa);
	
	public TarifaVersion cargarActivo(TarifaVersionId id);
	
	public List<Long> cargarMonedas(TarifaVersionId id);
	
	public List<TarifaVersion> cargarTarifas(TarifaVersionId id);
	
}
