package mx.com.afirme.midas2.dao.tarifa;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.tarifa.TarifaAutoRangoSA;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoRangoSAId;

@Local
public interface TarifaAutoRangoSADao extends TarifaDao<TarifaAutoRangoSAId, TarifaAutoRangoSA>{

	public List<TarifaAutoRangoSA> findByFilters(TarifaAutoRangoSA tarifaAutoRangoSA);
}
