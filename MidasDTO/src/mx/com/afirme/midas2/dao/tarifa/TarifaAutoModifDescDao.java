package mx.com.afirme.midas2.dao.tarifa;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.tarifa.TarifaAutoModifDesc;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoModifDescId;

@Local
public interface TarifaAutoModifDescDao extends TarifaDao<TarifaAutoModifDescId, TarifaAutoModifDesc>{
	
	public List<TarifaAutoModifDesc> findByFilters(TarifaAutoModifDesc filter);

}
