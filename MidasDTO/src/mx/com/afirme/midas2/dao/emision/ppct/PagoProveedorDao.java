package mx.com.afirme.midas2.dao.emision.ppct;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.contabilidad.SolicitudCheque;
import mx.com.afirme.midas2.domain.emision.ppct.CoberturaSeycos;
import mx.com.afirme.midas2.domain.emision.ppct.OrdenPago;
import mx.com.afirme.midas2.domain.emision.ppct.Proveedor;
import mx.com.afirme.midas2.domain.emision.ppct.Recibo;
import mx.com.afirme.midas2.domain.emision.ppct.ReciboProveedor;
import mx.com.afirme.midas2.dto.emision.ppct.SaldoDTO;

@Local
public interface PagoProveedorDao {

	
	BigDecimal obtenerImportePreeliminar(String sessionId);
	
	OrdenPago guardarOrdenPago (OrdenPago ordenPago);
	
	BigDecimal asignarRecibosPreeliminares(Proveedor proveedor, Date fechaCorte, String sessionId);
	
	void desasignarRecibosPreeliminares(String sessionId);
	
	void asignarRecibos(OrdenPago ordenPago, String sessionId);
	
	SolicitudCheque obtenerSolicitudCheque(OrdenPago ordenPago);
	
	OrdenPago obtenerOrdenPago(OrdenPago filtro);
	
	List<CoberturaSeycos> obtenerCoberturasProveedor(Proveedor proveedor, String claveNegocio);
	
	List<Recibo> obtenerRecibosPreeliminares(ReciboProveedor filtro);
	
	List<Recibo> obtenerRecibosOrdenPago(ReciboProveedor filtro);
	
	List<OrdenPago> obtenerOrdenesPago(OrdenPago filtro);
	
	List<SaldoDTO> obtenerDetalleReporteSaldos(SaldoDTO filtro);
	
	List<SaldoDTO> obtenerResumenReporteSaldos(SaldoDTO filtro);
	
}
