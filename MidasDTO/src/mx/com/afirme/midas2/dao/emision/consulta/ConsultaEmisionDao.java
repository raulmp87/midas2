package mx.com.afirme.midas2.dao.emision.consulta;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.emision.consulta.Consulta;
import mx.com.afirme.midas2.dto.emision.consulta.ConsultaAnexo;
import mx.com.afirme.midas2.dto.emision.consulta.ConsultaCobertura;
import mx.com.afirme.midas2.dto.emision.consulta.ConsultaCobranza;
import mx.com.afirme.midas2.dto.emision.consulta.ConsultaEndoso;

@Local
public interface ConsultaEmisionDao {

	public List<Consulta> getConsultaEmision(Consulta filtro);
		
	public List<Consulta> getConsultaCobranza(ConsultaCobranza filtro);
	
	public List<Consulta> getConsultaCobertura(ConsultaCobertura filtro);
	
	public List<Consulta> getConsultaEndoso(ConsultaEndoso filtro);
	
	public List<Consulta> getConsultaAnexo(ConsultaAnexo filtro);
	
}
