package mx.com.afirme.midas2.dao.movil.cliente;

import java.util.List;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.movil.cliente.PolizaRenovadaEnvioDocMovil;

public interface PolizaRenovadaEnvioDocMovilDao extends EntidadDao {

	public void save(PolizaRenovadaEnvioDocMovil entity);

	public void delete(PolizaRenovadaEnvioDocMovil entity);

	public PolizaRenovadaEnvioDocMovil update(PolizaRenovadaEnvioDocMovil entity);

	public PolizaRenovadaEnvioDocMovil findById(Long id);

	public List<PolizaRenovadaEnvioDocMovil> findByProperty(String propertyName,
			Object value);
	
	public List<PolizaRenovadaEnvioDocMovil> findAll(); 
}