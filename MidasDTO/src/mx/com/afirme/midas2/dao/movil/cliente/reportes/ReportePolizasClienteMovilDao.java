package mx.com.afirme.midas2.dao.movil.cliente.reportes;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.producto.NegocioSeguros;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.movil.cliente.PolizasClienteMovil;

@Local
public interface ReportePolizasClienteMovilDao  extends EntidadDao {

	public List<PolizasClienteMovil> getReportePolizasClienteMovil(String TipoPoliza);
	
	public List<NegocioSeguros> getTipoPolizas();
	
}
