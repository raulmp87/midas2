package mx.com.afirme.midas2.dao.juridico;

import javax.ejb.Local;


@Local
public interface JuridicoDao {

	/**
	 * Obtiene el ultimo id de los oficios de una reclamacion 
	 * @param idReclamacion
	 * @return
	 */
	public Integer obtenerSecuenciaOficioJuridico(Long idReclamacion);
	
}
