package mx.com.afirme.midas2.dao.juridico;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.juridico.CitaJuridico;
import mx.com.afirme.midas2.service.juridico.CitaJuridicoService.CitaJuridicoFiltro;

@Local
public interface CitaJuridicoDao extends EntidadDao{
	
	public List<CitaJuridico> filtrar(CitaJuridicoFiltro filtro);
	
	public Long filtrarPaginado(CitaJuridicoFiltro filtro);	
}
