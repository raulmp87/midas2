package mx.com.afirme.midas2.dao.fuerzaventa;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.GrupoCatalogoAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
/**
 * Interface para consultar valores del catalogo.
 * @author vmhersil
 *
 */
@Local
public interface ValorCatalogoAgentesDao extends EntidadDao{
	/**
	 * Obtiene la lista de catalogos segun los filtros que se proporcionen
	 * @param filtro
	 * @return
	 * @throws Exception
	 */
	public List<ValorCatalogoAgentes> findByFilters(ValorCatalogoAgentes filtro) throws Exception;
	
	
	/**
	 * Obtiene la lista de catalogos segun los filtros que se proporcionen
	 * @param filtro
	 * @return
	 * @throws Exception
	 */
	public List<ValorCatalogoAgentes> findByFiltersLike(ValorCatalogoAgentes filtro) throws Exception;
	
	
	/**
	 * Obtiene la lista de elementos de un catalogo por su nombre
	 * @param nombreCatalogo es el nombre del grupo catalogo del cual se quieren obtener la lista de valores.
	 * @return
	 * @throws Exception
	 */
	public List<ValorCatalogoAgentes> obtenerElementosPorCatalogo(String nombreCatalogo) throws Exception;
	/**
	 * Obtiene la lista de elementos por un catalogo con opcion a excluir elementos
	 * @param nombreCatalogo
	 * @param excluirElementos
	 * @return
	 * @throws Exception
	 */
	public List<ValorCatalogoAgentes> obtenerElementosPorCatalogo(String nombreCatalogo,String... excluirElementos) throws Exception;
	
	/**
	 * Obtiene el elemento de un catalogo por su nombre de valor y del catalogo en el que se encuentra
	 * @param nombreCatalogo
	 * @param nombreElementoCatalogo
	 * @return
	 * @throws Exception
	 */
	public ValorCatalogoAgentes obtenerElementoEspecifico(String nombreCatalogo,String nombreElementoCatalogo) throws Exception;
	
	/**
	 * Obtiene el id de un elemento del catalogo especifico.
	 * @param nombreCatalogo
	 * @param nombreElementoCatalogo
	 * @return
	 * @throws Exception
	 */
	public Long obtenerIdElementEspecifico(String nombreCatalogo,String nombreElementoCatalogo) throws Exception;
	
	public ValorCatalogoAgentes loadById(ValorCatalogoAgentes valorCatalogoAgentes) throws Exception;
	
	public List<GrupoCatalogoAgente> catalogFindByFilters(GrupoCatalogoAgente filtro) throws Exception;
	
	public GrupoCatalogoAgente obtenerCatalogoPorDescripcion(String descripcion) throws Exception;
	
	public Long obtenerIdCatalogoPorDescripcion(String descripcion)throws Exception;
	
	public List<ValorCatalogoAgentes> findByTipoCedula(ValorCatalogoAgentes filtro) throws Exception;
	
	public String deleteTipoCedula(ValorCatalogoAgentes filtro) throws Exception;
	
	public Long guardarElementoCatalogoEnSeycos(ValorCatalogoAgentes elemento) throws Exception;
	
	public GrupoCatalogoAgente loadCatalogById(GrupoCatalogoAgente filtro) throws Exception;
	
	public Long eliminarElementoCatalogoEnSeycos(ValorCatalogoAgentes elemento) throws Exception;
	
	/**
	 * Obtiene la lista de elementos por un catalogo con opcion a obtener ciertos elementos
	 * @param nombreCatalogo
	 * @param elementosEspecificos
	 * @return
	 * @throws Exception
	 */
	public List<ValorCatalogoAgentes> obtenerElementosEspecificosPorCatalogo(String nombreCatalogo,String... elementosEspecificos) throws Exception;
}
