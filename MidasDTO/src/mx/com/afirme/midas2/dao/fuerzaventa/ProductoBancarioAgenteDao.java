package mx.com.afirme.midas2.dao.fuerzaventa;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProductoBancarioAgente;
@Local
public interface ProductoBancarioAgenteDao extends EntidadDao{
	
	public List<ProductoBancarioAgente> findByFilters(ProductoBancarioAgente filtro);
	
	public List<ProductoBancarioAgente> findByAgente(Long idAgente, String fechaHistorico);
	
	public void delete(Long idProducto) throws Exception;
	
	public List<ProductoBancarioAgente> findByFilters(ProductoBancarioAgente filtro, String fechaHistorico);
}
