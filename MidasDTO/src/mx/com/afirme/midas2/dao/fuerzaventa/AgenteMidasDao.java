package mx.com.afirme.midas2.dao.fuerzaventa;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.DocumentoAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.TipoDocumentoAgente;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.DomicilioView;
@Local
public interface AgenteMidasDao extends EntidadDao{
	//TODO Agregar metodos adicionales para agente si es necesario
	public List<Agente> findByFilters(Agente filters);
	
	public List<AgenteView> findByFiltersWithAddressSupport(Agente filtroAgente);
	
	public List<AgenteView> findByFiltersWithAddressSupportPaging(Agente filtroAgente,Integer pagingCount,Long pagingStart);
	
	public Agente saveFull(Agente agente) throws Exception; 
	
	public Agente saveDatosGeneralesAgente(Agente agente) throws Exception;
	
	public Agente saveDomiciliosAgente(Agente agente) throws Exception;
	
	public Agente saveDatosFiscalesAgente(Agente agente) throws Exception;
	
	public Agente saveDatosContablesAgente(Agente agente) throws Exception;
	
	public Agente unsuscribe(Agente agente,String accion) throws Exception;
	
	public List<Agente> findAgentesByPromotoria(Long idPromotoria)throws Exception;
	
	public List<AgenteView> agentesPorAutorizar(Agente filtroAgente) throws Exception;
	
	public Agente loadById(Agente agente);
	
	public List<TipoDocumentoAgente> findAllTypeDocumentsAgent();
	
	public List<DocumentoAgente> findAllDocumentsAgent(Long id);
	
	public boolean validarClaveAgente(Agente agente)throws Exception;
	
	public DocumentoAgente saveDocumentosAgente(DocumentoAgente documentoAgente);
	
	public boolean llenarYGuardarListaHijosAgente(String productosAgregados, Agente agenteInternet);
	
	public boolean llenarYGuardarListaEntretenimientosAgente(String entretenimientosAgregados, Agente agenteInternet);
	
	public void eliminarDocumentosAgente(Long idAgente);
	
	public void eliminarHijosAgente(Long idAgente);
	
	public void eliminarEntretenimientosAgente(Long idAgente);
	
	public Agente findByClaveAgente(Agente agente)throws Exception;
	
	public void disable(Agente agente) throws Exception;
	
	public void saveInSeycos(Agente agente)throws Exception;
	
	public List<TipoDocumentoAgente> findTypeDocuments(Long idAgente,String tipoDocumento);
	
	public String[] generateExpedientAgent(Long id) throws Exception;
	
	public String[] generateDocument(Long id,String tituloAplicacion,String documentName,String folderName) throws Exception;
	
	public String[] getDocumentFortimax(Long id,String tituloAplicacion) throws Exception;
	
	public String[] generateLinkToDocument(Long id) throws Exception;
	
	public Agente saveAltaPorInternet(Agente agente) throws Exception;
	
	public Long getMaxRowNumFindByFiltersWithAddressSupportPaging(Agente filtroAgente);
	
	public List<AgenteView> findByFilterLightWeight(Agente filtroAgente);
	
	public List<AgenteView> findByFilterLightWeight(Map<String, Object> filters);
	
	public List<String> cargaMasiva(String url) throws Exception;
	
	public Agente loadByClave(Agente agente);
	
	public Agente loadById(Agente agente, String fechaHistorico);
	
	public List<Agente> findByFilters(Agente filters, String fechaHistorico);
	
	public <E extends Entidad> List<E> findByPropertyWithHistorySupport(Class<E> entityClass, String propertyName,
			final Object value, String fechaHistorico);
	
	public Agente findByCodigoUsuario(String codigoUsuario);
	
	public DomicilioView findDomicilioAgente(Agente agente)throws Exception;
	
	public Agente loadByIdImpresiones(Agente agente);
	
	public List<Agente> findByFiltersImpresiones(Agente filtroAgente);
	
	public Agente loadByClaveSolicitudes(Agente agente);
	
	public List<Agente> findByFiltersSolicitudes(Agente filtroAgente);
	
	/*servicio para cambiar el estatus a rechazado a los agentes que esten rechazados con emision y sus dias para emitir hayan vencido*/
	public void rechazarAgentesVenceDiasEmision() throws Exception;
	/**
	 * Servicio utilizado para el web service autoplazo , en el que originacion envia la idsucursal y obtenemos el agente correspondiente
	 * 
	 * @param idSucursal
	 * @return Agente
	 */
	public Agente getAgenteByIdSucursal (Integer idSucursal);

	/**
	 * Servicio utilizado para ver si el agente es sucursal envia la idagente
	 * @param idAgente
	 * @return result es sucursal
	 */
	public boolean isSucursal (Integer idAgente);
	
	public void updateEstatusReporteDetPrimas(String estatus);
		
	/**
	 * Busca en el historial del agente si este estuvo alguna vez autorizado
	 * @param id Surrogate key del agente (id del sistema MIDAS)
	 * @return Numero de modificaciones del agente con estatus autorizado en su historial
	 */
	public Integer buscarAgenteAutorizadoEnHistorial(Long id);
	
	public Integer buscarIdRolAgenteMidas();
	
	public Integer buscarIdRolPromotorMidas();
	
	public Integer buscarIdRolAgenteED();
	
	public Integer buscarIdRolPromotorED();
		
}
