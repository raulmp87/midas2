package mx.com.afirme.midas2.dao.fuerzaventa;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Afianzadora;
@Local
public interface AfianzadoraDao extends EntidadDao{
	public Afianzadora saveFull(Afianzadora afianzadora) throws Exception;
	
	public void unsubscribe(Afianzadora afianzadora)throws Exception;
	
	public List<Afianzadora> findByFilters(Afianzadora filtroAfianzadora, String fechaHistorico);
	
	public List<Afianzadora> findByFiltersView(Afianzadora filtroAfianzadora);
	
	public Afianzadora loadById(Afianzadora afianzadora, String fechaHistorico);
}
