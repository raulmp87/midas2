package mx.com.afirme.midas2.dao.fuerzaventa;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.dto.fuerzaventa.PromotoriaView;
@Local
public interface PromotoriaJPADao extends EntidadDao{
	
	public List<Promotoria> findByFilters(Promotoria filtroPromotoria, String fechaHistorico);
	
	public List<Promotoria> findByEjecutivo(Long idEjecutivo);
	
	public Promotoria loadById(Promotoria promotoria, String fechaHistorico);
	
	public List<PromotoriaView> findByFiltersView(Promotoria filtroPromotoria);
	
	public List<PromotoriaView> getList(boolean onlyActive);
	
	public List<PromotoriaView> findByEjecutivoLightWeight(Long idParent);
	
	public List<PromotoriaView> findPromotoriaConEjecutivosExcluyentes(List<Long> promotorias,List<Long> ejecutivosExcluyentes,List<Long> gerenciasExcluyentes,List<Long> centrosExcluyentes);
	
	public int agentesAsignadosEnPromotoria(Promotoria promotoria);
	
}