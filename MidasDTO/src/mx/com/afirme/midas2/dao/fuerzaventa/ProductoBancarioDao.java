package mx.com.afirme.midas2.dao.fuerzaventa;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProductoBancario;
@Local
public interface ProductoBancarioDao extends EntidadDao{
	public List<ProductoBancario> findByFilters(ProductoBancario productoBancario);
	
	public List<ProductoBancario> findByBanco(Long idBanco);
}
