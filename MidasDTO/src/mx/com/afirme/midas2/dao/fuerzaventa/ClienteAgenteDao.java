package mx.com.afirme.midas2.dao.fuerzaventa;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ClienteAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HerenciaClientes;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteCarteraClientesView;
import mx.com.afirme.midas2.dto.fuerzaventa.HerenciaClientesView;
@Local
public interface ClienteAgenteDao extends EntidadDao{
	public ClienteAgente saveFull(ClienteAgente clienteAgente) throws Exception;
	
	public void unsubscribe(ClienteAgente clienteAgente)throws Exception;
	
	public ClienteAgente loadByIdCheckAgent(ClienteAgente clienteAgente);
	
	public List<ClienteAgente> findByFilters(ClienteAgente filtro) throws Exception;
	
	public ClienteAgente loadById(ClienteAgente clienteAgente) throws Exception;
	
	public void heredarClientes(List<ClienteAgente> clientesHeredar,Agente agenteDestino,HerenciaClientes herencia,String accion) throws Exception;
	
	public void heredarClientes(ClienteAgente clienteHeredar,Agente agenteDestino,String accion) throws Exception;
	
	public void asignarCliente(ClienteAgente clienteHeredar,Agente agenteDestino,String accion) throws Exception;
	
	public void desAsignarCliente(ClienteAgente clienteAgente) throws RuntimeException;
	
	public void cancelarHerenciaClientes(List<ClienteAgente> clientesHeredar,Agente agenteDestino,HerenciaClientes herencia) throws Exception;
	
	public void cancelarHerenciaCliente(ClienteAgente clientesHeredar,Agente agenteDestino) throws Exception;
	
	public List<ClienteAgente> listarClientesAHeredar(Agente origen,Agente destino) throws Exception;
	
	public List<ClienteAgente> listarClientesAgente(Agente agente) throws Exception;
	
	public HerenciaClientes saveHerenciaCliente(HerenciaClientes herencia)  throws Exception;
	
	public List<HerenciaClientes> findHerenciasByFilters(HerenciaClientes filtro,String isAgente) throws Exception;
	
	public String buscarAsociacionClienteAgente(ClienteAgente filtroClienteAgente,Long idAgente)throws Exception;
	
	public List<HerenciaClientesView> findByFiltersView(HerenciaClientes filtroHerencia,String isAgente) throws Exception;
	
	public HerenciaClientes obtenerHerenciaPorId(HerenciaClientes herencia) throws Exception;
	
	public List<ClienteAgente> consultaDetalleHerenciaClientes(Long herenciaId,Long agenteId) throws Exception;
	
	public void herenciaClientes(List<ClienteAgente> clientes, Agente agenteDestino,HerenciaClientes herencia,String accion) throws Exception;
		
	public void herenciaClientes(ClienteAgente clienteAgente,Agente agenteDestino,String accion) throws Exception;
	
	public List<ClienteAgente> findByFilters(ClienteAgente filtro, String fechaHistorico) throws Exception;
	
	public List<AgenteCarteraClientesView> findByFiltersCarteraClientesGrid(ClienteAgente filtroClienteAgente, String fechaHistorico) throws Exception;
	
	public void generarEndososCambioAgente(ClienteAgente clienteAgenteActual,Agente agenteDestino);
	
}