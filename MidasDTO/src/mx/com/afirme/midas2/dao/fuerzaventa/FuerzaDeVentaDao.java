package mx.com.afirme.midas2.dao.fuerzaventa;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.SPDao;
import mx.com.afirme.midas2.dto.fuerzaventa.RegistroFuerzaDeVentaDTO;

@Local
public interface FuerzaDeVentaDao extends SPDao{
		
	public List<RegistroFuerzaDeVentaDTO> listar(Object idCentroOperacion, Object idGerencia, Object idOficina, Object idPromotoria, String tipo);
	
}
