package mx.com.afirme.midas2.dao.fuerzaventa;

import java.util.List;
import javax.ejb.Local;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProductoBancarioPromotoria;
/**
 * 
 * @author vmhersil
 *
 */
@Local
public interface ProductoBancarioPromotoriaDao extends EntidadDao{
	public List<ProductoBancarioPromotoria> findByFilters(ProductoBancarioPromotoria filtro);
	
	public List<ProductoBancarioPromotoria> findByPromotoria(Long idPromotoria);
	
	public void delete(Long idProducto) throws Exception;
	
	public ProductoBancarioPromotoria saveProducto(ProductoBancarioPromotoria productoBancarioPromotoria)throws Exception;
	
	public void saveListProductoBanc(List<ProductoBancarioPromotoria> lista) throws Exception;
	
	public List<ProductoBancarioPromotoria> findByPromotoria(Long idPromotoria, String fechaHistorico, String tipoAccion);
}
