package mx.com.afirme.midas2.dao.fuerzaventa;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.dto.fuerzaventa.EjecutivoView;
@Local
public interface EjecutivoDao extends EntidadDao{
	public List<Ejecutivo> findByGerencia(Long idGerencia);
	
	public void unsubscribe(Ejecutivo ejecutivo)throws Exception;
	
	public List<Ejecutivo> findByFilters(Ejecutivo filtroEjecutivo, String fechaHistorico);
	
	public Ejecutivo saveFull(Ejecutivo ejecutivo) throws Exception;
	
	public Ejecutivo loadById(Ejecutivo ejecutivo, String fechaHistorico);
	
	public List<EjecutivoView> findByFiltersView(Ejecutivo filtroEjecutivo);
	
	public List<EjecutivoView> getList(boolean onlyActive);
	
	public List<EjecutivoView> findByGerenciaLightWeight(Long idParent);
	
	public List<EjecutivoView> findEjecutivosConGerenciasExcluyentes(List<Long> ejecutivos,List<Long> gerenciasExcluyentes,List<Long> centrosExcluyentes);
	
	public List<Ejecutivo> findAllEjecutivoResponsable();
}
