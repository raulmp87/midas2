package mx.com.afirme.midas2.dao.fuerzaventa;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CedulaSubramo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ClienteAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HerenciaClientes;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;

@Local
public interface CedulaSubramoDao extends EntidadDao{
	
	
	public List<CedulaSubramo> findByFilters(CedulaSubramo filtroCedulaSubramo);
	
	public CedulaSubramo loadById(CedulaSubramo cedulaSubramo);
	
	public CedulaSubramo saveFull(CedulaSubramo cedulaSubramo) throws Exception;
	
	public List<RamoDTO> obtenerRamos() throws Exception;
	
	public List<SubRamoDTO> obtenerSubRamosPorRamo(RamoDTO idRamo) throws Exception;
	
	public List<CedulaSubramo> obtenerSubRamosAsociadosPorCedula(ValorCatalogoAgentes tipoCedula) throws Exception;
	
	public void guardarAsociacion(List<SubRamoDTO> subRamos,ValorCatalogoAgentes tipoCedula) throws Exception;
}
