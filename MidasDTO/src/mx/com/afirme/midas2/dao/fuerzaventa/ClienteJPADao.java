package mx.com.afirme.midas2.dao.fuerzaventa;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ClienteAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ClienteJPA;

@Local
public interface ClienteJPADao extends EntidadDao{
	
	public List<ClienteJPA> findByFilters(ClienteJPA clienteJPA);
	
	public ClienteJPA loadById(ClienteJPA clienteJPA);
}
