package mx.com.afirme.midas2.dao.fuerzaventa;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.fuerzaventa.ConfiguracionAgenteDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.GuiaHonorariosAgenteView;


@Local
public interface DocumentoAgenteDao {
	
	public List<GuiaHonorariosAgenteView> obtenerGuiasHonorarios (ConfiguracionAgenteDTO configuracion, String anioMes); 
	public List<GuiaHonorariosAgenteView> obtenerGuiasHonorariosPorIds(List<Long> ids);
		
}

  	