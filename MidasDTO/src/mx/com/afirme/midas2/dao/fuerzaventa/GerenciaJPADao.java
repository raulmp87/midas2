package mx.com.afirme.midas2.dao.fuerzaventa;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.dto.fuerzaventa.GerenciaView;
@Local
public interface GerenciaJPADao extends EntidadDao{
	public List<Gerencia> findByFilters(Gerencia filtroGerencia, String fechaHistorico);
	
	public Gerencia saveFull(Gerencia gerencia) throws Exception;
	
	public void unsubscribe(Gerencia gerencia)throws Exception;
	
	public List<Gerencia> findByCentroOperacion(Long idCentroOperacion);
	
	public Gerencia loadById(Gerencia gerencia, String fechaHistorico);
	
	public List<GerenciaView> findByFiltersView(Gerencia filtroGerencia);
	
	public List<GerenciaView> getList(boolean onlyActive);
	
	public List<GerenciaView> findByCentroOperacionLightWeight(Long idParent);
	/**
	 * Obtiene la lista de gerencias por medio de sus id's excluyendo aquellas que pertenezcan a los centros de operacion correspondientes
	 * @param gerencias
	 * @param centrosOperacionExcluyentes
	 * @return
	 */
	public List<GerenciaView> findGerenciasConCentroOperacionExcluyentes(List<Long> gerencias,List<Long> centrosOperacionExcluyentes);	
}
