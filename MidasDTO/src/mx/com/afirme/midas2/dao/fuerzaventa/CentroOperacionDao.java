package mx.com.afirme.midas2.dao.fuerzaventa;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.dto.fuerzaventa.CentroOperacionView;
@Local
public interface CentroOperacionDao extends EntidadDao{
	public CentroOperacion saveFull(CentroOperacion centroOperacion) throws Exception;
	
	public void unsubscribe(CentroOperacion centroOperacion)throws Exception;
	
	public List<CentroOperacion> findByFilters(CentroOperacion filtroCentroOperacion);
	
	public List<CentroOperacionView> findByFiltersView(CentroOperacion filtroCentroOperacion);	
	
	public CentroOperacion loadById(CentroOperacion centroOperacion, String fechaHistorico);
	
	public List<CentroOperacionView> getList(boolean onlyActive);
}
