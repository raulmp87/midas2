package mx.com.afirme.midas2.dao.fuerzaventa;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.EnvioFacturaAgente;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteFacturaPendiente;


@Local
public interface EnvioFacturaAgenteDao extends EntidadDao {
	
	public EnvioFacturaAgente guardaEnvio (EnvioFacturaAgente envio);
	public void saveRelationXMLCheque(Long idAgente, String aniomes, Long idEnvio);
	public Agente getDatosAgente(String codigoUsuario, Integer idPersona, Long idAgente) throws Exception;
	public List<AgenteFacturaPendiente> getAgentesFacturaPendiente();
	public PrestadorServicio getDatosPrestadorServicio(Integer idPersona, Long idProveedor) throws Exception;
	
	
}
