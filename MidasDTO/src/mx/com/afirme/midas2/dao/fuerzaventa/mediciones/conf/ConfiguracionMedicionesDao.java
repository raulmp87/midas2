package mx.com.afirme.midas2.dao.fuerzaventa.mediciones.conf;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.base.Scrollable;

@Local
public interface ConfiguracionMedicionesDao {

	public <T extends Scrollable> List<T> buscar(T filtro); 
		
	public Integer getSize(String className);
	
}
