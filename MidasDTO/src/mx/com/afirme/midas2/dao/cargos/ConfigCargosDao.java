package mx.com.afirme.midas2.dao.cargos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.cargos.ConfigCargos;
import mx.com.afirme.midas2.domain.prestamos.ConfigPrestamoAnticipo;
import mx.com.afirme.midas2.dto.Cargos.DetalleCargosView;
import mx.com.afirme.midas2.dto.fuerzaventa.EntregoDocumentosView;

@Local
public interface ConfigCargosDao extends EntidadDao {
	
	public static enum estatusMoviemientoCargos{
		PENDIENTE("PENDIENTE"), APLICADO("APLICADO"),CANCELADO("CANCELADO");
		
		private String value;
		private estatusMoviemientoCargos(String value){
			this.value=value;
		}
		public String getValue (){
			return value;
		}
	}

	public ConfigCargos  loadById(ConfigCargos obj) throws Exception;
	
	public List<DetalleCargosView> findByFilters(ConfigCargos configCargos) throws Exception;
	
	public ConfigCargos save(ConfigCargos obj)throws Exception;
	
	public ConfigCargos updateEstatus(ConfigCargos configCargos, String elementoCatalogo)throws Exception;
	
//	public ReporteIngresosAgente obtenerDatosReporte(ConfigPrestamoAnticipo configPrestamoAnticipo) throws Exception;
	
	public ConfigCargos aplicar(ConfigCargos configCargos) throws Exception;
	
	public void crearYGenerarDocumentosFortimax(ConfigCargos config) throws Exception;
	
	public List<EntregoDocumentosView> consultaEstatusDocumentos(Long idCargo,Long idAgente) throws Exception;
	
	public void auditarDocumentosEntregadosCargos(Long idCargo,Long idAgente,String nombreAplicacion) throws Exception;
}
