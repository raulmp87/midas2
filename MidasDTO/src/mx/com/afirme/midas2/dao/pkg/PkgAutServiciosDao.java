package mx.com.afirme.midas2.dao.pkg;

import javax.ejb.Local;

@Local
public interface PkgAutServiciosDao {
	
	/**
	 * Metodo que permite calcular la zona de circulacion dado 
	 * un estado y un municipio
	 * 
	 * @param idEstado
	 * @param idMunicipio
	 * @return String
	 */
	public String getZonaCirculacion(Integer idEstado, Integer idMunicipio);
	
}
