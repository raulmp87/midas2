package mx.com.afirme.midas2.dao.pkg;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;

@Local
public interface PkgAutGeneralesDao {
	public Map<String, String> emitirCotizacion(CotizacionDTO cotizacion);

	public Map<String, String> migraBitemporalidad(Long polizaId, Date validFrom, Date validTo);

	public List<ReciboDTO> emiteRecibos(String polizaEndoso);
	
	public void cancelarEmision(Double idToPoliza);
	
	public List<ReciboDTO> emiteRecibosEndoso(String polizaEndoso);
	
	public void updateEmisionPendiente(short tipoEmision, boolean esAutomatica);
	
	public void updateEmisionPendiente(short tipoEmision, boolean esAutomatica, Date fechaValidacion);
}
