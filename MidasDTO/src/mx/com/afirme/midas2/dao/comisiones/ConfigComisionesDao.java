package mx.com.afirme.midas2.dao.comisiones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.comisiones.ConfigComCentroOperacion;
import mx.com.afirme.midas2.domain.comisiones.ConfigComEjecutivo;
import mx.com.afirme.midas2.domain.comisiones.ConfigComGerencia;
import mx.com.afirme.midas2.domain.comisiones.ConfigComMotivoEstatus;
import mx.com.afirme.midas2.domain.comisiones.ConfigComPrioridad;
import mx.com.afirme.midas2.domain.comisiones.ConfigComPromotoria;
import mx.com.afirme.midas2.domain.comisiones.ConfigComSituacion;
import mx.com.afirme.midas2.domain.comisiones.ConfigComTipoAgente;
import mx.com.afirme.midas2.domain.comisiones.ConfigComTipoPromotoria;
import mx.com.afirme.midas2.domain.comisiones.ConfigComisiones;
import mx.com.afirme.midas2.domain.comisiones.ItemsConfigComision;
import mx.com.afirme.midas2.dto.comisiones.ConfigComisionesDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.CentroOperacionView;
import mx.com.afirme.midas2.dto.fuerzaventa.EjecutivoView;
import mx.com.afirme.midas2.dto.fuerzaventa.GerenciaView;
import mx.com.afirme.midas2.dto.fuerzaventa.PromotoriaView;
@Local
public interface ConfigComisionesDao extends EntidadDao {
	public ConfigComisiones saveConfiguration(ConfigComisiones configuration) throws Exception;

	public ConfigComisiones saveCentroOperacionConfig(ConfigComisiones configuration,List<ConfigComCentroOperacion> centroOperaciones) throws Exception;
	
	public ConfigComisiones saveGerenciaConfig(ConfigComisiones configuration,List<ConfigComGerencia> gerencias) throws Exception;
	
	public ConfigComisiones saveEjecutivoConfig(ConfigComisiones configuration,List<ConfigComEjecutivo> ejecutivos) throws Exception;
	
	public ConfigComisiones savePromotoriaConfig(ConfigComisiones configuration,List<ConfigComPromotoria> promotorias) throws Exception;
	
	public ConfigComisiones saveTipoPromotoriaConfig(ConfigComisiones configuration,List<ConfigComTipoPromotoria> tiposPromotoria) throws Exception;
	
	public ConfigComisiones saveTipoAgenteConfig(ConfigComisiones configuration,List<ConfigComTipoAgente> tiposAgente) throws Exception;
	
	public ConfigComisiones savePrioridadConfig(ConfigComisiones configuration,List<ConfigComPrioridad> prioridades) throws Exception;
	
	public ConfigComisiones saveSituacionConfig(ConfigComisiones configuration,List<ConfigComSituacion> situaciones) throws Exception;
	
	public List<CentroOperacionView> getCentroOperacionList()throws Exception;
	
	public List<GerenciaView> getGerenciaList()throws Exception;
	
	public List<EjecutivoView> getEjecutivoList()throws Exception;
	
	public List<PromotoriaView> getPromotoriaList() throws Exception;
	
	public List<ValorCatalogoAgentes> getTipoPromotoriaList() throws Exception;
	
	public List<ValorCatalogoAgentes> getTipoDeAgenteList() throws Exception;
	
	public List<ValorCatalogoAgentes> getPrioridadList() throws Exception;
	
	public List<ValorCatalogoAgentes> getSituacionList() throws Exception;
	
	public List<ValorCatalogoAgentes> getCatalogoPeriodo() throws Exception;
	
	public List<ValorCatalogoAgentes> getCatalogoModoEjecucion() throws Exception;
	
	public List<ConfigComisiones> findByFilters(ConfigComisiones filtro) throws Exception;
	
	public List<ConfigComisionesDTO> findByFiltersView(ConfigComisionesDTO filtro);
	
	public ConfigComisiones loadById(ConfigComisiones config) throws Exception;
	
	public void deleteConfiguration(ConfigComisiones configuration) throws Exception;
	
	public List<ConfigComCentroOperacion> getCentrosOperacionPorConfiguracion(ConfigComisiones config) throws Exception;
	
	public List<Long> getCentrosOperacionPorConfiguracionView(ConfigComisiones config) throws Exception ;
	
	public List<ConfigComGerencia> getGerenciasPorConfiguracion(ConfigComisiones config) throws Exception;
	
	public List<Long> getGerenciasPorConfiguracionView(ConfigComisiones config) throws Exception ;
	
	public List<ConfigComEjecutivo> getEjecutivosPorConfiguracion(ConfigComisiones config) throws Exception;
	
	public List<Long> getEjecutivosPorConfiguracionView(ConfigComisiones config) throws Exception ;
	
	public List<ConfigComPromotoria> getPromotoriasPorConfiguracion(ConfigComisiones config) throws Exception;
	
	public List<Long> getPromotoriasPorConfiguracionView(ConfigComisiones config) throws Exception ;
	
	public List<ConfigComPrioridad> getPrioridadesPorConfiguracion(ConfigComisiones config) throws Exception;
	
	public List<Long> getPrioridadesPorConfiguracionView(ConfigComisiones config) throws Exception ;
	
	public List<ConfigComTipoPromotoria> getTiposPromotoriaPorConfiguracion(ConfigComisiones config) throws Exception;
	
	public List<Long> getTiposPromotoriaPorConfiguracionView(ConfigComisiones config) throws Exception;
	
	public List<ConfigComTipoAgente> getTiposAgentePorConfiguracion(ConfigComisiones config) throws Exception;
	
	public List<Long> getTiposAgentePorConfiguracionView(ConfigComisiones config) throws Exception;
	
	public List<ConfigComSituacion> getSituacionesPorConfiguracion(ConfigComisiones config) throws Exception;

	public List<Long> getSituacionesPorConfiguracionView(ConfigComisiones config) throws Exception; 
	
	public List<GerenciaView> getGerenciasConCentrosOperacionExcluyentes(List<Long> configGerencias,List<Long> configCentrosOperacion);
	
	public List<EjecutivoView> getEjecutivosConGerenciasExcluyentes(List<Long> configEjecutivos,List<Long> configGerencias,List<Long> configCentros);
	
	public List<PromotoriaView> getPromotoriasConEjecutivosExcluyentes(List<Long> configPromotorias,List<Long> configEjecutivos,List<Long> configGerencias,List<Long> configCentros);
	
	public List<AgenteView> obtenerAgentesPorConfiguracion(ConfigComisiones configuracion) throws Exception;
	
	public List<ConfigComMotivoEstatus> getMotivoEstatusPorConfiguracion(ConfigComisiones config) throws Exception;
	
	public List<Long> getMotivoEstatusPorConfiguracionView(ConfigComisiones config) throws Exception;

	public ConfigComisiones saveMotivoEstatus(ConfigComisiones configuration,  List<ConfigComMotivoEstatus> motivoEstatus) throws Exception;
	
	public String obtenerAgentesPorConfiguracionQuery(ConfigComisiones configuracion) throws Exception;
	
	public String obtenerAgentesCuentaAfirme() throws Exception;
	
	//****************************************************************************************************
	public List<ItemsConfigComision> getCentrosOperacionPorConfigChecked(ConfigComisiones config) throws Exception;
	
	public List<ItemsConfigComision> getGerenciasPorConfigChecked(ConfigComisiones config) throws Exception;
	
	public List<ItemsConfigComision> getPromotoriasPorConfigChecked(ConfigComisiones config) throws Exception;	
	
	public List<ItemsConfigComision> getPrioridadesPorConfigChecked(ConfigComisiones config) throws Exception;	
	
	public List<ItemsConfigComision> getTiposPromotoriaPorConfigChecked(ConfigComisiones config) throws Exception;
	
	public List<ItemsConfigComision> getTiposAgentePorConfigChecked(ConfigComisiones config) throws Exception;
	
	public List<ItemsConfigComision> getSituacionesPorConfigChecked(ConfigComisiones config) throws Exception;
	
	public List<ItemsConfigComision> getEjecutivosPorConfigChecked(ConfigComisiones config) throws Exception;
	
	public List<ItemsConfigComision> getMotivoEstatusPorConfigChecked(ConfigComisiones config) throws Exception;
}
