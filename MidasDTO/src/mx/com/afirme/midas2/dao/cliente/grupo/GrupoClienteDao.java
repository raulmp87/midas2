package mx.com.afirme.midas2.dao.cliente.grupo;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.cliente.GrupoCliente;

@Local
public interface GrupoClienteDao extends Dao<Long, GrupoCliente>{

	public GrupoCliente findByClave(String clave);
	public List<GrupoCliente> findByCliente(Long idCliente);
	
}
