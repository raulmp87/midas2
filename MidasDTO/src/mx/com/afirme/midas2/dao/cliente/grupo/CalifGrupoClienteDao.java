package mx.com.afirme.midas2.dao.cliente.grupo;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.cliente.CalifGrupoCliente;

@Local
public interface CalifGrupoClienteDao extends Dao<Long, CalifGrupoCliente>{
	
}
