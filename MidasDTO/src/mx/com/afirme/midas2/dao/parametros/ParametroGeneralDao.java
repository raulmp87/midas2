package mx.com.afirme.midas2.dao.parametros;

import javax.ejb.Local;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;

@Local
public interface ParametroGeneralDao {
	
	public ParametroGeneralDTO findById(ParametroGeneralId id);

}
