package mx.com.afirme.midas2.dao.custShipmentTracking;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.OrdenRenovacionMasiva;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia.BitacoraGuia;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia.RegistroGuiasProveedor;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia.RegistroGuiasProveedorId;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia.ReporteEfectividadEntrega;

@Local
public interface TrackingDao extends EntidadDao{

	public void getGuias(List<String> guias);
	
	public RegistroGuiasProveedor findRegistroById(RegistroGuiasProveedorId id);
	
	public void saveRegistroGuiasProveedor(RegistroGuiasProveedor registro);
	
	public void updateRegistroGuia(RegistroGuiasProveedor registro);
	
	public List<RegistroGuiasProveedor> getRegiostosGuiaSinProcesar();
	
	public BitacoraGuia saveBitacora(BitacoraGuia bitacora);
	
	public void generaDetallePendienteGuia(BitacoraGuia bitacora);
	
	public List<ReporteEfectividadEntrega> getDatosReporteEfectividadEntrega(OrdenRenovacionMasiva ordenRenovacion);
	
	public void procesarTareaEstatusPendientes();
	
	public List<BitacoraGuia> getBitacoraByIdToPoliza(BigDecimal idToPoliza);
	
	public String getEstatusBitacora(Integer estatus);
}

