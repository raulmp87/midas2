package mx.com.afirme.midas2.dao.Excluir;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.catalogos.ExcluirAgentes.Excluir;

@Local
public interface ExcluirDao {

	public void eliminarExcluir(Excluir excluir);

	public List<Excluir> getListExcluirAgentes(Excluir excluir);

}
