package mx.com.afirme.midas2.dao.amis.alertas;

public class EnvioConsultaAcciones {
	private String vin;
	private String idCotizacion;
	private RespuestaSapAmisAlerta[] alertas;
	public String getVin() {
		return vin;
	}
	public void setVin(String vin) {
		this.vin = vin;
	}
	public String getIdCotizacion() {
		return idCotizacion;
	}
	public void setIdCotizacion(String idCotizacion) {
		this.idCotizacion = idCotizacion;
	}
	public RespuestaSapAmisAlerta[] getAlertas() {
		return alertas;
	}
	public void setAlertas(RespuestaSapAmisAlerta[] alertas) {
		this.alertas = alertas;
	}
}
