package mx.com.afirme.midas2.dao.amis.alertas;

public class RespuestaSapAmisAccion {
	protected long idAccion;
	protected String descAccion;
	public long getIdAccion() {
		return idAccion;
	}
	public void setIdAccion(long idAccion) {
		this.idAccion = idAccion;
	}
	public String getDescAccion() {
		return descAccion;
	}
	public void setDescAccion(String descAccion) {
		this.descAccion = descAccion;
	}
}
