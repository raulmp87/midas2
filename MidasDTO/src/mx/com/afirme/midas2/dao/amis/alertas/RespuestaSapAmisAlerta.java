package mx.com.afirme.midas2.dao.amis.alertas;

import java.util.Collection;

public class RespuestaSapAmisAlerta {
	protected long idAlerta;
	protected String sistema;
	protected String alerta;
	protected String vin;
	protected long idRelacion;
	
	protected Collection<RespuestaSapAmisAccion> acciones;
	
	public long getIdAlerta() {
		return idAlerta;
	}
	public void setIdAlerta(long idAlerta) {
		this.idAlerta = idAlerta;
	}
	public String getSistema() {
		return sistema;
	}
	public void setSistema(String sistema) {
		this.sistema = sistema;
	}
	public String getAlerta() {
		return alerta;
	}
	public void setAlerta(String alerta) {
		this.alerta = alerta;
	}
	public String getVin() {
		return vin;
	}
	public void setVin(String vin) {
		this.vin = vin;
	}
	public long getIdRelacion() {
		return idRelacion;
	}
	public void setIdRelacion(long idRelacion) {
		this.idRelacion = idRelacion;
	}
	public Collection<RespuestaSapAmisAccion> getAcciones() {
		return acciones;
	}
	public void setAcciones(Collection<RespuestaSapAmisAccion> acciones) {
		this.acciones = acciones;
	}
}
