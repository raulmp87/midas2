package mx.com.afirme.midas2.dao.amis.alertas;

public class EnvioConsultaAlertas {
	private String user;
	private String pass;
	private String[] vin;
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String[] getVin() {
		return vin;
	}
	public void setVin(String[] vin) {
		this.vin = vin;
	}
}
