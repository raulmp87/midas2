package mx.com.afirme.midas2.dao.movimientosmanuales;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.movimientosmanuales.MovimientosManuales;
/**
 * Interface para consultar valores del catalogo.
 * @author vmhersil
 *
 */
@Local
public interface MovimientosManualesDao extends EntidadDao{
	/**
	 * Obtiene la lista de catalogos segun los filtros que se proporcionen
	 * @param filtro
	 * @return
	 * @throws Exception
	 */
	public List<MovimientosManuales> findByFilters(MovimientosManuales filtro) throws Exception;
	
	public int aplicarMovimientoManual(Long idMovimientoManual, String idUsuarioAplica) throws Exception;

	public int eliminarMovimientoManual(Long idMovimientoManual);
		
	
}
