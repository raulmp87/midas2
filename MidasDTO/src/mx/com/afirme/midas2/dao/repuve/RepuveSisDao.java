package mx.com.afirme.midas2.dao.repuve;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.dto.repuve.sistemas.RepuveSisEmision;
import mx.com.afirme.midas2.dto.repuve.sistemas.RepuveSisPTT;
import mx.com.afirme.midas2.dto.repuve.sistemas.RepuveSisRecuperacion;
import mx.com.afirme.midas2.dto.repuve.sistemas.RepuveSisRobo;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;

/*******************************************************************************
 * Nombre Interface: 	RepuveSisDao.
 * 
 * Descripcion: 		Se utiliza para el manejo de Transacciones de las 
 * 						Bitacoras de procesos con la Base de Datos.
 * 						
 * 						Se utiliza el objeto SapAmisBitacoras para el manejo
 * 						de las transacciones.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/

public interface RepuveSisDao extends Serializable{
	public List<RepuveSisEmision> obtenerEmisionPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina);
	public List<RepuveSisRobo> 	obtenerRoboPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina);
	public List<RepuveSisRecuperacion> obtenerRecuperacionPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina);
	public List<RepuveSisPTT> obtenerPTTPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina);
}