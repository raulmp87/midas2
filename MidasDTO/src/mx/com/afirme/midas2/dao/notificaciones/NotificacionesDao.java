package mx.com.afirme.midas2.dao.notificaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.notificaciones.ConfiguracionNotificaciones;
import mx.com.afirme.midas2.domain.notificaciones.DestinatariosNotificaciones;

@Local
public interface NotificacionesDao {
	
	public Long saveConfigNotificaciones(ConfiguracionNotificaciones configNotificaciones);
	
	public List<ConfiguracionNotificaciones> findAllConfig();
	
	public ConfiguracionNotificaciones findByIdConfig(Long id);
	
	public List<DestinatariosNotificaciones>findDestinatariosByIdConfig(ConfiguracionNotificaciones idConfiguracion);
	
	public List<ConfiguracionNotificaciones> findByFilters(ConfiguracionNotificaciones configNotificaciones);
	
	public void eliminarNotificacion(ConfiguracionNotificaciones configNotificaciones);
	
}
