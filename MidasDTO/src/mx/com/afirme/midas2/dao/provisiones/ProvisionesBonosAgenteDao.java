package mx.com.afirme.midas2.dao.provisiones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProvisionesAgentes;
import mx.com.afirme.midas2.domain.provisiones.ProvisionImportesRamo;
import mx.com.afirme.midas2.domain.provisiones.ToAjusteProvision;
import mx.com.afirme.midas2.dto.fuerzaventa.AjusteProvisionDto;
import mx.com.afirme.midas2.dto.fuerzaventa.GenericaAgentesView;
import mx.com.afirme.midas2.dto.fuerzaventa.ProvisionesBonoAgentesView;
import mx.com.afirme.midas2.dto.fuerzaventa.ProvisionesImportePorRamo;

@Local
public interface ProvisionesBonosAgenteDao extends EntidadDao{

	public List<ProvisionesBonoAgentesView> findByFilterWiew(ProvisionesAgentes filtro) throws Exception;
	
	public ProvisionesAgentes loadById(Long idProvision) throws Exception;
	
	public void creaProvisionAutomatica() throws Exception;
	
	public ProvisionesAgentes autorizaProvision(ProvisionesAgentes provision) throws Exception;
	
	public List<GenericaAgentesView> creaFiltroBonos(Long idProvision) throws Exception;
	public List<GenericaAgentesView> creaFiltroPromotoria(Long idProvision) throws Exception;
	public List<GenericaAgentesView> creaFiltroAgente(Long idProvision) throws Exception;
	public List<GenericaAgentesView> creaFiltroGerencia(Long idProvision) throws Exception;
	public List<GenericaAgentesView> creaFiltroGerente(Long idProvision) throws Exception;
	public List<GenericaAgentesView> creaFiltroLineaVenta(Long idProvision) throws Exception;
	public List<GenericaAgentesView> creaFiltroProducto(Long idProvision) throws Exception;
	public List<GenericaAgentesView> creaFiltroEjecutivo(Long idProvision) throws Exception;
	public List<GenericaAgentesView> creaFiltroLineaNegocio(Long idProvision) throws Exception;
	
	public List<ProvisionesImportePorRamo>  getRamosByFilterProvision(ProvisionImportesRamo filtrosDetalleProvision) throws Exception;
	
	public ProvisionImportesRamo updateImporteRamo(ProvisionImportesRamo ramo) throws Exception;
	
	public List<ProvisionImportesRamo> updateImporteXRamo(List<ProvisionImportesRamo> listaRamos,ProvisionImportesRamo filtrosDetalleProvision) throws Exception;
	
	public List<ProvisionesImportePorRamo> importeTotalPorRamo(Long idProvision) throws Exception;
	
	public ProvisionesAgentes autorizarProvision(Long idProvision) throws Exception;
	
	public ProvisionImportesRamo existeRegistroImporteRamo(ProvisionImportesRamo filtrosDetalleProvision) throws Exception;
	
	public AjusteProvisionDto listAjusteProvision(Long idProvision) throws Exception;
	
	public String saveAjusteProvision(List<ToAjusteProvision> listaAjuste) throws Exception;
	
//	public List<GenericaAgentesView> listaDeRamos() throws Exception;
}
