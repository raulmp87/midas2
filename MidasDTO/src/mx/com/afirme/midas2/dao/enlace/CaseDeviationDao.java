package mx.com.afirme.midas2.dao.enlace;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dto.enlace.CaseDeviationDTO;

@Local
public interface CaseDeviationDao extends EntidadDao {
	public List<CaseDeviationDTO> getDeviation(String createUser);
}
