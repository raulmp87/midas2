package mx.com.afirme.midas2.dao.enlace;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dto.enlace.CaseDTO;

@Local
public interface CaseDao extends EntidadDao {
	/**
	 * Obtiene los casos que tengan determinados estatus y le pertenezcan a
	 * cierto usuario
	 * 
	 * @param createUser
	 * @param assignedUser
	 * @param status
	 * @return
	 */
	public List<CaseDTO> getByStatus(String createUser, String assignedUser,
			int... status);

}
