package mx.com.afirme.midas2.dao.vehiculo;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.vehiculo.ValorComercialVehiculo;
import mx.com.afirme.midas2.dto.vehiculo.ValorReferenciaComercialAutoDTO;

@Local
public interface ValorComercialVehiculoDao {
	
	public List<ValorComercialVehiculo> findByFilters(ValorComercialVehiculo valorComercialVehiculo, boolean tipo);
	
	public List<Integer> obtenerAnioValorComercial();
	
	public List<Integer> obtenerAnioModelo();
	
	List<ValorReferenciaComercialAutoDTO> getValorComercial(Long claveAmis,short modelo , Date fechaConsulta);
	
	
}
