package mx.com.afirme.midas2.dao.prestamos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentosAgrupados;
import mx.com.afirme.midas2.domain.prestamos.ConfigPrestamoAnticipo;
import mx.com.afirme.midas2.dto.fuerzaventa.EntregoDocumentosView;
import mx.com.afirme.midas2.dto.prestamos.ConfigPrestamoAnticipoView;
import mx.com.afirme.midas2.dto.prestamos.ReporteIngresosAgente;

@Local
public interface ConfigPrestamoAnticipoDao extends EntidadDao {
	
	public static enum EstatusMovimientoPrestamoAnticipo{
		PENDIENTE("PENDIENTE"),APLICADO("APLICADO"),CANCELADO("CANCELADO"),PAGADO("PAGADO");
		private String value;
		private EstatusMovimientoPrestamoAnticipo(String value){
			this.value=value;
		}
		public String getValue(){
			return value;
		}
	}
	
	public static enum TipoCalculo{
		PRESTAMOS("PRESTAMOS"),CARGOS("CARGOS");
		private String value;
		private TipoCalculo(String value){
			this.value=value;
		}
		public String getValue(){
			return value;
		}
	}

	public ConfigPrestamoAnticipo  loadById(ConfigPrestamoAnticipo obj) throws Exception;
	
	public List<ConfigPrestamoAnticipoView> findByFilters(ConfigPrestamoAnticipo obj) throws Exception;
	
	public ConfigPrestamoAnticipo save(ConfigPrestamoAnticipo obj)throws Exception;
	
	public ConfigPrestamoAnticipo updateEstatus(ConfigPrestamoAnticipo configPrestamoAnticipo, String elementoCatalogo)throws Exception;
	
	public ReporteIngresosAgente obtenerDatosReporte(ConfigPrestamoAnticipo configPrestamoAnticipo) throws Exception;
	
	public ConfigPrestamoAnticipo autorizarMovimiento(ConfigPrestamoAnticipo config) throws Exception;
	
	public ConfigPrestamoAnticipo solicitarChequeDelMovimiento(ConfigPrestamoAnticipo config) throws Exception;
	
	public List<EntregoDocumentosView> consultaEstatusDocumentos(Long idPrestamo,Long idAgente) throws Exception;
	
	public List<DocumentosAgrupados> documentosFortimaxGuardadosDB(Long idPrestamo,Long idAgente);
	
	public void auditarDocumentosEntregadosPrestamos(Long idPrestamo,Long idAgente,String nombreAplicacion) throws Exception;
}
