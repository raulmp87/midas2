package mx.com.afirme.midas2.dao.siniestros.notificaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.notificaciones.BitacoraDetalleNotificacion;
import mx.com.afirme.midas2.service.siniestros.notificaciones.BitacoraNotificacionService.BitacoraNotificacionFiltroDTO;
import mx.com.afirme.midas2.service.siniestros.notificaciones.BitacoraNotificacionService.BitacoraRegistroDetalleNotificacionDTO;

@Local
public interface BitacoraNotificacionDao{
	
	/**
	 * Obtiene la última versión de todos los detalles pertenecientes 
	 * a un registro marcado como recurrente.
	 * @return
	 */
	public List<BitacoraRegistroDetalleNotificacionDTO> obtenerRecurrentes();
	
	/**
	 * Obtener detalle de los registros de bitacora de una notificacion. 
	 * Cada detalle contiene la bitacora a quien corresponde.
	 * @param filtro
	 * @return
	 */
	public List<BitacoraDetalleNotificacion> obtenerRegistros(BitacoraNotificacionFiltroDTO filtro);

}
