package mx.com.afirme.midas2.dao.siniestros.configuracion.horario.ajustador;


import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.ajustador.HorarioAjustador;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.ajustador.HorarioAjustadorId;
import mx.com.afirme.midas2.dto.siniestros.configuracion.horario.ajustador.ConfiguracionDeDisponibilidadDTO;
import mx.com.afirme.midas2.service.siniestros.configuracion.horario.ajustador.HorarioAjustadorService.HorarioAjustadorFiltro;


@Local
public interface HorarioAjustadorDAO extends
		Dao<HorarioAjustadorId, HorarioAjustador> {

	
	/**
	 * Save settings for HorarioAjustador into database. Return a numeric variable. This for success code or error code.
	 * 
	 * @param ConfiguracionDeDisponibilidadDTO configuraciones
	 * 
	 */
	public int spGuardarHorariosAjustadorInt (ConfiguracionDeDisponibilidadDTO configuraciones);
	/**
	 * Funcion que genera el query para implemantar la RDN
	 * {3B578676-B7F8-4fee-8A1F-34D8E1864BF1} / Analisis MIDAS II.Requirements
	 * Model.Requerimientos MODULO SINIESTROS AUTOS.CABINA.Configurador de
	 * Horarios de Ajustadores.RDN: Búsqueda por varios campos capturados
	 * 
	 */
	public List<HorarioAjustador> buscar(HorarioAjustadorFiltro filtro);
	
	
	
	

}
