package mx.com.afirme.midas2.dao.siniestros.cabina.reportecabina;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.CausaMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.TipoDocumentoMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.TipoMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoSiniestro;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.MovimientoSiniestroDTO;

@Local
public interface MovimientoSiniestroDao {
	
	public MovimientoCoberturaSiniestro contabilizaReserva(Long movimientoId);
	
	public List<MovimientoSiniestroDTO> obtenerMovimientos(MovimientoSiniestroDTO filtroBusqueda);
	
	public List<MovimientoCoberturaSiniestro> obtenerMovimientos(Long reporteCabinaId, Long coberturaId, Long estimacionId, 
			String claveTipoCalculo, String claveSubCalculo, TipoDocumentoMovimiento tipoDocumento, TipoMovimiento tipoMovimiento, 
			CausaMovimiento causaMovimiento, Date fechaUltimoMovimiento);
	
	public BigDecimal obtenerImporteMovimientos(Long reporteCabinaId, Long coberturaId, Long estimacionId, 
			String claveTipoCalculo,String claveSubCalculo, TipoDocumentoMovimiento tipoDocumento, TipoMovimiento tipoMovimiento, 
			CausaMovimiento causaMovimiento, Date fechaUltimoMovimiento);
	
	public BigDecimal obtenerImporteMovimientos(MovimientoSiniestroDTO filtroBusqueda);
	
	public List<MovimientoSiniestroDTO> obtenerMovimientosGA(MovimientoSiniestroDTO filtroBusqueda);
	
	public List<MovimientoSiniestro> obtenerMovimientosGA(Long reporteCabinaId, Long ordenCompraId, 
			TipoDocumentoMovimiento tipoDocumento, TipoMovimiento tipoMovimiento, CausaMovimiento causaMovimiento, Date fechaUltimoMovimiento);
	
	public BigDecimal obtenerImporteMovimientosGA(Long reporteCabinaId, Long ordenCompraId, 
			TipoDocumentoMovimiento tipoDocumento, TipoMovimiento tipoMovimiento, CausaMovimiento causaMovimiento, Date fechaUltimoMovimiento);	
	
	public BigDecimal obtenerImporteMovimientosGA(MovimientoSiniestroDTO filtroBusqueda);
	
	
	public BigDecimal obtenerMontoCoberturaReporteCabina(Long idCoberturaReporte,Long estimacionId, String claveSubTipoCalculo);
	
	public BigDecimal obtenerMontoSiniestrosAnteriores(Long idCoberturaReporte);
	
	public Map<String,String> obtenerUsuariosMovSiniestros();
}
