package mx.com.afirme.midas2.dao.siniestros.recuperacion;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.CartaCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.OrdenCompraCartaCia;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionCompania;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.CartaCiaDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.RecuperacionCiaDTO;

/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 10-jul-2015 08:08:25 p.m.
 */
@Local
public interface RecuperacionCiaDao  {



	/**
	 * <b>SE AGREGA EN DISE�O REGISTRAR REDOCUMENTACION</b>
	 * 
	 * Obtener las Cartas que s ehan Elaborado para una Recuperaci�n de Compa��a.
	 * 
	 * Hacer un JPQL sobre CartaCompania y filtrar por aquellas Cartas que esten con
	 * estatus RECHAZADA o ELABORADA
	 * 
	 * @param recuperacionId
	 */
	public List<CartaCompania> obtenerCartasRedoc(Long recuperacionId);

	/**
	 * Crear un JPQL que obtenga las Ordenes de Compra asociadas a la Estimacion
	 * deseada.
	 * 
	 * El objeto a retornar debe contener lo siguiente:
	 * 
	 * <b>ordenCompra</b>: Orden de Compra asociada
	 * 
	 * <b>subTotalOC</b>: Suma del importe del subtotal de los Detalles de la OC
	 * 
	 * <b>conceptoDesc</b>: Si tiene mas de un concepto asociado debe mostrar la
	 * palabra VARIOS, de lo contario nel nombre del concepto asociado.
	 * 
	 * <b>subTotalRecupProveedor</b>: Buscar si la OC esta asociada a una Recuperaci�n
	 * de Proveedor que se encuentre en estatus RECUPERADO.
	 * 
	 * <b>porcentajeParticipacion</b>: Es el que se recibe de par�metro:
	 * 
	 * <b>montoARecuperar</b>: (SubTotalOc- <b>subTotalRecupProveedor ) *
	 * porcentajeParticipacion</b>
	 * 
	 * Si el par�metro ordenesConcat no es nulo, quiere decir que se deben marcar
	 * estas ordenes de compas como <b>seleccionadas = true</b>
	 * 
	 * Si el par�metro <b>soloSeleccionados </b>es true, entonces solo filtrar por
	 * estos ID
	 * 
	 * @param estimacionId
	 * @param ordenesConcat
	 * @param porcentajeParticipacion
	 * @param soloSeleccionados
	 */
	public List<OrdenCompraCartaCia>obtenerOCestimacion(Long recuperacionId, Long reporteCabinaId, Long coberturaId, Long estimacionId, 
			String ordenesConcat, Integer porcentajeParticipacion, Boolean soloSeleccionados);
	/**
	 * <b>SE DEFINE EN DISE�O DE MOSTRAR RECUPERACIONES CIA POR VENCER</b>
	 * 
	 * M�todo que obtiene las Recuperaciones de Compa��a que estan pr�ximas a vencer
	 * en base a la Fecha L�mite de Cobro.
	 * 
	 * Hacer un JPQL sobre RecuperacionCompania pero los siguientes datos transient
	 * sedeben obtener de la siguiente manera:
	 * 
	 * diasVencimiento: Obtener la ultima Carta Compania que tengas estatus REGISTRADO
	 * O PEND_ELAB
	 * 
	 * Condicionar que la diferencia entre la Fecha Limite de Cobro y la Fecha Actual
	 * es de menor o igual a 90 dias y que tenga una Carta Cia en estatus REGISTRADO o
	 * PEND_ELAB
	 */
	public List<RecuperacionCompania> obtenerRecuperacionesVencer();
	
	public List<PrestadorServicio> obtenerCiasPendientePago();
	
	public List<CartaCompania> obtenerCartasCiaSinPago(Long companiaId);
	
	public List<RecuperacionCompania> buscarRecuperacionesCia(RecuperacionCiaDTO recuperacionCiaFiltro);
	
	public List<CartaCompania> obtenerCartasARedoc();
	
	public List<CartaCiaDTO> obtenerCartasAElaborar();
	
	public List<String> obtenerReferenciasIdentificadas() ;
	
	public List<RecuperacionCompania> obtenerRecuperacionesActualizables(Long estimacionId, Integer companiaId);
	
	public List<RecuperacionCompania> obtenerRecuperacionActiva(Long estimacionId);

	public List<OrdenCompraCartaCia> obtenerOrdenesCompraCia(Long cartaCiaId);
	
	public List<RecuperacionCompania> obtenerRecuperacionPendientePorRecuperar(Long estimacionId);
}