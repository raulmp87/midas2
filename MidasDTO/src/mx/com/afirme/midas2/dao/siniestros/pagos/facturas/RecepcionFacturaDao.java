package mx.com.afirme.midas2.dao.siniestros.pagos.facturas;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.siniestro.finanzas.pagos.FacturaDTO;
import mx.com.afirme.midas.siniestro.finanzas.pagos.FacturaPteComplMonitor;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.EnvioValidacionFactura;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.dto.siniestros.pagos.facturas.OrdenCompraProveedorDTO;

@Local
public interface RecepcionFacturaDao {
	
	public Long obtenerBatchId();
	
	public DocumentoFiscal validaSiExiste(Integer idProveedor, DocumentoFiscal facturaSiniestro);
	
	public List<EnvioValidacionFactura> obtenerFacturasAsignables(Long batchId);

	public List<EnvioValidacionFactura> obtenerFacturasProcesadas(Long idBatch);
	
	public List<DocumentoFiscal> obtenerFacturasRegistradaByOrdenCompra(Long idOrdenCompra, String estatusFactura);
	
	public List<DocumentoFiscal> recepcionFacturaService(OrdenCompraProveedorDTO filtroAgrupador);
	
	public Boolean validaNombreDelAgrupador(String nombreAgrupador);
	public List<FacturaDTO> obtenerFacturasPendientesComplementoProveedor(Integer idProveedor, String origenEnvio) throws Exception;	
	public List<FacturaDTO> obtenerFacturasPendientesComplementoAgente(Integer idAgente) throws Exception;
	public Long obtenerIdLoteCarga() throws Exception;
	public List<FacturaPteComplMonitor> obtenerFacturasPendientesComplementoMonitor(Date fechaPagoInicio, Date fechaPagoFin, String origenEnvio) throws Exception;	
	public List<FacturaPteComplMonitor> obtenerFacturasPendientesComplementoNotificacion(Date fechaPagoInicio, Date fechaPagoFin, String origenEnvio) throws Exception;
}
