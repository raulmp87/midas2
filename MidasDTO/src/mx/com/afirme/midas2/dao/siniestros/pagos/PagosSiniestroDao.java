package mx.com.afirme.midas2.dao.siniestros.pagos;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.pagos.OrdenPagoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.FacturaLiquidacionDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.DesglosePagoConceptoDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.OrdenPagosDTO;

/**
 * Definicion de m�todos del m�dulo de Pagos
 * @author usuario
 * @version 1.0
 * @updated 22-sep-2014 03:59:37 p.m.
 */
@Local
public interface PagosSiniestroDao {

	/**
	 * Obtiene lista de facturas con ordenes de compra asociadas. Esto para las liquidaciones.
	 * 
	 * @param Long idProveedor
	 */
	public List<DocumentoFiscal> getFacturasConOrdenesCompraOrderByDateLow(Long idProveedor);
	/**
	 * Obtiene el detalle de una orden de pago
	 * 
	 * @param OrdenPagoSiniestro ordenPago
	 */
	public List<DesglosePagoConceptoDTO> obtenerDetalleOrdenPago (Long idOrdenPago, Boolean calcularReserva);
	
	public List<OrdenPagosDTO> buscarOrdenesPago(OrdenPagosDTO filtro,boolean soloOrdenCompra);	
	
	public List<DocumentoFiscal>  buscarFacturasLiquidacion(Long idProveedor);
	
	public List<FacturaLiquidacionDTO> buscarOrdenesPagoFactura(Long idFactura);
	
	public List<OrdenPagoSiniestro> buscarOrdenesPagoIndemnizacion(String nombreBeneficiario);
	
	/**Deberá obtener una lista de nombres de beneficiarios de Orden de Compra donde su orden de pago no aparezca en alguna liquidación.
	  *@param String beneficiario
	 * @return  List<String>   
	 * */
	public List<String>  buscarBeneficiario(String beneficiario);
	/**Deberá obtener dado un nombre de beneficiario, sus órdenes de pago que no aparezcan en alguna liquidación.
	 * 
	 * @param beneficiario
	 * @return List<OrdenPagoSiniestro>
	 */
	public List<OrdenPagoSiniestro>  buscarOrdenPagoIndemnizacion(String beneficiario);	
	
	public List<DesglosePagoConceptoDTO> obtenerPagosPorLiquidacion (Long idLiquidacion);
}