package mx.com.afirme.midas2.dao.siniestros.catalogo.conceptoajuste;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.siniestros.catalogos.conceptos.SeccionDTOConcepto;

@Local
public interface ConceptoAjusteDao {
	
	/**
	 * Obtiene la lista de secciones vigentes de autos para midas siniestros
	 * */
	public List<SeccionDTOConcepto> getListarSeccionesVigentesAutosUsablesSiniestros();

}
