package mx.com.afirme.midas2.dao.siniestros.cabina.notificaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.ConfiguracionNotificacionCabina;
import mx.com.afirme.midas2.dto.siniestros.cabina.notificaciones.AdjuntoConfigNotificacionCabinaDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.notificaciones.ConfiguracionNotificacionDTO;

@Local
public interface ConfiguracionNotificacionesDao {
	
	public Long guardarConfiguracion(ConfiguracionNotificacionCabina configuracion);
	
	public ConfiguracionNotificacionCabina obtenerConfiguracion(String codigo);
	
	public ConfiguracionNotificacionCabina obtenerConfiguracion(Long configuracionId);
	
	public List<ConfiguracionNotificacionCabina> obtenerConfiguraciones();
	
	public List<ConfiguracionNotificacionCabina> obtenerConfiguracionesPorFiltros(ConfiguracionNotificacionDTO configuracionDTO);
	
	public List<AdjuntoConfigNotificacionCabinaDTO>  obtenerAdjuntosSimpleList(ConfiguracionNotificacionCabina config);
	
}
