package mx.com.afirme.midas2.dao.siniestros.pagos.notasDeCredito;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.liquidacion.LiquidacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito.BusquedaNotasCreditoDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito.FacturaSiniestroRegistro;
import mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito.NotasCreditoRegistro;

/**
 * @author Israel
 * @version 1.0
 * @created 31-mar.-2015 12:44:59 p. m.
 */
@Local
public interface NotasCreditoDAO {


	public List<NotasCreditoRegistro> buscarNotasCeCredito(BusquedaNotasCreditoDTO notasCreditoFIltro);

	public List<NotasCreditoRegistro> obtenerNotasDeCreditoPorFactura(Long idFactura);
	
	public List<NotasCreditoRegistro> obtenerNotasDeCreditoPorRecuperacion(Long idRecuperacion);
	
	public List<FacturaSiniestroRegistro> obtenerFacturasParaCrearNotasDeCredito();
	
	public LiquidacionSiniestro obtenerLiquidacionDeFactura(Long facturaId);
	
	public List<DocumentoFiscal> obtenerNotasDeCreditoParaAsociarRecuperacion (Integer idProveedor);
	
	public List<NotasCreditoRegistro> obtenerNotasDeCreditoPorLiquidacion(Long idLiquidacion);
	
	public List<NotasCreditoRegistro> obtenerRecuperacionesNotasDeCreditoPorLiquidacion(Long idLiquidacion);

}