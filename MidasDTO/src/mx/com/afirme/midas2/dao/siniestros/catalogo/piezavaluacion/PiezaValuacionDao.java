package mx.com.afirme.midas2.dao.siniestros.catalogo.piezavaluacion;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.catalogo.pieza.PiezaValuacion;
import mx.com.afirme.midas2.service.siniestros.catalogo.pieza.PiezaValuacionService.PiezaValuacionFiltro;

@Local
public interface PiezaValuacionDao {
	
	/**
	 * Busqueda de piezas valuacion para el catalogo de piezas de siniestros
	 * @param piezaValuacionFiltro
	 * @return
	 */
	public List<PiezaValuacion> buscar(
			PiezaValuacionFiltro piezaValuacionFiltro);

}
