package mx.com.afirme.midas2.dao.siniestros.valuacion.ordencompra;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.OrdenCompraRecuperacionDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.BandejaOrdenCompraDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.OrdenCompraDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.permisos.OrdenesCompraAutorizarDTO;

/**
 * Definicion de m�todos del m�dulo de Pagos
 * @author usuario
 * @version 1.0
 * @updated 22-sep-2014 03:59:37 p.m.
 */
@Local
public interface OrdenCompraDao {

	
	/**
	 * Obtiene el detalle de una orden de pago
	 * 
	 * @param OrdenPagoSiniestro ordenPago
	 */
	List<OrdenCompraDTO> obtenerOrdenesCompraSiniestro(
			Long idReporteCabina,String tipo);
	//joksrc israel lizandro DAO
	public List<OrdenesCompraAutorizarDTO> getOrdenesCompraPorAuthViaSP();
	
	public List<OrdenCompra> obtenerOrdenesCompraParaFacturarPorProveedor(Integer idProveedor);

	public List<OrdenCompraRecuperacionDTO> obtenerOrdenCompraRecuperacionProveedor(Long idReporteCabina,String coberturaCompuesta , List<Long>   pasesAtencionId, String tipoOrdenCompra, Long idOrdenCompra);

	
	public BigDecimal obtenerTotalesOrdenesCompra(Long reporteCabinaId, Long coberturaId, Long estimacionId, 
			String claveTipoCalculo, String claveSubCalculo, Date fechaCreacionUltimoMovimiento, String tipoOrdenCompra, String estatus);
	
	public OrdenCompra obtenerOrdenCompra(Long keyOrdenCompra);
	
	public List<OrdenCompra> obtenerOrdenesCompraParaAgruparParaCompania(Integer idProveedor,Long oficinaSeleccionada, String tipoAgrupador);
	
	
	public List<BandejaOrdenCompraDTO> buscarOrdenesCompra(BandejaOrdenCompraDTO filtro );
	
	public Long contarOrdenesCompraBandeja(BandejaOrdenCompraDTO filtro);
	
	//Obtiene la suma de la columnda de detalle orden compra
	public BigDecimal obtenerMontoOrdenCompra(Long ordenCompraId, String columna);
	
	public OrdenCompra obtenerOrdenDeCompraExistente( Long estimacionId, BigDecimal subtotal, String tipoPago, String beneficiario, Long prestadorId, Boolean validaExiteOC  );
	
	
}