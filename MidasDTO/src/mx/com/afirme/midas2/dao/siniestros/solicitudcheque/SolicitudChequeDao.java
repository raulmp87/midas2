package mx.com.afirme.midas2.dao.siniestros.solicitudcheque;


import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.solicitudcheque.SolicitudChequeSiniestro;
import mx.com.afirme.midas2.domain.siniestros.solicitudcheque.SolicitudChequeSiniestroDetalle;
import mx.com.afirme.midas2.util.MidasException;

/**
 * Definicion de m�todos del m�dulo de Pagos
 * @author usuario
 * @version 1.0
 * @updated 22-sep-2014 03:59:37 p.m.
 */
@Local
public interface SolicitudChequeDao {
	
	/**
	 * Actualiza el estatus de una solicitud de cheque de MIZAR de un calculo, este cheque representa lo que se le va a pagar 
	 * @param cheque
	 * @throws MidasException
	 */
	
	public void actualizarSolitudChequeMizar(SolicitudChequeSiniestro solicitudCheque ) throws MidasException;
	

	/**
	 * Actualiza en MIZAR los detalles de una solicitud de cheque de un calculo, es decir, una solicitud de cheque puede tener
	 * al menos un o mas detalles de solicitud de cheque, estos se actualizan su estatus por medio de un stored procedure de SQLSERVER
	 * utilizando el datasource de Mizar
	 * @param cheque
	 * @param idCalculo
	 * @throws MidasException
	 */
	
	
	public void actualizarDetalleSolicitudChequeMizar(SolicitudChequeSiniestroDetalle cheque,Long idSolicitudCheque,Long idCalculo,
														Long claveAgente,Integer pctIVAAcreditable, Integer pctIVARetenido, 
														Integer pctISRRetenido, Integer claveTipoOperacion) throws MidasException;
	public void importarSolicitudesChequesAMizar(Long idCalculo) throws MidasException;
	/**
	 * Solicita el cheque a Mizar en base a una liquidacion o devoucion ingreso
	 * @param Long identificador 
	 * @return SolicitudChequeSiniestro
	 */
	public SolicitudChequeSiniestro solicitarChequeMizar(
			Long identificador ,SolicitudChequeSiniestro.OrigenSolicitud tipo ) throws Exception;
}