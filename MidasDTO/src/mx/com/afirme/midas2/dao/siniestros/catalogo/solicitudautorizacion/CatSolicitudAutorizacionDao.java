package mx.com.afirme.midas2.dao.siniestros.catalogo.solicitudautorizacion;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.catalogo.solicitudautorizacion.SolicitudAutorizacion;
import mx.com.afirme.midas2.domain.siniestros.catalogo.solicitudautorizacion.SolicitudAutorizacionAjusteAntiguedad;
import mx.com.afirme.midas2.dto.siniestros.catalogos.solicitudautorizacion.SolicitudAutorizacionAntiguedadDTO;
import mx.com.afirme.midas2.service.siniestros.catalogo.solicitudautorizacion.CatSolicitudAutorizacionService.SolicitudAutorizacionFiltro;

@Local
public interface CatSolicitudAutorizacionDao  {
	
	public List<SolicitudAutorizacionAjusteAntiguedad> buscarAutorizacionAntiguedad(SolicitudAutorizacionAntiguedadDTO filtro);
	
	public List<String> obtenerRolesAutorizador();
	
	public List<SolicitudAutorizacion> buscar(SolicitudAutorizacionFiltro filtroSA);
	
}
