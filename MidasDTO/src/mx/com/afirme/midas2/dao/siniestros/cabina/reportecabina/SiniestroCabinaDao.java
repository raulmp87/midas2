package mx.com.afirme.midas2.dao.siniestros.cabina.reportecabina;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;

@Local
public interface SiniestroCabinaDao extends Dao<Long, CatValorFijo>{

	public List<CatValorFijo> obtenerTerminosAjuste(String codigoTipoSiniestro, String codigoResponsabilidad);
	public String obtenerConsecutivoReporte(SiniestroCabina siniestroCabina) ;
}
