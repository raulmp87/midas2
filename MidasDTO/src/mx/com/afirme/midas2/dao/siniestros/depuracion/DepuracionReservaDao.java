package mx.com.afirme.midas2.dao.siniestros.depuracion;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.siniestros.depuracion.ConfiguracionDepuracionReserva;
import mx.com.afirme.midas2.domain.siniestros.depuracion.DepuracionReserva;
import mx.com.afirme.midas2.domain.siniestros.depuracion.DepuracionReservaDetalle;
import mx.com.afirme.midas2.dto.siniestros.depuracion.DepuracionReservaDTO;
import mx.com.afirme.midas2.dto.siniestros.depuracion.MovimientoPosteriorAjusteDTO;

@Local
public interface DepuracionReservaDao extends Dao<Long, DepuracionReserva>{
	
	public List<DepuracionReservaDetalle> getOnlyReservasDepuradasToExcel(String listaIdsDepurados);
	
	public Long procesarDepuracionPorSP(String listaIdsDepuraciones);
	
	public List<DepuracionReservaDetalle> mostrarListadoReservasParaDepurar(Long configuracionId);
	
	public List<DepuracionReservaDetalle> obtenerReservasExportacionSP(Long depuracionReservaId);
	
	public List<ConfiguracionDepuracionReserva> buscarConfiguraciones(DepuracionReservaDTO filtro);
	
	public List<ConfiguracionDepuracionReserva> obtenerConfiguracionesAlDia(Date fechaCriterio);
	
	public List<Oficina> obtenerOficinasRelacionadas(Long idDepuracion);
	
	public List<EstimacionCoberturaReporteCabina> obtenerReservas(Long idConfiguracion);
	
	public List<MovimientoCoberturaSiniestro> buscarMovimientosPosteriores (MovimientoPosteriorAjusteDTO filtro);
	
	public Long obtenerNumeroDeDepuracion(Long idConfiguracionDepuracion);
	//changes for commit
}


