package mx.com.afirme.midas2.dao.siniestros.expedientesjuridicos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.expedientejuridico.ExpedienteJuridico;
import mx.com.afirme.midas2.dto.expedientejuridico.ExpedienteJuridicoDTO;

/**
 * @author Israel
 * @version 1.0
 * @created 06-ago.-2015 10:30:24 a. m.
 */
@Local
public interface ExpedienteJuridicoDAO {

	public List<ExpedienteJuridico> buscarExpedientes(ExpedienteJuridicoDTO expedienteJuridicoDTO);

}