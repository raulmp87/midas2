package mx.com.afirme.midas2.dao.siniestros.catalogo;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;
import mx.com.afirme.midas2.service.siniestros.catalogo.serviciosiniestro.ServicioSiniestroService.ServicioSiniestroFiltro;

/**
 * Acceso a datos para servicio siniestro (ajustadores, abogados, etc)
 * @author Softnet01
 *
 */
@Local
public interface ServicioSiniestroDao {

	/**
	 * Retornar un listado de servicios (ajustador, abogados) para un filtro dado en forma de un query de objetos unmanaged para busquedas
	 * @param filtro
	 * @return
	 */
	public List<ServicioSiniestro> buscar(ServicioSiniestroFiltro filtro);
	
	/**
	 * Retornar un servicio(ajustador, abogados) 
	 * @param filtro
	 * @return
	 */
	public ServicioSiniestro getServicioSiniestroById(Long id);
	
	public ServicioSiniestro findById(Long id);
}
