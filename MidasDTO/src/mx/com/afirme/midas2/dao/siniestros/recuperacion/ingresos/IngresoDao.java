package mx.com.afirme.midas2.dao.siniestros.recuperacion.ingresos;
import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.siniestros.ListarIngresoDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos.DepositoBancarioDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos.MovimientoAcreedorDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos.MovimientoManualDTO;
import mx.com.afirme.midas2.util.MidasException;
@Local
public interface IngresoDao {

	/**
	 * Metodo que debera invocar una Interfaz con Seycos para realizar la busqueda de Depositos Bancarios en base a ciertos filtros.
	 * Utilizar StoreProcedureHelper para hacer la invocacion.
	 * Mapear el resultado creado objetos de tipo DepositoBancarioDTO
	 * @param filtroDeposito
	 * @return
	 */
	public List<DepositoBancarioDTO> buscarDepositosBancarios(DepositoBancarioDTO filtroDeposito, Boolean esConsulta);
	
	/**
	 * Invocar al procedure MIDAS.PKGSIN_RECUPERACIONES.buscarIngresos
	 * Utilizar StoreProcedureHelper para hacer la invocacion.
	 * Mapear el resultado creado objetos de tipo ListarIngresoDTO
	 * @param filtroIngreso
	 * @return
	 */
	public List<ListarIngresoDTO> buscarIngresos(ListarIngresoDTO filtroIngreso);
	
	///
	

	/**
	 * <b>SE AGREGA EN DISEÑO APLICAR INGRESO MANUAL</b>
	 * <b>
	 * </b>Método que actualiza el Saldo del REFUNIC
	 * Invocar a la Interfaz de SEYCOS <b><i>SEYCOS.PKG_INT_MIDAS_SIN.
	 * STPAPLICAREFUNIC</i></b>
	 * <b>
	 * </b>
	 * 
	 * @param refunic
	 * @param monto
	 * @param descripcion
	 */
	public void aplicaRefunic(String refunic, BigDecimal monto, String descripcion) throws MidasException;

	/**
	 * <b>SE AGREGA EN DISEÑO BUSCAR DEPOSITOS BANCARIOS</b>
	 * <b>
	 * </b>Método que deberá invocar una Interfaz con Seycos para realizar la búsqueda
	 * de Depósitos Bancarios en base a ciertos filtros.
	 * 
	 * 
	 * Utilizar StoreProcedureHelper para hacer la invocación a la Interfaz de Seycos
	 * seycos<font color="#0000ff">.</font>pkg_int_midas_sin<font color="#0000ff">.
	 * </font>clistarefunic.
	 * 
	 * 
	 * Mapear el resultado creado objetos de tipo DepositoBancarioDTO
	 * 
	 * @param filtro
	 * @param String
	 * @param Boolean
	 */
	public List<DepositoBancarioDTO> buscarDepositosBancarios(DepositoBancarioDTO filtro,String depositosConcat ,Boolean esConsulta );



	/**
	 * <b>SE AGREGA EN DISEÑO BUSCAR MOVIMIENTOS ACREEDORES</b>
	 * 
	 * Invocar al procedure MIDAS.PKGSIN_RECUPERACIONES.buscarMovimientosAcreedores
	 * Utilizar StoreProcedureHelper para hacer la invocación.
	 * 
	 * Mapear el resultado creado objetos de tipo MovimientoAcreedorDTO
	 * 
	 * @param filtro
	 * @param String
	 * @param Boolean
	 */
	public List<MovimientoAcreedorDTO> buscarMovimientosAcreedores(MovimientoAcreedorDTO filtro, String movAcreedoresConcat ,Boolean esConsulta);
	
	/**
	 * metodo para buscar movimientos acreedores por id de cuenta contable
	 * @param cuentaId
	 * @return
	 */
	public List<MovimientoAcreedorDTO> buscarMovimientosAcreedoresPorCuenta(Long cuentaId);

	/**
	 * <b>SE AGREGA EN DISEÑO DE APLICACION DE INGRESO MANUAL.</b>
	 * 
	 * Invocar al proceduire MIDAS.PKGSIN_RECUPERACIONES.contabilizaAplicacionIngreso
	 * 
	 * @param aplicacionId
	 */
	public void contabilizaAplicacionIngreso(Long aplicacionId) throws MidasException;

	public Boolean contabilizaCancelacion(Long aplicacionCobranzaId) ;
	
	public List<MovimientoManualDTO> buscarMovimientosManuales(MovimientoManualDTO filtro, String movimientosConcat, Boolean esConsulta);
	
}
