package mx.com.afirme.midas2.dao.siniestros.catalogo.oficina;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;

@Local
public interface CatalogoSiniestroOficinaDao {
	
	public List<EstadoMidas> estadosDisponiblesPorPais(String paisId, Long oficinaId);
}
