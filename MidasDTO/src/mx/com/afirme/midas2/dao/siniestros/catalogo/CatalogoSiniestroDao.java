package mx.com.afirme.midas2.dao.siniestros.catalogo;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService.CatalogoFiltro;

@Local
public interface CatalogoSiniestroDao {
	
	
	public <E extends Entidad, K extends CatalogoFiltro>  List<E> buscar( Class<E> entidad, K filtro );
	
	public <E extends Entidad, K extends CatalogoFiltro, T extends Object>  List<E> buscar( Class<E> entidad, K filtro, String ordenPor);
}
