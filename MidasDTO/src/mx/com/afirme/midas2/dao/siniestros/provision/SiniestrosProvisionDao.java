package mx.com.afirme.midas2.dao.siniestros.provision;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.provision.MovimientoProvisionSiniestros;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionCompania;

@Local
public interface SiniestrosProvisionDao {
	
	public List<MovimientoProvisionSiniestros> obtenerMovimientosProvisionadosPorPreporte(Long reporteId,MovimientoProvisionSiniestros.Origen origen);
	
	public BigDecimal obtenerSumaGruasPorEstimacion( Long estimacionId );
	
	public BigDecimal obtenerSumaRecuperacionesRecuperada( Long estimacionId );
	
	public RecuperacionCompania obtenerRecuperacionCiaPorEstimacion( Long estimacionId );
	
	public BigDecimal obtenerSumaPiezas( Long estimacionId );

}
