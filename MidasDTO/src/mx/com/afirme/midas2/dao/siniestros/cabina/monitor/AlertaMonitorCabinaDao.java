package mx.com.afirme.midas2.dao.siniestros.cabina.monitor;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.cabina.monitor.AlertaMonitorCabina;

@Local
public interface AlertaMonitorCabinaDao {

	public List<AlertaMonitorCabina> obtenerAlertas();
	
	public AlertaMonitorCabina obtenerAlerta(Long alertaId);

	public List<AlertaMonitorCabina> obtenerAlertasPorFiltros(AlertaMonitorCabina alertaMonitorCabina);
	
	public List<AlertaMonitorCabina> obtenerAlertasPorRol(String rol);

}
