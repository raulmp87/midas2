package mx.com.afirme.midas2.dao.siniestros.cabina.reportecabina;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.personadireccion.PersonaDireccionMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.dto.siniestros.BitacoraEventoSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.ReporteSiniestroRoboDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.ReporteSiniestroDTO;

@Local
public interface ReporteCabinaDao  extends Dao<Long, ReporteCabina>{
	
	/**
	 * 
	 * @param reporteCabina
	 * @return
	 */
	public String obtenerConsecutivoReporte( ReporteCabina reporteCabina );
	
	public List<BitacoraEventoSiniestroDTO> buscarEventos(BitacoraEventoSiniestroDTO bitacoraEventoSiniestroDTO, Long idReporte);
	public Long obtenerIncisoReporteCabina(Long id);
	
	/**
	 * Retornar el numero de registros que contiene la busqueda
	 * @param filtro
	 * @return
	 */
	public Long contarReporteSiniestro(ReporteSiniestroDTO filtro);
	
	public List<ReporteSiniestroDTO> buscarReporteSiniestro( ReporteSiniestroDTO filtro);
	/**
	 * Obtiene Reportes de Siniestrosa de cobertura tipo Robo RT con estimacionCobertura
	 * @param ReporteSiniestroRoboDTO
	 */
	public List<ReporteSiniestroRoboDTO> buscarReportesRobo(ReporteSiniestroRoboDTO reporteSiniestroDTO);	
	

	
	public List<PersonaDireccionMidas> obtenerDireccionPersona(Long idPersona);
	
	public String buscarNombreAseguradoAutos(BigDecimal idToPoliza, Integer numeroInciso, Date fechaValidez);
}
