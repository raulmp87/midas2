package mx.com.afirme.midas2.dao.siniestros.cabina.reportecabina;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.dto.siniestros.PaseAtencionExportacionDTO;
import mx.com.afirme.midas2.dto.siniestros.PaseAtencionSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.depuracion.CoberturaSeccionSiniestroDTO;

@Local
public interface EstimacionCoberturaSiniestroDao {
	
	public void actualizaEdicionDeEstimaciones(Long reporteCabinaId, String tipoSiniestro, String tipoResponsabilidad, String terminoAjuste); 
	
	public List<PaseAtencionSiniestroDTO> buscarPasesAtencion(PaseAtencionSiniestroDTO filtro);
	
	public List<PaseAtencionExportacionDTO> buscarPasesAtencionExportacion(PaseAtencionSiniestroDTO filtro);	
	
	public List<CoberturaSeccionDTO> obtenerCoberturasPorSeccion(Long idSeccion);	
	
	public List<CoberturaSeccionSiniestroDTO> obtenerCoberturasSiniestrosPorSeccion(Long idToSeccion);	
	
	public Integer obtenerSecuenciaPaseAtencion (Long idReporteCabina);
	
	public Integer obtenerSecuenciaPaseDeCobertura(Long idCoberturaReporte);

	public List<CatValorFijo> obtenerTipoDePaseMenosSoloRegistro();
	
	@Deprecated
	public List<CatValorFijo> obtenerTipoDePasePorTipoEstimacion(String codigoTipoEstimacion,
			String codigoTipoResponsabilidad);
	
	public List<CatValorFijo> obtenerTipoDePasePorTipoEstimacion(String codigoTipoEstimacion,
			String codigoTipoResponsabilidad, String codigoCausaSiniestro, String codigoTerminoAjuste);
	
	public List<EstimacionCoberturaReporteCabina> pasesRCVehiculoAnteriores(Long estimacionId, String numeroSerie);

	
	
	
		
	
	
	
	
	
	
	
	
	

	
	
	
	

	
	
	
	
}
