package mx.com.afirme.midas2.dao.siniestros.liquidacion;


import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.indemnizacion.RecepcionDocumentosSiniestro;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.LiquidacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.pagos.OrdenPagoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionProveedor;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.DetalleOrdenExpedicionDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.ImpresionOrdenExpedicionDTO;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.FacturaLiquidacionDTO;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.LiquidacionSiniestroRegistro;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.ParamAutLiquidacionDTO;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.ParamAutLiquidacionRegistro;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService.PrestadorServicioRegistro;
import mx.com.afirme.midas2.service.siniestros.liquidacion.LiquidacionSiniestroService.LiquidacionSiniestroFiltro;

/**
 * Definicion de m�todos del m�dulo de Pagos
 * @author usuario
 * @version 1.0
 * @updated 22-sep-2014 03:59:37 p.m.
 */
@Local
public interface LiquidacionSiniestroDao {
	/**
	 * Get data for pdf's prints for liquidations providers. 
	 * @param idLiquidacion 
	 */
	public ImpresionOrdenExpedicionDTO getDatosImportesLiquidacionProv(Long idLiquidacion);
	
	/**
	 * Get resume for liquidation's provider like siniestro, ordenPago, concepto. This into liquidation's PDF print. 
	 * @param idLiquidacion 
	 */
	public List<DetalleOrdenExpedicionDTO> getResumenLiquidacionProveedor(Long idLiquidacion);
	
	public List<LiquidacionSiniestroRegistro> buscarLiquidacionDetalle(LiquidacionSiniestroFiltro filtro);
	
	public List<DocumentoFiscal> buscarFacturasLiquidacion(Long idLiquidacion);
	
	public List<FacturaLiquidacionDTO> buscarOrdenesPago(Long idLiquidacion);	
	
	public Long getNumeroLiquidacion();
	
	public LiquidacionSiniestro asociaDesasociaFacturas(String listaFacturas, Long idLiquidacion, Short tipoOperacion);
	
	List<PrestadorServicioRegistro> buscarProveedoresLiquidacion();
	
	public List<ParamAutLiquidacionRegistro > buscarParametrosAutorizacion (ParamAutLiquidacionDTO filtro);
	
	public Long obtenerSiguienteNumeroDeLiquidacion();

	public Boolean validarUsuarioEsAutorizador(Long idLiquidacion, Long idUsuario, Date fechaVigencia);
	
	public Short cancelarLiquidacion(Long idLiquidacion, Short tipoCancelacion);
	
	public List<OrdenPagoSiniestro> buscarOrdenesPagoIndemnizacion(Long idLiquidacion);
	
	public List<RecepcionDocumentosSiniestro> obtenerInformacionBancariaIndemnizacionPorOrdenesPago(List<Long> listaOrdenes);
	
	/**
	 * Crear un JPQL que regrese la lista de Unmanage Objects de tipo Recuperacion que
	 * cumplan con las siguientes condiciones:
	 * <ul>
	 * 	<li>Recuperacion de tipo "<font color="#2a00ff">PRV</font>"</li>
	 * 	<li>Recuperaciíon estatus "<font
	 * color="#0000c0"><i>PENDIENTE</i></font>"</li>
	 * 	<li>Recupeación  que no este ligada a ninguna liquidación</li>
	 * 	<li>Recuperacion Medio "<font color="#0000c0"><i>NOTACRED</i></font>"</li>
	 * 	<li>recuperacion.ordenCompra.<font color="#0000c0">idBeneficiario ==
	 * idProveedor</font></li>
	 * </ul>
	 * 
	 * @param idProveedor
	 */
	public List<RecuperacionProveedor> obtenerRecuperacionesPendientesPorAsignar(Integer idProveedor);
	
	public LiquidacionSiniestro obtenerLiquidacionPorEstimacion(Long idEstimacion);
	
	public LiquidacionSiniestro recalculaLiquidacion(Long idLiquidacion);
}