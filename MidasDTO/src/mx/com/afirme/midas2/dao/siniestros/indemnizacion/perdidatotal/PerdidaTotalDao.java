package mx.com.afirme.midas2.dao.siniestros.indemnizacion.perdidatotal;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.indemnizacion.IndemnizacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.RolesIndemnizacionAutorizacion;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.PerdidaTotalFiltro;

@Local
public interface PerdidaTotalDao {

	/**
	 * Busca las ordenes de compra y las filtra dependiendo de los parametros que se le pasen 
	 * @param filtroPT
	 * @return
	 */
	public List<OrdenCompra> buscarOrdenesCompraDePerdidaTotal(PerdidaTotalFiltro filtroPT);
	
	/**
	 * Obtiene los diferentes roles que no tienen la configuracion de autorizacion final de la tabla de roles de autorizacion de indemnizacion
	 * @return
	 */
	public List<RolesIndemnizacionAutorizacion> obtenerListaRolesAutorizacionSinAutorizacionFinal();
	
	
	/**
	 * Retornar indemnizacion asociada a un reporte de robo para obener los datos de la perdida tota y orden de compra generada
	 * @param roboId
	 * @return
	 */
	public IndemnizacionSiniestro obtenerOrdenCompraRobo(Long roboId);
	
	/**
	 * Busca la Indemnizacion activa de la estimacion proporcionada
	 * @param idEstimacion
	 * @return
	 */
	public IndemnizacionSiniestro obtenerIndemnizacionActivaPorEstimacion(Long idEstimacion);
	
	/**
	 * Nuevo metodo de busqueda de indemnizaciones para optimizar la busqueda
	 * @param filtroPT
	 * @return
	 */
	public List<PerdidaTotalFiltro> buscarIndemnizaciones(PerdidaTotalFiltro filtroPT);

}
