package mx.com.afirme.midas2.dao.siniestros.recuperacion;



/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 19-may-2015 12:37:36 p.m.
 */
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.catalogos.banco.ConfigCuentaMidas;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.IndemnizacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.EstatusRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionProveedor;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionSalvamento;
import mx.com.afirme.midas2.dto.siniestros.ReferenciaBancariaDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.RecuperacionDTO;
import mx.com.afirme.midas2.util.MidasException;


@Local
public interface RecuperacionDao {

	

	/**
	 * 
	 * @param filtro
	 */
	public List<RecuperacionDTO> buscarRecuperaciones(RecuperacionDTO filtro);
	

	/**
	 * Desactiva todas las referencias bancarias de una recuperacion. Se utiliza cuando se genera un nuevo set de referencias.
	 * @param recuperacionId
	 */
	public void desactivarReferenciasBancarias(Long recuperacionId);
	

	/**
	 * 
	 * Método que genera las referencias bancarias de una Recuperación
	 * 
	 * @param recuperacionId
	 */
	public String generarReferenciaBancaria(String referenciaPreliminar, ConfigCuentaMidas cuenta, Date fecha, BigDecimal importe);

	/**
	 * 
	 * Método que regresa las Cuentas Bancarias asociadas a Ingresos.
	 */
	public List<ReferenciaBancariaDTO> obtenerCuentasBancarias();

	public IndemnizacionSiniestro obtenerDeterminacionPorRecuperacion(Long idRecuperacion);
	
	public List<ConfigCuentaMidas> listadoCuentasBancarias();
	
	public BigDecimal obtenerMontoRecuperacionSalvamento(Long siniestroId, EstatusRecuperacion estatus) ;
	
	public BigDecimal obtenerIvaRecuperacionSalvamento(Long siniestroId,EstatusRecuperacion estatus);
	
	public void invocaProvision(Long idRecuperacion)  throws MidasException ;
	
	/**
	 * El objetivo del Caso de Uso es enviar una notificación diario al Comprador del Salvamento que se le haya autorizado 
	 * una Prórroga y siempre y cuando no se haya recibido el pago correspondiente. 
	 * La notificación deberá ser enviada desde las 24 primeras horas posteriores de la Autorización de la Prórroga 
	 * hasta las la fecha límite de Prórroga en un lapso diferenciado entre cada notificación de 24 horas. 
	 * Haciendo un alto de envió de la notificación al identificar el Pago del Comprador.
	 * @return
	 */
	public List<RecuperacionSalvamento> obtenerVentasPorConfirmarIngresoProrroga();
	
	public List<RecuperacionProveedor> obtenerRecuperacionesProveedorPendientes(Long estimacionId) ;
	
	/**
	 *  Lista de Recuperaciones que cuentan con referencias bancarias ligadas a un reporte cabina
	 * @param idReporteCabina
	 */
	public List<RecuperacionDTO> buscarReferenciasBancarias(Long idReporteCabina);
	
}