package mx.com.afirme.midas2.dao.siniestros.recuperacion.ingresos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos.IngresoDevolucionDTO;
import mx.com.afirme.midas2.service.siniestros.recuperacion.ingresos.IngresoDevolucionService.IngresoDevolucionFiltro;

@Local
public interface IngresoDevolucionDao {
	public static final Short RESULTADO_OK = 0;
	public static final Short RESULTADO_NOEXISTE = -1;
	public static final Short RESULTADO_CHEQUEIMPRESO = -2;
	public static final Short RESULTADO_APLICADO = 1;
	public static final Short RESULTADO_ERROR = 2;
	
	public List<IngresoDevolucionDTO> buscar(IngresoDevolucionFiltro filtro);
	public Short solicitarCancelacionCheque(Long solicitudId);

}