package mx.com.afirme.midas2.dao.siniestros.cabina;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.siniestros.cabina.AjustadorEstatus;
import mx.com.afirme.midas2.service.siniestros.cabina.AjustadorEstatusService.ReporteEstatusDTO;

@Local
public interface AjustadorEstatusDao extends Dao<Long, AjustadorEstatus>{

	/**
	 * Obtiene la ultima ubicacion registrada para el Ajustador con la id correpondiente
	 * si no se ha registrado la posicion del ajustador nunca, este metodo devuelve null
	 * @param idAjustador
	 * @return
	 */
	public AjustadorEstatus obtenerUltimoAjustadorEstatus(Long idAjustador);
	
	/**
	 * Obtener ultimo estatus reportado para un ajustador en la fecha dada. Si la fecha es nula se toma el dia actual.
	 * @param idAjustador
	 * @param fecha
	 * @return
	 */	
	public AjustadorEstatus obtenerUltimoAjustadorEstatus(Long idAjustador, Date fecha);
	
	/**
	 * Obtener ultimo estatus reportado para cada ajustador de una oficina en particular que haya generado un reporte en la fecha seleccionada.
	 * Si la fecha es nula se toma el dia actual.
	 * @param oficinaId
	 * @param fecha
	 * @return
	 */
	public List<AjustadorEstatus> obtenerEstatusAjustadores(Long oficinaId, Date fecha);
	
	/**
	 * Obtener el historico de un ajustador desde la fecha de entrada.
	 * @param idAjustador
	 * @param desde
	 * @return
	 */
	public List<AjustadorEstatus> obtenerHistoricoPorAjustador(Long idAjustador, Date desde);
	
	/**
	 * Listar reportes en proceso que tienen coordenadas definidas y que no tiene una cita programada
	 * @param idOficina
	 * @return
	 */
	public List<ReporteEstatusDTO> obtenerReportesSinTerminacion(Long idOficina);
	
	/**
	 * Obtener un listado de reportes que el ajustador tiene asignados. 
	 * Todos los parametros son opcionales pudiendo ser <code>null</code>. Aquellos que se envien se incluiran en la busqueda 
	 * @param ajustadorId
	 * @param reporteId
	 * @param oficinaId
	 * @return resumen de los reportes que cumplen con los parametros, que estan en tramite y que no tienen fecha de terminacion
	 */
	public List<ReporteEstatusDTO> obtenerReportesAsignados(Long ajustadorId, Long reporteId, Long oficinaId);
	
	
	/**
	 * Eliminar historico de ajustadores de la fecha dada hacia atras
	 * @param date fecha limite superior del borrado (hasta)
	 * @param int, numero de registros eliminados
	 */
	public int eliminarHistoricoAjustadores(Date date);
	
	
}
