package mx.com.afirme.midas2.dao.siniestros.emisionFacturas;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.emisionFacturas.EmisionFactura;
import mx.com.afirme.midas2.dto.emisionFactura.FacturaResultadoDTO;
import mx.com.afirme.midas2.dto.emisionFactura.FiltroFacturaDTO;

@Local
public interface EmisionFacturaDao {
	
	public class Respuesta{
		
		private String mensaje;

		public String getMensaje() {
			return mensaje;
		}

		public void setMensaje(String mensaje) {
			this.mensaje = mensaje;
		}
	}
	
	public List<FacturaResultadoDTO> buscarFacturas(FiltroFacturaDTO filtroFacturaDTO);
	
	public void insertarInformacion(EmisionFactura emisionFactura);
	
	public void priorizarInformacion(EmisionFactura emisionFactura);
	
	public List<String> seEncuentraEnTablaDeError(String folioFactura);
	
	public Boolean seGeneraFacturaConExito(EmisionFactura emisionFactura);
	
	public List<EmisionFactura> validaCancelacionesEnWFactura();
	
	public void insertarRegistroCancelacion(String folioFactura); 
		
}
