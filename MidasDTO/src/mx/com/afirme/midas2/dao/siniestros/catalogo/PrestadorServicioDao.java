package mx.com.afirme.midas2.dao.siniestros.catalogo;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.TipoPrestadorServicio;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.gastoajuste.PrestadorGastoAjusteDTO;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService.PrestadorServicioFiltro;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService.PrestadorServicioRegistro;

@Local
public interface PrestadorServicioDao {

	/**
	 * @param nombre
	 * @param tipoPrestador
	 * @return List<PrestadorGastoAjusteDTO>
	 */
	public List<PrestadorGastoAjusteDTO> buscarPrestadorPorNombre(String nombre, String tipoPrestador);
	public TipoPrestadorServicio buscarTipoPrestadorbyNombre(String nombre);
	public Map<String, String> consultarPrestadoresGastoAjuste();
	
	/**
	 * Buscar prestadores de servicio basados en filtro. Este metodo se realizó debido a que el metodo actual de buscar en el service era lento
	 * por la cantidad de registros y las relaciones entre prestador, estados, tipos de prestador.
	 * @param filtro
	 * @return
	 */
	public List<PrestadorServicioRegistro> buscar(PrestadorServicioFiltro filtro);
}
