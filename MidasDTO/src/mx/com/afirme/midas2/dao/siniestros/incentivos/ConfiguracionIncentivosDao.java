package mx.com.afirme.midas2.dao.siniestros.incentivos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.incentivos.ConfiguracionIncentivos;
import mx.com.afirme.midas2.dto.siniestros.incentivos.IncentivoAjustadorDTO;


@Local
public interface ConfiguracionIncentivosDao {
	
	public List<ConfiguracionIncentivos> obtenerConfiguracionesParaBandejaAutorizacion();

	public List<IncentivoAjustadorDTO> obtenerIncentivoPreliminar(Long configuracionId, String codigoUsuario);
	
	public List<IncentivoAjustadorDTO> obtenerIncentivoDiaInhabil(Long configuracionId);
	
	public List<IncentivoAjustadorDTO> obtenerIncentivoEfectivo(Long configuracionId);
	
	public List<IncentivoAjustadorDTO> obtenerIncentivoCompanias(Long configuracionId);
	
	public List<IncentivoAjustadorDTO> obtenerIncentivoSIPAC(Long configuracionId);
	
	public List<IncentivoAjustadorDTO> obtenerIncentivoRechazos(Long configuracionId);
	
	public List<IncentivoAjustadorDTO> obtenerIncentivosAjustador(Long configuracionId);
	
	public void preRegistroIncentivos(Long configuracionId, String incentivosConcat);

}
