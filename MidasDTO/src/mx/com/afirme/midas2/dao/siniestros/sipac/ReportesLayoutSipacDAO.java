package mx.com.afirme.midas2.dao.siniestros.sipac;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.domain.siniestros.sipac.LayoutSipac;
import mx.com.afirme.midas2.domain.siniestros.sipac.LoteLayoutSipac;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatSubMarcaVehiculo;

public interface ReportesLayoutSipacDAO {
	
	public InputStream generarArchivoTxt(List<LayoutSipac> list);
	
	public InputStream generarTxtByLote(Long idLote);
	
	public List<LoteLayoutSipac> obtenerLotesSipac();
	
	public List<LoteLayoutSipac> obtenerLotesPendientes();
	
	public List<LayoutSipac> buscarReportesPend(Date fechaInicial, Date fechaFinal);
	
	public void saveLotes(LoteLayoutSipac entity);
	
	public void updateLote(LoteLayoutSipac entity);
	
	public LoteLayoutSipac findLoteById(Long idLote);
	
	public void updateLayout(LayoutSipac entity);
	
	public List<LayoutSipac> findLayoutByIdLote(Long idLote);
	
	public List<CatSubMarcaVehiculo> findMarcaTipoTercero(String estiloVehiculo);
}
