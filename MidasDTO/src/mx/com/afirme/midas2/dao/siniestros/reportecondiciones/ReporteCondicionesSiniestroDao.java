package mx.com.afirme.midas2.dao.siniestros.reportecondiciones;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.siniestros.reportecondiciones.ReporteCondicionesSiniestroDTO;

@Local
public interface ReporteCondicionesSiniestroDao {

	public List<ReporteCondicionesSiniestroDTO> obtenerCondicionesPolizaSiniestro(String numeroPoliza, Short tipoPoliza, Long idGerencia, 
												Long idCondicionEspecial, Date fechaSiniestroIni, Date fechaSiniestroFin);
	
	public List<ReporteCondicionesSiniestroDTO>  obtenerCondicionesSiniestro(String numeroPoliza, Short tipoPoliza, Long idGerencia, 
			Long idCondicionEspecial, Date fechaSiniestroIni, Date fechaSiniestroFin);
}
