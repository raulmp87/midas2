package mx.com.afirme.midas2.dao.vida;

import javax.ejb.Local;

import mx.com.afirme.midas.danios.reportes.reportercs.caducidad.CaducidadesDTO;
import mx.com.afirme.midas.danios.reportes.reportercs.calificaciones.CalificacionesREASDTO;
import mx.com.afirme.midas2.domain.vida.ValoresRescate;

@Local
public interface ArchivosCompDao {
	
	void insertaCaducidad(CaducidadesDTO caducidadesDTO);
	 
	void insertaValoresRescate(ValoresRescate valorRescate);
	
	int actualizaCalReas(CalificacionesREASDTO calificacionesREASDTO);
	
	void borraTablasVida(int fechaCorte, int nArchivo);  
	
}
