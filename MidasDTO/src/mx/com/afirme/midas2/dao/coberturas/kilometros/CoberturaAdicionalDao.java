package mx.com.afirme.midas2.dao.coberturas.kilometros;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.coberturas.kilometros.SolicitudCoberturaAdicionalDTO;

@Local
public interface CoberturaAdicionalDao {

	public void save(SolicitudCoberturaAdicionalDTO entity);
	
	public SolicitudCoberturaAdicionalDTO update(SolicitudCoberturaAdicionalDTO entity);
	
	public SolicitudCoberturaAdicionalDTO findBySolicitud(BigDecimal numeroSolicitud);
	
	public List<SolicitudCoberturaAdicionalDTO> findByPoliza(BigDecimal idToPoliza);
	
	public List<SolicitudCoberturaAdicionalDTO> findByProperty(String propertyName, final Object propertyValue);
}
