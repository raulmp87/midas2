package mx.com.afirme.midas2.dao.agtSaldoMovto;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.agtSaldoMovto.AgtSaldo;
import mx.com.afirme.midas2.dto.agtSaldoMovto.AgtSaldoView;
import mx.com.afirme.midas2.util.MidasException;

@Local
public interface AgtSaldoDao extends EntidadDao {

	public  List<AgtSaldo> findByFilter(AgtSaldo agtSaldo) throws Exception;
	
	public  List<AgtSaldoView> findByFilterview(AgtSaldo agtSaldo) throws Exception;
	
	public AgtSaldoView obtenerMovimientoSaldoPorAnioMesYAgente(AgtSaldo agtSaldo) throws MidasException;
	
	public Long actualizarSaldosMesAbierto(Long idAgente, Long anio) throws Exception;
}
