package mx.com.afirme.midas2.validators;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.util.StringUtil;
import mx.com.afirme.midas2.util.VINValidator;


public class NumSerieValidador{
	
	private static final NumSerieValidador INSTANCE = 
	    new NumSerieValidador();

	 public static NumSerieValidador getInstance(){
		    return NumSerieValidador.INSTANCE;
	 }
	 
	 //prevents instance
	 private NumSerieValidador(){}
  
  /**
   * Validates a serial number. The method validates that length must be 
   * between 8 and 17 characters. Validates that serial number only contains 
   * accepted characters. Applies VIN validation for 17 characters 
   * length numbers.
   * @param serialNumber
   *
   */
	public List<String> validate(String serialNumber){
		List<String> errors = new ArrayList<String>();		
		if(StringUtil.isEmpty(serialNumber)){
    		errors.add("El n\u00famero de serie esta vac\u00edo");
    		return errors;
    	}	    
		try{
			serialNumber = serialNumber.trim().toUpperCase();    
			validateLength(serialNumber);
			validateCharacters(serialNumber);
		    if(!VINValidator.getInstance().isValid(serialNumber)){
		       throw new RuntimeException("El n\u00famero de serie es inv\u00e1lido. El noveno d\u00edgito deber\u00eda ser un " + 
		    		   VINValidator.getInstance().getVerificationDigit(serialNumber) +
		    		   " para conformar uno v\u00e1lido.");
		    }
		}catch(Exception ex){
			errors.add(ex.getMessage());
		}  
		return errors;      
	}

	private void validateLength(String serialNumber){    
	    if(serialNumber.length() < 8 || serialNumber.length() > 17){
	      throw new RuntimeException("El n\u00famero de serie debe estar entre 8 y 17 caracteres");
	    }
	  }
	  
	  private void validateCharacters(String serialNumber){
	    for ( int i = 0; i < serialNumber.length(); ++i ){
	    	char c = serialNumber.charAt( i );
	    	int ascii = (int) c;
	    	if(((ascii < 48 || ascii > 90) && (ascii > 57 || ascii < 65)) || 
	    			(ascii == 73 || ascii == 79 || ascii == 81)){
		        throw new RuntimeException("El n\u00famero de serie contiene caracteres inv\u00e1lidos");
	      }
	    }     
	  }
	  
	  /**
	   * Se agrega metodo para la carga masiva de servicio publico
	   * Se remueve la validacion del VINValidator 
	   * (Cuando sea el unico error, el VIN validator se debe ignorar, pero como se valida al final, siempre es el unico error)
	   * (ver metodo validate(String serialNumber) original)
	   * @param serialNumber
	   * @return
	   */
	  public List<String> validateCargaMasivaSP(String serialNumber){
			List<String> errors = new ArrayList<String>();		
			if(StringUtil.isEmpty(serialNumber)){
	    		errors.add("El n\u00famero de serie esta vac\u00edo");
	    		return errors;
	    	}	    
			try{
				serialNumber = serialNumber.trim().toUpperCase();    
				validateLength(serialNumber);
				validateCharacters(serialNumber);
			}catch(Exception ex){
				errors.add(ex.getMessage());
			}  
			return errors;      
		}
}
