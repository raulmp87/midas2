package mx.com.afirme.midas2.clientesapi.dto;

import java.io.Serializable;
import java.math.BigDecimal;
/**
 * 
 * @author mario.dominguez
 *Clase para abstraer la estructura cooporativa de una empresa
 */
public class EstructuraCoorporativaDTO implements Serializable {


	private static final long serialVersionUID = 1L;

	private Long id;
    private String nombre;
    private String cveNacionalidad;
    private BigDecimal porcentajeCapital;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCveNacionalidad() {
        return cveNacionalidad;
    }

    public void setCveNacionalidad(String cveNacionalidad) {
        this.cveNacionalidad = cveNacionalidad;
    }

    public BigDecimal getPorcentajeCapital() {
        return porcentajeCapital;
    }

    public void setPorcentajeCapital(BigDecimal porcentajeCapital) {
        this.porcentajeCapital = porcentajeCapital;
    }
}

