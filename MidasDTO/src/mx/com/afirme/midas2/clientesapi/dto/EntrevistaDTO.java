package mx.com.afirme.midas2.clientesapi.dto;

import java.io.Serializable;
import java.math.BigDecimal;
/**
 * 
 * @author mario.dominguez
 *Clase para abstraer la entrevista de cliente unico
 */
public class EntrevistaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private BigDecimal primaEstimada;
	private Integer moneda;
	private Boolean pep;
	private String parentesco;
	private String nombrePuesto;
	private String periodoFunciones;
	private Boolean cuentaPropia;
	private String nombreRepresentado;
	private Boolean documentoRepresentacion;
	private String numeroSerieFiel;
	private String folioMercantil;
	private BigDecimal idToCotizacion;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getPrimaEstimada() {
		return primaEstimada;
	}

	public void setPrimaEstimada(BigDecimal primaEstimada) {
		this.primaEstimada = primaEstimada;
	}

	public Integer getMoneda() {
		return moneda;
	}

	public void setMoneda(Integer moneda) {
		this.moneda = moneda;
	}

	public Boolean isPep() {
		return pep;
	}

	public void setPep(Boolean pep) {
		this.pep = pep;
	}

	public String getParentesco() {
		return parentesco;
	}

	public void setParentesco(String parentesco) {
		this.parentesco = parentesco;
	}

	public String getNombrePuesto() {
		return nombrePuesto;
	}

	public void setNombrePuesto(String nombrePuesto) {
		this.nombrePuesto = nombrePuesto;
	}

	public String getPeriodoFunciones() {
		return periodoFunciones;
	}

	public void setPeriodoFunciones(String periodoFunciones) {
		this.periodoFunciones = periodoFunciones;
	}

	public Boolean isCuentaPropia() {
		return cuentaPropia;
	}

	public void setCuentaPropia(Boolean cuentaPropia) {
		this.cuentaPropia = cuentaPropia;
	}

	public String getNombreRepresentado() {
		return nombreRepresentado;
	}

	public void setNombreRepresentado(String nombreRepresentado) {
		this.nombreRepresentado = nombreRepresentado;
	}

	public Boolean isDocumentoRepresentacion() {
		return documentoRepresentacion;
	}

	public void setDocumentoRepresentacion(Boolean documentoRepresentacion) {
		this.documentoRepresentacion = documentoRepresentacion;
	}

	public String getNumeroSerieFiel() {
		return numeroSerieFiel;
	}

	public void setNumeroSerieFiel(String numeroSerieFiel) {
		this.numeroSerieFiel = numeroSerieFiel;
	}

	public String getFolioMercantil() {
		return folioMercantil;
	}

	public void setFolioMercantil(String folioMercantil) {
		this.folioMercantil = folioMercantil;
	}

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	
}
