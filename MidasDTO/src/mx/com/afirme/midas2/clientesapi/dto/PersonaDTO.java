package mx.com.afirme.midas2.clientesapi.dto;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
/**
 * 
 * @author mario.dominguez
 *
 */
public class PersonaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Identifier, if new registration is not required, to update the
	 * registration if required
	 */
	private Long id;
	/**
	 * Required value PF=Fisica or PM=Moral
	 */
	private String tipoPersona;
	/**
	 * Registro Federal de Contribuyentes
	 */
	private String rfc;
	/**
	 * Country code of nationality <br>
	 * If you do not have the data, you can use the code country of birth
	 */
	private String cveNacionalidad;
	/**
	 * Control Field Not required are obtained by the system
	 */
	private String usuarioCreacion;
	/**
	 * Control Field Not required are obtained by the system
	 */
	private String fechaCreacion;
	/**
	 * Control Field Not required are obtained by the system
	 */
	private String usuarioModificacion;
	/**
	 * Control Field Not required are obtained by the system
	 */
	private String fechaModificacion;
	/**
	 * Required if the type of person is fisica
	 */
	private PersonaFisicaDTO fisica;
	/**
	 * Required if the type of person is moral
	 */
	private PersonaMoralDTO moral;
	/**
	 * the address object is required
	 */
	private DomicilioDTO domicilio;
	/**
	 * the address object is required
	 */
	private DomicilioDTO domicilioFiscal;
	/**
	 * List of addresses, read only
	 */
	private List<DomicilioDTO> listDomicilios;
	/**
	 * Contact information
	 */
	private ComunicacionDTO comunicacion;
	/**
	 * List of documents
	 */
	private List<DocumentacionDTO> listDocumentacion;

	public PersonaDTO() {
		this.fisica = new PersonaFisicaDTO();
		this.moral = new PersonaMoralDTO();
		this.domicilio = new DomicilioDTO();
		this.domicilioFiscal = new DomicilioDTO();
		this.comunicacion = new ComunicacionDTO();
		this.listDocumentacion = new LinkedList<DocumentacionDTO>();
	}

	public PersonaDTO(Long id) {
		this.id = id;
		this.fisica = new PersonaFisicaDTO();
		this.moral = new PersonaMoralDTO();
		this.domicilio = new DomicilioDTO();
		this.domicilioFiscal = new DomicilioDTO();
		this.comunicacion = new ComunicacionDTO();
		this.listDocumentacion = new LinkedList<DocumentacionDTO>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	/**
	 * @return the cveNacionalidad
	 */
	public String getCveNacionalidad() {
		return cveNacionalidad;
	}

	/**
	 * @param cveNacionalidad
	 *            the cveNacionalidad to set
	 */
	public void setCveNacionalidad(String cveNacionalidad) {
		this.cveNacionalidad = cveNacionalidad;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public String getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public PersonaFisicaDTO getFisica() {
		return fisica;
	}

	public void setFisica(PersonaFisicaDTO fisica) {
		this.fisica = fisica;
	}

	public PersonaMoralDTO getMoral() {
		return moral;
	}

	public void setMoral(PersonaMoralDTO moral) {
		this.moral = moral;
	}

	public DomicilioDTO getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(DomicilioDTO domicilio) {
		this.domicilio = domicilio;
	}

	public DomicilioDTO getDomicilioFiscal() {
		return domicilioFiscal;
	}

	public void setDomicilioFiscal(DomicilioDTO domicilioFiscal) {
		this.domicilioFiscal = domicilioFiscal;
	}

	public List<DomicilioDTO> getListDomicilios() {
		return listDomicilios;
	}

	public void setListDomicilios(List<DomicilioDTO> listDomicilios) {
		this.listDomicilios = listDomicilios;
	}

	public ComunicacionDTO getComunicacion() {
		return comunicacion;
	}

	public void setComunicacion(ComunicacionDTO comunicacion) {
		this.comunicacion = comunicacion;
	}

	public List<DocumentacionDTO> getListDocumentacion() {
		return listDocumentacion;
	}

	public void setListDocumentacion(List<DocumentacionDTO> listDocumentacion) {
		this.listDocumentacion = listDocumentacion;
	}

}

