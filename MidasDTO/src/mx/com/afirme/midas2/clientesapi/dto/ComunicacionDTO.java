package mx.com.afirme.midas2.clientesapi.dto;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * 
 * @author mario.dominguez
 *Abstracción de datos de contacto en cliente unico
 */
public class ComunicacionDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * Identifier, if new registration is not required, to update the
	 * registration if required
	 */
	private Integer id;
	/**
	 * At least one phone is required
	 */
	private String telefonoCasa;
	/**
	 * At least one phone is required
	 */
	private String celular;
	/**
	 * At least one phone is required
	 */
	private String telefonoOficina;
	private String extension;
	private String fax;
	private String correoElectronico;
	private String paginaWeb;
	/**
	 * At least one phone is required
	 */
	private String telefonosAdicionales;
	private String correosAdicionales;
	private String puesto;
	private List<ComunicacionRedSocialDTO> listRedesSociales ;

	public ComunicacionDTO() {
		listRedesSociales = new LinkedList<ComunicacionRedSocialDTO>();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTelefonoCasa() {
		return telefonoCasa;
	}

	public void setTelefonoCasa(String telefonoCasa) {
		this.telefonoCasa = telefonoCasa;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getTelefonoOficina() {
		return telefonoOficina;
	}

	public void setTelefonoOficina(String telefonoOficina) {
		this.telefonoOficina = telefonoOficina;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getPaginaWeb() {
		return paginaWeb;
	}

	public void setPaginaWeb(String paginaWeb) {
		this.paginaWeb = paginaWeb;
	}

	public String getTelefonosAdicionales() {
		return telefonosAdicionales;
	}

	public void setTelefonosAdicionales(String telefonosAdicionales) {
		this.telefonosAdicionales = telefonosAdicionales;
	}

	public String getCorreosAdicionales() {
		return correosAdicionales;
	}

	public void setCorreosAdicionales(String correosAdicionales) {
		this.correosAdicionales = correosAdicionales;
	}

	public String getPuesto() {
		return puesto;
	}

	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}

	public List<ComunicacionRedSocialDTO> getListRedesSociales() {
		return listRedesSociales;
	}

	public void setListRedesSociales(List<ComunicacionRedSocialDTO> listRedesSociales) {
		this.listRedesSociales = listRedesSociales;
	}

}
