package mx.com.afirme.midas2.clientesapi.dto;
/**
 * clase para lanzar una exepcion personalizada relacionada a la validaciones de entrevista
 */


public class InterviewException  extends Exception{
	
	private static final long serialVersionUID = 1L;

	public InterviewException(String message){
		
		super(message);
	}

}
