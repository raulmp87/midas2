package mx.com.afirme.midas2.clientesapi.dto;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;



import org.springframework.http.ResponseEntity;

/**
 * 
 * @author mario.dominguez
 * Inteface con cliente unico
 */
public interface ClientesApiService {

	/**
	 * Constantes definidas para acceder a los datos del Map<String, Object>
	 * devuelto en el metodo getListClientesByFilter
	 * 
	 */
	static final String KEY_DATA = "data";
	static final String KEY_RECORDS_FILTERS = "recordsFiltered";
	static final String KEY_RECORDS_TOTAL = "recordsTotal";

	/**
	 * 
	 * @param clienteUnicoDTO
	 * @param isNewRecord
	 * @return
	 */
	ResponseEntity<Object> saveCliente(ClienteUnicoDTO clienteUnicoDTO);

	/**
	 * 
	 * @param clienteId
	 * @return
	 */
	ResponseEntity<ClienteDTO> findById(Long clienteId);
	
	/**
	 * 
	 * @param clienteId
	 * @return
	 */
	ResponseEntity<String> validateClientePrima(Long clienteId);
	

	/**
	 * 
	 * @param filter
	 * @return
	 */
	ResponseEntity<Map<String, Object>> getListClientesByFilter(FilterListClientes filter);

	/**
	 * 
	 * @param clienteId
	 * @param seycosIdCliente
	 * @return
	 */
	ResponseEntity<Long> clienteUnificado(Long clienteId, Long seycosIdCliente);

	/**
	 * 
	 * @param clienteUnicoDTO
	 * @return
	 */
	ClienteDTO convertClienteUnicoDTOToClienteDTO(ClienteUnicoDTO clienteUnicoDTO);

	/**
	 * 
	 * @param clienteDTO
	 * @return
	 */
	ClienteUnicoDTO convertClienteDTOToClienteUnicoDTO(ClienteDTO clienteDTO);

	/**
	 * 
	 * @param jsonClienteDTO
	 * @return
	 */
	ClienteUnicoDTO convertClienteDTOToClienteUnicoDTO(String jsonClienteDTO);
	
	/**
	 * 
	 * @param idCliente identificador de cliente
	 * @return 
	 */
	ClienteUnicoDTO findClienteUnicoById(Long idCliente);
	
	
	/**
	 * 
	 * @param filter
	 * @return
	 */
	List<ClienteUnicoDTO> getListClientesByFilter(ClienteUnicoDTO filter);
	
	/**
	 * 
	 * @param clienteUnico
	 * @return
	 */
	
	List<ErrorClienteUnicoDTO> validateCliente(ClienteUnicoDTO clienteUnico);
	
	/**
	 * 
	 * @param entity cliente a guardar, tanto en seycos como en cliente unico
	 * @param nombreUsuario usuario de sesión
	 * @param validaNegocio 
	 * @return
	 * @throws Exception
	 */
	 ClienteUnicoDTO saveFullDataClienteUnico(ClienteUnicoDTO entity, String nombreUsuario, boolean validaNegocio)
	throws Exception;
	 
	 /**
		 * 
		 * @param clienteUnico
		 * @return
		 */
		
		List<ClienteUnicoDTO> validateRegisterCliente(ClienteUnicoDTO clienteUnico);
		 /**
		 * 
		 * 
		 * @return
		 */
		
		List<OcupacionCNSFDTO> getGiros();
		
		
		 /**
		 * 
		 * @param  id cliente
		 * @return
		 */
		
		EntrevistaDTO findInterViews(Long idCliente) throws InterviewException;
		

		 /**
		 * 
		 * @param  id cliente, 
		 * @param entrevista
		 * @return
		 */
		
		EntrevistaDTO createInterview(Long idCliente,EntrevistaDTO entrevista);
		
		 /**
		 * 
		 * @param idpoliza id de poliza
		 * @param Ramo, valores V, A, D
		 * 
		 * @return
		 */
		
		byte[] createPDFInterviewApp(Long idCliente,String ramo,String moneda,String primaEmitida,final OutputStream os);
		
		 /**
		 * 
		 * @param  id cliente
		 * @return
		 */
		
		void validatePeps(Long idCliente,String idPoliza);
		 /**
		 * 
		 * @param  void
		 * @return Lista de documentos del catalago
		 */
		List<DocumentoDTO> getDocumentos(String exception);
		List<DocumentoDTO> getDocumentacion(Long idCliente);
		void getDocumentacionFaltante(Long idCliente) throws  DocumentationException;
		String getLinkFortmax(Long idCliente) ;
		void  saveDocument(Long idCliente, List<DocumentoDTO> documentos);
		
		
		
}
