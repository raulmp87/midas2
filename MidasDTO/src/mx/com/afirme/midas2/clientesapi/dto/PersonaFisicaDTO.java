package mx.com.afirme.midas2.clientesapi.dto;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
/**
 * 
 * @author mario.dominguez
 *Abstrae a una persona física
 */
public class PersonaFisicaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Identifier, if new registration is not required, to update the
	 * registration if required
	 */
	private Long id;
	/**
	 * Name (s) of person
	 */
	
	private String nombre;
	/**
	 * Last name
	 */
	
	private String apellidoPaterno;
	/**
	 * Mother's last name
	 */
	
	private String apellidoMaterno;
	/**
	 * Clave Única de Registro de Población
	 */
	
	private String curp;
	/**
	 * Birthdate
	 */
	
	private String fechaNacimiento;
	/**
	 * Code
	 */
	
	private String sexo;
	/**
	 * Country Code
	 */
	
	private String cvePaisNacimiento;
	/**
	 * State Code
	 */
	
	private String cveEstadoNacimiento;
	/**
	 * City Code
	 */
	
	private String cveCiudadNacimiento;
	/**
	 * Code
	 */
	
	private String estadoCivil;
	/**
	 * Occupation Code
	 */
	
	private String cveOcupacion;
	/**
	 * Occupation CNSF Code
	 */
	
	private String cveOcupacionCnsf;
	/**
	 * Additional High Risk Customer Data List of dependents
	 */
	private List<DependienteDTO> listDependientes = new LinkedList<DependienteDTO>();

	public PersonaFisicaDTO() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getCvePaisNacimiento() {
		return cvePaisNacimiento;
	}

	public void setCvePaisNacimiento(String cvePaisNacimiento) {
		this.cvePaisNacimiento = cvePaisNacimiento;
	}

	public String getCveEstadoNacimiento() {
		return cveEstadoNacimiento;
	}

	public void setCveEstadoNacimiento(String cveEstadoNacimiento) {
		this.cveEstadoNacimiento = cveEstadoNacimiento;
	}

	public String getCveCiudadNacimiento() {
		return cveCiudadNacimiento;
	}

	public void setCveCiudadNacimiento(String cveCiudadNacimiento) {
		this.cveCiudadNacimiento = cveCiudadNacimiento;
	}

	public String getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getCveOcupacion() {
		return cveOcupacion;
	}

	public void setCveOcupacion(String cveOcupacion) {
		this.cveOcupacion = cveOcupacion;
	}

	public String getCveOcupacionCnsf() {
		return cveOcupacionCnsf;
	}

	public void setCveOcupacionCnsf(String cveOcupacionCnsf) {
		this.cveOcupacionCnsf = cveOcupacionCnsf;
	}

	public List<DependienteDTO> getListDependientes() {
		return listDependientes;
	}

	public void setListDependientes(List<DependienteDTO> listDependientes) {
		this.listDependientes = listDependientes;
	}

}
