package mx.com.afirme.midas2.clientesapi.dto;

import java.io.Serializable;

/**
 * Clase para abstraer los errores resultado de la validacion de documentacion
 */
public class ErrorDocumentoDTO  implements Serializable{

	
	private static final long serialVersionUID = 1L;
	private String label;
	private String value;
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
	
}
