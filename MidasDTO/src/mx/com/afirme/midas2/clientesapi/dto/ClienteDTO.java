package mx.com.afirme.midas2.clientesapi.dto;

import java.io.Serializable;
/**
 * Abstracción de cliente dentro de cliente unico
 * 
 */
public class ClienteDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Identifier, if new registration is not required, to update the
	 * registration if required
	 */
	private Long id;

	/**
	 * Is the bank's customer number Only captures on the Emision Delegada
	 */
	private Long bancaClienteId;
	/**
	 * Customer Risk Classification Values A = High and B = Low
	 */
	private String riesgo;
	/**
	 * Not required A = Active and I = Inactive
	 */
	private String situacion;
	/**
	 * Not required A = Active and I = Inactive
	 */
	private String motivo;
	/**
	 * Control Field Not required are obtained by the system
	 */
	private String usuarioCreacion;
	/**
	 * Control Field Not required are obtained by the system
	 */
	private String fechaCreacion;
	/**
	 * Control Field Not required are obtained by the system
	 */
	private String usuarioModificacion;
	/**
	 * Control Field Not required are obtained by the system
	 */
	private String fechaModificacion;
	/**
	 * Required to record client information
	 */

	private PersonaDTO persona;
	/**
	 * It is required only when the client does not have the interview, this
	 * validation is done at the time of issuance
	 */
	private EntrevistaDTO entrevista;

	public ClienteDTO() {
		persona= new PersonaDTO();
	}

	public ClienteDTO(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBancaClienteId() {
		return bancaClienteId;
	}

	public void setBancaClienteId(Long bancaClienteId) {
		this.bancaClienteId = bancaClienteId;
	}

	public String getRiesgo() {
		return riesgo;
	}

	public void setRiesgo(String riesgo) {
		this.riesgo = riesgo;
	}

	public String getSituacion() {
		return situacion;
	}

	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public String getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public PersonaDTO getPersona() {
		return persona;
	}

	public void setPersona(PersonaDTO persona) {
		this.persona = persona;
	}

	public EntrevistaDTO getEntrevista() {
		return entrevista;
	}

	public void setEntrevista(EntrevistaDTO entrevista) {
		this.entrevista = entrevista;
	}
}

