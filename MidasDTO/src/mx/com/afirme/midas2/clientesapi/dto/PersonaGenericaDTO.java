package mx.com.afirme.midas2.clientesapi.dto;

import javax.validation.constraints.NotNull;
/**
 * Clase con datos mínimos de una persona
 * 
 */
public class PersonaGenericaDTO {

	/**
	 * Identifier
	 */
	private Long id;
	/**
	 * Required value PF=Fisica or PM=Moral
	 */
	@NotNull
	private String tipoPersona;
	/**
	 * Registro Federal de Contribuyentes, if rfc is not assigned, a generic one
	 * is established
	 */
	private String rfc;

	/**
	 * Name (s) of person, if the person is moral establish the social reason
	 * here
	 */
	@NotNull
	private String nombre;
	/**
	 * Last name
	 */
	private String apellidoPaterno;
	/**
	 * Mother's last name
	 */
	private String apellidoMaterno;
	/**
	 * Birthdate, if the person is moral establish the constitution date here
	 * <p>
	 * If a fisica person is required
	 */
	private String fechaNacimiento;
	/**
	 * Clave Única de Registro de Población
	 */
	private String curp;
	/**
	 * Code
	 */
	private String sexo;
	/**
	 * Code
	 */
	private String cveNacionalidad;
	/**
	 * Code
	 */
	private String cveEstadoNacimiento;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getCveNacionalidad() {
		return cveNacionalidad;
	}

	public void setCveNacionalidad(String cveNacionalidad) {
		this.cveNacionalidad = cveNacionalidad;
	}

	public String getCveEstadoNacimiento() {
		return cveEstadoNacimiento;
	}

	public void setCveEstadoNacimiento(String cveNacimiento) {
		this.cveEstadoNacimiento = cveNacimiento;
	}

}
