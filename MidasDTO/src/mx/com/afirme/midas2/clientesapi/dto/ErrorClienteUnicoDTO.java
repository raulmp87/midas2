package mx.com.afirme.midas2.clientesapi.dto;

import java.io.Serializable;
/**
 * clase para abstraer los errores de las validaciones del cliente
 */

public class ErrorClienteUnicoDTO implements Serializable{

	
	private static final long serialVersionUID = 1L;

	private String value;
	private String label;
	
	
	
	//Setters and getters
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
}
