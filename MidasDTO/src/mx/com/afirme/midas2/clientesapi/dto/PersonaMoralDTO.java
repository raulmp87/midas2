package mx.com.afirme.midas2.clientesapi.dto;


import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.validation.constraints.NotNull;
/**
 * 
 * @author mario.dominguez
 * Abstrae a la persona moral
 *
 */
public class PersonaMoralDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Identifier
	 */
	private Long id;
	/**
	 * Name by which a company is known collectively. It is an official and
	 * legal name that appears in the documentation that made it possible to
	 * constitute the legal entity in question
	 */
	@NotNull
	private String razonSocial;
	/***
	 * Constitution date
	 */
	@NotNull
	private String fechaConstitucion;
	/**
	 * Country Code
	 */
	@NotNull
	private String cvePaisConstitucion;
	/**
	 * Giro Code
	 */
	@NotNull
	private String cveGiro;
	/**
	 * Giro CNFS Code
	 */
	@NotNull
	private String cveGiroCnsf;
	/**
	 * 
	 */
	@NotNull
	private String sectorId;
	/**
	 * Person Representative
	 */
	private PersonaGenericaDTO representanteLegal = new PersonaGenericaDTO();
	/**
	 * Additional High Risk Customer Data Corporate Structure
	 */
	private List<EstructuraCoorporativaDTO> listEstructuraCoorporativa = new LinkedList<EstructuraCoorporativaDTO>();

	public PersonaMoralDTO() {
		// Default Constructor
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getFechaConstitucion() {
		return fechaConstitucion;
	}

	public void setFechaConstitucion(String fechaConstitucion) {
		this.fechaConstitucion = fechaConstitucion;
	}

	public String getCvePaisConstitucion() {
		return cvePaisConstitucion;
	}

	public void setCvePaisConstitucion(String cvePaisConstitucion) {
		this.cvePaisConstitucion = cvePaisConstitucion;
	}

	public String getCveGiro() {
		return cveGiro;
	}

	public void setCveGiro(String cveGiro) {
		this.cveGiro = cveGiro;
	}

	public String getCveGiroCnsf() {
		return cveGiroCnsf;
	}

	public void setCveGiroCnsf(String cveGiroCnsf) {
		this.cveGiroCnsf = cveGiroCnsf;
	}

	public String getSectorId() {
		return sectorId;
	}

	public void setSectorId(String sectorId) {
		this.sectorId = sectorId;
	}

	public PersonaGenericaDTO getRepresentanteLegal() {
		return representanteLegal;
	}

	public void setRepresentanteLegal(PersonaGenericaDTO representanteLegal) {
		this.representanteLegal = representanteLegal;
	}

	public List<EstructuraCoorporativaDTO> getListEstructuraCoorporativa() {
		return listEstructuraCoorporativa;
	}

	public void setListEstructuraCoorporativa(List<EstructuraCoorporativaDTO> listEstructuraCoorporativa) {
		this.listEstructuraCoorporativa = listEstructuraCoorporativa;
	}

}