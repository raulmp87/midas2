package mx.com.afirme.midas2.clientesapi.dto;

import java.io.Serializable;
/**
 * 
 * @author mario.dominguez
 *Abstracción de dependientes en cliente unico
 */
public class DependienteDTO implements Serializable {


    private static final long serialVersionUID = 1L;
	
	private Long id;
    private String nombre;
    private String claveTipo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getClaveTipo() {
        return claveTipo;
    }

    public void setClaveTipo(String claveTipo) {
        this.claveTipo = claveTipo;
    }
    
}

