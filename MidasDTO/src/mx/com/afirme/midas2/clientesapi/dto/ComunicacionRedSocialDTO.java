package mx.com.afirme.midas2.clientesapi.dto;

import java.io.Serializable;
/**
 * 
 * @author mario.dominguez
 *Abstracción de redes sociales en cliente unico
 */
public class ComunicacionRedSocialDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Integer redSocialId;
	private String valor;

	public ComunicacionRedSocialDTO() {
		// Constructor Principal
	}

	/**
	 * 
	 * @param id
	 */
	public ComunicacionRedSocialDTO(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getRedSocialId() {
		return redSocialId;
	}

	public void setRedSocialId(Integer redSocialId) {
		this.redSocialId = redSocialId;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

}