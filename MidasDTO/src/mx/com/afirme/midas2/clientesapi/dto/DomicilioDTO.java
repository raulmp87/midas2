package mx.com.afirme.midas2.clientesapi.dto;

import java.io.Serializable;
/**
 * 
 * @author mario.dominguez
 * Abstración de domicilio en cliente unico
 */
public class DomicilioDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Identifier, if new registration is not required, to update the
	 * registration if required
	 */
	private Long id;
	/**
	 * Country Code
	 */

	private String cvePais;
	/**
	 * State Code
	 */

	private String cveEstado;
	/**
	 * City Code
	 */

	private String cveCiudad;

	private String calle;

	private String numero;

	private String codigoPostal;

	private String colonia;

	private String tipoDomicilio;
	/**
	 * Read only information
	 */
	private String direccion;

	public DomicilioDTO() {
		// Default Constructor
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCvePais() {
		return cvePais;
	}

	public void setCvePais(String cvePais) {
		this.cvePais = cvePais;
	}

	public String getCveEstado() {
		return cveEstado;
	}

	public void setCveEstado(String cveEstado) {
		this.cveEstado = cveEstado;
	}

	public String getCveCiudad() {
		return cveCiudad;
	}

	public void setCveCiudad(String cveCiudad) {
		this.cveCiudad = cveCiudad;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getTipoDomicilio() {
		return tipoDomicilio;
	}

	public void setTipoDomicilio(String tipoDomicilio) {
		this.tipoDomicilio = tipoDomicilio;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

}
