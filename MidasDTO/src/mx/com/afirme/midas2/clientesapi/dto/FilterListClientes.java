package mx.com.afirme.midas2.clientesapi.dto;

/**
 * Clase utilizada para recibir los parametros de filtro del Request 'GET
 * /clientes-api' Listado de Clientes
 * 
 * 
 * @author mario.dominguez
 *
 */
public class FilterListClientes {

	private int page;
	private int perPage;
	private boolean view;
	private String tipoPersona;
	private String rfc;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String curp;
	private String razonSocial;
	private String fechaConstitucion;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPerPage() {
		return perPage;
	}

	public void setPerPage(int perPage) {
		this.perPage = perPage;
	}

	public boolean isView() {
		return view;
	}

	public void setView(boolean view) {
		this.view = view;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getFechaConstitucion() {
		return fechaConstitucion;
	}

	public void setFechaConstitucion(String fechaConstitucion) {
		this.fechaConstitucion = fechaConstitucion;
	}

	@Override
	public String toString() {
		StringBuilder strBr = new StringBuilder();
		strBr.append("page").append("=").append(page);
		strBr.append("perPage").append("=").append(perPage).append("&");
		strBr.append("tipoPersona").append("=").append(tipoPersona).append("&");
		strBr.append("rfc").append("=").append(rfc).append("&");
		strBr.append("nombre").append("=").append(nombre).append("&");
		strBr.append("apellidoPaterno").append("=").append(apellidoPaterno).append("&");
		strBr.append("apellidoMaterno").append("=").append(apellidoMaterno).append("&");
		strBr.append("curp").append("=").append(curp).append("&");
		strBr.append("razonSocial").append("=").append(razonSocial).append("&");
		strBr.append("fechaConstitucion").append("=").append(fechaConstitucion);
		return strBr.toString();
	}

}

