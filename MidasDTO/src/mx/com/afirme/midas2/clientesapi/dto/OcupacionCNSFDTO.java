package mx.com.afirme.midas2.clientesapi.dto;

import java.io.Serializable;

public class OcupacionCNSFDTO implements Serializable {

	/**
	 * Clase para abstraer el catalogo de ocupaciones de la CNSF 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String altoRiesgo;
	private String actividad;
	private String usuarioCreacion;
	private String fechaCreacion;
	private String fechaModificacion;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAltoRiesgo() {
		return altoRiesgo;
	}
	public void setAltoRiesgo(String altoRiesgo) {
		this.altoRiesgo = altoRiesgo;
	}
	public String getActividad() {
		return actividad;
	}
	public void setActividad(String actividad) {
		this.actividad = actividad;
	}
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	public String getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
}
