package mx.com.afirme.midas2.clientesapi.dto;

import java.io.Serializable;
/**
 * 
 * @author mario.dominguez
 *Abstracción de documentacion en cliente único
 */
public class DocumentacionDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long documentoId;
	private String descripcion;
	private String fechaVigencia;


	public DocumentacionDTO() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDocumentoId() {
		return documentoId;
	}

	public void setDocumentoId(Long documentacionId) {
		this.documentoId = documentacionId;
	}
	
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getFechaVigencia() {
		return fechaVigencia;
	}

	public void setFechaVigencia(String fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}

}
