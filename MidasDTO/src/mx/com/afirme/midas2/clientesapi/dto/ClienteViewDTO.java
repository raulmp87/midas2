package mx.com.afirme.midas2.clientesapi.dto;

/**
 * DTO utilizado para mostrar en los listados de clientes
 * 
 * @author mario.dominguez
 *
 */
public class ClienteViewDTO extends PersonaViewDTO {

	private Long id;
	private Long seycosIdCliente;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSeycosIdCliente() {
		return seycosIdCliente;
	}

	public void setSeycosIdCliente(Long seycosIdCliente) {
		this.seycosIdCliente = seycosIdCliente;
	}

}

