package mx.com.afirme.midas2.clientesapi.dto;
/**
 * Clase uso para excepciones personalizas para la validacion de documentación
 */

public class DocumentationException  extends Exception{
	
	private static final long serialVersionUID = 1L;

	public DocumentationException(String message){
		
		super( message);
		
	}

}
