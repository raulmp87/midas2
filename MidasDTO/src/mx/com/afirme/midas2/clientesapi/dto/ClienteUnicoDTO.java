package mx.com.afirme.midas2.clientesapi.dto;

import java.util.LinkedList;
import java.util.List;

import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;

/**
 * Abstracción de cliente dentro de midas para interacción la aplicación cliente unico
 * @author mario.dominguez
 * 
 */
public class ClienteUnicoDTO extends ClienteGenericoDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String idPaisNacimiento;
	private String idPaisConstitucion;
	private String ocupacionCNSF;
	private String gradoRiesgo;
	private String numeroExtension;
	private String ciudadNacimiento;
	private String nacionalidad;
	private String numeroExtensionFiscal;
	private String motivoContratacion;
	private String nombreConyugeDependientes;
	private String asociacionesPatrimoniales;
	private String idClienteUnico;
	private Boolean verificarRepetidos;
	private String cveGiroCNSF;
	private String numeroDom;
	private String numeroDomFiscal;
	private String idPaisFiscal;
	private String idMunicipioDirPrin;
	private String idEstadoDirPrin;

	private DomicilioDTO domicilioTemporal	;

	private List<DependienteDTO> listDependientes = new LinkedList<DependienteDTO>();

	public String getIdPaisNacimiento() {

		return idPaisNacimiento;
	}

	public void setIdPaisNacimiento(String idPaisNacimiento) {
		this.idPaisNacimiento = idPaisNacimiento;
	}

	public String getIdPaisConstitucion() {
		return idPaisConstitucion;
	}

	public void setIdPaisConstitucion(String idPaisConstitucion) {
		this.idPaisConstitucion = idPaisConstitucion;
	}

	public String getOcupacionCNSF() {
		return ocupacionCNSF;
	}

	public void setOcupacionCNSF(String ocupacionCNSF) {
		this.ocupacionCNSF = ocupacionCNSF;
	}

	public String getGradoRiesgo() {
		return gradoRiesgo;
	}

	public void setGradoRiesgo(String gradoRiesgo) {
		this.gradoRiesgo = gradoRiesgo;
	}

	public String getNumeroExtension() {
		return numeroExtension;
	}

	public void setNumeroExtension(String numeroExtension) {
		this.numeroExtension = numeroExtension;
	}

	public String getCiudadNacimiento() {
		return ciudadNacimiento;
	}

	public void setCiudadNacimiento(String ciudadNacimiento) {
		this.ciudadNacimiento = ciudadNacimiento;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public String getNumeroExtensionFiscal() {
		return numeroExtensionFiscal;
	}

	public void setNumeroExtensionFiscal(String numeroExtensionFiscal) {
		this.numeroExtensionFiscal = numeroExtensionFiscal;
	}

	public String getMotivoContratacion() {
		return motivoContratacion;
	}

	public void setMotivoContratacion(String motivoContratacion) {
		this.motivoContratacion = motivoContratacion;
	}

	public String getNombreConyugeDependientes() {
		return nombreConyugeDependientes;
	}

	public void setNombreConyugeDependientes(String nombreConyugeDependientes) {
		this.nombreConyugeDependientes = nombreConyugeDependientes;
	}

	public String getAsociacionesPatrimoniales() {
		return asociacionesPatrimoniales;
	}

	public void setAsociacionesPatrimoniales(String asociacionesPatrimoniales) {
		this.asociacionesPatrimoniales = asociacionesPatrimoniales;
	}

	public List<DependienteDTO> getListDependientes() {
		return listDependientes;
	}

	public void setListDependientes(List<DependienteDTO> listDependientes) {
		this.listDependientes = listDependientes;
	}

	public String getIdClienteUnico() {
		return idClienteUnico;
	}

	public void setIdClienteUnico(String idClienteUnico) {
		this.idClienteUnico = idClienteUnico;
	}

	public Boolean getVerificarRepetidos() {
		return verificarRepetidos;
	}

	public void setVerificarRepetidos(Boolean verificarRepetidos) {
		this.verificarRepetidos = verificarRepetidos;
	}

	public String getCveGiroCNSF() {
		return cveGiroCNSF;
	}

	public void setCveGiroCNSF(String cveGiroCNSF) {
		this.cveGiroCNSF = cveGiroCNSF;
	}

	public String getNumeroDom() {
		return numeroDom;
	}

	public void setNumeroDom(String numeroDom) {
		this.numeroDom = numeroDom;
	}

	public String getNumeroDomFiscal() {
		return numeroDomFiscal;
	}

	public void setNumeroDomFiscal(String numeroDomFiscal) {
		this.numeroDomFiscal = numeroDomFiscal;
	}

	public String getIdPaisFiscal() {
		return idPaisFiscal;
	}

	public void setIdPaisFiscal(String idPaisFiscal) {
		this.idPaisFiscal = idPaisFiscal;
	}

	public String getIdMunicipioDirPrin() {
		return idMunicipioDirPrin;
	}

	public void setIdMunicipioDirPrin(String idMunicipioDirPrin) {
		this.idMunicipioDirPrin = idMunicipioDirPrin;
	}

	public String getIdEstadoDirPrin() {
		return idEstadoDirPrin;
	}

	public void setIdEstadoDirPrin(String idEstadoDirPrin) {
		this.idEstadoDirPrin = idEstadoDirPrin;
	}

	public DomicilioDTO getDomicilioTemporal() {
		return domicilioTemporal;
	}

	public void setDomicilioTemporal(DomicilioDTO domicilioTemporal) {
		this.domicilioTemporal = domicilioTemporal;
	}

}
