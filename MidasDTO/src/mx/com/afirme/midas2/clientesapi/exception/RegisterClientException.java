package mx.com.afirme.midas2.clientesapi.exception;
/**
 * Clase para lanzar excepción personalizada al menejo de errores de cliente unico
 * 
 */
public class RegisterClientException extends Exception{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RegisterClientException(String message){
		
		super(message);
	}
}
