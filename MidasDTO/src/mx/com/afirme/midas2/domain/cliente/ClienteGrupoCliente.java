package mx.com.afirme.midas2.domain.cliente;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;

/**
 * ClienteGrupoClientes entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOCLIENTEGRUPOCLIENTES", schema = "MIDAS")
public class ClienteGrupoCliente implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private GrupoCliente grupoCliente;
	private Long idTcCliente;
	private ClienteDTO cliente;
	

	// Constructors

	/** default constructor */
	public ClienteGrupoCliente() {
	}

	/** full constructor */
	public ClienteGrupoCliente(Long id,
			GrupoCliente grupoCliente, Long idTcCliente) {
		this.id = id;
		this.grupoCliente = grupoCliente;
		this.idTcCliente = idTcCliente;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "ID_CTE_GPO_CTE_SEQ", allocationSize = 1, sequenceName = "idToClienteGrupoClientes_seq", schema="MIDAS")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_CTE_GPO_CTE_SEQ")
	@Column(name = "IDTOCLIENTEGRUPOCLIENTES", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "IDTCGRUPOCLIENTES", nullable = false)
	public GrupoCliente getGrupoCliente() {
		return grupoCliente;
	}
	
	public void setGrupoCliente(GrupoCliente grupoCliente) {
		this.grupoCliente = grupoCliente;
	}

	@Column(name = "IDTCCLIENTE", nullable = false, precision = 22, scale = 0)
	public Long getIdTcCliente() {
		return this.idTcCliente;
	}

	public void setIdTcCliente(Long idTcCliente) {
		this.idTcCliente = idTcCliente;
	}
	
	@Transient
	public ClienteDTO getCliente() {
		return cliente;
	}
	
	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((grupoCliente == null) ? 0 : grupoCliente.hashCode());
		result = prime * result
				+ ((idTcCliente == null) ? 0 : idTcCliente.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClienteGrupoCliente other = (ClienteGrupoCliente) obj;
		if (grupoCliente == null) {
			if (other.grupoCliente != null)
				return false;
		} else if (!grupoCliente.equals(other.grupoCliente))
			return false;
		if (idTcCliente == null) {
			if (other.idTcCliente != null)
				return false;
		} else if (!idTcCliente.equals(other.idTcCliente))
			return false;
		return true;
	}

}