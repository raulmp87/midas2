package mx.com.afirme.midas2.domain.cliente;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


import mx.com.afirme.midas2.domain.negocio.cliente.grupo.NegocioGrupoCliente;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

/**
 * GrupoClientes entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCGRUPOCLIENTES", schema = "MIDAS")
public class GrupoCliente implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;
	private String clave;
	private String descripcion;
	private Boolean vip;
	private CalifGrupoCliente califGrupoCliente;
	private List<ClienteGrupoCliente> clienteGrupoClientes = new ArrayList<ClienteGrupoCliente>();
	private List<NegocioGrupoCliente> negocioGrupoClientes = new ArrayList<NegocioGrupoCliente>();

	// Constructors

	/** default constructor */
	public GrupoCliente() {
	}

	/** minimal constructor */
	public GrupoCliente(Long id, String descripcion) {
		this.id = id;
		this.descripcion = descripcion;
	}

	/** full constructor */
	public GrupoCliente(Long id, String descripcion,
			List<ClienteGrupoCliente> clienteGrupoClientes,
			List<NegocioGrupoCliente> negocioGrupoClientes) {
		this.id = id;
		this.descripcion = descripcion;
		this.clienteGrupoClientes = clienteGrupoClientes;
		this.negocioGrupoClientes = negocioGrupoClientes;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "ID_GPO_CLIENTE_SEQ", allocationSize = 1, sequenceName = "idTcGrupoClientes_seq", schema="MIDAS")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_GPO_CLIENTE_SEQ")
	@Column(name = "IDTCGRUPOCLIENTES", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="CLAVEGRUPOCLIENTES", unique=true, nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=1, max=8, groups=EditItemChecks.class)
	public String getClave() {
		return clave!=null?this.clave.toUpperCase():this.clave;
	}
	
	public void setClave(String clave) {
		this.clave = clave;
	}

	@Column(name = "DESCRIPCIONGRUPOCLIENTES", nullable = false, length = 80)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=1, max=80, groups=EditItemChecks.class)
	public String getDescripcion() {
		return this.descripcion!=null?this.descripcion.toUpperCase():this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
	@Column(name="CLAVEVIP")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Boolean getVip() {
		return vip;
	}
	
	public void setVip(Boolean vip) {
		this.vip = vip;
	}
	
	@ManyToOne
	@JoinColumn(name="IDTCCALIFGRUPOCLIENTES", referencedColumnName="IDTCCALIFGRUPOCLIENTES")
	@Valid
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public CalifGrupoCliente getCalifGrupoCliente() {
		return califGrupoCliente;
	}
	
	public void setCalifGrupoCliente(CalifGrupoCliente califGrupoCliente) {
		this.califGrupoCliente = califGrupoCliente;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "grupoCliente", orphanRemoval=true)
	public List<ClienteGrupoCliente> getClienteGrupoClientes() {
		return clienteGrupoClientes;
	}
	
	public void setClienteGrupoClientes(
			List<ClienteGrupoCliente> clienteGrupoClientes) {
		this.clienteGrupoClientes = clienteGrupoClientes;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "grupoClientes")
	public List<NegocioGrupoCliente> getNegocioGrupoClientes() {
		return this.negocioGrupoClientes;
	}

	public void setNegocioGrupoClientes(List<NegocioGrupoCliente> negocioGrupoClientes) {
		this.negocioGrupoClientes = negocioGrupoClientes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clave == null) ? 0 : clave.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GrupoCliente other = (GrupoCliente) obj;
		if (clave == null) {
			if (other.clave != null)
				return false;
		} else if (!clave.equals(other.clave))
			return false;
		return true;
	}

}