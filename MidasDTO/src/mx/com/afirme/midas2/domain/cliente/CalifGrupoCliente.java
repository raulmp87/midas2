package mx.com.afirme.midas2.domain.cliente;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.validator.group.EditItemChecks;

@Entity
@Table(name = "TCCALIFGRUPOCLIENTES", schema = "MIDAS")
public class CalifGrupoCliente implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public Long id;
	public String descripcion;
	
	@Id
	@SequenceGenerator(name = "IDTCCALIFGRUPOCLIENTES_SEQ", allocationSize = 1, sequenceName = "IDTCCALIFGRUPOCLIENTES_SEQ", schema="MIDAS")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCCALIFGRUPOCLIENTES_SEQ")
	@Column(name = "IDTCCALIFGRUPOCLIENTES", unique = true, nullable = false, precision = 22, scale = 0)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="DESCRIPCIONCALIFGRUPOCLIENTES", unique=true)
	public String getDescripcion() {
		return descripcion!=null?this.descripcion.toUpperCase():this.descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((descripcion == null) ? 0 : descripcion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CalifGrupoCliente other = (CalifGrupoCliente) obj;
		if (descripcion == null) {
			if (other.descripcion != null)
				return false;
		} else if (!descripcion.equals(other.descripcion))
			return false;
		return true;
	}

}
