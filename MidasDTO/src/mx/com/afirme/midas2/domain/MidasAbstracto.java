/**
 * 
 */
package mx.com.afirme.midas2.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * @author smvr
 *
 */

@MappedSuperclass
public abstract class MidasAbstracto implements Entidad{

	private static final long serialVersionUID = 6219537747471352496L;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CREACION", nullable = false, updatable= false)
	protected Date   fechaCreacion = new Date();
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_MODIFICACION", nullable = false)
	protected Date   fechaModificacion = new Date();
	
	@Column(name = "CODIGO_USUARIO_CREACION", length = 8, updatable = false)
	protected String codigoUsuarioCreacion;
	
	@Column(name = "CODIGO_USUARIO_MODIFICACION", length = 8)
	protected String codigoUsuarioModificacion;

	/**
	 * @return the fechaCreacion
	 */
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	/**
	 * @param fechaCreacion the fechaCreacion to set
	 */
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	/**
	 * @return the fechaModificacion
	 */
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	/**
	 * @param fechaModificacion the fechaModificacion to set
	 */
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	/**
	 * @return the codigoUsuarioCreacion
	 */
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	/**
	 * @param codigoUsuarioCreacion the codigoUsuarioCreacion to set
	 */
	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	/**
	 * @return the codigoUsuarioModificacion
	 */
	public String getCodigoUsuarioModificacion() {
		return codigoUsuarioModificacion;
	}

	/**
	 * @param codigoUsuarioModificacion the codigoUsuarioModificacion to set
	 */
	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}


}
