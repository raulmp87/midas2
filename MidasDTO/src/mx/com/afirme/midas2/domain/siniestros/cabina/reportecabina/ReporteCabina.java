package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.catalogos.EnumBase;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestro.ReporteSiniestroMovil;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.cita.CitaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.gastoajuste.GastoAjusteReporte;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar.LugarSiniestroMidas;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.joda.time.DateTime;

@Entity(name = "ReporteCabina")
@Table(name = "TOREPORTECABINA", schema = "MIDAS")
public class ReporteCabina extends MidasAbstracto implements Entidad {
	
	/**
	 *  
	 */
	private static final long serialVersionUID = 6687090548324948046L;
	
	public enum EstatusReporteCabina implements EnumBase<Integer>{
		TRAMITE_REPORTE(1),
	    CANCELADO(2),
	    TRAMITE_SINIESTRO(3),
		RECHAZADO(4),
		TERMINADO(5),
		REPORTE_REAPERTURADO(6),
		SINIESTRO_REAPERTURADO(7);

	    private final Integer estatus; 

	    private EstatusReporteCabina(Integer estatus) {
	        this.estatus = estatus;
	    }

		@Override
		public Integer getValue() {
			return estatus;
		}

		@Override
		public String getLabel() {
			return null;
		}

	}
	

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOREPORTECABINA_ID_GENERATOR")
	@SequenceGenerator(name="TOREPORTECABINA_ID_GENERATOR", schema="MIDAS", sequenceName="IDTOREPORTECABINA_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;
	
	@Transient
	private String numeroReporte;
	
	@Column(name = "ESTATUS")
	private Integer estatus;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_MOD_ESTATUS", length = 7)
	private Date fechaModEstatus;

	@Column(name = "PERSONA_REPORTA")
	private String personaReporta;

	@Column(name = "ATENDIDO_POR")
	private String atendidoPor;
	
	@Column(name = "PERSONA_CONDUCTOR")
	private String personaConductor;

	@Column(name = "LADA_TELEFONO")
	private String ladaTelefono;

	@Column(name = "TELEFONO")
	private String telefono;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_OCURRIDO", length = 7)
	private Date fechaOcurrido;

	@Column(name = "HORA_OCURRIDO")
	private String horaOcurrido;

	@Column(name = "INFO_ADICIONAL")
	private String informacionAdicional;

	@Column(name = "DECLARACION_TEXTO")
	private String declaracionTexto;

	@Column(name = "CONSECUTIVO_REPORTE")
	private String consecutivoReporte;
	
	@Column(name = "ANIO_REPORTE")
	private String anioReporte;
	
	@Column(name = "IDTOSOLICITUD")
	private String idToSolicitud;

	@Column(name = "CLAVE_OFICINA")
	private String claveOficina;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_HORA_ASIGNACION")
	private Date fechaHoraAsignacion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_HORA_CONTACTO")
	private Date fechaHoraContacto;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_HORA_REPORTE")
	private Date fechaHoraReporte;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_HORA_TERMINACION")
	private Date fechaHoraTerminacion;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "CITA_ID", nullable = false, updatable = false)
	private CitaReporteCabina citaReporteCabina;

	@OneToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name="LUGAR_ATENCION_ID", referencedColumnName="ID")
	@JoinFetch(JoinFetchType.OUTER)
	private LugarSiniestroMidas lugarAtencion;
	
	@OneToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name="LUGAR_OCURRIDO_ID", referencedColumnName="ID")
	@JoinFetch(JoinFetchType.OUTER)
	private LugarSiniestroMidas lugarOcurrido;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="AJUSTADOR_ID", referencedColumnName="ID")
	@JoinFetch(JoinFetchType.OUTER)
	private ServicioSiniestro ajustador;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="OFICINA_ID", referencedColumnName="ID")
	@JoinFetch(JoinFetchType.INNER)
	private Oficina oficina;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="IDTOPOLIZA", referencedColumnName="IDTOPOLIZA")
	@JoinFetch(JoinFetchType.OUTER)
	private PolizaDTO poliza;
	
	@OneToOne(fetch=FetchType.LAZY, mappedBy="reporteCabina",cascade = CascadeType.ALL)
	private SiniestroCabina siniestroCabina;
	
	@OneToMany(fetch=FetchType.LAZY,  mappedBy = "reporteCabina", cascade = CascadeType.ALL)
    private List<GastoAjusteReporte> gastosAjuste = new ArrayList<GastoAjusteReporte>();
	
	@OneToOne(fetch=FetchType.LAZY, mappedBy="reporteCabina", cascade = CascadeType.ALL)
	private SeccionReporteCabina seccionReporteCabina;
	
	@Column(name = "AGENTE_ID", nullable = false, precision = 22, scale = 0)
	private BigDecimal agenteId;
	
	@ManyToOne
	@JoinColumn(name = "MONEDA_ID", referencedColumnName="IDTCMONEDA")
	private MonedaDTO moneda;
	
	@Column(name = "NOMBREAGENTE")	
	private String nombreAgente;
	
	@Column(name="PERSONACONTRATANTE_ID")
	private Long personaContratanteId;
	
	@Column(name = "CLAVENEGOCIO")
	private String claveNegocio;
	
	@Column(name = "TIPOCOTIZACION")
	private String tipoCotizacion;

	@Transient
	private Date fechaHoraOcurrido;
	
    @Transient
	private String error;
    
	@OneToOne(fetch = FetchType.LAZY, mappedBy = "reporteCabina", cascade = CascadeType.ALL)
    private ReporteSiniestroMovil reporteMovil;
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the estatus
	 */
	public Integer getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return the fechaModEstatus
	 */
	public Date getFechaModEstatus() {
		return fechaModEstatus;
	}

	/**
	 * @param fechaModEstatus the fechaModEstatus to set
	 */
	public void setFechaModEstatus(Date fechaModEstatus) {
		this.fechaModEstatus = fechaModEstatus;
	}

	/**
	 * @return the personaReporta
	 */
	public String getPersonaReporta() {
		return personaReporta;
	}

	/**
	 * @param personaReporta the personaReporta to set
	 */
	public void setPersonaReporta(String personaReporta) {
		this.personaReporta = personaReporta;
	}

	/**
	 * @return the personaConductor
	 */
	public String getPersonaConductor() {
		return personaConductor;
	}

	/**
	 * @param personaConductor the personaConductor to set
	 */
	public void setPersonaConductor(String personaConductor) {
		this.personaConductor = personaConductor;
	}

	public String getLadaTelefono() {
		return ladaTelefono;
	}

	public void setLadaTelefono(String ladaTelefono) {
		this.ladaTelefono = ladaTelefono;
	}

	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}

	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	/**
	 * @return the fechaOcurrido
	 */
	public Date getFechaOcurrido() {
		return fechaOcurrido;
	}

	/**
	 * @param fechaOcurrido the fechaOcurrido to set
	 */
	public void setFechaOcurrido(Date fechaOcurrido) {
		this.fechaOcurrido = fechaOcurrido;
	}

	/**
	 * @return the horaOcurrido
	 */
	public String getHoraOcurrido() {
		return horaOcurrido;
	}

	/**
	 * @param horaOcurrido the horaOcurrido to set
	 */
	public void setHoraOcurrido(String horaOcurrido) {
		this.horaOcurrido = horaOcurrido;
	}

	/**
	 * @return the informacionAdicional
	 */
	public String getInformacionAdicional() {
		return informacionAdicional;
	}

	/**
	 * @param informacionAdicional the informacionAdicional to set
	 */
	public void setInformacionAdicional(String informacionAdicional) {
		this.informacionAdicional = informacionAdicional;
	}

	/**
	 * @return the declaracionTexto
	 */
	public String getDeclaracionTexto() {
		return declaracionTexto;
	}

	/**
	 * @param declaracionTexto the declaracionTexto to set
	 */
	public void setDeclaracionTexto(String declaracionTexto) {
		this.declaracionTexto = declaracionTexto;
	}

	/**
	 * @return the fechaHoraAsignacion
	 */
	public Date getFechaHoraAsignacion() {
		return fechaHoraAsignacion;
	}

	/**
	 * @param fechaHoraAsignacion the fechaHoraAsignacion to set
	 */
	public void setFechaHoraAsignacion(Date fechaHoraAsignacion) {
		this.fechaHoraAsignacion = fechaHoraAsignacion;
	}

	/**
	 * @return the fechaHoraContacto
	 */
	public Date getFechaHoraContacto() {
		return fechaHoraContacto;
	}

	/**
	 * @param fechaHoraContacto the fechaHoraContacto to set
	 */
	public void setFechaHoraContacto(Date fechaHoraContacto) {
		this.fechaHoraContacto = fechaHoraContacto;
	}

	/**
	 * @return the fechaHoraReporte
	 */
	public Date getFechaHoraReporte() {
		return fechaHoraReporte;
	}

	/**
	 * @param fechaHoraReporte the fechaHoraReporte to set
	 */
	public void setFechaHoraReporte(Date fechaHoraReporte) {
		this.fechaHoraReporte = fechaHoraReporte;
	}

	/**
	 * @return the fechaHoraTerminacion
	 */
	public Date getFechaHoraTerminacion() {
		return fechaHoraTerminacion;
	}

	/**
	 * @param fechaHoraTerminacion the fechaHoraTerminacion to set
	 */
	public void setFechaHoraTerminacion(Date fechaHoraTerminacion) {
		this.fechaHoraTerminacion = fechaHoraTerminacion;
	}
	
	/**
	 * @return the citaReporteCabina
	 */
	public CitaReporteCabina getCitaReporteCabina() {
		return citaReporteCabina;
	}

	/**
	 * @param citaReporteCabina the citaReporteCabina to set
	 */
	public void setCitaReporteCabina(CitaReporteCabina citaReporteCabina) {
		this.citaReporteCabina = citaReporteCabina;	 
	}
		  
   /**
	 * @return the lugarAtencion
	 */
	public LugarSiniestroMidas getLugarAtencion() {
		return lugarAtencion;
	}

	/**
	 * @param lugarAtencion the lugarAtencion to set
	 */
	public void setLugarAtencion(LugarSiniestroMidas lugarAtencion) {
		this.lugarAtencion = lugarAtencion;
	}

	/**
	 * @return the lugarOcurrido
	 */
	public LugarSiniestroMidas getLugarOcurrido() {
		return lugarOcurrido;
	}

	/**
	 * @param lugarOcurrido the lugarOcurrido to set
	 */
	public void setLugarOcurrido(LugarSiniestroMidas lugarOcurrido) {
		this.lugarOcurrido = lugarOcurrido;
	}

	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof ReporteCabina) {
			ReporteCabina reporteCabina = (ReporteCabina) object;
			equal = reporteCabina.getId().equals(this.id);
		}
		return equal;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	public Oficina getOficina() {
		return oficina;
	}

	public void setOficina(Oficina oficina) {
		this.oficina = oficina;
	}

	public String getNumeroReporte() {
		numeroReporte = 
			this.getClaveOficina() + "-" + 
			this.getConsecutivoReporte() + "-" + 
			this.getAnioReporte();

		return numeroReporte;
	}

	public void setNumeroReporte(String numeroReporte) {
		this.numeroReporte = numeroReporte;
	}

	public String getConsecutivoReporte() {
		return consecutivoReporte;
	}

	public void setConsecutivoReporte(String consecutivoReporte) {
		this.consecutivoReporte = consecutivoReporte;
	}

	public String getAnioReporte() {
		return anioReporte;
	}

	public void setAnioReporte(String anioReporte) {
		this.anioReporte = anioReporte;
	}

	public String getAtendidoPor() {
		return atendidoPor;
	}

	public void setAtendidoPor(String atendidoPor) {
		this.atendidoPor = atendidoPor;
	}

	public PolizaDTO getPoliza() {
		return poliza;
	}

	public void setPoliza(PolizaDTO poliza) {
		this.poliza = poliza;
	}

	public String getIdToSolicitud() {
		return idToSolicitud;
	}

	public void setIdToSolicitud(String idToSolicitud) {
		this.idToSolicitud = idToSolicitud;
	}

	public List<GastoAjusteReporte> getGastosAjuste() {
		return gastosAjuste;
	}

	public void setGastosAjuste(List<GastoAjusteReporte> gastosAjuste) {
		this.gastosAjuste = gastosAjuste;
	}

	public ServicioSiniestro getAjustador() {
		return ajustador;
	}

	public void setAjustador(ServicioSiniestro ajustador) {
		this.ajustador = ajustador;
	}

	/**
	 * @return the seccionReporteCabina
	 */
	public SeccionReporteCabina getSeccionReporteCabina() {
		return seccionReporteCabina;
	}

	/**
	 * @param seccionReporteCabina the seccionReporteCabina to set
	 */
	public void setSeccionReporteCabina(SeccionReporteCabina seccionReporteCabina) {
		this.seccionReporteCabina = seccionReporteCabina;
	}
	
	public String getClaveOficina() {
		return claveOficina;
	}

	public void setClaveOficina(String claveOficina) {
		this.claveOficina = claveOficina;
	}
	
	public BigDecimal getAgenteId() {
		return agenteId;
	}

	public void setAgenteId(BigDecimal agenteId) {
		this.agenteId = agenteId;
	}	

	public MonedaDTO getMoneda() {
		return moneda;
	}

	public void setMoneda(MonedaDTO moneda) {
		this.moneda = moneda;
	}

	public String getNombreAgente() {
		return nombreAgente;
	}

	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}

	public Long getPersonaContratanteId() {
		return personaContratanteId;
	}

	public void setPersonaContratanteId(Long personaContratanteId) {
		this.personaContratanteId = personaContratanteId;
	}

	public SiniestroCabina getSiniestroCabina() {
		return siniestroCabina;
	}

	public void setSiniestroCabina(SiniestroCabina siniestroCabina) {
		this.siniestroCabina = siniestroCabina;
	}
	
	public String getClaveNegocio() {
		return claveNegocio!=null?this.claveNegocio.toUpperCase():this.claveNegocio;
	}

	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}	

	public String getTipoCotizacion() {
		return tipoCotizacion;
	}

	public void setTipoCotizacion(String tipoCotizacion) {
		this.tipoCotizacion = tipoCotizacion;
	}
	
	public ReporteSiniestroMovil getReporteMovil() {
		return reporteMovil;
	}

	public void setReporteMovil(ReporteSiniestroMovil reporteMovil) {
		this.reporteMovil = reporteMovil;
	}

	public Date getFechaHoraOcurrido() {
		try{
			DateTime dateTime = new DateTime(this.fechaOcurrido);
			String[] horaMinutos = this.horaOcurrido.split(":");
			String horas = horaMinutos[0];
			String minutos = horaMinutos[1];
			
			dateTime = dateTime.plusHours(Integer.parseInt(horas));
			dateTime = dateTime.plusMinutes(Integer.parseInt(minutos));
			
			this.fechaHoraOcurrido = dateTime.toDate();
		}catch(Exception ex){			
		}
		return fechaHoraOcurrido;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}	
	
	
	
	
}
