/**
 * 
 */
package mx.com.afirme.midas2.domain.siniestros.indemnizacion;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * @author admin
 *
 */
@Entity(name = "RolesIndemnizacionAutorizacion")
@Table(name = "TOROLAUTORIZAINDEMNIZACION", schema = "MIDAS")
public class RolesIndemnizacionAutorizacion implements Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name = "TOROLAUTORIZAINDEMNIZACION_SEQ_GENERADOR",allocationSize = 1, sequenceName = "TOROLAUTORIZAINDEMNIZACION_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TOROLAUTORIZAINDEMNIZACION_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id;
	
	@Column(name="ROL")
	private String rol;
	
	@Column(name="TIPO")
	private String tipo;
	
	@Column(name="AUTORIZACION_FINAL")
	private String autorizacionFinal;
	
	@Column(name="REQUERIDA")
	private String requerida;
	
	@Column(name="ACTIVO")
	private String activa;
	
	@Column(name="TIPOSINIESTRO")
	private String tipoSiniestro;
	
	@Column(name="DESC_ROL")
	private String descripcionRol;
	
	@Column(name="ROL_SUPLENTE")
	private String rolSuplente;
	
	public RolesIndemnizacionAutorizacion() {
		super();
	}

	public RolesIndemnizacionAutorizacion(String rol, String descripcionRol) {
		super();
		this.rol = rol;
		this.descripcionRol = descripcionRol;
	}


	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getRol() {
		return rol;
	}


	public void setRol(String rol) {
		this.rol = rol;
	}


	public String getTipo() {
		return tipo;
	}


	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	public String getAutorizacionFinal() {
		return autorizacionFinal;
	}


	public void setAutorizacionFinal(String autorizacionFinal) {
		this.autorizacionFinal = autorizacionFinal;
	}


	public String getRequerida() {
		return requerida;
	}


	public void setRequerida(String requerida) {
		this.requerida = requerida;
	}


	public String getActiva() {
		return activa;
	}


	public void setActiva(String activa) {
		this.activa = activa;
	}


	public String getTipoSiniestro() {
		return tipoSiniestro;
	}


	public void setTipoSiniestro(String tipoSiniestro) {
		this.tipoSiniestro = tipoSiniestro;
	}


	public String getDescripcionRol() {
		return descripcionRol;
	}


	public void setDescripcionRol(String descripcionRol) {
		this.descripcionRol = descripcionRol;
	}


	/**
	 * @return the rolSuplente
	 */
	public String getRolSuplente() {
		return rolSuplente;
	}


	/**
	 * @param rolSuplente the rolSuplente to set
	 */
	public void setRolSuplente(String rolSuplente) {
		this.rolSuplente = rolSuplente;
	}
	
}
