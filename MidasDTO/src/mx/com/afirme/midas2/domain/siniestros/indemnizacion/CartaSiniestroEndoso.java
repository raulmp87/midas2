package mx.com.afirme.midas2.domain.siniestros.indemnizacion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;

@Entity
@Table(name="TOCARTASINIESTROENDOSO", schema="MIDAS")
public class CartaSiniestroEndoso extends MidasAbstracto implements Entidad{

	private static final long serialVersionUID = 4688282682064290885L;

	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTOCARTASINIESTROENDOSO_GENERATOR")
	@SequenceGenerator(name="IDTOCARTASINIESTROENDOSO_GENERATOR", sequenceName="IDTOCARTASINIESTROENDOSO_SEQ", schema="MIDAS", allocationSize=1)
	private Long id;
	
	@Column(name="NOMBRE_ENDOSO", nullable=false)
	private String nombreEndoso;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CARTASINIESTROFACTURA", nullable=false)
	private CartaSiniestroFactura cartaSiniestroFactura;
	
	@Transient
	private Integer endosoSeleccionado;
	
	/**
	 * @return the nombreEndoso
	 */
	public String getNombreEndoso() {
		return nombreEndoso;
	}

	/**
	 * @param nombreEndoso the nombreEndoso to set
	 */
	public void setNombreEndoso(String nombreEndoso) {
		this.nombreEndoso = nombreEndoso;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	@SuppressWarnings("unchecked")
	public Long getKey() {
		return id;
	}


	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	public Long getBusinessKey() {
		return id;
	}

	/**
	 * @return the endosoSeleccionado
	 */
	public Integer getEndosoSeleccionado() {
		return endosoSeleccionado;
	}

	/**
	 * @param endosoSeleccionado the endosoSeleccionado to set
	 */
	public void setEndosoSeleccionado(Integer endosoSeleccionado) {
		this.endosoSeleccionado = endosoSeleccionado;
	}

	/**
	 * @return the cartaSiniestroFactura
	 */
	public CartaSiniestroFactura getCartaSiniestroFactura() {
		return cartaSiniestroFactura;
	}

	/**
	 * @param cartaSiniestroFactura the cartaSiniestroFactura to set
	 */
	public void setCartaSiniestroFactura(CartaSiniestroFactura cartaSiniestroFactura) {
		this.cartaSiniestroFactura = cartaSiniestroFactura;
	}
	
}