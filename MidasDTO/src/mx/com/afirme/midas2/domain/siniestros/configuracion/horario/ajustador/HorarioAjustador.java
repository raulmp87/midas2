package mx.com.afirme.midas2.domain.siniestros.configuracion.horario.ajustador;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dao.catalogos.EnumBase;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.laboral.HorarioLaboral;

import org.apache.bval.constraints.NotEmpty;

@SuppressWarnings("unchecked")
@Entity
@Table(name = "TOHORARIOAJUSTADOR", schema = "MIDAS")
public class HorarioAjustador extends MidasAbstracto {
	private static final long			serialVersionUID		= 1L;

	static public Map<String, String>	ctgDisponibilidad		= new HashMap<String, String>();
	static public TIPO_CATALOGO			ctgDisponibilidadTipo	= TIPO_CATALOGO.TIPO_DISPONIBILIDAD;

	static private Map<String, String>	mapVal					= new HashMap<String, String>();

	public enum TipoDisponibilidad implements EnumBase<String> {
		Normal("N", true), Guardia("G", true), Apoyo("A", true), Vacaciones(
				"V", false), Permiso("P", false), Incapacidad("I", false), Descanso(
				"D", false);

		private String	codigo;
		private Boolean	disponible;

		TipoDisponibilidad(String codigo, Boolean disponible) {
			this.codigo = codigo;
			this.disponible = disponible;
		}

		@Override
		public String getValue() {
			// return this.codigo;
			// val obtenido de la bd
			for (Map.Entry<String, String> entry : ctgDisponibilidad.entrySet()) {
				mapVal.put(entry.getValue(), entry.getKey());
			}
			return mapVal.get(this.toString());
		}

		public String getCodigo() {
			return this.codigo;
		}

		public Boolean estaDisponible() {
			return this.disponible;
		}

		@Override
		public String getLabel() {
			return this.toString();
		}
	}

	@EmbeddedId
	private HorarioAjustadorId	id				= new HorarioAjustadorId();

	@MapsId("ajustadorId")
	@JoinColumn(name = "AJUSTADOR_ID", referencedColumnName = "ID")
	@ManyToOne(fetch = FetchType.LAZY)
	private ServicioSiniestro	ajustador;

	@MapsId("oficinaId")
	@JoinColumn(name = "OFICINA_ID", referencedColumnName = "ID")
	@ManyToOne(fetch = FetchType.LAZY)
	private Oficina				oficina;

	@MapsId("horarioLaboralId")
	@JoinColumn(name = "HORARIOLABORAL_ID", referencedColumnName = "ID")
	@ManyToOne(fetch = FetchType.LAZY)
	private HorarioLaboral		horarioLaboral	= new HorarioLaboral();

	@NotNull
	@NotEmpty(message = "{org.hibernate.validator.constraints.NotEmpty.message}")
	@Column(name = "TIPO_DISPONIBILIDAD")
	private String				tipoDisponibilidadCode;

	@Column(name = "COMENTARIOS")
	private String				comentarios;

	@Transient
	private String				horaEntradaCode;

	@Transient
	private String				horaSalidaCode;

	@Transient
	private Date				fechaHoraEntrada;

	@Transient
	private Date				fechaHoraSalida;

	public HorarioAjustadorId getId() {
		return id;
	}

	public void setId(HorarioAjustadorId id) {
		this.id = id;
	}

	public void setAjustador(ServicioSiniestro ajustador) {
		this.ajustador = ajustador;
	}

	public ServicioSiniestro getAjustador() {
		return ajustador;
	}

	public void setOficina(Oficina oficina) {
		this.oficina = oficina;
	}

	public Oficina getOficina() {
		return oficina;
	}

	public void setHorarioLaboral(HorarioLaboral horarioLaboral) {
		this.horarioLaboral = horarioLaboral;
	}

	public HorarioLaboral getHorarioLaboral() {
		return horarioLaboral;
	}

	public void setTipoDisponibilidadCode(String tipoDisponibilidadCode) {
		this.tipoDisponibilidadCode = tipoDisponibilidadCode;
	}

	public String getTipoDisponibilidadCode() {
		return tipoDisponibilidadCode;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public String getComentarios() {
		return comentarios;
	}

	@Transient
	public Date getFecha() {
		return id.getFecha();
	}

	public void setFecha(Date fecha) {
		this.id.setFecha(fecha);
	}

	// region - campos transientes para calculo de fechas
	@Transient
	public String getHoraEntradaCode() {
		if (horaEntradaCode == null)
			horaEntradaCode = horarioLaboral.getHoraEntradaCode();
		return horaEntradaCode;
	}

	@Transient
	public String getHoraSalidaCode() {
		if (horaSalidaCode == null)
			horaSalidaCode = horarioLaboral.getHoraSalidaCode();
		return horaSalidaCode;
	}

	@Transient
	private static final long	millPorSegundo	= 1000L;
	@Transient
	private static final long	segundosPorDia	= 86400L;

	@Transient
	public Date getFechaHoraEntrada() {
		if (fechaHoraEntrada == null) {
			long entradaCode = Long.parseLong(getHoraEntradaCode());
			long dateGetTime = (entradaCode) * millPorSegundo;
			
			fechaHoraEntrada = new Date(id.getFecha().getTime() + dateGetTime);
		}
		return fechaHoraEntrada;
	}

	/**
	 * 
	 * <p>
	 * Funcion que se ocupa para evitar el problema de los horarios de un dia
	 * para otro.
	 * </p>
	 * <p>
	 * Funcion transiente que calcula la fecha hora de salida deacuerdo a la
	 * fecha y hora de entrada.
	 * </p>
	 * 
	 * Si la hora de salida es menor (horario laboral transnochador) o igual
	 * (horario laboral de 24hrs) la hora de entrada, sumarle un dia entero
	 * (segundosPorDia) a el codigo (hora en segundos), y multiplicarlo por la
	 * cantidad de milisegundos en segundos para que represente "la hora" del
	 * siguiente dia (Date.getTime())
	 * 
	 * @return
	 */
	@Transient
	public Date getFechaHoraSalida() {
		if (fechaHoraSalida == null) {
			long ajuste = 0;
			long entradaCode = Long.parseLong(getHoraEntradaCode());
			long salidaCode = Long.parseLong(getHoraSalidaCode());
			if (salidaCode <= entradaCode) {
				ajuste = segundosPorDia;
			}
			long dateGetTime = (salidaCode + ajuste) * millPorSegundo;
			fechaHoraSalida = new Date(id.getFecha().getTime() + dateGetTime);
		}
		return fechaHoraSalida;
	}

	// endregion - campos transientes para calculo de fechas

	@Override
	public String getValue() {
		return this.toString();
	}

	@Override
	public HorarioAjustadorId getKey() {
		return id;
	}

	@Override
	public HorarioAjustadorId getBusinessKey() {
		return id;
	}

	@Transient
	public TipoDisponibilidad getTipoDispEnum() {
		if (tipoDisponibilidadCode != null
				&& !tipoDisponibilidadCode.equals("")) {
			String enumVal = HorarioAjustador.ctgDisponibilidad
					.get(tipoDisponibilidadCode);
			return TipoDisponibilidad.valueOf(enumVal);
		} else {
			return null;
		}
	}

}
