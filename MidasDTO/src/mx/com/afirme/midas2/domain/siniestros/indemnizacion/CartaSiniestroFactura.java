package mx.com.afirme.midas2.domain.siniestros.indemnizacion;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;

@Entity
@Table(name="TOCARTASINIESTROFACTURA", schema="MIDAS")
public class CartaSiniestroFactura extends MidasAbstracto implements Entidad{

	private static final long serialVersionUID = 8253291604748098236L;

	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTOCARTASINIESTROFACTURA_GENERATOR")
	@SequenceGenerator(name="IDTOCARTASINIESTROFACTURA_GENERATOR", sequenceName="IDTOCARTASINIESTROFACTURA_SEQ", schema="MIDAS", allocationSize=1)
	private Long id;
	
	@Column(name="NUMERO_FACTURA")
	private String numeroFactura;
	
	@Column(name="EMISOR_FACTURA")
	private String emisorFactura;
	
	@Column(name="RECEPTOR_FACTURA")
	private String receptorFactura;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CARTASINIESTRO", nullable=false)
	private CartaSiniestro cartaSiniestro;
	
	@OneToMany(mappedBy="cartaSiniestroFactura", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<CartaSiniestroEndoso> endosos;
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the numeroFactura
	 */
	public String getNumeroFactura() {
		return numeroFactura;
	}

	/**
	 * @param numeroFactura the numeroFactura to set
	 */
	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	/**
	 * @return the emisorFactura
	 */
	public String getEmisorFactura() {
		return emisorFactura;
	}

	/**
	 * @param emisorFactura the emisorFactura to set
	 */
	public void setEmisorFactura(String emisorFactura) {
		this.emisorFactura = emisorFactura;
	}

	/**
	 * @return the receptorFactura
	 */
	public String getReceptorFactura() {
		return receptorFactura;
	}

	/**
	 * @param receptorFactura the receptorFactura to set
	 */
	public void setReceptorFactura(String receptorFactura) {
		this.receptorFactura = receptorFactura;
	}
	
	/**
	 * @return the cartaSiniestro
	 */
	public CartaSiniestro getCartaSiniestro() {
		return cartaSiniestro;
	}

	/**
	 * @param cartaSiniestro the cartaSiniestro to set
	 */
	public void setCartaSiniestro(CartaSiniestro cartaSiniestro) {
		this.cartaSiniestro = cartaSiniestro;
	}

	/**
	 * @return the endosos
	 */
	public List<CartaSiniestroEndoso> getEndosos() {
		return endosos;
	}

	/**
	 * @param endosos the endosos to set
	 */
	public void setEndosos(List<CartaSiniestroEndoso> endosos) {
		this.endosos = endosos;
	}

	@SuppressWarnings("unchecked")
	public Long getKey() {
		return id;
	}


	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	public Long getBusinessKey() {
		return id;
	}

}