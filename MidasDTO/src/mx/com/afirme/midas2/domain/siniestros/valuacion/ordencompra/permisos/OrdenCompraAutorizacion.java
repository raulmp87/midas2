/**
 * 
 */
package mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.permisos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.DetalleOrdenCompra;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;

/**
 * @author admin
 *
 */
@Entity(name = "OrdenCompraAutorizacion")
@Table(name = "TOORDENCOMPRAAUTORIZACION", schema = "MIDAS")
public class OrdenCompraAutorizacion implements Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private OrdenCompraAutorizacionId id;
	
	@Column(name="ESTATUS")
	private String estatus;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_AUTORIZACION")
	private Date fechaAutorizacion;
	
	
	@Column(name="COMENTARIOS")
	private String comentarios ;
	
	@Column(name="CODIGO_USUARIO")
	private String codigoUsuario ;
	
	@MapsId("idDetalleOrdenCOmpra")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_DETALLE_ORDEN_COMPRA", referencedColumnName = "ID")
	private DetalleOrdenCompra detalleOrdenCompra;
	
	@MapsId("idOrdenCompra")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_ORDEN_COMPRA", referencedColumnName = "ID")
	private OrdenCompra orderCompra;

	
	/**
	 * @return the id
	 */
	public OrdenCompraAutorizacionId getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(OrdenCompraAutorizacionId id) {
		this.id = id;
	}

	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return the fechaAutorizacion
	 */
	public Date getFechaAutorizacion() {
		return fechaAutorizacion;
	}

	/**
	 * @param fechaAutorizacion the fechaAutorizacion to set
	 */
	public void setFechaAutorizacion(Date fechaAutorizacion) {
		this.fechaAutorizacion = fechaAutorizacion;
	}

	/**
	 * @return the comentarios
	 */
	public String getComentarios() {
		return comentarios;
	}

	/**
	 * @param comentarios the comentarios to set
	 */
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	/**
	 * @return the codigoUsuario
	 */
	public String getCodigoUsuario() {
		return codigoUsuario;
	}

	/**
	 * @param codigoUsuario the codigoUsuario to set
	 */
	public void setCodigoUsuario(String codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}

	/**
	 * @return the detalleOrdenCompra
	 */
	public DetalleOrdenCompra getDetalleOrdenCompra() {
		return detalleOrdenCompra;
	}

	/**
	 * @param detalleOrdenCompra the detalleOrdenCompra to set
	 */
	public void setDetalleOrdenCompra(DetalleOrdenCompra detalleOrdenCompra) {
		this.detalleOrdenCompra = detalleOrdenCompra;
	}

	/**
	 * @return the orderCompra
	 */
	public OrdenCompra getOrderCompra() {
		return orderCompra;
	}

	/**
	 * @param orderCompra the orderCompra to set
	 */
	public void setOrderCompra(OrdenCompra orderCompra) {
		this.orderCompra = orderCompra;
	}

	@SuppressWarnings("unchecked")
	@Override
	public OrdenCompraAutorizacionId getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public OrdenCompraAutorizacionId getBusinessKey() {
		return id;
	}
	
	
	
	

}
