package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;


@Entity(name = "TerceroRCPersonas")
@Table(name = "TOTERCERORCPERSONAS", schema = "MIDAS")
@DiscriminatorValue("RCP")
@PrimaryKeyJoinColumn(name = "ESTIMACIONCOBERTURAREPCAB_ID", referencedColumnName = "ID")
public class TerceroRCPersonas extends EstimacionCoberturaReporteCabina {

	private static final long serialVersionUID = 1L;

	@Column(name="DESCRIPCION")
	private String descripcion;
	
	@Column(name="OTRO_HOSPITAL")
	private String otroHospital;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinFetch (JoinFetchType.INNER)
	@JoinColumn(name = "HOSPITAL_ID", referencedColumnName = "ID")
	private PrestadorServicio hospital;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinFetch (JoinFetchType.INNER)
	@JoinColumn(name = "MEDICO_ID", referencedColumnName = "ID")
	private PrestadorServicio medico;
	
	@Column(name="TIPO_ATENCION")
	private String tipoAtencion;
	
	@Column(name="ESTADO_ID")
	private String estado;
	
	@Column(name="EDAD")
	private Integer edad;
	

	public TerceroRCPersonas(){
	}
	
	

	public String getDescripcion() {
		return descripcion;
	}



	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}



	public String getOtroHospital() {
		return otroHospital;
	}



	public void setOtroHospital(String otroHospital) {
		this.otroHospital = otroHospital;
	}



	public PrestadorServicio getHospital() {
		return hospital;
	}



	public void setHospital(PrestadorServicio hospital) {
		this.hospital = hospital;
	}



	public PrestadorServicio getMedico() {
		return medico;
	}



	public void setMedico(PrestadorServicio medico) {
		this.medico = medico;
	}

	
	
	public String getTipoAtencion() {
		return tipoAtencion;
	}



	public void setTipoAtencion(String tipoAtencion) {
		this.tipoAtencion = tipoAtencion;
	}



	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	


}