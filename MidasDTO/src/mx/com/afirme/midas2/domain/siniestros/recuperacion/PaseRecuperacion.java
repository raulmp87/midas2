package mx.com.afirme.midas2.domain.siniestros.recuperacion;


import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;



@Entity(name = "PaseRecuperacion")
@Table(name = "TRSNPASERECUPERACION", schema = "MIDAS")
public class PaseRecuperacion implements Serializable, Entidad {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6828965659349709757L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COBERTURARECUPERACION_ID", referencedColumnName = "ID")
	private CoberturaRecuperacion coberturaRecuperacion;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ESTIMACIONREPORTE_ID", referencedColumnName = "ID")
	private EstimacionCoberturaReporteCabina estimacionCobertura;
	
	@EmbeddedId
	@AttributeOverrides( {
	        @AttributeOverride(name="coberturaId", column=@Column(name="COBERTURARECUPERACION_ID", nullable=false, insertable=false, updatable=false) ), 
	        @AttributeOverride(name="estimacionId", column=@Column(name="ESTIMACIONREPORTE_ID", nullable=false, insertable=false, updatable=false) )} )
	private PaseRecuperacionId id =  new PaseRecuperacionId();
	
	public PaseRecuperacion() {
		super();
	
	}
	
	public PaseRecuperacion(CoberturaRecuperacion coberturaRecuperacion, EstimacionCoberturaReporteCabina estimacionCobertura) {
		this.coberturaRecuperacion = coberturaRecuperacion;
		this.estimacionCobertura = estimacionCobertura;
		
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public PaseRecuperacionId getKey() {
		return id;
	}


	@Override
	public String getValue() {
		return null;
	}


	@SuppressWarnings("unchecked")
	@Override
	public PaseRecuperacionId getBusinessKey() {
		return id;
	}


	public CoberturaRecuperacion getCoberturaRecuperacion() {
		return coberturaRecuperacion;
	}


	public void setCoberturaRecuperacion(CoberturaRecuperacion coberturaRecuperacion) {
		this.coberturaRecuperacion = coberturaRecuperacion;
	}


	public EstimacionCoberturaReporteCabina getEstimacionCobertura() {
		return estimacionCobertura;
	}


	public void setEstimacionCobertura(
			EstimacionCoberturaReporteCabina estimacionCobertura) {
		this.estimacionCobertura = estimacionCobertura;
	}

	public PaseRecuperacionId getId() {
		return id;
	}

	public void setId(PaseRecuperacionId id) {
		this.id = id;
	}

	

}