package mx.com.afirme.midas2.domain.siniestros.liquidacion.parametros;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;


@Entity
@Table(name = "TOPARAMAUTORIZALIQUIDACION", schema = "MIDAS")
public class ParametroAutorizacionLiquidacion extends MidasAbstracto implements Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TOPARAMAUTORIZALIQ_SEQ")
	@SequenceGenerator(name = "TOPARAMAUTORIZALIQ_SEQ",  schema="MIDAS", sequenceName = "TOPARAMAUTORIZALIQ_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "NUMERO")
	private Long numero;
	
	@Column(name = "NOMBRE_CONFIGURACION")
	private String nombreConfiguracion;
	
	@Column(name = "ORIGEN_LIQUIDACION")
	private String origenLiquidacion;
	
	@Column(name = "TIPO_LIQUIDACION")
	private String tipoLiquidacion;
	
	@Column(name = "TIPO_PAGO")
	private String tipoPago;
	
	@Column(name = "TIPO_SERVICIO")
	private Integer tipoServicio;
	
	@Column(name = "ESTATUS")
	private Boolean estatus;
	
	@Column(name = "CONFIGURACION_MONTO")
	private Boolean configuracionMonto;
	
	@Column(name = "CONDICION_MONTO")
	private String condicionMonto;
	
	@Column(name = "CRITERIO_RANGO_MONTO")
	private Boolean criterioRangoMonto;
	
	@Column(name = "MONTO_INICIAL")
	private BigDecimal montoInicial;
	
	@Column(name = "MONTO_FINAL")
	private BigDecimal montoFinal;
	
	@Column(name = "CONFIGURACION_VIGENCIA")
	private Boolean configuracionVigencia;
	
	@Column(name = "CONDICION_VIGENCIA")
	private String condicionVigencia;
	
	@Column(name = "CRITERIO_RANGO_VIGENCIA")
	private Boolean criterioRangoVigencia;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "VIGENCIA_INICIAL")
	private Date vigenciaInicial;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "VIGENCIA_FINAL")
	private Date vigenciaFinal;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_INACTIVO")
	private Date fechaInactivo;
	
	@Column(name = "NOMBRE_USUARIO_CONFIGURADOR")
	private String nombreUsuarioConfigurador;
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="parametro")
	private List<ParametroAutorizacionLiquidacionDetalle> detalles;
	
	
	public ParametroAutorizacionLiquidacion(){

	}

	
	public Long getId() {
		return id;
	}




	public void setId(Long id) {
		this.id = id;
	}




	public Long getNumero() {
		return numero;
	}




	public void setNumero(Long numero) {
		this.numero = numero;
	}




	public String getNombreConfiguracion() {
		return nombreConfiguracion;
	}




	public void setNombreConfiguracion(String nombreConfiguracion) {
		this.nombreConfiguracion = nombreConfiguracion;
	}




	public String getOrigenLiquidacion() {
		return origenLiquidacion;
	}




	public void setOrigenLiquidacion(String origenLiquidacion) {
		this.origenLiquidacion = origenLiquidacion;
	}




	public String getTipoLiquidacion() {
		return tipoLiquidacion;
	}




	public void setTipoLiquidacion(String tipoLiquidacion) {
		this.tipoLiquidacion = tipoLiquidacion;
	}




	public String getTipoPago() {
		return tipoPago;
	}




	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}




	public Integer getTipoServicio() {
		return tipoServicio;
	}




	public void setTipoServicio(Integer tipoServicio) {
		this.tipoServicio = tipoServicio;
	}




	public Boolean getEstatus() {
		return estatus;
	}




	public void setEstatus(Boolean estatus) {
		this.estatus = estatus;
	}




	public Boolean getConfiguracionMonto() {
		return configuracionMonto;
	}




	public void setConfiguracionMonto(Boolean configuracionMonto) {
		this.configuracionMonto = configuracionMonto;
	}




	public String getCondicionMonto() {
		return condicionMonto;
	}




	public void setCondicionMonto(String condicionMonto) {
		this.condicionMonto = condicionMonto;
	}




	public Boolean getCriterioRangoMonto() {
		return criterioRangoMonto;
	}




	public void setCriterioRangoMonto(Boolean criterioRangoMonto) {
		this.criterioRangoMonto = criterioRangoMonto;
	}




	public BigDecimal getMontoInicial() {
		return montoInicial;
	}




	public void setMontoInicial(BigDecimal montoInicial) {
		this.montoInicial = montoInicial;
	}




	public BigDecimal getMontoFinal() {
		return montoFinal;
	}




	public void setMontoFinal(BigDecimal montoFinal) {
		this.montoFinal = montoFinal;
	}




	public Boolean getConfiguracionVigencia() {
		return configuracionVigencia;
	}




	public void setConfiguracionVigencia(Boolean configuracionVigencia) {
		this.configuracionVigencia = configuracionVigencia;
	}




	public String getCondicionVigencia() {
		return condicionVigencia;
	}




	public void setCondicionVigencia(String condicionVigencia) {
		this.condicionVigencia = condicionVigencia;
	}




	public Boolean getCriterioRangoVigencia() {
		return criterioRangoVigencia;
	}




	public void setCriterioRangoVigencia(Boolean criterioRangoVigencia) {
		this.criterioRangoVigencia = criterioRangoVigencia;
	}




	public Date getVigenciaInicial() {
		return vigenciaInicial;
	}




	public void setVigenciaInicial(Date vigenciaInicial) {
		this.vigenciaInicial = vigenciaInicial;
	}




	public Date getVigenciaFinal() {
		return vigenciaFinal;
	}




	public void setVigenciaFinal(Date vigenciaFinal) {
		this.vigenciaFinal = vigenciaFinal;
	}




	public Date getFechaInactivo() {
		return fechaInactivo;
	}




	public void setFechaInactivo(Date fechaInactivo) {
		this.fechaInactivo = fechaInactivo;
	}




	public List<ParametroAutorizacionLiquidacionDetalle> getDetalles() {
		return detalles;
	}




	public void setDetalles(List<ParametroAutorizacionLiquidacionDetalle> detalles) {
		this.detalles = detalles;
	}


	/**
	 * @return the nombreUsuarioConfigurador
	 */
	public String getNombreUsuarioConfigurador() {
		return nombreUsuarioConfigurador;
	}


	/**
	 * @param nombreUsuarioConfigurador the nombreUsuarioConfigurador to set
	 */
	public void setNombreUsuarioConfigurador(String nombreUsuarioConfigurador) {
		this.nombreUsuarioConfigurador = nombreUsuarioConfigurador;
	}


	public void finalize() throws Throwable {
		super.finalize();
	}

	@SuppressWarnings("unchecked")
	public Long getBusinessKey(){
		return null;
	}

	@SuppressWarnings("unchecked")
	public Long getKey(){
		return this.id;
	}

	public String getValue(){
		return "";
	}

}