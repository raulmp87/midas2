/**
 * 
 */
package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina;

import java.io.Serializable;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.SolicitudDataEnTramiteDTO;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;

import org.springframework.stereotype.Component;

/**
 * @author simavera
 *
 */
@Component
public class SolicitudReporteCabinaDTO implements Serializable{

	private static final long serialVersionUID = 7817745003786365161L;
	private SolicitudDTO solicitud;
	private SolicitudDataEnTramiteDTO solicitudDataEnTramite;
	private DocumentoDigitalSolicitudDTO documentoDigitalSolicitudDTO;
	
	/**
	 * @return the solicitud
	 */
	public SolicitudDTO getSolicitud() {
		return solicitud;
	}
	/**
	 * @param solicitud the solicitud to set
	 */
	public void setSolicitud(SolicitudDTO solicitud) {
		this.solicitud = solicitud;
	}
	/**
	 * @return the solicitudDataEnTramite
	 */
	public SolicitudDataEnTramiteDTO getSolicitudDataEnTramite() {
		return solicitudDataEnTramite;
	}
	/**
	 * @param solicitudDataEnTramite the solicitudDataEnTramite to set
	 */
	public void setSolicitudDataEnTramite(
			SolicitudDataEnTramiteDTO solicitudDataEnTramite) {
		this.solicitudDataEnTramite = solicitudDataEnTramite;
	}
	public DocumentoDigitalSolicitudDTO getDocumentoDigitalSolicitudDTO() {
		return documentoDigitalSolicitudDTO;
	}
	public void setDocumentoDigitalSolicitudDTO(
			DocumentoDigitalSolicitudDTO documentoDigitalSolicitudDTO) {
		this.documentoDigitalSolicitudDTO = documentoDigitalSolicitudDTO;
	}
	
	
	
}
