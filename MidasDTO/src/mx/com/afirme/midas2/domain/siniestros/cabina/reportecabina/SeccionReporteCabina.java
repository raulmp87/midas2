/**
 * 
 */
package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas2.domain.MidasAbstracto;

/**
 * @author simavera
 *
 */
@Entity(name = "SeccionReporteCabina")
@Table(name = "TOSECCIONREPORTECABINA", schema = "MIDAS")
public class SeccionReporteCabina extends MidasAbstracto{

	private static final long serialVersionUID = 3959641699892103423L;

	@Id
	@SequenceGenerator(name = "IDSECCIONREPORTECABINA_SEQ_GENERADOR",allocationSize = 1, sequenceName = "TOSECCIONREPORTECABINA_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "IDSECCIONREPORTECABINA_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="REPORTECABINA_ID")
	private ReporteCabina reporteCabina;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="IDTOSECCION", referencedColumnName="IDTOSECCION")
	private SeccionDTO seccionDTO;
	
	@OneToOne(fetch=FetchType.LAZY, mappedBy="seccionReporteCabina", cascade = CascadeType.ALL)
	private IncisoReporteCabina incisoReporteCabina;
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the reporteCabina
	 */
	public ReporteCabina getReporteCabina() {
		return reporteCabina;
	}
	/**
	 * @param reporteCabina the reporteCabina to set
	 */
	public void setReporteCabina(ReporteCabina reporteCabina) {
		this.reporteCabina = reporteCabina;
	}
	/**
	 * @return the seccionDTO
	 */
	public SeccionDTO getSeccionDTO() {
		return seccionDTO;
	}
	/**
	 * @param seccionDTO the seccionDTO to set
	 */
	public void setSeccionDTO(SeccionDTO seccionDTO) {
		this.seccionDTO = seccionDTO;
	}
	
	/**
	 * @param incisoReporteCabina the incisoReporteCabina to set
	 */
	public void setIncisoReporteCabina(IncisoReporteCabina incisoReporteCabina) {
		this.incisoReporteCabina = incisoReporteCabina;
	}
	/**
	 * @return the incisoReporteCabina
	 */
	public IncisoReporteCabina getIncisoReporteCabina() {
		return incisoReporteCabina;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getId();
	}
	@Override
	public String getValue() {
		return null;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.getId();
	}
	
}
