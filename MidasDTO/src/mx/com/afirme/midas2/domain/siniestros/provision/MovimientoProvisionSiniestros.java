package mx.com.afirme.midas2.domain.siniestros.provision;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.EnumBase;
import mx.com.afirme.midas2.domain.MidasAbstracto;

@Entity
@Table(name = "TOSNMOVIMIENTO_PROVISION", schema = "MIDAS")
public class MovimientoProvisionSiniestros extends MidasAbstracto{
	
	/**
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MOV_PROVISION_SEQ")
	@SequenceGenerator(name = "MOV_PROVISION_SEQ",  schema="MIDAS", sequenceName = "MOV_PROVISION_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "ID_REF")
	private Long idRefrencia;
	
	@Column(name = "ORIGEN")
	private String origen;
	
	@Column(name = "CAUSA")
	private String causa;
	
	@Column(name = "MONTO_MOVIMIENTO")
	private BigDecimal montoMovimiento;
	
	@Column(name = "PROVISION_ACTUAL")
	private BigDecimal provisionActual;
	
	@Column(name = "ESTA_PROVISIONADO")
	private Boolean estaProvisionado;
	
	
	
	
	
	public MovimientoProvisionSiniestros() {
		super();
	}
	
	
	public MovimientoProvisionSiniestros(Long id, Long idRefrencia,
			String origen, String causa, BigDecimal montoMovimiento,
			BigDecimal provisionActual, Boolean estaProvisionado) {
		super();
		this.id = id;
		this.idRefrencia = idRefrencia;
		this.origen = origen;
		this.causa = causa;
		this.montoMovimiento = montoMovimiento;
		this.provisionActual = provisionActual;
		this.estaProvisionado = estaProvisionado;
	}




	public static enum Origen implements EnumBase<String>{
		PASE("PASE","Pase de Atencion"), 
		RECUPERACION_SALVAMENTO("REC_SVM","Recuperacion de Salvamento"),
		RECUPERACION_SALVAMENTO_IVA("REC_SV_IVA","IVA Recuperacion Salvamento");
		

		private String codigo;
		private String label;

		Origen(String codigo,String label){
			this.codigo = codigo;
			this.label = label;
		}			

		@Override
		public String toString(){
			return codigo;
		}

		
		@Override
		public String getValue() {
			return codigo;
		}

		@Override
		public String getLabel() {
			return this.label;
		}


	}
	
	
	public static enum CAUSA implements EnumBase<String>{
		AJUSTE_RESERVA("AJRES","Ajuste de Reserva"),
		EXCLUSION_CARTA_CIA("EXCCARCIA","Exclusion Carta Compania"), 
		GRUA("GRUA","Grua"),
		APLICACION_INGRESO_SALVAMENTO("APL_SVM","Aplicacion de Ingreso Salvamento"),
		APLICACION_INGRESO_PROVEEDOR("APL_PVD","Aplicacion de Ingreso Proveedor"),
		APLICACION_INGRESO_COMPANIA("APL_CIA","Aplicacion de Ingreso Compania"),
		CAMBIO_PORCENTAJE_PARTICIPACION("PORC_PART","Cambio de porcentaje de participacion"),
		INACTIVACION_DE_CIA("INAC_COM","Inactivacion de compañia"),
		ACTIVACION_DE_CIA("ACT_CIA","Activacion de compañia"),
		CANCELACION_RECUPERACION("CANC_REC","Cancelacion de Recuperacion"),
		ACTIVACION_DE_SVM("ACT_SVM","Activacion de Salvamento")
		;
		

		private String codigo;
		private String label;

		CAUSA(String codigo,String label){
			this.codigo = codigo;
			this.label = label;
		}			

		@Override
		public String toString(){
			return codigo;
		}

		
		@Override
		public String getValue() {
			return codigo;
		}

		@Override
		public String getLabel() {
			return this.label;
		}


	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdRefrencia() {
		return idRefrencia;
	}

	public void setIdRefrencia(Long idRefrencia) {
		this.idRefrencia = idRefrencia;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getCausa() {
		return causa;
	}

	public void setCausa(String causa) {
		this.causa = causa;
	}

	public BigDecimal getMontoMovimiento() {
		return montoMovimiento;
	}

	public void setMontoMovimiento(BigDecimal montoMovimiento) {
		this.montoMovimiento = montoMovimiento;
	}

	public BigDecimal getProvisionActual() {
		return provisionActual;
	}

	public void setProvisionActual(BigDecimal provisionActual) {
		this.provisionActual = provisionActual;
	}

	public Boolean getEstaProvisionado() {
		return estaProvisionado;
	}

	public void setEstaProvisionado(Boolean estaProvisionado) {
		this.estaProvisionado = estaProvisionado;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
