package mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.annotation.Exportable;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.personadireccion.PersonaMidas;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;


@Entity
@Table(name = "TCPRESTADORSERVICIO", schema = "MIDAS")
public class PrestadorServicio extends MidasAbstracto  implements Entidad {
	
	/**
	 */
	private static final long serialVersionUID = 1L;

	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCPRESTADORSERVICIO_SEQ")
	@SequenceGenerator(name = "IDTCPRESTADORSERVICIO_SEQ",  schema="MIDAS", sequenceName = "IDTCPRESTADORSERVICIO_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Integer id;
	
	@Column(name = "CIA_SEGUROS_POLIZA_RC")
	private String ciaSegurosPolizaRC;
	
	@Column(name = "ESTATUS")
	private Integer estatus;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_MOD_ESTATUS")
	private Date fechaModEstatus;
	
	@Column(name = "NUM_POLIZA_RC")
	private String numPolizaRC;
	
	@Column(name = "SERVICIOS_PROFESIONALES")
	private Integer serviciosProfesionales;
	
	@Temporal(TemporalType.DATE)
	@Column(name="VIGENCIA_POLIZA_RC")
	private Date vigenciaPolizaRC;
	
	@Transient
	private String estatusNombre;
	
	@Column(name = "DANIOSCA")
	private Integer daniosCa;
	
	@Column(name = "VIDACA")
	private Integer vidaCa;
	
	@Column(name = "AUTOSCA")
	private Integer autosCa;
	
	@Column(name = "FORMAPAGOCA")
	private Integer formaPagoCa;
	
	@OneToMany(cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinTable(name = "TOPRESTADORSERVINFBANCARIA", schema="MIDAS", 
			joinColumns = {@JoinColumn(name="PRESTADOR_SERVICIO_ID", referencedColumnName="ID")},
			inverseJoinColumns = {@JoinColumn(name="INFORMACION_BANCARIA_ID", referencedColumnName="ID")}
	)
	private List<InformacionBancaria> informacionBancaria;
	
	@ManyToMany( fetch = FetchType.LAZY)
	@JoinTable(name = "TOPRESTADORSERVTIPOPRESTADOR", schema="MIDAS", 
			joinColumns = {@JoinColumn(name="PRESTADOR_SERVICIO_ID", referencedColumnName="ID")}, 
			inverseJoinColumns = {@JoinColumn(name="TIPO_PRESTADOR_SERVICIO_ID", referencedColumnName="ID")}
	)
	private List<TipoPrestadorServicio> tiposPrestador;
	
	@OneToOne(fetch = FetchType.LAZY, cascade= CascadeType.ALL)
	@JoinColumn(name = "PERSONA_ID", referencedColumnName="ID")
	private PersonaMidas personaMidas;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "OFICINA_ID")
	private Oficina oficina;

	@Transient
	private String idColoniaCheck;
	
	public PrestadorServicio(){
		super();
	}
	
	public PrestadorServicio(Integer id, String ciaSegurosPolizaRC,
			Integer estatus, Date fechaModEstatus, String numPolizaRC,
			Date vigenciaPolizaRC, PersonaMidas persona, Oficina oficina, 
			Date fechaCreacion, Date fechaModificacion, String codigoUsuarioCreacion, 
			String codigoUsuarioModificacion, Integer daniosCa, Integer vidaCa, Integer autosCa, Integer formaPagoCa) {
		this(id, ciaSegurosPolizaRC, estatus, fechaModEstatus, numPolizaRC, vigenciaPolizaRC, 
				estatus != null && estatus.intValue() == 1 ? "ACTIVO" : "INACTIVO", 
						new ArrayList<InformacionBancaria>(), new ArrayList<TipoPrestadorServicio>(), 
						persona, oficina, fechaCreacion, fechaModificacion, codigoUsuarioCreacion, codigoUsuarioModificacion,
						daniosCa, vidaCa,  autosCa,  formaPagoCa);		
	}




	public PrestadorServicio(Integer id, String ciaSegurosPolizaRC,
			Integer estatus, Date fechaModEstatus, String numPolizaRC,
			Date vigenciaPolizaRC, String estatusNombre,
			List<InformacionBancaria> informacionBancaria,
			List<TipoPrestadorServicio> tiposPrestador, PersonaMidas persona,
			Oficina oficina, Date fechaCreacion, Date fechaModificacion,
			String codigoUsuarioCreacion, String codigoUsuarioModificacion,Integer daniosCa, Integer vidaCa, Integer autosCa, Integer formaPagoCa) {
		this();
		this.id = id;
		this.ciaSegurosPolizaRC = ciaSegurosPolizaRC;
		this.estatus = estatus;
		this.fechaModEstatus = fechaModEstatus;
		this.numPolizaRC = numPolizaRC;
		this.vigenciaPolizaRC = vigenciaPolizaRC;
		this.estatusNombre = estatusNombre;
		this.informacionBancaria = informacionBancaria;
		this.tiposPrestador = tiposPrestador;
		this.personaMidas = persona;
		this.oficina = oficina;
		this.fechaModificacion = fechaModificacion;
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
		this.fechaCreacion = fechaCreacion;
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;	
		this.daniosCa = daniosCa;
		this.vidaCa = vidaCa;
		this.autosCa = autosCa;
		this.formaPagoCa = formaPagoCa;
	}

	@Exportable(columnName="No. PRESTADOR", columnOrder=0)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Exportable(columnName="NOMBRE", columnOrder=1)
	public String getNombrePersona() {
			return personaMidas != null ? personaMidas.getNombre() : null;
	}

	@Exportable(columnName="TIPO PRESTADOR", columnOrder=3)
	public String getTipoPrestadoresStr(){
		StringBuilder tipoPrestadoresStr = new StringBuilder();
		if(this.tiposPrestador != null){
			for(TipoPrestadorServicio tipoPrestador : this.tiposPrestador){
				if(tipoPrestadoresStr.length()>0)
					tipoPrestadoresStr.append(",");
				tipoPrestadoresStr.append(tipoPrestador.getDescripcion());
			}
		}
		return tipoPrestadoresStr.toString();
	}
	
	@Exportable(columnName="OFICINA", columnOrder=2)
	public String getNombreOficina(){
		return oficina != null ? oficina.getNombreOficina() : null;
	}
	
	
	
	public Integer getEstatus() {
		return estatus;
	}
	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}
	
	
	public Date getFechaModEstatus() {
		return fechaModEstatus;
	}
	public void setFechaModEstatus(Date fechaModEstatus) {
		this.fechaModEstatus = fechaModEstatus;
	}
	
	
	public String getCiaSegurosPolizaRC() {
		return ciaSegurosPolizaRC;
	}
	
	public void setCiaSegurosPolizaRC(String ciaSegurosPolizaRC) {
		this.ciaSegurosPolizaRC = ciaSegurosPolizaRC;
	}
	
	public String getNumPolizaRC() {
		return numPolizaRC;
	}
	public void setNumPolizaRC(String numPolizaRC) {
		this.numPolizaRC = numPolizaRC;
	}

	public Date getVigenciaPolizaRC() {
		return vigenciaPolizaRC;
	}
	public void setVigenciaPolizaRC(Date vigenciaPolizaRC) {
		this.vigenciaPolizaRC = vigenciaPolizaRC;
	}
	
	public List<InformacionBancaria> getInformacionBancaria() {
		return informacionBancaria;
	}
	public void setInformacionBancaria(List<InformacionBancaria> informacionBancaria) {
		this.informacionBancaria = informacionBancaria;
	}
	
	
	public String getIdColoniaCheck() {
		return idColoniaCheck;
	}
	public void setIdColoniaCheck(String idColoniaCheck) {
		this.idColoniaCheck = idColoniaCheck;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public List<TipoPrestadorServicio> getTiposPrestador() {
		return tiposPrestador;
	}
	public void setTiposPrestador(List<TipoPrestadorServicio> tiposPrestador) {
		this.tiposPrestador = tiposPrestador;
	}
	
	public PersonaMidas getPersonaMidas() {
		return personaMidas;
	}
	public void setPersonaMidas(PersonaMidas persona) {
		this.personaMidas = persona;
	}
	
	@Exportable(columnName="ESTATUS", columnOrder=4)
	public String getEstatusNombre() {
		return estatusNombre;
	}
	public void setEstatusNombre(String estatusNombre) {
		this.estatusNombre = estatusNombre;
	}
	
	public Oficina getOficina() {
		return oficina;
	}
	public void setOficina(Oficina oficina) {
		this.oficina = oficina;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Integer getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PrestadorServicio other = (PrestadorServicio) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Integer getServiciosProfesionales() {
		return serviciosProfesionales;
	}

	public void setServiciosProfesionales(Integer serviciosProfesionales) {
		this.serviciosProfesionales = serviciosProfesionales;
	}
	
	public InformacionBancaria getCuentaActiva(){
		InformacionBancaria infoBancaria = null;
		for(InformacionBancaria e: this.getInformacionBancaria()){
			if(e.getEstatus() == 1){//Activo
				infoBancaria = e;
			}
		}
		return infoBancaria;
	}

	public Integer getDaniosCa() {
		return daniosCa;
	}

	public Integer getVidaCa() {
		return vidaCa;
	}

	public Integer getAutosCa() {
		return autosCa;
	}

	public Integer getFormaPagoCa() {
		return formaPagoCa;
	}

	public void setDaniosCa(Integer daniosCa) {
		this.daniosCa = daniosCa;
	}

	public void setVidaCa(Integer vidaCa) {
		this.vidaCa = vidaCa;
	}

	public void setAutosCa(Integer autosCa) {
		this.autosCa = autosCa;
	}

	public void setFormaPagoCa(Integer formaPagoCa) {
		this.formaPagoCa = formaPagoCa;
	}

}
