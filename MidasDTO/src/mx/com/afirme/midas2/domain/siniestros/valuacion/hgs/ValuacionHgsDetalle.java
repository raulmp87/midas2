package mx.com.afirme.midas2.domain.siniestros.valuacion.hgs;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table ( name="TOVALUACIONHGSDETALLE" , schema = "MIDAS" )  
public class ValuacionHgsDetalle  implements Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTOVALUACIONHGSDETALLE_SEQ")
	@SequenceGenerator(name="IDTOVALUACIONHGSDETALLE_SEQ", schema="MIDAS", sequenceName="IDTOVALUACIONHGSDETALLE_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id; 
	
	@Column(name = "ASEGURADO_TERCERO_ID")
	private Long aseguradoTerceroId;
	
	@Column(name = "MONTO_MANO_OBRA")
	private BigDecimal montoManoObra;
	
	@Column(name = "INCISO_ID")
	private Long incisoId;
	
	@Column(name = "COBERTURA_ID")
	private Long coberturaId;
	
	@Column(name = "TIPO_PROCESO")
	private short tipoProceso;
	
	@Column(name = "NOMBRE_VALUADOR")
	private String nombreValuador;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_REFACCIONES_ENTREGADAS")
	private Date fechaRefaccionesEntregadas;
	
	@Column(name = "CLAVE_VALUADOR")
	private String claveValuador;
	
	@Column(name = "NUM_FOLIO")
	private String numFolio;
	
	@Column(name = "MONTO_REFACCIONES")
	private BigDecimal montoRefacciones;
	
	@Column(name = "CATEGORIA_ID")
	private Long categoriaId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_REINGRESO")
	private Date fechaReingreso;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_TERMINO")
	private Date fechaTermino;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_INGRESO")
	private Date fechaIngreso;
	
	@Column(name = "MONTO_VALUACION")
	private BigDecimal montoValuacion;
	
	@Column(name = "LINEA_NEGOCIO_ID")
	private Long lineaNegocioId;
	
	@Column(name = "REPORTE_SIN_ID")
	private Long reporteSinId;
	
	@Column(name = "ESTATUS_EXPEDIENTE")
	private Short estatusExpediente;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CREACION")
	private Date fechaCreacion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_VALUACION")
	private Date fechaValuacion;
	
	@OneToMany(fetch=FetchType.LAZY,  mappedBy = "valuacionHgsDetalle", cascade = CascadeType.ALL)
	List<ValuacionValeRefaccion> valuacionRefaccion = new ArrayList<ValuacionValeRefaccion>();
	
	@Column(name = "MONTO_SUBTOTAL_VALUACION")
	private BigDecimal montoSubtotal;
	
	@Column(name = "MONTO_IVA_VALUACION")
	private BigDecimal montoIva;

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	public Long getId() {
		return id;
	}

	public Long getAseguradoTerceroId() {
		return aseguradoTerceroId;
	}

	public BigDecimal getMontoManoObra() {
		return montoManoObra;
	}

	public Long getIncisoId() {
		return incisoId;
	}

	public Long getCoberturaId() {
		return coberturaId;
	}

	public short getTipoProceso() {
		return tipoProceso;
	}

	public String getNombreValuador() {
		return nombreValuador;
	}

	public Date getFechaRefaccionesEntregadas() {
		return fechaRefaccionesEntregadas;
	}

	public String getClaveValuador() {
		return claveValuador;
	}

	public String getNumFolio() {
		return numFolio;
	}

	public BigDecimal getMontoRefacciones() {
		return montoRefacciones;
	}

	public Long getCategoriaId() {
		return categoriaId;
	}

	public Date getFechaReingreso() {
		return fechaReingreso;
	}

	public Date getFechaTermino() {
		return fechaTermino;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public BigDecimal getMontoValuacion() {
		return montoValuacion;
	}

	public Long getLineaNegocioId() {
		return lineaNegocioId;
	}

	public Long getReporteSinId() {
		return reporteSinId;
	}

	public Short getEstatusExpediente() {
		return estatusExpediente;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setAseguradoTerceroId(Long aseguradoTerceroId) {
		this.aseguradoTerceroId = aseguradoTerceroId;
	}

	public void setMontoManoObra(BigDecimal montoManoObra) {
		this.montoManoObra = montoManoObra;
	}

	public void setIncisoId(Long incisoId) {
		this.incisoId = incisoId;
	}

	public void setCoberturaId(Long coberturaId) {
		this.coberturaId = coberturaId;
	}

	public void setTipoProceso(short tipoProceso) {
		this.tipoProceso = tipoProceso;
	}

	public void setNombreValuador(String nombreValuador) {
		this.nombreValuador = nombreValuador;
	}

	public void setFechaRefaccionesEntregadas(Date fechaRefaccionesEntregadas) {
		this.fechaRefaccionesEntregadas = fechaRefaccionesEntregadas;
	}

	public void setClaveValuador(String claveValuador) {
		this.claveValuador = claveValuador;
	}

	public void setNumFolio(String numFolio) {
		this.numFolio = numFolio;
	}

	public void setMontoRefacciones(BigDecimal montoRefacciones) {
		this.montoRefacciones = montoRefacciones;
	}

	public void setCategoriaId(Long categoriaId) {
		this.categoriaId = categoriaId;
	}

	public void setFechaReingreso(Date fechaReingreso) {
		this.fechaReingreso = fechaReingreso;
	}

	public void setFechaTermino(Date fechaTermino) {
		this.fechaTermino = fechaTermino;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public void setMontoValuacion(BigDecimal montoValuacion) {
		this.montoValuacion = montoValuacion;
	}

	public void setLineaNegocioId(Long lineaNegocioId) {
		this.lineaNegocioId = lineaNegocioId;
	}

	public void setReporteSinId(Long reporteSinId) {
		this.reporteSinId = reporteSinId;
	}

	public void setEstatusExpediente(Short estatusExpediente) {
		this.estatusExpediente = estatusExpediente;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaValuacion() {
		return fechaValuacion;
	}

	public List<ValuacionValeRefaccion> getValuacionRefaccion() {
		return valuacionRefaccion;
	}

	public void setFechaValuacion(Date fechaValuacion) {
		this.fechaValuacion = fechaValuacion;
	}

	public void setValuacionRefaccion(
			List<ValuacionValeRefaccion> valuacionRefaccion) {
		this.valuacionRefaccion = valuacionRefaccion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValuacionHgsDetalle other = (ValuacionHgsDetalle) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ValuacionHgsDetalle [id=" + id + ", aseguradoTerceroId="
				+ aseguradoTerceroId + ", montoManoObra=" + montoManoObra
				+ ", incisoId=" + incisoId + ", coberturaId=" + coberturaId
				+ ", tipoProceso=" + tipoProceso + ", nombreValuador="
				+ nombreValuador + ", fechaRefaccionesEntregadas="
				+ fechaRefaccionesEntregadas + ", claveValuador="
				+ claveValuador + ", numFolio=" + numFolio
				+ ", montoRefacciones=" + montoRefacciones + ", categoriaId="
				+ categoriaId + ", fechaReingreso=" + fechaReingreso
				+ ", fechaTermino=" + fechaTermino + ", fechaIngreso="
				+ fechaIngreso + ", montoValuacion=" + montoValuacion
				+ ", lineaNegocioId=" + lineaNegocioId + ", reporteSinId="
				+ reporteSinId + ", estatusExpediente=" + estatusExpediente
				+ ", fechaCreacion=" + fechaCreacion + ", fechaValuacion="
				+ fechaValuacion + ", valuacionRefaccion=" + valuacionRefaccion
				+ ", montoSubtotal=" + montoSubtotal + ", montoIva=" + montoIva
				+ "]";
	}

	public BigDecimal getMontoSubtotal() {
		return montoSubtotal;
	}

	public void setMontoSubtotal(BigDecimal montoSubtotal) {
		this.montoSubtotal = montoSubtotal;
	}

	public BigDecimal getMontoIva() {
		return montoIva;
	}

	public void setMontoIva(BigDecimal montoIva) {
		this.montoIva = montoIva;
	}

	
	
}
