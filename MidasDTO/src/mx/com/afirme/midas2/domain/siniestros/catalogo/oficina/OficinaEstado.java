package mx.com.afirme.midas2.domain.siniestros.catalogo.oficina;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;



@Entity
@Table(name="TCOFICINAESTADO", schema = "MIDAS")
public class OficinaEstado implements Entidad {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TCOFICINAESTADO_SEQ")
	@SequenceGenerator(name="TCOFICINAESTADO_SEQ", schema = "MIDAS" ,  sequenceName="TCOFICINAESTADO_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;
	
	//bi-directional many-to-one association to Estado
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="STATE_ID")
	private EstadoMidas estado;
	

	//bi-directional many-to-one association to Oficina
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="OFICINA_ID")
	private Oficina oficina;

    public OficinaEstado() {
    }

	public EstadoMidas getEstado() {
		return estado;
	}

	public void setEstado(EstadoMidas estado) {
		this.estado = estado;
	}


	public Oficina getOficina() {
		return oficina;
	}

	public void setOficina(Oficina oficina) {
		this.oficina = oficina;
	}

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return oficina.getClaveOficina().concat("-").concat(estado.getValue());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	
}
