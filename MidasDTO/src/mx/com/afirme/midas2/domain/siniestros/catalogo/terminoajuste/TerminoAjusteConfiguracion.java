package mx.com.afirme.midas2.domain.siniestros.catalogo.terminoajuste;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

import org.eclipse.persistence.annotations.ReadOnly;


@Entity
@Table(name = "TCTERMAJUSTE_SINAUTOCONF", schema = "MIDAS")
@ReadOnly
public class TerminoAjusteConfiguracion implements Entidad {

	private static final long serialVersionUID = 5205651977730713456L;

	@Id
	@Column(name="ID")
	private Long id;
	
	@Column(name="TIPO_SINIESTRO_CODIGO")
	private String codigoTipoSiniestro;

	@Column(name="RESPONSABILIDAD_CODIGO")
	private String codigoResponsabilidad;
	
	@Column(name="TERMINO_AJUSTE_CODIGO")
	private String codigoTerminoAjuste;
	
	@Column(name="DESCRIPCION")
	private String descripcion;
	
	@Column(name="ACTIVO")
	private Boolean activo;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigoTipoSiniestro() {
		return codigoTipoSiniestro;
	}

	public void setCodigoTipoSiniestro(String codigoTipoSiniestro) {
		this.codigoTipoSiniestro = codigoTipoSiniestro;
	}

	public String getCodigoResponsabilidad() {
		return codigoResponsabilidad;
	}

	public void setCodigoResponsabilidad(String codigoResponsabilidad) {
		this.codigoResponsabilidad = codigoResponsabilidad;
	}

	public String getCodigoTerminoAjuste() {
		return codigoTerminoAjuste;
	}

	public void setCodigoTerminoAjuste(String codigoTerminoAjuste) {
		this.codigoTerminoAjuste = codigoTerminoAjuste;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	
	
	
}
