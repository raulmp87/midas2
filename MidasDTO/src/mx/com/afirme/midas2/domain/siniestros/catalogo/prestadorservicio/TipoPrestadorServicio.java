package mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name = "TCTIPOPRESTADORSERVICIO", schema = "MIDAS")
public class TipoPrestadorServicio implements Entidad {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCTIPOPRESTADORSERVICIO_SEQ")
	@SequenceGenerator(name = "IDTCTIPOPRESTADORSERVICIO_SEQ",  schema="MIDAS", sequenceName = "IDTCTIPOPRESTADORSERVICIO_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "NOMBRE")
	private String nombre;
	
	@Column(name = "DESCRIPCION")
	private String descripcion;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION")
	private Date fechaCreacion;
	
	@Column(name = "CODIGO_USUARIO_CREACION")
	private String codigoUsuarioCreacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_MODIFICACION")
	private Date fechaModificacion;
	
	@Column(name = "CODIGO_USUARIO_MODIFICACION")
	private String codigoUsuarioModificacion;
	
	@ManyToMany( fetch = FetchType.LAZY)
	@JoinTable(name = "TOPRESTADORSERVTIPOPRESTADOR", schema="MIDAS", 
			joinColumns = {@JoinColumn(name="TIPO_PRESTADOR_SERVICIO_ID", referencedColumnName="ID")}, 
			inverseJoinColumns = {@JoinColumn(name="PRESTADOR_SERVICIO_ID", referencedColumnName="ID")}
	)
	private List<PrestadorServicio> prestadoresServicio;
	
	
		public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
	
	
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	
	public String getCodigoUsuarioModificacion() {
		return codigoUsuarioModificacion;
	}

	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}
	
	public List<PrestadorServicio> getPrestadoresServicio() {
		return prestadoresServicio;
	}

	public void setPrestadoresServicio(List<PrestadorServicio> prestadoresServicio) {
		this.prestadoresServicio = prestadoresServicio;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoPrestadorServicio other = (TipoPrestadorServicio) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
