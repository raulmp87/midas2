package mx.com.afirme.midas2.domain.siniestros.notificaciones;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class BitacoraDetalleNotificacionId implements Serializable{
	
	private static final long serialVersionUID = -8904568494160711229L;
	
	@Column(name="SECUENCIAENVIO", nullable=false, updatable=false)
	private Integer secuenciaEnvio;

	@Column(name="DESTINO_ID", nullable=false, updatable=false)
	private Long destinoId;

	public Integer getSecuenciaEnvio() {
		return secuenciaEnvio;
	}

	public void setSecuenciaEnvio(Integer secuenciaEnvio) {
		this.secuenciaEnvio = secuenciaEnvio;
	}

	public Long getDestinoId() {
		return destinoId;
	}

	public void setDestinoId(Long destinoId) {
		this.destinoId = destinoId;
	}

}
