/**
 * 
 */
package mx.com.afirme.midas2.domain.siniestros.pagos.facturas;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.compensaciones.LiquidacionCompensaciones;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.LiquidacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.pagos.notasDeCredito.ConjuntoOrdenCompraNota;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionProveedor;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;

/**
 * @author admin
 *
 */
@Entity(name = "DocumentoFiscal")
@Table(name = "TODOCUMENTO_FISCAL", schema = "MIDAS")
public class DocumentoFiscal  extends MidasAbstracto  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2996915891403229736L;
	
	public static enum EstatusDocumentoFiscal{
		PROCESADA("PR"),
		DEVUELTA("D"),
		CANCELADA("CANC"),
		REGISTRADA("R"),
		PAGADA("P"),
		ERROR("E"),
		ASOCIADA("ASOC");
		
		private static Map<String, EstatusDocumentoFiscal> lookup = new HashMap<String, EstatusDocumentoFiscal>();
		static {			
			
			for (EstatusDocumentoFiscal e : EstatusDocumentoFiscal.values()) {
				lookup.put(e.getValue(), e);
			}
		}		
		
		public static EstatusDocumentoFiscal get(String value) {
			return lookup.get(value);
		}
		
		private String value;
		
		private EstatusDocumentoFiscal(String value){
			this.value=value;
		}
		
		public String getValue(){
			return value;
		}
	}
	
	public static enum TipoDocumentoFiscal{
		NOTA_CREDITO("NTACRED"),
		FACTURA("FACT");
		
		private static Map<String, TipoDocumentoFiscal> lookup = new HashMap<String, TipoDocumentoFiscal>();
		static {			
			
			for (TipoDocumentoFiscal e : TipoDocumentoFiscal.values()) {
				lookup.put(e.getValue(), e);
			}
		}		
		
		public static TipoDocumentoFiscal get(String value) {
			return lookup.get(value);
		}
		
		private String value;
		
		private TipoDocumentoFiscal(String value){
			this.value=value;
		}
		
		public String getValue(){
			return value;
		}
	}
	
	public enum OrigenDocumentoFiscal{LSN,ISN,MIG};
	public enum TipoAgrupadorDocumentoFiscal{CIA,SIPAC};
		
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOFACTURASINIESTRO_ID_GENERATOR")
	@SequenceGenerator(name="TOFACTURASINIESTRO_ID_GENERATOR", schema="MIDAS", sequenceName="TOFACTURASINIESTRO_ID_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;
	
	@Column(name = "NUMERO_FACTURA")
	private String numeroFactura;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_FACTURA")
	private Date fechaFactura;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_RECEPCION_MATRIZ")
	private Date fechaRecepcionMatriz;
	
	@Column(name = "MONEDA_PAGO")
	private String monedaPago;
	
	@Column(name = "ID_MONEDA")
	private Long idMoneda;
	
	@Column(name = "MONTO_TOTAL")
	private BigDecimal montoTotal;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_REGISTRO")
	private Date fechaRegistro;
	
	@Column(name = "USUARIO_DEVOLUCION")
	private String usuarioDevolucion;
	
	@Column(name = "MOTIVO_DEVOLUCION")
	private String motivoDevolucion;
	
	@Column(name = "COMENTARIO_DEVOLUCION")
	private String comentariosDevolucion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_DEVOLUCION")
	private Date fechaDevolucion;
	
	@Column(name = "ENTREGADA")
	private String entregada;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_ENTREGA")
	private Date fechaEntrega;
	
	@Column(name = "CANCELAR_ORDEN_COMPRA")
	private Integer cancelarOrdenCompra;
	
	@Column(name = "ESTATUS")
	private String estatus;
	
	@Column(name = "NOMBRE_EMISOR")
	private String nombreEmisor; 
	
	@Column(name = "RFC_EMISOR")
	private String rfcEmisor;
	
	@Column(name = "NOMBRE_RECEPTOR")
	private String nombreReceptor;
	
	@Column(name = "RFC_RECEPTOR")
	private String rfcReceptor;
	
	@Column(name = "IVA")
	private BigDecimal iva;
	
	@Column(name = "IVA_RETENIDO")
	private BigDecimal ivaRetenido;
	
	@Column(name = "ISR")
	private BigDecimal isr;
	
	@Column(name = "SUBTOTAL")
	private BigDecimal subTotal;
	
	@Column(name = "XML_FACTURA")
	private String xmlFactura;
	
	@Column(name = "TIPO")
	private String tipo;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_BAJA")
	private Date fechaBaja;
	
	@Column(name = "NOMBRE_AGRUPADOR")
	private String nombreAgrupador;
	
	@Column(name = "TIPO_AGRUPADOR")
	private String tipoAgrupador;
	
	@Column(name = "ORIGEN")
	@Enumerated(EnumType.STRING)
	private OrigenDocumentoFiscal origen;
	
	@ManyToMany( fetch = FetchType.LAZY)
	@JoinTable(name = "TOORDENCOMPRAFACTURA", schema="MIDAS", 
			joinColumns = {@JoinColumn(name="ID_FACTURA", referencedColumnName="ID")}, 
			inverseJoinColumns = {@JoinColumn(name="ID_ORDEN_COMPRA", referencedColumnName="ID")}
	)
	private List<OrdenCompra> ordenesCompra;
	
	
	@ManyToOne(cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinTable(name = "TRCONJNOTACREDITO", schema="MIDAS", 
			joinColumns = {@JoinColumn(name="DOC_FISCAL_ID", referencedColumnName="ID")},
			inverseJoinColumns = {@JoinColumn(name="CONJ_ORDENNOTA_ID", referencedColumnName="ID")}
	)
	private ConjuntoOrdenCompraNota conjuntoDeNota;
	
	
	@OneToMany(cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinTable(name = "TRCONJORDENNOTA", schema="MIDAS", 
			joinColumns = {@JoinColumn(name="DOC_FISCAL_ID", referencedColumnName="ID")},
			inverseJoinColumns = {@JoinColumn(name="CONJ_ORDENNOTA_ID", referencedColumnName="ID")}
	)
	private List<ConjuntoOrdenCompraNota> conjuntoOrdenCompraNota;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinTable(name = "TRLIQUIDACIONFACTURA", schema = "MIDAS",
			joinColumns = {@JoinColumn(name="FACTURA_ID", referencedColumnName="ID")},
			inverseJoinColumns = {@JoinColumn(name="LIQUIDACION_ID", referencedColumnName="ID")}
	)
	private LiquidacionSiniestro liquidacionSiniestro;	
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinTable(name = "TRSNRECUPERNOTAS", schema = "MIDAS",
			joinColumns = {@JoinColumn(name="ID_NOTA_CREDITO", referencedColumnName="ID")},
			inverseJoinColumns = {@JoinColumn(name="ID_RECUPERACION", referencedColumnName="ID")}
	)
	private RecuperacionProveedor recuperacion;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_PROVEEDOR")
	private PrestadorServicio proveedor;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="ID_LIQUIDACION")
	private LiquidacionCompensaciones liquidacionCompensaciones;
	
	@Transient
	private String motivoDevolucionStr;
	
	@Transient
	private String estatusStr;
	
	@Transient
	private String tieneNotasCredito;
	
	@Transient
	private String numeroSiniestro;
	
	@Transient
	private String oficina;
	
	@Transient
	private String compania;
	
	@Transient
	private String siniestroTercero;
	
	@Transient
	private Date fechaCreacionOrdenCompra;
	
	@Transient
	private String facturaConOrdenesCompra;
	

	public DocumentoFiscal() {
		super();
	}
	
	public DocumentoFiscal(String numeroFactura, String estatus, Date fechaCreacion, BigDecimal iva,BigDecimal ivaRetenido,
			BigDecimal isr, BigDecimal subTotal,BigDecimal montoTotal) {
		super();
		this.numeroFactura = numeroFactura;
		this.estatus = estatus;
		this.fechaCreacion = fechaCreacion;
		this.iva = iva;
		this.ivaRetenido = ivaRetenido;
		this.isr = isr;
		this.subTotal = subTotal;
		this.montoTotal = montoTotal;
	}
	
	public DocumentoFiscal(Long idFactura, String numeroFactura, String estatus, Date fechaCreacion, BigDecimal iva,BigDecimal ivaRetenido,
			BigDecimal isr, BigDecimal subTotal,BigDecimal montoTotal, String tieneNotasCredito) {
		
		this(numeroFactura,estatus,fechaCreacion,iva,ivaRetenido,isr,subTotal,montoTotal);
		this.id = idFactura;
		this.tieneNotasCredito = tieneNotasCredito;
	}
	
	
	public DocumentoFiscal(Long idFactura, String numeroFactura, String estatus, Date fechaCreacion, BigDecimal iva,BigDecimal ivaRetenido,
			BigDecimal isr, BigDecimal subTotal,BigDecimal montoTotal, String tieneNotasCredito,String xNombreAgrupador) {
		
		this(numeroFactura,estatus,fechaCreacion,iva,ivaRetenido,isr,subTotal,montoTotal);
		this.id = idFactura;
		this.tieneNotasCredito = tieneNotasCredito;
		this.nombreAgrupador = xNombreAgrupador;
	}
	
	public DocumentoFiscal(Long idFactura, String numeroFactura, String estatus, Date fechaCreacion, BigDecimal iva,BigDecimal ivaRetenido,
			BigDecimal isr, BigDecimal subTotal,BigDecimal montoTotal, String tieneNotasCredito,String xNombreAgrupador, String facturaConOrdenCompra) {
		
		this(numeroFactura,estatus,fechaCreacion,iva,ivaRetenido,isr,subTotal,montoTotal);
		this.id = idFactura;
		this.tieneNotasCredito = tieneNotasCredito;
		if(xNombreAgrupador.equals("SIN_AGRUPADOR")){
			this.facturaConOrdenesCompra = facturaConOrdenCompra.concat(" - ").concat(numeroFactura);
		}else{
			this.facturaConOrdenesCompra = xNombreAgrupador;
		}
		
		
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long  getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	

	public List<OrdenCompra> getOrdenesCompra() {
		return ordenesCompra;
	}

	public void setOrdenesCompra(List<OrdenCompra> ordenesCompra) {
		this.ordenesCompra = ordenesCompra;
	}

	/**
	 * @return the numeroFactura
	 */
	public String getNumeroFactura() {
		return numeroFactura;
	}

	/**
	 * @param numeroFactura the numeroFactura to set
	 */
	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	/**
	 * @return the fechaFactura
	 */
	public Date getFechaFactura() {
		return fechaFactura;
	}

	/**
	 * @param fechaFactura the fechaFactura to set
	 */
	public void setFechaFactura(Date fechaFactura) {
		this.fechaFactura = fechaFactura;
	}

	/**
	 * @return the fechaRecepcionMatriz
	 */
	public Date getFechaRecepcionMatriz() {
		return fechaRecepcionMatriz;
	}

	/**
	 * @param fechaRecepcionMatriz the fechaRecepcionMatriz to set
	 */
	public void setFechaRecepcionMatriz(Date fechaRecepcionMatriz) {
		this.fechaRecepcionMatriz = fechaRecepcionMatriz;
	}

	/**
	 * @return the monedaPago
	 */
	public String getMonedaPago() {
		return monedaPago;
	}

	/**
	 * @param monedaPago the monedaPago to set
	 */
	public void setMonedaPago(String monedaPago) {
		this.monedaPago = monedaPago;
	}

	/**
	 * @return the montoTotal
	 */
	public BigDecimal getMontoTotal() {
		return montoTotal;
	}

	/**
	 * @param montoTotal the montoTotal to set
	 */
	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}

	/**
	 * @return the fechaRegistro
	 */
	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	/**
	 * @param fechaRegistro the fechaRegistro to set
	 */
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	/**
	 * @return the usuarioDevolucion
	 */
	public String getUsuarioDevolucion() {
		return usuarioDevolucion;
	}

	/**
	 * @param usuarioDevolucion the usuarioDevolucion to set
	 */
	public void setUsuarioDevolucion(String usuarioDevolucion) {
		this.usuarioDevolucion = usuarioDevolucion;
	}

	/**
	 * @return the motivoDevolucion
	 */
	public String getMotivoDevolucion() {
		return motivoDevolucion;
	}

	/**
	 * @param motivoDevolucion the motivoDevolucion to set
	 */
	public void setMotivoDevolucion(String motivoDevolucion) {
		this.motivoDevolucion = motivoDevolucion;
	}

	/**
	 * @return the comentariosDevolucion
	 */
	public String getComentariosDevolucion() {
		return comentariosDevolucion;
	}

	/**
	 * @param comentariosDevolucion the comentariosDevolucion to set
	 */
	public void setComentariosDevolucion(String comentariosDevolucion) {
		this.comentariosDevolucion = comentariosDevolucion;
	}

	/**
	 * @return the fechaDevolucion
	 */
	public Date getFechaDevolucion() {
		return fechaDevolucion;
	}

	/**
	 * @param fechaDevolucion the fechaDevolucion to set
	 */
	public void setFechaDevolucion(Date fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}

	/**
	 * @return the entregada
	 */
	public String getEntregada() {
		return entregada;
	}

	/**
	 * @param entregada the entregada to set
	 */
	public void setEntregada(String entregada) {
		this.entregada = entregada;
	}

	/**
	 * @return the fechaEntrega
	 */
	public Date getFechaEntrega() {
		return fechaEntrega;
	}

	/**
	 * @param fechaEntrega the fechaEntrega to set
	 */
	public void setFechaEntrega(Date fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}

	/**
	 * @return the cancelarOrdenCompra
	 */
	public Integer getCancelarOrdenCompra() {
		return cancelarOrdenCompra;
	}

	/**
	 * @param cancelarOrdenCompra the cancelarOrdenCompra to set
	 */
	public void setCancelarOrdenCompra(Integer cancelarOrdenCompra) {
		this.cancelarOrdenCompra = cancelarOrdenCompra;
	}
	
	/**
	 * @param cancelarOrdenCompra Str
	 */
	public void setCancelarOrdenCompraStr(String cancelarOrdenCompra) {
		if(cancelarOrdenCompra != null && cancelarOrdenCompra.length()>0){
			if(cancelarOrdenCompra.equals("true") || cancelarOrdenCompra.equals("True")){
				this.setCancelarOrdenCompra(1);
			}else{
				this.setCancelarOrdenCompra(0);
			}
		}else{
			this.setCancelarOrdenCompra(0);
		}
	}
	
	/**
	 * @param cancelarOrdenCompra Str
	 */
	public boolean getCancelarOrdenCompraStr() {
		
		if( this.getCancelarOrdenCompra().equals(1)){
			return Boolean.TRUE;
		}else{
			return Boolean.FALSE;
		}
	}

	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return the motivoDevolucionStr
	 */
	public String getMotivoDevolucionStr() {
		return motivoDevolucionStr;
	}

	/**
	 * @param motivoDevolucionStr the motivoDevolucionStr to set
	 */
	public void setMotivoDevolucionStr(String motivoDevolucionStr) {
		this.motivoDevolucionStr = motivoDevolucionStr;
	}

	public String getNombreEmisor() {
		return nombreEmisor;
	}

	public void setNombreEmisor(String nombreEmisor) {
		this.nombreEmisor = nombreEmisor;
	}

	public String getRfcEmisor() {
		return rfcEmisor;
	}

	public void setRfcEmisor(String rfcEmisor) {
		this.rfcEmisor = rfcEmisor;
	}

	public String getNombreReceptor() {
		return nombreReceptor;
	}

	public void setNombreReceptor(String nombreReceptor) {
		this.nombreReceptor = nombreReceptor;
	}

	public String getRfcReceptor() {
		return rfcReceptor;
	}

	public void setRfcReceptor(String rfcReceptor) {
		this.rfcReceptor = rfcReceptor;
	}

	public BigDecimal getIva() {
		return iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public BigDecimal getIvaRetenido() {
		return ivaRetenido;
	}

	public void setIvaRetenido(BigDecimal ivaRetenido) {
		this.ivaRetenido = ivaRetenido;
	}

	public BigDecimal getIsr() {
		return isr;
	}

	public void setIsr(BigDecimal isr) {
		this.isr = isr;
	}

	public BigDecimal getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}

	public String getEstatusStr() {
		return estatusStr;
	}

	public void setEstatusStr(String estatusStr) {
		this.estatusStr = estatusStr;
	}

	public Long getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}

	public String getXmlFactura() {
		return xmlFactura;
	}

	public void setXmlFactura(String xmlFactura) {
		this.xmlFactura = xmlFactura;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public List<ConjuntoOrdenCompraNota> getConjuntoOrdenCompraNota() {
		return conjuntoOrdenCompraNota;
	}

	public void setConjuntoOrdenCompraNota(
			List<ConjuntoOrdenCompraNota> conjuntoOrdenCompraNota) {
		this.conjuntoOrdenCompraNota = conjuntoOrdenCompraNota;
	}
	
	public LiquidacionCompensaciones getLiquidacionCompensaciones() {
		return liquidacionCompensaciones;
	}

	public void setLiquidacionCompensaciones(
			LiquidacionCompensaciones liquidacionCompensaciones) {
		this.liquidacionCompensaciones = liquidacionCompensaciones;
	}

	public LiquidacionSiniestro getLiquidacionSiniestro() {
		return liquidacionSiniestro;
	}

	public void setLiquidacionSiniestro(LiquidacionSiniestro liquidacionSiniestro) {
		this.liquidacionSiniestro = liquidacionSiniestro;
	}

	public ConjuntoOrdenCompraNota getConjuntoDeNota() {
		return conjuntoDeNota;
	}

	public void setConjuntoDeNota(ConjuntoOrdenCompraNota conjuntoDeNota) {
		this.conjuntoDeNota = conjuntoDeNota;
	}

	public Date getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public String getTieneNotasCredito() {
		return tieneNotasCredito;
	}

	public void setTieneNotasCredito(String tieneNotasCredito) {
		this.tieneNotasCredito = tieneNotasCredito;
	}

	public RecuperacionProveedor getRecuperacion() {
		return recuperacion;
	}

	public void setRecuperacion(RecuperacionProveedor recuperacion) {
		this.recuperacion = recuperacion;
	}
	
	public Boolean getTieneRecuperacion() {
		if(this.recuperacion==null){
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	public OrigenDocumentoFiscal getOrigen() {
		return origen;
	}

	public void setOrigen(OrigenDocumentoFiscal origen) {
		this.origen = origen;
	}

	public String getNombreAgrupador() {
		return nombreAgrupador;
	}

	public void setNombreAgrupador(String nombreAgrupador) {
		this.nombreAgrupador = nombreAgrupador;
	}
	
	public String getTipoAgrupador() {
		return tipoAgrupador;
	}

	public void setTipoAgrupador(String tipoAgrupador) {
		this.tipoAgrupador = tipoAgrupador;
	}

	public PrestadorServicio getProveedor() {
		return proveedor;
	}

	public void setProveedor(PrestadorServicio proveedor) {
		this.proveedor = proveedor;
	}

	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}

	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}

	public String getOficina() {
		return oficina;
	}

	public void setOficina(String oficina) {
		this.oficina = oficina;
	}

	public String getCompania() {
		return compania;
	}

	public void setCompania(String compania) {
		this.compania = compania;
	}

	public String getSiniestroTercero() {
		return siniestroTercero;
	}

	public void setSiniestroTercero(String siniestroTercero) {
		this.siniestroTercero = siniestroTercero;
	}

	public Date getFechaCreacionOrdenCompra() {
		return fechaCreacionOrdenCompra;
	}

	public void setFechaCreacionOrdenCompra(Date fechaCreacionOrdenCompra) {
		this.fechaCreacionOrdenCompra = fechaCreacionOrdenCompra;
	}
	

	public String getFacturaConOrdenesCompra() {
		return facturaConOrdenesCompra;
	}

	public void setFacturaConOrdenesCompra(String facturaConOrdenesCompra) {
		this.facturaConOrdenesCompra = facturaConOrdenesCompra;
	}
	
}
