package mx.com.afirme.midas2.domain.siniestros.valuacion;

import java.text.DecimalFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar.KeyNotEmpty;
import mx.com.afirme.midas2.domain.siniestros.catalogo.pieza.PiezaValuacion;

import org.apache.bval.constraints.NotEmpty;

@Entity
@Table(name="TOVALUACIONREPORTEPIEZA", schema="MIDAS")
public class ValuacionReportePieza implements Entidad{

	private static final long serialVersionUID = -2317346616591721060L;
	
	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOVALREPPIEZA_GENERATOR")
	@SequenceGenerator(name="TOVALREPPIEZA_GENERATOR", sequenceName="IDTOVALUACIONREPORTEPIEZA_SEQ", schema="MIDAS", allocationSize=1)
	private Long id;
	
	@Column(name="COSTO")
	private Double costo;
	
	@Column(name="MANO_OBRA")
	private Double manoObra;
	
	@Column(name="ORIGEN")
	private String origen;
	
	@Column(name="PINTURA")
	private Double pintura;
	
	@NotNull(message="Este campo es requerido")
	@NotEmpty(message="Este campo es requerido")
	@Min(value=1,message="Este campo es requerido")
	@Column(name="SECCION_ID", nullable=false)
	private Integer seccionAutoId;
	
	@NotNull(message="Este campo es requerido")
	@NotEmpty(message="Este campo es requerido")
	@Column(name="TIPO_AFECTACION", nullable=false)
	private String tipoAfectacion;
	
	@NotNull(message="Este campo es requerido")
	@KeyNotEmpty
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PIEZA_ID", nullable=false, referencedColumnName="ID")
	private PiezaValuacion pieza;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "VALUACIONREPORTE_ID", nullable=false, referencedColumnName="ID")
	private ValuacionReporte valuacionReporte;

	@Transient
	private String seccionAutoDesc;
	
	@Transient
	private String tipoAfectacionDesc;
	
	@Transient
	private Integer piezaSeleccionada;
	
	@Transient
	private final static DecimalFormat formato = new DecimalFormat("#.00");
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the costo
	 */
	public Double getCosto() {
		if(costo != null)
			costo = Double.parseDouble(formato.format(costo));
		return costo;
	}

	/**
	 * @param costo the costo to set
	 */
	public void setCosto(Double costo) {
		this.costo = costo;
	}

	/**
	 * @return the manoObra
	 */
	public Double getManoObra() {
		if(manoObra != null)
			manoObra = Double.parseDouble(formato.format(manoObra));
		return manoObra;
	}

	/**
	 * @param manoObra the manoObra to set
	 */
	public void setManoObra(Double manoObra) {
		this.manoObra = manoObra;
	}

	/**
	 * @return the origen
	 */
	public String getOrigen() {
		return origen;
	}

	/**
	 * @param origen the origen to set
	 */
	public void setOrigen(String origen) {
		this.origen = origen;
	}

	/**
	 * @return the pintura
	 */
	public Double getPintura() {
		if(pintura != null)
			pintura = Double.parseDouble(formato.format(pintura));
		return pintura;
	}

	/**
	 * @param pintura the pintura to set
	 */
	public void setPintura(Double pintura) {
		this.pintura = pintura;
	}

	/**
	 * @return the seccionAutoId
	 */
	public Integer getSeccionAutoId() {
		return seccionAutoId;
	}

	/**
	 * @param seccionAutoId the seccionAutoId to set
	 */
	public void setSeccionAutoId(Integer seccionAutoId) {
		this.seccionAutoId = seccionAutoId;
	}

	/**
	 * @return the tipoAfectacion
	 */
	public String getTipoAfectacion() {
		return tipoAfectacion;
	}

	/**
	 * @param tipoAfectacion the tipoAfectacion to set
	 */
	public void setTipoAfectacion(String tipoAfectacion) {
		this.tipoAfectacion = tipoAfectacion;
	}

	/**
	 * @return the pieza
	 */
	public PiezaValuacion getPieza() {
		return pieza;
	}

	/**
	 * @param pieza the pieza to set
	 */
	public void setPieza(PiezaValuacion pieza) {
		this.pieza = pieza;
	}

	/**
	 * @return the valuacionReporte
	 */
	public ValuacionReporte getValuacionReporte() {
		return valuacionReporte;
	}

	/**
	 * @param valuacionReporte the valuacionReporte to set
	 */
	public void setValuacionReporte(ValuacionReporte valuacionReporte) {
		this.valuacionReporte = valuacionReporte;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	/**
	 * @return the seccionAutoDesc
	 */
	public String getSeccionAutoDesc() {
		return seccionAutoDesc;
	}

	/**
	 * @param seccionAutoDesc the seccionAutoDesc to set
	 */
	public void setSeccionAutoDesc(String seccionAutoDesc) {
		this.seccionAutoDesc = seccionAutoDesc;
	}

	/**
	 * @return the tipoAfectacionDesc
	 */
	public String getTipoAfectacionDesc() {
		return tipoAfectacionDesc;
	}

	/**
	 * @param tipoAfectacionDesc the tipoAfectacionDesc to set
	 */
	public void setTipoAfectacionDesc(String tipoAfectacionDesc) {
		this.tipoAfectacionDesc = tipoAfectacionDesc;
	}

	/**
	 * @return the piezaSeleccionada
	 */
	public Integer getPiezaSeleccionada() {
		return piezaSeleccionada;
	}

	/**
	 * @param piezaSeleccionada the piezaSeleccionada to set
	 */
	public void setPiezaSeleccionada(Integer piezaSeleccionada) {
		this.piezaSeleccionada = piezaSeleccionada;
	}
	
}
