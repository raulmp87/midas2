package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.util.StringUtil;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;


@Entity(name = "TerceroRCVehiculo")
@Table(name = "TOTERCERORCVEHICULO", schema = "MIDAS")
@DiscriminatorValue("RCV")
@PrimaryKeyJoinColumn(name = "ESTIMACIONCOBERTURAREPCAB_ID", referencedColumnName = "ID")
public class TerceroRCVehiculo extends EstimacionCoberturaReporteCabina {

	private static final long serialVersionUID = 1L;
	
	@Column(name="COLOR")
	private String color;
	
	@Column(name="DANOS_CUBIERTOS")
	private String danosCubiertos;
	
	@Column(name="TIENE_DANOS_PREEXISTENTES")
	private Boolean tieneDanosPreexistentes;
	
	@Column(name="DANOS_PREEXISTENTES")
	private String danosPreexistentes;
	
	@Column(name="MARCA")
	private String marca;
	
	@Column(name="MODELOVEHICULO")
	private Short  modeloVehiculo;
	
	@Column(name="NUMEROMOTOR")
	private String numeroMotor;
	
	@Column(name="NUMEROSERIE")
	private String numeroSerie;
	
	@Column(name="OBSERVACIONES")
	private String observaciones;
	
	@Column(name="PLACA")
	private String placas;
	
	@Column(name="ESTILOVEHICULO")
	private String estiloVehiculo;
	
	@Column(name="TIPO_TRANSPORTE")
	private String tipoTransporte;	

	@Column(name="DANOS_MECANICOS")
	private String danosMecanicos;
	
	@Column(name="DANOS_INTERIORES")
	private String danosInteriores;
	
	@Column(name="BASTIADORES")
	private String bastiadores;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinFetch (JoinFetchType.OUTER)
	@JoinColumn(name = "TALLER_ID", referencedColumnName = "ID")
	private PrestadorServicio taller;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinFetch (JoinFetchType.OUTER)
	@JoinColumn(name = "ESTADO_ID", referencedColumnName = "STATE_ID")
	private EstadoMidas estado;
	
	@Column(name="DANOS")
	private String dano;

	@Column(name="LLEGADAS")
	private String llegadas;
	
	@Column(name="CLAVE_AJUSTADOR_TERCERO")
	private String claveAjustador;

	@Column(name="NOMBRE_AJUSTADOR_TERCERO")
	private String nombreAjustador;	
	
	@Transient
	private String descColor;
	
	@Transient
	private Boolean involucradoEnSiniestros;
	
	@Transient
	private String descripcionVehiculo;
	
	@Transient
	private String siniestroTerceroAfectacion;
	
	
	@Transient
	private String siniestroTerceroSRE;
	
	
	
	public TerceroRCVehiculo(){

	}



	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getDanosCubiertos() {
		return danosCubiertos;
	}

	public void setDanosCubiertos(String danosCubiertos) {
		this.danosCubiertos = danosCubiertos;
	}

	public Boolean getTieneDanosPreexistentes() {
		return tieneDanosPreexistentes;
	}

	public void setTieneDanosPreexistentes(Boolean tieneDanosPreexistentes) {
		this.tieneDanosPreexistentes = tieneDanosPreexistentes;
	}
	
	public String getDanosPreexistentes() {
		return danosPreexistentes;
	}

	public void setDanosPreexistentes(String danosPreexistentes) {
		this.danosPreexistentes = danosPreexistentes;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public Short getModeloVehiculo() {
		return modeloVehiculo;
	}

	public void setModeloVehiculo(Short modeloVehiculo) {
		this.modeloVehiculo = modeloVehiculo;
	}

	public String getNumeroMotor() {
		return numeroMotor;
	}

	public void setNumeroMotor(String numeroMotor) {
		this.numeroMotor = numeroMotor;
	}



	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getPlacas() {
		return placas;
	}

	public void setPlacas(String placas) {
		this.placas = placas;
	}

	public String getEstiloVehiculo() {
		return estiloVehiculo;
	}

	public void setEstiloVehiculo(String estiloVehiculo) {
		this.estiloVehiculo = estiloVehiculo;
	}

	public String getTipoTransporte() {
		return tipoTransporte;
	}
	
	public void setTipoTransporte(String tipoTransporte) {
		this.tipoTransporte = tipoTransporte;
	}

	public PrestadorServicio getTaller() {
		return taller;
	}

	public void setTaller(PrestadorServicio taller) {
		this.taller = taller;
	}

	public EstadoMidas getEstado() {
		return estado;
	}

	public void setEstado(EstadoMidas estado) {
		this.estado = estado;
	}

	public String getDano() {
		return dano;
	}

	public void setDano(String dano) {
		this.dano = dano;
	}

	public String getLlegadas() {
		return llegadas;
	}

	public void setLlegadas(String llegadas) {
		this.llegadas = llegadas;
	}

	public String getDanosMecanicos() {
		return danosMecanicos;
	}

	public void setDanosMecanicos(String danosMecanicos) {
		this.danosMecanicos = danosMecanicos;
	}

	public String getDanosInteriores() {
		return danosInteriores;
	}

	public void setDanosInteriores(String danosInteriores) {
		this.danosInteriores = danosInteriores;
	}

	public String getBastiadores() {
		return bastiadores;
	}

	public void setBastiadores(String bastiadores) {
		this.bastiadores = bastiadores;
	}

	public String getDescColor() {
		return descColor;
	}

	public void setDescColor(String descColor) {
		this.descColor = descColor;
	}

	public Boolean getInvolucradoEnSiniestros() {
		return involucradoEnSiniestros;
	}

	public void setInvolucradoEnSiniestros(Boolean involucradoEnSiniestros) {
		this.involucradoEnSiniestros = involucradoEnSiniestros;
	}
	
	public void setDescripcionVehiculo(String descripcionVehiculo) {
		this.descripcionVehiculo = descripcionVehiculo;
	}	

	public String getDescripcionVehiculo(){		
		StringBuilder str = new StringBuilder(!StringUtil.isEmpty(marca)? marca.concat(", ") : "");
		str.append(!StringUtil.isEmpty(estiloVehiculo)? estiloVehiculo.concat(", ") : "");
		str.append(modeloVehiculo != null ? modeloVehiculo : "");		
		return str.toString();
	}



	public String getSiniestroTerceroAfectacion() {
		return siniestroTerceroAfectacion;
	}



	public void setSiniestroTerceroAfectacion(String siniestroTerceroAfectacion) {
		this.siniestroTerceroAfectacion = siniestroTerceroAfectacion;
	}



	public String getSiniestroTerceroSRE() {
		return siniestroTerceroSRE;
	}



	public void setSiniestroTerceroSRE(String siniestroTerceroSRE) {
		this.siniestroTerceroSRE = siniestroTerceroSRE;
	}
	

	public String getClaveAjustador() {
		return claveAjustador;
	}



	public void setClaveAjustador(String claveAjustador) {
		this.claveAjustador = claveAjustador;
	}



	public String getNombreAjustador() {
		return nombreAjustador;
	}



	public void setNombreAjustador(String nombreAjustador) {
		this.nombreAjustador = nombreAjustador;
	}
	
}