package mx.com.afirme.midas2.domain.siniestros.valuacion.hgs;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table ( name="TOVALUACIONVALEREFACCIONFINAL" , schema = "MIDAS" ) 

public class ValuacionValeRefaccionFinal implements Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTOVALREFFINAL_SEQ")
	@SequenceGenerator(name="IDTOVALREFFINAL_SEQ", schema="MIDAS", sequenceName="IDTOVALREFFINAL_SEQ",allocationSize=1)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "MONTO")
	private Double monto;
	
	@Column(name = "PORCENTAJE_IVA")
	private Double  porcentajeIva;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_ACTUALIZACION")
	private Date   fechaActualizacion;
	
	@Column(name = "REFACCION")
	private Boolean refaccion;
	
	@Column(name = "MONTO_IVA")
	private Double montoIva;
	
	@Column(name = "FOLIO")
	private String folio;
	
	@Column(name = "ESTATUS_VALE")
	private String estatusDetalle;
	
	@Column(name = "VALE_ID")
	private Long   valeId;
	
	@Column(name = "CLAVE_PROVEEDOR")
	private Long   claveProveedor;
	
	@Column(name = "NOMBRE_PROVEEDOR")
	private String nombreProveedor;
	
	@Column(name = "CONCEPTO")
	private String concepto;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_EXPEDICION")
	private Date fechaExpedicion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CREACION")
	private Date fechaCreacion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_MODIFICACION")
	private Date fechaModificacion;
	
	@Column(name= "ORDEN_COMPRA_ID")
	private Long  ordenCompraId;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "VALUACION_DETALLE_ID", referencedColumnName = "ID", nullable = false, updatable = false, insertable = true)
	ValuacionHgs valuacionHgs = new ValuacionHgs();

	public Long getId() {
		return id;
	}
	public Double getMonto() {
		return monto;
	}
	public Double getPorcentajeIva() {
		return porcentajeIva;
	}
	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}
	public Double getMontoIva() {
		return montoIva;
	}
	public String getFolio() {
		return folio;
	}
	public String getEstatusDetalle() {
		return estatusDetalle;
	}
	public Long getValeId() {
		return valeId;
	}
	public Long getClaveProveedor() {
		return claveProveedor;
	}
	public String getNombreProveedor() {
		return nombreProveedor;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setMonto(Double monto) {
		this.monto = monto;
	}
	public void setPorcentajeIva(Double porcentajeIva) {
		this.porcentajeIva = porcentajeIva;
	}
	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}
	public void setMontoIva(Double montoIva) {
		this.montoIva = montoIva;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public void setEstatusDetalle(String estatusDetalle) {
		this.estatusDetalle = estatusDetalle;
	}
	public void setValeId(Long valeId) {
		this.valeId = valeId;
	}
	public void setClaveProveedor(Long claveProveedor) {
		this.claveProveedor = claveProveedor;
	}
	public void setNombreProveedor(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValuacionValeRefaccionFinal other = (ValuacionValeRefaccionFinal) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	public Boolean getRefaccion() {
		return refaccion;
	}

	public void setRefaccion(Boolean refaccion) {
		this.refaccion = refaccion;
	}

	@Override
	public Long getKey() {
		return this.getId();
	}
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	public Date getFechaExpedicion() {
		return fechaExpedicion;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaExpedicion(Date fechaExpedicion) {
		this.fechaExpedicion = fechaExpedicion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	public ValuacionHgs getValuacionHgs() {
		return valuacionHgs;
	}
	public void setValuacionHgs(ValuacionHgs valuacionHgs) {
		this.valuacionHgs = valuacionHgs;
	}
	public Long getOrdenCompraId() {
		return ordenCompraId;
	}
	public void setOrdenCompraId(Long ordenCompraId) {
		this.ordenCompraId = ordenCompraId;
	}
	@Override
	public String toString() {
		return "ValuacionValeRefaccionFinal [id=" + id + ", monto=" + monto
				+ ", porcentajeIva=" + porcentajeIva + ", fechaActualizacion="
				+ fechaActualizacion + ", refaccion=" + refaccion
				+ ", montoIva=" + montoIva + ", folio=" + folio
				+ ", estatusDetalle=" + estatusDetalle + ", valeId=" + valeId
				+ ", claveProveedor=" + claveProveedor + ", nombreProveedor="
				+ nombreProveedor + ", concepto=" + concepto
				+ ", fechaExpedicion=" + fechaExpedicion + ", fechaCreacion="
				+ fechaCreacion + ", fechaModificacion=" + fechaModificacion
				+ ", ordenCompraId=" + ordenCompraId + "]";
	}
	

	
}
