package mx.com.afirme.midas2.domain.siniestros.catalogo.conceptos;

import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name = "TOCONCEPTOCOBERTURA", schema = "MIDAS")
public class ConceptoCobertura implements Entidad {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3475584128632846993L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOCONCEPTOCOBERTURA_SEQ")
	@SequenceGenerator(name = "IDTOCONCEPTOCOBERTURA_SEQ", sequenceName = "IDTOCONCEPTOCOBERTURA_SEQ", schema = "MIDAS", allocationSize = 1)
	@Column(name = "ID", nullable = false)
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_CONCEPTO", referencedColumnName="ID")
	private ConceptoAjuste conceptoAjuste;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumns( { 
        @JoinColumn(name="ID_SECCION", referencedColumnName="idtoseccion", nullable=false, insertable=true, updatable=false), 
        @JoinColumn(name="ID_COBERTURA", referencedColumnName="idtocobertura", nullable=false, insertable=true, updatable=false) } 
	)
	private CoberturaSeccionDTO coberturaSeccion;
	
	@Column(name = "CLAVE_SUBCOBERTURA", nullable = false)
	private String claveSubcobertura;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CREACION", nullable = false)
	private Date fechaCreacion;
	
	@Column(name = "CODIGO_USUARIO_CREACION", nullable = false)
	private String codigoUsuarioCreacion;
	
	
	public Long getId() {
		return id;
	}
	public ConceptoAjuste getConceptoAjuste() {
		return conceptoAjuste;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setConceptoAjuste(ConceptoAjuste conceptoAjuste) {
		this.conceptoAjuste = conceptoAjuste;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
	public CoberturaSeccionDTO getCoberturaSeccion() {
		return coberturaSeccion;
	}
	public void setCoberturaSeccion(CoberturaSeccionDTO coberturaSeccion) {
		this.coberturaSeccion = coberturaSeccion;
	}
	@Override
	public Long getKey() {
		return this.getId();
	}
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public String getClaveSubcobertura() {
		return claveSubcobertura;
	}
	public void setClaveSubcobertura(String claveSubcobertura) {
		this.claveSubcobertura = claveSubcobertura;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConceptoCobertura other = (ConceptoCobertura) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	

	
}
