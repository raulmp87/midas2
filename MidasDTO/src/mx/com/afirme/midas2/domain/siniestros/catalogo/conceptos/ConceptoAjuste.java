package mx.com.afirme.midas2.domain.siniestros.catalogo.conceptos;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.domain.MidasAbstracto;

import org.apache.bval.constraints.NotEmpty;

@Entity
@Table(name = "TCCONCEPTOAJUSTE", schema = "MIDAS")
public class ConceptoAjuste extends MidasAbstracto {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -674853723356336081L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCCONCEPTOAJUSTE_SEQ")
	@SequenceGenerator(name = "IDTCCONCEPTOAJUSTE_SEQ", sequenceName = "IDTCCONCEPTOAJUSTE_SEQ", schema = "MIDAS", allocationSize = 1)
	@Column(name = "ID", nullable = false)
	private Long id;
	
	@NotNull
	@NotEmpty(message="{org.hibernate.validator.constraints.NotEmpty.message}")
	@Column(name = "NOMBRE", nullable = false)
	private String nombre;

	@NotEmpty(message="{org.hibernate.validator.constraints.NotEmpty.message}")
	@Column(name = "TIPO_CONCEPTO", nullable = false)
	private String tipoConcepto;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CAMBIO_ESTATUS", nullable = true)
	private Date fechaCambioEstatus;
	
	@NotNull
	@NotEmpty(message="{org.hibernate.validator.constraints.NotEmpty.message}")
	@Column(name = "ESTATUS", nullable = false)
	private short estatus;
	
	@NotNull
	@Column(name = "CATEGORIA", nullable = false)
	private Short categoria;	
	
	
	@Column(name="PORC_IVA") 
	private BigDecimal porcIva;
	
	
	@Column(name="PORC_ISR")
	private BigDecimal porcIsr;
	
	
	@Column(name="PORC_IVA_RETENIDO") 
	private BigDecimal porcIvaRetenido;
	
	
	
    @Column(name="APLICA_IVA")
	private Boolean aplicaIva = Boolean.TRUE;
    
    @Column(name="APLICA_IVA_RETENIDO")
	private Boolean aplicaIvaRetenido = Boolean.TRUE;
    
    @Column(name="APLICA_ISR")
	private Boolean aplicaIsr = Boolean.TRUE;
	
	@Column(name="ID_CUENTA_CONT")
	private Long idCuentaContable;
	
	@Column(name="ES_VALIDO_HGS")
	private Boolean esValidoHGS;
	
	

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		return this.toString();
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}


	public Long getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public String getTipoConcepto() {
		return tipoConcepto;
	}

	public Date getFechaCambioEstatus() {
		return fechaCambioEstatus;
	}

	public short getEstatus() {
		return estatus;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setTipoConcepto(String tipoConcepto) {
		this.tipoConcepto = tipoConcepto;
	}

	public void setFechaCambioEstatus(Date fechaCambioEstatus) {
		this.fechaCambioEstatus = fechaCambioEstatus;
	}

	public void setEstatus(short estatus) {
		this.estatus = estatus;
	}

	
	
	
	public BigDecimal getPorcIva() {
		return porcIva;
	}

	public void setPorcIva(BigDecimal porcIva) {
		this.porcIva = porcIva;
	}

	public BigDecimal getPorcIsr() {
		return porcIsr;
	}

	public void setPorcIsr(BigDecimal porcIsr) {
		this.porcIsr = porcIsr;
	}

	public BigDecimal getPorcIvaRetenido() {
		return porcIvaRetenido;
	}

	public void setPorcIvaRetenido(BigDecimal porcIvaRetenido) {
		this.porcIvaRetenido = porcIvaRetenido;
	}

	
	
	public Boolean getAplicaIva() {
		return aplicaIva;
	}

	public void setAplicaIva(Boolean aplicaIva) {
		this.aplicaIva = aplicaIva;
	}

	public Boolean getAplicaIvaRetenido() {
		return aplicaIvaRetenido;
	}

	public void setAplicaIvaRetenido(Boolean aplicaIvaRetenido) {
		this.aplicaIvaRetenido = aplicaIvaRetenido;
	}

	public Boolean getAplicaIsr() {
		return aplicaIsr;
	}

	public void setAplicaIsr(Boolean aplicaIsr) {
		this.aplicaIsr = aplicaIsr;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConceptoAjuste other = (ConceptoAjuste) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Short getCategoria() {
		return categoria;
	}

	public void setCategoria(Short categoria) {
		this.categoria = categoria;
	}

	/**
	 * @return the idCuentaContable
	 */
	public Long getIdCuentaContable() {
		return idCuentaContable;
	}

	/**
	 * @param idCuentaContable the idCuentaContable to set
	 */
	public void setIdCuentaContable(Long idCuentaContable) {
		this.idCuentaContable = idCuentaContable;
	}
	
	public Boolean getEsValidoHGS() {
		return esValidoHGS;
	}

	public void setEsValidoHGS(Boolean esValidoHGS) {
		this.esValidoHGS = esValidoHGS;
	}

	@Override
	public String toString() {
		return "ConceptoAjuste [id=" + id + ", nombre=" + nombre
				+ ", tipoConcepto=" + tipoConcepto + ", fechaCambioEstatus="
				+ fechaCambioEstatus + ", estatus=" + estatus + ", categoria="
				+ categoria + "]";
	}
	
}
