package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.EnumBase;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.depuracion.ConfiguracionDepuracionReserva;
import mx.com.afirme.midas2.util.StringUtil;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;


@Entity(name = "EstimacionCoberturaReporteCabina")
@Table(name = "TOESTIMACIONCOBERTURAREPCAB", schema = "MIDAS")
@DiscriminatorColumn(name="TIPO_ESTIMACION")
@Inheritance(strategy=InheritanceType.JOINED)
public class EstimacionCoberturaReporteCabina extends MidasAbstracto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDESTCOBERREPCAB_SEQ")
	@SequenceGenerator(name = "IDESTCOBERREPCAB_SEQ", schema = "MIDAS", sequenceName = "IDESTCOBERREPCAB_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Long id;

	@Column(name = "EMAIL_CONTACTO")
	private String email;
	
	@Column(name = "EMAIL_NO_PROPORCIONADO")
	private Boolean emailNoProporcionado;
	
	@Column(name = "EMAIL_NO_PROPORCIONADO_MOTIVO")
	private String emailNoProporcionadoMotivo;

	@Column(name = "FOLIO")
	private String folio;

	@Column(name = "FOLIO_COMPANIASEGUROS")
	private String folioCompaniaSeguros;

	@Column(name = "LADA_TEL_CONTACTO")
	private String ladaTelContacto;

	@Column(name = "LADA_TEL_CONTACTO2")
	private String ladaTelContactoDos;

	@Column(name = "NOMBRE_AFECTADO")
	private String nombreAfectado;

	@Column(name = "NOMBRE_PERSONA_CONTACTO")
	private String nombrePersonaContacto;
	
	@Column(name="TIPO_PERSONA")
	private String tipoPersona;

	@Column(name = "NUMERO_SINIESTRO_TERCERO")
	private String numeroSiniestroTercero;

	@Column(name = "SECUENCIA_PASE_ATENCION")
	private Integer secuenciaPaseAtencion;

	@Column(name = "SUMA_ASEGURADA_OBTENIDA")
	private BigDecimal sumaAseguradaObtenida;

	@Column(name = "TELEFONO_CONTACTO")
	private String telContacto;

	@Column(name = "TELEFONO_CONTACTO2")
	private String telContactoDos;

	@Column(name = "TIENE_COMPANIASEGUROS")
	private Boolean tieneCompaniaSeguros;

	@Column(name = "TIPO_ESTIMACION")
	private String tipoEstimacion;

	@Column(name = "TIPO_PASE_ATENCION")
	private String tipoPaseAtencion;
	
	@Column(name = "ESTATUS")
	private String estatus;
	
	@Column(name="APLICA_DEDUCIBLE")
	private Boolean aplicaDeducible;
	
	@Column(name = "SECUENCIA_PASE")
	private Integer secuenciaPaseDeCobertura;

	@Column(name = "SECUENCIA_IMPRESION")
	private Long secuenciaImpresion;
	
	@Column(name = "CLAVE_SUB_TIPO_CALCULO")
	private String cveSubCalculo;
	
	@Column(name = "ES_EDITABLE")
	private String esEditable;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_IMPRESION")
	private Date fechaImpresion;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COBERTURAREPORTECABINA_ID", referencedColumnName = "ID")
	private CoberturaReporteCabina coberturaReporteCabina;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANIASEGUROS_TERCERO_ID", referencedColumnName = "ID")
	@JoinFetch(JoinFetchType.OUTER)
	private PrestadorServicio companiaSegurosTercero;
	
	@OneToMany(mappedBy="estimacionCobertura", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	List<MovimientoCoberturaSiniestro> listadoMovimientos;

	@ManyToOne
	@JoinColumn(name = "CONFIG_DEPURACION_ID", referencedColumnName = "id")
	private ConfiguracionDepuracionReserva configuracionDepuracion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_DEPURACION_TOTAL")
	private Date fechaDepuracionTotal;
	
	@Column(name = "DEPURACION_TOTAL")
	private Boolean depuracionTotal;
	
	@Column(name = "MOTIVO_DEDUCIBLE")
	private String motivoNoAplicaDeducible;
	
	@Column(name = "DUA")
	private String dua;
	
	@Column(name = "CIRCUNSTANCIA")
	private String circunstancia;
	
	@Column(name="ES_OTRA_CIRCUNSTANCIA")
	private Boolean esOtraCircunstancia;
	
	@Column(name = "OTRA_CIRCUNSTANCIA")
	private String otraCircunstancia;
	
	@Transient
	private Boolean consultaSumaAsegurada;

	public EstimacionCoberturaReporteCabina() {

	}
	
	

	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getFolio() {
		return folio;
	}



	public void setFolio(String folio) {
		this.folio = folio;
	}



	public String getFolioCompaniaSeguros() {
		return folioCompaniaSeguros;
	}



	public void setFolioCompaniaSeguros(String folioCompaniaSeguros) {
		this.folioCompaniaSeguros = folioCompaniaSeguros;
	}



	public String getLadaTelContacto() {
		return ladaTelContacto;
	}



	public void setLadaTelContacto(String ladaTelContacto) {
		this.ladaTelContacto = ladaTelContacto;
	}



	public String getLadaTelContactoDos() {
		return ladaTelContactoDos;
	}



	public void setLadaTelContactoDos(String ladaTelContactoDos) {
		this.ladaTelContactoDos = ladaTelContactoDos;
	}



	public String getNombreAfectado() {
		return nombreAfectado;
	}



	public void setNombreAfectado(String nombreAfectado) {
		this.nombreAfectado = nombreAfectado;
	}



	public String getNombrePersonaContacto() {
		return nombrePersonaContacto;
	}



	public void setNombrePersonaContacto(String nombrePersonaContacto) {
		this.nombrePersonaContacto = nombrePersonaContacto;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getNumeroSiniestroTercero() {
		return numeroSiniestroTercero;
	}



	public void setNumeroSiniestroTercero(String numeroSiniestroTercero) {
		this.numeroSiniestroTercero = numeroSiniestroTercero;
	}



	public Integer getSecuenciaPaseAtencion() {
		return secuenciaPaseAtencion;
	}



	public void setSecuenciaPaseAtencion(Integer secuenciaPaseAtencion) {
		this.secuenciaPaseAtencion = secuenciaPaseAtencion;
	}



	



	public BigDecimal getSumaAseguradaObtenida() {
		return sumaAseguradaObtenida;
	}



	public void setSumaAseguradaObtenida(BigDecimal sumaAseguradaObtenida) {
		this.sumaAseguradaObtenida = sumaAseguradaObtenida;
	}



	public String getTelContacto() {
		return telContacto;
	}



	public void setTelContacto(String telContacto) {
		this.telContacto = telContacto;
	}



	public String getTelContactoDos() {
		return telContactoDos;
	}



	public void setTelContactoDos(String telContactoDos) {
		this.telContactoDos = telContactoDos;
	}



	public Boolean getTieneCompaniaSeguros() {
		return tieneCompaniaSeguros;
	}



	public void setTieneCompaniaSeguros(Boolean tieneCompaniaSeguros) {
		this.tieneCompaniaSeguros = tieneCompaniaSeguros;
	}


	public String getTipoEstimacion() {
		return tipoEstimacion;
	}



	public void setTipoEstimacion(String tipoEstimacion) {
		this.tipoEstimacion = tipoEstimacion;
	}



	public String getTipoPaseAtencion() {
		return tipoPaseAtencion;
	}



	public void setTipoPaseAtencion(String tipoPaseAtencion) {
		this.tipoPaseAtencion = tipoPaseAtencion;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}



	public CoberturaReporteCabina getCoberturaReporteCabina() {
		return coberturaReporteCabina;
	}



	public void setCoberturaReporteCabina(
			CoberturaReporteCabina coberturaReporteCabina) {
		this.coberturaReporteCabina = coberturaReporteCabina;
	}



	public PrestadorServicio getCompaniaSegurosTercero() {
		return companiaSegurosTercero;
	}

	public Integer getSecuenciaPaseDeCobertura() {
		return secuenciaPaseDeCobertura;
	}

	public void setSecuenciaPaseDeCobertura(Integer secuenciaPaseDeCobertura) {
		this.secuenciaPaseDeCobertura = secuenciaPaseDeCobertura;
	}



	public void setCompaniaSegurosTercero(PrestadorServicio companiaSegurosTercero) {
		this.companiaSegurosTercero = companiaSegurosTercero;
	}
	
	public List<MovimientoCoberturaSiniestro> getListadoMovimientos() {
		return listadoMovimientos;
	}

	public void setListadoMovimientos(
			List<MovimientoCoberturaSiniestro> listadoMovimientos) {
		this.listadoMovimientos = listadoMovimientos;
	}

	public void finalize() throws Throwable {
		super.finalize();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <K> K getKey() {
		return (K) this.id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}



	public Long getSecuenciaImpresion() {
		return secuenciaImpresion;
	}



	public void setSecuenciaImpresion(Long secuenciaImpresion) {
		this.secuenciaImpresion = secuenciaImpresion;
	}



	public Date getFechaImpresion() {
		return fechaImpresion;
	}



	public void setFechaImpresion(Date fechaImpresion) {
		this.fechaImpresion = fechaImpresion;
	}



	public ConfiguracionDepuracionReserva getConfiguracionDepuracion() {
		return configuracionDepuracion;
	}



	public void setConfiguracionDepuracion(
			ConfiguracionDepuracionReserva configuracionDepuracion) {
		this.configuracionDepuracion = configuracionDepuracion;
	}



	public Date getFechaDepuracionTotal() {
		return fechaDepuracionTotal;
	}



	public void setFechaDepuracionTotal(Date fechaDepuracionTotal) {
		this.fechaDepuracionTotal = fechaDepuracionTotal;
	}

	public Boolean getDepuracionTotal() {
		return depuracionTotal;
	}
	
	public void setDepuracionTotal(Boolean depuracionTotal) {
		this.depuracionTotal = depuracionTotal;
	}



	public Boolean getAplicaDeducible() {
		return aplicaDeducible;
	}



	public void setAplicaDeducible(Boolean aplicaDeducible) {
		this.aplicaDeducible = aplicaDeducible;
	}



	public String getCveSubCalculo() {
		return cveSubCalculo;
	}



	public void setCveSubCalculo(String cveSubCalculo) {
		this.cveSubCalculo = cveSubCalculo;
	}



	public String getEsEditable() {
		return esEditable;
	}



	public void setEsEditable(String esEditable) {
		this.esEditable = esEditable;
	}
	
	@Transient
	public String getTelefonoContactoCompleto(){
		StringBuilder telefono = new StringBuilder("");
		telefono.append(!StringUtil.isEmpty(ladaTelContacto) ? ladaTelContacto.concat(" ") : "");		
		telefono.append(!StringUtil.isEmpty(telContacto) ? telContacto : "");
		return telefono.toString();
	}
	
	@Transient
	public String getTelefonoContactoCompletoDos(){
		StringBuilder telefono = new StringBuilder("");
		telefono.append(!StringUtil.isEmpty(ladaTelContactoDos) ? ladaTelContactoDos.concat(" ") : "");		
		telefono.append(!StringUtil.isEmpty(telContactoDos) ? telContactoDos : "");
		return telefono.toString();
	}



	public String getMotivoNoAplicaDeducible() {
		return motivoNoAplicaDeducible;
	}



	public void setMotivoNoAplicaDeducible(String motivoNoAplicaDeducible) {
		this.motivoNoAplicaDeducible = motivoNoAplicaDeducible;
	}

	public Boolean getConsultaSumaAsegurada() {
		return consultaSumaAsegurada;
	}

	public void setConsultaSumaAsegurada(Boolean consultaSumaAsegurada) {
		this.consultaSumaAsegurada = consultaSumaAsegurada;
	}

	public Boolean getEmailNoProporcionado() {
		return emailNoProporcionado;
	}



	public void setEmailNoProporcionado(Boolean emailNoProporcionado) {
		this.emailNoProporcionado = emailNoProporcionado;
	}



	public String getEmailNoProporcionadoMotivo() {
		return emailNoProporcionadoMotivo;
	}



	public void setEmailNoProporcionadoMotivo(String emailNoProporcionadoMotivo) {
		this.emailNoProporcionadoMotivo = emailNoProporcionadoMotivo;
	}

	public String getDua() {
		return dua;
	}

	public void setDua(String dua) {
		this.dua = dua;
	}

	public String getCircunstancia() {
		return circunstancia;
	}

	public void setCircunstancia(String circunstancia) {
		this.circunstancia = circunstancia;
	}

	public Boolean getEsOtraCircunstancia() {
		return esOtraCircunstancia;
	}

	public void setEsOtraCircunstancia(Boolean esOtraCircunstancia) {
		this.esOtraCircunstancia = esOtraCircunstancia;
	}

	public String getOtraCircunstancia() {
		return otraCircunstancia;
	}

	public void setOtraCircunstancia(String otraCircunstancia) {
		this.otraCircunstancia = otraCircunstancia;
	}


	public enum TipoEstimacion implements EnumBase<String>{
		DANIOS_MATERIALES("DMA"),
		RC_BIENES("RCB"),
		RC_PERSONA("RCP"),
		RC_VEHICULO("RCV"),
		GASTOS_MEDICOS("GME"),
		GASTOS_MEDICOS_OCUPANTES("GMC"),
		RC_VIAJERO("RCJ"),
		DIRECTA("ESD");

	    private final String tipoEstimacion;

	    private TipoEstimacion(String tipoEstimacion) {
	        this.tipoEstimacion = tipoEstimacion;
	    }

	    @Override
	    public String toString(){
	       return tipoEstimacion;
	    }

		@Override
		public String getValue() {
			return tipoEstimacion;
		}

		@Override
		public String getLabel() {
			return tipoEstimacion;
		}

	}
	
	public enum TipoPaseAtencion implements EnumBase<String>{
		PASE_ATENCION_TALLER("PAT", "Pase de Atencion a Taller"),
		REEMBOLSO_A_CIA("RAC", "Reembolso a Compania"),
		SOLO_REGISTRO("SRE", "Solo Registro"),
		GASTOS_MEDICOS_TERCEROS("GMT", "Gastos Medicos Terceros"),
		GASTOS_MEDICOS_OCUPANTES("GMO", "Gastos Medicos Ocupantes"),
		OBRA_CIVIL("OBC", "Obra Civil"),
		PAGO_DANIOS("PDA", "Pago de Danios"),
		SIPAC("SIPAC","SIPAC"),
		SOLO_REG_SIPAC("SIPACSRE","Solo Registro SIPAC");

	    private final String tipoPase;
	    private final String label;

	    private TipoPaseAtencion(String tipoPase, String label) {
	        this.tipoPase = tipoPase;
	        this.label = label;
	    }

	    @Override
	    public String toString(){
	       return tipoPase;
	    }

		@Override
		public String getValue() {
			return tipoPase;
		}

		@Override
		public String getLabel() {
			return label;
		}

	}
	
	public enum ClaveSubCalculo implements EnumBase<String>{
		RC_BIENES("RCB"),
		RC_PERSONA("RCP"),
		RC_VEHICULO("RCV");

	    private final String claveSubCalculo;

	    private ClaveSubCalculo(String claveSubCalculo) {
	        this.claveSubCalculo = claveSubCalculo;
	    }

	    @Override
	    public String toString(){
	       return claveSubCalculo;
	    }

		@Override
		public String getValue() {
			return claveSubCalculo;
		}

		@Override
		public String getLabel() {
			return claveSubCalculo;
		}

	}
	
}
