package mx.com.afirme.midas2.domain.siniestros.valuacion.hgs;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name = "TOSNDEVOLUCIONHGSREFACCION", schema = "MIDAS") 
public class DevolucionHgsRefaccion implements Entidad {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTOSNDEVOLUCIONHGSREF_SEQ")
	@SequenceGenerator(name="IDTOSNDEVOLUCIONHGSREF_SEQ", schema="MIDAS", sequenceName="IDTOSNDEVOLUCIONHGSREF_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DEVOLUCION_ID", nullable=false, referencedColumnName="id")
	private DevolucionHgs devolucionHgs;
	
	@Column(name="NOMBRE")
	private String nombre;
	
	@Column(name="SUB_TOTAL")
	private BigDecimal subTotal;
	
	@Column(name="IVA")
	private BigDecimal iva;
	
	@Column(name="MONTO_TOTAL")
	private BigDecimal montoTotal;
	
	
	@Override
	public Long getKey() {
		// TODO Auto-generated method stub
		return this.id;
	}
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DevolucionHgs getDevolucionHgs() {
		return devolucionHgs;
	}

	public void setDevolucionHgs(DevolucionHgs devolucionHgs) {
		this.devolucionHgs = devolucionHgs;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public BigDecimal getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}

	public BigDecimal getIva() {
		return iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public BigDecimal getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String toString() {
		return "DevolucionHgsRefaccion [id=" + id + ", devolucionHgs="
				+ devolucionHgs + ", nombre=" + nombre + ", subTotal="
				+ subTotal + ", iva=" + iva + ", montoTotal=" + montoTotal
				+ "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DevolucionHgsRefaccion other = (DevolucionHgsRefaccion) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
