package mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.catalogos.EnumBase;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
@Entity(name = "ConfiguracionNotificacionCabina")
@Table(name = "TOCONFIGNOTIFICACIONCABINA", schema = "MIDAS")
public class ConfiguracionNotificacionCabina implements Entidad{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1165304030702533887L;
	
	public static enum Estatus implements EnumBase<Integer>{
		ACTIVA((int)1, "Activa"),  INACTIVA((int)0, "Inactiva");		
		
		Estatus(Integer estatus, String label) {
			this.estatus = estatus;
			this.label = label;
		}
		private Integer estatus;
		
		private String label;

		@Override
		public Integer getValue() {
			return estatus;
		}
		@Override
		public String getLabel() {
			return label;
		}

	};

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOCONFIGNOTIFCABINA_SEQ_ID_GENERATOR")
	@SequenceGenerator(name="TOCONFIGNOTIFCABINA_SEQ_ID_GENERATOR", schema="MIDAS", sequenceName="IDTOCONFIGNOTIFCABINA_SEQ", allocationSize=1)	
	@Column(name = "ID")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinFetch(JoinFetchType.OUTER)
	@JoinColumn(name = "OFICINA_ID", referencedColumnName = "id")
	private Oficina oficina;
	
	@ManyToOne
	@JoinFetch(JoinFetchType.INNER)
	@JoinColumn(name = "PROCESO_ID", referencedColumnName = "id")
	private ProcesoCabina procesoCabina;
	
	@ManyToOne
	@JoinFetch(JoinFetchType.INNER)
	@JoinColumn(name = "MOVIMIENTO_ID", referencedColumnName = "id")
	private MovimientoProcesoCabina movimientoProceso;
	
	@Column(name = "NOTAS")
	private String notas;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CREACION", nullable = false)
	private Date   fechaCreacion = new Date();
	
	@Column(name = "CODIGO_USUARIO_CREACION", length = 8)
	private String codigoUsuarioCreacion;
	
	@Column(name = "ESACTIVO")
	private Integer esActivo;
	
	@Column(name = "INVISIBLE")
	private Boolean invisible = Boolean.FALSE;
	
	@Column(name = "AGENTE_ID")
	private Integer agenteId;
	
	@Column(name = "NUMPOLIZA")
	private String numPoliza;
	
	@Column(name = "NUMINCISO")
	private String numInciso;
	
	@OneToMany(fetch=FetchType.LAZY,  mappedBy = "confNotificacionCabina", cascade = CascadeType.ALL, orphanRemoval=true)
	private List<DestinatarioConfigNotificacionCabina> destinatariosNotificacion = new ArrayList<DestinatarioConfigNotificacionCabina>();
	
	@OneToMany(fetch=FetchType.LAZY,  mappedBy = "confNotificacionCabina", cascade = CascadeType.ALL, orphanRemoval=true)
	private List<AdjuntoConfigNotificacionCabina> adjuntosNotificacion = new ArrayList<AdjuntoConfigNotificacionCabina>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Oficina getOficina() {
		return oficina;
	}

	public void setOficina(Oficina oficina) {
		this.oficina = oficina;
	}

	public ProcesoCabina getProcesoCabina() {
		return procesoCabina;
	}

	public void setProcesoCabina(ProcesoCabina procesoCabina) {
		this.procesoCabina = procesoCabina;
	}

	public MovimientoProcesoCabina getMovimientoProceso() {
		return movimientoProceso;
	}

	public void setMovimientoProceso(MovimientoProcesoCabina movimientoProceso) {
		this.movimientoProceso = movimientoProceso;
	}

	public String getNotas() {
		return notas;
	}

	public void setNotas(String notas) {
		this.notas = notas;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	public Integer getEsActivo() {
		return esActivo;
	}

	public void setEsActivo(Integer esActivo) {
		this.esActivo = esActivo;
	}

	@Transient
	public boolean esActiva() {
		return (this.esActivo.equals(Estatus.ACTIVA.getValue()));
	}

	public List<DestinatarioConfigNotificacionCabina> getDestinatariosNotificacion() {
		return destinatariosNotificacion;
	}

	public void setDestinatariosNotificacion(
			List<DestinatarioConfigNotificacionCabina> destinatariosNotificacion) {
		this.destinatariosNotificacion = destinatariosNotificacion;
	}

	public List<AdjuntoConfigNotificacionCabina> getAdjuntosNotificacion() {
		return adjuntosNotificacion;
	}

	public void setAdjuntosNotificacion(
			List<AdjuntoConfigNotificacionCabina> adjuntosNotificacion) {
		this.adjuntosNotificacion = adjuntosNotificacion;
	}

	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof ConfiguracionNotificacionCabina) {
			ConfiguracionNotificacionCabina confNotificacion = (ConfiguracionNotificacionCabina) object;
			equal = confNotificacion.getId().equals(this.id);
		}
		return equal;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	public Boolean getInvisible() {
		return invisible;
	}

	public void setInvisible(Boolean invisible) {
		this.invisible = invisible;
	}

	public Integer getAgenteId() {
		return agenteId;
	}

	public void setAgenteId(Integer agenteId) {
		this.agenteId = agenteId;
	}

	public String getNumPoliza() {
		return numPoliza;
	}

	public void setNumPoliza(String numPoliza) {
		this.numPoliza = numPoliza;
	}

	public String getNumInciso() {
		return numInciso;
	}

	public void setNumInciso(String numInciso) {
		this.numInciso = numInciso;
	}
	
	
}
