package mx.com.afirme.midas2.domain.siniestros.recuperacion;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 10-jul-2015 07:07:10 p.m.
 */
@Entity(name = "OrdenCompraCartaCia")
@Table(name = "TOSNORDENCOMPRACIA", schema = "MIDAS")
public class OrdenCompraCartaCia extends MidasAbstracto {		
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8330233374406299143L;

	@Id
	@SequenceGenerator(name = "TOSNORDENCOMPRACIA_SEQ_GENERADOR",allocationSize = 1, sequenceName = "TOSNORDENCOMPRACIA_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TOSNORDENCOMPRACIA_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id ; 

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "RECUPERACION_ID", referencedColumnName = "RECUPERACION_ID")
	private RecuperacionCompania recuperacion;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ORDENCOMPRA_ID")
	private OrdenCompra ordenCompra;	
	
	@Column(name = "SUBTOTAL_OC")
	private BigDecimal subtotalOC;
	
	@Column(name = "MONTO_RECUPERAR")
	private BigDecimal montoARecuperar;

	
	
	@Column(name = "SUBTOTAL_RECUP_PROVEEDOR")
	private BigDecimal subTotalRecupProveedor;

	
	@Transient
	private String conceptoDesc;
	
	@Transient
	private Integer porcentajeParticipacion;
	
	
	@Transient
	private String nombreProveedor;
	
	@Transient 
	String numeroFactura;
	
	@Transient
	private Boolean seleccionado;
	
	@Transient
	private Boolean isIndemnizacion;
	
	@Transient
	private String tipoOcDesc;
	
	@Transient
	private BigDecimal montoPiezasConPorcentaje;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	
	public RecuperacionCompania getRecuperacion() {
		return recuperacion;
	}
	public void setRecuperacion(RecuperacionCompania recuperacion) {
		this.recuperacion = recuperacion;
	}
	public OrdenCompra getOrdenCompra() {
		return ordenCompra;
	}
	public void setOrdenCompra(OrdenCompra ordenCompra) {
		this.ordenCompra = ordenCompra;
	}
	public BigDecimal getMontoARecuperar() {
		return montoARecuperar;
	}
	public void setMontoARecuperar(BigDecimal montoARecuperar) {
		this.montoARecuperar = montoARecuperar;
	}
	public BigDecimal getSubtotalOC() {
		return subtotalOC;
	}
	public void setSubtotalOC(BigDecimal subtotalOC) {
		this.subtotalOC = subtotalOC;
	}
	public BigDecimal getSubTotalRecupProveedor() {
		return subTotalRecupProveedor;
	}
	public void setSubTotalRecupProveedor(BigDecimal subTotalRecupProveedor) {
		this.subTotalRecupProveedor = subTotalRecupProveedor;
	}
	public String getConceptoDesc() {
		return conceptoDesc;
	}
	public void setConceptoDesc(String conceptoDesc) {
		this.conceptoDesc = conceptoDesc;
	}
	public Integer getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}
	public void setPorcentajeParticipacion(Integer porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}
	public Boolean getSeleccionado() {
		return seleccionado;
	}
	public void setSeleccionado(Boolean seleccionado) {
		this.seleccionado = seleccionado;
	}	
	
	public String getTipoOcDesc() {
		return tipoOcDesc;
	}	
	
	public void setTipoOcDesc(String tipoOcDesc) {
		this.tipoOcDesc = tipoOcDesc;
	}
	
	/**
	 * @return the nombreProveedor
	 */
	public String getNombreProveedor() {
		return nombreProveedor;

	}

	/**
	 *  @param nombreProveedor the nombreProveedor to set
	 */
	public void setNombreProveedor(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}
	/**
	 * @return the numeroFactura
	 */
	public String getNumeroFactura() {
		return numeroFactura;
	}
	/**
	 * @param numeroFactura the numeroFactura to set
	 */
	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	public Boolean getIsIndemnizacion() {
		return isIndemnizacion;
	}
	public void setIsIndemnizacion(Boolean isIndemnizacion) {
		this.isIndemnizacion = isIndemnizacion;
	}
	public BigDecimal getMontoPiezasConPorcentaje() {
		return montoPiezasConPorcentaje;
	}
	public void setMontoPiezasConPorcentaje(BigDecimal montoPiezasConPorcentaje) {
		this.montoPiezasConPorcentaje = montoPiezasConPorcentaje;
	}
	
}