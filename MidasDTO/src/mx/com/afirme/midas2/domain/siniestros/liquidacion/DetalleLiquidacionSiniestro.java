package mx.com.afirme.midas2.domain.siniestros.liquidacion;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.pagos.OrdenPagoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;


/**
 * entidad que mapea el detalle de una �rden de compra, cada objeto de esta tabla
 * representa un concepto de la �rden de compra.
 * @author usuario
 * @version 1.0
 * @created 22-ago-2014 10:48:42 a.m.
 */



@Entity(name = "DetalleLiquidacionSiniestro")
@Table(name = "TODETALLE_LIQ_SINIESTRO", schema = "MIDAS")
public class DetalleLiquidacionSiniestro extends MidasAbstracto {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TODETALLE_LIQ_SINIESTRO_SEQ_GENERADOR",allocationSize = 1, sequenceName = "TODETALLE_LIQ_SINIESTRO_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TODETALLE_LIQ_SINIESTRO_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_LIQUIDACION_SINIESTRO", referencedColumnName = "ID")	
	private LiquidacionSiniestro liquidacionSiniestro;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_ORDEN_COMPRA", referencedColumnName = "ID")
	@JoinFetch(JoinFetchType.OUTER)
	private OrdenCompra ordenCompra;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_ORDEN_PAGO ", referencedColumnName = "ID")
	@JoinFetch(JoinFetchType.OUTER)
	private OrdenPagoSiniestro ordenPagoSiniestro;
	
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LiquidacionSiniestro getLiquidacionSiniestro() {
		return liquidacionSiniestro;
	}

	public void setLiquidacionSiniestro(LiquidacionSiniestro liquidacionSiniestro) {
		this.liquidacionSiniestro = liquidacionSiniestro;
	}

	public OrdenCompra getOrdenCompra() {
		return ordenCompra;
	}

	public void setOrdenCompra(OrdenCompra ordenCompra) {
		this.ordenCompra = ordenCompra;
	}

	public OrdenPagoSiniestro getOrdenPagoSiniestro() {
		return ordenPagoSiniestro;
	}

	public void setOrdenPagoSiniestro(OrdenPagoSiniestro ordenPagoSiniestro) {
		this.ordenPagoSiniestro = ordenPagoSiniestro;
	}	

}