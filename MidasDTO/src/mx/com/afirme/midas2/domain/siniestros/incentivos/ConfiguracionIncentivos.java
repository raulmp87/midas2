package mx.com.afirme.midas2.domain.siniestros.incentivos;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.EnumBase;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;



@Entity
@Table(name = "TOSNCONFIGINCENTIVOS", schema = "MIDAS")
public class ConfiguracionIncentivos extends MidasAbstracto {
	
	private static final long serialVersionUID = 1L;
	
	public static final String SECUENCIA_FOLIO = "MIDAS.TOSNCONFIGINCENTIVO_FOLIO_SEQ";
	
	@Id 
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOSNCONFIGINCENTIVO_SEQ")
	@SequenceGenerator(name="TOSNCONFIGINCENTIVO_SEQ", schema="MIDAS", sequenceName="TOSNCONFIGINCENTIVO_SEQ", allocationSize=1)	
    @Column(name="ID", unique=true, nullable=false, precision=8)
	private Long id;
	
	
	@Column(name="FOLIO")
	private Long folio;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="OFICINA_ID", referencedColumnName="ID")
	@JoinFetch(JoinFetchType.INNER)
	private Oficina oficina;
	
	@Column(name="ESTATUS")
	@Enumerated(EnumType.STRING)
	private EstatusConfiguracionIncentivo estatus;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "PERIODO_INI")
	private Date periodoInicial;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "PERIODO_FIN")
	private Date periodoFinal;
	
	@Column(name="DIAS_INHABILES")
	private Boolean esDiasInhabiles;
	
	
	@Column(name="MONTO_DIA_INHABIL")
	private BigDecimal montoDiaInhabil;
	
	@Column(name="RECUPERACION_EFECTIVO")
	private Boolean esRecuperacionEfectivo;
	
	@Column(name="RANGO_EFECTIVO")
	private Boolean esRangoEfectivo;
	
	@Column(name="MONTODE_RANGOEFECTIVO")
	private BigDecimal montoRangoEfectivo;
	
	@Column(name="MONTOHASTA_RANGOEFECTIVO")
	private BigDecimal montoHastaRangoEfectivo;
	
	@Column(name="PORCENTAJE_RANGO_EFECTIVO")
	private BigDecimal porcentajeRangoEfectivo;
	
	@Column(name="EXCEDENTE_SINIESTRO")
	private Boolean esExcedenteSiniestro;
	
	@Column(name="CONDICION_EXCEDENTE")
	private String condicionExcedente;
	
	@Column(name="MONTO_EXCEDENTE")
	private BigDecimal montoExcedente;
	
	@Column(name="PORCENTAJE_EXCEDENTE")
	private BigDecimal porcentajeExcedente;
	
	@Column(name="RECUPERACION_CIA")
	private Boolean esRecuperacionCia;
	
	@Column(name="MONTO_RECUPERACION_CIA")
	private BigDecimal montoRecuperacionCia;
	
	@Column(name="RECUPERACION_SIPAC")
	private Boolean esRecuperacionSipac;
	
	@Column(name="MONTO_RECUPERACION_SIPAC")
	private BigDecimal montoRecuperacionSipac;
	
	@Column(name="SINIESTRO_RECHAZADO")
	private Boolean esSiniestroRechazado;;
	
	@Column(name="PORCENTAJE_RECHAZO")
	private BigDecimal porcentajeRechazo;
	
	@Column(name="CODIGO_USUARIO_SOLICITAUT")
	private String codigoUsuarioSolicitudAutorizacion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_SOLICITAUT")
	private Date fechaSolicitudAutorizacion;
	
	@Column(name="CODIGO_USR_PRIMER_NVL")
	private String codigoUsuarioPrimerNivel;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_AUT_NVL_UNO")
	private Date fechaAutorizacionPrimerNivel;
	
	@Column(name="CODIGO_USR_SEG_NVL")
	private String codigoUsuarioSegundoNivel;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_AUT_NVL_DOS")
	private Date fechaAutorizacionSegundoNivel;
	
	@Column(name="CODIGO_USR_RECHAZO")
	private String codigoUsuarioRechazo;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_DE_RECHAZO")
	private Date fechaDeRechazo;
	
	@Column(name="CODIGO_USR_CANCELACION")
	private String codigoUsuarioCancelacion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_DE_CANCELACION")
	private Date fechaDeCancelacion;
	
	@OneToMany(mappedBy="configuracionIncentivos", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinFetch(JoinFetchType.OUTER)
	private List<IncentivoAjustador> incentivos;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy = "configuracionIncentivo")
	@JoinFetch(JoinFetchType.OUTER)
	private List<ConfiguracionIncentivosNotificacion> historicoNotificaciones;
	
	public enum EstatusConfiguracionIncentivo implements EnumBase<String>{
		TRAMITE("En tr\u00E1mite"), 
		REGISTRADO("Registrado"), 
		XAUTORIZAR("Por Autorizar"), 
		PREAPROBAD("Preaprobado"), 
		RECHAZADO("Rechazado"), 
		AUTORIZADO("Autorizado"), 
		ENVIADO("Enviado"), 
		CANCELADO("Cancelado");
		
		private String descripcion;
		
		EstatusConfiguracionIncentivo(String descripcion){
			this.descripcion = descripcion;
		}

		@Override
		public String getValue() {
			return this.toString();
		}

		@Override
		public String getLabel() {
			return this.descripcion;
		}
		
	};
	
	@Transient
	private Boolean esAutorizable;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getFolio() {
		return folio;
	}

	public void setFolio(Long folio) {
		this.folio = folio;
	}

	public Oficina getOficina() {
		return oficina;
	}

	public void setOficina(Oficina oficina) {
		this.oficina = oficina;
	}

	public EstatusConfiguracionIncentivo getEstatus() {
		return estatus;
	}

	public void setEstatus(EstatusConfiguracionIncentivo estatus) {
		this.estatus = estatus;
	}

	public Date getPeriodoInicial() {
		return periodoInicial;
	}

	public void setPeriodoInicial(Date periodoInicial) {
		this.periodoInicial = periodoInicial;
	}

	public Date getPeriodoFinal() {
		return periodoFinal;
	}

	public void setPeriodoFinal(Date periodoFinal) {
		this.periodoFinal = periodoFinal;
	}

	public Boolean getEsDiasInhabiles() {
		return esDiasInhabiles;
	}

	public void setEsDiasInhabiles(Boolean esDiasInhabiles) {
		this.esDiasInhabiles = esDiasInhabiles;
	}

	public BigDecimal getMontoDiaInhabil() {
		return montoDiaInhabil;
	}

	public void setMontoDiaInhabil(BigDecimal montoDiaInhabil) {
		this.montoDiaInhabil = montoDiaInhabil;
	}

	public Boolean getEsRecuperacionEfectivo() {
		return esRecuperacionEfectivo;
	}

	public void setEsRecuperacionEfectivo(Boolean esRecuperacionEfectivo) {
		this.esRecuperacionEfectivo = esRecuperacionEfectivo;
	}

	public Boolean getEsRangoEfectivo() {
		return esRangoEfectivo;
	}

	public void setEsRangoEfectivo(Boolean esRangoEfectivo) {
		this.esRangoEfectivo = esRangoEfectivo;
	}

	public BigDecimal getMontoRangoEfectivo() {
		return montoRangoEfectivo;
	}

	public void setMontoRangoEfectivo(BigDecimal montoRangoEfectivo) {
		this.montoRangoEfectivo = montoRangoEfectivo;
	}

	public BigDecimal getMontoHastaRangoEfectivo() {
		return montoHastaRangoEfectivo;
	}

	public void setMontoHastaRangoEfectivo(BigDecimal montoHastaRangoEfectivo) {
		this.montoHastaRangoEfectivo = montoHastaRangoEfectivo;
	}

	public BigDecimal getPorcentajeRangoEfectivo() {
		return porcentajeRangoEfectivo;
	}

	public void setPorcentajeRangoEfectivo(BigDecimal porcentajeRangoEfectivo) {
		this.porcentajeRangoEfectivo = porcentajeRangoEfectivo;
	}

	public Boolean getEsExcedenteSiniestro() {
		return esExcedenteSiniestro;
	}

	public void setEsExcedenteSiniestro(Boolean esExcedenteSiniestro) {
		this.esExcedenteSiniestro = esExcedenteSiniestro;
	}

	public String getCondicionExcedente() {
		return condicionExcedente;
	}

	public void setCondicionExcedente(String condicionExcedente) {
		this.condicionExcedente = condicionExcedente;
	}

	public BigDecimal getMontoExcedente() {
		return montoExcedente;
	}

	public void setMontoExcedente(BigDecimal montoExcedente) {
		this.montoExcedente = montoExcedente;
	}

	public BigDecimal getPorcentajeExcedente() {
		return porcentajeExcedente;
	}

	public void setPorcentajeExcedente(BigDecimal porcentajeExcedente) {
		this.porcentajeExcedente = porcentajeExcedente;
	}

	public Boolean getEsRecuperacionCia() {
		return esRecuperacionCia;
	}

	public void setEsRecuperacionCia(Boolean esRecuperacionCia) {
		this.esRecuperacionCia = esRecuperacionCia;
	}

	public BigDecimal getMontoRecuperacionCia() {
		return montoRecuperacionCia;
	}

	public void setMontoRecuperacionCia(BigDecimal montoRecuperacionCia) {
		this.montoRecuperacionCia = montoRecuperacionCia;
	}

	public Boolean getEsRecuperacionSipac() {
		return esRecuperacionSipac;
	}

	public void setEsRecuperacionSipac(Boolean esRecuperacionSipac) {
		this.esRecuperacionSipac = esRecuperacionSipac;
	}	

	public BigDecimal getMontoRecuperacionSipac() {
		return montoRecuperacionSipac;
	}

	public void setMontoRecuperacionSipac(BigDecimal montoRecuperacionSipac) {
		this.montoRecuperacionSipac = montoRecuperacionSipac;
	}

	public Boolean getEsSiniestroRechazado() {
		return esSiniestroRechazado;
	}

	public void setEsSiniestroRechazado(Boolean esSiniestroRechazado) {
		this.esSiniestroRechazado = esSiniestroRechazado;
	}

	public BigDecimal getPorcentajeRechazo() {
		return porcentajeRechazo;
	}

	public void setPorcentajeRechazo(BigDecimal porcentajeRechazo) {
		this.porcentajeRechazo = porcentajeRechazo;
	}

	public String getCodigoUsuarioSolicitudAutorizacion() {
		return codigoUsuarioSolicitudAutorizacion;
	}

	public void setCodigoUsuarioSolicitudAutorizacion(
			String codigoUsuarioSolicitudAutorizacion) {
		this.codigoUsuarioSolicitudAutorizacion = codigoUsuarioSolicitudAutorizacion;
	}

	public Date getFechaSolicitudAutorizacion() {
		return fechaSolicitudAutorizacion;
	}

	public void setFechaSolicitudAutorizacion(Date fechaSolicitudAutorizacion) {
		this.fechaSolicitudAutorizacion = fechaSolicitudAutorizacion;
	}

	
	public List<IncentivoAjustador> getIncentivos() {
		return incentivos;
	}

	public void setIncentivos(List<IncentivoAjustador> incentivos) {
		this.incentivos = incentivos;
	}
	
	public String getEstatusStr(){
		return this.estatus.toString();
	}
	
	public Boolean getEsAutorizable() {
		return esAutorizable;
	}

	public void setEsAutorizable(Boolean esAutorizable) {
		this.esAutorizable = esAutorizable;
	}
	
	public String getCodigoUsuarioPrimerNivel() {
		return codigoUsuarioPrimerNivel;
	}

	public void setCodigoUsuarioPrimerNivel(String codigoUsuarioPrimerNivel) {
		this.codigoUsuarioPrimerNivel = codigoUsuarioPrimerNivel;
	}

	public Date getFechaAutorizacionPrimerNivel() {
		return fechaAutorizacionPrimerNivel;
	}

	public void setFechaAutorizacionPrimerNivel(Date fechaAutorizacionPrimerNivel) {
		this.fechaAutorizacionPrimerNivel = fechaAutorizacionPrimerNivel;
	}

	public String getCodigoUsuarioSegundoNivel() {
		return codigoUsuarioSegundoNivel;
	}

	public void setCodigoUsuarioSegundoNivel(String codigoUsuarioSegundoNivel) {
		this.codigoUsuarioSegundoNivel = codigoUsuarioSegundoNivel;
	}

	public Date getFechaAutorizacionSegundoNivel() {
		return fechaAutorizacionSegundoNivel;
	}

	public void setFechaAutorizacionSegundoNivel(Date fechaAutorizacionSegundoNivel) {
		this.fechaAutorizacionSegundoNivel = fechaAutorizacionSegundoNivel;
	}

	public String getCodigoUsuarioRechazo() {
		return codigoUsuarioRechazo;
	}

	public void setCodigoUsuarioRechazo(String codigoUsuarioRechazo) {
		this.codigoUsuarioRechazo = codigoUsuarioRechazo;
	}

	public Date getFechaDeRechazo() {
		return fechaDeRechazo;
	}

	public void setFechaDeRechazo(Date fechaDeRechazo) {
		this.fechaDeRechazo = fechaDeRechazo;
	}
	
	public List<ConfiguracionIncentivosNotificacion> getHistoricoNotificaciones() {
		return historicoNotificaciones;
	}

	public void setHistoricoNotificaciones(
			List<ConfiguracionIncentivosNotificacion> historicoNotificaciones) {
		this.historicoNotificaciones = historicoNotificaciones;
	}

	public String getUsuarioAutorizador(){
		String autorizador = "";
		if(codigoUsuarioSegundoNivel != null){
			autorizador = this.codigoUsuarioSegundoNivel;
		}
		if(codigoUsuarioPrimerNivel != null){
			autorizador = this.codigoUsuarioPrimerNivel;
		}
		if(codigoUsuarioPrimerNivel != null && codigoUsuarioSegundoNivel!=null){
			autorizador = "varios";
		}
		return autorizador;
	}
	
	
	public Date getFechaAutorizacion(){
		Date fechaAutorizacion = null;
		if(fechaAutorizacionPrimerNivel != null){
			fechaAutorizacion = this.fechaAutorizacionPrimerNivel;
		}else{
			if(fechaAutorizacionSegundoNivel !=null){
				fechaAutorizacion = this.fechaAutorizacionSegundoNivel;
			}
		}
		return fechaAutorizacion;
	}
	
	/**
	 * @return the codigoUsuarioCancelacion
	 */
	public String getCodigoUsuarioCancelacion() {
		return codigoUsuarioCancelacion;
	}

	/**
	 * @param codigoUsuarioCancelacion the codigoUsuarioCancelacion to set
	 */
	public void setCodigoUsuarioCancelacion(String codigoUsuarioCancelacion) {
		this.codigoUsuarioCancelacion = codigoUsuarioCancelacion;
	}

	/**
	 * @return the fechaDeCancelacion
	 */
	public Date getFechaDeCancelacion() {
		return fechaDeCancelacion;
	}

	/**
	 * @param fechaDeCancelacion the fechaDeCancelacion to set
	 */
	public void setFechaDeCancelacion(Date fechaDeCancelacion) {
		this.fechaDeCancelacion = fechaDeCancelacion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}
