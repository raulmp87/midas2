package mx.com.afirme.midas2.domain.siniestros.recuperacion;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PaseRecuperacionId implements Serializable{

	private static final long serialVersionUID = 3177765585190036249L;

	@Column(name="COBERTURARECUPERACION_ID", nullable=false, insertable=false, updatable=false)
	private Long coberturaId;

	@Column(name="ESTIMACIONREPORTE_ID", nullable=false, insertable=false, updatable=false)
	private Long estimacionId;	

	public Long getCoberturaId() {
		return coberturaId;
	}

	public void setCoberturaId(Long coberturaId) {
		this.coberturaId = coberturaId;
	}

	public Long getEstimacionId() {
		return estimacionId;
	}

	public void setEstimacionId(Long estimacionId) {
		this.estimacionId = estimacionId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
		+ ((coberturaId == null) ? 0 : coberturaId.hashCode());
		result = prime * result
		+ ((estimacionId == null) ? 0 : estimacionId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof PaseRecuperacionId)) {
			return false;
		}
		PaseRecuperacionId other = (PaseRecuperacionId) obj;
		if (coberturaId == null) {
			if (other.coberturaId != null) {
				return false;
			}
		} else if (!coberturaId.equals(other.coberturaId)) {
			return false;
		}
		if (estimacionId == null) {
			if (other.estimacionId != null) {
				return false;
			}
		} else if (!estimacionId.equals(other.estimacionId)) {
			return false;
		}
		return true;
	}

}


