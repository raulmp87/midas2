package mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.EnumBase;
import mx.com.afirme.midas2.domain.MidasAbstracto;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

@Entity
@Table(name="TOSNMOVTOSACREEDORES", schema="MIDAS")
public class MovimientoCuentaAcreedor extends MidasAbstracto {

	private static final long serialVersionUID = -1170979787419647112L;
	
	/**
	 * Tipo de cuenta <code>C</code> Creacion <code>default</code>, <code>S</code> Saldo.
	 * @author Administrator
	 *
	 */
	public enum TIPO_MOVIMIENTO_ACREEDOR implements EnumBase<String> {
		
		ORIGEN("O"), CARGO("C"), ABONO("A");
		
		private final String value;
		
		private TIPO_MOVIMIENTO_ACREEDOR(String value) {
			this.value = value;
		}
		
		@Override
		public String getValue() {
			return this.value;
		}
		@Override
		public String getLabel() {
			return name();
		}
		
	};

	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOSNMOVTOSACREEDORES_GENERATOR")
	@SequenceGenerator(name="TOSNMOVTOSACREEDORES_GENERATOR", schema="MIDAS", sequenceName="TOSNMOVTOSACREEDORES_SEQ",allocationSize=1)	
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="APLICACION_ORIGINAL_ID", referencedColumnName="ID")
	@JoinFetch(JoinFetchType.INNER)
	private AplicacionCobranza aplicacionOriginal;

	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="APLICACION_CANCELACION_ID", referencedColumnName="ID")
	@JoinFetch(JoinFetchType.INNER)
	private AplicacionCobranza aplicacionCancelacion;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="APLICACION_MOVTO_ID", referencedColumnName="ID")
	@JoinFetch(JoinFetchType.OUTER)
	private AplicacionCobranza aplicacionMovimiento;
	
	@Column(name="TIPO")
	private String tipo = TIPO_MOVIMIENTO_ACREEDOR.ORIGEN.getValue();
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CUENTA_ID", referencedColumnName="ID")
	@JoinFetch(JoinFetchType.INNER)
	private ConceptoGuia cuenta;
	
	@Column(name="MONTO")
	private BigDecimal monto;
	
	@Column(name="SALDO_FINAL")
	private BigDecimal saldoFinal;
	
	@Column(name="ESTATUS")
	private Boolean estatus = Boolean.TRUE;
	
	@Column(name="descripcion")
	private String descripcion;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MovimientoCuentaAcreedor other = (MovimientoCuentaAcreedor) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AplicacionCobranza getAplicacionOriginal() {
		return aplicacionOriginal;
	}

	public void setAplicacionOriginal(AplicacionCobranza aplicacionOriginal) {
		this.aplicacionOriginal = aplicacionOriginal;
	}

	public AplicacionCobranza getAplicacionCancelacion() {
		return aplicacionCancelacion;
	}

	public void setAplicacionCancelacion(AplicacionCobranza aplicacionCancelacion) {
		this.aplicacionCancelacion = aplicacionCancelacion;
	}

	public AplicacionCobranza getAplicacionMovimiento() {
		return aplicacionMovimiento;
	}

	public void setAplicacionMovimiento(AplicacionCobranza aplicacionMovimiento) {
		this.aplicacionMovimiento = aplicacionMovimiento;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public ConceptoGuia getCuenta() {
		return cuenta;
	}

	public void setCuenta(ConceptoGuia cuenta) {
		this.cuenta = cuenta;
	}

	public BigDecimal getMonto() {
		return monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	public BigDecimal getSaldoFinal() {
		return saldoFinal;
	}

	public void setSaldoFinal(BigDecimal saldoFinal) {
		this.saldoFinal = saldoFinal;
	}


	public Boolean getEstatus() {
		return estatus;
	}

	public void setEstatus(Boolean estatus) {
		this.estatus = estatus;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

}
