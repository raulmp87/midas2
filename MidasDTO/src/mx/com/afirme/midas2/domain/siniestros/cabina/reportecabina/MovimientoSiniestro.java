package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;


@Entity
@Table(name = "TOMOVIMIENTOSINIESTRO", schema = "MIDAS")
public class MovimientoSiniestro extends MidasAbstracto {

	
	/**
	 * 
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDMOVSINIESTRO_SEQ")
	@SequenceGenerator(name = "IDMOVSINIESTRO_SEQ",  schema="MIDAS", sequenceName = "IDMOVSINIESTRO_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch (JoinFetchType.OUTER)
	@JoinColumn(name="REPORTECABINA_ID", referencedColumnName="ID")
	private ReporteCabina reporteCabina;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch (JoinFetchType.OUTER)
	@JoinColumn(name="ORDENCOMPRA_ID", referencedColumnName="ID")
	private OrdenCompra ordenCompra;	
	
	@Column(name = "IMPORTE_MOVIMIENTO")
	private BigDecimal importeMovimiento;
	
	@Column(name = "TIPO_DOCUMENTO")
	private String tipoDocumento;
	
	@Column(name = "TIPO_MOVIMIENTO")
	private String tipoMovimiento;
	
	@Column(name = "CAUSA_MOVIMIENTO")
	private String causaMovimiento;
	
	@Column(name = "DESCRIPCION")
	private String descripcion;

	@Column(name = "CARGA_ABONO_ID")
	private Long cargaAbono;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CONTABILIZACION")
	private Date fechaContabilizacion;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ReporteCabina getReporteCabina() {
		return reporteCabina;
	}

	public void setReporteCabina(ReporteCabina reporteCabina) {
		this.reporteCabina = reporteCabina;
	}

	public OrdenCompra getOrdenCompra() {
		return ordenCompra;
	}

	public void setOrdenCompra(OrdenCompra ordenCompra) {
		this.ordenCompra = ordenCompra;
	}

	public BigDecimal getImporteMovimiento() {
		return importeMovimiento;
	}

	public void setImporteMovimiento(BigDecimal importeMovimiento) {
		this.importeMovimiento = importeMovimiento;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getTipoMovimiento() {
		return tipoMovimiento;
	}

	public void setTipoMovimiento(String tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

	public String getCausaMovimiento() {
		return causaMovimiento;
	}

	public void setCausaMovimiento(String causaMovimiento) {
		this.causaMovimiento = causaMovimiento;
	}



	public Long getCargaAbono() {
		return cargaAbono;
	}

	public void setCargaAbono(Long cargaAbono) {
		this.cargaAbono = cargaAbono;
	}

	public Date getFechaContabilizacion() {
		return fechaContabilizacion;
	}

	public void setFechaContabilizacion(Date fechaContabilizacion) {
		this.fechaContabilizacion = fechaContabilizacion;
	}	

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <K> K getKey() {
		return (K) this.id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	
}