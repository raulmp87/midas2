package mx.com.afirme.midas2.domain.siniestros.expedientejuridico;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.MidasAbstracto;


/**
 * @author Israel
 * @version 1.0
 * @created 06-ago.-2015 10:29:18 a. m.
 */
@Entity
@Table(name = "TOSN_COMENTARIO_JURIDICO", schema = "MIDAS")
public class ComentarioJuridico extends MidasAbstracto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id 
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOSN_COMENTARIO_JURIDICO_SEQ")
	@SequenceGenerator(name="TOSN_COMENTARIO_JURIDICO_SEQ", schema="MIDAS", sequenceName="TOSN_COMENTARIO_JURIDICO_SEQ", allocationSize=1)	
    @Column(name="ID", unique=true, nullable=false, precision=8)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_EXPEDIENTE_JURIDICO", referencedColumnName = "ID")	
	private ExpedienteJuridico expedienteJuridico;
	
	@Column(name="COMENTARIO")
	private String comentario;
	
	@Column(name="TIPO")
	private String tipo;
	
	
	@Transient
	private String tipoDesc;
	
	
	public ComentarioJuridico(){

	}
	
	public static enum Tipo{
		AFIRME("AFIRM"),
		PROVEEDOR("PROVE");
		
		
		private String value;
		
		private Tipo(String value){
			this.value=value;
		}
		
		public String getValue(){
			return value;
		}
	}
	

	

	public ExpedienteJuridico getExpedienteJuridico() {
		return expedienteJuridico;
	}



	public void setExpedienteJuridico(ExpedienteJuridico expedienteJuridico) {
		this.expedienteJuridico = expedienteJuridico;
	}



	public String getComentario() {
		return comentario;
	}



	public void setComentario(String comentario) {
		this.comentario = comentario;
	}



	public String getTipo() {
		return tipo;
	}
	
	public String getTipoDesc() {
		return tipoDesc;
	}



	public void setTipoDesc(String tipoDesc) {
		this.tipoDesc = tipoDesc;
	}



	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}



	
	
	

}