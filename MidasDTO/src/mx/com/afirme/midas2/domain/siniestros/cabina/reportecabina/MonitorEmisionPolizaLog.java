package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="TOMONITOREMISIONPOLIZALOG",schema = "MIDAS")

public class MonitorEmisionPolizaLog implements Entidad {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7885660359813453005L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTOMONITOREMISIONPOLIZA_SEQ")
	@SequenceGenerator(name="IDTOMONITOREMISIONPOLIZA_SEQ", schema="MIDAS", sequenceName="IDTOMONITOREMISIONPOLIZA_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long   id;

	@Column(name="ID_SOLICITUD")
	private Long   idSolicitud;
	
	@Column(name="NO_INCISOS")
	private Long   noIncisos;
	
	@Column(name="BITEMPORAL")
	private String bitemporal;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION")
	private Date   fechaCreacion;
	
	@Column(name="COMENTARIOS")
	private String comentarios;
	
	@Column(name="PADRE")
	private Long   padre;
	
	@Column(name="ID_POLIZA")
	private String poliza;
	
	

	public MonitorEmisionPolizaLog(Long idSolicitud, Long noIncisos,
			String bitemporal, Date fechaCreacion, String comentarios,String poliza, Long padre) {
		this.idSolicitud   = idSolicitud;
		this.noIncisos     = noIncisos;
		this.bitemporal    = bitemporal;
		this.fechaCreacion = fechaCreacion;
		this.comentarios   = comentarios;
		this.poliza        = poliza;
		this.padre         = padre;
	}
	
	public MonitorEmisionPolizaLog(){}

	public Long getId() {
		return id;
	}

	public Long getIdSolicitud() {
		return idSolicitud;
	}

	public Long getNoIncisos() {
		return noIncisos;
	}

	public String getBitemporal() {
		return bitemporal;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	public void setNoIncisos(Long noIncisos) {
		this.noIncisos = noIncisos;
	}

	public void setBitemporal(String bitemporal) {
		this.bitemporal = bitemporal;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	
	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toString() {
		return "MonitorEmisionPolizaLog [id=" + id + ", idSolicitud="
				+ idSolicitud + ", noIncisos=" + noIncisos + ", bitemporal="
				+ bitemporal + ", fechaCreacion=" + fechaCreacion + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MonitorEmisionPolizaLog other = (MonitorEmisionPolizaLog) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public Long getPadre() {
		return padre;
	}

	public String getPoliza() {
		return poliza;
	}

	public void setPadre(Long padre) {
		this.padre = padre;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
}
