package mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class AplicacionCobranzaIngresoId implements Serializable{

	private static final long serialVersionUID = -6757518732630007764L;
	
	@Column(name="APLICACION_ID", nullable=false, updatable=false)
	private Long aplicacionId;
	
	@Column(name="INGRESO_ID", nullable=false, updatable=false)
	private Long ingresoId;

	public Long getAplicacionId() {
		return aplicacionId;
	}

	public void setAplicacionId(Long aplicacionId) {
		this.aplicacionId = aplicacionId;
	}

	public Long getIngresoId() {
		return ingresoId;
	}

	public void setIngresoId(Long ingresoId) {
		this.ingresoId = ingresoId;
	}
		
}
