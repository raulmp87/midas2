/**
 * 
 */
package mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.permisos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import mx.com.afirme.midas2.domain.personadireccion.PersonaDireccionMidasId;

/**
 * @author admin
 *
 */
@Embeddable
public class OrdenCompraAutorizacionId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name="ID_ORDEN_COMPRA")
	private Long idOrdenCompra;
	
	@Column(name="ID_DETALLE_ORDEN_COMPRA")
	private Long idDetalleOrdenCOmpra;
	
	@Column(name="NIVEL")
	private Integer nivel ;

	/**
	 * @return the idOrdenCompra
	 */
	public Long getIdOrdenCompra() {
		return idOrdenCompra;
	}

	/**
	 * @param idOrdenCompra the idOrdenCompra to set
	 */
	public void setIdOrdenCompra(Long idOrdenCompra) {
		this.idOrdenCompra = idOrdenCompra;
	}

	/**
	 * @return the idDetalleOrdenCOmpra
	 */
	public Long getIdDetalleOrdenCOmpra() {
		return idDetalleOrdenCOmpra;
	}

	/**
	 * @param idDetalleOrdenCOmpra the idDetalleOrdenCOmpra to set
	 */
	public void setIdDetalleOrdenCOmpra(Long idDetalleOrdenCOmpra) {
		this.idDetalleOrdenCOmpra = idDetalleOrdenCOmpra;
	}
	
	
	
	/**
	 * @return the nivel
	 */
	public Integer getNivel() {
		return nivel;
	}

	/**
	 * @param nivel the nivel to set
	 */
	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idOrdenCompra == null) ? 0 : idOrdenCompra.hashCode());
		result = prime * result
				+ ((idDetalleOrdenCOmpra == null) ? 0 : idDetalleOrdenCOmpra.hashCode());
		result = prime * result
				+ ((nivel == null) ? 0 : nivel.hashCode());
		return result;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof PersonaDireccionMidasId)) {
			return false;
		}
		OrdenCompraAutorizacionId other = (OrdenCompraAutorizacionId) obj;
		if (idOrdenCompra == null) {
			if (other.idOrdenCompra != null) {
				return false;
			}
		} else if (!idOrdenCompra.equals(other.idOrdenCompra)) {
			return false;
		}
		if (idDetalleOrdenCOmpra == null) {
			if (other.idDetalleOrdenCOmpra != null) {
				return false;
			}
		} else if (!idDetalleOrdenCOmpra.equals(other.idDetalleOrdenCOmpra)) {
			return false;
		}
		
		if (nivel == null) {
			if (other.nivel != null) {
				return false;
			}
		} else if (!nivel.equals(other.nivel)) {
			return false;
		}
		return true;
	}


}
