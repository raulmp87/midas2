package mx.com.afirme.midas2.domain.siniestros.liquidacion;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.pagos.OrdenPagoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionProveedor;
import mx.com.afirme.midas2.domain.siniestros.solicitudcheque.SolicitudChequeSiniestro;

/**
 * Entidad que mapea los datos generales de una �rden de compra.
 * @author usuario
 * @version 1.0
 * @created 22-ago-2014 10:48:11 a.m.
 */
@Entity(name = "LiquidacionSiniestro")
@Table(name = "TOLIQUIDACION_SINIESTROS", schema = "MIDAS")
public class LiquidacionSiniestro extends MidasAbstracto  implements Entidad  {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static enum EstatusLiquidacionSiniestro{
		EN_TRAMITE("ENTRAM"),
		POR_AUTORIZAR("XAUT"),
		EN_TRAMITE_POR_RECHAZO("TRMXRECH"),
		LIBERADA_CON_SOLICITUD("LIBCNSOL"),
		/**
	     * @deprecated Se emplea EN_TRAMITE_POR_MIZAR en su lugar
	     */
		@Deprecated
		LIBERADA_SIN_SOLICITUD("LIBSNSOL"),
		APLICADA("APLIC"),
		ELIMINADA("ELIM"),
		CANCELADA("CANC"),
		EN_TRAMITE_POR_MIZAR("TRMXMIZAR");
		
		private static Map<String, EstatusLiquidacionSiniestro> lookup = new HashMap<String, EstatusLiquidacionSiniestro>();
		static {			
			
			for (EstatusLiquidacionSiniestro e : EstatusLiquidacionSiniestro.values()) {
				lookup.put(e.getValue(), e);
			}
		}		
		
		public static EstatusLiquidacionSiniestro get(String value) {
			return lookup.get(value);
		}
		
		private String value;
		
		private EstatusLiquidacionSiniestro(String value){
			this.value=value;
		}
		
		public String getValue(){
			return value;
		}
	}
	
	public static enum TipoOperacion{
		NO_APLICA("NA"),
		OTROS("OTR"),	
		SERVICIOS_PROFESIONALES("SPRO");
		
		private String value;
		
		private TipoOperacion(String value){
			this.value=value;
		}
		
		public String getValue(){
			return value;
		}
	}
	
	public static enum TipoLiquidacion{
		CHEQUE("CHQ"),	
		TRANSFERENCIA_BANCARIA("TRNBANC");
		
		private String value;
		
		private TipoLiquidacion(String value){
			this.value=value;
		}
		
		public String getValue(){
			return value;
		}
	}	
	
	public static enum OrigenLiquidacion{
		PROVEEDOR("PVD"),	
		BENEFICIARIO("BFC");
		
		private String value;
		
		private OrigenLiquidacion(String value){
			this.value=value;
		}
		
		public String getValue(){
			return value;
		}
	}
	
	
	/**
	 * Id de la orden de compra
	 */
	@Id
	@SequenceGenerator(name = "TOLIQUIDACION_SINIESTROS_SEQ_GENERADOR",allocationSize = 1, sequenceName = "TOLIQUIDACION_SINIESTROS_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TOLIQUIDACION_SINIESTROS_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id;
	
	@Column(name="BANCO")
	private String banco;
	
	@Column(name="COMENTARIOS")
	private String comentarios;
	
	@Column(name="CUENTA")
	private String cuenta;
	
	@Column(name="SOLICITADO_POR")
	private String solicitadoPor;
	
	@Column(name="AUTORIZADO_POR")
	private String autorizadoPor;
	
	@Column(name="RECHAZADO_POR")
	private String rechazadoPor;
	
	@Column(name="DEDUCIBLE")
	private BigDecimal deducible;
	
	@Column(name="DESCUENTO")
	private BigDecimal descuento;
	
	@OneToMany(fetch=FetchType.LAZY,  mappedBy = "liquidacionSiniestro", cascade = CascadeType.ALL)
    private List<DetalleLiquidacionSiniestro> detalleLiquidacionSiniestro = new ArrayList<DetalleLiquidacionSiniestro>();
	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "TRLIQUIDACIONFACTURA", schema = "MIDAS",
			joinColumns = {@JoinColumn(name="LIQUIDACION_ID", referencedColumnName="ID")},
			inverseJoinColumns = {@JoinColumn(name="FACTURA_ID", referencedColumnName="ID")}
	)
	private List<DocumentoFiscal> facturas;
	
	@Column(name="ESTATUS")	
	private String estatus;	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_APLICADA")	
	private Date fechaAplicada;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CANCELADA")
	private Date fechaCancelada;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_ELIMINADA")
	private Date fechaEliminada;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_LIBERADA_CON")
	private Date fechaLiberadaCon;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_LIBERADA_SIN")
	private Date fechaLiberadaSin;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_POR_AUT")
	private Date fechaPorAut;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_TRAMITE")
	private Date fechaTramite;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_TRAMITE_REC")
	private Date fechaTramiteRec;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_TRAMITE_MIZAR")
	private Date fechaTramiteMizar;

	@Column(name = "ID_CTO_OPER")
	private Long idCentroOperacion;
	
	/**
	 * id del beneficiario 
	 */
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PROVEEDOR")
	private PrestadorServicio proveedor;
	
	@Column(name = "ID_TCMONEDA")
	private Short idTcMoneda = MonedaDTO.MONEDA_PESOS; //De acuerdo a Analisis y Diseño todas las liquidaciones se realizan en pesos
	
	@Column(name = "IMPORTE_SALVAMENTO")
	private BigDecimal importeSalvamento;
	
	@Column(name = "ISR")
	private BigDecimal isr;
	
	@Column(name = "IVA")
	private BigDecimal iva;
	
	@Column(name = "IVA_RET")
	private BigDecimal ivaRet;
	
	@Column(name = "TOTAL")
	private BigDecimal total;
	
	@Column(name = "NETO_POR_PAGAR")
	private BigDecimal netoPorPagar;	
	
	@Column(name = "NUMERO_LIQUIDACION")
	private Long numeroLiquidacion;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_SOL_CHEQUE", referencedColumnName="ID")
	private SolicitudChequeSiniestro solicitudCheque;
	
	@Column(name = "SUBTOTAL")
	private BigDecimal subtotal;
	
	/**
	 *  Tipos en TCVALORFIJO con GRUPO_ID  =
	 */
	@Column(name="TIPO_LIQUIDACION")
	private String tipoLiquidacion;
	/**
	 *  Tipos en TCVALORFIJO con GRUPO_ID  =
	 */
	@Column(name="TIPO_OPERACION")
	private String tipoOperacion;	
	
	@Column(name="TIPOCANCELACION")
	private Short tipoCancelacion;	
	
	@Transient
	private String descripcionEstatus;
	
	@Transient
	private String descripcionMoneda = "Moneda Nacional"; //De acuerdo a Analisis y Diseño todas las liquidaciones se realizan en pesos
	
	@Column(name="ORIGEN_LIQUIDACION")
	private String origenLiquidacion;
	
	@Column(name="BENEFICIARIO")
	private String beneficiario;
	
	@Column(name="BANCO_BENEFICIARIO")
	private String bancoBeneficiario;
	
	@Column(name="CLABE_BENEFICIARIO")
	private String clabeBeneficiario;
	
	@Column(name="RFC_BENEFICIARIO")
	private String rfcBeneficiario;
	
	@Column(name="TELEFONO_NUMERO")
	private String telefonoNumero;
	
	@Column(name="TELEFONO_LADA")
	private String telefonoLada;
	
	@Column(name="CORREO")
	private String correo;
	
	@Column(name="PRIMA_PENDIENTE_PAGO")
	private BigDecimal primaPendientePago;
	
	@Column(name="PRIMA_NO_DEVENGADA")
	private BigDecimal primaNoDevengada;
	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "TRLIQUIDACIONORDENPAGO", schema = "MIDAS",
			joinColumns = {@JoinColumn(name="LIQUIDACION_ID", referencedColumnName="ID")},
			inverseJoinColumns = {@JoinColumn(name="ORDENPAGO_ID", referencedColumnName="ID")}
	)
	private List<OrdenPagoSiniestro> ordenesPago;
	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "TRSNLIQUIDACIONRECUPERACION", schema = "MIDAS",
			joinColumns = {@JoinColumn(name="LIQUIDACION_ID", referencedColumnName="ID")},
			inverseJoinColumns = {@JoinColumn(name="RECUPERACION_ID", referencedColumnName="ID")}
	)
	private List<RecuperacionProveedor> recuperaciones;
	
	
	@Transient
	private String facturasConcat;
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}	

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getEstatus() {
		return estatus;
	}


	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public SolicitudChequeSiniestro getSolicitudCheque() {
		return solicitudCheque;
	}


	public void setSolicitudCheque(SolicitudChequeSiniestro solicitudCheque) {
		this.solicitudCheque = solicitudCheque;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public BigDecimal getDeducible() {
		return deducible;
	}

	public void setDeducible(BigDecimal deducible) {
		this.deducible = deducible;
	}

	public BigDecimal getDescuento() {
		return descuento;
	}

	public void setDescuento(BigDecimal descuento) {
		this.descuento = descuento;
	}

	public List<DetalleLiquidacionSiniestro> getDetalleLiquidacionSiniestro() {
		return detalleLiquidacionSiniestro;
	}

	public void setDetalleLiquidacionSiniestro(
			List<DetalleLiquidacionSiniestro> detalleLiquidacionSiniestro) {
		this.detalleLiquidacionSiniestro = detalleLiquidacionSiniestro;
	}

	public List<DocumentoFiscal> getFacturas() {
		return facturas;
	}

	public void setFacturas(List<DocumentoFiscal> facturas) {
		this.facturas = facturas;
	}

	public Date getFechaAplicada() {
		return fechaAplicada;
	}

	public void setFechaAplicada(Date fechaAplicada) {
		this.fechaAplicada = fechaAplicada;
	}

	public Date getFechaCancelada() {
		return fechaCancelada;
	}

	public void setFechaCancelada(Date fechaCancelada) {
		this.fechaCancelada = fechaCancelada;
	}

	public Date getFechaEliminada() {
		return fechaEliminada;
	}

	public void setFechaEliminada(Date fechaEliminada) {
		this.fechaEliminada = fechaEliminada;
	}

	public Date getFechaLiberadaCon() {
		return fechaLiberadaCon;
	}

	public void setFechaLiberadaCon(Date fechaLiberadaCon) {
		this.fechaLiberadaCon = fechaLiberadaCon;
	}

	public Date getFechaLiberadaSin() {
		return fechaLiberadaSin;
	}

	public void setFechaLiberadaSin(Date fechaLiberadaSin) {
		this.fechaLiberadaSin = fechaLiberadaSin;
	}

	public Date getFechaPorAut() {
		return fechaPorAut;
	}

	public void setFechaPorAut(Date fechaPorAut) {
		this.fechaPorAut = fechaPorAut;
	}

	public Date getFechaTramite() {
		return fechaTramite;
	}

	public void setFechaTramite(Date fechaTramite) {
		this.fechaTramite = fechaTramite;
	}

	public Date getFechaTramiteRec() {
		return fechaTramiteRec;
	}

	public void setFechaTramiteRec(Date fechaTramiteRec) {
		this.fechaTramiteRec = fechaTramiteRec;
	}

	public Short getIdTcMoneda() {
		return idTcMoneda;
	}

	public void setIdTcMoneda(Short idTcMoneda) {
		this.idTcMoneda = idTcMoneda;
	}

	public BigDecimal getImporteSalvamento() {
		return importeSalvamento;
	}

	public void setImporteSalvamento(BigDecimal importeSalvamento) {
		this.importeSalvamento = importeSalvamento;
	}

	public BigDecimal getIsr() {
		return isr;
	}

	public void setIsr(BigDecimal isr) {
		this.isr = isr;
	}

	public BigDecimal getIva() {
		return iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public BigDecimal getIvaRet() {
		return ivaRet;
	}

	public void setIvaRet(BigDecimal ivaRet) {
		this.ivaRet = ivaRet;
	}

	public BigDecimal getNetoPorPagar() {
		return netoPorPagar;
	}

	public void setNetoPorPagar(BigDecimal netoPorPagar) {
		this.netoPorPagar = netoPorPagar;
	}

	public Long getNumeroLiquidacion() {
		return numeroLiquidacion;
	}

	public void setNumeroLiquidacion(Long numeroLiquidacion) {
		this.numeroLiquidacion = numeroLiquidacion;
	}

	public BigDecimal getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public String getTipoLiquidacion() {
		return tipoLiquidacion;
	}

	public void setTipoLiquidacion(String tipoLiquidacion) {
		this.tipoLiquidacion = tipoLiquidacion;
	}

	public String getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public String getSolicitadoPor() {
		return solicitadoPor;
	}

	public void setSolicitadoPor(String solicitadoPor) {
		this.solicitadoPor = solicitadoPor;
	}

	public String getAutorizadoPor() {
		return autorizadoPor;
	}

	public void setAutorizadoPor(String autorizadoPor) {
		this.autorizadoPor = autorizadoPor;
	}

	public String getRechazadoPor() {
		return rechazadoPor;
	}

	public void setRechazadoPor(String rechazadoPor) {
		this.rechazadoPor = rechazadoPor;
	}
	
	public PrestadorServicio getProveedor() {
		return proveedor;
	}

	public void setProveedor(PrestadorServicio proveedor) {
		this.proveedor = proveedor;
	}

	public String getDescripcionEstatus() {
		return descripcionEstatus;
	}

	public void setDescripcionEstatus(String descripcionEstatus) {
		this.descripcionEstatus = descripcionEstatus;
	}
	
	public String getDescripcionMoneda() {
		return descripcionMoneda;
	}

	public void setDescripcionMoneda(String descripcionMoneda) {
		this.descripcionMoneda = descripcionMoneda;
	}

	@Transient
	public Date getFechaEstatus()
	{
		Date fechaEstatus = null;
		
		if(this.estatus != null && !this.estatus.isEmpty())
		{
			EstatusLiquidacionSiniestro estatusEnum;
			estatusEnum = EstatusLiquidacionSiniestro.get(this.estatus);
			
			switch(estatusEnum)
			{
				case EN_TRAMITE: 
					fechaEstatus = this.getFechaTramite();
					break;
				
				case POR_AUTORIZAR: 
					fechaEstatus = this.getFechaPorAut();
				 	break;
				
				case EN_TRAMITE_POR_RECHAZO: 
					fechaEstatus = this.getFechaTramiteRec();
				 	break;
				
				case LIBERADA_CON_SOLICITUD: 
					fechaEstatus = this.getFechaLiberadaCon();
				 	break;
				
				case LIBERADA_SIN_SOLICITUD: 
					fechaEstatus = this.getFechaLiberadaSin();
				 	break;
				
				case APLICADA: 
					fechaEstatus = this.getFechaAplicada();
				 	break;
				
				case ELIMINADA: 
					fechaEstatus = this.getFechaEliminada();
				 	break;
				
				case CANCELADA: 
					fechaEstatus = this.getFechaCancelada();
				 	break;
				 	
				case EN_TRAMITE_POR_MIZAR:
					fechaEstatus = this.getFechaTramiteMizar();
					break;
			}			
		}
		
		return fechaEstatus;
	}

	public Long getIdCentroOperacion() {
		return idCentroOperacion;
	}

	public void setIdCentroOperacion(Long idCentroOperacion) {
		this.idCentroOperacion = idCentroOperacion;
	}

	public Short getTipoCancelacion() {
		return tipoCancelacion;
	}

	public void setTipoCancelacion(Short tipoCancelacion) {
		this.tipoCancelacion = tipoCancelacion;
	}

	public String getOrigenLiquidacion() {
		return origenLiquidacion;
	}

	public void setOrigenLiquidacion(String origenLiquidacion) {
		this.origenLiquidacion = origenLiquidacion;
	}

	public String getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	public String getBancoBeneficiario() {
		return bancoBeneficiario;
	}

	public void setBancoBeneficiario(String bancoBeneficiario) {
		this.bancoBeneficiario = bancoBeneficiario;
	}

	public String getClabeBeneficiario() {
		return clabeBeneficiario;
	}

	public void setClabeBeneficiario(String clabeBeneficiario) {
		this.clabeBeneficiario = clabeBeneficiario;
	}

	public String getRfcBeneficiario() {
		return rfcBeneficiario;
	}

	public void setRfcBeneficiario(String rfcBeneficiario) {
		this.rfcBeneficiario = rfcBeneficiario;
	}

	public BigDecimal getPrimaPendientePago() {
		return primaPendientePago;
	}

	public void setPrimaPendientePago(BigDecimal primaPendientePago) {
		this.primaPendientePago = primaPendientePago;
	}

	public BigDecimal getPrimaNoDevengada() {
		return primaNoDevengada;
	}

	public void setPrimaNoDevengada(BigDecimal primaNoDevengada) {
		this.primaNoDevengada = primaNoDevengada;
	}

	public List<OrdenPagoSiniestro> getOrdenesPago() {
		return ordenesPago;
	}

	public void setOrdenesPago(List<OrdenPagoSiniestro> ordenesPago) {
		this.ordenesPago = ordenesPago;
	}

	public String getTelefonoNumero() {
		return telefonoNumero;
	}

	public void setTelefonoNumero(String telefonoNumero) {
		this.telefonoNumero = telefonoNumero;
	}

	public String getTelefonoLada() {
		return telefonoLada;
	}

	public void setTelefonoLada(String telefonoLada) {
		this.telefonoLada = telefonoLada;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
	@Override
	public String toString() {
		return "LiquidacionSiniestro [id=" + id + ", banco=" + banco
				+ ", comentarios=" + comentarios + ", cuenta=" + cuenta
				+ ", solicitadoPor=" + solicitadoPor + ", autorizadoPor="
				+ autorizadoPor + ", rechazadoPor=" + rechazadoPor
				+ ", deducible=" + deducible + ", descuento=" + descuento
				+ ", detalleLiquidacionSiniestro="
				+ detalleLiquidacionSiniestro + ", facturas=" + facturas
				+ ", estatus=" + estatus + ", fechaAplicada=" + fechaAplicada
				+ ", fechaCancelada=" + fechaCancelada + ", fechaEliminada="
				+ fechaEliminada + ", fechaLiberadaCon=" + fechaLiberadaCon
				+ ", fechaLiberadaSin=" + fechaLiberadaSin + ", fechaPorAut="
				+ fechaPorAut + ", fechaTramite=" + fechaTramite
				+ ", fechaTramiteRec=" + fechaTramiteRec
				+ ", fechaTramiteMizar=" + fechaTramiteMizar
				+ ", idCentroOperacion=" + idCentroOperacion + ", proveedor="
				+ proveedor + ", idTcMoneda=" + idTcMoneda
				+ ", importeSalvamento=" + importeSalvamento + ", isr=" + isr
				+ ", iva=" + iva + ", ivaRet=" + ivaRet + ", total=" + total
				+ ", netoPorPagar=" + netoPorPagar + ", numeroLiquidacion="
				+ numeroLiquidacion + ", solicitudCheque=" + solicitudCheque
				+ ", subtotal=" + subtotal + ", tipoLiquidacion="
				+ tipoLiquidacion + ", tipoOperacion=" + tipoOperacion
				+ ", tipoCancelacion=" + tipoCancelacion
				+ ", descripcionEstatus=" + descripcionEstatus
				+ ", descripcionMoneda=" + descripcionMoneda
				+ ", origenLiquidacion=" + origenLiquidacion
				+ ", beneficiario=" + beneficiario + ", bancoBeneficiario="
				+ bancoBeneficiario + ", clabeBeneficiario="
				+ clabeBeneficiario + ", rfcBeneficiario=" + rfcBeneficiario
				+ ", telefonoNumero=" + telefonoNumero + ", telefonoLada="
				+ telefonoLada + ", correo=" + correo + ", primaPendientePago="
				+ primaPendientePago + ", primaNoDevengada=" + primaNoDevengada
				+ ", ordenesPago=" + ordenesPago + ", recuperaciones="
				+ recuperaciones + "]";
	}

	public List<RecuperacionProveedor> getRecuperaciones() {
		return recuperaciones;
	}

	public void setRecuperaciones(List<RecuperacionProveedor> recuperaciones) {
		this.recuperaciones = recuperaciones;
	}

	public Date getFechaTramiteMizar() {
		return fechaTramiteMizar;
	}

	public void setFechaTramiteMizar(Date fechaTramiteMizar) {
		this.fechaTramiteMizar = fechaTramiteMizar;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public String getFacturasConcat() {
		return facturasConcat;
	}

	public void setFacturasConcat(String facturasConcat) {
		this.facturasConcat = facturasConcat;
	}	
	
	
	
}