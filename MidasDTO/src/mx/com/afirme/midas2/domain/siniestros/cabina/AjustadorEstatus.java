package mx.com.afirme.midas2.domain.siniestros.cabina;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.Coordenadas;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;

@Entity
@Table(name = "TCAJUSTADORESTATUS", schema = "MIDAS")
@Access(AccessType.PROPERTY)
public class AjustadorEstatus extends MidasAbstracto implements Entidad{

	private static final long serialVersionUID = 4692103233149548039L;
	
	@Access(AccessType.FIELD)
	@Id
	@Column(name="ID",nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TCAJUSTADORESTATUS_GENERATOR")	
	@SequenceGenerator(name="TCAJUSTADORESTATUS_GENERATOR", sequenceName="IDTCAJUSTADORESTATUS_SEQ", schema="MIDAS", allocationSize=1)
	private Long id;
	
	@Access(AccessType.FIELD)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA", nullable=false)
	private Date fecha;
	
	@Access(AccessType.FIELD)
	@Column(name = "ESTATUS")
	private Integer estatus;
	
	@Access(AccessType.FIELD)
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_AJUSTADOR", nullable=false, referencedColumnName="ID")
	private ServicioSiniestro ajustador;
	
	
	private String disponibilidad;
	
	private Coordenadas coordenadas;
	
	public AjustadorEstatus() {
		super();
	}
	
	public AjustadorEstatus(Long id, Date fecha, Integer estatus,
			ServicioSiniestro ajustador, Coordenadas coordenadas) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.estatus = estatus;
		this.ajustador = ajustador;
		this.coordenadas = coordenadas;
	}

	
	public AjustadorEstatus(Long id, Date fecha, Integer estatus,
			ServicioSiniestro ajustador, Double latitud, Double longitud) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.estatus = estatus;
		this.ajustador = ajustador;		
		this.coordenadas = new Coordenadas(latitud, longitud);
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the fecha
	 */
	public Date getFecha() {
		return fecha;
	}
	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	/**
	 * @return the estatus
	 */
	public Integer getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}
	
	/**
	 * @return the coordenadas
	 */
	@Embedded
	public Coordenadas getCoordenadas() {
		return coordenadas;
	}
	/**
	 * @param coordenadas the coordenadas to set
	 */
	public void setCoordenadas(Coordenadas coordenadas) {
		this.coordenadas = coordenadas;
	}
	
	/**
	 * @return the ajustador
	 */
	public ServicioSiniestro getAjustador() {
		return ajustador;
	}
	/**
	 * @param ajustador the ajustador to set
	 */
	public void setAjustador(ServicioSiniestro ajustador) {
		this.ajustador = ajustador;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}
	
	@Override
	public String getValue() {
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	
	@Transient
	public String getDisponibilidad() {
		return disponibilidad;
	}
	public void setDisponibilidad(String disponibilidad) {
		this.disponibilidad = disponibilidad;
	}
	
}
