package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.EnumBase;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.depuracion.ConfiguracionDepuracionReserva;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;


@Entity
@Table(name = "TOMOVIMIENTOCOBERTURASINIESTRO", schema = "MIDAS")
public class MovimientoCoberturaSiniestro extends MidasAbstracto {

	
	/**
	 * 
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDMOVCOBSINIESTRO_SEQ")
	@SequenceGenerator(name = "IDMOVCOBSINIESTRO_SEQ",  schema="MIDAS", sequenceName = "IDMOVCOBSINIESTRO_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "CONTABILIZADO")
	private Boolean contabilizado;
	
	@Column(name = "CARGA_ABONO_ID")
	private Long cargaAbono;
	
	@Column(name = "LOTE_CONTABLE")
	private Long loteContable;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CONTABILIZACION")
	private Date fechaContabilizacion;
	
	@Column(name = "IMPORTE_MOVIMIENTO")
	private BigDecimal importeMovimiento;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch (JoinFetchType.OUTER)
	@JoinColumn(name="ESTIMACIONCOBREPCABINA_ID", referencedColumnName="ID")
	private EstimacionCoberturaReporteCabina estimacionCobertura;
	
	@Column(name = "TIPO_DOCUMENTO")
	private String tipoDocumento;
	
	@Column(name = "TIPO_MOVIMIENTO")
	private String tipoMovimiento;
	
	@Column(name = "CAUSA_MOVIMIENTO")
	private String causaMovimiento;
	
	@Column(name = "DESCRIPCION")
	private String descripcion;
	
	@Column(name = "ID_REF")
	private Long idReferencia;
	
	@ManyToOne
	@JoinColumn(name = "CONFIG_DEPURACION_ID", referencedColumnName = "id")
	private ConfiguracionDepuracionReserva configuracionDepuracion;
	
	

	public MovimientoCoberturaSiniestro(){
	}	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getContabilizado() {
		return contabilizado;
	}

	public void setContabilizado(Boolean contabilizado) {
		this.contabilizado = contabilizado;
	}

	public Date getFechaContabilizacion() {
		return fechaContabilizacion;
	}

	public void setFechaContabilizacion(Date fechaContabilizacion) {
		this.fechaContabilizacion = fechaContabilizacion;
	}

		public BigDecimal getImporteMovimiento() {
		return importeMovimiento;
	}

	public void setImporteMovimiento(BigDecimal importeMovimiento) {
		this.importeMovimiento = importeMovimiento;
	}

	public EstimacionCoberturaReporteCabina getEstimacionCobertura() {
		return estimacionCobertura;
	}

	public void setEstimacionCobertura(
			EstimacionCoberturaReporteCabina estimacionCobertura) {
		this.estimacionCobertura = estimacionCobertura;
	}
	
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getTipoMovimiento() {
		return tipoMovimiento;
	}

	public void setTipoMovimiento(String tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

	public String getCausaMovimiento() {
		return causaMovimiento;
	}

	public void setCausaMovimiento(String causaMovimiento) {
		this.causaMovimiento = causaMovimiento;
	}

	public Long getCargaAbono() {
		return cargaAbono;
	}

	public void setCargaAbono(Long cargaAbono) {
		this.cargaAbono = cargaAbono;
	}

	public Long getLoteContable() {
		return loteContable;
	}


	public void setLoteContable(Long loteContable) {
		this.loteContable = loteContable;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	


	public Long getIdReferencia() {
		return idReferencia;
	}


	public void setIdReferencia(Long idReferencia) {
		this.idReferencia = idReferencia;
	}


	public void finalize() throws Throwable {
		super.finalize();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <K> K getKey() {
		return (K) this.id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	public ConfiguracionDepuracionReserva getConfiguracionDepuracion() {
		return configuracionDepuracion;
	}
	
	public void setConfiguracionDepuracion(
			ConfiguracionDepuracionReserva configuracionDepuracion) {
		this.configuracionDepuracion = configuracionDepuracion;
	}
	

	
	public enum TipoDocumentoMovimiento implements EnumBase<String>{
		ESTIMACION ("EST", "Estimacion"),
	    PAGO ("PAGO", "Pago"),
	    INGRESO ("INGR", "Ingreso");

	    private final String tipoDocumento;
	    private final String label;    

	    private TipoDocumentoMovimiento(String tipoDocumento, String label) {
	        this.tipoDocumento = tipoDocumento;
	        this.label = label;
	    }


	    @Override
	    public String toString(){
	       return tipoDocumento;
	    }

		@Override
		public String getValue() {
			return tipoDocumento;
		}

		@Override
		public String getLabel() {
			return label;
		}

	}
	
	public enum TipoMovimiento implements EnumBase<String>{
		ESTIMACION_ORIGINAL ("EO"),
		AJUSTE_AUMENTO ("AA"),
		AJUSTE_DISMINUCION ("AD"),
		PAGO_INDEMNIZACION ("PIN"),
		CANCELACION_PAGO_INDEMNIZACION ("CPI"),
		PAGO_GASTOAJUSTE ("PGA"),
		CANCELACION_PAGO_GASTOAJUSTE ("CPGA"),
		PAGO_REEMBOLSOGASTOAJUSTE ("PRGA"),
		CANCELACION_PAGO_REEMBOLSOGASTOAJUSTE ("CPRGA"),
		INGRESO_DEDUCIBLES ("ID"),
		CANCELACION_INGRESO_DEDUCIBLES ("CID"),
		INGRESO_SALVAMENTOS ("IS"),
		CANCELACION_INGRESO_SALVAMENTOS ("CIS"),
		INGRESO_PROVEEDOR ("IRP"),
		CANCELACION_INGRESO_PROVEEDOR ("CRP"),
		INGRESO_COMPANIAS ("IRC"),
		CANCELACION_INGRESO_COMPANIAS ("CICO"),
		INGRESO_CRUCERO_JURIDICO ("IRJ"),
		CANCELACION_INGRESO_CRUCERO_JURIDICO ("CIRJ"),
		INGRESO_SIPAC ("ISP"),
		CANCELACION_INGRESO_SIPAC ("CISP")
		;

	    private final String tipoMovimiento;       

	    private TipoMovimiento(String tipoMovimiento) {
	        this.tipoMovimiento = tipoMovimiento;
	    }

	    @Override
	    public String toString(){
	       return tipoMovimiento;
	    }

		@Override
		public String getValue() {
			return tipoMovimiento;
		}

		@Override
		public String getLabel() {
			return null;
		}

	}
	
	public enum CausaMovimiento implements EnumBase<String>{
		AJUSTE_PRESUPUESTO_SERVICIO ("APDS"), //AJUSTE AL PRESUPUESTO DE SERVICIO
		AJUSTE_ACUERDO_VALUACION ("ADAV"), //AJUSTE DE ACUERDO A LA VALUACION
		AJUSTE_ACUERDO_VALUACION_XFOTO ("AAVF"), //AJUSTE DE ACUERDO A VALUACION X FOTOS
		AJUSTE_ACUERDO_GASTO_MED ("AAGM"), //AJUSTE DE ACUERDO AL GASTO MEDICO
		AJUSTE_PT_PAGO_DANIOS ("APPD"), //AJUSTE DE PT A PAGO DE DAÑOS
		AJUSTE_PT_REPARACION ("AJDR"), //AJUSTE DE PT A REPARACION
		AJUSTE_JURIDICO ("AJJU"), //AJUSTE JURIDICO
		AJUSTE_MODIFICA_VALOR_COMERCIAL ("AMVC"), //AJUSTE PARA MODIFICAR EL VALOR COMERCIAL
		AJUSTE_PAGO_CIA ("APPC"), //AJUSTE POR PAGO A COMPANÍA
		AJUSTE_PAGO_CRISTAL ("AJPC"), //AJUSTE POR PAGO DE CRISTAL
		AJUSTE_SOLICITUD_MULTI ("AJSM"), //AJUSTE SOLICITADO POR MULTI
		AJUSTE_CONCURRENCIA_SEGUROS ("AJCS"), //AJUSTE POR CONCURRENCIA DE SEGUROS
		AJUSTE_LOCALIZACION_VEH_ROBADO ("ALVR"), //AJUSTE POR LOCALIZACION VEH. ROBADO
		AJUSTE_SEGUNDA_PT ("ATSP"), //AJUSTE POR TRATARSE DE SEGUNDA P.T.
		APERTURA_RESERVA ("APRE"), //APERTURA DE RESERVA
		ARREGLO_ASEG_TERCERO ("ARRE"), //ARREGLO ASEG/TERC O TERC/ASEG
		CANCELACION_CONDUSEF ("CACO"), //CANCELACION CONDUSEF
		CONSTITUCION_CONDUSEF ("COCO"), //CONSTITUCION CONDUSEF
		DANIO_NO_RECL_ASEGURADO ("DRPA"), //DAÑOS NO RECLAMADOS POR ASEGURADO
		DANIO_NO_RECL_TERCERO ("DRPT"), //DAÑOS NO RECLAMADOS POR TERCERO
		DEPURACION_POR_CRITERIO ("DEPC"), //DEPURACIÓN POR CRITERIOS
		DEPURACION_POR_RECHAZO ("DERE"), //DEPURACION POR RECHAZO
		DEPURACION_DANIO_MENOR_DEDUCIBLE ("DDMD"), //DEPURACION POR DAÑO MENOR AL DEDUCIBLE
		DEPURACION_ERROR_COBERTURA ("DERC"), //DEPURACION POR ERROR EN COBERTURA
		DEPURACION_NO_PRESENT_FACTURA_COBRO ("DPFC"), //DEPURACION POR NO PRESENT.FACTURA COBRO
		DEPURACION_POR_PREESCRIPCION ("DEPR"), //DEPURACION POR PREESCRIPCION
		DEPURACION_SINIESTRO_DUPLICADO ("DPSD"), //DEPURACION POR SINIESTRO DUPLICADO
		DEVOLUCION_DEDUCIBLE ("DEDE"),  //DEVOLUCIÓN DE DEDUCIBLE
		DIFERENCIA_EDAD ("DIFE"), //DIFERENCIA EN EDAD
		ESTIMACION_INDEBIDA ("ESIN"),  //ESTIMACION INDEBIDA
		ESTIMACION_INSUFICIENTE ("ESIS"), //ESTIMACION INSUFICIENTE
		FALTA_DOC_PERDIDA_TOTAL ("FDPT"),  //FALTA DOCUMENTACIÓN (PÉRDIDA TOTAL)
		FALTA_DOC_ROBO ("FDRO"), //FALTA DOCUMENTACIÓN (ROBO)
		POR_DEPURACION ("PODE"), //POR DEPURACION
		POR_DIFERENCIA_DEDUCIBLE ("DIDE"),  //POR DIFERENCIA EN DEDUCIBLE
		POR_RECUPERACIONES ("PORE"), //POR RECUPERACIONES
		REESTIMACION_ACUERDO_VALOR_COMERCIAL ("RAVC"), //REESTIMACION DE ACUERDO AL VALOR COMERCIAL
		REESTIMACION_CANCELACION_INDEBIDA ("RPCI"), //REESTIMACION POR CANCELACIÓN INDEBIDA
		UNIDAD_NOPRESENTADA_REPARA_ASEG ("UPRA"), //UNIDAD NO PRESENTADA A REPARACIÓN (ASEGURADO)
		UNIDAD_NOPRESENTADA_REPARA_TERC ("UPRT"), //UNIDAD NO PRESENTADA A REPARACIÓN (TERCERO)
		POR_COMPLEMENTO_VALUACION ("POEV"), //POR COMPLEMENTO EN LA VALUACION
		POR_DETERMINACION_PERDIDA_TOTAL ("PDPT"), //POR DETERMINACION DE PERDIDA TOTAL
		GASTO_AJUSTE ("GASTO"), //GASTO DE AJUSTE
		REEMBOLSO_GASTO_AJUSTE ("RGASTO"), //REEMBOLSO DE GASTO DE AJUSTE
	    DEPURACION_MANUAL_RESERVA ("DMR"), //DEPURACION MANUAL DE RESERVA
		HGS("HGS"); //AJUSTE POR HGS

	    private final String causaMovimiento;       

	    private CausaMovimiento(String causaMovimiento) {
	        this.causaMovimiento = causaMovimiento;
	    }

	    @Override
	    public String toString(){
	       return causaMovimiento;
	    }
	    

		@Override
		public String getValue() {
			return causaMovimiento;
		}

		@Override
		public String getLabel() {
			return null;
		}

	}


}