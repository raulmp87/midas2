package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

public class EntidadKeyValidator implements ConstraintValidator<KeyNotEmpty, Entidad> {

	  @Override
	  public void initialize(KeyNotEmpty constraintAnnotation) {
	  }

	  @Override
	  public boolean isValid(Entidad value, ConstraintValidatorContext context) {
		  if(value != null && value.getKey() != null  && (
				  (value.getKey() instanceof String && !((String)value.getKey()).isEmpty()) ||
				  (value.getKey() instanceof Long && ((Long)value.getKey()) > 0)
				  )){
			  return true;
		  }
		  context.disableDefaultConstraintViolation();
		  context.buildConstraintViolationWithTemplate( "Este campo es requerido")
            .addNode( "id" ).addConstraintViolation();
		  //En caso de que el id de la entidad se llame diferente, agregar if instanceof y cambiar el node
		  return false;
	  }
}
