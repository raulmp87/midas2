package mx.com.afirme.midas2.domain.siniestros.indemnizacion;

import java.util.Date;

import org.springframework.stereotype.Component;

import mx.com.afirme.midas2.annotation.Exportable;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService.CatalogoFiltro;

@Component
public class PerdidaTotalFiltro extends CatalogoFiltro{

	private Long idOrdenCompra;
	private Long idIndemnizacion;
	private Long idOficina;
	private Long idReporteCabina;
	private Long idSiniestro;
	private String tipoBajaPlacas;
	private String oficinaNombre;
	private Date fechaSiniestro;
	private String tipoSiniestro;
	private String tipoSiniestroDesc;
	private String numeroSiniestro;
	private String siniestroClaveOficina;
	private String siniestroConsecutivoReporte;
	private String siniestroAnioReporte;
	private String numeroReporte;
	private String reporteClaveOficina;
	private String reporteConsecutivoReporte;
	private String reporteAnioReporte;
	private String numeroPoliza;
	private String numeroSerie;
	private Long numeroValuacion;
	private String etapa;
	private String etapaDesc;
	private String estatusIndemnizacion;
	private String estatusIndemnizacionDesc;
	private String estatusPerdidaTotal;
	private String estatusPerdidaTotalDesc;
	private String existeOrdenCompraFinal;
	private String existeRecepcionDocumentos;
	private String cobertura;
	private String numPaseAtencion;
	private String formaPago;
	
	public PerdidaTotalFiltro() {
	}
	
	public PerdidaTotalFiltro(Long idOrdenCompra, Long idIndemnizacion,
			Long idOficina, Long idReporteCabina, Long idSiniestro,
			String tipoBajaPlacas, String oficinaNombre,
			Date fechaSiniestro, String tipoSiniestro,
			String tipoSiniestroDesc, String numeroSiniestro,
			String siniestroClaveOficina,
			String siniestroConsecutivoReporte,
			String siniestroAnioReporte, String numeroReporte,
			String reporteClaveOficina, String reporteConsecutivoReporte,
			String reporteAnioReporte, String numeroPoliza,
			String numeroSerie, Long numeroValuacion, String etapa,
			String etapaDesc, String estatusIndemnizacion,
			String estatusIndemnizacionDesc, String estatusPerdidaTotal,
			String estatusPerdidaTotalDesc, String existeOrdenCompraFinal,
			String existeRecepcionDocumentos, String cobertura,
			String numPaseAtencion, String formaPago) {
		this.idOrdenCompra = idOrdenCompra;
		this.idIndemnizacion = idIndemnizacion;
		this.idOficina = idOficina;
		this.idReporteCabina = idReporteCabina;
		this.idSiniestro = idSiniestro;
		this.tipoBajaPlacas = tipoBajaPlacas;
		this.oficinaNombre = oficinaNombre;
		this.fechaSiniestro = fechaSiniestro;
		this.tipoSiniestro = tipoSiniestro;
		this.tipoSiniestroDesc = tipoSiniestroDesc;
		this.numeroSiniestro = numeroSiniestro;
		this.siniestroClaveOficina = siniestroClaveOficina;
		this.siniestroConsecutivoReporte = siniestroConsecutivoReporte;
		this.siniestroAnioReporte = siniestroAnioReporte;
		this.numeroReporte = numeroReporte;
		this.reporteClaveOficina = reporteClaveOficina;
		this.reporteConsecutivoReporte = reporteConsecutivoReporte;
		this.reporteAnioReporte = reporteAnioReporte;
		this.numeroPoliza = numeroPoliza;
		this.numeroSerie = numeroSerie;
		this.numeroValuacion = numeroValuacion;
		this.etapa = etapa;
		this.etapaDesc = etapaDesc;
		this.estatusIndemnizacion = estatusIndemnizacion;
		this.estatusIndemnizacionDesc = estatusIndemnizacionDesc;
		this.estatusPerdidaTotal = estatusPerdidaTotal;
		this.estatusPerdidaTotalDesc = estatusPerdidaTotalDesc;
		this.existeOrdenCompraFinal = existeOrdenCompraFinal;
		this.existeRecepcionDocumentos = existeRecepcionDocumentos;
		this.cobertura = cobertura;
		this.numPaseAtencion = numPaseAtencion;
		this.formaPago = formaPago;
	}
	
	
	public PerdidaTotalFiltro(Long idOrdenCompra, Long idIndemnizacion,
			Long idSiniestro, String tipoBajaPlacas, String oficinaNombre,
			Date fechaSiniestro, String tipoSiniestro,
			String tipoSiniestroDesc, String numeroSiniestro,
			String numeroReporte, String numeroPoliza, String numeroSerie,
			Long numeroValuacion, String etapa, String etapaDesc,
			String estatusIndemnizacion, String estatusIndemnizacionDesc,
			String estatusPerdidaTotal, String estatusPerdidaTotalDesc,
			String existeOrdenCompraFinal,
			String existeRecepcionDocumentos, String cobertura,
			String numPaseAtencion) {
		this.idOrdenCompra = idOrdenCompra;
		this.idIndemnizacion = idIndemnizacion;
		this.idSiniestro = idSiniestro;
		this.tipoBajaPlacas = tipoBajaPlacas;
		this.oficinaNombre = oficinaNombre;
		this.fechaSiniestro = fechaSiniestro;
		this.tipoSiniestro = tipoSiniestro;
		this.tipoSiniestroDesc = tipoSiniestroDesc;
		this.numeroSiniestro = numeroSiniestro;
		this.numeroReporte = numeroReporte;
		this.numeroPoliza = numeroPoliza;
		this.numeroSerie = numeroSerie;
		this.numeroValuacion = numeroValuacion;
		this.etapa = etapa;
		this.etapaDesc = etapaDesc;
		this.estatusIndemnizacion = estatusIndemnizacion;
		this.estatusIndemnizacionDesc = estatusIndemnizacionDesc;
		this.estatusPerdidaTotal = estatusPerdidaTotal;
		this.estatusPerdidaTotalDesc = estatusPerdidaTotalDesc;
		this.existeOrdenCompraFinal = existeOrdenCompraFinal;
		this.existeRecepcionDocumentos = existeRecepcionDocumentos;
		this.cobertura = cobertura;
		this.numPaseAtencion = numPaseAtencion;
	}

	/**
	 * @return the idOficina
	 */
	public Long getIdOficina() {
		return idOficina;
	}
	/**
	 * @param idOficina the idOficina to set
	 */
	public void setIdOficina(Long idOficina) {
		this.idOficina = idOficina;
	}
	/**
	 * @return the fechaSiniestro
	 */
	@Exportable(columnName="FECHA DEL SINIESTRO", columnOrder=9)
	public Date getFechaSiniestro() {
		return fechaSiniestro;
	}
	/**
	 * @param fechaSiniestro the fechaSiniestro to set
	 */
	public void setFechaSiniestro(Date fechaSiniestro) {
		this.fechaSiniestro = fechaSiniestro;
	}
	/**
	 * @return the tipoSiniestro
	 */
	public String getTipoSiniestro() {
		return tipoSiniestro;
	}
	/**
	 * @param tipoSiniestro the tipoSiniestro to set
	 */
	public void setTipoSiniestro(String tipoSiniestro) {
		this.tipoSiniestro = tipoSiniestro;
	}
	/**
	 * @return the numeroSiniestro
	 */
	@Exportable(columnName="NUMERO DE SINIESTRO", columnOrder=3)
	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}
	/**
	 * @param numeroSiniestro the numeroSiniestro to set
	 */
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}
	/**
	 * @return the numeroReporte
	 */
	@Exportable(columnName="NUMERO DE REPORTE", columnOrder=4)
	public String getNumeroReporte() {
		return numeroReporte;
	}
	/**
	 * @param numeroReporte the numeroReporte to set
	 */
	public void setNumeroReporte(String numeroReporte) {
		this.numeroReporte = numeroReporte;
	}
	/**
	 * @return the numeroPoliza
	 */
	@Exportable(columnName="NUMERO DE POLIZA", columnOrder=5)
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	/**
	 * @param numeroPoliza the numeroPoliza to set
	 */
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	/**
	 * @return the numeroSerie
	 */
	@Exportable(columnName="NUMERO DE SERIE", columnOrder=6)
	public String getNumeroSerie() {
		return numeroSerie;
	}
	/**
	 * @param numeroSerie the numeroSerie to set
	 */
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	/**
	 * @return the numeroValuacion
	 */
	@Exportable(columnName="NUMERO DE VALUACION", columnOrder=7)
	public Long getNumeroValuacion() {
		return numeroValuacion;
	}
	/**
	 * @param numeroValuacion the numeroValuacion to set
	 */
	public void setNumeroValuacion(Long numeroValuacion) {
		this.numeroValuacion = numeroValuacion;
	}
	/**
	 * @return the etapa
	 */
	public String getEtapa() {
		return etapa;
	}
	/**
	 * @param etapa the etapa to set
	 */
	public void setEtapa(String etapa) {
		this.etapa = etapa;
	}
	/**
	 * @return the oficinaNombre
	 */
	@Exportable(columnName="NOMBRE OFICINA", columnOrder=0)
	public String getOficinaNombre() {
		return oficinaNombre;
	}
	/**
	 * @param oficinaNombre the oficinaNombre to set
	 */
	public void setOficinaNombre(String oficinaNombre) {
		this.oficinaNombre = oficinaNombre;
	}
	/**
	 * @return the tipoSiniestroDesc
	 */
	@Exportable(columnName="TIPO DE INDEMNIZACION", columnOrder=2)
	public String getTipoSiniestroDesc() {
		return tipoSiniestroDesc;
	}
	/**
	 * @param tipoSiniestroDesc the tipoSiniestroDesc to set
	 */
	public void setTipoSiniestroDesc(String tipoSiniestroDesc) {
		this.tipoSiniestroDesc = tipoSiniestroDesc;
	}
	/**
	 * @return the etapaDesc
	 */
	@Exportable(columnName="ETAPA DE LA INDEMNIZACION", columnOrder=10)
	public String getEtapaDesc() {
		return etapaDesc;
	}
	/**
	 * @param etapaDesc the etapaDesc to set
	 */
	public void setEtapaDesc(String etapaDesc) {
		this.etapaDesc = etapaDesc;
	}
	/**
	 * @return the siniestroClaveOficina
	 */
	public String getSiniestroClaveOficina() {
		return siniestroClaveOficina;
	}
	/**
	 * @param siniestroClaveOficina the siniestroClaveOficina to set
	 */
	public void setSiniestroClaveOficina(String siniestroClaveOficina) {
		this.siniestroClaveOficina = siniestroClaveOficina;
	}
	/**
	 * @return the siniestroConsecutivoReporte
	 */
	public String getSiniestroConsecutivoReporte() {
		return siniestroConsecutivoReporte;
	}
	/**
	 * @param siniestroConsecutivoReporte the siniestroConsecutivoReporte to set
	 */
	public void setSiniestroConsecutivoReporte(String siniestroConsecutivoReporte) {
		this.siniestroConsecutivoReporte = siniestroConsecutivoReporte;
	}
	/**
	 * @return the siniestroAnioReporte
	 */
	public String getSiniestroAnioReporte() {
		return siniestroAnioReporte;
	}
	/**
	 * @param siniestroAnioReporte the siniestroAnioReporte to set
	 */
	public void setSiniestroAnioReporte(String siniestroAnioReporte) {
		this.siniestroAnioReporte = siniestroAnioReporte;
	}
	/**
	 * @return the reporteClaveOficina
	 */
	public String getReporteClaveOficina() {
		return reporteClaveOficina;
	}
	/**
	 * @param reporteClaveOficina the reporteClaveOficina to set
	 */
	public void setReporteClaveOficina(String reporteClaveOficina) {
		this.reporteClaveOficina = reporteClaveOficina;
	}
	/**
	 * @return the reporteConsecutivoReporte
	 */
	public String getReporteConsecutivoReporte() {
		return reporteConsecutivoReporte;
	}
	/**
	 * @param reporteConsecutivoReporte the reporteConsecutivoReporte to set
	 */
	public void setReporteConsecutivoReporte(String reporteConsecutivoReporte) {
		this.reporteConsecutivoReporte = reporteConsecutivoReporte;
	}
	/**
	 * @return the reporteAnioReporte
	 */
	public String getReporteAnioReporte() {
		return reporteAnioReporte;
	}
	/**
	 * @param reporteAnioReporte the reporteAnioReporte to set
	 */
	public void setReporteAnioReporte(String reporteAnioReporte) {
		this.reporteAnioReporte = reporteAnioReporte;
	}
	/**
	 * @return the idOrdenCompra
	 */
	public Long getIdOrdenCompra() {
		return idOrdenCompra;
	}
	/**
	 * @param idOrdenCompra the idOrdenCompra to set
	 */
	public void setIdOrdenCompra(Long idOrdenCompra) {
		this.idOrdenCompra = idOrdenCompra;
	}
	/**
	 * @return the idIndemnizacion
	 */
	public Long getIdIndemnizacion() {
		return idIndemnizacion;
	}
	/**
	 * @param idIndemnizacion the idIndemnizacion to set
	 */
	public void setIdIndemnizacion(Long idIndemnizacion) {
		this.idIndemnizacion = idIndemnizacion;
	}
	/**
	 * @return the estatusIndemnizacion
	 */
	public String getEstatusIndemnizacion() {
		return estatusIndemnizacion;
	}
	/**
	 * @param estatusIndemnizacion the estatusIndemnizacion to set
	 */
	public void setEstatusIndemnizacion(String estatusIndemnizacion) {
		this.estatusIndemnizacion = estatusIndemnizacion;
	}
	/**
	 * @return the estatusPerdidaTotal
	 */
	public String getEstatusPerdidaTotal() {
		return estatusPerdidaTotal;
	}
	/**
	 * @param estatusPerdidaTotal the estatusPerdidaTotal to set
	 */
	public void setEstatusPerdidaTotal(String estatusPerdidaTotal) {
		this.estatusPerdidaTotal = estatusPerdidaTotal;
	}
	/**
	 * @return the idReporteCabina
	 */
	public Long getIdReporteCabina() {
		return idReporteCabina;
	}
	/**
	 * @param idReporteCabina the idReporteCabina to set
	 */
	public void setIdReporteCabina(Long idReporteCabina) {
		this.idReporteCabina = idReporteCabina;
	}
	/**
	 * @return the idSiniestro
	 */
	public Long getIdSiniestro() {
		return idSiniestro;
	}
	/**
	 * @param idSiniestro the idSiniestro to set
	 */
	public void setIdSiniestro(Long idSiniestro) {
		this.idSiniestro = idSiniestro;
	}
	/**
	 * @return the tipoBajaPlacas
	 */
	public String getTipoBajaPlacas() {
		return tipoBajaPlacas;
	}
	/**
	 * @param tipoBajaPlacas the tipoBajaPlacas to set
	 */
	public void setTipoBajaPlacas(String tipoBajaPlacas) {
		this.tipoBajaPlacas = tipoBajaPlacas;
	}
	
	/**
	 * @return the estatusIndemnizacionDesc
	 */
	@Exportable(columnName="ESTATUS DE INDEMNIZACION", columnOrder=12)
	public String getEstatusIndemnizacionDesc() {
		return estatusIndemnizacionDesc;
	}
	/**
	 * @param estatusIndemnizacionDesc the estatusIndemnizacionDesc to set
	 */
	public void setEstatusIndemnizacionDesc(String estatusIndemnizacionDesc) {
		this.estatusIndemnizacionDesc = estatusIndemnizacionDesc;
	}
	/**
	 * @return the estatusPerdidaTotalDesc
	 */
	@Exportable(columnName="ESTATUS DE LA PERDIDA TOTAL", columnOrder=11)
	public String getEstatusPerdidaTotalDesc() {
		return estatusPerdidaTotalDesc;
	}
	/**
	 * @param estatusPerdidaTotalDesc the estatusPerdidaTotalDesc to set
	 */
	public void setEstatusPerdidaTotalDesc(String estatusPerdidaTotalDesc) {
		this.estatusPerdidaTotalDesc = estatusPerdidaTotalDesc;
	}
	/**
	 * @return the cobertura
	 */
	@Exportable(columnName="COBERTURA", columnOrder=1)
	public String getCobertura() {
		return cobertura;
	}
	/**
	 * @param cobertura the cobertura to set
	 */
	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}
	/**
	 * @return the numPaseAtencion
	 */
	@Exportable(columnName="NUMERO DE PASE DE ATENCION", columnOrder=8)
	public String getNumPaseAtencion() {
		return numPaseAtencion;
	}
	/**
	 * @param numPaseAtencion the numPaseAtencion to set
	 */
	public void setNumPaseAtencion(String numPaseAtencion) {
		this.numPaseAtencion = numPaseAtencion;
	}
	/**
	 * @return the formaPago
	 */
	public String getFormaPago() {
		return formaPago;
	}
	/**
	 * @param formaPago the formaPago to set
	 */
	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}

	/**
	 * @return the existeOrdenCompraFinal
	 */
	public String getExisteOrdenCompraFinal() {
		return existeOrdenCompraFinal;
	}

	/**
	 * @param existeOrdenCompraFinal the existeOrdenCompraFinal to set
	 */
	public void setExisteOrdenCompraFinal(String existeOrdenCompraFinal) {
		this.existeOrdenCompraFinal = existeOrdenCompraFinal;
	}

	/**
	 * @return the existeRecepcionDocumentos
	 */
	public String getExisteRecepcionDocumentos() {
		return existeRecepcionDocumentos;
	}

	/**
	 * @param existeRecepcionDocumentos the existeRecepcionDocumentos to set
	 */
	public void setExisteRecepcionDocumentos(String existeRecepcionDocumentos) {
		this.existeRecepcionDocumentos = existeRecepcionDocumentos;
	}

}
