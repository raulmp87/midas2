package mx.com.afirme.midas2.domain.siniestros.catalogo.oficina;

import java.util.Date;
import java.util.Set;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.annotation.Exportable;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.Coordenadas;

@Entity
@Table(name = "TCOFICINA", schema = "MIDAS")
@Access(AccessType.PROPERTY)
public class Oficina implements Entidad {

	private static final long serialVersionUID = 628318271104757957L;

	@Access(AccessType.FIELD)
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TCOFICINA_SEQ")
	@SequenceGenerator(name = "TCOFICINA_SEQ", schema = "MIDAS", sequenceName = "TCOFICINA_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Long id;

	@Access(AccessType.FIELD)
	@Column(name = "CLAVE_OFICINA")
	private String claveOficina;

	@Access(AccessType.FIELD)
	@Column(name = "CODIGO_USUARIO_CREACION")
	private String codigoUsuarioCreacion;

	@Access(AccessType.FIELD)
	@Column(name = "CODIGO_USUARIO_MODIFICACION")
	private String codigoUsuarioModificacion;

	@Access(AccessType.FIELD)
	@Column(name = "ESTATUS")
	private Integer estatus;

	@Access(AccessType.FIELD)
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CAMBIO_ESTATUS")
	private Date fechaCambioEstatus;

	@Access(AccessType.FIELD)
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION")
	private Date fechaCreacion;

	@Access(AccessType.FIELD)
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_MODIFICACION")
	private Date fechaModificacion;

	@Access(AccessType.FIELD)
	@Column(name = "NOMBRE_OFICINA")
	private String nombreOficina;

	@Access(AccessType.FIELD)
	@Column(name = "TEL_OFICINA")
	private String telOficina;

	@Access(AccessType.FIELD)
	@Column(name = "TEL_OFICINA_LADA")
	private String telOficinaLada;

	@Access(AccessType.FIELD)
	@Column(name = "TEL_OFICINA_LADA2")
	private String telOficinaLada2;

	@Access(AccessType.FIELD)
	@Column(name = "TEL_OFICINA2")
	private String telOficina2;
	
	private String oficinaNombre;

	@Access(AccessType.FIELD)
	@Column(name = "TIPOSERVICIO")
	private String tipoServicio;
	
	@Access(AccessType.FIELD)
	@Column(name = "RESPONSABLE")
	private String responsable;

	// bi-directional many-to-one association to OficinaEstado
	@Access(AccessType.FIELD)
	@OneToMany(mappedBy = "oficina", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<OficinaEstado> oficinaEstados;

	@Column(name = "CORREO_OFICINA_1")
	@Access(AccessType.FIELD)
	private String correo1;

	@Column(name = "CORREO_OFICINA_2")
	@Access(AccessType.FIELD)
	private String correo2;
	
	@Column(name = "DERIVA_HGS", nullable = false)
	@Access(AccessType.FIELD)
	private Boolean derivaHGS = Boolean.TRUE;
	
	@Column(name = "CONSECUTIVOREFERENCIA")
	@Access(AccessType.FIELD)
	private Long consecutivoReferencia;

	@Column(name = "INVOCA_WEB_SERVICE")
	@Access(AccessType.FIELD)
	private Boolean isWebService = Boolean.FALSE;
	
	private String endpoint;
	
	
	private Coordenadas coordenadas = new Coordenadas();

	public Oficina() {
		super();
	}

	@Exportable(columnName = "NUMERO OFICINA", columnOrder = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Exportable(columnName = "CLAVE OFICINA", columnOrder = 1)
	public String getClaveOficina() {
		return this.claveOficina;
	}

	public void setClaveOficina(String claveOficina) {
		this.claveOficina = claveOficina;
	}

	public String getCodigoUsuarioCreacion() {
		return this.codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	public String getCodigoUsuarioModificacion() {
		return this.codigoUsuarioModificacion;
	}

	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}
	
	/*@Transient
	@Exportable(columnName = "ESTATUS", columnOrder = 3)
	public String getEstatusStr(){
		return (estatus != null && estatus.intValue() == 1)?"Activo":"Inactivo";
	}*/

	@Exportable(columnName = "ESTATUS", columnOrder = 3, fixedValues="0=Inactivo,1=Activo")
	public Integer getEstatus() {
		return this.estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public Date getFechaCambioEstatus() {
		return this.fechaCambioEstatus;
	}

	public void setFechaCambioEstatus(Date fechaCambioEstatus) {
		this.fechaCambioEstatus = fechaCambioEstatus;
	}

	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Exportable(columnName = "NOMBRE", columnOrder = 2)
	public String getNombreOficina() {
		return this.nombreOficina;
	}

	public void setNombreOficina(String nombreOficina) {
		this.nombreOficina = nombreOficina;
	}

	@Exportable(columnName = "TELEFONO 1", columnOrder = 5)
	public String getTelOficina() {
		return this.telOficina;
	}

	public void setTelOficina(String telOficina) {
		this.telOficina = telOficina;
	}

	@Exportable(columnName = "LADA 1", columnOrder = 4)
	public String getTelOficinaLada() {
		return this.telOficinaLada;
	}

	public void setTelOficinaLada(String telOficinaLada) {
		this.telOficinaLada = telOficinaLada;
	}

	@Exportable(columnName = "LADA 2", columnOrder = 6)
	public String getTelOficinaLada2() {
		return this.telOficinaLada2;
	}

	public void setTelOficinaLada2(String telOficinaLada2) {
		this.telOficinaLada2 = telOficinaLada2;
	}

	@Exportable(columnName = "TELEFONO 2", columnOrder = 7)
	public String getTelOficina2() {
		return this.telOficina2;
	}

	public void setTelOficina2(String telOficina2) {
		this.telOficina2 = telOficina2;
	}

	public Set<OficinaEstado> getOficinaEstados() {
		return oficinaEstados;
	}

	public void setOficinaEstados(Set<OficinaEstado> oficinaEstados) {
		this.oficinaEstados = oficinaEstados;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return nombreOficina;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	@Transient
	public String getOficinaNombre() {
		return oficinaNombre;
	}

	public void setOficinaNombre(String oficinaNombre) {
		this.oficinaNombre = oficinaNombre;
	}

	public String getTipoServicio() {
		return tipoServicio;
	}

	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

	public String getCorreo1() {
		return correo1;
	}

	public void setCorreo1(String correo1) {
		this.correo1 = correo1;
	}

	public String getCorreo2() {
		return correo2;
	}

	public void setCorreo2(String correo2) {
		this.correo2 = correo2;
	}

	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	public Long getConsecutivoReferencia() {
		return consecutivoReferencia;
	}

	public void setConsecutivoReferencia(Long consecutivoReferencia) {
		this.consecutivoReferencia = consecutivoReferencia;
	}
	
	public Boolean getDerivaHGS() {
		return derivaHGS;
	}

	public void setDerivaHGS(Boolean derivaHGS) {
		this.derivaHGS = derivaHGS;
	}

	@Embedded
	public Coordenadas getCoordenadas() {
		return coordenadas;
	}

	public void setCoordenadas(Coordenadas coordenadas) {
		this.coordenadas = coordenadas;
	}

	public Boolean getIsWebService() {
		return isWebService;
	}

	public void setIsWebService(Boolean isWebService) {
		this.isWebService = isWebService;
	}

	@Transient
	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
	
}