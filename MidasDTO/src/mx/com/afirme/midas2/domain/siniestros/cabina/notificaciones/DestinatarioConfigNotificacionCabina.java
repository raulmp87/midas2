package mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name = "DestinatarioConfigNotificacionCabina")
@Table(name = "TODESTINATARIONOTIFCABINA", schema = "MIDAS")
public class DestinatarioConfigNotificacionCabina implements Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6389849237830742462L;

	public static enum EnumTipoDestinatario {
		CABINERO, AJUSTADOR, AGENTES, CLIENTE, OTROS, ROL, INTERESADO;

		public static String ROL_PREFIJO = "Rol_M2_";
	}

	public enum EnumTipoCorreo {
		CORREOSTODOS, CORREOSADICIONALES, CORREOPERSONAL;

	}

	public enum EnumModoEnvio {
		PARA, CC, CCO;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TODESTINOTIFCABINA_SEQ_ID_GENERATOR")
	@SequenceGenerator(name = "TODESTINOTIFCABINA_SEQ_ID_GENERATOR", schema = "MIDAS", sequenceName = "IDTODESTINOTIFCABINA_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "CONFIGNOTIFICACIONCABINA_ID", referencedColumnName = "id")
	private ConfiguracionNotificacionCabina confNotificacionCabina;

	@Column(name = "MODO_ENVIO_ID")
	private Integer modoEnvio;

	@Column(name = "TIPODESTINATARIO_ID")
	private Integer tipoDestinatario;

	@Column(name = "CORREOSENVIO_ID")
	private Integer correosEnvio;

	@Column(name = "PUESTO")
	private String puesto;

	@Column(name = "NOMBRE")
	private String nombre;

	@Column(name = "CORREO")
	private String correo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ConfiguracionNotificacionCabina getConfNotificacionCabina() {
		return confNotificacionCabina;
	}

	public void setConfNotificacionCabina(
			ConfiguracionNotificacionCabina confNotificacionCabina) {
		this.confNotificacionCabina = confNotificacionCabina;
	}

	public Integer getModoEnvio() {
		return modoEnvio;
	}

	public void setModoEnvio(Integer modoEnvio) {
		this.modoEnvio = modoEnvio;
	}

	public Integer getTipoDestinatario() {
		return tipoDestinatario;
	}

	public void setTipoDestinatario(Integer tipoDestinatario) {
		this.tipoDestinatario = tipoDestinatario;
	}

	public Integer getCorreosEnvio() {
		return correosEnvio;
	}

	public void setCorreosEnvio(Integer correosEnvio) {
		this.correosEnvio = correosEnvio;
	}

	public String getPuesto() {
		return puesto;
	}

	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof DestinatarioConfigNotificacionCabina) {
			DestinatarioConfigNotificacionCabina destinConfigNotif = (DestinatarioConfigNotificacionCabina) object;
			equal = destinConfigNotif.getId().equals(this.id);
		}
		return equal;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
}
