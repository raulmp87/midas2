package mx.com.afirme.midas2.domain.siniestros.notificaciones;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import mx.com.afirme.midas2.domain.MidasAbstracto;

@Entity
@Table(name="TOSNBITDETALLENOTIFICACION",schema="MIDAS")
public class BitacoraDetalleNotificacion extends MidasAbstracto {

	private static final long serialVersionUID = 1314221340339274906L;
	
	@EmbeddedId	
	private BitacoraDetalleNotificacionId id = new BitacoraDetalleNotificacionId();

	@ManyToOne(fetch=FetchType.LAZY)	
	@MapsId("destinoId")
	private BitacoraDestinoNotificacion destino;
	
	@Column(name="MAPADATOS")
	private String mapaDatos;
	
	
	public BitacoraDetalleNotificacion(){
		super();
	}

	public BitacoraDetalleNotificacion(Integer secuenciaEnvio, String mapaDatos, String codigoUsuarioCreacion) {
		this();
		this.getId().setSecuenciaEnvio(secuenciaEnvio != null ? secuenciaEnvio : 1);
		this.mapaDatos = mapaDatos;
		super.codigoUsuarioCreacion = codigoUsuarioCreacion;		
	}

	public BitacoraDetalleNotificacion(Integer secuenciaEnvio, String mapaDatos, BitacoraDestinoNotificacion destino, String codigoUsuarioCreacion) {
		this(secuenciaEnvio, mapaDatos, codigoUsuarioCreacion);		
		this.destino = destino;
	}
	
	public BitacoraDestinoNotificacion getDestino() {
		return destino;
	}

	public void setDestino(BitacoraDestinoNotificacion destino) {
		this.destino = destino;
	}

	public BitacoraDetalleNotificacionId getId() {
		return id;
	}

	public void setId(BitacoraDetalleNotificacionId id) {
		this.id = id;
	}	
	
	public String getMapaDatos() {
		return mapaDatos;
	}

	public void setMapaDatos(String mapaDatos) {
		this.mapaDatos = mapaDatos;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BitacoraDetalleNotificacionId getKey() {
		return id;
	}

	@Override
	public String getValue() {		
		return destino != null ? destino.getValue().concat("-").concat(id.getSecuenciaEnvio().toString()) : id.getSecuenciaEnvio().toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public BitacoraDetalleNotificacionId getBusinessKey() {
		return id;
	}

}
