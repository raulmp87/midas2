package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@Table(name = "TCTIPOPASE_RESPONSABILIDADCONF", schema = "MIDAS")
@ReadOnly
public class TipoPaseAtencionConf implements Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3684049448302018774L;

	@Id
	@Column(name="ID")
	private Long id;
	
	@Column(name="TIPO_ESTIMACION")
	private String tipoEstimacion;
	
	@Column(name="TIPO_RESPONSABILIDAD")
	private String tipoResponsabilidad;
	
	@Column(name="TIPO_PASE")
	private String tipoPase;
	
	@Column(name="DESCRIPCION")
	private String descripcion;
	
	@Column(name="CAUSA_SINIESTRO")
	private String causaSiniestro;
	
	@Column(name="TERMINO_AJUSTE")
	private String terminoAjuste;
	
	@Column(name="ACTIVO")
	private Boolean activo;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipoEstimacion() {
		return tipoEstimacion;
	}

	public void setTipoEstimacion(String tipoEstimacion) {
		this.tipoEstimacion = tipoEstimacion;
	}

	public String getTipoResponsabilidad() {
		return tipoResponsabilidad;
	}

	public void setTipoResponsabilidad(String tipoResponsabilidad) {
		this.tipoResponsabilidad = tipoResponsabilidad;
	}

	public String getTipoPase() {
		return tipoPase;
	}

	public void setTipoPase(String tipoPase) {
		this.tipoPase = tipoPase;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
	

	public String getCausaSiniestro() {
		return causaSiniestro;
	}

	public void setCausaSiniestro(String causaSiniestro) {
		this.causaSiniestro = causaSiniestro;
	}

	public String getTerminoAjuste() {
		return terminoAjuste;
	}

	public void setTerminoAjuste(String terminoAjuste) {
		this.terminoAjuste = terminoAjuste;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}
