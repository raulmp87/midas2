package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity(name = "EstimacionGenerica")
@Table(name = "TOESTIMACIONGENERICA", schema = "MIDAS")
@DiscriminatorValue("ESD")
@PrimaryKeyJoinColumn(name = "ESTIMACIONCOBERTURAREPCAB_ID", referencedColumnName = "ID")
public class EstimacionGenerica extends EstimacionCoberturaReporteCabina {

	private static final long serialVersionUID = 1L;

	@Column(name = "CORREO")
	private String correo;

	public EstimacionGenerica() {
	}

	public void finalize() throws Throwable {
		super.finalize();
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

}