package mx.com.afirme.midas2.domain.siniestros.incentivos;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;

@Entity
@Table(name = "TOSNINCENTIVOAJUSTADOR", schema = "MIDAS")
public class IncentivoAjustador  extends MidasAbstracto {
	
	private static final long serialVersionUID = 1L;
	@Id 
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOSNINCENTIVOAJUSTADOR_SEQ")
	@SequenceGenerator(name="TOSNINCENTIVOAJUSTADOR_SEQ", schema="MIDAS", sequenceName="TOSNINCENTIVOAJUSTADOR_SEQ", allocationSize=1)	
    @Column(name="ID", unique=true, nullable=false, precision=8)
    private Long id;
	
	
	@Column(name="CONCEPTO")
	private String concepto;
	
	
	@Column(name="IMPORTE_RECUPERACION")
	private BigDecimal importeRecuperacion;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CONFIGINCENTIVO_ID", referencedColumnName = "ID")
	private ConfiguracionIncentivos configuracionIncentivos;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "REPORTE_ID", referencedColumnName = "ID")
	private ReporteCabina reporteCabina;
	
	@Column(name="SELECCIONADO")
	private Boolean seleccionado;
	
	public enum ConceptoIncentivo{INHABIL, EFECTIVO, COMPANIA, SIPAC, RECHAZO};
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public BigDecimal getImporteRecuperacion() {
		return importeRecuperacion;
	}

	public void setImporteRecuperacion(BigDecimal importeRecuperacion) {
		this.importeRecuperacion = importeRecuperacion;
	}

	public ConfiguracionIncentivos getConfiguracionIncentivos() {
		return configuracionIncentivos;
	}

	public void setConfiguracionIncentivos(
			ConfiguracionIncentivos configuracionIncentivos) {
		this.configuracionIncentivos = configuracionIncentivos;
	}

	public ReporteCabina getReporteCabina() {
		return reporteCabina;
	}

	public void setReporteCabina(ReporteCabina reporteCabina) {
		this.reporteCabina = reporteCabina;
	}

	public Boolean getSeleccionado() {
		return seleccionado;
	}

	public void setSeleccionado(Boolean seleccionado) {
		this.seleccionado = seleccionado;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
