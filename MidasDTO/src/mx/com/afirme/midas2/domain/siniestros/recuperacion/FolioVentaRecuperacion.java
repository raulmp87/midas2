package mx.com.afirme.midas2.domain.siniestros.recuperacion;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;


/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 19-may-2015 09:16:30 a.m.
 */

@Entity(name = "FolioVentaRecuperacion")
@Table(name = "TOSNFOLIORECUPERACION", schema = "MIDAS")
public class FolioVentaRecuperacion implements Serializable, Entidad{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1852913441541710147L;


	@Id
	@Column(name="ID",nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOSNFOLIORECUPERACION_SEQ_GENERATOR")	
	@SequenceGenerator(name="TOSNFOLIORECUPERACION_SEQ_GENERATOR", schema="MIDAS", sequenceName="TOSNFOLIORECUPERACION_SEQ", allocationSize=1)	
	private Long id;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "RECUPERACION_ID", referencedColumnName = "ID")
	private Recuperacion recuperacion;
	
	@Column(name = "CONCEPTO") 
	private String concepto;
	
	@Column(name = "FOLIO") 
	private String folio;
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Recuperacion getRecuperacion() {
		return recuperacion;
	}
	public void setRecuperacion(Recuperacion recuperacion) {
		this.recuperacion = recuperacion;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	@Override
	public Long getKey() {
		return this.id;
	}
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	
	
}