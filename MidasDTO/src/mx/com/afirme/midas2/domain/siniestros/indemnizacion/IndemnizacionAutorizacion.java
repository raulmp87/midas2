/**
 * 
 */
package mx.com.afirme.midas2.domain.siniestros.indemnizacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * @author admin
 *
 */
@Entity(name = "IndemnizacionAutorizacion")
@Table(name = "TOINDEMNIZACIONAUTORIZACION", schema = "MIDAS")
public class IndemnizacionAutorizacion implements Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name = "INDEMNIZACIONAUTORIZACION_SEQ_GENERADOR",allocationSize = 1, sequenceName = "INDEMNIZACIONAUTORIZACION_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "INDEMNIZACIONAUTORIZACION_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "INDEMNIZACION_ID", referencedColumnName = "ID")	
	private IndemnizacionSiniestro indemnizacionSiniestro;
	
	@Column(name="ROL")
	private String rol;
	
	@Column(name="TIPO")
	private String tipo;	
	
	@Column(name="CODIGO_USUARIO")
	private String codigoUsuario ;	
	
	
	
	@Column(name="ESTATUS")
	private String estatus;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_AUTORIZACION")
	private Date fechaAutorizacion;
	
	
	@Column(name="COMENTARIOS")
	private String comentarios ;


	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public IndemnizacionSiniestro getIndemnizacionSiniestro() {
		return indemnizacionSiniestro;
	}


	public void setIndemnizacionSiniestro(
			IndemnizacionSiniestro indemnizacionSiniestro) {
		this.indemnizacionSiniestro = indemnizacionSiniestro;
	}


	public String getRol() {
		return rol;
	}


	public void setRol(String rol) {
		this.rol = rol;
	}


	public String getTipo() {
		return tipo;
	}


	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	public String getCodigoUsuario() {
		return codigoUsuario;
	}


	public void setCodigoUsuario(String codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}


	public String getEstatus() {
		return estatus;
	}


	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}


	public Date getFechaAutorizacion() {
		return fechaAutorizacion;
	}


	public void setFechaAutorizacion(Date fechaAutorizacion) {
		this.fechaAutorizacion = fechaAutorizacion;
	}


	public String getComentarios() {
		return comentarios;
	}


	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}
	
	
	
	
	
	

}
