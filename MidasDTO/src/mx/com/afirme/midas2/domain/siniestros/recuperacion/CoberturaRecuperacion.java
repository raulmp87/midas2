package mx.com.afirme.midas2.domain.siniestros.recuperacion;


import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;


/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 19-may-2015 09:15:56 a.m.
 */

/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 19-may-2015 09:16:22 a.m.
 */

//@Entity @Table(TRCOBERTURARECUPERACION)
@Entity(name = "CoberturaRecuperacion")
@Table(name = "TRSNCOBERTURARECUPERACION", schema = "MIDAS")
public class CoberturaRecuperacion implements Serializable, Entidad {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3909552591932534889L;

	@Id
	@Column(name="ID",nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TRSNCOBERTURARECUPERACION_SEQ_GENERATOR")	
	@SequenceGenerator(name="TRSNCOBERTURARECUPERACION_SEQ_GENERATOR", schema="MIDAS", sequenceName="TRSNCOBERTURARECUPERACION_SEQ", allocationSize=1)
	private Long id ;
	
	@Column(name = "CLAVE_SUBCALCULO")
	private String claveSubCalculo;
	
	//@ManyToOne @JoinColumn
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COBERTURAREPORTE_ID", referencedColumnName = "ID")
	private CoberturaReporteCabina coberturaReporte;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "RECUPERACION_ID", referencedColumnName = "ID")
	private Recuperacion recuperacion;
	
	
	@OneToMany(fetch=FetchType.LAZY,  mappedBy = "coberturaRecuperacion", cascade = CascadeType.ALL, orphanRemoval=true)
	private List<PaseRecuperacion> pasesAtencion;	
	

	public CoberturaRecuperacion(){
		super();
	}
	

	public CoberturaRecuperacion(String claveSubCalculo,
			CoberturaReporteCabina coberturaReporte, Recuperacion recuperacion,
			List<PaseRecuperacion> pasesAtencion) {
		this();
		this.claveSubCalculo = claveSubCalculo;
		this.coberturaReporte = coberturaReporte;
		this.recuperacion = recuperacion;
		this.pasesAtencion = pasesAtencion;
	}
	
	public CoberturaRecuperacion(String claveSubCalculo,
			CoberturaReporteCabina coberturaReporte, Recuperacion recuperacion) {
		this();
		this.claveSubCalculo = claveSubCalculo;
		this.coberturaReporte = coberturaReporte;
		this.recuperacion = recuperacion;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}


	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getClaveSubCalculo() {
		return claveSubCalculo;
	}


	public void setClaveSubCalculo(String claveSubCalculo) {
		this.claveSubCalculo = claveSubCalculo;
	}


	public CoberturaReporteCabina getCoberturaReporte() {
		return coberturaReporte;
	}


	public void setCoberturaReporte(CoberturaReporteCabina coberturaReporte) {
		this.coberturaReporte = coberturaReporte;
	}


	public Recuperacion getRecuperacion() {
		return recuperacion;
	}


	public void setRecuperacion(Recuperacion recuperacion) {
		this.recuperacion = recuperacion;
	}


	public List<PaseRecuperacion> getPasesAtencion() {
		return pasesAtencion;
	}


	public void setPasesAtencion(
			List<PaseRecuperacion> pasesAtencion) {
		this.pasesAtencion = pasesAtencion;
	}

}