package mx.com.afirme.midas2.domain.siniestros.catalogo.solicitudautorizacion;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

import mx.com.afirme.midas2.annotation.Exportable;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;




@Entity
@Table(name = "TOSOLAUTAJUSTEANTIGUEDAD", schema = "MIDAS")
public class SolicitudAutorizacionAjusteAntiguedad extends MidasAbstracto implements Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TOSOLAUTAJUSTEANT_ID_SEQ")
	@SequenceGenerator(name = "TOSOLAUTAJUSTEANT_ID_SEQ",  schema="MIDAS", sequenceName = "TOSOLAUTAJUSTEANT_ID_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Long id;

	
	@Column(name = "MONTO_AJUSTE")
	private BigDecimal montoAjuste;
	
	@Column(name = "CAUSA_MOVIMIENTO")
	private String causaMovimiento;
	
	@Column(name = "TIPO_AJUSTE")
	private String tipoAjuste;
	
	@Column(name = "ESTATUS")
	private String estatus;
	
	@Column(name = "ROL_PRIMERAAUTORIZACION")
	private String rolPrimerAutorizacion;
	
	@Column(name = "ROL_SEGUNDAAUTORIZACION")
	private String rolSegundaAutorizacion;
	
	@Column(name = "ROL_TERCERAAUTORIZACION")
	private String rolTerceraAutorizacion;
	
	@Column(name = "ROL_CUARTAAUTORIZACION")
	private String rolCuartaAutorizacion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_ESTATUS_PRIMERA")
	private Date fechaEstatusPrimera;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_ESTATUS_SEGUNDA")
	private Date fechaEstatusSegunda;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_ESTATUS_TERCERA")
	private Date fechaEstatusTercera;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_ESTATUS_CUARTA")
	private Date fechaEstatusCuarta;
	
	@Column(name = "CODIGO_USUARIO_PRIMERA")
	private String codigoUsuarioPrimera;
	
	@Column(name = "CODIGO_USUARIO_SEGUNDA")
	private String codigoUsuarioSegunda;
	
	@Column(name = "CODIGO_USUARIO_TERCERA")
	private String codigoUsuarioTercera;
	
	@Column(name = "CODIGO_USUARIO_CUARTA")
	private String codigoUsuarioCuarta;
	
	@Column(name = "ESTATUS_PRIMERA_AUTORIZACION")
	private String estatusPrimeraAutorizacion;
	
	@Column(name = "ESTATUS_SEGUNDA_AUTORIZACION")
	private String estatusSegundaAutorizacion;
	
	@Column(name = "ESTATUS_TERCERA_AUTORIZACION")
	private String estatusTercerAutorizacion;
	
	@Column(name = "ESTATUS_CUARTA_AUTORIZACION")
	private String estatusCuartaAutorizacion;
	
	@Transient
	private int autoriza;
	
	@Transient
	private String estatusDesc;
	
	@Transient
	private Integer mesesAntiguedad;
	
	@Transient
	private String nombreCobertura;
	
	@Transient
	private String terminoAjusteDesc;
	
	@Transient
	private String tipoAjusteDesc;
	
	
	@Transient
	private String oficinaDesc;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinFetch(JoinFetchType.INNER)
	@JoinColumn(name = "ESTIMACIONCOBERTURA_ID", referencedColumnName = "ID")
	private EstimacionCoberturaReporteCabina estimacionCobertura;

	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Exportable(columnName="MONTO DE AJUSTE", columnOrder=5)
	public BigDecimal getMontoAjuste() {
		return montoAjuste;
	}

	public void setMontoAjuste(BigDecimal montoAjuste) {
		this.montoAjuste = montoAjuste;
	}

	public String getCausaMovimiento() {
		return causaMovimiento;
	}

	public void setCausaMovimiento(String causaMovimiento) {
		this.causaMovimiento = causaMovimiento;
	}

	public String getTipoAjuste() {
		return tipoAjuste;
	}

	public void setTipoAjuste(String tipoAjuste) {
		this.tipoAjuste = tipoAjuste;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getRolPrimerAutorizacion() {
		return rolPrimerAutorizacion;
	}

	public void setRolPrimerAutorizacion(String rolPrimerAutorizacion) {
		this.rolPrimerAutorizacion = rolPrimerAutorizacion;
	}

	public String getRolSegundaAutorizacion() {
		return rolSegundaAutorizacion;
	}

	public void setRolSegundaAutorizacion(String rolSegundaAutorizacion) {
		this.rolSegundaAutorizacion = rolSegundaAutorizacion;
	}

	public String getRolTerceraAutorizacion() {
		return rolTerceraAutorizacion;
	}

	public void setRolTerceraAutorizacion(String rolTerceraAutorizacion) {
		this.rolTerceraAutorizacion = rolTerceraAutorizacion;
	}

	public String getRolCuartaAutorizacion() {
		return rolCuartaAutorizacion;
	}

	public void setRolCuartaAutorizacion(String rolCuartaAutorizacion) {
		this.rolCuartaAutorizacion = rolCuartaAutorizacion;
	}

	public Date getFechaEstatusPrimera() {
		return fechaEstatusPrimera;
	}

	public void setFechaEstatusPrimera(Date fechaEstatusPrimera) {
		this.fechaEstatusPrimera = fechaEstatusPrimera;
	}

	public Date getFechaEstatusSegunda() {
		return fechaEstatusSegunda;
	}

	public void setFechaEstatusSegunda(Date fechaEstatusSegunda) {
		this.fechaEstatusSegunda = fechaEstatusSegunda;
	}

	public Date getFechaEstatusTercera() {
		return fechaEstatusTercera;
	}

	public void setFechaEstatusTercera(Date fechaEstatusTercera) {
		this.fechaEstatusTercera = fechaEstatusTercera;
	}

	public Date getFechaEstatusCuarta() {
		return fechaEstatusCuarta;
	}

	public void setFechaEstatusCuarta(Date fechaEstatusCuarta) {
		this.fechaEstatusCuarta = fechaEstatusCuarta;
	}

	public String getCodigoUsuarioPrimera() {
		return codigoUsuarioPrimera;
	}

	public void setCodigoUsuarioPrimera(String codigoUsuarioPrimera) {
		this.codigoUsuarioPrimera = codigoUsuarioPrimera;
	}

	public String getCodigoUsuarioSegunda() {
		return codigoUsuarioSegunda;
	}

	public void setCodigoUsuarioSegunda(String codigoUsuarioSegunda) {
		this.codigoUsuarioSegunda = codigoUsuarioSegunda;
	}

	public String getCodigoUsuarioTercera() {
		return codigoUsuarioTercera;
	}

	public void setCodigoUsuarioTercera(String codigoUsuarioTercera) {
		this.codigoUsuarioTercera = codigoUsuarioTercera;
	}

	public String getCodigoUsuarioCuarta() {
		return codigoUsuarioCuarta;
	}

	public void setCodigoUsuarioCuarta(String codigoUsuarioCuarta) {
		this.codigoUsuarioCuarta = codigoUsuarioCuarta;
	}

	public String getEstatusPrimeraAutorizacion() {
		return estatusPrimeraAutorizacion;
	}

	public void setEstatusPrimeraAutorizacion(String estatusPrimeraAutorizacion) {
		this.estatusPrimeraAutorizacion = estatusPrimeraAutorizacion;
	}

	public String getEstatusSegundaAutorizacion() {
		return estatusSegundaAutorizacion;
	}

	public void setEstatusSegundaAutorizacion(String estatusSegundaAutorizacion) {
		this.estatusSegundaAutorizacion = estatusSegundaAutorizacion;
	}

	public String getEstatusTercerAutorizacion() {
		return estatusTercerAutorizacion;
	}

	public void setEstatusTercerAutorizacion(String estatusTercerAutorizacion) {
		this.estatusTercerAutorizacion = estatusTercerAutorizacion;
	}

	public String getEstatusCuartaAutorizacion() {
		return estatusCuartaAutorizacion;
	}

	public void setEstatusCuartaAutorizacion(String estatusCuartaAutorizacion) {
		this.estatusCuartaAutorizacion = estatusCuartaAutorizacion;
	}

	public int getAutoriza() {
		return autoriza;
	}

	public void setAutoriza(int autoriza) {
		this.autoriza = autoriza;
	}

	@Exportable(columnName="ESTATUS", columnOrder=6)
	public String getEstatusDesc() {
		return estatusDesc;
	}

	public void setEstatusDesc(String estatusDesc) {
		this.estatusDesc = estatusDesc;
	}

	@Exportable(columnName="MESES DE ANTIGUEDAD", columnOrder=7)
	public Integer getMesesAntiguedad() {
		return mesesAntiguedad;
	}

	public void setMesesAntiguedad(Integer mesesAntiguedad) {
		this.mesesAntiguedad = mesesAntiguedad;
	}

	@Exportable(columnName="COBERTURA", columnOrder=1)
	public String getNombreCobertura() {
		return nombreCobertura;
	}

	public void setNombreCobertura(String nombreCobertura) {
		this.nombreCobertura = nombreCobertura;
	}

	@Exportable(columnName="TERMINO AJUSTE", columnOrder=3)
	public String getTerminoAjusteDesc() {
		return terminoAjusteDesc;
	}

	public void setTerminoAjusteDesc(String terminoAjusteDesc) {
		this.terminoAjusteDesc = terminoAjusteDesc;
	}

	@Exportable(columnName="TIPO DE AJUSTE", columnOrder=4)
	public String getTipoAjusteDesc() {
		return tipoAjusteDesc;
	}

	public void setTipoAjusteDesc(String tipoAjusteDesc) {
		this.tipoAjusteDesc = tipoAjusteDesc;
	}

	public EstimacionCoberturaReporteCabina getEstimacionCobertura() {
		return estimacionCobertura;
	}

	public void setEstimacionCobertura(
			EstimacionCoberturaReporteCabina estimacionCobertura) {
		this.estimacionCobertura = estimacionCobertura;
	}
	
	@Exportable(columnName="OFICINA", columnOrder=0)
	public String getOficinaDesc() {
		return oficinaDesc;
	}

	public void setOficinaDesc(String oficinaDesc) {
		this.oficinaDesc = oficinaDesc;
	}
	
	@Exportable(columnName="SINIESTRO", columnOrder=2)
	public String getSiniestroDesc() {
		String siniestroDesc = "";
		SiniestroCabina siniestroCabina = this.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getSiniestroCabina();
		if(siniestroCabina!=null){
			siniestroDesc = siniestroCabina.getNumeroSiniestro();
		}
		return siniestroDesc;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long  getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}