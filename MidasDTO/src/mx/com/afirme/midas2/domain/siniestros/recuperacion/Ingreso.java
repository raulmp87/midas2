package mx.com.afirme.midas2.domain.siniestros.recuperacion;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.EnumBase;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza.AplicacionCobranzaIngreso;


/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 19-may-2015 09:16:37 a.m.
 */
//@Entity @Table(TOSNINGRESO)
@Entity(name = "Ingreso")
@Table(name = "TOSNINGRESO", schema = "MIDAS")
public class Ingreso extends MidasAbstracto {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4082392989997954561L;
	
	public static final String SECUENCIA_NUMERO = "MIDAS.SN_INGRESO_NUM_SEQ";

	@Id
	@SequenceGenerator(name = "TOSNINGRESO_SEQ_GENERADOR",allocationSize = 1, sequenceName = "TOSNINGRESO_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TOSNINGRESO_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id;
	
	@Column(name= "numero")
	private Long numero;

	@Column(name = "ESTATUS")
	private String estatus;
	
	@Column(name = "CODIGO_USUARIO_CANCELACION")
	private String codigoUsuarioCancelacion;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CANCELACION")
	private Date fechaCancelacion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_INGRESO")
	private Date fechaIngreso;
	
	@Column(name="CODIGO_USUARIO_INGRESO")
	private String codigoUsuarioIngreso;
	
	@Column(name="MONTO")
	private BigDecimal monto;
	
	@Column(name="MOTIVO_CANCELACION")
	private String motivoCancelacion;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "RECUPERACION_ID", referencedColumnName = "ID")
	private Recuperacion recuperacion;
	
	@OneToMany(fetch=FetchType.LAZY,  mappedBy = "ingreso")
	private List<AplicacionCobranzaIngreso> aplicacionesIngreso = new ArrayList<AplicacionCobranzaIngreso>();
	
	public static enum EstatusIngreso implements EnumBase<String>{
        PENDIENTE("PENDIENTE"), 
        APLICADO("APLICADO"), 
        CANCELADO_INGRESO_PENDIENTE_POR_APLICAR("CANCPEND"), 
        CANCELADO_POR_DEVOLUCION("CANCDEV"), 
        CANCELADO_ENVIO_CUENTA_ACREEDORA("CANCENVAC"), 
        CANCELADO_POR_REVERSA("REVERSA"); 
        private String value;
        
        private EstatusIngreso(String value){
              this.value=value;
        }
        
        @Override
        public String getValue(){
              return value;
        }

		@Override
		public String getLabel() {
			return name();
		}
	}

	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public Date getFechaCancelacion() {
		return fechaCancelacion;
	}
	public void setFechaCancelacion(Date fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}
	
	/**
	 * @return the codigoUsuarioCancelacion
	 */
	public String getCodigoUsuarioCancelacion() {
		return codigoUsuarioCancelacion;
	}
	/**
	 * @param codigoUsuarioCancelacion the codigoUsuarioCancelacion to set
	 */
	public void setCodigoUsuarioCancelacion(String codigoUsuarioCancelacion) {
		this.codigoUsuarioCancelacion = codigoUsuarioCancelacion;
	}
	
	/**
	 * @return the recuperacion
	 */
	public Recuperacion getRecuperacion() {
		return recuperacion;
	}
	
	/**
	 * @return the numero
	 */
	public Long getNumero() {
		return numero;
	}
	/**
	 * @param numero the numero to set
	 */
	public void setNumero(Long numero) {
		this.numero = numero;
	}
	/**
	 * @return the fechaIngreso
	 */
	public Date getFechaIngreso() {
		return fechaIngreso;
	}
	/**
	 * @param fechaIngreso the fechaIngreso to set
	 */
	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	/**
	 * @return the codigoUsuarioIngreso
	 */
	public String getCodigoUsuarioIngreso() {
		return codigoUsuarioIngreso;
	}
	/**
	 * @param codigoUsuarioIngreso the codigoUsuarioIngreso to set
	 */
	public void setCodigoUsuarioIngreso(String codigoUsuarioIngreso) {
		this.codigoUsuarioIngreso = codigoUsuarioIngreso;
	}
	/**
	 * @param recuperacion the recuperacion to set
	 */
	public void setRecuperacion(Recuperacion recuperacion) {
		this.recuperacion = recuperacion;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}
	
	@Override
	public String getValue() {
		return numero.toString();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.id;
	}
	/**
	 * @return the monto
	 */
	public BigDecimal getMonto() {
		return monto;
	}
	/**
	 * @param monto the monto to set
	 */
	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}
	
	public String getMotivoCancelacion() {
		return motivoCancelacion;
	}
	
	public void setMotivoCancelacion(String motivoCancelacion) {
		this.motivoCancelacion = motivoCancelacion;
	}
	
	public List<AplicacionCobranzaIngreso> getAplicacionesIngreso() {
		return aplicacionesIngreso;
	}
	public void setAplicacionesIngreso(
			List<AplicacionCobranzaIngreso> aplicacionesIngreso) {
		this.aplicacionesIngreso = aplicacionesIngreso;
	}

	
}