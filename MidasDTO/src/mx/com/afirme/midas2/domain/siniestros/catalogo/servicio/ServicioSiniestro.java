package mx.com.afirme.midas2.domain.siniestros.catalogo.servicio;
 
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.annotation.Exportable;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.personadireccion.PersonaMidas;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;

@Entity
@Table(name = "TCSERVICIOSINIESTRO", schema = "MIDAS")
public class ServicioSiniestro extends MidasAbstracto implements Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8036761723145186009L;
	@Transient
	public static final Short AMBITO_PRESTADOR_SERVICIO_INTERNO = Short.valueOf("1");

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCSERVICIOSINIESTRO_SEQ")
	@SequenceGenerator(name = "IDTCSERVICIOSINIESTRO_SEQ", sequenceName = "IDTCSERVICIOSINIESTRO_SEQ", schema = "MIDAS", allocationSize = 1)
	@Column(name = "ID", unique = true, nullable = false)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRESTADOR_ID")
	private PrestadorServicio prestadorServicio;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "OFICINA_ID")
	private Oficina oficina;

	@Column(name = "SERVICIO_SINIESTRO_ID", nullable = false)
	private Long servicioSiniestroId;

	@Column(name = "AMBITO_PRESTADOR_SERVICIO", nullable = false)
	private Short ambitoPrestadorServicio;

	@Column(name = "ESTATUS", nullable = false)
	private Short estatus;

	@Column(name = "CERTIFICACION")
	private Short certificacion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CAMBIO_ESTATUS")
	private Date fechaCambioEstatus;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CREACION")
	private Date fechaCreacion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_MODIFICACION")
	private Date fechaModificacion;

	@Column(name = "CODIGO_USUARIO_CREACION")
	private String codigoUsuarioCreacion;

	@Column(name = "CODIGO_USUARIO_MODIFICACION")
	private String codigoUsuarioModificacion;

	@OneToOne(fetch = FetchType.LAZY, cascade= CascadeType.ALL)
	@JoinColumn(name = "PERSONA_ID", referencedColumnName="ID")
	private PersonaMidas personaMidas;
	
	@Column(name = "ID_SEYCOS")
	private Long idSeycos;
	
	@Column(name = "CLAVE_USUARIO")
	private String claveUsuario;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, optional = true, mappedBy = "servicioSiniestro")
	private ServicioSiniestroUnidad servicioSiniestroUnidad;
	

	@Transient
	private String nombreEstatus;
	@Transient
	private String nombreTipoSolicitud;
	@Transient
	private String nombreCertificacion;
	@Transient
	private String estado;
	@Transient
	private String municipio;
	@Transient
	private String tipoDisponibilidad;
	@Transient
	private String estatusAsignacion;
	@Transient
	private String ambitoDesc;
		
	
	public ServicioSiniestro() {
		super();
	}
	
	public ServicioSiniestro(Long id, PrestadorServicio prestadorServicio,
			Oficina oficina, Long servicioSiniestroId,
			Short ambitoPrestadorServicio, Short estatus, Short certificacion,
			Date fechaCambioEstatus, Date fechaCreacion,
			Date fechaModificacion, String codigoUsuarioCreacion,
			String codigoUsuarioModificacion, PersonaMidas personaMidas,
			Long idSeycos) {
		this();
		this.id = id;
		this.prestadorServicio = prestadorServicio;
		this.oficina = oficina;
		this.servicioSiniestroId = servicioSiniestroId;
		this.ambitoPrestadorServicio = ambitoPrestadorServicio;
		this.estatus = estatus;
		this.certificacion = certificacion;
		this.fechaCambioEstatus = fechaCambioEstatus;
		this.fechaCreacion = fechaCreacion;
		this.fechaModificacion = fechaModificacion;
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
		this.personaMidas = personaMidas;
		this.idSeycos = idSeycos;
		
	}
	
	public static enum Ambito{
		
		INTERNO(new Short("1")),
		EXTERNO(new Short("2"));
		
		private Short value;
		
		private Ambito(Short value){
			this.value=value;
		}
		
		public Short getValue(){
			return value;
		}
	}
	



	@Exportable(columnName = "No. Abogado", columnOrder = 0, document="ABOGADO")
	public Long getId() {
		return id;
	}

	@Exportable(columnName = "No. Ajustador", columnOrder = 0, document="AJUSTADOR")
	@Transient
	public Long getIdAjustador() {
		return id;
	}
	@Exportable(columnName = "Nombre del Abogado", columnOrder = 1, document="ABOGADO")
	@Transient
	public String getNombrePersona() {
		return this.personaMidas.getNombre();
	}

	@Exportable(columnName = "Nombre del Ajustador", columnOrder = 1, document="AJUSTADOR")
	@Transient
	public String getNombrePersonaAjustador() {
		return this.personaMidas.getNombre();
	}
	
	@Exportable(columnName = "Oficina", columnOrder = 2, document="ABOGADO,AJUSTADOR")
	@Transient
	public String getNombreOficina() {
		return this.oficina.getNombreOficina();
	}

	@Exportable(columnName = "Tipo de Abogado", columnOrder = 3, document="ABOGADO")
	public String getAmbitoPrestadorServicioExportable() {

		if (this.ambitoPrestadorServicio == 1) {
			nombreTipoSolicitud = "Interno";
		} else {
			nombreTipoSolicitud = "Externo";
		}
		return nombreTipoSolicitud;
	}
	
	@Exportable(columnName = "Tipo de Ajustador", columnOrder = 3, document="AJUSTADOR")
	public String getAmbitoAjustadorExportable() {

		if (this.ambitoPrestadorServicio == 1) {
			nombreTipoSolicitud = "Interno";
		} else {
			nombreTipoSolicitud = "Externo";
		}
		return nombreTipoSolicitud;
	}

	@Exportable(columnName = "Estatus", columnOrder = 4, document="ABOGADO,AJUSTADOR")
	public String getNombreEstatus() {
		if (this.estatus == 1) {
			nombreEstatus = "Activo";
		} else {
			nombreEstatus = "Inactivo";
		}
		return nombreEstatus;
	}

	@Exportable(columnName = "No. Prestador", columnOrder = 5, document="ABOGADO,AJUSTADOR")
	@Transient
	public String getIdPrestadorServicio() {
		
		if( this.prestadorServicio == null ){
			return "";
		}else{
			return this.prestadorServicio.getId().toString();
		}
	}

	@Exportable(columnName = "Nombre Prestador", columnOrder = 6, document="ABOGADO,AJUSTADOR")
	@Transient
	public String getNombrePrestadorServicio() {
		if( this.prestadorServicio == null ){
			return "";
		}else{
			return this.prestadorServicio.getPersonaMidas().getNombre();
		}
	}


	@Exportable(columnName = "Id en Seycos", columnOrder = 7, document="ABOGADO, AJUSTADOR")
	public Long getIdSeycos() {
		return idSeycos;
	}

	
	public PrestadorServicio getPrestadorServicio() {
		return prestadorServicio;
	}

	public Long getServicioSiniestroId() {
		return servicioSiniestroId;
	}

	public Date getFechaCambioEstatus() {
		return fechaCambioEstatus;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	public String getCodigoUsuarioModificacion() {
		return codigoUsuarioModificacion;
	}

	// SETTERS

	public void setId(Long id) {
		this.id = id;
	}

	public void setServicioSiniestroId(Long servicioSiniestroId) {
		this.servicioSiniestroId = servicioSiniestroId;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Long getBusinessKey() {
		return this.id;
	}

	public void setNombreEstatus(String nombreEstatus) {
		this.nombreEstatus = nombreEstatus;
	}

	public void setNombreTipoSolicitud(String nombreTipoSolicitud) {
		this.nombreTipoSolicitud = nombreTipoSolicitud;
	}

	public void setNombreCertificacion(String nombreCertificacion) {
		this.nombreCertificacion = nombreCertificacion;
	}

	public String getNombreCertificacion() {

		if (this.certificacion == 1) {
			nombreCertificacion = "Tiene";
		} else {
			nombreCertificacion = "No tiene";
		}
		return nombreCertificacion;
	}



	public Short getEstatus() {
		return estatus;
	}

	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}

	public Short getCertificacion() {
		return certificacion;
	}

	public void setCertificacion(Short certificacion) {
		this.certificacion = certificacion;
	}

	public void setFechaCambioEstatus(Date fechaCambioEstatus) {
		this.fechaCambioEstatus = fechaCambioEstatus;
	}

	public PersonaMidas getPersonaMidas() {
		return personaMidas;
	}

	public void setPersonaMidas(PersonaMidas personaMidas) {
		this.personaMidas = personaMidas;
	}

	public Oficina getOficina() {
		return oficina;
	}

	public void setOficina(Oficina oficina) {
		this.oficina = oficina;
	}

	public void setPrestadorServicio(PrestadorServicio prestadorServicio) {
		this.prestadorServicio = prestadorServicio;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public void setAmbitoPrestadorServicio(Short ambitoPrestadorServicio) {
		this.ambitoPrestadorServicio = ambitoPrestadorServicio;
	}

	public Short getAmbitoPrestadorServicio() {
		return ambitoPrestadorServicio;
	}

	public String getTipoDisponibilidad() {
		return tipoDisponibilidad;
	}

	public void setTipoDisponibilidad(String tipoDisponibilidad) {
		this.tipoDisponibilidad = tipoDisponibilidad;
	}

	public String getEstatusAsignacion() {
		return estatusAsignacion;
	}

	public void setEstatusAsignacion(String estatusAsignacion) {
		this.estatusAsignacion = estatusAsignacion;
	}

	public void setIdSeycos(Long idSeycos) {
		this.idSeycos = idSeycos;
	}
	
	public String getClaveUsuario() {
		return claveUsuario;
	}

	public void setClaveUsuario(String claveUsuario) {
		this.claveUsuario = claveUsuario;
	}

	public String getAmbitoDesc() {
		return ambitoDesc;
	}

	public void setAmbitoDesc(String ambitoDesc) {
		this.ambitoDesc = ambitoDesc;
	}

	public String getNombreTipoSolicitud() {
		return nombreTipoSolicitud;
	}
	
	public ServicioSiniestroUnidad getServicioSiniestroUnidad() {
		return servicioSiniestroUnidad;
	}

	public void setServicioSiniestroUnidad(
			ServicioSiniestroUnidad servicioSiniestroUnidad) {
		this.servicioSiniestroUnidad = servicioSiniestroUnidad;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServicioSiniestro other = (ServicioSiniestro) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
