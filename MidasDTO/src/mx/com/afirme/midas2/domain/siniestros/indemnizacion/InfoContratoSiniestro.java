package mx.com.afirme.midas2.domain.siniestros.indemnizacion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;

@Entity
@Table(name="TOSNINFO_CONTRATO", schema="MIDAS")
public class InfoContratoSiniestro extends MidasAbstracto implements Entidad{

	private static final long serialVersionUID = 7597546357947767425L;
	
	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="INFOCONTRATO_GENERATOR")
	@SequenceGenerator(name="INFOCONTRATO_GENERATOR", sequenceName="TOSNINFO_CONTRATO_SEQ", schema="MIDAS", allocationSize=1)
	private Long id;
	
	@Column(name="APODERADO_AFIRME", nullable=false)
	private String apoderadoAfirme;
	
	@Column(name="SEGUNDO_APODERADO_AFIRME")
	private String segundoApoderadoAfirme;
	
	@Column(name="APODERADO_AFECTADO", nullable=false)
	private String apoderadoAfectado;
	
	@Column(name="CALLE_VENDEDOR", nullable=false)
	private String calleVendedor;
	
	@Column(name="NUMERO_VENDEDOR", nullable=false)
	private Integer numeroVendedor;
	
	@Column(name="COLONIA_VENDEDOR", nullable=false)
	private String coloniaVendedor;
	
	@Column(name="CP_VENDEDOR", nullable=false)
	private String cpVendedor;
	
	@Column(name="CIUDAD_VENDEDOR", nullable=false)
	private String ciudadVendedor;
	
	@Column(name="ESTADO_VENDEDOR", nullable=false)
	private String estadoVendedor;
	
	@Column(name="DOCUMENTACION", nullable= false)
	private String documentacion;
	
    @OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_INDEMNIZACION", nullable=false, referencedColumnName="ID")
    private IndemnizacionSiniestro indemnizacion;

	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the apoderadoAfirme
	 */
	public String getApoderadoAfirme() {
		return apoderadoAfirme;
	}

	/**
	 * @param apoderadoAfirme the apoderadoAfirme to set
	 */
	public void setApoderadoAfirme(String apoderadoAfirme) {
		this.apoderadoAfirme = apoderadoAfirme;
	}

	/**
	 * @return the segundoApoderadoAfirme
	 */
	public String getSegundoApoderadoAfirme() {
		return segundoApoderadoAfirme;
	}

	/**
	 * @param segundoApoderadoAfirme the segundoApoderadoAfirme to set
	 */
	public void setSegundoApoderadoAfirme(String segundoApoderadoAfirme) {
		this.segundoApoderadoAfirme = segundoApoderadoAfirme;
	}

	/**
	 * @return the apoderadoAfectado
	 */
	public String getApoderadoAfectado() {
		return apoderadoAfectado;
	}

	/**
	 * @param apoderadoAfectado the apoderadoAfectado to set
	 */
	public void setApoderadoAfectado(String apoderadoAfectado) {
		this.apoderadoAfectado = apoderadoAfectado;
	}

	/**
	 * @return the calleVendedor
	 */
	public String getCalleVendedor() {
		return calleVendedor;
	}

	/**
	 * @param calleVendedor the calleVendedor to set
	 */
	public void setCalleVendedor(String calleVendedor) {
		this.calleVendedor = calleVendedor;
	}

	/**
	 * @return the numeroVendedor
	 */
	public Integer getNumeroVendedor() {
		return numeroVendedor;
	}

	/**
	 * @param numeroVendedor the numeroVendedor to set
	 */
	public void setNumeroVendedor(Integer numeroVendedor) {
		this.numeroVendedor = numeroVendedor;
	}

	/**
	 * @return the coloniaVendedor
	 */
	public String getColoniaVendedor() {
		return coloniaVendedor;
	}

	/**
	 * @param coloniaVendedor the coloniaVendedor to set
	 */
	public void setColoniaVendedor(String coloniaVendedor) {
		this.coloniaVendedor = coloniaVendedor;
	}

	/**
	 * @return the cpVendedor
	 */
	public String getCpVendedor() {
		return cpVendedor;
	}

	/**
	 * @param cpVendedor the cpVendedor to set
	 */
	public void setCpVendedor(String cpVendedor) {
		this.cpVendedor = cpVendedor;
	}

	/**
	 * @return the ciudadVendedor
	 */
	public String getCiudadVendedor() {
		return ciudadVendedor;
	}

	/**
	 * @param ciudadVendedor the ciudadVendedor to set
	 */
	public void setCiudadVendedor(String ciudadVendedor) {
		this.ciudadVendedor = ciudadVendedor;
	}

	/**
	 * @return the documentacion
	 */
	public String getDocumentacion() {
		return documentacion;
	}

	/**
	 * @param documentacion the documentacion to set
	 */
	public void setDocumentacion(String documentacion) {
		this.documentacion = documentacion;
	}
	
	/**
	 * @return the indemnizacion
	 */
	public IndemnizacionSiniestro getIndemnizacion() {
		return indemnizacion;
	}

	/**
	 * @param indemnizacion the indemnizacion to set
	 */
	public void setIndemnizacion(IndemnizacionSiniestro indemnizacion) {
		this.indemnizacion = indemnizacion;
	}

	/**
	 * @return the estadoVendedor
	 */
	public String getEstadoVendedor() {
		return estadoVendedor;
	}

	/**
	 * @param estadoVendedor the estadoVendedor to set
	 */
	public void setEstadoVendedor(String estadoVendedor) {
		this.estadoVendedor = estadoVendedor;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	
}
