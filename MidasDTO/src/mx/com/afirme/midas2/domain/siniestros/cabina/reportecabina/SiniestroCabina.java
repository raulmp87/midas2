package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.MidasAbstracto;

@Entity(name = "SiniestroCabina")
@Table(name = "TOSINIESTROCABINA", schema = "MIDAS")
public class SiniestroCabina extends MidasAbstracto{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1046162607044783414L;
	
	@Id
	@SequenceGenerator(name = "TOSINIESTROCABINA_SEQ_GENERADOR",allocationSize = 1, sequenceName = "IDTOSINIESTROCABINA_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TOSINIESTROCABINA_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id;
	
	@OneToOne
	@JoinColumn(name = "REPORTECABINA_ID", referencedColumnName = "id")
	private ReporteCabina reporteCabina;
	
	@Column(name = "ANIO_REPORTE")
	private String anioReporte;
	
	@Column(name = "CLAVE_OFICINA")
	private String claveOficina;
	
	@Column(name = "CONSECUTIVO_REPORTE")
	private String consecutivoReporte;
	
	@Transient
	private String numeroSiniestro;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ReporteCabina getReporteCabina() {
		return reporteCabina;
	}

	public void setReporteCabina(ReporteCabina reporteCabina) {
		this.reporteCabina = reporteCabina;
	}

	public String getAnioReporte() {
		return anioReporte;
	}

	public void setAnioReporte(String anioReporte) {
		this.anioReporte = anioReporte;
	}

	public String getClaveOficina() {
		return claveOficina;
	}

	public void setClaveOficina(String claveOficina) {
		this.claveOficina = claveOficina;
	}

	public String getConsecutivoReporte() {
		return consecutivoReporte;
	}

	public void setConsecutivoReporte(String consecutivoReporte) {
		this.consecutivoReporte = consecutivoReporte;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {		
		return id;
	}

	@Override
	public String getValue() {
		return getNumeroSiniestro(); 
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.getId();
	}
	
	public String getNumeroSiniestro(){		
		setNumeroSiniestro();
		return numeroSiniestro;
	}

	public void setNumeroSiniestro(){
		StringBuilder builder = new StringBuilder(this.claveOficina != null ? claveOficina : "");
		builder.append("-");
		builder.append(this.consecutivoReporte != null ? this.consecutivoReporte : "");
		builder.append("-");
		builder.append(this.anioReporte != null ? this.anioReporte : "");
		this.numeroSiniestro = builder.toString();
	}
}
