package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.domain.MidasAbstracto;


@Entity
@Table(name = "TCCONFCALCULOCOBERTURASIN", schema = "MIDAS")
public class ConfiguracionCalculoCoberturaSiniestro extends MidasAbstracto {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDCONFCALCCOBERSIN_SEQ")
	@SequenceGenerator(name = "IDCONFCALCCOBERSIN_SEQ", schema = "MIDAS", sequenceName = "IDCONFCALCCOBERSIN_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "CLAVE_TIPO_CALCULO")
	private String cveTipoCalculoCobertura;
	
	@Column(name = "CLAVE_SUB_TIPO_CALCULO")
	private String cveSubTipoCalculoCobertura;
	
	@Column(name = "VALOR_SUMA_ESTIMADA")
	private String valorSumaEstimada;
	
	@Column(name = "VALOR_SUMA_PASE")
	private String sumaAseguradaPaseAtencion;
	
	@Column(name = "NOMBRE_COBERTURA")
	private String nombreCobertura;
	
	@Column(name = "EVENTO")
	private String evento;
	
	@Column(name = "TIPO_ESTIMACION")
	private String tipoEstimacion;
	
	@Column(name = "TIPO_CONFIGURACION", length = 3)
	private String tipoConfiguracion;
	
	@Column(name = "CUBRE")
	private String cubre = ValoresCubre.ASEGURADO.toString();

	public static enum ValoresCubre{
		ASEGURADO, TERCERO
	}
	
	public ConfiguracionCalculoCoberturaSiniestro(){

	}

	public void finalize() throws Throwable {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCveTipoCalculoCobertura() {
		return cveTipoCalculoCobertura;
	}

	public void setCveTipoCalculoCobertura(String cveTipoCalculoCobertura) {
		this.cveTipoCalculoCobertura = cveTipoCalculoCobertura;
	}

	public String getCveSubTipoCalculoCobertura() {
		return cveSubTipoCalculoCobertura;
	}

	public void setCveSubTipoCalculoCobertura(String cveSubTipoCalculoCobertura) {
		this.cveSubTipoCalculoCobertura = cveSubTipoCalculoCobertura;
	}

	public String getValorSumaEstimada() {
		return valorSumaEstimada;
	}

	public void setValorSumaEstimada(String valorSumaEstimada) {
		this.valorSumaEstimada = valorSumaEstimada;
	}

	public String getSumaAseguradaPaseAtencion() {
		return sumaAseguradaPaseAtencion;
	}

	public void setSumaAseguradaPaseAtencion(String sumaAseguradaPaseAtencion) {
		this.sumaAseguradaPaseAtencion = sumaAseguradaPaseAtencion;
	}

	public String getNombreCobertura() {
		return nombreCobertura;
	}

	public void setNombreCobertura(String nombreCobertura) {
		this.nombreCobertura = nombreCobertura;
	}

	public String getEvento() {
		return evento;
	}

	public void setEvento(String evento) {
		this.evento = evento;
	}

	public String getTipoEstimacion() {
		return tipoEstimacion;
	}

	public void setTipoEstimacion(String tipoEstimacion) {
		this.tipoEstimacion = tipoEstimacion;
	}

	public String getTipoConfiguracion() {
		return tipoConfiguracion;
	}

	public void setTipoConfiguracion(String tipoConfiguracion) {
		this.tipoConfiguracion = tipoConfiguracion;
	}

	/**
	 * @return the cubre
	 */
	public String getCubre() {
		return cubre;
	}

	/**
	 * @param cubre the cubre to set
	 */
	public void setCubre(String cubre) {
		this.cubre = cubre;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}