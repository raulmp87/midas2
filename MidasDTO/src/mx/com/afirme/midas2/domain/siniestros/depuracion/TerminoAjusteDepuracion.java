package mx.com.afirme.midas2.domain.siniestros.depuracion;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TerminoAjusteDepuracion {
  @Column(name="TERMINO_AJUSTE")
  private String terminoAjuste;

	public String getTerminoAjuste() {
		return terminoAjuste;
	}
	
	public void setTerminoAjuste(String terminoAjuste) {
		this.terminoAjuste = terminoAjuste;
	}
}
