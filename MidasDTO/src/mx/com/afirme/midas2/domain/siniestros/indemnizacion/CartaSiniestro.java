package mx.com.afirme.midas2.domain.siniestros.indemnizacion;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;

@Entity
@Table(name="TOCARTASINIESTRO", schema="MIDAS")
public class CartaSiniestro extends MidasAbstracto implements Entidad{

	private static final long serialVersionUID = -4407353509833571485L;

	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CARTASINIESTRO_GENERATOR")
	@SequenceGenerator(name="CARTASINIESTRO_GENERATOR", sequenceName="IDTOCARTASINIESTRO_SEQ", schema="MIDAS", allocationSize=1)
	private Long id;
	
	@Column(name="DESCRIPCION_SOLICITUD")
	private String descripcionSol;
	
	@Column(name="DESC_SOL_COMPLEMENTARIA")
	private String descSolComplementaria;
	
	@Column(name="TIPO")
	private String tipo;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_RECLAMACION")
	private Date fechaReclamacion;
	
	@Column(name="RESULTADO_ACC")
	private String resultadoAcc;
	
	@Column(name="LUGAR")
	private String lugar;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CARTA")
	private Date fechaCarta;
	
	@Column(name="NOMBRE_ASEGURADO")
	private String nombreAsegurado;
	
	@Column(name="NOMBRE_CONDUCTOR")
	private String nombreConductor;
	
	@Column(name="NOMBRE_TERCERO")
	private String nombreTercero;
	
	@Column(name="DOMICILIO_TERCERO")
	private String domicilioTercero;
	
	@Column(name="TEL_TERCERO")
	private String telefonoTercero;
	
	@Column(name="DOMICILIO_ASEGURADO")
	private String domicilioAsegurado;
	
	@Column(name="NOMBRE_FACTURA")
	private String nombreFactura;
	
	@Column(name="NUMERO_FACTURA")
	private String numeroFactura;
	
	@Column(name="DETALLE_FACTURA")
	private String detalleFactura;
	
	@Column(name="NOMBRE_COPIA_FACTURA")
	private String nombreCopiaFactura;
	
	@Column(name="NUMERO_COPIA_FACTURA")
	private String numeroCopiaFactura;
	
	@Column(name="DETALLE_COPIA_FACTURA")
	private String detalleCopiaFactura;
	
	@Column(name="CONST_BAJA_PLACAS")
	private String constanciaBajaPlacas;
	
	@Column(name="FOLIO_BAJA_PLACAS")
	private String folioBajaPlacas;
	
	@Column(name="CONST_PAGO_TENENCIA")
	private String constanciaPagoTenencia;
	
	@Column(name="FOLIO_ALTA_TENENCIA")
	private String folioAltaTenencia;
	
	@Column(name="FECHA_TENENCIA_INI")
	private String fechaTenenciaInicial;
	
	@Column(name="FECHA_TENENCIA_FIN")
	private String fechaTenenciaFin;
	
	@Column(name="PERSONA_AQUIEN_SEDIRIGE")
	private String personaAQuienSeDirige;
	
	/**
	 * Este campo se usa en la carta de entrega 
	 * de documentos para el Estado de la tenencia
	 * y en la carta de baja de placas para el 
	 * Estado de las Placas
	 */
	@Column(name="ESTADO_TENENCIA")
	private String estadoTenencia;
	
	@Column(name="LLAVES_ENTREGADAS")
	private String llavesEntregadas;
	
	@Column(name="NUM_AVERIGUACION")
	private String numeroAveriguacion;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="INDEMNIZACION_ID")
	private IndemnizacionSiniestro indemnizacion;
	
	@OneToMany(mappedBy="cartaSiniestro", fetch = FetchType.LAZY)
	private List<CartaSiniestroFactura> facturas;
	
	@Column(name = "INVESTIGACION")
	private Integer investigacion; //Para la pantalla de formato fechas PT en la sección de primas pendientes, 1  = si , 0 = no.
	
	@Column(name = "IMPORTE_PRIMA_PEND")
	private BigDecimal importePrimaPend; //importe de primas pendientes de la carta Formato Fechas PT, en la sección de primas pendientes. Este dato no es obligatorio
	
	@Column(name = "CODIGO_USUARIO_FINANZAS")
	private String usuarioFinanzas;//código de usuario de finanzas que revisó la carta Formato de fechas PT, se usa en la sección de primas pendientes.
	
	@Column(name = "TIPO_PERDIDA")
	private String tipoPerdida; //para el campo pérdida total y robo total de la pantalla de formato fechas PT, en la sección de primas pendientes. 
	
	@Column(name = "COMENTARIOS")
	private String comentarios;
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the descripcionSol
	 */
	public String getDescripcionSol() {
		return descripcionSol;
	}

	/**
	 * @param descripcionSol the descripcionSol to set
	 */
	public void setDescripcionSol(String descripcionSol) {
		this.descripcionSol = descripcionSol;
	}

	/**
	 * @return the descSolComplementaria
	 */
	public String getDescSolComplementaria() {
		return descSolComplementaria;
	}

	/**
	 * @param descSolComplementaria the descSolComplementaria to set
	 */
	public void setDescSolComplementaria(String descSolComplementaria) {
		this.descSolComplementaria = descSolComplementaria;
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the investigacion
	 */
	public Integer getInvestigacion() {
		return investigacion;
	}

	/**
	 * @param investigacion the investigacion to set
	 */
	public void setInvestigacion(Integer investigacion) {
		this.investigacion = investigacion;
	}

	/**
	 * @return the importePrimaPend
	 */
	public BigDecimal getImportePrimaPend() {
		return importePrimaPend;
	}

	/**
	 * @param importePrimaPend the importePrimaPend to set
	 */
	public void setImportePrimaPend(BigDecimal importePrimaPend) {
		this.importePrimaPend = importePrimaPend;
	}

	/**
	 * @return the usuarioFinanzas
	 */
	public String getUsuarioFinanzas() {
		return usuarioFinanzas;
	}

	/**
	 * @param usuarioFinanzas the usuarioFinanzas to set
	 */
	public void setUsuarioFinanzas(String usuarioFinanzas) {
		this.usuarioFinanzas = usuarioFinanzas;
	}

	/**
	 * @return the tipoPerdida
	 */
	public String getTipoPerdida() {
		return tipoPerdida;
	}

	/**
	 * @param tipoPerdida the tipoPerdida to set
	 */
	public void setTipoPerdida(String tipoPerdida) {
		this.tipoPerdida = tipoPerdida;
	}

	/**
	 * @return the comentarios
	 */
	public String getComentarios() {
		return comentarios;
	}

	/**
	 * @param comentarios the comentarios to set
	 */
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	/**
	 * @return the fechaReclamacion
	 */
	public Date getFechaReclamacion() {
		return fechaReclamacion;
	}

	/**
	 * @param fechaReclamacion the fechaReclamacion to set
	 */
	public void setFechaReclamacion(Date fechaReclamacion) {
		this.fechaReclamacion = fechaReclamacion;
	}

	/**
	 * @return the resultadoAcc
	 */
	public String getResultadoAcc() {
		return resultadoAcc;
	}

	/**
	 * @param resultadoAcc the resultadoAcc to set
	 */
	public void setResultadoAcc(String resultadoAcc) {
		this.resultadoAcc = resultadoAcc;
	}

	/**
	 * @return the lugar
	 */
	public String getLugar() {
		return lugar;
	}

	/**
	 * @param lugar the lugar to set
	 */
	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	/**
	 * @return the fechaCarta
	 */
	public Date getFechaCarta() {
		return fechaCarta;
	}

	/**
	 * @param fechaCarta the fechaCarta to set
	 */
	public void setFechaCarta(Date fechaCarta) {
		this.fechaCarta = fechaCarta;
	}

	/**
	 * @return the nombreAsegurado
	 */
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}

	/**
	 * @param nombreAsegurado the nombreAsegurado to set
	 */
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}

	/**
	 * @return the nombreConductor
	 */
	public String getNombreConductor() {
		return nombreConductor;
	}

	/**
	 * @param nombreConductor the nombreConductor to set
	 */
	public void setNombreConductor(String nombreConductor) {
		this.nombreConductor = nombreConductor;
	}

	/**
	 * @return the nombreTercero
	 */
	public String getNombreTercero() {
		return nombreTercero;
	}

	/**
	 * @param nombreTercero the nombreTercero to set
	 */
	public void setNombreTercero(String nombreTercero) {
		this.nombreTercero = nombreTercero;
	}

	/**
	 * @return the domicilioTercero
	 */
	public String getDomicilioTercero() {
		return domicilioTercero;
	}

	/**
	 * @param domicilioTercero the domicilioTercero to set
	 */
	public void setDomicilioTercero(String domicilioTercero) {
		this.domicilioTercero = domicilioTercero;
	}

	/**
	 * @return the telefonoTercero
	 */
	public String getTelefonoTercero() {
		return telefonoTercero;
	}

	/**
	 * @param telefonoTercero the telefonoTercero to set
	 */
	public void setTelefonoTercero(String telefonoTercero) {
		this.telefonoTercero = telefonoTercero;
	}

	/**
	 * @return the domicilioAsegurado
	 */
	public String getDomicilioAsegurado() {
		return domicilioAsegurado;
	}

	/**
	 * @param domicilioAsegurado the domicilioAsegurado to set
	 */
	public void setDomicilioAsegurado(String domicilioAsegurado) {
		this.domicilioAsegurado = domicilioAsegurado;
	}

	/**
	 * @return the nombreFactura
	 */
	public String getNombreFactura() {
		return nombreFactura;
	}

	/**
	 * @param nombreFactura the nombreFactura to set
	 */
	public void setNombreFactura(String nombreFactura) {
		this.nombreFactura = nombreFactura;
	}

	/**
	 * @return the numeroFactrua
	 */
	public String getNumeroFactura() {
		return numeroFactura;
	}

	/**
	 * @param numeroFactrua the numeroFactrua to set
	 */
	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	/**
	 * @return the detalleFactura
	 */
	public String getDetalleFactura() {
		return detalleFactura;
	}

	/**
	 * @param detalleFactura the detalleFactura to set
	 */
	public void setDetalleFactura(String detalleFactura) {
		this.detalleFactura = detalleFactura;
	}

	/**
	 * @return the nombreCopiaFactura
	 */
	public String getNombreCopiaFactura() {
		return nombreCopiaFactura;
	}

	/**
	 * @param nombreCopiaFactura the nombreCopiaFactura to set
	 */
	public void setNombreCopiaFactura(String nombreCopiaFactura) {
		this.nombreCopiaFactura = nombreCopiaFactura;
	}

	/**
	 * @return the numeroCopiaFactura
	 */
	public String getNumeroCopiaFactura() {
		return numeroCopiaFactura;
	}

	/**
	 * @param numeroCopiaFactura the numeroCopiaFactura to set
	 */
	public void setNumeroCopiaFactura(String numeroCopiaFactura) {
		this.numeroCopiaFactura = numeroCopiaFactura;
	}

	/**
	 * @return the folioBajaPlacas
	 */
	public String getFolioBajaPlacas() {
		return folioBajaPlacas;
	}

	/**
	 * @param folioBajaPlacas the folioBajaPlacas to set
	 */
	public void setFolioBajaPlacas(String folioBajaPlacas) {
		this.folioBajaPlacas = folioBajaPlacas;
	}

	/**
	 * @return the constanciaPagoTenencia
	 */
	public String getConstanciaPagoTenencia() {
		return constanciaPagoTenencia;
	}

	/**
	 * @param constanciaPagoTenencia the constanciaPagoTenencia to set
	 */
	public void setConstanciaPagoTenencia(String constanciaPagoTenencia) {
		this.constanciaPagoTenencia = constanciaPagoTenencia;
	}

	/**
	 * @return the folioAltaTenencia
	 */
	public String getFolioAltaTenencia() {
		return folioAltaTenencia;
	}

	/**
	 * @param folioAltaTenencia the folioAltaTenencia to set
	 */
	public void setFolioAltaTenencia(String folioAltaTenencia) {
		this.folioAltaTenencia = folioAltaTenencia;
	}

	/**
	 * @return the fechaTenenciaInicial
	 */
	public String getFechaTenenciaInicial() {
		return fechaTenenciaInicial;
	}

	/**
	 * @param fechaTenenciaInicial the fechaTenenciaInicial to set
	 */
	public void setFechaTenenciaInicial(String fechaTenenciaInicial) {
		this.fechaTenenciaInicial = fechaTenenciaInicial;
	}

	/**
	 * @return the fechaTenenciaFin
	 */
	public String getFechaTenenciaFin() {
		return fechaTenenciaFin;
	}

	/**
	 * @param fechaTenenciaFin the fechaTenenciaFin to set
	 */
	public void setFechaTenenciaFin(String fechaTenenciaFin) {
		this.fechaTenenciaFin = fechaTenenciaFin;
	}

	/**
	 * @return the estadoTenencia
	 */
	public String getEstadoTenencia() {
		return estadoTenencia;
	}

	/**
	 * @param estadoTenencia the estadoTenencia to set
	 */
	public void setEstadoTenencia(String estadoTenencia) {
		this.estadoTenencia = estadoTenencia;
	}

	/**
	 * @return the llavesEntregadas
	 */
	public String getLlavesEntregadas() {
		return llavesEntregadas;
	}

	/**
	 * @param llavesEntregadas the llavesEntregadas to set
	 */
	public void setLlavesEntregadas(String llavesEntregadas) {
		this.llavesEntregadas = llavesEntregadas;
	}

	/**
	 * @return the numeroAveriguacion
	 */
	public String getNumeroAveriguacion() {
		return numeroAveriguacion;
	}

	/**
	 * @param numeroAveriguacion the numeroAveriguacion to set
	 */
	public void setNumeroAveriguacion(String numeroAveriguacion) {
		this.numeroAveriguacion = numeroAveriguacion;
	}

	/**
	 * @return the detalleCopiaFactura
	 */
	public String getDetalleCopiaFactura() {
		return detalleCopiaFactura;
	}

	/**
	 * @param detalleCopiaFactura the detalleCopiaFactura to set
	 */
	public void setDetalleCopiaFactura(String detalleCopiaFactura) {
		this.detalleCopiaFactura = detalleCopiaFactura;
	}

	/**
	 * @return the constanciaBajaPlacas
	 */
	public String getConstanciaBajaPlacas() {
		return constanciaBajaPlacas;
	}

	/**
	 * @param constanciaBajaPlacas the constanciaBajaPlacas to set
	 */
	public void setConstanciaBajaPlacas(String constanciaBajaPlacas) {
		this.constanciaBajaPlacas = constanciaBajaPlacas;
	}

	/**
	 * @return the indemnizacion
	 */
	public IndemnizacionSiniestro getIndemnizacion() {
		return indemnizacion;
	}

	/**
	 * @param indemnizacion the indemnizacion to set
	 */
	public void setIndemnizacion(IndemnizacionSiniestro indemnizacion) {
		this.indemnizacion = indemnizacion;
	}

	/**
	 * @return the personaAQuienSeDirige
	 */
	public String getPersonaAQuienSeDirige() {
		return personaAQuienSeDirige;
	}

	/**
	 * @param personaAQuienSeDirige the personaAQuienSeDirige to set
	 */
	public void setPersonaAQuienSeDirige(String personaAQuienSeDirige) {
		this.personaAQuienSeDirige = personaAQuienSeDirige;
	}

	/**
	 * @return the facturas
	 */
	public List<CartaSiniestroFactura> getFacturas() {
		return facturas;
	}

	/**
	 * @param facturas the facturas to set
	 */
	public void setFacturas(List<CartaSiniestroFactura> facturas) {
		this.facturas = facturas;
	}

}