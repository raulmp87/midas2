package mx.com.afirme.midas2.domain.siniestros.expedientejuridico;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;


/**
 * @author Israel
 * @version 1.0
 * @created 06-ago.-2015 10:29:08 a. m.
 */
@Entity
@Table(name = "TOSN_EXPEDIENTE_JURIDICO", schema = "MIDAS")
public class ExpedienteJuridico extends MidasAbstracto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id 
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOSN_EXPEDIENTE_JURIDICO_SEQ")
	@SequenceGenerator(name="TOSN_EXPEDIENTE_JURIDICO_SEQ", schema="MIDAS", sequenceName="TOSN_EXPEDIENTE_JURIDICO_SEQ", allocationSize=1)	
    @Column(name="ID", unique=true, nullable=false, precision=8)
	private Long id;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CANCELACION")
	private Date fechaCancelacion;
	
	@Column(name="NUMERO_SINIESTRO")
	private String numeroSiniestro;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_TURNO")
	private Date fechaTurno;
	
	@Column(name="ESTATUS_JURIDICO")
	private String estatusJuridico;
	
	@Column(name="EXPED_PROVEEDOR")
	private String expedienteProveedor;
	
	@Column(name="MOTIVO_TURNO")
	private String motivoTurno;
	
	@Column(name="VEHICULO_DETENIDO")
	private Short vehiculoDetenido;
	
	@Column(name="ESTATUS_VEHICULO")
	private String estatusVehiculo;
	
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_LIBERACION_VEHICULO")
	private Date fechaLiberacionVehiculo;
	
	
	@Column(name="TIPO_CONCLUSION")
	private String tipoConclusion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CONCLUSION_LEGAL")
	private Date fechaConclusionLegal;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CONCLUSION_AFIRME")
	private Date fechaConclusionAfirme;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ABOGADO", referencedColumnName="ID")
	@JoinFetch(JoinFetchType.OUTER)
	private ServicioSiniestro abogado;
	
	@OneToMany(fetch=FetchType.LAZY,  mappedBy = "expedienteJuridico")
	private List<ComentarioJuridico> comentarios;
	
	@Transient
	private String estatusJuridicoDesc;
	
	@Transient
	private String tipoAbogadoDesc;
	
	@Transient
	private String estatusVehiculoDesc;
	
	@Transient
	private String vehiculoDetenidoDesc;
	
	@Transient
	private String motivoTurnoDesc;
	
	@Transient
	private String tipoConclusionDesc;
	
	
	
	public static enum EstatusJuridico{
		TRAMITE("TRAM"),
		CONSIGNADO("CONSIG"),
		EN_SENTENCIA("SENT"),
		TERMINADO("TERM"),
		CANCELADO("CANC");
		
		
		private String value;
		
		private EstatusJuridico(String value){
			this.value=value;
		}
		
		public String getValue(){
			return value;
		}
	}
	
	
	public static enum MotivoTurno{
		DEFINDEM("DEFINDEM"),
		LIBVEHICU("LIBVEHICU"),
		RECDANIO("RECDANIO"),
		LIB_VEHI_Y_REC_DAN("LVYRD"),
		LIB_VEHI_Y_DEF_IND("LVYDI");
		
		
		private String value;
		
		private MotivoTurno(String value){
			this.value=value;
		}
		
		public String getValue(){
			return value;
		}
	}
	
	
	public static enum TipoConclusion{
		NACOOP("NACOOP"),
		REORDAFI("REORDAFI"),
		REORDNA("REORDNA"),
		COBEFECA("COBEFECA"),
		DIMORDC("DIMORDC"),
		DIMORDT("DIMORDT"),
		PAGEFECT("PAGEFECT"),
		CASTIGO("CASTIGO"),
		PREESCR("PREESCR"),
		RECHAZO("RECHAZO"),
		CONCQSD("CONCQSD"),
		COBEFECNA("COBEFECNA"),
		NACONVTER("NACONVTER"),
		RESFALELE("RESFALELE"),
		UNILVYC("UNILVYC");
		
		
		
		private String value;
		
		private TipoConclusion(String value){
			this.value=value;
		}
		
		public String getValue(){
			return value;
		}
	}
	
	
	public static enum EstatusVehiculo{
		DETENIDO("DET"),
		LIBERADO("LIB");
		
		private String value;
		
		private EstatusVehiculo(String value){
			this.value=value;
		}
		
		public String getValue(){
			return value;
		}
	}
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFechaCancelacion() {
		return fechaCancelacion;
	}

	public void setFechaCancelacion(Date fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}

	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}

	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}

	public Date getFechaTurno() {
		return fechaTurno;
	}

	public void setFechaTurno(Date fechaTurno) {
		this.fechaTurno = fechaTurno;
	}

	public String getEstatusJuridico() {
		return estatusJuridico;
	}

	public void setEstatusJuridico(String estatusJuridico) {
		this.estatusJuridico = estatusJuridico;
	}

	public String getMotivoTurno() {
		return motivoTurno;
	}

	public void setMotivoTurno(String motivoTurno) {
		this.motivoTurno = motivoTurno;
	}

	

	public Short getVehiculoDetenido() {
		return vehiculoDetenido;
	}

	public void setVehiculoDetenido(Short vehiculoDetenido) {
		this.vehiculoDetenido = vehiculoDetenido;
	}

	public String getEstatusVehiculo() {
		return estatusVehiculo;
	}

	public void setEstatusVehiculo(String estatusVehiculo) {
		this.estatusVehiculo = estatusVehiculo;
	}

	public Date getFechaLiberacionVehiculo() {
		return fechaLiberacionVehiculo;
	}

	public void setFechaLiberacionVehiculo(Date fechaLiberacionVehiculo) {
		this.fechaLiberacionVehiculo = fechaLiberacionVehiculo;
	}

	public ServicioSiniestro getAbogado() {
		return abogado;
	}

	public void setAbogado(ServicioSiniestro abogado) {
		this.abogado = abogado;
	}

	public String getTipoConclusion() {
		return tipoConclusion;
	}

	public void setTipoConclusion(String tipoConclusion) {
		this.tipoConclusion = tipoConclusion;
	}

	public Date getFechaConclusionLegal() {
		return fechaConclusionLegal;
	}

	public void setFechaConclusionLegal(Date fechaConclusionLegal) {
		this.fechaConclusionLegal = fechaConclusionLegal;
	}

	public Date getFechaConclusionAfirme() {
		return fechaConclusionAfirme;
	}

	public void setFechaConclusionAfirme(Date fechaConclusionAfirme) {
		this.fechaConclusionAfirme = fechaConclusionAfirme;
	}

	public List<ComentarioJuridico> getComentarios() {
		return comentarios;
	}

	public void setComentarios(List<ComentarioJuridico> comentarios) {
		this.comentarios = comentarios;
	}
	
	

	public String getEstatusJuridicoDesc() {
		return estatusJuridicoDesc;
	}

	public void setEstatusJuridicoDesc(String estatusJuridicoDesc) {
		this.estatusJuridicoDesc = estatusJuridicoDesc;
	}
	
	public String getExpedienteProveedor() {
		return expedienteProveedor;
	}

	public void setExpedienteProveedor(String expedienteProveedor) {
		this.expedienteProveedor = expedienteProveedor;
	}
	
	public String getTipoAbogadoDesc() {
		return tipoAbogadoDesc;
	}

	public void setTipoAbogadoDesc(String tipoAbogadoDesc) {
		this.tipoAbogadoDesc = tipoAbogadoDesc;
	}
	
	public String getEstatusVehiculoDesc() {
		return estatusVehiculoDesc;
	}

	public void setEstatusVehiculoDesc(String estatusVehiculoDesc) {
		this.estatusVehiculoDesc = estatusVehiculoDesc;
	}
	
	public String getVehiculoDetenidoDesc() {
		return vehiculoDetenidoDesc;
	}

	public void setVehiculoDetenidoDesc(String vehiculoDetenidoDesc) {
		this.vehiculoDetenidoDesc = vehiculoDetenidoDesc;
	}

	public String getMotivoTurnoDesc() {
		return motivoTurnoDesc;
	}

	public void setMotivoTurnoDesc(String motivoTurnoDesc) {
		this.motivoTurnoDesc = motivoTurnoDesc;
	}
	
	

	public String getTipoConclusionDesc() {
		return tipoConclusionDesc;
	}

	public void setTipoConclusionDesc(String tipoConclusionDesc) {
		this.tipoConclusionDesc = tipoConclusionDesc;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}