package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.domain.MidasAbstracto;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

@Entity(name = "IncisoReporteCabina")
@Table(name = "TOINCISOREPORTECABINA", schema = "MIDAS")
public class IncisoReporteCabina extends MidasAbstracto{

	private static final long serialVersionUID = -3405055693662301440L;
	
	public static enum EstatusVigenciaInciso{
		VIGENTE, NOVIGENTE, CANCELADO;
	}
	
	
	@Id
	@SequenceGenerator(name = "IDINCISOREPORTECABINA_SEQ_GENERADOR",allocationSize = 1, sequenceName = "TOINCISOREPORTECABINA_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "IDINCISOREPORTECABINA_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id;
	
	@Column(name="NOMBREASEGURADO", length = 100)
	private String nombreAsegurado;

	
	@Column(name="NUMEROINCISO", nullable = false)
	private Integer numeroInciso;
	
	@OneToOne(fetch=FetchType.LAZY , cascade = CascadeType.PERSIST)
	@JoinColumn(name="SECCIONREPORTE_ID")
	private SeccionReporteCabina seccionReporteCabina;
	
	@Column(name="NO_APLICA_DEPURACION")
	private Boolean noAplicaDepuracion;	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="VALIDFROM")
	private Date validFrom;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="RECORDFROM")
	private Date recordFrom;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="VALIDTO")
	private Date validTo;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="RECORDTO")
	private Date recordTo;
	
	@Column(name="ESTATUS_INCISO")
	private String estatusInciso;
	
	@Column(name="SITUACION_COBRANZA")
	private String situacionCobranza;
	
	@OneToOne(fetch=FetchType.LAZY, mappedBy="incisoReporteCabina", cascade = CascadeType.ALL)
	private AutoIncisoReporteCabina autoIncisoReporteCabina;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="incisoReporteCabina", cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinFetch(JoinFetchType.OUTER)
	private List<CoberturaReporteCabina> coberturaReporteCabina;
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nombreAsegurado
	 */
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}

	/**
	 * @param nombreAsegurado the nombreAsegurado to set
	 */
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}

	/**
	 * @return the numeroInciso
	 */
	public Integer getNumeroInciso() {
		return numeroInciso;
	}

	/**
	 * @param numeroInciso the numeroInciso to set
	 */
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	/**
	 * @param seccionReporteCabina the seccionReporteCabina to set
	 */
	public void setSeccionReporteCabina(SeccionReporteCabina seccionReporteCabina) {
		this.seccionReporteCabina = seccionReporteCabina;
	}

	/**
	 * @return the seccionReporteCabina
	 */
	public SeccionReporteCabina getSeccionReporteCabina() {
		return seccionReporteCabina;
	}
	
	/**
	 * @param autoIncisoReporteCabina the autoIncisoReporteCabina to set
	 */
	public void setAutoIncisoReporteCabina(AutoIncisoReporteCabina autoIncisoReporteCabina) {
		this.autoIncisoReporteCabina = autoIncisoReporteCabina;
	}

	/**
	 * @return the autoIncisoReporteCabina
	 */
	public AutoIncisoReporteCabina getAutoIncisoReporteCabina() {
		return autoIncisoReporteCabina;
	}

	public List<CoberturaReporteCabina> getCoberturaReporteCabina() {
		return coberturaReporteCabina;
	}

	public void setCoberturaReporteCabina(
			List<CoberturaReporteCabina> coberturaReporteCabina) {
		this.coberturaReporteCabina = coberturaReporteCabina;
	}

	@SuppressWarnings("unchecked")
	@Override
	public  Long  getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.getId();
	}
	
	public Boolean getNoAplicaDepuracion() {
		return noAplicaDepuracion;
	}

	public void setNoAplicaDepuracion(Boolean noAplicaDepuracion) {
		this.noAplicaDepuracion = noAplicaDepuracion;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getRecordFrom() {
		return recordFrom;
	}

	public void setRecordFrom(Date recordFrom) {
		this.recordFrom = recordFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public Date getRecordTo() {
		return recordTo;
	}

	public void setRecordTo(Date recordTo) {
		this.recordTo = recordTo;
	}

	public String getEstatusInciso() {
		return estatusInciso;
	}

	public void setEstatusInciso(String estatusInciso) {
		this.estatusInciso = estatusInciso;
	}

	public String getSituacionCobranza() {
		return situacionCobranza;
	}

	public void setSituacionCobranza(String situacionCobranza) {
		this.situacionCobranza = situacionCobranza;
	}
		

}
