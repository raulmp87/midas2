package mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name = "AdjuntoConfigNotificacionCabina")
@Table(name = "TOADJUNTOCONFIGNOTIFCABINA", schema = "MIDAS")
public class AdjuntoConfigNotificacionCabina implements Entidad  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8802350391765798217L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOADJCONFIGNOTIFCABINA_SEQ_ID_GENERATOR")
	@SequenceGenerator(name="TOADJCONFIGNOTIFCABINA_SEQ_ID_GENERATOR", schema="MIDAS", sequenceName="IDTOADJCONFIGNOTIFCABINA_SEQ", allocationSize=1)	
	@Column(name = "ID")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinFetch(JoinFetchType.INNER)
	@JoinColumn(name = "CONFIGNOTIFICACIONCABINA_ID", referencedColumnName = "id")
	private ConfiguracionNotificacionCabina confNotificacionCabina;
	
	@Column(name = "DESCRIPCION")
	private String descripcion;
	
	@Column(name = "TIPO_ARCHIVO")
	private String tipoArchivo;
	
	@Column(name = "ARCHIVO")
	private byte[] archivo;
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ConfiguracionNotificacionCabina getConfNotificacionCabina() {
		return confNotificacionCabina;
	}

	public void setConfNotificacionCabina(
			ConfiguracionNotificacionCabina confNotificacionCabina) {
		this.confNotificacionCabina = confNotificacionCabina;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public byte[] getArchivo() {
		return archivo;
	}

	public void setArchivo(byte[] archivo) {
		this.archivo = archivo;
	}

	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof AdjuntoConfigNotificacionCabina) {
			AdjuntoConfigNotificacionCabina adjConfigNotifiCabina = (AdjuntoConfigNotificacionCabina) object;
			equal = adjConfigNotifiCabina.getId().equals(this.id);
		}
		return equal;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	public String getTipoArchivo() {
		return tipoArchivo;
	}

	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}
	
	
}
