package mx.com.afirme.midas2.domain.siniestros.catalogo.conceptos;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import org.apache.bval.constraints.NotEmpty;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.TipoPrestadorServicio;
import mx.com.afirme.midas2.util.EnumUtil;

@Entity
@Table(name = "TOCONCEPTOTIPOPRESTADOR", schema = "MIDAS")
public class ConceptoTipoPrestador extends MidasAbstracto {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */

	@Id
	@SequenceGenerator(name = "TOCONCEPTOTIPOPRESTADOR_SEQ_GENERADOR",allocationSize = 1, sequenceName = "TOCONCEPTOTIPOPRESTADOR_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TOCONCEPTOTIPOPRESTADOR_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id;
	
	
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_CONCEPTO", referencedColumnName = "ID")
	private ConceptoAjuste conceptoAjuste;
	
	
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_TIPO_PRESTADOR", referencedColumnName = "ID")
	private TipoPrestadorServicio tipoPrestadorServicio;
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ConceptoAjuste getConceptoAjuste() {
		return conceptoAjuste;
	}

	public void setConceptoAjuste(ConceptoAjuste conceptoAjuste) {
		this.conceptoAjuste = conceptoAjuste;
	}

	public TipoPrestadorServicio getTipoPrestadorServicio() {
		return tipoPrestadorServicio;
	}

	public void setTipoPrestadorServicio(TipoPrestadorServicio tipoPrestadorServicio) {
		this.tipoPrestadorServicio = tipoPrestadorServicio;
	}

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}
