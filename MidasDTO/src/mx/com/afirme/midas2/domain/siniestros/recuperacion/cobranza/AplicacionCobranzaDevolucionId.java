package mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class AplicacionCobranzaDevolucionId implements Serializable{

	private static final long serialVersionUID = -1345598736282388708L;

	@Column(name="APLICACION_ID", nullable=false, updatable=false)
	private Long aplicacionId;
	
	@Column(name="DEVOLUCION_ID", nullable=false, updatable=false)
	private Long devolucionId;

	public Long getAplicacionId() {
		return aplicacionId;
	}

	public void setAplicacionId(Long aplicacionId) {
		this.aplicacionId = aplicacionId;
	}

	public Long getDevolucionId() {
		return devolucionId;
	}

	public void setDevolucionId(Long devolucionId) {
		this.devolucionId = devolucionId;
	}
		
}
