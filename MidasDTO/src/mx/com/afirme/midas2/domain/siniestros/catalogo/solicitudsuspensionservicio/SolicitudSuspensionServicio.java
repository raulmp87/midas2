/**
 * 
 */
package mx.com.afirme.midas2.domain.siniestros.catalogo.solicitudsuspensionservicio;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;


import mx.com.afirme.midas2.annotation.Exportable;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;



/**
 * @author simavera
 *
 */
@Entity(name = "SolicitudSuspensionServicio")
@Table(name = "TCSOLICITUDSUSPSERVICIO", schema = "MIDAS")
public class SolicitudSuspensionServicio extends MidasAbstracto implements Entidad,Serializable{

	private static final long serialVersionUID = 986973801832711090L;

	@Id
	@SequenceGenerator(name = "IDSOLICITUDSUSPSERVICIO_SEQ_GENERADOR",allocationSize = 1, sequenceName = "TCSOLICITUDSUSPSERVICIO_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "IDSOLICITUDSUSPSERVICIO_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id;
	
	@Column(name="estatus", nullable = false)
	@NotNull
	private Integer estatus;
	
	@Transient
	private String estatusName;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_INICIO", nullable = false, length = 7)
	@NotNull
	private Date fechaInicio;

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_FIN", nullable = false, length = 7)
	@NotNull
	private Date fechaFin;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CAMBIO_ESTATUS", length = 7)
	private Date fechaModEstatus;
	
	@Column(name = "MOTIVO", nullable = false, length = 500)
	@NotNull
	private String motivo;
	
	@Column(name = "NUMERO_SERIE", nullable = false, length = 30)
	@NotNull
	private String numeroSerie;
	
	@OneToOne(fetch=FetchType.LAZY )
	@JoinColumn(name = "OFICINA_ID")//, insertable=false, updatable=false
	@NotNull
	Oficina oficina;

	/**
	 * Constructor Default
	 */
	public SolicitudSuspensionServicio() {
		super();
	}	
	/**
	 * @param id
	 * @param estatus
	 * @param fechaInicio
	 * @param fechaFin
	 * @param fechaModEstatus
	 * @param motivo
	 * @param numeroSerie
	 */
	public SolicitudSuspensionServicio(Long id, Integer estatus,
			Date fechaInicio, Date fechaFin, Date fechaModEstatus,
			String motivo, String numeroSerie) {
		super();
		this.id = id;
		this.estatus = estatus;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.fechaModEstatus = fechaModEstatus;
		this.motivo = motivo;
		this.numeroSerie = numeroSerie;
	}
	

	/**
	 * @return the id
	 */
	@Exportable(columnName="No. Solicitud", columnOrder=0)
	public Long getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the estatus
	 */
	public Integer getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}
	/**
	 * @return the fechaInicio
	 */
	@Exportable(columnName="Fecha Inicio", format="dd/MM/yyyy", columnOrder=3)
	public Date getFechaInicio() {
		return fechaInicio;
	}
	/**
	 * @param fechaInicio the fechaInicio to set
	 */
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	/**
	 * @return the fechaFin
	 */
	@Exportable(columnName="Fecha Fin", format="dd/MM/yyyy", columnOrder=4)
	public Date getFechaFin() {
		return fechaFin;
	}
	/**
	 * @param fechaFin the fechaFin to set
	 */
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	/**
	 * @return the fechaModEstatus
	 */
	public Date getFechaModEstatus() {
		return fechaModEstatus;
	}
	/**
	 * @param fechaModEstatus the fechaModEstatus to set
	 */
	public void setFechaModEstatus(Date fechaModEstatus) {
		this.fechaModEstatus = fechaModEstatus;
	}
	/**
	 * @return the motivo
	 */
	public String getMotivo() {
		return motivo;
	}
	/**
	 * @param motivo the motivo to set
	 */
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	/**
	 * @return the numeroSerie
	 */
	@Exportable(columnName="Número de Serie", columnOrder=1)
	public String getNumeroSerie() {
		return numeroSerie;
	}
	/**
	 * @param numeroSerie the numeroSerie to set
	 */
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	/**
	 * @return the oficina
	 */	
	public Oficina getOficina() {
		return oficina;
	}
	/**
	 * @param oficina the oficina to set
	 */
	public void setOficina(Oficina oficina) {
		this.oficina = oficina;
	}
	
	@Exportable(columnName="OFICINA", columnOrder=5)
	public String getNombreOficina(){
		return oficina != null ? oficina.getNombreOficina() : null;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SolicitudSuspensionServicio other = (SolicitudSuspensionServicio) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	@SuppressWarnings("unchecked")
	@Override
	public  Long  getKey() {
		return this.getId();
	}
	@Override
	public String getValue() {
		return this.getNumeroSerie();
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.getId();
	}
	/**
	 * @return the estatusName
	 */
	@Exportable(columnName="Estatus", columnOrder=2)
	public String getEstatusName() {
		return estatusName;
	}
	/**
	 * @param estatusName the estatusName to set
	 */
	public void setEstatusName(String estatusName) {
		this.estatusName = estatusName;
	}
	
	
}
