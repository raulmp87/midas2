package mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name = "ProcesoCabina")
@Table(name = "TCPROCESOCABINA", schema = "MIDAS")
public class ProcesoCabina implements Entidad {
	
	private static final long serialVersionUID = -6220017085456182022L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TCPROCESOCABINA_SEQ_ID_GENERATOR")
	@SequenceGenerator(name="TCPROCESOCABINA_SEQ_ID_GENERATOR", schema="MIDAS", sequenceName="IDTCPROCESOCABINA_SEQ", allocationSize=1)	
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "DESCRIPCION")
	private String descripcion;
	
	@Column(name = "VISIBLE")
	private Boolean visible = Boolean.TRUE;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof ProcesoCabina) {
			ProcesoCabina procesoCabina = (ProcesoCabina) object;
			equal = procesoCabina.getId().equals(this.id);
		}
		return equal;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	
}
