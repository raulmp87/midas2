package mx.com.afirme.midas2.domain.siniestros.sipac;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.directwebremoting.annotations.DataTransferObject;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name="loteLayoutSipacDTO")
@Table(name="TOLOTELAYOUTSIPAC", schema="MIDAS")
@DataTransferObject
public class LoteLayoutSipac implements Serializable, Entidad{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "LOTELAYOUTSIPAC_SEQ", allocationSize = 1, sequenceName ="MIDAS.LOTELAYOUTSIPAC_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LOTELAYOUTSIPAC_SEQ")
	@Column(name="ID", unique=true, nullable=false)
	private Long id;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION", nullable=false)
	private Date fechaCreacion;
	
	@Column(name = "USUARIO_CREACION", nullable=false)
	private String usuarioCreacion;
	
	@Column(name = "ESTATUS", nullable=false)
	private String estatus;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_OCURRIDO_DESDE", nullable=false)
	private Date fechaOcurridoDesde;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_OCURRIDO_HASTA", nullable=false)
	private Date fechaOcurridoHasta;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public Date getFechaOcurridoDesde() {
		return fechaOcurridoDesde;
	}

	public void setFechaOcurridoDesde(Date fechaOcurridoDesde) {
		this.fechaOcurridoDesde = fechaOcurridoDesde;
	}

	public Date getFechaOcurridoHasta() {
		return fechaOcurridoHasta;
	}

	public void setFechaOcurridoHasta(Date fechaOcurridoHasta) {
		this.fechaOcurridoHasta = fechaOcurridoHasta;
	}
	
	@Override
	public <K> K getKey() {
		return null;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
	
}
