package mx.com.afirme.midas2.domain.siniestros.recuperacion;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.EnumBase;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;

@Entity(name = "RecuperacionDeducible")
@Table(name = "TOSNRECUPERACIONDEDUCIBLE", schema = "MIDAS")
@DiscriminatorValue("DED")
@PrimaryKeyJoinColumn(name="RECUPERACION_ID", referencedColumnName="ID")
public class RecuperacionDeducible  extends Recuperacion{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3845260254798029574L;

	//@Id
	//private Long recuperacionId;
	
	@Column(name = "TIPO_PROCESO")
	private String tipoProceso;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_INGRESO")
	private Date fechaIngreso;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_ENTREGA")
	private Date fechaEntrega;
	
	@Column(name = "NOMBRE_VALUADOR")
	private String nombreValuador;

	@Column(name = "AFECTADO")
	private String afectado;
	
	@Column(name = "NUM_VALUACION")
	private String numeroValuacion;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ESTIMACION_ID")
	private EstimacionCoberturaReporteCabina estimacionCobertura;
	
	@Column(name = "MARCA_ID")
	private Long marcaId;
	
	@Column(name="ESTILO_ID")
	private String estiloId;	
	
	@Column(name="MODELO_VEHICULO")
	private Integer modeloVehiculo;
	
	@Column(name="TIPO_DEDUCIBLE")
	private String tipoDeducible;
	
	@Column(name="PORCENTAJE_DEDUCIBLE")
	private String porcentajeDeducible;
	
	@Column(name="MONTO_DEDUCIBLE")
	private BigDecimal montoDeducible;
	
	@Column(name="MONTO_FINAL_DEDUCIBLE")
	private BigDecimal montoFinalDeducible;
	
	@Column(name="ORIGEN_DED")
	private String origenDeducible = Origen.PASE.getValue() ;
	
	@Transient
	private Integer antiguedadCobro;
	
	@Transient
	private Integer antiguedadCreacion;
	
	@Transient
	private String descripcionEstilo;
	
	@Transient
	private String descripcionMarca;
	
	@Transient
	private String tipoSiniestro;
	
	@Transient
	private String descripcionTipoDeducible;
	
	@Transient
	private String taller;
	
	@Transient
	private String referencia;
	
	public  enum Origen implements EnumBase<String>{
		PASE,
		LIQUI;

		@Override
		public String getValue() {
			return this.toString();
		}

		@Override
		public String getLabel() {
			return this.toString();
		}
	}
	

	public String getTipoProceso() {
		return tipoProceso;
	}

	public void setTipoProceso(String tipoProceso) {
		this.tipoProceso = tipoProceso;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public Date getFechaEntrega() {
		return fechaEntrega;
	}

	public void setFechaEntrega(Date fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}

	public String getNombreValuador() {
		return nombreValuador;
	}

	public void setNombreValuador(String nombreValuador) {
		this.nombreValuador = nombreValuador;
	}

	public String getAfectado() {
		return afectado;
	}

	public void setAfectado(String afectado) {
		this.afectado = afectado;
	}

	public String getNumeroValuacion() {
		return numeroValuacion;
	}

	public void setNumeroValuacion(String numeroValuacion) {
		this.numeroValuacion = numeroValuacion;
	}

	public EstimacionCoberturaReporteCabina getEstimacionCobertura() {
		return estimacionCobertura;
	}

	public void setEstimacionCobertura(
			EstimacionCoberturaReporteCabina estimacionCobertura) {
		this.estimacionCobertura = estimacionCobertura;
	}

	public Long getMarcaId() {
		return marcaId;
	}

	public void setMarcaId(Long marcaId) {
		this.marcaId = marcaId;
	}

	public String getEstiloId() {
		return estiloId;
	}

	public void setEstiloId(String estiloId) {
		this.estiloId = estiloId;
	}

	public Integer getModeloVehiculo() {
		return modeloVehiculo;
	}

	public void setModeloVehiculo(Integer modeloVehiculo) {
		this.modeloVehiculo = modeloVehiculo;
	}

	public String getTipoDeducible() {
		return tipoDeducible;
	}

	public void setTipoDeducible(String tipoDeducible) {
		this.tipoDeducible = tipoDeducible;
	}

	public String getPorcentajeDeducible() {
		return porcentajeDeducible;
	}

	public void setPorcentajeDeducible(String porcentajeDeducible) {
		this.porcentajeDeducible = porcentajeDeducible;
	}

	public BigDecimal getMontoDeducible() {
		return montoDeducible;
	}

	public void setMontoDeducible(BigDecimal montoDeducible) {
		this.montoDeducible = montoDeducible;
	}

	public BigDecimal getMontoFinalDeducible() {
		return montoFinalDeducible;
	}

	public void setMontoFinalDeducible(BigDecimal montoFinalDeducible) {
		this.montoFinalDeducible = montoFinalDeducible;
	}

	public Integer getAntiguedadCobro() {
		return antiguedadCobro;
	}

	public void setAntiguedadCobro(Integer antiguedadCobro) {
		this.antiguedadCobro = antiguedadCobro;
	}

	public Integer getAntiguedadCreacion() {
		return antiguedadCreacion;
	}

	public void setAntiguedadCreacion(Integer antiguedadCreacion) {
		this.antiguedadCreacion = antiguedadCreacion;
	}

	public String getDescripcionEstilo() {
		return descripcionEstilo;
	}

	public void setDescripcionEstilo(String descripcionEstilo) {
		this.descripcionEstilo = descripcionEstilo;
	}

	public String getDescripcionMarca() {
		return descripcionMarca;
	}

	public void setDescripcionMarca(String descripcionMarca) {
		this.descripcionMarca = descripcionMarca;
	}

	public String getTipoSiniestro() {
		return tipoSiniestro;
	}

	public void setTipoSiniestro(String tipoSiniestro) {
		this.tipoSiniestro = tipoSiniestro;
	}

	public String getDescripcionTipoDeducible() {
		return descripcionTipoDeducible;
	}

	public void setDescripcionTipoDeducible(String descripcionTipoDeducible) {
		this.descripcionTipoDeducible = descripcionTipoDeducible;
	}

	public String getTaller() {
		return taller;
	}

	public void setTaller(String taller) {
		this.taller = taller;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	@Override
	public String toString() {
		return super.toString() + " ---- RecuperacionDeducible [tipoProceso=" + tipoProceso
				+ ", fechaIngreso=" + fechaIngreso + ", fechaEntrega="
				+ fechaEntrega + ", nombreValuador=" + nombreValuador
				+ ", afectado=" + afectado + ", numeroValuacion="
				+ numeroValuacion + ", estimacionCobertura="
				+ estimacionCobertura + ", marcaId=" + marcaId + ", estiloId="
				+ estiloId + ", modeloVehiculo=" + modeloVehiculo
				+ ", tipoDeducible=" + tipoDeducible + ", porcentajeDeducible="
				+ porcentajeDeducible + ", montoDeducible=" + montoDeducible
				+ ", montoFinalDeducible=" + montoFinalDeducible
				+ ", antiguedadCobro=" + antiguedadCobro
				+ ", antiguedadCreacion=" + antiguedadCreacion
				+ ", descripcionEstilo=" + descripcionEstilo
				+ ", descripcionMarca=" + descripcionMarca + ", tipoSiniestro="
				+ tipoSiniestro + ", descripcionTipoDeducible="
				+ descripcionTipoDeducible + ", taller=" + taller
				+ ", referencia=" + referencia + "]";
	}

	public String getOrigenDeducible() {
		return origenDeducible;
	}

	public void setOrigenDeducible(String origenDeducible) {
		this.origenDeducible = origenDeducible;
	}	
		
}
