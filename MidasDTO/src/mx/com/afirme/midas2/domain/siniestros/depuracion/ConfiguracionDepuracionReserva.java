package mx.com.afirme.midas2.domain.siniestros.depuracion;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

@Entity(name = "ConfiguracionDepuracionReserva")
@Table(name = "TOCONFIGDEPURACIONRESERVA", schema = "MIDAS")
public class ConfiguracionDepuracionReserva extends MidasAbstracto {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8738853953809659688L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOCONFIGDEPURACIONRESERVA_ID_GENERATOR")
	@SequenceGenerator(name="TOCONFIGDEPURACIONRESERVA_ID_GENERATOR", schema="MIDAS", sequenceName="TOCONFIGDEPURACIONRESERVA_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;
	

	@Column(name="ANTIGUEDAD_COMPLETA")
	private Boolean antiguedadCompleta;
	
	@Column(name="ANTIGUEDAD_PRESUPUESTO")
	private Integer antiguedadPresupuesto;
	
	@Column(name="ANTIGUEDAD_SIN_PAGO")
	private Integer antiguedadSinPago;
	
	@Column(name="APLICA_PRESUPUESTO")
	private Boolean aplicaPresupuesto;
	
	@Column(name="APLICA_PAGO")
	private Boolean aplicaPago;
	
	@Column(name="APLICA_SIN_DEPURAR")
	private Boolean aplicaSinDepurar;
	
	@Column(name="CONDICION_ANTIGUEDAD_PRES")
	private String condicionAntiguedadPresupuesto;
	
	@Column(name="CONDICION_ANTIGUEDAD_SIN_PAGO")
	private String condicionAntiguedadSinPago;
	
	@Column(name="CONDICION_ANTIGUEDAD_SIN")
	private String condicionAntiguedadSin;
	
	@Column(name="CONDICION_MONTO_ACTUAL")
	private	String condicionMontoActual;
	
	@Column(name="CONDICION_PORCENTAJE_PAGO")
	private String condicionPorcentajePago;
	
	@Column(name="CONFIG_ANTIGUEDAD_PAGO")
	private Boolean configAntiguedadPago;
	
	@Column(name="CONFIG_ANTIGUEDAD_PRES")
	private Boolean configAntiguedadPres;
	
	@Column(name="CONFIG_ANTIGUEDAD_SIN")
	private Boolean configAntiguedadSin;
	
	@Column(name="CONFIG_COBERTURAS")
	private Boolean configCoberturas;
	
	@Column(name="CONFIG_DM_RCV")
	private Boolean configDMRCV;
	
	@Column(name="CONFIG_GM_RCP")
	private Boolean configGMRCP;
	
	@Column(name="CONFIG_MONTO")
	private Boolean configMonto;
	
	@Column(name="CONFIG_PAGOS")
	private Boolean configPagos;
	
	@Column(name="CONFIG_PORCENTAJE_PAGO")
	private Boolean configPorcentajePago;
	
	@Column(name="CONFIG_PRESUPUESTOS")
	private Boolean configPresupuestos;
	
	@Column(name="CONFIG_TERMINOS")
	private Boolean configTerminos;
	
	@Column(name="CRITERIO_ANTIGUEDAD_PRES")
	private String criterioAntiguedadPresupuesto;
	
	@Column(name="CRITERIO_ANTIGUEDAD_SIN_PAGO")
	private String criterioAntiguedadSinPago;
	
	@Column(name="CRITERO_ANTIGUEDAD_SIN")
	private String criterioAntiguedadSin;
	
	@Column(name="CRITERIO_RANGO_MONTO")
	private Boolean criterioRangoMonto;
	
	@Column(name="CRITERIO_RANGO_PORCENTAJE")
	private Boolean criterioRangoPorcentaje;
	
	@Transient
	private String descripcionEstatus;
	
	@Column(name="DIA_PROGRAMACION")
	private Integer diaProgramacion;
	
	@Column(name="ESTATUS")
	private Integer estatus;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_ACTIVO")
	private Date fechaActivo;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_INACTIVO")
	private Date fechaInactivo;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_PROGRAMACION")
	private Date fechaProgramacion;
	
	@Column(name="MONTO_FINAL")
	private BigDecimal montoFinal;
	
	@Column(name="MONTO_INICIAL")
	private BigDecimal montoInicial;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="NEGOCIO_ID", referencedColumnName="IDTONEGOCIO")
	@JoinFetch(JoinFetchType.OUTER)
	private Negocio negocio;
	
	@Column(name="NOMBRE")
	private String nombre;
	
	@Column(name="NUMERO_INCISO")
	private Integer numeroInciso;
	
	@Column(name="NUMERO_POLIZA")
	private String numeroPoliza;
	
	@Column(name="NUMERO_SINIESTRO")
	private String numeroSiniestro;
	
	@Column(name="OBSERVACIONES")
	private String observaciones;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="OFICINA_ID", referencedColumnName="ID")
	@JoinFetch(JoinFetchType.OUTER)
	private Oficina oficina;
	
	@Column(name="PORCENTAJE")
	private Integer porcentaje;
	
	@Column(name="PORCENTAJE_PAGO_FINAL")
	private Integer porcentajePagoFinal;
	
	@Column(name="PORCENTAJE_PAGO_INICIAL")
	private Integer porcentajePagoInicial;
	
	@Column(name="TIPO_ATENCION")
	private String tipoAtencion;
	
	@Column(name="TIPO_PERDIDA")
	private String tipoPerdida;
	
	@Column(name="TIPO_PROGRAMACION")
	private String tipoProgramacion;
	
	@Column(name="TIPO_SERVICIO")
	private String tipoServicio;
	
	@Column(name="VALOR_ANTIGUEDAD_SIN")
	private Integer valorAntiguedadSin;
	
	@ElementCollection
	@CollectionTable(
		schema="MIDAS",
        name="TOTERMINOAJUSTEDEPURACION",
        joinColumns=@JoinColumn(name="CONFIGURACIONDEPURACION_ID")
	)
	private List<String> termino_Ajuste;
	
	@OneToMany(fetch=FetchType.LAZY,  mappedBy = "configuracion")
	private List<CoberturaDepuracionReserva> coberturas;
	
	@OneToMany(fetch=FetchType.LAZY,  mappedBy = "configuracion")
    private List<DepuracionReserva> depuracionReservas;

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return nombre;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getAntiguedadPresupuesto() {
		return antiguedadPresupuesto;
	}

	public void setAntiguedadPresupuesto(Integer antiguedadPresupuesto) {
		this.antiguedadPresupuesto = antiguedadPresupuesto;
	}

	public Boolean getAplicaPresupuesto() {
		return aplicaPresupuesto;
	}

	public void setAplicaPresupuesto(Boolean aplicaPresupuesto) {
		this.aplicaPresupuesto = aplicaPresupuesto;
	}

	public Boolean getAplicaSinDepurar() {
		return aplicaSinDepurar;
	}

	public void setAplicaSinDepurar(Boolean aplicaSinDepurar) {
		this.aplicaSinDepurar = aplicaSinDepurar;
	}

	public String getCondicionAntiguedadSin() {
		return condicionAntiguedadSin;
	}

	public void setCondicionAntiguedadSin(String condicionAntiguedadSin) {
		this.condicionAntiguedadSin = condicionAntiguedadSin;
	}

	public String getCondicionMontoActual() {
		return condicionMontoActual;
	}

	public void setCondicionMontoActual(String condicionMontoActual) {
		this.condicionMontoActual = condicionMontoActual;
	}

	public String getCondicionPorcentajePago() {
		return condicionPorcentajePago;
	}

	public void setCondicionPorcentajePago(String condicionPorcentajePago) {
		this.condicionPorcentajePago = condicionPorcentajePago;
	}

	public Boolean getConfigAntiguedadPago() {
		return configAntiguedadPago;
	}

	public void setConfigAntiguedadPago(Boolean configAntiguedadPago) {
		this.configAntiguedadPago = configAntiguedadPago;
	}

	public Boolean getConfigAntiguedadPres() {
		return configAntiguedadPres;
	}

	public void setConfigAntiguedadPres(Boolean configAntiguedadPres) {
		this.configAntiguedadPres = configAntiguedadPres;
	}

	public Boolean getConfigAntiguedadSin() {
		return configAntiguedadSin;
	}

	public void setConfigAntiguedadSin(Boolean configAntiguedadSin) {
		this.configAntiguedadSin = configAntiguedadSin;
	}

	public Boolean getConfigCoberturas() {
		return configCoberturas;
	}

	public void setConfigCoberturas(Boolean configCoberturas) {
		this.configCoberturas = configCoberturas;
	}

	public Boolean getConfigDMRCV() {
		return configDMRCV;
	}

	public void setConfigDMRCV(Boolean configDMRCV) {
		this.configDMRCV = configDMRCV;
	}

	public Boolean getConfigGMRCP() {
		return configGMRCP;
	}

	public void setConfigGMRCP(Boolean configGMRCP) {
		this.configGMRCP = configGMRCP;
	}

	public Boolean getConfigMonto() {
		return configMonto;
	}

	public void setConfigMonto(Boolean configMonto) {
		this.configMonto = configMonto;
	}

	public Boolean getConfigPagos() {
		return configPagos;
	}

	public void setConfigPagos(Boolean configPagos) {
		this.configPagos = configPagos;
	}

	public Boolean getConfigPorcentajePago() {
		return configPorcentajePago;
	}

	public void setConfigPorcentajePago(Boolean configPorcentajePago) {
		this.configPorcentajePago = configPorcentajePago;
	}

	public Boolean getConfigPresupuestos() {
		return configPresupuestos;
	}

	public void setConfigPresupuestos(Boolean configPresupuestos) {
		this.configPresupuestos = configPresupuestos;
	}

	public Boolean getConfigTerminos() {
		return configTerminos;
	}

	public void setConfigTerminos(Boolean configTerminos) {
		this.configTerminos = configTerminos;
	}

	public String getCriterioAntiguedadSin() {
		return criterioAntiguedadSin;
	}

	public void setCriterioAntiguedadSin(String criterioAntiguedadSin) {
		this.criterioAntiguedadSin = criterioAntiguedadSin;
	}
	
	public Boolean getCriterioRangoMonto() {
		return criterioRangoMonto;
	}

	public void setCriterioRangoMonto(Boolean criterioRangoMonto) {
		this.criterioRangoMonto = criterioRangoMonto;
	}

	public Boolean getCriterioRangoPorcentaje() {
		return criterioRangoPorcentaje;
	}

	public void setCriterioRangoPorcentaje(Boolean criterioRangoPorcentaje) {
		this.criterioRangoPorcentaje = criterioRangoPorcentaje;
	}

	public String getDescripcionEstatus() {
		return descripcionEstatus;
	}

	public void setDescripcionEstatus(String descripcionEstatus) {
		this.descripcionEstatus = descripcionEstatus;
	}

	public Integer getDiaProgramacion() {
		return diaProgramacion;
	}

	public void setDiaProgramacion(Integer diaProgramacion) {
		this.diaProgramacion = diaProgramacion;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public Date getFechaActivo() {
		return fechaActivo;
	}

	public void setFechaActivo(Date fechaActivo) {
		this.fechaActivo = fechaActivo;
	}

	public Date getFechaInactivo() {
		return fechaInactivo;
	}

	public void setFechaInactivo(Date fechaInactivo) {
		this.fechaInactivo = fechaInactivo;
	}

	public Date getFechaProgramacion() {
		return fechaProgramacion;
	}

	public void setFechaProgramacion(Date fechaProgramacion) {
		this.fechaProgramacion = fechaProgramacion;
	}

	public BigDecimal getMontoFinal() {
		return montoFinal;
	}

	public void setMontoFinal(BigDecimal montoFinal) {
		this.montoFinal = montoFinal;
	}

	public BigDecimal getMontoInicial() {
		return montoInicial;
	}

	public void setMontoInicial(BigDecimal montoInicial) {
		this.montoInicial = montoInicial;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}

	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Oficina getOficina() {
		return oficina;
	}

	public void setOficina(Oficina oficina) {
		this.oficina = oficina;
	}

	public Integer getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(Integer porcentaje) {
		this.porcentaje = porcentaje;
	}

	public Integer getPorcentajePagoFinal() {
		return porcentajePagoFinal;
	}

	public void setPorcentajePagoFinal(Integer porcentajePagoFinal) {
		this.porcentajePagoFinal = porcentajePagoFinal;
	}

	public Integer getPorcentajePagoInicial() {
		return porcentajePagoInicial;
	}

	public void setPorcentajePagoInicial(Integer porcentajePagoInicial) {
		this.porcentajePagoInicial = porcentajePagoInicial;
	}

	public String getTipoAtencion() {
		return tipoAtencion;
	}

	public void setTipoAtencion(String tipoAtencion) {
		this.tipoAtencion = tipoAtencion;
	}

	public String getTipoPerdida() {
		return tipoPerdida;
	}

	public void setTipoPerdida(String tipoPerdida) {
		this.tipoPerdida = tipoPerdida;
	}

	public String getTipoProgramacion() {
		return tipoProgramacion;
	}

	public void setTipoProgramacion(String tipoProgramacion) {
		this.tipoProgramacion = tipoProgramacion;
	}

	public String getTipoServicio() {
		return tipoServicio;
	}

	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

	public Integer getValorAntiguedadSin() {
		return valorAntiguedadSin;
	}

	public void setValorAntiguedadSin(Integer valorAntiguedadSin) {
		this.valorAntiguedadSin = valorAntiguedadSin;
	}

	public List<String> getTermino_Ajuste() {
		return termino_Ajuste;
	}

	public void setTermino_Ajuste(List<String> termino_Ajuste) {
		this.termino_Ajuste = termino_Ajuste;
	}

	public List<CoberturaDepuracionReserva> getCoberturas() {
		return coberturas;
	}

	public void setCoberturas(List<CoberturaDepuracionReserva> coberturas) {
		this.coberturas = coberturas;
	}
	
	public Integer getAntiguedadSinPago() {
		return antiguedadSinPago;
	}

	public void setAntiguedadSinPago(Integer antiguedadSinPago) {
		this.antiguedadSinPago = antiguedadSinPago;
	}

	public Boolean getAplicaPago() {
		return aplicaPago;
	}

	public void setAplicaPago(Boolean aplicaPago) {
		this.aplicaPago = aplicaPago;
	}

	public String getCondicionAntiguedadPresupuesto() {
		return condicionAntiguedadPresupuesto;
	}

	public void setCondicionAntiguedadPresupuesto(
			String condicionAntiguedadPresupuesto) {
		this.condicionAntiguedadPresupuesto = condicionAntiguedadPresupuesto;
	}

	public String getCondicionAntiguedadSinPago() {
		return condicionAntiguedadSinPago;
	}

	public void setCondicionAntiguedadSinPago(String condicionAntiguedadSinPago) {
		this.condicionAntiguedadSinPago = condicionAntiguedadSinPago;
	}

	public String getCriterioAntiguedadPresupuesto() {
		return criterioAntiguedadPresupuesto;
	}

	public void setCriterioAntiguedadPresupuesto(
			String criterioAntiguedadPresupuesto) {
		this.criterioAntiguedadPresupuesto = criterioAntiguedadPresupuesto;
	}

	public String getCriterioAntiguedadSinPago() {
		return criterioAntiguedadSinPago;
	}

	public void setCriterioAntiguedadSinPago(String criterioAntiguedadSinPago) {
		this.criterioAntiguedadSinPago = criterioAntiguedadSinPago;
	}

	public List<DepuracionReserva> getDepuracionReservas() {
		return depuracionReservas;
	}

	public void setDepuracionReservas(List<DepuracionReserva> depuracionReservas) {
		this.depuracionReservas = depuracionReservas;
	}
	
	public Boolean getAntiguedadCompleta() {
		return antiguedadCompleta;
	}

	public void setAntiguedadCompleta(Boolean antiguedadCompleta) {
		this.antiguedadCompleta = antiguedadCompleta;
	}
	
	@Embeddable
	public class TerminoAjusteDepuracion {
	  @Column(name="TERMINO_AJUSTE")
	  private String terminoAjuste;

	public String getTerminoAjuste() {
		return terminoAjuste;
	}

	public void setTerminoAjuste(String terminoAjuste) {
		this.terminoAjuste = terminoAjuste;
	}
	}

}
