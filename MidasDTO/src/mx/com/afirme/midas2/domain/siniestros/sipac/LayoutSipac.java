package mx.com.afirme.midas2.domain.siniestros.sipac;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;






import mx.com.afirme.midas2.dao.catalogos.EnumBase;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;


/**
 * @author bitfarm
 *
 */
@Entity(name ="LayoutSipac")
@Table(name = "TOLAYOUTSIPAC", schema = "MIDAS")
public class LayoutSipac extends MidasAbstracto {
	

	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "LAYOUTSIPAC_SEQ", allocationSize = 1, sequenceName = "MIDAS.LAYOUTSIPAC_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LAYOUTSIPAC_SEQ")	
	@Column(name="ID")
	private Long id;
	
	@Column(name="NUMERO_SECUENCIAL")
	private String numeroSecuencial;
	
	@Column(name="TIPO_OPERACION")
	private String tipoOperacion;
	
	@Column(name="CIA_DEUDORA")
	private String ciaDeudora;
	
	@Column(name="FOLIO_ORDEN")
	private String folioOrden;
	
	@Column(name="TIPO_CAPTURA")
	private String tipoCaptura;
	
	@Column(name="SINIESTRO_DEUDOR")
	private String siniestroDeudor;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_SINIESTRO")
	private Date fechaSiniestro;
	
	@Column(name="PAIS")
	private String pais;
	
	@Column(name="ESTADO")
	private String estado;
	
	@Column(name="MUNICIPIO")
	private String municipio;
	
	@Column(name="POLIZA_DEUDOR")
	private String polizaDeudor;
	
	@Column(name="INCISO_DEUDOR")
	private String incisoDeudor;
	
	@Column(name="PERS_DEUDOR")
	private String persDeudor;
	
	@Column(name="NOMBRE_RESPONSABLE")
	private String nombreResponsable;
	
	@Column(name="AP_PATERNO_RESPONSABLE")
	private String apPaternoResponsable;
	
	@Column(name="AP_MATERNO_RESPONSABLE")
	private String apMaternoResponsable;
	
	@Column(name="TIPO_TRANSPORTE_DEUDOR")
	private String tipoTransporteDeudor;
	
	@Column(name="MARCA_DEUDOR")
	private String marcaDeudor;
	
	@Column(name="TIPO_VEH_DEUDOR")
	private String tipoVehDeudor;
	
	@Column(name="SERIE_DEUDOR")
	private String serieDeudor;
	
	@Column(name="MODELO_DEUDOR")
	private String modeloDeudor;
	
	@Column(name="COLOR_VEH_DEUDOR")
	private String colorVehDeudor;
	
	@Column(name="PLACA_DEUDOR")
	private String placaDeudor;
	
	@Column(name="AJUSTADOR_DEUDORA")
	private String ajustadorDeudora;
	
	@Column(name="CLAVE_AJUST_DEUDORA")
	private String claveAjustDeudora;
	
	@Column(name="COBERTURA")
	private String cobertura;
	
	@Column(name="CIA_ACREEDORA")
	private String ciaAcreedora;
	
	@Column(name="SINIESTRO_ACREEDOR")
	private String siniestroAcreedor;
	
	@Column(name="OBS_DEUDOR")
	private String obsDeudor;
	
	@Column(name="TIPO_ORDEN")
	private String tipoOrden;
	
	@Column(name="ORIGEN")
	private String origen;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_EXPEDICION")
	private Date fechaExpedicion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_DICTAMEN")
	private Date fechaDictamen;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_INICIO_JUICIO")
	private Date fechaInicioJuicio;
	
	@Column(name="POLIZA_ACREEDOR")
	private String polizaAcreedor;
	
	@Column(name="INCISO_ACREEDOR")
	private String incisoAcreedor;
	
	@Column(name="PERS_AFECTADO")
	private String persAfectado;
	
	@Column(name="NOMBRE_AFECTADO")
	private String nombreAfectado;
	
	@Column(name="AP_PATERNO_AFECTADO")
	private String apPaternoAfectado;
	
	@Column(name="AP_MATERNO_AFECTADO")
	private String apMaternoAfectado;
	
	@Column(name="TIPO_TRANSPORTE_AFECTADO")
	private String tipoTransporteAfectado;
	
	@Column(name="MARCA_AFECTADO")
	private String marcaAfectado;
	
	@Column(name="TIPO_VEH_AFECTADO")
	private String tipoVehAfectado;
	
	@Column(name="SERIE_AFECTADO")
	private String serieAfectado;
	
	@Column(name="MODELO_AFECTADO")
	private String modeloAfectado;
	
	@Column(name="COLOR_VEH_AFECTADO")
	private String colorVehAfectado;
	
	@Column(name="PLACA_AFECTADO")
	private String placaAfectado;
	
	@Column(name="NOMBRE_LESIONADO")
	private String nombreLesionado;
	
	@Column(name="AP_PATERNO_LESIONADO")
	private String apPaternoLesionado;
	
	@Column(name="AP_MATERNO_LESIONADO")
	private String apMaternoLesionado;
	
	@Column(name="TIPO_LESIONADO")
	private String tipoLesionado;
	
	@Column(name="TIPO_PERSONA_LESIONADO")
	private String tipoPersonaLesionado;
	
	@Column(name="DOMICILIO_LESIONADO")
	private String domicilioLesionado;
	
	@Column(name="TELEFONO_LESIONADO")
	private BigDecimal telefonoLesionado;
	
	@Column(name="EDAD_LESIONADO")
	private BigDecimal edadLesionado;
	
	@Column(name="AJUSTADOR_ACREEDORA")
	private String ajustadorAcreedora;
	
	@Column(name="CLAVE_AJUST_ACREEDORA")
	private String claveAjustAcreedora;
	
	@Column(name="OBS_ACREEDOR")
	private String obsAcreedor;
	
	@Column(name="DANOS_PREEXISTENTES")
	private String danosPreexistentes;
	
	@Column(name="DESCR_DPREEXISTENTES")
	private String descrDpreexistentes;
	
	@Column(name="DANOS_LESIONES")
	private String danosLesiones;
	
	@Column(name="FOLIO_LESIONADO")
	private String folioLesionado;
	
	@Column(name="CIRCUNSTANCIA_DEU")
	private String circunstanciaDeu;
	
	@Column(name="CIRCUNSTANCIA_ACR")
	private String circunstanciaAcr;
	
	@Column(name="USO_VEHICULO_DEU")
	private String usoVehiculoDeu;
	
	@Column(name="USO_VEHICULO_ACR")
	private String usoVehiculoAcr;
	
	@Column(name="ESTATUS")
	private String estatus;
	
	@Column(name="DESC_ERROR")
	private String descError;
	
	@JoinColumn(name="TOLOTELAYOUTSIPAC_ID", referencedColumnName = "ID")
	private LoteLayoutSipac lote; 
	
	@JoinColumn(name="TOREPORTECABINA_ID", referencedColumnName = "ID")
	private ReporteCabina reporte; 
	
	public enum Estatus implements EnumBase<String>{
		PENDIENTE("1"),
		ENVIADO("2"),
		ERROR("3"),
		ACEPTADO("4");
		
		private String value;
		
		private Estatus(String value){
			this.value=value;
		}
		
		public String getValue(){
			return value;
		}

		@Override
		public String getLabel() {
			return value;
		}
	}	
	
	public static final String PERSONA_FISICA = "F";
	public static final String PERSONA_MORAL = "M";
	
	
	public LayoutSipac() {
		super();
		this.tipoOperacion = "1";
		this.tipoCaptura = "R";
		this.ciaAcreedora = "7";
		this.tipoOrden = "D";
		this.estatus = Estatus.PENDIENTE.getValue();
		this.cobertura = "B";
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long idLayoutSipac) {
		this.id = idLayoutSipac;
	}

	public String getNumeroSecuencial() {
		return numeroSecuencial;
	}

	public void setNumeroSecuencial(String numeroSecuencial) {
		this.numeroSecuencial = numeroSecuencial;
	}

	public String getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public String getCiaDeudora() {
		return ciaDeudora;
	}

	public void setCiaDeudora(String ciaDeudora) {
		this.ciaDeudora = ciaDeudora;
	}

	public String getFolioOrden() {
		return folioOrden;
	}

	public void setFolioOrden(String folioOrden) {
		this.folioOrden = folioOrden;
	}

	public String getTipoCaptura() {
		return tipoCaptura;
	}

	public void setTipoCaptura(String tipoCaptura) {
		this.tipoCaptura = tipoCaptura;
	}

	public String getSiniestroDeudor() {
		return siniestroDeudor;
	}

	public void setSiniestroDeudor(String siniestroDeudor) {
		this.siniestroDeudor = siniestroDeudor;
	}

	public Date getFechaSiniestro() {
		return fechaSiniestro;
	}

	public void setFechaSiniestro(Date fechaSiniestro) {
		this.fechaSiniestro = fechaSiniestro;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getPolizaDeudor() {
		return polizaDeudor;
	}

	public void setPolizaDeudor(String polizaDeudor) {
		this.polizaDeudor = polizaDeudor;
	}

	public String getIncisoDeudor() {
		return incisoDeudor;
	}

	public void setIncisoDeudor(String incisoDeudor) {
		this.incisoDeudor = incisoDeudor;
	}

	public String getPersDeudor() {
		return persDeudor;
	}

	public void setPersDeudor(String persDeudor) {
		this.persDeudor = persDeudor;
	}
	
	public String getNombreResponsable() {
		return nombreResponsable;
	}

	public void setNombreResponsable(String nombreResponsable) {
		this.nombreResponsable = nombreResponsable;
	}

	public String getApPaternoResponsable() {
		return apPaternoResponsable;
	}

	public void setApPaternoResponsable(String apPaternoResponsable) {
		this.apPaternoResponsable = apPaternoResponsable;
	}

	public String getApMaternoResponsable() {
		return apMaternoResponsable;
	}

	public void setApMaternoResponsable(String apMaternoResponsable) {
		this.apMaternoResponsable = apMaternoResponsable;
	}

	public String getTipoTransporteDeudor() {
		return tipoTransporteDeudor;
	}

	public void setTipoTransporteDeudor(String tipoTransporteDeudor) {
		this.tipoTransporteDeudor = tipoTransporteDeudor;
	}

	public String getMarcaDeudor() {
		return marcaDeudor;
	}

	public void setMarcaDeudor(String marcaDeudor) {
		this.marcaDeudor = marcaDeudor;
	}

	public String getTipoVehDeudor() {
		return tipoVehDeudor;
	}

	public void setTipoVehDeudor(String tipoVehDeudor) {
		this.tipoVehDeudor = tipoVehDeudor;
	}

	public String getSerieDeudor() {
		return serieDeudor;
	}

	public void setSerieDeudor(String serieDeudor) {
		this.serieDeudor = serieDeudor;
	}

	public String getModeloDeudor() {
		return modeloDeudor;
	}

	public void setModeloDeudor(String modeloDeudor) {
		this.modeloDeudor = modeloDeudor;
	}

	public String getColorVehDeudor() {
		return colorVehDeudor;
	}

	public void setColorVehDeudor(String colorVehDeudor) {
		this.colorVehDeudor = colorVehDeudor;
	}

	public String getPlacaDeudor() {
		return placaDeudor;
	}

	public void setPlacaDeudor(String placaDeudor) {
		this.placaDeudor = placaDeudor;
	}

	public String getAjustadorDeudora() {
		return ajustadorDeudora;
	}

	public void setAjustadorDeudora(String ajustadorDeudora) {
		this.ajustadorDeudora = ajustadorDeudora;
	}

	public String getClaveAjustDeudora() {
		return claveAjustDeudora;
	}

	public void setClaveAjustDeudora(String claveAjustDeudora) {
		this.claveAjustDeudora = claveAjustDeudora;
	}

	public String getCobertura() {
		return cobertura;
	}

	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}

	public String getCiaAcreedora() {
		return ciaAcreedora;
	}

	public void setCiaAcreedora(String ciaAcreedora) {
		this.ciaAcreedora = ciaAcreedora;
	}

	public String getSiniestroAcreedor() {
		return siniestroAcreedor;
	}

	public void setSiniestroAcreedor(String siniestroAcreedor) {
		this.siniestroAcreedor = siniestroAcreedor;
	}

	public String getObsDeudor() {
		return obsDeudor;
	}

	public void setObsDeudor(String obsDeudor) {
		this.obsDeudor = obsDeudor;
	}

	public String getTipoOrden() {
		return tipoOrden;
	}

	public void setTipoOrden(String tipoOrden) {
		this.tipoOrden = tipoOrden;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public Date getFechaExpedicion() {
		return fechaExpedicion;
	}

	public void setFechaExpedicion(Date fechaExpedicion) {
		this.fechaExpedicion = fechaExpedicion;
	}

	public Date getFechaDictamen() {
		return fechaDictamen;
	}

	public void setFechaDictamen(Date fechaDictamen) {
		this.fechaDictamen = fechaDictamen;
	}

	public Date getFechaInicioJuicio() {
		return fechaInicioJuicio;
	}

	public void setFechaInicioJuicio(Date fechaInicioJuicio) {
		this.fechaInicioJuicio = fechaInicioJuicio;
	}

	public String getPolizaAcreedor() {
		return polizaAcreedor;
	}

	public void setPolizaAcreedor(String polizaAcreedor) {
		this.polizaAcreedor = polizaAcreedor;
	}

	public String getIncisoAcreedor() {
		return incisoAcreedor;
	}

	public void setIncisoAcreedor(String incisoAcreedor) {
		this.incisoAcreedor = incisoAcreedor;
	}

	public String getPersAfectado() {
		return persAfectado;
	}

	public void setPersAfectado(String persAfectado) {
		this.persAfectado = persAfectado;
	}

	public String getNombreAfectado() {
		return nombreAfectado;
	}

	public void setNombreAfectado(String nombreAfectado) {
		this.nombreAfectado = nombreAfectado;
	}

	public String getApPaternoAfectado() {
		return apPaternoAfectado;
	}

	public void setApPaternoAfectado(String apPaternoAfectado) {
		this.apPaternoAfectado = apPaternoAfectado;
	}

	public String getApMaternoAfectado() {
		return apMaternoAfectado;
	}

	public void setApMaternoAfectado(String apMaternoAfectado) {
		this.apMaternoAfectado = apMaternoAfectado;
	}

	public String getTipoTransporteAfectado() {
		return tipoTransporteAfectado;
	}

	public void setTipoTransporteAfectado(String tipoTransporteAfectado) {
		this.tipoTransporteAfectado = tipoTransporteAfectado;
	}

	public String getMarcaAfectado() {
		return marcaAfectado;
	}

	public void setMarcaAfectado(String marcaAfectado) {
		this.marcaAfectado = marcaAfectado;
	}

	public String getTipoVehAfectado() {
		return tipoVehAfectado;
	}

	public void setTipoVehAfectado(String tipoVehAfectado) {
		this.tipoVehAfectado = tipoVehAfectado;
	}

	public String getSerieAfectado() {
		return serieAfectado;
	}

	public void setSerieAfectado(String serieAfectado) {
		this.serieAfectado = serieAfectado;
	}

	public String getModeloAfectado() {
		return modeloAfectado;
	}

	public void setModeloAfectado(String modeloAfectado) {
		this.modeloAfectado = modeloAfectado;
	}

	public String getColorVehAfectado() {
		return colorVehAfectado;
	}

	public void setColorVehAfectado(String colorVehAfectado) {
		this.colorVehAfectado = colorVehAfectado;
	}

	public String getPlacaAfectado() {
		return placaAfectado;
	}

	public void setPlacaAfectado(String placaAfectado) {
		this.placaAfectado = placaAfectado;
	}

	public String getNombreLesionado() {
		return nombreLesionado;
	}

	public void setNombreLesionado(String nombreLesionado) {
		this.nombreLesionado = nombreLesionado;
	}

	public String getApPaternoLesionado() {
		return apPaternoLesionado;
	}

	public void setApPaternoLesionado(String apPaternoLesionado) {
		this.apPaternoLesionado = apPaternoLesionado;
	}

	public String getApMaternoLesionado() {
		return apMaternoLesionado;
	}

	public void setApMaternoLesionado(String apMaternoLesionado) {
		this.apMaternoLesionado = apMaternoLesionado;
	}

	public String getTipoLesionado() {
		return tipoLesionado;
	}

	public void setTipoLesionado(String tipoLesionado) {
		this.tipoLesionado = tipoLesionado;
	}

	public String getTipoPersonaLesionado() {
		return tipoPersonaLesionado;
	}

	public void setTipoPersonaLesionado(String tipoPersonaLesionado) {
		this.tipoPersonaLesionado = tipoPersonaLesionado;
	}

	public String getDomicilioLesionado() {
		return domicilioLesionado;
	}

	public void setDomicilioLesionado(String domicilioLesionado) {
		this.domicilioLesionado = domicilioLesionado;
	}

	public BigDecimal getTelefonoLesionado() {
		return telefonoLesionado;
	}

	public void setTelefonoLesionado(BigDecimal telefonoLesionado) {
		this.telefonoLesionado = telefonoLesionado;
	}

	public BigDecimal getEdadLesionado() {
		return edadLesionado;
	}

	public void setEdadLesionado(BigDecimal edadLesionado) {
		this.edadLesionado = edadLesionado;
	}

	public String getAjustadorAcreedora() {
		return ajustadorAcreedora;
	}

	public void setAjustadorAcreedora(String ajustadorAcreedora) {
		this.ajustadorAcreedora = ajustadorAcreedora;
	}

	public String getClaveAjustAcreedora() {
		return claveAjustAcreedora;
	}

	public void setClaveAjustAcreedora(String claveAjustAcreedora) {
		this.claveAjustAcreedora = claveAjustAcreedora;
	}

	public String getObsAcreedor() {
		return obsAcreedor;
	}

	public void setObsAcreedor(String obsAcreedor) {
		this.obsAcreedor = obsAcreedor;
	}

	public String getDanosPreexistentes() {
		return danosPreexistentes;
	}

	public void setDanosPreexistentes(String danosPreexistentes) {
		this.danosPreexistentes = danosPreexistentes;
	}

	public String getDescrDpreexistentes() {
		return descrDpreexistentes;
	}

	public void setDescrDpreexistentes(String descrDpreexistentes) {
		this.descrDpreexistentes = descrDpreexistentes;
	}

	public String getDanosLesiones() {
		return danosLesiones;
	}

	public void setDanosLesiones(String danosLesiones) {
		this.danosLesiones = danosLesiones;
	}

	public String getFolioLesionado() {
		return folioLesionado;
	}

	public void setFolioLesionado(String folioLesionado) {
		this.folioLesionado = folioLesionado;
	}

	public String getCircunstanciaDeu() {
		return circunstanciaDeu;
	}

	public void setCircunstanciaDeu(String circunstanciaDeu) {
		this.circunstanciaDeu = circunstanciaDeu;
	}

	public String getCircunstanciaAcr() {
		return circunstanciaAcr;
	}

	public void setCircunstanciaAcr(String circunstanciaAcr) {
		this.circunstanciaAcr = circunstanciaAcr;
	}

	public String getUsoVehiculoDeu() {
		
		return usoVehiculoDeu; 
	}

	public void setUsoVehiculoDeu(String usoVehiculoDeu) {
		this.usoVehiculoDeu = usoVehiculoDeu;
	}

	public String getUsoVehiculoAcr() {
		
		return usoVehiculoAcr;
	}

	public void setUsoVehiculoAcr(String usoVehiculoAcr) {
		this.usoVehiculoAcr = usoVehiculoAcr;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getDescError() {
		return descError;
	}

	public void setDescError(String descError) {
		this.descError = descError;
	}

	public LoteLayoutSipac getLote() {
		return lote;
	}

	public void setLote(LoteLayoutSipac lote) {
		this.lote = lote;
	}

	public ReporteCabina getReporte() {
		return reporte;
	}

	public void setReporte(ReporteCabina reporte) {
		this.reporte = reporte;
	}

	@Override
	public <K> K getKey() {
		return null;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
	

}
