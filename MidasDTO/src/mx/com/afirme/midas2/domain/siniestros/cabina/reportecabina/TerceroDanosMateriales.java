package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;

@Entity(name = "TerceroDanosMateriales")
@Table(name = "TOTERCERODANOSMATERIALES", schema = "MIDAS")
@DiscriminatorValue("DMA")
@PrimaryKeyJoinColumn(name="ESTIMACIONCOBERTURAREPCAB_ID", referencedColumnName="ID")
public class TerceroDanosMateriales extends EstimacionCoberturaReporteCabina {

	private static final long serialVersionUID = 1L;

	@Column(name="DANOS_CUBIERTOS")
	private String danosCubiertos;
	
	@Column(name="DANOS_PREEXISTENTES")
	private String danosPreexistentes;
	
	@Column(name="TIENE_DANOS_PREEXISTENTES")
	private Boolean tieneDanosPreexistentes;
	
	@Column(name="OBSERVACIONES")
	private String observaciones;
	
	@Column(name="PLACA")
	private String placas;
	
	@Column(name="TIPO_TRANSPORTE")
	private String tipoTransporte;
	
	@Column(name="DANOS_MECANICOS")
	private String danosMecanicos;
	
	@Column(name="DANOS_INTERIORES")
	private String danosInteriores;
	
	@Column(name="BASTIADORES")
	private String bastiadores;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch(JoinFetchType.OUTER)
	@JoinColumn(name="ESTADO_ID", referencedColumnName="STATE_ID")
	private EstadoMidas estado;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinFetch(JoinFetchType.OUTER)
	@JoinColumn(name = "TALLER_ID")
	private PrestadorServicio taller;
	
	
	
	public TerceroDanosMateriales() {

	}

	public String getDanosCubiertos() {
		return danosCubiertos;
	}



	public void setDanosCubiertos(String danosCubiertos) {
		this.danosCubiertos = danosCubiertos;
	}



	public String getDanosPreexistentes() {
		return danosPreexistentes;
	}



	public void setDanosPreexistentes(String danosPreexistentes) {
		this.danosPreexistentes = danosPreexistentes;
	}



	public String getObservaciones() {
		return observaciones;
	}



	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}



	public String getPlacas() {
		return placas;
	}



	public void setPlacas(String placas) {
		this.placas = placas;
	}

	public String getTipoTransporte() {
		return tipoTransporte;
	}

	public void setTipoTransporte(String tipoTransporte) {
		this.tipoTransporte = tipoTransporte;
	}

	public EstadoMidas getEstado() {
		return estado;
	}



	public void setEstado(EstadoMidas estado) {
		this.estado = estado;
	}



	public PrestadorServicio getTaller() {
		return taller;
	}



	public void setTaller(PrestadorServicio taller) {
		this.taller = taller;
	}



	public String getDanosMecanicos() {
		return danosMecanicos;
	}



	public void setDanosMecanicos(String danosMecanicos) {
		this.danosMecanicos = danosMecanicos;
	}



	public String getDanosInteriores() {
		return danosInteriores;
	}



	public void setDanosInteriores(String danosInteriores) {
		this.danosInteriores = danosInteriores;
	}



	public String getBastiadores() {
		return bastiadores;
	}



	public void setBastiadores(String bastiadores) {
		this.bastiadores = bastiadores;
	}


	public Boolean getTieneDanosPreexistentes() {
		return tieneDanosPreexistentes;
	}

	public void setTieneDanosPreexistentes(Boolean tieneDanosPreexistentes) {
		this.tieneDanosPreexistentes = tieneDanosPreexistentes;
	}

}