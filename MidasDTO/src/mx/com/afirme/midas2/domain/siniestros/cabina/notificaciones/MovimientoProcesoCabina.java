package mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

@Entity(name = "MovimientoProcesoCabina")
@Table(name = "TCMOVIMIENTOPROCESOCABINA", schema = "MIDAS")
public class MovimientoProcesoCabina implements Entidad {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8830764365442274723L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TCMOVPROCESOCABINA_SEQ_ID_GENERATOR")
	@SequenceGenerator(name="TCMOVPROCESOCABINA_SEQ_ID_GENERATOR", schema="MIDAS", sequenceName="IDTCMOVPROCESOCABINA_SEQ", allocationSize=1)	
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "DESCRIPCION")
	private String descripcion;
	
	@ManyToOne
	@JoinFetch(JoinFetchType.INNER)
	@JoinColumn(name = "PROCESO_ID", referencedColumnName = "id")
	private ProcesoCabina procesoCabina;
	
	@Column(name = "CODIGO")
	private String codigo;
	
	@Column(name = "PLANTILLA")
	private String plantilla;

	@Column(name = "TITULO")
	private String titulo;
	
	@Column(name = "ESPROGRAMADA")
	private Boolean esProgramada;
	
	@Column(name = "TIEMPOATRANSCURRIR")
	private Long tiempoTranscurrir;
	
	@Column(name = "SINOFICINA")
	private Boolean sinOficina = Boolean.FALSE;
	
	@Column(name = "LLEVARREGISTRO")
	private Boolean llevarRegistro = Boolean.FALSE;
	
	@Column(name = "RECURRENTE")
	private Boolean recurrente = Boolean.FALSE;
	
	@Column(name = "TIEMPORECURRENCIA")
	private Long tiempoRecurrencia = 0l;
	
	@Column(name = "ACTIVO")
	private Boolean activo = Boolean.TRUE;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public ProcesoCabina getProcesoCabina() {
		return procesoCabina;
	}

	public void setProcesoCabina(ProcesoCabina procesoCabina) {
		this.procesoCabina = procesoCabina;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getPlantilla() {
		return plantilla;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setPlantilla(String plantilla) {
		this.plantilla = plantilla;
	}

	public Boolean getEsProgramada() {
		return esProgramada;
	}

	public void setEsProgramada(Boolean esProgramada) {
		this.esProgramada = esProgramada;
	}
	
	@Transient
	public boolean esProgramada() {
		return this.tiempoTranscurrir != null && this.tiempoTranscurrir > 0;
	}


	public Long getTiempoTranscurrir() {
		return tiempoTranscurrir;
	}

	public void setTiempoTranscurrir(Long tiempoTranscurrir) {
		this.tiempoTranscurrir = tiempoTranscurrir;
	}

	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof MovimientoProcesoCabina) {
			MovimientoProcesoCabina movProceso = (MovimientoProcesoCabina) object;
			equal = movProceso.getId().equals(this.id);
		}
		return equal;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return descripcion;
	}

	public Boolean getLlevarRegistro() {
		return llevarRegistro;
	}

	public void setLlevarRegistro(Boolean llevarRegistro) {
		this.llevarRegistro = llevarRegistro;
	}

	public Boolean getRecurrente() {
		return recurrente;
	}

	public void setRecurrente(Boolean recurrente) {
		this.recurrente = recurrente;
	}

	public Long getTiempoRecurrencia() {
		return tiempoRecurrencia;
	}

	public void setTiempoRecurrencia(Long tiempoRecurrencia) {
		this.tiempoRecurrencia = tiempoRecurrencia;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	public Boolean getSinOficina() {
		return sinOficina;
	}

	public void setSinOficina(Boolean sinOficina) {
		this.sinOficina = sinOficina;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	
}
