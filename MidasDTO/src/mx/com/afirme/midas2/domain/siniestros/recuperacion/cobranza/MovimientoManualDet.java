package mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;

@Entity
@Table(name="TOSNMOVTOMANUALDET", schema="MIDAS")
public class MovimientoManualDet extends MidasAbstracto implements Entidad{

	private static final long serialVersionUID = 3953566853521890354L;
	
	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDMOVTOMANUALDET_GENERATOR")
	@SequenceGenerator(name="IDMOVTOMANUALDET_GENERATOR", sequenceName="TOSNMOVTOMANUALDET_SEQ", schema="MIDAS", allocationSize=1)
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MOVTO_ID", referencedColumnName="ID")
	private MovimientoManual movimientoManual;
	
	@Column(name="IMPORTE")
	private BigDecimal importe;

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the movimientoManual
	 */
	public MovimientoManual getMovimientoManual() {
		return movimientoManual;
	}

	/**
	 * @param movimientoManual the movimientoManual to set
	 */
	public void setMovimientoManual(MovimientoManual movimientoManual) {
		this.movimientoManual = movimientoManual;
	}

	/**
	 * @return the importe
	 */
	public BigDecimal getImporte() {
		return importe;
	}

	/**
	 * @param importe the importe to set
	 */
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

}
