/**
 * 
 */
package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.catalogos.EnumBase;

/**
 * @author simavera
 *
 */
@Entity(name = "CoberturaReporteCabina")
@Table(name = "TOCOBERTURAREPORTECABINA", schema = "MIDAS")
public class CoberturaReporteCabina implements Entidad{

	private static final long serialVersionUID = 3706301876991692696L;
	
	public enum ClaveTipoCalculo implements EnumBase<String>{
		ACCIDENTES_AUT_CONDUCTOR("AAC"),
		ASISTENCIA_JURIDICA("AJ"),
		ASISTENCIA_VIAJES_VIALKM0("AV"),
		ADAPTACIONES_CONVERSIONES("AYC"),
		PROTECCION_MAXIMA_AFIRME("CCE"),
		DANIOS_MATERIALES("DM"),
		DANIOS_OCASIONADOS_CARGA("DXC"),
		EXENCION_DEDUCIBLE_DM("EDDM"),
		EXENCION_DEDUCIBLE_RT("EDRT"),
		EQUIPO_ESPECIAL("EQE"),
		EXTENSION_RESP_CIVIL("EXRC"),
		GASTOS_MEDICOS("GMO"),
		PERDIDA_TOTAL_DM("PTDM"),
		RESPONSABILIDAD_CIVIL("RC"),
		RESP_CIVIL_EXCESO_MUERTE("CEM"),
		RESP_CIVIL_USA_CANADA_LUC("RCUSA"),
		RESP_CIVIL_VIAJERO("RCV"),
		ROBO_TOTAL("RT"),
		PROTECCION_AUTO_SIGUE_AFIRME("SIGA");

	    private final String clave;

	    private ClaveTipoCalculo(String clave) {
	        this.clave = clave;
	    }

	    @Override
	    public String toString(){
	       return clave;
	    }

		@Override
		public String getValue() {
			return clave;
		}

		@Override
		public String getLabel() {
			return clave;
		}

	}
	
	@Id
	@SequenceGenerator(name = "IDCOBERTURAREPORTECABINA_SEQ_GENERADOR",allocationSize = 1, sequenceName = "TOCOBERTURAREPORTECABINA_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "IDCOBERTURAREPORTECABINA_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id;
	
	//bi-directional many-to-one association to IncisoReporteCabina
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="INCISOREPORTECABINA_ID", nullable = false)
	private IncisoReporteCabina incisoReporteCabina;
	
	//bi-directional many-to-one association to SubRamoDTO
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SUBRAMO_ID", nullable = false)
	private SubRamoDTO subRamoDTO;
	
	@Column(name="VALORPRIMANETA")
	private Double valorPrimaNeta;
	
	@Column(name="VALORSUMAASEGURADA")
	private Double valorSumaAsegurada;
	
	@Column(name="VALORCOASEGURO")
	private Double valorCoaseguro;
	
	@Column(name="VALORDEDUCIBLE")
	private Double valorDeducible;
	
	@Column(name="CODIGOUSUARIOAUTREASEGURO", length = 8 )
	private String codigoUsuarioAutReaseguro;
	
	@Column(name="CLAVEOBLIGATORIEDAD")
	private Short claveObligatoriedad;
	
	@Column(name="CLAVECONTRATO")
	private Short claveContrato;
	
	@Column(name="VALORCUOTA")
	private Double valorCuota;
	
	@Column(name="PORCENTAJECOASEGURO")
	private Double porcentajeCoaseguro;
	
	@Column(name="CLAVEAUTCOASEGURO")
	private Short claveAutCoaseguro;
	
	@Column(name="CODIGOUSUARIOAUTCOASEGURO", length = 8)
	private String codigoUsuarioAutCoaseguro;
	
	@Column(name="PORCENTAJEDEDUCIBLE")
	private Double porcentajeDeducible;
	
	@Column(name="CLAVEAUTDEDUCBLE")
	private Short claveAutDeducible;
	
	@Column(name="CODIGOUSUARIOAUTDEDUCIBLE", length = 8)
	private String codigoUsuarioAutDeducible;
	
	@Column(name="NUMEROAGRUPACION")
	private Short numeroAgrupacion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHASOLAUTCOASEGURO", length = 7 )
	private Date fechaSolAutCoaSeguro;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAAUTCOASEGURO", length = 7)
	private Date fechaAutCoaSeguro;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHASOLAUTDEDUCIBLE", length = 7)
	private Date fechaSolAutDeducible;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAUTDEDUCIBLE", length = 7)
	private Date fechaAutDeducible;
	
	@Column(name="CLAVETIPODEDUCIBLE")
	private Short claveTipoDeducible;
	
	@Column(name="CLAVETIPOLIMITEDEDUCIBLE")
	private Short claveTipoLimiteDeducible;
	
	@Column(name="VALORMINIMOLIMITEDEDUCIBLE")
	private Double valorMinimoLimiteDeducible;
	
	@Column(name="VALORMAXIMOLIMITEDEDUCIBLE")
	private Double valorMaximoLimiteDeducible;

	@Column(name="CLAVEFACULTATIVO")
	private Short claveFacultativo;
	
	@Column(name="AGRUPADORTARIFA_ID")
	private Integer agrupadorTarifaId;
	
	@Column(name="VERAGRUPADORTARIFA_ID")
	private Integer verAgrupadorTarifaId;
	
	@Column(name="VERSIONCARGA_ID")
	private Integer verCargaId;
	
	@Column(name="VERSIONTARIFA_ID")
	private Integer verTarifaId;
	
	@Column(name="TARIFAEXT_ID")
	private Integer tarifaExtId;
	
	@Column(name="VERSIONTARIFAEXT_ID")
	private Integer verTarifaExtId;
	
	@Column(name="DIASSALARIOMINIMO")
	private Integer diasSalarioMinimo;
	
	@Column(name="VALORPRIMADIARIA")
	private Double valorPrimaDiaria;
	
	@Column(name="CLAVETIPOCALCULO", length = 6)
	private String claveTipoCalculo;
	
	@Column(name="MONTO_RECHAZO")
	private String montoRechazo;
	
	//bi-directional many-to-one association to CoberturaDTO
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="COBERTURA_ID", nullable = false)
	private CoberturaDTO coberturaDTO;
	
	@Column(name="VALORSUMAASEGURADAMIN")
    private Double valorSumaAseguradaMin;

	@Column(name="VALORSUMAASEGURADAMAX")
    private Double valorSumaAseguradaMax;
    
	@Column(name="DESCRIPCIONDEDUCIBLE", length = 50)
    private String descripcionDeducible;
	
	@Column(name="SINIESTRADO")
    private Boolean siniestrado;
	
	@OneToMany(fetch = FetchType.LAZY )
	@JoinColumn(name="COBERTURAREPORTECABINA_ID", updatable = false,nullable = false, insertable = false)
	private List<EstimacionCoberturaReporteCabina> lEstimacionCoberturaReporteCabina;
		
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID", referencedColumnName="COBERTURAREPORTECABINA_ID", updatable=false, insertable=false, nullable=false)
	@JoinFetch(JoinFetchType.OUTER)
	private ReporteRoboSiniestro reporteRoboSiniestro;
	
	public CoberturaReporteCabina() {
		super();
		this.siniestrado = false;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the incisoReporteCabina
	 */
	public IncisoReporteCabina getIncisoReporteCabina() {
		return incisoReporteCabina;
	}

	/**
	 * @param incisoReporteCabina the incisoReporteCabina to set
	 */
	public void setIncisoReporteCabina(IncisoReporteCabina incisoReporteCabina) {
		this.incisoReporteCabina = incisoReporteCabina;
	}

	/**
	 * @return the subRamoDTO
	 */
	public SubRamoDTO getSubRamoDTO() {
		return subRamoDTO;
	}

	/**
	 * @param subRamoDTO the subRamoDTO to set
	 */
	public void setSubRamoDTO(SubRamoDTO subRamoDTO) {
		this.subRamoDTO = subRamoDTO;
	}

	/**
	 * @return the valorPrimaNeta
	 */
	public Double getValorPrimaNeta() {
		return valorPrimaNeta;
	}

	/**
	 * @param valorPrimaNeta the valorPrimaNeta to set
	 */
	public void setValorPrimaNeta(Double valorPrimaNeta) {
		this.valorPrimaNeta = valorPrimaNeta;
	}

	/**
	 * @return the valorSumaAsegurada
	 */
	public Double getValorSumaAsegurada() {
		return valorSumaAsegurada;
	}

	/**
	 * @param valorSumaAsegurada the valorSumaAsegurada to set
	 */
	public void setValorSumaAsegurada(Double valorSumaAsegurada) {
		this.valorSumaAsegurada = valorSumaAsegurada;
	}

	/**
	 * @return the valorCoaseguro
	 */
	public Double getValorCoaseguro() {
		return valorCoaseguro;
	}

	/**
	 * @param valorCoaseguro the valorCoaseguro to set
	 */
	public void setValorCoaseguro(Double valorCoaseguro) {
		this.valorCoaseguro = valorCoaseguro;
	}

	/**
	 * @return the valorDeducible
	 */
	public Double getValorDeducible() {
		return valorDeducible;
	}

	/**
	 * @param valorDeducible the valorDeducible to set
	 */
	public void setValorDeducible(Double valorDeducible) {
		this.valorDeducible = valorDeducible;
	}

	/**
	 * @return the codigoUsuarioAutReaseguro
	 */
	public String getCodigoUsuarioAutReaseguro() {
		return codigoUsuarioAutReaseguro;
	}

	/**
	 * @param codigoUsuarioAutReaseguro the codigoUsuarioAutReaseguro to set
	 */
	public void setCodigoUsuarioAutReaseguro(String codigoUsuarioAutReaseguro) {
		this.codigoUsuarioAutReaseguro = codigoUsuarioAutReaseguro;
	}

	/**
	 * @return the claveObligatoriedad
	 */
	public Short getClaveObligatoriedad() {
		return claveObligatoriedad;
	}

	/**
	 * @param claveObligatoriedad the claveObligatoriedad to set
	 */
	public void setClaveObligatoriedad(Short claveObligatoriedad) {
		this.claveObligatoriedad = claveObligatoriedad;
	}

	/**
	 * @return the claveContrato
	 */
	public Short getClaveContrato() {
		return claveContrato;
	}

	/**
	 * @param claveContrato the claveContrato to set
	 */
	public void setClaveContrato(Short claveContrato) {
		this.claveContrato = claveContrato;
	}

	/**
	 * @return the valorCuota
	 */
	public Double getValorCuota() {
		return valorCuota;
	}

	/**
	 * @param valorCuota the valorCuota to set
	 */
	public void setValorCuota(Double valorCuota) {
		this.valorCuota = valorCuota;
	}

	/**
	 * @return the porcentajeCoaseguro
	 */
	public Double getPorcentajeCoaseguro() {
		return porcentajeCoaseguro;
	}

	/**
	 * @param porcentajeCoaseguro the porcentajeCoaseguro to set
	 */
	public void setPorcentajeCoaseguro(Double porcentajeCoaseguro) {
		this.porcentajeCoaseguro = porcentajeCoaseguro;
	}

	/**
	 * @return the claveAutCoaseguro
	 */
	public Short getClaveAutCoaseguro() {
		return claveAutCoaseguro;
	}

	/**
	 * @param claveAutCoaseguro the claveAutCoaseguro to set
	 */
	public void setClaveAutCoaseguro(Short claveAutCoaseguro) {
		this.claveAutCoaseguro = claveAutCoaseguro;
	}

	/**
	 * @return the codigoUsuarioAutCoaseguro
	 */
	public String getCodigoUsuarioAutCoaseguro() {
		return codigoUsuarioAutCoaseguro;
	}

	/**
	 * @param codigoUsuarioAutCoaseguro the codigoUsuarioAutCoaseguro to set
	 */
	public void setCodigoUsuarioAutCoaseguro(String codigoUsuarioAutCoaseguro) {
		this.codigoUsuarioAutCoaseguro = codigoUsuarioAutCoaseguro;
	}

	/**
	 * @return the porcentajeDeducible
	 */
	public Double getPorcentajeDeducible() {
		return porcentajeDeducible;
	}

	/**
	 * @param porcentajeDeducible the porcentajeDeducible to set
	 */
	public void setPorcentajeDeducible(Double porcentajeDeducible) {
		this.porcentajeDeducible = porcentajeDeducible;
	}

	/**
	 * @return the claveAutDeducible
	 */
	public Short getClaveAutDeducible() {
		return claveAutDeducible;
	}

	/**
	 * @param claveAutDeducible the claveAutDeducible to set
	 */
	public void setClaveAutDeducible(Short claveAutDeducible) {
		this.claveAutDeducible = claveAutDeducible;
	}

	/**
	 * @return the codigoUsuarioAutDeducible
	 */
	public String getCodigoUsuarioAutDeducible() {
		return codigoUsuarioAutDeducible;
	}

	/**
	 * @param codigoUsuarioAutDeducible the codigoUsuarioAutDeducible to set
	 */
	public void setCodigoUsuarioAutDeducible(String codigoUsuarioAutDeducible) {
		this.codigoUsuarioAutDeducible = codigoUsuarioAutDeducible;
	}

	/**
	 * @return the numeroAgrupacion
	 */
	public Short getNumeroAgrupacion() {
		return numeroAgrupacion;
	}

	/**
	 * @param numeroAgrupacion the numeroAgrupacion to set
	 */
	public void setNumeroAgrupacion(Short numeroAgrupacion) {
		this.numeroAgrupacion = numeroAgrupacion;
	}

	/**
	 * @return the fechaSolAutCoaSeguro
	 */
	public Date getFechaSolAutCoaSeguro() {
		return fechaSolAutCoaSeguro;
	}

	/**
	 * @param fechaSolAutCoaSeguro the fechaSolAutCoaSeguro to set
	 */
	public void setFechaSolAutCoaSeguro(Date fechaSolAutCoaSeguro) {
		this.fechaSolAutCoaSeguro = fechaSolAutCoaSeguro;
	}

	/**
	 * @return the fechaAutCoaSeguro
	 */
	public Date getFechaAutCoaSeguro() {
		return fechaAutCoaSeguro;
	}

	/**
	 * @param fechaAutCoaSeguro the fechaAutCoaSeguro to set
	 */
	public void setFechaAutCoaSeguro(Date fechaAutCoaSeguro) {
		this.fechaAutCoaSeguro = fechaAutCoaSeguro;
	}

	/**
	 * @return the fechaSolAutDeducible
	 */
	public Date getFechaSolAutDeducible() {
		return fechaSolAutDeducible;
	}

	/**
	 * @param fechaSolAutDeducible the fechaSolAutDeducible to set
	 */
	public void setFechaSolAutDeducible(Date fechaSolAutDeducible) {
		this.fechaSolAutDeducible = fechaSolAutDeducible;
	}

	/**
	 * @return the fechaAutDeducible
	 */
	public Date getFechaAutDeducible() {
		return fechaAutDeducible;
	}

	/**
	 * @param fechaAutDeducible the fechaAutDeducible to set
	 */
	public void setFechaAutDeducible(Date fechaAutDeducible) {
		this.fechaAutDeducible = fechaAutDeducible;
	}

	/**
	 * @return the claveTipoDeducible
	 */
	public Short getClaveTipoDeducible() {
		return claveTipoDeducible;
	}

	/**
	 * @param claveTipoDeducible the claveTipoDeducible to set
	 */
	public void setClaveTipoDeducible(Short claveTipoDeducible) {
		this.claveTipoDeducible = claveTipoDeducible;
	}

	/**
	 * @return the claveTipoLimiteDeducible
	 */
	public Short getClaveTipoLimiteDeducible() {
		return claveTipoLimiteDeducible;
	}

	/**
	 * @param claveTipoLimiteDeducible the claveTipoLimiteDeducible to set
	 */
	public void setClaveTipoLimiteDeducible(Short claveTipoLimiteDeducible) {
		this.claveTipoLimiteDeducible = claveTipoLimiteDeducible;
	}

	/**
	 * @return the valorMinimoLimiteDeducible
	 */
	public Double getValorMinimoLimiteDeducible() {
		return valorMinimoLimiteDeducible;
	}

	/**
	 * @param valorMinimoLimiteDeducible the valorMinimoLimiteDeducible to set
	 */
	public void setValorMinimoLimiteDeducible(Double valorMinimoLimiteDeducible) {
		this.valorMinimoLimiteDeducible = valorMinimoLimiteDeducible;
	}

	/**
	 * @return the valorMaximoLimiteDeducible
	 */
	public Double getValorMaximoLimiteDeducible() {
		return valorMaximoLimiteDeducible;
	}

	/**
	 * @param valorMaximoLimiteDeducible the valorMaximoLimiteDeducible to set
	 */
	public void setValorMaximoLimiteDeducible(Double valorMaximoLimiteDeducible) {
		this.valorMaximoLimiteDeducible = valorMaximoLimiteDeducible;
	}

	/**
	 * @return the claveFacultativo
	 */
	public Short getClaveFacultativo() {
		return claveFacultativo;
	}

	/**
	 * @param claveFacultativo the claveFacultativo to set
	 */
	public void setClaveFacultativo(Short claveFacultativo) {
		this.claveFacultativo = claveFacultativo;
	}

	/**
	 * @return the agrupadorTarifaId
	 */
	public Integer getAgrupadorTarifaId() {
		return agrupadorTarifaId;
	}

	/**
	 * @param agrupadorTarifaId the agrupadorTarifaId to set
	 */
	public void setAgrupadorTarifaId(Integer agrupadorTarifaId) {
		this.agrupadorTarifaId = agrupadorTarifaId;
	}

	/**
	 * @return the verAgrupadorTarifaId
	 */
	public Integer getVerAgrupadorTarifaId() {
		return verAgrupadorTarifaId;
	}

	/**
	 * @param verAgrupadorTarifaId the verAgrupadorTarifaId to set
	 */
	public void setVerAgrupadorTarifaId(Integer verAgrupadorTarifaId) {
		this.verAgrupadorTarifaId = verAgrupadorTarifaId;
	}

	/**
	 * @return the verCargaId
	 */
	public Integer getVerCargaId() {
		return verCargaId;
	}

	/**
	 * @param verCargaId the verCargaId to set
	 */
	public void setVerCargaId(Integer verCargaId) {
		this.verCargaId = verCargaId;
	}

	/**
	 * @return the verTarifaId
	 */
	public Integer getVerTarifaId() {
		return verTarifaId;
	}

	/**
	 * @param verTarifaId the verTarifaId to set
	 */
	public void setVerTarifaId(Integer verTarifaId) {
		this.verTarifaId = verTarifaId;
	}

	/**
	 * @return the tarifaExtId
	 */
	public Integer getTarifaExtId() {
		return tarifaExtId;
	}

	/**
	 * @param tarifaExtId the tarifaExtId to set
	 */
	public void setTarifaExtId(Integer tarifaExtId) {
		this.tarifaExtId = tarifaExtId;
	}

	/**
	 * @return the verTarifaExtId
	 */
	public Integer getVerTarifaExtId() {
		return verTarifaExtId;
	}

	/**
	 * @param verTarifaExtId the verTarifaExtId to set
	 */
	public void setVerTarifaExtId(Integer verTarifaExtId) {
		this.verTarifaExtId = verTarifaExtId;
	}

	/**
	 * @return the diasSalarioMinimo
	 */
	public Integer getDiasSalarioMinimo() {
		return diasSalarioMinimo;
	}

	/**
	 * @param diasSalarioMinimo the diasSalarioMinimo to set
	 */
	public void setDiasSalarioMinimo(Integer diasSalarioMinimo) {
		this.diasSalarioMinimo = diasSalarioMinimo;
	}

	/**
	 * @return the valorPrimaDiaria
	 */
	public Double getValorPrimaDiaria() {
		return valorPrimaDiaria;
	}

	/**
	 * @param valorPrimaDiaria the valorPrimaDiaria to set
	 */
	public void setValorPrimaDiaria(Double valorPrimaDiaria) {
		this.valorPrimaDiaria = valorPrimaDiaria;
	}

	/**
	 * @return the claveTipoCalculo
	 */
	public String getClaveTipoCalculo() {
		return claveTipoCalculo;
	}

	/**
	 * @param claveTipoCalculo the claveTipoCalculo to set
	 */
	public void setClaveTipoCalculo(String claveTipoCalculo) {
		this.claveTipoCalculo = claveTipoCalculo;
	}

	/**
	 * @return the coberturaDTO
	 */
	public CoberturaDTO getCoberturaDTO() {
		return coberturaDTO;
	}

	/**
	 * @param coberturaDTO the coberturaDTO to set
	 */
	public void setCoberturaDTO(CoberturaDTO coberturaDTO) {
		this.coberturaDTO = coberturaDTO;
	}
	
	public Boolean getSiniestrado() {
		return siniestrado;
	}

	public void setSiniestrado(Boolean siniestrado) {
		this.siniestrado = siniestrado;
	}

	/**
	 * @return the valorSumaAseguradaMin
	 */
	public Double getValorSumaAseguradaMin() {
		return valorSumaAseguradaMin;
	}

	/**
	 * @param valorSumaAseguradaMin the valorSumaAseguradaMin to set
	 */
	public void setValorSumaAseguradaMin(Double valorSumaAseguradaMin) {
		this.valorSumaAseguradaMin = valorSumaAseguradaMin;
	}

	/**
	 * @return the valorSumaAseguradaMax
	 */
	public Double getValorSumaAseguradaMax() {
		return valorSumaAseguradaMax;
	}

	/**
	 * @param valorSumaAseguradaMax the valorSumaAseguradaMax to set
	 */
	public void setValorSumaAseguradaMax(Double valorSumaAseguradaMax) {
		this.valorSumaAseguradaMax = valorSumaAseguradaMax;
	}

	/**
	 * @return the descripcionDeducible
	 */
	public String getDescripcionDeducible() {
		return descripcionDeducible;
	}

	/**
	 * @param descripcionDeducible the descripcionDeducible to set
	 */
	public void setDescripcionDeducible(String descripcionDeducible) {
		this.descripcionDeducible = descripcionDeducible;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}

	public List<EstimacionCoberturaReporteCabina> getlEstimacionCoberturaReporteCabina() {
		return lEstimacionCoberturaReporteCabina;
	}

	public void setlEstimacionCoberturaReporteCabina(
			List<EstimacionCoberturaReporteCabina> lEstimacionCoberturaReporteCabina) {
		this.lEstimacionCoberturaReporteCabina = lEstimacionCoberturaReporteCabina;
	}

	public String getMontoRechazo() {
		return montoRechazo;
	}

	public void setMontoRechazo(String montoRechazo) {
		this.montoRechazo = montoRechazo;
	}

	public ReporteRoboSiniestro getReporteRoboSiniestro() {
		return reporteRoboSiniestro;
	}

	public void setReporteRoboSiniestro(ReporteRoboSiniestro reporteRoboSiniestro) {
		this.reporteRoboSiniestro = reporteRoboSiniestro;
	}
	
}
