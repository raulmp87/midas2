package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.gastoajuste;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.catalogos.EnumBase;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;

@Entity(name = "GastoAjusteReporte")
@Table(name = "TOGASTOAJUSTEREPORTE", schema = "MIDAS")
public class GastoAjusteReporte extends MidasAbstracto implements Entidad{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8076826626310854999L;
	
	public static enum Origen implements EnumBase<Short>{
		PRESTADOR((short)1, "Prestador"),  SERVICIO_SINIESTRO((short)2, "Servicio Siniestro");		
		
		Origen(Short origen, String label) {
			this.origen = origen;
			this.label = label;
		}
		private Short origen;
		
		private String label;

		@Override
		public Short getValue() {
			return origen;
		}
		@Override
		public String getLabel() {
			return label;
		}

	};
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOGASTOAJUSTEREPORTE_SEQ_ID_GENERATOR")
	@SequenceGenerator(name="TOGASTOAJUSTEREPORTE_SEQ_ID_GENERATOR", schema="MIDAS", sequenceName="IDTOGASTOAJUSTEREPORTE_SEQ", allocationSize=1)	
	@Column(name = "ID")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "REPORTECABINA_ID", referencedColumnName = "id", nullable = false, updatable = false)
	private ReporteCabina reporteCabina;
	
	@ManyToOne
	@JoinColumn(name = "PRESTADORSERVICIO_ID", referencedColumnName = "id")
	private PrestadorServicio prestadorServicio;
	
	@ManyToOne
	@JoinColumn(name = "SERVICIO_SINIESTRO_ID", referencedColumnName = "id")
	private ServicioSiniestro servicioSiniestro;
	
	@Column(name = "TIPOPRESTADORSERVICIO")
	private String tipoPrestadorServicio;
	
	@Column(name = "ORIGEN")
	private Short origen;
	
	@Column(name = "NUMEROFACTURA")
	private String numeroFactura;
	
	@Column(name = "MOTIVOSITUACION")
	private Integer motivoSituacion;
	
	@Column(name = "NUMEROREPORTEEXTERNO")
	private String numeroReporteExterno;
	
	@Column(name = "TIPOGRUA")
	private Integer tipoGrua;
	
	@ManyToOne
	@JoinColumn(name = "TALLERASIGNADO_ID", referencedColumnName = "id")
	private PrestadorServicio tallerAsignado;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ReporteCabina getReporteCabina() {
		return reporteCabina;
	}

	public void setReporteCabina(ReporteCabina reporteCabina) {
		this.reporteCabina = reporteCabina;
	}

	public ServicioSiniestro getServicioSiniestro() {
		return servicioSiniestro;
	}

	public void setServicioSiniestro(ServicioSiniestro servicioSiniestro) {
		this.servicioSiniestro = servicioSiniestro;
	}

	public PrestadorServicio getPrestadorServicio() {
		return prestadorServicio;
	}

	public void setPrestadorServicio(PrestadorServicio prestadorServicio) {
		this.prestadorServicio = prestadorServicio;
	}

	public String getNumeroFactura() {
		return numeroFactura;
	}

	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	public Integer getMotivoSituacion() {
		return motivoSituacion;
	}

	public void setMotivoSituacion(Integer motivoSituacion) {
		this.motivoSituacion = motivoSituacion;
	}

	public String getNumeroReporteExterno() {
		return numeroReporteExterno;
	}

	public void setNumeroReporteExterno(String numeroReporteExterno) {
		this.numeroReporteExterno = numeroReporteExterno;
	}

	public Integer getTipoGrua() {
		return tipoGrua;
	}

	public void setTipoGrua(Integer tipoGrua) {
		this.tipoGrua = tipoGrua;
	}

	public PrestadorServicio getTallerAsignado() {
		return tallerAsignado;
	}

	public void setTallerAsignado(PrestadorServicio tallerAsignado) {
		this.tallerAsignado = tallerAsignado;
	}

	public String getTipoPrestadorServicio() {
		return tipoPrestadorServicio;
	}

	public void setTipoPrestadorServicio(String tipoPrestadorServicio) {
		this.tipoPrestadorServicio = tipoPrestadorServicio;
	}

	public Short getOrigen() {
		return origen;
	}

	public void setOrigen(Short origen) {
		this.origen = origen;
	}

	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof GastoAjusteReporte) {
			GastoAjusteReporte gastoAjuste = (GastoAjusteReporte) object;
			equal = gastoAjuste.getId().equals(this.id);
		}
		return equal;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
}
