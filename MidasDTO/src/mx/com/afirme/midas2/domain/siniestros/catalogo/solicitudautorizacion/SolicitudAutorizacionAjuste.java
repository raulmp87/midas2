package mx.com.afirme.midas2.domain.siniestros.catalogo.solicitudautorizacion;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;

@Entity
@Table(name = "TOSOLAUTORIZACIONAJUSTE", schema = "MIDAS")
public class SolicitudAutorizacionAjuste extends MidasAbstracto implements Entidad {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOSOLAUTORIZACIONAJUSTE_SEQ")
	@SequenceGenerator(name = "IDTOSOLAUTORIZACIONAJUSTE_SEQ",  schema="MIDAS", sequenceName = "IDTOSOLAUTORIZACIONAJUSTE_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "CAUSA_MOVIMIENTO")
	private String causaMovimiento;
	
	@Column(name = "ESTIMACION_NUEVA")
	private BigDecimal estimacionNueva;
	
	@Column(name="ESTATUS")
	private Integer estatus;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinFetch(JoinFetchType.OUTER)
	@JoinColumn(name = "ESTIMACIONCOBERTURA_ID", referencedColumnName = "ID")
	private EstimacionCoberturaReporteCabina estimacionCobertura;
	
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SOLICITUDAUTORIZACION_ID")
	private SolicitudAutorizacion solicitudAutorizacion;

	

	public String getCausaMovimiento() {
		return causaMovimiento;
	}

	public void setCausaMovimiento(String causaMovimiento) {
		this.causaMovimiento = causaMovimiento;
	}

	public EstimacionCoberturaReporteCabina getEstimacionCobertura() {
		return estimacionCobertura;
	}

	public void setEstimacionCobertura(
			EstimacionCoberturaReporteCabina estimacionCobertura) {
		this.estimacionCobertura = estimacionCobertura;
	}

	public BigDecimal getEstimacionNueva() {
		return estimacionNueva;
	}

	public void setEstimacionNueva(BigDecimal estimacionNueva) {
		this.estimacionNueva = estimacionNueva;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SolicitudAutorizacion getSolicitudAutorizacion() {
		return solicitudAutorizacion;
	}

	public void setSolicitudAutorizacion(SolicitudAutorizacion solicitudAutorizacion) {
		this.solicitudAutorizacion = solicitudAutorizacion;
	}
	
	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	@Override
	public <K> K getKey() {
		return  (K) this.id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	
	

}
