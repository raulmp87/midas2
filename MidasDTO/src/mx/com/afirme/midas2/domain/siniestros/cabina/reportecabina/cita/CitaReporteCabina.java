/**
 * 
 */
package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.cita;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * @author jair.rivas
 *
 */
@Entity(name = "CitaReporteCabina")
@Table(name = "TOCITA", schema = "MIDAS")
public class CitaReporteCabina implements Entidad{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1104651847439697010L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOCITA_SEQ_ID_GENERATOR")
	@SequenceGenerator(name="TOCITA_SEQ_ID_GENERATOR", schema="MIDAS", sequenceName="IDTOCITA_SEQ", allocationSize=1)	
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "ASEGURADO", nullable = false, length = 100)
	private String asegurado;
	
	@Column(name = "LADA_TELEFONO", length = 3)
	private String ladaTelefono;
	
	@Column(name = "TELEFONO", length = 8)
	private String telefono;
	
	@Column(name = "LADA_CELULAR", length = 3)
	private String ladaCelular;
	
	@Column(name = "CELULAR", nullable = false, length = 8)
	private String celular;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CITA", nullable = false, length = 7)
	private Date fechaCita;
	
	@Column(name = "HORA_CITA", nullable = false, length = 5)
	private String horaCita;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_TERMINO", length = 7)
	private Date fechaTermino;
	
	@Column(name = "HORA_TERMINO", length = 5)
	private String horaTermino;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the asegurado
	 */
	public String getAsegurado() {
		return asegurado;
	}

	/**
	 * @param asegurado the asegurado to set
	 */
	public void setAsegurado(String asegurado) {
		this.asegurado = asegurado;
	}

	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}

	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	/**
	 * @return the celular
	 */
	public String getCelular() {
		return celular;
	}

	/**
	 * @param celular the celular to set
	 */
	public void setCelular(String celular) {
		this.celular = celular;
	}

	/**
	 * @return the fechaCita
	 */
	public Date getFechaCita() {
		return fechaCita;
	}

	/**
	 * @param fechaCita the fechaCita to set
	 */
	public void setFechaCita(Date fechaCita) {
		this.fechaCita = fechaCita;
	}

	/**
	 * @return the horaCita
	 */
	public String getHoraCita() {
		return horaCita;
	}

	/**
	 * @param horaCita the horaCita to set
	 */
	public void setHoraCita(String horaCita) {
		this.horaCita = horaCita;
	}

	/**
	 * @return the fechaTermino
	 */
	public Date getFechaTermino() {
		return fechaTermino;
	}

	/**
	 * @param fechaTermino the fechaTermino to set
	 */
	public void setFechaTermino(Date fechaTermino) {
		this.fechaTermino = fechaTermino;
	}

	/**
	 * @return the horaTermino
	 */
	public String getHoraTermino() {
		return horaTermino;
	}

	/**
	 * @param horaTermino the horaTermino to set
	 */
	public void setHoraTermino(String horaTermino) {
		this.horaTermino = horaTermino;
	}

	/**
	 * @return the ladaTelefono
	 */
	public String getLadaTelefono() {
		return ladaTelefono;
	}

	/**
	 * @param ladaTelefono the ladaTelefono to set
	 */
	public void setLadaTelefono(String ladaTelefono) {
		this.ladaTelefono = ladaTelefono;
	}

	/**
	 * @return the ladaCelular
	 */
	public String getLadaCelular() {
		return ladaCelular;
	}

	/**
	 * @param ladaCelular the ladaCelular to set
	 */
	public void setLadaCelular(String ladaCelular) {
		this.ladaCelular = ladaCelular;
	}
	
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof CitaReporteCabina) {
			CitaReporteCabina citaReporteCabina = (CitaReporteCabina) object;
			equal = citaReporteCabina.getId().equals(this.id);
		}
		return equal;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
}
