package mx.com.afirme.midas2.domain.siniestros.liquidacion.parametros;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;

@Entity
@Table(name = "TOPARAMAUTORIZALIQUIDACIONDET", schema = "MIDAS")
public class ParametroAutorizacionLiquidacionDetalle extends MidasAbstracto implements Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static enum TipoParametroAutLiquidacion{
		OFICINA(1),
		ESTADO(2),
		PROVEEDOR(3),
		USUARIO_SOLICITADOR(4),
		USUARIO_AUTORIZADOR(5);
		
		private int value;
		
		private TipoParametroAutLiquidacion(int value){
			this.value=value;
		}
		
		public int getValue(){
			return value;
		}
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TOPARAMAUTLIQDET_SEQ")
	@SequenceGenerator(name = "TOPARAMAUTLIQDET_SEQ",  schema="MIDAS", sequenceName = "TOPARAMAUTLIQDET_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private long id;
	
	@Column(name = "TIPO_RELACION")
	private String tipoRelacion;
	
	@Column(name = "ID_RELACIONADO")
	private String idRelacionado;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PARAMETRO_ID", referencedColumnName = "ID")	
	private ParametroAutorizacionLiquidacion parametro;
	

	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTipoRelacion() {
		return tipoRelacion;
	}

	public void setTipoRelacion(String tipoRelacion) {
		this.tipoRelacion = tipoRelacion;
	}

	public String getIdRelacionado() {
		return idRelacionado;
	}

	public void setIdRelacionado(String idRelacionado) {
		this.idRelacionado = idRelacionado;
	}

	public ParametroAutorizacionLiquidacion getParametro() {
		return parametro;
	}

	public void setParametro(ParametroAutorizacionLiquidacion parametro) {
		this.parametro = parametro;
	}
	
	

}
