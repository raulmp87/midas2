package mx.com.afirme.midas2.domain.siniestros.recuperacion;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.EnumBase;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 10-jul-2015 07:07:10 p.m.
 */
@Entity(name = "CartaCompania")
@Table(name = "TOSNCARTACOMPANIA", schema = "MIDAS")
public class CartaCompania extends MidasAbstracto implements Comparable<CartaCompania> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TOSNCARTACOMPANIA_SEQ_GENERADOR",allocationSize = 1, sequenceName = "TOSNCARTACOMPANIA_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TOSNCARTACOMPANIA_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id ; 
	
	@ManyToOne(fetch=FetchType.LAZY, cascade=CascadeType.PERSIST)
	@JoinColumn(name = "RECUPERACION_ID", referencedColumnName = "RECUPERACION_ID")
	private RecuperacionCompania recuperacion;
	
	@Column(name = "ESTATUS")	
	private String estatus;
	
	@Column(name = "CAUSA_RECHAZO")	
	private String causaRechazo;
	
	@Column(name = "CAUSA_NO_ELABORACION")	
	private String causasNoElaboracion;
	
	@Column(name = "COMENTARIOS")	
	private String comentarios;
	
	@Column(name = "COMENTARIOS_REDOC")	
	private String comentariosRedoc;
	
	@Column(name = "CONCEPTO")	
	private String concepto;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_ACUSE")	
	private Date fechaAcuse;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_ELABORACION")	
	private Date fechaElaboracion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_ENTREGA")	
	private Date fechaEntrega;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_IMPRESION")	
	private Date fechaImpresion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_RECHAZO")	
	private Date fechaRechazo;
	
	@Column(name = "FOLIO")	
	private Long folio;
	
	@Column(name = "MONTOARECUPERAR")	
	private BigDecimal montoARecuperar;
	@Column(name = "MONTO_EXCLUSION")	
	private BigDecimal montoExclusion;
	@Column(name = "MOTIVO_EXCLUSION")	
	private String motivoExclusion;
	@Column(name = "TIPO_RECHAZO")	
	private String tipoRechazo;
	@Column(name = "TIPO_REDOCUMENTACION")	
	private String tipoRedocumentacion;
	@Column(name = "USUARIO_ELABORACION")	
	private String usuarioElaboracion;
	@Column(name = "USUARIO_ENTREGA")	
	private String usuarioEntrega;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "OFICINA_RECEPCION")	
	private Oficina oficinaRecepcion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_ENVIO")	
	private Date fechaEnvio;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_RECEPCION")	
	private Date fechaRecepcion;
	
	@Column(name = "USUARIO_ENVIO")
	private String usuarioEnvio;
	
	@Column(name = "USUARIO_RECEPCION")
	private String usuarioRecepcion;
	
	@Column(name = "NOMBRE_PERSONA_RECEPCION")
	private String nombrePersonaRecepcion;
	
	@Column(name = "USUARIO_ELIMINA_RECEPCION")
	private String usuarioEliminaRecepcion;
	
	@Column(name = "USUARIO_ELIMINA_ENVIO")
	private String usuarioEliminaEnvio;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_ELIMINA_RECEPCION")	
	private Date fechaEliminaRecepcion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_ELIMINA_ENVIO")	
	private Date fechaEliminaEnvio;
	
	//joksrc transiente 
	@Transient
	private Date fechaAcuseCartasMigradas;
	
	
	public Date getFechaAcuseCartasMigradas() {
		return fechaAcuseCartasMigradas;
	}

	public void setFechaAcuseCartasMigradas(Date fechaAcuseCartasMigradas) {
		this.fechaAcuseCartasMigradas = fechaAcuseCartasMigradas;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	


	public RecuperacionCompania getRecuperacion() {
		return recuperacion;
	}

	public void setRecuperacion(RecuperacionCompania recuperacion) {
		this.recuperacion = recuperacion;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getCausaRechazo() {
		return causaRechazo;
	}

	public void setCausaRechazo(String causaRechazo) {
		this.causaRechazo = causaRechazo;
	}

	public String getCausasNoElaboracion() {
		return causasNoElaboracion;
	}

	public void setCausasNoElaboracion(String causasNoElaboracion) {
		this.causasNoElaboracion = causasNoElaboracion;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public String getComentariosRedoc() {
		return comentariosRedoc;
	}

	public void setComentariosRedoc(String comentariosRedoc) {
		this.comentariosRedoc = comentariosRedoc;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public Date getFechaAcuse() {
		return fechaAcuse;
	}

	public void setFechaAcuse(Date fechaAcuse) {
		this.fechaAcuse = fechaAcuse;
	}

	public Date getFechaElaboracion() {
		return fechaElaboracion;
	}

	public void setFechaElaboracion(Date fechaElaboracion) {
		this.fechaElaboracion = fechaElaboracion;
	}

	public Date getFechaEntrega() {
		return fechaEntrega;
	}

	public void setFechaEntrega(Date fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}

	public Date getFechaImpresion() {
		return fechaImpresion;
	}

	public void setFechaImpresion(Date fechaImpresion) {
		this.fechaImpresion = fechaImpresion;
	}

	public Date getFechaRechazo() {
		return fechaRechazo;
	}

	public void setFechaRechazo(Date fechaRechazo) {
		this.fechaRechazo = fechaRechazo;
	}

	public Long getFolio() {
		return folio;
	}

	public void setFolio(Long folio) {
		this.folio = folio;
	}

	public BigDecimal getMontoARecuperar() {
		return montoARecuperar;
	}

	public void setMontoARecuperar(BigDecimal montoARecuperar) {
		this.montoARecuperar = montoARecuperar;
	}

	public BigDecimal getMontoExclusion() {
		return montoExclusion;
	}

	public void setMontoExclusion(BigDecimal montoExclusion) {
		this.montoExclusion = montoExclusion;
	}

	public String getMotivoExclusion() {
		return motivoExclusion;
	}

	public void setMotivoExclusion(String motivoExclusion) {
		this.motivoExclusion = motivoExclusion;
	}

	public String getTipoRechazo() {
		return tipoRechazo;
	}

	public void setTipoRechazo(String tipoRechazo) {
		this.tipoRechazo = tipoRechazo;
	}

	public String getTipoRedocumentacion() {
		return tipoRedocumentacion;
	}

	public void setTipoRedocumentacion(String tipoRedocumentacion) {
		this.tipoRedocumentacion = tipoRedocumentacion;
	}

	public String getUsuarioElaboracion() {
		return usuarioElaboracion;
	}

	public void setUsuarioElaboracion(String usuarioElaboracion) {
		this.usuarioElaboracion = usuarioElaboracion;
	}

	public String getUsuarioEntrega() {
		return usuarioEntrega;
	}

	public void setUsuarioEntrega(String usuarioEntrega) {
		this.usuarioEntrega = usuarioEntrega;
	}

	public Oficina getOficinaRecepcion() {
		return oficinaRecepcion;
	}

	public void setOficinaRecepcion(Oficina oficinaRecepcion) {
		this.oficinaRecepcion = oficinaRecepcion;
	}

	public Date getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	public Date getFechaRecepcion() {
		return fechaRecepcion;
	}

	public void setFechaRecepcion(Date fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}

	public String getUsuarioEnvio() {
		return usuarioEnvio;
	}

	public void setUsuarioEnvio(String usuarioEnvio) {
		this.usuarioEnvio = usuarioEnvio;
	}

	public String getUsuarioRecepcion() {
		return usuarioRecepcion;
	}

	public void setUsuarioRecepcion(String usuarioRecepcion) {
		this.usuarioRecepcion = usuarioRecepcion;
	}

	public String getNombrePersonaRecepcion() {
		return nombrePersonaRecepcion;
	}

	public void setNombrePersonaRecepcion(String nombrePersonaRecepcion) {
		this.nombrePersonaRecepcion = nombrePersonaRecepcion;
	}

	public String getUsuarioEliminaRecepcion() {
		return usuarioEliminaRecepcion;
	}

	public void setUsuarioEliminaRecepcion(String usuarioEliminaRecepcion) {
		this.usuarioEliminaRecepcion = usuarioEliminaRecepcion;
	}

	public String getUsuarioEliminaEnvio() {
		return usuarioEliminaEnvio;
	}

	public void setUsuarioEliminaEnvio(String usuarioEliminaEnvio) {
		this.usuarioEliminaEnvio = usuarioEliminaEnvio;
	}

	public Date getFechaEliminaRecepcion() {
		return fechaEliminaRecepcion;
	}

	public void setFechaEliminaRecepcion(Date fechaEliminaRecepcion) {
		this.fechaEliminaRecepcion = fechaEliminaRecepcion;
	}

	public Date getFechaEliminaEnvio() {
		return fechaEliminaEnvio;
	}

	public void setFechaEliminaEnvio(Date fechaEliminaEnvio) {
		this.fechaEliminaEnvio = fechaEliminaEnvio;
	}
	
	public Long compare(CartaCompania c1, CartaCompania c2) {
		 return c2.getId() - c1.getId();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public static enum EstatusCartaCompania implements EnumBase<String>{
		REGISTRADO("REGISTRADO"), 
		PENDIENTE_ELABORACION("PEND_ELAB"), 
		ELABORADO("ELABORADO"), 
		IMPRESO("IMPRESO"), 
		ENTREGADO("ENTREGADO"), 
		RECHAZADO("RECHAZADO"),
		POR_REDOCUMENTAR("POR_REDOC"),
		PENDIENTE_CANCELAR("PEND_CANC"),
		CANCELADO("CANCELADO");
		
		public final String codigo;
		
		EstatusCartaCompania(String codigo){
			this.codigo = codigo;
		}		

		@Override
		public String getValue() {
			return codigo;
		}

		@Override
		public String getLabel() {
			return name();
		}
	}
	
	public int compareTo(CartaCompania c) {
        return (this.id.compareTo(c.id));
    }
	
	public static final Long FOLIO_CERO = new Long(0);
	
	


}