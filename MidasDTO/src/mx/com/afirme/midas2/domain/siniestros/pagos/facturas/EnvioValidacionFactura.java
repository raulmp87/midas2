package mx.com.afirme.midas2.domain.siniestros.pagos.facturas;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;

@Entity
@Table(name = "TOENVIOVALIDACIONFACTURA", schema = "MIDAS")
public class EnvioValidacionFactura  extends MidasAbstracto implements Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENVALFAC_ID_SEQ")
	@SequenceGenerator(name = "ENVALFAC_ID_SEQ",  schema="MIDAS", sequenceName = "ENVALFAC_ID_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Long id;
	
	
	@Column(name = "ID_BATCH")
	private Long idBatch;
	
	@Column(name = "ORIGEN")
	private String origen;
	
	@Column(name = "ESTATUS")
	private String estatus;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_ENVIO")
	private Date fechaEnvio;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_FACTURA", referencedColumnName="ID")
	private DocumentoFiscal factura;
	
	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY, mappedBy = "envioValidacionFactura")
	private List<MensajeValidacionFactura> mensajes; 
	
	@Transient
	private Boolean tieneMensajes;
	
	public EnvioValidacionFactura(){
		
	}
	
	public EnvioValidacionFactura(DocumentoFiscal factura) {
		this.factura = factura;
	}
	
	public EnvioValidacionFactura(Long id,DocumentoFiscal factura, Long numeroMensajes) {
		this.id = id;
		this.factura = factura;
		if(numeroMensajes>0){
			this.tieneMensajes = Boolean.TRUE;
		}
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DocumentoFiscal getFactura() {
		return factura;
	}

	public void setFactura(DocumentoFiscal factura) {
		this.factura = factura;
	}

	public Long getIdBatch() {
		return idBatch;
	}

	public void setIdBatch(Long idBatch) {
		this.idBatch = idBatch;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public Date getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}
	
	public List<MensajeValidacionFactura> getMensajes() {
		return mensajes;
	}

	public void setMensajes(List<MensajeValidacionFactura> mensajes) {
		this.mensajes = mensajes;
	}
	
	public Boolean getTieneMensajes() {
		return tieneMensajes;
	}

	public void setTieneMensajes(Boolean tieneMensajes) {
		this.tieneMensajes = tieneMensajes;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	

}
