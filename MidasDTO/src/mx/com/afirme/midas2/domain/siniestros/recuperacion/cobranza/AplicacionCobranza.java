package mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Ingreso;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza.AplicacionCobranzaCuenta.TipoAplicacionCuenta;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza.AplicacionCobranzaCuenta.TipoOperacionCobranza;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza.AplicacionCobranzaDevolucion.TipoAplicacionDevolucion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza.AplicacionCobranzaIngreso.TipoAplicacionIngreso;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.ingresos.IngresoDevolucion;

@Entity
@Table(name="TOSNAPLICACIONCOB", schema="MIDAS")
public class AplicacionCobranza extends MidasAbstracto {

	private static final long serialVersionUID = 3152184369087160613L;
	
	/**
	 * TIPO APLICACION  <code>A</code> Aplicacion <code>default</code>, <code>C</code> Cancelacion, <code>R</code> - Reversa, <code>D</code> Devolucion (cheque)
	 * @author Administrator
	 *
	 */
	public enum TipoAplicacionCobranza{A, C, R, D};
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOSNAPLICACIONCOB_GENERATOR")
	@SequenceGenerator(name="TOSNAPLICACIONCOB_GENERATOR", schema="MIDAS", sequenceName="TOSNAPLICACIONCOB_SEQ",allocationSize=1)	
	@Column(name="ID")
	private Long id;
	
	@Column(name="TIPO")
	@Enumerated(EnumType.STRING)
	private TipoAplicacionCobranza tipo = TipoAplicacionCobranza.A;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MONEDA_ID")
	private MonedaDTO moneda; 
		
	@Column(name="DESCRIPCION")	
	private String descripcion;	
	
	
	@Column(name="ID_CONTABLE")	
	private Long idContable;	
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="aplicacion", cascade = CascadeType.ALL)
	private List<AplicacionCobranzaIngreso> aplicacionesIngreso = new ArrayList<AplicacionCobranzaIngreso>();
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="aplicacion", cascade = CascadeType.ALL)
	private List<AplicacionCobranzaCuenta> aplicacionesCuenta = new ArrayList<AplicacionCobranzaCuenta>();
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="aplicacion", cascade = CascadeType.ALL)
	private List<AplicacionCobranzaDevolucion> aplicacionesDevolucion = new ArrayList<AplicacionCobranzaDevolucion>();
	
	public AplicacionCobranza(){
		super();
		this.moneda = new MonedaDTO();
		this.moneda.setIdTcMoneda(((Integer)MonedaDTO.MONEDA_PESOS).shortValue());
		this.moneda.setDescripcion("NACIONAL");
	}
	
	public AplicacionCobranza(TipoAplicacionCobranza tipo, String descripcion, String codigoUsuarioCreacion){
		this();
		this.tipo = tipo;
		this.descripcion = descripcion;
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
	
	public AplicacionCobranza(TipoAplicacionCobranza tipo, MonedaDTO moneda,
			String descripcion, String codigoUsuarioCreacion) {
		this(tipo, descripcion, codigoUsuarioCreacion);				
		this.moneda = moneda;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoAplicacionCobranza getTipo() {
		return tipo;
	}

	public void setTipo(TipoAplicacionCobranza tipo) {
		this.tipo = tipo;
	}

	public MonedaDTO getMoneda() {
		return moneda;
	}

	public void setMoneda(MonedaDTO moneda) {
		this.moneda = moneda;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public List<AplicacionCobranzaIngreso> getAplicacionesIngreso() {
		return aplicacionesIngreso;
	}

	public void setAplicacionesIngreso(List<AplicacionCobranzaIngreso> aplicacionesIngreso) {
		this.aplicacionesIngreso = aplicacionesIngreso;
	}
	
	public List<AplicacionCobranzaCuenta> getAplicacionesCuenta() {
		return aplicacionesCuenta;
	}

	public void setAplicacionesCuenta(
			List<AplicacionCobranzaCuenta> aplicacionesCuenta) {
		this.aplicacionesCuenta = aplicacionesCuenta;
	}
	
	public List<AplicacionCobranzaDevolucion> getAplicacionesDevolucion() {
		return aplicacionesDevolucion;
	}

	public void setAplicacionesDevolucion(
			List<AplicacionCobranzaDevolucion> aplicacionesDevolucion) {
		this.aplicacionesDevolucion = aplicacionesDevolucion;
	}

	public Long getIdContable() {
		return idContable;
	}

	public void setIdContable(Long idContable) {
		this.idContable = idContable;
	}

	/**
	 * Generar una aplicacion ingreso y agregarla a la aplicacion de cobranza
	 * @param ingreso
	 * @param tipo
	 * @param codigoUsuarioCreacion
	 */
	public void agregar(Ingreso ingreso, TipoAplicacionIngreso tipo, String codigoUsuarioCreacion, ConceptoGuia conceptoGuia){
		AplicacionCobranzaIngreso aplicacionIngreso = new AplicacionCobranzaIngreso(this, ingreso, tipo, codigoUsuarioCreacion, conceptoGuia);
		this.aplicacionesIngreso.add(aplicacionIngreso);
	}
	
	/**
	 * Generar una aplicacion devolucion (cheque) y agregarla a la aplicacion de cobranza
	 * @param ingreso
	 * @param tipo
	 * @param codigoUsuarioCreacion
	 */
	public void agregar(IngresoDevolucion devolucion, TipoAplicacionDevolucion tipo, String codigoUsuarioCreacion, ConceptoGuia conceptoGuia){
		AplicacionCobranzaDevolucion aplicacionDevolucion = new AplicacionCobranzaDevolucion(this, devolucion, tipo, codigoUsuarioCreacion, conceptoGuia);
		this.aplicacionesDevolucion.add(aplicacionDevolucion);
	}
	
	
	
	/**
	 * Agregar una aplicacion de ingreso (ya construido) a la aplicacion de cobranza
	 * @param aplicacionIngreso
	 */
	public void agregar(AplicacionCobranzaIngreso aplicacionIngreso){
		aplicacionIngreso.setAplicacion(this);
		this.aplicacionesIngreso.add(aplicacionIngreso);
	}
	
	/**
	 * Agregar una aplicacion de devolucion - cheque (ya construido) a la aplicacion de cobranza
	 * @param aplicacionIngreso
	 */
	public void agregar(AplicacionCobranzaDevolucion aplicacionDevolucion){
		aplicacionDevolucion.setAplicacion(this);
		this.aplicacionesDevolucion.add(aplicacionDevolucion);
	}
	
	/***
	 * Generar una cuenta de cobranza (deposito, acreedor, manual) y agregarla a la aplicacion de cobranza
	 * @param identificador
	 * @param tipo
	 * @param montoOrigen
	 * @param montoAplicado
	 * @param codigoUsuarioCreacion
	 * @param descripcion - No requerida
	 * @param conceptoGuia - No requerida
	 * @param operacion - tipo de operacion Cargo o Abono sobre la cuenta
	 */
	public void agregar(String identificador, TipoAplicacionCuenta tipo, BigDecimal montoOrigen, 
			BigDecimal montoAplicado, String codigoUsuarioCreacion, String descripcion, ConceptoGuia conceptoGuia, 
			TipoOperacionCobranza operacion){
		AplicacionCobranzaCuenta aplicacionCuenta = new AplicacionCobranzaCuenta(this, identificador, tipo, 
				montoOrigen, montoAplicado, codigoUsuarioCreacion, descripcion, conceptoGuia, operacion);
		this.aplicacionesCuenta.add(aplicacionCuenta);
	}
	
	/***
	 * Generar una cuenta de cobranza (deposito, acreedor, manual) y agregarla a la aplicacion de cobranza
	 * @param identificador
	 * @param tipo
	 * @param montoOrigen
	 * @param montoAplicado
	 * @param codigoUsuarioCreacion
	 * @param descripcion - No requerida
	 * @param conceptoGuia - No requerida
	 */
	public void agregar(String identificador, TipoAplicacionCuenta tipo, BigDecimal montoOrigen, 
			BigDecimal montoAplicado, String codigoUsuarioCreacion, String descripcion, ConceptoGuia conceptoGuia){
		agregar(identificador, tipo, montoOrigen, montoAplicado, codigoUsuarioCreacion, descripcion, conceptoGuia, TipoOperacionCobranza.C);		
	}
	
	/**
	 * Agregar una cuenta de cobranza a la aplicacion de la cobranza
	 * @param cuenta
	 */
	public void agregar(AplicacionCobranzaCuenta cuenta){
		cuenta.setAplicacion(this);
		this.aplicacionesCuenta.add(cuenta);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AplicacionCobranza other = (AplicacionCobranza) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {	
		return descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	

}
