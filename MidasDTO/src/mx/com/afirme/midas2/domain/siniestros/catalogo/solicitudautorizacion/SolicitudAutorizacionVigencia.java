/**
 * 
 */
package mx.com.afirme.midas2.domain.siniestros.catalogo.solicitudautorizacion;

import java.math.BigDecimal;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

import mx.com.afirme.midas.base.MidasAbstractoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.catalogos.EnumBase;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;

/**
 * @author admin
 *
 */
@Entity
@Table(name="TOSOLAUTORIZACIONVIGENCIA",schema="MIDAS")
@Access(AccessType.FIELD)
public class SolicitudAutorizacionVigencia extends MidasAbstractoDTO implements Entidad{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOSOLAUTVIGENCIA_SEQ")
	@SequenceGenerator(name = "IDTOSOLAUTVIGENCIA_SEQ",  schema="MIDAS", sequenceName = "IDTOSOLAUTVIGENCIA_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Long id;
	
	@Column(name="NUMEROINCISO")
	private BigDecimal numeroInciso;
	
	@Column(name="ESTATUS")
	private Integer estatus;
	
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinFetch (JoinFetchType.INNER)
	@JoinColumn(name = "OFICINA_ID" )
	private Oficina oficina;
	
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinFetch (JoinFetchType.INNER)
	@JoinColumn(name = "SOLICITUDAUTORIZACION_ID")
	private SolicitudAutorizacion solicitudAutorizacion;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="POLIZA_ID", referencedColumnName="IDTOPOLIZA")
	private PolizaDTO poliza;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="REPORTE_ID", referencedColumnName="ID")
	private ReporteCabina reporte;
	
	public enum EstatusAutorizacion implements EnumBase<Integer>{
		PENDIENTE (0),
	    AUTORIZADO (1),
	    RECHAZADO (2);

	    private final Integer estatus; 

	    private EstatusAutorizacion(Integer estatus) {
	        this.estatus = estatus;
	    }
	    @Override
	    public String toString(){
	       return this.toString();
	    }

		@Override
		public Integer getValue() {
			return estatus;
		}

		@Override
		public String getLabel() {
			return this.toString();
		}

	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public Oficina getOficina() {
		return oficina;
	}

	public void setOficina(Oficina oficina) {
		this.oficina = oficina;
	}

	public SolicitudAutorizacion getSolicitudAutorizacion() {
		return solicitudAutorizacion;
	}

	public void setSolicitudAutorizacion(SolicitudAutorizacion solicitudAutorizacion) {
		this.solicitudAutorizacion = solicitudAutorizacion;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public PolizaDTO getPoliza() {
		return poliza;
	}

	public void setPoliza(PolizaDTO poliza) {
		this.poliza = poliza;
	}

	public ReporteCabina getReporte() {
		return reporte;
	}

	public void setReporte(ReporteCabina reporte) {
		this.reporte = reporte;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.id;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof SolicitudAutorizacionVigencia)) {
			return false;
		}
		SolicitudAutorizacionVigencia other = (SolicitudAutorizacionVigencia) obj;
		if (poliza == null) {
			if (other.poliza != null) {
				return false;
			}
		} else if (!poliza.getIdToPoliza().equals(other.poliza.getIdToPoliza())) {
			return false;
		}
		if (reporte == null) {
			if (other.reporte != null) {
				return false;
			}
		} else if (!reporte.getId().equals(other.reporte.getId())) {
			return false;
		}
		return true;
	}

	

}
