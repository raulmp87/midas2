package mx.com.afirme.midas2.domain.siniestros.recuperacion;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.MidasAbstracto;


/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 10-jul-2015 07:07:28 p.m.
 */
@Entity(name = "SeguimientoRecuperacion")
@Table(name = "TOSNSEGUIMIENTO_RECUPERACION", schema = "MIDAS")
public class SeguimientoRecuperacion extends MidasAbstracto {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8355153012704623628L;


	@Id
	@SequenceGenerator(name = "TOSNSEGUIMIENTO_RECUPERACION_SEQ_GENERADOR",allocationSize = 1, sequenceName = "TOSNSEGUIMIENTO_RECUPERA_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TOSNSEGUIMIENTO_RECUPERACION_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id ; 
	
	
	@Column(name = "COMENTARIO")
	private String comentario;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "RECUPERACION_ID", referencedColumnName = "ID")
	private Recuperacion recuperacion;

	
	@Transient
	private String usuarioCreacionNombre;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public Recuperacion getRecuperacion() {
		return recuperacion;
	}

	public void setRecuperacion(Recuperacion recuperacion) {
		this.recuperacion = recuperacion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setUsuarioCreacionNombre(String usuarioCreacionNombre) {
		this.usuarioCreacionNombre = usuarioCreacionNombre;
	}

	public String getUsuarioCreacionNombre() {
		return usuarioCreacionNombre;
	}
}