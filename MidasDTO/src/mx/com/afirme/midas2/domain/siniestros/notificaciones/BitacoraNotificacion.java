package mx.com.afirme.midas2.domain.siniestros.notificaciones;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.ConfiguracionNotificacionCabina;

@Entity
@Table(name="TOSNBITNOTIFICACION",schema="MIDAS")
public class BitacoraNotificacion extends MidasAbstracto {

	private static final long serialVersionUID = -5639645470236957327L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOBITNOTIFICACIONMIDAS_GENERATOR")
	@SequenceGenerator(name="TOBITNOTIFICACIONMIDAS_GENERATOR", schema="MIDAS", sequenceName="TOBITNOTIFICACION_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;
	
	@Column(name="folio")
	private String folio;
	
	@Column(name="esrecurrente")
	private Boolean recurrente = Boolean.FALSE;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "CONFIGURACION_ID", referencedColumnName = "ID", nullable = false)
	private ConfiguracionNotificacionCabina configuracion;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="bitacora", cascade=CascadeType.ALL)
	private List<BitacoraDestinoNotificacion> destinos = new ArrayList<BitacoraDestinoNotificacion>();

	public BitacoraNotificacion(){
		super();
	}

	public BitacoraNotificacion(String folio, ConfiguracionNotificacionCabina configuracion,
			String codigoUsuarioCreacion) {
		this();		
		this.folio = folio;
		this.configuracion = configuracion;	
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
		this.recurrente = configuracion.getMovimientoProceso().getRecurrente() != null ?
				configuracion.getMovimientoProceso().getRecurrente() : Boolean.FALSE;
	}
	
	public BitacoraNotificacion(String folio, ConfiguracionNotificacionCabina configuracion,
			List<BitacoraDestinoNotificacion> destinos, String codigoUsuarioCreacion) {
		this(folio, configuracion, codigoUsuarioCreacion);
		this.destinos = destinos;
	}
	
	@Transient
	public void agregar(BitacoraDestinoNotificacion destino){
		destino.setBitacora(this);
		this.destinos.add(destino);
	}
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public Boolean getRecurrente() {
		return recurrente;
	}

	public void setRecurrente(Boolean recurrente) {
		this.recurrente = recurrente;
	}

	public ConfiguracionNotificacionCabina getConfiguracion() {
		return configuracion;
	}

	public void setConfiguracion(ConfiguracionNotificacionCabina configuracion) {
		this.configuracion = configuracion;
	}
	
	public List<BitacoraDestinoNotificacion> getDestinos() {
		return destinos;
	}

	public void setDestinos(List<BitacoraDestinoNotificacion> destinos) {
		this.destinos = destinos;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return folio;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getBusinessKey() {
		return folio;
	}
	
	public BitacoraDestinoNotificacion obtener(String destinatario){
		if(this.id != null && destinos != null && destinos.size() > 0){
			for(BitacoraDestinoNotificacion destino : destinos){
				if(destino.getDestinatario().equals(destinatario) && destino.getBitacora().getId().equals(this.id)){
					return destino;
				}
			}					
		}
		return null;
	}

}
