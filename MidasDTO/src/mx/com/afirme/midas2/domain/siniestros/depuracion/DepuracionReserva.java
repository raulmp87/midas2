package mx.com.afirme.midas2.domain.siniestros.depuracion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.MidasAbstracto;

@Entity(name = "DepuracionReserva")
@Table(name = "TODEPURACIONRESERVA", schema = "MIDAS")
public class DepuracionReserva extends MidasAbstracto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8157916564070018151L;

	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TODEPURACIONRESERVA_ID_GENERATOR")
	@SequenceGenerator(name="TODEPURACIONRESERVA_ID_GENERATOR", schema="MIDAS", sequenceName="TODEPURACIONRESERVA_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;
	
	@Column(name="CODIGO_USUARIO_EJECUCION")
	private String codigoUsuarioEjecucion;
	
	@ManyToOne
	@JoinColumn(name = "CONFIGURACIONDEPURACION_ID", referencedColumnName = "id")
	private ConfiguracionDepuracionReserva configuracion;
	
	@Column(name="ESTATUS")
	private String estatus;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_EJECUCION")
	private Date fechaEjecucion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_PROGRAMACION")
	private Date fechaProgramacion;
	
	@Column(name="MONTO_RESERVA")
	private BigDecimal montoReserva;
	
	@Column(name="MONTO_RESERVA_DEPURADA")
	private BigDecimal montoReservaDepurada;
	
	@Column(name="NUMERO_DEPURACION")
	private Long numeroDepuracion;
	
	@Column(name="REGISTROS_DEPURADOS")
	private Long registrosDepurados;
	
	@OneToMany(fetch=FetchType.LAZY,  mappedBy = "depuracionReserva", cascade = CascadeType.ALL)
    private List<DepuracionReservaDetalle> depuracionReservaDetalles = new ArrayList<DepuracionReservaDetalle>();
	
	@Transient
	private String descripcionEstatus;

	@Transient
	private Long idDepuracionReserva;

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigoUsuarioEjecucion() {
		return codigoUsuarioEjecucion;
	}

	public void setCodigoUsuarioEjecucion(String codigoUsuarioEjecucion) {
		this.codigoUsuarioEjecucion = codigoUsuarioEjecucion;
	}

	public ConfiguracionDepuracionReserva getConfiguracion() {
		return configuracion;
	}

	public void setConfiguracion(ConfiguracionDepuracionReserva configuracion) {
		this.configuracion = configuracion;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public Date getFechaEjecucion() {
		return fechaEjecucion;
	}

	public void setFechaEjecucion(Date fechaEjecucion) {
		this.fechaEjecucion = fechaEjecucion;
	}

	public Date getFechaProgramacion() {
		return fechaProgramacion;
	}

	public void setFechaProgramacion(Date fechaProgramacion) {
		this.fechaProgramacion = fechaProgramacion;
	}

	public BigDecimal getMontoReserva() {
		return montoReserva;
	}

	public void setMontoReserva(BigDecimal montoReserva) {
		this.montoReserva = montoReserva;
	}

	public BigDecimal getMontoReservaDepurada() {
		return montoReservaDepurada;
	}

	public void setMontoReservaDepurada(BigDecimal montoReservaDepurada) {
		this.montoReservaDepurada = montoReservaDepurada;
	}

	public Long getNumeroDepuracion() {
		return numeroDepuracion;
	}

	public void setNumeroDepuracion(Long numeroDepuracion) {
		this.numeroDepuracion = numeroDepuracion;
	}

	public Long getRegistrosDepurados() {
		return registrosDepurados;
	}

	public void setRegistrosDepurados(Long registrosDepurados) {
		this.registrosDepurados = registrosDepurados;
	}

	public List<DepuracionReservaDetalle> getDepuracionReservaDetalles() {
		return depuracionReservaDetalles;
	}

	public void setDepuracionReservaDetalles(
			List<DepuracionReservaDetalle> depuracionReservaDetalles) {
		this.depuracionReservaDetalles = depuracionReservaDetalles;
	}

	public String getDescripcionEstatus() {
		return descripcionEstatus;
	}

	public void setDescripcionEstatus(String descripcionEstatus) {
		this.descripcionEstatus = descripcionEstatus;
	}
	
	public Long getIdDepuracionReserva() {
		return idDepuracionReserva;
	}

	public void setIdDepuracionReserva(Long idDepuracionReserva) {
		this.idDepuracionReserva = idDepuracionReserva;
	}
	

}
