package mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio;

import java.util.Date;

import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;

public class ArchivoAdjuntoInfoBancaria {
	
	private Long id;
	private String codigoUsuarioCreacion;
	private String codigoUsuarioModificacion;
	private Date fechaCreacion;
	private Date fechaModificacion;
	private ControlArchivoDTO controlArchivo;
	private InformacionBancaria informacionBancaria;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}
	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
	public String getCodigoUsuarioModificacion() {
		return codigoUsuarioModificacion;
	}
	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	public ControlArchivoDTO getControlArchivo() {
		return controlArchivo;
	}
	public void setControlArchivo(ControlArchivoDTO controlArchivo) {
		this.controlArchivo = controlArchivo;
	}
	public InformacionBancaria getInformacionBancaria() {
		return informacionBancaria;
	}
	public void setInformacionBancaria(InformacionBancaria informacionBancaria) {
		this.informacionBancaria = informacionBancaria;
	}
	
	
	
	

}
