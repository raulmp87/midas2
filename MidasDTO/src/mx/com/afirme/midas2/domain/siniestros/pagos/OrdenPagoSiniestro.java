package mx.com.afirme.midas2.domain.siniestros.pagos;


import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.LiquidacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.dto.siniestros.pagos.DesglosePagoConceptoDTO;

/**
 * Entidad que mapea los datos de �rdenes de pago.
 * @author usuario
 * @version 1.0
 * @created 22-sep-2014 12:55:21 p.m.
 */
@Entity(name = "OrdenPagoSiniestro")
@Table(name = "TOORDENPAGO_SINIESTRO", schema = "MIDAS")
public class OrdenPagoSiniestro extends MidasAbstracto {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1070013247757868309L;
	@Id
	@SequenceGenerator(name = "TOORDENPAGO_GENERADOR",allocationSize = 1, sequenceName = "TOORDENPAGO_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TOORDENPAGO_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id;
	
	
	/**
	 * numero de orden de pago de seycos usado como referencia, es string por que no
	 * conocemos el formato en seycos
	 */
	@Column(name="ID_ORDEN_PAGO_SEYCOS")
	private String idOrdenPagoSeycos;
	
	/**
	 * Tramite, Autorizada, Cancelada, Pagada
	 */
	@Column(name="ESTATUS")
	private String estatus;
	
	/**
	 * TOTAL, PARCIAL
	 */
	@Column(name="TIPO_PAGO")
	private String tipoPago;

	
	/**
	 * fecha en que se sincronizo la informaci�n con seycos, esto se har� en una
	 * segudan fase para obtener la informaci�n de seycos de la orden de pago
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_ACT_SEYCOS")
	private Date fechaActualizacionSeycos;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CANCELACION")
	private Date fechaCancelacion;

	/**
	 * fecha de pago
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_PAGO")
	private Date fechaPago;

	/**
	 * estatus de la orden en seycos, se traer� en una segunda fase mediante una
	 * inferface
	 */
	@Column(name="ESTATUS_SEYCOS")
	private String estatusSeycos;
	
	@Column(name="MOTIVO_CANCELACION")
	private String motivoCancelacion;
	
	@Column(name="COMENTARIOS")
	private String comentarios;
	
	@Column(name = "CODIGO_USUARIO_CANCELA", length = 8)
	private String codigoUsuarioCancela;
	

	@Column(name = "FACTURA")
	private String factura;
	
	
	@Column(name = "ORIGENPAGO")
	private String origenPago;
	
	
	/**
	 * relacion con orden de compra, debe ser uno a uno
	 */
//	@Column(name="ID_ORDEN_COMPRA")
//	private Long ordenCompraId;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ORDEN_COMPRA", referencedColumnName="ID")
	private OrdenCompra ordenCompra;
		
	

	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinTable(name = "TRLIQUIDACIONORDENPAGO", schema = "MIDAS",
				joinColumns = {@JoinColumn(name="ORDENPAGO_ID", referencedColumnName="ID")},
				inverseJoinColumns = {@JoinColumn(name="LIQUIDACION_ID", referencedColumnName="ID")}
		)
	private LiquidacionSiniestro liquidacion;
	
	/* Campos para liquidacion de indemnizaciones */
	@Transient
	private SiniestroCabina siniestro;
	
	@Transient
	private String formaPago;
	
	@Transient
	private Boolean esPerdidaTotal;
	
	@Transient
	private String tipoIndemnizacion;
	
	@Transient
	private String numeroSiniestro;
	
	@Transient
	private String esPT;
	
	@Transient
	private DesglosePagoConceptoDTO totales;	
	/*---------------------------- */
	
	public OrdenPagoSiniestro()
	{
		super();
	}
	
	/* constructor para liquidacion de indemnizaciones */
	public OrdenPagoSiniestro(Long idOrdenPago, SiniestroCabina siniestro, String formaPago, Boolean esPerdidaTotal, String tipoIndemnizacion)
	{
		this.id = idOrdenPago;
		this.siniestro = siniestro;	
		this.formaPago = formaPago;
		this.esPerdidaTotal = esPerdidaTotal;
		this.tipoIndemnizacion = tipoIndemnizacion;
	}
	
	public String getOrigenPago() {
		return origenPago;
	}

	public void setOrigenPago(String origenPago) {
		this.origenPago = origenPago;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdOrdenPagoSeycos() {
		return idOrdenPagoSeycos;
	}

	public void setIdOrdenPagoSeycos(String idOrdenPagoSeycos) {
		this.idOrdenPagoSeycos = idOrdenPagoSeycos;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}

	public Date getFechaActualizacionSeycos() {
		return fechaActualizacionSeycos;
	}

	public void setFechaActualizacionSeycos(Date fechaActualizacionSeycos) {
		this.fechaActualizacionSeycos = fechaActualizacionSeycos;
	}

	public String getEstatusSeycos() {
		return estatusSeycos;
	}

	public void setEstatusSeycos(String estatusSeycos) {
		this.estatusSeycos = estatusSeycos;
	}

	public Date getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	public String getMotivoCancelacion() {
		return motivoCancelacion;
	}

	public void setMotivoCancelacion(String motivoCancelacion) {
		this.motivoCancelacion = motivoCancelacion;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public String getCodigoUsuarioCancela() {
		return codigoUsuarioCancela;
	}

	public void setCodigoUsuarioCancela(String codigoUsuarioCancela) {
		this.codigoUsuarioCancela = codigoUsuarioCancela;
	}

	public String getFactura() {
		return factura;
	}

	public void setFactura(String factura) {
		this.factura = factura;
	}

	public Date getFechaCancelacion() {
		return fechaCancelacion;
	}

	public void setFechaCancelacion(Date fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}

	


	public OrdenCompra getOrdenCompra() {
		return ordenCompra;
	}

	public void setOrdenCompra(OrdenCompra ordenCompra) {
		this.ordenCompra = ordenCompra;
	}

	public SiniestroCabina getSiniestro() {
		return siniestro;
	}

	public void setSiniestro(SiniestroCabina siniestro) {
		this.siniestro = siniestro;
	}

	public String getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}

	public Boolean getEsPerdidaTotal() {
		return esPerdidaTotal;
	}

	public void setEsPerdidaTotal(Boolean esPerdidaTotal) {
		this.esPerdidaTotal = esPerdidaTotal;
	}

	public DesglosePagoConceptoDTO getTotales() {
		return totales;
	}

	public void setTotales(DesglosePagoConceptoDTO totales) {
		this.totales = totales;
	}

	public String getTipoIndemnizacion() {
		return tipoIndemnizacion;
	}

	public void setTipoIndemnizacion(String tipoIndemnizacion) {
		this.tipoIndemnizacion = tipoIndemnizacion;
	}

	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}

	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}

	public String getEsPT() {
		return esPT;
	}

	public void setEsPT(String esPT) {
		this.esPT = esPT;
	}

	/**
	 * @return the liquidacion
	 */
	public LiquidacionSiniestro getLiquidacion() {
		return liquidacion;
	}

	/**
	 * @param liquidacion the liquidacion to set
	 */
	public void setLiquidacion(LiquidacionSiniestro liquidacion) {
		this.liquidacion = liquidacion;
	}
		
}