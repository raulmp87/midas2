package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.domain.MidasAbstracto;


@Entity
@Table(name = "TCCONFCOBERTURASINIESTRO", schema = "MIDAS")
public class ConfiguracionCoberturaSiniestro extends MidasAbstracto {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDCONFCOBERSIN_SEQ")
	@SequenceGenerator(name = "IDCONFCOBERSIN_SEQ", schema = "MIDAS", sequenceName = "IDCONFCOBERSIN_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "CLAVE_TIPO_CALCULO")
	private String cveTipoCalculoCobertura;
	
	@Column(name = "CLAVE_SUB_TIPO_CALCULO")
	private String cveSubTipoCalculoCobertura;
	
	@Column(name = "TIPO_RESPONSABILIDAD")
	private String responsabilidad;
	
	@Column(name = "TERMINO_AJUSTE")
	private String terminoAjuste;
	
	@Column(name = "TIPO_SINIESTRO")
	private String tipoSiniestro;
	
	@Column(name = "TIPO_CONFIGURACION", length = 3)
	private String tipoConfiguracion;
	
	

	public ConfiguracionCoberturaSiniestro(){

	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCveTipoCalculoCobertura() {
		return cveTipoCalculoCobertura;
	}



	public void setCveTipoCalculoCobertura(String cveTipoCalculoCobertura) {
		this.cveTipoCalculoCobertura = cveTipoCalculoCobertura;
	}



	public String getCveSubTipoCalculoCobertura() {
		return cveSubTipoCalculoCobertura;
	}



	public void setCveSubTipoCalculoCobertura(String cveSubTipoCalculoCobertura) {
		this.cveSubTipoCalculoCobertura = cveSubTipoCalculoCobertura;
	}



	public String getResponsabilidad() {
		return responsabilidad;
	}



	public void setResponsabilidad(String responsabilidad) {
		this.responsabilidad = responsabilidad;
	}



	public String getTerminoAjuste() {
		return terminoAjuste;
	}



	public void setTerminoAjuste(String terminoAjuste) {
		this.terminoAjuste = terminoAjuste;
	}

	public String getTipoSiniestro() {
		return tipoSiniestro;
	}

	public void setTipoSiniestro(String tipoSiniestro) {
		this.tipoSiniestro = tipoSiniestro;
	}	

	public String getTipoConfiguracion() {
		return tipoConfiguracion;
	}

	public void setTipoConfiguracion(String tipoConfiguracion) {
		this.tipoConfiguracion = tipoConfiguracion;
	}

	public void finalize() throws Throwable {
		super.finalize();
	}

	@Override
	public <K> K getKey() {
		return (K) this.id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}