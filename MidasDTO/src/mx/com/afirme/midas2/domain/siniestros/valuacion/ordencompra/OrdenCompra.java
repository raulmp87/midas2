package mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.IndemnizacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.pagos.OrdenPagoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.util.StringUtil;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;


/**
 * Entidad que mapea los datos generales de una ï¿½rden de compra.
 * @author usuario
 * @version 1.0
 * @created 22-ago-2014 10:48:11 a.m.
 */
@Entity(name = "OrdenCompra")
@Table(name = "TOORDENCOMPRA", schema = "MIDAS")
public class OrdenCompra extends MidasAbstracto  {
	/**
	 * Id de la orden de compra
	 */
	@Id
	@SequenceGenerator(name = "TOORDENCOMPRA_SEQ_GENERADOR",allocationSize = 1, sequenceName = "TOORDENCOMPRA_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TOORDENCOMPRA_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id;
	/**
	 * 
	 */
	private static final long serialVersionUID = -5777458063156444531L;
	/**
	 * cobertura ligada a la orden de compra, tiene que ser una cobertura afectada 
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COBERTURAREPORTECABINA_ID", referencedColumnName = "ID")
	private CoberturaReporteCabina coberturaReporteCabina;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "REPORTE_CABINA_ID", referencedColumnName = "ID")
	private ReporteCabina reporteCabina;
	
	
	@Column(name = "NUMEROVALUACION")
	private Long numeroValuacion ; 
	
	@Column(name = "CLAVE_SUB_TIPO_CALCULO")
	private String cveSubTipoCalculoCobertura;
	
	@Column(name = "IDCOBERTURA_COMPUESTO")
	private String idCoberturaCompuesta;
	
	
	@Column(name = "REEMBOLSO_GASTOAJUSTE")
	private String reembolsoGA;
	
	
	@Column(name = "TIPOPERSONA")
	private String tipoPersona;
	
	
	@ManyToMany( fetch = FetchType.LAZY)
	@JoinTable(name = "TOORDENCOMPRAFACTURA", schema="MIDAS", 
			joinColumns = {@JoinColumn(name="ID_ORDEN_COMPRA", referencedColumnName="ID")}, 
			inverseJoinColumns = {@JoinColumn(name="ID_FACTURA", referencedColumnName="ID")}
	)
    private List<DocumentoFiscal> facturasSiniestroList;
	
	@Transient
	private BigDecimal totalCostoUnitarioDetalle;
	
	public String getIdCoberturaCompuesta() {
		return idCoberturaCompuesta;
	}

	public void setIdCoberturaCompuesta(String idCoberturaCompuesta) {
		this.idCoberturaCompuesta = idCoberturaCompuesta;
	}

	@OneToMany(fetch=FetchType.LAZY,  mappedBy = "ordenCompra", cascade = CascadeType.ALL)
    private List<DetalleOrdenCompra> detalleOrdenCompras = new ArrayList<DetalleOrdenCompra>();
	
	
	@Column(name="CURP")
	private String curp;
	/**
	 * Estatus de orden de compra: TRAMITE, PAGADO, CANCELADO
	 */
	@Column(name="ESTATUS")	
	private String estatus;
	@Column(name="FACTURA")
	private String factura;
	/**
	 * id del beneficiario 
	 */
	@Column(name="ID_BENEFICIARIO")
	private Integer idBeneficiario;
	/**
	 * id del tercero afectado, proviene del pase de atencion seleccionado
	 */
	@Column(name="ID_TERCERO_AFECTADO")
	private Long idTercero;
	
	
	@Transient
	private BigDecimal deducible;
	
	
	@Column(name="DESCUENTO")
	private BigDecimal descuento;
	
	
	@Column(name="CLABE")
	private String clabe;
	
	
	@Column(name="CORREO")
	private String correo;
	
	
	@Column(name="LADA")
	private String lada;
	
	
	@Column(name="TELEFONO")
	private String telefono;
	
	
	@Column(name="FACTURA_ORIGEN")
	private String facturaOrigen;
	
	@Column(name="PROVEEDOR_ORIGEN")
	private String proveedorOrigen;
	
	@Column(name="RFC_ORIGEN")
	private String rfcOrigen;
	
	@Column(name="TIPO_AFECTADO")
	private String tipoAfectado;
	
	
	@Column(name = "BANCO_ID")
	private Integer bancoId;
	
	
	/**
	 * id de la orden compra origen solo aplica para cuando la orden compra es de tipo indemnizacion.
	 */
	@Column(name="ID_ORDENCOMPRA_ORIGEN")
	private Long idOrdenCompraOrigen;
	
	/**
	 * nombre del beneficario
	 */
	@Column(name="NOM_BENEFICIARIO")
	private String nomBeneficiario;

	
	@Column(name="RFC")
	private String rfc;
	/**
	 * tipo de orden de compra puede ser ORDEN_COMPRA, GASTO_AJUSTE
	 */
	@Column(name="TIPO")
	private String tipo;
	/**
	 * Tipo de pago posible: Pago proveedor, pago beneficiario
	 */
	@Column(name="TIPO_PAGO")
	private String tipoPago;
	

	/**
	 * id del proveedor
	 */
	@Column(name="TIPO_PROVEEDOR")
	private String tipoProveedor;
	
	@Column(name="ORIGEN")
	private String origen;
	
	@OneToOne(cascade=CascadeType.ALL,fetch=FetchType.LAZY, mappedBy="ordenCompra")
	@JoinFetch(JoinFetchType.OUTER)
	private OrdenPagoSiniestro ordenPago;
	
	@OneToOne(cascade=CascadeType.ALL,fetch=FetchType.LAZY, mappedBy="ordenCompraGenerada")
    @JoinFetch(JoinFetchType.OUTER)
    private IndemnizacionSiniestro indemnizacion;
	
	@OneToOne(cascade=CascadeType.ALL,fetch=FetchType.LAZY, mappedBy="ordenCompra")
    @JoinFetch(JoinFetchType.OUTER)
    private IndemnizacionSiniestro indemnizacionOrigen; 
	
	@OneToOne(fetch=FetchType.LAZY, mappedBy="ordenCompra")
	private CartaPago cartaPago;
	

	@Column(name = "APLICA_DEDUCIBLE")
	private Boolean aplicaDeducible;
	
	@Column(name = "SIPAC")
	private Boolean sipac;
	
	public OrdenCompra(){
		super();
	}
	
	public OrdenCompra(Long id, BigDecimal totalCostoUnitarioDetalle) {
		this();
		this.id = id;
		this.totalCostoUnitarioDetalle = totalCostoUnitarioDetalle;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}


	public CoberturaReporteCabina getCoberturaReporteCabina() {
		return coberturaReporteCabina;
	}

	public void setCoberturaReporteCabina(
			CoberturaReporteCabina coberturaReporteCabina) {
		this.coberturaReporteCabina = coberturaReporteCabina;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getFactura() {
		return factura;
	}

	public void setFactura(String factura) {
		this.factura = factura;
	}

	@Override
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	@Override
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Override
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	@Override
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getIdBeneficiario() {
		return idBeneficiario;
	}

	public void setIdBeneficiario(Integer idBeneficiario) {
		this.idBeneficiario = idBeneficiario;
	}

	public Long getIdTercero() {
		return idTercero;
	}

	public void setIdTercero(Long idTercero) {
		this.idTercero = idTercero;
	}

	public String getNomBeneficiario() {
		return nomBeneficiario;
	}

	public void setNomBeneficiario(String nomBeneficiario) {
		this.nomBeneficiario = nomBeneficiario;
	}
	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	

	public String getTipoProveedor() {
		return tipoProveedor;
	}

	public void setTipoProveedor(String tipoProveedor) {
		this.tipoProveedor = tipoProveedor;
	}

	public List<DetalleOrdenCompra> getDetalleOrdenCompras() {
		return detalleOrdenCompras;
	}

	public void setDetalleOrdenCompras(List<DetalleOrdenCompra> detalleOrdenCompras) {
		this.detalleOrdenCompras = detalleOrdenCompras;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	public ReporteCabina getReporteCabina() {
		return reporteCabina;
	}

	public void setReporteCabina(ReporteCabina reporteCabina) {
		this.reporteCabina = reporteCabina;
	}

	public String getCveSubTipoCalculoCobertura() {
		return cveSubTipoCalculoCobertura;
	}

	public void setCveSubTipoCalculoCobertura(String cveSubTipoCalculoCobertura) {
		this.cveSubTipoCalculoCobertura = cveSubTipoCalculoCobertura;
	}

	public Long getNumeroValuacion() {
		return numeroValuacion;
	}

	public void setNumeroValuacion(Long numeroValuacion) {
		this.numeroValuacion = numeroValuacion;
	}

	@Transient
	public String getDetalleConceptos(){
        final StringBuilder detalle= new StringBuilder("");
        int contador = 0;
        if(!this.getDetalleOrdenCompras().isEmpty()){
             for( DetalleOrdenCompra detalleOrden : this.getDetalleOrdenCompras()){
                  if(null!=detalleOrden.getConceptoAjuste() && !StringUtil.isEmpty(detalleOrden.getConceptoAjuste().getNombre())  ){
                      if (contador ==0){
                    	  detalle.append(detalleOrden.getConceptoAjuste().getNombre());
                      }
                      else  {
                    	  detalle.append(",").append(detalleOrden.getConceptoAjuste().getNombre());
                  }
                    contador++;
                  }
             }    
        }
        return detalle.toString();
    }
	
	@Transient
	public DocumentoFiscal getFacturaReciente(){
		DocumentoFiscal facturaReciente = null;
		if(facturasSiniestroList != null){
			for(DocumentoFiscal factura : facturasSiniestroList){
				if(factura == null || factura.getFechaCreacion().before(facturaReciente.getFechaCreacion())){
					facturaReciente = factura;
				}
			}
		}
		return facturaReciente;
	}

	public List<DocumentoFiscal> getFacturasSiniestroList() {
		return facturasSiniestroList;
	}

	public void setFacturasSiniestroList(
			List<DocumentoFiscal> facturasSiniestroList) {
		this.facturasSiniestroList = facturasSiniestroList;
	}

	
	public String getReembolsoGA() {
		return reembolsoGA;
	}

	public void setReembolsoGA(String reembolsoGA) {
		this.reembolsoGA = reembolsoGA;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	@Override
	public String toString() {
		return "OrdenCompra [id=" + id + ", numeroValuacion=" + numeroValuacion
				+ ", cveSubTipoCalculoCobertura=" + cveSubTipoCalculoCobertura
				+ ", idCoberturaCompuesta=" + idCoberturaCompuesta
				+ ", facturasSiniestro=  , curp=" + curp
				+ ", estatus=" + estatus + ", factura=" + factura
				+ ", idBeneficiario=" + idBeneficiario + ", idTercero="
				+ idTercero + ", nomBeneficiario=" + nomBeneficiario + ", rfc="
				+ rfc + ", tipo=" + tipo + ", tipoPago=" + tipoPago
				+ ", tipoProveedor=" + tipoProveedor + ", origen=" + origen
				+ "]";
	}
	
	

	public Long getIdOrdenCompraOrigen() {
		return idOrdenCompraOrigen;
	}

	public void setIdOrdenCompraOrigen(Long idOrdenCompraOrigen) {
		this.idOrdenCompraOrigen = idOrdenCompraOrigen;
	}

	public BigDecimal getDeducible() {
		return deducible;
	}

	public void setDeducible(BigDecimal deducible) {
		this.deducible = deducible;
	}

	public BigDecimal getDescuento() {
		return descuento;
	}

	public void setDescuento(BigDecimal descuento) {
		this.descuento = descuento;
	}

	public String getClabe() {
		return clabe;
	}

	public void setClabe(String clabe) {
		this.clabe = clabe;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getLada() {
		return lada;
	}

	public void setLada(String lada) {
		this.lada = lada;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getFacturaOrigen() {
		return facturaOrigen;
	}

	public void setFacturaOrigen(String facturaOrigen) {
		this.facturaOrigen = facturaOrigen;
	}

	public String getProveedorOrigen() {
		return proveedorOrigen;
	}

	public void setProveedorOrigen(String proveedorOrigen) {
		this.proveedorOrigen = proveedorOrigen;
	}

	public String getRfcOrigen() {
		return rfcOrigen;
	}

	public void setRfcOrigen(String rfcOrigen) {
		this.rfcOrigen = rfcOrigen;
	}

	public String getTipoAfectado() {
		return tipoAfectado;
	}

	public void setTipoAfectado(String tipoAfectado) {
		this.tipoAfectado = tipoAfectado;
	}

	public Integer getBancoId() {
		return bancoId;
	}

	public void setBancoId(Integer bancoId) {
		this.bancoId = bancoId;
	}

	public OrdenPagoSiniestro getOrdenPago() {
		return ordenPago;
	}

	public void setOrdenPago(OrdenPagoSiniestro ordenPago) {
		this.ordenPago = ordenPago;
	}

	/**
	 * @return the indemnizacion
	 */
	public IndemnizacionSiniestro getIndemnizacion() {
		return indemnizacion;
	}

	/**
	 * @param indemnizacion the indemnizacion to set
	 */
	public void setIndemnizacion(IndemnizacionSiniestro indemnizacion) {
		this.indemnizacion = indemnizacion;
	}

	public Boolean getAplicaDeducible() {
		return aplicaDeducible;
	}

	public void setAplicaDeducible(Boolean aplicaDeducible) {
		this.aplicaDeducible = aplicaDeducible;
	}

	public Boolean getSipac() {
		return sipac;
	}

	public void setSipac(Boolean sipac) {
		this.sipac = sipac;
	}

	public IndemnizacionSiniestro getIndemnizacionOrigen() {
		return indemnizacionOrigen;
	}

	public void setIndemnizacionOrigen(IndemnizacionSiniestro indemnizacionOrigen) {
		this.indemnizacionOrigen = indemnizacionOrigen;
	}

	public CartaPago getCartaPago() {
		return cartaPago;
	}

	public void setCartaPago(CartaPago cartaPago) {
		this.cartaPago = cartaPago;
	}

	public BigDecimal getTotalCostoUnitarioDetalle() {
		return totalCostoUnitarioDetalle;
	}

	public void setTotalCostoUnitarioDetalle(BigDecimal totalCostoUnitarioDetalle) {
		this.totalCostoUnitarioDetalle = totalCostoUnitarioDetalle;
	}
	
	
}