package mx.com.afirme.midas2.domain.siniestros.catalogo.servicio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;

@Entity
@Table(name = "SERVICIOSINIESTRO_UNIDAD", schema = "MIDAS")
public class ServicioSiniestroUnidad extends MidasAbstracto implements Entidad {
	
	
	private static final long serialVersionUID = -5865776551618962761L;
	public static final String TIPO_DEFAULT = "DEFAULT";
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SERVICIOSINIESTROUNIDAD_SEQ")
	@SequenceGenerator(name = "SERVICIOSINIESTROUNIDAD_SEQ", sequenceName = "SERVICIOSINIESTROUNIDAD_SEQ", schema = "MIDAS", allocationSize = 1)
	@Column(name = "ID", unique = true, nullable = false)
	private Long id;
		
	@Column(name = "CODIGO")
	private String codigo;
	
	@Column(name = "PLACA")
	private String placa;
	
	@Column(name = "TIPO")
	private String tipo;
	
	@Column(name = "DESCRIPCION")
	private String descripcion;
	
	@OneToOne
	@JoinColumn(name = "SERVICIOSINIESTRO_ID", referencedColumnName = "ID")
	private ServicioSiniestro servicioSiniestro;
	
	@Override
	@SuppressWarnings("unchecked")
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Long getBusinessKey() {
		return id;
	}
	
	/* ********* Getters & Setters **********/

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public ServicioSiniestro getServicioSiniestro() {
		return servicioSiniestro;
	}

	public void setServicioSiniestro(ServicioSiniestro servicioSiniestro) {
		this.servicioSiniestro = servicioSiniestro;
	}
	
}
