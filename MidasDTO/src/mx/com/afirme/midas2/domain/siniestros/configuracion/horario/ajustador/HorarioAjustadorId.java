package mx.com.afirme.midas2.domain.siniestros.configuracion.horario.ajustador;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.httpclient.util.DateUtil;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.springframework.web.util.HtmlUtils;

@Embeddable
public class HorarioAjustadorId implements Serializable {

	private static final long	serialVersionUID	= -6185909071144036768L;

	@Column(name = "AJUSTADOR_ID", nullable = false)
	private Long				ajustadorId;

	@JoinColumn(name = "OFICINA_ID", nullable = false)
	private Long				oficinaId;

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA", nullable = false)
	private Date				fecha;

	@JoinColumn(name = "HORARIOLABORAL_ID", nullable = false)
	private Long				horarioLaboralId;

	public Long getAjustadorId() {
		return ajustadorId;
	}

	public void setAjustadorId(Long ajustadorId) {
		this.ajustadorId = ajustadorId;
	}

	public Long getOficinaId() {
		return oficinaId;
	}

	public void setOficinaId(Long oficinaId) {
		this.oficinaId = oficinaId;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Long getHorarioLaboralId() {
		return horarioLaboralId;
	}

	public void setHorarioLaboralId(Long horarioLaboralId) {
		this.horarioLaboralId = horarioLaboralId;
	}

	@Override
	public String toString() {
		String separador = "%2F";
		String concatenar = "&";
		String fechaString = DateUtil.formatDate(fecha, "dd") + separador
				+ DateUtil.formatDate(fecha, "MM") + separador
				+ DateUtil.formatDate(fecha, "yyyy");
		return HtmlUtils.htmlEscape("horarioAjustadorId.fecha=")
				+ fechaString
				+ HtmlUtils.htmlEscape(concatenar
						+ "horarioAjustadorId.ajustadorId=" + ajustadorId
						+ concatenar + "horarioAjustadorId.oficinaId="
						+ oficinaId + concatenar
						+ "horarioAjustadorId.horarioLaboralId="
						+ horarioLaboralId);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj, false, null);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

}
