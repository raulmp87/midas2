package mx.com.afirme.midas2.domain.siniestros.recuperacion;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;


@Entity(name = "RecuperacionSipac")
@Table(name = "TOSNRECUPERACIONSIPAC", schema = "MIDAS")
@DiscriminatorValue("SIP")
@PrimaryKeyJoinColumn(name="RECUPERACION_ID", referencedColumnName="ID")
public class RecuperacionSipac extends Recuperacion {
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -3165992796801139591L;

	@Column(name="VALOR_ESTIMADO")
	private BigDecimal valorEstimado;
	
	@OneToOne
	@JoinColumn(name="ESTIMACION_ID", referencedColumnName="ID")
	private EstimacionCoberturaReporteCabina estimacion;
	
	@Transient
	private BigDecimal marcaId;
	
	@Transient
	private String estiloId;	
	
	@Transient
	private Short modeloVehiculo;
	
	@Transient
	private String descripcionEstilo;
	
	@Transient
	private String descripcionMarca;
	
	@Transient
	private String polizaCia;
	
	@Transient
	private String incisoCia;
	
	@Transient
	private String siniestroCia;
	
	@Transient
	private String ciaSeguros;
	
	@Transient
	private String nombreCiaSeguros;
	
	@Transient
	private String numPoliza;
	
	@Transient
	private String lugarSiniestro;
	
	@Transient
	private Date fechaSiniestro;
	
	@Transient
	private String vehiculoTercero;
	
	@Transient
	private BigDecimal totalEstimado;
	
	
	
	public RecuperacionSipac() {
		super();
	}
	
	public RecuperacionSipac(BigDecimal valorEstimado,
			EstimacionCoberturaReporteCabina estimacion, 
			String codigoUsuarioCreacion) {
		super();
		this.valorEstimado = valorEstimado;
		this.estimacion = estimacion;
		super.codigoUsuarioCreacion = codigoUsuarioCreacion;
		super.codigoUsuarioModificacion = codigoUsuarioCreacion;
		super.fechaModificacion = new Date();
		super.setTipo(TipoRecuperacion.SIPAC.toString());
		super.setEstatus(EstatusRecuperacion.PENDIENTE.toString());
		super.setOrigen(OrigenRecuperacion.AUTOMATICA.toString());
		super.setMedio(MedioRecuperacion.SIPAC.toString());
		super.setReporteCabina(estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina());
	}
	
	public BigDecimal getValorEstimado() {
		return valorEstimado;
	}

	public void setValorEstimado(BigDecimal valorEstimado) {
		this.valorEstimado = valorEstimado;
	}


	public EstimacionCoberturaReporteCabina getEstimacion() {
		return estimacion;
	}

	public void setEstimacion(EstimacionCoberturaReporteCabina estimacion) {
		this.estimacion = estimacion;
	}
	

	public BigDecimal getMarcaId() {
		return marcaId;
	}

	public void setMarcaId(BigDecimal marcaId) {
		this.marcaId = marcaId;
	}

	public String getEstiloId() {
		return estiloId;
	}

	public void setEstiloId(String estiloId) {
		this.estiloId = estiloId;
	}

	public Short getModeloVehiculo() {
		return modeloVehiculo;
	}

	public void setModeloVehiculo(Short modeloVehiculo) {
		this.modeloVehiculo = modeloVehiculo;
	}
	
	public String getDescripcionEstilo() {
		return descripcionEstilo;
	}

	public void setDescripcionEstilo(String descripcionEstilo) {
		this.descripcionEstilo = descripcionEstilo;
	}

	public String getDescripcionMarca() {
		return descripcionMarca;
	}

	public void setDescripcionMarca(String descripcionMarca) {
		this.descripcionMarca = descripcionMarca;
	}
	
	public String getPolizaCia() {
		return polizaCia;
	}
	
	public void setPolizaCia(String polizaCia) {
		this.polizaCia = polizaCia;
	}
	
	public String getIncisoCia() {
		return incisoCia;
	}
	
	public void setIncisoCia(String incisoCia) {
		this.incisoCia = incisoCia;
	}
	
	public String getSiniestroCia() {
		return siniestroCia;
	}
	
	public void setSiniestroCia(String siniestroCia) {
		this.siniestroCia = siniestroCia;
	}
	
	public String getCiaSeguros() {
		return ciaSeguros;
	}
	
	public void setCiaSeguros(String ciaSeguros) {
		this.ciaSeguros = ciaSeguros; 
	}
	
	public String getNombreCiaSeguros() {
		return nombreCiaSeguros;
	}
	
	public void setNombreCiaSeguros(String nombreCia) {
		this.nombreCiaSeguros = nombreCia;
	}
	
	public String getNumPoliza() {
		return numPoliza;
	}
	
	public void setNumPoliza(String numPoliza) {
		this.numPoliza = numPoliza;
	}

	public String getLugarSiniestro() {
		return lugarSiniestro;
	}

	public void setLugarSiniestro(String lugarSiniestro) {
		this.lugarSiniestro = lugarSiniestro;
	}

	public Date getFechaSiniestro() {
		return fechaSiniestro;
	}

	public void setFechaSiniestro(Date fechaSiniestro) {
		this.fechaSiniestro = fechaSiniestro;
	}

	public String getVehiculoTercero() {
		return vehiculoTercero;
	}

	public void setVehiculoTercero(String vehiculoTercero) {
		this.vehiculoTercero = vehiculoTercero;
	}

	public BigDecimal getTotalEstimado() {
		return totalEstimado;
	}

	public void setTotalEstimado(BigDecimal totalEstimado) {
		this.totalEstimado = totalEstimado;
	}
	
}
