package mx.com.afirme.midas2.domain.siniestros.depuracion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.domain.MidasAbstracto;

@Entity(name = "CoberturaDepuracionReserva")
@Table(name = "TOCOBERTURADEPURACION", schema = "MIDAS")
public class CoberturaDepuracionReserva extends MidasAbstracto  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3728779015532667786L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOCOBERTURADEPURACION_ID_GENERATOR")
	@SequenceGenerator(name="TOCOBERTURADEPURACION_ID_GENERATOR", schema="MIDAS", sequenceName="TOCOBERTURADEPURACION_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;
	
	@Column(name="CLAVE_SUBCALCULO")
	private String claveSubCalculo;
	
	@ManyToOne
	@JoinColumn(name = "IDTOCOBERTURA", referencedColumnName = "idToCobertura")
	private CoberturaDTO cobertura;
	
	@ManyToOne
	@JoinColumn(name = "CONFIGURACIONDEPURACION_ID", referencedColumnName = "id")
	private ConfiguracionDepuracionReserva configuracion;
	
	@Transient
	private String nombreCobertura;
	
	@ManyToOne
	@JoinColumn(name = "IDTOSECCION", referencedColumnName = "idToSeccion")
	private SeccionDTO seccion;
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getClaveSubCalculo() {
		return claveSubCalculo;
	}

	public void setClaveSubCalculo(String claveSubCalculo) {
		this.claveSubCalculo = claveSubCalculo;
	}

	public CoberturaDTO getCobertura() {
		return cobertura;
	}

	public void setCobertura(CoberturaDTO cobertura) {
		this.cobertura = cobertura;
	}

	public ConfiguracionDepuracionReserva getConfiguracion() {
		return configuracion;
	}

	public void setConfiguracion(ConfiguracionDepuracionReserva configuracion) {
		this.configuracion = configuracion;
	}

	public String getNombreCobertura() {
		return nombreCobertura;
	}

	public void setNombreCobertura(String nombreCobertura) {
		this.nombreCobertura = nombreCobertura;
	}

	public SeccionDTO getSeccion() {
		return seccion;
	}

	public void setSeccion(SeccionDTO seccion) {
		this.seccion = seccion;
	}

}
