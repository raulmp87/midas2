/**
 * 
 */
package mx.com.afirme.midas2.domain.siniestros.catalogo.solicitudautorizacion;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

/**
 * @author admin
 *
 */
@Entity
@Table(name="TCSOLAUTORIZACION",schema="MIDAS")
@Access(AccessType.FIELD)
public class SolicitudAutorizacion extends MidasAbstracto implements Entidad  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCSOLAUTORIZACION_SEQ")
	@SequenceGenerator(name = "IDTCSOLAUTORIZACION_SEQ",  schema="MIDAS", sequenceName = "IDTCSOLAUTORIZACION_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Long id;
	
	@Column(name="CODIGO_USUARIO_AUTORIZADOR")
	private String codUsuarioAutorizador;

	@Column(name="ESTATUS")
	private Integer estatus;

	@Temporal (TemporalType.TIMESTAMP)
	@Column(name="FECHA_CAMBIO_ESTATUS")
	private Date fechaModEstatus;
	
	@OneToOne( mappedBy="solicitudAutorizacion")
	@JoinFetch(JoinFetchType.OUTER)
	private SolicitudAutorizacionVigencia solicitudAutorizacionVigencia;
	
	@OneToOne( mappedBy="solicitudAutorizacion")
	@JoinFetch(JoinFetchType.OUTER)
	private SolicitudAutorizacionAjuste  solicitudAutorizacionAjuste;
	
	@Column(name="TIPO_SOLICITUD")
	private String tipoSolicitud;

	@Column(name="COMENTARIOS")
	private String comentarios;
	
	
	@Override
	public Long getKey() {
		return this.id;
	}


	@Override
	public String getValue() {
		return null;
	}

	@Override
	public Long getBusinessKey() {
		return this.id;
	}

	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getCodUsuarioAutorizador() {
		return codUsuarioAutorizador;
	}


	public void setCodUsuarioAutorizador(String codUsuarioAutorizador) {
		this.codUsuarioAutorizador = codUsuarioAutorizador;
	}


	public Integer getEstatus() {
		return estatus;
	}


	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}


	public Date getFechaModEstatus() {
		return fechaModEstatus;
	}


	public void setFechaModEstatus(Date fechaModEstatus) {
		this.fechaModEstatus = fechaModEstatus;
	}


	public String getTipoSolicitud() {
		return tipoSolicitud;
	}


	public void setTipoSolicitud(String tipoSolicitud) {
		this.tipoSolicitud = tipoSolicitud;
	}


	public SolicitudAutorizacionVigencia getSolicitudAutorizacionVigencia() {
		return solicitudAutorizacionVigencia;
	}


	public void setSolicitudAutorizacionVigencia(
			SolicitudAutorizacionVigencia solicitudAutorizacionVigencia) {
		this.solicitudAutorizacionVigencia = solicitudAutorizacionVigencia;
	}


	public SolicitudAutorizacionAjuste getSolicitudAutorizacionAjuste() {
		return solicitudAutorizacionAjuste;
	}


	public void setSolicitudAutorizacionAjuste(
			SolicitudAutorizacionAjuste solicitudAutorizacionAjuste) {
		this.solicitudAutorizacionAjuste = solicitudAutorizacionAjuste;
	}


	public String getComentarios() {
		return comentarios;
	}


	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}
	
	
	
}
