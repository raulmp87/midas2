package mx.com.afirme.midas2.domain.siniestros.autorizacion;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;

/**
 * @author simavera
 */
@Entity(name = "ParametroAutorizacionReserva")
@Table(name = "TOPARAMETROAUTRESERVA", schema = "MIDAS")
public class ParametroAutorizacionReserva extends MidasAbstracto implements Entidad {

	private static final long serialVersionUID = -1059313302026237761L;
	
	@Id
	@SequenceGenerator(name = "IDPARAMETRO_SEQ_GENERADOR",allocationSize = 1, sequenceName = "TOPARAMETROAUTRESERVA_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "IDPARAMETRO_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="OFICINA_ID", referencedColumnName = "ID")	
	private Oficina oficina;
	
	@Column(name="NOMBRE_CONFIGURACION", nullable = false , length = 100)
	private String nombreConfiguracion;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SECCION_ID", referencedColumnName = "IDTOSECCION")
	private SeccionDTO seccion;
	
	//bi-directional many-to-one association to CoberturaDTO
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="COBERTURA_ID", referencedColumnName = "IDTOCOBERTURA")
	private CoberturaDTO cobertura;
	
	@Column(name="CLAVE_SUBCALCULO", length = 3)
	private String claveSubCalculo;
	
	@Column(name="ESTATUS", nullable = false)
	private Boolean estatus;
	
	@Column(name="CONDICION_RESERVA")
	private Boolean condicionReserva;
	
	@Column(name="CRITERIO_RANGO_RESERVA")
	private String criterioRangoReserva;
	
	@Column(name="MONTO_RESERVA_INI")
	private BigDecimal montoReservaInicial;
	
	@Column(name="MONTO_RESERVA_FIN")
	private BigDecimal montoReservaFinal;
	
	@Column(name="CONDICION_DEDUCIBLE")
	private Boolean condicionDeducible;
	
	@Column(name="CRITERIO_RANGO_DEDUCIBLE")
	private String criterioRangoDeducible;
	
	@Column(name="MONTO_DEDUCIBLE_INI")
	private BigDecimal montoDeducibleInicial;
	
	@Column(name="MONTO_DEDUCIBLE_FIN")
	private BigDecimal montoDeducibleFinal;
	
	@Column(name="CONDICION_PORCENTAJE_DED")
	private Boolean condicionPorcentajeDeducible;
	
	@Column(name="CRITERIO_RANGO_PORCENTAJE")
	private String criterioRangoPorcentaje;
	
	@Column(name="MONTO_PORCENTAJE_INI")
	private BigDecimal montoPorcentajeInicial;
	
	@Column(name="MONTO_PORCENTAJE_FIN")
	private BigDecimal montoPorcentajeFinal;
	
	@Transient
	private String descripcionEstatus;
	
	@Transient
	private String fechaCreacionStr;
	
	@Transient
	private String nombreCobertura;
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * @return the oficina
	 */
	public Oficina getOficina() {
		return oficina;
	}
	
	/**
	 * @param oficina the oficina to set
	 */
	public void setOficina(Oficina oficina) {
		this.oficina = oficina;
	}
	
	/**
	 * @return the nombreConfiguracion
	 */
	public String getNombreConfiguracion() {
		return nombreConfiguracion;
	}
	
	/**
	 * @param nombreConfiguracion the nombreConfiguracion to set
	 */
	public void setNombreConfiguracion(String nombreConfiguracion) {
		this.nombreConfiguracion = nombreConfiguracion;
	}
	
	/**
	 * @return the seccion
	 */
	public SeccionDTO getSeccion() {
		return seccion;
	}
	
	/**
	 * @param seccion the seccion to set
	 */
	public void setSeccion(SeccionDTO seccion) {
		this.seccion = seccion;
	}
	
	/**
	 * @return the cobertura
	 */
	public CoberturaDTO getCobertura() {
		return cobertura;
	}
	
	/**
	 * @param cobertura the cobertura to set
	 */
	public void setCobertura(CoberturaDTO cobertura) {
		this.cobertura = cobertura;
	}
	
	/**
	 * @return the claveSubCalculo
	 */
	public String getClaveSubCalculo() {
		return claveSubCalculo;
	}
	
	/**
	 * @param claveSubCalculo the claveSubCalculo to set
	 */
	public void setClaveSubCalculo(String claveSubCalculo) {
		this.claveSubCalculo = claveSubCalculo;
	}
	
	/**
	 * @return the estatus
	 */
	public Boolean getEstatus() {
		return estatus;
	}
	
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(Boolean estatus) {
		this.estatus = estatus;
	}
	
	/**
	 * @return the condicionReserva
	 */
	public Boolean getCondicionReserva() {
		return condicionReserva;
	}
	
	/**
	 * @param condicionReserva the condicionReserva to set
	 */
	public void setCondicionReserva(Boolean condicionReserva) {
		this.condicionReserva = condicionReserva;
	}
	
	/**
	 * @return the criterioRangoReserva
	 */
	public String getCriterioRangoReserva() {
		return criterioRangoReserva;
	}
	
	/**
	 * @param criterioRangoReserva the criterioRangoReserva to set
	 */
	public void setCriterioRangoReserva(String criterioRangoReserva) {
		this.criterioRangoReserva = criterioRangoReserva;
	}
	
	/**
	 * @return the montoReservaInicial
	 */
	public BigDecimal getMontoReservaInicial() {
		return montoReservaInicial;
	}
	
	/**
	 * @param montoReservaInicial the montoReservaInicial to set
	 */
	public void setMontoReservaInicial(BigDecimal montoReservaInicial) {
		this.montoReservaInicial = montoReservaInicial;
	}
	
	/**
	 * @return the montoReservaFinal
	 */
	public BigDecimal getMontoReservaFinal() {
		return montoReservaFinal;
	}
	
	/**
	 * @param montoReservaFinal the montoReservaFinal to set
	 */
	public void setMontoReservaFinal(BigDecimal montoReservaFinal) {
		this.montoReservaFinal = montoReservaFinal;
	}
	
	/**
	 * @return the condicionDeducible
	 */
	public Boolean getCondicionDeducible() {
		return condicionDeducible;
	}
	
	/**
	 * @param condicionDeducible the condicionDeducible to set
	 */
	public void setCondicionDeducible(Boolean condicionDeducible) {
		this.condicionDeducible = condicionDeducible;
	}
	
	/**
	 * @return the criterioRangoDeducible
	 */
	public String getCriterioRangoDeducible() {
		return criterioRangoDeducible;
	}
	
	/**
	 * @param criterioRangoDeducible the criterioRangoDeducible to set
	 */
	public void setCriterioRangoDeducible(String criterioRangoDeducible) {
		this.criterioRangoDeducible = criterioRangoDeducible;
	}
	
	/**
	 * @return the montoDeducibleInicial
	 */
	public BigDecimal getMontoDeducibleInicial() {
		return montoDeducibleInicial;
	}
	
	/**
	 * @param montoDeducibleInicial the montoDeducibleInicial to set
	 */
	public void setMontoDeducibleInicial(BigDecimal montoDeducibleInicial) {
		this.montoDeducibleInicial = montoDeducibleInicial;
	}
	
	/**
	 * @return the montoDeducibleFinal
	 */
	public BigDecimal getMontoDeducibleFinal() {
		return montoDeducibleFinal;
	}
	
	/**
	 * @param montoDeducibleFinal the montoDeducibleFinal to set
	 */
	public void setMontoDeducibleFinal(BigDecimal montoDeducibleFinal) {
		this.montoDeducibleFinal = montoDeducibleFinal;
	}
	
	/**
	 * @return the condicionPorcentajeDeducible
	 */
	public Boolean getCondicionPorcentajeDeducible() {
		return condicionPorcentajeDeducible;
	}
	
	/**
	 * @param condicionPorcentajeDeducible the condicionPorcentajeDeducible to set
	 */
	public void setCondicionPorcentajeDeducible(Boolean condicionPorcentajeDeducible) {
		this.condicionPorcentajeDeducible = condicionPorcentajeDeducible;
	}
	
	/**
	 * @return the criterioRangoPorcentaje
	 */
	public String getCriterioRangoPorcentaje() {
		return criterioRangoPorcentaje;
	}
	
	/**
	 * @param criterioRangoPorcentaje the criterioRangoPorcentaje to set
	 */
	public void setCriterioRangoPorcentaje(String criterioRangoPorcentaje) {
		this.criterioRangoPorcentaje = criterioRangoPorcentaje;
	}
	
	/**
	 * @return the montoPorcentajeInicial
	 */
	public BigDecimal getMontoPorcentajeInicial() {
		return montoPorcentajeInicial;
	}
	
	/**
	 * @param montoPorcentajeInicial the montoPorcentajeInicial to set
	 */
	public void setMontoPorcentajeInicial(BigDecimal montoPorcentajeInicial) {
		this.montoPorcentajeInicial = montoPorcentajeInicial;
	}
	
	/**
	 * @return the montoPorcentajeFinal
	 */
	public BigDecimal getMontoPorcentajeFinal() {
		return montoPorcentajeFinal;
	}
	
	/**
	 * @param montoPorcentajeFinal the montoPorcentajeFinal to set
	 */
	public void setMontoPorcentajeFinal(BigDecimal montoPorcentajeFinal) {
		this.montoPorcentajeFinal = montoPorcentajeFinal;
	}
	
	/**
	 * @return the descripcionEstatus
	 */
	public String getDescripcionEstatus() {
		if(this.estatus){
			return "Activo";
		}else{
			return "Inactivo";
		}
	}
	/**
	 * @param descripcionEstatus the descripcionEstatus to set
	 */
	public void setDescripcionEstatus(String descripcionEstatus) {
		this.descripcionEstatus = descripcionEstatus;
	}
	
	/**
	 * 
	 * @return the date format
	 */
	public String getFechaCreacionStr(){
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		sdf.format( getFechaCreacion() );
		return sdf.format( getFechaCreacion() ); 
	}
	
	/**
	 * @param fechaCreacionStr the fechaCreacionStr to set
	 */
	public void setFechaCreacionStr(String fechaCreacionStr) {
		this.fechaCreacionStr = fechaCreacionStr;
	}
	/**
	 * @return the nombreCobertura
	 */
	public String getNombreCobertura() {
		return nombreCobertura;
	}

	/**
	 * @param nombreCobertura the nombreCobertura to set
	 */
	public void setNombreCobertura(String nombreCobertura) {
		this.nombreCobertura = nombreCobertura;
	}

	@Override
	public Long getKey() {
		return this.getId();
	}
	
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Long getBusinessKey() {
		return this.getId();
	}
	
	@Override
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParametroAutorizacionReserva other = (ParametroAutorizacionReserva) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}