package mx.com.afirme.midas2.domain.siniestros.catalogo.pieza;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.annotation.Exportable;
import mx.com.afirme.midas2.domain.MidasAbstracto;

import org.apache.bval.constraints.NotEmpty;

@Entity
@Table(name = "TCPIEZAVALUACION", schema = "MIDAS")
public class PiezaValuacion extends MidasAbstracto {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2143750821086026289L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCPIEZAVALUACION_SEQ")
	@SequenceGenerator(name = "IDTCPIEZAVALUACION_SEQ", sequenceName = "IDTCPIEZAVALUACION_SEQ", schema = "MIDAS", allocationSize = 1)
	@Column(name = "ID", nullable = false)
	private Long id;
	
	@NotNull
	@NotEmpty(message="{org.hibernate.validator.constraints.NotEmpty.message}")
	@Column(name = "SECCIONAUTOMOVIL", nullable = false)
	private String seccionAutomovil;
	
	@NotNull
	@NotEmpty(message="{org.hibernate.validator.constraints.NotEmpty.message}")
	@Column(name = "DESCRIPCION", nullable = false)
	private String descripcion;
	
	@NotNull
	@NotEmpty(message="{org.hibernate.validator.constraints.NotEmpty.message}")
	@Column(name = "ESTATUS", nullable = false)
	private int estatus;
	
	@Transient
	private String nombreSeccionAutomovil = null;
	
	@Transient
	private String nombreEstatus = null;
	
	public PiezaValuacion() {
		super();
	}

	public PiezaValuacion(Long id, String seccionAutomovil, String descripcion,
			int estatus) {
		super();
		this.id = id;
		this.seccionAutomovil = seccionAutomovil;
		this.descripcion = descripcion;
		this.estatus = estatus;
	}

	public PiezaValuacion(Long id, String seccionAutomovil, String descripcion,
			int estatus, String nombreSeccionAutomovil, String nombreEstatus) {
		this(id, seccionAutomovil, descripcion, estatus);
		this.nombreSeccionAutomovil = nombreSeccionAutomovil;
		this.nombreEstatus = nombreEstatus;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		return this.getSeccionAutomovil()+" "+this.getDescripcion();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.getId();
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Exportable(columnName = "No. Pieza", columnOrder = 0)
	public Long getId() {
		return id;
	}

	public String getSeccionAutomovil() {
		return seccionAutomovil;
	}
	
	@Exportable(columnName = "Sección Automovil", columnOrder = 1)
	public String getNombreSeccionAutomovil() {
		return this.nombreSeccionAutomovil;		
	}
	
	public void setNombreSeccionAutomovil(String nombreSeccionAutomovil) {
		this.nombreSeccionAutomovil = nombreSeccionAutomovil;
	}
	
	@Exportable(columnName = "Descripción Pieza", columnOrder = 2)
	public String getDescripcion() {
		return descripcion;
	}

	@Exportable(columnName = "Estatus", columnOrder = 3)
	@Transient
	public String getNombreEstatus(){
		return nombreEstatus;
	}
	
	public int getEstatus() {
		return estatus;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setSeccionAutomovil(String seccionAutomovil) {
		this.seccionAutomovil = seccionAutomovil;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PiezaValuacion other = (PiezaValuacion) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}



}
