package mx.com.afirme.midas2.domain.siniestros.recuperacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.personadireccion.CiudadMidas;
import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.VentaSalvamento.EstatusSalvamento;
import mx.com.afirme.midas2.util.CollectionUtils;
import mx.com.afirme.midas2.util.StringUtil;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;


@Entity(name = "RecuperacionSalvamento")
@Table(name = "TOSNRECUPERACIONSALVAMENTO", schema = "MIDAS")
@DiscriminatorValue("SVM")
@PrimaryKeyJoinColumn(name="RECUPERACION_ID", referencedColumnName="ID")
public class RecuperacionSalvamento extends Recuperacion {

	private static final long serialVersionUID = 374347850454762402L;

	public enum TipoSalvamento{POR_DANIOS, RT, DEVOLUCION};
	
	@Column(name="VALOR_ESTIMADO")
	private BigDecimal valorEstimado;
	
	@Column(name="IVA_VALOR_ESTIMADO")
	private BigDecimal ivaValorEstimado;
	
	@Column(name="SUBTOTAL_VALOR_ESTIMADO")
	private BigDecimal subtotalValorEstimado;
	
	@Column(name="PORCENTAJE_VALOR_ESTIMADO")
	private BigDecimal porcentajeValorEstimado;
	
	@Column(name="ABANDONO_INCOSTEABLE")
	private Boolean abandonoIncosteable = Boolean.FALSE;
	
	@Column(name="ESTACIONAMIENTO_TALLER")
	private String estacionamientoTaller;
	
	@Column(name="UBICACION_SALVAMENTO")
	private String direccionSalvamento;
	
	@Column(name="UBICACION_DETERMINACION")
	private String ubicacionDeterminacion;
	
	@Column(name="CONTACTO_TALLER_UBICACION")
	private String contactoTallerUbicacion;
	
	@OneToOne
	@JoinColumn(name = "CODIGO_CIUDAD_UBICACION", referencedColumnName = "CITY_ID")
	private CiudadMidas ciudad;
	
	@Column(name="MOTIVO_ABANDONO_INCOSTEABLE")
	private String motivoAbandonoIncosteable;
	
	@Column(name="USUARIO_ABANDONO_INCOSTEABLE")
	private String codigoUsuarioAbandono;
	
	@OneToOne
	@JoinColumn(name="ESTIMACION_ID", referencedColumnName="ID")
	private EstimacionCoberturaReporteCabina estimacion;
	
	@Column(name="TIPO_SALVAMENTO")
	@Enumerated(EnumType.STRING)
	private TipoSalvamento tipoSalvamento;
	
	@Column(name="FECHA_INICIO_SUBASTA")
	@Temporal(TemporalType.DATE)
	private Date fechaInicioSubasta;
	
	@Column(name="SUBASTA_ACTUAL")
	private Long subastaActual;
	
	@OneToMany(fetch=FetchType.LAZY,  mappedBy = "recuperacionSalvamento", cascade=CascadeType.ALL)
	@JoinFetch(JoinFetchType.OUTER)
	private List<VentaSalvamento> listaVentasSalvamentos = new ArrayList<VentaSalvamento>();
	
	@Column(name="SOLICITUD_PRORROGA")
	private boolean solicitudProrroga;
	
	@Column(name="FECHA_LIMITE_PRORROGA")
	@Temporal(TemporalType.DATE)
	private Date fechaLimiteProrroga;
	
	@Column(name="COMENTARIOS_PRORROGA")
	private String comentariosProrroga;
	
	@Column(name="SOLICITANTE_PRORROGA")
	private String solicitanteProrroga;
	
	@Column(name="FECHA_SOLICITUD_PRORROGA")
	@Temporal(TemporalType.DATE)
	private Date fechaSolicitudProrroga;	
	
	@Column(name="AUTORIZACION_PRORROGA")
	private boolean autorizacionProrroga;
	
	@Column(name="AUTORIZADOR_PRORROGA")
	private String autorizadorProrroga;
	
	@Column(name="FECHA_AUTORIZACION_PRORROGA")
	@Temporal(TemporalType.DATE)
	private Date fechaAutorizacionProrroga;
	
	@Column(name="BANCO_CONFIRMACION_INGRESO")
	private Long bancoConfirmacionIngreso;
	
	@Column(name="MONTO_CONFIRMACION_INGRESO")
	private BigDecimal montoConfirmacionIngreso;
	
	@Column(name="CONFIRMADO_POR")
	private String confirmadoPor;
	
	@Column(name="CUENTA_CONFIRMACION_INGRESO")
	private String cuentaConfirmacionIngreso;
	
	@Column(name="FECHA_DEPOSITO_CONF_INGRESO")
	@Temporal(TemporalType.DATE)
	private Date fechaDepositoConfirmacionIngreso;
	
	@Column(name="FECHA_DEPOSITO_CONFIRMACION")
	@Temporal(TemporalType.DATE)
	private Date fechaDepositoConfirmacion;	
	
	
	@Transient
	private String indemnizada;
	
	@Transient
	private Date fechaIndemnizacion;
	
	@Transient
	private Date fechaDevolucion;
	
	@Transient
	private String ptDocumentada;
	
	@Transient
	private Integer antiguedadSubasta;

	@Transient
	private Integer cantidadSubastas;
	
	public RecuperacionSalvamento() {
		super();
	}
	
	
	public RecuperacionSalvamento(BigDecimal valorEstimado,
			Boolean abandonoIncosteable, String ubicacionDeterminacion,
			String direccionSalvamento, CiudadMidas ciudad, EstimacionCoberturaReporteCabina estimacion,
			TipoSalvamento tipoSalvamento, String codigoUsuarioCreacion, String motivoIncosteable) {
		super();
		this.valorEstimado = valorEstimado;
		this.abandonoIncosteable = abandonoIncosteable;
		this.ubicacionDeterminacion = ubicacionDeterminacion;
		this.direccionSalvamento = direccionSalvamento;
		this.ciudad = ciudad;		
		this.estimacion = estimacion;
		this.tipoSalvamento = tipoSalvamento;
		this.motivoAbandonoIncosteable = motivoIncosteable;
		super.codigoUsuarioCreacion = codigoUsuarioCreacion;
		super.codigoUsuarioModificacion = codigoUsuarioCreacion;
		super.fechaModificacion = new Date();
		super.setTipo(TipoRecuperacion.SALVAMENTO.toString());
		super.setEstatus(EstatusRecuperacion.REGISTRADO.toString());
		super.setOrigen(OrigenRecuperacion.AUTOMATICA.toString());
		super.setMedio(MedioRecuperacion.VENTA.toString());
		super.setTipoOrdenCompra(TipoOrdenCompra.AFECTACION.toString());
		super.setReporteCabina(estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina());
	}

	public VentaSalvamento obtenerVentaActiva(){
		
		VentaSalvamento ventaActiva = null;
		
		for(VentaSalvamento lVentaSalvamento: CollectionUtils.emptyIfNull( listaVentasSalvamentos ) ){
			String estatus = lVentaSalvamento.getEstatus().toString();
			if( estatus.equals(EstatusSalvamento.ACT.toString()) ){
				ventaActiva = lVentaSalvamento;
				break;
			}
			
		}
		
		return ventaActiva;

	}
	
	public void agregarVenta(VentaSalvamento venta){
		venta.setRecuperacionSalvamento(this);
		listaVentasSalvamentos.add(venta);
	}
	
	public BigDecimal getValorEstimado() {
		return valorEstimado;
	}

	public void setValorEstimado(BigDecimal valorEstimado) {
		this.valorEstimado = valorEstimado;
	}

	public Boolean getAbandonoIncosteable() {
		return abandonoIncosteable;
	}

	public void setAbandonoIncosteable(Boolean abandonoIncosteable) {
		this.abandonoIncosteable = abandonoIncosteable;
	}

	public String getEstacionamientoTaller() {
		return estacionamientoTaller;
	}

	public void setEstacionamientoTaller(String estacionamientoTaller) {
		this.estacionamientoTaller = estacionamientoTaller;
	}

	public String getDireccionSalvamento() {
		return direccionSalvamento;
	}

	public void setDireccionSalvamento(String direccionSalvamento) {
		this.direccionSalvamento = direccionSalvamento;
	}

	public CiudadMidas getCiudad() {
		return ciudad;
	}

	public void setCiudad(CiudadMidas ciudad) {
		this.ciudad = ciudad;
	}

	public String getMotivoAbandonoIncosteable() {
		return motivoAbandonoIncosteable;
	}

	public void setMotivoAbandonoIncosteable(String motivoAbandonoIncosteable) {
		this.motivoAbandonoIncosteable = motivoAbandonoIncosteable;
	}

	public String getCodigoUsuarioAbandono() {
		return codigoUsuarioAbandono;
	}

	public void setCodigoUsuarioAbandono(String codigoUsuarioAbandono) {
		this.codigoUsuarioAbandono = codigoUsuarioAbandono;
	}

	public EstimacionCoberturaReporteCabina getEstimacion() {
		return estimacion;
	}

	public void setEstimacion(EstimacionCoberturaReporteCabina estimacion) {
		this.estimacion = estimacion;
	}

	public TipoSalvamento getTipoSalvamento() {
		return tipoSalvamento;
	}

	public void setTipoSalvamento(TipoSalvamento tipoSalvamento) {
		this.tipoSalvamento = tipoSalvamento;
	}

	@Transient
	public String getTipoSalvamentoStr(){
		String tipo; 
		switch (tipoSalvamento) {
		case POR_DANIOS:
			tipo = "POR DAÑOS";
			break;
		case RT:
			tipo = "Por Recuperación de Robo Total";
			break;
		case DEVOLUCION:
			tipo = "Devolución";
			break;
		default:
			tipo = "No identificado";
			break;
		}
		return tipo;
	}
	
	public void getTipoSalvamentoStr(String val){}
	
	@Transient
	public EstadoMidas getEdoUbicacion(){
		if(ciudad != null){
			return ciudad.getEstado();
		}
		return null;
	}

	public void setEdoUbicacion(EstadoMidas edo){}

	public String getUbicacionDeterminacion() {
		return ubicacionDeterminacion;
	}


	public void setUbicacionDeterminacion(String ubicacionDeterminacion) {
		this.ubicacionDeterminacion = ubicacionDeterminacion;
	}
	
	public String getContactoTallerUbicacion() {
		return contactoTallerUbicacion;
	}


	public void setContactoTallerUbicacion(String contactoTallerUbicacion) {
		this.contactoTallerUbicacion = contactoTallerUbicacion;
	}


	public Date getFechaInicioSubasta() {
		return fechaInicioSubasta;
	}


	public void setFechaInicioSubasta(Date fechaInicioSubasta) {
		this.fechaInicioSubasta = fechaInicioSubasta;
	}


	public String getIndemnizada() {
		return indemnizada;
	}


	public void setIndemnizada(String indemnizada) {
		this.indemnizada = indemnizada;
	}


	public Date getFechaIndemnizacion() {
		return fechaIndemnizacion;
	}


	public void setFechaIndemnizacion(Date fechaIndemnizacion) {
		this.fechaIndemnizacion = fechaIndemnizacion;
	}


	public Date getFechaDevolucion() {
		return fechaDevolucion;
	}


	public void setFechaDevolucion(Date fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}


	public String getPtDocumentada() {
		return ptDocumentada;
	}


	public void setPtDocumentada(String ptDocumentada) {
		this.ptDocumentada = ptDocumentada;
	}


	public Integer getAntiguedadSubasta() {
		return antiguedadSubasta;
	}


	public void setAntiguedadSubasta(Integer antiguedadSubasta) {
		this.antiguedadSubasta = antiguedadSubasta;
	}


	public Integer getCantidadSubastas() {
		return cantidadSubastas;
	}


	public void setCantidadSubastas(Integer cantidadSubastas) {
		this.cantidadSubastas = cantidadSubastas;
	}


	public Long getSubastaActual() {
		return subastaActual;
	}


	public void setSubastaActual(Long subastaActual) {
		this.subastaActual = subastaActual;
	}
	
	public void setSubastaActual(String subastaActual) {
		this.subastaActual = !StringUtil.isEmpty(subastaActual) ? Long.valueOf(subastaActual) : null;
	}


	public List<VentaSalvamento> getListaVentasSalvamentos() {
		return listaVentasSalvamentos;
	}


	public void setListaVentasSalvamentos(
			List<VentaSalvamento> listaVentasSalvamentos) {
		this.listaVentasSalvamentos = listaVentasSalvamentos;
	}


	public Date getFechaLimiteProrroga() {
		return fechaLimiteProrroga;
	}


	public void setFechaLimiteProrroga(Date fechaLimiteProrroga) {
		this.fechaLimiteProrroga = fechaLimiteProrroga;
	}


	public String getComentariosProrroga() {
		return comentariosProrroga;
	}


	public void setComentariosProrroga(String comentariosProrroga) {
		this.comentariosProrroga = comentariosProrroga;
	}


	public String getSolicitanteProrroga() {
		return solicitanteProrroga;
	}


	public void setSolicitanteProrroga(String solicitanteProrroga) {
		this.solicitanteProrroga = solicitanteProrroga;
	}


	public Date getFechaSolicitudProrroga() {
		return fechaSolicitudProrroga;
	}


	public void setFechaSolicitudProrroga(Date fechaSolicitudProrroga) {
		this.fechaSolicitudProrroga = fechaSolicitudProrroga;
	}

	public String getAutorizadorProrroga() {
		return autorizadorProrroga;
	}


	public void setAutorizadorProrroga(String autorizadorProrroga) {
		this.autorizadorProrroga = autorizadorProrroga;
	}


	public Date getFechaAutorizacionProrroga() {
		return fechaAutorizacionProrroga;
	}


	public void setFechaAutorizacionProrroga(Date fechaAutorizacionProrroga) {
		this.fechaAutorizacionProrroga = fechaAutorizacionProrroga;
	}


	public boolean isSolicitudProrroga() {
		return solicitudProrroga;
	}


	public void setSolicitudProrroga(boolean solicitudProrroga) {
		this.solicitudProrroga = solicitudProrroga;
	}


	public boolean isAutorizacionProrroga() {
		return autorizacionProrroga;
	}


	public void setAutorizacionProrroga(boolean autorizacionProrroga) {
		this.autorizacionProrroga = autorizacionProrroga;
	}


	public Long getBancoConfirmacionIngreso() {
		return bancoConfirmacionIngreso;
	}


	public void setBancoConfirmacionIngreso(Long bancoConfirmacionIngreso) {
		this.bancoConfirmacionIngreso = bancoConfirmacionIngreso;
	}


	public BigDecimal getMontoConfirmacionIngreso() {
		return montoConfirmacionIngreso;
	}


	public void setMontoConfirmacionIngreso(BigDecimal montoConfirmacionIngreso) {
		this.montoConfirmacionIngreso = montoConfirmacionIngreso;
	}


	public String getConfirmadoPor() {
		return confirmadoPor;
	}


	public void setConfirmadoPor(String confirmadoPor) {
		this.confirmadoPor = confirmadoPor;
	}

	public Date getFechaDepositoConfirmacionIngreso() {
		return fechaDepositoConfirmacionIngreso;
	}


	public void setFechaDepositoConfirmacionIngreso(
			Date fechaDepositoConfirmacionIngreso) {
		this.fechaDepositoConfirmacionIngreso = fechaDepositoConfirmacionIngreso;
	}


	public Date getFechaDepositoConfirmacion() {
		return fechaDepositoConfirmacion;
	}


	public void setFechaDepositoConfirmacion(Date fechaDepositoConfirmacion) {
		this.fechaDepositoConfirmacion = fechaDepositoConfirmacion;
	}


	public String getCuentaConfirmacionIngreso() {
		return cuentaConfirmacionIngreso;
	}


	public void setCuentaConfirmacionIngreso(String cuentaConfirmacionIngreso) {
		this.cuentaConfirmacionIngreso = cuentaConfirmacionIngreso;
	}


	public BigDecimal getIvaValorEstimado() {
		return ivaValorEstimado;
	}


	public void setIvaValorEstimado(BigDecimal ivaValorEstimado) {
		this.ivaValorEstimado = ivaValorEstimado;
	}


	public BigDecimal getSubtotalValorEstimado() {
		return subtotalValorEstimado;
	}


	public void setSubtotalValorEstimado(BigDecimal subtotalValorEstimado) {
		this.subtotalValorEstimado = subtotalValorEstimado;
	}


	public BigDecimal getPorcentajeValorEstimado() {
		return porcentajeValorEstimado;
	}


	public void setPorcentajeValorEstimado(BigDecimal porcentajeValorEstimado) {
		this.porcentajeValorEstimado = porcentajeValorEstimado;
	}
	
}
