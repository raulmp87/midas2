package mx.com.afirme.midas2.domain.siniestros.depuracion;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.annotation.Exportable;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;

@Entity(name = "DepuracionReservaDetalle")
@Table(name = "TODEPURACIONRESERVADET", schema = "MIDAS")
public class DepuracionReservaDetalle extends MidasAbstracto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1301722789100635480L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TODEPURACIONRESERVADET_ID_GENERATOR")
	@SequenceGenerator(name="TODEPURACIONRESERVADET_ID_GENERATOR", schema="MIDAS", sequenceName="TODEPURACIONRESERVADET_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "DEPURACIONRESERVA_ID", referencedColumnName = "id")
	private DepuracionReserva depuracionReserva;
	
	@Column(name="ESTATUS")
	private Integer estatus;
	
	@ManyToOne
	@JoinColumn(name = "ESTIMACIONCOBERTURA_ID", referencedColumnName = "id")
	private EstimacionCoberturaReporteCabina estimacionCobertura;
	
	@Column(name="NUMERO")
	private Long numero;
	
	@Column(name="RESERVA_ACTUAL")
	private BigDecimal reservaActual;
	
	@Column(name="RESERVA_DEPURADA")
	private BigDecimal reservaDepurada;
	
	@Column(name="RESERVA_POR_DEPURAR")
	private BigDecimal reservaPorDepurar;
	
	@Transient
	private String nombreCobertura;
	
	@Transient
	private String estatusStr;
	
	@Transient
	private String numeroSiniestro;
	
	@Transient
	private String numeroPolizaFormateada;

	@Transient
	private Integer numeroInciso;
	
	@Transient
	private String folio;
	
	@Transient
	private String nombreAsegurado;
	
	@Transient
	private String terminoAjusteDesc;
	
	@Transient
	private String nombreConfiguracion;
	
	@Transient
	private Integer porcentajeDepurado;
	
	@Transient
	private Integer porcentajeDepurar;
	
	@Transient
	private Integer depuratedPercent;
	
	

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DepuracionReserva getDepuracionReserva() {
		return depuracionReserva;
	}

	public void setDepuracionReserva(DepuracionReserva depuracionReserva) {
		this.depuracionReserva = depuracionReserva;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public EstimacionCoberturaReporteCabina getEstimacionCobertura() {
		return estimacionCobertura;
	}

	public void setEstimacionCobertura(
			EstimacionCoberturaReporteCabina estimacionCobertura) {
		this.estimacionCobertura = estimacionCobertura;
	}

	@Exportable(columnName="NUMERO", columnOrder=0, document="DEPURADA,DEPURAR")
	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	@Exportable(columnName="RESERVA ACTUAL", columnOrder=9, document="DEPURADA,DEPURAR", format="$* #,##0.00")
	public BigDecimal getReservaActual() {
		return reservaActual;
	}

	public void setReservaActual(BigDecimal reservaActual) {
		this.reservaActual = reservaActual;
	}

	@Exportable(columnName="RESERVA DEPURADA", columnOrder=10, document="DEPURADA", format="$#,##0.00")
	public BigDecimal getReservaDepurada() {
		return reservaDepurada;
	}

	public void setReservaDepurada(BigDecimal reservaDepurada) {
		this.reservaDepurada = reservaDepurada;
	}

	@Exportable(columnName="RESERVA A DEPURAR", columnOrder=10, document="DEPURAR", format="$#,##0.00")
	public BigDecimal getReservaPorDepurar() {
		return reservaPorDepurar;
	}

	public void setReservaPorDepurar(BigDecimal reservaPorDepurar) {
		this.reservaPorDepurar = reservaPorDepurar;
	}

	/**
	 * @return the nombreCobertura
	 */
	@Exportable(columnName="COBERTURA", columnOrder=5, document="DEPURADA,DEPURAR")
	public String getNombreCobertura() {
		return nombreCobertura;
	}

	/**
	 * @param nombreCobertura the nombreCobertura to set
	 */
	public void setNombreCobertura(String nombreCobertura) {
		this.nombreCobertura = nombreCobertura;
	}

	/**
	 * @return the estatusStr
	 */
	@Exportable(columnName="DEPURADO", columnOrder=12, document="DEPURADA")
	public String getEstatusStr() {
		if( this.estatus == 0 ){
			return "No";
		}else{
			return "Sí";
		}
	}

	/**
	 * @param estatusStr the estatusStr to set
	 */
	public void setEstatusStr(String estatusStr) {
		this.estatusStr = estatusStr;
	}

	/**
	 * @return the numeroSiniestro
	 */
	@Exportable(columnName="SINIESTRO", columnOrder=1, document="DEPURADA,DEPURAR")
	public String getNumeroSiniestro() {
		return this.numeroSiniestro;
	}

	/**
	 * @param numeroSiniestro the numeroSiniestro to set
	 */
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}

	/**
	 * @return the numeroPolizaFormateada
	 */
	@Exportable(columnName="PÓLIZA", columnOrder=2, document="DEPURADA,DEPURAR")
	public String getNumeroPolizaFormateada() {
		return numeroPolizaFormateada;
	}

	/**
	 * @param numeroPolizaFormateada the numeroPolizaFormateada to set
	 */
	public void setNumeroPolizaFormateada(String numeroPolizaFormateada) {
		this.numeroPolizaFormateada = numeroPolizaFormateada;
	}

	/**
	 * @return the numeroInciso
	 */
	@Exportable(columnName="INCISO", columnOrder=3, document="DEPURADA,DEPURAR")
	public Integer getNumeroInciso() {
		return numeroInciso;
	}

	/**
	 * @param numeroInciso the numeroInciso to set
	 */
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	/**
	 * @return the folio
	 */
	@Exportable(columnName="FOLIO PASE", columnOrder=4, document="DEPURADA,DEPURAR")
	public String getFolio() {
		return folio;
	}

	/**
	 * @param folio the folio to set
	 */
	public void setFolio(String folio) {
		this.folio = folio;
	}

	/**
	 * @return the nombreAsegurado
	 */
	@Exportable(columnName="CONTRATANTE", columnOrder=6, document="DEPURADA,DEPURAR")
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}

	/**
	 * @param nombreAsegurado the nombreAsegurado to set
	 */
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}

	/**
	 * @return the terminoAjusteDesc
	 */
	@Exportable(columnName="TÉRMINO DE AJUSTE", columnOrder=7, document="DEPURADA,DEPURAR")
	public String getTerminoAjusteDesc() {
		return terminoAjusteDesc;
	}

	/**
	 * @param terminoAjusteDesc the terminoAjusteDesc to set
	 */
	public void setTerminoAjusteDesc(String terminoAjusteDesc) {
		this.terminoAjusteDesc = terminoAjusteDesc;
	}

	/**
	 * @return the nombreConfiguracion
	 */
	@Exportable(columnName="NOMBRE DE LA CONFIGURACIÓN", columnOrder=8, document="DEPURADA,DEPURAR")
	public String getNombreConfiguracion() {
		return nombreConfiguracion;
	}

	/**
	 * @param nombreConfiguracion the nombreConfiguracion to set
	 */
	public void setNombreConfiguracion(String nombreConfiguracion) {
		this.nombreConfiguracion = nombreConfiguracion;
	}

	/**
	 * @return the porcentajeDepurado
	 */
	@Exportable(columnName="% DEPURADO", columnOrder=11, document="DEPURADA", format="0.00##\\%;[Red](0.00##\\%)")
	public Integer getPorcentajeDepurado() {
		return porcentajeDepurado;
	}

	/**
	 * @param porcentajeDepurado the porcentajeDepurado to set
	 */
	public void setPorcentajeDepurado(Integer porcentajeDepurado) {
		this.porcentajeDepurado = porcentajeDepurado;
	}

	/**
	 * @return the porcentajeDepurar
	 */
	public Integer getPorcentajeDepurar() {
		return this.depuracionReserva.getConfiguracion().getPorcentaje();
	}

	/**
	 * @param porcentajeDepurar the porcentajeDepurar to set
	 */
	public void setPorcentajeDepurar(Integer porcentajeDepurar) {
		this.porcentajeDepurar = porcentajeDepurar;
	}
	
	/**
	 * @param depuratedPercent the depuratedPercent to set
	 */
	@Exportable(columnName="PORCENTAJE DEPURAR", columnOrder=11, document="DEPURAR", format="0.00##\\%;[Red](0.00##\\%)")
	public Integer getDepuratedPercent() {
		return depuratedPercent;
	}

	public void setDepuratedPercent(Integer depuratedPercent) {
		this.depuratedPercent = depuratedPercent;
	}

}
