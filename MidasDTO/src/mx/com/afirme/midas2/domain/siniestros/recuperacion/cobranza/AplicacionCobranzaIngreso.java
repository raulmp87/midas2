package mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Ingreso;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

@Entity
@Table(name="TOSNAPLICACIONCOBINGRESO", schema="MIDAS")
public class AplicacionCobranzaIngreso extends MidasAbstracto {

	private static final long serialVersionUID = -7440940826062826092L;
	
	/**
	 * TIPO APLICACION INGRESO  <code>A</code> Aplicacion <code>default</code>, <code>R</code> - Reversa
	 * @author Administrator
	 *
	 */
	public enum TipoAplicacionIngreso{A, R};

	@EmbeddedId
	private AplicacionCobranzaIngresoId id = new AplicacionCobranzaIngresoId();
	
	@ManyToOne(fetch=FetchType.LAZY)	
	@MapsId("aplicacionId")
	private AplicacionCobranza aplicacion;
	
	
	@ManyToOne(fetch=FetchType.LAZY)	
	@MapsId("ingresoId")
	private Ingreso ingreso;
	
	@Column(name="tipo")
	@Enumerated(EnumType.STRING)
	private TipoAplicacionIngreso tipo = TipoAplicacionIngreso.A;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CONCEPTO_ID", referencedColumnName="ID")
	@JoinFetch(JoinFetchType.INNER)
	private ConceptoGuia conceptoGuia;
	
	public AplicacionCobranzaIngreso(){
		super();
	}
	
	public AplicacionCobranzaIngreso(Ingreso ingreso, TipoAplicacionIngreso tipo, String codigoUsuarioCreacion,  ConceptoGuia conceptoGuia){
		super();
		this.ingreso = ingreso;
		this.id.setIngresoId(ingreso != null ? ingreso.getId() : null);
		this.tipo = tipo;
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
		this.conceptoGuia = conceptoGuia;
	}
	
	public AplicacionCobranzaIngreso(AplicacionCobranza aplicacion,
			Ingreso ingreso, TipoAplicacionIngreso tipo, String codigoUsuarioCreacion,  ConceptoGuia conceptoGuia) {
		this(ingreso, tipo, codigoUsuarioCreacion, conceptoGuia);
		this.aplicacion = aplicacion;
		this.id.setAplicacionId(aplicacion!= null ? aplicacion.getId() : null);		
	}

	public AplicacionCobranza getAplicacion() {
		return aplicacion;
	}

	public void setAplicacion(AplicacionCobranza aplicacion) {
		this.aplicacion = aplicacion;
	}

	public Ingreso getIngreso() {
		return ingreso;
	}

	public void setIngreso(Ingreso ingreso) {
		this.ingreso = ingreso;
	}

	public TipoAplicacionIngreso getTipo() {
		return tipo;
	}

	public void setTipo(TipoAplicacionIngreso tipo) {
		this.tipo = tipo;
	}
	
	public AplicacionCobranzaIngresoId getId() {
		return id;
	}

	public void setId(AplicacionCobranzaIngresoId id) {
		this.id = id;
	}
	
	public ConceptoGuia getConceptoGuia() {
		return conceptoGuia;
	}

	public void setConceptoGuia(ConceptoGuia conceptoGuia) {
		this.conceptoGuia = conceptoGuia;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getKey() {		
		return aplicacion.getKey().toString().concat(" - ").concat(ingreso.getKey().toString());
	}

	@Override
	public String getValue() {
		return aplicacion.getKey().toString().concat(" - ").concat(
				StringUtil.isEmpty(aplicacion.getValue()) ? "" : 
					getAplicacion().getValue()).concat(" - ").concat(ingreso.getValue());
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getBusinessKey() {		
		return aplicacion.getKey().toString().concat(" - ").concat(ingreso.getKey().toString());
	}

}
