/**
 * 
 */
package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.EnumBase;
import mx.com.afirme.midas2.domain.MidasAbstracto;

/**
 * @author simavera
 *
 */
@Entity(name = "AutoIncisoReporteCabina")
@Table(name = "TOAUTOINCISOREPORTECABINA", schema = "MIDAS")
public class AutoIncisoReporteCabina extends MidasAbstracto{

	private static final long serialVersionUID = -4788704540827799456L;

	@Id
	@SequenceGenerator(name = "IDAUTOINCISOREPORTECABINA_SEQ_GENERADOR",allocationSize = 1, sequenceName = "TOAUTOINCISOREPORTECABINA_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "IDAUTOINCISOREPORTECABINA_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id;
	
	@Column(name="NUMEROSERIE", length = 30)
	private String numeroSerie;
	
	//bi-directional many-to-one association to IncisoReporteCabina
	@OneToOne(fetch=FetchType.LAZY , cascade = CascadeType.PERSIST)
	@JoinColumn(name="INCISOREPORTECABINA_ID")
	private IncisoReporteCabina incisoReporteCabina;
	
	@Column(name="ESTILO_ID", length = 30)
	private String estiloId;
	
	@Column(name = "CAUSA_SINIESTRO")
	private String causaSiniestro;
	
	@Column(name = "CELULAR")
	private String celular;
	
	@Column(name = "CIA_SEGUROS")
	private String ciaSeguros;
	
	@Column(name = "CONDUCTOR")
	private String conductor;
	
	@Column(name = "CONDUCTOR_MISMO_ASEGURADO")
	private Integer conductorMismoAsegurado;
	
	@Column(name = "CURP")
	private String curp;
	
	@Column(name = "EDAD")
	private Integer edad;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_SOL_VALUACION", length = 7)
	private Date fechaSolValuacion;
	
	@Column(name = "FUGO_TERCERO_RESP")
	private Integer fugoTerceroResponsable;
	
	@Column(name = "GENERO")
	private String genero;
	
	@Column(name = "IMPORTE_ESTIMADO")
	private BigDecimal importeEstimado;
	
	@Column(name = "IMPORTE_RESERVA")
	private BigDecimal importeReserva;
	
	@Column(name = "LADA_CELULAR")
	private String ladaCelular;
	
	@Column(name = "LADA_TELEFONO")
	private String ladaTelefono;
	
	@Column(name = "NUM_OCUPANTES")
	private Integer numOcupantes;
	
	@Column(name = "NUM_VALUACION")
	private Long numValuacion;
	
	@Column(name = "PLACA")
	private String placa;
	
	@Column(name = "RECIBIDA_ORDEN_CIA")
	private Integer recibidaOrdenCia;
	
	@Column(name = "RFC")
	private String rfc;
	
	@Column(name = "SOL_VALUACION")
	private Integer solValuacion;
	
	@Column(name = "TELEFONO")
	private String telefono;
	
	@Column(name = "TERMINO_AJUSTE")
	private String terminoAjuste;
	
	@Transient
	private String terminoAjusteDesc;
	
	@Column(name = "TERMINO_SINIESTRO")
	private String terminoSiniestro;
	
	@Column(name = "TIPO_CORRESPONSABILIDAD")
	private String tipoCorresponsabilidad;
	
	@Column(name = "TIPO_LICENCIA")
	private String tipoLicencia;
	
	@Column(name = "TIPO_PERDIDA")
	private String tipoPerdida;
	
	@Column(name = "TIPO_RESPONSABILIDAD")
	private String tipoResponsabilidad;
	
	@Column(name = "UNIDAD_EQUIPO_PESADO")
	private Integer unidadEquipoPesado;
	
	@Column(name = "VERIFICADO")
	private Integer verificado;
	
	@Column(name = "COLOR")
	private String color;
	
	@Column(name = "MARCA_ID")
	private BigDecimal marcaId;
	
	@Column(name = "MODELOVEHICULO")
	private Short modeloVehiculo;
	
	@Column(name = "DESCRIPCION_FINAL")
	private String descripcionFinal;
	
	@Column(name="NUMEROMOTOR", length = 30)
	private String numeroMotor;
	
	@Column(name="NUMEROSERIEOCRA", length = 30)
	private String numeroSerieOcra;
	
	@Column(name="ESTADO_ID")
	private String estadoId;	

	@Column(name="TIPOUSO_ID")
	private Long tipoUsoId;
	
	@Column(name="NOMBRECOMPLETO_CONDUCTOR")
	private String nombreCompletoConductor;
	
	@Column(name="PERSONAASEGURADO_ID")
	private Long personaAseguradoId;
	
	@Column(name="POLIZA_CIA")
	private String polizaCia;
	
	@Column(name="INCISO_CIA")
	private String incisoCia;
	
	@Column(name="PORCENTAJEPARTICIPACION_CIA")
	private Integer porcentajeParticipacion;
	
	@Column(name="SINIESTRO_CIA")
	private String siniestroCia;	
	
	@Column(name="MONTO_DANOS")
	private BigDecimal montoDanos;	
	
	
	@Column(name="MOTIVO_RECHAZO")
	private String motivoRechazo;
	
	@Column(name="OBSERVACIONES_RECHAZO")
	private String observacionesRechazo;
	
	@Column(name="DESISTIMIENTO")
	private Integer desistimiento;
	
	@Column(name="ORIGEN")
	private String origen;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_EXPEDICION", length = 7)
	private Date fechaExpedicion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_DICTAMEN", length = 7)
	private Date fechaDictamen;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_JUICIO", length = 7)
	private Date fechaJuicio;
	
	@Transient
	private String descMarca;
	
	@Transient
	private String descTipoUso;
	
	@Transient
	private String descEstilo;
	
	@Transient
	private String descColor;
	
	@Column(name="CVEAMIS_SUPERVISIONCAMPO")
	private Long claveAmisSupervisionCampo;
	
	@Transient
	private String descripcionSupervisionCampo;

	public enum CausaSiniestro{
		CRISTALES("CS_5"),
		CRISTALES_A_REPARACION("CS_6"),
		ROBO_CON_VIOLENCIA("CS_13"),
		ROBO_ESTACIONADO("CS_14");
		
		private String value;
		
		private CausaSiniestro(String value){
			this.value=value;
		}
		
		public String getValue(){
			return value;
		}
	}
	
	public enum TerminoAjuste implements EnumBase<String>{
		SE_RECICIO_ORDENCIA("ROC"),
		SE_RECIBIO_SIPAC("SRSIPAC"),
		SE_RECIBIO_SIPAC_REEMBOLSOCIA("SRSIPACYRC"),
		SE_ENTREGO_SIPAC("SESIPAC"),
		SE_ENTREGO_NA_SIPAC_REEMBOLSOCIA("SESIPACYRC"),
		SE_ENTREGO_NA_SIPAC("SEONASIPAC"),
		SE_ENTREGO_NA_SIPAC_TERCERO("SEONASIPTR"),
		SE_ENTREGO_NA_SIPAC_REEMBOLSOCIA_TERCERO("SEONASIOCT"),
		SE_ENTREGO_SIPAC_OC_TERCERO("SEONASIOCT"),
		SE_ENTREGO_SIPAC_TERCERO("SESIPACTER"),
		SE_ENTREGO_SIPAC_REEMBOLSOCIA("SESIPACTER");
		
		private String value;
		
		private TerminoAjuste(String value){
			this.value=value;
		}
		
		public String getValue(){
			return value;
		}

		@Override
		public String getLabel() {
			return value;
		}
	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the numeroSerie
	 */
	public String getNumeroSerie() {
		return numeroSerie;
	}

	/**
	 * @param numeroSerie the numeroSerie to set
	 */
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	/**
	 * @return the incisoReporteCabina
	 */
	public IncisoReporteCabina getIncisoReporteCabina() {
		return incisoReporteCabina;
	}

	/**
	 * @param incisoReporteCabina the incisoReporteCabina to set
	 */
	public void setIncisoReporteCabina(IncisoReporteCabina incisoReporteCabina) {
		this.incisoReporteCabina = incisoReporteCabina;
	}

	/**
	 * @param estiloId the estiloId to set
	 */
	public void setEstiloId(String estiloId) {
		this.estiloId = estiloId;
	}

	/**
	 * @return the estiloId
	 */
	public String getEstiloId() {
		return estiloId;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.getId();
	}

	public String getCausaSiniestro() {
		return causaSiniestro;
	}

	public void setCausaSiniestro(String causaSiniestro) {
		this.causaSiniestro = causaSiniestro;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getCiaSeguros() {
		return ciaSeguros;
	}

	public void setCiaSeguros(String ciaSeguros) {
		this.ciaSeguros = ciaSeguros;
	}

	public String getConductor() {
		return conductor;
	}

	public void setConductor(String conductor) {
		this.conductor = conductor;
	}

	public Integer getConductorMismoAsegurado() {
		return conductorMismoAsegurado;
	}

	public void setConductorMismoAsegurado(Integer conductorMismoAsegurado) {
		this.conductorMismoAsegurado = conductorMismoAsegurado;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public Date getFechaSolValuacion() {
		return fechaSolValuacion;
	}

	public void setFechaSolValuacion(Date fechaSolValuacion) {
		this.fechaSolValuacion = fechaSolValuacion;
	}

	public Integer getFugoTerceroResponsable() {
		return fugoTerceroResponsable;
	}

	public void setFugoTerceroResponsable(Integer fugoTerceroResponsable) {
		this.fugoTerceroResponsable = fugoTerceroResponsable;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public BigDecimal getImporteEstimado() {
		return importeEstimado;
	}

	public void setImporteEstimado(BigDecimal importeEstimado) {
		this.importeEstimado = importeEstimado;
	}

	public BigDecimal getImporteReserva() {
		return importeReserva;
	}

	public void setImporteReserva(BigDecimal importeReserva) {
		this.importeReserva = importeReserva;
	}

	public String getLadaCelular() {
		return ladaCelular;
	}

	public void setLadaCelular(String ladaCelular) {
		this.ladaCelular = ladaCelular;
	}

	public String getLadaTelefono() {
		return ladaTelefono;
	}

	public void setLadaTelefono(String ladaTelefono) {
		this.ladaTelefono = ladaTelefono;
	}

	public Integer getNumOcupantes() {
		return numOcupantes;
	}

	public void setNumOcupantes(Integer numOcupantes) {
		this.numOcupantes = numOcupantes;
	}

	public Long getNumValuacion() {
		return numValuacion;
	}

	public void setNumValuacion(Long numValuacion) {
		this.numValuacion = numValuacion;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Integer getRecibidaOrdenCia() {
		return recibidaOrdenCia;
	}

	public void setRecibidaOrdenCia(Integer recibidaOrdenCia) {
		this.recibidaOrdenCia = recibidaOrdenCia;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public Integer getSolValuacion() {
		return solValuacion;
	}

	public void setSolValuacion(Integer solValuacion) {
		this.solValuacion = solValuacion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getTerminoAjuste() {
		return terminoAjuste;
	}

	public void setTerminoAjuste(String terminoAjuste) {
		this.terminoAjuste = terminoAjuste;
	}

	/**
	 * @return the terminoAjusteDesc
	 */
	public String getTerminoAjusteDesc() {
		return terminoAjusteDesc;
	}

	/**
	 * @param terminoAjusteDesc the terminoAjusteDesc to set
	 */
	public void setTerminoAjusteDesc(String terminoAjusteDesc) {
		this.terminoAjusteDesc = terminoAjusteDesc;
	}

	public String getTerminoSiniestro() {
		return terminoSiniestro;
	}

	public void setTerminoSiniestro(String terminoSiniestro) {
		this.terminoSiniestro = terminoSiniestro;
	}

	public String getTipoCorresponsabilidad() {
		return tipoCorresponsabilidad;
	}

	public void setTipoCorresponsabilidad(String tipoCorresponsabilidad) {
		this.tipoCorresponsabilidad = tipoCorresponsabilidad;
	}

	public String getTipoLicencia() {
		return tipoLicencia;
	}

	public void setTipoLicencia(String tipoLicencia) {
		this.tipoLicencia = tipoLicencia;
	}

	public String getTipoPerdida() {
		return tipoPerdida;
	}

	public void setTipoPerdida(String tipoPerdida) {
		this.tipoPerdida = tipoPerdida;
	}

	public String getTipoResponsabilidad() {
		return tipoResponsabilidad;
	}

	public void setTipoResponsabilidad(String tipoResponsabilidad) {
		this.tipoResponsabilidad = tipoResponsabilidad;
	}

	public Integer getUnidadEquipoPesado() {
		return unidadEquipoPesado;
	}

	public void setUnidadEquipoPesado(Integer unidadEquipoPesado) {
		this.unidadEquipoPesado = unidadEquipoPesado;
	}

	public Integer getVerificado() {
		return verificado;
	}

	public void setVerificado(Integer verificado) {
		this.verificado = verificado;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @return the marcaId
	 */
	public BigDecimal getMarcaId() {
		return marcaId;
	}

	/**
	 * @param marcaId the marcaId to set
	 */
	public void setMarcaId(BigDecimal marcaId) {
		this.marcaId = marcaId;
	}

	/**
	 * @return the modeloVehiculo
	 */
	public Short getModeloVehiculo() {
		return modeloVehiculo;
	}

	/**
	 * @param modeloVehiculo the modeloVehiculo to set
	 */
	public void setModeloVehiculo(Short modeloVehiculo) {
		this.modeloVehiculo = modeloVehiculo;
	}

	/**
	 * @return the descripcionFinal
	 */
	public String getDescripcionFinal() {
		return descripcionFinal;
	}

	/**
	 * @param descripcionFinal the descripcionFinal to set
	 */
	public void setDescripcionFinal(String descripcionFinal) {
		this.descripcionFinal = descripcionFinal;
	}

	public String getNumeroMotor() {
		return numeroMotor;
	}

	public void setNumeroMotor(String numeroMotor) {
		this.numeroMotor = numeroMotor;
	}

	public String getNumeroSerieOcra() {
		return numeroSerieOcra;
	}

	public void setNumeroSerieOcra(String numeroSerieOcra) {
		this.numeroSerieOcra = numeroSerieOcra;
	}

	public String getEstadoId() {
		return estadoId;
	}

	public void setEstadoId(String estadoId) {
		this.estadoId = estadoId;
	}

	public Long getTipoUsoId() {
		return tipoUsoId;
	}

	public void setTipoUsoId(Long tipoUsoId) {
		this.tipoUsoId = tipoUsoId;
	}	

	public String getNombreCompletoConductor() {
		return nombreCompletoConductor;
	}

	public void setNombreCompletoConductor(String nombreCompletoConductor) {
		this.nombreCompletoConductor = nombreCompletoConductor;
	}	

	public Long getPersonaAseguradoId() {
		return personaAseguradoId;
	}

	public void setPersonaAseguradoId(Long personaAseguradoId) {
		this.personaAseguradoId = personaAseguradoId;
	}

	public String getDescMarca() {
		return descMarca;
	}

	public void setDescMarca(String descMarca) {
		this.descMarca = descMarca;
	}

	public String getDescTipoUso() {
		return descTipoUso;
	}

	public void setDescTipoUso(String descTipoUso) {
		this.descTipoUso = descTipoUso;
	}

	public String getDescEstilo() {
		return descEstilo;
	}

	public void setDescEstilo(String descEstilo) {
		this.descEstilo = descEstilo;
	}

	public String getDescColor() {
		return descColor;
	}

	public void setDescColor(String descColor) {
		this.descColor = descColor;
	}

	public String getPolizaCia() {
		return polizaCia;
	}

	public void setPolizaCia(String polizaCia) {
		this.polizaCia = polizaCia;
	}

	public String getIncisoCia() {
		return incisoCia;
	}

	public void setIncisoCia(String incisoCia) {
		this.incisoCia = incisoCia;
	}

	public Integer getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}

	public void setPorcentajeParticipacion(Integer porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}

	public String getSiniestroCia() {
		return siniestroCia;
	}

	public void setSiniestroCia(String siniestroCia) {
		this.siniestroCia = siniestroCia;
	}

	public BigDecimal getMontoDanos() {
		return montoDanos;
	}

	public void setMontoDanos(BigDecimal montoDanos) {
		this.montoDanos = montoDanos;
	}

	public String getMotivoRechazo() {
		return motivoRechazo;
	}

	public void setMotivoRechazo(String motivoRechazo) {
		this.motivoRechazo = motivoRechazo;
	}

	public String getObservacionesRechazo() {
		return observacionesRechazo;
	}

	public void setObservacionesRechazo(String observacionesRechazo) {
		this.observacionesRechazo = observacionesRechazo;
	}

	public Integer getDesistimiento() {
		return desistimiento;
	}

	public void setDesistimiento(Integer desistimiento) {
		this.desistimiento = desistimiento;
	}	
 	
	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public Date getFechaExpedicion() {
		return fechaExpedicion;
	}

	public void setFechaExpedicion(Date fechaExpedicion) {
		this.fechaExpedicion = fechaExpedicion;
	}

	public Date getFechaDictamen() {
		return fechaDictamen;
	}

	public void setFechaDictamen(Date fechaDictamen) {
		this.fechaDictamen = fechaDictamen;
	}

	public Date getFechaJuicio() {
		return fechaJuicio;
	}

	public void setFechaJuicio(Date fechaJuicio) {
		this.fechaJuicio = fechaJuicio;
	}

	public Long getClaveAmisSupervisionCampo() {
		return claveAmisSupervisionCampo;
	}

	public void setClaveAmisSupervisionCampo(Long claveAmisSupervisionCampo) {
		this.claveAmisSupervisionCampo = claveAmisSupervisionCampo;
	}
}
