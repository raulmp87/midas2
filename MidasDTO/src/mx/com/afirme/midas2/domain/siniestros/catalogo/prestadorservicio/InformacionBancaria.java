package mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name = "TCINFORMACIONBANCARIA", schema = "MIDAS")
public class InformacionBancaria implements Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCINFORMACIONBANCARIA_SEQ")
	@SequenceGenerator(name = "IDTCINFORMACIONBANCARIA_SEQ", schema = "MIDAS", sequenceName = "IDTCINFORMACIONBANCARIA_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Long id;

	@Column(name = "NUMERO_CUENTA")
	private String numeroCuenta;
	@Column(name = "CLABE")
	private String clabe;
	@Column(name = "BANCO_ID")
	private Integer bancoId;
	@Column(name = "ESTATUS")
	private Integer estatus;

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_MOD_ESTATUS")
	private Date fechaModEstatus;

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION")
	private Date fechaCreacion;

	@Column(name = "CODIGO_USUARIO_CREACION")
	private String codigoUsuarioCreacion;

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_MODIFICACION")
	private Date fechaModificacion;

	@Column(name = "CODIGO_USUARIO_MODIFICACION")
	private String codigoUsuarioModificacion;

	@Transient
	private String bankName;

	@Transient
	private String statusName;

	@Column(name = "TIPOMONEDACA")
	private int tipoMoneda; 
	
	@Transient
	private String moneda; 
	
	@Column(name = "LOCATIONCA")
	private String location;
	
	@Column(name = "ABACA")
	private String aba;
	
	@Column(name = "SWIFTCA")
	private String swift;
	
	@Column(name = "BENEFICIARYCA")
	private String beneficiary;
	
	@Column(name = "FFCCA")
	private String ffc;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getBancoId() {
		return bancoId;
	}

	public void setBancoId(Integer bancoId) {
		this.bancoId = bancoId;
	}

	public String getClabe() {
		return clabe;
	}

	public void setClabe(String clabe) {
		this.clabe = clabe;
	}

	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	public String getCodigoUsuarioModificacion() {
		return codigoUsuarioModificacion;
	}

	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModEstatus() {
		return fechaModEstatus;
	}

	public void setFechaModEstatus(Date fechaModEstatus) {
		this.fechaModEstatus = fechaModEstatus;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	
	public int getTipoMoneda() {
		return tipoMoneda;
	}

	public void setTipoMoneda(int tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getAba() {
		return aba;
	}

	public void setAba(String aba) {
		this.aba = aba;
	}

	public String getSwift() {
		return swift;
	}

	public void setSwift(String swift) {
		this.swift = swift;
	}

	public String getBeneficiary() {
		return beneficiary;
	}

	public void setBeneficiary(String beneficiary) {
		this.beneficiary = beneficiary;
	}

	public String getFfc() {
		return ffc;
	}

	public void setFfc(String ffc) {
		this.ffc = ffc;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		return null;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InformacionBancaria other = (InformacionBancaria) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
