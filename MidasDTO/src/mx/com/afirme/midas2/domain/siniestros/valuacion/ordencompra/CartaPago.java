package mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;


@Entity(name = "CartaPago")
@Table(name = "TOSNCARTAPAGO", schema = "MIDAS")
public class CartaPago extends MidasAbstracto  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static enum EstatusCartaPago{REGISTRADO,PENDXAUT,AUTORIZADO,ASOCIADA,DEVUELTA,CANCELADA};
	
	@Id
	@SequenceGenerator(name = "TOSNCARTAPAGO_SEQ_GENERADOR",allocationSize = 1, sequenceName = "TOSNCARTAPAGO_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TOSNCARTAPAGO_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id;
	
	
	@Column(name = "ESTATUS")
	private String estatus;
	
	
	@Column(name = "SINIESTRO_TERCERO")
	private String siniestroTercero;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_RECEPCION")
	private Date fechaRecepcion;
	
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_ESTIMACION")
	private EstimacionCoberturaReporteCabina paseDeAtencion;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_ORDEN_COMPRA")
	private OrdenCompra ordenCompra;
	
	

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getSiniestroTercero() {
		return siniestroTercero;
	}

	public void setSiniestroTercero(String siniestroTercero) {
		this.siniestroTercero = siniestroTercero;
	}

	public Date getFechaRecepcion() {
		return fechaRecepcion;
	}

	public void setFechaRecepcion(Date fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}

	public EstimacionCoberturaReporteCabina getPaseDeAtencion() {
		return paseDeAtencion;
	}

	public void setPaseDeAtencion(EstimacionCoberturaReporteCabina paseDeAtencion) {
		this.paseDeAtencion = paseDeAtencion;
	}

	public OrdenCompra getOrdenCompra() {
		return ordenCompra;
	}

	public void setOrdenCompra(OrdenCompra ordenCompra) {
		this.ordenCompra = ordenCompra;
	}
	
	
	
}
