package mx.com.afirme.midas2.domain.siniestros.recuperacion;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 10-jul-2015 07:07:01 p.m.
 */
@Entity(name = "RecuperacionCompania")
@Table(name = "TOSNRECUPERACIONCIA", schema = "MIDAS")
@DiscriminatorValue("CIA")
@PrimaryKeyJoinColumn(name="RECUPERACION_ID", referencedColumnName="ID")
public class RecuperacionCompania extends Recuperacion {
	
	private static final long serialVersionUID = 6697306674987749926L;
	
	@OneToMany(fetch=FetchType.LAZY,  mappedBy = "recuperacion", cascade={CascadeType.ALL}, orphanRemoval=true)
	@JoinFetch(JoinFetchType.OUTER)
	private List<CartaCompania> cartas = new ArrayList<CartaCompania>();
	
	@OneToMany(fetch=FetchType.LAZY,  mappedBy = "recuperacion",  cascade={CascadeType.ALL}, orphanRemoval=true)
	private List<OrdenCompraCartaCia> ordenesCompra = new ArrayList<OrdenCompraCartaCia>();

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANIA_ID")
	private PrestadorServicio compania;		
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_LIMITE_COBRO")	
	private Date fechaLimiteCobro;
	
	@Column(name="IMPORTE")
	private BigDecimal importe;
	
	@Column(name="MONTO_GRUA")
	private BigDecimal montoGAGrua;
	
	@Column(name="MONTO_INDEMNIZACION")
	private BigDecimal montoIndemnizacion;
	
	@Column(name="MONTO_SALVAMENTO")
	private BigDecimal montoSalvamento;
	
	@Column(name="MONTO_PIEZAS")
	private BigDecimal montoPiezas;
	
	@Column(name="PASEATENCION_CIA")
	private String paseAtencionCia;
	
	@Column(name="POLIZA_CIA")
	private String polizaCia;
	
	@Column(name="PORCENTAJE_PARTICIPACION")
	private Integer porcentajeParticipacion;
	
	@Column(name="SINIESTRO_CIA")
	private String siniestroCia;	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_SOL_CANCELACION")
	private Date fechaSolCancelacion;
	
	@Column(name = "USUARIO_SOL_CANCELACION", length = 8)
	private String usuarioSolicitudCancelacion;
	
	@Column(name="REFERENCIA_BANCARIA")
	private String referenciaBancaria;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "OFICINA_REASIGNADA")
	private Oficina oficinaReasignada;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_REASIGNACION")
	private Date fechaReasignacion;
	
	@Column(name="USUARIO_REASIGNACION")
	private String usuarioReasignacion;
	

    @Column(name="ES_COMPLEMENTO")
	private Boolean esComplemento = Boolean.FALSE;
    
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "RECUPERACION_ORIGINAL")
	private RecuperacionCompania recuperacionOriginal;
	
	@Column(name="IMPORTE_INICIAL")
	private BigDecimal importeInicial;
	
	@Transient
	private CartaCompania ultimaCarta;
	
	@Transient
	private Integer diasVencimiento;
	
	@Transient
	private CartaCompania primeraCarta;
	
	public RecuperacionCompania (){
		
	}
	

	@SuppressWarnings("unchecked")
	public RecuperacionCompania (Long numero, Long id,Object cartas,Date fechaCreacion,PrestadorServicio compania,
			ReporteCabina reporteCabina,String referenciaBancaria, String estatus, BigDecimal importe,
			String codigoUsuarioCreacion,Object coberturas, Oficina xOficinaReasignada){
			super.numero = numero;
			super.id = id;
			if(cartas != null){
				if(cartas instanceof List){
					this.cartas = (List<CartaCompania>)cartas;
				}else if( cartas instanceof CartaCompania ) {
					this.cartas = new ArrayList<CartaCompania>();
					this.cartas.add((CartaCompania)cartas);
				}
			}
			if(coberturas != null){
				if(coberturas instanceof List){
					this.setCoberturas( (List<CoberturaRecuperacion>)coberturas);
				}else if (coberturas instanceof CoberturaRecuperacion){
					List<CoberturaRecuperacion> coberturasList = new ArrayList<CoberturaRecuperacion>();
					coberturasList.add((CoberturaRecuperacion)coberturas);
					this.setCoberturas( coberturasList);
				}
			}
			this.fechaCreacion = fechaCreacion;
			this.compania = compania;
			super.setReporteCabina(reporteCabina);
			this.referenciaBancaria = referenciaBancaria;
			this.estatus = estatus;
			this.importe = importe;
			this.codigoUsuarioCreacion =codigoUsuarioCreacion; 
			this.oficinaReasignada = xOficinaReasignada;
	}
	
	public List<CartaCompania> getCartas() {
		return cartas;
	}
	public void setCartas(List<CartaCompania> cartas) {
		this.cartas = cartas;
	}
	public List<OrdenCompraCartaCia> getOrdenesCompra() {
		return ordenesCompra;
	}
	public void setOrdenesCompra(List<OrdenCompraCartaCia> ordenesCompra) {
		this.ordenesCompra = ordenesCompra;
	}
	public PrestadorServicio getCompania() {
		return compania;
	}
	public void setCompania(PrestadorServicio compania) {
		this.compania = compania;
	}
	public Integer getDiasVencimiento() {
		return diasVencimiento;
	}
	public void setDiasVencimiento(Integer diasVencimiento) {
		this.diasVencimiento = diasVencimiento;
	}
	public Date getFechaLimiteCobro() {
		return fechaLimiteCobro;
	}
	public void setFechaLimiteCobro(Date fechaLimiteCobro) {
		this.fechaLimiteCobro = fechaLimiteCobro;
	}
	public BigDecimal getImporte() {
		return importe;
	}
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}
	public BigDecimal getMontoGAGrua() {
		return montoGAGrua;
	}
	public void setMontoGAGrua(BigDecimal montoGAGrua) {
		this.montoGAGrua = montoGAGrua;
	}
	public BigDecimal getMontoIndemnizacion() {
		return montoIndemnizacion;
	}
	public void setMontoIndemnizacion(BigDecimal montoIndemnizacion) {
		this.montoIndemnizacion = montoIndemnizacion;
	}
	public BigDecimal getMontoSalvamento() {
		return montoSalvamento;
	}
	public void setMontoSalvamento(BigDecimal montoSalvamento) {
		this.montoSalvamento = montoSalvamento;
	}
	public String getPaseAtencionCia() {
		return paseAtencionCia;
	}
	public void setPaseAtencionCia(String paseAtencionCia) {
		this.paseAtencionCia = paseAtencionCia;
	}
	public String getPolizaCia() {
		return polizaCia;
	}
	public void setPolizaCia(String polizaCia) {
		this.polizaCia = polizaCia;
	}
	public Integer getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}
	public void setPorcentajeParticipacion(Integer porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}
	public String getSiniestroCia() {
		return siniestroCia;
	}
	public void setSiniestroCia(String siniestroCia) {
		this.siniestroCia = siniestroCia;
	}
	public Date getFechaSolCancelacion() {
		return fechaSolCancelacion;
	}
	public void setFechaSolCancelacion(Date fechaSolCancelacion) {
		this.fechaSolCancelacion = fechaSolCancelacion;
	}
	public String getUsuarioSolicitudCancelacion() {
		return usuarioSolicitudCancelacion;
	}
	public void setUsuarioSolicitudCancelacion(String usuarioSolicitudCancelacion) {
		this.usuarioSolicitudCancelacion = usuarioSolicitudCancelacion;
	}

	public String getReferenciaBancaria() {
		return referenciaBancaria;
	}
	public void setReferenciaBancaria(String referenciaBancaria) {
		this.referenciaBancaria = referenciaBancaria;
	}
	
	public Oficina getOficinaReasignada() {
		return oficinaReasignada;
	}
	public void setOficinaReasignada(Oficina oficinaReasignada) {
		this.oficinaReasignada = oficinaReasignada;
	}
	public Date getFechaReasignacion() {
		return fechaReasignacion;
	}
	public void setFechaReasignacion(Date fechaReasignacion) {
		this.fechaReasignacion = fechaReasignacion;
	}
	public String getUsuarioReasignacion() {
		return usuarioReasignacion;
	}
	public void setUsuarioReasignacion(String usuarioReasignacion) {
		this.usuarioReasignacion = usuarioReasignacion;
	}
	public Boolean getEsComplemento() {
		return esComplemento;
	}
	public void setEsComplemento(Boolean esComplemento) {
		this.esComplemento = esComplemento;
	}
	public RecuperacionCompania getRecuperacionOriginal() {
		return recuperacionOriginal;
	}
	public void setRecuperacionOriginal(RecuperacionCompania recuperacionOriginal) {
		this.recuperacionOriginal = recuperacionOriginal;
	}	
	
	public BigDecimal getImporteInicial() {
		return importeInicial;
	}
	public void setImporteInicial(BigDecimal importeInicial) {
		this.importeInicial = importeInicial;
	}
	public CartaCompania getUltimaCarta(){
		setUltimaCarta();
		return ultimaCarta;
	}
	
	public void setUltimaCarta(){
		CartaCompania carta = null;
		if(cartas.size() > 0){
			carta = cartas.get(0);
			for(CartaCompania c :cartas){
				if(c.getFolio() > carta.getFolio()){
					carta = c;
				}
			}
		}
		this.ultimaCarta = carta;
	}
	
	public CartaCompania getPrimeraCarta() {
		CartaCompania carta = null;
		if(cartas.size() > 0){
			carta = cartas.get(0);
			for(CartaCompania c :cartas){
				if(c.getFolio() < carta.getFolio()){
					carta = c;
				}
			}
		}
		this.primeraCarta = carta;
		return primeraCarta;
	}



	public static enum PaseAtencionCompania{
		ORIGINAL, 
		COPIA 
		
	}
	
	public void agregar(CartaCompania carta){
		carta.setRecuperacion(this);
		this.cartas.add(carta);
	}
	
	public void agregar(OrdenCompraCartaCia ordenCompra){
		ordenCompra.setRecuperacion(this);
		this.ordenesCompra.add(ordenCompra);
	}


	public BigDecimal getMontoPiezas() {
		return montoPiezas;
	}


	public void setMontoPiezas(BigDecimal montoPiezas) {
		this.montoPiezas = montoPiezas;
	}


}