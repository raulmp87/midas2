package mx.com.afirme.midas2.domain.siniestros.valuacion.hgs;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name = "TOHGSENVIOLOG", schema = "MIDAS") 
public class HgsEnvioLog implements Entidad {
	
	public enum TipoLogHGS{
		REPORTE,
		CONSULTA_SUMA_ASEGURADA,
		VALUACION,
		ID_VALUACION,
		NOTI_ORDEN_COMPRA_PAGADA,
		RECIBE_DEVOLUCION_PIEZAS,
		
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTOHGSENVIO_SEQ")
	@SequenceGenerator(name="IDTOHGSENVIO_SEQ", schema="MIDAS", sequenceName="IDTOHGSENVIO_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;
	
	@Column(name="REPORTE_ID")
	private Long reporteId;
	
	@Column(name="ESTIMACION_REPORTE_CABINA_ID")
	private Long estimacionReporteCabinaId;
	
	@Column(name="TIPO")
	private String tipo;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_ENVIO")
	private Date fechaEnvio = new Date();

	@Column(name="RESULTADO")
	private String resultado;
	
	@Column(name="DATOS")
	private String datos;
	
	@Column(name="REPORTE")
	private String reporte;	

	public HgsEnvioLog(Long keyReporteCabina,
			Long keyEstimacionCoberturaReporteCabina,
			String tipo, Date fechaEnvio, String resultado,String datos,String reporte) {
		super();
		this.reporteId 			       = keyReporteCabina;
		this.estimacionReporteCabinaId = keyEstimacionCoberturaReporteCabina;
		this.tipo 					   = tipo;
		this.fechaEnvio 			   = fechaEnvio;
		this.resultado 				   = resultado;
		this.datos					   = datos;
		this.reporte                   = reporte;
	}

	public HgsEnvioLog(){}

	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	public Long getId() {
		return id;
	}

	public Date getFechaEnvio() {
		return fechaEnvio;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HgsEnvioLog other = (HgsEnvioLog) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getDatos() {
		return datos;
	}

	public void setDatos(String datos) {
		this.datos = datos;
	}

	public Long getReporteId() {
		return reporteId;
	}

	public Long getEstimacionReporteCabinaId() {
		return estimacionReporteCabinaId;
	}

	public void setReporteId(Long reporteId) {
		this.reporteId = reporteId;
	}

	public void setEstimacionReporteCabinaId(Long estimacionReporteCabinaId) {
		this.estimacionReporteCabinaId = estimacionReporteCabinaId;
	}

	public String getReporte() {
		return reporte;
	}

	public void setReporte(String reporte) {
		this.reporte = reporte;
	}

}
