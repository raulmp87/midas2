package mx.com.afirme.midas2.domain.siniestros.pagos.notasDeCredito;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;

/**
 * @author Israel
 * @version 1.0
 * @created 31-mar.-2015 12:45:22 p. m.
 */
@Entity
@Table(name = "TOCONJORDENNOTA", schema = "MIDAS")
public class ConjuntoOrdenCompraNota extends MidasAbstracto implements Entidad {

	private static final long serialVersionUID = 303176091086713462L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONJORDNOTA_ID_SEQ")
	@SequenceGenerator(name = "CONJORDNOTA_ID_SEQ",  schema="MIDAS", sequenceName = "CONJORDNOTA_ID_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Long id;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinTable(name = "TRCONJORDENNOTA", schema="MIDAS", 
			joinColumns = {@JoinColumn(name="CONJ_ORDENNOTA_ID", referencedColumnName="ID")},
			inverseJoinColumns = {@JoinColumn(name="DOC_FISCAL_ID", referencedColumnName="ID")}
	)
	private DocumentoFiscal factura;
	
	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "TRCONJNOTACREDITO", schema="MIDAS", 
			joinColumns = {@JoinColumn(name="CONJ_ORDENNOTA_ID", referencedColumnName="ID")},
			inverseJoinColumns = {@JoinColumn(name="DOC_FISCAL_ID", referencedColumnName="ID")}
	)
	private List<DocumentoFiscal> notasDeCredito;
	
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "TRCONJORDENCOMPRA", schema="MIDAS", 
			joinColumns = {@JoinColumn(name="CONJ_ORDENNOTA_ID", referencedColumnName="ID")},
			inverseJoinColumns = {@JoinColumn(name="ORDEN_COMPRA_ID", referencedColumnName="ID")}
	)
	private List<OrdenCompra> ordenesCompra;
	

	public ConjuntoOrdenCompraNota() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<DocumentoFiscal> getNotasDeCredito() {
		return notasDeCredito;
	}

	public void setNotasDeCredito(List<DocumentoFiscal> notasDeCredito) {
		this.notasDeCredito = notasDeCredito;
	}

	public List<OrdenCompra> getOrdenesCompra() {
		return ordenesCompra;
	}

	public void setOrdenesCompra(List<OrdenCompra> ordenesCompra) {
		this.ordenesCompra = ordenesCompra;
	}

	public DocumentoFiscal getFactura() {
		return factura;
	}

	public void setFactura(DocumentoFiscal factura) {
		this.factura = factura;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
}