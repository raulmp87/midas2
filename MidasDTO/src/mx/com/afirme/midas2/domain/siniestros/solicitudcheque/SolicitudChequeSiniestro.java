package mx.com.afirme.midas2.domain.siniestros.solicitudcheque;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.domain.MidasAbstracto;


/**  LIQB DEVING 
 * Entidad que mapea los datos generales de una �rden de compra.
 * @author usuario
 * @version 1.0
 * @created 22-ago-2014 10:48:11 a.m.
 */
@Entity(name = "SolicitudChequeSiniestro")
@Table(name = "TOSOLICITUDCHEQUE", schema = "MIDAS")
public class SolicitudChequeSiniestro extends MidasAbstracto  {
	
	public static enum OrigenSolicitud{
		LIQUIDACION_SINIESTRO_PROVEEDOR("LIQP"),
		LIQUIDACION_SINIESTRO_BENEFICIARIO("LIQB"),
		DEVOLUCION_INGRESO("DEVING"),
		LIQUIDACION_COMPENSACIONES("LIQCA");
		private String value;
		private OrigenSolicitud(String value){
			this.value=value;
		}
		public String getValue(){
			return value;
		}
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static enum EstatusSolicitudCheque{
		DISPONIBLE_PARA_MIZAR("P"),
		POR_AUTORIZAR_IMPRIMIR_MIZAR("S"),
		IMPRESA_MIZAR("T"),	
		CANCELADA_MIZAR("C");
		
		private String value;
		
		private EstatusSolicitudCheque(String value){
			this.value=value;
		}
		
		public String getValue(){
			return value;
		}
	}

	/**
	 * Id de la orden de compra
	 */
	@Id
	@SequenceGenerator(name = "TOSOLICITUDCHEQUE_SEQ_GENERADOR",allocationSize = 1, sequenceName = "TOSOLICITUDCHEQUE_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TOSOLICITUDCHEQUE_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id;
	
	
	
	@OneToMany(fetch=FetchType.LAZY,  mappedBy = "solicitudChequeSiniestro", cascade = CascadeType.ALL)
    private List<SolicitudChequeSiniestroDetalle> SolicitudChequeSiniestroDetalle = new ArrayList<SolicitudChequeSiniestroDetalle>();
	
	@Column(name="IDSOLCHEQUE")
	private Long idSolCheque;
	
	@Column(name="IDENTIFICADOR")
	private Long identificador;
	
	@Column(name="SITUACION")
	private String situacion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHASOLICITUD")
	protected Date   fechaSolicitud;
	
	
	@Column(name="IMPORTECHEQUE")
	private BigDecimal importeCheque;
	
	
	@Column(name="CLAVEUSUARIO")
	private String cveUsuario;
	

	@Column(name="IDMONEDA")
	private Long idMoneda;
	
	@Column(name="BENEFICIARIO")
	private String beneficiario;
	
	@Column(name="CHEQUE")
	private String cheque;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHAPAGO")
	private Date fechaPago;
	
	@Column(name="CONCEPTO")
	private String concepto;
	
	@Column(name="COMENTARIO")
	private String comentario;	
	
	@Column(name="ORIGEN_SOLICITUD")
	private String origenSolicitud;
	
	@Column(name="BANCO")
	private String banco;
	
	@Column(name="CUENTA")
	private String cuenta;

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	

	public List<SolicitudChequeSiniestroDetalle> getSolicitudChequeSiniestroDetalle() {
		return SolicitudChequeSiniestroDetalle;
	}

	public void setSolicitudChequeSiniestroDetalle(
			List<SolicitudChequeSiniestroDetalle> solicitudChequeSiniestroDetalle) {
		SolicitudChequeSiniestroDetalle = solicitudChequeSiniestroDetalle;
	}

	public Long getIdSolCheque() {
		return idSolCheque;
	}

	public void setIdSolCheque(Long idSolCheque) {
		this.idSolCheque = idSolCheque;
	}

	public String getSituacion() {
		return situacion;
	}

	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}

	public String getCveUsuario() {
		return cveUsuario;
	}

	public void setCveUsuario(String cveUsuario) {
		this.cveUsuario = cveUsuario;
	}

	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}

	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	public Long getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}

	public String getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	public String getCheque() {
		return cheque;
	}

	public void setCheque(String cheque) {
		this.cheque = cheque;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public BigDecimal getImporteCheque() {
		return importeCheque;
	}

	public void setImporteCheque(BigDecimal importeCheque) {
		this.importeCheque = importeCheque;
	}

	/**
	 * @return the fechaPago
	 */
	public Date getFechaPago() {
		return fechaPago;
	}

	/**
	 * @param fechaPago the fechaPago to set
	 */
	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	public Long getIdentificador() {
		return identificador;
	}

	public void setIdentificador(Long identificador) {
		this.identificador = identificador;
	}

	

	public String getOrigenSolicitud() {
		return origenSolicitud;
	}

	public void setOrigenSolicitud(String origenSolicitud) {
		this.origenSolicitud = origenSolicitud;
	}

	/**
	 * @return the cuenta
	 */
	public String getCuenta() {
		return cuenta;
	}

	/**
	 * @param cuenta the cuenta to set
	 */
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	/**
	 * @return the banco
	 */
	public String getBanco() {
		return banco;
	}

	/**
	 * @param banco the banco to set
	 */
	public void setBanco(String banco) {
		this.banco = banco;
	}

}