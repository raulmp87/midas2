/**
 * 
 */
package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import java.math.BigDecimal;

/**
 * @author simavera
 *
 */
@Entity(name = "ReporteRoboSiniestro")
@Table(name = "TOREPORTEROBO", schema = "MIDAS")
public class ReporteRoboSiniestro   extends MidasAbstracto{

	private static final long serialVersionUID = -4788704540827799456L;

	@Id
	@SequenceGenerator(name = "TOREPORTEROBO_SEQ_GENERADOR",allocationSize = 1, sequenceName = "TOREPORTEROBO_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TOREPORTEROBO_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_ROBO")
	private Date fechaRobo;
	
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CAPTURAOCRA" )
	private Date fechaCapturaOcra;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_RECUPERACION" )
	private Date fechaRecuperacion;	
	
	public Date getFechaRecuperacion() {
		return fechaRecuperacion;
	}
	public void setFechaRecuperacion(Date fechaRecuperacion) {
		this.fechaRecuperacion = fechaRecuperacion;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_AVERIGUACION" )
	private Date fechaAveriguacion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_LOCALIZACION" )
	private Date fechaLocalizacion;
	
	@Column(name="TIPOROBO", length = 30)
	private String tipoRobo;
	
	@Column(name="OBSERVACIONES", length = 30)
	private String observaciones;	
	
	
	@Column(name="ESTATUS_VEHICULO", length = 30)
	private String estatusVehiculo;
	
	@Column(name="NUMEROACTA", length = 30)
	private String numeroActa;	
	
	@Column(name="NUMEROAVERIGUACIONLOCAL", length = 30)
	private String numeroAveriguacionLocal;
	
	@Column(name="NUMEROAVERIGUACIONDEN", length = 30)
	private String numeroAveriguacionDen;
	
	@Column(name="PROVEEDOR", length = 30)
	private String proveedor;
	
	@Column(name="LOCALIZADOR", length = 30)
	private String localizador;
	
	@Column(name="UBICACIONVEHICULO", length = 50)
	private String ubicacionVehiculo;
	
	@Column(name="RECUPERADOR", length = 30)
	private String recuperador;
	
	@Column(name="CAUSAMOVIMIENTO", length = 30)
	private String causaMovimiento;
	
	@Column(name="VALORCOMERCIAL_MOMENTO")
	private BigDecimal valorComercialMomento;
	
	@Column(name="PRESUPUESTODANOS")
	private BigDecimal presupuestoDanos;
	
	@Column(name="FALTANTES", length = 30)	
	private String faltantes ;
	
	@Column(name="MONTOPROVISION")
	private BigDecimal montoProvicion;
	
	@Column(name="IMPORTEPAGADO")
	private BigDecimal importePagado;
	
	
	@Column(name="COBERTURAREPORTECABINA_ID")
	private Long coberturaReporteCabinaId;
	
	
	@Column(name="NOMBREAGENTE_MINISTERIOPUBLICO")
	private String nombreAgenteMinisterioPublico;

	public static enum EstatusVehiculo{
		LOCALIZADO("EST_VEHI_1"), 
		LOCQALIZADO_INCOSTEABLE("EST_VEHI_2"), 
		NO_LOCALIZADO("EST_VEHI_3"), 
		RECUPERADO_ASEGURADO_REPARADO("EST_VEHI_7"), 
		RECUPERADO_ASEGURADO_SINDANIOS("EST_VEHI_5"), 
		RECUPERADO_ASEGURADO_PERDIDATOTAL("EST_VEHI_6"),
		RECUPERADO_PARA_AFIRME("EST_VEHI_4");
		
		private String codigo;
		
		EstatusVehiculo(String codigo){
			this.codigo = codigo;
		}
		
		@Override
		public String toString(){
		   return codigo;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getFechaRobo() {
		return fechaRobo;
	}
	public void setFechaRobo(Date fechaRobo) {
		this.fechaRobo = fechaRobo;
	}
	public Date getFechaCapturaOcra() {
		return fechaCapturaOcra;
	}
	public void setFechaCapturaOcra(Date fechaCapturaOcra) {
		this.fechaCapturaOcra = fechaCapturaOcra;
	}
	public Date getFechaAveriguacion() {
		return fechaAveriguacion;
	}
	public void setFechaAveriguacion(Date fechaAveriguacion) {
		this.fechaAveriguacion = fechaAveriguacion;
	}
	public Date getFechaLocalizacion() {
		return fechaLocalizacion;
	}
	public void setFechaLocalizacion(Date fechaLocalizacion) {
		this.fechaLocalizacion = fechaLocalizacion;
	}
	public String getTipoRobo() {
		return tipoRobo;
	}
	public void setTipoRobo(String tipoRobo) {
		this.tipoRobo = tipoRobo;
	}
	public String getEstatusVehiculo() {
		return estatusVehiculo;
	}
	public void setEstatusVehiculo(String estatusVehiculo) {
		this.estatusVehiculo = estatusVehiculo;
	}
	public String getNumeroActa() {
		return numeroActa;
	}
	public void setNumeroActa(String numeroActa) {
		this.numeroActa = numeroActa;
	}
	public String getNumeroAveriguacionLocal() {
		return numeroAveriguacionLocal;
	}
	public void setNumeroAveriguacionLocal(String numeroAveriguacionLocal) {
		this.numeroAveriguacionLocal = numeroAveriguacionLocal;
	}

	public String getLocalizador() {
		return localizador;
	}

	public void setLocalizador(String localizador) {
		this.localizador = localizador;
	}
	public String getUbicacionVehiculo() {
		return ubicacionVehiculo;
	}
	public void setUbicacionVehiculo(String ubicacionVehiculo) {
		this.ubicacionVehiculo = ubicacionVehiculo;
	}
	public String getRecuperador() {
		return recuperador;
	}

	public void setRecuperador(String recuperador) {
		this.recuperador = recuperador;
	}
	public String getCausaMovimiento() {
		return causaMovimiento;
	}
	public void setCausaMovimiento(String causaMovimiento) {
		this.causaMovimiento = causaMovimiento;
	}
	
	
	
	public String getFaltantes() {
		return faltantes;
	}
	public void setFaltantes(String faltantes) {
		this.faltantes = faltantes;
	}
	

	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public BigDecimal getValorComercialMomento() {
		return valorComercialMomento;
	}
	public void setValorComercialMomento(BigDecimal valorComercialMomento) {
		this.valorComercialMomento = valorComercialMomento;
	}
	public BigDecimal getPresupuestoDanos() {
		return presupuestoDanos;
	}
	public void setPresupuestoDanos(BigDecimal presupuestoDanos) {
		this.presupuestoDanos = presupuestoDanos;
	}
	
	public BigDecimal getMontoProvicion() {
		return montoProvicion;
	}
	public void setMontoProvicion(BigDecimal montoProvicion) {
		this.montoProvicion = montoProvicion;
	}
	public BigDecimal getImportePagado() {
		return importePagado;
	}
	public void setImportePagado(BigDecimal importePagado) {
		this.importePagado = importePagado;
	}

	public String getNumeroAveriguacionDen() {
		return numeroAveriguacionDen;
	}
	public void setNumeroAveriguacionDen(String numeroAveriguacionDen) {
		this.numeroAveriguacionDen = numeroAveriguacionDen;
	}
	public String getProveedor() {
		return proveedor;
	}
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}
	public Long getCoberturaReporteCabinaId() {
		return coberturaReporteCabinaId;
	}
	public void setCoberturaReporteCabinaId(Long coberturaReporteCabinaId) {
		this.coberturaReporteCabinaId = coberturaReporteCabinaId;
	}
	public String getNombreAgenteMinisterioPublico() {
		return nombreAgenteMinisterioPublico;
	}
	public void setNombreAgenteMinisterioPublico(
			String nombreAgenteMinisterioPublico) {
		this.nombreAgenteMinisterioPublico = nombreAgenteMinisterioPublico;
	}
	
	
}
