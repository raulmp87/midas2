package mx.com.afirme.midas2.domain.siniestros.solicitudcheque;
import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;


/**
 * entidad que mapea el detalle de una �rden de compra, cada objeto de esta tabla
 * representa un concepto de la �rden de compra.
 * @author usuario
 * @version 1.0
 * @created 22-ago-2014 10:48:42 a.m.
 */



@Entity(name = "SolicitudChequeSiniestroDetalle")
@Table(name = "TOSOLICITUDCHEQUEDETALLE", schema = "MIDAS")
public class SolicitudChequeSiniestroDetalle  extends MidasAbstracto  {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TOSOLICITUDCHEQUEDETALLE_SEQ_GENERADOR",allocationSize = 1, sequenceName = "TOSOLICITUDCHEQUEDETALLE_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TOSOLICITUDCHEQUEDETALLE_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDSOLICITUDCHEQUE", referencedColumnName = "ID")	
	private SolicitudChequeSiniestro solicitudChequeSiniestro;
	
	@Column(name="CVEL_T_RUBRO")	
	private String rubro;
	
	@Column(name="NATURALEZA")	
	private String naturaleza; /*Cargo o Abono*/
	
	@Column(name="CUENTA_CONTABLE")	
	private String cuentaContable;
	/*Unidad Especializada de negocio*/
	/*@Column(name="UEN")	
	private String uen;*/
	
	@Column(name="AUXILIAR")	
	private String auxiliar;
	

	
	@Column(name="IMPORTE")	
	private BigDecimal importe;


	@Column(name="TIPOOPERACION")	
	private Integer tipoOperacion;
	
	@Column(name="PCTIVA")	
	private Integer pctIva;
	
	@Column(name="PCTISR")	
	private Integer pctIsr;
	
	@Column(name="PCTIVAR")	
	private Integer pctIvaR;

	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}





	public SolicitudChequeSiniestro getSolicitudChequeSiniestro() {
		return solicitudChequeSiniestro;
	}



	public void setSolicitudChequeSiniestro(
			SolicitudChequeSiniestro solicitudChequeSiniestro) {
		this.solicitudChequeSiniestro = solicitudChequeSiniestro;
	}



	public String getRubro() {
		return rubro;
	}



	public void setRubro(String rubro) {
		this.rubro = rubro;
	}



	public String getNaturaleza() {
		return naturaleza;
	}



	public void setNaturaleza(String naturaleza) {
		this.naturaleza = naturaleza;
	}



	public String getCuentaContable() {
		return cuentaContable;
	}



	public void setCuentaContable(String cuentaContable) {
		this.cuentaContable = cuentaContable;
	}



	/*public String getUen() {
		return uen;
	}



	public void setUen(String uen) {
		this.uen = uen;
	}*/



	public String getAuxiliar() {
		return auxiliar;
	}



	public void setAuxiliar(String auxiliar) {
		this.auxiliar = auxiliar;
	}



	/*public String getCentroCostos() {
		return CentroCostos;
	}



	public void setCentroCostos(String centroCostos) {
		CentroCostos = centroCostos;
	}*/



	public BigDecimal getImporte() {
		return importe;
	}



	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}



	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}



	public Integer getTipoOperacion() {
		return tipoOperacion;
	}



	public void setTipoOperacion(Integer tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}



	public Integer getPctIva() {
		return pctIva;
	}



	public void setPctIva(Integer pctIva) {
		this.pctIva = pctIva;
	}



	public Integer getPctIsr() {
		return pctIsr;
	}



	public void setPctIsr(Integer pctIsr) {
		this.pctIsr = pctIsr;
	}



	public Integer getPctIvaR() {
		return pctIvaR;
	}



	public void setPctIvaR(Integer pctIvaR) {
		this.pctIvaR = pctIvaR;
	}



	



	
	
	
	
	
	

}