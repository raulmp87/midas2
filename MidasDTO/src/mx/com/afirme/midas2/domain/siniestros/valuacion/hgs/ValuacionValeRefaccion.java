package mx.com.afirme.midas2.domain.siniestros.valuacion.hgs;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table ( name="TOVALUACIONVALEREFACCION" , schema = "MIDAS" ) 

public class ValuacionValeRefaccion implements Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTOVALUACIONREFACCION_SEQ")
	@SequenceGenerator(name="IDTOVALUACIONREFACCION_SEQ", schema="MIDAS", sequenceName="IDTOVALUACIONREFACCION_SEQ",allocationSize=1)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "MONTO")
	private Double monto;
	
	@Column(name = "PORCENTAJE_IVA")
	private Double  porcentajeIva;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_ACTUALIZACION")
	private Date   fechaActualizacion;
	
	@Column(name = "REFACCION")
	private Boolean refaccion;
	
	@Column(name = "MONTO_IVA")
	private Double montoIva;
	
	@Column(name = "FOLIO")
	private String folio;
	
	@Column(name = "ESTATUS_VALE")
	private String estatusDetalle;
	
	@Column(name = "VALE_ID")
	private Long   valeId;
	
	@Column(name = "CLAVE_PROVEEDOR")
	private Long   claveProveedor;
	
	@Column(name = "NOMBRE_PROVEEDOR")
	private String nombreProveedor;
	
	@Column(name = "CONCEPTO")
	private String concepto;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_EXPEDICION")
	private Date FechaExpedicion;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "VALUACION_DETALLE_ID", referencedColumnName = "ID", nullable = false, updatable = false, insertable = true)
	ValuacionHgsDetalle valuacionHgsDetalle = new ValuacionHgsDetalle();
	
	
	
	
	public Long getId() {
		return id;
	}
	public Double getMonto() {
		return monto;
	}
	public Double getPorcentajeIva() {
		return porcentajeIva;
	}
	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}
	public Double getMontoIva() {
		return montoIva;
	}
	public String getFolio() {
		return folio;
	}
	public String getEstatusDetalle() {
		return estatusDetalle;
	}
	public Long getValeId() {
		return valeId;
	}
	public Long getClaveProveedor() {
		return claveProveedor;
	}
	public String getNombreProveedor() {
		return nombreProveedor;
	}
	public String getConcepto() {
		return concepto;
	}
	public Date getFechaExpedicion() {
		return FechaExpedicion;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setMonto(Double monto) {
		this.monto = monto;
	}
	public void setPorcentajeIva(Double porcentajeIva) {
		this.porcentajeIva = porcentajeIva;
	}
	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}
	public void setMontoIva(Double montoIva) {
		this.montoIva = montoIva;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public void setEstatusDetalle(String estatusDetalle) {
		this.estatusDetalle = estatusDetalle;
	}
	public void setValeId(Long valeId) {
		this.valeId = valeId;
	}
	public void setClaveProveedor(Long claveProveedor) {
		this.claveProveedor = claveProveedor;
	}
	public void setNombreProveedor(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public void setFechaExpedicion(Date fechaExpedicion) {
		FechaExpedicion = fechaExpedicion;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValuacionValeRefaccion other = (ValuacionValeRefaccion) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	public Boolean getRefaccion() {
		return refaccion;
	}
	public ValuacionHgsDetalle getValuacionHgsDetalle() {
		return valuacionHgsDetalle;
	}
	public void setRefaccion(Boolean refaccion) {
		this.refaccion = refaccion;
	}
	public void setValuacionHgsDetalle(ValuacionHgsDetalle valuacionHgsDetalle) {
		this.valuacionHgsDetalle = valuacionHgsDetalle;
	}
	@Override
	public Long getKey() {
		return this.getId();
	}
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String toString() {
		return "ValuacionValeRefaccion [id=" + id + ", monto=" + monto
				+ ", porcentajeIva=" + porcentajeIva + ", fechaActualizacion="
				+ fechaActualizacion + ", refaccion=" + refaccion
				+ ", montoIva=" + montoIva + ", folio=" + folio
				+ ", estatusDetalle=" + estatusDetalle + ", valeId=" + valeId
				+ ", claveProveedor=" + claveProveedor + ", nombreProveedor="
				+ nombreProveedor + ", concepto=" + concepto
				+ ", FechaExpedicion=" + FechaExpedicion + "]";
	}
	
	
	
}
