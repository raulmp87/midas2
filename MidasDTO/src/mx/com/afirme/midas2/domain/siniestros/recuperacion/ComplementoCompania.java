package mx.com.afirme.midas2.domain.siniestros.recuperacion;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.domain.MidasAbstracto;

/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 26-ago-2015 10:48:46 a.m.
 */
@Entity(name = "ComplementoCompania")
@Table(name = "TOSNCOMPLEMENTOCIA", schema = "MIDAS")
public class ComplementoCompania extends MidasAbstracto{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -4318690644543168862L;


	@Id
	@SequenceGenerator(name = "TOSNCOMPLEMENTOCIA_SEQ_GENERADOR",allocationSize = 1, sequenceName = "TOSNCOMPLEMENTOCIA_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TOSNCOMPLEMENTOCIA_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id ; 
	
	
	@Column(name = "ACTIVO")	
	private Boolean activo;
	
	
	@Column(name = "MONTO")	
	private BigDecimal monto;
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "RECUPERACION_ID", referencedColumnName = "RECUPERACION_ID")
	private RecuperacionCompania recuperacion;
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public BigDecimal getMonto() {
		return monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	public RecuperacionCompania getRecuperacion() {
		return recuperacion;
	}

	public void setRecuperacion(RecuperacionCompania recuperacion) {
		this.recuperacion = recuperacion;
	}
	
	
	



}