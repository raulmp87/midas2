package mx.com.afirme.midas2.domain.siniestros.valuacion.hgs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.IncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

import java.math.BigDecimal;

@Entity
@Table ( name = "TOVALUACIONHGS" , schema = "MIDAS" ) 
public class ValuacionHgs extends MidasAbstracto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOVALUACIONHGS_SEQ")
	@SequenceGenerator(name="TOVALUACIONHGS_SEQ", schema="MIDAS", sequenceName="TOVALUACIONHGS_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;
	
	@OneToOne (fetch=FetchType.LAZY )
	@JoinColumn(name="REPORTE_ID", referencedColumnName="ID" , updatable = false)
	private ReporteCabina  reporteCabina;
	
	@Column(name = "LINEANEGOCIO_ID")
	private Long  lineaNegocioId;
	
	@OneToOne (fetch=FetchType.LAZY )
	@JoinColumn(name="INCISO_ID", referencedColumnName="ID" , updatable = false)
	private IncisoReporteCabina incisoReporteCabina;

	@Column(name = "COBERTURA_ID")
	private Long  coberturaId;

	@Column(name = "PASEATENCION_ID")
	private Long  paseatencionId;
	
	@Column(name = "FOLIO")
	private String  folio;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_PROCESO")
	private Date  fechaProceso;
	
	@Column(name = "VALUADOR_ID")
	private String  valuadorId;
	
	@Column(name = "VALUADOR_NOMBRE")
	private String  valuadorNombre;
	
	@Column(name = "TIPO_MOVIMIENTO")
	private String  tipoMovimiento;
	
	@Column(name = "CLAVE_CAUSA_MOV")
	private Short  claveCausaMov;
	
	@Column(name = "CLAVE_TIPO_PROCESO")
	private Short  claveTipoProceso;
	
	@Column(name = "CLAVE_ESTATUS_VAL")
	private Short  claveEstatusVal;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_INGRESO_TALLER")
	private Date  fechaIngresoTaller;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_VALUACION")
	private Date  fechaValuacion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_REINGRESO_TALLER")
	private Date  fechaReingresoTaller;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_ULTIMO_SURTIDO")
	private Date  fechaUltimoSurtido;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_TERMINACION")
	private Date  fechaTerminacion;
	
	@Column(name = "IMP_REFACCIONES")
	private BigDecimal  imRefacciones;
	
	@Column(name = "IMP_MANO_OBRA")
	private BigDecimal  impNanoObra;
	
	@Column(name = "IMP_TOTAL_VAL")
	private BigDecimal  impTotalVal;
	
	@Column(name= "ORDEN_COMPRA_ID")
	private Long  ordenCompraId;
	
	@Column(name= "VALUACION_HGS")
	private Long  valuacionHgs;
	
	@OneToMany(fetch=FetchType.LAZY,  mappedBy = "valuacionHgs", cascade = CascadeType.ALL)
	@JoinFetch(JoinFetchType.OUTER)
	List<ValuacionValeRefaccionFinal> valuacionRefaccionFinal = new ArrayList<ValuacionValeRefaccionFinal>();
	
	
	@Column(name = "IMP_SUBTOTAL_VALUACION")
	private BigDecimal  impSubTotalValuacion;
	
	@Column(name = "IMP_IVA_VALUACION")
	private BigDecimal  impIvaValuacion;

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Long getPaseatencionId() {
		return paseatencionId;
	}

	public String getFolio() {
		return folio;
	}

	public Date getFechaProceso() {
		return fechaProceso;
	}

	public String getValuadorId() {
		return valuadorId;
	}

	public String getValuadorNombre() {
		return valuadorNombre;
	}

	public String getTipoMovimiento() {
		return tipoMovimiento;
	}

	public Short getClaveCausaMov() {
		return claveCausaMov;
	}

	public Short getClaveTipoProceso() {
		return claveTipoProceso;
	}

	public Short getClaveEstatusVal() {
		return claveEstatusVal;
	}

	public Date getFechaIngresoTaller() {
		return fechaIngresoTaller;
	}

	public Date getFechaValuacion() {
		return fechaValuacion;
	}

	public Date getFechaReingresoTaller() {
		return fechaReingresoTaller;
	}

	public Date getFechaUltimoSurtido() {
		return fechaUltimoSurtido;
	}

	public Date getFechaTerminacion() {
		return fechaTerminacion;
	}
	
	public void setPaseatencionId(Long paseatencionId) {
		this.paseatencionId = paseatencionId;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	public void setValuadorId(String valuadorId) {
		this.valuadorId = valuadorId;
	}

	public void setValuadorNombre(String valuadorNombre) {
		this.valuadorNombre = valuadorNombre;
	}

	public void setTipoMovimiento(String tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

	public void setClaveCausaMov(Short claveCausaMov) {
		this.claveCausaMov = claveCausaMov;
	}

	public void setClaveTipoProceso(Short claveTipoProceso) {
		this.claveTipoProceso = claveTipoProceso;
	}

	public void setClaveEstatusVal(Short claveEstatusVal) {
		this.claveEstatusVal = claveEstatusVal;
	}

	public void setFechaIngresoTaller(Date fechaIngresoTaller) {
		this.fechaIngresoTaller = fechaIngresoTaller;
	}

	public void setFechaValuacion(Date fechaValuacion) {
		this.fechaValuacion = fechaValuacion;
	}

	public void setFechaReingresoTaller(Date fechaReingresoTaller) {
		this.fechaReingresoTaller = fechaReingresoTaller;
	}

	public void setFechaUltimoSurtido(Date fechaUltimoSurtido) {
		this.fechaUltimoSurtido = fechaUltimoSurtido;
	}

	public void setFechaTerminacion(Date fechaTerminacion) {
		this.fechaTerminacion = fechaTerminacion;
	}

	public IncisoReporteCabina getIncisoReporteCabina() {
		return incisoReporteCabina;
	}

	public BigDecimal getImRefacciones() {
		return imRefacciones;
	}

	public BigDecimal getImpNanoObra() {
		return impNanoObra;
	}

	public BigDecimal getImpTotalVal() {
		return impTotalVal;
	}

	public void setIncisoReporteCabina(IncisoReporteCabina incisoReporteCabina) {
		this.incisoReporteCabina = incisoReporteCabina;
	}

	public void setImRefacciones(BigDecimal imRefacciones) {
		this.imRefacciones = imRefacciones;
	}

	public void setImpNanoObra(BigDecimal impNanoObra) {
		this.impNanoObra = impNanoObra;
	}

	public void setImpTotalVal(BigDecimal impTotalVal) {
		this.impTotalVal = impTotalVal;
	}

	public ReporteCabina getReporteCabina() {
		return reporteCabina;
	}

	public void setReporteCabina(ReporteCabina reporteCabina) {
		this.reporteCabina = reporteCabina;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<ValuacionValeRefaccionFinal> getValuacionRefaccionFinal() {
		return valuacionRefaccionFinal;
	}

	public void setValuacionRefaccionFinal(
			List<ValuacionValeRefaccionFinal> valuacionRefaccionFinal) {
		this.valuacionRefaccionFinal = valuacionRefaccionFinal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValuacionHgs other = (ValuacionHgs) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	

	public Long getLineaNegocioId() {
		return lineaNegocioId;
	}

	public void setLineaNegocioId(Long lineaNegocioId) {
		this.lineaNegocioId = lineaNegocioId;
	}

	public Long getOrdenCompraId() {
		return ordenCompraId;
	}

	public void setOrdenCompraId(Long ordenCompraId) {
		this.ordenCompraId = ordenCompraId;
	}
	
	public Long getCoberturaId() {
		return coberturaId;
	}

	public void setCoberturaId(Long coberturaId) {
		this.coberturaId = coberturaId;
	}

	@Override
	public String toString() {
		return "ValuacionHgs [id=" + id + ", reporteCabina=" + reporteCabina
				+ ", lineaNegocioId=" + lineaNegocioId
				+ ", incisoReporteCabina=" + incisoReporteCabina
				+ ", coberturaId=" + coberturaId + ", paseatencionId="
				+ paseatencionId + ", folio=" + folio + ", fechaProceso="
				+ fechaProceso + ", valuadorId=" + valuadorId
				+ ", valuadorNombre=" + valuadorNombre + ", tipoMovimiento="
				+ tipoMovimiento + ", claveCausaMov=" + claveCausaMov
				+ ", claveTipoProceso=" + claveTipoProceso
				+ ", claveEstatusVal=" + claveEstatusVal
				+ ", fechaIngresoTaller=" + fechaIngresoTaller
				+ ", fechaValuacion=" + fechaValuacion
				+ ", fechaReingresoTaller=" + fechaReingresoTaller
				+ ", fechaUltimoSurtido=" + fechaUltimoSurtido
				+ ", fechaTerminacion=" + fechaTerminacion + ", imRefacciones="
				+ imRefacciones + ", impNanoObra=" + impNanoObra
				+ ", impTotalVal=" + impTotalVal + ", ordenCompraId="
				+ ordenCompraId + ", valuacionHgs=" + valuacionHgs
				+ ", valuacionRefaccionFinal=" + valuacionRefaccionFinal
				+ ", impSubTotalValuacion=" + impSubTotalValuacion
				+ ", impIvaValuacion=" + impIvaValuacion + "]";
	}

	public Long getValuacionHgs() {
		return valuacionHgs;
	}

	public void setValuacionHgs(Long valuacionHgs) {
		this.valuacionHgs = valuacionHgs;
	}

	public BigDecimal getImpSubTotalValuacion() {
		return impSubTotalValuacion;
	}

	public void setImpSubTotalValuacion(BigDecimal impSubTotalValuacion) {
		this.impSubTotalValuacion = impSubTotalValuacion;
	}

	public BigDecimal getImpIvaValuacion() {
		return impIvaValuacion;
	}

	public void setImpIvaValuacion(BigDecimal impIvaValuacion) {
		this.impIvaValuacion = impIvaValuacion;
	}



}
