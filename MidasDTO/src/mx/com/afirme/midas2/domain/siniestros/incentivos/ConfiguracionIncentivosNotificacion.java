package mx.com.afirme.midas2.domain.siniestros.incentivos;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.domain.MidasAbstracto;



@Entity
@Table(name = "TOSNCONFINCENTIVO_NOTIFICACION", schema = "MIDAS")
public class ConfiguracionIncentivosNotificacion extends MidasAbstracto {
	
	private static final long serialVersionUID = 1L;
	@Id 
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOSNCONFINCENTIVONOTIFICA_SEQ")
	@SequenceGenerator(name="TOSNCONFINCENTIVONOTIFICA_SEQ", schema="MIDAS", sequenceName="TOSNCONFINCENTIVONOTIFICA_SEQ", allocationSize=1)	
    @Column(name="ID", unique=true, nullable=false, precision=8)
	private Long id;
	
	
	@Column(name="USUARIO_REMITENTE")
	private String usuarioRemitente;
	
	@Column(name="DESTINATARIOS")
	private String destinatrarios;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_CONFIGURACION", referencedColumnName = "ID")
	private  ConfiguracionIncentivos configuracionIncentivo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsuarioRemitente() {
		return usuarioRemitente;
	}

	public void setUsuarioRemitente(String usuarioRemitente) {
		this.usuarioRemitente = usuarioRemitente;
	}

	public String getDestinatrarios() {
		return destinatrarios;
	}

	public void setDestinatrarios(String destinatrarios) {
		this.destinatrarios = destinatrarios;
	}

	public ConfiguracionIncentivos getConfiguracionIncentivo() {
		return configuracionIncentivo;
	}

	public void setConfiguracionIncentivo(
			ConfiguracionIncentivos configuracionIncentivo) {
		this.configuracionIncentivo = configuracionIncentivo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	
	
	

}
