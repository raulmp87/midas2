/**
 * 
 */
package mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.permisos;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.catalogo.conceptos.ConceptoAjuste;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;

/**
 * @author admin
 *
 */
@Entity
@Table(name = "TCORDENCOMPRAPERMISO", schema = "MIDAS")
@Access(AccessType.FIELD)
public class OrdenCompraPermiso extends MidasAbstracto  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_TCORDENCOMPRAPERMISO_SEQ")
	@SequenceGenerator(name = "ID_TCORDENCOMPRAPERMISO_SEQ",  schema="MIDAS", sequenceName = "ID_TCORDENCOMPRAPERMISO_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Long id;
	
	
	/**
	 * 1 =Activo , 0= Inactivo
	 */
	@Column(name="ESTATUS")
	private Integer estatus = 1;
	
	@Column(name="MONTO_MAX")
	private BigDecimal montoMax;
	
	@Column(name="MONTO_MIN")
	private BigDecimal montoMin;
	
	@Column(name="NIVEL")
	private Integer nivel;
	
	/**
	 * tipo de permiso :
	 * Afectacion Reserva/Orden Compra = OC
	 * Gasto de Ajuste = GA
	 */
	@Column(name="TIPO")
	private String tipo;
	
	@Column(name="APLICA_MONTOS")
	private boolean validarMontos;
	
	@OneToOne(fetch=FetchType.LAZY,targetEntity=CoberturaSeccionDTO.class)
	@JoinColumns( { 
        @JoinColumn(name="ID_SECCION", referencedColumnName="idtoseccion"), 
        @JoinColumn(name="ID_COBERTURA", referencedColumnName="idtocobertura") } )
	private CoberturaSeccionDTO coberturaSeccion;
	

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_CONCEPTO", referencedColumnName = "ID")
	private ConceptoAjuste concepto;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_OFICINA", referencedColumnName = "ID")
	private Oficina oficina;
	
	@OneToMany(fetch=FetchType.LAZY,  mappedBy = "permiso")
    private List<OrdenCompraPermisoUsuario> permisoUsuarios;
	
	
	

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the estatus
	 */
	public Integer getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return the montoMax
	 */
	public BigDecimal getMontoMax() {
		return montoMax;
	}

	/**
	 * @param montoMax the montoMax to set
	 */
	public void setMontoMax(BigDecimal montoMax) {
		this.montoMax = montoMax;
	}

	/**
	 * @return the montoMin
	 */
	public BigDecimal getMontoMin() {
		return montoMin;
	}

	/**
	 * @param montoMin the montoMin to set
	 */
	public void setMontoMin(BigDecimal montoMin) {
		this.montoMin = montoMin;
	}

	/**
	 * @return the nivel
	 */
	public Integer getNivel() {
		return nivel;
	}

	/**
	 * @param nivel the nivel to set
	 */
	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}


	/**
	 * @return the concepto
	 */
	public ConceptoAjuste getConcepto() {
		return concepto;
	}

	/**
	 * @param concepto the concepto to set
	 */
	public void setConcepto(ConceptoAjuste concepto) {
		this.concepto = concepto;
	}

	/**
	 * @return the oficina
	 */
	public Oficina getOficina() {
		return oficina;
	}

	/**
	 * @param oficina the oficina to set
	 */
	public void setOficina(Oficina oficina) {
		this.oficina = oficina;
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the validarMontos
	 */
	public boolean isValidarMontos() {
		return validarMontos;
	}

	/**
	 * @param validarMontos the validarMontos to set
	 */
	public void setValidarMontos(boolean validarMontos) {
		this.validarMontos = validarMontos;
	}

	/**
	 * @return the coberturaSeccion
	 */
	public CoberturaSeccionDTO getCoberturaSeccion() {
		return coberturaSeccion;
	}

	/**
	 * @param coberturaSeccion the coberturaSeccion to set
	 */
	public void setCoberturaSeccion(CoberturaSeccionDTO coberturaSeccion) {
		this.coberturaSeccion = coberturaSeccion;
	}

	/**
	 * @return the permisoUsuarios
	 */
	public List<OrdenCompraPermisoUsuario> getPermisoUsuarios() {
		return permisoUsuarios;
	}

	/**
	 * @param permisoUsuarios the permisoUsuarios to set
	 */
	public void setPermisoUsuarios(List<OrdenCompraPermisoUsuario> permisoUsuarios) {
		this.permisoUsuarios = permisoUsuarios;
	}
	
	

}
