package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;

@Entity
@Table(name = "TOLLEGADACIASINIESTRO", schema = "MIDAS")
public class LlegadaCiaSeguros extends MidasAbstracto  implements Entidad {
	
	/**
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LLEGSIN_ID_SEQ")
	@SequenceGenerator(name = "LLEGSIN_ID_SEQ",  schema="MIDAS", sequenceName = "LLEGSIN_ID_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Integer id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "REPORTECABINA_ID")
	private ReporteCabina reporteCabina;
	
	@ManyToOne(fetch=FetchType.LAZY)
   	@JoinColumn(name="CIASEGUROS_ID")
	private PrestadorServicio proveedor;
	
	@Column(name = "ORDEN")
	private Integer orden;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ReporteCabina getReporteCabina() {
		return reporteCabina;
	}

	public void setReporteCabina(ReporteCabina reporteCabina) {
		this.reporteCabina = reporteCabina;
	}

	public PrestadorServicio getProveedor() {
		return proveedor;
	}

	public void setProveedor(PrestadorServicio proveedor) {
		this.proveedor = proveedor;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	@Override
	public Integer getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
}
