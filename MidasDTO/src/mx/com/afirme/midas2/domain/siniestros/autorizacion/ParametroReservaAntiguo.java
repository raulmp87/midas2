package mx.com.afirme.midas2.domain.siniestros.autorizacion;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;


@Entity
@Table(name = "TOPARAMETRORESERVAANTIGUO", schema = "MIDAS")
public class ParametroReservaAntiguo extends MidasAbstracto  implements Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOPARAMRESERVAANT_SEQ")
	@SequenceGenerator(name = "IDTOPARAMRESERVAANT_SEQ",  schema="MIDAS", sequenceName = "IDTOPARAMRESERVAANT_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "CLAVE_SUBCALCULO")
	private String claveSubCalculo;

	@Column(name = "CONDICION_MESES")
	private String condicionMeses;

	@Column(name = "CONDICION_MONTO")
	private String condicionMonto;
	
	@Column(name = "ESTATUS")
	private Boolean estatus;

	@Column(name = "MESES")
	private Integer meses;
	
	@Column(name = "MONTO_RESERVA")
	private BigDecimal monto;
	
	@Column(name = "TIPO_AJUSTE")
	private String tipoAjuste;
	
	@Column(name = "TIPO_SINIESTRO")
	private String tipoSiniestro;

	@Column(name = "ROL_PRIMERAAUTORIZACION")
	private String rolPrimeraAutorizacion;

	@Column(name = "ROL_SEGUNDAAUTORIZACION")
	private String rolSegundaAutorizacion;
	
	@Column(name = "ROL_TERCERAAUTORIZACION")
	private String rolTerceraAutorizacion;

	@Column(name = "ROL_CUARTAAUTORIZACION")
	private String rolCuartaAutorizacion;
	
	@Transient
	private String autorizadores;
	@Transient
	private String configuracionDesc;
	@Transient
	private String estatusDesc;
	@Transient
	private String tipoSiniestroDesc;
	@Transient
	private String nombreCoberturaDesc;
	
	@Transient
	private String idCoberturaClaveSubCalculo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinFetch(JoinFetchType.OUTER)
	@JoinColumn(name = "OFICINA_ID", referencedColumnName = "ID")
	private Oficina oficina;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinFetch(JoinFetchType.OUTER)
	@JoinColumn(name = "COBERTURA_ID", referencedColumnName = "IDTOCOBERTURA")
	private CoberturaDTO cobertura;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getClaveSubCalculo() {
		return claveSubCalculo;
	}


	public void setClaveSubCalculo(String claveSubCalculo) {
		this.claveSubCalculo = claveSubCalculo;
	}


	public String getCondicionMeses() {
		return condicionMeses;
	}


	public void setCondicionMeses(String condicionMeses) {
		this.condicionMeses = condicionMeses;
	}


	public String getCondicionMonto() {
		return condicionMonto;
	}


	public void setCondicionMonto(String condicionMonto) {
		this.condicionMonto = condicionMonto;
	}


	public Boolean getEstatus() {
		return estatus;
	}


	public void setEstatus(Boolean estatus) {
		this.estatus = estatus;
	}


	public Integer getMeses() {
		return meses;
	}


	public void setMeses(Integer meses) {
		this.meses = meses;
	}


	public BigDecimal getMonto() {
		return monto;
	}


	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}


	public String getTipoAjuste() {
		return tipoAjuste;
	}


	public void setTipoAjuste(String tipoAjuste) {
		this.tipoAjuste = tipoAjuste;
	}


	public String getTipoSiniestro() {
		return tipoSiniestro;
	}


	public void setTipoSiniestro(String tipoSiniestro) {
		this.tipoSiniestro = tipoSiniestro;
	}


	public String getRolPrimeraAutorizacion() {
		return rolPrimeraAutorizacion;
	}


	public void setRolPrimeraAutorizacion(String rolPrimeraAutorizacion) {
		this.rolPrimeraAutorizacion = rolPrimeraAutorizacion;
	}


	public String getRolSegundaAutorizacion() {
		return rolSegundaAutorizacion;
	}


	public void setRolSegundaAutorizacion(String rolSegundaAutorizacion) {
		this.rolSegundaAutorizacion = rolSegundaAutorizacion;
	}


	public String getRolTerceraAutorizacion() {
		return rolTerceraAutorizacion;
	}


	public void setRolTerceraAutorizacion(String rolTerceraAutorizacion) {
		this.rolTerceraAutorizacion = rolTerceraAutorizacion;
	}


	public String getRolCuartaAutorizacion() {
		return rolCuartaAutorizacion;
	}


	public void setRolCuartaAutorizacion(String rolCuartaAutorizacion) {
		this.rolCuartaAutorizacion = rolCuartaAutorizacion;
	}


	public String getAutorizadores() {
		return autorizadores;
	}


	public void setAutorizadores(String autorizadores) {
		this.autorizadores = autorizadores;
	}


	public String getConfiguracionDesc() {
		return configuracionDesc;
	}


	public void setConfiguracionDesc(String configuracionDesc) {
		this.configuracionDesc = configuracionDesc;
	}


	public String getEstatusDesc() {
		return estatusDesc;
	}


	public void setEstatusDesc(String estatusDesc) {
		this.estatusDesc = estatusDesc;
	}


	public String getTipoSiniestroDesc() {
		return tipoSiniestroDesc;
	}


	public void setTipoSiniestroDesc(String tipoSiniestroDesc) {
		this.tipoSiniestroDesc = tipoSiniestroDesc;
	}
	
	public String getNombreCoberturaDesc() {
		return nombreCoberturaDesc;
	}


	public void setNombreCoberturaDesc(String nombreCoberturaDesc) {
		this.nombreCoberturaDesc = nombreCoberturaDesc;
	}


	public Oficina getOficina() {
		return oficina;
	}


	public void setOficina(Oficina oficina) {
		this.oficina = oficina;
	}


	public CoberturaDTO getCobertura() {
		return cobertura;
	}


	public void setCobertura(CoberturaDTO cobertura) {
		this.cobertura = cobertura;
	}
	
	public String getIdCoberturaClaveSubCalculo() {
		return idCoberturaClaveSubCalculo;
	}


	public void setIdCoberturaClaveSubCalculo(String idCoberturaClaveSubCalculo) {
		this.idCoberturaClaveSubCalculo = idCoberturaClaveSubCalculo;
	}


	public String[] getRolArray(){
		List<String> rolesList = new ArrayList<String>();
		if(this.rolPrimeraAutorizacion!=null){
			rolesList.add(this.rolPrimeraAutorizacion);
		}
		if(this.rolSegundaAutorizacion!=null){
			rolesList.add(this.rolSegundaAutorizacion);
		}
		if(this.rolTerceraAutorizacion!=null){
			rolesList.add(this.rolTerceraAutorizacion);
		}
		if(this.rolCuartaAutorizacion!=null){
			rolesList.add(this.rolCuartaAutorizacion);
		}
		return rolesList.toArray(new String[rolesList.size()]);
	}


	@SuppressWarnings("unchecked")
	@Override
	public <K> K getKey() {
		return (K) this.id;
	}


	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	

}