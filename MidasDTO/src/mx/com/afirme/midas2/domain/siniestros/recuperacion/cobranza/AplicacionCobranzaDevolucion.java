package mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.ingresos.IngresoDevolucion;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;


@Entity
@Table(name="TOSNAPLICACIONCOBDEVOLUCION", schema="MIDAS")
public class AplicacionCobranzaDevolucion extends MidasAbstracto{
	

	private static final long serialVersionUID = -8158323608917523800L;

	/**
	 * TIPO APLICACION INGRESO  <code>C</code> Cheque <code>default</code>
	 * @author Administrator
	 *
	 */
	public enum TipoAplicacionDevolucion{C, T};
	
	/**
	 * Tipo operacion utilizado en la cuenta <code>C</code> Cargo, <code>A</code> Abono. El default es Abono
	 *
	 */
	public enum TipoOperacionDevolucion{C, A}	

	@EmbeddedId
	private AplicacionCobranzaDevolucionId id = new AplicacionCobranzaDevolucionId();
	
	@ManyToOne(fetch=FetchType.LAZY)	
	@MapsId("aplicacionId")
	private AplicacionCobranza aplicacion;
	
	
	@ManyToOne(fetch=FetchType.LAZY)	
	@MapsId("devolucionId")
	private IngresoDevolucion devolucion;
	
	@Column(name="tipo")
	@Enumerated(EnumType.STRING)
	private TipoAplicacionDevolucion tipo = TipoAplicacionDevolucion.C;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CONCEPTO_ID", referencedColumnName="ID")
	@JoinFetch(JoinFetchType.INNER)
	private ConceptoGuia conceptoGuia;
	
	@Column(name="OPERACION")
	@Enumerated(EnumType.STRING)
	private TipoOperacionDevolucion operacion = TipoOperacionDevolucion.A;	
	
	public AplicacionCobranzaDevolucion() {
		super();
	}
	
	public AplicacionCobranzaDevolucion(IngresoDevolucion devolucion, TipoAplicacionDevolucion tipo, String codigoUsuarioCreacion,  ConceptoGuia conceptoGuia){
		super();
		this.devolucion = devolucion;
		this.id.setDevolucionId(devolucion != null ? devolucion.getId() : null);
		this.tipo = tipo;
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
		this.conceptoGuia = conceptoGuia;
	}
	
	public AplicacionCobranzaDevolucion(AplicacionCobranza aplicacion,
			IngresoDevolucion devolucion, TipoAplicacionDevolucion tipo, String codigoUsuarioCreacion,  ConceptoGuia conceptoGuia) {
		this(devolucion, tipo, codigoUsuarioCreacion, conceptoGuia);
		this.aplicacion = aplicacion;
		this.id.setAplicacionId(aplicacion!= null ? aplicacion.getId() : null);		
	}

	public AplicacionCobranzaDevolucion(IngresoDevolucion devolucion, TipoAplicacionDevolucion tipo, String codigoUsuarioCreacion,  
			ConceptoGuia conceptoGuia, TipoOperacionDevolucion operacion){
		super();
		this.devolucion = devolucion;
		this.id.setDevolucionId(devolucion != null ? devolucion.getId() : null);
		this.tipo = tipo;
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
		this.conceptoGuia = conceptoGuia;
		this.operacion = operacion;
	}
	
	public AplicacionCobranzaDevolucionId getId() {
		return id;
	}

	public void setId(AplicacionCobranzaDevolucionId id) {
		this.id = id;
	}

	public AplicacionCobranza getAplicacion() {
		return aplicacion;
	}

	public void setAplicacion(AplicacionCobranza aplicacion) {
		this.aplicacion = aplicacion;
	}

	public IngresoDevolucion getDevolucion() {
		return devolucion;
	}

	public void setDevolucion(IngresoDevolucion devolucion) {
		this.devolucion = devolucion;
	}

	public TipoAplicacionDevolucion getTipo() {
		return tipo;
	}

	public void setTipo(TipoAplicacionDevolucion tipo) {
		this.tipo = tipo;
	}

	public ConceptoGuia getConceptoGuia() {
		return conceptoGuia;
	}

	public void setConceptoGuia(ConceptoGuia conceptoGuia) {
		this.conceptoGuia = conceptoGuia;
	}
	
	public TipoOperacionDevolucion getOperacion() {
		return operacion;
	}

	public void setOperacion(TipoOperacionDevolucion operacion) {
		this.operacion = operacion;
	}

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
