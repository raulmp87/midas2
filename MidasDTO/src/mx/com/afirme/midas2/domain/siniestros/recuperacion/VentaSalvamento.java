package mx.com.afirme.midas2.domain.siniestros.recuperacion;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;

@Entity(name = "VentaSalvamento")
@Table(name = "TOSNVENTASALVAMENTO", schema = "MIDAS")
public class VentaSalvamento extends MidasAbstracto  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public enum EstatusSalvamento{ACT,CANC};

	@Id
	@SequenceGenerator(name = "TOSNVENTASALVAMENTO_SEQ",allocationSize = 1, sequenceName = "TOSNVENTASALVAMENTO_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TOSNVENTASALVAMENTO_SEQ")
	@Column(name = "ID", nullable = false)
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_COMPRADOR")
	private PrestadorServicio comprador;
	
	@Column(name="CORREO")
	private String correo;
	
	@Column(name="EFICIENCIA_INDEMINIZACION")
	private BigDecimal eficienciaIndeminizacion;
	
	@Column(name="EFICIENCIA_PROVISION")
	private BigDecimal eficienciaProvision;
	
	@Column(name="ESTATUS")
	@Enumerated(EnumType.STRING)
	private EstatusSalvamento estatus;
		
	@Column(name="IVA")
	private BigDecimal iva;
	
	@Column(name="NO_FACTURA")
	private String noFactura;
	
	@Column(name="NO_SUBASTA")
	private String noSubasta;
	
	@Column(name="PORCENTAJE_IVA")
	private Integer porcentajeIva;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "ID_RECUPERACION", referencedColumnName = "RECUPERACION_ID")
	private RecuperacionSalvamento recuperacionSalvamento;
	
	@Column(name="SUBTOTAL")
	private BigDecimal subtotal;
	
	@Column(name="TOTAL_VENTA")
	private BigDecimal totalVenta;
	
	@Column(name="CODIGO_USUARIO_CANCELACION")
	private String CODIGO_USUARIO_CANCELACION;
	
	@Column(name="FECHA_CANCELACION_SUBASTA")
	@Temporal(TemporalType.DATE)
	private Date fechaCancelacionDeSubasta;
	
	@Column(name="FECHA_CIERRE_SUBASTA")
	@Temporal(TemporalType.DATE)
	private Date fechaCierreDeSubasta;
	
	@Column(name="FECHA_ENTREGA")
	@Temporal(TemporalType.DATE)
	private Date fechaEntrega;
	
	@Column(name="FECHA_FACTURA")
	@Temporal(TemporalType.DATE)
	private Date fechaFactura;
	
	@Column(name="FECHA_VENTA")
	@Temporal(TemporalType.DATE)
	private Date fechaVenta;
	
	@Column(name="COMENTARIO_CANCELACION")
	private String comentarioCancelacion;
	
	@Transient
	private String nombreEstatus;
	

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public PrestadorServicio getComprador() {
		return comprador;
	}
	public void setComprador(PrestadorServicio comprador) {
		this.comprador = comprador;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public BigDecimal getEficienciaIndeminizacion() {
		return eficienciaIndeminizacion;
	}
	public void setEficienciaIndeminizacion(BigDecimal eficienciaIndeminizacion) {
		this.eficienciaIndeminizacion = eficienciaIndeminizacion;
	}
	public BigDecimal getEficienciaProvision() {
		return eficienciaProvision;
	}
	public void setEficienciaProvision(BigDecimal eficienciaProvision) {
		this.eficienciaProvision = eficienciaProvision;
	}

	public Date getFechaCancelacionDeSubasta() {
		return fechaCancelacionDeSubasta;
	}
	public void setFechaCancelacionDeSubasta(Date fechaCancelacionDeSubasta) {
		this.fechaCancelacionDeSubasta = fechaCancelacionDeSubasta;
	}
	public Date getFechaCierreDeSubasta() {
		return fechaCierreDeSubasta;
	}
	public void setFechaCierreDeSubasta(Date fechaCierreDeSubasta) {
		this.fechaCierreDeSubasta = fechaCierreDeSubasta;
	}
	public Date getFechaEntrega() {
		return fechaEntrega;
	}
	public void setFechaEntrega(Date fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}
	public Date getFechaFactura() {
		return fechaFactura;
	}
	public void setFechaFactura(Date fechaFactura) {
		this.fechaFactura = fechaFactura;
	}
	public Date getFechaVenta() {
		return fechaVenta;
	}
	public void setFechaVenta(Date fechaVenta) {
		this.fechaVenta = fechaVenta;
	}
	public BigDecimal getIva() {
		return iva;
	}
	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}
	public String getNoFactura() {
		return noFactura;
	}
	public void setNoFactura(String noFactura) {
		this.noFactura = noFactura;
	}
	public String getNoSubasta() {
		return noSubasta;
	}
	public void setNoSubasta(String noSubasta) {
		this.noSubasta = noSubasta;
	}
	public Integer getPorcentajeIva() {
		return porcentajeIva;
	}
	public void setPorcentajeIva(Integer porcentajeIva) {
		this.porcentajeIva = porcentajeIva;
	}
	public BigDecimal getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}
	public BigDecimal getTotalVenta() {
		return totalVenta;
	}
	public void setTotalVenta(BigDecimal totalVenta) {
		this.totalVenta = totalVenta;
	}
	
	@Override
	public Long getKey() {
		return this.id;
	}
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Long getBusinessKey() {
		return this.id;
	}
	public EstatusSalvamento getEstatus() {
		return estatus;
	}
	public void setEstatus(EstatusSalvamento estatus) {
		this.estatus = estatus;
	}
	public RecuperacionSalvamento getRecuperacionSalvamento() {
		return recuperacionSalvamento;
	}
	public void setRecuperacionSalvamento(
			RecuperacionSalvamento recuperacionSalvamento) {
		this.recuperacionSalvamento = recuperacionSalvamento;
	}
	public String getCODIGO_USUARIO_CANCELACION() {
		return CODIGO_USUARIO_CANCELACION;
	}
	public void setCODIGO_USUARIO_CANCELACION(String cODIGO_USUARIO_CANCELACION) {
		CODIGO_USUARIO_CANCELACION = cODIGO_USUARIO_CANCELACION;
	}
	public String getComentarioCancelacion() {
		return comentarioCancelacion;
	}
	public void setComentarioCancelacion(String comentarioCancelacion) {
		this.comentarioCancelacion = comentarioCancelacion;
	}

	public String getNombreEstatus() {
		if( this.estatus.name().equals(EstatusSalvamento.ACT.toString()) && this.recuperacionSalvamento.isAutorizacionProrroga() ){
			return "PRORROGA";
		}else if ( this.estatus.name().equals(EstatusSalvamento.ACT.toString()) && ( !this.recuperacionSalvamento.isAutorizacionProrroga()  ) ){
			return "VENTA";
		}else{
			return "CANCELADA";
		}
	}
	public void setNombreEstatus(String nombreEstatus) {
		this.nombreEstatus = nombreEstatus;
	}
	@Override
	public String toString() {
		return "VentaSalvamento [id=" + id + ", comprador=" + comprador
				+ ", correo=" + correo + ", eficienciaIndeminizacion="
				+ eficienciaIndeminizacion + ", eficienciaProvision="
				+ eficienciaProvision + ", estatus=" + estatus + ", iva=" + iva
				+ ", noFactura=" + noFactura + ", noSubasta=" + noSubasta
				+ ", porcentajeIva=" + porcentajeIva
				+ ", recuperacionSalvamento=" + recuperacionSalvamento
				+ ", subtotal=" + subtotal + ", totalVenta=" + totalVenta
				+ ", CODIGO_USUARIO_CANCELACION=" + CODIGO_USUARIO_CANCELACION
				+ ", fechaCancelacionDeSubasta=" + fechaCancelacionDeSubasta
				+ ", fechaCierreDeSubasta=" + fechaCierreDeSubasta
				+ ", fechaEntrega=" + fechaEntrega + ", fechaFactura="
				+ fechaFactura + ", fechaVenta=" + fechaVenta
				+ ", comentarioCancelacion=" + comentarioCancelacion + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VentaSalvamento other = (VentaSalvamento) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
}
