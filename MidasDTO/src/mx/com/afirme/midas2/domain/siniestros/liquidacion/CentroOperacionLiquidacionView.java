package mx.com.afirme.midas2.domain.siniestros.liquidacion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name = "CentroOperacionLiquidacionView")
@Table(name = "CLISTACENTROOPER", schema = "MIDAS")
public class CentroOperacionLiquidacionView implements Entidad{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_CENTRO_OPER", insertable=true, updatable=true)
	private Long id;
	
	@Column(name="NOM_CENTRO_OPER", insertable = false, updatable = false)
	private String nombre;
	
	@Override
	public Long getKey() {
		
		return id;
	}

	@Override
	public String getValue() {
		
		return nombre;
	}

	@Override
	public Long getBusinessKey() {
		
		return id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
