package mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

@Entity
@Table(name="TOSNMOVTOMANUAL", schema="MIDAS")
public class MovimientoManual extends MidasAbstracto implements Entidad{

	private static final long serialVersionUID = -5689414792727237783L;
	
	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDMOVTOMANUAL_GENERATOR")
	@SequenceGenerator(name="IDMOVTOMANUAL_GENERATOR", sequenceName="TOSNMOVTOMANUAL_SEQ", schema="MIDAS", allocationSize=1)
	private Long id;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CUENTA_ID", referencedColumnName="ID")
	@JoinFetch(JoinFetchType.INNER)
	private ConceptoGuia cuenta;
	
	@Column(name="ESTATUS")
	private Boolean activo = Boolean.TRUE;
	
	@Column(name="CAUSA_MOVIMIENTO")
	private String causaMovimiento;
	
	@OneToMany(mappedBy="movimientoManual", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
	private List<MovimientoManualDet> detalles;
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the cuenta
	 */
	public ConceptoGuia getCuenta() {
		return cuenta;
	}

	/**
	 * @param cuenta the cuenta to set
	 */
	public void setCuenta(ConceptoGuia cuenta) {
		this.cuenta = cuenta;
	}	

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	/**
	 * @return the causaMovimiento
	 */
	public String getCausaMovimiento() {
		return causaMovimiento;
	}

	/**
	 * @param causaMovimiento the causaMovimiento to set
	 */
	public void setCausaMovimiento(String causaMovimiento) {
		this.causaMovimiento = causaMovimiento;
	}

	/**
	 * @return the detalles
	 */
	public List<MovimientoManualDet> getDetalles() {
		return detalles;
	}

	/**
	 * @param detalles the detalles to set
	 */
	public void setDetalles(List<MovimientoManualDet> detalles) {
		this.detalles = detalles;
	}
	

	public void agregar(BigDecimal importe, String codigoUsuarioCreacion, Date fechaCreacion) {
		MovimientoManualDet detalle = new MovimientoManualDet();
		detalle.setImporte(importe);
		detalle.setCodigoUsuarioCreacion(codigoUsuarioCreacion);	
		detalle.setMovimientoManual(this);
		detalle.setFechaCreacion(fechaCreacion);
		
		if(detalles == null){
			detalles = new ArrayList<MovimientoManualDet>(); 
		}
		
		this.detalles.add(detalle);
	}
	
	

}
