package mx.com.afirme.midas2.domain.siniestros.configuracion.horario.laboral;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;

import org.apache.bval.constraints.NotEmpty;

@Entity(name = "HorarioLaboral")
@Table(name = "TOHORARIOLABORAL", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = {
		"HORA_ENTRADA", "HORA_SALIDA" }))
public class HorarioLaboral extends MidasAbstracto implements Entidad {

	private static final long			serialVersionUID	= -1953902474114041590L;
	@Transient
	static public Map<String, String>	ctgHorarios			= new HashMap<String, String>();
	@Transient
	static public TIPO_CATALOGO			ctgHorariosTipo		= TIPO_CATALOGO.HORA_LABORAL;
	@Transient
	final private String				separador			= " - ";

	public static enum Estatus {
		ACTIVO((Integer) 1), INACTIVO((Integer) 0);

		private Integer	estatus;

		Estatus(Integer estatus) {
			this.estatus = estatus;
		}

		public Integer obtenerEstatus() {
			return this.estatus;
		}

		public Integer getObtenerEstatus() {
			return this.obtenerEstatus();
		}

	};

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TOHORARIOLABORAL_SEQ_ID_GENERATOR")
	@SequenceGenerator(name = "TOHORARIOLABORAL_SEQ_ID_GENERATOR", schema = "MIDAS", sequenceName = "IDTOHORARIOLABORAL_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Long	id;

	@NotNull
	@NotEmpty(message = "{org.hibernate.validator.constraints.NotEmpty.message}")
	@Column(name = "HORA_ENTRADA", updatable = false)
	private String	horaEntradaCode;

	@NotNull
	@NotEmpty(message = "{org.hibernate.validator.constraints.NotEmpty.message}")
	@Column(name = "HORA_SALIDA", updatable = false)
	private String	horaSalidaCode;

	@NotNull
	@NotEmpty(message = "{org.hibernate.validator.constraints.NotEmpty.message}")
	@Column(name = "ESTATUS")
	private Integer	estatus;

	@NotNull
	@NotEmpty(message = "{org.hibernate.validator.constraints.NotEmpty.message}")
	@Column(name = "PREDETERMINADO")
	private Boolean	predeterminado;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHoraEntradaCode() {
		return horaEntradaCode;
	}

	public void setHoraEntradaCode(String horaEntradaCode) {
		this.horaEntradaCode = horaEntradaCode;
	}

	public String getHoraSalidaCode() {
		return horaSalidaCode;
	}

	public void setHoraSalidaCode(String horaSalidaCode) {
		this.horaSalidaCode = horaSalidaCode;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public Boolean getPredeterminado() {
		return predeterminado;
	}

	public Boolean esPredeterminado() {
		return predeterminado;
	}

	public void setPredeterminado(Boolean predeterminado) {
		this.predeterminado = predeterminado;
	}

	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof HorarioLaboral) {
			HorarioLaboral instance = (HorarioLaboral) object;
			equal = instance.getId().equals(this.id);
		}
		return equal;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	@Transient
	public String getHoraEntrada() {
		String horaEntrada = "";
		if (horaEntradaCode != null && ctgHorarios.get(horaEntradaCode) != null) {
			horaEntrada = ctgHorarios.get(horaEntradaCode);
		}

		return horaEntrada;
	}

	@Transient
	public String getHoraSalida() {
		String horaSalida = "";
		if (horaSalidaCode != null && ctgHorarios != null) {
			horaSalida = ctgHorarios.get(horaSalidaCode);
		}
		return horaSalida;
	}

	@Transient
	public String getDescripcion() {
		if (getHoraEntrada() != "" && getHoraSalida() != "") {
			return getHoraEntrada() + separador + getHoraSalida();
		} else {
			return "";
		}
	}
}
