package mx.com.afirme.midas2.domain.siniestros.recuperacion;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;

@Entity(name = "RecuperacionCruceroJuridica")
@Table(name = "TOSNRECUPERACIONCRUCERO", schema = "MIDAS")
@DiscriminatorValue("CRU")
@PrimaryKeyJoinColumn(name="RECUPERACION_ID", referencedColumnName="ID")
public class RecuperacionCruceroJuridica  extends Recuperacion{

	private static final long	serialVersionUID	= 6503150552846191981L;
	
	@OneToOne
	@JoinColumn(name = "SINIESTRO_ID")
	private SiniestroCabina siniestroCabina;
	
	@Column(name = "MONTO_FINAL")
	private BigDecimal montoFinal;

	@Column(name= "BANCO")
	private Integer banco;
	
	@Column(name = "NUMERO_APROBACION")
	private String numAprobacion;

	@Column(name = "NUMERO_DEPOSITO")
	private String numDeposito;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name= "FECHA_DEPOSITO", nullable = true)
	private Date fechaDeposito;
	
	public SiniestroCabina getSiniestroCabina() {
		return siniestroCabina;
	}

	public void setSiniestroCabina(SiniestroCabina siniestroCabina) {
		this.siniestroCabina = siniestroCabina;
	}

	public BigDecimal getMontoFinal() {
		return montoFinal;
	}

	public void setMontoFinal(BigDecimal montoFinal) {
		this.montoFinal = montoFinal;
	}

	public Integer getBanco() {
		return banco;
	}

	public void setBanco(Integer banco) {
		this.banco = banco;
	}

	public String getNumAprobacion() {
		return numAprobacion;
	}

	public void setNumAprobacion(String numAprobacion) {
		this.numAprobacion = numAprobacion;
	}

	public String getNumDeposito() {
		return numDeposito;
	}

	public void setNumDeposito(String numDeposito) {
		this.numDeposito = numDeposito;
	}

	public Date getFechaDeposito() {
		return fechaDeposito;
	}

	public void setFechaDeposito(Date fechaDeposito) {
		this.fechaDeposito = fechaDeposito;
	}	
	
}