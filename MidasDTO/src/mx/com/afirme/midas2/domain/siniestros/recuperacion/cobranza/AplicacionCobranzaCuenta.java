package mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.domain.MidasAbstracto;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

@Entity
@Table(name="TOSNAPLICACIONCOBCUENTA", schema="MIDAS")
public class AplicacionCobranzaCuenta extends MidasAbstracto {

	private static final long serialVersionUID = -4696821750646627478L;
	
	/**
	 * Tipo de cuenta <code>D</code> Deposito <code>default</code>, <code>A</code> Acreedoras, <code>M</code> Manual,<code>L</code> Liquidacion
	 * @author Administrator
	 *
	 */
	public enum TipoAplicacionCuenta{D, A, M, L}
	
	/**
	 * Tipo operacion utilizado en la cuenta <code>C</code> Cargo, <code>A</code> Abono. El default es Cargo
	 * @author Administrator
	 *
	 */
	public enum TipoOperacionCobranza{C, A}

	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOSNAPLICACIONCOBCUENTA_GENERATOR")
	@SequenceGenerator(name="TOSNAPLICACIONCOBCUENTA_GENERATOR", schema="MIDAS", sequenceName="TOSNAPLICACIONCOBCUENTA_SEQ",allocationSize=1)	
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="APLICACION_ID", referencedColumnName="ID")
	@JoinFetch(JoinFetchType.INNER)
	private AplicacionCobranza aplicacion;
	
	/**
	 * id refunic, id mov acreedor, id mov manual
	 */
	@Column(name="IDENTIFICADOR")
	private String identificador;
	
	@Column(name="TIPO")
	@Enumerated(EnumType.STRING)
	private TipoAplicacionCuenta tipo = TipoAplicacionCuenta.D;
	
	@Column(name="MONTO_ORIGEN")
	private BigDecimal montoOrigen;
	
	@Column(name="MONTO_APLICADO")
	private BigDecimal montoAplicado;
	
	@Column(name="DESCRIPCION")
	private String descripcion;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CONCEPTO_ID", referencedColumnName="ID")
	@JoinFetch(JoinFetchType.INNER)
	private ConceptoGuia conceptoGuia;
	
	@Column(name="OPERACION")
	@Enumerated(EnumType.STRING)
	private TipoOperacionCobranza operacion = TipoOperacionCobranza.C;
	
	public AplicacionCobranzaCuenta(){
		super();
	}
	
	public AplicacionCobranzaCuenta(AplicacionCobranza aplicacion, String identificador, TipoAplicacionCuenta tipo, 
			String codigoUsuarioCreacion, String descripcion, ConceptoGuia conceptoGuia){
		this();
		this.identificador = identificador;
		this.tipo = tipo;
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;		
		this.aplicacion = aplicacion;
		this.descripcion = descripcion;
		this.conceptoGuia = conceptoGuia;
	}
	
	public AplicacionCobranzaCuenta(AplicacionCobranza aplicacion, String identificador, TipoAplicacionCuenta tipo,
			BigDecimal montoOrigen, BigDecimal montoAplicado, String codigoUsuarioCreacion, String descripcion, 
			ConceptoGuia conceptoGuia, TipoOperacionCobranza operacion) {
		this(aplicacion, identificador, tipo, codigoUsuarioCreacion, descripcion, conceptoGuia);
		this.montoOrigen = montoOrigen;
		this.montoAplicado = montoAplicado;
		this.operacion = operacion;
	}	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public AplicacionCobranza getAplicacion() {
		return aplicacion;
	}

	public void setAplicacion(AplicacionCobranza aplicacion) {
		this.aplicacion = aplicacion;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public TipoAplicacionCuenta getTipo() {
		return tipo;
	}

	public void setTipo(TipoAplicacionCuenta tipo) {
		this.tipo = tipo;
	}

	public BigDecimal getMontoOrigen() {
		return montoOrigen;
	}

	public void setMontoOrigen(BigDecimal montoOrigen) {
		this.montoOrigen = montoOrigen;
	}

	public BigDecimal getMontoAplicado() {
		return montoAplicado;
	}

	public void setMontoAplicado(BigDecimal montoAplicado) {
		this.montoAplicado = montoAplicado;
	}	
	
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public ConceptoGuia getConceptoGuia() {
		return conceptoGuia;
	}

	public void setConceptoGuia(ConceptoGuia conceptoGuia) {
		this.conceptoGuia = conceptoGuia;
	}
	
	public TipoOperacionCobranza getOperacion() {
		return operacion;
	}

	public void setOperacion(TipoOperacionCobranza operacion) {
		if(operacion != null){
			this.operacion = operacion;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}	

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AplicacionCobranzaCuenta other = (AplicacionCobranzaCuenta) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return identificador;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

}
