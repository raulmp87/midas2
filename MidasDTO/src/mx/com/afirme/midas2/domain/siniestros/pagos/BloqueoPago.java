package mx.com.afirme.midas2.domain.siniestros.pagos;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;

@Entity
@Table(name = "TOBLOQUEOPAGOS", schema = "MIDAS")
public class BloqueoPago extends MidasAbstracto  implements Entidad {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TOBLOQUEOPAGOS_ID_SEQ")
	@SequenceGenerator(name = "TOBLOQUEOPAGOS_ID_SEQ",  schema="MIDAS", sequenceName = "TOBLOQUEOPAGOS_ID_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "ESTATUS")
	private Boolean estatus;
	
	@Column(name = "TIPO")
	private String tipo;
	
	@Column(name = "TIPO_PRESTADOR")
	private String tipoPrestador;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_BLOQUEO")
	private Date fechaBloqueo;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_DESBLOQUEO")
	private Date fechaDesbloqueo;
	
	@Column(name = "MOTIVO_BLOQUEO")
	private String motivoBloqueo;
	
	@Column(name = "MOTIVO_DESBLOQUEO")
	private String motivoDesbloqueo;
	
	@Column(name = "USUARIO_BLOQUEO")
	private String usuarioBloqueo;
	
	@Column(name = "USUARIO_DESBLOQUEO")
	private String usuarioDesbloqueo;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PAGO", referencedColumnName="ID")
	private OrdenPagoSiniestro ordenPago;
	
	
	
	@ManyToOne(fetch=FetchType.LAZY)
   	@JoinColumn(name="ID_PROVEEDOR")
	private PrestadorServicio proveedor;
	
	@Transient
	private String situacionActualDesc;
	
	@Transient
	private String tipoDeBloqueoDesc;
	
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Boolean getEstatus() {
		return estatus;
	}
	public void setEstatus(Boolean estatus) {
		this.estatus = estatus;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Date getFechaBloqueo() {
		return fechaBloqueo;
	}
	public void setFechaBloqueo(Date fechaBloqueo) {
		this.fechaBloqueo = fechaBloqueo;
	}
	public Date getFechaDesbloqueo() {
		return fechaDesbloqueo;
	}
	public void setFechaDesbloqueo(Date fechaDesbloqueo) {
		this.fechaDesbloqueo = fechaDesbloqueo;
	}
	public String getMotivoBloqueo() {
		return motivoBloqueo;
	}
	public void setMotivoBloqueo(String motivoBloqueo) {
		this.motivoBloqueo = motivoBloqueo;
	}
	public String getMotivoDesbloqueo() {
		return motivoDesbloqueo;
	}
	public void setMotivoDesbloqueo(String motivoDesbloqueo) {
		this.motivoDesbloqueo = motivoDesbloqueo;
	}
	public String getUsuarioBloqueo() {
		return usuarioBloqueo;
	}
	public void setUsuarioBloqueo(String usuarioBloqueo) {
		this.usuarioBloqueo = usuarioBloqueo;
	}
	public String getUsuarioDesbloqueo() {
		return usuarioDesbloqueo;
	}
	public void setUsuarioDesbloqueo(String usuarioDesbloqueo) {
		this.usuarioDesbloqueo = usuarioDesbloqueo;
	}
	public OrdenPagoSiniestro getOrdenPago() {
		return ordenPago;
	}
	public void setOrdenPago(OrdenPagoSiniestro ordenPago) {
		this.ordenPago = ordenPago;
	}
	public PrestadorServicio getProveedor() {
		return proveedor;
	}
	public void setProveedor(PrestadorServicio proveedor) {
		this.proveedor = proveedor;
	}
	public String getSituacionActualDesc() {
		return situacionActualDesc;
	}
	public void setSituacionActualDesc(String situacionActualDesc) {
		this.situacionActualDesc = situacionActualDesc;
	}
	public String getTipoDeBloqueoDesc() {
		return tipoDeBloqueoDesc;
	}
	public void setTipoDeBloqueoDesc(String tipoDeBloqueoDesc) {
		this.tipoDeBloqueoDesc = tipoDeBloqueoDesc;
	}
	public String getTipoPrestador() {
		return tipoPrestador;
	}
	public void setTipoPrestador(String tipoPrestador) {
		this.tipoPrestador = tipoPrestador;
	}
}