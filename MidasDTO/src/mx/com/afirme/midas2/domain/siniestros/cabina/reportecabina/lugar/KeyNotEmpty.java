package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = {EntidadKeyValidator.class})
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(value = RetentionPolicy.RUNTIME)
@Documented
public @interface KeyNotEmpty {

  String message() default "Este campo es requerido";

  Class<?>[] groups() default {};
  Class<? extends Payload>[] payload() default {};
}