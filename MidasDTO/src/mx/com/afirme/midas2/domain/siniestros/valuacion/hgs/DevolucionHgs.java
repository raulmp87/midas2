package mx.com.afirme.midas2.domain.siniestros.valuacion.hgs;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;

@Entity
@Table(name = "TOSNDEVOLUCIONHGS", schema = "MIDAS") 
public class DevolucionHgs extends MidasAbstracto implements Entidad {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTOSNDEVOLUCIONHGS_SEQ")
	@SequenceGenerator(name="IDTOSNDEVOLUCIONHGS_SEQ", schema="MIDAS", sequenceName="IDTOSNDEVOLUCIONHGS_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;
	
	/*@Column(name="ORDEN_COMPRA_ID")
	private Long ordenCompraId;**/
	@OneToOne (fetch=FetchType.LAZY )
	@JoinColumn(name="ORDEN_COMPRA_ID", referencedColumnName="ID" , updatable = false)
	OrdenCompra ordenCompra;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "VALUACION_ID", nullable=false, referencedColumnName="VALUACION_HGS")
	private ValuacionHgs valuacion;
	
	@Column(name="NOMBRE_ADMIN_REFACCIONES")
	private String nombreAdminRefacciones;
	
	@Column(name="NOMBRE_PERSONA_DEVOLUCION")
	private String nomprePersonaDevolucion;
	
	@Column(name="MOTIVO")
	private String motivo;
	
	@Column(name="CANTIDAD_REFACCIONES")
	private int cantidadRefacciones;
	
	@Column(name="SUBTOTAL")
	private BigDecimal subtotal;
	
	@Column(name="IVA_TOTAL")
	private BigDecimal iva;
	
	@Column(name="MONTO_TOTAL")
	private BigDecimal montoTotal;
	
	@OneToMany(mappedBy="devolucionHgs", fetch = FetchType.LAZY,cascade=CascadeType.ALL)
	private List<DevolucionHgsRefaccion> devolucionHgsRefaccion;
	
	
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		// TODO Auto-generated method stub
		return this.id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}



	public String getNombreAdminRefacciones() {
		return nombreAdminRefacciones;
	}

	public void setNombreAdminRefacciones(String nombreAdminRefacciones) {
		this.nombreAdminRefacciones = nombreAdminRefacciones;
	}

	public String getNomprePersonaDevolucion() {
		return nomprePersonaDevolucion;
	}

	public void setNomprePersonaDevolucion(String nomprePersonaDevolucion) {
		this.nomprePersonaDevolucion = nomprePersonaDevolucion;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public int getCantidadRefacciones() {
		return cantidadRefacciones;
	}

	public void setCantidadRefacciones(int cantidadRefacciones) {
		this.cantidadRefacciones = cantidadRefacciones;
	}

	public BigDecimal getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public BigDecimal getIva() {
		return iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public BigDecimal getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DevolucionHgs other = (DevolucionHgs) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public List<DevolucionHgsRefaccion> getDevolucionHgsRefaccion() {
		return devolucionHgsRefaccion;
	}

	public void setDevolucionHgsRefaccion(
			List<DevolucionHgsRefaccion> devolucionHgsRefaccion) {
		this.devolucionHgsRefaccion = devolucionHgsRefaccion;
	}

	public ValuacionHgs getValuacion() {
		return valuacion;
	}

	public void setValuacion(ValuacionHgs valuacion) {
		this.valuacion = valuacion;
	}

	@Override
	public String toString() {
		return "DevolucionHgs [id=" + id + ", ordenCompra=" + ordenCompra.getId()
				+ ", valuacion=" + valuacion + ", nombreAdminRefacciones="
				+ nombreAdminRefacciones + ", nomprePersonaDevolucion="
				+ nomprePersonaDevolucion + ", motivo=" + motivo
				+ ", cantidadRefacciones=" + cantidadRefacciones
				+ ", subtotal=" + subtotal + ", iva=" + iva + ", montoTotal="
				+ montoTotal + ", devolucionHgsRefaccion="
				+ devolucionHgsRefaccion + "]";
	}

	public OrdenCompra getOrdenCompra() {
		return ordenCompra;
	}

	public void setOrdenCompra(OrdenCompra ordenCompra) {
		this.ordenCompra = ordenCompra;
	}

}
