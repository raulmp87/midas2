package mx.com.afirme.midas2.domain.siniestros.notificaciones;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.MidasAbstracto;

@Entity
@Table(name="TOSNBITDESTINONOTIFICACION", schema="MIDAS")
public class BitacoraDestinoNotificacion extends MidasAbstracto {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOBITDESTINONOTIFMIDAS_GENERATOR")
	@SequenceGenerator(name="TOBITDESTINONOTIFMIDAS_GENERATOR", schema="MIDAS", sequenceName="TOSNBITDESTINONOTIF_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;
	
	@Column(name="DESTINATARIO")
	private String destinatario;
	
	@ManyToOne(fetch=FetchType.LAZY)	
	@JoinColumn(name = "BITACORA_ID", referencedColumnName = "ID", nullable = false)
	private BitacoraNotificacion bitacora;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="destino", cascade=CascadeType.ALL)
	private List<BitacoraDetalleNotificacion> detalles = new ArrayList<BitacoraDetalleNotificacion>();
	
	public BitacoraDestinoNotificacion(){
		super();
	}

	public BitacoraDestinoNotificacion(String destinatario, String  codigoUsuarioCreacion) {
		this();
		this.destinatario = destinatario;		
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
	
	public BitacoraDestinoNotificacion(String destinatario,
			BitacoraNotificacion bitacora, String codigoUsuarioCreacion) {		
		this(destinatario, codigoUsuarioCreacion);
		this.bitacora = bitacora;		
	}
	
	@Transient
	public void agregar(BitacoraDetalleNotificacion detalle){
		detalle.setDestino(this);
		detalle.getId().setDestinoId(this.id);
		this.detalles.add(detalle);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	public BitacoraNotificacion getBitacora() {
		return bitacora;
	}

	public void setBitacora(BitacoraNotificacion bitacora) {
		this.bitacora = bitacora;
	}

	public List<BitacoraDetalleNotificacion> getDetalles() {
		return detalles;
	}

	public void setDetalles(List<BitacoraDetalleNotificacion> detalles) {
		this.detalles = detalles;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return bitacora != null ? bitacora.getFolio().concat("-").concat(destinatario) : destinatario;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {		
		return id;
	}
	
	@Transient
	public Integer siguienteSecuencia(){
		int size = this.detalles.size();
		if(size == 0){
			return 1;
		}else{
			return ++size;
		}		
	}
	
}
