package mx.com.afirme.midas2.domain.siniestros.recuperacion;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.banco.BancoMidas;

/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 19-may-2015 09:15:56 a.m.
 */
@Entity(name = "ReferenciaBancaria")
@Table(name = "TOSNREFBANCARIARECUPERACION", schema = "MIDAS")
public class ReferenciaBancaria implements Serializable, Entidad {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2540885975952220872L;

	public static final String SECUENCIA_NUMERO = "MIDAS.SN_REFBANC_CONSECUTIVO_SEQ";

	@Id
	@SequenceGenerator(name = "TOSNREFBANCARIARECU_SEQ_GENERADOR",allocationSize = 1, sequenceName = "TOSNREFBANCARIARECU_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TOSNREFBANCARIARECU_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "RECUPERACION_ID", referencedColumnName = "ID")
	private Recuperacion recuperacion;
	
	
	@Column(name = "MEDIO_RECUPERACION")
	private String medioRecuperacion;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "BANCO_ID", referencedColumnName = "ID_BANCO")
	private BancoMidas banco;
	
	@Column(name = "CLABE")
	private String clabe;
	
	
	@Column(name = "CUENTA_BANCARIA")
	private String cuentaBancaria;
	
	
	@Column(name = "REFERENCIA")
	private String referencia;
	
	@Column(name = "CONSECUTIVO_REFERENCIA")
	private Long consecutivo;
	
	@Column(name = "ACTIVO")
	private Boolean activo = Boolean.TRUE;


	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}



	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.id;
	}



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public Recuperacion getRecuperacion() {
		return recuperacion;
	}



	public void setRecuperacion(Recuperacion recuperacion) {
		this.recuperacion = recuperacion;
	}



	public String getMedioRecuperacion() {
		return medioRecuperacion;
	}



	public void setMedioRecuperacion(String medioRecuperacion) {
		this.medioRecuperacion = medioRecuperacion;
	}


	public BancoMidas getBanco() {
		return banco;
	}



	public void setBanco(BancoMidas banco) {
		this.banco = banco;
	}



	public String getClabe() {
		return clabe;
	}



	public void setClabe(String clabe) {
		this.clabe = clabe;
	}



	public String getCuentaBancaria() {
		return cuentaBancaria;
	}



	public void setCuentaBancaria(String cuentaBancaria) {
		this.cuentaBancaria = cuentaBancaria;
	}



	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
		
	public Long getConsecutivo() {
		return consecutivo;
	}

	public void setConsecutivo(Long consecutivo) {
		this.consecutivo = consecutivo;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}


	public static enum AlgoritmoReferenciaBancaria{
		NO_APLICA("0"), 
		MODULO_45("1"), 
		MODULO_97("2"), 
		MODULO_22("3"), 
		RUP("4");
		
		
		public final String codigo;
		AlgoritmoReferenciaBancaria(String codigo){
			this.codigo = codigo;
		}
	}

	
	public static enum NivelCuentaBancaria {
		GENERAL("0"), 
		CLIENTE("1"), 
		AGENTE("2"), 
		INGRESOS("3");
		
		
		public final String codigo;
		NivelCuentaBancaria(String codigo){
			this.codigo = codigo;
		}
	}

	

	
	
	

}