package mx.com.afirme.midas2.domain.siniestros.recuperacion;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.EnumBase;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.util.CollectionUtils;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;


/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 19-may-2015 09:15:56 a.m.
 */
//@Entity @Table(TOSNRECUPERACION)@Inheritance(strategy = InheritanceType.JOINED) @DiscriminatorColumn(tipo)
@Entity(name = "Recuperacion")
@Table(name = "TOSNRECUPERACION", schema = "MIDAS")
@DiscriminatorColumn(name="TIPO")
@Inheritance(strategy=InheritanceType.JOINED)
public class Recuperacion extends MidasAbstracto {	

	/**
	 * 
	 */
	private static final long serialVersionUID = -672340007059133410L;
	
	public static final String SECUENCIA_NUMERO = "MIDAS.SN_RECUPERACION_NUM_SEQ";
	
	@Id
	@Column(name="ID",nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOSNRECUPERACION_SEQ_GENERATOR")	
	@SequenceGenerator(name="TOSNRECUPERACION_SEQ_GENERATOR", schema="MIDAS", sequenceName="TOSNRECUPERACION_SEQ", allocationSize=1)	
	protected Long id;	
	@Column(name = "ACTIVO")
	private Boolean activo = Boolean.TRUE;	
	@Column(name = "ESTATUS")
	protected String estatus;	  
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_APLICACION")
	private Date fechaAplicacion;	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CANCELACION")
	private Date fechaCancelacion;
	@Column(name = "MOTIVO_CANCELACION")
	private String motivoCancelacion;
	@Column(name = "NUMERO")
	protected Long numero;
	@Column(name = "MEDIO")
	private String medio;
	@Column(name = "ORIGEN")
	private String origen;
	@Column(name = "TIPO") 
	private String tipo;
	@Column(name = "TIPO_ORDENCOMPRA") 
	private String tipoOrdenCompra;	
	@Column(name = "CODIGO_USUARIO_CANCELACION")
	private String codigoUsuarioCancelacion;

	@Column(name = "CODIGO_USUARIO_APLICACION")
	private String codigoUsuarioAplicacion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_INACTIVACION")
	private Date fechaInactivacion;

	@Column(name = "CAUSA_INACTIVACION")
	private String causaInactivacion;
	
	
	@Column(name = "ESTATUS_PROVISION", columnDefinition="default 0")
	private Boolean estatusProvision;

	@OneToMany(fetch=FetchType.LAZY,  mappedBy = "recuperacion")
	@JoinFetch(JoinFetchType.OUTER)
	private List<Ingreso> ingresos;

	@OneToMany(fetch=FetchType.LAZY, mappedBy = "recuperacion")
	@JoinFetch(JoinFetchType.OUTER)
	private List<ReferenciaBancaria> referenciasBancarias;

	public  enum EstatusRecuperacion implements EnumBase<String>{
		REGISTRADO,
		PENDIENTE,
		CANCELADO,
		RECUPERADO,
		INACTIVO;

		@Override
		public String getValue() {
			return this.toString();
		}

		@Override
		public String getLabel() {
			return this.toString();
		}
	}

	public static enum TipoRecuperacion implements EnumBase<String>{
		COMPANIA("CIA", "01"), 
		CRUCEROJURIDICA("CRU", "02"), 
		DEDUCIBLE("DED", "03"), 
		PROVEEDOR("PRV", "04"), 
		SALVAMENTO("SVM", "05"),
		SIPAC("SIP", "06");

		private String codigo, codigoReferencia;

		TipoRecuperacion(String codigo, String codigoReferencia){
			this.codigo = codigo;
			this.codigoReferencia = codigoReferencia;
		}			

		@Override
		public String toString(){
			return codigo;
		}

		public String getCodigoReferencia() {
			return codigoReferencia;
		}

		@Override
		public String getValue() {
			return codigo;
		}

		@Override
		public String getLabel() {
			return null;
		}


	}

	/**
	 * REEMBOLSO,VENTA,NOTACRED
	 */
	public  enum MedioRecuperacion{
		REEMBOLSO,VENTA,NOTACRED,EFECTIVO,TDC,INDEMNIZA,SIPAC;
	}

	/**
	 * MANUAL,AUTOMATICA
	 */
	public enum OrigenRecuperacion {
		MANUAL,AUTOMATICA;
	}

	public enum TipoServicioPoliza {
		PUBLICO,PARTICULAR;
	}




	public static enum TipoOrdenCompra{
		AFECTACION,GASTO,TODOS;
	}


	//@ManyToOne @JoinColumn @MapsId

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "REPORTECABINA_ID", referencedColumnName = "ID")
	private ReporteCabina reporteCabina;

	@OneToMany(fetch=FetchType.LAZY, mappedBy="recuperacion", cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinFetch(JoinFetchType.OUTER)
	private List<CoberturaRecuperacion> coberturas;

	@Transient
	public Ingreso getIngresoActivo(){
		Ingreso ingresoActivo = null;	
		for(Ingreso ingreso : CollectionUtils.emptyIfNull(ingresos)){
			if(ingreso.getEstatus().equals(Ingreso.EstatusIngreso.PENDIENTE.getValue())
					|| ingreso.getEstatus().equals(Ingreso.EstatusIngreso.APLICADO.getValue())){
				ingresoActivo = ingreso;
				break;
			}
		}
		return ingresoActivo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public Date getFechaAplicacion() {
		return fechaAplicacion;
	}

	public void setFechaAplicacion(Date fechaAplicacion) {
		this.fechaAplicacion = fechaAplicacion;
	}

	public Date getFechaCancelacion() {
		return fechaCancelacion;
	}

	public void setFechaCancelacion(Date fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}

	public String getMotivoCancelacion() {
		return motivoCancelacion;
	}

	public void setMotivoCancelacion(String motivoCancelacion) {
		this.motivoCancelacion = motivoCancelacion;
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public String getMedio() {
		return medio;
	}

	public void setMedio(String medio) {
		this.medio = medio;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getTipoOrdenCompra() {
		return tipoOrdenCompra;
	}

	public void setTipoOrdenCompra(String tipoOrdenCompra) {
		this.tipoOrdenCompra = tipoOrdenCompra;
	}

	public ReporteCabina getReporteCabina() {
		return reporteCabina;
	}

	public void setReporteCabina(ReporteCabina reporteCabina) {
		this.reporteCabina = reporteCabina;
	}

	public String getCodigoUsuarioCancelacion() {
		return codigoUsuarioCancelacion;
	}

	public void setCodigoUsuarioCancelacion(String codigoUsuarioCancelacion) {
		this.codigoUsuarioCancelacion = codigoUsuarioCancelacion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		return id.toString() + "-" + numero.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.id;
	}

	public Date getFechaInactivacion() {
		return fechaInactivacion;
	}

	public void setFechaInactivacion(Date fechaInactivacion) {
		this.fechaInactivacion = fechaInactivacion;
	}

	public String getCausaInactivacion() {
		return causaInactivacion;
	}

	public void setCausaInactivacion(String causaInactivacion) {
		this.causaInactivacion = causaInactivacion;
	}

	/**
	 * @return the ingresos
	 */
	public List<Ingreso> getIngresos() {
		return ingresos;
	}

	/**
	 * @param ingresos the ingresos to set
	 */
	public void setIngresos(List<Ingreso> ingresos) {
		this.ingresos = ingresos;
	}

	public String getCodigoUsuarioAplicacion() {
		return codigoUsuarioAplicacion;
	}

	public void setCodigoUsuarioAplicacion(String codigoUsuarioAplicacion) {
		this.codigoUsuarioAplicacion = codigoUsuarioAplicacion;
	}	

	public List<CoberturaRecuperacion> getCoberturas() {
		return coberturas;
	}	

	public void setCoberturas(List<CoberturaRecuperacion> coberturas) {
		this.coberturas = coberturas;
	}


	/**
	 * @return the referenciasBancarias
	 */
	public List<ReferenciaBancaria> getReferenciasBancarias() {
		List<ReferenciaBancaria> referencias = new ArrayList<ReferenciaBancaria>(); 
		if(referenciasBancarias  != null && referenciasBancarias.size() > 0){
			for(ReferenciaBancaria ref : referenciasBancarias){
				if(ref.getActivo()){
					referencias.add(ref);
				}
			}
		}		
		return referenciasBancarias;
	}
	
	public Boolean getEstatusProvision() {
		return estatusProvision;
	}

	public void setEstatusProvision(Boolean estatusProvision) {
		this.estatusProvision = estatusProvision;
	}

	/**
	 * @param referenciasBancarias the referenciasBancarias to set
	 */
	public void setReferenciasBancarias(
			List<ReferenciaBancaria> referenciasBancarias) {
		this.referenciasBancarias = referenciasBancarias;
	}

	@Override
	public String toString() {
		return "Recuperacion [id=" + id + ", activo=" + activo + ", estatus="
		+ estatus + ", fechaAplicacion=" + fechaAplicacion
		+ ", fechaCancelacion=" + fechaCancelacion
		+ ", motivoCancelacion=" + motivoCancelacion + ", numero="
		+ numero + ", medio=" + medio + ", origen=" + origen
		+ ", tipo=" + tipo + ", tipoOrdenCompra=" + tipoOrdenCompra
		+ ", codigoUsuarioCancelacion=" + codigoUsuarioCancelacion
		+ ", codigoUsuarioAplicacion=" + codigoUsuarioAplicacion
		+ ", fechaInactivacion=" + fechaInactivacion
		+ ", causaInactivacion=" + causaInactivacion + ", ingresos="
		+ ingresos + ", reporteCabina=" + reporteCabina + "]";
	}
	
	@PrePersist
    protected void actualizaEstatusProvision() {
		if(this.estatusProvision== null){
			this.estatusProvision = false;
		}
	}

}
