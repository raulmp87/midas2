package mx.com.afirme.midas2.domain.siniestros.cabina.monitor;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name = "AlertaMonitorCabina")
@Table(name = "TOALERTAMONITORCABINA", schema = "MIDAS")
public class AlertaMonitorCabina implements Entidad {
	
	private static final long serialVersionUID = -6509041399408290990L;

	public static final String CITA = "Cita";
	
	public static final String CRUCERO = "Crucero";
	
	public static enum CampoInicioConteo{
		FECHA_HORA_REPORTE, FECHA_HORA_ASIGNACION, FECHA_HORA_CONTACTO, FECHA_HORA_CITA, FECHA_HORA_CITA_CONTACTO, FECHA_HORA_CITA_TERMINACION
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOCONFIGNOTIFCABINA_SEQ_ID_GENERATOR")
	@SequenceGenerator(name="TOALERTAMONITORCABINA_SEQ_ID_GENERATOR", schema="MIDAS", sequenceName="IDTOALERTAMONITORCABINA_SEQ", allocationSize=1)	
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "SECCION_MONITOR")
	private String seccionMonitor;
	
	@Column(name = "ROL")
	private String rol;
	
	@Column(name = "TIENE_CITA")
	private Boolean tieneCita;
	
	@Column(name = "TIEMPO_PARA_ALERTA")
	private Long tiempoParaAlerta;
	
	@Column(name = "CAMPO_INICIO_CONTEO")
	private String campoInicioConteo;

	@Column(name = "CUENTA_REGRESIVA")
	private Boolean cuentaRegresiva;
	
	@Column(name = "TIPO_ALERTA")
	private String tipoAlerta;
	
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;
	
	@Column(name="CODIGO_USUARIO_CREACION")
	private String codigoUsuarioCreacion;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSeccionMonitor() {
		return seccionMonitor;
	}

	public void setSeccionMonitor(String seccionMonitor) {
		this.seccionMonitor = seccionMonitor;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public String getRol() {
		return rol;
	}

	public Boolean getTieneCita() {
		return tieneCita;
	}

	public void setTieneCita(Boolean tieneCita) {
		this.tieneCita = tieneCita;
	}
	
	public String getCitaCruzero() {
		return tieneCita?CITA:CRUCERO;
	}

	public Long getTiempoParaAlerta() {
		return tiempoParaAlerta;
	}

	public void setTiempoParaAlerta(Long tiempoParaAlerta) {
		this.tiempoParaAlerta = tiempoParaAlerta;
	}

	public String getCampoInicioConteo() {
		return campoInicioConteo;
	}

	public void setCampoInicioConteo(String campoInicioConteo) {
		this.campoInicioConteo = campoInicioConteo;
	}

	public void setCuentaRegresiva(Boolean cuentaRegresiva) {
		this.cuentaRegresiva = cuentaRegresiva;
	}

	public Boolean getCuentaRegresiva() {
		return cuentaRegresiva;
	}

	public String getTipoAlerta() {
		return tipoAlerta;
	}

	public void setTipoAlerta(String tipoAlerta) {
		this.tipoAlerta = tipoAlerta;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

}
