package mx.com.afirme.midas2.domain.siniestros.valuacion;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;

@Entity
@Table(name = "TOVALUACIONREPORTE", schema = "MIDAS")
public class ValuacionReporte extends MidasAbstracto implements Entidad{

	private static final long serialVersionUID = 6975084395035025959L;
	
	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOVALUACIONREPORTE_GENERATOR")
	@SequenceGenerator(name="TOVALUACIONREPORTE_GENERATOR", sequenceName="IDTOVALUACIONREPORTE_SEQ", schema="MIDAS", allocationSize=1)
	private Long id;
	
	@Column(name="CODIGO_VALUADOR", nullable=false)
	private String codigoValuador;
	
	@Column(name="ESTATUS", nullable=false)
	private Integer estatus;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_VALUACION")
	private Date fechaValuacion;
	
	@Column(name="OBSERVACIONES")
	private String observaciones;
	
	@Column(name="TOTAL")
	private Double total;
	
	@Column(name="TOTAL_HOJALATERIA")
	private Double totalHojalateria;
	
	@Column(name="TOTAL_PINTURA")
	private Double totalPintura;
	
	@Column(name="TOTAL_REFACCIONES")
	private Double totalRefacciones;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="REPORTE_ID", nullable=false, referencedColumnName="ID")
	private ReporteCabina reporte;
	
	@OneToMany(mappedBy="valuacionReporte", fetch = FetchType.LAZY)
	private List<ValuacionReportePieza> piezas;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="AJUSTADOR_ID", referencedColumnName="ID")
	private ServicioSiniestro ajustador;
	
	@Transient
	private final static DecimalFormat formato = new DecimalFormat("#.00"); 
	

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the ajustadorId
	 */
	public ServicioSiniestro getAjustador() {
		return ajustador;
	}

	/**
	 * @param ajustadorId the ajustadorId to set
	 */
	public void setAjustador(ServicioSiniestro ajustador) {
		this.ajustador = ajustador;
	}

	/**
	 * @return the codigoValuador
	 */
	public String getCodigoValuador() {
		return codigoValuador;
	}

	/**
	 * @param codigoValuador the codigoValuador to set
	 */
	public void setCodigoValuador(String codigoValuador) {
		this.codigoValuador = codigoValuador;
	}

	/**
	 * @return the estatus
	 */
	public Integer getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return the fechaValuacion
	 */
	public Date getFechaValuacion() {
		return fechaValuacion;
	}

	/**
	 * @param fechaValuacion the fechaValuacion to set
	 */
	public void setFechaValuacion(Date fechaValuacion) {
		this.fechaValuacion = fechaValuacion;
	}

	/**
	 * @return the observaciones
	 */
	public String getObservaciones() {
		return observaciones;
	}

	/**
	 * @param observaciones the observaciones to set
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	/**
	 * @return the total
	 */
	public Double getTotal() {
		if(total != null)
			total = Double.parseDouble(formato.format(total));
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(Double total) {
		this.total = total;
	}

	/**
	 * @return the totalHojalateria
	 */
	public Double getTotalHojalateria() {
		if(totalHojalateria != null)
			totalHojalateria = Double.parseDouble(formato.format(totalHojalateria));
		return totalHojalateria;
	}

	/**
	 * @param totalHojalateria the totalHojalateria to set
	 */
	public void setTotalHojalateria(Double totalHojalateria) {
		this.totalHojalateria = totalHojalateria;
	}

	/**
	 * @return the totalPintura
	 */
	public Double getTotalPintura() {
		if(totalPintura != null)
			totalPintura = Double.parseDouble(formato.format(totalPintura));
		return totalPintura;
	}

	/**
	 * @param totalPintura the totalPintura to set
	 */
	public void setTotalPintura(Double totalPintura) {
		this.totalPintura = totalPintura;
	}

	/**
	 * @return the totalRefacciones
	 */
	public Double getTotalRefacciones() {
		if(totalRefacciones != null)
			totalRefacciones = Double.parseDouble(formato.format(totalRefacciones));
		return totalRefacciones;
	}

	/**
	 * @param totalRefacciones the totalRefacciones to set
	 */
	public void setTotalRefacciones(Double totalRefacciones) {
		this.totalRefacciones = totalRefacciones;
	}

	/**
	 * @return the piezas
	 */
	public List<ValuacionReportePieza> getPiezas() {
		return piezas;
	}

	/**
	 * @param piezas the piezas to set
	 */
	public void setPiezas(List<ValuacionReportePieza> piezas) {
		this.piezas = piezas;
	}

	/**
	 * @return the reporte
	 */
	public ReporteCabina getReporte() {
		return reporte;
	}

	/**
	 * @param reporte the reporte to set
	 */
	public void setReporte(ReporteCabina reporte) {
		this.reporte = reporte;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

}
