package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;


@Entity(name = "BitacoraEventoSiniestro")
@Table(name = "TOBITACORAEVENTOSINIESTRO", schema = "MIDAS")
public class BitacoraEventoSiniestro extends MidasAbstracto  implements Entidad {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6687090548324948046L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOBITACORAEVENTOSINIESTRO_ID_GENERATOR")
	@SequenceGenerator(name="TOBITACORAEVENTOSINIESTRO_ID_GENERATOR", schema="MIDAS", sequenceName="IDBITACORAEVENTOSINIESTRO_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;
	@Column(name = "DESCRIPCION")
	private String descripcion;
	@Column(name="EVENTO_ID")
	private String idEvento;

	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="REPORTECABINA_ID")
	private ReporteCabina reporteCabina;
	
	
	
	/**
	 * @param fechaHoraTerminacion the fechaHoraTerminacion to set
	 */
	
	/**
	 * @return the citaReporteCabina
	 */
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(String idEvento) {
		this.idEvento = idEvento;
	}
	public void setIdEvento(TipoEvento idEvento) {
		this.idEvento = idEvento.toString();
	}

	public ReporteCabina getReporteCabina() {
		return reporteCabina;
	}

	public void setReporteCabina(ReporteCabina reporteCabina) {
		this.reporteCabina = reporteCabina;
	}

//------
	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof ReporteCabina) {
			BitacoraEventoSiniestro bitacoraEventoSiniestro = (BitacoraEventoSiniestro) object;
			equal = bitacoraEventoSiniestro.getId().equals(this.id);
		}
		return equal;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	/**
	 * ENUM de tipo de evento, llenar con los valores del anexo de tipos de evento
	 */
	
   /**
	* CATALOGO DE EVENTOS 
	*	CODIGO|DESCRIPCION
	*	EVENTO_1|Emisión de Póliza
	*	EVENTO_2|Emisión de Endoso
	*	EVENTO_3|Apertura de Reporte
	*	EVENTO_4|Programación de Cita
	*	EVENTO_5|Modificación de Cita
	*	EVENTO_6|Asignación de Ajustador a reporte de siniestro
	*	EVENTO_7|Modificación de Ajustador
	*	EVENTO_8|Registro Lugar de Atención
	*	EVENTO_9|Modificación de Lugar de Atención
	*	EVENTO_10|Confirmación Lugar de Ocurrido
	*	EVENTO_11|Modificación Lugar de Ocurrido
	*	EVENTO_12|Registro de Información Adicional
	*	EVENTO_13|Asignación de Inciso a reporte de siniestro
	*	EVENTO_14|Liberación del Ajustador
	*	EVENTO_15|Registro de Condición especial.
	*	EVENTO_16|Asignación de Póliza
	*	EVENTO_17|Asignación de Solicitud de Póliza
	*	EVENTO_18|Solicitud de Afectación de Inciso
	*	EVENTO_19|Registro de Condiciones Especiales
	*	EVENTO_20|Registro de Pase de Atención DM
	*	EVENTO_21|Registro Afectación RT
	*	EVENTO_22|Registro de Pase de Atención RC Personas
	*	EVENTO_23|Registro de Pase de Atención GM
	*	EVENTO_24|Registro Afectación AJ
	*	EVENTO_25|Registro de Afectación A Viajes y Vial KM"0"
	*	EVENTO_26|Registro de Pase de Atención RC Vehículo
	*	EVENTO_27|Registro Afectación RC USA y Canada
	*	EVENTO_28|Registro Afectación RC Ex. Por Muerte
	*	EVENTO_29|Convertir a Siniestro
	*	EVENTO_30|Registro Afectación Exención de Deducible DM
	*	EVENTO_31|Registro Afectación Exención de Deducible RT
	*	EVENTO_32|Registro Afectación Protección Máxima Afirme
	*	EVENTO_33|Registro Afectación Equipo Especial
	*	EVENTO_34|Registro Afectación Adaptaciones y Conversiones
	*	EVENTO_35|Registro Afectación Accidentes Automovilísticos al Conductor
	*	EVENTO_36|Cancelar Reporte
	*	EVENTO_37|Rechazar Reporte
	*	EVENTO_38|Terminar Reporte
	*	EVENTO_39|Reaperturar Reporte
	*	EVENTO_40|Reaperturar Siniestro
	*	EVENTO_41|Depuración de Reserva
	*   EVENTO_52|Registro evento por parte del ajustador movil
	*   EVENTO_53|Se Autoriza, la rehabilitación de póliza por parte de Cobranza
	*   EVENTO_54|Se Rechaza, la rehabilitación de póliza por parte de Cobranza
	*   EVENTO_55|Se autoriza solicitud por ajuste de reserva
	*   EVENTO_56|Se rechaza solicitud por ajuste de reserva
	*/
	public enum TipoEvento {
		/**
		 * EVENTO_1|Emisión de Póliza
		 */
		EVENTO_1	,
		/**
		 * EVENTO_2|Emisión de Endoso
		 */
		EVENTO_2	,
		/**
		 * EVENTO_3|Apertura de Reporte
		 */		
		EVENTO_3	,
		/**
		 * EVENTO_4|Programación de Cita
		 */			 
		EVENTO_4	,
		/**
		 * EVENTO_5|Modificación de Cita
		 */		
		EVENTO_5	,
		/**
		 * EVENTO_6|Asignación de Ajustador a reporte de siniestro
		 */		
		EVENTO_6	,
		/**
		 * EVENTO_7|Modificación de Ajustador
		 */			
		EVENTO_7	,
		/**
		 * EVENTO_8|Registro Lugar de Atención
		 */			
		EVENTO_8	,
		/**
		 * EVENTO_9|Modificación de Lugar de Atención
		 */			
		EVENTO_9	,
		/**
		 * EVENTO_10|Confirmación Lugar de Ocurrido
		 */			
		EVENTO_10	,
		/**
		 * EVENTO_11|Modificación Lugar de Ocurrido
		 */			
		EVENTO_11	,
		/**
		 * EVENTO_12|Registro de Información Adicional
		 */			
		EVENTO_12	,
		/**
		 * EVENTO_13|Asignación de Inciso a reporte de siniestro
		 */			
		EVENTO_13	,
		/**
		 * EVENTO_14|Liberación del Ajustador
		 */			
		EVENTO_14	,
		/**
		 * EVENTO_15|Registro de Condición especial.
		 */			
		EVENTO_15	,
		/**
		 * EVENTO_16|Asignación de Póliza
		 */			
		EVENTO_16	,
		/**
		 * EVENTO_17|Asignación de Solicitud de Póliza
		 */			
		EVENTO_17	,
		/**
		 * EVENTO_18|Solicitud de Afectación de Inciso
		 */			
		EVENTO_18	,
		/**
		 * EVENTO_19|Registro de Condiciones Especiales
		 */			
		EVENTO_19	,
		/**
		 * EVENTO_20|Registro de Pase de Atención DM
		 */			
		EVENTO_20	,
		/**
		 * EVENTO_21|Registro Afectación RT
		 */			
		EVENTO_21	,
		/**
		 * EVENTO_22|Registro de Pase de Atención RC Personas
		 */			
		EVENTO_22	,
		/**
		 * EVENTO_23|Registro de Pase de Atención GM
		 */			
		EVENTO_23	,
		/**
		 * EVENTO_24|Registro Afectación AJ
		 */			
		EVENTO_24	,
		/**
		 * EVENTO_25|Registro de Afectación A Viajes y Vial KM"0"
		 */			
		EVENTO_25	,
		/**
		 * EVENTO_26|Registro de Pase de Atención RC Vehículo
		 */			
		EVENTO_26	,
		/**
		 * EVENTO_27|Registro Afectación RC USA y Canada
		 */			
		EVENTO_27	,
		/**
		 * EVENTO_28|Registro Afectación RC Ex. Por Muerte
		 */			
		EVENTO_28	,
		/**
		 * EVENTO_29|Convertir a Siniestro
		 */			
		EVENTO_29	,
		/**
		 * EVENTO_30|Registro Afectación Exención de Deducible DM
		 */			
		EVENTO_30	,
		/**
		 * EVENTO_31|Registro Afectación Exención de Deducible RT
		 */			
		EVENTO_31	,
		/**
		 * EVENTO_32|Registro Afectación Protección Máxima Afirme
		 */			
		EVENTO_32	,
		/**
		 * EVENTO_33|Registro Afectación Equipo Especial
		 */			
		EVENTO_33	,
		/**
		 * EVENTO_34|Registro Afectación Adaptaciones y Conversiones
		 */			
		EVENTO_34	,
		/**
		 * EVENTO_35|Registro Afectación Accidentes Automovilísticos al Conductor
		 */			
		EVENTO_35	,
		/**
		 * 	EVENTO_46|Cancelar Reporte
		 */
		EVENTO_46,
		/**
		 * 	EVENTO_47|Rechazar Reporte
		 */
		EVENTO_47,
		/**
		 * 	EVENTO_48|Terminar Reporte
		 */
		EVENTO_48,
		/**
		 * 	EVENTO_49|Reaperturar Reporte
		 */
		EVENTO_49,
		/**
		 * 50|Reaperturar Siniestro
		 */
		EVENTO_50,
		/**
		 * 	EVENTO_51|Depuración de Reserva
         */
		EVENTO_51,
		/**
		 * EVENTO_52| Registro evento por parte del ajustador
		 */
		EVENTO_52,
		/**
		 * EVENTO_53| Se Autoriza, la rehabilitación de póliza por parte de Cobranza
		 */
		EVENTO_53,
		/**
		 * EVENTO_54| Se Rechaza, la rehabilitación de póliza por parte de Cobranza
		 */
		EVENTO_54,
		/**
		 * EVENTO_55| Se autoriza solicitud por ajuste de reserva
		 */
		EVENTO_55,
		/**
		 * EVENTO_56| Se rechaza solicitud por ajuste de reserva
		 */
		EVENTO_56
	};
	
	
}
