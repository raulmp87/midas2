package mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.domain.MidasAbstracto;

@Entity
@Table(name="TCSNCONCEPTOSGUIAS", schema="MIDAS")
public class ConceptoGuia extends MidasAbstracto {
	
	
	private static final long serialVersionUID = -6278454528322897211L;

	public enum TIPO_CONCEPTO_GUIA{REFUNIC, ACREEDORA, MANUAL, INGRESO_PROVEEDOR, INGRESO_SIPAC,
		INGRESO_DEDUCIBLE, INGRESO_COMPANIA, INGRESO_SALVAMENTO, INGRESO_CRUC_JUR, DEVOLUCION_CHEQUE};
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TCSNCONCEPTOSGUIAS_GENERATOR")
	@SequenceGenerator(name="TCSNCONCEPTOSGUIAS_GENERATOR", schema="MIDAS", sequenceName="TCSNCONCEPTOSGUIAS_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;
	
	@Column(name="CODIGO")
	private String codigo;
	
	@Column(name="DESCRIPCION")
	private String descripcion;
	
	@Column(name="ACTIVO")
	private Boolean activo = Boolean.TRUE;
	
	@Column(name="VISIBLE")
	private Boolean visible = Boolean.TRUE;
	
	@Column(name="GUIA_SEYCOS_ID")
	private Integer guiaContableSeycosId;
	
	@Column(name="CONCEPTO_GUIA_ID")
	private Integer conceptoGuiaContableId;
	
	@Column(name="CONCEPTO_CUENTA_ID")
	private Integer conceptoCuentaId;
	
	@Column(name="TIPO")
	@Enumerated(EnumType.STRING)
	private TIPO_CONCEPTO_GUIA tipo;
	
	@Column(name="SUB_TIPO")
	private String subTipo;
	
	@Column(name="NATURALEZA")
	private String naturaleza;		
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	public Integer getGuiaContableSeycosId() {
		return guiaContableSeycosId;
	}

	public void setGuiaContableSeycosId(Integer guiaContableSeycosId) {
		this.guiaContableSeycosId = guiaContableSeycosId;
	}
	
	public TIPO_CONCEPTO_GUIA getTipo() {
		return tipo;
	}

	public void setTipo(TIPO_CONCEPTO_GUIA tipo) {
		this.tipo = tipo;
	}

	public String getSubTipo() {
		return subTipo;
	}

	public void setSubTipo(String subTipo) {
		this.subTipo = subTipo;
	}

	public String getNaturaleza() {
		return naturaleza;
	}

	public void setNaturaleza(String naturaleza) {
		this.naturaleza = naturaleza;
	}
	
	public Integer getConceptoGuiaContableId() {
		return conceptoGuiaContableId;
	}

	public void setConceptoGuiaContableId(Integer conceptoGuiaContableId) {
		this.conceptoGuiaContableId = conceptoGuiaContableId;
	}

	public Integer getConceptoCuentaId() {
		return conceptoCuentaId;
	}

	public void setConceptoCuentaId(Integer conceptoCuentaId) {
		this.conceptoCuentaId = conceptoCuentaId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConceptoGuia other = (ConceptoGuia) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

}
