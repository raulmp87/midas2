package mx.com.afirme.midas2.domain.siniestros.indemnizacion;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.catalogos.EnumBase;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;

@Entity
@Table(name="TOINDEMNIZACIONSINIESTRO", schema="MIDAS")
public class IndemnizacionSiniestro extends MidasAbstracto implements Entidad{

	private static final long serialVersionUID = -3020135481272505931L;
	
	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="INDEMNIZACIONSINIESTRO_GENERATOR")
	@SequenceGenerator(name="INDEMNIZACIONSINIESTRO_GENERATOR", sequenceName="IDTOINDEMNIZACIONSINIESTRO_SEQ", schema="MIDAS", allocationSize=1)
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_PERDIDA_TOTAL")
	private Date fechaAutPerdidaTotal;
	
	@Column(name="USUARIO_PERDIDA_TOTAL")
	private String usuarioPerdidaTotal;
	
	@Column(name="ESTATUS_PERDIDA_TOTAL", nullable=false)
	private String estatusPerdidaTotal = "P";
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_INDEMNIZACION")
	private Date fechaAutIndemnizacion;
	
	@Column(name="USUARIO_INDEMNIZACION")
	private String usuarioIndemnizacion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_DETERMINACION_PT")
	private Date fechaAutDeterminacion;
	
	@Column(name="USUARIO_DETERMINACION_PT")
	private String usuarioDeterminacion;
	
	@Column(name="ESTATUS_INDEMNIZACION", nullable=false)
	private String estatusIndemnizacion = "P";
	
	@Column(name="MOTIVO_AUT_PERDIDA")
	private String motivoAutPerdidaTotal;
	
	@Column(name="MOTIVO_AUT_INDEM")
	private String motivoAutIndemnizacion;
	
	@Column(name="GUIA_NADA")
	private BigDecimal guiaNada;
	
	@Column(name="GUIA_KBB")
	private BigDecimal guiaKbb;
	
	@Column(name="GUIA_EBC")
	private BigDecimal guiaEbc;
	
	@Column(name="GUIA_AUTOMETRICA")
	private BigDecimal guiaAutometrica;
	
	@Column(name="VALOR_PERITO_EQ_PESADO")
	private BigDecimal valorPeritoEqPesado;
	
	@Column(name="VALOR_PORC_PT")
	private BigDecimal valorPorcPerdidaTotal;
	
	@Column(name="VALOR_FACTURA")
	private BigDecimal valorFactura;
	
	@Column(name="MANO_OBRA_HP")
	private BigDecimal manoObraHP;
	
	@Column(name="REF_CARROCERIA")
	private BigDecimal refaccionesCarroceria;
	
	@Column(name="MANO_OBRA_MECANICA")
	private BigDecimal manoObraMecanica;
	
	@Column(name="REF_MECANICAS")
	private BigDecimal refaccionesMecanicas;
	
	@Column(name="FALTANTES_DESARME")
	private BigDecimal faltanteDesarme;
	
	@Column(name="TOTAL_MO_PIEZAS")
	private BigDecimal totalMoPiezas;
	
	@Column(name="PORC_DANIOS_VALOR_UNIDAD")
	private BigDecimal porcentajeDanioValorUnidad;
	
	@Column(name="SUMA_GUIAS")
	private BigDecimal sumaGuias;
	
	@Column(name="NUM_GUIAS")
	private Integer numeroGuias;
	
	@Column(name="PROMEDIO_GUIAS")
	private BigDecimal promedioGuias;
	
	@Column(name="VALOR_UNIDAD")
	private BigDecimal valorUnidad;
	
	@Column(name="DEDUCIBLE")
	private BigDecimal deducible;
	
	@Column(name="SUBTOTAL_1")
	private BigDecimal subTotal;
	
	@Column(name="VALOR_EQ_ADAP")
	private BigDecimal valorEqAdaptaciones;
	
	@Column(name="DEDUCIBLE_EQ_ADAP")
	private BigDecimal deducibleEqAdaptaciones;
	
	@Column(name="PRIMAS_PENDIENTES")
	private BigDecimal primasPendientes;
	
	@Column(name="TOTAL_INDEMNIZAR")
	private BigDecimal totalIndemnizar;
	
	@Column(name="SALV_GUIA_AUTOMETRICA")
	private BigDecimal guiaAutometricaSalv;
	
	@Column(name="ULTIMA_TENENCIA")
	private BigDecimal ultimaTenencia;
	
	@Column(name="UBICACION_SALVAMENTO")
	private String ubicacionSalvamento;
	
	@Column(name="TIPO_DEPOSITO")
	private String tipoDeposito;
	
	@Column(name="COSTO_PENSION")
	private BigDecimal costoPension;
	
	@Column(name="ESTIMADO_VALUADOR")
	private BigDecimal estimadoValuador;
	
	@Column(name="SALV_SUGERIDO")
	private BigDecimal sugeridoDireccion;
	
	@Column(name="DERIVADO_AUTOPLAZO")
	private Boolean derivadoAutoplazo;
	
	@Column(name="PERS_MORAL_RENOV")
	private Boolean personaMoralRenovacion;
	
	@Column(name="PERS_FISICA_RENOV")
	private Boolean personaFisicaRenovacion;
	
	@Column(name="UNIDAD_INSPECC")
	private Boolean unidadInspecContratacion;
	
	@Column(name="POLIZA_ANTERIOR")
	private Boolean polizaAnteriorInmediata;
	
	@Column(name="SINIESTROS_ANTERIORES")
	private Boolean siniestrosAnteriores;
	
	@Column(name="OBSERVACIONES")
	private String observaciones;
	
	@Column(name="ETAPA", nullable=false)
	private String etapa;
	
	@Column(name="ES_PAGO_DANIOS")
	private Boolean esPagoDanios = Boolean.FALSE;
	
	@Column(name="TOTAL_PAGO_DANIOS")
	private BigDecimal totalPagoDanios;
	
	@Column(name="DESCUENTO")
	private BigDecimal descuento;
	
	@Column(name="NOTA_DESCUENTO")
	private String notaDescuento;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SINIESTRO", nullable=false, referencedColumnName="ID")
	private SiniestroCabina siniestro;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ORDEN_COMPRA", nullable=false, referencedColumnName="ID")
	private OrdenCompra ordenCompra;
	
	@OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ID_ORDEN_COMPRA_GENERADA", nullable=true, referencedColumnName="ID")
    private OrdenCompra ordenCompraGenerada;
	
	@OneToOne(cascade=CascadeType.ALL,fetch=FetchType.LAZY, mappedBy="indemnizacion")
    @JoinFetch(JoinFetchType.OUTER)
    private RecepcionDocumentosSiniestro recepcionDocumentosSiniestro;
	
	@OneToOne(cascade=CascadeType.ALL,fetch=FetchType.LAZY, mappedBy="indemnizacion")
    @JoinFetch(JoinFetchType.OUTER)
	private InfoContratoSiniestro contrato;
	
	public static enum EstatusAutorizacionIndemnizacion implements EnumBase<String>{
		NO_APLICA("NA"), PENDIENTE("P"), AUTORIZADA("A"), RECHAZADA("R"), CANCELADA("C"), REGISTRADA("G");
		public final String codigo;
		EstatusAutorizacionIndemnizacion(String codigo){
			this.codigo = codigo;
		}
		@Override
		public String getValue() {
			return codigo;
		}
		@Override
		public String getLabel() {
			return this.toString();
		}
	}
	
	public static enum EtapasIndemnizacion{
		AUTORIZAR_PERDIDA_TOTAL("AT"), AUTORIZAR_INDEMNIZACION("AI"), DETERMINACION("DT"), REGISTRO_DOCUMENTOS("RD"),
		ORDEN_COMPRA_GENERADA("OC");
		public final String codigo;
		EtapasIndemnizacion(String codigo){
			this.codigo = codigo;
		}
	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the fechaAutPerdidaTotal
	 */
	public Date getFechaAutPerdidaTotal() {
		return fechaAutPerdidaTotal;
	}

	/**
	 * @param fechaAutPerdidaTotal the fechaAutPerdidaTotal to set
	 */
	public void setFechaAutPerdidaTotal(Date fechaAutPerdidaTotal) {
		this.fechaAutPerdidaTotal = fechaAutPerdidaTotal;
	}

	/**
	 * @return the usuarioPerdidaTotal
	 */
	public String getUsuarioPerdidaTotal() {
		return usuarioPerdidaTotal;
	}

	/**
	 * @param usuarioPerdidaTotal the usuarioPerdidaTotal to set
	 */
	public void setUsuarioPerdidaTotal(String usuarioPerdidaTotal) {
		this.usuarioPerdidaTotal = usuarioPerdidaTotal;
	}

	/**
	 * @return the estatusPerdidaTotal
	 */
	public String getEstatusPerdidaTotal() {
		return estatusPerdidaTotal;
	}

	/**
	 * @param estatusPerdidaTotal the estatusPerdidaTotal to set
	 */
	public void setEstatusPerdidaTotal(String estatusPerdidaTotal) {
		this.estatusPerdidaTotal = estatusPerdidaTotal;
	}

	/**
	 * @return the fechaAutIndemnizacion
	 */
	public Date getFechaAutIndemnizacion() {
		return fechaAutIndemnizacion;
	}

	/**
	 * @param fechaAutIndemnizacion the fechaAutIndemnizacion to set
	 */
	public void setFechaAutIndemnizacion(Date fechaAutIndemnizacion) {
		this.fechaAutIndemnizacion = fechaAutIndemnizacion;
	}

	/**
	 * @return the usuarioIndemnizacion
	 */
	public String getUsuarioIndemnizacion() {
		return usuarioIndemnizacion;
	}

	/**
	 * @param usuarioIndemnizacion the usuarioIndemnizacion to set
	 */
	public void setUsuarioIndemnizacion(String usuarioIndemnizacion) {
		this.usuarioIndemnizacion = usuarioIndemnizacion;
	}

	/**
	 * @return the fechaAutDeterminacion
	 */
	public Date getFechaAutDeterminacion() {
		return fechaAutDeterminacion;
	}

	/**
	 * @param fechaAutDeterminacion the fechaAutDeterminacion to set
	 */
	public void setFechaAutDeterminacion(Date fechaAutDeterminacion) {
		this.fechaAutDeterminacion = fechaAutDeterminacion;
	}

	/**
	 * @return the usuarioDeterminacion
	 */
	public String getUsuarioDeterminacion() {
		return usuarioDeterminacion;
	}

	/**
	 * @param usuarioDeterminacion the usuarioDeterminacion to set
	 */
	public void setUsuarioDeterminacion(String usuarioDeterminacion) {
		this.usuarioDeterminacion = usuarioDeterminacion;
	}

	/**
	 * @return the estatusIndemnizacion
	 */
	public String getEstatusIndemnizacion() {
		return estatusIndemnizacion;
	}

	/**
	 * @param estatusIndemnizacion the estatusIndemnizacion to set
	 */
	public void setEstatusIndemnizacion(String estatusIndemnizacion) {
		this.estatusIndemnizacion = estatusIndemnizacion;
	}

	/**
	 * @return the motivoAutPerdidaTotal
	 */
	public String getMotivoAutPerdidaTotal() {
		return motivoAutPerdidaTotal;
	}

	/**
	 * @param motivoAutPerdidaTotal the motivoAutPerdidaTotal to set
	 */
	public void setMotivoAutPerdidaTotal(String motivoAutPerdidaTotal) {
		this.motivoAutPerdidaTotal = motivoAutPerdidaTotal;
	}

	/**
	 * @return the motivoAutIndemnizacion
	 */
	public String getMotivoAutIndemnizacion() {
		return motivoAutIndemnizacion;
	}

	/**
	 * @param motivoAutIndemnizacion the motivoAutIndemnizacion to set
	 */
	public void setMotivoAutIndemnizacion(String motivoAutIndemnizacion) {
		this.motivoAutIndemnizacion = motivoAutIndemnizacion;
	}

	/**
	 * @return the guiaNada
	 */
	public BigDecimal getGuiaNada() {
		return guiaNada;
	}

	/**
	 * @param guiaNada the guiaNada to set
	 */
	public void setGuiaNada(BigDecimal guiaNada) {
		this.guiaNada = guiaNada;
	}

	/**
	 * @return the guiaKbb
	 */
	public BigDecimal getGuiaKbb() {
		return guiaKbb;
	}

	/**
	 * @param guiaKbb the guiaKbb to set
	 */
	public void setGuiaKbb(BigDecimal guiaKbb) {
		this.guiaKbb = guiaKbb;
	}

	/**
	 * @return the guiaEbc
	 */
	public BigDecimal getGuiaEbc() {
		return guiaEbc;
	}

	/**
	 * @param guiaEbc the guiaEbc to set
	 */
	public void setGuiaEbc(BigDecimal guiaEbc) {
		this.guiaEbc = guiaEbc;
	}

	/**
	 * @return the guiaAutometrica
	 */
	public BigDecimal getGuiaAutometrica() {
		return guiaAutometrica;
	}

	/**
	 * @param guiaAutometrica the guiaAutometrica to set
	 */
	public void setGuiaAutometrica(BigDecimal guiaAutometrica) {
		this.guiaAutometrica = guiaAutometrica;
	}

	/**
	 * @return the valorPeritoEqPesado
	 */
	public BigDecimal getValorPeritoEqPesado() {
		return valorPeritoEqPesado;
	}

	/**
	 * @param valorPeritoEqPesado the valorPeritoEqPesado to set
	 */
	public void setValorPeritoEqPesado(BigDecimal valorPeritoEqPesado) {
		this.valorPeritoEqPesado = valorPeritoEqPesado;
	}

	/**
	 * @return the valorPorcPerdidaTotal
	 */
	public BigDecimal getValorPorcPerdidaTotal() {
		return valorPorcPerdidaTotal;
	}

	/**
	 * @param valorPorcPerdidaTotal the valorPorcPerdidaTotal to set
	 */
	public void setValorPorcPerdidaTotal(BigDecimal valorPorcPerdidaTotal) {
		this.valorPorcPerdidaTotal = valorPorcPerdidaTotal;
	}

	/**
	 * @return the manoObraHP
	 */
	public BigDecimal getManoObraHP() {
		return manoObraHP;
	}

	/**
	 * @param manoObraHP the manoObraHP to set
	 */
	public void setManoObraHP(BigDecimal manoObraHP) {
		this.manoObraHP = manoObraHP;
	}

	/**
	 * @return the refaccionesCarroceria
	 */
	public BigDecimal getRefaccionesCarroceria() {
		return refaccionesCarroceria;
	}

	/**
	 * @param refaccionesCarroceria the refaccionesCarroceria to set
	 */
	public void setRefaccionesCarroceria(BigDecimal refaccionesCarroceria) {
		this.refaccionesCarroceria = refaccionesCarroceria;
	}

	/**
	 * @return the manoObraMecanica
	 */
	public BigDecimal getManoObraMecanica() {
		return manoObraMecanica;
	}

	/**
	 * @param manoObraMecanica the manoObraMecanica to set
	 */
	public void setManoObraMecanica(BigDecimal manoObraMecanica) {
		this.manoObraMecanica = manoObraMecanica;
	}

	/**
	 * @return the refaccionesMecanicas
	 */
	public BigDecimal getRefaccionesMecanicas() {
		return refaccionesMecanicas;
	}

	/**
	 * @param refaccionesMecanicas the refaccionesMecanicas to set
	 */
	public void setRefaccionesMecanicas(BigDecimal refaccionesMecanicas) {
		this.refaccionesMecanicas = refaccionesMecanicas;
	}

	/**
	 * @return the faltanteDesarme
	 */
	public BigDecimal getFaltanteDesarme() {
		return faltanteDesarme;
	}

	/**
	 * @param faltanteDesarme the faltanteDesarme to set
	 */
	public void setFaltanteDesarme(BigDecimal faltanteDesarme) {
		this.faltanteDesarme = faltanteDesarme;
	}

	/**
	 * @return the totalMoPiezas
	 */
	public BigDecimal getTotalMoPiezas() {
		return totalMoPiezas;
	}

	/**
	 * @param totalMoPiezas the totalMoPiezas to set
	 */
	public void setTotalMoPiezas(BigDecimal totalMoPiezas) {
		this.totalMoPiezas = totalMoPiezas;
	}

	/**
	 * @return the porcentajeDanioValorUnidad
	 */
	public BigDecimal getPorcentajeDanioValorUnidad() {
		return porcentajeDanioValorUnidad;
	}

	/**
	 * @param porcentajeDanioValorUnidad the porcentajeDanioValorUnidad to set
	 */
	public void setPorcentajeDanioValorUnidad(BigDecimal porcentajeDanioValorUnidad) {
		this.porcentajeDanioValorUnidad = porcentajeDanioValorUnidad;
	}

	/**
	 * @return the sumaGuias
	 */
	public BigDecimal getSumaGuias() {
		return sumaGuias;
	}

	/**
	 * @param sumaGuias the sumaGuias to set
	 */
	public void setSumaGuias(BigDecimal sumaGuias) {
		this.sumaGuias = sumaGuias;
	}

	/**
	 * @return the numeroGuias
	 */
	public Integer getNumeroGuias() {
		return numeroGuias;
	}

	/**
	 * @param numeroGuias the numeroGuias to set
	 */
	public void setNumeroGuias(Integer numeroGuias) {
		this.numeroGuias = numeroGuias;
	}

	/**
	 * @return the promedioGuias
	 */
	public BigDecimal getPromedioGuias() {
		return promedioGuias;
	}

	/**
	 * @param promedioGuias the promedioGuias to set
	 */
	public void setPromedioGuias(BigDecimal promedioGuias) {
		this.promedioGuias = promedioGuias;
	}

	/**
	 * @return the valorUnidad
	 */
	public BigDecimal getValorUnidad() {
		return valorUnidad;
	}

	/**
	 * @param valorUnidad the valorUnidad to set
	 */
	public void setValorUnidad(BigDecimal valorUnidad) {
		this.valorUnidad = valorUnidad;
	}

	/**
	 * @return the deducible
	 */
	public BigDecimal getDeducible() {
		return deducible;
	}

	/**
	 * @param deducible the deducible to set
	 */
	public void setDeducible(BigDecimal deducible) {
		this.deducible = deducible;
	}

	/**
	 * @return the subTotal
	 */
	public BigDecimal getSubTotal() {
		return subTotal;
	}

	/**
	 * @param subTotal the subTotal to set
	 */
	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}

	/**
	 * @return the valorEqAdaptaciones
	 */
	public BigDecimal getValorEqAdaptaciones() {
		return valorEqAdaptaciones;
	}

	/**
	 * @param valorEqAdaptaciones the valorEqAdaptaciones to set
	 */
	public void setValorEqAdaptaciones(BigDecimal valorEqAdaptaciones) {
		this.valorEqAdaptaciones = valorEqAdaptaciones;
	}

	/**
	 * @return the deducibleEqAdaptaciones
	 */
	public BigDecimal getDeducibleEqAdaptaciones() {
		return deducibleEqAdaptaciones;
	}

	/**
	 * @param deducibleEqAdaptaciones the deducibleEqAdaptaciones to set
	 */
	public void setDeducibleEqAdaptaciones(BigDecimal deducibleEqAdaptaciones) {
		this.deducibleEqAdaptaciones = deducibleEqAdaptaciones;
	}

	/**
	 * @return the primasPendientes
	 */
	public BigDecimal getPrimasPendientes() {
		return primasPendientes;
	}

	/**
	 * @param primasPendientes the primasPendientes to set
	 */
	public void setPrimasPendientes(BigDecimal primasPendientes) {
		this.primasPendientes = primasPendientes;
	}

	/**
	 * @return the totalIndemnizar
	 */
	public BigDecimal getTotalIndemnizar() {
		return totalIndemnizar;
	}

	/**
	 * @param totalIndemnizar the totalIndemnizar to set
	 */
	public void setTotalIndemnizar(BigDecimal totalIndemnizar) {
		this.totalIndemnizar = totalIndemnizar;
	}

	/**
	 * @return the ultimaTenencia
	 */
	public BigDecimal getUltimaTenencia() {
		return ultimaTenencia;
	}

	/**
	 * @param ultimaTenencia the ultimaTenencia to set
	 */
	public void setUltimaTenencia(BigDecimal ultimaTenencia) {
		this.ultimaTenencia = ultimaTenencia;
	}

	/**
	 * @return the ubicacionSalvamento
	 */
	public String getUbicacionSalvamento() {
		return ubicacionSalvamento;
	}

	/**
	 * @param ubicacionSalvamento the ubicacionSalvamento to set
	 */
	public void setUbicacionSalvamento(String ubicacionSalvamento) {
		this.ubicacionSalvamento = ubicacionSalvamento;
	}

	/**
	 * @return the tipoDeposito
	 */
	public String getTipoDeposito() {
		return tipoDeposito;
	}

	/**
	 * @param tipoDeposito the tipoDeposito to set
	 */
	public void setTipoDeposito(String tipoDeposito) {
		this.tipoDeposito = tipoDeposito;
	}

	/**
	 * @return the estimadoValuador
	 */
	public BigDecimal getEstimadoValuador() {
		return estimadoValuador;
	}

	/**
	 * @param estimadoValuador the estimadoValuador to set
	 */
	public void setEstimadoValuador(BigDecimal estimadoValuador) {
		this.estimadoValuador = estimadoValuador;
	}

	/**
	 * @return the sugeridoDireccion
	 */
	public BigDecimal getSugeridoDireccion() {
		return sugeridoDireccion;
	}

	/**
	 * @param sugeridoDireccion the sugeridoDireccion to set
	 */
	public void setSugeridoDireccion(BigDecimal sugeridoDireccion) {
		this.sugeridoDireccion = sugeridoDireccion;
	}

	/**
	 * @return the derivadoAutoplazo
	 */
	public Boolean getDerivadoAutoplazo() {
		return derivadoAutoplazo;
	}

	/**
	 * @param derivadoAutoplazo the derivadoAutoplazo to set
	 */
	public void setDerivadoAutoplazo(Boolean derivadoAutoplazo) {
		this.derivadoAutoplazo = derivadoAutoplazo;
	}

	/**
	 * @return the personaMoralRenovacion
	 */
	public Boolean getPersonaMoralRenovacion() {
		return personaMoralRenovacion;
	}

	/**
	 * @param personaMoralRenovacion the personaMoralRenovacion to set
	 */
	public void setPersonaMoralRenovacion(Boolean personaMoralRenovacion) {
		this.personaMoralRenovacion = personaMoralRenovacion;
	}

	/**
	 * @return the personaFisicaRenovacion
	 */
	public Boolean getPersonaFisicaRenovacion() {
		return personaFisicaRenovacion;
	}

	/**
	 * @param personaFisicaRenovacion the personaFisicaRenovacion to set
	 */
	public void setPersonaFisicaRenovacion(Boolean personaFisicaRenovacion) {
		this.personaFisicaRenovacion = personaFisicaRenovacion;
	}

	/**
	 * @return the unidadInspecContratacion
	 */
	public Boolean getUnidadInspecContratacion() {
		return unidadInspecContratacion;
	}

	/**
	 * @param unidadInspecContratacion the unidadInspecContratacion to set
	 */
	public void setUnidadInspecContratacion(Boolean unidadInspecContratacion) {
		this.unidadInspecContratacion = unidadInspecContratacion;
	}

	/**
	 * @return the polizaAnteriorInmediata
	 */
	public Boolean getPolizaAnteriorInmediata() {
		return polizaAnteriorInmediata;
	}

	/**
	 * @param polizaAnteriorInmediata the polizaAnteriorInmediata to set
	 */
	public void setPolizaAnteriorInmediata(Boolean polizaAnteriorInmediata) {
		this.polizaAnteriorInmediata = polizaAnteriorInmediata;
	}

	/**
	 * @return the siniestrosAnteriores
	 */
	public Boolean getSiniestrosAnteriores() {
		return siniestrosAnteriores;
	}

	/**
	 * @param siniestrosAnteriores the siniestrosAnteriores to set
	 */
	public void setSiniestrosAnteriores(Boolean siniestrosAnteriores) {
		this.siniestrosAnteriores = siniestrosAnteriores;
	}

	/**
	 * @return the observaciones
	 */
	public String getObservaciones() {
		return observaciones;
	}

	/**
	 * @param observaciones the observaciones to set
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	/**
	 * @return the etapa
	 */
	public String getEtapa() {
		return etapa;
	}

	/**
	 * @param etapa the etapa to set
	 */
	public void setEtapa(String etapa) {
		this.etapa = etapa;
	}

	/**
	 * @return the siniestro
	 */
	public SiniestroCabina getSiniestro() {
		return siniestro;
	}

	/**
	 * @param siniestro the siniestro to set
	 */
	public void setSiniestro(SiniestroCabina siniestro) {
		this.siniestro = siniestro;
	}

	/**
	 * @return the ordenCompra
	 */
	public OrdenCompra getOrdenCompra() {
		return ordenCompra;
	}

	/**
	 * @param ordenCompra the ordenCompra to set
	 */
	public void setOrdenCompra(OrdenCompra ordenCompra) {
		this.ordenCompra = ordenCompra;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	/**
	 * @return the esPagoDanios
	 */
	public Boolean getEsPagoDanios() {
		return esPagoDanios;
	}

	/**
	 * @param esPagoDanios the esPagoDanios to set
	 */
	public void setEsPagoDanios(Boolean esPagoDanios) {
		this.esPagoDanios = esPagoDanios;
	}

	/**
	 * @return the totalPagoDanios
	 */
	public BigDecimal getTotalPagoDanios() {
		return totalPagoDanios;
	}

	/**
	 * @param totalPagoDanios the totalPagoDanios to set
	 */
	public void setTotalPagoDanios(BigDecimal totalPagoDanios) {
		this.totalPagoDanios = totalPagoDanios;
	}

	/**
	 * @return the guiaAutometricaSalv
	 */
	public BigDecimal getGuiaAutometricaSalv() {
		return guiaAutometricaSalv;
	}

	/**
	 * @param guiaAutometricaSalv the guiaAutometricaSalv to set
	 */
	public void setGuiaAutometricaSalv(BigDecimal guiaAutometricaSalv) {
		this.guiaAutometricaSalv = guiaAutometricaSalv;
	}

	/**
	 * @return the costoPension
	 */
	public BigDecimal getCostoPension() {
		return costoPension;
	}

	/**
	 * @param costoPension the costoPension to set
	 */
	public void setCostoPension(BigDecimal costoPension) {
		this.costoPension = costoPension;
	}

	/**
	 * @return the descuento
	 */
	public BigDecimal getDescuento() {
		return descuento;
	}

	/**
	 * @param descuento the descuento to set
	 */
	public void setDescuento(BigDecimal descuento) {
		this.descuento = descuento;
	}

	/**
	 * @return the notaDescuento
	 */
	public String getNotaDescuento() {
		return notaDescuento;
	}

	/**
	 * @param notaDescuento the notaDescuento to set
	 */
	public void setNotaDescuento(String notaDescuento) {
		this.notaDescuento = notaDescuento;
	}

	/**
	 * @return the valorFactura
	 */
	public BigDecimal getValorFactura() {
		return valorFactura;
	}

	/**
	 * @param valorFactura the valorFactura to set
	 */
	public void setValorFactura(BigDecimal valorFactura) {
		this.valorFactura = valorFactura;
	}

	/**
	 * @return the ordenCompraGenerada
	 */
	public OrdenCompra getOrdenCompraGenerada() {
		return ordenCompraGenerada;
	}

	/**
	 * @param ordenCompraGenerada the ordenCompraGenerada to set
	 */
	public void setOrdenCompraGenerada(OrdenCompra ordenCompraGenerada) {
		this.ordenCompraGenerada = ordenCompraGenerada;
	}

	public RecepcionDocumentosSiniestro getRecepcionDocumentosSiniestro() {
		return recepcionDocumentosSiniestro;
	}

	public void setRecepcionDocumentosSiniestro(
			RecepcionDocumentosSiniestro recepcionDocumentosSiniestro) {
		this.recepcionDocumentosSiniestro = recepcionDocumentosSiniestro;
	}

	/**
	 * @return the contrato
	 */
	public InfoContratoSiniestro getContrato() {
		return contrato;
	}

	/**
	 * @param contrato the contrato to set
	 */
	public void setContrato(InfoContratoSiniestro contrato) {
		this.contrato = contrato;
	}
	
}