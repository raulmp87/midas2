package mx.com.afirme.midas2.domain.siniestros.recuperacion;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.domain.personadireccion.CiudadMidas;
import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.LiquidacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;


/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 19-may-2015 09:15:23 a.m.
 */
	
	@Entity(name = "RecuperacionProveedor")
	@Table(name = "TOSNRECUPERACIONPROVEEDOR", schema = "MIDAS")
	@DiscriminatorValue("PRV")
	@PrimaryKeyJoinColumn(name="RECUPERACION_ID", referencedColumnName="ID")
	
public class RecuperacionProveedor extends Recuperacion {
		
		
	//@Column	
		

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name = "ADMINREFACCIONES")
	private String adminRefacciones;
	@Column(name = "COMENTARIOSVENTA")
	private String comentariosVenta;
	@Column(name = "CONCEPTODEVOLUCION")
	private String conceptoDevolucion;
	@Column(name = "CORREO_COMPRADOR")
	private String correoComprador;
	@Column(name = "CORREO_PROVEEDOR")
	private String correoProveedor;
	@Column(name = "DESCRIPCIONVEHICULO")
	private String descripcionVehiculo;
	@Column(name = "ESREFACCION")
	private Boolean esRefaccion;
	@Column(name = "ESTATUSPREVENTA")
	private String estatusPreventa;
	@Column(name = "ESTILODESC")
	private String estiloDesc;
	
	
	@Column(name = "NUMERO_VALUACION")
	private Long numeroValuacion ;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAREGISTROVENTA")
	private Date fechaRegistroVenta;
	
	@Column(name = "MARCADESC")
	private String marcaDesc;
	
	
	
	@Column(name = "MODELOVEHICULO")
	private Integer modeloVehiculo;
	@Column(name = "MOTIVODEVOLUCION")
	private String motivoDevolucion;
	@Column(name = "NOMBRETALLER")
	private String nombreTaller;
	@Column(name = "NOMBREVALUADOR")
	private String nombreValuador;
	
	@Column(name = "OTROMOTIVODEVOLUCION")
	private String otroMotivoDevolucion;
	@Column(name = "PERSONADEVOLUCION")
	private String personaDevolucion;
	@Column(name = "PERSONAENTREGAREFACCION")
	private String personaEntregaRefaccion;
	@Column(name = "PERSONARECOGEREFACCION")
	private String personaRecogeRefaccion;
	@Column(name = "TELEFONOCOMPRADOR")
	private Long telefonoComprador;
	@Column(name = "UBICACIONVENTA")
	private String ubicacionVenta;
	
	@Column(name = "MONTOTOTAL")
	private BigDecimal montoTotal;
	@Column(name = "ISR")
	private BigDecimal isr;
	@Column(name = "IVA")
	private BigDecimal iva;
	@Column(name = "SUBTOTAL")
	private BigDecimal subTotal;
	@Column(name = "IVARETENIDO")
	private BigDecimal ivaRetenido;
	@Column(name = "MONTOVENTA")
	private BigDecimal montoVenta;	
	@Column(name = "IVAVENTA")
	private BigDecimal ivaVenta;	
	@Column(name = "SUBTOTALVENTA")
	private BigDecimal subTotalVenta;
	



	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CIUDADVENTA_ID", referencedColumnName = "CITY_ID")
	private CiudadMidas  ciudadVenta;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ESTADOVENTA_ID", referencedColumnName = "STATE_ID")
	private EstadoMidas estadoVenta;	
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPRADOR_ID", referencedColumnName = "ID")
	private PrestadorServicio comprador;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ORDENCOMPRA_ID", referencedColumnName = "ID")
	private OrdenCompra ordenCompra;
	

	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "TRSNRECUPERNOTAS", schema="MIDAS", 
			joinColumns = {@JoinColumn(name="ID_RECUPERACION", referencedColumnName="ID")},
			inverseJoinColumns = {@JoinColumn(name="ID_NOTA_CREDITO", referencedColumnName="ID")}
	)
	private List<DocumentoFiscal> notasdeCredito;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinTable(name = "TRSNLIQUIDACIONRECUPERACION", schema = "MIDAS",
			joinColumns = {@JoinColumn(name="RECUPERACION_ID", referencedColumnName="ID")},
			inverseJoinColumns = {@JoinColumn(name="LIQUIDACION_ID", referencedColumnName="ID")}
	)
	private LiquidacionSiniestro liquidacion;
	
	public RecuperacionProveedor()
	{
		super();
	}
	
	public RecuperacionProveedor(Long id, Long numero, String estatus, Date fechaCreacion, BigDecimal iva,
			BigDecimal ivaRetenido, BigDecimal isr, BigDecimal subTotal, BigDecimal montoTotal)
	{
		super();
		this.id = id;
		this.numero = numero;
		this.estatus = estatus;
		this.fechaCreacion = fechaCreacion;
		this.iva = iva;
		this.ivaRetenido = ivaRetenido;
		this.isr = isr;
		this.subTotal = subTotal;
		this.montoTotal = montoTotal;		
	}


	public String getAdminRefacciones() {
		return adminRefacciones;
	}


	public void setAdminRefacciones(String adminRefacciones) {
		this.adminRefacciones = adminRefacciones;
	}


	public String getComentariosVenta() {
		return comentariosVenta;
	}


	public void setComentariosVenta(String comentariosVenta) {
		this.comentariosVenta = comentariosVenta;
	}


	public String getConceptoDevolucion() {
		return conceptoDevolucion;
	}


	public void setConceptoDevolucion(String conceptoDevolucion) {
		this.conceptoDevolucion = conceptoDevolucion;
	}


	public String getCorreoComprador() {
		return correoComprador;
	}


	public void setCorreoComprador(String correoComprador) {
		this.correoComprador = correoComprador;
	}


	public String getCorreoProveedor() {
		return correoProveedor;
	}


	public void setCorreoProveedor(String correoProveedor) {
		this.correoProveedor = correoProveedor;
	}


	public String getDescripcionVehiculo() {
		return descripcionVehiculo;
	}


	public void setDescripcionVehiculo(String descripcionVehiculo) {
		this.descripcionVehiculo = descripcionVehiculo;
	}


	public Boolean getEsRefaccion() {
		return esRefaccion;
	}


	public void setEsRefaccion(Boolean esRefaccion) {
		this.esRefaccion = esRefaccion;
	}


	public String getEstatusPreventa() {
		return estatusPreventa;
	}


	public void setEstatusPreventa(String estatusPreventa) {
		this.estatusPreventa = estatusPreventa;
	}


	public String getEstiloDesc() {
		return estiloDesc;
	}


	public void setEstiloDesc(String estiloDesc) {
		this.estiloDesc = estiloDesc;
	}


	public Date getFechaRegistroVenta() {
		return fechaRegistroVenta;
	}


	public void setFechaRegistroVenta(Date fechaRegistroVenta) {
		this.fechaRegistroVenta = fechaRegistroVenta;
	}


	public String getMarcaDesc() {
		return marcaDesc;
	}


	public void setMarcaDesc(String marcaDesc) {
		this.marcaDesc = marcaDesc;
	}


	public Integer getModeloVehiculo() {
		return modeloVehiculo;
	}


	public void setModeloVehiculo(Integer modeloVehiculo) {
		this.modeloVehiculo = modeloVehiculo;
	}


	public String getMotivoDevolucion() {
		return motivoDevolucion;
	}


	public void setMotivoDevolucion(String motivoDevolucion) {
		this.motivoDevolucion = motivoDevolucion;
	}


	public String getNombreTaller() {
		return nombreTaller;
	}


	public void setNombreTaller(String nombreTaller) {
		this.nombreTaller = nombreTaller;
	}


	public String getNombreValuador() {
		return nombreValuador;
	}


	public void setNombreValuador(String nombreValuador) {
		this.nombreValuador = nombreValuador;
	}


	public String getOtroMotivoDevolucion() {
		return otroMotivoDevolucion;
	}


	public void setOtroMotivoDevolucion(String otroMotivoDevolucion) {
		this.otroMotivoDevolucion = otroMotivoDevolucion;
	}


	public String getPersonaDevolucion() {
		return personaDevolucion;
	}


	public void setPersonaDevolucion(String personaDevolucion) {
		this.personaDevolucion = personaDevolucion;
	}


	public String getPersonaEntregaRefaccion() {
		return personaEntregaRefaccion;
	}


	public void setPersonaEntregaRefaccion(String personaEntregaRefaccion) {
		this.personaEntregaRefaccion = personaEntregaRefaccion;
	}


	public String getPersonaRecogeRefaccion() {
		return personaRecogeRefaccion;
	}


	public void setPersonaRecogeRefaccion(String personaRecogeRefaccion) {
		this.personaRecogeRefaccion = personaRecogeRefaccion;
	}


	public Long getTelefonoComprador() {
		return telefonoComprador;
	}


	public void setTelefonoComprador(Long telefonoComprador) {
		this.telefonoComprador = telefonoComprador;
	}


	public String getUbicacionVenta() {
		return ubicacionVenta;
	}


	public void setUbicacionVenta(String ubicacionVenta) {
		this.ubicacionVenta = ubicacionVenta;
	}


	public BigDecimal getMontoTotal() {
		return montoTotal;
	}


	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}


	public BigDecimal getIsr() {
		return isr;
	}


	public void setIsr(BigDecimal isr) {
		this.isr = isr;
	}


	public BigDecimal getIva() {
		return iva;
	}


	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}


	public BigDecimal getSubTotal() {
		return subTotal;
	}


	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}


	public BigDecimal getIvaRetenido() {
		return ivaRetenido;
	}


	public void setIvaRetenido(BigDecimal ivaRetenido) {
		this.ivaRetenido = ivaRetenido;
	}


	public BigDecimal getMontoVenta() {
		return montoVenta;
	}


	public void setMontoVenta(BigDecimal montoVenta) {
		this.montoVenta = montoVenta;
	}


	public BigDecimal getIvaVenta() {
		return ivaVenta;
	}


	public void setIvaVenta(BigDecimal ivaVenta) {
		this.ivaVenta = ivaVenta;
	}


	public BigDecimal getSubTotalVenta() {
		return subTotalVenta;
	}


	public void setSubTotalVenta(BigDecimal subTotalVenta) {
		this.subTotalVenta = subTotalVenta;
	}


	public CiudadMidas getCiudadVenta() {
		return ciudadVenta;
	}


	public void setCiudadVenta(CiudadMidas ciudadVenta) {
		this.ciudadVenta = ciudadVenta;
	}


	public EstadoMidas getEstadoVenta() {
		return estadoVenta;
	}


	public void setEstadoVenta(EstadoMidas estadoVenta) {
		this.estadoVenta = estadoVenta;
	}


	public PrestadorServicio getComprador() {
		return comprador;
	}


	public void setComprador(PrestadorServicio comprador) {
		this.comprador = comprador;
	}


	public OrdenCompra getOrdenCompra() {
		return ordenCompra;
	}


	public void setOrdenCompra(OrdenCompra ordenCompra) {
		this.ordenCompra = ordenCompra;
	}


	public List<DocumentoFiscal> getNotasdeCredito() {
		return notasdeCredito;
	}


	public void setNotasdeCredito(List<DocumentoFiscal> notasdeCredito) {
		this.notasdeCredito = notasdeCredito;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public Long getNumeroValuacion() {
		return numeroValuacion;
	}


	public void setNumeroValuacion(Long numeroValuacion) {
		this.numeroValuacion = numeroValuacion;
	}

	/**
	 * @return the liquidacion
	 */
	public LiquidacionSiniestro getLiquidacion() {
		return liquidacion;
	}

	/**
	 * @param liquidacion the liquidacion to set
	 */
	public void setLiquidacion(LiquidacionSiniestro liquidacion) {
		this.liquidacion = liquidacion;
	}
	
}