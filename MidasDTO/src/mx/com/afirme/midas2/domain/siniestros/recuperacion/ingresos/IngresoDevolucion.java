package mx.com.afirme.midas2.domain.siniestros.recuperacion.ingresos;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Ingreso;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza.ConceptoGuia;
import mx.com.afirme.midas2.domain.siniestros.solicitudcheque.SolicitudChequeSiniestro;

@Entity(name = "IngresoDevolucion")
@Table(name = "TOSNINGRESODEVOLUCION", schema = "MIDAS")
public class IngresoDevolucion extends MidasAbstracto{

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -8901914106909187844L;
	public static enum TipoDevolucion{P, T};
	public static enum Estatus{S, PA, A, P, C};
	public static enum FormaPago{CHQ, TRNBANC};
	
	@Id
	@SequenceGenerator(name = "IDTOSNINGRESODEVOLUCION_SEQ_GENERADOR",allocationSize = 1, sequenceName = "IDTOSNINGRESODEVOLUCION_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "IDTOSNINGRESODEVOLUCION_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id;
	
	@Column(name= "SINIESTRO_ORIGEN", nullable = false)
	private String siniestroOrigen;
	
	@Column(name= "TIPO_DEVOLUCION", nullable = false)
	@Enumerated(EnumType.STRING)
	private TipoDevolucion tipoDevolucion;
	
	@Column(name= "FORMA_PAGO", nullable = true)
	@Enumerated(EnumType.STRING)
	private FormaPago formaPago;
	
	@Column(name= "ESTATUS", nullable = false)
	@Enumerated(EnumType.STRING)
	private Estatus estatus;
	
	@Column(name= "USUARIO_SOLICITA", nullable = true)
	private String usuarioSolicita;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name= "FECHA_SOLICITA", nullable = true)
	private Date fechaSolicita;
	
	@Column(name= "USUARIO_AUTORIZARECHAZA", nullable = true)
	private String usuarioAutorizaRechaza;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name= "FECHA_AUTORIZARECHAZA", nullable = true)
	private Date fechaAutorizaRechaza;
	
	@Column(name= "CHEQUE_REF", nullable = true)
	private String chequeReferencia;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name= "FECHA_CHEQUE_REF", nullable = true)
	private Date fechaChequeReferencia;
	
	@Column(name= "BENEFICIARIO", nullable = false)
	private String beneficiario;
	
	@Column(name= "LADA_TELEFONO")
	private String ladaTelefono;
	
	@Column(name= "TELEFONO")
	private String telefono;
	
	@Column(name= "CORREO")
	private String correo;
	
	@Column(name= "BANCO")
	private Integer banco;
	
	@Column(name= "CLABE")
	private String clabe;
	
	@Column(name= "OBSERVACIONES")
	private String observaciones;
	
	@Column(name= "SUBTOTAL")
	private BigDecimal subtotal;
	
	@Column(name= "IVA")
	private BigDecimal iva;
	
	@Column(name= "IVA_RETENIDO")
	private BigDecimal ivaRetenido;
	
	@Column(name= "ISR")
	private BigDecimal isr;
	
	@Column(name= "IMPORTE_TOTAL")
	private BigDecimal importeTotal;

	@Column(name= "MONTO_RESTANTE")
	private BigDecimal montoRestante;

	@Column(name= "IMPORTE_DEVOLUCION")
	private BigDecimal importeDevolucion;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="INGRESO_ID", nullable=true, referencedColumnName="ID")
	private Ingreso ingreso;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="FACTURA_ID", nullable=true, referencedColumnName="ID")
	private DocumentoFiscal factura;
	
	@OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="SOLICITUDCHEQUE_ID", nullable=true, referencedColumnName="ID")
	private SolicitudChequeSiniestro solicitudCheque;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CUENTA_ACREEDORA", nullable=true, referencedColumnName="ID")
	private ConceptoGuia cuentaAcreedora;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CUENTA_ACREEDORA_RESTANTE", nullable=true, referencedColumnName="ID")
	private ConceptoGuia cuentaAcreedoraRestante;
	
	public IngresoDevolucion(){

	}
	
	
	//Transients
	@Transient
	private BigDecimal subtotalRec;
	
	@Transient
	private BigDecimal ivaRec;
	
	@Transient
	private BigDecimal ivaRetenidoRec;
	
	@Transient
	private BigDecimal isrRec;
	
	@Transient
	private BigDecimal importeTotalRec;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Ingreso getIngreso() {
		return ingreso;
	}
	public void setIngreso(Ingreso ingreso) {
		this.ingreso = ingreso;
	}
	public ConceptoGuia getCuentaAcreedora() {
		return cuentaAcreedora;
	}
	public void setCuentaAcreedora(ConceptoGuia cuentaAcreedora) {
		this.cuentaAcreedora = cuentaAcreedora;
	}
	public String getSiniestroOrigen() {
		return siniestroOrigen;
	}
	public void setSiniestroOrigen(String siniestroOrigen) {
		this.siniestroOrigen = siniestroOrigen;
	}
	public TipoDevolucion getTipoDevolucion() {
		return tipoDevolucion;
	}
	public void setTipoDevolucion(TipoDevolucion tipoDevolucion) {
		this.tipoDevolucion = tipoDevolucion;
	}
	public FormaPago getFormaPago() {
		return formaPago;
	}
	public void setFormaPago(FormaPago formaPago) {
		this.formaPago = formaPago;
	}
	public Estatus getEstatus() {
		return estatus;
	}
	public void setEstatus(Estatus estatus) {
		this.estatus = estatus;
	}
	public SolicitudChequeSiniestro getSolicitudCheque() {
		return solicitudCheque;
	}
	public void setSolicitudCheque(SolicitudChequeSiniestro solicitudCheque) {
		this.solicitudCheque = solicitudCheque;
	}
	public String getUsuarioSolicita() {
		return usuarioSolicita;
	}
	public void setUsuarioSolicita(String usuarioSolicita) {
		this.usuarioSolicita = usuarioSolicita;
	}
	public Date getFechaSolicita() {
		return fechaSolicita;
	}
	public void setFechaSolicita(Date fechaSolicita) {
		this.fechaSolicita = fechaSolicita;
	}
	public String getUsuarioAutorizaRechaza() {
		return usuarioAutorizaRechaza;
	}
	public void setUsuarioAutorizaRechaza(String usuarioAutorizaRechaza) {
		this.usuarioAutorizaRechaza = usuarioAutorizaRechaza;
	}
	public Date getFechaAutorizaRechaza() {
		return fechaAutorizaRechaza;
	}
	public void setFechaAutorizaRechaza(Date fechaAutorizaRechaza) {
		this.fechaAutorizaRechaza = fechaAutorizaRechaza;
	}
	public String getChequeReferencia() {
		return chequeReferencia;
	}
	public void setChequeReferencia(String chequeReferencia) {
		this.chequeReferencia = chequeReferencia;
	}
	public Date getFechaChequeReferencia() {
		return fechaChequeReferencia;
	}
	public void setFechaChequeReferencia(Date fechaChequeReferencia) {
		this.fechaChequeReferencia = fechaChequeReferencia;
	}
	public String getBeneficiario() {
		return beneficiario;
	}
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}
	public String getLadaTelefono() {
		return ladaTelefono;
	}
	public void setLadaTelefono(String ladaTelefono) {
		this.ladaTelefono = ladaTelefono;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public Integer getBanco() {
		return banco;
	}
	public void setBanco(Integer banco) {
		this.banco = banco;
	}
	public String getClabe() {
		return clabe;
	}
	public void setClabe(String clabe) {
		this.clabe = clabe;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public DocumentoFiscal getFactura() {
		return factura;
	}
	public void setFactura(DocumentoFiscal factura) {
		this.factura = factura;
	}
	public BigDecimal getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}
	public BigDecimal getIva() {
		return iva;
	}
	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}
	public BigDecimal getIvaRetenido() {
		return ivaRetenido;
	}
	public void setIvaRetenido(BigDecimal ivaRetenido) {
		this.ivaRetenido = ivaRetenido;
	}
	public BigDecimal getIsr() {
		return isr;
	}
	public void setIsr(BigDecimal isr) {
		this.isr = isr;
	}
	public BigDecimal getImporteTotal() {
		return importeTotal;
	}
	public void setImporteTotal(BigDecimal importeTotal) {
		this.importeTotal = importeTotal;
	}

	public BigDecimal getMontoRestante() {
		return montoRestante;
	}

	public void setMontoRestante(BigDecimal montoRestante) {
		this.montoRestante = montoRestante;
	}

	public BigDecimal getImporteDevolucion() {
		return importeDevolucion;
	}

	public void setImporteDevolucion(BigDecimal importeDevolucion) {
		this.importeDevolucion = importeDevolucion;
	}

	public BigDecimal getSubtotalRec() {
		return subtotalRec;
	}
	public void setSubtotalRec(BigDecimal subtotalRec) {
		this.subtotalRec = subtotalRec;
	}
	public BigDecimal getIvaRec() {
		return ivaRec;
	}
	public void setIvaRec(BigDecimal ivaRec) {
		this.ivaRec = ivaRec;
	}
	public BigDecimal getIvaRetenidoRec() {
		return ivaRetenidoRec;
	}
	public void setIvaRetenidoRec(BigDecimal ivaRetenidoRec) {
		this.ivaRetenidoRec = ivaRetenidoRec;
	}
	public BigDecimal getIsrRec() {
		return isrRec;
	}
	public void setIsrRec(BigDecimal isrRec) {
		this.isrRec = isrRec;
	}
	public BigDecimal getImporteTotalRec() {
		return importeTotalRec;
	}
	public void setImporteTotalRec(BigDecimal importeTotalRec) {
		this.importeTotalRec = importeTotalRec;
	}
	
	public ConceptoGuia getCuentaAcreedoraRestante() {
		return cuentaAcreedoraRestante;
	}
	public void setCuentaAcreedoraRestante(ConceptoGuia cuentaAcreedoraRestante) {
		this.cuentaAcreedoraRestante = cuentaAcreedoraRestante;
	}
	@Override
	@SuppressWarnings("unchecked")
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Long getBusinessKey() {
		return id;
	}
	@Override
	public String toString() {
		return "IngresoDevolucion [id=" + id + ", cuentaAcreedora=" + cuentaAcreedora + ", siniestroOrigen=" + siniestroOrigen
				+ ", tipoDevolucion=" + tipoDevolucion + ", formaPago=" + formaPago + ", estatus=" + estatus + ", usuarioSolicita="
				+ usuarioSolicita + ", fechaSolicita=" + fechaSolicita + ", usuarioAutorizaRechaza=" + usuarioAutorizaRechaza
				+ ", fechaAutorizaRechaza=" + fechaAutorizaRechaza + ", chequeReferencia=" + chequeReferencia + ", fechaChequeReferencia="
				+ fechaChequeReferencia + ", beneficiario=" + beneficiario + ", ladaTelefono=" + ladaTelefono + ", telefono=" + telefono
				+ ", correo=" + correo + ", banco=" + banco + ", clabe=" + clabe + ", observaciones=" + observaciones + ", subtotal="
				+ subtotal + ", iva=" + iva + ", ivaRetenido=" + ivaRetenido + ", isr=" + isr + ", importeTotal=" + importeTotal
				+ ", montoRestante=" + montoRestante + ", importeDevolucion=" + importeDevolucion + ", ingreso=" + ingreso + ", factura="
				+ factura + ", solicitudCheque=" + solicitudCheque + ", subtotalRec=" + subtotalRec + ", ivaRec=" + ivaRec
				+ ", ivaRetenidoRec=" + ivaRetenidoRec + ", isrRec=" + isrRec + ", importeTotalRec=" + importeTotalRec + "]";
	}

}
