package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.Coordenadas;
import mx.com.afirme.midas2.domain.personadireccion.CiudadMidas;
import mx.com.afirme.midas2.domain.personadireccion.ColoniaMidas;
import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;
import mx.com.afirme.midas2.domain.personadireccion.PaisMidas;
import mx.com.afirme.midas2.util.StringUtil;

import org.apache.log4j.Logger;
import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

@Entity
@Table(name = "TOLUGARMIDAS", schema = "MIDAS")
@Access(AccessType.PROPERTY)
public class LugarSiniestroMidas implements Entidad {
	
	public static final Logger log = Logger.getLogger(LugarSiniestroMidas.class);

	private static final long serialVersionUID = -2184590980336098792L;
	
	public static enum TipoLugar{
		AT,OC
	}
	
	public static enum TipoDireccion{
		CA, CD
	}
	
	@Access(AccessType.FIELD)
	@Id
	@Column(name="ID",nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="LUGARMIDAS_GENERATOR")	
	@SequenceGenerator(name="LUGARMIDAS_GENERATOR", sequenceName="IDTOLUGARMIDAS_SEQ", schema="MIDAS", allocationSize=1)
	private Long id;
	
	@Access(AccessType.FIELD)
	@Pattern(regexp=".{0,100}", message="Calle demasiado largo")
	@Column(name = "CALLE_NUMERO", length = 100)
    private String calleNumero;
	
	@Access(AccessType.FIELD)
	@Pattern(regexp="(^$|\\d{5,6})", message="El codigo postal no es valido")
	@Column(name = "CODIGO_POSTAL", length = 6)
    private String codigoPostal;
	
	@Access(AccessType.FIELD)
	@Pattern(regexp="(^$|\\d{5,6})", message="El codigo postal no es valido")
	@Column(name = "CODIGO_POSTAL_OTRA_COLONIA", length = 6)
    private String codigoPostalOtraColonia;
	
	@Access(AccessType.FIELD)
	@Pattern(regexp=".{0,20}", message="Campo demasiado largo")
	@Column(name = "KILOMETRO")
    private String kilometro;
	
	@Access(AccessType.FIELD)
	@Pattern(regexp=".{0,100}", message="Nombre demasiado largo")
	@Column(name = "NOMBRE_CARRETERA", length = 100)
    private String nombreCarretera;
	
	@Access(AccessType.FIELD)
	@Pattern(regexp=".{0,50}", message="Colonia demasiado larga")
	@Column(name = "OTRA_COLONIA", length = 50)
    private String otraColonia;
	
	@Access(AccessType.FIELD)
	//@Pattern(regexp="{0,200}", message="Referencia demasiado larga")
	@Size(max=200, message="Referencia demasiado larga")
	@Column(name = "REFERENCIA", length = 200)
    private String referencia;
	
	@Access(AccessType.FIELD)
	@Pattern(regexp="(^$|AT|OC)", message="El tipo no es valido")
	@Column(name = "TIPO", nullable = false, length = 2)
    private String tipo;
	
	@Access(AccessType.FIELD)
	@Pattern(regexp="(^$|LI|CU)", message="El tipo no es valido")
	@Column(name = "TIPO_CARRETERA", length = 2)
    private String tipoCarretera;
	
	@Access(AccessType.FIELD)
	@NotNull
	@Size(min=2, max=2, message="Es necesario seleccionar este campo")
	@Column(name = "ZONA", nullable = false, length = 2)
    private String zona;
	
	@Access(AccessType.FIELD)
	@NotNull
	@KeyNotEmpty
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CIUDAD_ID", nullable=false, referencedColumnName="CITY_ID")
    private CiudadMidas ciudad;
	
	@Access(AccessType.FIELD)
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch(JoinFetchType.OUTER)
	@JoinColumn(name="COLONIA_ID", referencedColumnName="COLONY_ID")
    private ColoniaMidas colonia;
	
	@Access(AccessType.FIELD)
	@NotNull
	@KeyNotEmpty
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ESTADO_ID", nullable=false, referencedColumnName="STATE_ID")
    private EstadoMidas estado;
	
	@Access(AccessType.FIELD)
	@NotNull
	@KeyNotEmpty
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PAIS_ID", nullable=false, referencedColumnName="COUNTRY_ID")
    private PaisMidas pais;
	
	private Coordenadas coordenadas;
	
	private Boolean idColoniaCheck;
	
	/**
	 * Coordenadas de contacto de un ajustador. Se integro este atributo para interfaz con movil.
	 */
	private Coordenadas coordenadasContacto;
	
	@Access(AccessType.FIELD)
	@Column(name = "FECHA_COORD_CONTACTO", length = 2)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCoordenadasContacto;
    
    /**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the calleNumero
	 */
	public String getCalleNumero() {
		return calleNumero;
	}

	/**
	 * @param calleNumero the calleNumero to set
	 */
	public void setCalleNumero(String calleNumero) {
		this.calleNumero = calleNumero;
	}

	/**
	 * @return the codigoPostal
	 */
	public String getCodigoPostal() {
		return codigoPostal;
	}

	/**
	 * @param codigoPostal the codigoPostal to set
	 */
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	/**
	 * @return the codigoPostalOtraColonia
	 */
	public String getCodigoPostalOtraColonia() {
		return codigoPostalOtraColonia;
	}

	/**
	 * @param codigoPostalOtraColonia the codigoPostalOtraColonia to set
	 */
	public void setCodigoPostalOtraColonia(String codigoPostalOtraColonia) {
		this.codigoPostalOtraColonia = codigoPostalOtraColonia;
	}

	/**
	 * @return the kilometro
	 */
	public String getKilometro() {
		return kilometro;
	}

	/**
	 * @param kilometro the kilometro to set
	 */
	public void setKilometro(String kilometro) {
		this.kilometro = kilometro;
	}

	/**
	 * @return the nombreCarretera
	 */
	public String getNombreCarretera() {
		return nombreCarretera;
	}

	/**
	 * @param nombreCarretera the nombreCarretera to set
	 */
	public void setNombreCarretera(String nombreCarretera) {
		this.nombreCarretera = nombreCarretera;
	}

	/**
	 * @return the otraColonia
	 */
	public String getOtraColonia() {
		return otraColonia;
	}

	/**
	 * @param otraColonia the otraColonia to set
	 */
	public void setOtraColonia(String otraColonia) {
		this.otraColonia = otraColonia;
	}

	/**
	 * @return the referencia
	 */
	public String getReferencia() {
		return referencia;
	}

	/**
	 * @param referencia the referencia to set
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the tipoCarretera
	 */
	public String getTipoCarretera() {
		return tipoCarretera;
	}

	/**
	 * @param tipoCarretera the tipoCarretera to set
	 */
	public void setTipoCarretera(String tipoCarretera) {
		this.tipoCarretera = tipoCarretera;
	}

	/**
	 * @return the zona
	 */
	public String getZona() {
		return zona;
	}

	/**
	 * @param zona the zona to set
	 */
	public void setZona(String zona) {
		this.zona = zona;
	}

	/**
	 * @return the ciudad
	 */
	public CiudadMidas getCiudad() {
		return ciudad;
	}

	/**
	 * @param ciudad the ciudad to set
	 */
	public void setCiudad(CiudadMidas ciudad) {
		this.ciudad = ciudad;
	}

	/**
	 * @return the colonia
	 */
	public ColoniaMidas getColonia() {
		return colonia;
	}

	/**
	 * @param colonia the colonia to set
	 */
	public void setColonia(ColoniaMidas colonia) {
		this.colonia = colonia;
	}

	/**
	 * @return the estado
	 */
	public EstadoMidas getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(EstadoMidas estado) {
		this.estado = estado;
	}


	/**
	 * @return the pais
	 */
	public PaisMidas getPais() {
		return pais;
	}

	/**
	 * @param pais the pais to set
	 */
	public void setPais(PaisMidas pais) {
		this.pais = pais;
	}

	/**
	 * @return the idColoniaCheck
	 */
	@Transient
	public Boolean getIdColoniaCheck() {
		if(this.otraColonia == null || this.otraColonia.isEmpty()){
			idColoniaCheck = false;
		}else{
			idColoniaCheck = true;
		}
		return idColoniaCheck;
	}

	/**
	 * @param idColoniaCheck the idColoniaCheck to set
	 */
	public void setIdColoniaCheck(Boolean idColoniaCheck) {
		this.idColoniaCheck = idColoniaCheck;
	}
	
	/**
	 * @return the coordenadas
	 */
	@Embedded
	public Coordenadas getCoordenadas() {
		return coordenadas;
	}

	/**
	 * @param coordenadas the coordenadas to set
	 */
	public void setCoordenadas(Coordenadas coordenadas) {
		this.coordenadas = coordenadas;
	}
	
	
	@Embedded
	@AttributeOverrides({
	    @AttributeOverride(name="latitud",column=@Column(name="latitudContacto")),
	    @AttributeOverride(name="longitud",column=@Column(name="longitudContacto"))
	  })
	public Coordenadas getCoordenadasContacto() {
		return coordenadasContacto;
	}

	public void setCoordenadasContacto(Coordenadas coordenadasContacto) {
		this.coordenadasContacto = coordenadasContacto;
	}
	
	public Date getFechaCoordenadasContacto() {
		return fechaCoordenadasContacto;
	}

	public void setFechaCoordenadasContacto(Date fechaCoordenadasContacto) {
		this.fechaCoordenadasContacto = fechaCoordenadasContacto;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	
	public String validarDireccion(){
		String erroresDireccion = "";
		if(zona == null || zona.isEmpty()){
			erroresDireccion += "Zona,\n";
		}
		if(pais == null || pais.getId() == null || pais.getId().isEmpty()){
			erroresDireccion += "Pais,\n";
		}
		if(estado == null || estado.getId() == null || estado.getId().isEmpty()){
			erroresDireccion += "Estado,\n";
		}
		if(ciudad == null || ciudad.getId() == null || ciudad.getId().isEmpty()){
			erroresDireccion += "Municipio,\n";
		}
		if(zona.equals(TipoDireccion.CD.toString())){
			if(calleNumero == null || calleNumero.isEmpty()){
				erroresDireccion += "Calle y Número,\n";
			}

			if(!idColoniaCheck){
				/*if(colonia == null || colonia.getId() == null || colonia.getId().isEmpty()){
					erroresDireccion += "Colonia,\n";
				}*/
				if(codigoPostal == null || codigoPostal.isEmpty()){
					erroresDireccion += "Codigo Postal\n";
				}
			}else{
				/*if(otraColonia == null || otraColonia.isEmpty()){
					erroresDireccion += "Nueva Colonia,\n";
				}*/
				if(codigoPostalOtraColonia == null || codigoPostalOtraColonia.isEmpty()){
					erroresDireccion += "Codigo Postal\n";
				}
			}
		}else if(zona.equals(TipoDireccion.CA.toString())){
			if(tipoCarretera == null || tipoCarretera.isEmpty()){
				erroresDireccion += "Tipo de Carretera,\n";
			}
		}
		if(!erroresDireccion.isEmpty()){
			erroresDireccion = "Debe llenar los siguientes campos:\n" + erroresDireccion;
			if(erroresDireccion.endsWith(",\n")){
				erroresDireccion = erroresDireccion.substring(0, erroresDireccion.lastIndexOf(",\n"));
			}
			erroresDireccion += ".";
		}
		return erroresDireccion;
	}

	@Override	
	public String toString() {
		//ciudad
		StringBuilder builder = new StringBuilder("");
		try{
			if(this.zona.equals(TipoDireccion.CD.toString())){
				builder.append(this.calleNumero).append(", ").append(StringUtil.isEmpty(this.otraColonia)?this.colonia.getDescripcion():this.otraColonia)
					.append(", ").append(this.ciudad.getDescripcion()).append(", ").append(this.estado.getDescripcion()).append(", ").append(this.pais.getDescripcion())
						.append(", CP ").append(this.codigoPostal);
			}else{
				//carretera
				builder.append(this.referencia).append(", ").append(this.kilometro).append(", ").append(this.ciudad.getDescripcion()).append(", ")
					.append(this.getEstado().getDescripcion()).append(", ").append(this.getPais().getDescripcion());			
			}
		}catch(Exception ex){
			log.error(ex);
			builder.append("No pudo obtenerse la direccion");
		}
		return builder.toString();
	}
	
	

}