package mx.com.afirme.midas2.domain.siniestros.indemnizacion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import mx.com.afirme.midas2.dao.catalogos.Entidad;


@Entity(name = "LineaNegocioConversionMidasSeycos")
@Table(name = "VW_CONVLINEAS", schema = "MIDAS")
public class LineaNegocioConversionMidasSeycos implements Entidad{

	private static final long serialVersionUID = 8416711668458546941L;
	
	@Id
	@Column(name = "ID_MIDAS")
	private String idMidas;
	
	@Column(name = "ID_SEYCOS")
	private String idSeycos;

	/**
	 * @return the idMidas
	 */
	public String getIdMidas() {
		return idMidas;
	}

	/**
	 * @param idMidas the idMidas to set
	 */
	public void setIdMidas(String idMidas) {
		this.idMidas = idMidas;
	}

	/**
	 * @return the idSeycos
	 */
	public String getIdSeycos() {
		return idSeycos;
	}

	/**
	 * @param idSeycos the idSeycos to set
	 */
	public void setIdSeycos(String idSeycos) {
		this.idSeycos = idSeycos;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getKey() {
		return idMidas;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getBusinessKey() {
		return idMidas;
	}
	
}
