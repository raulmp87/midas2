package mx.com.afirme.midas2.domain.siniestros.pagos.facturas;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;

@Entity
@Table(name = "TOMENSAJEVALIDACIONFACTURA", schema = "MIDAS")
public class MensajeValidacionFactura extends MidasAbstracto implements Entidad {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MENVALFAC_ID_SEQ")
	@SequenceGenerator(name = "MENVALFAC_ID_SEQ",  schema="MIDAS", sequenceName = "MENVALFAC_ID_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "MENSAJE")
	private String mensaje;
	
	@Column(name = "TIPO_MENSAJE")
	private Long tipoMensaje;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinFetch(JoinFetchType.OUTER)
	@JoinColumn(name = "ID_ENVIO_FACTURA", referencedColumnName = "id")
	private EnvioValidacionFactura envioValidacionFactura;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public Long getTipoMensaje() {
		return tipoMensaje;
	}
	public void setTipoMensaje(Long tipoMensaje) {
		this.tipoMensaje = tipoMensaje;
	}
	
	public EnvioValidacionFactura getEnvioValidacionFactura() {
		return envioValidacionFactura;
	}
	public void setEnvioValidacionFactura(
			EnvioValidacionFactura envioValidacionFactura) {
		this.envioValidacionFactura = envioValidacionFactura;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	

}
