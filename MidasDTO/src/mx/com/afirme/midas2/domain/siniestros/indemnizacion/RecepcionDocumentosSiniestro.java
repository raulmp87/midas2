package mx.com.afirme.midas2.domain.siniestros.indemnizacion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.catalogos.EnumBase;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;

@Entity
@Table(name="TORECEPCIONDOCUMENTOSINIESTRO", schema="MIDAS")
public class RecepcionDocumentosSiniestro extends MidasAbstracto implements Entidad{

	private static final long serialVersionUID = 5229269110906458253L;

	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="RECEPCIONDOCTOS_GENERATOR")
	@SequenceGenerator(name="RECEPCIONDOCTOS_GENERATOR", sequenceName="IDTORECEPCIONDOCUMENTOSIN_SEQ", schema="MIDAS", allocationSize=1)
	private Long id;
	
	@Column(name="PENSION", nullable=false)
	private String pension;
	
	@Column(name="JUEGO_LLAVES", nullable=false)
	private Integer juegoLlaves;
	
	@Column(name="REFRENDO_TENENCIA", nullable=false)
	private String refrendoTenencia;
	
	@Column(name="BAJA_PLACAS", nullable=false)
	private String bajaPlacas;
	
	@Column(name="TIPO_PERSONA", nullable=false)
	private String tipoPersona;
	
	@Column(name="FACTURA_ORIGINAL", nullable=false)
	private String factruraOriginal;
	
	@Column(name="SEGUIMIENTO_FACT", nullable=false)
	private String seguimientoFactura;
	
	@Column(name="ENDOSO_AFIRME", nullable=false)
	private String endosoAfirme;
	
	@Column(name="FORMA_PAGO", nullable=false)
	private String formaPago;
	
	@Column(name="UBICACION", nullable=false)
	private String ubicacion;
	
	@Column(name="DIRECCION", nullable=false)
	private String direccion;
	
	@Column(name="DISPOSICION", nullable=false)
	private String disposicion;
	
	@Column(name="DENUNCIA_ROBO")
	private String denunciaRobo;
	
	@Column(name="BANCO_ID")
	private Integer bancoId;
	
	@Column(name="CLABE_BANCO")
	private String clabeBanco;
	
	@Column(name="COMENTARIOS")
	private String comentarios;
	
	@Column(name="ES_PAGO_DANIOS")
	private Boolean esPagoDanios = Boolean.FALSE;
	
	@Column(name="BENEFICIARIO")
	private String beneficiario;
	
	@Column(name="RFC")
	private String rfc;
	
	@Column(name="TELEFONO_NUMERO")
	private String telefonoNumero;
	
	@Column(name="TELEFONO_LADA")
	private String telefonoLada;
	
	@Column(name="CORREO")
	private String correo;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SINIESTRO_ID", nullable=false, referencedColumnName="ID")
	private SiniestroCabina siniestroCabina;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="INDEMNIZACION_ID", nullable=false, referencedColumnName="ID")
	private IndemnizacionSiniestro indemnizacion;
	
	public static enum FormaPago{
		TRANSFERENCIA_BANCARIA("TB"), CHEQUE("CH");
		public final String codigo;
		FormaPago(String codigo){
			this.codigo = codigo;
		}
	}
	
	public enum TipoPersona implements EnumBase<String>{
		PERSONA_FISICA("PF"),
		PERSONA_MORAL("PM");
		
		private final String tipoPersona;
		
		private TipoPersona(String tipoPersona){
			this.tipoPersona = tipoPersona;
		}

		@Override
		public String getValue() {
			return tipoPersona;
		}

		@Override
		public String getLabel() {
			return tipoPersona;
		}
		
	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the juegoLlaves
	 */
	public Integer getJuegoLlaves() {
		return juegoLlaves;
	}
	/**
	 * @param juegoLlaves the juegoLlaves to set
	 */
	public void setJuegoLlaves(Integer juegoLlaves) {
		this.juegoLlaves = juegoLlaves;
	}
	/**
	 * @return the tipoPersona
	 */
	public String getTipoPersona() {
		return tipoPersona;
	}
	/**
	 * @param tipoPersona the tipoPersona to set
	 */
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	/**
	 * @return the factruraOriginal
	 */
	public String getFactruraOriginal() {
		return factruraOriginal;
	}
	/**
	 * @param factruraOriginal the factruraOriginal to set
	 */
	public void setFactruraOriginal(String factruraOriginal) {
		this.factruraOriginal = factruraOriginal;
	}
	/**
	 * @return the seguimientoFactura
	 */
	public String getSeguimientoFactura() {
		return seguimientoFactura;
	}
	/**
	 * @param seguimientoFactura the seguimientoFactura to set
	 */
	public void setSeguimientoFactura(String seguimientoFactura) {
		this.seguimientoFactura = seguimientoFactura;
	}
	/**
	 * @return the endosoAfirme
	 */
	public String getEndosoAfirme() {
		return endosoAfirme;
	}
	/**
	 * @param endosoAfirme the endosoAfirme to set
	 */
	public void setEndosoAfirme(String endosoAfirme) {
		this.endosoAfirme = endosoAfirme;
	}
	/**
	 * @return the formaPago
	 */
	public String getFormaPago() {
		return formaPago;
	}
	/**
	 * @param formaPago the formaPago to set
	 */
	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	/**
	 * @return the clabeBanco
	 */
	public String getClabeBanco() {
		return clabeBanco;
	}
	/**
	 * @param clabeBanco the clabeBanco to set
	 */
	public void setClabeBanco(String clabeBanco) {
		this.clabeBanco = clabeBanco;
	}
	/**
	 * @return the comentarios
	 */
	public String getComentarios() {
		return comentarios;
	}
	/**
	 * @param comentarios the comentarios to set
	 */
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}
	@Override
	public String getValue() {
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	/**
	 * @return the ubicacion
	 */
	public String getUbicacion() {
		return ubicacion;
	}
	/**
	 * @param ubicacion the ubicacion to set
	 */
	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}
	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}
	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	/**
	 * @return the disposicion
	 */
	public String getDisposicion() {
		return disposicion;
	}
	/**
	 * @param disposicion the disposicion to set
	 */
	public void setDisposicion(String disposicion) {
		this.disposicion = disposicion;
	}
	/**
	 * @return the siniestroCabina
	 */
	public SiniestroCabina getSiniestroCabina() {
		return siniestroCabina;
	}
	/**
	 * @param siniestroCabina the siniestroCabina to set
	 */
	public void setSiniestroCabina(SiniestroCabina siniestroCabina) {
		this.siniestroCabina = siniestroCabina;
	}
	/**
	 * @return the pension
	 */
	public String getPension() {
		return pension;
	}
	/**
	 * @param pension the pension to set
	 */
	public void setPension(String pension) {
		this.pension = pension;
	}
	/**
	 * @return the refrendoTenencia
	 */
	public String getRefrendoTenencia() {
		return refrendoTenencia;
	}
	/**
	 * @param refrendoTenencia the refrendoTenencia to set
	 */
	public void setRefrendoTenencia(String refrendoTenencia) {
		this.refrendoTenencia = refrendoTenencia;
	}
	/**
	 * @return the bajaPlacas
	 */
	public String getBajaPlacas() {
		return bajaPlacas;
	}
	/**
	 * @param bajaPlacas the bajaPlacas to set
	 */
	public void setBajaPlacas(String bajaPlacas) {
		this.bajaPlacas = bajaPlacas;
	}
	/**
	 * @return the denunciaRobo
	 */
	public String getDenunciaRobo() {
		return denunciaRobo;
	}
	/**
	 * @param denunciaRobo the denunciaRobo to set
	 */
	public void setDenunciaRobo(String denunciaRobo) {
		this.denunciaRobo = denunciaRobo;
	}
	/**
	 * @return the esPagoDanios
	 */
	public Boolean getEsPagoDanios() {
		return esPagoDanios;
	}
	/**
	 * @param esPagoDanios the esPagoDanios to set
	 */
	public void setEsPagoDanios(Boolean esPagoDanios) {
		this.esPagoDanios = esPagoDanios;
	}
	/**
	 * @return the indemnizacion
	 */
	public IndemnizacionSiniestro getIndemnizacion() {
		return indemnizacion;
	}
	/**
	 * @param indemnizacion the indemnizacion to set
	 */
	public void setIndemnizacion(IndemnizacionSiniestro indemnizacion) {
		this.indemnizacion = indemnizacion;
	}
	/**
	 * @return the bancoId
	 */
	public Integer getBancoId() {
		return bancoId;
	}
	/**
	 * @param bancoId the bancoId to set
	 */
	public void setBancoId(Integer bancoId) {
		this.bancoId = bancoId;
	}
	/**
	 * @return the beneficiario
	 */
	public String getBeneficiario() {
		return beneficiario;
	}
	/**
	 * @param beneficiario the beneficiario to set
	 */
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}
	/**
	 * @return the rfc
	 */
	public String getRfc() {
		return rfc;
	}
	/**
	 * @param rfc the rfc to set
	 */
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	/**
	 * @return the telefonoNumero
	 */
	public String getTelefonoNumero() {
		return telefonoNumero;
	}
	/**
	 * @param telefonoNumero the telefonoNumero to set
	 */
	public void setTelefonoNumero(String telefonoNumero) {
		this.telefonoNumero = telefonoNumero;
	}
	/**
	 * @return the telefonoLada
	 */
	public String getTelefonoLada() {
		return telefonoLada;
	}
	/**
	 * @param telefonoLada the telefonoLada to set
	 */
	public void setTelefonoLada(String telefonoLada) {
		this.telefonoLada = telefonoLada;
	}
	/**
	 * @return the correo
	 */
	public String getCorreo() {
		return correo;
	}
	/**
	 * @param correo the correo to set
	 */
	public void setCorreo(String correo) {
		this.correo = correo;
	}

}