/**
 * 
 */
package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;

/**
 * @author simavera
 *
 */
@Entity(name = "CondicionEspecialReporte")
@Table(name = "TOCONDICIONESPECIALREPORTE", schema = "MIDAS")
public class CondicionEspecialReporte extends MidasAbstracto{

	@Id
	@SequenceGenerator(name = "IDCONDICIONESPECIALREPORTE_SEQ_GENERADOR",allocationSize = 1, sequenceName = "TOCONDICIONESPECIALREPORTE_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "IDCONDICIONESPECIALREPORTE_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id;

	//bi-directional many-to-one association to CondicionEspecial
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CONDICIONESPECIAL_ID")
	CondicionEspecial condicionEspecial;
	

	//bi-directional many-to-one association to ReporteCabina
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="REPORTECABINA_ID")
	ReporteCabina reporteCabina;
	
	@Column(name = "CLAVECONTRATO")
	private Boolean claveContrato;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the condicionEspecial
	 */
	public CondicionEspecial getCondicionEspecial() {
		return condicionEspecial;
	}

	/**
	 * @param condicionEspecial the condicionEspecial to set
	 */
	public void setCondicionEspecial(CondicionEspecial condicionEspecial) {
		this.condicionEspecial = condicionEspecial;
	}

	/**
	 * @return the reporteCabina
	 */
	public ReporteCabina getReporteCabina() {
		return reporteCabina;
	}

	/**
	 * @param reporteCabina the reporteCabina to set
	 */
	public void setReporteCabina(ReporteCabina reporteCabina) {
		this.reporteCabina = reporteCabina;
	}

	private static final long serialVersionUID = 8765325391752986483L;

	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	public Boolean getClaveContrato() {
		return claveContrato;
	}

	public void setClaveContrato(Boolean claveContrato) {
		this.claveContrato = claveContrato;
	}
	
	

}
