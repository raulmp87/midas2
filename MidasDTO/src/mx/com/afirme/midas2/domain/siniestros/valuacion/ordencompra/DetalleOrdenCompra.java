package mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.catalogo.conceptos.ConceptoAjuste;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;


/**
 * entidad que mapea el detalle de una �rden de compra, cada objeto de esta tabla
 * representa un concepto de la �rden de compra.
 * @author usuario
 * @version 1.0
 * @created 22-ago-2014 10:48:42 a.m.
 */



@Entity(name = "DetalleOrdenCompra")
@Table(name = "TODETALLEORDENCOMPRA", schema = "MIDAS")
public class DetalleOrdenCompra extends MidasAbstracto {


	@Id
	@SequenceGenerator(name = "TODETALLEORDENCOMPRA_SEQ_GENERADOR",allocationSize = 1, sequenceName = "TODETALLEORDENCOMPRA_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TODETALLEORDENCOMPRA_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id;
	
	
	private static final long serialVersionUID = 4586781888829736644L;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CONCEPTO", referencedColumnName="ID")
	@JoinFetch(JoinFetchType.OUTER)
	private ConceptoAjuste conceptoAjuste;
	
	@Column(name="COSTO_UNITARIO")
	private BigDecimal costoUnitario;/*SUBTOTAL */
	
	@Column(name="ESTATUS")
	private String estatus;
	
	@Column(name="IMPORTE")
	private BigDecimal importe; /*ES TOTAL DEL SUBTOTAL (Costo_unitario) + IVA (IMPUESTO) */
	/**
	 * monto pagado para este concepto
	 */
	@Column(name="IMPORTE_PAGADO")
	private BigDecimal importePagado;
	
	@Column(name="IMPUESTO") /*IVA*/
	private BigDecimal iva;
	
	@Column(name="IVA_RETENIDO") /*IVA*/
	private BigDecimal ivaRetenido;
	
	@Column(name="ISR") /*IVA*/
	private BigDecimal isr;
	
	@Column(name="OBSERVACIONES")
	private String observaciones;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_ORDEN_COMPRA", referencedColumnName = "ID")	
	private OrdenCompra ordenCompra;
	
	@Column(name="PORC_IVA_RET") 
	private BigDecimal porcIvaRetenido;

	@Column(name="PORC_ISR") 
	private BigDecimal porcIsr;
	
	
	@Column(name="PORC_IMPUESTO") 
	private BigDecimal porcIva;
	
	
	
	public BigDecimal getCostoUnitario() {
		return costoUnitario;
	}

	public void setCostoUnitario(BigDecimal costoUnitario) {
		this.costoUnitario = costoUnitario;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getImporte() {
		return importe;
	}

	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	public BigDecimal getImportePagado() {
		return importePagado;
	}

	public void setImportePagado(BigDecimal importePagado) {
		this.importePagado = importePagado;
	}

	public BigDecimal getIva() {
		return iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	

	public OrdenCompra getOrdenCompra() {
		return ordenCompra;
	}

	public void setOrdenCompra(OrdenCompra ordenCompra) {
		this.ordenCompra = ordenCompra;
	}

	public BigDecimal getPorcIva() {
		return porcIva;
	}

	public void setPorcIva(BigDecimal porcIva) {
		this.porcIva = porcIva;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	public ConceptoAjuste getConceptoAjuste() {
		return conceptoAjuste;
	}

	public void setConceptoAjuste(ConceptoAjuste conceptoAjuste) {
		this.conceptoAjuste = conceptoAjuste;
	}

	@Override
	public String toString() {
		return "DetalleOrdenCompra [id=" + id + ", costoUnitario="
				+ costoUnitario + ", estatus=" + estatus + ", importe="
				+ importe + ", importePagado=" + importePagado + ", iva=" + iva
				+ ", observaciones=" + observaciones + ", porcIva=" + porcIva
				+ "]";
	}

	public BigDecimal getIvaRetenido() {
		return ivaRetenido;
	}

	public void setIvaRetenido(BigDecimal ivaRetenido) {
		this.ivaRetenido = ivaRetenido;
	}

	public BigDecimal getIsr() {
		return isr;
	}

	public void setIsr(BigDecimal isr) {
		this.isr = isr;
	}

	public BigDecimal getPorcIvaRetenido() {
		return porcIvaRetenido;
	}

	public void setPorcIvaRetenido(BigDecimal porcIvaRetenido) {
		this.porcIvaRetenido = porcIvaRetenido;
	}

	public BigDecimal getPorcIsr() {
		return porcIsr;
	}

	public void setPorcIsr(BigDecimal porcIsr) {
		this.porcIsr = porcIsr;
	}

	
	
	

}