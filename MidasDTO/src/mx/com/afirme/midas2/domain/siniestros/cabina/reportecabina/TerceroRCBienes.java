package mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.personadireccion.CiudadMidas;
import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;
import mx.com.afirme.midas2.domain.personadireccion.PaisMidas;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.util.StringUtil;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;



@Entity(name = "TerceroRCBienes")
@Table(name = "TOTERCERORCBIENES", schema = "MIDAS")
@DiscriminatorValue("RCB")
@PrimaryKeyJoinColumn(name="ESTIMACIONCOBERTURAREPCAB_ID", referencedColumnName="ID")
public class TerceroRCBienes extends EstimacionCoberturaReporteCabina {

	private static final long serialVersionUID = 1L;
	
	@Column(name="UBICACION")
	private String ubicacion;
	
	@Column(name="OBSERVACIONES")
	private String observacion;
	
	@Column(name="DANOS_PREEXISTENTES")
	private String danosPreexistentes;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch(JoinFetchType.OUTER)
	@JoinColumn(name="PAIS_ID", referencedColumnName="COUNTRY_ID")
	private PaisMidas pais;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch(JoinFetchType.OUTER)
	@JoinColumn(name="ESTADO_ID", referencedColumnName="STATE_ID")
	private EstadoMidas estado;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch(JoinFetchType.OUTER)
	@JoinColumn(name="MUNICIPIO_ID", referencedColumnName="CITY_ID")
	private CiudadMidas municipio;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "RESPONSABLE_REPARACION_ID")
	private PrestadorServicio responsableReparacion;
	
	@Column(name="DANOS")
	private String dano;
	
	@Column(name="TIPO_BIEN")
	private String tipoBien;
	

	public TerceroRCBienes(){

	}

	public void finalize() throws Throwable {
		super.finalize();
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public PaisMidas getPais() {
		return pais;
	}

	public void setPais(PaisMidas pais) {
		this.pais = pais;
	}

	public EstadoMidas getEstado() {
		return estado;
	}

	public void setEstado(EstadoMidas estado) {
		this.estado = estado;
	}

	public CiudadMidas getMunicipio() {
		return municipio;
	}

	public void setMunicipio(CiudadMidas municipio) {
		this.municipio = municipio;
	}

	public PrestadorServicio getResponsableReparacion() {
		return responsableReparacion;
	}

	public void setResponsableReparacion(PrestadorServicio responsableReparacion) {
		this.responsableReparacion = responsableReparacion;
	}

	public String getDano() {
		return dano;
	}

	public void setDano(String dano) {
		this.dano = dano;
	}

	public String getTipoBien() {
		return tipoBien;
	}

	public void setTipoBien(String tipoBien) {
		this.tipoBien = tipoBien;
	}

	public String getDanosPreexistentes() {
		return danosPreexistentes;
	}

	public void setDanosPreexistentes(String danosPreexistentes) {
		this.danosPreexistentes = danosPreexistentes;
	}
	
	@Transient
	public String getDomicilio(){
		StringBuilder direccion = new StringBuilder("");
		direccion.append(!StringUtil.isEmpty(ubicacion) ? ubicacion.concat(", ")  : "");
		direccion.append(municipio != null && !StringUtil.isEmpty(municipio.getDescripcion()) ? municipio.getDescripcion().concat(", ") : "");
		direccion.append(estado != null && !StringUtil.isEmpty(estado.getDescripcion()) ? estado.getDescripcion().concat(", ") : "");
		direccion.append(pais != null && !StringUtil.isEmpty(pais.getDescripcion()) ? pais.getDescripcion() : "");
		return direccion.toString();
	}

}