package mx.com.afirme.midas2.domain.vida;


public class ValoresRescate {
	
	private static final long serialVersionUID = -7703645329320642555L;
	
	private String producto;
	private Integer temporalidad;
	private Integer moneda;
	private Integer edadContratacion;
	private Integer anioVigencia;
	private Double valorRescate;
	
	public ValoresRescate(String producto, Integer temporalidad,
			Integer moneda, Integer edadContratacion, Integer anioVigencia,
			Double valorRescate) {
		super();
		this.producto = producto;
		this.temporalidad = temporalidad;
		this.moneda = moneda;
		this.edadContratacion = edadContratacion;
		this.anioVigencia = anioVigencia;
		this.valorRescate = valorRescate;
	}
	
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public Integer getTemporalidad() {
		return temporalidad;
	}
	public void setTemporalidad(Integer temporalidad) {
		this.temporalidad = temporalidad;
	}
	public Integer getMoneda() {
		return moneda;
	}
	public void setMoneda(Integer moneda) {
		this.moneda = moneda;
	}
	public Integer getEdadContratacion() {
		return edadContratacion;
	}
	public void setEdadContratacion(Integer edadContratacion) {
		this.edadContratacion = edadContratacion;
	}
	public Integer getAnioVigencia() {
		return anioVigencia;
	}
	public void setAnioVigencia(Integer anioVigencia) {
		this.anioVigencia = anioVigencia;
	}
	public Double getValorRescate() {
		return valorRescate;
	}
	public void setValorRescate(Double valorRescate) {
		this.valorRescate = valorRescate;
	}
}
