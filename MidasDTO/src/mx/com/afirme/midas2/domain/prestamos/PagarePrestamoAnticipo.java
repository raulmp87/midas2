package mx.com.afirme.midas2.domain.prestamos;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

@Entity(name="PagarePrestamoAnticipo")
@Table(name="toPagarePrestamoAnticipo",schema="MIDAS")
public class PagarePrestamoAnticipo implements Entidad,Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private ConfigPrestamoAnticipo configPrestamoAnticipo;
	private Date fechaCargo;
	private Double capitalInicial;
	private Double interesOrdinario;
	private Double interesMoratorio;
	private Double abonoCapital;
	private Double iva;
	private Double importePago;
	private Double capitalfinal;
	private Double abonoPorPagare;
	private ValorCatalogoAgentes estatus;
	private String fechaCargoString;
	private String estatusPagare;
	private Long idSolicitudCheque;
	private Long idConfigPrestamo;
	
	
	@Id
	@SequenceGenerator(name="idToPagarePrestamoAnticipo_seq",sequenceName="MIDAS.idToPagarePrestamoAnticipo_seq",allocationSize=1)
	@GeneratedValue(generator="idToPagarePrestamoAnticipo_seq",strategy=GenerationType.SEQUENCE)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ConfigPrestamoAnticipo.class)
	@JoinColumn(name="IDCONFIGPRESTAMOANTICIPO")
	public ConfigPrestamoAnticipo getConfigPrestamoAnticipo() {
		return configPrestamoAnticipo;
	}

	public void setConfigPrestamoAnticipo(
			ConfigPrestamoAnticipo configPrestamoAnticipo) {
		this.configPrestamoAnticipo = configPrestamoAnticipo;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHACARGO")
	public Date getFechaCargo() {
		return fechaCargo;
	}

	public void setFechaCargo(Date fechaCargo) {
		this.fechaCargo = fechaCargo;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
		this.fechaCargoString = sdf.format(fechaCargo);
	}

	@Column(name="capitalInicial")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public Double getCapitalInicial() {
		return capitalInicial;
	}

	public void setCapitalInicial(Double capitalInicial) {
		this.capitalInicial = capitalInicial;
	}

	@Column(name="interesOrdinario")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public Double getInteresOrdinario() {
		return interesOrdinario;
	}

	public void setInteresOrdinario(Double interesOrdinario) {
		this.interesOrdinario = interesOrdinario;
	}

	@Column(name="interesMoratorio")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public Double getInteresMoratorio() {
		return interesMoratorio;
	}

	public void setInteresMoratorio(Double interesMoratorio) {
		this.interesMoratorio = interesMoratorio;
	}

	@Column(name="abonoCapital")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public Double getAbonoCapital() {
		return abonoCapital;
	}

	public void setAbonoCapital(Double abonoCapital) {
		this.abonoCapital = abonoCapital;
	}

	@Column(name="iva")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public Double getIva() {
		return iva;
	}

	public void setIva(Double iva) {
		this.iva = iva;
	}

	@Column(name="importePago")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public Double getImportePago() {
		return importePago;
	}

	public void setImportePago(Double importePago) {
		this.importePago = importePago;
	}

	@Column(name="capitalFinal")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public Double getCapitalfinal() {
		return capitalfinal;
	}

	public void setCapitalfinal(Double capitalfinal) {
		this.capitalfinal = capitalfinal;
	}

	@Column(name="abonoPorPagare")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public Double getAbonoPorPagare() {
		return abonoPorPagare;
	}

	public void setAbonoPorPagare(Double abonoPorPagare) {
		this.abonoPorPagare = abonoPorPagare;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="claveEstatus")
	public ValorCatalogoAgentes getEstatus() {
		return estatus;
	}

	public void setEstatus(ValorCatalogoAgentes estatus) {
		this.estatus = estatus;
	}
	
	@Column(name="IDSOLICITUDCHEQUE")
	public Long getIdSolicitudCheque() {
		return idSolicitudCheque;
	}

	public void setIdSolicitudCheque(Long idSolicitudCheque) {
		this.idSolicitudCheque = idSolicitudCheque;
	}

	@Transient
	public String getFechaCargoString() {
		return fechaCargoString;
	}

	public void setFechaCargoString(String fechaCargoString) {
		this.fechaCargoString = fechaCargoString;
	}

	
	@Transient
	public String getEstatusPagare() {
		return estatusPagare;
	}

	public void setEstatusPagare(String estatusPagare) {
		this.estatusPagare = estatusPagare;
	}
	
	
	@Transient
	public Long getIdConfigPrestamo() {
		return idConfigPrestamo;
	}

	public void setIdConfigPrestamo(Long idConfigPrestamo) {
		this.idConfigPrestamo = idConfigPrestamo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}
