package mx.com.afirme.midas2.domain.prestamos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

@Entity(name="ConfigPrestamoAnticipo")
@Table(name="TOCONFIGPRESTAMOANTICIPO",schema="MIDAS")
public class ConfigPrestamoAnticipo implements Entidad,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private ValorCatalogoAgentes tipoMovimiento;
	private Agente agente;
	private ValorCatalogoAgentes estatus;
	private Date fechaAltaMovimiento;
	private Double importeOtorgado;
	private ValorCatalogoAgentes plazo;
	private Date fechaInicioCalculo;
	private Integer numeroPagos;
	private Double pcteInteresOrdinario;
	private ValorCatalogoAgentes plazoInteres;
	private Double importeInteresOrdinario;
	private Double pcteInteresMoratorio;
	private Double importePago;
	private String observaciones;
	private String nombreAval;
	private String coloniaAval;
	private String calleNumeroAval;
	private String codigoPostalAval;
	private String estadoAval;
	private String municipioAval;
	private String telefonoAval;
	private Date  fechaDeAltaMovimiento;
	private Date  fechaDeFinMovimiento;
	private ValorCatalogoAgentes idTipoCalculo;
	private Long idSolicitudCheque;
	
	public static final String CARPETA_ANTICIPOS = "ANTICIPOS";
	public static final String CARPETA_PRESTAMOS = "PRESTAMOS";
	
	@Id
	@SequenceGenerator(name="idToCfgPresAnt_seq",sequenceName="MIDAS.idToCfgPresAnt_seq",allocationSize=1)
	@GeneratedValue(generator="idToCfgPresAnt_seq",strategy=GenerationType.SEQUENCE)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDTIPOMOVIMIENTO")
	public ValorCatalogoAgentes getTipoMovimiento() {
		return tipoMovimiento;
	}

	public void setTipoMovimiento(ValorCatalogoAgentes tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=Agente.class)
	@JoinColumn(name="idAgente")
	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="claveEstatus")
	public ValorCatalogoAgentes getEstatus() {
		return estatus;
	}

	public void setEstatus(ValorCatalogoAgentes estatus) {
		this.estatus = estatus;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="fechaAltaMovimiento")
	public Date getFechaAltaMovimiento() {
		return fechaAltaMovimiento;
	}

	public void setFechaAltaMovimiento(Date fechaAltaMovimiento) {
		this.fechaAltaMovimiento = fechaAltaMovimiento;
	}

	@Column(name="importeOtorgado")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public Double getImporteOtorgado() {
		return importeOtorgado;
	}

	public void setImporteOtorgado(Double importeOtorgado) {
		this.importeOtorgado = importeOtorgado;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="idPlazo")
	public ValorCatalogoAgentes getPlazo() {
		return plazo;
	}

	public void setPlazo(ValorCatalogoAgentes plazo) {
		this.plazo = plazo;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="fechaInicioCalculo")
	public Date getFechaInicioCalculo() {
		return fechaInicioCalculo;
	}

	public void setFechaInicioCalculo(Date fechaInicioCalculo) {
		this.fechaInicioCalculo = fechaInicioCalculo;
	}

	@Column(name="numeroPagos")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public Integer getNumeroPagos() {
		return numeroPagos;
	}

	public void setNumeroPagos(Integer numeroPagos) {
		this.numeroPagos = numeroPagos;
	}

	@Column(name="pcteInteresOrdinario")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public Double getPcteInteresOrdinario() {
		return pcteInteresOrdinario;
	}

	public void setPcteInteresOrdinario(Double pcteInteresOrdinario) {
		this.pcteInteresOrdinario = pcteInteresOrdinario;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="idPlazoInteres")
	public ValorCatalogoAgentes getPlazoInteres() {
		return plazoInteres;
	}

	public void setPlazoInteres(ValorCatalogoAgentes plazoInteres) {
		this.plazoInteres = plazoInteres;
	}

	@Column(name="importeInteresOrdinario")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public Double getImporteInteresOrdinario() {
		return importeInteresOrdinario;
	}

	public void setImporteInteresOrdinario(Double importeInteresOrdinario) {
		this.importeInteresOrdinario = importeInteresOrdinario;
	}

	@Column(name="pcteInteresMoratorio")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public Double getPcteInteresMoratorio() {
		return pcteInteresMoratorio;
	}

	public void setPcteInteresMoratorio(Double pcteInteresMoratorio) {
		this.pcteInteresMoratorio = pcteInteresMoratorio;
	}

	@Column(name="importePago")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public Double getImportePago() {
		return importePago;
	}

	public void setImportePago(Double importePago) {
		this.importePago = importePago;
	}

	@Column(name="observaciones")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	@Column(name="nombreAval")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public String getNombreAval() {
		return nombreAval;
	}

	public void setNombreAval(String nombreAval) {
		this.nombreAval = nombreAval;
	}

	@Column(name="coloniaAval")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public String getColoniaAval() {
		return coloniaAval;
	}

	public void setColoniaAval(String coloniaAval) {
		this.coloniaAval = coloniaAval;
	}

	@Column(name="calleNumeroAval")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public String getCalleNumeroAval() {
		return calleNumeroAval;
	}

	public void setCalleNumeroAval(String calleNumeroAval) {
		this.calleNumeroAval = calleNumeroAval;
	}

	@Column(name="codigoPostalAval")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public String getCodigoPostalAval() {
		return codigoPostalAval;
	}

	public void setCodigoPostalAval(String codigoPostalAval) {
		this.codigoPostalAval = codigoPostalAval;
	}

	@Column(name="estadoAval")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public String getEstadoAval() {
		return estadoAval;
	}

	public void setEstadoAval(String estadoAval) {
		this.estadoAval = estadoAval;
	}

	@Column(name="municipioAval")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public String getMunicipioAval() {
		return municipioAval;
	}

	public void setMunicipioAval(String municipioAval) {
		this.municipioAval = municipioAval;
	}

	@Column(name="telefonoAval")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public String getTelefonoAval() {
		return telefonoAval;
	}

	public void setTelefonoAval(String telefonoAval) {
		this.telefonoAval = telefonoAval;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="TIPOCALCULO")
	public ValorCatalogoAgentes getIdTipoCalculo() {
		return idTipoCalculo;
	}

	public void setIdTipoCalculo(ValorCatalogoAgentes idTipoCalculo) {
		this.idTipoCalculo = idTipoCalculo;
	}
	
	@Column(name="IDSOLICITUDCHEQUE")
	public Long getIdSolicitudCheque() {
		return idSolicitudCheque;
	}
	public void setIdSolicitudCheque(Long idSolicitudCheque) {
		this.idSolicitudCheque = idSolicitudCheque;
	}
	
	@Transient
	public Date getFechaDeAltaMovimiento() {
		return fechaDeAltaMovimiento;
	}

	public void setFechaDeAltaMovimiento(Date fechaDeAltaMovimiento) {
		this.fechaDeAltaMovimiento = fechaDeAltaMovimiento;
	}

	@Transient
	public Date getFechaDeFinMovimiento() {
		return fechaDeFinMovimiento;
	}

	public void setFechaDeFinMovimiento(Date fechaDeFinMovimiento) {
		this.fechaDeFinMovimiento = fechaDeFinMovimiento;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
}
