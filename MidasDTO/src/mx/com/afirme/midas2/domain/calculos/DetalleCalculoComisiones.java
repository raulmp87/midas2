package mx.com.afirme.midas2.domain.calculos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.util.StaticCommonVariables;
/**
 * Entidad para el detalle de cada calculo de agentes y/o promotoria
 * @author vmhersil
 *
 */
@Entity(name="DetalleCalculoComisiones")
@Table(schema=StaticCommonVariables.DEFAULT_SCHEMA,name="toDetalleCalculoComisiones")
@SqlResultSetMappings(value={
	@SqlResultSetMapping(name="detalleCalculoComisionesView",entities={
		@EntityResult(entityClass=DetalleCalculoComisiones.class,fields={
			@FieldResult(name="id",column="id"),
			@FieldResult(name="agente.id",column="idAgente"),
			@FieldResult(name="calculoComisiones.id",column="idCalculo"),
			@FieldResult(name="numeroCuenta",column="numeroCuenta"),
			@FieldResult(name="importe",column="importeCalculo"),
			@FieldResult(name="promotoria.id",column="idPromotoria"),
			@FieldResult(name="idProductoBancario",column="idProductoBancario"),
			@FieldResult(name="idSolicitudCheque",column="idSolicitudCheque"),
			@FieldResult(name="claveEstatus",column="claveEstatus")
		})
	}),
	@SqlResultSetMapping(name="detalleCalculoComisionesComparativoView",entities={
			@EntityResult(entityClass=DetalleCalculoComisiones.class,fields={
				@FieldResult(name="id",column="id"),
				@FieldResult(name="idSolicitudCheque",column="idSolicitudCheque"),
				@FieldResult(name="clageAgente",column="claveAgente"),
				@FieldResult(name="importe",column="importe"),
				@FieldResult(name="agente.id",column="idAgente"),
				@FieldResult(name="nombreAgente",column="nombreAgente"),
				@FieldResult(name="tieneDiferencia",column="diferencia")
			})
	})
})
public class DetalleCalculoComisiones implements Serializable,Entidad{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1101415470507651921L;
	private Long id;
	private CalculoComisiones calculoComisiones;
	private Agente agente;
	private Promotoria promotoria;
	private Double importe;
	private String importeString;
	private Long idSolicitudCheque;
	private ValorCatalogoAgentes claveEstatus;
	private ValorCatalogoAgentes estatusRecibo;
	private Integer tieneDiferencia;
	/**
	 * ============================================
	 * Transient fields for Mizar Comparisson
	 * ============================================
	 */
	private Long clageAgente;
	private String nombreAgente;
	private Double importeTruncate;
	
	private String idSolicitudChequeStr;
	
	public DetalleCalculoComisiones(){}
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idToDetalleCalculoCom_seq")
	@SequenceGenerator(name="idToDetalleCalculoCom_seq", sequenceName="MIDAS.idToDetalleCalculoCom_seq",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=CalculoComisiones.class)
	@JoinColumn(name="IDCALCULOCOMISIONES")
	public CalculoComisiones getCalculoComisiones() {
		return calculoComisiones;
	}

	public void setCalculoComisiones(CalculoComisiones calculoComisiones) {
		this.calculoComisiones = calculoComisiones;
	}
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=Agente.class)
	@JoinColumn(name="IDAGENTE")
	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=Promotoria.class)
	@JoinColumn(name="IDPROMOTORIA")
	public Promotoria getPromotoria() {
		return promotoria;
	}

	public void setPromotoria(Promotoria promotoria) {
		this.promotoria = promotoria;
	}
	@Column(name="IMPORTE")
	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
		if(importe!=null){
				this.importeTruncate =  truncateDecimal(importe,2).doubleValue();
			}
	}
	
	@Column(name="IDSOLICITUDCHEQUE")
	public Long getIdSolicitudCheque() {
		return idSolicitudCheque;
	}
	public void setIdSolicitudCheque(Long idSolicitudCheque) {
		this.idSolicitudCheque = idSolicitudCheque;
	}
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="CLAVEESTATUS")
	public ValorCatalogoAgentes getClaveEstatus() {
		return claveEstatus;
	}
	public void setClaveEstatus(ValorCatalogoAgentes claveEstatus) {
		this.claveEstatus = claveEstatus;
	}
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="ESTATUSRECIBO")
	public ValorCatalogoAgentes getEstatusRecibo() {
		return estatusRecibo;
	}
	public void setEstatusRecibo(ValorCatalogoAgentes estatusRecibo) {
		this.estatusRecibo = estatusRecibo;
	}
	@Transient
	public String getImporteString() {
		if(importe!=null){
			DecimalFormat doubleFormat = new DecimalFormat("#.##");
			this.importeString = doubleFormat.format(importe);
		}
		return importeString;
	}
	public void setImporteString(String importeString) {
		this.importeString = importeString;
	}
	@Transient
	public Long getClageAgente() {
		return clageAgente;
	}
	public void setClageAgente(Long clageAgente) {
		this.clageAgente = clageAgente;
	}
	@Transient
	public String getNombreAgente() {
		return nombreAgente;
	}
	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	/**
	 * Se agrega solo para la tabla comparativa Midas vs Mizar
	 * @return
	 */
	@Transient
	public Integer getTieneDiferencia() {
		return tieneDiferencia;
	}
	public void setTieneDiferencia(Integer tieneDiferencia) {
		this.tieneDiferencia = tieneDiferencia;
	}
	/**
	 * se agrega este campo para que en el fron se trunque a dos decimales el importe total
	 */
	@Transient
	public Double getImporteTruncate() {
		return importeTruncate;
	}
	public void setImporteTruncate(Double importeTruncate) {
		this.importeTruncate = importeTruncate;
	}
	
	private static BigDecimal truncateDecimal(double x,int numberofDecimals)
	{
	    if ( x > 0) {
	        return new BigDecimal(String.valueOf(x)).setScale(numberofDecimals, BigDecimal.ROUND_FLOOR);
	    } else {
	        return new BigDecimal(String.valueOf(x)).setScale(numberofDecimals, BigDecimal.ROUND_CEILING);
	    }
	}
	
	@Transient
	public String getIdSolicitudChequeStr() {
		return idSolicitudChequeStr;
	}
	public void setIdSolicitudChequeStr(String idSolicitudChequeStr) {
		this.idSolicitudChequeStr = idSolicitudChequeStr;
	}
	
}
