package mx.com.afirme.midas2.domain.calculos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.util.StaticCommonVariables;
@Entity(name="MovimientoAgente")
@Table(schema=StaticCommonVariables.DEFAULT_SCHEMA,name="toAgenteMovimientos")
public class MovimientoAgente implements Serializable,Entidad{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3354537363246700801L;
	private Long 	id;
	private Long	idEmpresa;
	private Agente	agente;
	private Date 	fechaCorte;
	private Integer	idConsecMovto;
	private String	anioMes;
	private	ValorCatalogoAgentes concepto;
	private ValorCatalogoAgentes moneda;
	private ValorCatalogoAgentes monedaOrigen;
	private Double 	tipoCambio;
	private String	descripcionMotivo;
	private	Long	idRamoContable;
	private	Long	idSubramoContable;
	private Long 	idLineaNegocio;
	private	Date	fechaMovimiento;
	private Long	idCentroEmisor;
	private	Long	numPoliza;
	private	Integer	numRenovacionPoliza;
	private Long	idCotizacion;
	private Long 	idVersionPol;
	private Long	idCentroEmisorE;
	private ValorCatalogoAgentes tipoEndoso;
	private Long	numeroEndoso;
	private Long	idSolicitud;
	private Long	idVersionEndoso;
	private String	referencia;
	private CentroOperacion centroOperacion;
	private Long	idRemesa;
	private	Integer	idConsecMovtor;
	private	String	cveOrigenRemesa;
	private	String	serieFolioRecibo;
	private Long	numFolioRecibo;
	private Long	idRecibo;
	private Long	idVersionRecibo;
	private Date	fechaMovimientoRecibo;
	private Double 	porcentajeParticipanteAgente;
	private Double	importePrimaNeta;
	private Double	imprcgosPagoFr;
	private Double 	importeComisionAgente;//Campo a considerar para calcular el pago de comisiones.
	private	String	claveOrigenApli;
	private	String 	claveOrigenMovto;
	private Date	fechaCortePagoComision;
	private ValorCatalogoAgentes estatus;
	private Long	idUsuarioInteg;
	private	Date	fhIntegracion;
	private Date	fhAplicacion;
	private ValorCatalogoAgentes naturalezaConcepto;
	private ValorCatalogoAgentes tipoMovimientoCobranza;
	private Date 	fTransfDepv;
	private Long	idTransacDepv;
	private Long	idTransaccOrig;
	private Agente	agenteOrigen;
	private Long	idCobertura;
	private	String	esFacultativo;
	private Integer anioVigenciaPoliza;
	private String	cveSistAdmon;
	private String 	cveExpCalcDiv;
	private Double	importePrimaDCP;
	private Double	importePrimaTotal;
	private String	cveTcPTOACO;
	private Long	idConceptoO;
	private String	esMasivo;
	private Double 	importeBaseCalculo;
	private Float	porcentajeComisionAgente;
	private Long	numMovtoManAgte;
	private	Double	impPagoAntImptos;
	private	Float	porcentajeIva;
	private	Float	porcentajeIvaRetenido;
	private	Float	porcentajeISRRetenido;
	private	Double	importeIva;
	private	Double	importeIvaRetenido;
	private Double	importeISR;
	private	String	claveSaldoImpto;
	private String	esImpuesto;
	private String	anioMesPago;
	public MovimientoAgente(){}
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idtoAgenteMovimientos_seq")
	@SequenceGenerator(name="idtoAgenteMovimientos_seq", sequenceName="MIDAS.idtoAgenteMovimientos_seq",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(name="IDEMPRESA")
	public Long getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	//FIXME
	@ManyToOne(fetch=FetchType.LAZY,targetEntity=Agente.class)
	@JoinColumn(name="IDAGENTE")
	public Agente getAgente() {
		return agente;
	}
	public void setAgente(Agente agente) {
		this.agente = agente;
	}
	@Temporal(TemporalType.DATE)
	@Column(name="FEHCACORTEEDOCTA")
	public Date getFechaCorte() {
		return fechaCorte;
	}
	public void setFechaCorte(Date fechaCorte) {
		this.fechaCorte = fechaCorte;
	}
	@Column(name="IDCONSECMOVTO")
	public Integer getIdConsecMovto() {
		return idConsecMovto;
	}
	public void setIdConsecMovto(Integer idConsecMovto) {
		this.idConsecMovto = idConsecMovto;
	}
	@Column(name="ANIOMES")
	public String getAnioMes() {
		return anioMes;
	}
	public void setAnioMes(String anioMes) {
		this.anioMes = anioMes;
	}
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDCONCEPTO")
	public ValorCatalogoAgentes getConcepto() {
		return concepto;
	}
	public void setConcepto(ValorCatalogoAgentes concepto) {
		this.concepto = concepto;
	}
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDMONEDA")
	public ValorCatalogoAgentes getMoneda() {
		return moneda;
	}
	public void setMoneda(ValorCatalogoAgentes moneda) {
		this.moneda = moneda;
	}
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDMONEDAORIGEN")
	public ValorCatalogoAgentes getMonedaOrigen() {
		return monedaOrigen;
	}
	public void setMonedaOrigen(ValorCatalogoAgentes monedaOrigen) {
		this.monedaOrigen = monedaOrigen;
	}
	@Column(name="TIPOCAMBIO")
	public Double getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	@Column(name="DESCRIPCIONMOVTO")
	public String getDescripcionMotivo() {
		return descripcionMotivo;
	}
	public void setDescripcionMotivo(String descripcionMotivo) {
		this.descripcionMotivo = descripcionMotivo;
	}
	@Column(name="IDRAMOCONTABLE")
	public Long getIdRamoContable() {
		return idRamoContable;
	}
	public void setIdRamoContable(Long idRamoContable) {
		this.idRamoContable = idRamoContable;
	}
	@Column(name="IDSUBRAMOCONTABLE")
	public Long getIdSubramoContable() {
		return idSubramoContable;
	}
	public void setIdSubramoContable(Long idSubramoContable) {
		this.idSubramoContable = idSubramoContable;
	}
	@Column(name="IDLINEANEGOCIO")
	public Long getIdLineaNegocio() {
		return idLineaNegocio;
	}
	public void setIdLineaNegocio(Long idLineaNegocio) {
		this.idLineaNegocio = idLineaNegocio;
	}
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAMOVIMIENTO")
	public Date getFechaMovimiento() {
		return fechaMovimiento;
	}
	public void setFechaMovimiento(Date fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
	}
	@Column(name="IDCENTROEMISOR")
	public Long getIdCentroEmisor() {
		return idCentroEmisor;
	}
	public void setIdCentroEmisor(Long idCentroEmisor) {
		this.idCentroEmisor = idCentroEmisor;
	}
	@Column(name="NUMPOLIZA")
	public Long getNumPoliza() {
		return numPoliza;
	}
	public void setNumPoliza(Long numPoliza) {
		this.numPoliza = numPoliza;
	}
	@Column(name="NUMRENOVACIONPOLIZA")
	public Integer getNumRenovacionPoliza() {
		return numRenovacionPoliza;
	}
	public void setNumRenovacionPoliza(Integer numRenovacionPoliza) {
		this.numRenovacionPoliza = numRenovacionPoliza;
	}
	@Column(name="IDCOTIZACION")
	public Long getIdCotizacion() {
		return idCotizacion;
	}
	public void setIdCotizacion(Long idCotizacion) {
		this.idCotizacion = idCotizacion;
	}
	@Column(name="IDVERSIONPOL")
	public Long getIdVersionPol() {
		return idVersionPol;
	}
	public void setIdVersionPol(Long idVersionPol) {
		this.idVersionPol = idVersionPol;
	}
	@Column(name="IDCENTROEMISORE")
	public Long getIdCentroEmisorE() {
		return idCentroEmisorE;
	}
	public void setIdCentroEmisorE(Long idCentroEmisorE) {
		this.idCentroEmisorE = idCentroEmisorE;
	}
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="TIPOENDOSO")
	public ValorCatalogoAgentes getTipoEndoso() {
		return tipoEndoso;
	}
	public void setTipoEndoso(ValorCatalogoAgentes tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}	
	@Column(name="NUMEROENDOSO")
	public Long getNumeroEndoso() {
		return numeroEndoso;
	}
	public void setNumeroEndoso(Long numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}
	@Column(name="IDSOLICITUD")
	public Long getIdSolicitud() {
		return idSolicitud;
	}
	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}
	@Column(name="IDVERSIONENDOSO")
	public Long getIdVersionEndoso() {
		return idVersionEndoso;
	}
	public void setIdVersionEndoso(Long idVersionEndoso) {
		this.idVersionEndoso = idVersionEndoso;
	}
	@Column(name="REFERENCIA")
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	@ManyToOne(fetch=FetchType.LAZY,targetEntity=CentroOperacion.class)
	@JoinColumn(name="IDCENTROOPERACION")
	public CentroOperacion getCentroOperacion() {
		return centroOperacion;
	}
	public void setCentroOperacion(CentroOperacion centroOperacion) {
		this.centroOperacion = centroOperacion;
	}
	@Column(name="IDREMESA")
	public Long getIdRemesa() {
		return idRemesa;
	}
	public void setIdRemesa(Long idRemesa) {
		this.idRemesa = idRemesa;
	}
	@Column(name="IDCONSECMOVTOR")
	public Integer getIdConsecMovtor() {
		return idConsecMovtor;
	}
	public void setIdConsecMovtor(Integer idConsecMovtor) {
		this.idConsecMovtor = idConsecMovtor;
	}
	@Column(name="CLAVEORIGENREMESA")
	public String getCveOrigenRemesa() {
		return cveOrigenRemesa;
	}
	public void setCveOrigenRemesa(String cveOrigenRemesa) {
		this.cveOrigenRemesa = cveOrigenRemesa;
	}
	@Column(name="SERIEFOLIORBO")
	public String getSerieFolioRecibo() {
		return serieFolioRecibo;
	}
	public void setSerieFolioRecibo(String serieFolioRecibo) {
		this.serieFolioRecibo = serieFolioRecibo;
	}
	@Column(name="NUMFOLIORBO")
	public Long getNumFolioRecibo() {
		return numFolioRecibo;
	}
	public void setNumFolioRecibo(Long numFolioRecibo) {
		this.numFolioRecibo = numFolioRecibo;
	}
	@Column(name="IDRECIBO")
	public Long getIdRecibo() {
		return idRecibo;
	}
	public void setIdRecibo(Long idRecibo) {
		this.idRecibo = idRecibo;
	}
	@Column(name="IDVERSIONRBO")
	public Long getIdVersionRecibo() {
		return idVersionRecibo;
	}
	public void setIdVersionRecibo(Long idVersionRecibo) {
		this.idVersionRecibo = idVersionRecibo;
	}
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAVENCIMIENTOREC")
	public Date getFechaMovimientoRecibo() {
		return fechaMovimientoRecibo;
	}
	public void setFechaMovimientoRecibo(Date fechaMovimientoRecibo) {
		this.fechaMovimientoRecibo = fechaMovimientoRecibo;
	}
	@Column(name="PORCENTAJEPARTAGENTE")
	public Double getPorcentajeParticipanteAgente() {
		return porcentajeParticipanteAgente;
	}
	public void setPorcentajeParticipanteAgente(Double porcentajeParticipanteAgente) {
		this.porcentajeParticipanteAgente = porcentajeParticipanteAgente;
	}
	@Column(name="IMPORTEPRIMANETA")
	public Double getImportePrimaNeta() {
		return importePrimaNeta;
	}
	public void setImportePrimaNeta(Double importePrimaNeta) {
		this.importePrimaNeta = importePrimaNeta;
	}
	@Column(name="IMPRCGOSSPAGOFR")
	public Double getImprcgosPagoFr() {
		return imprcgosPagoFr;
	}
	public void setImprcgosPagoFr(Double imprcgosPagoFr) {
		this.imprcgosPagoFr = imprcgosPagoFr;
	}
	@Column(name="IMPORTECOMISIONAGENTE")
	public Double getImporteComisionAgente() {
		return importeComisionAgente;
	}
	public void setImporteComisionAgente(Double importeComisionAgente) {
		this.importeComisionAgente = importeComisionAgente;
	}
	@Column(name="CLAVEORIGENAPLIC")
	public String getClaveOrigenApli() {
		return claveOrigenApli;
	}
	public void setClaveOrigenApli(String claveOrigenApli) {
		this.claveOrigenApli = claveOrigenApli;
	}
	@Column(name="CLAVEORIGENMOVTO")
	public String getClaveOrigenMovto() {
		return claveOrigenMovto;
	}
	public void setClaveOrigenMovto(String claveOrigenMovto) {
		this.claveOrigenMovto = claveOrigenMovto;
	}
	@Temporal(TemporalType.DATE)
	@Column(name="FECHACORTEPAGOCOM")
	public Date getFechaCortePagoComision() {
		return fechaCortePagoComision;
	}
	public void setFechaCortePagoComision(Date fechaCortePagoComision) {
		this.fechaCortePagoComision = fechaCortePagoComision;
	}
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="ESTATUSMOVIMIENTO")
	public ValorCatalogoAgentes getEstatus() {
		return estatus;
	}
	public void setEstatus(ValorCatalogoAgentes estatus) {
		this.estatus = estatus;
	}
	@Column(name="IDUSUARIOINTEGEG")
	public Long getIdUsuarioInteg() {
		return idUsuarioInteg;
	}
	public void setIdUsuarioInteg(Long idUsuarioInteg) {
		this.idUsuarioInteg = idUsuarioInteg;
	}
	@Temporal(TemporalType.DATE)
	@Column(name="FHINTEGRACION")
	public Date getFhIntegracion() {
		return fhIntegracion;
	}
	public void setFhIntegracion(Date fhIntegracion) {
		this.fhIntegracion = fhIntegracion;
	}
	@Temporal(TemporalType.DATE)
	@Column(name="FHAPLICACION")
	public Date getFhAplicacion() {
		return fhAplicacion;
	}
	public void setFhAplicacion(Date fhAplicacion) {
		this.fhAplicacion = fhAplicacion;
	}
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="NATURALEZACONCEPTO")
	public ValorCatalogoAgentes getNaturalezaConcepto() {
		return naturalezaConcepto;
	}
	public void setNaturalezaConcepto(ValorCatalogoAgentes naturalezaConcepto) {
		this.naturalezaConcepto = naturalezaConcepto;
	}
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="TIPOMOVTOCOBRANZA")
	public ValorCatalogoAgentes getTipoMovimientoCobranza() {
		return tipoMovimientoCobranza;
	}
	public void setTipoMovimientoCobranza(
			ValorCatalogoAgentes tipoMovimientoCobranza) {
		this.tipoMovimientoCobranza = tipoMovimientoCobranza;
	}
	@Temporal(TemporalType.DATE)
	@Column(name="FTRANSFDEPV")
	public Date getfTransfDepv() {
		return fTransfDepv;
	}
	public void setfTransfDepv(Date fTransfDepv) {
		this.fTransfDepv = fTransfDepv;
	}
	@Column(name="IDTRANSACDEPV")
	public Long getIdTransacDepv() {
		return idTransacDepv;
	}
	public void setIdTransacDepv(Long idTransacDepv) {
		this.idTransacDepv = idTransacDepv;
	}
	@Column(name="IDTREANSACCORIG")
	public Long getIdTransaccOrig() {
		return idTransaccOrig;
	}
	public void setIdTransaccOrig(Long idTransaccOrig) {
		this.idTransaccOrig = idTransaccOrig;
	}
	@ManyToOne(fetch=FetchType.LAZY,targetEntity=Agente.class)
	@JoinColumn(name="IDAGENTEORIGEN")
	public Agente getAgenteOrigen() {
		return agenteOrigen;
	}
	public void setAgenteOrigen(Agente agenteOrigen) {
		this.agenteOrigen = agenteOrigen;
	}
	@Column(name="IDCOBERTURA")
	public Long getIdCobertura() {
		return idCobertura;
	}
	public void setIdCobertura(Long idCobertura) {
		this.idCobertura = idCobertura;
	}
	@Column(name="ESFACULTATIVO")
	public String getEsFacultativo() {
		return esFacultativo;
	}
	public void setEsFacultativo(String esFacultativo) {
		this.esFacultativo = esFacultativo;
	}
	@Column(name="ANIOVIGENCIAPOLIZA")
	public Integer getAnioVigenciaPoliza() {
		return anioVigenciaPoliza;
	}
	public void setAnioVigenciaPoliza(Integer anioVigenciaPoliza) {
		this.anioVigenciaPoliza = anioVigenciaPoliza;
	}
	@Column(name="CLAVESISTADMON")
	public String getCveSistAdmon() {
		return cveSistAdmon;
	}
	public void setCveSistAdmon(String cveSistAdmon) {
		this.cveSistAdmon = cveSistAdmon;
	}
	@Column(name="CLAVEEXPCALCDIV")
	public String getCveExpCalcDiv(){
		return cveExpCalcDiv;
	}
	public void setCveExpCalcDiv(String cveExpCalcDiv){
		this.cveExpCalcDiv=cveExpCalcDiv;
	}
	@Column(name="IMPORTEPRIMADCP")
	public Double getImportePrimaDCP() {
		return importePrimaDCP;
	}
	public void setImportePrimaDCP(Double importePrimaDCP) {
		this.importePrimaDCP = importePrimaDCP;
	}
	@Column(name="IMPORTEPRIMATOTAL")
	public Double getImportePrimaTotal() {
		return importePrimaTotal;
	}
	public void setImportePrimaTotal(Double importePrimaTotal) {
		this.importePrimaTotal = importePrimaTotal;
	}
	@Column(name="CVETCPTOACO")
	public String getCveTcPTOACO() {
		return cveTcPTOACO;
	}
	public void setCveTcPTOACO(String cveTcPTOACO) {
		this.cveTcPTOACO = cveTcPTOACO;
	}
	@Column(name="IDCONCEPTOO")
	public Long getIdConceptoO() {
		return idConceptoO;
	}
	public void setIdConceptoO(Long idConceptoO) {
		this.idConceptoO = idConceptoO;
	}
	@Column(name="ESMASIVO")
	public String getEsMasivo() {
		return esMasivo;
	}
	public void setEsMasivo(String esMasivo) {
		this.esMasivo = esMasivo;
	}
	@Column(name="IMPORTEBASECALCULO")
	public Double getImporteBaseCalculo() {
		return importeBaseCalculo;
	}
	public void setImporteBaseCalculo(Double importeBaseCalculo) {
		this.importeBaseCalculo = importeBaseCalculo;
	}
	@Column(name="PORCENTAJECOMISIONAGENTE")
	public Float getPorcentajeComisionAgente() {
		return porcentajeComisionAgente;
	}
	public void setPorcentajeComisionAgente(Float porcentajeComisionAgente) {
		this.porcentajeComisionAgente = porcentajeComisionAgente;
	}
	@Column(name="NUMMOVTOMANAGENTE")
	public Long getNumMovtoManAgte() {
		return numMovtoManAgte;
	}
	public void setNumMovtoManAgte(Long numMovtoManAgte) {
		this.numMovtoManAgte = numMovtoManAgte;
	}
	@Column(name="IMPORTEPAGOANTIMPTOS")
	public Double getImpPagoAntImptos() {
		return impPagoAntImptos;
	}
	public void setImpPagoAntImptos(Double impPagoAntImptos) {
		this.impPagoAntImptos = impPagoAntImptos;
	}
	@Column(name="PORCENTAJEIVA")
	public Float getPorcentajeIva() {
		return porcentajeIva;
	}
	public void setPorcentajeIva(Float porcentajeIva) {
		this.porcentajeIva = porcentajeIva;
	}
	@Column(name="PORCENTAGEIVARETENIDO")
	public Float getPorcentajeIvaRetenido() {
		return porcentajeIvaRetenido;
	}
	public void setPorcentajeIvaRetenido(Float porcentajeIvaRetenido) {
		this.porcentajeIvaRetenido = porcentajeIvaRetenido;
	}
	@Column(name="PORCENTAJEISRRET")
	public Float getPorcentajeISRRetenido() {
		return porcentajeISRRetenido;
	}
	public void setPorcentajeISRRetenido(Float porcentajeISRRetenido) {
		this.porcentajeISRRetenido = porcentajeISRRetenido;
	}
	@Column(name="IMPORTEIVA")
	public Double getImporteIva() {
		return importeIva;
	}
	public void setImporteIva(Double importeIva) {
		this.importeIva = importeIva;
	}
	@Column(name="IMPORTEIVARET")
	public Double getImporteIvaRetenido() {
		return importeIvaRetenido;
	}
	public void setImporteIvaRetenido(Double importeIvaRetenido) {
		this.importeIvaRetenido = importeIvaRetenido;
	}
	@Column(name="IMPORTEISR")
	public Double getImporteISR() {
		return importeISR;
	}
	public void setImporteISR(Double importeISR) {
		this.importeISR = importeISR;
	}
	@Column(name="CVESDOIMPTO")
	public String getClaveSaldoImpto() {
		return claveSaldoImpto;
	}
	public void setClaveSaldoImpto(String claveSaldoImpto) {
		this.claveSaldoImpto = claveSaldoImpto;
	}
	@Column(name="ESIMPUESTO")
	public String getEsImpuesto() {
		return esImpuesto;
	}
	public void setEsImpuesto(String esImpuesto) {
		this.esImpuesto = esImpuesto;
	}
	@Column(name="ANIOMESPAGO")
	public String getAnioMesPago() {
		return anioMesPago;
	}
	public void setAnioMesPago(String anioMesPago) {
		this.anioMesPago = anioMesPago;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		// TODO Auto-generated method stub
		return id;
	}
	@Override
	public String getValue() {
		return null;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
}
