package mx.com.afirme.midas2.domain.calculos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;

@Table(name="TOCALCULOCOMISIONEJECUCION", schema="MIDAS")
@Entity(name="CalculoComisionEjecucion")
public class CalculoComisionEjecucion implements Serializable, Entidad{

	
	private static final long serialVersionUID = 1L;
	private Long id;
	private Long idConfiguracion;
	private String queryAgentes;
	private ValorCatalogoAgentes modoEjecucion;
	private ValorCatalogoAgentes estatusEjecucion;
	private Date fechaEjecucion;
	private Long idCalculoComisiones;
	private Integer porcentajeAvanceAprox;
	private Date fechaFinEjecucion;	
	private String usuario;
	private String Descripcion;
	private Integer estatusEnvio;
	
	@Id
	@SequenceGenerator(name="TOCALCULOCOMIS_EJECUCION_SEQ",sequenceName="MIDAS.TOCALCULOCOMIS_EJECUCION_SEQ",allocationSize=1)
	@GeneratedValue(generator="TOCALCULOCOMIS_EJECUCION_SEQ",strategy=GenerationType.SEQUENCE)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="IDCONFIGURACION")
	public Long getIdConfiguracion() {
		return idConfiguracion;
	}
	public void setIdConfiguracion(Long idConfiguracion) {
		this.idConfiguracion = idConfiguracion;
	}
	
	@Column(name="QUERYAGENTES")
	public String getQueryAgentes() {
		return queryAgentes;
	}
	public void setQueryAgentes(String queryAgentes) {
		this.queryAgentes = queryAgentes;
	}
	
	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDMODOEJECUCION")
	public ValorCatalogoAgentes getModoEjecucion() {
		return modoEjecucion;
	}
	public void setModoEjecucion(ValorCatalogoAgentes modoEjecucion) {
		this.modoEjecucion = modoEjecucion;
	}
	
	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDESTATUSEJECUCION")
	public ValorCatalogoAgentes getEstatusEjecucion() {
		return estatusEjecucion;
	}
	public void setEstatusEjecucion(ValorCatalogoAgentes estatusEjecucion) {
		this.estatusEjecucion = estatusEjecucion;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAEJECUCION")
	public Date getFechaEjecucion() {
		return fechaEjecucion;
	}
	public void setFechaEjecucion(Date fechaEjecucion) {
		this.fechaEjecucion = fechaEjecucion;
	}
	
	@Column(name="IDCALCULOCOMISIONES")
	public Long getIdCalculoComisiones() {
		return idCalculoComisiones;
	}
	public void setIdCalculoComisiones(Long idCalculoComisiones) {
		this.idCalculoComisiones = idCalculoComisiones;
	}
	
	@Column(name="PCTAVANCEAPROX")
	public Integer getPorcentajeAvanceAprox() {
		return porcentajeAvanceAprox;
	}
	public void setPorcentajeAvanceAprox(Integer porcentajeAvanceAprox) {
		this.porcentajeAvanceAprox = porcentajeAvanceAprox;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHAFINALIZACALCULO")
	public Date getFechaFinEjecucion() {
		return fechaFinEjecucion;
	}
	public void setFechaFinEjecucion(Date fechaFinEjecucion) {
		this.fechaFinEjecucion = fechaFinEjecucion;
	}
	
	@Column(name="USUARIO")	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	@Column(name="DESCRIPCION")	
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}	
	
	@Column(name="ESTATUSENVIO")	
	public Integer getEstatusEnvio() {
		return estatusEnvio;
	}
	public void setEstatusEnvio(Integer estatusEnvio) {
		this.estatusEnvio = estatusEnvio;
	}
	
	@Override
	public Long getKey() {
		
		return id;
	}
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
}
