package mx.com.afirme.midas2.domain.calculos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.comisiones.ConfigComisiones;
import mx.com.afirme.midas2.util.StaticCommonVariables;
/**
 * Entidad para el calculo de comisiones
 * @author vmhersil
 *
 */
@Entity(name="CalculoComisiones")
@Table(schema=StaticCommonVariables.DEFAULT_SCHEMA,name="toCalculoComisiones")
@SqlResultSetMapping(name="calculoComisionesView",entities={
	@EntityResult(entityClass=CalculoComisiones.class,fields={
		@FieldResult(name="id",column="id"),
		@FieldResult(name="numComisionesProcesadas",column="numComisionesProcesadas"),
		@FieldResult(name="importeTotal",column="importeTotal"),
		@FieldResult(name="fechaCalculo",column="fechaCalculo"),
		@FieldResult(name="configuracionComisiones.id",column="idConfigComisiones")
	})
})
public class CalculoComisiones implements Serializable,Entidad{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8758578782308432837L;
	private Long id;
	private ConfigComisiones configuracionComisiones;
	private Date fechaCalculo;
	private String fechaCalculoString;
	private Integer numComisionesProcesadas;
	private Double importeTotal;
	private String importeTotalString;
	private ValorCatalogoAgentes claveEstatus;
	private Date fechaCalculoFin;
	private String fechaCalculoFinString;
	private Double importeTruncate;
	private Boolean enProceso;
	
	public CalculoComisiones(){}
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idToCalculoComisiones_seq")
	@SequenceGenerator(name="idToCalculoComisiones_seq", sequenceName="MIDAS.idToCalculoComisiones_seq",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ConfigComisiones.class)
	@JoinColumn(name="IDCONFIGURACIONCOMISIONES")
	public ConfigComisiones getConfiguracionComisiones() {
		return configuracionComisiones;
	}
	public void setConfiguracionComisiones(ConfigComisiones configuracionComisiones) {
		this.configuracionComisiones = configuracionComisiones;
	}
	@Temporal(TemporalType.DATE)
	@Column(name="FECHACALCULO")
	public Date getFechaCalculo() {
		return fechaCalculo;
	}

	public void setFechaCalculo(Date fechaCalculo) {
		SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
		if(fechaCalculo!=null){			
			this.fechaCalculoString = format.format(fechaCalculo);
		}		
		this.fechaCalculo = fechaCalculo;
	}
	@Column(name="NUMCOMISIONESPROCESADAS")
	public Integer getNumComisionesProcesadas() {
		return numComisionesProcesadas;
	}

	public void setNumComisionesProcesadas(Integer numComisionesProcesadas) {
		this.numComisionesProcesadas = numComisionesProcesadas;
	}
	@Column(name="IMPORTETOTAL")
	public Double getImporteTotal() {
		return importeTotal;
	}

	public void setImporteTotal(Double importeTotal) {
		if(importeTotal!=null){
			DecimalFormat doubleFormat = new DecimalFormat("#.##");
			this.importeTotalString = doubleFormat.format(importeTotal);
		}
		if(importeTotal!=null){
			this.importeTruncate =  truncateDecimal(importeTotal,2).doubleValue();
		}
		this.importeTotal = importeTotal;
	}
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="CLAVEESTATUS")
	public ValorCatalogoAgentes getClaveEstatus() {
		return claveEstatus;
	}

	public void setClaveEstatus(ValorCatalogoAgentes claveEstatus) {
		this.claveEstatus = claveEstatus;
	}
	@Transient
	public Date getFechaCalculoFin() {
		return fechaCalculoFin;
	}
	public void setFechaCalculoFin(Date fechaCalculoFin) {
		SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
		if(fechaCalculo!=null){			
			this.fechaCalculoFinString = format.format(fechaCalculoFin);
		}	
		this.fechaCalculoFin = fechaCalculoFin;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}
	@Override
	public String getValue() {
		return null;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	
	@Transient
	public String getFechaCalculoString() {
		return fechaCalculoString;
	}
	
	public void setFechaCalculoString(String fechaCalculoString) {
		this.fechaCalculoString = fechaCalculoString;
	}
	
	@Transient
	public String getImporteTotalString() {
		return importeTotalString;
	}
	
	public void setImporteTotalString(String importeTotalString) {
		this.importeTotalString = importeTotalString;
	}
	
	@Transient
	public String getFechaCalculoFinString() {
		return fechaCalculoFinString;
	}
	
	public void setFechaCalculoFinString(String fechaCalculoFinString) {
		this.fechaCalculoFinString = fechaCalculoFinString;
	}
	/**
	 * se agrega este campo para que en el fron se trunque a dos decimales el importe total
	 */
	@Transient
	public Double getImporteTruncate() {
		return importeTruncate;
	}
	public void setImporteTruncate(Double importeTruncate) {
		this.importeTruncate = importeTruncate;
	}
		
	@Column(name="EN_PROCESO", precision=1, scale=0)
	public Boolean isEnProceso() {
		return enProceso;
	}
	
	public void setEnProceso(Boolean enProceso) {
		this.enProceso = enProceso;
	}
	
	private static BigDecimal truncateDecimal(double x,int numberofDecimals)
	{
	    if ( x > 0) {
	        return new BigDecimal(String.valueOf(x)).setScale(numberofDecimals, BigDecimal.ROUND_FLOOR);
	    } else {
	        return new BigDecimal(String.valueOf(x)).setScale(numberofDecimals, BigDecimal.ROUND_CEILING);
	    }
	}
}
