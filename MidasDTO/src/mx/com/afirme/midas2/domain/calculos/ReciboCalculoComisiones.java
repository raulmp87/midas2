package mx.com.afirme.midas2.domain.calculos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.util.StaticCommonVariables;
@Entity(name="ReciboCalculoComisiones")
@Table(schema=StaticCommonVariables.DEFAULT_SCHEMA,name="toReciboCalculoComisiones")
@SqlResultSetMapping(name="reciboCalculoComisionesView",entities={
	@EntityResult(entityClass=ReciboCalculoComisiones.class,fields={
		@FieldResult(name="idRecibo",column="idRecibo"),
		@FieldResult(name="promotoria.id",column="idPromotoria"),
		@FieldResult(name="importeComision",column="importeComision"),
		@FieldResult(name="agente.id",column="idAgente"),
		@FieldResult(name="detalleCalculoComisiones.id",column="idDetalleCalculoComisiones"),
		@FieldResult(name="id",column="id")
	})
})
public class ReciboCalculoComisiones implements Serializable,Entidad{
	/**
	 * 
	 */
	private static final long serialVersionUID = 829215929492524637L;
	private Long id;
	private DetalleCalculoComisiones detalleCalculoComisiones;
	private Agente agente;
	private Promotoria promotoria;
	private Long idRecibo;
	private Double importeComision;
	private Date fechaVencimiento;
	
	public ReciboCalculoComisiones(){}
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idToReciboCalculoCom_seq")
	@SequenceGenerator(name="idToReciboCalculoCom_seq", sequenceName="MIDAS.idToReciboCalculoCom_seq",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=DetalleCalculoComisiones.class)
	@JoinColumn(name="IDDETALLECALCULOCOMISIONES")
	public DetalleCalculoComisiones getDetalleCalculoComisiones() {
		return detalleCalculoComisiones;
	}

	public void setDetalleCalculoComisiones(
			DetalleCalculoComisiones detalleCalculoComisiones) {
		this.detalleCalculoComisiones = detalleCalculoComisiones;
	}
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=Agente.class)
	@JoinColumn(name="IDAGENTE")
	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=Promotoria.class)
	@JoinColumn(name="IDPROMOTORIA")
	public Promotoria getPromotoria() {
		return promotoria;
	}

	public void setPromotoria(Promotoria promotoria) {
		this.promotoria = promotoria;
	}
	@Column(name="IDRECIBO")
	public Long getIdRecibo() {
		return idRecibo;
	}

	public void setIdRecibo(Long idRecibo) {
		this.idRecibo = idRecibo;
	}
	@Column(name="PRIMANETA")
	public Double getImporteComision() {
		return importeComision;
	}

	public void setImporteComision(Double importeComision) {
		this.importeComision = importeComision;
	}
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAVENCIMIENTO")
	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return null;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
}
