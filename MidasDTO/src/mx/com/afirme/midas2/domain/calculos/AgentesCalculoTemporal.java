package mx.com.afirme.midas2.domain.calculos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.util.StaticCommonVariables;
/**
 * 
 * @author vmhersil
 *
 */
@Entity(name="AgentesCalculoTemporal")
@Table(schema=StaticCommonVariables.DEFAULT_SCHEMA,name="Gt_AgentesCalculoTemporal")
public class AgentesCalculoTemporal implements Serializable,Entidad{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8258667927999067902L;
	private Long id;
	private Long idRegistro;
	private String	catalogo;
	private Long idCalculo;
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idGt_AgentesCalcTemporal_seq")
	@SequenceGenerator(name="idGt_AgentesCalcTemporal_seq", sequenceName="MIDAS.idGt_AgentesCalcTemporal_seq",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@Column(name="IDREGISTRO")
	public Long getIdRegistro() {
		return idRegistro;
	}

	public void setIdRegistro(Long idRegistro) {
		this.idRegistro = idRegistro;
	}

	@Column(name="catalogo")
	public String getCatalogo() {
		return catalogo;
	}

	public void setCatalogo(String catalogo) {
		this.catalogo = catalogo;
	}
	
	@Column(name="idCalculo")
	public Long getIdCalculo() {
		return idCalculo;
	}

	public void setIdCalculo(Long idCalculo) {
		this.idCalculo = idCalculo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
}
