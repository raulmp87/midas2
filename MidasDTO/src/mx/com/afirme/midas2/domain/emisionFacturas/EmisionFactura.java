package mx.com.afirme.midas2.domain.emisionFacturas;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.personadireccion.CiudadMidas;
import mx.com.afirme.midas2.domain.personadireccion.ColoniaMidas;
import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Ingreso;

@Entity(name = "EmisionFactura")
@Table(name = "TOSN_EMISION_INFO_FACTURA", schema = "MIDAS")
public class EmisionFactura extends MidasAbstracto{
	
	public static enum TipoPersona{
		ASEGURADO("ASEG"),
		OTRA_PERSONA("OTRAPER"),	
		PROVEEDOR("PROVEE");
		
		private String value;
		
		private TipoPersona(String value){
			this.value=value;
		}
		
		public String getValue(){
			return value;
		}
	}
	
	public static enum EstatusFactura{
		TRAMITE("TRAMITE"),
		PROCESO("PROC"),	
		FACTURADA("FACT"),
		RECHAZADA("RECHAZ"),
		RE_FACTURADA("REFACT"),
		CANCELACION_EN_TRAMITE("CANCTRAMIT"),
		CANCELADA("CANC");
		
		private String value;
		
		private EstatusFactura(String value){
			this.value=value;
		}
		
		public String getValue(){
			return value;
		}
	}
	
	private static String CONCEPTO_DEDUCIBLE = "DEDUCIBLE DEL [SINIESTRO] Siniestro DE LA PÓLIZA [POLIZA]";
	private static String CONCEPTO_SALVAMENTO = "VENTA DEL VEHÍCULO USADO, MARCA: , TIPO: , MODELO: , SERIE: , MOTOR: , DEL SINIESTRO [SINIESTRO], SE VENDE EN LAS CONDICIONES QUE SE ENCUENTRAN";
	private static String CONCEPTO_COMPANIA = "RECUPERACIÓN DEL SINIESTRO [SINIESTRO] DE LA PÓLIZA [POLIZA]";
	private static String CONCEPTO_PROVEEDOR = "DEVOLUCIÓN DE PIEZAS CORRESPONDIENTE AL SINIESTRO [SINIESTRO] DE LA PÓLIZA [POLIZA]";
	private static String CONCEPTO_CRUCERO = "RECUPERACIÓN DEL SINIESTRO [SINIESTRO] DE LA PÓLIZA [POLIZA]";
	private static String CONCEPTO_JURIDICO = "RECUPERACIÓN DEL SINIESTRO [SINIESTRO] DE LA PÓLIZA [POLIZA]";
	
	@Id
	@SequenceGenerator(name = "TOSN_EMISION_INFO_FACTURA_SEQ_GENERATOR",allocationSize = 1, sequenceName = "TOSN_EMISION_INFO_FACTURA_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TOSN_EMISION_INFO_FACTURA_SEQ_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Long id ; 
	
	@Column(name = "ESTATUS")	
	private String estatus;
	
	@Column(name = "TIPO_PERSONA")	
	private String tipoPersona;
	
	@Column(name = "NOMBRE_RAZON_SOCIAL")	
	private String nombreRazonSocial;
               
	@Column(name = "RFC")	
	private String rfc;

	@Column(name = "CALLE")	
	private String calle;

	@Column(name = "NUMERO")	
	private String numero;
	
	@OneToOne
	@JoinColumn(name = "COLONIA", referencedColumnName = "COLONY_ID")
	private ColoniaMidas coloniaMidas;
	
	@OneToOne
	@JoinColumn(name = "ESTADO", referencedColumnName = "STATE_ID")
	private EstadoMidas estadoMidas;
	
	@OneToOne
	@JoinColumn(name = "CIUDAD", referencedColumnName = "CITY_ID")
	private CiudadMidas ciudadMidas;
	
	
	@Column(name = "CP")	
	private String cp;
	
	
	@Column(name = "TELEFONO")	
	private String telefono;
	
	@Column(name = "EMAIL")	
	private String email;
	 
	@Column(name = "IMPORTE")	
	private BigDecimal importe;
	
	@Column(name = "IVA")	
	private BigDecimal iva;

	@Column(name = "TOTAL")	
	private BigDecimal totalFactura;

	@Column(name = "METODO_PAGO")	
	private String metodoPago;
	
	@Column(name = "DIGITOS")	
	private Long digitos;
	
	@Column(name = "CONCEPTO")	
	private String concepto;
	
	@Column(name = "DESCRIPCION_FACTURACION")	
	private String descripcionFacturacion;
	
	@Column(name = "OBSERVACIONES_FACTURACION")	
	private String observacionesFacturacion;
	
	@Column(name = "FOLIO_FACTURA")	
	private String folioFactura;
	
	@OneToOne(fetch = FetchType.LAZY )
	@JoinColumn(name = "SINIESTRO", referencedColumnName = "ID")
	private SiniestroCabina siniestroCabina;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumns( { 
        @JoinColumn(name="INGRESO_ID", referencedColumnName="ID", nullable=false, insertable=true, updatable=false) } 
	)
	private Ingreso ingreso;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_GENERACION")	
	private Date fechaGeneracion;
	
	@Column(name = "CODIGO_USUARIO_GENERACION", length = 8, updatable = false)
	private String codigoUsuarioGeneracion; 
	
	@OneToMany(fetch=FetchType.LAZY,  mappedBy = "emisionFactura")
	@JoinFetch(JoinFetchType.OUTER)
	private List<EmisionFacturaHistorico> lstEmisionFacturaHistorico = new ArrayList<EmisionFacturaHistorico>();
	
	
	@Transient
	private String color;
	
	@Transient
	private String marca;
	
	@Transient
	private String modelo;
	
	@Transient
	private String noMotor;
	
	@Transient
	private String noSerie;
	
	@Transient
	private String tipo;
	
	@Transient
	private String colonia;
	
	@Transient
	private String estado;
	
	@Transient
	private String ciudad;

	@Transient
	private Integer estatusCancelacion;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * @param tipoPersona the tipoPersona to set
	 */
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	/**
	 * @return the tipoPersona
	 */
	public String getTipoPersona() {
		return tipoPersona;
	}

	/**
	 * @param nombreRazonSocial the nombreRazonSocial to set
	 */
	public void setNombreRazonSocial(String nombreRazonSocial) {
		this.nombreRazonSocial = nombreRazonSocial;
	}

	/**
	 * @return the nombreRazonSocial
	 */
	public String getNombreRazonSocial() {
		return nombreRazonSocial;
	}

	/**
	 * @param rfc the rfc to set
	 */
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	/**
	 * @return the rfc
	 */
	public String getRfc() {
		return rfc;
	}

	/**
	 * @param calle the calle to set
	 */
	public void setCalle(String calle) {
		this.calle = calle;
	}

	/**
	 * @return the calle
	 */
	public String getCalle() {
		return calle;
	}

	/**
	 * @param numero the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		if(ciudadMidas != null){
			return ciudadMidas.getEstado().getDescripcion();
		}
		return null;
	}


	/**
	 * @param cp the cp to set
	 */
	public void setCp(String cp) {
		this.cp = cp;
	}

	/**
	 * @return the cp
	 */
	public String getCp() {
		return cp;
	}

	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param importe the importe to set
	 */
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	/**
	 * @return the importe
	 */
	public BigDecimal getImporte() {
		return importe;
	}

	/**
	 * @param iva the iva to set
	 */
	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	/**
	 * @return the iva
	 */
	public BigDecimal getIva() {
		return iva;
	}

	/**
	 * @param metodoPago the metodoPago to set
	 */
	public void setMetodoPago(String metodoPago) {
		this.metodoPago = metodoPago;
	}

	/**
	 * @return the metodoPago
	 */
	public String getMetodoPago() {
		return metodoPago;
	}

	/**
	 * @param digitos the digitos to set
	 */
	public void setDigitos(Long digitos) {
		this.digitos = digitos;
	}

	/**
	 * @return the digitos
	 */
	public Long getDigitos() {
		return digitos;
	}

	/**
	 * @param concepto the concepto to set
	 */
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	/**
	 * @return the concepto
	 */
	public String getConcepto() {
		return concepto;
	}

	/**
	 * @param descripcionFacturacion the descripcionFacturacion to set
	 */
	public void setDescripcionFacturacion(String descripcionFacturacion) {
		this.descripcionFacturacion = descripcionFacturacion;
	}

	/**
	 * @return the descripcionFacturacion
	 */
	public String getDescripcionFacturacion() {
		return descripcionFacturacion;
	}

	/**
	 * @param observacionesFacturacion the observacionesFacturacion to set
	 */
	public void setObservacionesFacturacion(String observacionesFacturacion) {
		this.observacionesFacturacion = observacionesFacturacion;
	}

	/**
	 * @return the observacionesFacturacion
	 */
	public String getObservacionesFacturacion() {
		return observacionesFacturacion;
	}

	/**
	 * @param ingresoId the ingresoId to set
	 */
	/*public void setIngresoId(Long ingresoId) {
		this.ingresoId = ingresoId;
	}*/

	/**
	 * @return the ingresoId
	 */
	/*public Long getIngresoId() {
		return ingresoId;
	}*/

	/**
	 * @param folioFactura the folioFactura to set
	 */
	public void setFolioFactura(String folioFactura) {
		this.folioFactura = folioFactura;
	}

	/**
	 * @return the folioFactura
	 */
	public String getFolioFactura() {
		return folioFactura;
	}

	/**
	 * @param totalFactura the totalFactura to set
	 */
	public void setTotalFactura(BigDecimal totalFactura) {
		this.totalFactura = totalFactura;
	}

	/**
	 * @return the totalFactura
	 */
	public BigDecimal getTotalFactura() {
		return totalFactura;
	}
	
	@Override
	public Long getKey() {
		return this.getId();
	}
	
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Long getBusinessKey() {
		return this.getId();
	}
	
	@Override
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmisionFactura other = (EmisionFactura) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	/**
	 * @param ingreso the ingreso to set
	 */
	public void setIngreso(Ingreso ingreso) {
		this.ingreso = ingreso;
	}

	/**
	 * @return the ingreso
	 */
	public Ingreso getIngreso() {
		return ingreso;
	}

	public List<EmisionFacturaHistorico> getLstEmisionFacturaHistorico() {
		return lstEmisionFacturaHistorico;
	}

	public void setLstEmisionFacturaHistorico(
			List<EmisionFacturaHistorico> lstEmisionFacturaHistorico) {
		this.lstEmisionFacturaHistorico = lstEmisionFacturaHistorico;
	}

	public SiniestroCabina getSiniestroCabina() {
		return siniestroCabina;
	}

	public void setSiniestroCabina(SiniestroCabina siniestroCabina) {
		this.siniestroCabina = siniestroCabina;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getNoMotor() {
		return noMotor;
	}

	public void setNoMotor(String noMotor) {
		this.noMotor = noMotor;
	}

	public String getNoSerie() {
		return noSerie;
	}

	public void setNoSerie(String noSerie) {
		this.noSerie = noSerie;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "EmisionFactura [id=" + id + ", estatus=" + estatus
				+ ", tipoPersona=" + tipoPersona + ", nombreRazonSocial="
				+ nombreRazonSocial + ", rfc=" + rfc + ", calle=" + calle
				+ ", numero=" + numero +  ", estado="
				+ estado + ", cp=" + cp + ", telefono="
				+ telefono + ", email=" + email + ", importe=" + importe
				+ ", iva=" + iva + ", totalFactura=" + totalFactura
				+ ", metodoPago=" + metodoPago + ", digitos=" + digitos
				+ ", concepto=" + concepto + ", descripcionFacturacion="
				+ descripcionFacturacion + ", observacionesFacturacion="
				+ observacionesFacturacion + ", folioFactura=" + folioFactura
				+ ", siniestroCabina=" + siniestroCabina + ", ingreso="
				+ ingreso + ", lstEmisionFacturaHistorico="
				+ lstEmisionFacturaHistorico + ", color=" + color + ", marca="
				+ marca + ", modelo=" + modelo + ", noMotor=" + noMotor
				+ ", noSerie=" + noSerie + ", tipo=" + tipo + "]";
	}
	
	public Date getFechaGeneracion() {
		return fechaGeneracion;
	}

	public void setFechaGeneracion(Date fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}

	public String getCodigoUsuarioGeneracion() {
		return codigoUsuarioGeneracion;
	}

	public void setCodigoUsuarioGeneracion(String codigoUsuarioGeneracion) {
		this.codigoUsuarioGeneracion = codigoUsuarioGeneracion;
	}


	public ColoniaMidas getColoniaMidas() {
		return coloniaMidas;
	}

	public void setColoniaMidas(ColoniaMidas coloniaMidas) {
		this.coloniaMidas = coloniaMidas;
	}

	public CiudadMidas getCiudadMidas() {
		return ciudadMidas;
	}

	public void setCiudadMidas(CiudadMidas ciudadMidas) {
		this.ciudadMidas = ciudadMidas;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public EstadoMidas getEstadoMidas() {
		return estadoMidas;
	}

	public void setEstadoMidas(EstadoMidas estadoMidas) {
		this.estadoMidas = estadoMidas;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public Integer getEstatusCancelacion() {
		return estatusCancelacion;
	}

	public void setEstatusCancelacion(Integer estatusCancelacion) {
		this.estatusCancelacion = estatusCancelacion;
	}

}
