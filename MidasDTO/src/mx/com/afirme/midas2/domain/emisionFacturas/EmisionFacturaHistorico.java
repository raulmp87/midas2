package mx.com.afirme.midas2.domain.emisionFacturas;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.EnumBase;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.emisionFacturas.EmisionFactura.EstatusFactura;
import mx.com.afirme.midas2.util.EnumUtil;

@Entity(name="EmisionFacturaHistorico")
@Table(name = "TOSN_EMISION_FACTURA_HIST", schema = "MIDAS")
public class EmisionFacturaHistorico extends MidasAbstracto {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TOSN_EMISION_FACTURA_HIST_SEQ",allocationSize = 1, sequenceName = "TOSN_EMISION_FACTURA_HIST_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TOSN_EMISION_FACTURA_HIST_SEQ")
	@Column(name="ID")
	private Long id;
	
	@Column(name="ESTATUS")
	private String estatus;
	
	@Column(name="MOTIVO_CANCELACION")
	private String motivoCancelacion;
	
	@Column(name="OBSERVACIONES")
	private String observaciones;
	
	@Column(name="FOLIO_FACTURA")
	private String folioFactura;
	
	@ManyToOne(fetch=FetchType.LAZY )
	@JoinColumn(name = "ID_EMISION_FACTURA", referencedColumnName = "ID")
	private EmisionFactura emisionFactura;
	
	@Transient
	private String estatusFacturaDesc;
	
	@Transient
	private String estatusHistoricoDesc;
	
	@Transient
	private String noFactura;
	
	public static enum EstatusEmisionFactCancelacion implements EnumBase<String>{
		FALLAS_INTERNAS("FLADMIN"), 
		DATOS_FISCALES_ERRONEOS("DATFISERR"), 
		DEVOLUCION_SALVAMENTO_CAUSAS_INTERNAS("DEVSAL"), 
		OTRO_MOTIVO("OTR");

		private String codigo;

		EstatusEmisionFactCancelacion(String codigo){
			this.codigo = codigo;
		}			

		@Override
		public String toString(){
			return codigo;
		}

		@Override
		public String getValue() {
			return codigo;
		}

		@Override
		public String getLabel() {
			// TODO Auto-generated method stub
			return null;
		}

	}
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public String getFolioFactura() {
		return folioFactura;
	}
	public void setFolioFactura(String folioFactura) {
		this.folioFactura = folioFactura;
	}
	public EmisionFactura getEmisionFactura() {
		return emisionFactura;
	}
	public void setEmisionFactura(EmisionFactura emisionFactura) {
		this.emisionFactura = emisionFactura;
	}
	
	@Override
	public Long getKey() {
		return id;
	}
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmisionFacturaHistorico other = (EmisionFacturaHistorico) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "EmisionFacturaHistorico [id=" + id + ", estatus=" + estatus
				+ ", observaciones=" + observaciones + ", folioFactura="
				+ folioFactura + ", emisionFactura=" + emisionFactura + "]";
	}
	

	public String getNoFactura() {
		return this.emisionFactura.getFolioFactura();
	}
	public void setNoFactura(String noFactura) {
		this.noFactura = noFactura;
	}
	public String getEstatusHistoricoDesc() {
		for (EstatusEmisionFactCancelacion estatusEmisionFactCancelacion : EstatusEmisionFactCancelacion.values()) {
		     if( estatusEmisionFactCancelacion.getValue().equals(this.motivoCancelacion) ){
		    	 return estatusEmisionFactCancelacion.name().replace("_"," ");
		     }
		 }
		return "";
		
	}
	public void setEstatusHistoricoDesc(String estatusHistoricoDesc) {
		this.estatusHistoricoDesc = estatusHistoricoDesc;
	}
	public String getEstatusFacturaDesc() {
		
		if( this.estatus.equals( EstatusFactura.CANCELACION_EN_TRAMITE.getValue() ) ){
			return "CANCELACION EN TRÁMITE";
		}else{
		
			for (EstatusFactura estatusFactura : EstatusFactura.values()) {
			     if( estatusFactura.getValue().equals(this.estatus) ){
			    	 return estatusFactura.name();
			     }
			 }
			return "";
		}
	}
	public void setEstatusFacturaDesc(String estatusFacturaDesc) {
		this.estatusFacturaDesc = estatusFacturaDesc;
	}
	public String getMotivoCancelacion() {
		return motivoCancelacion;
	}
	public void setMotivoCancelacion(String motivoCancelacion) {
		this.motivoCancelacion = motivoCancelacion;
	}
	
	
	
	

}
