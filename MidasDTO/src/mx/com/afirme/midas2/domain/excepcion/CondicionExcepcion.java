package mx.com.afirme.midas2.domain.excepcion;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;


/**
 * CondicionExcepcion entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOCONDICIONEXCEPCION"
    ,schema="MIDAS"
)

public class CondicionExcepcion  implements java.io.Serializable, Entidad {


	private static final long serialVersionUID = -3715526721281918544L;
	@Id 
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOCONDICIONEXCEPCION_ID_SEQ")
	@SequenceGenerator(name="TOCONDICIONEXCEPCION_ID_SEQ", sequenceName="MIDAS.TOCONDICIONEXCEPCION_ID_SEQ", allocationSize=1)	
    @Column(name="ID", unique=true, nullable=false, precision=8)
	private Long id;
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="IDEXCEPCION", nullable=false) 
	private ExcepcionSuscripcion excepcionSuscripcion;
	@Column(name="DESCRIPCION", length=50)
    private String descripcion;
	 @Column(name="TIPOCONDICION", nullable=false)
     private Integer tipoCondicion;
	 @Column(name="TIPOVALOR", nullable=false, precision=8)
     private short tipoValor;
     @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="condicionExcepcion")
     private List<ValorExcepcion> valoresExcepcion = new ArrayList<ValorExcepcion>(0);

     
    

    // Constructors

    /** default constructor */
    public CondicionExcepcion() {
    }

	/** minimal constructor */
    public CondicionExcepcion(Long id, ExcepcionSuscripcion excepcionSuscripcion, Integer tipoCondicion, short tipoValor) {
        this.id = id;
        this.excepcionSuscripcion = excepcionSuscripcion;
        this.tipoCondicion = tipoCondicion;
        this.tipoValor = tipoValor;
    }
    
    /** full constructor */
    public CondicionExcepcion(Long id, ExcepcionSuscripcion excepcionSuscripcion, String descripcion, Integer tipoCondicion, short tipoValor, List<ValorExcepcion> valoresExcepcion) {
        this.id = id;
        this.excepcionSuscripcion = excepcionSuscripcion;
        this.descripcion = descripcion;
        this.tipoCondicion = tipoCondicion;
        this.tipoValor = tipoValor;
        this.valoresExcepcion = valoresExcepcion;
    }

   
    // Property accessors
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public ExcepcionSuscripcion getExcepcionSuscripcion() {
        return this.excepcionSuscripcion;
    }
    
    public void setExcepcionSuscripcion(ExcepcionSuscripcion excepcionSuscripcion) {
        this.excepcionSuscripcion = excepcionSuscripcion;
    }
 
    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
   

    public Integer getTipoCondicion() {
        return this.tipoCondicion;
    }
    
    public void setTipoCondicion(Integer tipoCondicion) {
        this.tipoCondicion = tipoCondicion;
    }
    
    

    public short getTipoValor() {
        return this.tipoValor;
    }
    
    public void setTipoValor(short tipoValor) {
        this.tipoValor = tipoValor;
    }


    public List<ValorExcepcion> getValorExcepcions() {
        return this.valoresExcepcion;
    }
    
    public void setValorExcepcions(List<ValorExcepcion> valoresExcepcion) {
        this.valoresExcepcion = valoresExcepcion;
    }

    @SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	
	@Override
	public String getValue() {
		return this.descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.id;
	}
   
	public boolean equals(Object obj){
		if(obj instanceof CondicionExcepcion){
			CondicionExcepcion condicion = (CondicionExcepcion)obj;
			return condicion.getId() == this.id;
		}
		return false;
	}

	public void addValor(ValorExcepcion condicion){
		condicion.setCondicionExcepcion(this);
		valoresExcepcion.add(condicion);
	}
	




}