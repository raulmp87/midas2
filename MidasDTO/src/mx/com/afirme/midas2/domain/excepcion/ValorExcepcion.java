package mx.com.afirme.midas2.domain.excepcion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;


/**
 * ValorExcepcion entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOVALOREXCEPCION"
    ,schema="MIDAS"
)

public class ValorExcepcion  implements java.io.Serializable, Entidad {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 4667717719522439403L;
	private Long id;
     private CondicionExcepcion condicionExcepcion;
     private String valor;


    // Constructors

    /** default constructor */
    public ValorExcepcion() {
    }

	/** minimal constructor */
    public ValorExcepcion(Long id, CondicionExcepcion condicionExcepcion) {
        this.id = id;
        this.condicionExcepcion = condicionExcepcion;
    }
    
    /** full constructor */
    public ValorExcepcion(Long id, CondicionExcepcion condicionExcepcion, String valor) {
        this.id = id;
        this.condicionExcepcion = condicionExcepcion;
        this.valor = valor;
    }

   
    // Property accessors
    @Id 
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOVALOREXCEPCION_ID_SEQ")
	@SequenceGenerator(name="TOVALOREXCEPCION_ID_SEQ", sequenceName="MIDAS.TOVALOREXCEPCION_ID_SEQ", allocationSize=1)
    @Column(name="ID", unique=true, nullable=false, precision=8)

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="IDCONDICION", nullable=false)

    public CondicionExcepcion getCondicionExcepcion() {
        return this.condicionExcepcion;
    }
    
    public void setCondicionExcepcion(CondicionExcepcion condicionExcepcion) {
        this.condicionExcepcion = condicionExcepcion;
    }
    
    @Column(name="VALOR", length=50)

    public String getValor() {
        return this.valor;
    }
    
    public void setValor(String valor) {
        this.valor = valor;
    }

    @SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		return this.valor;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValorExcepcion other = (ValorExcepcion) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
   








}