package mx.com.afirme.midas2.domain.excepcion;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.NewItemChecks;


/**
 * Excepcion entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOEXCEPCIONSUSCRIPCION"
    ,schema="MIDAS"
)

public class ExcepcionSuscripcion  implements java.io.Serializable, Entidad {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = -1498751884021556527L;
	 private Long id;
     private String descripcion;
     private Long numero;
     private String correoNotificacion;
     private String cveNegocio;
     private boolean activo;
     private boolean habilitado;
     private boolean requiereAutorizacion;
     private boolean requiereNotificacion;
     private Integer nivelEvaluacion;
     
     private List<CondicionExcepcion> condicionExcepcions = new ArrayList<CondicionExcepcion>(0);


    // Constructors

    /** default constructor */
    public ExcepcionSuscripcion() {    	
    }

	/** minimal constructor */
    public ExcepcionSuscripcion(Long id, Long numero, String cveNegocio, boolean activo) {
        this.id = id;
        this.numero = numero;
        this.cveNegocio = cveNegocio;
        this.activo = activo;
    }
    
    /** full constructor */
    public ExcepcionSuscripcion(Long id, String descripcion, Long numero, String correoNotificacion, String cveNegocio, boolean activo, boolean habilitado, boolean requiereAutorizacion, boolean requiereNotificacion, List<CondicionExcepcion> condicionExcepcions) {
        this.id = id;
        this.descripcion = descripcion;
        this.numero = numero;
        this.correoNotificacion = correoNotificacion;
        this.cveNegocio = cveNegocio;
        this.activo = activo;
        this.habilitado = habilitado;
        this.requiereAutorizacion = requiereAutorizacion;
        this.requiereNotificacion = requiereNotificacion;
        this.condicionExcepcions = condicionExcepcions;
    }

   
    // Property accessors
    @Id 
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOEXCEPCIONSUSCRIPCION_ID_SEQ")
	@SequenceGenerator(name="TOEXCEPCIONSUSCRIPCION_ID_SEQ", sequenceName="MIDAS.TOEXCEPCIONSUSCRIPCION_ID_SEQ", allocationSize=1)	
    @Column(name="ID", unique=true, nullable=false, precision=8)

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    @Column(name="DESCRIPCION", length=50)
    @NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=1, max=50, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public Long getNumero() {
        return this.id + 1000;
    }
    
    @Column(name="CORREONOTIFICACION", length=2000)

    public String getCorreoNotificacion() {
        return this.correoNotificacion;
    }
    
    public void setCorreoNotificacion(String correoNotificacion) {
        this.correoNotificacion = correoNotificacion;
    }
    
    @Column(name="CVENEGOCIO", nullable=false, length=10)

    public String getCveNegocio() {
        return this.cveNegocio;
    }
    
    public void setCveNegocio(String cveNegocio) {
        this.cveNegocio = cveNegocio;
    }
    
    @Column(name="ACTIVO", nullable=false, precision=8)
    public boolean getActivo() {
        return this.activo;
    }
    
    public void setActivo(boolean activo) {
        this.activo = activo;
    }
    
    @Column(name="HABILITADO", precision=8)

    public boolean getHabilitado() {
        return this.habilitado;
    }
    
    public void setHabilitado(boolean habilitado) {
        this.habilitado = habilitado;
    }
    
    @Column(name="REQUIEREAUTORIZACION", precision=8)

    public boolean getRequiereAutorizacion() {
        return this.requiereAutorizacion;
    }
    
    public void setRequiereAutorizacion(boolean requiereAutorizacion) {
        this.requiereAutorizacion = requiereAutorizacion;
    }
    
    @Column(name="REQUIERENOTIFICACION", precision=8)

    public boolean getRequiereNotificacion() {
        return this.requiereNotificacion;
    }
    
    public void setRequiereNotificacion(boolean requiereNotificacion) {
        this.requiereNotificacion = requiereNotificacion;
    }

    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="excepcionSuscripcion")
    public List<CondicionExcepcion> getCondicionExcepcion() {
        return this.condicionExcepcions;
    }
    
    public void setCondicionExcepcion(List<CondicionExcepcion> condicionExcepcions) {
        this.condicionExcepcions = condicionExcepcions;
    }

    @SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExcepcionSuscripcion other = (ExcepcionSuscripcion) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
   
	public void addCondicion(CondicionExcepcion condicion){
		condicion.setExcepcionSuscripcion(this);
		condicionExcepcions.add(condicion);
	}

	@Column(name="NIVEL_EVALUACION", nullable=false)
	public Integer getNivelEvaluacion() {
		return nivelEvaluacion;
	}

	public void setNivelEvaluacion(Integer nivelEvaluacion) {
		this.nivelEvaluacion = nivelEvaluacion;
	}
	
	@Transient
	public boolean editable(){
		return this.id.longValue() >= 100;
	}
	
}