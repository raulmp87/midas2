/**
 * 
 */
package mx.com.afirme.midas2.domain.personadireccion;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.base.StringUtil;

/**
 * @author smvr
 *
 */
@Entity(name = "PersonaFisicaMidas")
@Table(name = "PERSONA_FISICA_MIDAS", schema = "MIDAS")
@DiscriminatorValue("PF")
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class PersonaFisicaMidas extends PersonaMidas{

	private static final long serialVersionUID = 5096305419339162512L;
	
	@Column(name = "APELLIDO_PATERNO", nullable = false, length = 20)
	private String apellidoPaterno;
	
	@Column(name = "APELLIDO_MATERNO", nullable = false, length = 20)
	private String apellidoMaterno;
	
	@Column(name = "CURP", nullable = false, length = 18)
	private String curp;
	
	@Column(name = "CVE_ACTIVIDAD")
	private String cveActividad;
	
	@Column(name = "CVE_ESTADO_CIVIL", nullable = false, length = 1)
	private String cveEstadoCivil ="S";
	
	@Column(name = "CVE_ESTADO_NACIMIENTO", nullable = false, length = 6)
	private String cveEstadoNacimiento;
	
	@Column(name = "CVE_MUNICIPIO_NACIMIENTO", nullable = false, length = 6)
	private String cveMunicipioNacimiento;
	
	@Column(name = "CVE_RAMA_LABORAL")
	private String cveRamaLaboral;
	
	@Column(name = "CVE_SEXO", nullable = false, length = 1)
	private String cveSexo = "M";
	
	@Column(name = "EMPRESA_LABORAL",  length = 80)
	private String empresaLaboral;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_NACIMIENTO", nullable = false, length = 7)
	private Date fechaNacimiento;
	
	@Column(name = "LUGAR_NACIMIENTO")
	private String lugarNacimiento;
	
	@Column(name = "NOMBRE", nullable = false,  length = 40)
	private String nombrePersona;
	
	@Column(name = "PUESTO",  length = 80)
	private String puesto;
	
	@Column(name = "TITULO_PROFESIONAL", length = 10)
	private String tituloProfesional;
	
	
	public PersonaFisicaMidas() {
		super();
		super.setTipoPersona(TIPO_PERSONA.PF.toString());
	}
	
	
	public PersonaFisicaMidas(String nombrePersona, String apellidoPaterno, String apellidoMaterno,
			String curp, Date fechaNacimiento, String codigoRFC, String fechaRFC, String homoclaveRFC, String codigoUsuarioCreacion) {
		super((nombrePersona.concat(" ").concat(apellidoPaterno).concat(" ").concat(!StringUtil.isEmpty(apellidoMaterno)?apellidoMaterno:"")).trim(), 
				codigoRFC, fechaRFC, homoclaveRFC, TIPO_PERSONA.PF.toString(), codigoUsuarioCreacion);			
		this.apellidoPaterno = apellidoPaterno;
		this.apellidoMaterno = apellidoMaterno;
		this.nombrePersona = nombrePersona;
		this.curp = curp;
		this.fechaNacimiento = fechaNacimiento;		
	}

	
	public String getNombreCompleto(){
        if(!StringUtil.isEmpty(nombrePersona) && !StringUtil.isEmpty(apellidoPaterno)){
              return (nombrePersona.concat(" ").concat(apellidoPaterno).concat(" ").concat(
                          !StringUtil.isEmpty(apellidoMaterno)?apellidoMaterno:"")).trim();
        }
        return null;
	}


	/**
	 * @return the apellidoPaterno
	 */
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	/**
	 * @param apellidoPaterno the apellidoPaterno to set
	 */
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	/**
	 * @return the apellidoMaterno
	 */
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	/**
	 * @param apellidoMaterno the apellidoMaterno to set
	 */
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	/**
	 * @return the curp
	 */
	public String getCurp() {
		return curp;
	}
	/**
	 * @param curp the curp to set
	 */
	public void setCurp(String curp) {
		this.curp = curp;
	}
	/**
	 * @return the cveActividad
	 */
	public String getCveActividad() {
		return cveActividad;
	}
	/**
	 * @param cveActividad the cveActividad to set
	 */
	public void setCveActividad(String cveActividad) {
		this.cveActividad = cveActividad;
	}
	/**
	 * @return the cveEstadoCivil
	 */
	public String getCveEstadoCivil() {
		return cveEstadoCivil;
	}
	/**
	 * @param cveEstadoCivil the cveEstadoCivil to set
	 */
	public void setCveEstadoCivil(String cveEstadoCivil) {
		this.cveEstadoCivil = cveEstadoCivil;
	}
	/**
	 * @return the cveEstadoNacimiento
	 */
	public String getCveEstadoNacimiento() {
		return cveEstadoNacimiento;
	}
	/**
	 * @param cveEstadoNacimiento the cveEstadoNacimiento to set
	 */
	public void setCveEstadoNacimiento(String cveEstadoNacimiento) {
		this.cveEstadoNacimiento = cveEstadoNacimiento;
	}
	/**
	 * @return the cveMunicipioNacimiento
	 */
	public String getCveMunicipioNacimiento() {
		return cveMunicipioNacimiento;
	}
	/**
	 * @param cveMunicipioNacimiento the cveMunicipioNacimiento to set
	 */
	public void setCveMunicipioNacimiento(String cveMunicipioNacimiento) {
		this.cveMunicipioNacimiento = cveMunicipioNacimiento;
	}
	/**
	 * @return the cveRamaLaboral
	 */
	public String getCveRamaLaboral() {
		return cveRamaLaboral;
	}
	/**
	 * @param cveRamaLaboral the cveRamaLaboral to set
	 */
	public void setCveRamaLaboral(String cveRamaLaboral) {
		this.cveRamaLaboral = cveRamaLaboral;
	}
	/**
	 * @return the cveSexo
	 */
	public String getCveSexo() {
		return cveSexo;
	}
	/**
	 * @param cveSexo the cveSexo to set
	 */
	public void setCveSexo(String cveSexo) {
		this.cveSexo = cveSexo;
	}
	/**
	 * @return the empresaLaboral
	 */
	public String getEmpresaLaboral() {
		return empresaLaboral;
	}
	/**
	 * @param empresaLaboral the empresaLaboral to set
	 */
	public void setEmpresaLaboral(String empresaLaboral) {
		this.empresaLaboral = empresaLaboral;
	}
	/**
	 * @return the fechaNacimiento
	 */
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	/**
	 * @param fechaNacimiento the fechaNacimiento to set
	 */
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	/**
	 * @return the lugarNacimiento
	 */
	public String getLugarNacimiento() {
		return lugarNacimiento;
	}
	/**
	 * @param lugarNacimiento the lugarNacimiento to set
	 */
	public void setLugarNacimiento(String lugarNacimiento) {
		this.lugarNacimiento = lugarNacimiento;
	}
	/**
	 * @return the nombrePersona
	 */
	public String getNombrePersona() {
		return nombrePersona;
	}
	/**
	 * @param nombrePersona the nombrePersona to set
	 */
	public void setNombrePersona(String nombrePersona) {
		this.nombrePersona = nombrePersona;
	}
	/**
	 * @return the puesto
	 */
	public String getPuesto() {
		return puesto;
	}
	/**
	 * @param puesto the puesto to set
	 */
	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}
	/**
	 * @return the tituloProfesional
	 */
	public String getTituloProfesional() {
		return tituloProfesional;
	}
	/**
	 * @param tituloProfesional the tituloProfesional to set
	 */
	public void setTituloProfesional(String tituloProfesional) {
		this.tituloProfesional = tituloProfesional;
	}
		
	
}
