/**
 * 
 */
package mx.com.afirme.midas2.domain.personadireccion;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.EnumBase;
import mx.com.afirme.midas2.domain.MidasAbstracto;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

/**
 * @author smvr
 *
 */
@Entity
@Table(name = "PERSONA_DIRECCION_MIDAS", schema = "MIDAS")
public class PersonaDireccionMidas extends MidasAbstracto{

	private static final long serialVersionUID = 3130280860329956629L;
	
	public enum TIPO_DOMICILIO implements EnumBase<String> {
		FISCAL("FIS", "FISCAL"), PERSONAL("PER", "PERSONAL"), OFICINA("OFI", "OFICINA");
		private String estatus;
		private String name;

		TIPO_DOMICILIO(String estatus, String name) {
			this.estatus = estatus;
			this.name = name;
		}

		@Override
		public String getValue() {
			return this.estatus;
		}

		@Override
		public String getLabel() {
			return this.name;
		} 

	}
	
	@EmbeddedId
	@AttributeOverrides( {
	        @AttributeOverride(name="personaId", column=@Column(name="PERSONA_ID", nullable=false, insertable=false, updatable=false) ), 
	        @AttributeOverride(name="direccionId", column=@Column(name="DIRECCION_ID", nullable=false, insertable=false, updatable=false) )} )
	private PersonaDireccionMidasId id =  new PersonaDireccionMidasId();
	
	@ManyToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name="DIRECCION_ID", referencedColumnName="ID")
	@JoinFetch(JoinFetchType.INNER)
	private DireccionMidas direccion;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PERSONA_ID", referencedColumnName="ID")
	@JoinFetch(JoinFetchType.INNER)
	private PersonaMidas persona;
	
	@Column(name="TIPO_DIRECCION", nullable=false)
	private String tipoDireccion;
	
	public PersonaDireccionMidas() {
		super();
	}

	public PersonaDireccionMidas(DireccionMidas direccion, String tipoDireccion, PersonaMidas persona, String codigoUsuarioCreacion) {
		super();
		this.direccion = direccion;
		this.persona = persona;
		this.tipoDireccion = tipoDireccion;
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
		this.fechaCreacion = new Date();
		this.codigoUsuarioModificacion = codigoUsuarioCreacion;
		this.fechaModificacion = new Date();
	}
	
	public String getTipoDireccion() {
		return tipoDireccion;
	}

	public void setTipoDireccion(String tipoDireccion) {
		this.tipoDireccion = tipoDireccion;
	}
	
	public void setTipoDireccion(TIPO_DOMICILIO tipoDireccion) {
		this.tipoDireccion = tipoDireccion.getValue();
	}	

	@SuppressWarnings("unchecked")
	@Override
	public PersonaDireccionMidasId getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public PersonaDireccionMidasId getBusinessKey() {
		return id;
	}

	public PersonaDireccionMidasId getId() {
		return id;
	}

	public void setId(PersonaDireccionMidasId id) {
		this.id = id;
	}

	public void setDireccion(DireccionMidas direccion) {
		this.direccion = direccion;
	}

	public void setPersona(PersonaMidas persona) {
		this.persona = persona;
	}

	public DireccionMidas getDireccion() {
		return direccion;
	}

	public PersonaMidas getPersona() {
		return persona;
	}

	
	
}
