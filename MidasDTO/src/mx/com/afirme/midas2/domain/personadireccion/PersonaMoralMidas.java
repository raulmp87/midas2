/**
 * 
 */
package mx.com.afirme.midas2.domain.personadireccion;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author smvr
 *
 */
@Entity(name = "PersonaMoralMidas")
@Table(name = "PERSONA_MORAL_MIDAS", schema = "MIDAS")
@DiscriminatorValue("PM")
@PrimaryKeyJoinColumn(name="ID", referencedColumnName="ID")
public class PersonaMoralMidas extends PersonaMidas{

	private static final long serialVersionUID = -3237931226547026073L;
	
	@Column(name = "CVE_RAMA")
	private String cveRama;
	
	@Column(name = "CVE_SECTOR")
	private String cveSector;
	
	@Column(name = "CVE_SUBRAMA")
	private String cveSubRama;
	
	@Column(name = "NOMBRE_RAZON_SOCIAL", nullable = false,  length = 200)
	private String nombreRazonSocial;
	
	@Column(name = "REPRESENTANTE_LEGAL", nullable = false,  length = 80)
	private String representanteLegal;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CONSTITUCION", nullable = false, length = 7)
	private Date fechaConstruccion;
	
	
	
	public PersonaMoralMidas() {
		super();
		super.setTipoPersona(TIPO_PERSONA.PM.toString());
	}
	
	
	
	public PersonaMoralMidas(String nombreRazonSocial, String representanteLegal, Date fechaConstruccion, 
			String codigoRFC, String fechaRFC, String homoclaveRFC, String codigoUsuarioCreacion) {
		super(nombreRazonSocial, codigoRFC, fechaRFC, homoclaveRFC, TIPO_PERSONA.PM.toString(), codigoUsuarioCreacion);
		this.nombreRazonSocial = nombreRazonSocial;
		this.representanteLegal = representanteLegal;
		this.fechaConstruccion = fechaConstruccion;
	}



	/**
	 * @return the cveRama
	 */
	public String getCveRama() {
		return cveRama;
	}
	/**
	 * @param cveRama the cveRama to set
	 */
	public void setCveRama(String cveRama) {
		this.cveRama = cveRama;
	}
	/**
	 * @return the cveSector
	 */
	public String getCveSector() {
		return cveSector;
	}
	/**
	 * @param cveSector the cveSector to set
	 */
	public void setCveSector(String cveSector) {
		this.cveSector = cveSector;
	}
	/**
	 * @return the cveSubRama
	 */
	public String getCveSubRama() {
		return cveSubRama;
	}
	/**
	 * @param cveSubRama the cveSubRama to set
	 */
	public void setCveSubRama(String cveSubRama) {
		this.cveSubRama = cveSubRama;
	}
	/**
	 * @return the nombreRazonSocial
	 */
	public String getNombreRazonSocial() {
		return nombreRazonSocial;
	}
	/**
	 * @param nombreRazonSocial the nombreRazonSocial to set
	 */
	public void setNombreRazonSocial(String nombreRazonSocial) {
		this.nombreRazonSocial = nombreRazonSocial;
	}
	/**
	 * @return the representanteLegal
	 */
	public String getRepresentanteLegal() {
		return representanteLegal;
	}
	/**
	 * @param representanteLegal the representanteLegal to set
	 */
	public void setRepresentanteLegal(String representanteLegal) {
		this.representanteLegal = representanteLegal;
	}
	/**
	 * @return the fechaConstruccion
	 */
	public Date getFechaConstruccion() {
		return fechaConstruccion;
	}
	/**
	 * @param fechaConstruccion the fechaConstruccion to set
	 */
	public void setFechaConstruccion(Date fechaConstruccion) {
		this.fechaConstruccion = fechaConstruccion;
	}
	
}
