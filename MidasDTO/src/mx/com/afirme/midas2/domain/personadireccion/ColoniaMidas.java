package mx.com.afirme.midas2.domain.personadireccion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.domain.MidasAbstracto;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

/**
 * @author smvr
 *
 */
@Entity(name = "ColoniaMidas")
@Table(name = "VW_COLONY_MIDAS", schema = "MIDAS")
public class ColoniaMidas extends MidasAbstracto{

	private static final long serialVersionUID = 356138787778420990L;
	
	@Id
	@Column(name = "COLONY_ID")
	private String id;
	
	@Column(name = "ZIP_CODE", unique = false, nullable = false, insertable = true, updatable = true, length = 6)
	private String  codigoPostal;
	
	@Column(name = "ZIP_CODE_USER_ID")
	private Integer idUsuarioCodigoPostal;
	
	@Column(name = "COLONY_NAME", unique = false, nullable = false, insertable = true, updatable = true, length = 100)
	private String  descripcion;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch(JoinFetchType.INNER)
	@JoinColumn(name="CITY_ID", referencedColumnName="CITY_ID", insertable=false, updatable=false)
	private CiudadMidas  ciudad;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the codigoPostal
	 */
	public String getCodigoPostal() {
		return codigoPostal;
	}

	/**
	 * @param codigoPostal the codigoPostal to set
	 */
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	/**
	 * @return the idUsuarioCodigoPostal
	 */
	public Integer getIdUsuarioCodigoPostal() {
		return idUsuarioCodigoPostal;
	}

	/**
	 * @param idUsuarioCodigoPostal the idUsuarioCodigoPostal to set
	 */
	public void setIdUsuarioCodigoPostal(Integer idUsuarioCodigoPostal) {
		this.idUsuarioCodigoPostal = idUsuarioCodigoPostal;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the ciudad
	 */
	public CiudadMidas getCiudad() {
		return ciudad;
	}

	/**
	 * @param ciudad the ciudad to set
	 */
	public void setCiudad(CiudadMidas ciudad) {
		this.ciudad = ciudad;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ColoniaMidas other = (ColoniaMidas) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getBusinessKey() {	
		return id;
	}

	
}
