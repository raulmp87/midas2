/**
 * 
 */
package mx.com.afirme.midas2.domain.personadireccion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.domain.MidasAbstracto;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

/**
 * @author smvr
 *
 */
@Entity(name = "DireccionMidas")
@Table(name = "DIRECCION_MIDAS", schema = "MIDAS")
public class DireccionMidas extends MidasAbstracto{

	private static final long serialVersionUID = -5380445931141615030L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DOMICILIOMIDAS_GENERATOR")	
	@SequenceGenerator(name="DOMICILIOMIDAS_GENERATOR", schema="MIDAS", sequenceName="DOMICILIOMIDAS_SEQ", allocationSize=1)	
	@Column(name="ID",nullable=false)
	private Long id;
	
	@Column(name = "CALLE", nullable = false, length = 100)
	private String calle;

	@Column(name = "NUM_EXTERIOR", nullable = false, length = 6)
	private String numExterior;
	
	@Column(name = "NUM_INTERIOR", length = 6)
	private String numInterior;
	
	@Column(name = "REFERENCIA")
	private String referencia;
	
	@Column(name = " CODIGO_POSTAL_OTRA_COLONIA", length = 5)
	private String codigoPostalColonia;
	
	@Column(name = "ID_CODIGO_POSTAL_OTRA_COLONIA")
	private Integer idCodigoPostalOtraColonia;
	
	@Column(name = "OTRA_COLONIA")
	private String otraColonia;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch(JoinFetchType.OUTER)
	@JoinColumn(name="COLONIA_ID", referencedColumnName="COLONY_ID")
	private ColoniaMidas colonia;
	
	@OneToMany(fetch=FetchType.LAZY,  mappedBy = "direccion", cascade = CascadeType.ALL)
	private List<PersonaDireccionMidas> personaDireccion = new ArrayList<PersonaDireccionMidas>();
	
	public DireccionMidas() {
		super();
	}
	
	public DireccionMidas(String calle, String numero, ColoniaMidas colonia, String otraColonia, 
			Integer idCodigoPostalOtraColonia, String codigoUsuarioCreacion) {
		super();
		this.calle = calle;
		this.numExterior = numero;
		this.colonia = colonia;
		this.otraColonia = otraColonia;
		this.idCodigoPostalOtraColonia = idCodigoPostalOtraColonia;	
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
		this.fechaCreacion = new Date();
		this.codigoUsuarioModificacion = codigoUsuarioCreacion;
		this.fechaModificacion = new Date();
	}	
		
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the calle
	 */
	public String getCalle() {
		return calle;
	}
	/**
	 * @param calle the calle to set
	 */
	public void setCalle(String calle) {
		this.calle = calle;
	}

	/**
	 * @return the codigoPostalColonia
	 */
	public String getCodigoPostalColonia() {
		return codigoPostalColonia;
	}
	/**
	 * @param codigoPostalColonia the codigoPostalColonia to set
	 */
	public void setCodigoPostalColonia(String codigoPostalColonia) {
		this.codigoPostalColonia = codigoPostalColonia;
	}
	/**
	 * @return the idCodigoPostalOtraColonia
	 */
	public Integer getIdCodigoPostalOtraColonia() {
		return idCodigoPostalOtraColonia;
	}
	/**
	 * @param idCodigoPostalOtraColonia the idCodigoPostalOtraColonia to set
	 */
	public void setIdCodigoPostalOtraColonia(Integer idCodigoPostalOtraColonia) {
		this.idCodigoPostalOtraColonia = idCodigoPostalOtraColonia;
	}
	/**
	 * @return the numExterior
	 */
	public String getNumExterior() {
		return numExterior;
	}
	/**
	 * @param numExterior the numExterior to set
	 */
	public void setNumExterior(String numExterior) {
		this.numExterior = numExterior;
	}
	/**
	 * @return the numInterior
	 */
	public String getNumInterior() {
		return numInterior;
	}
	/**
	 * @param numInterior the numInterior to set
	 */
	public void setNumInterior(String numInterior) {
		this.numInterior = numInterior;
	}
	/**
	 * @return the otraColonia
	 */
	public String getOtraColonia() {
		return otraColonia;
	}
	/**
	 * @param otraColonia the otraColonia to set
	 */
	public void setOtraColonia(String otraColonia) {
		this.otraColonia = otraColonia;
	}
	/**
	 * @return the referencia
	 */
	public String getReferencia() {
		return referencia;
	}
	/**
	 * @param referencia the referencia to set
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	/**
	 * @return the colonia
	 */
	public ColoniaMidas getColonia() {
		return colonia;
	}
	/**
	 * @param colonia the colonia to set
	 */
	public void setColonia(ColoniaMidas colonia) {
		this.colonia = colonia;		
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DireccionMidas other = (DireccionMidas) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}
	@Override
	public String getValue() {
		return calle.concat(" ").concat(numExterior).concat(" INT ").concat(numInterior);
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	public List<PersonaDireccionMidas> getPersonaDireccion() {
		return personaDireccion;
	}

	public void setPersonaDireccion(List<PersonaDireccionMidas> personaDireccion) {
		this.personaDireccion = personaDireccion;
	}
	
	
}
