
package mx.com.afirme.midas2.domain.personadireccion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas.base.MidasAbstractoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * @author smvr
 *
 */
/**
 * PaisDTO entity. @author MyEclipse Persistence Tools
 */
@Entity(name = "PaisMidas")
@Table(name = "VW_COUNTRY", schema="MIDAS")
public class PaisMidas extends MidasAbstractoDTO implements Entidad {

	private static final long serialVersionUID = 6640373499959711827L;
	
	
	@Id
    @Column(name="COUNTRY_ID", length=6)
	private String id;
	
	@Column(name="COUNTRY_NAME", unique=false, nullable=false, insertable=false, updatable=false, length=40)
	private String descripcion;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaisMidas other = (PaisMidas) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getKey() {
		return id;
	}

	@Override
	public String getValue() {	
		return descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getBusinessKey() {
		return id;
	}
	
    
}
