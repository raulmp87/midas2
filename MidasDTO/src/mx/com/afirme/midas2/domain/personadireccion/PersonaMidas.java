/**
 * 
 */
package mx.com.afirme.midas2.domain.personadireccion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.personadireccion.PersonaDireccionMidas.TIPO_DOMICILIO;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

/**
 * @author smvr
 *
 */
@Entity(name = "PersonaMidas")
@Table(name = "PERSONA_MIDAS", schema = "MIDAS")
@DiscriminatorColumn(name="TIPO_PERSONA")
@Inheritance(strategy=InheritanceType.JOINED)
public class PersonaMidas extends MidasAbstracto{

	private static final long serialVersionUID = -2906061742816857980L;
	
	public enum TIPO_PERSONA{PM, PF};
	
	@Id
	@Column(name="ID",nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PERSONAMIDAS_GENERATOR")	
	@SequenceGenerator(name="PERSONAMIDAS_GENERATOR", schema="MIDAS", sequenceName="PERSONAMIDAS_SEQ", allocationSize=1)	
	private Long id;
	
	@Column(name = "NOMBRE", nullable = false, length = 200)
	private String nombre;
	
	@Column(name = "ACTIVO", nullable = false)
	private Boolean activo = true;
	
	@Column(name = "CODIGO_RFC", nullable = false, length = 4)
	private String codigoRfc;
	
	@Column(name = "FECHA_RFC", nullable = false, length = 6)
	private String fechaRfc;
	
	@Column(name = "HOMOCLAVE_RFC", length = 3)
	private String homoclaveRfc;
	
	@Column(name = "RFC", nullable = false, length = 13)
	private String rfc;
	
	@Column(name = "TIPO_PERSONA", nullable = false, length = 2)
	private String tipoPersona;
	
	@Column(name = "CVE_NACIONALIDAD", nullable = false, length = 6)
	private String cveNacionalidad;
	
	@OneToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name="CONTACTO_ID", referencedColumnName="ID")
	@JoinFetch(JoinFetchType.OUTER)
	private DatoContactoMidas contacto = new DatoContactoMidas();
	
	@OneToMany(fetch=FetchType.LAZY,  mappedBy = "persona", cascade = CascadeType.ALL)
	private List<PersonaDireccionMidas> personaDireccion = new ArrayList<PersonaDireccionMidas>();
	
	
	public PersonaMidas() {
		super();
	}
	
	
	public PersonaMidas(String nombre, String codigoRFC, String fechaRFC, String homoclaveRFC, String tipoPersona, String codigoUsuarioCreacion) {
		super();
		this.nombre = nombre;
		this.codigoRfc = codigoRFC;
		this.fechaRfc = fechaRFC;
		this.homoclaveRfc = homoclaveRFC;
		this.rfc = codigoRFC.concat(fechaRFC).concat(!StringUtil.isEmpty(homoclaveRFC)?homoclaveRFC:"");
		this.tipoPersona = tipoPersona;
		this.activo = Boolean.TRUE;		
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
		this.codigoUsuarioModificacion = codigoUsuarioCreacion;
		this.fechaCreacion = new Date();
		this.fechaModificacion = new Date();
		this.cveNacionalidad = "PAMEXI";		
	}


	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the activo
	 */
	public Boolean getActivo() {
		return activo;
	}
	/**
	 * @param activo the activo to set
	 */
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
	/**
	 * @return the codigoRfc
	 */
	public String getCodigoRfc() {
		return codigoRfc;
	}
	/**
	 * @param codigoRfc the codigoRfc to set
	 */
	public void setCodigoRfc(String codigoRfc) {
		this.codigoRfc = codigoRfc;
	}
	/**
	 * @return the cveNacionalidad
	 */
	public String getCveNacionalidad() {
		return cveNacionalidad;
	}
	/**
	 * @param cveNacionalidad the cveNacionalidad to set
	 */
	public void setCveNacionalidad(String cveNacionalidad) {
		this.cveNacionalidad = cveNacionalidad;
	}
	/**
	 * @return the fechaRfc
	 */
	public String getFechaRfc() {
		return fechaRfc;
	}
	/**
	 * @param fechaRfc the fechaRfc to set
	 */
	public void setFechaRfc(String fechaRfc) {
		this.fechaRfc = fechaRfc;
	}
	/**
	 * @return the homoclaveRfc
	 */
	public String getHomoclaveRfc() {
		return homoclaveRfc;
	}
	/**
	 * @param homoclaveRfc the homoclaveRfc to set
	 */
	public void setHomoclaveRfc(String homoclaveRfc) {
		this.homoclaveRfc = homoclaveRfc;
	}
	/**
	 * @return the rfc
	 */
	public String getRfc() {
		return rfc;
	}
	/**
	 * @param rfc the rfc to set
	 */
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	/**
	 * @return the tipoPersona
	 */
	public String getTipoPersona() {
		return tipoPersona;
	}
	/**
	 * @param tipoPersona the tipoPersona to set
	 */
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	/**
	 * @return the contacto
	 */
	public DatoContactoMidas getContacto() {
		return contacto;
	}
	/**
	 * @param contacto the contacto to set
	 */
	public void setContacto(DatoContactoMidas contacto) {
		this.contacto = contacto;		
	}
	/**
	 * @return the personaDireccion
	 */
	public List<PersonaDireccionMidas> getPersonaDireccion() {
		return personaDireccion;
	}
	/**
	 * @param personaDireccion the personaDireccion to set
	 */
	public void setPersonaDireccion(List<PersonaDireccionMidas> personaDireccion) {
		this.personaDireccion = personaDireccion;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonaMidas other = (PersonaMidas) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}
	@Override
	public String getValue() {
		return nombre;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
		
	public void agregarDireccion(DireccionMidas direccion, TIPO_DOMICILIO tipo, String codigoUsuarioCreacion){
		PersonaDireccionMidas obj = new PersonaDireccionMidas(direccion, tipo.getValue(), this, codigoUsuarioCreacion);		
		this.personaDireccion.add(obj);		
	}
	
}
