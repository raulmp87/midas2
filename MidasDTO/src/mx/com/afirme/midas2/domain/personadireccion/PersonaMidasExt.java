package mx.com.afirme.midas2.domain.personadireccion;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.domain.MidasAbstracto;

@Entity(name = "PersonaMidasExt")
@Table(name = "PERSONA_MIDAS_EXT", schema = "MIDAS")
public class PersonaMidasExt extends MidasAbstracto {
	
	/** serialVersionUID **/
	private static final long serialVersionUID = 5828011618320071820L;
	
	@Id
	@Column(name = "ID", nullable = false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PERSONAMIDASEXT_GENERATOR")	
	@SequenceGenerator(name="PERSONAMIDASEXT_GENERATOR", schema="MIDAS", sequenceName="PERSONA_MIDAS_EXT_SEQ", allocationSize=1)	
	private Long id;
	
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "FOTO", columnDefinition = "BLOB NULL")
	private byte[] foto;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PERSONA_MIDAS_ID")
	private PersonaMidas personaMidas;

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	
	public PersonaMidas getPersonaMidas() {
		return personaMidas;
	}

	public void setPersonaMidas(PersonaMidas personaMidas) {
		this.personaMidas = personaMidas;
	}

	/* ******** Getters & setters ******** */
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public byte[] getFoto() {
		return foto;
	}
	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

}
