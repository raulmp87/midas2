package mx.com.afirme.midas2.domain.personadireccion;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PersonaDireccionMidasId implements Serializable{

	private static final long serialVersionUID = 3177765585190036249L;

	@Column(name="PERSONA_ID", nullable=false, insertable=false, updatable=false)
	private Long personaId;
	
	@Column(name="DIRECCION_ID", nullable=false, insertable=false, updatable=false)
	private Long direccionId;

	public Long getPersonaId() {
		return personaId;
	}

	public void setPersonaId(Long personaId) {
		this.personaId = personaId;
	}

	public Long getDireccionId() {
		return direccionId;
	}

	public void setDireccionId(Long direccionId) {
		this.direccionId = direccionId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((direccionId == null) ? 0 : direccionId.hashCode());
		result = prime * result
				+ ((personaId == null) ? 0 : personaId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof PersonaDireccionMidasId)) {
			return false;
		}
		PersonaDireccionMidasId other = (PersonaDireccionMidasId) obj;
		if (direccionId == null) {
			if (other.direccionId != null) {
				return false;
			}
		} else if (!direccionId.equals(other.direccionId)) {
			return false;
		}
		if (personaId == null) {
			if (other.personaId != null) {
				return false;
			}
		} else if (!personaId.equals(other.personaId)) {
			return false;
		}
		return true;
	}

}
