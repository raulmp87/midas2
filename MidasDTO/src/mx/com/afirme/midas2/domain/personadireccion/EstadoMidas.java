
package mx.com.afirme.midas2.domain.personadireccion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.base.MidasAbstractoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

/**
 * @author smvr
 *
 */
@Entity(name = "EstadoMidas")
@Table(name = "VW_STATE", schema = "MIDAS")
public class EstadoMidas extends MidasAbstractoDTO implements Entidad{


	private static final long serialVersionUID = -9078378160204443316L;
	
	@Id
	@Column(name = "STATE_ID", length = 6)
	private String id;
	
	@Column(name = "STATE_NAME", unique = false, nullable = false, insertable = false, updatable = false, length = 40)
	private String descripcion;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch(JoinFetchType.INNER)
	@JoinColumn(name="COUNTRY_ID", referencedColumnName="COUNTRY_ID", insertable=false, updatable=false)
	private PaisMidas pais;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @param pais the pais to set
	 */
	public void setPais(PaisMidas pais) {
		this.pais = pais;
	}

	/**
	 * @return the pais
	 */
	public PaisMidas getPais() {
		return pais;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getBusinessKey() {
		return id;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EstadoMidas other = (EstadoMidas) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	

}
