/**
 * 
 */
package mx.com.afirme.midas2.domain.personadireccion;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.domain.MidasAbstracto;

/**
 * @author smvr
 *
 */
@Entity(name = "DatoContactoMidas")
@Table(name = "DATO_CONTACTO_MIDAS", schema = "MIDAS")
public class DatoContactoMidas extends MidasAbstracto{

	private static final long serialVersionUID = -5173776537474307132L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DATOCONTACTO_GENERATOR")	
	@SequenceGenerator(name="DATOCONTACTO_GENERATOR", schema="MIDAS", sequenceName="DATOCONTACTOMIDAS_SEQ", allocationSize=1)	
	@Column(name="ID", nullable=false)
	private	Long id;
	
	@Column(name = "CORREO_PRINCIPAL", nullable = false, length = 60)
	private String correoPrincipal = "";
	
	@Column(name = "CORREOS_ADICIONALES", length = 300)
	private String correosAdicionales;
	
	@Column(name = "FACEBOOK", length = 300)
	private String facebook;
	
	@Column(name = "TWITTER", length = 300)
	private String twitter;
	
	@Column(name = "PAGINA_WEB", length = 300)
	private String paginaWeb;
	
	@Column(name = "TEL_CELULAR", length = 8)
	private String telCelular;
	
	@Column(name = "TEL_LADA_CELULAR", length = 3)
	private String telCelularLada;
	
	@Column(name = "TEL_CASA", length = 8)
	private String telCasa;
	
	@Column(name = "TEL_LADA_CASA", length = 3)
	private String telCasaLada;
	
	@Column(name = "TEL_CASA_EXT", length = 5)
	private String telCasaExt;
	
	@Column(name = "TEL_OFICINA", length = 8)
	private String telOficina;
	
	@Column(name = "TEL_LADA_OFICINA", length = 3)
	private String telOficinaLada;
	
	@Column(name = "TEL_OFICINA_EXT", length = 5)
	private String telOficinaExt;
	
	@Column(name = "TEL_OTRO1", length = 8)
	private String telOtro1;
	
	@Column(name = "TEL_OTRO1_EXT", length = 5)
	private String telOtro1Ext;
	
	@Column(name = "TEL_LADA_OTRO1", length = 3)
	private String telOtro1Lada;
	
	@Column(name = "TEL_OTRO2", length = 8)
	private String telOtro2;
	
	@Column(name = "TEL_LADA_OTRO2", length = 3)
	private String telOtro2Lada;
	
	@Column(name = "TEL_OTRO2_EXT", length = 5)
	private String telOtro2Ext;
	
	
	public DatoContactoMidas() {
		super();
	}
	
	public DatoContactoMidas(String codigoUsuarioCreacion) {
		super();
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
		this.fechaCreacion = new Date();
		this.codigoUsuarioModificacion = codigoUsuarioCreacion;
		this.fechaModificacion = new Date();
	}
	

	public DatoContactoMidas(String correoPrincipal, String telCasa,
			String telCasaLada, String telCasaExt, String codigoUsuarioCreacion) {
		super();
		this.correoPrincipal = correoPrincipal;
		this.telCasa = telCasa;
		this.telCasaLada = telCasaLada;
		this.telCasaExt = telCasaExt;
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
		this.fechaCreacion = new Date();
		this.codigoUsuarioModificacion = codigoUsuarioCreacion;
		this.fechaModificacion = new Date();
	}



	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the correoPrincipal
	 */
	public String getCorreoPrincipal() {
		return correoPrincipal;
	}

	/**
	 * @param correoPrincipal the correoPrincipal to set
	 */
	public void setCorreoPrincipal(String correoPrincipal) {
		this.correoPrincipal = correoPrincipal;
	}

	/**
	 * @return the correosAdicionales
	 */
	public String getCorreosAdicionales() {
		return correosAdicionales;
	}

	/**
	 * @param correosAdicionales the correosAdicionales to set
	 */
	public void setCorreosAdicionales(String correosAdicionales) {
		this.correosAdicionales = correosAdicionales;
	}

	/**
	 * @return the facebook
	 */
	public String getFacebook() {
		return facebook;
	}

	/**
	 * @param facebook the facebook to set
	 */
	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	/**
	 * @return the twitter
	 */
	public String getTwitter() {
		return twitter;
	}

	/**
	 * @param twitter the twitter to set
	 */
	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	/**
	 * @return the paginaWeb
	 */
	public String getPaginaWeb() {
		return paginaWeb;
	}

	/**
	 * @param paginaWeb the paginaWeb to set
	 */
	public void setPaginaWeb(String paginaWeb) {
		this.paginaWeb = paginaWeb;
	}

	/**
	 * @return the telCelular
	 */
	public String getTelCelular() {
		return telCelular;
	}

	/**
	 * @param telCelular the telCelular to set
	 */
	public void setTelCelular(String telCelular) {
		this.telCelular = telCelular;
	}

	/**
	 * @return the telLadaCelular
	 */
	public String getTelCelularLada() {
		return telCelularLada;
	}

	/**
	 * @param telCelularLada the telCelularLada to set
	 */
	public void setTelCelularLada(String telCelularLada) {
		this.telCelularLada = telCelularLada;
	}

	/**
	 * @return the telCasa
	 */
	public String getTelCasa() {
		return telCasa;
	}

	/**
	 * @param telCasa the telCasa to set
	 */
	public void setTelCasa(String telCasa) {
		this.telCasa = telCasa;
	}

	/**
	 * @return the telCasaLada
	 */
	public String getTelCasaLada() {
		return telCasaLada;
	}

	/**
	 * @param telCasaLada the telCasaLada to set
	 */
	public void setTelCasaLada(String telCasaLada) {
		this.telCasaLada = telCasaLada;
	}

	/**
	 * @return the telCasaExt
	 */
	public String getTelCasaExt() {
		return telCasaExt;
	}

	/**
	 * @param telCasaExt the telCasaExt to set
	 */
	public void setTelCasaExt(String telCasaExt) {
		this.telCasaExt = telCasaExt;
	}

	/**
	 * @return the telOficina
	 */
	public String getTelOficina() {
		return telOficina;
	}

	/**
	 * @param telOficina the telOficina to set
	 */
	public void setTelOficina(String telOficina) {
		this.telOficina = telOficina;
	}

	/**
	 * @return the telOficinaLada
	 */
	public String getTelOficinaLada() {
		return telOficinaLada;
	}

	/**
	 * @param telOficinaLada the telOficinaLada to set
	 */
	public void setTelOficinaLada(String telOficinaLada) {
		this.telOficinaLada = telOficinaLada;
	}

	/**
	 * @return the telOficinaExt
	 */
	public String getTelOficinaExt() {
		return telOficinaExt;
	}

	/**
	 * @param telOficinaExt the telOficinaExt to set
	 */
	public void setTelOficinaExt(String telOficinaExt) {
		this.telOficinaExt = telOficinaExt;
	}

	

	/**
	 * @return the telOtro1
	 */
	public String getTelOtro1() {
		return telOtro1;
	}

	/**
	 * @param telOtro1 the telOtro1 to set
	 */
	public void setTelOtro1(String telOtro1) {
		this.telOtro1 = telOtro1;
	}

	/**
	 * @return the telOtro1Ext
	 */
	public String getTelOtro1Ext() {
		return telOtro1Ext;
	}

	/**
	 * @param telOtro1Ext the telOtro1Ext to set
	 */
	public void setTelOtro1Ext(String telOtro1Ext) {
		this.telOtro1Ext = telOtro1Ext;
	}

	/**
	 * @return the telOtro1Lada
	 */
	public String getTelOtro1Lada() {
		return telOtro1Lada;
	}

	/**
	 * @param telOtro1Lada the telOtro1Lada to set
	 */
	public void setTelOtro1Lada(String telOtro1Lada) {
		this.telOtro1Lada = telOtro1Lada;
	}

	/**
	 * @return the telOtro2
	 */
	public String getTelOtro2() {
		return telOtro2;
	}

	/**
	 * @param telOtro2 the telOtro2 to set
	 */
	public void setTelOtro2(String telOtro2) {
		this.telOtro2 = telOtro2;
	}

	/**
	 * @return the telOtro2Lada
	 */
	public String getTelOtro2Lada() {
		return telOtro2Lada;
	}

	/**
	 * @param telOtro2Lada the telOtro2Lada to set
	 */
	public void setTelOtro2Lada(String telOtro2Lada) {
		this.telOtro2Lada = telOtro2Lada;
	}

	/**
	 * @return the telOtro2Ext
	 */
	public String getTelOtro2Ext() {
		return telOtro2Ext;
	}

	/**
	 * @param telOtro2Ext the telOtro2Ext to set
	 */
	public void setTelOtro2Ext(String telOtro2Ext) {
		this.telOtro2Ext = telOtro2Ext;
	}

	public boolean equals(Object object) {
		boolean equal = (object == this);
		if (!equal && object instanceof DatoContactoMidas) {
			DatoContactoMidas datoContacto = (DatoContactoMidas) object;
			equal = datoContacto.getId().equals(this.id);
		}
		return equal;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	
	public String getTelCasaCompleto(){
		String lada = ((telCasaLada==null)?"":this.telCasaLada);
		String telefono =(telCasa==null)?"":telCasa; 
		return lada+telefono;
	}
	
	public String getTelOficinaCompleto(){
		String lada = ((telOficinaLada==null)?"":this.telOficinaLada);
		String telefono =(telOficina==null)?"":telOficina; 
		return lada+telefono;
	}
	
	public String getTelCelularCompleto(){
		String lada = ((telCelularLada==null)?"":this.telCelularLada);
		String telefono =(telCelular==null)?"":telCelular;
		return lada+telefono;
	}
	
	public String getTelOtroCompleto(){
		String lada = ((telOtro1Lada==null)?"":this.telOtro1Lada);
		String telefono =(telOtro1==null)?"":telOtro1;
		return lada+telefono;
		
		
	}
	
	public String getTelOtro2Completo(){
		String lada = ((telOtro2Lada==null)?"":this.telOtro2Lada);
		String telefono =(telOtro2==null)?"":telOtro2;
		return lada+telefono;
	}
	
	
	
}
