package mx.com.afirme.midas2.domain.ws.autoplazo;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class Cliente implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@XmlElement(name="nombres",required=false)
	public String nombres;
	@XmlElement(name="apellido_paterno",required=false)
	public String apellidoPaterno;
	@XmlElement(name="apellido_materno",required=false)
	public String apellidoMaterno;
	@XmlElement(name="rfc",required=false)
	public String rfc;
	@XmlElement(name="clave_sexo",required=false)
	public String claveSexo;
	@XmlElement(name="clave_estado_civil",required=false)
	public String claveEstadoCivil;
	@XmlElement(name="id_estado_nacimiento",required=false)
	public String IdEstadoNacimiento;
	@XmlElement(name="fecha_nacimiento",required=false)
	public Date fechaNacimiento;
	@XmlElement(name="codigo_postal",required=false)
	public String codigoPostal;
	@XmlElement(name="id_estado",required=false)
	public String idEstado;
	@XmlElement(name="id_ciudad",required=false)
	public String idCiudad;
	@XmlElement(name="colonia",required=false)
	public String colonia;
	@XmlElement(name="calle_y_numero",required=false)
	public String calleYNumero;
	@XmlElement(name="telefono_casa",required=false)
	public String telefonoCasa;
	@XmlElement(name="telefono_oficina",required=false)
	public String telefonoOficina;
	@XmlElement(name="email",required=false)
	public String email;
	
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getClaveSexo() {
		return claveSexo;
	}
	public void setClaveSexo(String claveSexo) {
		this.claveSexo = claveSexo;
	}
	public String getClaveEstadoCivil() {
		return claveEstadoCivil;
	}
	public void setClaveEstadoCivil(String claveEstadoCivil) {
		this.claveEstadoCivil = claveEstadoCivil;
	}
	public String getIdEstadoNacimiento() {
		return IdEstadoNacimiento;
	}
	public void setIdEstadoNacimiento(String idEstadoNacimiento) {
		IdEstadoNacimiento = idEstadoNacimiento;
	}
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public String getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}
	public String getIdCiudad() {
		return idCiudad;
	}
	public void setIdCiudad(String idCiudad) {
		this.idCiudad = idCiudad;
	}
	public String getColonia() {
		return colonia;
	}
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	public String getCalleYNumero() {
		return calleYNumero;
	}
	public void setCalleYNumero(String calleYNumero) {
		this.calleYNumero = calleYNumero;
	}
	public String getTelefonoCasa() {
		return telefonoCasa;
	}
	public void setTelefonoCasa(String telefonoCasa) {
		this.telefonoCasa = telefonoCasa;
	}
	public String getTelefonoOficina() {
		return telefonoOficina;
	}
	public void setTelefonoOficina(String telefonoOficina) {
		this.telefonoOficina = telefonoOficina;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
}
