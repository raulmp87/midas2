package mx.com.afirme.midas2.domain.portal.seguros;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name="OficinaPortal")
@Table(name = "TOOFICINAPORTAL", schema = "MIDAS")
public class Oficina implements Serializable , Entidad{

	private static final long serialVersionUID = 2303294100199776870L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TOOFICINAPORTAL_SEQ")
	@SequenceGenerator(name = "TOOFICINAPORTAL_SEQ", schema="MIDAS", sequenceName = "TOOFICINAPORTAL_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name="DESCRIPCION")
	private String descripcion;
	
	@Column(name="DIRECCION")
	private String direccion;
	
	@Column(name="COORDENADAS")
	private String coordenadas;
	
	@Column(name="ESPROPIA")
	private boolean esPropia;
	
	public Oficina() {
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	public String getCoordenadas() {
		return coordenadas;
	}
	public void setCoordenadas(String coordenadas) {
		this.coordenadas = coordenadas;
	}
	
	public boolean getEsPropia() {
		return esPropia;
	}
	public void setEsPropia(boolean esPropia) {
		this.esPropia = esPropia;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Integer getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}