package mx.com.afirme.midas2.domain.portal.cotizador;

import java.io.Serializable;

public class Vehiculo implements Serializable{

	private static final long serialVersionUID = 3945395512832097228L;

	private String descripcionVehiculo;

	public String getDescripcionVehiculo() {
		return descripcionVehiculo;
	}
	public void setDescripcionVehiculo(String descripcionVehiculo) {
		this.descripcionVehiculo = descripcionVehiculo;
	}
}
