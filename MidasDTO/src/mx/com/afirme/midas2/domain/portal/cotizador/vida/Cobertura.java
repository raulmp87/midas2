package mx.com.afirme.midas2.domain.portal.cotizador.vida;

import java.io.Serializable;

public class Cobertura implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3957102415606587885L;
	private String nombreComercial;
	private String valorSumaAseguradaStr;

	public String getNombreComercial() {
		return nombreComercial;
	}
	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}
	public String getValorSumaAseguradaStr() {
		return valorSumaAseguradaStr;
	}
	public void setValorSumaAseguradaStr(String valorSumaAseguradaStr) {
		this.valorSumaAseguradaStr = valorSumaAseguradaStr;
	}
}
