package mx.com.afirme.midas2.domain.portal.cotizador;

import java.io.Serializable;
import java.math.BigDecimal;

public class Cobertura implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2002466264102954704L;
	
	private Short claveContrato;
	private String nombreComercial;
	private String descripcionSumaAsegurada;
	private String valorSumaAseguradaStr;
	private String valorDeducibleStr;
	
	public static final BigDecimal RC_VIAJERO_AUT = new BigDecimal(2650);
	public static final BigDecimal RC_VIAJERO_CAM = new BigDecimal(2860);
	public static final BigDecimal RC_VIAJERO_BUS = new BigDecimal(3020);
	public static final BigDecimal RC_VIAJERO_MOT = new BigDecimal(4821);
	
	public static final String CLAVE_FUENTE_SA_VALOR_PROPORCIONADO = "0";
	public static final String CLAVE_FUENTE_SA_VALOR_COMERCIAL = "1";
	public static final String CLAVE_FUENTE_SA_VALOR_FACTURA = "2";
	public static final String CLAVE_FUENTE_SA_VALOR_CONVENIDO = "9";
	public static final String CLAVE_FUENTE_SA_VALOR_AMPARADA = "3";
	
	public static final String CLAVE_TIPO_DEDUCIBLE_PORCENTAJE  = "1";

	public Short getClaveContrato() {
		return claveContrato;
	}
	public void setClaveContrato(Short claveContrato) {
		this.claveContrato = claveContrato;
	}
	public String getNombreComercial() {
		return nombreComercial;
	}
	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}
	public String getDescripcionSumaAsegurada() {
		return descripcionSumaAsegurada;
	}
	public void setDescripcionSumaAsegurada(String descripcionSumaAsegurada) {
		this.descripcionSumaAsegurada = descripcionSumaAsegurada;
	}
	public String getValorSumaAseguradaStr() {
		return valorSumaAseguradaStr;
	}
	public void setValorSumaAseguradaStr(String valorSumaAseguradaStr) {
		this.valorSumaAseguradaStr = valorSumaAseguradaStr;
	}
	public String getValorDeducibleStr() {
		return valorDeducibleStr;
	}
	public void setValorDeducibleStr(String valorDeducibleStr) {
		this.valorDeducibleStr = valorDeducibleStr;
	}
}
