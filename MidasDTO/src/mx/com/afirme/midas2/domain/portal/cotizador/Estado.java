package mx.com.afirme.midas2.domain.portal.cotizador;

import java.io.Serializable;

public class Estado implements Serializable{

	private static final long serialVersionUID = -6385778313335989567L;

	private String nombreEstado;

	public String getNombreEstado() {
		return nombreEstado;
	}
	public void setNombreEstado(String nombreEstado) {
		this.nombreEstado = nombreEstado;
	}
}