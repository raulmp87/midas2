package mx.com.afirme.midas2.domain.portal.seguros;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name = "TODEPARTAMENTOPORTAL", schema = "MIDAS")
public class Departamento implements Serializable, Entidad {
	
	private static final long serialVersionUID = -6154158454742029964L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TODEPARTAMENTOPORTAL_SEQ")
	@SequenceGenerator(name = "TODEPARTAMENTOPORTAL_SEQ", schema="MIDAS", sequenceName = "TODEPARTAMENTOPORTAL_SEQ", allocationSize = 1)
	private Integer id;
	
	@Column(name="DESCRIPCION")
	private String descripcion;
	
	@Column(name="RECIPIENTES")
	private String recipientes;
	
	@Column(name="COPIAS")
	private String copias;
	
	public Departamento() {
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getRecipientes() {
		return recipientes;
	}
	public void setRecipientes(String recipientes) {
		this.recipientes = recipientes;
	}
	public String getCopias() {
		return copias;
	}
	public void setCopias(String copias) {
		this.copias = copias;
	}

	@Override
	public <K> K getKey() {
		return null;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}