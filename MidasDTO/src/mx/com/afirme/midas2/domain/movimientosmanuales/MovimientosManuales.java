package mx.com.afirme.midas2.domain.movimientosmanuales;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.base.PaginadoDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;

@Entity(name="MovimientosManuales")
@Table(name="TOAGTMOVIMIENTOMAN",schema="MIDAS")
public class MovimientosManuales extends PaginadoDTO implements Entidad,Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 460531000484010L;
	private	Long	id = 0L;
	private	Long	idEmpresa = 0L;
	private	Agente	agente = null;
	//private	Date	fechaMovimiento;
	//private	Long	idConsecMovto = 0L;
	private	Long	idConcepto = 0L;
	private	ValorCatalogoAgentes moneda = null; 
	private	String	idRamoContable = null;
	private	Double	importeMovto = 0D;
	private	String	descripcionMovto = null;
	private	String	referencia = null;
	private	String	idUsuarioCreacion = null;
	private	Date	fechaCreacion = null;
	private	String	cvetCptoAco = null;
	private	String	naturalezaConcepto = null;
	private	String	idSubramoContable  = null;
	//private	Long	numeroMovtoManAgente  = 0L;
	private SubRamoDTO  subRamoDTO = null;
	private Integer estatusRegistro = null;
	
	private Date fechaInicioPeriodo;
	private Date fechaFinPeriodo;

	@Column(name="IMPORTEMOVTO")
	public Double getImporteMovto() {
		return importeMovto;
	}

	public void setImporteMovto(Double importeMovto) {
		this.importeMovto = importeMovto;
	}

	@Column(name="IDUSUARIOCREACION")
	public String getIdUsuarioCreacion() {
		return idUsuarioCreacion;
	}

	public void setIdUsuarioCreacion(String idUsuarioCreacion) {
		this.idUsuarioCreacion = idUsuarioCreacion;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FHCREACION")
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
/*
	@Column(name="IDTOAGENTEMOVIMIENTOS")
	public Long getNumeroMovtoManAgente() {
		return numeroMovtoManAgente;
	}

	public void setNumeroMovtoManAgente(Long numeroMovtoManAgente) {
		this.numeroMovtoManAgente = numeroMovtoManAgente;
	}	*/
	
	@Id
	@SequenceGenerator(name = "IDTOAGTMOVIMIENTOMAN_SEQ_GENERATOR",allocationSize = 1, sequenceName = "MIDAS.IDTOAGTMOVIMIENTOMAN_SEQ")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "IDTOAGTMOVIMIENTOMAN_SEQ_GENERATOR")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="IDEMPRESA")
	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=Agente.class)
	@JoinColumn(name="IDAGENTE",referencedColumnName="IDAGENTE")
	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}
	/*
	@Column(name="IDCONSECMOVTO")
	public Long getIdConsecMovto() {
		return idConsecMovto;
	}

	public void setIdConsecMovto(Long idConsecMovto) {
		this.idConsecMovto = idConsecMovto;
	}*/
	
	@Column(name="IDCONCEPTO")
	public Long getIdConcepto() {
		return idConcepto;
	}

	public void setIdConcepto(Long idConcepto) {
		this.idConcepto = idConcepto;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDMONEDA")
	public ValorCatalogoAgentes getMoneda() {
		return moneda;
	}

	public void setMoneda(ValorCatalogoAgentes moneda) {
		this.moneda = moneda;
	}
	
	@Column(name="DESCRIPCIONMOVTO")
	public String getDescripcionMovto() {
		return descripcionMovto;
	}

	public void setDescripcionMovto(String descripcionMovto) {
		this.descripcionMovto = descripcionMovto;
	}

	@Column(name="IDRAMOCONTABLE")
	public String getIdRamoContable() {
		return idRamoContable;
	}

	public void setIdRamoContable(String idRamoContable) {
		this.idRamoContable = idRamoContable;
	}

	@Column(name="IDSUBRAMOCONTABLE")
	public String getIdSubramoContable() {
		return idSubramoContable;
	}

	public void setIdSubramoContable(String idSubramoContable) {
		this.idSubramoContable = idSubramoContable;
	}
/*
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAMOVIMIENTO")
	public Date getFechaMovimiento() {
		return fechaMovimiento;
	}

	public void setFechaMovimiento(Date fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
	}*/

	@Column(name="REFERENCIA")
	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}	
	
	@Column(name="NATURALEZACONCEPTO")
	public String getNaturalezaConcepto() {
		return naturalezaConcepto;
	}

	public void setNaturalezaConcepto(String naturalezaConcepto) {
		this.naturalezaConcepto = naturalezaConcepto;
	}

	
	@Column(name="CVETCPTOACO")
	public String getCvetCptoAco() {
		return cvetCptoAco;
	}

	public void setCvetCptoAco(String cvetCptoAco) {
		this.cvetCptoAco = cvetCptoAco;
	}

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Transient
	public Date getFechaInicioPeriodo() {
		return fechaInicioPeriodo;
	}

	public void setFechaInicioPeriodo(Date fechaInicioPeriodo) {
		this.fechaInicioPeriodo = fechaInicioPeriodo;
	}

	@Transient
	public Date getFechaFinPeriodo() {
		return fechaFinPeriodo;
	}

	public void setFechaFinPeriodo(Date fechaFinPeriodo) {
		this.fechaFinPeriodo = fechaFinPeriodo;
	}

	@ManyToOne
	@JoinColumn(name="IDTCSUBRAMO", referencedColumnName="IDTCSUBRAMO")
	public SubRamoDTO getSubRamoDTO() {
		return subRamoDTO;
	}

	public void setSubRamoDTO(SubRamoDTO subRamoDTO) {
		this.subRamoDTO = subRamoDTO;
	}

	public Integer getEstatusRegistro() {
		return estatusRegistro;
	}

	public void setEstatusRegistro(Integer estatusRegistro) {
		this.estatusRegistro = estatusRegistro;
	}
}