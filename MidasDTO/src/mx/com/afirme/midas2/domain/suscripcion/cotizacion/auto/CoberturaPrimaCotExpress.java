package mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.domain.catalogos.Paquete;

@Entity
@Table(name = "TOCOTIZACIONEXPRESS", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = {
		"IDTOLINEANEGOCIO", "IDTOAGRUPADORTARIFA", "IDVERAGRUPADORTARIFA",
		"IDMONEDA", "IDVERSIONCARGA", "CLAVETIPOBIEN", "CLAVEESTILO",
		"IDTCTIPOVEHICULO", "IDTCMARCAVEHICULO", "MODELOVEHICULO",
		"CLAVEZONACIRCULACION", "CLAVEUSOVEHICULO", "CLAVESERVICIOVEHICULO",
		"IDPAQUETE", "IDTOCOBERTURA" }))
public class CoberturaPrimaCotExpress implements Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	
	//Valores de entrada para la combinacion
	private Long idLineaNegocio;
	private Long idAgrupadorTarifa; // AgrupadorTarifa
	private Long idVerAgrupadorTarifa; // AgrupadorTarifa
	private Long idMoneda; // modeloVehiculoDTO
	private Long idVersionCarga; // modeloVehiculoDTO
	private String claveTipoBien; // modeloVehiculoDTO
	private String claveEstilo; // modeloVehiculoDTO
	private Short modeloVehiculo; // modeloVehiculoDTO
	private Long idTipoVehiculo;
	private Long idMarcaVehiculo;
	private String claveZonaCirculacion;
	private String claveUsoVehiculo;
	private String claveServicioVehiculo;
	private Paquete paquete;
	
	//Valores de Salida
	private CoberturaDTO cobertura;
	private Short claveFuenteSumaAsegurada;
	private CoberturaDTO coberturaSumaAsegurada;
	private Short claveTipoLimiteSumaAsegurada;
	private BigDecimal valorSumaAseguradaMin;
	private BigDecimal valorSumaAseguradaMax;
	private BigDecimal sumaAsegurada;
	private Integer claveContratoDm;
	private Integer claveContratoRt;
	private Integer claveContratoRc;
	private Integer idTipoDeducible;
	private BigDecimal pctDeducible;
	private BigDecimal dsmgvdfDeducible;
	private BigDecimal primaNetaB;
	private BigDecimal primaNetaARdt;

	// Constructors

	/** default constructor */
	public CoberturaPrimaCotExpress() {
	}

	// Property accessors
	@Id
	@Column(name = "ID", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "IDTOLINEANEGOCIO", nullable = false, precision = 22, scale = 0)
	public Long getIdLineaNegocio() {
		return idLineaNegocio;
	}

	public void setIdLineaNegocio(Long idLineaNegocio) {
		this.idLineaNegocio = idLineaNegocio;
	}

	@Column(name = "IDTOAGRUPADORTARIFA", nullable = false, precision = 22, scale = 0)
	public Long getIdAgrupadorTarifa() {
		return idAgrupadorTarifa;
	}

	public void setIdAgrupadorTarifa(Long idAgrupadorTarifa) {
		this.idAgrupadorTarifa = idAgrupadorTarifa;
	}

	@Column(name = "IDVERAGRUPADORTARIFA", nullable = false, precision = 22, scale = 0)
	public Long getIdVerAgrupadorTarifa() {
		return idVerAgrupadorTarifa;
	}

	public void setIdVerAgrupadorTarifa(Long idVerAgrupadorTarifa) {
		this.idVerAgrupadorTarifa = idVerAgrupadorTarifa;
	}

	@Column(name = "IDMONEDA", nullable = false, precision = 22, scale = 0)
	public Long getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}

	@Column(name = "IDVERSIONCARGA", nullable = false, precision = 22, scale = 0)
	public Long getIdVersionCarga() {
		return idVersionCarga;
	}

	public void setIdVersionCarga(Long idVersionCarga) {
		this.idVersionCarga = idVersionCarga;
	}

	@Column(name = "CLAVETIPOBIEN", nullable = false, length = 5)
	public String getClaveTipoBien() {
		return claveTipoBien;
	}

	public void setClaveTipoBien(String claveTipoBien) {
		this.claveTipoBien = claveTipoBien;
	}

	@Column(name = "CLAVEESTILO", nullable = false, length = 8)
	public String getClaveEstilo() {
		return claveEstilo;
	}

	public void setClaveEstilo(String claveEstilo) {
		this.claveEstilo = claveEstilo;
	}

	@Column(name = "IDTCTIPOVEHICULO", nullable = false, precision = 22, scale = 0)
	public Long getIdTipoVehiculo() {
		return idTipoVehiculo;
	}

	public void setIdTipoVehiculo(Long idTipoVehiculo) {
		this.idTipoVehiculo = idTipoVehiculo;
	}

	@Column(name = "IDTCMARCAVEHICULO", nullable = false, precision = 22, scale = 0)
	public Long getIdMarcaVehiculo() {
		return idMarcaVehiculo;
	}

	public void setIdMarcaVehiculo(Long idMarcaVehiculo) {
		this.idMarcaVehiculo = idMarcaVehiculo;
	}

	@Column(name = "MODELOVEHICULO", nullable = false, precision = 4, scale = 0)
	public Short getModeloVehiculo() {
		return modeloVehiculo;
	}

	public void setModeloVehiculo(Short modeloVehiculo) {
		this.modeloVehiculo = modeloVehiculo;
	}

	@Column(name = "CLAVEZONACIRCULACION", nullable = false, length = 4)
	public String getClaveZonaCirculacion() {
		return claveZonaCirculacion;
	}

	public void setClaveZonaCirculacion(String claveZonaCirculacion) {
		this.claveZonaCirculacion = claveZonaCirculacion;
	}

	@Column(name = "CLAVEUSOVEHICULO", nullable = false, length = 4)
	public String getClaveUsoVehiculo() {
		return claveUsoVehiculo;
	}

	public void setClaveUsoVehiculo(String claveUsoVehiculo) {
		this.claveUsoVehiculo = claveUsoVehiculo;
	}

	@Column(name = "CLAVESERVICIOVEHICULO", nullable = false, length = 4)
	public String getClaveServicioVehiculo() {
		return claveServicioVehiculo;
	}

	public void setClaveServicioVehiculo(String claveServicioVehiculo) {
		this.claveServicioVehiculo = claveServicioVehiculo;
	}

	@ManyToOne
	@JoinColumn(name = "IDPAQUETE", nullable = false, referencedColumnName = "IDPAQUETE")
	public Paquete getPaquete() {
		return paquete;
	}

	public void setPaquete(Paquete paquete) {
		this.paquete = paquete;
	}

	@ManyToOne
	@JoinColumn(name = "IDTOCOBERTURA", nullable = false, referencedColumnName = "IDTOCOBERTURA")
	public CoberturaDTO getCobertura() {
		return cobertura;
	}

	public void setCobertura(CoberturaDTO cobertura) {
		this.cobertura = cobertura;
	}

	@Column(name = "CLAVEFUENTESUMAASEGURADA", nullable = false, precision = 4, scale = 0)
	public Short getClaveFuenteSumaAsegurada() {
		return claveFuenteSumaAsegurada;
	}

	public void setClaveFuenteSumaAsegurada(Short claveFuenteSumaAsegurada) {
		this.claveFuenteSumaAsegurada = claveFuenteSumaAsegurada;
	}

	@ManyToOne
	@JoinColumn(name = "IDCOBERTURASUMAASEGURADA", nullable = false, referencedColumnName = "IDTOCOBERTURA")
	public CoberturaDTO getCoberturaSumaAsegurada() {
		return coberturaSumaAsegurada;
	}

	public void setCoberturaSumaAsegurada(CoberturaDTO coberturaSumaAsegurada) {
		this.coberturaSumaAsegurada = coberturaSumaAsegurada;
	}

	@Column(name = "CLAVETIPOLIMITESUMAASEG", nullable = false, precision = 4, scale = 0)
	public Short getClaveTipoLimiteSumaAsegurada() {
		return claveTipoLimiteSumaAsegurada;
	}

	public void setClaveTipoLimiteSumaAsegurada(
			Short claveTipoLimiteSumaAsegurada) {
		this.claveTipoLimiteSumaAsegurada = claveTipoLimiteSumaAsegurada;
	}

	@Column(name = "VALORSUMAASEGURADAMIN", nullable = false, precision = 16)
	public BigDecimal getValorSumaAseguradaMin() {
		return valorSumaAseguradaMin;
	}

	public void setValorSumaAseguradaMin(BigDecimal valorSumaAseguradaMin) {
		this.valorSumaAseguradaMin = valorSumaAseguradaMin;
	}

	@Column(name = "VALORSUMAASEGURADAMAX", nullable = false, precision = 16)
	public BigDecimal getValorSumaAseguradaMax() {
		return valorSumaAseguradaMax;
	}

	public void setValorSumaAseguradaMax(BigDecimal valorSumaAseguradaMax) {
		this.valorSumaAseguradaMax = valorSumaAseguradaMax;
	}

	@Column(name = "SUMAASEGURADA", nullable = false, precision = 16)
	public BigDecimal getSumaAsegurada() {
		return sumaAsegurada;
	}

	public void setSumaAsegurada(BigDecimal sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}

	@Column(name = "CLAVECONTRATODM", nullable = false, precision = 4, scale = 0)
	public Integer getClaveContratoDm() {
		return claveContratoDm;
	}

	public void setClaveContratoDm(Integer claveContratoDm) {
		this.claveContratoDm = claveContratoDm;
	}

	@Column(name = "CLAVECONTRATORT", nullable = false, precision = 4, scale = 0)
	public Integer getClaveContratoRt() {
		return claveContratoRt;
	}

	public void setClaveContratoRt(Integer claveContratoRt) {
		this.claveContratoRt = claveContratoRt;
	}

	@Column(name = "CLAVECONTRATORC", nullable = false, precision = 4, scale = 0)
	public Integer getClaveContratoRc() {
		return claveContratoRc;
	}

	public void setClaveContratoRc(Integer claveContratoRc) {
		this.claveContratoRc = claveContratoRc;
	}

	@Column(name = "IDTIPODEDUCIBLE", nullable = false, precision = 4, scale = 0)
	public Integer getIdTipoDeducible() {
		return idTipoDeducible;
	}

	public void setIdTipoDeducible(Integer idTipoDeducible) {
		this.idTipoDeducible = idTipoDeducible;
	}

	@Column(name = "PCTDEDUCIBLE", nullable = false, precision = 8, scale = 4)
	public BigDecimal getPctDeducible() {
		return pctDeducible;
	}

	public void setPctDeducible(BigDecimal pctDeducible) {
		this.pctDeducible = pctDeducible;
	}

	@Column(name = "DSMGVDFDEDUCIBLE", nullable = false, precision = 22, scale = 0)
	public BigDecimal getDsmgvdfDeducible() {
		return dsmgvdfDeducible;
	}

	public void setDsmgvdfDeducible(BigDecimal dsmgvdfDeducible) {
		this.dsmgvdfDeducible = dsmgvdfDeducible;
	}

	@Column(name = "PRIMANETAB", nullable = false, precision = 16)
	public BigDecimal getPrimaNetaB() {
		return primaNetaB;
	}

	public void setPrimaNetaB(BigDecimal primaNetaB) {
		this.primaNetaB = primaNetaB;
	}

	@Column(name = "PRIMANETAARDT", nullable = false, precision = 16)
	public BigDecimal getPrimaNetaARdt() {
		return primaNetaARdt;
	}

	public void setPrimaNetaARdt(BigDecimal primaNetaARdt) {
		this.primaNetaARdt = primaNetaARdt;
	}

}