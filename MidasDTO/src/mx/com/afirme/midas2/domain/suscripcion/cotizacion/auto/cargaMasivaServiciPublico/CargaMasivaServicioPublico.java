package mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;

@Entity
@Table(name = "TOARCHIVOCARGAMASIVASP", schema = "MIDAS")
public class CargaMasivaServicioPublico extends MidasAbstracto implements Entidad{

	private static final long serialVersionUID = -2848054585353649291L;

	public enum EstatusArchivo{
		PROCESO(new Long(1), "Archivo en proceso de validacion"),
		TERMINADO_EXITO(new Long(2), "Ver detalle de los registros"),
		TERMINADO_ERRORES(new Long(3), "Registros con errores (ver detalle)"),
		NO_RECONOCIDO(new Long(4), "No reconocido"),
		ERROR_CARGA(new Long (5), "Error al cargar archivo");
		
		private final Long estatus;
		private final String descripcion;
		
		private EstatusArchivo(Long estatus, String descripcion) {
			this.estatus = estatus;
			this.descripcion = descripcion;
		}
		
		public Long getEstatusArchivo(){
			return estatus;
		}
		public String getDescripcionEstatus(){
			return descripcion;
		}
		
		public Long getEstatus() {
			return estatus;
		}
		public String getDescripcion() {
			return descripcion;
		}
		
	}

	/***************************************DECLARA ATRIBUTOS Y RELACIÓN ENTIDAD BD*******************************************/
	@Id
	@SequenceGenerator(name = "TOARCHIVOCARGAMASIVASP_ID_GENERATOR", sequenceName = "TOARCHIVOCARGAMASIVASP_ID_SEQ", allocationSize=1, schema="MIDAS")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TOARCHIVOCARGAMASIVASP_ID_GENERATOR")
	@Column(name = "ID")
	private Long idArchivoCargaMasiva;
	
	@Column(name = "NOMBREARCHIVO")
	private String nombreArchivoCargaMasiva;
	
	@Column(name = "IDENTIFYFILE")
	private String md5CargaMasiva;
	
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "ARCHIVO", columnDefinition = "BLOB NULL")
	private byte[] archivo;
	
	@Column(name = "ESTATUS")
	private Long estatusArchivoCargaMasiva;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "archivoCargaMasivaSP_ID")
	private List<CargaMasivaServicioPublicoDetalle> cargaMasivaSerPubDetalle = new ArrayList<CargaMasivaServicioPublicoDetalle>(1);
	
	@Transient
	private String descripcionEstatus;
	
	/****************************SET & GET DE ATRIBUTOS********************************/
	
	public Long getIdArchivoCargaMasiva() {
		return idArchivoCargaMasiva;
	}
	public void setIdArchivoCargaMasiva(Long idArchivoCargaMasiva) {
		this.idArchivoCargaMasiva = idArchivoCargaMasiva;
	}

	public String getNombreArchivoCargaMasiva() {
		return nombreArchivoCargaMasiva;
	}
	public void setNombreArchivoCargaMasiva(String nombreArchivoCargaMasiva) {
		this.nombreArchivoCargaMasiva = nombreArchivoCargaMasiva;
	}

	public String getMd5CargaMasiva() {
		return md5CargaMasiva;
	}
	public void setMd5CargaMasiva(String md5CargaMasiva) {
		this.md5CargaMasiva = md5CargaMasiva;
	}

	public byte[] getArchivo() {
		return archivo;
	}
	public void setArchivo(byte[] archivo) {
		this.archivo = archivo;
	}

	public Long getEstatusArchivoCargaMasiva() {
		return estatusArchivoCargaMasiva;
	}
	public void setEstatusArchivoCargaMasiva(Long estatusArchivoCargaMasiva) {
		this.estatusArchivoCargaMasiva = estatusArchivoCargaMasiva;
	}

	public List<CargaMasivaServicioPublicoDetalle> getCargaMasivaSerPubDetalle() {
		return cargaMasivaSerPubDetalle;
	}
	public void setCargaMasivaSerPubDetalle(List<CargaMasivaServicioPublicoDetalle> cargaMasivaSerPubDetalle) {
		this.cargaMasivaSerPubDetalle = cargaMasivaSerPubDetalle;
	}

	public void setDescripcionEstatus(String descripcionEstatus) {
		this.descripcionEstatus = descripcionEstatus;
	}

	public String getDescripcionEstatus() {
		switch (Integer.parseInt(this.estatusArchivoCargaMasiva.toString())) {
		case 1:
			descripcionEstatus = EstatusArchivo.PROCESO.getDescripcion();
			break;
		case 2:
			descripcionEstatus = EstatusArchivo.TERMINADO_EXITO.getDescripcion();
			break;
		case 3:
			descripcionEstatus = EstatusArchivo.TERMINADO_ERRORES.getDescripcion();
			break;
		case 4:
			descripcionEstatus = EstatusArchivo.NO_RECONOCIDO.getDescripcion();
			break;
		case 5:
			descripcionEstatus = EstatusArchivo.ERROR_CARGA.getDescripcion();
			break;
		}
		
		return descripcionEstatus;
	}

	/************************************IMPLEMENTADOS DE Entidad*****************************************/
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.idArchivoCargaMasiva;
	}

	@Override
	public String getValue() {
		return this.md5CargaMasiva.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getBusinessKey() {
		return this.md5CargaMasiva;
	}
}
