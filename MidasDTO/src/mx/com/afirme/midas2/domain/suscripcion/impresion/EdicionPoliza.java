package mx.com.afirme.midas2.domain.suscripcion.impresion;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.dto.impresiones.DatosLineasDeNegocioDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosPolizaDTO;

/**
 * Clase para almacenar la informacion que se desea modificar en la impresion 
 * de la caratula de la poliza. Estos cambios no afectan la informacion del
 * sistema, unicamente son modificaciones para la impresion. 
 * 
 */
@Entity
@Table(name="TOEDICIONPOLIZA", schema="MIDAS")
public class EdicionPoliza extends MidasAbstracto implements Entidad{

	private static final long serialVersionUID = -8959367034731686579L;
	
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToPoliza", column = @Column(name = "IDTOPOLIZA", nullable = false)),
			@AttributeOverride(name = "validOn", column = @Column(name = "VALID_ON", nullable = false)),
			@AttributeOverride(name = "recordFrom", column = @Column(name = "RECORD_FROM", nullable = false)),
			@AttributeOverride(name = "claveTipoEndoso", column = @Column(name = "TIPO_ENDOSO", nullable = false)),
			@AttributeOverride(name = "esSituacionActual", column = @Column(name = "ES_SITUACION_ACTUAL", nullable = false)),
			@AttributeOverride(name = "version", column = @Column(name = "VERSION", nullable = false)) })
	private EdicionPolizaId id;
	
	//FIELDS
	@Column(name="HORA_INICIO_VIGENCIA")
	private String 		horaInicioVigencia;
	
	@Column(name="HORA_FIN_VIGENCIA")
	private String 		horaFinVigencia;
	
	@Column(name="NOMBRE_CONTRATANTE")
	private String 		nombreContratante;
	
	@Column(name="INFORMACION_ADICIONAL1")
	private String 		informacionAdicional1;
	
	@Column(name="INFORMACION_ADICIONAL2")
	private String 		informacionAdicional2;
	
	@Column(name="DOMICILIO")
	private String 		txDomicilio;
	
	@Column(name="RFC_CONTRATANTE")
	private String 		rfcContratante;
	
	@Column(name="PERSONA_CONTRATANTE_ID")
	private BigDecimal 	idToPersonaContratante;
	
	@Column(name="IMPRIMIR_PRIMAS")
	private Boolean 	imprimirPrimas = Boolean.TRUE;
	
	@Column(name="MONTO_IVA")
	private Double 		montoIva;
	
	@Column(name="OBSERVACIONES")
	private String 		observaciones;	//303
	
	//PARAMETERS
	@Column(name="DATOS_AGENTE_CARATULA")
	private String 		datosAgenteCaratula;
	
	@Transient
	private DatosPolizaDTO datosPoliza;
	
	@Transient
	private List<DatosLineasDeNegocioDTO> datosLineasDeNegocio;
	
	@Transient
	private PolizaDTO polizaDTO;
	
	@Transient
	private boolean esSeguroObligatorio;
	
	
	@SuppressWarnings("unchecked")
	@Override
	public EdicionPolizaId getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public EdicionPolizaId getBusinessKey() {
		return id;
	}

	public EdicionPolizaId getId() {
		return id;
	}

	public void setId(EdicionPolizaId id) {
		this.id = id;
	}

	public String getNombreContratante() {
		return nombreContratante;
	}

	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}

	public String getInformacionAdicional1() {
		return informacionAdicional1;
	}

	public void setInformacionAdicional1(String informacionAdicional1) {
		this.informacionAdicional1 = informacionAdicional1;
	}

	public String getInformacionAdicional2() {
		return informacionAdicional2;
	}

	public void setInformacionAdicional2(String informacionAdicional2) {
		this.informacionAdicional2 = informacionAdicional2;
	}

	public String getTxDomicilio() {
		return txDomicilio;
	}

	public void setTxDomicilio(String txDomicilio) {
		this.txDomicilio = txDomicilio;
	}

	public String getRfcContratante() {
		return rfcContratante;
	}

	public void setRfcContratante(String rfcContratante) {
		this.rfcContratante = rfcContratante;
	}

	public BigDecimal getIdToPersonaContratante() {
		return idToPersonaContratante;
	}

	public void setIdToPersonaContratante(BigDecimal idToPersonaContratante) {
		this.idToPersonaContratante = idToPersonaContratante;
	}

	public Boolean getImprimirPrimas() {
		return imprimirPrimas;
	}

	public void setImprimirPrimas(Boolean imprimirPrimas) {
		this.imprimirPrimas = imprimirPrimas;
	}

	public Double getMontoIva() {
		return montoIva;
	}

	public void setMontoIva(Double montoIva) {
		this.montoIva = montoIva;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getDatosAgenteCaratula() {
		return datosAgenteCaratula;
	}

	public void setDatosAgenteCaratula(String datosAgenteCaratula) {
		this.datosAgenteCaratula = datosAgenteCaratula;
	}

	public String getHoraInicioVigencia() {
		return horaInicioVigencia;
	}

	public void setHoraInicioVigencia(String horaInicioVigencia) {
		this.horaInicioVigencia = horaInicioVigencia;
	}

	public String getHoraFinVigencia() {
		return horaFinVigencia;
	}

	public void setHoraFinVigencia(String horaFinVigencia) {
		this.horaFinVigencia = horaFinVigencia;
	}

	public DatosPolizaDTO getDatosPoliza() {
		return datosPoliza;
	}

	public void setDatosPoliza(DatosPolizaDTO datosPoliza) {
		this.datosPoliza = datosPoliza;
	}

	public List<DatosLineasDeNegocioDTO> getDatosLineasDeNegocio() {
		return datosLineasDeNegocio;
	}

	public void setDatosLineasDeNegocio(
			List<DatosLineasDeNegocioDTO> datosLineasDeNegocio) {
		this.datosLineasDeNegocio = datosLineasDeNegocio;
	}

	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	public boolean isEsSeguroObligatorio() {
		return esSeguroObligatorio;
	}

	public void setEsSeguroObligatorio(boolean esSeguroObligatorio) {
		this.esSeguroObligatorio = esSeguroObligatorio;
	}


	
}
