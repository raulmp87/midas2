package mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion;

import java.io.Serializable;
import javax.persistence.*;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.dato.DatoSeccion;

import java.math.BigDecimal;

/**
 * The persistent class for the TODATOINCISOCOTAUTO database table.
 * 
 */
@Entity
@Table(name = "TODATOINCISOCOTAUTO", schema="MIDAS")
public class DatoIncisoCotAuto implements Serializable,Entidad {
	private static final long serialVersionUID = 1L;

	private Long id;

	private BigDecimal claveDetalle;

	private BigDecimal idDato;

	private BigDecimal idTcRamo;

	private BigDecimal idTcSubRamo;

	private BigDecimal idToCobertura;

	private BigDecimal idToCotizacion;

	private BigDecimal idToSeccion;

	private BigDecimal numeroInciso;

	private String valor;

	public DatoIncisoCotAuto() {
	}
	
	public DatoIncisoCotAuto(DatoSeccion value) {
		this.valor = value.getValor();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTODATOINCISOCOTAUTO_ID_GENERATOR")
	@SequenceGenerator(name = "IDTODATOINCISOCOTAUTO_ID_GENERATOR", sequenceName = "MIDAS.IDTODATOINCISOCOTAUTO_SEQ", allocationSize = 1)
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "CLAVEDETALLE")
	public BigDecimal getClaveDetalle() {
		return this.claveDetalle;
	}

	public void setClaveDetalle(BigDecimal claveDetalle) {
		this.claveDetalle = claveDetalle;
	}

	@Column(name = "IDDATO")
	public BigDecimal getIdDato() {
		return this.idDato;
	}

	public void setIdDato(BigDecimal idDato) {
		this.idDato = idDato;
	}

	@Column(name = "IDTCRAMO")
	public BigDecimal getIdTcRamo() {
		return this.idTcRamo;
	}

	public void setIdTcRamo(BigDecimal idTcRamo) {
		this.idTcRamo = idTcRamo;
	}

	@Column(name = "IDTCSUBRAMO")
	public BigDecimal getIdTcSubRamo() {
		return this.idTcSubRamo;
	}

	public void setIdTcSubRamo(BigDecimal idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}

	@Column(name = "IDTOCOBERTURA")
	public BigDecimal getIdToCobertura() {
		return this.idToCobertura;
	}

	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	@Column(name = "IDTOCOTIZACION")
	public BigDecimal getIdToCotizacion() {
		return this.idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	@Column(name = "IDTOSECCION")
	public BigDecimal getIdToSeccion() {
		return this.idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	@Column(name = "NUMEROINCISO")
	public BigDecimal getNumeroInciso() {
		return this.numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	@Column(name = "VALOR")
	public String getValor() {
		return this.valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	@SuppressWarnings("unchecked")
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		return this.id.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.id;
	}

}