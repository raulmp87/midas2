package mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso;


import java.io.Serializable;
import javax.persistence.*;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TCDESCUENTOPORVOLUMENAUTO database table.
 * 
 */
@Entity
@Table(name="TCDESCUENTOPORVOLUMENAUTO", schema="MIDAS")
public class DescuentoPorVolumenAuto implements Serializable, Entidad {


	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5325357948932367984L;
	private Long id;
	private BigDecimal creationUser;
	private Date creatorDate;
	private double defaultDiscountVolumen;
	private BigDecimal maxTotalSubsections;
	private BigDecimal minTotalSubsections;
	private Date modificationDate;
	private BigDecimal modificatorUser;

    public DescuentoPorVolumenAuto() {
    }
    
    @Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TCDESCUENTOPORVOLUMENAUTO_SEQ")
	@SequenceGenerator(name="TCDESCUENTOPORVOLUMENAUTO_SEQ", sequenceName="MIDAS.TCDESCUENTOPORVOLUMENAUTO_SEQ", allocationSize=1)
	@Column(name = "ID", nullable = false, precision = 22, scale = 0)
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="CREATION_USER")
	public BigDecimal getCreationUser() {
		return this.creationUser;
	}

	public void setCreationUser(BigDecimal creationUser) {
		this.creationUser = creationUser;
	}
	
	@Temporal( TemporalType.DATE)
	@Column(name="CREATOR_DATE")
	public Date getCreatorDate() {
		return this.creatorDate;
	}

	public void setCreatorDate(Date creatorDate) {
		this.creatorDate = creatorDate;
	}

	@Column(name="DEFAULT_DISCOUNT_VOLUMEN")
	public double getDefaultDiscountVolumen() {
		return this.defaultDiscountVolumen;
	}

	public void setDefaultDiscountVolumen(double defaultDiscountVolumen) {
		this.defaultDiscountVolumen = defaultDiscountVolumen;
	}

	@Column(name="MAX_TOTAL_SUBSECTIONS")
	public BigDecimal getMaxTotalSubsections() {
		return this.maxTotalSubsections;
	}

	public void setMaxTotalSubsections(BigDecimal maxTotalSubsections) {
		this.maxTotalSubsections = maxTotalSubsections;
	}
	
	@Column(name="MIN_TOTAL_SUBSECTIONS")
	public BigDecimal getMinTotalSubsections() {
		return this.minTotalSubsections;
	}

	public void setMinTotalSubsections(BigDecimal minTotalSubsections) {
		this.minTotalSubsections = minTotalSubsections;
	}

    @Temporal( TemporalType.DATE)
	@Column(name="MODIFICATION_DATE")
	public Date getModificationDate() {
		return this.modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	@Column(name="MODIFICATOR_USER")
	public BigDecimal getModificatorUser() {
		return this.modificatorUser;
	}

	public void setModificatorUser(BigDecimal modificatorUser) {
		this.modificatorUser = modificatorUser;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

}
