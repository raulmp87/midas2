package mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto;

import java.io.Serializable;

import javax.persistence.Transient;

public class CoberturaCotizacionExpress implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private CoberturaPrimaCotExpress coberturaPrimaCotExpress;
	private String sumaAseguradaStr;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public CoberturaPrimaCotExpress getCoberturaPrimaCotExpress() {
		return coberturaPrimaCotExpress;
	}
	
	public void setCoberturaPrimaCotExpress(
			CoberturaPrimaCotExpress coberturaPrimaCotExpress) {
		this.coberturaPrimaCotExpress = coberturaPrimaCotExpress;
	}
	
	@Transient
	public String getSumaAseguradaStr() {
		return sumaAseguradaStr;
	}
	
	public void setSumaAseguradaStr(String sumaAseguradaStr) {
		this.sumaAseguradaStr = sumaAseguradaStr;
	}
	
}
