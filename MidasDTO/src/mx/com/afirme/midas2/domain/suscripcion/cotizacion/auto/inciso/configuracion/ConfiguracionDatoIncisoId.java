package mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;

/**
 * The primary key class for the TCCONFIGDATOINCISOCOTAUTO database table.
 * 
 */
@Embeddable
public class ConfiguracionDatoIncisoId implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7985790480899561452L;

	private BigDecimal idTcRamo;

	private BigDecimal idTcSubRamo;

	private BigDecimal idToCobertura;

	private Short claveDetalle;

	private BigDecimal idDato;

	public ConfiguracionDatoIncisoId() {
	}

	@Column(name = "IDTCRAMO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcRamo() {
		return this.idTcRamo;
	}

	public void setIdTcRamo(BigDecimal idTcRamo) {
		this.idTcRamo = idTcRamo;
	}

	@Column(name = "IDTCSUBRAMO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdTcSubRamo() {
		return this.idTcSubRamo;
	}

	public void setIdTcSubRamo(BigDecimal idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}

	@Column(name = "IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCobertura() {
		return this.idToCobertura;
	}

	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	@Column(name = "CLAVEDETALLE", nullable = false, precision = 4, scale = 0)
	public Short getClaveDetalle() {
		return this.claveDetalle;
	}

	public void setClaveDetalle(Short claveDetalle) {
		this.claveDetalle = claveDetalle;
	}

	@Column(name = "IDDATO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdDato() {
		return this.idDato;
	}

	public void setIdDato(BigDecimal idDato) {
		this.idDato = idDato;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((claveDetalle == null) ? 0 : claveDetalle.hashCode());
		result = prime * result + ((idDato == null) ? 0 : idDato.hashCode());
		result = prime * result
				+ ((idTcRamo == null) ? 0 : idTcRamo.hashCode());
		result = prime * result
				+ ((idTcSubRamo == null) ? 0 : idTcSubRamo.hashCode());
		result = prime * result
				+ ((idToCobertura == null) ? 0 : idToCobertura.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConfiguracionDatoIncisoId other = (ConfiguracionDatoIncisoId) obj;
		if (claveDetalle == null) {
			if (other.claveDetalle != null)
				return false;
		} else if (!claveDetalle.equals(other.claveDetalle))
			return false;
		if (idDato == null) {
			if (other.idDato != null)
				return false;
		} else if (!idDato.equals(other.idDato))
			return false;
		if (idTcRamo == null) {
			if (other.idTcRamo != null)
				return false;
		} else if (!idTcRamo.equals(other.idTcRamo))
			return false;
		if (idTcSubRamo == null) {
			if (other.idTcSubRamo != null)
				return false;
		} else if (!idTcSubRamo.equals(other.idTcSubRamo))
			return false;
		if (idToCobertura == null) {
			if (other.idToCobertura != null)
				return false;
		} else if (!idToCobertura.equals(other.idToCobertura))
			return false;
		return true;
	}

}
