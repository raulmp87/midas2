package mx.com.afirme.midas2.domain.suscripcion.cambiosglobales;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;

/**
 * The primary key class for the TOCONFPLANTILLACOBERTURAS database table.
 * 
 */
@Embeddable
public class ConfiguracionPlantillaNegCoberturaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;
	private BigDecimal coberturaId;
	private Long plantillaId;

    public ConfiguracionPlantillaNegCoberturaPK() {
    }

	@Column(name="COBERTURA_ID", unique=true, nullable=false)	
	public BigDecimal getCoberturaId() {
		return this.coberturaId;
	}
	public void setCoberturaId(BigDecimal coberturaId) {
		this.coberturaId = coberturaId;
	}

	@Column(name="PLANTILLA_ID", unique=true, nullable=false)
	public Long getPlantillaId() {
		return this.plantillaId;
	}
	public void setPlantillaId(Long plantillaId) {
		this.plantillaId = plantillaId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ConfiguracionPlantillaNegCoberturaPK)) {
			return false;
		}
		ConfiguracionPlantillaNegCoberturaPK castOther = (ConfiguracionPlantillaNegCoberturaPK)other;
		return 
			(this.coberturaId == castOther.coberturaId)
			&& (this.plantillaId == castOther.plantillaId);

    }
    
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.coberturaId.longValue() ^ (this.coberturaId.longValue() >>> 32)));
		hash = hash * prime + ((int) (this.plantillaId ^ (this.plantillaId >>> 32)));
		
		return hash;
    }
}