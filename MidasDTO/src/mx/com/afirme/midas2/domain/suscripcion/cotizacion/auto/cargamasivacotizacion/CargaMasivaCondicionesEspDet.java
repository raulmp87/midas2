package mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasivacotizacion;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name = "TOCARGAMASIVACONDESPDETALLE", schema = "MIDAS")
public class CargaMasivaCondicionesEspDet implements Serializable,Entidad{

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long idCargaMasivaCondicionesEsp;
	private Long idToCotizacion;
	private Long idInciso;
	private String condicionesAlta;
	private String condicionesBaja;
	private CargaMasivaCondicionesEsp cargaMasivaCondiciones;
	
	
	@Id
	@SequenceGenerator(name = "IDTOCARGAMASIVACONDESPDET_SEQ", sequenceName = "IDTOCARGAMASIVACONDESPDET_SEQ", schema="MIDAS", allocationSize =1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOCARGAMASIVACONDESPDET_SEQ")
	@Column(name = "ID", unique = true, nullable = false, scale = 0)
	public Long getId() {
		return id;
	}

	@Column(name = "IDCARGAMASIVACONDICIONESESP", nullable = false)
	public Long getIdCargaMasivaCondicionesEsp() {
		return idCargaMasivaCondicionesEsp;
	}

	@Column(name = "IDTOCOTIZACION")
	public Long getIdToCotizacion() {
		return idToCotizacion;
	}

	@Column(name = "IDINCISO")
	public Long getIdInciso() {
		return idInciso;
	}
	
	@Column(name = "CONDICIONESALTA", nullable = true)
	public String getCondicionesAlta() {
		return condicionesAlta;
	}
	
	@Column(name = "CONDICIONESBAJA", nullable = true)
	public String getCondicionesBaja() {
		return condicionesBaja;
	}

	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDCARGAMASIVACONDICIONESESP", referencedColumnName="ID",nullable = false, insertable = false, updatable = false)
	public CargaMasivaCondicionesEsp getCargaMasivaCondiciones() {
		return cargaMasivaCondiciones;
	}

	
	public void setCondicionesAlta(String condicionesAlta) {
		this.condicionesAlta = condicionesAlta;
	}

	public void setCondicionesBaja(String condicionesBaja) {
		this.condicionesBaja = condicionesBaja;
	}
	
	public void setCargaMasivaCondiciones(
			CargaMasivaCondicionesEsp cargaMasivaCondiciones) {
		this.cargaMasivaCondiciones = cargaMasivaCondiciones;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public void setIdCargaMasivaCondicionesEsp(Long idCargaMasivaCondicionesEsp) {
		this.idCargaMasivaCondicionesEsp = idCargaMasivaCondicionesEsp;
	}
	
	public void setIdToCotizacion(Long idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	
	public void setIdInciso(Long idInciso) {
		this.idInciso = idInciso;
	}

	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long getBusinessKey() {
		return this.getIdCargaMasivaCondicionesEsp();
	}




}
