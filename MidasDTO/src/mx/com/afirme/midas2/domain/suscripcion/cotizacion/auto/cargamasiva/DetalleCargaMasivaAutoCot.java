package mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasiva;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
/**
 * TOCARGAMASIVAAUTODETALLECOT entity. @author Lizandro Perez
 */
@Entity
@Table(name = "TOCARGAMASIVAAUTODETALLECOT", schema = "MIDAS")
public class DetalleCargaMasivaAutoCot implements Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal idToDetalleCargaMasivaAutoCot;
	private BigDecimal idToCargaMasivaAutoCot;
	private BigDecimal numeroInciso;
	private Short claveEstatus;	
	//Datos n Proceso
	private String lineaNegocioDescripcion;
	private Long idNegocioSeccion;
	private String marcaDescripcion;
	private String tipoUsoDescripcion;
	private Long idTipoDeUso;
	private String descripcion;
	private String claveAMIS;
	private Short modeloVehiculo;
	private String paqueteDescripcion;
	private String codigoPostal;
	private Double deducibleDanosMateriales;
	private Double deducibleRoboTotal;
	private Double limiteRcTerceros;
	private Double limiteGastosMedicos;
	private Short asistenciaJuridica;
	private Short asistenciaViajes;
	private Short exencionDeducibleDanosMateriales;
	private Double limiteAccidenteConductor;
	private Short exencionRC;
	private Double limiteAdaptacionConversion;
	private Double limiteEquipoEspecial;
	private Double deducibleEquipoEspecial;
	private Short exencionDeducibleRoboTotal;
	private Short responsabilidadCivilUsaCanada;
	private Double igualacion;
	private Integer numeroRemolques;
	private String tipoCargaDescripcion;
	private CatalogoValorFijoDTO tipoCarga;
	private Double limiteMuerte;
	private Double valorSumaAseguradaDM;
	private Double valorSumaAseguradaRT;
	private String emailContactos;
	private Double diasSalarioMinimo;
	private String idMedioPago;
	private String conductoCobro;
	private String institucionBancaria;
	private String tipoTarjeta;
	private String numeroTarjetaClave;
	private String codigoSeguridad;
	private String fechaVencimiento;
	
	// Atributos @Transient
	private List<CondicionEspecial> altaCondicionesEspeciales;
	private List<CondicionEspecial> bajaCondicionesEspeciales;
	private IncisoCotizacionDTO incisoCotizacion;
		
	//Terminado
	private String asegurado;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String numeroLicencia;
	private Date fechaNacimiento;
	private String numeroMotor;
	private String numeroSerie;
	private String numeroPlaca;
	private String repuve;
	private String descripcionEquipoEspecial;
	private String descripcionAdaptacionConversion;
	private String ocupacion;
	private String observacionesInciso;
	
	private String mensajeError;
	
	public static final Short ESTATUS_ERROR = 0;
	public static final Short ESTATUS_COTIZADO = 1;
	public static final Short ESTATUS_PENDIENTE = 2;
	public static final Short ESTATUS_TERMINADO = 3;

	public String getDescripcionEstatus(){
		String descripcionEstatus = "";
		if(claveEstatus.equals(ESTATUS_ERROR)){
			descripcionEstatus = "ERROR";
		}else if(claveEstatus.equals(ESTATUS_COTIZADO)){
			descripcionEstatus = "COTIZADO";
		}else if(claveEstatus.equals(ESTATUS_PENDIENTE)){
			descripcionEstatus = "PENDIENTE";
		}else if(claveEstatus.equals(ESTATUS_TERMINADO)){
			descripcionEstatus = "TERMINADO";
		}
		
		return descripcionEstatus;
	}

	public void setIdToDetalleCargaMasivaAutoCot(
			BigDecimal idToDetalleCargaMasivaAutoCot) {
		this.idToDetalleCargaMasivaAutoCot = idToDetalleCargaMasivaAutoCot;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTOCARGAMASIVAAUTODETCOT_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOCARGAMASIVAAUTODETCOT_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOCARGAMASIVAAUTODETCOT_SEQ_GENERADOR")	  	
	@Column(name = "IDTODETALLECARGAMASIVAAUTOCOT", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToDetalleCargaMasivaAutoCot() {
		return idToDetalleCargaMasivaAutoCot;
	}

	public void setIdToCargaMasivaAutoCot(BigDecimal idToCargaMasivaAutoCot) {
		this.idToCargaMasivaAutoCot = idToCargaMasivaAutoCot;
	}

	@Column(name = "IDTOCARGAMASIVAAUTOCOT", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCargaMasivaAutoCot() {
		return idToCargaMasivaAutoCot;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	@Column(name = "NUMEROINCISO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setClaveEstatus(Short claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	@Column(name = "CLAVEESTATUS", nullable = false, precision = 4, scale = 0)
	public Short getClaveEstatus() {
		return claveEstatus;
	}

	public void setLineaNegocioDescripcion(String lineaNegocioDescripcion) {
		this.lineaNegocioDescripcion = lineaNegocioDescripcion;
	}

	@Column(name = "LINEANEGOCIODESCRIPCION", nullable = false, length = 200)
	public String getLineaNegocioDescripcion() {
		return lineaNegocioDescripcion;
	}

	public void setIdNegocioSeccion(Long idNegocioSeccion) {
		this.idNegocioSeccion = idNegocioSeccion;
	}
	@Transient
	public Long getIdNegocioSeccion() {
		return idNegocioSeccion;
	}

	public void setMarcaDescripcion(String marcaDescripcion) {
		this.marcaDescripcion = marcaDescripcion;
	}

	@Transient
	public String getMarcaDescripcion() {
		return marcaDescripcion;
	}

	public void setTipoUsoDescripcion(String tipoUsoDescripcion) {
		this.tipoUsoDescripcion = tipoUsoDescripcion;
	}

	@Transient
	public String getTipoUsoDescripcion() {
		return tipoUsoDescripcion;
	}

	public void setIdTipoDeUso(Long idTipoDeUso) {
		this.idTipoDeUso = idTipoDeUso;
	}
	
	@Transient
	public Long getIdTipoDeUso() {
		return idTipoDeUso;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "DESCRIPCION", nullable = false, length = 200)
	public String getDescripcion() {
		return descripcion;
	}
		
	public void setClaveAMIS(String claveAMIS) {
		this.claveAMIS = claveAMIS;
	}

	@Transient
	public String getClaveAMIS() {
		return claveAMIS;
	}

	public void setModeloVehiculo(Short modeloVehiculo) {
		this.modeloVehiculo = modeloVehiculo;
	}

	@Transient
	public Short getModeloVehiculo() {
		return modeloVehiculo;
	}

	public void setPaqueteDescripcion(String paqueteDescripcion) {
		this.paqueteDescripcion = paqueteDescripcion;
	}

	@Transient
	public String getPaqueteDescripcion() {
		return paqueteDescripcion;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	@Column(name = "MENSAJEERROR", nullable = false, length = 500)
	public String getMensajeError() {
		return mensajeError;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.idToDetalleCargaMasivaAutoCot;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	@Transient
	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setDeducibleDanosMateriales(Double deducibleDanosMateriales) {
		this.deducibleDanosMateriales = deducibleDanosMateriales;
	}

	@Transient
	public Double getDeducibleDanosMateriales() {
		return deducibleDanosMateriales;
	}

	public void setDeducibleRoboTotal(Double deducibleRoboTotal) {
		this.deducibleRoboTotal = deducibleRoboTotal;
	}

	@Transient
	public Double getDeducibleRoboTotal() {
		return deducibleRoboTotal;
	}

	public void setLimiteRcTerceros(Double limiteRcTerceros) {
		this.limiteRcTerceros = limiteRcTerceros;
	}

	@Transient
	public Double getLimiteRcTerceros() {
		return limiteRcTerceros;
	}

	public void setLimiteGastosMedicos(Double limiteGastosMedicos) {
		this.limiteGastosMedicos = limiteGastosMedicos;
	}

	@Transient
	public Double getLimiteGastosMedicos() {
		return limiteGastosMedicos;
	}

	public void setAsistenciaJuridica(Short asistenciaJuridica) {
		this.asistenciaJuridica = asistenciaJuridica;
	}

	@Transient
	public Short getAsistenciaJuridica() {
		return asistenciaJuridica;
	}

	public void setAsistenciaViajes(Short asistenciaViajes) {
		this.asistenciaViajes = asistenciaViajes;
	}

	@Transient
	public Short getAsistenciaViajes() {
		return asistenciaViajes;
	}

	public void setExencionDeducibleDanosMateriales(
			Short exencionDeducibleDanosMateriales) {
		this.exencionDeducibleDanosMateriales = exencionDeducibleDanosMateriales;
	}

	@Transient
	public Short getExencionDeducibleDanosMateriales() {
		return exencionDeducibleDanosMateriales;
	}

	public void setLimiteAccidenteConductor(Double limiteAccidenteConductor) {
		this.limiteAccidenteConductor = limiteAccidenteConductor;
	}

	@Transient
	public Double getLimiteAccidenteConductor() {
		return limiteAccidenteConductor;
	}

	public void setExencionRC(Short exencionRC) {
		this.exencionRC = exencionRC;
	}

	@Transient
	public Short getExencionRC() {
		return exencionRC;
	}

	public void setLimiteAdaptacionConversion(Double limiteAdaptacionConversion) {
		this.limiteAdaptacionConversion = limiteAdaptacionConversion;
	}

	@Transient
	public Double getLimiteAdaptacionConversion() {
		return limiteAdaptacionConversion;
	}

	public void setLimiteEquipoEspecial(Double limiteEquipoEspecial) {
		this.limiteEquipoEspecial = limiteEquipoEspecial;
	}

	@Transient
	public Double getLimiteEquipoEspecial() {
		return limiteEquipoEspecial;
	}

	public void setDeducibleEquipoEspecial(Double deducibleEquipoEspecial) {
		this.deducibleEquipoEspecial = deducibleEquipoEspecial;
	}

	@Transient
	public Double getDeducibleEquipoEspecial() {
		return deducibleEquipoEspecial;
	}

	public void setExencionDeducibleRoboTotal(Short exencionDeducibleRoboTotal) {
		this.exencionDeducibleRoboTotal = exencionDeducibleRoboTotal;
	}

	@Transient
	public Short getExencionDeducibleRoboTotal() {
		return exencionDeducibleRoboTotal;
	}

	public void setResponsabilidadCivilUsaCanada(
			Short responsabilidadCivilUsaCanada) {
		this.responsabilidadCivilUsaCanada = responsabilidadCivilUsaCanada;
	}

	@Transient
	public Short getResponsabilidadCivilUsaCanada() {
		return responsabilidadCivilUsaCanada;
	}

	public void setAsegurado(String asegurado) {
		this.asegurado = asegurado;
	}

	@Transient
	public String getAsegurado() {
		return asegurado;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Transient
	public String getNombre() {
		return nombre;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	@Transient
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	@Transient
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setNumeroLicencia(String numeroLicencia) {
		this.numeroLicencia = numeroLicencia;
	}

	@Transient
	public String getNumeroLicencia() {
		return numeroLicencia;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	@Transient
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setNumeroMotor(String numeroMotor) {
		this.numeroMotor = numeroMotor;
	}

	@Transient
	public String getNumeroMotor() {
		return numeroMotor;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	@Transient
	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroPlaca(String numeroPlaca) {
		this.numeroPlaca = numeroPlaca;
	}

	@Transient
	public String getNumeroPlaca() {
		return numeroPlaca;
	}

	public void setRepuve(String repuve) {
		this.repuve = repuve;
	}

	@Transient
	public String getRepuve() {
		return repuve;
	}

	public void setDescripcionEquipoEspecial(String descripcionEquipoEspecial) {
		this.descripcionEquipoEspecial = descripcionEquipoEspecial;
	}

	@Transient
	public String getDescripcionEquipoEspecial() {
		return descripcionEquipoEspecial;
	}

	public void setDescripcionAdaptacionConversion(
			String descripcionAdaptacionConversion) {
		this.descripcionAdaptacionConversion = descripcionAdaptacionConversion;
	}

	@Transient
	public String getDescripcionAdaptacionConversion() {
		return descripcionAdaptacionConversion;
	}

	public void setOcupacion(String ocupacion) {
		this.ocupacion = ocupacion;
	}

	@Transient
	public String getOcupacion() {
		return ocupacion;
	}

	public void setIgualacion(Double igualacion) {
		this.igualacion = igualacion;
	}

	@Transient
	public Double getIgualacion() {
		return igualacion;
	}
	
	@Transient
	public boolean isDanosCargaContratada() {
		if (!StringUtils.isBlank(getTipoCargaDescripcion())) {
			return true;
		}
		return false;
	}

	@Transient
	public Integer getNumeroRemolques() {
		return numeroRemolques;
	}

	public void setNumeroRemolques(Integer numeroRemolques) {
		this.numeroRemolques = numeroRemolques;
	}

	@Transient
	public String getTipoCargaDescripcion() {
		return tipoCargaDescripcion;
	}

	public void setTipoCargaDescripcion(String tipoCargaDescripcion) {
		this.tipoCargaDescripcion = tipoCargaDescripcion;
	}
	
	@Transient
	public CatalogoValorFijoDTO getTipoCarga() {
		return tipoCarga;
	}
	
	public void setTipoCarga(CatalogoValorFijoDTO tipoCarga) {
		this.tipoCarga = tipoCarga;
	}
	
	@Transient
	public String getObservacionesInciso() {
		return observacionesInciso;
	}
	
	public void setObservacionesInciso(String observacionesInciso) {		
		this.observacionesInciso = observacionesInciso;
		if (observacionesInciso != null) {
			this.observacionesInciso =  observacionesInciso.trim();
			final int maxLength = 500;
			if (this.observacionesInciso.length() > maxLength) {
				this.observacionesInciso = this.observacionesInciso.substring(0, maxLength);
			}
		}
	}

	public void setLimiteMuerte(Double limiteMuerte) {
		this.limiteMuerte = limiteMuerte;
	}

	@Transient
	public Double getLimiteMuerte() {
		return limiteMuerte;
	}

	public void setValorSumaAseguradaDM(Double valorSumaAseguradaDM) {
		this.valorSumaAseguradaDM = valorSumaAseguradaDM;
	}

	@Transient
	public Double getValorSumaAseguradaDM() {
		return valorSumaAseguradaDM;
	}

	public void setValorSumaAseguradaRT(Double valorSumaAseguradaRT) {
		this.valorSumaAseguradaRT = valorSumaAseguradaRT;
	}

	@Transient
	public Double getValorSumaAseguradaRT() {
		return valorSumaAseguradaRT;
	}

	public void setEmailContactos(String emailContactos) {
		this.emailContactos = emailContactos;
	}

	@Transient
	public String getEmailContactos() {
		return emailContactos;
	}

	@Transient
	public List<CondicionEspecial> getAltaCondicionesEspeciales() {
		return altaCondicionesEspeciales;
	}
	
	public void setAltaCondicionesEspeciales(List<CondicionEspecial> altaCondicionesEspeciales) {
		this.altaCondicionesEspeciales = altaCondicionesEspeciales;
	}
	
	@Transient
	public List<CondicionEspecial> getBajaCondicionesEspeciales() {
		return bajaCondicionesEspeciales;
	}

	public void setBajaCondicionesEspeciales(List<CondicionEspecial> bajaCondicionesEspeciales) {
		this.bajaCondicionesEspeciales = bajaCondicionesEspeciales;
	}

	@Transient
	public IncisoCotizacionDTO getIncisoCotizacion() {
		return incisoCotizacion;
	}

	public void setIncisoCotizacion(IncisoCotizacionDTO incisoCotizacion) {
		this.incisoCotizacion = incisoCotizacion;
	}

	public void setDiasSalarioMinimo(Double diasSalarioMinimo) {
		this.diasSalarioMinimo = diasSalarioMinimo;
	}

	@Transient
	public Double getDiasSalarioMinimo() {
		return diasSalarioMinimo;
	}

	@Transient
	public String getIdMedioPago() {
		return idMedioPago;
	}

	public void setIdMedioPago(String idMedioPago) {
		this.idMedioPago = idMedioPago;
	}

	@Transient
	public String getConductoCobro() {
		return conductoCobro;
	}

	
	public void setConductoCobro(String conductoCobro) {
		this.conductoCobro = conductoCobro;
	}

	
	@Transient
	public String getInstitucionBancaria() {
		return institucionBancaria;
	}

	
	public void setInstitucionBancaria(String institucionBancaria) {
		this.institucionBancaria = institucionBancaria;
	}

	
	@Transient
	public String getTipoTarjeta() {
		return tipoTarjeta;
	}

	
	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	
	@Transient
	public String getNumeroTarjetaClave() {
		return numeroTarjetaClave;
	}

	
	public void setNumeroTarjetaClave(String numeroTarjetaClave) {
		this.numeroTarjetaClave = numeroTarjetaClave;
	}

	
	@Transient
	public String getCodigoSeguridad() {
		return codigoSeguridad;
	}

	
	public void setCodigoSeguridad(String codigoSeguridad) {
		this.codigoSeguridad = codigoSeguridad;
	}

	
	@Transient
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	
	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
}
