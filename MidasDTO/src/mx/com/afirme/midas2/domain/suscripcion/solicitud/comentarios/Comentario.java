package mx.com.afirme.midas2.domain.suscripcion.solicitud.comentarios;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

/**
 * Comentario entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOCOMENTARIOSOL", schema = "MIDAS")
public class Comentario implements Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8876336558757459278L;
	// Fields

	private Long id;
	private String valor;
	private SolicitudDTO solicitudDTO;
	private DocumentoDigitalSolicitudDTO documentoDigitalSolicitudDTO;
	private String codigoUsuarioCreacion;
	private Date fechaCreacion;
	private String nombreArchivoOriginal;
	private String extension;
	private Long idComentario;

	// Constructors

	/** default constructor */
	public Comentario() {
	}

	/** minimal constructor */
	public Comentario(Long id) {
		this.id = id;
	}

	/** full constructor */
	public Comentario(Long id, String valor, SolicitudDTO solicitudDTO, DocumentoDigitalSolicitudDTO documentoDigitalSolicitudDTO, String codigoUsuarioCreacion, Date fechaCreacion) {
		this.id = id;
		this.valor = valor;
		this.solicitudDTO = solicitudDTO;
		this.documentoDigitalSolicitudDTO = documentoDigitalSolicitudDTO;
		this.codigoUsuarioCreacion=codigoUsuarioCreacion;
		this.fechaCreacion=fechaCreacion;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDCOMENTARIOSOL_SEQ")
	@SequenceGenerator(name="IDCOMENTARIOSOL_SEQ", sequenceName="MIDAS.IDCOMENTARIOSOL_SEQ", allocationSize=1)
	@Column(name = "id", nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "codigoUsuarioCreacion", length = 8)
	@Size(min=1, max=8, groups=EditItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getCodigoUsuarioCreacion() {
		return this.codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "SOLICITUD_ID", nullable = false)
	public SolicitudDTO getSolicitudDTO() {
		return solicitudDTO;
	}

	public void setSolicitudDTO(SolicitudDTO solicitudDTO) {
		this.solicitudDTO = solicitudDTO;
	}

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "DOCDIGITALSOL_ID", referencedColumnName="IDTODOCDIGITALSOL", nullable = false)
	public DocumentoDigitalSolicitudDTO getDocumentoDigitalSolicitudDTO() {
		return documentoDigitalSolicitudDTO;
	}

	public void setDocumentoDigitalSolicitudDTO(
			DocumentoDigitalSolicitudDTO documentoDigitalSolicitudDTO) {
		this.documentoDigitalSolicitudDTO = documentoDigitalSolicitudDTO;
	}
	
	@Column(name = "valor", length = 500)
	@Size(min=1, max=500, groups=EditItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getValor() {
		return this.valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHACREACION", nullable = false)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}
	
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Transient
	public String getNombreArchivoOriginal() {
		return nombreArchivoOriginal;
	}

	public void setNombreArchivoOriginal(String nombreArchivoOriginal) {
		this.nombreArchivoOriginal = nombreArchivoOriginal;
	}

    @Transient	
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}

@Transient	
	public Long getIdComentario() {
		return idComentario;
	}

	public void setIdComentario(Long idComentario) {
		this.idComentario = idComentario;
	}

	@Override
	public <K> K getKey() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	@Override
	public String getValue() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}


}