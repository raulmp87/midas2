package mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasiva;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * TOCargaMasivaAutoCot entity. @author Lizandro Perez
 */
@Entity
@Table(name = "TOCARGAMASIVAAUTOCOT", schema = "MIDAS")
public class CargaMasivaAutoCot implements Serializable, Entidad {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal idToCargaMasivaAutoCot;
	private BigDecimal idToCotizacion;
	private BigDecimal idToControlArchivo;
	private ControlArchivoDTO controlArchivo;
	private Short claveEstatus;
	private Date fechaCreacion;
	private String codigoUsuarioCreacion;
	private List<DetalleCargaMasivaAutoCot> incisos;
	
	public static final Short ESTATUS_CON_ERROR = 0;
	public static final Short ESTATUS_TERMINADO = 1;
	public static final Short ESTATUS_PENDIENTE = 2;
	public static final Short ESTATUS_EN_PROCESO = 3;
	
	public CargaMasivaAutoCot(){
		
	}
	
	public String getDescripcionEstatus(){
		String descripcionEstatus = "";
		if(claveEstatus.equals(ESTATUS_CON_ERROR)){
			descripcionEstatus = "CON ERROR";
		}else if(claveEstatus.equals(ESTATUS_TERMINADO)){
			descripcionEstatus = "TERMINADO";
		}else if(claveEstatus.equals(ESTATUS_PENDIENTE)){
			descripcionEstatus = "PENDIENTE";
		}else if(claveEstatus.equals(ESTATUS_EN_PROCESO)){
			descripcionEstatus = "EN PROCESO";
		}else{
			descripcionEstatus = "ERROR";
		}
		return descripcionEstatus;
	}
	
	public void setIdToCargaMasivaAutoCot(BigDecimal idToCargaMasivaAutoCot) {
		this.idToCargaMasivaAutoCot = idToCargaMasivaAutoCot;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTOCARGAMASIVAAUTOCOT_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOCARGAMASIVAAUTOCOT_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOCARGAMASIVAAUTOCOT_SEQ_GENERADOR")	  	
	@Column(name = "IDTOCARGAMASIVAAUTOCOT", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCargaMasivaAutoCot() {
		return idToCargaMasivaAutoCot;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	@Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
		this.idToControlArchivo = idToControlArchivo;
	}

	@Column(name = "IDTOCONTROLARCHIVO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToControlArchivo() {
		return idToControlArchivo;
	}

	public void setControlArchivo(ControlArchivoDTO controlArchivo) {
		this.controlArchivo = controlArchivo;
	}

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOCONTROLARCHIVO", nullable = false, insertable = false, updatable = false)
	public ControlArchivoDTO getControlArchivo() {
		return controlArchivo;
	}

	public void setClaveEstatus(Short claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	@Column(name = "CLAVEESTATUS", nullable = false, precision = 4, scale = 0)
	public Short getClaveEstatus() {
		return claveEstatus;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHACREACION", nullable = false, length = 7)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	@Column(name = "CODIGOUSUARIOCREACION", nullable = false, length = 8)
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.idToCargaMasivaAutoCot;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setIncisos(List<DetalleCargaMasivaAutoCot> incisos) {
		this.incisos = incisos;
	}

	@Transient
	public List<DetalleCargaMasivaAutoCot> getIncisos() {
		return incisos;
	}

}
