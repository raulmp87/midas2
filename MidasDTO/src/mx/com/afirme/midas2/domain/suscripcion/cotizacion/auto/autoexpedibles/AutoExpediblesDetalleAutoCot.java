package mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.autoexpedibles;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
/**
 * TOAUTOEXPEDIBLESAUTODETALLECOT entity. @author Lizandro Perez
 */
@Entity
@Table(name = "TOAUTOEXPEDIBLESAUTODETALLECOT", schema = "MIDAS")
public class AutoExpediblesDetalleAutoCot implements Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal idToDetalleAutoExpediblesAutoCot;
	private BigDecimal idToAutoExpediblesAutoCot;
	private BigDecimal idToCotizacion;
	private BigDecimal idToPoliza;
	private Date fechaVigencia;
	private String nombreCliente;
	private Short claveEstatus;	
	private Short claveTipo;
	private BigDecimal idPolizaAutoExpedible;
	
	//Datos n Proceso
	private String idAgente;
	private String moneda;
	private String idCentroEmisor;
	private String idOficina;
	private BigDecimal numeroCotizacion;
	private String numeroPoliza;
	private BigDecimal numeroLiquidacion;
	private String autorizacionProsa;
	private Date fechaVigenciaInicio;
	private Date fechaEmision;
	private Date fechaVigenciaFin;
	private String lineaNegocio;
	private String paquete;
	private BigDecimal numeroEmpleado;
	private Short tipoPersonaCliente;
	private String rfcCliente;
	private Short clienteVIP;
	private String nombreORazonSocial;
	private String apellidoPaternoCliente;
	private String apellidoMaternoCliente;
	private String codigoPostalCliente;
	private String coloniaCliente;
	private String telefonoCliente;
	private String claveAMIS;
	private Short modelo;
	private String nciRepuve;
	private String claveUso;
	private String placas;
	private String formaPago;
	private String numeroMotor;
	private String numeroSerie;
	private BigDecimal valorComercial;
	private Short numeroPasajeros;
	private String descripcionVehiculo;
	private Double primaTotal;
	private Double deducibleDanosMateriales;
	private Double deducibleRoboTotal;
	private Double limiteRcTerceros;
	private Double deducibleRcTerceros;
	private Double limiteAdaptacionConversion;
	private Double limiteGastosMedicos;
	private Double deducibleEquipoEspecial;
	private Double limiteMuerte;
	private Double limiteRcViajero;
	private String asistenciaJuridica;
	private Double limiteEquipoEspecial;
	private String igualacion;
	private Double derechos;
	private String solicitarAutorizacion;
	private String causaAutorizacion;
	private Long idSolicitudAutorizacion;
	private String descripcionEquipoEspecial;
	private String descripcionAdaptacionConversion;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String numeroLicencia;
	private Date fechaNacimiento;
	private String ocupacion;
	private BigDecimal idCliente;
	
	//Terminado

	
	private String mensajeError;
	
	public static final Short ESTATUS_ERROR = 0;
	public static final Short ESTATUS_TERMINADO = 1;
	public static final Short ESTATUS_PENDIENTE = 2;
	public static final Short ESTATUS_AUTORIZACION = 3;
	public static final Short ESTATUS_EMITIDO = 4;

	public String getDescripcionEstatus(){
		String descripcionEstatus = "";
		if(claveEstatus.equals(ESTATUS_ERROR)){
			descripcionEstatus = "ERROR";
		}else if(claveEstatus.equals(ESTATUS_TERMINADO)){
			descripcionEstatus = "TERMINADO";
		}else if(claveEstatus.equals(ESTATUS_PENDIENTE)){
			descripcionEstatus = "PENDIENTE";
		}else if(claveEstatus.equals(ESTATUS_AUTORIZACION)){
			descripcionEstatus = "SOLICITO AUTORIZACION";
		}else if(claveEstatus.equals(ESTATUS_EMITIDO)){
			descripcionEstatus = "POLIZA EMITIDA";
		}
		return descripcionEstatus;
	}
	
	public String getNombreCompleto(){
		String nombreCompleto = "";
		if(this.getNombreORazonSocial() != null){
			nombreCompleto += this.getNombreORazonSocial() + " ";
		}
		if(this.getApellidoPaternoCliente() != null){
			nombreCompleto += this.getApellidoPaternoCliente() + " ";
		}
		if(this.getApellidoMaternoCliente() != null){
			nombreCompleto += this.getApellidoMaternoCliente() + " ";
		}
		return nombreCompleto.toUpperCase().trim();
	}

	public void setIdToDetalleAutoExpediblesAutoCot(
			BigDecimal idToDetalleAutoExpediblesAutoCot) {
		this.idToDetalleAutoExpediblesAutoCot = idToDetalleAutoExpediblesAutoCot;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTODETAUTOEXPAUTOCOT_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTODETAUTOEXPAUTOCOT_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTODETAUTOEXPAUTOCOT_SEQ_GENERADOR")	  	
	@Column(name = "IDTODETAUTOEXPEDIBLESAUTOCOT", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToDetalleAutoExpediblesAutoCot() {
		return idToDetalleAutoExpediblesAutoCot;
	}

	public void setIdToAutoExpediblesAutoCot(BigDecimal idToAutoExpediblesAutoCot) {
		this.idToAutoExpediblesAutoCot = idToAutoExpediblesAutoCot;
	}

	@Column(name = "IDTOAUTOEXPEDIBLESAUTOCOT", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToAutoExpediblesAutoCot() {
		return idToAutoExpediblesAutoCot;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	@Column(name = "IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	@Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}

	public void setFechaVigencia(Date fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}

	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAVIGENCIA", nullable = false, length = 7)
	public Date getFechaVigencia() {
		return fechaVigencia;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	@Column(name = "NOMBRECLIENTE", nullable = false, length = 200)
	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setClaveEstatus(Short claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	@Column(name = "CLAVEESTATUS", nullable = false, precision = 4, scale = 0)
	public Short getClaveEstatus() {
		return claveEstatus;
	}
	
	public void setClaveTipo(Short claveTipo) {
		this.claveTipo = claveTipo;
	}

	@Column(name = "CLAVETIPO", nullable = false, precision = 4, scale = 0)
	public Short getClaveTipo() {
		return claveTipo;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	@Column(name = "MENSAJEERROR", nullable = false, length = 500)
	public String getMensajeError() {
		return mensajeError;
	}
	
	public void setIdPolizaAutoExpedible(BigDecimal idPolizaAutoExpedible) {
		this.idPolizaAutoExpedible = idPolizaAutoExpedible;
	}

	@Column(name = "IDPOLIZAAUTOEXPEDIBLE", nullable = false, length = 8)
	public BigDecimal getIdPolizaAutoExpedible() {
		return idPolizaAutoExpedible;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.idToDetalleAutoExpediblesAutoCot;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setIdAgente(String idAgente) {
		this.idAgente = idAgente;
	}

	@Transient
	public String getIdAgente() {
		return idAgente;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	@Transient
	public String getMoneda() {
		return moneda;
	}

	public void setIdCentroEmisor(String idCentroEmisor) {
		this.idCentroEmisor = idCentroEmisor;
	}

	@Transient
	public String getIdCentroEmisor() {
		return idCentroEmisor;
	}

	public void setIdOficina(String idOficina) {
		this.idOficina = idOficina;
	}

	@Transient
	public String getIdOficina() {
		return idOficina;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	@Transient
	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroLiquidacion(BigDecimal numeroLiquidacion) {
		this.numeroLiquidacion = numeroLiquidacion;
	}

	@Transient
	public BigDecimal getNumeroLiquidacion() {
		return numeroLiquidacion;
	}

	public void setAutorizacionProsa(String autorizacionProsa) {
		this.autorizacionProsa = autorizacionProsa;
	}

	@Transient
	public String getAutorizacionProsa() {
		return autorizacionProsa;
	}

	public void setFechaVigenciaInicio(Date fechaVigenciaInicio) {
		this.fechaVigenciaInicio = fechaVigenciaInicio;
	}

	@Transient
	public Date getFechaVigenciaInicio() {
		return fechaVigenciaInicio;
	}

	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	@Transient
	public Date getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaVigenciaFin(Date fechaVigenciaFin) {
		this.fechaVigenciaFin = fechaVigenciaFin;
	}

	@Transient
	public Date getFechaVigenciaFin() {
		return fechaVigenciaFin;
	}

	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}

	@Transient
	public String getLineaNegocio() {
		return lineaNegocio;
	}

	public void setPaquete(String paquete) {
		this.paquete = paquete;
	}

	@Transient
	public String getPaquete() {
		return paquete;
	}

	public void setTipoPersonaCliente(Short tipoPersonaCliente) {
		this.tipoPersonaCliente = tipoPersonaCliente;
	}

	@Transient
	public Short getTipoPersonaCliente() {
		return tipoPersonaCliente;
	}

	public void setNumeroEmpleado(BigDecimal numeroEmpleado) {
		this.numeroEmpleado = numeroEmpleado;
	}

	@Transient
	public BigDecimal getNumeroEmpleado() {
		return numeroEmpleado;
	}

	public void setRfcCliente(String rfcCliente) {
		this.rfcCliente = rfcCliente;
	}

	@Transient
	public String getRfcCliente() {
		return rfcCliente;
	}

	public void setClienteVIP(Short clienteVIP) {
		this.clienteVIP = clienteVIP;
	}

	@Transient
	public Short getClienteVIP() {
		return clienteVIP;
	}

	public void setNombreORazonSocial(String nombreORazonSocial) {
		this.nombreORazonSocial = nombreORazonSocial;
	}

	@Transient
	public String getNombreORazonSocial() {
		return nombreORazonSocial;
	}

	public void setApellidoPaternoCliente(String apellidoPaternoCliente) {
		this.apellidoPaternoCliente = apellidoPaternoCliente;
	}

	@Transient
	public String getApellidoPaternoCliente() {
		return apellidoPaternoCliente;
	}

	public void setApellidoMaternoCliente(String apellidoMaternoCliente) {
		this.apellidoMaternoCliente = apellidoMaternoCliente;
	}

	@Transient
	public String getApellidoMaternoCliente() {
		return apellidoMaternoCliente;
	}

	public void setCodigoPostalCliente(String codigoPostalCliente) {
		this.codigoPostalCliente = codigoPostalCliente;
	}

	@Transient
	public String getCodigoPostalCliente() {
		return codigoPostalCliente;
	}

	public void setColoniaCliente(String coloniaCliente) {
		this.coloniaCliente = coloniaCliente;
	}

	@Transient
	public String getColoniaCliente() {
		return coloniaCliente;
	}

	public void setTelefonoCliente(String telefonoCliente) {
		this.telefonoCliente = telefonoCliente;
	}

	@Transient
	public String getTelefonoCliente() {
		return telefonoCliente;
	}

	public void setClaveAMIS(String claveAMIS) {
		this.claveAMIS = claveAMIS;
	}

	@Transient
	public String getClaveAMIS() {
		return claveAMIS;
	}

	public void setModelo(Short modelo) {
		this.modelo = modelo;
	}

	@Transient
	public Short getModelo() {
		return modelo;
	}

	public void setNciRepuve(String nciRepuve) {
		this.nciRepuve = nciRepuve;
	}

	@Transient
	public String getNciRepuve() {
		return nciRepuve;
	}

	public void setClaveUso(String claveUso) {
		this.claveUso = claveUso;
	}

	@Transient
	public String getClaveUso() {
		return claveUso;
	}

	public void setPlacas(String placas) {
		this.placas = placas;
	}

	@Transient
	public String getPlacas() {
		return placas;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}

	@Transient
	public String getFormaPago() {
		return formaPago;
	}

	public void setNumeroMotor(String numeroMotor) {
		this.numeroMotor = numeroMotor;
	}

	@Transient
	public String getNumeroMotor() {
		return numeroMotor;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	@Transient
	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setValorComercial(BigDecimal valorComercial) {
		this.valorComercial = valorComercial;
	}

	@Transient
	public BigDecimal getValorComercial() {
		return valorComercial;
	}

	public void setNumeroPasajeros(Short numeroPasajeros) {
		this.numeroPasajeros = numeroPasajeros;
	}

	@Transient
	public Short getNumeroPasajeros() {
		return numeroPasajeros;
	}

	public void setDescripcionVehiculo(String descripcionVehiculo) {
		this.descripcionVehiculo = descripcionVehiculo;
	}

	@Transient
	public String getDescripcionVehiculo() {
		return descripcionVehiculo;
	}

	public void setPrimaTotal(Double primaTotal) {
		this.primaTotal = primaTotal;
	}

	@Transient
	public Double getPrimaTotal() {
		return primaTotal;
	}

	public void setDeducibleDanosMateriales(Double deducibleDanosMateriales) {
		this.deducibleDanosMateriales = deducibleDanosMateriales;
	}

	@Transient
	public Double getDeducibleDanosMateriales() {
		return deducibleDanosMateriales;
	}

	public void setDeducibleRoboTotal(Double deducibleRoboTotal) {
		this.deducibleRoboTotal = deducibleRoboTotal;
	}

	@Transient
	public Double getDeducibleRoboTotal() {
		return deducibleRoboTotal;
	}

	public void setLimiteRcTerceros(Double limiteRcTerceros) {
		this.limiteRcTerceros = limiteRcTerceros;
	}

	@Transient
	public Double getLimiteRcTerceros() {
		return limiteRcTerceros;
	}

	public void setDeducibleRcTerceros(Double deducibleRcTerceros) {
		this.deducibleRcTerceros = deducibleRcTerceros;
	}

	@Transient
	public Double getDeducibleRcTerceros() {
		return deducibleRcTerceros;
	}

	public void setLimiteGastosMedicos(Double limiteGastosMedicos) {
		this.limiteGastosMedicos = limiteGastosMedicos;
	}

	@Transient
	public Double getLimiteGastosMedicos() {
		return limiteGastosMedicos;
	}

	public void setLimiteMuerte(Double limiteMuerte) {
		this.limiteMuerte = limiteMuerte;
	}

	@Transient
	public Double getLimiteMuerte() {
		return limiteMuerte;
	}

	public void setLimiteRcViajero(Double limiteRcViajero) {
		this.limiteRcViajero = limiteRcViajero;
	}

	@Transient
	public Double getLimiteRcViajero() {
		return limiteRcViajero;
	}

	public void setAsistenciaJuridica(String asistenciaJuridica) {
		this.asistenciaJuridica = asistenciaJuridica;
	}

	@Transient
	public String getAsistenciaJuridica() {
		return asistenciaJuridica;
	}

	public void setLimiteEquipoEspecial(Double limiteEquipoEspecial) {
		this.limiteEquipoEspecial = limiteEquipoEspecial;
	}

	@Transient
	public Double getLimiteEquipoEspecial() {
		return limiteEquipoEspecial;
	}

	public void setIgualacion(String igualacion) {
		this.igualacion = igualacion;
	}

	@Transient
	public String getIgualacion() {
		return igualacion;
	}

	public void setDerechos(Double derechos) {
		this.derechos = derechos;
	}

	@Transient
	public Double getDerechos() {
		return derechos;
	}

	public void setSolicitarAutorizacion(String solicitarAutorizacion) {
		this.solicitarAutorizacion = solicitarAutorizacion;
	}

	@Transient
	public String getSolicitarAutorizacion() {
		return solicitarAutorizacion;
	}

	public void setCausaAutorizacion(String causaAutorizacion) {
		this.causaAutorizacion = causaAutorizacion;
	}

	@Transient
	public String getCausaAutorizacion() {
		return causaAutorizacion;
	}

	public void setIdSolicitudAutorizacion(Long idSolicitudAutorizacion) {
		this.idSolicitudAutorizacion = idSolicitudAutorizacion;
	}

	@Transient
	public Long getIdSolicitudAutorizacion() {
		return idSolicitudAutorizacion;
	}

	public void setNumeroCotizacion(BigDecimal numeroCotizacion) {
		this.numeroCotizacion = numeroCotizacion;
	}

	@Transient
	public BigDecimal getNumeroCotizacion() {
		return numeroCotizacion;
	}

	public void setDescripcionEquipoEspecial(String descripcionEquipoEspecial) {
		this.descripcionEquipoEspecial = descripcionEquipoEspecial;
	}

	@Transient
	public String getDescripcionEquipoEspecial() {
		return descripcionEquipoEspecial;
	}

	public void setDescripcionAdaptacionConversion(
			String descripcionAdaptacionConversion) {
		this.descripcionAdaptacionConversion = descripcionAdaptacionConversion;
	}

	@Transient
	public String getDescripcionAdaptacionConversion() {
		return descripcionAdaptacionConversion;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Transient
	public String getNombre() {
		return nombre;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	@Transient
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	@Transient
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setNumeroLicencia(String numeroLicencia) {
		this.numeroLicencia = numeroLicencia;
	}

	@Transient
	public String getNumeroLicencia() {
		return numeroLicencia;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	@Transient
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setOcupacion(String ocupacion) {
		this.ocupacion = ocupacion;
	}

	@Transient
	public String getOcupacion() {
		return ocupacion;
	}

	public void setLimiteAdaptacionConversion(Double limiteAdaptacionConversion) {
		this.limiteAdaptacionConversion = limiteAdaptacionConversion;
	}

	@Transient
	public Double getLimiteAdaptacionConversion() {
		return limiteAdaptacionConversion;
	}

	public void setDeducibleEquipoEspecial(Double deducibleEquipoEspecial) {
		this.deducibleEquipoEspecial = deducibleEquipoEspecial;
	}

	@Transient
	public Double getDeducibleEquipoEspecial() {
		return deducibleEquipoEspecial;
	}

	public void setIdCliente(BigDecimal idCliente) {
		this.idCliente = idCliente;
	}

	@Transient
	public BigDecimal getIdCliente() {
		return idCliente;
	}

}
