package mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasivaindividual;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * TOCARGAMASIVAINDAUTOCOT entity. @author Lizandro Perez
 */
@Entity
@Table(name = "TOCARGAMASIVAINDAUTOCOT", schema = "MIDAS")
public class CargaMasivaIndividualAutoCot implements Serializable, Entidad {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal idToCargaMasivaIndAutoCot;
	private BigDecimal idToControlArchivo;
	private BigDecimal idNegocio;
	private BigDecimal idToNegProducto;
	private BigDecimal idToNegTipoPoliza;
	private ControlArchivoDTO controlArchivo;
	private Short claveEstatus;
	private Date fechaCreacion;
	private String codigoUsuarioCreacion;
	private Short claveTipo;
	
	public static final Short ESTATUS_CON_ERROR = 0;
	public static final Short ESTATUS_TERMINADO = 1;
	public static final Short ESTATUS_PENDIENTE = 2;
	public static final Short ESTATUS_EN_PROCESO = 3;
	
	public static final Short TIPO_COTIZACION = 0;
	public static final Short TIPO_EMISION = 1;
	
	public CargaMasivaIndividualAutoCot(){
		
	}
	
	public String getDescripcionEstatus(){
		String descripcionEstatus = "";
		if(claveEstatus.equals(ESTATUS_CON_ERROR)){
			descripcionEstatus = "CON ERROR";
		}else if(claveEstatus.equals(ESTATUS_TERMINADO)){
			descripcionEstatus = "TERMINADO";
		}else if(claveEstatus.equals(ESTATUS_PENDIENTE)){
			descripcionEstatus = "PENDIENTE";
		}else if(claveEstatus.equals(ESTATUS_EN_PROCESO)){
			descripcionEstatus = "EN PROCESO";
		}
		return descripcionEstatus;
	}
	
	public void setIdToCargaMasivaIndAutoCot(BigDecimal idToCargaMasivaIndAutoCot) {
		this.idToCargaMasivaIndAutoCot = idToCargaMasivaIndAutoCot;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTOCARGAMASIVAINDAUTOCOT_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOCARGAMASIVAINDAUTOCOT_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOCARGAMASIVAINDAUTOCOT_SEQ_GENERADOR")	  	
	@Column(name = "IDTOCARGAMASIVAINDAUTOCOT", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCargaMasivaIndAutoCot() {
		return idToCargaMasivaIndAutoCot;
	}

	public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
		this.idToControlArchivo = idToControlArchivo;
	}

	@Column(name = "IDTOCONTROLARCHIVO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToControlArchivo() {
		return idToControlArchivo;
	}

	public void setControlArchivo(ControlArchivoDTO controlArchivo) {
		this.controlArchivo = controlArchivo;
	}

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOCONTROLARCHIVO", nullable = false, insertable = false, updatable = false)
	public ControlArchivoDTO getControlArchivo() {
		return controlArchivo;
	}

	public void setClaveEstatus(Short claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	@Column(name = "CLAVEESTATUS", nullable = false, precision = 4, scale = 0)
	public Short getClaveEstatus() {
		return claveEstatus;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHACREACION", nullable = false, length = 7)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	@Column(name = "CODIGOUSUARIOCREACION", nullable = false, length = 8)
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}
	
	public void setClaveTipo(Short claveTipo) {
		this.claveTipo = claveTipo;
	}

	@Column(name = "CLAVETIPO", nullable = false, precision = 4, scale = 0)
	public Short getClaveTipo() {
		return claveTipo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.idToCargaMasivaIndAutoCot;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}

	public void setIdNegocio(BigDecimal idNegocio) {
		this.idNegocio = idNegocio;
	}

	@Column(name = "IDNEGOCIO", nullable = false, length = 8)
	public BigDecimal getIdNegocio() {
		return idNegocio;
	}

	public void setIdToNegProducto(BigDecimal idToNegProducto) {
		this.idToNegProducto = idToNegProducto;
	}

	@Column(name = "IDTONEGPRODUCTO", nullable = false, length = 8)
	public BigDecimal getIdToNegProducto() {
		return idToNegProducto;
	}

	public void setIdToNegTipoPoliza(BigDecimal idToNegTipoPoliza) {
		this.idToNegTipoPoliza = idToNegTipoPoliza;
	}

	@Column(name = "IDTONEGTIPOPOLIZA", nullable = false, length = 8)
	public BigDecimal getIdToNegTipoPoliza() {
		return idToNegTipoPoliza;
	}





}
