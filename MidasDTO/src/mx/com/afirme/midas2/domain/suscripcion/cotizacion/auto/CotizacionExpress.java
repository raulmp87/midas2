package mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioDTO;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;

public class CotizacionExpress implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	
	private ModeloVehiculoDTO modeloVehiculo;
	private EstadoDTO estado;
	private MunicipioDTO municipio;
	private String variableModificadoraDescripcion;
	private Agente agente;
	private FormaPagoDTO formaPago;
	private Paquete paquete;
	private NegocioSeccion negocioSeccion;
	private NegocioPaqueteSeccion negocioPaqueteSeccion;
	private BigDecimal iva;
	private BigDecimal totalPrimas = BigDecimal.ZERO;
	private BigDecimal descuentoGlobal = BigDecimal.ZERO;
	private BigDecimal descuentoComision = BigDecimal.ZERO;
	private BigDecimal primaNeta = BigDecimal.ZERO;
	private BigDecimal primaNetaB = BigDecimal.ZERO;
	private BigDecimal financiamiento = BigDecimal.ZERO;
	private BigDecimal gastosExpedicion = BigDecimal.ZERO;
	private BigDecimal totalIva = BigDecimal.ZERO;
	private BigDecimal primaTotal = BigDecimal.ZERO;
	private List<CoberturaCotizacionExpress> coberturaCotizacionExpresses = new ArrayList<CoberturaCotizacionExpress>();
	private Short idMoneda;
	private Long paso;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public ModeloVehiculoDTO getModeloVehiculo() {
		return modeloVehiculo;
	}

	public void setModeloVehiculo(ModeloVehiculoDTO modeloVehiculo) {
		this.modeloVehiculo = modeloVehiculo;
	}

	public EstadoDTO getEstado() {
		return estado;
	}

	public void setEstado(EstadoDTO estado) {
		this.estado = estado;
	}

	public MunicipioDTO getMunicipio() {
		return municipio;
	}

	public void setMunicipio(MunicipioDTO municipio) {
		this.municipio = municipio;
	}
	
	public String getVariableModificadoraDescripcion() {
		return variableModificadoraDescripcion;
	}

	public void setVariableModificadoraDescripcion(
			String variableModificadoraDescripcion) {
		this.variableModificadoraDescripcion = variableModificadoraDescripcion;
	}

	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	public FormaPagoDTO getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(FormaPagoDTO formaPago) {
		this.formaPago = formaPago;
	}

	public Paquete getPaquete() {
		return paquete;
	}

	public void setPaquete(Paquete paquete) {
		this.paquete = paquete;
	}

	public NegocioSeccion getNegocioSeccion() {
		return negocioSeccion;
	}

	public void setNegocioSeccion(NegocioSeccion negocioSeccion) {
		this.negocioSeccion = negocioSeccion;
	}
	
	public BigDecimal getIva() {
		return iva;
	}
	
	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public BigDecimal getTotalPrimas() {
		return totalPrimas;
	}

	public void setTotalPrimas(BigDecimal totalPrimas) {
		this.totalPrimas = totalPrimas;
	}

	public BigDecimal getDescuentoGlobal() {
		return descuentoGlobal;
	}

	public void setDescuentoGlobal(BigDecimal descuentoGlobal) {
		this.descuentoGlobal = descuentoGlobal;
	}

	public BigDecimal getDescuentoComision() {
		return descuentoComision;
	}

	public void setDescuentoComision(BigDecimal descuentoComision) {
		this.descuentoComision = descuentoComision;
	}

	public BigDecimal getPrimaNeta() {
		return primaNeta;
	}

	public void setPrimaNeta(BigDecimal primaNeta) {
		this.primaNeta = primaNeta;
	}

	public BigDecimal getPrimaNetaB() {
		return primaNetaB;
	}

	public void setPrimaNetaB(BigDecimal primaNetaB) {
		this.primaNetaB = primaNetaB;
	}

	public BigDecimal getFinanciamiento() {
		return financiamiento;
	}

	public void setFinanciamiento(BigDecimal financiamiento) {
		this.financiamiento = financiamiento;
	}

	public BigDecimal getGastosExpedicion() {
		return gastosExpedicion;
	}

	public void setGastosExpedicion(BigDecimal gastosExpedicion) {
		this.gastosExpedicion = gastosExpedicion;
	}

	public BigDecimal getTotalIva() {
		return totalIva;
	}

	public void setTotalIva(BigDecimal totalIva) {
		this.totalIva = totalIva;
	}

	public BigDecimal getPrimaTotal() {
		return primaTotal;
	}

	public void setPrimaTotal(BigDecimal primaTotal) {
		this.primaTotal = primaTotal;
	}

	public List<CoberturaCotizacionExpress> getCoberturaCotizacionExpresses() {
		return coberturaCotizacionExpresses;
	}
	
	public void setCoberturaCotizacionExpresses(
			List<CoberturaCotizacionExpress> coberturaCotizacionExpresses) {
		this.coberturaCotizacionExpresses = coberturaCotizacionExpresses;
	}
	
	public Short getIdMoneda() {
		return idMoneda;
	}
	
	public void setIdMoneda(Short idMoneda) {
		this.idMoneda = idMoneda;
	}
	
	public Long getPaso() {
		return paso;
	}
	
	public void setPaso(Long paso) {
		this.paso = paso;
	}

	public NegocioPaqueteSeccion getNegocioPaqueteSeccion() {
		return negocioPaqueteSeccion;
	}

	public void setNegocioPaqueteSeccion(NegocioPaqueteSeccion negocioPaqueteSeccion) {
		this.negocioPaqueteSeccion = negocioPaqueteSeccion;
	}
	
}
