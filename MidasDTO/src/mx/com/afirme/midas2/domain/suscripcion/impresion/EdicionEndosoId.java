package mx.com.afirme.midas2.domain.suscripcion.impresion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class EdicionEndosoId implements Serializable{

	private static final long serialVersionUID = -6560844286484525328L;

	@Column(name="IDTOCOTIZACION")
	private BigDecimal 	idToCotizacion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="RECORD_FROM")
	private Date	recordFrom;
	
	@Column(name="TIPO_ENDOSO")
	private Short 		claveTipoEndoso;
	
	@Column(name="VERSION")
	private Long version;
	
	public EdicionEndosoId() {
		super();
	}

	public EdicionEndosoId(BigDecimal idToCotizacion, Date recordFrom,
			Short claveTipoEndoso, Long version) {
		super();
		this.idToCotizacion = idToCotizacion;
		this.recordFrom = recordFrom;
		this.claveTipoEndoso = claveTipoEndoso;
		this.version = version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((claveTipoEndoso == null) ? 0 : claveTipoEndoso.hashCode());
		result = prime * result
				+ ((idToCotizacion == null) ? 0 : idToCotizacion.hashCode());
		result = prime * result
				+ ((recordFrom == null) ? 0 : recordFrom.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EdicionEndosoId other = (EdicionEndosoId) obj;
		if (claveTipoEndoso == null) {
			if (other.claveTipoEndoso != null)
				return false;
		} else if (!claveTipoEndoso.equals(other.claveTipoEndoso))
			return false;
		if (idToCotizacion == null) {
			if (other.idToCotizacion != null)
				return false;
		} else if (!idToCotizacion.equals(other.idToCotizacion))
			return false;
		if (recordFrom == null) {
			if (other.recordFrom != null)
				return false;
		} else if (!recordFrom.equals(other.recordFrom))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public Date getRecordFrom() {
		return recordFrom;
	}

	public void setRecordFrom(Date recordFrom) {
		this.recordFrom = recordFrom;
	}

	public Short getClaveTipoEndoso() {
		return claveTipoEndoso;
	}

	public void setClaveTipoEndoso(Short claveTipoEndoso) {
		this.claveTipoEndoso = claveTipoEndoso;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

}
