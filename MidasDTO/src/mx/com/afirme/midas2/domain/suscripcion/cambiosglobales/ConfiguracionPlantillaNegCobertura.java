package mx.com.afirme.midas2.domain.suscripcion.cambiosglobales;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;


/**
 * The persistent class for the TOCONFPLANTILLACOBERTURAS database table.
 * 
 */
@Entity
@Table(name="TOCONFPLANTILLACOBERTURAS", schema="MIDAS")
public class ConfiguracionPlantillaNegCobertura implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;
	private ConfiguracionPlantillaNegCoberturaPK id;
	private Boolean contratada;
	private Double deducible;
	private Double sumaAsegurada;
	private ConfiguracionPlantillaNeg configuracionPlantilla;

    public ConfiguracionPlantillaNegCobertura() {
    }


	@EmbeddedId
	public ConfiguracionPlantillaNegCoberturaPK getId() {
		return this.id;
	}

	public void setId(ConfiguracionPlantillaNegCoberturaPK id) {
		this.id = id;
	}
	

	public Boolean getContratada() {
		return this.contratada;
	}

	public void setContratada(Boolean contratada) {
		this.contratada = contratada;
	}


	public Double getDeducible() {
		return this.deducible;
	}

	public void setDeducible(Double deducible) {
		this.deducible = deducible;
	}


	@Column(name="SUMA_ASEGURADA")
	public Double getSumaAsegurada() {
		return this.sumaAsegurada;
	}

	public void setSumaAsegurada(Double sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}


	//bi-directional many-to-one association to ConfiguracionPlantillaNeg
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PLANTILLA_ID", nullable=false, insertable=false, updatable=false)
	public ConfiguracionPlantillaNeg getConfiguracionPlantilla() {
		return this.configuracionPlantilla;
	}

	public void setConfiguracionPlantilla(ConfiguracionPlantillaNeg configuracionPlantilla) {
		this.configuracionPlantilla = configuracionPlantilla;
	}


	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
}