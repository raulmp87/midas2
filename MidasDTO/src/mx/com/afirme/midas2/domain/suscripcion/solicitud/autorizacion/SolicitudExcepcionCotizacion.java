package mx.com.afirme.midas2.domain.suscripcion.solicitud.autorizacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.base.PaginadoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;


/**
 * The persistent class for the TOSOLICITUDCOTIZACION database table.
 * 
 */
@Entity
@Table(name="TOSOLICITUDCOTIZACION", schema="MIDAS")
public class SolicitudExcepcionCotizacion extends PaginadoDTO implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;
	private Long id;
	private String descripcion;
	private Short estatus;
	private Date fechaSolicitud;
	private BigDecimal toIdCotizacion;
	private Long usuarioAutorizador;
	private Long usuarioSolicitante;
	private List<SolicitudExcepcionDetalle> solicitudExcepcionDetalles = new ArrayList<SolicitudExcepcionDetalle>();
	private String cveNegocio;
	private String nombreAutorizador;
	private String nombreSolicitante;
	private Date fechaHasta;
    public SolicitudExcepcionCotizacion() {
    }


	@Id
	@SequenceGenerator(name="TOSOLICITUDCOTIZACION_ID_GENERATOR", sequenceName="MIDAS.IDTOSOLICITUDCOT_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOSOLICITUDCOTIZACION_ID_GENERATOR")
	@Column(unique=true, nullable=false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}


    @Lob()
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	@Column(nullable=false)
	public Short getEstatus() {
		return this.estatus;
	}

	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}


    @Temporal( TemporalType.DATE)
	@Column(name="FECHA_SOLICITUD", nullable=false)
	public Date getFechaSolicitud() {
		return this.fechaSolicitud;
	}

	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}


	@Column(nullable=false)
	public BigDecimal getToIdCotizacion() {
		return this.toIdCotizacion;
	}

	public void setToIdCotizacion(BigDecimal toIdCotizacion) {
		this.toIdCotizacion = toIdCotizacion;
	}


	@Column(name="USUARIO_AUTORIZADOR", nullable=false)
	public Long getUsuarioAutorizador() {
		return this.usuarioAutorizador;
	}

	public void setUsuarioAutorizador(Long usuarioAutorizador) {
		this.usuarioAutorizador = usuarioAutorizador;
	}


	@Column(name="USUARIO_SOLICITANTE", nullable=false)
	public Long getUsuarioSolicitante() {
		return this.usuarioSolicitante;
	}

	public void setUsuarioSolicitante(Long usuarioSolicitante) {
		this.usuarioSolicitante = usuarioSolicitante;
	}


	//bi-directional many-to-one association to Tosolicituddetalle
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="solicitudExcepcionCotizacion")
	public List<SolicitudExcepcionDetalle> getDetalle() {
		return this.solicitudExcepcionDetalles;
	}

	public void setDetalle(List<SolicitudExcepcionDetalle> solicitudExcepcionDetalles) {
		this.solicitudExcepcionDetalles = solicitudExcepcionDetalles;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		// TODO Auto-generated method stub
		return id;
	}


	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		// TODO Auto-generated method stub
		return id;
	}
	
	public void addDetalle(SolicitudExcepcionDetalle detalle){
		detalle.setSolicitudExcepcionCotizacion(this);
		this.solicitudExcepcionDetalles.add(detalle);
	}

	@Column(name="CVE_NEGOCIO", nullable=false)
	public String getCveNegocio() {
		return cveNegocio;
	}


	public void setCveNegocio(String cveNegocio) {
		this.cveNegocio = cveNegocio;
	}


	public void setNombreAutorizador(String nombreAutorizador) {
		this.nombreAutorizador = nombreAutorizador;
	}


	@Transient
	public String getNombreAutorizador() {
		return nombreAutorizador;
	}


	public void setNombreSolicitante(String nombreSolicitante) {
		this.nombreSolicitante = nombreSolicitante;
	}


	@Transient
	public String getNombreSolicitante() {
		return nombreSolicitante;
	}

	@Transient
	public Date getFechaHasta() {
		return fechaHasta;
	}


	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	
	
	
}