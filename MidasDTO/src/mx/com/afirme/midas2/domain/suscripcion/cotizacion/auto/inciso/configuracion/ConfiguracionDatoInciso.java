package mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the TCCONFIGDATOINCISOCOTAUTO database table.
 * 
 */
@Entity
@Table(name = "TCCONFIGDATOINCISOCOTAUTO", schema="MIDAS")
public class ConfiguracionDatoInciso implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4631113218542063076L;
	
	public static enum Estatus{
		EN_COTIZACION((short)0), HASTA_EMISION((short)1), TODOS((short)2);		
		Estatus(Short estatus) {
			this.estatus = estatus;
		}
		private Short estatus;
		public Short getEstatus(){
			return this.estatus;
		}
	};
	
	public enum DescripcionEtiqueta {
		NUMERO_REMOLQUES("Número de Remolques"),
		TIPO_CARGA("Tipo de Carga");
		
		private String descripcionEtiqueta;
		
		DescripcionEtiqueta(String descripcionEtiqueta) {
			this.descripcionEtiqueta = descripcionEtiqueta;
		}
		
		@Override
		public String toString() {
			return this.descripcionEtiqueta;
		}
	}

	@EmbeddedId
	private ConfiguracionDatoIncisoId id;

	private Short claveHastaEmision;

	private Short claveTipoControl;

	private Short claveTipoValidacion;

	private String descripcionClaseRemota;

	private String descripcionEtiqueta;

	private Short idGrupo;

	public ConfiguracionDatoInciso() {
	}

	@EmbeddedId
	@AttributeOverrides({
			@AttributeOverride(name = "idTcRamo", column = @Column(name = "IDTCRAMO")),
			@AttributeOverride(name = "idTcSubRamo", column = @Column(name = "IDTCSUBRAMO")),
			@AttributeOverride(name = "idToCobertura", column = @Column(name = "IDTOCOBERTURA")),
			@AttributeOverride(name = "claveDetalle", column = @Column(name = "CLAVEDETALLE")),
			@AttributeOverride(name = "idDato", column = @Column(name = "IDDATO")) })
	public ConfiguracionDatoIncisoId getId() {
		return this.id;
	}

	public void setId(ConfiguracionDatoIncisoId id) {
		this.id = id;
	}

	@Column(name = "CLAVEHASTAEMISION")
	public Short getClaveHastaEmision() {
		return this.claveHastaEmision;
	}

	public void setClaveHastaEmision(Short claveHastaEmision) {
		this.claveHastaEmision = claveHastaEmision;
	}

	@Column(name = "CLAVETIPOCONTROL")
	public Short getClaveTipoControl() {
		return this.claveTipoControl;
	}

	public void setClaveTipoControl(Short claveTipoControl) {
		this.claveTipoControl = claveTipoControl;
	}

	@Column(name = "CLAVETIPOVALIDACION")
	public Short getClaveTipoValidacion() {
		return this.claveTipoValidacion;
	}

	public void setClaveTipoValidacion(Short claveTipoValidacion) {
		this.claveTipoValidacion = claveTipoValidacion;
	}

	@Column(name = "DESCRIPCIONCLASEREMOTA")
	public String getDescripcionClaseRemota() {
		return this.descripcionClaseRemota;
	}

	public void setDescripcionClaseRemota(String descripcionClaseRemota) {
		this.descripcionClaseRemota = descripcionClaseRemota;
	}

	@Column(name = "DESCRIPCIONETIQUETA")
	public String getDescripcionEtiqueta() {
		return this.descripcionEtiqueta;
	}

	public void setDescripcionEtiqueta(String descripcionEtiqueta) {
		this.descripcionEtiqueta = descripcionEtiqueta;
	}

	@Column(name = "IDGRUPO")
	public Short getIdGrupo() {
		return this.idGrupo;
	}

	public void setIdGrupo(Short idGrupo) {
		this.idGrupo = idGrupo;
	}

}
