package mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasivacotizacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name = "TOCARGAMASIVACONDICIONESESP", schema = "MIDAS")
public class CargaMasivaCondicionesEsp implements Serializable,Entidad {
	
	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1542303481838727800L;
	private Long id;
	private Long idToControlArchivo;
	private Short claveEstatus;
	private Date fechaCreacion;
	private String codigoUsuarioCreacion;
	private Long idToSolicitud;
	private ControlArchivoDTO controlArchivoDTO;
	private String descripcionEstatus;
	
	public static final Short ESTATUS_CON_ERROR  = 0;
	public static final Short ESTATUS_TERMINADO  = 1;
	public static final Short ESTATUS_PENDIENTE  = 2;
	public static final Short ESTATUS_EN_PROCESO = 3;
	
	
	@Id
	@SequenceGenerator(name = "IDTOCARGAMASIVACONDESP_SEQ", sequenceName = "IDTOCARGAMASIVACONDESP_SEQ", schema="MIDAS",allocationSize =1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOCARGAMASIVACONDESP_SEQ")
	@Column(name = "ID", unique = true, nullable = false, scale = 0)
	public Long getId() {
		return id;
	}
	
	@Column(name = "IDTOCONTROLARCHIVO", nullable = false)
	public Long getIdToControlArchivo() {
		return idToControlArchivo;
	}
	
	@Column(name = "CLAVEESTATUS", nullable = false)
	public Short getClaveEstatus() {
		return claveEstatus;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHACREACION", nullable = false, length = 7)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	@Column(name = "CODIGOUSUARIOCREACION", nullable = true)
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	@Column(name = "IDTOSOLICITUD", nullable = true)
	public Long getIdToSolicitud() {
		return idToSolicitud;
	}
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinFetch(JoinFetchType.INNER)
	@JoinColumn(name = "IDTOCONTROLARCHIVO", nullable = false, updatable = false, insertable=false)
	public ControlArchivoDTO getControlArchivoDTO() {
		return controlArchivoDTO;
	}
	
	@Transient
	public String getDescripcionEstatus() {
		
		if(claveEstatus.equals(ESTATUS_CON_ERROR)){
			descripcionEstatus =  "CON ERROR";
		}else if(claveEstatus.equals(ESTATUS_TERMINADO)){
			descripcionEstatus =  "TERMINADO";
		}else if(claveEstatus.equals(ESTATUS_PENDIENTE)){
			descripcionEstatus =  "PENDIENTE";
		}else if(claveEstatus.equals(ESTATUS_EN_PROCESO)){
			descripcionEstatus =  "EN PROCESO";
		}else{
			descripcionEstatus =  "ERROR";
		}
		return descripcionEstatus; 
	}

	public void setControlArchivoDTO(ControlArchivoDTO controlArchivoDTO) {
		this.controlArchivoDTO = controlArchivoDTO;
	}


	public void setId(Long id) {
		this.id = id;
	}
	
	public void setIdToControlArchivo(Long idToControlArchivo) {
		this.idToControlArchivo = idToControlArchivo;
	}
	
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	public void setClaveEstatus(Short claveEstatus) {
		this.claveEstatus = claveEstatus;
	}
	
	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
	
	public void setIdToSolicitud(Long idToSolicitud) {
		this.idToSolicitud = idToSolicitud;
	}
	
	public void setDescripcionEstatus(String descripcionEstatus) {
		this.descripcionEstatus = descripcionEstatus;
	}
	

	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long getBusinessKey() {
		return this.getIdToControlArchivo();
	}


}
