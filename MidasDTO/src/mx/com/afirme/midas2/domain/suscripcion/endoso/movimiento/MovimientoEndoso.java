package mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.dto.suscripcion.endoso.movimiento.FuenteMovimientoDTO;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.TipoMovimientoEndoso;

@Entity
@Table(name = "MMOVIMIENTOENDOSO", schema = "MIDAS")
public class MovimientoEndoso implements Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1708467654947975823L;

	public static enum Concepto {

		PRIMA_NETA(1), 
		RPF(2), 
		BONIFICACION_COMISION(3), 
		BONIFICACION_RPF(4), 
		COMISION(5), 
		COMISION_RPF(6),
		DERECHOS(7), 
		IVA(8),
		SOBRECOMISIONAGENTE(9),
		SOBRECOMISIONPROM(10),
		BONOAGENTE(11),
		BONOPROM(12),
		CESIONDERECHOSAGENTE(13),
		CESIONDERECHOSPROM(14),
		SOBRECOMISIONUDIAGENTE(15),
		SOBRECOMISIONUDIPROM(16);
		
		private final Integer value;
		
		Concepto(Integer value) {
			this.value = value;
		}
		
		public Integer getValue() {
			return value;
		}
	};
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQMMOVIMIENTOENDOSOID")
	@SequenceGenerator(name = "SEQMMOVIMIENTOENDOSOID", sequenceName = "MIDAS.SEQMMOVIMIENTOENDOSOID", allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name="controlendosocot_id", referencedColumnName="ID", nullable=false)
	private ControlEndosoCot controlEndosoCot;

	@Enumerated
	@Column(name = "TIPO", nullable=false)
	private TipoMovimientoEndoso tipo;
	
	@Column(name = "DESCRIPCION", nullable=false)
	private String descripcion;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "mmovimientoendoso_id", referencedColumnName="id")
	private MovimientoEndoso movimientoEndoso;
		
	@OneToMany(mappedBy="movimientoEndoso", cascade=CascadeType.ALL, orphanRemoval=true)
	private List<MovimientoEndoso> movimientosEndoso = new ArrayListNullAware<MovimientoEndoso>();
	
	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinColumn(name = "MINCISOB_ID", referencedColumnName = "ID")
	private BitemporalInciso bitemporalInciso;

	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinColumn(name = "MCOBERTURASECCIONB_ID", referencedColumnName = "ID")
	private BitemporalCoberturaSeccion bitemporalCoberturaSeccion;
	
	@Column(name = "VALORMOVIMIENTO")
	private BigDecimal valorMovimiento;

	@Column(name = "VALORANTERIOR")
	private BigDecimal valorAnterior;

	@Column(name = "VALORPOSTERIOR")
	private BigDecimal valorPosterior;
	
	@Column(name = "FACTORIGUALACIONCOBRANZA") 
	private BigDecimal factorCobranzaPrimaNeta = new BigDecimal(1);//TODO renombrar la columna en la BD a FACTCOBRANZAPRIMANETA
	
	@Column(name = "VALORBONCOMISION") 
	private BigDecimal valorBonificacionComision;
	
	@Column(name = "FACTCOBRANZABONCOMISION") 
	private BigDecimal factorCobranzaBonificacionComision = new BigDecimal(1);
	
	@Column(name = "VALORBONRPF") 
	private BigDecimal valorBonificacionRPF;
	
	@Column(name = "FACTCOBRANZABONRPF") 
	private BigDecimal factorCobranzaBonificacionRPF = new BigDecimal(1);
	
	@Column(name = "VALORRPF") 
	private BigDecimal valorRPF;
	
	@Column(name = "FACTCOBRANZARPF") 
	private BigDecimal factorCobranzaRPF = new BigDecimal(1);
	
	@Column(name = "VALORCOMISION") 
	private BigDecimal valorComision;
	
	@Column(name = "FACTCOBRANZACOMISION") 
	private BigDecimal factorCobranzaComision = new BigDecimal(1);
	
	@Column(name = "VALORCOMISIONRPF") 
	private BigDecimal valorComisionRPF;
	
	@Column(name = "FACTCOBRANZACOMISIONRPF") 
	private BigDecimal factorCobranzaComisionRPF = new BigDecimal(1);
	
	@Column(name = "VALORDERECHOS") 
	private BigDecimal valorDerechos;
	
	@Column(name = "FACTCOBRANZADERECHOS") 
	private BigDecimal factorCobranzaDerechos = new BigDecimal(1);
	
	@Column(name = "VALORIVA") 
	private BigDecimal valorIva;
	
	@Column(name = "FACTCOBRANZAIVA") 
	private BigDecimal factorCobranzaIva = new BigDecimal(1);
	
	@Transient
	private Integer numeroCoberturasInciso;
	
	@Transient
	private Boolean ajusteValorCeroPrimaNeta;
	
	@Transient
	private Boolean ajusteValorCeroRPF;
	
	@Transient
	private Boolean ajusteValorCeroBonificacionComision;
	
	@Transient
	private Boolean ajusteValorCeroBonificacionRPF;
	
	@Transient
	private Boolean ajusteValorCeroComision;
	
	@Transient
	private Boolean ajusteValorCeroComisionRPF;
	
	@Transient
	private Boolean ajusteValorCeroDerechos;

	//Seccion ajustes de redondeo
	
	@Transient
	private Integer numeroCoberturasPrimaNeta;
	
	@Transient
	private Integer numeroCoberturasRPF;
	
	@Transient
	private Integer numeroCoberturasBonificacionComision;
	
	@Transient
	private Integer numeroCoberturasBonificacionRPF;
	
	@Transient
	private Integer numeroCoberturasComision;
	
	@Transient
	private Integer numeroCoberturasComisionRPF;
	
	@Transient
	private Integer numeroCoberturasDerechos;
	
	@Transient
	private Integer numeroCoberturasIva;
	
	@Transient
	private BigDecimal diferenciaPrimaNeta;
	
	@Transient
	private BigDecimal diferenciaRPF;
	
	@Transient
	private BigDecimal diferenciaBonificacionComision;
	
	@Transient
	private BigDecimal diferenciaBonificacionRPF;
	
	@Transient
	private BigDecimal diferenciaComision;
	
	@Transient
	private BigDecimal diferenciaComisionRPF;
	
	@Transient
	private BigDecimal diferenciaDerechos;
	
	@Transient
	private BigDecimal diferenciaIva;
	
	@Transient
	private EndosoIDTO validacionInciso;
	
	@Column(name = "VALORSOBRECOMISIONAGENTE") 
	private BigDecimal valorSobreComisionAgente;
	
	@Column(name = "FACTCOBRANZASOBRECOMAGENTE") 
	private BigDecimal factorCobranzaSobreComisionAgente = new BigDecimal(1);
	
	@Column(name = "VALORSOBRECOMISIONPROM") 
	private BigDecimal valorSobreComisionProm;
	
	@Column(name = "FACTCOBRANZASOBRECOMPROM") 
	private BigDecimal factorCobranzaSobreComisionProm = new BigDecimal(1);
	
	@Column(name = "VALORBONOAGENTE") 
	private BigDecimal valorBonoAgente;
	
	@Column(name = "FACTCOBRANZABONOAGENTE") 
	private BigDecimal factorCobranzaBonoAgente = new BigDecimal(1);
	
	@Column(name = "VALORBONOPROM") 
	private BigDecimal valorBonoProm;
	
	@Column(name = "FACTCOBRANZABONOPROM") 
	private BigDecimal factorCobranzaBonoProm = new BigDecimal(1);
	
	@Column(name = "VALORCESIONDERECHOSAGENTE") 
	private BigDecimal valorCesionDerechosAgente;
	
	@Column(name = "FACTCOBRANZACESDERECHOSAGENTE") 
	private BigDecimal factorCobranzaCesionDerechosAgente = new BigDecimal(1);
	
	@Column(name = "VALORCESIONDERECHOSPROM") 
	private BigDecimal valorCesionDerechosProm;
	
	@Column(name = "FACTCOBRANZACESDERECHOSPROM") 
	private BigDecimal factorCobranzaCesionDerechosProm = new BigDecimal(1);
	
	@Column(name = "VALORSOBRECOMUDIAGENTE") 
	private BigDecimal valorSobreComisionUDIAgente;
	
	@Column(name = "FACTCOBRANZASOBRECOMUDIAGENTE") 
	private BigDecimal factorCobranzaSobreComisionUDIAgente = new BigDecimal(1);
	
	@Column(name = "VALORSOBRECOMUDIPROM") 
	private BigDecimal valorSobreComisionUDIProm;
	
	@Column(name = "FACTCOBRANZASOBRECOMUDIPROM") 
	private BigDecimal factorCobranzaSobreComisionUDIProm = new BigDecimal(1);
	
	@Transient
	private Integer numeroCoberturasSobreComisionAgente;
	
	@Transient
	private Integer numeroCoberturasSobreComisionProm;
	
	@Transient
	private Integer numeroCoberturasBonoAgente;
	
	@Transient
	private Integer numeroCoberturasBonoProm;
	
	@Transient
	private Integer numeroCoberturasCesionDerechosAgente;
	
	@Transient
	private Integer numeroCoberturasCesionDerechosProm;
	
	@Transient
	private Integer numeroCoberturasSobreComisionUDIAgente;
	
	@Transient
	private Integer numeroCoberturasSobreComisionUDIProm;
	
	@Transient
	private Boolean ajusteValorCeroSobreComisionAgente;
	
	@Transient
	private Boolean ajusteValorCeroSobreComisionProm;
	
	@Transient
	private Boolean ajusteValorCeroBonoAgente;
	
	@Transient
	private Boolean ajusteValorCeroBonoProm;
	
	@Transient
	private Boolean ajusteValorCeroCesionDerechosAgente;
	
	@Transient
	private Boolean ajusteValorCeroCesionDerechosProm;
	
	@Transient
	private Boolean ajusteValorCeroSobreComisionUDIAgente;
	
	@Transient
	private Boolean ajusteValorCeroSobreComisionUDIProm;
	
	@Transient
	private BigDecimal diferenciaSobreComisionAgente;
	
	@Transient
	private BigDecimal diferenciaSobreComisionProm;
	
	@Transient
	private BigDecimal diferenciaBonoAgente;
	
	@Transient
	private BigDecimal diferenciaBonoProm;
	
	@Transient
	private BigDecimal diferenciaCesionDerechosAgente;
	
	@Transient
	private BigDecimal diferenciaCesionDerechosProm;
	
	@Transient
	private BigDecimal diferenciaSobreComisionUDIAgente;
	
	@Transient
	private BigDecimal diferenciaSobreComisionUDIProm;
	
	@Transient
	private FuenteMovimientoDTO fuenteMovimiento;
		
	public MovimientoEndoso() {
		
	}
	
	//Movimiento general
	public MovimientoEndoso(ControlEndosoCot controlEndosoCot, TipoMovimientoEndoso tipo, String descripcion) {
		setControlEndosoCot(controlEndosoCot);
		setTipo(tipo);
		setDescripcion(descripcion);
	}
	
	//Movimiento de Inciso
	public MovimientoEndoso(ControlEndosoCot controlEndosoCot, TipoMovimientoEndoso tipo, String descripcion, BitemporalInciso bitemporalInciso) {
		setControlEndosoCot(controlEndosoCot);
		setTipo(tipo);
		setDescripcion(descripcion);
		setBitemporalInciso(bitemporalInciso);
	}
	
	//Movimiento de Cobertura
	public MovimientoEndoso(ControlEndosoCot controlEndosoCot, TipoMovimientoEndoso tipo, String descripcion, BitemporalCoberturaSeccion bitemporalCoberturaSeccion) {
		setControlEndosoCot(controlEndosoCot);
		setTipo(tipo);
		setDescripcion(descripcion);
		setBitemporalCoberturaSeccion(bitemporalCoberturaSeccion);
	}
	
	public MovimientoEndoso(MovimientoEndoso movimiento) {
		setControlEndosoCot(movimiento.getControlEndosoCot());
		setTipo(movimiento.getTipo());
		setDescripcion(movimiento.getDescripcion());
		setBitemporalInciso(movimiento.getBitemporalInciso());
		setBitemporalCoberturaSeccion(movimiento.getBitemporalCoberturaSeccion());
		setValorAnterior(movimiento.getValorAnterior());
		setValorPosterior(movimiento.getValorPosterior());
		setValorMovimiento(movimiento.getValorMovimiento());
		setFactorCobranzaPrimaNeta(movimiento.getFactorCobranzaPrimaNeta());
		setValorBonificacionComision(movimiento.getValorBonificacionComision());
		setFactorCobranzaBonificacionComision(movimiento.getFactorCobranzaBonificacionComision());
		setValorBonificacionRPF(movimiento.getValorBonificacionRPF());
		setFactorCobranzaBonificacionRPF(movimiento.getFactorCobranzaBonificacionRPF());
		setValorRPF(movimiento.getValorRPF());
		setFactorCobranzaRPF(movimiento.getFactorCobranzaRPF());
		setValorComision(movimiento.getValorComision());
		setFactorCobranzaComision(movimiento.getFactorCobranzaComision());
		setValorComisionRPF(movimiento.getValorComisionRPF());
		setFactorCobranzaComisionRPF(movimiento.getFactorCobranzaComisionRPF());
		setValorDerechos(movimiento.getValorDerechos());
		setFactorCobranzaDerechos(movimiento.getFactorCobranzaDerechos());
		setValorIva(movimiento.getValorIva());
		setFactorCobranzaIva(movimiento.getFactorCobranzaIva());
		setValorSobreComisionAgente(movimiento.getValorSobreComisionAgente());
		setValorSobreComisionProm(movimiento.getValorSobreComisionProm());
		setValorBonoAgente(movimiento.getValorBonoAgente());
		setValorBonoProm(movimiento.getValorBonoProm());
		setValorCesionDerechosAgente(movimiento.getValorCesionDerechosAgente());
		setValorCesionDerechosProm(movimiento.getValorCesionDerechosProm());
		setValorSobreComisionUDIAgente(movimiento.getValorSobreComisionUDIAgente());
		setValorSobreComisionUDIProm(movimiento.getValorSobreComisionUDIProm());
		setFactorCobranzaSobreComisionAgente(movimiento.getFactorCobranzaSobreComisionAgente());
		setFactorCobranzaSobreComisionProm(movimiento.getFactorCobranzaSobreComisionProm());
		setFactorCobranzaBonoAgente(movimiento.getFactorCobranzaBonoAgente());
		setFactorCobranzaBonoProm(movimiento.getFactorCobranzaBonoProm());
		setFactorCobranzaCesionDerechosAgente(movimiento.getFactorCobranzaCesionDerechosAgente());
		setFactorCobranzaCesionDerechosProm(movimiento.getFactorCobranzaCesionDerechosProm());
		setFactorCobranzaSobreComisionUDIAgente(movimiento.getFactorCobranzaSobreComisionUDIAgente());
		setFactorCobranzaSobreComisionUDIProm(movimiento.getFactorCobranzaSobreComisionUDIProm());
		
		
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ControlEndosoCot getControlEndosoCot() {
		return controlEndosoCot;
	}
	
	public void setControlEndosoCot(ControlEndosoCot controlEndosoCot) {
		this.controlEndosoCot = controlEndosoCot;
	}

	public TipoMovimientoEndoso getTipo() {
		return tipo;
	}

	public void setTipo(TipoMovimientoEndoso tipo) {
		this.tipo = tipo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public MovimientoEndoso getMovimientoEndoso() {
		return movimientoEndoso;
	}

	public void setMovimientoEndoso(MovimientoEndoso movimientoEndoso) {
		this.movimientoEndoso = movimientoEndoso;
	}
	
	public List<MovimientoEndoso> getMovimientosEndoso() {
		return movimientosEndoso;
	}

	public void setMovimientosEndoso(List<MovimientoEndoso> movimientosEndoso) {
		this.movimientosEndoso = movimientosEndoso;
	}

	public BitemporalInciso getBitemporalInciso() {
		return bitemporalInciso;
	}
	
	public void setBitemporalInciso(BitemporalInciso bitemporalInciso) {
		this.bitemporalInciso = bitemporalInciso;
	}

	public BitemporalCoberturaSeccion getBitemporalCoberturaSeccion() {
		return bitemporalCoberturaSeccion;
	}

	public void setBitemporalCoberturaSeccion(
			BitemporalCoberturaSeccion bitemporalCoberturaSeccion) {
		this.bitemporalCoberturaSeccion = bitemporalCoberturaSeccion;
	}

	public BigDecimal getValorMovimiento() {
		return valorMovimiento;
	}

	public void setValorMovimiento(BigDecimal valorMovimiento) {
		this.valorMovimiento = valorMovimiento;
	}

	public BigDecimal getValorAnterior() {
		return valorAnterior;
	}

	public void setValorAnterior(BigDecimal valorAnterior) {
		this.valorAnterior = valorAnterior;
	}

	public BigDecimal getValorPosterior() {
		return valorPosterior;
	}

	public void setValorPosterior(BigDecimal valorPosterior) {
		this.valorPosterior = valorPosterior;
	}
	
	public BigDecimal getFactorCobranzaPrimaNeta() {
		return factorCobranzaPrimaNeta;
	}

	public void setFactorCobranzaPrimaNeta(BigDecimal factorCobranzaPrimaNeta) {
		this.factorCobranzaPrimaNeta = factorCobranzaPrimaNeta;
	}

	public BigDecimal getValorBonificacionComision() {
		return valorBonificacionComision;
	}

	public void setValorBonificacionComision(BigDecimal valorBonificacionComision) {
		this.valorBonificacionComision = valorBonificacionComision;
	}

	public BigDecimal getFactorCobranzaBonificacionComision() {
		return factorCobranzaBonificacionComision;
	}

	public void setFactorCobranzaBonificacionComision(
			BigDecimal factorCobranzaBonificacionComision) {
		this.factorCobranzaBonificacionComision = factorCobranzaBonificacionComision;
	}

	public BigDecimal getValorBonificacionRPF() {
		return valorBonificacionRPF;
	}

	public void setValorBonificacionRPF(BigDecimal valorBonificacionRPF) {
		this.valorBonificacionRPF = valorBonificacionRPF;
	}

	public BigDecimal getFactorCobranzaBonificacionRPF() {
		return factorCobranzaBonificacionRPF;
	}

	public void setFactorCobranzaBonificacionRPF(
			BigDecimal factorCobranzaBonificacionRPF) {
		this.factorCobranzaBonificacionRPF = factorCobranzaBonificacionRPF;
	}

	public BigDecimal getValorRPF() {
		return valorRPF;
	}

	public void setValorRPF(BigDecimal valorRPF) {
		this.valorRPF = valorRPF;
	}

	public BigDecimal getFactorCobranzaRPF() {
		return factorCobranzaRPF;
	}

	public void setFactorCobranzaRPF(BigDecimal factorCobranzaRPF) {
		this.factorCobranzaRPF = factorCobranzaRPF;
	}

	public BigDecimal getValorComision() {
		return valorComision;
	}

	public void setValorComision(BigDecimal valorComision) {
		this.valorComision = valorComision;
	}

	public BigDecimal getFactorCobranzaComision() {
		return factorCobranzaComision;
	}

	public void setFactorCobranzaComision(BigDecimal factorCobranzaComision) {
		this.factorCobranzaComision = factorCobranzaComision;
	}

	public BigDecimal getValorComisionRPF() {
		return valorComisionRPF;
	}

	public void setValorComisionRPF(BigDecimal valorComisionRPF) {
		this.valorComisionRPF = valorComisionRPF;
	}

	public BigDecimal getFactorCobranzaComisionRPF() {
		return factorCobranzaComisionRPF;
	}

	public void setFactorCobranzaComisionRPF(BigDecimal factorCobranzaComisionRPF) {
		this.factorCobranzaComisionRPF = factorCobranzaComisionRPF;
	}

	public BigDecimal getValorDerechos() {
		return valorDerechos;
	}

	public void setValorDerechos(BigDecimal valorDerechos) {
		this.valorDerechos = valorDerechos;
	}

	public BigDecimal getFactorCobranzaDerechos() {
		return factorCobranzaDerechos;
	}

	public void setFactorCobranzaDerechos(BigDecimal factorCobranzaDerechos) {
		this.factorCobranzaDerechos = factorCobranzaDerechos;
	}
	
	public BigDecimal getValorIva() {
		return valorIva;
	}

	public void setValorIva(BigDecimal valorIva) {
		this.valorIva = valorIva;
	}

	public BigDecimal getFactorCobranzaIva() {
		return factorCobranzaIva;
	}

	public void setFactorCobranzaIva(BigDecimal factorCobranzaIva) {
		this.factorCobranzaIva = factorCobranzaIva;
	}
	
	public Integer getNumeroCoberturasInciso() {
		return numeroCoberturasInciso;
	}

	public void setNumeroCoberturasInciso(Integer numeroCoberturasInciso) {
		this.numeroCoberturasInciso = numeroCoberturasInciso;
	}

	public Boolean getAjusteValorCeroPrimaNeta() {
		return ajusteValorCeroPrimaNeta;
	}

	public void setAjusteValorCeroPrimaNeta(Boolean ajusteValorCeroPrimaNeta) {
		this.ajusteValorCeroPrimaNeta = ajusteValorCeroPrimaNeta;
	}

	public Boolean getAjusteValorCeroRPF() {
		return ajusteValorCeroRPF;
	}

	public void setAjusteValorCeroRPF(Boolean ajusteValorCeroRPF) {
		this.ajusteValorCeroRPF = ajusteValorCeroRPF;
	}

	public Boolean getAjusteValorCeroBonificacionComision() {
		return ajusteValorCeroBonificacionComision;
	}

	public void setAjusteValorCeroBonificacionComision(
			Boolean ajusteValorCeroBonificacionComision) {
		this.ajusteValorCeroBonificacionComision = ajusteValorCeroBonificacionComision;
	}

	public Boolean getAjusteValorCeroBonificacionRPF() {
		return ajusteValorCeroBonificacionRPF;
	}

	public void setAjusteValorCeroBonificacionRPF(
			Boolean ajusteValorCeroBonificacionRPF) {
		this.ajusteValorCeroBonificacionRPF = ajusteValorCeroBonificacionRPF;
	}

	public Boolean getAjusteValorCeroComision() {
		return ajusteValorCeroComision;
	}

	public void setAjusteValorCeroComision(Boolean ajusteValorCeroComision) {
		this.ajusteValorCeroComision = ajusteValorCeroComision;
	}

	public Boolean getAjusteValorCeroComisionRPF() {
		return ajusteValorCeroComisionRPF;
	}

	public void setAjusteValorCeroComisionRPF(Boolean ajusteValorCeroComisionRPF) {
		this.ajusteValorCeroComisionRPF = ajusteValorCeroComisionRPF;
	}

	public Boolean getAjusteValorCeroDerechos() {
		return ajusteValorCeroDerechos;
	}

	public void setAjusteValorCeroDerechos(Boolean ajusteValorCeroDerechos) {
		this.ajusteValorCeroDerechos = ajusteValorCeroDerechos;
	}
	
	public Integer getNumeroCoberturasPrimaNeta() {
		return numeroCoberturasPrimaNeta;
	}

	public void setNumeroCoberturasPrimaNeta(Integer numeroCoberturasPrimaNeta) {
		this.numeroCoberturasPrimaNeta = numeroCoberturasPrimaNeta;
	}

	public Integer getNumeroCoberturasRPF() {
		return numeroCoberturasRPF;
	}

	public void setNumeroCoberturasRPF(Integer numeroCoberturasRPF) {
		this.numeroCoberturasRPF = numeroCoberturasRPF;
	}

	public Integer getNumeroCoberturasBonificacionComision() {
		return numeroCoberturasBonificacionComision;
	}

	public void setNumeroCoberturasBonificacionComision(
			Integer numeroCoberturasBonificacionComision) {
		this.numeroCoberturasBonificacionComision = numeroCoberturasBonificacionComision;
	}

	public Integer getNumeroCoberturasBonificacionRPF() {
		return numeroCoberturasBonificacionRPF;
	}

	public void setNumeroCoberturasBonificacionRPF(
			Integer numeroCoberturasBonificacionRPF) {
		this.numeroCoberturasBonificacionRPF = numeroCoberturasBonificacionRPF;
	}

	public Integer getNumeroCoberturasComision() {
		return numeroCoberturasComision;
	}

	public void setNumeroCoberturasComision(Integer numeroCoberturasComision) {
		this.numeroCoberturasComision = numeroCoberturasComision;
	}

	public Integer getNumeroCoberturasComisionRPF() {
		return numeroCoberturasComisionRPF;
	}

	public void setNumeroCoberturasComisionRPF(Integer numeroCoberturasComisionRPF) {
		this.numeroCoberturasComisionRPF = numeroCoberturasComisionRPF;
	}

	public Integer getNumeroCoberturasDerechos() {
		return numeroCoberturasDerechos;
	}

	public void setNumeroCoberturasDerechos(Integer numeroCoberturasDerechos) {
		this.numeroCoberturasDerechos = numeroCoberturasDerechos;
	}

	public Integer getNumeroCoberturasIva() {
		return numeroCoberturasIva;
	}

	public void setNumeroCoberturasIva(Integer numeroCoberturasIva) {
		this.numeroCoberturasIva = numeroCoberturasIva;
	}

	public BigDecimal getDiferenciaPrimaNeta() {
		return diferenciaPrimaNeta;
	}

	public void setDiferenciaPrimaNeta(BigDecimal diferenciaPrimaNeta) {
		this.diferenciaPrimaNeta = diferenciaPrimaNeta;
	}

	public BigDecimal getDiferenciaRPF() {
		return diferenciaRPF;
	}

	public void setDiferenciaRPF(BigDecimal diferenciaRPF) {
		this.diferenciaRPF = diferenciaRPF;
	}

	public BigDecimal getDiferenciaBonificacionComision() {
		return diferenciaBonificacionComision;
	}

	public void setDiferenciaBonificacionComision(
			BigDecimal diferenciaBonificacionComision) {
		this.diferenciaBonificacionComision = diferenciaBonificacionComision;
	}

	public BigDecimal getDiferenciaBonificacionRPF() {
		return diferenciaBonificacionRPF;
	}

	public void setDiferenciaBonificacionRPF(BigDecimal diferenciaBonificacionRPF) {
		this.diferenciaBonificacionRPF = diferenciaBonificacionRPF;
	}

	public BigDecimal getDiferenciaComision() {
		return diferenciaComision;
	}

	public void setDiferenciaComision(BigDecimal diferenciaComision) {
		this.diferenciaComision = diferenciaComision;
	}

	public BigDecimal getDiferenciaComisionRPF() {
		return diferenciaComisionRPF;
	}

	public void setDiferenciaComisionRPF(BigDecimal diferenciaComisionRPF) {
		this.diferenciaComisionRPF = diferenciaComisionRPF;
	}

	public BigDecimal getDiferenciaDerechos() {
		return diferenciaDerechos;
	}

	public void setDiferenciaDerechos(BigDecimal diferenciaDerechos) {
		this.diferenciaDerechos = diferenciaDerechos;
	}

	public BigDecimal getDiferenciaIva() {
		return diferenciaIva;
	}

	public void setDiferenciaIva(BigDecimal diferenciaIva) {
		this.diferenciaIva = diferenciaIva;
	}
	
	public EndosoIDTO getValidacionInciso() {
		return validacionInciso;
	}

	public void setValidacionInciso(EndosoIDTO validacionInciso) {
		this.validacionInciso = validacionInciso;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return getId();
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return null;
	}

	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("id: " + getId() + ", ");
		sb.append("controlEndosoCot: " + getControlEndosoCot());
		sb.append("]");
		
		return sb.toString();
	}

	public void setValorSobreComisionAgente(BigDecimal valorSobreComisionAgente) {
		this.valorSobreComisionAgente = valorSobreComisionAgente;
	}

	public BigDecimal getValorSobreComisionAgente() {
		return valorSobreComisionAgente;
	}

	public void setFactorCobranzaSobreComisionAgente(
			BigDecimal factorCobranzaSobreComisionAgente) {
		this.factorCobranzaSobreComisionAgente = factorCobranzaSobreComisionAgente;
	}

	public BigDecimal getFactorCobranzaSobreComisionAgente() {
		return factorCobranzaSobreComisionAgente;
	}

	public void setValorSobreComisionProm(BigDecimal valorSobreComisionProm) {
		this.valorSobreComisionProm = valorSobreComisionProm;
	}

	public BigDecimal getValorSobreComisionProm() {
		return valorSobreComisionProm;
	}

	public void setFactorCobranzaSobreComisionProm(
			BigDecimal factorCobranzaSobreComisionProm) {
		this.factorCobranzaSobreComisionProm = factorCobranzaSobreComisionProm;
	}

	public BigDecimal getFactorCobranzaSobreComisionProm() {
		return factorCobranzaSobreComisionProm;
	}

	public void setValorBonoAgente(BigDecimal valorBonoAgente) {
		this.valorBonoAgente = valorBonoAgente;
	}

	public BigDecimal getValorBonoAgente() {
		return valorBonoAgente;
	}

	public void setFactorCobranzaBonoAgente(BigDecimal factorCobranzaBonoAgente) {
		this.factorCobranzaBonoAgente = factorCobranzaBonoAgente;
	}

	public BigDecimal getFactorCobranzaBonoAgente() {
		return factorCobranzaBonoAgente;
	}

	public void setValorBonoProm(BigDecimal valorBonoProm) {
		this.valorBonoProm = valorBonoProm;
	}

	public BigDecimal getValorBonoProm() {
		return valorBonoProm;
	}

	public void setFactorCobranzaBonoProm(BigDecimal factorCobranzaBonoProm) {
		this.factorCobranzaBonoProm = factorCobranzaBonoProm;
	}

	public BigDecimal getFactorCobranzaBonoProm() {
		return factorCobranzaBonoProm;
	}

	public void setValorCesionDerechosAgente(BigDecimal valorCesionDerechosAgente) {
		this.valorCesionDerechosAgente = valorCesionDerechosAgente;
	}

	public BigDecimal getValorCesionDerechosAgente() {
		return valorCesionDerechosAgente;
	}

	public void setFactorCobranzaCesionDerechosAgente(
			BigDecimal factorCobranzaCesionDerechosAgente) {
		this.factorCobranzaCesionDerechosAgente = factorCobranzaCesionDerechosAgente;
	}

	public BigDecimal getFactorCobranzaCesionDerechosAgente() {
		return factorCobranzaCesionDerechosAgente;
	}

	public void setValorCesionDerechosProm(BigDecimal valorCesionDerechosProm) {
		this.valorCesionDerechosProm = valorCesionDerechosProm;
	}

	public BigDecimal getValorCesionDerechosProm() {
		return valorCesionDerechosProm;
	}

	public void setValorSobreComisionUDIAgente(
			BigDecimal valorSobreComisionUDIAgente) {
		this.valorSobreComisionUDIAgente = valorSobreComisionUDIAgente;
	}

	public BigDecimal getValorSobreComisionUDIAgente() {
		return valorSobreComisionUDIAgente;
	}

	public void setFactorCobranzaSobreComisionUDIAgente(
			BigDecimal factorCobranzaSobreComisionUDIAgente) {
		this.factorCobranzaSobreComisionUDIAgente = factorCobranzaSobreComisionUDIAgente;
	}

	public BigDecimal getFactorCobranzaSobreComisionUDIAgente() {
		return factorCobranzaSobreComisionUDIAgente;
	}

	public void setValorSobreComisionUDIProm(BigDecimal valorSobreComisionUDIProm) {
		this.valorSobreComisionUDIProm = valorSobreComisionUDIProm;
	}

	public BigDecimal getValorSobreComisionUDIProm() {
		return valorSobreComisionUDIProm;
	}

	public void setFactorCobranzaSobreComisionUDIProm(
			BigDecimal factorCobranzaSobreComisionUDIProm) {
		this.factorCobranzaSobreComisionUDIProm = factorCobranzaSobreComisionUDIProm;
	}

	public BigDecimal getFactorCobranzaSobreComisionUDIProm() {
		return factorCobranzaSobreComisionUDIProm;
	}

	public void setFactorCobranzaCesionDerechosProm(
			BigDecimal factorCobranzaCesionDerechosProm) {
		this.factorCobranzaCesionDerechosProm = factorCobranzaCesionDerechosProm;
	}

	public BigDecimal getFactorCobranzaCesionDerechosProm() {
		return factorCobranzaCesionDerechosProm;
	}

	public void setNumeroCoberturasSobreComisionAgente(
			Integer numeroCoberturasSobreComisionAgente) {
		this.numeroCoberturasSobreComisionAgente = numeroCoberturasSobreComisionAgente;
	}

	public Integer getNumeroCoberturasSobreComisionAgente() {
		return numeroCoberturasSobreComisionAgente;
	}

	public void setNumeroCoberturasSobreComisionProm(
			Integer numeroCoberturasSobreComisionProm) {
		this.numeroCoberturasSobreComisionProm = numeroCoberturasSobreComisionProm;
	}

	public Integer getNumeroCoberturasSobreComisionProm() {
		return numeroCoberturasSobreComisionProm;
	}

	public void setNumeroCoberturasBonoAgente(Integer numeroCoberturasBonoAgente) {
		this.numeroCoberturasBonoAgente = numeroCoberturasBonoAgente;
	}

	public Integer getNumeroCoberturasBonoAgente() {
		return numeroCoberturasBonoAgente;
	}

	public void setNumeroCoberturasBonoProm(Integer numeroCoberturasBonoProm) {
		this.numeroCoberturasBonoProm = numeroCoberturasBonoProm;
	}

	public Integer getNumeroCoberturasBonoProm() {
		return numeroCoberturasBonoProm;
	}

	public void setNumeroCoberturasCesionDerechosAgente(
			Integer numeroCoberturasCesionDerechosAgente) {
		this.numeroCoberturasCesionDerechosAgente = numeroCoberturasCesionDerechosAgente;
	}

	public Integer getNumeroCoberturasCesionDerechosAgente() {
		return numeroCoberturasCesionDerechosAgente;
	}

	public void setNumeroCoberturasCesionDerechosProm(
			Integer numeroCoberturasCesionDerechosProm) {
		this.numeroCoberturasCesionDerechosProm = numeroCoberturasCesionDerechosProm;
	}

	public Integer getNumeroCoberturasCesionDerechosProm() {
		return numeroCoberturasCesionDerechosProm;
	}

	public void setNumeroCoberturasSobreComisionUDIAgente(
			Integer numeroCoberturasSobreComisionUDIAgente) {
		this.numeroCoberturasSobreComisionUDIAgente = numeroCoberturasSobreComisionUDIAgente;
	}

	public Integer getNumeroCoberturasSobreComisionUDIAgente() {
		return numeroCoberturasSobreComisionUDIAgente;
	}

	public void setNumeroCoberturasSobreComisionUDIProm(
			Integer numeroCoberturasSobreComisionUDIProm) {
		this.numeroCoberturasSobreComisionUDIProm = numeroCoberturasSobreComisionUDIProm;
	}

	public Integer getNumeroCoberturasSobreComisionUDIProm() {
		return numeroCoberturasSobreComisionUDIProm;
	}

	public void setAjusteValorCeroSobreComisionAgente(
			Boolean ajusteValorCeroSobreComisionAgente) {
		this.ajusteValorCeroSobreComisionAgente = ajusteValorCeroSobreComisionAgente;
	}

	public Boolean getAjusteValorCeroSobreComisionAgente() {
		return ajusteValorCeroSobreComisionAgente;
	}

	public void setAjusteValorCeroSobreComisionProm(
			Boolean ajusteValorCeroSobreComisionProm) {
		this.ajusteValorCeroSobreComisionProm = ajusteValorCeroSobreComisionProm;
	}

	public Boolean getAjusteValorCeroSobreComisionProm() {
		return ajusteValorCeroSobreComisionProm;
	}

	public void setAjusteValorCeroBonoAgente(Boolean ajusteValorCeroBonoAgente) {
		this.ajusteValorCeroBonoAgente = ajusteValorCeroBonoAgente;
	}

	public Boolean getAjusteValorCeroBonoAgente() {
		return ajusteValorCeroBonoAgente;
	}

	public void setAjusteValorCeroBonoProm(Boolean ajusteValorCeroBonoProm) {
		this.ajusteValorCeroBonoProm = ajusteValorCeroBonoProm;
	}

	public Boolean getAjusteValorCeroBonoProm() {
		return ajusteValorCeroBonoProm;
	}

	public void setAjusteValorCeroCesionDerechosAgente(
			Boolean ajusteValorCeroCesionDerechosAgente) {
		this.ajusteValorCeroCesionDerechosAgente = ajusteValorCeroCesionDerechosAgente;
	}

	public Boolean getAjusteValorCeroCesionDerechosAgente() {
		return ajusteValorCeroCesionDerechosAgente;
	}

	public void setAjusteValorCeroCesionDerechosProm(
			Boolean ajusteValorCeroCesionDerechosProm) {
		this.ajusteValorCeroCesionDerechosProm = ajusteValorCeroCesionDerechosProm;
	}

	public Boolean getAjusteValorCeroCesionDerechosProm() {
		return ajusteValorCeroCesionDerechosProm;
	}

	public void setAjusteValorCeroSobreComisionUDIAgente(
			Boolean ajusteValorCeroSobreComisionUDIAgente) {
		this.ajusteValorCeroSobreComisionUDIAgente = ajusteValorCeroSobreComisionUDIAgente;
	}

	public Boolean getAjusteValorCeroSobreComisionUDIAgente() {
		return ajusteValorCeroSobreComisionUDIAgente;
	}

	public void setAjusteValorCeroSobreComisionUDIProm(
			Boolean ajusteValorCeroSobreComisionUDIProm) {
		this.ajusteValorCeroSobreComisionUDIProm = ajusteValorCeroSobreComisionUDIProm;
	}

	public Boolean getAjusteValorCeroSobreComisionUDIProm() {
		return ajusteValorCeroSobreComisionUDIProm;
	}

	public void setDiferenciaSobreComisionAgente(
			BigDecimal diferenciaSobreComisionAgente) {
		this.diferenciaSobreComisionAgente = diferenciaSobreComisionAgente;
	}

	public BigDecimal getDiferenciaSobreComisionAgente() {
		return diferenciaSobreComisionAgente;
	}

	public void setDiferenciaSobreComisionProm(
			BigDecimal diferenciaSobreComisionProm) {
		this.diferenciaSobreComisionProm = diferenciaSobreComisionProm;
	}

	public BigDecimal getDiferenciaSobreComisionProm() {
		return diferenciaSobreComisionProm;
	}

	public void setDiferenciaBonoAgente(BigDecimal diferenciaBonoAgente) {
		this.diferenciaBonoAgente = diferenciaBonoAgente;
	}

	public BigDecimal getDiferenciaBonoAgente() {
		return diferenciaBonoAgente;
	}

	public void setDiferenciaBonoProm(BigDecimal diferenciaBonoProm) {
		this.diferenciaBonoProm = diferenciaBonoProm;
	}

	public BigDecimal getDiferenciaBonoProm() {
		return diferenciaBonoProm;
	}

	public void setDiferenciaCesionDerechosAgente(
			BigDecimal diferenciaCesionDerechosAgente) {
		this.diferenciaCesionDerechosAgente = diferenciaCesionDerechosAgente;
	}

	public BigDecimal getDiferenciaCesionDerechosAgente() {
		return diferenciaCesionDerechosAgente;
	}

	public void setDiferenciaCesionDerechosProm(
			BigDecimal diferenciaCesionDerechosProm) {
		this.diferenciaCesionDerechosProm = diferenciaCesionDerechosProm;
	}

	public BigDecimal getDiferenciaCesionDerechosProm() {
		return diferenciaCesionDerechosProm;
	}

	public void setDiferenciaSobreComisionUDIAgente(
			BigDecimal diferenciaSobreComisionUDIAgente) {
		this.diferenciaSobreComisionUDIAgente = diferenciaSobreComisionUDIAgente;
	}

	public BigDecimal getDiferenciaSobreComisionUDIAgente() {
		return diferenciaSobreComisionUDIAgente;
	}

	public void setDiferenciaSobreComisionUDIProm(
			BigDecimal diferenciaSobreComisionUDIProm) {
		this.diferenciaSobreComisionUDIProm = diferenciaSobreComisionUDIProm;
	}

	public BigDecimal getDiferenciaSobreComisionUDIProm() {
		return diferenciaSobreComisionUDIProm;
	}

	public FuenteMovimientoDTO getFuenteMovimiento() {
		return fuenteMovimiento;
	}

	public void setFuenteMovimiento(FuenteMovimientoDTO fuenteMovimiento) {
		this.fuenteMovimiento = fuenteMovimiento;
	}
	
	
}
