package mx.com.afirme.midas2.domain.suscripcion.impresion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;

/**
 * Clase para almacenar la informacion que se desea modificar en la impresion 
 * de las coberturas de los incisos. Estos cambios no afectan la informacion del
 * sistema, unicamente son modificaciones para la impresion. 
 * 
 */
@Entity
@Table(name="TOEDICIONINCISOCOBERTURA", schema="MIDAS")
public class EdicionIncisoCobertura extends MidasAbstracto implements Entidad{

	private static final long serialVersionUID = -4304607279112319371L;

	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EDICIONINCISOCOBERTURA_GENERATOR")
	@SequenceGenerator(name="EDICIONINCISOCOBERTURA_GENERATOR", sequenceName="TOEDICIONINCISOCOBERTURA_SEQ", schema="MIDAS", allocationSize=1)
	private Long id;
	
	@Column(name="RIESGO")
	private Integer riesgo;
	
	@Column(name="SUMA_ASEGURADA")
	private String sumaAsegurada;
	
	@Column(name="DEDUCIBLE")
	private String deducible;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( { 
        @JoinColumn(name="IDTOPOLIZA", referencedColumnName="IDTOPOLIZA", nullable=false), 
        @JoinColumn(name="VALID_ON", referencedColumnName="VALID_ON", nullable=false),
        @JoinColumn(name="RECORD_FROM", referencedColumnName="RECORD_FROM", nullable=false),
        @JoinColumn(name="TIPO_ENDOSO", referencedColumnName="TIPO_ENDOSO", nullable=false), 
        @JoinColumn(name="VERSION", referencedColumnName="VERSION", nullable=false) })
	private EdicionInciso edicionInciso;
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getRiesgo() {
		return riesgo;
	}

	public void setRiesgo(Integer riesgo) {
		this.riesgo = riesgo;
	}

	public String getSumaAsegurada() {
		return sumaAsegurada;
	}

	public void setSumaAsegurada(String sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}

	public String getDeducible() {
		return deducible;
	}

	public void setDeducible(String deducible) {
		this.deducible = deducible;
	}

	public EdicionInciso getEdicionInciso() {
		return edicionInciso;
	}

	public void setEdicionInciso(EdicionInciso edicionInciso) {
		this.edicionInciso = edicionInciso;
	}

}
