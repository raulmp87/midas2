package mx.com.afirme.midas2.domain.suscripcion.cambiosglobales;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;


/**
 * The persistent class for the TOCONFPLANTILLA database table.
 * 
 */
@Entity
@Table(name="TOCONFPLANTILLA", schema="MIDAS")
public class ConfiguracionPlantillaNeg implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;
	private Long plantillaId;
	private String comentario;
	private BigDecimal cotizacionId;
	private BigDecimal lineaId;
	private BigDecimal paqueteId;
	private List<ConfiguracionPlantillaNegCobertura> configuracionCoberturaList = new ArrayList<ConfiguracionPlantillaNegCobertura>();

    public ConfiguracionPlantillaNeg() {
    }


	@Id
	@SequenceGenerator(name="IDTOCONFPLANTILLA_SEQ", sequenceName="MIDAS.IDTOCONFPLANTILLA_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTOCONFPLANTILLA_SEQ")
	@Column(name="PLANTILLA_ID", unique=true, nullable=false)
	public Long getPlantillaId() {
		return this.plantillaId;
	}

	public void setPlantillaId(Long plantillaId) {
		this.plantillaId = plantillaId;
	}


    @Lob()
	public String getComentario() {
		return this.comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}


	@Column(name="COTIZACION_ID", unique=true, nullable=false)
	public BigDecimal getCotizacionId() {
		return this.cotizacionId;
	}

	public void setCotizacionId(BigDecimal cotizacionId) {
		this.cotizacionId = cotizacionId;
	}


	@Column(name="LINEA_ID", unique=true, nullable=false)
	public BigDecimal getLineaId() {
		return this.lineaId;
	}

	public void setLineaId(BigDecimal lineaId) {
		this.lineaId = lineaId;
	}


	@Column(name="PAQUETE_ID", unique=true, nullable=false)
	public BigDecimal getPaqueteId() {
		return this.paqueteId;
	}

	public void setPaqueteId(BigDecimal paqueteId) {
		this.paqueteId = paqueteId;
	}


	//bi-directional many-to-one association to ConfiguracionPlantillaNegCobertura
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="configuracionPlantilla")
	public List<ConfiguracionPlantillaNegCobertura> getConfiguracionCoberturaList() {
		return this.configuracionCoberturaList;
	}

	public void setConfiguracionCoberturaList(List<ConfiguracionPlantillaNegCobertura> configuracionCoberturaList) {
		this.configuracionCoberturaList = configuracionCoberturaList;
	}
	
	public void addCobertura(ConfiguracionPlantillaNegCobertura cobertura){
		cobertura.setConfiguracionPlantilla(this);
		configuracionCoberturaList.add(cobertura);
	}


	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
}