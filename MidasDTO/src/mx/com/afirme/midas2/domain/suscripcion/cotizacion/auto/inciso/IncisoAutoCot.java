package mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoInciso;
import mx.com.afirme.midas2.domain.catalogos.Paquete;

/**
 * The persistent class for the TOINCISOAUTOCOT database table.
 * 
 */
@Entity
@Table(name="TOINCISOAUTOCOT",schema="MIDAS")
public class IncisoAutoCot implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;
	//POS 	- 				CARACTERISTICA 	- 	ID
	public static final int TRANSMISION		= 	10;
	public static final int CILINDROS		= 	20;
	public static final int BOLSAS_AIRE		=	30;
	public static final int SIS_ELECTRICO	=	40;
	public static final int VESTIDURAS		=	50;
	public static final int QUEMACOCOS		=	60;
	public static final int VERSION			=	70;
	public static final int FRENOS			=	80;
	public static final int SONIDO			=	90;
	public static final int AIR_ACONDIC		=	100;
	public static final int COMBUSTIBLE		=	110;
	public static final int ALARMA_FABR		=	120;
	public static final int INYECCION		=	130;

	private Long id;

	private String estiloId;

	private String claveTipoBien;

	private String estadoId;

	private Short idMoneda;

	private BigDecimal idVersionCarga;

	private Short modeloVehiculo;

	private String modificadoresDescripcion;

	private String modificadoresPrima;

	private String municipioId;

	private String numeroMotor;

	private String numeroSerie;

	private Paquete paquete;
	
	private IncisoCotizacionDTO incisoCotizacionDTO;
	
	private BigDecimal marcaId;
	
	private String descripcionFinal;
	
	private String placa;
	
	private String repuve;
	
	private Long negocioSeccionId;
	
	private Long negocioPaqueteId;
	
	private Long tipoUsoId;
	
	private String numeroLicencia;
	private String nombreConductor;
	private String paternoConductor;
	private String maternoConductor;
	private Date fechaNacConductor;
	private String ocupacionConductor;
	private Integer asociadaCotizacion;
	private Long personaAseguradoId;
	private String nombreAsegurado;
	private String observacionesinciso;	
	private BigDecimal tipoServicioId;
	private Boolean claveSimilar;
	private Double pctDescuentoEstado;
	private String claveAmis;
	private String claveSesa;
	private String observacionesSesa;
	
	private String rutaCirculacion;	
	private BigDecimal idAgrupadorPasajeros;
	private Boolean vinValido;
	private Boolean coincideEstilo;
	private Long codigoPostal;

	public IncisoAutoCot() {
    }
	
	public IncisoAutoCot(AutoInciso autoInciso) {
		this.asociadaCotizacion = autoInciso.getAsociadaCotizacion();
		this.claveTipoBien = autoInciso.getClaveTipoBien();
		//autoInciso.getDescEstilo();
		//autoInciso.getDescLineaNegocio();
		//autoInciso.getDescMarca();
		this.descripcionFinal = autoInciso.getDescripcionFinal();
		//autoInciso.getDescTipoUso();
		this.estadoId = autoInciso.getEstadoId();
		this.estiloId = autoInciso.getEstiloId();
		this.fechaNacConductor = autoInciso.getFechaNacConductor();
		this.idMoneda = autoInciso.getIdMoneda();
		this.idVersionCarga = autoInciso.getIdVersionCarga();
		this.marcaId = autoInciso.getMarcaId();
		this.maternoConductor = autoInciso.getMaternoConductor();
		this.modeloVehiculo = autoInciso.getModeloVehiculo();
		this.modificadoresDescripcion = autoInciso.getModificadoresDescripcion();
		this.modificadoresPrima = autoInciso.getModificadoresPrima();
		this.municipioId = autoInciso.getMunicipioId();
		this.negocioPaqueteId = autoInciso.getNegocioPaqueteId();
		this.negocioSeccionId = autoInciso.getNegocioSeccionId();
		this.nombreAsegurado = autoInciso.getNombreAsegurado();
		//autoInciso.getNombreCompletoConductor();
		this.nombreConductor = autoInciso.getNombreConductor();
		//autoInciso.getNombreEstado();
		//autoInciso.getNombreMunicipio();
		this.numeroLicencia = autoInciso.getNumeroLicencia();
		this.numeroMotor = autoInciso.getNumeroMotor();
		this.numeroSerie = autoInciso.getNumeroSerie();
		this.ocupacionConductor = autoInciso.getOcupacionConductor();
		this.paquete = autoInciso.getPaquete();
		this.paternoConductor = autoInciso.getPaternoConductor();
		this.personaAseguradoId = autoInciso.getPersonaAseguradoId();
		this.placa = autoInciso.getPlaca();
		this.repuve = autoInciso.getRepuve();
		this.tipoUsoId = autoInciso.getTipoUsoId();
		this.claveSimilar = autoInciso.getClaveSimilar();
		this.pctDescuentoEstado = autoInciso.getPctDescuentoEstado();
		this.claveAmis = autoInciso.getClaveAmis();
		this.claveSesa = autoInciso.getClaveSesa();
		this.vinValido = autoInciso.getVinValido();
		this.observacionesSesa = autoInciso.getObservacionesSesa();
		this.coincideEstilo = autoInciso.getCoincideEstilo();
		this.observacionesinciso = autoInciso.getObservacionesinciso();
		this.codigoPostal = autoInciso.getCodigoPostal();
    }	
    
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOINCISOAUTOCOT_ID_GENERATOR")	
	@SequenceGenerator(name="TOINCISOAUTOCOT_ID_GENERATOR", sequenceName="MIDAS.TOINCISOAUTOCOT_SEQ", allocationSize=1)
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="ESTILO_ID")
	public String getEstiloId() {
		return this.estiloId;
	}

	public void setEstiloId(String estiloId) {
		this.estiloId = estiloId;
	}

	@Column(name="CLAVETIPOBIEN")
	public String getClaveTipoBien() {
		return this.claveTipoBien;
	}

	public void setClaveTipoBien(String claveTipoBien) {
		this.claveTipoBien = claveTipoBien;
	}

	@Column(name="ESTADO_ID")
	public String getEstadoId() {
		return this.estadoId;
	}

	public void setEstadoId(String estadoId) {
		this.estadoId = estadoId;
	}

	@Column(name="IDMONEDA")
	public Short getIdMoneda() {
		return this.idMoneda;
	}

	public void setIdMoneda(Short idMoneda) {
		this.idMoneda = idMoneda;
	}

	@Column(name="IDVERSIONCARGA")
	public BigDecimal getIdVersionCarga() {
		return this.idVersionCarga;
	}

	public void setIdVersionCarga(BigDecimal idVersionCarga) {
		this.idVersionCarga = idVersionCarga;
	}

	@Column(name="MODELOVEHICULO")
	public Short getModeloVehiculo() {
		return this.modeloVehiculo;
	}

	public void setModeloVehiculo(Short modeloVehiculo) {
		this.modeloVehiculo = modeloVehiculo;
	}

	@Column(name="MODIFICADORESDESCRIPCION")
	public String getModificadoresDescripcion() {
		return this.modificadoresDescripcion;
	}

	public void setModificadoresDescripcion(String modificadoresDescripcion) {
		this.modificadoresDescripcion = modificadoresDescripcion;
	}

	@Column(name="MODIFICADORESPRIMA")
	public String getModificadoresPrima() {
		return this.modificadoresPrima;
	}

	public void setModificadoresPrima(String modificadoresPrima) {
		this.modificadoresPrima = modificadoresPrima;
	}

	@Column(name="MUNICIPIO_ID")
	public String getMunicipioId() {
		return this.municipioId;
	}

	public void setMunicipioId(String municipioId) {
		this.municipioId = municipioId;
	}

	@OneToOne(fetch = FetchType.EAGER)
	  @JoinColumns({@JoinColumn(name = "NUMEROINCISO", referencedColumnName="NUMEROINCISO"),
    	@JoinColumn(name = "COTIZACION_ID", referencedColumnName="IDTOCOTIZACION")})
	public IncisoCotizacionDTO getIncisoCotizacionDTO() {
		return incisoCotizacionDTO;
	}

	public void setIncisoCotizacionDTO(IncisoCotizacionDTO incisoCotizacionDTO) {
		this.incisoCotizacionDTO = incisoCotizacionDTO;
	}
	
	
	@Column(name="NUMEROMOTOR")
	@Size(max=30,message="Caracteres permitidos 30")
	public String getNumeroMotor() {
		return this.numeroMotor;
	}

	public void setNumeroMotor(String numeroMotor) {
		this.numeroMotor = numeroMotor;
	}

	@Column(name="NUMEROSERIE")
	@Size(max=30,message="Caracteres permitidos 30")
	public String getNumeroSerie() {
		return this.numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "PAQUETE_ID", referencedColumnName="IDPAQUETE")
	public Paquete getPaquete() {
		return this.paquete;
	}

	public void setPaquete(Paquete paquete) {
		this.paquete = paquete;
	}

	@Column(name="MARCA_ID")
	public BigDecimal getMarcaId() {
		return marcaId;
	}

	public void setMarcaId(BigDecimal marcaId) {
		this.marcaId = marcaId;
	}

	@Column(name="DESCRIPCION_FINAL")
	public String getDescripcionFinal() {
		return descripcionFinal;
	}

	public void setDescripcionFinal(String descripcionFinal) {
		this.descripcionFinal = descripcionFinal;
	}

	@Column(name="PLACA")
	@Size(max=12,message="Caracteres permitidos 12")
	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		return this.descripcionFinal;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Column(name="REPUVE")
	@Size(min=0,max=10,message="Caracteres permitidos 10")
	public String getRepuve() {
		return repuve;
	}

	public void setRepuve(String repuve) {
		this.repuve = repuve;
	}

	@Column(name="NEGOCIOSECCION_ID")
	public Long getNegocioSeccionId() {
		return negocioSeccionId;
	}

	
	public void setNegocioSeccionId(Long negocioSeccionId) {
		this.negocioSeccionId = negocioSeccionId;
	}
	
	@Column(name="NEGOCIOPAQUETE_ID")
	public Long getNegocioPaqueteId() {
		return negocioPaqueteId;
	}

	public void setNegocioPaqueteId(Long negocioPaqueteId) {
		this.negocioPaqueteId = negocioPaqueteId;
	}

	@Column(name="TIPOUSO_ID")
	public Long getTipoUsoId() {
		return tipoUsoId;
	}

	public void setTipoUsoId(Long tipoUsoId) {
		this.tipoUsoId = tipoUsoId;
	}

	@Column(name="NUMEROLICENCIA")
	public String getNumeroLicencia() {
		return numeroLicencia;
	}
	
	public void setNumeroLicencia(String numeroLicencia) {
		this.numeroLicencia = numeroLicencia;
	}

	@Column(name="NOMBRECONDUCTOR")
	public String getNombreConductor() {
		return nombreConductor;
	}
	
	public void setNombreConductor(String nombreConductor) {
		this.nombreConductor = nombreConductor;
	}

	@Column(name="PATERNOCONDUCTOR")
	public String getPaternoConductor() {
		return paternoConductor;
	}

	public void setPaternoConductor(String paternoConductor) {
		this.paternoConductor = paternoConductor;
	}

	@Column(name="MATERNOCONDUCTOR")
	public String getMaternoConductor() {
		return maternoConductor;
	}
	
	public void setMaternoConductor(String maternoConductor) {
		this.maternoConductor = maternoConductor;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHANACCONDUCTOR", length = 7) 
	public Date getFechaNacConductor() {
		return fechaNacConductor;
	}

	public void setFechaNacConductor(Date fechaNacConductor) {
		this.fechaNacConductor = fechaNacConductor;
	}

	@Column(name="OCUPACIONCONDUCTOR")
	public String getOcupacionConductor() {
		return ocupacionConductor;
	}

	public void setOcupacionConductor(String ocupacionConductor) {
		this.ocupacionConductor = ocupacionConductor;
	}
	
	@Column(name="ASOCIADA_COTIZACION")
	public Integer getAsociadaCotizacion() {
		return asociadaCotizacion;
	}

	public void setAsociadaCotizacion(Integer asociadaCotizacion) {
		this.asociadaCotizacion = asociadaCotizacion;
	}
	
	@Column(name="PERSONAASEGURADO_ID")
    public Long getPersonaAseguradoId() {
		return personaAseguradoId;
	}

	public void setPersonaAseguradoId(Long personaAseguradoId) {
		this.personaAseguradoId = personaAseguradoId;
	}

	@Column(name="NOMBREASEGURADO")
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}

	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}
	
	@Transient
	public String getNombreCompletoConductor(){
		if(this.nombreConductor != null || this.paternoConductor != null || this.maternoConductor!=null){
			return this.nombreConductor+" "+this.paternoConductor+" "+this.maternoConductor;
		}else{
			return new String("");
		}
	}
	
	@Transient
	public String getNombreAseguradoUpper() {
		return this.nombreAsegurado!=null?this.nombreAsegurado.toUpperCase():this.nombreAsegurado;
	}

	@Column(name="OBSERVACIONESINCISO")
	public String getObservacionesinciso() {
		return observacionesinciso;
	}

	public void setObservacionesinciso(String observacionesinciso) {
		this.observacionesinciso = observacionesinciso;
	}

	@Column(name="TIPOSERVICIO_ID")
	public BigDecimal getTipoServicioId() {
		return tipoServicioId;
	}

	public void setTipoServicioId(BigDecimal tipoServicioId) {
		this.tipoServicioId = tipoServicioId;
	}

	public void setClaveSimilar(Boolean claveSimilar) {
		this.claveSimilar = claveSimilar;
	}

	@Column(name="CLAVESIMILAR")
	public Boolean getClaveSimilar() {
		return claveSimilar;
	}

	@Column(name="PCTDESCUENTOESTADO")
	public Double getPctDescuentoEstado() {
		if(pctDescuentoEstado == null) {
			pctDescuentoEstado = 0.0;
		}
		return pctDescuentoEstado;
	}

	public void setPctDescuentoEstado(Double pctDescuentoEstado) {
		this.pctDescuentoEstado = pctDescuentoEstado;
	}

	@Column(name="CLAVEAMIS")
	public String getClaveAmis() {
		return claveAmis;
	}

	public void setClaveAmis(String claveAmis) {
		this.claveAmis = claveAmis;
	}

	@Column(name="CLAVESESA")
	public String getClaveSesa() {
		return claveSesa;
	}

	public void setClaveSesa(String claveSesa) {
		this.claveSesa = claveSesa;
	}

	@Column(name="OBSERVACIONESSESA")
	public String getObservacionesSesa() {
		return observacionesSesa;
	}

	public void setObservacionesSesa(String observacionesSesa) {
		this.observacionesSesa = observacionesSesa;
	}

	public void setRutaCirculacion(String rutaCirculacion) {
		this.rutaCirculacion = rutaCirculacion;
	}

	@Column(name="RUTACIRCULACION")
	public String getRutaCirculacion() {
		return rutaCirculacion;
	}

	@Column(name="IDAGRUPADORPASAJEROS")
	public BigDecimal getIdAgrupadorPasajeros() {
		return idAgrupadorPasajeros;
	}

	public void setIdAgrupadorPasajeros(BigDecimal idAgrupadorPasajeros) {
		this.idAgrupadorPasajeros = idAgrupadorPasajeros;
	}

	@Column(name="VINVALIDO")
	public Boolean getVinValido() {
		return vinValido;
	}

	public void setVinValido(Boolean vinValido) {
		this.vinValido = vinValido;
	}

	@Column(name="COINCIDEESTILO")
	public Boolean getCoincideEstilo() {
		return coincideEstilo;
	}

	public void setCoincideEstilo(Boolean coincideEstilo) {
		this.coincideEstilo = coincideEstilo;
	}

	@Column(name="CODIGO_POSTAL")
	public Long getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(Long codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
}
