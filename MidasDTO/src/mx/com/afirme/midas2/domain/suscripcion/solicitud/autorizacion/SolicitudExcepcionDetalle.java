package mx.com.afirme.midas2.domain.suscripcion.solicitud.autorizacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;


/**
 * The persistent class for the TOSOLICITUDDETALLE database table.
 * 
 */
@Entity
@Table(name="TOSOLICITUDDETALLE", schema="MIDAS")
public class SolicitudExcepcionDetalle implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;
	private Long id;
	private String descripcion;
	private Short estatus;
	private Date fechaDetalle;	
	private String observacion;	
	private BigDecimal idSeccion;
	private BigDecimal idInciso;
	private BigDecimal idCobertura;
	private Long idExcepcion;
	private	String provocadorExcepcion;
	private String claveProvocadorExcepcion;
	private SolicitudExcepcionCotizacion solicitudExcepcionCotizacion;
	
	private Long numeroSecuencia;
	private String descripcionSeccion;
	private String descripcionInciso;
	private String descripcionCobertura;

    public SolicitudExcepcionDetalle() {
    }


	@Id
	@SequenceGenerator(name="TOSOLICITUDDETALLE_ID_GENERATOR", sequenceName="MIDAS.IDTOSOLICITUDDETALLE_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOSOLICITUDDETALLE_ID_GENERATOR")
	@Column(unique=true, nullable=false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}


    @Lob()
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	@Column(nullable=false)
	public Short getEstatus() {
		return this.estatus;
	}

	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}

	@Column(name="OBSERVACIONES")
	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	//bi-directional many-to-one association to Tosolicitudcotizacion
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SOLICITUD", nullable=false)
	public SolicitudExcepcionCotizacion getSolicitudExcepcionCotizacion() {
		return this.solicitudExcepcionCotizacion;
	}

	public void setSolicitudExcepcionCotizacion(SolicitudExcepcionCotizacion solicitudExcepcionCotizacion) {
		this.solicitudExcepcionCotizacion = solicitudExcepcionCotizacion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		// TODO Auto-generated method stub
		return id;
	}
	
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		// TODO Auto-generated method stub
		return id;
	}

	@Column(name="ID_SECCION")
	public BigDecimal getIdSeccion() {
		return idSeccion;
	}


	public void setIdSeccion(BigDecimal idSeccion) {
		this.idSeccion = idSeccion;
	}

	@Column(name="ID_INCISO")
	public BigDecimal getIdInciso() {
		return idInciso;
	}


	public void setIdInciso(BigDecimal idInciso) {
		this.idInciso = idInciso;
	}

	@Temporal( TemporalType.DATE)
	@Column(name="FECHA_CREACION", nullable=false)
	public Date getFechaDetalle() {
		return fechaDetalle;
	}


	public void setFechaDetalle(Date fechaDetalle) {
		this.fechaDetalle = fechaDetalle;
	}

	@Column(name="ID_COBERTURA")
	public BigDecimal getIdCobertura() {
		return idCobertura;
	}


	public void setIdCobertura(BigDecimal idCobertura) {
		this.idCobertura = idCobertura;
	}

	@Column(name="ID_EXCEPCION")
	public Long getIdExcepcion() {
		return idExcepcion;
	}


	public void setIdExcepcion(Long idExcepcion) {
		this.idExcepcion = idExcepcion;
	}


	public void setDescripcionSeccion(String descripcionSeccion) {
		this.descripcionSeccion = descripcionSeccion;
	}


	@Transient
	public String getDescripcionSeccion() {
		return descripcionSeccion;
	}


	public void setDescripcionInciso(String descripcionInciso) {
		this.descripcionInciso = descripcionInciso;
	}


	@Transient
	public String getDescripcionInciso() {
		return descripcionInciso;
	}


	public void setDescripcionCobertura(String descripcionCobertura) {
		this.descripcionCobertura = descripcionCobertura;
	}


	@Transient
	public String getDescripcionCobertura() {
		return descripcionCobertura;
	}


	public void setNumeroSecuencia(Long numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}

	@Transient
	public Long getNumeroSecuencia() {
		return numeroSecuencia;
	}

	@Transient
	public List<String> getListaProvocadorExcepcion(){
		if(provocadorExcepcion == null){
			return new ArrayList<String>();
		}else{
			return Arrays.asList(provocadorExcepcion.split(","));
		}
	}
	
	@Column(name="PROVOCADOR_EXCEPCION")
	public String getProvocadorExcepcion() {
		return provocadorExcepcion;
	}


	public void setProvocadorExcepcion(String provocadorExcepcion) {
		this.provocadorExcepcion = provocadorExcepcion;
	}

	@Column(name="CLAVE_PROVOCADOR_EXCEPCION")
	public String getClaveProvocadorExcepcion() {
		return claveProvocadorExcepcion;
	}


	public void setClaveProvocadorExcepcion(String claveProvocadorExcepcion) {
		this.claveProvocadorExcepcion = claveProvocadorExcepcion;
	}
	
	
}