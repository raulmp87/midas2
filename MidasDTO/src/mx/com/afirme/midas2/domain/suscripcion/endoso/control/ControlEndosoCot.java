package mx.com.afirme.midas2.domain.suscripcion.endoso.control;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="CONTROLENDOSOCOT",schema="MIDAS")
public class ControlEndosoCot implements Serializable,Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1727541274848142972L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDCONTROLENDOSOCOT_SEQ")
	@SequenceGenerator(name = "IDCONTROLENDOSOCOT_SEQ", sequenceName = "MIDAS.IDCONTROLENDOSOCOT_SEQ", allocationSize = 1)
	private Long id;
	
	@Column(name="COTIZACION_ID")
	private Long cotizacionId;
	
	@Column(name="CLAVE_ESTATUS_COT")
	private Long claveEstatusCot;
	
	@OneToOne
	@JoinColumn(name="SOLICITUD_ID", referencedColumnName="IDTOSOLICITUD")
	private SolicitudDTO solicitud;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="VALIDFROM")
	private Date validFrom;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="RECORDFROM")
	private Date recordFrom;

	@Column(name="IDCANCELA")
	private Long idCancela;
	
	@Column(name="NUMENDOSOAJUSTA")
	private Short numeroEndosoAjusta;
	
	@Column(name="PRIMATOTALIGUALAR")
	private BigDecimal primaTotalIgualar;
	
	@Transient
	private Boolean migrada;
	
	@Column(name="PORCENTAJERPF") 
	private Double porcentajePagoFraccionado;
		
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCotizacionId() {
		return cotizacionId;
	}

	public void setCotizacionId(Long cotizacionId) {
		this.cotizacionId = cotizacionId;
	}

	public Long getClaveEstatusCot() {
		return claveEstatusCot;
	}

	public void setClaveEstatusCot(Long claveEstatusCot) {
		this.claveEstatusCot = claveEstatusCot;
	}

	public SolicitudDTO getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(SolicitudDTO solicitud) {
		this.solicitud = solicitud;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getRecordFrom() {
		return recordFrom;
	}

	public void setRecordFrom(Date recordFrom) {
		this.recordFrom = recordFrom;
	}
	
	public Long getIdCancela() {
		return idCancela;
	}

	public void setIdCancela(Long idCancela) {
		this.idCancela = idCancela;
	}
	
	public Boolean getMigrada() {
		return migrada;
	}

	public void setMigrada(Boolean migrada) {
		this.migrada = migrada;
	}

	public Short getNumeroEndosoAjusta() {
		return numeroEndosoAjusta;
	}

	public void setNumeroEndosoAjusta(Short numeroEndosoAjusta) {
		this.numeroEndosoAjusta = numeroEndosoAjusta;
	}

	public BigDecimal getPrimaTotalIgualar() {
		return primaTotalIgualar;
	}

	public void setPrimaTotalIgualar(BigDecimal primaTotalIgualar) {
		this.primaTotalIgualar = primaTotalIgualar;
	}
	
	public Double getPorcentajePagoFraccionado() {
		return porcentajePagoFraccionado;
	}

	public void setPorcentajePagoFraccionado(Double porcentajePagoFraccionado) {
		this.porcentajePagoFraccionado = porcentajePagoFraccionado;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return getId();
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return null;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("id: " + this.id + ", ");
		sb.append("cotizacionId: " + this.cotizacionId + ", ");
		sb.append("IdToSolicitud: " + this.getSolicitud().getIdToSolicitud());
		sb.append("]");
		
		return sb.toString();
	}
}
