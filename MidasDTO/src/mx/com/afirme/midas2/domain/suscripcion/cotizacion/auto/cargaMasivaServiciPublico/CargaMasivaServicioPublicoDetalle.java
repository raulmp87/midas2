package mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;

@Entity
@Table(name = "TOARCHIVOCARGAMASIVASPDETALLE", schema = "MIDAS")
public class CargaMasivaServicioPublicoDetalle extends MidasAbstracto implements Entidad{

	private static final long serialVersionUID = -2848054585353649291L;

	public enum EstatusDetalle{
		POR_EMITIR,
		POR_VALIDAR,
		EMITIDA,
		ERROR,
		AVISO,
		ERROR_COTIZACION,
		POR_AUTORIZAR,
		ERROR_EMISION,
		ERROR_POLIZA
	}
	
	/***************************************DECLARA ATRIBUTOS Y RELACIÓN ENTIDAD BD*******************************************/
	@Id
	@SequenceGenerator(name = "TOARCHIVOCARGAMASIVASPDETALLE_GENERATOR_ID", sequenceName = "TOARCHCARMASSPDET_ID_SEQ", allocationSize=1, schema="MIDAS")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TOARCHIVOCARGAMASIVASPDETALLE_GENERATOR_ID")
	@Column(name = "ID")
	private Long idDetalleCargaMasiva;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TOARCHIVOCARGAMASIVASP_ID", nullable = false)
	private CargaMasivaServicioPublico archivoCargaMasivaSP_ID = new CargaMasivaServicioPublico();
	
	@Column(name="ESTATUS")
	private String estatus;
	
	@Column(name="IDNEGOCIO")
	private Long idNegocio;
	
	@Column(name="IDPRODUCTO")
	private Long idProducto;
	
	@Column(name="IDTIPOPOLIZA")
	private Long idTipoPoliza;
	
	@Column(name="IDAGENTE")
	private Long idAgente;
	
	@Column(name="IDPAQUETE")
	private Long idPaquete;
	
	@Column(name="IDCENTROEMISOR")
	private Long idCentroEmisor;
	
	@Column(name="IDOFICINA")
	private Long idOficina;
	
	@Column(name="NUMTOPOLIZA")
	private Long numToPoliza;
	
	@Column(name="NUMLIQUIDACION")
	private Long numLiquidacion;
	
	@Column(name="AUTORIZACIONPROSA")
	private String autorizacionProsa;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAVIGENCIA")
	private Date fechaVigencia;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAEMISION")
	private Date fechaEmision;
	
	@Column(name="VIGENCIA")
	private Long vigencia;
	
	@Column(name="TIPOPERSONACLIENTE")
	private String tipoPersonaCliente;
	
	@Column(name="RFCCLIENTE")
	private String RFCCliente;
	
	@Column(name="NOMBRERAZONSOCIAL")
	private String nombreRazonSocial;
	
	@Column(name="APELLIDOPATERNO")
	private String apellidoPaterno;
	
	@Column(name="APELLIDOMATERNO")
	private String apellidoMaterno;
	
	@Column(name="CODIGOPOSTALCLIENTE")
	private String codigoPostalCliente;
	
	@Column(name="CALLECLIENTE")
	private String calleCliente;
	
	@Column(name="COLONIACLIENTE")
	private String coloniaCliente;
	
	@Column(name="TELEFONOCLIENTE")
	private String telefonoCliente;
	
	@Column(name="CLAVEAMIS")
	private String claveAMIS;
	
	@Column(name="MODELO")
	private Long modelo;
	
	@Column(name="NCIREPUVE")
	private String ncRepuve;
	
	@Column(name="CLAVEUSO")
	private String claveUso;
	
	@Column(name="PLACAS")
	private String placas;
	
	@Column(name="NUMEROMOTOR")
	private String numeroMotor;
	
	@Column(name="NUMEROSERIE")
	private String numeroSerie;
	
	@Column(name="VALORCOMERCIAL")
	private Double valorComercial;
	
	@Column(name="TOTALPASAJEROS")
	private Long totalPasajeros;
	
	@Column(name="DESCRIPCIONVEHICULO")
	private String descripcionVehiculo;
	
	@Column(name="DERROTERO")
	private String derrotero;
	
	@Column(name="RAMAL")
	private String ramal;
	
	@Column(name="RUTA")
	private String ruta;
	
	@Column(name="NUMEROECONOMICO")
	private String numeroEconomico;
	
	@Column(name="PRIMATOTAL")
	private Double primaTotal;
	
	@Column(name="DEDUCIBLEDANOSMATERIALES")
	private Double deducibleDanosMateriales;
	
	@Column(name="DEDUCIBLEROBOTOTAL")
	private Double deducibleRoboTotal;
	
	@Column(name="LIMITERCTERCEROS")
	private Double limiteRcTerceros;
	
	@Column(name="DEDUCIBLERCTERCEROS")
	private Double deducibleRcTerceros;
	
	@Column(name="LIMITEGASTOSMEDICOS")
	private Double limiteGastosMedicos;
	
	@Column(name="LIMITEMUERTE")
	private Double limiteMuerte;
	
	@Column(name="LIMITERCVIAJERO")
	private Double limiteRcViajero;
	
	@Column(name="DEDUCIBLERCV")
	private Double deducibleRCV;
	
	@Column(name="ASISTENCIAJURIDICA")
	private String asistenciaJuridica;
	
	@Column(name="LIMITEEQUIPOESPECIAL")
	private Double limiteEquipoEspecial;
	
	@Column(name="ASISTENCIAVIAL")
	private String asistenciaVial;
	
	@Column(name="IGUALACION")
	private Long igualacion;
	
	@Column(name="DERECHOS")
	private Double derechos;
	
	@Column(name="SOLICITARAUTORIZACION")
	private Long solicitarAutorizacion;
	
	@Column(name="CAUSAAUTORIZACION")
	private String causaAutorizacion;
	
	@Column(name="LINEANEGOCIO")
	private String lineaNegocio;
	
	@Column(name="ANUALMESESSININTERESES")
	private Long anualMesesSinIntereses;
	
	@Column(name="ESTATUSPROCESO")
	private String estatusProceso;
	
	@Column(name="DESCRIESTATUSPROCESO")
	private String descripcionEstatusProceso;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "detalle")
	private CargaMasivaServicioPublicoDetalleTradSeyMid cargaMasivaSerPubDetalleTradSeyMid = new CargaMasivaServicioPublicoDetalleTradSeyMid();
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="IDTOCOTIZACION", nullable=false, referencedColumnName="IDTOCOTIZACION")
	private CotizacionDTO cotizacion;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="IDTOPOLIZA", nullable=false, referencedColumnName="IDTOPOLIZA")
	private PolizaDTO poliza;
	
	@Transient
	private Boolean existePolizaSeycos;
	
	@Transient
	private Long exitePolizaSeycosNumero; //Para recibir la respuesta del procedure de validacion de poliza en seycos. 0 = false, 1 = true
	
	@Transient
	private Date fechaFinVigencia;
	
	/****************************SET & GET DE ATRIBUTOS********************************/

	public Long getIdDetalleCargaMasiva() {
		return idDetalleCargaMasiva;
	}
	public void setIdDetalleCargaMasiva(Long idDetalleCargaMasiva) {
		this.idDetalleCargaMasiva = idDetalleCargaMasiva;
	}

	public CargaMasivaServicioPublico getArchivoCargaMasivaSP_ID() {
		return archivoCargaMasivaSP_ID;
	}
	public void setArchivoCargaMasivaSP_ID(
			CargaMasivaServicioPublico archivoCargaMasivaSP_ID) {
		this.archivoCargaMasivaSP_ID = archivoCargaMasivaSP_ID;
	}

	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public Long getIdNegocio() {
		return idNegocio;
	}
	public void setIdNegocio(Long idNegocio) {
		this.idNegocio = idNegocio;
	}
	public Long getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}
	public Long getIdTipoPoliza() {
		return idTipoPoliza;
	}
	public void setIdTipoPoliza(Long idTipoPoliza) {
		this.idTipoPoliza = idTipoPoliza;
	}
	public Long getIdAgente() {
		return idAgente;
	}
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}

	public Long getIdPaquete() {
		return idPaquete;
	}
	public void setIdPaquete(Long idPaquete) {
		this.idPaquete = idPaquete;
	}

	public Long getIdCentroEmisor() {
		return idCentroEmisor;
	}
	public void setIdCentroEmisor(Long idCentroEmisor) {
		this.idCentroEmisor = idCentroEmisor;
	}

	public Long getIdOficina() {
		return idOficina;
	}
	public void setIdOficina(Long idOficina) {
		this.idOficina = idOficina;
	}

	public Long getNumToPoliza() {
		return numToPoliza;
	}
	public void setNumToPoliza(Long numToPoliza) {
		this.numToPoliza = numToPoliza;
	}

	public Long getNumLiquidacion() {
		return numLiquidacion;
	}
	public void setNumLiquidacion(Long numLiquidacion) {
		this.numLiquidacion = numLiquidacion;
	}

	public String getAutorizacionProsa() {
		return autorizacionProsa;
	}
	public void setAutorizacionProsa(String autorizacionProsa) {
		this.autorizacionProsa = autorizacionProsa;
	}

	public Date getFechaVigencia() {
		return fechaVigencia;
	}
	public void setFechaVigencia(Date fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}

	public Date getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public Long getVigencia() {
		return vigencia;
	}
	public void setVigencia(Long vigencia) {
		this.vigencia = vigencia;
	}

	public String getTipoPersonaCliente() {
		return tipoPersonaCliente;
	}
	public void setTipoPersonaCliente(String tipoPersonaCliente) {
		this.tipoPersonaCliente = tipoPersonaCliente;
	}

	public String getRFCCliente() {
		return RFCCliente;
	}
	public void setRFCCliente(String rFCCliente) {
		RFCCliente = rFCCliente;
	}

	public String getNombreRazonSocial() {
		return nombreRazonSocial;
	}
	public void setNombreRazonSocial(String nombreRazonSocial) {
		this.nombreRazonSocial = nombreRazonSocial;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getCodigoPostalCliente() {
		return codigoPostalCliente;
	}
	public void setCodigoPostalCliente(String codigoPostalCliente) {
		this.codigoPostalCliente = codigoPostalCliente;
	}

	public String getCalleCliente() {
		return calleCliente;
	}
	public void setCalleCliente(String calleCliente) {
		this.calleCliente = calleCliente;
	}

	public String getColoniaCliente() {
		return coloniaCliente;
	}
	public void setColoniaCliente(String coloniaCliente) {
		this.coloniaCliente = coloniaCliente;
	}

	public String getTelefonoCliente() {
		return telefonoCliente;
	}
	public void setTelefonoCliente(String telefonoCliente) {
		this.telefonoCliente = telefonoCliente;
	}

	public String getClaveAMIS() {
		return claveAMIS;
	}
	public void setClaveAMIS(String claveAMIS) {
		this.claveAMIS = claveAMIS;
	}

	public Long getModelo() {
		return modelo;
	}
	public void setModelo(Long modelo) {
		this.modelo = modelo;
	}

	public String getNcRepuve() {
		return ncRepuve;
	}
	public void setNcRepuve(String ncRepuve) {
		this.ncRepuve = ncRepuve;
	}

	public String getClaveUso() {
		return claveUso;
	}
	public void setClaveUso(String claveUso) {
		this.claveUso = claveUso;
	}

	public String getPlacas() {
		return placas;
	}
	public void setPlacas(String placas) {
		this.placas = placas;
	}

	public String getNumeroMotor() {
		return numeroMotor;
	}
	public void setNumeroMotor(String numeroMotor) {
		this.numeroMotor = numeroMotor;
	}

	public String getNumeroSerie() {
		return numeroSerie;
	}
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public Double getValorComercial() {
		return valorComercial;
	}
	public void setValorComercial(Double valorComercial) {
		this.valorComercial = valorComercial;
	}

	public Long getTotalPasajeros() {
		return totalPasajeros;
	}
	public void setTotalPasajeros(Long totalPasajeros) {
		this.totalPasajeros = totalPasajeros;
	}

	public String getDescripcionVehiculo() {
		return descripcionVehiculo;
	}
	public void setDescripcionVehiculo(String descripcionVehiculo) {
		this.descripcionVehiculo = descripcionVehiculo;
	}

	public String getDerrotero() {
		return derrotero;
	}
	public void setDerrotero(String derrotero) {
		this.derrotero = derrotero;
	}

	public String getRamal() {
		return ramal;
	}
	public void setRamal(String ramal) {
		this.ramal = ramal;
	}

	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public String getNumeroEconomico() {
		return numeroEconomico;
	}
	public void setNumeroEconomico(String numeroEconomico) {
		this.numeroEconomico = numeroEconomico;
	}

	public Double getPrimaTotal() {
		return primaTotal;
	}
	public void setPrimaTotal(Double primaTotal) {
		this.primaTotal = primaTotal;
	}

	public Double getDeducibleDanosMateriales() {
		return deducibleDanosMateriales;
	}
	public void setDeducibleDanosMateriales(Double deducibleDanosMateriales) {
		this.deducibleDanosMateriales = deducibleDanosMateriales;
	}

	public Double getDeducibleRoboTotal() {
		return deducibleRoboTotal;
	}
	public void setDeducibleRoboTotal(Double deducibleRoboTotal) {
		this.deducibleRoboTotal = deducibleRoboTotal;
	}

	public Double getLimiteRcTerceros() {
		return limiteRcTerceros;
	}
	public void setLimiteRcTerceros(Double limiteRcTerceros) {
		this.limiteRcTerceros = limiteRcTerceros;
	}

	public Double getDeducibleRcTerceros() {
		return deducibleRcTerceros;
	}
	public void setDeducibleRcTerceros(Double deducibleRcTerceros) {
		this.deducibleRcTerceros = deducibleRcTerceros;
	}

	public Double getLimiteGastosMedicos() {
		return limiteGastosMedicos;
	}
	public void setLimiteGastosMedicos(Double limiteGastosMedicos) {
		this.limiteGastosMedicos = limiteGastosMedicos;
	}

	public Double getLimiteMuerte() {
		return limiteMuerte;
	}
	public void setLimiteMuerte(Double limiteMuerte) {
		this.limiteMuerte = limiteMuerte;
	}

	public Double getLimiteRcViajero() {
		return limiteRcViajero;
	}
	public void setLimiteRcViajero(Double limiteRcViajero) {
		this.limiteRcViajero = limiteRcViajero;
	}

	public Double getDeducibleRCV() {
		return deducibleRCV;
	}
	public void setDeducibleRCV(Double deducibleRCV) {
		this.deducibleRCV = deducibleRCV;
	}

	public String getAsistenciaJuridica() {
		return asistenciaJuridica;
	}
	public void setAsistenciaJuridica(String asistenciaJuridica) {
		this.asistenciaJuridica = asistenciaJuridica;
	}

	public Double getLimiteEquipoEspecial() {
		return limiteEquipoEspecial;
	}

	public void setLimiteEquipoEspecial(Double limiteEquipoEspecial) {
		this.limiteEquipoEspecial = limiteEquipoEspecial;
	}

	public String getAsistenciaVial() {
		return asistenciaVial;
	}
	public void setAsistenciaVial(String asistenciaVial) {
		this.asistenciaVial = asistenciaVial;
	}

	public Long getIgualacion() {
		return igualacion;
	}
	public void setIgualacion(Long igualacion) {
		this.igualacion = igualacion;
	}

	public Double getDerechos() {
		return derechos;
	}
	public void setDerechos(Double drechos) {
		this.derechos = drechos;
	}

	public Long getSolicitarAutorizacion() {
		return solicitarAutorizacion;
	}
	public void setSolicitarAutorizacion(Long solicitarAutorizacion) {
		this.solicitarAutorizacion = solicitarAutorizacion;
	}

	public String getCausaAutorizacion() {
		return causaAutorizacion;
	}
	public void setCausaAutorizacion(String causaAutorizacion) {
		this.causaAutorizacion = causaAutorizacion;
	}

	public String getLineaNegocio() {
		return lineaNegocio;
	}
	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}

	public Long getAnualMesesSinIntereses() {
		return anualMesesSinIntereses;
	}
	public void setAnualMesesSinIntereses(Long anualMesesSinIntereses) {
		this.anualMesesSinIntereses = anualMesesSinIntereses;
	}

	public String getEstatusProceso() {
		return estatusProceso;
	}
	public void setEstatusProceso(String estatusProceso) {
		this.estatusProceso = estatusProceso;
	}

	public String getDescripcionEstatusProceso() {
		return descripcionEstatusProceso;
	}
	public void setDescripcionEstatusProceso(String descripcionEstatusProceso) {
		this.descripcionEstatusProceso = descripcionEstatusProceso;
	}

	public CargaMasivaServicioPublicoDetalleTradSeyMid getCargaMasivaSerPubDetalleTradSeyMid() {
		return cargaMasivaSerPubDetalleTradSeyMid;
	}
	public void setCargaMasivaSerPubDetalleTradSeyMid(
			CargaMasivaServicioPublicoDetalleTradSeyMid cargaMasivaSerPubDetalleTradSeyMid) {
		this.cargaMasivaSerPubDetalleTradSeyMid = cargaMasivaSerPubDetalleTradSeyMid;
	}
	
	public Boolean getExistePolizaSeycos() {
		return existePolizaSeycos;
	}
	public void setExistePolizaSeycos(Boolean existePolizaSeycos) {
		this.existePolizaSeycos = existePolizaSeycos;
	}
	
	public Long getExitePolizaSeycosNumero() {
		return exitePolizaSeycosNumero;
	}
	public void setExitePolizaSeycosNumero(Long exitePolizaSeycosNumero) {
		this.exitePolizaSeycosNumero = exitePolizaSeycosNumero;
	}
	
	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}
	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}
	
	
	public CotizacionDTO getCotizacion() {
		return cotizacion;
	}
	public void setCotizacion(CotizacionDTO cotizacion) {
		this.cotizacion = cotizacion;
	}
	public PolizaDTO getPoliza() {
		return poliza;
	}
	public void setPoliza(PolizaDTO poliza) {
		this.poliza = poliza;
	}
	/************************************IMPLEMENTADOS DE Entidad*****************************************/
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.idDetalleCargaMasiva;
	}

	@Override
	public String getValue() {
		return this.numToPoliza.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getBusinessKey() {
		return this.numToPoliza.toString();
	}
}
