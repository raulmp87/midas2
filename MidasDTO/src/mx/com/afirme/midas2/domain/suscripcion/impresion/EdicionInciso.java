package mx.com.afirme.midas2.domain.suscripcion.impresion;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;

/**
 * Clase para almacenar la informacion que se desea modificar en la impresion 
 * de los incisos. Estos cambios no afectan la informacion del
 * sistema, unicamente son modificaciones para la impresion. 
 * 
 */
@Entity
@Table(name="TOEDICIONINCISO", schema="MIDAS")
public class EdicionInciso extends MidasAbstracto implements Entidad{

	private static final long serialVersionUID = -4386594004126207367L;
	
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToPoliza", column = @Column(name = "IDTOPOLIZA", nullable = false)),
			@AttributeOverride(name = "validOn", column = @Column(name = "VALID_ON", nullable = false)),
			@AttributeOverride(name = "recordFrom", column = @Column(name = "RECORD_FROM", nullable = false)),
			@AttributeOverride(name = "claveTipoEndoso", column = @Column(name = "TIPO_ENDOSO", nullable = false)),
			@AttributeOverride(name = "version", column = @Column(name = "VERSION", nullable = false)) })
	private EdicionIncisoId id;
	
	
	@Column(name="HORA_INICIO_VIGENCIA")
	private String 		horaInicioVigencia;
	
	@Column(name="HORA_FIN_VIGENCIA")
	private String 		horaFinVigencia;
	
	@Column(name="NOMBRE_CONTRATANTE")
	private String 		nombreContratante;
	
	@Column(name="NOMBRE_CONTRATANTE_INCISO")
	private String 		nombreContratanteInciso;
	
	@Column(name="INFORMACION_ADICIONAL1")
	private String 		informacionAdicional1;
	
	@Column(name="INFORMACION_ADICIONAL2")
	private String 		informacionAdicional2;
	
	@Column(name="CALLE_NUMERO")
	private String 		calleNumero;
	
	@Column(name="COLONIA")
	private String 		colonia;
	
	@Column(name="CIUDAD")
	private String 		ciudad;
	
	@Column(name="ESTADO")
	private String 		estado;
	
	@Column(name="CODIGO_POSTAL")
	private String 		codigoPostal;
	
	@Column(name="RFC_CONTRATANTE")
	private String 		rfcContratante;
	
	@Column(name="PERSONA_CONTRATANTE_ID")
	private BigDecimal 	idToPersonaContratante;
	
	@Column(name="IMPRIMIR_PRIMAS")
	private Boolean 	imprimirPrimas = Boolean.TRUE;
	
	@Column(name="MONTO_IVA")
	private Double 		montoIva;
	
	@Column(name="OBSERVACIONES")
	private String 		observaciones;	//303
	
	@Column(name="TIPO_SERVICIO")
	private String		tipoServicio;
	
	@Column(name="TIPO_USO")
	private String		tipoUso;
	
	//PARAMETERS
	@Column(name="DATOS_AGENTE_CARATULA")
	private String 		datosAgenteCaratula;
	
	@Column(name="TELEFONO1")
	private String		telefono1;
	
	@Column(name="DESC_TELEFONO1")
	private String		descTelefono1;
	
	@Column(name="TELEFONO2")
	private String		telefono2;
	
	@Column(name="DESC_TELEFONO2")
	private String		descTelefono2;
	
	@Column(name="TELEFONO3")
	private String		telefono3;
	
	@Column(name="DESC_TELEFONO3")
	private String		descTelefono3;
	
	@Column(name="IMPRIMIR_AGENTE")
	private Boolean imprimirAgenteCaratula = Boolean.TRUE;
	
	@OneToMany(mappedBy="edicionInciso", fetch = FetchType.LAZY)
	private List<EdicionIncisoCobertura> coberturas;

	@SuppressWarnings("unchecked")
	@Override
	public EdicionIncisoId getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public EdicionIncisoId getBusinessKey() {
		return id;
	}

	public EdicionIncisoId getId() {
		return id;
	}

	public void setId(EdicionIncisoId id) {
		this.id = id;
	}

	public String getNombreContratante() {
		return nombreContratante;
	}

	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}

	public String getNombreContratanteInciso() {
		return nombreContratanteInciso;
	}

	public void setNombreContratanteInciso(String nombreContratanteInciso) {
		this.nombreContratanteInciso = nombreContratanteInciso;
	}

	public String getInformacionAdicional1() {
		return informacionAdicional1;
	}

	public void setInformacionAdicional1(String informacionAdicional1) {
		this.informacionAdicional1 = informacionAdicional1;
	}

	public String getInformacionAdicional2() {
		return informacionAdicional2;
	}

	public void setInformacionAdicional2(String informacionAdicional2) {
		this.informacionAdicional2 = informacionAdicional2;
	}

	public String getCalleNumero() {
		return calleNumero;
	}

	public void setCalleNumero(String calleNumero) {
		this.calleNumero = calleNumero;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getRfcContratante() {
		return rfcContratante;
	}

	public void setRfcContratante(String rfcContratante) {
		this.rfcContratante = rfcContratante;
	}

	public BigDecimal getIdToPersonaContratante() {
		return idToPersonaContratante;
	}

	public void setIdToPersonaContratante(BigDecimal idToPersonaContratante) {
		this.idToPersonaContratante = idToPersonaContratante;
	}

	public Boolean getImprimirPrimas() {
		return imprimirPrimas;
	}

	public void setImprimirPrimas(Boolean imprimirPrimas) {
		this.imprimirPrimas = imprimirPrimas;
	}

	public Double getMontoIva() {
		return montoIva;
	}

	public void setMontoIva(Double montoIva) {
		this.montoIva = montoIva;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getDatosAgenteCaratula() {
		return datosAgenteCaratula;
	}

	public void setDatosAgenteCaratula(String datosAgenteCaratula) {
		this.datosAgenteCaratula = datosAgenteCaratula;
	}

	public List<EdicionIncisoCobertura> getCoberturas() {
		return coberturas;
	}

	public void setCoberturas(List<EdicionIncisoCobertura> coberturas) {
		this.coberturas = coberturas;
	}

	public String getHoraInicioVigencia() {
		return horaInicioVigencia;
	}

	public void setHoraInicioVigencia(String horaInicioVigencia) {
		this.horaInicioVigencia = horaInicioVigencia;
	}

	public String getHoraFinVigencia() {
		return horaFinVigencia;
	}

	public void setHoraFinVigencia(String horaFinVigencia) {
		this.horaFinVigencia = horaFinVigencia;
	}

	public String getTipoServicio() {
		return tipoServicio;
	}

	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

	public String getTipoUso() {
		return tipoUso;
	}

	public void setTipoUso(String tipoUso) {
		this.tipoUso = tipoUso;
	}

	public String getTelefono1() {
		return telefono1;
	}

	public void setTelefono1(String telefono1) {
		this.telefono1 = telefono1;
	}

	public String getDescTelefono1() {
		return descTelefono1;
	}

	public void setDescTelefono1(String descTelefono1) {
		this.descTelefono1 = descTelefono1;
	}

	public String getTelefono2() {
		return telefono2;
	}

	public void setTelefono2(String telefono2) {
		this.telefono2 = telefono2;
	}

	public String getDescTelefono2() {
		return descTelefono2;
	}

	public void setDescTelefono2(String descTelefono2) {
		this.descTelefono2 = descTelefono2;
	}

	public String getTelefono3() {
		return telefono3;
	}

	public void setTelefono3(String telefono3) {
		this.telefono3 = telefono3;
	}

	public String getDescTelefono3() {
		return descTelefono3;
	}

	public void setDescTelefono3(String descTelefono3) {
		this.descTelefono3 = descTelefono3;
	}

	public Boolean getImprimirAgenteCaratula() {
		return imprimirAgenteCaratula;
	}

	public void setImprimirAgenteCaratula(Boolean imprimirAgenteCaratula) {
		this.imprimirAgenteCaratula = imprimirAgenteCaratula;
	}
	
	
}
