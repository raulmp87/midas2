package mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name = "TOTRADSEYMIDCARGAMASIVASP", schema = "MIDAS")
public class CargaMasivaServicioPublicoDetalleTradSeyMid implements Entidad{

	private static final long serialVersionUID = -4526250235185671955L;

	/***************************************DECLARA ATRIBUTOS Y RELACIÓN ENTIDAD BD*******************************************/
	@Id
	@SequenceGenerator(name = "TOARCHIVOCARGAMASIVASPDETALLETRASEYMID_GENERATOR_ID", sequenceName = "TOTRADSEYMIDCARMASP_ID_SEQ", allocationSize=1, schema="MIDAS")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TOARCHIVOCARGAMASIVASPDETALLETRASEYMID_GENERATOR_ID")
	@Column(name = "ID")
	private Long idTradSeyMidCarMas;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DETALLE_ID", nullable = false)
	private CargaMasivaServicioPublicoDetalle detalle;
	
	@Column(name="PRODUCTOIDMIDAS")
	private Long productoIdMidas;
	
	@Column(name="LINEANEGIDMIDAS")
	private Long lineaNegIdMidas;	
	
	/****************************SET & GET DE ATRIBUTOS********************************/
	
	public Long getIdTradSeyMidCarMas() {
		return idTradSeyMidCarMas;
	}

	public void setIdTradSeyMidCarMas(Long idTradSeyMidCarMas) {
		this.idTradSeyMidCarMas = idTradSeyMidCarMas;
	}

	public CargaMasivaServicioPublicoDetalle getDetalle() {
		return detalle;
	}

	public void setDetalle(CargaMasivaServicioPublicoDetalle detalleId) {
		this.detalle = detalleId;
	}

	public Long getProductoIdMidas() {
		return productoIdMidas;
	}

	public void setProductoIdMidas(Long productoIdMidas) {
		this.productoIdMidas = productoIdMidas;
	}

	public Long getLineaNegIdMidas() {
		return lineaNegIdMidas;
	}

	public void setLineaNegIdMidas(Long lineaNegIdMidas) {
		this.lineaNegIdMidas = lineaNegIdMidas;
	}
	
	/************************************IMPLEMENTADOS DE Entidad*****************************************/
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.idTradSeyMidCarMas;
	}

	@Override
	public String getValue() {
		return this.detalle.getIdDetalleCargaMasiva().toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getBusinessKey() {
		return this.detalle.getIdDetalleCargaMasiva().toString();
	}

}
