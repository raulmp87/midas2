package mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento;

import java.util.ArrayList;

public class ArrayListNullAware<E> extends ArrayList<E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -306852756728678159L;

	public boolean add(E object) {
		if (object == null) return true;
		return super.add(object);
	}
}
