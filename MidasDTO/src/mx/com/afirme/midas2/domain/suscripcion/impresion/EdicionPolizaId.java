package mx.com.afirme.midas2.domain.suscripcion.impresion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class EdicionPolizaId implements Serializable{

	private static final long serialVersionUID = -6560844286484525328L;

	@Column(name="IDTOPOLIZA")
	private BigDecimal 	idToPoliza;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="VALID_ON")
	private Date		validOn;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="RECORD_FROM")
	private Date		recordFrom;
	
	@Column(name="TIPO_ENDOSO")
	private Short 		claveTipoEndoso;
	
	@Column(name="ES_SITUACION_ACTUAL")
	private Boolean		esSituacionActual;
	
	@Column(name="VERSION")
	private Long version;
	
	public EdicionPolizaId() {
		super();
	}

	public EdicionPolizaId(BigDecimal idToPoliza, Date validOn,
			Date recordFrom, Short claveTipoEndoso, Boolean esSituacionActual,
			Long version) {
		super();
		this.idToPoliza = idToPoliza;
		this.validOn = validOn;
		this.recordFrom = recordFrom;
		this.claveTipoEndoso = claveTipoEndoso;
		this.esSituacionActual = esSituacionActual;
		this.version = version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((claveTipoEndoso == null) ? 0 : claveTipoEndoso.hashCode());
		result = prime
				* result
				+ ((esSituacionActual == null) ? 0 : esSituacionActual
						.hashCode());
		result = prime * result
				+ ((idToPoliza == null) ? 0 : idToPoliza.hashCode());
		result = prime * result
				+ ((recordFrom == null) ? 0 : recordFrom.hashCode());
		result = prime * result + ((validOn == null) ? 0 : validOn.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EdicionPolizaId other = (EdicionPolizaId) obj;
		if (claveTipoEndoso == null) {
			if (other.claveTipoEndoso != null)
				return false;
		} else if (!claveTipoEndoso.equals(other.claveTipoEndoso))
			return false;
		if (esSituacionActual == null) {
			if (other.esSituacionActual != null)
				return false;
		} else if (!esSituacionActual.equals(other.esSituacionActual))
			return false;
		if (idToPoliza == null) {
			if (other.idToPoliza != null)
				return false;
		} else if (!idToPoliza.equals(other.idToPoliza))
			return false;
		if (recordFrom == null) {
			if (other.recordFrom != null)
				return false;
		} else if (!recordFrom.equals(other.recordFrom))
			return false;
		if (validOn == null) {
			if (other.validOn != null)
				return false;
		} else if (!validOn.equals(other.validOn))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	public Date getValidOn() {
		return validOn;
	}

	public void setValidOn(Date validOn) {
		this.validOn = validOn;
	}

	public Date getRecordFrom() {
		return recordFrom;
	}

	public void setRecordFrom(Date recordFrom) {
		this.recordFrom = recordFrom;
	}

	public Short getClaveTipoEndoso() {
		return claveTipoEndoso;
	}

	public void setClaveTipoEndoso(Short claveTipoEndoso) {
		this.claveTipoEndoso = claveTipoEndoso;
	}

	public Boolean getEsSituacionActual() {
		return esSituacionActual;
	}

	public void setEsSituacionActual(Boolean esSituacionActual) {
		this.esSituacionActual = esSituacionActual;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	

}
