package mx.com.afirme.midas2.domain.suscripcion.impresion;

import java.util.Map;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;

/**
 * Clase para almacenar la informacion que se desea modificar en la impresion 
 * de los endosos. Estos cambios no afectan la informacion del
 * sistema, unicamente son modificaciones para la impresion. 
 * 
 */
@Entity
@Table(name="TOEDICIONENDOSO", schema="MIDAS")
public class EdicionEndoso extends MidasAbstracto implements Entidad{

	private static final long serialVersionUID = 4501866151246009899L;
	
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToCotizacion", column = @Column(name = "IDTOCOTIZACION", nullable = false)),
			@AttributeOverride(name = "recordFrom", column = @Column(name = "RECORD_FROM", nullable = false)),
			@AttributeOverride(name = "claveTipoEndoso", column = @Column(name = "TIPO_ENDOSO", nullable = false)),
			@AttributeOverride(name = "version", column = @Column(name = "VERSION", nullable = false)) })
	private EdicionEndosoId id;
	
	@Column(name="HORA_INICIO_VIGENCIA_POLIZA")
	private String 		horaInicioVigenciaPoliza;
	
	@Column(name="HORA_FIN_VIGENCIA_POLIZA")
	private String 		horaFinVigenciaPoliza;
	
	@Column(name="HORA_INICIO_VIGENCIA_ENDOSO")
	private String 		horaInicioVigenciaEndoso;
	
	@Column(name="HORA_FIN_VIGENCIA_ENDOSO")
	private String 		horaFinVigenciaEndoso;
	
	@Transient
	private Map<String,Object> informacionImpresionEndoso;

	@SuppressWarnings("unchecked")
	@Override
	public EdicionEndosoId getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public EdicionEndosoId getBusinessKey() {
		return id;
	}

	public EdicionEndosoId getId() {
		return id;
	}

	public void setId(EdicionEndosoId id) {
		this.id = id;
	}

	public String getHoraInicioVigenciaPoliza() {
		return horaInicioVigenciaPoliza;
	}

	public void setHoraInicioVigenciaPoliza(String horaInicioVigenciaPoliza) {
		this.horaInicioVigenciaPoliza = horaInicioVigenciaPoliza;
	}

	public String getHoraFinVigenciaPoliza() {
		return horaFinVigenciaPoliza;
	}

	public void setHoraFinVigenciaPoliza(String horaFinVigenciaPoliza) {
		this.horaFinVigenciaPoliza = horaFinVigenciaPoliza;
	}

	public String getHoraInicioVigenciaEndoso() {
		return horaInicioVigenciaEndoso;
	}

	public void setHoraInicioVigenciaEndoso(String horaInicioVigenciaEndoso) {
		this.horaInicioVigenciaEndoso = horaInicioVigenciaEndoso;
	}

	public String getHoraFinVigenciaEndoso() {
		return horaFinVigenciaEndoso;
	}

	public void setHoraFinVigenciaEndoso(String horaFinVigenciaEndoso) {
		this.horaFinVigenciaEndoso = horaFinVigenciaEndoso;
	}

	public Map<String, Object> getInformacionImpresionEndoso() {
		return informacionImpresionEndoso;
	}

	public void setInformacionImpresionEndoso(
			Map<String, Object> informacionImpresionEndoso) {
		this.informacionImpresionEndoso = informacionImpresionEndoso;
	}

}
