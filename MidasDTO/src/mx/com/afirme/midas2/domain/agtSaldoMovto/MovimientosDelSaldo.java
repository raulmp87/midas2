package mx.com.afirme.midas2.domain.agtSaldoMovto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;

@Entity(name="MovimientosDelSaldo")
@Table(name="toAgenteMovimientos",schema="MIDAS")
public class MovimientosDelSaldo implements Entidad,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private	Long	id;
	private	Long	idEmpresa;
	private	Agente	agente;
	private	Date	fehcaCorteEdocta;
	private	Long	idConsecMovto;
	private String	anioMes;
	private	Long	idConcepto;
	private	ValorCatalogoAgentes moneda;
	private	ValorCatalogoAgentes	monedaOrigen;
	private	Long	tipoCambio;
	private	String	descripcionMovto;
	private	String	idRamoContable;
	private	String	idSubramoContable;
	private	Long	idLineaNegocio;
	private	Date	fechaMovimiento;
	private	Long	idCentroEmisor;
	private	Long	numPoliza;
	private	Long	numRenovacionPoliza;
	private	Long	idCotizacion;
	private	Long	idVersionPol;
	private	Long	idCentroEmisore;
	private	Long	tipoEndoso;
	private	Long	numeroEndoso;
	private	Long	idSolicitud;
	private	Long	idVersionEndoso;
	private	String	referencia;
	private	CentroOperacion	centroOperacion;
	private	Long	idRemesa;
	private	Long	idConsecMovtoR;
	private	String	claveOrigenRemesa;
	private	String	serieFolioRbo;
	private	Long	numFolioRbo;
	private	Long	idRecibo;
	private	Long	idVersionRbo;
	private	Date	fechavencimientorec	;
	private	Double	porcentajePartAgente;
	private	Double	importePrimaNeta;
	private	Double	impRcgossPagoFr;
	private	Double	importeComisionAgente;
	private	String	claveOrigenAplic;
	private	String	claveOrigenMovto;
	private	Date	fechaCortePagoCom;
	private	ValorCatalogoAgentes	estatusMovimiento;
	private	String	idUsuarioIntegeg;
	private	Date	fhIntegracion;
	private	Date	fhAplicacion;
	private	String	naturalezaConcepto;
	private	String	tipoMovtoCobranza;
	private	Date	fTransfDePv;
	private	Long	idTransacDePv;
	private	Long	idTreansaccOrig;
	private	Agente	agenteOrigen;
	private	Long	idCobertura;
	private	String	esFacultativo;
	private	Long	anioVigenciaPoliza;
	private	String	claveSistAdmon;
	private	String	claveExpCalcDiv;
	private	Long	importePrimaDcp;
	private	Long	importePrimaTotal;
	private	String	cvetCptoAco;
	private	Long	idConceptoO;
	private	String	esMasivo;
	private	Double	importeBaseCalculo;
	private	Double	porcentajeComisionAgente;
	private	Long	numMovtoManAgente;
	private	Double	importePagoAntImptos;
	private	Double	porcentajeIva;
	private	Double	porcentageIvaRetenido;
	private	Double	porcentajeIsrRet;
	private	Double	importeIva;
	private	Double	importeIvaRet;
	private	Double	importeIsr;
	private	String	cveSdoImpto;
	private	String	esImpuesto;
	private	Long	anioMesPago;
	private	Long	idCalculo;
	private String descripcionRamo;
	private String descripcionSubramo;
	private String descripcionOrigenRemesa;
	private String descripcionConcepto;
	private String descripcionCatConcepto;
	
	
	@Id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="IDEMPRESA")
	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=Agente.class)
	@JoinColumn(name="IDAGENTE")
	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}


	@Temporal(TemporalType.DATE)
	@Column(name="FEHCACORTEEDOCTA")
	public Date getFehcaCorteEdocta() {
		return fehcaCorteEdocta;
	}

	
	public void setFehcaCorteEdocta(Date fehcaCorteEdocta) {
		this.fehcaCorteEdocta = fehcaCorteEdocta;
	}

	@Column(name="IDCONSECMOVTO")
	public Long getIdConsecMovto() {
		return idConsecMovto;
	}

	public void setIdConsecMovto(Long idConsecMovto) {
		this.idConsecMovto = idConsecMovto;
	}

	@Column(name="ANIOMES")
	public String getAnioMes() {
		return anioMes;
	}

	public void setAnioMes(String anioMes) {
		this.anioMes = anioMes;
	}

	@Column(name="IDCONCEPTO")
	public Long getIdConcepto() {
		return idConcepto;
	}

	public void setIdConcepto(Long idConcepto) {
		this.idConcepto = idConcepto;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDMONEDA")
	public ValorCatalogoAgentes getMoneda() {
		return moneda;
	}

	public void setMoneda(ValorCatalogoAgentes moneda) {
		this.moneda = moneda;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDMONEDAORIGEN")
	public ValorCatalogoAgentes getMonedaOrigen() {
		return monedaOrigen;
	}

	public void setMonedaOrigen(ValorCatalogoAgentes monedaOrigen) {
		this.monedaOrigen = monedaOrigen;
	}

	@Column(name="TIPOCAMBIO")
	public Long getTipoCambio() {
		return tipoCambio;
	}

	public void setTipoCambio(Long tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	@Column(name="DESCRIPCIONMOVTO")
	public String getDescripcionMovto() {
		return descripcionMovto;
	}

	public void setDescripcionMovto(String descripcionMovto) {
		this.descripcionMovto = descripcionMovto;
	}

	@Column(name="IDRAMOCONTABLE")
	public String getIdRamoContable() {
		return idRamoContable;
	}

	public void setIdRamoContable(String idRamoContable) {
		this.idRamoContable = idRamoContable;
	}

	@Column(name="IDSUBRAMOCONTABLE")
	public String getIdSubramoContable() {
		return idSubramoContable;
	}

	public void setIdSubramoContable(String idSubramoContable) {
		this.idSubramoContable = idSubramoContable;
	}

	@Column(name="IDLINEANEGOCIO")
	public Long getIdLineaNegocio() {
		return idLineaNegocio;
	}

	public void setIdLineaNegocio(Long idLineaNegocio) {
		this.idLineaNegocio = idLineaNegocio;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHAMOVIMIENTO")
	public Date getFechaMovimiento() {
		return fechaMovimiento;
	}

	public void setFechaMovimiento(Date fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
	}

	@Column(name="IDCENTROEMISOR")
	public Long getIdCentroEmisor() {
		return idCentroEmisor;
	}

	public void setIdCentroEmisor(Long idCentroEmisor) {
		this.idCentroEmisor = idCentroEmisor;
	}

	@Column(name="NUMPOLIZA")
	public Long getNumPoliza() {
		return numPoliza;
	}

	public void setNumPoliza(Long numPoliza) {
		this.numPoliza = numPoliza;
	}

	@Column(name="NUMRENOVACIONPOLIZA")
	public Long getNumRenovacionPoliza() {
		return numRenovacionPoliza;
	}

	public void setNumRenovacionPoliza(Long numRenovacionPoliza) {
		this.numRenovacionPoliza = numRenovacionPoliza;
	}

	@Column(name="IDCOTIZACION")
	public Long getIdCotizacion() {
		return idCotizacion;
	}

	public void setIdCotizacion(Long idCotizacion) {
		this.idCotizacion = idCotizacion;
	}

	@Column(name="IDVERSIONPOL")
	public Long getIdVersionPol() {
		return idVersionPol;
	}

	public void setIdVersionPol(Long idVersionPol) {
		this.idVersionPol = idVersionPol;
	}

	@Column(name="IDCENTROEMISORE")
	public Long getIdCentroEmisore() {
		return idCentroEmisore;
	}

	public void setIdCentroEmisore(Long idCentroEmisore) {
		this.idCentroEmisore = idCentroEmisore;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="TIPOENDOSO")
	public Long getTipoEndoso() {
		return tipoEndoso;
	}

	public void setTipoEndoso(Long tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}

	@Column(name="NUMEROENDOSO")
	public Long getNumeroEndoso() {
		return numeroEndoso;
	}

	public void setNumeroEndoso(Long numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}

	@Column(name="IDSOLICITUD")
	public Long getIdSolicitud() {
		return idSolicitud;
	}

	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	@Column(name="IDVERSIONENDOSO")
	public Long getIdVersionEndoso() {
		return idVersionEndoso;
	}

	public void setIdVersionEndoso(Long idVersionEndoso) {
		this.idVersionEndoso = idVersionEndoso;
	}

	@Column(name="REFERENCIA")
	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=CentroOperacion.class)
	@JoinColumn(name="IDCENTROOPERACION")
	public CentroOperacion getCentroOperacion() {
		return centroOperacion;
	}

	public void setCentroOperacion(CentroOperacion centroOperacion) {
		this.centroOperacion = centroOperacion;
	}

	@Column(name="IDREMESA")
	public Long getIdRemesa() {
		return idRemesa;
	}

	public void setIdRemesa(Long idRemesa) {
		this.idRemesa = idRemesa;
	}

	@Column(name="IDCONSECMOVTOR")
	public Long getIdConsecMovtoR() {
		return idConsecMovtoR;
	}

	public void setIdConsecMovtoR(Long idConsecMovtoR) {
		this.idConsecMovtoR = idConsecMovtoR;
	}

	@Column(name="CLAVEORIGENREMESA")
	public String getClaveOrigenRemesa() {
		return claveOrigenRemesa;
	}

	public void setClaveOrigenRemesa(String claveOrigenRemesa) {
		this.claveOrigenRemesa = claveOrigenRemesa;
	}

	@Column(name="SERIEFOLIORBO")
	public String getSerieFolioRbo() {
		return serieFolioRbo;
	}

	public void setSerieFolioRbo(String serieFolioRbo) {
		this.serieFolioRbo = serieFolioRbo;
	}

	@Column(name="NUMFOLIORBO")
	public Long getNumFolioRbo() {
		return numFolioRbo;
	}

	public void setNumFolioRbo(Long numFolioRbo) {
		this.numFolioRbo = numFolioRbo;
	}

	@Column(name="IDRECIBO")
	public Long getIdRecibo() {
		return idRecibo;
	}

	public void setIdRecibo(Long idRecibo) {
		this.idRecibo = idRecibo;
	}

	@Column(name="IDVERSIONRBO")
	public Long getIdVersionRbo() {
		return idVersionRbo;
	}

	public void setIdVersionRbo(Long idVersionRbo) {
		this.idVersionRbo = idVersionRbo;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHAVENCIMIENTOREC")
	public Date getFechavencimientorec() {
		return fechavencimientorec;
	}

	public void setFechavencimientorec(Date fechavencimientorec) {
		this.fechavencimientorec = fechavencimientorec;
	}

	@Column(name="PORCENTAJEPARTAGENTE")
	public Double getPorcentajePartAgente() {
		return porcentajePartAgente;
	}

	public void setPorcentajePartAgente(Double porcentajePartAgente) {
		this.porcentajePartAgente = porcentajePartAgente;
	}

	@Column(name="IMPORTEPRIMANETA")
	public Double getImportePrimaNeta() {
		return importePrimaNeta;
	}

	public void setImportePrimaNeta(Double importePrimaNeta) {
		this.importePrimaNeta = importePrimaNeta;
	}

	@Column(name="IMPRCGOSSPAGOFR")
	public Double getImpRcgossPagoFr() {
		return impRcgossPagoFr;
	}

	public void setImpRcgossPagoFr(Double impRcgossPagoFr) {
		this.impRcgossPagoFr = impRcgossPagoFr;
	}

	@Column(name="IMPORTECOMISIONAGENTE")
	public Double getImporteComisionAgente() {
		return importeComisionAgente;
	}

	public void setImporteComisionAgente(Double importeComisionAgente) {
		this.importeComisionAgente = importeComisionAgente;
	}

	@Column(name="CLAVEORIGENAPLIC")
	public String getClaveOrigenAplic() {
		return claveOrigenAplic;
	}

	public void setClaveOrigenAplic(String claveOrigenAplic) {
		this.claveOrigenAplic = claveOrigenAplic;
	}

	@Column(name="CLAVEORIGENMOVTO")
	public String getClaveOrigenMovto() {
		return claveOrigenMovto;
	}

	public void setClaveOrigenMovto(String claveOrigenMovto) {
		this.claveOrigenMovto = claveOrigenMovto;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHACORTEPAGOCOM")
	public Date getFechaCortePagoCom() {
		return fechaCortePagoCom;
	}

	public void setFechaCortePagoCom(Date fechaCortePagoCom) {
		this.fechaCortePagoCom = fechaCortePagoCom;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="ESTATUSMOVIMIENTO")
	public ValorCatalogoAgentes getEstatusMovimiento() {
		return estatusMovimiento;
	}

	public void setEstatusMovimiento(ValorCatalogoAgentes estatusMovimiento) {
		this.estatusMovimiento = estatusMovimiento;
	}

	@Column(name="IDUSUARIOINTEGEG")
	public String getIdUsuarioIntegeg() {
		return idUsuarioIntegeg;
	}

	public void setIdUsuarioIntegeg(String idUsuarioIntegeg) {
		this.idUsuarioIntegeg = idUsuarioIntegeg;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FHINTEGRACION")
	public Date getFhIntegracion() {
		return fhIntegracion;
	}

	public void setFhIntegracion(Date fhIntegracion) {
		this.fhIntegracion = fhIntegracion;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FHAPLICACION")
	public Date getFhAplicacion() {
		return fhAplicacion;
	}

	public void setFhAplicacion(Date fhAplicacion) {
		this.fhAplicacion = fhAplicacion;
	}

	@Column(name="NATURALEZACONCEPTO")
	public String getNaturalezaConcepto() {
		return naturalezaConcepto;
	}

	public void setNaturalezaConcepto(String naturalezaConcepto) {
		this.naturalezaConcepto = naturalezaConcepto;
	}

	@Column(name="TIPOMOVTOCOBRANZA")
	public String getTipoMovtoCobranza() {
		return tipoMovtoCobranza;
	}

	public void setTipoMovtoCobranza(String tipoMovtoCobranza) {
		this.tipoMovtoCobranza = tipoMovtoCobranza;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FTRANSFDEPV")
	public Date getfTransfDePv() {
		return fTransfDePv;
	}

	public void setfTransfDePv(Date fTransfDePv) {
		this.fTransfDePv = fTransfDePv;
	}

	@Column(name="IDTRANSACDEPV")
	public Long getIdTransacDePv() {
		return idTransacDePv;
	}

	public void setIdTransacDePv(Long idTransacDePv) {
		this.idTransacDePv = idTransacDePv;
	}

	@Column(name="IDTREANSACCORIG")
	public Long getIdTreansaccOrig() {
		return idTreansaccOrig;
	}

	public void setIdTreansaccOrig(Long idTreansaccOrig) {
		this.idTreansaccOrig = idTreansaccOrig;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=Agente.class)
	@JoinColumn(name="IDAGENTEORIGEN",insertable=false,updatable=false,referencedColumnName="IDAGENTE")
	public Agente getAgenteOrigen() {
		return agenteOrigen;
	}

	public void setAgenteOrigen(Agente agenteOrigen) {
		this.agenteOrigen = agenteOrigen;
	}

	@Column(name="IDCOBERTURA")
	public Long getIdCobertura() {
		return idCobertura;
	}

	public void setIdCobertura(Long idCobertura) {
		this.idCobertura = idCobertura;
	}

	@Column(name="ESFACULTATIVO")
	public String getEsFacultativo() {
		return esFacultativo;
	}

	public void setEsFacultativo(String esFacultativo) {
		this.esFacultativo = esFacultativo;
	}

	@Column(name="ANIOVIGENCIAPOLIZA")
	public Long getAnioVigenciaPoliza() {
		return anioVigenciaPoliza;
	}

	public void setAnioVigenciaPoliza(Long anioVigenciaPoliza) {
		this.anioVigenciaPoliza = anioVigenciaPoliza;
	}

	@Column(name="CLAVESISTADMON")
	public String getClaveSistAdmon() {
		return claveSistAdmon;
	}

	public void setClaveSistAdmon(String claveSistAdmon) {
		this.claveSistAdmon = claveSistAdmon;
	}

	@Column(name="CLAVEEXPCALCDIV")
	public String getClaveExpCalcDiv() {
		return claveExpCalcDiv;
	}

	public void setClaveExpCalcDiv(String claveExpCalcDiv) {
		this.claveExpCalcDiv = claveExpCalcDiv;
	}

	@Column(name="IMPORTEPRIMADCP")
	public Long getImportePrimaDcp() {
		return importePrimaDcp;
	}

	public void setImportePrimaDcp(Long importePrimaDcp) {
		this.importePrimaDcp = importePrimaDcp;
	}

	@Column(name="IMPORTEPRIMATOTAL")
	public Long getImportePrimaTotal() {
		return importePrimaTotal;
	}

	public void setImportePrimaTotal(Long importePrimaTotal) {
		this.importePrimaTotal = importePrimaTotal;
	}

	@Column(name="CVETCPTOACO")
	public String getCvetCptoAco() {
		return cvetCptoAco;
	}

	public void setCvetCptoAco(String cvetCptoAco) {
		this.cvetCptoAco = cvetCptoAco;
	}

	@Column(name="IDCONCEPTOO")
	public Long getIdConceptoO() {
		return idConceptoO;
	}

	public void setIdConceptoO(Long idConceptoO) {
		this.idConceptoO = idConceptoO;
	}

	@Column(name="ESMASIVO")
	public String getEsMasivo() {
		return esMasivo;
	}

	public void setEsMasivo(String esMasivo) {
		this.esMasivo = esMasivo;
	}

	@Column(name="IMPORTEBASECALCULO")
	public Double getImporteBaseCalculo() {
		return importeBaseCalculo;
	}

	public void setImporteBaseCalculo(Double importeBaseCalculo) {
		this.importeBaseCalculo = importeBaseCalculo;
	}

	@Column(name="PORCENTAJECOMISIONAGENTE")
	public Double getPorcentajeComisionAgente() {
		return porcentajeComisionAgente;
	}

	public void setPorcentajeComisionAgente(Double porcentajeComisionAgente) {
		this.porcentajeComisionAgente = porcentajeComisionAgente;
	}

	@Column(name="NUMMOVTOMANAGENTE")
	public Long getNumMovtoManAgente() {
		return numMovtoManAgente;
	}

	public void setNumMovtoManAgente(Long numMovtoManAgente) {
		this.numMovtoManAgente = numMovtoManAgente;
	}

	@Column(name="IMPORTEPAGOANTIMPTOS")
	public Double getImportePagoAntImptos() {
		return importePagoAntImptos;
	}

	public void setImportePagoAntImptos(Double importePagoAntImptos) {
		this.importePagoAntImptos = importePagoAntImptos;
	}

	@Column(name="PORCENTAJEIVA")
	public Double getPorcentajeIva() {
		return porcentajeIva;
	}

	public void setPorcentajeIva(Double porcentajeIva) {
		this.porcentajeIva = porcentajeIva;
	}

	@Column(name="PORCENTAGEIVARETENIDO")
	public Double getPorcentageIvaRetenido() {
		return porcentageIvaRetenido;
	}

	public void setPorcentageIvaRetenido(Double porcentageIvaRetenido) {
		this.porcentageIvaRetenido = porcentageIvaRetenido;
	}

	@Column(name="PORCENTAJEISRRET")
	public Double getPorcentajeIsrRet() {
		return porcentajeIsrRet;
	}

	public void setPorcentajeIsrRet(Double porcentajeIsrRet) {
		this.porcentajeIsrRet = porcentajeIsrRet;
	}

	@Column(name="IMPORTEIVA")
	public Double getImporteIva() {
		return importeIva;
	}

	public void setImporteIva(Double importeIva) {
		this.importeIva = importeIva;
	}

	@Column(name="IMPORTEIVARET")
	public Double getImporteIvaRet() {
		return importeIvaRet;
	}

	public void setImporteIvaRet(Double importeIvaRet) {
		this.importeIvaRet = importeIvaRet;
	}

	@Column(name="IMPORTEISR")
	public Double getImporteIsr() {
		return importeIsr;
	}

	public void setImporteIsr(Double importeIsr) {
		this.importeIsr = importeIsr;
	}

	@Column(name="CVESDOIMPTO")
	public String getCveSdoImpto() {
		return cveSdoImpto;
	}

	public void setCveSdoImpto(String cveSdoImpto) {
		this.cveSdoImpto = cveSdoImpto;
	}

	@Column(name="ESIMPUESTO")
	public String getEsImpuesto() {
		return esImpuesto;
	}

	public void setEsImpuesto(String esImpuesto) {
		this.esImpuesto = esImpuesto;
	}

	@Column(name="ANIOMESPAGO")
	public Long getAnioMesPago() {
		return anioMesPago;
	}

	public void setAnioMesPago(Long anioMesPago) {
		this.anioMesPago = anioMesPago;
	}

	@Column(name="IDCALCULO")
	public Long getIdCalculo() {
		return idCalculo;
	}

	public void setIdCalculo(Long idCalculo) {
		this.idCalculo = idCalculo;
	}

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Transient
	public String getDescripcionRamo() {
		return descripcionRamo;
	}

	public void setDescripcionRamo(String descripcionRamo) {
		this.descripcionRamo = descripcionRamo;
	}

	@Transient
	public String getDescripcionSubramo() {
		return descripcionSubramo;
	}

	public void setDescripcionSubramo(String descripcionSubramo) {
		this.descripcionSubramo = descripcionSubramo;
	}

	@Transient
	public String getDescripcionOrigenRemesa() {
		return descripcionOrigenRemesa;
	}

	public void setDescripcionOrigenRemesa(String descripcionOrigenRemesa) {
		this.descripcionOrigenRemesa = descripcionOrigenRemesa;
	}

	@Transient
	public String getDescripcionConcepto() {
		return descripcionConcepto;
	}

	public void setDescripcionConcepto(String descripcionConcepto) {
		this.descripcionConcepto = descripcionConcepto;
	}

	@Transient
	public String getDescripcionCatConcepto() {
		return descripcionCatConcepto;
	}

	public void setDescripcionCatConcepto(String descripcionCatConcepto) {
		this.descripcionCatConcepto = descripcionCatConcepto;
	}

}
