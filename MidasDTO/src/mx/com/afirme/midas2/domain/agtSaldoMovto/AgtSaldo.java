package mx.com.afirme.midas2.domain.agtSaldoMovto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;

@Entity(name="AgtSaldo")
@Table(name="toAgtSaldo", schema="MIDAS")
public class AgtSaldo implements Serializable,Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5704373885295749799L;
	private Long id;
	private Integer idEmpresa;
	private Agente idAgente;
	private Long verdaderoIdAgente;
	private Date fechaInicioPeriodo;
	private ValorCatalogoAgentes idMoneda;
	private Date fechaFinPeriodo;
	private Long anioMes;
	private Double saldoInicial;
	private Double impcomisionagentec;
	private Double impcomisionagentea;
	private Double impcomisionsupv_c;
	private Double impcomisionsupv_a;
	private Double impgravablec;
	private Double impgravablea;
	private Double impexcentoc;
	private Double impexcentoa;
	private Double impisr;
	private Double impivaret;
	private Double impivaacred;
	private Double impliquidado;
	private Double saldofinal;
	private BigDecimal importeSaldoFinal;
	private ValorCatalogoAgentes idsituacionsaldo;
	private Date fechasituacion;
	private Double impbonovida;
	private Double impbonodanos;
	private Double imppagoantic;
	private Double impmiscc;
	private Double impmisca;
	private Double imppercepcion;
	private Double impbgisrret;
	private Double impbgivaret;
	private Double impbeivaret;
	private Double impbgivaacr;
	private Double impbeivaacr;
	private Double impbonos;
	private Double impcargos;
	private Long numfoliofiscal;
	private Double saldoinicialisr;
	private Double saldofinalisr;
	private Double saldoiniisriva;
	private Double saldofinisriva;
	private Double saldoinisisriva;
	private Double saldofinsisriva;
	private Double impsdoisrivach;
	private Double impsdoisr_ch;
	private Double impsdosisrivach;
	private Double impcomgravivaa;
	private Double impcomexenivaa;
	private Double impcomgravivas;
	private Double impcomexenivas;
	private Double saliniiva;
	private Double salfiniva;
	private Double saliniisr;
	private Double salfinisr;
	private Double saliniriva;
	private Double salfinriva;
	private Long anio;
	
	@Id
//	@SequenceGenerator(name="IDTOAGTSALDO_SEQ", sequenceName="IDTOAGTSALDO_SEQ", allocationSize=1)
//	@GeneratedValue(generator="IDTOAGTSALDO_SEQ", strategy=GenerationType.SEQUENCE)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(name="IDEMPRESA")
	public Integer getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	
	@Transient
	public Agente getIdAgente() {
		return idAgente;
	}
	public void setIdAgente(Agente idAgente) {
		this.idAgente = idAgente;
	}
	
	@Column(name="IDAGENTE")
	public Long getVerdaderoIdAgente() {
		return verdaderoIdAgente;
	}
	
	public void setVerdaderoIdAgente(Long verdaderoIdAgente) {
		this.verdaderoIdAgente = verdaderoIdAgente;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAINICIOPERIODO")
	public Date getFechaInicioPeriodo() {
		return fechaInicioPeriodo;
	}
	public void setFechaInicioPeriodo(Date fechaInicioPeriodo) {
		this.fechaInicioPeriodo = fechaInicioPeriodo;
	}
	@ManyToOne(fetch=FetchType.LAZY, targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDMONEDA")
//	@Column(name="IDMONEDA")
	public ValorCatalogoAgentes getIdMoneda() {
		return idMoneda;
	}
	public void setIdMoneda(ValorCatalogoAgentes idMoneda) {
		this.idMoneda = idMoneda;
	}
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAFINPERIODO")
	public Date getFechaFinPeriodo() {
		return fechaFinPeriodo;
	}
	public void setFechaFinPeriodo(Date fechaFinPeriodo) {
		this.fechaFinPeriodo = fechaFinPeriodo;
	}
	@Column(name="ANIOMES")
	public Long getAnioMes() {
		return anioMes;
	}
	public void setAnioMes(Long anioMes) {
		this.anioMes = anioMes;
	}
	@Column(name="SALDOINICIAL")
	public Double getSaldoInicial() {
		return saldoInicial;
	}
	public void setSaldoInicial(Double saldoInicial) {
		this.saldoInicial = saldoInicial;
	}
	@Column(name="IMPCOMISIONAGENTEC")
	public Double getImpcomisionagentec() {
		return impcomisionagentec;
	}
	public void setImpcomisionagentec(Double impcomisionagentec) {
		this.impcomisionagentec = impcomisionagentec;
	}
	@Column(name="IMPCOMISIONAGENTEA")
	public Double getImpcomisionagentea() {
		return impcomisionagentea;
	}
	public void setImpcomisionagentea(Double impcomisionagentea) {
		this.impcomisionagentea = impcomisionagentea;
	}
	@Column(name="IMPCOMISIONSUPV_C")
	public Double getImpcomisionsupv_c() {
		return impcomisionsupv_c;
	}
	public void setImpcomisionsupv_c(Double impcomisionsupv_c) {
		this.impcomisionsupv_c = impcomisionsupv_c;
	}
	@Column(name="IMPCOMISIONSUPV_A")
	public Double getImpcomisionsupv_a() {
		return impcomisionsupv_a;
	}
	public void setImpcomisionsupv_a(Double impcomisionsupv_a) {
		this.impcomisionsupv_a = impcomisionsupv_a;
	}
	@Column(name="IMPGRAVABLEC")
	public Double getImpgravablec() {
		return impgravablec;
	}
	public void setImpgravablec(Double impgravablec) {
		this.impgravablec = impgravablec;
	}
	@Column(name="IMPGRAVABLEA")
	public Double getImpgravablea() {
		return impgravablea;
	}
	public void setImpgravablea(Double impgravablea) {
		this.impgravablea = impgravablea;
	}
	@Column(name="IMPEXCENTOC")
	public Double getImpexcentoc() {
		return impexcentoc;
	}
	public void setImpexcentoc(Double impexcentoc) {
		this.impexcentoc = impexcentoc;
	}
	@Column(name="IMPEXCENTOA")
	public Double getImpexcentoa() {
		return impexcentoa;
	}
	public void setImpexcentoa(Double impexcentoa) {
		this.impexcentoa = impexcentoa;
	}
	@Column(name="IMPISR")
	public Double getImpisr() {
		return impisr;
	}
	public void setImpisr(Double impisr) {
		this.impisr = impisr;
	}
	@Column(name="IMPIVARET")
	public Double getImpivaret() {
		return impivaret;
	}
	public void setImpivaret(Double impivaret) {
		this.impivaret = impivaret;
	}
	@Column(name="IMPIVAACRED")
	public Double getImpivaacred() {
		return impivaacred;
	}
	public void setImpivaacred(Double impivaacred) {
		this.impivaacred = impivaacred;
	}
	@Column(name="IMPLIQUIDADO")
	public Double getImpliquidado() {
		return impliquidado;
	}
	public void setImpliquidado(Double impliquidado) {
		this.impliquidado = impliquidado;
	}
	@Column(name="SALDOFINAL")
	public Double getSaldofinal() {
		return saldofinal;
	}
	public void setSaldofinal(Double saldofinal) {
		this.saldofinal = saldofinal;
	}
	@Column(name="IMPORTESALDOFINAL")
	public BigDecimal getImporteSaldoFinal() {
		return importeSaldoFinal;
	}
	public void setImporteSaldoFinal(BigDecimal importeSaldoFinal) {
		this.importeSaldoFinal = importeSaldoFinal;
	}
	@ManyToOne(fetch=FetchType.LAZY, targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDSITUACIONSALDO")
	public ValorCatalogoAgentes getIdsituacionsaldo() {
		return idsituacionsaldo;
	}
	public void setIdsituacionsaldo(ValorCatalogoAgentes idsituacionsaldo) {
		this.idsituacionsaldo = idsituacionsaldo;
	}
	@Temporal(TemporalType.DATE)
	@Column(name="FECHASITUACION")
	public Date getFechasituacion() {
		return fechasituacion;
	}
	public void setFechasituacion(Date fechasituacion) {
		this.fechasituacion = fechasituacion;
	}
	@Column(name="IMPBONOVIDA")
	public Double getImpbonovida() {
		return impbonovida;
	}
	public void setImpbonovida(Double impbonovida) {
		this.impbonovida = impbonovida;
	}
	@Column(name="IMPBONODANOS")
	public Double getImpbonodanos() {
		return impbonodanos;
	}
	public void setImpbonodanos(Double impbonodanos) {
		this.impbonodanos = impbonodanos;
	}
	@Column(name="IMPPAGOANTIC")
	public Double getImppagoantic() {
		return imppagoantic;
	}
	public void setImppagoantic(Double imppagoantic) {
		this.imppagoantic = imppagoantic;
	}
	@Column(name="IMPMISCC")
	public Double getImpmiscc() {
		return impmiscc;
	}
	public void setImpmiscc(Double impmiscc) {
		this.impmiscc = impmiscc;
	}
	@Column(name="IMPMISCA")
	public Double getImpmisca() {
		return impmisca;
	}
	public void setImpmisca(Double impmisca) {
		this.impmisca = impmisca;
	}
	@Column(name="IMPPERCEPCION")
	public Double getImppercepcion() {
		return imppercepcion;
	}
	public void setImppercepcion(Double imppercepcion) {
		this.imppercepcion = imppercepcion;
	}
	@Column(name="IMPBGISRRET")
	public Double getImpbgisrret() {
		return impbgisrret;
	}
	public void setImpbgisrret(Double impbgisrret) {
		this.impbgisrret = impbgisrret;
	}
	@Column(name="IMPBGIVARET")
	public Double getImpbgivaret() {
		return impbgivaret;
	}
	public void setImpbgivaret(Double impbgivaret) {
		this.impbgivaret = impbgivaret;
	}
	@Column(name="IMPBEIVARET")
	public Double getImpbeivaret() {
		return impbeivaret;
	}
	public void setImpbeivaret(Double impbeivaret) {
		this.impbeivaret = impbeivaret;
	}
	@Column(name="IMPBGIVAACR")
	public Double getImpbgivaacr() {
		return impbgivaacr;
	}
	public void setImpbgivaacr(Double impbgivaacr) {
		this.impbgivaacr = impbgivaacr;
	}
	@Column(name="IMPBEIVAACR")
	public Double getImpbeivaacr() {
		return impbeivaacr;
	}
	public void setImpbeivaacr(Double impbeivaacr) {
		this.impbeivaacr = impbeivaacr;
	}
	@Column(name="IMPBONOS")
	public Double getImpbonos() {
		return impbonos;
	}
	public void setImpbonos(Double impbonos) {
		this.impbonos = impbonos;
	}
	@Column(name="IMPCARGOS")
	public Double getImpcargos() {
		return impcargos;
	}
	public void setImpcargos(Double impcargos) {
		this.impcargos = impcargos;
	}
	@Column(name="NUMFOLIOFISCAL")
	public Long getNumfoliofiscal() {
		return numfoliofiscal;
	}
	public void setNumfoliofiscal(Long numfoliofiscal) {
		this.numfoliofiscal = numfoliofiscal;
	}
	@Column(name="SALDOINICIALISR")
	public Double getSaldoinicialisr() {
		return saldoinicialisr;
	}
	public void setSaldoinicialisr(Double saldoinicialisr) {
		this.saldoinicialisr = saldoinicialisr;
	}
	@Column(name="SALDOFINALISR")
	public Double getSaldofinalisr() {
		return saldofinalisr;
	}
	public void setSaldofinalisr(Double saldofinalisr) {
		this.saldofinalisr = saldofinalisr;
	}
	@Column(name="SALDOINIISRIVA")
	public Double getSaldoiniisriva() {
		return saldoiniisriva;
	}
	public void setSaldoiniisriva(Double saldoiniisriva) {
		this.saldoiniisriva = saldoiniisriva;
	}
	@Column(name="SALDOFINISRIVA")
	public Double getSaldofinisriva() {
		return saldofinisriva;
	}
	public void setSaldofinisriva(Double saldofinisriva) {
		this.saldofinisriva = saldofinisriva;
	}
	@Column(name="SALDOINISISRIVA")
	public Double getSaldoinisisriva() {
		return saldoinisisriva;
	}
	public void setSaldoinisisriva(Double saldoinisisriva) {
		this.saldoinisisriva = saldoinisisriva;
	}
	@Column(name="SALDOFINSISRIVA")
	public Double getSaldofinsisriva() {
		return saldofinsisriva;
	}
	public void setSaldofinsisriva(Double saldofinsisriva) {
		this.saldofinsisriva = saldofinsisriva;
	}
	@Column(name="IMPSDOISRIVACH")
	public Double getImpsdoisrivach() {
		return impsdoisrivach;
	}
	public void setImpsdoisrivach(Double impsdoisrivach) {
		this.impsdoisrivach = impsdoisrivach;
	}
	@Column(name="IMPSDOISR_CH")
	public Double getImpsdoisr_ch() {
		return impsdoisr_ch;
	}
	public void setImpsdoisr_ch(Double impsdoisr_ch) {
		this.impsdoisr_ch = impsdoisr_ch;
	}
	@Column(name="IMPSDOSISRIVACH")
	public Double getImpsdosisrivach() {
		return impsdosisrivach;
	}
	public void setImpsdosisrivach(Double impsdosisrivach) {
		this.impsdosisrivach = impsdosisrivach;
	}
	@Column(name="IMPCOMGRAVIVAA")
	public Double getImpcomgravivaa() {
		return impcomgravivaa;
	}
	public void setImpcomgravivaa(Double impcomgravivaa) {
		this.impcomgravivaa = impcomgravivaa;
	}
	@Column(name="IMPCOMEXENIVAA")
	public Double getImpcomexenivaa() {
		return impcomexenivaa;
	}
	public void setImpcomexenivaa(Double impcomexenivaa) {
		this.impcomexenivaa = impcomexenivaa;
	}
	@Column(name="IMPCOMGRAVIVAS")
	public Double getImpcomgravivas() {
		return impcomgravivas;
	}
	public void setImpcomgravivas(Double impcomgravivas) {
		this.impcomgravivas = impcomgravivas;
	}
	@Column(name="IMPCOMEXENIVAS")
	public Double getImpcomexenivas() {
		return impcomexenivas;
	}
	public void setImpcomexenivas(Double impcomexenivas) {
		this.impcomexenivas = impcomexenivas;
	}
	@Column(name="SALINIIVA")
	public Double getSaliniiva() {
		return saliniiva;
	}
	public void setSaliniiva(Double saliniiva) {
		this.saliniiva = saliniiva;
	}
	@Column(name="SALFINIVA")
	public Double getSalfiniva() {
		return salfiniva;
	}
	public void setSalfiniva(Double salfiniva) {
		this.salfiniva = salfiniva;
	}
	@Column(name="SALINIISR")
	public Double getSaliniisr() {
		return saliniisr;
	}
	public void setSaliniisr(Double saliniisr) {
		this.saliniisr = saliniisr;
	}
	@Column(name="SALFINISR")
	public Double getSalfinisr() {
		return salfinisr;
	}
	public void setSalfinisr(Double salfinisr) {
		this.salfinisr = salfinisr;
	}
	@Column(name="SALINIRIVA")
	public Double getSaliniriva() {
		return saliniriva;
	}
	public void setSaliniriva(Double saliniriva) {
		this.saliniriva = saliniriva;
	}
	@Column(name="SALFINRIVA")
	public Double getSalfinriva() {
		return salfinriva;
	}
	public void setSalfinriva(Double salfinriva) {
		this.salfinriva = salfinriva;
	}
	@Transient
	public Long getAnio() {
		return anio;
	}
	public void setAnio(Long anio) {
		this.anio = anio;
	}
	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
}
