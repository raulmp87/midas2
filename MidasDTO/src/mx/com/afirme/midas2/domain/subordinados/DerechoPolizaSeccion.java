package mx.com.afirme.midas2.domain.subordinados;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;

@Entity
@Table(name = "TODERECHOPOLIZASECCION", schema = "MIDAS")
public class DerechoPolizaSeccion implements Comparable<DerechoPolizaSeccion>, Serializable {

	private static final long serialVersionUID = 5340422862768713430L;

	private Long id;
	
	private SeccionDTO seccion;
	
	private Integer numeroSecuencia;
	
	private BigDecimal valor;
	
	private Boolean claveDefault;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTODERECHOPOLIZASECCION_SEQ")
	@SequenceGenerator(name="IDTODERECHOPOLIZASECCION_SEQ", sequenceName="MIDAS.IDTODERECHOPOLIZASECCION_SEQ", allocationSize=1)
	@Column(name = "IDTODERECHOPOLIZASECCION", nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOSECCION", nullable = false, insertable = true, updatable = true)
	public SeccionDTO getSeccion() {
		return seccion;
	}

	public void setSeccion(SeccionDTO seccion) {
		this.seccion = seccion;
	}

	@Column(name = "NUMEROSECUENCIA", nullable = false)
	@NotNull
	public Integer getNumeroSecuencia() {
		return numeroSecuencia;
	}

	public void setNumeroSecuencia(Integer numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}

	@Column(name = "VALOR", nullable = false, precision = 16, scale = 2)
	@NotNull
	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	@Column(name = "CLAVEDEFAULT", nullable = false, precision = 4)
	@NotNull
	public Boolean getClaveDefault() {
		return claveDefault;
	}

	public void setClaveDefault(Boolean claveDefault) {
		this.claveDefault = claveDefault;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((numeroSecuencia == null) ? 0 : numeroSecuencia.hashCode());
		result = prime * result + ((seccion == null) ? 0 : seccion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DerechoPolizaSeccion other = (DerechoPolizaSeccion) obj;
		if (numeroSecuencia == null) {
			if (other.numeroSecuencia != null)
				return false;
		} else if (!numeroSecuencia.equals(other.numeroSecuencia))
			return false;
		if (seccion == null) {
			if (other.seccion != null)
				return false;
		} else if (!seccion.equals(other.seccion))
			return false;
		return true;
	}

	@Override
	public int compareTo(DerechoPolizaSeccion other) {
		
		final int MENOR = -1;
	    final int IGUAL = 0;
	    final int MAYOR = 1;

	    if (this == other) return IGUAL;
	    
	    if (this.numeroSecuencia < other.numeroSecuencia) return MENOR;
	    if (this.numeroSecuencia > other.numeroSecuencia) return MAYOR;
	    
	    if (this.seccion != null) {
	    	if (this.seccion.getDescripcion().compareTo(other.seccion.getDescripcion()) < 0) return MENOR;
	    	if (this.seccion.getDescripcion().compareTo(other.seccion.getDescripcion()) > 0) return MAYOR;
	    }
		
		return IGUAL;
	}

	public DerechoPolizaSeccion() {
		
	}
	
	
	
	
}
