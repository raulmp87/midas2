package mx.com.afirme.midas2.domain.bitacora.pago.programado.ibs;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Clase que representa a la tabla de IBS_DATA_TMP
 * para que se pueda persistir la informacion del archivo 
 * que retorna IBS.
 * 
 * @author SEGUROS AFIRME
 * 
 * @since 24012017
 * 
 * @version 1.0
 *
 */
@Entity
@Table(name = "BITACORA_PAGOPROG_IBS", schema = "MIDAS")
public class BitacoraPagoProgIBSTO implements Serializable{
	
	private static final long serialVersionUID = -5782728640004162834L;
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BITACORA_PAGOPROG_IBS_SEQ")
	@SequenceGenerator(name="BITACORA_PAGOPROG_IBS_SEQ", sequenceName="BITACORA_PAGOPROG_IBS_SEQ", schema="MIDAS", allocationSize=1)
	private BigDecimal id;

	@Column(name="IDCOTIZACION")
	private BigDecimal idCotizacion;

	@Column(name="BANCO")
	private String banco;

	@Column(name="IDSUCURSAL")
	private BigDecimal idSucursal;

	@Column(name="SUCURSAL")
	private String sucursal;

	@Column(name="MONEDA")
	private String moneda;

	@Column(name="POLIZA")
	private String poliza;

	@Column(name="RECIBO")
	private BigDecimal recibo;

	@Column(name="CTACGO")
	private BigDecimal cuentaCargo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECCUOTA")
	private Date fechaCuota;

	@Column(name="NUMCUOTA")
	private BigDecimal numeroCuota;

	@Column(name="IMPCUOTA")
	private Double importeCuota;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECPAGO")
	private Date fechaPago;

	@Column(name="IMPPAGO")
	private Double importePago;

	@Column(name="USUARIO")
	private String usuario;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECOPER")
	private Date fechaOperacion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECREG")
	private Date fechaRegistro;

	@Column(name="NUMCANC")
	private BigDecimal numeroCancelacion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECCANC")
	private Date fechaCancelacion;

	@Column(name="USUCANC")
	private String usuarioCancelacion;

	@Column(name="MOTIVO")
	private String motivo;

	@Column(name="INTENTOS")
	private BigDecimal intentos;

	@Column(name="ERROR")
	private String error;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECRECH")
	private Date fechaReciboH;

	@Column(name="ORIG")
	private String original;

	@Column(name="ESTATUS")
	private String estatus;

	@Column(name="FREC")
	private String fechaRecibo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="F_INI_REG")
	private Date fechaInicioRegistro;

	@Column(name="ESTATUSCTA")
	private String estatusCuenta;

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public BigDecimal getIdCotizacion() {
		return idCotizacion;
	}

	public void setIdCotizacion(BigDecimal idCotizacion) {
		this.idCotizacion = idCotizacion;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public BigDecimal getIdSucursal() {
		return idSucursal;
	}

	public void setIdSucursal(BigDecimal idSucursal) {
		this.idSucursal = idSucursal;
	}

	public String getSucursal() {
		return sucursal;
	}

	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getPoliza() {
		return poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	public BigDecimal getRecibo() {
		return recibo;
	}

	public void setRecibo(BigDecimal recibo) {
		this.recibo = recibo;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public BigDecimal getIntentos() {
		return intentos;
	}

	public void setIntentos(BigDecimal intentos) {
		this.intentos = intentos;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getOriginal() {
		return original;
	}

	public void setOriginal(String original) {
		this.original = original;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public BigDecimal getCuentaCargo() {
		return cuentaCargo;
	}

	public void setCuentaCargo(BigDecimal cuentaCargo) {
		this.cuentaCargo = cuentaCargo;
	}

	public Date getFechaCuota() {
		return fechaCuota;
	}

	public void setFechaCuota(Date fechaCuota) {
		this.fechaCuota = fechaCuota;
	}

	public BigDecimal getNumeroCuota() {
		return numeroCuota;
	}

	public void setNumeroCuota(BigDecimal numeroCuota) {
		this.numeroCuota = numeroCuota;
	}

	public Double getImporteCuota() {
		return importeCuota;
	}

	public void setImporteCuota(Double importeCuota) {
		this.importeCuota = importeCuota;
	}

	public Date getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	public Double getImportePago() {
		return importePago;
	}

	public void setImportePago(Double importePago) {
		this.importePago = importePago;
	}

	public Date getFechaOperacion() {
		return fechaOperacion;
	}

	public void setFechaOperacion(Date fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public BigDecimal getNumeroCancelacion() {
		return numeroCancelacion;
	}

	public void setNumeroCancelacion(BigDecimal numeroCancelacion) {
		this.numeroCancelacion = numeroCancelacion;
	}

	public Date getFechaCancelacion() {
		return fechaCancelacion;
	}

	public void setFechaCancelacion(Date fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}

	public String getFechaRecibo() {
		return fechaRecibo;
	}

	public void setFechaRecibo(String fechaRecibo) {
		this.fechaRecibo = fechaRecibo;
	}

	public Date getFechaInicioRegistro() {
		return fechaInicioRegistro;
	}

	public void setFechaInicioRegistro(Date fechaInicioRegistro) {
		this.fechaInicioRegistro = fechaInicioRegistro;
	}

	public String getEstatusCuenta() {
		return estatusCuenta;
	}

	public void setEstatusCuenta(String estatusCuenta) {
		this.estatusCuenta = estatusCuenta;
	}

	public String getUsuarioCancelacion() {
		return usuarioCancelacion;
	}

	public void setUsuarioCancelacion(String usuarioCancelacion) {
		this.usuarioCancelacion = usuarioCancelacion;
	}

	public Date getFechaReciboH() {
		return fechaReciboH;
	}

	public void setFechaReciboH(Date fechaReciboH) {
		this.fechaReciboH = fechaReciboH;
	}
	
}
