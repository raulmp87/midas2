package mx.com.afirme.midas2.domain.recepcionDocumentos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;

@Entity(name="RecepcionDocumentos")
@Table(name="toRecepcionDocumentos", schema="MIDAS")
public class RecepcionDocumentos implements Entidad,Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4394324759530989453L;
	private Long id;
	private Date fechaInicio;
	private Date fechaFin;
	private Long idAgente;
	private ValorCatalogoAgentes motivoRechazo;
	private String descripcion;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idToRecepcionDocumentos_seq")
	@SequenceGenerator(name="idToRecepcionDocumentos_seq",sequenceName="MIDAS.idToRecepcionDocumentos_seq",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHAINICIO")
	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHAFIN")
	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	@Column(name="IDAGENTE")
	public Long getIdAgente() {
		return idAgente;
	}

	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="MOTIVORECHAZO")
	public ValorCatalogoAgentes getMotivoRechazo() {
		return motivoRechazo;
	}

	public void setMotivoRechazo(ValorCatalogoAgentes motivoRechazo) {
		this.motivoRechazo = motivoRechazo;
	}

	@Column(name="DESCRIPCION")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}
