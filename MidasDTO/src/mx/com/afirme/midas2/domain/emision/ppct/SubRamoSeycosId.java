package mx.com.afirme.midas2.domain.emision.ppct;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * SubRamoSeycosId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class SubRamoSeycosId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = -969824681250488472L;
	private String idRamoContable;
     private String idSubrContable;


    // Constructors

    /** default constructor */
    public SubRamoSeycosId() {
    }

    
    /** full constructor */
    public SubRamoSeycosId(String idRamoContable, String idSubrContable) {
        this.idRamoContable = idRamoContable;
        this.idSubrContable = idSubrContable;
    }

   
    // Property accessors

    @Column(name="ID_RAMO_CONTABLE", nullable=false, length=2)
    public String getIdRamoContable() {
        return this.idRamoContable;
    }
    
    public void setIdRamoContable(String idRamoContable) {
        this.idRamoContable = idRamoContable;
    }

    @Column(name="ID_SUBR_CONTABLE", nullable=false, length=2)
    public String getIdSubrContable() {
        return this.idSubrContable;
    }
    
    public void setIdSubrContable(String idSubrContable) {
        this.idSubrContable = idSubrContable;
    }

    
   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof SubRamoSeycosId) ) return false;
		 SubRamoSeycosId castOther = ( SubRamoSeycosId ) other; 
         
		 return ( (this.getIdRamoContable()==castOther.getIdRamoContable()) || ( this.getIdRamoContable()!=null && castOther.getIdRamoContable()!=null && this.getIdRamoContable().equals(castOther.getIdRamoContable()) ) )
 && ( (this.getIdSubrContable()==castOther.getIdSubrContable()) || ( this.getIdSubrContable()!=null && castOther.getIdSubrContable()!=null && this.getIdSubrContable().equals(castOther.getIdSubrContable()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdRamoContable() == null ? 0 : this.getIdRamoContable().hashCode() );
         result = 37 * result + ( getIdSubrContable() == null ? 0 : this.getIdSubrContable().hashCode() );
         return result;
   }   

}