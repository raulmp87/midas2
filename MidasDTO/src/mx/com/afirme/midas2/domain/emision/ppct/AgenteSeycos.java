package mx.com.afirme.midas2.domain.emision.ppct;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * agenteSeycos entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="AGENTE"
    ,schema="SEYCOS"
)

public class AgenteSeycos  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 8614754631503086369L;
	private AgenteSeycosId id;
     private Integer idPersona;
     private String cveTAgente;
     private Long idSupervisoria;
     private String BComision;
     private String numCedula;
     private Date FAutorizacion;
     private Date FVenctoAutor;
     private String numFianza;
     private Date FIniVigFianza;
     private Date FFinVigFianza;
     private Double impFianza;
     private Integer idAfianzadora;
     private String EMail;
     private String BVtaDanos;
     private String BVtaVidaInd;
     private String BVtaVidaGpo;
     private String BVtaVidaCol;
     private String BVtaAccPers;
     private String BVtaGtosMed;
     private Double pctDividendos;
     private Date FAlta;
     private String sitAgente;
     private String motSitAgte;
     private String txObserv;
     private Integer idBanco;
     private Integer idSucursal;
     private Integer idPlaza;
     private String numCuenta;
     private String idUsuario;
     private String BUltMod;
     private String tipoCedula;
     private String sitCedula;
     private String cveImprRecibos;
     private Date FUltMovEdocta;
     private Date FUltPagoCom;
     private String cveClaseAgente;
     private String BGeneraCheque;
     private Integer idAgteReclutad;
     private String BVtasAnioPasado;
     private String BImpEdoCta;
     private String BContabComis;

     private Long idEmpresa;
     private Long idAgente;
     private Date FSit;
    // Constructors

    /** default constructor */
    public AgenteSeycos() {
    }

	/** minimal constructor */
    public AgenteSeycos(AgenteSeycosId id, Long idEmpresa, Long idAgente, Date FSit) {
        this.id = id;
        this.idEmpresa = idEmpresa;
		this.idAgente = idAgente;
		this.FSit = FSit;
    }
    
    /** full constructor */
    public AgenteSeycos(AgenteSeycosId id, Integer idPersona, String cveTAgente, Long idSupervisoria, String BComision, String numCedula, Date FAutorizacion, Date FVenctoAutor, String numFianza, Date FIniVigFianza, Date FFinVigFianza, Double impFianza, Integer idAfianzadora, String EMail, String BVtaDanos, String BVtaVidaInd, String BVtaVidaGpo, String BVtaVidaCol, String BVtaAccPers, String BVtaGtosMed, Double pctDividendos, Date FAlta, String sitAgente, String motSitAgte, String txObserv, Integer idBanco, Integer idSucursal, Integer idPlaza, String numCuenta, String idUsuario, String BUltMod, String tipoCedula, String sitCedula, String cveImprRecibos, Date FUltMovEdocta, Date FUltPagoCom, String cveClaseAgente, String BGeneraCheque, Integer idAgteReclutad, String BVtasAnioPasado, String BImpEdoCta, String BContabComis, Long idEmpresa, Long idAgente, Date FSit) {
        this.id = id;
        this.idPersona = idPersona;
        this.cveTAgente = cveTAgente;
        this.idSupervisoria = idSupervisoria;
        this.BComision = BComision;
        this.numCedula = numCedula;
        this.FAutorizacion = FAutorizacion;
        this.FVenctoAutor = FVenctoAutor;
        this.numFianza = numFianza;
        this.FIniVigFianza = FIniVigFianza;
        this.FFinVigFianza = FFinVigFianza;
        this.impFianza = impFianza;
        this.idAfianzadora = idAfianzadora;
        this.EMail = EMail;
        this.BVtaDanos = BVtaDanos;
        this.BVtaVidaInd = BVtaVidaInd;
        this.BVtaVidaGpo = BVtaVidaGpo;
        this.BVtaVidaCol = BVtaVidaCol;
        this.BVtaAccPers = BVtaAccPers;
        this.BVtaGtosMed = BVtaGtosMed;
        this.pctDividendos = pctDividendos;
        this.FAlta = FAlta;
        this.sitAgente = sitAgente;
        this.motSitAgte = motSitAgte;
        this.txObserv = txObserv;
        this.idBanco = idBanco;
        this.idSucursal = idSucursal;
        this.idPlaza = idPlaza;
        this.numCuenta = numCuenta;
        this.idUsuario = idUsuario;
        this.BUltMod = BUltMod;
        this.tipoCedula = tipoCedula;
        this.sitCedula = sitCedula;
        this.cveImprRecibos = cveImprRecibos;
        this.FUltMovEdocta = FUltMovEdocta;
        this.FUltPagoCom = FUltPagoCom;
        this.cveClaseAgente = cveClaseAgente;
        this.BGeneraCheque = BGeneraCheque;
        this.idAgteReclutad = idAgteReclutad;
        this.BVtasAnioPasado = BVtasAnioPasado;
        this.BImpEdoCta = BImpEdoCta;
        this.BContabComis = BContabComis;
        this.idEmpresa = idEmpresa;
		this.idAgente = idAgente;
		this.FSit = FSit;
    }

   
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idEmpresa", column=@Column(name="ID_EMPRESA", precision=8, scale=0) ), 
        @AttributeOverride(name="idAgente", column=@Column(name="ID_AGENTE", precision=8, scale=0) ), 
        @AttributeOverride(name="FSit", column=@Column(name="F_SIT", length=7) ) } )

    public AgenteSeycosId getId() {
        return this.id;
    }
    
    public void setId(AgenteSeycosId id) {
        this.id = id;
    }
    
    @Column(name="ID_PERSONA", precision=8, scale=0)

    public Integer getIdPersona() {
        return this.idPersona;
    }
    
    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }
    
    @Column(name="CVE_T_AGENTE", length=4)

    public String getCveTAgente() {
        return this.cveTAgente;
    }
    
    public void setCveTAgente(String cveTAgente) {
        this.cveTAgente = cveTAgente;
    }
    
    @Column(name="ID_SUPERVISORIA", precision=10, scale=0)

    public Long getIdSupervisoria() {
        return this.idSupervisoria;
    }
    
    public void setIdSupervisoria(Long idSupervisoria) {
        this.idSupervisoria = idSupervisoria;
    }
    
    @Column(name="B_COMISION", length=1)

    public String getBComision() {
        return this.BComision;
    }
    
    public void setBComision(String BComision) {
        this.BComision = BComision;
    }
    
    @Column(name="NUM_CEDULA", length=20)

    public String getNumCedula() {
        return this.numCedula;
    }
    
    public void setNumCedula(String numCedula) {
        this.numCedula = numCedula;
    }
    
    @Column(name="F_AUTORIZACION", length=7)
    @Temporal(TemporalType.DATE)
    public Date getFAutorizacion() {
        return this.FAutorizacion;
    }
    
    public void setFAutorizacion(Date FAutorizacion) {
        this.FAutorizacion = FAutorizacion;
    }
    
    @Column(name="F_VENCTO_AUTOR", length=7)
    @Temporal(TemporalType.DATE)
    public Date getFVenctoAutor() {
        return this.FVenctoAutor;
    }
    
    public void setFVenctoAutor(Date FVenctoAutor) {
        this.FVenctoAutor = FVenctoAutor;
    }
    
    @Column(name="NUM_FIANZA", length=15)

    public String getNumFianza() {
        return this.numFianza;
    }
    
    public void setNumFianza(String numFianza) {
        this.numFianza = numFianza;
    }
    
    @Column(name="F_INI_VIG_FIANZA", length=7)
    @Temporal(TemporalType.DATE)
    public Date getFIniVigFianza() {
        return this.FIniVigFianza;
    }
    
    public void setFIniVigFianza(Date FIniVigFianza) {
        this.FIniVigFianza = FIniVigFianza;
    }
    
    @Column(name="F_FIN_VIG_FIANZA", length=7)
    @Temporal(TemporalType.DATE)
    public Date getFFinVigFianza() {
        return this.FFinVigFianza;
    }
    
    public void setFFinVigFianza(Date FFinVigFianza) {
        this.FFinVigFianza = FFinVigFianza;
    }
    
    @Column(name="IMP_FIANZA", precision=16)

    public Double getImpFianza() {
        return this.impFianza;
    }
    
    public void setImpFianza(Double impFianza) {
        this.impFianza = impFianza;
    }
    
    @Column(name="ID_AFIANZADORA", precision=8, scale=0)

    public Integer getIdAfianzadora() {
        return this.idAfianzadora;
    }
    
    public void setIdAfianzadora(Integer idAfianzadora) {
        this.idAfianzadora = idAfianzadora;
    }
    
    @Column(name="E_MAIL", length=150)

    public String getEMail() {
        return this.EMail;
    }
    
    public void setEMail(String EMail) {
        this.EMail = EMail;
    }
    
    @Column(name="B_VTA_DANOS", length=1)

    public String getBVtaDanos() {
        return this.BVtaDanos;
    }
    
    public void setBVtaDanos(String BVtaDanos) {
        this.BVtaDanos = BVtaDanos;
    }
    
    @Column(name="B_VTA_VIDA_IND", length=1)

    public String getBVtaVidaInd() {
        return this.BVtaVidaInd;
    }
    
    public void setBVtaVidaInd(String BVtaVidaInd) {
        this.BVtaVidaInd = BVtaVidaInd;
    }
    
    @Column(name="B_VTA_VIDA_GPO", length=1)

    public String getBVtaVidaGpo() {
        return this.BVtaVidaGpo;
    }
    
    public void setBVtaVidaGpo(String BVtaVidaGpo) {
        this.BVtaVidaGpo = BVtaVidaGpo;
    }
    
    @Column(name="B_VTA_VIDA_COL", length=1)

    public String getBVtaVidaCol() {
        return this.BVtaVidaCol;
    }
    
    public void setBVtaVidaCol(String BVtaVidaCol) {
        this.BVtaVidaCol = BVtaVidaCol;
    }
    
    @Column(name="B_VTA_ACC_PERS", length=1)

    public String getBVtaAccPers() {
        return this.BVtaAccPers;
    }
    
    public void setBVtaAccPers(String BVtaAccPers) {
        this.BVtaAccPers = BVtaAccPers;
    }
    
    @Column(name="B_VTA_GTOS_MED", length=1)

    public String getBVtaGtosMed() {
        return this.BVtaGtosMed;
    }
    
    public void setBVtaGtosMed(String BVtaGtosMed) {
        this.BVtaGtosMed = BVtaGtosMed;
    }
    
    @Column(name="PCT_DIVIDENDOS", precision=8, scale=4)

    public Double getPctDividendos() {
        return this.pctDividendos;
    }
    
    public void setPctDividendos(Double pctDividendos) {
        this.pctDividendos = pctDividendos;
    }
    
    @Column(name="F_ALTA", length=7)
    @Temporal(TemporalType.DATE)
    public Date getFAlta() {
        return this.FAlta;
    }
    
    public void setFAlta(Date FAlta) {
        this.FAlta = FAlta;
    }
    
    @Column(name="SIT_AGENTE", length=1)

    public String getSitAgente() {
        return this.sitAgente;
    }
    
    public void setSitAgente(String sitAgente) {
        this.sitAgente = sitAgente;
    }
    
    @Column(name="MOT_SIT_AGTE", length=2)

    public String getMotSitAgte() {
        return this.motSitAgte;
    }
    
    public void setMotSitAgte(String motSitAgte) {
        this.motSitAgte = motSitAgte;
    }
    
    @Column(name="TX_OBSERV", length=2000)

    public String getTxObserv() {
        return this.txObserv;
    }
    
    public void setTxObserv(String txObserv) {
        this.txObserv = txObserv;
    }
    
    @Column(name="ID_BANCO", precision=8, scale=0)

    public Integer getIdBanco() {
        return this.idBanco;
    }
    
    public void setIdBanco(Integer idBanco) {
        this.idBanco = idBanco;
    }
    
    @Column(name="ID_SUCURSAL", precision=5, scale=0)

    public Integer getIdSucursal() {
        return this.idSucursal;
    }
    
    public void setIdSucursal(Integer idSucursal) {
        this.idSucursal = idSucursal;
    }
    
    @Column(name="ID_PLAZA", precision=5, scale=0)

    public Integer getIdPlaza() {
        return this.idPlaza;
    }
    
    public void setIdPlaza(Integer idPlaza) {
        this.idPlaza = idPlaza;
    }
    
    @Column(name="NUM_CUENTA", length=20)

    public String getNumCuenta() {
        return this.numCuenta;
    }
    
    public void setNumCuenta(String numCuenta) {
        this.numCuenta = numCuenta;
    }
    
    @Column(name="ID_USUARIO", length=8)

    public String getIdUsuario() {
        return this.idUsuario;
    }
    
    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }
    
    @Column(name="B_ULT_MOD", length=1)

    public String getBUltMod() {
        return this.BUltMod;
    }
    
    public void setBUltMod(String BUltMod) {
        this.BUltMod = BUltMod;
    }
    
    @Column(name="TIPO_CEDULA", length=2)

    public String getTipoCedula() {
        return this.tipoCedula;
    }
    
    public void setTipoCedula(String tipoCedula) {
        this.tipoCedula = tipoCedula;
    }
    
    @Column(name="SIT_CEDULA", length=1)

    public String getSitCedula() {
        return this.sitCedula;
    }
    
    public void setSitCedula(String sitCedula) {
        this.sitCedula = sitCedula;
    }
    
    @Column(name="CVE_IMPR_RECIBOS", length=2)

    public String getCveImprRecibos() {
        return this.cveImprRecibos;
    }
    
    public void setCveImprRecibos(String cveImprRecibos) {
        this.cveImprRecibos = cveImprRecibos;
    }
    
    @Column(name="F_ULT_MOV_EDOCTA", length=7)
    @Temporal(TemporalType.DATE)
    public Date getFUltMovEdocta() {
        return this.FUltMovEdocta;
    }
    
    public void setFUltMovEdocta(Date FUltMovEdocta) {
        this.FUltMovEdocta = FUltMovEdocta;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="F_ULT_PAGO_COM", length=7)
    public Date getFUltPagoCom() {
        return this.FUltPagoCom;
    }
    
    public void setFUltPagoCom(Date FUltPagoCom) {
        this.FUltPagoCom = FUltPagoCom;
    }
    
    @Column(name="CVE_CLASE_AGENTE", length=1)

    public String getCveClaseAgente() {
        return this.cveClaseAgente;
    }
    
    public void setCveClaseAgente(String cveClaseAgente) {
        this.cveClaseAgente = cveClaseAgente;
    }
    
    @Column(name="B_GENERA_CHEQUE", length=1)

    public String getBGeneraCheque() {
        return this.BGeneraCheque;
    }
    
    public void setBGeneraCheque(String BGeneraCheque) {
        this.BGeneraCheque = BGeneraCheque;
    }
    
    @Column(name="ID_AGTE_RECLUTAD", precision=8, scale=0)

    public Integer getIdAgteReclutad() {
        return this.idAgteReclutad;
    }
    
    public void setIdAgteReclutad(Integer idAgteReclutad) {
        this.idAgteReclutad = idAgteReclutad;
    }
    
    @Column(name="B_VTAS_ANIO_PASADO", length=1)

    public String getBVtasAnioPasado() {
        return this.BVtasAnioPasado;
    }
    
    public void setBVtasAnioPasado(String BVtasAnioPasado) {
        this.BVtasAnioPasado = BVtasAnioPasado;
    }
    
    @Column(name="B_IMP_EDO_CTA", length=1)

    public String getBImpEdoCta() {
        return this.BImpEdoCta;
    }
    
    public void setBImpEdoCta(String BImpEdoCta) {
        this.BImpEdoCta = BImpEdoCta;
    }
    
    @Column(name="B_CONTAB_COMIS", length=1)

    public String getBContabComis() {
        return this.BContabComis;
    }
    
    public void setBContabComis(String BContabComis) {
        this.BContabComis = BContabComis;
    }

    @Column(name = "ID_EMPRESA")
	public Long getIdEmpresa() {
		return idEmpresa;
	}
	
	@Column(name = "ID_AGENTE")
	public Long getIdAgente() {
		return idAgente;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "F_SIT")
	public Date getFSit() {
		return FSit;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}

	public void setFSit(Date fSit) {
		FSit = fSit;
	}
}