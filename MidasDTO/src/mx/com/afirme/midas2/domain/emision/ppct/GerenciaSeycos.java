package mx.com.afirme.midas2.domain.emision.ppct;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * GerenciaSeycos entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="GERENCIA"
    ,schema="SEYCOS"
)

public class GerenciaSeycos  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 6413155309911092762L;
	 private GerenciaSeycosId id;
     private String nomGerencia;
     private Integer idCentroOper;
     private Integer idDirector;
     private Integer idGerente;
     private Integer idDomicilio;
     private String EMail;
     private String sitGerencia;
     private String idUsuario;
     private String BUltMod;
     
     private Long idEmpresa;
     private Long idGerencia;
     private Date FSit;


    // Constructors

    /** default constructor */
    public GerenciaSeycos() {
    }

	/** minimal constructor */
    public GerenciaSeycos(GerenciaSeycosId id, String nomGerencia, Integer idCentroOper, Integer idDomicilio, String sitGerencia, String idUsuario, String BUltMod, Long idEmpresa,Long idGerencia,Date FSit) {
        this.id = id;
        this.nomGerencia = nomGerencia;
        this.idCentroOper = idCentroOper;
        this.idDomicilio = idDomicilio;
        this.sitGerencia = sitGerencia;
        this.idUsuario = idUsuario;
        this.BUltMod = BUltMod;
        this.idGerencia = idGerencia;
        this.idEmpresa = idEmpresa;
        this.FSit = FSit;
    }
    
    /** full constructor */
    public GerenciaSeycos(GerenciaSeycosId id, String nomGerencia, Integer idCentroOper, Integer idDirector, Integer idGerente, Integer idDomicilio, String EMail, String sitGerencia, String idUsuario, String BUltMod, Long idEmpresa,Long idGerencia,Date FSit) {
        this.id = id;
        this.nomGerencia = nomGerencia;
        this.idCentroOper = idCentroOper;
        this.idDirector = idDirector;
        this.idGerente = idGerente;
        this.idDomicilio = idDomicilio;
        this.EMail = EMail;
        this.sitGerencia = sitGerencia;
        this.idUsuario = idUsuario;
        this.BUltMod = BUltMod;
        this.idGerencia = idGerencia;
        this.idEmpresa = idEmpresa;
        this.FSit = FSit;
    }

   
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idEmpresa", column=@Column(name="ID_EMPRESA", nullable=false, precision=8, scale=0, insertable=false, updatable=false) ), 
        @AttributeOverride(name="idGerencia", column=@Column(name="ID_GERENCIA", nullable=false, length=3,insertable=false, updatable=false) ), 
        @AttributeOverride(name="FSit", column=@Column(name="F_SIT", nullable=false, insertable=false, updatable=false) ) } )
        
        
    public GerenciaSeycosId getId() {
        return this.id;
    }
    
    public void setId(GerenciaSeycosId id) {
        this.id = id;
    }
    
    @Column(name="NOM_GERENCIA", nullable=false, length=80)

    public String getNomGerencia() {
        return this.nomGerencia;
    }
    
    public void setNomGerencia(String nomGerencia) {
        this.nomGerencia = nomGerencia;
    }
    
    @Column(name="ID_CENTRO_OPER", nullable=false, precision=5, scale=0)

    public Integer getIdCentroOper() {
        return this.idCentroOper;
    }
    
    public void setIdCentroOper(Integer idCentroOper) {
        this.idCentroOper = idCentroOper;
    }
    
    @Column(name="ID_DIRECTOR", precision=8, scale=0)

    public Integer getIdDirector() {
        return this.idDirector;
    }
    
    public void setIdDirector(Integer idDirector) {
        this.idDirector = idDirector;
    }
    
    @Column(name="ID_GERENTE", precision=8, scale=0)

    public Integer getIdGerente() {
        return this.idGerente;
    }
    
    public void setIdGerente(Integer idGerente) {
        this.idGerente = idGerente;
    }
    
    @Column(name="ID_DOMICILIO", nullable=false, precision=8, scale=0)

    public Integer getIdDomicilio() {
        return this.idDomicilio;
    }
    
    public void setIdDomicilio(Integer idDomicilio) {
        this.idDomicilio = idDomicilio;
    }
    
    @Column(name="E_MAIL", length=60)

    public String getEMail() {
        return this.EMail;
    }
    
    public void setEMail(String EMail) {
        this.EMail = EMail;
    }
    
    @Column(name="SIT_GERENCIA", nullable=false, length=1)

    public String getSitGerencia() {
        return this.sitGerencia;
    }
    
    public void setSitGerencia(String sitGerencia) {
        this.sitGerencia = sitGerencia;
    }
    
    @Column(name="ID_USUARIO", nullable=false, length=8)

    public String getIdUsuario() {
        return this.idUsuario;
    }
    
    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }
    
    @Column(name="B_ULT_MOD", nullable=false, length=1)

    public String getBUltMod() {
        return this.BUltMod;
    }
    
    public void setBUltMod(String BUltMod) {
        this.BUltMod = BUltMod;
    }
    
    @Column(name="ID_EMPRESA", nullable=false, length=2)
	public Long getIdEmpresa() {
		return idEmpresa;
	}
    
    @Column(name="ID_GERENCIA", nullable=false,  length=2)
	public Long getIdGerencia() {
		return idGerencia;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name="F_SIT", nullable=false,length=7)
	public Date getFSit() {
		return FSit;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public void setIdGerencia(Long idGerencia) {
		this.idGerencia = idGerencia;
	}

	public void setFSit(Date fSit) {
		FSit = fSit;
	}
   








}