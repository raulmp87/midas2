package mx.com.afirme.midas2.domain.emision.ppct;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dto.emision.ppct.RangoFechasDTO;

@Entity(name="ReciboProveedor")
@Table(name="PPCT_RECIBO",schema="MIDAS")
public class ReciboProveedor extends Recibo implements Entidad, Serializable {

		
	private static final long serialVersionUID = 7389580591327258943L;

	private BigDecimal id;
	
	private String numeroPoliza;
	
	private Integer numeroEndoso;
		
	private String numeroInciso;
	
	private CoberturaSeycos cobertura;
	
	private ReciboSeycos reciboOriginal;
	
	private OrdenPago ordenPago;
	
	private ConfiguracionPago configuracionPago;
	
	private BigDecimal monto;
	
	private Boolean seleccionado;
	
	private String sesion;
	
	private RangoFechasDTO rangoFechas;

	@Id
	@SequenceGenerator(name="PPCT_RECIBO_SEQ", sequenceName="MIDAS.PPCT_RECIBO_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PPCT_RECIBO_SEQ")
	@Column(name = "ID", nullable = false, precision = 22, scale = 0)
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}
		
	@Column(name = "NUMERO_POLIZA", length = 20)
	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	@Column(name = "NUMERO_ENDOSO", nullable = false, precision = 15, scale = 0)
	public Integer getNumeroEndoso() {
		return numeroEndoso;
	}

	public void setNumeroEndoso(Integer numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}
		
	@Column(name = "NUMERO_INCISO", nullable = false, length = 80)
	public String getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	@ManyToOne(fetch=FetchType.LAZY, targetEntity=CoberturaSeycos.class)
    @JoinColumn(name = "ID_COBERTURA")
	public CoberturaSeycos getCobertura() {
		return cobertura;
	}

	public void setCobertura(CoberturaSeycos cobertura) {
		this.cobertura = cobertura;
	}

	@ManyToOne(fetch=FetchType.LAZY, targetEntity=ReciboSeycos.class)
    @JoinColumns( { 
    @JoinColumn(name="ID_RECIBO", referencedColumnName="ID_RECIBO", nullable=false, insertable=false, updatable=false), 
    @JoinColumn(name="ID_VERSION_RBO", referencedColumnName="ID_VERSION_RBO", nullable=false, insertable=false, updatable=false)})
	public ReciboSeycos getReciboOriginal() {
		return reciboOriginal;
	}

	public void setReciboOriginal(ReciboSeycos reciboOriginal) {
		this.reciboOriginal = reciboOriginal;
	}

	@ManyToOne(fetch=FetchType.LAZY, targetEntity=OrdenPago.class)
    @JoinColumn(name = "PPCT_ORDEN_PAGO_ID")
	public OrdenPago getOrdenPago() {
		return ordenPago;
	}

	public void setOrdenPago(OrdenPago ordenPago) {
		this.ordenPago = ordenPago;
	}

	@ManyToOne(fetch=FetchType.LAZY, targetEntity=ConfiguracionPago.class)
    @JoinColumn(name = "PPCT_CONF_COBERTURA_ID")
	public ConfiguracionPago getConfiguracionPago() {
		return configuracionPago;
	}

	public void setConfiguracionPago(ConfiguracionPago configuracionPago) {
		this.configuracionPago = configuracionPago;
	}

	@Column(name = "MONTO", nullable = false, precision = 15, scale = 2)
	public BigDecimal getMonto() {
		return monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	@Column(name="SELECCIONADO", precision=1, scale=0)
	public Boolean getSeleccionado() {
		return seleccionado;
	}

	public void setSeleccionado(Boolean seleccionado) {
		this.seleccionado = seleccionado;
	}

	@Column(name = "SESION", length = 200)
	public String getSesion() {
		return sesion;
	}

	public void setSesion(String sesion) {
		this.sesion = sesion;
	}
	
	@Transient
	public RangoFechasDTO getRangoFechas() {
		return rangoFechas;
	}

	public void setRangoFechas(RangoFechasDTO rangoFechas) {
		this.rangoFechas = rangoFechas;
	}

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
}
