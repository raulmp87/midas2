package mx.com.afirme.midas2.domain.emision.ppct;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import org.directwebremoting.annotations.DataTransferObject;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="DP_COBERTURA",schema="SEYCOS")
@SqlResultSetMapping(name="CoberturaSeycosResult", 
        entities={ @EntityResult(entityClass=CoberturaSeycos.class)}
    )
@DataTransferObject
public class CoberturaSeycos implements Serializable, Entidad {

	private static final long serialVersionUID = 7607891360757962310L;

	private BigDecimal id;
	
	private String descripcion;

	@Id
	@Column(name = "ID_COBERTURA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	@Column(name = "NOM_COBERTURA", insertable= false, updatable = false, nullable = false, length = 70)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	
}
