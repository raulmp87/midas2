package mx.com.afirme.midas2.domain.emision.conductoresAdicionales;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Adriana Flores
 * Llave primaria de PolizaAutoSeycos
 *
 */
@Embeddable
public class PolizaAutoSeycosPk implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "ID_COTIZACION", unique = false, nullable = false, precision = 10, scale = 0)
	private Long idCotizacion;

	@Column(name = "ID_LIN_NEGOCIO", nullable = false, precision = 5, scale = 0)
	private Integer lineaNegocio;
	@Column(name = "ID_INCISO", nullable = false, precision = 6, scale = 0)
	private Integer numeroInciso;
	@Column(name = "ID_VERSION_POL", nullable = false, precision = 4, scale = 0)
	private Integer idVersionPoliza;
	
	public PolizaAutoSeycosPk() {
		
	}
	public PolizaAutoSeycosPk(Long idCotizacion, Integer lineaNegocio,
			Integer numeroInciso, Integer idVersionPoliza) {
		super();
		this.idCotizacion = idCotizacion;
		this.lineaNegocio = lineaNegocio;
		this.numeroInciso = numeroInciso;
		this.idVersionPoliza = idVersionPoliza;
	}
	public Long getIdCotizacion() {
		return idCotizacion;
	}
	public void setIdCotizacion(Long idCotizacion) {
		this.idCotizacion = idCotizacion;
	}
	public Integer getLineaNegocio() {
		return lineaNegocio;
	}
	public void setLineaNegocio(Integer lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}
	public Integer getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public Integer getIdVersionPoliza() {
		return idVersionPoliza;
	}
	public void setIdVersionPoliza(Integer idVersionPoliza) {
		this.idVersionPoliza = idVersionPoliza;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idCotizacion == null) ? 0 : idCotizacion.hashCode());
		result = prime * result
				+ ((idVersionPoliza == null) ? 0 : idVersionPoliza.hashCode());
		result = prime * result
				+ ((lineaNegocio == null) ? 0 : lineaNegocio.hashCode());
		result = prime * result
				+ ((numeroInciso == null) ? 0 : numeroInciso.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PolizaAutoSeycosPk other = (PolizaAutoSeycosPk) obj;
		if (idCotizacion == null) {
			if (other.idCotizacion != null)
				return false;
		} else if (!idCotizacion.equals(other.idCotizacion))
			return false;
		if (idVersionPoliza == null) {
			if (other.idVersionPoliza != null)
				return false;
		} else if (!idVersionPoliza.equals(other.idVersionPoliza))
			return false;
		if (lineaNegocio == null) {
			if (other.lineaNegocio != null)
				return false;
		} else if (!lineaNegocio.equals(other.lineaNegocio))
			return false;
		if (numeroInciso == null) {
			if (other.numeroInciso != null)
				return false;
		} else if (!numeroInciso.equals(other.numeroInciso))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "PolizaAutoSeycosPk [idCotizacion=" + idCotizacion
				+ ", lineaNegocio=" + lineaNegocio + ", numeroInciso="
				+ numeroInciso + ", idVersionPoliza=" + idVersionPoliza + "]";
	}
}
