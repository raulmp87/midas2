package mx.com.afirme.midas2.domain.emision.ppct;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * GerenciaSeycosId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class GerenciaSeycosId  implements java.io.Serializable {

    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 59404231686884427L;
	private Integer idEmpresa;
    private String idGerencia;
    private Date FSit;


    // Constructors

    /** default constructor */
    public GerenciaSeycosId() {
    }

    
    /** full constructor */
    public GerenciaSeycosId(Integer idEmpresa, String idGerencia, Date FSit) {
        this.idEmpresa = idEmpresa;
        this.idGerencia = idGerencia;
        this.FSit = FSit;
    }

   
    // Property accessors

    @Column(name="ID_EMPRESA", nullable=false, precision=8, scale=0)
    public Integer getIdEmpresa() {
        return this.idEmpresa;
    }
    
    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    @Column(name="ID_GERENCIA", nullable=false, length=3)
    public String getIdGerencia() {
        return this.idGerencia;
    }
    
    public void setIdGerencia(String idGerencia) {
        this.idGerencia = idGerencia;
    }

    @Temporal(TemporalType.DATE)
	@Column(name="F_SIT")
    public Date getFSit() {
        return this.FSit;
    }
    
    public void setFSit(Date FSit) {
        this.FSit = FSit;
    }
    
    /**
     * Metodo Utilidazado para settear el valor
     * a FSit cuando el parametro es Long
     * @param fsit
     */
    public void setFsit(Long fsit){
    	this.FSit = new Date(fsit);
    }

   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof GerenciaSeycosId) ) return false;
		 GerenciaSeycosId castOther = ( GerenciaSeycosId ) other; 
         
		 return ( (this.getIdEmpresa()==castOther.getIdEmpresa()) || ( this.getIdEmpresa()!=null && castOther.getIdEmpresa()!=null && this.getIdEmpresa().equals(castOther.getIdEmpresa()) ) )
 && ( (this.getIdGerencia()==castOther.getIdGerencia()) || ( this.getIdGerencia()!=null && castOther.getIdGerencia()!=null && this.getIdGerencia().equals(castOther.getIdGerencia()) ) )
 && ( (this.getFSit()==castOther.getFSit()) || ( this.getFSit()!=null && castOther.getFSit()!=null && this.getFSit().equals(castOther.getFSit()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdEmpresa() == null ? 0 : this.getIdEmpresa().hashCode() );
         result = 37 * result + ( getIdGerencia() == null ? 0 : this.getIdGerencia().hashCode() );
         result = 37 * result + ( getFSit() == null ? 0 : this.getFSit().hashCode() );
         return result;
   }   
   
}