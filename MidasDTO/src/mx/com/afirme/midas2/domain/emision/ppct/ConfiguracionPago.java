package mx.com.afirme.midas2.domain.emision.ppct;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.directwebremoting.annotations.DataTransferObject;

@Entity
@Table(name="PPCT_CONF_COBERTURA",schema="MIDAS")
@DataTransferObject
public class ConfiguracionPago implements Serializable {

	private static final long serialVersionUID = -7959198291922183395L;

	private BigDecimal id;
	
	private Proveedor proveedor;
		
	@Id
	@SequenceGenerator(name="PPCT_CONF_COBERTURA_SEQ", sequenceName="MIDAS.PPCT_CONF_COBERTURA_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PPCT_CONF_COBERTURA_SEQ")
	@Column(name = "ID", nullable = false, precision = 22, scale = 0)
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PPCT_PROVEEDOR_ID")
	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}
	
	
}
