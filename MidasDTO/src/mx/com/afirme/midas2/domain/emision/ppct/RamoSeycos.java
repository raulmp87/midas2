package mx.com.afirme.midas2.domain.emision.ppct;
// default package

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * RamoSeycos entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="ING_RAMO"
    ,schema="SEYCOS"
)

public class RamoSeycos  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = -6131216503158788797L;
	private String idRamoContable;
     private String nomRamo;
     private String BRamoVirtual;


    // Constructors

    /** default constructor */
    public RamoSeycos() {
    }

	/** minimal constructor */
    public RamoSeycos(String idRamoContable, String nomRamo) {
        this.idRamoContable = idRamoContable;
        this.nomRamo = nomRamo;
    }
    
    /** full constructor */
    public RamoSeycos(String idRamoContable, String nomRamo, String BRamoVirtual) {
        this.idRamoContable = idRamoContable;
        this.nomRamo = nomRamo;
        this.BRamoVirtual = BRamoVirtual;
    }

   
    // Property accessors
    @Id 
    @Column(name="ID_RAMO_CONTABLE", unique=true, nullable=false, length=2)
    public String getIdRamoContable() {
        return this.idRamoContable;
    }
    
    public void setIdRamoContable(String idRamoContable) {
        this.idRamoContable = idRamoContable;
    }
    
    @Column(name="NOM_RAMO", nullable=false, length=30)

    public String getNomRamo() {
        return this.nomRamo;
    }
    
    public void setNomRamo(String nomRamo) {
        this.nomRamo = nomRamo;
    }
    
    @Column(name="B_RAMO_VIRTUAL", length=1)

    public String getBRamoVirtual() {
        return this.BRamoVirtual;
    }
    
    public void setBRamoVirtual(String BRamoVirtual) {
        this.BRamoVirtual = BRamoVirtual;
    }
    
}