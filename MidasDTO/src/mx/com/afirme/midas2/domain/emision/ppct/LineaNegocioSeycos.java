package mx.com.afirme.midas2.domain.emision.ppct;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * LineaNegocioSeycos entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="DP_LINEA_NEG"
    ,schema="SEYCOS"
, uniqueConstraints = @UniqueConstraint(columnNames={"ID_LIN_NEGOCIO", "CVE_T_OPERACION", "CVE_DIVISION", "NOM_LINEA_NEG"})
)

public class LineaNegocioSeycos  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	 private static final long serialVersionUID = -436079890420737619L;
	 private Long idLinNegocio;
     private String nomLineaNeg;
     private String nomCtoLin;
     private String cveDivision;
     private String cveTBien;
     private String txObserv;
     private String cveMetodoCont;
     private String cveTOperacion;
     private String cveTDistRea;


    // Constructors

    /** default constructor */
    public LineaNegocioSeycos() {
    }

	/** minimal constructor */
    public LineaNegocioSeycos(Long  idLinNegocio) {
        this.idLinNegocio = idLinNegocio;
    }
    
    /** full constructor */
    public LineaNegocioSeycos(Long  idLinNegocio, String nomLineaNeg, String nomCtoLin, String cveDivision, String cveTBien, String txObserv, String cveMetodoCont, String cveTOperacion, String cveTDistRea) {
        this.idLinNegocio = idLinNegocio;
        this.nomLineaNeg = nomLineaNeg;
        this.nomCtoLin = nomCtoLin;
        this.cveDivision = cveDivision;
        this.cveTBien = cveTBien;
        this.txObserv = txObserv;
        this.cveMetodoCont = cveMetodoCont;
        this.cveTOperacion = cveTOperacion;
        this.cveTDistRea = cveTDistRea;
    }

   
    // Property accessors
    
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ01_DP_LINEA_NEG")
	@SequenceGenerator(name="SQ01_DP_LINEA_NEG", schema="SEYCOS",sequenceName="SQ01_DP_LINEA_NEG",allocationSize=1)
	@Id
	@Column(name="ID_LIN_NEGOCIO",nullable=false)
    public Long getIdLinNegocio() {
        return this.idLinNegocio;
    }
    
    public void setIdLinNegocio(Long  idLinNegocio) {
        this.idLinNegocio = idLinNegocio;
    }
    
    @Column(name="NOM_LINEA_NEG", length=60)

    public String getNomLineaNeg() {
        return this.nomLineaNeg;
    }
    
    public void setNomLineaNeg(String nomLineaNeg) {
        this.nomLineaNeg = nomLineaNeg;
    }
    
    @Column(name="NOM_CTO_LIN", length=10)

    public String getNomCtoLin() {
        return this.nomCtoLin;
    }
    
    public void setNomCtoLin(String nomCtoLin) {
        this.nomCtoLin = nomCtoLin;
    }
    
    @Column(name="CVE_DIVISION", length=2)

    public String getCveDivision() {
        return this.cveDivision;
    }
    
    public void setCveDivision(String cveDivision) {
        this.cveDivision = cveDivision;
    }
    
    @Column(name="CVE_T_BIEN", length=5)

    public String getCveTBien() {
        return this.cveTBien;
    }
    
    public void setCveTBien(String cveTBien) {
        this.cveTBien = cveTBien;
    }
    
    @Column(name="TX_OBSERV", length=2000)

    public String getTxObserv() {
        return this.txObserv;
    }
    
    public void setTxObserv(String txObserv) {
        this.txObserv = txObserv;
    }
    
    @Column(name="CVE_METODO_CONT", length=2)

    public String getCveMetodoCont() {
        return this.cveMetodoCont;
    }
    
    public void setCveMetodoCont(String cveMetodoCont) {
        this.cveMetodoCont = cveMetodoCont;
    }
    
    @Column(name="CVE_T_OPERACION", length=5)

    public String getCveTOperacion() {
        return this.cveTOperacion;
    }
    
    public void setCveTOperacion(String cveTOperacion) {
        this.cveTOperacion = cveTOperacion;
    }
    
    @Column(name="CVE_T_DIST_REA", length=5)

    public String getCveTDistRea() {
        return this.cveTDistRea;
    }
    
    public void setCveTDistRea(String cveTDistRea) {
        this.cveTDistRea = cveTDistRea;
    }
}