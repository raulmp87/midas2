package mx.com.afirme.midas2.domain.emision.ppct;
import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * ProductoVersionSeycosId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class ProductoVersionSeycosId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 3038452127323613636L;
	private Long idProducto;
     private Short idVersion;


    // Constructors

    /** default constructor */
    public ProductoVersionSeycosId() {
    }

    
    /** full constructor */
    public ProductoVersionSeycosId(Long idProducto, Short idVersion) {
        this.idProducto = idProducto;
        this.idVersion = idVersion;
    }

   
    // Property accessors

    @Column(name="ID_PRODUCTO", precision=5, scale=0)

    public Long getIdProducto() {
        return this.idProducto;
    }
    
    public void setIdProducto(Long idProducto) {
        this.idProducto = idProducto;
    }

    @Column(name="ID_VERSION", precision=3, scale=0)

    public Short getIdVersion() {
        return this.idVersion;
    }
    
    public void setIdVersion(Short idVersion) {
        this.idVersion = idVersion;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof ProductoVersionSeycosId) ) return false;
		 ProductoVersionSeycosId castOther = ( ProductoVersionSeycosId ) other; 
         
		 return ( (this.getIdProducto()==castOther.getIdProducto()) || ( this.getIdProducto()!=null && castOther.getIdProducto()!=null && this.getIdProducto().equals(castOther.getIdProducto()) ) )
 && ( (this.getIdVersion()==castOther.getIdVersion()) || ( this.getIdVersion()!=null && castOther.getIdVersion()!=null && this.getIdVersion().equals(castOther.getIdVersion()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdProducto() == null ? 0 : this.getIdProducto().hashCode() );
         result = 37 * result + ( getIdVersion() == null ? 0 : this.getIdVersion().hashCode() );
         return result;
   }   





}