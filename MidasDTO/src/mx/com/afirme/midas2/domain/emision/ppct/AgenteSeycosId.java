package mx.com.afirme.midas2.domain.emision.ppct;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * agenteSeycosId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class AgenteSeycosId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = -4388320764388177992L;
	private Integer idEmpresa;
     private Integer idAgente;
     private Date FSit;


    // Constructors

    /** default constructor */
    public AgenteSeycosId() {
    }

    
    /** full constructor */
    public AgenteSeycosId(Integer idEmpresa, Integer idAgente, Date FSit) {
        this.idEmpresa = idEmpresa;
        this.idAgente = idAgente;
        this.FSit = FSit;
    }

   
    // Property accessors

    @Column(name="ID_EMPRESA", precision=8, scale=0)

    public Integer getIdEmpresa() {
        return this.idEmpresa;
    }
    
    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    @Column(name="ID_AGENTE", precision=8, scale=0)

    public Integer getIdAgente() {
        return this.idAgente;
    }
    
    public void setIdAgente(Integer idAgente) {
        this.idAgente = idAgente;
    }

    @Temporal(TemporalType.DATE)
	@Column(name="F_SIT")
	public Date getFSit() {
        return this.FSit;
    }
    
    public void setFSit(Date FSit) {
        this.FSit = FSit;
    }

   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof AgenteSeycosId) ) return false;
		 AgenteSeycosId castOther = ( AgenteSeycosId ) other; 
         
		 return ( (this.getIdEmpresa()==castOther.getIdEmpresa()) || ( this.getIdEmpresa()!=null && castOther.getIdEmpresa()!=null && this.getIdEmpresa().equals(castOther.getIdEmpresa()) ) )
 && ( (this.getIdAgente()==castOther.getIdAgente()) || ( this.getIdAgente()!=null && castOther.getIdAgente()!=null && this.getIdAgente().equals(castOther.getIdAgente()) ) )
 && ( (this.getFSit()==castOther.getFSit()) || ( this.getFSit()!=null && castOther.getFSit()!=null && this.getFSit().equals(castOther.getFSit()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdEmpresa() == null ? 0 : this.getIdEmpresa().hashCode() );
         result = 37 * result + ( getIdAgente() == null ? 0 : this.getIdAgente().hashCode() );
         result = 37 * result + ( getFSit() == null ? 0 : this.getFSit().hashCode() );
         return result;
   }   

}