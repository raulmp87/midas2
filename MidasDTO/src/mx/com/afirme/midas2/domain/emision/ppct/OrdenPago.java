package mx.com.afirme.midas2.domain.emision.ppct;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.base.Scrollable;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.contabilidad.SolicitudCheque;
import mx.com.afirme.midas2.dto.emision.ppct.RangoFechasDTO;

@Entity
@Table(name="PPCT_ORDEN_PAGO",schema="MIDAS")
public class OrdenPago extends Scrollable implements Serializable, Entidad {

	private static final long serialVersionUID = 6393225923768484095L;
	
	private BigDecimal id;
	
	private BigDecimal folio;
	
	private Date fechaCorte;
	
	private Proveedor proveedor;
	
	private SolicitudCheque solicitudCheque;
	
	private String claveNegocio;
	
	private BigDecimal importePreeliminar;
	
	private RangoFechasDTO rangoFechas;
	
	@Id
	@SequenceGenerator(name="PPCT_ORDEN_PAGO_SEQ", sequenceName="MIDAS.PPCT_ORDEN_PAGO_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PPCT_ORDEN_PAGO_SEQ")
	@Column(name = "ID", nullable = false, precision = 22, scale = 0)
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	@Column(name = "FOLIO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getFolio() {
		return folio; 
	}

	public void setFolio(BigDecimal folio) {
		this.folio = folio;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CORTE", nullable = false, length = 7)
	public Date getFechaCorte() {
		return fechaCorte;
	}

	public void setFechaCorte(Date fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PPCT_PROVEEDOR_ID")
	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_SOL_CHEQUE")
	public SolicitudCheque getSolicitudCheque() {
		return solicitudCheque;
	}

	public void setSolicitudCheque(SolicitudCheque solicitudCheque) {
		this.solicitudCheque = solicitudCheque;
	}
	
	@Column(name = "CLAVENEGOCIO", nullable = false, length = 1)	
	public String getClaveNegocio() {
		return claveNegocio;
	}

	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}

	@Transient
	public BigDecimal getImportePreeliminar() {
		return importePreeliminar;
	}

	public void setImportePreeliminar(BigDecimal importePreeliminar) {
		this.importePreeliminar = importePreeliminar;
	}
	
	@Transient
	public RangoFechasDTO getRangoFechas() {
		return rangoFechas;
	}

	public void setRangoFechas(RangoFechasDTO rangoFechas) {
		this.rangoFechas = rangoFechas;
	}

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	

}
