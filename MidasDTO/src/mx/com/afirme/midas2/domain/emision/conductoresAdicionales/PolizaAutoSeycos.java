package mx.com.afirme.midas2.domain.emision.conductoresAdicionales;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * @author Adriana Flores
 * The persistent class for the POL_AUTOS database table.
 * 
 */

@Entity
@SqlResultSetMapping(
	name = "PolizaSeycosMapping",
	entities = {
		@EntityResult(
			entityClass = PolizaAutoSeycos.class,
			fields = {
				@FieldResult(name = "idPolizaAutoSeycos.idCotizacion", column = "ID_COTIZACION"),
				@FieldResult(name = "idPolizaAutoSeycos.lineaNegocio", column = "ID_LIN_NEGOCIO"),
				@FieldResult(name = "idPolizaAutoSeycos.numeroInciso", column = "ID_INCISO"),
				@FieldResult(name = "idPolizaAutoSeycos.idVersionPoliza", column = "ID_VERSION_POL"),
				@FieldResult(name = "descripcionVehiculo", column = "DESC_VEHIC"),
				@FieldResult(name = "numeroSerie", column = "NUM_SERIE"),
				@FieldResult(name = "numeroMotor", column = "NUM_MOTOR"),
				@FieldResult(name = "nombreConductorHabitual", column = "NOM_COND_HAB"),
				@FieldResult(name = "nombreConductorAdicional1", column = "NOM_COND_ADIC_1"),
				@FieldResult(name = "nombreConductorAdicional2", column = "NOM_COND_ADIC_2"),
				@FieldResult(name = "nombreConductorAdicional3", column = "NOM_COND_ADIC_3"),
				@FieldResult(name = "nombreConductorAdicional4", column = "NOM_COND_ADIC_4"),
				@FieldResult(name = "nombreConductorAdicional5", column = "NOM_COND_ADIC_5")
			}
		)	
	},
	columns ={
		@ColumnResult(name = "codigoProducto"),
		@ColumnResult(name = "codigoTipoPoliza"),
		@ColumnResult(name = "numeroRenovacion"),
		@ColumnResult(name = "numeroPoliza"),
		@ColumnResult(name = "fechaInicioVigencia"),
		@ColumnResult(name = "fechaFinVigencia"),
		@ColumnResult(name = "claveEstatus"),
		@ColumnResult(name = "nombreContratante"),
		@ColumnResult(name = "descripcionTipoPoliza"),
		@ColumnResult(name = "paquete")	
	}
)
@Table(name="POL_AUTO", schema="SEYCOS")
public class PolizaAutoSeycos implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private PolizaAutoSeycosPk idPolizaAutoSeycos;
	
	@Column(name = "DESC_VEHIC", length = 150, nullable = false)
	private String descripcionVehiculo;
	@Column(name = "NUM_SERIE", length = 30, nullable = false)
	private String numeroSerie;
	@Column(name = "NUM_MOTOR", length = 30, nullable = false)
	private String numeroMotor;
	@Column(name = "NOM_COND_HAB", length = 80, nullable = false)
	private String nombreConductorHabitual;
	@Column(name = "NOM_COND_ADIC_1", length = 80, nullable = false)
	private String nombreConductorAdicional1;
	@Column(name = "NOM_COND_ADIC_2", length = 80, nullable = false)
	private String nombreConductorAdicional2;
	@Column(name = "NOM_COND_ADIC_3", length = 80, nullable = false)
	private String nombreConductorAdicional3;
	@Column(name = "NOM_COND_ADIC_4", length = 80, nullable = false)
	private String nombreConductorAdicional4;
	@Column(name = "NOM_COND_ADIC_5", length = 80, nullable = false)	
	private String nombreConductorAdicional5;
		
	@Transient
	private Integer numeroPoliza;
	@Transient
	private String claveEstatus;
	@Transient
	private String nombreContratante;
	@Transient
	private String descripcionTipoPoliza;
	@Transient
	private String codigoProducto;
	@Transient
	private String codigoTipoPoliza;
	@Transient
	private Integer numeroRenovacion;
	@Transient
	private Date fechaInicioVigencia;
	@Transient
	private Date fechaFinVigencia;
	@Transient
	private String paquete;
	
	
	public PolizaAutoSeycos() {		
	}

	public String getDescripcionVehiculo() {
		return descripcionVehiculo;
	}
	public void setDescripcionVehiculo(String descripcionVehiculo) {
		this.descripcionVehiculo = descripcionVehiculo;
	}
	public String getNumeroSerie() {
		return numeroSerie;
	}
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	public String getNumeroMotor() {
		return numeroMotor;
	}
	public void setNumeroMotor(String numeroMotor) {
		this.numeroMotor = numeroMotor;
	}
	public String getNombreConductorHabitual() {
		return nombreConductorHabitual;
	}
	public void setNombreConductorHabitual(String nombreConductorHabitual) {
		this.nombreConductorHabitual = nombreConductorHabitual;
	}
	public String getNombreConductorAdicional1() {
		return nombreConductorAdicional1;
	}
	public void setNombreConductorAdicional1(String nombreConductorAdicional1) {
		this.nombreConductorAdicional1 = nombreConductorAdicional1;
	}
	public String getNombreConductorAdicional2() {
		return nombreConductorAdicional2;
	}
	public void setNombreConductorAdicional2(String nombreConductorAdicional2) {
		this.nombreConductorAdicional2 = nombreConductorAdicional2;
	}
	public String getNombreConductorAdicional3() {
		return nombreConductorAdicional3;
	}
	public void setNombreConductorAdicional3(String nombreConductorAdicional3) {
		this.nombreConductorAdicional3 = nombreConductorAdicional3;
	}
	public String getNombreConductorAdicional4() {
		return nombreConductorAdicional4;
	}
	public void setNombreConductorAdicional4(String nombreConductorAdicional4) {
		this.nombreConductorAdicional4 = nombreConductorAdicional4;
	}
	public String getNombreConductorAdicional5() {
		return nombreConductorAdicional5;
	}
	public void setNombreConductorAdicional5(String nombreConductorAdicional5) {
		this.nombreConductorAdicional5 = nombreConductorAdicional5;
	}
	
	public Integer getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(Integer numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	
	public PolizaAutoSeycosPk getIdPolizaAutoSeycos() {
		return idPolizaAutoSeycos;
	}

	public void setIdPolizaAutoSeycos(PolizaAutoSeycosPk idPolizaAutoSeycos) {
		this.idPolizaAutoSeycos = idPolizaAutoSeycos;
	}

	@SuppressWarnings("unchecked")
	@Override
	public PolizaAutoSeycosPk getKey() {		
		return idPolizaAutoSeycos;
	}
	@Override
	public String getValue() {
		return null;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return null;
	}

	public String getClaveEstatus() {
		return claveEstatus;
	}

	public void setClaveEstatus(String claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	public String getNombreContratante() {
		return nombreContratante;
	}

	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}

	public String getDescripcionTipoPoliza() {
		return descripcionTipoPoliza;
	}

	public void setDescripcionTipoPoliza(String descripcionTipoPoliza) {
		this.descripcionTipoPoliza = descripcionTipoPoliza;
	}

	public String getCodigoProducto() {
		return codigoProducto;
	}

	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	public String getCodigoTipoPoliza() {
		return codigoTipoPoliza;
	}

	public void setCodigoTipoPoliza(String codigoTipoPoliza) {
		this.codigoTipoPoliza = codigoTipoPoliza;
	}

	public Integer getNumeroRenovacion() {
		return numeroRenovacion;
	}

	public void setNumeroRenovacion(Integer numeroRenovacion) {
		this.numeroRenovacion = numeroRenovacion;
	}

	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}

	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	public String getPaquete() {
		return paquete;
	}

	public void setPaquete(String paquete) {
		this.paquete = paquete;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((claveEstatus == null) ? 0 : claveEstatus.hashCode());
		result = prime * result
				+ ((codigoProducto == null) ? 0 : codigoProducto.hashCode());
		result = prime
				* result
				+ ((codigoTipoPoliza == null) ? 0 : codigoTipoPoliza.hashCode());
		result = prime
				* result
				+ ((descripcionTipoPoliza == null) ? 0 : descripcionTipoPoliza
						.hashCode());
		result = prime
				* result
				+ ((descripcionVehiculo == null) ? 0 : descripcionVehiculo
						.hashCode());
		result = prime
				* result
				+ ((fechaFinVigencia == null) ? 0 : fechaFinVigencia.hashCode());
		result = prime
				* result
				+ ((fechaInicioVigencia == null) ? 0 : fechaInicioVigencia
						.hashCode());
		result = prime
				* result
				+ ((idPolizaAutoSeycos == null) ? 0 : idPolizaAutoSeycos
						.hashCode());
		result = prime
				* result
				+ ((nombreConductorAdicional1 == null) ? 0
						: nombreConductorAdicional1.hashCode());
		result = prime
				* result
				+ ((nombreConductorAdicional2 == null) ? 0
						: nombreConductorAdicional2.hashCode());
		result = prime
				* result
				+ ((nombreConductorAdicional3 == null) ? 0
						: nombreConductorAdicional3.hashCode());
		result = prime
				* result
				+ ((nombreConductorAdicional4 == null) ? 0
						: nombreConductorAdicional4.hashCode());
		result = prime
				* result
				+ ((nombreConductorAdicional5 == null) ? 0
						: nombreConductorAdicional5.hashCode());
		result = prime
				* result
				+ ((nombreConductorHabitual == null) ? 0
						: nombreConductorHabitual.hashCode());
		result = prime
				* result
				+ ((nombreContratante == null) ? 0 : nombreContratante
						.hashCode());
		result = prime * result
				+ ((numeroMotor == null) ? 0 : numeroMotor.hashCode());
		result = prime * result
				+ ((numeroPoliza == null) ? 0 : numeroPoliza.hashCode());
		result = prime
				* result
				+ ((numeroRenovacion == null) ? 0 : numeroRenovacion.hashCode());
		result = prime * result
				+ ((numeroSerie == null) ? 0 : numeroSerie.hashCode());
		result = prime * result + ((paquete == null) ? 0 : paquete.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PolizaAutoSeycos other = (PolizaAutoSeycos) obj;
		if (claveEstatus == null) {
			if (other.claveEstatus != null)
				return false;
		} else if (!claveEstatus.equals(other.claveEstatus))
			return false;
		if (codigoProducto == null) {
			if (other.codigoProducto != null)
				return false;
		} else if (!codigoProducto.equals(other.codigoProducto))
			return false;
		if (codigoTipoPoliza == null) {
			if (other.codigoTipoPoliza != null)
				return false;
		} else if (!codigoTipoPoliza.equals(other.codigoTipoPoliza))
			return false;
		if (descripcionTipoPoliza == null) {
			if (other.descripcionTipoPoliza != null)
				return false;
		} else if (!descripcionTipoPoliza.equals(other.descripcionTipoPoliza))
			return false;
		if (descripcionVehiculo == null) {
			if (other.descripcionVehiculo != null)
				return false;
		} else if (!descripcionVehiculo.equals(other.descripcionVehiculo))
			return false;
		if (fechaFinVigencia == null) {
			if (other.fechaFinVigencia != null)
				return false;
		} else if (!fechaFinVigencia.equals(other.fechaFinVigencia))
			return false;
		if (fechaInicioVigencia == null) {
			if (other.fechaInicioVigencia != null)
				return false;
		} else if (!fechaInicioVigencia.equals(other.fechaInicioVigencia))
			return false;
		if (idPolizaAutoSeycos == null) {
			if (other.idPolizaAutoSeycos != null)
				return false;
		} else if (!idPolizaAutoSeycos.equals(other.idPolizaAutoSeycos))
			return false;
		if (nombreConductorAdicional1 == null) {
			if (other.nombreConductorAdicional1 != null)
				return false;
		} else if (!nombreConductorAdicional1
				.equals(other.nombreConductorAdicional1))
			return false;
		if (nombreConductorAdicional2 == null) {
			if (other.nombreConductorAdicional2 != null)
				return false;
		} else if (!nombreConductorAdicional2
				.equals(other.nombreConductorAdicional2))
			return false;
		if (nombreConductorAdicional3 == null) {
			if (other.nombreConductorAdicional3 != null)
				return false;
		} else if (!nombreConductorAdicional3
				.equals(other.nombreConductorAdicional3))
			return false;
		if (nombreConductorAdicional4 == null) {
			if (other.nombreConductorAdicional4 != null)
				return false;
		} else if (!nombreConductorAdicional4
				.equals(other.nombreConductorAdicional4))
			return false;
		if (nombreConductorAdicional5 == null) {
			if (other.nombreConductorAdicional5 != null)
				return false;
		} else if (!nombreConductorAdicional5
				.equals(other.nombreConductorAdicional5))
			return false;
		if (nombreConductorHabitual == null) {
			if (other.nombreConductorHabitual != null)
				return false;
		} else if (!nombreConductorHabitual
				.equals(other.nombreConductorHabitual))
			return false;
		if (nombreContratante == null) {
			if (other.nombreContratante != null)
				return false;
		} else if (!nombreContratante.equals(other.nombreContratante))
			return false;
		if (numeroMotor == null) {
			if (other.numeroMotor != null)
				return false;
		} else if (!numeroMotor.equals(other.numeroMotor))
			return false;
		if (numeroPoliza == null) {
			if (other.numeroPoliza != null)
				return false;
		} else if (!numeroPoliza.equals(other.numeroPoliza))
			return false;
		if (numeroRenovacion == null) {
			if (other.numeroRenovacion != null)
				return false;
		} else if (!numeroRenovacion.equals(other.numeroRenovacion))
			return false;
		if (numeroSerie == null) {
			if (other.numeroSerie != null)
				return false;
		} else if (!numeroSerie.equals(other.numeroSerie))
			return false;
		if (paquete == null) {
			if (other.paquete != null)
				return false;
		} else if (!paquete.equals(other.paquete))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PolizaAutoSeycos [idPolizaAutoSeycos=" + idPolizaAutoSeycos
				+ ", descripcionVehiculo=" + descripcionVehiculo
				+ ", numeroSerie=" + numeroSerie + ", numeroMotor="
				+ numeroMotor + ", nombreConductorHabitual="
				+ nombreConductorHabitual + ", nombreConductorAdicional1="
				+ nombreConductorAdicional1 + ", nombreConductorAdicional2="
				+ nombreConductorAdicional2 + ", nombreConductorAdicional3="
				+ nombreConductorAdicional3 + ", nombreConductorAdicional4="
				+ nombreConductorAdicional4 + ", nombreConductorAdicional5="
				+ nombreConductorAdicional5 + ", numeroPoliza=" + numeroPoliza
				+ ", claveEstatus=" + claveEstatus + ", nombreContratante="
				+ nombreContratante + ", descripcionTipoPoliza="
				+ descripcionTipoPoliza + ", codigoProducto=" + codigoProducto
				+ ", codigoTipoPoliza=" + codigoTipoPoliza
				+ ", numeroRenovacion=" + numeroRenovacion
				+ ", fechaInicioVigencia=" + fechaInicioVigencia
				+ ", fechaFinVigencia=" + fechaFinVigencia + ", paquete="
				+ paquete + "]";
	}
}