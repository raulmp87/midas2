package mx.com.afirme.midas2.domain.emision.ppct;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.directwebremoting.annotations.DataTransferObject;

@Entity
@Table(name="POL_RECIBO",schema="SEYCOS")
@DataTransferObject
public class ReciboSeycos implements Serializable {

	private static final long serialVersionUID = 5970215293987482934L;

	private ReciboSeycosId id;
	
	private String serie;
	
	private BigDecimal numeroFolio;
	
	private Date fechaInicioVigencia;
		
	private Date fechaFinVigencia;
	
	private Date fechaCreacion;
	
	private String cveTipoRecibo;
	
	private String sitRecibo;
	
	private BigDecimal impPrimaTotal;
	
	private BigDecimal idProgPago;
	
	private BigDecimal idCotizacion;
	
	private Date fechaTerReg;
	
	public static final String FECHA_TERMINO_REGISTRO_SEYCOS = "31/12/4712"; 

	@EmbeddedId
	public ReciboSeycosId getId() {
		return id;
	}

	public void setId(ReciboSeycosId id) {
		this.id = id;
	}

	@Column(name = "SERIE_FOLIO_FISCAL", insertable= false, updatable = false, nullable = false, length = 3)
	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	@Column(name = "NUM_FOLIO_FISCAL", insertable= false, updatable = false, nullable = false, precision = 10, scale = 0)
	public BigDecimal getNumeroFolio() {
		return numeroFolio;
	}

	public void setNumeroFolio(BigDecimal numeroFolio) {
		this.numeroFolio = numeroFolio;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "F_CUBRE_DESDE", insertable= false, updatable = false, nullable = true, length = 7)
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "F_CUBRE_HASTA", insertable= false, updatable = false, nullable = true, length = 7)
	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}

	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FH_CREACION", insertable= false, updatable = false, nullable = true, length = 7)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public void setCveTipoRecibo(String cveTipoRecibo) {
		this.cveTipoRecibo = cveTipoRecibo;
	}

	@Column(name = "CVE_T_RECIBO", insertable= false, updatable = false)
	public String getCveTipoRecibo() {
		return cveTipoRecibo;
	}

	public void setSitRecibo(String sitRecibo) {
		this.sitRecibo = sitRecibo;
	}

	@Column(name = "SIT_RECIBO", insertable= false, updatable = false)
	public String getSitRecibo() {
		return sitRecibo;
	}

	public void setImpPrimaTotal(BigDecimal impPrimaTotal) {
		this.impPrimaTotal = impPrimaTotal;
	}

	@Column(name = "IMP_PRIMA_TOTAL", insertable= false, updatable = false)
	public BigDecimal getImpPrimaTotal() {
		return impPrimaTotal;
	}
	
	@Transient
	public String getLlaveFiscal(){
		if(serie != null && numeroFolio != null){
			return serie + " " + numeroFolio;
		}
		return null;
	}

	public void setIdProgPago(BigDecimal idProgPago) {
		this.idProgPago = idProgPago;
	}

	@Column(name = "ID_PROG_PAGO", insertable= false, updatable = false)
	public BigDecimal getIdProgPago() {
		return idProgPago;
	}

	public void setIdCotizacion(BigDecimal idCotizacion) {
		this.idCotizacion = idCotizacion;
	}

	@Column(name = "ID_COTIZACION", insertable= false, updatable = false)
	public BigDecimal getIdCotizacion() {
		return idCotizacion;
	}

	public void setFechaTerReg(Date fechaTerReg) {
		this.fechaTerReg = fechaTerReg;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "F_TER_REG", length = 7, insertable= false, updatable = false)
	public Date getFechaTerReg() {
		return fechaTerReg;
	}
	
	
}
