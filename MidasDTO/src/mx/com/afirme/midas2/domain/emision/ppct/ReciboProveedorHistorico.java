package mx.com.afirme.midas2.domain.emision.ppct;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="PPCT_RECIBO_HIST",schema="MIDAS")
public class ReciboProveedorHistorico extends Recibo implements Entidad, Serializable {

	private static final long serialVersionUID = 8595577544708373106L;
	
	private BigDecimal id;
	
	private BigDecimal idOrigen;
	
	private String numeroPoliza;
	
	private Integer numeroEndoso;
	
	private String numeroInciso;
	
	private CoberturaSeycos cobertura;
	
	private ReciboSeycos reciboOriginal;
	
	private OrdenPago ordenPago;
	
	private ConfiguracionPago configuracionPago;
	
	private BigDecimal monto;
	
	private Date fechaInicioRegistro;
	
	private Date fechaFinRegistro;

	@Id
	@SequenceGenerator(name="PPCT_RECIBO_HIST_SEQ", sequenceName="MIDAS.PPCT_RECIBO_HIST_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PPCT_RECIBO_HIST_SEQ")
	@Column(name = "ID", nullable = false, precision = 22, scale = 0)
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}
	
	@Column(name = "IDORIGEN", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdOrigen() {
		return idOrigen;
	}

	public void setIdOrigen(BigDecimal idOrigen) {
		this.idOrigen = idOrigen;
	}

	@Column(name = "NUMERO_POLIZA", length = 20)
	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	@Column(name = "NUMERO_ENDOSO", nullable = false, precision = 15, scale = 0)
	public Integer getNumeroEndoso() {
		return numeroEndoso;
	}

	public void setNumeroEndoso(Integer numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}

	@Column(name = "NUMERO_INCISO", nullable = false, length = 80)
	public String getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "ID_COBERTURA")
	public CoberturaSeycos getCobertura() {
		return cobertura;
	}

	public void setCobertura(CoberturaSeycos cobertura) {
		this.cobertura = cobertura;
	}

	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumns( { 
    @JoinColumn(name="ID_RECIBO", referencedColumnName="ID_RECIBO", nullable=false, insertable=false, updatable=false), 
    @JoinColumn(name="ID_VERSION_RBO", referencedColumnName="ID_VERSION_RBO", nullable=false, insertable=false, updatable=false)})
	public ReciboSeycos getReciboOriginal() {
		return reciboOriginal;
	}

	public void setReciboOriginal(ReciboSeycos reciboOriginal) {
		this.reciboOriginal = reciboOriginal;
	}

	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "PPCT_ORDEN_PAGO_ID")
	public OrdenPago getOrdenPago() {
		return ordenPago;
	}

	public void setOrdenPago(OrdenPago ordenPago) {
		this.ordenPago = ordenPago;
	}

	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "PPCT_CONF_COBERTURA_ID")
	public ConfiguracionPago getConfiguracionPago() {
		return configuracionPago;
	}

	public void setConfiguracionPago(ConfiguracionPago configuracionPago) {
		this.configuracionPago = configuracionPago;
	}

	@Column(name = "MONTO", nullable = false, precision = 15, scale = 2)
	public BigDecimal getMonto() {
		return monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "H_START_DATE", nullable = false, length = 7)
	public Date getFechaInicioRegistro() {
		return fechaInicioRegistro;
	}

	public void setFechaInicioRegistro(Date fechaInicioRegistro) {
		this.fechaInicioRegistro = fechaInicioRegistro;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "H_END_DATE", nullable = false, length = 7)
	public Date getFechaFinRegistro() {
		return fechaFinRegistro;
	}

	public void setFechaFinRegistro(Date fechaFinRegistro) {
		this.fechaFinRegistro = fechaFinRegistro;
	}

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
