package mx.com.afirme.midas2.domain.emision.ppct;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * SubRamoSeycos entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="ING_RAMO_SUBRAMO"
    ,schema="SEYCOS"
)

public class SubRamoSeycos  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = -395462950622548833L;
	private SubRamoSeycosId id;
     private String nomSubramo;
     private String idRamoPCont;
     private String idSubrPCont;
     private String cveUsoCumulos;
     private String idRamoPCum;
     private String idSubrPCum;
     private Double pctGtosAdquis;
     private String BSubrVirtual;
     private String cveOperSrBon;
     private String cveUsoPlenos;
     private String nomSubramoOfic;
     private String uen;

    // Constructors

    /** default constructor */
    public SubRamoSeycos() {
    }

	/** minimal constructor */
    public SubRamoSeycos(SubRamoSeycosId id, String nomSubramo, String cveUsoCumulos, String BSubrVirtual) {
        this.id = id;
        this.nomSubramo = nomSubramo;
        this.cveUsoCumulos = cveUsoCumulos;
        this.BSubrVirtual = BSubrVirtual;
    }
    
    /** full constructor */
    public SubRamoSeycos(SubRamoSeycosId id, SubRamoSeycos subRamoSeycos, String nomSubramo, String idRamoPCont, String idSubrPCont, String cveUsoCumulos, String idRamoPCum, String idSubrPCum, Double pctGtosAdquis, String BSubrVirtual, String cveOperSrBon, String cveUsoPlenos, String nomSubramoOfic, String uen) {
        this.id = id;
        this.nomSubramo = nomSubramo;
        this.idRamoPCont = idRamoPCont;
        this.idSubrPCont = idSubrPCont;
        this.cveUsoCumulos = cveUsoCumulos;
        this.idRamoPCum = idRamoPCum;
        this.idSubrPCum = idSubrPCum;
        this.pctGtosAdquis = pctGtosAdquis;
        this.BSubrVirtual = BSubrVirtual;
        this.cveOperSrBon = cveOperSrBon;
        this.cveUsoPlenos = cveUsoPlenos;
        this.nomSubramoOfic = nomSubramoOfic;
        this.uen = uen;
    }

   
    // Property accessors
    @EmbeddedId
    @AttributeOverrides( {
        @AttributeOverride(name="idRamoContable", column=@Column(name="ID_RAMO_CONTABLE", nullable=false, length=2) ), 
        @AttributeOverride(name="idSubrContable", column=@Column(name="ID_SUBR_CONTABLE", nullable=false, length=2) ) } )

    public SubRamoSeycosId getId() {
        return this.id;
    }
    
    public void setId(SubRamoSeycosId id) {
        this.id = id;
    }
    
    @Column(name="NOM_SUBRAMO", nullable=false, length=30)

    public String getNomSubramo() {
        return this.nomSubramo;
    }
    
    public void setNomSubramo(String nomSubramo) {
        this.nomSubramo = nomSubramo;
    }
    
    @Column(name="ID_RAMO_P_CONT", length=2)

    public String getIdRamoPCont() {
        return this.idRamoPCont;
    }
    
    public void setIdRamoPCont(String idRamoPCont) {
        this.idRamoPCont = idRamoPCont;
    }
    
    @Column(name="ID_SUBR_P_CONT", length=2)

    public String getIdSubrPCont() {
        return this.idSubrPCont;
    }
    
    public void setIdSubrPCont(String idSubrPCont) {
        this.idSubrPCont = idSubrPCont;
    }
    
    @Column(name="CVE_USO_CUMULOS", nullable=false, length=4)

    public String getCveUsoCumulos() {
        return this.cveUsoCumulos;
    }
    
    public void setCveUsoCumulos(String cveUsoCumulos) {
        this.cveUsoCumulos = cveUsoCumulos;
    }
    
    @Column(name="ID_RAMO_P_CUM", length=2)

    public String getIdRamoPCum() {
        return this.idRamoPCum;
    }
    
    public void setIdRamoPCum(String idRamoPCum) {
        this.idRamoPCum = idRamoPCum;
    }
    
    @Column(name="ID_SUBR_P_CUM", length=2)

    public String getIdSubrPCum() {
        return this.idSubrPCum;
    }
    
    public void setIdSubrPCum(String idSubrPCum) {
        this.idSubrPCum = idSubrPCum;
    }
    
    @Column(name="PCT_GTOS_ADQUIS", precision=8, scale=4)

    public Double getPctGtosAdquis() {
        return this.pctGtosAdquis;
    }
    
    public void setPctGtosAdquis(Double pctGtosAdquis) {
        this.pctGtosAdquis = pctGtosAdquis;
    }
    
    @Column(name="B_SUBR_VIRTUAL", nullable=false, length=1)

    public String getBSubrVirtual() {
        return this.BSubrVirtual;
    }
    
    public void setBSubrVirtual(String BSubrVirtual) {
        this.BSubrVirtual = BSubrVirtual;
    }
    
    @Column(name="CVE_OPER_SR_BON", length=1)

    public String getCveOperSrBon() {
        return this.cveOperSrBon;
    }
    
    public void setCveOperSrBon(String cveOperSrBon) {
        this.cveOperSrBon = cveOperSrBon;
    }
    
    @Column(name="CVE_USO_PLENOS", length=3)

    public String getCveUsoPlenos() {
        return this.cveUsoPlenos;
    }
    
    public void setCveUsoPlenos(String cveUsoPlenos) {
        this.cveUsoPlenos = cveUsoPlenos;
    }
    
    @Column(name="NOM_SUBRAMO_OFIC", length=50)

    public String getNomSubramoOfic() {
        return this.nomSubramoOfic;
    }
    
    public void setNomSubramoOfic(String nomSubramoOfic) {
        this.nomSubramoOfic = nomSubramoOfic;
    }
    
    @Column(name="UEN", length=16)

    public String getUen() {
        return this.uen;
    }
    
    public void setUen(String uen) {
        this.uen = uen;
    }

}