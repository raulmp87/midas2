package mx.com.afirme.midas2.domain.emision.ppct;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;


/**
 * ProductoSeycos entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="DP_PRODUCTO"
    ,schema="SEYCOS"
, uniqueConstraints = @UniqueConstraint(columnNames={"ID_PRODUCTO", "NOM_PRODUCTO"})
)

public class ProductoSeycos  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 7942209197266980645L;
	 private Long idProducto;
     private String nomProducto;
     private String nomCtoProd;
     private String BRenovacion;
     private String cveTPoliza;
     private String cveEmiRecibo;
 	 private Integer checado;
    // Constructors

    /** default constructor */
    public ProductoSeycos() {
    }

	/** minimal constructor */
    public ProductoSeycos(Long idProducto) {
        this.idProducto = idProducto;
    }
    
    /** full constructor */
    public ProductoSeycos(Long idProducto, String nomProducto, String nomCtoProd, String BRenovacion, String cveTPoliza, String cveEmiRecibo) {
        this.idProducto = idProducto;
        this.nomProducto = nomProducto;
        this.nomCtoProd = nomCtoProd;
        this.BRenovacion = BRenovacion;
        this.cveTPoliza = cveTPoliza;
        this.cveEmiRecibo = cveEmiRecibo;
    }

   
    // Property accessors
    @Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ01_DP_PRODUCTO")
	@SequenceGenerator(name="SQ01_DP_PRODUCTO", schema="SEYCOS",sequenceName="SQ01_DP_PRODUCTO",allocationSize=1)
	@Column(name="ID_PRODUCTO",nullable=false)
    public Long getIdProducto() {
        return this.idProducto;
    }
    
    public void setIdProducto(Long idProducto) {
        this.idProducto = idProducto;
    }
    
    @Column(name="NOM_PRODUCTO", length=30)

    public String getNomProducto() {
        return this.nomProducto;
    }
    
    public void setNomProducto(String nomProducto) {
        this.nomProducto = nomProducto;
    }
    
    @Column(name="NOM_CTO_PROD", length=10)

    public String getNomCtoProd() {
        return this.nomCtoProd;
    }
    
    public void setNomCtoProd(String nomCtoProd) {
        this.nomCtoProd = nomCtoProd;
    }
    
    @Column(name="B_RENOVACION", length=1)

    public String getBRenovacion() {
        return this.BRenovacion;
    }
    
    public void setBRenovacion(String BRenovacion) {
        this.BRenovacion = BRenovacion;
    }
    
    @Column(name="CVE_T_POLIZA", length=4)

    public String getCveTPoliza() {
        return this.cveTPoliza;
    }
    
    public void setCveTPoliza(String cveTPoliza) {
        this.cveTPoliza = cveTPoliza;
    }
    
    @Column(name="CVE_EMI_RECIBO", length=5)

    public String getCveEmiRecibo() {
        return this.cveEmiRecibo;
    }
    
    public void setCveEmiRecibo(String cveEmiRecibo) {
        this.cveEmiRecibo = cveEmiRecibo;
    }
    
    @Transient
	public Integer getChecado() {
		return checado;
	}

	public void setChecado(Integer checado) {
		this.checado = checado;
	}

}