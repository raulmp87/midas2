package mx.com.afirme.midas2.domain.emision.ppct;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ReciboSeycosId implements Serializable {

	private static final long serialVersionUID = 1856349735448122769L;

	
	private BigDecimal idRecibo;
	
	
	private BigDecimal idVersion;
		
	
	
	@Column(name = "ID_RECIBO", nullable = false, precision = 8, scale = 0)
	public BigDecimal getIdRecibo() {
		return idRecibo;
	}

	public void setIdRecibo(BigDecimal idRecibo) {
		this.idRecibo = idRecibo;
	}
	
	@Column(name = "ID_VERSION_RBO", nullable = false, precision = 3, scale = 0)
	public BigDecimal getIdVersion() {
		return idVersion;
	}

	public void setIdVersion(BigDecimal idVersion) {
		this.idVersion = idVersion;
	}
	
}
