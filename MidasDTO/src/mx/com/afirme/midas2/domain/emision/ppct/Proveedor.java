package mx.com.afirme.midas2.domain.emision.ppct;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.directwebremoting.annotations.DataTransferObject;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="PPCT_PROVEEDOR",schema="MIDAS")
@DataTransferObject
public class Proveedor implements Serializable, Entidad {

	private static final long serialVersionUID = -384893138835009234L;

	private BigDecimal id;
	
	private Integer clave;
	
	private String nombre;
	
	private String razonSocial;
	
	private String rfc;
	
	private BigDecimal idCuentaContable;
	
	
	@Id
	@SequenceGenerator(name="PPCT_PROVEEDOR_SEQ", sequenceName="MIDAS.PPCT_PROVEEDOR_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PPCT_PROVEEDOR_SEQ")
	@Column(name = "ID", nullable = false, precision = 22, scale = 0)
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	@Column(name = "CLAVE", nullable = false, precision = 8, scale = 0)
	public Integer getClave() {
		return clave;
	}

	public void setClave(Integer clave) {
		this.clave = clave;
	}

	@Column(name = "NOMBRE", nullable = false, length = 200)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "RAZON_SOCIAL", nullable = false, length = 200)
	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	@Column(name = "RFC", nullable = false, length = 15)
	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	@Column(name = "ID_CUENTA_CONT", nullable = false, precision = 8, scale = 0)
	public BigDecimal getIdCuentaContable() {
		return idCuentaContable;
	}

	public void setIdCuentaContable(BigDecimal idCuentaContable) {
		this.idCuentaContable = idCuentaContable;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return nombre;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Integer getBusinessKey() {
		return clave;
	}
	
}
