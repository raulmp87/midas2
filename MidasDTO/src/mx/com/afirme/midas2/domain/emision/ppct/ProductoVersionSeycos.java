package mx.com.afirme.midas2.domain.emision.ppct;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * ProductoVersionSeycos entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="DP_PRODUCTO_VER"
    ,schema="SEYCOS"
)

public class ProductoVersionSeycos  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 5018549104380518286L;
	private ProductoVersionSeycosId id;
     private ProductoSeycos productoSeycos;
     private Date FVigDesde;
     private Date FVigHasta;
     private String cveTDerechos;
     private String cveUnidadPzo;
     private Short plazoMin;
     private Short plazoMax;
     private Short plazoDft;
     private String idFmtoPredis;
     private String sitProducto;
     private Date FSit;
     private String txObserv;
     private String BPagoInmediato;
     private String BCalcProrrata;
     private String BCalcProrrRcg;
     private Integer idTrProrrRcg;
     private String BCalcCortoPzo;
     private Integer idTrCortoPzo;
     private String cveTCalculo;
     private String BCalcProrratam;
     private String BCalcProrrRcm;
     private Integer idTrProrrRcgm;
     private String BCalcLargoPzo;
     private Integer idTrLargoPzo;
     private String cveTCalculoM;
     private String BMesesCompl;
     private Date fhCreacion;
     private String idUsuarioCreac;
     private Date fhActivacion;
     private String idUsuarioActiv;
     private Date FIniReg;
     private Date FTerReg;
     private String BUltRegDia;
     private Short diasRetroact;
     private Short diasDiferim;
     private Byte aniosPagoPrim;
     private String BProdExclus;
     private Integer idCliente;
     private String cveEmiRecibo;
     private String BConsolidaRbos;
    // Constructors

    /** default constructor */
    public ProductoVersionSeycos() {
    }

	/** minimal constructor */
    public ProductoVersionSeycos(ProductoVersionSeycosId id) {
        this.id = id;
    }
    
    /** full constructor */
    public ProductoVersionSeycos(ProductoVersionSeycosId id, ProductoSeycos productoSeycos, Date FVigDesde, Date FVigHasta, String cveTDerechos, String cveUnidadPzo, Short plazoMin, Short plazoMax, Short plazoDft, String idFmtoPredis, String sitProducto, Date FSit, String txObserv, String BPagoInmediato, String BCalcProrrata, String BCalcProrrRcg, Integer idTrProrrRcg, String BCalcCortoPzo, Integer idTrCortoPzo, String cveTCalculo, String BCalcProrratam, String BCalcProrrRcm, Integer idTrProrrRcgm, String BCalcLargoPzo, Integer idTrLargoPzo, String cveTCalculoM, String BMesesCompl, Date fhCreacion, String idUsuarioCreac, Date fhActivacion, String idUsuarioActiv, Date FIniReg, Date FTerReg, String BUltRegDia, Short diasRetroact, Short diasDiferim, Byte aniosPagoPrim, String BProdExclus, Integer idCliente, String cveEmiRecibo, String BConsolidaRbos) {
        this.id = id;
        this.productoSeycos = productoSeycos;
        this.FVigDesde = FVigDesde;
        this.FVigHasta = FVigHasta;
        this.cveTDerechos = cveTDerechos;
        this.cveUnidadPzo = cveUnidadPzo;
        this.plazoMin = plazoMin;
        this.plazoMax = plazoMax;
        this.plazoDft = plazoDft;
        this.idFmtoPredis = idFmtoPredis;
        this.sitProducto = sitProducto;
        this.FSit = FSit;
        this.txObserv = txObserv;
        this.BPagoInmediato = BPagoInmediato;
        this.BCalcProrrata = BCalcProrrata;
        this.BCalcProrrRcg = BCalcProrrRcg;
        this.idTrProrrRcg = idTrProrrRcg;
        this.BCalcCortoPzo = BCalcCortoPzo;
        this.idTrCortoPzo = idTrCortoPzo;
        this.cveTCalculo = cveTCalculo;
        this.BCalcProrratam = BCalcProrratam;
        this.BCalcProrrRcm = BCalcProrrRcm;
        this.idTrProrrRcgm = idTrProrrRcgm;
        this.BCalcLargoPzo = BCalcLargoPzo;
        this.idTrLargoPzo = idTrLargoPzo;
        this.cveTCalculoM = cveTCalculoM;
        this.BMesesCompl = BMesesCompl;
        this.fhCreacion = fhCreacion;
        this.idUsuarioCreac = idUsuarioCreac;
        this.fhActivacion = fhActivacion;
        this.idUsuarioActiv = idUsuarioActiv;
        this.FIniReg = FIniReg;
        this.FTerReg = FTerReg;
        this.BUltRegDia = BUltRegDia;
        this.diasRetroact = diasRetroact;
        this.diasDiferim = diasDiferim;
        this.aniosPagoPrim = aniosPagoPrim;
        this.BProdExclus = BProdExclus;
        this.idCliente = idCliente;
        this.cveEmiRecibo = cveEmiRecibo;
        this.BConsolidaRbos = BConsolidaRbos;
    }

   
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idProducto", column=@Column(name="ID_PRODUCTO", precision=5, scale=0) ), 
        @AttributeOverride(name="idVersion", column=@Column(name="ID_VERSION", precision=3, scale=0) ) } )

    public ProductoVersionSeycosId getId() {
        return this.id;
    }
    
    public void setId(ProductoVersionSeycosId id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="ID_PRODUCTO", insertable=false, updatable=false)

    public ProductoSeycos getProductoSeycos() {
        return this.productoSeycos;
    }
    
    public void setProductoSeycos(ProductoSeycos productoSeycos) {
        this.productoSeycos = productoSeycos;
    }
    
    @Temporal(TemporalType.DATE)
    @Column(name="F_VIG_DESDE", length=7)
    public Date getFVigDesde() {
        return this.FVigDesde;
    }
    
    public void setFVigDesde(Date FVigDesde) {
        this.FVigDesde = FVigDesde;
    }
    
    @Temporal(TemporalType.DATE)
    @Column(name="F_VIG_HASTA", length=7)
    public Date getFVigHasta() {
        return this.FVigHasta;
    }
    
    public void setFVigHasta(Date FVigHasta) {
        this.FVigHasta = FVigHasta;
    }
    
    @Column(name="CVE_T_DERECHOS", length=2)

    public String getCveTDerechos() {
        return this.cveTDerechos;
    }
    
    public void setCveTDerechos(String cveTDerechos) {
        this.cveTDerechos = cveTDerechos;
    }
    
    @Column(name="CVE_UNIDAD_PZO", length=1)

    public String getCveUnidadPzo() {
        return this.cveUnidadPzo;
    }
    
    public void setCveUnidadPzo(String cveUnidadPzo) {
        this.cveUnidadPzo = cveUnidadPzo;
    }
    
    @Column(name="PLAZO_MIN", precision=3, scale=0)

    public Short getPlazoMin() {
        return this.plazoMin;
    }
    
    public void setPlazoMin(Short plazoMin) {
        this.plazoMin = plazoMin;
    }
    
    @Column(name="PLAZO_MAX", precision=3, scale=0)

    public Short getPlazoMax() {
        return this.plazoMax;
    }
    
    public void setPlazoMax(Short plazoMax) {
        this.plazoMax = plazoMax;
    }
    
    @Column(name="PLAZO_DFT", precision=3, scale=0)

    public Short getPlazoDft() {
        return this.plazoDft;
    }
    
    public void setPlazoDft(Short plazoDft) {
        this.plazoDft = plazoDft;
    }
    
    @Column(name="ID_FMTO_PREDIS", length=10)

    public String getIdFmtoPredis() {
        return this.idFmtoPredis;
    }
    
    public void setIdFmtoPredis(String idFmtoPredis) {
        this.idFmtoPredis = idFmtoPredis;
    }
    
    @Column(name="SIT_PRODUCTO", length=2)

    public String getSitProducto() {
        return this.sitProducto;
    }
    
    public void setSitProducto(String sitProducto) {
        this.sitProducto = sitProducto;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="F_SIT", length=7)
    public Date getFSit() {
        return this.FSit;
    }
    
    public void setFSit(Date FSit) {
        this.FSit = FSit;
    }
    
    @Column(name="TX_OBSERV", length=2000)

    public String getTxObserv() {
        return this.txObserv;
    }
    
    public void setTxObserv(String txObserv) {
        this.txObserv = txObserv;
    }
    
    @Column(name="B_PAGO_INMEDIATO", length=1)

    public String getBPagoInmediato() {
        return this.BPagoInmediato;
    }
    
    public void setBPagoInmediato(String BPagoInmediato) {
        this.BPagoInmediato = BPagoInmediato;
    }
    
    @Column(name="B_CALC_PRORRATA", length=1)

    public String getBCalcProrrata() {
        return this.BCalcProrrata;
    }
    
    public void setBCalcProrrata(String BCalcProrrata) {
        this.BCalcProrrata = BCalcProrrata;
    }
    
    @Column(name="B_CALC_PRORR_RCG", length=1)

    public String getBCalcProrrRcg() {
        return this.BCalcProrrRcg;
    }
    
    public void setBCalcProrrRcg(String BCalcProrrRcg) {
        this.BCalcProrrRcg = BCalcProrrRcg;
    }
    
    @Column(name="ID_TR_PRORR_RCG", precision=5, scale=0)

    public Integer getIdTrProrrRcg() {
        return this.idTrProrrRcg;
    }
    
    public void setIdTrProrrRcg(Integer idTrProrrRcg) {
        this.idTrProrrRcg = idTrProrrRcg;
    }
    
    @Column(name="B_CALC_CORTO_PZO", length=1)

    public String getBCalcCortoPzo() {
        return this.BCalcCortoPzo;
    }
    
    public void setBCalcCortoPzo(String BCalcCortoPzo) {
        this.BCalcCortoPzo = BCalcCortoPzo;
    }
    
    @Column(name="ID_TR_CORTO_PZO", precision=5, scale=0)

    public Integer getIdTrCortoPzo() {
        return this.idTrCortoPzo;
    }
    
    public void setIdTrCortoPzo(Integer idTrCortoPzo) {
        this.idTrCortoPzo = idTrCortoPzo;
    }
    
    @Column(name="CVE_T_CALCULO", length=3)

    public String getCveTCalculo() {
        return this.cveTCalculo;
    }
    
    public void setCveTCalculo(String cveTCalculo) {
        this.cveTCalculo = cveTCalculo;
    }
    
    @Column(name="B_CALC_PRORRATAM", length=1)

    public String getBCalcProrratam() {
        return this.BCalcProrratam;
    }
    
    public void setBCalcProrratam(String BCalcProrratam) {
        this.BCalcProrratam = BCalcProrratam;
    }
    
    @Column(name="B_CALC_PRORR_RCM", length=1)

    public String getBCalcProrrRcm() {
        return this.BCalcProrrRcm;
    }
    
    public void setBCalcProrrRcm(String BCalcProrrRcm) {
        this.BCalcProrrRcm = BCalcProrrRcm;
    }
    
    @Column(name="ID_TR_PRORR_RCGM", precision=5, scale=0)

    public Integer getIdTrProrrRcgm() {
        return this.idTrProrrRcgm;
    }
    
    public void setIdTrProrrRcgm(Integer idTrProrrRcgm) {
        this.idTrProrrRcgm = idTrProrrRcgm;
    }
    
    @Column(name="B_CALC_LARGO_PZO", length=1)

    public String getBCalcLargoPzo() {
        return this.BCalcLargoPzo;
    }
    
    public void setBCalcLargoPzo(String BCalcLargoPzo) {
        this.BCalcLargoPzo = BCalcLargoPzo;
    }
    
    @Column(name="ID_TR_LARGO_PZO", precision=5, scale=0)

    public Integer getIdTrLargoPzo() {
        return this.idTrLargoPzo;
    }
    
    public void setIdTrLargoPzo(Integer idTrLargoPzo) {
        this.idTrLargoPzo = idTrLargoPzo;
    }
    
    @Column(name="CVE_T_CALCULO_M", length=3)

    public String getCveTCalculoM() {
        return this.cveTCalculoM;
    }
    
    public void setCveTCalculoM(String cveTCalculoM) {
        this.cveTCalculoM = cveTCalculoM;
    }
    
    @Column(name="B_MESES_COMPL", length=1)

    public String getBMesesCompl() {
        return this.BMesesCompl;
    }
    
    public void setBMesesCompl(String BMesesCompl) {
        this.BMesesCompl = BMesesCompl;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FH_CREACION", length=7)
    public Date getFhCreacion() {
        return this.fhCreacion;
    }
    
    public void setFhCreacion(Date fhCreacion) {
        this.fhCreacion = fhCreacion;
    }
    
    @Column(name="ID_USUARIO_CREAC", length=8)

    public String getIdUsuarioCreac() {
        return this.idUsuarioCreac;
    }
    
    public void setIdUsuarioCreac(String idUsuarioCreac) {
        this.idUsuarioCreac = idUsuarioCreac;
    }
    
    @Temporal(TemporalType.DATE)
    @Column(name="FH_ACTIVACION", length=7)
    public Date getFhActivacion() {
        return this.fhActivacion;
    }
    
    public void setFhActivacion(Date fhActivacion) {
        this.fhActivacion = fhActivacion;
    }
    
    @Column(name="ID_USUARIO_ACTIV", length=8)

    public String getIdUsuarioActiv() {
        return this.idUsuarioActiv;
    }
    
    public void setIdUsuarioActiv(String idUsuarioActiv) {
        this.idUsuarioActiv = idUsuarioActiv;
    }
    
    @Temporal(TemporalType.DATE)
    @Column(name="F_INI_REG", length=7)
    public Date getFIniReg() {
        return this.FIniReg;
    }
    
    public void setFIniReg(Date FIniReg) {
        this.FIniReg = FIniReg;
    }
    
    @Temporal(TemporalType.DATE)
    @Column(name="F_TER_REG", length=7)
    public Date getFTerReg() {
        return this.FTerReg;
    }
    
    public void setFTerReg(Date FTerReg) {
        this.FTerReg = FTerReg;
    }
    
    @Column(name="B_ULT_REG_DIA", length=1)

    public String getBUltRegDia() {
        return this.BUltRegDia;
    }
    
    public void setBUltRegDia(String BUltRegDia) {
        this.BUltRegDia = BUltRegDia;
    }
    
    @Column(name="DIAS_RETROACT", precision=4, scale=0)

    public Short getDiasRetroact() {
        return this.diasRetroact;
    }
    
    public void setDiasRetroact(Short diasRetroact) {
        this.diasRetroact = diasRetroact;
    }
    
    @Column(name="DIAS_DIFERIM", precision=4, scale=0)

    public Short getDiasDiferim() {
        return this.diasDiferim;
    }
    
    public void setDiasDiferim(Short diasDiferim) {
        this.diasDiferim = diasDiferim;
    }
    
    @Column(name="ANIOS_PAGO_PRIM", precision=2, scale=0)

    public Byte getAniosPagoPrim() {
        return this.aniosPagoPrim;
    }
    
    public void setAniosPagoPrim(Byte aniosPagoPrim) {
        this.aniosPagoPrim = aniosPagoPrim;
    }
    
    @Column(name="B_PROD_EXCLUS", length=1)

    public String getBProdExclus() {
        return this.BProdExclus;
    }
    
    public void setBProdExclus(String BProdExclus) {
        this.BProdExclus = BProdExclus;
    }
    
    @Column(name="ID_CLIENTE", precision=8, scale=0)

    public Integer getIdCliente() {
        return this.idCliente;
    }
    
    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }
    
    @Column(name="CVE_EMI_RECIBO", length=5)

    public String getCveEmiRecibo() {
        return this.cveEmiRecibo;
    }
    
    public void setCveEmiRecibo(String cveEmiRecibo) {
        this.cveEmiRecibo = cveEmiRecibo;
    }
    
    @Column(name="B_CONSOLIDA_RBOS", length=1)

    public String getBConsolidaRbos() {
        return this.BConsolidaRbos;
    }
    
    public void setBConsolidaRbos(String BConsolidaRbos) {
        this.BConsolidaRbos = BConsolidaRbos;
    }

}