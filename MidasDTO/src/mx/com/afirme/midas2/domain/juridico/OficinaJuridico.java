package mx.com.afirme.midas2.domain.juridico;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;

@Entity(name = "OficinaJuridico")
@Table(name = "TCOFICINAJURIDICO", schema = "MIDAS")
public class OficinaJuridico extends MidasAbstracto implements Entidad{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3528935943214253703L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TCOFICINAJURIDICO_ID_GENERATOR")
	@SequenceGenerator(name="TCOFICINAJURIDICO_ID_GENERATOR", schema="MIDAS", sequenceName="TCOFICINAJURIDICO_SEQ",allocationSize=1)
	@Column(name="ID")	
	private	Long id;
	
	@Column(name = "CLAVE_OFICINA", nullable = false)
	private	String claveOficina;
	
	@Column(name = "NOMBRE_OFICINA", nullable = false)
	private	String nombreOficina;
	
	@Column(name = "ESTATUS", nullable = false)
	private String estatus;	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getClaveOficina() {
		return claveOficina;
	}
	public void setClaveOficina(String claveOficina) {
		this.claveOficina = claveOficina;
	}
	public String getNombreOficina() {
		return nombreOficina;
	}
	public void setNombreOficina(String nombreOficina) {
		this.nombreOficina = nombreOficina;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	
	@Override
	public Long getKey() {
		
		return id;
	}
	@Override
	public String getValue() {
		
		return nombreOficina;
	}
	@Override
	public String getBusinessKey() {
		
		return claveOficina;
	}	

}
