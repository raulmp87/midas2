package mx.com.afirme.midas2.domain.juridico;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;

@Entity
@Table(name="TORECLAMACIONJURIDICO", schema="MIDAS")
public class ReclamacionJuridico extends MidasAbstracto{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3456245086157604704L;
	
	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="RECLAMACIONJURIDICO_GENERATOR")
	@SequenceGenerator(name="RECLAMACIONJURIDICO_GENERATOR", sequenceName="TORECLAMACIONJURIDICO_SEQ", schema="MIDAS", allocationSize=1)
	private Long id;
	
	@Column(name="ESTATUS", nullable=false)
	private String estatus;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_NOTIFICACION")
	private Date fechaNotificacion;
	
	@Column(name="RAMO")
	private String ramo;
	
	@Column(name="NUMERO_POLIZA")
	private String numeroPoliza;
	
	@Column(name="NUMERO_SINIESTRO")
	private String numeroSiniestro;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="OFICINASINIESTRO_ID", referencedColumnName="ID")
	private Oficina oficinaSiniestro;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SINIESTRO_ID", referencedColumnName="ID")
	private SiniestroCabina siniestro;
	
	@Column(name="TIPO_RECLAMACION", nullable=false)
	private String tipoReclamacion;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="OFICINAJURIDICO_ID", nullable=false, referencedColumnName="ID")
	private OficinaJuridico oficinaJuridico;
	
	@OneToMany(mappedBy="reclamacionJuridico", fetch = FetchType.LAZY)
	private List<ReclamacionOficioJuridico> oficios;

	public static enum TIPOS_RECLAMACION{
		LITIGIOS("LI"), GESTION_ELECTRONICA("GE"), CONDUSEF("CO"), ALTA_AUTORIDAD("AA");
		public final String codigo;
		TIPOS_RECLAMACION(String codigo){
			this.codigo = codigo;
		}
	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return the fechaNotificacion
	 */
	public Date getFechaNotificacion() {
		return fechaNotificacion;
	}

	/**
	 * @param fechaNotificacion the fechaNotificacion to set
	 */
	public void setFechaNotificacion(Date fechaNotificacion) {
		this.fechaNotificacion = fechaNotificacion;
	}

	/**
	 * @return the ramo
	 */
	public String getRamo() {
		return ramo;
	}

	/**
	 * @param ramo the ramo to set
	 */
	public void setRamo(String ramo) {
		this.ramo = ramo;
	}

	/**
	 * @return the tipoReclamacion
	 */
	public String getTipoReclamacion() {
		return tipoReclamacion;
	}

	/**
	 * @param tipoReclamacion the tipoReclamacion to set
	 */
	public void setTipoReclamacion(String tipoReclamacion) {
		this.tipoReclamacion = tipoReclamacion;
	}

	/**
	 * @return the oficinaJuridico
	 */
	public OficinaJuridico getOficinaJuridico() {
		return oficinaJuridico;
	}

	/**
	 * @param oficinaJuridico the oficinaJuridico to set
	 */
	public void setOficinaJuridico(OficinaJuridico oficinaJuridico) {
		this.oficinaJuridico = oficinaJuridico;
	}

	/**
	 * @return the oficios
	 */
	public List<ReclamacionOficioJuridico> getOficios() {
		return oficios;
	}

	/**
	 * @param oficios the oficios to set
	 */
	public void setOficios(List<ReclamacionOficioJuridico> oficios) {
		this.oficios = oficios;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	/**
	 * @return the siniestro
	 */
	public SiniestroCabina getSiniestro() {
		return siniestro;
	}

	/**
	 * @param siniestro the siniestro to set
	 */
	public void setSiniestro(SiniestroCabina siniestro) {
		this.siniestro = siniestro;
	}

	/**
	 * @return the numeroPoliza
	 */
	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	/**
	 * @param numeroPoliza the numeroPoliza to set
	 */
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	/**
	 * @return the oficinaSiniestro
	 */
	public Oficina getOficinaSiniestro() {
		return oficinaSiniestro;
	}

	/**
	 * @param oficinaSiniestro the oficinaSiniestro to set
	 */
	public void setOficinaSiniestro(Oficina oficinaSiniestro) {
		this.oficinaSiniestro = oficinaSiniestro;
	}

	/**
	 * @return the numeroSiniestro
	 */
	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}

	/**
	 * @param numeroSiniestro the numeroSiniestro to set
	 */
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}

}