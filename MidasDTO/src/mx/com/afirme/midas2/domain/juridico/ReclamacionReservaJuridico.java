package mx.com.afirme.midas2.domain.juridico;

import java.math.BigDecimal;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.MidasAbstracto;

@Entity
@Table(name="TORECLAMACIONRESERVAJURIDICO", schema="MIDAS")
public class ReclamacionReservaJuridico extends MidasAbstracto{

	private static final long serialVersionUID = -607401523851102147L;

	@AttributeOverrides( {
			@AttributeOverride(name = "oficio", column = @Column(name = "OFICIO", nullable = false)),
			@AttributeOverride(name = "coberturaId", column = @Column(name = "COBERTURA_ID", nullable = false)),
			@AttributeOverride(name = "claveSubCobertura", column = @Column(name = "CLAVE_SUBCOBERTURA", nullable = false)),
			@AttributeOverride(name = "reclamacionJuridicoId", column = @Column(name = "RECLAMACIONJURIDICO_ID", nullable = false)) })
	@EmbeddedId
	private ReclamacionReservaIdJuridico id;
	
	@Column(name="MONTO_RESERVA_AUTORIDAD")
	private BigDecimal montoReservadoAutoridad;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( { 
        @JoinColumn(name="OFICIO", referencedColumnName="OFICIO", nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="RECLAMACIONJURIDICO_ID", referencedColumnName="RECLAMACIONJURIDICO_ID", nullable=false, insertable=false, updatable=false) })
	private ReclamacionOficioJuridico reclamacionOficio;

	@Transient
	private BigDecimal reservaSistema;
	
	/**
	 * @return the id
	 */
	public ReclamacionReservaIdJuridico getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(ReclamacionReservaIdJuridico id) {
		this.id = id;
	}

	/**
	 * @return the montoReservadoAutoridad
	 */
	public BigDecimal getMontoReservadoAutoridad() {
		return montoReservadoAutoridad;
	}

	/**
	 * @param montoReservadoAutoridad the montoReservadoAutoridad to set
	 */
	public void setMontoReservadoAutoridad(BigDecimal montoReservadoAutoridad) {
		this.montoReservadoAutoridad = montoReservadoAutoridad;
	}

	/**
	 * @return the reclamacionOficio
	 */
	public ReclamacionOficioJuridico getReclamacionOficio() {
		return reclamacionOficio;
	}

	/**
	 * @param reclamacionOficio the reclamacionOficio to set
	 */
	public void setReclamacionOficio(ReclamacionOficioJuridico reclamacionOficio) {
		this.reclamacionOficio = reclamacionOficio;
	}

	/**
	 * @return the reservaSistema
	 */
	public BigDecimal getReservaSistema() {
		return reservaSistema;
	}

	/**
	 * @param reservaSistema the reservaSistema to set
	 */
	public void setReservaSistema(BigDecimal reservaSistema) {
		this.reservaSistema = reservaSistema;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ReclamacionReservaIdJuridico getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ReclamacionReservaIdJuridico getBusinessKey() {
		return id;
	}

}