package mx.com.afirme.midas2.domain.juridico;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ReclamacionOficioIdJuridico implements Serializable{

	private static final long serialVersionUID = 3722682888899734173L;
	
	@Column(name = "RECLAMACIONJURIDICO_ID", nullable = false)
	private Long reclamacionJuridicoId;
	
	@Column(name = "OFICIO", nullable = false)
	private Integer oficio;
	
	/**
	 * default constructor
	 */
	public ReclamacionOficioIdJuridico(){
	}
	
	/**
	 * full constructor
	 * @param oficio
	 * @param reclamacionJuridicoId
	 */
	public ReclamacionOficioIdJuridico(Long reclamacionJuridicoId,
			Integer oficio) {
		super();
		this.oficio = oficio;
		this.reclamacionJuridicoId = reclamacionJuridicoId;
	}
	
	/**
	 * @return the oficio
	 */
	public Integer getOficio() {
		return oficio;
	}
	/**
	 * @param oficio the oficio to set
	 */
	public void setOficio(Integer oficio) {
		this.oficio = oficio;
	}
	/**
	 * @return the reclamacionJuridicoId
	 */
	public Long getReclamacionJuridicoId() {
		return reclamacionJuridicoId;
	}
	/**
	 * @param reclamacionJuridicoId the reclamacionJuridicoId to set
	 */
	public void setReclamacionJuridicoId(Long reclamacionJuridicoId) {
		this.reclamacionJuridicoId = reclamacionJuridicoId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((oficio == null) ? 0 : oficio.hashCode());
		result = prime
				* result
				+ ((reclamacionJuridicoId == null) ? 0 : reclamacionJuridicoId
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReclamacionOficioIdJuridico other = (ReclamacionOficioIdJuridico) obj;
		if (oficio == null) {
			if (other.oficio != null)
				return false;
		} else if (!oficio.equals(other.oficio))
			return false;
		if (reclamacionJuridicoId == null) {
			if (other.reclamacionJuridicoId != null)
				return false;
		} else if (!reclamacionJuridicoId.equals(other.reclamacionJuridicoId))
			return false;
		return true;
	}

}
