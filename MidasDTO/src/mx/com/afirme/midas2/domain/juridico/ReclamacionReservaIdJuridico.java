package mx.com.afirme.midas2.domain.juridico;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ReclamacionReservaIdJuridico implements Serializable {

	private static final long serialVersionUID = -2166759875261831312L;

	@Column(name = "RECLAMACIONJURIDICO_ID", nullable = false)
	private Long reclamacionJuridicoId;

	@Column(name = "OFICIO", nullable = false)
	private Integer oficio;

	@Column(name = "COBERTURA_ID", nullable = false)
	private Long coberturaId;

	@Column(name = "CLAVE_SUBCOBERTURA", nullable = false)
	private String claveSubCobertura;

	/**
	 * default constructor
	 */
	public ReclamacionReservaIdJuridico() {
	}

	public ReclamacionReservaIdJuridico(Long reclamacionJuridicoId,
			Integer oficio, Long coberturaId, String claveSubCobertura) {
		super();
		this.reclamacionJuridicoId = reclamacionJuridicoId;
		this.oficio = oficio;
		this.coberturaId = coberturaId;
		this.claveSubCobertura = claveSubCobertura;
	}

	/**
	 * @return the reclamacionJuridicoId
	 */
	public Long getReclamacionJuridicoId() {
		return reclamacionJuridicoId;
	}

	/**
	 * @param reclamacionJuridicoId
	 *            the reclamacionJuridicoId to set
	 */
	public void setReclamacionJuridicoId(Long reclamacionJuridicoId) {
		this.reclamacionJuridicoId = reclamacionJuridicoId;
	}

	/**
	 * @return the oficio
	 */
	public Integer getOficio() {
		return oficio;
	}

	/**
	 * @param oficio
	 *            the oficio to set
	 */
	public void setOficio(Integer oficio) {
		this.oficio = oficio;
	}

	/**
	 * @return the coberturaId
	 */
	public Long getCoberturaId() {
		return coberturaId;
	}

	/**
	 * @param coberturaId
	 *            the coberturaId to set
	 */
	public void setCoberturaId(Long coberturaId) {
		this.coberturaId = coberturaId;
	}

	/**
	 * @return the claveSubCobertura
	 */
	public String getClaveSubCobertura() {
		return claveSubCobertura;
	}

	/**
	 * @param claveSubCobertura
	 *            the claveSubCobertura to set
	 */
	public void setClaveSubCobertura(String claveSubCobertura) {
		this.claveSubCobertura = claveSubCobertura;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((claveSubCobertura == null) ? 0 : claveSubCobertura
						.hashCode());
		result = prime * result
				+ ((coberturaId == null) ? 0 : coberturaId.hashCode());
		result = prime * result + ((oficio == null) ? 0 : oficio.hashCode());
		result = prime
				* result
				+ ((reclamacionJuridicoId == null) ? 0 : reclamacionJuridicoId
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReclamacionReservaIdJuridico other = (ReclamacionReservaIdJuridico) obj;
		if (claveSubCobertura == null) {
			if (other.claveSubCobertura != null)
				return false;
		} else if (!claveSubCobertura.equals(other.claveSubCobertura))
			return false;
		if (coberturaId == null) {
			if (other.coberturaId != null)
				return false;
		} else if (!coberturaId.equals(other.coberturaId))
			return false;
		if (oficio == null) {
			if (other.oficio != null)
				return false;
		} else if (!oficio.equals(other.oficio))
			return false;
		if (reclamacionJuridicoId == null) {
			if (other.reclamacionJuridicoId != null)
				return false;
		} else if (!reclamacionJuridicoId.equals(other.reclamacionJuridicoId))
			return false;
		return true;
	}

}
