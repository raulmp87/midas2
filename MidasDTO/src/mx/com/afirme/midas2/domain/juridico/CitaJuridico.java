package mx.com.afirme.midas2.domain.juridico;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.annotation.Exportable;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;

@Entity(name = "CitaJuridico")
@Table(name = "TOCITAJURIDICO", schema = "MIDAS")
public class CitaJuridico extends MidasAbstracto implements Entidad {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8744161819454457903L;	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOCITAJURIDICO_ID_GENERATOR")
	@SequenceGenerator(name="TOCITAJURIDICO_ID_GENERATOR", schema="MIDAS", sequenceName="TOCITAJURIDICO_SEQ",allocationSize=1)
	@Column(name="ID")
	private	Long id;
	
	@OneToOne
	@JoinColumn(name = "OFICINA_ID", nullable = false)
	private OficinaJuridico oficina;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_INICIO", nullable = false)
	private Date fechaInicio;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_FIN", nullable = false)
	private Date fechaFin;
	
	@Column(name = "USUARIO", nullable = false)
	private String nombreUsuario;
	
	@Column(name = "ASIGNADO", nullable = false)
	private String asignado;
	
	@Column(name = "TIPO_RECLAMACION", nullable = false)
	private String tipoReclamacion;
	
	@Column(name = "LUGAR_CITA", nullable = false)
	private String lugarCita;
	
	@Column(name = "RAMO", nullable = false)
	private String ramo;
	
	@Column(name = "ESTATUS", nullable = false)
	private String estatus;
	
	@Column(name = "MOTIVO")
	private String motivo;

	@Transient
	private String descripcionTipoReclamacion;
	@Transient
	private String descripcionRamo;
	@Transient
	private String descripcionEstatus;

	@Exportable(columnName="NUMERO DE CITA", columnOrder=1)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OficinaJuridico getOficina() {
		return oficina;
	}

	public void setOficina(OficinaJuridico oficina) {
		this.oficina = oficina;
	}

	@Exportable(columnName="FECHA DE LA CITA", columnOrder=5)
	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	@Exportable(columnName="NOMBRE DE USUARIO", columnOrder=2)
	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	@Exportable(columnName="ASIGNADO A", columnOrder=3)
	public String getAsignado() {
		return asignado;
	}

	public void setAsignado(String asignado) {
		this.asignado = asignado;
	}	

	public String getTipoReclamacion() {
		return tipoReclamacion;
	}

	public void setTipoReclamacion(String tipoReclamacion) {
		this.tipoReclamacion = tipoReclamacion;
	}

	@Exportable(columnName="LUGAR DE LA CITA", columnOrder=7)
	public String getLugarCita() {
		return lugarCita;
	}

	public void setLugarCita(String lugarCita) {
		this.lugarCita = lugarCita;
	}	

	public String getRamo() {
		return ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {		
		return this.id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {		
		return this.id;
	}

	@Exportable(columnName="TIPO DE RECLAMACION", columnOrder=6)
	public String getDescripcionTipoReclamacion() {
		return descripcionTipoReclamacion;
	}

	public void setDescripcionTipoReclamacion(String descripcionTipoReclamacion) {
		this.descripcionTipoReclamacion = descripcionTipoReclamacion;
	}

	@Exportable(columnName="RAMO", columnOrder=8)
	public String getDescripcionRamo() {
		return descripcionRamo;
	}

	public void setDescripcionRamo(String descripcionRamo) {
		this.descripcionRamo = descripcionRamo;
	}

	@Exportable(columnName="ESTATUS", columnOrder=9)
	public String getDescripcionEstatus() {
		return descripcionEstatus;
	}

	public void setDescripcionEstatus(String descripcionEstatus) {
		this.descripcionEstatus = descripcionEstatus;
	}
	
	@Transient
	@Exportable(columnName="OFICINA JURIDICA", columnOrder=0)
	public String getNombreOficina(){
		if(oficina != null){
			return oficina.getNombreOficina();
		}
		return "";
	}
	
	@Exportable(columnName="FECHA DE ALTA", columnOrder=4)
	public Date getFechaCreacion(){
		return this.fechaCreacion;
	}

}
