package mx.com.afirme.midas2.domain.juridico;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.juridico.OficinaJuridico;


/**
 * Entidad que persiste la información de los catálogos jurídico.
 * @author usuario
 * @version 1.0
 * @created 22-ago-2014 10:48:11 a.m.
 */
@Entity(name = "CatalogoJuridico")
@Table(name = "TCCATALOGOJURIDICO", schema = "MIDAS")
public class CatalogoJuridico extends MidasAbstracto  {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4514742342123301512L;
	/**
	 * Id de la orden de compra
	 */
	@Id
	@SequenceGenerator(name = "TCCATALOGOJURIDICO_SEQ_GENERADOR",allocationSize = 1, sequenceName = "TCCATALOGOJURIDICO_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TCCATALOGOJURIDICO_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id;
	
	
	@Column(name = "TIPO_CATALOGO")
	private String tipoCatalogo;
	
	
	@Column(name = "NUMERO")
	private Long numero;

	@Column(name = "NOMBRE")
	private String nombre;
	
    @Column(name="ESTATUS")
	private Boolean estatus = Boolean.TRUE;
    
    
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_ACTIVO")
	private Date fechaActivo;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_INACTIVO")
	private Date fechaInactivo;
	

	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_OFICINA_JURIDICO", referencedColumnName = "ID")
	private OficinaJuridico oficinaJuridico;
	
	

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getTipoCatalogo() {
		return tipoCatalogo;
	}



	public void setTipoCatalogo(String tipoCatalogo) {
		this.tipoCatalogo = tipoCatalogo;
	}



	public Long getNumero() {
		return numero;
	}



	public void setNumero(Long numero) {
		this.numero = numero;
	}



	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public Boolean getEstatus() {
		return estatus;
	}



	public void setEstatus(Boolean estatus) {
		this.estatus = estatus;
	}



	public Date getFechaActivo() {
		return fechaActivo;
	}



	public void setFechaActivo(Date fechaActivo) {
		this.fechaActivo = fechaActivo;
	}



	public Date getFechaInactivo() {
		return fechaInactivo;
	}



	public void setFechaInactivo(Date fechaInactivo) {
		this.fechaInactivo = fechaInactivo;
	}



	public OficinaJuridico getOficinaJuridico() {
		return oficinaJuridico;
	}



	public void setOficinaJuridico(OficinaJuridico oficinaJuridico) {
		this.oficinaJuridico = oficinaJuridico;
	}



	
	
	
	
	
	
	

}