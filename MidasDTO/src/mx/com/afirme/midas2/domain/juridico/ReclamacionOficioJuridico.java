package mx.com.afirme.midas2.domain.juridico;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.personadireccion.CiudadMidas;
import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;

@Entity
@Table(name="TORECLAMACIONOFICIOJURIDICO", schema="MIDAS")
public class ReclamacionOficioJuridico extends MidasAbstracto{

	private static final long serialVersionUID = 1666800177263050642L;

	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "oficio", column = @Column(name = "OFICIO", nullable = false)),
			@AttributeOverride(name = "reclamacionJuridicoId", column = @Column(name = "RECLAMACIONJURIDICO_ID", nullable = false)) })
	private ReclamacionOficioIdJuridico id;
	
	@Column(name="ACTOR")
	private String actor;
	
	@Column(name="ASEGURADO")
	private String asegurado;
	
	@Column(name="ASIGNADO")
	private String asignado;
	
	@Column(name="CLASIFICACION_RECLAMACION")
	private String clasificacionReclamacion;
	
	@Column(name="DEMANDADO")
	private String demandado;
	
	@Column(name="DESCRIPCION")
	private String descripcion;
	
	@Column(name="ESTATUS")
	private String estatus;
	
	@Column(name="ETAPA_JUICIO")
	private String etapaJuicio;
	
	@Column(name="EXPEDIENTE")
	private String expediente;
	
	@Column(name="MONTO_RECLAMADO")
	private BigDecimal montoReclamado;
	
	@Column(name="PARTE_JUICIO")
	private String parteEnJuicio;
	
	@Column(name="PROBABILIDAD_EXITO")
	private String probabilidadExito;
	
	@Column(name="RECLAMANTE")
	private String reclamante;
	
	@Column(name="TIPO_QUEJA")
	private String tipoQueja;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="DELEGACION_ID", referencedColumnName="ID")
	private CatalogoJuridico delegacion;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MOTIVORECLAMACION_ID", referencedColumnName="ID")
	private CatalogoJuridico motivoReclamacion;

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PROCEDIMIENTO_ID", referencedColumnName="ID")
	private CatalogoJuridico procedimiento;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ESTADO", referencedColumnName="STATE_ID")
	private EstadoMidas estado;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CIUDAD", referencedColumnName="CITY_ID")
	private CiudadMidas ciudad;
	
	@OneToMany(mappedBy="reclamacionOficio", fetch = FetchType.LAZY)
	private List<ReclamacionReservaJuridico> reservas;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="RECLAMACIONJURIDICO_ID", referencedColumnName="ID", insertable=false, updatable=false)
	private ReclamacionJuridico reclamacionJuridico;
	
	@SuppressWarnings("unchecked")
	@Override
	public ReclamacionOficioIdJuridico getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ReclamacionOficioIdJuridico getBusinessKey() {
		return id;
	}

	/**
	 * @return the id
	 */
	public ReclamacionOficioIdJuridico getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(ReclamacionOficioIdJuridico id) {
		this.id = id;
	}

	/**
	 * @return the asegurado
	 */
	public String getAsegurado() {
		return asegurado;
	}

	/**
	 * @param asegurado the asegurado to set
	 */
	public void setAsegurado(String asegurado) {
		this.asegurado = asegurado;
	}

	/**
	 * @return the asignado
	 */
	public String getAsignado() {
		return asignado;
	}

	/**
	 * @param asignado the asignado to set
	 */
	public void setAsignado(String asignado) {
		this.asignado = asignado;
	}

	/**
	 * @return the clasificacionReclamacion
	 */
	public String getClasificacionReclamacion() {
		return clasificacionReclamacion;
	}

	/**
	 * @param clasificacionReclamacion the clasificacionReclamacion to set
	 */
	public void setClasificacionReclamacion(String clasificacionReclamacion) {
		this.clasificacionReclamacion = clasificacionReclamacion;
	}

	/**
	 * @return the demandado
	 */
	public String getDemandado() {
		return demandado;
	}

	/**
	 * @param demandado the demandado to set
	 */
	public void setDemandado(String demandado) {
		this.demandado = demandado;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return the etapaJuicio
	 */
	public String getEtapaJuicio() {
		return etapaJuicio;
	}

	/**
	 * @param etapaJuicio the etapaJuicio to set
	 */
	public void setEtapaJuicio(String etapaJuicio) {
		this.etapaJuicio = etapaJuicio;
	}

	/**
	 * @return the expediente
	 */
	public String getExpediente() {
		return expediente;
	}

	/**
	 * @param expediente the expediente to set
	 */
	public void setExpediente(String expediente) {
		this.expediente = expediente;
	}

	/**
	 * @return the montoReclamado
	 */
	public BigDecimal getMontoReclamado() {
		return montoReclamado;
	}

	/**
	 * @param montoReclamado the montoReclamado to set
	 */
	public void setMontoReclamado(BigDecimal montoReclamado) {
		this.montoReclamado = montoReclamado;
	}

	/**
	 * @return the parteEnJuicio
	 */
	public String getParteEnJuicio() {
		return parteEnJuicio;
	}

	/**
	 * @param parteEnJuicio the parteEnJuicio to set
	 */
	public void setParteEnJuicio(String parteEnJuicio) {
		this.parteEnJuicio = parteEnJuicio;
	}

	/**
	 * @return the probabilidadExito
	 */
	public String getProbabilidadExito() {
		return probabilidadExito;
	}

	/**
	 * @param probabilidadExito the probabilidadExito to set
	 */
	public void setProbabilidadExito(String probabilidadExito) {
		this.probabilidadExito = probabilidadExito;
	}

	/**
	 * @return the reclamante
	 */
	public String getReclamante() {
		return reclamante;
	}

	/**
	 * @param reclamante the reclamante to set
	 */
	public void setReclamante(String reclamante) {
		this.reclamante = reclamante;
	}

	/**
	 * @return the tipoQueja
	 */
	public String getTipoQueja() {
		return tipoQueja;
	}

	/**
	 * @param tipoQueja the tipoQueja to set
	 */
	public void setTipoQueja(String tipoQueja) {
		this.tipoQueja = tipoQueja;
	}

	/**
	 * @return the estado
	 */
	public EstadoMidas getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(EstadoMidas estado) {
		this.estado = estado;
	}

	/**
	 * @return the ciudad
	 */
	public CiudadMidas getCiudad() {
		return ciudad;
	}

	/**
	 * @param ciudad the ciudad to set
	 */
	public void setCiudad(CiudadMidas ciudad) {
		this.ciudad = ciudad;
	}

	/**
	 * @return the reservas
	 */
	public List<ReclamacionReservaJuridico> getReservas() {
		return reservas;
	}

	/**
	 * @param reservas the reservas to set
	 */
	public void setReservas(List<ReclamacionReservaJuridico> reservas) {
		this.reservas = reservas;
	}

	/**
	 * @return the reclamacionJuridico
	 */
	public ReclamacionJuridico getReclamacionJuridico() {
		return reclamacionJuridico;
	}

	/**
	 * @param reclamacionJuridico the reclamacionJuridico to set
	 */
	public void setReclamacionJuridico(ReclamacionJuridico reclamacionJuridico) {
		this.reclamacionJuridico = reclamacionJuridico;
	}

	/**
	 * @return the delegacion
	 */
	public CatalogoJuridico getDelegacion() {
		return delegacion;
	}

	/**
	 * @param delegacion the delegacion to set
	 */
	public void setDelegacion(CatalogoJuridico delegacion) {
		this.delegacion = delegacion;
	}

	/**
	 * @return the motivoReclamacion
	 */
	public CatalogoJuridico getMotivoReclamacion() {
		return motivoReclamacion;
	}

	/**
	 * @param motivoReclamacion the motivoReclamacion to set
	 */
	public void setMotivoReclamacion(CatalogoJuridico motivoReclamacion) {
		this.motivoReclamacion = motivoReclamacion;
	}

	/**
	 * @return the procedimiento
	 */
	public CatalogoJuridico getProcedimiento() {
		return procedimiento;
	}

	/**
	 * @param procedimiento the procedimiento to set
	 */
	public void setProcedimiento(CatalogoJuridico procedimiento) {
		this.procedimiento = procedimiento;
	}

	/**
	 * @return the actor
	 */
	public String getActor() {
		return actor;
	}

	/**
	 * @param actor the actor to set
	 */
	public void setActor(String actor) {
		this.actor = actor;
	}

}
