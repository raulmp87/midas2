package mx.com.afirme.midas2.domain.vehiculo;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.annotation.Exportable;
import mx.com.afirme.midas2.domain.MidasAbstracto;

@Entity
@Table(name = "TOVALORCOMERCIALVEHICULO", schema = "MIDAS")
public class ValorComercialVehiculo extends MidasAbstracto implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3692760559338581353L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOVALORCOMERCIALVEHICULO_SEQ")
	@SequenceGenerator(name = "IDTOVALORCOMERCIALVEHICULO_SEQ", sequenceName = "IDTOVALORCOMERCIALVEHICULO_SEQ", schema = "MIDAS", allocationSize = 1)
	@Column(name=" ID ", nullable=false )
	private Long   id;
	
	@Column(name=" PROVEEDOR_ID ", nullable=false )
	private Long   proveedorId;
	
	@Column(name=" CLAVE_AMIS ", nullable=false )
	private String claveAmis;
	
	@Column(name=" DESCRIPCIONESTILO ", nullable=false )
	private String descripcionVehiculo;
	
	@Column(name=" MODELOVEHICULO ", nullable=false )
	private Integer   modeloVehiculo;
	
	@Column(name=" VALORCOMERCIAL ", nullable=false )
	private Double valorComercial;
	
	@Column(name=" VALORCOMERCIALNUEVO ", nullable=false )
	private Double valorComercialNuevo;
	
	@Column(name=" VALORCOMERCIALANIO ", nullable=false )
	private int    valorComercialAnio;
	
	@Column(name=" VALORCOMERCIALMES ", nullable=false )
	private int    valorComercialMes;
	
	@Column(name=" ESTATUSREGISTRO ", nullable=false )
	private short  estatusRegistro;
	
	@Column(name=" DOCUMENTO ", nullable=false )
	private Long  documento;
	
	@Transient
	private short tipoBusquedaFecha;
	
	@Transient
	private boolean error;
	
	@Transient
	Map<Integer,Double> datosModeloPrecio;
	
	@Transient
	private int filaExcel;
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}
	
	@Override
	public String getValue() {
		
		return "ValorComercialVehiculo [id=" + id + ", proveedorId="
		+ proveedorId + ", claveAmis=" + claveAmis
		+ ", descripcionVehiculo=" + descripcionVehiculo
		+ ", modeloVehiculo=" + modeloVehiculo + ", valorComercial="
		+ valorComercial + ", valorComercialNuevo="
		+ valorComercialNuevo + ", valorComercialAnio="
		+ valorComercialAnio + ", valorComerciaMes=" + valorComercialMes
		+ ", estatusRegistro=" + estatusRegistro + "]";
	}
	
	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public Long getId() {
		return id;
	}
	
	@Exportable(columnName = "Proveedor Id", columnOrder = 0)
	@Transient
	public Long getProveedorId() {
		return proveedorId;
	}
	
	@Exportable(columnName = "AMIS", columnOrder = 1)
	@Transient
	public String getClaveAmis() {
		return claveAmis;
	}
	
	@Exportable(columnName = "Descripción del Modelo", columnOrder = 2)
	@Transient
	public String getDescripcionVehiculo() {
		return descripcionVehiculo;
	}
	
	@Exportable(columnName = "Modelo", columnOrder = 3)
	@Transient
	public Integer getModeloVehiculo() {
		return modeloVehiculo;
	}
	
	@Exportable(columnName = "Valor Comercial", columnOrder = 4)
	@Transient
	public Double getValorComercial() {
		return valorComercial;
	}
	
	@Exportable(columnName = "Mes Valor Comercial", columnOrder = 5)
	@Transient
	public String getValorComercialNombreMes() {
		String nombreMes = "";
		switch( valorComercialMes ){
			case 1:
				nombreMes= "Enero";
				break;
			case 2:
				nombreMes= "Febrero";
				break;
			case 3:
				nombreMes= "Marzo";
				break;
			case 4:
				nombreMes= "Abril";
				break;
			case 5:
				nombreMes= "Mayo";
				break;
			case 6:
				nombreMes= "Junio";
				break;
			case 7:
				nombreMes= "Julio";
				break;
			case 8:
				nombreMes= "Agosto";
				break;
			case 9:
				nombreMes= "Septiembre";
				break;
			case 10:
				nombreMes= "Octubre";
				break;
			case 11:
				nombreMes= "Noviembre";
				break;
			case 12:
				nombreMes= "Diciembre";
				break;
		}
		return nombreMes;
	}
	
	@Exportable(columnName = "Año Valor Comercial", columnOrder = 6)
	@Transient
	public int getValorComercialAnio() {
		return valorComercialAnio;
	}
	
	@Exportable(columnName = "Fecha Creación", format="dd/MM/yyyy HH:mm:ss",  columnOrder = 7)
	@Transient
	public Date getFechaRegistro() {
		return this.fechaCreacion;
	}
	
	@Exportable(columnName = "Estatus",  columnOrder = 8)
	@Transient
	public String getNombreEstatus(){
		return ( ( this.estatusRegistro == 1 ) ? "Activo" : "Desactivo" ); 
	}
	
	public short getEstatusRegistro() {
		return estatusRegistro;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setProveedorId(Long proveedorId) {
		this.proveedorId = proveedorId;
	}
	public void setClaveAmis(String claveAmis) {
		this.claveAmis = claveAmis;
	}
	public void setDescripcionVehiculo(String descripcionVehiculo) {
		this.descripcionVehiculo = descripcionVehiculo;
	}
	public void setModeloVehiculo(Integer modeloVehiculo) {
		this.modeloVehiculo = modeloVehiculo;
	}
	public void setValorComercial(Double valorComercial) {
		this.valorComercial = valorComercial;
	}
	public void setValorComercialNuevo(Double valorComercialNuevo) {
		this.valorComercialNuevo = valorComercialNuevo;
	}
	public void setValorComercialAnio(int valorComercialAnio) {
		this.valorComercialAnio = valorComercialAnio;
	}
	public void setValorComercialMes(int valorComerciaMes) {
		this.valorComercialMes = valorComerciaMes;
	}
	public void setEstatusRegistro(short estatusRegistro) {
		this.estatusRegistro = estatusRegistro;
	}
	public short getTipoBusquedaFecha() {
		return tipoBusquedaFecha;
	}
	public int getValorComercialMes() {
		return valorComercialMes;
	}
	public void setTipoBusquedaFecha(short tipoBusquedaFecha) {
		this.tipoBusquedaFecha = tipoBusquedaFecha;
	}
	public Long getDocumento() {
		return documento;
	}
	public void setDocumento(Long documento) {
		this.documento = documento;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValorComercialVehiculo other = (ValorComercialVehiculo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public int getFilaExcel() {
		return filaExcel;
	}

	public void setFilaExcel(int filaExcel) {
		this.filaExcel = filaExcel;
	}

	public Map<Integer, Double> getDatosModeloPrecio() {
		return datosModeloPrecio;
	}

	public void setDatosModeloPrecio(Map<Integer, Double> datosModeloPrecio) {
		this.datosModeloPrecio = datosModeloPrecio;
	}

	public Double getValorComercialNuevo() {
		return valorComercialNuevo;
	}



}
