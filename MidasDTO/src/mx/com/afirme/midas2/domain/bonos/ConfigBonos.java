package mx.com.afirme.midas2.domain.bonos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import org.eclipse.persistence.annotations.Customizer;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.dto.bonos.ConfigBonosDTO;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

@Entity(name="ConfigBonos")
@Table(name="toConfigBonos",schema="MIDAS")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class ConfigBonos implements Serializable,Entidad{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8243683856607928280L;
	private Long	id;
	private ValorCatalogoAgentes tipoBono;
	private	String	descripcion;
	private Boolean pagoSinFacturaBoolean;
	private Integer	pagoSinFactura;
	private Date	fechaInicioVigencia;
	private	Date 	fechaFinVigencia;
	private List<ConfigBonoLineaVenta> lineaVenta;
	private List<ConfigBonoProducto> productos;
	private	List<ConfigBonoRamo> ramos;
	private	List<ConfigBonoSubramo> subramos;
	private List<ConfigBonoSeccion> secciones;
	private	List<ConfigBonoCobertura> coberturas;
	private	Long	idMoneda;
	private	Boolean	ajusteDeOficinaBoolean;
	private Integer	ajusteDeOficina;
	private ValorCatalogoAgentes periodoAjuste;
	private Long	idBonoAjustar;
	private ValorCatalogoAgentes produccionSobre;
	private	ValorCatalogoAgentes siniestralidadSobre;
	private BigDecimal	produccionMinima;
	private BigDecimal 	porcentajeSiniestralidadMaxima;
	private Boolean globaBoolean;
	private Integer	global;
	private Date 	periodoInicial;
	private Date	periodoFinal;
	private ValorCatalogoAgentes modoAplicacion;
	private Boolean	porcentajeImporteDirectoBoolean;
	private	BigDecimal	porcentajeImporteDirecto;
	private Double 	porcentajeAplicacionDirecto;
	private Double	importeAplicacionDirecto;
	private Boolean porcentajeImporteRangosBoolean;
	private Integer porcentajeImporteRangos;
	private ValorCatalogoAgentes periodoComparacion;
	private Boolean tipoBeneficiarioBoolean;
	private Long	tipoBeneficiario;
	private Boolean todasPromotoriasBoolean;
	private Integer	todasPromotorias;
	private Boolean todosAgentesBoolean;
	private Integer	todosAgentes;
	private Boolean activoBoolean;
	private Integer	activo;
	private List<ConfigBonoCentroOperacion> listaCentroOperaciones;
	private List<ConfigBonoGerencia> listaGerencias;
	private List<ConfigBonoEjecutivo> listaEjecutivos;
	private List<ConfigBonoPromotoria> listaPromotorias;
	private List<ConfigBonoTipoPromotoria> listaTiposPromotoria;
	private List<ConfigBonoPrioridad> listaPrioridades;
	private List<ConfigBonoSituacion> listaSituaciones;
	private List<ConfigBonoTipoAgente> listaTipoAgentes;
	private List<ConfigBonoAgente> listaAgentes;
	private List<ConfigBonoAplicaAgente> listaAplicaAgentes;
	private List<ConfigBonoAplicaPromotoria> listaAplicaPromotorias;
	private List<ConfigBonoPoliza> listaConfigPolizas;
	private Double conDescuentoMayorIgual;
	private Integer TodasPolizas;
	private Integer ContratadoConSobreComision;
	private Date fechaVencimientoCedula;
	private Long diasAntesVencimiento;
	private Integer tipoPersona;
	private Long poliza_id;
	private Integer tipoCedulaId;
	private	Date 	fechaCreacion;
	private Long conIncisosMayorIgual;
	private Long idNivelSiniestralidad;
	private ValorCatalogoAgentes idSubtipoBono;
	private Long idNivelCrecimiento;
	private Long idNivelProductividad;
	
	public ConfigBonos(){}
	
	public ConfigBonos(ConfigBonosDTO dto){
		this.activo=dto.getActivo();
		this.activoBoolean=dto.getActivoBoolean();
		this.ajusteDeOficina=dto.getAjusteDeOficina();
		this.ajusteDeOficinaBoolean=dto.getAjusteDeOficinaBoolean();
		this.descripcion=dto.getDescripcion();
		this.fechaFinVigencia=dto.getFechaFinVigencia();
		this.fechaInicioVigencia=dto.getFechaInicioVigencia();
		this.globaBoolean=dto.getGlobaBoolean();
		this.global=dto.getGlobal();
		this.id=dto.getId();
		this.idBonoAjustar=dto.getIdBonoAjustar();
		this.idMoneda=dto.getIdMoneda();
		this.importeAplicacionDirecto=dto.getImporteAplicacionDirecto();
		this.modoAplicacion=dto.getModoAplicacion();
		this.pagoSinFactura=dto.getPagoSinFactura();
		this.pagoSinFacturaBoolean=dto.getPagoSinFacturaBoolean();
		this.periodoAjuste=dto.getPeriodoAjuste();
		this.periodoComparacion=dto.getPeriodoComparacion();
		this.periodoFinal=dto.getPeriodoFinal();
		this.periodoInicial=dto.getPeriodoInicial();
		this.porcentajeAplicacionDirecto=dto.getPorcentajeAplicacionDirecto();
		this.porcentajeImporteDirecto=dto.getPorcentajeImporteDirecto();
		this.porcentajeImporteDirectoBoolean=dto.getPorcentajeImporteDirectoBoolean();
		this.porcentajeImporteRangos=dto.getPorcentajeImporteRangos();
		this.porcentajeImporteRangosBoolean=dto.getPorcentajeImporteRangosBoolean();
		this.porcentajeSiniestralidadMaxima=dto.getPorcentajeSiniestralidadMaxima();
		this.produccionMinima=dto.getProduccionMinima();
		this.produccionSobre=dto.getProduccionSobre();
		this.siniestralidadSobre=dto.getSiniestralidadSobre();
		this.tipoBeneficiario=dto.getTipoBeneficiario();
		this.tipoBeneficiarioBoolean=dto.getTipoBeneficiarioBoolean();
		this.tipoBono=dto.getTipoBono();
		this.todasPromotorias=dto.getTodasPromotorias();
		this.todasPromotoriasBoolean=dto.getTodasPromotoriasBoolean();
		this.todosAgentes=dto.getTodosAgentes();
		this.todosAgentesBoolean=dto.getTodosAgentesBoolean();
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idToConfigBonos_seq")
	@SequenceGenerator(name="idToConfigBonos_seq", sequenceName="MIDAS.idToConfigBonos_seq",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDTIPOBONO")
	public ValorCatalogoAgentes getTipoBono() {
		return tipoBono;
	}

	public void setTipoBono(ValorCatalogoAgentes tipoBono) {
		this.tipoBono = tipoBono;
	}

	@Column(name="DESCRIPCIONBONO")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	@Transient
	public Boolean getPagoSinFacturaBoolean() {
		return pagoSinFacturaBoolean;
	}

	public void setPagoSinFacturaBoolean(Boolean pagoSinFacturaBoolean) {
		this.pagoSinFacturaBoolean = pagoSinFacturaBoolean;
		this.pagoSinFactura=(pagoSinFacturaBoolean)?1:0;
	}

	@Column(name="CLAVEPAGOSINFACTURA")
	@Digits(integer=1,fraction=0)
	public Integer getPagoSinFactura() {
		return pagoSinFactura;
	}

	public void setPagoSinFactura(Integer pagoSinFactura) {
		this.pagoSinFactura = pagoSinFactura;
	}
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAINICIOVIGENCIA")
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHAFINVIGENCIA")
	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}

	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}
	
	
	@Transient
	public List<ConfigBonoLineaVenta> getLineaVenta() {
		return lineaVenta;
	}

	public void setLineaVenta(List<ConfigBonoLineaVenta> lineaVenta) {
		this.lineaVenta = lineaVenta;
	}

	@Transient
	public List<ConfigBonoProducto> getProductos() {
		return productos;
	}

	public void setProductos(List<ConfigBonoProducto> productos) {
		this.productos = productos;
	}

	@Transient
	public List<ConfigBonoRamo> getRamos() {
		return ramos;
	}

	public void setRamos(List<ConfigBonoRamo> ramos) {
		this.ramos = ramos;
	}
	
	@Transient
	public List<ConfigBonoSubramo> getSubramos() {
		return subramos;
	}

	public void setSubramos(List<ConfigBonoSubramo> subramos) {
		this.subramos = subramos;
	}
	
	@Transient
	public List<ConfigBonoSeccion> getSecciones() {
		return secciones;
	}

	public void setSecciones(List<ConfigBonoSeccion> secciones) {
		this.secciones = secciones;
	}
	
	@Transient
	public List<ConfigBonoCobertura> getCoberturas() {
		return coberturas;
	}

	public void setCoberturas(List<ConfigBonoCobertura> coberturas) {
		this.coberturas = coberturas;
	}

	@Column(name="IDTOMONEDA")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}
	@Transient
	public Boolean getAjusteDeOficinaBoolean() {
		return ajusteDeOficinaBoolean;
	}

	public void setAjusteDeOficinaBoolean(Boolean ajusteDeOficinaBoolean) {
		this.ajusteDeOficinaBoolean = ajusteDeOficinaBoolean;
		this.ajusteDeOficina=(ajusteDeOficinaBoolean)?1:0;
	}

	@Column(name="CLAVEAJUSTEDEOFICINA")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Integer getAjusteDeOficina() {
		return ajusteDeOficina;
	}

	public void setAjusteDeOficina(Integer ajusteDeOficina) {
		this.ajusteDeOficina = ajusteDeOficina;
	}
	
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDPERIODODEAJUSTE")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ValorCatalogoAgentes getPeriodoAjuste() {
		return periodoAjuste;
	}

	public void setPeriodoAjuste(ValorCatalogoAgentes periodoAjuste) {
		this.periodoAjuste = periodoAjuste;
	}

	@Column(name="IDBONOAJUSTAR")
	public Long getIdBonoAjustar() {
		return idBonoAjustar;
	}

	public void setIdBonoAjustar(Long idBonoAjustar) {
		this.idBonoAjustar = idBonoAjustar;
	}
	
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDPRODUCCIONSOBRE")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ValorCatalogoAgentes getProduccionSobre() {
		return produccionSobre;
	}

	public void setProduccionSobre(ValorCatalogoAgentes produccionSobre) {
		this.produccionSobre = produccionSobre;
	}
	
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDSINIESTRALIDADSOBRE")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ValorCatalogoAgentes getSiniestralidadSobre() {
		return siniestralidadSobre;
	}

	public void setSiniestralidadSobre(ValorCatalogoAgentes siniestralidadSobre) {
		this.siniestralidadSobre = siniestralidadSobre;
	}

	@Column(name="PRODUCCIONMINIMA")
	@Digits(integer=14,fraction=2)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public BigDecimal getProduccionMinima() {
		return produccionMinima;
	}

	public void setProduccionMinima(BigDecimal produccionMinima) {
		this.produccionMinima = produccionMinima;
	}

	@Column(name="PCTSINIESTRALIDADMAXIMA")
	@Digits(integer=14,fraction=2)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public BigDecimal getPorcentajeSiniestralidadMaxima() {
		return porcentajeSiniestralidadMaxima;
	}

	public void setPorcentajeSiniestralidadMaxima(
			BigDecimal porcentajeSiniestralidadMaxima) {
		this.porcentajeSiniestralidadMaxima = porcentajeSiniestralidadMaxima;
	}
	@Transient
	public Boolean getGlobaBoolean() {
		return globaBoolean;
	}

	public void setGlobaBoolean(Boolean globaBoolean) {
		this.globaBoolean = globaBoolean;
		this.global=(globaBoolean)?1:0;
	}
	
	@Column(name="CLAVEGLOBAL")
	public Integer getGlobal() {
		return global;
	}

	public void setGlobal(Integer global) {
		this.global = global;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name="PERIODOINICIAL")
	public Date getPeriodoInicial() {
		return periodoInicial;
	}

	public void setPeriodoInicial(Date periodoInicial) {
		this.periodoInicial = periodoInicial;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="PERIODOFINAL")
	public Date getPeriodoFinal() {
		return periodoFinal;
	}

	public void setPeriodoFinal(Date periodoFinal) {
		this.periodoFinal = periodoFinal;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="CLAVEMODOAPLICACION")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ValorCatalogoAgentes getModoAplicacion() {
		return modoAplicacion;
	}

	public void setModoAplicacion(ValorCatalogoAgentes modoAplicacion) {
		this.modoAplicacion = modoAplicacion;
	}

	@Column(name="PCTAPLICACIONDIRECTO")
	public Double getPorcentajeAplicacionDirecto() {
		return porcentajeAplicacionDirecto;
	}

	public void setPorcentajeAplicacionDirecto(Double porcentajeAplicacionDirecto) {
		this.porcentajeAplicacionDirecto = porcentajeAplicacionDirecto;
	}

	@Column(name="IMPORTEAPLICACIONDIRECTO")
	public Double getImporteAplicacionDirecto() {
		return importeAplicacionDirecto;
	}

	public void setImporteAplicacionDirecto(Double importeAplicacionDirecto) {
		this.importeAplicacionDirecto = importeAplicacionDirecto;
	}

	@Transient
	public Boolean getPorcentajeImporteDirectoBoolean() {
		return porcentajeImporteDirectoBoolean;
	}

	public void setPorcentajeImporteDirectoBoolean(Boolean porcentajeImporteDirectoBoolean) {
		this.porcentajeImporteDirectoBoolean = porcentajeImporteDirectoBoolean;
	}

	@Column(name="clavePctImporteDirecto")
	@Digits(integer=14,fraction=2)
	public BigDecimal getPorcentajeImporteDirecto() {
		return porcentajeImporteDirecto;
	}

	public void setPorcentajeImporteDirecto(BigDecimal porcentajeImporteDirecto) {
		this.porcentajeImporteDirecto = porcentajeImporteDirecto;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDPERIODOCOMPARACION")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ValorCatalogoAgentes getPeriodoComparacion() {
		return periodoComparacion;
	}

	public void setPeriodoComparacion(ValorCatalogoAgentes periodoComparacion) {
		this.periodoComparacion = periodoComparacion;
	}

	@Transient
	public Boolean getTipoBeneficiarioBoolean() {
		return tipoBeneficiarioBoolean;
	}

	public void setTipoBeneficiarioBoolean(Boolean tipoBeneficiarioBoolean) {
		this.tipoBeneficiarioBoolean = tipoBeneficiarioBoolean;
	}

	@Column(name="CLAVETIPOBENEFICIARIO")
	public Long getTipoBeneficiario() {
		return tipoBeneficiario;
	}

	public void setTipoBeneficiario(Long tipoBeneficiario) {
		this.tipoBeneficiario = tipoBeneficiario;
	}
	@Transient
	public Boolean getTodasPromotoriasBoolean() {
		return todasPromotoriasBoolean;
	}

	public void setTodasPromotoriasBoolean(Boolean todasPromotoriasBoolean) {
		this.todasPromotoriasBoolean = todasPromotoriasBoolean;
		this.todasPromotorias=(todasPromotoriasBoolean)?1:0;
	}

	@Column(name="CLAVETODASPROMOTORIAS")
	public Integer getTodasPromotorias() {
		return todasPromotorias;
	}

	public void setTodasPromotorias(Integer todasPromotorias) {
		this.todasPromotorias = todasPromotorias;
	}
	
	@Transient
	public Boolean getTodosAgentesBoolean() {
		return todosAgentesBoolean;
	}

	public void setTodosAgentesBoolean(Boolean todosAgentesBoolean) {
		this.todosAgentesBoolean = todosAgentesBoolean;
		this.todosAgentes=(todosAgentesBoolean)?1:0;
	}

	@Column(name="CLAVETODOSAGENTES")
	public Integer getTodosAgentes() {
		return todosAgentes;
	}

	public void setTodosAgentes(Integer todosAgentes) {
		this.todosAgentes = todosAgentes;
	}

	@Transient
	public Boolean getActivoBoolean() {
		return activoBoolean;
	}

	public void setActivoBoolean(Boolean activoBoolean) {
		this.activoBoolean = activoBoolean;
		this.activo=(activoBoolean)?1:0;
	}
	@Column(name="CLAVEACTIVO")
	public Integer getActivo() {
		return activo;
	}

	public void setActivo(Integer activo) {
		this.activo = activo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	
	@Transient
	public Boolean getPorcentajeImporteRangosBoolean() {
		return porcentajeImporteRangosBoolean;
	}

	public void setPorcentajeImporteRangosBoolean(
			Boolean porcentajeImporteRangosBoolean) {
		this.porcentajeImporteRangosBoolean = porcentajeImporteRangosBoolean;
	}
	@Column(name="clavePctImporteRangos")
	@Digits(integer=12,fraction=2)
	public Integer getPorcentajeImporteRangos() {
		return porcentajeImporteRangos;
	}

	public void setPorcentajeImporteRangos(Integer porcentajeImporteRangos) {
		this.porcentajeImporteRangos = porcentajeImporteRangos;
	}
//	@OneToMany(mappedBy="configuracionBonos",fetch=FetchType.LAZY,targetEntity=ConfigBonoCentroOperacion.class)
	@Transient
	public List<ConfigBonoCentroOperacion> getListaCentroOperaciones() {
		return listaCentroOperaciones;
	}

	public void setListaCentroOperaciones(List<ConfigBonoCentroOperacion> listaCentroOperaciones) {
		this.listaCentroOperaciones = listaCentroOperaciones;
	}
//	@OneToMany(mappedBy="configuracionBonos",fetch=FetchType.LAZY,targetEntity=ConfigBonoGerencia.class)
	@Transient
	public List<ConfigBonoGerencia> getListaGerencias() {
		return listaGerencias;
	}

	public void setListaGerencias(List<ConfigBonoGerencia> listaGerencias) {
		this.listaGerencias = listaGerencias;
	}
//	@OneToMany(mappedBy="configuracionBonos",fetch=FetchType.LAZY,targetEntity=ConfigBonoEjecutivo.class)
	@Transient
	public List<ConfigBonoEjecutivo> getListaEjecutivos() {
		return listaEjecutivos;
	}

	public void setListaEjecutivos(List<ConfigBonoEjecutivo> listaEjecutivos) {
		this.listaEjecutivos = listaEjecutivos;
	}
//	@OneToMany(mappedBy="configuracionBonos",fetch=FetchType.LAZY,targetEntity=ConfigBonoPromotoria.class)
	@Transient
	public List<ConfigBonoPromotoria> getListaPromotorias() {
		return listaPromotorias;
	}

	public void setListaPromotorias(List<ConfigBonoPromotoria> listaPromotorias) {
		this.listaPromotorias = listaPromotorias;
	}
//	@OneToMany(mappedBy="configuracionBonos",fetch=FetchType.LAZY,targetEntity=ConfigBonoTipoPromotoria.class)
	@Transient
	public List<ConfigBonoTipoPromotoria> getListaTiposPromotoria() {
		return listaTiposPromotoria;
	}

	public void setListaTiposPromotoria(
			List<ConfigBonoTipoPromotoria> listaTiposPromotoria) {
		this.listaTiposPromotoria = listaTiposPromotoria;
	}
//	@OneToMany(mappedBy="configuracionBonos",fetch=FetchType.LAZY,targetEntity=ConfigBonoPrioridad.class)
	@Transient
	public List<ConfigBonoPrioridad> getListaPrioridades() {
		return listaPrioridades;
	}

	public void setListaPrioridades(List<ConfigBonoPrioridad> listaPrioridades) {
		this.listaPrioridades = listaPrioridades;
	}
//	@OneToMany(mappedBy="configuracionBonos",fetch=FetchType.LAZY,targetEntity=ConfigBonoSituacion.class)
	@Transient
	public List<ConfigBonoSituacion> getListaSituaciones() {
		return listaSituaciones;
	}

	public void setListaSituaciones(List<ConfigBonoSituacion> listaSituaciones) {
		this.listaSituaciones = listaSituaciones;
	}
//	@OneToMany(mappedBy="configuracionBonos",fetch=FetchType.LAZY,targetEntity=ConfigBonoTipoAgente.class)
	@Transient
	public List<ConfigBonoTipoAgente> getListaTipoAgentes() {
		return listaTipoAgentes;
	}

	public void setListaTipoAgentes(List<ConfigBonoTipoAgente> listaTipoAgentes) {
		this.listaTipoAgentes = listaTipoAgentes;
	}
//	@OneToMany(mappedBy="configuracionBonos",fetch=FetchType.LAZY,targetEntity=ConfigBonoAgente.class)
	@Transient
	public List<ConfigBonoAgente> getListaAgentes() {
		return listaAgentes;
	}

	public void setListaAgentes(List<ConfigBonoAgente> listaAgentes) {
		this.listaAgentes = listaAgentes;
	}
//	@OneToMany(mappedBy="configuracionBonos",fetch=FetchType.LAZY,targetEntity=ConfigBonoAplicaAgente.class)
	@Transient
	public List<ConfigBonoAplicaAgente> getListaAplicaAgentes() {
		return listaAplicaAgentes;
	}

	public void setListaAplicaAgentes(
			List<ConfigBonoAplicaAgente> listaAplicaAgentes) {
		this.listaAplicaAgentes = listaAplicaAgentes;
	}
//	@OneToMany(mappedBy="configuracionBonos",fetch=FetchType.LAZY,targetEntity=ConfigBonoAplicaPromotoria.class)
	@Transient
	public List<ConfigBonoAplicaPromotoria> getListaAplicaPromotorias() {
		return listaAplicaPromotorias;
	}

	public void setListaAplicaPromotorias(
			List<ConfigBonoAplicaPromotoria> listaAplicaPromotorias) {
		this.listaAplicaPromotorias = listaAplicaPromotorias;
	}

	@Transient
	public List<ConfigBonoPoliza> getListaConfigPolizas() {
		return listaConfigPolizas;
	}

	public void setListaConfigPolizas(List<ConfigBonoPoliza> listaConfigPolizas) {
		this.listaConfigPolizas = listaConfigPolizas;
	}
	
	@Column(name="CON_DESC_MAY_IGUAL")
	@Digits(integer=12,fraction=2)
	public Double getConDescuentoMayorIgual() {
		return conDescuentoMayorIgual;
	}

	public void setConDescuentoMayorIgual(Double conDescuentoMayorIgual) {
		this.conDescuentoMayorIgual = conDescuentoMayorIgual;
	}

	@Column(name="TODASPOLIZAS")
	public Integer getTodasPolizas() {
		return TodasPolizas;
	}

	public void setTodasPolizas(Integer todasPolizas) {
		TodasPolizas = todasPolizas;
	}

	@Column(name="CONTRATADOCONSOBRECOMISION")
	public Integer getContratadoConSobreComision() {
		return ContratadoConSobreComision;
	}

	public void setContratadoConSobreComision(Integer contratadoConSobreComision) {
		ContratadoConSobreComision = contratadoConSobreComision;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHAVENCIMIENTOCEDULA")
	public Date getFechaVencimientoCedula() {
		return fechaVencimientoCedula;
	}

	public void setFechaVencimientoCedula(Date fechaVencimientoCedula) {
		this.fechaVencimientoCedula = fechaVencimientoCedula;
	}

	@Column(name="DIASANTESVENCIMIENTO")
	public Long getDiasAntesVencimiento() {
		return diasAntesVencimiento;
	}

	public void setDiasAntesVencimiento(Long diasAntesVencimiento) {
		this.diasAntesVencimiento = diasAntesVencimiento;
	}

	@Column(name="IDTIPOPERSONA")
	public Integer getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(Integer tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	@Transient
	public Long getPoliza_id() {
		return poliza_id;
	}

	public void setPoliza_id(Long poliza_id) {
		this.poliza_id = poliza_id;
	}
	@Transient
	public Integer getTipoCedulaId() {
		return tipoCedulaId;
	}

	public void setTipoCedulaId(Integer tipoCedulaId) {
		this.tipoCedulaId = tipoCedulaId;
	}

	@Temporal(TemporalType.DATE)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Column(name="CON_INCISOS_MAY_IGUAL")
	public Long getConIncisosMayorIgual() {
		return conIncisosMayorIgual;
	}

	public void setConIncisosMayorIgual(Long conIncisosMayorIgual) {
		this.conIncisosMayorIgual = conIncisosMayorIgual;
	}
	
	@Column(name="IDNIVELSINIESTRALIDAD")
	public Long getIdNivelSiniestralidad() {
		return idNivelSiniestralidad;
	}

	public void setIdNivelSiniestralidad(Long idNivelSiniestralidad) {
		this.idNivelSiniestralidad = idNivelSiniestralidad;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="idSubtipoBono")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ValorCatalogoAgentes getIdSubtipoBono() {
		return idSubtipoBono;
	}

	public void setIdSubtipoBono(ValorCatalogoAgentes idSubtipoBono) {
		this.idSubtipoBono = idSubtipoBono;
	}
		
	@Column(name="IDNIVELCRECIMIENTO")
	public Long getIdNivelCrecimiento() {
		return idNivelCrecimiento;
	}

	public void setIdNivelCrecimiento(Long idNivelCrecimiento) {
		this.idNivelCrecimiento = idNivelCrecimiento;
	}

	@Column(name="IDNIVELPRODUCTIVIDAD")
	public Long getIdNivelProductividad() {
		return idNivelProductividad;
	}

	public void setIdNivelProductividad(Long idNivelProductividad) {
		this.idNivelProductividad = idNivelProductividad;
	}
	
	
}
