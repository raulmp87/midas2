package mx.com.afirme.midas2.domain.bonos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.annotation.HistoryTable;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

import org.eclipse.persistence.annotations.Customizer;
/**
 * 
 * @author vmhersil
 *
 */
@Entity(name="ConfigBonoCentroOperacion")
@Table(name="TRCONFIGBONOCENTROOPERACION",schema="MIDAS")
@HistoryTable(name="TRCONFIGBONOCENTROOPERA_HIST",schema="MIDAS")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class ConfigBonoCentroOperacion implements Serializable,Entidad{
	/**
	 * 
	 */
	private static final long serialVersionUID = 773885092793034497L;
	private Long	id;
	private	ConfigBonos configuracionBonos;
	private CentroOperacion centroOperacion;
	
	public ConfigBonoCentroOperacion(){}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idTrCfgBonoCentroOper_seq")
	@SequenceGenerator(name="idTrCfgBonoCentroOper_seq", sequenceName="MIDAS.idTrCfgBonoCentroOper_seq",allocationSize=1)
	@Column(name="ID")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ConfigBonos.class)
	@JoinColumn(name="CONFIGBONO_ID")
	public ConfigBonos getConfiguracionBonos() {
		return configuracionBonos;
	}

	public void setConfiguracionBonos(ConfigBonos configuracionBonos) {
		this.configuracionBonos = configuracionBonos;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=CentroOperacion.class)
	@JoinColumn(name="CENTROOPERACION_ID")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public CentroOperacion getCentroOperacion() {
		return centroOperacion;
	}

	public void setCentroOperacion(CentroOperacion centroOperacion) {
		this.centroOperacion = centroOperacion;
	}
}
