package mx.com.afirme.midas2.domain.bonos;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

import org.eclipse.persistence.annotations.Customizer;


@Entity(name="ConfigBonoPoliza")
@Table(name="TRCONFIGBONOPOLIZA",schema="MIDAS")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class ConfigBonoPoliza implements Entidad, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2930630543421890386L;
	private Long id;
	private ConfigBonos configbonos;
	private BigDecimal idCotizacion;
	private BigDecimal idPoliza;
	private String polizamidaFormateada;

	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="IDTRCFGBONOPOLIZA_SEQ")
	@SequenceGenerator(name="IDTRCFGBONOPOLIZA_SEQ",sequenceName="MIDAS.IDTRCFGBONOPOLIZA_SEQ",allocationSize=1)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

//	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ConfigBonos.class)
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ConfigBonos.class)
	@JoinColumn(name="CONFIGBONO_ID")	
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ConfigBonos getConfigbonos() {
		return configbonos;
	}
	
	public void setConfigbonos(ConfigBonos configbonos) {
		this.configbonos = configbonos;
	}

//	@ManyToOne(fetch=FetchType.LAZY,targetEntity=PolizaDTO.class)
//	@ManyToOne(fetch=FetchType.EAGER,targetEntity=PolizaDTO.class)
	@Column(name="POLIZA_ID")	
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public BigDecimal getIdCotizacion() {
		return idCotizacion;
	}

	public void setIdCotizacion(BigDecimal idCotizacion) {
		this.idCotizacion = idCotizacion;
	}

	
	
	@Transient
	public BigDecimal getIdPoliza() {
		return idPoliza;
	}

	public void setIdPoliza(BigDecimal idPoliza) {
		this.idPoliza = idPoliza;
	}
	
	@Transient
	public String getPolizamidaFormateada() {
		return polizamidaFormateada;
	}

	public void setPolizamidaFormateada(String polizamidaFormateada) {
		this.polizamidaFormateada = polizamidaFormateada;
	}

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}
