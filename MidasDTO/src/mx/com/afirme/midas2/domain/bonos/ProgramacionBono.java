package mx.com.afirme.midas2.domain.bonos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;

@Entity(name="ProgramacionBono")
@Table(name="toProgramacionBono",schema="MIDAS")
@SqlResultSetMapping(name="programacionBonosView",entities={
	@EntityResult(entityClass=ProgramacionBono.class,fields={
		@FieldResult(name="id",column="idProgramacion"),
		@FieldResult(name="idBono",column="idConfiguracion"),
		@FieldResult(name="fechaEjecucion",column="fecha")
	})
})
public class ProgramacionBono implements Entidad,Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -666085169519696296L;
	private Long id;
	private ValorCatalogoAgentes tipoBono;
	private Date fechaAlta;
	private Date fechaEjecucion;
	private ValorCatalogoAgentes periodoEjecucion; //
	private String hora;//
	private ValorCatalogoAgentes claveEstatus;
	private String usuarioAlta;
	private Integer activo;
	private ValorCatalogoAgentes tipoConfiguracion;
	private Long idBono;
	
	private String descripcionBono;
	private String descripcionEstatus;
	
	public static final Long TIPO_CONFIG_NEGOCIO_ESPECIAL = new Long(0);
	public static final Long TIPO_CONFIG_CONFIG_BONO = new Long(0);

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idToProgramacionBono_seq")
	@SequenceGenerator(name="idToProgramacionBono_seq", sequenceName="MIDAS.idToProgramacionBono_seq",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="TIPOBONO")
	public ValorCatalogoAgentes getTipoBono() {
		return tipoBono;
	}

	public void setTipoBono(ValorCatalogoAgentes tipoBono) {
		this.tipoBono = tipoBono;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHAALTA")
	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHAEJECUCION")
	public Date getFechaEjecucion() {
		return fechaEjecucion;
	}

	public void setFechaEjecucion(Date fechaEjecucion) {
		this.fechaEjecucion = fechaEjecucion;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="PERIODOEJECUCION")
	public ValorCatalogoAgentes getPeriodoEjecucion() {
		return periodoEjecucion;
	}

	public void setPeriodoEjecucion(ValorCatalogoAgentes periodoEjecucion) {
		this.periodoEjecucion = periodoEjecucion;
	}

	@Column(name="HORA")
	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="CLAVEESTATUS")
	public ValorCatalogoAgentes getClaveEstatus() {
		return claveEstatus;
	}

	public void setClaveEstatus(ValorCatalogoAgentes claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	@Column(name="USUARIOALTA")
	public String getUsuarioAlta() {
		return usuarioAlta;
	}

	public void setUsuarioAlta(String codigoUsuarioAlta) {
		this.usuarioAlta = codigoUsuarioAlta;
	}

	@Column(name="ACTIVO")
	public Integer getActivo() {
		return activo;
	}

	public void setActivo(Integer activo) {
		this.activo = activo;
	}

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="TIPOCONFIGURACION")
	public ValorCatalogoAgentes getTipoConfiguracion() {
		return tipoConfiguracion;
	}

	public void setTipoConfiguracion(ValorCatalogoAgentes tipoConfiguracion) {
		this.tipoConfiguracion = tipoConfiguracion;
	}

	@Column(name="IDBONO")
	public Long getIdBono() {
		return idBono;
	}

	public void setIdBono(Long idBono) {
		this.idBono = idBono;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void setDescripcionBono(String descripcionBono) {
		this.descripcionBono = descripcionBono;
	}

	@Transient
	public String getDescripcionBono() {
		return descripcionBono;
	}

	public void setDescripcionEstatus(String descripcionEstatus) {
		this.descripcionEstatus = descripcionEstatus;
	}

	@Transient
	public String getDescripcionEstatus() {
		return descripcionEstatus;
	}


}
