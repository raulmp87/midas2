package mx.com.afirme.midas2.domain.bonos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

import org.eclipse.persistence.annotations.Customizer;

@Entity(name="ConfigBonoLineaVenta")
@Table(name="TRCONFIGBONOLINEAVENTA",schema="MIDAS")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class ConfigBonoLineaVenta implements Entidad,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private ConfigBonos configBono;
	private Long idLineaVenta;

	
	
	
	@Id
	@GeneratedValue(generator="IDTRCFGBONOLINVENTA_SEQ",strategy=GenerationType.SEQUENCE)
	@SequenceGenerator(name="IDTRCFGBONOLINVENTA_SEQ",sequenceName="MIDAS.IDTRCFGBONOLINVENTA_SEQ",allocationSize=1)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ConfigBonos.class)
	@JoinColumn(name="CONFIGBONO_ID")
	public ConfigBonos getConfigBono() {
		return configBono;
	}

	public void setConfigBono(ConfigBonos configBono) {
		this.configBono = configBono;
	}

	@Column(name="ID_LINEAVENTA")
	public Long getIdLineaVenta() {
		return idLineaVenta;
	}

	public void setIdLineaVenta(Long idLineaVenta) {
		this.idLineaVenta = idLineaVenta;
	}

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}
