/**
 * 
 */
package mx.com.afirme.midas2.domain.bonos;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.eclipse.persistence.annotations.Customizer;

import mx.com.afirme.midas2.annotation.HistoryTable;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.util.StaticCommonVariables;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

/**
 * 
 * @author admin
 *
 */
@Entity(name="BonoExcepcionPoliza")
@Table(schema=StaticCommonVariables.DEFAULT_SCHEMA, name="TRCONFIGBONOEXCEPCIONPOLIZA")
@HistoryTable(name="TRCONFIGBONOEXCEPCIONPOL_HIST",schema="MIDAS")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class BonoExcepcionPoliza implements Entidad, Serializable {

	/**
	 * 
	 */
	private Long idExcepcionPoliza;
	private ConfigBonos idConfig;
	private BigDecimal idCotizacion;
	private BigDecimal idPoliza;
	private Double valorMontoPcteAgente;
	private Double valorMontoPctePromotoria;
	private Integer noAplicaDescuentoExcepcion;
	private Double descuentoPoliza;
	private Integer numeroPoliza;
	private String polizaFormateada;

	private static final long serialVersionUID = 6760874407562883906L;

	/**
	 * 
	 */
	public BonoExcepcionPoliza() {
	}

	/* (non-Javadoc)
	 * @see mx.com.afirme.midas2.dao.catalogos.Entidad#getKey()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return idExcepcionPoliza;
	}

	/* (non-Javadoc)
	 * @see mx.com.afirme.midas2.dao.catalogos.Entidad#getValue()
	 */
	@Override
	public String getValue() {
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.afirme.midas2.dao.catalogos.Entidad#getBusinessKey()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return idExcepcionPoliza;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTRCONFIGBONOEXCPOLIZA_SEQ")
	@SequenceGenerator(name="IDTRCONFIGBONOEXCPOLIZA_SEQ", sequenceName="MIDAS.IDTRCONFIGBONOEXCPOLIZA_SEQ", allocationSize=1)
	@Column(name="ID")
	public Long getIdExcepcionPoliza() {
		return idExcepcionPoliza;
	}

	public void setIdExcepcionPoliza(Long idExcepcionPoliza) {
		this.idExcepcionPoliza = idExcepcionPoliza;
	}

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=ConfigBonos.class)
	@JoinColumn(name="IDCONFIG")
	@NotNull(groups =EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ConfigBonos getIdConfig() {
		return idConfig;
	}

	public void setIdConfig(ConfigBonos idConfig) {
		this.idConfig = idConfig;
	}

	@Column(name="IDPOLIZA")
	@NotNull(groups =EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public BigDecimal getIdCotizacion() {
		return idCotizacion;
	}

	public void setIdCotizacion(BigDecimal idCotizacion) {
		this.idCotizacion = idCotizacion;
	}

	@Column(name="VALORMONTOPCTEAGENTE")
	public Double getValorMontoPcteAgente() {
		return valorMontoPcteAgente;
	}

	public void setValorMontoPcteAgente(Double valorMontoPcteAgente) {
		this.valorMontoPcteAgente = valorMontoPcteAgente;
	}

	@Column(name="VALORMONTOPCTEPROMOTORIA")
	public Double getValorMontoPctePromotoria() {
		return valorMontoPctePromotoria;
	}

	public void setValorMontoPctePromotoria(Double valorMontoPctePromotoria) {
		this.valorMontoPctePromotoria = valorMontoPctePromotoria;
	}

	@Column(name="NOAPLICADESCEXCEPCION")
	public Integer getNoAplicaDescuentoExcepcion() {
		return noAplicaDescuentoExcepcion;
	}

	public void setNoAplicaDescuentoExcepcion(Integer noAplicaDescuentoExcepcion) {
		this.noAplicaDescuentoExcepcion = noAplicaDescuentoExcepcion;
	}

	@Column(name="DESCUENTOPOLIZA")
	public Double getDescuentoPoliza() {
		return descuentoPoliza;
	}

	public void setDescuentoPoliza(Double descuentoPoliza) {
		this.descuentoPoliza = descuentoPoliza;
	}
	
	@Transient
	public BigDecimal getIdPoliza() {
		return idPoliza;
	}

	public void setIdPoliza(BigDecimal idPoliza) {
		this.idPoliza = idPoliza;
	}
		
	@Transient
	public Integer getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(Integer numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	@Transient
	public String getPolizaFormateada() {
		return polizaFormateada;
	}

	public void setPolizaFormateada(String polizaFormateada) {
		this.polizaFormateada = polizaFormateada;
	}

	
}
