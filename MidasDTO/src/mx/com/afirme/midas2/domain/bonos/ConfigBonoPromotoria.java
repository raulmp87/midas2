package mx.com.afirme.midas2.domain.bonos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.eclipse.persistence.annotations.Customizer;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
/**
 * 
 * @author vmhersil
 *
 */
@Entity(name="ConfigBonoPromotoria")
@Table(name="trConfigBonoPromotoria",schema="MIDAS")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class ConfigBonoPromotoria implements Serializable,Entidad{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1739456915554372022L;
	private Long	id;
	private	ConfigBonos configuracionBonos;
	private Promotoria promotoria;
	
	public ConfigBonoPromotoria(){}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idTrCfgBonoPromotoria_seq")
	@SequenceGenerator(name="idTrCfgBonoPromotoria_seq", sequenceName="MIDAS.idTrCfgBonoPromotoria_seq",allocationSize=1)
	@Column(name="ID")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ConfigBonos.class)
	@JoinColumn(name="CONFIGBONO_ID")
	public ConfigBonos getConfiguracionBonos() {
		return configuracionBonos;
	}

	public void setConfiguracionBonos(ConfigBonos configuracionBonos) {
		this.configuracionBonos = configuracionBonos;
	}
	
	@ManyToOne(fetch=FetchType.LAZY,targetEntity=Promotoria.class)
	@JoinColumn(name="PROMOTORIA_ID")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Promotoria getPromotoria() {
		return promotoria;
	}

	public void setPromotoria(Promotoria promotoria) {
		this.promotoria = promotoria;
	}
}
