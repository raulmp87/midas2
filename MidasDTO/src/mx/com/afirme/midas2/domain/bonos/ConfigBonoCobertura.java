package mx.com.afirme.midas2.domain.bonos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.util.StaticCommonVariables;
/**
 * 
 * @author vmhersil
 *
 */
@Entity(name="ConfigBonoCobertura")
@Table(schema=StaticCommonVariables.DEFAULT_SCHEMA,name="trConfigBonoCobertura")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class ConfigBonoCobertura implements Entidad,Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2772619209549244845L;
	private Long id;
	private ConfigBonos configuracionBonos;
	private Long idCobertura;
	public ConfigBonoCobertura(){}
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idTrConfigBonoCobertura_seq")
	@SequenceGenerator(name="idTrConfigBonoCobertura_seq", sequenceName="MIDAS.idTrConfigBonoCobertura_seq",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ConfigBonos.class)
	@JoinColumn(name="IDCONFIGBONO")
	public ConfigBonos getConfiguracionBonos() {
		return configuracionBonos;
	}
	public void setConfiguracionBonos(ConfigBonos configuracionBonos) {
		this.configuracionBonos = configuracionBonos;
	}
	@Column(name="IDCOBERTURA")
	public Long getIdCobertura() {
		return idCobertura;
	}
	public void setIdCobertura(Long idCobertura) {
		this.idCobertura = idCobertura;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}
	@Override
	public String getValue() {
		return null;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	
}
