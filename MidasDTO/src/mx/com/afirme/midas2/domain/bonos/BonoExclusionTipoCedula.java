/**
 * 
 */
package mx.com.afirme.midas2.domain.bonos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.eclipse.persistence.annotations.Customizer;

import mx.com.afirme.midas2.annotation.HistoryTable;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.util.StaticCommonVariables;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

/**
 * @author sams
 *
 */
@Entity(name="BonoExclusionTipoCedula")
@Table(schema= StaticCommonVariables.DEFAULT_SCHEMA, name="TRCONFIGBONOEXCLUSIONTIPOCED")
@HistoryTable(name="TRCONFIGBONOEXCLUSTIPOCED_HIST",schema="MIDAS")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class BonoExclusionTipoCedula implements Entidad, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1146558539456734475L;
	Long idExclusionCedula;
	ConfigBonos idConfig;
	ValorCatalogoAgentes idTipoBono;
	/**
	 * 
	 */
	public BonoExclusionTipoCedula() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see mx.com.afirme.midas2.dao.catalogos.Entidad#getKey()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		// TODO Auto-generated method stub
		return idExclusionCedula;
	}

	/* (non-Javadoc)
	 * @see mx.com.afirme.midas2.dao.catalogos.Entidad#getValue()
	 */
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.afirme.midas2.dao.catalogos.Entidad#getBusinessKey()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		// TODO Auto-generated method stub
		return idExclusionCedula;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTRCONFIGBONOEXCTIPOCED_SEQ")
	@SequenceGenerator(name="IDTRCONFIGBONOEXCTIPOCED_SEQ", sequenceName ="MIDAS.IDTRCONFIGBONOEXCTIPOCED_SEQ" ,allocationSize=1)
	@Column(name = "ID", nullable=false, unique= true)
	public Long getIdExclusionCedula() {
		return idExclusionCedula;
	}

	public void setIdExclusionCedula(Long idExclusionCedula) {
		this.idExclusionCedula = idExclusionCedula;
	}

	@ManyToOne(fetch=FetchType.EAGER, targetEntity = ConfigBonos.class)
	@JoinColumn(name="IDCONFIG")
	@NotNull(groups =EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ConfigBonos getIdConfig() {
		return idConfig;
	}

	public void setIdConfig(ConfigBonos idConfig) {
		this.idConfig = idConfig;
	}
	
	@ManyToOne(fetch= FetchType.EAGER, targetEntity = ValorCatalogoAgentes.class)
	@JoinColumn(name="IDTIPOBONO")
	@NotNull(groups =EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ValorCatalogoAgentes getIdTipoBono() {
		return idTipoBono;
	}

	public void setIdTipoBono(ValorCatalogoAgentes idTipoBono) {
		this.idTipoBono = idTipoBono;
	}

}
