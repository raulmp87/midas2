package mx.com.afirme.midas2.domain.bonos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Customizer;

import mx.com.afirme.midas2.annotation.HistoryTable;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;

@Entity(name="ConfigBonoExcepAgente")
@Table(name="TRCONFIGBONOEXCEPCIONAGENTE",schema="MIDAS")
@HistoryTable(name="TRCONFIGBONOEXCEPCIONAGT_HIST",schema="MIDAS")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class ConfigBonoExcepAgente implements Serializable,Entidad{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2982215761736140705L;
	private Long id;
	private ConfigBonos configBono;
	private Agente agente;
	private Double valorMontoPcteAgente;
	private Integer noAplicaDescuentoExcepcion;
	private Long idAgente;
		
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="idTrConfigBonoExcAgente_seq")
	@SequenceGenerator(name="idTrConfigBonoExcAgente_seq",sequenceName="MIDAS.idTrConfigBonoExcAgente_seq",allocationSize=1)
	@Column(name="ID")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ConfigBonos.class)
	@JoinColumn(name="IDCONFIG")
	public ConfigBonos getConfigBono() {
		return configBono;
	}

	public void setConfigBono(ConfigBonos configBono) {
		this.configBono = configBono;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=Agente.class)
	@JoinColumn(name="IDAGENTE")
	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	
	@Column(name="VALORMONTOPCTEAGENTE")
	public Double getValorMontoPcteAgente() {
		return valorMontoPcteAgente;
	}

	public void setValorMontoPcteAgente(Double valorMontoPcteAgente) {
		this.valorMontoPcteAgente = valorMontoPcteAgente;
	}

	@Column(name="NOAPLICADESCEXCEPCION")
	public Integer getNoAplicaDescuentoExcepcion() {
		return noAplicaDescuentoExcepcion;
	}
	
	public void setNoAplicaDescuentoExcepcion(Integer noAplicaDescuentoExcepcion) {
		this.noAplicaDescuentoExcepcion = noAplicaDescuentoExcepcion;
	}
	
	@Transient
	public Long getIdAgente() {
		return idAgente;
	}

	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}


	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}
