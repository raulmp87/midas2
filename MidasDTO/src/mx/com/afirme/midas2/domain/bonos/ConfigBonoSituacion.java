package mx.com.afirme.midas2.domain.bonos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.eclipse.persistence.annotations.Customizer;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
/**
 * 
 * @author vmhersil
 *
 */
@Entity(name="ConfigBonoSituacion")
@Table(schema="MIDAS",name="trConfigBonoSituacion")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class ConfigBonoSituacion implements Entidad,Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1938860838316961207L;
	private Long	id;
	private	ConfigBonos configuracionBonos;
	private ValorCatalogoAgentes situacionAgente;
	
	public ConfigBonoSituacion(){}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idTrCfgBonoSituacion_seq")
	@SequenceGenerator(name="idTrCfgBonoSituacion_seq", sequenceName="MIDAS.idTrCfgBonoSituacion_seq",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ConfigBonos.class)
	@JoinColumn(name="CONFIGBONO_ID")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ConfigBonos getConfiguracionBonos() {
		return configuracionBonos;
	}

	public void setConfiguracionBonos(ConfigBonos configuracionBonos) {
		this.configuracionBonos = configuracionBonos;
	}
	
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDSITUACIONAGENTE")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ValorCatalogoAgentes getSituacionAgente() {
		return situacionAgente;
	}

	public void setSituacionAgente(ValorCatalogoAgentes situacionAgente) {
		this.situacionAgente = situacionAgente;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null; 
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
}
