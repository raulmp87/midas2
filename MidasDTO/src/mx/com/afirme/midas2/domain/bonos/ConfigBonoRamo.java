package mx.com.afirme.midas2.domain.bonos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.util.StaticCommonVariables;
/**
 * 
 * @author vmhersil
 *
 */
@Entity(name="ConfigBonoRamo")
@Table(schema=StaticCommonVariables.DEFAULT_SCHEMA,name="trConfigBonoRamo")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class ConfigBonoRamo implements Entidad,Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8711858719530944993L;
	private Long id;
	private ConfigBonos configuracionBonos;
	private Long idRamo;
	public ConfigBonoRamo(){}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idTrConfigBonoRamo_seq")
	@SequenceGenerator(name="idTrConfigBonoRamo_seq", sequenceName="MIDAS.idTrConfigBonoRamo_seq",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ConfigBonos.class)
	@JoinColumn(name="IDCONFIGBONO")
	public ConfigBonos getConfiguracionBonos() {
		return configuracionBonos;
	}


	public void setConfiguracionBonos(ConfigBonos configuracionBonos) {
		this.configuracionBonos = configuracionBonos;
	}

	@Column(name="IDRAMO")
	public Long getIdRamo() {
		return idRamo;
	}


	public void setIdRamo(Long idRamo) {
		this.idRamo = idRamo;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

}
