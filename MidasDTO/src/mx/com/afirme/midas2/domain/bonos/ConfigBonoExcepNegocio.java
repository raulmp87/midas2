package mx.com.afirme.midas2.domain.bonos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Customizer;

import mx.com.afirme.midas2.annotation.HistoryTable;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.negocio.Negocio;

@Entity(name="ConfigBonoExcepNegocio")
@Table(name="TRCONFIGBONOEXCEPCIONNEGOCIO",schema="MIDAS")
@HistoryTable(name="TRCONFIGBONOEXCEPCIONNEGO_HIST",schema="MIDAS")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class ConfigBonoExcepNegocio implements Serializable,Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4538587300301847891L;
	private Long id;
	private ConfigBonos configBono;
	private Negocio negocio;
	private Double valorMontoPcteAgente;
	private Double valorMontoPctePromotoria;
	private Double descuentoMaximo; //pcteDescuento;//descuentoMaximo;
	private Long idNegocio;
	
		
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="idTrConfigBonoExcNegocio_seq")
	@SequenceGenerator(name="idTrConfigBonoExcNegocio_seq",sequenceName="MIDAS.idTrConfigBonoExcNegocio_seq",allocationSize=1)
	@Column(name="ID")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ConfigBonos.class)
	@JoinColumn(name="IDCONFIG")
	public ConfigBonos getConfigBono() {
		return configBono;
	}

	public void setConfigBono(ConfigBonos configBono) {
		this.configBono = configBono;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=Negocio.class)
	@JoinColumn(name="IDNEGOCIO")
	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	@Column(name="VALORMONTOPCTEAGENTE")	
	public Double getValorMontoPcteAgente() {
		return valorMontoPcteAgente;
	}

	public void setValorMontoPcteAgente(Double valorMontoPcteAgente) {
		this.valorMontoPcteAgente = valorMontoPcteAgente;
	}

	@Column(name="VALORMONTOPCTEPROMOTORIA")
	public Double getValorMontoPctePromotoria() {
		return valorMontoPctePromotoria;
	}

	public void setValorMontoPctePromotoria(Double valorMontoPctePromotoria) {
		this.valorMontoPctePromotoria = valorMontoPctePromotoria;
	}

	@Column(name="DESCUENTOMAXIMO")
	public Double getDescuentoMaximo() {
		return descuentoMaximo;
	}

	public void setDescuentoMaximo(Double descuentoMaximo) {
		this.descuentoMaximo = descuentoMaximo;
	}
	
	
	@Transient
	public Long getIdNegocio() {
		return idNegocio;
	}


	public void setIdNegocio(Long idNegocio) {
		this.idNegocio = idNegocio;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	
	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}
