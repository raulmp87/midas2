package mx.com.afirme.midas2.domain.bonos;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;

import mx.com.afirme.midas2.dao.catalogos.Entidad;



@Entity(name="ConfigBonoRangoAplica")
@Table(name="TRCONFIGBONORANGOAPLICA",schema="MIDAS")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class ConfigBonoRangoAplica implements Serializable,Entidad{

	/**
	 * 
	 * 
	 */
	private static final long serialVersionUID = -9219398578181336645L;
	private Long id;
	private ConfigBonos configBono;
	private BigDecimal importePrimaInicial;
	private BigDecimal importePrimaFinal;
	private BigDecimal pctSiniestralidadInicial;
	private BigDecimal pctSiniestralidadFinal;
	private BigDecimal pctCrecimientoInicial;
	private BigDecimal pctCrecimientoFinal;
	private BigDecimal pctSupMetaInicial;
	private BigDecimal pctSupMetaFinal;
	private BigDecimal valorAplicacion;
	private Integer numPolizasEmitidasInicial;
	private Integer numPolizasEmitidasFinal;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="IDTRCFGBONORANGOAPLICA_SEQ")
	@SequenceGenerator(name="IDTRCFGBONORANGOAPLICA_SEQ",sequenceName="MIDAS.IDTRCFGBONORANGOAPLICA_SEQ",allocationSize=1)
	@Column(name="ID")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ConfigBonos.class)
	@JoinColumn(name="CONFIGBONO_ID")
	public ConfigBonos getConfigBono() {
		return configBono;
	}

	public void setConfigBono(ConfigBonos configBono) {
		this.configBono = configBono;
	}

	@Column(name="IMPORTEPRIMAINICIAL")
	public BigDecimal getImportePrimaInicial() {
		return importePrimaInicial;
	}

	public void setImportePrimaInicial(BigDecimal importePrimaInicial) {
		this.importePrimaInicial = importePrimaInicial;
	}

	@Column(name="IMPORTEPRIMAFINAL")
	public BigDecimal getImportePrimaFinal() {
		return importePrimaFinal;
	}

	public void setImportePrimaFinal(BigDecimal importePrimaFinal) {
		this.importePrimaFinal = importePrimaFinal;
	}

	@Column(name="PCTSINIESTRALIDADINICIAL")
	public BigDecimal getPctSiniestralidadInicial() {
		return pctSiniestralidadInicial;
	}

	public void setPctSiniestralidadInicial(BigDecimal pctSiniestralidadInicial) {
		this.pctSiniestralidadInicial = pctSiniestralidadInicial;
	}

	@Column(name="PCTSINIESTRALIDADFINAL")
	public BigDecimal getPctSiniestralidadFinal() {
		return pctSiniestralidadFinal;
	}

	public void setPctSiniestralidadFinal(BigDecimal pctSiniestralidadFinal) {
		this.pctSiniestralidadFinal = pctSiniestralidadFinal;
	}

	@Column(name="PCTCRECIMIENTOINICIAL")
	public BigDecimal getPctCrecimientoInicial() {
		return pctCrecimientoInicial;
	}

	public void setPctCrecimientoInicial(BigDecimal pctCrecimientoInicial) {
		this.pctCrecimientoInicial = pctCrecimientoInicial;
	}

	@Column(name="PCTCRECIMIENTOFINAL")
	public BigDecimal getPctCrecimientoFinal() {
		return pctCrecimientoFinal;
	}

	public void setPctCrecimientoFinal(BigDecimal pctCrecimientoFinal) {
		this.pctCrecimientoFinal = pctCrecimientoFinal;
	}

	@Column(name="PCTSUPMETAINICIAL")
	public BigDecimal getPctSupMetaInicial() {
		return pctSupMetaInicial;
	}

	public void setPctSupMetaInicial(BigDecimal pctSupMetaInicial) {
		this.pctSupMetaInicial = pctSupMetaInicial;
	}

	@Column(name="PCTSUPMETAFINAL")
	public BigDecimal getPctSupMetaFinal() {
		return pctSupMetaFinal;
	}

	public void setPctSupMetaFinal(BigDecimal pctSupMetaFinal) {
		this.pctSupMetaFinal = pctSupMetaFinal;
	}

	@Column(name="VALORAPLICACION")
	public BigDecimal getValorAplicacion() {
		return valorAplicacion;
	}

	public void setValorAplicacion(BigDecimal valorAplicacion) {
		this.valorAplicacion = valorAplicacion;
	}

	@Column(name="NUMPOLIZASEMITIDASINICIAL")
	public Integer getNumPolizasEmitidasInicial() {
		return numPolizasEmitidasInicial;
	}

	public void setNumPolizasEmitidasInicial(Integer numPolizasEmitidasInicial) {
		this.numPolizasEmitidasInicial = numPolizasEmitidasInicial;
	}

	@Column(name="NUMPOLIZASEMITIDASFINAL")
	public Integer getNumPolizasEmitidasFinal() {
		return numPolizasEmitidasFinal;
	}

	public void setNumPolizasEmitidasFinal(Integer numPolizasEmitidasFinal) {
		this.numPolizasEmitidasFinal = numPolizasEmitidasFinal;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	

}
