package mx.com.afirme.midas2.domain.bonos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;

import mx.com.afirme.midas2.annotation.HistoryTable;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="TRCONFIGBONORANGOCLAVEAMIS",schema="MIDAS")
@HistoryTable(name="TRCONFIGBONORANGCLAVAMIS_HIST",schema="MIDAS")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class ConfigBonoRangoClaveAmis implements Serializable,Entidad{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private ConfigBonos configBono;
	private String cve_amis_ini;
	private String cve_amis_fin;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="IDBONORANGOCLAVEAMIS_SEQ")
	@SequenceGenerator(name="IDBONORANGOCLAVEAMIS_SEQ",sequenceName="MIDAS.IDBONORANGOCLAVEAMIS_SEQ",allocationSize=1)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ConfigBonos.class)
	@JoinColumn(name="configBono_id")
	public ConfigBonos getConfigBono() {
		return configBono;
	}
	public void setConfigBono(ConfigBonos configBono) {
		this.configBono = configBono;
	}
	@Column(name="cve_amis_ini")
	public String getCve_amis_ini() {
		return cve_amis_ini;
	}
	public void setCve_amis_ini(String cve_amis_ini) {
		this.cve_amis_ini = cve_amis_ini;
	}
	@Column(name="cve_amis_fin")
	public String getCve_amis_fin() {
		return cve_amis_fin;
	}
	public void setCve_amis_fin(String cve_amis_fin) {
		this.cve_amis_fin = cve_amis_fin;
	}
	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
