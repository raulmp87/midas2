package mx.com.afirme.midas2.domain.bonos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;

@Entity(name="DetalleCalculoBono")
@Table(name="toDetalleCalculoBono",schema="MIDAS")
public class DetalleCalculoBono implements Entidad,Serializable{

	private static final long serialVersionUID = -6297216268230481571L;

	private Long id;
	private ValorCatalogoAgentes tipoBeneficiario;
	private String beneficiario;
//	private Double prima;
	private BigDecimal prima;
	private String descripcionBono;
	private BigDecimal bonoPagar;
	private CalculoBono calculoBono;
	private ValorCatalogoAgentes status;
	private Long idSolicitudCheque;
	private Long idBeneficiario;
	private Date fechaAplicacion;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="idToDetalleCalculoBonos_seq")
	@SequenceGenerator(name="idToDetalleCalculoBonos_seq",sequenceName="MIDAS.idToDetalleCalculoBonos_seq",allocationSize=1)
	@Column(name="ID")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="TIPOBENEFICIARIO")
	public ValorCatalogoAgentes getTipoBeneficiario() {
		return tipoBeneficiario;
	}

	public void setTipoBeneficiario(ValorCatalogoAgentes tipoBeneficiario) {
		this.tipoBeneficiario = tipoBeneficiario;
	}

	@Column(name="BENEFICIARIO")
	public String getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	@Column(name="PRIMA")
	public BigDecimal getPrima() {
		return prima;
	}

	public void setPrima(BigDecimal prima) {
		this.prima = prima;
	}
//	public Double getPrima() {
//		return prima;
//	}
//
//	public void setPrima(Double prima) {
//		this.prima = prima;
//	}

	@Column(name="DESCRIPCIONBONO")
	public String getDescripcionBono() {
		return descripcionBono;
	}

	public void setDescripcionBono(String descripcionBono) {
		this.descripcionBono = descripcionBono;
	}

	@Column(name="BONOPAGAR")
	public BigDecimal getBonoPagar() {
		return bonoPagar;
	}

	public void setBonoPagar(BigDecimal bonoPagar) {
		this.bonoPagar = bonoPagar;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=CalculoBono.class)
	@JoinColumn(name="IDCALCULO")
	public CalculoBono getCalculoBono() {
		return calculoBono;
	}

	public void setCalculoBono(CalculoBono calculoBono) {
		this.calculoBono = calculoBono;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="STATUS")
	public ValorCatalogoAgentes getStatus() {
		return status;
	}

	public void setStatus(ValorCatalogoAgentes status) {
		this.status = status;
	}
	
	@Column(name="IDSOLICITUDCHEQUE")
	public Long getIdSolicitudCheque() {
		return idSolicitudCheque;
	}
	
	public void setIdSolicitudCheque(Long idSolicitudCheque) {
		this.idSolicitudCheque = idSolicitudCheque;
	}
	
	@Column(name="IDBENEFICIARIO")
	public Long getIdBeneficiario() {
		return idBeneficiario;
	}

	public void setIdBeneficiario(Long idBeneficiario) {
		this.idBeneficiario = idBeneficiario;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHAAPLICACION")
	public Date getFechaAplicacion() {
		return fechaAplicacion;
	}

	public void setFechaAplicacion(Date fechaAplicacion) {
		this.fechaAplicacion = fechaAplicacion;
	}

}
