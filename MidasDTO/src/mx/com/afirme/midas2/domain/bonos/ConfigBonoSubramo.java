package mx.com.afirme.midas2.domain.bonos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.util.StaticCommonVariables;
/**
 * 
 * @author vmhersil
 *
 */
@Entity(name="ConfigBonoSubramo")
@Table(schema=StaticCommonVariables.DEFAULT_SCHEMA,name="trConfigBonoSubramo")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class ConfigBonoSubramo implements Entidad,Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3788198900380889104L;
	private Long id;
	private ConfigBonos configuracionBonos;
	private Long idSubramo;
	public ConfigBonoSubramo(){}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idTrConfigBonoSubramo_seq")
	@SequenceGenerator(name="idTrConfigBonoSubramo_seq", sequenceName="MIDAS.idTrConfigBonoSubramo_seq",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ConfigBonos.class)
	@JoinColumn(name="IDCONFIGBONO")
	public ConfigBonos getConfiguracionBonos() {
		return configuracionBonos;
	}
	public void setConfiguracionBonos(ConfigBonos configuracionBonos) {
		this.configuracionBonos = configuracionBonos;
	}
	
	@Column(name="IDSUBRAMO")
	public Long getIdSubramo() {
		return idSubramo;
	}
	public void setIdSubramo(Long idSubramo) {
		this.idSubramo = idSubramo;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}
	@Override
	public String getValue() {
		return null;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

}
