package mx.com.afirme.midas2.domain.bonos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;

@Entity(name="CalculoBono")
@Table(name="toCalculoBono",schema="MIDAS")
public class CalculoBono implements Entidad,Serializable{

	private static final long serialVersionUID = 8960402748369171898L;
	
	private Long id;
	private ValorCatalogoAgentes tipoBono;
	private Date ultimoPago;
	private String ultimoPagoString;
	private Date fechaCorte;
	private String fechaCorteString;
	private ValorCatalogoAgentes produccionSobre;
	private Date produccionDesde;
	private String produccionDesdeString;
	private Date produccionHasta;
	private String produccionHastaString;
	private ValorCatalogoAgentes modoAplicacion;
	private Integer totalBeneficiarios;
	private BigDecimal importeTotal;
	private ProgramacionBono programacionBono;	
	private ValorCatalogoAgentes modoEjecucion;
	private String descripcionBono;
	private ValorCatalogoAgentes status;
	private Long banderaExisteAmis;
	private Boolean enProceso;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idToCalculoBono_seq")
	@SequenceGenerator(name="idToCalculoBono_seq", sequenceName="MIDAS.idToCalcloBono_seq",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="TIPOBONO")
	public ValorCatalogoAgentes getTipoBono() {
		return tipoBono;
	}

	public void setTipoBono(ValorCatalogoAgentes tipoBono) {
		this.tipoBono = tipoBono;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="ULTIMOPAGO")
	public Date getUltimoPago() {
		return ultimoPago;
	}

	public void setUltimoPago(Date ultimoPago) {
		this.ultimoPago = ultimoPago;
		if(ultimoPago!=null){ 
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");    	
		this.ultimoPagoString=sdf.format(ultimoPago);
		}
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHACORTE")
	public Date getFechaCorte() {
		return fechaCorte;
	}

	public void setFechaCorte(Date fechaCorte) {
		this.fechaCorte = fechaCorte;
		if(fechaCorte!=null){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			this.fechaCorteString=sdf.format(fechaCorte);
		}
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="PRODUCCIONSOBRE")
	public ValorCatalogoAgentes getProduccionSobre() {
		return produccionSobre;
	}

	public void setProduccionSobre(ValorCatalogoAgentes produccionSobre) {
		this.produccionSobre = produccionSobre;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="PRODUCCIONDESDE")
	public Date getProduccionDesde() {
		return produccionDesde;
	}

	public void setProduccionDesde(Date produccionDesde) {
		this.produccionDesde = produccionDesde;
		if(produccionDesde!=null){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");    	
			this.produccionDesdeString=sdf.format(produccionDesde);
		}
	}

	@Temporal(TemporalType.DATE)
	@Column(name="PRODUCCIONHASTA")
	public Date getProduccionHasta() {
		return produccionHasta;
	}

	public void setProduccionHasta(Date produccionHasta) {
		this.produccionHasta = produccionHasta;
		if(produccionHasta!=null){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");    	
			this.produccionHastaString=sdf.format(produccionHasta);
		}
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDMODOAPLICACION")
	public ValorCatalogoAgentes getModoAplicacion() {
		return modoAplicacion;
	}

	public void setModoAplicacion(ValorCatalogoAgentes modoAplicacion) {
		this.modoAplicacion = modoAplicacion;
	}

	@Column(name="TOTALBENEFICIARIOS")
	public Integer getTotalBeneficiarios() {
		return totalBeneficiarios;
	}

	public void setTotalBeneficiarios(Integer totalBeneficiarios) {
		this.totalBeneficiarios = totalBeneficiarios;
	}

	@Column(name="IMPORTETOTAL")
	public BigDecimal getImporteTotal() {
		return importeTotal;
	}

	public void setImporteTotal(BigDecimal importeTotal) {
		this.importeTotal = importeTotal;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ProgramacionBono.class)
	@JoinColumn(name="IDPROGRAMACIONBONO")
	public ProgramacionBono getProgramacionBono() {
		return programacionBono;
	}

	public void setProgramacionBono(ProgramacionBono programacionBono) {
		this.programacionBono = programacionBono;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDMODOEJECUCION")
	public ValorCatalogoAgentes getModoEjecucion() {
		return modoEjecucion;
	}

	public void setModoEjecucion(ValorCatalogoAgentes modoEjecucion) {
		this.modoEjecucion = modoEjecucion;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="STATUS")
	public ValorCatalogoAgentes getStatus() {
		return status;
	}

	public void setStatus(ValorCatalogoAgentes status) {
		this.status = status;
	}
	
	@Transient
	public String getUltimoPagoString() {
		return ultimoPagoString;
	}

	public void setUltimoPagoString(String ultimoPagoString) {
		this.ultimoPagoString = ultimoPagoString;
	}

	@Transient
	public String getFechaCorteString() {
		return fechaCorteString;
	}

	public void setFechaCorteString(String fechaCorteString) {
		this.fechaCorteString = fechaCorteString;
	}

	@Transient
	public String getProduccionDesdeString() {
		return produccionDesdeString;
	}

	public void setProduccionDesdeString(String produccionDesdeString) {
		this.produccionDesdeString = produccionDesdeString;
	}

	@Transient
	public String getProduccionHastaString() {
		return produccionHastaString;
	}

	public void setProduccionHastaString(String produccionHastaString) {
		this.produccionHastaString = produccionHastaString;
	}

	@Column(name="DESCRIPCIONBONO")
	public String getDescripcionBono() {
		return descripcionBono;
	}

	public void setDescripcionBono(String descripcionBono) {
		this.descripcionBono = descripcionBono;
	}

	@Transient
	public Long getBanderaExisteAmis() {
		return banderaExisteAmis;
	}
	
	public void setBanderaExisteAmis(Long banderaExisteAmis) {
		this.banderaExisteAmis = banderaExisteAmis;
	}
	
	@Column(name="EN_PROCESO", precision=1, scale=0)
	public Boolean isEnProceso() {
		return enProceso;
	}
	
	public void setEnProceso(Boolean enProceso) {
		this.enProceso = enProceso;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}


}
