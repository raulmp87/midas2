package mx.com.afirme.midas2.domain.bonos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.eclipse.persistence.annotations.Customizer;

import mx.com.afirme.midas2.annotation.HistoryTable;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.util.StaticCommonVariables;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
/**
 * 
 * @author
 *
 */

@Entity(name="BonoExclusionesTipoBono")
@Table(schema =StaticCommonVariables.DEFAULT_SCHEMA, name="TRCONFIGBONOEXCLUSIONTIPOBONO")
@HistoryTable(name="TRCONFIGBONOEXCLTIPOBONO_HIST",schema="MIDAS")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class BonoExclusionesTipoBono implements Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6157616487683214789L;
	Long id;
	ConfigBonos idConfig;
	ConfigBonos idBono;
	private String test;
	

	public BonoExclusionesTipoBono(){}

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTRCONFIGBONOEXCTIPOBONO_SEQ")
	@SequenceGenerator(name = "IDTRCONFIGBONOEXCTIPOBONO_SEQ", sequenceName="MIDAS.IDTRCONFIGBONOEXCTIPOBONO_SEQ", allocationSize = 1)
	@Column(name="ID", nullable=false, unique=true)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.EAGER, targetEntity=ConfigBonos.class)
	@JoinColumn(name = "IDCONFIG")
	@NotNull(groups =EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ConfigBonos getIdConfig() {
		return idConfig;
	}
	public void setIdConfig(ConfigBonos idConfig) {
		this.idConfig = idConfig;
	}

	@ManyToOne(fetch=FetchType.EAGER, targetEntity = ConfigBonos.class)
    @JoinColumn(name="IDBONO")
    @NotNull(groups =EditItemChecks.class, message="{com.afirme.midas2.requerido}")
    public ConfigBonos getIdBono() {
          return idBono;
    }

    public void setIdBono(ConfigBonos idBono) {
          this.idBono = idBono;
    }

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {		
		return id;
	}
	
}
