package mx.com.afirme.midas2.domain.tarifa;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dto.DynamicControl;
import mx.com.afirme.midas2.dto.DynamicControl.TipoControl;
import mx.com.afirme.midas2.dto.IdDynamicRow;
import mx.com.afirme.midas2.util.UtileriasWeb;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

@Entity (name="TarifaAuto")
@Table(name = "TOTARIFAAUTO", schema = "MIDAS")
public class TarifaAuto implements Serializable {
	private static final long serialVersionUID = 1L;

	private TarifaAutoId id;
	private BigDecimal valor;
	private Long idExterno = 0L;
	private Long idVerExterno = 0L;

	@DynamicControl(atributoMapeo="id", tipoControl = TipoControl.TEXTBOX,etiqueta="Clave:",editable=true,esComponenteId=true,secuencia="0" )
	@IdDynamicRow
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idMoneda", column = @Column(name = "IDMONEDA", nullable = false, precision = 38, scale = 0)),
			@AttributeOverride(name = "idToRiesgo", column = @Column(name = "IDTORIESGO", nullable = false, precision = 38, scale = 0)),
			@AttributeOverride(name = "idConcepto", column = @Column(name = "IDCONCEPTO", nullable = false, precision = 4, scale = 0)),
			@AttributeOverride(name = "idVerTarifa", column = @Column(name = "IDVERTARIFA", nullable = false, precision = 38, scale = 0)),
			@AttributeOverride(name = "idBase1", column = @Column(name = "IDBASE1", nullable = false, precision = 38, scale = 0)),
			@AttributeOverride(name = "idBase2", column = @Column(name = "IDBASE2", nullable = false, precision = 38, scale = 0)),
			@AttributeOverride(name = "idBase3", column = @Column(name = "IDBASE3", nullable = false, precision = 38, scale = 0)),
			@AttributeOverride(name = "idBase4", column = @Column(name = "IDBASE4", nullable = false, precision = 38, scale = 0)) })
	@Valid
	public TarifaAutoId getId() {
		return id;
	}
	public void setId(TarifaAutoId id) {
		this.id = id;
	}
	
	@Column(name = "VALOR", nullable = false, precision = 22, scale = 10)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer = 22, fraction= 10,groups=EditItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
	@Column(name = "IDEXTERNO", nullable = false, precision = 38, scale = 0)
	public Long getIdExterno() {
		return idExterno;
	}
	public void setIdExterno(Long idExterno) {
		this.idExterno = idExterno;
	}
	
	@Column(name = "IDVEREXTERNO", nullable = false, precision = 38, scale = 0)
	public Long getIdVerExterno() {
		return idVerExterno;
	}
	public void setIdVerExterno(Long idVerExterno) {
		this.idVerExterno = idVerExterno;
	}
	
	public static String[] validarClave(String clave){
		if(clave == null || clave.trim().equals("") || clave.indexOf(UtileriasWeb.SEPARADOR) == -1){
			return null;
		}
		
		String [] arrayClave = clave.split(UtileriasWeb.SEPARADOR);
		
		return arrayClave;
	}
}
