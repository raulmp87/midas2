package mx.com.afirme.midas2.domain.tarifa;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * AgrupadorTarifaId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class AgrupadorTarifaId implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields

	private BigDecimal idToAgrupadorTarifa;
	private BigDecimal idVerAgrupadorTarifa;

	// Constructors

	/** default constructor */
	public AgrupadorTarifaId() {
	}

	/** full constructor */
	public AgrupadorTarifaId(BigDecimal idToAgrupadorTarifa,
			BigDecimal idVerAgrupadorTarifa) {
		this.idToAgrupadorTarifa = idToAgrupadorTarifa;
		this.idVerAgrupadorTarifa = idVerAgrupadorTarifa;
	}

	// Property accessors

	@Column(name = "IDTOAGRUPADORTARIFA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToAgrupadorTarifa() {
		return this.idToAgrupadorTarifa;
	}

	public void setIdToAgrupadorTarifa(BigDecimal idToAgrupadorTarifa) {
		this.idToAgrupadorTarifa = idToAgrupadorTarifa;
	}

	@Column(name = "IDVERAGRUPADORTARIFA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdVerAgrupadorTarifa() {
		return this.idVerAgrupadorTarifa;
	}

	public void setIdVerAgrupadorTarifa(BigDecimal idVerAgrupadorTarifa) {
		this.idVerAgrupadorTarifa = idVerAgrupadorTarifa;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof AgrupadorTarifaId))
			return false;
		AgrupadorTarifaId castOther = (AgrupadorTarifaId) other;

		return ((this.getIdToAgrupadorTarifa() == castOther
				.getIdToAgrupadorTarifa()) || (this.getIdToAgrupadorTarifa() != null
				&& castOther.getIdToAgrupadorTarifa() != null && this
				.getIdToAgrupadorTarifa().equals(
						castOther.getIdToAgrupadorTarifa())))
				&& ((this.getIdVerAgrupadorTarifa() == castOther
						.getIdVerAgrupadorTarifa()) || (this
						.getIdVerAgrupadorTarifa() != null
						&& castOther.getIdVerAgrupadorTarifa() != null && this
						.getIdVerAgrupadorTarifa().equals(
								castOther.getIdVerAgrupadorTarifa())));
	}
	
	 @Override
	    public String toString(){
		 return ""+idToAgrupadorTarifa+"_"+idVerAgrupadorTarifa;
	 }

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToAgrupadorTarifa() == null ? 0 : this
						.getIdToAgrupadorTarifa().hashCode());
		result = 37
				* result
				+ (getIdVerAgrupadorTarifa() == null ? 0 : this
						.getIdVerAgrupadorTarifa().hashCode());
		return result;
	}

}
