package mx.com.afirme.midas2.domain.tarifa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TarifaVersionId implements Serializable {

	private static final long serialVersionUID = -7424705476827076467L;
		
	private Long idMoneda;
	
	private Long idRiesgo;
	
	private Long idConcepto;
	
	private Long version;


	@Column(name = "IDMONEDA", nullable = false, precision = 22, scale = 0)
	public Long getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}

	@Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)
	public Long getIdRiesgo() {
		return idRiesgo;
	}

	public void setIdRiesgo(Long idRiesgo) {
		this.idRiesgo = idRiesgo;
	}

	@Column(name = "IDCONCEPTO", nullable = false, precision = 4, scale = 0)
	public Long getIdConcepto() {
		return idConcepto;
	}

	public void setIdConcepto(Long idConcepto) {
		this.idConcepto = idConcepto;
	}

	@Column(name = "IDVERTARIFA", nullable = false, precision = 22, scale = 0)
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	
	
@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idConcepto == null) ? 0 : idConcepto.hashCode());
		result = prime * result
				+ ((idMoneda == null) ? 0 : idMoneda.hashCode());
		result = prime * result
				+ ((idRiesgo == null) ? 0 : idRiesgo.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TarifaVersionId other = (TarifaVersionId) obj;
		if (idConcepto == null) {
			if (other.idConcepto != null)
				return false;
		} else if (!idConcepto.equals(other.idConcepto))
			return false;
		if (idMoneda == null) {
			if (other.idMoneda != null)
				return false;
		} else if (!idMoneda.equals(other.idMoneda))
			return false;
		if (idRiesgo == null) {
			if (other.idRiesgo != null)
				return false;
		} else if (!idRiesgo.equals(other.idRiesgo))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	public TarifaVersionId(Long idMoneda, Long idRiesgo, Long idConcepto, 
			Long version) {
		this.idMoneda = idMoneda;
		this.idRiesgo = idRiesgo;
		this.idConcepto = idConcepto;
		this.version = version;
	}

	public TarifaVersionId() {
	}
	
	
	
	
	
}
