package mx.com.afirme.midas2.domain.tarifa;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.vigencia.VigenciaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;

@Entity
@Table(name = "TOTARIFASERVICIOPUBLICO", schema = "MIDAS")
public class TarifaServicioPublico implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;
	private BigDecimal id;
	private NegocioPaqueteSeccion negocioPaqueteSeccion;
	private MonedaDTO monedaDTO;
	private EstadoDTO estadoDTO;
	private CiudadDTO ciudadDTO;
	private CoberturaDTO coberturaDTO;
	private VigenciaDTO vigenciaDTO;
	private Double prima;
	private String valorSumaAsegurada;
	private Boolean seleccionado;

	// Constructors

	/** default constructor */
	public TarifaServicioPublico() {
	}

	// Property accessors
	@Id
	@SequenceGenerator(name="TAF_SEV_PUB_GEN", sequenceName="IDTOTARIFASERVICIOPUB_SEQ", allocationSize=1, schema="MIDAS")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAF_SEV_PUB_GEN")
	@Column(name = "id", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTONEGPAQUETESECCION", nullable = false)
	public NegocioPaqueteSeccion getNegocioPaqueteSeccion() {
		return this.negocioPaqueteSeccion;
	}

	public void setNegocioPaqueteSeccion(
			NegocioPaqueteSeccion negocioPaqueteSeccion) {
		this.negocioPaqueteSeccion = negocioPaqueteSeccion;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idMoneda", referencedColumnName="IDTCMONEDA", nullable = false)
	public MonedaDTO getMonedaDTO() {
		return monedaDTO;
	}

	public void setMonedaDTO(MonedaDTO monedaDTO) {
		this.monedaDTO = monedaDTO;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idTcEstado", referencedColumnName="STATE_ID", nullable = true)
	public EstadoDTO getEstadoDTO() {
		return estadoDTO;
	}

	public void setEstadoDTO(EstadoDTO estadoDTO) {
		this.estadoDTO = estadoDTO;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idTcCiudad", referencedColumnName="CITY_ID", nullable = true)
	public CiudadDTO getCiudadDTO() {
		return ciudadDTO;
	}

	public void setCiudadDTO(CiudadDTO ciudadDTO) {
		this.ciudadDTO = ciudadDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOCOBERTURA", nullable = false)
	public CoberturaDTO getCoberturaDTO() {
		return this.coberturaDTO;
	}

	public void setCoberturaDTO(CoberturaDTO coberturaDTO) {
		this.coberturaDTO = coberturaDTO;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCVIGENCIA", nullable = false)
	public VigenciaDTO getVigenciaDTO() {
		return vigenciaDTO;
	}

	public void setVigenciaDTO(VigenciaDTO vigenciaDTO) {
		this.vigenciaDTO = vigenciaDTO;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	@Column(name = "prima", nullable = false, precision = 16)
	public Double getPrima() {
		return prima;
	}

	public void setPrima(Double prima) {
		this.prima = prima;
	}

	@Transient
	public String getValorSumaAsegurada() {
		return valorSumaAsegurada;
	}

	public void setValorSumaAsegurada(String valorSumaAsegurada) {
		this.valorSumaAsegurada = valorSumaAsegurada;
	}
	
	@Transient
	public Boolean getSeleccionado() {
		return seleccionado;
	}

	public void setSeleccionado(Boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
}
