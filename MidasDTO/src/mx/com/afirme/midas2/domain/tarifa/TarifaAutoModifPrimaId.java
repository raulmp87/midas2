package mx.com.afirme.midas2.domain.tarifa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dto.DynamicControl;
import mx.com.afirme.midas2.dto.DynamicControl.TipoControl;
import mx.com.afirme.midas2.dto.RegistroDinamicoDTO;
import mx.com.afirme.midas2.dto.componente.vista.VistaTarifaVarModificadoraPrima;
import mx.com.afirme.midas2.util.UtileriasWeb;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

import org.apache.commons.lang.math.NumberUtils;
@Embeddable
public class TarifaAutoModifPrimaId  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3104747294155508763L;

//	private Long idLineaNegocio;
	
	private Long idMoneda;
	
	private Long idRiesgo;
	
	private Long idConcepto;
	
	private Long version;
		
	private Long variableModifPrimaId;
	
	
	@DynamicControl(atributoMapeo="id.idMoneda", tipoControl = TipoControl.HIDDEN,etiqueta="Moneda",
			esComponenteId=true,editable=true,esNumerico=true,secuencia="2",vistas=VistaTarifaVarModificadoraPrima.class)
	@Column(name = "IDMONEDA", nullable = false, precision = 22, scale = 0)
	public Long getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}
	
	@DynamicControl(atributoMapeo="id.idRiesgo", tipoControl = TipoControl.HIDDEN,etiqueta="Riesgo",
			esComponenteId=true,editable=true,esNumerico=true,secuencia="3",vistas=VistaTarifaVarModificadoraPrima.class)
	@Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)
	public Long getIdRiesgo() {
		return idRiesgo;
	}

	public void setIdRiesgo(Long idRiesgo) {
		this.idRiesgo = idRiesgo;
	}
	
	@DynamicControl(atributoMapeo="id.idConcepto", tipoControl = TipoControl.HIDDEN,etiqueta="Concepto",
			esComponenteId=true,editable=true,esNumerico=true,secuencia="4",vistas=VistaTarifaVarModificadoraPrima.class)
	@Column(name = "IDCONCEPTO", nullable = false, precision = 4, scale = 0)
	public Long getIdConcepto() {
		return idConcepto;
	}

	public void setIdConcepto(Long idConcepto) {
		this.idConcepto = idConcepto;
	}
	
	@DynamicControl(atributoMapeo="id.version", tipoControl = TipoControl.HIDDEN,etiqueta="Versi\u00F3n",
			esComponenteId=true,editable=true,esNumerico=true,secuencia="5",vistas=VistaTarifaVarModificadoraPrima.class)
	@Column(name = "IDVERTARIFA", nullable = false, precision = 22, scale = 0)
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
	
	@Column(name="IDTCVARMODIFPRIMA",nullable=false,precision=22,scale=0)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer = 10, fraction= 0,groups=EditItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public Long getVariableModifPrimaId() {
		return variableModifPrimaId;
	}

	public void setVariableModifPrimaId(Long variableModifPrimaId) {
		this.variableModifPrimaId = variableModifPrimaId;
	}


	public TarifaAutoModifPrimaId(Long idMoneda, Long idRiesgo,Long idConcepto, Long version, Long variableModifPrimaId) {
		this.idMoneda = idMoneda;
		this.idRiesgo = idRiesgo;
		this.idConcepto = idConcepto;
		this.version = version;
		this.variableModifPrimaId = variableModifPrimaId;
	}

	public TarifaAutoModifPrimaId() {
	}
	
	public TarifaAutoModifPrimaId(TarifaVersionId tarifaVersionId){
		if(tarifaVersionId!=null){
			this.idMoneda=tarifaVersionId.getIdMoneda();
			this.idRiesgo=tarifaVersionId.getIdRiesgo();
			this.idConcepto=tarifaVersionId.getIdConcepto();
			this.version=tarifaVersionId.getVersion();
		}
	}
	
	public TarifaAutoModifPrimaId(String key){
		if(key!=null && !key.trim().isEmpty()){
			key=key.trim();
			if(key.startsWith(RegistroDinamicoDTO.TARIFA)){
				String[] parts=key.split(UtileriasWeb.SEPARADOR);
				//Verificamos si tiene al menos 6 elementos que son los requeridos por la clave
				if(parts != null && parts.length >= 6){
					//Llave: T_idMoneda_idRiesgo_idConcepto_version_idVariableModifPrima
					
					this.idMoneda=(NumberUtils.isNumber(parts[1]))?Long.parseLong(parts[1]):0l;
					this.idRiesgo=(NumberUtils.isNumber(parts[2]))?Long.parseLong(parts[2]):0l;
					this.idConcepto=(NumberUtils.isNumber(parts[3]))?Long.parseLong(parts[3]):0l;
					this.version=(NumberUtils.isNumber(parts[4]))?Long.parseLong(parts[4]):0l;
					this.variableModifPrimaId = (NumberUtils.isNumber(parts[5]))?Long.parseLong(parts[5]):0l;
				}
			}
		}
	}
	
	@Override
	public String toString(){
		String cadena="T_"+getIdMoneda()+UtileriasWeb.SEPARADOR+getIdRiesgo()+UtileriasWeb.SEPARADOR+getIdConcepto()+UtileriasWeb.SEPARADOR
		+getVersion()+UtileriasWeb.SEPARADOR+ getVariableModifPrimaId();
		return cadena;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idConcepto == null) ? 0 : idConcepto.hashCode());
		result = prime * result
				+ ((idMoneda == null) ? 0 : idMoneda.hashCode());
		result = prime * result
				+ ((idRiesgo == null) ? 0 : idRiesgo.hashCode());
		result = prime
				* result
				+ ((variableModifPrimaId == null) ? 0 : variableModifPrimaId
						.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TarifaAutoModifPrimaId other = (TarifaAutoModifPrimaId) obj;
		if (idConcepto == null) {
			if (other.idConcepto != null)
				return false;
		} else if (!idConcepto.equals(other.idConcepto))
			return false;
		if (idMoneda == null) {
			if (other.idMoneda != null)
				return false;
		} else if (!idMoneda.equals(other.idMoneda))
			return false;
		if (idRiesgo == null) {
			if (other.idRiesgo != null)
				return false;
		} else if (!idRiesgo.equals(other.idRiesgo))
			return false;
		if (variableModifPrimaId == null) {
			if (other.variableModifPrimaId != null)
				return false;
		} else if (!variableModifPrimaId.equals(other.variableModifPrimaId))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}
	
	
	
	
}
