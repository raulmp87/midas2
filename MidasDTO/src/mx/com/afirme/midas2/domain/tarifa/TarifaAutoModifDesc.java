package mx.com.afirme.midas2.domain.tarifa;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.domain.catalogos.VarModifDescripcion;
import mx.com.afirme.midas2.dto.CatalogoAyuda;
import mx.com.afirme.midas2.dto.DynamicControl;
import mx.com.afirme.midas2.dto.DynamicControl.TipoControl;
import mx.com.afirme.midas2.dto.DynamicControl.TipoValidacion;
import mx.com.afirme.midas2.dto.IdDynamicRow;
import mx.com.afirme.midas2.dto.componente.grupo.GrupoVariableModificadoraDescripcion;
import mx.com.afirme.midas2.dto.componente.vista.VistaTarifaVarModificadoraPrima;
import mx.com.afirme.midas2.validator.group.NewItemChecks;
import mx.com.afirme.midas2.validator.group.TarifaAutoModifChecks;

@Entity
@Table(name="TOTARIFAAUTOMODIFDESC", schema="MIDAS")
public class TarifaAutoModifDesc implements Serializable {

	private static final long serialVersionUID = 2022915850179775481L;
	
	private TarifaAutoModifDescId id;
	
	private VarModifDescripcion varModifDescripcion;
	
	private BigDecimal valor;
	
	@IdDynamicRow
	@EmbeddedId
	public TarifaAutoModifDescId getId() {
		return id;
	}
	
	public void setId(TarifaAutoModifDescId id) {
		this.id = id;
	}
	
	@MapsId("varModifDescripcionId")
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCVARMODIFDESCRIPCION", referencedColumnName = "IDTCVARMODIFDESCRIPCION", nullable = false)
	@CatalogoAyuda(atributoMapeo = "varModifDescripcion", nombre = GrupoVariableModificadoraDescripcion.class,vistas=VistaTarifaVarModificadoraPrima.class)
	@NotNull(groups=TarifaAutoModifChecks.class, message="{com.afirme.midas2.requerido}")
	@Valid
	public VarModifDescripcion getVarModifDescripcion() {
		return varModifDescripcion;
	}

	public void setVarModifDescripcion(VarModifDescripcion varModifDescripcion) {
		this.varModifDescripcion = varModifDescripcion;
	}
	
	
	@DynamicControl(atributoMapeo="valor", tipoControl = TipoControl.TEXTBOX,etiqueta="Valor",editable=true,esNumerico=true,secuencia="7",
			vistas=VistaTarifaVarModificadoraPrima.class,longitudMaxima ="17", tipoValidacion = TipoValidacion.NUM_DECIMAL)
	@Column(name="VALOR", nullable = false, precision = 12, scale = 4)
	@NotNull(groups=TarifaAutoModifChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer = 12, fraction= 4,groups=TarifaAutoModifChecks.class,message="{javax.validation.constraints.Digits.message}")
	public BigDecimal getValor() {
		return valor;
	}
	
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TarifaAutoModifDesc other = (TarifaAutoModifDesc) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
