package mx.com.afirme.midas2.domain.tarifa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TarifaConceptoId implements Serializable {

	
	private static final long serialVersionUID = -9119406103855318245L;
	private Long idRiesgo;
	private Long idConcepto;
	
	@Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)
	public Long getIdRiesgo() {
		return idRiesgo;
	}
	public void setIdRiesgo(Long idRiesgo) {
		this.idRiesgo = idRiesgo;
	}
	
	@Column(name = "IDCONCEPTO", nullable = false, precision = 4, scale = 0)
	public Long getIdConcepto() {
		return idConcepto;
	}
	public void setIdConcepto(Long idConcepto) {
		this.idConcepto = idConcepto;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idConcepto == null) ? 0 : idConcepto.hashCode());
		result = prime * result
				+ ((idRiesgo == null) ? 0 : idRiesgo.hashCode());
		return result;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TarifaConceptoId other = (TarifaConceptoId) obj;
		if (idConcepto == null) {
			if (other.idConcepto != null)
				return false;
		} else if (!idConcepto.equals(other.idConcepto))
			return false;
		if (idRiesgo == null) {
			if (other.idRiesgo != null)
				return false;
		} else if (!idRiesgo.equals(other.idRiesgo))
			return false;
		return true;
	}
	
	
	public TarifaConceptoId(Long idRiesgo, Long idConcepto) {
		this.idRiesgo = idRiesgo;
		this.idConcepto = idConcepto;
	}
	
	public TarifaConceptoId() {
		
	}
	
	
	
	
	
	
	
	
}
