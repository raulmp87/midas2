package mx.com.afirme.midas2.domain.tarifa;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.domain.catalogos.VarModifPrima;
import mx.com.afirme.midas2.dto.CatalogoAyuda;
import mx.com.afirme.midas2.dto.DynamicControl;
import mx.com.afirme.midas2.dto.DynamicControl.TipoControl;
import mx.com.afirme.midas2.dto.DynamicControl.TipoValidacion;
import mx.com.afirme.midas2.dto.IdDynamicRow;
import mx.com.afirme.midas2.dto.componente.grupo.GrupoVariableModificadoraPrima;
import mx.com.afirme.midas2.dto.componente.vista.VistaTarifaVarModificadoraPrima;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
import mx.com.afirme.midas2.validator.group.NewItemChecks;
import mx.com.afirme.midas2.validator.group.TarifaAutoModifChecks;
/**
 * Catalogo de TOTARIFAAUTOMODIFPRIMA
 * @author vmhersil
 *
 */
@Entity
@Table(name = "TOTARIFAAUTOMODIFPRIMA", schema = "MIDAS")
public class TarifaAutoModifPrima  implements Serializable{
	private static final long serialVersionUID = -6862258901081077048L;
	
	public static final short ESTATUS_CREADO = 0;
	public static final short ESTATUS_ACTIVO = 1;
	public static final short ESTATUS_INACTIVO = 2;
	public static final short ESTATUS_BORRADO = 3;
	
	private TarifaAutoModifPrimaId id;
	
	private VarModifPrima varModifPrima;
	
	private BigDecimal valor;
	
	private Long idExterno = 0L;
	
	private Long idVerExterno = 0L;
	
	@IdDynamicRow
	@EmbeddedId
    public TarifaAutoModifPrimaId getId() {
		return id;
	}
	
	public void setId(TarifaAutoModifPrimaId id) {
		this.id = id;
	}
	
	@MapsId("variableModifPrimaId")
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCVARMODIFPRIMA", referencedColumnName = "IDTCVARMODIFPRIMA", nullable = false)
	@NotNull(groups=TarifaAutoModifChecks.class, message="{com.afirme.midas2.requerido}")
	@CatalogoAyuda(atributoMapeo = "varModifPrima", nombre = GrupoVariableModificadoraPrima.class,vistas=VistaTarifaVarModificadoraPrima.class)
	@Valid
	public VarModifPrima getVarModifPrima() {
		return varModifPrima;
	}

	public void setVarModifPrima(VarModifPrima varModifPrima) {
		this.varModifPrima = varModifPrima;
	}

	@DynamicControl(atributoMapeo="valor", tipoControl = TipoControl.TEXTBOX,etiqueta="Valor",
			editable=true,esNumerico=true,secuencia="8",vistas=VistaTarifaVarModificadoraPrima.class, 
			longitudMaxima ="17", tipoValidacion = TipoValidacion.NUM_DECIMAL)
	@Column(name="VALOR",nullable=false,precision=12,scale=4)
	@NotNull(groups={EditItemChecks.class, TarifaAutoModifChecks.class}, message="{com.afirme.midas2.requerido}")
	@Digits(integer = 12, fraction= 4,groups={EditItemChecks.class, TarifaAutoModifChecks.class},message="{javax.validation.constraints.Digits.message}")
	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
	@Column(name="IDEXTERNO",nullable=true,precision=22)
	public Long getIdExterno() {
		return idExterno;
	}

	public void setIdExterno(Long idExterno) {
		this.idExterno = idExterno;
	}
	
	@Column(name="IDVEREXTERNO",nullable=true,precision=22)
	public Long getIdVerExterno() {
		return idVerExterno;
	}

	public void setIdVerExterno(Long idVerExterno) {
		this.idVerExterno = idVerExterno;
	}
	
	/**
	 * Se sobreescribe el hashcode que es utilizado internamente por el m�todo equals
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result+ ((id == null) ? 0 : id.hashCode());
		return result;
	}
	/**
	 * Se sobreescribe el metodo equals para las comparaciones u ordenamiento,
	 * este metodo llama implicitamente el m�todo hashCode, por eso se sobreescribe.
	 */
	@Override
	public boolean equals(Object obj){
		if(this==obj){
			return true;
		}
		if(obj==null){
			return false;
		}
		if(getClass()!=obj.getClass()){
			return false;
		}
		TarifaAutoModifPrima other=(TarifaAutoModifPrima)obj;
		if(id==null){
			if(other.id!=null){
				return false;
			}
		}else if(!id.equals(other.id)){
			return false;
		}
		return true;
	}
	
	public TarifaAutoModifPrima(){
		
	}
}
