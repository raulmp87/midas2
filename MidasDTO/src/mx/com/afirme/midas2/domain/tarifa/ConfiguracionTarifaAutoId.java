package mx.com.afirme.midas2.domain.tarifa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ConfiguracionTarifaAutoId implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idToRiesgo;
	private Long idConcepto;
	private Long idDato;
	
	@Column(name = "IDTORIESGO", nullable = false, precision = 38, scale = 0)
	public Long getIdToRiesgo() {
		return idToRiesgo;
	}
	public void setIdToRiesgo(Long idToRiesgo) {
		this.idToRiesgo = idToRiesgo;
	}
	
	@Column(name = "IDCONCEPTO", nullable = false, precision = 38, scale = 0)
	public Long getIdConcepto() {
		return idConcepto;
	}
	public void setIdConcepto(Long idConcepto) {
		this.idConcepto = idConcepto;
	}
	
	@Column(name = "IDDATO", nullable = false, precision = 38, scale = 0)
	public Long getIdDato() {
		return idDato;
	}
	public void setIdDato(Long idDato) {
		this.idDato = idDato;
	}
	
	
}
