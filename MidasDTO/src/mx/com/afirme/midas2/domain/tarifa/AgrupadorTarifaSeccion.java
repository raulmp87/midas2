package mx.com.afirme.midas2.domain.tarifa;

import java.io.Serializable;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * Tragrupadortarifaseccion entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TRAGRUPADORTARIFASECCION", schema = "MIDAS")
public class AgrupadorTarifaSeccion implements Serializable, Entidad {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -8537188642991111653L;
	private AgrupadorTarifaSeccionId id = new AgrupadorTarifaSeccionId();
	private Short claveDefault;
	private MonedaDTO monedaDTO;
	private SeccionDTO seccionDTO;
	private AgrupadorTarifa agrupadorTarifa;
	private List<AgrupadorTarifa> agrupadorVersion;
	
	private static final Short NO = new Short("0");
	private static final Short SI = new Short("1");

	// Constructors

	/** default constructor */
	public AgrupadorTarifaSeccion() {
	}

	/** full constructor */
	public AgrupadorTarifaSeccion(AgrupadorTarifaSeccionId id,
			Short claveDefault) {
		this.id = id;
		this.claveDefault = claveDefault;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToSeccion", column = @Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idMoneda", column = @Column(name = "IDMONEDA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToAgrupadorTarifa", column = @Column(name = "IDTOAGRUPADORTARIFA", nullable = false, precision = 22, scale = 0)) })
	@Valid
	public AgrupadorTarifaSeccionId getId() {
		return this.id;
	}

	public void setId(AgrupadorTarifaSeccionId id) {
		this.id = id;
	}

	@Column(name = "claveDefault", nullable = false, precision = 4, scale = 0)
	public Short getClaveDefault() {
		return this.claveDefault;
	}

	public void setClaveDefault(Short claveDefault) {
		this.claveDefault = claveDefault;
	}
	
	@Transient
	public boolean getClaveDefaultBoolean(){
		return claveDefault == SI;
	}

	public void setClaveDefaultBoolean(boolean claveDefault){
		this.claveDefault = claveDefault?SI:NO;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDMONEDA", referencedColumnName="IDTCMONEDA", nullable = false, insertable = false, updatable = false)
	public MonedaDTO getMonedaDTO() {
		return monedaDTO;
	}

	public void setMonedaDTO(MonedaDTO monedaDTO) {
		this.monedaDTO = monedaDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOSECCION", referencedColumnName="IDTOSECCION", nullable = false, insertable = false, updatable = false)
	public SeccionDTO getSeccionDTO() {
		return seccionDTO;
	}

	public void setSeccionDTO(SeccionDTO seccionDTO) {
		this.seccionDTO = seccionDTO;
	}
	
	@Transient
	public AgrupadorTarifa getAgrupadorTarifa(){
		return agrupadorTarifa;
	}
	
	public void setAgrupadorTarifa(AgrupadorTarifa agrupadorTarifa){
		this.agrupadorTarifa = agrupadorTarifa;
	}

	@SuppressWarnings("unchecked")
	@Override
	public AgrupadorTarifaSeccionId getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		return this.claveDefault.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getBusinessKey() {
		return this.id.toString();
	}

	/**
	 * @return the agrupadorVersion
	 */
	@Transient
	public List<AgrupadorTarifa> getAgrupadorVersion() {
		return agrupadorVersion;
	}

	/**
	 * @param agrupadorVersion the agrupadorVersion to set
	 */
	public void setAgrupadorVersion(List<AgrupadorTarifa> agrupadorVersion) {
		this.agrupadorVersion = agrupadorVersion;
	}

}