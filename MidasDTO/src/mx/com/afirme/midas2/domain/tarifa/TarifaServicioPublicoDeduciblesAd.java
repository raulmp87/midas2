package mx.com.afirme.midas2.domain.tarifa;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.vigencia.VigenciaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducibles.NegocioDeducibleCob;

@Entity
@Table(name = "TOTARIFASERVICIOPUBLICODE", schema = "MIDAS")
public class TarifaServicioPublicoDeduciblesAd implements Serializable, Entidad{
	private static final long serialVersionUID = 1L;
	private BigDecimal id;
	private NegocioPaqueteSeccion negocioPaqueteSeccion;
	private MonedaDTO monedaDTO;
	private EstadoDTO estadoDTO;
	private CiudadDTO ciudadDTO;
	private CoberturaDTO coberturaDTO;
	private VigenciaDTO vigenciaDTO;
	private NegocioDeducibleCob negocioDeducibleCob;
	private Double valorDeducible1;
	private Double valorDeducible2;
	private Double valorDeducible3;
	private Double valorDeducible4;
	private Double valorDeducible5;
	private Double valorDeducible6;
	private Double valorSumaAsegurada;
	private String valorSumaAseguradaStr;
	private Short claveSumaAsegurada; //0 default, 1 minima, 2 maxima
	
	public static enum ClaveSumaAsegurada{
		Default((short)0),Minima((short)1),Maxima((short)2);
		
		private short valor;
		
		ClaveSumaAsegurada(short valor){
			this.valor = valor;
		}
		
		public short getValor(){return valor;}
	};


	// Constructors

	/** default constructor */
	public TarifaServicioPublicoDeduciblesAd() {
	}

	// Property accessors
	@Id
	@SequenceGenerator(name="TAF_SEV_PUB_DE_GEN", sequenceName="IDTOTARIFASERVICIOPUBDE_SEQ", allocationSize=1, schema="MIDAS")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAF_SEV_PUB_DE_GEN")
	@Column(name = "id", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTONEGPAQUETESECCION", nullable = false)
	public NegocioPaqueteSeccion getNegocioPaqueteSeccion() {
		return this.negocioPaqueteSeccion;
	}

	public void setNegocioPaqueteSeccion(
			NegocioPaqueteSeccion negocioPaqueteSeccion) {
		this.negocioPaqueteSeccion = negocioPaqueteSeccion;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idMoneda", referencedColumnName="IDTCMONEDA", nullable = false)
	public MonedaDTO getMonedaDTO() {
		return monedaDTO;
	}

	public void setMonedaDTO(MonedaDTO monedaDTO) {
		this.monedaDTO = monedaDTO;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idTcEstado", referencedColumnName="STATE_ID", nullable = true)
	public EstadoDTO getEstadoDTO() {
		return estadoDTO;
	}

	public void setEstadoDTO(EstadoDTO estadoDTO) {
		this.estadoDTO = estadoDTO;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idTcCiudad", referencedColumnName="CITY_ID", nullable = true)
	public CiudadDTO getCiudadDTO() {
		return ciudadDTO;
	}

	public void setCiudadDTO(CiudadDTO ciudadDTO) {
		this.ciudadDTO = ciudadDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOCOBERTURA", nullable = false)
	public CoberturaDTO getCoberturaDTO() {
		return this.coberturaDTO;
	}

	public void setCoberturaDTO(CoberturaDTO coberturaDTO) {
		this.coberturaDTO = coberturaDTO;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCVIGENCIA", nullable = false)
	public VigenciaDTO getVigenciaDTO() {
		return vigenciaDTO;
	}

	public void setVigenciaDTO(VigenciaDTO vigenciaDTO) {
		this.vigenciaDTO = vigenciaDTO;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idDeducible", referencedColumnName="ID", nullable = true)
	public NegocioDeducibleCob getNegocioDeducibleCob() {
		return negocioDeducibleCob;
	}

	public void setNegocioDeducibleCob(NegocioDeducibleCob negocioDeducibleCob) {
		this.negocioDeducibleCob = negocioDeducibleCob;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	@Column(name = "valorDeducible1", nullable = false, precision = 16)
	public Double getValorDeducible1() {
		return valorDeducible1;
	}

	public void setValorDeducible1(Double valorDeducible1) {
		this.valorDeducible1 = valorDeducible1;
	}

	@Column(name = "valorDeducible2", nullable = false, precision = 16)
	public Double getValorDeducible2() {
		return valorDeducible2;
	}

	public void setValorDeducible2(Double valorDeducible2) {
		this.valorDeducible2 = valorDeducible2;
	}

	@Column(name = "valorDeducible3", nullable = false, precision = 16)
	public Double getValorDeducible3() {
		return valorDeducible3;
	}

	public void setValorDeducible3(Double valorDeducible3) {
		this.valorDeducible3 = valorDeducible3;
	}
	
	@Column(name = "valorDeducible4", nullable = false, precision = 16)
	public Double getValorDeducible4() {
		return valorDeducible4;
	}

	public void setValorDeducible4(Double valorDeducible4) {
		this.valorDeducible4 = valorDeducible4;
	}
	
	@Column(name = "valorDeducible5", nullable = false, precision = 16)
	public Double getValorDeducible5() {
		return valorDeducible5;
	}

	public void setValorDeducible5(Double valorDeducible5) {
		this.valorDeducible5 = valorDeducible5;
	}
	
	@Column(name = "valorDeducible6", nullable = false, precision = 16)
	public Double getValorDeducible6() {
		return valorDeducible6;
	}

	public void setValorDeducible6(Double valorDeducible6) {
		this.valorDeducible6 = valorDeducible6;
	}
	
	@Transient
	public Double getValorSumaAsegurada() {
		return valorSumaAsegurada;
	}

	public void setValorSumaAsegurada(Double valorSumaAsegurada) {
		this.valorSumaAsegurada = valorSumaAsegurada;
	}

	@Transient
	public String getValorSumaAseguradaStr() {
		return valorSumaAseguradaStr;
	}

	public void setValorSumaAseguradaStr(String valorSumaAseguradaStr) {
		this.valorSumaAseguradaStr = valorSumaAseguradaStr;
	}

	@Column(name="CLAVESUMAASEGURADA", nullable=false, precision=4, scale=0)
	public Short getClaveSumaAsegurada() {
		return claveSumaAsegurada;
	}

	public void setClaveSumaAsegurada(Short claveSumaAsegurada) {
		this.claveSumaAsegurada = claveSumaAsegurada;
	}	
}
