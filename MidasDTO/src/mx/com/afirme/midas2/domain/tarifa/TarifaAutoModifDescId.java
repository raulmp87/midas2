package mx.com.afirme.midas2.domain.tarifa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dto.DynamicControl;
import mx.com.afirme.midas2.dto.DynamicControl.TipoControl;
import mx.com.afirme.midas2.dto.RegistroDinamicoDTO;
import mx.com.afirme.midas2.dto.componente.vista.VistaTarifaVarModificadoraPrima;
import mx.com.afirme.midas2.util.UtileriasWeb;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

@Embeddable
public class TarifaAutoModifDescId implements Serializable {

	private static final long serialVersionUID = 1167405858667248788L;
	
	private Long idMoneda;
	private Long idRiesgo;
	private Long idConcepto;
	private Long version;
	private Long varModifDescripcionId;
	
	public TarifaAutoModifDescId(){
		
	}
	
	public TarifaAutoModifDescId(String id){
		String[] id_arr = id.split(UtileriasWeb.SEPARADOR);
		//Revisar que la longitud del arreglo sea la correcta
		if(id_arr.length == 6){
			//Revisar que el id pertenezca al bean correcto
			if(id_arr[0].equals("T")){
				idMoneda = Long.parseLong(id_arr[1]);
				idRiesgo = Long.parseLong(id_arr[2]);
				idConcepto = Long.parseLong(id_arr[3]);
				version = Long.parseLong(id_arr[4]);
				varModifDescripcionId = Long.parseLong(id_arr[5]);
			}
		}
	}
	
	public TarifaAutoModifDescId(TarifaVersionId tarifaVersionId){
		this.idMoneda = tarifaVersionId.getIdMoneda();
		this.idRiesgo = tarifaVersionId.getIdRiesgo();
		this.idConcepto = tarifaVersionId.getIdConcepto();
		this.version = tarifaVersionId.getVersion();
	}

	public TarifaAutoModifDescId(Long idLineaNegocio, Long idMoneda, 
			Long idToRiesgo, Long idConcepto, Long idVerTarifa, 
			Long varModifDescripcionId) {
		this.idMoneda = idMoneda;
		this.idRiesgo = idToRiesgo;
		this.idConcepto = idConcepto;
		this.version = idVerTarifa;
		this.varModifDescripcionId = varModifDescripcionId;
	}


	@DynamicControl(atributoMapeo="id.idMoneda", tipoControl = TipoControl.HIDDEN,etiqueta="Moneda",esComponenteId=true,editable=true,esNumerico=true,
			secuencia="2",vistas=VistaTarifaVarModificadoraPrima.class)
	@Column(name = "IDMONEDA", nullable = false, precision = 22, scale = 0)
	public Long getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}

	@DynamicControl(atributoMapeo="id.idRiesgo", tipoControl = TipoControl.HIDDEN,etiqueta="Riesgo",esComponenteId=true,editable=true,esNumerico=true,
			secuencia="3",vistas=VistaTarifaVarModificadoraPrima.class)
	@Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)
	public Long getIdRiesgo() {
		return idRiesgo;
	}

	public void setIdRiesgo(Long idRiesgo) {
		this.idRiesgo = idRiesgo;
	}

	@DynamicControl(atributoMapeo="id.idConcepto", tipoControl = TipoControl.HIDDEN,etiqueta="Concepto",esComponenteId=true,editable=true,esNumerico=true,
			secuencia="4",vistas=VistaTarifaVarModificadoraPrima.class)
	@Column(name = "IDCONCEPTO", nullable = false, precision = 4, scale = 0)
	public Long getIdConcepto() {
		return idConcepto;
	}

	public void setIdConcepto(Long idConcepto) {
		this.idConcepto = idConcepto;
	}

	@DynamicControl(atributoMapeo="id.version", tipoControl = TipoControl.HIDDEN,etiqueta="Versi\u00F3n",esComponenteId=true,editable=true,esNumerico=true,
			secuencia="5",vistas=VistaTarifaVarModificadoraPrima.class)
	@Column(name = "IDVERTARIFA", nullable = false, precision = 22, scale = 0)
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	@Column(name="IDTCVARMODIFDESCRIPCION", nullable=false,precision=22,scale=0)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer = 10, fraction= 0,groups=EditItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public Long getVarModifDescripcionId() {
		return varModifDescripcionId;
	}

	public void setVarModifDescripcionId(Long varModifDescripcionId) {
		this.varModifDescripcionId = varModifDescripcionId;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idConcepto == null) ? 0 : idConcepto.hashCode());
		result = prime * result
				+ ((idMoneda == null) ? 0 : idMoneda.hashCode());
		result = prime * result
				+ ((idRiesgo == null) ? 0 : idRiesgo.hashCode());
		result = prime
				* result
				+ ((varModifDescripcionId == null) ? 0 : varModifDescripcionId
						.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TarifaAutoModifDescId other = (TarifaAutoModifDescId) obj;
		if (idConcepto == null) {
			if (other.idConcepto != null)
				return false;
		} else if (!idConcepto.equals(other.idConcepto))
			return false;
		if (idMoneda == null) {
			if (other.idMoneda != null)
				return false;
		} else if (!idMoneda.equals(other.idMoneda))
			return false;
		if (idRiesgo == null) {
			if (other.idRiesgo != null)
				return false;
		} else if (!idRiesgo.equals(other.idRiesgo))
			return false;
		if (varModifDescripcionId == null) {
			if (other.varModifDescripcionId != null)
				return false;
		} else if (!varModifDescripcionId.equals(other.varModifDescripcionId))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	@Override
	public String toString(){
		String sep = UtileriasWeb.SEPARADOR;
		StringBuilder toRet = new StringBuilder();
		toRet.append(RegistroDinamicoDTO.TARIFA).append(sep)
			 .append(idMoneda.toString()).append(sep)
			 .append(idRiesgo.toString()).append(sep)
			 .append(idConcepto.toString()).append(sep)
			 .append(version.toString()).append(sep)
			 .append(varModifDescripcionId.toString());
		
		return toRet.toString();
	}

}
