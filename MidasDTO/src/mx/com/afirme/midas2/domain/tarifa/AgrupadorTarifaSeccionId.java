package mx.com.afirme.midas2.domain.tarifa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.util.UtileriasWeb;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

/**
 * TragrupadortarifaseccionId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class AgrupadorTarifaSeccionId implements Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 6556052576102340469L;
	
	private Long idToSeccion;
	private Long idMoneda;
	private Long idToAgrupadorTarifa;
	

	// Constructors

	/** default constructor */
	public AgrupadorTarifaSeccionId() {
	}

	/** full constructor */
	public AgrupadorTarifaSeccionId(Long idToSeccion,
			Long idMoneda, Long idToAgrupadorTarifa) {
		this.idToSeccion = idToSeccion;
		this.idMoneda = idMoneda;
		this.idToAgrupadorTarifa = idToAgrupadorTarifa;
	}
	
	public AgrupadorTarifaSeccionId(String id){
		String arr[] = id.split(UtileriasWeb.SEPARADOR);
		try{
			idToSeccion = Long.parseLong(arr[0]);
			idMoneda = Long.parseLong(arr[1]);
			idToAgrupadorTarifa = Long.parseLong(arr[2]);
		}catch(Exception e){
			
		}
	}

	// Property accessors

	@Column(name = "idToSeccion", nullable = false, precision = 22, scale = 0)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getIdToSeccion() {
		return this.idToSeccion;
	}

	public void setIdToSeccion(Long idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	@Column(name = "idMoneda", nullable = false, precision = 22, scale = 0)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getIdMoneda() {
		return this.idMoneda;
	}

	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}

	@Column(name = "idToAgrupadorTarifa", nullable = false, precision = 22, scale = 0)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getIdToAgrupadorTarifa() {
		return this.idToAgrupadorTarifa;
	}

	public void setIdToAgrupadorTarifa(Long idToAgrupadorTarifa) {
		this.idToAgrupadorTarifa = idToAgrupadorTarifa;
	}
	

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof AgrupadorTarifaSeccionId))
			return false;
		AgrupadorTarifaSeccionId castOther = (AgrupadorTarifaSeccionId) other;

		return ((this.getIdToSeccion() == castOther.getIdToSeccion()) || (this
				.getIdToSeccion() != null
				&& castOther.getIdToSeccion() != null && this.getIdToSeccion()
				.equals(castOther.getIdToSeccion())))
				&& ((this.getIdMoneda() == castOther.getIdMoneda()) || (this
						.getIdMoneda() != null
						&& castOther.getIdMoneda() != null && this
						.getIdMoneda().equals(castOther.getIdMoneda())))
				&& ((this.getIdToAgrupadorTarifa() == castOther
						.getIdToAgrupadorTarifa()) || (this
						.getIdToAgrupadorTarifa() != null
						&& castOther.getIdToAgrupadorTarifa() != null && this
						.getIdToAgrupadorTarifa().equals(
								castOther.getIdToAgrupadorTarifa())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToSeccion() == null ? 0 : this.getIdToSeccion()
						.hashCode());
		result = 37 * result
				+ (getIdMoneda() == null ? 0 : this.getIdMoneda().hashCode());
		result = 37
				* result
				+ (getIdToAgrupadorTarifa() == null ? 0 : this
						.getIdToAgrupadorTarifa().hashCode());
		return result;
	}
	
	@Override
	public String toString(){
		return idToSeccion + UtileriasWeb.SEPARADOR + idMoneda + UtileriasWeb.SEPARADOR + idToAgrupadorTarifa;
	}

}