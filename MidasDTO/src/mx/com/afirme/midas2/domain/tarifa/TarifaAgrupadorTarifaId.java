package mx.com.afirme.midas2.domain.tarifa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;

/**
 * TrtarifaagrupadortarifaId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class TarifaAgrupadorTarifaId implements Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -6477003764438414961L;
	private Long idToAgrupadorTarifa;
	private Long idVerAgrupadorTarifa;
	private Long idLineanegocio;
	private Long idMoneda;
	private Long idToRiesgo;
	private Short idConcepto;
	private Long idVertarifa;

	// Constructors

	/** default constructor */
	public TarifaAgrupadorTarifaId() {
	}

	/** full constructor */
	public TarifaAgrupadorTarifaId(Long idToAgrupadorTarifa,
			Long idVerAgrupadorTarifa, Long idLineanegocio,
			Long idMoneda, Long idToRiesgo, Short idConcepto,
			Long idVertarifa) {
		this.idToAgrupadorTarifa = idToAgrupadorTarifa;
		this.idVerAgrupadorTarifa = idVerAgrupadorTarifa;
		this.idLineanegocio = idLineanegocio;
		this.idMoneda = idMoneda;
		this.idToRiesgo = idToRiesgo;
		this.idConcepto = idConcepto;
		this.idVertarifa = idVertarifa;
	}

	// Property accessors

	@Column(name = "idToAgrupadorTarifa", nullable = false, precision = 22, scale = 0)
	public Long getIdToAgrupadorTarifa() {
		return this.idToAgrupadorTarifa;
	}

	public void setIdToAgrupadorTarifa(Long idToAgrupadorTarifa) {
		this.idToAgrupadorTarifa = idToAgrupadorTarifa;
	}

	@Column(name = "idVerAgrupadorTarifa", nullable = false, precision = 22, scale = 0)
	public Long getIdVerAgrupadorTarifa() {
		return this.idVerAgrupadorTarifa;
	}

	public void setIdVerAgrupadorTarifa(Long idVerAgrupadorTarifa) {
		this.idVerAgrupadorTarifa = idVerAgrupadorTarifa;
	}

	//@Column(name = "idLineanegocio", nullable = false, precision = 22, scale = 0)
	@Transient
	public Long getIdLineanegocio() {
		return this.idLineanegocio;
	}

	public void setIdLineanegocio(Long idLineanegocio) {
		this.idLineanegocio = idLineanegocio;
	}

	@Column(name = "idMoneda", nullable = false, precision = 22, scale = 0)
	public Long getIdMoneda() {
		return this.idMoneda;
	}

	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}

	@Column(name = "idToRiesgo", nullable = false, precision = 22, scale = 0)
	public Long getIdToRiesgo() {
		return this.idToRiesgo;
	}

	public void setIdToRiesgo(Long idToRiesgo) {
		this.idToRiesgo = idToRiesgo;
	}

	@Column(name = "idConcepto", nullable = false, precision = 4, scale = 0)
	public Short getIdConcepto() {
		return this.idConcepto;
	}

	public void setIdConcepto(Short idConcepto) {
		this.idConcepto = idConcepto;
	}

	@Column(name = "idVertarifa", nullable = false, precision = 22, scale = 0)
	public Long getIdVertarifa() {
		return this.idVertarifa;
	}

	public void setIdVertarifa(Long idVertarifa) {
		this.idVertarifa = idVertarifa;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof TarifaAgrupadorTarifaId))
			return false;
		TarifaAgrupadorTarifaId castOther = (TarifaAgrupadorTarifaId) other;

		return ((this.getIdToAgrupadorTarifa() == castOther
				.getIdToAgrupadorTarifa()) || (this.getIdToAgrupadorTarifa() != null
				&& castOther.getIdToAgrupadorTarifa() != null && this
				.getIdToAgrupadorTarifa().equals(
						castOther.getIdToAgrupadorTarifa())))
				&& ((this.getIdVerAgrupadorTarifa() == castOther
						.getIdVerAgrupadorTarifa()) || (this
						.getIdVerAgrupadorTarifa() != null
						&& castOther.getIdVerAgrupadorTarifa() != null && this
						.getIdVerAgrupadorTarifa().equals(
								castOther.getIdVerAgrupadorTarifa())))
				&& ((this.getIdLineanegocio() == castOther.getIdLineanegocio()) || (this
						.getIdLineanegocio() != null
						&& castOther.getIdLineanegocio() != null && this
						.getIdLineanegocio().equals(
								castOther.getIdLineanegocio())))
				&& ((this.getIdMoneda() == castOther.getIdMoneda()) || (this
						.getIdMoneda() != null
						&& castOther.getIdMoneda() != null && this
						.getIdMoneda().equals(castOther.getIdMoneda())))
				&& ((this.getIdToRiesgo() == castOther.getIdToRiesgo()) || (this
						.getIdToRiesgo() != null
						&& castOther.getIdToRiesgo() != null && this
						.getIdToRiesgo().equals(castOther.getIdToRiesgo())))
				&& ((this.getIdConcepto() == castOther.getIdConcepto()) || (this
						.getIdConcepto() != null
						&& castOther.getIdConcepto() != null && this
						.getIdConcepto().equals(castOther.getIdConcepto())))
				&& ((this.getIdVertarifa() == castOther.getIdVertarifa()) || (this
						.getIdVertarifa() != null
						&& castOther.getIdVertarifa() != null && this
						.getIdVertarifa().equals(castOther.getIdVertarifa())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToAgrupadorTarifa() == null ? 0 : this
						.getIdToAgrupadorTarifa().hashCode());
		result = 37
				* result
				+ (getIdVerAgrupadorTarifa() == null ? 0 : this
						.getIdVerAgrupadorTarifa().hashCode());
		result = 37
				* result
				+ (getIdLineanegocio() == null ? 0 : this.getIdLineanegocio()
						.hashCode());
		result = 37 * result
				+ (getIdMoneda() == null ? 0 : this.getIdMoneda().hashCode());
		result = 37
				* result
				+ (getIdToRiesgo() == null ? 0 : this.getIdToRiesgo()
						.hashCode());
		result = 37
				* result
				+ (getIdConcepto() == null ? 0 : this.getIdConcepto()
						.hashCode());
		result = 37
				* result
				+ (getIdVertarifa() == null ? 0 : this.getIdVertarifa()
						.hashCode());
		return result;
	}

}