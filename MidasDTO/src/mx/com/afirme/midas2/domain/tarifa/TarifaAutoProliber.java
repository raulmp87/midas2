package mx.com.afirme.midas2.domain.tarifa;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dto.DynamicControl;
import mx.com.afirme.midas2.dto.DynamicControl.TipoControl;
import mx.com.afirme.midas2.dto.DynamicControl.TipoValidacion;
import mx.com.afirme.midas2.dto.IdDynamicRow;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

@Entity
@Table(name = "TOTARIFAAUTOPROLIBER", schema = "MIDAS")
public class TarifaAutoProliber implements Serializable{
	
	private static final long serialVersionUID = 8687670576186498934L;
	
	private TarifaAutoProliberId id;
	private BigDecimal valorPrimaTotal;
	private BigDecimal valorPrimaTercero;

	//Atributos de Mapeo
	public static final String A_M_CODIGOTIPOUSOVEHICULO = "id.codigoTipoUsoVehiculo";
	public static final String A_M_CODIGOTIPOSERVICIOVEHICULO = "id.codigoTipoServicioVehiculo";
	public static final String A_M_CLAVEGRUPOPROLIBER = "id.claveGrupoProliber";
	public static final String A_M_TONELEDASCAPACMIN = "id.toneledasCapacMin";
	public static final String A_M_TONELEDASCAPACMAX = "id.toneledasCapacMax";
	public static final String A_M_NUMEROREMOLQUES = "id.numeroRemolques";
	public static final String A_M_IDTCTIPOVEHICULO = "id.idTcTipoVehiculo";
	public static final String A_M_VALORPRIMATERCERO =  "valorPrimaTercero";

	@IdDynamicRow
	@EmbeddedId
	@AttributeOverrides({
		@AttributeOverride(name="idMoneda", column = @Column(name = "IDMONEDA", nullable = false, precision = 22, scale = 0)),
		@AttributeOverride(name="idRiesgo", column = @Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)),
		@AttributeOverride(name="idConcepto", column = @Column(name = "IDCONCEPTO", nullable = false, precision = 4, scale = 0)),
		@AttributeOverride(name="version", column = @Column(name = "IDVERTARIFA", nullable = false, precision = 22, scale = 0)),
		@AttributeOverride(name="idTcTipoVehiculo", column = @Column(name = "IDTCTIPOVEHICULO", nullable = false, precision = 22, scale = 0)),
		@AttributeOverride(name="claveGrupoProliber", column = @Column(name = "CLAVEGRUPOPROLIBER", nullable = false, length = 5)),
		@AttributeOverride(name="toneledasCapacMin", column = @Column(name = "TONELADASCAPACMIN", nullable = false, precision = 8, scale = 2)),
		@AttributeOverride(name="toneledasCapacMax", column = @Column(name = "TONELADASCAPACMAX", nullable = false, precision = 8, scale = 2)),
		@AttributeOverride(name="numeroRemolques", column = @Column(name = "NUMEROREMOLQUES", nullable = false, precision = 22, scale = 0)),
		@AttributeOverride(name="codigoTipoUsoVehiculo", column = @Column(name = "CODIGOTIPOUSOVEHICULO", nullable = false, length = 5)),
		@AttributeOverride(name="codigoTipoServicioVehiculo", column = @Column(name = "CODIGOTIPOSERVICIOVEHICULO", nullable = false, length = 5))
	})
	@Valid
	public TarifaAutoProliberId getId() {
		return id;
	}
	
	public void setId(TarifaAutoProliberId id) {
		this.id = id;
	}
	
	@DynamicControl(atributoMapeo="valorPrimaTotal", tipoControl=TipoControl.TEXTBOX, etiqueta="Prima Proliber", editable=true, esNumerico=true,
			secuencia="13", longitudMaxima ="17", tipoValidacion = TipoValidacion.NUM_DECIMAL)
	@Column(name = "VALORPRIMATOTAL", nullable = false, precision = 16, scale = 2)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	/*@Digits(integer = 16, fraction= 4,groups=NewItemChecks.class,message="{javax.validation.constraints.Digits.message}")*/
	@Digits(integer = 14, fraction= 2,groups=EditItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public BigDecimal getValorPrimaTotal() {
		return valorPrimaTotal;
	}
	
	public void setValorPrimaTotal(BigDecimal valorPrimaTotal) {
		this.valorPrimaTotal = valorPrimaTotal;
	}
	
	@DynamicControl(atributoMapeo="valorPrimaTercero", tipoControl=TipoControl.TEXTBOX, etiqueta="Prima Tercero", editable=true, esNumerico=true,
			secuencia="14", longitudMaxima ="17", tipoValidacion = TipoValidacion.NUM_DECIMAL)
	@Column(name = "VALORPRIMATERCERO", nullable = false, precision = 16, scale = 2)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	/*@Digits(integer = 16, fraction= 2,groups=NewItemChecks.class,message="{javax.validation.constraints.Digits.message}")*/
	@Digits(integer = 14, fraction= 2,groups=EditItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public BigDecimal getValorPrimaTercero() {
		return valorPrimaTercero;
	}
	
	public void setValorPrimaTercero(BigDecimal valorPrimaTercero) {
		this.valorPrimaTercero = valorPrimaTercero;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TarifaAutoProliber other = (TarifaAutoProliber) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
