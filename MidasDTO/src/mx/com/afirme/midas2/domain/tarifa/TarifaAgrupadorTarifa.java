package mx.com.afirme.midas2.domain.tarifa;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * Trtarifaagrupadortarifa entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TRTARIFAAGRUPADORTARIFA", schema = "MIDAS")
public class TarifaAgrupadorTarifa implements Serializable, Entidad {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -9204828547280286880L;
	private TarifaAgrupadorTarifaId id;
	private String idToAgrupadorTarifaFromString;

	// Constructors

	/** default constructor */
	public TarifaAgrupadorTarifa() {
	}

	/** full constructor */
	public TarifaAgrupadorTarifa(TarifaAgrupadorTarifaId id) {
		this.id = id;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToAgrupadorTarifa", column = @Column(name = "IDTOAGRUPADORTARIFA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idVerAgrupadorTarifa", column = @Column(name = "IDVERAGRUPADORTARIFA", nullable = false, precision = 22, scale = 0)),
			//@AttributeOverride(name = "idLineanegocio", column = @Column(name = "IDLINEANEGOCIO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idMoneda", column = @Column(name = "IDMONEDA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idToRiesgo", column = @Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idConcepto", column = @Column(name = "IDCONCEPTO", nullable = false, precision = 4, scale = 0)),
			@AttributeOverride(name = "idVertarifa", column = @Column(name = "IDVERTARIFA", nullable = false, precision = 22, scale = 0)) })
	public TarifaAgrupadorTarifaId getId() {
		return this.id;
	}

	public void setId(TarifaAgrupadorTarifaId id) {
		this.id = id;
	}
    
	@Transient
	public String getIdToAgrupadorTarifaFromString() {
		return idToAgrupadorTarifaFromString;
	}

	public void setIdToAgrupadorTarifaFromString(
			String idToAgrupadorTarifaFromString) {
		this.idToAgrupadorTarifaFromString = idToAgrupadorTarifaFromString;
	}

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}