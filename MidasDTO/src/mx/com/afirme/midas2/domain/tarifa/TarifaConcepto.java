package mx.com.afirme.midas2.domain.tarifa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "TCCONCEPTOTARIFA", schema = "MIDAS")
public class TarifaConcepto implements Serializable {
	private static final long serialVersionUID = -3645850039368753368L;
	
	public static final short TARIFA_ESTANDAR= 1;
	public static final short TARIFA_POR_RANGO_SA = 2;
	public static final short TARIFA_POR_SERVICIOS_TERCEROS = 3;
	public static final short TARIFA_POR_PRIMA = 4;
	public static final short TARIFA_POR_DESCRIPCION = 5;
	
	private TarifaConceptoId id;
	private String descripcion;
	private Long secuencia;
	private String claveNegocio;
	private Short tipoTarifa;
	private List<TarifaVersion> tarifaVersiones = new ArrayList<TarifaVersion>();
	
	
	@EmbeddedId
    @AttributeOverrides( {
	    @AttributeOverride(name = "idRiesgo", column = @Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)),
	    @AttributeOverride(name = "idConcepto", column = @Column(name = "IDCONCEPTO", nullable = false, precision = 4, scale = 0)) })
	public TarifaConceptoId getId() {
		return id;
	}
	
	public void setId(TarifaConceptoId id) {
		this.id = id;
	}
	
	@Column(name = "DESCRIPCIONCONCEPTO", nullable = false, length = 200)
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Column(name = "IDSECUENCIA", nullable = false, precision = 22, scale = 0)
	public Long getSecuencia() {
		return secuencia;
	}
	
	public void setSecuencia(Long secuencia) {
		this.secuencia = secuencia;
	}
	
	@Column(name = "CLAVENEGOCIO", nullable = false)
	public String getClaveNegocio() {
		return claveNegocio;
	}
	
	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}

	@Column(name = "CLAVETIPOTARIFA", nullable = false, precision = 4, scale = 0)
	public Short getTipoTarifa() {
		return tipoTarifa;
	}

	public void setTipoTarifa(Short tipoTarifa) {
		this.tipoTarifa = tipoTarifa;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tarifaConcepto", orphanRemoval=true)
	public List<TarifaVersion> getTarifaVersiones() {
		return tarifaVersiones;
	}

	public void setTarifaVersiones(List<TarifaVersion> tarifaVersiones) {
		this.tarifaVersiones = tarifaVersiones;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TarifaConcepto other = (TarifaConcepto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public TarifaConcepto() {
		
	}


	
}
