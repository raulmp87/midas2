package mx.com.afirme.midas2.domain.tarifa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dto.DynamicControl;
import mx.com.afirme.midas2.dto.RegistroDinamicoDTO;
import mx.com.afirme.midas2.dto.DynamicControl.TipoControl;
import mx.com.afirme.midas2.util.UtileriasWeb;
import mx.com.afirme.midas2.validator.group.NewItemChecks;

@Embeddable
public class TarifaAutoId implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long idMoneda;
	private Long idToRiesgo;
	private Long idConcepto;
	private Long idVerTarifa;
	private Long idBase1;
	private Long idBase2;
	private Long idBase3;
	private Long idBase4;
	
	public TarifaAutoId(){
		
	}
	
	public TarifaAutoId(String id){
		String [] arrayClave = TarifaAuto.validarClave(id);
		if(arrayClave == null || arrayClave.length < 9 || !arrayClave[0].equals("T")){
			throw new RuntimeException("clave mal formada: "+id);
		}
		Long idMoneda = new Long(arrayClave[1]);
		Long idRiesgo = new Long(arrayClave[2]);
		Long idConcepto = new Long(arrayClave[3]);
		Long version = new Long(arrayClave[4]);
		Long idBase1 = new Long(arrayClave[5]);
		Long idBase2 = new Long(arrayClave[6]);
		Long idBase3 = new Long(arrayClave[7]);
		Long idBase4 = new Long(arrayClave[8]);
		
		setIdMoneda(idMoneda);
		setIdToRiesgo(idRiesgo);
		setIdConcepto(idConcepto);
		setIdVerTarifa(version);
		setIdBase1(idBase1);
		setIdBase2(idBase2);
		setIdBase3(idBase3);
		setIdBase4(idBase4);
	}
	
	public TarifaAutoId(TarifaVersionId tarifaVersionId){
		setIdMoneda(tarifaVersionId.getIdMoneda());
		setIdToRiesgo(tarifaVersionId.getIdRiesgo());
		setIdConcepto(tarifaVersionId.getIdConcepto());
		setIdVerTarifa(tarifaVersionId.getVersion());
		setIdBase1(0l);
		setIdBase2(0l);
		setIdBase3(0l);
		setIdBase4(0l);
	}
	
	
	@DynamicControl(atributoMapeo="id.idMoneda", tipoControl = TipoControl.HIDDEN,etiqueta="Moneda",editable=true,esComponenteId=true,secuencia="1")
	@Column(name = "IDMONEDA", nullable = false, precision = 38, scale = 0)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getIdMoneda() {
		return idMoneda;
	}
	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}
	
	@DynamicControl(atributoMapeo="id.idToRiesgo", tipoControl = TipoControl.HIDDEN,etiqueta="Riesgo",editable=true,esComponenteId=true,secuencia="2")
	@Column(name = "IDTORIESGO", nullable = false, precision = 38, scale = 0)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getIdToRiesgo() {
		return idToRiesgo;
	}
	public void setIdToRiesgo(Long idToRiesgo) {
		this.idToRiesgo = idToRiesgo;
	}
	
	@DynamicControl(atributoMapeo="id.idConcepto", tipoControl = TipoControl.HIDDEN,etiqueta="Concepto",editable=true,esComponenteId=true,secuencia="3")
	@Column(name = "IDCONCEPTO", nullable = false, precision = 4, scale = 0)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getIdConcepto() {
		return idConcepto;
	}
	public void setIdConcepto(Long idConcepto) {
		this.idConcepto = idConcepto;
	}
	
	@DynamicControl(atributoMapeo="id.idVerTarifa", tipoControl = TipoControl.HIDDEN,etiqueta="Versi\u00F3n",editable=true,esComponenteId=true,secuencia="4")
	@Column(name = "IDVERTARIFA", nullable = false, precision = 38, scale = 0)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getIdVerTarifa() {
		return idVerTarifa;
	}
	public void setIdVerTarifa(Long idVerTarifa) {
		this.idVerTarifa = idVerTarifa;
	}
	
	@Column(name = "IDBASE1", nullable = false, precision = 38, scale = 0)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer = 10, fraction= 0,groups=NewItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public Long getIdBase1() {
		return idBase1;
	}
	public void setIdBase1(Long idBase1) {
		this.idBase1 = idBase1;
	}
	
	@Column(name = "IDBASE2", nullable = false, precision = 38, scale = 0)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer = 10, fraction= 0,groups=NewItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public Long getIdBase2() {
		return idBase2;
	}
	public void setIdBase2(Long idBase2) {
		this.idBase2 = idBase2;
	}
	
	@Column(name = "IDBASE3", nullable = false, precision = 38, scale = 0)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer = 10, fraction= 0,groups=NewItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public Long getIdBase3() {
		return idBase3;
	}
	public void setIdBase3(Long idBase3) {
		this.idBase3 = idBase3;
	}
	
	@Column(name = "IDBASE4", nullable = false, precision = 38, scale = 0)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer = 10, fraction= 0,groups=NewItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public Long getIdBase4() {
		return idBase4;
	}
	public void setIdBase4(Long idBase4) {
		this.idBase4 = idBase4;
	}
	
	public String toString(){
		String sep = UtileriasWeb.SEPARADOR;
		StringBuilder toRet = new StringBuilder("");
		toRet.append(RegistroDinamicoDTO.TARIFA).append(sep)
			 .append(idMoneda.toString()).append(sep)
			 .append(idToRiesgo.toString()).append(sep)
			 .append(idConcepto.toString()).append(sep)
			 .append(idVerTarifa.toString()).append(sep)
			 .append(idBase1.toString()).append(sep)
			 .append(idBase2.toString()).append(sep)
			 .append(idBase3.toString()).append(sep)
			 .append(idBase4.toString()).append(sep);
		return toRet.toString();
	}
}
