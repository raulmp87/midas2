
package mx.com.afirme.midas2.domain.tarifa;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.util.UtileriasWeb;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

/**
 * AgrupadorTarifa entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOAGRUPADORTARIFA", schema = "MIDAS")
public class AgrupadorTarifa implements Entidad,java.io.Serializable,Comparable<AgrupadorTarifa> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields

	private AgrupadorTarifaId id;
	private String descripcionAgrupador;
	private String descripcionVersion;
	private Date fechaInicioVigencia;
	private Date fechaFinVigencia;
	private Short claveEstatus;
	private Date fechaCreacion;
	private String codigoUsuarioCreacion;
	private Date fechaActivacion;
	private String codigoUsuarioActivacion;
	private String claveNegocio;

	public static final short ESTATUS_CREADO = 0;
	public static final short ESTATUS_ACTIVO = 1;
	public static final short ESTATUS_NO_ACTIVO = 2;
	public static final short ESTATUS_BORRADO = 3;
	
	// Constructors

	/** default constructor */
	public AgrupadorTarifa() {
	}

	/** minimal constructor */
	public AgrupadorTarifa(AgrupadorTarifaId id, String descripcionAgrupador,
			String descripcionVersion, Date fechaInicioVigencia,
			Date fechaFinVigencia, Short claveEstatus, Date fechaCreacion,
			String codigoUsuarioCreacion, String claveNegocio) {
		this.id = id;
		this.descripcionAgrupador = descripcionAgrupador;
		this.descripcionVersion = descripcionVersion;
		this.fechaInicioVigencia = fechaInicioVigencia;
		this.fechaFinVigencia = fechaFinVigencia;
		this.claveEstatus = claveEstatus;
		this.fechaCreacion = fechaCreacion;
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
		this.claveNegocio = claveNegocio;
	}

	/** full constructor */
	public AgrupadorTarifa(AgrupadorTarifaId id, String descripcionAgrupador,
			String descripcionVersion, Date fechaInicioVigencia,
			Date fechaFinVigencia, Short claveEstatus, Date fechaCreacion,
			String codigoUsuarioCreacion, Date fechaActivacion,
			String codigoUsuarioActivacion, String claveNegocio) {
		this.id = id;
		this.descripcionAgrupador = descripcionAgrupador;
		this.descripcionVersion = descripcionVersion;
		this.fechaInicioVigencia = fechaInicioVigencia;
		this.fechaFinVigencia = fechaFinVigencia;
		this.claveEstatus = claveEstatus;
		this.fechaCreacion = fechaCreacion;
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
		this.fechaActivacion = fechaActivacion;
		this.codigoUsuarioActivacion = codigoUsuarioActivacion;
		this.claveNegocio = claveNegocio;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides({
			@AttributeOverride(name = "idToAgrupadorTarifa", column = @Column(name = "IDTOAGRUPADORTARIFA", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idVerAgrupadorTarifa", column = @Column(name = "IDVERAGRUPADORTARIFA", nullable = false, precision = 22, scale = 0)) })
	public AgrupadorTarifaId getId() {
		return this.id;
	}

	public void setId(AgrupadorTarifaId id) {
		this.id = id;
	}

	@Column(name = "DESCRIPCIONAGRUPADOR", nullable = false, length = 200)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=1, max=200, groups=EditItemChecks.class, message="{javax.validation.constraints.Size.message}")	
	public String getDescripcionAgrupador() {
		return this.descripcionAgrupador;
	}

	public void setDescripcionAgrupador(String descripcionAgrupador) {
		this.descripcionAgrupador = descripcionAgrupador;
	}

	@Column(name = "DESCRIPCIONVERSION", nullable = false, length = 200)
	public String getDescripcionVersion() {
		return this.descripcionVersion;
	}

	public void setDescripcionVersion(String descripcionVersion) {
		this.descripcionVersion = descripcionVersion;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAINICIOVIGENCIA", nullable = false, length = 7)
	public Date getFechaInicioVigencia() {
		return this.fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAFINVIGENCIA", nullable = false, length = 7)
	public Date getFechaFinVigencia() {
		return this.fechaFinVigencia;
	}

	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	@Column(name = "CLAVEESTATUS", nullable = false, precision = 4, scale = 0)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Short getClaveEstatus() {
		return this.claveEstatus;
	}

	public void setClaveEstatus(Short claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHACREACION", nullable = false, length = 7)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Column(name = "CODIGOUSUARIOCREACION", nullable = false, length = 8)
	public String getCodigoUsuarioCreacion() {
		return this.codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAACTIVACION", length = 7)
	public Date getFechaActivacion() {
		return this.fechaActivacion;
	}

	public void setFechaActivacion(Date fechaActivacion) {
		this.fechaActivacion = fechaActivacion;
	}

	@Column(name = "CODIGOUSUARIOACTIVACION", length = 8)
	public String getCodigoUsuarioActivacion() {
		return this.codigoUsuarioActivacion;
	}

	public void setCodigoUsuarioActivacion(String codigoUsuarioActivacion) {
		this.codigoUsuarioActivacion = codigoUsuarioActivacion;
	}

	@Column(name = "CLAVENEGOCIO", nullable = false, length = 1)
	public String getClaveNegocio() {
		return this.claveNegocio;
	}

	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}

	@SuppressWarnings("unchecked")
	@Override
	public AgrupadorTarifaId getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		return this.descripcionAgrupador;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getBusinessKey() {
		return this.id.getIdToAgrupadorTarifa().toString() + UtileriasWeb.SEPARADOR
		+ this.id.getIdVerAgrupadorTarifa().toString();
		
	}

	@Transient
	public String getDescripcionEstatus(){
		if(this.getClaveEstatus() == ESTATUS_ACTIVO){
			return  "ACTIVO";
		}else if (this.getClaveEstatus() == ESTATUS_BORRADO){
			return "BORRADO";
		}else if (this.getClaveEstatus() == ESTATUS_CREADO){
			return "CREADO";
		}else if (this.getClaveEstatus() == ESTATUS_NO_ACTIVO){
			return "NO ACTIVO";			
		}else{
			return "NO DISPONIBLE";
		}
	}

	@Transient
	public Map<Short, String> getEstatusValidos() {
		Map<Short,String> estatusValidos = new HashMap<Short, String>();
		if(this.getClaveEstatus() == ESTATUS_ACTIVO){
			estatusValidos.put(ESTATUS_ACTIVO, "ACTIVO");
			estatusValidos.put(ESTATUS_NO_ACTIVO, "NO ACTIVO");
		}else if (this.getClaveEstatus() == ESTATUS_CREADO){
			estatusValidos.put(null, "Seleccione ...");
			estatusValidos.put(ESTATUS_ACTIVO, "ACTIVO");
			estatusValidos.put(ESTATUS_NO_ACTIVO, "NO ACTIVO");
		}else if (this.getClaveEstatus() == ESTATUS_NO_ACTIVO){
			estatusValidos.put(ESTATUS_NO_ACTIVO, "NO ACTIVO");
			estatusValidos.put(ESTATUS_ACTIVO, "ACTIVO");	
		}else{
			estatusValidos.put(ESTATUS_CREADO, "CREADO");	
		}
		return estatusValidos;
	}

	@Override
	public int compareTo(AgrupadorTarifa o) {
		return this.id.getIdVerAgrupadorTarifa().compareTo(o.getId().getIdVerAgrupadorTarifa());
	}
}