package mx.com.afirme.midas2.domain.tarifa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.dto.DynamicControl;
import mx.com.afirme.midas2.dto.RegistroDinamicoDTO;
import mx.com.afirme.midas2.dto.DynamicControl.TipoControl;
import mx.com.afirme.midas2.dto.DynamicControl.TipoValidacion;
import mx.com.afirme.midas2.util.UtileriasWeb;
import mx.com.afirme.midas2.validator.group.NewItemChecks;

@Embeddable
public class TarifaAutoProliberId implements Serializable{

	private static final long serialVersionUID = 6009344793353050443L;
	
//	private Long idLineaNegocio;
	private Long idMoneda;
	private Long idRiesgo;
	private Long idConcepto;
	private Long version;
	private Long idTcTipoVehiculo;
	private String claveGrupoProliber;
	private Double toneledasCapacMin;
	private Double toneledasCapacMax;
	private Long numeroRemolques;
	private String codigoTipoUsoVehiculo;
	private String codigoTipoServicioVehiculo;
	
	public static final Long AUTO_SUSTITUTO_ROBO = 491l;
	
	public TarifaAutoProliberId(){
		
	}
	
	public TarifaAutoProliberId(String id){
		String[] id_arr = id.split(UtileriasWeb.SEPARADOR);
		//Revisar que la longitud del arreglo sea la correcta
		if(id_arr.length == 12){
			//Revisar que el id pertenezca al bean correcto
			if(id_arr[0].equals(RegistroDinamicoDTO.TARIFA)){
				idMoneda = Long.parseLong(id_arr[1]);
				idRiesgo = Long.parseLong(id_arr[2]);
				idConcepto = Long.parseLong(id_arr[3]);
				version = Long.parseLong(id_arr[4]);
				idTcTipoVehiculo = Long.parseLong(id_arr[5]);
				claveGrupoProliber = id_arr[6];
				toneledasCapacMin = Double.parseDouble(id_arr[7]);
				toneledasCapacMax = Double.parseDouble(id_arr[8]);
				numeroRemolques = Long.parseLong(id_arr[9]);
				codigoTipoUsoVehiculo = id_arr[10];
				codigoTipoServicioVehiculo = id_arr[11];
			}
		}
	}
	
	public TarifaAutoProliberId(TarifaVersionId tarifaVersionId){
		this.idMoneda = tarifaVersionId.getIdMoneda();
		this.idRiesgo = tarifaVersionId.getIdRiesgo();
		this.idConcepto = tarifaVersionId.getIdConcepto();
		this.version = tarifaVersionId.getVersion();
	}
	
	public TarifaAutoProliberId(Long idMoneda,Long idToRiesgo, Long idConcepto, 
			Long idVerTarifa, Long idTcTipoVehiculo, String claveGrupoProliber,
			Double toneledasCapacMin, Double toneledasCapacMax, 
			Long numeroRemolques, String codigoTipoUsoVehiculo, 
			String codigoTipoServicioVehiculo) {
		this.idMoneda = idMoneda;
		this.idRiesgo = idToRiesgo;
		this.idConcepto = idConcepto;
		this.version = idVerTarifa;
		this.idTcTipoVehiculo = idTcTipoVehiculo;
		this.claveGrupoProliber = claveGrupoProliber;
		this.toneledasCapacMin = toneledasCapacMin;
		this.toneledasCapacMax = toneledasCapacMax;
		this.numeroRemolques = numeroRemolques;
		this.codigoTipoUsoVehiculo = codigoTipoUsoVehiculo;
		this.codigoTipoServicioVehiculo = codigoTipoServicioVehiculo;
	}


	@DynamicControl(atributoMapeo="id.idMoneda", tipoControl = TipoControl.HIDDEN,etiqueta="Moneda",esComponenteId=true,editable=true,esNumerico=true,secuencia="2")
	@Column(name = "IDMONEDA", nullable = false, precision = 22, scale = 0)
	public Long getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}

	@DynamicControl(atributoMapeo="id.idRiesgo", tipoControl = TipoControl.HIDDEN,etiqueta="Riesgo",esComponenteId=true,editable=true,esNumerico=true,secuencia="3")
	@Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)
	public Long getIdRiesgo() {
		return idRiesgo;
	}

	public void setIdRiesgo(Long idRiesgo) {
		this.idRiesgo = idRiesgo;
	}

	@DynamicControl(atributoMapeo="id.idConcepto", tipoControl = TipoControl.HIDDEN,etiqueta="Concepto",esComponenteId=true,editable=true,esNumerico=true,secuencia="4")
	@Column(name = "IDCONCEPTO", nullable = false, precision = 4, scale = 0)
	public Long getIdConcepto() {
		return idConcepto;
	}

	public void setIdConcepto(Long idConcepto) {
		this.idConcepto = idConcepto;
	}

	@DynamicControl(atributoMapeo="id.version", tipoControl = TipoControl.HIDDEN,etiqueta="Versi\u00F3n",esComponenteId=true,editable=true,esNumerico=true,secuencia="5")
	@Column(name = "IDVERTARIFA", nullable = false, precision = 22, scale = 0)
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	@DynamicControl(atributoMapeo="id.idTcTipoVehiculo", tipoControl = TipoControl.SELECT,etiqueta="Tipo Veh\u00EDculo",esComponenteId=true,editable=true,esNumerico=true,secuencia="6",
			className="mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoFacadeRemote", 
			javaScriptOnChange="cascadeoGenerico(this,'USO-TIPO-VEH-PROL','id.codigoTipoUsoVehiculo'); cascadeoGenerico(this,'SERV-TIPO-VEH-PROL','id.codigoTipoServicioVehiculo');")
	@Column(name = "IDTCTIPOVEHICULO", nullable = false, precision = 22, scale = 0)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getIdTcTipoVehiculo() {
		return idTcTipoVehiculo;
	}

	public void setIdTcTipoVehiculo(Long idTcTipoVehiculo) {
		this.idTcTipoVehiculo = idTcTipoVehiculo;
	}

	@DynamicControl(atributoMapeo="id.claveGrupoProliber", tipoControl = TipoControl.TEXTBOX,etiqueta="Grupo PROLIBER",esComponenteId=true,editable=true,
			secuencia="7",longitudMaxima ="5", tipoValidacion = TipoValidacion.ALFANUMERICO)
	@Column(name = "CLAVEGRUPOPROLIBER", nullable = false, length = 5)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=1, max=5, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getClaveGrupoProliber() {
		return claveGrupoProliber!=null?this.claveGrupoProliber.toUpperCase():this.claveGrupoProliber;
	}

	public void setClaveGrupoProliber(String claveGrupoProliber) {
		this.claveGrupoProliber = claveGrupoProliber;
	}

	@DynamicControl(atributoMapeo="id.toneledasCapacMin", tipoControl = TipoControl.TEXTBOX,etiqueta="Toneladas Min",esComponenteId=true,editable=true,
			esNumerico=true,secuencia="8",longitudMaxima ="9", tipoValidacion = TipoValidacion.NUM_DECIMAL)
	@Column(name = "TONELADASCAPACMIN", nullable = false, precision = 8, scale = 2)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer = 6, fraction= 2,groups=NewItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public Double getToneledasCapacMin() {
		return toneledasCapacMin;
	}

	public void setToneledasCapacMin(Double toneledasCapacMin) {
		this.toneledasCapacMin = toneledasCapacMin;
	}

	@DynamicControl(atributoMapeo="id.toneledasCapacMax", tipoControl = TipoControl.TEXTBOX,etiqueta="Toneladas Max",esComponenteId=true,editable=true,
			esNumerico=true,secuencia="9", longitudMaxima ="9", tipoValidacion = TipoValidacion.NUM_DECIMAL)
	@Column(name = "TONELADASCAPACMAX", nullable = false, precision = 8, scale = 2)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer = 6, fraction= 2,groups=NewItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public Double getToneledasCapacMax() {
		return toneledasCapacMax;
	}

	public void setToneledasCapacMax(Double toneledasCapacMax) {
		this.toneledasCapacMax = toneledasCapacMax;
	}

	@DynamicControl(atributoMapeo="id.numeroRemolques", tipoControl = TipoControl.TEXTBOX,etiqueta="Remolques",esComponenteId=true,editable=true,
			esNumerico=true,secuencia="10",longitudMaxima ="10", tipoValidacion = TipoValidacion.NUM_ENTERO)
	@Column(name = "NUMEROREMOLQUES", nullable = false, precision = 10, scale = 0)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer = 10, fraction= 0,groups=NewItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public Long getNumeroRemolques() {
		return numeroRemolques;
	}

	public void setNumeroRemolques(Long numeroRemolques) {
		this.numeroRemolques = numeroRemolques;
	}

	@DynamicControl(atributoMapeo="id.codigoTipoUsoVehiculo", tipoControl = TipoControl.SELECT,etiqueta="Uso Veh\u00EDculo",esComponenteId=true,editable=true,secuencia="11",className="mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoFacadeRemote")
	@Column(name = "CODIGOTIPOUSOVEHICULO", nullable = false, length = 5)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=1, max=5, groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	public String getCodigoTipoUsoVehiculo() {
		return codigoTipoUsoVehiculo!=null?this.codigoTipoUsoVehiculo.toUpperCase():this.codigoTipoUsoVehiculo;
	}

	public void setCodigoTipoUsoVehiculo(String codigoTipoUsoVehiculo) {
		this.codigoTipoUsoVehiculo = codigoTipoUsoVehiculo;
	}

	@DynamicControl(atributoMapeo="id.codigoTipoServicioVehiculo", tipoControl = TipoControl.SELECT,etiqueta="Servicio Veh\u00EDculo",esComponenteId=true,editable=true,secuencia="12",className="mx.com.afirme.midas.catalogos.tiposerviciovehiculo.TipoServicioVehiculoFacadeRemote")
	@Column(name = "codigoTipoServicioVehiculo", nullable = false, length = 5)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=1, max=5, groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	public String getCodigoTipoServicioVehiculo() {
		return codigoTipoServicioVehiculo!=null?this.codigoTipoServicioVehiculo.toUpperCase():this.codigoTipoServicioVehiculo;
	}

	public void setCodigoTipoServicioVehiculo(String codigoTipoServicioVehiculo) {
		this.codigoTipoServicioVehiculo = codigoTipoServicioVehiculo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((claveGrupoProliber == null) ? 0 : claveGrupoProliber
						.hashCode());
		result = prime
				* result
				+ ((codigoTipoServicioVehiculo == null) ? 0
						: codigoTipoServicioVehiculo.hashCode());
		result = prime
				* result
				+ ((codigoTipoUsoVehiculo == null) ? 0 : codigoTipoUsoVehiculo
						.hashCode());
		result = prime * result
				+ ((idConcepto == null) ? 0 : idConcepto.hashCode());
		result = prime * result
				+ ((idMoneda == null) ? 0 : idMoneda.hashCode());
		result = prime
				* result
				+ ((idTcTipoVehiculo == null) ? 0 : idTcTipoVehiculo.hashCode());
		result = prime * result
				+ ((idRiesgo == null) ? 0 : idRiesgo.hashCode());
		result = prime * result
				+ ((version == null) ? 0 : version.hashCode());
		result = prime * result
				+ ((numeroRemolques == null) ? 0 : numeroRemolques.hashCode());
		result = prime
				* result
				+ ((toneledasCapacMax == null) ? 0 : toneledasCapacMax
						.hashCode());
		result = prime
				* result
				+ ((toneledasCapacMin == null) ? 0 : toneledasCapacMin
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TarifaAutoProliberId other = (TarifaAutoProliberId) obj;
		if (claveGrupoProliber == null) {
			if (other.claveGrupoProliber != null)
				return false;
		} else if (!claveGrupoProliber.equals(other.claveGrupoProliber))
			return false;
		if (codigoTipoServicioVehiculo == null) {
			if (other.codigoTipoServicioVehiculo != null)
				return false;
		} else if (!codigoTipoServicioVehiculo
				.equals(other.codigoTipoServicioVehiculo))
			return false;
		if (codigoTipoUsoVehiculo == null) {
			if (other.codigoTipoUsoVehiculo != null)
				return false;
		} else if (!codigoTipoUsoVehiculo.equals(other.codigoTipoUsoVehiculo))
			return false;
		if (idConcepto == null) {
			if (other.idConcepto != null)
				return false;
		} else if (!idConcepto.equals(other.idConcepto))
			return false;
		if (idMoneda == null) {
			if (other.idMoneda != null)
				return false;
		} else if (!idMoneda.equals(other.idMoneda))
			return false;
		if (idTcTipoVehiculo == null) {
			if (other.idTcTipoVehiculo != null)
				return false;
		} else if (!idTcTipoVehiculo.equals(other.idTcTipoVehiculo))
			return false;
		if (idRiesgo == null) {
			if (other.idRiesgo != null)
				return false;
		} else if (!idRiesgo.equals(other.idRiesgo))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		if (numeroRemolques == null) {
			if (other.numeroRemolques != null)
				return false;
		} else if (!numeroRemolques.equals(other.numeroRemolques))
			return false;
		if (toneledasCapacMax == null) {
			if (other.toneledasCapacMax != null)
				return false;
		} else if (!toneledasCapacMax.equals(other.toneledasCapacMax))
			return false;
		if (toneledasCapacMin == null) {
			if (other.toneledasCapacMin != null)
				return false;
		} else if (!toneledasCapacMin.equals(other.toneledasCapacMin))
			return false;
		return true;
	}
	
	@Override
	public String toString(){
		String sep = UtileriasWeb.SEPARADOR;
		StringBuilder toRet = new StringBuilder("");
		toRet.append(RegistroDinamicoDTO.TARIFA).append(sep)
			 .append(idMoneda.toString()).append(sep)
			 .append(idRiesgo.toString()).append(sep)
			 .append(idConcepto.toString()).append(sep)
			 .append(version.toString()).append(sep)
			 .append(idTcTipoVehiculo.toString()).append(sep)
			 .append(claveGrupoProliber).append(sep)
			 .append(toneledasCapacMin.toString()).append(sep)
			 .append(toneledasCapacMax.toString()).append(sep)
			 .append(numeroRemolques.toString()).append(sep)
			 .append(codigoTipoUsoVehiculo).append(sep)
			 .append(codigoTipoServicioVehiculo).append(sep);
		return toRet.toString();
	}

}
