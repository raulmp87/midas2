package mx.com.afirme.midas2.domain.tarifa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "TOTARIFAVERSION", schema = "MIDAS")
public class TarifaVersion implements Serializable {

	private static final long serialVersionUID = -2986919278647132603L;
	
	public static final short ESTATUS_CREADO = 0;
	public static final short ESTATUS_ACTIVO = 1;
	public static final short ESTATUS_INACTIVO = 2;
	public static final short ESTATUS_BORRADO = 3;
	
	
	private TarifaVersionId id;
	
	private String descripcion;
	
	private Date fechaInicioVigencia;
	
	private Date fechaFinVigencia;
	
	private Short claveEstatus;
	
	private String descripcionEstatus;
	
	private Date fechaCreacion;
	
	private String codigoUsuarioCreacion;
	
	private Date fechaActivacion;
	
	private String codigoUsuarioActivacion;
	
	private TarifaConcepto tarifaConcepto = new TarifaConcepto();

	@EmbeddedId
    @AttributeOverrides( {
    	@AttributeOverride(name = "idMoneda", column = @Column(name = "IDMONEDA", nullable = false, precision = 22, scale = 0)),
    	@AttributeOverride(name = "idRiesgo", column = @Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)),
	    @AttributeOverride(name = "idConcepto", column = @Column(name = "IDCONCEPTO", nullable = false, precision = 4, scale = 0)),
	    @AttributeOverride(name = "version", column = @Column(name = "IDVERTARIFA", nullable = false, precision = 22, scale = 0))
	    })
	public TarifaVersionId getId() {
		return id;
	}

	public void setId(TarifaVersionId id) {
		this.id = id;
	}

	@Column(name = "DESCRIPCIONVERSION", nullable = true, length = 200)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Temporal(TemporalType.DATE)
    @Column(name="FECHAINICIOVIGENCIA", length=7, nullable=true)
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	@Temporal(TemporalType.DATE)
    @Column(name="FECHAFINVIGENCIA", length=7, nullable=true)
	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}

	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	@Column(name = "CLAVEESTATUS", nullable = false, precision = 4, scale = 0)
	public Short getClaveEstatus() {
		return claveEstatus;
	}

	public void setClaveEstatus(Short claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	@Transient
	public String getDescripcionEstatus() {
		
		switch (this.claveEstatus) {
		
			case ESTATUS_CREADO : {
				descripcionEstatus = "CREADO";
				break;
			}
			case ESTATUS_ACTIVO : {
				descripcionEstatus = "ACTIVO";
				break;
			}
			case ESTATUS_INACTIVO : {
				descripcionEstatus = "NO ACTIVO";
				break;
			}
			case ESTATUS_BORRADO : {
				descripcionEstatus = "BORRADO";
				break;
			}
			default: {
				descripcionEstatus = "";
				break;
			}
		}
		
		return descripcionEstatus;
	}

	@Temporal(TemporalType.DATE)
    @Column(name="FECHACREACION", length=7, nullable=true)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Column(name = "CODIGOUSUARIOCREACION", nullable = true, length = 8)
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	@Temporal(TemporalType.DATE)
    @Column(name="FECHAACTIVACION", length=7, nullable=true)
	public Date getFechaActivacion() {
		return fechaActivacion;
	}

	public void setFechaActivacion(Date fechaActivacion) {
		this.fechaActivacion = fechaActivacion;
	}

	@Column(name = "CODIGOUSUARIOACTIVACION", nullable = true, length = 8)
	public String getCodigoUsuarioActivacion() {
		return codigoUsuarioActivacion;
	}
	
	public void setCodigoUsuarioActivacion(String codigoUsuarioActivacion) {
		this.codigoUsuarioActivacion = codigoUsuarioActivacion;
	}

	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumns( { 
        @JoinColumn(name="IDTORIESGO", referencedColumnName="IDTORIESGO", nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="IDCONCEPTO", referencedColumnName="IDCONCEPTO", nullable=false, insertable=false, updatable=false) } )
	public TarifaConcepto getTarifaConcepto() {
		return tarifaConcepto;
	}

	public void setTarifaConcepto(TarifaConcepto tarifaConcepto) {
		this.tarifaConcepto = tarifaConcepto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TarifaVersion other = (TarifaVersion) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public TarifaVersion() {
	}
	
	
	
	
	
}
