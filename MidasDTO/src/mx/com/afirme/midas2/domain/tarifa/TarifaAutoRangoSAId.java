package mx.com.afirme.midas2.domain.tarifa;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.dto.DynamicControl;
import mx.com.afirme.midas2.dto.RegistroDinamicoDTO;
import mx.com.afirme.midas2.dto.DynamicControl.TipoControl;
import mx.com.afirme.midas2.dto.DynamicControl.TipoValidacion;
import mx.com.afirme.midas2.util.UtileriasWeb;
import mx.com.afirme.midas2.validator.group.NewItemChecks;

@Embeddable
public class TarifaAutoRangoSAId implements Serializable {

	private static final long serialVersionUID = -1391822302115560613L;
	
	private Long idMoneda;
	private Long idRiesgo;
	private Long idConcepto;
	private Long version;
	private Long idPaquete;
	private Long idGrupoRC;
	private String codigoTipoServicioVehiculo;
	private BigDecimal valorSumaAseguradaBase;
	private BigDecimal valorSumaAseguradaMax;
	private BigDecimal numeroToneladasMin;
	private BigDecimal numeroToneladasMax;
	
	public TarifaAutoRangoSAId(){
		
	}
	
	public TarifaAutoRangoSAId(String id){
		String[] id_arr = id.split(UtileriasWeb.SEPARADOR);
		//Revisar que la longitud del arreglo sea la correcta
		if(id_arr.length == 12){
			//Revisar que el id pertenezca al bean correcto
			if(id_arr[0].equals(RegistroDinamicoDTO.TARIFA)){
				idMoneda = Long.parseLong(id_arr[1]);
				idRiesgo = Long.parseLong(id_arr[2]);
				idConcepto = Long.parseLong(id_arr[3]);
				version = Long.parseLong(id_arr[4]);
				idPaquete = Long.parseLong(id_arr[5]);
				idGrupoRC = Long.parseLong(id_arr[6]);
				codigoTipoServicioVehiculo = id_arr[7];
				valorSumaAseguradaBase = new BigDecimal(id_arr[8]);
				valorSumaAseguradaMax = new BigDecimal(id_arr[9]);
				numeroToneladasMin = new BigDecimal(id_arr[10]);
				numeroToneladasMax = new BigDecimal(id_arr[11]);
			}
		}
	}
	
	public TarifaAutoRangoSAId(TarifaVersionId tarifaVersionId){
		this.idMoneda = tarifaVersionId.getIdMoneda();
		this.idRiesgo = tarifaVersionId.getIdRiesgo();
		this.idConcepto = tarifaVersionId.getIdConcepto();
		this.version = tarifaVersionId.getVersion();
	}

	public TarifaAutoRangoSAId(Long idMoneda, Long idToRiesgo, Long idConcepto, 
			Long idVerTarifa, Long idPaquete, Long idGrupoRC, 
			String codigoTipoServicioVehiculo, BigDecimal valorSumaAseguradaBase, 
			BigDecimal valorSumaAseguradaMax, BigDecimal numeroToneladasMin, 
			BigDecimal numeroToneladasMax) {
		this.idMoneda = idMoneda;
		this.idRiesgo = idToRiesgo;
		this.idConcepto = idConcepto;
		this.version = idVerTarifa;
		this.idPaquete = idPaquete;
		this.idGrupoRC = idGrupoRC;
		this.codigoTipoServicioVehiculo = codigoTipoServicioVehiculo;
		this.valorSumaAseguradaBase = valorSumaAseguradaBase;
		this.valorSumaAseguradaMax = valorSumaAseguradaMax;
		this.numeroToneladasMin = numeroToneladasMin;
		this.numeroToneladasMax = numeroToneladasMax;
	}


	@DynamicControl(atributoMapeo="id.idMoneda", tipoControl = TipoControl.HIDDEN,etiqueta="Moneda",esComponenteId=true,editable=true,esNumerico=true,secuencia="2")
	@Column(name = "IDMONEDA", nullable = false, precision = 22, scale = 0)
	public Long getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}

	@DynamicControl(atributoMapeo="id.idRiesgo", tipoControl = TipoControl.HIDDEN,etiqueta="Riesgo",esComponenteId=true,editable=true,esNumerico=true,secuencia="3")
	@Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)
	public Long getIdRiesgo() {
		return idRiesgo;
	}

	public void setIdRiesgo(Long idRiesgo) {
		this.idRiesgo = idRiesgo;
	}

	@DynamicControl(atributoMapeo="id.idConcepto", tipoControl = TipoControl.HIDDEN,etiqueta="Concepto",esComponenteId=true,editable=true,esNumerico=true,secuencia="4")
	@Column(name = "IDCONCEPTO", nullable = false, precision = 4, scale = 0)
	public Long getIdConcepto() {
		return idConcepto;
	}

	public void setIdConcepto(Long idConcepto) {
		this.idConcepto = idConcepto;
	}

	@DynamicControl(atributoMapeo="id.version", tipoControl = TipoControl.HIDDEN,etiqueta="Versi\u00F3n",esComponenteId=true,editable=true,esNumerico=true,secuencia="5")
	@Column(name = "IDVERTARIFA", nullable = false, precision = 22, scale = 0)
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	@DynamicControl(atributoMapeo="id.idPaquete", tipoControl = TipoControl.SELECT,etiqueta="Paquete",esComponenteId=true,editable=true,esNumerico=true,secuencia="6",className="mx.com.afirme.midas2.dao.catalogos.PaqueteDao")
	@Column(name = "IDPAQUETE", nullable = false, precision = 10, scale = 0)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer = 10, fraction= 0,groups=NewItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public Long getIdPaquete() {
		return idPaquete;
	}

	public void setIdPaquete(Long idPaquete) {
		this.idPaquete = idPaquete;
	}

	@DynamicControl(atributoMapeo="id.idGrupoRC", tipoControl = TipoControl.TEXTBOX,etiqueta="Grupo RC",esComponenteId=true,editable=true,esNumerico=true,
			secuencia="7", longitudMaxima ="10", tipoValidacion = TipoValidacion.NUM_ENTERO)
	@Column(name = "IDGRUPORC", nullable = false, precision = 10, scale = 0)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer = 10, fraction= 0,groups=NewItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public Long getIdGrupoRC() {
		return idGrupoRC;
	}

	public void setIdGrupoRC(Long idGrupoRC) {
		this.idGrupoRC = idGrupoRC;
	}

	@DynamicControl(atributoMapeo="id.codigoTipoServicioVehiculo", tipoControl = TipoControl.SELECT,etiqueta="Servicio",esComponenteId=true,editable=true,esNumerico=true,secuencia="8",className="mx.com.afirme.midas.catalogos.tiposerviciovehiculo.TipoServicioVehiculoFacadeRemote")
	@Column(name = "CODIGOTIPOSERVICIOVEHICULO", nullable = false, length = 5)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=1, max=5, groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	public String getCodigoTipoServicioVehiculo() {
		return codigoTipoServicioVehiculo!=null?this.codigoTipoServicioVehiculo.toUpperCase():this.codigoTipoServicioVehiculo;
	}

	public void setCodigoTipoServicioVehiculo(String codigoTipoServicioVehiculo) {
		this.codigoTipoServicioVehiculo = codigoTipoServicioVehiculo;
	}

	@DynamicControl(atributoMapeo="id.valorSumaAseguradaBase", tipoControl = TipoControl.TEXTBOX,etiqueta="Suma Asegurada Base",esComponenteId=true,
			editable=true,esNumerico=true,secuencia="9", longitudMaxima ="16", tipoValidacion = TipoValidacion.NUM_ENTERO)
	@Column(name = "VALORSUMAASEGURADABASE", nullable = false, precision = 16, scale = 0)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer = 16, fraction= 0,groups=NewItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public BigDecimal getValorSumaAseguradaBase() {
		return valorSumaAseguradaBase;
	}

	public void setValorSumaAseguradaBase(BigDecimal valorSumaAseguradaBase) {
		this.valorSumaAseguradaBase = valorSumaAseguradaBase;
	}

	@DynamicControl(atributoMapeo="id.valorSumaAseguradaMax", tipoControl = TipoControl.TEXTBOX,etiqueta="Suma Asegurada M\u00E1xima",esComponenteId=true,
			editable=true,esNumerico=true,secuencia="10", longitudMaxima ="16", tipoValidacion = TipoValidacion.NUM_ENTERO)
	@Column(name = "VALORSUMAASEGURADAMAX", nullable = false, precision = 16, scale = 0)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer = 16, fraction= 0,groups=NewItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public BigDecimal getValorSumaAseguradaMax() {
		return valorSumaAseguradaMax;
	}

	public void setValorSumaAseguradaMax(BigDecimal valorSumaAseguradaMax) {
		this.valorSumaAseguradaMax = valorSumaAseguradaMax;
	}

	@DynamicControl(atributoMapeo="id.numeroToneladasMin", tipoControl = TipoControl.TEXTBOX,etiqueta="Toneladas Min",esComponenteId=true,editable=true,
			esNumerico=true,secuencia="11", longitudMaxima ="9", tipoValidacion = TipoValidacion.NUM_DECIMAL)
	@Column(name = "NUMEROTONELADASMIN", nullable = false, precision = 8, scale = 2)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer = 6, fraction= 2,groups=NewItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public BigDecimal getNumeroToneladasMin() {
		return numeroToneladasMin;
	}

	public void setNumeroToneladasMin(BigDecimal numeroToneladasMin) {
		this.numeroToneladasMin = numeroToneladasMin;
	}
	
	

	@DynamicControl(atributoMapeo="id.numeroToneladasMax", tipoControl = TipoControl.TEXTBOX,etiqueta="Toneladas Max",esComponenteId=true,editable=true,
			esNumerico=true,secuencia="12", longitudMaxima ="9", tipoValidacion = TipoValidacion.NUM_DECIMAL)
	@Column(name = "NUMEROTONELADASMAX", nullable = false, precision = 8, scale = 2)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer = 6, fraction= 2,groups=NewItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public BigDecimal getNumeroToneladasMax() {
		return numeroToneladasMax;
	}

	public void setNumeroToneladasMax(BigDecimal numeroToneladasMax) {
		this.numeroToneladasMax = numeroToneladasMax;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((codigoTipoServicioVehiculo == null) ? 0
						: codigoTipoServicioVehiculo.hashCode());
		result = prime * result
				+ ((idConcepto == null) ? 0 : idConcepto.hashCode());
		result = prime * result
				+ ((idGrupoRC == null) ? 0 : idGrupoRC.hashCode());
		result = prime * result
				+ ((idMoneda == null) ? 0 : idMoneda.hashCode());
		result = prime * result
				+ ((idPaquete == null) ? 0 : idPaquete.hashCode());
		result = prime * result
				+ ((idRiesgo == null) ? 0 : idRiesgo.hashCode());
		result = prime * result
				+ ((version == null) ? 0 : version.hashCode());
		result = prime
				* result
				+ ((numeroToneladasMax == null) ? 0 : numeroToneladasMax
						.hashCode());
		result = prime
				* result
				+ ((numeroToneladasMin == null) ? 0 : numeroToneladasMin
						.hashCode());
		result = prime
				* result
				+ ((valorSumaAseguradaBase == null) ? 0
						: valorSumaAseguradaBase.hashCode());
		result = prime
				* result
				+ ((valorSumaAseguradaMax == null) ? 0 : valorSumaAseguradaMax
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TarifaAutoRangoSAId other = (TarifaAutoRangoSAId) obj;
		if (codigoTipoServicioVehiculo == null) {
			if (other.codigoTipoServicioVehiculo != null)
				return false;
		} else if (!codigoTipoServicioVehiculo
				.equals(other.codigoTipoServicioVehiculo))
			return false;
		if (idConcepto == null) {
			if (other.idConcepto != null)
				return false;
		} else if (!idConcepto.equals(other.idConcepto))
			return false;
		if (idGrupoRC == null) {
			if (other.idGrupoRC != null)
				return false;
		} else if (!idGrupoRC.equals(other.idGrupoRC))
			return false;
		if (idMoneda == null) {
			if (other.idMoneda != null)
				return false;
		} else if (!idMoneda.equals(other.idMoneda))
			return false;
		if (idPaquete == null) {
			if (other.idPaquete != null)
				return false;
		} else if (!idPaquete.equals(other.idPaquete))
			return false;
		if (idRiesgo == null) {
			if (other.idRiesgo != null)
				return false;
		} else if (!idRiesgo.equals(other.idRiesgo))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		if (numeroToneladasMax == null) {
			if (other.numeroToneladasMax != null)
				return false;
		} else if (!numeroToneladasMax.equals(other.numeroToneladasMax))
			return false;
		if (numeroToneladasMin == null) {
			if (other.numeroToneladasMin != null)
				return false;
		} else if (!numeroToneladasMin.equals(other.numeroToneladasMin))
			return false;
		if (valorSumaAseguradaBase == null) {
			if (other.valorSumaAseguradaBase != null)
				return false;
		} else if (!valorSumaAseguradaBase.equals(other.valorSumaAseguradaBase))
			return false;
		if (valorSumaAseguradaMax == null) {
			if (other.valorSumaAseguradaMax != null)
				return false;
		} else if (!valorSumaAseguradaMax.equals(other.valorSumaAseguradaMax))
			return false;
		return true;
	}
	
	@Override
	public String toString(){
		String sep = UtileriasWeb.SEPARADOR;
		StringBuilder toRet = new StringBuilder("");
		toRet.append(RegistroDinamicoDTO.TARIFA).append(sep)
			 .append(idMoneda.toString()).append(sep)
			 .append(idRiesgo.toString()).append(sep)
			 .append(idConcepto.toString()).append(sep)
			 .append(version.toString()).append(sep)
			 .append(idPaquete.toString()).append(sep)
			 .append(idGrupoRC.toString()).append(sep)
			 .append(codigoTipoServicioVehiculo.toString()).append(sep)
			 .append(valorSumaAseguradaBase.toString()).append(sep)
			 .append(valorSumaAseguradaMax.toString()).append(sep)
			 .append(numeroToneladasMin.toString()).append(sep)
			 .append(numeroToneladasMax.toString()).append(sep);
		return toRet.toString();
	}
	
	
}
