package mx.com.afirme.midas2.domain.tarifa;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

@Entity(name = "ConfiguracionTarifaAuto")
@Table(name = "TCCONFIGURACIONTARIFAAUTO", schema = "MIDAS")
@Cache(
		type=CacheType.FULL,
		size=100000,
		alwaysRefresh=false,
		disableHits=false,
		refreshOnlyIfNewer=true
		)
public class ConfiguracionTarifaAuto implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final short CASCADEO_PADRE = 1;
	public static final short CASCADEO_HIJO = 2;
	
	public static final int HIDDEN = 0;
	public static final int COMBO_PROPIO = 1;
	public static final int COMBO_CATALOGO_VALOR_FIJO= 2;
	public static final int COMBO_ESCALON = 3;
	public static final int CAJA_TEXTO = 4;
	public static final int COMBO_POR_TARIFA_VERSION_ID = 5;
	public static final int COMBO_POR_TIPO_VEHICULO = 6;
	
	private ConfiguracionTarifaAutoId id;
	private Long claveTipoControl;
	private Long claveTipoValidacion;
	private String descripcionEtiqueta;
	private String descripcionClaseRemota;
	private Long idGrupoMN;
	private Long idGrupoDLS;
	
	private String claveComboHijo;
	private Short claveCascadeo;
	
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToRiesgo", column = @Column(name = "IDTORIESGO", nullable = false, precision = 38, scale = 0)),
			@AttributeOverride(name = "idConcepto", column = @Column(name = "IDCONCEPTO", nullable = false, precision = 38, scale = 0)),
			@AttributeOverride(name = "idDato", column = @Column(name = "IDDATO", nullable = false, precision = 38, scale = 0)) })
	public ConfiguracionTarifaAutoId getId() {
		return id;
	}
	public void setId(ConfiguracionTarifaAutoId id) {
		this.id = id;
	}
	
	@Column(name = "CLAVETIPOCONTROL", nullable = false, precision = 4, scale = 0)
	public Long getClaveTipoControl() {
		return claveTipoControl;
	}
	public void setClaveTipoControl(Long claveTipoControl) {
		this.claveTipoControl = claveTipoControl;
	}
	
	@Column(name = "CLAVETIPOVALIDACION", nullable = false, precision = 4, scale = 0)
	public Long getClaveTipoValidacion() {
		return claveTipoValidacion;
	}
	public void setClaveTipoValidacion(Long claveTipoValidacion) {
		this.claveTipoValidacion = claveTipoValidacion;
	}
	
	@Column(name = "DESCRIPCIONETIQUETA")
	public String getDescripcionEtiqueta() {
		return descripcionEtiqueta;
	}
	public void setDescripcionEtiqueta(String descripcionEtiqueta) {
		this.descripcionEtiqueta = descripcionEtiqueta;
	}
	
	@Column(name = "DESCRIPCIONCLASEREMOTA")
	public String getDescripcionClaseRemota() {
		return descripcionClaseRemota;
	}
	public void setDescripcionClaseRemota(String descripcionClaseRemota) {
		this.descripcionClaseRemota = descripcionClaseRemota;
	}
	
	@Column(name = "IDGRUPOMN", nullable = false, precision = 38, scale = 0)
	public Long getIdGrupoMN() {
		return idGrupoMN;
	}
	public void setIdGrupoMN(Long idGrupoMN) {
		this.idGrupoMN = idGrupoMN;
	}
	
	@Column(name = "IDGRUPODLS", nullable = false, precision = 38, scale = 0)
	public Long getIdGrupoDLS() {
		return idGrupoDLS;
	}
	public void setIdGrupoDLS(Long idGrupoDLS) {
		this.idGrupoDLS = idGrupoDLS;
	}
	
	@Transient
	public String getClaveComboHijo() {
		return claveComboHijo;
	}
	public void setClaveComboHijo(String claveComboHijo) {
		this.claveComboHijo = claveComboHijo;
	}
	
	@Transient
	public Short getClaveCascadeo() {
		return claveCascadeo;
	}
	public void setClaveCascadeo(Short claveCascadeo) {
		this.claveCascadeo = claveCascadeo;
	}
	
	
}
