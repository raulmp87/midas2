package mx.com.afirme.midas2.domain.tarifa;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dto.DynamicControl;
import mx.com.afirme.midas2.dto.DynamicControl.TipoControl;
import mx.com.afirme.midas2.dto.DynamicControl.TipoValidacion;
import mx.com.afirme.midas2.dto.IdDynamicRow;
import mx.com.afirme.midas2.validator.group.EditItemChecks;


@Entity
@Table(name="TOTARIFAAUTORANGOSSA", schema="MIDAS")
public class TarifaAutoRangoSA implements Serializable {

	private static final long serialVersionUID = -1824613606325460857L;
	private TarifaAutoRangoSAId id = null;
	private BigDecimal cuotaBase = null;
	private BigDecimal cuotaExceso = null;
	@IdDynamicRow
	@EmbeddedId
	@AttributeOverrides({
		@AttributeOverride(name="idMoneda", column = @Column(name = "IDMONEDA", nullable = false, precision = 22, scale = 0)),
		@AttributeOverride(name="idRiesgo", column = @Column(name = "IDTORIESGO", nullable = false, precision = 22, scale = 0)),
		@AttributeOverride(name="idConcepto", column = @Column(name = "IDCONCEPTO", nullable = false, precision = 4, scale = 0)),
		@AttributeOverride(name="version", column = @Column(name = "IDVERTARIFA", nullable = false, precision = 22, scale = 0)),
		@AttributeOverride(name="idPaquete", column = @Column(name = "IDPAQUETE", nullable = false, precision = 22, scale = 0)),
		@AttributeOverride(name="idGrupoRC", column = @Column(name = "IDGRUPORC", nullable = false, precision = 22, scale = 0)),
		@AttributeOverride(name="codigoTipoServicioVehiculo", column = @Column(name = "CODIGOTIPOSERVICIOVEHICULO", nullable = false)),
		@AttributeOverride(name="valorSumaAseguradaBase", column = @Column(name = "VALORSUMAASEGURADABASE", nullable = false, precision = 16, scale = 0)),
		@AttributeOverride(name="valorSumaAseguradaMax", column = @Column(name = "VALORSUMAASEGURADAMAX", nullable = false, precision = 16, scale = 0)),
		@AttributeOverride(name="numeroToneladasMin", column = @Column(name = "NUMEROTONELADASMIN", nullable = false, precision = 8, scale = 2)),
		@AttributeOverride(name="numeroToneladasMax", column = @Column(name = "NUMEROTONELADASMAX", nullable = false, precision = 8, scale = 2))
	})
	@Valid
	public TarifaAutoRangoSAId getId() {
		return id;
	}
	
	public void setId(TarifaAutoRangoSAId id) {
		this.id = id;
	}
	
	@DynamicControl(atributoMapeo="cuotaBase", tipoControl=TipoControl.TEXTBOX, etiqueta="Cuota Base", editable=true, esNumerico=true,
			secuencia="13", longitudMaxima ="23", tipoValidacion = TipoValidacion.NUM_DECIMAL)
	@Column(name = "CUOTABASE", nullable = false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer = 12, fraction= 10,groups=EditItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public BigDecimal getCuotaBase() {
		return cuotaBase;
	}
	
	public void setCuotaBase(BigDecimal cuotaBase) {
		this.cuotaBase = cuotaBase;
	}
	
	@DynamicControl(atributoMapeo="cuotaExceso", tipoControl=TipoControl.TEXTBOX, etiqueta="Cuota Exceso", editable=true, esNumerico=true,
			secuencia="14", longitudMaxima ="23", tipoValidacion = TipoValidacion.NUM_DECIMAL)
	@Column(name = "CUOTAEXCESO", nullable = false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer = 12, fraction= 10,groups=EditItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public BigDecimal getCuotaExceso() {
		return cuotaExceso;
	}
	
	public void setCuotaExceso(BigDecimal cuotaExceso) {
		this.cuotaExceso = cuotaExceso;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TarifaAutoRangoSA other = (TarifaAutoRangoSA) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	

}
