package mx.com.afirme.midas2.domain.tarea;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.base.PaginadoDTO;
import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name = "TTEMISIONPENDIENTE", schema = "MIDAS")
public class EmisionPendiente extends PaginadoDTO implements Entidad, Serializable {

	
	private static final long serialVersionUID = -4711900943642279990L;
	public static final short POLIZA_NO_MIGRADA = 0;
	public static final short POLIZA_MIGRADA = 1;
	
    public static enum ClaveTipoEndoso{
		
		CLAVETIPOENDOSO_CAP("CAP"), 
		CLAVETIPOENDOSO_CPP("CPP"), 
		CLAVETIPOENDOSO_REAP("REAP"), 
		CLAVETIPOENDOSO_REPP("REPP"),
		CLAVETIPOENDOSO_CAE("CAE"),
		CLAVETIPOENDOSO_CEP("CEP"),
		CLAVETIPOENDOSO_BIN("BIN"),
		CLAVETIPOENDOSO_CAI("CAI"),
		CLAVETIPOENDOSO_REIC("REIC"),
		CLAVETIPOENDOSO_REAE("REAE");
		
		
		ClaveTipoEndoso(String claveTipoEndoso) {
			this.claveTipoEndoso = claveTipoEndoso;
		}
		private String claveTipoEndoso;
		public String getClaveTipoEndoso(){
			return this.claveTipoEndoso;
		}
	};
	
	public static enum TipoEmision{
		
		CANC_POL_DANIOS((short)1), 
		REHAB_POL_DANIOS((short)2), 
		CANC_END_DANIOS((short)3), 
		REHAB_END_DANIOS((short)4), 
		BAJA_INCISO_PERDIDA_TOTAL_AUTOS((short)5), 
		REHAB_INCISO_AUTOS((short)6),
		CANC_POL_AUTOS((short)7), 
		REHAB_POL_AUTOS((short)8), 
		CANC_END_AUTOS((short)9), 
		REHAB_END_AUTOS((short)10),
		CANC_INCISO_AUTOS((short)11), 
		CAMBIO_FORMA_PAGO((short)12);
		
		TipoEmision(Short tipoEmision) {
			this.tipoEmision = tipoEmision;
		}
		private Short tipoEmision;
		public Short getTipoEmision(){
			return this.tipoEmision;
		}
	};
	
	public static enum EstatusRegistro{
		
		SIN_PROCESAR((short)0), 
		EN_PROCESO((short)1), 
		PROCESADO((short)2), 
		EMISION_FALLO((short)3);
		
		EstatusRegistro(Short estatusRegistro) {
			this.estatusRegistro = estatusRegistro;
		}
		private Short estatusRegistro;
		public Short getEstatusRegistro(){
			return this.estatusRegistro;
		}
	};
		
	private Long id;
	private BigDecimal idToPoliza;
    private Long numeroEndoso;
    private Short estatusRegistro;
    private Short claveTipoEndoso;
	private Date fechaInicioVigencia;
	private Double valorPrimaNeta;
	private Double valorBonifComision;
	private Double valorDerechos;
	private Double valorRPF;
	private Double valorIVA;
	private Double valorPrimaTotal;
	private Double valorComision;
	private Double valorComisionRPF;
	private Double valorBonifComisionRPF;
	private String numeroInciso;
	private Short tipoEmision;
	private Short bloqueoEmision;
	private Date fechaValidacion;
	private String nombreContratante;
	private String nombreAgente;
	private Short migrada;	
	private Short claveMotivoEndoso;	
	private Boolean checked;
	private String auxiliarFlag;
	private BigDecimal numeroPolizaSeycos;	
	private String clavePolizaSeycos;
	private BigDecimal codigoAgente;
	private String codigoProducto;
	private String codigoTipoPoliza;
	private Integer numeroRenovacion;
	private Integer numPoliza;
	private String numeroPoliza;
	
	
	
	public EmisionPendiente() {
		super();
	}
	
	public EmisionPendiente(Long id, BigDecimal idToPoliza, Long numeroEndoso,
			Short estatusRegistro, Short claveTipoEndoso,
			Date fechaInicioVigencia, Double valorPrimaNeta,
			Double valorBonifComision, Double valorDerechos, Double valorRPF,
			Double valorIVA, Double valorPrimaTotal, Double valorComision,
			Double valorComisionRPF, Double valorBonifComisionRPF,
			String numeroInciso, Short tipoEmision, Short bloqueoEmision,
			Date fechaValidacion, String nombreContratante, String nombreAgente, Short migrada,
			Short claveMotivoEndoso, Boolean checked, String auxiliarFlag,
			BigDecimal numeroPolizaSeycos, String clavePolizaSeycos,
			BigDecimal codigoAgente, String codigoProducto, String codigoTipoPoliza,
			Integer numeroRenovacion, Integer numPoliza) {
		super();
		this.id = id;
		this.idToPoliza = idToPoliza;
		this.numeroEndoso = numeroEndoso;
		this.estatusRegistro = estatusRegistro;
		this.claveTipoEndoso = claveTipoEndoso;
		this.fechaInicioVigencia = fechaInicioVigencia;
		this.valorPrimaNeta = valorPrimaNeta;
		this.valorBonifComision = valorBonifComision;
		this.valorDerechos = valorDerechos;
		this.valorRPF = valorRPF;
		this.valorIVA = valorIVA;
		this.valorPrimaTotal = valorPrimaTotal;
		this.valorComision = valorComision;
		this.valorComisionRPF = valorComisionRPF;
		this.valorBonifComisionRPF = valorBonifComisionRPF;
		this.numeroInciso = numeroInciso;
		this.tipoEmision = tipoEmision;
		this.bloqueoEmision = bloqueoEmision;
		this.fechaValidacion = fechaValidacion;
		this.nombreContratante = nombreContratante;
		this.nombreAgente = nombreAgente;
		this.migrada = migrada;
		this.claveMotivoEndoso = claveMotivoEndoso;
		this.checked = checked;
		this.auxiliarFlag = auxiliarFlag;
		this.numeroPolizaSeycos = numeroPolizaSeycos;
		this.clavePolizaSeycos = clavePolizaSeycos;
		this.codigoAgente = codigoAgente;
		this.codigoProducto = codigoProducto;
		this.codigoTipoPoliza = codigoTipoPoliza;
		this.numeroRenovacion = numeroRenovacion;
		this.numPoliza = numPoliza;
	}
	
	public static HashMap<Short, TipoEmision> tipoEmisionMap = new HashMap<Short, EmisionPendiente.TipoEmision>(11);
	static {
		tipoEmisionMap.put((short)1, TipoEmision.CANC_POL_DANIOS); 
		tipoEmisionMap.put((short)2, TipoEmision.REHAB_POL_DANIOS); 
		tipoEmisionMap.put((short)3, TipoEmision.CANC_END_DANIOS);
		tipoEmisionMap.put((short)4, TipoEmision.REHAB_END_DANIOS);
		tipoEmisionMap.put((short)5, TipoEmision.BAJA_INCISO_PERDIDA_TOTAL_AUTOS); 
		tipoEmisionMap.put((short)6, TipoEmision.REHAB_INCISO_AUTOS);
		tipoEmisionMap.put((short)7, TipoEmision.CANC_POL_AUTOS);
		tipoEmisionMap.put((short)8, TipoEmision.REHAB_POL_AUTOS);
		tipoEmisionMap.put((short)9, TipoEmision.CANC_END_AUTOS);
		tipoEmisionMap.put((short)10, TipoEmision.REHAB_END_AUTOS);
		tipoEmisionMap.put((short)11, TipoEmision.CANC_INCISO_AUTOS);
	}
	
	@Id
	@SequenceGenerator(name = "IDTTEMISIONPENDIENTE_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTTEMISIONPENDIENTE_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTTEMISIONPENDIENTE_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="IDTOPOLIZA", nullable=false, precision=22, scale=0)
	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}
	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}
	
	@Column(name="NUMEROENDOSO", nullable=false, precision=22, scale=0)
	public Long getNumeroEndoso() {
		return numeroEndoso;
	}
	public void setNumeroEndoso(Long numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}
	
	@Column(name = "ESTATUSREGISTRO", nullable = false, precision = 1, scale = 0)
	public Short getEstatusRegistro() {
		return estatusRegistro;
	}
	public void setEstatusRegistro(Short estatusRegistro) {
		this.estatusRegistro = estatusRegistro;
	}
	
	@Column(name = "CLAVETIPOENDOSO", nullable = false, precision = 1, scale = 0)
	public Short getClaveTipoEndoso() {
		return claveTipoEndoso;
	}
	public void setClaveTipoEndoso(Short claveTipoEndoso) {
		this.claveTipoEndoso = claveTipoEndoso;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAINICIOVIGENCIA", nullable = false, length = 11)
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}
	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	
	@Column(name = "VALORPRIMANETA", nullable = false, precision = 16)
	public Double getValorPrimaNeta() {
		return valorPrimaNeta;
	}
	public void setValorPrimaNeta(Double valorPrimaNeta) {
		this.valorPrimaNeta = valorPrimaNeta;
	}
	
	@Column(name = "VALORBONIFCOMISION", nullable = false, precision = 16)
	public Double getValorBonifComision() {
		return valorBonifComision;
	}
	public void setValorBonifComision(Double valorBonifComision) {
		this.valorBonifComision = valorBonifComision;
	}
	
	@Column(name = "VALORDERECHOS", nullable = false, precision = 16)
	public Double getValorDerechos() {
		return valorDerechos;
	}
	public void setValorDerechos(Double valorDerechos) {
		this.valorDerechos = valorDerechos;
	}
	
	@Column(name = "VALORRECARGOPAGOFRAC", nullable = false, precision = 16)
	public Double getValorRPF() {
		return valorRPF;
	}
	public void setValorRPF(Double valorRPF) {
		this.valorRPF = valorRPF;
	}
	
	@Column(name = "VALORIVA", nullable = false, precision = 16)
	public Double getValorIVA() {
		return valorIVA;
	}
	public void setValorIVA(Double valorIVA) {
		this.valorIVA = valorIVA;
	}
	
	@Column(name = "VALORPRIMATOTAL", nullable = false, precision = 16)
	public Double getValorPrimaTotal() {
		return valorPrimaTotal;
	}
	public void setValorPrimaTotal(Double valorPrimaTotal) {
		this.valorPrimaTotal = valorPrimaTotal;
	}
	
	@Column(name = "VALORCOMISION")
	public Double getValorComision() {
		return valorComision;
	}
	public void setValorComision(Double valorComision) {
		this.valorComision = valorComision;
	}
	
	@Column(name = "VALORCOMISIONRECPAGOFRAC")
	public Double getValorComisionRPF() {
		return valorComisionRPF;
	}
	public void setValorComisionRPF(Double valorComisionRPF) {
		this.valorComisionRPF = valorComisionRPF;
	}
	
	@Column(name = "VALORBONIFCOMRECPAGOFRAC")
	public Double getValorBonifComisionRPF() {
		return valorBonifComisionRPF;
	}
	public void setValorBonifComisionRPF(Double valorBonifComisionRPF) {
		this.valorBonifComisionRPF = valorBonifComisionRPF;
	}
	
	@Column(name = "NUMEROINCISO", length = 1500)
	public String getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
		
	@Column(name = "TIPOEMISION")
	public Short getTipoEmision() {
		return tipoEmision;
	}
	public void setTipoEmision(Short tipoEmision) {
		this.tipoEmision = tipoEmision;
	}
	
	@Column(name = "BLOQUEOEMISION", nullable = false, precision = 1, scale = 0)
	public Short getBloqueoEmision() {
		return bloqueoEmision;
	}
	public void setBloqueoEmision(Short bloqueoEmision) {
		this.bloqueoEmision = bloqueoEmision;
	}
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAVALIDACION", nullable = false, length = 11)
	public Date getFechaValidacion() {
		return fechaValidacion;
	}
	public void setFechaValidacion(Date fechaValidacion) {
		this.fechaValidacion = fechaValidacion;
	}
	
	@Column(name="MIGRADA",nullable = true, precision = 1, scale = 0)
	public Short getMigrada() {
		return migrada;
	}
	public void setMigrada(Short migrada) {
		this.migrada = migrada;
	}
	
	@Column(name = "CLAVEMOTIVOENDOSO", nullable = true, precision = 1, scale = 0)
	public Short getClaveMotivoEndoso() {
		return claveMotivoEndoso;
	}
	public void setClaveMotivoEndoso(Short claveMotivoEndoso) {
		this.claveMotivoEndoso = claveMotivoEndoso;
	}
	
	@Transient
	public String getNumeroPoliza() {
		return this.numeroPoliza != null ? this.numeroPoliza : getNumeroPolizaFormateada();
	}	
	
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	
	@Transient	
	public String getNombreContratante() {
		return nombreContratante;
	}
	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}
		
	@Transient
	public String getNombreAgente() {
		return nombreAgente;
	}
	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return getId();
	}
	@Override
	public String getValue() {
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getBusinessKey() {
		return getIdToPoliza();
	}
	
	@Transient
	public Boolean getChecked() {
		return checked;
	}
	public void setChecked(Boolean checked) {
		this.checked = checked;
	}
	
	@Transient
	public String getAuxiliarFlag() {
		return auxiliarFlag;
	}
	public void setAuxiliarFlag(String auxiliarFlag) {
		this.auxiliarFlag = auxiliarFlag;
	}
	@Transient
	public BigDecimal getNumeroPolizaSeycos() {
		return numeroPolizaSeycos;
	}
	public void setNumeroPolizaSeycos(BigDecimal numeroPolizaSeycos) {
		this.numeroPolizaSeycos = numeroPolizaSeycos;
	}
	@Transient
	public String getClavePolizaSeycos() {
		return !StringUtil.isEmpty(clavePolizaSeycos) ? clavePolizaSeycos : getNumeroPolizaFormateada();
	}
	public void setClavePolizaSeycos(String clavePolizaSeycos) {
		this.clavePolizaSeycos = clavePolizaSeycos;
	}
	@Transient
	public BigDecimal getCodigoAgente() {
		return codigoAgente;
	}
	public void setCodigoAgente(BigDecimal codigoAgente) {
		this.codigoAgente = codigoAgente;
	}	
	
	@Transient
	public String getCodigoProducto() {
		return codigoProducto;
	}

	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	@Transient
	public String getCodigoTipoPoliza() {
		return codigoTipoPoliza;
	}

	public void setCodigoTipoPoliza(String codigoTipoPoliza) {
		this.codigoTipoPoliza = codigoTipoPoliza;
	}

	@Transient
	public Integer getNumeroRenovacion() {
		return numeroRenovacion;
	}

	public void setNumeroRenovacion(Integer numeroRenovacion) {
		this.numeroRenovacion = numeroRenovacion;
	}

	@Transient
	public String getNumeroPolizaFormateada() {
		String numeroPoliza = "";
		if ((this.codigoProducto != null && this.codigoProducto.length() > 0)
				&& (this.codigoTipoPoliza != null && this.codigoTipoPoliza
						.length() > 0) && (this.numPoliza != null)
				&& (this.numeroRenovacion != null)) {
			try {
				numeroPoliza += String.format("%02d", Integer
						.parseInt(this.codigoProducto));
				numeroPoliza += String.format("%02d", Integer
						.parseInt(this.codigoTipoPoliza));
				numeroPoliza += "-";
				numeroPoliza += String.format("%06d", this.numPoliza);
				numeroPoliza += "-";
				numeroPoliza += String.format("%02d", this.numeroRenovacion);
			} catch (Exception e) {
				return "";
			}

		}
		return numeroPoliza;
	}
}
