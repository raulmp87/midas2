package mx.com.afirme.midas2.domain.fronterizos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="TOBITACORACESVI" , schema="MIDAS")
public class BitacoraCESVI implements Serializable, Entidad {

	private static final long serialVersionUID = -2514525577397307291L;
	private Long id;
	private BigDecimal idToCotizacion;
	private BigDecimal numeroInciso;
	private Long idReporteCabina;
	private String numeroSerie;
	private BigDecimal valorComercial;
	private Date fechaCreacion;
	private String codigoUsuarioCreacion;
	
	public BitacoraCESVI() {
	        
	}

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOBITACORACESVI_ID_GENERATOR")
	@SequenceGenerator(name="TOBITACORACESVI_ID_GENERATOR", sequenceName="MIDAS.IDTOBITACORACESVI_SEQ", allocationSize=1)
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "IDTOCOTIZACION")
	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	@Column(name = "NUMEROINCISO")
	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	
	@Column(name = "IDREPORTECABINA")
	public Long getIdReporteCabina() {
		return idReporteCabina;
	}

	public void setIdReporteCabina(Long idReporteCabina) {
		this.idReporteCabina = idReporteCabina;
	}

	@Column(name = "NUMEROSERIE")
	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	@Column(name = "VALORCOMERCIAL")
	public BigDecimal getValorComercial() {
		return valorComercial;
	}

	public void setValorComercial(BigDecimal valorComercial) {
		this.valorComercial = valorComercial;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHACREACION", nullable = false, length = 7)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Column(name = "CODIGOUSUARIOCREACION", nullable = false, length = 8)
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {

		return null;
	}

	@Override
	public <K> K getBusinessKey() {

		return null;
	}
}
