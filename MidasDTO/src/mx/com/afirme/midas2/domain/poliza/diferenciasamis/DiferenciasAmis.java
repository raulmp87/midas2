package mx.com.afirme.midas2.domain.poliza.diferenciasamis;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="toDiferenciasAmis" , schema="MIDAS")
public class DiferenciasAmis implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;

	private Long id;
	private BigDecimal idToPoliza;
	private BigDecimal idToCotizacion;
	private BigDecimal numeroInciso;
	private String numeroSerie;
	private String claveUnica;
	private String version;
	//inicia campos comparables
    private String marca;
    private String anos;
    private String puertas;
    private String cilindros;
    private String combustible;
    private String interiores;
    private String pasajeros;
    //termina campos comparables
    private Integer numeroDiferencias;
    private Date fechaCreacion;
	private String codigoUsuarioCreacion;
	private Short estatus;
	private int activo;
	private Integer claveError;
	private String mensajeError;
	
	//campos adicionales
	private Date fechaIni;
	private Date fechaFin;
	private Long agenteId;
	private BigDecimal numeroPoliza;
	private Long strPolizaControl;
	
	public static final int GRUPO_CILINDROS = 20;
	public static final int GRUPO_VESTIDURAS = 50;
	public static final int GRUPO_COMBUSTIBLE = 110;
	
	public static final Short ESTATUS_PENDIENTE_REVISION = 1;
	public static final Short ESTATUS_CAMBIO_SOLICITADO = 2;
	public static final Short ESTATUS_CANCELADA = 3;
	
	public static final int ERROR_VIN_INFORMACION_NO_COINCIDE = 1;
	public static final int ERROR_VIN_ALTERADO = 2;
	public static final int ERROR_VIN_NO_REGISTRADO = 3;
	public static final int ERROR_VIN_ERRONEO = 4;
	public static final int ERROR_VIN_INVALIDO = 5;	
	public static final int ERROR_VIN_NO_ENCONTRADO = 6;
	public static final int ERROR_VIN_LONGITUD_RESPUESTA = 99;
	
	public static final BigDecimal VALIDACION_VIN_INDIVIDUAL_ACTIVADA = new BigDecimal(131050);
	public static final BigDecimal VALIDACION_VIN_FLOTILLA_ACTIVADA = new BigDecimal(131060);
	public static final BigDecimal DIAS_VIN_FLOTILLA = new BigDecimal(131080);
	
	public static final int ACTIVO = 1;
	
	public static final String VIN_SIN_ERRORES  = "1";
    
    public DiferenciasAmis() {
        
	}
	
    @Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TODIFERENCIASAMIS_ID_GENERATOR")
	@SequenceGenerator(name="TODIFERENCIASAMIS_ID_GENERATOR", sequenceName="MIDAS.idToDiferenciasAmis_seq", allocationSize=1)
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	
	public Long getId() {
		return this.id;
	}	
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}
	
	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	@Column(name = "NUMEROSERIE")
	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	@Column(name = "CLAVEUNICA")
	public String getClaveUnica() {
		return claveUnica;
	}

	public void setClaveUnica(String claveUnica) {
		this.claveUnica = claveUnica;
	}
	
	@Column(name = "VERSION")
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Column(name = "MARCA")
	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	@Column(name = "ANOS")
	public String getAnos() {
		return anos;
	}

	public void setAnos(String anos) {
		this.anos = anos;
	}

	@Column(name = "PUERTAS")
	public String getPuertas() {
		return puertas;
	}

	public void setPuertas(String puertas) {
		this.puertas = puertas;
	}

	@Column(name = "CILINDROS")
	public String getCilindros() {
		return cilindros;
	}

	public void setCilindros(String cilindros) {
		this.cilindros = cilindros;
	}

	@Column(name = "COMBUSTIBLE")
	public String getCombustible() {
		return combustible;
	}

	public void setCombustible(String combustible) {
		this.combustible = combustible;
	}

	@Column(name = "INTERIORES")
	public String getInteriores() {
		return interiores;
	}

	public void setInteriores(String interiores) {
		this.interiores = interiores;
	}

	@Column(name = "PASAJEROS")
	public String getPasajeros() {
		return pasajeros;
	}

	public void setPasajeros(String pasajeros) {
		this.pasajeros = pasajeros;
	}

	@Transient
	public Integer getNumeroDiferencias() {
		return numeroDiferencias;
	}

	public void setNumeroDiferencias(Integer numeroDiferencias) {
		this.numeroDiferencias = numeroDiferencias;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHACREACION", nullable = false, length = 7)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Column(name = "CODIGOUSUARIOCREACION", nullable = false, length = 8)
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
	
	@Column(name = "ESTATUS")
	public Short getEstatus() {
		return estatus;
	}

	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}
	
	@Column(name = "ACTIVO")
	public int getActivo() {
		return activo;
	}

	public void setActivo(int activo) {
		this.activo = activo;
	}

	@Column(name = "CLAVEERROR")
	public Integer getClaveError() {
		return claveError;
	}

	public void setClaveError(Integer claveError) {
		this.claveError = claveError;
	}
	
	@Column(name = "MENSAJEERROR")
	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	@Transient
	public Date getFechaIni() {
		return fechaIni;
	}

	public void setFechaIni(Date fechaIni) {
		this.fechaIni = fechaIni;
	}

	@Transient
	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	@Transient
	public Long getAgenteId() {
		return agenteId;
	}

	public void setAgenteId(Long agenteId) {
		this.agenteId = agenteId;
	}

	@Transient
	public BigDecimal getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(BigDecimal numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	@Transient
	public Long getStrPolizaControl() {
		return strPolizaControl;
	}

	public void setStrPolizaControl(Long strPolizaControl) {
		this.strPolizaControl = strPolizaControl;
	}	
}
