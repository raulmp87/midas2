package mx.com.afirme.midas2.domain.poliza.renovacionmasiva;

import java.math.BigDecimal;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;


import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * OrdenRenovacionMasivaDet entity. @author Lizandro Perez
 */
@Entity
@Table(name = "TOORDENRENOVACIONMASIVADET", schema = "MIDAS")
public class OrdenRenovacionMasivaDet implements java.io.Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9123922408673180563L;

	private OrdenRenovacionMasivaDetId id;
	private PolizaDTO polizaAnterior;
	private BigDecimal idToPolizaRenovada;
	private PolizaDTO polizaRenovada;
	private BigDecimal idToCotizacion;
	private CotizacionDTO cotizacion;
	private Short claveEstatus;
	private Double descuentoNoSiniestro;
	private String mensaje;
	
	public static final Short ESTATUS_INICIADA = 0;
	public static final Short ESTATUS_PROCESANDO_COTIZACION = 1;
	public static final Short ESTATUS_FALLO_COT = 2;
	public static final Short ESTATUS_PROCESANDO_EMISION = 3;
	public static final Short ESTATUS_FALLO_EMISION = 4;
	public static final Short ESTATUS_TERMINADA = 5;
	public static final Short ESTATUS_CANCELADA = 6;
	
	
	public OrdenRenovacionMasivaDet(){
		this.cotizacion = new CotizacionDTO();
	}
	
	public String getDescripcionEstatus(){
		String descripcionEstatus = "";
		if(claveEstatus.equals(ESTATUS_INICIADA)){
			descripcionEstatus = "INICIADA";
		} else if(claveEstatus.equals(ESTATUS_PROCESANDO_COTIZACION)){
			descripcionEstatus = "PROCESANDO COTIZACION";
		} else if(claveEstatus.equals(ESTATUS_FALLO_COT)){
			descripcionEstatus = "FALLO COTIZACION";
		} else if(claveEstatus.equals(ESTATUS_PROCESANDO_EMISION)){
			descripcionEstatus = "PROCESANDO EMISION";
		} else if(claveEstatus.equals(ESTATUS_FALLO_EMISION)){
			descripcionEstatus = "FALLO EMISION";
		} else if(claveEstatus.equals(ESTATUS_TERMINADA)){
			descripcionEstatus = "TERMINADA";
		} else if(claveEstatus.equals(ESTATUS_CANCELADA)){
			descripcionEstatus = "CANCELADA";
		}
		return descripcionEstatus;
	}

	public void setId(OrdenRenovacionMasivaDetId id) {
		this.id = id;
	}

    @EmbeddedId
    @AttributeOverrides( {
        @AttributeOverride(name="idToOrdenRenovacion", column=@Column(name="IDTOORDENRENOVACION", nullable=false, precision = 22, scale = 0) ), 
        @AttributeOverride(name="idToPoliza", column=@Column(name="IDTOPOLIZA", nullable=false,  precision = 22, scale = 0) ) } )
	public OrdenRenovacionMasivaDetId getId() {
		return id;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public OrdenRenovacionMasivaDetId getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setPolizaAnterior(PolizaDTO polizaAnterior) {
		this.polizaAnterior = polizaAnterior;
	}

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOPOLIZA", nullable = false, insertable = false, updatable = false)
	public PolizaDTO getPolizaAnterior() {
		return polizaAnterior;
	}

	public void setIdToPolizaRenovada(BigDecimal idToPolizaRenovada) {
		this.idToPolizaRenovada = idToPolizaRenovada;
	}

	@Column(name="IDTOPOLIZARENOVADA", nullable = true, precision = 22, scale = 0)
	public BigDecimal getIdToPolizaRenovada() {
		return idToPolizaRenovada;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	@Column(name="IDTOCOTIZACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setCotizacion(CotizacionDTO cotizacion) {
		this.cotizacion = cotizacion;
	}

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOCOTIZACION", nullable = false, insertable = false, updatable = false)
	public CotizacionDTO getCotizacion() {
		return cotizacion;
	}

	public void setPolizaRenovada(PolizaDTO polizaRenovada) {
		this.polizaRenovada = polizaRenovada;
	}

	@Transient
	public PolizaDTO getPolizaRenovada() {
		return polizaRenovada;
	}

	public void setClaveEstatus(Short claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	@Column(name = "CLAVEESTATUS", nullable = false, precision = 4, scale = 0)
	public Short getClaveEstatus() {
		return claveEstatus;
	}

	public void setDescuentoNoSiniestro(Double descuentoNoSiniestro) {
		this.descuentoNoSiniestro = descuentoNoSiniestro;
	}

	@Column(name = "DESCUENTONOSINIESTRO", nullable = false, precision = 4, scale = 0)
	public Double getDescuentoNoSiniestro() {
		return descuentoNoSiniestro;
	}

	public void setMensaje(String mensaje) {
		if(mensaje != null && mensaje.length() > 500){
			mensaje = mensaje.substring(0, 499).trim();
		}
		this.mensaje = mensaje;
	}

	@Column(name="MENSAJE",length = 500)
	public String getMensaje() {
		return mensaje;
	}
	
}
