package mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dto.IdDynamicRow;

@Entity
@Table(name = "TODETALLEBITACORA", schema = "MIDAS")
public class DetalleBitacora implements java.io.Serializable, Entidad{

	private static final long serialVersionUID = -9123922408645180553L;

	@Id
	@SequenceGenerator(name = "TODETALLEBITACORA_SEQ", allocationSize = 1, sequenceName = "MIDAS.TODETALLEBITACORA_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TODETALLEBITACORA_SEQ")
	@Column(name = "IDTODETALLE", nullable = false)
	@IdDynamicRow
	private BigDecimal id;
	
	@Column(name = "IDTOBITACORA")
	private BigDecimal IdToBitacora;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA")
	private Date fecha;
	
	@Column(name = "ESTATUS")
	private String estatus;
	
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public BigDecimal getIdToBitacora() {
		return IdToBitacora;
	}

	public void setIdToBitacora(BigDecimal idToBitacora) {
		IdToBitacora = idToBitacora;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return getId();
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}
