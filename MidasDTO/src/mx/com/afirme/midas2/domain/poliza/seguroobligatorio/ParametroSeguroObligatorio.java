package mx.com.afirme.midas2.domain.poliza.seguroobligatorio;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.base.PaginadoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name = "TOPARAMETROSEGUROOBLIGATORIO", schema = "MIDAS")
public class ParametroSeguroObligatorio extends PaginadoDTO implements java.io.Serializable, Entidad{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8443323026568541689L;
	
	private BigDecimal id;
	private String descripcion;
	private Short habilitar;
	
	public static final BigDecimal PARAMETRO_SEGURO_OBLIGATORIO = new BigDecimal(0);
	public static final BigDecimal PARAMETRO_EMISION = new BigDecimal(1);
	public static final BigDecimal PARAMETRO_CARGA_MASIVA = new BigDecimal(2);
	public static final BigDecimal PARAMETRO_RENOVACION = new BigDecimal(3);
	public static final BigDecimal PARAMETRO_ALTA_INCISO = new BigDecimal(4);
	public static final BigDecimal PARAMETRO_CAMBIO_DATOS = new BigDecimal(5);
	public static final BigDecimal PARAMETRO_A_PETICION = new BigDecimal(6);
	

	public void setId(BigDecimal id) {
		this.id = id;
	}

	@Id
	@Column(name = "ID", nullable = false, precision = 22, scale = 0)
	public BigDecimal getId() {
		return id;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "DESCRIPCION", length = 400)
	public String getDescripcion() {
		return descripcion;
	}

	public void setHabilitar(Short habilitar) {
		this.habilitar = habilitar;
	}

	@Column(name = "HABILITAR", nullable = false, precision = 4, scale = 0)
	public Short getHabilitar() {
		return habilitar;
	}
	
	@Transient
	public Boolean  getHabilitado(){
		Short habilitado = 1;
		if(habilitar == null){
			return false;
		}else{
			if(habilitar.equals(habilitado)){
				return true;
			}else{
				return false;
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}
