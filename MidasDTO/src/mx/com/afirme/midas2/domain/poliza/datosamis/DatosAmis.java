package mx.com.afirme.midas2.domain.poliza.datosamis;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="toDatosAmis" , schema="MIDAS")
public class DatosAmis implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String descripcion;
	private int activo;	
	
    
    public DatosAmis() {
        
	}
	
    @Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TODATOSAMIS_ID_GENERATOR")
	@SequenceGenerator(name="TODATOSAMIS_ID_GENERATOR", sequenceName="MIDAS.idToDatosAmis_seq", allocationSize=1)
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	
	public Long getId() {
		return this.id;
	}	
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	@Column(name = "DESCRIPCION")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Column(name = "ACTIVO")
	public int getActivo() {
		return activo;
	}

	public void setActivo(int activo) {
		this.activo = activo;
	}
}
