package mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name = "TOREGISTROGUIASPROVEEDOR", schema = "MIDAS")
public class RegistroGuiasProveedor implements java.io.Serializable, Entidad{

	private static final long serialVersionUID = -9123922408673180552L;
	private RegistroGuiasProveedorId id;
	private Boolean procesado;
	
	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idGuia", column = @Column(name = "ID_GUIA", nullable = false)),
			@AttributeOverride(name = "idToPolizaRenovada", column = @Column(name = "IDTOPOLIZA", nullable = false)) })
	public RegistroGuiasProveedorId getId() {
		return id;
	}

	public void setId(RegistroGuiasProveedorId id) {
		this.id = id;
	}

	@Column(name="PROCESADO")
	public Boolean getProcesado() {
		return procesado;
	}

	public void setProcesado(Boolean procesado) {
		this.procesado = procesado;
	}

	@SuppressWarnings("unchecked")
	@Override
	public RegistroGuiasProveedorId getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}
