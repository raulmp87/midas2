package mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ReporteEfectividadEntrega implements Serializable {

	private static final long serialVersionUID = 4307646113374115190L;

	private BigDecimal idToBitacora;
	private BigDecimal idToPoliza;
	private BigDecimal idToOrdenRenovacion;
	private String numeroPolizaSeycos;
	private String numeroPolizaMidas;
	private String numeroPolizaSeycosRenovada;
	private String numeroPolizaMidasRenovada;
	private String contratante;
	private String titular;
	private String telefono;
	private String vehiculo;
	private BigDecimal modelo;
	private String serie;
	private BigDecimal primaTotalPoliza;
	private String conductoCobro;	
	private Date fechaEnvio;	
	private String guia;	
	private BigDecimal estatus;	
	private Date fechaEntrega;	
	private String recibe;	
	private String rechazo;
	
	public BigDecimal getIdToBitacora() {
		return idToBitacora;
	}

	public void setIdToBitacora(BigDecimal idToBitacora) {
		this.idToBitacora = idToBitacora;
	}

	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	public BigDecimal getIdToOrdenRenovacion() {
		return idToOrdenRenovacion;
	}

	public void setIdToOrdenRenovacion(BigDecimal idToOrdenRenovacion) {
		this.idToOrdenRenovacion = idToOrdenRenovacion;
	}

	public String getNumeroPolizaSeycos() {
		return numeroPolizaSeycos;
	}
	
	public void setNumeroPolizaSeycos(String numeroPolizaSeycos) {
		this.numeroPolizaSeycos = numeroPolizaSeycos;
	}
	
	public String getNumeroPolizaMidas() {
		return numeroPolizaMidas;
	}
	
	public void setNumeroPolizaMidas(String numeroPolizaMidas) {
		this.numeroPolizaMidas = numeroPolizaMidas;
	}
	
	public String getNumeroPolizaSeycosRenovada() {
		return numeroPolizaSeycosRenovada;
	}
	
	public void setNumeroPolizaSeycosRenovada(String numeroPolizaSeycosRenovada) {
		this.numeroPolizaSeycosRenovada = numeroPolizaSeycosRenovada;
	}
	
	public String getNumeroPolizaMidasRenovada() {
		return numeroPolizaMidasRenovada;
	}
	
	public void setNumeroPolizaMidasRenovada(String numeroPolizaMidasRenovada) {
		this.numeroPolizaMidasRenovada = numeroPolizaMidasRenovada;
	}
	
	public String getContratante() {
		return contratante;
	}
	
	public void setContratante(String contratante) {
		this.contratante = contratante;
	}
	
	public String getTitular() {
		return titular;
	}
	
	public void setTitular(String titular) {
		this.titular = titular;
	}
	
	public String getTelefono() {
		return telefono;
	}
	
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	public String getVehiculo() {
		return vehiculo;
	}
	
	public void setVehiculo(String vehiculo) {
		this.vehiculo = vehiculo;
	}
	
	public BigDecimal getModelo() {
		return modelo;
	}

	public void setModelo(BigDecimal modelo) {
		this.modelo = modelo;
	}

	public String getSerie() {
		return serie;
	}
	
	public void setSerie(String serie) {
		this.serie = serie;
	}
	
	public BigDecimal getPrimaTotalPoliza() {
		return primaTotalPoliza;
	}
	
	public void setPrimaTotalPoliza(BigDecimal primaTotalPoliza) {
		this.primaTotalPoliza = primaTotalPoliza;
	}
	
	public String getConductoCobro() {
		return conductoCobro;
	}
	
	public void setConductoCobro(String conductoCobro) {
		this.conductoCobro = conductoCobro;
	}
	
	public Date getFechaEnvio() {
		return fechaEnvio;
	}
	
	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}
	
	public String getGuia() {
		return guia;
	}
	
	public void setGuia(String guia) {
		this.guia = guia;
	}
	
	public BigDecimal getEstatus() {
		return estatus;
	}
	
	public void setEstatus(BigDecimal estatus) {
		this.estatus = estatus;
	}
	
	public Date getFechaEntrega() {
		return fechaEntrega;
	}
	
	public void setFechaEntrega(Date fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}
	
	public String getRecibe() {
		return recibe;
	}
	
	public void setRecibe(String recibe) {
		this.recibe = recibe;
	}
	
	public String getRechazo() {
		return rechazo;
	}
	
	public void setRechazo(String rechazo) {
		this.rechazo = rechazo;
	}	
}
