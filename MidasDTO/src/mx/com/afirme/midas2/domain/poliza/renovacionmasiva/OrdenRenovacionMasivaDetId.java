package mx.com.afirme.midas2.domain.poliza.renovacionmasiva;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * OrderRenovacionMasivaDetId entity. @author Lizandro Perez
 */
@Embeddable
public class OrdenRenovacionMasivaDetId implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3623990640792394127L;
	
	private BigDecimal idToOrdenRenovacion;
	private BigDecimal idToPoliza;
	
	public OrdenRenovacionMasivaDetId(){
		
	}
	
	public OrdenRenovacionMasivaDetId(BigDecimal idToOrdenRenovacion, BigDecimal idToPoliza){
		this.idToOrdenRenovacion = idToOrdenRenovacion;
		this.idToPoliza = idToPoliza;
	}
	
	public void setIdToOrdenRenovacion(BigDecimal idToOrdenRenovacion) {
		this.idToOrdenRenovacion = idToOrdenRenovacion;
	}
	
	@Column(name="IDTOORDENRENOVACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToOrdenRenovacion() {
		return idToOrdenRenovacion;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	@Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}

}
