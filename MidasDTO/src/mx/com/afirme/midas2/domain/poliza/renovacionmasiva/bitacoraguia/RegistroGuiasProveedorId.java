package mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class RegistroGuiasProveedorId   implements Serializable{
	private static final long serialVersionUID = -9123922408673180534L;
	private String idGuia;
	private String idToPolizaRenovada;
	
	public RegistroGuiasProveedorId() {
    }

    
    /** full constructor */
    public RegistroGuiasProveedorId(String idGuia, String idToPolizaRenovada) {
        this.idGuia = idGuia;
        this.idToPolizaRenovada = idToPolizaRenovada;
    }
    
	@Column(name="ID_GUIA", nullable = false)
	public String getIdGuia() {
		return idGuia;
	}

	public void setIdGuia(String idGuia) {
		this.idGuia = idGuia;
	}
	
	@Column(name="IDTOPOLIZA", nullable = false)
	public String getIdToPolizaRenovada() {
		return idToPolizaRenovada;
	}

	public void setIdToPolizaRenovada(String idToPolizaRenovada) {
		this.idToPolizaRenovada = idToPolizaRenovada;
	}

}
