package mx.com.afirme.midas2.domain.poliza.seguroobligatorio;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import mx.com.afirme.midas.base.PaginadoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name = "TOPOLIZAANEXA", schema = "MIDAS")
public class PolizaAnexa extends PaginadoDTO implements java.io.Serializable, Entidad{

	/**
	 * 
	 */
	private static final long serialVersionUID = 488180169579111373L;
	
	private PolizaAnexaId id;	
	private PolizaDTO polizaAnexaDTO;
	private String origen;
	
	public static final String ORIGEN_MIDAS = "M";
	public static final String ORIGEN_SEYCOS = "S";
	
	public static final Short TIPO_AUTOMATICA = 1;
	public static final Short TIPO_PETICION = 2;
	
	public void setId(PolizaAnexaId id) {
		this.id = id;
	}

    @EmbeddedId
    @AttributeOverrides( {
    	@AttributeOverride(name="idToPoliza", column=@Column(name="IDTOPOLIZA", nullable=false,  precision = 22, scale = 0)),
        @AttributeOverride(name="idToPolizaAnexa", column=@Column(name="IDTOPOLIZAANEXA", nullable=false, precision = 22, scale = 0) ) 
          } )
	public PolizaAnexaId getId() {
		return id;
	}	
	
	public void setPolizaAnexaDTO(PolizaDTO polizaAnexaDTO) {
		this.polizaAnexaDTO = polizaAnexaDTO;
	}

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOPOLIZAANEXA", nullable = false, insertable = false, updatable = false)
	public PolizaDTO getPolizaAnexaDTO() {
		return polizaAnexaDTO;
	}

	@SuppressWarnings("unchecked")
	@Override
	public PolizaAnexaId getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	@Column(name="ORIGEN", nullable = true)
	public String getOrigen() {
		return origen;
	}



}
