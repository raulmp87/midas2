package mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dto.IdDynamicRow;

@Entity
@Table(name = "TOBITACORAGUIAS", schema = "MIDAS")
public class BitacoraGuia implements java.io.Serializable, Entidad {
	
	private static final long serialVersionUID = -9123922408673180553L;
	//Constants
	public static enum estatusGuia{
		PROCESADA(1),
		CONFIRMADO(2),
		EN_TRANSITO(3),
		DEVOLUCION(4);
		
		private Integer valor;
		
		estatusGuia(Integer id){
			valor = id;
		}
		
		public Integer valor(){
			return valor;
		}		
	}
	
	public static final String BUSQUEDA_LIST = "L";
	public static final String BUSQUEDA_RANGO = "R";
	public static final String TIPO_BUSQUEDA_GUIA = "G";
	public static final String TIPO_BUSQUEDA_RASTREO = "R";
	
	public static enum TipoHistorial{
		ALL("ALL"),
		ONLY_EXCEPTIONS("ONLY_EXCEPTIONS"),
		LAST_EVENT("LAST_EVENT");
		
		private String valor;
		
		TipoHistorial(String id){
			valor = id;
		}
		
		public String valor(){
			return valor;
		}		
	}
	
	@Id
	@SequenceGenerator(name = "TOBITACORAGUIAS_SEQ", allocationSize = 1, sequenceName = "MIDAS.TOBITACORAGUIAS_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TOBITACORAGUIAS_SEQ")
	@Column(name = "IDTOBITACORA", nullable = false)
	@IdDynamicRow
	private BigDecimal id;

	@Column(name="ID_GUIA", nullable = false)
	private String idGuia;
	
	@Column(name = "IDTOPOLIZA")
	private BigDecimal idToPoliza;
	
	@Column(name = "IDTOORDENRENOVACION")
	private BigDecimal idToOrdenRenovacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA")
	private Date fecha;
	
	@Column(name = "ESTATUS")
	private Integer estatus;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAENTREGA")
	private Date fechaEntrega;

	@Column(name = "RECIBE")
	private String recibe;

	@Column(name = "MOTIVORECHAZO")
	private String motivoRechazo;

	@Transient
	private List<DetalleBitacora> detalleBitacora;
	
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	
	public String getIdGuia() {
		return idGuia;
	}
	
	public void setIdGuia(String idGuia) {
		this.idGuia = idGuia;
	}
	
	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}
	
	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}
	
	public BigDecimal getIdToOrdenRenovacion() {
		return idToOrdenRenovacion;
	}
	
	public void setIdToOrdenRenovacion(BigDecimal idToOrdenRenovacion) {
		this.idToOrdenRenovacion = idToOrdenRenovacion;
	}
	
	public Date getFecha() {
		return fecha;
	}
	
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Integer getEstatus() {
		return estatus;
	}
	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}
	
	public Date getFechaEntrega() {
		return fechaEntrega;
	}
	
	public void setFechaEntrega(Date fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}
	
	public String getRecibe() {
		return recibe;
	}
	
	public void setRecibe(String recibe) {
		this.recibe = recibe;
	}
	
	public String getMotivoRechazo() {
		return motivoRechazo;
	}
	
	public void setMotivoRechazo(String motivoRechazo) {
		this.motivoRechazo = motivoRechazo;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return getId();
	}
	
	public List<DetalleBitacora> getDetalleBitacora() {
		return detalleBitacora;
	}
	
	public void setDetalleBitacora(List<DetalleBitacora> detalleBitacora) {
		this.detalleBitacora = detalleBitacora;
	}
	@Override
	public String getValue() {
		return null;
	}
	@Override
	public <K> K getBusinessKey() {
		return null;
	}
	
}
