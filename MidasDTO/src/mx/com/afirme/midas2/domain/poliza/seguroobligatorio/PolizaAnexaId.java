package mx.com.afirme.midas2.domain.poliza.seguroobligatorio;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PolizaAnexaId implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3006236393314701950L;
	
	private BigDecimal idToPoliza;
	private BigDecimal idToPolizaAnexa;
	
	public PolizaAnexaId(){
		
	}
	
	public PolizaAnexaId(BigDecimal idToPoliza, BigDecimal idToPolizaAnexa){
		this.idToPoliza = idToPoliza;
		this.idToPolizaAnexa = idToPolizaAnexa;
	}	
	
	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}
	
	@Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}
	
	public void setIdToPolizaAnexa(BigDecimal idToPolizaAnexa) {
		this.idToPolizaAnexa = idToPolizaAnexa;
	}

	@Column(name="IDTOPOLIZAANEXA", nullable = true, precision = 22, scale = 0)
	public BigDecimal getIdToPolizaAnexa() {
		return idToPolizaAnexa;
	}
	
}
