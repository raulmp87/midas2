package mx.com.afirme.midas2.domain.poliza.renovacionmasiva;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.base.PaginadoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * OrdenRenovacionMasiva entity. @author Lizandro Perez
 */
@Entity
@Table(name = "TOORDENRENOVACIONMASIVA", schema = "MIDAS")
public class OrdenRenovacionMasiva extends PaginadoDTO implements java.io.Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9123922408673180563L;
	
	public static enum AccionRenovacion{
		RENOVAR_EMITIR((short)1), RENOVAR_COTIZAR((short)2), RENOVAR_NUEVO_NEGOCIO((short)3);		
		AccionRenovacion(Short accion) {
			this.accion = accion;
		}
		private Short accion;
		public Short obtenerAccion(){
			return this.accion;
		}
		public Short getObtenerAccion(){
			return this.obtenerAccion();
		}
		
	};	

	private BigDecimal idToOrdenRenovacion;
	private Date fechaCreacion;
	private String nombreUsuarioCreacion;
	private String codigoUsuarioCreacion;
	private BigDecimal numeroPolizas;
	private Short claveEstatus;
	private Date fechaModificacion;
	private String codigoUsuarioModificacion;
	private Short accionRenovacion;
	private Short enviado;
	
	private Date fechaCreacionHasta;
	private Date fechaModificacionHasta;
	private Integer numeroPoliza;
	
	public static final Short ESTATUS_INICIADA = 0;
	public static final Short ESTATUS_CONTROL_USUARIO = 1;
	public static final Short ESTATUS_TERMINADA  = 2;
	public static final Short ESTATUS_CANCELADA = 3;
	
	public static final int SIT_POLIZA = 0;
	public static final int ID_COTIZACION = 1;
	public static final int ID_VERSION_POL = 2;
	public static final int ID_CONTRATANTE = 3;
	public static final int ID_PRODUCTO = 4;
	public static final int CVE_T_POLIZA = 5;
	public static final int F_INI_VIGENCIA = 6;
	public static final int F_FIN_VIGENCIA = 7;
	public static final int NUM_COTIZACION = 8;
	public static final int NUM_POLIZA = 9;
	public static final int NUM_POLIZA_ANT = 10;
	public static final int ID_USUARIO_EMIS = 11;
	public static final int NOM_SOLICITANTE = 12;
	public static final int ID_CENTRO_EMISRG = 13;
	public static final int ID_EMPRESA = 14;
	//public static final int ID_CENTRO_EMIS = 15;
	public static final int ID_USUARIO_CREAC = 15;
	public static final int ID_BITACORA = 16; 
	public static final int ID_MONEDA = 17;
	public static final int CVE_T_POLIZA_1 = 18;
	public static final int ID_USUARIO_AUTOR = 19;
	public static final int IMP_PRIMA_TOTAL = 20;
	public static final int FH_CREACION = 21; 
	public static final int IMP_PRIMA_ANT = 22;
	public static final int INCREMENTO = 23;
	public static final int NOM_LINEA_NEG = 24;
	public static final int PAQUETE = 25; 
	public static final int LINEA_ANTERIOR = 26;
	public static final int ORIGEN = 27;
	public static final int RENOVACION = 28;
	public static final int NUM_SIN_PREVIO = 29;
	public static final int NUM_SIN_TOTAL = 30; 
	public static final int DESCUENTO = 31;
	public static final int DESCUENTO_POR_ESTADO = 32;
	public static final int ESTILO = 33;
	public static final int DESC_VEHIC = 34;
	public static final int MODELO = 35;
	public static final int ESTADO = 36;
	//public static final int CLASE = 37;
	public static final int DEDUCIBLE_RT = 37;
	public static final int ESTATUS_ORD_RENOV = 38;
	
	public static final int CLAVEAGENTE = 39;
	public static final int NOMBRE_AGENTE = 40;
	public static final int NOMBRE_NEGOCIO_POLIZA_ANTERIOR = 41;
	public static final int NOMBRE_NEGOCIO_POLIZA_RENOVAR = 42;
	
	//public static final int DOMICILIO_CONTRATANTE = 42;
	public static final int DOMICILIO_CALLE_NUMERO = 43;
	public static final int DOMICILIO_COLONIA = 44;
	public static final int DOMICILIO_CODIGO_POSTAL = 45;
	public static final int DOMICILIO_CIUDAD = 46;
	public static final int DOMICILIO_ESTADO = 47;
	public static final int FORMA_PAGO = 48;
	public static final int CONDUCTO_COBRO = 49;
	public static final int NOMBRE_TITULAR_CONDUCTO_COBRO = 50;
	public static final int TELEFONO_CASA = 51;
	public static final int TELEFONO_OFICINA = 52;
	public static final int NUM_POLIZA_SEYCOS_INT = 53;
	public static final int DEDUCIBLE_DANOS_MATERIALES_INT_1 = 54;
	public static final int DEDUCIBLE_DANOS_MATERIALES_INT_2 = 55;
	public static final int PORCENTAJE_IVA = 56;
	
	public static final int TOTAL_PRIMAS = 57;
	public static final int DESCUENTO_COMIS_CEDIDA = 58;
	public static final int PRIMA_NETA = 59;
	public static final int RECARGO = 60;
	public static final int DERECHOS = 61;
	public static final int IVA = 62;
	//public static final int TIPO_RIESGO = 47;
	public static final int ENVIADO = 63;
	
	public static final int GUIA = 64;
	public static final int DESTATUS = 65;
	public static final int FECHA_ESTATUS = 66;
	public static final int FECHA_ENTREGA = 67;
	public static final int PERSONA_RECIBE = 68;
	public static final int DESC_NO_ENTREGA = 69;
	public static final int CONTRATO = 70;
	public static final int SIT_CONTRATO = 71;
	public static final int FECHA_INI_CONTRATO = 72;
	public static final int FECHA_FIN_CONTRATO = 73;
	
	//Excel de Control
	public static final int NUM_POLIZA_SEYCOS = 0;
	public static final int NUM_POLIZA_MIDAS = 1;
	public static final int NUM_POLIZA_SEYCOS_NUEVA = 2;
	public static final int NUM_POLIZA_MIDAS_NUEVA = 3;
	public static final int CONTRATANTE = 4;
	public static final int TITULAR = 5;
	public static final int CALLE_NUMERO = 6;
	public static final int COLONIA = 7;
	public static final int CODIGO_POSTAL = 8;
	public static final int CIUDAD_P = 9;
	public static final int ESTADO_P = 10;
	public static final int TELEFONO_P = 11;
	public static final int DESCRIPCION_VEHICULO = 12;
	public static final int MODELO_VEHICULO = 13;
	public static final int SERIE = 14;
	public static final int PRIMA_TOTAL_POLIZA = 15;
	public static final int DEDUCIBLE_DANOS_MATERIALES = 16;
	public static final int IMPORTE_PRIMER_RECIBO = 17;
	public static final int DOMICILIO_TX = 18;
	public static final int CONDUCTO_COBRO_P = 19;
	public static final int FECHA_DE_ENVIO = 20;
	public static final int ID_GUIA = 21;
	public static final int ESTATUS_GUIA = 22;
	public static final int FECHA_DE_ENTREGA = 23;
	public static final int RECIBE_GUIA = 24;
	public static final int RECHAZO_GUIA = 25;
	public static final int DEDUCIBLE_DANOS_MATERIALES_BIT = 99;
	
	
	
	private List<OrdenRenovacionMasivaDet> ordenRenovacionMasivaDetList = new ArrayList<OrdenRenovacionMasivaDet>(1);
	
	public OrdenRenovacionMasiva(){
		
	}
	
	public String getDescripcionEstatus(){
		String descripcionEstatus = "";
		if(claveEstatus.equals(ESTATUS_INICIADA)){
			descripcionEstatus = "INICIADA";
		} else if(claveEstatus.equals(ESTATUS_CONTROL_USUARIO)){
			descripcionEstatus = "CONTROL USUARIO";
		} else if(claveEstatus.equals(ESTATUS_TERMINADA)){
			descripcionEstatus = "TERMINADA";
		} else if(claveEstatus.equals(ESTATUS_CANCELADA)){
			descripcionEstatus = "CANCELADA";
		}
		return descripcionEstatus;
	}
	
	public void setIdToOrdenRenovacion(BigDecimal idToOrdenRenovacion) {
		this.idToOrdenRenovacion = idToOrdenRenovacion;
	}
	
	// Property accessors
	@Id
	@SequenceGenerator(name = "IDTOORDENRENOVACION_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.IDTOORDENRENOVACION_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOORDENRENOVACION_SEQ_GENERADOR")	  	
	@Column(name = "IDTOORDENRENOVACION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToOrdenRenovacion() {
		return idToOrdenRenovacion;
	}
	
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHACREACION", nullable = false, length = 7)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	
	public void setNombreUsuarioCreacion(String nombreUsuarioCreacion) {
		this.nombreUsuarioCreacion = nombreUsuarioCreacion;
	}
	
	@Column(name = "NOMBREUSUARIOCREACION", nullable = false, length = 200)
	public String getNombreUsuarioCreacion() {
		return nombreUsuarioCreacion;
	}
	
	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
	
	@Column(name = "CODIGOUSUARIOCREACION", nullable = false, length = 8)
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}
	
	public void setNumeroPolizas(BigDecimal numeroPolizas) {
		this.numeroPolizas = numeroPolizas;
	}
	
	@Column(name = "NUMEROPOLIZAS", nullable = false, precision = 8, scale = 0)
	public BigDecimal getNumeroPolizas() {
		return numeroPolizas;
	}
	
	public void setClaveEstatus(Short claveEstatus) {
		this.claveEstatus = claveEstatus;
	}
	
	@Column(name = "CLAVEESTATUS", nullable = false, precision = 4, scale = 0)
	public Short getClaveEstatus() {
		return claveEstatus;
	}
	
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAMODIFICACION", nullable = false, length = 7)
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	
	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}
	
	@Column(name = "CODIGOUSUARIOMODIFICACION", nullable = false, length = 8)
	public String getCodigoUsuarioModificacion() {
		return codigoUsuarioModificacion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.getIdToOrdenRenovacion();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setFechaCreacionHasta(Date fechaCreacionHasta) {
		this.fechaCreacionHasta = fechaCreacionHasta;
	}

	@Transient
	public Date getFechaCreacionHasta() {
		return fechaCreacionHasta;
	}

	public void setFechaModificacionHasta(Date fechaModificacionHasta) {
		this.fechaModificacionHasta = fechaModificacionHasta;
	}

	@Transient
	public Date getFechaModificacionHasta() {
		return fechaModificacionHasta;
	}

	public void setOrdenRenovacionMasivaDetList(
			List<OrdenRenovacionMasivaDet> ordenRenovacionMasivaDetList) {
		this.ordenRenovacionMasivaDetList = ordenRenovacionMasivaDetList;
	}

	@Transient
	public List<OrdenRenovacionMasivaDet> getOrdenRenovacionMasivaDetList() {
		return ordenRenovacionMasivaDetList;
	}

	public void setAccionRenovacion(Short accionRenovacion) {
		this.accionRenovacion = accionRenovacion;
	}

	@Column(name = "ACCIONRENOVACION", nullable = false, precision = 4, scale = 0)
	public Short getAccionRenovacion() {
		return accionRenovacion;
	}

	public void setEnviado(Short enviado) {
		this.enviado = enviado;
	}

	@Column(name = "ENVIADO", nullable = false, precision = 4, scale = 0)
	public Short getEnviado() {
		return enviado;
	}

	public void setNumeroPoliza(Integer numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	@Transient
	public Integer getNumeroPoliza() {
		return numeroPoliza;
	}
	
	
}
