package mx.com.afirme.midas2.domain.cobranza;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Recibos implements Serializable{
	
	/**
	 * objeto, homologado al cursor
	 */
	private static final long serialVersionUID = 7893079105752477398L;
	private Integer idCotizacion;
	private String  contratante;
	private Integer idRecibo;
	private Integer numRecibo;
	private Date    vencimiento;
	private Integer  folioFiscal;
	private String  serieFiscal;
	private BigDecimal  importe;
	private String  llaveComprobante;
	private String  archivoXml;
	private String  archivoPdf;
	private String  email;
	private String  poliza;
	private Integer endoso;
	private Date    finivigencia;
	private Date    ffinvigencia;
	private String  ramo;
	
	
	
	public Integer getIdCotizacion() {
		return idCotizacion;
	}
	public void setIdCotizacion(Integer idCotizacion) {
		this.idCotizacion = idCotizacion;
	}
	
	public String getContratante() {
		return contratante;
	}
	public void setContratante(String contratante) {
		this.contratante = contratante;
	}
	public Integer getIdRecibo() {
		return idRecibo;
	}
	public void setIdRecibo(Integer idRecibo) {
		this.idRecibo = idRecibo;
	}
	public Integer getNumRecibo() {
		return numRecibo;
	}
	public void setNumRecibo(Integer numRecibo) {
		this.numRecibo = numRecibo;
	}
	public Date getVencimiento() {
		return vencimiento;
	}
	public void setVencimiento(Date vencimiento) {
		this.vencimiento = vencimiento;
	}

	public Integer getFolioFiscal() {
		return folioFiscal;
	}
	public void setFolioFiscal(Integer folioFiscal) {
		this.folioFiscal = folioFiscal;
	}
	public String getSerieFiscal() {
		return serieFiscal;
	}
 
	public BigDecimal getImporte() {
		return importe;
	}
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}
	public void setSerieFiscal(String serieFiscal) {
		this.serieFiscal = serieFiscal;
	}
	public String getLlaveComprobante() {
		return llaveComprobante;
	}
	public void setLlaveComprobante(String llaveComprobante) {
		this.llaveComprobante = llaveComprobante;
	}
	public String getArchivoXml() {
		return archivoXml;
	}
	public void setArchivoXml(String archivoXml) {
		this.archivoXml = archivoXml;
	}
	public String getArchivoPdf() {
		return archivoPdf;
	}
	public void setArchivoPdf(String archivoPdf) {
		this.archivoPdf = archivoPdf;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPoliza() {
		return poliza;
	}
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	public String getRamo() {
		return ramo;
	}
	public void setRamo(String ramo) {
		this.ramo = ramo;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Integer getEndoso() {
		return endoso;
	}
	public void setEndoso(Integer endoso) {
		this.endoso = endoso;
	}
	public Date getFinivigencia() {
		return finivigencia;
	}
	public void setFinivigencia(Date finivigencia) {
		this.finivigencia = finivigencia;
	}
	public Date getFfinvigencia() {
		return ffinvigencia;
	}
	public void setFfinvigencia(Date ffinvigencia) {
		this.ffinvigencia = ffinvigencia;
	}
 
	
}
	
	
