package mx.com.afirme.midas2.domain.cobranza.pagos;

import java.util.List;

public class RespuestaPagoDTO {
	
	private String poliza;
	private String reciboLlaveFiscal;
	private boolean pagado;
	private List<String> correos;
	
	public RespuestaPagoDTO(){ }
	
	public RespuestaPagoDTO(String reciboLlaveFiscal, List<String> correos, String poliza){
		this.reciboLlaveFiscal = reciboLlaveFiscal;
		this.correos = correos;
		this.poliza = poliza;		
	}

	public String getPoliza() {
		return poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	public String getReciboLlaveFiscal() {
		return reciboLlaveFiscal;
	}

	public void setReciboLlaveFiscal(String reciboLlaveFiscal) {
		this.reciboLlaveFiscal = reciboLlaveFiscal;
	}

	public boolean isPagado() {
		return pagado;
	}

	public void setPagado(boolean pagado) {
		this.pagado = pagado;
	}

	public List<String> getCorreos() {
		return correos;
	}

	public void setCorreos(List<String> correos) {
		this.correos = correos;
	}
	
}