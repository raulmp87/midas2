package mx.com.afirme.midas2.domain.cobranza.recibos.consolidar;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.com.afirme.midas.base.LogBaseDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name = "CONSOLIDADO", schema = "SEYCOS")
public class Consolidado extends LogBaseDTO  implements java.io.Serializable, Entidad {

	private BigDecimal idMedioPago;
	private static final long serialVersionUID = 1L;
	private Long referenciacargoconsolidado;
	private Double importecargoconsolidado;
	private Date fechaconsolidado;
	private Date fechalimite;
	private Date fechacorte;
	private Date fechacreacion;
	private String fechaconsolidadostr;
	private String fechalimitestr;
	private String fechacortestr;
	private String fechacreacionstr;
	private String foliofiscal = "";
	private String poliza = "";
	private String usuariocreacion = "";
	private String estatus = "";
	private String estatusdesc = "";
	private Long numTarjeta;
	
	private Long totalCount;
	private Integer posStart;
	private Integer count;
	private String orderByAttribute;
	private String filterForm;
	private String reporteExcel;
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	private String fechaIncorrecta= "Fecha incorrecta";
	private static final Logger log = LoggerFactory.getLogger(Consolidado.class);
	/**
	 * @return the referenciacargoconsolidado
	 */

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_REF_CARGO_CONS_AUTOM")
	@SequenceGenerator(name = "SEQ_REF_CARGO_CONS_AUTOM", schema = "SEYCOS", sequenceName = "SEQ_REF_CARGO_CONS_AUTOM", allocationSize = 1)
	@Column(name = "REF_CARGO_CONS", unique = true, nullable = false, precision = 10, scale = 0)
	public Long getReferenciacargoconsolidado() {
		return referenciacargoconsolidado;
	}

	/**
	 * @param referenciacargoconsolidado
	 *            the referenciacargoconsolidado to set  
	 */
	public void setReferenciacargoconsolidado(Long referenciacargoconsolidado) {
		this.referenciacargoconsolidado = referenciacargoconsolidado;
	}

	/**
	 * @return the importecargoconsolidado
	 */
	@Column(name = "IMP_CARGO_CONS", precision = 0)
	public Double getImportecargoconsolidado() {
		return importecargoconsolidado;
	}

	/**
	 * @param importecargoconsolidado
	 *            the importecargoconsolidado to set
	 */
	public void setImportecargoconsolidado(Double importecargoconsolidado) {
		this.importecargoconsolidado = importecargoconsolidado;
	}

	/**
	 * @return the fechalimite
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "FH_LIMITE", length = 7)
	public Date getFechalimite() {
		return fechalimite;
	}

	/**
	 * @param fechalimite
	 *            the fechalimite to set
	 */
	public void setFechalimite(Date fechalimite) {
		this.fechalimite = fechalimite;
		try {
			this.setFechalimitestr(sdf.format(fechalimite ));
		} catch (Exception e) {
			log.error(fechaIncorrecta, e);
			this.setFechalimitestr(fechaIncorrecta);
		}
		
	}

	/**
	 * @return the fechaconsolidado
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "FH_CONSOLIDADO", length = 7)
	public Date getFechaconsolidado() {
		return fechaconsolidado;
	}

	/**
	 * @param fechaconsolidado
	 *            the fechaconsolidado to set
	 */
	public void setFechaconsolidado(Date fechaconsolidado) {
		this.fechaconsolidado = fechaconsolidado;
	
		try {
			this.setFechaconsolidadostr(sdf.format(fechaconsolidado ));
		} catch (Exception e) {
			log.error(fechaIncorrecta, e);
			this.setFechaconsolidadostr(fechaIncorrecta);
		}
	}

	/**
	 * @return the fechacorte
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "FH_CORTE", length = 7)
	public Date getFechacorte() {
		return fechacorte;
	}

	/**
	 * @param fechacorte
	 *            the fechacorte to set
	 */
	public void setFechacorte(Date fechacorte) {
		this.fechacorte = fechacorte;
	
		try {
			this.setFechacortestr(sdf.format(fechacorte ));
		} catch (Exception e) {
			log.error(fechaIncorrecta, e);
			this.setFechacortestr(fechaIncorrecta);
		}
	}

	/**
	 * @return the fechacreacion
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "FH_CREACION", length = 7)
	public Date getFechacreacion() {
		return fechacreacion;
	}

	/**
	 * @param fechacreacion
	 *            the fechacreacion to set
	 */
	public void setFechacreacion(Date fechacreacion) {
		this.fechacreacion = fechacreacion;
		
		try {
			this.setFechacreacionstr(sdf.format(fechacreacion ));  
		} catch (Exception e) {
			log.error(fechaIncorrecta, e);
			this.setFechacreacionstr(fechaIncorrecta);
		}
	}

	/**
	 * @return the foliofiscal
	 */
	@Column(name = "FOLIO_FISCAL", precision = 10, scale = 0)
	public String getFoliofiscal() {
		return foliofiscal;
	}

	/**
	 * @param foliofiscal
	 *            the foliofiscal to set
	 */
	public void setFoliofiscal(String foliofiscal) {
		this.foliofiscal = foliofiscal;
	}

	/**
	 * @return the poliza
	 */
	@Column(name = "POLIZA", precision = 10, scale = 0)
	public String getPoliza() {
		return poliza;
	}

	/**
	 * @param poliza
	 *            the poliza to set
	 */
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	/**
	 * @return the usuariocreacion
	 */
	@Column(name = "USUARIO_CREACION", precision = 10, scale = 0)
	public String getUsuariocreacion() {
		return usuariocreacion;
	}

	/**
	 * @param usuariocreacion
	 *            the usuariocreacion to set
	 */
	public void setUsuariocreacion(String usuariocreacion) {
		this.usuariocreacion = usuariocreacion;
	}

	/**
	 * @return the estatus
	 */

	@Column(name = "ESTATUS", precision = 10, scale = 0)
	public String getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus
	 *            the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
		
		if("C".equalsIgnoreCase(this.estatus)){
			this.setEstatusdesc("Cancelada");
		}else if("N".equalsIgnoreCase(this.estatus)){
			this.setEstatusdesc("Nueva");
		}else {
			this.setEstatusdesc("Sin Estatus");
		}
		
	}

	
	
	/**
	 * @return the fechaconsolidadostr
	 */
	@Transient
	public String getFechaconsolidadostr() {
		return fechaconsolidadostr;
	}

	/**
	 * @param fechaconsolidadostr the fechaconsolidadostr to set
	 */
	public void setFechaconsolidadostr(String fechaconsolidadostr) {
		this.fechaconsolidadostr = fechaconsolidadostr;
	}

	/**
	 * @return the fechalimitestr
	 */
	@Transient
	public String getFechalimitestr() {
		return fechalimitestr;
	}

	/**
	 * @param fechalimitestr the fechalimitestr to set
	 */
	public void setFechalimitestr(String fechalimitestr) {
		this.fechalimitestr = fechalimitestr;
	}

	/**
	 * @return the fechacortestr
	 */
	@Transient
	public String getFechacortestr() {
		return fechacortestr;
	}

	/**
	 * @param fechacortestr the fechacortestr to set
	 */
	public void setFechacortestr(String fechacortestr) {
		this.fechacortestr = fechacortestr;
	}

	/**
	 * @return the fechacreacionstr
	 */
	@Transient
	public String getFechacreacionstr() {
		return fechacreacionstr;
	}

	/**
	 * @param fechacreacionstr the fechacreacionstr to set
	 */
	public void setFechacreacionstr(String fechacreacionstr) {
		this.fechacreacionstr = fechacreacionstr;
	}

	/**
	 * @return the estatusdesc
	 */
	@Transient
	public String getEstatusdesc() {
		return estatusdesc;
	}

	/**
	 * @param estatusdesc the estatusdesc to set
	 */
	public void setEstatusdesc(String estatusdesc) {
		this.estatusdesc = estatusdesc;
	}
	

	/**
	 * @return the idMedioPago
	 */
	@Transient
	public BigDecimal getIdMedioPago() {
		return idMedioPago;
	}

	/**
	 * @param idMedioPago the idMedioPago to set
	 */
	public void setIdMedioPago(BigDecimal idMedioPago) {
		this.idMedioPago = idMedioPago;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the numTarjeta
	 */
	@Column(name = "NUM_TARJETA", precision = 0)
	public Long getNumTarjeta() {
		return numTarjeta;
	}

	/**
	 * @return the totalCount
	 */
	@Transient
	public Long getTotalCount() {
		return totalCount;
	}

	/**
	 * @param totalCount the totalCount to set
	 */
	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}



	/**
	 * @return the count
	 */
	@Transient
	public Integer getCount() {
		return count;
	}

	/**
	 * @param count the count to set
	 */
	public void setCount(Integer count) {
		this.count = count;
	}

	/**
	 * @return the orderByAttribute
	 */
	@Transient
	public String getOrderByAttribute() {
		return orderByAttribute;
	}

	/**
	 * @param orderByAttribute the orderByAttribute to set
	 */
	public void setOrderByAttribute(String orderByAttribute) {
		this.orderByAttribute = orderByAttribute;
	}

	/**
	 * @return the filterForm
	 */
	@Transient
	public String getFilterForm() {
		return filterForm;
	}

	/**
	 * @param filterForm the filterForm to set
	 */
	public void setFilterForm(String filterForm) {
		this.filterForm = filterForm;
	}

	/**
	 * @param numTarjeta the numTarjeta to set
	 */
	public void setNumTarjeta(Long numTarjeta) {
		this.numTarjeta = numTarjeta;
	}

	/**
	 * @return the reporteExcel
	 */
	@Transient
	public String getReporteExcel() {
		return reporteExcel;
	}

	/**
	 * @param reporteExcel the reporteExcel to set
	 */
	public void setReporteExcel(String reporteExcel) {
		this.reporteExcel = reporteExcel;
	}
	/**
	 * @return the posStart
	 */
	@Transient
	public Integer getPosStart() {
		return posStart;
	}

	/**
	 * @param posStart the posStart to set
	 */
	public void setPosStart(Integer posStart) {
		this.posStart = posStart;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.referenciacargoconsolidado;
	}

	@Override
	public String getValue() {
		if (this.referenciacargoconsolidado != null
				&& this.referenciacargoconsolidado > 0) {
			return "CONSOLIDAD ID =" + this.referenciacargoconsolidado;
		} else {
			return "SE DESCONOCE EL VALOR";
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.referenciacargoconsolidado;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Consolidado [idMedioPago=" + idMedioPago
				+ ", referenciacargoconsolidado=" + referenciacargoconsolidado
				+ ", importecargoconsolidado=" + importecargoconsolidado
				+ ", fechaconsolidado=" + fechaconsolidado + ", fechalimite="
				+ fechalimite + ", fechacorte=" + fechacorte
				+ ", fechacreacion=" + fechacreacion + ", fechaconsolidadostr="
				+ fechaconsolidadostr + ", fechalimitestr=" + fechalimitestr
				+ ", fechacortestr=" + fechacortestr + ", fechacreacionstr="
				+ fechacreacionstr + ", foliofiscal=" + foliofiscal
				+ ", poliza=" + poliza + ", usuariocreacion=" + usuariocreacion
				+ ", estatus=" + estatus + ", estatusdesc=" + estatusdesc
				+ ", numTarjeta=" + numTarjeta + ", totalCount=" + totalCount
				+ ", posStart=" + posStart + ", count=" + count
				+ ", orderByAttribute=" + orderByAttribute + ", filterForm="
				+ filterForm + ", sdf=" + sdf + "]";
	}

	
}