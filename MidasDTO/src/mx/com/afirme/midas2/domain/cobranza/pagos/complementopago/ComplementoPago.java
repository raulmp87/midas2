package mx.com.afirme.midas2.domain.cobranza.pagos.complementopago;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import mx.com.afirme.midas.base.LogBaseDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.cobranza.recibos.consolidar.Consolidado;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author bitfarm GustavoR
 */

@Entity
@Table(name = "TBL_PAGOS_RECIBO", schema = "SEYCOS")
public class ComplementoPago extends LogBaseDTO implements java.io.Serializable, Entidad {

	private static final long serialVersionUID = 1L;
	private BigDecimal id;
	private BigDecimal idRecibo;
	private BigDecimal idFactura;
	private BigDecimal idVersionRbo;
	private Integer numParcialidad;
	private BigDecimal impSaldoAnt;
	private BigDecimal impPagado;
	private BigDecimal impSaldoInsoluto;
	private Date fechaExpedicionInicial;
	private Date fechaExpedicionFinal;
	private Date fechaPago;
	private Date fechaPagoInicio;
	private Date fechaPagoFin;
	private String fechaPagoInicioStr;
	private String fechaPagoFinStr;
	private String fechaPagostr;
	private String fechaExpedicionInicialStr;
	private String fechaExpedicionFinalStr;
	private Integer noRecibo;
	private String numPoliza;
	private String numRenPoliza;
	private String emisionPoliza;
	private String incisoPoliza;
	private Integer claveAgente;
	private String nomCliente;
	private String clienteRfc;
	private String folioFiscal;
	private BigDecimal numExhibicion;
	private String error;
	private String situacionRecibo;
	private BigDecimal remesaDesp;
	private String usuarioCreacion;
	private String fechaEmision;
	private String fechaContab;
	private String fechaCubreDesde;
	private String fechaCubreHasta;
	private String tipoMovimiento;
	private String factura;
	private BigDecimal monitorNumPoliza;
	private BigDecimal monitorNumRenovPoliza;
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	private String fechaIncorrecta= "Fecha incorrecta";
	private static final Logger log = LoggerFactory.getLogger(ComplementoPago.class);


	//Property Accessors
		@Id
		@SequenceGenerator(name = "TBL_PAGOS_RECIBO_ID_SEQ", allocationSize = 1, sequenceName = "TBL_PAGOS_RECIBO_ID_SEQ")
		@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_PAGOS_RECIBO_ID_SEQ")
		@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
		public BigDecimal getId() {
			return id;
		}
		public void setId(BigDecimal id) {
			this.id = id;
		}

		@Column(name = "ID_RECIBO", nullable = false)
		public BigDecimal getIdRecibo() {
			return idRecibo;
		}
		public void setIdRecibo(BigDecimal idRecibo) {
			this.idRecibo = idRecibo;
		}
		@Column(name = "ID_FACTURA", nullable = false)
		public BigDecimal getIdFactura() {
			return idFactura;
		}
		public void setIdFactura(BigDecimal idFactura) {
			this.idFactura = idFactura;
		}
		@Column(name = "NUMPARCIALIDAD")
		public Integer getNumParcialidad() {
			return numParcialidad;
		}
		public void setNumParcialidad(Integer numParcialidad) {
			this.numParcialidad = numParcialidad;
		}
		@Column(name = "IMPSALDOANT")
		public BigDecimal getImpSaldoAnt() {
			return impSaldoAnt;
		}
		public void setImpSaldoAnt(BigDecimal impSaldoAnt) {
			this.impSaldoAnt = impSaldoAnt;
		}
		@Column(name = "IMPPAGADO")
		public BigDecimal getImpPagado() {
			return impPagado;
		}
		public void setImpPagado(BigDecimal impPagado) {
			this.impPagado = impPagado;
		}
		@Column(name = "IMPSALDOINSOLUTO")
		public BigDecimal getImpSaldoInsoluto() {
			return impSaldoInsoluto;
		}
		public void setImpSaldoInsoluto(BigDecimal impSaldoInsoluto) {
			this.impSaldoInsoluto = impSaldoInsoluto;
		}
		@Temporal(TemporalType.DATE)
		@Column(name = "FECHAPAGO")
		public Date getFechaPago() {
			return fechaPago;
		}
		public void setFechaPago(Date fechaPago) {
			this.fechaPago = fechaPago;
			if(fechaPago==null){
				this.setFechaPagostr("");
			}else{
				try{
					this.setFechaPagostr(sdf.format(fechaPago));
				}catch (Exception e){
					log.error(fechaIncorrecta, e);
					this.setFechaPagostr(fechaIncorrecta);
				}
			}
		}

		@Transient
		public Date getFechaPagoInicio() {
			return fechaPagoInicio;
		}
		public void setFechaPagoInicio(Date fechaPagoInicio) {
			this.fechaPagoInicio = fechaPagoInicio;
			if(fechaPagoInicio==null){
				this.setFechaPagoInicioStr("");
			}else{
				try{
					this.setFechaPagoInicioStr(sdf.format(fechaPagoInicio));
				}catch (Exception e){
					log.error(fechaIncorrecta, e);
					this.setFechaPagostr(fechaIncorrecta);
				}
			}
		}

		@Transient
		public Date getFechaPagoFin() {
			return fechaPagoFin;
		}
		public void setFechaPagoFin(Date fechaPagoFin) {
			this.fechaPagoFin = fechaPagoFin;
			if(fechaPagoFin==null){
				this.setFechaPagoFinStr("");
			}else{
				try{
					this.setFechaPagoFinStr(sdf.format(fechaPagoFin));
				}catch (Exception e){
					log.error(fechaIncorrecta, e);
					this.setFechaPagostr(fechaIncorrecta);
				}
			}
		}


		@Transient
		public Date getFechaExpedicionInicial() {
			return fechaExpedicionInicial;
		}
		public void setFechaExpedicionInicial(Date fechaExpedicionInicial) {
			this.fechaExpedicionInicial = fechaExpedicionInicial;
			if (fechaExpedicionInicial==null) {
				this.setFechaExpedicionInicialStr("");
			}else{
				try{
					this.setFechaExpedicionInicialStr(sdf.format(fechaExpedicionInicial));
				}catch (Exception e){
					log.error(fechaIncorrecta, e);
					this.setFechaPagostr(fechaIncorrecta);
				}
			}
		}

		@Transient
		public Date getFechaExpedicionFinal() {
			return fechaExpedicionFinal;
		}
		public void setFechaExpedicionFinal(Date fechaExpedicionFinal) {
			this.fechaExpedicionFinal = fechaExpedicionFinal;
			if(fechaExpedicionFinal==null){
				this.setFechaExpedicionFinalStr("");
			}else{
				try{
					this.setFechaExpedicionFinalStr(sdf.format(fechaExpedicionFinal));
				}catch (Exception e){
					log.error(fechaIncorrecta, e);
					this.setFechaPagostr(fechaIncorrecta);
				}
			}
		}

		@Transient
		public String getFechaPagostr() {
			return fechaPagostr;
		}
		public void setFechaPagostr(String fechaPagostr) {
			this.fechaPagostr = fechaPagostr;
		}


		@Transient
		public String getFechaPagoInicioStr() {
			return fechaPagoInicioStr;
		}
		public void setFechaPagoInicioStr(String fechaPagoInicioStr) {
			this.fechaPagoInicioStr = fechaPagoInicioStr;
		}

		@Transient
		public String getFechaPagoFinStr() {
			return fechaPagoFinStr;
		}
		public void setFechaPagoFinStr(String fechaPagoFinStr) {
			this.fechaPagoFinStr = fechaPagoFinStr;
		}

		@Column(name = "ID_VERSION_RBO", nullable = false)
		public BigDecimal getIdVersionRbo() {
			return idVersionRbo;
		}

		public void setIdVersionRbo(BigDecimal idVersionRbo) {
			this.idVersionRbo = idVersionRbo;
		}

		@Transient
		public Integer getNoRecibo() {
			return noRecibo;
		}

		public void setNoRecibo(Integer noRecibo) {
			this.noRecibo = noRecibo;
		}

		@Transient
		public String getNumPoliza() {
			return numPoliza;
		}

		public void setNumPoliza(String numPoliza) {
			this.numPoliza = numPoliza;
		}

		@Transient
		public String getNumRenPoliza() {
			return numRenPoliza;
		}

		public void setNumRenPoliza(String numRenPoliza) {
			this.numRenPoliza = numRenPoliza;
		}

		@Transient
		public String getEmisionPoliza() {
			return emisionPoliza;
		}

		public void setEmisionPoliza(String emisionPoliza) {
			this.emisionPoliza = emisionPoliza;
		}

		@Transient
		public String getIncisoPoliza() {
			return incisoPoliza;
		}

		public void setIncisoPoliza(String incisoPoliza) {
			this.incisoPoliza = incisoPoliza;
		}

		@Transient
		public Integer getClaveAgente() {
			return claveAgente;
		}

		public void setClaveAgente(Integer claveAgente) {
			this.claveAgente = claveAgente;
		}

		@Transient
		public String getNomCliente() {
			return nomCliente;
		}

		public void setNomCliente(String nomCliente) {
			this.nomCliente = nomCliente;
		}

		@Transient
		public String getClienteRfc() {
			return clienteRfc;
		}

		public void setClienteRfc(String clienteRfc) {
			this.clienteRfc = clienteRfc;
		}

		@Transient
		public String getFolioFiscal() {
			return folioFiscal;
		}
		public void setFolioFiscal(String folioFiscal) {
			this.folioFiscal = folioFiscal;
		}

		@SuppressWarnings("unchecked")
		public BigDecimal getKey(){
			return this.id;
		}

		public String getValue(){
			if(this.id != null){
				return "COMPLEMENTO PAGO ID = " + this.id;
			}else{
				return "SE DESCONOCE EL COMPLEMENTO";
			}
		}

		@SuppressWarnings("unchecked")
		public BigDecimal getBusinessKey(){
			return this.id;
		}
		@Transient
		public BigDecimal getNumExhibicion() {
			return numExhibicion;
		}
		public void setNumExhibicion(BigDecimal numExhibicion) {
			this.numExhibicion = numExhibicion;
		}

		@Transient
		public String getError() {
			return error;
		}
		public void setError(String error) {
			this.error = error;
		}

		@Transient
		public String getFechaExpedicionInicialStr() {
			return fechaExpedicionInicialStr;
		}
		public void setFechaExpedicionInicialStr(String fechaExpedicionInicialStr) {
			this.fechaExpedicionInicialStr = fechaExpedicionInicialStr;
		}

		@Transient
		public String getFechaExpedicionFinalStr() {
			return fechaExpedicionFinalStr;
		}
		public void setFechaExpedicionFinalStr(String fechaExpedicionFinalStr) {
			this.fechaExpedicionFinalStr = fechaExpedicionFinalStr;
		}
		
		@Transient
		public String getSituacionRecibo() {
			return situacionRecibo;
		}
		public void setSituacionRecibo(String situacionRecibo) {
			this.situacionRecibo = situacionRecibo;
		}
		
		@Transient
		public BigDecimal getRemesaDesp() {
			return remesaDesp;
		}
		public void setRemesaDesp(BigDecimal remesaDesp) {
			this.remesaDesp = remesaDesp;
		}
		
		@Transient
		public String getUsuarioCreacion() {
			return usuarioCreacion;
		}
		public void setUsuarioCreacion(String usuarioCreacion) {
			this.usuarioCreacion = usuarioCreacion;
		}
		
		
		@Transient
		public String getFechaEmision() {
			return fechaEmision;
		}
		public void setFechaEmision(String fechaEmision) {
			this.fechaEmision = fechaEmision;
		}
		
		@Transient
		public String getTipoMovimiento() {
			return tipoMovimiento;
		}
		public void setTipoMovimiento(String tipoMovimiento) {
			this.tipoMovimiento = tipoMovimiento;
		}
		
		@Transient
		public BigDecimal getMonitorNumPoliza() {
			return monitorNumPoliza;
		}
		public void setMonitorNumPoliza(BigDecimal monitorNumPoliza) {
			this.monitorNumPoliza = monitorNumPoliza;
		}
		
		@Transient
		public BigDecimal getMonitorNumRenovPoliza() {
			return monitorNumRenovPoliza;
		}
		public void setMonitorNumRenovPoliza(BigDecimal monitorNumRenovPoliza) {
			this.monitorNumRenovPoliza = monitorNumRenovPoliza;
		}
		
		@Transient
		public String getFechaContab() {
			return fechaContab;
		}
		public void setFechaContab(String fechaContab) {
			this.fechaContab = fechaContab;
		}
		
		@Transient
		public String getFechaCubreDesde() {
			return fechaCubreDesde;
		}
		public void setFechaCubreDesde(String fechaCubreDesde) {
			this.fechaCubreDesde = fechaCubreDesde;
		}
		
		@Transient
		public String getFechaCubreHasta() {
			return fechaCubreHasta;
		}
		public void setFechaCubreHasta(String fechaCubreHasta) {
			this.fechaCubreHasta = fechaCubreHasta;
		}
		
		@Transient
		public String getFactura() {
			return factura;
		}
		public void setFactura(String factura) {
			this.factura = factura;
		}		
}
