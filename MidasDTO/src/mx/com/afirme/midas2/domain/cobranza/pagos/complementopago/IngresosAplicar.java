package mx.com.afirme.midas2.domain.cobranza.pagos.complementopago;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Transient;

import mx.com.afirme.midas2.annotation.Exportable;

public class IngresosAplicar {

	private BigDecimal idVersionSdo;
	private Date fTerReg;
	private Date fIniReg;
	private String idRefunic;
	private BigDecimal sdoRefunicDep;
	private Date fIngreso;
	private Date fCarga;
	private String cveUsuario;
	private String cuentaBancaria;
	private String nomBanco;
	private String moneda;
	private String referenciaOri;
	private String usuCarga;
	private String codigo;
	
    @Exportable(columnName="ID_VERSION_SDO", columnOrder=0)
	public BigDecimal getIdVersionSdo() {
		return idVersionSdo;
	}

	public void setIdVersionSdo(BigDecimal idVersionSdo) {
		this.idVersionSdo = idVersionSdo;
	}
	
    @Exportable(columnName="F_TER_REG", columnOrder=1, format= "dd/MM/yyyy")
	public Date getfTerReg() {
		return fTerReg;
	}

	public void setfTerReg(Date fTerReg) {
		this.fTerReg = fTerReg;
	}

    @Exportable(columnName="F_INI_REG", columnOrder=2, format= "dd/MM/yyyy")
	public Date getfIniReg() {
		return fIniReg;
	}

	public void setfIniReg(Date fIniReg) {
		this.fIniReg = fIniReg;
	}

    @Exportable(columnName="ID_REFUNIC", columnOrder=3)
	public String getIdRefunic() {
		return idRefunic;
	}

	public void setIdRefunic(String idRefunic) {
		this.idRefunic = idRefunic;
	}

    @Exportable(columnName="SDO_REFUNIC_DEP", columnOrder=4, format="$* #,##0.00")
	public BigDecimal getSdoRefunicDep() {
		return sdoRefunicDep;
	}

	public void setSdoRefunicDep(BigDecimal sdoRefunicDep) {
		this.sdoRefunicDep = sdoRefunicDep;
	}

    @Exportable(columnName="F_INGRESO", columnOrder=5, format= "dd/MM/yyyy")
	public Date getfIngreso() {
		return fIngreso;
	}

	public void setfIngreso(Date fIngreso) {
		this.fIngreso = fIngreso;
	}

    @Exportable(columnName="F_CARGA", columnOrder=6, format= "dd/MM/yyyy")
	public Date getfCarga() {
		return fCarga;
	}

	public void setfCarga(Date fCarga) {
		this.fCarga = fCarga;
	}

    @Exportable(columnName="CVE_USUARIO", columnOrder=7)
	public String getCveUsuario() {
		return cveUsuario;
	}

	public void setCveUsuario(String cveUsuario) {
		this.cveUsuario = cveUsuario;
	}

	@Transient
    @Exportable(columnName="CUENTA_BANCARIA", columnOrder=8)
	public String getCuentaBancaria() {
		return cuentaBancaria;
	}

	public void setCuentaBancaria(String cuentaBancaria) {
		this.cuentaBancaria = cuentaBancaria;
	}

    @Exportable(columnName="NOM_BANCO", columnOrder=9)
	public String getNomBanco() {
		return nomBanco;
	}

	public void setNomBanco(String nomBanco) {
		this.nomBanco = nomBanco;
	}

    @Exportable(columnName="ID_MONEDA", columnOrder=10)
	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

    @Exportable(columnName="REFERENCIA_ORI", columnOrder=11)
	public String getReferenciaOri() {
		return referenciaOri;
	}

	public void setReferenciaOri(String referenciaOri) {
		this.referenciaOri = referenciaOri;
	}

    @Exportable(columnName="USU_CARGA", columnOrder=12)
	public String getUsuCarga() {
		return usuCarga;
	}

	public void setUsuCarga(String usuCarga) {
		this.usuCarga = usuCarga;
	}

    @Exportable(columnName="CODIGO", columnOrder=13)
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
}