package mx.com.afirme.midas2.domain.cobranza.pagos;

import java.security.NoSuchAlgorithmException;

import mx.com.afirme.midas2.service.cobranza.pagos.CreditCardPaymentService;
import mx.com.afirme.midas2.util.StringUtil;

public class PROSATransaction {
	
	private static final String STORE = "1234";
	private static final String TERM = "001";	
	private int total;
	private int currency;
	private String orderId;
	private String insurancePolicy;
	private String subsection;
	private String merchant;
	private String url_back;
	
	public String getSubsection() {
		return subsection;
	}

	public void setSubsection(String subsection) {
		this.subsection = subsection;
	}

	public int getTotal() {
		return total;
	}

	public int getCurrency() {
		return currency;
	}

	public String getOrderId() {
		return orderId;
	}

	public String getInsurancePolicy() {
		return insurancePolicy;
	}
	
	public void setPolicy(String policy) {
		this.insurancePolicy = policy;
	}

	public void setTotal(int total) {
		this.total = total;
	}
	
	public void setCurrency(int currency) {
		this.currency = currency;
	}
	
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}	
	
	public String getMerchant() {
		return merchant;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public String getUrl_back() {
		return url_back;
	}

	public void setUrl_back(String url_back) {
		this.url_back = url_back;
	}

	public String getDigest() throws NoSuchAlgorithmException{
		String string = merchant + STORE + TERM + total + currency + orderId;		
		return StringUtil.sha1(string);
	}
	
	public String getURL() throws NoSuchAlgorithmException{
		String url = CreditCardPaymentService.URL_PROSA + "?"
					+ "urlBack=" + url_back
					+ "&" + "total=" + total
					+ "&" + "currency=" + currency
					+ "&" + "order_id=" + orderId
					+ "&" + "merchant=" + merchant
					+ "&" + "store=" + STORE
					+ "&" + "term=" + TERM
					+ "&" + "insurancePolicy=" + insurancePolicy
					+ "&" + "digest=" + getDigest();
					if(subsection != null){
						url += "&" + "subsection=" + subsection;
					}
		return url;
	}
}