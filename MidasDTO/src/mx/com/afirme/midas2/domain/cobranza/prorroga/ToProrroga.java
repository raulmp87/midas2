package mx.com.afirme.midas2.domain.cobranza.prorroga;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "TOPRORROGA", schema = "MIDAS")
public class ToProrroga  implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOPRORROGA_ID_GENERATOR")	
	@SequenceGenerator(name="TOPRORROGA_ID_GENERATOR", sequenceName="MIDAS.TOPRORROGA_SEQ", allocationSize=100)	
	@Column(name="ID")
	private Long id;
	@Column(name="IDTOCOTIZACION")
	private Long idToCotizacion;
	@Column(name="NUMEROINCISO")
	private Long numeroInciso;
	@Column(name="NUM_PRORROGA")
	private Long num_prorroga;
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAINICIO")
	private Date fechaInicio;
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAFIN")
	private Date fechaFin;
	@Column(name="COMENTARIO")
	private String comentario;
	@Temporal(TemporalType.DATE)
	@Column(name="FECHACREACION")
	private Date fechaCreacion;
	@Column(name="CODIGOUSUARIOCREACION")
	private String codigoUsuarioCreacion;
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAAUTORIZACION")
	private Date fechaAutorizacion;
	@Column(name="CODIGOUSUARIOAUTORIZA")
	private String codigoUsuarioAutoriza;
	@Column(name="ESTATUS")
	private int estatus;	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdToCotizacion() {
		return idToCotizacion;
	}
	public void setIdToCotizacion(Long idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	public Long getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(Long numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public Long getNum_prorroga() {
		return num_prorroga;
	}
	public void setNum_prorroga(Long num_prorroga) {
		this.num_prorroga = num_prorroga;
	}
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}
	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
	public Date getFechaAutorizacion() {
		return fechaAutorizacion;
	}
	public void setFechaAutorizacion(Date fechaAutorizacion) {
		this.fechaAutorizacion = fechaAutorizacion;
	}
	public String getCodigoUsuarioAutoriza() {
		return codigoUsuarioAutoriza;
	}
	public void setCodigoUsuarioAutoriza(String codigoUsuarioAutoriza) {
		this.codigoUsuarioAutoriza = codigoUsuarioAutoriza;
	}
	public int getEstatus() {
		return estatus;
	}
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Transient
	public long getDiasPorVencer() {
		
		Date dateToday = new Date();
		Date dateFechaFin = fechaFin;
		
		long diff = dateFechaFin.getTime() - dateToday.getTime();
		long diffDays = diff / (24 * 60 * 60 * 1000);
		
		return diffDays;
	}	
}
