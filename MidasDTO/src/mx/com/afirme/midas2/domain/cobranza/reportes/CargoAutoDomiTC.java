package mx.com.afirme.midas2.domain.cobranza.reportes;

import java.math.BigDecimal;

import mx.com.afirme.midas2.annotation.Exportable;

public class CargoAutoDomiTC {
	
	private String  numeroPoliza;
	
	private BigDecimal numeroRecibo;
	
	private BigDecimal numeroExhibicion;
	
	private String nombreAsegurado;
	
	private String titular;
	
	private String tipoCobro;
	
	private String fechaCubreDesde;
	
	private String fechaCubreHasta;
	
	private BigDecimal importeRecibo;
	
	private String nombreBanco;
	
	private String numeroCuenta;
	
	private String tipoConductoCobro;
	
	private Integer intento;
	

	@Exportable(columnName="Numero Poliza", columnOrder=0)
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	@Exportable(columnName="Numero Recibo", columnOrder=1)
	public BigDecimal getNumeroRecibo() {
		return numeroRecibo;
	}
	
	public void setNumeroRecibo(BigDecimal numeroRecibo) {
		this.numeroRecibo = numeroRecibo;
	}
	
	@Exportable(columnName="Numero Exhibicion", columnOrder=2)
	public BigDecimal getNumeroExhibicion() {
		return numeroExhibicion;
	}

	public void setNumeroExhibicion(BigDecimal numeroExhibicion) {
		this.numeroExhibicion = numeroExhibicion;
	}

	@Exportable(columnName="Nombre Asegurado", columnOrder=3)
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}
	
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}

	@Exportable(columnName="Titular", columnOrder=4)
	public String getTitular() {
		return titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	@Exportable(columnName="Tipo Cobro", columnOrder=5)
	public String getTipoCobro() {
		return tipoCobro;
	}
	
	public void setTipoCobro(String tipoCobro) {
		this.tipoCobro = tipoCobro;
	}

	@Exportable(columnName="Fecha Cubre Desde", columnOrder=6)
	public String getFechaCubreDesde() {
		return fechaCubreDesde;
	}

	public void setFechaCubreDesde(String fechaCubreDesde) {
		this.fechaCubreDesde = fechaCubreDesde;
	}
	
	@Exportable(columnName="Fecha Cubre Hasta", columnOrder=7)
	public String getFechaCubreHasta() {
		return fechaCubreHasta;
	}

	public void setFechaCubreHasta(String fechaCubreHasta) {
		this.fechaCubreHasta = fechaCubreHasta;
	}

	@Exportable(columnName="Importe Recibo", columnOrder=8)
	public BigDecimal getImporteRecibo() {
		return importeRecibo;
	}
	
	public void setImporteRecibo(BigDecimal importeRecibo) {
		this.importeRecibo = importeRecibo;
	}

	@Exportable(columnName="Nombre Banco", columnOrder=9)
	public String getNombreBanco() {
		return nombreBanco;
	}

	public void setNombreBanco(String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}

	@Exportable(columnName="Numero Cuenta", columnOrder=10)
	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	@Exportable(columnName="Tipo Conducto Cobro", columnOrder=11)
	public String getTipoConductoCobro() {
		return tipoConductoCobro;
	}
	
	public void setTipoConductoCobro(String tipoConductoCobro) {
		this.tipoConductoCobro = tipoConductoCobro;
	}

	@Exportable(columnName="Intento", columnOrder=12)
	public Integer getIntento() {
		return intento;
	}

	public void setIntento(Integer intento) {
		this.intento = intento;
	}
	
}
