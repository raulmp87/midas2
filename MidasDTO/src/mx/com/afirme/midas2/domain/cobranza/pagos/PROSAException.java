package mx.com.afirme.midas2.domain.cobranza.pagos;

public class PROSAException extends Exception{

	private static final long serialVersionUID = -1558029979847461391L;
	
	private String message;
	
	public static final String RECEIPTS_NOT_FOUND = "Su búsqueda no obtuvo resultados. Intente de nuevo.";
	public static final String TRANSACTION_DENIED = "No se pudo completar la transacción. Es posible que la información proporcionada sea incorrecta.";
	public static final String INVALID_CURRENCY = "Elija recibos con el mismo tipo de moneda.";
	public static final String INVALID_AMMOUNT = "El monto total del pago no es válido.";
	public static final String LOCKED_OUT = "La poliza esta bloqueada.";
	public static final String RECEIPTS_NOT_SELECTED = "Seleccione al menos un recibo para pagar.";
	public static final String UNKNOWN_ERROR = "Ocurrió un error inesperado. Por favor intente mas tarde.";
	public static final String DIGEST_ERROR = "Ocurrió un error al validar la información. Reportelo a sistemas.";
	public static final String ENVIO_COMPROBANTE_ERROR = "No se pudo enviar comprobante de pago por email, transaccion ";
	
	public PROSAException(String message){
		this.message = message;
	}

	@Override
	public String getMessage() {
		return this.message;
	}	
}