package mx.com.afirme.midas2.domain.cobranza.reportes;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="CALENDAR_GONHER" , schema="SEYCOS")
public class CalendarioGonherDTO  implements Serializable, Entidad {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="ID_CALENDAR")
	private BigDecimal idCalendar;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_PROGRAMACION")
	private Date fechaProgramacion;
	@Column(name="APLICA_PAGO")
	private Integer aplicaPago;
	@Column(name="ACTIVO")
	private Integer activo;
	@Column(name="MES")
	private Integer mes;
	@Column(name="ID_USUARIO_CREAC")
	private String idUsuarioCreac;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREAC")
	private Date fechaCreac;
	@Column(name="ID_USUARIO_MODIF")
	private String idUsuarioModif;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_MODIF")
	private Date fechaModif;
	@Column(name="CONFIRM_USER")
	private Integer confirmUser;
	
	

	public BigDecimal getIdCalendar() {
		return idCalendar;
	}

	public void setIdCalendar(BigDecimal idCalendar) {
		this.idCalendar = idCalendar;
	}

	public Date getFechaProgramacion() {
		return fechaProgramacion;
	}

	public void setFechaProgramacion(Date fechaProgramacion) {
		this.fechaProgramacion = fechaProgramacion;
	}

	public Integer getAplicaPago() {
		return aplicaPago;
	}

	public void setAplicaPago(Integer aplicaPago) {
		this.aplicaPago = aplicaPago;
	}

	public Integer getActivo() {
		return activo;
	}

	public void setActivo(Integer activo) {
		this.activo = activo;
	}

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

	public String getIdUsuarioCreac() {
		return idUsuarioCreac;
	}

	public void setIdUsuarioCreac(String idUsuarioCreac) {
		this.idUsuarioCreac = idUsuarioCreac;
	}

	public Date getFechaCreac() {
		return fechaCreac;
	}

	public void setFechaCreac(Date fechaCreac) {
		this.fechaCreac = fechaCreac;
	}

	public String getIdUsuarioModif() {
		return idUsuarioModif;
	}

	public void setIdUsuarioModif(String idUsuarioModif) {
		this.idUsuarioModif = idUsuarioModif;
	}

	public Date getFechaModif() {
		return fechaModif;
	}

	public void setFechaModif(Date fechaModif) {
		this.fechaModif = fechaModif;
	}

	public Integer getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(Integer confirmUser) {
		this.confirmUser = confirmUser;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.idCalendar;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <k> k getBusinessKey() {
		return null;
	}
}