package mx.com.afirme.midas2.domain.cobranza.pagos;

import java.io.Serializable;

public class RecibosFoleados implements Serializable {

	private static final long serialVersionUID = 1L;
	private String llaveFiscal;
	private Integer errorCode;
	private String errorDesc;
	private String fechaAsignacion;
	
	public String getLlaveFiscal() {
		return llaveFiscal;
	}
	
	public void setLlaveFiscal(String llaveFiscal) {
		this.llaveFiscal = llaveFiscal;
	}
	
	public Integer getErrorCode() {
		return errorCode;
	}
	
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	
	public String getErrorDesc() {
		return errorDesc;
	}
	
	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}
	
	public String getFechaAsignacion() {
		return fechaAsignacion;
	}
	
	public void setFechaAsignacion(String fechaAsignacion) {
		this.fechaAsignacion = fechaAsignacion;
	}	
}