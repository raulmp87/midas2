package mx.com.afirme.midas2.domain.cobranza.programapago;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="TOPROGPAGO", schema="MIDAS")
public class ToProgPago implements Entidad, Serializable {

	private static final long serialVersionUID = 8374042026097192220L;
	
	public enum TIPODOCUMENTO{POLIZA, ENDOSO_ALTA, ENDOSO_DISMINUCION};
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOPROGPAGO_ID_GENERATOR")	
	@SequenceGenerator(name="TOPROGPAGO_ID_GENERATOR", sequenceName="MIDAS.TOPROGPAGO_SEQ", allocationSize=100)	
	@Column(name="ID")
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="IDTOCOTIZACION", referencedColumnName="IDTOCOTIZACION", nullable=false)
	private CotizacionDTO cotizacion;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "progPago")
	private List<ToRecibo> recibos;
	
	@Column(name="IDREFERENCIA")
	private Long idReferencia;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="IDMONEDA", referencedColumnName="idTcMoneda", nullable=false)
	private MonedaDTO moneda;
	
	@Column(name="NUMPROGPAGO")
	private Integer numProgPago;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="IDDOCUMENTO", referencedColumnName="idToSolicitud", nullable=true)
	private SolicitudDTO solicitud;
	
	@Column(name="RECUOTIFICADO")
	private boolean recuotificado;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAREGISTRO")
	private Date fechaRegistro;
	
	@Column(name="CODIGOUSUARIOCREACION")
	private String codigoUsuarioCreacion;
	
	@Column(name="IDTOPERSONACONTRATANTE")
	private Long idContratante;
	
	@Column(name="NUMINCISO")
	private BigDecimal numInciso;
	
	@Transient
	private String nombreContratante;
	
	@Transient
	private int numRecibos;
	
	@Transient
	private Date inicioPrograma;
	
	@Transient
	private Date finPrograma;
	
	
	public Date getInicioPrograma() {
		return inicioPrograma;
	}

	public void setInicioPrograma(Date inicioPrograma) {
		this.inicioPrograma = inicioPrograma;
	}

	public Date getFinPrograma() {
		return finPrograma;
	}

	public void setFinPrograma(Date finPrograma) {
		this.finPrograma = finPrograma;
	}

	public int getNumRecibos() {
		return numRecibos;
	}

	public void setNumRecibos(int numRecibos) {
		this.numRecibos = numRecibos;
	}
	
	@Transient
	private BigDecimal impPrimaNetaDiferencia = BigDecimal.ZERO;

	@Transient
	private BigDecimal impPrimaNeta = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal impBonComPN = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal impBonComRPF = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal impBonComis = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal impDerechos = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal impRcgosPagoFR = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal impIVA = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal impOtrosImptos = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal impPrimaTotal = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal impComAgtPN = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal impComAgtRPF = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal impComAgt = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal impComSupPN = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal impComSupRPF = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal impComSup = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal impSobreComAgt = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal impSobreComProm = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal impCesionDerechosAgt = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal impCesionDerechosProm = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal impSobreComUdiAgt = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal impSobreComUdiProm = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal impBonoAgt = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal impBomoProm = BigDecimal.ZERO;;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id= id;
	}

	public CotizacionDTO getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(CotizacionDTO cotizacion) {
		this.cotizacion = cotizacion;
	}

	public Long getIdReferencia() {
		return idReferencia;
	}

	public void setIdReferencia(Long idReferencia) {
		this.idReferencia = idReferencia;
	}

	public MonedaDTO getMoneda() {
		return moneda;
	}

	public void setMoneda(MonedaDTO moneda) {
		this.moneda = moneda;
	}

	public Integer getNumProgPago() {
		return numProgPago;
	}

	public void setNumProgPago(Integer numProgPago) {
		this.numProgPago = numProgPago;
	}

	public SolicitudDTO getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(SolicitudDTO solicitud) {
		this.solicitud = solicitud;
	}

	public boolean isRecuotificado() {
		return recuotificado;
	}

	public void setRecuotificado(boolean recuotificado) {
		this.recuotificado = recuotificado;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
	
	public List<ToRecibo> getRecibos() {
		return recibos;
	}

	public void setRecibos(List<ToRecibo> recibos) {
		this.recibos = recibos;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {	
		return id;
	}

	@Override
	public String getValue() {		
		return cotizacion.getIdToCotizacion().toString().concat("-").concat(this.numProgPago.toString());
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getBusinessKey() {
		return cotizacion.getIdToCotizacion().toString().concat("-").concat(this.numProgPago.toString());
	}

	public BigDecimal getImpPrimaNeta() {
		impPrimaNeta = BigDecimal.ZERO;
		for(ToRecibo recibo : recibos){
			impPrimaNeta = recibo.getImpPrimaNeta() != null ? impPrimaNeta.add(recibo.getImpPrimaNeta()) : impPrimaNeta; 
		}
		return impPrimaNeta;
	}

	public void setImpPrimaNeta(BigDecimal impPrimaNeta) {
		this.impPrimaNeta = impPrimaNeta;
	}

	public BigDecimal getImpBonComPN() {
		return impBonComPN;
	}

	public void setImpBonComPN(BigDecimal impBonComPN) {
		this.impBonComPN = impBonComPN;
	}

	public BigDecimal getImpBonComRPF() {
		return impBonComRPF;
	}

	public void setImpBonComRPF(BigDecimal impBonComRPF) {
		this.impBonComRPF = impBonComRPF;
	}

	public BigDecimal getImpBonComis() {
		return impBonComis;
	}

	public void setImpBonComis(BigDecimal impBonComis) {
		this.impBonComis = impBonComis;
	}

	public BigDecimal getImpDerechos() {
		return impDerechos;
	}

	public void setImpDerechos(BigDecimal impDerechos) {
		this.impDerechos = impDerechos;
	}

	public BigDecimal getImpRcgosPagoFR() {
		return impRcgosPagoFR;
	}

	public void setImpRcgosPagoFR(BigDecimal impRcgosPagoFR) {
		this.impRcgosPagoFR = impRcgosPagoFR;
	}

	public BigDecimal getImpIVA() {
		return impIVA;
	}

	public void setImpIVA(BigDecimal impIVA) {
		this.impIVA = impIVA;
	}

	public BigDecimal getImpOtrosImptos() {
		return impOtrosImptos;
	}

	public void setImpOtrosImptos(BigDecimal impOtrosImptos) {
		this.impOtrosImptos = impOtrosImptos;
	}

	public BigDecimal getImpPrimaTotal() {
		return impPrimaTotal;
	}

	public void setImpPrimaTotal(BigDecimal impPrimaTotal) {
		this.impPrimaTotal = impPrimaTotal;
	}

	public BigDecimal getImpComAgtPN() {
		return impComAgtPN;
	}

	public void setImpComAgtPN(BigDecimal impComAgtPN) {
		this.impComAgtPN = impComAgtPN;
	}

	public BigDecimal getImpComAgtRPF() {
		return impComAgtRPF;
	}

	public void setImpComAgtRPF(BigDecimal impComAgtRPF) {
		this.impComAgtRPF = impComAgtRPF;
	}

	public BigDecimal getImpComAgt() {
		return impComAgt;
	}

	public void setImpComAgt(BigDecimal impComAgt) {
		this.impComAgt = impComAgt;
	}

	public BigDecimal getImpComSupPN() {
		return impComSupPN;
	}

	public void setImpComSupPN(BigDecimal impComSupPN) {
		this.impComSupPN = impComSupPN;
	}

	public BigDecimal getImpComSupRPF() {
		return impComSupRPF;
	}

	public void setImpComSupRPF(BigDecimal impComSupRPF) {
		this.impComSupRPF = impComSupRPF;
	}

	public BigDecimal getImpComSup() {
		return impComSup;
	}

	public void setImpComSup(BigDecimal impComSup) {
		this.impComSup = impComSup;
	}

	public BigDecimal getImpSobreComAgt() {
		return impSobreComAgt;
	}

	public void setImpSobreComAgt(BigDecimal impSobreComAgt) {
		this.impSobreComAgt = impSobreComAgt;
	}

	public BigDecimal getImpSobreComProm() {
		return impSobreComProm;
	}

	public void setImpSobreComProm(BigDecimal impSobreComProm) {
		this.impSobreComProm = impSobreComProm;
	}

	public BigDecimal getImpCesionDerechosAgt() {
		return impCesionDerechosAgt;
	}

	public void setImpCesionDerechosAgt(BigDecimal impCesionDerechosAgt) {
		this.impCesionDerechosAgt = impCesionDerechosAgt;
	}

	public BigDecimal getImpCesionDerechosProm() {
		return impCesionDerechosProm;
	}

	public void setImpCesionDerechosProm(BigDecimal impCesionDerechosProm) {
		this.impCesionDerechosProm = impCesionDerechosProm;
	}

	public BigDecimal getImpSobreComUdiAgt() {
		return impSobreComUdiAgt;
	}

	public void setImpSobreComUdiAgt(BigDecimal impSobreComUdiAgt) {
		this.impSobreComUdiAgt = impSobreComUdiAgt;
	}

	public BigDecimal getImpSobreComUdiProm() {
		return impSobreComUdiProm;
	}

	public void setImpSobreComUdiProm(BigDecimal impSobreComUdiProm) {
		this.impSobreComUdiProm = impSobreComUdiProm;
	}

	public BigDecimal getImpBonoAgt() {
		return impBonoAgt;
	}

	public void setImpBonoAgt(BigDecimal impBonoAgt) {
		this.impBonoAgt = impBonoAgt;
	}

	public BigDecimal getImpBomoProm() {
		return impBomoProm;
	}

	public void setImpBomoProm(BigDecimal impBomoProm) {
		this.impBomoProm = impBomoProm;
	}

	public Long getIdContratante() {
		return idContratante;
	}

	public void setIdContratante(Long idContratante) {
		this.idContratante = idContratante;
	}

	public String getNombreContratante() {
		return nombreContratante;
	}

	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}

	public BigDecimal getImpPrimaNetaDiferencia() {
		return impPrimaNetaDiferencia;
	}

	public void setImpPrimaNetaDiferencia(BigDecimal impPrimaNetaDiferencia) {
		this.impPrimaNetaDiferencia = impPrimaNetaDiferencia;
	}

	public BigDecimal getNumInciso() {
		return numInciso;
	}

	public void setNumInciso(BigDecimal numInciso) {
		this.numInciso = numInciso;
	}


}
