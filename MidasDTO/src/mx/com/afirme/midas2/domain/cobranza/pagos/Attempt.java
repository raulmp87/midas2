package mx.com.afirme.midas2.domain.cobranza.pagos;

import java.math.BigDecimal;

public class Attempt {
	private BigDecimal lastAttempt;
	private BigDecimal timeElapsed;

	
	public Attempt() {
		super();
		this.lastAttempt = BigDecimal.ZERO;
		this.timeElapsed = BigDecimal.ZERO;
	}
	public BigDecimal getLastAttempt() {
		return lastAttempt;
	}
	public void setLastAttempt(BigDecimal lastAttempt) {
		this.lastAttempt = lastAttempt;
	}
	public BigDecimal getTimeElapsed() {
		return timeElapsed;
	}
	public void setTimeElapsed(BigDecimal timeElapsed) {
		this.timeElapsed = timeElapsed;
	}
	
	
}
