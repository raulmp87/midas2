package mx.com.afirme.midas2.domain.cobranza.recibos.reexpedir;

import java.math.BigDecimal;

public class FiltroReciboReexpedicionDTO {

	private String numeroPoliza;
	private BigDecimal numeroCotizacion;
	private BigDecimal numeroEndoso;
	private BigDecimal numeroExhibicion;
	private BigDecimal numeroRecibo;
	private String clave;
	private BigDecimal primaTotal;
	private String situacion;
	private BigDecimal idAgente;
	private String nombreAgente;
	private BigDecimal numeroCliente;
	private String nombreCliente;
	private BigDecimal numeroInciso;
	private String serieFolioFiscal;
	private BigDecimal numFolioFiscal;
	private String llaveFiscal;
	
	private Long totalRowsCount;
	private Integer rowsStart;
	private Integer rowsCount;
	
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public BigDecimal getNumeroCotizacion() {
		return numeroCotizacion;
	}
	public void setNumeroCotizacion(BigDecimal numeroCotizacion) {
		this.numeroCotizacion = numeroCotizacion;
	}
	public BigDecimal getNumeroEndoso() {
		return numeroEndoso;
	}
	public void setNumeroEndoso(BigDecimal numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}
	public BigDecimal getNumeroExhibicion() {
		return numeroExhibicion;
	}
	public void setNumeroExhibicion(BigDecimal numeroExhibicion) {
		this.numeroExhibicion = numeroExhibicion;
	}
	public BigDecimal getNumeroRecibo() {
		return numeroRecibo;
	}
	public void setNumeroRecibo(BigDecimal numeroRecibo) {
		this.numeroRecibo = numeroRecibo;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public BigDecimal getPrimaTotal() {
		return primaTotal;
	}
	public void setPrimaTotal(BigDecimal primaTotal) {
		this.primaTotal = primaTotal;
	}
	public String getSituacion() {
		return situacion;
	}
	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}

	public BigDecimal getIdAgente() {
		return idAgente;
	}
	public void setIdAgente(BigDecimal idAgente) {
		this.idAgente = idAgente;
	}
	public BigDecimal getNumeroCliente() {
		return numeroCliente;
	}
	public void setNumeroCliente(BigDecimal numeroCliente) {
		this.numeroCliente = numeroCliente;
	}
	public String getNombreAgente() {
		return nombreAgente;
	}
	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public String getSerieFolioFiscal() {
		return serieFolioFiscal;
	}
	public void setSerieFolioFiscal(String serieFolioFiscal) {
		this.serieFolioFiscal = serieFolioFiscal;
	}
	public BigDecimal getNumFolioFiscal() {
		return numFolioFiscal;
	}
	public void setNumFolioFiscal(BigDecimal numFolioFiscal) {
		this.numFolioFiscal = numFolioFiscal;
	}
	public String getLlaveFiscal() {
		return llaveFiscal;
	}
	public void setLlaveFiscal(String llaveFiscal) {
		this.llaveFiscal = llaveFiscal;
	}
	public Long getTotalRowsCount() {
		return totalRowsCount;
	}
	public void setTotalRowsCount(Long totalRowsCount) {
		this.totalRowsCount = totalRowsCount;
	}
	public Integer getRowsStart() {
		return rowsStart;
	}
	public void setRowsStart(Integer rowsStart) {
		this.rowsStart = rowsStart;
	}
	public Integer getRowsCount() {
		return rowsCount;
	}
	public void setRowsCount(Integer rowsCount) {
		this.rowsCount = rowsCount;
	}
}
