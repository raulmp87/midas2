package mx.com.afirme.midas2.domain.cobranza.recibos.consolidar;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;


@Entity
@Table(name="REL_RECIBOS_CONSOLIDADO",schema="SEYCOS")
public class RelRecibosConsolidado implements java.io.Serializable, Entidad {
	
	private static final long serialVersionUID = 1L;
     private Long id;
     
     private Long referenciacargoconsolidado;
     private BigDecimal importecargoconsolidado;
     private Long idrecibo;
     private Date fechacreacion;
     private String descripcionError="";
     private Boolean tieneError = false;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the referenciacargoconsolidado
	 */
    @Column(name="REF_CARGO_CONS", precision=10, scale=0)
	public Long getReferenciacargoconsolidado() {
		return referenciacargoconsolidado;
	}
	/**
	 * @param referenciacargoconsolidado the referenciacargoconsolidado to set
	 */
	public void setReferenciacargoconsolidado(Long referenciacargoconsolidado) {
		this.referenciacargoconsolidado = referenciacargoconsolidado;
	}
	/**
	 * @return the importecargoconsolidado
	 */
	@Column(name="IMP_CARGO_CONS", precision=10, scale=0)
	public BigDecimal getImportecargoconsolidado() {
		return importecargoconsolidado;
	}
	/**
	 * @param importecargoconsolidado the importecargoconsolidado to set
	 */
	public void setImportecargoconsolidado(BigDecimal importecargoconsolidado) {
		this.importecargoconsolidado = importecargoconsolidado;
	}
	/**
	 * @return the idrecibo
	 */
	@Id 
    @Column(name="ID_RECIBO", unique=true, nullable=false, precision=10, scale=0)
	public Long getIdrecibo() {
		return idrecibo;
	}
	/**
	 * @param idrecibo the idrecibo to set
	 */
	public void setIdrecibo(Long idrecibo) {
		this.idrecibo = idrecibo;
	}
	/**
	 * @return the fechacreacion
	 */
    @Temporal(TemporalType.DATE)
    @Column(name="FH_CREACION", length=7)
	public Date getFechacreacion() {
		return fechacreacion;
	}
	/**
	 * @param fechacreacion the fechacreacion to set
	 */
	public void setFechacreacion(Date fechacreacion) {
		this.fechacreacion = fechacreacion;
	}
	
	
	/**
	 * @return the descripcionError
	 */
	public String getDescripcionError() {
		return descripcionError;
	}
	/**
	 * @param descripcionError the descripcionError to set
	 */
	public void setDescripcionError(String descripcionError) {
		this.descripcionError = descripcionError;
	}
	/**
	 * @return the tieneError
	 */
	public Boolean getTieneError() {
		return tieneError;
	}
	/**
	 * @param tieneError the tieneError to set
	 */
	public void setTieneError(Boolean tieneError) {
		this.tieneError = tieneError;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	 @SuppressWarnings("unchecked")
		@Override
		public Long getKey() {
			return this.id;
		}
	 
	 @Override
		public String getValue() {		
			if (this.idrecibo != null && this.idrecibo > 0 ){
				return "RECIBO ID =" + this.idrecibo;
			}else{
				return "SE DESCONOCE EL VALOR";
			}
		}
	 @SuppressWarnings("unchecked")
		@Override
	public Long getBusinessKey() {
		 return this.idrecibo;
	}

	 @Override
	public String toString() {
		return "RelRecibosConsolidado [id=" + id
				+ ", referenciacargoconsolidado=" + referenciacargoconsolidado
				+ ", importecargoconsolidado=" + importecargoconsolidado
				+ ", idrecibo=" + idrecibo + ", fechacreacion=" + fechacreacion
				+ "]";
	}
     	
}