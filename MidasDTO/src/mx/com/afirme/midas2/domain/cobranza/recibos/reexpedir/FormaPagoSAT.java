package mx.com.afirme.midas2.domain.cobranza.recibos.reexpedir;

import java.math.BigDecimal;

public class FormaPagoSAT {
	private BigDecimal id;
	private String clave;
	private String concepto;
	
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getClave() {
		return clave;
	}
	
	public void setClave(String clave) {
		this.clave = clave;
	}
	
	public String getConcepto() {
		return concepto;
	}
	
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
}