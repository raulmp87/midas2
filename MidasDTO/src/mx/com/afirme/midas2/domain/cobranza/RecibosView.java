package mx.com.afirme.midas2.domain.cobranza;

import java.io.Serializable;

public class RecibosView extends Recibos implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String estatus;
	private String estatusDesc;
	private String finivigenciaStr;
	private String ffinvigenciaStr;
	private String vencimientoStr;
	
	private String idLinea;
	private String idInciso;
	private String pdgIdVerRecibo;
	private String prIdVerRecibo; 
	private String serieFolioRecibo;
	private String situacionPoliza;
	private String tipoPoliza;
	private String tipoProducto;
	private String idProducto;
	private String solicitante;
	private int moneda;
	private String tipoDocumento;
	private String exhibicion;
	
	
	public String getExhibicion() {
		return exhibicion;
	}

	public void setExhibicion(String exhibicion) {
		this.exhibicion = exhibicion;
	}

	public int getMoneda() {
		return moneda;
	}

	public void setMoneda(int moneda) {
		this.moneda = moneda;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getSituacionPoliza() {
		return situacionPoliza;
	}

	public void setSituacionPoliza(String situacionPoliza) {
		this.situacionPoliza = situacionPoliza;
	}

	public String getTipoPoliza() {
		return tipoPoliza;
	}

	public void setTipoPoliza(String tipoPoliza) {
		this.tipoPoliza = tipoPoliza;
	}

	public String getTipoProducto() {
		return tipoProducto;
	}

	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}

	public String getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}

	public String getSerieFolioRecibo() {
		return serieFolioRecibo;
	}

	public void setSerieFolioRecibo(String serieFolioRecibo) {
		this.serieFolioRecibo = serieFolioRecibo;
	}

	private static String importePago;
	private static String comercio;
	private static String nombreCliente;
	private static String num_poliza;

	public static String getNum_poliza() {
		return num_poliza;
	}

	public static void setNum_poliza(String num_poliza) {
		RecibosView.num_poliza = num_poliza;
	}

	private static String numReciboPagados;
	private static String numeroTarjeta;
	private static String fechaPago;
	private static String  horaPago;
	private static String codigoAutorizacion;
	private static String periodoCubreRecibo;
	

	
	public static String getImportePago() {
		return importePago;
	}

	public static void setImportePago(String importePago) {
		RecibosView.importePago = importePago;
	}

	public static String getComercio() {
		return comercio;
	}

	public static void setComercio(String comercio) {
		RecibosView.comercio = comercio;
	}

	public static String getNombreCliente() {
		return nombreCliente;
	}

	public static void setNombreCliente(String nombreCliente) {
		RecibosView.nombreCliente = nombreCliente;
	}

	public static String getNumReciboPagados() {
		return numReciboPagados;
	}

	public static void setNumReciboPagados(String numReciboPagados) {
		RecibosView.numReciboPagados = numReciboPagados;
	}

	public static String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	public static void setNumeroTarjeta(String numeroTarjeta) {
		RecibosView.numeroTarjeta = numeroTarjeta;
	}

	public static String getFechaPago() {
		return fechaPago;
	}

	public static void setFechaPago(String fechaPago) {
		RecibosView.fechaPago = fechaPago;
	}

	public static String getHoraPago() {
		return horaPago;
	}

	public static void setHoraPago(String horaPago) {
		RecibosView.horaPago = horaPago;
	}

	public static String getCodigoAutorizacion() {
		return codigoAutorizacion;
	}

	public static void setCodigoAutorizacion(String codigoAutorizacion) {
		RecibosView.codigoAutorizacion = codigoAutorizacion;
	}

	public static String getPeriodoCubreRecibo() {
		return periodoCubreRecibo;
	}

	public static void setPeriodoCubreRecibo(String periodoCubreRecibo) {
		RecibosView.periodoCubreRecibo = periodoCubreRecibo;
	}

	public String getIdLinea() {
		return idLinea;
	}

	public void setIdLinea(String idLinea) {
		this.idLinea = idLinea;
	}

	public String getIdInciso() {
		return idInciso;
	}

	public void setIdInciso(String idInciso) {
		this.idInciso = idInciso;
	}

	public String getPdgIdVerRecibo() {
		return pdgIdVerRecibo;
	}

	public void setPdgIdVerRecibo(String pdgIdVerRecibo) {
		this.pdgIdVerRecibo = pdgIdVerRecibo;
	}

	public String getPrIdVerRecibo() {
		return prIdVerRecibo;
	}

	public void setPrIdVerRecibo(String prIdVerRecibo) {
		this.prIdVerRecibo = prIdVerRecibo;
	}

	
	
	

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	
	public String getFinivigenciaStr() {
		return finivigenciaStr;
	}

	public void setFinivigenciaStr(String finivigenciaStr) {
		this.finivigenciaStr = finivigenciaStr;
	}

	public String getFfinvigenciaStr() {
		return ffinvigenciaStr;
	}

	public void setFfinvigenciaStr(String ffinvigenciaStr) {
		this.ffinvigenciaStr = ffinvigenciaStr;
	}

	public String getVencimientoStr() {
		return vencimientoStr;
	}

	public void setVencimientoStr(String vencimientoStr) {
		this.vencimientoStr = vencimientoStr;
	}
	
	public String getEstatusDesc() {
		return estatusDesc;
	}

	public void setEstatusDesc(String estatusDesc) {
		this.estatusDesc = estatusDesc;
	}
	
	private String checkEnable;
	public String getCheckEnable() {
		return checkEnable;
	}

	public void setCheckEnable(String estatusDesc) {
		this.checkEnable = estatusDesc;
	}

	public void setSolicitante(String solicitante) {
		this.solicitante = solicitante;
	}

	public String getSolicitante() {
		return solicitante;
	}
	
}
