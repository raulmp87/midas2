package mx.com.afirme.midas2.domain.cobranza.recibos.consolidar;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Transient;
public class RecibosConsolidar implements Serializable {

	/**
	 * objeto, homologado al cursor
	 */
	private static final long serialVersionUID = 7893079105752477398L;
	private Integer idCotizacion;
	private String contratante;
	private Integer idRecibo;
	private Integer recibo;
	private Integer numRecibo;
	private Date vencimiento;
	private Integer folioFiscal;
	private String serieFiscal;
	private BigDecimal importe;
	private BigDecimal iva;
	private String llaveComprobante;
	private String archivoXml;
	private String archivoPdf;
	private String email;
	private String poliza;
	private Integer endoso;
	private Date finivigencia;
	private Date ffinvigencia;
	private Date fechaEmision;
	private String finivigenciastr;
	private String ffinvigenciastr;
	private String fechaEmisionstr;
	private String ramo;
	private String linneg;
	private Integer inciso;
	private String vehiculo;
	private Integer modelo;
	private String serie;
	private BigDecimal primaneta;
	private BigDecimal primatotal;
	private String sitrbo;
	private Integer progpago;
	private String condhabitual;
	private String nombreUsuario;
	private Integer ubicacion;
	private String titular;
	private String idRecibosConsolidar;
	private BigDecimal importeTotal;
	private boolean reciboChecked;
	private Date fechaCorte;
	private Long numPolizaConsolidar;
	private Date fechaConsolidado;
	private Integer centroEmision;
	private Integer numRenovPol;
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	@Transient
	private int filaExcel;
	public RecibosConsolidar() {
		super();
	}

	public Integer getIdCotizacion() {
		return idCotizacion;
	}

	public void setIdCotizacion(Integer idCotizacion) {
		this.idCotizacion = idCotizacion;
	}

	public String getContratante() {
		return contratante;
	}

	public void setContratante(String contratante) {
		this.contratante = contratante;
	}

	public Integer getIdRecibo() {
		return idRecibo;
	}

	public void setIdRecibo(Integer idRecibo) {
		this.idRecibo = idRecibo;
	}

	public Integer getNumRecibo() {
		return numRecibo;
	}

	public void setNumRecibo(Integer numRecibo) {
		this.numRecibo = numRecibo;
	}

	public Date getVencimiento() {
		return vencimiento;
	}

	public void setVencimiento(Date vencimiento) {
		this.vencimiento = vencimiento;
	}

	public Integer getFolioFiscal() {
		return folioFiscal;
	}

	public void setFolioFiscal(Integer folioFiscal) {
		this.folioFiscal = folioFiscal;
	}

	public String getSerieFiscal() {
		return serieFiscal;
	}

	public BigDecimal getImporte() {
		return importe;
	}

	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	public void setSerieFiscal(String serieFiscal) {
		this.serieFiscal = serieFiscal;
	}

	public String getLlaveComprobante() {
		return llaveComprobante;
	}

	public void setLlaveComprobante(String llaveComprobante) {
		this.llaveComprobante = llaveComprobante;
	}

	public String getArchivoXml() {
		return archivoXml;
	}

	public void setArchivoXml(String archivoXml) {
		this.archivoXml = archivoXml;
	}

	public String getArchivoPdf() {
		return archivoPdf;
	}

	public void setArchivoPdf(String archivoPdf) {
		this.archivoPdf = archivoPdf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPoliza() {
		return poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	public String getRamo() {
		return ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getEndoso() {
		return endoso;
	}

	public void setEndoso(Integer endoso) {
		this.endoso = endoso;
	}

	public Date getFinivigencia() {
		return finivigencia;
	}

	public void setFinivigencia(Date finivigencia) {
		this.finivigencia = finivigencia;
		this.setFinivigenciastr( sdf.format(finivigencia ));
	}

	public Date getFfinvigencia() {
		return ffinvigencia;
	}

	public void setFfinvigencia(Date ffinvigencia) {
		this.ffinvigencia = ffinvigencia;
		this.setFfinvigenciastr( sdf.format(ffinvigencia ));
	}

	/**
	 * @return the linneg
	 */
	public String getLinneg() {
		return linneg;
	}

	/**
	 * @param linneg
	 *            the linneg to set
	 */
	public void setLinneg(String linneg) {
		this.linneg = linneg;
	}

	/**
	 * @return the inciso
	 */
	public Integer getInciso() {
		return inciso;
	}

	/**
	 * @param inciso
	 *            the inciso to set
	 */
	public void setInciso(Integer inciso) {
		this.inciso = inciso;
	}

	/**
	 * @return the vehiculo
	 */
	public String getVehiculo() {
		return vehiculo;
	}

	/**
	 * @param vehiculo
	 *            the vehiculo to set
	 */
	public void setVehiculo(String vehiculo) {
		this.vehiculo = vehiculo;
	}

	/**
	 * @return the modelo
	 */
	public Integer getModelo() {
		return modelo;
	}

	/**
	 * @param modelo
	 *            the modelo to set
	 */
	public void setModelo(Integer modelo) {
		this.modelo = modelo;
	}

	/**
	 * @return the serie
	 */
	public String getSerie() {
		return serie;
	}

	/**
	 * @param serie
	 *            the serie to set
	 */
	public void setSerie(String serie) {
		this.serie = serie;
	}

	/**
	 * @return the primaneta
	 */
	public BigDecimal getPrimaneta() {
		return primaneta;
	}

	/**
	 * @param primaneta
	 *            the primaneta to set
	 */
	public void setPrimaneta(BigDecimal primaneta) {
		this.primaneta = primaneta;
	}

	/**
	 * @return the primatotal
	 */
	public BigDecimal getPrimatotal() {
		return primatotal;
	}

	/**
	 * @param primatotal
	 *            the primatotal to set
	 */
	public void setPrimatotal(BigDecimal primatotal) {
		this.primatotal = primatotal;
	}

	/**
	 * @return the sitrbo
	 */
	public String getSitrbo() {
		return sitrbo;
	}

	/**
	 * @param sitrbo
	 *            the sitrbo to set
	 */
	public void setSitrbo(String sitrbo) {
		this.sitrbo = sitrbo;
	}

	/**
	 * @return the progpago
	 */
	public Integer getProgpago() {
		return progpago;
	}

	/**
	 * @param progpago
	 *            the progpago to set
	 */
	public void setProgpago(Integer progpago) {
		this.progpago = progpago;
	}

	/**
	 * @return the condhabitual
	 */
	public String getCondhabitual() {
		return condhabitual;
	}

	/**
	 * @param condhabitual
	 *            the condhabitual to set
	 */
	public void setCondhabitual(String condhabitual) {
		this.condhabitual = condhabitual;
	}

	/**
	 * @return the ubicacion
	 */
	public Integer getUbicacion() {
		return ubicacion;
	}

	/**
	 * @param ubicacion
	 *            the ubicacion to set
	 */
	public void setUbicacion(Integer ubicacion) {
		this.ubicacion = ubicacion;
	}

	/**
	 * @return the titular
	 */
	public String getTitular() {
		return titular;
	}

	/**
	 * @param titular
	 *            the titular to set
	 */
	public void setTitular(String titular) {
		this.titular = titular;
	}

	public Integer getRecibo() {
		return recibo;
	}

	public void setRecibo(Integer recibo) {
		this.recibo = recibo;
	}

	public boolean isReciboChecked() {
		return reciboChecked;
	}

	public void setReciboChecked(boolean reciboChecked) {
		this.reciboChecked = reciboChecked;
	}

	/**
	 * @return the fechaCorte
	 */
	public Date getFechaCorte() {
		return fechaCorte;
	}

	/**
	 * @param fechaCorte
	 *            the fechaCorte to set
	 */
	public void setFechaCorte(Date fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	/**
	 * @return the numPolizaConsolidar
	 */
	public Long getNumPolizaConsolidar() {
		return numPolizaConsolidar;
	}

	/**
	 * @param numPolizaConsolidar
	 *            the numPolizaConsolidar to set
	 */
	public void setNumPolizaConsolidar(Long numPolizaConsolidar) {
		this.numPolizaConsolidar = numPolizaConsolidar;
	}

	/**
	 * @return the idRecibosConsolidar
	 */
	public String getIdRecibosConsolidar() {
		return idRecibosConsolidar;
	}

	/**
	 * @param idRecibosConsolidar
	 *            the idRecibosConsolidar to set
	 */
	public void setIdRecibosConsolidar(String idRecibosConsolidar) {
		this.idRecibosConsolidar = idRecibosConsolidar;
	}

	/**
	 * @return the importeTotal
	 */
	public BigDecimal getImporteTotal() {
		return importeTotal;
	}

	/**
	 * @param importeTotal
	 *            the importeTotal to set
	 */
	public void setImporteTotal(BigDecimal importeTotal) {
		this.importeTotal = importeTotal;
	}

	/**
	 * @return the iva
	 */
	public BigDecimal getIva() {
		return iva;
	}

	/**
	 * @param iva
	 *            the iva to set
	 */
	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public Date getFechaConsolidado() {
		return fechaConsolidado;
	}

	public void setFechaConsolidado(Date fechaConsolidado) {
		this.fechaConsolidado = fechaConsolidado;
	}

	/**
	 * @return the fechaEmision
	 */
	public Date getFechaEmision() {
		return fechaEmision;
	}

	/**
	 * @param fechaEmision
	 *            the fechaEmision to set
	 */
	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
		this.setFechaEmisionstr( sdf.format(fechaEmision ));
	}

	/**
	 * @return the nombreUsuario
	 */
	public String getNombreUsuario() {
		return nombreUsuario;
	}

	
	/**
	 * @return the filaExcel
	 */
	public int getFilaExcel() {
		return filaExcel;
	}

	/**
	 * @param filaExcel the filaExcel to set
	 */
	public void setFilaExcel(int filaExcel) {
		this.filaExcel = filaExcel;
	}

	/**
	 * @param nombreUsuario
	 *            the nombreUsuario to set
	 */
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	/**
	 * @return the finivigenciastr
	 */
	public String getFinivigenciastr() {
		return finivigenciastr;
	}

	/**
	 * @param finivigenciastr the finivigenciastr to set
	 */
	public void setFinivigenciastr(String finivigenciastr) {
		this.finivigenciastr = finivigenciastr;
	}

	/**
	 * @return the ffinvigenciastr
	 */
	public String getFfinvigenciastr() {
		return ffinvigenciastr;
	}

	/**
	 * @param ffinvigenciastr the ffinvigenciastr to set
	 */
	public void setFfinvigenciastr(String ffinvigenciastr) {
		this.ffinvigenciastr = ffinvigenciastr;
	}

	/**
	 * @return the fechaEmisionstr
	 */
	public String getFechaEmisionstr() {
		return fechaEmisionstr;
	}

	/**
	 * @param fechaEmisionstr the fechaEmisionstr to set
	 */
	public void setFechaEmisionstr(String fechaEmisionstr) {
		this.fechaEmisionstr = fechaEmisionstr;
	}



	/**
	 * @return the centroEmision
	 */
	public Integer getCentroEmision() {
		return centroEmision;
	}

	/**
	 * @param centroEmision the centroEmision to set
	 */
	public void setCentroEmision(Integer centroEmision) {
		this.centroEmision = centroEmision;
	}

	/**
	 * @return the numRenovPol
	 */
	public Integer getNumRenovPol() {
		return numRenovPol;
	}

	/**
	 * @param numRenovPol the numRenovPol to set
	 */
	public void setNumRenovPol(Integer numRenovPol) {
		this.numRenovPol = numRenovPol;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RecibosConsolidar [idCotizacion=" + idCotizacion
				+ ", contratante=" + contratante + ", idRecibo=" + idRecibo
				+ ", recibo=" + recibo + ", numRecibo=" + numRecibo
				+ ", vencimiento=" + vencimiento + ", folioFiscal="
				+ folioFiscal + ", serieFiscal=" + serieFiscal + ", importe="
				+ importe + ", iva=" + iva + ", llaveComprobante="
				+ llaveComprobante + ", archivoXml=" + archivoXml
				+ ", archivoPdf=" + archivoPdf + ", email=" + email
				+ ", poliza=" + poliza + ", endoso=" + endoso
				+ ", finivigencia=" + finivigencia + ", ffinvigencia="
				+ ffinvigencia + ", fechaEmision=" + fechaEmision
				+ ", finivigenciastr=" + finivigenciastr + ", ffinvigenciastr="
				+ ffinvigenciastr + ", fechaEmisionstr=" + fechaEmisionstr
				+ ", ramo=" + ramo + ", linneg=" + linneg + ", inciso="
				+ inciso + ", vehiculo=" + vehiculo + ", modelo=" + modelo
				+ ", serie=" + serie + ", primaneta=" + primaneta
				+ ", primatotal=" + primatotal + ", sitrbo=" + sitrbo
				+ ", progpago=" + progpago + ", condhabitual=" + condhabitual
				+ ", nombreUsuario=" + nombreUsuario + ", ubicacion="
				+ ubicacion + ", titular=" + titular + ", idRecibosConsolidar="
				+ idRecibosConsolidar + ", importeTotal=" + importeTotal
				+ ", reciboChecked=" + reciboChecked + ", fechaCorte="
				+ fechaCorte + ", numPolizaConsolidar=" + numPolizaConsolidar
				+ ", fechaConsolidado=" + fechaConsolidado + ", centroEmision="
				+ centroEmision + ", numRenovPol=" + numRenovPol + ", sdf="
				+ sdf + ", filaExcel=" + filaExcel + "]";
	}





	
}
