package mx.com.afirme.midas2.domain.cobranza;

public class FiltroRecibos {
	
	private String  Poliza;
	private Integer Agente;
	private Integer Asegurado;	
	private Integer Filtro;
	
	public String getPoliza() {
		return Poliza;
	}
	public void setPoliza(String poliza) {
		Poliza = poliza;
	}
	public Integer getAgente() {
		return Agente;
	}
	public void setAgente(Integer agente) {
		Agente = agente;
	}
	public Integer getAsegurado() {
		return Asegurado;
	}
	public void setAsegurado(Integer asegurado) {
		Asegurado = asegurado;
	}
	public Integer getFiltro() {
		return Filtro;
	}
	public void setFiltro(Integer filtro) {
		Filtro = filtro;
	}
	
	

}
