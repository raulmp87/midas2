package mx.com.afirme.midas2.domain.cobranza.pagos;

import java.security.NoSuchAlgorithmException;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.util.StringUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class PROSATransactionResponse {
	
	private static final Logger LOG = Logger.getLogger(PROSATransactionResponse.class);
	public enum PROSAResponse{
		DENIED("000000");
		
		private final String code;
		
		public String code(){
			return code;
		}
		
		PROSAResponse(String code){
			this.code = code;
		}
	}
	
	public static final int FIRST_ATTEMPT_LOCKOUT_TIME = 30;
	public static final int SECOND_ATTEMPT_LOCKOUT_TIME = 30;
	public static final int THIRD_ATTEMPT_LOCKOUT_TIME = 1440;
	
	private String response;
	private String total;
	private String orderId;
	private String merchant;
	private String store;
	private String term;
	private String refNum;
	private String auth;
	private String digest;
	private String cc_number;
	private String cc_name;
	private String insurancePolicy;
	private String subsection;
	
	private String idCotiza;
	private String [] recibos;
	private String totalFormateado;
	
	public String getSubsection() {
		return subsection;
	}

	public void setSubsection(String subsection) {
		this.subsection = subsection;
	}

	public String getPolicy() {
		return insurancePolicy;
	}

	public void setPolicy(String policy) {
		this.insurancePolicy = policy;
	}

	public String getCc_number() {
		return cc_number;
	}
	
	public void setCc_number(String cc_number) {
		this.cc_number = cc_number;
	}
	
	public String getCc_name(){
		return cc_name;
	}
	
	public void setCc_name(String cc_name){
		this.cc_name = cc_name;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public void setStore(String store) {
		this.store = store;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	public void setDigest(String digest) {
		this.digest = digest;
	}

	public String getResponse() {
		return response;
	}
	
	public String getTotal() {
		return total;
	}
	
	public String getOrderId() {
		return orderId;
	}
	
	public String getMerchant() {
		return merchant;
	}
	
	public String getStore() {
		return store;
	}
	
	public String getTerm() {
		return term;
	}
	
	public String getRefNum() {
		return refNum;
	}
	
	public String getAuth() {
		return auth;
	}
	
	public String getDigest() {
		return digest;
	}

	public String getIdCotiza() {
		return idCotiza;
	}

	public void setIdCotiza(String idCotiza) {
		this.idCotiza = idCotiza;
	}	
	
	public String [] getRecibos() {
		return recibos;
	}

	public void setRecibos (String [] recibos) {
		this.recibos = recibos;
	}

	public void setTotalFormateado(String totalFormateado) {
		this.totalFormateado = totalFormateado;
	}

	public boolean isApproved(){
		boolean approved = false;
		
		if(!this.auth.equals(PROSAResponse.DENIED.code)){
			approved = true;
		}
		
		return approved;
	}
	
	public boolean isValidDigest() throws NoSuchAlgorithmException{
		boolean valid = false;
		
		if(this.digest.equals(generateDigest())){
			valid = true;
		}
		
		return valid;
	}
	
	private String generateDigest() throws NoSuchAlgorithmException{
		String string = this.total + this.orderId + this.merchant + this.store + this.term + this.refNum + "-" + this.auth;
		return StringUtil.sha1(string);
	}
	
	public String getTotalFormateado(){		
		setTotalFormateado(total.substring(0, total.length()-2)+"."+ total.substring(total.length()-2));
		return this.totalFormateado;
	}
	
	public boolean store() throws Exception{
		try{		
			StoredProcedureHelper spBitacoraPROSA = new StoredProcedureHelper("SEYCOS.PKG_CC_PAYMENT.SAVE_TRANSACTION");
			spBitacoraPROSA.estableceParametro("pPoliza", insurancePolicy);
			spBitacoraPROSA.estableceParametro("response", response);
			spBitacoraPROSA.estableceParametro("total", getTotalFormateado());
			spBitacoraPROSA.estableceParametro("orderid", orderId);
			spBitacoraPROSA.estableceParametro("refnum", refNum);
			spBitacoraPROSA.estableceParametro("auth", auth);
			spBitacoraPROSA.estableceParametro("ccnumber", cc_number);
			spBitacoraPROSA.estableceParametro("idCotiza", idCotiza);
			spBitacoraPROSA.estableceParametro("idRecibo", StringUtils.join(recibos, ","));
			spBitacoraPROSA.ejecutaActualizar();
			return true;
		}catch(Exception ex){
			LOG.error("No se pudo guardar la bitacora de Portal Web [poliza " + insurancePolicy +
			" ,orderId " + orderId + " ,refnum " + refNum + " ,auth "+ auth + "]", ex);
			return false;
		}	
	}
}