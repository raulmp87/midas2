package mx.com.afirme.midas2.domain.cobranza.programapago;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

@Entity
@Table(name="TORECIBO", schema="MIDAS")
public class ToRecibo implements Entidad{
	
	private static final long serialVersionUID = 6470744867768511385L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TORECIBO_ID_GENERATOR")	
	@SequenceGenerator(name="TORECIBO_ID_GENERATOR", sequenceName="MIDAS.TORECIBO_SEQ", allocationSize=1)	
	@Column(name="ID")
	private Long id;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinFetch(JoinFetchType.INNER)
    @JoinColumn(name="PROGPAGO_ID", nullable=false)
	private ToProgPago progPago;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "recibo")
	private List<ToDesgloseRecibo> deglose;
	
	@Column(name="NUMEXHIBICION")
	private Integer numExhibicion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAREGISTRO")
	private Date fechaRegistro;
	
	@Column(name="CODIGOUSUARIOCREACION")
	private String codigoUsuarioCreacion;
	
	@Column(name="CVETIPODOCUMENTO")
	private String cveTipoDocumento;
	
	@Column(name="CVERECIBO")
	private String cveRecibo;
	
	@Column(name="SITRECIBO")
	private String sitRecibo;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAVENCIMIENTO")
	private Date fechaVencimiento;
	
	@Temporal(TemporalType.DATE)
	@Column(name="fechaCubreDesde")
	private Date fechaCubreDesde;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHACUBREHASTA")
	private Date fechaCubreHasta;
	
	@Column(name="IMPPRIMANETA")
	private BigDecimal impPrimaNeta;
	
	@Column(name="IMPBONCOMPN")
	private BigDecimal impBonComPN;
	
	@Column(name="IMPBONCOMRPF")
	private BigDecimal impBonComRPF;
	
	@Column(name="IMPBONCOMIS")
	private BigDecimal impBonComis;
	
	@Column(name="IMPDERECHOS")
	private BigDecimal impDerechos;
	
	@Column(name="IMPRCGOSPAGOFR")
	private BigDecimal impRcgosPagoFR;
	
	@Column(name="IMPIVA")
	private BigDecimal impIVA;
	
	@Column(name="IMPOTROSIMPTOS")
	private BigDecimal impOtrosImptos;
	
	@Column(name="IMPPRIMATOTAL")
	private BigDecimal impPrimaTotal;
	
	@Column(name="IMPCOMAGTPN")
	private BigDecimal impComAgtPN;
	
	@Column(name="IMPCOMAGTRPF")
	private BigDecimal impComAgtRPF;
	
	@Column(name="IMPCOMAGT")
	private BigDecimal impComAgt;
	
	@Column(name="IMPCOMSUPPN")
	private BigDecimal impComSupPN;
	
	@Column(name="IMPCOMSUPRPF")
	private BigDecimal impComSupRPF;
	
	@Column(name="IMPCOMSUP")
	private BigDecimal impComSup;
	
	@Column(name="IMPSOBRECOMAGT")
	private BigDecimal impSobreComAgt;
	
	@Column(name="IMPSOBRECOMPROM")
	private BigDecimal impSobreComProm;
	
	@Column(name="IMPCESIONDERECHOSAGT")
	private BigDecimal impCesionDerechosAgt;
	
	@Column(name="IMPCESIONDERECHOSPROM")
	private BigDecimal impCesionDerechosProm;
	
	@Column(name="IMPSOBRECOMUDIAGT")
	private BigDecimal impSobreComUdiAgt;
	
	@Column(name="IMPSOBRECOMUDIPROM")
	private BigDecimal impSobreComUdiProm;
	
	@Column(name="IMPBONOAGT")
	private BigDecimal impBonoAgt;
	
	@Column(name="IMPBOMOPROM")
	private BigDecimal impBomoProm;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ToProgPago getProgPago() {
		return progPago;
	}

	public void setProgPago(ToProgPago progPago) {
		this.progPago = progPago;
	}

	public Integer getNumExhibicion() {
		return numExhibicion;
	}

	public void setNumExhibicion(Integer numExhibicion) {
		this.numExhibicion = numExhibicion;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	public String getCveTipoDocumento() {
		return cveTipoDocumento;
	}

	public void setCveTipoDocumento(String cveTipoDocumento) {
		this.cveTipoDocumento = cveTipoDocumento;
	}

	public String getCveRecibo() {
		return cveRecibo;
	}

	public void setCveRecibo(String cveRecibo) {
		this.cveRecibo = cveRecibo;
	}

	public String getSitRecibo() {
		return sitRecibo;
	}

	public void setSitRecibo(String sitRecibo) {
		this.sitRecibo = sitRecibo;
	}

	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public Date getFechaCubreDesde() {
		return fechaCubreDesde;
	}

	public void setFechaCubreDesde(Date fechaCubreDesde) {
		this.fechaCubreDesde = fechaCubreDesde;
	}

	public Date getFechaCubreHasta() {
		return fechaCubreHasta;
	}

	public void setFechaCubreHasta(Date fechaCubreHasta) {
		this.fechaCubreHasta = fechaCubreHasta;
	}

	public BigDecimal getImpPrimaNeta() {
		return impPrimaNeta;
	}

	public void setImpPrimaNeta(BigDecimal impPrimaNeta) {
		this.impPrimaNeta = impPrimaNeta;
	}

	public BigDecimal getImpBonComPN() {
		return impBonComPN;
	}

	public void setImpBonComPN(BigDecimal impBonComPN) {
		this.impBonComPN = impBonComPN;
	}

	public BigDecimal getImpBonComRPF() {
		return impBonComRPF;
	}

	public void setImpBonComRPF(BigDecimal impBonComRPF) {
		this.impBonComRPF = impBonComRPF;
	}

	public BigDecimal getImpBonComis() {
		return impBonComis;
	}

	public void setImpBonComis(BigDecimal impBonComis) {
		this.impBonComis = impBonComis;
	}

	public BigDecimal getImpDerechos() {
		return impDerechos;
	}

	public void setImpDerechos(BigDecimal impDerechos) {
		this.impDerechos = impDerechos;
	}

	public BigDecimal getImpRcgosPagoFR() {
		return impRcgosPagoFR;
	}

	public void setImpRcgosPagoFR(BigDecimal impRcgosPagoFR) {
		this.impRcgosPagoFR = impRcgosPagoFR;
	}

	public BigDecimal getImpIVA() {
		return impIVA;
	}

	public void setImpIVA(BigDecimal impIVA) {
		this.impIVA = impIVA;
	}

	public BigDecimal getImpOtrosImptos() {
		return impOtrosImptos;
	}

	public void setImpOtrosImptos(BigDecimal impOtrosImptos) {
		this.impOtrosImptos = impOtrosImptos;
	}

	public BigDecimal getImpPrimaTotal() {
		return impPrimaTotal;
	}

	public void setImpPrimaTotal(BigDecimal impPrimaTotal) {
		this.impPrimaTotal = impPrimaTotal;
	}

	public BigDecimal getImpComAgtPN() {
		return impComAgtPN;
	}

	public void setImpComAgtPN(BigDecimal impComAgtPN) {
		this.impComAgtPN = impComAgtPN;
	}

	public BigDecimal getImpComAgtRPF() {
		return impComAgtRPF;
	}

	public void setImpComAgtRPF(BigDecimal impComAgtRPF) {
		this.impComAgtRPF = impComAgtRPF;
	}

	public BigDecimal getImpComAgt() {
		return impComAgt;
	}

	public void setImpComAgt(BigDecimal impComAgt) {
		this.impComAgt = impComAgt;
	}

	public BigDecimal getImpComSupPN() {
		return impComSupPN;
	}

	public void setImpComSupPN(BigDecimal impComSupPN) {
		this.impComSupPN = impComSupPN;
	}

	public BigDecimal getImpComSupRPF() {
		return impComSupRPF;
	}

	public void setImpComSupRPF(BigDecimal impComSupRPF) {
		this.impComSupRPF = impComSupRPF;
	}

	public BigDecimal getImpComSup() {
		return impComSup;
	}

	public void setImpComSup(BigDecimal impComSup) {
		this.impComSup = impComSup;
	}

	public BigDecimal getImpSobreComAgt() {
		return impSobreComAgt;
	}

	public void setImpSobreComAgt(BigDecimal impSobreComAgt) {
		this.impSobreComAgt = impSobreComAgt;
	}

	public BigDecimal getImpSobreComProm() {
		return impSobreComProm;
	}

	public void setImpSobreComProm(BigDecimal impSobreComProm) {
		this.impSobreComProm = impSobreComProm;
	}

	public BigDecimal getImpCesionDerechosAgt() {
		return impCesionDerechosAgt;
	}

	public void setImpCesionDerechosAgt(BigDecimal impCesionDerechosAgt) {
		this.impCesionDerechosAgt = impCesionDerechosAgt;
	}

	public BigDecimal getImpCesionDerechosProm() {
		return impCesionDerechosProm;
	}

	public void setImpCesionDerechosProm(BigDecimal impCesionDerechosProm) {
		this.impCesionDerechosProm = impCesionDerechosProm;
	}

	public BigDecimal getImpSobreComUdiAgt() {
		return impSobreComUdiAgt;
	}

	public void setImpSobreComUdiAgt(BigDecimal impSobreComUdiAgt) {
		this.impSobreComUdiAgt = impSobreComUdiAgt;
	}

	public BigDecimal getImpSobreComUdiProm() {
		return impSobreComUdiProm;
	}

	public void setImpSobreComUdiProm(BigDecimal impSobreComUdiProm) {
		this.impSobreComUdiProm = impSobreComUdiProm;
	}

	public BigDecimal getImpBonoAgt() {
		return impBonoAgt;
	}

	public void setImpBonoAgt(BigDecimal impBonoAgt) {
		this.impBonoAgt = impBonoAgt;
	}

	public BigDecimal getImpBomoProm() {
		return impBomoProm;
	}

	public void setImpBomoProm(BigDecimal impBomoProm) {
		this.impBomoProm = impBomoProm;
	}
	
	public List<ToDesgloseRecibo> getDeglose() {
		return deglose;
	}

	public void setDeglose(List<ToDesgloseRecibo> deglose) {
		this.deglose = deglose;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return progPago.getValue().concat("-").concat(id.toString());
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getBusinessKey() {
		return progPago.getBusinessKey().concat("-").concat(id.toString());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((codigoUsuarioCreacion == null) ? 0 : codigoUsuarioCreacion
						.hashCode());
		result = prime * result
				+ ((cveRecibo == null) ? 0 : cveRecibo.hashCode());
		result = prime
				* result
				+ ((cveTipoDocumento == null) ? 0 : cveTipoDocumento.hashCode());
		result = prime * result + ((deglose == null) ? 0 : deglose.hashCode());
		result = prime * result
				+ ((fechaCubreDesde == null) ? 0 : fechaCubreDesde.hashCode());
		result = prime * result
				+ ((fechaCubreHasta == null) ? 0 : fechaCubreHasta.hashCode());
		result = prime * result
				+ ((fechaRegistro == null) ? 0 : fechaRegistro.hashCode());
		result = prime
				* result
				+ ((fechaVencimiento == null) ? 0 : fechaVencimiento.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((impBomoProm == null) ? 0 : impBomoProm.hashCode());
		result = prime * result
				+ ((impBonComPN == null) ? 0 : impBonComPN.hashCode());
		result = prime * result
				+ ((impBonComRPF == null) ? 0 : impBonComRPF.hashCode());
		result = prime * result
				+ ((impBonComis == null) ? 0 : impBonComis.hashCode());
		result = prime * result
				+ ((impBonoAgt == null) ? 0 : impBonoAgt.hashCode());
		result = prime
				* result
				+ ((impCesionDerechosAgt == null) ? 0 : impCesionDerechosAgt
						.hashCode());
		result = prime
				* result
				+ ((impCesionDerechosProm == null) ? 0 : impCesionDerechosProm
						.hashCode());
		result = prime * result
				+ ((impComAgt == null) ? 0 : impComAgt.hashCode());
		result = prime * result
				+ ((impComAgtPN == null) ? 0 : impComAgtPN.hashCode());
		result = prime * result
				+ ((impComAgtRPF == null) ? 0 : impComAgtRPF.hashCode());
		result = prime * result
				+ ((impComSup == null) ? 0 : impComSup.hashCode());
		result = prime * result
				+ ((impComSupPN == null) ? 0 : impComSupPN.hashCode());
		result = prime * result
				+ ((impComSupRPF == null) ? 0 : impComSupRPF.hashCode());
		result = prime * result
				+ ((impDerechos == null) ? 0 : impDerechos.hashCode());
		result = prime * result + ((impIVA == null) ? 0 : impIVA.hashCode());
		result = prime * result
				+ ((impOtrosImptos == null) ? 0 : impOtrosImptos.hashCode());
		result = prime * result
				+ ((impPrimaNeta == null) ? 0 : impPrimaNeta.hashCode());
		result = prime * result
				+ ((impPrimaTotal == null) ? 0 : impPrimaTotal.hashCode());
		result = prime * result
				+ ((impRcgosPagoFR == null) ? 0 : impRcgosPagoFR.hashCode());
		result = prime * result
				+ ((impSobreComAgt == null) ? 0 : impSobreComAgt.hashCode());
		result = prime * result
				+ ((impSobreComProm == null) ? 0 : impSobreComProm.hashCode());
		result = prime
				* result
				+ ((impSobreComUdiAgt == null) ? 0 : impSobreComUdiAgt
						.hashCode());
		result = prime
				* result
				+ ((impSobreComUdiProm == null) ? 0 : impSobreComUdiProm
						.hashCode());
		result = prime * result
				+ ((numExhibicion == null) ? 0 : numExhibicion.hashCode());
		result = prime * result
				+ ((progPago == null) ? 0 : progPago.hashCode());
		result = prime * result
				+ ((sitRecibo == null) ? 0 : sitRecibo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		ToRecibo other = (ToRecibo) obj;
		
//		if (codigoUsuarioCreacion == null) {
//			if (other.codigoUsuarioCreacion != null)
//				return false;
//		} else if (!codigoUsuarioCreacion.equals(other.codigoUsuarioCreacion))
//			return false;
		
//		if (cveRecibo == null) {
//			if (other.cveRecibo != null)
//				return false;
//		} else if (!cveRecibo.equals(other.cveRecibo))
//			return false;
//		if (cveTipoDocumento == null) {
//			if (other.cveTipoDocumento != null)
//				return false;
//		} else if (!cveTipoDocumento.equals(other.cveTipoDocumento))
//			return false;
//		if (deglose == null) {
//			if (other.deglose != null)
//				return false;
//		} else if (!deglose.equals(other.deglose))
//			return false;
		if (fechaCubreDesde == null) {
			if (other.fechaCubreDesde != null)
				return false;
		} else if (!fechaCubreDesde.equals(other.fechaCubreDesde))
			return false;
		if (fechaCubreHasta == null) {
			if (other.fechaCubreHasta != null)
				return false;
		} else if (!fechaCubreHasta.equals(other.fechaCubreHasta))
			return false;
//		if (fechaRegistro == null) {
//			if (other.fechaRegistro != null)
//				return false;
//		} else if (!fechaRegistro.equals(other.fechaRegistro))
//			return false;
		if (fechaVencimiento == null) {
			if (other.fechaVencimiento != null)
				return false;
		} else if (!fechaVencimiento.equals(other.fechaVencimiento))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
//		if (impBomoProm == null) {
//			if (other.impBomoProm != null)
//				return false;
//		} else if (!impBomoProm.equals(other.impBomoProm))
//			return false;
//		if (impBonComPN == null) {
//			if (other.impBonComPN != null)
//				return false;
//		} else if (!impBonComPN.equals(other.impBonComPN))
//			return false;
//		if (impBonComRPF == null) {
//			if (other.impBonComRPF != null)
//				return false;
//		} else if (!impBonComRPF.equals(other.impBonComRPF))
//			return false;
//		if (impBonComis == null) {
//			if (other.impBonComis != null)
//				return false;
//		} else if (!impBonComis.equals(other.impBonComis))
//			return false;
//		if (impBonoAgt == null) {
//			if (other.impBonoAgt != null)
//				return false;
//		} else if (!impBonoAgt.equals(other.impBonoAgt))
//			return false;
//		if (impCesionDerechosAgt == null) {
//			if (other.impCesionDerechosAgt != null)
//				return false;
//		} else if (!impCesionDerechosAgt.equals(other.impCesionDerechosAgt))
//			return false;
//		if (impCesionDerechosProm == null) {
//			if (other.impCesionDerechosProm != null)
//				return false;
//		} else if (!impCesionDerechosProm.equals(other.impCesionDerechosProm))
//			return false;
//		if (impComAgt == null) {
//			if (other.impComAgt != null)
//				return false;
//		} else if (!impComAgt.equals(other.impComAgt))
//			return false;
//		if (impComAgtPN == null) {
//			if (other.impComAgtPN != null)
//				return false;
//		} else if (!impComAgtPN.equals(other.impComAgtPN))
//			return false;
//		if (impComAgtRPF == null) {
//			if (other.impComAgtRPF != null)
//				return false;
//		} else if (!impComAgtRPF.equals(other.impComAgtRPF))
//			return false;
//		if (impComSup == null) {
//			if (other.impComSup != null)
//				return false;
//		} else if (!impComSup.equals(other.impComSup))
//			return false;
//		if (impComSupPN == null) {
//			if (other.impComSupPN != null)
//				return false;
//		} else if (!impComSupPN.equals(other.impComSupPN))
//			return false;
//		if (impComSupRPF == null) {
//			if (other.impComSupRPF != null)
//				return false;
//		} else if (!impComSupRPF.equals(other.impComSupRPF))
//			return false;
		if (impDerechos == null) {
			if (other.impDerechos != null)
				return false;
		} else if (!impDerechos.equals(other.impDerechos))
			return false;
		if (impIVA == null) {
			if (other.impIVA != null)
				return false;
		} else if (!impIVA.equals(other.impIVA))
			return false;
//		if (impOtrosImptos == null) {
//			if (other.impOtrosImptos != null)
//				return false;
//		} else if (!impOtrosImptos.equals(other.impOtrosImptos))
//			return false;
		if (impPrimaNeta == null) {
			if (other.impPrimaNeta != null)
				return false;
		} else if (!impPrimaNeta.equals(other.impPrimaNeta))
			return false;
		if (impPrimaTotal == null) {
			if (other.impPrimaTotal != null)
				return false;
		} else if (!impPrimaTotal.equals(other.impPrimaTotal))
			return false;
		if (impRcgosPagoFR == null) {
			if (other.impRcgosPagoFR != null)
				return false;
		} else if (!impRcgosPagoFR.equals(other.impRcgosPagoFR))
			return false;
//		if (impSobreComAgt == null) {
//			if (other.impSobreComAgt != null)
//				return false;
//		} else if (!impSobreComAgt.equals(other.impSobreComAgt))
//			return false;
//		if (impSobreComProm == null) {
//			if (other.impSobreComProm != null)
//				return false;
//		} else if (!impSobreComProm.equals(other.impSobreComProm))
//			return false;
//		if (impSobreComUdiAgt == null) {
//			if (other.impSobreComUdiAgt != null)
//				return false;
//		} else if (!impSobreComUdiAgt.equals(other.impSobreComUdiAgt))
//			return false;
//		if (impSobreComUdiProm == null) {
//			if (other.impSobreComUdiProm != null)
//				return false;
//		} else if (!impSobreComUdiProm.equals(other.impSobreComUdiProm))
//			return false;
		if (numExhibicion == null) {
			if (other.numExhibicion != null)
				return false;
		} else if (!numExhibicion.equals(other.numExhibicion))
			return false;
//		if (progPago == null) {
//			if (other.progPago != null)
//				return false;
//		} else if (!progPago.equals(other.progPago))
//			return false;
//		if (sitRecibo == null) {
//			if (other.sitRecibo != null)
//				return false;
//		} else if (!sitRecibo.equals(other.sitRecibo))
//			return false;

		return true;
	}

}
