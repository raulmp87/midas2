package mx.com.afirme.midas2.domain.cobranza.pagos;

public class PROSAMessage {
	public enum Type{
		SUCCESS("success"),
		INFO("info"),
		WARNING("warning"),
		DANGER("danger");
		
		private final String type;
		
		private Type(String type){
			this.type = type;
		}
		
		public String getType(){
			return this.type;
		}
	}
	
	private Type type;
	private String message;
	
	public PROSAMessage(Type type, String message) {
		this.type = type;
		this.message = message;
	}
	public String getType() {
		return type.getType();
	}
	public void setType(Type type) {
		this.type = type;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}	
}