package mx.com.afirme.midas2.domain.cobranza.programapago;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

@Entity
@Table(name="TODESGLOSERECIBO", schema="MIDAS")
public class ToDesgloseRecibo implements Entidad {
	
	private static final long serialVersionUID = 4838513641277825879L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TODESGLOSERECIBO_ID_GENERATOR")	
	@SequenceGenerator(name="TODESGLOSERECIBO_ID_GENERATOR", sequenceName="MIDAS.TODESGLOSERECIBO_SEQ", allocationSize=100)	
	@Column(name="ID")
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	 @JoinColumns( { 
        @JoinColumn(name="IDTOCOTIZACION", referencedColumnName="IDTOCOTIZACION", nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="NUMEROINCISO", referencedColumnName="NUMEROINCISO", nullable=false, insertable=false, updatable=false), 
        @JoinColumn(name="IDTOSECCION", referencedColumnName="IDTOSECCION", nullable=false, insertable=false, updatable=false),
        @JoinColumn(name="IDTOCOBERTURA", referencedColumnName="IDTOCOBERTURA", nullable=false, insertable=false, updatable=false)} )
    private CoberturaCotizacionDTO cobertura;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinFetch(JoinFetchType.INNER)
	@JoinColumn(name="RECIBO_ID", nullable=false)
	private ToRecibo recibo;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAREGISTRO")
	private Date fechaRegistro;
	
	@Column(name="CODIGOUSUARIOCREACION")
	private String codigoUsuarioCreacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="fechaCubreDesde")
	private Date fechaCubreDesde;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHACUBREHASTA")
	private Date fechaCubreHasta;
	
	@Column(name="IMPPRIMANETA")
	private BigDecimal impPrimaNeta;
	
	@Column(name="IMPBONCOMPN")
	private BigDecimal impBonComPN;
	
	@Column(name="IMPBONCOMRPF")
	private BigDecimal impBonComRPF;
	
	@Column(name="IMPBONCOMIS")
	private BigDecimal impBonComis;
	
	@Column(name="IMPDERECHOS")
	private BigDecimal impDerechos;
	
	@Column(name="IMPRCGOSPAGOFR")
	private BigDecimal impRcgosPagoFR;
	
	@Column(name="IMPIVA")
	private BigDecimal impIVA;
	
	@Column(name="IMPOTROSIMPTOS")
	private BigDecimal impOtrosImptos;
	
	@Column(name="IMPPRIMATOTAL")
	private BigDecimal impPrimaTotal;
	
	@Column(name="IMPCOMAGTPN")
	private BigDecimal impComAgtPN;
	
	@Column(name="IMPCOMAGTRPF")
	private BigDecimal impComAgtRPF;
	
	@Column(name="IMPCOMAGT")
	private BigDecimal impComAgt;
	
	@Column(name="IMPCOMSUPPN")
	private BigDecimal impComSupPN;
	
	@Column(name="IMPCOMSUPRPF")
	private BigDecimal impComSupRPF;
	
	@Column(name="IMPCOMSUP")
	private BigDecimal impComSup;
	
	@Column(name="IMPSOBRECOMAGT")
	private BigDecimal impSobreComAgt;
	
	@Column(name="IMPSOBRECOMPROM")
	private BigDecimal impSobreComProm;
	
	@Column(name="IMPCESIONDERECHOSAGT")
	private BigDecimal impCesionDerechosAgt;
	
	@Column(name="IMPCESIONDERECHOSPROM")
	private BigDecimal impCesionDerechosProm;
	
	@Column(name="IMPSOBRECOMUDIAGT")
	private BigDecimal impSobreComUdiAgt;
	
	@Column(name="IMPSOBRECOMUDIPROM")
	private BigDecimal impSobreComUdiProm;
	
	@Column(name="IMPBONOAGT")
	private BigDecimal impBonoAgt;
	
	@Column(name="IMPBOMOPROM")
	private BigDecimal impBomoProm;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CoberturaCotizacionDTO getCobertura() {
		return cobertura;
	}

	public void setCobertura(CoberturaCotizacionDTO cobertura) {
		this.cobertura = cobertura;
	}

	public ToRecibo getRecibo() {
		return recibo;
	}

	public void setRecibo(ToRecibo recibo) {
		this.recibo = recibo;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	public Date getFechaCubreDesde() {
		return fechaCubreDesde;
	}

	public void setFechaCubreDesde(Date fechaCubreDesde) {
		this.fechaCubreDesde = fechaCubreDesde;
	}

	public Date getFechaCubreHasta() {
		return fechaCubreHasta;
	}

	public void setFechaCubreHasta(Date fechaCubreHasta) {
		this.fechaCubreHasta = fechaCubreHasta;
	}

	public BigDecimal getImpPrimaNeta() {
		return impPrimaNeta;
	}

	public void setImpPrimaNeta(BigDecimal impPrimaNeta) {
		this.impPrimaNeta = impPrimaNeta;
	}

	public BigDecimal getImpBonComPN() {
		return impBonComPN;
	}

	public void setImpBonComPN(BigDecimal impBonComPN) {
		this.impBonComPN = impBonComPN;
	}

	public BigDecimal getImpBonComRPF() {
		return impBonComRPF;
	}

	public void setImpBonComRPF(BigDecimal impBonComRPF) {
		this.impBonComRPF = impBonComRPF;
	}

	public BigDecimal getImpBonComis() {
		return impBonComis;
	}

	public void setImpBonComis(BigDecimal impBonComis) {
		this.impBonComis = impBonComis;
	}

	public BigDecimal getImpDerechos() {
		return impDerechos;
	}

	public void setImpDerechos(BigDecimal impDerechos) {
		this.impDerechos = impDerechos;
	}

	public BigDecimal getImpRcgosPagoFR() {
		return impRcgosPagoFR;
	}

	public void setImpRcgosPagoFR(BigDecimal impRcgosPagoFR) {
		this.impRcgosPagoFR = impRcgosPagoFR;
	}

	public BigDecimal getImpIVA() {
		return impIVA;
	}

	public void setImpIVA(BigDecimal impIVA) {
		this.impIVA = impIVA;
	}

	public BigDecimal getImpOtrosImptos() {
		return impOtrosImptos;
	}

	public void setImpOtrosImptos(BigDecimal impOtrosImptos) {
		this.impOtrosImptos = impOtrosImptos;
	}

	public BigDecimal getImpPrimaTotal() {
		return impPrimaTotal;
	}

	public void setImpPrimaTotal(BigDecimal impPrimaTotal) {
		this.impPrimaTotal = impPrimaTotal;
	}

	public BigDecimal getImpComAgtPN() {
		return impComAgtPN;
	}

	public void setImpComAgtPN(BigDecimal impComAgtPN) {
		this.impComAgtPN = impComAgtPN;
	}

	public BigDecimal getImpComAgtRPF() {
		return impComAgtRPF;
	}

	public void setImpComAgtRPF(BigDecimal impComAgtRPF) {
		this.impComAgtRPF = impComAgtRPF;
	}

	public BigDecimal getImpComAgt() {
		return impComAgt;
	}

	public void setImpComAgt(BigDecimal impComAgt) {
		this.impComAgt = impComAgt;
	}

	public BigDecimal getImpComSupPN() {
		return impComSupPN;
	}

	public void setImpComSupPN(BigDecimal impComSupPN) {
		this.impComSupPN = impComSupPN;
	}

	public BigDecimal getImpComSupRPF() {
		return impComSupRPF;
	}

	public void setImpComSupRPF(BigDecimal impComSupRPF) {
		this.impComSupRPF = impComSupRPF;
	}

	public BigDecimal getImpComSup() {
		return impComSup;
	}

	public void setImpComSup(BigDecimal impComSup) {
		this.impComSup = impComSup;
	}

	public BigDecimal getImpSobreComAgt() {
		return impSobreComAgt;
	}

	public void setImpSobreComAgt(BigDecimal impSobreComAgt) {
		this.impSobreComAgt = impSobreComAgt;
	}

	public BigDecimal getImpSobreComProm() {
		return impSobreComProm;
	}

	public void setImpSobreComProm(BigDecimal impSobreComProm) {
		this.impSobreComProm = impSobreComProm;
	}

	public BigDecimal getImpCesionDerechosAgt() {
		return impCesionDerechosAgt;
	}

	public void setImpCesionDerechosAgt(BigDecimal impCesionDerechosAgt) {
		this.impCesionDerechosAgt = impCesionDerechosAgt;
	}

	public BigDecimal getImpCesionDerechosProm() {
		return impCesionDerechosProm;
	}

	public void setImpCesionDerechosProm(BigDecimal impCesionDerechosProm) {
		this.impCesionDerechosProm = impCesionDerechosProm;
	}

	public BigDecimal getImpSobreComUdiAgt() {
		return impSobreComUdiAgt;
	}

	public void setImpSobreComUdiAgt(BigDecimal impSobreComUdiAgt) {
		this.impSobreComUdiAgt = impSobreComUdiAgt;
	}

	public BigDecimal getImpSobreComUdiProm() {
		return impSobreComUdiProm;
	}

	public void setImpSobreComUdiProm(BigDecimal impSobreComUdiProm) {
		this.impSobreComUdiProm = impSobreComUdiProm;
	}

	public BigDecimal getImpBonoAgt() {
		return impBonoAgt;
	}

	public void setImpBonoAgt(BigDecimal impBonoAgt) {
		this.impBonoAgt = impBonoAgt;
	}

	public BigDecimal getImpBomoProm() {
		return impBomoProm;
	}

	public void setImpBomoProm(BigDecimal impBomoProm) {
		this.impBomoProm = impBomoProm;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {		
		return recibo.getValue().concat("-").concat(id.toString());
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getBusinessKey() {
		return recibo.getBusinessKey().concat("-").concat(id.toString());
	}
}
