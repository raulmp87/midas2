package mx.com.afirme.midas2.domain.cobranza.cliente;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * CotizacionDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TORELCOTIZACIONCOBRANZA", schema = "MIDAS")
public class CotizacionCobranza implements Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTOCOTIZACION", nullable = false)
	private CotizacionDTO cotizacionDTO;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TORELCOTIZACIONCOBRANZA_SEQ")
	@SequenceGenerator(name = "TORELCOTIZACIONCOBRANZA_SEQ", schema="MIDAS", sequenceName = "TORELCOTIZACIONCOBRANZA_SEQ", allocationSize = 1)
	private BigDecimal id;

	@Column(name="ID_CONDUCTO")
	private Integer idConducto;
	
	@Column(name="ID_COND_COBRO")
	private Integer idConductoCobro;
	
	@Column(name="CLIENTEESTITULAR")
	private boolean clienteEsTitular;
	
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}
	
	public Integer getIdConducto() {
		return idConducto;
	}
	
	public void setIdConducto(Integer idConducto) {
		this.idConducto = idConducto;
	}
	
	public Integer getIdConductoCobro() {
		return idConductoCobro;
	}
	
	public void setIdConductoCobro(Integer idConductoCobro) {
		this.idConductoCobro = idConductoCobro;
	}
	
	public boolean getClienteEsTitular() {
		return clienteEsTitular;
	}
	
	public void setClienteEsTitular(boolean clienteEsTitular) {
		this.clienteEsTitular = clienteEsTitular;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public CotizacionDTO getCotizacionDTO() {
		return cotizacionDTO;
	}

	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}
}