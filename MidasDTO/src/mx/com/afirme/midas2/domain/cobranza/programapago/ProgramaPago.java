package mx.com.afirme.midas2.domain.cobranza.programapago;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;

public class ProgramaPago implements Serializable{
	
	private static final long serialVersionUID = -8290937472044868504L;
	private List<ToProgPago> listadoDeProgramasDePago = new ArrayList<ToProgPago>();
	private BigDecimal idToCotizacion;
	private Long idProgPago;
	CotizacionDTO cotizacionDTO;
	private String tipoMov;
	
	//Valores de endoso
	private BigDecimal idContinuity = BigDecimal.ZERO;
	private String tipoFormaPago = "0";
	private BigDecimal idToSolicitud = BigDecimal.ZERO;
	private BigDecimal idToPoliza = BigDecimal.ZERO;
	private BigDecimal numeroEndoso = BigDecimal.ZERO;
	
	private Boolean tieneRolRecuotificacion = Boolean.FALSE;
	private Boolean hasEditPrimaTotalPermit = Boolean.FALSE;
	private BigDecimal ajustePermitidoPrimaTotal = BigDecimal.ZERO;

	
	

	public BigDecimal getAjustePermitidoPrimaTotal() {
		return ajustePermitidoPrimaTotal;
	}

	public void setAjustePermitidoPrimaTotal(BigDecimal ajustePermitidoPrimaTotal) {
		this.ajustePermitidoPrimaTotal = ajustePermitidoPrimaTotal;
	}

	public Boolean getHasEditPrimaTotalPermit() {
		return hasEditPrimaTotalPermit;
	}

	public void setHasEditPrimaTotalPermit(Boolean hasEditPrimaTotalPermit) {
		this.hasEditPrimaTotalPermit = hasEditPrimaTotalPermit;
	}

	public Boolean getTieneRolRecuotificacion() {
		return tieneRolRecuotificacion;
	}

	public void setTieneRolRecuotificacion(Boolean tieneRolRecuotificacion) {
		this.tieneRolRecuotificacion = tieneRolRecuotificacion;
	}

	public BigDecimal getNumeroEndoso() {
		return numeroEndoso;
	}

	public void setNumeroEndoso(BigDecimal numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}

	public BigDecimal getIdContinuity() {
		return idContinuity;
	}

	public void setIdContinuity(BigDecimal idContinuity) {
		this.idContinuity = idContinuity;
	}

	public String getTipoFormaPago() {
		return tipoFormaPago;
	}

	public void setTipoFormaPago(String tipoFormaPago) {
		this.tipoFormaPago = tipoFormaPago;
	}

	public BigDecimal getIdToSolicitud() {
		return idToSolicitud;
	}

	public void setIdToSolicitud(BigDecimal idToSolicitud) {
		this.idToSolicitud = idToSolicitud;
	}

	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}
	
	public String getTipoMov() {
		return tipoMov;
	}

	public void setTipoMov(String tipoMov) {
		this.tipoMov = tipoMov;
	}

	public CotizacionDTO getCotizacionDTO() {
		return cotizacionDTO;
	}

	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}

	private boolean readOnly;
	
	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	//Datos del contratante
	private String nombreDelContrante;
	public String getNombreDelContrante() {
		return nombreDelContrante;
	}

	public void setNombreDelContrante(String nombreDelContrante) {
		this.nombreDelContrante = nombreDelContrante;
	}

	public String getDireccionFiscal() {
		return direccionFiscal;
	}

	public void setDireccionFiscal(String direccionFiscal) {
		this.direccionFiscal = direccionFiscal;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	private String direccionFiscal;
	private String rfc;
	
	
	public Long getIdProgPago() {
		return idProgPago;
	}

	public void setIdProgPago(Long idProgPago) {
		this.idProgPago = idProgPago;
	}

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	
	public List<ToProgPago> getListadoDeProgramasDePago() {
		return listadoDeProgramasDePago;
	}

	public void setListadoDeProgramasDePago(
			List<ToProgPago> listadoDeProgramasDePago) {
		this.listadoDeProgramasDePago = listadoDeProgramasDePago;
	}

}
