package mx.com.afirme.midas2.domain.movil.cotizador;

public class ValidacionEstatusUsuarioActual implements java.io.Serializable {
	// Fields
	private String mensaje;
	private boolean isActivo;
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public boolean isActivo() {
		return isActivo;
	}
	public void setActivo(boolean isActivo) {
		this.isActivo = isActivo;
	}
}