package mx.com.afirme.midas2.domain.movil.ajustador;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import mx.com.afirme.midas2.domain.Coordenadas;

public class ReporteSiniestro implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4328598198163501438L;
	private Long id;
	private Long numeroReporte;
	private String polizaId;
	private String poliza;
	private Integer numeroInciso;
	private String tipoInciso; // MOTOS, AUTOS, CAMN etc 
	private String descripcionInciso;
	private String estatusInciso;
	private String pais;
	private String estado;
	private String ciudad;
	private String calleNumero;
	private String colonia;
	private String codigoPostal;
	private String referencia;
	private String kilometro;
	private Coordenadas ubicacionCoordenadas;
	private String vehiculosParticipantes;
	private String nombreReporta;
	private String nombreConductor;
	private String causaSiniestro;
	private String telefono;
	private String tipo;
	private Ajustador ajustador;
	private Date fechaReporte;
	private Date fechaOcurrido;
	private Date fechaCita;
	private Date fechaContacto;
	private Date fechaTerminacion;
	private Date fechaEnteradoAsignacion;
	private String horaOcurrido;
	private String numeroReporteCabina;
	private Date fechaAsignacion;
	
	private Long tipoSiniestro;
	private String terminoAjuste;
	private String responsabilidad;
	private Integer porcientoResponsabilidad;
	private String declaracion;	
	private Integer ciaRespId;
	
	private boolean conductorMismoAsegurado;
	private String conductor;
	private Integer edad;
	private String sexo;
	private String curp;
	private String rfc;
	private String telefonoAseg;
	private String estadoNacimiento;
	private String ladaTelefonoAseg;
	private String ladaCelular;
	private String celular;
	
	private Boolean participoUnidadEqPesado;
	private String lugarAjuste;
	private String codigoTipoSiniestro;
	private String codigoResponsabilidad;
	private String codigoTerminoAjuste;
	private String color;
	private String placa;
	private String tipoLicencia;
	private String terminoSiniestro;
	private boolean seFugoTerceroResp;
	private boolean recibidaOrdenCia;
	private Long numeroValuacion;
	private Short incisoAutorizado;
	private String curpAseg;
	private String rfcAseg;
	private String estatusValuacion;
	private BigDecimal montoValuacion;
	private String estatusVigenciaInciso;
	private String numeroSiniestroCabina;
	private Short claveEstatus;
	private String polizaCia;
	private String siniestroCia;
	private Long idTicket;
	
	/**
	 * El id del expediente en Fortimax. Incialmente no se crea ningun expediente en Fortimax esto sucede hasta que se sube por primera vez un documento.
	 */
	private String expedienteFortimaxId;
	private Long numeroSiniestro;
	private String asegurado;
	private String numeroSerie;
	
	/**
	 * Campos de Origen SIPAC
	 */
	private String origen;
	private String incisoCia;
	private Date fechaExpedicion;
	private Date fechaDictamen;
	private Date fechaJuicio;
	
	/**
	 * Variables para termino de ajuste rechazo
	 */
	private BigDecimal montoDanos;
	private String motivoRechazo;
	private String rechazoDesistimiento;
	private String observacionRechazo;
	
	private ProcedeSiniestro procedeSiniestro;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public Long getNumeroReporte() {
		return numeroReporte;
	}

	public void setNumeroReporte(Long numeroReporte) {
		this.numeroReporte = numeroReporte;
	}
	
	public String getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(String polizaId) {
		this.polizaId = polizaId;
	}

	public String getPoliza() {
		return poliza;
	}
	
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	
	public Integer getNumeroInciso() {
		return numeroInciso;
	}
	
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	
	public String getTipoInciso() {
		return tipoInciso;
	}

	public void setTipoInciso(String tipoInciso) {
		this.tipoInciso = tipoInciso;
	}	

	public String getDescripcionInciso() {
		return descripcionInciso;
	}

	public void setDescripcionInciso(String descripcionInciso) {
		this.descripcionInciso = descripcionInciso;
	}
	
	public String getEstatusInciso() {
		return estatusInciso;
	}

	public void setEstatusInciso(String estatusInciso) {
		this.estatusInciso = estatusInciso;
	}

	public String getPais() {
		return pais;
	}
	
	public void setPais(String pais) {
		this.pais = pais;
	}
	
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getCalleNumero() {
		return calleNumero;
	}

	public void setCalleNumero(String calleNumero) {
		this.calleNumero = calleNumero;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getKilometro() {
		return kilometro;
	}

	public void setKilometro(String kilometro) {
		this.kilometro = kilometro;
	}

	public Coordenadas getUbicacionCoordenadas() {
		return ubicacionCoordenadas;
	}
	
	public void setUbicacionCoordenadas(Coordenadas ubicacionCoordenadas) {
		this.ubicacionCoordenadas = ubicacionCoordenadas;
	}
	
	public String getVehiculosParticipantes() {
		return vehiculosParticipantes;
	}

	public void setVehiculosParticipantes(String vehiculosParticipantes) {
		this.vehiculosParticipantes = vehiculosParticipantes;
	}

	public String getNombreReporta() {
		return nombreReporta;
	}

	public void setNombreReporta(String nombreReporta) {
		this.nombreReporta = nombreReporta;
	}

	public String getNombreConductor() {
		return nombreConductor;
	}

	public void setNombreConductor(String nombreConductor) {
		this.nombreConductor = nombreConductor;
	}

	public String getCausaSiniestro() {
		return causaSiniestro;
	}

	public void setCausaSiniestro(String causaSiniestro) {
		this.causaSiniestro = causaSiniestro;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public Ajustador getAjustador() {
		return ajustador;
	}
	
	public void setAjustador(Ajustador ajustador) {
		this.ajustador = ajustador;
	}

	public Date getFechaReporte() {
		return fechaReporte;
	}

	public void setFechaReporte(Date fechaReporte) {
		this.fechaReporte = fechaReporte;
	}
	
	public Date getFechaOcurrido() {
		return fechaOcurrido;
	}
	
	public void setFechaOcurrido(Date fechaOcurrido) {
		this.fechaOcurrido = fechaOcurrido;
	}

	public Date getFechaCita() {
		return fechaCita;
	}

	public void setFechaCita(Date fechaCita) {
		this.fechaCita = fechaCita;
	}

	public Date getFechaContacto() {
		return fechaContacto;
	}

	public void setFechaContacto(Date fechaContacto) {
		this.fechaContacto = fechaContacto;
	}
	
	public Date getFechaTerminacion() {
		return fechaTerminacion;
	}
	
	public void setFechaTerminacion(Date fechaTerminacion) {
		this.fechaTerminacion = fechaTerminacion;
	}
	
	public Date getFechaEnteradoAsignacion() {
		return fechaEnteradoAsignacion;
	}
	
	public void setFechaEnteradoAsignacion(Date fechaEnteradoAsignacion) {
		this.fechaEnteradoAsignacion = fechaEnteradoAsignacion;
	}

	public Long getTipoSiniestro() {
		return tipoSiniestro;
	}

	public void setTipoSiniestro(Long tipoSiniestro) {
		this.tipoSiniestro = tipoSiniestro;
	}

	public String getTerminoAjuste() {
		return terminoAjuste;
	}

	public void setTerminoAjuste(String terminoAjuste) {
		this.terminoAjuste = terminoAjuste;
	}

	public String getResponsabilidad() {
		return responsabilidad;
	}

	public void setResponsabilidad(String responsabilidad) {
		this.responsabilidad = responsabilidad;
	}
	
	public Integer getCiaRespId() {
		return ciaRespId;
	}

	public void setCiaRespId(Integer ciaRespId) {
		this.ciaRespId = ciaRespId;
	}

	public String getDeclaracion() {
		return declaracion;
	}

	public void setDeclaracion(String declaracion) {
		this.declaracion = declaracion;
	}
	
	public boolean isConductorMismoAsegurado() {
		return conductorMismoAsegurado;
	}

	public void setConductorMismoAsegurado(boolean conductorMismoAsegurado) {
		this.conductorMismoAsegurado = conductorMismoAsegurado;
	}

	public String getConductor() {
		return conductor;
	}

	public void setConductor(String conductor) {
		this.conductor = conductor;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	
	public String getTelefonoAseg() {
		return telefonoAseg;
	}

	public void setTelefonoAseg(String telefonoAseg) {
		this.telefonoAseg = telefonoAseg;
	}

	public String getEstadoNacimiento() {
		return estadoNacimiento;
	}

	public void setEstadoNacimiento(String estadoNacimiento) {
		this.estadoNacimiento = estadoNacimiento;
	}

	/**
	 * @return el participoUnidadEqPesado
	 */
	public Boolean getParticipoUnidadEqPesado() {
		return participoUnidadEqPesado;
	}

	/**
	 * @param participoUnidadEqPesado el participoUnidadEqPesado a establecer
	 */
	public void setParticipoUnidadEqPesado(Boolean participoUnidadEqPesado) {
		this.participoUnidadEqPesado = participoUnidadEqPesado;
	}

	public String getLugarAjuste() {
		return lugarAjuste;
	}

	public void setLugarAjuste(String lugarAjuste) {
		this.lugarAjuste = lugarAjuste;
	}

	public String getExpedienteFortimaxId() {
		return expedienteFortimaxId;
	}
	
	public void setExpedienteFortimaxId(String expedienteFortimaxId) {
		this.expedienteFortimaxId = expedienteFortimaxId;
	}
	
	public Long getNumeroSiniestro() {
		return numeroSiniestro;
	}
	
	public void setNumeroSiniestro(Long numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}

	public ProcedeSiniestro getProcedeSiniestro() {
		return procedeSiniestro;
	}

	public void setProcedeSiniestro(ProcedeSiniestro procedeSiniestro) {
		this.procedeSiniestro = procedeSiniestro;
	}

	public void setHoraOcurrido(String horaOcurrido) {
		this.horaOcurrido = horaOcurrido;
	}

	public String getHoraOcurrido() {
		return horaOcurrido;
	}

	public Integer getPorcientoResponsabilidad() {
		return porcientoResponsabilidad;
	}

	public void setPorcientoResponsabilidad(Integer porcientoResponsabilidad) {
		this.porcientoResponsabilidad = porcientoResponsabilidad;
	}

	public void setLadaTelefonoAseg(String ladaTelefonoAseg) {
		this.ladaTelefonoAseg = ladaTelefonoAseg;
	}

	public String getLadaTelefonoAseg() {
		return ladaTelefonoAseg;
	}

	public void setLadaCelular(String ladaCelular) {
		this.ladaCelular = ladaCelular;
	}

	public String getLadaCelular() {
		return ladaCelular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getCelular() {
		return celular;
	}

	public void setCodigoTipoSiniestro(String codigoTipoSiniestro) {
		this.codigoTipoSiniestro = codigoTipoSiniestro;
	}

	public String getCodigoTipoSiniestro() {
		return codigoTipoSiniestro;
	}

	public void setCodigoResponsabilidad(String codigoResponsabilidad) {
		this.codigoResponsabilidad = codigoResponsabilidad;
	}

	public String getCodigoResponsabilidad() {
		return codigoResponsabilidad;
	}

	public void setCodigoTerminoAjuste(String codigoTerminoAjuste) {
		this.codigoTerminoAjuste = codigoTerminoAjuste;
	}

	public String getCodigoTerminoAjuste() {
		return codigoTerminoAjuste;
	}

	public String getNumeroReporteCabina() {
		return numeroReporteCabina;
	}

	public void setNumeroReporteCabina(String numeroReporteCabina) {
		this.numeroReporteCabina = numeroReporteCabina;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getTipoLicencia() {
		return tipoLicencia;
	}

	public void setTipoLicencia(String tipoLicencia) {
		this.tipoLicencia = tipoLicencia;
	}

	public String getTerminoSiniestro() {
		return terminoSiniestro;
	}

	public void setTerminoSiniestro(String terminoSiniestro) {
		this.terminoSiniestro = terminoSiniestro;
	}

	public boolean isSeFugoTerceroResp() {
		return seFugoTerceroResp;
	}

	public void setSeFugoTerceroResp(boolean seFugoTerceroResp) {
		this.seFugoTerceroResp = seFugoTerceroResp;
	}

	public Date getFechaAsignacion() {
		return fechaAsignacion;
	}

	public void setFechaAsignacion(Date fechaAsignacion) {
		this.fechaAsignacion = fechaAsignacion;
	}

	public boolean isRecibidaOrdenCia() {
		return recibidaOrdenCia;
	}

	public void setRecibidaOrdenCia(boolean recibidaOrdenCia) {
		this.recibidaOrdenCia = recibidaOrdenCia;
	}

	public Long getNumeroValuacion() {
		return numeroValuacion;
	}

	public void setNumeroValuacion(Long numeroValuacion) {
		this.numeroValuacion = numeroValuacion;
	}

	public Short getIncisoAutorizado() {
		return incisoAutorizado;
	}

	public void setIncisoAutorizado(Short incisoAutorizado) {
		this.incisoAutorizado = incisoAutorizado;
	}

	public String getCurpAseg() {
		return curpAseg;
	}

	public void setCurpAseg(String curpAseg) {
		this.curpAseg = curpAseg;
	}

	public String getRfcAseg() {
		return rfcAseg;
	}

	public void setRfcAseg(String rfcAseg) {
		this.rfcAseg = rfcAseg;
	}

	public String getEstatusValuacion() {
		return estatusValuacion;
	}

	public void setEstatusValuacion(String estatusValuacion) {
		this.estatusValuacion = estatusValuacion;
	}

	public BigDecimal getMontoValuacion() {
		return montoValuacion;
	}

	public void setMontoValuacion(BigDecimal montoValuacion) {
		this.montoValuacion = montoValuacion;
	}

	public String getEstatusVigenciaInciso() {
		return estatusVigenciaInciso;
	}

	public void setEstatusVigenciaInciso(String estatusVigenciaInciso) {
		this.estatusVigenciaInciso = estatusVigenciaInciso;
	}

	public String getNumeroSiniestroCabina() {
		return numeroSiniestroCabina;
	}

	public void setNumeroSiniestroCabina(String numeroSiniestroCabina) {
		this.numeroSiniestroCabina = numeroSiniestroCabina;
	}

	public String getAsegurado() {
		return asegurado;
	}

	public void setAsegurado(String asegurado) {
		this.asegurado = asegurado;
	}

	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public Short getClaveEstatus() {
		return claveEstatus;
	}

	public void setClaveEstatus(Short claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	public String getPolizaCia() {
		return polizaCia;
	}

	public void setPolizaCia(String polizaCia) {
		this.polizaCia = polizaCia;
	}

	public String getSiniestroCia() {
		return siniestroCia;
	}

	public void setSiniestroCia(String siniestroCia) {
		this.siniestroCia = siniestroCia;
	}
	public Long getIdTicket() {
		return idTicket;
	}
	
	public void setIdTicket(Long idTicket) {
		this.idTicket = idTicket;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getIncisoCia() {
		return incisoCia;
	}

	public void setIncisoCia(String inciso) {
		this.incisoCia = inciso;
	}

	public Date getFechaExpedicion() {
		return fechaExpedicion;
	}

	public void setFechaExpedicion(Date fechaExpedicion) {
		this.fechaExpedicion = fechaExpedicion;
	}

	public Date getFechaDictamen() {
		return fechaDictamen;
	}

	public void setFechaDictamen(Date fechaDictamen) {
		this.fechaDictamen = fechaDictamen;
	}

	public Date getFechaJuicio() {
		return fechaJuicio;
	}

	public void setFechaJuicio(Date fechaJuicio) {
		this.fechaJuicio = fechaJuicio;
	}

	public String getMotivoRechazo() {
		return motivoRechazo;
	}

	public void setMotivoRechazo(String motivoRechazo) {
		this.motivoRechazo = motivoRechazo;
	}

	public String getRechazoDesistimiento() {
		return rechazoDesistimiento;
	}

	public void setRechazoDesistimiento(String rechazoDesistimiento) {
		this.rechazoDesistimiento = rechazoDesistimiento;
	}

	public String getObservacionRechazo() {
		return observacionRechazo;
	}

	public void setObservacionRechazo(String observacionRechazo) {
		this.observacionRechazo = observacionRechazo;
	}

	public BigDecimal getMontoDanos() {
		return montoDanos;
	}

	public void setMontoDanos(BigDecimal montoDanos) {
		this.montoDanos = montoDanos;
	}	
}