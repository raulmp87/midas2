package mx.com.afirme.midas2.domain.movil.cotizador;

public class Resultado  implements java.io.Serializable{
	 private String id;
	 private String resultado;
	public void setId(String id) {
		this.id = id;
	}
	public String getId() {
		return id;
	}
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
	public String getResultado() {
		return resultado;
	}
	 
}
