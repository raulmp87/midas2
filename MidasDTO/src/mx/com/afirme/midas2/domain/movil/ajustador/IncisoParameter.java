package mx.com.afirme.midas2.domain.movil.ajustador;

import java.io.Serializable;

public class IncisoParameter implements Serializable {
	private String id;
	private Long reporteSiniestroId;
	private String codigoTipoSiniestro;
	private String codigoTerminoAjuste;
	private String codigoResponsabilidad;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getReporteSiniestroId() {
		return reporteSiniestroId;
	}
	public void setReporteSiniestroId(Long reporteSiniestroId) {
		this.reporteSiniestroId = reporteSiniestroId;
	}
	public String getCodigoTipoSiniestro() {
		return codigoTipoSiniestro;
	}
	public void setCodigoTipoSiniestro(String codigoTipoSiniestro) {
		this.codigoTipoSiniestro = codigoTipoSiniestro;
	}
	public String getCodigoTerminoAjuste() {
		return codigoTerminoAjuste;
	}
	public void setCodigoTerminoAjuste(String codigoTerminoAjuste) {
		this.codigoTerminoAjuste = codigoTerminoAjuste;
	}
	public String getCodigoResponsabilidad() {
		return codigoResponsabilidad;
	}
	public void setCodigoResponsabilidad(String codigoResponsabilidad) {
		this.codigoResponsabilidad = codigoResponsabilidad;
	}
}
