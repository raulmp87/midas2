package mx.com.afirme.midas2.domain.movil.cotizador;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.Transient;

import mx.com.afirme.midas.base.LogBaseDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dto.impresiones.ResumenCotMovilVidaDTO;

/**
 * Tocotizacionmovil entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOCOTIZACIONMOVIL", schema = "MIDAS")
public class CotizacionMovilDTO  extends LogBaseDTO implements Entidad {

	private static final long serialVersionUID = -7349632288063455593L;
	private Long idtocotizacionmovil;
	private String grupovehiculos;
	private Short modelovehiculo;
	private String tipovehiculo;
	private String marcavehiculo;
	private String estado;
	private String descripciontarifa;
	private Double oc;
	private BigDecimal tarifaamplia;
	private BigDecimal tarifalimitada;
	private BigDecimal tarifaBasica;
	private BigDecimal tarifaPlata;
	private BigDecimal tarifaOro;
	private BigDecimal tarifaPlatino;
	private String clavetarifa;
	private BigDecimal idtocotizacionmidas;
	private BigDecimal idtocotizacionseycos;
	private Date fecharegistro;
    private Long userId;
	private DescuentosAgenteDTO descuentoAgente ;//= new DescuentosAgenteDTO();
    private String claveagente; 
    private String clavepromo; 
    private BigDecimal valorPrimaTotal;
    private BigDecimal valorPrimaAmplia;
    private BigDecimal valorPrimaLimitada;
	private BigDecimal valorPrimaBasica;
	private BigDecimal valorPrimaPlata;
	private BigDecimal valorPrimaOro;
	private BigDecimal valorPrimaPlatino;
    private Date fechanacimiento;
    private BigDecimal sumaasegurada;
    private String sexo;
    private BigDecimal peso;
    private BigDecimal estatura;
    private Long ocupacionId;
    private String claveNegocio;
    private Usuario usuario;
    private String porcentajeDescuento;
    private String tipoDescuento;
    private String nombreProspecto;
    private String emailProspecto;
    private String telefonoProspecto;
    private Short estatusProspecto;
    private String osMovil;
    private String uuId;
    private String telefono;
    private String nombre;
    private String email;
    private List<DatosDesglosePagosMovil> datosDesglosePagosMovil;
    private String tipoCotizacion;
    private Boolean procesada;
    private ResumenCotMovilVidaDTO resumenCotMovilVidaDTO;
    
	// Constructors

	/** default constructor */
	public CotizacionMovilDTO() {
	}
	@Id
	@Column(name = "IDTOCOTIZACIONMOVIL")
	@SequenceGenerator(name="TOCOTIZACIONMOVIL_ID_GENERATOR", sequenceName="MIDAS.TOCOTIZACIONMOVIL_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOCOTIZACIONMOVIL_ID_GENERATOR")
	public Long getIdtocotizacionmovil() {
		return this.idtocotizacionmovil;
	}

	public void setIdtocotizacionmovil(Long idtocotizacionmovil) {
		this.idtocotizacionmovil = idtocotizacionmovil;
	}

	@Column(name = "GRUPOVEHICULOS", length = 100)
	public String getGrupovehiculos() {
		return this.grupovehiculos;
	}

	public void setGrupovehiculos(String grupovehiculos) {
		this.grupovehiculos = grupovehiculos;
	}

	@Column(name = "MODELOVEHICULO", precision = 0)
	public Short getModelovehiculo() {
		return this.modelovehiculo;
	}

	public void setModelovehiculo(Short modelovehiculo) {
		this.modelovehiculo = modelovehiculo;
	}

	@Column(name = "TIPOVEHICULO", length = 100)
	public String getTipovehiculo() {
		return this.tipovehiculo;
	}

	public void setTipovehiculo(String tipovehiculo) {
		this.tipovehiculo = tipovehiculo;
	}

	@Column(name = "MARCAVEHICULO", length = 100)
	public String getMarcavehiculo() {
		return this.marcavehiculo;
	}

	public void setMarcavehiculo(String marcavehiculo) {
		this.marcavehiculo = marcavehiculo;
	}

	@Column(name = "ESTADO", length = 100)
	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Column(name = "DESCRIPCIONTARIFA", length = 200)
	public String getDescripciontarifa() {
		return this.descripciontarifa;
	}

	public void setDescripciontarifa(String descripciontarifa) {
		this.descripciontarifa = descripciontarifa;
	}

	@Column(name = "OC", precision = 0)
	public Double getOc() {
		return this.oc;
	}

	public void setOc(Double oc) {
		this.oc = oc;
	}

	@Column(name = "TARIFAAMPLIA", precision = 0)
	public BigDecimal getTarifaamplia() {
		return this.tarifaamplia;
	}

	public void setTarifaamplia(BigDecimal tarifaamplia) {
		this.tarifaamplia = tarifaamplia;
	}

	@Column(name = "TARIFALIMITADA", precision = 0)
	public BigDecimal getTarifalimitada() {
		return this.tarifalimitada;
	}

	public void setTarifalimitada(BigDecimal tarifalimitada) {
		this.tarifalimitada = tarifalimitada;
	}
	
	@Column(name = "TARIFABASICA", precision = 0)
	public BigDecimal getTarifaBasica() {
		return tarifaBasica;
	}


	public void setTarifaBasica(BigDecimal tarifaBasica) {
		this.tarifaBasica = tarifaBasica;
	}

	@Column(name = "TARIFAPLATA", precision = 0)
	public BigDecimal getTarifaPlata() {
		return tarifaPlata;
	}


	public void setTarifaPlata(BigDecimal tarifaPlata) {
		this.tarifaPlata = tarifaPlata;
	}

	@Column(name = "TARIFAORO", precision = 0)
	public BigDecimal getTarifaOro() {
		return tarifaOro;
	}


	public void setTarifaOro(BigDecimal tarifaOro) {
		this.tarifaOro = tarifaOro;
	}

	@Column(name = "TARIFAPLATINO", precision = 0)
	public BigDecimal getTarifaPlatino() {
		return tarifaPlatino;
	}


	public void setTarifaPlatino(BigDecimal tarifaPlatino) {
		this.tarifaPlatino = tarifaPlatino;
	}

	@Column(name = "CLAVETARIFA", nullable = false, length = 200)
	public String getClavetarifa() {
		return this.clavetarifa;
	}

	public void setClavetarifa(String clavetarifa) {
		this.clavetarifa = clavetarifa;
	}

	@Column(name = "IDTOCOTIZACIONMIDAS", precision = 22, scale = 0)
	public BigDecimal getIdtocotizacionmidas() {
		return this.idtocotizacionmidas;
	}

	public void setIdtocotizacionmidas(BigDecimal idtocotizacionmidas) {
		this.idtocotizacionmidas = idtocotizacionmidas;
	}
	@Column(name = "IDTOCOTIZACIONSEYCOS", precision = 22, scale = 0)
	public BigDecimal getIdtocotizacionseycos() {
		return idtocotizacionseycos;
	}
	public void setIdtocotizacionseycos(BigDecimal idtocotizacionseycos) {
		this.idtocotizacionseycos = idtocotizacionseycos;
	}
	@Column(name = "FECHAREGISTRO")
	@Temporal(TemporalType.DATE)
	public Date getFecharegistro() {
		return this.fecharegistro;
	}

	public void setFecharegistro(Date fecharegistro) {
		this.fecharegistro = fecharegistro;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return idtocotizacionmovil;
	}
	@Override
	public String getValue() {
		return null;
	}
	@Override
	public <K> K getBusinessKey() {
		return null;
	}
	@Column(name = "CLAVEAGENTE")
	public String getClaveagente() {
		return this.claveagente;
	}

	public void setClaveagente(String claveagente) {
		this.claveagente = claveagente;
	}
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="IDTCDESCUENTOSAGENTE", nullable = false)
	public DescuentosAgenteDTO getDescuentoAgente() {
		return descuentoAgente;
	}
	public void setDescuentoAgente(DescuentosAgenteDTO descuentoAgente) {
		this.descuentoAgente = descuentoAgente;
	}
	@Column(name = "VALORPRIMATOTAL", nullable = true, precision = 16, scale = 2)
	public BigDecimal getValorPrimaTotal() {
		return valorPrimaTotal;
	}

	public void setValorPrimaTotal(BigDecimal valorPrimaTotal) {
		this.valorPrimaTotal = valorPrimaTotal;
	}
	@Column(name = "VALORPRIMAAPMPLIA", nullable = true, precision = 16, scale = 2)
	public BigDecimal getValorPrimaAmplia() {
		return valorPrimaAmplia;
	}
	public void setValorPrimaAmplia(BigDecimal valorPrimaAmplia) {
		this.valorPrimaAmplia = valorPrimaAmplia;
	}
	@Column(name = "VALORPRIMALIMITADA", nullable = true, precision = 16, scale = 2)
	public BigDecimal getValorPrimaLimitada() {
		return valorPrimaLimitada;
	}
	public void setValorPrimaLimitada(BigDecimal valorPrimaLimitada) {
		this.valorPrimaLimitada = valorPrimaLimitada;
	}
	@Column(name = "VALORPRIMABASICA", nullable = true, precision = 16, scale = 2)
	public BigDecimal getValorPrimaBasica() {
		return valorPrimaBasica;
	}
	public void setValorPrimaBasica(BigDecimal valorPrimaBasica) {
		this.valorPrimaBasica = valorPrimaBasica;
	}
	@Column(name = "VALORPRIMAPLATA", nullable = true, precision = 16, scale = 2)
	public BigDecimal getValorPrimaPlata() {
		return valorPrimaPlata;
	}
	public void setValorPrimaPlata(BigDecimal valorPrimaPlata) {
		this.valorPrimaPlata = valorPrimaPlata;
	}
	@Column(name = "VALORPRIMAORO", nullable = true, precision = 16, scale = 2)
	public BigDecimal getValorPrimaOro() {
		return valorPrimaOro;
	}
	public void setValorPrimaOro(BigDecimal valorPrimaOro) {
		this.valorPrimaOro = valorPrimaOro;
	}
	@Column(name = "VALORPRIMAPLATINO", nullable = true, precision = 16, scale = 2)
	public BigDecimal getValorPrimaPlatino() {
		return valorPrimaPlatino;
	}
	public void setValorPrimaPlatino(BigDecimal valorPrimaPlatino) {
		this.valorPrimaPlatino = valorPrimaPlatino;
	}
	@Column(name="IDUSUARIO")
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	@Column(name = "FECHANACIMIENTO")
	@Temporal(TemporalType.DATE)
	public Date getFechanacimiento() {
		return fechanacimiento;
	}
	public void setFechanacimiento(Date fechanacimiento) {
		this.fechanacimiento = fechanacimiento;
	}
	@Column(name = "SA")
	public BigDecimal getSumaasegurada() {
		return sumaasegurada;
	}
	public void setSumaasegurada(BigDecimal sumaasegurada) {
		this.sumaasegurada = sumaasegurada;
	}
	@Column(name = "SEXO")
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	@Column(name="PESO")
	public BigDecimal getPeso() {
		return peso;
	}
	public void setPeso(BigDecimal peso) {
		this.peso = peso;
	}
	@Column(name="ESTATURA")
	public BigDecimal getEstatura() {
		return estatura;
	}
	public void setEstatura(BigDecimal estatura) {
		this.estatura = estatura;
	}
	@Column(name="IDGIROOCUPACION")
	public Long getOcupacionId() {
		return ocupacionId;
	}
	public void setOcupacionId(Long ocupacionId) {
		this.ocupacionId = ocupacionId;
	}

	@Column(name = "CLAVENEGOCIO", nullable = false, length = 1)
	public String getClaveNegocio() {
		return claveNegocio;
	}
	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}
	@Transient
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	@Column(name = "CLAVEPROMO")
	public String getClavepromo() {
		return clavepromo;
	}
	public void setClavepromo(String clavepromo) {
		this.clavepromo = clavepromo;
	}
	@Column(name = "PORCENTAJEDESCUENTO")
	public String getPorcentajeDescuento() {
		return porcentajeDescuento;
	}
	public void setPorcentajeDescuento(String porcentajeDescuento) {
		this.porcentajeDescuento = porcentajeDescuento;
	}
	@Column(name = "TIPODESCUENTO")
	public String getTipoDescuento() {
		return tipoDescuento;
	}
	public void setTipoDescuento(String tipoDescuento) {
		this.tipoDescuento = tipoDescuento;
	}
	@Column(name = "NOMBREPROSPECTO")
	public String getNombreProspecto() {
		return nombreProspecto;
	}
	public void setNombreProspecto(String nombreProspecto) {
		this.nombreProspecto = nombreProspecto;
	}
	@Column(name = "EMAILPROSPECTO")
	public String getEmailProspecto() {
		return emailProspecto;
	}
	public void setEmailProspecto(String emailProspecto) {
		this.emailProspecto = emailProspecto;
	}
	@Column(name = "TELEFONOPROSPECTO")
	public String getTelefonoProspecto() {
		return telefonoProspecto;
	}
	public void setTelefonoProspecto(String telefonoProspecto) {
		this.telefonoProspecto = telefonoProspecto;
	}
	@Column(name = "ESTATUSPROSPECTO", nullable = false, precision = 4, scale = 0)
	public Short getEstatusProspecto() {
		return estatusProspecto;
	}
	public void setEstatusProspecto(Short estatusProspecto) {
		this.estatusProspecto = estatusProspecto;
	}
	@Column(name = "OSMOVIL")
	public String getOsMovil() {
		return osMovil;
	}
	public void setOsMovil(String osMovil) {
		this.osMovil = osMovil;
	}
	@Column(name = "UUID")
	public String getUuId() {
		return uuId;
	}
	public void setUuId(String uuId) {
		this.uuId = uuId;
	}
	@Transient
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	@Transient
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	@Transient
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Transient
	public List<DatosDesglosePagosMovil> getDatosDesglosePagosMovil() {
		return datosDesglosePagosMovil;
	}
	public void setDatosDesglosePagosMovil(
			List<DatosDesglosePagosMovil> datosDesglosePagosMovil) {
		this.datosDesglosePagosMovil = datosDesglosePagosMovil;
	}
	@Column(name = "TIPOCOTIZACION")
	public String getTipoCotizacion() {
		return tipoCotizacion;
	}
	public void setTipoCotizacion(String tipoCotizacion) {
		this.tipoCotizacion = tipoCotizacion;
	}
	@Column(name = "PROCESADA")
	public Boolean getProcesada() {
		return procesada;
	}
	public void setProcesada(Boolean procesada) {
		this.procesada = procesada;
	}
	@Transient
	public ResumenCotMovilVidaDTO getResumenCotMovilVidaDTO() {
		return resumenCotMovilVidaDTO;
	}
	public void setResumenCotMovilVidaDTO(
			ResumenCotMovilVidaDTO resumenCotMovilVidaDTO) {
		this.resumenCotMovilVidaDTO = resumenCotMovilVidaDTO;
	}
}