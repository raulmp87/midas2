package mx.com.afirme.midas2.domain.movil.ajustador;

import java.io.Serializable;

public class CondicionEspecialMovil implements Serializable {
	private Long id;
	private String nombre;
	private Long codigo;
	private String descripcion;	
	private Short estatus;
	private Short nivelAplicacion;
	private Short nivelImportancia;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Short getNivelAplicacion() {
		return nivelAplicacion;
	}
	public void setNivelAplicacion(Short nivelAplicacion) {
		this.nivelAplicacion = nivelAplicacion;
	}
	public Short getNivelImportancia() {
		return nivelImportancia;
	}
	public void setNivelImportancia(Short nivelImportancia) {
		this.nivelImportancia = nivelImportancia;
	}
	public Short getEstatus() {
		return estatus;
	}
	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}
	
}
