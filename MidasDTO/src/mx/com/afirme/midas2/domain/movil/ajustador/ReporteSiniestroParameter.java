package mx.com.afirme.midas2.domain.movil.ajustador;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ReporteSiniestroParameter  implements Serializable{
	private Long id;
	private String tipoInciso;
	private String tipoSiniestro;
	private String terminoAjuste;
	private String responsabilidad;
	private String declaracion;

	private boolean conductorMismoAsegurado;
	private String conductor;
	private Integer edad;
	private String sexo;
	private String curp;
	private String rfc;
	private String ladaTelefonoAseg;
	private String telefonoAseg;
	private String estadoNacimiento;
	private String ladaCelular;
	private String celular;
	
	private boolean participoUnidadEqPesado;
	private String lugarAjuste;
	private Integer ciaRespId;
	private Integer porcentajeParticipacion;
	private BigDecimal montoRecuperado;
	private String codigoTipoSiniestro;
	private String codigoTerminoAjuste;
	private String codigoResponsabilidad;
	private Long ajustadorId;
	private String tipoLicencia;
	private String terminoSiniestro;
	private boolean seFugoTerceroResp;
	private boolean recibidaOrdenCia;
	private Long numeroValuacion;
	
	private String placa;
	private String color;
	private String polizaCia;
	private String siniestroCia;
	
	/*Variables para sipac, se generaron tambien getters y setters*/
	private String origen;
	private String incisoCia;
	private Date fechaExpedicion;
	private Date fechaDictamen;
	private Date fechaJuicio;
	
	/*Variables para termino de ajuste rechazo*/
	private BigDecimal montoDanos;
	private String motivoRechazo;
	private String rechazoDesistimiento;
	private String observacionRechazo;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}		

	public String getTipoInciso() {
		return tipoInciso;
	}

	public void setTipoInciso(String tipoInciso) {
		this.tipoInciso = tipoInciso;
	}

	public String getTipoSiniestro() {
		return tipoSiniestro;
	}

	public void setTipoSiniestro(String tipoSiniestro) {
		this.tipoSiniestro = tipoSiniestro;
	}

	public String getTerminoAjuste() {
		return terminoAjuste;
	}

	public void setTerminoAjuste(String terminoAjuste) {
		this.terminoAjuste = terminoAjuste;
	}

	public String getResponsabilidad() {
		return responsabilidad;
	}

	public void setResponsabilidad(String responsabilidad) {
		this.responsabilidad = responsabilidad;
	}

	public String getDeclaracion() {
		return declaracion;
	}

	public void setDeclaracion(String declaracion) {
		this.declaracion = declaracion;
	}

	public boolean isConductorMismoAsegurado() {
		return conductorMismoAsegurado;
	}

	public void setConductorMismoAsegurado(boolean conductorMismoAsegurado) {
		this.conductorMismoAsegurado = conductorMismoAsegurado;
	}

	public String getConductor() {
		return conductor;
	}

	public void setConductor(String conductor) {
		this.conductor = conductor;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getTelefonoAseg() {
		return telefonoAseg;
	}

	public void setTelefonoAseg(String telefonoAseg) {
		this.telefonoAseg = telefonoAseg;
	}

	public String getEstadoNacimiento() {
		return estadoNacimiento;
	}

	public void setEstadoNacimiento(String estadoNacimiento) {
		this.estadoNacimiento = estadoNacimiento;
	}

	public boolean isParticipoUnidadEqPesado() {
		return participoUnidadEqPesado;
	}

	public void setParticipoUnidadEqPesado(boolean participoUnidadEqPesado) {
		this.participoUnidadEqPesado = participoUnidadEqPesado;
	}

	public String getLugarAjuste() {
		return lugarAjuste;
	}

	public void setLugarAjuste(String lugarAjuste) {
		this.lugarAjuste = lugarAjuste;
	}

	public Integer getCiaRespId() {
		return ciaRespId;
	}

	public void setCiaRespId(Integer ciaRespId) {
		this.ciaRespId = ciaRespId;
	}

	public void setPorcentajeParticipacion(Integer porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}

	public Integer getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}

	public void setLadaTelefonoAseg(String ladaTelefonoAseg) {
		this.ladaTelefonoAseg = ladaTelefonoAseg;
	}

	public String getLadaTelefonoAseg() {
		return ladaTelefonoAseg;
	}

	public void setLadaCelular(String ladaCelular) {
		this.ladaCelular = ladaCelular;
	}

	public String getLadaCelular() {
		return ladaCelular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getCelular() {
		return celular;
	}

	public void setMontoRecuperado(BigDecimal montoRecuperado) {
		this.montoRecuperado = montoRecuperado;
	}

	public BigDecimal getMontoRecuperado() {
		return montoRecuperado;
	}

	public void setCodigoTipoSiniestro(String codigoTipoSiniestro) {
		this.codigoTipoSiniestro = codigoTipoSiniestro;
	}

	public String getCodigoTipoSiniestro() {
		return codigoTipoSiniestro;
	}

	public void setCodigoTerminoAjuste(String codigoTerminoAjuste) {
		this.codigoTerminoAjuste = codigoTerminoAjuste;
	}

	public String getCodigoTerminoAjuste() {
		return codigoTerminoAjuste;
	}

	public void setCodigoResponsabilidad(String codigoResponsabilidad) {
		this.codigoResponsabilidad = codigoResponsabilidad;
	}

	public String getCodigoResponsabilidad() {
		return codigoResponsabilidad;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Long getAjustadorId() {
		return ajustadorId;
	}

	public void setAjustadorId(Long ajustadorId) {
		this.ajustadorId = ajustadorId;
	}

	public String getTipoLicencia() {
		return tipoLicencia;
	}

	public void setTipoLicencia(String tipoLicencia) {
		this.tipoLicencia = tipoLicencia;
	}

	public String getTerminoSiniestro() {
		return terminoSiniestro;
	}

	public void setTerminoSiniestro(String terminoSiniestro) {
		this.terminoSiniestro = terminoSiniestro;
	}

	public boolean isSeFugoTerceroResp() {
		return seFugoTerceroResp;
	}

	public void setSeFugoTerceroResp(boolean seFugoTerceroResp) {
		this.seFugoTerceroResp = seFugoTerceroResp;
	}

	public boolean isRecibidaOrdenCia() {
		return recibidaOrdenCia;
	}

	public void setRecibidaOrdenCia(boolean recibidaOrdenCia) {
		this.recibidaOrdenCia = recibidaOrdenCia;
	}

	public Long getNumeroValuacion() {
		return numeroValuacion;
	}

	public void setNumeroValuacion(Long numeroValuacion) {
		this.numeroValuacion = numeroValuacion;
	}

	public String getPolizaCia() {
		return polizaCia;
	}

	public void setPolizaCia(String polizaCia) {
		this.polizaCia = polizaCia;
	}

	public String getSiniestroCia() {
		return siniestroCia;
	}

	public void setSiniestroCia(String siniestroCia) {
		this.siniestroCia = siniestroCia;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getIncisoCia() {
		return incisoCia;
	}

	public void setIncisoCia(String incisoCia) {
		this.incisoCia = incisoCia;
	}

	public Date getFechaExpedicion() {
		return fechaExpedicion;
	}

	public void setFechaExpedicion(Date fechaExpedicion) {
		this.fechaExpedicion = fechaExpedicion;
	}

	public Date getFechaDictamen() {
		return fechaDictamen;
	}

	public void setFechaDictamen(Date fechaDictamen) {
		this.fechaDictamen = fechaDictamen;
	}

	public Date getFechaJuicio() {
		return fechaJuicio;
	}

	public void setFechaJuicio(Date fechaJuicio) {
		this.fechaJuicio = fechaJuicio;
	}

	public String getMotivoRechazo() {
		return motivoRechazo;
	}

	public void setMotivoRechazo(String motivoRechazo) {
		this.motivoRechazo = motivoRechazo;
	}

	public String getRechazoDesistimiento() {
		return rechazoDesistimiento;
	}

	public void setRechazoDesistimiento(String rechazoDesistimiento) {
		this.rechazoDesistimiento = rechazoDesistimiento;
	}

	public String getObservacionRechazo() {
		return observacionRechazo;
	}

	public void setObservacionRechazo(String observacionRechazo) {
		this.observacionRechazo = observacionRechazo;
	}

	public BigDecimal getMontoDanos() {
		return montoDanos;
	}

	public void setMontoDanos(BigDecimal montoDanos) {
		this.montoDanos = montoDanos;
	}
}