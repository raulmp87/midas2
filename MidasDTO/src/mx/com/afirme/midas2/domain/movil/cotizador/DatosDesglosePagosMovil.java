package mx.com.afirme.midas2.domain.movil.cotizador;

import java.io.Serializable;


public class DatosDesglosePagosMovil implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9040621613986588088L;
	private String formaPago;	
	private Double montoRecargoPagoFraccionado;
	private Double montoIVA;
	private Double primaNetaTotal;
	private Double primerPago;
	private Double pagosSubsecuentes;
	private Double descuentoComisionCedida=0.0d;
	private Double derechos;
	private Double primaTotal;
	private String descripcionPaquete;
	
	public Double getDerechos() {
		return derechos;
	}
	public void setDerechos(Double derechos) {
		this.derechos = derechos;
	}
	
	public String getFormaPago() {
		return formaPago;
	}
	
	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	
	public Double getMontoRecargoPagoFraccionado() {
		return montoRecargoPagoFraccionado;
	}
	
	public void setMontoRecargoPagoFraccionado(Double montoRecargoPagoFraccionado) {
		this.montoRecargoPagoFraccionado = montoRecargoPagoFraccionado;
	}
	
	public Double getMontoIVA() {
		return montoIVA;
	}
	
	public void setMontoIVA(Double montoIVA) {
		this.montoIVA = montoIVA;
	}
	
	public Double getPrimaNetaTotal() {
		return primaNetaTotal;
	}
	
	public void setPrimaNetaTotal(Double primaNetaTotal) {
		this.primaNetaTotal = primaNetaTotal;
	}
	
	public Double getPrimerPago() {
		return primerPago;
	}
	
	public void setPrimerPago(Double primerPago) {
		this.primerPago = primerPago;
	}
	
	public Double getPagosSubsecuentes() {
		return pagosSubsecuentes;
	}
	
	public void setPagosSubsecuentes(Double pagosSubsecuentes) {
		this.pagosSubsecuentes = pagosSubsecuentes;
	}

	public Double getDescuentoComisionCedida() {
		return descuentoComisionCedida;
	}

	public void setDescuentoComisionCedida(Double descuentoComisionCedida) {
		this.descuentoComisionCedida = descuentoComisionCedida;
	}
	public Double getPrimaTotal() {
		return primaTotal;
	}
	public void setPrimaTotal(Double primaTotal) {
		this.primaTotal = primaTotal;
	}
	public String getDescripcionPaquete() {
		return descripcionPaquete;
	}
	public void setDescripcionPaquete(String descripcionPaquete) {
		this.descripcionPaquete = descripcionPaquete;
	}
}
