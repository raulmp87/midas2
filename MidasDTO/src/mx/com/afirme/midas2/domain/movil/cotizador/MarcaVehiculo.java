package mx.com.afirme.midas2.domain.movil.cotizador;
public class MarcaVehiculo implements java.io.Serializable {

	// Fields

	private long id;
	private String descripcionMarcaVehiculo;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDescripcionMarcaVehiculo() {
		return descripcionMarcaVehiculo;
	}
	public void setDescripcionMarcaVehiculo(String descripcionMarcaVehiculo) {
		this.descripcionMarcaVehiculo = descripcionMarcaVehiculo;
	}

	
}