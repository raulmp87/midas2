package mx.com.afirme.midas2.domain.movil.cotizador;

import java.math.BigDecimal;

public class TarifasMovilCarga  {
	
	/**
	 * 
	 */
	private String grupovehiculos;
	private BigDecimal modelovehiculo;
	private String tipovehiculo;
	private String marcavehiculo;
	private String estado;
	private String descripciontarifa;
	private BigDecimal oc;
	private BigDecimal tarifaamplia;
	private BigDecimal tarifalimitada;	
	private String clavetarifa;
	private BigDecimal bajalogica;
	
	public String getGrupovehiculos() {
		return grupovehiculos;
	}
	public void setGrupovehiculos(String grupovehiculos) {
		this.grupovehiculos = grupovehiculos;
	}
	public BigDecimal getModelovehiculo() {
		return modelovehiculo;
	}
	public void setModelovehiculo(BigDecimal modelovehiculo) {
		this.modelovehiculo = modelovehiculo;
	}
	public String getTipovehiculo() {
		return tipovehiculo;
	}
	public void setTipovehiculo(String tipovehiculo) {
		this.tipovehiculo = tipovehiculo;
	}
	public String getMarcavehiculo() {
		return marcavehiculo;
	}
	public void setMarcavehiculo(String marcavehiculo) {
		this.marcavehiculo = marcavehiculo;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getDescripciontarifa() {
		return descripciontarifa;
	}
	public void setDescripciontarifa(String descripciontarifa) {
		this.descripciontarifa = descripciontarifa;
	}
	public BigDecimal getOc() {
		return oc;
	}
	public void setOc(BigDecimal oc) {
		this.oc = oc;
	}
	public BigDecimal getTarifaamplia() {
		return tarifaamplia;
	}
	public void setTarifaamplia(BigDecimal tarifaamplia) {
		this.tarifaamplia = tarifaamplia;
	}
	public BigDecimal getTarifalimitada() {
		return tarifalimitada;
	}
	public void setTarifalimitada(BigDecimal tarifalimitada) {
		this.tarifalimitada = tarifalimitada;
	}
	public String getClavetarifa() {
		return clavetarifa;
	}
	public void setClavetarifa(String clavetarifa) {
		this.clavetarifa = clavetarifa;
	}
	
	public void setBajalogica(BigDecimal bajalogica) {
		this.bajalogica = bajalogica;
	}
	public BigDecimal getBajalogica() {
		return bajalogica;
	}
		
}