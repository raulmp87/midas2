package mx.com.afirme.midas2.domain.movil.cotizador;
public class Estado implements java.io.Serializable {
	
	private Long id;
	private String nombreEstado;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombreEstado() {
		return nombreEstado;
	}
	public void setNombreEstado(String nombreEstado) {
		this.nombreEstado = nombreEstado;
	}
	
	
}