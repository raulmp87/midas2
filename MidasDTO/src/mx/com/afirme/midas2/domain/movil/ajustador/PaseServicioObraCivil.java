package mx.com.afirme.midas2.domain.movil.ajustador;

import java.util.Date;

/**
 * @author jreyes
 *
 */
public class PaseServicioObraCivil {
	
	private String numFolio;
	private Date fSiniestro;
	private Date fPaseDeAtencion;
	private Long numeroReporte;
	private Long numeroSiniestro;
	private String poliza;
	private Integer inciso;
	private String nombreIngenieroAsignadoHacerReparacion;
	private String telefonoIngeniero;
	private String descripcionBienDaniado;
	private Domicilio domicilioBienDaniado;
	
	private String telefono;
	private String nombreInteresado;
	private String nombreAjustador;
	private String numeroReporteCabina;
	private String numeroSiniestroCabina;
	
	public String getNumFolio() {
		return numFolio;
	}
	public void setNumFolio(String numFolio) {
		this.numFolio = numFolio;
	}
	public Date getfSiniestro() {
		return fSiniestro;
	}
	public void setfSiniestro(Date fSiniestro) {
		this.fSiniestro = fSiniestro;
	}
	public Date getfPaseDeAtencion() {
		return fPaseDeAtencion;
	}
	public void setfPaseDeAtencion(Date fPaseDeAtencion) {
		this.fPaseDeAtencion = fPaseDeAtencion;
	}
	public Long getNumeroReporte() {
		return numeroReporte;
	}
	public void setNumeroReporte(Long numeroReporte) {
		this.numeroReporte = numeroReporte;
	}
	public Long getNumeroSiniestro() {
		return numeroSiniestro;
	}
	public void setNumeroSiniestro(Long numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}
	public String getPoliza() {
		return poliza;
	}
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	public Integer getInciso() {
		return inciso;
	}
	public void setInciso(Integer inciso) {
		this.inciso = inciso;
	}
	public String getNombreIngenieroAsignadoHacerReparacion() {
		return nombreIngenieroAsignadoHacerReparacion;
	}
	public void setNombreIngenieroAsignadoHacerReparacion(
			String nombreIngenieroAsignadoHacerReparacion) {
		this.nombreIngenieroAsignadoHacerReparacion = nombreIngenieroAsignadoHacerReparacion;
	}
	public String getTelefonoIngeniero() {
		return telefonoIngeniero;
	}
	public void setTelefonoIngeniero(String telefonoIngeniero) {
		this.telefonoIngeniero = telefonoIngeniero;
	}
	public String getDescripcionBienDaniado() {
		return descripcionBienDaniado;
	}
	public void setDescripcionBienDaniado(String descripcionBienDaniado) {
		this.descripcionBienDaniado = descripcionBienDaniado;
	}
	public Domicilio getDomicilioBienDaniado() {
		if(domicilioBienDaniado==null){
			domicilioBienDaniado = new Domicilio();
		}
		return domicilioBienDaniado;
	}
	public void setDomicilioBienDaniado(Domicilio domicilioBienDaniado) {
		this.domicilioBienDaniado = domicilioBienDaniado;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getNombreInteresado() {
		return nombreInteresado;
	}
	public void setNombreInteresado(String nombreInteresado) {
		this.nombreInteresado = nombreInteresado;
	}
	public String getNombreAjustador() {
		return nombreAjustador;
	}
	public void setNombreAjustador(String nombreAjustador) {
		this.nombreAjustador = nombreAjustador;
	}
	public String getNumeroReporteCabina() {
		return numeroReporteCabina;
	}
	public void setNumeroReporteCabina(String numeroReporteCabina) {
		this.numeroReporteCabina = numeroReporteCabina;
	}
	public String getNumeroSiniestroCabina() {
		return numeroSiniestroCabina;
	}
	public void setNumeroSiniestroCabina(String numeroSiniestroCabina) {
		this.numeroSiniestroCabina = numeroSiniestroCabina;
	}
}