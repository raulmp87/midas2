package mx.com.afirme.midas2.domain.movil.cotizador;

import java.math.BigDecimal;

public class SolicitudCotizacionMovil implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1532081815905914257L;
	// Fields
    private Long idDescripcionEstilo;
    private String email;
    private String claveagente;
    private String tipoCotizacion;
    private String os;
    private String uuid;
    private Long idPaquete;
    private BigDecimal idFormaPago;
    private Long idToCotizacionMovil;
    private BigDecimal idToCotizacionMidas;
    private String telefono;
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String claveNegocio;
    private Boolean contratar = true;

	
	public static final String TIPO_COTIZACION_PORTAL = "TCP";
	public static final String TIPO_COTIZACION_MOVIL = "TCM";
	
	public Long getIdDescripcionEstilo() {
		return idDescripcionEstilo;
	}
	public void setIdDescripcionEstilo(Long idDescripcionEstilo) {
		this.idDescripcionEstilo = idDescripcionEstilo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getClaveagente() {
		return this.claveagente;
	}

	public void setClaveagente(String claveagente) {
		this.claveagente = claveagente;
	}
	public String getTipoCotizacion() {
		return tipoCotizacion;
	}
	public void setTipoCotizacion(String tipoCotizacion) {
		this.tipoCotizacion = tipoCotizacion;
	}
	public String getOs() {
		return os;
	}
	public void setOs(String os) {
		this.os = os;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public Long getIdPaquete() {
		return idPaquete;
	}
	public void setIdPaquete(Long idPaquete) {
		this.idPaquete = idPaquete;
	}
	public BigDecimal getIdFormaPago() {
		return idFormaPago;
	}
	public void setIdFormaPago(BigDecimal idFormaPago) {
		this.idFormaPago = idFormaPago;
	}
	public Long getIdToCotizacionMovil() {
		return idToCotizacionMovil;
	}
	public void setIdToCotizacionMovil(Long idToCotizacionMovil) {
		this.idToCotizacionMovil = idToCotizacionMovil;
	}
	public BigDecimal getIdToCotizacionMidas() {
		return idToCotizacionMidas;
	}
	public void setIdToCotizacionMidas(BigDecimal idToCotizacionMidas) {
		this.idToCotizacionMidas = idToCotizacionMidas;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Boolean getContratar() {
		return contratar;
	}
	public void setContratar(Boolean contratar) {
		this.contratar = contratar;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getClaveNegocio() {
		return claveNegocio;
	}
	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}
}