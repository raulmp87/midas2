package mx.com.afirme.midas2.domain.movil.cotizador;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * Tcdescuentosagente entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCDESCUENTOSAGENTE", schema = "MIDAS")
public class DescuentosAgenteDTO implements java.io.Serializable {

	// Fields

	private Long idtcdescuentosagente;
	private String claveagente;
	private String clavepromo;
	private String nombre;
	private String porcentaje;
	private Date fecharegistro;
	private Date fechamodificacion;
	private Short bajalogica;
	private String numerotelefono;
	private String email;
	private String claveNegocio;
	private Boolean esNuevoAgente = false;
	// Constructors

	/** default constructor */
	public DescuentosAgenteDTO() {
	}
	@Id
	@Column(name = "IDTCDESCUENTOSAGENTE")
	@SequenceGenerator(name="TCDESCUENTOSAGENTE_ID_GENERATOR", sequenceName="MIDAS.TCDESCUENTOSAGENTE_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TCDESCUENTOSAGENTE_ID_GENERATOR")
	public Long getIdtcdescuentosagente() {
		return this.idtcdescuentosagente;
	}

	public void setIdtcdescuentosagente(Long idtcdescuentosagente) {
		this.idtcdescuentosagente = idtcdescuentosagente;
	}

	@Column(name = "CLAVEAGENTE", length = 100)
	public String getClaveagente() {
		return this.claveagente;
	}

	public void setClaveagente(String claveagente) {
		this.claveagente = claveagente;
	}

	@Column(name = "NOMBRE", length = 100)
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "PORCENTAJE", length = 100)
	public String getPorcentaje() {
		return this.porcentaje;
	}

	public void setPorcentaje(String porcentaje) {
		this.porcentaje = porcentaje;
	}

	@Column(name = "FECHAREGISTRO")
	@Temporal(TemporalType.DATE)
	public Date getFecharegistro() {
		return this.fecharegistro;
	}

	public void setFecharegistro(Date fecharegistro) {
		this.fecharegistro = fecharegistro;
	}
	@Column(name = "FECHAMODIFICACION")
	@Temporal(TemporalType.DATE)
	public Date getFechamodificacion() {
		return this.fechamodificacion;
	}

	public void setFechamodificacion(Date fechamodificacion) {
		this.fechamodificacion = fechamodificacion;
	}
	
	@Column(name = "BAJALOGICA", nullable = false, precision = 4, scale = 0)
	public Short getBajalogica() {
		return this.bajalogica;
	}

	public void setBajalogica(Short bajalogica) {
		this.bajalogica = bajalogica;
	}
	@Column(name = "NUMEROTELEFONO")
	public String getNumerotelefono() {
		return this.numerotelefono;
	}

	public void setNumerotelefono(String numerotelefono) {
		this.numerotelefono = numerotelefono;
	}

	@Column(name = "EMAIL", nullable = false, length = 100)
	public String getEmail() {
		return this.email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setClavepromo(String clavepromo) {
		this.clavepromo = clavepromo;
	}
	
	@Column(name = "CLAVEPROMO", length = 100)
	public String getClavepromo() {
		return clavepromo;
	}
	@Column(name = "CLAVENEGOCIO", nullable = false, length = 1)
	public String getClaveNegocio() {
		return claveNegocio;
	}
	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}
	@Transient
	public Boolean getEsNuevoAgente() {
		return esNuevoAgente;
	}
	public void setEsNuevoAgente(Boolean esNuevoAgente) {
		this.esNuevoAgente = esNuevoAgente;
	}
}