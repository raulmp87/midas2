package mx.com.afirme.midas2.domain.movil.ajustador;

import java.io.Serializable;

public class MarcarTerminacionParameter implements Serializable {

	private Long reporteSiniestroId;

	public Long getReporteSiniestroId() {
		return reporteSiniestroId;
	}

	public void setReporteSiniestroId(Long reporteSiniestroId) {
		this.reporteSiniestroId = reporteSiniestroId;
	}
}
