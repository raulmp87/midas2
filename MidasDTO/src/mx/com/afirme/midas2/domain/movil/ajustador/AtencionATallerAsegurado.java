package mx.com.afirme.midas2.domain.movil.ajustador;

import java.util.Date;

/**
 * @author jreyes
 *
 */
public class AtencionATallerAsegurado { //implements Serializable{

	private String numFolio;
	private Date fPaseAtencion;
	private Long numeroReporte;
	private Long numeroSiniestro;
	private String poliza;
	private Integer numeroInciso;
	private String nombreTallerAsignado;
	private String telefonoTallerAsignado;
	private Domicilio domicilioTaller;
	
	private String nombreAsegurado;
	private String nombreInteresado;
	private Domicilio domicilioInteresado;
	
	private String telefonos;
	private String email;	
	private String marcaVehiculoAsegurado;
	private String tipoVehiculoAsegurado;
	private Integer modeloVehiculoAsegurado;
	private String placasVehiculoAsegurado;
	private String numSerieVehiculoAsegurado;
	private String areasDaniadas;
	private String daniosPreexistentes;
	private String aplicaDeducible;
	private Double deducible;
	private Double pct_deducible;
	private String numeroReporteCabina;
	private String numeroSiniestroCabina;
	
	public String getNumFolio() {
		return numFolio;
	}
	public void setNumFolio(String numFolio) {
		this.numFolio = numFolio;
	}
	public Date getfPaseAtencion() {
		return fPaseAtencion;
	}
	public void setfPaseAtencion(Date fPaseAtencion) {
		this.fPaseAtencion = fPaseAtencion;
	}
	public Long getNumeroReporte() {
		return numeroReporte;
	}
	public void setNumeroReporte(Long numeroReporte) {
		this.numeroReporte = numeroReporte;
	}
	public Long getNumeroSiniestro() {
		return numeroSiniestro;
	}
	public void setNumeroSiniestro(Long numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}
	public String getPoliza() {
		return poliza;
	}
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	public Integer getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public String getNombreTallerAsignado() {
		return nombreTallerAsignado;
	}
	public void setNombreTallerAsignado(String nombreTallerAsignado) {
		this.nombreTallerAsignado = nombreTallerAsignado;
	}
	public String getTelefonoTallerAsignado() {
		return telefonoTallerAsignado;
	}
	public void setTelefonoTallerAsignado(String telefonoTallerAsignado) {
		this.telefonoTallerAsignado = telefonoTallerAsignado;
	}
	public Domicilio getDomicilioTaller() {
	if(domicilioTaller==null){
			domicilioTaller = new Domicilio();
		}
		return domicilioTaller;
	}
	public void setDomicilioTaller(Domicilio domicilioTaller) {
		this.domicilioTaller = domicilioTaller;
	}
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}
	public String getNombreInteresado() {
		return nombreInteresado;
	}
	public void setNombreInteresado(String nombreInteresado) {
		this.nombreInteresado = nombreInteresado;
	}
	public Domicilio getDomicilioInteresado() {
	if(domicilioInteresado==null){
			domicilioInteresado = new Domicilio();
		}
		return domicilioInteresado;
	}
	public void setDomicilioInteresado(Domicilio domicilioInteresado) {
		this.domicilioInteresado = domicilioInteresado;
	}
	public String getTelefonos() {
		return telefonos;
	}
	public void setTelefonos(String telefonos) {
		this.telefonos = telefonos;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMarcaVehiculoAsegurado() {
		return marcaVehiculoAsegurado;
	}
	public void setMarcaVehiculoAsegurado(String marcaVehiculoAsegurado) {
		this.marcaVehiculoAsegurado = marcaVehiculoAsegurado;
	}
	public String getTipoVehiculoAsegurado() {
		return tipoVehiculoAsegurado;
	}
	public void setTipoVehiculoAsegurado(String tipoVehiculoAsegurado) {
		this.tipoVehiculoAsegurado = tipoVehiculoAsegurado;
	}
	public Integer getModeloVehiculoAsegurado() {
		return modeloVehiculoAsegurado;
	}
	public void setModeloVehiculoAsegurado(Integer modeloVehiculoAsegurado) {
		this.modeloVehiculoAsegurado = modeloVehiculoAsegurado;
	}
	public String getPlacasVehiculoAsegurado() {
		return placasVehiculoAsegurado;
	}
	public void setPlacasVehiculoAsegurado(String placasVehiculoAsegurado) {
		this.placasVehiculoAsegurado = placasVehiculoAsegurado;
	}
	public String getNumSerieVehiculoAsegurado() {
		return numSerieVehiculoAsegurado;
	}
	public void setNumSerieVehiculoAsegurado(String numSerieVehiculoAsegurado) {
		this.numSerieVehiculoAsegurado = numSerieVehiculoAsegurado;
	}
	public String getAreasDaniadas() {
		return areasDaniadas;
	}
	public void setAreasDaniadas(String areasDaniadas) {
		this.areasDaniadas = areasDaniadas;
	}
	public String getDaniosPreexistentes() {
		return daniosPreexistentes;
	}
	public void setDaniosPreexistentes(String daniosPreexistentes) {
		this.daniosPreexistentes = daniosPreexistentes;
	}
	public String getAplicaDeducible() {
		return aplicaDeducible;
	}
	public void setAplicaDeducible(String aplicaDeducible) {
		this.aplicaDeducible = aplicaDeducible;
	}
	public Double getDeducible() {
		return deducible;
	}
	public void setDeducible(Double deducible) {
		this.deducible = deducible;
	}
	public Double getPct_deducible() {
		return pct_deducible;
	}
	public void setPct_deducible(Double pct_deducible) {
		this.pct_deducible = pct_deducible;
	}
	public String getNumeroReporteCabina() {
		return numeroReporteCabina;
	}
	public void setNumeroReporteCabina(String numeroReporteCabina) {
		this.numeroReporteCabina = numeroReporteCabina;
	}
	public String getNumeroSiniestroCabina() {
		return numeroSiniestroCabina;
	}
	public void setNumeroSiniestroCabina(String numeroSiniestroCabina) {
		this.numeroSiniestroCabina = numeroSiniestroCabina;
	}
	
	
}
