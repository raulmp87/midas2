package mx.com.afirme.midas2.domain.movil.ajustador;

import java.io.Serializable;

public class LlegadaCiaResponse implements Serializable{
	private Integer id;
	private Long idReporte;
	private Integer idProveedor;
	private String nombreProveedor;
	private Integer Orden;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getOrden() {
		return Orden;
	}
	public void setOrden(Integer orden) {
		Orden = orden;
	}
	public Long getIdReporte() {
		return idReporte;
	}
	public void setIdReporte(Long idReporte) {
		this.idReporte = idReporte;
	}
	public Integer getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(Integer idProveedor) {
		this.idProveedor = idProveedor;
	}
	public String getNombreProveedor() {
		return nombreProveedor;
	}
	public void setNombreProveedor(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}
	

}
