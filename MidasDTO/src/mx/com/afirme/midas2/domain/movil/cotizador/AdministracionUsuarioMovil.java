package mx.com.afirme.midas2.domain.movil.cotizador;
public class AdministracionUsuarioMovil implements java.io.Serializable {

	// Fields

	private Long idusuario;
	private String email;
	private String nombreCompleto;
	private Short bajalogica;
	private Long numeroCotizaciones;
	private String claveagente;
	private String deviceUuid;
	private String numeroTelefono;
	// Constructors
	
	public Long getIdusuario() {
		return idusuario;
	}
	public void setIdusuario(Long idusuario) {
		this.idusuario = idusuario;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	public Short getBajalogica() {
		return bajalogica;
	}
	public void setBajalogica(Short bajalogica) {
		this.bajalogica = bajalogica;
	}
	public Long getNumeroCotizaciones() {
		return numeroCotizaciones;
	}
	public void setNumeroCotizaciones(Long numeroCotizaciones) {
		this.numeroCotizaciones = numeroCotizaciones;
	}
	public String getClaveagente() {
		return claveagente;
	}
	public void setClaveagente(String claveagente) {
		this.claveagente = claveagente;
	}
	public String getDeviceUuid() {
		return deviceUuid;
	}
	public void setDeviceUuid(String deviceUuid) {
		this.deviceUuid = deviceUuid;
	}
	public String getNumeroTelefono() {
		return numeroTelefono;
	}
	public void setNumeroTelefono(String numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}
	
	

	

}