package mx.com.afirme.midas2.domain.movil.ajustador;

import java.io.Serializable;

import mx.com.afirme.midas2.domain.Coordenadas;

public class MarcarContactoParameter implements Serializable {

	private Long reporteSiniestroId;
	private Coordenadas coordenadas;
	private Long ajustadorId;
	
	public Long getReporteSiniestroId() {
		return reporteSiniestroId;
	}
	
	public void setReporteSiniestroId(Long reporteSiniestroId) {
		this.reporteSiniestroId = reporteSiniestroId;
	}
	
	public Coordenadas getCoordenadas() {
		return coordenadas;
	}
	
	public void setCoordenadas(Coordenadas coordenadas) {
		this.coordenadas = coordenadas;
	}

	public Long getAjustadorId() {
		return ajustadorId;
	}

	public void setAjustadorId(Long ajustadorId) {
		this.ajustadorId = ajustadorId;
	}
	
}
