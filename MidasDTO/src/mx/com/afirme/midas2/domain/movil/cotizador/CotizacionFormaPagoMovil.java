package mx.com.afirme.midas2.domain.movil.cotizador;
public class CotizacionFormaPagoMovil implements java.io.Serializable {

	// Fields

	private Long id;
	private String descripcionVehiculo;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescripcionVehiculo() {
		return descripcionVehiculo;
	}
	public void setDescripcionVehiculo(String descripcionVehiculo) {
		this.descripcionVehiculo = descripcionVehiculo;
	}
	
}