package mx.com.afirme.midas2.domain.movil.ajustador;

import java.io.Serializable;

public class Acciones implements Serializable {
	private String accion;

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}
}
