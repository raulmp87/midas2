package mx.com.afirme.midas2.domain.movil.ajustador;

import java.io.Serializable;
import java.util.Date;

public class FindPolizaParameter implements Serializable {

	private String numeroSerie;
	private String poliza;
	private Integer inciso;
	private String nombre;
	private String placa;
	private String contratante;
	private Date fechaOcurrido;
	private String numeroMotor;
	
	public String getNumeroSerie() {
		return numeroSerie;
	}
	
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	
	public String getPoliza() {
		return poliza;
	}
	
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	
	public Integer getInciso() {
		return inciso;
	}
	
	public void setInciso(Integer inciso) {
		this.inciso = inciso;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getContratante() {
		return contratante;
	}
	public void setContratante(String contratante) {
		this.contratante = contratante;
	}
	public Date getFechaOcurrido() {
		return fechaOcurrido;
	}
	public void setFechaOcurrido(Date fechaOcurrido) {
		this.fechaOcurrido = fechaOcurrido;
	}

	public void setNumeroMotor(String numeroMotor) {
		this.numeroMotor = numeroMotor;
	}

	public String getNumeroMotor() {
		return numeroMotor;
	}
}
