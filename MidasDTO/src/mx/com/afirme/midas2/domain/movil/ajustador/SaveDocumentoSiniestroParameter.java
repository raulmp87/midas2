package mx.com.afirme.midas2.domain.movil.ajustador;

import java.io.File;

import javax.validation.constraints.NotNull;

public class SaveDocumentoSiniestroParameter {

	private Long reporteSiniestroId;
	private Long tipoDocumentoSiniestroId;
	private File file;
	private String fileContentType;
	private String fileFileName;
	private String folio;
	private String folioExpediente ;
	private String cveProceso ;
	
	@NotNull
	public Long getReporteSiniestroId() {
		return reporteSiniestroId;
	}
	
	public void setReporteSiniestroId(Long reporteSiniestroId) {
		this.reporteSiniestroId = reporteSiniestroId;
	}
	
	@NotNull
	public Long getTipoDocumentoSiniestroId() {
		return tipoDocumentoSiniestroId;
	}
	
	public void setTipoDocumentoSiniestroId(Long tipoDocumentoSiniestroId) {
		this.tipoDocumentoSiniestroId = tipoDocumentoSiniestroId;
	}

	@NotNull
	public File getFile() {
		return file;
	}
	
	public void setFile(File file) {
		this.file = file;
	}
	
	public String getFileContentType() {
		return fileContentType;
	}
	
	public void setFileContentType(String fileContentType) {
		this.fileContentType = fileContentType;
	}
	
	public String getFileFileName() {
		return fileFileName;
	}
	
	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getFolioExpediente() {
		return folioExpediente;
	}

	public void setFolioExpediente(String folioExpediente) {
		this.folioExpediente = folioExpediente;
	}

	public String getCveProceso() {
		return cveProceso;
	}

	public void setCveProceso(String cveProceso) {
		this.cveProceso = cveProceso;
	}
}
