package mx.com.afirme.midas2.domain.movil.ajustador;

import java.io.Serializable;

public class TerceroParameter implements Serializable {
	private Long id;
	private String polizaId;
	private Long reporteSiniestroId;
	private Integer coberturaId;
	private String tipoEstimacion;
	private String tipoCalculo;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPolizaId() {
		return polizaId;
	}
	public void setPolizaId(String polizaId) {
		this.polizaId = polizaId;
	}
	public Long getReporteSiniestroId() {
		return reporteSiniestroId;
	}
	public void setReporteSiniestroId(Long reporteSiniestroId) {
		this.reporteSiniestroId = reporteSiniestroId;
	}
	public Integer getCoberturaId() {
		return coberturaId;
	}
	public void setCoberturaId(Integer coberturaId) {
		this.coberturaId = coberturaId;
	}
	public void setTipoEstimacion(String tipoEstimacion) {
		this.tipoEstimacion = tipoEstimacion;
	}
	public String getTipoEstimacion() {
		return tipoEstimacion;
	}
	public void setTipoCalculo(String tipoCalculo) {
		this.tipoCalculo = tipoCalculo;
	}
	public String getTipoCalculo() {
		return tipoCalculo;
	}	
}