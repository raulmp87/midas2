package mx.com.afirme.midas2.domain.movil.cliente;

import java.io.Serializable;

import mx.com.afirme.midas.poliza.NumeroPolizaCompleto;

public class InfoPolizaParameter implements Serializable{
	
	private static final long serialVersionUID = 4623596576424517766L;
	private String polizaNo;
	private String inciso;
	private String username;
	private String descripcion;
	private String tipo;
	private int polizaID;
	private NumeroPolizaCompleto numeroPoliza;
	private String ramo;
	private String polizasPermitidos;
	private String nombreUsuario;
	private String imei;
	private boolean consultaPoliza;
	
	public int getPolizaID() {
		return polizaID;
	}
	public void setPolizaID(int polizaID) {
		this.polizaID = polizaID;
	}
	public String getPolizaNo() {
		return polizaNo;
	}
	public void setPolizaNo(String polizaNo) {
		this.polizaNo = polizaNo;
	}
	public String getInciso() {
		return inciso;
	}
	public void setInciso(String inciso) {
		this.inciso = inciso;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public void setNumeroPoliza(NumeroPolizaCompleto numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public NumeroPolizaCompleto getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setRamo(String ramo) {
		this.ramo = ramo;
	}
	public String getRamo() {
		return ramo;
	}
	public String getPolizasPermitidos() {
		return polizasPermitidos;
	}
	public void setPolizasPermitidos(String polizasPermitidos) {
		this.polizasPermitidos = polizasPermitidos;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	/**
	 * @return the imei
	 */
	public String getImei() {
		return imei;
	}
	/**
	 * @param imei the imei to set
	 */
	public void setImei(String imei) {
		this.imei = imei;
	}
	/**
	 * @return the consultaPoliza
	 */
	public boolean isConsultaPoliza() {
		return consultaPoliza;
	}
	/**
	 * @param consultaPoliza the consultaPoliza to set
	 */
	public void setConsultaPoliza(boolean consultaPoliza) {
		this.consultaPoliza = consultaPoliza;
	}
}
