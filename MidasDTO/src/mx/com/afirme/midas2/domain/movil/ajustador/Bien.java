package mx.com.afirme.midas2.domain.movil.ajustador;

public class Bien extends Tercero{
	
	public Bien(){
		super.setTipo("BIEN");
	}
	
	public Bien(Long reporteSiniestroId, Integer coberturaId){
		super.setTipo("BIEN");
		super.setReporteSiniestroId(reporteSiniestroId);
		super.setCoberturaId(coberturaId);
	}
	
	private String tipoBien;
	private String ingeniero;
	private String areasDanadas;
	private boolean perdidaTotal;
	
	private String paisId;
	private String estadoId;
	private String ciudadId;
	private String calleNumero;
	private String codigoPostal;
	private String colonia;
	private String referencia;
	private String km;
	private Integer idIngeniero;
	public String getTipoBien() {
		return tipoBien;
	}
	public void setTipoBien(String tipoBien) {
		this.tipoBien = tipoBien;
	}	
	public String getIngeniero() {
		return ingeniero;
	}
	public void setIngeniero(String ingeniero) {
		this.ingeniero = ingeniero;
	}
	public String getAreasDanadas() {
		return areasDanadas;
	}
	public void setAreasDanadas(String areasDanadas) {
		this.areasDanadas = areasDanadas;
	}
	public boolean isPerdidaTotal() {
		return perdidaTotal;
	}
	public void setPerdidaTotal(boolean perdidaTotal) {
		this.perdidaTotal = perdidaTotal;
	}	
	public String getPaisId() {
		return paisId;
	}
	public void setPaisId(String paisId) {
		this.paisId = paisId;
	}
	public String getEstadoId() {
		return estadoId;
	}
	public void setEstadoId(String estadoId) {
		this.estadoId = estadoId;
	}
	public String getCiudadId() {
		return ciudadId;
	}
	public void setCiudadId(String ciudadId) {
		this.ciudadId = ciudadId;
	}
	public String getCalleNumero() {
		return calleNumero;
	}
	public void setCalleNumero(String calleNumero) {
		this.calleNumero = calleNumero;
	}
	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public String getColonia() {
		return colonia;
	}
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getKm() {
		return km;
	}
	public void setKm(String km) {
		this.km = km;
	}

	public void setIdIngeniero(Integer idIngeniero) {
		this.idIngeniero = idIngeniero;
	}

	public Integer getIdIngeniero() {
		return idIngeniero;
	}	
}
