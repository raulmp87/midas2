package mx.com.afirme.midas2.domain.movil.cotizador;
public class CatalogoEstatusActualizacion implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8359935211023492344L;
	private Long id;
	private String descripcion;
	private Short bajaLogica;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Short getBajaLogica() {
		return bajaLogica;
	}
	public void setBajaLogica(Short bajaLogica) {
		this.bajaLogica = bajaLogica;
	}
	
}