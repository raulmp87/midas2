package mx.com.afirme.midas2.domain.movil.cliente;

import java.util.Date;

public class DatosPolizaSeycos {
	private String nombreAsegurado;
	private Long idCotizacionSeycos;
	private String situacionPoliza;
	private Integer numRenovacion;
	private Date finVigencia;
	private Date iniVigencia;
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}
	public Long getIdCotizacionSeycos() {
		return idCotizacionSeycos;
	}
	public void setIdCotizacionSeycos(Long idCotizacionSeycos) {
		this.idCotizacionSeycos = idCotizacionSeycos;
	}
	public String getSituacionPoliza() {
		return situacionPoliza;
	}
	public void setSituacionPoliza(String situacionPoliza) {
		this.situacionPoliza = situacionPoliza;
	}
	public Integer getNumRenovacion() {
		return numRenovacion;
	}
	public void setNumRenovacion(Integer numRenovacion) {
		this.numRenovacion = numRenovacion;
	}
	public Date getFinVigencia() {
		return finVigencia;
	}
	public void setFinVigencia(Date finVigencia) {
		this.finVigencia = finVigencia;
	}
	public Date getIniVigencia() {
		return iniVigencia;
	}
	public void setIniVigencia(Date iniVigencia) {
		this.iniVigencia = iniVigencia;
	}
}