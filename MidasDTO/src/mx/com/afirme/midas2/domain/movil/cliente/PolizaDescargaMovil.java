package mx.com.afirme.midas2.domain.movil.cliente;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.base.LogBaseDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * TRUSRPOLIZADESCARGAdescarga entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TRUSRPOLIZADESCARGA", schema = "MIDAS")
public class PolizaDescargaMovil extends LogBaseDTO implements Entidad{

	// Fields

		private Long id;
		private String usuario;
		private String poliza;
		private Date fecha;
		private Long descargas;

		// Property accessors
		@Id
		@Column(name = "ID")
		@SequenceGenerator(name="TRUSRPOLIZADESCARGA_ID_GENERATOR", sequenceName="MIDAS.TRUSRPOLIZADESC_SEQ", allocationSize=1)
		@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TRUSRPOLIZADESCARGA_ID_GENERATOR")

		public Long getId() {
			return this.id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		@Column(name = "USUARIO", length = 40)
		public String getUsuario() {
			return this.usuario;
		}

		public void setUsuario(String usuario) {
			this.usuario = usuario;
		}

		@Column(name = "POLIZA", length = 20)
		public String getPoliza() {
			return this.poliza;
		}

		public void setPoliza(String poliza) {
			this.poliza = poliza;
		}

		@Column(name = "FECHA")
		@Temporal(TemporalType.DATE)
		public Date getFecha() {
			return this.fecha;
		}

		public void setFecha(Date fecha) {
			this.fecha = fecha;
		}

		@Column(name = "DESCARGAS", precision = 10, scale = 0)
		public Long getDescargas() {
			return this.descargas;
		}

		public void setDescargas(Long descargas) {
			this.descargas = descargas;
		}

		@SuppressWarnings("unchecked")
		@Override
		public Long getKey() {
			// TODO Apéndice de método generado automáticamente
			return id;
		}

		@Override
		public String getValue() {
			// TODO Apéndice de método generado automáticamente
			return null;
		}

		@Override
		public <K> K getBusinessKey() {
			// TODO Apéndice de método generado automáticamente
			return null;
		}

}