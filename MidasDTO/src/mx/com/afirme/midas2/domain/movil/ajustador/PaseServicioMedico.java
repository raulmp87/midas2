package mx.com.afirme.midas2.domain.movil.ajustador;

import java.util.Date;

/**
 * @author jreyes
 *
 */
public class PaseServicioMedico {

	private String numFolio;
	private Date fPaseAtencion;
	private Long numeroReporte;
	private Long numeroSiniestro;
	private String poliza;
	private Integer numeroInciso;
	private String coberturaAfectada;
	private String institucionHospitalaria;
	private String telefonoHospital;
	private Domicilio domicilioHospital;
	
	private Integer edad;
	private String telefonos;
	private Date fOcurrio;
	private String descripcionLesiones;
	private Double atencionHastaCantidad;
	private String nombreLesionado;
	private String numeroReporteCabina;
	private String numeroSiniestroCabina;
	
	private String cia;
	
	public String getNumFolio() {
		return numFolio;
	}
	public void setNumFolio(String numFolio) {
		this.numFolio = numFolio;
	}
	public Date getfPaseAtencion() {
		return fPaseAtencion;
	}
	public void setfPaseAtencion(Date fPaseAtencion) {
		this.fPaseAtencion = fPaseAtencion;
	}
	public Long getNumeroReporte() {
		return numeroReporte;
	}
	public void setNumeroReporte(Long numeroReporte) {
		this.numeroReporte = numeroReporte;
	}
	public Long getNumeroSiniestro() {
		return numeroSiniestro;
	}
	public void setNumeroSiniestro(Long numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}
	public String getPoliza() {
		return poliza;
	}
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	public Integer getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public String getCoberturaAfectada() {
		return coberturaAfectada;
	}
	public void setCoberturaAfectada(String coberturaAfectada) {
		this.coberturaAfectada = coberturaAfectada;
	}
	public String getInstitucionHospitalaria() {
		return institucionHospitalaria;
	}
	public void setInstitucionHospitalaria(String institucionHospitalaria) {
		this.institucionHospitalaria = institucionHospitalaria;
	}
	public String getTelefonoHospital() {
		return telefonoHospital;
	}
	public void setTelefonoHospital(String telefonoHospital) {
		this.telefonoHospital = telefonoHospital;
	}
	public Integer getEdad() {
		return edad;
	}
	public void setEdad(Integer edad) {
		this.edad = edad;
	}
	public Domicilio getDomicilioHospital() {
		if(domicilioHospital==null){
			domicilioHospital = new Domicilio();
		}
		return domicilioHospital;
	}
	public void setDomicilioHospital(Domicilio domicilioHospital) {
		this.domicilioHospital = domicilioHospital;
	}
	public String getTelefonos() {
		return telefonos;
	}
	public void setTelefonos(String telefonos) {
		this.telefonos = telefonos;
	}
	public Date getfOcurrio() {
		return fOcurrio;
	}
	public void setfOcurrio(Date fOcurrio) {
		this.fOcurrio = fOcurrio;
	}
	public String getDescripcionLesiones() {
		return descripcionLesiones;
	}
	public void setDescripcionLesiones(String descripcionLesiones) {
		this.descripcionLesiones = descripcionLesiones;
	}
	public Double getAtencionHastaCantidad() {
		return atencionHastaCantidad;
	}
	public void setAtencionHastaCantidad(Double atencionHastaCantidad) {
		this.atencionHastaCantidad = atencionHastaCantidad;
	}
	public String getNombreLesionado() {
		return nombreLesionado;
	}
	public void setNombreLesionado(String nombreLesionado) {
		this.nombreLesionado = nombreLesionado;
	}
	public String getCia() {
		return cia;
	}
	public void setCia(String cia) {
		this.cia = cia;
	}
	public String getNumeroReporteCabina() {
		return numeroReporteCabina;
	}
	public void setNumeroReporteCabina(String numeroReporteCabina) {
		this.numeroReporteCabina = numeroReporteCabina;
	}
	public String getNumeroSiniestroCabina() {
		return numeroSiniestroCabina;
	}
	public void setNumeroSiniestroCabina(String numeroSiniestroCabina) {
		this.numeroSiniestroCabina = numeroSiniestroCabina;
	}	
}
