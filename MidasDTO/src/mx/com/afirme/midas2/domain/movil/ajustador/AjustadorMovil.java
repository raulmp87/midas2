package mx.com.afirme.midas2.domain.movil.ajustador;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="TOAJUSTADORMOVIL", schema="MIDAS")
public class AjustadorMovil implements Serializable {

	private Long id;
	private String codigoUsuario;
	private Long idSeycos;
	private String nombre;
	
	@Id
	@SequenceGenerator(name = "IDTOAJUSTADORMOVIL_SEQ", allocationSize = 1, sequenceName = "MIDAS.IDTOAJUSTADORMOVIL_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOAJUSTADORMOVIL_SEQ")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigoUsuario() {
		return codigoUsuario;
	}

	public void setCodigoUsuario(String codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}

	public Long getIdSeycos() {
		return idSeycos;
	}

	public void setIdSeycos(Long idSeycos) {
		this.idSeycos = idSeycos;
	}
	
	@Transient
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
