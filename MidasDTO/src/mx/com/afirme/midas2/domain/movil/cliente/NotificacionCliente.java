package mx.com.afirme.midas2.domain.movil.cliente;

public class NotificacionCliente {
	
	private String mensaje;
	private String tipoNotificacion;
	private String fechaCreacion;
	private String usuario;
	private String idtopoliza;
	private String IdClientePoliza;
	private String  poliza;
	private String  tipoPoliza;
	private String  idNotificacion;
	
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getTipoNotificacion() {
		return tipoNotificacion;
	}
	public void setTipoNotificacion(String tipoNotificacion) {
		this.tipoNotificacion = tipoNotificacion;
	}
	public String getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getIdtopoliza() {
		return idtopoliza;
	}
	public void setIdtopoliza(String idtopoliza) {
		this.idtopoliza = idtopoliza;
	}
	public String getIdClientePoliza() {
		return IdClientePoliza;
	}
	public void setIdClientePoliza(String idClientePoliza) {
		IdClientePoliza = idClientePoliza;
	}
	public String getPoliza() {
		return poliza;
	}
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	public String getTipoPoliza() {
		return tipoPoliza;
	}
	public void setTipoPoliza(String tipoPoliza) {
		this.tipoPoliza = tipoPoliza;
	}
	public String getIdNotificacion() {
		return idNotificacion;
	}
	public void setIdNotificacion(String idNotificacion) {
		this.idNotificacion = idNotificacion;
	}

}
