package mx.com.afirme.midas2.domain.movil;


public class MensajeValidacion {
	
	private String mensaje;
	private boolean esValido;
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public boolean isEsValido() {
		return esValido;
	}
	public void setEsValido(boolean esValido) {
		this.esValido = esValido;
	}
}
