package mx.com.afirme.midas2.domain.movil.ajustador;

import java.io.Serializable;

public class ProcedeSiniestro implements Serializable {
	
	private Boolean procede;
	private String descripcion;
	
	public Boolean getProcede() {
		return procede;
	}
	
	public void setProcede(Boolean procede) {
		this.procede = procede;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}
