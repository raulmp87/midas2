package mx.com.afirme.midas2.domain.movil.ajustador;

import java.util.Date;

/**
 * @author jreyes
 *
 */
public class AtencionATercero {
	
	private String numFolio;
	private Date fPaseAtencion;
	private Long numeroReporte;
	private Long numeroSiniestro;
	private String poliza;
	private Integer numeroInciso;
	private String nombreTallerAsignado;
	private String telefonoTallerAsignado;
	private Domicilio domicilioTaller;
	
	private String nombreInteresado;
	private Domicilio domicilioInteresado;
	
	private String cia;
	
	private String telefonos;
	private String email;	
	private String marcaVehiculo;
	private String tipoVehiculo;
	private Integer modeloVehiculo;
	private String placasVehiculo;
	private String numSerieVehiculo;
	private String areasDaniadas;
	private String daniosPreexistentes;
	private String numeroReporteCabina;
	private String numeroSiniestroCabina;
	
	public String getNumFolio() {
		return numFolio;
	}
	public void setNumFolio(String numFolio) {
		this.numFolio = numFolio;
	}
	public Date getfPaseAtencion() {
		return fPaseAtencion;
	}
	public void setfPaseAtencion(Date fPaseAtencion) {
		this.fPaseAtencion = fPaseAtencion;
	}
	public Long getNumeroReporte() {
		return numeroReporte;
	}
	public void setNumeroReporte(Long numeroReporte) {
		this.numeroReporte = numeroReporte;
	}
	public Long getNumeroSiniestro() {
		return numeroSiniestro;
	}
	public void setNumeroSiniestro(Long numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}
	public String getPoliza() {
		return poliza;
	}
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	public Integer getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public String getNombreTallerAsignado() {
		return nombreTallerAsignado;
	}
	public void setNombreTallerAsignado(String nombreTallerAsignado) {
		this.nombreTallerAsignado = nombreTallerAsignado;
	}
	public String getTelefonoTallerAsignado() {
		return telefonoTallerAsignado;
	}
	public void setTelefonoTallerAsignado(String telefonoTallerAsignado) {
		this.telefonoTallerAsignado = telefonoTallerAsignado;
	}
	public String getNombreInteresado() {
		return nombreInteresado;
	}
	public void setNombreInteresado(String nombreInteresado) {
		this.nombreInteresado = nombreInteresado;
	}
	public Domicilio getDomicilioTaller() {
		if(domicilioTaller==null){
			domicilioTaller = new Domicilio();
		}
		return domicilioTaller;
	}
	public void setDomicilioTaller(Domicilio domicilioTaller) {
		this.domicilioTaller = domicilioTaller;
	}
	public Domicilio getDomicilioInteresado() {
		if(domicilioInteresado==null){
			domicilioInteresado = new Domicilio();
		}
		return domicilioInteresado;
	}
	public void setDomicilioInteresado(Domicilio domicilioInteresado) {
		this.domicilioInteresado = domicilioInteresado;
	}
	
	public String getCia() {
		return cia;
	}
	public void setCia(String cia) {
		this.cia = cia;
	}
	public String getTelefonos() {
		return telefonos;
	}
	public void setTelefonos(String telefonos) {
		this.telefonos = telefonos;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMarcaVehiculo() {
		return marcaVehiculo;
	}
	public void setMarcaVehiculo(String marcaVehiculo) {
		this.marcaVehiculo = marcaVehiculo;
	}
	public String getTipoVehiculo() {
		return tipoVehiculo;
	}
	public void setTipoVehiculo(String tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}
	public Integer getModeloVehiculo() {
		return modeloVehiculo;
	}
	public void setModeloVehiculo(Integer modeloVehiculo) {
		this.modeloVehiculo = modeloVehiculo;
	}
	public String getPlacasVehiculo() {
		return placasVehiculo;
	}
	public void setPlacasVehiculo(String placasVehiculo) {
		this.placasVehiculo = placasVehiculo;
	}
	public String getNumSerieVehiculo() {
		return numSerieVehiculo;
	}
	public void setNumSerieVehiculo(String numSerieVehiculo) {
		this.numSerieVehiculo = numSerieVehiculo;
	}
	public String getAreasDaniadas() {
		return areasDaniadas;
	}
	public void setAreasDaniadas(String areasDaniadas) {
		this.areasDaniadas = areasDaniadas;
	}
	public String getDaniosPreexistentes() {
		return daniosPreexistentes;
	}
	public void setDaniosPreexistentes(String daniosPreexistentes) {
		this.daniosPreexistentes = daniosPreexistentes;
	}
	public String getNumeroReporteCabina() {
		return numeroReporteCabina;
	}
	public void setNumeroReporteCabina(String numeroReporteCabina) {
		this.numeroReporteCabina = numeroReporteCabina;
	}
	public String getNumeroSiniestroCabina() {
		return numeroSiniestroCabina;
	}
	public void setNumeroSiniestroCabina(String numeroSiniestroCabina) {
		this.numeroSiniestroCabina = numeroSiniestroCabina;
	} 

}