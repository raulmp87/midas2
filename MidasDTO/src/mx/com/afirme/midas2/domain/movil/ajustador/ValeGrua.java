package mx.com.afirme.midas2.domain.movil.ajustador;

import java.util.Date;

/**
 * @author jreyes
 *
 */
public class ValeGrua {
	
	private String numFolio;
	private Date fVale;
	private Long numeroReporte;
	private Long numeroSiniestro;
	private Date fSiniestro;
	private String poliza;
	private Integer numeroInciso;
	private String marcaVehiculo;
	private String tipoVehiculo;
	private Integer modeloVehiculo;
	private String placasVehiculo;
	private String numSerieVehiculo;
	private String color;
	private String vehiculoAseguradoOTercero;
	private String nombreInteresado;
	private String servicioManiobraOArrastre;
	private String recogerEn;
	private String entregarEn;
	private String nombreAjustador;
	private Integer pension;
	private String numeroReporteCabina;
	private String numeroSiniestroCabina;
	
	public String getNumFolio() {
		return numFolio;
	}
	public void setNumFolio(String numFolio) {
		this.numFolio = numFolio;
	}
	public Date getfVale() {
		return fVale;
	}
	public void setfVale(Date fVale) {
		this.fVale = fVale;
	}
	public Long getNumeroReporte() {
		return numeroReporte;
	}
	public void setNumeroReporte(Long numeroReporte) {
		this.numeroReporte = numeroReporte;
	}
	public Long getNumeroSiniestro() {
		return numeroSiniestro;
	}
	public void setNumeroSiniestro(Long numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}
	public Date getfSiniestro() {
		return fSiniestro;
	}
	public void setfSiniestro(Date fSiniestro) {
		this.fSiniestro = fSiniestro;
	}
	public String getPoliza() {
		return poliza;
	}
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	public Integer getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public String getMarcaVehiculo() {
		return marcaVehiculo;
	}
	public void setMarcaVehiculo(String marcaVehiculo) {
		this.marcaVehiculo = marcaVehiculo;
	}
	public String getTipoVehiculo() {
		return tipoVehiculo;
	}
	public void setTipoVehiculo(String tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}
	public Integer getModeloVehiculo() {
		return modeloVehiculo;
	}
	public void setModeloVehiculo(Integer modeloVehiculo) {
		this.modeloVehiculo = modeloVehiculo;
	}
	public String getPlacasVehiculo() {
		return placasVehiculo;
	}
	public void setPlacasVehiculo(String placasVehiculo) {
		this.placasVehiculo = placasVehiculo;
	}
	public String getNumSerieVehiculo() {
		return numSerieVehiculo;
	}
	public void setNumSerieVehiculo(String numSerieVehiculo) {
		this.numSerieVehiculo = numSerieVehiculo;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getVehiculoAseguradoOTercero() {
		return vehiculoAseguradoOTercero;
	}
	public void setVehiculoAseguradoOTercero(String vehiculoAseguradoOTercero) {
		this.vehiculoAseguradoOTercero = vehiculoAseguradoOTercero;
	}
	public String getNombreInteresado() {
		return nombreInteresado;
	}
	public void setNombreInteresado(String nombreInteresado) {
		this.nombreInteresado = nombreInteresado;
	}
	public String getServicioManiobraOArrastre() {
		return servicioManiobraOArrastre;
	}
	public void setServicioManiobraOArrastre(String servicioManiobraOArrastre) {
		this.servicioManiobraOArrastre = servicioManiobraOArrastre;
	}
	public String getRecogerEn() {
		return recogerEn;
	}
	public void setRecogerEn(String recogerEn) {
		this.recogerEn = recogerEn;
	}
	public String getEntregarEn() {
		return entregarEn;
	}
	public void setEntregarEn(String entregarEn) {
		this.entregarEn = entregarEn;
	}
	public String getNombreAjustador() {
		return nombreAjustador;
	}
	public void setNombreAjustador(String nombreAjustador) {
		this.nombreAjustador = nombreAjustador;
	}
	public Integer getPension() {
		return pension;
	}
	public void setPension(Integer pension) {
		this.pension = pension;
	}
	public String getNumeroReporteCabina() {
		return numeroReporteCabina;
	}
	public void setNumeroReporteCabina(String numeroReporteCabina) {
		this.numeroReporteCabina = numeroReporteCabina;
	}
	public String getNumeroSiniestroCabina() {
		return numeroSiniestroCabina;
	}
	public void setNumeroSiniestroCabina(String numeroSiniestroCabina) {
		this.numeroSiniestroCabina = numeroSiniestroCabina;
	}
}