package mx.com.afirme.midas2.domain.movil.cotizador;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
/**
 * Tctarifas entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCTARIFAS", schema = "MIDAS")
public class TarifasDTO implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4335667215376516294L;
	// Fields
	private Long idtctarifas;
	private String grupovehiculos;
	private Short modelovehiculo;
	private String tipovehiculo;
	private String marcavehiculo;
	private String estado;
	private String descripciontarifa;
	private Double oc;
	private BigDecimal tarifaamplia;
	private BigDecimal tarifalimitada;
	private BigDecimal tarifaBasica;
	private BigDecimal tarifaPlata;
	private BigDecimal tarifaOro;
	private BigDecimal tarifaPlatino;
	private String clavetarifa;
	private Date fecharegistro;
	private Short bajalogica;
	private boolean tarifaValida;
	private String mensajeTarifaValida;
	// Constructors

	/** default constructor */
	public TarifasDTO() {
	}


	// Property accessors
	@Id
	@Column(name = "IDTCTARIFAS")
	@SequenceGenerator(name="TCTARIFAS_ID_GENERATOR", sequenceName="MIDAS.TCTARIFAS_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TCTARIFAS_ID_GENERATOR")
	public Long getIdtctarifas() {
		return this.idtctarifas;
	}

	public void setIdtctarifas(Long idtctarifas) {
		this.idtctarifas = idtctarifas;
	}

	@Column(name = "GRUPOVEHICULOS", length = 100)
	public String getGrupovehiculos() {
		return this.grupovehiculos;
	}

	public void setGrupovehiculos(String grupovehiculos) {
		this.grupovehiculos = grupovehiculos;
	}

	@Column(name = "MODELOVEHICULO", length = 100)
	public Short getModelovehiculo() {
		return this.modelovehiculo;
	}

	public void setModelovehiculo(Short modelovehiculo) {
		this.modelovehiculo = modelovehiculo;
	}

	@Column(name = "TIPOVEHICULO", length = 100)
	public String getTipovehiculo() {
		return this.tipovehiculo;
	}

	public void setTipovehiculo(String tipovehiculo) {
		this.tipovehiculo = tipovehiculo;
	}

	@Column(name = "MARCAVEHICULO", length = 100)
	public String getMarcavehiculo() {
		return this.marcavehiculo;
	}

	public void setMarcavehiculo(String marcavehiculo) {
		this.marcavehiculo = marcavehiculo;
	}

	@Column(name = "ESTADO", length = 100)
	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Column(name = "DESCRIPCIONTARIFA", length = 200)
	public String getDescripciontarifa() {
		return this.descripciontarifa;
	}

	public void setDescripciontarifa(String descripciontarifa) {
		this.descripciontarifa = descripciontarifa;
	}

	@Column(name = "OC", precision = 0)
	public Double getOc() {
		return this.oc;
	}

	public void setOc(Double oc) {
		this.oc = oc;
	}

	@Column(name = "TARIFAAMPLIA", precision = 0)
	public BigDecimal getTarifaamplia() {
		return this.tarifaamplia;
	}

	public void setTarifaamplia(BigDecimal tarifaamplia) {
		this.tarifaamplia = tarifaamplia;
	}

	@Column(name = "TARIFALIMITADA", precision = 0)
	public BigDecimal getTarifalimitada() {
		return this.tarifalimitada;
	}

	public void setTarifalimitada(BigDecimal tarifalimitada) {
		this.tarifalimitada = tarifalimitada;
	}

	@Column(name = "CLAVETARIFA", nullable = false, length = 200)
	public String getClavetarifa() {
		return this.clavetarifa;
	}

	public void setClavetarifa(String clavetarifa) {
		this.clavetarifa = clavetarifa;
	}
	@Column(name = "FECHAREGISTRO")
	@Temporal(TemporalType.DATE)
	public Date getFecharegistro() {
		return fecharegistro;
	}

	public void setFecharegistro(Date fecharegistro) {
		this.fecharegistro = fecharegistro;
	}


	@Column(name = "TARIFABASICA", precision = 0)
	public BigDecimal getTarifaBasica() {
		return tarifaBasica;
	}


	public void setTarifaBasica(BigDecimal tarifaBasica) {
		this.tarifaBasica = tarifaBasica;
	}

	@Column(name = "TARIFAPLATA", precision = 0)
	public BigDecimal getTarifaPlata() {
		return tarifaPlata;
	}


	public void setTarifaPlata(BigDecimal tarifaPlata) {
		this.tarifaPlata = tarifaPlata;
	}

	@Column(name = "TARIFAORO", precision = 0)
	public BigDecimal getTarifaOro() {
		return tarifaOro;
	}


	public void setTarifaOro(BigDecimal tarifaOro) {
		this.tarifaOro = tarifaOro;
	}

	@Column(name = "TARIFAPLATINO", precision = 0)
	public BigDecimal getTarifaPlatino() {
		return tarifaPlatino;
	}


	public void setTarifaPlatino(BigDecimal tarifaPlatino) {
		this.tarifaPlatino = tarifaPlatino;
	}
	
	@Column(name = "BAJALOGICA")
	public Short getBajalogica() {
		return this.bajalogica;
	}

	public void setBajalogica(Short bajalogica) {
		this.bajalogica = bajalogica;
	}

	@Transient
	public boolean isTarifaValida() {
		return tarifaValida;
	}


	public void setTarifaValida(boolean tarifaValida) {
		this.tarifaValida = tarifaValida;
	}

	@Transient
	public String getMensajeTarifaValida() {
		return mensajeTarifaValida;
	}


	public void setMensajeTarifaValida(String mensajeTarifaValida) {
		this.mensajeTarifaValida = mensajeTarifaValida;
	}
	
	
}