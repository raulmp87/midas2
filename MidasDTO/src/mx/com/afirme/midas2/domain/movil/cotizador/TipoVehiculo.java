package mx.com.afirme.midas2.domain.movil.cotizador;
public class TipoVehiculo implements java.io.Serializable {
	
	private Long id;
	private String descripcionTipoVehiculo;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescripcionTipoVehiculo() {
		return descripcionTipoVehiculo;
	}
	public void setDescripcionTipoVehiculo(String descripcionTipoVehiculo) {
		this.descripcionTipoVehiculo = descripcionTipoVehiculo;
	}
	
	
}