package mx.com.afirme.midas2.domain.movil.cliente;

import java.util.Date;

import mx.com.afirme.midas2.annotation.Exportable;


public class ClientePolizas {
	
	private String descripcion;
	private String polizaNo;
	private String estatus;
	private int polizaID;
	private String asegurado;
	private String inciso;
	private boolean bloqueado;
	private String tipo;
	private String usuario;
	private Date fechaInicioVigencia;
	private Date fechaFinVigencia;
	
	public boolean isBloqueado() {
		return bloqueado;
	}
	public void setBloqueado(boolean bloqueado) {
		this.bloqueado = bloqueado;
	}
	@Exportable(columnName="Nombre_Asegurado", columnOrder=3)
	public String getAsegurado() {
		return asegurado;
	}
	@Exportable(columnName="Numero_Inciso", columnOrder=1)
	public String getInciso() {
		return inciso;
	}
	public void setInciso(String inciso) {
		this.inciso = inciso;
	}
	public void setAsegurado(String asegurado) {
		this.asegurado = asegurado;
	}
	public int getPolizaID() {
		return polizaID;
	}
	public void setPolizaID(int polizaID) {
		this.polizaID = polizaID;
	}
	@Exportable(columnName="Descripcion", columnOrder=2)
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	@Exportable(columnName="Numero_Poliza", columnOrder=0)
	public String getPolizaNo() {
		return polizaNo;
	}
	public void setPolizaNo(String polizaNo) {
		this.polizaNo = polizaNo;
	}
	@Exportable(columnName="Estatus", columnOrder=4)
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	@Exportable(columnName="Tipo_Poliza", columnOrder=5)
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	@Exportable(columnName="Usuario_Aplicacion", columnOrder=6)
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	@Exportable(columnName="Fecha_Inicio_Vigencia", columnOrder=7)
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}
	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	@Exportable(columnName="Fecha_Fin_Vigencia", columnOrder=8)
	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}
	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	public enum EstatusPolizasMovil{
	    
	    CARATULA(1l, "1"), RECIBO(0l, "2");
	        
	    private long id;
	    private String nombre;
	        
	    private EstatusPolizasMovil(long id, String nombre) {
	      this.id = id;
	      this.nombre = nombre;
	    }
	    
	    public long getId() {
	      return id;
	    }
	    
	    public String getNombre() {
	      return nombre;
	    }
	    
	    public static EstatusPolizasMovil parse(long id) {
	    	EstatusPolizasMovil e = null; // Default
	            for (EstatusPolizasMovil item : EstatusPolizasMovil.values()) {
	                if (item.getId() ==id) {
	                    e = item;
	                    break;
	                }
	            }
	            return e;
	        }
	  }
}
