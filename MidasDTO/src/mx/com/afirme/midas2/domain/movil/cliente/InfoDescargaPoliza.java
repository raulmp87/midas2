package mx.com.afirme.midas2.domain.movil.cliente;

import java.util.Date;

public class InfoDescargaPoliza {
	
	private String descargas;
	private String usuario;
	private String poliza;
	private Date fechaDescarga;
	private String estatus;
		
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getDescargas() {
		return descargas;
	}
	public void setDescargas(String descargas) {
		this.descargas = descargas;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getPoliza() {
		return poliza;
	}
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	public Date getFechaDescarga() {
		return fechaDescarga;
	}
	public void setFechaDescarga(Date fechaDescarga) {
		this.fechaDescarga = fechaDescarga;
	}
}
