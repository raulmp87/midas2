package mx.com.afirme.midas2.domain.movil.ajustador;

import java.io.Serializable;

public class CondicionEspecial implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 996633771214840591L;
	private String nombre;
	private String descripcion;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	
}