package mx.com.afirme.midas2.domain.movil.ajustador;

import java.io.Serializable;
import java.util.List;

public class Alertas implements Serializable {

	private String sistema;
	private String alerta;
	private List<Acciones> acciones;
	
	public String getSistema() {
		return sistema;
	}
	public void setSistema(String sistema) {
		this.sistema = sistema;
	}
	public String getAlerta() {
		return alerta;
	}
	public void setAlerta(String alerta) {
		this.alerta = alerta;
	}
	public List<Acciones> getAcciones() {
		return acciones;
	}
	public void setAcciones(List<Acciones> acciones) {
		this.acciones = acciones;
	}
}
