package mx.com.afirme.midas2.domain.movil.cliente;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas.base.LogBaseDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * Trusrpoliza entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TRUSRPOLIZA", schema = "MIDAS")
public class PolizasClienteMovil extends LogBaseDTO implements Entidad{

	// Fields

		private Long id;
		private String usuario;
		private String poliza;
		private String descripcion;
		private Short inciso;
		private String tipo;
		private String bloqueado;
		private String origenTipoPoliza;
		private PolizaDTO polizaDTO;
		private Date fechaCreacion = new Date();
		private Long idCotizacionSeycos;
		private Long polizasPermitidos;
		private String nombreUsuario;
		private String email;
		private String imei;
		
		// Property accessors
		@Id
		@Column(name = "ID")
		@SequenceGenerator(name="TRUSRPOLIZA_ID_GENERATOR", sequenceName="MIDAS.TRUSRPOLIZA_SEQ", allocationSize=1)
		@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TRUSRPOLIZA_ID_GENERATOR")

		public Long getId() {
			return this.id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		@Column(name = "USUARIO")
		public String getUsuario() {
			return this.usuario;
		}

		public void setUsuario(String usuario) {
			this.usuario = usuario;
		}

		@Column(name = "POLIZA")
		public String getPoliza() {
			return this.poliza;
		}

		public void setPoliza(String poliza) {
			this.poliza = poliza;
		}

		@Column(name = "DESCRIPCION")
		public String getDescripcion() {
			return this.descripcion;
		}

		public void setDescripcion(String descripcion) {
			this.descripcion = descripcion;
		}

		@Column(name = "INCISO")
		public Short getInciso() {
			return this.inciso;
		}

		public void setInciso(Short inciso) {
			this.inciso = inciso;
		}

		@Column(name = "TIPO")
		public String getTipo() {
			return this.tipo;
		}

		public void setTipo(String tipo) {
			this.tipo = tipo;
		}

		@Column(name = "BLOQUEADO")
		public String getBloqueado() {
			return this.bloqueado;
		}

		public void setBloqueado(String bloqueado) {
			this.bloqueado = bloqueado;
		}

		public void setOrigenTipoPoliza(String origenTipoPoliza) {
			this.origenTipoPoliza = origenTipoPoliza;
		}
		
		@Column(name = "ORIGENTIPOPOLIZA")
		public String getOrigenTipoPoliza() {
			return origenTipoPoliza;
		}
		@ManyToOne(fetch=FetchType.EAGER)
		@JoinColumn(name="IDTOPOLIZA")
		public PolizaDTO getPolizaDTO() {
			return polizaDTO;
		}
		
		public void setPolizaDTO(PolizaDTO polizaDTO) {
			this.polizaDTO = polizaDTO;
		}
		@Temporal(TemporalType.DATE)
		@NotNull
		public Date getFechaCreacion() {
			return fechaCreacion;
		}
		
		public void setFechaCreacion(Date fechaCreacion) {
			this.fechaCreacion = fechaCreacion;
		}

		@SuppressWarnings("unchecked")
		@Override
		public Long  getKey() {
			// TODO Apéndice de método generado automáticamente
			return id;
		}

		@Override
		public String getValue() {
			// TODO Apéndice de método generado automáticamente
			return null;
		}

		@Override
		public <K> K getBusinessKey() {
			// TODO Apéndice de método generado automáticamente
			return null;
		}

		public void setIdCotizacionSeycos(Long idCotizacionSeycos) {
			this.idCotizacionSeycos = idCotizacionSeycos;
		}
		@Column(name = "ID_COTIZACION_SEYCOS")
		public Long getIdCotizacionSeycos() {
			return idCotizacionSeycos;
		}
		
		@Column(name = "POLIZAS_PERMITIDOS")
		public Long getPolizasPermitidos() {
			return polizasPermitidos;
		}
		public void setPolizasPermitidos(Long polizasPermitidos) {
			this.polizasPermitidos = polizasPermitidos;
		}
		
		@Column(name = "NOMBRE_PROSPECTO")
		public String getNombreUsuario() {
			return nombreUsuario;
		}
		public void setNombreUsuario(String nombreUsuario) {
			this.nombreUsuario = nombreUsuario;
		}
		
		@Column(name = "EMAIL")
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}

		/**
		 * @return the imei
		 */
		@Column(name = "IMEI")
		public String getImei() {
			return imei;
		}

		/**
		 * @param imei the imei to set
		 */
		public void setImei(String imei) {
			this.imei = imei;
		}
		
}