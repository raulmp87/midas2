package mx.com.afirme.midas2.domain.movil.ajustador;

import java.io.Serializable;
import java.util.Date;

public class Recibo implements Serializable {
	private static final long serialVersionUID = 1764117869701801566L;
	private Long id;
	private Long folio;
	private Date fechaInicio;
	private Date fechaFin;
	private String estatus;
	private String primaTotal;
	private String llaveFiscal;

	public Long getId() {
		return id;
	}

	public String getPrimaTotal() {
		return primaTotal;
	}

	public void setPrimaTotal(String primaTotal) {
		this.primaTotal = primaTotal;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getFolio() {
		return folio;
	}

	public void setFolio(Long folio) {
		this.folio = folio;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getLlaveFiscal() {
		return llaveFiscal; 
	}

	public void setLlaveFiscal(String llaveFiscal) {
		this.llaveFiscal = llaveFiscal;
	}
}
