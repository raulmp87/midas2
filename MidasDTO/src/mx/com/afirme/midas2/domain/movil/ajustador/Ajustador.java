package mx.com.afirme.midas2.domain.movil.ajustador;

import java.io.Serializable;

/**
 * Independientemente del sistema donde provenga el Ajustador este se utiliza como modelo.
 * @author amosomar
 */
public class Ajustador implements Serializable {

	private Long id;
	private String nombre;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
