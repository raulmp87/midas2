package mx.com.afirme.midas2.domain.movil.ajustador;

import java.math.BigDecimal;

import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;


public class Vehiculo extends Tercero{
	
	public Vehiculo(){
		super.setTipo("VEH");
	}
	
	public Vehiculo(Long reporteSiniestroId, Integer coberturaId){
		super.setTipo("VEH");
		super.setReporteSiniestroId(reporteSiniestroId);
		super.setCoberturaId(coberturaId);
	}
	
	public Vehiculo(String tipo, Long reporteSiniestroId, Integer coberturaId){
		super.setTipo(tipo);
		super.setReporteSiniestroId(reporteSiniestroId);
		super.setCoberturaId(coberturaId);
	}
	
	private String marcaId;
	private Integer modelo;
	private String descripcionTipoEstilo;
	private String serie;
	private String placa;
	private String color;
	private String tallerId;
	private String areasDanadas;
	private String danosPreexistentes;
	private BigDecimal pctDeducible;
	private boolean perdidaTotal;
	private String idEstado;
	private BigDecimal porcentajeDeducibleValorComercial;
	private String     motivoNoAplicaDeducibleSel;
	private BigDecimal deducible;
	private BigDecimal deducibleCalculado;
	private boolean consultaSumaAsegurada;
	/*añadiendo datos para registro RCV*/
	private String tipoTransporte;
	private String numeroMotor;
	private boolean tieneDanosPreexistentes;
	
	public String getMarcaId() {
		return marcaId;
	}
	public void setMarcaId(String marcaId) {
		this.marcaId = marcaId;
	}
	public Integer getModelo() {
		return modelo;
	}
	public void setModelo(Integer modelo) {
		this.modelo = modelo;
	}
	public String getDescripcionTipoEstilo() {
		return descripcionTipoEstilo;
	}
	public void setDescripcionTipoEstilo(String descripcionTipoEstilo) {
		this.descripcionTipoEstilo = descripcionTipoEstilo;
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getTallerId() {
		return tallerId;
	}
	public void setTallerId(String tallerId) {
		this.tallerId = tallerId;
	}
	public String getAreasDanadas() {
		return areasDanadas;
	}
	public void setAreasDanadas(String areasDanadas) {
		this.areasDanadas = areasDanadas;
	}
	public String getDanosPreexistentes() {
		return danosPreexistentes;
	}
	public void setDanosPreexistentes(String danosPreexistentes) {
		this.danosPreexistentes = danosPreexistentes;
	}	
	public BigDecimal getPctDeducible() {
		return pctDeducible;
	}
	public void setPctDeducible(BigDecimal pctDeducible) {
		this.pctDeducible = pctDeducible;
	}
	public boolean isPerdidaTotal() {
		return perdidaTotal;
	}
	public void setPerdidaTotal(boolean perdidaTotal) {
		this.perdidaTotal = perdidaTotal;
	}

	public void setPorcentajeDeducibleValorComercial(
			BigDecimal porcentajeDeducibleValorComercial) {
		this.porcentajeDeducibleValorComercial = porcentajeDeducibleValorComercial;
	}

	public BigDecimal getPorcentajeDeducibleValorComercial() {
		return porcentajeDeducibleValorComercial;
	}

	public void setMotivoNoAplicaDeducibleSel(String motivoNoAplicaDeducibleSel) {
		this.motivoNoAplicaDeducibleSel = motivoNoAplicaDeducibleSel;
	}

	public String getMotivoNoAplicaDeducibleSel() {
		return motivoNoAplicaDeducibleSel;
	}

	public void setDeducible(BigDecimal deducible) {
		this.deducible = deducible;
	}

	public BigDecimal getDeducible() {
		return deducible;
	}

	public void setDeducibleCalculado(BigDecimal deducibleCalculado) {
		this.deducibleCalculado = deducibleCalculado;
	}

	public BigDecimal getDeducibleCalculado() {
		return deducibleCalculado;
	}

	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}

	public String getIdEstado() {
		return idEstado;
	}

	public boolean isConsultaSumaAsegurada() {
		return consultaSumaAsegurada;
	}

	public void setConsultaSumaAsegurada(boolean consultaSumaAsegurada) {
		this.consultaSumaAsegurada = consultaSumaAsegurada;
	}

	public String getTipoTransporte() {
		return tipoTransporte;
	}

	public void setTipoTransporte(String tipoTransporte) {
		this.tipoTransporte = tipoTransporte;
	}

	public String getNumeroMotor() {
		return numeroMotor;
	}

	public void setNumeroMotor(String numeroMotor) {
		this.numeroMotor = numeroMotor;
	}

	public boolean isTieneDanosPreexistentes() {
		return tieneDanosPreexistentes;
	}

	public void setTieneDanosPreexistentes(boolean tieneDañosPreexistentes) {
		this.tieneDanosPreexistentes = tieneDañosPreexistentes;
	}
}
