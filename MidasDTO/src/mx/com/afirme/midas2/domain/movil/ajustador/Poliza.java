package mx.com.afirme.midas2.domain.movil.ajustador;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Poliza implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8343945202424593015L;
	private String id;
	private String numeroPoliza;
	private Integer numeroInciso;
	private Date fechaInicioVigencia;
	private Date fechaFinVigencia;
	private String lineaNegocio;
	private String estatus;
	private ProcedeSiniestro procedeSiniestro;
	private String nombreAsegurado;
	private String nombreContratante;
	private String nombreConductor;
	private String vehiculo;
	private String numeroSerie;
	private String numeroMotor;
	private String placa;
	private String capacidad;
	private String servicio;
	private String uso;
	private String tipoCarga;
	private String paquete;
	private List<Cobertura> coberturas;
	private List<Recibo> recibos;
	private List<Alertas> alertas;
	private List<CondicionEspecial> condiciones;
	private String estatusVigenciaInciso;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	public Integer getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}

	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}
	
	public String getLineaNegocio() {
		return lineaNegocio;
	}
	
	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public ProcedeSiniestro getProcedeSiniestro() {
		return procedeSiniestro;
	}
	
	public void setProcedeSiniestro(ProcedeSiniestro procedeSiniestro) {
		this.procedeSiniestro = procedeSiniestro;
	}

	public String getNombreAsegurado() {
		return nombreAsegurado;
	}

	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}
	
	public String getNombreContratante() {
		return nombreContratante;
	}
	
	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}
	
	public String getNombreConductor() {
		return nombreConductor;
	}
	
	public void setNombreConductor(String nombreConductor) {
		this.nombreConductor = nombreConductor;
	}

	public String getVehiculo() {
		return vehiculo;
	}

	public void setVehiculo(String vehiculo) {
		this.vehiculo = vehiculo;
	}

	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	
	public String getNumeroMotor() {
		return numeroMotor;
	}
	
	public void setNumeroMotor(String numeroMotor) {
		this.numeroMotor = numeroMotor;
	}
	
	public String getPlaca() {
		return placa;
	}
	
	
	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(String capacidad) {
		this.capacidad = capacidad;
	}

	public String getServicio() {
		return servicio;
	}

	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

	public String getUso() {
		return uso;
	}

	public void setUso(String uso) {
		this.uso = uso;
	}
	
	public String getTipoCarga() {
		return tipoCarga;
	}
	
	public void setTipoCarga(String tipoCarga) {
		this.tipoCarga = tipoCarga;
	}
	
	public String getPaquete() {
		return paquete;
	}
	
	public void setPaquete(String paquete) {
		this.paquete = paquete;
	}
	
	public List<Cobertura> getCoberturas() {
		return coberturas;
	}
	
	public void setCoberturas(List<Cobertura> coberturas) {
		this.coberturas = coberturas;
	}
	
	public List<Recibo> getRecibos() {
		return recibos;
	}
	
	public void setRecibos(List<Recibo> recibos) {
		this.recibos = recibos;
	}

	public List<Alertas> getAlertas() {
		return alertas;
	}

	public void setAlertas(List<Alertas> alertas) {
		this.alertas = alertas;
	}

	public List<CondicionEspecial> getCondiciones() {
		return condiciones;
	}

	public void setCondiciones(List<CondicionEspecial> condiciones) {
		this.condiciones = condiciones;
	}

	public String getEstatusVigenciaInciso() {
		return estatusVigenciaInciso;
	}

	public void setEstatusVigenciaInciso(String estatusVigenciaInciso) {
		this.estatusVigenciaInciso = estatusVigenciaInciso;
	}
}
