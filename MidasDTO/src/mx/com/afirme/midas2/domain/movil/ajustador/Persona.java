package mx.com.afirme.midas2.domain.movil.ajustador;

import java.math.BigDecimal;

public class Persona extends Tercero{
	
	public Persona(){
		super.setTipo("PERS");
	}

	public Persona(Long reporteSiniestroId, Integer coberturaId){
		super.setTipo("PERS");
		super.setReporteSiniestroId(reporteSiniestroId);
		super.setCoberturaId(coberturaId);
	}
	
	public Persona(String tipo, Long reporteSiniestroId, Integer coberturaId){
		super.setTipo(tipo);
		super.setReporteSiniestroId(reporteSiniestroId);
		super.setCoberturaId(coberturaId);
	}

	private String hospitalId;
	private String medicoId;
	private String lesiones;
	private Integer edad;
	private boolean homicidio;
	private String numOrdenAtencion;
	private BigDecimal limiteAtencion;
	private String tipoAtencion;
	
	public String getHospitalId() {
		return hospitalId;
	}
	public void setHospitalId(String hospitalId) {
		this.hospitalId = hospitalId;
	}
	public String getMedicoId() {
		return medicoId;
	}
	public void setMedicoId(String medicoId) {
		this.medicoId = medicoId;
	}
	public String getLesiones() {
		return lesiones;
	}
	public void setLesiones(String lesiones) {
		this.lesiones = lesiones;
	}
	public Integer getEdad() {
		return edad;
	}
	public void setEdad(Integer edad) {
		this.edad = edad;
	}
	public boolean isHomicidio() {
		return homicidio;
	}
	public void setHomicidio(boolean homicidio) {
		this.homicidio = homicidio;
	}	
	public String getNumOrdenAtencion() {
		return numOrdenAtencion;
	}
	public void setNumOrdenAtencion(String numOrdenAtencion) {
		this.numOrdenAtencion = numOrdenAtencion;
	}
	public BigDecimal getLimiteAtencion() {
		return limiteAtencion;
	}
	public void setLimiteAtencion(BigDecimal limiteAtencion) {
		this.limiteAtencion = limiteAtencion;
	}

	public String getTipoAtencion() {
		return tipoAtencion;
	}

	public void setTipoAtencion(String tipoAtencion) {
		this.tipoAtencion = tipoAtencion;
	}	
}
