package mx.com.afirme.midas2.domain.movil.ajustador;

import java.io.Serializable;
import java.util.Date;

public class FindPolizaResponse implements Serializable {
	
	private String id;
	private String numeroPoliza;
	private Integer numeroInciso;
	private String numeroSerie;
	private String vehiculo;
	private Date fechaInicioVigencia;
	private Date fechaFinVigencia;
	private String estatus;
	private String placa;
	private ProcedeSiniestro procedeSiniestro;
	private String nombreContratante;
	private String estatusVigenciaInciso;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	
	public Integer getNumeroInciso() {
		return numeroInciso;
	}
	
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public String getVehiculo() {
		return vehiculo;
	}

	public void setVehiculo(String vehiculo) {
		this.vehiculo = vehiculo;
	}

	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}

	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}
	
	public String getEstatus() {
		return estatus;
	}
	
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	
	public String getPlaca() {
		return placa;
	}
	
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	
	public ProcedeSiniestro getProcedeSiniestro() {
		return procedeSiniestro;
	}
	
	public void setProcedeSiniestro(ProcedeSiniestro procedeSiniestro) {
		this.procedeSiniestro = procedeSiniestro;
	}
	
	public String getNombreContratante() {
		return nombreContratante;
	}
	
	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}

	public String getEstatusVigenciaInciso() {
		return estatusVigenciaInciso;
	}

	public void setEstatusVigenciaInciso(String estatusVigenciaInciso) {
		this.estatusVigenciaInciso = estatusVigenciaInciso;
	}
	
}
