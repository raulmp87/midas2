package mx.com.afirme.midas2.domain.movil.ajustador;

import java.io.Serializable;
import java.math.BigDecimal;

public class Cobertura implements Serializable {
	
	private Long id;
	private String nombre;
	private String limiteMaxResponsabilidad;
	private BigDecimal deducible;
	private BigDecimal prima;
	private boolean afectable;
	private boolean registro;
	private String tipoEstimacion;
	private String cveTipoCalculoCobertura;
	private String descripcionDeducible;
	private String claveFuenteSumaAsegurada;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getLimiteMaxResponsabilidad() {
		return limiteMaxResponsabilidad;
	}

	public void setLimiteMaxResponsabilidad(String limiteMaxResponsabilidad) {
		this.limiteMaxResponsabilidad = limiteMaxResponsabilidad;
	}

	public BigDecimal getDeducible() {
		return deducible;
	}

	public void setDeducible(BigDecimal deducible) {
		this.deducible = deducible;
	}

	public BigDecimal getPrima() {
		return prima;
	}

	public void setPrima(BigDecimal prima) {
		this.prima = prima;
	}

	public boolean isAfectable() {
		return afectable;
	}

	public void setAfectable(boolean afectable) {
		this.afectable = afectable;
	}

	public boolean isRegistro() {
		return registro;
	}

	public void setRegistro(boolean registro) {
		this.registro = registro;
	}

	public void setTipoEstimacion(String tipoEstimacion) {
		this.tipoEstimacion = tipoEstimacion;
	}

	public String getTipoEstimacion() {
		return tipoEstimacion;
	}

	public void setCveTipoCalculoCobertura(String cveTipoCalculoCobertura) {
		this.cveTipoCalculoCobertura = cveTipoCalculoCobertura;
	}

	public String getCveTipoCalculoCobertura() {
		return cveTipoCalculoCobertura;
	}

	public String getDescripcionDeducible() {
		return descripcionDeducible;
	}

	public void setDescripcionDeducible(String descripcionDeducible) {
		this.descripcionDeducible = descripcionDeducible;
	}

	public String getClaveFuenteSumaAsegurada() {
		return claveFuenteSumaAsegurada;
	}

	public void setClaveFuenteSumaAsegurada(String claveFuenteSumaAsegurada) {
		this.claveFuenteSumaAsegurada = claveFuenteSumaAsegurada;
	}
}