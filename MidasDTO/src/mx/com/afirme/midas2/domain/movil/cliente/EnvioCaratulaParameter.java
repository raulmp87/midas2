package mx.com.afirme.midas2.domain.movil.cliente;

import java.io.Serializable;

import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.endoso.GeneraPlantillaReporteBitemporalService;
import mx.com.afirme.midas2.util.MailService;

public class EnvioCaratulaParameter implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6025007422545122329L;
	private String username;
	private String correo;
	private String mailAgente;
	private String polizaID;
	private String URL;
	private boolean isPolizaMidas;
	private MailService mailService;
	private EntidadService entidadService;
	private GeneraPlantillaReporteBitemporalService generaPlantillaReporteBitemporalService;
	private String llaveFiscal;
	private String tipoPoliza;
	private String message;
	private String title;
	private String subject;
	private String greeting;
	private Long idNotificacionRenov;
	
	public String getTipoPoliza() {
		return tipoPoliza;
	}
	public void setTipoPoliza(String tipoPoliza) {
		this.tipoPoliza = tipoPoliza;
	}
	public String getLlaveFiscal() {
		return llaveFiscal;
	}
	public void setLlaveFiscal(String llaveFiscal) {
		this.llaveFiscal = llaveFiscal;
	}
	public String getUsername() {
		return username;
	}
	public String getMailAgente() {
		return mailAgente;
	}
	public void setMailAgente(String mailAgente) {
		this.mailAgente = mailAgente;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getPolizaID() {
		return polizaID;
	}
	public void setPolizaID(String polizaID) {
		this.polizaID = polizaID;
	}
	public String getURL() {
		return URL;
	}
	public void setURL(String uRL) {
		URL = uRL;
	}
	public boolean isPolizaMidas() {
		return isPolizaMidas;
	}
	public void setPolizaMidas(boolean isPolizaMidas) {
		this.isPolizaMidas = isPolizaMidas;
	}
	public MailService getMailService() {
		return mailService;
	}
	public void setMailService(MailService mailService) {
		this.mailService = mailService;
	}
	public EntidadService getEntidadService() {
		return entidadService;
	}
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	public GeneraPlantillaReporteBitemporalService getGeneraPlantillaReporteBitemporalService() {
		return generaPlantillaReporteBitemporalService;
	}
	public void setGeneraPlantillaReporteBitemporalService(
			GeneraPlantillaReporteBitemporalService generaPlantillaReporteBitemporalService) {
		this.generaPlantillaReporteBitemporalService = generaPlantillaReporteBitemporalService;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getGreeting() {
		return greeting;
	}
	public void setGreeting(String greeting) {
		this.greeting = greeting;
	}
	public Long getIdNotificacionRenov() {
		return idNotificacionRenov;
	}
	public void setIdNotificacionRenov(Long idNotificacionRenov) {
		this.idNotificacionRenov = idNotificacionRenov;
	}

}
