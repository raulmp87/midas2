package mx.com.afirme.midas2.domain.movil.cotizador;
public class ModeloVehiculo implements java.io.Serializable {

	// Fields

	private Long id;
	private Short modeloVehiculo;
	// Constructors
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Short getModeloVehiculo() {
		return modeloVehiculo;
	}
	public void setModeloVehiculo(Short modeloVehiculo) {
		this.modeloVehiculo = modeloVehiculo;
	}
	

	

}