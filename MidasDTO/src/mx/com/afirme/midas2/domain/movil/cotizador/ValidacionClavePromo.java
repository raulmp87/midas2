package mx.com.afirme.midas2.domain.movil.cotizador;

public class ValidacionClavePromo implements java.io.Serializable {
	// Fields
	private String clavePromo;
	private boolean esValido;
	public String getClavePromo() {
		return clavePromo;
	}
	public void setClavePromo(String clavePromo) {
		this.clavePromo = clavePromo;
	}
	public boolean getEsValido() {
		return esValido;
	}
	public void setEsValido(boolean esValido) {
		this.esValido = esValido;
	} 
}