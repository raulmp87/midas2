package mx.com.afirme.midas2.domain.movil.cliente;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.base.LogBaseDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * Trusrnotificacion entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TRUSRNOTIFICACION", schema = "MIDAS")
public class NotificacionClienteMovil extends LogBaseDTO implements Entidad{

	// Fields

		private Long id;
		private String mensaje;
		private Date fechacreacion;
		private String tiponotificacion;
		private String poliza;
		private PolizaDTO polizaDTO;
		private Long id_cotizacion_seycos;
		// Property accessors

		@Id
		@Column(name = "ID")
		@SequenceGenerator(name="TRUSRNOTIFICACION_ID_GENERATOR", sequenceName="MIDAS.TRUSRNOTIFICACION_SEQ", allocationSize=1)
		@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TRUSRNOTIFICACION_ID_GENERATOR")
		public Long getId() {
			return this.id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		@Column(name = "MENSAJE")
		public String getMensaje() {
			return this.mensaje;
		}

		public void setMensaje(String mensaje) {
			this.mensaje = mensaje;
		}

		@Column(name = "FECHACREACION")
		@Temporal(TemporalType.DATE)
		public Date getFechacreacion() {
			return this.fechacreacion;
		}

		public void setFechacreacion(Date fechacreacion) {
			this.fechacreacion = fechacreacion;
		}

		@Column(name = "TIPONOTIFICACION", length = 80)
		public String getTiponotificacion() {
			return this.tiponotificacion;
		}

		public void setTiponotificacion(String tiponotificacion) {
			this.tiponotificacion = tiponotificacion;
		}

		@Column(name = "POLIZA", length = 20)
		public String getPoliza() {
			return this.poliza;
		}

		public void setPoliza(String poliza) {
			this.poliza = poliza;
		}

		@SuppressWarnings("unchecked")
		@Override
		public Long getKey() {
			// TODO Apéndice de método generado automáticamente
			return id;
		}

		@Override
		public String getValue() {
			// TODO Apéndice de método generado automáticamente
			return null;
		}

		@Override
		public <K> K getBusinessKey() {
			// TODO Apéndice de método generado automáticamente
			return null;
		}
		
		@JoinColumn(name="IDTOPOLIZA", nullable = false)
		public PolizaDTO getPolizaDTO() {
			return polizaDTO;
		}
		
		public void setPolizaDTO(PolizaDTO polizaDTO) {
			this.polizaDTO = polizaDTO;
		}
		@Column(name = "ID_COTIZACION_SEYCOS")
		public Long getId_cotizacion_seycos() {
			return id_cotizacion_seycos;
		}

		public void setId_cotizacion_seycos(Long id_cotizacion_seycos) {
			this.id_cotizacion_seycos = id_cotizacion_seycos;
		}
		
}