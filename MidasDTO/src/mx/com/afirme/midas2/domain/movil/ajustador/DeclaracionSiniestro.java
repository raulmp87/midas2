package mx.com.afirme.midas2.domain.movil.ajustador;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author jreyes
 *
 */
public class DeclaracionSiniestro  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7894853713211183563L;
	
	private Date fSiniestro;
	private Date fAtencion;
	private Long numeroReporte;
	private Long numeroSiniestro;
	private Date hora;
	private String poliza;
	private Integer inciso;
	private String nombreAsegurado;
	private String nombreConductor;
	private String marcaVehiculoAsegurado;
	private String tipoVehiculoAsegurado;
	private Integer modeloVehiculoAsegurado;
	private String placasVehiculoAsegurado;
	private String numSerieVehiculoAsegurado;
	private String declaracionSiniestro;
	private String terminoAjuste;
	private String responsabilidad;
	private String estatusPoliza;
	private String numeroReporteCabina;
	private String numeroSiniestroCabina;
	private String motivoSituacion;
	private Long idReporte;
	
	private List<Tercero> terceros;
	
	public Date getfSiniestro() {
		return fSiniestro;
	}
	public void setfSiniestro(Date fSiniestro) {
		this.fSiniestro = fSiniestro;
	}
	public Date getfAtencion() {
		return fAtencion;
	}
	public void setfAtencion(Date fAtencion) {
		this.fAtencion = fAtencion;
	}
	public Long getNumeroReporte() {
		return numeroReporte;
	}
	public void setNumeroReporte(Long numeroReporte) {
		this.numeroReporte = numeroReporte;
	}
	public Long getNumeroSiniestro() {
		return numeroSiniestro;
	}
	public void setNumeroSiniestro(Long numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}
	public Integer getInciso() {
		return inciso;
	}
	public void setInciso(Integer inciso) {
		this.inciso = inciso;
	}
	public Date getHora() {
		return hora;
	}
	public void setHora(Date hora) {
		this.hora = hora;
	}
	public String getPoliza() {
		return poliza;
	}
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}
	public String getNombreConductor() {
		return nombreConductor;
	}
	public void setNombreConductor(String nombreConductor) {
		this.nombreConductor = nombreConductor;
	}
	public String getMarcaVehiculoAsegurado() {
		return marcaVehiculoAsegurado;
	}
	public void setMarcaVehiculoAsegurado(String marcaVehiculoAsegurado) {
		this.marcaVehiculoAsegurado = marcaVehiculoAsegurado;
	}
	public String getTipoVehiculoAsegurado() {
		return tipoVehiculoAsegurado;
	}
	public void setTipoVehiculoAsegurado(String tipoVehiculoAsegurado) {
		this.tipoVehiculoAsegurado = tipoVehiculoAsegurado;
	}
	public Integer getModeloVehiculoAsegurado() {
		return modeloVehiculoAsegurado;
	}
	public void setModeloVehiculoAsegurado(Integer modeloVehiculoAsegurado) {
		this.modeloVehiculoAsegurado = modeloVehiculoAsegurado;
	}
	public String getPlacasVehiculoAsegurado() {
		return placasVehiculoAsegurado;
	}
	public void setPlacasVehiculoAsegurado(String placasVehiculoAsegurado) {
		this.placasVehiculoAsegurado = placasVehiculoAsegurado;
	}
	public String getNumSerieVehiculoAsegurado() {
		return numSerieVehiculoAsegurado;
	}
	public void setNumSerieVehiculoAsegurado(String numSerieVehiculoAsegurado) {
		this.numSerieVehiculoAsegurado = numSerieVehiculoAsegurado;
	}
	public String getDeclaracionSiniestro() {
		return declaracionSiniestro;
	}
	public void setDeclaracionSiniestro(String declaracionSiniestro) {
		this.declaracionSiniestro = declaracionSiniestro;
	}
	public List<Tercero> getTerceros() {
		return terceros;
	}
	public void setTerceros(List<Tercero> terceros) {
		this.terceros = terceros;
	}
	/**
	 * @return el terminoAjuste
	 */
	public String getTerminoAjuste() {
		return terminoAjuste;
	}
	/**
	 * @param terminoAjuste el terminoAjuste a establecer
	 */
	public void setTerminoAjuste(String terminoAjuste) {
		this.terminoAjuste = terminoAjuste;
	}
	/**
	 * @return el responsabilidad
	 */
	public String getResponsabilidad() {
		return responsabilidad;
	}
	/**
	 * @param responsabilidad el responsabilidad a establecer
	 */
	public void setResponsabilidad(String responsabilidad) {
		this.responsabilidad = responsabilidad;
	}
	public void setEstatusPoliza(String estatusPoliza) {
		this.estatusPoliza = estatusPoliza;
	}
	public String getEstatusPoliza() {
		return estatusPoliza;
	}
	public String getNumeroReporteCabina() {
		return numeroReporteCabina;
	}
	public void setNumeroReporteCabina(String numeroReporteCabina) {
		this.numeroReporteCabina = numeroReporteCabina;
	}
	public String getNumeroSiniestroCabina() {
		return numeroSiniestroCabina;
	}
	public void setNumeroSiniestroCabina(String numeroSiniestroCabina) {
		this.numeroSiniestroCabina = numeroSiniestroCabina;
	}
	public String getMotivoSituacion() {
		return motivoSituacion;
	}
	public void setMotivoSituacion(String motivoSituacion) {
		this.motivoSituacion = motivoSituacion;
	}
	public Long getIdReporte() {
		return idReporte;
	}
	public void setIdReporte(Long idReporte) {
		this.idReporte = idReporte;
	}	
}
