package mx.com.afirme.midas2.domain.movil.ajustador;

import java.io.Serializable;

public class CoberturaIncisoParameter implements Serializable {
	private String polizaId;
	private Long reporteSiniestroId;
	private Integer coberturaId;	
	private String tipoEstimacion;
	private String responsabilidad;
	private String codigoCausaSiniestro;
	private String codigoTerminoAjuste;
	
	public String getPolizaId() {
		return polizaId;
	}
	public void setPolizaId(String polizaId) {
		this.polizaId = polizaId;
	}
	public Long getReporteSiniestroId() {
		return reporteSiniestroId;
	}
	public void setReporteSiniestroId(Long reporteSiniestroId) {
		this.reporteSiniestroId = reporteSiniestroId;
	}
	public Integer getCoberturaId() {
		return coberturaId;
	}
	public void setCoberturaId(Integer coberturaId) {
		this.coberturaId = coberturaId;
	}
	public void setTipoEstimacion(String tipoEstimacion) {
		this.tipoEstimacion = tipoEstimacion;
	}
	public String getTipoEstimacion() {
		return tipoEstimacion;
	}
	public void setResponsabilidad(String responsabilidad) {
		this.responsabilidad = responsabilidad;
	}
	public String getResponsabilidad() {
		return responsabilidad;
	}
	public String getCodigoCausaSiniestro() {
		return codigoCausaSiniestro;
	}
	public void setCodigoCausaSiniestro(String codigoCausaSiniestro) {
		this.codigoCausaSiniestro = codigoCausaSiniestro;
	}
	public String getCodigoTerminoAjuste() {
		return codigoTerminoAjuste;
	}
	public void setCodigoTerminoAjuste(String codigoTerminoAjuste) {
		this.codigoTerminoAjuste = codigoTerminoAjuste;
	}	
}
