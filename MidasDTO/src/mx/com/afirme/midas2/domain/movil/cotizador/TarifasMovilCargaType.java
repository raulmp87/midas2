package mx.com.afirme.midas2.domain.movil.cotizador;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.math.BigDecimal;

public class TarifasMovilCargaType extends TarifasMovilCarga implements SQLData {

    public String getSQLTypeName() throws SQLException {
        return "ACTOR_TYPE"; 
    }

    public void readSQL(SQLInput sqlInput, String string) throws SQLException { 
        setGrupovehiculos(sqlInput.readString());

    }

    public void writeSQL(SQLOutput sqlOutput) throws SQLException { 
        
    }

}