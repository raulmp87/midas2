package mx.com.afirme.midas2.domain.movil.ajustador;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="TCTIPODOCSINIESTROAUT", schema="MIDAS")
public class TipoDocumentoSiniestro implements Serializable {

	private Long id;
	private String nombre;
	private String fortimaxDocumentoNombre;
	private String fortimaxDocumentoDirectorio;
	
	@Id
	@SequenceGenerator(name = "IDTCTIPODOCSINIESTROAUT_SEQ", allocationSize = 1, sequenceName = "MIDAS.IDTCTIPODOCSINIESTROAUT_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCTIPODOCSINIESTROAUT_SEQ")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	/**
	 * El nombre del documento en Fortimax.
	 * @return
	 */
	public String getFortimaxDocumentoNombre() {
		return fortimaxDocumentoNombre;
	}
	
	public void setFortimaxDocumentoNombre(String fortimaxDocumentoNombre) {
		this.fortimaxDocumentoNombre = fortimaxDocumentoNombre;
	}
	
	public String getFortimaxDocumentoDirectorio() {
		return fortimaxDocumentoDirectorio;
	}
	
	public void setFortimaxDocumentoDirectorio(
			String fortimaxDocumentoDirectorio) {
		this.fortimaxDocumentoDirectorio = fortimaxDocumentoDirectorio;
	}
	
}
