package mx.com.afirme.midas2.domain.movil.cliente;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.base.LogBaseDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * TRUSRPOLRENOVADAENVIODOC entity.
 */
@Entity
@Table(name = "TRUSRPOLRENOVADAENVIODOC", schema = "MIDAS")
public class PolizaRenovadaEnvioDocMovil extends LogBaseDTO implements Entidad{
	// Fields

			private Long id;
			private String usuario;
			private String poliza;
			private Date fechaEnvio;
			private String documentos;
			private String destinatarios;
			private String cc;
			private String asunto;
			private String mensaje;
			private String observaciones;
			private Long idToPoliza;
			private Long idCotizacionSey;
			private boolean enviado;

			// Property accessors
			@Id
			@Column(name = "ID")
			@SequenceGenerator(name="TRUSRPOLRENOVADAENVIODOC_ID_GENERATOR", sequenceName="MIDAS.TRUSRPOLRENOVADAENVIODOC_SEQ", allocationSize=1)
			@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TRUSRPOLRENOVADAENVIODOC_ID_GENERATOR")

			public Long getId() {
				return this.id;
			}

			public void setId(Long id) {
				this.id = id;
			}

			@Column(name = "USUARIO", length = 40)
			public String getUsuario() {
				return this.usuario;
			}

			public void setUsuario(String usuario) {
				this.usuario = usuario;
			}

			@Column(name = "POLIZA", length = 20)
			public String getPoliza() {
				return this.poliza;
			}

			public void setPoliza(String poliza) {
				this.poliza = poliza;
			}


			@SuppressWarnings("unchecked")
			@Override
			public Long getKey() {
				// TODO Apéndice de método generado automáticamente
				return id;
			}

			@Override
			public String getValue() {
				// TODO Apéndice de método generado automáticamente
				return null;
			}

			@Override
			public <K> K getBusinessKey() {
				// TODO Apéndice de método generado automáticamente
				return null;
			}
			@Column(name = "FECHAENVIO")
			@Temporal(TemporalType.DATE)
			public Date getFechaEnvio() {
				return fechaEnvio;
			}

			public void setFechaEnvio(Date fechaEnvio) {
				this.fechaEnvio = fechaEnvio;
			}
			@Column(name = "DOCUMENTOS")
			public String getDocumentos() {
				return documentos;
			}

			public void setDocumentos(String documentos) {
				this.documentos = documentos;
			}
			@Column(name = "PARA")
			public String getDestinatarios() {
				return destinatarios;
			}

			public void setDestinatarios(String destinatarios) {
				this.destinatarios = destinatarios;
			}
			@Column(name = "CC")
			public String getCc() {
				return cc;
			}

			public void setCc(String cc) {
				this.cc = cc;
			}
			@Column(name = "ASUNTO")
			public String getAsunto() {
				return asunto;
			}

			public void setAsunto(String asunto) {
				this.asunto = asunto;
			}
			@Column(name = "MENSAJE")
			public String getMensaje() {
				return mensaje;
			}

			public void setMensaje(String mensaje) {
				this.mensaje = mensaje;
			}
			@Column(name = "OBSERVACIONES")
			public String getObservaciones() {
				return observaciones;
			}

			public void setObservaciones(String observaciones) {
				this.observaciones = observaciones;
			}
			@Column(name = "BENVIADO")
			public boolean isEnviado() {
				return enviado;
			}

			public void setEnviado(boolean enviado) {
				this.enviado = enviado;
			}
			@Column(name = "IDTOPOLIZA")
			public Long getIdToPoliza() {
				return idToPoliza;
			}

			public void setIdToPoliza(Long idToPoliza) {
				this.idToPoliza = idToPoliza;
			}
			@Column(name = "ID_COTIZACION_SEYCOS")
			public Long getIdCotizacionSey() {
				return idCotizacionSey;
			}

			public void setIdCotizacionSey(Long idCotizacionSey) {
				this.idCotizacionSey = idCotizacionSey;
			}



}