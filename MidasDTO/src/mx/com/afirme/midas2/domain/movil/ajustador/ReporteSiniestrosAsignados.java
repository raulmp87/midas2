package mx.com.afirme.midas2.domain.movil.ajustador;

import java.io.Serializable;
import java.util.List;

public class ReporteSiniestrosAsignados implements Serializable {

	private List<ReporteSiniestro> enAtencion;
	private List<ReporteSiniestro> sinTerminoAjuste;
	
	public List<ReporteSiniestro> getEnAtencion() {
		return enAtencion;
	}
	
	public void setEnAtencion(List<ReporteSiniestro> enAtencion) {
		this.enAtencion = enAtencion;
	}
	
	public List<ReporteSiniestro> getSinTerminoAjuste() {
		return sinTerminoAjuste;
	}
	
	public void setSinTerminoAjuste(List<ReporteSiniestro> sinTerminoAjuste) {
		this.sinTerminoAjuste = sinTerminoAjuste;
	}
	
}
