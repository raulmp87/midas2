package mx.com.afirme.midas2.domain.movil.cliente;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.base.LogBaseDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * Trusrcorreo entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TRUSRCORREO", schema = "MIDAS")
public class CorreoClienteMovil extends LogBaseDTO implements Entidad{

	// Fields

		private Long id;
		private String correo;
		private String nombre;
		private Long idGerencia;


		// Property accessors

		@Id
		@Column(name = "ID")
		@SequenceGenerator(name="TRUSRCORREO_ID_GENERATOR", sequenceName="MIDAS.TRUSRCORREO_SEQ", allocationSize=1)
		@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TRUSRCORREO_ID_GENERATOR")
		public Long getId() {
			return this.id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		@Column(name = "CORREO", length = 40)
		public String getCorreo() {
			return this.correo;
		}

		public void setCorreo(String correo) {
			this.correo = correo;
		}

		@Column(name = "NOMBRE", length = 80)
		public String getNombre() {
			return this.nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		@Column(name = "ID_GERENCIA", precision = 10, scale = 0)
		public Long getIdGerencia() {
			return this.idGerencia;
		}

		public void setIdGerencia(Long idGerencia) {
			this.idGerencia = idGerencia;
		}

		@SuppressWarnings("unchecked")
		@Override
		public Long getKey() {
			// TODO Apéndice de método generado automáticamente
			return id;
		}

		@Override
		public String getValue() {
			// TODO Apéndice de método generado automáticamente
			return null;
		}

		@Override
		public <K> K getBusinessKey() {
			// TODO Apéndice de método generado automáticamente
			return null;
		}

}