package mx.com.afirme.midas2.domain.movil;

import java.util.List;

public class RespuestaCargaMasiva implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields
	private List<String> registrosAfectados;
	private Integer  registrosNoInsertados;
	private Integer registrosInsertados;
	private Integer registrosActualizados;
	public List<String> getRegistrosAfectados() {
		return registrosAfectados;
	}
	public void setRegistrosAfectados(List<String> registrosAfectados) {
		this.registrosAfectados = registrosAfectados;
	}
	public Integer getRegistrosNoInsertados() {
		return registrosNoInsertados;
	}
	public void setRegistrosNoInsertados(Integer registrosNoInsertados) {
		this.registrosNoInsertados = registrosNoInsertados;
	}
	public Integer getRegistrosInsertados() {
		return registrosInsertados;
	}
	public void setRegistrosInsertados(Integer registrosInsertados) {
		this.registrosInsertados = registrosInsertados;
	}
	public Integer getRegistrosActualizados() {
		return registrosActualizados;
	}
	public void setRegistrosActualizados(Integer registrosActualizados) {
		this.registrosActualizados = registrosActualizados;
	} 
}