package mx.com.afirme.midas2.domain.movil.ajustador;

import java.math.BigDecimal;

public class Tercero {
	
	public Tercero(){
		super();
	}
	
	public Tercero(Long reporteSiniestroId, Integer coberturaId){	
		this.setReporteSiniestroId(reporteSiniestroId);
		this.setCoberturaId(coberturaId);
	}
	
	private Long id;
	private Long reporteSiniestroId;
	private String nombre;
	private String contacto;
	private String descripcion;
	private String telefono;
	private String correo;
	private String cobertura;
	private Integer coberturaId;
	private BigDecimal sumaAmparada;
	private BigDecimal estimacion;
	private String causaMovimiento;
	private BigDecimal estimacionAnterior;
	private Boolean aplicaDeducible;
	private String observaciones;	
	private String tipo;
	private String tipoPase;
	private String tipoEstimacion;
	private String tipoCalculo;
	private String estatus;
	private String marca;
	private String folio;
	private String fuenteSumaAsegurada;
	/*añadiendodatos de RCV*/
	private String tipoPersona;
	private String circunstancia;
	private Boolean esOtraCircunstancia;
	private String otraCircunstancia;
	private String dua;
	private String claveAjustadorTercero;
	private String nombreAjustadorTercero;
	/**
	 * Compañia Tercero
	 */
	private Boolean tieneCia;
	private Integer ciaId;
	private String siniestroCia;
	private Boolean recibeOrdenCia;
	private Boolean llegoCia;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getReporteSiniestroId() {
		return reporteSiniestroId;
	}
	public void setReporteSiniestroId(Long reporteSiniestroId) {
		this.reporteSiniestroId = reporteSiniestroId;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getContacto() {
		return contacto;
	}
	public void setContacto(String contacto) {
		this.contacto = contacto;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}	
	public BigDecimal getSumaAmparada() {
		return sumaAmparada;
	}
	public void setSumaAmparada(BigDecimal sumaAmparada) {
		this.sumaAmparada = sumaAmparada;
	}	
	public String getCobertura() {
		return cobertura;
	}
	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}
	public Integer getCoberturaId() {
		return coberturaId;
	}
	public void setCoberturaId(Integer coberturaId) {
		this.coberturaId = coberturaId;
	}
	public BigDecimal getEstimacion() {
		return estimacion;
	}
	public void setEstimacion(BigDecimal estimacion) {
		this.estimacion = estimacion;
	}
	public String getCausaMovimiento() {
		return causaMovimiento;
	}
	public void setCausaMovimiento(String causaMovimiento) {
		this.causaMovimiento = causaMovimiento;
	}
	public BigDecimal getEstimacionAnterior() {
		return estimacionAnterior;
	}
	public void setEstimacionAnterior(BigDecimal estimacionAnterior) {
		this.estimacionAnterior = estimacionAnterior;
	}
	public Boolean getAplicaDeducible() {
		return aplicaDeducible;
	}
	public void setAplicaDeducible(Boolean aplicaDeducible) {
		this.aplicaDeducible = aplicaDeducible;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getTipoPase() {
		return tipoPase;
	}
	public void setTipoPase(String tipoPase) {
		this.tipoPase = tipoPase;
	}
	public Boolean getTieneCia() {
		return tieneCia;
	}
	public void setTieneCia(Boolean tieneCia) {
		this.tieneCia = tieneCia;
	}
	public Integer getCiaId() {
		return ciaId;
	}
	public void setCiaId(Integer ciaId) {
		this.ciaId = ciaId;
	}
	public String getSiniestroCia() {
		return siniestroCia;
	}
	public void setSiniestroCia(String siniestroCia) {
		this.siniestroCia = siniestroCia;
	}
	public Boolean getRecibeOrdenCia() {
		return recibeOrdenCia;
	}
	public void setRecibeOrdenCia(Boolean recibeOrdenCia) {
		this.recibeOrdenCia = recibeOrdenCia;
	}
	public Boolean getLlegoCia() {
		return llegoCia;
	}
	public void setLlegoCia(Boolean llegoCia) {
		this.llegoCia = llegoCia;
	}

	public void setTipoEstimacion(String tipoEstimacion) {
		this.tipoEstimacion = tipoEstimacion;
	}

	public String getTipoEstimacion() {
		return tipoEstimacion;
	}

	public void setTipoCalculo(String tipoCalculo) {
		this.tipoCalculo = tipoCalculo;
	}

	public String getTipoCalculo() {
		return tipoCalculo;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getEstatus() {
		return estatus;

	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getMarca() {
		return marca;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getFuenteSumaAsegurada() {
		return fuenteSumaAsegurada;
	}

	public void setFuenteSumaAsegurada(String fuenteSumaAsegurada) {
		this.fuenteSumaAsegurada = fuenteSumaAsegurada;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}


	public String getCircunstancia() {
		return circunstancia;
	}

	public void setCircunstancia(String circunstancia) {
		this.circunstancia = circunstancia;
	}

	public String getDua() {
		return dua;
	}

	public void setDua(String dua) {
		this.dua = dua;
	}

	public String getClaveAjustadorTercero() {
		return claveAjustadorTercero;
	}

	public void setClaveAjustadorTercero(String claveAjustadorTercero) {
		this.claveAjustadorTercero = claveAjustadorTercero;
	}

	public String getNombreAjustadorTercero() {
		return nombreAjustadorTercero;
	}

	public void setNombreAjustadorTercero(String nombreAjustadorTercero) {
		this.nombreAjustadorTercero = nombreAjustadorTercero;
	}

	public String getOtraCircunstancia() {
		return otraCircunstancia;
	}

	public void setOtraCircunstancia(String otraCircunstancia) {
		this.otraCircunstancia = otraCircunstancia;
	}

	public Boolean getEsOtraCircunstancia() {
		return esOtraCircunstancia;
	}

	public void setEsOtraCircunstancia(Boolean esOtraCircunstancia) {
		this.esOtraCircunstancia = esOtraCircunstancia;
	}		
}
