/**
 * 
 */
package mx.com.afirme.midas2.domain.movil.ajustador;

/**
 * @author jreyes
 *
 */
public class Domicilio {

	private String pais;
	private String estado;
	private String ciudad;
	private String colonia;
	private String cp;
	private String calleNumero;
	
	public Domicilio() {
	}
	public Domicilio(String pais, String estado, String ciudad, String colonia, String cp, String calleNumero) {
		this.setPais(pais);
		this.setEstado(estado);
		this.setCiudad(ciudad);
		this.setColonia(colonia);
		this.setCp(cp);
		this.setCalleNumero(calleNumero);
	}
	
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getColonia() {
		return colonia;
	}
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	public String getCp() {
		return cp;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}
	public String getCalleNumero() {
		return calleNumero;
	}
	public void setCalleNumero(String calleNumero) {
		this.calleNumero = calleNumero;
	}
	@Override
	public String toString() {
		return (calleNumero!=null?calleNumero:"")+(colonia!=null?"\n"+colonia:"")+(ciudad!=null?"\n"+ciudad:"")+(estado!=null?" "+estado:"")+(pais!=null?" "+pais:"")+(cp!=null?"\nCP: " +cp:"");
	}
}
