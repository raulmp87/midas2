package mx.com.afirme.midas2.domain.folioReexpedible;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * Entidad que contendr\u00e1 la informaci\u00f3n de 
 * la tabla de folio reexpedible.
 * 
 * @since 09022016
 * 
 * @author AFIRME
 *
 */
@Entity
@Table(name="TOFOLIOREEXPEDIBLE", schema="MIDAS")
public class ToFolioReexpedible implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SQ_FOLIOREEXPEDIBLE")
	@SequenceGenerator(name="SQ_FOLIOREEXPEDIBLE", sequenceName="MIDAS.SQ_FOLIOREEXPEDIBLE", allocationSize=1)
	@Column(name = "ID_FOLIO_REEXPEDIBLE", unique = true, nullable = false, precision = 22, scale = 0)
	private long idFolioReexpedible;

	@Column(name="ID_TIPO_MONEDA", nullable=false)
	private long idTipoMoneda;
	
	@Column(name="ID_NEGOCIOPRODUCTO", nullable=false)
	private long negprod;
	
	@Column(name="ID_NEGOCIO", nullable=false)
	private long negocio;
	
	@Column(name="ID_NEG_TIPO_POLIZA",nullable=false)
	private long negTpoliza;
	
	@Column(name="ID_VIGENCIA", nullable=false)
	private long vigencia;
	
	@Column(name="ID_AGENTE_FACULTATIVO", nullable=false)
	private long agenteFacultativo;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_VALIDEZ_HASTA", nullable=false)
	private Date fechaValidesHasta;
	
	@Column(name="COMENTARIOS", nullable=true)
	private String comentarios;
	
	@Column(name="NUMERO_FOLIO", nullable = false)
	private long numeroFolio;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_TIPO_FOLIO", nullable=false)
	private TcTipoFolio tipoFolio;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION", nullable=false)
	private Date fechaCreacion;
	
	@Column(name="ACTIVO")
	private boolean activo;
	
	@Column(name="ID_AGENTEVINCULADO", nullable=true)
	private long agenteVinculado;
	
	public ToFolioReexpedible(){}
	
	public long getIdFolioReexpedible() {
		return idFolioReexpedible;
	}
	
	public void setIdFolioReexpedible(long idFolioReexpedible) {
		this.idFolioReexpedible = idFolioReexpedible;
	}
	
	public long getIdTipoMoneda() {
		return idTipoMoneda;
	}

	public void setIdTipoMoneda(long idTipoMoneda) {
		this.idTipoMoneda = idTipoMoneda;
	}

	public long getNegocio() {
		return negocio;
	}

	public void setNegocio(long negocio) {
		this.negocio = negocio;
	}

	public long getNegTpoliza() {
		return negTpoliza;
	}

	public void setNegTpoliza(long negTpoliza) {
		this.negTpoliza = negTpoliza;
	}

	public long getVigencia() {
		return vigencia;
	}

	public void setVigencia(long vigencia) {
		this.vigencia = vigencia;
	}

	public long getAgenteFacultativo() {
		return agenteFacultativo;
	}

	public void setAgenteFacultativo(long agenteFacultativo) {
		this.agenteFacultativo = agenteFacultativo;
	}

	public Date getFechaValidesHasta() {
		return fechaValidesHasta;
	}

	public void setFechaValidesHasta(Date fechaValidesHasta) {
		this.fechaValidesHasta = fechaValidesHasta;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public TcTipoFolio getTipoFolio() {
		return tipoFolio;
	}

	public void setTipoFolio(TcTipoFolio tipoFolio) {
		this.tipoFolio = tipoFolio;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public long getAgenteVinculado() {
		return agenteVinculado;
	}

	public void setAgenteVinculado(long agenteVinculado) {
		this.agenteVinculado = agenteVinculado;
	}

	public long getNegprod() {
		return negprod;
	}

	public void setNegprod(long negprod) {
		this.negprod = negprod;
	}

	public long getNumeroFolio() {
		return numeroFolio;
	}

	public void setNumeroFolio(long numeroFolio) {
		this.numeroFolio = numeroFolio;
	}

}
