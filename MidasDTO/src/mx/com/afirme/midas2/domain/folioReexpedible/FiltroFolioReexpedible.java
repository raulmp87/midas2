package mx.com.afirme.midas2.domain.folioReexpedible;

import java.util.Date;

import mx.com.afirme.midas.base.LogBaseDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.vigencia.VigenciaDTO;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;

public class FiltroFolioReexpedible extends LogBaseDTO  {
	
	
	private static final long serialVersionUID = 1L;
	
	private TcTipoFolio tipoFolio;
	private String folioInicio;
	private String folioFin;
	private Negocio negocio;
	private NegocioProducto producto;
	private VigenciaDTO vigencia;
	private MonedaDTO moneda;
	private Date fechaValidez;
	private NegocioTipoPoliza tipoPoliza;
	private String nombreAgente;
	private Long folioInicioL;
	private Long folioFinL;
	private String comentarios;
	
	private Integer totalCount;
	private Integer posStart;
	private Integer count;
	private String orderByAttribute;
	
	private String descripcionProducto;
	private String desctipcionTipoPoliza;
	private String descTipoMoneda;
	private String descVigencia;
	
	public FiltroFolioReexpedible() {
	}
	
	public TcTipoFolio getTipoFolio() {
		return tipoFolio;
	}
	
	public void setTipoFolio(TcTipoFolio tipoFolio) {
		this.tipoFolio = tipoFolio;
	}
	
	public String getFolioInicio() {
		return folioInicio;
	}
	
	public void setFolioInicio(String folioInicio) {
		this.folioInicio = folioInicio;
	}
	
	public String getFolioFin() {
		return folioFin;
	}
	
	public void setFolioFin(String folioFin) {
		this.folioFin = folioFin;
	}
	
	public Negocio getNegocio() {
		return negocio;
	}
	
	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}
	
	public NegocioProducto getProducto() {
		return producto;
	}
	
	public void setProducto(NegocioProducto producto) {
		this.producto = producto;
	}
	
	public VigenciaDTO getVigencia() {
		return vigencia;
	}
	
	public void setVigencia(VigenciaDTO vigencia) {
		this.vigencia = vigencia;
	}
	
	public MonedaDTO getMoneda() {
		return moneda;
	}
	
	public void setMoneda(MonedaDTO moneda) {
		this.moneda = moneda;
	}
	
	public Date getFechaValidez() {
		return fechaValidez;
	}
	
	public void setFechaValidez(Date fechaValidez) {
		this.fechaValidez = fechaValidez;
	}

	public NegocioTipoPoliza getTipoPoliza() {
		return tipoPoliza;
	}

	public void setTipoPoliza(NegocioTipoPoliza tipoPoliza) {
		this.tipoPoliza = tipoPoliza;
	}

	public String getDesctipcionTipoPoliza() {
		return desctipcionTipoPoliza;
	}

	public void setDesctipcionTipoPoliza(String desctipcionTipoPoliza) {
		this.desctipcionTipoPoliza = desctipcionTipoPoliza;
	}

	public String getDescTipoMoneda() {
		return descTipoMoneda;
	}

	public void setDescTipoMoneda(String descTipoMoneda) {
		this.descTipoMoneda = descTipoMoneda;
	}

	public String getDescVigencia() {
		return descVigencia;
	}

	public void setDescVigencia(String descVigencia) {
		this.descVigencia = descVigencia;
	}

	public String getDescripcionProducto() {
		return descripcionProducto;
	}

	public void setDescripcionProducto(String descripcionProducto) {
		this.descripcionProducto = descripcionProducto;
	}

	public Long getFolioInicioL() {
		return folioInicioL;
	}

	public void setFolioInicioL(Long folioInicioL) {
		this.folioInicioL = folioInicioL;
	}

	public Long getFolioFinL() {
		return folioFinL;
	}

	public void setFolioFinL(Long folioFinL) {
		this.folioFinL = folioFinL;
	}

	public String getNombreAgente() {
		return nombreAgente;
	}

	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getPosStart() {
		return posStart;
	}

	public void setPosStart(Integer posStart) {
		this.posStart = posStart;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getOrderByAttribute() {
		return orderByAttribute;
	}

	public void setOrderByAttribute(String orderByAttribute) {
		this.orderByAttribute = orderByAttribute;
	}
}
