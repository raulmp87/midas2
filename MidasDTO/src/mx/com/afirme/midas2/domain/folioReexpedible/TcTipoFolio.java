package mx.com.afirme.midas2.domain.folioReexpedible;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entidad que contendr\u00e1 la informaci\u00f3n de la tabla de catalogo tipo
 * folio.
 * 
 * @since 09022016
 * 
 * @author AFIRME
 *
 */
@Entity
@Table(name = "TCTIPOFOLIO", schema = "MIDAS")
public class TcTipoFolio implements Serializable {

	private static final long serialVersionUID = 1L;

	private long idTipoFolio;
	private String claveTipoFolio;
	private String descripcion;
	private boolean activo;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_TIPOFOLIO")
	@SequenceGenerator(name = "SQ_TIPOFOLIO", sequenceName = "MIDAS.SQ_TIPOFOLIO", allocationSize = 1)
	@Column(name = "ID_TIPO_FOLIO", unique = true, nullable = false, precision = 22, scale = 0)
	public long getIdTipoFolio() {
		return idTipoFolio;
	}

	public void setIdTipoFolio(long idTipoFolio) {
		this.idTipoFolio = idTipoFolio;
	}

	@Column(name = "CLAVE_TIPO_FOLIO")
	public String getClaveTipoFolio() {
		return claveTipoFolio;
	}

	public void setClaveTipoFolio(String claveTipoFolio) {
		this.claveTipoFolio = claveTipoFolio;
	}

	@Column(name = "DESCRIPCION_TIPO_FOLIO")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "ACTIVO")
	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	/**
	 * Constructor generado por default.
	 */
	public TcTipoFolio() {
	}

	/**
	 * M\u00e9todo constructor de entidad sólo con el identificador del elemento
	 * dentro de base.
	 * 
	 * @param idTipoFolio
	 *            identificador del elemento del catalogo.
	 */
	public TcTipoFolio(long idTipoFolio) {
		this.idTipoFolio = idTipoFolio;
	}
}
