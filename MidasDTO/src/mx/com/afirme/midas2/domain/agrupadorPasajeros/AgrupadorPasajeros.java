package mx.com.afirme.midas2.domain.agrupadorPasajeros;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name = "TOAGRUPADORPASAJEROS", schema = "MIDAS")
public class AgrupadorPasajeros  implements Serializable, Entidad{
	private static final long serialVersionUID = 1L;
	private BigDecimal id;
	private String descripcion;
	private Double valorMin;
	private Double valorMax;

	// Constructors

	/** default constructor */
	public AgrupadorPasajeros() {
	}

	// Property accessors
	@Id
	@SequenceGenerator(name="AGR_PAS_GEN", sequenceName="IDTOAGRUPPASAJEROS_SEQ", allocationSize=1, schema="MIDAS")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="AGR_PAS_GEN")
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	@Column(name = "DESCRIPCION")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "VALORMIN", nullable = false, precision = 16)
	public Double getValorMin() {
		return valorMin;
	}

	public void setValorMin(Double valorMin) {
		this.valorMin = valorMin;
	}

	@Column(name = "VALORMAX", nullable = false, precision = 16)
	public Double getValorMax() {
		return valorMax;
	}

	public void setValorMax(Double valorMax) {
		this.valorMax = valorMax;
	}
}
