package mx.com.afirme.midas2.domain.reportes.reporteSesas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TOGENERACIONSESASARCHIVOS", schema = "MIDAS")
public class GeneracionSesasArchivosDTO  implements Serializable {
	

	private static final long serialVersionUID = 1L;
	
	
	private ReporteSesasArchivosId id;
	private Long idGeneracion;
	private Long idArchivo;
	private String cveArchivo;
	private String nomArchivo;
	private String nomArchivoFrtmx;
	private Date fechaCreacion;
	private String codigoUsuarioCreacion;
	private String estatus;
	private String resultado;
	private String tipoReporte;
	
	
	   /** default constructor */
    public GeneracionSesasArchivosDTO() {
    }

	
	
	@EmbeddedId
	@AttributeOverrides( {
        @AttributeOverride(name="idGeneracion", column=@Column(name="IDGENERACION", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idArchivo", column=@Column(name="IDARCHIVO", nullable=false, precision=22, scale=0) ) } )
	
	public ReporteSesasArchivosId getId() {
		return id;
	}
	public void setId(ReporteSesasArchivosId id) {
		this.id = id;
	}
	@Column(name="IDGENERACION")
	public Long getIdGeneracion() {
		return idGeneracion;
	}
	public void setIdGeneracion(Long idGeneracion) {
		this.idGeneracion = idGeneracion;
	}
	@Column(name="IDARCHIVO")
	public Long getIdArchivo() {
		return idArchivo;
	}
	public void setIdArchivo(Long idArchivo) {
		this.idArchivo = idArchivo;
	}
	@Column(name="CVEARCHIVO")
	public String getCveArchivo() {
		return cveArchivo;
	}
	public void setCveArchivo(String cveArchivo) {
		this.cveArchivo = cveArchivo;
	}
	@Column(name="NOMARCHIVO")
	public String getNomArchivo() {
		return nomArchivo;
	}
	public void setNomArchivo(String nomArchivo) {
		this.nomArchivo = nomArchivo;
	}
	@Column(name="NOMARCHIVOFRTMX")
	public String getNomArchivoFrtmx() {
		return nomArchivoFrtmx;
	}
	public void setNomArchivoFrtmx(String nomArchivoFrtmx) {
		this.nomArchivoFrtmx = nomArchivoFrtmx;
	}
	@Temporal(TemporalType.DATE)
	@Column(name="FECHACREACION")
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	@Column(name="CODIGOUSUARIOCREACION")
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}
	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
	@Column(name="ESTATUS")
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	@Column(name="RESULTADO")
	public String getResultado() {
		return resultado;
	}
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}


	@Column(name="TIPO_REPORTE")
	public String getTipoReporte() {
		return tipoReporte;
	}
	
	public void setTipoReporte(String tipoReporte) {
		this.tipoReporte = tipoReporte;
	}
	


}
