package mx.com.afirme.midas2.domain.reportes;


public class ReporteEmisionView {

	private String fechaInicial;
	private String fechaFinal;
	private String claveAgente;

	public String getFechaInicial() {
		return this.fechaInicial;
	}

	public void setFechaInicial(String fechaInicial){
		this.fechaInicial = fechaInicial;
	}

	public String getFechaFinal() {
		return this.fechaFinal;
	}

	public void setFechaFinal(String fechaFinal){
		this.fechaFinal = fechaFinal;
	}

	public String getClaveAgente() {
		return claveAgente;
	}

	public void setClaveAgente(String claveAgente) {
		this.claveAgente = claveAgente;
	}
	
}