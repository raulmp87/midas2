package mx.com.afirme.midas2.domain.reportes.reporteSesas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TOGENERACIONSESAS", schema = "MIDAS")
public class GeneracionSesasDTO  implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTOGENERACIONSESAS_ID_GENERATOR")	
	@SequenceGenerator(name="IDTOGENERACIONSESAS_ID_GENERATOR", sequenceName="MIDAS.IDTOGENERACIONSESAS_SEQ", allocationSize=100)	
	@Column(name="ID")
	private Long id;
	@Column(name="LOGTXT")
	private String logTxt;
	@Temporal(TemporalType.DATE)
	@Column(name="FECHACREACION")
	private Date fechaCreacion;
	@Column(name="CODIGOUSUARIOCREACION")
	private String codigoUsuarioCreacion;
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAINICIAL")
	private Date fechaInicial;
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAFINAL")
	private Date fechaFinal;
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAEJECUCION")
	private Date fechaEjecucion;
	@Column(name="ESTATUS")
	private String estatus;
	@Column(name="RESULTADO")
	private String resultado;
	@Column(name="CLAVENEGOCIO")
	private String claveNeg;
	
	
	public String getClaveNeg() {
		return claveNeg;
	}
	public void setClaveNeg(String claveNeg) {
		this.claveNeg = claveNeg;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLogTxt() {
		return logTxt;
	}
	public void setLogTxt(String logTxt) {
		this.logTxt = logTxt;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}
	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
	public Date getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public Date getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}	
	public Date getFechaEjecucion() {
		return fechaEjecucion;
	}
	public void setFechaEjecucion(Date fechaEjecucion) {
		this.fechaEjecucion = fechaEjecucion;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getResultado() {
		return resultado;
	}
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}	
}
