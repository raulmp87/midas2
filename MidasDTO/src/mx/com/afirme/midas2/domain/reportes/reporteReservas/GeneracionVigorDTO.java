package mx.com.afirme.midas2.domain.reportes.reporteReservas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "LOG_ARCHIVOS_RESERVAS", schema = "SEYCOS")
public class GeneracionVigorDTO  implements Serializable {

	private static final long serialVersionUID = 1L;
	

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTOGENERACIONVIGOR_ID_GENERATOR")	
	@SequenceGenerator(name="IDTOGENERACIONVIGOR_ID_GENERATOR", sequenceName="SEYCOS.LOG_ARCH_RESER_SEQ", allocationSize=100)	
	@Column(name="ID_EJECUCION")
	private Long id;
	@Column(name="TIPO_ARCHIVO")
	private BigDecimal tipoArchivo;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_INI")
	private Date fechaIni;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_FIN")
	private Date fechaFin;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_EJEC_INI")
	private Date fechaInicialE;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_EJEC_FIN")
	private Date fechaFinalE;
	
	@Column(name="ESTATUS")
	private String estatus;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public BigDecimal getTipoArchivo() {
		return tipoArchivo;
	}
	public void setTipoArchivo(BigDecimal tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}
	public Date getFechaIni() {
		return fechaIni;
	}
	public void setFechaIni(Date fechaIni) {
		this.fechaIni = fechaIni;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	public Date getFechaInicialE() {
		return fechaInicialE;
	}
	public void setFechaInicialE(Date fechaInicialE) {
		this.fechaInicialE = fechaInicialE;
	}
	public Date getFechaFinalE() {
		return fechaFinalE;
	}
	public void setFechaFinalE(Date fechaFinalE) {
		this.fechaFinalE = fechaFinalE;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	
}
