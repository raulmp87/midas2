package mx.com.afirme.midas2.domain.reportes.reporteSesas;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ReporteSesasArchivosId  implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	private Integer idGeneracion;
    private Integer idArchivo;

    public ReporteSesasArchivosId() {
    }

    
    public ReporteSesasArchivosId(Integer idGeneracion, Integer idArchivo) {
        this.idGeneracion = idGeneracion;
        this.idArchivo = idArchivo;
    }

    @Column(name="idGeneracion", nullable=false, precision=22, scale=0)

    public Integer getidGeneracion() {
        return this.idGeneracion;
    }
    
    public void setidGeneracion(Integer idGeneracion) {
        this.idGeneracion = idGeneracion;
    }

    @Column(name="idArchivo", nullable=false, precision=22, scale=0)

    public Integer getidArchivo() {
        return this.idArchivo;
    }
    
    public void setidArchivo(Integer idArchivo) {
        this.idArchivo = idArchivo;
    }
}
