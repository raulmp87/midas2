package mx.com.afirme.midas2.domain.reportes.reporteSesas;

import java.math.BigDecimal;

public class ReporteSesasMatrizDTO {


		private static final long serialVersionUID = 1L;
		
		private String id_tipo_arch;
		private String nombre_campo;
		private Integer id_validacion;
		private String descripcion;
		private BigDecimal midas_ind_err;
		private BigDecimal midas_ind_war;
		private BigDecimal midas_flo_err;
		private BigDecimal midas_flo_war;
		private BigDecimal midas_tot_err;
		private BigDecimal midas_tot_war;
		private BigDecimal seycos_ind_err;
		private BigDecimal seycos_ind_war;
		private BigDecimal seycos_flo_err;
		private BigDecimal seycos_flo_war;
		private BigDecimal seycos_tot_err;
		private BigDecimal seycos_tot_war;
		public String getId_tipo_arch() {
			return id_tipo_arch;
		}
		public String getNombre_campo() {
			return nombre_campo;
		}
		public Integer getId_validacion() {
			return id_validacion;
		}
		public String getDescripcion() {
			return descripcion;
		}
		public BigDecimal getMidas_ind_err() {
			return midas_ind_err;
		}
		public BigDecimal getMidas_ind_war() {
			return midas_ind_war;
		}
		public BigDecimal getMidas_flo_err() {
			return midas_flo_err;
		}
		public BigDecimal getMidas_flo_war() {
			return midas_flo_war;
		}
		public BigDecimal getMidas_tot_err() {
			return midas_tot_err;
		}
		public BigDecimal getMidas_tot_war() {
			return midas_tot_war;
		}
		public BigDecimal getSeycos_ind_err() {
			return seycos_ind_err;
		}
		public BigDecimal getSeycos_ind_war() {
			return seycos_ind_war;
		}
		public BigDecimal getSeycos_flo_err() {
			return seycos_flo_err;
		}
		public BigDecimal getSeycos_flo_war() {
			return seycos_flo_war;
		}
		public BigDecimal getSeycos_tot_err() {
			return seycos_tot_err;
		}
		public BigDecimal getSeycos_tot_war() {
			return seycos_tot_war;
		}
		public void setId_tipo_arch(String id_tipo_arch) {
			this.id_tipo_arch = id_tipo_arch;
		}
		public void setNombre_campo(String nombre_campo) {
			this.nombre_campo = nombre_campo;
		}
		public void setId_validacion(Integer id_validacion) {
			this.id_validacion = id_validacion;
		}
		public void setDescripcion(String descripcion) {
			this.descripcion = descripcion;
		}
		public void setMidas_ind_err(BigDecimal midas_ind_err) {
			this.midas_ind_err = midas_ind_err;
		}
		public void setMidas_ind_war(BigDecimal midas_ind_war) {
			this.midas_ind_war = midas_ind_war;
		}
		public void setMidas_flo_err(BigDecimal midas_flo_err) {
			this.midas_flo_err = midas_flo_err;
		}
		public void setMidas_flo_war(BigDecimal midas_flo_war) {
			this.midas_flo_war = midas_flo_war;
		}
		public void setMidas_tot_err(BigDecimal midas_tot_err) {
			this.midas_tot_err = midas_tot_err;
		}
		public void setMidas_tot_war(BigDecimal midas_tot_war) {
			this.midas_tot_war = midas_tot_war;
		}
		public void setSeycos_ind_err(BigDecimal seycos_ind_err) {
			this.seycos_ind_err = seycos_ind_err;
		}
		public void setSeycos_ind_war(BigDecimal seycos_ind_war) {
			this.seycos_ind_war = seycos_ind_war;
		}
		public void setSeycos_flo_err(BigDecimal seycos_flo_err) {
			this.seycos_flo_err = seycos_flo_err;
		}
		public void setSeycos_flo_war(BigDecimal seycos_flo_war) {
			this.seycos_flo_war = seycos_flo_war;
		}
		public void setSeycos_tot_err(BigDecimal seycos_tot_err) {
			this.seycos_tot_err = seycos_tot_err;
		}
		public void setSeycos_tot_war(BigDecimal seycos_tot_war) {
			this.seycos_tot_war = seycos_tot_war;
		}
		
		


		
	}


