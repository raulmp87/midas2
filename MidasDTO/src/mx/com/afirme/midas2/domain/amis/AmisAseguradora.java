package mx.com.afirme.midas2.domain.amis;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name = "SAP_AMIS_COMP_ASEG", schema = "MIDAS")

public class AmisAseguradora implements Entidad {
	
	private static final long serialVersionUID = 1L;
		
	@Id
	@Column(name="ID")
	private Long id;
	
	@Column(name = "TCPRESTADORSERVICIO_ID")
	private Long ciaId;
	
	@Column(name = "SAPAMISCATASEGURADORAS_ID")
	private String amisId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCiaId() {
		return ciaId;
	}

	public void setCiaId(Long ciaId) {
		this.ciaId = ciaId;
	}

	public String getAmisId() {
		return amisId;
	}

	public void setAmisId(String amisId) {
		this.amisId = amisId;
	}

	@Override
	public <K> K getKey() {
		return null;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}

}
