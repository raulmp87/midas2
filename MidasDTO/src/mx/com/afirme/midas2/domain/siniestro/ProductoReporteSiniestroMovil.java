package mx.com.afirme.midas2.domain.siniestro;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Column;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="TCPRODREPORTESINMOV", schema="MIDAS")
public class ProductoReporteSiniestroMovil implements Serializable, Entidad {

	private static final long serialVersionUID = 6033360641522944722L;

	private Long id;
	private String nombre;
	private String clave;

	public static final Long ID_AUTOS = 1l;
	public static final Long ID_CASA = 2l;
	
	
	@Id
	@SequenceGenerator(name = "IDTCPRODREPORTESINMOV_SEQ", allocationSize = 1, sequenceName = "MIDAS.IDTCPRODREPORTESINMOV_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCPRODREPORTESINMOV_SEQ")
	@Column(name = "ID")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "NOMBRE")
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Column(name = "CLAVE")
	public String getClave() {
		return clave;
	}
	
	public void setClave(String clave) {
		this.clave = clave;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
		
}
