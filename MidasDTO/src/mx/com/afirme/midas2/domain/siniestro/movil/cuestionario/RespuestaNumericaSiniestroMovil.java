package mx.com.afirme.midas2.domain.siniestro.movil.cuestionario;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.siniestro.ReporteSiniestroMovil;

@Entity(name = "RespuestaNumericaSiniestroMovil")
@Table(name = "TORESPUESTANUMSINMOVIL", schema = "MIDAS")
public class RespuestaNumericaSiniestroMovil extends MidasAbstracto implements Entidad {
	
	private static final long serialVersionUID = 3955333765752454657L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TORESPUESTANUMSINMOVIL_ID_GENERATOR")
	@SequenceGenerator(name = "TORESPUESTANUMSINMOVIL_ID_GENERATOR", schema = "MIDAS", sequenceName = "TORESPUESTANUMSINMOVIL_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "RESPUESTA")
	private Long respuesta;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "REPORTESINIESTROMOVIL_ID")
	private ReporteSiniestroMovil reporte;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PREGUNTASINIESTROMOVIL_ID")
	private PreguntaSiniestroMovil pregunta;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(Long respuesta) {
		this.respuesta = respuesta;
	}
	
	public ReporteSiniestroMovil getReporte() {
		return reporte;
	}

	public void setReporte(ReporteSiniestroMovil reporte) {
		this.reporte = reporte;
	}

	public PreguntaSiniestroMovil getPregunta() {
		return pregunta;
	}

	public void setPregunta(PreguntaSiniestroMovil pregunta) {
		this.pregunta = pregunta;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Long getBusinessKey() {
		return id;
	}
	
}
