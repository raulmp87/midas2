package mx.com.afirme.midas2.domain.siniestro;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="TCCABINA", schema="MIDAS")
public class Cabina implements Serializable, Entidad {

	private static final long serialVersionUID = -8779134160905978815L;
	
	
	public static final String NOMBRE_MONTERREY = "MONTERREY";
	public static final String NOMBRE_SPV = "SPV";
	public static final String NOMBRE_MULTIASISTENCIA = "MULTIASISTENCIA";

	
	private Long id;
	private String nombre;
	
	@Id
	@SequenceGenerator(name = "IDTCCABINA_SEQ", allocationSize = 1, sequenceName = "MIDAS.IDTCCABINA_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCCABINA_SEQ")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
		
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
