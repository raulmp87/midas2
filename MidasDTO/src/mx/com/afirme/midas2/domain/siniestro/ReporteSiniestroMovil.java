package mx.com.afirme.midas2.domain.siniestro;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.Coordenadas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;

@Entity
@Table(name="TOREPORTESINIESTROMOVIL", schema="MIDAS")
@Access(AccessType.PROPERTY)
public class ReporteSiniestroMovil implements Serializable, Entidad {

	public enum Estatus {
		
		SIN_ASIGNAR(1l, "SIN ASIGNAR"), ASIGNADA(2l, "ASIGNADA"), TERMINADA(3l, "TERMINADA");
				
		private long id;
		private String nombre;
				
		private Estatus(long id, String nombre) {
			this.id = id;
			this.nombre = nombre;
		}
		
		public long getId() {
			return id;
		}
		
		public String getNombre() {
			return nombre;
		}
		
		public static Estatus parse(long id) {
            Estatus e = null; // Default
            for (Estatus item : Estatus.values()) {
                if (item.getId() ==id) {
                    e = item;
                    break;
                }
            }
            return e;
        }
	}
	
	private static final long serialVersionUID = 3614744854666726421L;

	private Long id;

	/**
	 * No se relaciona la entidad con IncisoPolizaDTO debido a que las altas o bajas de inciso en endosos no se actualizan ahí y habría casos en donde no los podriamos
	 * encontrar. Lo que si podemos establecer es que al menos todas las polizas se encontraran en PolizaDTO como inicialmente nacio.
	 */
	private PolizaDTO polizaDTO;
	private BigDecimal numeroInciso;
	private String numeroCelular;
	private String nombrePersonaReporta;
	private String nombreUsuarioReporta;
	private Coordenadas coordenadas;
	private TipoSiniestroMovil tipoSiniestroMovil;
	private Cabina cabina;
	private Date fechaCreacion = new Date();
	@Access(AccessType.FIELD)
	@Column(name = "ESTATUS_ID")
	private Long estatusId = Estatus.SIN_ASIGNAR.getId();
	private Oficina oficina;

	/**
	 * El usuario al que se le asigno el reporte de siniestro.
	 */
	private String nombreUsuarioAsignado;
	/**
	 * Indica si el reporte de siniestro es una falsa alarma.
	 */
	private Boolean falsaAlarma;
	private Boolean arriboConfirmado;
	private Boolean primeroEnArribar;
	private String numeroReporteSiniestro;
	private Long version;
	private ReporteCabina reporteCabina;

	@Id
	@SequenceGenerator(name = "IDTOREPORTESINIESTROMOVIL_SEQ", allocationSize = 1, sequenceName = "MIDAS.IDTOREPORTESINIESTROMOVIL_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOREPORTESINIESTROMOVIL_SEQ")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name="IDTOPOLIZA")
	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}
	
	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}
	
	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}
	
	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	
	@NotNull
	public String getNumeroCelular() {
		return numeroCelular;
	}
	
	public void setNumeroCelular(String numeroCelular) {
		this.numeroCelular = numeroCelular;
	}
	
	@NotNull
	public String getNombrePersonaReporta() {
		return nombrePersonaReporta;
	}
	
	public void setNombrePersonaReporta(String nombrePersonaReporta) {
		this.nombrePersonaReporta = nombrePersonaReporta;
	}
	
	@Embedded
	public Coordenadas getCoordenadas() {
		return coordenadas;
	}
	
	public void setCoordenadas(Coordenadas coordenadas) {
		this.coordenadas = coordenadas;
	}
	
	@ManyToOne
	@NotNull
	public TipoSiniestroMovil getTipoSiniestroMovil() {
		return tipoSiniestroMovil;
	}
	
	public void setTipoSiniestroMovil(TipoSiniestroMovil tipoSiniestroMovil) {
		this.tipoSiniestroMovil = tipoSiniestroMovil;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@NotNull
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	@ManyToOne
	@JoinColumn(name="CABINA_ID")
	public Cabina getCabina() {
		return cabina;
	}
	
	public void setCabina(Cabina cabina) {
		this.cabina = cabina;
	}
		
	@Transient
	public Estatus getEstatus() {
		if (estatusId == null) {
			return null;
		}
        return Estatus.parse(estatusId);
    }

    public void setEstatus(Estatus estatus) {
    	if (estatus != null) {
    		this.estatusId = estatus.getId();
    	}
    }
    
    public String getNombreUsuarioAsignado() {
		return nombreUsuarioAsignado;
	}
    
    public void setNombreUsuarioAsignado(String nombreUsuarioAsignado) {
		this.nombreUsuarioAsignado = nombreUsuarioAsignado;
	}
    
    @Column(name = "nombreUsuarioReporta")
    public String getNombreUsuarioReporta() {
		return nombreUsuarioReporta;
	}
    
    public void setNombreUsuarioReporta(String nombreUsuarioReporta) {
		this.nombreUsuarioReporta = nombreUsuarioReporta;
	}
    
    public Boolean getFalsaAlarma() {
		return falsaAlarma;
	}
    
    public void setFalsaAlarma(Boolean falsaAlarma) {
		this.falsaAlarma = falsaAlarma;
	}
    
    @Column(name = "arriboConfirmado")
    public Boolean getArriboConfirmado() {
		return arriboConfirmado;
	}

	public void setArriboConfirmado(Boolean arriboConfirmado) {
		this.arriboConfirmado = arriboConfirmado;
	}

	@Column(name = "primeroEnArribar")
	public Boolean getPrimeroEnArribar() {
		return primeroEnArribar;
	}

	public void setPrimeroEnArribar(Boolean primeroEnArribar) {
		this.primeroEnArribar = primeroEnArribar;
	}

	public String getNumeroReporteSiniestro() {
		return numeroReporteSiniestro;
	}
    
    public void setNumeroReporteSiniestro(String numeroReporteSiniestro) {
		this.numeroReporteSiniestro = numeroReporteSiniestro;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Long getKey() {
		return getId();
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return getId();
	}
	
	@Version
	public Long getVersion() {
		return version;
	}
	
	public void setVersion(Long version) {
		this.version = version;
	}
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "REPORTECABINA_ID", nullable = false, referencedColumnName = "ID")
	public ReporteCabina getReporteCabina() {
		return reporteCabina;
	}

	public void setReporteCabina(ReporteCabina reporteCabina) {
		this.reporteCabina = reporteCabina;
	}
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="OFICINA_ID", referencedColumnName="ID")
	public Oficina getOficina() {
		return oficina;
	}
	
	public void setOficina(Oficina oficina) {
		this.oficina = oficina;
	}

}
