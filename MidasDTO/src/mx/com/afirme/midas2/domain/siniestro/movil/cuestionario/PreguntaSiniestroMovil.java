package mx.com.afirme.midas2.domain.siniestro.movil.cuestionario;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name = "PreguntaSiniestroMovil")
@Table(name = "TOPREGUNTASINMOVIL", schema = "MIDAS")
public class PreguntaSiniestroMovil implements Entidad {
	
	private static final long serialVersionUID = 352760129510292366L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TOPREGUNTASINMOVIL_ID_GENERATOR")
	@SequenceGenerator(name = "TOPREGUNTASINMOVIL_ID_GENERATOR", schema = "MIDAS", sequenceName = "TOPREGUNTASINMOVIL_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "TEXTO")
	private String texto;
	
	@Column(name = "ACTIVO")
	private Boolean activo;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Long getBusinessKey() {
		return id;
	}

}
