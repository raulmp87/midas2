package mx.com.afirme.midas2.domain.siniestro;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="TCTIPOSINIESTROMOVIL", schema = "MIDAS")
public class TipoSiniestroMovil implements Serializable, Entidad {

	private static final long serialVersionUID = -8779134160905978815L;

	private Long id;
	private ProductoReporteSiniestroMovil productoReporteSiniestroMovil;
	private String nombre;
	private Long nivel; 
	private Long padre; 
	private String imagen;
	private boolean tieneHijos;
	
	@Id
	@SequenceGenerator(name = "IDTCTIPOSINIESTROMOVIL_SEQ", allocationSize = 1, sequenceName = "MIDAS.IDTCTIPOSINIESTROMOVIL_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTCTIPOSINIESTROMOVIL_SEQ")
	@Column(name = "ID")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne
	@JoinColumn(name="PRODREPORTESINMOV_ID")
	public ProductoReporteSiniestroMovil getProductoReporteSiniestroMovil() {
		return productoReporteSiniestroMovil;
	}
	
	public void setProductoReporteSiniestroMovil(
			ProductoReporteSiniestroMovil productoReporteSiniestroMovil) {
		this.productoReporteSiniestroMovil = productoReporteSiniestroMovil;
	}
	
	@Column(name = "NOMBRE")
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Column(name = "NIVEL")
	public Long getNivel() {
		return nivel;
	}
	
	public void setNivel(Long nivel) {
		this.nivel = nivel;
	}
	
	@Column(name = "PADRE")
	public Long getPadre() {
		return padre;
	}
	
	public void setPadre(Long padre) {
		this.padre = padre;
	}
	
	@Column(name = "IMAGEN")
	public String getImagen() {
		return imagen;
	}
	
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @return true si tieneHijos
	 */
	@Transient
	public boolean isTieneHijos() {
		return tieneHijos;
	}

	/**
	 * @param se asigna true o false dependiendo si tiene hijos o no
	 */
	public void setTieneHijos(boolean tieneHijos) {
		this.tieneHijos = tieneHijos;
	}
	
}
