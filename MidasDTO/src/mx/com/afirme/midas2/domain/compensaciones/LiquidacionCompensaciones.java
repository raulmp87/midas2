package mx.com.afirme.midas2.domain.compensaciones;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.solicitudcheque.SolicitudChequeSiniestro;


@Entity
@Table(name="CA_LIQUIDACION",schema="MIDAS")
public class LiquidacionCompensaciones  implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	
	// Fields    
	private Long id;
    private SolicitudChequeSiniestro solicitudChequeSiniestro;
    private String estatus;
    private Timestamp fechaTramite;
    private Timestamp fechaPorAut;
    private Timestamp fechaLiberadaCon;
    private Timestamp fechaLiberadaSin;
    private Timestamp fechaTramiteRec;
    private Timestamp fechaAplicada;
    private Timestamp fechaEliminada;
    private Timestamp fechaCancelada;
    private Long idTcmoneda;
    private String tipoOperacion;
    private String tipoLiquidacion;
    private String banco;
    private String cuenta;
    private String noChequeRef;
    private String solicitadoPor;
    private String autorizadoPor;
    private String rechazadoPor;
    private Timestamp fechaCheque;
    private Double importePrima;
    private Double importeBs;
    private Double importeDerpol;
    private Double importeCumMeta;
    private Double importeUtilidad;
    private Double subtotal;
    private Double importeTotal;
    private Double iva;
    private Double ivaRetenido;
    private Double isr;
    private String comentarios;
    private Timestamp fechaCreacion;
    private String codigoUsuarioCreacion;
    private Timestamp fechaModificacion;
    private String codigoUsuarioModificacion;
    private String origenLiquidacion;
    private Long identificadorBenId;
    private String cveTipoPersona;
    private String cveTipoEntidad;
    private String nombreBeneficiario;
    private String claveNombre;
    private String bancoBeneficiario;
    private String rfcBeneficiario;
    private String correo;
    private String clabeBeneficiario;
    private String telefonoLada;
    private String telefonoNumero;
    private Timestamp fechaTramiteMizar;
    private Long idRemesa;
    private Double netoPorPagar;
    private Short estatusFactura;
    private Short estatusOrdenPago;
    private Date fecha;
    private Timestamp fechaFactura;
    private String nombreArchivoXml;
    
    // Constructors

    /** default constructor */
    public LiquidacionCompensaciones() {
    }

	/** minimal constructor */
    public LiquidacionCompensaciones(Long id, Timestamp fechaCreacion, String codigoUsuarioCreacion) {
        this.id = id;
        this.fechaCreacion = fechaCreacion;
        this.codigoUsuarioCreacion = codigoUsuarioCreacion;
    }
    
    /** full constructor */
    public LiquidacionCompensaciones(Long id, SolicitudChequeSiniestro solicitudChequeSiniestro, 
    		String estatus, Timestamp fechaTramite, Timestamp fechaPorAut, Timestamp fechaLiberadaCon, 
    		Timestamp fechaLiberadaSin, Timestamp fechaTramiteRec, Timestamp fechaAplicada, Timestamp fechaEliminada, 
    		Timestamp fechaCancelada, Long idTcmoneda, String tipoOperacion, String tipoLiquidacion, 
    		String banco, String cuenta, String noChequeRef, String solicitadoPor, String autorizadoPor, 
    		String rechazadoPor, Timestamp fechaCheque, Double importePrima, Double importeBs, 
    		Double importeDerpol, Double importeCumMeta, Double importeUtilidad, Double subtotal, 
    		Double importeTotal, Double iva, Double ivaRetenido, Double isr, String comentarios, 
    		Timestamp fechaCreacion, String codigoUsuarioCreacion, Timestamp fechaModificacion, 
    		String codigoUsuarioModificacion, String origenLiquidacion, Double identificadorBenId, 
    		String cveTipoPersona, String cveTipoEntidad, String nombreBeneficiario, String claveNombre, 
    		String bancoBeneficiario, String clabeBeneficiario, String rfcBeneficiario, String correo, 
    		String telefonoLada, String telefonoNumero, Timestamp fechaTramiteMizar, Long idRemesa, 
    		Double netoPorPagar) {
        this.id = id;
        this.solicitudChequeSiniestro = solicitudChequeSiniestro;
        this.estatus = estatus;
        this.fechaTramite = fechaTramite;
        this.fechaPorAut = fechaPorAut;
        this.fechaLiberadaCon = fechaLiberadaCon;
        this.fechaLiberadaSin = fechaLiberadaSin;
        this.fechaTramiteRec = fechaTramiteRec;
        this.fechaAplicada = fechaAplicada;
        this.fechaEliminada = fechaEliminada;
        this.fechaCancelada = fechaCancelada;
        this.idTcmoneda = idTcmoneda;
        this.tipoOperacion = tipoOperacion;
        this.tipoLiquidacion = tipoLiquidacion;
        this.banco = banco;
        this.cuenta = cuenta;
        this.noChequeRef = noChequeRef;
        this.solicitadoPor = solicitadoPor;
        this.autorizadoPor = autorizadoPor;
        this.rechazadoPor = rechazadoPor;
        this.fechaCheque = fechaCheque;
        this.importePrima = importePrima;
        this.importeBs = importeBs;
        this.importeDerpol = importeDerpol;
        this.importeCumMeta = importeCumMeta;
        this.importeUtilidad = importeUtilidad;
        this.subtotal = subtotal;
        this.importeTotal = importeTotal;
        this.iva = iva;
        this.ivaRetenido = ivaRetenido;
        this.isr = isr;
        this.comentarios = comentarios;
        this.fechaCreacion = fechaCreacion;
        this.codigoUsuarioCreacion = codigoUsuarioCreacion;
        this.fechaModificacion = fechaModificacion;
        this.codigoUsuarioModificacion = codigoUsuarioModificacion;
        this.origenLiquidacion = origenLiquidacion;
        this.nombreBeneficiario = nombreBeneficiario;
        this.bancoBeneficiario = bancoBeneficiario;
        this.clabeBeneficiario = clabeBeneficiario;
        this.rfcBeneficiario = rfcBeneficiario;
        this.correo = correo;
        this.telefonoLada = telefonoLada;
        this.telefonoNumero = telefonoNumero;
        this.fechaTramiteMizar = fechaTramiteMizar;
        this.idRemesa = idRemesa;
        this.netoPorPagar = netoPorPagar;
    }

   
    // Property accessors
    @Id 
    
    @Column(name="ID", unique=true, nullable=false, precision=0)

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }    
    
    @Column(name="ESTATUS", length=29)
    public String getEstatus() {
        return this.estatus;
    }
    
    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }
    
    @Column(name="FECHA_TRAMITE", length=7)
    public Timestamp getFechaTramite() {
        return this.fechaTramite;
    }
    
    public void setFechaTramite(Timestamp fechaTramite) {
        this.fechaTramite = fechaTramite;
    }
    
    @Column(name="FECHA_POR_AUT", length=7)
    public Timestamp getFechaPorAut() {
        return this.fechaPorAut;
    }
    
    public void setFechaPorAut(Timestamp fechaPorAut) {
        this.fechaPorAut = fechaPorAut;
    }
    
    @Column(name="FECHA_LIBERADA_CON", length=7)
    public Timestamp getFechaLiberadaCon() {
        return this.fechaLiberadaCon;
    }
    
    public void setFechaLiberadaCon(Timestamp fechaLiberadaCon) {
        this.fechaLiberadaCon = fechaLiberadaCon;
    }
    
    @Column(name="FECHA_LIBERADA_SIN", length=7)
    public Timestamp getFechaLiberadaSin() {
        return this.fechaLiberadaSin;
    }
    
    public void setFechaLiberadaSin(Timestamp fechaLiberadaSin) {
        this.fechaLiberadaSin = fechaLiberadaSin;
    }
    
    @Column(name="FECHA_TRAMITE_REC", length=7)
    public Timestamp getFechaTramiteRec() {
        return this.fechaTramiteRec;
    }
    
    public void setFechaTramiteRec(Timestamp fechaTramiteRec) {
        this.fechaTramiteRec = fechaTramiteRec;
    }
    
    @Column(name="FECHA_APLICADA", length=7)
    public Timestamp getFechaAplicada() {
        return this.fechaAplicada;
    }
    
    public void setFechaAplicada(Timestamp fechaAplicada) {
        this.fechaAplicada = fechaAplicada;
    }
    
    @Column(name="FECHA_ELIMINADA", length=7)
    public Timestamp getFechaEliminada() {
        return this.fechaEliminada;
    }
    
    public void setFechaEliminada(Timestamp fechaEliminada) {
        this.fechaEliminada = fechaEliminada;
    }
    
    @Column(name="FECHA_CANCELADA", length=7)
    public Timestamp getFechaCancelada() {
        return this.fechaCancelada;
    }
    
    public void setFechaCancelada(Timestamp fechaCancelada) {
        this.fechaCancelada = fechaCancelada;
    }
    
    @Column(name="ID_TCMONEDA", precision=0)
    public Long getIdTcmoneda() {
        return this.idTcmoneda;
    }
    
    public void setIdTcmoneda(Long idTcmoneda) {
        this.idTcmoneda = idTcmoneda;
    }
    
    @Column(name="TIPO_OPERACION", length=30)
    public String getTipoOperacion() {
        return this.tipoOperacion;
    }
    
    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }
    
    @Column(name="TIPO_LIQUIDACION", length=30)
    public String getTipoLiquidacion() {
        return this.tipoLiquidacion;
    }
    
    public void setTipoLiquidacion(String tipoLiquidacion) {
        this.tipoLiquidacion = tipoLiquidacion;
    }
    
    @Column(name="BANCO", length=200)
    public String getBanco() {
        return this.banco;
    }
    
    public void setBanco(String banco) {
        this.banco = banco;
    }
    
    @Column(name="CUENTA", length=100)
    public String getCuenta() {
        return this.cuenta;
    }
    
    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }
    
    @Column(name="NO_CHEQUE_REF", length=100)
    public String getNoChequeRef() {
        return this.noChequeRef;
    }
    
    public void setNoChequeRef(String noChequeRef) {
        this.noChequeRef = noChequeRef;
    }
    
    @Column(name="SOLICITADO_POR", length=100)
    public String getSolicitadoPor() {
        return this.solicitadoPor;
    }
    
    public void setSolicitadoPor(String solicitadoPor) {
        this.solicitadoPor = solicitadoPor;
    }
    
    @Column(name="AUTORIZADO_POR", length=100)
    public String getAutorizadoPor() {
        return this.autorizadoPor;
    }
    
    public void setAutorizadoPor(String autorizadoPor) {
        this.autorizadoPor = autorizadoPor;
    }
    
    @Column(name="RECHAZADO_POR", length=100)
    public String getRechazadoPor() {
        return this.rechazadoPor;
    }
    
    public void setRechazadoPor(String rechazadoPor) {
        this.rechazadoPor = rechazadoPor;
    }
    
    @Column(name="FECHA_CHEQUE", length=7)
    public Timestamp getFechaCheque() {
        return this.fechaCheque;
    }
    
    public void setFechaCheque(Timestamp fechaCheque) {
        this.fechaCheque = fechaCheque;
    }
    
    @Column(name="IMPORTE_PRIMA")
    public Double getImportePrima() {
        return this.importePrima;
    }
    
    public void setImportePrima(Double importePrima) {
        this.importePrima = importePrima;
    }
    
    @Column(name="IMPORTE_BS")
    public Double getImporteBs() {
        return this.importeBs;
    }
    
    public void setImporteBs(Double importeBs) {
        this.importeBs = importeBs;
    }
    
    @Column(name="IMPORTE_DERPOL")
    public Double getImporteDerpol() {
        return this.importeDerpol;
    }
    
    public void setImporteDerpol(Double importeDerpol) {
        this.importeDerpol = importeDerpol;
    }
    
    @Column(name="IMPORTE_CUM_META")
    public Double getImporteCumMeta() {
        return this.importeCumMeta;
    }
    
    public void setImporteCumMeta(Double importeCumMeta) {
        this.importeCumMeta = importeCumMeta;
    }
    
    @Column(name="IMPORTE_UTILIDAD")
    public Double getImporteUtilidad() {
        return this.importeUtilidad;
    }
    
    public void setImporteUtilidad(Double importeUtilidad) {
        this.importeUtilidad = importeUtilidad;
    }
    
    @Column(name="SUBTOTAL")
    public Double getSubtotal() {
        return this.subtotal;
    }
    
    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }
    
    @Column(name="IMPORTE_TOTAL")
    public Double getImporteTotal() {
        return this.importeTotal;
    }
    
    public void setImporteTotal(Double importeTotal) {
        this.importeTotal = importeTotal;
    }
    
    @Column(name="IVA")
    public Double getIva() {
        return this.iva;
    }
    
    public void setIva(Double iva) {
        this.iva = iva;
    }
    
    @Column(name="IVA_RETENIDO")
    public Double getIvaRetenido() {
        return this.ivaRetenido;
    }
    
    public void setIvaRetenido(Double ivaRetenido) {
        this.ivaRetenido = ivaRetenido;
    }
    
    @Column(name="ISR")
    public Double getIsr() {
        return this.isr;
    }
    
    public void setIsr(Double isr) {
        this.isr = isr;
    }
    
    @Column(name="COMENTARIOS", length=1000)
    public String getComentarios() {
        return this.comentarios;
    }
    
    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }
    
    @Column(name="FECHA_CREACION", nullable=false, length=7)
    public Timestamp getFechaCreacion() {
        return this.fechaCreacion;
    }
    
    public void setFechaCreacion(Timestamp fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    
    @Column(name="CODIGO_USUARIO_CREACION", nullable=false, length=8)
    public String getCodigoUsuarioCreacion() {
        return this.codigoUsuarioCreacion;
    }
    
    public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
        this.codigoUsuarioCreacion = codigoUsuarioCreacion;
    }
    
    @Column(name="FECHA_MODIFICACION", length=7)
    public Timestamp getFechaModificacion() {
        return this.fechaModificacion;
    }
    
    public void setFechaModificacion(Timestamp fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
    
    @Column(name="CODIGO_USUARIO_MODIFICACION", length=8)
    public String getCodigoUsuarioModificacion() {
        return this.codigoUsuarioModificacion;
    }
    
    public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
        this.codigoUsuarioModificacion = codigoUsuarioModificacion;
    }
    
    @Column(name="ORIGEN_LIQUIDACION", length=10)
    public String getOrigenLiquidacion() {
        return this.origenLiquidacion;
    }
    
    public void setOrigenLiquidacion(String origenLiquidacion) {
        this.origenLiquidacion = origenLiquidacion;
    }
    
    @Column(name="IDENTIFICADOR_BEN_ID", precision=0)
    public Long getIdentificadorBenId() {
        return this.identificadorBenId;
    }
    
    public void setIdentificadorBenId(Long identificadorBenId) {
        this.identificadorBenId = identificadorBenId;
    }
    
    @Column(name="CVE_TIPO_PERSONA", length=5)
    public String getCveTipoPersona() {
        return this.cveTipoPersona;
    }
    
    public void setCveTipoPersona(String cveTipoPersona) {
        this.cveTipoPersona = cveTipoPersona;
    }
    
    @Column(name="CVE_TIPO_ENTIDAD", length=30)
    public String getCveTipoEntidad() {
        return this.cveTipoEntidad;
    }
    
    public void setCveTipoEntidad(String cveTipoEntidad) {
        this.cveTipoEntidad = cveTipoEntidad;
    }
    
    @Column(name="NOMBRE_BENEFICIARIO")
    public String getNombreBeneficiario() {
        return this.nombreBeneficiario;
    }
    
    public void setNombreBeneficiario(String nombreBeneficiario) {
        this.nombreBeneficiario = nombreBeneficiario;
    }
    
    @Column(name="CLAVE_NOMBRE")
    public String getClaveNombre() {
        return this.claveNombre;
    }
    
    public void setClaveNombre(String claveNombre) {
        this.claveNombre = claveNombre;
    }
    @Column(name="BANCO_BENEFICIARIO", length=100)
    public String getBancoBeneficiario() {
        return this.bancoBeneficiario;
    }
    
    public void setBancoBeneficiario(String bancoBeneficiario) {
        this.bancoBeneficiario = bancoBeneficiario;
    }

	@Column(name="CLABE_BENEFICIARIO", length=18)
    public String getClabeBeneficiario() {
        return this.clabeBeneficiario;
    }
    
    public void setClabeBeneficiario(String clabeBeneficiario) {
        this.clabeBeneficiario = clabeBeneficiario;
    }
    
    @Column(name="RFC_BENEFICIARIO", length=13)
    public String getRfcBeneficiario() {
        return this.rfcBeneficiario;
    }
    
    public void setRfcBeneficiario(String rfcBeneficiario) {
        this.rfcBeneficiario = rfcBeneficiario;
    }
    
    @Column(name="CORREO", length=100)
    public String getCorreo() {
        return this.correo;
    }
    
    public void setCorreo(String correo) {
        this.correo = correo;
    }
    
    @Column(name="TELEFONO_LADA", length=3)
    public String getTelefonoLada() {
        return this.telefonoLada;
    }
    
    public void setTelefonoLada(String telefonoLada) {
        this.telefonoLada = telefonoLada;
    }
    
    @Column(name="TELEFONO_NUMERO", length=8)
    public String getTelefonoNumero() {
        return this.telefonoNumero;
    }
    
    public void setTelefonoNumero(String telefonoNumero) {
        this.telefonoNumero = telefonoNumero;
    }
    
    @Column(name="FECHA_TRAMITE_MIZAR", length=7)
    public Timestamp getFechaTramiteMizar() {
        return this.fechaTramiteMizar;
    }
    
    public void setFechaTramiteMizar(Timestamp fechaTramiteMizar) {
        this.fechaTramiteMizar = fechaTramiteMizar;
    }
    
    @Column(name="ID_REMESA", precision=0)
    public Long getIdRemesa() {
        return this.idRemesa;
    }
    
    public void setIdRemesa(Long idRemesa) {
        this.idRemesa = idRemesa;
    }
    
    @Column(name="NETO_POR_PAGAR", precision=16)
    public Double getNetoPorPagar() {
        return this.netoPorPagar;
    }
    
    public void setNetoPorPagar(Double netoPorPagar) {
        this.netoPorPagar = netoPorPagar;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_SOL_CHEQUE")
	public SolicitudChequeSiniestro getSolicitudChequeSiniestro() {
		return solicitudChequeSiniestro;
	}

	public void setSolicitudChequeSiniestro(
			SolicitudChequeSiniestro solicitudChequeSiniestro) {
		this.solicitudChequeSiniestro = solicitudChequeSiniestro;
	}
    
	@Transient
	public Short getEstatusFactura() {
		return estatusFactura;
	}

	public void setEstatusFactura(Short estatusFactura) {
		this.estatusFactura = estatusFactura;
	}
    @Transient
	public Short getEstatusOrdenPago() {
		return estatusOrdenPago;
	}

	public void setEstatusOrdenPago(Short estatusOrdenPago) {
		this.estatusOrdenPago = estatusOrdenPago;
	}
    @Transient
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	@Column(name = "FECHA_FACTURA", length = 7)
	  public Timestamp getFechaFactura() {
	    return this.fechaFactura;
	  }

	  public void setFechaFactura(Timestamp fechaFactura) {
	    this.fechaFactura = fechaFactura;
	  }

	  @Column(name = "NOMBRE_ARCHIVO_XML", length = 200)
	  public String getNombreArchivoXml() {
	    return this.nombreArchivoXml;
	  }

	  public void setNombreArchivoXml(String nombreArchivoXml) {
	    this.nombreArchivoXml = nombreArchivoXml;
	  }
  
	
}