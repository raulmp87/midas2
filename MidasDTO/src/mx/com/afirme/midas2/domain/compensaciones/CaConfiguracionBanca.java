package mx.com.afirme.midas2.domain.compensaciones;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="CA_CONFIGURACION_BANCA",schema="MIDAS")

public class CaConfiguracionBanca {
	
	
	private Long id;
	private String nombre;
	private String usuario;
	private Date fechaModificacion;
	
	public CaConfiguracionBanca(){
		
	}
	
	public CaConfiguracionBanca(Long id, String nombre){
		this.id = id;
		this.nombre = nombre;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Id 
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CABANCA_ID_SEQ")
	@SequenceGenerator(name = "CABANCA_ID_SEQ",  schema="MIDAS", sequenceName = "CABANCA_ID_SEQ", allocationSize = 1)
    @Column(name="ID", unique=true, nullable=false, precision=10, scale=0)
	public Long getId() {
		return id;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Column(name="NOMBRE")
	public String getNombre() {
		return nombre;
	}
	
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	@Column(name="USUARIO")
	public String getUsuario() {
		return usuario;
	}
	
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAMODIFICACION", length=7)
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

}
