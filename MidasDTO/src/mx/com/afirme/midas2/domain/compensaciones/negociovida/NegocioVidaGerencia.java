package mx.com.afirme.midas2.domain.compensaciones.negociovida;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.emision.ppct.GerenciaSeycos;

/**
 * NegocioVidaGerencia entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CA_TRNEGOCIOVIDA_GERENCIA", schema = "MIDAS")
public class NegocioVidaGerencia implements java.io.Serializable,Entidad  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5827208072217005414L;
	// Fields

	private Long id;
	private NegocioVida negocioVida;	
	private GerenciaSeycos gerencia;
	private Long idEmpresa;
    private Long idGerencia;
    private Date FSit;
	// Constructors

	/** default constructor */
	public NegocioVidaGerencia() {
	}

	/** minimal constructor */
	public NegocioVidaGerencia(Long id, Long idEmpresa, Long idGerencia, Date FSit) {
		this.id = id;
		this.idEmpresa = idEmpresa;
		this.idGerencia = idGerencia;
		this.FSit = FSit;
	}

	/** full constructor */
	public NegocioVidaGerencia(Long id, NegocioVida negocioVida, GerenciaSeycos gerencia,Long idEmpresa, Long idGerencia, Date FSit) {
		this.id = id;
		this.negocioVida = negocioVida;
		this.gerencia = gerencia;
		this.idEmpresa = idEmpresa;
		this.idGerencia = idGerencia;
		this.FSit = FSit;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CATRNEGVIDAGERENCIA_ID_SEQ")
	@SequenceGenerator(name="CATRNEGVIDAGERENCIA_ID_SEQ", schema="MIDAS",sequenceName="CATRNEGVIDAGERENCIA_ID_SEQ",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "NEGOCIOVIDA_ID")
	public NegocioVida getNegocioVida() {
		return this.negocioVida;
	}

	public void setNegocioVida(NegocioVida negocioVida) {
		this.negocioVida = negocioVida;
	}

//	@ManyToOne(fetch=FetchType.EAGER)
//    @JoinColumns( { 
//    @JoinColumn(name="ID_EMPRESA", referencedColumnName="ID_EMPRESA"), 
//    @JoinColumn(name="ID_GERENCIA", referencedColumnName="ID_GERENCIA"),
//	@JoinColumn(name="F_SIT", referencedColumnName="F_SIT")})
	@Transient
    public GerenciaSeycos getGerencia() {
		return this.gerencia;
	}

	public void setGerencia(GerenciaSeycos gerencia) {
		this.gerencia = gerencia;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long  getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	@Column(name = "ID_EMPRESA")
	public Long getIdEmpresa() {
		return idEmpresa;
	}
	
	@Column(name = "ID_GERENCIA")
	public Long getIdGerencia() {
		return idGerencia;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "F_SIT")
	public Date getFSit() {
		return FSit;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	

	public void setIdGerencia(Long idGerencia) {
		this.idGerencia = idGerencia;
	}

	public void setFSit(Date fSit) {
		FSit = fSit;
	}
}