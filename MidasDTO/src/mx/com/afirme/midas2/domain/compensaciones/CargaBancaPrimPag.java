package mx.com.afirme.midas2.domain.compensaciones;

public class CargaBancaPrimPag implements java.io.Serializable{
	
	 private Long id;
	 private String ofsuc;
     private String nomCanal;
     private String codCanal;
     private String canalNvo;
     private String nomGrupo;
     private String agte;
     private String idAgente;
     private String nomAgte;
     private String nomEjec;
     private String nomRamo;
     private String idCentroEmision;
     private String numPoliza;
     private String numRenovPoliz;
     private String sec;
     private String mon;
     private String asegurado;
     private String fecEmi;
     private String vigDes;
     private String vigHas;
     private String lineaNeg;
     private Double pmaEmi;
     private Double pmaPagada;
     private String ordimp;
     private Double anio;
     private Double mes;
     private Long bancaPrimpagCpId;
     
     public CargaBancaPrimPag(){
     }
     
     public CargaBancaPrimPag(String ofsuc,String nomCanal,String codCanal,String canalNvo, String nomGrupo,String agte,String idAgente,String nomAgte,String nomEjec,
     		String nomRamo,String idCentroEmision,String numPoliza,String numRenovPoliz,String sec,String mon,String asegurado,String fecEmi,String vigDes,String vigHas,
     		String lineaNeg, Double pmaEmi,Double pmaPagada, String ordimp, Double anio, Double mes,Long bancaPrimpagCpId){
     	
     	this.ofsuc = ofsuc;
     	this.nomCanal = nomCanal;
     	this.codCanal = codCanal;
     	this.canalNvo = canalNvo;
     	this.nomGrupo = nomGrupo;
     	this.agte= agte;
     	this.idAgente= idAgente;
     	this.nomAgte = nomAgte;
     	this.nomEjec = nomEjec;
     	this.nomRamo = nomRamo;
     	this.idCentroEmision = idCentroEmision;
     	this.numPoliza = numPoliza;
     	this.numRenovPoliz = numRenovPoliz;
     	this.sec = sec;
     	this.mon = mon;
     	this.asegurado = asegurado;
     	this.fecEmi = fecEmi;
     	this.vigDes = vigDes;
     	this.vigHas = vigHas;
     	this.lineaNeg = lineaNeg;
     	this.pmaEmi = pmaEmi;
     	this.pmaPagada = pmaPagada;
     	this.ordimp = ordimp;
     	this.anio = anio;
     	this.mes = mes;
     	this.bancaPrimpagCpId = bancaPrimpagCpId;
     }

     
     public Long getId() {
         return this.id;
     }
     
     public void setId(Long id) {
         this.id = id;
     }
	public String getOfsuc() {
		return ofsuc;
	}

	public void setOfsuc(String ofsuc) {
		this.ofsuc = ofsuc;
	}

	public String getNomCanal() {
		return nomCanal;
	}

	public void setNomCanal(String nomCanal) {
		this.nomCanal = nomCanal;
	}

	public String getCodCanal() {
		return codCanal;
	}

	public void setCodCanal(String codCanal) {
		this.codCanal = codCanal;
	}

	public String getCanalNvo() {
		return canalNvo;
	}

	public void setCanalNvo(String canalNvo) {
		this.canalNvo = canalNvo;
	}

	public String getNomGrupo() {
		return nomGrupo;
	}

	public void setNomGrupo(String nomGrupo) {
		this.nomGrupo = nomGrupo;
	}

	public String getAgte() {
		return agte;
	}

	public void setAgte(String agte) {
		this.agte = agte;
	}

	public String getIdAgente() {
		return idAgente;
	}

	public void setIdAgente(String idAgente) {
		this.idAgente = idAgente;
	}

	public String getNomAgte() {
		return nomAgte;
	}

	public void setNomAgte(String nomAgte) {
		this.nomAgte = nomAgte;
	}

	public String getNomEjec() {
		return nomEjec;
	}

	public void setNomEjec(String nomEjec) {
		this.nomEjec = nomEjec;
	}

	public String getNomRamo() {
		return nomRamo;
	}

	public void setNomRamo(String nomRamo) {
		this.nomRamo = nomRamo;
	}

	public String getIdCentroEmision() {
		return idCentroEmision;
	}

	public void setIdCentroEmision(String idCentroEmision) {
		this.idCentroEmision = idCentroEmision;
	}

	public String getNumPoliza() {
		return numPoliza;
	}

	public void setNumPoliza(String numPoliza) {
		this.numPoliza = numPoliza;
	}

	public String getNumRenovPoliz() {
		return numRenovPoliz;
	}

	public void setNumRenovPoliz(String numRenovPoliz) {
		this.numRenovPoliz = numRenovPoliz;
	}

	public String getSec() {
		return sec;
	}

	public void setSec(String sec) {
		this.sec = sec;
	}

	public String getMon() {
		return mon;
	}

	public void setMon(String mon) {
		this.mon = mon;
	}

	public String getAsegurado() {
		return asegurado;
	}

	public void setAsegurado(String asegurado) {
		this.asegurado = asegurado;
	}

	public String getFecEmi() {
		return fecEmi;
	}

	public void setFecEmi(String fecEmi) {
		this.fecEmi = fecEmi;
	}

	public String getVigDes() {
		return vigDes;
	}

	public void setVigDes(String vigDes) {
		this.vigDes = vigDes;
	}

	public String getVigHas() {
		return vigHas;
	}

	public void setVigHas(String vigHas) {
		this.vigHas = vigHas;
	}

	public String getLineaNeg() {
		return lineaNeg;
	}

	public void setLineaNeg(String lineaNeg) {
		this.lineaNeg = lineaNeg;
	}

	public Double getPmaEmi() {
		return pmaEmi;
	}

	public void setPmaEmi(Double pmaEmi) {
		this.pmaEmi = pmaEmi;
	}

	public Double getPmaPagada() {
		return pmaPagada;
	}

	public void setPmaPagada(Double pmaPagada) {
		this.pmaPagada = pmaPagada;
	}

	public String getOrdimp() {
		return ordimp;
	}

	public void setOrdimp(String ordimp) {
		this.ordimp = ordimp;
	}

	public Double getAnio() {
		return anio;
	}

	public void setAnio(Double anio) {
		this.anio = anio;
	}

	public Double getMes() {
		return mes;
	}

	public void setMes(Double mes) {
		this.mes = mes;
	}

	public Long getBancaPrimpagCpId() {
		return bancaPrimpagCpId;
	}

	public void setBancaPrimpagCpId(Long bancaPrimpagCpId) {
		this.bancaPrimpagCpId = bancaPrimpagCpId;
	}

}
