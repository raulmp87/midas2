package mx.com.afirme.midas2.domain.compensaciones;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.poliza.PolizaDTO;

@Entity
@Table(name = "CA_ORDENPAGOS_DETALLE", schema = "MIDAS")
public class OrdenPagosDetalle implements java.io.Serializable {
	//Constants
	private static final long serialVersionUID = 1L;
	//CONSTANTES DE ESTATUS DE ORDEN DE PAGO GENERADO  DETALLE
	public static final Long ESTATUS_ORDPAG_GEN_PROCESO = 1L;
	public static final Long ESTATUS_ORDPAG_GEN_EXCLUIDO = 2L;
	public static final Long ESTATUS_ORDPAG_GEN_INCLUIDO = 3L;
	public static final Long ESTATUS_ORDPAG_GEN_GENERADO = 4L;
	//CONSTANTES DE ESTATUS DE ORDEN DE PAGO DETALLE
	public static final Long ESTATUS_ORDENPAGO_CANCELADO = 1L;
	public static final Long ESTATUS_ORDENPAGO_VALIDA = 2L;
	public static final Long ESTATUS_ORDENPAGO_AUTORIZADA = 3L;
	//ESTATUS_FACTURA
	public static final Long ESTATUS_FACTURA_PENDIENTE = 1L;
	public static final Long ESTATUS_FACTURA_VALIDA = 2L;
	
	private Long id;
	private CaTipoEntidad caTipoentidad;
	private CaTipoCompensacion caTipoCompensacion;
	private CaEntidadPersona caEntidadpersona;
	private CaRamo caRamo;
	private CaCompensacion caCompensacion;
	private PolizaDTO polizaDTO;
	private Long idtopoliza;
	private Long idRecibo;
	private String nombreBeneficiario;
	private String claveNombre;
	private String nombreContratante;
	private Double importePrima;
	private Double importeBs;
	private Double importeDerpol;
	private Double importeCumMeta;
	private Double importeUtilidad;
	private Double subtotal;
	private Double iva;
	private Double ivaRetenido;
	private Double isr;
	private Short estatusFactura;
	private Short estatusOrdenpago;
	private Short estatusOrdpagGen;
	private Double importeTotal;
	private Timestamp fechaCorte;
	private Timestamp fechaCreacion;
	private Date fechaOrdenPagoGenerado;
	private Long folio;
	private Long numeroEndoso;
    private String primaBase;
    private Double importePrimaBase;
    private Double totalCompensacion;
    private Double porcentajeCompPrima;
    private Long liquidacionId;
    private Long poliza;
    private Double recargo;
    private Double comisionSistema;
    private Double comisionAjuste;
    private String numeroPoliza;
    
    private String ramo;
    private String tipoEntidad;
    private Long negocio;
    private String nombreNegocio;
    private Long agente;
    private Long promotor;
    private Long proveedor;
    private Long gerencia;
    private Long numFolioRbo;
	private Date fechaModificacion;
	private Double montoBanca;
	private Double porcentajeSiniestralidad;
	private Double importeCompensacion;
    
    
	// Constructors
	/** default constructor */
	public OrdenPagosDetalle() {
	}

	/** minimal constructor */
	public OrdenPagosDetalle(Long id) {
		this.id = id;
	}

	/** full constructor */
	public OrdenPagosDetalle(Long id, CaTipoEntidad caTipoentidad,
			CaTipoCompensacion caTipoCompensacion, CaEntidadPersona caEntidadpersona,
			CaRamo caRamo, CaCompensacion caCompensacion,PolizaDTO polizaDTO ,Long idtopoliza,
			Long idRecibo, String nombreBeneficiario, String claveNombre,
			String nombreContratante, Double importePrima, Double importeBs,
			Double importeDerpol, Double importeCumMeta,
			Double importeUtilidad, Double subtotal, Double iva,
			Double ivaRetenido, Double isr, Short estatusFactura,
			Short estatusOrdenpago, Short estatusOrdpagGen,
			Double importeTotal, Timestamp fechaCorte, Timestamp fechaCreacion, Long tipoCompensacion_id,Long folio,
			Long numeroEndoso, String primaBase,Double importe_prima_base,Double porcentajeCompPrima, Long liquidacionId, 
			Double totalCompensacion, Long poliza,Double recargo,Double comisionSistema,Double comisionAjuste,Date fechaModificacion) {
		this.id = id;
		this.caTipoentidad = caTipoentidad;
		this.caTipoCompensacion = caTipoCompensacion;
		this.caEntidadpersona = caEntidadpersona;
		this.caRamo = caRamo;
		this.caCompensacion = caCompensacion;
		this.polizaDTO = polizaDTO;
		this.idtopoliza = idtopoliza;
		this.idRecibo = idRecibo;
		this.nombreBeneficiario = nombreBeneficiario;
		this.claveNombre = claveNombre;
		this.nombreContratante = nombreContratante;
		this.importePrima = importePrima;
		this.importeBs = importeBs;
		this.importeDerpol = importeDerpol;
		this.importeCumMeta = importeCumMeta;
		this.importeUtilidad = importeUtilidad;
		this.subtotal = subtotal;
		this.iva = iva;
		this.ivaRetenido = ivaRetenido;
		this.isr = isr;
		this.estatusFactura = estatusFactura;
		this.estatusOrdenpago = estatusOrdenpago;
		this.estatusOrdpagGen = estatusOrdpagGen;
		this.importeTotal = importeTotal;
		this.fechaCorte = fechaCorte;
		this.fechaCreacion = fechaCreacion;
		this.folio = folio;
		this.numeroEndoso = numeroEndoso;
	    this.primaBase = primaBase;
	    this.importePrimaBase = importe_prima_base;
	    this.porcentajeCompPrima = porcentajeCompPrima;
	    this.liquidacionId = liquidacionId;
	    this.totalCompensacion = totalCompensacion;
	    this.poliza = poliza;
	    this.recargo = recargo;
	    this.comisionAjuste = comisionAjuste;
	    this.comisionSistema = comisionSistema;
	    this.fechaModificacion = fechaModificacion;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = ".CAORDENPAGOS_DETALLE_ID_SEQ")
	@SequenceGenerator(name = ".CAORDENPAGOS_DETALLE_ID_SEQ", schema = "MIDAS", sequenceName = ".CAORDENPAGOS_DETALLE_ID_SEQ", allocationSize = 1)
	@Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TIPOENTIDAD_ID")
	public CaTipoEntidad getCaTipoentidad() {
		return this.caTipoentidad;
	}

	public void setCaTipoentidad(CaTipoEntidad caTipoentidad) {
		this.caTipoentidad = caTipoentidad;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ENTIDADPERSONA_ID")
	public CaEntidadPersona getCaEntidadpersona() {
		return this.caEntidadpersona;
	}

	public void setCaEntidadpersona(CaEntidadPersona caEntidadpersona) {
		this.caEntidadpersona = caEntidadpersona;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "RAMO_ID")
	public CaRamo getCaRamo() {
		return this.caRamo;
	}

	public void setCaRamo(CaRamo caRamo) {
		this.caRamo = caRamo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPENSACION_ID")
	public CaCompensacion getCaCompensacion() {
		return this.caCompensacion;
	}

	public void setCaCompensacion(CaCompensacion caCompensacion) {
		this.caCompensacion = caCompensacion;
	}

	@Column(name = "IDTOPOLIZA", precision = 0)
	public Long getIdtopoliza() {
		return this.idtopoliza;
	}

	public void setIdtopoliza(Long idtopoliza) {
		this.idtopoliza = idtopoliza;
	}

	@Column(name = "ID_RECIBO", precision = 0)
	public Long getIdRecibo() {
		return this.idRecibo;
	}

	public void setIdRecibo(Long idRecibo) {
		this.idRecibo = idRecibo;
	}

	@Column(name = "NOMBRE_BENEFICIARIO")
	public String getNombreBeneficiario() {
		return this.nombreBeneficiario;
	}

	public void setNombreBeneficiario(String nombreBeneficiario) {
		this.nombreBeneficiario = nombreBeneficiario;
	}

	@Column(name = "CLAVE_NOMBRE")
	public String getClaveNombre() {
		return this.claveNombre;
	}

	public void setClaveNombre(String claveNombre) {
		this.claveNombre = claveNombre;
	}

	@Column(name = "NOMBRE_CONTRATANTE")
	public String getNombreContratante() {
		return this.nombreContratante;
	}

	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}

	@Column(name = "IMPORTE_PRIMA" )
	public Double getImportePrima() {
		return this.importePrima;
	}

	public void setImportePrima(Double importePrima) {
		this.importePrima = importePrima;
	}

	@Column(name = "IMPORTE_BS")
	public Double getImporteBs() {
		return this.importeBs;
	}

	public void setImporteBs(Double importeBs) {
		this.importeBs = importeBs;
	}

	@Column(name = "IMPORTE_DERPOL")
	public Double getImporteDerpol() {
		return this.importeDerpol;
	}

	public void setImporteDerpol(Double importeDerpol) {
		this.importeDerpol = importeDerpol;
	}

	@Column(name = "IMPORTE_CUM_META")
	public Double getImporteCumMeta() {
		return this.importeCumMeta;
	}

	public void setImporteCumMeta(Double importeCumMeta) {
		this.importeCumMeta = importeCumMeta;
	}

	@Column(name = "IMPORTE_UTILIDAD")
	public Double getImporteUtilidad() {
		return this.importeUtilidad;
	}

	public void setImporteUtilidad(Double importeUtilidad) {
		this.importeUtilidad = importeUtilidad;
	}

	@Column(name = "SUBTOTAL")
	public Double getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(Double subtotal) {
		this.subtotal = subtotal;
	}

	@Column(name = "IVA")
	public Double getIva() {
		return this.iva;
	}

	public void setIva(Double iva) {
		this.iva = iva;
	}

	@Column(name = "IVA_RETENIDO")
	public Double getIvaRetenido() {
		return this.ivaRetenido;
	}

	public void setIvaRetenido(Double ivaRetenido) {
		this.ivaRetenido = ivaRetenido;
	}

	@Column(name = "ISR")
	public Double getIsr() {
		return this.isr;
	}

	public void setIsr(Double isr) {
		this.isr = isr;
	}

	@Column(name = "ESTATUS_FACTURA", precision = 0)
	public Short getEstatusFactura() {
		return this.estatusFactura;
	}

	public void setEstatusFactura(Short estatusFactura) {
		this.estatusFactura = estatusFactura;
	}

	@Column(name = "ESTATUS_ORDENPAGO", precision = 0)
	public Short getEstatusOrdenpago() {
		return this.estatusOrdenpago;
	}

	public void setEstatusOrdenpago(Short estatusOrdenpago) {
		this.estatusOrdenpago = estatusOrdenpago;
	}

	@Column(name = "ESTATUS_ORDPAG_GEN", precision = 0)
	public Short getEstatusOrdpagGen() {
		return this.estatusOrdpagGen;
	}

	public void setEstatusOrdpagGen(Short estatusOrdpagGen) {
		this.estatusOrdpagGen = estatusOrdpagGen;
	}

	@Column(name = "IMPORTE_TOTAL")
	public Double getImporteTotal() {
		return this.importeTotal;
	}

	public void setImporteTotal(Double importeTotal) {
		this.importeTotal = importeTotal;
	}

	@Column(name = "FECHA_CORTE", length = 7)
	public Timestamp getFechaCorte() {
		return this.fechaCorte;
	}

	public void setFechaCorte(Timestamp fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	@Column(name = "FECHA_CREACION", length = 7)
	public Timestamp getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Timestamp fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TIPOCOMPENSACION_ID")
	public CaTipoCompensacion getCaTipoCompensacion() {
		return caTipoCompensacion;
	}

	public void setCaTipoCompensacion(CaTipoCompensacion caTipoCompensacion) {
		this.caTipoCompensacion = caTipoCompensacion;
	}

	public void setFechaOrdenPagoGenerado(Date fechaOrdenPagoGenerado) {
		this.fechaOrdenPagoGenerado = fechaOrdenPagoGenerado;
	}
	
	@Column(name = "FECHA_ORDEN_PAGO_GEN")
	@Temporal(TemporalType.DATE)
	public Date getFechaOrdenPagoGenerado() {
		return fechaOrdenPagoGenerado;
	}
	
	@Column(name = "FOLIO")
	public Long getFolio() {
		return folio;
	}

	public void setFolio(Long folio) {
		this.folio = folio;
	}
	@Column(name = "NUMEROENDOSO")
	public Long getNumeroEndoso(){
		return numeroEndoso;
	}
	public void setNumeroEndoso(Long numeroEndoso){
		this.numeroEndoso = numeroEndoso;
	}
	@Column(name = "PRIMABASE")
	 public String getPrimaBase(){
		return primaBase;
	}
	public void setPrimaBase(String primaBase){
		this.primaBase = primaBase;
	}
	@Column(name = "IMPORTEPRIMABASE")
	public Double getImportePrimaBase(){
		return importePrimaBase;
	}
	public void setImportePrimaBase(Double importePrimaBase){
		this.importePrimaBase = importePrimaBase;
	}
	@Column(name = "PORCENTAJECOMPPRIMA")
	 public Double getPorcentajeCompPrima(){
		return porcentajeCompPrima;
	}
	public void setPorcentajeCompPrima(Double porcentajeCompPrima){
		this.porcentajeCompPrima = porcentajeCompPrima;
	}
	@Column(name = "LIQUIDACIONID")
	public Long getLiquidacionId(){
		return liquidacionId;
	}
	public void setLiquidacionId(Long liquidacionId){
		this.liquidacionId = liquidacionId;
	}
	@Column(name = "TOTALCOMPENSACION")
	public Double getTotalCompensacion(){
		return totalCompensacion;
	}
	public void setTotalCompensacion(Double totalCompensacion){
		this.totalCompensacion = totalCompensacion;
	}
	@Column(name = "POLIZA")
	
	public Long getPoliza(){
		return poliza;
	}
	public void setPoliza(Long poliza){
		this.poliza = poliza;
	}
	@Column(name = "RECARGO")
	
	public Double getRecargo() {
		return recargo;
	}

	public void setRecargo(Double recargo) {
		this.recargo = recargo;
	}
    @Column(name = "COMISIONSISTEMA")
	public Double getComisionSistema() {
		return comisionSistema;
	}

	public void setComisionSistema(Double comisionSistema) {
		this.comisionSistema = comisionSistema;
	}
    @Column(name = "COMISIONAJUSTE")
	public Double getComisionAjuste() {
		return comisionAjuste;
	}

	public void setComisionAjuste(Double comisionAjuste) {
		this.comisionAjuste = comisionAjuste;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOPOLIZA", nullable=false, insertable=false, updatable=false)
	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}
	
	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	@Transient
	public String getNumeroPolizaFormateada() {
		String numeroPoliza = "";
		if ((polizaDTO.getCodigoProducto() != null && polizaDTO.getCodigoProducto().length() > 0)
				&& (polizaDTO.getCodigoTipoPoliza() != null && polizaDTO.getCodigoTipoPoliza()
						.length() > 0) && (polizaDTO.getNumeroPoliza() != null)
				&& (polizaDTO.getNumeroRenovacion() != null)) {
			try {
				numeroPoliza += String.format("%02d", Integer
						.parseInt(polizaDTO.getCodigoProducto()));
				numeroPoliza += String.format("%02d", Integer
						.parseInt(polizaDTO.getCodigoTipoPoliza()));
				numeroPoliza += "-";
				numeroPoliza += String.format("%06d", polizaDTO.getNumeroPoliza());
				numeroPoliza += "-";
				numeroPoliza += String.format("%02d", polizaDTO.getNumeroRenovacion());
			} catch (Exception e) {
				return "";
			}

		}
		return numeroPoliza;
	}

	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	@Transient
	public String getRamo() {
		return ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}
	
	@Transient
	public String getTipoEntidad() {
		return tipoEntidad;
	}

	public void setTipoEntidad(String tipoEntidad) {
		this.tipoEntidad = tipoEntidad;
	}
	@Transient
	public Long getNegocio() {
		return negocio;
	}

	public void setNegocio(Long negocio) {
		this.negocio = negocio;
	}
	@Transient
	public String getNombreNegocio() {
		return nombreNegocio;
	}

	public void setNombreNegocio(String nombreNegocio) {
		this.nombreNegocio = nombreNegocio;
	}
	@Transient
	public Long getAgente() {
		return agente;
	}

	public void setAgente(Long agente) {
		this.agente = agente;
	}
	@Transient
	public Long getPromotor() {
		return promotor;
	}

	public void setPromotor(Long promotor) {
		this.promotor = promotor;
	}
	@Transient
	public Long getProveedor() {
		return proveedor;
	}

	public void setProveedor(Long proveedor) {
		this.proveedor = proveedor;
	}
	@Transient
	public Long getGerencia() {
		return gerencia;
	}

	public void setGerencia(Long gerencia) {
		this.gerencia = gerencia;
	}
	@Transient
	public Long getNumFolioRbo() {
		return numFolioRbo;
	}

	public void setNumFolioRbo(Long numFolioRbo) {
		this.numFolioRbo = numFolioRbo;
	}
	
	@Column(name = "FECHA_MODIFICACION", length = 7)
	@Temporal(TemporalType.DATE)
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	@Column(name = "MONTO_ASIG_BANCA")
	public Double getMontoBanca() {
		return montoBanca;
	}

	public void setMontoBanca(Double montoBanca) {
		this.montoBanca = montoBanca;
	}
	
	@Column(name = "PORC_ASIG_SINIESTRALIDAD", length = 7)
	public Double getPorcentajeSiniestralidad() {
		return porcentajeSiniestralidad;
	}

	public void setPorcentajeSiniestralidad(Double porcentajeSiniestralidad) {
		this.porcentajeSiniestralidad = porcentajeSiniestralidad;
	}
	@Transient
	public Double getImporteCompensacion() {
		return importeCompensacion;
	}

	public void setImporteCompensacion(Double importeCompensacion) {
		this.importeCompensacion = importeCompensacion;
	}
}