package mx.com.afirme.midas2.domain.compensaciones;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Transient;

@Entity
public class CaConfiguracionCompensacionLista implements Serializable{
	
	private static final long serialVersionUID = -6295990373029559993L;
	private Long id;
	private String idNegocio;
	private String negocio;
	private String cotizacion;
	private String poliza;
	private String ramo;
	private String estatus;
	private String sinCompensacion;
	private CaCompensacion caCompensacion;
	
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIdNegocio() {
		return idNegocio;
	}
	public void setIdNegocio(String idNegocio) {
		this.idNegocio = idNegocio;
	}
	public String getNegocio() {
		return negocio;
	}
	public void setNegocio(String negocio) {
		this.negocio = negocio;
	}
	public String getCotizacion() {
		return cotizacion;
	}
	public void setCotizacion(String cotizacion) {
		this.cotizacion = cotizacion;
	}
	public String getPoliza() {
		return poliza;
	}
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	public String getRamo() {
		return ramo;
	}
	public void setRamo(String ramo) {
		this.ramo = ramo;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	@Transient
	public String getSinCompensacion(){
		return sinCompensacion;
	}
	public void setSinCompensacion(String sinCompensacion){
		this.sinCompensacion = sinCompensacion;
	}
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="COMPENSACION_ID")

	public CaCompensacion getCaCompensacion() {
	    return caCompensacion;
	}
	
	public void setCaCompensacion(CaCompensacion caCompensacion) {
	    this.caCompensacion = caCompensacion;
	}

}
