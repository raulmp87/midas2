package mx.com.afirme.midas2.domain.compensaciones.negociovida;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.emision.ppct.ProductoSeycos;

/**
 * NegocioVidaProducto entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CA_TRNEGOCIOVIDA_PRODUCTO", schema = "MIDAS")
public class NegocioVidaProducto implements java.io.Serializable,Entidad  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields

	private Long id;
	private NegocioVida negocioVida;
	private ProductoSeycos producto;

	// Constructors

	/** default constructor */
	public NegocioVidaProducto() {
	}

	/** minimal constructor */
	public NegocioVidaProducto(Long id) {
		this.id = id;
	}

	/** full constructor */
	public NegocioVidaProducto(Long id, NegocioVida negocioVida,
			ProductoSeycos producto) {
		this.id = id;
		this.negocioVida = negocioVida;
		this.producto = producto;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CATRNEGVIDAPROD_ID_SEQ")
	@SequenceGenerator(name="CATRNEGVIDAPROD_ID_SEQ", schema="MIDAS",sequenceName="CATRNEGVIDAPROD_ID_SEQ",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "NEGOCIOVIDA_ID")
	public NegocioVida getNegocioVida() {
		return this.negocioVida;
	}

	public void setNegocioVida(NegocioVida negocioVida) {
		this.negocioVida = negocioVida;
	}
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "PRODUCTO_ID")
	public ProductoSeycos getProducto() {
		return this.producto;
	}

	public void setProducto(ProductoSeycos producto) {
		this.producto = producto;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long  getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

}