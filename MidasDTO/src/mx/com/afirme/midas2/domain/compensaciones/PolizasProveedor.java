package mx.com.afirme.midas2.domain.compensaciones;

public class PolizasProveedor implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8513376028251740108L;

	private String idPoliza;
	
	public PolizasProveedor(){
		
	}

	public PolizasProveedor(String idPoliza) {
		this.idPoliza = idPoliza;
	}

	public String getIdPoliza() {
		return idPoliza;
	}

	public void setIdPoliza(String idPoliza) {
		this.idPoliza = idPoliza;
	}
	
	


}
