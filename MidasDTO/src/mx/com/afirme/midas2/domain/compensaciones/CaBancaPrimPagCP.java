package mx.com.afirme.midas2.domain.compensaciones;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name="CA_BANCA_PRIMPAG_CP",schema="MIDAS")
public class CaBancaPrimPagCP implements java.io.Serializable{


	
	private static final long serialVersionUID = 1931177205232977563L;
	private Long id;
    private BigDecimal costonetosinaut;
    private BigDecimal costonetosindan;
    private BigDecimal primasretendevenaut;
    private BigDecimal primasretendevendan;
    private Double anio;
    private Double mes;
    private Date modificacion;
    private String usuario;
    private String valor;
    
    public CaBancaPrimPagCP(){
    }
    
    public CaBancaPrimPagCP(BigDecimal costonetosinaut,BigDecimal costonetosindan,BigDecimal primasretendevenaut,BigDecimal primasretendevendan,Double anio,Double mes
    		,Date modificacion,String usuario){
    	this.costonetosinaut = costonetosinaut;
    	this.costonetosindan = costonetosindan;
    	this.primasretendevenaut = primasretendevenaut;
    	this.primasretendevendan = primasretendevendan;
    	this.anio = anio;
    	this.mes = mes;
    	this.modificacion = modificacion;
    	this.usuario = usuario;
    }
    
    @Id 
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CA_BANCA_PRIMPAG_CP_ID_SEQ")
	@SequenceGenerator(name = "CA_BANCA_PRIMPAG_CP_ID_SEQ",  schema="MIDAS", sequenceName = "CA_BANCA_PRIMPAG_CP_ID_SEQ", allocationSize = 1)
    @Column(name="ID", unique=true)
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    @Column(name="COSTONETOSINAUT")
    public BigDecimal getCostonetosinaut() {
        return this.costonetosinaut;
    }
    
    public void setCostonetosinaut(BigDecimal costonetosinaut) {
        this.costonetosinaut = costonetosinaut;
    }
    
    @Column(name="COSTONETOSINDAN")
    public BigDecimal getCostonetosindan() {
        return this.costonetosindan;
    }
    
    public void setCostonetosindan(BigDecimal costonetosindan) {
        this.costonetosindan = costonetosindan;
    }
    
    @Column(name="PRIMASRETENDEVENAUT")
    public BigDecimal getPrimasretendevenaut() {
        return this.primasretendevenaut;
    }
    
    public void setPrimasretendevenaut(BigDecimal primasretendevenaut) {
        this.primasretendevenaut = primasretendevenaut;
    }
    
    @Column(name="PRIMASRETENDEVENDAN")
    public BigDecimal getPrimasretendevendan() {
        return this.primasretendevendan;
    }
    
    public void setPrimasretendevendan(BigDecimal primasretendevendan) {
        this.primasretendevendan = primasretendevendan;
    }
    
    @Column(name="ANIO")
	public Double getAnio() {
		return anio;
	}

	public void setAnio(Double anio) {
		this.anio = anio;
	}
	
	@Column(name="MES")
	public Double getMes() {
		return mes;
	}

	public void setMes(Double mes) {
		this.mes = mes;
	}
	@Transient
	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
	@Temporal(TemporalType.DATE)
	public Date getModificacion() {
		return modificacion;
	}

	public void setModificacion(Date modificacion) {
		this.modificacion = modificacion;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
   

}
