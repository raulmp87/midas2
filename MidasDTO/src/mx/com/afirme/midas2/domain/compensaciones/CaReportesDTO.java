package mx.com.afirme.midas2.domain.compensaciones;

import java.util.Date;

public class CaReportesDTO implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5307765510540167695L;
	

	
	private Long identificador_ben_id;
	private String rfc_beneficiario;
	private String nombre_beneficiario;
	private String domicilio;
	private String colonia;
	private Long codigoPostal;
	private String promotoria;
	private String gerencia;
	private String ejecutivo;
	private String centroOperacion;
	private String numeroCedula;
	private String nombre_oficina;
	private Double saldoInicial;
	private Double importe_comGrabadasIva;
	private Double importe_comExentasIva;
	private Double subTotal;
	private Double importe_ivaPagada;
	private Double importe_ivaRetenido;
	private Double importe_isrRetenido;
	private Double importe_netoPago;
	private Double saldo;
	private Double saldoInicial_mesAnterior;
	private Double imp_comGrab_mesAnterior;
	private Double imp_comGrab_acomEjercicio;
	private Double imp_comExen_mesAnterior;
	private Double imp_comExen_acomEjercicio;
	private Double subTotal_mesAnterior;
	private Double subTotal_acomEjercicio;
	private Double imp_ivaPagada_mesAnterior;
	private Double imp_ivaPagada_acomEjercicio;
	private Double imp_ivaRetenido_mesAnterior;
	private Double imp_ivaRetenido_acomEjercicio;
	private Double imp_isrRetenido_mesAnterior;
	private Double imp_isrRetenido_acomEjercicio;
	private Double importe_netoPago_mesAnterior;
	private Double importe_netoPago_acomEjercicio;
	private Double saldo_mesAnterior;
	private String fecha_tramite;
		                             
   
	
	
	public static class DatosReporteEstadoCuentaParametrosDTO implements java.io.Serializable {
		private Long anioMes;
		private Long cveAgente;
		private Long cvePromotor;
		private Long cveProveedor;
		
		
		public DatosReporteEstadoCuentaParametrosDTO(Long anioMes,Long cveAgente,Long cvePromotor,Long cveProveedor)
		{
			super();
			this.anioMes = anioMes;
			this.cveAgente = cveAgente;
			this.cvePromotor = cvePromotor;
			this.cveProveedor = cveProveedor;
			
		}
		

			public Long getCveAgente() {
				return cveAgente;
			}
			public void setCveAgente(Long cveAgente) {
				this.cveAgente = cveAgente;
			}
			public Long getCvePromotor() {
				return cvePromotor;
			}
			public void setCvePromotor(Long cvePromotor) {
				this.cvePromotor = cvePromotor;
			}
			public Long getCveProveedor() {
				return cveProveedor;
			}
			public void setCveProveedor(Long cveProveedor) {
				this.cveProveedor = cveProveedor;
			}
			public Long getAnioMes() {
				return anioMes;
			}
			public void setAnioMes(Long anioMes) {
				this.anioMes = anioMes;
			}
			
		
	}
	
	
	
	public Long getIdentificador_ben_id() {
		return identificador_ben_id;
	}
	public void setIdentificador_ben_id(Long identificador_ben_id) {
		this.identificador_ben_id = identificador_ben_id;
	}
	public String getDomicilio() {
		return domicilio;
	}
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	public String getColonia() {
		return colonia;
	}
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	public Long getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(Long codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public String getPromotoria() {
		return promotoria;
	}
	public void setPromotoria(String promotoria) {
		this.promotoria = promotoria;
	}
	public String getRfc_beneficiario() {
		return rfc_beneficiario;
	}
	public void setRfc_beneficiario(String rfc_beneficiario) {
		this.rfc_beneficiario = rfc_beneficiario;
	}
	public String getNombre_beneficiario() {
		return nombre_beneficiario;
	}
	public void setNombre_beneficiario(String nombre_beneficiario) {
		this.nombre_beneficiario = nombre_beneficiario;
	}
	public String getGerencia() {
		return gerencia;
	}
	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}
	public String getEjecutivo() {
		return ejecutivo;
	}
	public void setEjecutivo(String ejecutivo) {
		this.ejecutivo = ejecutivo;
	}
	public String getCentroOperacion() {
		return centroOperacion;
	}
	public void setCentroOperacion(String centroOperacion) {
		this.centroOperacion = centroOperacion;
	}
	public String getNumeroCedula() {
		return numeroCedula;
	}
	public void setNumeroCedula(String numeroCedula) {
		this.numeroCedula = numeroCedula;
	}
	public String getNombre_oficina() {
		return nombre_oficina;
	}
	public void setNombre_oficina(String nombre_oficina) {
		this.nombre_oficina = nombre_oficina;
	}
	public String getFecha_tramite() {
		return fecha_tramite;
	}
	public void setFecha_tramite(String fecha_tramite) {
		this.fecha_tramite = fecha_tramite;
	}
	public Double getSaldoInicial() {
		return saldoInicial;
	}
	public void setSaldoInicial(Double saldoInicial) {
		this.saldoInicial = saldoInicial;
	}
	public Double getImporte_comGrabadasIva() {
		return importe_comGrabadasIva;
	}
	public void setImporte_comGrabadasIva(Double importe_comGrabadasIva) {
		this.importe_comGrabadasIva = importe_comGrabadasIva;
	}
	public Double getImporte_comExentasIva() {
		return importe_comExentasIva;
	}
	public void setImporte_comExentasIva(Double importe_comExentasIva) {
		this.importe_comExentasIva = importe_comExentasIva;
	}
	public Double getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}
	public Double getImporte_ivaPagada() {
		return importe_ivaPagada;
	}
	public void setImporte_ivaPagada(Double importe_ivaPagada) {
		this.importe_ivaPagada = importe_ivaPagada;
	}
	public Double getImporte_ivaRetenido() {
		return importe_ivaRetenido;
	}
	public void setImporte_ivaRetenido(Double importe_ivaRetenido) {
		this.importe_ivaRetenido = importe_ivaRetenido;
	}
	public Double getImporte_isrRetenido() {
		return importe_isrRetenido;
	}
	public void setImporte_isrRetenido(Double importe_isrRetenido) {
		this.importe_isrRetenido = importe_isrRetenido;
	}
	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	public Double getImporte_netoPago() {
		return importe_netoPago;
	}
	public void setImporte_netoPago(Double importe_netoPago) {
		this.importe_netoPago = importe_netoPago;
	}
	public Double getSaldoInicial_mesAnterior() {
		return saldoInicial_mesAnterior;
	}
	public void setSaldoInicial_mesAnterior(Double saldoInicial_mesAnterior) {
		this.saldoInicial_mesAnterior = saldoInicial_mesAnterior;
	}
	public Double getImp_comGrab_mesAnterior() {
		return imp_comGrab_mesAnterior;
	}
	public void setImp_comGrab_mesAnterior(Double imp_comGrab_mesAnterior) {
		this.imp_comGrab_mesAnterior = imp_comGrab_mesAnterior;
	}
	public Double getImp_comExen_mesAnterior() {
		return imp_comExen_mesAnterior;
	}
	public void setImp_comExen_mesAnterior(Double imp_comExen_mesAnterior) {
		this.imp_comExen_mesAnterior = imp_comExen_mesAnterior;
	}
	public Double getSubTotal_mesAnterior() {
		return subTotal_mesAnterior;
	}
	public void setSubTotal_mesAnterior(Double subTotal_mesAnterior) {
		this.subTotal_mesAnterior = subTotal_mesAnterior;
	}
	public Double getImp_ivaPagada_mesAnterior() {
		return imp_ivaPagada_mesAnterior;
	}
	public void setImp_ivaPagada_mesAnterior(Double imp_ivaPagada_mesAnterior) {
		this.imp_ivaPagada_mesAnterior = imp_ivaPagada_mesAnterior;
	}
	public Double getImp_ivaRetenido_mesAnterior() {
		return imp_ivaRetenido_mesAnterior;
	}
	public void setImp_ivaRetenido_mesAnterior(Double imp_ivaRetenido_mesAnterior) {
		this.imp_ivaRetenido_mesAnterior = imp_ivaRetenido_mesAnterior;
	}
	public Double getImp_isrRetenido_mesAnterior() {
		return imp_isrRetenido_mesAnterior;
	}
	public void setImp_isrRetenido_mesAnterior(Double imp_isrRetenido_mesAnterior) {
		this.imp_isrRetenido_mesAnterior = imp_isrRetenido_mesAnterior;
	}
	public Double getImporte_netoPago_mesAnterior() {
		return importe_netoPago_mesAnterior;
	}
	public void setImporte_netoPago_mesAnterior(Double importe_netoPago_mesAnterior) {
		this.importe_netoPago_mesAnterior = importe_netoPago_mesAnterior;
	}
	public Double getSaldo_mesAnterior() {
		return saldo_mesAnterior;
	}
	public void setSaldo_mesAnterior(Double saldo_mesAnterior) {
		this.saldo_mesAnterior = saldo_mesAnterior;
	}
	public Double getImp_comExen_acomEjercicio() {
		return imp_comExen_acomEjercicio;
	}
	public void setImp_comExen_acomEjercicio(Double imp_comExen_acomEjercicio) {
		this.imp_comExen_acomEjercicio = imp_comExen_acomEjercicio;
	}
	public Double getImp_comGrab_acomEjercicio() {
		return imp_comGrab_acomEjercicio;
	}
	public void setImp_comGrab_acomEjercicio(Double imp_comGrab_acomEjercicio) {
		this.imp_comGrab_acomEjercicio = imp_comGrab_acomEjercicio;
	}
	public Double getSubTotal_acomEjercicio() {
		return subTotal_acomEjercicio;
	}
	public void setSubTotal_acomEjercicio(Double subTotal_acomEjercicio) {
		this.subTotal_acomEjercicio = subTotal_acomEjercicio;
	}
	public Double getImp_ivaPagada_acomEjercicio() {
		return imp_ivaPagada_acomEjercicio;
	}
	public void setImp_ivaPagada_acomEjercicio(Double imp_ivaPagada_acomEjercicio) {
		this.imp_ivaPagada_acomEjercicio = imp_ivaPagada_acomEjercicio;
	}
	public Double getImp_ivaRetenido_acomEjercicio() {
		return imp_ivaRetenido_acomEjercicio;
	}
	public void setImp_ivaRetenido_acomEjercicio(
			Double imp_ivaRetenido_acomEjercicio) {
		this.imp_ivaRetenido_acomEjercicio = imp_ivaRetenido_acomEjercicio;
	}
	public Double getImp_isrRetenido_acomEjercicio() {
		return imp_isrRetenido_acomEjercicio;
	}
	public void setImp_isrRetenido_acomEjercicio(
			Double imp_isrRetenido_acomEjercicio) {
		this.imp_isrRetenido_acomEjercicio = imp_isrRetenido_acomEjercicio;
	}
	public Double getImporte_netoPago_acomEjercicio() {
		return importe_netoPago_acomEjercicio;
	}
	public void setImporte_netoPago_acomEjercicio(
			Double importe_netoPago_acomEjercicio) {
		this.importe_netoPago_acomEjercicio = importe_netoPago_acomEjercicio;
	}
	
	
	
	
}