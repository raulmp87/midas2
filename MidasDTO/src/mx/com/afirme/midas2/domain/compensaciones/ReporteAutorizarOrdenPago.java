package mx.com.afirme.midas2.domain.compensaciones;

public class ReporteAutorizarOrdenPago implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String gerencia;
	private String promotoria;
    private Long   idBeneficiario;
	private String nombreBeneficiario;
	private String tipoEntidad;
	private Double importeTotal;
	private String nombreBanco;
    private String noCuenta;
    private Double sumatoria;
    
	public String getGerencia() {
		return gerencia;
	}
	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}
	public String getPromotoria() {
		return promotoria;
	}
	public void setPromotoria(String promotoria) {
		this.promotoria = promotoria;
	}
	public Long getIdBeneficiario() {
		return idBeneficiario;
	}
	public void setIdBeneficiario(Long idBeneficiario) {
		this.idBeneficiario = idBeneficiario;
	}
	public String getNombreBeneficiario() {
		return nombreBeneficiario;
	}
	public void setNombreBeneficiario(String nombreBeneficiario) {
		this.nombreBeneficiario = nombreBeneficiario;
	}
	public String getTipoEntidad() {
		return tipoEntidad;
	}
	public void setTipoEntidad(String tipoEntidad) {
		this.tipoEntidad = tipoEntidad;
	}
	public Double getImporteTotal() {
		return importeTotal;
	}
	public void setImporteTotal(Double importeTotal) {
		this.importeTotal = importeTotal;
	}
	public String getNombreBanco() {
		return nombreBanco;
	}
	public void setNombreBanco(String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}
	public String getNoCuenta() {
		return noCuenta;
	}
	public void setNoCuenta(String noCuenta) {
		this.noCuenta = noCuenta;
	}
	public Double getSumatoria() {
		return sumatoria;
	}
	public void setSumatoria(Double sumatoria) {
		this.sumatoria = sumatoria;
	}
    }
