package mx.com.afirme.midas2.domain.compensaciones;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.EnvioFacturaAgente;

@Entity
@Table(name="CA_ENVIOXMLAGENTESCOMP",schema="MIDAS")

public class CaEnvioXml  implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 5385628445097655292L;
	// Fields
	 
     private Long idenvioca;
     private Long idenvio;
     private Long tipomsj;
     private String mensaje;
     private Long identificadorBenId;
     private Long liquidacionId;
     private String tipoMensaje;
     private EnvioFacturaAgente envioFactura;

    // Constructors
	/** default constructor */
    public CaEnvioXml() {
    }
    
    /** full constructor */
    public CaEnvioXml(Long idenvioca, Long idenvio, Long tipomsj, String mensaje, Long identificadorBenId, Long liquidacionId) {
        this.idenvioca = idenvioca;
        this.idenvio = idenvio;
        this.tipomsj = tipomsj;
        this.mensaje = mensaje;
        this.identificadorBenId = identificadorBenId;
        this.liquidacionId = liquidacionId;
    }
   
    // Property accessors
    @Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CATRNEGVIDAPROD_ID_SEQ")
	@SequenceGenerator(name="CA_ENVIOXMLAGENTESCOMP_ID_SEQ", schema="MIDAS",sequenceName="CA_ENVIOXMLAGENTESCOMP_ID_SEQ",allocationSize=1)
    @Column(name="IDENVIOCA", precision=0)
    public Long getIdenvioca() {
        return this.idenvioca;
    }
    
    public void setIdenvioca(Long idenvioca) {
        this.idenvioca = idenvioca;
    }
    @Transient
    public Long getIdenvio() {
        return this.idenvio;
    }
    
    public void setIdenvio(Long idenvio) {
        this.idenvio = idenvio;
    }

    @Column(name="TIPOMSJ", precision=0)
    public Long getTipomsj() {
        return this.tipomsj;
    }
    
    public void setTipomsj(Long tipomsj) {
        this.tipomsj = tipomsj;
    }

    @Column(name="MENSAJE")
    public String getMensaje() {
        return this.mensaje;
    }
    
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Column(name="IDENTIFICADOR_BEN_ID", precision=0)
    public Long getIdentificadorBenId() {
        return this.identificadorBenId;
    }
    
    public void setIdentificadorBenId(Long identificadorBenId) {
        this.identificadorBenId = identificadorBenId;
    }

    @Column(name="LIQUIDACION_ID", precision=0)
    public Long getLiquidacionId() {
        return this.liquidacionId;
    }
    
    public void setLiquidacionId(Long liquidacionId) {
        this.liquidacionId = liquidacionId;
    }


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof CaEnvioXml) ) return false;
		 CaEnvioXml castOther = ( CaEnvioXml ) other; 
         
		 return ( (this.getIdenvioca()==castOther.getIdenvioca()) || ( this.getIdenvioca()!=null && castOther.getIdenvioca()!=null && this.getIdenvioca().equals(castOther.getIdenvioca()) ) )
 && ( (this.getIdenvio()==castOther.getIdenvio()) || ( this.getIdenvio()!=null && castOther.getIdenvio()!=null && this.getIdenvio().equals(castOther.getIdenvio()) ) )
 && ( (this.getTipomsj()==castOther.getTipomsj()) || ( this.getTipomsj()!=null && castOther.getTipomsj()!=null && this.getTipomsj().equals(castOther.getTipomsj()) ) )
 && ( (this.getMensaje()==castOther.getMensaje()) || ( this.getMensaje()!=null && castOther.getMensaje()!=null && this.getMensaje().equals(castOther.getMensaje()) ) )
 && ( (this.getIdentificadorBenId()==castOther.getIdentificadorBenId()) || ( this.getIdentificadorBenId()!=null && castOther.getIdentificadorBenId()!=null && this.getIdentificadorBenId().equals(castOther.getIdentificadorBenId()) ) )
 && ( (this.getLiquidacionId()==castOther.getLiquidacionId()) || ( this.getLiquidacionId()!=null && castOther.getLiquidacionId()!=null && this.getLiquidacionId().equals(castOther.getLiquidacionId()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdenvioca() == null ? 0 : this.getIdenvioca().hashCode() );
         result = 37 * result + ( getIdenvio() == null ? 0 : this.getIdenvio().hashCode() );
         result = 37 * result + ( getTipomsj() == null ? 0 : this.getTipomsj().hashCode() );
         result = 37 * result + ( getMensaje() == null ? 0 : this.getMensaje().hashCode() );
         result = 37 * result + ( getIdentificadorBenId() == null ? 0 : this.getIdentificadorBenId().hashCode() );
         result = 37 * result + ( getLiquidacionId() == null ? 0 : this.getLiquidacionId().hashCode() );
         return result;
   }

   @Transient
	public String getTipoMensaje() {
		return tipoMensaje;
	}
	   
	public void setTipoMensaje(String tipoMensaje) {
		this.tipoMensaje = tipoMensaje;
	}

	@JoinColumn(name = "IDENVIO")
	public EnvioFacturaAgente getEnvioFactura() {
		return envioFactura;
	}

	public void setEnvioFactura(EnvioFacturaAgente envioFactura) {
		this.envioFactura = envioFactura;
	}   

}