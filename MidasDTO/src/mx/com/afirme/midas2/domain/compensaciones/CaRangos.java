package mx.com.afirme.midas2.domain.compensaciones;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="CA_RANGOS"
    ,schema="MIDAS"
)

public class CaRangos  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
     private Long id;
     private CaParametros caParametros;
     private CaTipoRango caTipoRango;
     private String nombre;
     private Long valorCompensacion;
     private BigDecimal valorMinimo;
     private BigDecimal valorMaximo;
     private Date fechaCreacion;
     private Date fechaModificacion;
     private String usuario;
     private Boolean borradoLogico;


    public CaRangos() {
    }


    public CaRangos(Long id) {
        this.id = id;
    }
    
    public CaRangos(Long id, CaParametros caParametros,
			CaTipoRango caTipoRango, String nombre, Long valorcompensacion,
			BigDecimal valorMinimo, BigDecimal valorMaximo, Date fechaCreacion,
			Date fechaModificacion, String usuario, Boolean borradoLogico) {
		super();
		this.id = id;
		this.caParametros = caParametros;
		this.caTipoRango = caTipoRango;
		this.nombre = nombre;
		this.valorCompensacion = valorcompensacion;
		this.valorMinimo = valorMinimo;
		this.valorMaximo = valorMaximo;
		this.fechaCreacion = fechaCreacion;
		this.fechaModificacion = fechaModificacion;
		this.usuario = usuario;
		this.borradoLogico = borradoLogico;
	}


	@Id 
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CARANGOSCA_ID_SEQ")
	@SequenceGenerator(name = "CARANGOSCA_ID_SEQ",  schema="MIDAS", sequenceName = "CARANGOSCA_ID_SEQ", allocationSize = 1)
    @Column(name="ID", unique=true, nullable=false, precision=10, scale=0)

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="PARAMETROS_ID")

    public CaParametros getCaParametros() {
        return this.caParametros;
    }
    
    public void setCaParametros(CaParametros caParametros) {
        this.caParametros = caParametros;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="TIPORANGO_ID")

    public CaTipoRango getCaTipoRango() {
        return this.caTipoRango;
    }
    
    public void setCaTipoRango(CaTipoRango caTipoRango) {
        this.caTipoRango = caTipoRango;
    }
    
    @Column(name="NOMBRE")

    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    @Column(name="VALORMINIMO")

    public BigDecimal getValorMinimo() {
        return this.valorMinimo;
    }
    
    public void setValorMinimo(BigDecimal valorMinimo) {
        this.valorMinimo = valorMinimo;
    }
    
    @Column(name="VALORMAXIMO")

    public BigDecimal getValorMaximo() {
        return this.valorMaximo;
    }
    
    public void setValorMaximo(BigDecimal valorMaximo) {
        this.valorMaximo = valorMaximo;
    }
    
    @Temporal(TemporalType.DATE)
    @Column(name="FECHACREACION", length=7)

    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }
    
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAMODIFICACION", length=7)

    public Date getFechamodificacion() {
        return this.fechaModificacion;
    }
    
    public void setFechamodificacion(Date fechamodificacion) {
        this.fechaModificacion = fechamodificacion;
    }
    
    @Column(name="USUARIO")

    public String getUsuario() {
        return this.usuario;
    }
    
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    @Column(name="BORRADOLOGICO", precision=1, scale=0)

    public Boolean getBorradoLogico() {
        return this.borradoLogico;
    }
    
    public void setBorradoLogico(Boolean borradoLogico) {
        this.borradoLogico = borradoLogico;
    }
    
    @Column(name="VALORCOMPENSACION")

    public Long getValorCompensacion() {
        return this.valorCompensacion;
    }
    
    public void setValorCompensacion(Long valorcompensacion) {
        this.valorCompensacion = valorcompensacion;
    }
    
}