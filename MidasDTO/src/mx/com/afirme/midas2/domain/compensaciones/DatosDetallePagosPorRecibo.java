package mx.com.afirme.midas2.domain.compensaciones;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class DatosDetallePagosPorRecibo implements Serializable {
	private static final long serialVersionUID = 1L;

	private String baseCalculo;

	private BigDecimal clave;
	private BigDecimal compensacionId;

	private BigDecimal endoso;

	private Date fechaAplicacionPago;

	private BigDecimal folio;

	private BigDecimal impCumlimientoMeta;

	private BigDecimal importeBs;

	private BigDecimal importeDerpol;

	private BigDecimal importePrima;

	private BigDecimal importeTotal;

	private BigDecimal importeUtilidad;

	private BigDecimal isr;

	private BigDecimal iva;

	private BigDecimal ivaRetenido;

	private String nombreBeneficiario;

	private String poliza;

	private BigDecimal porcComDerpol;

	private BigDecimal porcCumplimientoMeta;

	private BigDecimal porcUtilidad;

	private BigDecimal porcentajeBajaSin;

	private BigDecimal porcentajecompprima;

	private String ramo;

	private BigDecimal recibo;

	private BigDecimal subtotal;

	private String tipoBeneficiario;

	private String tipoPago;

	public DatosDetallePagosPorRecibo() {
	}

	public String getBaseCalculo() {
		return this.baseCalculo;
	}

	public void setBaseCalculo(String baseCalculo) {
		this.baseCalculo = baseCalculo;
	}

	public BigDecimal getClave() {
		return this.clave;
	}

	public void setClave(BigDecimal clave) {
		this.clave = clave;
	}

	public BigDecimal getCompensacionId() {
		return this.compensacionId;
	}

	public void setCompensacionId(BigDecimal compensacionId) {
		this.compensacionId = compensacionId;
	}

	public BigDecimal getEndoso() {
		return this.endoso;
	}

	public void setEndoso(BigDecimal endoso) {
		this.endoso = endoso;
	}

	public Date getFechaAplicacionPago() {
		return this.fechaAplicacionPago;
	}

	public void setFechaAplicacionPago(Date fechaAplicacionPago) {
		this.fechaAplicacionPago = fechaAplicacionPago;
	}

	public BigDecimal getFolio() {
		return this.folio;
	}

	public void setFolio(BigDecimal folio) {
		this.folio = folio;
	}

	public BigDecimal getImpCumlimientoMeta() {
		return this.impCumlimientoMeta;
	}

	public void setImpCumlimientoMeta(BigDecimal impCumlimientoMeta) {
		this.impCumlimientoMeta = impCumlimientoMeta;
	}

	public BigDecimal getImporteBs() {
		return this.importeBs;
	}

	public void setImporteBs(BigDecimal importeBs) {
		this.importeBs = importeBs;
	}

	public BigDecimal getImporteDerpol() {
		return this.importeDerpol;
	}

	public void setImporteDerpol(BigDecimal importeDerpol) {
		this.importeDerpol = importeDerpol;
	}

	public BigDecimal getImportePrima() {
		return this.importePrima;
	}

	public void setImportePrima(BigDecimal importePrima) {
		this.importePrima = importePrima;
	}

	public BigDecimal getImporteTotal() {
		return this.importeTotal;
	}

	public void setImporteTotal(BigDecimal importeTotal) {
		this.importeTotal = importeTotal;
	}

	public BigDecimal getImporteUtilidad() {
		return this.importeUtilidad;
	}

	public void setImporteUtilidad(BigDecimal importeUtilidad) {
		this.importeUtilidad = importeUtilidad;
	}

	public BigDecimal getIsr() {
		return this.isr;
	}

	public void setIsr(BigDecimal isr) {
		this.isr = isr;
	}

	public BigDecimal getIva() {
		return this.iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public BigDecimal getIvaRetenido() {
		return this.ivaRetenido;
	}

	public void setIvaRetenido(BigDecimal ivaRetenido) {
		this.ivaRetenido = ivaRetenido;
	}

	public String getNombreBeneficiario() {
		return this.nombreBeneficiario;
	}

	public void setNombreBeneficiario(String nombreBeneficiario) {
		this.nombreBeneficiario = nombreBeneficiario;
	}

	public String getPoliza() {
		return this.poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	public BigDecimal getPorcComDerpol() {
		return this.porcComDerpol;
	}

	public void setPorcComDerpol(BigDecimal porcComDerpol) {
		this.porcComDerpol = porcComDerpol;
	}

	public BigDecimal getPorcCumplimientoMeta() {
		return this.porcCumplimientoMeta;
	}

	public void setPorcCumplimientoMeta(BigDecimal porcCumplimientoMeta) {
		this.porcCumplimientoMeta = porcCumplimientoMeta;
	}

	public BigDecimal getPorcUtilidad() {
		return this.porcUtilidad;
	}

	public void setPorcUtilidad(BigDecimal porcUtilidad) {
		this.porcUtilidad = porcUtilidad;
	}

	public BigDecimal getPorcentajeBajaSin() {
		return this.porcentajeBajaSin;
	}

	public void setPorcentajeBajaSin(BigDecimal porcentajeBajaSin) {
		this.porcentajeBajaSin = porcentajeBajaSin;
	}

	public BigDecimal getPorcentajecompprima() {
		return this.porcentajecompprima;
	}

	public void setPorcentajecompprima(BigDecimal porcentajecompprima) {
		this.porcentajecompprima = porcentajecompprima;
	}

	public String getRamo() {
		return this.ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}

	public BigDecimal getRecibo() {
		return this.recibo;
	}

	public void setRecibo(BigDecimal recibo) {
		this.recibo = recibo;
	}

	public BigDecimal getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public String getTipoBeneficiario() {
		return this.tipoBeneficiario;
	}

	public void setTipoBeneficiario(String tipoBeneficiario) {
		this.tipoBeneficiario = tipoBeneficiario;
	}

	public String getTipoPago() {
		return this.tipoPago;
	}

	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}

}