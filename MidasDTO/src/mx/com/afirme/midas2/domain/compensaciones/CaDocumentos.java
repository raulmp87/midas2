package mx.com.afirme.midas2.domain.compensaciones;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="CA_DOCUMENTOS"
    ,schema="MIDAS"
)

public class CaDocumentos  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
     private Long id;
     private CaTipoDocumento caTipoDocumento;
     private CaEntidadPersona caEntidadPersona;
     private CaCompensacion caCompensacion;
     private String nombre;
     private String valor;
     private Date fechaCreacion;
     private Date fechaModificacion;
     private String usuario;
     private Boolean borradoLogico;


    public CaDocumentos() {
    }


    public CaDocumentos(Long id) {
        this.id = id;
    }


    public CaDocumentos(Long id, CaTipoDocumento tipodocumentoca, CaEntidadPersona caEntidadPersona, String nombre, String valor, Date fechaCreacion, Date fechaModificacion, String usuario, Boolean borradoLogico) {
        this.id = id;
        this.caTipoDocumento = tipodocumentoca;
        this.caEntidadPersona = caEntidadPersona;
        this.nombre = nombre;
        this.valor = valor;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.usuario = usuario;
        this.borradoLogico = borradoLogico;
    }


    @Id 
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CADOCUMENTOS_ID_SEQ")
	@SequenceGenerator(name = "CADOCUMENTOS_ID_SEQ",  schema="MIDAS", sequenceName = "CADOCUMENTOS_ID_SEQ", allocationSize = 1)
    @Column(name="ID", unique=true, nullable=false, precision=10, scale=0)

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="TIPODOCUMENTO_ID")

    public CaTipoDocumento getCaTipoDocumento() {
        return this.caTipoDocumento;
    }
    
    public void setCaTipoDocumento(CaTipoDocumento tipodocumentoca) {
        this.caTipoDocumento = tipodocumentoca;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="ENTIDADPERSONA_ID")

    public CaEntidadPersona getCaEntidadPersona() {
        return this.caEntidadPersona;
    }
    
    public void setCaEntidadPersona(CaEntidadPersona caEntidadPersona) {
        this.caEntidadPersona = caEntidadPersona;
    }
    
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="COMPENSACION_ID")
    public CaCompensacion getCaCompensacion() {
		return caCompensacion;
	}

	public void setCaCompensacion(CaCompensacion caCompensacion) {
		this.caCompensacion = caCompensacion;
	}


	@Column(name="NOMBRE")

    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    @Column(name="VALOR")

    public String getValor() {
        return this.valor;
    }
    
    public void setValor(String valor) {
        this.valor = valor;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHACREACION", length=7)

    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }
    
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAMODIFICACION", length=7)

    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }
    
    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
    
    @Column(name="USUARIO")

    public String getUsuario() {
        return this.usuario;
    }
    
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    @Column(name="BORRADOLOGICO", precision=1, scale=0)

    public Boolean getBorradoLogico() {
        return this.borradoLogico;
    }
    
    public void setBorradoLogico(Boolean borradoLogico) {
        this.borradoLogico = borradoLogico;
    }
    
}