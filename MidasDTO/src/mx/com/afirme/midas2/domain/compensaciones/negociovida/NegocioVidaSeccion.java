package mx.com.afirme.midas2.domain.compensaciones.negociovida;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.emision.ppct.LineaNegocioSeycos;

/**
 * NegocioVidaSeccion entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CA_TRNEGOCIOVIDA_SECCION", schema = "MIDAS")
public class NegocioVidaSeccion implements java.io.Serializable,Entidad  {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 6433901330855738288L;
	private Long id;
	private NegocioVida negocioVida;
	private LineaNegocioSeycos seccion;

	// Constructors

	/** default constructor */
	public NegocioVidaSeccion() {
	}

	/** minimal constructor */
	public NegocioVidaSeccion(Long id) {
		this.id = id;
	}

	/** full constructor */
	public NegocioVidaSeccion(Long id, NegocioVida negocioVida,
			LineaNegocioSeycos seccion) {
		this.id = id;
		this.negocioVida = negocioVida;
		this.seccion = seccion;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CATRNEGVIDASEC_ID_SEQ")
	@SequenceGenerator(name="CATRNEGVIDASEC_ID_SEQ", schema="MIDAS",sequenceName="CATRNEGVIDASEC_ID_SEQ",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "NEGOCIOVIDA_ID")
	public NegocioVida getNegocioVida() {
		return this.negocioVida;
	}

	public void setNegocioVida(NegocioVida negocioVida) {
		this.negocioVida = negocioVida;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SECCION_ID")
	public LineaNegocioSeycos getSeccion() {
		return this.seccion;
	}

	public void setSeccion(LineaNegocioSeycos seccion) {
		this.seccion = seccion;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long  getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

}