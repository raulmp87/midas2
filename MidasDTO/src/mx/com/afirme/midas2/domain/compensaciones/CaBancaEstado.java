package mx.com.afirme.midas2.domain.compensaciones;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="CA_BANCA_ESTADO",schema="MIDAS")

public class CaBancaEstado {
	

	private Long id;
	private Long configuracionBancaId;
	private String estadoId;
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Id 
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CABANCAESTADO_ID_SEQ")
	@SequenceGenerator(name = "CABANCAESTADO_ID_SEQ",  schema="MIDAS", sequenceName = "CABANCAESTADO_ID_SEQ", allocationSize = 1)
    @Column(name="ID", unique=true, nullable=false, precision=10, scale=0)
	public Long getId() {
		return id;
	}
	
	public void setConfiguracionBancaId(Long configuracionBancaId) {
		this.configuracionBancaId = configuracionBancaId;
	}

	@Column(name="CABANCACONFIGURACION_ID")
	public Long getConfiguracionBancaId() {
		return configuracionBancaId;
	}

	public void setEstadoId(String estadoId) {
		this.estadoId = estadoId;
	}
	
	@Column(name="ESTADO_ID", nullable=false, length=6)
	public String getEstadoId() {
		return estadoId;
	}
	
}
