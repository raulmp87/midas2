package mx.com.afirme.midas2.domain.compensaciones;


import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.domain.compensaciones.CaRamo;

/**
 * CaCorreos entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CA_CORREOS", schema = "MIDAS")
public class CaCorreos implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal id;
	private CaRamo caRamo;
	private Long tipoAutorizacionId;
	private String oficina;
	private String username;

	// Constructors

	/** default constructor */
	public CaCorreos() {
	}

	/** minimal constructor */
	public CaCorreos(BigDecimal id) {
		this.id = id;
	}

	/** full constructor */
	public CaCorreos(BigDecimal id, CaRamo caRamo, Long tipoAutorizacionId, String oficina,
			String username) {
		this.id = id;
		this.caRamo = caRamo;
		this.tipoAutorizacionId = tipoAutorizacionId;
		this.oficina = oficina;
		this.username = username;
	}
	
	// Property accessors
	@Id
	@Column(name = "ID", unique = true, nullable = false, precision = 32, scale = 0)
	public BigDecimal getId() {
		return this.id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "RAMO_ID")
	public CaRamo getCaRamo() {
		return this.caRamo;
	}

	public void setCaRamo(CaRamo caRamo) {
		this.caRamo = caRamo;
	}

	public Long getTipoAutorizacionId() {
		return tipoAutorizacionId;
	}

	public void setTipoAutorizacionId(Long tipoAutorizacionId) {
		this.tipoAutorizacionId = tipoAutorizacionId;
	}

	@Column(name = "OFICINA", length = 100)
	public String getOficina() {
		return this.oficina;
	}

	public void setOficina(String oficina) {
		this.oficina = oficina;
	}

	@Column(name = "USERNAME", length = 10)
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}