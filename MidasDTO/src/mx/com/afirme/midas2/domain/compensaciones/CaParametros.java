package mx.com.afirme.midas2.domain.compensaciones;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="CA_PARAMETROS"
    ,schema="MIDAS"
)

public class CaParametros  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	private Long id;
	private CaTipoMoneda caTipoMoneda;
	private CaTipoCompensacion caTipoCompensacion;     
	private CaTipoCondicionCalculo caTipoCondicionCalculo;
	private CaBaseCalculo caBaseCalculo;
	private Boolean emisionInterna;
	private Boolean emisionExterna;
	private CaTipoProvision caTipoProvision;
	private CaCompensacion caCompensacion;   
	private CaEntidadPersona caEntidadPersona;
	private BigDecimal montoUtilidades;
	private BigDecimal porcentajePago;
	private BigDecimal montoPago;
	private BigDecimal porcentajeAgenteProveedor;
	private BigDecimal porcentajePromotor;
	private Boolean parametroActivo;
	private BigDecimal topeMaximo;
	private Date fechaCreacion;
	private Date fechaModificacion;
	private String usuario;
	private Boolean borradoLogico;
	private Boolean comisionAgente;
	private CaFrecuenciaPagos caFrencuenciaPagos;
	private BigDecimal bonoFijoVida;
	private String presupuestoAnual;
	private Boolean iva;

    public CaParametros() {
    }


    public CaParametros(Long id) {
        this.id = id;
    }

	public CaParametros(Long id, CaTipoMoneda caTipoMoneda,
			CaTipoCompensacion caTipoCompensacion,
			CaTipoCondicionCalculo caTipoCondicionCalculo,
			CaBaseCalculo caBaseCalculo, Boolean emisioninterna,
			Boolean emisionexterna, CaTipoProvision caTipoProvision,
			CaCompensacion caCompensacion, CaEntidadPersona entidadPersona, BigDecimal montoUtilidades,
			BigDecimal porcentajePago, BigDecimal montoPago,
			BigDecimal porcentajeAgenteProveedor,
			BigDecimal porcentajePromotor, Boolean parametroactivo,
			BigDecimal topemaximo, Date fechaCreacion, Date fechaModificacion,
			String usuario, Boolean borradoLogico) {
		super();
		this.id = id;
		this.caTipoMoneda = caTipoMoneda;
		this.caTipoCompensacion = caTipoCompensacion;
		this.caTipoCondicionCalculo = caTipoCondicionCalculo;
		this.caBaseCalculo = caBaseCalculo;
		this.emisionInterna = emisioninterna;
		this.emisionExterna = emisionexterna;
		this.caTipoProvision = caTipoProvision;
		this.caCompensacion = caCompensacion;
		this.caEntidadPersona = entidadPersona;
		this.montoUtilidades = montoUtilidades;
		this.porcentajePago = porcentajePago;
		this.montoPago = montoPago;
		this.porcentajeAgenteProveedor = porcentajeAgenteProveedor;
		this.porcentajePromotor = porcentajePromotor;
		this.parametroActivo = parametroactivo;
		this.topeMaximo = topemaximo;
		this.fechaCreacion = fechaCreacion;
		this.fechaModificacion = fechaModificacion;
		this.usuario = usuario;
		this.borradoLogico = borradoLogico;
	}


	@Id 
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CAPARAMETROS_ID_SEQ")
	@SequenceGenerator(name = "CAPARAMETROS_ID_SEQ",  schema="MIDAS", sequenceName = "CAPARAMETROS_ID_SEQ", allocationSize = 1)
    @Column(name="ID", unique=true, nullable=false, precision=10, scale=0)

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="TIPOCOMPENSACION_ID")

    public CaTipoCompensacion getCaTipoCompensacion() {
        return this.caTipoCompensacion;
    }
    
    public void setCaTipoCompensacion(CaTipoCompensacion caTipoCompensacion) {
        this.caTipoCompensacion = caTipoCompensacion;
    }	
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="BASECALCULO_ID")

    public CaBaseCalculo getCaBaseCalculo() {
        return this.caBaseCalculo;
    }
    
    public void setCaBaseCalculo(CaBaseCalculo caBaseCalculo) {
        this.caBaseCalculo = caBaseCalculo;
    }

	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="COMPENSACION_ID")

    public CaCompensacion getCaCompensacion() {
        return this.caCompensacion;
    }
    
    public void setCaCompensacion(CaCompensacion caCompensacion) {
        this.caCompensacion = caCompensacion;
    }
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="ENTIDADPERSONA_ID")
    public CaEntidadPersona getCaEntidadPersona() {
		return caEntidadPersona;
	}

	public void setCaEntidadPersona(CaEntidadPersona caEntidadPersona) {
		this.caEntidadPersona = caEntidadPersona;
	}

	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="TIPOMONEDA_ID")
    public CaTipoMoneda getCaTipoMoneda() {
		return this.caTipoMoneda;
	}

	public void setCaTipoMoneda(CaTipoMoneda caTipoMoneda) {
		this.caTipoMoneda = caTipoMoneda;
	}

	@Column(name="MONTOUTILIDADES")

    public BigDecimal getMontoUtilidades() {
        return this.montoUtilidades;
    }
    
    public void setMontoUtilidades(BigDecimal montoUtilidades) {
        this.montoUtilidades = montoUtilidades;
    }
    
    @Column(name="PORCENTAJEPAGO")

    public BigDecimal getPorcentajePago() {
        return this.porcentajePago;
    }
    
    public void setPorcentajePago(BigDecimal porcentajePago) {
        this.porcentajePago = porcentajePago;
    }
    
    @Column(name="MONTOPAGO")

    public BigDecimal getMontoPago() {
        return this.montoPago;
    }
    
    public void setMontoPago(BigDecimal montoPago) {
        this.montoPago = montoPago;
    }
    
    @Column(name="PORCENTAJEAGENTE")

    public BigDecimal getPorcentajeAgenteProveedor() {
        return this.porcentajeAgenteProveedor;
    }
    
    public void setPorcentajeAgenteProveedor(BigDecimal porcentajeAgenteProveedor) {
        this.porcentajeAgenteProveedor = porcentajeAgenteProveedor;
    }
    
    @Column(name="PORCENTAJEPROMOTOR")

    public BigDecimal getPorcentajePromotor() {
        return this.porcentajePromotor;
    }
    
    public void setPorcentajePromotor(BigDecimal porcentajePromotor) {
        this.porcentajePromotor = porcentajePromotor;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHACREACION", length=7)

    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }
    
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAMODIFICACION", length=7)

    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }
    
    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
    
    @Column(name="USUARIO")

    public String getUsuario() {
        return this.usuario;
    }
    
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    @Column(name="BORRADOLOGICO", precision=1, scale=0)

    public Boolean getBorradoLogico() {
        return this.borradoLogico;
    }
    
    public void setBorradoLogico(Boolean borradoLogico) {
        this.borradoLogico = borradoLogico;
    }

	public BigDecimal getTopeMaximo() {
		return topeMaximo;
	}


	public void setTopeMaximo(BigDecimal topemaximo) {
		this.topeMaximo = topemaximo;
	}

	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="TIPOCONDICIONESCALCULO_ID")
	public CaTipoCondicionCalculo getCaTipoCondicionCalculo() {
		return caTipoCondicionCalculo;
	}


	public void setCaTipoCondicionCalculo(CaTipoCondicionCalculo caTipoCondicionCalculo) {
		this.caTipoCondicionCalculo = caTipoCondicionCalculo;
	}

	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="TIPOPROVISION_ID")
	public CaTipoProvision getCaTipoProvision() {
		return caTipoProvision;
	}

	public void setCaTipoProvision(CaTipoProvision caTipoProvision) {
		this.caTipoProvision = caTipoProvision;
	}
	@Column(name="EMISIONINTERNA", precision=1, scale=0)

    public Boolean getEmisionInterna() {
        return this.emisionInterna;
    }
    
    public void setEmisionInterna(Boolean emisioninterna) {
        this.emisionInterna = emisioninterna;
    }
    
    @Column(name="EMISIONEXTERNA", precision=1, scale=0)

    public Boolean getEmisionExterna() {
        return this.emisionExterna;
    }
    
    public void setEmisionExterna(Boolean emisionexterna) {
        this.emisionExterna = emisionexterna;
    }
    @Column(name="PARAMETROACTIVO", precision=1, scale=0)

    public Boolean getParametroActivo() {
        return this.parametroActivo;
    }
    
    public void setParametroActivo(Boolean parametroactivo) {
        this.parametroActivo = parametroactivo;
    }

    @Column(name="COMISIONAGENTE", precision=1, scale=0)
	public Boolean getComisionAgente() {
		return comisionAgente;
	}
    
	public void setComisionAgente(Boolean comisionAgente) {
		this.comisionAgente = comisionAgente;
	}

	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="FRECUENCIAPAGOS_ID")
	public CaFrecuenciaPagos getCaFrencuenciaPagos() {
		return caFrencuenciaPagos;
	}
	
	public void setCaFrencuenciaPagos(CaFrecuenciaPagos caFrencuenciaPagos) {
		this.caFrencuenciaPagos = caFrencuenciaPagos;
	}


	public void setBonoFijoVida(BigDecimal bonoFijoVida) {
		this.bonoFijoVida = bonoFijoVida;
	}

	@Column(name="BONOFIJOVIDA")
	public BigDecimal getBonoFijoVida() {
		return bonoFijoVida;
	}


	public void setPresupuestoAnual(String presupuestoAnual) {
		this.presupuestoAnual = presupuestoAnual;
	}

    @Column(name="PRESUPUESTOANUAL")
	public String getPresupuestoAnual() {
		return presupuestoAnual;
	}

	@Column(name="IVA", precision=1, scale=0)
	public Boolean getIva() {
		return iva;
	}

	public void setIva(Boolean iva) {
		this.iva = iva;
	}

}