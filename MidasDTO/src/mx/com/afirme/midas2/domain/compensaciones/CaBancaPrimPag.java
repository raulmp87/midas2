package mx.com.afirme.midas2.domain.compensaciones;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CA_BANCA_PRIMPAG",schema="MIDAS")
public class CaBancaPrimPag implements java.io.Serializable{

	
	  	 private Long id;
	     private String ofsuc;
	     private String nomCanal;
	     private Long codCanal;
	     private String canalNvo;
	     private String nomGrupo;
	     private String agte;
	     private Long idAgente;
	     private String nomAgte;
	     private String nomEjec;
	     private String nomRamo;
	     private String idCentroEmision;
	     private String numPoliza;
	     private String numRenovPoliz;
	     private String codigoRamo;
	     private String mon;
	     private String asegurado;
	     private String fecEmi;
	     private String vigDes;
	     private String vigHas;
	     private String lineaNeg;
	     private Double pmaEmi;
	     private Double pmaPagada;
	     private String ordimp;
	     private Double anio;
	     private Double mes;
	     private Long bancaPrimpagCpId;
    
    public CaBancaPrimPag(){
    }
    
    public CaBancaPrimPag(String ofsuc,String nomCanal,Long codCanal,String canalNvo,String nomGrupo,String agte,Long idAgente,
    		String nomAgte,String nomEjec,String nomRamo,String idCentroEmision,String numPoliza,String numRenovPoliz,String codigoRamo,
    	     String mon,String asegurado,String fecEmi,String vigDes,String vigHas,String lineaNeg,Double pmaEmi,Double pmaPagada,
    	     String ordimp,Double anio,Double mes,Long bancaPrimpagCpId){
    	this.ofsuc = ofsuc;
    	this.nomCanal = nomCanal;
    	this.codCanal = codCanal;
    	this.canalNvo = canalNvo;
    	this.nomGrupo = nomGrupo;
    	this.agte = agte;
    	this.idAgente = idAgente;
    	this.nomAgte= nomAgte;
    	this.nomEjec = nomEjec;
    	this.nomRamo = nomRamo;
    	this.idCentroEmision = idCentroEmision;
    	this.numPoliza = numPoliza;
    	this.numRenovPoliz = numRenovPoliz;
    	this.codigoRamo= codigoRamo;
    	this.mon = mon;
    	this.asegurado = asegurado;
    	this.fecEmi = fecEmi;
    	this.vigDes = vigDes;
    	this.vigHas= vigHas;
    	this.lineaNeg = lineaNeg;
    	this.pmaEmi = pmaEmi;
    	this.pmaPagada = pmaPagada;
    	this.ordimp = ordimp;
    	this.anio = anio;
    	this.mes = mes;
    	this.bancaPrimpagCpId = bancaPrimpagCpId;
    	
    	
    	this.bancaPrimpagCpId = bancaPrimpagCpId;
    }
    
    @Id 
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CA_BANCA_PRIMPAG_ID_SEQ")
	@SequenceGenerator(name = "CA_BANCA_PRIMPAG_ID_SEQ",  schema="MIDAS", sequenceName = "CA_BANCA_PRIMPAG_ID_SEQ", allocationSize = 1)
    @Column(name="ID", unique=true)
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    @Column(name="OFSUC")

    public String getOfsuc() {
        return this.ofsuc;
    }
    
    public void setOfsuc(String ofsuc) {
        this.ofsuc = ofsuc;
    }
    
    @Column(name="NOM_CANAL")

    public String getNomCanal() {
        return this.nomCanal;
    }
    
    public void setNomCanal(String nomCanal) {
        this.nomCanal = nomCanal;
    }
    
    @Column(name="COD_CANAL")

    public Long getCodCanal() {
        return this.codCanal;
    }
    
    public void setCodCanal(Long codCanal) {
        this.codCanal = codCanal;
    }
    
    @Column(name="CANAL_NVO")

    public String getCanalNvo() {
        return this.canalNvo;
    }
    
    public void setCanalNvo(String canalNvo) {
        this.canalNvo = canalNvo;
    }
    
    @Column(name="NOM_GRUPO")

    public String getNomGrupo() {
        return this.nomGrupo;
    }
    
    public void setNomGrupo(String nomGrupo) {
        this.nomGrupo = nomGrupo;
    }
    
    @Column(name="AGTE")

    public String getAgte() {
        return this.agte;
    }
    
    public void setAgte(String agte) {
        this.agte = agte;
    }
    
    @Column(name="ID_AGENTE")

    public Long getIdAgente() {
        return this.idAgente;
    }
    
    public void setIdAgente(Long idAgente) {
        this.idAgente = idAgente;
    }
    
    @Column(name="NOM_AGTE")

    public String getNomAgte() {
        return this.nomAgte;
    }
    
    public void setNomAgte(String nomAgte) {
        this.nomAgte = nomAgte;
    }
    
    @Column(name="NOM_EJEC")

    public String getNomEjec() {
        return this.nomEjec;
    }
    
    public void setNomEjec(String nomEjec) {
        this.nomEjec = nomEjec;
    }
    
    @Column(name="NOM_RAMO")

    public String getNomRamo() {
        return this.nomRamo;
    }
    
    public void setNomRamo(String nomRamo) {
        this.nomRamo = nomRamo;
    }
    
    @Column(name="ID_CENTRO_EMISION")

    public String getIdCentroEmision() {
        return this.idCentroEmision;
    }
    
    public void setIdCentroEmision(String idCentroEmision) {
        this.idCentroEmision = idCentroEmision;
    }
    
    @Column(name="NUM_POLIZA")

    public String getNumPoliza() {
        return this.numPoliza;
    }
    
    public void setNumPoliza(String numPoliza) {
        this.numPoliza = numPoliza;
    }
    
    @Column(name="NUM_RENOV_POLIZ")

    public String getNumRenovPoliz() {
        return this.numRenovPoliz;
    }
    
    public void setNumRenovPoliz(String numRenovPoliz) {
        this.numRenovPoliz = numRenovPoliz;
    }
    
    @Column(name="CODIGORAMO")

   public String getCodigoRamo() {
		return codigoRamo;
	}

	public void setCodigoRamo(String codigoRamo) {
		this.codigoRamo = codigoRamo;
	}
    
    @Column(name="MON")

    public String getMon() {
        return this.mon;
    }
    
    public void setMon(String mon) {
        this.mon = mon;
    }
    
    @Column(name="ASEGURADO")

    public String getAsegurado() {
        return this.asegurado;
    }
    
    public void setAsegurado(String asegurado) {
        this.asegurado = asegurado;
    }
    
    @Column(name="FEC_EMI")
    public String getFecEmi() {
        return this.fecEmi;
    }
    
    public void setFecEmi(String fecEmi) {
        this.fecEmi = fecEmi;
    }
    
    @Column(name="VIG_DES")
    public String getVigDes() {
        return this.vigDes;
    }
    
    public void setVigDes(String vigDes) {
        this.vigDes = vigDes;
    }

    @Column(name="VIG_HAS")
    public String getVigHas() {
        return this.vigHas;
    }
    
    public void setVigHas(String vigHas) {
        this.vigHas = vigHas;
    }
    
    @Column(name="LINEA_NEG")

    public String getLineaNeg() {
        return this.lineaNeg;
    }
    
    public void setLineaNeg(String lineaNeg) {
        this.lineaNeg = lineaNeg;
    }
    
    @Column(name="PMA_EMI")

    public Double getPmaEmi() {
        return this.pmaEmi;
    }
    
    public void setPmaEmi(Double pmaEmi) {
        this.pmaEmi = pmaEmi;
    }
    
    @Column(name="PMA_PAGADA")

    public Double getPmaPagada() {
        return this.pmaPagada;
    }
    
    public void setPmaPagada(Double pmaPagada) {
        this.pmaPagada = pmaPagada;
    }
    
    @Column(name="ORDIMP")

    public String getOrdimp() {
        return this.ordimp;
    }
    
    public void setOrdimp(String ordimp) {
        this.ordimp = ordimp;
    }
    
    @Column(name="ANIO")

    public Double getAnio() {
        return this.anio;
    }
    
    public void setAnio(Double anio) {
        this.anio = anio;
    }
    
    @Column(name="MES")

    public Double getMes() {
        return this.mes;
    }
    
    public void setMes(Double mes) {
        this.mes = mes;
    }
    
    @Column(name="BANCA_PRIMPAG_CP_ID")

    public Long getBancaPrimpagCpId() {
        return this.bancaPrimpagCpId;
    }
    
    public void setBancaPrimpagCpId(Long bancaPrimpagCpId) {
        this.bancaPrimpagCpId = bancaPrimpagCpId;
    }

	

}
