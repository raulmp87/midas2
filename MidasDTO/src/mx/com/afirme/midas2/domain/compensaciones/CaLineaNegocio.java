package mx.com.afirme.midas2.domain.compensaciones;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;

@Entity
@Table(name="CA_LINEANEGOCIO"
    ,schema="MIDAS"
)

public class CaLineaNegocio  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
     private Long id;
     private CaParametros caParametros;
     private CaCompensacion caCompensacion;
     private Boolean contraprestacion;
     private Long negocioId;
     private SeccionDTO seccionDto;
     private String valorPorcentaje;
     private String valorId;
     private String valorDescripcion;
     private Date fechaCreacion;
     private Date fechaModificacion;
     private String usuario;
     private Boolean borradoLogico;

    public CaLineaNegocio() {
    }

    public CaLineaNegocio(Long id) {
        this.id = id;
    }

	public CaLineaNegocio(Long id, CaParametros caParametros,
			CaCompensacion caCompensacion, Boolean contraprestacion,
			Long negocioId, String valorporcentaje, String valorid,
			String valordescripcion, Date fechacreacion,
			Date fechamodificacion, String usuario, Boolean borradologico) {
		super();
		this.id = id;
		this.caParametros = caParametros;
		this.caCompensacion = caCompensacion;
		this.contraprestacion = contraprestacion;
		this.negocioId = negocioId;
		this.valorPorcentaje = valorporcentaje;
		this.valorId = valorid;
		this.valorDescripcion = valordescripcion;
		this.fechaCreacion = fechacreacion;
		this.fechaModificacion = fechamodificacion;
		this.usuario = usuario;
		this.borradoLogico = borradologico;
	}

	@Id 
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CALINEANEGOCIO_ID_SEQ")
	@SequenceGenerator(name = "CALINEANEGOCIO_ID_SEQ",  schema="MIDAS", sequenceName = "CALINEANEGOCIO_ID_SEQ", allocationSize = 1)
    @Column(name="ID", unique=true, nullable=false, precision=10, scale=0)

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="PARAMETROS_ID")

    public CaParametros getCaParametros() {
        return this.caParametros;
    }
    
    public void setCaParametros(CaParametros caParametros) {
        this.caParametros = caParametros;
    }

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="TOSECCION_ID")

	public SeccionDTO getSeccionDTO() {
	    return this.seccionDto;
	}
	
	public void setSeccionDTO(SeccionDTO seccionDto) {
	    this.seccionDto = seccionDto;
	}
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="COMPENSACION_ID")

    public CaCompensacion getCaCompensacion() {
        return this.caCompensacion;
    }
    
    public void setCaCompensacion(CaCompensacion caCompensacion) {
        this.caCompensacion = caCompensacion;
    }
    
    @Column(name="NEGOCIO_ID", precision=10, scale=0)

    public Long getNegocioId() {
        return this.negocioId;
    }
    
    public void setNegocioId(Long negocioId) {
        this.negocioId = negocioId;
    }
    
    @Column(name="VALORPORCENTAJE")

    public String getValorPorcentaje() {
        return this.valorPorcentaje;
    }
    
    public void setValorPorcentaje(String valorporcentaje) {
        this.valorPorcentaje = valorporcentaje;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHACREACION", length=7)

    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }
    
    public void setFechaCreacion(Date fechacreacion) {
        this.fechaCreacion = fechacreacion;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAMODIFICACION", length=7)

    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }
    
    public void setFechaModificacion(Date fechamodificacion) {
        this.fechaModificacion = fechamodificacion;
    }
    
    @Column(name="USUARIO")

    public String getUsuario() {
        return this.usuario;
    }
    
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    @Column(name="BORRADOLOGICO", precision=1, scale=0)

    public Boolean getBorradoLogico() {
        return this.borradoLogico;
    }
    
    public void setBorradoLogico(Boolean borradologico) {
        this.borradoLogico = borradologico;
    }
    @Column(name="VALORID")

    public String getValorId() {
        return this.valorId;
    }
    
    public void setValorId(String valorid) {
        this.valorId = valorid;
    }
    @Column(name="VALORDESCRIPCION")

    public String getValorDescripcion() {
        return this.valorDescripcion;
    }
    
    public void setValorDescripcion(String valordescripcion) {
        this.valorDescripcion = valordescripcion;
    }
    
    @Column(name="CONTRAPRESTACION", precision=1, scale=0)

    public Boolean getContraprestacion() {
        return this.contraprestacion;
    }
    
    public void setContraprestacion(Boolean contraprestacion) {
        this.contraprestacion = contraprestacion;
    }
    
}