package mx.com.afirme.midas2.domain.compensaciones;

import java.util.Date;
import java.util.List;

public class OrdenesPagoDTO implements java.io.Serializable {
	static final long serialVersionUID = 1L;
	private Long id;
	private Long folio;
	private String ramo;
	private String agente;
	private String promotor; 
	private String proveedor;
	private String contratante;
	private String gerencia;
	private String negocio;
	private String grupo;
	private String concepto;
	private Date fechaInicio;
	private Date fechaFin;
	private String estatus_op;
	private String estatus_op_gen;
	private String tipobeneficiario;
	private String status_factura;
	private Long identificador_ben_id;
	private String usuario;
	private String claveNombre;
	private Double importePrima;
	private Double importeBs;
	private Double importeDerpol;
	private Double importeCumMeta;
	private Double importeUtilidad;
	private Double importeTotal;
	
	///nuevos
	private String nombreContratante;
	private String estatusFactura;
	private String estatusOrdenpago;
	private String tipoEntidad;
	private Date fechaCreacion;
	private Integer solicitudCheque;
	
	private List<ReporteAutorizarOrdenPago> reporteAutorizarOrdenPagos;
    private Double sumatoria;
	
	public List<ReporteAutorizarOrdenPago> getReporteAutorizarOrdenPagos() {
		return reporteAutorizarOrdenPagos;
	}
	public void setReporteAutorizarOrdenPagos(
			List<ReporteAutorizarOrdenPago> reporteAutorizarOrdenPagos) {
		this.reporteAutorizarOrdenPagos = reporteAutorizarOrdenPagos;
	}
	public Double getSumatoria() {
		return sumatoria;
	}
	public void setSumatoria(Double sumatoria) {
		this.sumatoria = sumatoria;
	}
	public Long getId() {
		return id;
	}
	public String getRamo() {
		return ramo;
	}
	public String getAgente() {
		return agente;
	}
	public String getPromotor() {
		return promotor;
	}
	public String getProveedor() {
		return proveedor;
	}
	public String getContratante() {
		return contratante;
	}
	public String getGerencia() {
		return gerencia;
	}
	public String getNegocio() {
		return negocio;
	}
	public String getGrupo() {
		return grupo;
	}
	public String getConcepto() {
		return concepto;
	}
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setFolio(Long folio) {
		this.folio = folio;
	}
	public Long getFolio() {
		return folio;
	}
	public void setRamo(String ramo) {
		this.ramo = ramo;
	}
	public void setAgente(String agente) {
		this.agente = agente;
	}
	public void setPromotor(String promotor) {
		this.promotor = promotor;
	}
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}
	public void setContratante(String contratante) {
		this.contratante = contratante;
	}
	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}
	public void setNegocio(String negocio) {
		this.negocio = negocio;
	}
	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getEstatus_op() {
		return estatus_op;
	}
	public String getEstatus_op_gen() {
		return estatus_op_gen;
	}
	public String getTipobeneficiario() {
		return tipobeneficiario;
	}
	public String getStatus_factura() {
		return status_factura;
	}
	public void setEstatus_op(String estatus_op) {
		this.estatus_op = estatus_op;
	}
	public void setEstatus_op_gen(String estatus_op_gen) {
		this.estatus_op_gen = estatus_op_gen;
	}
	public void setTipobeneficiario(String tipobeneficiario) {
		this.tipobeneficiario = tipobeneficiario;
	}
	public void setStatus_factura(String status_factura) {
		this.status_factura = status_factura;
	}
	public Long getIdentificador_ben_id() {
		return identificador_ben_id;
	}
	public void setIdentificador_ben_id(Long identificador_ben_id) {
		this.identificador_ben_id = identificador_ben_id;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getClaveNombre() {
		return claveNombre;
	}
	public Double getImporteBs() {
		return importeBs;
	}
	public Double getImporteDerpol() {
		return importeDerpol;
	}
	public Double getImporteCumMeta() {
		return importeCumMeta;
	}
	public Double getImporteUtilidad() {
		return importeUtilidad;
	}
	public Double getImporteTotal() {
		return importeTotal;
	}
	public void setClaveNombre(String claveNombre) {
		this.claveNombre = claveNombre;
	}
	public void setImporteBs(Double importeBs) {
		this.importeBs = importeBs;
	}
	public void setImporteDerpol(Double importeDerpol) {
		this.importeDerpol = importeDerpol;
	}
	public void setImporteCumMeta(Double importeCumMeta) {
		this.importeCumMeta = importeCumMeta;
	}
	public void setImporteUtilidad(Double importeUtilidad) {
		this.importeUtilidad = importeUtilidad;
	}
	public void setImporteTotal(Double importeTotal) {
		this.importeTotal = importeTotal;
	}
	public Double getImportePrima() {
		return importePrima;
	}
	public void setImportePrima(Double importePrima) {
		this.importePrima = importePrima;
	}
	public String getNombreContratante() {
		return nombreContratante;
	}
	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}
	public String getEstatusFactura() {
		return estatusFactura;
	}
	public void setEstatusFactura(String estatusFactura) {
		this.estatusFactura = estatusFactura;
	}
	public String getEstatusOrdenpago() {
		return estatusOrdenpago;
	}
	public void setEstatusOrdenpago(String estatusOrdenpago) {
		this.estatusOrdenpago = estatusOrdenpago;
	}
	public String getTipoEntidad() {
		return tipoEntidad;
	}
	public void setTipoEntidad(String tipoEntidad) {
		this.tipoEntidad = tipoEntidad;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public Integer getSolicitudCheque() {
		return solicitudCheque;
	}
	public void setSolicitudCheque(Integer solicitudCheque) {
		this.solicitudCheque = solicitudCheque;
	}
	
	
	
	
}