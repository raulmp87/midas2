package mx.com.afirme.midas2.domain.compensaciones;

import java.io.Serializable;
import java.util.Date;

public class CaReportesCompDTO implements java.io.Serializable {	

	private static final long serialVersionUID = 6063935662531724849L;
		private String ramo;
		private String noPoliza;
		private Date fechaEmision;
		private Long idCompensacion;
		private Long clave;
		private String razonSocial;
		private String tipoContrato;
		private String frec_pago;
		private String prima_base;
		private Double imp_prima_base;
		private Double pct_com_prima;
		private Double mon_com_prima;
		private Double prima_com_sin;
		private Double pct_com_sin;
		private Double importeDerpol;
		private Double pct_com_dp;
		private Double mon_com_dp;
		private Long idNegocio;				
		private String nombreNegocio;
		
		public String getRamo() {
			return ramo;
		}
		public String getNoPoliza() {
			return noPoliza;
		}
		public Date getFechaEmision() {
			return fechaEmision;
		}
		public Long getIdCompensacion() {
			return idCompensacion;
		}
		public String getRazonSocial() {
			return razonSocial;
		}
		public String getTipoContrato() {
			return tipoContrato;
		}
		public String getFrec_pago() {
			return frec_pago;
		}
		public Long getIdNegocio() {
			return idNegocio;
		}
		public String getNombreNegocio() {
			return nombreNegocio;
		}
		public void setRamo(String ramo) {
			this.ramo = ramo;
		}
		public void setNoPoliza(String noPoliza) {
			this.noPoliza = noPoliza;
		}
		public void setFechaEmision(Date fechaEmision) {
			this.fechaEmision = fechaEmision;
		}
		public void setIdCompensacion(Long idCompensacion) {
			this.idCompensacion = idCompensacion;
		}
		public void setRazonSocial(String razonSocial) {
			this.razonSocial = razonSocial;
		}
		public void setTipoContrato(String tipoContrato) {
			this.tipoContrato = tipoContrato;
		}
		public void setFrec_pago(String frec_pago) {
			this.frec_pago = frec_pago;
		}
		public void setIdNegocio(Long idNegocio) {
			this.idNegocio = idNegocio;
		}
		public void setNombreNegocio(String nombreNegocio) {
			this.nombreNegocio = nombreNegocio;
		}
		public Long getClave() {
			return clave;
		}
		public void setClave(Long clave) {
			this.clave = clave;
		}	
		
		public Double getImp_prima_base() {
			return imp_prima_base;
		}
		public Double getPct_com_prima() {
			return pct_com_prima;
		}
		public Double getMon_com_prima() {
			return mon_com_prima;
		}
		public Double getPrima_com_sin() {
			return prima_com_sin;
		}
		public Double getPct_com_sin() {
			return pct_com_sin;
		}
		public Double getImporteDerpol() {
			return importeDerpol;
		}
		public Double getPct_com_dp() {
			return pct_com_dp;
		}
		public Double getMon_com_dp() {
			return mon_com_dp;
		}
		
		public void setImp_prima_base(Double imp_prima_base) {
			this.imp_prima_base = imp_prima_base;
		}
		public void setPct_com_prima(Double pct_com_prima) {
			this.pct_com_prima = pct_com_prima;
		}
		public void setMon_com_prima(Double mon_com_prima) {
			this.mon_com_prima = mon_com_prima;
		}
		public void setPrima_com_sin(Double prima_com_sin) {
			this.prima_com_sin = prima_com_sin;
		}
		public void setPct_com_sin(Double pct_com_sin) {
			this.pct_com_sin = pct_com_sin;
		}
		public void setImporteDerpol(Double importeDerpol) {
			this.importeDerpol = importeDerpol;
		}
		public void setPct_com_dp(Double pct_com_dp) {
			this.pct_com_dp = pct_com_dp;
		}
		public void setMon_com_dp(Double mon_com_dp) {
			this.mon_com_dp = mon_com_dp;
		}


		public String getPrima_base() {
			return prima_base;
		}
		public void setPrima_base(String prima_base) {
			this.prima_base = prima_base;
		}


		public static class DatosReporteCompParametrosDTO implements Serializable {
			
			private static final long serialVersionUID = -7814134288120214474L;
			private Short tipoReporte;
			private String agente;
			private String promotor;
			private String proveedor;
			private String gerencia;
			private String ramo;
			private Long idNegocio;
			private String nombreNegocio;
			private Date fechaInicio;
			private Date fechaFin;
			
			public DatosReporteCompParametrosDTO(Short tipoReporte,String agente,String promotor,
					 String proveedor,String gerencia,String ramo, Long idNegocio,String nombreNegocio,
					 Date fechaInicio,Date fechaFin) {
				super();
				this.tipoReporte = tipoReporte;
				this.agente = agente;
				this.promotor = promotor;
				this.proveedor = proveedor;
				this.gerencia = gerencia;
				this.ramo = ramo;
				this.idNegocio= idNegocio;
				this.nombreNegocio = nombreNegocio;
				this.fechaInicio = fechaInicio;
				this.fechaFin = fechaFin;
			}
			
			public Short getTipoReporte() {
				return tipoReporte;
			}
			public String getAgente() {
				return agente;
			}
			public String getPromotor() {
				return promotor;
			}
			public String getProveedor() {
				return proveedor;
			}
			public String getGerencia() {
				return gerencia;
			}
			public String getRamo() {
				return ramo;
			}
			public Long getIdNegocio() {
				return idNegocio;
			}
			public String getNombreNegocio() {
				return nombreNegocio;
			}
			public void setTipoReporte(Short tipoReporte) {
				this.tipoReporte = tipoReporte;
			}
			public void setAgente(String agente) {
				this.agente = agente;
			}
			public void setPromotor(String promotor) {
				this.promotor = promotor;
			}
			public void setProveedor(String proveedor) {
				this.proveedor = proveedor;
			}
			public void setGerencia(String gerencia) {
				this.gerencia = gerencia;
			}
			public void setRamo(String ramo) {
				this.ramo = ramo;
			}
			public void setIdNegocio(Long idNegocio) {
				this.idNegocio = idNegocio;
			}
			public void setNombreNegocio(String nombreNegocio) {
				this.nombreNegocio = nombreNegocio;
			}

			public Date getFechaInicio() {
				return fechaInicio;
			}

			public Date getFechaFin() {
				return fechaFin;
			}

			public void setFechaInicio(Date fechaInicio) {
				this.fechaInicio = fechaInicio;
			}

			public void setFechaFin(Date fechaFin) {
				this.fechaFin = fechaFin;
			}
			
			
		}	
	
}