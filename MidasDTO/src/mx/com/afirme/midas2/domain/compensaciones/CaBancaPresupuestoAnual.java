package mx.com.afirme.midas2.domain.compensaciones;


import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="CA_BANCA_PRESUPUESTOANUAL"
    ,schema="MIDAS"
)
public class CaBancaPresupuestoAnual implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private Long cabancaconfiguracionId;
	private String ramoseycosId;
	private Integer mes;
	private BigDecimal monto;


	public CaBancaPresupuestoAnual() {
	}

	public CaBancaPresupuestoAnual(Long cabancaconfiguracionId, String ramoseycosId, Integer mes, BigDecimal monto) {
		this.cabancaconfiguracionId = cabancaconfiguracionId;
		this.ramoseycosId = ramoseycosId;
		this.mes = mes;
		this.monto = monto;
	}


    @Id 
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CA_BANCA_PRESUPUESTO_ID_SEQ")
	@SequenceGenerator(name = "CA_BANCA_PRESUPUESTO_ID_SEQ",  schema="MIDAS", sequenceName = "CA_BANCA_PRESUPUESTO_ID_SEQ", allocationSize = 1)
    @Column(name="ID", unique=true, nullable=false, precision=32, scale=0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "CABANCACONFIGURACION_ID", precision = 32, scale = 0)
	public Long getCabancaconfiguracionId() {
		return this.cabancaconfiguracionId;
	}

	public void setCabancaconfiguracionId(Long cabancaconfiguracionId) {
		this.cabancaconfiguracionId = cabancaconfiguracionId;
	}

	@Column(name = "RAMOSEYCOS_ID", length = 2)
	public String getRamoseycosId() {
		return this.ramoseycosId;
	}

	public void setRamoseycosId(String ramoseycosId) {
		this.ramoseycosId = ramoseycosId;
	}

	@Column(name = "MES", precision = 2, scale = 0)
	public Integer getMes() {
		return this.mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

	@Column(name = "MONTO")
	public BigDecimal getMonto() {
		return this.monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}
	
	@Override
	public String toString(){
		StringBuilder buffer = new StringBuilder();
		buffer.append(this.getClass().getName()).append(" [");
		buffer.append("id").append("=").append(this.id).append(", ");
		buffer.append("cabancaconfiguracionId").append("=").append(this.cabancaconfiguracionId).append(", ");
		buffer.append("ramoseycosId").append("=").append(this.ramoseycosId).append(", ");
		buffer.append("mes").append("=").append(this.mes).append(", ");
		buffer.append("monto").append("=").append(this.monto).append("]");		
		return buffer.toString();
	}
	

	
}