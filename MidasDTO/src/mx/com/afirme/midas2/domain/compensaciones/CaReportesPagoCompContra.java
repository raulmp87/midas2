package mx.com.afirme.midas2.domain.compensaciones;

import java.util.Date;

public class CaReportesPagoCompContra implements java.io.Serializable{

	private static final long serialVersionUID = 8938331556170465543L;
	
	private Date fechaEmision;
	private String gerencia;
	private String promotoria;
	private String nombreAgentePromotorProveedor;
	private String personaFisicaMoral;
	private Long numeroPoliza;
	private Long recibo;
	private Date fechaReciboDesde;
	private Date fechaReciboHasta;
	private Double moneda;
	private String ramo;
	private String subRamo;
	private String nombreSolicitanteAsegurado;
	private Long polizaContableRegistroProvisionEmision;
	private Long polizaContableRegistroPagoTrCh;
	private Double importePago;
	private String tipoContrato;
	private String ramoProducto;
	private Double primaNeta;
	private Double primaNetaExtraPrima;
	private Double derechosPoliza;
	private Double primaTotal;
	private String referenciaPago;
	private Double porcentajeComision;
	private Double cantidadComisionBono;
	private Double saldoInicialBonos;
	private Double bonosDelMes;
	private Double bonosPorDevengar;
	private Double saldoFinalBonosPorDevengar;
	private Double saldoInicialBonosPorPagar;
	private Double bonosPorPagarDelMes;
	private Double bonosPagadosEnElMes;
	private Double saldoFinalBonosPorPagar;
	private Double saldoInicialBonosPorAmortizar;
	private Double bonosPorAmortizarDelMes;
	private Double amortizacion;
	private Double saldoFinalDeBonosPorAmortizar;
	private String articulo_102;
	private Double cta_1622_04;
	private Double cta_1622_05;
	private Double cta_2408_09;
	private Double cta_2408_10;
	private Double cta_1633_01;
	private Double cta_2605_02;
	private Double cta_2605_12;	
	private Double cta_1502_01;
	private String tipo_contrato;
	private String uen;
	private String cve_tipo_entidad;
	private Long id_negocio;
	public static class DatosReportePagoCompContraParametrosDTO implements java.io.Serializable{
		private Date fechaInicio;
		private Date fechaFinal;
		private Long claveNombreAgente;
		private Long claveNombrePromotor;
		private Long claveNombreProveedor;
		private Long claveNombreGerencia;
		private String ramo;
		private Long poliza;

		public DatosReportePagoCompContraParametrosDTO(Date fechaInicio,Date fechaFinal,Long claveNombreAgente,Long claveNombrePromotor,Long claveNombreProveedor,Long claveNombreGerencia,String ramo,Long poliza)
		{
			super();
			this.fechaInicio = fechaInicio;
			this.fechaFinal = fechaFinal;
			this.claveNombreAgente = claveNombreAgente;
			this.claveNombrePromotor = claveNombrePromotor;
			this.claveNombreProveedor = claveNombreProveedor;
			this.claveNombreGerencia = claveNombreGerencia;
			this.ramo = ramo;
			this.poliza = poliza;
			
		}

		public Long getClaveNombreAgente() {
			return claveNombreAgente;
		}

		public void setClaveNombreAgente(Long claveNombreAgente) {
			this.claveNombreAgente = claveNombreAgente;
		}

		public Long getClaveNombreProveedor() {
			return claveNombreProveedor;
		}

		public void setClaveNombreProveedor(Long claveNombreProveedor) {
			this.claveNombreProveedor = claveNombreProveedor;
		}

		public String getRamo() {
			return ramo;
		}

		public void setRamo(String ramo) {
			this.ramo = ramo;
		}

		public Long getPoliza() {
			return poliza;
		}

		public void setPoliza(Long poliza) {
			this.poliza = poliza;
		}

		public Long getClaveNombreGerencia() {
			return claveNombreGerencia;
		}

		public void setClaveNombreGerencia(Long claveNombreGerencia) {
			this.claveNombreGerencia = claveNombreGerencia;
		}

		public Long getClaveNombrePromotor() {
			return claveNombrePromotor;
		}

		public void setClaveNombrePromotor(Long claveNombrePromotor) {
			this.claveNombrePromotor = claveNombrePromotor;
		}

		public Date getFechaInicio() {
			return fechaInicio;
		}

		public void setFechaInicio(Date fechaInicio) {
			this.fechaInicio = fechaInicio;
		}

		public Date getFechaFinal() {
			return fechaFinal;
		}

		public void setFechaFinal(Date fechaFinal) {
			this.fechaFinal = fechaFinal;
		}		

	}
	
	
	public Date getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public String getGerencia() {
		return gerencia;
	}
	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}
	public String getPromotoria() {
		return promotoria;
	}
	public void setPromotoria(String promotoria) {
		this.promotoria = promotoria;
	}
	public String getNombreAgentePromotorProveedor() {
		return nombreAgentePromotorProveedor;
	}
	public void setNombreAgentePromotorProveedor(
			String nombreAgentePromotorProveedor) {
		this.nombreAgentePromotorProveedor = nombreAgentePromotorProveedor;
	}
	public String getPersonaFisicaMoral() {
		return personaFisicaMoral;
	}
	public void setPersonaFisicaMoral(String personaFisicaMoral) {
		this.personaFisicaMoral = personaFisicaMoral;
	}
	public Long getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(Long numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public Long getRecibo() {
		return recibo;
	}
	public void setRecibo(Long recibo) {
		this.recibo = recibo;
	}
	public Date getFechaReciboDesde() {
		return fechaReciboDesde;
	}
	public void setFechaReciboDesde(Date fechaReciboDesde) {
		this.fechaReciboDesde = fechaReciboDesde;
	}
	public Date getFechaReciboHasta() {
		return fechaReciboHasta;
	}
	public void setFechaReciboHasta(Date fechaReciboHasta) {
		this.fechaReciboHasta = fechaReciboHasta;
	}
	public Double getMoneda() {
		return moneda;
	}
	public void setMoneda(Double moneda) {
		this.moneda = moneda;
	}
	public String getRamo() {
		return ramo;
	}
	public void setRamo(String ramo) {
		this.ramo = ramo;
	}
	public String getSubRamo() {
		return subRamo;
	}
	public void setSubRamo(String subRamo) {
		this.subRamo = subRamo;
	}
	public String getNombreSolicitanteAsegurado() {
		return nombreSolicitanteAsegurado;
	}
	public void setNombreSolicitanteAsegurado(String nombreSolicitanteAsegurado) {
		this.nombreSolicitanteAsegurado = nombreSolicitanteAsegurado;
	}
	public Long getPolizaContableRegistroProvisionEmision() {
		return polizaContableRegistroProvisionEmision;
	}
	public void setPolizaContableRegistroProvisionEmision(
			Long polizaContableRegistroProvisionEmision) {
		this.polizaContableRegistroProvisionEmision = polizaContableRegistroProvisionEmision;
	}
	public Long getPolizaContableRegistroPagoTrCh() {
		return polizaContableRegistroPagoTrCh;
	}
	public void setPolizaContableRegistroPagoTrCh(
			Long polizaContableRegistroPagoTrCh) {
		this.polizaContableRegistroPagoTrCh = polizaContableRegistroPagoTrCh;
	}
	public Double getImportePago() {
		return importePago;
	}
	public void setImportePago(Double importePago) {
		this.importePago = importePago;
	}
	public String getTipoContrato() {
		return tipoContrato;
	}
	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}
	public String getRamoProducto() {
		return ramoProducto;
	}
	public void setRamoProducto(String ramoProducto) {
		this.ramoProducto = ramoProducto;
	}
	public Double getPrimaNeta() {
		return primaNeta;
	}
	public void setPrimaNeta(Double primaNeta) {
		this.primaNeta = primaNeta;
	}
	public Double getPrimaNetaExtraPrima() {
		return primaNetaExtraPrima;
	}
	public void setPrimaNetaExtraPrima(Double primaNetaExtraPrima) {
		this.primaNetaExtraPrima = primaNetaExtraPrima;
	}
	public Double getDerechosPoliza() {
		return derechosPoliza;
	}
	public void setDerechosPoliza(Double derechosPoliza) {
		this.derechosPoliza = derechosPoliza;
	}
	public Double getPrimaTotal() {
		return primaTotal;
	}
	public void setPrimaTotal(Double primaTotal) {
		this.primaTotal = primaTotal;
	}
	public String getReferenciaPago() {
		return referenciaPago;
	}
	public void setReferenciaPago(String referenciaPago) {
		this.referenciaPago = referenciaPago;
	}
	public Double getPorcentajeComision() {
		return porcentajeComision;
	} 
	public void setPorcentajeComision(Double porcentajeComision) {
		this.porcentajeComision = porcentajeComision;
	}
	public Double getCantidadComisionBono() {
		return cantidadComisionBono;
	}
	public void setCantidadComisionBono(Double cantidadComisionBono) {
		this.cantidadComisionBono = cantidadComisionBono;
	}
	public Double getBonosDelMes() {
		return bonosDelMes;
	}
	public void setBonosDelMes(Double bonosDelMes) {
		this.bonosDelMes = bonosDelMes;
	}
	public Double getBonosPorDevengar() {
		return bonosPorDevengar;
	}
	public void setBonosPorDevengar(Double bonosPorDevengar) {
		this.bonosPorDevengar = bonosPorDevengar;
	}
	public Double getSaldoFinalBonosPorDevengar() {
		return saldoFinalBonosPorDevengar;
	}
	public void setSaldoFinalBonosPorDevengar(Double saldoFinalBonosPorDevengar) {
		this.saldoFinalBonosPorDevengar = saldoFinalBonosPorDevengar;
	}
	public Double getSaldoInicialBonosPorPagar() {
		return saldoInicialBonosPorPagar;
	}
	public void setSaldoInicialBonosPorPagar(Double saldoInicialBonosPorPagar) {
		this.saldoInicialBonosPorPagar = saldoInicialBonosPorPagar;
	}
	public Double getBonosPorPagarDelMes() {
		return bonosPorPagarDelMes;
	}
	public void setBonosPorPagarDelMes(Double bonosPorPagarDelMes) {
		this.bonosPorPagarDelMes = bonosPorPagarDelMes;
	}
	public Double getBonosPagadosEnElMes() {
		return bonosPagadosEnElMes;
	}
	public void setBonosPagadosEnElMes(Double bonosPagadosEnElMes) {
		this.bonosPagadosEnElMes = bonosPagadosEnElMes;
	}
	public Double getSaldoFinalBonosPorPagar() {
		return saldoFinalBonosPorPagar;
	}
	public void setSaldoFinalBonosPorPagar(Double saldoFinalBonosPorPagar) {
		this.saldoFinalBonosPorPagar = saldoFinalBonosPorPagar;
	}
	public Double getSaldoInicialBonosPorAmortizar() {
		return saldoInicialBonosPorAmortizar;
	}
	public void setSaldoInicialBonosPorAmortizar(
			Double saldoInicialBonosPorAmortizar) {
		this.saldoInicialBonosPorAmortizar = saldoInicialBonosPorAmortizar;
	}
	public Double getBonosPorAmortizarDelMes() {
		return bonosPorAmortizarDelMes;
	}
	public void setBonosPorAmortizarDelMes(Double bonosPorAmortizarDelMes) {
		this.bonosPorAmortizarDelMes = bonosPorAmortizarDelMes;
	}
	public Double getAmortizacion() {
		return amortizacion;
	}
	public void setAmortizacion(Double amortizacion) {
		this.amortizacion = amortizacion;
	}
	public Double getSaldoFinalDeBonosPorAmortizar() {
		return saldoFinalDeBonosPorAmortizar;
	}
	public void setSaldoFinalDeBonosPorAmortizar(
			Double saldoFinalDeBonosPorAmortizar) {
		this.saldoFinalDeBonosPorAmortizar = saldoFinalDeBonosPorAmortizar;
	}
	public Double getSaldoInicialBonos() {
		return saldoInicialBonos;
	}
	public void setSaldoInicialBonos(Double saldoInicialBonos) {
		this.saldoInicialBonos = saldoInicialBonos;
	}
	
	public Double getCta_1622_04() {
		return cta_1622_04;
	}
	public void setCta_1622_04(Double cta_1622_04) {
		this.cta_1622_04 = cta_1622_04;
	}
	public Double getCta_1622_05() {
		return cta_1622_05;
	}
	public void setCta_1622_05(Double cta_1622_05) {
		this.cta_1622_05 = cta_1622_05;
	}
	public Double getCta_2408_09() {
		return cta_2408_09;
	}
	public void setCta_2408_09(Double cta_2408_09) {
		this.cta_2408_09 = cta_2408_09;
	}
	public Double getCta_2408_10() {
		return cta_2408_10;
	}
	public void setCta_2408_10(Double cta_2408_10) {
		this.cta_2408_10 = cta_2408_10;
	}
	public Double getCta_1633_01() {
		return cta_1633_01;
	}
	public void setCta_1633_01(Double cta_1633_01) {
		this.cta_1633_01 = cta_1633_01;
	}
	public Double getCta_2605_02() {
		return cta_2605_02;
	}
	public void setCta_2605_02(Double cta_2605_02) {
		this.cta_2605_02 = cta_2605_02;
	}
	public Double getCta_2605_12() {
		return cta_2605_12;
	}
	public void setCta_2605_12(Double cta_2605_12) {
		this.cta_2605_12 = cta_2605_12;
	}
	public Double getCta_1502_01() {
		return cta_1502_01;
	}
	public void setCta_1502_01(Double cta_1502_01) {
		this.cta_1502_01 = cta_1502_01;
	}
	public String getArticulo_102() {
		return articulo_102;
	}
	public void setArticulo_102(String articulo_102) {
		this.articulo_102 = articulo_102;
	}
	public String getTipo_contrato() {
		return tipo_contrato;
	}
	public void setTipo_contrato(String tipo_contrato) {
		this.tipo_contrato = tipo_contrato;
	}
	public String getUen() {
		return uen;
	}
	public void setUen(String uen) {
		this.uen = uen;
	}
	public String getCve_tipo_entidad() {
		return cve_tipo_entidad;
	}
	public void setCve_tipo_entidad(String cve_tipo_entidad) {
		this.cve_tipo_entidad = cve_tipo_entidad;
	}
	public Long getId_negocio() {
		return id_negocio;
	}
	public void setId_negocio(Long id_negocio) {
		this.id_negocio = id_negocio;
	}
	

}
