package mx.com.afirme.midas2.domain.compensaciones.negociovida;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.emision.ppct.AgenteSeycos;

/**
 * NegocioVidaAgente entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CA_TRNEGOCIOVIDA_AGENTE", schema = "MIDAS")
public class NegocioVidaAgente implements java.io.Serializable,Entidad  {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private NegocioVida negocioVida;
	private AgenteSeycos agenteSeycos;
	private Long idEmpresa;
    private Long idAgente;
    private Date FSit;
	// Constructors

	/** default constructor */
	public NegocioVidaAgente() {
	}

	/** minimal constructor */
	public NegocioVidaAgente(Long id, Long idEmpresa, Long idAgente, Date FSit) {
		this.id = id;
		this.idEmpresa = idEmpresa;
		this.idAgente = idAgente;
		this.FSit = FSit;
	}

	/** full constructor */
	public NegocioVidaAgente(Long id, NegocioVida negocioVida, AgenteSeycos agenteSeycos, Long idEmpresa, Long idAgente, Date FSit) {
		this.id = id;
		this.negocioVida = negocioVida;
		this.agenteSeycos = agenteSeycos;
		this.idEmpresa = idEmpresa;
		this.idAgente = idAgente;
		this.FSit = FSit;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CATRNEGVIDAAGT_ID_SEQ")
	@SequenceGenerator(name="CATRNEGVIDAAGT_ID_SEQ", schema="MIDAS",sequenceName="CATRNEGVIDAAGT_ID_SEQ",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "NEGOCIOVIDA_ID")
	public NegocioVida getNegocioVida() {
		return this.negocioVida;
	}

	public void setNegocioVida(NegocioVida negocioVida) {
		this.negocioVida = negocioVida;
	}
	
//	@ManyToOne(fetch=FetchType.EAGER)
//    @JoinColumns( { 
//    @JoinColumn(name="ID_EMPRESA", referencedColumnName="ID_EMPRESA", nullable=false, insertable=false, updatable=false), 
//    @JoinColumn(name="ID_AGENTE", referencedColumnName="ID_AGENTE", nullable=false, insertable=false, updatable=false),
//    @JoinColumn(name="F_SIT", referencedColumnName="F_SIT", nullable=false, insertable=false, updatable=false)})
	@Transient
	public AgenteSeycos getAgenteSeycos() {
		return agenteSeycos;
	}

	public void setAgenteSeycos(AgenteSeycos agenteSeycos) {
		this.agenteSeycos = agenteSeycos;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long  getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	@Column(name = "ID_EMPRESA")
	public Long getIdEmpresa() {
		return idEmpresa;
	}
	
	@Column(name = "ID_AGENTE")
	public Long getIdAgente() {
		return idAgente;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "F_SIT")
	public Date getFSit() {
		return FSit;
	}
	
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public void setFSit(Date fSit) {
		FSit = fSit;
	}

}