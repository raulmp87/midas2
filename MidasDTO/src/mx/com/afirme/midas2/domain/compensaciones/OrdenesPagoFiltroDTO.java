package mx.com.afirme.midas2.domain.compensaciones;
import java.util.List;

import mx.com.afirme.midas2.dto.wrapper.CatalogoValorFijoComboDTO;

public class OrdenesPagoFiltroDTO implements java.io.Serializable {
	static final long serialVersionUID = 1L;

	private Long id;
	private List<CatalogoValorFijoComboDTO> estatusFacura;
	private List<CatalogoValorFijoComboDTO> estatusOrdenpago;
	private List<CaRamo> ramos;
	
	public OrdenesPagoFiltroDTO(Long id,
			List<CatalogoValorFijoComboDTO> estatusFacura,
			List<CatalogoValorFijoComboDTO> estatusOrdenpago,
			List<CaRamo> ramos) {
		super();
		this.id = id;
		this.estatusFacura = estatusFacura;
		this.estatusOrdenpago = estatusOrdenpago;
		this.ramos = ramos;
	}
	
	public OrdenesPagoFiltroDTO(List<CatalogoValorFijoComboDTO> estatusFacura,
			List<CatalogoValorFijoComboDTO> estatusOrdenpago,
			List<CaRamo> ramos) {
		super();
		this.estatusFacura = estatusFacura;
		this.estatusOrdenpago = estatusOrdenpago;
		this.ramos = ramos;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	public Long getId() {
		return id;
	}
	public List<CatalogoValorFijoComboDTO> getEstatusFacura() {
		return estatusFacura;
	}
	public void setEstatusFacura(List<CatalogoValorFijoComboDTO> estatusFacura) {
		this.estatusFacura = estatusFacura;
	}
	public List<CatalogoValorFijoComboDTO> getEstatusOrdenpago() {
		return estatusOrdenpago;
	}
	public void setEstatusOrdenpago(List<CatalogoValorFijoComboDTO> estatusOrdenpago) {
		this.estatusOrdenpago = estatusOrdenpago;
	}
	public List<CaRamo> getRamos() {
		return ramos;
	}
	public void setRamos(List<CaRamo> ramos) {
		this.ramos = ramos;
	}
	
}