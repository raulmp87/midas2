package mx.com.afirme.midas2.domain.compensaciones.negociovida;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Clase que representa una vista para presentar los datos del agente en el grid de agentes por autorizar
 * @author vmhersil
 *
 */
public class DatosSeycos implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3656043584317073133L;
	private static final Logger LOG = LoggerFactory.getLogger(DatosSeycos.class);
	/**
	 * 
	 */
	private Long id;
	private String idAgente;
	private String idEmpresa;
	private String fsit;
	private Date dateFsit;
	private String nombreCompleto;
	private String idSupervisoria;
	private String nombreSupervisoria;
	
	public Long getId() {
		return id;
	}
	public String getIdAgente() {
		return idAgente;
	}
	public String getIdEmpresa() {
		return idEmpresa;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setIdAgente(String idAgente) {
		this.idAgente = idAgente;
		this.setId(Long.valueOf(idAgente));
	}
	public void setIdEmpresa(String idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	public void setIdSupervisoria(String idSupervisoria) {
		this.idSupervisoria = idSupervisoria;
	}
	public String getIdSupervisoria() {
		return idSupervisoria;
	}
	public void setNombreSupervisoria(String nombreSupervisoria) {
		this.nombreSupervisoria = nombreSupervisoria;
	}
	public String getNombreSupervisoria() {
		return nombreSupervisoria;
	}
	public void setFsit(String fsit) {
		this.fsit = fsit;
	}
	public String getFsit() {
		return fsit;
	}
	
	public Date getDateFsit(){		
		Date fsit = null;		
		if(this.dateFsit != null){
			fsit = dateFsit;
		}else if(this.fsit != null && !this.fsit.trim().isEmpty()){
			try {
				fsit = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(this.fsit);
			} catch (Exception e) {
				LOG.error("Ocurrio un error al Parsear el String a Date: "+e.getMessage());			
			}
		}else{
			LOG.info("DatosSeycos: No se encontro la propiedad dateFsit");
		}		
		return fsit;
	}
	
	public void setDateFsit(Date dateFsit) {
		this.dateFsit = dateFsit;
		if(this.dateFsit != null){
			try {
				String fsit = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(this.dateFsit);
				this.setFsit(fsit);
			} catch (Exception e) {
				LOG.error("Ocurrio un error al Parsear Date a String: "+e.getMessage());			
			}
		}
	}
	
}
