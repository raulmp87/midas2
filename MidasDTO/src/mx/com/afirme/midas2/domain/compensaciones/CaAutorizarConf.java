package mx.com.afirme.midas2.domain.compensaciones;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * CaAutorizarConf entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CA_AUTORIZARCONF", schema = "MIDAS")
public class CaAutorizarConf implements java.io.Serializable {

	// Fields

	private static final long serialVersionUID = 1L;
	private Long id;
	private CaRamo caRamo;
	private Double endoso;
	private Double modificable;

	// Constructors

	/** default constructor */
	public CaAutorizarConf() {
	}

	/** minimal constructor */
	public CaAutorizarConf(Long id) {
		this.id = id;
	}

	/** full constructor */
	public CaAutorizarConf(Long id, CaRamo caRamo, Double endoso,
			Double modificable) {
		this.id = id;
		this.caRamo = caRamo;
		this.endoso = endoso;
		this.modificable = modificable;
	}

	// Property accessors
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = ".CA_AUTORIZARCONF_ID_SEQ")
	@SequenceGenerator(name = ".CA_AUTORIZARCONF_ID_SEQ", schema = "MIDAS", sequenceName = ".CA_AUTORIZARCONF_ID_SEQ", allocationSize = 1)
	@Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
	public Long getId() {
		return id;
	}

	
	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "RAMO_ID")
	public CaRamo getCaRamo() {
		return this.caRamo;
	}

	public void setCaRamo(CaRamo caRamo) {
		this.caRamo = caRamo;
	}

	@Column(name = "ENDOSO", precision = 0)
	public Double getEndoso() {
		return this.endoso;
	}

	public void setEndoso(Double endoso) {
		this.endoso = endoso;
	}

	@Column(name = "MODIFICABLE", precision = 0)
	public Double getModificable() {
		return this.modificable;
	}

	public void setModificable(Double modificable) {
		this.modificable = modificable;
	}

}