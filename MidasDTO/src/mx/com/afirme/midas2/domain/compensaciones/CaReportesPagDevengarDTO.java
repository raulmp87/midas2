package mx.com.afirme.midas2.domain.compensaciones;

import java.io.Serializable;
import java.util.Date;

public class CaReportesPagDevengarDTO implements java.io.Serializable {	

		private static final long serialVersionUID = 7157916039579132183L;
		private Long noPoliza;
		private Long recibo;
		private Date fechaReciboDesde;
		private Date fechaReciboHasta;
		private Long moneda;
		private String descMoneda;
		private String ramo;
		private String subRamo;
		private Date fechaContmizar;
		private Long polizaContReg;
		private String agenteNoAgente;
		private String perFisoPerMor;
		private String tipoContrato;
		private Long claveAgente;
		private String nombreAgente;
		private String gerencia;
		private String nomSolAseg;
		private String ramoProducto;
		private Double prima_neta;
		private Double recargos;
		private Double derechos;
		private Double pct_comision;
		private Double sald_inicial_bon;
		private Double bonos_mes;
		private Double bon_x_pag_mes;
		private Double bon_pag_mes;
		private Double bon_x_devengar;
		private Double sal_final_bon_dev;
		private Double sal_ini_bon_pagar;
		private Double sal_fin_bon_x_pag;
		private Double sal_fin_bon_x_amort;
		private Double bon_amort_mes;
		private Double amortizar;
		private Double sal_ini_x_amortizar;

		
		
		
		
		private Long idNegocio;
		
		public String getRamo() {
			return ramo;
		}
		public String getTipoContrato() {
			return tipoContrato;
		}
		
		public Long getIdNegocio() {
			return idNegocio;
		}
		public void setRamo(String ramo) {
			this.ramo = ramo;
		}
		public void setTipoContrato(String tipoContrato) {
			this.tipoContrato = tipoContrato;
		}
		public void setIdNegocio(Long idNegocio) {
			this.idNegocio = idNegocio;
		}

		public Long getRecibo() {
			return recibo;
		}
		public Date getFechaReciboDesde() {
			return fechaReciboDesde;
		}
		public Date getFechaReciboHasta() {
			return fechaReciboHasta;
		}
		public String getSubRamo() {
			return subRamo;
		}
		
		public Date getFechaContmizar() {
			return fechaContmizar;
		}
		public Long getPolizaContReg() {
			return polizaContReg;
		}
		public String getPerFisoPerMor() {
			return perFisoPerMor;
		}
		public String getNombreAgente() {
			return nombreAgente;
		}
		public String getGerencia() {
			return gerencia;
		}
		public String getNomSolAseg() {
			return nomSolAseg;
		}
		public String getRamoProducto() {
			return ramoProducto;
		}
		public Double getPrima_neta() {
			return prima_neta;
		}
		public Double getPct_comision() {
			return pct_comision;
		}
		public Double getSald_inicial_bon() {
			return sald_inicial_bon;
		}
		public Double getBonos_mes() {
			return bonos_mes;
		}
		public Double getSal_final_bon_dev() {
			return sal_final_bon_dev;
		}
		public Double getSal_ini_bon_pagar() {
			return sal_ini_bon_pagar;
		}
		public Double getBon_x_pag_mes() {
			return bon_x_pag_mes;
		}
		public Double getBon_pag_mes() {
			return bon_pag_mes;
		}
		public Double getSal_fin_bon_x_pag() {
			return sal_fin_bon_x_pag;
		}
		public Double getSal_ini_x_amortizar() {
			return sal_ini_x_amortizar;
		}
		public Double getBon_amort_mes() {
			return bon_amort_mes;
		}
		public Double getAmortizar() {
			return amortizar;
		}
		public Double getSal_fin_bon_x_amort() {
			return sal_fin_bon_x_amort;
		}
		public void setRecibo(Long recibo) {
			this.recibo = recibo;
		}
		public void setFechaReciboDesde(Date fechaReciboDesde) {
			this.fechaReciboDesde = fechaReciboDesde;
		}
		public void setFechaReciboHasta(Date fechaReciboHasta) {
			this.fechaReciboHasta = fechaReciboHasta;
		}
		public void setSubRamo(String subRamo) {
			this.subRamo = subRamo;
		}
		public void setFechaContmizar(Date fechaContmizar) {
			this.fechaContmizar = fechaContmizar;
		}
		public void setPolizaContReg(Long polizaContReg) {
			this.polizaContReg = polizaContReg;
		}
		public void setPerFisoPerMor(String perFisoPerMor) {
			this.perFisoPerMor = perFisoPerMor;
		}
		public void setNombreAgente(String nombreAgente) {
			this.nombreAgente = nombreAgente;
		}
		public void setGerencia(String gerencia) {
			this.gerencia = gerencia;
		}
		public void setNomSolAseg(String nomSolAseg) {
			this.nomSolAseg = nomSolAseg;
		}
		public void setRamoProducto(String ramoProducto) {
			this.ramoProducto = ramoProducto;
		}
		public void setPrima_neta(Double prima_neta) {
			this.prima_neta = prima_neta;
		}
		public void setPct_comision(Double pct_comision) {
			this.pct_comision = pct_comision;
		}
		public void setSald_inicial_bon(Double sald_inicial_bon) {
			this.sald_inicial_bon = sald_inicial_bon;
		}
		public void setBonos_mes(Double bonos_mes) {
			this.bonos_mes = bonos_mes;
		}
		public void setSal_final_bon_dev(Double sal_final_bon_dev) {
			this.sal_final_bon_dev = sal_final_bon_dev;
		}
		public void setSal_ini_bon_pagar(Double sal_ini_bon_pagar) {
			this.sal_ini_bon_pagar = sal_ini_bon_pagar;
		}
		public void setBon_x_pag_mes(Double bon_x_pag_mes) {
			this.bon_x_pag_mes = bon_x_pag_mes;
		}
		public void setBon_pag_mes(Double bon_pag_mes) {
			this.bon_pag_mes = bon_pag_mes;
		}
		public void setSal_fin_bon_x_pag(Double sal_fin_bon_x_pag) {
			this.sal_fin_bon_x_pag = sal_fin_bon_x_pag;
		}
		public void setSal_ini_x_amortizar(Double sal_ini_x_amortizar) {
			this.sal_ini_x_amortizar = sal_ini_x_amortizar;
		}
		public void setBon_amort_mes(Double bon_amort_mes) {
			this.bon_amort_mes = bon_amort_mes;
		}
		public void setAmortizar(Double amortizar) {
			this.amortizar = amortizar;
		}
		public void setSal_fin_bon_x_amort(Double sal_fin_bon_x_amort) {
			this.sal_fin_bon_x_amort = sal_fin_bon_x_amort;
		}

		public Long getNoPoliza() {
			return noPoliza;
		}
		public Long getMoneda() {
			return moneda;
		}
		public String getDescMoneda() {
			return descMoneda;
		}
		public String getAgenteNoAgente() {
			return agenteNoAgente;
		}
		public Long getClaveAgente() {
			return claveAgente;
		}
		public Double getRecargos() {
			return recargos;
		}
		public Double getDerechos() {
			return derechos;
		}
		public Double getBon_x_devengar() {
			return bon_x_devengar;
		}
		public void setNoPoliza(Long noPoliza) {
			this.noPoliza = noPoliza;
		}
		public void setMoneda(Long moneda) {
			this.moneda = moneda;
		}
		public void setDescMoneda(String descMoneda) {
			this.descMoneda = descMoneda;
		}
		public void setAgenteNoAgente(String agenteNoAgente) {
			this.agenteNoAgente = agenteNoAgente;
		}
		public void setClaveAgente(Long claveAgente) {
			this.claveAgente = claveAgente;
		}
		public void setRecargos(Double recargos) {
			this.recargos = recargos;
		}
		public void setDerechos(Double derechos) {
			this.derechos = derechos;
		}
		public void setBon_x_devengar(Double bon_x_devengar) {
			this.bon_x_devengar = bon_x_devengar;
		}

		public static class DatosReportePagDevDTO implements Serializable {
			
			private static final long serialVersionUID = -1976140050287186718L;
			private Short tipoReporte;
			private Short tipoArchivo;
			
			private Long agente;
			private Long promotor;
			private Long proveedor;
			private Long gerencia;
			private String ramo;
			private Long idNegocio;
			private String nombreNegocio;
			private Long anioMes;
			private String noPoliza;
			
			public DatosReportePagDevDTO(Short tipoReporte,Short tipoArchivo,String ramo,Long agente,Long promotor,
					 Long proveedor,Long gerencia, Long idNegocio,String nombreNegocio,
					 Long anioMes, String noPoliza) {
				super();
				this.tipoReporte = tipoReporte;
				this.tipoArchivo = tipoArchivo;
				this.ramo = ramo;
				this.agente = agente;
				this.promotor = promotor;
				this.proveedor = proveedor;
				this.gerencia = gerencia;
				this.idNegocio= idNegocio;
				this.nombreNegocio = nombreNegocio;
				this.anioMes = anioMes;
				this.noPoliza = noPoliza;
			}
			
			public Short getTipoReporte() {
				return tipoReporte;
			}
			public Long getAgente() {
				return agente;
			}
			public Long getPromotor() {
				return promotor;
			}
			public Long getProveedor() {
				return proveedor;
			}
			public String getRamo() {
				return ramo;
			}
			public Long getIdNegocio() {
				return idNegocio;
			}
			public String getNombreNegocio() {
				return nombreNegocio;
			}
			public void setTipoReporte(Short tipoReporte) {
				this.tipoReporte = tipoReporte;
			}
			public void setAgente(Long agente) {
				this.agente = agente;
			}
			public void setPromotor(Long promotor) {
				this.promotor = promotor;
			}
			public void setProveedor(Long proveedor) {
				this.proveedor = proveedor;
			}
			public void setRamo(String ramo) {
				this.ramo = ramo;
			}
			public void setIdNegocio(Long idNegocio) {
				this.idNegocio = idNegocio;
			}
			public void setNombreNegocio(String nombreNegocio) {
				this.nombreNegocio = nombreNegocio;
			}

			public String getNoPoliza() {
				return noPoliza;
			}

			public void setNoPoliza(String noPoliza) {
				this.noPoliza = noPoliza;
			}

			public Long getGerencia() {
				return gerencia;
			}

			public void setGerencia(Long gerencia) {
				this.gerencia = gerencia;
			}

			public Long getAnioMes() {
				return anioMes;
			}

			public void setAnioMes(Long anioMes) {
				this.anioMes = anioMes;
			}

			public Short getTipoArchivo() {
				return tipoArchivo;
			}

			public void setTipoArchivo(Short tipoArchivo) {
				this.tipoArchivo = tipoArchivo;
			}
			
			
		}	
	
}