package mx.com.afirme.midas2.domain.compensaciones;

import java.math.BigDecimal;
import java.util.Date;

public class ReporteDetallePrimas implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String diaCorte;
	private String diaAplicacion;
    private String idRecibo;
   	private String forma_pago;
	private String num_poliza;
    private String num_endoso;
    private String nomRamo;
    private String nomSubramo;
    private String desc_moneda;
    private Double imp_prima_neta;
    private Double imp_rcgos_pagofr;
    private Double imp_comis_agte;
    private Double imp_prima_total;
    private Double imp_prima_riesgo;
    private Double imp_prima_cedida;
    private Double imp_der_pol_bc;
    private Double imp_prima;
    private Double imp_B_S;
    private Double imp_Der_Pol;
    private Double subTotal;
    private Double imp_iva_acred;
    private Double imp_iva_ret;
    private Double imp_isr;
    private Double totalporpoliza;
    private Double comis_acum_dia;
    private Double totalpormovimiento;
    
   
    public ReporteDetallePrimas() {
		this.imp_prima_neta = new Double(0);
		this.imp_rcgos_pagofr = new Double(0);
		this.imp_comis_agte = new Double(0);
		this.imp_prima_total = new Double(0);
		this.imp_prima_riesgo = new Double(0);
		this.imp_prima_cedida = new Double(0);
		this.imp_der_pol_bc = new Double(0);
		this.imp_prima = new Double(0);
		this.imp_B_S = new Double(0);
		this.imp_Der_Pol = new Double(0);
		this.subTotal = new Double(0);
		this.imp_iva_acred = new Double(0);
		this.imp_iva_ret = new Double(0);
		this.imp_isr = new Double(0);
		this.totalporpoliza = new Double(0);
		this.comis_acum_dia = new Double(0);
		this.totalpormovimiento = new Double(0);
	}

	public String getDiaCorte() {
		return diaCorte;
	}
	public void setDiaCorte(String diaCorte) {
		this.diaCorte = diaCorte;
	}
	public String getDiaAplicacion() {
		return diaAplicacion;
	}
	public void setDiaAplicacion(String diaAplicacion) {
		this.diaAplicacion = diaAplicacion;
	}
	public String getIdRecibo() {
		return idRecibo;
	}
	public void setIdRecibo(String idRecibo) {
		this.idRecibo = idRecibo;
	}
	public String getForma_pago() {
		return forma_pago;
	}
	public void setForma_pago(String forma_pago) {
		this.forma_pago = forma_pago;
	}
	public String getNum_poliza() {
		return num_poliza;
	}
	public void setNum_poliza(String num_poliza) {
		this.num_poliza = num_poliza;
	}
	public String getNum_endoso() {
		return num_endoso;
	}
	public void setNum_endoso(String num_endoso) {
		this.num_endoso = num_endoso;
	}
	public String getNomRamo() {
		return nomRamo;
	}
	public void setNomRamo(String nomRamo) {
		this.nomRamo = nomRamo;
	}
	public String getNomSubramo() {
		return nomSubramo;
	}
	public void setNomSubramo(String nomSubramo) {
		this.nomSubramo = nomSubramo;
	}
	public String getDesc_moneda() {
		return desc_moneda;
	}
	public void setDesc_moneda(String desc_moneda) {
		this.desc_moneda = desc_moneda;
	}
	public Double getImp_prima_neta() {
		return imp_prima_neta;
	}
	public void setImp_prima_neta(Double imp_prima_neta) {
		this.imp_prima_neta = imp_prima_neta;
	}
	public Double getImp_rcgos_pagofr() {
		return imp_rcgos_pagofr;
	}
	public void setImp_rcgos_pagofr(Double imp_rcgos_pagofr) {
		this.imp_rcgos_pagofr = imp_rcgos_pagofr;
	}
	public Double getImp_comis_agte() {
		return imp_comis_agte;
	}
	public void setImp_comis_agte(Double imp_comis_agte) {
		this.imp_comis_agte = imp_comis_agte;
	}
	public Double getImp_prima_total() {
		return imp_prima_total;
	}
	public void setImp_prima_total(Double imp_prima_total) {
		this.imp_prima_total = imp_prima_total;
	}
	public Double getImp_prima_riesgo() {
		return imp_prima_riesgo;
	}
	public void setImp_prima_riesgo(Double imp_prima_riesgo) {
		this.imp_prima_riesgo = imp_prima_riesgo;
	}
	public Double getImp_prima_cedida() {
		return imp_prima_cedida;
	}
	public void setImp_prima_cedida(Double imp_prima_cedida) {
		this.imp_prima_cedida = imp_prima_cedida;
	}
	public Double getImp_prima() {
		return imp_prima;
	}
	public void setImp_prima(Double imp_prima) {
		this.imp_prima = imp_prima;
	}
	public Double getImp_B_S() {
		return imp_B_S;
	}
	public void setImp_B_S(Double imp_B_S) {
		this.imp_B_S = imp_B_S;
	}
	public Double getImp_Der_Pol() {
		return imp_Der_Pol;
	}
	public void setImp_Der_Pol(Double imp_Der_Pol) {
		this.imp_Der_Pol = imp_Der_Pol;
	}
	public Double getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}
	public Double getImp_iva_acred() {
		return imp_iva_acred;
	}
	public void setImp_iva_acred(Double imp_iva_acred) {
		this.imp_iva_acred = imp_iva_acred;
	}
	public Double getImp_iva_ret() {
		return imp_iva_ret;
	}
	public void setImp_iva_ret(Double imp_iva_ret) {
		this.imp_iva_ret = imp_iva_ret;
	}
	public Double getImp_isr() {
		return imp_isr;
	}
	public void setImp_isr(Double imp_isr) {
		this.imp_isr = imp_isr;
	}
	public Double getTotalporpoliza() {
		return totalporpoliza;
	}
	public void setTotalporpoliza(Double totalporpoliza) {
		this.totalporpoliza = totalporpoliza;
	}
	public Double getComis_acum_dia() {
		return comis_acum_dia;
	}
	public void setComis_acum_dia(Double comis_acum_dia) {
		this.comis_acum_dia = comis_acum_dia;
	}
	public Double getTotalpormovimiento() {
		return totalpormovimiento;
	}
	public void setTotalpormovimiento(Double totalpormovimiento) {
		this.totalpormovimiento = totalpormovimiento;
	}
	public Double getImp_der_pol_bc() {
		return imp_der_pol_bc;
	}
	public void setImp_der_pol_bc(Double imp_der_pol_bc) {
		this.imp_der_pol_bc = imp_der_pol_bc;
	}
	
}
