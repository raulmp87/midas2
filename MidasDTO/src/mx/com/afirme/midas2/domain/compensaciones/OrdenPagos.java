package mx.com.afirme.midas2.domain.compensaciones;

// default package

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoEntidad;
import mx.com.afirme.midas2.domain.compensaciones.CaRamo;

/**
 * CaOrdenPago entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CA_ORDENPAGOS", schema = "MIDAS")
public class OrdenPagos implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	//CONSTANTES DE ESTATUS DE ORDEN DE PAGO GENERADO
	public static final Long ESTATUS_ORDPAG_GEN_PROCESO = 1L;
	public static final Long ESTATUS_ORDPAG_GEN_EXCLUIDO = 2L;
	public static final Long ESTATUS_ORDPAG_GEN_INCLUIDO = 3L;
	public static final Long ESTATUS_ORDPAG_GEN_GENERADO = 4L;
	//CONSTANTES DE ESTATUS DE ORDEN DE PAGO
	public static final Long ESTATUS_ORDENPAGO_CANCELADO = 1L;
	public static final Long ESTATUS_ORDENPAGO_VALIDA = 2L;
	public static final Long ESTATUS_ORDENPAGO_AUTORIZADA = 3L;
	//ESTATUS_FACTURA
	public static final Long ESTATUS_FACTURA_PENDIENTE = 1L;
	public static final Long ESTATUS_FACTURA_VALIDA = 2L;

	private Long id;
	private Long folio;
	private CaTipoEntidad caTipoEntidad;
	private CaTipoCompensacion caTipoCompensacion;
	private CaRamo caRamo;
	private Long idToPoliza;
	private CaCompensacion caCompensacion;
	private CaEntidadPersona caEntidadPersona;
	private String nombreBeneficiario;
	private Long identificador_ben_id;
	private String claveNombre;
	private String nombreContratante;
	private Double importePrima;
	private Double importeBs;
	private Double importeDerpol;
	private Double importeCumMeta;
	private Double importeUtilidad;
	private Double subtotal;
	private Double iva;
	private Double ivaRetenido;
	private Double isr;
	private Short estatusFactura;
	private Short estatusOrdenpago;
	private Double importeTotal;
	private Timestamp fechaCorte;
	private Timestamp fechaCreacion;
	private Boolean incluir;
	private Promotoria promotoria;

	// Constructors

	/** default constructor */
	public OrdenPagos() {
	}

	/** minimal constructor */
	public OrdenPagos(Long id) {
		this.id = id;
	}

	/** full constructor */
	public OrdenPagos(Long id, CaTipoEntidad caTipoEntidad,CaTipoCompensacion caTipoCompensacion, CaRamo caRamo,Long idToPoliza,
			CaCompensacion caCompensacion, CaEntidadPersona caEntidadPersona,String nombreBeneficiario, Long identificador_ben_id,
			String claveNombre, String nombreContratante, Double importePrima,
			Double importeBs, Double importeDerpol, Double importeCumMeta,
			Double importeUtilidad, Double subtotal, Double iva,
			Double ivaRetenido, Double isr, Short estatusFactura,
			Short estatusOrdenpago, Double importeTotal, Timestamp fechaCorte,
			Timestamp fechaCreacion, Long tipoCompensacion_id) {
		this.id = id;
		this.caTipoEntidad = caTipoEntidad;
		this.caTipoCompensacion = caTipoCompensacion;
		this.caRamo = caRamo;
		this.idToPoliza = idToPoliza;
		this.caCompensacion = caCompensacion;
		this.caEntidadPersona = caEntidadPersona;
		this.nombreBeneficiario = nombreBeneficiario;
		this.identificador_ben_id = identificador_ben_id;
		this.claveNombre = claveNombre;
		this.nombreContratante = nombreContratante;
		this.importePrima = importePrima;
		this.importeBs = importeBs;
		this.importeDerpol = importeDerpol;
		this.importeCumMeta = importeCumMeta;
		this.importeUtilidad = importeUtilidad;
		this.subtotal = subtotal;
		this.iva = iva;
		this.ivaRetenido = ivaRetenido;
		this.isr = isr;
		this.estatusFactura = estatusFactura;
		this.estatusOrdenpago = estatusOrdenpago;
		this.importeTotal = importeTotal;
		this.fechaCorte = fechaCorte;
		this.fechaCreacion = fechaCreacion;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CAORDENPAGOS_ID_SEQ")
	@SequenceGenerator(name = "CAORDENPAGOS_ID_SEQ", schema = "MIDAS", sequenceName = "CAORDENPAGOS_ID_SEQ", allocationSize = 1)
	@Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setFolio(Long folio) {
		this.folio = folio;
	}
	@Column (name = "FOLIO")
	public Long getFolio() {
		return folio;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "TIPOENTIDAD_ID")
	public CaTipoEntidad getCaTipoentidad() {
		return this.caTipoEntidad;
	}

	public void setCaTipoentidad(CaTipoEntidad caTipoEntidad) {
		this.caTipoEntidad = caTipoEntidad;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "RAMO_ID")
	public CaRamo getCaRamo() {
		return this.caRamo;
	}

	public void setCaRamo(CaRamo caRamo) {
		this.caRamo = caRamo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPENSACION_ID")
	public CaCompensacion getCaCompensacion() {
		return caCompensacion;
	}
	public void setCaCompensacion(CaCompensacion caCompensacion) {
		this.caCompensacion = caCompensacion;
	}
	
	@Column (name = "IDTOPOLIZA")
	public Long getIdToPoliza() {
		return idToPoliza;
	}
	public void setIdToPoliza(Long idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ENTIDADPERSONA_ID")
	public CaEntidadPersona getCaEntidadPersona() {
		return caEntidadPersona;
	}

	public void setCaEntidadPersona(CaEntidadPersona caEntidadPersona) {
		this.caEntidadPersona = caEntidadPersona;
	}
	
	@Column(name = "NOMBRE_BENEFICIARIO")
	public String getNombreBeneficiario() {
		return nombreBeneficiario;
	}

	public void setNombreBeneficiario(String nombreBeneficiario) {
		this.nombreBeneficiario = nombreBeneficiario;
	}

	@Column(name = "CLAVE_NOMBRE")
	public String getClaveNombre() {
		return this.claveNombre;
	}

	public void setClaveNombre(String claveNombre) {
		this.claveNombre = claveNombre;
	}

	@Column(name = "NOMBRE_CONTRATANTE")
	public String getNombreContratante() {
		return this.nombreContratante;
	}

	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}

	@Column(name = "IMPORTE_PRIMA")
	public Double getImportePrima() {
		return this.importePrima;
	}

	public void setImportePrima(Double importePrima) {
		this.importePrima = importePrima;
	}

	@Column(name = "IMPORTE_BS")
	public Double getImporteBs() {
		return this.importeBs;
	}

	public void setImporteBs(Double importeBs) {
		this.importeBs = importeBs;
	}

	@Column(name = "IMPORTE_DERPOL")
	public Double getImporteDerpol() {
		return this.importeDerpol;
	}

	public void setImporteDerpol(Double importeDerpol) {
		this.importeDerpol = importeDerpol;
	}

	@Column(name = "IMPORTE_CUM_META")
	public Double getImporteCumMeta() {
		return this.importeCumMeta;
	}

	public void setImporteCumMeta(Double importeCumMeta) {
		this.importeCumMeta = importeCumMeta;
	}

	@Column(name = "IMPORTE_UTILIDAD")
	public Double getImporteUtilidad() {
		return this.importeUtilidad;
	}

	public void setImporteUtilidad(Double importeUtilidad) {
		this.importeUtilidad = importeUtilidad;
	}

	@Column(name = "SUBTOTAL")
	public Double getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(Double subtotal) {
		this.subtotal = subtotal;
	}

	@Column(name = "IVA")
	public Double getIva() {
		return this.iva;
	}

	public void setIva(Double iva) {
		this.iva = iva;
	}

	@Column(name = "IVA_RETENIDO")
	public Double getIvaRetenido() {
		return this.ivaRetenido;
	}

	public void setIvaRetenido(Double ivaRetenido) {
		this.ivaRetenido = ivaRetenido;
	}

	@Column(name = "ISR")
	public Double getIsr() {
		return this.isr;
	}

	public void setIsr(Double isr) {
		this.isr = isr;
	}

	@Column(name = "ESTATUS_FACTURA", precision = 0)
	public Short getEstatusFactura() {
		return this.estatusFactura;
	}

	public void setEstatusFactura(Short estatusFactura) {
		this.estatusFactura = estatusFactura;
	}

	@Column(name = "ESTATUS_ORDENPAGO", precision = 0)
	public Short getEstatusOrdenpago() {
		return this.estatusOrdenpago;
	}

	public void setEstatusOrdenpago(Short estatusOrdenpago) {
		this.estatusOrdenpago = estatusOrdenpago;
	}

	@Column(name = "IMPORTE_TOTAL")
	public Double getImporteTotal() {
		return this.importeTotal;
	}

	public void setImporteTotal(Double importeTotal) {
		this.importeTotal = importeTotal;
	}

	@Column(name = "FECHA_CORTE", length = 7)
	public Timestamp getFechaCorte() {
		return this.fechaCorte;
	}

	public void setFechaCorte(Timestamp fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	@Column(name = "FECHA_CREACION", length = 7)
	public Timestamp getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Timestamp fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "TIPOCOMPENSACION_ID")
	public CaTipoCompensacion getCaTipoCompensacion() {
		return caTipoCompensacion;
	}

	public void setCaTipoCompensacion(CaTipoCompensacion caTipoCompensacion) {
		this.caTipoCompensacion = caTipoCompensacion;
	}

	public void setIncluir(Boolean incluir) {
		this.incluir = incluir;
	}
	@Transient
	public Boolean getIncluir() {
		return incluir;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "TOPROMOTORIA_ID")
	public Promotoria getPromotoria() {
		return promotoria;
	}

	public void setPromotoria(Promotoria promotoria) {
		this.promotoria = promotoria;
	}
	
	@Column (name = "IDENTIFICADOR_BEN_ID")
	public Long getIdentificador_ben_id() {
		return identificador_ben_id;
	}

	public void setIdentificador_ben_id(Long identificador_ben_id) {
		this.identificador_ben_id = identificador_ben_id;
	}

}