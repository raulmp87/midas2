package mx.com.afirme.midas2.domain.compensaciones;

import java.io.Serializable;
import java.util.Date;

public class CaReportesDerPolDTO implements java.io.Serializable {	

	private static final long serialVersionUID = 6298762337579824994L;
		private String ramo;
		private Long idCompensacion;
		private String noPoliza;
		private Long numeroEndoso;
		private Date fechaEmision;
		private Long clave;
		private String razonSocial;
		private Double pct_pag_dp;
		private Double mto_pag_dp;
		private Double importeDerpol;
		private String frec_pago;
		private Long idNegocio;				
		private String nombreNegocio;
		
		public String getRamo() {
			return ramo;
		}
		public String getNoPoliza() {
			return noPoliza;
		}
		public Long getIdCompensacion() {
			return idCompensacion;
		}
		public String getRazonSocial() {
			return razonSocial;
		}
		public String getFrec_pago() {
			return frec_pago;
		}
		public Long getIdNegocio() {
			return idNegocio;
		}
		public String getNombreNegocio() {
			return nombreNegocio;
		}
		public void setRamo(String ramo) {
			this.ramo = ramo;
		}
		public void setNoPoliza(String noPoliza) {
			this.noPoliza = noPoliza;
		}
		public void setIdCompensacion(Long idCompensacion) {
			this.idCompensacion = idCompensacion;
		}
		public void setRazonSocial(String razonSocial) {
			this.razonSocial = razonSocial;
		}
		public void setFrec_pago(String frec_pago) {
			this.frec_pago = frec_pago;
		}
		public void setIdNegocio(Long idNegocio) {
			this.idNegocio = idNegocio;
		}
		public void setNombreNegocio(String nombreNegocio) {
			this.nombreNegocio = nombreNegocio;
		}
		public Long getClave() {
			return clave;
		}
		public void setClave(Long clave) {
			this.clave = clave;
		}
		public Double getImporteDerpol() {
			return importeDerpol;
		}
		public void setImporteDerpol(Double importeDerpol) {
			this.importeDerpol = importeDerpol;
		}	
		public Double getPct_pag_dp() {
			return pct_pag_dp;
		}
		public Double getMto_pag_dp() {
			return mto_pag_dp;
		}
		public void setPct_pag_dp(Double pct_pag_dp) {
			this.pct_pag_dp = pct_pag_dp;
		}
		public void setMto_pag_dp(Double mto_pag_dp) {
			this.mto_pag_dp = mto_pag_dp;
		}

		public Date getFechaEmision() {
			return fechaEmision;
		}
		public void setFechaEmision(Date fechaEmision) {
			this.fechaEmision = fechaEmision;
		}
		public Long getNumeroEndoso() {
			return numeroEndoso;
		}
		public void setNumeroEndoso(Long numeroEndoso) {
			this.numeroEndoso = numeroEndoso;
		}


		public static class DatosRepDerPolParametrosDTO implements Serializable {

			private static final long serialVersionUID = -8483471044796021391L;
			private Short tipoReporte;
			private String agente;
			private String promotor;
			private String ramo;
			private Long idNegocio;
			private String nombreNegocio;
			private Date fechaInicio;
			private Date fechaFin;
			
			public DatosRepDerPolParametrosDTO(Short tipoReporte,String agente,String promotor,
					 String ramo, Long idNegocio,String nombreNegocio,
					 Date fechaInicio,Date fechaFin) {
				super();
				this.tipoReporte = tipoReporte;
				this.agente = agente;
				this.promotor = promotor;
				this.ramo = ramo;
				this.idNegocio= idNegocio;
				this.nombreNegocio = nombreNegocio;
				this.fechaInicio = fechaInicio;
				this.fechaFin = fechaFin;
			}
			
			public Short getTipoReporte() {
				return tipoReporte;
			}
			public String getAgente() {
				return agente;
			}
			public String getPromotor() {
				return promotor;
			}
			public String getRamo() {
				return ramo;
			}
			public Long getIdNegocio() {
				return idNegocio;
			}
			public String getNombreNegocio() {
				return nombreNegocio;
			}
			public void setTipoReporte(Short tipoReporte) {
				this.tipoReporte = tipoReporte;
			}
			public void setAgente(String agente) {
				this.agente = agente;
			}
			public void setPromotor(String promotor) {
				this.promotor = promotor;
			}
			public void setRamo(String ramo) {
				this.ramo = ramo;
			}
			public void setIdNegocio(Long idNegocio) {
				this.idNegocio = idNegocio;
			}
			public void setNombreNegocio(String nombreNegocio) {
				this.nombreNegocio = nombreNegocio;
			}

			public Date getFechaInicio() {
				return fechaInicio;
			}

			public Date getFechaFin() {
				return fechaFin;
			}

			public void setFechaInicio(Date fechaInicio) {
				this.fechaInicio = fechaInicio;
			}

			public void setFechaFin(Date fechaFin) {
				this.fechaFin = fechaFin;
			}
			
			
		}	
	
}