package mx.com.afirme.midas2.domain.compensaciones;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="CA_BANCA_GERENCIA",schema="MIDAS")

public class CaBancaGerencia {
	

	private Long id;
	private Long configuracionBancaId;
	private Integer idEmpresa;
    private String idGerencia;
    private Date fsit;
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Id 
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CABANCAGERENCIA_ID_SEQ")
	@SequenceGenerator(name = "CABANCAGERENCIA_ID_SEQ",  schema="MIDAS", sequenceName = "CABANCAGERENCIA_ID_SEQ", allocationSize = 1)
    @Column(name="ID", unique=true, nullable=false, precision=10, scale=0)
	public Long getId() {
		return id;
	}
	
	public void setConfiguracionBancaId(Long configuracionBancaId) {
		this.configuracionBancaId = configuracionBancaId;
	}

	@Column(name="CABANCACONFIGURACION_ID")
	public Long getConfiguracionBancaId() {
		return configuracionBancaId;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	@Column(name="ID_EMPRESA")
	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdGerencia(String idGerencia) {
		this.idGerencia = idGerencia;
	}

	@Column(name="ID_GERENCIA", nullable=false, length=3)
	public String getIdGerencia() {
		return idGerencia;
	}

	public void setFsit(Date fsit) {
		this.fsit = fsit;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="F_SIT", columnDefinition="DATE")
	public Date getFsit() {
		return fsit;
	}

	
	
}
