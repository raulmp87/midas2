package mx.com.afirme.midas2.domain.compensaciones;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="CA_CALCULOBANCA"
    ,schema="MIDAS"
)

public class CaCalculoBanca  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
     private Long id;
     private CaTipoCompensacion caTipoCompensacion;
     private CaRamo caRamo;
     private CaBaseCalculo caBaseCalculo;     
     private Long lineaNegocioId;
     private BigDecimal monto;
     private BigDecimal porcentaje;
     private Date fechaCreacion;
     private Date fechaModificacion;
     private String usuario;
     private Boolean borradoLogico;


    public CaCalculoBanca() {
    }


    public CaCalculoBanca(Long id) {
        this.id = id;
    }
   
    public CaCalculoBanca(Long id, CaTipoCompensacion caTipoCompensacion,
			CaRamo caRamo, CaBaseCalculo caBaseCalculo,
			Long lineaNegocioId, BigDecimal monto, BigDecimal porcentaje,
			Date fechaCreacion, Date fechaModificacion, String usuario,
			Boolean borradoLogico) {
		super();
		this.id = id;
		this.caTipoCompensacion = caTipoCompensacion;
		this.caRamo = caRamo;
		this.caBaseCalculo = caBaseCalculo;
		this.lineaNegocioId = lineaNegocioId;
		this.monto = monto;
		this.porcentaje = porcentaje;
		this.fechaCreacion = fechaCreacion;
		this.fechaModificacion = fechaModificacion;
		this.usuario = usuario;
		this.borradoLogico = borradoLogico;
	}


	@Id 
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CACALCULOBANCA_ID_SEQ")
	@SequenceGenerator(name = "CACALCULOBANCA_ID_SEQ",  schema="MIDAS", sequenceName = "CACALCULOBANCA_ID_SEQ", allocationSize = 1)
    @Column(name="ID", unique=true, nullable=false, precision=10, scale=0)

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="TIPOCOMPENSACION_ID")

    public CaTipoCompensacion getCaTipoCompensacion() {
        return this.caTipoCompensacion;
    }
    
    public void setCaTipoCompensacion(CaTipoCompensacion caTipoCompensacion) {
        this.caTipoCompensacion = caTipoCompensacion;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="RAMO_ID")

    public CaRamo getCaRamo() {
        return this.caRamo;
    }
    
    public void setCaRamo(CaRamo caRamo) {
        this.caRamo = caRamo;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="BASECALCULO_ID")

    public CaBaseCalculo getCaBaseCalculo() {
        return this.caBaseCalculo;
    }
    
    public void setCaBaseCalculo(CaBaseCalculo caBaseCalculo) {
        this.caBaseCalculo = caBaseCalculo;
    }
		
    @Column(name="LINEANEGOCIO_ID", precision=10, scale=0)

    public Long getLineaNegocioId() {
        return this.lineaNegocioId;
    }
    
    public void setLineaNegocioId(Long lineaNegocioId) {
        this.lineaNegocioId = lineaNegocioId;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHACREACION", length=7)

    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }
    
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAMODIFICACION", length=7)

    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }
    
    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
    
    @Column(name="USUARIO")

    public String getUsuario() {
        return this.usuario;
    }
    
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    @Column(name="BORRADOLOGICO", precision=1, scale=0)

    public Boolean getBorradoLogico() {
        return this.borradoLogico;
    }
    
    public void setBorradoLogico(Boolean borradoLogico) {
        this.borradoLogico = borradoLogico;
    }


	public BigDecimal getMonto() {
		return monto;
	}


	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}


	public BigDecimal getPorcentaje() {
		return porcentaje;
	}


	public void setPorcentaje(BigDecimal porcentaje) {
		this.porcentaje = porcentaje;
	}

}