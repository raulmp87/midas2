package mx.com.afirme.midas2.domain.compensaciones;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="CA_BITACORA",schema="MIDAS")

public class CaBitacora  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	private Long id;
	private String usuario;
	private Date fecha;
	private String movimiento;
	private Long compensacionId;
	private Long configuracionBancaId;
	private Long confgiracionBancaCalculoId;

    public CaBitacora(){
    }
    
    public CaBitacora(Long id) {
        this.id = id;
    }


    public CaBitacora(String usuario, Date fecha, String movimiento, Long compensacionId) {
        this.usuario = usuario;
        this.fecha = fecha;
        this.movimiento = movimiento;
        this.compensacionId = compensacionId;
    }
    

    @Id 
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CABITACORA_ID_SEQ")
	@SequenceGenerator(name = "CABITACORA_ID_SEQ",  schema="MIDAS", sequenceName = "CABITACORA_ID_SEQ", allocationSize = 1)
    @Column(name="ID", unique=true, nullable=false, precision=10, scale=0)
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    @Column(name="USUARIO")
    public String getUsuario() {
        return this.usuario;
    }
    
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="FECHA", nullable=false)
    public Date getFecha() {
        return this.fecha;
    }
    
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Column(name="MOVIMIENTO")
    public String getMovimiento() {
        return this.movimiento;
    }
    
    public void setMovimiento(String movimiento) {
        this.movimiento = movimiento;
    }    

    @Column(name="COMPENSACION_ID")
	public Long getCompensacionId() {
		return compensacionId;
	}


	public void setCompensacionId(Long compensacionId) {
		this.compensacionId = compensacionId;
	}

	public void setConfiguracionBancaId(Long configuracionBancaId) {
		this.configuracionBancaId = configuracionBancaId;
	}

	@Column(name="CABANCACONFIGURACION_ID")
	public Long getConfiguracionBancaId() {
		return configuracionBancaId;
	}
	
	@Column(name="CABANCACONFIGCAL_ID")
	public Long getConfgiracionBancaCalculoId() {
		return confgiracionBancaCalculoId;
	}

	public void setConfgiracionBancaCalculoId(Long confgiracionBancaCalculoId) {
		this.confgiracionBancaCalculoId = confgiracionBancaCalculoId;
	}
   
}