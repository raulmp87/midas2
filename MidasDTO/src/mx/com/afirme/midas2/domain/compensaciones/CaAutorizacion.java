package mx.com.afirme.midas2.domain.compensaciones;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="CA_AUTORIZACION"
    ,schema="MIDAS"
)

public class CaAutorizacion  implements java.io.Serializable {
     
	private static final long serialVersionUID = 1L;
	private Long id;
     private CaTipoAutorizacion caTipoAutorizacion;
     private CaEstatus caEstatus;
     private CaCompensacion caCompensacion;
     private String valor;
     private Date fechaCreacion;
     private Date fechaModificacion;
     private String usuario;
     private Boolean borradoLogico;

    public CaAutorizacion() {
    }

    public CaAutorizacion(Long id) {
        this.id = id;
    }

    public CaAutorizacion(Long id, CaTipoAutorizacion caTipoAutorizacion,CaEstatus caEstatus, CaCompensacion caCompensacion, String valor, Date fechacreacion, Date fechamodificacion, String usuario, Boolean borradologico) {
        this.id = id;
        this.caTipoAutorizacion = caTipoAutorizacion;
        this.caCompensacion = caCompensacion;
        this.caEstatus = caEstatus;
        this.valor = valor;
        this.fechaCreacion = fechacreacion;
        this.fechaModificacion = fechamodificacion;
        this.usuario = usuario;
        this.borradoLogico = borradologico;
    }

    @Id 
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CAAUTORIZACION_ID_SEQ")
	@SequenceGenerator(name = "CAAUTORIZACION_ID_SEQ",  schema="MIDAS", sequenceName = "CAAUTORIZACION_ID_SEQ", allocationSize = 1)
    @Column(name="ID", unique=true, nullable=false, precision=10, scale=0)

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="TIPOAUTORIZACION_ID")

    public CaTipoAutorizacion getCaTipoAutorizacion() {
        return this.caTipoAutorizacion;
    }
    
    public void setCaTipoAutorizacion(CaTipoAutorizacion caTipoAutorizacion) {
        this.caTipoAutorizacion = caTipoAutorizacion;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="COMPENSACION_ID")

    public CaCompensacion getCaCompensacion() {
        return this.caCompensacion;
    }
    
    public void setCaCompensacion(CaCompensacion caCompensacion) {
        this.caCompensacion = caCompensacion;
    }
    
    @Column(name="VALOR")

    public String getValor() {
        return this.valor;
    }
    
    public void setValor(String valor) {
        this.valor = valor;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHACREACION", length=7)

    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }
    
    public void setFechaCreacion(Date fechacreacion) {
        this.fechaCreacion = fechacreacion;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAMODIFICACION", length=7)

    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }
    
    public void setFechaModificacion(Date fechamodificacion) {
        this.fechaModificacion = fechamodificacion;
    }
    
    @Column(name="USUARIO")

    public String getUsuario() {
        return this.usuario;
    }
    
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    @Column(name="BORRADOLOGICO", precision=1, scale=0)

    public Boolean getBorradoLogico() {
        return this.borradoLogico;
    }
    
    public void setBorradoLogico(Boolean borradologico) {
        this.borradoLogico = borradologico;
    }
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="ESTATUS_ID")
	public CaEstatus getCaEstatus() {
		return caEstatus;
	}

	public void setCaEstatus(CaEstatus caEstatus) {
		this.caEstatus = caEstatus;
	}
}