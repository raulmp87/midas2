package mx.com.afirme.midas2.domain.compensaciones;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;

@Entity
@Table(name="CA_SUBRAMOSDANIOS"
    ,schema="MIDAS"
)

public class CaSubramosDanios  implements java.io.Serializable {
	
	 private static final long serialVersionUID = 1L;
     private Long id; 
     private CotizacionDTO cotizacionDTO;
     private CotizacionDTO cotizacionEndosoDTO;
     private SubRamoDTO subRamoDTO;
	 private CaParametros caParametros;
     private BigDecimal porcentajeCompensacion;
     private Date fechaCreacion;
     private Date fechaModificacion;
     private String usuario;
     private Boolean borradoLogico;
     
    public CaSubramosDanios() {
    }
    
    public CaSubramosDanios(Long id) {
        this.id = id;
    }


    public CaSubramosDanios(Long id, CotizacionDTO cotizacionDTO, SubRamoDTO subRamoDTO, CaParametros caParametros,	BigDecimal porcentajeCompensacion, Date fechaCreacion, Date fechaModificacion, String usuario, Boolean borradoLogico) {
        this.id = id;
		this.cotizacionDTO= cotizacionDTO;
		this.subRamoDTO = subRamoDTO;
		this.caParametros = caParametros;
        this.porcentajeCompensacion = porcentajeCompensacion;
        this.fechaCreacion = fechaCreacion;	
        this.fechaModificacion = fechaModificacion;
        this.usuario = usuario;
        this.borradoLogico = borradoLogico;
    }


    @Id 
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CASUBRAMOSDANIOS_ID_SEQ")
	@SequenceGenerator(name = "CASUBRAMOSDANIOS_ID_SEQ",  schema="MIDAS", sequenceName = "CASUBRAMOSDANIOS_ID_SEQ", allocationSize = 1)
    @Column(name="ID", unique=true, nullable=false, precision=10, scale=0)

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }    
    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COTIZACION_ID")	
	public CotizacionDTO getCotizacionDTO() {
		return cotizacionDTO;
	}

	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TCSUBRAMO_ID")
	public SubRamoDTO getSubRamoDTO() {
		return subRamoDTO;
	}

	public void setSubRamoDTO(SubRamoDTO subRamoDTO) {
		this.subRamoDTO = subRamoDTO;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PARAMETROS_ID")
	public CaParametros getCaParametros() {
		return this.caParametros;
	}

	public void setCaParametros(CaParametros caParametros) {
		this.caParametros = caParametros;
	}
    
    @Column(name="PORCENTAJECOMPENSACION")
    public BigDecimal getPorcentajeCompensacion() {
        return this.porcentajeCompensacion;
    }
    
	public void setPorcentajeCompensacion(BigDecimal porcentajeCompensacion) {
        this.porcentajeCompensacion = porcentajeCompensacion;
        
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHACREACION", length=7)

    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }    
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAMODIFICACION", length=7)

    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }
    
    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
    
    @Column(name="USUARIO")
    public String getUsuario() {
        return this.usuario;
    }
    
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    @Column(name="BORRADOLOGICO", precision=1, scale=0)
    public Boolean getBorradoLogico() {
        return this.borradoLogico;
    }
    
    public void setBorradoLogico(Boolean borradoLogico) {
        this.borradoLogico = borradoLogico;
    }

	public void setCotizacionEndosoDTO(CotizacionDTO cotizacionEndosoDTO) {
		this.cotizacionEndosoDTO = cotizacionEndosoDTO;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COTIZACIONENDOSO_ID")	
	public CotizacionDTO getCotizacionEndosoDTO() {
		return cotizacionEndosoDTO;
	}
 
}