package mx.com.afirme.midas2.domain.compensaciones.negociovida;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * NegocioVida entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CA_NEGOCIOVIDA", schema = "MIDAS")
public class NegocioVida implements java.io.Serializable,Entidad {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -5217099890945074904L;
	private Long id;
	private String codigo;
	private String descripcionnegociovida;
	private String usuario;
	private Date fechacreacion;
	private Short bajalogica;	
	private List<NegocioVidaAgente> negocioVidaAgentes;
	private List<NegocioVidaGerencia> negocioVidaGerencias;
	private List<NegocioVidaSubRamo> negocioVidaSubRamos;
	private List<NegocioVidaProducto> negocioVidaProductos ;
	private List<NegocioVidaSeccion> negocioVidaSeccions;
	private List<NegocioVidaRamo> negocioVidaRamos;
			

	// Constructors

	/** default constructor */
	public NegocioVida() {
	}

	/** minimal constructor */
	public NegocioVida(Long id) {
		this.id = id;
	}
	public NegocioVida(Long id,String descripcionnegociovida) {
		this.id = id;
		this.descripcionnegociovida=descripcionnegociovida;
	}

	/** full constructor */
	public NegocioVida(Long id, String codigo, String descripcionnegociovida,
			String usuario, Date fechacreacion, Short bajalogica,
			List<NegocioVidaAgente> negocioVidaAgentes,
			List<NegocioVidaGerencia> negocioVidaGerencias,
			List<NegocioVidaSubRamo> negocioVidaSubRamos,
			List<NegocioVidaProducto> negocioVidaProductos,
			List<NegocioVidaSeccion> negocioVidaSeccions,
			List<NegocioVidaRamo> negocioVidaRamos) {
		this.id = id;
		this.codigo = codigo;
		this.descripcionnegociovida = descripcionnegociovida;
		this.usuario = usuario;
		this.fechacreacion = fechacreacion;
		this.bajalogica = bajalogica;
		this.negocioVidaAgentes = negocioVidaAgentes;
		this.negocioVidaGerencias = negocioVidaGerencias;
		this.negocioVidaSubRamos = negocioVidaSubRamos;
		this.negocioVidaProductos = negocioVidaProductos;
		this.negocioVidaSeccions = negocioVidaSeccions;
		this.negocioVidaRamos = negocioVidaRamos;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CANEGOCIOVIDA_ID_SEQ")
	@SequenceGenerator(name="CANEGOCIOVIDA_ID_SEQ" , schema="MIDAS",sequenceName="CANEGOCIOVIDA_ID_SEQ",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "CODIGO", length = 100)
	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Column(name = "DESCRIPCIONNEGOCIOVIDA", length = 200)
	public String getDescripcionnegociovida() {
		return this.descripcionnegociovida;
	}

	public void setDescripcionnegociovida(String descripcionnegociovida) {
		this.descripcionnegociovida = descripcionnegociovida;
	}

	@Column(name = "USUARIO", length = 50)
	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHACREACION")
	public Date getFechacreacion() {
		return this.fechacreacion;
	}

	public void setFechacreacion(Date fechacreacion) {
		this.fechacreacion = fechacreacion;
	}

	@Column(name = "BAJALOGICA", precision = 4, scale = 0)
	public Short getBajalogica() {
		return this.bajalogica;
	}

	public void setBajalogica(Short bajalogica) {
		this.bajalogica = bajalogica;
	}

	@Transient
	public List<NegocioVidaAgente> getNegocioVidaAgentes() {
		return this.negocioVidaAgentes;
	}

	public void setNegocioVidaAgentes(List<NegocioVidaAgente> negocioVidaAgentes) {
		this.negocioVidaAgentes = negocioVidaAgentes;
	}

	@Transient
	public List<NegocioVidaGerencia> getNegocioVidaGerencias() {
		return this.negocioVidaGerencias;
	}

	public void setNegocioVidaGerencias(
			List<NegocioVidaGerencia> negocioVidaGerencias) {
		this.negocioVidaGerencias = negocioVidaGerencias;
	}

	@Transient
	public List<NegocioVidaSubRamo> getNegocioVidaSubRamos() {
		return this.negocioVidaSubRamos;
	}

	public void setNegocioVidaSubRamos(
			List<NegocioVidaSubRamo> negocioVidaSubRamos) {
		this.negocioVidaSubRamos = negocioVidaSubRamos;
	}

	@Transient
	public List<NegocioVidaProducto> getNegocioVidaProductos() {
		return this.negocioVidaProductos;
	}

	public void setNegocioVidaProductos(
			List<NegocioVidaProducto> negocioVidaProductos) {
		this.negocioVidaProductos = negocioVidaProductos;
	}

	@Transient
	public List<NegocioVidaSeccion> getNegocioVidaSeccions() {
		return this.negocioVidaSeccions;
	}

	public void setNegocioVidaSeccions(
			List<NegocioVidaSeccion> negocioVidaSeccions) {
		this.negocioVidaSeccions = negocioVidaSeccions;
	}

	@Transient
	public List<NegocioVidaRamo> getNegocioVidaRamos() {
		return this.negocioVidaRamos;
	}

	public void setNegocioVidaRamos(List<NegocioVidaRamo> negocioVidaRamos) {
		this.negocioVidaRamos = negocioVidaRamos;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long  getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

}