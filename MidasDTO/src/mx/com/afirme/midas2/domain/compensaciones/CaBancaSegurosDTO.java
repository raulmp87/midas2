package mx.com.afirme.midas2.domain.compensaciones;

import java.util.Date;

public class CaBancaSegurosDTO implements java.io.Serializable {

	private static final long serialVersionUID  = 8938331556170465543L;
	
	private String numeroPoliza;
	private String gerencia;
	private String contratante;
	private Double primaEmitida;
	private Double primaPagada;
	private Double primaDevengada;
	private Double costoSiniestralidad;
	private Date fechaCreacion;

	
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public String getGerencia() {
		return gerencia;
	}
	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}
	public String getContratante() {
		return contratante;
	}
	public void setContratante(String contratante) {
		this.contratante = contratante;
	}
	public Double getPrimaEmitida() {
		return primaEmitida;
	}
	public void setPrimaEmitida(Double primaEmitida) {
		this.primaEmitida = primaEmitida;
	}
	public Double getPrimaPagada() {
		return primaPagada;
	}
	public void setPrimaPagada(Double primaPagada) {
		this.primaPagada = primaPagada;
	}
	public Double getPrimaDevengada() {
		return primaDevengada;
	}
	public void setPrimaDevengada(Double primaDevengada) {
		this.primaDevengada = primaDevengada;
	}
	public Double getCostoSiniestralidad() {
		return costoSiniestralidad;
	}
	public void setCostoSiniestralidad(Double costoSiniestralidad) {
		this.costoSiniestralidad = costoSiniestralidad;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
}
