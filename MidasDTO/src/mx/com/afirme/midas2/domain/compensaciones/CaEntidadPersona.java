package mx.com.afirme.midas2.domain.compensaciones;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;

@Entity
@Table(name="CA_ENTIDADPERSONA"
    ,schema="MIDAS"
)

public class CaEntidadPersona  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
     private Long id;
     private Agente agente;
     private Integer idEmpresa;
     private Integer idAgente;
     private Date fsit;
     private Long idProveedor;
     private CaTipoEntidad caTipoEntidad;
     private CaCompensacion caCompensacion;
     private String nombres;    
     private Date fechaCreacion;
     private Date fechaModificacion;
     private String usuario;
     private Boolean borradoLogico;
     private CaTipoContrato caTipoContrato;
     private boolean cuadernoConcurso;
     private boolean convenioEspecial;
     private String sinCompensacion;

    public CaEntidadPersona() {
    }
    
	public CaEntidadPersona(Long id) {
        this.id = id;
    }
    
	public CaEntidadPersona(Long id, Agente agente,
			CaTipoEntidad caTipoEntidad, CaCompensacion caCompensacion,
			String nombres, Long idProveedor,
			Date fechaCreacion, Date fechaModificacion, String usuario,
			Boolean borradoLogico) {
		super();
		this.id = id;
		this.agente = agente;
		this.caTipoEntidad = caTipoEntidad;
		this.caCompensacion = caCompensacion;
		this.nombres = nombres;
		this.idProveedor = idProveedor;
		this.fechaCreacion = fechaCreacion;
		this.fechaModificacion = fechaModificacion;
		this.usuario = usuario;
		this.borradoLogico = borradoLogico;
	}

	@Id 
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CAENTIDADPERSONA_ID_SEQ")
	@SequenceGenerator(name = "CAENTIDADPERSONA_ID_SEQ",  schema="MIDAS", sequenceName = "CAENTIDADPERSONA_ID_SEQ", allocationSize = 1)
    @Column(name="ID", unique=true, nullable=false, precision=10, scale=0)

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="TOAGENTE_ID")
    public Agente getAgente() {
        return this.agente;
    }    
    public void setAgente(Agente agente) {
        this.agente = agente;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="TIPOENTIDAD_ID")

    public CaTipoEntidad getCaTipoEntidad() {
        return this.caTipoEntidad;
    }
    
    public void setCaTipoEntidad(CaTipoEntidad tipoentidadca) {
        this.caTipoEntidad = tipoentidadca;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="COMPENSACION_ID")

    public CaCompensacion getCaCompensacion() {
        return this.caCompensacion;
    }
    
    public void setCaCompensacion(CaCompensacion caCompensacion) {
        this.caCompensacion = caCompensacion;
    }
    
    @Column(name="NOMBRES")

    public String getNombres() {
        return this.nombres;
    }
    
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }
    
    @Column(name="PROVEEDOR_ID")

    public Long getIdProveedor() {
        return this.idProveedor;
    }
    
    public void setIdProveedor(Long idProveedor) {
        this.idProveedor = idProveedor;
    }
    
    @Temporal(TemporalType.DATE)
    @Column(name="FECHACREACION", length=7)

    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }
    
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAMODIFICACION", length=7)

    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }
    
    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
    
    @Column(name="USUARIO")

    public String getUsuario() {
        return this.usuario;
    }
    
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    @Column(name="BORRADOLOGICO", precision=1, scale=0)

    public Boolean getBorradoLogico() {
        return this.borradoLogico;
    }
    
    public void setBorradoLogico(Boolean borradoLogico) {
        this.borradoLogico = borradoLogico;
    }
    
    public void setCaTipoContrato(CaTipoContrato caTipoContrato) {
		this.caTipoContrato = caTipoContrato;
	}

	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="TIPOCONTRATO_ID")
	public CaTipoContrato getCaTipoContrato() {
		return caTipoContrato;
	}


	public void setCuadernoConcurso(boolean cuadernoConcurso) {
		this.cuadernoConcurso = cuadernoConcurso;
	}

	@Column(name="CUADERNOCONCURSO", precision=1, scale=0)
	public boolean isCuadernoConcurso() {
		return cuadernoConcurso;
	}


	public void setConvenioEspecial(boolean convenioEspecial) {
		this.convenioEspecial = convenioEspecial;
	}

	@Column(name="CONVENIOESPECIAL", precision=1, scale=0)
	public boolean isConvenioEspecial() {
		return convenioEspecial;
	}
	
	@Column(name="ID_EMPRESA", precision=8, scale=0)
	public Integer getIdEmpresa() {
		return idEmpresa;
	}
	
	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	
    @Column(name="ID_AGENTE", precision=8, scale=0)
	public Integer getIdAgente() {
		return idAgente;
	}
    
    public void setIdAgente(Integer idAgente) {
		this.idAgente = idAgente;
	}

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="F_SIT", columnDefinition="DATE")
	public Date getFsit() {
		return fsit;
	}

	public void setFsit(Date fsit) {
		this.fsit = fsit;
	}
	@Transient
	public String getSinCompensacion() {
		return sinCompensacion;
	}

	public void setSinCompensacion(String sinCompensacion) {
		this.sinCompensacion = sinCompensacion;
	}
	
	
}