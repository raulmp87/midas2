package mx.com.afirme.midas2.domain.compensaciones;


public class CargaBancaSiniMes implements java.io.Serializable{

	 private Long id;
     private String ofsuc;
     private String nomOfsuc;
     private String nomGrupo;
     private String nomCanal;
     private String codCanal;
     private String canalNvo;
     private String agte;
     private String nomAgte;
     private String nomEjec;
     private String sec;
     private String nomRamo;
     private String idCentroEmision;
     private String numPoliza;
     private String numRenovPoliz;
     private String mon;
     private String asegurado;
     private String fecEmi;
     private String vigDes;
     private String vigHas;
     private Double pmaEmi;
     private Double pmaPagada;
     private Double pmaDev;
     private Double reclama;
     private Double gastos;
     private Double salvRecu;
     private Double costoSin;
     private Double porSin;
     private String lineaNeg;
     private Double anio;
     private Double mes;
     private Long bancaPrimpagCpId;
     
     public CargaBancaSiniMes(){

     }

     public CargaBancaSiniMes( String ofsuc,String nomOfsuc,String nomGrupo,String nomCanal,String codCanal,String canalNvo,String agte,String nomAgte,String nomEjec,String sec,String nomRamo,String idCentroEmision,String numPoliza,String numRenovPoliz,String mon,String asegurado,String fecEmi,String vigDes,String vigHas,Double pmaEmi,Double pmaPagada,Double pmaDev,Double reclama,Double gastos,Double salvRecu,Double costoSin,Double porSin,String lineaNeg, Double anio,Double mes,Long bancaPrimpagCpId){
        this.ofsuc= ofsuc;
        this.nomOfsuc = nomOfsuc;
        this.nomGrupo = nomGrupo;
        this.nomCanal = nomCanal;
        this.codCanal = codCanal;
        this.canalNvo = canalNvo;
        this.agte = agte;
        this.nomAgte = nomAgte;
        this.nomEjec = nomEjec;
        this.sec = sec;
        this.nomRamo= nomRamo;
        this.idCentroEmision= idCentroEmision;
        this.numPoliza = numPoliza;
        this.numRenovPoliz = numRenovPoliz;
        this.mon = mon;
        this.asegurado= asegurado;
        this.fecEmi = fecEmi;
        this.vigDes = vigDes;
        this.vigHas = vigHas;
        this.pmaEmi = pmaEmi;
        this.pmaPagada = pmaPagada;
        this.pmaDev = pmaDev;
        this.reclama = reclama;
        this.gastos = gastos;
        this.salvRecu = salvRecu;
        this.costoSin = costoSin;
        this.porSin = porSin;
        this.lineaNeg = lineaNeg;
        this.anio = anio;
        this.mes = mes;
        this.bancaPrimpagCpId = bancaPrimpagCpId;
     }

     
     public Long getId() {
         return this.id;
     }
     
     public void setId(Long id) {
         this.id = id;
     }
     
	public String getOfsuc() {
		return ofsuc;
	}

	public void setOfsuc(String ofsuc) {
		this.ofsuc = ofsuc;
	}

	public String getNomOfsuc() {
		return nomOfsuc;
	}

	public void setNomOfsuc(String nomOfsuc) {
		this.nomOfsuc = nomOfsuc;
	}

	public String getNomGrupo() {
		return nomGrupo;
	}

	public void setNomGrupo(String nomGrupo) {
		this.nomGrupo = nomGrupo;
	}

	public String getNomCanal() {
		return nomCanal;
	}

	public void setNomCanal(String nomCanal) {
		this.nomCanal = nomCanal;
	}

	public String getCodCanal() {
		return codCanal;
	}

	public void setCodCanal(String codCanal) {
		this.codCanal = codCanal;
	}

	public String getCanalNvo() {
		return canalNvo;
	}

	public void setCanalNvo(String canalNvo) {
		this.canalNvo = canalNvo;
	}

	public String getAgte() {
		return agte;
	}

	public void setAgte(String agte) {
		this.agte = agte;
	}

	public String getNomAgte() {
		return nomAgte;
	}

	public void setNomAgte(String nomAgte) {
		this.nomAgte = nomAgte;
	}

	public String getNomEjec() {
		return nomEjec;
	}

	public void setNomEjec(String nomEjec) {
		this.nomEjec = nomEjec;
	}

	public String getSec() {
		return sec;
	}

	public void setSec(String sec) {
		this.sec = sec;
	}

	public String getNomRamo() {
		return nomRamo;
	}

	public void setNomRamo(String nomRamo) {
		this.nomRamo = nomRamo;
	}

	public String getIdCentroEmision() {
		return idCentroEmision;
	}

	public void setIdCentroEmision(String idCentroEmision) {
		this.idCentroEmision = idCentroEmision;
	}

	public String getNumPoliza() {
		return numPoliza;
	}

	public void setNumPoliza(String numPoliza) {
		this.numPoliza = numPoliza;
	}

	public String getNumRenovPoliz() {
		return numRenovPoliz;
	}

	public void setNumRenovPoliz(String numRenovPoliz) {
		this.numRenovPoliz = numRenovPoliz;
	}

	public String getMon() {
		return mon;
	}

	public void setMon(String mon) {
		this.mon = mon;
	}

	public String getAsegurado() {
		return asegurado;
	}

	public void setAsegurado(String asegurado) {
		this.asegurado = asegurado;
	}

	public String getFecEmi() {
		return fecEmi;
	}

	public void setFecEmi(String fecEmi) {
		this.fecEmi = fecEmi;
	}

	public String getVigDes() {
		return vigDes;
	}

	public void setVigDes(String vigDes) {
		this.vigDes = vigDes;
	}

	public String getVigHas() {
		return vigHas;
	}

	public void setVigHas(String vigHas) {
		this.vigHas = vigHas;
	}

	public Double getPmaEmi() {
		return pmaEmi;
	}

	public void setPmaEmi(Double pmaEmi) {
		this.pmaEmi = pmaEmi;
	}

	public Double getPmaPagada() {
		return pmaPagada;
	}

	public void setPmaPagada(Double pmaPagada) {
		this.pmaPagada = pmaPagada;
	}

	public Double getPmaDev() {
		return pmaDev;
	}

	public void setPmaDev(Double pmaDev) {
		this.pmaDev = pmaDev;
	}

	public Double getReclama() {
		return reclama;
	}

	public void setReclama(Double reclama) {
		this.reclama = reclama;
	}

	public Double getGastos() {
		return gastos;
	}

	public void setGastos(Double gastos) {
		this.gastos = gastos;
	}

	public Double getSalvRecu() {
		return salvRecu;
	}

	public void setSalvRecu(Double salvRecu) {
		this.salvRecu = salvRecu;
	}

	public Double getCostoSin() {
		return costoSin;
	}

	public void setCostoSin(Double costoSin) {
		this.costoSin = costoSin;
	}

	public Double getPorSin() {
		return porSin;
	}

	public void setPorSin(Double porSin) {
		this.porSin = porSin;
	}

	public String getLineaNeg() {
		return lineaNeg;
	}

	public void setLineaNeg(String lineaNeg) {
		this.lineaNeg = lineaNeg;
	}

	public Double getAnio() {
		return anio;
	}

	public void setAnio(Double anio) {
		this.anio = anio;
	}

	public Double getMes() {
		return mes;
	}

	public void setMes(Double mes) {
		this.mes = mes;
	}

	public Long getBancaPrimpagCpId() {
		return bancaPrimpagCpId;
	}

	public void setBancaPrimpagCpId(Long bancaPrimpagCpId) {
		this.bancaPrimpagCpId = bancaPrimpagCpId;
	}


    

}
