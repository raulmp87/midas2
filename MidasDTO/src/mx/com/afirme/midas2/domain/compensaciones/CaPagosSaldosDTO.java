package mx.com.afirme.midas2.domain.compensaciones;

import java.math.BigDecimal;
import java.util.Date;



public class CaPagosSaldosDTO implements java.io.Serializable{
	private static final long serialVersionUID  = 8938331556170465543L;
	
	private Long nombreContratante;
	private String subRamo;
	private BigDecimal poliza;
	private Date fechaInicial;
	private Date fechaFinal;
	private BigDecimal numeroEndoso;
	private BigDecimal centroCosto;
	private String nombreBeneficiario;
	private String tipoPersona;
	private String tipoContrato;
	private BigDecimal saldoInicial;
	private BigDecimal pagado;
	private Date fechaPago;
	private String moneda;
	private BigDecimal tipoCambio;
	private BigDecimal saldoFinal;
	
	public Long getNombreContratante() {
		return nombreContratante;
	}
	public void setNombreContratante(Long nombreContratante) {
		this.nombreContratante = nombreContratante;
	}
	public String getSubRamo() {
		return subRamo;
	}
	public void setSubRamo(String subRamo) {
		this.subRamo = subRamo;
	}
	public BigDecimal getPoliza() {
		return poliza;
	}
	public void setPoliza(BigDecimal poliza) {
		this.poliza = poliza;
	}
	public Date getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public Date getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	public BigDecimal getNumeroEndoso() {
		return numeroEndoso;
	}
	public void setNumeroEndoso(BigDecimal numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}
	public BigDecimal getCentroCosto() {
		return centroCosto;
	}
	public void setCentroCosto(BigDecimal centroCosto) {
		this.centroCosto = centroCosto;
	}
	public String getNombreBeneficiario() {
		return nombreBeneficiario;
	}
	public void setNombreBeneficiario(String nombreBeneficiario) {
		this.nombreBeneficiario = nombreBeneficiario;
	}
	public String getTipoPersona() {
		return tipoPersona;
	}
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	public String getTipoContrato() {
		return tipoContrato;
	}
	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}
	public BigDecimal getSaldoInicial() {
		return saldoInicial;
	}
	public void setSaldoInicial(BigDecimal saldoInicial) {
		this.saldoInicial = saldoInicial;
	}
	public BigDecimal getPagado() {
		return pagado;
	}
	public void setPagado(BigDecimal pagado) {
		this.pagado = pagado;
	}
	public Date getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public BigDecimal getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(BigDecimal tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public BigDecimal getSaldoFinal() {
		return saldoFinal;
	}
	public void setSaldoFinal(BigDecimal saldoFinal) {
		this.saldoFinal = saldoFinal;
	}
	
}
