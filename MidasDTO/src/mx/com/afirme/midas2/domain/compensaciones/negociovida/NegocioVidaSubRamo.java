package mx.com.afirme.midas2.domain.compensaciones.negociovida;
// default package

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.emision.ppct.SubRamoSeycos;

/**
 * NegocioVidaSubRamo entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CA_TRNEGOCIOVIDA_SUBRAMO", schema = "MIDAS")
public class NegocioVidaSubRamo implements java.io.Serializable,Entidad  {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 2700865176770952923L;
	private Long id;
	private NegocioVida negocioVida;
	private SubRamoSeycos subRamo;

	// Constructors

	/** default constructor */
	public NegocioVidaSubRamo() {
	}

	/** minimal constructor */
	public NegocioVidaSubRamo(Long id) {
		this.id = id;
	}

	/** full constructor */
	public NegocioVidaSubRamo(Long id, NegocioVida negocioVida,
			SubRamoSeycos subRamo) {
		this.id = id;
		this.negocioVida = negocioVida;
		this.subRamo = subRamo;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CATRNEGVIDASUBRAMO_ID_SEQ")
	@SequenceGenerator(name="CATRNEGVIDASUBRAMO_ID_SEQ", schema="MIDAS",sequenceName="CATRNEGVIDASUBRAMO_ID_SEQ",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "NEGOCIOVIDA_ID")
	public NegocioVida getNegocioVida() {
		return this.negocioVida;
	}

	public void setNegocioVida(NegocioVida negocioVida) {
		this.negocioVida = negocioVida;
	}
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumns( { 
    @JoinColumn(name="ID_RAMO_CONTABLE", referencedColumnName="ID_RAMO_CONTABLE"), 
    @JoinColumn(name="ID_SUBR_CONTABLE", referencedColumnName="ID_SUBR_CONTABLE")})
	
	public SubRamoSeycos getSubRamo() {
		return this.subRamo;
	}

	public void setSubRamo(SubRamoSeycos subRamo) {
		this.subRamo = subRamo;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long  getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

}