package mx.com.afirme.midas2.domain.compensaciones;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CA_BANCA_SIN_MES",schema="MIDAS")
public class CaBancaSiniMes implements java.io.Serializable{

	
	 private Long id;
     private String ofsuc;
     private String nomOfsuc;
     private String nomGrupo;
     private String nomCanal;
     private Long codCanal;
     private String canalNvo;
     private String agte;
     private String nomAgte;
     private String nomEjec;
     private String codigoRamo;
     private String nomRamo;
     private String idCentroEmision;
     private String numPoliza;
     private String numRenovPoliza;
     private String mon;
     private String asegurado;
     private String fecEmi;
     private String vigDes;
     private String vigHas;
     private Double pmaEmi;
     private Double pmaPagada;
     private Double pmaDev;
     private Double reclama;
     private Double gastos;
     private Double salvRecu;
     private Double costoSin;
     private Double porSin;
     private String lineaNeg;
     private Double anio;
     private Double mes;
     private Long bancaPrimpagCpId;
     
     public CaBancaSiniMes(){

     }

     public CaBancaSiniMes(String ofsuc,String nomOfsuc,String nomGrupo,String nomCanal,Long codCanal,String canalNvo,
    	     String agte,String nomAgte,String nomEjec,String codigoRamo,String nomRamo,String idCentroEmision,String numPoliza,
    	     String numRenovPoliza,String mon,String asegurado,String fecEmi,String vigDes,String vigHas,Double pmaEmi,Double pmaPagada,
    	     Double pmaDev,Double reclama,Double gastos,Double salvRecu,Double costoSin,Double porSin,String lineaNeg,Double anio,
    	     Double mes,Long bancaPrimpagCpId){
        this.ofsuc = ofsuc;
        this.nomOfsuc= nomOfsuc;
        this.nomGrupo = nomGrupo;
        this.nomCanal = nomCanal;
        this.codCanal = codCanal;
        this.canalNvo=canalNvo;
        this.agte = agte;
        this.nomAgte = nomAgte;
        this.nomEjec= nomEjec;
        this.codigoRamo = codigoRamo;
        this.nomRamo = nomRamo;
        this.idCentroEmision = idCentroEmision;
        this.numPoliza = numPoliza;
        this.numRenovPoliza= numRenovPoliza;
        this.mon = mon;
        this.asegurado = asegurado;
        this.fecEmi = fecEmi;
        this.vigDes =vigDes;
        this.vigHas = vigHas;
        this.pmaEmi=pmaEmi;
        this.pmaPagada= pmaPagada;
        this.pmaDev= pmaDev;
        this.reclama=reclama;
        this.gastos= gastos;
        this.salvRecu = salvRecu;
        this.costoSin = costoSin;
        this.porSin = porSin;
        this.lineaNeg=lineaNeg;
        this.anio= anio;
        this.mes = mes;
        this.bancaPrimpagCpId=bancaPrimpagCpId;
     }


     @Id 
     @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CA_BANCA_SIN_MES_ID_SEQ")
 	@SequenceGenerator(name = "CA_BANCA_SIN_MES_ID_SEQ",  schema="MIDAS", sequenceName = "CA_BANCA_SIN_MES_ID_SEQ", allocationSize = 1)
     @Column(name="ID", unique=true)
     public Long getId() {
         return this.id;
     }
     
     public void setId(Long id) {
         this.id = id;
     }
     
     @Column(name="OFSUC")

     public String getOfsuc() {
         return this.ofsuc;
     }
     
     public void setOfsuc(String ofsuc) {
         this.ofsuc = ofsuc;
     }
     
     @Column(name="NOM_OFSUC")

     public String getNomOfsuc() {
         return this.nomOfsuc;
     }
     
     public void setNomOfsuc(String nomOfsuc) {
         this.nomOfsuc = nomOfsuc;
     }
     
     @Column(name="NOM_GRUPO")

     public String getNomGrupo() {
         return this.nomGrupo;
     }
     
     public void setNomGrupo(String nomGrupo) {
         this.nomGrupo = nomGrupo;
     }
     
     @Column(name="NOM_CANAL")

     public String getNomCanal() {
         return this.nomCanal;
     }
     
     public void setNomCanal(String nomCanal) {
         this.nomCanal = nomCanal;
     }
     
     @Column(name="COD_CANAL")

     public Long getCodCanal() {
         return this.codCanal;
     }
     
     public void setCodCanal(Long codCanal) {
         this.codCanal = codCanal;
     }
     
     @Column(name="CANAL_NVO")

     public String getCanalNvo() {
         return this.canalNvo;
     }
     
     public void setCanalNvo(String canalNvo) {
         this.canalNvo = canalNvo;
     }
     
     @Column(name="AGTE")

     public String getAgte() {
         return this.agte;
     }
     
     public void setAgte(String agte) {
         this.agte = agte;
     }
     
     @Column(name="NOM_AGTE")

     public String getNomAgte() {
         return this.nomAgte;
     }
     
     public void setNomAgte(String nomAgte) {
         this.nomAgte = nomAgte;
     }
     
     @Column(name="NOM_EJEC")

     public String getNomEjec() {
         return this.nomEjec;
     }
     
     public void setNomEjec(String nomEjec) {
         this.nomEjec = nomEjec;
     }
     
     @Column(name="CODIGORAMO")

     public String getCodigoRamo() {
		return codigoRamo;
	}

	public void setCodigoRamo(String codigoRamo) {
		this.codigoRamo = codigoRamo;
	}
     
     @Column(name="NOM_RAMO")

     public String getNomRamo() {
         return this.nomRamo;
     }
     
     public void setNomRamo(String nomRamo) {
         this.nomRamo = nomRamo;
     }
     
     @Column(name="ID_CENTRO_EMISION")

     public String getIdCentroEmision() {
         return this.idCentroEmision;
     }
     
     public void setIdCentroEmision(String idCentroEmision) {
         this.idCentroEmision = idCentroEmision;
     }
     
     @Column(name="NUM_POLIZA")

     public String getNumPoliza() {
         return this.numPoliza;
     }
     
     public void setNumPoliza(String numPoliza) {
         this.numPoliza = numPoliza;
     }
     
     @Column(name="NUM_RENOV_POLIZA")

     public String getNumRenovPoliza() {
         return this.numRenovPoliza;
     }
     
     public void setNumRenovPoliza(String numRenovPoliza) {
         this.numRenovPoliza = numRenovPoliza;
     }
     
     @Column(name="MON")

     public String getMon() {
         return this.mon;
     }
     
     public void setMon(String mon) {
         this.mon = mon;
     }
     
     @Column(name="ASEGURADO")

     public String getAsegurado() {
         return this.asegurado;
     }
     
     public void setAsegurado(String asegurado) {
         this.asegurado = asegurado;
     }
     
     @Column(name="FEC_EMI")
     public String getFecEmi() {
         return this.fecEmi;
     }
     
     public void setFecEmi(String fecEmi) {
         this.fecEmi = fecEmi;
     }
     
     @Column(name="VIG_DES")
     public String getVigDes() {
         return this.vigDes;
     }
     
     public void setVigDes(String vigDes) {
         this.vigDes = vigDes;
     }
     
     @Column(name="VIG_HAS")
     public String getVigHas() {
         return this.vigHas;
     }
     
     public void setVigHas(String vigHas) {
         this.vigHas = vigHas;
     }
     
     @Column(name="PMA_EMI")

     public Double getPmaEmi() {
         return this.pmaEmi;
     }
     
     public void setPmaEmi(Double pmaEmi) {
         this.pmaEmi = pmaEmi;
     }
     
     @Column(name="PMA_PAGADA")

     public Double getPmaPagada() {
         return this.pmaPagada;
     }
     
     public void setPmaPagada(Double pmaPagada) {
         this.pmaPagada = pmaPagada;
     }
     
     @Column(name="PMA_DEV")

     public Double getPmaDev() {
         return this.pmaDev;
     }
     
     public void setPmaDev(Double pmaDev) {
         this.pmaDev = pmaDev;
     }
     
     @Column(name="RECLAMA")

     public Double getReclama() {
         return this.reclama;
     }
     
     public void setReclama(Double reclama) {
         this.reclama = reclama;
     }
     
     @Column(name="GASTOS")

     public Double getGastos() {
         return this.gastos;
     }
     
     public void setGastos(Double gastos) {
         this.gastos = gastos;
     }
     
     @Column(name="SALV_RECU")

     public Double getSalvRecu() {
         return this.salvRecu;
     }
     
     public void setSalvRecu(Double salvRecu) {
         this.salvRecu = salvRecu;
     }
     
     @Column(name="COSTO_SIN")

     public Double getCostoSin() {
         return this.costoSin;
     }
     
     public void setCostoSin(Double costoSin) {
         this.costoSin = costoSin;
     }
     
     @Column(name="POR_SIN")

     public Double getPorSin() {
         return this.porSin;
     }
     
     public void setPorSin(Double porSin) {
         this.porSin = porSin;
     }
     
     @Column(name="LINEA_NEG")

     public String getLineaNeg() {
         return this.lineaNeg;
     }
     
     public void setLineaNeg(String lineaNeg) {
         this.lineaNeg = lineaNeg;
     }
     
     @Column(name="ANIO")

     public Double getAnio() {
         return this.anio;
     }
     
     public void setAnio(Double anio) {
         this.anio = anio;
     }
     
     @Column(name="MES")

     public Double getMes() {
         return this.mes;
     }
     
     public void setMes(Double mes) {
         this.mes = mes;
     }
     
     @Column(name="BANCA_PRIMPAG_CP_ID")

     public Long getBancaPrimpagCpId() {
         return this.bancaPrimpagCpId;
     }
     
     public void setBancaPrimpagCpId(Long bancaPrimpagCpId) {
         this.bancaPrimpagCpId = bancaPrimpagCpId;
     }

	

}
