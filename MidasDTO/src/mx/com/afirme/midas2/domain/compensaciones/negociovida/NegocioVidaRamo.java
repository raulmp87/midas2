package mx.com.afirme.midas2.domain.compensaciones.negociovida;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.emision.ppct.RamoSeycos;

/**
 * NegocioVidaRamo entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CA_TRNEGOCIOVIDA_RAMO", schema = "MIDAS")
public class NegocioVidaRamo implements java.io.Serializable,Entidad  {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 355710120277174426L;
	private Long id;
	private NegocioVida negocioVida;
	private RamoSeycos ramo;

	// Constructors

	/** default constructor */
	public NegocioVidaRamo() {
	}

	/** minimal constructor */
	public NegocioVidaRamo(Long id) {
		this.id = id;
	}

	/** full constructor */
	public NegocioVidaRamo(Long id, NegocioVida negocioVida, RamoSeycos ramo) {
		this.id = id;
		this.negocioVida = negocioVida;
		this.ramo = ramo;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CATRNEGVIDARAMO_ID_SEQ")
	@SequenceGenerator(name="CATRNEGVIDARAMO_ID_SEQ", schema="MIDAS",sequenceName="CATRNEGVIDARAMO_ID_SEQ",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "NEGOCIOVIDA_ID")
	public NegocioVida getNegocioVida() {
		return this.negocioVida;
	}

	public void setNegocioVida(NegocioVida negocioVida) {
		this.negocioVida = negocioVida;
	}
    
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "RAMO_ID")
	public RamoSeycos getRamo() {
		return this.ramo;
	}

	public void setRamo(RamoSeycos ramo) {
		this.ramo = ramo;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long  getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

}