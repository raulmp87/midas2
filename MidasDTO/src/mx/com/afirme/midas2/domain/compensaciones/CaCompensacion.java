package mx.com.afirme.midas2.domain.compensaciones;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;


@Entity
@Table(name="CA_COMPENSACION"
    ,schema="MIDAS"
)

public class CaCompensacion implements java.io.Serializable, Entidad {
	
	private static final long serialVersionUID = 1L;
     private Long id;
     private CaEstatus caEstatus;     
     private BigDecimal cotizacionId;
     private CaRamo caRamo;
     private BigDecimal polizaId;
     private String numeroPoliza;
     private Long negocioId;
     private String nombreNegocio;
     private Boolean contraprestacion;
     private Boolean retroactivo;
     private Boolean modificable;
     private Boolean excepcionDocumentos;
     private Boolean compensacionPorSubramos;
     private Date fechaVigorInicio;
     private Date fechaVigorFin;
     private Date fechaCreacion;
     private Date fechaModificacion;
     private String usuario;
     private Boolean borradoLogico;
     private Long negocioVidaId;
     private Long configuracionBancaId;
     private Date fechaModificacionVida;
     private Boolean aplicaComision;  
     private int idReciboModificacion;
     private BigDecimal topeMaximo;
     private String formaPagoBanca;

    public CaCompensacion() {
    }


    public CaCompensacion(Long id) {
        this.id = id;
    }


    public CaCompensacion(Long id, CaEstatus caEstatus, BigDecimal cotizacionId, CaRamo caRamo, BigDecimal polizaId, Long negocioId, Boolean contraprestacion, Boolean retroActivo, Boolean modificable, Boolean excepcionDocumentos, Boolean compensacionPorSubramos, Date fechaVigorInicio, Date fechaVigorFin, Date fechaCreacion, Date fechaModificacion, String usuario, Boolean borradoLogico) {
        this.id = id;
        this.caEstatus = caEstatus;
        this.cotizacionId = cotizacionId;
        this.caRamo = caRamo;
        this.polizaId = polizaId;
        this.negocioId = negocioId;
        this.contraprestacion = contraprestacion;        
        this.retroactivo = retroActivo;
        this.modificable = modificable;
        this.excepcionDocumentos = excepcionDocumentos;
        this.compensacionPorSubramos = compensacionPorSubramos;
        this.fechaVigorInicio = fechaVigorInicio;
        this.fechaVigorFin = fechaVigorFin;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.usuario = usuario;
        this.borradoLogico = borradoLogico;
    }

        
    @Id 
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CACOMPENSACION_ID_SEQ")
	@SequenceGenerator(name = "CACOMPENSACION_ID_SEQ",  schema="MIDAS", sequenceName = "CACOMPENSACION_ID_SEQ", allocationSize = 1)
    @Column(name="ID", unique=true, nullable=false, precision=10, scale=0)

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="ESTATUS_ID")

    public CaEstatus getCaEstatus() {
        return this.caEstatus;
    }
    
    public void setCaEstatus(CaEstatus caEstatus) {
        this.caEstatus = caEstatus;
    }
	
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="RAMO_ID")

    public CaRamo getCaRamo() {
        return this.caRamo;
    }
    
    public void setCaRamo(CaRamo caRamo) {
        this.caRamo = caRamo;
    }
    
    @Column(name="POLIZA_ID", precision=10, scale=0)

    public BigDecimal getPolizaId() {
        return this.polizaId;
    }
    
    public void setPolizaId(BigDecimal polizaId) {
        this.polizaId = polizaId;
    }
    
    @Transient
    public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	@Column(name="NEGOCIO_ID", precision=10, scale=0)

    public Long getNegocioId() {
        return this.negocioId;
    }
    
    public void setNegocioId(Long negocioId) {
        this.negocioId = negocioId;
    }
    
    @Transient
    public String getNombreNegocio() {
		return nombreNegocio;
	}

	public void setNombreNegocio(String nombreNegocio) {
		this.nombreNegocio = nombreNegocio;
	}

	@Column(name="CONTRAPRESTACION")

    public Boolean getContraprestacion() {
        return this.contraprestacion;
    }
    
    public void setContraprestacion(Boolean contraprestacion) {
        this.contraprestacion = contraprestacion;
    }    
        
    @Column(name="RETROACTIVO", precision=1, scale=0)

    public Boolean getRetroactivo() {
        return this.retroactivo;
    }
    
    public void setRetroactivo(Boolean retroactivo) {
        this.retroactivo = retroactivo;
    }
    
    @Column(name="MODIFICABLE")

    public Boolean getModificable() {
        return this.modificable;
    }
    
    public void setModificable(Boolean modificable) {
        this.modificable = modificable;
    }
    
    @Column(name="EXCEPCIONDOCUMENTOS")

    public Boolean getExcepcionDocumentos() {
        return this.excepcionDocumentos;
    }
    
    public void setExcepcionDocumentos(Boolean excepcionDocumentos) {
        this.excepcionDocumentos = excepcionDocumentos;
    }
    
    @Column(name="COMPENSACIONPORSUBRAMOS")

    public Boolean getCompensacionPorSubramos() {
        return this.compensacionPorSubramos;
    }
    
    public void setCompensacionPorSubramos(Boolean compensacionPorSubramos) {
        this.compensacionPorSubramos = compensacionPorSubramos;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAVIGORINICIO", length=7)

    public Date getFechaVigorInicio() {
        return this.fechaVigorInicio;
    }
    
    public void setFechaVigorInicio(Date fechaVigorInicio) {
        this.fechaVigorInicio = fechaVigorInicio;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAVIGORFIN", length=7)

    public Date getFechaVigorFin() {
        return this.fechaVigorFin;
    }
    
    public void setFechaVigorFin(Date fechaVigorFin) {
        this.fechaVigorFin = fechaVigorFin;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHACREACION", length=7)

    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }
    
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAMODIFICACION", length=7)

    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }
    
    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
    
    @Column(name="USUARIO")

    public String getUsuario() {
        return this.usuario;
    }
    
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    @Column(name="BORRADOLOGICO", precision=1, scale=0)

    public Boolean getBorradoLogico() {
        return this.borradoLogico;
    }
    
    public void setBorradoLogico(Boolean borradoLogico) {
        this.borradoLogico = borradoLogico;
    }

    @SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {		
		if (this.negocioId != null && this.negocioId > 0 ){
			return "COMPENSACION ADICIONAL DE AUTOS PARA EL NEGOCIO ID =" + this.negocioId;
		}else{
			return "SE DESCONOCE EL VALOR";
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.id;
	}

	
	@Column(name="COTIZACION_ID", precision=10, scale=0)
	
	public BigDecimal getCotizacionId() {
		return cotizacionId;
	}

	public void setCotizacionId(BigDecimal cotizacionId) {
		this.cotizacionId = cotizacionId;
	}

	@Column(name="NEGOCIOVIDA_ID", precision=10, scale=0)

	public Long getNegocioVidaId() {
		return negocioVidaId;
	}
	
	public void setNegocioVidaId(Long negocioVidaId) {
		this.negocioVidaId = negocioVidaId;
	}
	
	@Column(name="CONFIGURACION_BANCA_ID", precision=10, scale=0)
	public Long getConfiguracionBancaId() {
		return configuracionBancaId;
	}
	
	public void setConfiguracionBancaId(Long configuracionBancaId) {
		this.configuracionBancaId = configuracionBancaId;
	}

	@Override 
	public boolean equals(Object other) {
	    boolean result = false;
	    if (other instanceof CaCompensacion) {
	    	CaCompensacion that = (CaCompensacion) other;
	        result = this.getId() == that.getId();
	    }
	    return result;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHAMODIFICACIONVIDA", columnDefinition="DATE")
	public Date getFechaModificacionVida() {
		return fechaModificacionVida;
	}

	public void setFechaModificacionVida(Date fechaModificacionVida) {
		this.fechaModificacionVida = fechaModificacionVida;
	}

    @Column(name="APLICACOMISION", precision=1, scale=0)
	public Boolean getAplicaComision() {
		return aplicaComision;
	}

	public void setAplicaComision(Boolean aplicaComision) {
		this.aplicaComision = aplicaComision;
	}
	@Column(name="ID_RECIBO_MODIFICACION", precision=10, scale=0)
	public int getIdReciboModificacion() {
		return idReciboModificacion;
	}
	
	public void setIdReciboModificacion(int idReciboModificacion) {
		this.idReciboModificacion = idReciboModificacion;
	}

	@Column(name="TOPE_MAXIMO")
	public BigDecimal getTopeMaximo() {
		return topeMaximo;
	}

	public void setTopeMaximo(BigDecimal topeMaximo) {
		this.topeMaximo = topeMaximo;
	}

	@Column(name="FORMA_PAGO_BANCA")
	public String getFormaPagoBanca() {
		return formaPagoBanca;
	}

	public void setFormaPagoBanca(String formaPagoBanca) {
		this.formaPagoBanca = formaPagoBanca;
	}
}