package mx.com.afirme.midas2.domain.compensaciones;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="CA_BANCA_RAMO",schema="MIDAS")

public class CaBancaRamo {
	

	private Long id;
	private Long configuracionBancaId;
	private String ramoSeycosId;
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Id 
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CABANCARAMO_ID_SEQ")
	@SequenceGenerator(name = "CABANCARAMO_ID_SEQ",  schema="MIDAS", sequenceName = "CABANCARAMO_ID_SEQ", allocationSize = 1)
    @Column(name="ID", unique=true, nullable=false, precision=10, scale=0)
	public Long getId() {
		return id;
	}
	
	public void setConfiguracionBancaId(Long configuracionBancaId) {
		this.configuracionBancaId = configuracionBancaId;
	}

	@Column(name="CABANCACONFIGURACION_ID")	
	public Long getConfiguracionBancaId() {
		return configuracionBancaId;
	}

	public void setRamoSeycosId(String ramoSeycosId) {
		this.ramoSeycosId = ramoSeycosId;
	}
	
	@Column(name="RAMOSEYCOS_ID", nullable=false, length=2)
	public String getRamoSeycosId() {
		return ramoSeycosId;
	}
	
}
