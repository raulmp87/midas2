package mx.com.afirme.midas2.domain.compensaciones;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="CA_TIPOAUTORIZACION"
    ,schema="MIDAS"
)

public class CaTipoAutorizacion  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
     private String nombre;
     private Long valor;
     private Date fechaCreacion;
     private Date fechaModificacion;
     private String usuario;
     private Boolean borradoLogico;

    public CaTipoAutorizacion() {
    }

    public CaTipoAutorizacion(Long id) {
        this.id = id;
    }

    public CaTipoAutorizacion(Long id, String nombre, Long valor, Date fechacreacion, Date fechamodificacion, String usuario, Boolean borradologico) {
        this.id = id;
        this.nombre = nombre;
        this.valor = valor;
        this.fechaCreacion = fechacreacion;
        this.fechaModificacion = fechamodificacion;
        this.usuario = usuario;
        this.borradoLogico = borradologico;
    }

    @Id 
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CATIPOAUTORIZACION_ID_SEQ")
	@SequenceGenerator(name = "CATIPOAUTORIZACION_ID_SEQ",  schema="MIDAS", sequenceName = "CATIPOAUTORIZACION_ID_SEQ", allocationSize = 1)
    @Column(name="ID", unique=true, nullable=false, precision=10, scale=0)

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    @Column(name="NOMBRE")

    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    @Column(name="VALOR", precision=10, scale=0)

    public Long getValor() {
        return this.valor;
    }
    
    public void setValor(Long valor) {
        this.valor = valor;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHACREACION", length=7)

    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }
    
    public void setFechaCreacion(Date fechacreacion) {
        this.fechaCreacion = fechacreacion;
    }
    @Temporal(TemporalType.DATE)
    @Column(name="FECHAMODIFICACION", length=7)

    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }
    
    public void setFechaModificacion(Date fechamodificacion) {
        this.fechaModificacion = fechamodificacion;
    }
    
    @Column(name="USUARIO")

    public String getUsuario() {
        return this.usuario;
    }
    
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    @Column(name="BORRADOLOGICO", precision=1, scale=0)

    public Boolean getBorradoLogico() {
        return this.borradoLogico;
    }
    
    public void setBorradoLogico(Boolean borradologico) {
        this.borradoLogico = borradologico;
    }
}