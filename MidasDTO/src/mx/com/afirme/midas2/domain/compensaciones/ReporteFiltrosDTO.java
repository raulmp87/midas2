package mx.com.afirme.midas2.domain.compensaciones;

public class ReporteFiltrosDTO  implements  java.io.Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -6805384057559181999L;
	/**
	 * 
	 */
	private Long anioMes;
    private Long cveAgente;
    private Long cvePromotor;
    private Long cveProveedor;
	public Long getAnioMes() {
		return anioMes;
	}
	public void setAnioMes(Long anioMes) {
		this.anioMes = anioMes;
	}
	public Long getCveAgente() {
		return cveAgente;
	}
	public void setCveAgente(Long cveAgente) {
		this.cveAgente = cveAgente;
	}
	public Long getCvePromotor() {
		return cvePromotor;
	}
	public void setCvePromotor(Long cvePromotor) {
		this.cvePromotor = cvePromotor;
	}
	public Long getCveProveedor() {
		return cveProveedor;
	}
	public void setCveProveedor(Long cveProveedor) {
		this.cveProveedor = cveProveedor;
	}
     
}
