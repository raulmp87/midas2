package mx.com.afirme.midas2.domain.documentosGenericosFortimax;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

@Entity(name="DocumentoEntidadFortimax")
@Table(name="toDocumentoEntidadFortimax",schema="MIDAS")
@SqlResultSetMapping(name="documentoEntidadFortimaxView",entities={
	@EntityResult(entityClass=DocumentoEntidadFortimax.class,fields={
		@FieldResult(name="id",column="id"),
		@FieldResult(name="catalogoDocumentoFortimax.id",column="idDocumento"),
		@FieldResult(name="idRegistro",column="idRegistro"),
		@FieldResult(name="existeDocumento",column="existeDocumento")
	})
})
public class DocumentoEntidadFortimax implements Serializable, Entidad{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9136500043028670720L;
	private Long id;
	private CatalogoDocumentoFortimax catalogoDocumentoFortimax;
	private Long idRegistro;
	private Integer existeDocumento;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idToDocEntidadFortimax_seq")
	@SequenceGenerator(name="idToDocEntidadFortimax_seq", sequenceName="MIDAS.idToDocEntidadFortimax_seq",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="IDDOCUMENTO")
	public CatalogoDocumentoFortimax getCatalogoDocumentoFortimax() {
		return catalogoDocumentoFortimax;
	}

	public void setCatalogoDocumentoFortimax(CatalogoDocumentoFortimax catalogoDocumentoFortimax) {
		this.catalogoDocumentoFortimax = catalogoDocumentoFortimax;
	}

	@Column(name="IDREGISTRO",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getIdRegistro() {
		return idRegistro;
	}

	public void setIdRegistro(Long idRegistro) {
		this.idRegistro = idRegistro;
	}
	@Column(name="existeDocumento")
	public Integer getExisteDocumento() {
		return existeDocumento;
	}

	public void setExisteDocumento(Integer existeDocumento) {
		this.existeDocumento = existeDocumento;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

}
