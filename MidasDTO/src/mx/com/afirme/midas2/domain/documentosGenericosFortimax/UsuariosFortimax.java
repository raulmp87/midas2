package mx.com.afirme.midas2.domain.documentosGenericosFortimax;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name="UsuariosFortimax")
@Table(name="TCUSUARIOSFORTIMAX",schema="MIDAS")
public class UsuariosFortimax implements Serializable, Entidad{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4448520374947084898L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTCUSUARIOSFORTIMAX_GENERATOR")
	@SequenceGenerator(name="IDTCUSUARIOSFORTIMAX_GENERATOR", schema="MIDAS", sequenceName="TCUSUARIOSFORTIMAX_SEQ",allocationSize=1)
	@Column(name="ID")	
	private Long id;
	
	@Column(name="CODIGO_ASM",nullable=false,length=50)
	@NotNull
	@Size(min=1,max=50)
	private String codASM;
	
	
	@Column(name="IDAPLICACION",nullable=false)
	@NotNull
	private Long idAplicacion;
	

	@Column(name="DESCRIPCION",nullable=false,length=3000)
	@NotNull
	@Size(min=1,max=3000)
	private String descripcion;
	
	

	@Column(name="USUARIO",nullable=false,length=50)
	@NotNull
	@Size(min=1,max=50)
	private String user;
		
	@Column(name="PASSWORD",nullable=false,length=50)
	@NotNull
	@Size(min=1,max=50)
	private String pass;
	
	
	@Column(name="NIVEL",nullable=false)
	@NotNull
	private Long nivel;
	
	

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}


	@Override
	public String getValue() {
		return user;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getCodASM() {
		return codASM;
	}


	public void setCodASM(String codASM) {
		this.codASM = codASM;
	}


	public Long getIdAplicacion() {
		return idAplicacion;
	}


	public void setIdAplicacion(Long idAplicacion) {
		this.idAplicacion = idAplicacion;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public String getUser() {
		return user;
	}


	public void setUser(String user) {
		this.user = user;
	}


	public String getPass() {
		return pass;
	}


	public void setPass(String pass) {
		this.pass = pass;
	}


	public Long getNivel() {
		return nivel;
	}


	public void setNivel(Long nivel) {
		this.nivel = nivel;
	}
	

}
