package mx.com.afirme.midas2.domain.documentosGenericosFortimax;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

@Entity(name="CarpetaAplicacionFortimax")
@Table(name="tcCarpetaAplicacionFortimax",schema="MIDAS")
public class CarpetaAplicacionFortimax implements Serializable, Entidad{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2368519675839441105L;
	private Long id;
	private CatalogoAplicacionFortimax aplicacion;
	private String nombreCarpeta;
	private String nombreCarpetaFortimax;
	private String descripcion;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idTcCarpApFortimax_seq")
	@SequenceGenerator(name="idTcCarpApFortimax_seq", sequenceName="MIDAS.idTcCarpApFortimax_seq",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="IDAPLICACION") 
	public CatalogoAplicacionFortimax getAplicacion() {
		return aplicacion;
	}

	public void setAplicacion(CatalogoAplicacionFortimax aplicacion) {
		this.aplicacion = aplicacion;
	}

	@Column(name="NOMBRECARPETA",nullable=false,length=100)
	@Size(min=1,max=100)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public String getNombreCarpeta() {
		return nombreCarpeta;
	}

	public void setNombreCarpeta(String nombreCarpeta) {
		this.nombreCarpeta = nombreCarpeta;
	}

	@Column(name="NOMBRECARPETAFORTIMAX",nullable=false,length=100)
	@Size(min=1,max=100)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public String getNombreCarpetaFortimax() {
		return nombreCarpetaFortimax;
	}

	public void setNombreCarpetaFortimax(String nombreCarpetaFortimax) {
		this.nombreCarpetaFortimax = nombreCarpetaFortimax;
	}

	@Column(name="DESCRIPCION",nullable=false,length=100)
	@Size(min=1,max=100)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return nombreCarpeta;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
}
