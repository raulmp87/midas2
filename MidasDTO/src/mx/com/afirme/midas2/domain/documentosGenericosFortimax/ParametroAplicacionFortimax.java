package mx.com.afirme.midas2.domain.documentosGenericosFortimax;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

@Entity(name="ParametroAplicacionFortimax")
@Table(name="tcParametroAplicacionFortimax",schema="MIDAS")
public class ParametroAplicacionFortimax implements Serializable, Entidad{

	private static final long serialVersionUID = 7179855227263624521L;
	private Long id;
	private CatalogoAplicacionFortimax aplicacion;
	private String nombreParametro;	
	private String descripcion;
	private Integer esLlave;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idTcParamApFortimax_seq")
	@SequenceGenerator(name="idTcParamApFortimax_seq", sequenceName="MIDAS.idTcParamApFortimax_seq",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="IDAPLICACION") 
	public CatalogoAplicacionFortimax getAplicacion() {
		return aplicacion;
	}

	public void setAplicacion(CatalogoAplicacionFortimax aplicacion) {
		this.aplicacion = aplicacion;
	}

	@Column(name="NOMBREPARAMETRO",nullable=false,length=100)
	@Size(min=1,max=100)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public String getNombreParametro() {
		return nombreParametro;
	}

	public void setNombreParametro(String nombreParametro) {
		this.nombreParametro = nombreParametro;
	}
	
	@Column(name="DESCRIPCION",nullable=false,length=100)
	@Size(min=1,max=100)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name="ESLLAVE",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Integer getEsLlave() {
		return esLlave;
	}

	public void setEsLlave(Integer esLlave) {
		this.esLlave = esLlave;
	}	

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return nombreParametro;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

}
