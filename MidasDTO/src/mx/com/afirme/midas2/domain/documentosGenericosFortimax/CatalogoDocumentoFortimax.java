package mx.com.afirme.midas2.domain.documentosGenericosFortimax;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

@Entity(name="CatalogoDocumentoFortimax")
@Table(name="tcCatalogoDocumentoFortimax",schema="MIDAS")
@SqlResultSetMapping(name="catalogoDocumentoFortimaxView",entities={
	@EntityResult(entityClass=CatalogoDocumentoFortimax.class,fields={
		@FieldResult(name="id",column="id"),
		@FieldResult(name="nombreDocumento",column="nombreDocumento"),
		@FieldResult(name="nombreDocumentoFortimax",column="nombreDocumentoFortimax"),
		@FieldResult(name="carpeta.id",column="idCarpeta"),
		@FieldResult(name="clavePF",column="clavePF"),
		@FieldResult(name="clavePM",column="clavePM"),
		@FieldResult(name="requerido",column="requerido")
	})
})
public class CatalogoDocumentoFortimax implements Serializable, Entidad{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5375671621873864103L;
	private Long id;
	private String nombreDocumento;
	private String nombreDocumentoFortimax;
	private CarpetaAplicacionFortimax carpeta;
	private Integer clavePF;
	private Integer clavePM;
	private Integer requerido;
	private String descripcion;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idTcCatDocCarpetaFortimax_seq")
	@SequenceGenerator(name="idTcCatDocCarpetaFortimax_seq", sequenceName="MIDAS.idTcCatDocCarpetaFortimax_seq",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="NOMBREDOCUMENTO",nullable=false,length=100)
	@Size(min=1,max=100)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public String getNombreDocumento() {
		return nombreDocumento;
	}

	public void setNombreDocumento(String nombreDocumento) {
		this.nombreDocumento = nombreDocumento;
	}

	@Column(name="NOMBREDOCUMENTOFORTIMAX",nullable=false,length=100)
	@Size(min=1,max=100)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public String getNombreDocumentoFortimax() {
		return nombreDocumentoFortimax;
	}

	public void setNombreDocumentoFortimax(String nombreDocumentoFortimax) {
		this.nombreDocumentoFortimax = nombreDocumentoFortimax;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="IDCARPETA")
	public CarpetaAplicacionFortimax getCarpeta() {
		return carpeta;
	}

	public void setCarpeta(CarpetaAplicacionFortimax carpeta) {
		this.carpeta = carpeta;
	}

	@Column(name="CLAVEPF",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Integer getClavePF() {
		return clavePF;
	}

	public void setClavePF(Integer clavePF) {
		this.clavePF = clavePF;
	}

	@Column(name="CLAVEPM",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Integer getClavePM() {
		return clavePM;
	}

	public void setClavePM(Integer clavePM) {
		this.clavePM = clavePM;
	}

	@Column(name="REQUERIDO",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Integer getRequerido() {
		return requerido;
	}

	public void setRequerido(Integer requerido) {
		this.requerido = requerido;
	}

	@Column(name="DESCRIPCION")
	@Size(min=1,max=100)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return nombreDocumento;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

}
