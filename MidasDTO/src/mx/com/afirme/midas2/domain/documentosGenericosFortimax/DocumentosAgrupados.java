package mx.com.afirme.midas2.domain.documentosGenericosFortimax;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name="DocumentosAgrupados")
@Table(name="trDocumentosAgrupados",schema="MIDAS")
public class DocumentosAgrupados implements Entidad, Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 846422293742060479L;
	private Long id;
	private Long idDocumento;
	private Long idEntidad;
	private Long idAgrupador;
	private String nombre;
	private Integer existeDocumento;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idTrDocumentosAgrupados_seq")
	@SequenceGenerator(name="idTrDocumentosAgrupados_seq", sequenceName="MIDAS.idTrDocumentosAgrupados_seq",allocationSize=1)
	@Column(name="id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="IDDOCUMENTO")
	public Long getIdDocumento() {
		return idDocumento;
	}

	public void setIdDocumento(Long idDocumento) {
		this.idDocumento = idDocumento;
	}

	@Column(name="IDENTIDAD")
	public Long getIdEntidad() {
		return idEntidad;
	}

	public void setIdEntidad(Long idEntidad) {
		this.idEntidad = idEntidad;
	}

	@Column(name="IDAGRUPADOR")
	public Long getIdAgrupador() {
		return idAgrupador;
	}

	public void setIdAgrupador(Long idAgrupador) {
		this.idAgrupador = idAgrupador;
	}

	@Column(name="NOMBRE")
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name="EXISTEDOCUMENTO")
	public Integer getExisteDocumento() {
		return existeDocumento;
	}

	public void setExisteDocumento(Integer existeDocumento) {
		this.existeDocumento = existeDocumento;
	}

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}
