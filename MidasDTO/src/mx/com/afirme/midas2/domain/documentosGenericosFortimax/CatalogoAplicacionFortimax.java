package mx.com.afirme.midas2.domain.documentosGenericosFortimax;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name="CatalogoAplicacionFortimax")
@Table(name="tcCatalogoAplicacionFortimax",schema="MIDAS")
public class CatalogoAplicacionFortimax implements Serializable,Entidad{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8769055740865349140L;
	private Long id;
	private String nombreAplicacion;
	private String nombreFortimax;
	private String descripcion;
	private String clase;
	private String nombreCarpetaRaiz;
	
	public static final String APLICACION_AGENTES_FORTIMAX = "AGENTES";
	public static final String APLICACION_CLIENTES_FORTIMAX = "CLIENTES";
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idTcCatalogoApFortimax_seq")
	@SequenceGenerator(name="idTcCatalogoApFortimax_seq", sequenceName="MIDAS.idTcCatalogoApFortimax_seq",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="NOMBREAPLICACION",nullable=false,length=100)
	@NotNull
	@Size(min=1,max=100)
	public String getNombreAplicacion() {
		return nombreAplicacion;
	}

	public void setNombreAplicacion(String nombreAplicacion) {
		this.nombreAplicacion = nombreAplicacion;
	}

	@Column(name="NOMBREAPLICACIONFORTIMAX",nullable=false,length=100)
	@NotNull
	@Size(min=1,max=100)
	public String getNombreFortimax() {
		return nombreFortimax;
	}	

	public void setNombreFortimax(String nombreFortimax) {
		this.nombreFortimax = nombreFortimax;
	}

	@Column(name="DESCRIPCION",nullable=false,length=100)
	@Size(min=1,max=100)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name="CLASE",nullable=false,length=100)
	@NotNull
	@Size(min=1,max=100)
	public String getClase() {
		return clase;
	}

	public void setClase(String clase) {
		this.clase = clase;
	}
	
	@Column(name="nombreCarpetaRaiz")
	@NotNull
	@Size(min=1,max=200)
	public String getNombreCarpetaRaiz() {
		return nombreCarpetaRaiz;
	}

	public void setNombreCarpetaRaiz(String nombreCarpetaRaiz) {
		this.nombreCarpetaRaiz = nombreCarpetaRaiz;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return nombreAplicacion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

}
