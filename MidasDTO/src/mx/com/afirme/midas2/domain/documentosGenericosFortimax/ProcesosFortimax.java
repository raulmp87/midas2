package mx.com.afirme.midas2.domain.documentosGenericosFortimax;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

@Entity(name="ProcesosFortimax")
@Table(name="TCPROCESOSFORTIMAX",schema="MIDAS")
public class ProcesosFortimax implements Serializable, Entidad{

	private static final long serialVersionUID = -8946607988391164997L;



	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTCPROCESOFORTIMAX_GENERATOR")
	@SequenceGenerator(name="IDTCPROCESOFORTIMAX_GENERATOR", schema="MIDAS", sequenceName="IDTCPROCESOFORTIMAX_SEQ",allocationSize=1)
	@Column(name="ID")	
	private Long id;
	
	@Column(name="PROCESO",nullable=false,length=300)
	@NotNull
	@Size(min=1,max=300)
	private String proceso;
	

	@Column(name="DESCRIPCION",nullable=false,length=3000)
	@NotNull
	@Size(min=1,max=3000)
	private String descripcion;
	
	@Column(name="RUTA",nullable=true,length=300)
	@NotNull
	@Size(min=1,max=300)
	private String ruta;
	@Column(name="NOMBREARCHIVO",nullable=true,length=300)
	@NotNull
	@Size(min=1,max=300)
	private String nombreArchivo;	
	
	@Column(name="ACTIVO")
    private Boolean activo = Boolean.TRUE;	
	
	@Column(name="TIPO",nullable=false,length=1)
	@NotNull
	@Size(min=1,max=3000)
	private String tipo;
	
	@Column(name="CODIGOGPORFIJO",nullable=false,length=80)
	@NotNull
	@Size(min=1,max=3000)
	private String codigoGpoFijo;
	
	@Column(name="IDAPLICACION",nullable=false)
	@NotNull
	private Long idAplicacion;
	
	@Column(name="REQUIEREFOLIO")
	private Short requiereFolio;
	
	
	
	
	
	public Short getRequiereFolio() {
		return requiereFolio;
	}


	public void setRequiereFolio(Short requiereFolio) {
		this.requiereFolio = requiereFolio;
	}


	public Long getIdAplicacion() {
		return idAplicacion;
	}


	public void setIdAplicacion(Long idAplicacion) {
		this.idAplicacion = idAplicacion;
	}


	public String getTipo() {
		return tipo;
	}


	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	public String getCodigoGpoFijo() {
		return codigoGpoFijo;
	}


	public void setCodigoGpoFijo(String codigoGpoFijo) {
		this.codigoGpoFijo = codigoGpoFijo;
	}


	public void setProceso(String proceso) {
		this.proceso = proceso;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getProceso() {
		return proceso;
	}


	public void setProceso( PROCESO_FORTIMAX proceso ) {
		this.proceso = proceso.toString();
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public String getRuta() {
		return ruta;
	}


	public void setRuta(String ruta) {
		this.ruta = ruta;
	}


	public String getNombreArchivo() {
		return nombreArchivo;
	}


	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}


	public Boolean getActivo() {
		return activo;
	}


	public void setActivo(Boolean activo) {
		this.activo = activo;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}


	@Override
	public String getValue() {
		return proceso;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	

	public enum PROCESO_FORTIMAX {
		/**
		 * Imágenes Cabina
		 */
		CRUC	,
		/**
		 * Imágenes Daños Materiales
		 */
		PDM	, //Imágenes Daños Materiales
		
		/**
		 * Imágenes Responsabilidad Civil Bien
		 */
		PRCB	,
		/**
		 *Imágenes Responsabilidad Civil Vehículo
		 */		
		PRCV	,
		/**
		 *Imágenes Robos
		 */
		ROBO		,
		/**
		 *Documentos de Ajuste  
		 */
		DOCS_AJUSTE	,
		/**
		 *Prestador de Servicio  
		 */
		PREST_SERVICIO ,
		/**
		 *Anexo recuperacion compañia  
		 */
		RECUP_CIA,
		COMPENSACION_AGENTE,
		COMPENSACION_PROMOTOR,
		COMPENSACION_PROVEEDOR
	};
}
