package mx.com.afirme.midas2.domain.documentosFortimax;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name="DocumentoEntity")
@Table(name="toDocumentoEntity",schema="MIDAS")
public class DocumentoEntity implements Serializable,Entidad{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String idString;
	private	Long entityId;
	private	String comentarios;
	private	Long idArchivo;
	private String idArchivoString;
	private TipoDocumentoEntity tipoDocumentoEntity;
	public DocumentoEntity(){}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idToDocumentoEntity_seq")
	@SequenceGenerator(name="idToDocumentoEntity_seq", sequenceName="MIDAS.idToDocumentoEntity_seq",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}	
	
	@Column(name="ENTITY_ID",nullable=false)
	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	@Column(name="COMENTARIOS",nullable=true,length=200)
	@Size(min=0,max=200)
	public String getComentarios() {
		return comentarios;
	}
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}
	
	@Column(name="IDARCHIVO",nullable=true)
	public Long getIdArchivo() {
		return idArchivo;
	}
	public void setIdArchivo(Long idArchivo) {
		this.idArchivo = idArchivo;
	}
	
	@ManyToOne
	@JoinColumn(name="TIPODOCUMENTOENTITY_ID",nullable=false)
	public TipoDocumentoEntity getTipoDocumentoEntity() {
		return tipoDocumentoEntity;
	}
	public void setTipoDocumentoEntity(TipoDocumentoEntity tipoDocumentoEntity) {
		this.tipoDocumentoEntity = tipoDocumentoEntity;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return (tipoDocumentoEntity!=null)?tipoDocumentoEntity.getDescripcion():null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	@Transient
	public String getIdString() {
		return idString;
	}

	public void setIdString(String idString) {
		this.idString = idString;
		this.id = Long.valueOf(idString);
	}

	@Transient
	public String getIdArchivoString() {
		return idArchivoString;
	}

	public void setIdArchivoString(String idArchivoString) {
		this.idArchivoString=idArchivoString;
		this.idArchivo = Long.valueOf(idArchivoString);
	}
	
}
