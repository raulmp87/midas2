package mx.com.afirme.midas2.domain.documentosFortimax;

import java.util.Map;

import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;


public class FortimaxDocument {
	
	/**
	 * Identificador Expediente
	 * */	
	private String folio;	
	private Long id;
	
	/**
	 * Datos Aplicacion 
	 * */		
	private String aplicacion;		
	private String 	[]	expediente=new String[1];
	private	String	[] 	fieldValuesGenerico =new String[2];
	private	String	[] 	fieldValues;
	/**
	 * Manejo de documentos
	 * */			
	private String ruta;
	private String subcarpeta;	
		/**
		 *Tipo Documento C: Carpeta , D:Documento
		 * */
	private String tipoDoc; 
	private String prefijo;	
	private String nombreArchivo;
	private Map <String, String> documentosGaveta;

	/**
	 *Parametros salids WS
	 * */	
	private String resultado = null;	
	private String codError = null;
	private String descError = null;
	
	/**
	 *Datos Consulta
	 * */	
	private String token = null;	
	private String ligaIfimax = null;	
	private String nodo = null;	
	
	
	
	
	/**
	 * Manejo de Archivos   
	 * */
	private TransporteImpresionDTO archivoAlta;
	private TransporteImpresionDTO archivoDescarga;
	
	
	public void inicializar (){
		resultado = null;	
		codError = null;
		descError = null;		
	}
	
	
		
	public String[] getFieldValues() {
		return fieldValues;
	}
	public void setFieldValues(String[] fieldValues) {
		this.fieldValues = fieldValues;
	}
	public void setAplicacion(String aplicacion) {
		this.aplicacion = aplicacion;
	}
	public String[] getExpediente() {
		return expediente;
	}
	public void setExpediente(String[] expediente) {
		this.expediente = expediente;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		expediente[0]=id.toString();
		fieldValuesGenerico[0]=id.toString();
		this.id = id;
	}
	public String getAplicacion() {
		return aplicacion;
	}
	public void setAplicacion(FORTIMAX_APLICACION aplicacion) {
		this.aplicacion = aplicacion.toString();
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getSubcarpeta() {
		return subcarpeta;
	}
	public void setSubcarpeta(String subcarpeta) {
		this.subcarpeta = subcarpeta;
	}
	public String getTipoDoc() {
		return tipoDoc;
	}
	public void setTipoDoc(FORTIMAX_TIPO_DOC tipoDoc) {
		this.tipoDoc = tipoDoc.toString();
	}
	public String getPrefijo() {
		return prefijo;
	}
	public void setPrefijo(String prefijo) {
		this.prefijo = prefijo;
	}
	
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		fieldValuesGenerico[1]=folio;
		this.folio = folio;
	}

	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}
	

	public String getResultado() {
		return resultado;
	}
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
	public String getCodError() {
		return codError;
	}
	public void setCodError(String codError) {
		this.codError = codError;
	}
	public String getDescError() {
		return descError;
	}
	public void setDescError(String descError) {
		this.descError = descError;
	}


	public String getToken() {
		return token;
	}



	public void setToken(String token) {
		this.token = token;
	}



	public String getLigaIfimax() {
		return ligaIfimax;
	}



	public void setLigaIfimax(String ligaIfimax) {
		this.ligaIfimax = ligaIfimax;
	}



	public String getNodo() {
		return nodo;
	}



	public void setNodo(String nodo) {
		this.nodo = nodo;
	}



	public TransporteImpresionDTO getArchivoAlta() {
		return archivoAlta;
	}



	public void setArchivoAlta(TransporteImpresionDTO archivoAlta) {
		this.archivoAlta = archivoAlta;
	}



	public TransporteImpresionDTO getArchivoDescarga() {
		return archivoDescarga;
	}



	public void setArchivoDescarga(TransporteImpresionDTO archivoDescarga) {
		this.archivoDescarga = archivoDescarga;
	}


	public enum FORTIMAX_TIPO_DOC {
		C,D
	};

	public enum FORTIMAX_APLICACION {
		SINIESTROS_AUTOS_REPORTE,SINIESTROS_AUTOS_PRESTADOR
	}
	
	public String[] getFieldValuesGenerico() {
		return this.fieldValuesGenerico ;
	}



	public Map<String, String> getDocumentosGaveta() {
		return documentosGaveta;
	}



	public void setDocumentosGaveta(Map<String, String> documentosGaveta) {
		this.documentosGaveta = documentosGaveta;
	}


	
	
	
}
