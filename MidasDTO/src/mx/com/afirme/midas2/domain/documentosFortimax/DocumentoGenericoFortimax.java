package mx.com.afirme.midas2.domain.documentosFortimax;

import java.io.Serializable;

public class DocumentoGenericoFortimax implements Serializable{
	private static final long serialVersionUID = -5820970072590624739L;
	
	private String descripcion;
	private String idDocumentoAgenteString;
	private String idTipoDocumentoString;
	private String idArchivoString;
	private String comentarios;
	private String claveAplicaPersonaFisicaString;
	private String claveAplicaPersonaMoralString;
	private String existe;
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getIdArchivoString() {
		return idArchivoString;
	}
	
	public void setIdArchivoString(String idArchivoString) {
		this.idArchivoString = idArchivoString;
	}
	
	public String getComentarios() {
		return comentarios;
	}
	
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}
	
	public String getClaveAplicaPersonaFisicaString() {
		return claveAplicaPersonaFisicaString;
	}
	
	public void setClaveAplicaPersonaFisicaString(
			String claveAplicaPersonaFisicaString) {
		this.claveAplicaPersonaFisicaString = claveAplicaPersonaFisicaString;
	}
	
	public String getClaveAplicaPersonaMoralString() {
		return claveAplicaPersonaMoralString;
	}
	
	public void setClaveAplicaPersonaMoralString(
			String claveAplicaPersonaMoralString) {
		this.claveAplicaPersonaMoralString = claveAplicaPersonaMoralString;
	}

	public String getIdDocumentoAgenteString() {
		return idDocumentoAgenteString;
	}

	public void setIdDocumentoAgenteString(String idDocumentoAgenteString) {
		this.idDocumentoAgenteString = idDocumentoAgenteString;
	}

	public String getIdTipoDocumentoString() {
		return idTipoDocumentoString;
	}

	public void setIdTipoDocumentoString(String idTipoDocumentoString) {
		this.idTipoDocumentoString = idTipoDocumentoString;
	}

	public String getExiste() {
		return existe;
	}

	public void setExiste(String existe) {
		this.existe = existe;
	}	
	
}
