package mx.com.afirme.midas2.domain.persona;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

import org.directwebremoting.annotations.DataTransferObject;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.validator.group.NewItemChecks;

@Entity(name="Persona")
@Table(name="VW_PERSONA",schema="MIDAS")
@DataTransferObject
public class Persona implements Serializable,Entidad{
	private static final long serialVersionUID = 1L;
	private String apellidoMaterno;
	private String apellidoPaterno;
	private String bcapturaAseg;
	private String claveEstadoCivil;
	private String claveEstadoNacimiento;
	private String claveMunicipioNacimiento;
	private String claveNacionalidad;
	private String claveRamaActiv;
	private String claveRamaPersonaFisica;
	private String claveResidencia;
	private String claveSexo;
	private String claveSituacion;
	private String claveSubramaActiv;
	private Long claveTipoPersona;
	private String claveTipoSector;
	private String claveTipoSectorMoral;
	private String claveTipoSituacion;
	private String claveUsuarioModificacion;
	private String correosAdicionales;
	private String curp;
	private String email;
	private String facebook;
	private Date fechaAlta;
	private Date fechaBaja;
	private Date fechaConstitucion;
	private Date fechaModificacion;
	private Date fechaNacimiento;
	private Long idPersona;
	private Long idRepresentante;
	private String lugarNacimiento;
	private String nombre;
	private String nombreCompleto;
	private String nombreContacto;
	private String paginaWeb;
	private String puestoContacto;
	private String razonSocial;
	private String telefonoCasa;
	private String telefonoCelular;
	private String telefonoFax;
	private String telefonoOficina;
	private String telefonosAdicionales;
	private String twitter;
	private String rfc;
	private List<Domicilio> domicilios;
	private String claveSectorFinanciero;
	
    public Persona() {
    }

    @Id
    @Column(name = "IDPERSONA", unique = true, nullable = false, precision = 8, scale = 0)
    @Digits(integer = 8, fraction= 0,groups=NewItemChecks.class,message="{javax.validation.constraints.Digits.message}")
    public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}
    
	@Column(name="NOMBRE")    
    @Size(min=0, max=20, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
    public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
    
    @Column(name="APELLIDOMATERNO")    
    @Size(min=0, max=20, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getApellidoMaterno() {
		return this.apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	@Column(name="APELLIDOPATERNO")
    @Size(min=0, max=20, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getApellidoPaterno() {
		return this.apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	@Column(name="BCAPTURAASEG", nullable = false)
	@Size(min=0, max=1, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getBcapturaAseg() {
		return this.bcapturaAseg;
	}

	public void setBcapturaAseg(String bcapturaAseg) {
		this.bcapturaAseg = bcapturaAseg;
	}

	@Column(name="CLAVEESTADOCIVIL")
	@Size(min=0, max=1, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getClaveEstadoCivil() {
		return this.claveEstadoCivil;
	}

	public void setClaveEstadoCivil(String claveEstadoCivil) {
		this.claveEstadoCivil = claveEstadoCivil;
	}

	@Column(name="CLAVEESTADONACIMIENTO")
	@Size(min=0, max=5, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getClaveEstadoNacimiento() {
		return this.claveEstadoNacimiento;
	}

	public void setClaveEstadoNacimiento(String claveEstadoNacimiento) {
		this.claveEstadoNacimiento = claveEstadoNacimiento;
	}

	@Column(name="CLAVEMUNICIPIONACIMIENTO")
    @Size(min=0, max=20, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getClaveMunicipioNacimiento() {
		return this.claveMunicipioNacimiento;
	}

	public void setClaveMunicipioNacimiento(String claveMunicipioNacimiento) {
		this.claveMunicipioNacimiento = claveMunicipioNacimiento;
	}

	@Column(name="CLAVENACIONALIDAD", nullable = false)
	@Size(min=0, max=6, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getClaveNacionalidad() {
		return this.claveNacionalidad;
	}

	public void setClaveNacionalidad(String claveNacionalidad) {
		this.claveNacionalidad = claveNacionalidad;
	}

	@Column(name="CURP")
    @Size(min=0, max=18, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getCurp() {
		return this.curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	@Column(name="EMAIL")
    @Size(min=0, max=16, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name="FACEBOOK")
    @Size(min=0, max=50, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getFacebook() {
		return this.facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	@Column(name="TWITTER")
    @Size(min=0, max=50, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getTwitter() {
		return this.twitter;
	}

	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	@Column(name="CLAVERAMAACTIV")
    @Size(min=0, max=2, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getClaveRamaActiv() {
		return claveRamaActiv;
	}

	public void setClaveRamaActiv(String claveRamaActiv) {
		this.claveRamaActiv = claveRamaActiv;
	}

	@Column(name="CLAVERAMAPERSONAFISICA")
    @Size(min=0, max=2, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getClaveRamaPersonaFisica() {
		return claveRamaPersonaFisica;
	}

	public void setClaveRamaPersonaFisica(String claveRamaPersonaFisica) {
		this.claveRamaPersonaFisica = claveRamaPersonaFisica;
	}

	@Column(name="CLAVERESIDENCIA",nullable = false)
    @Size(min=1, max=6, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getClaveResidencia() {
		return claveResidencia;
	}

	public void setClaveResidencia(String claveResidencia) {
		this.claveResidencia = claveResidencia;
	}

	@Column(name="CLAVESEXO")
    @Size(min=0, max=1, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getClaveSexo() {
		return claveSexo;
	}

	public void setClaveSexo(String claveSexo) {
		this.claveSexo = claveSexo;
	}

	@Column(name="CLAVESITUACION")
    @Size(min=0, max=2, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getClaveSituacion() {
		return claveSituacion;
	}

	public void setClaveSituacion(String claveSituacion) {
		this.claveSituacion = claveSituacion;
	}

	@Column(name="CLAVESUBRAMAACTIV")
    @Size(min=0, max=2, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getClaveSubramaActiv() {
		return claveSubramaActiv;
	}

	public void setClaveSubramaActiv(String claveSubramaActiv) {
		this.claveSubramaActiv = claveSubramaActiv;
	}

	@Column(name = "CLAVETIPOPERSONA", precision = 8, scale = 0)
    @Digits(integer = 8, fraction= 0,groups=NewItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public Long getClaveTipoPersona() {
		return claveTipoPersona;
	}

	public void setClaveTipoPersona(Long claveTipoPersona) {
		this.claveTipoPersona = claveTipoPersona;
	}

	@Column(name="CLAVETIPOSECTOR")
    @Size(min=0, max=2, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getClaveTipoSector() {
		return claveTipoSector;
	}

	public void setClaveTipoSector(String claveTipoSector) {
		this.claveTipoSector = claveTipoSector;
	}

	@Column(name="CLAVETIPOSECTORMORAL")
    @Size(min=0, max=2, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getClaveTipoSectorMoral() {
		return claveTipoSectorMoral;
	}

	public void setClaveTipoSectorMoral(String claveTipoSectorMoral) {
		this.claveTipoSectorMoral = claveTipoSectorMoral;
	}

	@Column(name="CLAVETIPOSITUACION", nullable = false)
    @Size(min=0, max=2, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getClaveTipoSituacion() {
		return claveTipoSituacion;
	}

	public void setClaveTipoSituacion(String claveTipoSituacion) {
		this.claveTipoSituacion = claveTipoSituacion;
	}

	@Column(name="CLAVEUSUARIOMODIFICACION")
    @Size(min=0, max=8, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getClaveUsuarioModificacion() {
		return claveUsuarioModificacion;
	}

	public void setClaveUsuarioModificacion(String claveUsuarioModificacion) {
		this.claveUsuarioModificacion = claveUsuarioModificacion;
	}

	@Column(name="CORREOSADICIONALES")
    @Size(min=0, max=300, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getCorreosAdicionales() {
		return correosAdicionales;
	}

	public void setCorreosAdicionales(String correosAdicionales) {
		this.correosAdicionales = correosAdicionales;
	}	

	@Column(name = "IDREPRESENTANTE", precision = 8, scale = 0)
    @Digits(integer = 8, fraction= 0,groups=NewItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public Long getIdRepresentante() {
		return idRepresentante;
	}

	public void setIdRepresentante(Long idRepresentante) {
		this.idRepresentante = idRepresentante;
	}

	@Column(name="LUGARNACIMIENTO")
    @Size(min=0, max=60, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getLugarNacimiento() {
		return lugarNacimiento;
	}

	public void setLugarNacimiento(String lugarNacimiento) {
		this.lugarNacimiento = lugarNacimiento;
	}

	@Column(name="NOMBRECOMPLETO")
    @Size(min=0, max=200, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	@Column(name="NOMBRECONTACTO")
    @Size(min=0, max=50, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getNombreContacto() {
		return nombreContacto;
	}

	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}

	@Column(name="PAGINAWEB")
    @Size(min=0, max=50, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getPaginaWeb() {
		return paginaWeb;
	}

	public void setPaginaWeb(String paginaWeb) {
		this.paginaWeb = paginaWeb;
	}

	@Column(name="PUESTOCONTACTO")
    @Size(min=0, max=50, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getPuestoContacto() {
		return puestoContacto;
	}

	public void setPuestoContacto(String puestoContacto) {
		this.puestoContacto = puestoContacto;
	}

	@Column(name="RAZONSOCIAL")
    @Size(min=0, max=200, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	@Column(name="TELEFONOCASA")
    @Size(min=0, max=12, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getTelefonoCasa() {
		return telefonoCasa;
	}

	public void setTelefonoCasa(String telefonoCasa) {
		this.telefonoCasa = telefonoCasa;
	}

	@Column(name="TELEFONOCELULAR")
    @Size(min=0, max=12, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getTelefonoCelular() {
		return telefonoCelular;
	}

	public void setTelefonoCelular(String telefonoCelular) {
		this.telefonoCelular = telefonoCelular;
	}

	@Column(name="TELEFONOFAX")
    @Size(min=0, max=12, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getTelefonoFax() {
		return telefonoFax;
	}

	public void setTelefonoFax(String telefonoFax) {
		this.telefonoFax = telefonoFax;
	}

	@Column(name="TELEFONOOFICINA")
    @Size(min=0, max=12, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getTelefonoOficina() {
		return telefonoOficina;
	}

	public void setTelefonoOficina(String telefonoOficina) {
		this.telefonoOficina = telefonoOficina;
	}

	@Column(name="TELEFONOSADICIONALES")
    @Size(min=0, max=200, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getTelefonosAdicionales() {
		return telefonosAdicionales;
	}

	public void setTelefonosAdicionales(String telefonosAdicionales) {
		this.telefonosAdicionales = telefonosAdicionales;
	}

	@Column(name="CODIGORFC")
    @Size(min=0, max=14, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
    public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	@Temporal( TemporalType.DATE)
    @Column(name = "FECHAALTA", nullable = false, length = 7)
	public Date getFechaAlta() {
		return this.fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}


    @Temporal( TemporalType.DATE)
    @Column(name = "FECHABAJA", length = 7)
	public Date getFechaBaja() {
		return this.fechaBaja;
	}

	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}


    @Temporal( TemporalType.DATE)
    @Column(name = "FECHACONSTITUCION", length = 7)
	public Date getFechaConstitucion() {
		return this.fechaConstitucion;
	}

	public void setFechaConstitucion(Date fechaConstitucion) {
		this.fechaConstitucion = fechaConstitucion;
	}

    @Temporal( TemporalType.DATE)
    @Column(name = "FECHAMODIFICACION", length = 7)
	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

    @Temporal( TemporalType.DATE)
    @Column(name = "FECHANACIMIENTO", length = 7)
	public Date getFechaNacimiento() {
		return this.fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	//bi-directional many-to-one association to Domicilio	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name="IDPERSONA")
	public List<Domicilio> getDomicilios() {
		return this.domicilios;
	}

	public void setDomicilios(List<Domicilio> domicilios) {
		this.domicilios = domicilios;
	}

	@Transient
	public String getClaveSectorFinanciero() {
		return claveSectorFinanciero;
	}

	public void setClaveSectorFinanciero(String claveSectorFinanciero) {
		this.claveSectorFinanciero = claveSectorFinanciero;
	}

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
