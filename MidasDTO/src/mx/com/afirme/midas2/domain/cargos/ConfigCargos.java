package mx.com.afirme.midas2.domain.cargos;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

@Entity(name="ConfigCargos")
@Table(name="toConfigCargos", schema="MIDAS")
public class ConfigCargos implements Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id; 
	private ValorCatalogoAgentes tipoCargo; 
	private Agente agente; 
	private ValorCatalogoAgentes estatusCargo;
	private Date fechaAltaCargo; 
	private double importe; 
	private ValorCatalogoAgentes plazo; 
	private Date fechaInicioCoBro; 
    private Long numeroCargos; 
    private double importeCargo;
    private String observaciones;
    //transient
    private ValorCatalogoAgentes tipoAgente; 
	private Date fechaFinCobro;
	private CentroOperacion centroOperacion;
	private Gerencia gerencia;
	private Promotoria promotoria;
	private Ejecutivo ejecutivo;
	private String fechaStrInicioCobro;
	private String fechaStrAltaCargo;
	private Integer tipoReporte;
	private List<ValorCatalogoAgentes> listEstatusCargo = new ArrayList<ValorCatalogoAgentes>();
	
	
    @Id
    @SequenceGenerator(name="idToCfgCargo_seq", sequenceName="MIDAS.idToCfgCargo_seq", allocationSize=1)
    @GeneratedValue(generator="idToCfgCargo_seq", strategy=GenerationType.SEQUENCE)
	public Long getId() {
		return id;
	}
    
	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY, targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDTIPOCARGO")
	public ValorCatalogoAgentes getTipoCargo() {
		return tipoCargo;
	}
	
	public void setTipoCargo(ValorCatalogoAgentes tipoCargo) {
		this.tipoCargo = tipoCargo;
	}

	@ManyToOne(fetch=FetchType.LAZY, targetEntity = Agente.class)
	@JoinColumn(name="IDAGENTE")
	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}


	@ManyToOne(fetch=FetchType.LAZY, targetEntity = ValorCatalogoAgentes.class)
	@JoinColumn(name="IDESTATUSCARGO")
	public ValorCatalogoAgentes getEstatusCargo() {
		return estatusCargo;
	}

	public void setEstatusCargo(ValorCatalogoAgentes estatusCargo) {
		this.estatusCargo = estatusCargo;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHAALTACARGO")
	public Date getFechaAltaCargo() {
		return fechaAltaCargo;
	}

	public void setFechaAltaCargo(Date fechaAltaCargo) {
		this.fechaAltaCargo = fechaAltaCargo;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");    	
		this.fechaStrAltaCargo=sdf.format(fechaAltaCargo);
	}

	@Column(name="IMPORTE")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public double getImporte() {
		return importe;
	}

	public void setImporte(double importe) {
		this.importe = importe;
	}
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="IDPLAZO")
	public ValorCatalogoAgentes getPlazo() {
		return plazo;
	}

	public void setPlazo(ValorCatalogoAgentes plazo) {
		this.plazo = plazo;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHAINICIOCOBRO")
	public Date getFechaInicioCoBro() {
		return fechaInicioCoBro;
	}

	public void setFechaInicioCoBro(Date fechaInicioCoBro) {
		this.fechaInicioCoBro = fechaInicioCoBro;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");    	
		this.fechaStrInicioCobro=sdf.format(fechaInicioCoBro);
	}

//	@Temporal(TemporalType.DATE)
	@Transient
	public Date getFechaFinCobro() {
		return fechaFinCobro;
	}

	public void setFechaFinCobro(Date fechaFinCobro) {
		this.fechaFinCobro = fechaFinCobro;
	}

	@Column(name="NUMEROCARGOS")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public Long getNumeroCargos() {
		return numeroCargos;
	}

	public void setNumeroCargos(Long numeroCargos) {
		this.numeroCargos = numeroCargos;
	}

	@Column(name="IMPORTECARGO")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public double getImporteCargo() {
		return importeCargo;
	}

	public void setImporteCargo(double importeCargo) {
		this.importeCargo = importeCargo;
	}

	@Column(name="OBSERVACIONES")
	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	@Transient
	public ValorCatalogoAgentes getTipoAgente() {
		return tipoAgente;
	}

	public void setTipoAgente(ValorCatalogoAgentes tipoAgente) {
		this.tipoAgente = tipoAgente;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		// TODO Auto-generated method stub
		return id;
	}

	@Transient
	public CentroOperacion getCentroOperacion() {
		return centroOperacion;
	}

	public void setCentroOperacion(CentroOperacion centroOperacion) {
		this.centroOperacion = centroOperacion;
	}

	@Transient
	public Gerencia getGerencia() {
		return gerencia;
	}

	public void setGerencia(Gerencia gerencia) {
		this.gerencia = gerencia;
	}

	@Transient
	public Promotoria getPromotoria() {
		return promotoria;
	}

	public void setPromotoria(Promotoria promotoria) {
		this.promotoria = promotoria;
	}

	@Transient
	public Ejecutivo getEjecutivo() {
		return ejecutivo;
	}

	public void setEjecutivo(Ejecutivo ejecutivo) {
		this.ejecutivo = ejecutivo;
	}

	@Transient
	public String getFechaStrInicioCobro() {
		return fechaStrInicioCobro;
	}

	public void setFechaStrInicioCobro(String fechaStrInicioCobro) {
		this.fechaStrInicioCobro = fechaStrInicioCobro;
	}

	@Transient
	public String getFechaStrAltaCargo() {
		return fechaStrAltaCargo;
	}

	public void setFechaStrAltaCargo(String fechaStrAltaCargo) {
		this.fechaStrAltaCargo = fechaStrAltaCargo;
	}

	@Transient
	public List<ValorCatalogoAgentes> getListEstatusCargo() {
		return listEstatusCargo;
	}

	public void setListEstatusCargo(List<ValorCatalogoAgentes> listEstatusCargo) {
		this.listEstatusCargo = listEstatusCargo;
	}

	@Transient
	public Integer getTipoReporte() {
		return tipoReporte;
	}

	public void setTipoReporte(Integer tipoReporte) {
		this.tipoReporte = tipoReporte;
	}
	
}
