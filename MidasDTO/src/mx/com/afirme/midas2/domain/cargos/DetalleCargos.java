package mx.com.afirme.midas2.domain.cargos;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

@Entity(name="DetalleCargos")
@Table(name="TODETALLECARGOS", schema="MIDAS")
public class DetalleCargos implements Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private ConfigCargos configCargos;
	private Date fechaCargo;
	private Double capitalInicial;
	private Double importeCargo;
	private Double importeRestante;
	private ValorCatalogoAgentes estatus;
	private String fechaCargoStr;
	
	@Id
	@SequenceGenerator(name="idtoDetalleCargos_seq", sequenceName="MIDAS.idtoDetalleCargos_seq", allocationSize=1)
	@GeneratedValue(generator="idtoDetalleCargos_seq", strategy=GenerationType.SEQUENCE)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY, targetEntity=ConfigCargos.class)
	@JoinColumn(name="IDCONFIGCARGOS")
	public ConfigCargos getConfigCargos() {
		return configCargos;
	}

	public void setConfigCargos(ConfigCargos configCargos) {
		this.configCargos = configCargos;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHACARGO")
	public Date getFechaCargo() {
		return fechaCargo;
	}

	public void setFechaCargo(Date fechaCargo) {
		this.fechaCargo = fechaCargo;
		if(fechaCargo!=null){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");    	
			this.fechaCargoStr=sdf.format(fechaCargo);
		}
	}

	@Column(name="CAPITALINICIAL")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public Double getCapitalInicial() {
		return capitalInicial;
	}

	public void setCapitalInicial(Double capitalInicial) {
		this.capitalInicial = capitalInicial;
	}

	@Column(name="IMPORTECARGO")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public Double getImporteCargo() {
		return importeCargo;
	}

	public void setImporteCargo(Double importeCargo) {
		this.importeCargo = importeCargo;
	}

	@Column(name="IMPORTERESTANTE")
	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public Double getImporteRestante() {
		return importeRestante;
	}

	public void setImporteRestante(Double importeRestante) {
		this.importeRestante = importeRestante;
	}

	@ManyToOne(fetch=FetchType.LAZY, targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDESTATUS")
	public ValorCatalogoAgentes getEstatus() {
		return estatus;
	}

	public void setEstatus(ValorCatalogoAgentes estatus) {
		this.estatus = estatus;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		// TODO Auto-generated method stub
		return id;
	}

	@Transient
	public String getFechaCargoStr() {
		return fechaCargoStr;
	}

	public void setFechaCargoStr(String fechaCargoStr) {
		this.fechaCargoStr = fechaCargoStr;
	}

}
