package mx.com.afirme.midas2.domain.comisiones;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

@Entity(name="ConfigComEjecutivo")
@Table(schema="MIDAS",name="trConfigComEjecutivo")
@SqlResultSetMapping(name="configComEjecutivoView",entities={
	@EntityResult(entityClass=ConfigComEjecutivo.class,fields={
		@FieldResult(name="id",column="id"),
		@FieldResult(name="idEjecutivo",column="idEjecutivo")
	})
})
public class ConfigComEjecutivo implements Entidad,Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7077796400428624844L;
	private Long	id;
	private	ConfigComisiones configuracionComisiones;
	private Ejecutivo ejecutivo;
	private Long idEjecutivo;
	public ConfigComEjecutivo(){}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTRCONFIGCOMEJECUTIVO_SEQ")
	@SequenceGenerator(name="IDTRCONFIGCOMEJECUTIVO_SEQ", sequenceName="MIDAS.IDTRCONFIGCOMEJECUTIVO_SEQ",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ConfigComisiones.class)
	@JoinColumn(name="CONFIGCOM_ID")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ConfigComisiones getConfiguracionComisiones() {
		return configuracionComisiones;
	}

	public void setConfiguracionComisiones(ConfigComisiones configuracionComisiones) {
		this.configuracionComisiones = configuracionComisiones;
	}
	@ManyToOne(fetch=FetchType.LAZY,targetEntity=Ejecutivo.class)
	@JoinColumn(name="EJECUTIVO_ID")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Ejecutivo getEjecutivo() {
		return ejecutivo;
	}

	public void setEjecutivo(Ejecutivo ejecutivo) {
		this.ejecutivo = ejecutivo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null; 
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	
	/**
	 * Hace el casting de una lista de configuracion de Ejecutivos a una lista de Ejecutivos
	 * @param listToConvert
	 * @return
	 */
	public static List<Ejecutivo> toEjecutivoList(List<ConfigComEjecutivo> listToConvert){
		List<Ejecutivo> list=new ArrayList<Ejecutivo>();
		if(listToConvert!=null && !listToConvert.isEmpty()){
			for(ConfigComEjecutivo element:listToConvert){
				Ejecutivo obj=toEjecutivo(element);
				if(obj!=null){
					list.add(obj);
				}
			}
		}
		return list;
	}
	/**
	 * metodo para convertir por medio de una configuracion obtener  Ejecutivo.
	 * @param objectToConvert
	 * @return
	 */
	public static Ejecutivo toEjecutivo(ConfigComEjecutivo objectToConvert){
		Ejecutivo convert=(objectToConvert!=null && objectToConvert.getEjecutivo()!=null)?objectToConvert.getEjecutivo():null;
		//Si no trae id entonces es nulo la conversion
		if(convert==null || convert.getId()==null){
			convert=null;
		}
		return convert;
	}
	
	@Transient
	public Long getIdEjecutivo() {
		return id;
	}

	public void setIdEjecutivo(Long idEjecutivo) {
		this.idEjecutivo = idEjecutivo;
	}
}
