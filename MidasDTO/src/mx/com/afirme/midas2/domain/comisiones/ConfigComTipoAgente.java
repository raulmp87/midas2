package mx.com.afirme.midas2.domain.comisiones;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
/**
 * 
 * @author vmhersil
 *
 */
@Entity(name="ConfigComTipoAgente")
@Table(schema="MIDAS",name="trConfigComTipoAgente")
@SqlResultSetMapping(name="configComTipoAgenteView",entities={
	@EntityResult(entityClass=ConfigComTipoAgente.class,fields={
		@FieldResult(name="id",column="id"),
		@FieldResult(name="idTipoAgente",column="idTipoAgente")
	})
})
public class ConfigComTipoAgente implements Entidad,Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8296427322752076838L;
	private Long	id;
	private	ConfigComisiones configuracionComisiones;
	private ValorCatalogoAgentes tipoAgente;
	private Long idTipoAgente;
	public ConfigComTipoAgente(){}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTRCONFIGCOMTIPOAGTE_SEQ")
	@SequenceGenerator(name="IDTRCONFIGCOMTIPOAGTE_SEQ", sequenceName="MIDAS.IDTRCONFIGCOMTIPOAGTE_SEQ",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ConfigComisiones.class)
	@JoinColumn(name="CONFIGCOM_ID")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ConfigComisiones getConfiguracionComisiones() {
		return configuracionComisiones;
	}

	public void setConfiguracionComisiones(ConfigComisiones configuracionComisiones) {
		this.configuracionComisiones = configuracionComisiones;
	}
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDTIPOAGENTE")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ValorCatalogoAgentes getTipoAgente() {
		return tipoAgente;
	}

	public void setTipoAgente(ValorCatalogoAgentes tipoAgente) {
		this.tipoAgente = tipoAgente;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null; 
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	@Transient
	public Long getIdTipoAgente() {
		return id;
	}

	public void setIdTipoAgente(Long idTipoAgente) {
		this.idTipoAgente = idTipoAgente;
	}
}
