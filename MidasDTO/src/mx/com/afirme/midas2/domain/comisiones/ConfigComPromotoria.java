package mx.com.afirme.midas2.domain.comisiones;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
/**
 * 
 * @author vmhersil
 *
 */
@Entity(name="ConfigComPromotoria")
@Table(schema="MIDAS",name="trConfigComPromotoria")
@SqlResultSetMapping(name="configComPromotoriaView",entities={
	@EntityResult(entityClass=ConfigComPromotoria.class,fields={
		@FieldResult(name="id",column="id"),
		@FieldResult(name="idPromotoria",column="idPromotoria")
	})
})
public class ConfigComPromotoria implements Entidad,Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3053447060453479467L;
	private Long	id;
	private	ConfigComisiones configuracionComisiones;
	private Promotoria promotoria;
	private Long idPromotoria;
	
	public ConfigComPromotoria(){}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTRCONFIGCOMPROMOTORIA_SEQ")
	@SequenceGenerator(name="IDTRCONFIGCOMPROMOTORIA_SEQ", sequenceName="MIDAS.IDTRCONFIGCOMPROMOTORIA_SEQ",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ConfigComisiones.class)
	@JoinColumn(name="CONFIGCOM_ID")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ConfigComisiones getConfiguracionComisiones() {
		return configuracionComisiones;
	}

	public void setConfiguracionComisiones(ConfigComisiones configuracionComisiones) {
		this.configuracionComisiones = configuracionComisiones;
	}
	@ManyToOne(fetch=FetchType.LAZY,targetEntity=Promotoria.class)
	@JoinColumn(name="PROMOTORIA_ID")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Promotoria getPromotoria() {
		return promotoria;
	}

	public void setPromotoria(Promotoria promotoria) {
		this.promotoria = promotoria;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null; 
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	
	/**
	 * Hace el casting de una lista de configuracion de Promotorias a una lista de Promotorias
	 * @param listToConvert
	 * @return
	 */
	public static List<Promotoria> toPromotoriaList(List<ConfigComPromotoria> listToConvert){
		List<Promotoria> list=new ArrayList<Promotoria>();
		if(listToConvert!=null && !listToConvert.isEmpty()){
			for(ConfigComPromotoria element:listToConvert){
				Promotoria obj=toPromotoria(element);
				if(obj!=null){
					list.add(obj);
				}
			}
		}
		return list;
	}
	/**
	 * metodo para convertir por medio de una configuracion obtener  Promotoria.
	 * @param objectToConvert
	 * @return
	 */
	public static Promotoria toPromotoria(ConfigComPromotoria objectToConvert){
		Promotoria convert=(objectToConvert!=null && objectToConvert.getPromotoria()!=null)?objectToConvert.getPromotoria():null;
		//Si no trae id entonces es nulo la conversion
		if(convert==null || convert.getId()==null){
			convert=null;
		}
		return convert;
	}
	@Transient
	public Long getIdPromotoria() {
		return id;
	}

	public void setIdPromotoria(Long idPromotoria) {
		this.idPromotoria = idPromotoria;
	}
}
