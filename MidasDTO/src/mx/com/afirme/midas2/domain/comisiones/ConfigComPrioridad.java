package mx.com.afirme.midas2.domain.comisiones;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
/**
 * 
 * @author vmhersil
 *
 */
@Entity(name="ConfigComPrioridad")
@Table(schema="MIDAS",name="trConfigComPrioridad")
@SqlResultSetMapping(name="configComPrioridadView",entities={
	@EntityResult(entityClass=ConfigComPrioridad.class,fields={
		@FieldResult(name="id",column="id"),
		@FieldResult(name="idPrioridad",column="idPrioridad")
	})
})
public class ConfigComPrioridad implements Entidad,Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8768874652493370676L;
	private Long	id;
	private	ConfigComisiones configuracionComisiones;
	private ValorCatalogoAgentes prioridadAgente;
	private Long idPrioridad;
	
	public ConfigComPrioridad(){}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTRCONFIGCOMPRIORIDAD_SEQ")
	@SequenceGenerator(name="IDTRCONFIGCOMPRIORIDAD_SEQ", sequenceName="MIDAS.IDTRCONFIGCOMPRIORIDAD_SEQ",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ConfigComisiones.class)
	@JoinColumn(name="CONFIGCOM_ID")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ConfigComisiones getConfiguracionComisiones() {
		return configuracionComisiones;
	}

	public void setConfiguracionComisiones(ConfigComisiones configuracionComisiones) {
		this.configuracionComisiones = configuracionComisiones;
	}
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDPRIORIDADAGENTE")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ValorCatalogoAgentes getPrioridadAgente() {
		return prioridadAgente;
	}

	public void setPrioridadAgente(ValorCatalogoAgentes prioridadAgente) {
		this.prioridadAgente = prioridadAgente;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null; 
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	@Transient
	public Long getIdPrioridad() {
		return id;
	}

	public void setIdPrioridad(Long idPrioridad) {
		this.idPrioridad = idPrioridad;
	}
}
