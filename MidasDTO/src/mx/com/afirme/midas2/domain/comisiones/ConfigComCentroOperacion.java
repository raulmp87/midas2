package mx.com.afirme.midas2.domain.comisiones;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
/**
 * 
 * @author vmhersil
 *
 */
@Entity(name="ConfigComCentroOperacion")
@Table(schema="MIDAS",name="trConfigComCentroOperacion")
@SqlResultSetMapping(name="configComCentroOperacionView",entities={
	@EntityResult(entityClass=ConfigComCentroOperacion.class,fields={
		@FieldResult(name="id",column="id"),
		@FieldResult(name="idCentroOperacion",column="idCentroOperacion")
	})
})
public class ConfigComCentroOperacion implements Entidad,Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5140578541493291821L;
	private Long	id;
	private	ConfigComisiones configuracionComisiones;
	private CentroOperacion centroOperacion;
	private Long idCentroOperacion;
	
	public ConfigComCentroOperacion(){}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTRCONFIGCOMCENTROOPE_SEQ")
	@SequenceGenerator(name="IDTRCONFIGCOMCENTROOPE_SEQ", sequenceName="MIDAS.IDTRCONFIGCOMCENTROOPE_SEQ",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ConfigComisiones.class)
	@JoinColumn(name="CONFIGCOM_ID")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ConfigComisiones getConfiguracionComisiones() {
		return configuracionComisiones;
	}

	public void setConfiguracionComisiones(ConfigComisiones configuracionComisiones) {
		this.configuracionComisiones = configuracionComisiones;
	}
	@ManyToOne(fetch=FetchType.LAZY,targetEntity=CentroOperacion.class)
	@JoinColumn(name="CENTROOPERACION_ID")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public CentroOperacion getCentroOperacion() {
		return centroOperacion;
	}

	public void setCentroOperacion(CentroOperacion centroOperacion) {
		this.centroOperacion = centroOperacion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null; 
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	
	/**
	 * Hace el casting de una lista de configuracion de CentroOperacion a una lista de CentroOperaciones
	 * @param listToConvert
	 * @return
	 */
	public static List<CentroOperacion> toCentroOperacionList(List<ConfigComCentroOperacion> listToConvert){
		List<CentroOperacion> list=new ArrayList<CentroOperacion>();
		if(listToConvert!=null && !listToConvert.isEmpty()){
			for(ConfigComCentroOperacion element:listToConvert){
				CentroOperacion obj=toCentroOperacion(element);
				if(obj!=null){
					list.add(obj);
				}
			}
		}
		return list;
	}
	/**
	 * metodo para convertir por medio de una configuracion obtener  CentroOperacion.
	 * @param objectToConvert
	 * @return
	 */
	public static CentroOperacion toCentroOperacion(ConfigComCentroOperacion objectToConvert){
		CentroOperacion convert=(objectToConvert!=null && objectToConvert.getCentroOperacion()!=null)?objectToConvert.getCentroOperacion():null;
		//Si no trae id entonces es nulo la conversion
		if(convert==null || convert.getId()==null){
			convert=null;
		}
		return convert;
	}
	@Transient
	public Long getIdCentroOperacion() {
		return id;
	}

	public void setIdCentroOperacion(Long idCentroOperacion) {
		this.idCentroOperacion = idCentroOperacion;
	}
	
	
}
