package mx.com.afirme.midas2.domain.comisiones;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ItemsConfigComision implements Serializable {

	private static final long serialVersionUID = 6757602520752911345L;

	Long idElemento;
	Integer id;
	
	@Id
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Long getIdElemento() {
		return idElemento;
	}
	public void setIdElemento(Long idElemento) {
		this.idElemento = idElemento;
	}
}
