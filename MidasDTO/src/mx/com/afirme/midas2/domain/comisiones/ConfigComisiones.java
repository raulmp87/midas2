package mx.com.afirme.midas2.domain.comisiones;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.dto.comisiones.ConfigComisionesDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.CentroOperacionView;
import mx.com.afirme.midas2.dto.fuerzaventa.EjecutivoView;
import mx.com.afirme.midas2.dto.fuerzaventa.GerenciaView;
import mx.com.afirme.midas2.dto.fuerzaventa.PromotoriaView;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
@Entity(name="ConfigComisiones")
@Table(name="toConfigComisiones",schema="MIDAS")
@SqlResultSetMapping(name="configComisionesProgramacionView",entities={
	@EntityResult(entityClass=ConfigComisiones.class,fields={
		@FieldResult(name="id",column="id"),
		@FieldResult(name="fechaInicioVigencia",column="fechaInicioVigencia")
	})
})
public class ConfigComisiones implements Serializable,Entidad{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3795302761914642507L;
	private Long 		id;
	private Boolean 	todosLosAgentesBoolean;
	private Integer 	todosLosAgentes;
	private Long 		agenteInicio;
	private Long 		agenteFin;
	private Double 		importeMinimo;
	private Boolean 	pagoSinFacturaBoolean;
	private Integer 	pagoSinFactura;
	private Date 		fechaInicioVigencia;
	private ValorCatalogoAgentes modoEjecucion;
	private ValorCatalogoAgentes periodoEjecucion;
	private Integer		diaEjecucion;
	private	Date		horarioEjecucion;
	private	Boolean		activoBoolean;
	private	Integer		activo;
	private String 		agentes;
	private Boolean 	todosLosAgentesAfirmeBoolean;
	private Integer 	todosLosAgentesAfirme;
	private List<ConfigComCentroOperacion> listaCentroOperaciones;
	private List<ConfigComGerencia> listaGerencias;
	private List<ConfigComEjecutivo> listaEjecutivos;
	private List<ConfigComPromotoria> listaPromotorias;
	private List<ConfigComTipoPromotoria> listaTiposPromotoria;
	private List<ConfigComPrioridad> listaPrioridades;
	private List<ConfigComSituacion> listaSituaciones;
	private List<ConfigComTipoAgente> listaTipoAgentes;
	private List<ConfigComMotivoEstatus>listaMotivoEstatus;
	//Estas listas se utilizan solo para hacer el calculo por una configuracion
	private List<CentroOperacionView> listaCentroOperacionView;
	private List<GerenciaView> listaGerenciaView;
	private List<EjecutivoView> listaEjecutivoView;
	private List<PromotoriaView> listaPromotoriaView;
	private Integer tipoCedulaId;
	public ConfigComisiones(){}
	
	public ConfigComisiones(ConfigComisionesDTO dto){
		this.activo=dto.getActivo();
		this.activoBoolean=dto.getActivoBoolean();
		this.agenteFin=dto.getAgenteFin();
		this.agenteInicio=dto.getAgenteInicio();
		this.diaEjecucion=dto.getDiaEjecucion();
		this.fechaInicioVigencia=dto.getFechaInicioVigencia();
		this.horarioEjecucion=dto.getHorarioEjecucion();
		this.id=dto.getId();
		this.agentes=dto.getAgentes();
		this.importeMinimo=dto.getImporteMinimo();
		this.modoEjecucion=dto.getModoEjecucion();
		this.pagoSinFactura=dto.getPagoSinFactura();
		this.pagoSinFacturaBoolean=dto.getPagoSinFacturaBoolean();
		this.periodoEjecucion=dto.getPeriodoEjecucion();
		this.todosLosAgentes=dto.getTodosLosAgentes();
		this.todosLosAgentesBoolean=dto.getTodosLosAgentesBoolean();
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTOCONFIGCOMISIONES_SEQ")
	@SequenceGenerator(name="IDTOCONFIGCOMISIONES_SEQ", sequenceName="MIDAS.IDTOCONFIGCOMISIONES_SEQ",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@Transient
	public Boolean getTodosLosAgentesBoolean() {
		return todosLosAgentesBoolean;
	}

	public void setTodosLosAgentesBoolean(Boolean todosLosAgentesBoolean) {
		this.todosLosAgentesBoolean = todosLosAgentesBoolean;
		this.todosLosAgentes=(this.todosLosAgentesBoolean)?1:0;
	}
	@Column(name="claveTodosAgentes")
	public Integer getTodosLosAgentes() {
		return todosLosAgentes;
	}

	public void setTodosLosAgentes(Integer todosLosAgentes) {
		this.todosLosAgentes = todosLosAgentes;
	}
	@Column(name="IDAGENTEINICIO")
	public Long getAgenteInicio() {
		return agenteInicio;
	}
	
	public void setAgenteInicio(Long agenteInicio) {
		this.agenteInicio = agenteInicio;
	}
	@Column(name="IDAGENTEFIN")
	public Long getAgenteFin() {
		return agenteFin;
	}

	public void setAgenteFin(Long agenteFin) {
		this.agenteFin = agenteFin;
	}
	@Column(name="IMPORTEMINIMO")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(fraction=3,integer=12,message="{com.afirme.midas2.decimal.length12}")
	public Double getImporteMinimo() {
		return importeMinimo;
	}

	public void setImporteMinimo(Double importeMinimo) {
		this.importeMinimo = importeMinimo;
	}
	@Transient
	public Boolean getPagoSinFacturaBoolean() {
		return pagoSinFacturaBoolean;
	}

	public void setPagoSinFacturaBoolean(Boolean pagoSinFacturaBoolean) {
		this.pagoSinFacturaBoolean = pagoSinFacturaBoolean;
		this.pagoSinFactura=(pagoSinFacturaBoolean)?1:0;
	}
	@Column(name="CLAVEPAGOSINFACTURA")
	public Integer getPagoSinFactura() {
		return pagoSinFactura;
	}
	
	public void setPagoSinFactura(Integer pagoSinFactura) {
		this.pagoSinFactura = pagoSinFactura;
	}
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAINICIOVIGENCIA")
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDMODOEJECUCION")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ValorCatalogoAgentes getModoEjecucion() {
		return modoEjecucion;
	}

	public void setModoEjecucion(ValorCatalogoAgentes modoEjecucion) {
		this.modoEjecucion = modoEjecucion;
	}
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDPERIODOEJECUCION")
	public ValorCatalogoAgentes getPeriodoEjecucion() {
		return periodoEjecucion;
	}

	public void setPeriodoEjecucion(ValorCatalogoAgentes periodoEjecucion) {
		this.periodoEjecucion = periodoEjecucion;
	}
	@Column(name="DIAEJECUCION")
	public Integer getDiaEjecucion() {
		return diaEjecucion;
	}

	public void setDiaEjecucion(Integer diaEjecucion) {
		this.diaEjecucion = diaEjecucion;
	}
	@Temporal(TemporalType.DATE)
	@Column(name="horarioEjecucion")
	public Date getHorarioEjecucion() {
		return horarioEjecucion;
	}

	public void setHorarioEjecucion(Date horarioEjecucion) {
		this.horarioEjecucion = horarioEjecucion;
	}
	@Transient
	public Boolean getActivoBoolean() {
		return activoBoolean;
	}

	public void setActivoBoolean(Boolean activoBoolean) {
		this.activoBoolean = activoBoolean;
		this.activo=(activoBoolean)?1:0;
	}
	@Column(name="CLAVEACTIVO")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Integer getActivo() {
		return activo;
	}

	public void setActivo(Integer activo) {
		this.activo = activo;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}
	@Column(name = "AGENTES", nullable = false, length = 4000)
	public String getAgentes() {
		return agentes;
	}

	public void setAgentes(String agentes) {
		this.agentes = agentes;
	}
	
	@Transient
	public Boolean getTodosLosAgentesAfirmeBoolean() {
		return todosLosAgentesAfirmeBoolean;
	}

	public void setTodosLosAgentesAfirmeBoolean(Boolean todosLosAgentesAfirmeBoolean) {
		this.todosLosAgentesAfirmeBoolean = todosLosAgentesAfirmeBoolean;
		this.todosLosAgentesAfirme=(this.todosLosAgentesAfirmeBoolean)?1:0;
	}

	@Column(name="CLAVETODOSAGENTESAFIRME")
	public Integer getTodosLosAgentesAfirme() {
		return todosLosAgentesAfirme;
	}

	public void setTodosLosAgentesAfirme(Integer todosLosAgentesAfirme) {
		this.todosLosAgentesAfirme = todosLosAgentesAfirme;
	}

	@Override
	public String getValue() {
		return null;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	
//	@OneToMany(mappedBy="configuracionComisiones",fetch=FetchType.LAZY,targetEntity=ConfigComCentroOperacion.class)
	@Transient
	public List<ConfigComCentroOperacion> getListaCentroOperaciones() {
		return listaCentroOperaciones;
	}

	public void setListaCentroOperaciones(List<ConfigComCentroOperacion> listaCentroOperaciones) {
		this.listaCentroOperaciones = listaCentroOperaciones;
	}
	
//	@OneToMany(mappedBy="configuracionComisiones",fetch=FetchType.LAZY,targetEntity=ConfigComGerencia.class)
	@Transient
	public List<ConfigComGerencia> getListaGerencias() {
		return listaGerencias;
	}

	public void setListaGerencias(List<ConfigComGerencia> listaGerencias) {
		this.listaGerencias = listaGerencias;
	}
//	@OneToMany(mappedBy="configuracionComisiones",fetch=FetchType.LAZY,targetEntity=ConfigComEjecutivo.class)
	@Transient
	public List<ConfigComEjecutivo> getListaEjecutivos() {
		return listaEjecutivos;
	}

	public void setListaEjecutivos(List<ConfigComEjecutivo> listaEjecutivos) {
		this.listaEjecutivos = listaEjecutivos;
	}
//	@OneToMany(mappedBy="configuracionComisiones",fetch=FetchType.LAZY, targetEntity=ConfigComPromotoria.class)
	@Transient
	public List<ConfigComPromotoria> getListaPromotorias() {
		return listaPromotorias;
	}

	public void setListaPromotorias(List<ConfigComPromotoria> listaPromotorias) {
		this.listaPromotorias = listaPromotorias;
	}
//	@OneToMany(mappedBy="configuracionComisiones",fetch=FetchType.LAZY,targetEntity=ConfigComTipoPromotoria.class)
	@Transient
	public List<ConfigComTipoPromotoria> getListaTiposPromotoria() {
		return listaTiposPromotoria;
	}

	public void setListaTiposPromotoria(
			List<ConfigComTipoPromotoria> listaTiposPromotoria) {
		this.listaTiposPromotoria = listaTiposPromotoria;
	}
//	@OneToMany(mappedBy="configuracionComisiones",fetch=FetchType.LAZY,targetEntity=ConfigComPrioridad.class)
	@Transient
	public List<ConfigComPrioridad> getListaPrioridades() {
		return listaPrioridades;
	}

	public void setListaPrioridades(List<ConfigComPrioridad> listaPrioridades) {
		this.listaPrioridades = listaPrioridades;
	}
//	@OneToMany(mappedBy="configuracionComisiones",fetch=FetchType.LAZY,targetEntity=ConfigComSituacion.class)
	@Transient
	public List<ConfigComSituacion> getListaSituaciones() {
		return listaSituaciones;
	}

	public void setListaSituaciones(List<ConfigComSituacion> listaSituaciones) {
		this.listaSituaciones = listaSituaciones;
	}
//	@OneToMany(mappedBy="configuracionComisiones",fetch=FetchType.LAZY,targetEntity=ConfigComTipoAgente.class)
	@Transient
	public List<ConfigComTipoAgente> getListaTipoAgentes() {
		return listaTipoAgentes;
	}

	public void setListaTipoAgentes(List<ConfigComTipoAgente> listaTipoAgentes) {
		this.listaTipoAgentes = listaTipoAgentes;
	}
	@Transient
	public List<CentroOperacionView> getListaCentroOperacionView() {
		return listaCentroOperacionView;
	}

	public void setListaCentroOperacionView(
			List<CentroOperacionView> listaCentroOperacionView) {
		this.listaCentroOperacionView = listaCentroOperacionView;
	}
	@Transient
	public List<GerenciaView> getListaGerenciaView() {
		return listaGerenciaView;
	}

	public void setListaGerenciaView(List<GerenciaView> listaGerenciaView) {
		this.listaGerenciaView = listaGerenciaView;
	}
	@Transient
	public List<EjecutivoView> getListaEjecutivoView() {
		return listaEjecutivoView;
	}

	public void setListaEjecutivoView(List<EjecutivoView> listaEjecutivoView) {
		this.listaEjecutivoView = listaEjecutivoView;
	}
	@Transient
	public List<PromotoriaView> getListaPromotoriaView() {
		return listaPromotoriaView;
	}

	public void setListaPromotoriaView(List<PromotoriaView> listaPromotoriaView) {
		this.listaPromotoriaView = listaPromotoriaView;
	}
	
	@Transient
	public Integer getTipoCedulaId() {
		return tipoCedulaId;
	}

	public void setTipoCedulaId(Integer tipoCedulaId) {
		this.tipoCedulaId = tipoCedulaId;
	}

	@Transient
	public List<ConfigComMotivoEstatus> getListaMotivoEstatus() {
		return listaMotivoEstatus;
	}

	public void setListaMotivoEstatus(
			List<ConfigComMotivoEstatus> listaMotivoEstatus) {
		this.listaMotivoEstatus = listaMotivoEstatus;
	}
	
	
}
