package mx.com.afirme.midas2.domain.comisiones;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

@Entity(name="ConfigComMotivoEstatus")
@Table(schema="MIDAS",name="TRCONFIGCOMMOTESTATUS")
@SqlResultSetMapping(name="configComMotivoEstatusView",entities={
	@EntityResult(entityClass=ConfigComTipoAgente.class,fields={
		@FieldResult(name="id",column="id"),
		@FieldResult(name="idMotivoEstatus",column="idTipoAgente")
	})
})
public class ConfigComMotivoEstatus implements Entidad, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5366064855624250248L;
	private Long	id;
	private	ConfigComisiones configuracionComisiones;
	private ValorCatalogoAgentes idMotivoEstatus;


	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "TRCONFIGCOMMOTESTATUS_SEQ")
	@SequenceGenerator(name= "TRCONFIGCOMMOTESTATUS_SEQ", sequenceName="MIDAS.TRCONFIGCOMMOTESTATUS_SEQ",allocationSize=1)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ConfigComisiones.class)
	@JoinColumn(name="CONFIGCOM_ID")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ConfigComisiones getConfiguracionComisiones() {
		return configuracionComisiones;
	}

	public void setConfiguracionComisiones(ConfigComisiones configuracionComisiones) {
		this.configuracionComisiones = configuracionComisiones;
	}
	
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDMOTIVOESTATUS")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ValorCatalogoAgentes getIdMotivoEstatus() {
		return idMotivoEstatus;
	}

	public void setIdMotivoEstatus(ValorCatalogoAgentes idMotivoEstatus) {
		this.idMotivoEstatus = idMotivoEstatus;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		// TODO Auto-generated method stub
		return id;
	}
	
}
