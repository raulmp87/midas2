package mx.com.afirme.midas2.domain.comisiones;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

@Entity(name="ConfigComGerencia")
@Table(schema="MIDAS",name="trConfigComGerencia")
@SqlResultSetMapping(name="configComGerenciaView",entities={
	@EntityResult(entityClass=ConfigComGerencia.class,fields={
		@FieldResult(name="id",column="id"),
		@FieldResult(name="idGerencia",column="idGerencia")
	})
})
public class ConfigComGerencia implements Entidad,Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4236079402636611093L;
	private Long	id;
	private	ConfigComisiones configuracionComisiones;
	private Gerencia gerencia;
	private Long idGerencia;
	public ConfigComGerencia(){}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTRCONFIGCOMGERENCIA_SEQ")
	@SequenceGenerator(name="IDTRCONFIGCOMGERENCIA_SEQ", sequenceName="MIDAS.IDTRCONFIGCOMGERENCIA_SEQ",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ConfigComisiones.class)
	@JoinColumn(name="CONFIGCOM_ID")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ConfigComisiones getConfiguracionComisiones() {
		return configuracionComisiones;
	}

	public void setConfiguracionComisiones(ConfigComisiones configuracionComisiones) {
		this.configuracionComisiones = configuracionComisiones;
	}
	@ManyToOne(fetch=FetchType.LAZY,targetEntity=Gerencia.class)
	@JoinColumn(name="GERENCIA_ID")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Gerencia getGerencia() {
		return gerencia;
	}

	public void setGerencia(Gerencia gerencia) {
		this.gerencia = gerencia;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null; 
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	/**
	 * Hace el casting de una lista de configuracion de gerencias a una lista de gerencias
	 * @param listToConvert
	 * @return
	 */
	public static List<Gerencia> toGerenciaList(List<ConfigComGerencia> listToConvert){
		List<Gerencia> list=new ArrayList<Gerencia>();
		if(listToConvert!=null && !listToConvert.isEmpty()){
			for(ConfigComGerencia element:listToConvert){
				Gerencia obj=toGerencia(element);
				if(obj!=null){
					list.add(obj);
				}
			}
		}
		return list;
	}
	/**
	 * metodo para convertir por medio de una configuracion obtener la gerencia.
	 * @param objectToConvert
	 * @return
	 */
	public static Gerencia toGerencia(ConfigComGerencia objectToConvert){
		Gerencia convert=(objectToConvert!=null && objectToConvert.getGerencia()!=null )?objectToConvert.getGerencia():null;
		//Si no trae id entonces es nulo la conversion
		if(convert==null || convert.getId()==null){
			convert=null;
		}
		return convert;
	}
	@Transient
	public Long getIdGerencia() {
		return id;
	}

	public void setIdGerencia(Long idGerencia) {
		this.idGerencia = idGerencia;
	}	
}
