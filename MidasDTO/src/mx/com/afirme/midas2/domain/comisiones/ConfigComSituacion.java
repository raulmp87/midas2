package mx.com.afirme.midas2.domain.comisiones;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
/**
 * 
 * @author vmhersil
 *
 */
@Entity(name="ConfigComSituacion")
@Table(schema="MIDAS",name="trConfigComSituacion")
@SqlResultSetMapping(name="configComSituacionView",entities={
	@EntityResult(entityClass=ConfigComSituacion.class,fields={
		@FieldResult(name="id",column="id"),
		@FieldResult(name="idSituacion",column="idSituacion")
	})
})
public class ConfigComSituacion implements Entidad,Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7953789878207354720L;
	private Long	id;
	private	ConfigComisiones configuracionComisiones;
	private ValorCatalogoAgentes situacionAgente;
	private Long idSituacion;
	public ConfigComSituacion(){}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTRCONFIGCOMSITUACION_SEQ")
	@SequenceGenerator(name="IDTRCONFIGCOMSITUACION_SEQ", sequenceName="MIDAS.IDTRCONFIGCOMSITUACION_SEQ",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ConfigComisiones.class)
	@JoinColumn(name="CONFIGCOM_ID")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ConfigComisiones getConfiguracionComisiones() {
		return configuracionComisiones;
	}

	public void setConfiguracionComisiones(ConfigComisiones configuracionComisiones) {
		this.configuracionComisiones = configuracionComisiones;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDSITUACIONAGENTE")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ValorCatalogoAgentes getSituacionAgente() {
		return situacionAgente;
	}

	public void setSituacionAgente(ValorCatalogoAgentes situacionAgente) {
		this.situacionAgente = situacionAgente;
	}

	@Override
	public String getValue() {
		return null; 
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	@Transient
	public Long getIdSituacion() {
		return id;
	}

	public void setIdSituacion(Long idSituacion) {
		this.idSituacion = idSituacion;
	}
}
