package mx.com.afirme.midas2.domain.provisiones;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;

@Table(name="toReciboProvisionBono",schema="MIDAS")
@Entity(name="ReciboProvisionBono")
public class ReciboProvisionBono implements Serializable,Entidad{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private DetalleProvisionBono detalleProvisionBono;
	private Agente agente;
	private Long idRecibo;
	private Double primaNeta;
	private Date fechaVencimiento;
	private Long idRamo;
	private Long idSubRamo;
	private Long idLineaNegocio;
	private Long numPoliza;
	private Long idCotizacion;
	private Long idVersionPol;
	private Long idSolicitud;
	
	
	@Id
	@GeneratedValue(generator="idToReciboProvBon_seq",strategy=GenerationType.SEQUENCE)
	@SequenceGenerator(name="idToReciboProvBon_seq",sequenceName="MIDAS.idToReciboProvBon_seq",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=DetalleProvisionBono.class)
	@JoinColumn(name="IDDETALLEPROVISIONBONO")
	public DetalleProvisionBono getDetalleProvisionBono() {
		return detalleProvisionBono;
	}

	public void setDetalleProvisionBono(DetalleProvisionBono detalleProvisionBono) {
		this.detalleProvisionBono = detalleProvisionBono;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=Agente.class)
	@JoinColumn(name="IDAGENTE")
	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	@Column(name="IDRECIBO")
	public Long getIdRecibo() {
		return idRecibo;
	}

	public void setIdRecibo(Long idRecibo) {
		this.idRecibo = idRecibo;
	}

	@Column(name="PRIMANETA")
	public Double getPrimaNeta() {
		return primaNeta;
	}

	public void setPrimaNeta(Double primaNeta) {
		this.primaNeta = primaNeta;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHAVENCIMIENTO")
	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	@Column(name="IDRAMO")
	public Long getIdRamo() {
		return idRamo;
	}

	public void setIdRamo(Long idRamo) {
		this.idRamo = idRamo;
	}

	@Column(name="IDSUBRAMO")
	public Long getIdSubRamo() {
		return idSubRamo;
	}

	public void setIdSubRamo(Long idSubRamo) {
		this.idSubRamo = idSubRamo;
	}

	@Column(name="IDLINEANEGOCIO")
	public Long getIdLineaNegocio() {
		return idLineaNegocio;
	}

	public void setIdLineaNegocio(Long idLineaNegocio) {
		this.idLineaNegocio = idLineaNegocio;
	}

	@Column(name="NUMPOLIZA")
	public Long getNumPoliza() {
		return numPoliza;
	}

	public void setNumPoliza(Long numPoliza) {
		this.numPoliza = numPoliza;
	}

	@Column(name="IDCOTIZACION")
	public Long getIdCotizacion() {
		return idCotizacion;
	}

	public void setIdCotizacion(Long idCotizacion) {
		this.idCotizacion = idCotizacion;
	}

	@Column(name="IDVERSIONPOL")
	public Long getIdVersionPol() {
		return idVersionPol;
	}

	public void setIdVersionPol(Long idVersionPol) {
		this.idVersionPol = idVersionPol;
	}

	@Column(name="IDSOLICITUD")
	public Long getIdSolicitud() {
		return idSolicitud;
	}

	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}
