package mx.com.afirme.midas2.domain.provisiones;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoAgente;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProvisionesAgentes;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;

@Table(name="toProvisionImportesRamo",schema="MIDAS")
@Entity(name="ProvisionImportesRamo")
public class ProvisionImportesRamo implements Serializable,Entidad{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private ProvisionesAgentes provisionAgentes;
	private Long idRamo;
	private ConfigBonos configBono;
	private Double importe;
	private Long idLineaVenta;
	private ProductoDTO producto;
	private Long idLineaNegocio;
	private Long idCobertura;
	private Long idPoliza;
	private Long idCentroOperacion;
	private Long idGerencia;
	private Ejecutivo ejecutivo;
	private Promotoria promotoria;
	private Agente agenteOrigen;
	private Agente agenteFin;
	private ValorCatalogoAgentes tipoPersona;
	
	@Id
	@SequenceGenerator(name="idToProvImportesRamo_seq",sequenceName="MIDAS.idToProvImportesRamo_seq",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="idToProvImportesRamo_seq")
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ProvisionesAgentes.class)
	@JoinColumn(name="IDPROVISION")
	public ProvisionesAgentes getProvisionAgentes() {
		return provisionAgentes;
	}

	public void setProvisionAgentes(ProvisionesAgentes provisionAgentes) {
		this.provisionAgentes = provisionAgentes;
	}

	@Column(name="IDRAMO")
	public Long getIdRamo() {
		return idRamo;
	}

	public void setIdRamo(Long idRamo) {
		this.idRamo = idRamo;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ConfigBonos.class)
	@JoinColumn(name="IDBONO")
	public ConfigBonos getConfigBono() {
		return configBono;
	}

	public void setConfigBono(ConfigBonos configBono) {
		this.configBono = configBono;
	}

	@Column(name="IMPORTE")
	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	@Column(name="IDLINEAVENTA")
	public Long getIdLineaVenta() {
		return idLineaVenta;
	}

	public void setIdLineaVenta(Long idLineaVenta) {
		this.idLineaVenta = idLineaVenta;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ProductoDTO.class)
	@JoinColumn(name="IDPRODUCTO")
	public ProductoDTO getProducto() {
		return producto;
	}

	public void setProducto(ProductoDTO producto) {
		this.producto = producto;
	}

	@Column(name="IDLINEANEGOCIO")
	public Long getIdLineaNegocio() {
		return idLineaNegocio;
	}

	public void setIdLineaNegocio(Long idLineaNegocio) {
		this.idLineaNegocio = idLineaNegocio;
	}

	@Column(name="IDCOBERTURA")
	public Long getIdCobertura() {
		return idCobertura;
	}

	public void setIdCobertura(Long idCobertura) {
		this.idCobertura = idCobertura;
	}

	@Column(name="IDPOLIZA")
	public Long getIdPoliza() {
		return idPoliza;
	}

	public void setIdPoliza(Long idPoliza) {
		this.idPoliza = idPoliza;
	}

	@Column(name="IDCENTROOPERACION")
	public Long getIdCentroOperacion() {
		return idCentroOperacion;
	}

	public void setIdCentroOperacion(Long idCentroOperacion) {
		this.idCentroOperacion = idCentroOperacion;
	}

	@Column(name="IDGERENCIA")
	public Long getIdGerencia() {
		return idGerencia;
	}

	public void setIdGerencia(Long idGerencia) {
		this.idGerencia = idGerencia;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=Ejecutivo.class)
	@JoinColumn(name="IDEJECUTIVO")
	public Ejecutivo getEjecutivo() {
		return ejecutivo;
	}

	public void setEjecutivo(Ejecutivo ejecutivo) {
		this.ejecutivo = ejecutivo;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=Promotoria.class)
	@JoinColumn(name="IDPROMOTORIA")
	public Promotoria getPromotoria() {
		return promotoria;
	}

	public void setPromotoria(Promotoria promotoria) {
		this.promotoria = promotoria;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=Agente.class)
	@JoinColumn(name="IDAGENTEORIGEN")
	public Agente getAgenteOrigen() {
		return agenteOrigen;
	}

	public void setAgenteOrigen(Agente agenteOrigen) {
		this.agenteOrigen = agenteOrigen;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=Agente.class)
	@JoinColumn(name="IDAGENTEFIN")
	public Agente getAgenteFin() {
		return agenteFin;
	}

	public void setAgenteFin(Agente agenteFin) {
		this.agenteFin = agenteFin;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="TIPOPERSONA")
	public ValorCatalogoAgentes getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(ValorCatalogoAgentes tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}
