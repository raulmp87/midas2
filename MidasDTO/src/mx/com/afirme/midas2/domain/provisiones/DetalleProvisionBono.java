package mx.com.afirme.midas2.domain.provisiones;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProvisionesAgentes;

@Table(name="toDetalleProvisionBono",schema="MIDAS")
@Entity(name="DetalleProvisionBono")
public class DetalleProvisionBono implements Serializable,Entidad{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Agente idBeneficiario;
	private String beneficiario;
	private Double prima;
	private Double bonoAPagar;
	private ConfigBonoAgente idBono;
	private ProvisionesAgentes idProvision;
	

	@Id
	@GeneratedValue(generator="idToDetalleProvBono_seq",strategy=GenerationType.SEQUENCE)
	@SequenceGenerator(name="idToDetalleProvBono_seq",sequenceName="MIDAS.idToDetalleProvBono_seq",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=Agente.class)
	@JoinColumn(name="IDBENEFICIARIO")
	public Agente getIdBeneficiario() {
		return idBeneficiario;
	}

	public void setIdBeneficiario(Agente idBeneficiario) {
		this.idBeneficiario = idBeneficiario;
	}

	@Column(name="BENEFICIARIO")
	public String getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	@Column(name="PRIMA")
	public Double getPrima() {
		return prima;
	}

	public void setPrima(Double prima) {
		this.prima = prima;
	}

	@Column(name="BONOPAGAR")
	public Double getBonoAPagar() {
		return bonoAPagar;
	}

	public void setBonoAPagar(Double bonoAPagar) {
		this.bonoAPagar = bonoAPagar;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ConfigBonoAgente.class)
	@JoinColumn(name="IDBONO")
	public ConfigBonoAgente getIdBono() {
		return idBono;
	}

	public void setIdBono(ConfigBonoAgente idBono) {
		this.idBono = idBono;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ProvisionesAgentes.class)
	@JoinColumn(name="IDPROVISION")
	public ProvisionesAgentes getIdProvision() {
		return idProvision;
	}

	public void setIdProvision(ProvisionesAgentes idProvision) {
		this.idProvision = idProvision;
	}

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}
