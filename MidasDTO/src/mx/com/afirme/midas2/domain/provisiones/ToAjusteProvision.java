package mx.com.afirme.midas2.domain.provisiones;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProvisionesAgentes;

@Entity(name="ToAjusteProvision")
@Table(schema="MIDAS", name="TOAJUSTEPROVISION")
public class ToAjusteProvision implements Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1410392096624493442L;
	
	public Long id;
	public ProvisionesAgentes provision;
	public RamoDTO ramo;
	public Double importe;
	
	@Id
	@Column(name="ID")
	@SequenceGenerator(name="idToajusteProv_seq", sequenceName="MIDAS.idToajusteProv_seq", allocationSize=1)
	@GeneratedValue(generator="idToajusteProv_seq", strategy=GenerationType.SEQUENCE)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ProvisionesAgentes.class)
	@JoinColumn(name="IDPROVISION")
	public ProvisionesAgentes getProvision() {
		return provision;
	}
	public void setProvision(ProvisionesAgentes provision) {
		this.provision = provision;
	}
	
	@ManyToOne(fetch=FetchType.LAZY,targetEntity=RamoDTO.class)
	@JoinColumn(name="IDRAMO")
	public RamoDTO getRamo() {
		return ramo;
	}
	public void setRamo(RamoDTO ramo) {
		this.ramo = ramo;
	}
	
	@Column(name="IMPORTE")
	public Double getImporte() {
		return importe;
	}
	public void setImporte(Double importe) {
		this.importe = importe;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long  getKey() {
		return id;
	}
	@Override
	public String getValue() {
		return null;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		// TODO Auto-generated method stub
		return id;
	}
	
}
