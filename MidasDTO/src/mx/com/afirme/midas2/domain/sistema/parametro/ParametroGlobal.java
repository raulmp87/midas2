package mx.com.afirme.midas2.domain.sistema.parametro;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

@Entity
@Table(name="TCPARAMETROGLOBAL",schema="MIDAS")
@Cache(type=CacheType.FULL,	
		alwaysRefresh = false, 
		size = 100, 
		disableHits = false, 
		refreshOnlyIfNewer=true)
public class ParametroGlobal implements Entidad{
	/**
	 * 
	 */
	private static final long serialVersionUID = 170531232830412812L;
	
    @EmbeddedId
    @AttributeOverrides( {
        @AttributeOverride(name="aplicacionId", column=@Column(name="ID_APLICACION") ), 
        @AttributeOverride(name="parametroId", column=@Column(name="ID_PARAMETRO") ) } )
	ParametroGlobalId id;
    @Column(name="VALOR")
    String valor;
    @Column(name="DESCRIPCION")
    String descripcion;
    @Column(name="ACTIVO")
    boolean activo;
    
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CREACION", nullable = false, updatable= false)
	protected Date   fechaCreacion = new Date();
    
    @Column(name = "CODIGO_USUARIO_CREACION", length = 8, updatable = false)
	protected String codigoUsuarioCreacion;
	
	
	public ParametroGlobalId getId() {
		return id;
	}
	public void setId(ParametroGlobalId id) {
		this.id = id;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public boolean isActivo() {
		return activo;
	}
	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ParametroGlobalId getKey() {
		return id;
	}
	@Override
	public String getValue() {
		return valor;
	}
	@SuppressWarnings("unchecked")
	@Override
	public ParametroGlobalId getBusinessKey() {
		return id;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}
	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
	
}