package mx.com.afirme.midas2.domain.sistema.comm;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name = "TOCOMMBITACORADETALLE", schema = "MIDAS")
public class CommBitacoraDetalle implements Entidad{

	private static final long serialVersionUID = 4207506870792848666L;
	
	public static enum STATUS_COMM{
		PENDIENTE, OK, NOK_DATA_ERROR, NOK_COMM_ERROR, NOK_FATAL_ERROR
	};

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOCOMMBITACORADETALLE_GENERATOR")
	@SequenceGenerator(name="TOCOMMBITACORADETALLE_GENERATOR", schema="MIDAS", sequenceName="TOCOMMBITACORADETALLE_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn( name="bitacoraid", referencedColumnName="id", updatable=false)
	private CommBitacora bitacora;
	
	@Column(name="MENSAJEINICIAL")
	private String mensajeInicialSerializado;
	
	@Column(name="MENSAJEFINAL")
	private String mensajeFinalSerializado;
	
	@Column(name="CVEUSUARIO")
	private String cveUsuario;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHAENVIO")
	private Date fechaEnvio;
	
	@Column(name="STATUSCOMM")
	private String statusComm;
	
	@Column(name="STATUSDESCRIPCION")
	private String statusDescripcion;
	
	public CommBitacoraDetalle() {
		super();
	}
		
	public CommBitacoraDetalle(String mensajeInicialSerializado, STATUS_COMM status, String cveUsuario, String statusDescripcion) {
		super();	
		this.mensajeInicialSerializado = mensajeInicialSerializado;
		this.statusComm = status != null ? status.toString() : null;
		this.cveUsuario = cveUsuario;
		this.fechaEnvio = new Date();
		this.statusDescripcion = statusDescripcion;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CommBitacora getBitacora() {
		return bitacora;
	}

	public void setBitacora(CommBitacora bitacora) {
		this.bitacora = bitacora;
	}

	public String getMensajeInicialSerializado() {
		return mensajeInicialSerializado;
	}

	public void setMensajeInicialSerializado(String mensajeInicialSerializado) {
		this.mensajeInicialSerializado = mensajeInicialSerializado;
	}

	public String getMensajeFinalSerializado() {
		return mensajeFinalSerializado;
	}

	public void setMensajeFinalSerializado(String mensajeFinalSerializado) {
		this.mensajeFinalSerializado = mensajeFinalSerializado;
	}

	public Date getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	public String getStatusComm() {
		return statusComm;
	}

	public void setStatusComm(String statusComm) {
		this.statusComm = statusComm;
	}
	
	public String getCveUsuario() {
		return cveUsuario;
	}

	public void setCveUsuario(String cveUsuario) {
		this.cveUsuario = cveUsuario;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return id.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {	
		return id;
	}

	public String getStatusDescripcion() {
		return statusDescripcion;
	}

	public void setStatusDescripcion(String statusDescripcion) {
		this.statusDescripcion = statusDescripcion;
	}

}
