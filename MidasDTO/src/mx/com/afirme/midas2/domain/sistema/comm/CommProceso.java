package mx.com.afirme.midas2.domain.sistema.comm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;


@Entity
@Table(name = "TOCOMMPROCESO", schema = "MIDAS")
public class CommProceso implements Entidad {

	private static final long serialVersionUID = 6633539626886870015L;

	public static enum TIPO_PROCESO {
		HGS_ALTA_PASE, OCRA_ALTA_ROBO, ORDEN_COMPRA_PAGADA, SPV_ENVIAR_REPORTE
	};
	
	public static enum TIPO_COMM{
		WS
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTOCOMMPROCESO_GENERATOR")
	@SequenceGenerator(name="IDTOCOMMPROCESO_GENERATOR", schema="MIDAS", sequenceName="IDTOCOMMPROCESO_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;
	
	@Column(name="CODIGO")
	private String codigo;
	
	@Column(name="DESCRIPCION")
	private String descripcion;
	
	@Column(name="TIPOCOMM")
	private String tipoComm;
	
	@Column(name="MAXNUMREINTENTOS")
	private Integer maxNumReintentos;
	
	@Column(name="NOTIFICAR")
	private Boolean notificar;
	
	@Column(name="CODIGOPROCESONOTIFICACION")
	private String codigoProcesoNotificacion;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTipoComm() {
		return tipoComm;
	}

	public void setTipoComm(String tipoComm) {
		this.tipoComm = tipoComm;
	}

	public Integer getMaxNumReintentos() {
		return maxNumReintentos;
	}

	public void setMaxNumReintentos(Integer maxNumReintentos) {
		this.maxNumReintentos = maxNumReintentos;
	}

	public Boolean getNotificar() {
		return notificar;
	}

	public void setNotificar(Boolean notificar) {
		this.notificar = notificar;
	}

	public String getCodigoProcesoNotificacion() {
		return codigoProcesoNotificacion;
	}

	public void setCodigoProcesoNotificacion(String codigoProcesoNotificacion) {
		this.codigoProcesoNotificacion = codigoProcesoNotificacion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {	
		return id;
	}

	@Override
	public String getValue() {	
		return descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getBusinessKey() {
		return codigo;
	}
	
	
}
