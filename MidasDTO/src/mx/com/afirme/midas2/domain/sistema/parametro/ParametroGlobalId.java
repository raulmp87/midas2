package mx.com.afirme.midas2.domain.sistema.parametro;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ParametroGlobalId {
	
	@Column(name="ID_APLICACION")
	int aplicacionId;
	@Column(name="ID_PARAMETRO")
	String parametroId;
	
	public ParametroGlobalId(){
		super();
	}

	public ParametroGlobalId(int aplicacionId, String parametroId) {
		super();
		this.aplicacionId = aplicacionId;
		this.parametroId = parametroId;
	}

	public int getAplicacionId() {
		return aplicacionId;
	}

	public void setAplicacionId(int aplicacionId) {
		this.aplicacionId = aplicacionId;
	}

	public String getParametroId() {
		return parametroId;
	}

	public void setParametroId(String parametroId) {
		this.parametroId = parametroId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + aplicacionId;
		result = prime * result + ((parametroId == null) ? 0 : parametroId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParametroGlobalId other = (ParametroGlobalId) obj;
		if (aplicacionId != other.aplicacionId)
			return false;
		if (parametroId == null) {
			if (other.parametroId != null)
				return false;
		} else if (!parametroId.equals(other.parametroId))
			return false;
		return true;
	}
	
	
}
