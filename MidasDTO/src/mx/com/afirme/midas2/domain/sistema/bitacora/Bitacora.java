package mx.com.afirme.midas2.domain.sistema.bitacora;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="TOBITACORAMIDAS", schema="MIDAS")
public class Bitacora implements Entidad {

	private static final long serialVersionUID = 2585159796603797572L;

	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOBITACORAMIDAS_GENERATOR")
	@SequenceGenerator(name="TOBITACORAMIDAS_GENERATOR", schema="MIDAS", sequenceName="TOBITACORAMIDAS_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;
	
	@Enumerated(EnumType.STRING)
	@Column(name="TIPO_BITACORA")
	private TIPO_BITACORA tipoBitacora;
	
	@Enumerated(EnumType.STRING)
	@Column(name="EVENTO")
	private EVENTO evento;
	
	@Column(name="IDENTIFICADOR")
	private String identificador;
	
	@Column(name="MENSAJE")
	private String mensaje;
	
	@Column(name="DETALLE")
	private String detalle;
	
	@Column(name="CONTENEDOR_DETALLE")
	private String contenedorDetalle;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;
	
	@Column(name="CLAVE_USUARIO")
	private String usuario;
	
	public enum TIPO_BITACORA{
		LIQUIDACION,
		ORDEN_COMPRA,
		ORDEN_PAGO,
		FACTURACION,
		NOTA_CREDITO,
		HGS,
		RECUPERACION,
		INGRESO,
		SALVAMENTO,
		EMISION_FACTURA,
		REPORTE_CABINA,
		MONITORES,
		INGRESO_APL_AUTOMATICA,
		SPV,
		CARGA_MASIVA_SP,
		EJECUTIVO_COLOCA
	};
	
	public enum EVENTO{
		SOLICITUD_CHEQUE,
		CREACION_ORDENCOMPRA,
		MODIFICACION_ORDENCOMPRA,
		ORDENCOMPRA_ALTA_CONCEPTO,
		ORDENCOMPRA_BAJA_CONCEPTO,
		MODIFICACION_LIQUIDACION,
		CREACION_ORDENPAGO,
		MODIFICACION_ORDENPAGO,
		CREACION_FACTURA,
		CREACION_NOTA_CREDITO,
		CANCELACION_NOTA_CREDITO,
		RECUPERACION_PIEZAS,
		NOTIFICACION_ORDEN_COMPRA_HGS,
		CANCELAR_RECUPERACION,
		CREAR_RECUPERACION,
		EDITAR_RECUPERACION,
		DERIVACION_HGS,
		CANCELAR_INGRESO_PENDIENTE_APLICAR,
		RECUPERACION_SALVAMENTO,
		CANCELAR_INGRESO_DEVOLUCION,
		CANCELAR_INGRESO_CUENTA_ACREEDORA,
		CANCELAR_INGRESO_REGRESA_PENDIENTE,
		REGISTRAR_DEVOLUCION_SOLICITUDCHEQUE,
		AUTORIZAR_DEVOLUCION_SOLICITUDCHEQUE,
		CANCELAR_DEVOLUCION_SOLICITUDCHEQUE,
		RECHAZA_DEVOLUCION_INGRESO,
		ENVIAR_EDOCUENTA_COMPANIA,
		VENTA_SALVAMENTO,
		CANCELAR_VENTA_SALVAMENTO,
		AUTORIZAR_PRORROGA_SALVAMENTO,
		SOLICITAR_PRORROGA_SALVAMENTO,
		REDOCUMENTAR_CARTA,
		IDENTIFICA_CARTA_AELABORAR,
		GENERAR_INGRESO,
		CREAR_EMISION_FACTURA,
		ACTUALIZAR_EMISION_FACTURA,
		GENERAR_FACTURA,
		REENVIAR_FACTURA,
		GENERAR_REPORTE_CABINA_WS,
		BUSQEDA_INCISO_CABINA_WS,
		ELIMINAR_HISTORIAL_AJUSTADORES,
		GENERAR_ORDEN_COMPRA_HGS,
		INGRESO_APL_AUTOMATICA_EXITO,
		INGRESO_APL_AUTOMATICA_RECHAZO,
		SPV_ENVIO_REPORTE_WS,
		NOTIFICACION_SALVAMENTO_RECURRENTE,
		ERROR_GENERACION_APLICACION_DED_STORE,
		ERROR_VALIDACION_VIN,
		EDICION_EJECUTIVO_COLOCA
	}
	
	/**
	 * Requerido por JPA
	 */
	protected Bitacora(){		
	}

	/**
	 * Construye una instancia de bitacora
	 * @param tipoBitacora Catalogo agrupador del tipo de bitacora. Requerido
	 * @param evento Define la accion que se esta registrando. Requerido
	 * @param identificador Identificador de la operacion realizada, ejem. Id reporte. Requerido
	 * @param mensaje Descripcion de la operacion realizada. Requerido
	 * @param detalle Detalle de la operacion, ejem. serializacion de algun objeto. Opcional
	 * @param usuario Usuario que realizo la operacion. Requerido.
	 */
	public Bitacora(TIPO_BITACORA tipoBitacora, EVENTO evento, String identificador,
			String mensaje, String detalle, String usuario) {
		this(tipoBitacora, evento, identificador, mensaje, detalle, null, usuario);
	}
	
	/**
	 * Construye una instancia de bitacora
	 * @param tipoBitacora Catalogo agrupador del tipo de bitacora. Requerido
	 * @param evento Define la accion que se esta registrando. Requerido
	 * @param identificador Identificador de la operacion realizada, ejem. Id reporte. Requerido
	 * @param mensaje Descripcion de la operacion realizada. Requerido
	 * @param detalle Detalle de la operacion, ejem. serializacion de algun objeto. Opcional
	 * @param contenedorDetalle Nombre cannonical del tipo de objeto que se esta serializando en caso de ser requerido. Opcional
	 * @param usuario Usuario que realizo la operacion. Requerido.
	 */

	public Bitacora(TIPO_BITACORA tipoBitacora, EVENTO evento, String identificador,
			String mensaje, String detalle, String contenedorDetalle, String usuario) {
		super();
		this.tipoBitacora = tipoBitacora;
		this.evento = evento;
		this.identificador = identificador;
		this.mensaje = mensaje;
		this.detalle = detalle;
		this.contenedorDetalle = contenedorDetalle;
		this.fechaCreacion = new Date();
		this.usuario = usuario;
	}
	
	public TIPO_BITACORA getTipoBitacora() {
		return tipoBitacora;
	}

	public void setTipoBitacora(TIPO_BITACORA tipoBitacora) {
		this.tipoBitacora = tipoBitacora;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	
	public EVENTO getEvento() {
		return evento;
	}

	public void setEvento(EVENTO evento) {
		this.evento = evento;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return tipoBitacora.toString().concat("|").concat(evento.toString()).concat("|").concat(identificador);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	public String getContenedorDetalle() {
		return contenedorDetalle;
	}

	/**
	 * En caso de que se haya serializado un objeto como detalle, es recomendable definir el tipo de objeto que se serializo. 
	 * @return
	 */
	public void setContenedorDetalle(String contenedorDetalle) {
		this.contenedorDetalle = contenedorDetalle;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Bitacora)) {
			return false;
		}
		Bitacora other = (Bitacora) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	
	

}
