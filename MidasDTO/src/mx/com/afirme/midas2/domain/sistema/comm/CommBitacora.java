package mx.com.afirme.midas2.domain.sistema.comm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.sistema.comm.CommBitacoraDetalle.STATUS_COMM;

@Entity
@Table(name = "TOCOMMBITACORA", schema = "MIDAS")
public class CommBitacora implements Entidad {

	private static final long serialVersionUID = 7238652584019605171L;

	public static enum STATUS_BITACORA{
		TRAMITE, CANCELADO, ERROR, TERMINADO
	};
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOCOMMBITACORA_GENERATOR")
	@SequenceGenerator(name="TOCOMMBITACORA_GENERATOR", schema="MIDAS", sequenceName="TOCOMMBITACORA_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn( name="procesoID", referencedColumnName="id")
	private CommProceso proceso;
	
	@Column(name="FOLIO")
	private String folio;
	
	@Column(name="CVEUSUARIO")
	private String cveUsuario;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHACREACION")
	private Date fechaCreacion;
	
	@Column(name="STATUS")
	private String status;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "bitacora")
	private List<CommBitacoraDetalle> detalle = new ArrayList<CommBitacoraDetalle>();
	
	public CommBitacora(){
		super();
	}
	
	public CommBitacora(CommProceso proceso, String folio, String cveUsuario, String serializacion) {
		super();
		this.proceso = proceso;
		this.folio = folio;
		this.cveUsuario = cveUsuario;
		this.fechaCreacion = new Date();
		this.status = STATUS_BITACORA.TRAMITE.toString();
		CommBitacoraDetalle detalle = new CommBitacoraDetalle(serializacion, STATUS_COMM.PENDIENTE, cveUsuario, null);
		agregarDetalle(detalle);
	}

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CommProceso getProceso() {
		return proceso;
	}

	public void setProceso(CommProceso proceso) {
		this.proceso = proceso;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getCveUsuario() {
		return cveUsuario;
	}

	public void setCveUsuario(String cveUsuario) {
		this.cveUsuario = cveUsuario;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {	
		return id;
	}

	@Override
	public String getValue() {
		return folio;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getBusinessKey() {
		return folio;
	}

	public List<CommBitacoraDetalle> getDetalle() {
		return detalle;
	}

	public void setDetalle(List<CommBitacoraDetalle> detalle) {
		this.detalle = detalle;
	}

	public void agregarDetalle(CommBitacoraDetalle detalle){
		detalle.setBitacora(this);
		this.detalle.add(detalle);		
	}
	
	
}
