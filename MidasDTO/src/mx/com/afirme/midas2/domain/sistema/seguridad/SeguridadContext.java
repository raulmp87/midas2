package mx.com.afirme.midas2.domain.sistema.seguridad;

import mx.com.afirme.midas.sistema.seguridad.Usuario;

public class SeguridadContext {

	private static ThreadLocal<SeguridadContext> contextHolder = new ThreadLocal<SeguridadContext>();
	
	/**
	 * Obtiene el contexto asociado del Thread actual, en caso de no existir ninguno crea un contexto vacio.
	 * @return el contexto
	 */
	public static SeguridadContext getContext() {
		SeguridadContext ctx = contextHolder.get();
		if (ctx == null) {
			//Crear un contexto vacio.
			contextHolder.set(new SeguridadContext());
		}
		return contextHolder.get();
	}
	
	public static void borrarContext() {
		SeguridadContext ctx = contextHolder.get();
		if (ctx == null) {
			return;
		}
		contextHolder.remove();
	}
	
	private Usuario usuario;
	
	public Usuario getUsuario() {
		return usuario;
	}
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
