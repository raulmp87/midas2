package mx.com.afirme.midas2.domain.sistema;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(schema="SEYCOS", name="EVENTO")
public class EventoSeycos extends Evento implements Serializable {

	private static final long serialVersionUID = -1096979841196273252L;
	
	private Long id;
	
	@Id
	@SequenceGenerator(name = "SQ01_EVENTO", allocationSize = 1, schema="SEYCOS", sequenceName = "SQ01_EVENTO")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ01_EVENTO")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public EventoSeycos() {
		super();
	}
	
}
