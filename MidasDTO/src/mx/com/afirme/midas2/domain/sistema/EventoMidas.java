package mx.com.afirme.midas2.domain.sistema;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(schema="MIDAS", name="EVENTO")
public class EventoMidas extends Evento implements Serializable {

	private static final long serialVersionUID = -8741218510260160930L;
		
	private Long id;
		
	@Id
	@SequenceGenerator(name = "SQ_EVENTO", allocationSize = 1, schema="MIDAS", sequenceName = "SQ_EVENTO")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_EVENTO")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public EventoMidas() {
		super();
	}
	
}
