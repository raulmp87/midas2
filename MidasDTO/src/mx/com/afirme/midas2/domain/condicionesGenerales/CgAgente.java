package mx.com.afirme.midas2.domain.condicionesGenerales;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;

@Entity
@Table(name="tocgagente" , schema="MIDAS")
public class CgAgente implements Serializable, Entidad {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="TOAGENTE_ID")
	private Long id;
	@Column(name="CVE_PAIS")
	private String cvePais;	
	@Column(name="CVE_ESTADO")
	private String cveEstado;
	@Column(name="CVE_CIUDAD")
	private String cveCiudad;
	@Column(name="CALLE_NUMERO")
	private String calleNumero;
	@Column(name="COLONIA")
	private String colonia;
	@Column(name="CODIGO_POSTAL")
	private String codigoPostal;	
	@Column(name="PERSONA_RECIBE")
	private String personaRecibe;
	@Column(name="TELEFONO_RECIBE")
	private String telefonoRecibe;
	@Column(name="OBSERVACIONES")
	private String observaciones;
	@Column(name="INVENTARIO")
	private Long inventario;
	@Column(name="VENTAS")
	private Long ventas;
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_ULTIMO")
	private Date fechaUltimo;
	@Column(name="CANTIDAD_ULTIMO")
	private Long cantidadUltimo;	
	@Column(name="PROMEDIO_MENSUAL")
	private Long promedioMensual;
	@Column(name="FACTOR_SURTIR")
	private Long factorSurtir;
	@Column(name="PORCENTAJE_SURTIR")
	private Long porcentajeSurtir;
	@Column(name="DIAS_ENTREGA")
	private Long diasEntrega;
	@Column(name="SURTIDO_EJECUTIVO")
	private Long surtidoEjecutivo;
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;
	@Column(name="USER_ID")
	private String userId;
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_MODIFICACION")
	private Date fechaModificacion;
	@Column(name="USER_MODIF_ID")
	private String userModifId;	
	@Column(name="ESTATUS")
	private Long estatus;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TOAGENTE_ID", nullable = false, insertable = false, updatable = false)
	private Agente agente;
	
	@Transient
	private CgAgenteView cgAgenteView;
	@Transient
	private boolean resultado;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCvePais() {
		return cvePais;
	}

	public void setCvePais(String cvePais) {
		this.cvePais = cvePais;
	}
	

	public String getCveEstado() {
		return cveEstado;
	}

	public void setCveEstado(String cveEstado) {
		this.cveEstado = cveEstado;
	}

	public String getCveCiudad() {
		return cveCiudad;
	}

	public void setCveCiudad(String cveCiudad) {
		this.cveCiudad = cveCiudad;
	}

	public String getCalleNumero() {
		return calleNumero;
	}

	public void setCalleNumero(String calleNumero) {
		this.calleNumero = calleNumero;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getPersonaRecibe() {
		return personaRecibe;
	}

	public void setPersonaRecibe(String personaRecibe) {
		this.personaRecibe = personaRecibe;
	}

	public String getTelefonoRecibe() {
		return telefonoRecibe;
	}

	public void setTelefonoRecibe(String telefonoRecibe) {
		this.telefonoRecibe = telefonoRecibe;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Long getInventario() {
		return inventario;
	}

	public void setInventario(Long inventario) {
		this.inventario = inventario;
	}

	public Long getVentas() {
		return ventas;
	}

	public void setVentas(Long ventas) {
		this.ventas = ventas;
	}

	public Date getFechaUltimo() {
		return fechaUltimo;
	}

	public void setFechaUltimo(Date fechaUltimo) {
		this.fechaUltimo = fechaUltimo;
	}

	public Long getCantidadUltimo() {
		return cantidadUltimo;
	}

	public void setCantidadUltimo(Long cantidadUltimo) {
		this.cantidadUltimo = cantidadUltimo;
	}

	public Long getPromedioMensual() {
		return promedioMensual;
	}

	public void setPromedioMensual(Long promedioMensual) {
		this.promedioMensual = promedioMensual;
	}

	public Long getFactorSurtir() {
		return factorSurtir;
	}

	public void setFactorSurtir(Long factorSurtir) {
		this.factorSurtir = factorSurtir;
	}

	public Long getPorcentajeSurtir() {
		return porcentajeSurtir;
	}

	public void setPorcentajeSurtir(Long porcentajeSurtir) {
		this.porcentajeSurtir = porcentajeSurtir;
	}

	public Long getDiasEntrega() {
		return diasEntrega;
	}

	public void setDiasEntrega(Long diasEntrega) {
		this.diasEntrega = diasEntrega;
	}

	public Long getSurtidoEjecutivo() {
		return surtidoEjecutivo;
	}

	public void setSurtidoEjecutivo(Long surtidoEjecutivo) {
		this.surtidoEjecutivo = surtidoEjecutivo;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getUserModifId() {
		return userModifId;
	}

	public void setUserModifId(String userModifId) {
		this.userModifId = userModifId;
	}

	public Long getEstatus() {
		return estatus;
	}

	public void setEstatus(Long estatus) {
		this.estatus = estatus;
	}

	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	public boolean isResultado() {
		return resultado;
	}

	public void setResultado(boolean resultado) {
		this.resultado = resultado;
	}

	public CgAgenteView getCgAgenteView() {
		return cgAgenteView;
	}

	public void setCgAgenteView(CgAgenteView cgAgenteView) {
		this.cgAgenteView = cgAgenteView;
	}

	@Override
	public <K> K getKey() {
		
		return null;
	}

	@Override
	public String getValue() {
		
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		
		return null;
	}

}