package mx.com.afirme.midas2.domain.condicionesGenerales;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="tocgorden" , schema="MIDAS")
public class CgOrden implements Serializable, Entidad  {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTOCGORDEN_ID_GENERATOR")	
	@SequenceGenerator(name="IDTOCGORDEN_ID_GENERATOR", sequenceName="MIDAS.IDTOCGORDEN_SEQ", allocationSize=1)	
	@Column(name="TOCGORDEN_ID")
	private Long id;
	@Column(name="TOCGAGENTE_ID")
	private Long tocgagenteId;
	@Column(name="TOCGPROVEEDOR_ID")
	private Long tocgproveedorId;
	@Column(name="TOCGCENTRO_ID")
	private Long tocgcentroId;
	@Column(name="CANTIDAD")
	private Long cantidad;
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA")
	private Date fecha;
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_ENTREGA")
	private Date fechaEntrega;	
	@Column(name="MOTIVOS")
	private String motivos;
	@Column(name="TIPO")
	private String tipo;
	@Column(name="ALERTA")
	private Long alerta;	
	@Column(name="ESTATUS")
	private Long estatus;
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;
	@Column(name="USER_ID")
	private String userId;
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_MODIFICACION")
	private Date fechaModificacion;
	@Column(name="USER_MODIF_ID")
	private String userModifId;
	@Column(name="NOMBRECOMPLETO")
	private String nombreCompleto;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TOCGCENTRO_ID", insertable = false, updatable = false)
	private CgCentro cgCentro;
	
	@Transient
	private Date fechaInicio;
	@Transient
	private Date fechaFin;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTocgagenteId() {
		return tocgagenteId;
	}

	public void setTocgagenteId(Long tocgagenteId) {
		this.tocgagenteId = tocgagenteId;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public Long getTocgproveedorId() {
		return tocgproveedorId;
	}

	public void setTocgproveedorId(Long tocgproveedorId) {
		this.tocgproveedorId = tocgproveedorId;
	}

	public Long getTocgcentroId() {
		return tocgcentroId;
	}

	public void setTocgcentroId(Long tocgcentroId) {
		this.tocgcentroId = tocgcentroId;
	}

	public Long getCantidad() {
		return cantidad;
	}

	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Date getFechaEntrega() {
		return fechaEntrega;
	}

	public void setFechaEntrega(Date fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}

	public String getMotivos() {
		return motivos;
	}

	public void setMotivos(String motivos) {
		this.motivos = motivos;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Long getAlerta() {
		return alerta;
	}

	public void setAlerta(Long alerta) {
		this.alerta = alerta;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getUserModifId() {
		return userModifId;
	}

	public void setUserModifId(String userModifId) {
		this.userModifId = userModifId;
	}

	public Long getEstatus() {
		return estatus;
	}

	public void setEstatus(Long estatus) {
		this.estatus = estatus;
	}

	public CgCentro getCgCentro() {
		return cgCentro;
	}

	public void setCgCentro(CgCentro cgCentro) {
		this.cgCentro = cgCentro;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	@Override
	public <K> K getKey() {
		
		return null;
	}

	@Override
	public String getValue() {
		
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		
		return null;
	}

}