package mx.com.afirme.midas2.domain.condicionesGenerales;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;

/**
* Remote interface for CgCondicionesFacade.
* @author MyEclipse Persistence Tools
*/
@Remote
public interface DocumentoAnexoCgFacadeRemote {
		/**
	 Perform an initial save of a previously unsaved CgCondiciones entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CgCondiciones entity to persist
	  @throws RuntimeException when the operation fails
	 */
 public void save(CgCondiciones entity);
 /**
	 Delete a persistent CgCondiciones entity.
	  @param entity CgCondiciones entity to delete
	 @throws RuntimeException when the operation fails
	 */
 public void delete(CgCondiciones entity);
/**
	 Persist a previously saved CgCondiciones entity and return it or a copy of it to the sender. 
	 A copy of the CgCondiciones entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CgCondiciones entity to update
	 @return CgCondiciones the persisted CgCondiciones entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CgCondiciones update(CgCondiciones entity);
	public CgCondiciones findById( BigDecimal id);
	 /**
	 * Find all CgCondiciones entities with a specific property value.  
	 
	  @param propertyName the name of the CgCondiciones property to query
	  @param value the property value to match
	  	  @return List<CgCondiciones> found by query
	 */
	public List<CgCondiciones> findByProperty(String propertyName, Object value);
	/**
	 * Find all CgCondiciones entities.
	  	  @return List<CgCondiciones> all CgCondiciones entities
	 */
	public List<CgCondiciones> findAll();	
}