package mx.com.afirme.midas2.domain.condicionesGenerales;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

@Entity
@SqlResultSetMapping(name="cgAgenteView",entities={
		@EntityResult(entityClass=CgAgenteView.class,fields={
			@FieldResult(name="id",column="id"),
			@FieldResult(name="idAgente",column="idAgente"),
			@FieldResult(name="promotoria",column="promotoria"),
			@FieldResult(name="idPromotoria",column="idPromotoria"),
			@FieldResult(name="idPersona",column="idPersona"),
			@FieldResult(name="nombreCompleto",column="nombreCompleto"),
			@FieldResult(name="codigoRfc",column="codigoRfc"),
			@FieldResult(name="tipoCedulaAgente",column="tipoCedulaAgente"),
			@FieldResult(name="ejecutivo",column="ejecutivo"),
			@FieldResult(name="tipoSituacion",column="tipoSituacion"),
			@FieldResult(name="gerencia",column="gerencia"),
			@FieldResult(name="diasEntrega",column="dias_entrega")
		})
	}
)
public class CgAgenteView implements Serializable {
	
	private static final long serialVersionUID = 3732789591057174423L;
	private Long id;
	private Long idAgente;
	private String promotoria;
	private Long idPromotoria;
	private Long idPersona;	
	private String nombreCompleto;
	private String codigoRfc;
	private String tipoCedulaAgente;
	private String ejecutivo;
	private String tipoSituacion;
	private String gerencia;
	private Long diasEntrega;

	public CgAgenteView(){}

	@Id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdAgente() {
		return idAgente;
	}

	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}

	public String getPromotoria() {
		return promotoria;
	}

	public void setPromotoria(String promotoria) {
		this.promotoria = promotoria;
	}

	public Long getIdPromotoria() {
		return idPromotoria;
	}

	public void setIdPromotoria(Long idPromotoria) {
		this.idPromotoria = idPromotoria;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public String getCodigoRfc() {
		return codigoRfc;
	}

	public void setCodigoRfc(String codigoRfc) {
		this.codigoRfc = codigoRfc;
	}

	public String getTipoCedulaAgente() {
		return tipoCedulaAgente;
	}

	public void setTipoCedulaAgente(String tipoCedulaAgente) {
		this.tipoCedulaAgente = tipoCedulaAgente;
	}

	public String getEjecutivo() {
		return ejecutivo;
	}

	public void setEjecutivo(String ejecutivo) {
		this.ejecutivo = ejecutivo;
	}

	public String getTipoSituacion() {
		return tipoSituacion;
	}

	public void setTipoSituacion(String tipoSituacion) {
		this.tipoSituacion = tipoSituacion;
	}

	public String getGerencia() {
		return gerencia;
	}

	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}

	public Long getDiasEntrega() {
		return diasEntrega;
	}

	public void setDiasEntrega(Long diasEntrega) {
		this.diasEntrega = diasEntrega;
	}

}