package mx.com.afirme.midas2.domain.condicionesGenerales;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;

@Entity
@Table(name="tocgproveedor" , schema="MIDAS")
public class CgProveedor implements Serializable, Entidad  {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTOCGPROVEEDOR_ID_GENERATOR")	
	@SequenceGenerator(name="IDTOCGPROVEEDOR_ID_GENERATOR", sequenceName="MIDAS.IDTOCGPROVEEDOR_SEQ", allocationSize=1)	
	@Column(name="TOCGPROVEEDOR_ID")
	private Long id;
	@Column(name="CONTACTO")
	private String contacto;
	@Column(name="OBSERVACIONES")
	private String observaciones;	
	@Column(name="TCPRESTADORSERVICIO_ID")
	private Long tcprestadorservicioId;	
	@Column(name="CENTROS")
	private String centros;
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;
	@Column(name="USER_ID")
	private String userId;
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_MODIFICACION")
	private Date fechaModificacion;
	@Column(name="USER_MODIF_ID")
	private String userModifId;
	
	@Transient
	private String nombrePrestador;
	@Transient
	private List<CgCentro> centrosProveedor = new ArrayList<CgCentro>();
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TCPRESTADORSERVICIO_ID", nullable = false, insertable = false, updatable = false)
	private PrestadorServicio prestadorServicio;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}	

	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Long getTcprestadorservicioId() {
		return tcprestadorservicioId;
	}

	public void setTcprestadorservicioId(Long tcprestadorservicioId) {
		this.tcprestadorservicioId = tcprestadorservicioId;
	}

	public String getCentros() {
		return centros;
	}

	public void setCentros(String centros) {
		this.centros = centros;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getUserModifId() {
		return userModifId;
	}

	public void setUserModifId(String userModifId) {
		this.userModifId = userModifId;
	}

	public String getNombrePrestador() {
		return nombrePrestador;
	}

	public void setNombrePrestador(String nombrePrestador) {
		this.nombrePrestador = nombrePrestador;
	}

	public List<CgCentro> getCentrosProveedor() {
		return centrosProveedor;
	}

	public void setCentrosProveedor(List<CgCentro> centrosProveedor) {
		this.centrosProveedor = centrosProveedor;
	}

	public PrestadorServicio getPrestadorServicio() {
		return prestadorServicio;
	}

	public void setPrestadorServicio(PrestadorServicio prestadorServicio) {
		this.prestadorServicio = prestadorServicio;
	}

	@Override
	public <K> K getKey() {
		
		return null;
	}

	@Override
	public String getValue() {
		
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		
		return null;
	}

}