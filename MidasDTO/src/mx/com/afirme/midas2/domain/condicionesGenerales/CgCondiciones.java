package mx.com.afirme.midas2.domain.condicionesGenerales;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="tocgcondiciones", schema="MIDAS")
public class CgCondiciones implements Serializable, Entidad {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTOCGCONDICIONES_ID_GENERATOR")	
	@SequenceGenerator(name="IDTOCGCONDICIONES_ID_GENERATOR", sequenceName="MIDAS.IDTOCGCONDICIONES_SEQ", allocationSize=1)	
	@Column(name="TOCGCONDICIONES_ID")
	private BigDecimal id;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="IDTOSECCION")
	private SeccionDTO seccionDTO;
	
	@Column(name="NOMBRE", nullable=false)
	private String nombre;
	
	@Column(name="NUMERO_REGISTRO")
	private String numeroRegistro;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_INICIO", nullable=false)
	private Date fechaInicio;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_FIN")
	private Date fechaFin;
	
	@Column(name="ACTIVO", precision=4, scale=0)
	private Short activo;
	
	@Column(name="PORTAL", precision=4, scale=0)
	private Short portal;
	
	@Column(name="ED", precision=4, scale=0)
	private Short ed;

	@Column(name="FORTIMAXDOC_ID", nullable=false)
	private String fortimaxDocId;

	@Column(name="FORTIMAXDOC_NOMBRE", nullable=false)
	private String fortimaxDocNombre;

	@Column(name="FORTIMAXDOC_GAVETA", nullable=false)
	private String fortimaxDocGaveta;

	@Column(name="FORTIMAXDOC_CARPETA")
	private String fortimaxDocCarpeta;
		
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;
	
	@Column(name="USER_ID")
	private String userId;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_MODIFICACION")
	private Date fechaModificacion;
	
	@Column(name="USER_MODIF_ID")
	private String userModifId;	
	
	
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public SeccionDTO getSeccionDTO() {
		return seccionDTO;
	}
	public void setSeccionDTO(SeccionDTO seccionDTO) {
		this.seccionDTO = seccionDTO;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNumeroRegistro() {
		return numeroRegistro;
	}

	public void setNumeroRegistro(String numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Short getActivo() {
		return activo;
	}

	public void setActivo(Short activo) {
		this.activo = activo;
	}
	
	public Short getPortal() {
		return portal;
	}

	public void setPortal(Short portal) {
		this.portal = portal;
	}
	
	public Short getEd() {
		return ed;
	}

	public void setEd(Short ed) {
		this.ed = ed;
	}
	
	public void setFortimaxDocId(String fortimaxDocId) {
		this.fortimaxDocId = fortimaxDocId;
	}

	public String getFortimaxDocId() {
		return fortimaxDocId;
	}

	public void setFortimaxDocNombre(String fortimaxDocNombre) {
		this.fortimaxDocNombre = fortimaxDocNombre;
	}

	public String getFortimaxDocNombre() {
		return fortimaxDocNombre;
	}

	public void setFortimaxDocGaveta(String fortimaxDocGaveta) {
		this.fortimaxDocGaveta = fortimaxDocGaveta;
	}

	public String getFortimaxDocGaveta() {
		return fortimaxDocGaveta;
	}

	public void setFortimaxDocCarpeta(String fortimaxDocCarpeta) {
		this.fortimaxDocCarpeta = fortimaxDocCarpeta;
	}

	public String getFortimaxDocCarpeta() {
		return fortimaxDocCarpeta;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getUserModifId() {
		return userModifId;
	}

	public void setUserModifId(String userModifId) {
		this.userModifId = userModifId;
	}

	@Override
	public <K> K getKey() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public String getValue() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

}
