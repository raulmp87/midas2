package mx.com.afirme.midas2.domain.catalogos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

import org.eclipse.persistence.annotations.ReadOnly;

@Entity
@Table(name="TCVALORFIJO",schema="MIDAS")
@ReadOnly
public class CatValorFijo implements Entidad{

	private static final long serialVersionUID = -1700680820923178843L;

    @Id
    @Column(name="id")
   	private Integer id;
	
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn( name="GRUPO_ID", referencedColumnName="id", insertable=false, updatable=false)
	private CatGrupoFijo grupo;
	
    @Column(name="CODIGO")
	private String codigo;
	
    @Column(name="DESCRIPCION")
	private String descripcion;
	
    @Column(name="ACTIVO")
	private Boolean activo = Boolean.TRUE;
	
    @Column(name="CODIGO_PADRE")
	private String codigoPadre;
    
    @Column(name="ORDEN")
    private String orden;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public CatGrupoFijo getGrupo() {
		return grupo;
	}

	public void setGrupo(CatGrupoFijo grupo) {
		this.grupo = grupo;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getCodigoPadre() {
		return codigoPadre;
	}

	public void setCodigoPadre(String codigoPadre) {
		this.codigoPadre = codigoPadre;
	}
	
	public String getOrden() {
		return orden;
	}

	public void setOrden(String orden) {
		this.orden = orden;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof CatValorFijo)) {
			return false;
		}
		CatValorFijo other = (CatValorFijo) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Integer getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Integer getBusinessKey() {
		return id;
	}

}
