package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.eclipse.persistence.annotations.Customizer;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

@Entity(name="EntretenimientoAgente")
@Table(name="toEntretenimientoAgente",schema="MIDAS")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class EntretenimientoAgente implements Serializable,Entidad{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Agente agente;
	private	String comentarios;
	//private Long idTipoEntretenimiento;
	private ValorCatalogoAgentes tipoEntretenimiento;
	public EntretenimientoAgente(){}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idToEntretenimientoAgente_seq")
	@SequenceGenerator(name="idToEntretenimientoAgente_seq", sequenceName="MIDAS.idToEntretenimientoAgente_seq",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="AGENTE_ID",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Agente getAgente() {
		return agente;
	}
	public void setAgente(Agente agente) {
		this.agente = agente;
	}
	
	@Column(name="COMENTARIOS",nullable=true,length=200)
	@Size(min=0,max=200)
	public String getComentarios() {
		return comentarios;
	}
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}
	
//	@Column(name="IDTIPOENTRETENIMIENTO",nullable=false)
//	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
//	public Long getIdTipoEntretenimiento() {
//		return idTipoEntretenimiento;
//	}
//	public void setIdTipoEntretenimiento(Long idTipoEntretenimiento) {
//		this.idTipoEntretenimiento = idTipoEntretenimiento;
//	}

	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return comentarios;
	}

	@Override
	public Long getBusinessKey() {
		return id;
	}

	public void setTipoEntretenimiento(ValorCatalogoAgentes tipoEntretenimiento) {
		this.tipoEntretenimiento = tipoEntretenimiento;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="IDTIPOENTRETENIMIENTO",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ValorCatalogoAgentes getTipoEntretenimiento() {
		return tipoEntretenimiento;
	}
}
