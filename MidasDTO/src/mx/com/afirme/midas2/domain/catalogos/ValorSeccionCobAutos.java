package mx.com.afirme.midas2.domain.catalogos;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.EditItemChecks;


/**
 * ValorSeccionCobAutos entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TOVALORSECCIONCOBAUTOS"
    ,schema="MIDAS"
)

public class ValorSeccionCobAutos  implements Serializable, Entidad {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1201250118784941624L;
	private ValorSeccionCobAutosId id;
     private Short claveTipoLimiteSumaAseg;
     private Double valorSumaAseguradaMin;
     private Double valorSumaAseguradaMax;
     private Double valorSumaAseguradaDefault;
     private CoberturaSeccionDTO coberturaSeccionDTO;
     private TipoUsoVehiculoDTO tipoUsoVehiculoDTO;
     private MonedaDTO monedaDTO;
     
     private String descripcionTipoSumaAsegurada;
    // Constructors

    /** default constructor */
    public ValorSeccionCobAutos() {
    }

    
    /** full constructor */
    public ValorSeccionCobAutos(ValorSeccionCobAutosId id, Short claveTipoLimiteSumaAseg, Double valorSumaAseguradaMin, Double valorSumaAseguradaMax, Double valorSumaAseguradaDefault) {
        this.id = id;
        this.claveTipoLimiteSumaAseg = claveTipoLimiteSumaAseg;
        this.valorSumaAseguradaMin = valorSumaAseguradaMin;
        this.valorSumaAseguradaMax = valorSumaAseguradaMax;
        this.valorSumaAseguradaDefault = valorSumaAseguradaDefault;
    }

   
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idToSeccion", column=@Column(name="IDTOSECCION", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idToCobertura", column=@Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idMoneda", column=@Column(name="IDMONEDA", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idTcTipoUsoVehiculo", column=@Column(name="IDTCTIPOUSOVEHICULO", nullable=false, precision=22, scale=0) ) } )

    public ValorSeccionCobAutosId getId() {
        return this.id;
    }
    
    public void setId(ValorSeccionCobAutosId id) {
        this.id = id;
    }
    
    @Column(name="CLAVETIPOLIMITESUMAASEG", nullable=false, precision=4, scale=0)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
    public Short getClaveTipoLimiteSumaAseg() {
        return this.claveTipoLimiteSumaAseg;
    }
    
    public void setClaveTipoLimiteSumaAseg(Short claveTipoLimiteSumaAseg) {
        this.claveTipoLimiteSumaAseg = claveTipoLimiteSumaAseg;
    }
    
    @Column(name="VALORSUMAASEGURADAMIN", nullable=false, precision=16)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer = 16, fraction= 2,groups=EditItemChecks.class,message="{javax.validation.constraints.Digits.message}")
    public Double getValorSumaAseguradaMin() {
        return this.valorSumaAseguradaMin;
    }
    
    public void setValorSumaAseguradaMin(Double valorSumaAseguradaMin) {
        this.valorSumaAseguradaMin = valorSumaAseguradaMin;
    }
    
    @Column(name="VALORSUMAASEGURADAMAX", nullable=false, precision=16)
    @NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
    @Digits(integer = 16, fraction= 2,groups=EditItemChecks.class,message="{javax.validation.constraints.Digits.message}")
    public Double getValorSumaAseguradaMax() {
        return this.valorSumaAseguradaMax;
    }
    
    public void setValorSumaAseguradaMax(Double valorSumaAseguradaMax) {
        this.valorSumaAseguradaMax = valorSumaAseguradaMax;
    }
    
    @Column(name="VALORSUMAASEGURADADEFAULT", nullable=false, precision=16)
    @NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
    @Digits(integer = 16, fraction= 2,groups=EditItemChecks.class,message="{javax.validation.constraints.Digits.message}")
    public Double getValorSumaAseguradaDefault() {
        return this.valorSumaAseguradaDefault;
    }
    
    public void setValorSumaAseguradaDefault(Double valorSumaAseguradaDefault) {
        this.valorSumaAseguradaDefault = valorSumaAseguradaDefault;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumns({@JoinColumn(name = "IDTOSECCION", referencedColumnName="IDTOSECCION", nullable = false, insertable = false, updatable = false),
    	@JoinColumn(name = "IDTOCOBERTURA", referencedColumnName="IDTOCOBERTURA", nullable = false, insertable = false, updatable = false)})
	public CoberturaSeccionDTO getCoberturaSeccionDTO() {
		return coberturaSeccionDTO;
	}


	public void setCoberturaSeccionDTO(CoberturaSeccionDTO coberturaSeccionDTO) {
		this.coberturaSeccionDTO = coberturaSeccionDTO;
	}

	@Transient
	public TipoUsoVehiculoDTO getTipoUsoVehiculoDTO() {
		return tipoUsoVehiculoDTO;
	}


	public void setTipoUsoVehiculoDTO(TipoUsoVehiculoDTO tipoUsoVehiculoDTO) {
		this.tipoUsoVehiculoDTO = tipoUsoVehiculoDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDMONEDA", referencedColumnName="IDTCMONEDA", nullable = false, insertable = false, updatable = false)
	public MonedaDTO getMonedaDTO() {
		return monedaDTO;
	}


	public void setMonedaDTO(MonedaDTO monedaDTO) {
		this.monedaDTO = monedaDTO;
	}


	@SuppressWarnings("unchecked")
	@Override
	public ValorSeccionCobAutosId getKey() {
		return this.id;
	}


	@Override
	public String getValue() {
		return null;
	}


	@SuppressWarnings("unchecked")
	@Override
	public String getBusinessKey() {
		return this.id.getIdTcTipoUsoVehiculo()+"_"+this.id.getIdMoneda();
	}


	public void setDescripcionTipoSumaAsegurada(
			String descripcionTipoSumaAsegurada) {
		this.descripcionTipoSumaAsegurada = descripcionTipoSumaAsegurada;
	}


	@Transient
	public String getDescripcionTipoSumaAsegurada() {
		return descripcionTipoSumaAsegurada;
	}
   


}