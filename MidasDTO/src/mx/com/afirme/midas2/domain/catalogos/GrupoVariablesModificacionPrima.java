package mx.com.afirme.midas2.domain.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
import mx.com.afirme.midas2.validator.group.EditVarModifPrimaChecks;
import mx.com.afirme.midas2.validator.group.NewItemChecks;
import mx.com.afirme.midas2.validator.group.TarifaAutoModifChecks;

@Entity
@Table(name = "TCGRUPOVARMODIFPRIMA", schema = "MIDAS")
public class GrupoVariablesModificacionPrima extends CacheableDTO implements Serializable {

	private static final long serialVersionUID = 3559980581599521412L;
	
	private Long id;
	
	private Integer clave;
	
	private String descripcion;

	private Short claveTipoDetalle;
	
	private String descripcionDetalle;

	private List<CoberturaSeccionDTO> coberturaSeccionList = new ArrayList<CoberturaSeccionDTO>();
	

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTCGRUPOVARMODIFPRIMA_SEQ")
	@SequenceGenerator(name="IDTCGRUPOVARMODIFPRIMA_SEQ", sequenceName="MIDAS.IDTCGRUPOVARMODIFPRIMA_SEQ", allocationSize=1)
	@Column(name = "IDTCGRUPOVARMODIFPRIMA", nullable = false, precision = 22, scale = 0)
	@NotNull(groups={TarifaAutoModifChecks.class,EditVarModifPrimaChecks.class}, message="{com.afirme.midas2.requerido}")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "IDGRUPO", nullable = false)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer = 5, fraction= 0,groups=NewItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public Integer getClave() {
		return clave;
	}

	public void setClave(Integer clave) {
		this.clave = clave;
	}

	@Column(name = "DESCRIPCIONGRUPO", nullable = false, length = 80)
    @NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=1, max=100, groups=EditItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getDescripcion() {
		return descripcion!=null?this.descripcion.toUpperCase():this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "CLAVETIPODETALLE", nullable = false)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")	
	@Digits(integer = 5, fraction= 0,groups=NewItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public Short getClaveTipoDetalle() {
		return claveTipoDetalle;
	}

	public void setClaveTipoDetalle(Short claveTipoDetalle) {
	this.claveTipoDetalle = claveTipoDetalle;
	}
	
	public void setDescripcionDetalle(String descripcionDetalle) {
		this.descripcionDetalle = descripcionDetalle;
	}

	@Column(name = "DESCRIPCIONDETALLE", nullable = true, length = 20)
	@Size(min=1, max=20, groups=EditItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getDescripcionDetalle() {
		return descripcionDetalle!=null?this.descripcionDetalle.toUpperCase():this.descripcionDetalle;
	}
	
	@ManyToMany(mappedBy="gruposVariablesModificacionPrimas")
	public List<CoberturaSeccionDTO> getCoberturaSeccionList() {
		return coberturaSeccionList;
	}

	public void setCoberturaSeccionList(List<CoberturaSeccionDTO> coberturaSeccionList) {
		this.coberturaSeccionList = coberturaSeccionList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clave == null) ? 0 : clave.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GrupoVariablesModificacionPrima other = (GrupoVariablesModificacionPrima) obj;
		if (clave == null) {
			if (other.clave != null)
				return false;
		} else if (!clave.equals(other.clave))
			return false;
		return true;
	}

	public GrupoVariablesModificacionPrima() {
		
	}

	@Override
	public String getDescription() {
		return getDescripcion();
	}
	
	

}
