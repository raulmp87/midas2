package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.DomicilioSeycos;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

import org.directwebremoting.annotations.DataTransferObject;
import org.eclipse.persistence.annotations.Customizer;


/**
 * The persistent class for the TOEJECUTIVO database table.
 * 
 */
@Entity(name="Ejecutivo")
@Table(name="TOEJECUTIVO",schema="MIDAS")

@SqlResultSetMapping(name="ejecutivoView", entities={
	@EntityResult(entityClass=Ejecutivo.class,fields={
		@FieldResult(name="id",column="id"),
		@FieldResult(name="idEjecutivo",column="idEjecutivo"),
		@FieldResult(name="gerencia.id",column="gerencia_id"),
		@FieldResult(name="gerencia.descripcion",column="descripcion"),
		@FieldResult(name="personaResponsable.idPersona",column="idPersona"),
		@FieldResult(name="personaResponsable.nombreCompleto",column="nombreCompleto"),
		@FieldResult(name="tipoEjecutivo.id",column="idTipoEjecutivo"),
		@FieldResult(name="tipoEjecutivo.valor",column="tipoEjecutivo"),
		@FieldResult(name="claveEstatus",column="claveEstatus")		
	})
})

@DataTransferObject
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizer.class)
public class Ejecutivo implements Serializable,Entidad {
	private static final long serialVersionUID = 1L;
	private Long id;
	private Long claveEstatus;
	private Long idEjecutivo;
	private Gerencia gerencia;
	private Domicilio domicilio;
	private Persona personaResponsable;
	private ValorCatalogoAgentes tipoEjecutivo;
	private Date fechaInicio;
	private Date fechaFin;
	private DomicilioSeycos domicilioHistorico; 
	
	public Ejecutivo() {
    }
	
	public Ejecutivo(Long id) {
		this.id = id;
	}


	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTOEJECUTIVO_SEQ")
	@SequenceGenerator(name="IDTOEJECUTIVO_SEQ", sequenceName="MIDAS.IDTOEJECUTIVO_SEQ",allocationSize=1)
	@Column(name="ID",nullable=false,unique=true)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	//bi-directional many-to-one association to Gerencia
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="GERENCIA_ID")
    @NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Gerencia getGerencia() {
		return this.gerencia;
	}

	public void setGerencia(Gerencia gerencia) {
		this.gerencia = gerencia;
	}

	@Column(name="CLAVEESTATUS",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getClaveEstatus() {
		return claveEstatus;
	}


	public void setClaveEstatus(Long claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	@Column(name="IDEJECUTIVO",nullable=false)
	public Long getIdEjecutivo() {
		return idEjecutivo;
	}


	public void setIdEjecutivo(Long idEjecutivo) {
		this.idEjecutivo = idEjecutivo;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="IDTIPOEJECUTIVO")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ValorCatalogoAgentes getTipoEjecutivo() {
		return tipoEjecutivo;
	}


	public void setTipoEjecutivo(ValorCatalogoAgentes tipoEjecutivo) {
		this.tipoEjecutivo = tipoEjecutivo;
	}

	@OneToOne(fetch=FetchType.EAGER)
	//@JoinColumn(name="IDDOMICILIO")
	@JoinColumns({
		@JoinColumn(name="IDDOMICILIO",referencedColumnName="IDDOMICILIO"),
		@JoinColumn(name="IDPERSONA",referencedColumnName="IDPERSONA",insertable=false,updatable=false)
	})
	public Domicilio getDomicilio() {
		return domicilio;
	}


	public void setDomicilio(Domicilio domicilio) {
		this.domicilio =domicilio;
	}
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="IDPERSONA",referencedColumnName="IDPERSONA",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")	
	public Persona getPersonaResponsable() {
		return personaResponsable;
	}

	public void setPersonaResponsable(Persona personaResponsable) {
		this.personaResponsable = personaResponsable;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}


	@Override
	public String getValue() {
		return null;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	@Transient
	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	@Transient
	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name="IDDOMICILIO",referencedColumnName="ID_DOMICILIO", insertable=false, updatable= false),
		@JoinColumn(name="IDPERSONA",referencedColumnName="ID_PERSONA",insertable=false,updatable=false)
	})
	public DomicilioSeycos getDomicilioHistorico() {
		return domicilioHistorico;
	}

	public void setDomicilioHistorico(DomicilioSeycos domicilioHistorico) {
		this.domicilioHistorico = domicilioHistorico;
	}	
}