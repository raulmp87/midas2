package mx.com.afirme.midas2.domain.catalogos.reaseguradorcnsf;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name="ReaseguradorCnsf")
@Table(name="CNSFREASEGURADOR",schema="MIDAS")
public class ReaseguradorCnsf implements Serializable, Entidad{

	private static final long serialVersionUID = 1L;
	
	private BigDecimal idCnsfReasegurador;
	private String nombreReasegurador;
	private String claveCnsf;
	private String claveCnsfAnterior;
	private BigDecimal estatus;
	private String usuario;
	private BigDecimal idTcReasegurador;
	
	@Id
	@SequenceGenerator(name = "CNSF_REASEGURADOR_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.CNSFREASEGURADOR_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CNSF_REASEGURADOR_SEQ_GENERADOR")
	@Column(name="ID")
	public BigDecimal getIdCnsfReasegurador() {
		return idCnsfReasegurador;
	}

	public void setIdCnsfReasegurador(BigDecimal idCnsfReasegurador) {
		this.idCnsfReasegurador = idCnsfReasegurador;
	}
	
	@Column(name="NOMBREREASEGURADOR")
	public String getNombreReasegurador() {
		return nombreReasegurador;
	}

	public void setNombreReasegurador(String nombreReasegurador) {
		this.nombreReasegurador = nombreReasegurador;
	}
		
	@Column(name="CLAVECNSF")
	public String getClaveCnsf() {
		return claveCnsf;
	}

	public void setClaveCnsf(String claveCnsf) {
		this.claveCnsf = claveCnsf;
	}
	
	@Column(name="CLAVECNSFANTERIOR")
	public String getClaveCnsfAnterior() {
		return claveCnsfAnterior;
	}

	public void setClaveCnsfAnterior(String claveCnsfAnterior) {
		this.claveCnsfAnterior = claveCnsfAnterior;
	}

	@Column(name="ESTATUS")
	public BigDecimal getEstatus() {
		return estatus;
	}

	public void setEstatus(BigDecimal estatus) {
		this.estatus = estatus;
	}
	
	@Column(name="USUARIO")
	public String getUsuario() {
		return usuario;
	}
	
	@Column(name="IDTCREASEGURADORCORREDOR")
	public BigDecimal getIdTcReasegurador() {
		return idTcReasegurador;
	}

	public void setIdTcReasegurador(BigDecimal idTcReasegurador) {
		this.idTcReasegurador = idTcReasegurador;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.idCnsfReasegurador;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}

}
