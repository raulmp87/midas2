package mx.com.afirme.midas2.domain.catalogos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.dto.DynamicControl;
import mx.com.afirme.midas2.dto.DynamicControl.TipoControl;
import mx.com.afirme.midas2.dto.DynamicControl.TipoValidacion;
import mx.com.afirme.midas2.dto.IdDynamicRow;
import mx.com.afirme.midas2.util.UtileriasWeb;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
import mx.com.afirme.midas2.validator.group.EditVarModifPrimaChecks;
import mx.com.afirme.midas2.validator.group.NewItemChecks;
import mx.com.afirme.midas2.validator.group.NewVarModifPrimaChecks;
import mx.com.afirme.midas2.validator.group.TarifaAutoModifChecks;


@Entity
@Table(name = "TCVARMODIFPRIMA", schema = "MIDAS")
public class VarModifPrima implements Serializable {

	private static final long serialVersionUID = 3559980581599521412L;
	
	private Long id;
	
	private GrupoVariablesModificacionPrima grupo;
	
	private Integer numeroSecuencia;
	
	private String rangoMinimo;
	

    String rangoMaximo;
	
	private String valor;
	@DynamicControl(atributoMapeo="id", tipoControl = TipoControl.HIDDEN,etiqueta="Clave",editable=false,esNumerico=true,secuencia="1")
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTCVARMODIFPRIMA_SEQ")
	@SequenceGenerator(name="IDTCVARMODIFPRIMA_SEQ", sequenceName="MIDAS.IDTCVARMODIFPRIMA_SEQ", allocationSize=1)
	@Column(name = "IDTCVARMODIFPRIMA", nullable = false, precision = 22, scale = 0)
	@NotNull(groups=TarifaAutoModifChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCGRUPOVARMODIFPRIMA", nullable = false, insertable = true, updatable = true)
	@Valid
	public GrupoVariablesModificacionPrima getGrupo() {
		return grupo;
	}

	public void setGrupo(GrupoVariablesModificacionPrima grupo) {
		this.grupo = grupo;
	}
	
	@DynamicControl(atributoMapeo="grupo.id", tipoControl = TipoControl.SELECT,etiqueta="Grupo",esComponenteId=false,editable=true,esNumerico=true,secuencia="2",className="mx.com.afirme.midas2.dao.catalogos.GrupoVariablesModificacionPrimaDao",javaScriptOnChange="habilitarCamposPorGrupo(this)")
	@Transient
	public Long getIdGrupo(){
		return (this.grupo!= null)?this.grupo.getId():null;
	}
	
	@DynamicControl(atributoMapeo="numeroSecuencia", tipoControl = TipoControl.TEXTBOX,etiqueta="N\u00FAmero de secuencia",esComponenteId=false,editable=true,
			esNumerico=true,secuencia="3",longitudMaxima ="5", tipoValidacion = TipoValidacion.NUM_ENTERO)
	@Column(name = "NUMEROSECUENCIA", nullable = false)
	@NotNull(groups=NewVarModifPrimaChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer = 5, fraction= 0,groups=EditVarModifPrimaChecks.class,message="{javax.validation.constraints.Digits.message}")
	public Integer getNumeroSecuencia() {
		return numeroSecuencia;
	}

	public void setNumeroSecuencia(Integer numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}

	@DynamicControl(atributoMapeo="rangoMinimo", tipoControl = TipoControl.TEXTBOX,etiqueta="Rango M\u00EDnimo",esComponenteId=false,editable=true,
			esNumerico=true,secuencia="4",longitudMaxima ="6", tipoValidacion = TipoValidacion.ALFANUMERICO)
	@Column(name = "RANGOMINIMO", nullable = true, length = 6)
	@NotNull(groups=EditVarModifPrimaChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=1, max=6, groups=EditVarModifPrimaChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getRangoMinimo() {
		return rangoMinimo!=null?this.rangoMinimo.toUpperCase():this.rangoMinimo;
	}

	public void setRangoMinimo(String rangoMinimo) {
		this.rangoMinimo = rangoMinimo;
	}
	
	@DynamicControl(atributoMapeo="rangoMaximo", tipoControl = TipoControl.TEXTBOX,etiqueta="Rango M\u00E1ximo",esComponenteId=false,editable=true,
			esNumerico=true,secuencia="5",longitudMaxima ="6", tipoValidacion = TipoValidacion.ALFANUMERICO)
	@Column(name = "RANGOMAXIMO", nullable = true, length = 6)
	@NotNull(groups=EditVarModifPrimaChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=1, max=6, groups=EditVarModifPrimaChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getRangoMaximo() {
		return rangoMaximo!=null?this.rangoMaximo.toUpperCase():this.rangoMaximo;
	}

	public void setRangoMaximo(String rangoMaximo) {
		this.rangoMaximo = rangoMaximo;
	}
	
	@DynamicControl(atributoMapeo="valor", tipoControl = TipoControl.TEXTBOX,etiqueta="Valor",esComponenteId=false,editable=true,esNumerico=true,
			secuencia="6",longitudMaxima ="17", tipoValidacion = TipoValidacion.ALFANUMERICO)
	@Column(name = "VALOR", nullable = true, length = 6)
	@NotNull(groups=EditVarModifPrimaChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=1, max=6, groups=EditVarModifPrimaChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getValor() {
		return valor!=null?this.valor.toUpperCase():this.valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((grupo == null) ? 0 : grupo.hashCode());
		result = prime * result
				+ ((numeroSecuencia == null) ? 0 : numeroSecuencia.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VarModifPrima other = (VarModifPrima) obj;
		if (grupo == null) {
			if (other.grupo != null)
				return false;
		} else if (!grupo.equals(other.grupo))
			return false;
		if (numeroSecuencia == null) {
			if (other.numeroSecuencia != null)
				return false;
		} else if (!numeroSecuencia.equals(other.numeroSecuencia))
			return false;
		return true;
	}

	
	public VarModifPrima() {
		
	}
	/**
	 * Constructor que recibe la clave VMP_1, y obtiene el id=1
	 * @param clave
	 */
	public VarModifPrima(String clave){
		if(clave!=null && !clave.isEmpty() && clave.startsWith("VMP_")){
			String[] parts=clave.split(UtileriasWeb.SEPARADOR);
			//Si tiene 2 elementos
			if(parts!=null && parts.length==2){
				String idPart=parts[1];
				this.id=Long.parseLong(idPart);
			}
		}
	}

	public VarModifPrima( Long id,
			GrupoVariablesModificacionPrima grupo,
			Integer numeroSecuencia,
			String rangoMinimo,
			String rangoMaximo,
			String valor) {
	
		
	this.id=id;
	this.grupo=grupo;
	this.numeroSecuencia=numeroSecuencia;
	this.rangoMinimo=rangoMinimo;
	this.rangoMaximo=rangoMaximo;
		
	}
	/**
	 * Se agrega esta propiedad para obtener la clave de cada registro Dinamico
	 * @return
	 */
	@IdDynamicRow
	public String getIdDynamicControl(){
		return "VMP_"+getId();
	}
	
	@DynamicControl(atributoMapeo="grupo.claveTipoDetalle", tipoControl = TipoControl.HIDDEN,etiqueta="Clave tipo Detalle",esComponenteId=false,editable=false,esNumerico=true,secuencia="7")
	@Transient
	public Short getClaveTipoDetalle(){
		return (this.grupo!= null)?this.grupo.getClaveTipoDetalle():null;
	}
	
}
