package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.dao.catalogos.Entidad;



@Entity(name="EnvioFacturaAgenteDet")
@Table(name="TOENVIOXMLAGENTESDET", schema="MIDAS")
public class EnvioFacturaAgenteDet implements Serializable, Entidad {
	
	private Long idEnvioDet;
	private ValorCatalogoAgentes tipoMsj;
	private String tipoMensaje;
	private String mensaje;
	private EnvioFacturaAgente envioFactura;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDENVIOXMLAGEDET_SEQ")
	@SequenceGenerator(name="IDENVIOXMLAGEDET_SEQ", schema="MIDAS", sequenceName="IDENVIOXMLAGEDET_SEQ", allocationSize=1)
	@Column(name="IDENVIODET", nullable=false)
	public Long getIdEnvioDet() {
		return idEnvioDet;
	}
	
	public void setIdEnvioDet(Long idEnvioDet) {
		this.idEnvioDet = idEnvioDet;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDENVIO")
	public EnvioFacturaAgente getEnvioFactura() {
		return envioFactura;
	}

	public void setEnvioFactura(EnvioFacturaAgente envioFactura) {
		this.envioFactura = envioFactura;
	}
		
	@Transient
	public String getTipoMensaje() {
		return tipoMensaje;
	}

	public void setTipoMensaje(String tipoMensaje) {
		this.tipoMensaje = tipoMensaje;
	}

	@Column(name="MENSAJE")
	@Size(min=0, max=200)
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="TIPOMSJ")
	public ValorCatalogoAgentes getTipoMsj() {
		return tipoMsj;
	}

	public void setTipoMsj(ValorCatalogoAgentes tipoMsj) {
		this.tipoMsj = tipoMsj;
	}
	
	@Override
	public <K> K getKey() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// Auto-generated method stub
		return null;
	}
}
