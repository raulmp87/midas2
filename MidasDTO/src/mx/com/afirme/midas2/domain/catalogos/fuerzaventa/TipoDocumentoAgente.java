package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

@Entity(name="TipoDocumentoAgente")
@Table(name="tcTipoDocumentoAgente",schema="MIDAS")
public class TipoDocumentoAgente implements Serializable,Entidad{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String idString;
	private	Integer	claveAplicaPersonaFisica;
	private String claveAplicaPersonaFisicaString;
	private	Integer	claveAplicaPersonaMoral;
	private	String	claveAplicaPersonaMoralString;
	private	String	descripcion;
	private ValorCatalogoAgentes idTipoSeccion;
	
	public TipoDocumentoAgente(){}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idTcTipoDocumentoAgente_seq")
	@SequenceGenerator(name="idTcTipoDocumentoAgente_seq", sequenceName="MIDAS.idTcTipoDocumentoAgente_seq")
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="CLAVEAPLICAPERSONAFISICA",nullable=false,precision=4)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Integer getClaveAplicaPersonaFisica() {
		return claveAplicaPersonaFisica;
	}
	public void setClaveAplicaPersonaFisica(Integer claveAplicaPersonaFisica) {
		this.claveAplicaPersonaFisica = claveAplicaPersonaFisica;
	}
	
	@Column(name="CLAVEAPLICAPERSONAMORAL",nullable=false,precision=4)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Integer getClaveAplicaPersonaMoral() {
		return claveAplicaPersonaMoral;
	}
	public void setClaveAplicaPersonaMoral(Integer claveAplicaPersonaMoral) {
		this.claveAplicaPersonaMoral = claveAplicaPersonaMoral;
	}
	
	@Column(name="DESCRIPCION",nullable=false,length=200)
	@Size(min=1,max=200)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}
	@Override
	public String getValue() {
		return descripcion;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	@Transient
	public String getIdString() {
		return idString;
	}

	public void setIdString(String idString) {
		this.idString=idString;
		this.id = Long.valueOf(id);
	}

	@Transient
	public String getClaveAplicaPersonaFisicaString() {
		return claveAplicaPersonaFisicaString;
	}

	public void setClaveAplicaPersonaFisicaString(
			String claveAplicaPersonaFisicaString) {
		this.claveAplicaPersonaFisicaString=claveAplicaPersonaFisicaString;
		this.claveAplicaPersonaFisica = Integer.valueOf(claveAplicaPersonaFisicaString);
	}

	@Transient
	public String getClaveAplicaPersonaMoralString() {
		return claveAplicaPersonaMoralString;
	}

	public void setClaveAplicaPersonaMoralString(
			String claveAplicaPersonaMoralString) {
		this.claveAplicaPersonaMoralString=claveAplicaPersonaMoralString;
		this.claveAplicaPersonaMoral = Integer.parseInt(claveAplicaPersonaMoralString);
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="idTipoSeccion") 
	public ValorCatalogoAgentes getIdTipoSeccion() {
		return idTipoSeccion;
	}

	public void setIdTipoSeccion(ValorCatalogoAgentes idTipoSeccion) {
		this.idTipoSeccion = idTipoSeccion;
	}
	
}
