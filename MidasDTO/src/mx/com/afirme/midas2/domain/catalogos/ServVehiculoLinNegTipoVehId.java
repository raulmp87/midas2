package mx.com.afirme.midas2.domain.catalogos;

import java.io.Serializable;


import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.validator.group.EditItemChecks;

/**
 * ServVehiculoLinNegTipoVehId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class ServVehiculoLinNegTipoVehId implements Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -7075398951760970070L;
	private Long idToSeccion;
	private Long idTcTipoVehiculo;

	// Constructors

	/** default constructor */
	public ServVehiculoLinNegTipoVehId() {
	}

	/** full constructor */
	public ServVehiculoLinNegTipoVehId(Long idToSeccion,
			Long idTcTipoVehiculo) {
		this.idToSeccion = idToSeccion;
		this.idTcTipoVehiculo = idTcTipoVehiculo;
	}

	// Property accessors

	@Column(name = "idToSeccion", nullable = false, precision = 22, scale = 0)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getIdToSeccion() {
		return this.idToSeccion;
	}

	public void setIdToSeccion(Long idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	@Column(name = "idTcTipoVehiculo", nullable = false, precision = 22, scale = 0)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getIdTcTipoVehiculo() {
		return this.idTcTipoVehiculo;
	}

	public void setIdTcTipoVehiculo(Long idTcTipoVehiculo) {
		this.idTcTipoVehiculo = idTcTipoVehiculo;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ServVehiculoLinNegTipoVehId))
			return false;
		ServVehiculoLinNegTipoVehId castOther = (ServVehiculoLinNegTipoVehId) other;

		return ((this.getIdToSeccion() == castOther.getIdToSeccion()) || (this
				.getIdToSeccion() != null
				&& castOther.getIdToSeccion() != null && this.getIdToSeccion()
				.equals(castOther.getIdToSeccion())))
				&& ((this.getIdTcTipoVehiculo() == castOther
						.getIdTcTipoVehiculo()) || (this.getIdTcTipoVehiculo() != null
						&& castOther.getIdTcTipoVehiculo() != null && this
						.getIdTcTipoVehiculo().equals(
								castOther.getIdTcTipoVehiculo())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getIdToSeccion() == null ? 0 : this.getIdToSeccion()
						.hashCode());
		result = 37
				* result
				+ (getIdTcTipoVehiculo() == null ? 0 : this
						.getIdTcTipoVehiculo().hashCode());
		return result;
	}

}