package mx.com.afirme.midas2.domain.catalogos;



import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas.catalogos.tiposerviciovehiculo.TipoServicioVehiculoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

/**
 * ServVehiculoLinNegTipoVeh entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOServVehiculoLinNegTipoVeh", schema = "MIDAS")
public class ServVehiculoLinNegTipoVeh implements Serializable, Entidad {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -3485702213890163536L;
	private ServVehiculoLinNegTipoVehId id = new ServVehiculoLinNegTipoVehId();
	private Long idTcTipoServicioVehiculo;
	private TipoServicioVehiculoDTO tipoServicioVehiculoDTO;
	private SeccionDTO seccionDTO;
	
	private String descripcionTipoVehiculo;
	private String descripcionTipoServicioVehiculo;

	// Constructors

	/** default constructor */
	public ServVehiculoLinNegTipoVeh() {
	}

	/** full constructor */
	public ServVehiculoLinNegTipoVeh(ServVehiculoLinNegTipoVehId id,
			Long idTcTipoServicioVehiculo) {
		this.id = id;
		this.idTcTipoServicioVehiculo = idTcTipoServicioVehiculo;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idToSeccion", column = @Column(name = "IDTOSECCION", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idTcTipoVehiculo", column = @Column(name = "IDTCTIPOVEHICULO", nullable = false, precision = 22, scale = 0)) })
	@Valid
	public ServVehiculoLinNegTipoVehId getId() {
		return this.id;
	}

	public void setId(ServVehiculoLinNegTipoVehId id) {
		this.id = id;
	}

	@Column(name = "idTcTipoServicioVehiculo", nullable = false, precision = 22, scale = 0)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getIdTcTipoServicioVehiculo() {
		return this.idTcTipoServicioVehiculo;
	}

	public void setIdTcTipoServicioVehiculo(Long idTcTipoServicioVehiculo) {
		this.idTcTipoServicioVehiculo = idTcTipoServicioVehiculo;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idTcTipoServicioVehiculo", referencedColumnName="idTcTipoServicioVehiculo", nullable = false, insertable = false, updatable = false)
	public TipoServicioVehiculoDTO getTipoServicioVehiculoDTO() {
		return tipoServicioVehiculoDTO;
	}

	public void setTipoServicioVehiculoDTO(
			TipoServicioVehiculoDTO tipoServicioVehiculoDTO) {
		this.tipoServicioVehiculoDTO = tipoServicioVehiculoDTO;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOSECCION", referencedColumnName="IDTOSECCION", nullable = false, insertable = false, updatable = false)
	public SeccionDTO getSeccionDTO() {
		return seccionDTO;
	}

	public void setSeccionDTO(SeccionDTO seccionDTO) {
		this.seccionDTO = seccionDTO;
	}
	
	@Transient
	public String getDescripcionTipoVehiculo() {
		return descripcionTipoVehiculo;
	}

	public void setDescripcionTipoVehiculo(String descripcionTipoVehiculo) {
		this.descripcionTipoVehiculo = descripcionTipoVehiculo;
	}
	
	@Transient
	public String getDescripcionTipoServicioVehiculo() {
		return descripcionTipoServicioVehiculo;
	}

	public void setDescripcionTipoServicioVehiculo(
			String descripcionTipoServicioVehiculo) {
		this.descripcionTipoServicioVehiculo = descripcionTipoServicioVehiculo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ServVehiculoLinNegTipoVehId getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}