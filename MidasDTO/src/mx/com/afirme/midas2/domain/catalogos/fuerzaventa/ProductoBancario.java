package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas.catalogos.institucionbancaria.BancoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.EditItemChecks;


/**
 * The persistent class for the TCPRODUCTOBANCARIO database table.
 * 
 */
@Entity(name="ProductoBancario")
@Table(name="TCPRODUCTOBANCARIO", schema="MIDAS")
public class ProductoBancario implements Serializable,Entidad{
	private static final long serialVersionUID = 1L;
	private Long id;
	private Integer claveCredito;
	private Integer claveTransfelectronica;
	private String descripcion;
	private BancoDTO banco;
	private List<ProductoBancarioAgente> productoBancarioAgentes;

    public ProductoBancario() {
    }
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTCPRODUCTOBANCARIO_SEQ")
	@SequenceGenerator(name="IDTCPRODUCTOBANCARIO_SEQ", sequenceName="MIDAS.IDTCPRODUCTOBANCARIO_SEQ")
	@Column(name="ID",nullable=false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="CLAVECREDITO",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=0,max=4)
	public Integer getClaveCredito() {
		return claveCredito;
	}
	public void setClaveCredito(Integer claveCredito) {
		this.claveCredito = claveCredito;
	}
	
	@Column(name="CLAVETRANSFELECTRONICA",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=0,max=4)
	public Integer getClaveTransfelectronica() {
		return claveTransfelectronica;
	}
	public void setClaveTransfelectronica(Integer claveTransfelectronica) {
		this.claveTransfelectronica = claveTransfelectronica;
	}
	
	@Column(name="DESCRIPCION",nullable=false,length=80)
	@Size(min=1,max=80)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	//bi-directional many-to-one association to ProductoBancarioAgente
	@OneToMany(mappedBy="productoBancario",fetch=FetchType.LAZY)
	public List<ProductoBancarioAgente> getProductoBancarioAgentes() {
		return this.productoBancarioAgentes;
	}

	public void setProductoBancarioAgentes(List<ProductoBancarioAgente> productoBancarioAgentes) {
		this.productoBancarioAgentes = productoBancarioAgentes;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}
	
	@Override
	public String getValue() {
		return descripcion;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="IDBANCO")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public BancoDTO getBanco() {
		return banco;
	}
	public void setBanco(BancoDTO banco) {
		this.banco = banco;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ProductoBancario))
			return false;
		ProductoBancario other = (ProductoBancario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}