package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Digits;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name="EnvioFacturaAgenteEst")
@Table(name="TOENVIOXMLAGENTESEST", schema="MIDAS")
public class EnvioFacturaAgenteEst implements Serializable, Entidad {
	private Long id;
	private Long idAgente;
	private Long estatus;

	private static final long serialVersionUID = 1L;
	
	public static final Long ESTATUS_FACTURAS_PENDIENTES = 0L;
	public static final Long ESTATUS_FACTURAS_ENTREGADAS = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTOENVIOXMLAGENTESEST_SEQ")
	@SequenceGenerator(name="IDTOENVIOXMLAGENTESEST_SEQ", schema="MIDAS", sequenceName="IDTOENVIOXMLAGENTESEST_SEQ", allocationSize=1)
	@Column(name="ID", nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="IDAGENTE", length=8)
	@Digits(integer=8, fraction=0)
	public Long getIdAgente() {
		return idAgente;
	}

	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}

	@Column(name="ESTATUS")
	public Long getEstatus() {
		return estatus;
	}

	public void setEstatus(Long estatus) {
		this.estatus = estatus;
	}

	@Override
	public <K> K getKey() {
		return null;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}
