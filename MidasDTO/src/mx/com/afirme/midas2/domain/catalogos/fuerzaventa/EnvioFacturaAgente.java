package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;

@Entity(name="EnvioFacturaAgente")
@Table(name="TOENVIOXMLAGENTES", schema="MIDAS")
public class EnvioFacturaAgente implements Serializable, Entidad {

	/**
	 * 
	 */
	 private static final long serialVersionUID = 1L;
	 
	 private Long idEnvio;
	 private ValorCatalogoAgentes status;
	 private Long idAgente;
	 private Date fechaEnvio;
	 private String fechaEnvioString;
	 private String nombreArchivo;
	 private Date fechaModifica;
	 private String fechaModificaString;
	 private HonorariosAgente honorarios;
	 private String anioMes;
	 private List<EnvioFacturaAgenteDet>  mensajes = new ArrayListNullAware<EnvioFacturaAgenteDet>();
	 private String uuid;
	 private String usuario;
	 private String messagesFactura="";
	 
	 
	 public EnvioFacturaAgente(){}
	 
	 public EnvioFacturaAgente(Long idEnvio, ValorCatalogoAgentes status, Long idAgente, Date fechaEnvio, String nombreArchivo, Date fechaModifica){
		 this.idEnvio = idEnvio;
		 this.status = status;
		 this.idAgente = idAgente;
		 this.fechaEnvio = fechaEnvio;
		 this.nombreArchivo = nombreArchivo;
		 this.fechaModifica = fechaModifica;
	 }
	 
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDENVIOXMLAGE_SEQ")
	@SequenceGenerator(name="IDENVIOXMLAGE_SEQ", schema="MIDAS", sequenceName="IDENVIOXMLAGE_SEQ", allocationSize=1)
	@Column(name="IDENVIO", nullable=false)
	public Long getIdEnvio() {
		return idEnvio;
	}
		
	public void setIdEnvio(Long idEnvio) {
		this.idEnvio = idEnvio;
	}
	
	@Column(name="IDAGENTE", length=8)
	@Digits(integer=8, fraction=0)
	public Long getIdAgente() {
		return idAgente;
	}
	
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHAENVIO")
	public Date getFechaEnvio() {
		return fechaEnvio;
	}
	
	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
		if(fechaEnvio!=null){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			this.fechaEnvioString=sdf.format(fechaEnvio);
		}
	}
	
	@Transient
	public String getFechaEnvioString() {
		return fechaEnvioString;
	}

	public void setFechaEnvioString(String fechaEnvioString) {
		this.fechaEnvioString = fechaEnvioString;
	}
	
	@Transient
	public String getFechaModificaString() {
		return fechaModificaString;
	}

	public void setFechaModificaString(String fechaModificaString) {
		this.fechaModificaString = fechaModificaString;
	}

	
	@Column(name="NOMBREARCHIVO")
	@Size(min=0, max=100)
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAMODIFICA")
	public Date getFechaModifica() {
		return fechaModifica;
	}
	
	public void setFechaModifica(Date fechaModifica) {
		this.fechaModifica = fechaModifica;
		if(fechaModifica!=null){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			this.fechaModificaString=sdf.format(fechaModifica);
		}
	}
		
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "envioFactura")
	public List<EnvioFacturaAgenteDet> getMensajes() {
		return mensajes;
	}
		
	public void setMensajes(List<EnvioFacturaAgenteDet> mensajes) {
		this.mensajes = mensajes;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="STATUS")
	public ValorCatalogoAgentes getStatus() {
		return status;
	}

	public void setStatus(ValorCatalogoAgentes status) {
		this.status = status;
	}
	
	
	@OneToOne(fetch=FetchType.EAGER, mappedBy="envioFactura")
	public HonorariosAgente getHonorarios() {
		return honorarios;
	}

	public void setHonorarios(HonorariosAgente honorarios) {
		this.honorarios = honorarios;
	}

	
	@Transient
	public String getAnioMes() {
		return anioMes;
	}

	public void setAnioMes(String anioMes) {
		this.anioMes = anioMes;
	}
	
	@Column(name = "UUID", nullable = false, length = 255)	
	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	@Column(name="USUARIO")
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	@Override
	public <K> K getKey() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// Auto-generated method stub
		return null;
	}
	@Transient
	public String getMessagesFactura() {
		return messagesFactura;
	}
	public void setMessagesFactura(String messagesFactura) {
		this.messagesFactura = messagesFactura;
	}
}
