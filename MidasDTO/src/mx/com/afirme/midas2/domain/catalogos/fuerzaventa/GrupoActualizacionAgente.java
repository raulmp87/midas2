package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

import java.util.List;


/**
 * The persistent class for the TCGRUPOACTUALIZACIONAGENTES database table.
 * 
 */
@Entity(name="GrupoActualizacionAgente")
@Table(name="TCGRUPOACTUALIZACIONAGENTES", schema="MIDAS")
public class GrupoActualizacionAgente implements Serializable,Entidad {
	private static final long serialVersionUID = 1L;
	private Long id;
	private String descripcion;
	private List<ActualizacionAgente> actualizacionAgentes;

    public GrupoActualizacionAgente() {
    }


	@Id
	@Column(name="ID",nullable=false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="DESCRIPCION",nullable=false,length=80)
	@Size(min=1,max=80)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	//bi-directional many-to-one association to Tlactualizacionagente
	@OneToMany(mappedBy="grupoActualizacionAgente",fetch=FetchType.LAZY)
	public List<ActualizacionAgente> getActualizacionAgentes() {
		return this.actualizacionAgentes;
	}

	public void setActualizacionAgentes(List<ActualizacionAgente> actualizacionAgentes) {
		this.actualizacionAgentes = actualizacionAgentes;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}


	@Override
	public String getValue() {
		return descripcion;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	
}