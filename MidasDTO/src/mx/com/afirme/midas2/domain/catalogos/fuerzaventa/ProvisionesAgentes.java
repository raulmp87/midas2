package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;


@Table(name="toProvisionesBonoAgente",schema="MIDAS")
@Entity(name="ProvisionesAgentes")
public class ProvisionesAgentes implements Serializable,Entidad{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String nombreProvision;
	private Date fechaProvision;
	private BigDecimal importeProvision;
	private ValorCatalogoAgentes modoEjecucion;
	private ValorCatalogoAgentes claveEstatus;
	private Integer totalBeneficiarios;
	private Integer anioMes;
	private Date fechaProvisionInicio;
	private Date fechaProvisionFin;
	
	
	
	@Id
	@SequenceGenerator(name="idToProvBonoAgt_seq",sequenceName="MIDAS.idToProvBonoAgt_seq",allocationSize=1)
	@GeneratedValue(generator="idToProvBonoAgt_seq",strategy=GenerationType.SEQUENCE)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="nombreProvision")
	public String getNombreProvision() {
		return nombreProvision;
	}

	public void setNombreProvision(String nombreProvision) {
		this.nombreProvision = nombreProvision;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="fechaProvision")
	public Date getFechaProvision() {
		return fechaProvision;
	}

	public void setFechaProvision(Date fechaProvision) {
		this.fechaProvision = fechaProvision;
	}

	@Column(name="importeProvision")
	public BigDecimal getImporteProvision() {
		return importeProvision;
	}

	public void setImporteProvision(BigDecimal importeProvision) {
		this.importeProvision = importeProvision;
	}


	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="modoEjecucion")
	public ValorCatalogoAgentes getModoEjecucion() {
		return modoEjecucion;
	}
	
	public void setModoEjecucion(ValorCatalogoAgentes modoEjecucion) {
		this.modoEjecucion = modoEjecucion;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="claveEstatus")
	public ValorCatalogoAgentes getClaveEstatus() {
		return claveEstatus;
	}

	public void setClaveEstatus(ValorCatalogoAgentes claveEstatus) {
		this.claveEstatus = claveEstatus;
	}
	
	
	@Column(name="TOTALBENEFICIARIOS")
	public Integer getTotalBeneficiarios() {
		return totalBeneficiarios;
	}

	public void setTotalBeneficiarios(Integer totalBeneficiarios) {
		this.totalBeneficiarios = totalBeneficiarios;
	}

	@Column(name="anioMes")
	public Integer getAnioMes() {
		return anioMes;
	}

	public void setAnioMes(Integer anioMes) {
		this.anioMes = anioMes;
	}
	
	
	@Transient
	public Date getFechaProvisionInicio() {
		return fechaProvisionInicio;
	}

	public void setFechaProvisionInicio(Date fechaProvisionInicio) {
		this.fechaProvisionInicio = fechaProvisionInicio;
	}
	
	@Transient
	public Date getFechaProvisionFin() {
		return fechaProvisionFin;
	}

	public void setFechaProvisionFin(Date fechaProvisionFin) {
		this.fechaProvisionFin = fechaProvisionFin;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}


}
