package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.lang.reflect.Method;

import javax.persistence.Table;

import mx.com.afirme.midas2.annotation.HistoryTable;

import org.eclipse.persistence.config.DescriptorCustomizer;
import org.eclipse.persistence.descriptors.ClassDescriptor;
import org.eclipse.persistence.history.HistoryPolicy;
import org.eclipse.persistence.mappings.DatabaseMapping;
import org.eclipse.persistence.mappings.ManyToManyMapping;

public class HistoryCustomizerOnlyQuerying implements DescriptorCustomizer{
	
	@Override
	public void customize(ClassDescriptor descriptor) throws Exception {
		
		HistoryPolicy policy = new HistoryPolicy();		
		policy.addHistoryTableName(descriptor.getTableName(), getHistoryTableName(descriptor.getJavaClass()));
		policy.addStartFieldName(START_FIELD_NAME);
		policy.addEndFieldName(END_FIELD_NAME);
		
		/*
		 Con este parametro se indica al Framework que no debe crear los registros historicos, 
		 el desarrollador debe proporcionar un mecanismo para realizar esta tarea, en este caso se usaran triggers.
		 De esta manera se asegura la integridad de informacion historica aun y cuando no sea modificada por la aplicacion
		 */
		policy.setShouldHandleWrites(false);
		
		descriptor.setHistoryPolicy(policy);		
		
		if (descriptor.hasRelationships()) {
			
			ManyToManyMapping relationMapping;
			String relationTableName;
			HistoryPolicy relationHistoryPolicy;
			
			for (DatabaseMapping mapping : descriptor.getMappings()) {
			
				if (mapping instanceof ManyToManyMapping) {
					
					relationMapping = (ManyToManyMapping) mapping;
					
					relationTableName = relationMapping.getRelationTableQualifiedName();
					
					relationHistoryPolicy = new HistoryPolicy();		
					
					relationHistoryPolicy.addHistoryTableName(relationTableName
							, getHistoryTableName(descriptor.getJavaClass(), relationMapping.getGetMethodName(), relationTableName));
					
					relationHistoryPolicy.addStartFieldName(START_FIELD_NAME);
					relationHistoryPolicy.addEndFieldName(END_FIELD_NAME);
					relationHistoryPolicy.setShouldHandleWrites(false);
					
					relationMapping.setHistoryPolicy(relationHistoryPolicy);		
									
				}
				
			}
			
			
		}
	
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String getHistoryTableName(Class clazz) {
		
		String historyTableName = null;
		
		if (clazz.isAnnotationPresent(HistoryTable.class)) {
			
			HistoryTable hist = (HistoryTable) clazz.getAnnotation(HistoryTable.class);
			
			historyTableName = getSchema(hist.schema()) + "." + hist.name().toUpperCase();
			
		} else if (clazz.isAnnotationPresent(Table.class)) {
			
			Table original = (Table) clazz.getAnnotation(Table.class);
			
			historyTableName = getSchema(original.schema()) + "." + original.name().toUpperCase() 
				+ (original.name().toUpperCase().indexOf(HISTORY_SUFFIX) < 0? HISTORY_SUFFIX:"");
			
		}
		
		return historyTableName;
		
	}
		
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String getHistoryTableName(Class clazz, String methodName, String relationTableName) throws Exception {
		
		String historyTableName = null;
		
		Method method = clazz.getMethod(methodName);
				
		if (method.isAnnotationPresent(HistoryTable.class)) {
			
			HistoryTable hist = method.getAnnotation(HistoryTable.class);
			
			historyTableName = getSchema(hist.schema()) + "." + hist.name().toUpperCase();
			
		} else {
			
			historyTableName = relationTableName.toUpperCase() 
				+ (relationTableName.toUpperCase().indexOf(HISTORY_SUFFIX) < 0? HISTORY_SUFFIX:"");
			
		}
		
		return historyTableName;
		
	}
	
	
	private String getSchema(String schema) {
		
		if (schema.trim().equals("")) {
			
			return DEFAULT_SCHEMA;
			
		}
		
		return schema.trim().toUpperCase();
		
	}
	
	private static final String START_FIELD_NAME = "H_START_DATE";
	private static final String END_FIELD_NAME = "H_END_DATE";
	private static final String HISTORY_SUFFIX = "_HIST";
	private static final String DEFAULT_SCHEMA = "MIDAS";
	
	
	
}
