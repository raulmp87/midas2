package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dao.catalogos.Entidad;


/**
 * The persistent class for the VNCLIENTE database table.
 * 
 */
@Entity(name="ClienteJPA")
@Table(name="VNCLIENTE",schema="MIDAS")
public class ClienteJPA implements Serializable, Entidad{
	private static final long serialVersionUID = 1L;
	
	private String apellidoMaterno;
	private String apellidoPaterno;
	private String claveActividadPersonaFisica;
	private String claveNacionalidad;
	private String claveRamaActividad;
	private String claveRamaPersonaFisica;
	private String claveResidencia;
	private String claveSubramaActividad;
	private String claveTipoSector;
	private String claveTipoSectorMoral;
	private String claveUsuarioModificacion;
	private String direccion;
	private String estadoCivil;
	
	private Date fechaBaja;
	private Date fechaConstitucionEmpresa;
	private Date fechaModificacion;
	private Date fechaNacimiento;
	private Date fehcaAlta;
	
	private String fechaRFC;
	private String homoclave;
	private Long idCliente;
	private Long idDireccion;
	private Long idPersona;
	private String lugarNacimiento;
	private String nacionalidad;
	private String nombre;
	private String nombrePersonaFisica;
	private String razonSocial;
	private String residencia;
	private String sexo;
	private String siglasRFC;
	private String sitioPersona;
	private String telefonoCasa;
	private String telefonoFax;
	private String telefonoOficina;
	private String tipoPersona;
	
	public ClienteJPA(){
	}
	
    @Id
	@Column(name="IDCLIENTE",nullable=false,unique=true)
	public Long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	
	@Column(name="APELLIDOMATERNO")
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	@Column(name="APELLIDOPATERNO" )
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	@Column(name="CLAVEACTIVIDADPERSONAFISICA" )
	public String getClaveActividadPersonaFisica() {
		return claveActividadPersonaFisica;
	}
	public void setClaveActividadPersonaFisica(String claveActividadPersonaFisica) {
		this.claveActividadPersonaFisica = claveActividadPersonaFisica;
	}

	@Column(name="CLAVENACIONALIDAD" )
	public String getClaveNacionalidad() {
		return claveNacionalidad;
	}
	public void setClaveNacionalidad(String claveNacionalidad) {
		this.claveNacionalidad = claveNacionalidad;
	}

	@Column(name="CLAVERAMAACTIVIDAD" )
	public String getClaveRamaActividad() {
		return claveRamaActividad;
	}
	public void setClaveRamaActividad(String claveRamaActividad) {
		this.claveRamaActividad = claveRamaActividad;
	}

	@Column(name="CLAVERAMAPERSONAFISICA" )
	public String getClaveRamaPersonaFisica() {
		return claveRamaPersonaFisica;
	}
	public void setClaveRamaPersonaFisica(String claveRamaPersonaFisica) {
		this.claveRamaPersonaFisica = claveRamaPersonaFisica;
	}

	@Column(name="CLAVERESIDENCIA" )
	public String getClaveResidencia() {
		return claveResidencia;
	}
	public void setClaveResidencia(String claveResidencia) {
		this.claveResidencia = claveResidencia;
	}

	@Column(name="CLAVESUBRAMAACTIVIDAD" )
	public String getClaveSubramaActividad() {
		return claveSubramaActividad;
	}
	public void setClaveSubramaActividad(String claveSubramaActividad) {
		this.claveSubramaActividad = claveSubramaActividad;
	}

	@Column(name="CLAVETIPOSECTOR" )
	public String getClaveTipoSector() {
		return claveTipoSector;
	}
	public void setClaveTipoSector(String claveTipoSector) {
		this.claveTipoSector = claveTipoSector;
	}

	@Column(name="CLAVETIPOSECTORMORAL" )
	public String getClaveTipoSectorMoral() {
		return claveTipoSectorMoral;
	}
	public void setClaveTipoSectorMoral(String claveTipoSectorMoral) {
		this.claveTipoSectorMoral = claveTipoSectorMoral;
	}

	@Column(name="CLAVEUSUARIOMODIFICACION" )
	public String getClaveUsuarioModificacion() {
		return claveUsuarioModificacion;
	}
	public void setClaveUsuarioModificacion(String claveUsuarioModificacion) {
		this.claveUsuarioModificacion = claveUsuarioModificacion;
	}

	@Column(name="DIRECCION" )
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	@Column(name="ESTADOCIVIL" )
	public String getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHABAJA")
	public Date getFechaBaja() {
		return fechaBaja;
	}
	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHACONSTITUCIONEMPRESA")
	public Date getFechaConstitucionEmpresa() {
		return fechaConstitucionEmpresa;
	}
	public void setFechaConstitucionEmpresa(Date fechaConstitucionEmpresa) {
		this.fechaConstitucionEmpresa = fechaConstitucionEmpresa;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHAMODIFICACION")
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHANACIMIENTO")
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FEHCAALTA",nullable=false)
	@NotNull(message="{com.afirme.midas2.requerido}")
	public Date getFehcaAlta() {
		return fehcaAlta;
	}
	public void setFehcaAlta(Date fehcaAlta) {
		this.fehcaAlta = fehcaAlta;
	}

	@Column(name="FECHARFC" )
	@NotNull(message="{com.afirme.midas2.requerido}")
	public String getFechaRFC() {
		return fechaRFC;
	}
	public void setFechaRFC(String fechaRFC) {
		this.fechaRFC = fechaRFC;
	}

	@Column(name="HOMOCLAVE" )
	@NotNull(message="{com.afirme.midas2.requerido}")
	public String getHomoclave() {
		return homoclave;
	}
	public void setHomoclave(String homoclave) {
		this.homoclave = homoclave;
	}
	
	@Column(name="IDDIRECCION")
	public Long getIdDireccion() {
		return idDireccion;
	}
	public void setIdDireccion(Long idDireccion) {
		this.idDireccion = idDireccion;
	}

	@Column(name="IDPERSONA")
	public Long getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	@Column(name="LUGARNACIMIENTO" )
	public String getLugarNacimiento() {
		return lugarNacimiento;
	}
	public void setLugarNacimiento(String lugarNacimiento) {
		this.lugarNacimiento = lugarNacimiento;
	}

	@Column(name="NACIONALIDAD" )
	public String getNacionalidad() {
		return nacionalidad;
	}
	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	@Column(name="NOMBRE" )
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name="NOMBREPERSONAFISICA" )
	public String getNombrePersonaFisica() {
		return nombrePersonaFisica;
	}
	public void setNombrePersonaFisica(String nombrePersonaFisica) {
		this.nombrePersonaFisica = nombrePersonaFisica;
	}

	@Column(name="RAZONSOCIAL" )
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	@Column(name="RESIDENCIA" )
	public String getResidencia() {
		return residencia;
	}
	public void setResidencia(String residencia) {
		this.residencia = residencia;
	}

	@Column(name="SEXO" )
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	@Column(name="SIGLASRFC" )
	@NotNull(message="{com.afirme.midas2.requerido}")
	public String getSiglasRFC() {
		return siglasRFC;
	}
	public void setSiglasRFC(String siglasRFC) {
		this.siglasRFC = siglasRFC;
	}

	@Column(name="SITIOPERSONA" )
	@NotNull(message="{com.afirme.midas2.requerido}")
	public String getSitioPersona() {
		return sitioPersona;
	}
	public void setSitioPersona(String sitioPersona) {
		this.sitioPersona = sitioPersona;
	}

	@Column(name="TELEFONOCASA" )
	public String getTelefonoCasa() {
		return telefonoCasa;
	}
	public void setTelefonoCasa(String telefonoCasa) {
		this.telefonoCasa = telefonoCasa;
	}

	@Column(name="TELEFONOFAX")
	public String getTelefonoFax() {
		return telefonoFax;
	}
	public void setTelefonoFax(String telefonoFax) {
		this.telefonoFax = telefonoFax;
	}

	@Column(name="TELEFONOOFICINA" )
	public String getTelefonoOficina() {
		return telefonoOficina;
	}
	public void setTelefonoOficina(String telefonoOficina) {
		this.telefonoOficina = telefonoOficina;
	}

	@Column(name="TIPOPERSONA" )
	@NotNull(message="{com.afirme.midas2.requerido}")
	public String getTipoPersona() {
		return tipoPersona;
	}
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		// TODO Auto-generated method stub
		return idCliente;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		// TODO Auto-generated method stub
		return idCliente;
	}

}