package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.eclipse.persistence.annotations.Customizer;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
@Entity(name="ClienteAgente")
@Table(name="toClienteAgente",schema="MIDAS")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)//HistoryCustomizer
public class ClienteAgente implements Serializable,Entidad{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private	Agente agente;
	private ClienteJPA cliente;
	private	Integer	claveEstatus;
    private HerenciaClientes herenciaClientes; 
	
	public ClienteAgente(){}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idToClienteAgente_seq")
	@SequenceGenerator(name="idToClienteAgente_seq", sequenceName="MIDAS.idToClienteAgente_seq",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="AGENTE_ID",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Agente getAgente() {
		return agente;
	}
	public void setAgente(Agente agente) {
		this.agente = agente;
	}
	
	@Column(name="CLAVEESTATUS",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Integer getClaveEstatus() {
		return claveEstatus;
	}
	public void setClaveEstatus(Integer claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ClienteJPA.class)
	@JoinColumn(name="IDCLIENTE")
	public ClienteJPA getCliente() {
		return cliente;
	}
	public void setCliente(ClienteJPA cliente) {
		this.cliente = cliente;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=HerenciaClientes.class)
	@JoinColumn(name="IDHERENCIA",nullable=false)
		public HerenciaClientes getHerenciaClientes() {
		return herenciaClientes;
	}

	public void setHerenciaClientes(HerenciaClientes herenciaClientes) {
		this.herenciaClientes = herenciaClientes;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}



	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

}
