package mx.com.afirme.midas2.domain.catalogos.retencionimpuestos;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.dto.impuestosestatales.RetencionImpuestosDTO;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "TOIMPUESTOAGENTE", schema = "MIDAS")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class RetencionImpuestos implements Serializable, Entidad {
	private static final long serialVersionUID = -5580856982113721106L;
	private Long id;
	private EstadoDTO estado;
	private Long porcentaje;
	private String concepto;
	private ValorCatalogoAgentes tipoComision;
	private ValorCatalogoAgentes personalidadJuridica;
	private Date fechaInicioVig;
	private Date fechaFinVig;
	private String estatus;
	private String usuario;

	public RetencionImpuestos() {}
	
	public RetencionImpuestos(RetencionImpuestosDTO dto) {
		this.id = dto.getId();
		this.estado = dto.getEstado();
		this.porcentaje = dto.getPorcentaje();
		this.concepto = dto.getConcepto();
		this.tipoComision = dto.getTipoComision();
		this.personalidadJuridica = dto.getPersonalidadJuridica();
		this.fechaInicioVig = dto.getFechaInicioVig();
		this.fechaFinVig = dto.getFechaFinVig();
		this.estatus = dto.getEstatus();
		this.usuario = dto.getUsuario();
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "toImpuestoAgente_seq")
	@SequenceGenerator(name = "toImpuestoAgente_seq", sequenceName = "toImpuestoAgente_seq", allocationSize=1, schema="MIDAS")
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}

	@Column(name = "PORCENTAJE")
	public Long getPorcentaje() {
		return porcentaje;
	}

	@Column(name = "CONCEPTO")
	public String getConcepto() {
		return concepto;
	}

	@ManyToOne(fetch = FetchType.LAZY,targetEntity=EstadoDTO.class)
	@JoinColumn(name = "IDESTADO", referencedColumnName = "STATE_ID")
	public EstadoDTO getEstado() {
		return estado;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name = "TIPOCOMISION_ID", referencedColumnName = "ID")
	public ValorCatalogoAgentes getTipoComision() {
		return tipoComision;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name = "PERSONALIDADJURIDICA_ID", referencedColumnName = "ID")
	public ValorCatalogoAgentes getPersonalidadJuridica() {
		return personalidadJuridica;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAINICIOVIG")
	public Date getFechaInicioVig() {
		return fechaInicioVig;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAFINVIG")
	public Date getFechaFinVig() {
		return fechaFinVig;
	}

	@Column(name = "ESTATUS")
	public String getEstatus() {
		return estatus;
	}
	
	@Column(name = "USUARIO")
	public String getUsuario() {
		return usuario;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public void setPorcentaje(Long porcentaje) {
		this.porcentaje = porcentaje;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public void setFechaInicioVig(Date fechaInicioVig) {
		this.fechaInicioVig = fechaInicioVig;
	}
	public void setFechaFinVig(Date fechaFinVig) {
		this.fechaFinVig = fechaFinVig;
	}
	public void setEstado(EstadoDTO estado) {
		this.estado = estado;
	}
	public void setTipoComision(ValorCatalogoAgentes tipoComision) {
		this.tipoComision = tipoComision;
	}
	public void setPersonalidadJuridica(ValorCatalogoAgentes personalidadJuridica) {
		this.personalidadJuridica = personalidadJuridica;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}
	
	@Override
	public String getValue() {
		return null;
	}
	
	@Override
	public <K> K getBusinessKey() {
		return null;
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		SimpleDateFormat format = new SimpleDateFormat("dd/mm/yyyy");
		result.append("Estatus: " + (StringUtils.isNotEmpty(this.getEstatus()) ? this.getEstatus() : " "));
		result.append(", ID: " + (this.getId() != null ? this.getId() : " "));
		result.append(", Estado: " + (this.getEstado() != null ? this.getEstatus() : " "));
		result.append(", Porcentaje: " + (this.getPorcentaje() != null ? this.getPorcentaje() : " "));
		result.append(", Concepto" + (StringUtils.isNotEmpty(this.getConcepto()) ? this.getEstatus() : " "));
		result.append(", Tipo Comision: " + (this.getTipoComision() != null && this.getTipoComision().getValor() != null ? this.getTipoComision().getValor() : " "));
		result.append(", Tipo Persona Juridica: " + (this.getPersonalidadJuridica() != null && this.getPersonalidadJuridica().getValor() != null ? this.getPersonalidadJuridica().getValor() : " "));
		result.append(", Fecha Inicio Vigencia: " + (this.getFechaInicioVig() != null ? format.format(this.getFechaInicioVig()) : " "));
		result.append(", Fecha Fin Vigencia: " + (this.getFechaFinVig() != null ? format.format(this.getFechaFinVig()) : " "));
		result.append(", Usuario Autor: " + (StringUtils.isNotEmpty(this.getUsuario()) ? this.getUsuario() : " "));
		return result.toString();
	}

}
