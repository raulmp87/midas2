package mx.com.afirme.midas2.domain.catalogos;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;


/**
 * CobPaquetesSeccion entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TRCOBPAQUETESECCION"
    ,schema="MIDAS"
)

public class CobPaquetesSeccion  implements Serializable, Entidad {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = -4184292566348940869L;
	private CobPaquetesSeccionId id;
    private Short claveObligatoriedad;


    // Constructors

    /** default constructor */
    public CobPaquetesSeccion() {
    }

    
    /** full constructor */
    public CobPaquetesSeccion(CobPaquetesSeccionId id, Short claveObligatoriedad) {
        this.id = id;
        this.claveObligatoriedad = claveObligatoriedad;
    }

   
    // Property accessors
    @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idToSeccion", column=@Column(name="IDTOSECCION", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idPaquete", column=@Column(name="IDPAQUETE", nullable=false, precision=22, scale=0) ), 
        @AttributeOverride(name="idToCobertura", column=@Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0) ) } )

    public CobPaquetesSeccionId getId() {
        return this.id;
    }
    
    public void setId(CobPaquetesSeccionId id) {
        this.id = id;
    }
    
    @Column(name="CLAVEOBLIGATORIEDAD", nullable=false, precision=4, scale=0)

    public Short getClaveObligatoriedad() {
        return this.claveObligatoriedad;
    }
    
    public void setClaveObligatoriedad(Short claveObligatoriedad) {
        this.claveObligatoriedad = claveObligatoriedad;
    }


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CobPaquetesSeccion other = (CobPaquetesSeccion) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


	@Override
	public CobPaquetesSeccionId getKey() {		
		return id;
	}


	@Override
	public String getValue() {		
		return null;
	}


	@Override
	public CobPaquetesSeccionId getBusinessKey() {		
		return id;
	}
   

}