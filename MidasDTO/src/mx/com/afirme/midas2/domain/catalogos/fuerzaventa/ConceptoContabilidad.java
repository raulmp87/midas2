package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name="ConceptoContabilidad")
@Table(name="CONTCONCEPTO", schema="MIDAS")
public class ConceptoContabilidad implements Serializable,Entidad {	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private String claveTipoConcepto;
	private Long idConcepto;
	private String descripcionConcepto;
	private String claveCargoAbono;
	private Long idGrupoConcepto;
	private Short activo;
	private Long idMoneda;
	private Short banderaImpuesto;
	private Short banderaPercepcionDeducible;
	private Short banderaBonos;
	private Short banderaGravable;
	private Short visible;
	private Long idGuiaContable;
	private Long IdRubroPrincipalAfectado;
	
	
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="CVETIPOCONCEPTO", nullable=false, length=5)
	public String getClaveTipoConcepto() {
		return claveTipoConcepto;
	}
	public void setClaveTipoConcepto(String claveTipoConcepto) {
		this.claveTipoConcepto = claveTipoConcepto;
	}
	
	@Column(name="IDCONCEPTO", nullable=false, precision=5)
	public Long getIdConcepto() {
		return idConcepto;
	}
	public void setIdConcepto(Long idConcepto) {
		this.idConcepto = idConcepto;
	}
	
	@Column(name="DESCRIPCION" ,nullable=false,length=80)
	public String getDescripcionConcepto() {
		return descripcionConcepto;
	}
	public void setDescripcionConcepto(String descripcionConcepto) {
		this.descripcionConcepto = descripcionConcepto;
	}
	
	@Column(name="CVECA",nullable=false,length=1)
	public String getClaveCargoAbono() {
		return claveCargoAbono;
	}
	public void setClaveCargoAbono(String claveCargoAbono) {
		this.claveCargoAbono = claveCargoAbono;
	}
	
	@Column(name="IDGRUPOCPTO")
	public Long getIdGrupoConcepto() {
		return idGrupoConcepto;
	}
	public void setIdGrupoConcepto(Long idGrupoConcepto) {
		this.idGrupoConcepto = idGrupoConcepto;
	}
	
	@Column(name="ACTIVO",nullable=false)
	public Short getActivo() {
		return activo;
	}
	public void setActivo(Short activo) {
		this.activo = activo;
	}
	
	@Column(name="IDMONEDA",precision=3)
	public Long getIdMoneda() {
		return idMoneda;
	}
	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}
	
	@Column(name="BIMPUESTO",nullable=false)
	public Short getBanderaImpuesto() {
		return banderaImpuesto;
	}
	public void setBanderaImpuesto(Short banderaImpuesto) {
		this.banderaImpuesto = banderaImpuesto;
	}
	
	@Column(name="BPERCEPDEDUC",nullable=false)
	public Short getBanderaPercepcionDeducible() {
		return banderaPercepcionDeducible;
	}
	public void setBanderaPercepcionDeducible(Short banderaPercepcionDeducible) {
		this.banderaPercepcionDeducible = banderaPercepcionDeducible;
	}
	
	@Column(name="BBONO",nullable=false)
	public Short getBanderaBonos() {
		return banderaBonos;
	}
	public void setBanderaBonos(Short banderaBonos) {
		this.banderaBonos = banderaBonos;
	}
	
	@Column(name="BGRAVABLE",nullable=false)
	public Short getBanderaGravable() {
		return banderaGravable;
	}
	public void setBanderaGravable(Short banderaGravable) {
		this.banderaGravable = banderaGravable;
	}	
	
	@Column(name="VISIBLE",nullable=false)
	public Short getVisible() {
		return visible;
	}
	public void setVisible(Short visible) {
		this.visible = visible;
	}
	
	@Column(name="IDGUIACONTABLE", nullable=false)
	public Long getIdGuiaContable() {
		return idGuiaContable;
	}
	public void setIdGuiaContable(Long idGuiaContable) {
		this.idGuiaContable = idGuiaContable;
	}
	
	@Column(name="IDRUBROPPALAFECTADO")
	public Long getIdRubroPrincipalAfectado() {
		return IdRubroPrincipalAfectado;
	}
	public void setIdRubroPrincipalAfectado(Long idRubroPrincipalAfectado) {
		IdRubroPrincipalAfectado = idRubroPrincipalAfectado;
	} 
	
		
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		// TODO Auto-generated method stub
		return id;
	}
	
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return descripcionConcepto;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		// TODO Auto-generated method stub
		return id;
	}
}
