package mx.com.afirme.midas2.domain.catalogos;

import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * CobPaquetesSeccionId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class CobPaquetesSeccionId  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = -870365152904380716L;
	private Long idToSeccion;
    private Long idPaquete;
    private Long idToCobertura;


    // Constructors

    /** default constructor */
    public CobPaquetesSeccionId() {
    }

    
    /** full constructor */
    public CobPaquetesSeccionId(Long idToSeccion, Long idPaquete, Long idToCobertura) {
        this.idToSeccion = idToSeccion;
        this.idPaquete = idPaquete;
        this.idToCobertura = idToCobertura;
    }

   
    // Property accessors

    @Column(name="IDTOSECCION", nullable=false, precision=22, scale=0)

    public Long getIdToSeccion() {
        return this.idToSeccion;
    }
    
    public void setIdToSeccion(Long idToSeccion) {
        this.idToSeccion = idToSeccion;
    }

    @Column(name="IDPAQUETE", nullable=false, precision=22, scale=0)

    public Long getIdPaquete() {
        return this.idPaquete;
    }
    
    public void setIdPaquete(Long idPaquete) {
        this.idPaquete = idPaquete;
    }

    @Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0)

    public Long getIdToCobertura() {
        return this.idToCobertura;
    }
    
    public void setIdToCobertura(Long idToCobertura) {
        this.idToCobertura = idToCobertura;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof CobPaquetesSeccionId) ) return false;
		 CobPaquetesSeccionId castOther = ( CobPaquetesSeccionId ) other; 
         
		 return ( (this.getIdToSeccion()==castOther.getIdToSeccion()) || ( this.getIdToSeccion()!=null && castOther.getIdToSeccion()!=null && this.getIdToSeccion().equals(castOther.getIdToSeccion()) ) )
 && ( (this.getIdPaquete()==castOther.getIdPaquete()) || ( this.getIdPaquete()!=null && castOther.getIdPaquete()!=null && this.getIdPaquete().equals(castOther.getIdPaquete()) ) )
 && ( (this.getIdToCobertura()==castOther.getIdToCobertura()) || ( this.getIdToCobertura()!=null && castOther.getIdToCobertura()!=null && this.getIdToCobertura().equals(castOther.getIdToCobertura()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdToSeccion() == null ? 0 : this.getIdToSeccion().hashCode() );
         result = 37 * result + ( getIdPaquete() == null ? 0 : this.getIdPaquete().hashCode() );
         result = 37 * result + ( getIdToCobertura() == null ? 0 : this.getIdToCobertura().hashCode() );
         return result;
   } 

}