package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.eclipse.persistence.annotations.Customizer;

import mx.com.afirme.midas.catalogos.sector.SectorDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.DomicilioSeycos;
import mx.com.afirme.midas2.validator.group.EditItemChecks;


/**
 * The persistent class for the TOAFIANZADORA database table.
 * 
 */
@Entity(name="Afianzadora")
@Table(name="TOAFIANZADORA", schema="MIDAS")
@SqlResultSetMapping(name="afianzadoraView",entities={
		@EntityResult(entityClass=Afianzadora.class,fields={
			@FieldResult(name="id",column="id"),
			@FieldResult(name="idAfianzadora",column="idAfianzadora"),
			@FieldResult(name="razonSocial",column="razonSocial"),
			@FieldResult(name="siglasRfc",column="siglasRfc"),
			@FieldResult(name="fechaRfc",column="fechaRfc"),
			@FieldResult(name="homoClaveRfc",column="homoClaveRfc"),
			@FieldResult(name="sector.idSector",column="SECTOR_ID"),
			@FieldResult(name="sector.nombreSector",column="nombreSector")
		})
		/*@EntityResult(entityClass=SectorDTO.class,fields={
			@FieldResult(name="idSector ",column="SECTOR_ID"),
			@FieldResult(name="nombreSector ",column="nombreSector")
		})*/
	}
)
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class Afianzadora implements Serializable,Entidad{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4513055386509138249L;
	private Long id;
	private Long claveEstatus;
	private String correoElectronico;
	private Long extensionOficina;
	private String faxOficina;
	private Date fechaAlta;
	private Date fechaBaja;
	private String fechaRfc;
	private String homoclaveRfc;
	private Long idAfianzadora;
	private Domicilio domicilio;
	private String paginaWeb;
	private String razonSocial;
	private String siglasRfc;
	private String telefonoCelular;
	private String telefonoOficina;
	private String telefonoParticular;
	private SectorDTO sector;
	private Date fechaInicio;
	private Date fechaFin;
	private List<ActualizacionAgente> actualizaciones;
	private DomicilioSeycos domicilioHistorico;

    public Afianzadora() {
    }


	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTOAFIANZADORA_SEQ")
	@SequenceGenerator(name="IDTOAFIANZADORA_SEQ", sequenceName="MIDAS.IDTOAFIANZADORA_SEQ",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

    @Temporal( TemporalType.DATE)
    @Column(name="FECHAALTA",nullable=false)
	public Date getFechaAlta() {
		return this.fechaAlta;
	}

	public void setFechaAlta(Date fechaalta) {
		this.fechaAlta = fechaalta;
	}


    @Temporal( TemporalType.DATE)
    @Column(name="FECHABAJA",nullable=true)
	public Date getFechaBaja() {
		return this.fechaBaja;
	}

	public void setFechaBaja(Date fechabaja) {
		this.fechaBaja = fechabaja;
	}

	@Column(name="CLAVEESTATUS",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getClaveEstatus() {
		return claveEstatus;
	}


	public void setClaveEstatus(Long claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	@Column(name="CORREOELECTRONICO",length=80)
	@Size(min=0,max=80)
	public String getCorreoElectronico() {
		return correoElectronico;
	}


	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	@Column(name="EXTENSIONOFICINA",length=6)
	@Digits(integer=6,fraction=0)
	public Long getExtensionOficina() {
		return extensionOficina;
	}


	public void setExtensionOficina(Long extensionOficina) {
		this.extensionOficina = extensionOficina;
	}

	@Column(name="FAXOFICINA",length=20)
	@Size(min=0,max=20)
	public String getFaxOficina() {
		return faxOficina;
	}

	public void setFaxOficina(String faxOficina) {
		this.faxOficina = faxOficina;
	}
	
	@Column(name="FECHARFC",length=6)
	@Size(min=0,max=6)
	public String getFechaRfc() {
		return fechaRfc;
	}


	public void setFechaRfc(String fechaRfc) {
		this.fechaRfc = fechaRfc;
	}

	@Column(name="HOMOCLAVERFC",length=4,nullable=false)
	@Size(min=0,max=4)
	public String getHomoclaveRfc() {
		return homoclaveRfc;
	}


	public void setHomoclaveRfc(String homoclaveRfc) {
		this.homoclaveRfc = homoclaveRfc;
	}

	@Column(name="IDAFIANZADORA",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getIdAfianzadora() {
		return idAfianzadora;
	}


	public void setIdAfianzadora(Long idAfianzadora) {
		this.idAfianzadora = idAfianzadora;
	}

	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumns({
		@JoinColumn(name="IDDOMICILIO",referencedColumnName="IDDOMICILIO"),
		@JoinColumn(name="IDAFIANZADORA",referencedColumnName="IDPERSONA",insertable=false,updatable=false)
	})
	public Domicilio getDomicilio() {
		return domicilio;
	}


	public void setDomicilio(Domicilio domicilio) {
		this.domicilio =domicilio;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="IDSECTOR")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public SectorDTO getSector() {
		return sector;
	}


	public void setSector(SectorDTO sector) {
		this.sector = sector;
	}

	@Column(name="PAGINAWEB",length=80)
	@Size(min=0,max=80)
	public String getPaginaWeb() {
		return paginaWeb;
	}


	public void setPaginaWeb(String paginaWeb) {
		this.paginaWeb = paginaWeb;
	}

	@Column(name="RAZONSOCIAL",length=120,nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=0,max=120)
	public String getRazonSocial() {
		return razonSocial;
	}


	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	@Column(name="SIGLASRFC",length=4,nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=0,max=4)
	public String getSiglasRfc() {
		return siglasRfc;
	}


	public void setSiglasRfc(String siglasRfc) {
		this.siglasRfc = siglasRfc;
	}

	@Column(name="TELEFONOCELULAR",length=20)
	@Size(min=0,max=20)
	public String getTelefonoCelular() {
		return telefonoCelular;
	}


	public void setTelefonoCelular(String telefonoCelular) {
		this.telefonoCelular = telefonoCelular;
	}

	@Column(name="TELEFONOOFICINA",length=20)
	@Size(min=0,max=20)
	public String getTelefonoOficina() {
		return telefonoOficina;
	}


	public void setTelefonoOficina(String telefonoOficina) {
		this.telefonoOficina = telefonoOficina;
	}

	@Column(name="TELEFONOPARTICULAR",length=20)
	@Size(min=0,max=20)
	public String getTelefonoParticular() {
		return telefonoParticular;
	}


	public void setTelefonoParticular(String telefonoParticular) {
		this.telefonoParticular = telefonoParticular;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}


	@Override
	public String getValue() {
		return razonSocial;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	@Transient
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	@Transient
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	@OneToMany(fetch=FetchType.LAZY,targetEntity=ActualizacionAgente.class)
	@JoinColumn(name="IDREGISTRO")
	public List<ActualizacionAgente> getActualizaciones() {
		return actualizaciones;
	}

	public void setActualizaciones(List<ActualizacionAgente> actualizaciones) {
		this.actualizaciones = actualizaciones;
	}
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name="IDDOMICILIO",referencedColumnName="ID_DOMICILIO", insertable=false, updatable= false),
		@JoinColumn(name="IDAFIANZADORA",referencedColumnName="ID_PERSONA",insertable=false,updatable=false)
	})
	public DomicilioSeycos getDomicilioHistorico() {
		return domicilioHistorico;
	}


	public void setDomicilioHistorico(DomicilioSeycos domicilioHistorico) {
		this.domicilioHistorico = domicilioHistorico;
	}
}