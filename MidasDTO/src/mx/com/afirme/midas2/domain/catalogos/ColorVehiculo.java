package mx.com.afirme.midas2.domain.catalogos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
import mx.com.afirme.midas2.validator.group.NewItemChecks;



@Entity
@Table(name = "TCCOLORVEHICULO", schema = "MIDAS")
public class ColorVehiculo implements Serializable, Entidad {

	private static final long serialVersionUID = 1153768635128894145L;
	
	private Long id;
	private Integer clave;
	private String descripcion;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTCCOLORVEHICULO_SEQ")
	@SequenceGenerator(name="IDTCCOLORVEHICULO_SEQ", sequenceName="MIDAS.IDTCCOLORVEHICULO_SEQ", allocationSize=1)
	@Column(name = "IDTCCOLORVEHICULO", nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "CODIGOCOLOR", nullable = false, precision = 8, scale = 4)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Integer getClave() {
		return clave;
	}
	public void setClave(Integer clave) {
		this.clave = clave;
	}
	
	@Column(name = "DESCRIPCIONCOLOR", nullable = true, length = 100)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=1, max=100, groups=EditItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clave == null) ? 0 : clave.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ColorVehiculo other = (ColorVehiculo) obj;
		if (clave == null) {
			if (other.clave != null)
				return false;
		} else if (!clave.equals(other.clave))
			return false;
		return true;
	}
	
	
	public ColorVehiculo() {
	}
	
	@Override
	public String getValue() {
		return this.descripcion;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public  Long getKey() {
		return this.id;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Integer getBusinessKey() {
		return this.clave;
	}
	
	
	
	
	
	
	
}
