package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.EditItemChecks;



/**
 * The persistent class for the TLACTUALIZACIONAGENTES database table.
 * 
 */
@Entity(name="ActualizacionAgente")
@Table(name="TLACTUALIZACIONAGENTES", schema="MIDAS")
public class ActualizacionAgente implements Serializable,Entidad{
	private static final long serialVersionUID = 1L;
	private Long id;
	private String comentarios;
	private Date fechaHoraActualizacion;
	private String fechaHoraActualizacionString;
	private Long idRegistro;
	private String usuarioActualizacion;
	private GrupoActualizacionAgente grupoActualizacionAgente;
	private String tipoMovimiento;
	private String fechaHoraActualizFormatoMostrar;

    public ActualizacionAgente() {
    }


	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTLACTUALIZACIONAGENTES_SEQ")
	@SequenceGenerator(name="IDTLACTUALIZACIONAGENTES_SEQ", sequenceName="MIDAS.IDTLACTUALIZACIONAGENTES_SEQ",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@Column(name="COMENTARIOS",nullable=true,length=200)
	@Size(min=0,max=200)
	public String getComentarios() {
		return comentarios;
	}


	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHAHORAACTUALIZACION",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Date getFechaHoraActualizacion() {
		return fechaHoraActualizacion;
	}

	public void setFechaHoraActualizacion(Date fechaHoraActualizacion) {
		this.fechaHoraActualizacion = fechaHoraActualizacion;
	}

	@Column(name="IDREGISTRO",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getIdRegistro() {
		return idRegistro;
	}


	public void setIdRegistro(Long idRegistro) {
		this.idRegistro = idRegistro;
	}

	@Column(name="USUARIOACTUALIZACION",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=1,max=8)
	public String getUsuarioActualizacion() {
		return usuarioActualizacion;
	}


	public void setUsuarioActualizacion(String usuarioActualizacion) {
		this.usuarioActualizacion = usuarioActualizacion;
	}


	//bi-directional many-to-one association to GrupoActualizacionAgente
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="GRUPOACTUALIZACIONAGENTES_ID")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public GrupoActualizacionAgente getGrupoActualizacionAgente() {
		return this.grupoActualizacionAgente;
	}

	public void setGrupoActualizacionAgente(GrupoActualizacionAgente grupoActualizacionAgente) {
		this.grupoActualizacionAgente = grupoActualizacionAgente;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}


	@Override
	public String getValue() {
		return this.comentarios;
	}


	@SuppressWarnings("unchecked")
	@Override
	public  Long getBusinessKey() {
		return id;
	}

	@Column(name="TIPOMOVIMIENTO")
	public String getTipoMovimiento() {
		return tipoMovimiento;
	}


	public void setTipoMovimiento(String tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}
	
	@Transient
    public String getFechaHoraActualizacionString() {
          if(fechaHoraActualizacion!=null){
                SimpleDateFormat formatFecha=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSSSSS a");
                fechaHoraActualizacionString=formatFecha.format(fechaHoraActualizacion);
          }
          return fechaHoraActualizacionString;
    }

	public void setFechaHoraActualizacionString(String fechaHoraActualizacionString) {
		this.fechaHoraActualizacionString = fechaHoraActualizacionString;
	}

	@Transient
	public String getFechaHoraActualizFormatoMostrar() {
		if(fechaHoraActualizacion!=null){
            SimpleDateFormat formatFecha=new SimpleDateFormat("dd/MM/yyyy h:mm a");
            fechaHoraActualizFormatoMostrar=formatFecha.format(fechaHoraActualizacion);
      }
		return fechaHoraActualizFormatoMostrar;
	}

	public void setFechaHoraActualizFormatoMostrar(
			String fechaHoraActualizFormatoMostrar) {
		this.fechaHoraActualizFormatoMostrar = fechaHoraActualizFormatoMostrar;
	}	
}