package mx.com.afirme.midas2.domain.catalogos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CatValorFijoId implements Serializable{

	
	private static final long serialVersionUID = -8521914751601389665L;

	@Column(name="ID")
	private Integer id;
	
	@Column(name="GRUPO_ID")
	private Integer grupoId;
	
	public CatValorFijoId() {
		super();
	}

	public CatValorFijoId(Integer id, Integer grupoId) {
		super();
		this.id = id;
		this.grupoId = grupoId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((grupoId == null) ? 0 : grupoId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof CatValorFijoId)) {
			return false;
		}
		CatValorFijoId other = (CatValorFijoId) obj;
		if (grupoId == null) {
			if (other.grupoId != null) {
				return false;
			}
		} else if (!grupoId.equals(other.grupoId)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getGrupoId() {
		return grupoId;
	}

	public void setGrupoId(Integer grupoId) {
		this.grupoId = grupoId;
	}
	
	
}
