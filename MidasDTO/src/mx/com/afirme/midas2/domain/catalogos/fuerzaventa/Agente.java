package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.directwebremoting.annotations.DataTransferObject;
import org.eclipse.persistence.annotations.Customizer;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.validator.group.EditItemChecks;


/**
 * The persistent class for the TOAGENTE database table.
 * 
 */
@Entity(name="Agente")
@Table(name="TOAGENTE",schema="MIDAS")
@DataTransferObject
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class Agente implements Serializable,Entidad {
	private static final long serialVersionUID = 1L;
	private Long id;
	private String apellidoMaternoConyuge;
	private String apellidoPaternoConyuge;
	private Integer claveComision;
	private Integer claveContabilizaComision;
	private ValorCatalogoAgentes tipoSituacion;
	private Integer claveGeneraCheque;
	private Integer claveImprimeEstadoCta;
	private Date fechaActivacion;
	private Date fechaAlta;
	private Date fechaAltaFin;
	private Date fechaAutorizacionCedula;
	private Date fechaAutorizacionFianza;
	private Date fechaAutorizacionFianzaFin;
	private Date fechaMatrimonio;
	private Date fechaNacimientoConyuge;
	private Date fechaVencimientoCedula;
	private String vencimientoCedulaString;
	private Date fechaVencimientoFianza;
	private String vencimientoFianzaString;
	private Afianzadora afianzadora;
	private Long idAgente;
	private Long idAgenteReclutador;
	private Long idMotivoEstatusAgente;//FIXME
	private Persona persona;
	private Long idPrioridadAgente;//FIXME
	private Promotoria promotoria;
	private Long idTipoAgente;//FIXME
	private ValorCatalogoAgentes tipoCedula;
	private BigDecimal montoFianza;
	private String nombresConyuge;
	private String numeroCedula;
	private String numeroFianza;
	private String observaciones;
	private BigDecimal porcentajeDividendos;
	private String tipoVehiculo;
	private Boolean claveGeneraChequeBoolean;
	private Boolean claveContabilizaComisionBoolean;
	private Boolean claveComisionBoolean;	
	private Boolean claveImprimeEstadoCtaBoolean;
	private Integer idMotivoRechazo;//FIXME
	private Integer diasParaEmitirEnRechazo;
	private String observacionesRechazo;
	private Date fechaAutorizacionRechazo;
	private Date fechaAutorizacionRechazoFin;
	private List<ActualizacionAgente> cambios; 
	private String tipoAgente;
	private String direccionAgente;
	private Long idSuspension;
	private String codigoUsuario;
	private String descripMotivoEstatusAgente;
	private Date fechaAutorizacionAgenteInicio;
	private Date fechaAutorizacionAgenteFin;
	private String fechaAltaString;
	private Long conductoAlta;
	private Boolean envioDeCorreo;
	private ValorCatalogoAgentes clasificacionAgentes;
	
	public static final Long ID_REGISTRO_AUTORIZADO = new Long(1302);
	
	public static enum Situacion {
		
		PENDIENTE_AUTORIZAR("PENDIENTE POR AUTORIZAR"),
		AUTORIZADO("AUTORIZADO"),
		RECHAZADO("RECHAZADO"),
		RECHAZADO_EMISION("RECHAZADO CON EMISION"),
		INACTIVO("INACTIVO"),
		BAJA("BAJA"), 
		SUSPENDIDO("SUSPENDIDO");
		
		private String value;
		
		private Situacion(String value) {
			
			this.value = value;
			
		}
		
		public String getValue() {
			
			return value;
			
		}
		
		public static final String GRUPO_CATALOGO = "Estatus de Agente (Situacion)";
		
	}
	
	
	
    public Agente() {
    }
    
    public Agente(Long gerenciaId, Long oficinaId, Long promotoriaId) {
    	this.promotoria = new Promotoria();
    	this.promotoria.setId(promotoriaId);
    	this.promotoria.setEjecutivo(new Ejecutivo());
    	this.promotoria.getEjecutivo().setId(oficinaId);
    	if(gerenciaId != null){
    		this.promotoria.getEjecutivo().setGerencia(new Gerencia());
    		this.promotoria.getEjecutivo().getGerencia().setId(gerenciaId);
    	}
    }
    

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTOAGENTE_SEQ")
	@SequenceGenerator(name="IDTOAGENTE_SEQ", sequenceName="MIDAS.IDTOAGENTE_SEQ",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="APELLIDOMATERNOCONYUGE")
	@Size(min=0,max=20)
	public String getApellidoMaternoConyuge() {
		return apellidoMaternoConyuge;
	}


	public void setApellidoMaternoConyuge(String apellidoMaternoConyuge) {
		this.apellidoMaternoConyuge = apellidoMaternoConyuge;
	}

	@Column(name="APELLIDOPATERNOCONYUGE")
	@Size(min=0,max=20)
	public String getApellidoPaternoConyuge() {
		return apellidoPaternoConyuge;
	}


	public void setApellidoPaternoConyuge(String apellidoPaternoConyuge) {
		this.apellidoPaternoConyuge = apellidoPaternoConyuge;
	}

	@Column(name="CLAVECOMISION",length=4)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer=4,fraction=0)
	public Integer getClaveComision() {
		return claveComision;
	}


	public void setClaveComision(Integer claveComision) {
		this.claveComision = claveComision;
	}

	@Column(name="CLAVECONTABILIZACOMISION",length=4)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer=4,fraction=0)
	public Integer getClaveContabilizaComision() {
		return claveContabilizaComision;
	}


	public void setClaveContabilizaComision(Integer claveContabilizaComision) {
		this.claveContabilizaComision = claveContabilizaComision;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="IDSITUACIONAGENTE")
	@Valid
	public ValorCatalogoAgentes getTipoSituacion() {
		return tipoSituacion;
	}

	public void setTipoSituacion(ValorCatalogoAgentes tipoSituacion) {
		this.tipoSituacion = tipoSituacion;
	}


	@Column(name="CLAVEGENERACHEQUE",length=4)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer=4,fraction=0)
	public Integer getClaveGeneraCheque() {
		return claveGeneraCheque;
	}


	public void setClaveGeneraCheque(Integer claveGeneraCheque) {
		this.claveGeneraCheque = claveGeneraCheque;
	}

	@Column(name="CLAVEIMPRIMEESTADOCTA",length=4)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer=4,fraction=0)
	public Integer getClaveImprimeEstadoCta() {
		return claveImprimeEstadoCta;
	}


	public void setClaveImprimeEstadoCta(Integer claveImprimeEstadoCta) {
		this.claveImprimeEstadoCta = claveImprimeEstadoCta;
	}

	@Temporal( TemporalType.DATE)
	@Column(name="FECHAACTIVACION")
	public Date getFechaActivacion() {
		return fechaActivacion;
	}


	public void setFechaActivacion(Date fechaActivacion) {
		this.fechaActivacion = fechaActivacion;
	}

	@Temporal( TemporalType.DATE)
	@Column(name="FECHAALTA",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Date getFechaAlta() {
		return fechaAlta;
	}


	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	@Temporal( TemporalType.DATE)
	@Column(name="FECHAAUTORIZACIONCEDULA")
	public Date getFechaAutorizacionCedula() {
		return fechaAutorizacionCedula;
	}


	public void setFechaAutorizacionCedula(Date fechaAutorizacionCedula) {
		this.fechaAutorizacionCedula = fechaAutorizacionCedula;
	}

	@Temporal( TemporalType.DATE)
	@Column(name="FECHAAUTORIZACIONFIANZA")
	public Date getFechaAutorizacionFianza() {
		return fechaAutorizacionFianza;
	}


	public void setFechaAutorizacionFianza(Date fechaAutorizacionFianza) {
		this.fechaAutorizacionFianza = fechaAutorizacionFianza;
	}

	@Temporal( TemporalType.DATE)
	@Column(name="FECHAMATRIMONIO")
	public Date getFechaMatrimonio() {
		return fechaMatrimonio;
	}


	public void setFechaMatrimonio(Date fechaMatrimonio) {
		this.fechaMatrimonio = fechaMatrimonio;
	}

	@Temporal( TemporalType.DATE)
	@Column(name="FECHANACIMIENTOCONYUGE")
	public Date getFechaNacimientoConyuge() {
		return fechaNacimientoConyuge;
	}


	public void setFechaNacimientoConyuge(Date fechaNacimientoConyuge) {
		this.fechaNacimientoConyuge = fechaNacimientoConyuge;
	}

	@Temporal( TemporalType.DATE)
	@Column(name="FECHAVENCIMIENTOCEDULA")
	public Date getFechaVencimientoCedula() {
		return fechaVencimientoCedula;
	}


	public void setFechaVencimientoCedula(Date fechaVencimientoCedula) {
		this.fechaVencimientoCedula = fechaVencimientoCedula;
		if(fechaVencimientoCedula!=null){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");    	
			this.vencimientoCedulaString=sdf.format(fechaVencimientoCedula);
		}
	}

	@Temporal( TemporalType.DATE)
	@Column(name="FECHAVENCIMIENTOFIANZA")
	public Date getFechaVencimientoFianza() {
		return fechaVencimientoFianza;
	}


	public void setFechaVencimientoFianza(Date fechaVencimientoFianza) {
		this.fechaVencimientoFianza = fechaVencimientoFianza;
		if(fechaVencimientoFianza!=null){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");    	
			this.vencimientoFianzaString=sdf.format(fechaVencimientoFianza);
		}
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="IDAFIANZADORA",nullable=true)
	public Afianzadora getAfianzadora() {
		return afianzadora;
	}


	public void setAfianzadora(Afianzadora afianzadora) {
		this.afianzadora = afianzadora;
	}

	@Column(name="IDAGENTE",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getIdAgente() {
		return idAgente;
	}


	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}

	@Column(name="IDAGENTERECLUTADOR",nullable=true)
	public Long getIdAgenteReclutador() {
		return idAgenteReclutador;
	}


	public void setIdAgenteReclutador(Long idAgenteReclutador) {
		this.idAgenteReclutador = idAgenteReclutador;
	}

	@Column(name="IDMOTIVOESTATUSAGENTE",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getIdMotivoEstatusAgente() {
		return idMotivoEstatusAgente;
	}


	public void setIdMotivoEstatusAgente(Long idMotivoEstatusAgente) {
		this.idMotivoEstatusAgente = idMotivoEstatusAgente;
	}

	@Column(name="IDPRIORIDADAGENTE",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getIdPrioridadAgente() {
		return idPrioridadAgente;
	}


	public void setIdPrioridadAgente(Long idPrioridadAgente) {
		this.idPrioridadAgente = idPrioridadAgente;
	}

	@OneToOne(fetch=FetchType.EAGER,targetEntity=Promotoria.class)
	@JoinColumn(name="IDPROMOTORIA",nullable=false)
	public Promotoria getPromotoria() {
		return promotoria;
	}


	public void setPromotoria(Promotoria promotoria) {
		this.promotoria = promotoria;
	}

	@Column(name="IDTIPOAGENTE",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getIdTipoAgente() {
		return idTipoAgente;
	}


	public void setIdTipoAgente(Long idTipoAgente) {
		this.idTipoAgente = idTipoAgente;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDTIPOCEDULAAGENTE")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ValorCatalogoAgentes getTipoCedula() {
		return tipoCedula;
	}


	public void setTipoCedula(ValorCatalogoAgentes tipoCedula) {
		this.tipoCedula = tipoCedula;
	}

	@Column(name="MONTOFIANZA",precision=10,scale=2)
	@Digits(integer=13,fraction=2)
	public BigDecimal getMontoFianza() {
		return montoFianza;
	}


	public void setMontoFianza(BigDecimal montoFianza) {
		this.montoFianza = montoFianza;
	}

	@Column(name="NOMBRESCONYUGE")
	@Size(min=0,max=40)
	public String getNombresConyuge() {
		return nombresConyuge;
	}


	public void setNombresConyuge(String nombresConyuge) {
		this.nombresConyuge = nombresConyuge;
	}

	@Column(name="NUMEROCEDULA")
	public String getNumeroCedula() {
		return numeroCedula;
	}


	public void setNumeroCedula(String numeroCedula) {
		this.numeroCedula = numeroCedula;
	}

	@Column(name="NUMEROFIANZA")
	public String getNumeroFianza() {
		return numeroFianza;
	}


	public void setNumeroFianza(String numeroFianza) {
		this.numeroFianza = numeroFianza;
	}

	@Column(name="OBSERVACIONES",length=200)
	@Size(min=0,max=200)
	public String getObservaciones() {
		return observaciones;
	}


	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	@Column(name="PORCENTAJEDIVIDENDOS",precision=8,scale=4)
	@Digits(integer=13,fraction=2)
//	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public BigDecimal getPorcentajeDividendos() {
		return porcentajeDividendos;
	}


	public void setPorcentajeDividendos(BigDecimal porcentajeDividendos) {
		this.porcentajeDividendos = porcentajeDividendos;
	}

	@Column(name="TIPOVEHICULO",length=200)
	@Size(min=0,max=200)
	public String getTipoVehiculo() {
		return tipoVehiculo;
	}


	public void setTipoVehiculo(String tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}

	@OneToOne(fetch=FetchType.LAZY)// cascade = CascadeType.REMOVE
	@JoinColumn(name="IDPERSONA")
	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}
	
	@Transient
	public Boolean getClaveGeneraChequeBoolean() {
		return claveGeneraChequeBoolean;
	}


	public void setClaveGeneraChequeBoolean(Boolean claveGeneraChequeBoolean) {
		this.claveGeneraChequeBoolean = claveGeneraChequeBoolean;
        if(this.claveGeneraChequeBoolean){
              this.claveGeneraCheque=1;
        }else{
              this.claveGeneraCheque=0;
        }		
	}

	@Transient
	public Boolean getClaveContabilizaComisionBoolean() {
		return claveContabilizaComisionBoolean;
	}


	public void setClaveContabilizaComisionBoolean(
			Boolean claveContabilizaComisionBoolean) {
		this.claveContabilizaComisionBoolean = claveContabilizaComisionBoolean;		
        if(this.claveContabilizaComisionBoolean){
              this.claveContabilizaComision=1;
        }else{
              this.claveContabilizaComision=0;
        }		
	}

	@Transient
	public Boolean getClaveComisionBoolean() {
		return claveComisionBoolean;
	}


	public void setClaveComisionBoolean(Boolean claveComisionBoolean) {
		this.claveComisionBoolean = claveComisionBoolean;
		if(this.claveComisionBoolean){
            this.claveComision=1;
		}else{
            this.claveComision=0;
		}		
	}

	@Transient
	public Boolean getClaveImprimeEstadoCtaBoolean() {
		return claveImprimeEstadoCtaBoolean;
	}


	public void setClaveImprimeEstadoCtaBoolean(Boolean claveImprimeEstadoCtaBoolean) {
		this.claveImprimeEstadoCtaBoolean = claveImprimeEstadoCtaBoolean;
		if(this.claveImprimeEstadoCtaBoolean){
            this.claveImprimeEstadoCta=1;
		}else{
            this.claveImprimeEstadoCta=0;
		}	
	}

	
	@Column(name="IDMOTIVORECHAZO",length=4)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer=4,fraction=0)
	public Integer getIdMotivoRechazo() {
		return idMotivoRechazo;
	}

	public void setIdMotivoRechazo(Integer idMotivoRechazo) {
		this.idMotivoRechazo = idMotivoRechazo;
	}
	
	@Column(name="DIASPARAEMITIRENRECHAZO",length=4)
	@Digits(integer=4,fraction=0)
	public Integer getDiasParaEmitirEnRechazo() {
		return diasParaEmitirEnRechazo;
	}


	public void setDiasParaEmitirEnRechazo(Integer diasParaEmitirEnRechazo) {
		this.diasParaEmitirEnRechazo = diasParaEmitirEnRechazo;
	}


	@Column(name="OBSERVACIONESRECHAZO")
	@Size(min=0,max=2000)
	public String getObservacionesRechazo() {
		return observacionesRechazo;
	}


	public void setObservacionesRechazo(String observacionesRechazo) {
		this.observacionesRechazo = observacionesRechazo;
	}

	@Temporal( TemporalType.DATE)
	@Column(name="FECHAAUTORIZACIONRECHAZO",nullable=false)
	public Date getFechaAutorizacionRechazo() {
		return fechaAutorizacionRechazo;
	}


	public void setFechaAutorizacionRechazo(Date fechaAutorizacionRechazo) {
		this.fechaAutorizacionRechazo = fechaAutorizacionRechazo;
	}

	@Column(name="CONDUCTO_ALTA")
	public Long getConductoAlta() {
		return conductoAlta;
	}

	public void setConductoAlta(Long conductoAlta) {
		this.conductoAlta = conductoAlta;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}


	@Override
	public String getValue() {
		return null;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	@OneToMany(fetch=FetchType.EAGER,targetEntity=ActualizacionAgente.class)
	@JoinColumn(name="ID")
	public List<ActualizacionAgente> getCambios() {
		return cambios;
	}


	public void setCambios(List<ActualizacionAgente> cambios) {
		this.cambios = cambios;
	}
	@Transient
	public Date getFechaAltaFin() {
		return fechaAltaFin;
	}

	public void setFechaAltaFin(Date fechaAltaFin) {
		this.fechaAltaFin = fechaAltaFin;
	}
	@Transient
	public Date getFechaAutorizacionFianzaFin() {
		return fechaAutorizacionFianzaFin;
	}

	public void setFechaAutorizacionFianzaFin(Date fechaAutorizacionFianzaFin) {
		this.fechaAutorizacionFianzaFin = fechaAutorizacionFianzaFin;
	}
	
	@Column(name="IDSOLICSUSPENSION")
	public Long getIdSuspension() {
		return idSuspension;
	}

	public void setIdSuspension(Long idSuspension) {
		this.idSuspension = idSuspension;
	}

	@Transient
	public String getTipoAgente() {
		return tipoAgente;
	}

	public void setTipoAgente(String tipoAgente) {
		this.tipoAgente = tipoAgente;
	}

	@Transient
	public String getDireccionAgente() {
		return direccionAgente;
	}

	public void setDireccionAgente(String direccionAgente) {
		this.direccionAgente = direccionAgente;
	}

	@Transient
	public String getVencimientoCedulaString() {
		return vencimientoCedulaString;
	}

	public void setVencimientoCedulaString(String vencimientoCedulaString) {
		this.vencimientoCedulaString = vencimientoCedulaString;
	}

	@Transient
	public String getVencimientoFianzaString() {
		return vencimientoFianzaString;
	}

	public void setVencimientoFianzaString(String vencimientoFianzaString) {
		this.vencimientoFianzaString = vencimientoFianzaString;
	}
	
	public String getCodigoUsuario() {
		return codigoUsuario;
	}
	
	public void setCodigoUsuario(String codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}
	@Transient
	public Date getFechaAutorizacionRechazoFin() {
		return fechaAutorizacionRechazoFin;
	}

	public void setFechaAutorizacionRechazoFin(Date fechaAutorizacionRechazoFin) {
		this.fechaAutorizacionRechazoFin = fechaAutorizacionRechazoFin;
	}

	@Transient
	public String getDescripMotivoEstatusAgente() {
		return descripMotivoEstatusAgente;
	}

	public void setDescripMotivoEstatusAgente(String descripMotivoEstatusAgente) {
		this.descripMotivoEstatusAgente = descripMotivoEstatusAgente;
	}

	@Transient
	public Date getFechaAutorizacionAgenteInicio() {
		return fechaAutorizacionAgenteInicio;
	}

	public void setFechaAutorizacionAgenteInicio(Date fechaAutorizacionAgenteInicio) {
		this.fechaAutorizacionAgenteInicio = fechaAutorizacionAgenteInicio;
	}

	@Transient
	public Date getFechaAutorizacionAgenteFin() {
		return fechaAutorizacionAgenteFin;
	}

	public void setFechaAutorizacionAgenteFin(Date fechaAutorizacionAgenteFin) {
		this.fechaAutorizacionAgenteFin = fechaAutorizacionAgenteFin;
	}

	@Transient
	public String getFechaAltaString() {
		return fechaAltaString;
	}

	public void setFechaAltaString(String fechaAltaString) {
		this.fechaAltaString = fechaAltaString;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Agente))
			return false;
		Agente other = (Agente) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Transient
	public Boolean getEnvioDeCorreo() {
		return envioDeCorreo;
	}

	public void setEnvioDeCorreo(Boolean envioDeCorreo) {
		this.envioDeCorreo = envioDeCorreo;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="CLASIFICACIONAGENTES")
	@Valid
	public ValorCatalogoAgentes getClasificacionAgentes() {
		return clasificacionAgentes;
	}

	public void setClasificacionAgentes(ValorCatalogoAgentes clasificacionAgentes) {
		this.clasificacionAgentes = clasificacionAgentes;
	}

	
	
}