package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Table(name="tocalculobono_ejecuciones", schema="MIDAS")
@Entity(name="CalculoBonoEjecuciones")
public class CalculoBonoEjecuciones implements Serializable, Entidad{

		private static final long serialVersionUID = 1L;
		private Long id;
		private Long esNegocioEspecial;
		private Long idProgramacion;
		private Long idConfiguracion;
		private String nombreConfiguracionBono;
		private ValorCatalogoAgentes modoEjecucion;
		private ValorCatalogoAgentes estatusEjecucion;
		private Long idCalculoBono;
		private Date fechaEjecucion;
		private Date fechaFinEjecucion;
		private String usuario;
		
		@Id
		@SequenceGenerator(name="IDTOEJECUCIONBONO_SEQ",sequenceName="MIDAS.IDTOEJECUCIONBONO_SEQ",allocationSize=1)
		@GeneratedValue(generator="IDTOEJECUCIONBONO_SEQ",strategy=GenerationType.SEQUENCE)
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		
		@Column(name="esNegocioEspecial")		
		public Long getEsNegocioEspecial() {
			return esNegocioEspecial;
		}
		public void setEsNegocioEspecial(Long esNegocioEspecial) {
			this.esNegocioEspecial = esNegocioEspecial;
		}
		
		@Column(name="idProgramacion")
		public Long getIdProgramacion() {
			return idProgramacion;
		}
		public void setIdProgramacion(Long idProgramacion) {
			this.idProgramacion = idProgramacion;
		}
		
		@Column(name="idConfiguracion")
		public Long getIdConfiguracion() {
			return idConfiguracion;
		}
		public void setIdConfiguracion(Long idConfiguracion) {
			this.idConfiguracion = idConfiguracion;
		}
		@Column(name="nombreConfiguracionBono")
		public String getNombreConfiguracionBono() {
			return nombreConfiguracionBono;
		}
		public void setNombreConfiguracionBono(String nombreConfiguracionBono) {
			this.nombreConfiguracionBono = nombreConfiguracionBono;
		}
		
		@ManyToOne(fetch=FetchType.LAZY,targetEntity=ValorCatalogoAgentes.class)
		@JoinColumn(name="idModoEjecucion")
		public ValorCatalogoAgentes getModoEjecucion() {
			return modoEjecucion;
		}
		public void setModoEjecucion(ValorCatalogoAgentes modoEjecucion) {
			this.modoEjecucion = modoEjecucion;
		}
		
		@ManyToOne(fetch=FetchType.LAZY,targetEntity=ValorCatalogoAgentes.class)
		@JoinColumn(name="idEstatusEjecucion")
		public ValorCatalogoAgentes getEstatusEjecucion() {
			return estatusEjecucion;
		}
		public void setEstatusEjecucion(ValorCatalogoAgentes estatusEjecucion) {
			this.estatusEjecucion = estatusEjecucion;
		}
		
		@Column(name="idCalculoBono")
		public Long getIdCalculoBono() {
			return idCalculoBono;
		}
		public void setIdCalculoBono(Long idCalculoBono) {
			this.idCalculoBono = idCalculoBono;
		}
		
		@Temporal(TemporalType.DATE)
		@Column(name="fechaEjecucion")
		public Date getFechaEjecucion() {
			return fechaEjecucion;
		}
		public void setFechaEjecucion(Date fechaEjecucion) {
			this.fechaEjecucion = fechaEjecucion;
		}
		
		@Temporal(TemporalType.TIMESTAMP)
		@Column(name="fechaFinalizaCalculo")
		public Date getFechaFinEjecucion() {
			return fechaFinEjecucion;
		}
		public void setFechaFinEjecucion(Date fechaFinEjecucion) {
			this.fechaFinEjecucion = fechaFinEjecucion;
		}
		
		@Column(name="usuario")		
		public String getUsuario() {
			return usuario;
		}
		public void setUsuario(String usuario) {
			this.usuario = usuario;
		}
		@Override
		public <K> K getKey() {
			// TODO Auto-generated method stub
			return null;
		}
		@Override
		public String getValue() {
			// TODO Auto-generated method stub
			return null;
		}
		@Override
		public <K> K getBusinessKey() {
			// TODO Auto-generated method stub
			return null;
		}
		
		
		
}
