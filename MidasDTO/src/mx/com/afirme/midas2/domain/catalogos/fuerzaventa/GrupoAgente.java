package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Size;
import javax.validation.constraints.Digits;

import org.directwebremoting.annotations.DataTransferObject;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

/**
 * 
 * @author Efren Rodriguez
 *
 */

@Entity(name="grupoAgenteDTO")
@Table(name="TOAGENTE_GRUPO",schema="MIDAS")
@DataTransferObject
public class GrupoAgente implements Serializable,Entidad {
	
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Short estatus;
	private String descripcion;
	private String nombre;
	private Date fechaCreacion;
	private Date fechaModificacion;

	
	/*GETTERS AND SETTERS*/
	@Id 
	@SequenceGenerator(name = "TOAGENTE_GRUPO_ID_SEQ_GENERADOR",allocationSize = 1, sequenceName = "MIDAS.TOAGENTE_GRUPO_SEQ")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TOAGENTE_GRUPO_ID_SEQ_GENERADOR")
    @Column(name="ID", unique=true, nullable=false, precision = 22, scale = 0)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="ESTATUS",length=1)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer=1,fraction=0)
	public Short getEstatus() {
		return estatus;
	}

	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}
	
	@Column(name="DESCRIPCION")
	@Size(min=0,max=2000)
	public String getDescripcion() {
		return descripcion;
	}
	

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Column(name="NOMBRE")
	@Size(min=0,max=250)
	public String getNombre() {
		return nombre;
	}
	

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREACION")
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_MODIFICACION")
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	
	/*methods required for the implement*/

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

}
