package mx.com.afirme.midas2.domain.catalogos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
/**
 * LLave primaria de domicilio
 * @author vmhersil
 *
 */
@Embeddable
public class DomicilioPk implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7370036523307778448L;
	private Long idDomicilio;
	private Long idPersona;
	@Column(name="idDomicilio",nullable=false)
	public Long getIdDomicilio() {
		return idDomicilio;
	}
	public void setIdDomicilio(Long idDomicilio) {
		this.idDomicilio = idDomicilio;
	}
	@Column(name="idPersona")
	public Long getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}
}
