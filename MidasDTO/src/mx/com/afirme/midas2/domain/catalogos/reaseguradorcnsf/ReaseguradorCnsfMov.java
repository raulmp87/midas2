package mx.com.afirme.midas2.domain.catalogos.reaseguradorcnsf;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name="ReaseguradorCnsfMov")
@Table(name="CNSFREASEGURADORMOV",schema="MIDAS")
public class ReaseguradorCnsfMov implements Serializable, Entidad{

	private static final long serialVersionUID = 1L;
	
	private BigDecimal idreaseguradormov;
	private BigDecimal idreasegurador;
	private Date fechacorte;
	private BigDecimal idagencia;
	private BigDecimal idcalificacion;
	private String usuario;
	private String claveReasegurador;
	private String claveReaseguradorAnt;
	private String nombreReasegurador;
	private String nombreAgencia;
	private String calificacion;
	private BigDecimal idTcReasegurador;
	
	public ReaseguradorCnsfMov() {
		idagencia = new BigDecimal(0);
		idcalificacion =  new BigDecimal(0000);
	}
	
	@Id
	@Column(name="ID")
	@SequenceGenerator(name = "ID_SEQ_GENERADORREAS", allocationSize = 1, sequenceName = "MIDAS.CNSFREASEGURADORMOV_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_SEQ_GENERADORREAS")
	public BigDecimal getIdreaseguradormov() {
		return idreaseguradormov;
	}

	public void setIdreaseguradormov(BigDecimal idreaseguradormov) {
		this.idreaseguradormov = idreaseguradormov;
	}
	
	@Column(name="IDREASEGURADOR")
	public BigDecimal getIdreasegurador() {
		return idreasegurador;
	}

	public void setIdreasegurador(BigDecimal idreasegurador) {
		this.idreasegurador = idreasegurador;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHACORTE")
	public Date getFechacorte() {
		return fechacorte;
	}

	public void setFechacorte(Date fechacorte) {
		this.fechacorte = fechacorte;
	}
	
	@Column(name="IDAGENCIA")
	public BigDecimal getIdagencia() {
		return idagencia;
	}

	public void setIdagencia(BigDecimal idagencia) {
		this.idagencia = idagencia;
	}
	
	@Column(name="IDCALIFICACION")
	public BigDecimal getIdcalificacion() {
		return idcalificacion;
	}

	public void setIdcalificacion(BigDecimal idcalificacion) {
		this.idcalificacion = idcalificacion;
	}
	
	@Column(name="USUARIO")
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}


	@Transient
	public String getClaveReasegurador() {
		return claveReasegurador;
	}

	public void setClaveReasegurador(String claveReasegurador) {
		this.claveReasegurador = claveReasegurador;
	}
	
	@Transient
	public String getClaveReaseguradorAnt() {
		return claveReaseguradorAnt;
	}

	public void setClaveReaseguradorAnt(String claveReaseguradorAnt) {
		this.claveReaseguradorAnt = claveReaseguradorAnt;
	}
	@Transient
	public String getNombreReasegurador() {
		return nombreReasegurador;
	}

	public void setNombreReasegurador(String nombreReasegurador) {
		this.nombreReasegurador = nombreReasegurador;
	}	
	
	@Transient
	public String getNombreAgencia() {
		return nombreAgencia;
	}

	public void setNombreAgencia(String nombreAgencia) {
		this.nombreAgencia = nombreAgencia;
	}
	
	@Transient
	public String getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(String calificacion) {
		this.calificacion = calificacion;
	}
		
	@Transient
	public BigDecimal getIdTcReasegurador() {
		return idTcReasegurador;
	}

	public void setIdTcReasegurador(BigDecimal idTcReasegurador) {
		this.idTcReasegurador = idTcReasegurador;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.idreaseguradormov;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}

}
