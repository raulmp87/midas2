package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import org.eclipse.persistence.annotations.Customizer;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

@Entity(name="ProductoBancarioPromotoria")
@Table(schema="MIDAS",name="toProductoBancarioProm")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class ProductoBancarioPromotoria implements Serializable,Entidad{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2913059254542594021L;
	private Long id;
	private Promotoria promotoria;
	private ProductoBancario productoBancario;
	private Integer claveDefault;
	private BigDecimal montoCredito;
	private BigDecimal montoPago;
	private String numeroClabe;
	private String numeroCuenta;
	private Integer numeroPlazos;
	private String plaza;
	private String sucursal;
	private Boolean claveDefaultBoolean;
	private Long idProductoBancario; 
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idToProductoBancProm_seq")
	@SequenceGenerator(name="idToProductoBancProm_seq", sequenceName="MIDAS.idToProductoBancProm_seq",allocationSize=1)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@ManyToOne(fetch=FetchType.LAZY,targetEntity=Promotoria.class)
	@JoinColumn(name="Promotoria_id")
	public Promotoria getPromotoria() {
		return promotoria;
	}

	public void setPromotoria(Promotoria promotoria) {
		this.promotoria = promotoria;
	}
	
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ProductoBancario.class)
	@JoinColumn(name="ProductoBancario_id")
	public ProductoBancario getProductoBancario() {
		return productoBancario;
	}

	public void setProductoBancario(ProductoBancario productoBancario) {
		this.productoBancario = productoBancario;
	}
	
	@Column(name="CLAVEDEFAULT",precision=4)
	@Digits(integer=4,fraction=0)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Integer getClaveDefault() {
		return claveDefault;
	}

	public void setClaveDefault(Integer claveDefault) {
		this.claveDefault = claveDefault;
	}

	@Column(name="MONTOCREDITO",precision=10,scale=2)
	@Digits(integer=10,fraction=3)
//	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public BigDecimal getMontoCredito() {
		return montoCredito;
	}

	public void setMontoCredito(BigDecimal montoCredito) {
		this.montoCredito = montoCredito;
	}

	@Column(name="MONTOPAGO",precision=10,scale=2)
	@Digits(integer=10,fraction=3)
//	@NotNull(message="{com.afirme.midas2.requerido}")
	public BigDecimal getMontoPago() {
		return montoPago;
	}

	public void setMontoPago(BigDecimal montoPago) {
		this.montoPago = montoPago;
	}

	@Column(name="NUMEROCLABE",precision=20)
//	@Digits(integer=20,fraction=0)
//	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public String getNumeroClabe() {
		return numeroClabe;
	}

	public void setNumeroClabe(String numeroClabe) {
		this.numeroClabe = numeroClabe;
	}
	
	@Column(name="NUMEROCUENTA",precision=20)
//	@Digits(integer=20,fraction=0)
//	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	@Column(name="NUMEROPLAZOS",precision=4)
	@Digits(integer=4,fraction=0)
	public Integer getNumeroPlazos() {
		return numeroPlazos;
	}

	public void setNumeroPlazos(Integer numeroPlazos) {
		this.numeroPlazos = numeroPlazos;
	}
	
	@Column(name="PLAZA",length=40)
//	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public String getPlaza() {
		return plaza;
	}

	public void setPlaza(String plaza) {
		this.plaza = plaza;
	}
	
	@Column(name="SUCURSAL",length=40)
//	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public String getSucursal() {
		return sucursal;
	}
	
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	
	@Transient
	public Boolean getClaveDefaultBoolean() {
		return claveDefaultBoolean;
	}

	public void setClaveDefaultBoolean(Boolean claveDefaultBoolean) {
		this.claveDefaultBoolean = claveDefaultBoolean;
		if(this.claveDefaultBoolean){
			this.claveDefault=1;
		}else{
			this.claveDefault=0;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	@Transient
	public Long getIdProductoBancario() {
		return idProductoBancario;
	}

	public void setIdProductoBancario(Long idProductoBancario) {
		this.idProductoBancario = idProductoBancario;
	}
	
}
