package mx.com.afirme.midas2.domain.catalogos.fuerzaventa.mediciones.conf;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.base.Scrollable;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;

@Entity
@Table(name="TOPROMOTORIA_CONF_MEDICIONES",schema="MIDAS")
public class ConfiguracionMedicionesPromotoria extends Scrollable implements Serializable,Entidad {

	
	private static final long serialVersionUID = 1645866787161631691L;

	private Long id;
	
	private Promotoria promotoria;
	
	private String email;
		
	private Boolean habilitado;
	
	
	@Id
	@SequenceGenerator(name="TOPROM_CONF_MED_SEQGEN", sequenceName="TOPROM_CONF_MEDICIONES_SEQ", schema="MIDAS", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOPROM_CONF_MED_SEQGEN")
	@Column(name = "ID", nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return id;
	}

	
	public void setId(Long id) {
		this.id = id;
	}
		

	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "PROMOTORIA_ID", unique=true, nullable=false, updatable=false)
	public Promotoria getPromotoria() {
		return promotoria;
	}


	public void setPromotoria(Promotoria promotoria) {
		this.promotoria = promotoria;
	}

	
	@Column(name = "EMAIL", length = 100)
	public String getEmail() {
		return email;
	}

	
	public void setEmail(String email) {
		this.email = email;
	}

	
	@Column(name="HABILITADO", precision=1, scale=0)
	public Boolean getHabilitado() {
		return habilitado;
	}

	
	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}

	
	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	

}
