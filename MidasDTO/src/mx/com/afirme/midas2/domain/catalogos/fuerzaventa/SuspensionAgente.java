package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name="SuspensionAgente")
@Table(name="TOSUSPENSIONAGENTE", schema="MIDAS")
public class SuspensionAgente implements Serializable,Entidad{

	private static final long serialVersionUID = 1L;
	private Long id;
	private Agente agente;
	private Date fechaSolicitud;
	private String fechaSolicitudString;
	private ValorCatalogoAgentes estatusMovimiento;
	private String nombreSolicitante;
	private ValorCatalogoAgentes estatusAgtSolicitado;
	private ValorCatalogoAgentes motivoEstatus;
	private String observaciones;
	private Date fechaInicio;
	private Date fechaFin;

	public static final String CARPETA_SUSPENSIONES = "SUSPENSIONES";
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="idToSuspensionAgte_seq")
	@SequenceGenerator(name="idToSuspensionAgte_seq",sequenceName="MIDAS.idToSuspensionAgte_seq",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=Agente.class)
	@JoinColumn(name="IDAGENTE")
	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHASOLICITUD")
	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}

	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
		this.fechaSolicitudString = sdf.format(fechaSolicitud);
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="claveEstatusMovimiento")
	public ValorCatalogoAgentes getEstatusMovimiento() {
		return estatusMovimiento;
	}

	public void setEstatusMovimiento(ValorCatalogoAgentes estatusMovimiento) {
		this.estatusMovimiento = estatusMovimiento;
	}

	@Column(name="NOMBRESOLICITANTE")
	public String getNombreSolicitante() {
		return nombreSolicitante;
	}

	public void setNombreSolicitante(String nombreSolicitante) {
		this.nombreSolicitante = nombreSolicitante;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="estatusAgtSolicitado")
	public ValorCatalogoAgentes getEstatusAgtSolicitado() {
		return estatusAgtSolicitado;
	}

	public void setEstatusAgtSolicitado(ValorCatalogoAgentes estatusAgtSolicitado) {
		this.estatusAgtSolicitado = estatusAgtSolicitado;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="claveMotivoEstatus")
	public ValorCatalogoAgentes getMotivoEstatus() {
		return motivoEstatus;
	}

	public void setMotivoEstatus(ValorCatalogoAgentes motivoEstatus) {
		this.motivoEstatus = motivoEstatus;
	}

	@Column(name="OBSERVACIONES")
	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	
	@Transient
	public String getFechaSolicitudString() {
		return fechaSolicitudString;
	}

	public void setFechaSolicitudString(String fechaSolicitudString) {
		this.fechaSolicitudString = fechaSolicitudString;
	}

	@Transient
	public Date getFechaInicio() {
		return fechaInicio;
	}

	
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	@Transient
	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}
