package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.directwebremoting.annotations.DataTransferObject;
import org.eclipse.persistence.annotations.Customizer;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.validator.group.CapturaManualClavePromotoriaChecks;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

/**
 * The persistent class for the TOPROMOTORIA database table.
 * 
 */
@Entity(name = "Promotoria")
@Table(name = "TOPROMOTORIA",schema="MIDAS")
@DataTransferObject
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizer.class)
public class Promotoria implements Serializable, Entidad {
	
	private static final long serialVersionUID = 827704998696673144L;
	private Long id;
	private Long idPromotoria;
	private Agente agentePromotor;
	private Integer claveEstatus;
	private String correoElectronico;
	private String descripcion;
	private ValorCatalogoAgentes tipoPromotoria;
	private ValorCatalogoAgentes agrupadorPromotoria;
	private Ejecutivo ejecutivo;
	private Date fechaInicio;
	private Date fechaFin;
	private Persona personaResponsable;
	private Long idAgentePromotor;
	private boolean capturaManualClave;
	
	public static final Integer ESTATUS_ACTIVO = 1;
	public static final Integer ESTATUS_INACTIVO = 0;

	public Promotoria() {
	}
	
	public Promotoria(Long id) {
		this.id = id;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOPROMOTORIA_SEQ")
	@SequenceGenerator(name = "IDTOPROMOTORIA_SEQ", sequenceName = "MIDAS.IDTOPROMOTORIA_SEQ",allocationSize=1)
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	// bi-directional many-to-one association to Ejecutivo
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "EJECUTIVO_ID")
	@NotNull(groups = EditItemChecks.class, message = "{com.afirme.midas2.requerido}")
	public Ejecutivo getEjecutivo() {
		return this.ejecutivo;
	}

	public void setEjecutivo(Ejecutivo ejecutivo) {
		this.ejecutivo = ejecutivo;
	}

	@Transient
	public Agente getAgentePromotor() {
		return agentePromotor;
	}

	public void setAgentePromotor(Agente agentePromotor) {
		this.agentePromotor = agentePromotor;
	}

	@Column(name = "CLAVEESTATUS", nullable = false)
	@NotNull(groups = EditItemChecks.class, message = "{com.afirme.midas2.requerido}")
	public Integer getClaveEstatus() {
		return claveEstatus;
	}

	public void setClaveEstatus(Integer claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	@Column(name = "CORREOELECTRONICO", length = 80)
	@Size(groups = EditItemChecks.class, min = 0, max = 80)
	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	@Column(name = "DESCRIPCION", nullable = false, length = 200)
	@Size(groups = EditItemChecks.class, min = 1, max = 200)
	@NotNull(groups = EditItemChecks.class, message = "{com.afirme.midas2.requerido}")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name = "IDAGRUPADORPROMOTORIA")
	@NotNull(groups = EditItemChecks.class, message = "{com.afirme.midas2.requerido}")
	public ValorCatalogoAgentes getAgrupadorPromotoria() {
		return agrupadorPromotoria;
	}

	public void setAgrupadorPromotoria(ValorCatalogoAgentes agrupadorPromotoria) {
		this.agrupadorPromotoria = agrupadorPromotoria;
	}

	@Column(name = "IDPROMOTORIA", nullable = true)
	@NotNull(groups = CapturaManualClavePromotoriaChecks.class, message = "{com.afirme.midas2.requerido}")
	@Digits(integer = 10, fraction= 0,groups=CapturaManualClavePromotoriaChecks.class,message="{javax.validation.constraints.Digits.message}")
	public Long getIdPromotoria() {
		return idPromotoria;
	}

	public void setIdPromotoria(Long idPromotoria) {
		this.idPromotoria = idPromotoria;
	}
	
	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name = "IDTIPOPROMOTORIA")
	@NotNull(groups = EditItemChecks.class, message = "{com.afirme.midas2.requerido}")
	public ValorCatalogoAgentes getTipoPromotoria() {
		return tipoPromotoria;
	}

	public void setTipoPromotoria(ValorCatalogoAgentes tipoPromotoria) {
		this.tipoPromotoria = tipoPromotoria;
	}
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="IDPERSONARESPONSABLE",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Persona getPersonaResponsable() {
		return personaResponsable;
	}

	public void setPersonaResponsable(Persona personaResponsable) {
		this.personaResponsable = personaResponsable;
	}

	@Column(name="AGENTEPROMOTOR_ID", nullable = true)
	public Long getIdAgentePromotor() {
		return idAgentePromotor;
	}

	public void setIdAgentePromotor(Long idAgentePromotor) {
		this.idAgentePromotor = idAgentePromotor;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	@Transient
	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	@Transient
	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	@Transient
	public boolean isCapturaManualClave() {
		return capturaManualClave;
	}

	public void setCapturaManualClave(boolean capturaManualClave) {
		this.capturaManualClave = capturaManualClave;
	}
	
	
}