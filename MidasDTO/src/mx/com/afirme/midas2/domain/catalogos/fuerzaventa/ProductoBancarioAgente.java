package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

import org.eclipse.persistence.annotations.Customizer;


/**
 * The persistent class for the TOPRODUCTOBANCARIOAGENTE database table.
 * 
 */
@Entity(name="ProductoBancarioAgente")
@Table(name="TOPRODUCTOBANCARIOAGENTE", schema="MIDAS")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)//HistoryCustomizer
public class ProductoBancarioAgente implements Serializable,Entidad {
	private static final long serialVersionUID = 1L;
	private Long id;
	private Agente agente;
	private Integer claveDefault;
	private BigDecimal montoCredito;
	private BigDecimal montoPago;
	private String numeroClabe;
	private String numeroCuenta;
	private Integer numeroPlazos;
	private String plaza;
	private String sucursal;
	private ProductoBancario productoBancario;
	private Boolean claveDefaultBoolean;

    public ProductoBancarioAgente() {
    }


	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTOPRODUCTOBANCARIOAGENTE_SEQ")
	@SequenceGenerator(name="IDTOPRODUCTOBANCARIOAGENTE_SEQ", sequenceName="MIDAS.IDTOPRODUCTOBANCARIOAGENTE_SEQ",allocationSize=1)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="AGENTE_ID")
	@NotNull(message="{com.afirme.midas2.requerido}")
	public Agente getAgente() {
		return this.agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	//bi-directional many-to-one association to ProductoBancario
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PRODUCTOBANCARIO_ID",nullable=false)
	@NotNull(message="{com.afirme.midas2.requerido}")
	public ProductoBancario getProductoBancario() {
		return this.productoBancario;
	}

	public void setProductoBancario(ProductoBancario productoBancario) {
		this.productoBancario = productoBancario;
	}

	@Column(name="CLAVEDEFAULT",precision=4)
	@Digits(integer=4,fraction=0)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Integer getClaveDefault() {
		return claveDefault;
	}


	public void setClaveDefault(Integer claveDefault) {
		this.claveDefault = claveDefault;
	}

	@Column(name="MONTOCREDITO",precision=10,scale=2)
	@Digits(integer=10,fraction=3)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public BigDecimal getMontoCredito() {
		return montoCredito;
	}


	public void setMontoCredito(BigDecimal montoCredito) {
		this.montoCredito = montoCredito;
	}

	@Column(name="MONTOPAGO",precision=10,scale=2)
	@Digits(integer=10,fraction=3)
	@NotNull(message="{com.afirme.midas2.requerido}")
	public BigDecimal getMontoPago() {
		return montoPago;
	}


	public void setMontoPago(BigDecimal montoPago) {
		this.montoPago = montoPago;
	}

	@Column(name="NUMEROCLABE",precision=20)
//	@Digits(integer=20,fraction=0)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public String getNumeroClabe() {
		return numeroClabe;
	}


	public void setNumeroClabe(String numeroClabe) {
		this.numeroClabe = numeroClabe;
	}
	
	@Column(name="NUMEROCUENTA",precision=20)
//	@Digits(integer=20,fraction=0)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public String getNumeroCuenta() {
		return numeroCuenta;
	}


	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	@Column(name="NUMEROPLAZOS",precision=4)
	@Digits(integer=4,fraction=0)
	public Integer getNumeroPlazos() {
		return numeroPlazos;
	}


	public void setNumeroPlazos(Integer numeroPlazos) {
		this.numeroPlazos = numeroPlazos;
	}
	
	@Column(name="PLAZA",length=40)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public String getPlaza() {
		return plaza;
	}


	public void setPlaza(String plaza) {
		this.plaza = plaza;
	}
	
	@Column(name="SUCURSAL",length=40)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public String getSucursal() {
		return sucursal;
	}

	
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	
	@Transient
	public Boolean getClaveDefaultBoolean() {
		return claveDefaultBoolean;
	}


	public void setClaveDefaultBoolean(Boolean claveDefaultBoolean) {
		this.claveDefaultBoolean = claveDefaultBoolean;
		if(this.claveDefaultBoolean){
			this.claveDefault=1;
		}else{
			this.claveDefault=0;
		}
	}


	@Override
	public Long getKey() {
		return id;
	}


	@Override
	public String getValue() {
		return null;
	}


	@Override
	public Long getBusinessKey() {
		return id;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((agente == null) ? 0 : agente.hashCode());
		result = prime * result
				+ ((claveDefault == null) ? 0 : claveDefault.hashCode());
		result = prime
				* result
				+ ((claveDefaultBoolean == null) ? 0 : claveDefaultBoolean
						.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((montoCredito == null) ? 0 : montoCredito.hashCode());
		result = prime * result
				+ ((montoPago == null) ? 0 : montoPago.hashCode());
		result = prime * result
				+ ((numeroClabe == null) ? 0 : numeroClabe.hashCode());
		result = prime * result
				+ ((numeroCuenta == null) ? 0 : numeroCuenta.hashCode());
		result = prime * result
				+ ((numeroPlazos == null) ? 0 : numeroPlazos.hashCode());
		result = prime * result + ((plaza == null) ? 0 : plaza.hashCode());
		result = prime
				* result
				+ ((productoBancario == null) ? 0 : productoBancario.hashCode());
		result = prime * result
				+ ((sucursal == null) ? 0 : sucursal.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ProductoBancarioAgente))
			return false;
		ProductoBancarioAgente other = (ProductoBancarioAgente) obj;
		if (agente == null) {
			if (other.agente != null)
				return false;
		} else if (!agente.equals(other.agente))
			return false;
		if (claveDefault == null) {
			if (other.claveDefault != null)
				return false;
		} else if (!claveDefault.equals(other.claveDefault))
			return false;
		if (claveDefaultBoolean == null) {
			if (other.claveDefaultBoolean != null)
				return false;
		} else if (!claveDefaultBoolean.equals(other.claveDefaultBoolean))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (montoCredito == null) {
			if (other.montoCredito != null)
				return false;
		} else if (!montoCredito.equals(other.montoCredito))
			return false;
		if (montoPago == null) {
			if (other.montoPago != null)
				return false;
		} else if (!montoPago.equals(other.montoPago))
			return false;
		if (numeroClabe == null) {
			if (other.numeroClabe != null)
				return false;
		} else if (!numeroClabe.equals(other.numeroClabe))
			return false;
		if (numeroCuenta == null) {
			if (other.numeroCuenta != null)
				return false;
		} else if (!numeroCuenta.equals(other.numeroCuenta))
			return false;
		if (numeroPlazos == null) {
			if (other.numeroPlazos != null)
				return false;
		} else if (!numeroPlazos.equals(other.numeroPlazos))
			return false;
		if (plaza == null) {
			if (other.plaza != null)
				return false;
		} else if (!plaza.equals(other.plaza))
			return false;
		if (productoBancario == null) {
			if (other.productoBancario != null)
				return false;
		} else if (!productoBancario.equals(other.productoBancario))
			return false;
		if (sucursal == null) {
			if (other.sucursal != null)
				return false;
		} else if (!sucursal.equals(other.sucursal))
			return false;
		return true;
	}
	
	
}