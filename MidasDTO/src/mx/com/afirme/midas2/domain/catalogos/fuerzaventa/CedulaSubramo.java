package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name="CedulaSubramo")
@Table(name = "TOCEDULASUBRAMO",schema="MIDAS")
public class CedulaSubramo implements Serializable, Entidad{

	private static final long serialVersionUID = 7484078468605487827L;
	private Long id;
	private ValorCatalogoAgentes tipoCedula;
	private SubRamoDTO subramoDTO;
	
	public CedulaSubramo(){
		
	}

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="IDTOCEDULA_SEQ")
	@SequenceGenerator(name="IDTOCEDULA_SEQ",sequenceName="MIDAS.IDTOCEDULA_SEQ",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}


	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="IDCEDULA",nullable=false)
		public ValorCatalogoAgentes getTipoCedula() {
		return tipoCedula;
	}

	public void setTipoCedula(ValorCatalogoAgentes tipoCedula) {
		this.tipoCedula = tipoCedula;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="IDSUBRAMO",nullable=false)
	public SubRamoDTO getSubramoDTO() {
		return subramoDTO;
	}


	public void setSubramoDTO(SubRamoDTO subramoDTO) {
		this.subramoDTO = subramoDTO;
	}



	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		// TODO Apéndice de método generado automáticamente
		return id;
	}

	@Override
	public String getValue() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		// TODO Apéndice de método generado automáticamente
		return id;
	}

}
