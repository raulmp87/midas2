package mx.com.afirme.midas2.domain.catalogos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

import org.directwebremoting.annotations.DataTransferObject;

@Entity(name="DomicilioImpresion")
@Table(name="VW_DOMICILIO_IMPRESION",schema="MIDAS")
@DataTransferObject
public class DomicilioImpresion implements Serializable, Entidad{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//private Long idDomicilio;
	private DomicilioPk idDomicilio;
	private String clavePais;
	private String claveEstado;
	private String estado;
	private String claveCiudad;
	private String ciudad;
	private String calleNumero;
	private String codigoPostal;
	private String nombreColonia;
	private String claveUsuarioModificacion;
	private Date fechaModificacion;
	private String bCatPuraAseg;
//	private PersonaSeycosDTO persona;
	private Long idPersona;
	private String tipoDomicilio;
	private String idColonia;
	private String nuevaColonia;
	
//	@Id
//	@Column(name="IDDOMICILIO",nullable=false)
	@EmbeddedId
	public DomicilioPk getIdDomicilio() {
		return idDomicilio;
	}
	public void setIdDomicilio(DomicilioPk idDomicilio) {
		this.idDomicilio = idDomicilio;
	}
	@Column(name="CLAVEPAIS")
	public String getClavePais() {
		return clavePais;
	}
	public void setClavePais(String clavePais) {
		this.clavePais = clavePais;
	}
	@Column(name="CLAVEESTADO")
	public String getClaveEstado() {
		return claveEstado;
	}
	public void setClaveEstado(String claveEstado) {
		this.claveEstado = claveEstado;
	}
	@Column(name="CLAVECIUDAD")
	public String getClaveCiudad() {
		return claveCiudad;
	}
	public void setClaveCiudad(String claveCiudad) {
		this.claveCiudad = claveCiudad;
	}
	@Column(name="CALLENUMERO")
	public String getCalleNumero() {
		return calleNumero;
	}
	public void setCalleNumero(String calleNumero) {
		this.calleNumero = calleNumero;
	}
	@Column(name="CODIGOPOSTAL")
	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	@Column(name="COLONIA")
	public String getNombreColonia() {
		return nombreColonia;
	}
	public void setNombreColonia(String nombreColonia) {
		this.nombreColonia = nombreColonia;
	}
	@Column(name="CLAVEUSUARIOMODIFICACION")
	public String getClaveUsuarioModificacion() {
		return claveUsuarioModificacion;
	}
	public void setClaveUsuarioModificacion(String claveUsuarioModificacion) {
		this.claveUsuarioModificacion = claveUsuarioModificacion;
	}
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAMODIFICACION")
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	@Column(name="BCATPURAASEG")
	public String getbCatPuraAseg() {
		return bCatPuraAseg;
	}
	public void setbCatPuraAseg(String bCatPuraAseg) {
		this.bCatPuraAseg = bCatPuraAseg;
	}
	@Column(name="IDPERSONA")
	public Long getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}
	//TODO Anotacion de columna a mapear
	@Column(name="TIPODOMICILIO")
	public String getTipoDomicilio() {
		return tipoDomicilio;
	}
	public void setTipoDomicilio(String tipoDomicilio) {
		this.tipoDomicilio = tipoDomicilio;
	}
	@Column(name="ESTADO")
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	@Column(name="CIUDAD")
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	@Column(name="IDCOLONIA")
	public String getIdColonia() {
		return idColonia;
	}
	public void setIdColonia(String idColonia) {
		this.idColonia = idColonia;
	}
	@Transient
	public String getNuevaColonia() {
		return nuevaColonia;
	}
	public void setNuevaColonia(String nuevaColonia) {
		this.nuevaColonia = nuevaColonia;
	}
	/**
	 * Metodo dummy para pruebas de domicilio.
	 * @return
	 */
	public static DomicilioImpresion getDummy(){
		DomicilioImpresion domicilio=new DomicilioImpresion();
		domicilio.setClavePais("PAMEXI");
		domicilio.setClaveEstado("08000");
		domicilio.setClaveCiudad("06005");
		domicilio.setCalleNumero("Milagros inesperados");
		domicilio.setCodigoPostal("31500 ");
		domicilio.setNombreColonia("CENTRO");
		domicilio.setTipoDomicilio("PERSONAL");
		return domicilio;
	}
//	
//	public PersonaSeycosDTO getPersona() {
//		return persona;
//	}
//	public void setPersona(PersonaSeycosDTO persona) {
//		this.persona = persona;
//	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("idDomicilio: " + this.idDomicilio + ", ");
		sb.append("idColonia: " + this.idColonia);
		sb.append("]");
		
		return sb.toString();
	}
	@SuppressWarnings("unchecked")
	@Override
	public DomicilioPk getKey() {
		return idDomicilio;
	}
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Domicilio getDomicilio(){
		Domicilio domicilio = new Domicilio();
		domicilio.setbCatPuraAseg(bCatPuraAseg);
		domicilio.setCalleNumero(calleNumero);
		domicilio.setCiudad(ciudad);
		domicilio.setClaveCiudad(claveCiudad);
		domicilio.setClaveEstado(claveEstado);
		domicilio.setClavePais(clavePais);
		domicilio.setClaveUsuarioModificacion(claveUsuarioModificacion);
		domicilio.setCodigoPostal(codigoPostal);
		domicilio.setEstado(claveEstado);
		domicilio.setFechaModificacion(fechaModificacion);
		domicilio.setIdColonia(idColonia);
		domicilio.setIdDomicilio(idDomicilio);
		domicilio.setIdPersona(idPersona);
		domicilio.setNombreColonia(nombreColonia);
		domicilio.setNuevaColonia(nuevaColonia);
		domicilio.setTipoDomicilio(tipoDomicilio);		
		
		return domicilio;
	}
}
