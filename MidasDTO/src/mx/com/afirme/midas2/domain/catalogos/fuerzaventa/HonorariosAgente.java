package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Digits;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.contabilidad.SolicitudCheque;


@Entity(name="HonorariosAgente")
@Table(name="TOHONORARIOSAGENTES", schema="MIDAS")
public class HonorariosAgente implements Serializable, Entidad, Comparable<HonorariosAgente> {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Integer idAgente;
	private BigDecimal impBase;
	private BigDecimal impGravable;
	private BigDecimal impExcento;
	private BigDecimal impIva;
	private BigDecimal impSubtotal;
	private BigDecimal impRetIva;
	private BigDecimal impRetIsr;
	private BigDecimal importeRetenidoEstatal;
	private BigDecimal impTotal;
	private Integer anioMes;
	private Date fechaCreacion;
	private EnvioFacturaAgente envioFactura;
	private ValorCatalogoAgentes status;
	
	private String autorizadorExcepcion1;
	private Date fechaAutorizacionExcepcion1;
	private String autorizadorExcepcion2;
	private Date fechaAutorizacionExcepcion2;
	private SolicitudCheque solicitudCheque;
	
	
	
	
	@Id
	@Column(name="ID", nullable=false)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="IDAGENTE", nullable=false)
	@Digits(integer=8, fraction=0)
	public Integer getIdAgente() {
		return idAgente;
	}

	public void setIdAgente(Integer idAgente) {
		this.idAgente = idAgente;
	}

	@Column(name="IMPBASE",precision=10,scale=2)
	@Digits(integer=13,fraction=2)
	public BigDecimal getImpBase() {
		return impBase;
	}

	public void setImpBase(BigDecimal impBase) {
		this.impBase = impBase;
	}
	
	@Column(name="IMPGRAVABLE",precision=10,scale=2)
	@Digits(integer=13,fraction=2)
	public BigDecimal getImpGravable() {
		return impGravable;
	}

	public void setImpGravable(BigDecimal impGravable) {
		this.impGravable = impGravable;
	}
	
	@Column(name="IMPEXENTO",precision=10,scale=2)
	@Digits(integer=13,fraction=2)
	public BigDecimal getImpExcento() {
		return impExcento;
	}

	public void setImpExcento(BigDecimal impExcento) {
		this.impExcento = impExcento;
	}
	
	@Column(name="IMPIVA",precision=10,scale=2)
	@Digits(integer=13,fraction=2)
	public BigDecimal getImpIva() {
		return impIva;
	}

	public void setImpIva(BigDecimal impIva) {
		this.impIva = impIva;
	}
	
	@Column(name="IMPSUBTOTAL",precision=10,scale=2)
	@Digits(integer=13,fraction=2)
	public BigDecimal getImpSubtotal() {
		return impSubtotal;
	}

	public void setImpSubtotal(BigDecimal impSubtotal) {
		this.impSubtotal = impSubtotal;
	}
	
	@Column(name="IMPRETIVA",precision=10,scale=2)
	@Digits(integer=13,fraction=2)
	public BigDecimal getImpRetIva() {
		return impRetIva;
	}

	public void setImpRetIva(BigDecimal impRetIva) {
		this.impRetIva = impRetIva;
	}
	
	@Column(name="IMPRETISR",precision=10,scale=2)
	@Digits(integer=13,fraction=2)
	public BigDecimal getImpRetIsr() {
		return impRetIsr;
	}

	public void setImpRetIsr(BigDecimal impRetIsr) {
		this.impRetIsr = impRetIsr;
	}
	
	@Column(name="IMPTOTAL",precision=10,scale=2)
	@Digits(integer=13,fraction=2)
	public BigDecimal getImpTotal() {
		return impTotal;
	}

	public void setImpTotal(BigDecimal impTotal) {
		this.impTotal = impTotal;
	}
	
	@Column(name="ANIOMES", nullable=false)
	@Digits(integer=8,fraction=0)
	public Integer getAnioMes() {
		return anioMes;
	}

	public void setAnioMes(Integer anioMes) {
		this.anioMes = anioMes;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHACREACION")
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	@OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDENVIO", unique=true)
	public EnvioFacturaAgente getEnvioFactura() {
		return envioFactura;
	}

	public void setEnvioFactura(EnvioFacturaAgente envioFactura) {
		this.envioFactura = envioFactura;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="STATUS")
	public ValorCatalogoAgentes getStatus() {
		return status;
	}

	public void setStatus(ValorCatalogoAgentes status) {
		this.status = status;
	}
	
	@Column(name="IMP_RET_EST",precision=10,scale=2)
	@Digits(integer=13,fraction=2)
	public BigDecimal getimporteRetenidoEstatal() {
		return importeRetenidoEstatal;
	}

	public void setimporteRetenidoEstatal(BigDecimal importeRetenidoEstatal) {
		this.importeRetenidoEstatal = importeRetenidoEstatal;
	}

	@Column(name = "USUARIO_AUTORIZA_1", length = 8)
	public String getAutorizadorExcepcion1() {
		return autorizadorExcepcion1;
	}

	public void setAutorizadorExcepcion1(String autorizadorExcepcion1) {
		this.autorizadorExcepcion1 = autorizadorExcepcion1;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_AUTORIZA_1")
	public Date getFechaAutorizacionExcepcion1() {
		return fechaAutorizacionExcepcion1;
	}

	public void setFechaAutorizacionExcepcion1(Date fechaAutorizacionExcepcion1) {
		this.fechaAutorizacionExcepcion1 = fechaAutorizacionExcepcion1;
	}

	@Column(name = "USUARIO_AUTORIZA_2", length = 8)
	public String getAutorizadorExcepcion2() {
		return autorizadorExcepcion2;
	}

	public void setAutorizadorExcepcion2(String autorizadorExcepcion2) {
		this.autorizadorExcepcion2 = autorizadorExcepcion2;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_AUTORIZA_2")
	public Date getFechaAutorizacionExcepcion2() {
		return fechaAutorizacionExcepcion2;
	}

	public void setFechaAutorizacionExcepcion2(Date fechaAutorizacionExcepcion2) {
		this.fechaAutorizacionExcepcion2 = fechaAutorizacionExcepcion2;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="CHEQUE_ID")
	public SolicitudCheque getSolicitudCheque() {
		return solicitudCheque;
	}

	public void setSolicitudCheque(SolicitudCheque solicitudCheque) {
		this.solicitudCheque = solicitudCheque;
	}

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int compareTo(HonorariosAgente o) {
		if (o == null) { 
			return 1; 
		}
		
		int x1 = 0;
		BigDecimal aux1= BigDecimal.valueOf(0);
		
		if (o.solicitudCheque != null){
			aux1 = o.solicitudCheque.getId();
		}
		if (this.solicitudCheque != null){
			x1 = this.solicitudCheque.getId().compareTo(aux1);
		}
		
		if (x1 == 0){
			return  (this.getAnioMes() == null? 0:this.getAnioMes()) - (o.getAnioMes() == null?0:o.getAnioMes());
		}
		else{
			return x1;
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		
		HonorariosAgente other = (HonorariosAgente) obj;
		
		if (solicitudCheque == null) {
			if (other.solicitudCheque != null){
				return false;
			}
		} else if (!solicitudCheque.equals(other.solicitudCheque)) {
			return false;
		}
		
		if (anioMes == null) {
			if (other.anioMes != null){
				return false;
			}
		} else if (!anioMes.equals(other.anioMes)) {
			return false;
		}
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		
		result = prime * result + ((solicitudCheque == null) ? 0 : solicitudCheque.hashCode());
		result = prime * result + ((anioMes == null) ? 0 : anioMes.hashCode());
		
		return result;
	}
	
}
