package mx.com.afirme.midas2.domain.catalogos.banco;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

import org.eclipse.persistence.annotations.ReadOnly;



/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 26-may-2015 01:24:20 p.m.
 */
@Entity(name = "CuentaBancoMidas")
@Table(name = "TCCUENTABANCO_MIDAS", schema = "MIDAS")
@ReadOnly
public class CuentaBancoMidas  implements Serializable, Entidad {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name = "ACTIVO") 
	private Boolean activo;
	@Column(name = "ALGORITMO") 
	private String algoritmo;
	@Column(name = "CLABE") 
	private String clabe;
	@Column(name = "CONVENIO") 
	private String convenio;
	@Column(name = "DESCRIPCION") 
	private String descripcion;

	@Id
	@Column(name = "ID_CUENTABANCO") 
	private Long id;
	@Column(name = "ID_MONEDA") 
	private Long monedaId;
	@Column(name = "NUMERO_CUENTA")  
	private String numeroCuenta;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_BANCO", referencedColumnName = "ID_BANCO")
	public BancoMidas bancoMidas;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CREACION")
	private Date fechaCreacion;
	
	
	@Column(name = "CODIGO_USUARIO_CREACION") 
	private String codigoUsuarioCreacion;
	
	
	@Override
	public Long getKey() {
		return this.id;
	}
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	public Boolean getActivo() {
		return activo;
	}
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
	public String getAlgoritmo() {
		return algoritmo;
	}
	public void setAlgoritmo(String algoritmo) {
		this.algoritmo = algoritmo;
	}
	public String getClabe() {
		return clabe;
	}
	public void setClabe(String clabe) {
		this.clabe = clabe;
	}
	public String getConvenio() {
		return convenio;
	}
	public void setConvenio(String convenio) {
		this.convenio = convenio;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getMonedaId() {
		return monedaId;
	}
	public void setMonedaId(Long monedaId) {
		this.monedaId = monedaId;
	}
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	
	public BancoMidas getBancoMidas() {
		return bancoMidas;
	}
	public void setBancoMidas(BancoMidas bancoMidas) {
		this.bancoMidas = bancoMidas;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}
	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	
	

}