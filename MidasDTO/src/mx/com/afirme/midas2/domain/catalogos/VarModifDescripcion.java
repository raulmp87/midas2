package mx.com.afirme.midas2.domain.catalogos;
// default package

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
import mx.com.afirme.midas2.validator.group.NewItemChecks;
import mx.com.afirme.midas2.validator.group.TarifaAutoModifChecks;


/**
 * VarModifDescripcion entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TCVARMODIFDESCRIPCION" 
      ,schema="MIDAS" 
      , uniqueConstraints = @UniqueConstraint(columnNames={"IDGRUPO", "NUMEROSECUENCIA"})
)

public class VarModifDescripcion extends CacheableDTO implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
    private Integer idGrupo;
    private BigDecimal numeroSecuencia;
    private String valor;


    // Constructors

    /** default constructor */
    public VarModifDescripcion() {
    }

	/** minimal constructor */
    public VarModifDescripcion(Long id, Integer idGrupo, BigDecimal numeroSecuencia) {
        this.id = id;
        this.idGrupo = idGrupo;
        this.numeroSecuencia = numeroSecuencia;
    }
    
    /** full constructor */
    public VarModifDescripcion(Long id, Integer idGrupo, BigDecimal numeroSecuencia, String valor) {
        this.id = id;
        this.idGrupo = idGrupo;
        this.numeroSecuencia = numeroSecuencia;
        this.valor = valor;
    }

   
    // Property accessors
    @Id 
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTCVARMODIFDESCRIPCION_SEQ")
	@SequenceGenerator(name="IDTCVARMODIFDESCRIPCION_SEQ", sequenceName="MIDAS.IDTCVARMODIFDESCRIPCION_SEQ", allocationSize=1)
    @Column(name="IDTCVARMODIFDESCRIPCION", unique=true, nullable=false, precision=22, scale=0)
    @NotNull(groups=TarifaAutoModifChecks.class, message="{com.afirme.midas2.requerido}")
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    
    @Column(name="IDGRUPO", nullable=false, precision=22, scale=0)
    @NotNull(groups={ NewItemChecks.class, TarifaAutoModifChecks.class}, message="{com.afirme.midas2.requerido}")
    @Digits(integer = 5, fraction= 0,groups=NewItemChecks.class,message="{javax.validation.constraints.Digits.message}")
    @Min(value=1,groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
    public Integer getIdGrupo() {
        return this.idGrupo;
    }
    
    public void setIdGrupo(Integer idGrupo) {
        this.idGrupo = idGrupo;
    }
    
    
	@Column(name="NUMEROSECUENCIA", nullable=false, precision=22, scale=0)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer = 5, fraction= 0,groups=NewItemChecks.class,message="{javax.validation.constraints.Digits.message}")
    public BigDecimal getNumeroSecuencia() {
        return this.numeroSecuencia;
    }
    
    public void setNumeroSecuencia(BigDecimal numeroSecuencia) {
        this.numeroSecuencia = numeroSecuencia;
    }
    
    
    @Column(name="VALOR", length=6)
    @Size(min=1, max=6, groups=EditItemChecks.class, message="{javax.validation.constraints.Size.message}")
    public String getValor() {
        return this.valor!=null?this.valor.toUpperCase():this.valor;
    }
    
    public void setValor(String valor) {
        this.valor = valor;
    }
   
    
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

    
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VarModifDescripcion other = (VarModifDescripcion) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String getDescription() {
		return getValor();
	}








}