package mx.com.afirme.midas2.domain.catalogos.notificacionCierreMes;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name = "TONOTIFICABITACORA", schema = "MIDAS")
public class NotificacionCM implements Entidad{

	 /**
	 *Fields
	 */
	private static final long serialVersionUID = 1L;
	private Long idBitacora;
	private String descripcionEnvio;
	private String estatus;
	private String usuarioAlta;
	private Date fechaAlta;
	private String usuarioAtualiza;
	private Date fechaActualiza;

	/** default constructor */
	public NotificacionCM() {

	}

	@Id
	 @SequenceGenerator(name = "NOTIFICA_BITACORA_SEQ", allocationSize = 1,
	 sequenceName = "MIDAS.NOTIFICA_BITACORA_SEQ")
	 @GeneratedValue(strategy = GenerationType.SEQUENCE, generator ="NOTIFICA_BITACORA_SEQ")
	@Column(name = "ID_BITACORA", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getIdBitacora() {
		return this.idBitacora;
	}

	public void setIdBitacora(Long idBitacora) {
		this.idBitacora = idBitacora;
	}

	@Column(name = "DESCRIPCION_ENVIO", nullable = false, length = 50)
	public String getDescripcionEnvio() {
		return this.descripcionEnvio;
	}

	public void setDescripcionEnvio(String descripcionEnvio) {
		this.descripcionEnvio = descripcionEnvio;
	}

	@Column(name = "ESTATUS", nullable = false, length = 50)
	public String getEstatus() {
		return this.estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	@Column(name = "USUARIO_ALTA", nullable = false, length = 50)
	public String getUsuarioAlta() {
		return this.usuarioAlta;
	}

	public void setUsuarioAlta(String usuarioAlta) {
		this.usuarioAlta = usuarioAlta;
	}

	@Temporal( TemporalType.DATE)
	@Column(name = "FECHA_ALTA", length = 7,nullable = false)
	public Date getFechaAlta() {
		return this.fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	@Column(name = "USUARIO_ACTUALIZA", nullable = false, length = 50)
	public String getUsuarioAtualiza() {
		return this.usuarioAtualiza;
	}

	public void setUsuarioAtualiza(String usuarioAtualiza) {
		this.usuarioAtualiza = usuarioAtualiza;
	}

	@Temporal( TemporalType.DATE)
	@Column(name = "FECHA_ACTUALIZA", length = 7,nullable = false)
	public Date getFechaActualiza() {
		return this.fechaActualiza;
	}

	public void setFechaActualiza(Date fechaActualiza) {
		this.fechaActualiza = fechaActualiza;
	}

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}
