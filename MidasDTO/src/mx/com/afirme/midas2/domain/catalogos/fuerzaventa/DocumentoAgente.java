package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

@Entity(name="DocumentoAgente")
@Table(name="toDocumentoAgente",schema="MIDAS")
public class DocumentoAgente implements Serializable,Entidad{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String idString;
	private	Agente agente;
	private	String comentarios;
	private	Long idArchivo;
	private String idArchivoString;
	private TipoDocumentoAgente tipoDocumentoAgente;
	public DocumentoAgente(){}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idToDocumentoAgente_seq")
	@SequenceGenerator(name="idToDocumentoAgente_seq", sequenceName="MIDAS.idToDocumentoAgente_seq",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="AGENTE_ID",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Agente getAgente() {
		return agente;
	}
	public void setAgente(Agente agente) {
		this.agente = agente;
	}
	
	@Column(name="COMENTARIOS",nullable=true,length=200)
	@Size(min=0,max=200)
	public String getComentarios() {
		return comentarios;
	}
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}
	
	@Column(name="IDARCHIVO",nullable=true)
	public Long getIdArchivo() {
		return idArchivo;
	}
	public void setIdArchivo(Long idArchivo) {
		this.idArchivo = idArchivo;
	}
	
	@ManyToOne
	@JoinColumn(name="TIPODOCUMENTOAGENTE_ID",nullable=false)
	public TipoDocumentoAgente getTipoDocumentoAgente() {
		return tipoDocumentoAgente;
	}
	public void setTipoDocumentoAgente(TipoDocumentoAgente tipoDocumentoAgente) {
		this.tipoDocumentoAgente = tipoDocumentoAgente;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return (tipoDocumentoAgente!=null)?tipoDocumentoAgente.getDescripcion():null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	@Transient
	public String getIdString() {
		return idString;
	}

	public void setIdString(String idString) {
		this.idString = idString;
		this.id = Long.valueOf(idString);
	}

	@Transient
	public String getIdArchivoString() {
		return idArchivoString;
	}

	public void setIdArchivoString(String idArchivoString) {
		this.idArchivoString=idArchivoString;
		this.idArchivo = Long.valueOf(idArchivoString);
	}
	
}
