package mx.com.afirme.midas2.domain.catalogos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.CotizacionExpressPaso2Checks;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
import mx.com.afirme.midas2.validator.group.NewItemChecks;

@Entity
@Table(name = "TCPAQUETE", schema = "MIDAS")
public class Paquete extends CacheableDTO implements Entidad,Serializable {

	private static final long serialVersionUID = -5988894780482777813L;
	
	private Long id;
	private String descripcion;
	private Long claveAmisTipoSeguro;
	private String descripcionAmisTipoSeguro;
	private List<SeccionDTO> secciones = new ArrayList<SeccionDTO>() ;
	
	public static final Long BASICA = new Long(678);
	public static final Long LIMITADA = new Long(3);
	public static final Long AMPLIA = new Long(1);
	public static final Long PLATA = new Long(774);
	public static final Long ORO = new Long(775);
	public static final Long PLATINO = new Long(776);
	
	@Id
	@Column(name = "IDPAQUETE", nullable = false)
	@NotNull(groups={NewItemChecks.class, CotizacionExpressPaso2Checks.class}, message="{com.afirme.midas2.requerido}")	
	@Digits(integer = 5, fraction= 0,groups=NewItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "DESCRIPCIONPAQUETE", nullable = true, length = 100)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=1, max=100, groups=EditItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "CVEAMIS_TIPOSEGURO", nullable = true, length = 100)
	@NotNull()
	@Digits(integer = 5, fraction= 0,groups=NewItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public Long getClaveAmisTipoSeguro() {
		return claveAmisTipoSeguro;
	}
	public void setClaveAmisTipoSeguro(Long claveAmisTipoSeguro) {
		this.claveAmisTipoSeguro = claveAmisTipoSeguro;
	}

	@Transient
	public String getDescripcionAmisTipoSeguro() {
		return descripcionAmisTipoSeguro;
	}

	public void setDescripcionAmisTipoSeguro(String descripcionAmisTipoSeguro) {
		this.descripcionAmisTipoSeguro = descripcionAmisTipoSeguro;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "TRPAQUETESECCION", schema = "MIDAS",
			joinColumns = {
				@JoinColumn(name="IDPAQUETE")
			},
			inverseJoinColumns = {
				@JoinColumn(name="IDTOSECCION")
			}
	)
	public List<SeccionDTO> getSecciones() {
		return secciones;
	}

	public void setSecciones(List<SeccionDTO> secciones) {
		this.secciones = secciones;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((descripcion == null) ? 0 : descripcion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Paquete other = (Paquete) obj;
		if (descripcion == null) {
			if (other.descripcion != null)
				return false;
		} else if (!descripcion.equals(other.descripcion))
			return false;
		return true;
	}

	public Paquete() {
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		return this.descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public  Long getBusinessKey() {
		return this.id;
	}
	
	@Override
	public String getDescription(){
		return descripcion;
	}
	
	public List<Long> getPaquetesCotizacionExpress(){
		List<Long> paquetesCotExpress = new ArrayList<Long>(1);
		paquetesCotExpress.add(AMPLIA);
		paquetesCotExpress.add(BASICA);
		paquetesCotExpress.add(LIMITADA);
		paquetesCotExpress.add(ORO);
		paquetesCotExpress.add(PLATA);
		paquetesCotExpress.add(PLATINO);
		return paquetesCotExpress;
	}
	

}
