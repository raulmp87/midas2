package mx.com.afirme.midas2.domain.catalogos.retencionimpuestos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;

@Entity
@Table(name = "TOIMPUESTOAGENTE_MONITOR", schema = "MIDAS")
public class RetencionCedularesView implements Serializable, Entidad {
	private static final long serialVersionUID = 5406080117689663930L;
	private Long id;
	private Long avance;
	private ValorCatalogoAgentes estatusEjecucion;
	private Date fechaEjecucion;
	private Long idConfiguracion;
	private ValorCatalogoAgentes tipoEjecucion;
	private String usuario;
	private Integer estatus;
	
	public RetencionCedularesView() {}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TOIMPUESTOAGENTE_MONITOR_SEQ")
	@SequenceGenerator(name = "TOIMPUESTOAGENTE_MONITOR_SEQ", sequenceName = "MIDAS.TOIMPUESTOAGENTE_MONITOR_SEQ", allocationSize = 1)
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}
	
	@Column(name = "AVANCE")
	public Long getAvance() {
		return avance;
	}
	
	@ManyToOne(fetch = FetchType.EAGER, targetEntity = ValorCatalogoAgentes.class)
	@JoinColumn(name = "ESTATUSEJECUCION_ID")
	public ValorCatalogoAgentes getEstatusEjecucion() {
		return estatusEjecucion;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAEJECUCION")
	public Date getFechaEjecucion() {
		return fechaEjecucion;
	}
	
	@Column(name = "IDCONFIGURACION")
	public Long getIdConfiguracion() {
		return idConfiguracion;
	}
	
	@ManyToOne(fetch = FetchType.EAGER, targetEntity = ValorCatalogoAgentes.class)
	@JoinColumn(name = "TIPOEJECUCION_ID")
	public ValorCatalogoAgentes getTipoEjecucion() {
		return tipoEjecucion;
	}
	
	@Column(name = "USUARIOALTA")
	public String getUsuario() {
		return usuario;
	}
	
	@Column(name = "ENPROCESO")
	public Integer getEstatus() {
		return this.estatus;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	public void setAvance(Long avance) {
		this.avance = avance;
	}
	public void setEstatusEjecucion(ValorCatalogoAgentes estatusEjecucion) {
		this.estatusEjecucion = estatusEjecucion;
	}
	public void setFechaEjecucion(Date fechaEjecucion) {
		this.fechaEjecucion = fechaEjecucion;
	}
	public void setIdConfiguracion(Long idConfiguracion) {
		this.idConfiguracion = idConfiguracion;
	}
	public void setTipoEjecucion(ValorCatalogoAgentes tipoEjecucion) {
		this.tipoEjecucion = tipoEjecucion;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}

}
