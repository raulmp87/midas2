package mx.com.afirme.midas2.domain.catalogos.ExcluirAgentes;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="TOEXCLUIRAGENTE", schema="MIDAS")
public class Excluir implements Entidad{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public Excluir(){
		
	}
	
	/**
	 *Fields
	 */
	private Long id;
	private String idAgente;
	private Boolean comision;
	private Boolean bono;
	private Integer estatus;
	private String usuario;
	private Date fechaAlta;
	private String usuarioActualiza;
	private Date fechaActualiza;
	
	

	
	@Id
	@SequenceGenerator(name="EXCLUIR_AGENTES_SEQ",allocationSize = 1,sequenceName="MIDAS.EXCLUIR_AGENTES_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator ="EXCLUIR_AGENTES_SEQ")
	@Column(name = "ID", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "ID_AGENTE")	
	public String getIdAgente() {
		return idAgente;
	}
	public void setIdAgente(String idAgente) {
		this.idAgente = idAgente;
	}

	@Column(name="COMISION")
	public Boolean isComision() {
		return comision;
	}
	public void setComision(Boolean comision) {
		this.comision = comision;
	}
	@Column(name="BONO")
	public Boolean isBono() {
		return bono;
	}
	public void setBono(Boolean bono) {
		this.bono = bono;
	}
	
	@Column(name = "ESTATUS", nullable = false, length = 50)
	public Integer getEstatus() {
		return estatus;
	}
	
	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}
	
	@Column(name = "USUARIO_ALTA", nullable = false, length = 50)
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name = "FECHA_ALTA")
	public Date getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	@Column(name = "USUARIO_ACTUALIZA", nullable = false, length = 50)
	public String getUsuarioActualiza() {
		return usuarioActualiza;
	}
	public void setUsuarioActualiza(String usuarioActualiza) {
		this.usuarioActualiza = usuarioActualiza;
	}
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name = "FECHA_ACTUALIZA")
	public Date getFechaActualiza() {
		return fechaActualiza;
	}
	public void setFechaActualiza(Date fechaActualiza) {
		this.fechaActualiza = fechaActualiza;
	}

	@Override
	public Long  getKey() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	
}
