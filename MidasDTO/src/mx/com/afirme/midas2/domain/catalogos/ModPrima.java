package mx.com.afirme.midas2.domain.catalogos;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
import mx.com.afirme.midas2.validator.group.NewItemChecks;

@Entity
@Table(name = "TCVARMODIFPRIMA", schema = "MIDAS")
public class ModPrima implements Entidad,Serializable {

	private static final long serialVersionUID = -5988894780482777813L;
	
	private Long id;
	private Integer numeroSecuencia;
	private String rangoMinimo;
    String  rangoMaximo;
	private String valor;
	
	@Id
	@Column(name = "IDTCGRUPOVARMODIFPRIMA", nullable = false)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")	
	@Digits(integer = 5, fraction= 0,groups=NewItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	
	@Column(name = "NUMEROSECUENCIA", nullable = true, length = 100)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=1, max=100, groups=EditItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public Integer getNumeroSecuencia() {
		return numeroSecuencia;
	}
	public void setNumeroSecuencia(Integer numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}
	
	
	@Column(name = "RANGOMINIMO", nullable = true, length = 100)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=1, max=100, groups=EditItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getRangoMinimo() {
		return rangoMinimo;
	}
	public void setRangoMinimo(String rangoMinimo) {
		this.rangoMinimo = rangoMinimo;
	}	
	
	
	@Column(name = "RANGOMAXIMO", nullable = true, length = 100)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=1, max=100, groups=EditItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getRangoMaximo() {
		return rangoMaximo;
	}
	public void setRangoMaximo(String rangoMaximo) {
		this.rangoMaximo = rangoMaximo;
	}
	
	
	@Column(name = "VALOR", nullable = true, length = 100)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=1, max=100, groups=EditItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}		

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((rangoMinimo == null) ? 0 : rangoMinimo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModPrima other = (ModPrima) obj;
		if (rangoMinimo == null) {
			if (other.rangoMinimo != null)
				return false;
		} else if (!rangoMinimo.equals(other.rangoMinimo))
			return false;
		return true;
	}

	public ModPrima() {
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		return this.rangoMinimo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public  Long getBusinessKey() {
		return this.id;
	}
	

}
