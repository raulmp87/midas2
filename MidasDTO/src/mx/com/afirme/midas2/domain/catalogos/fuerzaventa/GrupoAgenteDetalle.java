package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;

import javax.persistence.JoinColumn;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;

import org.directwebremoting.annotations.DataTransferObject;
import org.eclipse.persistence.annotations.Customizer;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

@Entity(name="grupoAgenteDetalle")
@Table(name="TOAGENTE_GRUPO_DETALLE",schema="MIDAS")
@DataTransferObject
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class GrupoAgenteDetalle implements Serializable,Entidad{

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private GrupoAgente grupo;
	private Long idAgente;
	
	
	/*GETTERS AND SETTERS*/
	@Id 
	@SequenceGenerator(name = "TOAGENTE_GRUPO_DETALLE_ID_SEQ_GENERADOR",allocationSize = 1, sequenceName = "MIDAS.TOAGENTE_GRUPO_DETALLE_SEQ")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TOAGENTE_GRUPO_DETALLE_ID_SEQ_GENERADOR")
    @Column(name="ID", unique=true, nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@JoinColumn(name="GRUPO_ID",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public GrupoAgente getGrupo() {
		return grupo;
	}

	public void setGrupo(GrupoAgente idGrupo) {
		this.grupo = idGrupo;
	}

	@Column(name="AGENTE_ID",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getIdAgente() {
		return idAgente;
	}

	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return null;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return null;
	}

}
