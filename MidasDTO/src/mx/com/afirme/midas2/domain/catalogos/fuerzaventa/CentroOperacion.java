package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.DomicilioSeycos;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
import org.eclipse.persistence.annotations.Customizer;

/**
 * The persistent class for the TOCENTROOPERACION database table.
 * 
 */
@Entity(name="CentroOperacion")
@Table(name="toCentroOperacion",schema="MIDAS")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class CentroOperacion implements Serializable,Entidad{
	private static final long serialVersionUID = 1L;
	private Long id;
	private Long claveEstatus;
	private String correoElectronico;
	private String descripcion;
	private Long idCentroOperacion;
	private Domicilio domicilio;
	private Persona personaResponsable;
	private List<Gerencia> gerencias;
	private Date fechaInicio;
	private Date fechaFin;	
	private DomicilioSeycos domicilioHistorico;
	
	public CentroOperacion() {
    }
    
    public CentroOperacion(Long id) {
    	this.id = id;
    }

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idToCentroOperacion_seq")
	@SequenceGenerator(name="idToCentroOperacion_seq", sequenceName="MIDAS.idToCentroOperacion_seq",allocationSize=1)
	@Column(name="ID",nullable=false,unique=true)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="CLAVEESTATUS",nullable=false)
	@NotNull(message="{com.afirme.midas2.requerido}")
	public Long getClaveEstatus() {
		return this.claveEstatus;
	}

	public void setClaveEstatus(Long claveestatus) {
		this.claveEstatus = claveestatus;
	}

	@Column(name="CORREOELECTRONICO",length=80)
	@Size(min=0,max=80)
	public String getCorreoElectronico() {
		return this.correoElectronico;
	}

	public void setCorreoElectronico(String correoelectronico) {
		this.correoElectronico = correoelectronico;
	}

	@Column(name="DESCRIPCION",nullable=false,length=80)
	@Size(min=1,max=80)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name="IDCENTROOPERACION",nullable=false)
	public Long getIdCentroOperacion() {
		return this.idCentroOperacion;
	}
	
	public void setIdCentroOperacion(Long idcentrooperacion) {
		this.idCentroOperacion = idcentrooperacion;
	}
	
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumns({
		@JoinColumn(name="IDDOMICILIO",referencedColumnName="IDDOMICILIO"),
		@JoinColumn(name="IDPERSONARESPONSABLE",referencedColumnName="IDPERSONA",insertable=false,updatable=false)
	})
	public Domicilio getDomicilio() {
		return this.domicilio;
	}

	public void setDomicilio(Domicilio domicilio) {
		this.domicilio = domicilio;
	}
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name="IDDOMICILIO",referencedColumnName="ID_DOMICILIO", insertable=false, updatable= false),
		@JoinColumn(name="IDPERSONARESPONSABLE",referencedColumnName="ID_PERSONA",insertable=false,updatable=false)
	})
	public DomicilioSeycos getDomicilioHistorico() {
		return domicilioHistorico;
	}


	public void setDomicilioHistorico(DomicilioSeycos domicilioHistorico) {
		this.domicilioHistorico = domicilioHistorico;
	}

	//bi-directional many-to-one association to Gerencia
	@OneToMany(mappedBy="centroOperacion",fetch=FetchType.LAZY)
	public List<Gerencia> getGerencias() {
		return this.gerencias;
	}

	public void setGerencias(List<Gerencia> gerencias) {
		this.gerencias = gerencias;
	}
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="IDPERSONARESPONSABLE",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Persona getPersonaResponsable() {
		return personaResponsable;
	}

	public void setPersonaResponsable(Persona personaResponsable) {
		this.personaResponsable = personaResponsable;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}


	@Override
	public String getValue() {
		return descripcion;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	
	private void writeObject(ObjectOutputStream out) throws IOException{
		if(gerencias!=null){
			gerencias.size();
		}
		out.defaultWriteObject();
	}

	@Transient
	public Date getFechaInicio() {
		return fechaInicio;
	}


	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	@Transient
	public Date getFechaFin() {
		return fechaFin;
	}


	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}	
}