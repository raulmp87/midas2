package mx.com.afirme.midas2.domain.catalogos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.validator.group.NewItemChecks;


/**
 * ValorSeccionCobAutosId entity. @author MyEclipse Persistence Tools
 */
@Embeddable

public class ValorSeccionCobAutosId  implements Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = -2952157433061720645L;
	private Long idToSeccion;
     private Long idToCobertura;
     private Long idMoneda;
     private Long idTcTipoUsoVehiculo;


    // Constructors

    /** default constructor */
    public ValorSeccionCobAutosId() {
    }

    
    /** full constructor */
    public ValorSeccionCobAutosId(Long idToSeccion, Long idToCobertura, Long idMoneda, Long idTcTipoUsoVehiculo) {
        this.idToSeccion = idToSeccion;
        this.idToCobertura = idToCobertura;
        this.idMoneda = idMoneda;
        this.idTcTipoUsoVehiculo = idTcTipoUsoVehiculo;
    }

   
    // Property accessors

    @Column(name="IDTOSECCION", nullable=false, precision=22, scale=0)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
    public Long getIdToSeccion() {
        return this.idToSeccion;
    }
    
    public void setIdToSeccion(Long idToSeccion) {
        this.idToSeccion = idToSeccion;
    }

    @Column(name="IDTOCOBERTURA", nullable=false, precision=22, scale=0)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
    public Long getIdToCobertura() {
        return this.idToCobertura;
    }
    
    public void setIdToCobertura(Long idToCobertura) {
        this.idToCobertura = idToCobertura;
    }

    @Column(name="IDMONEDA", nullable=false, precision=22, scale=0)
    @NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
    public Long getIdMoneda() {
        return this.idMoneda;
    }
    
    public void setIdMoneda(Long idMoneda) {
        this.idMoneda = idMoneda;
    }

    @Column(name="IDTCTIPOUSOVEHICULO", nullable=false, precision=22, scale=0)
    @NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
    public Long getIdTcTipoUsoVehiculo() {
        return this.idTcTipoUsoVehiculo;
    }
    
    public void setIdTcTipoUsoVehiculo(Long idTcTipoUsoVehiculo) {
        this.idTcTipoUsoVehiculo = idTcTipoUsoVehiculo;
    }
   



   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof ValorSeccionCobAutosId) ) return false;
		 ValorSeccionCobAutosId castOther = ( ValorSeccionCobAutosId ) other; 
         
		 return ( (this.getIdToSeccion()==castOther.getIdToSeccion()) || ( this.getIdToSeccion()!=null && castOther.getIdToSeccion()!=null && this.getIdToSeccion().equals(castOther.getIdToSeccion()) ) )
 && ( (this.getIdToCobertura()==castOther.getIdToCobertura()) || ( this.getIdToCobertura()!=null && castOther.getIdToCobertura()!=null && this.getIdToCobertura().equals(castOther.getIdToCobertura()) ) )
 && ( (this.getIdMoneda()==castOther.getIdMoneda()) || ( this.getIdMoneda()!=null && castOther.getIdMoneda()!=null && this.getIdMoneda().equals(castOther.getIdMoneda()) ) )
 && ( (this.getIdTcTipoUsoVehiculo()==castOther.getIdTcTipoUsoVehiculo()) || ( this.getIdTcTipoUsoVehiculo()!=null && castOther.getIdTcTipoUsoVehiculo()!=null && this.getIdTcTipoUsoVehiculo().equals(castOther.getIdTcTipoUsoVehiculo()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdToSeccion() == null ? 0 : this.getIdToSeccion().hashCode() );
         result = 37 * result + ( getIdToCobertura() == null ? 0 : this.getIdToCobertura().hashCode() );
         result = 37 * result + ( getIdMoneda() == null ? 0 : this.getIdMoneda().hashCode() );
         result = 37 * result + ( getIdTcTipoUsoVehiculo() == null ? 0 : this.getIdTcTipoUsoVehiculo().hashCode() );
         return result;
   }   





}