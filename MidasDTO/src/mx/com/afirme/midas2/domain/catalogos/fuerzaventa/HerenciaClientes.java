package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name="HerenciaClientes")
@Table(name="TOHERENCIACLIENTES",schema="MIDAS")
public class HerenciaClientes implements Serializable,Entidad{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6952370575473828064L;
	private Long id;
	private Agente agenteOrigen;
	private Agente agenteDestino;
	private ValorCatalogoAgentes motivoCesion;
	private Date fechaAlta; 
	private String fechaString;
	private Date fechaInicio;
	private Date fechaFin;
	private ValorCatalogoAgentes status;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="idToHerenciaClientes_seq")
	@SequenceGenerator(name="idToHerenciaClientes_seq",sequenceName="MIDAS.idToHerenciaClientes_seq",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=Agente.class)
	@JoinColumn(name="IDAGENTEORIGEN",nullable=false)
//	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public Agente getAgenteOrigen() {
		return agenteOrigen;
	}

	public void setAgenteOrigen(Agente agenteOrigen) {
		this.agenteOrigen = agenteOrigen;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=Agente.class)
	@JoinColumn(name="IDAGENTEDESTINO",nullable=false)
//	@NotNull(groups=EditItemChecks.class,message="{com.afirme.midas2.requerido}")
	public Agente getAgenteDestino() {
		return agenteDestino;
	}

	public void setAgenteDestino(Agente agenteDestino) {
		this.agenteDestino = agenteDestino;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDMOTIVOCESION",nullable=false)
	//@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public ValorCatalogoAgentes getMotivoCesion() {
		return motivoCesion;
	}

	public void setMotivoCesion(ValorCatalogoAgentes motivoCesion) {
		this.motivoCesion = motivoCesion;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHAALTA",nullable=false)
	public Date getFechaAlta() {
		return fechaAlta;
	}	

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");    	
		this.fechaString=sdf.format(fechaAlta);
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="STATUS")
	public ValorCatalogoAgentes getStatus() {
		return status;
	}

	public void setStatus(ValorCatalogoAgentes status) {
		this.status = status;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}
	
	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	@Transient
	public String getFechaString() {
		return fechaString;
	}

	public void setFechaString(String fechaString) {
		this.fechaString = fechaString;
	}
	
	@Transient
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	@Transient
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
}
