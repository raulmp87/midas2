package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.DomicilioSeycos;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

import org.directwebremoting.annotations.DataTransferObject;
import org.eclipse.persistence.annotations.Customizer;


/**
 * The persistent class for the TOGERENCIA database table.
 * 
 */
@Entity(name="Gerencia")
@Table(name="TOGERENCIA",schema="MIDAS")

@SqlResultSetMapping(name="gerenciaView",entities={
		@EntityResult(entityClass=Gerencia.class,fields={
			@FieldResult(name="id",column="id"),
			@FieldResult(name="idGerencia",column="idGerencia"),
			@FieldResult(name="descripcion",column="descripcion"),
			@FieldResult(name="personaResponsable.idPersona",column="idPersonaResponsable"),
			@FieldResult(name="personaResponsable.nombreCompleto",column="nombreGerente"),
			@FieldResult(name="claveEstatus",column="claveestatus")
//			@FieldResult(name="centroOperacion.id",column="centroOperacion_id")
		})
	}
)

@DataTransferObject
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizer.class)
public class Gerencia implements Serializable,Entidad {
	private static final long serialVersionUID = 1L;
	private Long id;
	private Long claveEstatus;
	private String correoElectronico;
	private String descripcion;
	private Domicilio domicilio;
	private Long idGerencia;
	private Persona personaResponsable;
	private List<Ejecutivo> ejecutivos;
	private CentroOperacion centroOperacion;
	private Date fechaInicio;
	private Date fechaFin;
	private DomicilioSeycos domicilioHistorico;
	
    public Gerencia() {}
    public Gerencia(Long id) {
    	this.id = id;
    }
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTOGERENCIA_SEQ")
	@SequenceGenerator(name="IDTOGERENCIA_SEQ", sequenceName="MIDAS.IDTOGERENCIA_SEQ",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	//bi-directional many-to-one association to Ejecutivo
	@OneToMany(mappedBy="gerencia",fetch=FetchType.LAZY)
	public List<Ejecutivo> getEjecutivos() {
		return this.ejecutivos;
	}

	public void setEjecutivos(List<Ejecutivo> ejecutivos) {
		this.ejecutivos = ejecutivos;
	}
	
	@Column(name="CLAVEESTATUS",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getClaveEstatus() {
		return claveEstatus;
	}

	public void setClaveEstatus(Long claveEstatus) {
		this.claveEstatus = claveEstatus;
	}
	@Column(name="CORREOELECTRONICO",length=80)
	@Size(min=0,max=80)
	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}
	@Column(name="DESCRIPCION",nullable=false,length=80)
	@Size(min=1,max=80)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Column(name="IDGERENCIA",nullable=false)
	public Long getIdGerencia() {
		return idGerencia;
	}

	public void setIdGerencia(Long idGerencia) {
		this.idGerencia = idGerencia;
	}
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="IDPERSONARESPONSABLE",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Persona getPersonaResponsable() {
		return personaResponsable;
	}

	public void setPersonaResponsable(Persona personaResponsable) {
		this.personaResponsable = personaResponsable;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		// TODO Auto-generated method stub
		return id;
	}
	
	@OneToOne(fetch=FetchType.EAGER)
	//@JoinColumn(name="IDDOMICILIO")
	@JoinColumns({
		@JoinColumn(name="IDDOMICILIO",referencedColumnName="IDDOMICILIO"),
		@JoinColumn(name="IDPERSONARESPONSABLE",referencedColumnName="IDPERSONA",insertable=false,updatable=false)
	})
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Domicilio getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(Domicilio domicilio) {
		this.domicilio = domicilio;
	}
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="CENTROOPERACION_ID")
	public CentroOperacion getCentroOperacion() {
		return centroOperacion;
	}

	public void setCentroOperacion(CentroOperacion centroOperacion) {
		this.centroOperacion = centroOperacion;
	}

	@Transient
	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	@Transient
	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name="IDDOMICILIO",referencedColumnName="ID_DOMICILIO", insertable=false, updatable= false),
		@JoinColumn(name="IDPERSONARESPONSABLE",referencedColumnName="ID_PERSONA",insertable=false,updatable=false)
	})
	public DomicilioSeycos getDomicilioHistorico() {
		return domicilioHistorico;
	}


	public void setDomicilioHistorico(DomicilioSeycos domicilioHistorico) {
		this.domicilioHistorico = domicilioHistorico;
	}
	
}