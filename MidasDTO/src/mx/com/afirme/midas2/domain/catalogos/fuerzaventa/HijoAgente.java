package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.eclipse.persistence.annotations.Customizer;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
@Entity(name="HijoAgente")
@Table(name="TOHIJOAGENTE",schema="MIDAS")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class HijoAgente implements Entidad, Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String nombres;
	private Date fechaNacimiento;
	private String ultimaEscolaridad;
	private Agente agente;
	private String fechaNacimientoString;
	public HijoAgente(){}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="idToHijoAgente_seq")
	@SequenceGenerator(name="idToHijoAgente_seq",sequenceName="MIDAS.idToHijoAgente_seq",allocationSize=1)
	@Column(name="ID",nullable=false,unique=true)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="AGENTE_ID",nullable=false)
	@NotNull
	public Agente getAgente() {
		return agente;
	}
	public void setAgente(Agente agente) {
		this.agente = agente;
	}
	@Column(name="APELLIDOPATERNO",nullable=false)
	@NotNull
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	@Column(name="APELLIDOMATERNO",nullable=false)
	@NotNull
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	@Column(name="NOMBRES",nullable=false)
	@NotNull
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHANACIMIENTO",nullable=false)
	@NotNull
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");    	
		this.fechaNacimientoString=sdf.format(fechaNacimiento);		
	}
	@Column(name="ULTIMAESCOLARIDAD",length=80)
	@Size(min=0,max=80)
	public String getUltimaEscolaridad() {
		return ultimaEscolaridad;
	}
	public void setUltimaEscolaridad(String ultimaEscolaridad) {
		this.ultimaEscolaridad = ultimaEscolaridad;
	}

	@Transient
	public String getFechaNacimientoString() {		
			return fechaNacimientoString;
	}

	public void setFechaNacimientoString(String fechaNacimientoString) {		
		this.fechaNacimientoString = fechaNacimientoString;
	}

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
}
