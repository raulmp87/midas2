package mx.com.afirme.midas2.domain.catalogos.banco;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

import org.eclipse.persistence.annotations.ReadOnly;


/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 26-may-2015 01:24:08 p.m.
 */
@Entity(name = "BancoMidas")
@Table(name = "TCBANCO_MIDAS", schema = "MIDAS")
@ReadOnly
public class BancoMidas implements Serializable, Entidad{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2249234568040395344L;
	@Column(name = "ACTIVO")
	private Boolean activo;
	@Column(name = "CLAVE_CONTAB_CHEQUE")
	private String claveContabCheque;
	@Column(name = "CLAVE_EXTENSION_TRANSFERENCIA")
	private String claveExtensionTransferencia;
	@Column(name = "CODIGO_USUARIO_CREACION") 
	private String codigoUsuarioCreacion;
	 
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CREACION")
	private Date fechaCreacion;
	
	@Id
	@Column(name = "ID_BANCO")
	private Long id;
	@Column(name = "NOMBRE_BANCO")
	private String nombre;
	@Column(name = "NOMBRE_CORTO") 
	private String nombreCorto;
	@Column(name = "TRANSFERENCIA_INMEDIATA")
	private Boolean transferenciaInmediata;	
	
	public Boolean getActivo() {
		return activo;
	}
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
	public String getClaveContabCheque() {
		return claveContabCheque;
	}
	public void setClaveContabCheque(String claveContabCheque) {
		this.claveContabCheque = claveContabCheque;
	}
	public String getClaveExtensionTransferencia() {
		return claveExtensionTransferencia;
	}
	public void setClaveExtensionTransferencia(String claveExtensionTransferencia) {
		this.claveExtensionTransferencia = claveExtensionTransferencia;
	}
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}
	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getNombreCorto() {
		return nombreCorto;
	}
	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}
	public Boolean getTransferenciaInmediata() {
		return transferenciaInmediata;
	}
	public void setTransferenciaInmediata(Boolean transferenciaInmediata) {
		this.transferenciaInmediata = transferenciaInmediata;
	}
	@Override
	public Long getKey() {
		// TODO Auto-generated method stub
		return this.id;
	}
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	

}