package mx.com.afirme.midas2.domain.catalogos;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name="DomicilioSeycos")
@Table(name="DOMICILIO",schema="SEYCOS")
public class DomicilioSeycos implements Serializable, Entidad {
	
	private static final long serialVersionUID = 1L;
	
	private DomicilioPk idDomicilio;
	private String clavePais;
	private String claveEstado;
	private String claveCiudad;	
	private String calleNumero;
	private String codigoPostal;
	private String nombreColonia;
	private String claveUsuarioModificacion;
	private Date fechaModificacion;
	private String bCatPuraAseg;
	private Long idPersona;
	private String tipoDomicilio;
	private String idColonia;
	private String idUsrCodPost;	
	
	@EmbeddedId
	@AttributeOverrides( {
		@AttributeOverride(name = "idDomicilio", column = @Column(name = "ID_DOMICILIO")),
		@AttributeOverride(name = "idPersona", column = @Column(name = "ID_PERSONA"))})
	public DomicilioPk getIdDomicilio() {
		return idDomicilio;
	}
	public void setIdDomicilio(DomicilioPk idDomicilio) {
		this.idDomicilio = idDomicilio;
	}
	@Column(name="CVE_PAIS")
	public String getClavePais() {
		return clavePais;
	}
	public void setClavePais(String clavePais) {
		this.clavePais = clavePais;
	}
	@Column(name="CVE_ESTADO")
	public String getClaveEstado() {
		return claveEstado;
	}
	public void setClaveEstado(String claveEstado) {
		this.claveEstado = claveEstado;
	}
	@Column(name="CVE_CIUDAD")
	public String getClaveCiudad() {
		return claveCiudad;
	}
	public void setClaveCiudad(String claveCiudad) {
		this.claveCiudad = claveCiudad;
	}
	@Column(name="CALLE_NUMERO")
	public String getCalleNumero() {
		return calleNumero;
	}
	public void setCalleNumero(String calleNumero) {
		this.calleNumero = calleNumero;
	}
	@Column(name="CODIGO_POSTAL")
	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	@Column(name="COLONIA")
	public String getNombreColonia() {
		return nombreColonia;
	}
	public void setNombreColonia(String nombreColonia) {
		this.nombreColonia = nombreColonia;
	}
	@Column(name="CVE_USUAR_MODIF")
	public String getClaveUsuarioModificacion() {
		return claveUsuarioModificacion;
	}
	public void setClaveUsuarioModificacion(String claveUsuarioModificacion) {
		this.claveUsuarioModificacion = claveUsuarioModificacion;
	}
	@Temporal(TemporalType.DATE)
	@Column(name="FH_MODIFICACION")
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	@Column(name="B_CAPTURA_ASEG")
	public String getbCatPuraAseg() {
		return bCatPuraAseg;
	}
	public void setbCatPuraAseg(String bCatPuraAseg) {
		this.bCatPuraAseg = bCatPuraAseg;
	}
	@Column(name="ID_PERSONA")
	public Long getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	@Column(name="TIPO_DOMICILIO")
	public String getTipoDomicilio() {
		return tipoDomicilio;
	}
	public void setTipoDomicilio(String tipoDomicilio) {
		this.tipoDomicilio = tipoDomicilio;
	}
	
	@Transient
	public String getIdColonia() {
		return this.codigoPostal + "-" + this.idUsrCodPost;
	}
	public void setIdColonia(String idColonia) {
		this.idColonia = idColonia;
	}
	
	@Column(name="ID_USR_COD_POST")
	public String getIdUsrCodPost() {
		return idUsrCodPost;
	}
	
	public void setIdUsrCodPost(String idUsrCodPost) {
		this.idUsrCodPost = idUsrCodPost;
	}

	@SuppressWarnings("unchecked")
	@Override
	public DomicilioPk getKey() {
		return idDomicilio;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}
