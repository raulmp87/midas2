package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import org.eclipse.persistence.config.DescriptorCustomizer;
import org.eclipse.persistence.descriptors.ClassDescriptor;
import org.eclipse.persistence.history.HistoryPolicy;

public class HistoryCustomizer implements DescriptorCustomizer{

	@Override
	public void customize(ClassDescriptor descriptor) throws Exception {
		
		HistoryPolicy policy = new HistoryPolicy();		
		policy.addHistoryTableName(descriptor.getTableName(),"MIDAS."+descriptor.getTableName()+ "_HIST");
		policy.addStartFieldName("H_START_DATE");
		policy.addEndFieldName("H_END_DATE");
		descriptor.setHistoryPolicy(policy);		
	}
}
