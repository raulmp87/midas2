package mx.com.afirme.midas2.domain.catalogos.banco;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

import org.eclipse.persistence.annotations.ReadOnly;



/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 26-may-2015 01:24:14 p.m.
 */


@Entity(name = "ConfigCuentaMidas")
@Table(name = "TCCONFIGCUENTA_MIDAS", schema = "MIDAS")
@ReadOnly
public class ConfigCuentaMidas implements Serializable, Entidad{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TCCONFIGCUENTA_SEQ_GENERADOR",allocationSize = 1, sequenceName = "TCCONFIGCUENTA_SEQ", schema = "MIDAS")
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "TCCONFIGCUENTA_SEQ_GENERADOR")
	@Column(name = "ID", nullable = false)
	private Long id;

	

	@Column(name = "ACTIVO") 
	private Boolean activo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_CUENTABANCARIA", referencedColumnName = "ID_CUENTABANCO")
	private CuentaBancoMidas cuenta;
	
	
	@Column(name = "IDENTIFICADOR") 
	private Long identificador;
	@Column(name = "NIVEL") 
	private String nivel;
	@Column(name = "ORDEN") 
	private Integer orden;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CREACION")
	private Date fechaCreacion;
	
	@Column(name = "CODIGO_USUARIO_CREACION") 
	private String codigoUsuarioCreacion;
	
	
	@Override
	public Long getKey() {
		return  this.id;
	}
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	public Boolean getActivo() {
		return activo;
	}
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
	public CuentaBancoMidas getCuenta() {
		return cuenta;
	}
	public void setCuenta(CuentaBancoMidas cuenta) {
		this.cuenta = cuenta;
	}
	public Long getIdentificador() {
		return identificador;
	}
	public void setIdentificador(Long identificador) {
		this.identificador = identificador;
	}
	public String getNivel() {
		return nivel;
	}
	public void setNivel(String nivel) {
		this.nivel = nivel;
	}
	public Integer getOrden() {
		return orden;
	}
	public void setOrden(Integer orden) {
		this.orden = orden;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}
	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}



}