package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

import java.util.List;


/**
 * The persistent class for the TCGRUPOCATALOGOAGENTES database table.
 * 
 */
@Entity(name="GrupoCatalogoAgente")
@Table(name="TCGRUPOCATALOGOAGENTES", schema="MIDAS")
public class GrupoCatalogoAgente implements Serializable,Entidad {
	private static final long serialVersionUID = 1L;
	private Long id;
	private String descripcion;
	private List<ValorCatalogoAgentes> valorCatalogoAgentes;
	
	public static final String GRUPO_CAT_AGTE_ESTATUS_ENVIO_REPCB_MENSUAL = "Estatus Envio Reporte Calculo Bono Mensual";
	public static final String GRUPO_CAT_AGTE_MODOS_EJECUCION_COMISIONES = "Modos de Ejecucion de Comisiones";
	public static final String GRUPO_CAT_AGTE_TIPOS_BENEFICIARIO = "Tipo de Beneficiario";

    public GrupoCatalogoAgente() {
    }


	@Id
	@Column(name="ID",nullable=false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="DESCRIPCION",nullable=false,length=80)
	@Size(min=1,max=80)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	//bi-directional many-to-one association to ValorCatalogoAgentes
	@OneToMany(mappedBy="grupoCatalogoAgente",fetch=FetchType.LAZY)
	public List<ValorCatalogoAgentes> getValorCatalogoAgentes() {
		return this.valorCatalogoAgentes;
	}

	public void setValorCatalogoAgentes(List<ValorCatalogoAgentes> valorCatalogoAgentes) {
		this.valorCatalogoAgentes = valorCatalogoAgentes;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}


	@Override
	public String getValue() {
		return descripcion;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	
}