package mx.com.afirme.midas2.domain.catalogos.fuerzaventa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.EditItemChecks;


/**
 * The persistent class for the TOVALORCATALOGOAGENTES database table.
 * 
 */
@Entity(name="ValorCatalogoAgentes")
@Table(name="TOVALORCATALOGOAGENTES", schema="MIDAS")
public class ValorCatalogoAgentes implements Serializable,Entidad {
	private static final long serialVersionUID = 1L;
	private Long id;
	private Long idRegistro;
	private String valor;
	private GrupoCatalogoAgente grupoCatalogoAgente;
	private String clave;
	private Integer checado;

	public ValorCatalogoAgentes() {
    }


	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTOVALORCATALOGOAGENTES_SEQ")
	@SequenceGenerator(name="IDTOVALORCATALOGOAGENTES_SEQ", sequenceName="MIDAS.IDTOVALORCATALOGOAGENTES_SEQ",allocationSize=1)
	@Column(name="ID",nullable=false,unique=true)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Digits(integer=4,fraction=0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	//bi-directional many-to-one association to GrupoCatalogoAgente
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="GRUPOCATALOGOAGENTES_ID")
	public GrupoCatalogoAgente getGrupoCatalogoAgente() {
		return this.grupoCatalogoAgente;
	}

	public void setGrupoCatalogoAgente(GrupoCatalogoAgente grupoCatalogoAgente) {
		this.grupoCatalogoAgente = grupoCatalogoAgente;
	}

	@Column(name="IDREGISTRO",nullable=false)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Long getIdRegistro() {
		return idRegistro;
	}


	public void setIdRegistro(Long idRegistro) {
		this.idRegistro = idRegistro;
	}
	
	@Column(name="VALOR",length=80)
	@Size(min=0,max=80)
	public String getValor() {
		return valor;
	}


	public void setValor(String valor) {
		this.valor = valor;
	}
	@Column(name="CLAVE",length=10)
	public String getClave() {
		return clave;
	}


	public void setClave(String clave) {
		this.clave = clave;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}


	@Override
	public String getValue() {
		return valor;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}
	
	@Transient
	 public Integer getChecado() {
			return checado;
		}

		public void setChecado(Integer checado) {
			this.checado = checado;
		}
}