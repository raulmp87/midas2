package mx.com.afirme.midas2.domain.envioxml;

import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;


@Entity(name="EnvioFactura")
@Table(name="TOENVIOXML", schema="MIDAS")
public class EnvioFactura implements Serializable, Entidad {

	/**
	 * 
	 */
	 private static final long serialVersionUID = 1L;
	 
	 private Long idEnvio;
	 private Date fechaEnvio;
	 private boolean valid;
	 private boolean fisicamenteEnRepositorio;
	 private Integer numeroErrores;
	 private Integer estatusReporte;
	 private Integer estatusEnvio;
	 private Long idOrigenEnvio;
	 private String origenEnvio;
	 
	 private String fechaCfdi;
	 private String folioCfdi;
	 private String importeCfdi;
	 private String monedaCfdi;
	 private String rfcEmisorCfdi;
	 private String rfcReceptorCfdi;
	 private String serieCfdi;
	 private String tipoCambioCfdi;
	 private String uuidCfdi;
	 
	 //Datos para el envio del comprobante a Axosnet
	 private String comprobante;
	 private String razonSocial;
	 private String rfcAxosnet;
	 /**
	  * importe se compara contra subtotal de comprobante
	  */
	 private String importe;
	 /**
	  * el iva se compara contra la suma de todos los importes iva del CFDI
	  */
	 private String iva;
	 private String isr;
	 private String ivaRetenido;
	 /**
	  * iva trasladado es el iva normal
	  */
	 private String ivaTrasladado;
	 private String descuento;
	 private String total;
	 private String tolerancia;
	 private String idSociedad;
	 private String idDepartamento;
	 private String referencia;
	 private String concepto;
	 private String reaseguroTipo;
	 private String cuentaAfectada;
	 private String tipoComprobante;
	 private String uuidDocRelacionado;
	 private BigDecimal importeSaldoAnt;
	 private BigDecimal importePagado;
	 private BigDecimal importeSaldoInsoltulo;
	 private String formaPagoPago;
	 private String nomedaPago;
	 private float tipoCambioPago;
	 private Integer numeroParcialidad;
	 private String numeroFactura;
	 private Date fechaPagoP;
	 private Long idLote;



	/**
	  * Comprobante en forma de file
	  */
	 private File facturaXml;
	 
	 
//	 Respuestas que nos proporciona axosnet
	 private List<EnvioFacturaDet> respuestas = new LinkedList<EnvioFacturaDet>();
	 	 
	 public EnvioFactura(){}
	 
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDENVIOXML_SEQ")
	@SequenceGenerator(name="IDENVIOXML_SEQ", schema="MIDAS", sequenceName="IDENVIOXML_SEQ", allocationSize=1)
	@Column(name="IDENVIO", nullable=false)
	public Long getIdEnvio() {
		return idEnvio;
	}
		
	public void setIdEnvio(Long idEnvio) {
		this.idEnvio = idEnvio;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHAENVIO")
	public Date getFechaEnvio() {
		return fechaEnvio;
	}
	
	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}
	
	@Column(name="ESTATUSENVIO")
	public Integer getEstatusEnvio() {
		return estatusEnvio;
	}

	public void setEstatusEnvio(Integer estatusEnvio) {
		this.estatusEnvio = estatusEnvio;
	}

	@Column(name="IDORIGENENVIO")
	public Long getIdOrigenEnvio() {
		return idOrigenEnvio;
	}

	public void setIdOrigenEnvio(Long idOrigenEnvio) {
		this.idOrigenEnvio = idOrigenEnvio;
	}

	@Column(name="ORIGENENVIO")
	public String getOrigenEnvio() {
		return origenEnvio;
	}

	public void setOrigenEnvio(String origenEnvio) {
		this.origenEnvio = origenEnvio;
	}

	@Column(name="FECHA_CFDI")
	public String getFechaCfdi() {
		return fechaCfdi;
	}

	public void setFechaCfdi(String fechaCfdi) {
		this.fechaCfdi = fechaCfdi;
	}

	@Column(name="FOLIO_CFDI")
	public String getFolioCfdi() {
		return folioCfdi;
	}

	public void setFolioCfdi(String folioCfdi) {
		this.folioCfdi = folioCfdi;
	}

	@Column(name="IMPORTE_CFDI")
	public String getImporteCfdi() {
		return importeCfdi;
	}

	public void setImporteCfdi(String importeCfdi) {
		this.importeCfdi = importeCfdi;
	}

	@Column(name="MONEDA_CFDI")
	public String getMonedaCfdi() {
		return monedaCfdi;
	}

	public void setMonedaCfdi(String monedaCfdi) {
		this.monedaCfdi = monedaCfdi;
	}

	@Column(name="RFC_EMISOR_CFDI")
	public String getRfcEmisorCfdi() {
		return rfcEmisorCfdi;
	}

	public void setRfcEmisorCfdi(String rfcEmisorCfdi) {
		this.rfcEmisorCfdi = rfcEmisorCfdi;
	}

	@Column(name="RFC_RECEPTOR_CFDI")
	public String getRfcReceptorCfdi() {
		return rfcReceptorCfdi;
	}

	public void setRfcReceptorCfdi(String rfcReceptorCfdi) {
		this.rfcReceptorCfdi = rfcReceptorCfdi;
	}

	@Column(name="SERIE_CFDI")
	public String getSerieCfdi() {
		return serieCfdi;
	}

	public void setSerieCfdi(String serieCfdi) {
		this.serieCfdi = serieCfdi;
	}

	@Column(name="TIPO_CAMBIO_CFDI")
	public String getTipoCambioCfdi() {
		return tipoCambioCfdi;
	}

	public void setTipoCambioCfdi(String tipoCambioCfdi) {
		this.tipoCambioCfdi = tipoCambioCfdi;
	}

	@Column(name="UUID_CFDI")
	public String getUuidCfdi() {
		return uuidCfdi;
	}

	public void setUuidCfdi(String uuidCfdi) {
		this.uuidCfdi = uuidCfdi;
	}

	@Transient
	public List<EnvioFacturaDet> getRespuestas() {
		return respuestas;
	}

	public void setRespuestas(List<EnvioFacturaDet> respuestas) {
		this.respuestas = respuestas;
	}
	
	@Transient
	public String getComprobante() {
		return comprobante;
	}

	public void setComprobante(String comprobante) {
		this.comprobante = comprobante;
	}

	@Transient
	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	@Transient
	public String getImporte() {
		return importe;
	}

	@Transient
	public String getRfcAxosnet() {
		return rfcAxosnet;
	}

	public void setRfcAxosnet(String rfcAxosnet) {
		this.rfcAxosnet = rfcAxosnet;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	@Transient
	public String getIva() {
		return iva;
	}

	public void setIva(String iva) {
		this.iva = iva;
	}

	@Transient
	public String getIsr() {
		return isr;
	}

	public void setIsr(String isr) {
		this.isr = isr;
	}

	@Transient
	public String getIvaRetenido() {
		return ivaRetenido;
	}

	public void setIvaRetenido(String ivaRetenido) {
		this.ivaRetenido = ivaRetenido;
	}

	@Transient
	public String getIvaTrasladado() {
		return ivaTrasladado;
	}

	public void setIvaTrasladado(String ivaTrasladado) {
		this.ivaTrasladado = ivaTrasladado;
	}

	@Transient
	public String getDescuento() {
		return descuento;
	}

	public void setDescuento(String descuento) {
		this.descuento = descuento;
	}

	@Transient
	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	@Transient
	public String getTolerancia() {
		return tolerancia;
	}

	public void setTolerancia(String tolerancia) {
		this.tolerancia = tolerancia;
	}

	@Transient
	public String getIdSociedad() {
		return idSociedad;
	}

	public void setIdSociedad(String idSociedad) {
		this.idSociedad = idSociedad;
	}

	@Transient
	public String getIdDepartamento() {
		return idDepartamento;
	}

	public void setIdDepartamento(String idDepartamento) {
		this.idDepartamento = idDepartamento;
	}

	@Column(name="REFERENCIA")
	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	
	@Transient
	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public void setFacturaXml(File facturaXml) {
		this.facturaXml = facturaXml;
	}

	@Transient
	public File getFacturaXml() {
		return facturaXml;
	}
	
	@Transient
	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	/**
	 * @return el fisicamenteEnRepositorio
	 */
	@Transient
	public boolean isFisicamenteEnRepositorio() {
		return fisicamenteEnRepositorio;
	}

	/**
	 * @param fisicamenteEnRepositorio el fisicamenteEnRepositorio a establecer
	 */
	public void setFisicamenteEnRepositorio(boolean fisicamenteEnRepositorio) {
		this.fisicamenteEnRepositorio = fisicamenteEnRepositorio;
	}

	/**
	 * @return el numeroErrores
	 */
	@Transient
	public Integer getNumeroErrores() {
		return numeroErrores;
	}

	/**
	 * @param numeroErrores el numeroErrores a establecer
	 */
	public void setNumeroErrores(Integer numeroErrores) {
		this.numeroErrores = numeroErrores;
	}

	@Override
	public <K> K getKey() {
		return null;
	}

	@Override
	public String getValue() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// Auto-generated method stub
		return null;
	}

	public void setEstatusReporte(Integer estatusReporte) {
		this.estatusReporte = estatusReporte;
	}

	@Column(name="ESTATUSREPORTA")
	public Integer getEstatusReporte() {
		return estatusReporte;
	}
	
	@Transient
	public String getReaseguroTipo() {
		return reaseguroTipo;
	}
	
	public void setReaseguroTipo(String reaseguroTipo) {
		this.reaseguroTipo = reaseguroTipo;
	}
	
	@Column(name="CUENTA_AFECTADA")
	public String getCuentaAfectada() {
		return cuentaAfectada;
	}
	
	public void setCuentaAfectada(String cuentaAfectada) {
		this.cuentaAfectada = cuentaAfectada;
	}
	
	@Column(name="TIPOCOMPROBANTE")
	public String getTipoComprobante() {
		return tipoComprobante;
	}

	public void setTipoComprobante(String tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}

	@Column(name="UUID_CFDIPAGO")
	public String getUuidDocRelacionado() {
		return uuidDocRelacionado;
	}

	public void setUuidDocRelacionado(String uuidDocRelacionado) {
		this.uuidDocRelacionado = uuidDocRelacionado;
	}
	@Column(name="IMPSALDOANT")
	 public BigDecimal getImporteSaldoAnt() {
		return importeSaldoAnt;
	}

	public void setImporteSaldoAnt(BigDecimal importeSaldoAnt) {
		this.importeSaldoAnt = importeSaldoAnt;
	}
	
	@Column(name="IMPPAGADO")
	public BigDecimal getImportePagado() {
		return importePagado;
	}

	public void setImportePagado(BigDecimal importePagado) {
		this.importePagado = importePagado;
	}

	@Column(name="IMPSALDOINSOLUTO")
	public BigDecimal getImporteSaldoInsoltulo() {
		return importeSaldoInsoltulo;
	}

	public void setImporteSaldoInsoltulo(BigDecimal importeSaldoInsoltulo) {
		this.importeSaldoInsoltulo = importeSaldoInsoltulo;
	}

	@Column(name="FORMADEPAGOP")
	public String getFormaPagoPago() {
		return formaPagoPago;
	}

	public void setFormaPagoPago(String formaPagoPago) {
		this.formaPagoPago = formaPagoPago;
	}

	@Column(name="MONEDAP")
	public String getNomedaPago() {
		return nomedaPago;
	}

	public void setNomedaPago(String nomedaPago) {
		this.nomedaPago = nomedaPago;
	}

	@Column(name="TIPOCAMBIOP")
	public float getTipoCambioPago() {
		return tipoCambioPago;
	}

	public void setTipoCambioPago(float tipoCambioPago) {
		this.tipoCambioPago = tipoCambioPago;
	}

	@Column(name="NUMPARCIALIDAD")
	public Integer getNumeroParcialidad() {
		return numeroParcialidad;
	}

	public void setNumeroParcialidad(Integer numeroParcialidad) {
		this.numeroParcialidad = numeroParcialidad;
	}

	@Transient
	public String getNumeroFactura() {
		return serieCfdi+" "+folioCfdi;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHAPAGOP")
	public Date getFechaPagoP() {
		return fechaPagoP;
	}

	public void setFechaPagoP(Date fechaPagoP) {
		this.fechaPagoP = fechaPagoP;
	}

	@Column(name="IDLOTE")
	public Long getIdLote() {
		return idLote;
	}

	public void setIdLote(Long idLote) {
		this.idLote = idLote;
	}
	
}
