package mx.com.afirme.midas2.domain.envioxml;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;



@Entity(name="EnvioFacturaDet")
@Table(name="TOENVIOXMLDET", schema="MIDAS")
public class EnvioFacturaDet implements Serializable, Entidad {
	
	private Long idEnvioDet;
	private Long idEnvio;
	private Integer idTipoRespuesta;
	private Integer tipoRespuesta;
	private String descripcionRespuesta;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDENVIOXMLDET_SEQ")
	@SequenceGenerator(name="IDENVIOXMLDET_SEQ", schema="MIDAS", sequenceName="IDENVIOXMLDET_SEQ", allocationSize=1)
	@Column(name="IDENVIODET", nullable=false)
	public Long getIdEnvioDet() {
		return idEnvioDet;
	}

	public void setIdEnvioDet(Long idEnvioDet) {
		this.idEnvioDet = idEnvioDet;
	}
	
	@Column(name="IDENVIO")
	public Long getIdEnvio() {
		return idEnvio;
	}

	public void setIdEnvio(Long idEnvio) {
		this.idEnvio = idEnvio;
	}
	
	@Column(name="IDTIPORESPUESTA")
	public Integer getIdTipoRespuesta() {
		return idTipoRespuesta;
	}

	public void setIdTipoRespuesta(Integer idTipoRespuesta) {
		this.idTipoRespuesta = idTipoRespuesta;
	}
	
	@Column(name="TIPORESPUESTA")
	public Integer getTipoRespuesta() {
		return tipoRespuesta;
	}

	public void setTipoRespuesta(Integer tipoRespuesta) {
		this.tipoRespuesta = tipoRespuesta;
	}

	@Column(name="DESCRIPCIONRESPUESTA")
	public String getDescripcionRespuesta() {
		return descripcionRespuesta;
	}

	public void setDescripcionRespuesta(String descripcionRespuesta) {
		this.descripcionRespuesta = descripcionRespuesta;
	}

	@Override
	public <K> K getKey() {
		// Auto-generated method stub
		return null;
	}
	
	@Override
	public String getValue() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// Auto-generated method stub
		return null;
	}
}
