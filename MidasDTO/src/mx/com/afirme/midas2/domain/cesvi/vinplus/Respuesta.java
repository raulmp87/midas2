package mx.com.afirme.midas2.domain.cesvi.vinplus;

import java.math.BigDecimal;

public class Respuesta  implements java.io.Serializable {
	private BigDecimal idToPoliza;
	private BigDecimal idToCotizacion;
	private BigDecimal idToInciso;
	private BigDecimal numeroInciso;
	private Short claveTipoSolicitud;
	private String estiloId;
	private String observacionesSesa;
	
    private java.lang.String CLAVE_UNICA;

    private java.lang.String SEGMENTO;

    private java.lang.String MARCA;

    private java.lang.String MODELO;

    private java.lang.String VERSION_C;

    private java.lang.String ANO;

    private java.lang.String p_LISTA;

    private java.lang.String CLASE;

    private java.lang.String TIPO;

    private java.lang.String PUERTAS;

    private java.lang.String MOTOR;

    private java.lang.String NO_CILINDRO;

    private java.lang.String POTENCIA;

    private java.lang.String COMBUSTIBLE;

    private java.lang.String TRANSMISION;

    private java.lang.String TRACCION;

    private java.lang.String TIPO_DE_DIRECCION;

    private java.lang.String INTERIORES;

    private java.lang.String BOLSA_DE_AIRE;

    private java.lang.String AIRE_ACONDICIONADO;

    private java.lang.String ELEVADORES_ELECTRICOS;

    private java.lang.String QUEMACOCOS;

    private java.lang.String ESTEREO;

    private java.lang.String TIPO_DE_RIN;

    private java.lang.String RIN;

    private java.lang.String NEUMATICO;

    private java.lang.String FRENOS_DEL;

    private java.lang.String SUSPENSION_DELANTERA;

    private java.lang.String SUSPENSION_TRASERA;

    private java.lang.String TIPO_DE_CABINA;

    private java.lang.String TECHO;

    private java.lang.String PASAJEROS;

    private java.lang.String VALVULAS;

    private java.lang.String PRECIO_VENTA;
    
    private java.lang.String CVE_SESA;
    
    private java.lang.String OCRA;
    
    private java.lang.String CVE_AMIS;
    
    private java.lang.String ID_TRANSPORTE;
    
    private java.lang.String CVE_AFIRME;    

    private java.lang.String VIN;

    private java.lang.String RESULTADO;
    
    private java.lang.String PAIS_ORIGEN;

    private java.lang.String PLANTA_ENSAMBLE;
        

    public Respuesta() {
    }

    public Respuesta(
           java.lang.String CLAVE_UNICA,
           java.lang.String SEGMENTO,
           java.lang.String MARCA,
           java.lang.String MODELO,
           java.lang.String VERSION_C,
           java.lang.String ANO,
           java.lang.String p_LISTA,
           java.lang.String CLASE,
           java.lang.String TIPO,
           java.lang.String PUERTAS,
           java.lang.String MOTOR,
           java.lang.String NO_CILINDRO,
           java.lang.String POTENCIA,
           java.lang.String COMBUSTIBLE,
           java.lang.String TRANSMISION,
           java.lang.String TRACCION,
           java.lang.String TIPO_DE_DIRECCION,
           java.lang.String INTERIORES,
           java.lang.String BOLSA_DE_AIRE,
           java.lang.String AIRE_ACONDICIONADO,
           java.lang.String ELEVADORES_ELECTRICOS,
           java.lang.String QUEMACOCOS,
           java.lang.String ESTEREO,
           java.lang.String TIPO_DE_RIN,
           java.lang.String RIN,
           java.lang.String NEUMATICO,
           java.lang.String FRENOS_DEL,
           java.lang.String SUSPENSION_DELANTERA,
           java.lang.String SUSPENSION_TRASERA,
           java.lang.String TIPO_DE_CABINA,
           java.lang.String TECHO,
           java.lang.String PASAJEROS,
           java.lang.String VALVULAS,
           java.lang.String PRECIO_VENTA,
           java.lang.String CVE_SESA,
           java.lang.String OCRA,
           java.lang.String CVE_AMIS,
           java.lang.String ID_TRANSPORTE,
           java.lang.String CVE_AFIRME,
           java.lang.String VIN,
           java.lang.String RESULTADO,
           java.lang.String PAIS_ORIGEN,
           java.lang.String PLANTA_ENSAMBLE) {
           this.CLAVE_UNICA = CLAVE_UNICA;
           this.SEGMENTO = SEGMENTO;
           this.MARCA = MARCA;
           this.MODELO = MODELO;
           this.VERSION_C = VERSION_C;
           this.ANO = ANO;
           this.p_LISTA = p_LISTA;
           this.CLASE = CLASE;
           this.TIPO = TIPO;
           this.PUERTAS = PUERTAS;
           this.MOTOR = MOTOR;
           this.NO_CILINDRO = NO_CILINDRO;
           this.POTENCIA = POTENCIA;
           this.COMBUSTIBLE = COMBUSTIBLE;
           this.TRANSMISION = TRANSMISION;
           this.TRACCION = TRACCION;
           this.TIPO_DE_DIRECCION = TIPO_DE_DIRECCION;
           this.INTERIORES = INTERIORES;
           this.BOLSA_DE_AIRE = BOLSA_DE_AIRE;
           this.AIRE_ACONDICIONADO = AIRE_ACONDICIONADO;
           this.ELEVADORES_ELECTRICOS = ELEVADORES_ELECTRICOS;
           this.QUEMACOCOS = QUEMACOCOS;
           this.ESTEREO = ESTEREO;
           this.TIPO_DE_RIN = TIPO_DE_RIN;
           this.RIN = RIN;
           this.NEUMATICO = NEUMATICO;
           this.FRENOS_DEL = FRENOS_DEL;
           this.SUSPENSION_DELANTERA = SUSPENSION_DELANTERA;
           this.SUSPENSION_TRASERA = SUSPENSION_TRASERA;
           this.TIPO_DE_CABINA = TIPO_DE_CABINA;
           this.TECHO = TECHO;
           this.PASAJEROS = PASAJEROS;
           this.VALVULAS = VALVULAS;
           this.PRECIO_VENTA = PRECIO_VENTA;
           this.CVE_SESA = CVE_SESA;
           this.OCRA = OCRA;
           this.CVE_AMIS = CVE_AMIS;
           this.ID_TRANSPORTE = ID_TRANSPORTE;          
           this.CVE_AFIRME = CVE_AFIRME;
           this.VIN = VIN;
           this.RESULTADO = RESULTADO;
           this.PAIS_ORIGEN = PAIS_ORIGEN;
           this.PLANTA_ENSAMBLE = PLANTA_ENSAMBLE;
    }


    /**
     * Gets the CLAVE_UNICA value for this Respuesta.
     * 
     * @return CLAVE_UNICA
     */
    public java.lang.String getCLAVE_UNICA() {
        return CLAVE_UNICA;
    }


    /**
     * Sets the CLAVE_UNICA value for this Respuesta.
     * 
     * @param CLAVE_UNICA
     */
    public void setCLAVE_UNICA(java.lang.String CLAVE_UNICA) {
        this.CLAVE_UNICA = CLAVE_UNICA;
    }


    /**
     * Gets the SEGMENTO value for this Respuesta.
     * 
     * @return SEGMENTO
     */
    public java.lang.String getSEGMENTO() {
        return SEGMENTO;
    }


    /**
     * Sets the SEGMENTO value for this Respuesta.
     * 
     * @param SEGMENTO
     */
    public void setSEGMENTO(java.lang.String SEGMENTO) {
        this.SEGMENTO = SEGMENTO;
    }


    /**
     * Gets the MARCA value for this Respuesta.
     * 
     * @return MARCA
     */
    public java.lang.String getMARCA() {
        return MARCA;
    }


    /**
     * Sets the MARCA value for this Respuesta.
     * 
     * @param MARCA
     */
    public void setMARCA(java.lang.String MARCA) {
        this.MARCA = MARCA;
    }


    /**
     * Gets the MODELO value for this Respuesta.
     * 
     * @return MODELO
     */
    public java.lang.String getMODELO() {
        return MODELO;
    }


    /**
     * Sets the MODELO value for this Respuesta.
     * 
     * @param MODELO
     */
    public void setMODELO(java.lang.String MODELO) {
        this.MODELO = MODELO;
    }


    /**
     * Gets the VERSION_C value for this Respuesta.
     * 
     * @return VERSION_C
     */
    public java.lang.String getVERSION_C() {
        return VERSION_C;
    }


    /**
     * Sets the VERSION_C value for this Respuesta.
     * 
     * @param VERSION_C
     */
    public void setVERSION_C(java.lang.String VERSION_C) {
        this.VERSION_C = VERSION_C;
    }


    /**
     * Gets the ANO value for this Respuesta.
     * 
     * @return ANO
     */
    public java.lang.String getANO() {
        return ANO;
    }


    /**
     * Sets the ANO value for this Respuesta.
     * 
     * @param ANO
     */
    public void setANO(java.lang.String ANO) {
        this.ANO = ANO;
    }


    /**
     * Gets the p_LISTA value for this Respuesta.
     * 
     * @return p_LISTA
     */
    public java.lang.String getP_LISTA() {
        return p_LISTA;
    }


    /**
     * Sets the p_LISTA value for this Respuesta.
     * 
     * @param p_LISTA
     */
    public void setP_LISTA(java.lang.String p_LISTA) {
        this.p_LISTA = p_LISTA;
    }


    /**
     * Gets the CLASE value for this Respuesta.
     * 
     * @return CLASE
     */
    public java.lang.String getCLASE() {
        return CLASE;
    }


    /**
     * Sets the CLASE value for this Respuesta.
     * 
     * @param CLASE
     */
    public void setCLASE(java.lang.String CLASE) {
        this.CLASE = CLASE;
    }


    /**
     * Gets the TIPO value for this Respuesta.
     * 
     * @return TIPO
     */
    public java.lang.String getTIPO() {
        return TIPO;
    }


    /**
     * Sets the TIPO value for this Respuesta.
     * 
     * @param TIPO
     */
    public void setTIPO(java.lang.String TIPO) {
        this.TIPO = TIPO;
    }


    /**
     * Gets the PUERTAS value for this Respuesta.
     * 
     * @return PUERTAS
     */
    public java.lang.String getPUERTAS() {
        return PUERTAS;
    }


    /**
     * Sets the PUERTAS value for this Respuesta.
     * 
     * @param PUERTAS
     */
    public void setPUERTAS(java.lang.String PUERTAS) {
        this.PUERTAS = PUERTAS;
    }


    /**
     * Gets the MOTOR value for this Respuesta.
     * 
     * @return MOTOR
     */
    public java.lang.String getMOTOR() {
        return MOTOR;
    }


    /**
     * Sets the MOTOR value for this Respuesta.
     * 
     * @param MOTOR
     */
    public void setMOTOR(java.lang.String MOTOR) {
        this.MOTOR = MOTOR;
    }


    /**
     * Gets the NO_CILINDRO value for this Respuesta.
     * 
     * @return NO_CILINDRO
     */
    public java.lang.String getNO_CILINDRO() {
        return NO_CILINDRO;
    }


    /**
     * Sets the NO_CILINDRO value for this Respuesta.
     * 
     * @param NO_CILINDRO
     */
    public void setNO_CILINDRO(java.lang.String NO_CILINDRO) {
        this.NO_CILINDRO = NO_CILINDRO;
    }


    /**
     * Gets the POTENCIA value for this Respuesta.
     * 
     * @return POTENCIA
     */
    public java.lang.String getPOTENCIA() {
        return POTENCIA;
    }


    /**
     * Sets the POTENCIA value for this Respuesta.
     * 
     * @param POTENCIA
     */
    public void setPOTENCIA(java.lang.String POTENCIA) {
        this.POTENCIA = POTENCIA;
    }


    /**
     * Gets the COMBUSTIBLE value for this Respuesta.
     * 
     * @return COMBUSTIBLE
     */
    public java.lang.String getCOMBUSTIBLE() {
        return COMBUSTIBLE;
    }


    /**
     * Sets the COMBUSTIBLE value for this Respuesta.
     * 
     * @param COMBUSTIBLE
     */
    public void setCOMBUSTIBLE(java.lang.String COMBUSTIBLE) {
        this.COMBUSTIBLE = COMBUSTIBLE;
    }


    /**
     * Gets the TRANSMISION value for this Respuesta.
     * 
     * @return TRANSMISION
     */
    public java.lang.String getTRANSMISION() {
        return TRANSMISION;
    }


    /**
     * Sets the TRANSMISION value for this Respuesta.
     * 
     * @param TRANSMISION
     */
    public void setTRANSMISION(java.lang.String TRANSMISION) {
        this.TRANSMISION = TRANSMISION;
    }


    /**
     * Gets the TRACCION value for this Respuesta.
     * 
     * @return TRACCION
     */
    public java.lang.String getTRACCION() {
        return TRACCION;
    }


    /**
     * Sets the TRACCION value for this Respuesta.
     * 
     * @param TRACCION
     */
    public void setTRACCION(java.lang.String TRACCION) {
        this.TRACCION = TRACCION;
    }


    /**
     * Gets the TIPO_DE_DIRECCION value for this Respuesta.
     * 
     * @return TIPO_DE_DIRECCION
     */
    public java.lang.String getTIPO_DE_DIRECCION() {
        return TIPO_DE_DIRECCION;
    }


    /**
     * Sets the TIPO_DE_DIRECCION value for this Respuesta.
     * 
     * @param TIPO_DE_DIRECCION
     */
    public void setTIPO_DE_DIRECCION(java.lang.String TIPO_DE_DIRECCION) {
        this.TIPO_DE_DIRECCION = TIPO_DE_DIRECCION;
    }


    /**
     * Gets the INTERIORES value for this Respuesta.
     * 
     * @return INTERIORES
     */
    public java.lang.String getINTERIORES() {
        return INTERIORES;
    }


    /**
     * Sets the INTERIORES value for this Respuesta.
     * 
     * @param INTERIORES
     */
    public void setINTERIORES(java.lang.String INTERIORES) {
        this.INTERIORES = INTERIORES;
    }


    /**
     * Gets the BOLSA_DE_AIRE value for this Respuesta.
     * 
     * @return BOLSA_DE_AIRE
     */
    public java.lang.String getBOLSA_DE_AIRE() {
        return BOLSA_DE_AIRE;
    }


    /**
     * Sets the BOLSA_DE_AIRE value for this Respuesta.
     * 
     * @param BOLSA_DE_AIRE
     */
    public void setBOLSA_DE_AIRE(java.lang.String BOLSA_DE_AIRE) {
        this.BOLSA_DE_AIRE = BOLSA_DE_AIRE;
    }


    /**
     * Gets the AIRE_ACONDICIONADO value for this Respuesta.
     * 
     * @return AIRE_ACONDICIONADO
     */
    public java.lang.String getAIRE_ACONDICIONADO() {
        return AIRE_ACONDICIONADO;
    }


    /**
     * Sets the AIRE_ACONDICIONADO value for this Respuesta.
     * 
     * @param AIRE_ACONDICIONADO
     */
    public void setAIRE_ACONDICIONADO(java.lang.String AIRE_ACONDICIONADO) {
        this.AIRE_ACONDICIONADO = AIRE_ACONDICIONADO;
    }


    /**
     * Gets the ELEVADORES_ELECTRICOS value for this Respuesta.
     * 
     * @return ELEVADORES_ELECTRICOS
     */
    public java.lang.String getELEVADORES_ELECTRICOS() {
        return ELEVADORES_ELECTRICOS;
    }


    /**
     * Sets the ELEVADORES_ELECTRICOS value for this Respuesta.
     * 
     * @param ELEVADORES_ELECTRICOS
     */
    public void setELEVADORES_ELECTRICOS(java.lang.String ELEVADORES_ELECTRICOS) {
        this.ELEVADORES_ELECTRICOS = ELEVADORES_ELECTRICOS;
    }


    /**
     * Gets the QUEMACOCOS value for this Respuesta.
     * 
     * @return QUEMACOCOS
     */
    public java.lang.String getQUEMACOCOS() {
        return QUEMACOCOS;
    }


    /**
     * Sets the QUEMACOCOS value for this Respuesta.
     * 
     * @param QUEMACOCOS
     */
    public void setQUEMACOCOS(java.lang.String QUEMACOCOS) {
        this.QUEMACOCOS = QUEMACOCOS;
    }


    /**
     * Gets the ESTEREO value for this Respuesta.
     * 
     * @return ESTEREO
     */
    public java.lang.String getESTEREO() {
        return ESTEREO;
    }


    /**
     * Sets the ESTEREO value for this Respuesta.
     * 
     * @param ESTEREO
     */
    public void setESTEREO(java.lang.String ESTEREO) {
        this.ESTEREO = ESTEREO;
    }


    /**
     * Gets the TIPO_DE_RIN value for this Respuesta.
     * 
     * @return TIPO_DE_RIN
     */
    public java.lang.String getTIPO_DE_RIN() {
        return TIPO_DE_RIN;
    }


    /**
     * Sets the TIPO_DE_RIN value for this Respuesta.
     * 
     * @param TIPO_DE_RIN
     */
    public void setTIPO_DE_RIN(java.lang.String TIPO_DE_RIN) {
        this.TIPO_DE_RIN = TIPO_DE_RIN;
    }


    /**
     * Gets the RIN value for this Respuesta.
     * 
     * @return RIN
     */
    public java.lang.String getRIN() {
        return RIN;
    }


    /**
     * Sets the RIN value for this Respuesta.
     * 
     * @param RIN
     */
    public void setRIN(java.lang.String RIN) {
        this.RIN = RIN;
    }


    /**
     * Gets the NEUMATICO value for this Respuesta.
     * 
     * @return NEUMATICO
     */
    public java.lang.String getNEUMATICO() {
        return NEUMATICO;
    }


    /**
     * Sets the NEUMATICO value for this Respuesta.
     * 
     * @param NEUMATICO
     */
    public void setNEUMATICO(java.lang.String NEUMATICO) {
        this.NEUMATICO = NEUMATICO;
    }


    /**
     * Gets the FRENOS_DEL value for this Respuesta.
     * 
     * @return FRENOS_DEL
     */
    public java.lang.String getFRENOS_DEL() {
        return FRENOS_DEL;
    }


    /**
     * Sets the FRENOS_DEL value for this Respuesta.
     * 
     * @param FRENOS_DEL
     */
    public void setFRENOS_DEL(java.lang.String FRENOS_DEL) {
        this.FRENOS_DEL = FRENOS_DEL;
    }


    /**
     * Gets the SUSPENSION_DELANTERA value for this Respuesta.
     * 
     * @return SUSPENSION_DELANTERA
     */
    public java.lang.String getSUSPENSION_DELANTERA() {
        return SUSPENSION_DELANTERA;
    }


    /**
     * Sets the SUSPENSION_DELANTERA value for this Respuesta.
     * 
     * @param SUSPENSION_DELANTERA
     */
    public void setSUSPENSION_DELANTERA(java.lang.String SUSPENSION_DELANTERA) {
        this.SUSPENSION_DELANTERA = SUSPENSION_DELANTERA;
    }


    /**
     * Gets the SUSPENSION_TRASERA value for this Respuesta.
     * 
     * @return SUSPENSION_TRASERA
     */
    public java.lang.String getSUSPENSION_TRASERA() {
        return SUSPENSION_TRASERA;
    }


    /**
     * Sets the SUSPENSION_TRASERA value for this Respuesta.
     * 
     * @param SUSPENSION_TRASERA
     */
    public void setSUSPENSION_TRASERA(java.lang.String SUSPENSION_TRASERA) {
        this.SUSPENSION_TRASERA = SUSPENSION_TRASERA;
    }


    /**
     * Gets the TIPO_DE_CABINA value for this Respuesta.
     * 
     * @return TIPO_DE_CABINA
     */
    public java.lang.String getTIPO_DE_CABINA() {
        return TIPO_DE_CABINA;
    }


    /**
     * Sets the TIPO_DE_CABINA value for this Respuesta.
     * 
     * @param TIPO_DE_CABINA
     */
    public void setTIPO_DE_CABINA(java.lang.String TIPO_DE_CABINA) {
        this.TIPO_DE_CABINA = TIPO_DE_CABINA;
    }


    /**
     * Gets the TECHO value for this Respuesta.
     * 
     * @return TECHO
     */
    public java.lang.String getTECHO() {
        return TECHO;
    }


    /**
     * Sets the TECHO value for this Respuesta.
     * 
     * @param TECHO
     */
    public void setTECHO(java.lang.String TECHO) {
        this.TECHO = TECHO;
    }


    /**
     * Gets the PASAJEROS value for this Respuesta.
     * 
     * @return PASAJEROS
     */
    public java.lang.String getPASAJEROS() {
        return PASAJEROS;
    }


    /**
     * Sets the PASAJEROS value for this Respuesta.
     * 
     * @param PASAJEROS
     */
    public void setPASAJEROS(java.lang.String PASAJEROS) {
        this.PASAJEROS = PASAJEROS;
    }


    /**
     * Gets the VALVULAS value for this Respuesta.
     * 
     * @return VALVULAS
     */
    public java.lang.String getVALVULAS() {
        return VALVULAS;
    }


    /**
     * Sets the VALVULAS value for this Respuesta.
     * 
     * @param VALVULAS
     */
    public void setVALVULAS(java.lang.String VALVULAS) {
        this.VALVULAS = VALVULAS;
    }


    /**
     * Gets the PRECIO_VENTA value for this Respuesta.
     * 
     * @return PRECIO_VENTA
     */
    public java.lang.String getPRECIO_VENTA() {
        return PRECIO_VENTA;
    }


    /**
     * Sets the PRECIO_VENTA value for this Respuesta.
     * 
     * @param PRECIO_VENTA
     */
    public void setPRECIO_VENTA(java.lang.String PRECIO_VENTA) {
        this.PRECIO_VENTA = PRECIO_VENTA;
    }


    /**
     * Gets the VIN value for this Respuesta.
     * 
     * @return VIN
     */
    public java.lang.String getVIN() {
        return VIN;
    }


    /**
     * Sets the VIN value for this Respuesta.
     * 
     * @param VIN
     */
    public void setVIN(java.lang.String VIN) {
        this.VIN = VIN;
    }


    /**
     * Gets the RESULTADO value for this Respuesta.
     * 
     * @return RESULTADO
     */
    public java.lang.String getRESULTADO() {
        return RESULTADO;
    }


    /**
     * Sets the RESULTADO value for this Respuesta.
     * 
     * @param RESULTADO
     */
    public void setRESULTADO(java.lang.String RESULTADO) {
        this.RESULTADO = RESULTADO;
    }


    /**
     * Gets the CVE_AMIS value for this Respuesta.
     * 
     * @return CVE_AMIS
     */
    public java.lang.String getCVE_AMIS() {
        return CVE_AMIS;
    }


    /**
     * Sets the CVE_AMIS value for this Respuesta.
     * 
     * @param CVE_AMIS
     */
    public void setCVE_AMIS(java.lang.String CVE_AMIS) {
        this.CVE_AMIS = CVE_AMIS;
    }


    /**
     * Gets the CVE_SESA value for this Respuesta.
     * 
     * @return CVE_SESA
     */
    public java.lang.String getCVE_SESA() {
        return CVE_SESA;
    }


    /**
     * Sets the CVE_SESA value for this Respuesta.
     * 
     * @param CVE_SESA
     */
    public void setCVE_SESA(java.lang.String CVE_SESA) {
        this.CVE_SESA = CVE_SESA;
    }


    /**
     * Gets the CVE_AFIRME value for this Respuesta.
     * 
     * @return CVE_AFIRME
     */
    public java.lang.String getCVE_AFIRME() {
        return CVE_AFIRME;
    }


    /**
     * Sets the CVE_AFIRME value for this Respuesta.
     * 
     * @param CVE_AFIRME
     */
    public void setCVE_AFIRME(java.lang.String CVE_AFIRME) {
        this.CVE_AFIRME = CVE_AFIRME;
    }

    public java.lang.String getOCRA() {
		return OCRA;
	}

	public void setOCRA(java.lang.String oCRA) {
		OCRA = oCRA;
	}

	public java.lang.String getID_TRANSPORTE() {
		return ID_TRANSPORTE;
	}

	public void setID_TRANSPORTE(java.lang.String iD_TRANSPORTE) {
		ID_TRANSPORTE = iD_TRANSPORTE;
	}

	public java.lang.String getPAIS_ORIGEN() {
		return PAIS_ORIGEN;
	}

	public void setPAIS_ORIGEN(java.lang.String pAIS_ORIGEN) {
		PAIS_ORIGEN = pAIS_ORIGEN;
	}

	public java.lang.String getPLANTA_ENSAMBLE() {
		return PLANTA_ENSAMBLE;
	}

	public void setPLANTA_ENSAMBLE(java.lang.String pLANTA_ENSAMBLE) {
		PLANTA_ENSAMBLE = pLANTA_ENSAMBLE;
	}

	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public BigDecimal getIdToInciso() {
		return idToInciso;
	}

	public void setIdToInciso(BigDecimal idToInciso) {
		this.idToInciso = idToInciso;
	}

	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public Short getClaveTipoSolicitud() {
		return claveTipoSolicitud;
	}

	public void setClaveTipoSolicitud(Short claveTipoSolicitud) {
		this.claveTipoSolicitud = claveTipoSolicitud;
	}

	public String getEstiloId() {
		return estiloId;
	}

	public void setEstiloId(String estiloId) {
		this.estiloId = estiloId;
	}

	public String getObservacionesSesa() {
		return observacionesSesa;
	}

	public void setObservacionesSesa(String observacionesSesa) {
		this.observacionesSesa = observacionesSesa;
	}

	private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Respuesta)) return false;
        Respuesta other = (Respuesta) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.CLAVE_UNICA==null && other.getCLAVE_UNICA()==null) || 
             (this.CLAVE_UNICA!=null &&
              this.CLAVE_UNICA.equals(other.getCLAVE_UNICA()))) &&
            ((this.SEGMENTO==null && other.getSEGMENTO()==null) || 
             (this.SEGMENTO!=null &&
              this.SEGMENTO.equals(other.getSEGMENTO()))) &&
            ((this.MARCA==null && other.getMARCA()==null) || 
             (this.MARCA!=null &&
              this.MARCA.equals(other.getMARCA()))) &&
            ((this.MODELO==null && other.getMODELO()==null) || 
             (this.MODELO!=null &&
              this.MODELO.equals(other.getMODELO()))) &&
            ((this.VERSION_C==null && other.getVERSION_C()==null) || 
             (this.VERSION_C!=null &&
              this.VERSION_C.equals(other.getVERSION_C()))) &&
            ((this.ANO==null && other.getANO()==null) || 
             (this.ANO!=null &&
              this.ANO.equals(other.getANO()))) &&
            ((this.p_LISTA==null && other.getP_LISTA()==null) || 
             (this.p_LISTA!=null &&
              this.p_LISTA.equals(other.getP_LISTA()))) &&
            ((this.CLASE==null && other.getCLASE()==null) || 
             (this.CLASE!=null &&
              this.CLASE.equals(other.getCLASE()))) &&
            ((this.TIPO==null && other.getTIPO()==null) || 
             (this.TIPO!=null &&
              this.TIPO.equals(other.getTIPO()))) &&
            ((this.PUERTAS==null && other.getPUERTAS()==null) || 
             (this.PUERTAS!=null &&
              this.PUERTAS.equals(other.getPUERTAS()))) &&
            ((this.MOTOR==null && other.getMOTOR()==null) || 
             (this.MOTOR!=null &&
              this.MOTOR.equals(other.getMOTOR()))) &&
            ((this.NO_CILINDRO==null && other.getNO_CILINDRO()==null) || 
             (this.NO_CILINDRO!=null &&
              this.NO_CILINDRO.equals(other.getNO_CILINDRO()))) &&
            ((this.POTENCIA==null && other.getPOTENCIA()==null) || 
             (this.POTENCIA!=null &&
              this.POTENCIA.equals(other.getPOTENCIA()))) &&
            ((this.COMBUSTIBLE==null && other.getCOMBUSTIBLE()==null) || 
             (this.COMBUSTIBLE!=null &&
              this.COMBUSTIBLE.equals(other.getCOMBUSTIBLE()))) &&
            ((this.TRANSMISION==null && other.getTRANSMISION()==null) || 
             (this.TRANSMISION!=null &&
              this.TRANSMISION.equals(other.getTRANSMISION()))) &&
            ((this.TRACCION==null && other.getTRACCION()==null) || 
             (this.TRACCION!=null &&
              this.TRACCION.equals(other.getTRACCION()))) &&
            ((this.TIPO_DE_DIRECCION==null && other.getTIPO_DE_DIRECCION()==null) || 
             (this.TIPO_DE_DIRECCION!=null &&
              this.TIPO_DE_DIRECCION.equals(other.getTIPO_DE_DIRECCION()))) &&
            ((this.INTERIORES==null && other.getINTERIORES()==null) || 
             (this.INTERIORES!=null &&
              this.INTERIORES.equals(other.getINTERIORES()))) &&
            ((this.BOLSA_DE_AIRE==null && other.getBOLSA_DE_AIRE()==null) || 
             (this.BOLSA_DE_AIRE!=null &&
              this.BOLSA_DE_AIRE.equals(other.getBOLSA_DE_AIRE()))) &&
            ((this.AIRE_ACONDICIONADO==null && other.getAIRE_ACONDICIONADO()==null) || 
             (this.AIRE_ACONDICIONADO!=null &&
              this.AIRE_ACONDICIONADO.equals(other.getAIRE_ACONDICIONADO()))) &&
            ((this.ELEVADORES_ELECTRICOS==null && other.getELEVADORES_ELECTRICOS()==null) || 
             (this.ELEVADORES_ELECTRICOS!=null &&
              this.ELEVADORES_ELECTRICOS.equals(other.getELEVADORES_ELECTRICOS()))) &&
            ((this.QUEMACOCOS==null && other.getQUEMACOCOS()==null) || 
             (this.QUEMACOCOS!=null &&
              this.QUEMACOCOS.equals(other.getQUEMACOCOS()))) &&
            ((this.ESTEREO==null && other.getESTEREO()==null) || 
             (this.ESTEREO!=null &&
              this.ESTEREO.equals(other.getESTEREO()))) &&
            ((this.TIPO_DE_RIN==null && other.getTIPO_DE_RIN()==null) || 
             (this.TIPO_DE_RIN!=null &&
              this.TIPO_DE_RIN.equals(other.getTIPO_DE_RIN()))) &&
            ((this.RIN==null && other.getRIN()==null) || 
             (this.RIN!=null &&
              this.RIN.equals(other.getRIN()))) &&
            ((this.NEUMATICO==null && other.getNEUMATICO()==null) || 
             (this.NEUMATICO!=null &&
              this.NEUMATICO.equals(other.getNEUMATICO()))) &&
            ((this.FRENOS_DEL==null && other.getFRENOS_DEL()==null) || 
             (this.FRENOS_DEL!=null &&
              this.FRENOS_DEL.equals(other.getFRENOS_DEL()))) &&
            ((this.SUSPENSION_DELANTERA==null && other.getSUSPENSION_DELANTERA()==null) || 
             (this.SUSPENSION_DELANTERA!=null &&
              this.SUSPENSION_DELANTERA.equals(other.getSUSPENSION_DELANTERA()))) &&
            ((this.SUSPENSION_TRASERA==null && other.getSUSPENSION_TRASERA()==null) || 
             (this.SUSPENSION_TRASERA!=null &&
              this.SUSPENSION_TRASERA.equals(other.getSUSPENSION_TRASERA()))) &&
            ((this.TIPO_DE_CABINA==null && other.getTIPO_DE_CABINA()==null) || 
             (this.TIPO_DE_CABINA!=null &&
              this.TIPO_DE_CABINA.equals(other.getTIPO_DE_CABINA()))) &&
            ((this.TECHO==null && other.getTECHO()==null) || 
             (this.TECHO!=null &&
              this.TECHO.equals(other.getTECHO()))) &&
            ((this.PASAJEROS==null && other.getPASAJEROS()==null) || 
             (this.PASAJEROS!=null &&
              this.PASAJEROS.equals(other.getPASAJEROS()))) &&
            ((this.VALVULAS==null && other.getVALVULAS()==null) || 
             (this.VALVULAS!=null &&
              this.VALVULAS.equals(other.getVALVULAS()))) &&
            ((this.PRECIO_VENTA==null && other.getPRECIO_VENTA()==null) || 
             (this.PRECIO_VENTA!=null &&
              this.PRECIO_VENTA.equals(other.getPRECIO_VENTA()))) &&
            ((this.VIN==null && other.getVIN()==null) || 
             (this.VIN!=null &&
              this.VIN.equals(other.getVIN()))) &&
            ((this.RESULTADO==null && other.getRESULTADO()==null) || 
             (this.RESULTADO!=null &&
              this.RESULTADO.equals(other.getRESULTADO()))) &&
            ((this.CVE_AMIS==null && other.getCVE_AMIS()==null) || 
             (this.CVE_AMIS!=null &&
              this.CVE_AMIS.equals(other.getCVE_AMIS()))) &&
            ((this.CVE_SESA==null && other.getCVE_SESA()==null) || 
             (this.CVE_SESA!=null &&
              this.CVE_SESA.equals(other.getCVE_SESA()))) &&
            ((this.CVE_AFIRME==null && other.getCVE_AFIRME()==null) || 
             (this.CVE_AFIRME!=null &&
              this.CVE_AFIRME.equals(other.getCVE_AFIRME()))) &&
            ((this.OCRA==null && other.getOCRA()==null) || 
             (this.OCRA!=null &&
              this.OCRA.equals(other.getOCRA()))) &&
            ((this.ID_TRANSPORTE==null && other.getID_TRANSPORTE()==null) || 
             (this.ID_TRANSPORTE!=null &&
              this.ID_TRANSPORTE.equals(other.getID_TRANSPORTE()))) &&
            ((this.PAIS_ORIGEN==null && other.getPAIS_ORIGEN()==null) || 
             (this.PAIS_ORIGEN!=null &&
              this.PAIS_ORIGEN.equals(other.getPAIS_ORIGEN())))  &&
            ((this.PLANTA_ENSAMBLE==null && other.getPLANTA_ENSAMBLE()==null) || 
             (this.PLANTA_ENSAMBLE!=null &&
              this.PLANTA_ENSAMBLE.equals(other.getPLANTA_ENSAMBLE())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCLAVE_UNICA() != null) {
            _hashCode += getCLAVE_UNICA().hashCode();
        }
        if (getSEGMENTO() != null) {
            _hashCode += getSEGMENTO().hashCode();
        }
        if (getMARCA() != null) {
            _hashCode += getMARCA().hashCode();
        }
        if (getMODELO() != null) {
            _hashCode += getMODELO().hashCode();
        }
        if (getVERSION_C() != null) {
            _hashCode += getVERSION_C().hashCode();
        }
        if (getANO() != null) {
            _hashCode += getANO().hashCode();
        }
        if (getP_LISTA() != null) {
            _hashCode += getP_LISTA().hashCode();
        }
        if (getCLASE() != null) {
            _hashCode += getCLASE().hashCode();
        }
        if (getTIPO() != null) {
            _hashCode += getTIPO().hashCode();
        }
        if (getPUERTAS() != null) {
            _hashCode += getPUERTAS().hashCode();
        }
        if (getMOTOR() != null) {
            _hashCode += getMOTOR().hashCode();
        }
        if (getNO_CILINDRO() != null) {
            _hashCode += getNO_CILINDRO().hashCode();
        }
        if (getPOTENCIA() != null) {
            _hashCode += getPOTENCIA().hashCode();
        }
        if (getCOMBUSTIBLE() != null) {
            _hashCode += getCOMBUSTIBLE().hashCode();
        }
        if (getTRANSMISION() != null) {
            _hashCode += getTRANSMISION().hashCode();
        }
        if (getTRACCION() != null) {
            _hashCode += getTRACCION().hashCode();
        }
        if (getTIPO_DE_DIRECCION() != null) {
            _hashCode += getTIPO_DE_DIRECCION().hashCode();
        }
        if (getINTERIORES() != null) {
            _hashCode += getINTERIORES().hashCode();
        }
        if (getBOLSA_DE_AIRE() != null) {
            _hashCode += getBOLSA_DE_AIRE().hashCode();
        }
        if (getAIRE_ACONDICIONADO() != null) {
            _hashCode += getAIRE_ACONDICIONADO().hashCode();
        }
        if (getELEVADORES_ELECTRICOS() != null) {
            _hashCode += getELEVADORES_ELECTRICOS().hashCode();
        }
        if (getQUEMACOCOS() != null) {
            _hashCode += getQUEMACOCOS().hashCode();
        }
        if (getESTEREO() != null) {
            _hashCode += getESTEREO().hashCode();
        }
        if (getTIPO_DE_RIN() != null) {
            _hashCode += getTIPO_DE_RIN().hashCode();
        }
        if (getRIN() != null) {
            _hashCode += getRIN().hashCode();
        }
        if (getNEUMATICO() != null) {
            _hashCode += getNEUMATICO().hashCode();
        }
        if (getFRENOS_DEL() != null) {
            _hashCode += getFRENOS_DEL().hashCode();
        }
        if (getSUSPENSION_DELANTERA() != null) {
            _hashCode += getSUSPENSION_DELANTERA().hashCode();
        }
        if (getSUSPENSION_TRASERA() != null) {
            _hashCode += getSUSPENSION_TRASERA().hashCode();
        }
        if (getTIPO_DE_CABINA() != null) {
            _hashCode += getTIPO_DE_CABINA().hashCode();
        }
        if (getTECHO() != null) {
            _hashCode += getTECHO().hashCode();
        }
        if (getPASAJEROS() != null) {
            _hashCode += getPASAJEROS().hashCode();
        }
        if (getVALVULAS() != null) {
            _hashCode += getVALVULAS().hashCode();
        }
        if (getPRECIO_VENTA() != null) {
            _hashCode += getPRECIO_VENTA().hashCode();
        }
        if (getVIN() != null) {
            _hashCode += getVIN().hashCode();
        }
        if (getRESULTADO() != null) {
            _hashCode += getRESULTADO().hashCode();
        }
        if (getCVE_AMIS() != null) {
            _hashCode += getCVE_AMIS().hashCode();
        }
        if (getCVE_SESA() != null) {
            _hashCode += getCVE_SESA().hashCode();
        }
        if (getCVE_AFIRME() != null) {
            _hashCode += getCVE_AFIRME().hashCode();
        }
        if (getOCRA() != null) {
            _hashCode += getOCRA().hashCode();
        }
        if (getID_TRANSPORTE() != null) {
            _hashCode += getID_TRANSPORTE().hashCode();
        }
        if (getPAIS_ORIGEN() != null) {
            _hashCode += getPAIS_ORIGEN().hashCode();
        }
        if (getPLANTA_ENSAMBLE() != null) {
            _hashCode += getPLANTA_ENSAMBLE().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }
}
