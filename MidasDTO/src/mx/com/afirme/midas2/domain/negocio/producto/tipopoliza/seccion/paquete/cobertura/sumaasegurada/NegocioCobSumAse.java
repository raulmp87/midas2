package mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.sumaasegurada;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;


@Entity
@Table(name = "TONEGCOBPAQSECSUM", schema = "MIDAS")
public class NegocioCobSumAse extends MidasAbstracto implements Entidad{

	private static final long serialVersionUID = 8403477931184569422L;

	@Id
	@SequenceGenerator(name = "TONEGCOBPAQSECSUM_ID_GENERATOR", sequenceName = "IDTONEGCOBPAQSECSUM_SEQ", allocationSize=1, schema="MIDAS")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TONEGCOBPAQSECSUM_ID_GENERATOR")
	@Column(name = "IDSUMAASEGURADA")
	private Long idSumaAsegurada;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTONEGCOBPAQSECCION", nullable = false)	
	private NegocioCobPaqSeccion negocioCobPaqSeccion = new NegocioCobPaqSeccion();
	
	@Column(name = "VALORSUMAASEGURADA")
	private Double valorSumaAsegurada;
	
	@Column(name = "DEFAULTVALOR")
	private short defaultValor;

	@Column(name = "NUMEROSECUENCIA")
	private Long numeroSecuencia;
	
	public Long getIdSumaAsegurada() {
		return idSumaAsegurada;
	}

	public void setIdSumaAsegurada(Long idSumaAsegurada) {
		this.idSumaAsegurada = idSumaAsegurada;
	}

	public NegocioCobPaqSeccion getNegocioCobPaqSeccion() {
		return negocioCobPaqSeccion;
	}

	public void setNegocioCobPaqSeccion(
			NegocioCobPaqSeccion negocioCobPaqSeccion) {
		this.negocioCobPaqSeccion = negocioCobPaqSeccion;
	}

	public Double getValorSumaAsegurada() {
		return valorSumaAsegurada;
	}

	public void setValorSumaAsegurada(Double valorSumaAsegurada) {
		this.valorSumaAsegurada = valorSumaAsegurada;
	}

	public short getDefaultValor() {
		return defaultValor;
	}

	public void setDefaultValor(short defaultValor) {
		this.defaultValor = defaultValor;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.idSumaAsegurada;
	}

	@Override
	public String getValue() {
		return this.valorSumaAsegurada.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Double getBusinessKey() {
		return this.valorSumaAsegurada;
	}

	public Long getNumeroSecuencia() {
		return numeroSecuencia;
	}

	public void setNumeroSecuencia(Long numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}

	
}
