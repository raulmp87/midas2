package mx.com.afirme.midas2.domain.negocio.recuotificacion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

@Entity
@Table(name="TONEGRECAUTPROGPAGO", schema="MIDAS")
public class NegocioRecuotificacionAutProgPago extends MidasAbstracto implements Entidad{

	private static final long serialVersionUID = 2281873344063847540L;
	
	public static final Integer ANUAL = 365;
	
	public static final Integer SEMESTRAL = 183;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TONEGRECAUTPROGPAGO_ID_GENERATOR")
	@SequenceGenerator(name="TONEGRECAUTPROGPAGO_ID_GENERATOR", schema="MIDAS", sequenceName="TONEGRECAUTPROGPAGO_ID_SEQ",allocationSize=1)		
	@Column(name="ID")
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch(JoinFetchType.INNER)
	@JoinColumn(name="TONEGRECAUT_ID", nullable=false)
	private NegocioRecuotificacionAutomatica automatica;
	
	@Column(name="NUMPROGPAGO")
	private Integer numProgPago;
	
	@Column(name="PCTDISTPRIMANETA")
	private Double pctDistribucionPrima;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "progPago")	
	private List<NegocioRecuotificacionAutRecibo> recibos = new ArrayList<NegocioRecuotificacionAutRecibo>();
	
	@Transient
	private String usuarioCreacion;
	
	@Transient
	private Double pcteDerechosTotales;
	
	
	public NegocioRecuotificacionAutProgPago(){
		super();
	}
	
	public NegocioRecuotificacionAutProgPago(String codigoUsuarioCreacion){
		this(codigoUsuarioCreacion, new Date());
	}
	
	public NegocioRecuotificacionAutProgPago(String codigoUsuarioCreacion, Date fechaCreacion) {
		this();
		/*NegocioRecuotificacionAutRecibo recibo = new NegocioRecuotificacionAutRecibo();
		recibo.setNumExhibicion(1);
		recibo.setPctDistPrimaNeta(100d);
		recibo.setPctDistDerechos(0d);
		recibo.setProgPago(this);
		recibo.setDiasDuracion(this.getAutomatica() == null ? ANUAL : (this.getAutomatica().getTipoVigencia().equals(TipoVigencia.ANUAL)?ANUAL:SEMESTRAL));
		recibo.setCodigoUsuarioCreacion(codigoUsuarioCreacion);
		recibo.setFechaCreacion(fechaCreacion);
		this.agregarRecibo(recibo);*/
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public NegocioRecuotificacionAutomatica getAutomatica() {
		return automatica;
	}

	public void setAutomatica(NegocioRecuotificacionAutomatica automatica) {
		this.automatica = automatica;
	}

	public Integer getNumProgPago() {
		return numProgPago;
	}

	public void setNumProgPago(Integer numProgPago) {
		this.numProgPago = numProgPago;
	}

	public Double getPctDistribucionPrima() {
		return pctDistribucionPrima;
	}

	public void setPctDistribucionPrima(Double pctDistribucionPrima) {
		this.pctDistribucionPrima = pctDistribucionPrima;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return this.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	public List<NegocioRecuotificacionAutRecibo> getRecibos() {
		return recibos;
	}

	public void setRecibos(List<NegocioRecuotificacionAutRecibo> recibos) {
		this.recibos = recibos;
	}
	
	public void agregarRecibo(NegocioRecuotificacionAutRecibo obj){			
		this.recibos.add(obj);		
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public Double getPcteDerechosTotales() {
		return pcteDerechosTotales;
	}

	public void setPcteDerechosTotales(Double pcteDerechosTotales) {
		this.pcteDerechosTotales = pcteDerechosTotales;
	}
	
	
	
}
