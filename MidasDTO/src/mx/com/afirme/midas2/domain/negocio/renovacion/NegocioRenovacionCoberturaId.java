package mx.com.afirme.midas2.domain.negocio.renovacion;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * NegocioRenovacionId entity. @author Lizandro Perez
 */
@Embeddable
public class NegocioRenovacionCoberturaId implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4306679357674448027L;

	private Long idToNegocio;
	private Short tipoRiesgo;
	private BigDecimal idToSeccion;
	private BigDecimal idToCobertura;
	
	
		
	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}
	
	@Column(name="IDTONEGOCIO", nullable = false, precision = 22, scale = 0)
	public Long getIdToNegocio() {
		return idToNegocio;
	}
	
	public void setTipoRiesgo(Short tipoRiesgo) {
		this.tipoRiesgo = tipoRiesgo;
	}
	
	@Column(name = "TIPORIESGO", nullable = false, precision = 4, scale = 0)
	public Short getTipoRiesgo() {
		return tipoRiesgo;
	}
	
	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}
	
	@Column(name="IDTOSECCION", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}
	
	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}
	
	@Column(name="IDTOCOBERTURA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToCobertura() {
		return idToCobertura;
	}

	
	
}
