package mx.com.afirme.midas2.domain.negocio.canalventa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.negocio.Negocio;



@Entity
@Table( name="TONEGCANALVENTA", schema = "MIDAS")
public class NegocioCanalVenta extends MidasAbstracto implements Entidad{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8997420717550068836L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTONEGCANALVTA_ID_GENERATOR")
	@SequenceGenerator(name="IDTONEGCANALVTA_ID_GENERATOR", schema="MIDAS", sequenceName="TONEGCANALVENTA_ID_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch(JoinFetchType.INNER)
	@JoinColumn(name="IDTONEGOCIO", nullable=false)
	private Negocio negocio;
	

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch(JoinFetchType.INNER)
	@JoinColumn(name="IDTCVALORFIJO", nullable=false)
	private CatValorFijo catalogo;
	
	
	@Column(name="STATUS", nullable=false)
	private Boolean status = Boolean.FALSE;
	
	@Column(name="REQUERIDO", nullable=false)
	private Boolean requerido = Boolean.FALSE;
	
	
	@Transient
	private String descripcionHijo = "";
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	public CatValorFijo getCatalogo() {
		return catalogo;
	}

	public void setCatalogo(CatValorFijo catalogo) {
		this.catalogo = catalogo;
	}
	

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	
	

	public Boolean getRequerido() {
		return requerido;
	}

	public void setRequerido(Boolean requerido) {
		this.requerido = requerido;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		return this.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.getId();
	}

	public void setDescripcionHijo(String descripcionHijo) {
		this.descripcionHijo = descripcionHijo;
	}

	public String getDescripcionHijo() {
		return descripcionHijo;
	}

}
