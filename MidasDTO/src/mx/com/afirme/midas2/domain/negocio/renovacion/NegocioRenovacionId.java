package mx.com.afirme.midas2.domain.negocio.renovacion;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * NegocioRenovacionId entity. @author Lizandro Perez
 */
@Embeddable
public class NegocioRenovacionId implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1553589392288074589L;
	
	private Long idToNegocio;
	private Short tipoRiesgo;
	
	public NegocioRenovacionId(){
		
	}
	
	public NegocioRenovacionId(Long idToNegocio, Short tipoRiesgo){
		this.idToNegocio = idToNegocio;
		this.tipoRiesgo = tipoRiesgo;
	}
	
	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}
	
	@Column(name="IDTONEGOCIO", nullable = false, precision = 22, scale = 0)
	public Long getIdToNegocio() {
		return idToNegocio;
	}
	
	public void setTipoRiesgo(Short tipoRiesgo) {
		this.tipoRiesgo = tipoRiesgo;
	}
	
	@Column(name = "TIPORIESGO", nullable = false, precision = 4, scale = 0)
	public Short getTipoRiesgo() {
		return tipoRiesgo;
	}

}
