package mx.com.afirme.midas2.domain.negocio.producto;

// default package

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.producto.formapago.NegocioFormaPago;
import mx.com.afirme.midas2.domain.negocio.producto.mediopago.NegocioMedioPago;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;

/**
 * NegocioProducto entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TONEGPRODUCTO", schema = "MIDAS")
public class NegocioProducto implements java.io.Serializable, Entidad {

	// Fields
	private static final long serialVersionUID = 1L;
	private Long idToNegProducto;
	private ProductoDTO productoDTO;
	private Negocio negocio;
	private List<NegocioTipoPoliza> negocioTipoPolizaList = new ArrayList<NegocioTipoPoliza>();
	private List<NegocioFormaPago> negocioFormaPagos = new ArrayList<NegocioFormaPago>();
	private List<NegocioMedioPago> negocioMedioPagos = new ArrayList<NegocioMedioPago>();

	// Constructors

	/** default constructor */
	public NegocioProducto() {
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "SEQ_NEG_PROD_GEN", allocationSize = 1, sequenceName = "IDTONEGPRODUCTO_SEQ", schema="MIDAS")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_NEG_PROD_GEN")
	@Column(name = "idToNegProducto", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getIdToNegProducto() {
		return this.idToNegProducto;
	}

	public void setIdToNegProducto(Long idToNegProducto) {
		this.idToNegProducto = idToNegProducto;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOPRODUCTO", nullable = false)
	public ProductoDTO getProductoDTO() {
		return this.productoDTO;
	}

	public void setProductoDTO(ProductoDTO productoDTO) {
		this.productoDTO = productoDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTONEGOCIO", nullable = false)
	public Negocio getNegocio() {
		return this.negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "negocioProducto")
	public List<NegocioTipoPoliza> getNegocioTipoPolizaList() {
		return this.negocioTipoPolizaList;
	}

	public void setNegocioTipoPolizaList(List<NegocioTipoPoliza> negocioTipoPolizaList) {
		this.negocioTipoPolizaList = negocioTipoPolizaList;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "negocioProducto")
	public List<NegocioFormaPago> getNegocioFormaPagos() {
		return this.negocioFormaPagos;
	}

	public void setNegocioFormaPagos(List<NegocioFormaPago> negocioFormaPagos) {
		this.negocioFormaPagos = negocioFormaPagos;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "negocioProducto")
	public List<NegocioMedioPago> getNegocioMedioPagos() {
		return this.negocioMedioPagos;
	}

	public void setNegocioMedioPagos(List<NegocioMedioPago> negocioMedioPagos) {
		this.negocioMedioPagos = negocioMedioPagos;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.idToNegProducto;
	}

	@Override
	public String getValue() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

}