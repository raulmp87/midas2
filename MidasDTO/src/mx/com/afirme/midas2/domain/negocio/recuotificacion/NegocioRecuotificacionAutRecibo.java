package mx.com.afirme.midas2.domain.negocio.recuotificacion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

@Entity
@Table(name="TONEGRECAUTRECIBO", schema="MIDAS")
public class NegocioRecuotificacionAutRecibo extends MidasAbstracto implements Entidad {

	private static final long serialVersionUID = -6630437501819765733L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TONEGRECAUTRECIBO_ID_GENERATOR")
	@SequenceGenerator(name="TONEGRECAUTRECIBO_ID_GENERATOR", schema="MIDAS", sequenceName="TONEGRECAUTRECIBO_ID_SEQ",allocationSize=1)		
	@Column(name="ID")	
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch(JoinFetchType.INNER)
	@JoinColumn(name="TONEGRECAUTPP_ID", nullable=false)
	private NegocioRecuotificacionAutProgPago progPago;
	
	@Column(name="NUMEXHIBICION")
	private Integer numExhibicion;
	
	@Column(name="DIASDURACION")
	private Integer diasDuracion;
	
	@Column(name="PCTDISTPRIMANETA")
	private Double pctDistPrimaNeta;
	
	@Column(name="PCTDISTDERECHOS")
	private Double pctDistDerechos;
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return this.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public NegocioRecuotificacionAutProgPago getProgPago() {
		return progPago;
	}

	public void setProgPago(NegocioRecuotificacionAutProgPago progPago) {
		this.progPago = progPago;
	}

	public Integer getNumExhibicion() {
		return numExhibicion;
	}

	public void setNumExhibicion(Integer numExhibicion) {
		this.numExhibicion = numExhibicion;
	}

	public Integer getDiasDuracion() {
		return diasDuracion;
	}

	public void setDiasDuracion(Integer diasDuracion) {
		this.diasDuracion = diasDuracion;
	}

	public Double getPctDistPrimaNeta() {
		return pctDistPrimaNeta;
	}

	public void setPctDistPrimaNeta(Double pctDistPrimaNeta) {
		this.pctDistPrimaNeta = pctDistPrimaNeta;
	}

	public Double getPctDistDerechos() {
		return pctDistDerechos;
	}

	public void setPctDistDerechos(Double pctDistDerechos) {
		this.pctDistDerechos = pctDistDerechos;
	}
	
}
