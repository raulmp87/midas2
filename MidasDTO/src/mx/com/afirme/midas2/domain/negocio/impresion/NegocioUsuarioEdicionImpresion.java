package mx.com.afirme.midas2.domain.negocio.impresion;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.negocio.Negocio;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

@Entity
@Table( name="TONEGUSUARIOEDIMP", schema = "MIDAS")
public class NegocioUsuarioEdicionImpresion implements Entidad{

	private static final long serialVersionUID = -5379277891175823595L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TONEGUSUARIOEDIMP_ID_GENERATOR")
	@SequenceGenerator(name="TONEGUSUARIOEDIMP_ID_GENERATOR", schema="MIDAS", sequenceName="TONEGUSUARIOEDIMP_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch(JoinFetchType.INNER)
	@JoinColumn(name="IDTONEGOCIO", nullable=false)
	private Negocio negocio;
	
	@Column(name="CLAVE_USUARIO")
	private String claveUsuario;
	
	@Column(name="NOMBRE_USUARIO")
	private String nombreUsuario;
	
	@Column(name="PERMISO_IMPRIMIR")
	private Integer permisoImprimir;
	
	@Column(name="PERMISO_EDITAR")
	private Integer permisoEditar;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREACION")
	private Date fechaCreacion;
	
	@Column(name="CODIGO_USUARIO_CREACION")
	private String codigoUsuarioCreacion;

	public NegocioUsuarioEdicionImpresion() {
		super();
	}

	public NegocioUsuarioEdicionImpresion(String claveUsuario,
			String nombreUsuario) {
		super();
		this.claveUsuario = claveUsuario;
		this.nombreUsuario = nombreUsuario;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	public String getClaveUsuario() {
		return claveUsuario;
	}

	public void setClaveUsuario(String claveUsuario) {
		this.claveUsuario = claveUsuario;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	public Integer getPermisoImprimir() {
		return permisoImprimir;
	}

	public void setPermisoImprimir(Integer permisoImprimir) {
		this.permisoImprimir = permisoImprimir;
	}

	public Integer getPermisoEditar() {
		return permisoEditar;
	}

	public void setPermisoEditar(Integer permisoEditar) {
		this.permisoEditar = permisoEditar;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((claveUsuario == null) ? 0 : claveUsuario.hashCode());
		result = prime
				* result
				+ ((codigoUsuarioCreacion == null) ? 0 : codigoUsuarioCreacion
						.hashCode());
		result = prime * result
				+ ((fechaCreacion == null) ? 0 : fechaCreacion.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((negocio == null) ? 0 : negocio.hashCode());
		result = prime * result
				+ ((nombreUsuario == null) ? 0 : nombreUsuario.hashCode());
		result = prime * result
				+ ((permisoEditar == null) ? 0 : permisoEditar.hashCode());
		result = prime * result
				+ ((permisoImprimir == null) ? 0 : permisoImprimir.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NegocioUsuarioEdicionImpresion other = (NegocioUsuarioEdicionImpresion) obj;
		if (claveUsuario == null) {
			if (other.claveUsuario != null)
				return false;
		} else if (!claveUsuario.equals(other.claveUsuario))
			return false;
		if (codigoUsuarioCreacion == null) {
			if (other.codigoUsuarioCreacion != null)
				return false;
		} else if (!codigoUsuarioCreacion.equals(other.codigoUsuarioCreacion))
			return false;
		if (fechaCreacion == null) {
			if (other.fechaCreacion != null)
				return false;
		} else if (!fechaCreacion.equals(other.fechaCreacion))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (negocio == null) {
			if (other.negocio != null)
				return false;
		} else if (!negocio.equals(other.negocio))
			return false;
		if (nombreUsuario == null) {
			if (other.nombreUsuario != null)
				return false;
		} else if (!nombreUsuario.equals(other.nombreUsuario))
			return false;
		if (permisoEditar == null) {
			if (other.permisoEditar != null)
				return false;
		} else if (!permisoEditar.equals(other.permisoEditar))
			return false;
		if (permisoImprimir == null) {
			if (other.permisoImprimir != null)
				return false;
		} else if (!permisoImprimir.equals(other.permisoImprimir))
			return false;
		return true;
	}
	
}
