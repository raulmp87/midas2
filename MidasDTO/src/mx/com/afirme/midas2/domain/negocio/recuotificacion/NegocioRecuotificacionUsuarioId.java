package mx.com.afirme.midas2.domain.negocio.recuotificacion;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class NegocioRecuotificacionUsuarioId implements Serializable{
		
	private static final long serialVersionUID = -8544851923855606769L;

	@Column(name = "TONEGRECUOTIFICA_ID", nullable = false)
	private Long recuotificacionId;

	@Column(name = "USER_ID", nullable = false)
	private Long usuarioId;

	public NegocioRecuotificacionUsuarioId(){
		super();
	}
	
	public NegocioRecuotificacionUsuarioId(Long recuotificacionId,
			Long usuarioId) {
		super();
		this.recuotificacionId = recuotificacionId;
		this.usuarioId = usuarioId;
	}

	public Long getRecuotificacionId() {
		return recuotificacionId;
	}

	public void setRecuotificacionId(Long recuotificacionId) {
		this.recuotificacionId = recuotificacionId;
	}

	public Long getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((recuotificacionId == null) ? 0 : recuotificacionId
						.hashCode());
		result = prime * result
				+ ((usuarioId == null) ? 0 : usuarioId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NegocioRecuotificacionUsuarioId other = (NegocioRecuotificacionUsuarioId) obj;
		if (recuotificacionId == null) {
			if (other.recuotificacionId != null)
				return false;
		} else if (!recuotificacionId.equals(other.recuotificacionId))
			return false;
		if (usuarioId == null) {
			if (other.usuarioId != null)
				return false;
		} else if (!usuarioId.equals(other.usuarioId))
			return false;
		return true;
	}

}
