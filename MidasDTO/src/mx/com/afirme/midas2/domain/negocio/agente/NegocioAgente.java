package mx.com.afirme.midas2.domain.negocio.agente;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.Negocio;

/**
 * NegocioAgente entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TONEGAGENTE", schema = "MIDAS")
public class NegocioAgente implements java.io.Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields

	private BigDecimal idToNegAgente;
	private Negocio negocio = new Negocio();
	private Agente agente = new Agente();
	private Integer idAgente;

	// Constructors

	/** default constructor */
	public NegocioAgente() {
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTONEGAGENTE_SEQ")
	@SequenceGenerator(name="IDTONEGAGENTE_SEQ", sequenceName="IDTONEGAGENTE_SEQ", allocationSize=1,schema="MIDAS")
	@Column(name = "idToNegAgente", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToNegAgente() {
		return this.idToNegAgente;
	}

	public void setIdToNegAgente(BigDecimal idToNegAgente) {
		this.idToNegAgente = idToNegAgente;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTONEGOCIO", nullable = false)
	public Negocio getNegocio() {
		return this.negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	
	@Transient
	public Agente getAgente() {
		return this.agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.idToNegAgente;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Column(name = "idAgente")
	public Integer getIdTcAgente() {
		return idAgente;
	}

	public void setIdTcAgente(Integer idTcAgente) {
		this.idAgente = idTcAgente;
	}

	
}