package mx.com.afirme.midas2.domain.negocio.producto.formapago;

// default package

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;

/**
 * NegocioFormaPago entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TONEGFORMAPAGO", schema = "MIDAS")
public class NegocioFormaPago implements java.io.Serializable, Entidad {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToNegFormaPago;
	private FormaPagoDTO formaPagoDTO;
	private NegocioProducto negocioProducto;

	// Constructors

	/** default constructor */
	public NegocioFormaPago() {
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTONEGFORMAPAGO_SEQ")
	@SequenceGenerator(name="IDTONEGFORMAPAGO_SEQ", sequenceName="IDTONEGFORMAPAGO_SEQ", allocationSize=1,schema="MIDAS")
	@Column(name = "idToNegFormaPago", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToNegFormaPago() {
		return this.idToNegFormaPago;
	}

	public void setIdToNegFormaPago(BigDecimal idToNegFormaPago) {
		this.idToNegFormaPago = idToNegFormaPago;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCFORMAPAGO", nullable = false)
	public FormaPagoDTO getFormaPagoDTO() {
		return this.formaPagoDTO;
	}

	public void setFormaPagoDTO(FormaPagoDTO formaPagoDTO) {
		this.formaPagoDTO = formaPagoDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idToNegProducto", nullable = false)
	public NegocioProducto getNegocioProducto() {
		return this.negocioProducto;
	}

	public void setNegocioProducto(NegocioProducto negocioProducto) {
		this.negocioProducto = negocioProducto;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.idToNegFormaPago;
	}

	@Override
	public String getValue() {
		return this.formaPagoDTO.getDescripcion();
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}