package mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tipouso;

// default package

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;

/**
 * NegocioTipoUso entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TONEGTIPOUSO", schema = "MIDAS")
public class NegocioTipoUso implements java.io.Serializable,Entidad {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long idToNegTipoUso;
	private TipoUsoVehiculoDTO tipoUsoVehiculoDTO;
	private NegocioSeccion negocioSeccion;
	private Boolean claveDefault;

	// Constructors

	/** default constructor */
	public NegocioTipoUso() {
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTONEGTIPOUSO_SEQ")
	@SequenceGenerator(name="IDTONEGTIPOUSO_SEQ", sequenceName="IDTONEGTIPOUSO_SEQ", allocationSize=1,schema="MIDAS")
	@Column(name = "idToNegTipoUso", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getIdToNegTipoUso() {
		return this.idToNegTipoUso;
	}

	public void setIdToNegTipoUso(Long idToNegTipoUso) {
		this.idToNegTipoUso = idToNegTipoUso;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCTIPOUSOVEHICULO", nullable = false)
	public TipoUsoVehiculoDTO getTipoUsoVehiculoDTO() {
		return this.tipoUsoVehiculoDTO;
	}

	public void setTipoUsoVehiculoDTO(TipoUsoVehiculoDTO tipoUsoVehiculoDTO) {
		this.tipoUsoVehiculoDTO = tipoUsoVehiculoDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTONEGSECCION", nullable = false)
	public NegocioSeccion getNegocioSeccion() {
		return this.negocioSeccion;
	}

	public void setNegocioSeccion(NegocioSeccion negocioSeccion) {
		this.negocioSeccion = negocioSeccion;
	}

	@Column(name = "claveDefault", nullable = false, precision = 4, scale = 0)
	public Boolean getClaveDefault() {
		return this.claveDefault;
	}

	public void setClaveDefault(Boolean claveDefault) {
		this.claveDefault = claveDefault;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.idToNegTipoUso;
	}

	@Override
	public String getValue() {
		return this.tipoUsoVehiculoDTO.getDescripcionTipoUsoVehiculo();
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}