package mx.com.afirme.midas2.domain.negocio.tipovigencia;

import java.io.Serializable;

import javax.persistence.*;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.negocio.Negocio;


/**
 * The persistent class for the TRNEGTIPOVIGENCIA database table.
 * 
 */
@Entity
@Table(name="TRNEGTIPOVIGENCIA", schema="MIDAS")
public class NegocioTipoVigencia implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TRNEGTIPOVIGENCIA_ID_GENERATOR", sequenceName="SEQ_TRNEGTIPOVIGENCIA_ID", allocationSize=1, schema="MIDAS")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TRNEGTIPOVIGENCIA_ID_GENERATOR")
	@Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
	private Long id;

	@Column(name = "ESDEFAULT", nullable = true, precision = 1, scale = 0)
	private Integer esDefault;

	//bi-directional many-to-one association to TipoVigencia
	@ManyToOne( fetch = FetchType.EAGER)
	@JoinColumn(name="TCTIPOVIGENCIA_ID", nullable=false)
	private TipoVigencia tipoVigencia;

	//bi-directional many-to-one association to Tonegocio
	@ManyToOne
	@JoinColumn(name="TONEGOCIO_ID", nullable=false)
	private Negocio negocio;

	/** default constructor */
	public NegocioTipoVigencia() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getEsDefault() {
		return this.esDefault;
	}

	public void setEsDefault(Integer esDefault) {
		this.esDefault = esDefault;
	}

	public TipoVigencia getTipoVigencia() {
		return this.tipoVigencia;
	}

	public void setTipoVigencia(TipoVigencia tipoVigencia) {
		this.tipoVigencia = tipoVigencia;
	}

	public Negocio getNegocio() {
		return this.negocio;
	}

	public void setNegocio(Negocio Negocio) {
		this.negocio = Negocio;
	}

	/* (non-Javadoc)
	 * @see mx.com.afirme.midas2.dao.catalogos.Entidad#getKey()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	/* (non-Javadoc)
	 * @see mx.com.afirme.midas2.dao.catalogos.Entidad#getValue()
	 */
	@Override
	public String getValue() {
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.afirme.midas2.dao.catalogos.Entidad#getBusinessKey()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.id;
	}

}