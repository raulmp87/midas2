package mx.com.afirme.midas2.domain.negocio.recuotificacion;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;

@Entity
@Table( name="TONEGRECUSER", schema = "MIDAS")
public class NegocioRecuotificacionUsuario extends MidasAbstracto implements Entidad {
	
	private static final long serialVersionUID = -6572034735332291821L;
	
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "recuotificacionId", column = @Column(name = "TONEGRECUOTIFICA_ID", nullable = false)),			
			@AttributeOverride(name = "usuarioId", column = @Column(name = "USER_ID", nullable = false)) })
	private NegocioRecuotificacionUsuarioId id = new NegocioRecuotificacionUsuarioId();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TONEGRECUOTIFICA_ID", nullable = false, insertable = false, updatable = false)
	private NegocioRecuotificacion recuotificacion;
	
	@Transient
	private String cveUsuario;
	
	@Transient
	private String nombreUsuario;
	
	@Transient
	private Long usuarioId;
	
	@Transient
	private Long recuotificacionId;

	public NegocioRecuotificacionUsuarioId getId() {
		return id;
	}

	public void setId(NegocioRecuotificacionUsuarioId id) {
		this.id = id;
	}

	public NegocioRecuotificacion getRecuotificacion() {
		return recuotificacion;
	}

	public void setRecuotificacion(NegocioRecuotificacion recuotificacion) {
		this.recuotificacion = recuotificacion;
	}
	
	public void setRecuotificacionId(Long id){
		this.id.setRecuotificacionId(id);
		this.recuotificacionId = id;
	}
	
	public void setUsuarioId(Long id){
		this.usuarioId = id;
		this.id.setUsuarioId(id);
	}
	
	public Long getUsuarioId() {
		return usuarioId;
	}

	@SuppressWarnings("unchecked")
	@Override
	public NegocioRecuotificacionUsuarioId getKey() {		
		return id;
	}

	@Override
	public String getValue() {
		return this.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public NegocioRecuotificacionUsuarioId getBusinessKey() {
		return id;
	}

	public String getCveUsuario() {
		return cveUsuario;
	}

	public void setCveUsuario(String cveUsuario) {
		this.cveUsuario = cveUsuario;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public Long getRecuotificacionId() {
		return recuotificacionId;
	}

	
	
}
