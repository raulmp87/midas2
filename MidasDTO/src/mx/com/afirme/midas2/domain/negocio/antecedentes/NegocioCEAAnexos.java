package mx.com.afirme.midas2.domain.negocio.antecedentes;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="tocenaanexos", schema="MIDAS")
public class NegocioCEAAnexos implements Serializable, Entidad {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTOCENAANEXOS_ID_GENERATOR")	
	@SequenceGenerator(name="IDTOCENAANEXOS_ID_GENERATOR", sequenceName="MIDAS.IDTOCENAANEXOS_SEQ", allocationSize=1)	
	@Column(name="ID")
	private Long id;
	
	@Column(name="NOMBRE", nullable=false)
	private String nombre;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_REGISTRO", nullable=false)
	private Date fechaRegistro;

	@Column(name="USUARIO_NOMBRE", nullable=false)
	private String usuarioNombre;

	@Column(name="USUARIO_ID", nullable=false)
	private String usuarioId;

	@Column(name="TEXTO")
	private String texto;

	@Column(name="FMX_ID", nullable=false)
	private String fortimaxDocId;

	@Column(name="FMX_NOMBRE", nullable=false)
	private String fortimaxDocNombre;

	@Column(name="FMX_GAVETA", nullable=false)
	private String fortimaxDocGaveta;

	@Column(name="FMX_CARPETA")
	private String fortimaxDocCarpeta;
	
	@Column(name="IDTONEGOCIO", nullable=false)
	private Long idToNegocio;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getUsuarioNombre() {
		return usuarioNombre;
	}

	public void setUsuarioNombre(String usuarioNombre) {
		this.usuarioNombre = usuarioNombre;
	}

	public String getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(String usuarioId) {
		this.usuarioId = usuarioId;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getFortimaxDocId() {
		return fortimaxDocId;
	}

	public void setFortimaxDocId(String fortimaxDocId) {
		this.fortimaxDocId = fortimaxDocId;
	}

	public String getFortimaxDocNombre() {
		return fortimaxDocNombre;
	}

	public void setFortimaxDocNombre(String fortimaxDocNombre) {
		this.fortimaxDocNombre = fortimaxDocNombre;
	}

	public String getFortimaxDocGaveta() {
		return fortimaxDocGaveta;
	}

	public void setFortimaxDocGaveta(String fortimaxDocGaveta) {
		this.fortimaxDocGaveta = fortimaxDocGaveta;
	}

	public String getFortimaxDocCarpeta() {
		return fortimaxDocCarpeta;
	}

	public void setFortimaxDocCarpeta(String fortimaxDocCarpeta) {
		this.fortimaxDocCarpeta = fortimaxDocCarpeta;
	}
	
	public Long getIdToNegocio() {
		return idToNegocio;
	}

	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}

	@Override
	public <K> K getKey() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public String getValue() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

}
