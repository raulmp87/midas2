package mx.com.afirme.midas2.domain.negocio.zonacirculacion;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.negocio.Negocio;


/**
 * The persistent class for the toNegEstado database table.
 * 
 */
@Entity
@Table(name="toNegEstado" , schema="MIDAS")
public class NegocioEstado implements Serializable, Entidad{
	private static final long serialVersionUID = 1L;

	private Long id;
	private List<NegocioMunicipio> negocioMunicipioList ;
	private Negocio negocio;
    private EstadoDTO estadoDTO;

	
	public NegocioEstado() {
    
	}
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TONEGESTADO_ID_GENERATOR")
	@SequenceGenerator(name="TONEGESTADO_ID_GENERATOR", sequenceName="MIDAS.idtonegestado_seq", allocationSize=1)
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	
	public Long getId() {
		return this.id;
	}	
	


	public void setId(Long id) {
		this.id = id;
	}

	
	@OneToMany(cascade=CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy="negocioEstado" )
	public List<NegocioMunicipio> getNegocioMunicipioList() {
		return negocioMunicipioList;
	}

	public void setNegocioMunicipioList(List<NegocioMunicipio> negocioMunicipioList) {
		this.negocioMunicipioList = negocioMunicipioList;
	} 

	@ManyToOne(fetch = FetchType.LAZY )
  	@JoinColumn(name = "IDTONEGOCIO", referencedColumnName="IDTONEGOCIO" )
	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}


	@ManyToOne(fetch = FetchType.LAZY )
  	@JoinColumn(name = "IDESTADO", referencedColumnName="STATE_ID" )
	public EstadoDTO getEstadoDTO() {
		return estadoDTO;
	}
	public void setEstadoDTO(EstadoDTO estadoDTO) {
		this.estadoDTO = estadoDTO;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		// TODO Apendice de motodo generado automaticamente
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Apendice de motodo generado automaticamente
		return null;
	}

}
