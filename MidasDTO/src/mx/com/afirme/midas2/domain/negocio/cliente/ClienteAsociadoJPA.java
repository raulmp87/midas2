package mx.com.afirme.midas2.domain.negocio.cliente;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "ClienteAsociadoJPA")
@Table(name = "VNCLIENTEASOCIADO", schema = "MIDAS")
public class ClienteAsociadoJPA implements Serializable {
	private static final long serialVersionUID = 1L;
	private String nombre;
	private String codigoRFC;
	private Long idToNegocio;
	private Long idCliente;

	@Id
	@Column(name = "IDCLIENTE", nullable = false, unique = true)
	public Long getIdCliente() {
		return idCliente;
	}

	@Column(name = "IDTONEGOCIO", nullable = false)
	public Long getIdToNegocio() {
		return idToNegocio;
	}

	@Column(name = "NOMBRE", nullable = false)
	public String getNombre() {
		return nombre;
	}

	@Column(name = "CODIGORFC")
	public String getCodigoRFC() {
		return codigoRFC;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setCodigoRFC(String codigoRFC) {
		this.codigoRFC = codigoRFC;
	}

	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

}
