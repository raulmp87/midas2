package mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducibles;

// default package

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;

/**
 * NegocioDeducibleCobPP entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TONEGDEDUCIBLECOB", schema="MIDAS")
public class NegocioDeducibleCob implements java.io.Serializable, Entidad {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 2548923174633896557L;
	private Long id;
	private NegocioCobPaqSeccion negocioCobPaqSeccion;
	private Integer numeroSecuencia;
	private Double valorDeducible;
	private Short claveDefault;

	// Constructors

	/** default constructor */
	public NegocioDeducibleCob() {
	}

	/** full constructor */
	public NegocioDeducibleCob(Long id,
			NegocioCobPaqSeccion negocioCobPaqSeccion, Integer numeroSecuencia,
			Double valorDeducible, Short claveDefault) {
		this.id = id;
		this.negocioCobPaqSeccion = negocioCobPaqSeccion;
		this.numeroSecuencia = numeroSecuencia;
		this.valorDeducible = valorDeducible;
		this.claveDefault = claveDefault;
	}

	// Property accessors

	@Id
	@SequenceGenerator(name = "TONEGDEDUCIBLECOB_ID_GENERATOR", sequenceName = "IDTONEGDEDUCIBLECOB_SEQ", allocationSize=1, schema="MIDAS")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TONEGDEDUCIBLECOB_ID_GENERATOR")
	@Column(name = "ID")
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTONEGCOBPAQSECCION", nullable = false)
	public NegocioCobPaqSeccion getNegocioCobPaqSeccion() {
		return this.negocioCobPaqSeccion;
	}

	public void setNegocioCobPaqSeccion(
			NegocioCobPaqSeccion negocioCobPaqSeccion) {
		this.negocioCobPaqSeccion = negocioCobPaqSeccion;
	}

	@Column(name = "numeroSecuencia")
	public Integer getNumeroSecuencia() {
		return this.numeroSecuencia;
	}

	public void setNumeroSecuencia(Integer numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}

	@Column(name = "valorDeducible", nullable = false, precision = 16, scale = 4)
	public Double getValorDeducible() {
		return this.valorDeducible;
	}

	public void setValorDeducible(Double valorDeducible) {
		this.valorDeducible = valorDeducible;
	}

	@Column(name = "claveDefault", nullable = false, precision = 4, scale = 0)
	public Short getClaveDefault() {
		return this.claveDefault;
	}

	public void setClaveDefault(Short claveDefault) {
		this.claveDefault = claveDefault;
	}

	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		return this.valorDeducible.toString();
	}

	@Override
	public Double getBusinessKey() {
		return this.valorDeducible;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NegocioDeducibleCob other = (NegocioDeducibleCob) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}