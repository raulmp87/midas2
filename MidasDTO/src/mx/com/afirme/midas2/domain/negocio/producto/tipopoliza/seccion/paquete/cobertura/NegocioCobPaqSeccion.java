package mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducibles.NegocioDeducibleCob;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.sumaasegurada.NegocioCobSumAse;

/**
 * NegocioCobPaqSeccion entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TONEGCOBPAQSECCION", schema = "MIDAS"/*, uniqueConstraints = @UniqueConstraint(columnNames = {
		"IDTONEGPAQUETESECCION", "IDTOCOBERTURA", "claveZonaCirculacion" })*/)
public class NegocioCobPaqSeccion implements java.io.Serializable, Entidad {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToNegCobPaqSeccion;
	private NegocioPaqueteSeccion negocioPaqueteSeccion;
	private MonedaDTO monedaDTO;
	private EstadoDTO estadoDTO;
	private CiudadDTO ciudadDTO;
	private CoberturaDTO coberturaDTO;
//	private String claveZonaCirculacion;
	private Short claveTipoDeduciblePT;
	private Short claveTipoDeduciblePP;
	private Short claveTipoSumaAsegurada;
	private Double valorSumaAseguradaMin;
	private Double valorSumaAseguradaMax;
	private Double valorSumaAseguradaDefault;
	private TipoUsoVehiculoDTO tipoUsoVehiculo;
	private Agente agente;
	private Boolean renovacion;
	private float descuento;

	private List<NegocioDeducibleCob> negocioDeducibleCobList = new ArrayList<NegocioDeducibleCob>(1);
	private List<NegocioCobSumAse> negocioCobSumAseList = new ArrayList<NegocioCobSumAse>(1);

	// Constructors

	/** default constructor */
	public NegocioCobPaqSeccion() {
	}

	// Property accessors
	@Id
	@SequenceGenerator(name="NEG_COB_PAQ_SEC_GEN", sequenceName="IDTONEGCOBPAQSECCION_SEQ", allocationSize=1, schema="MIDAS")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="NEG_COB_PAQ_SEC_GEN")
	@Column(name = "idToNegCobPaqSeccion", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToNegCobPaqSeccion() {
		return this.idToNegCobPaqSeccion;
	}

	public void setIdToNegCobPaqSeccion(BigDecimal idToNegCobPaqSeccion) {
		this.idToNegCobPaqSeccion = idToNegCobPaqSeccion;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTONEGPAQUETESECCION", nullable = false)
	public NegocioPaqueteSeccion getNegocioPaqueteSeccion() {
		return this.negocioPaqueteSeccion;
	}

	public void setNegocioPaqueteSeccion(
			NegocioPaqueteSeccion negocioPaqueteSeccion) {
		this.negocioPaqueteSeccion = negocioPaqueteSeccion;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idMoneda", referencedColumnName="IDTCMONEDA", nullable = false)
	public MonedaDTO getMonedaDTO() {
		return monedaDTO;
	}

	public void setMonedaDTO(MonedaDTO monedaDTO) {
		this.monedaDTO = monedaDTO;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idTcEstado", referencedColumnName="STATE_ID", nullable = true)
	public EstadoDTO getEstadoDTO() {
		return estadoDTO;
	}

	public void setEstadoDTO(EstadoDTO estadoDTO) {
		this.estadoDTO = estadoDTO;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idTcCiudad", referencedColumnName="CITY_ID", nullable = true)
	public CiudadDTO getCiudadDTO() {
		return ciudadDTO;
	}

	public void setCiudadDTO(CiudadDTO ciudadDTO) {
		this.ciudadDTO = ciudadDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOCOBERTURA", nullable = false)
	public CoberturaDTO getCoberturaDTO() {
		return this.coberturaDTO;
	}

	public void setCoberturaDTO(CoberturaDTO coberturaDTO) {
		this.coberturaDTO = coberturaDTO;
	}

//	@Column(name = "claveZonaCirculacion", length = 2)
//	public String getClaveZonaCirculacion() {
//		return this.claveZonaCirculacion;
//	}
//
//	public void setClaveZonaCirculacion(String claveZonaCirculacion) {
//		this.claveZonaCirculacion = claveZonaCirculacion;
//	}

	@Column(name = "claveTipoDeduciblePT", nullable = false, precision = 4, scale = 0)
	public Short getClaveTipoDeduciblePT() {
		return this.claveTipoDeduciblePT;
	}

	public void setClaveTipoDeduciblePT(Short claveTipoDeduciblePT) {
		this.claveTipoDeduciblePT = claveTipoDeduciblePT;
	}

	@Column(name = "claveTipoDeduciblePP", nullable = false, precision = 4, scale = 0)
	public Short getClaveTipoDeduciblePP() {
		return this.claveTipoDeduciblePP;
	}

	public void setClaveTipoDeduciblePP(Short claveTipoDeduciblePP) {
		this.claveTipoDeduciblePP = claveTipoDeduciblePP;
	}

	@Column(name = "claveTipoSumaAsegurada", nullable = false, precision = 4, scale = 0)
	public Short getClaveTipoSumaAsegurada() {
		return this.claveTipoSumaAsegurada;
	}

	public void setClaveTipoSumaAsegurada(Short claveTipoSumaAsegurada) {
		this.claveTipoSumaAsegurada = claveTipoSumaAsegurada;
	}

	@Column(name = "valorSumaAseguradaMin", nullable = false, precision = 16)
	public Double getValorSumaAseguradaMin() {
		return this.valorSumaAseguradaMin;
	}

	public void setValorSumaAseguradaMin(Double valorSumaAseguradaMin) {
		this.valorSumaAseguradaMin = valorSumaAseguradaMin;
	}

	@Column(name = "valorSumaAseguradaMax", nullable = false, precision = 16)
	public Double getValorSumaAseguradaMax() {
		return this.valorSumaAseguradaMax;
	}

	public void setValorSumaAseguradaMax(Double valorSumaAseguradaMax) {
		this.valorSumaAseguradaMax = valorSumaAseguradaMax;
	}

	@Column(name = "valorSumaAseguradaDefault", nullable = false, precision = 16)
	public Double getValorSumaAseguradaDefault() {
		return this.valorSumaAseguradaDefault;
	}

	public void setValorSumaAseguradaDefault(Double valorSumaAseguradaDefault) {
		this.valorSumaAseguradaDefault = valorSumaAseguradaDefault;
	}
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTCTIPOUSOVEHICULO") 
	public TipoUsoVehiculoDTO getTipoUsoVehiculo() {
		return tipoUsoVehiculo;
	}

	public void setTipoUsoVehiculo(TipoUsoVehiculoDTO tipoUsoVehiculo) {
		this.tipoUsoVehiculo = tipoUsoVehiculo;
	}
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDAGENTE")
	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	@Column(name = "RENOVACION", nullable = true)
	public Boolean isRenovacion() {
		return renovacion;
	}

	public void setRenovacion(Boolean renovacion) {
		this.renovacion = renovacion;
	}

	@Transient
	public String getRenovacionString() {
		return (renovacion == null)? "null" : renovacion.toString();
	}

	public void setRenovacionString(String renovacionString) {
		if(Boolean.TRUE.toString().equals(renovacionString)){
			renovacion = Boolean.TRUE;
		}else if(Boolean.FALSE.toString().equals(renovacionString)){
			renovacion = Boolean.FALSE;
		}else{
			renovacion = null;
		}
	}

	@Column(name = "DESCUENTO", nullable = true, precision = 16)
	public float getDescuento() {
		return descuento;
	}

	public void setDescuento(float descuento) {
		this.descuento = descuento;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "negocioCobPaqSeccion")
	public List<NegocioDeducibleCob> getNegocioDeducibleCobList() {
		return this.negocioDeducibleCobList;
	}

	public void setNegocioDeducibleCobList(
			List<NegocioDeducibleCob> negocioDeducibleCobList) {
		this.negocioDeducibleCobList = negocioDeducibleCobList;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "negocioCobPaqSeccion")
	public List<NegocioCobSumAse> getNegocioCobSumAseList() {
		return negocioCobSumAseList;
	}

	public void setNegocioCobSumAseList(
			List<NegocioCobSumAse> negocioCobSumAseList) {
		this.negocioCobSumAseList = negocioCobSumAseList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return this.getIdToNegCobPaqSeccion();
	}

	@Override
	public String getValue() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}
	
	public String getClaveTipoSumaAseguradaStr(){
		String valor = "";
		Short uno = 1;
		Short dos = 2;
		Short tres = 3;
		if(this.claveTipoSumaAsegurada.equals(uno)){
			valor = "B\u00C1SICA";
		}else if(this.claveTipoSumaAsegurada.equals(dos)){
			valor = "AMPARADA";
		}else if(this.claveTipoSumaAsegurada.equals(tres)){
			valor = "SUBL\u00CDMITE";
		}
		return valor;
	}

}