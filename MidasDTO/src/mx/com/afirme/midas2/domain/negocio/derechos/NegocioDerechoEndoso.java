package mx.com.afirme.midas2.domain.negocio.derechos;
// default package

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.negocio.Negocio;


/**
 * NegocioDerechoEndoso entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TONEGDERECHOENDOSO"
    ,schema="MIDAS"
)
public class NegocioDerechoEndoso  implements Serializable, Entidad {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = -8859325398952506236L;
	private Long idToNegDerechoEndoso;
    private Negocio negocio;
    private Double importeDerecho;
    private Boolean claveDefault;
    private Boolean esAltaInciso;
    private Boolean activo;


    // Constructors

    /** default constructor */
    public NegocioDerechoEndoso() {
    }

    
    /** full constructor */
    public NegocioDerechoEndoso(Long idToNegDerechoEndoso, Negocio negocio, Double importeDerecho, Boolean claveDefault) {
        this.idToNegDerechoEndoso = idToNegDerechoEndoso;
        this.negocio = negocio;
        this.importeDerecho = importeDerecho;
        this.claveDefault = claveDefault;
    }

   
    // Property accessors
    @Id 
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTONEGDERECHOENDOSO_SEQ")
    @SequenceGenerator(name="IDTONEGDERECHOENDOSO_SEQ", sequenceName="MIDAS.IDTONEGDERECHOENDOSO_SEQ", allocationSize=1)
    @Column(name="idToNegDerechoEndoso", unique=true, nullable=false, precision=22, scale=0)
    public Long getIdToNegDerechoEndoso() {
        return this.idToNegDerechoEndoso;
    }
    
    public void setIdToNegDerechoEndoso(Long idToNegDerechoEndoso) {
        this.idToNegDerechoEndoso = idToNegDerechoEndoso;
    }
	
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTONEGOCIO", nullable=false)
    public Negocio getNegocio() {
        return this.negocio;
    }
    
    public void setNegocio(Negocio negocio) {
        this.negocio = negocio;
    }
    
    @Column(name="importeDerecho", nullable=false, precision=16)
    @NotNull
    public Double getImporteDerecho() {
        return this.importeDerecho;
    }
    
    public void setImporteDerecho(Double importeDerecho) {
        this.importeDerecho = importeDerecho;
    }
    
    @Column(name="claveDefault", nullable=false, precision=4, scale=0)
    @NotNull
    public Boolean getClaveDefault() {
        return this.claveDefault;
    }
    
    public void setClaveDefault(Boolean claveDefault) {
        this.claveDefault = claveDefault;
    }
    
    @Column(name="ESALTAINCISO", nullable=false, precision=1, scale=0)
    @NotNull
	public Boolean getEsAltaInciso() {
		return esAltaInciso;
	}


	public void setEsAltaInciso(Boolean esAltaInciso) {
		this.esAltaInciso = esAltaInciso;
	}
	
	
	@Column(name="ACTIVO", nullable=false, precision=1, scale=0)
    @NotNull
	public Boolean getActivo() {
		return activo;
	}


	public void setActivo(Boolean activo) {
		this.activo = activo;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.idToNegDerechoEndoso;
	}


	@Override
	public String getValue() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return "";
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.idToNegDerechoEndoso;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((idToNegDerechoEndoso == null) ? 0 : idToNegDerechoEndoso
						.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NegocioDerechoEndoso other = (NegocioDerechoEndoso) obj;
		if (idToNegDerechoEndoso == null) {
			if (other.idToNegDerechoEndoso != null)
				return false;
		} else if (!idToNegDerechoEndoso.equals(other.idToNegDerechoEndoso))
			return false;
		return true;
	}
	
}