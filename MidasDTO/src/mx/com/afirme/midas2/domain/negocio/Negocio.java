package mx.com.afirme.midas2.domain.negocio;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.negocio.agente.NegocioAgente;
import mx.com.afirme.midas2.domain.negocio.bonocomision.NegocioBonoComision;
import mx.com.afirme.midas2.domain.negocio.cliente.NegocioCliente;
import mx.com.afirme.midas2.domain.negocio.cliente.grupo.NegocioGrupoCliente;
import mx.com.afirme.midas2.domain.negocio.condicionespecial.NegocioCondicionEspecial;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioConfiguracionDerecho;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoEndoso;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.estadodescuento.NegocioEstadoDescuento;
import mx.com.afirme.midas2.domain.negocio.impresion.NegocioUsuarioEdicionImpresion;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioEstado;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
import mx.com.afirme.midas2.validator.group.NewItemChecks;

/**
 * Negocio entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TONEGOCIO", schema = "MIDAS")
public class Negocio implements java.io.Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields
	public static enum EstatusNegocio{
		CREADO((short)0), ACTIVO((short)1), INACTIVO((short)2), BORRADO((short)3);		
		EstatusNegocio(Short estatus) {
			this.estatus = estatus;
		}
		private Short estatus;
		public Short obtenerEstatus(){
			return this.estatus;
		}
		public Short getObtenerEstatus(){
			return this.obtenerEstatus();
		}
		
	};		
	private Long idToNegocio;
	private Double pctMaximoDescuento;
	private Double descuentoMaxOriginacion;
	private Double pctPrimaCeder;
	private Date fechaInicioVigencia;
	private Date fechaFinVigencia;
	private String formulaDividendos;
	private BigDecimal diasRetroactividad;
	private BigDecimal diasDiferimiento;
	private BigDecimal diasGraciaPoliza;
	private BigDecimal diasGraciaPolizaSubsecuentes;
	private BigDecimal diasPlazoUsuario;
	private BigDecimal diasGraciaEndosoSubsecuentes;
	private Boolean claveManejaUdi;
	private Boolean clavePorcentajeMontoUdi;
	private Double valorUdi;
	private Double pctUdiPromotor;
	private Double pctUdiAgente;
	private Boolean claveUdiIncluyeIva;
	private String descripcionNegocio;
	private BigDecimal versionNegocio;
	private Date fechaCreacion;
	private Boolean claveManejaTarifaBase;
	private Short claveEstatus;
	private List<NegocioProducto> negocioProductos = new ArrayList<NegocioProducto>();
	private List<NegocioCliente> negocioClientes = new ArrayList<NegocioCliente>();
	private List<NegocioDerechoEndoso> negocioDerechoEndosos = new ArrayList<NegocioDerechoEndoso>();
	private List<NegocioAgente> negocioAgentes = new ArrayList<NegocioAgente>();
	private List<NegocioDerechoPoliza> negocioDerechoPolizas = new ArrayList<NegocioDerechoPoliza>();
	private List<NegocioGrupoCliente> grupoCliente = new ArrayList<NegocioGrupoCliente>();
	private List<NegocioCondicionEspecial> condicionesEspeciales  = new ArrayList<NegocioCondicionEspecial>();
	private List<NegocioUsuarioEdicionImpresion> usuariosEdicionImpresion  = new ArrayList<NegocioUsuarioEdicionImpresion>();
	private List<NegocioConfiguracionDerecho> configuracionesDerecho = new ArrayList<NegocioConfiguracionDerecho>();
	private String claveNegocio;
	List<NegocioEstado> negocioEstadoList = new ArrayList<NegocioEstado>(1);
	List<NegocioEstadoDescuento> negocioEstadoDescuentoList = new ArrayList<NegocioEstadoDescuento>(1);
	private Boolean claveDefault = false;
	private Boolean aplicaUsuarioExterno = false;
	private Boolean aplicaUsuarioInterno = false;
	private NegocioBonoComision negocioBonoComision; 
	private Date fechaFinVigenciaFija;
	private Boolean aplicaPctPagoFraccionado;
	private Double pctDescuentoDefault = 0.0;
	private Boolean aplicaCURPObligatorio = false;
	private Boolean aplicaValidacionExterno = true;
	private Boolean aplicaValidacionRecibo = true;
	private Boolean aplicaReciboGlobal = true;
	private Boolean habilitaAgrupacionRecibos = false;
	private Boolean aplicaValidacionNumeroSerie = true;
	private Boolean aplicaValidacionArticulo140 = true;
	private Boolean aplicaValidacionIgualacionPrimas = true;
	private Boolean aplicaPolizaGobierno = true;
	private boolean aplicaEnlace = false;
	private Boolean aplicaDiasRetroCanCfp = false;
	
	private Integer numeroDiasEspera;
	private boolean correoObligatorio;
	private Boolean aplicaSucursal;
	private Boolean aplicaEndosoConductoCobro = false;
	private Boolean aplicaValidacionPersonaMoral = false;	
	private String codigoUsuarioModificacion;
	private Short tipoPersona; // 1 - Persona Física, 2 - Persona Moral, 0 indefinido
	
	// Constructors

	//Activacion
	private Date fechaActivacion;
	private String codigoUsuarioActivacion;

	/** default constructor */
	public Negocio() {
		pctMaximoDescuento = Double.valueOf(0);
		pctPrimaCeder = Double.valueOf(0);
		fechaInicioVigencia = new Date();
		diasRetroactividad = BigDecimal.ZERO;
		diasDiferimiento = BigDecimal.ZERO;
		diasGraciaPoliza = BigDecimal.ZERO;
		diasGraciaPolizaSubsecuentes = BigDecimal.ZERO;
		diasPlazoUsuario = BigDecimal.ZERO;
		diasGraciaEndosoSubsecuentes = BigDecimal.ZERO;
		claveManejaUdi = Boolean.FALSE;
		clavePorcentajeMontoUdi = Boolean.FALSE;
		valorUdi = Double.valueOf(0);
		pctUdiPromotor = Double.valueOf(0);
		pctUdiAgente = Double.valueOf(0);
		claveUdiIncluyeIva = Boolean.FALSE;
		versionNegocio = BigDecimal.ONE;
		fechaCreacion = new Date();
		claveManejaTarifaBase = Boolean.FALSE;
		claveEstatus = 0;
		formulaDividendos=BigDecimal.ZERO.toString();
		habilitaAgrupacionRecibos = false;
		aplicaValidacionNumeroSerie = true;
		aplicaValidacionArticulo140 = true;
		aplicaValidacionIgualacionPrimas = true;
		aplicaPolizaGobierno = true;
		aplicaEnlace = false;
		this.numeroDiasEspera = 0;
		this.correoObligatorio = Boolean.TRUE;
		this.aplicaSucursal = Boolean.FALSE;
		aplicaEndosoConductoCobro = false;
		aplicaValidacionPersonaMoral = false;
	}

	public static final String CLAVE_NEGOCIO_AUTOS = "A";
	public static final long NEGOCIO_BASE_WS = 516;
	// Property accessors
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTONEGOCIO_SEQ")
	@SequenceGenerator(name="IDTONEGOCIO_SEQ", sequenceName="MIDAS.IDTONEGOCIO_SEQ", allocationSize=1)	
	@Column(name = "IDTONEGOCIO", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getIdToNegocio() {
		return this.idToNegocio;
	}

	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}
	
	@Column(name = "CLAVENEGOCIO")
	public String getClaveNegocio() {
		return claveNegocio!=null?this.claveNegocio.toUpperCase():this.claveNegocio;
	}

	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}


	@Column(name = "PCTMAXIMODESCUENTO", nullable = false, precision = 8, scale = 4)
	@Digits(integer = 8, fraction= 4,groups=EditItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Double getPctMaximoDescuento() {
		return this.pctMaximoDescuento;
	}

	public void setPctMaximoDescuento(Double pctMaximoDescuento) {
		this.pctMaximoDescuento = pctMaximoDescuento;
	}

	@Column(name = "PCTPRIMACEDER", nullable = false, precision = 8, scale = 4)
	@Digits(integer = 8, fraction= 4,groups=EditItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	
	public Double getPctPrimaCeder() {
		return this.pctPrimaCeder;
	}

	public void setPctPrimaCeder(Double pctPrimaCeder) {
		this.pctPrimaCeder = pctPrimaCeder;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAINICIOVIGENCIA", nullable = false, length = 7)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Date getFechaInicioVigencia() {
		return this.fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAFINVIGENCIA", length = 7)
	@Future(groups=NewItemChecks.class, message="{javax.validation.constraints.Future.message}")
	public Date getFechaFinVigencia() {
		return this.fechaFinVigencia;
	}

	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	@Column(name = "FORMULADIVIDENDOS", length = 500)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=1, max=500, groups=EditItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getFormulaDividendos() {
		return this.formulaDividendos!=null?this.formulaDividendos.toUpperCase():this.formulaDividendos;
	}

	public void setFormulaDividendos(String formulaDividendos) {
		this.formulaDividendos = formulaDividendos;
	}

	@Column(name = "DIASRETROACTIVIDAD", nullable = false, precision = 22, scale = 0)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public BigDecimal getDiasRetroactividad() {
		return this.diasRetroactividad;
	}

	public void setDiasRetroactividad(BigDecimal diasRetroactividad) {
		this.diasRetroactividad = diasRetroactividad;
	}

	@Column(name = "DIASDIFERIMIENTO", nullable = false, precision = 22, scale = 0)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public BigDecimal getDiasDiferimiento() {
		return this.diasDiferimiento;
	}

	public void setDiasDiferimiento(BigDecimal diasDiferimiento) {
		this.diasDiferimiento = diasDiferimiento;
	}

	@Column(name = "DIASGRACIAPOLIZA", nullable = false, precision = 22, scale = 0)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public BigDecimal getDiasGraciaPoliza() {
		return this.diasGraciaPoliza;
	}

	public void setDiasGraciaPoliza(BigDecimal diasGraciaPoliza) {
		this.diasGraciaPoliza = diasGraciaPoliza;
	}

	@Column(name = "DIASGRACIAPOLIZASUBSECUENTES", nullable = false, precision = 22, scale = 0)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public BigDecimal getDiasGraciaPolizaSubsecuentes() {
		return this.diasGraciaPolizaSubsecuentes;
	}

	public void setDiasGraciaPolizaSubsecuentes(
			BigDecimal diasGraciaPolizaSubsecuentes) {
		this.diasGraciaPolizaSubsecuentes = diasGraciaPolizaSubsecuentes;
	}

	@Column(name = "DIASPLAZOUSUARIO", nullable = false, precision = 22, scale = 0)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public BigDecimal getDiasPlazoUsuario() {
		return this.diasPlazoUsuario;
	}

	public void setDiasPlazoUsuario(BigDecimal diasPlazoUsuario) {
		this.diasPlazoUsuario = diasPlazoUsuario;
	}

	@Column(name = "DIASGRACIAENDOSOSUBSECUENTES", nullable = false, precision = 22, scale = 0)
	public BigDecimal getDiasGraciaEndosoSubsecuentes() {
		return this.diasGraciaEndosoSubsecuentes;
	}

	public void setDiasGraciaEndosoSubsecuentes(
			BigDecimal diasGraciaEndosoSubsecuentes) {
		this.diasGraciaEndosoSubsecuentes = diasGraciaEndosoSubsecuentes;
	}

	@Column(name = "CLAVEMANEJAUDI", nullable = false, precision = 4, scale = 0)
	public Boolean getClaveManejaUdi() {
		return this.claveManejaUdi;
	}

	public void setClaveManejaUdi(Boolean claveManejaUdi) {
		this.claveManejaUdi = claveManejaUdi;
	}
	
	
	//@OneToMany(fetch = FetchType.LAZY, mappedBy="negocio" )
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "negocio")
	public List<NegocioEstado> getNegocioEstadoList() {
		return negocioEstadoList;
	}

	public void setNegocioEstadoList(List<NegocioEstado> negocioEstadoList) {
		this.negocioEstadoList = negocioEstadoList;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "negocio")
	public List<NegocioEstadoDescuento> getNegocioEstadoDescuentoList() {
		return negocioEstadoDescuentoList;
	}

	public void setNegocioEstadoDescuentoList(List<NegocioEstadoDescuento> negocioEstadoDescuentoList) {
		this.negocioEstadoDescuentoList = negocioEstadoDescuentoList;
	}

	@Column(name = "CLAVEPORCENTAJEMONTOUDI", nullable = false, precision = 4, scale = 0)
	public Boolean getClavePorcentajeMontoUdi() {
		return this.clavePorcentajeMontoUdi;
	}

	public void setClavePorcentajeMontoUdi(Boolean clavePorcentajeMontoUdi) {
		this.clavePorcentajeMontoUdi = clavePorcentajeMontoUdi;
	}

	@Column(name = "VALORUDI", nullable = false, precision = 16, scale = 4)
	@Digits(integer = 16, fraction= 4,groups=EditItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Double getValorUdi() {
		return this.valorUdi;
	}

	public void setValorUdi(Double valorUdi) {
		this.valorUdi = valorUdi;
	}

	@Column(name = "PCTUDIPROMOTOR", nullable = false, precision = 8, scale = 4)
	@Digits(integer = 8, fraction= 4,groups=EditItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Double getPctUdiPromotor() {
		return this.pctUdiPromotor;
	}

	public void setPctUdiPromotor(Double pctUdiPromotor) {
		this.pctUdiPromotor = pctUdiPromotor;
	}

	@Column(name = "PCTUDIAGENTE", nullable = false, precision = 8, scale = 4)
	@Digits(integer = 8, fraction= 4,groups=EditItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Double getPctUdiAgente() {
		return this.pctUdiAgente;
	}

	public void setPctUdiAgente(Double pctUdiAgente) {
		this.pctUdiAgente = pctUdiAgente;
	}

	@Column(name = "CLAVEUDIINCLUYEIVA", nullable = false, precision = 4, scale = 0)
	public Boolean getClaveUdiIncluyeIva() {
		return this.claveUdiIncluyeIva;
	}

	public void setClaveUdiIncluyeIva(Boolean claveUdiIncluyeIva) {
		this.claveUdiIncluyeIva = claveUdiIncluyeIva;
	}
	
	@Column(name = "DESCRIPCIONNEGOCIO", nullable = true, length = 200)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=1, max=200, groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public String getDescripcionNegocio() {
		return descripcionNegocio!=null?this.descripcionNegocio.toUpperCase():this.descripcionNegocio;
	}

	public void setDescripcionNegocio(String descripcionNegocio) {
		this.descripcionNegocio = descripcionNegocio;
	}
	
	@Column(name = "VERSIONNEGOCIO", nullable = true, precision = 4, scale = 0)
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public BigDecimal getVersionNegocio() {
		return versionNegocio;
	}

	public void setVersionNegocio(BigDecimal versionNegocio) {
		this.versionNegocio = versionNegocio;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHACREACION", nullable = false)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Column(name = "CLAVEMANEJATARIFABASE", nullable = true, precision = 4, scale = 0)
	public Boolean getClaveManejaTarifaBase() {
		return claveManejaTarifaBase;
	}

	public void setClaveManejaTarifaBase(Boolean claveManejaTarifaBase) {
		this.claveManejaTarifaBase = claveManejaTarifaBase;
	}

	@Column(name = "CLAVEESTATUS", nullable = true, precision = 4, scale = 0)
	public Short getClaveEstatus() {
		return claveEstatus;
	}

	public void setClaveEstatus(Short claveEstatus) {
		this.claveEstatus = claveEstatus;
	}
	
	@Column(name = "TIPOPERSONA", nullable = true, precision = 4, scale = 0)
	public Short getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(Short tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "negocio")
	public List<NegocioProducto> getNegocioProductos() {
		return this.negocioProductos;
	}

	public void setNegocioProductos(List<NegocioProducto> negocioProductos) {
		this.negocioProductos = negocioProductos;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "negocio")
	public List<NegocioCliente> getNegocioClientes() {
		return this.negocioClientes;
	}

	public void setNegocioClientes(List<NegocioCliente> negocioClientes) {
		this.negocioClientes = negocioClientes;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "negocio")
	public List<NegocioDerechoEndoso> getNegocioDerechoEndosos() {
		return this.negocioDerechoEndosos;
	}

	public void setNegocioDerechoEndosos(
			List<NegocioDerechoEndoso> negocioDerechoEndosos) {
		this.negocioDerechoEndosos = negocioDerechoEndosos;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "negocio")
	public List<NegocioAgente> getNegocioAgentes() {
		return this.negocioAgentes;
	}

	public void setNegocioAgentes(List<NegocioAgente> negocioAgentes) {
		this.negocioAgentes = negocioAgentes;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "negocio")
	public List<NegocioDerechoPoliza> getNegocioDerechoPolizas() {
		return this.negocioDerechoPolizas;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "negocio")
	public List<NegocioGrupoCliente> getGrupoCliente() {
		return grupoCliente;
	}

	public void setGrupoCliente(List<NegocioGrupoCliente> grupoCliente) {
		this.grupoCliente = grupoCliente;
	}

	public void setNegocioDerechoPolizas(
			List<NegocioDerechoPoliza> negocioDerechoPolizas) {
		this.negocioDerechoPolizas = negocioDerechoPolizas;
	}
	
	@Column(name = "claveDefault", nullable = false, precision = 4, scale = 0)
	public Boolean getClaveDefault() {
		return claveDefault;
	}

	public void setClaveDefault(Boolean claveDefault) {
		this.claveDefault = claveDefault;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.idToNegocio;
	}

	@Override
	public String getValue() {		
		return this.descripcionNegocio!=null?this.descripcionNegocio.toUpperCase():this.descripcionNegocio;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.idToNegocio;
	}

	public void setAplicaUsuarioExterno(Boolean aplicaUsuarioExterno) {
		this.aplicaUsuarioExterno = aplicaUsuarioExterno;
	}

	@Column(name = "aplicaUsuarioExterno", nullable = false, precision = 4, scale = 0)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Boolean getAplicaUsuarioExterno() {
		return aplicaUsuarioExterno;
	}

	public void setAplicaUsuarioInterno(Boolean aplicaUsuarioInterno) {
		this.aplicaUsuarioInterno = aplicaUsuarioInterno;
	}

	@Column(name = "aplicaUsuarioInterno", nullable = false, precision = 4, scale = 0)
	public Boolean getAplicaUsuarioInterno() {
		return aplicaUsuarioInterno;
	}

	@OneToOne(fetch = FetchType.EAGER, mappedBy = "negocio", cascade = CascadeType.PERSIST)
	public NegocioBonoComision getNegocioBonoComision() {
		return negocioBonoComision;
	}

	public void setNegocioBonoComision(NegocioBonoComision negocioBonoComision) {
		this.negocioBonoComision = negocioBonoComision;
	}

	public void setFechaActivacion(Date fechaActivacion) {
		this.fechaActivacion = fechaActivacion;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAACTIVACION", nullable = true, length = 7)
	public Date getFechaActivacion() {
		return fechaActivacion;
	}

	public void setCodigoUsuarioActivacion(String codigoUsuarioActivacion) {
		this.codigoUsuarioActivacion = codigoUsuarioActivacion;
	}

	@Column(name = "CODIGOUSUARIOACTIVACION", nullable = true, length = 8)
	public String getCodigoUsuarioActivacion() {
		return codigoUsuarioActivacion;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAFINVIGENCIAFIJA", nullable = true, length = 7)
	public Date getFechaFinVigenciaFija() {
		return fechaFinVigenciaFija;
	}

	public void setFechaFinVigenciaFija(Date fechaFinVigenciaFija) {
		this.fechaFinVigenciaFija = fechaFinVigenciaFija;
	}
	
	@Column(name = "APLICAPCTPAGOFRACCIONADO", nullable = false, precision = 4, scale = 0)
	public Boolean getAplicaPctPagoFraccionado() {
		return aplicaPctPagoFraccionado;
	}

	public void setAplicaPctPagoFraccionado(Boolean aplicaPctPagoFraccionado) {
		this.aplicaPctPagoFraccionado = aplicaPctPagoFraccionado;
	}

	/**
	 * @return the pctDescuentoDefault
	 */
	@Column(name = "PCTDESCUENTODEFAULT", nullable = false, precision = 8, scale = 4)
	@Digits(integer = 8, fraction= 4,groups=EditItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")
	public Double getPctDescuentoDefault() {
		return pctDescuentoDefault;
	}

	/**
	 * @param pctDescuentoDefault
	 *            the pctDescuentoDefault to set
	 */
	public void setPctDescuentoDefault(Double pctDescuentoDefault) {
		this.pctDescuentoDefault = pctDescuentoDefault;
	}

	public void setAplicaCURPObligatorio(Boolean aplicaCURPObligatorio) {
		this.aplicaCURPObligatorio = aplicaCURPObligatorio;
	}

	@Column(name = "APLICACURPOBLIGATORIO", nullable = false, precision = 4, scale = 0)
	public Boolean getAplicaCURPObligatorio() {
		return aplicaCURPObligatorio;
	}
	
	
	@Transient
	public boolean isNegocioActivoVigente() {
		Date now = new Date();
		return EstatusNegocio.ACTIVO.obtenerEstatus().equals(getClaveEstatus()) &&  now.compareTo(getFechaInicioVigencia()) > 0;
	}

	public void setAplicaValidacionExterno(Boolean aplicaValidacionExterno) {
		this.aplicaValidacionExterno = aplicaValidacionExterno;
	}

	@Column(name = "aplicaValidacionExterno", nullable = false, precision = 4, scale = 0)
	public Boolean getAplicaValidacionExterno() {
		return aplicaValidacionExterno;
	}

	public void setAplicaValidacionRecibo(Boolean aplicaValidacionRecibo) {
		this.aplicaValidacionRecibo = aplicaValidacionRecibo;
	}

	@Column(name = "aplicaValidacionRecibo", nullable = false, precision = 4, scale = 0)
	public Boolean getAplicaValidacionRecibo() {
		return aplicaValidacionRecibo;
	}

	public void setAplicaReciboGlobal(Boolean aplicaReciboGlobal) {
		this.aplicaReciboGlobal = aplicaReciboGlobal;
	}

	@Column(name = "aplicaReciboGlobal", nullable = false, precision = 4, scale = 0)
	public Boolean getAplicaReciboGlobal() {
		return aplicaReciboGlobal;
	}

	@Column(name = "habilitaAgrupacionRecibos", precision = 4, scale = 0)
	public Boolean getHabilitaAgrupacionRecibos() {
		return habilitaAgrupacionRecibos != null && habilitaAgrupacionRecibos.booleanValue() ? Boolean.TRUE : Boolean.FALSE;
	}

	public void setHabilitaAgrupacionRecibos(Boolean habilitaAgrupacionRecibos) {
		this.habilitaAgrupacionRecibos = habilitaAgrupacionRecibos;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "negocio")
	public List<NegocioCondicionEspecial> getCondicionesEspeciales() {
		return condicionesEspeciales;
	}
	
	public void setCondicionesEspeciales(
			List<NegocioCondicionEspecial> condicionesEspeciales) {
		this.condicionesEspeciales = condicionesEspeciales;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "negocio")
	public List<NegocioUsuarioEdicionImpresion> getUsuariosEdicionImpresion(){
		return usuariosEdicionImpresion;
	}
	
	public void setUsuariosEdicionImpresion(
			List<NegocioUsuarioEdicionImpresion> usuariosEdicionImpresion){
		this.usuariosEdicionImpresion = usuariosEdicionImpresion;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "negocio")
	public List<NegocioConfiguracionDerecho> getConfiguracionesDerecho() {
		return configuracionesDerecho;
	}

	public void setConfiguracionesDerecho(
			List<NegocioConfiguracionDerecho> configuracionesDerecho) {
		this.configuracionesDerecho = configuracionesDerecho;
	}

	@Column(name = "DESCUENTOMAXORIGINACION")
	public Double getDescuentoMaxOriginacion() {
		return descuentoMaxOriginacion;
	}

	public void setDescuentoMaxOriginacion(Double descuentoMaxOriginacion) {
		this.descuentoMaxOriginacion = descuentoMaxOriginacion;
	}

	public void setAplicaValidacionNumeroSerie(
			Boolean aplicaValidacionNumeroSerie) {
		this.aplicaValidacionNumeroSerie = aplicaValidacionNumeroSerie;
	}

	@Column(name = "APLICAVALIDACIONNUMSERIE")
	public Boolean getAplicaValidacionNumeroSerie() {
		return aplicaValidacionNumeroSerie;
	}

	public void setAplicaValidacionArticulo140(
			Boolean aplicaValidacionArticulo140) {
		this.aplicaValidacionArticulo140 = aplicaValidacionArticulo140;
	}

	@Column(name = "APLICAVALIDACIONARTICULO140")
	public Boolean getAplicaValidacionArticulo140() {
		return aplicaValidacionArticulo140;
	}

	public void setAplicaValidacionIgualacionPrimas(
			Boolean aplicaValidacionIgualacionPrimas) {
		this.aplicaValidacionIgualacionPrimas = aplicaValidacionIgualacionPrimas;
	}

	@Column(name = "APLICAVALIDACIONIGUALACION")
	public Boolean getAplicaValidacionIgualacionPrimas() {
		return aplicaValidacionIgualacionPrimas;
	}
	
	public void setAplicaPolizaGobierno(Boolean aplicaPolizaGobierno) {
		this.aplicaPolizaGobierno = aplicaPolizaGobierno;
	}
	
	@Column(name = "APLICAPOLIZAGOBIERNO")
	public Boolean getAplicaPolizaGobierno() {
		return aplicaPolizaGobierno;
	}

	@Column(name = "APLICAENLACE")
	public boolean getAplicaEnlace() {
		return aplicaEnlace;
	}

	public void setAplicaEnlace(boolean aplicaEnlace) {
		this.aplicaEnlace = aplicaEnlace;
	}
	
	@Column(name="NUMERO_DIAS_ESPERA")
	public Integer getNumeroDiasEspera() {
		return numeroDiasEspera;
	}

	public void setNumeroDiasEspera(Integer numeroDiasEspera) {
		this.numeroDiasEspera = numeroDiasEspera;
	}

	@Column(name="CORREO_OBLIGATORIO")
	public boolean isCorreoObligatorio() {
		return correoObligatorio;
	}

	public void setCorreoObligatorio(boolean correoObligatorio) {
		this.correoObligatorio = correoObligatorio;
	}
	
	@Column(name="APLICASUCURSAL")
	public Boolean getAplicaSucursal() {
		return aplicaSucursal;
	}

	public void setAplicaSucursal(Boolean aplicaSucursal) {
		this.aplicaSucursal = aplicaSucursal;
	}

	@Column(name="APLICAENDOSOCONDUCTOCOBRO")
	public Boolean getAplicaEndosoConductoCobro() {
		return aplicaEndosoConductoCobro;
	}

	public void setAplicaEndosoConductoCobro(Boolean aplicaEndosoConductoCobro) {
		this.aplicaEndosoConductoCobro = aplicaEndosoConductoCobro;
	}

	@Column(name="VALIDAPERSONAMORAL")
	public Boolean getAplicaValidacionPersonaMoral() {
		return aplicaValidacionPersonaMoral;
	}

	public void setAplicaValidacionPersonaMoral(Boolean aplicaValidacionPersonaMoral) {
		this.aplicaValidacionPersonaMoral = aplicaValidacionPersonaMoral;
	}

	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}

	@Column(name="CODIGOUSUARIOMODIF")
	public String getCodigoUsuarioModificacion() {
		return codigoUsuarioModificacion;
	}

	public void setAplicaDiasRetroCanCfp(Boolean aplicaDiasRetroCanCfp) {
		this.aplicaDiasRetroCanCfp = aplicaDiasRetroCanCfp;
	}

	@Column(name="APLICADIASRETROCANCFP")
	public Boolean getAplicaDiasRetroCanCfp() {
		return aplicaDiasRetroCanCfp;
	}
}