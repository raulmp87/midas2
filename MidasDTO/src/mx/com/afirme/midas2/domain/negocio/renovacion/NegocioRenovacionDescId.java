package mx.com.afirme.midas2.domain.negocio.renovacion;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * NegocioRenovacionDescId entity. @author Lizandro Perez
 */
@Embeddable
public class NegocioRenovacionDescId implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5037827528031749041L;
	
	private Long idToNegocio;
	private Short tipoRiesgo;
	private Short tipoDescuento;
	private Short numSiniestros;
	
	public NegocioRenovacionDescId(){
		
	}
	
	public NegocioRenovacionDescId(Long idToNegocio, Short tipoRiesgo, Short tipoDescuento, Short numSiniestros){
		this.setIdToNegocio(idToNegocio);
		this.setTipoRiesgo(tipoRiesgo);
		this.setTipoDescuento(tipoDescuento);
		this.setNumSiniestros(numSiniestros);
	}

	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}

	@Column(name="IDTONEGOCIO", nullable = false, precision = 22, scale = 0)
	public Long getIdToNegocio() {
		return idToNegocio;
	}

	public void setTipoRiesgo(Short tipoRiesgo) {
		this.tipoRiesgo = tipoRiesgo;
	}

	@Column(name = "TIPORIESGO", nullable = false, precision = 4, scale = 0)
	public Short getTipoRiesgo() {
		return tipoRiesgo;
	}

	public void setTipoDescuento(Short tipoDescuento) {
		this.tipoDescuento = tipoDescuento;
	}

	@Column(name = "TIPODESCUENTO", nullable = false, precision = 4, scale = 0)
	public Short getTipoDescuento() {
		return tipoDescuento;
	}

	public void setNumSiniestros(Short numSiniestros) {
		this.numSiniestros = numSiniestros;
	}

	@Column(name = "NUMSINIESTROS", nullable = false, precision = 4, scale = 0)
	public Short getNumSiniestros() {
		return numSiniestros;
	}

}
