package mx.com.afirme.midas2.domain.negocio.cliente;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * ConfiguracionCampo entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOCONFIGCAMPO", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = "CODIGO"))
public class ConfiguracionCampo implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	// Fields

	private Long id;
	private ConfiguracionGrupo configuracionGrupo;
	private String codigo;
	private String descripcion;
	private Short valorDefault;

	// Constructors

	/** default constructor */
	public ConfiguracionCampo() {
	}

	/** minimal constructor */
	public ConfiguracionCampo(Long id, ConfiguracionGrupo configuracionGrupo,
			String codigo, String descripcion) {
		this.id = id;
		this.configuracionGrupo = configuracionGrupo;
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	/** full constructor */
	public ConfiguracionCampo(Long id, ConfiguracionGrupo configuracionGrupo,
			String codigo, String descripcion, Short valorDefault) {
		this.id = id;
		this.configuracionGrupo = configuracionGrupo;
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.valorDefault = valorDefault;
	}

	public boolean esObligatorio(){
		return getValorDefault().equals((short)1);
	}
	
	// Property accessors
	@Id
	@SequenceGenerator(name = "IDCONFCMPO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "TOCONFIGCAMPO_IDTOCONFIGCA_SEQ", schema="MIDAS")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDCONFCMPO_SEQ_GENERADOR")
	@Column(name = "IDTOCONFIGCAMPO", unique = true, nullable = false, precision = 8, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOCONFIGGRUPO", nullable = false)
	public ConfiguracionGrupo getConfiguracionGrupo() {
		return this.configuracionGrupo;
	}

	public void setConfiguracionGrupo(ConfiguracionGrupo configuracionGrupo) {
		this.configuracionGrupo = configuracionGrupo;
	}

	@Column(name = "CODIGO", unique = true, nullable = false, length = 50)
	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Column(name = "DESCRIPCION", nullable = false, length = 300)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "VALORDAFAULT", precision = 2, scale = 0)
	public Short getValorDefault() {
		return this.valorDefault;
	}

	public void setValorDefault(Short valorDefault) {
		this.valorDefault = valorDefault;
	}

}