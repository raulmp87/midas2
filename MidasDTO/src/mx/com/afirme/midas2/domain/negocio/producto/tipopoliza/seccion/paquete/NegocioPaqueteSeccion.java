package mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;

/**
 * NegocioPaqueteSeccion entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TONEGPAQUETESECCION", schema = "MIDAS")
public class NegocioPaqueteSeccion implements java.io.Serializable,Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields

	private Long idToNegPaqueteSeccion;
	private Paquete paquete;
	private NegocioSeccion negocioSeccion;
	private BigDecimal modeloAntiguedadMax;
	private Integer aplicaPctDescuentoEdo;
	private List<NegocioCobPaqSeccion> negocioCobPaqSeccionList = new ArrayList<NegocioCobPaqSeccion>();

	// Constructors

	/** default constructor */
	public NegocioPaqueteSeccion() {
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "SEQ_NEG_PAQ_SEC_GEN", allocationSize = 1, sequenceName = "IDTONEGPAQUETESECCION_SEQ", schema="MIDAS")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_NEG_PAQ_SEC_GEN")
	@Column(name = "idToNegPaqueteSeccion", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getIdToNegPaqueteSeccion() {
		return this.idToNegPaqueteSeccion;
	}

	public void setIdToNegPaqueteSeccion(Long idToNegPaqueteSeccion) {
		this.idToNegPaqueteSeccion = idToNegPaqueteSeccion;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDPAQUETE", nullable = false)
	public Paquete getPaquete() {
		return this.paquete;
	}

	public void setPaquete(Paquete paquete) {
		this.paquete = paquete;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTONEGSECCION", nullable = false)
	public NegocioSeccion getNegocioSeccion() {
		return this.negocioSeccion;
	}

	public void setNegocioSeccion(NegocioSeccion negocioSeccion) {
		this.negocioSeccion = negocioSeccion;
	}

	@Column(name = "modeloAntiguedadMax", nullable = false, precision = 22, scale = 0)
	public BigDecimal getModeloAntiguedadMax() {
		return this.modeloAntiguedadMax;
	}

	public void setModeloAntiguedadMax(BigDecimal modeloAntiguedadMax) {
		this.modeloAntiguedadMax = modeloAntiguedadMax;
	}
	
	
	@Column(name = "aplicaPctDescuentoEdo")
	public Integer getAplicaPctDescuentoEdo() {
		return aplicaPctDescuentoEdo;
	}

	public void setAplicaPctDescuentoEdo(Integer aplicaPctDescuentoEdo) {
		this.aplicaPctDescuentoEdo = aplicaPctDescuentoEdo;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "negocioPaqueteSeccion")
	public List<NegocioCobPaqSeccion> getNegocioCobPaqSeccionList() {
		return this.negocioCobPaqSeccionList;
	}

	public void setNegocioCobPaqSeccionList(
			List<NegocioCobPaqSeccion> negocioCobPaqSeccionList) {
		this.negocioCobPaqSeccionList = negocioCobPaqSeccionList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.idToNegPaqueteSeccion;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.idToNegPaqueteSeccion;
	}
	

}