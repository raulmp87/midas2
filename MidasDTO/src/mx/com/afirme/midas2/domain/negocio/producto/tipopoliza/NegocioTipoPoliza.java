package mx.com.afirme.midas2.domain.negocio.producto.tipopoliza;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;

/**
 * NegocioTipoPoliza entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TONEGTIPOPOLIZA", schema = "MIDAS")
public class NegocioTipoPoliza implements java.io.Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields

	private BigDecimal idToNegTipoPoliza;
	private TipoPolizaDTO tipoPolizaDTO;
	private NegocioProducto negocioProducto;
	private List<NegocioSeccion> negocioSeccionList = new ArrayList<NegocioSeccion>();

	// Constructors

	/** default constructor */
	public NegocioTipoPoliza() {
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTONEGTIPOPOLIZA_SEQ")
    @SequenceGenerator(name="IDTONEGTIPOPOLIZA_SEQ", sequenceName="IDTONEGTIPOPOLIZA_SEQ", allocationSize=1, schema="MIDAS")
    @Column(name = "IDTONEGTIPOPOLIZA", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToNegTipoPoliza() {
		return idToNegTipoPoliza;
	}

	public void setIdToNegTipoPoliza(BigDecimal idToNegTipoPoliza) {
		this.idToNegTipoPoliza = idToNegTipoPoliza;
	}	


	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idToTipoPoliza", unique = true, nullable = false, insertable = false, updatable = true)
	public TipoPolizaDTO getTipoPolizaDTO() {
		return this.tipoPolizaDTO;
	}

	public void setTipoPolizaDTO(TipoPolizaDTO tipoPolizaDTO) {
		this.tipoPolizaDTO = tipoPolizaDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTONEGPRODUCTO", nullable = false)
	public NegocioProducto getNegocioProducto() {
		return this.negocioProducto;
	}

	public void setNegocioProducto(NegocioProducto negocioProducto) {
		this.negocioProducto = negocioProducto;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "negocioTipoPoliza")
	public List<NegocioSeccion> getNegocioSeccionList() {
		return this.negocioSeccionList;
	}

	public void setNegocioSeccionList(List<NegocioSeccion> negocioSeccionList) {
		this.negocioSeccionList = negocioSeccionList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		
		return idToNegTipoPoliza;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}