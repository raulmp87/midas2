package mx.com.afirme.midas2.domain.negocio.cliente;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * ConfiguracionSeccion entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOCONFIGSECCION", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = "CODIGO"))
public class ConfiguracionSeccion implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields

	private Long id;
	private String codigo;
	private String descripcion;
	private Short valorDefault;
	private List<ConfiguracionGrupo> configuracionGrupos = new ArrayList<ConfiguracionGrupo>();
	private List<ConfiguracionSeccionPorNegocio> configuracionSeccionPorNegocios = new ArrayList<ConfiguracionSeccionPorNegocio>();

	// Constructors

	/** default constructor */
	public ConfiguracionSeccion() {
	}

	/** minimal constructor */
	public ConfiguracionSeccion(Long id, String codigo) {
		this.id = id;
		this.codigo = codigo;
	}

	/** full constructor */
	public ConfiguracionSeccion(Long id, String codigo, String descripcion,
			Short valorDefault, List<ConfiguracionGrupo> configuracionGrupos,
			List<ConfiguracionSeccionPorNegocio> configuracionSeccionPorNegocios) {
		this.id = id;
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.valorDefault = valorDefault;
		this.configuracionGrupos = configuracionGrupos;
		this.configuracionSeccionPorNegocios = configuracionSeccionPorNegocios;
	}

	public boolean esObligatorio(){
		return getValorDefault().equals((short)1);
	}
	
	// Property accessors
	@Id
	@SequenceGenerator(name = "IDCONFSEC_SEQ_GENERADOR", allocationSize = 1, sequenceName = "TOCONFIGSECCI_IDTOCONFIGSE_SEQ", schema="MIDAS")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDCONFSEC_SEQ_GENERADOR")
	@Column(name = "IDTOCONFIGSECCION", unique = true, nullable = false, precision = 8, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "CODIGO", unique = true, nullable = false, length = 50)
	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Column(name = "DESCRIPCION", length = 300)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "VALORDEFAULT", precision = 4, scale = 0)
	public Short getValorDefault() {
		return this.valorDefault;
	}

	public void setValorDefault(Short valorDefault) {
		this.valorDefault = valorDefault;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "configuracionSeccion")
	public List<ConfiguracionGrupo> getConfiguracionGrupos() {
		return this.configuracionGrupos;
	}

	public void setConfiguracionGrupos(List<ConfiguracionGrupo> configuracionGrupos) {
		this.configuracionGrupos = configuracionGrupos;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "configuracionSeccion")
	public List<ConfiguracionSeccionPorNegocio> getConfiguracionSeccionPorNegocios() {
		return this.configuracionSeccionPorNegocios;
	}

	public void setConfiguracionSeccionPorNegocios(List<ConfiguracionSeccionPorNegocio> configuracionSeccionPorNegocios) {
		this.configuracionSeccionPorNegocios = configuracionSeccionPorNegocios;
	}

}