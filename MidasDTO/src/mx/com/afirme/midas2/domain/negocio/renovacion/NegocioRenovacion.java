package mx.com.afirme.midas2.domain.negocio.renovacion;

import java.math.BigDecimal;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;


import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * NegocioRenovacion entity. @author Lizandro Perez
 */
@Entity
@Table(name = "TONEGRENOVACION", schema = "MIDAS")
public class NegocioRenovacion implements java.io.Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields

	public static enum TipoRiesgo{
		NORMAL((short)1), ALTORIESGO((short)2), ALTAFRECUENCIA((short)3);		
		TipoRiesgo(Short tipo) {
			this.tipo = tipo;
		}
		private Short tipo;
		public Short obtenerTipo(){
			return this.tipo;
		}
		public Short getObtenerTipo(){
			return this.obtenerTipo();
		}
		
	};		
	private NegocioRenovacionId id = new NegocioRenovacionId();
	
	private Boolean renuevaPoliza;
	private Boolean mantieneDescuentoComercial;
	private Boolean otorgaDescuentoComercial;
	private Double valorDescuentoComercial;
	private Boolean mantieneCesionComision;
	private Boolean aplicarPrimaAnterior;
	private Boolean detenerNumeroIncisos;
	private BigDecimal numeroIncisos;
	private Boolean aumentarPctTarifa;
	private Double pctTarifa;
	private Boolean limiteIncrementoTarifa;
	private Double pctLimiteIncremento;
	private Boolean sinLimiteSinietros;
	private BigDecimal limiteSiniestros;
	private Boolean aplicaDescuentoNoSinietro;
	private Boolean aplicaDeducibleDanos;
	
	
	// Constructors

	/** default constructor */
	public NegocioRenovacion() {
		
		setRenuevaPoliza(Boolean.TRUE);
		setMantieneDescuentoComercial(Boolean.TRUE);
		setOtorgaDescuentoComercial(Boolean.FALSE);
		setValorDescuentoComercial(Double.valueOf(0));
		setMantieneCesionComision(Boolean.FALSE);
		setAplicarPrimaAnterior(Boolean.FALSE);
		setDetenerNumeroIncisos(Boolean.FALSE);
		setNumeroIncisos(BigDecimal.ZERO);
		setAumentarPctTarifa(Boolean.FALSE);
		setPctTarifa(Double.valueOf(0));
		setLimiteIncrementoTarifa(Boolean.FALSE);
		setPctLimiteIncremento(Double.valueOf(0));
		setSinLimiteSinietros(Boolean.FALSE);
		setLimiteSiniestros(BigDecimal.ZERO);
		setAplicaDescuentoNoSinietro(Boolean.FALSE);
		setAplicaDeducibleDanos(Boolean.FALSE);
		
	}


	public void setId(NegocioRenovacionId id) {
		this.id = id;
	}

    @EmbeddedId
    @AttributeOverrides( {
        @AttributeOverride(name="idToNegocio", column=@Column(name="IDTONEGOCIO", nullable=false, precision = 22, scale = 0) ), 
        @AttributeOverride(name="tipoRiesgo", column=@Column(name="TIPORIESGO", nullable=false,  precision = 4, scale = 0) ) } )
	public NegocioRenovacionId getId() {
		return id;
	}

	public void setRenuevaPoliza(Boolean renuevaPoliza) {
		this.renuevaPoliza = renuevaPoliza;
	}

	@Column(name = "RENUEVAPOLIZA", nullable = true, precision = 4, scale = 0)
	public Boolean getRenuevaPoliza() {
		return renuevaPoliza;
	}


	public void setMantieneDescuentoComercial(Boolean mantieneDescuentoComercial) {
		this.mantieneDescuentoComercial = mantieneDescuentoComercial;
	}

	@Column(name = "MANTIENEDESCUENTOCOMERCIAL", nullable = true, precision = 4, scale = 0)
	public Boolean getMantieneDescuentoComercial() {
		return mantieneDescuentoComercial;
	}


	public void setOtorgaDescuentoComercial(Boolean otorgaDescuentoComercial) {
		this.otorgaDescuentoComercial = otorgaDescuentoComercial;
	}

	@Column(name = "OTORGADESCUENTOCOMERCIAL", nullable = true, precision = 4, scale = 0)
	public Boolean getOtorgaDescuentoComercial() {
		return otorgaDescuentoComercial;
	}


	public void setValorDescuentoComercial(Double valorDescuentoComercial) {
		this.valorDescuentoComercial = valorDescuentoComercial;
	}

	@Column(name = "VALORDESCUENTOCOMERCIAL", nullable = false, precision = 8, scale = 4)
	public Double getValorDescuentoComercial() {
		return valorDescuentoComercial;
	}


	public void setMantieneCesionComision(Boolean mantieneCesionComision) {
		this.mantieneCesionComision = mantieneCesionComision;
	}

	@Column(name = "MANTIENECESIONCOMISION", nullable = true, precision = 4, scale = 0)
	public Boolean getMantieneCesionComision() {
		return mantieneCesionComision;
	}


	public void setAplicarPrimaAnterior(Boolean aplicarPrimaAnterior) {
		this.aplicarPrimaAnterior = aplicarPrimaAnterior;
	}

	@Column(name = "APLICARPRIMAANTERIOR", nullable = true, precision = 4, scale = 0)
	public Boolean getAplicarPrimaAnterior() {
		return aplicarPrimaAnterior;
	}


	public void setDetenerNumeroIncisos(Boolean detenerNumeroIncisos) {
		this.detenerNumeroIncisos = detenerNumeroIncisos;
	}

	@Column(name = "DETENERNUMEROINCISOS", nullable = true, precision = 4, scale = 0)
	public Boolean getDetenerNumeroIncisos() {
		return detenerNumeroIncisos;
	}


	public void setNumeroIncisos(BigDecimal numeroIncisos) {
		this.numeroIncisos = numeroIncisos;
	}

	@Column(name = "NUMEROINCISOS", nullable = false, precision = 8, scale = 4)
	public BigDecimal getNumeroIncisos() {
		return numeroIncisos;
	}


	public void setAumentarPctTarifa(Boolean aumentarPctTarifa) {
		this.aumentarPctTarifa = aumentarPctTarifa;
	}

	@Column(name = "AUMENTARPCTTARIFA", nullable = true, precision = 4, scale = 0)
	public Boolean getAumentarPctTarifa() {
		return aumentarPctTarifa;
	}


	public void setPctTarifa(Double pctTarifa) {
		this.pctTarifa = pctTarifa;
	}

	@Column(name = "PCTTARIFA", nullable = false, precision = 8, scale = 4)
	public Double getPctTarifa() {
		return pctTarifa;
	}


	public void setLimiteIncrementoTarifa(Boolean limiteIncrementoTarifa) {
		this.limiteIncrementoTarifa = limiteIncrementoTarifa;
	}

	@Column(name = "LIMITEINCREMENTOTARIFA", nullable = true, precision = 4, scale = 0)
	public Boolean getLimiteIncrementoTarifa() {
		return limiteIncrementoTarifa;
	}


	public void setPctLimiteIncremento(Double pctLimiteIncremento) {
		this.pctLimiteIncremento = pctLimiteIncremento;
	}

	@Column(name = "PCTLIMITEINCREMENTO", nullable = true, precision = 8, scale = 4)
	public Double getPctLimiteIncremento() {
		return pctLimiteIncremento;
	}


	public void setSinLimiteSinietros(Boolean sinLimiteSinietros) {
		this.sinLimiteSinietros = sinLimiteSinietros;
	}

	@Column(name = "SINLIMITESINIESTROS", nullable = true, precision = 4, scale = 0)
	public Boolean getSinLimiteSinietros() {
		return sinLimiteSinietros;
	}


	public void setLimiteSiniestros(BigDecimal limiteSiniestros) {
		this.limiteSiniestros = limiteSiniestros;
	}

	@Column(name = "LIMITESINIESTROS", nullable = true, precision = 22, scale = 0)
	public BigDecimal getLimiteSiniestros() {
		return limiteSiniestros;
	}


	public void setAplicaDescuentoNoSinietro(Boolean aplicaDescuentoNoSinietro) {
		this.aplicaDescuentoNoSinietro = aplicaDescuentoNoSinietro;
	}

	@Column(name = "APLICADESCUENTONOSINIESTRO", nullable = true, precision = 4, scale = 0)
	public Boolean getAplicaDescuentoNoSinietro() {
		return aplicaDescuentoNoSinietro;
	}


	public void setAplicaDeducibleDanos(Boolean aplicaDeducibleDanos) {
		this.aplicaDeducibleDanos = aplicaDeducibleDanos;
	}

	@Column(name = "APLICADEDUCIBLEDANOS", nullable = true, precision = 4, scale = 0)
	public Boolean getAplicaDeducibleDanos() {
		return aplicaDeducibleDanos;
	}


	@SuppressWarnings("unchecked")
	@Override
	public NegocioRenovacionId getKey() {
		return this.id;
	}


	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	
}