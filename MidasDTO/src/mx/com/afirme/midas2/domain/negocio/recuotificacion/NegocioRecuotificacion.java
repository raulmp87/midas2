package mx.com.afirme.midas2.domain.negocio.recuotificacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.negocio.Negocio;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

@Entity
@Table( name="TONEGRECUOTIFICA", schema = "MIDAS")
public class NegocioRecuotificacion extends MidasAbstracto implements Entidad {
	
	private static final long serialVersionUID = -6572034735332291821L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTONEGREC_ID_GENERATOR")
	@SequenceGenerator(name="IDTONEGREC_ID_GENERATOR", schema="MIDAS", sequenceName="TONEGRECUOTIFICA_ID_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch(JoinFetchType.INNER)
	@JoinColumn(name="IDTONEGOCIO", nullable=false)
	private Negocio negocio;
	
	@Column(name="RECAUTOMATICA")	
	private Boolean activarAutomatica = Boolean.FALSE;
	
	@Column(name="RECMANUAL")
	private Boolean activarManual = Boolean.FALSE;
	
	@Column(name="RECAJUSTEPRIMA")
	private Boolean activarModificacionPrimaTotal = Boolean.FALSE;
	
	@Column(name="RANGOAJUSTEPRIMA")	
	private BigDecimal montoPrimaTotal = BigDecimal.ZERO;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "negRecuotificacion")
	private List<NegocioRecuotificacionAutomatica> versiones = new ArrayList<NegocioRecuotificacionAutomatica>();
	
	@Transient
	private String ultimaVersionCreada = null;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	
	public Boolean getActivarAutomatica() {
		return activarAutomatica;
	}

	public void setActivarAutomatica(Boolean activarAutomatica) {
		this.activarAutomatica = activarAutomatica;
	}


	public Boolean getActivarManual() {
		return activarManual;
	}

	public void setActivarManual(Boolean activarManual) {
		this.activarManual = activarManual;
	}

	
	public Boolean getActivarModificacionPrimaTotal() {
		return activarModificacionPrimaTotal;
	}

	public void setActivarModificacionPrimaTotal(
			Boolean activarModificacionPrimaTotal) {
		this.activarModificacionPrimaTotal = activarModificacionPrimaTotal;
	}

	public BigDecimal getMontoPrimaTotal() {
		return montoPrimaTotal;
	}


	public void setMontoPrimaTotal(BigDecimal montoPrimaTotal) {
		this.montoPrimaTotal = montoPrimaTotal;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {		
		return this.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {		
		return this.getId();
	}

	public String getUltimaVersionCreada() {
		return ultimaVersionCreada;
	}

	public void setUltimaVersionCreada(String ultimaVersionCreada) {
		this.ultimaVersionCreada = ultimaVersionCreada;
	}

	public List<NegocioRecuotificacionAutomatica> getVersiones() {
		return versiones;
	}

	public void setVersiones(List<NegocioRecuotificacionAutomatica> versiones) {
		this.versiones = versiones;
	}

}
