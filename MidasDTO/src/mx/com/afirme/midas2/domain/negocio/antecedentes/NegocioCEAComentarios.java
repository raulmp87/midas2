package mx.com.afirme.midas2.domain.negocio.antecedentes;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="tocenacomentarios", schema="MIDAS")
public class NegocioCEAComentarios implements Serializable, Entidad {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTOCENACOMENTARIOS_ID_GENERATOR")	
	@SequenceGenerator(name="IDTOCENACOMENTARIOS_ID_GENERATOR", sequenceName="MIDAS.IDTOCENACOMENTARIOS_SEQ", allocationSize=1)	
	@Column(name="ID")
	private Long id;

	@Column(name="COMENTARIO", nullable=false)
	private String comentario;	

	@Column(name="USUARIO_NOMBRE", nullable=false)
	private String usuarioNombre;

	@Column(name="USUARIO_ID", nullable=false)
	private String usuarioId;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREACION", nullable=false)
	private Date fechaCreacion;
	
	@Column(name="IDTONEGOCIO", nullable=false)
	private Long idToNegocio;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public String getUsuarioNombre() {
		return usuarioNombre;
	}

	public void setUsuarioNombre(String usuarioNombre) {
		this.usuarioNombre = usuarioNombre;
	}

	public String getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(String usuarioId) {
		this.usuarioId = usuarioId;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	public Long getIdToNegocio() {
		return idToNegocio;
	}

	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}

	@Override
	public <K> K getKey() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public String getValue() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

}
