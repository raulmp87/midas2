package mx.com.afirme.midas2.domain.negocio.renovacion;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;


import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * NegocioRenovacionDesc entity. @author Lizandro Perez
 */
@Entity
@Table(name = "TONEGRENOVACIONDESC", schema = "MIDAS")
public class NegocioRenovacionDesc implements java.io.Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields

	public static enum TipoRiesgo{
		NORMAL((short)1), ALTORIESGO((short)2), ALTAFRECUENCIA((short)3);		
		TipoRiesgo(Short tipo) {
			this.tipo = tipo;
		}
		private Short tipo;
		public Short obtenerTipo(){
			return this.tipo;
		}
		public Short getObtenerTipo(){
			return this.obtenerTipo();
		}
		
	};		
	
	public static enum TipoDescuento{
		DESCUENTO((short)1), DEDUCIBLE((short)2);		
		TipoDescuento(Short tipo) {
			this.tipo = tipo;
		}
		private Short tipo;
		public Short obtenerTipo(){
			return this.tipo;
		}
		public Short getObtenerTipo(){
			return this.obtenerTipo();
		}
		
	};	
	
	private NegocioRenovacionDescId id = new NegocioRenovacionDescId();
	
	private Double ren1;
	private Double ren2;
	private Double ren3;
	private Double ren4;
	private Double ren5;
	private Double ren6;
	
	// Constructors

	/** default constructor */
	public NegocioRenovacionDesc() {
		setRen1(Double.valueOf(0));
		setRen2(Double.valueOf(0));
		setRen3(Double.valueOf(0));
		setRen4(Double.valueOf(0));
		setRen5(Double.valueOf(0));
		setRen6(Double.valueOf(0));
	}
	
	public String getSiniestroStr(){
		String siniestroStr = "";
		
		switch (id.getNumSiniestros()) {
		case 7:
			siniestroStr = "7 o m\u00E1s";
			break;
		default:
			siniestroStr = id.getNumSiniestros().toString();
		}
		
		return siniestroStr;
	}


	public void setId(NegocioRenovacionDescId id) {
		this.id = id;
	}

    @EmbeddedId
    @AttributeOverrides( {
        @AttributeOverride(name="idToNegocio", column=@Column(name="IDTONEGOCIO", nullable=false, precision = 22, scale = 0) ),
        @AttributeOverride(name="tipoRiesgo", column=@Column(name="TIPORIESGO", nullable=false,  precision = 4, scale = 0) ),
        @AttributeOverride(name="tipoDescuento", column=@Column(name="TIPODESCUENTO", nullable=false,  precision = 4, scale = 0) ),
        @AttributeOverride(name="numSiniestros", column=@Column(name="NUMSINIESTROS", nullable=false,  precision = 4, scale = 0) ) } )
	public NegocioRenovacionDescId getId() {
		return id;
	}


	public void setRen1(Double ren1) {
		this.ren1 = ren1;
	}


	@Column(name = "REN1", nullable = false, precision = 8, scale = 4)
	public Double getRen1() {
		return ren1;
	}


	public void setRen2(Double ren2) {
		this.ren2 = ren2;
	}


	@Column(name = "REN2", nullable = false, precision = 8, scale = 4)
	public Double getRen2() {
		return ren2;
	}


	public void setRen3(Double ren3) {
		this.ren3 = ren3;
	}


	@Column(name = "REN3", nullable = false, precision = 8, scale = 4)
	public Double getRen3() {
		return ren3;
	}


	public void setRen4(Double ren4) {
		this.ren4 = ren4;
	}


	@Column(name = "REN4", nullable = false, precision = 8, scale = 4)
	public Double getRen4() {
		return ren4;
	}


	public void setRen5(Double ren5) {
		this.ren5 = ren5;
	}


	@Column(name = "REN5", nullable = false, precision = 8, scale = 4)
	public Double getRen5() {
		return ren5;
	}


	public void setRen6(Double ren6) {
		this.ren6 = ren6;
	}


	@Column(name = "REN6", nullable = false, precision = 8, scale = 4)
	public Double getRen6() {
		return ren6;
	}


	@SuppressWarnings("unchecked")
	@Override
	public NegocioRenovacionDescId getKey() {
		return this.id;
	}


	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}


	
}