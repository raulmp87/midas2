package mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tiposervicio;

// default package

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.tiposerviciovehiculo.TipoServicioVehiculoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;

/**
 * NegocioTipoServicio entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TONEGTIPOSERVICIO", schema = "MIDAS")
public class NegocioTipoServicio implements java.io.Serializable,Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields

	private Long idToNegTipoServicio;
	private NegocioSeccion negocioSeccion;
	private TipoServicioVehiculoDTO tipoServicioVehiculoDTO;
	private Boolean claveDefault;

	// Constructors

	/** default constructor */
	public NegocioTipoServicio() {
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTONEGTIPOSERVICIO_SEQ")
	@SequenceGenerator(name="IDTONEGTIPOSERVICIO_SEQ", sequenceName="IDTONEGTIPOSERVICIO_SEQ", allocationSize=1,schema="MIDAS")
	@Column(name = "idToNegTipoServicio", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getIdToNegTipoServicio() {
		return this.idToNegTipoServicio;
	}

	public void setIdToNegTipoServicio(Long idToNegTipoServicio) {
		this.idToNegTipoServicio = idToNegTipoServicio;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTONEGSECCION", nullable = false)
	public NegocioSeccion getNegocioSeccion() {
		return this.negocioSeccion;
	}

	public void setNegocioSeccion(NegocioSeccion negocioSeccion) {
		this.negocioSeccion = negocioSeccion;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCTIPOSERVICIOVEHICULO", nullable = false)
	public TipoServicioVehiculoDTO getTipoServicioVehiculoDTO() {
		return this.tipoServicioVehiculoDTO;
	}

	public void setTipoServicioVehiculoDTO(
			TipoServicioVehiculoDTO tipoServicioVehiculoDTO) {
		this.tipoServicioVehiculoDTO = tipoServicioVehiculoDTO;
	}

	@Column(name = "claveDefault", nullable = false, precision = 4, scale = 0)
	public Boolean getClaveDefault() {
		return this.claveDefault;
	}

	public void setClaveDefault(Boolean claveDefault) {
		this.claveDefault = claveDefault;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.idToNegTipoServicio;
	}

	@Override
	public String getValue() {
		return this.getTipoServicioVehiculoDTO().getDescripcionTipoServVehiculo();
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}