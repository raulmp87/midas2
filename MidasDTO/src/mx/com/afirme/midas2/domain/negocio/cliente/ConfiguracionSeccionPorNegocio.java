package mx.com.afirme.midas2.domain.negocio.cliente;
// default package

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * ConfiguracionSeccionPorNegocio entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TRCONFIGSECCIONNEG", schema = "MIDAS")
public class ConfiguracionSeccionPorNegocio implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private ConfiguracionSeccion configuracionSeccion;
	private Long idNegocio;
	private Short obligatoriedad;
	private List<ConfiguracionGrupoPorNegocio> configuracionGrupoPorNegocios = new ArrayList<ConfiguracionGrupoPorNegocio>();

	// Constructors

	/** default constructor */
	public ConfiguracionSeccionPorNegocio() {
	}

	/** minimal constructor */
	public ConfiguracionSeccionPorNegocio(Long id,
			ConfiguracionSeccion configuracionSeccion, Long idNegocio,
			Short obligatoriedad) {
		this.id = id;
		this.configuracionSeccion = configuracionSeccion;
		this.idNegocio = idNegocio;
		this.obligatoriedad = obligatoriedad;
	}

	/** full constructor */
	public ConfiguracionSeccionPorNegocio(Long id,
			ConfiguracionSeccion configuracionSeccion, Long idNegocio,
			Short obligatoriedad,
			List<ConfiguracionGrupoPorNegocio> configuracionGrupoPorNegocios) {
		this.id = id;
		this.configuracionSeccion = configuracionSeccion;
		this.idNegocio = idNegocio;
		this.obligatoriedad = obligatoriedad;
		this.configuracionGrupoPorNegocios = configuracionGrupoPorNegocios;
	}

	public boolean esObligatorio(){
		return getObligatoriedad().equals((short)1);
	}
	
	// Property accessors
	@Id
	@SequenceGenerator(name = "IDCONFSECPORNEG_SEQ_GEN", allocationSize = 1, sequenceName = "TRCONFIGSECCI_IDTOCONFIGSE_SEQ", schema="MIDAS")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDCONFSECPORNEG_SEQ_GEN")
	@Column(name = "IDTOCONFIGSECCIONNEG", unique = true, nullable = false, precision = 8, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOCONFIGSECCION", nullable = false)
	public ConfiguracionSeccion getConfiguracionSeccion() {
		return this.configuracionSeccion;
	}

	public void setConfiguracionSeccion(
			ConfiguracionSeccion configuracionSeccion) {
		this.configuracionSeccion = configuracionSeccion;
	}

	@Column(name = "IDNEGOCIO", nullable = false, precision = 8, scale = 0)
	public Long getIdNegocio() {
		return this.idNegocio;
	}

	public void setIdNegocio(Long idNegocio) {
		this.idNegocio = idNegocio;
	}

	@Column(name = "OBLIGATORIEDAD", nullable = false, precision = 2, scale = 0)
	public Short getObligatoriedad() {
		return this.obligatoriedad;
	}

	public void setObligatoriedad(Short obligatoriedad) {
		this.obligatoriedad = obligatoriedad;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "configuracionSeccionPorNegocio")
	public List<ConfiguracionGrupoPorNegocio> getConfiguracionGrupoPorNegocios() {
		return this.configuracionGrupoPorNegocios;
	}

	public void setConfiguracionGrupoPorNegocios(
			List<ConfiguracionGrupoPorNegocio> configuracionGrupoPorNegocios) {
		this.configuracionGrupoPorNegocios = configuracionGrupoPorNegocios;
	}
}