package mx.com.afirme.midas2.domain.negocio.derechos;
// default package

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.negocio.Negocio;


/**
 * NegocioDerechoPoliza entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TONEGDERECHOPOLIZA"
    ,schema="MIDAS"
)
public class NegocioDerechoPoliza  implements Serializable, Entidad {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = -7551238541093948579L;
	private Long idToNegDerechoPoliza;
    private Negocio negocio;
    private Double importeDerecho;
    private Boolean claveDefault;
    private Boolean activo;


    // Constructors

    /** default constructor */
    public NegocioDerechoPoliza() {
    }

    
    /** full constructor */
    public NegocioDerechoPoliza(Long idToNegDerechoPoliza, Negocio negocio, Double importeDerecho, Boolean claveDefault) {
        this.idToNegDerechoPoliza = idToNegDerechoPoliza;
        this.negocio = negocio;
        this.importeDerecho = importeDerecho;
        this.claveDefault = claveDefault;
    }

   
    // Property accessors
    @Id 
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTONEGDERECHOPOLIZA_SEQ")
	@SequenceGenerator(name="IDTONEGDERECHOPOLIZA_SEQ", sequenceName="MIDAS.IDTONEGDERECHOPOLIZA_SEQ", allocationSize=1)
	@Column(name="idToNegDerechoPoliza", unique=true, nullable=false, precision=22, scale=0)
    public Long getIdToNegDerechoPoliza() {
        return this.idToNegDerechoPoliza;
    }
    
    public void setIdToNegDerechoPoliza(Long idToNegDerechoPoliza) {
        this.idToNegDerechoPoliza = idToNegDerechoPoliza;
    }
	
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="IDTONEGOCIO", nullable=false)
    public Negocio getNegocio() {
        return this.negocio;
    }
    
    public void setNegocio(Negocio negocio) {
        this.negocio = negocio;
    }
    
    @Column(name="importeDerecho", nullable=false, precision=16)
    @NotNull
    public Double getImporteDerecho() {
        return this.importeDerecho;
    }
    
    public void setImporteDerecho(Double importeDerecho) {
        this.importeDerecho = importeDerecho;
    }
    
    @Column(name="claveDefault", nullable=false, precision=4, scale=0)
    @NotNull
    public Boolean getClaveDefault() {
        return this.claveDefault;
    }
    
    public void setClaveDefault(Boolean claveDefault) {
        this.claveDefault = claveDefault;
    }


	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.idToNegDerechoPoliza;
	}


	@Override
	public String getValue() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.idToNegDerechoPoliza;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((idToNegDerechoPoliza == null) ? 0 : idToNegDerechoPoliza
						.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NegocioDerechoPoliza other = (NegocioDerechoPoliza) obj;
		if (idToNegDerechoPoliza == null) {
			if (other.idToNegDerechoPoliza != null)
				return false;
		} else if (!idToNegDerechoPoliza.equals(other.idToNegDerechoPoliza))
			return false;
		return true;
	}
	
	@Column(name="ACTIVO", nullable=false, precision=1, scale=0)
    @NotNull
	public Boolean getActivo() {
		return activo;
	}


	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
    
}