package mx.com.afirme.midas2.domain.negocio.tipovigencia;

import java.io.Serializable;

import javax.persistence.*;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.validator.group.NewItemChecks;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.validation.constraints.Min;


/**
 * The persistent class for the TCTIPOVIGENCIA database table.
 * 
 */
@Entity
@Table(name="TCTIPOVIGENCIA", schema="MIDAS")
public class TipoVigencia implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TCTIPOVIGENCIA_ID_GENERATOR", sequenceName="SEQ_TCTIPOVIGENCIA_ID", allocationSize=1, schema="MIDAS")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TCTIPOVIGENCIA_ID_GENERATOR")
	@Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
	private Long id;

	@Column(name = "DESCRIPCION", length = 500, nullable = false)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=1, max=500, groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	private String descripcion;

	@Column(name = "DIAS", nullable = false, precision = 10, scale = 0)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Min(value = 1, groups=NewItemChecks.class, message = "{javax.validation.constraints.Min.message}")
	private Integer dias;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHACREACION", nullable = false)
	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	private Date fechaCreacion;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHAMODIFICACION", nullable = true)	
	private Date fechaModificacion;

	@Column(name="USUARIOCREACION_ID", nullable = false)
	private Integer usuarioCreacionId;

	@Column(name="USUARIOMODIFICACION_ID", nullable = true)
	private Integer usuarioModificacionId;

	//bi-directional many-to-one association to NegocioTipoVigencia
	@OneToMany(mappedBy="tipoVigencia", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<NegocioTipoVigencia> negocioTipoVigencia;

	/** default constructor */
	public TipoVigencia() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getDias() {
		return this.dias;
	}

	public void setDias(Integer dias) {
		this.dias = dias;
	}

	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public Integer getUsuarioCreacionId() {
		return this.usuarioCreacionId;
	}

	public void setUsuarioCreacionId(Integer usuarioCreacionId) {
		this.usuarioCreacionId = usuarioCreacionId;
	}

	public Integer getUsuarioModificacionId() {
		return this.usuarioModificacionId;
	}

	public void setUsuarioModificacionId(Integer usuarioModificacionId) {
		this.usuarioModificacionId = usuarioModificacionId;
	}

	public List<NegocioTipoVigencia> getNegocioTipoVigencia() {
		return this.negocioTipoVigencia;
	}

	public void setNegocioTipoVigencia(List<NegocioTipoVigencia> negocioTipoVigencia) {
		this.negocioTipoVigencia = negocioTipoVigencia;
	}

	public NegocioTipoVigencia addNegocioTipoVigencia(NegocioTipoVigencia negocioTipoVigencia) {
		getNegocioTipoVigencia().add(negocioTipoVigencia);
		negocioTipoVigencia.setTipoVigencia(this);

		return negocioTipoVigencia;
	}

	public NegocioTipoVigencia removeNegocioTipoVigencia(NegocioTipoVigencia negocioTipoVigencia) {
		getNegocioTipoVigencia().remove(negocioTipoVigencia);
		negocioTipoVigencia.setTipoVigencia(null);

		return negocioTipoVigencia;
	}

	/* (non-Javadoc)
	 * @see mx.com.afirme.midas2.dao.catalogos.Entidad#getKey()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	/* (non-Javadoc)
	 * @see mx.com.afirme.midas2.dao.catalogos.Entidad#getValue()
	 */
	@Override
	public String getValue() {
		return this.descripcion;
	}

	/* (non-Javadoc)
	 * @see mx.com.afirme.midas2.dao.catalogos.Entidad#getBusinessKey()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.id;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((descripcion == null) ? 0 : descripcion.hashCode());
		result = prime * result + ((dias == null) ? 0 : dias.hashCode());
		result = prime * result
				+ ((fechaCreacion == null) ? 0 : fechaCreacion.hashCode());
		result = prime
				* result
				+ ((fechaModificacion == null) ? 0 : fechaModificacion
						.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime
				* result
				+ ((negocioTipoVigencia == null) ? 0 : negocioTipoVigencia
						.hashCode());
		result = prime
				* result
				+ ((usuarioCreacionId == null) ? 0 : usuarioCreacionId
						.hashCode());
		result = prime
				* result
				+ ((usuarioModificacionId == null) ? 0 : usuarioModificacionId
						.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoVigencia other = (TipoVigencia) obj;
		if (descripcion == null) {
			if (other.descripcion != null)
				return false;
		} else if (!descripcion.equals(other.descripcion))
			return false;
		if (dias == null) {
			if (other.dias != null)
				return false;
		} else if (!dias.equals(other.dias))
			return false;
		if (fechaCreacion == null) {
			if (other.fechaCreacion != null)
				return false;
		} else if (!fechaCreacion.equals(other.fechaCreacion))
			return false;
		if (fechaModificacion == null) {
			if (other.fechaModificacion != null)
				return false;
		} else if (!fechaModificacion.equals(other.fechaModificacion))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (negocioTipoVigencia == null) {
			if (other.negocioTipoVigencia != null)
				return false;
		} else if (!negocioTipoVigencia.equals(other.negocioTipoVigencia))
			return false;
		if (usuarioCreacionId == null) {
			if (other.usuarioCreacionId != null)
				return false;
		} else if (!usuarioCreacionId.equals(other.usuarioCreacionId))
			return false;
		if (usuarioModificacionId == null) {
			if (other.usuarioModificacionId != null)
				return false;
		} else if (!usuarioModificacionId.equals(other.usuarioModificacionId))
			return false;
		return true;
	}

}