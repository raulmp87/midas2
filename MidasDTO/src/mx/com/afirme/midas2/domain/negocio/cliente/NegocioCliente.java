package mx.com.afirme.midas2.domain.negocio.cliente;

// default package

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.negocio.Negocio;

/**
 * NegocioCliente entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TONEGCLIENTE", schema = "MIDAS")
public class NegocioCliente implements java.io.Serializable, Entidad {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Negocio negocio;
	private BigDecimal idCliente;
	
	private ClienteDTO clienteDTO;

	// Constructors

	/** default constructor */
	public NegocioCliente() {
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "ID_NEG_CTE_SEQ", allocationSize = 1, sequenceName = "IDTONEGCLIENTE_SEQ", schema="MIDAS")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_NEG_CTE_SEQ")
	@Column(name = "IDTONEGCLIENTE", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTONEGOCIO", nullable = false)
	public Negocio getNegocio() {
		return this.negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	
	@Column(name = "IDTCCLIENTE")
	public BigDecimal getIdCliente() {
		return this.idCliente;
	}

	public void setIdCliente(BigDecimal idCliente) {
		this.idCliente = idCliente;
	}

	@Transient
	public ClienteDTO getClienteDTO() {
		return clienteDTO;
	}

	public void setClienteDTO(ClienteDTO clienteDTO) {
		this.clienteDTO = clienteDTO;
	}

	@SuppressWarnings("unchecked")	
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}