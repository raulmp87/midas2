package mx.com.afirme.midas2.domain.negocio.ligaagente;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.Negocio;

@Entity
@Table(name = "CONFIGURACION_LIGA_AGENTE", schema = "MIDAS")
public class ConfiguracionLigaAgente extends MidasAbstracto  implements Entidad {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5812971525340820522L;
	
	public static final Integer ESTATUS_ACTIVO = 1;
	public static final Integer ESTATUS_INACTIVO = 0;

	public static enum Estatus{
		ACTIVO("ACTIVO"),
		INACTIVO ("INACTIVO"),
		SUSPENDIDO("SUSPENDIDO");
		
		private static Map<String, Estatus> lookup = new HashMap<String, Estatus>();
		static {			
			
			for (Estatus e : Estatus.values()) {
				lookup.put(e.getValue(), e);
			}
		}		
		
		public static Estatus get(String value) {
			return lookup.get(value);
		}
		
		private String value;
		
		private Estatus(String value){
			this.value=value;
		}
		
		public String getValue(){
			return value;
		}
	}
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONFIGURACION_LIGA_SEQ")
	@SequenceGenerator(name = "CONFIGURACION_LIGA_SEQ",  schema="MIDAS", sequenceName = "CONFIGURACION_LIGA_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "NOMBRE")
	private String nombre;
	
	@Column(name = "ESTATUS")
	private String estatus;
	
	@Column(name = "COMENTARIO_SUSPENSION")
	private String comentarioSuspension;
	
	@Column(name = "LIGA")
	private String liga;
	
	@Column(name = "DECRIPCION")
	private String descripcion;
	
	
	@Column(name = "TELEFONO")
	private String telefono;
	
	@Column(name = "CORREO")
	private String correo;
	
	@Column(name = "TOKEN")
	private String token;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "AGENTE_ID")
	private Agente agente;
	
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "NEGOCIO_ID")
	private Negocio negocio;
	
	@Column(name = "EMITIR")
	private Boolean emitir;
	

	@Transient
	private String estatusStr;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getComentarioSuspension() {
		return comentarioSuspension;
	}

	public void setComentarioSuspension(String comentarioSuspension) {
		this.comentarioSuspension = comentarioSuspension;
	}

	public String getLiga() {
		return liga;
	}

	public void setLiga(String liga) {
		this.liga = liga;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getEstatusStr() {
		return estatusStr;
	}

	public void setEstatusStr(String estatusStr) {
		this.estatusStr = estatusStr;
	}
	
	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	public Boolean getEmitir() {
		return emitir;
	}

	public void setEmitir(Boolean emitir) {
		this.emitir = emitir;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}

}
