package mx.com.afirme.midas2.domain.negocio.zonacirculacion;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * NegocioMunicipio entity. @author MyEclipse Persistence Tools
 */

@Entity
@Table(name="tonegmunicipio", schema="MIDAS")
public class NegocioMunicipio implements Serializable, Entidad{
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private  NegocioEstado  negocioEstado;
	private MunicipioDTO municipioDTO;
	

	public NegocioMunicipio() {
	    
	}
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="tonegmunicipio_ID_GENERATOR")
	@SequenceGenerator(name="tonegmunicipio_ID_GENERATOR", sequenceName="MIDAS.idToNegMunicipio_seq", allocationSize=1)
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	/*
	 * Relaciona entidad con negocioestado
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTONEGESTADO", referencedColumnName="ID")
	public NegocioEstado getNegocioEstado() {
		return negocioEstado;
	}

	public void setNegocioEstado(NegocioEstado negocioEstado) {
		this.negocioEstado = negocioEstado;
	}
	
	/*
	 * Relaciona entidad con negociomunicipio
	 */
	@OneToOne(fetch = FetchType.LAZY )
	@JoinColumn(name = "idMunicipio", referencedColumnName="MUNICIPALITY_ID")
	public MunicipioDTO getMunicipioDTO() {
		return municipioDTO;
	}

	public void setMunicipioDTO(MunicipioDTO municipioDTO) {
		this.municipioDTO = municipioDTO;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return  this.id;
	}
	@Override
	public String getValue() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}
	@Override
	public <K> K getBusinessKey() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}
}
