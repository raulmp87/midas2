package mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion;

// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.estilovehiculo.NegocioEstiloVehiculo;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tiposervicio.NegocioTipoServicio;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUso;
import mx.com.afirme.midas2.validator.group.CotizacionExpressPaso1Checks;

/**
 * NegocioSeccion entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TONEGSECCION", schema = "MIDAS")
public class NegocioSeccion implements java.io.Serializable, Entidad {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToNegSeccion;
	private SeccionDTO seccionDTO;
	private NegocioTipoPoliza negocioTipoPoliza;
//	private BigDecimal idToAgrupadorTarifa;
//	private BigDecimal idVerAgrupadorTarifa;
	private List<NegocioEstiloVehiculo> negocioEstiloVehiculoList = new ArrayList<NegocioEstiloVehiculo>();
	private List<NegocioPaqueteSeccion> negocioPaqueteSeccionList = new ArrayList<NegocioPaqueteSeccion>();
	private List<NegocioTipoUso> negocioTipoUsoList = new ArrayList<NegocioTipoUso>();
	private List<NegocioTipoServicio> negocioTipoServicioList = new ArrayList<NegocioTipoServicio>();
	private List<NegocioAgrupadorTarifaSeccion> negocioAgrupadorTarifaSeccions = new ArrayList<NegocioAgrupadorTarifaSeccion>();
	private Short consultaNumSerie = 1;
	

	public static final Short PERMITE_CONSULTA_NUM_SERIE = 1;

	// Constructors

	/** default constructor */
	public NegocioSeccion() {
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTONEGSECCION_SEQ")
	@SequenceGenerator(name="IDTONEGSECCION_SEQ", sequenceName="IDTONEGSECCION_SEQ", allocationSize=1,schema="MIDAS")
	@Column(name = "idToNegSeccion", unique = true, nullable = false, precision = 22, scale = 0)
	@NotNull(groups=CotizacionExpressPaso1Checks.class, message="{com.afirme.midas2.requerido}")
	public BigDecimal getIdToNegSeccion() {
		return idToNegSeccion;
	}

	public void setIdToNegSeccion(BigDecimal idToNegSeccion) {
		this.idToNegSeccion = idToNegSeccion;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idToSeccion", unique = true, nullable = false, insertable = true, updatable = true)
	public SeccionDTO getSeccionDTO() {
		return this.seccionDTO;
	}

	public void setSeccionDTO(SeccionDTO seccionDTO) {
		this.seccionDTO = seccionDTO;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTONEGTIPOPOLIZA", nullable = false)
	public NegocioTipoPoliza getNegocioTipoPoliza() {
		return this.negocioTipoPoliza;
	}

	public void setNegocioTipoPoliza(NegocioTipoPoliza negocioTipoPoliza) {
		this.negocioTipoPoliza = negocioTipoPoliza;
	}

//	@Column(name = "idToAgrupadorTarifa", nullable = false, precision = 22, scale = 0)
//	public BigDecimal getIdToAgrupadorTarifa() {
//		return this.idToAgrupadorTarifa;
//	}
//
//	public void setIdToAgrupadorTarifa(BigDecimal idToAgrupadorTarifa) {
//		this.idToAgrupadorTarifa = idToAgrupadorTarifa;
//	}

//	@Column(name = "idVerAgrupadorTarifa", nullable = false, precision = 22, scale = 0)
//	public BigDecimal getIdVerAgrupadorTarifa() {
//		return this.idVerAgrupadorTarifa;
//	}
//
//	public void setIdVerAgrupadorTarifa(BigDecimal idVerAgrupadorTarifa) {
//		this.idVerAgrupadorTarifa = idVerAgrupadorTarifa;
//	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "negocioSeccion")
	public List<NegocioEstiloVehiculo> getNegocioEstiloVehiculoList() {
		return this.negocioEstiloVehiculoList;
	}

	public void setNegocioEstiloVehiculoList(
			List<NegocioEstiloVehiculo> negocioEstiloVehiculoList) {
		this.negocioEstiloVehiculoList = negocioEstiloVehiculoList;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "negocioSeccion")
	public List<NegocioPaqueteSeccion> getNegocioPaqueteSeccionList() {
		return this.negocioPaqueteSeccionList;
	}

	public void setNegocioPaqueteSeccionList(
			List<NegocioPaqueteSeccion> negocioPaqueteSeccionList) {
		this.negocioPaqueteSeccionList = negocioPaqueteSeccionList;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "negocioSeccion")
	public List<NegocioTipoUso> getNegocioTipoUsoList() {
		return this.negocioTipoUsoList;
	}

	public void setNegocioTipoUsoList(List<NegocioTipoUso> negocioTipoUsoList) {
		this.negocioTipoUsoList = negocioTipoUsoList;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "negocioSeccion")
	public List<NegocioTipoServicio> getNegocioTipoServicioList() {
		return this.negocioTipoServicioList;
	}

	public void setNegocioTipoServicioList(
			List<NegocioTipoServicio> negocioTipoServicioList) {
		this.negocioTipoServicioList = negocioTipoServicioList;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "negocioSeccion")
	public List<NegocioAgrupadorTarifaSeccion> getNegocioAgrupadorTarifaSeccions() {
		return this.negocioAgrupadorTarifaSeccions;
	}

	public void setNegocioAgrupadorTarifaSeccions(
			List<NegocioAgrupadorTarifaSeccion> negocioAgrupadorTarifaSeccions) {
		this.negocioAgrupadorTarifaSeccions = negocioAgrupadorTarifaSeccions;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {

		return this.idToNegSeccion;
	}

	@Override
	public String getValue() {

		return null;
	}

	@Override
	public <K> K getBusinessKey() {

		return null;
	}

	@Column(name = "CONSULTANUMSERIE")
	public Short getConsultaNumSerie() {
		return consultaNumSerie;
	}

	public void setConsultaNumSerie(Short consultaNumSerie) {
		this.consultaNumSerie = consultaNumSerie;
	}
}