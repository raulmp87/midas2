package mx.com.afirme.midas2.domain.negocio.condicionespecial;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.negocio.Negocio;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

/**
 * Entidad de relación entre Negocios y Condiciones Especiales
 * 
 * @author Softnet01
 * @version 1.0
 * @created 07-abr.-2014 05:44:31 p. m.
 */
@Entity
@Table( name="TONEGCONDICIONESP", schema = "MIDAS")
public class NegocioCondicionEspecial implements Entidad {

	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TONEGCONDICIONESP_ID_GENERATOR")
	@SequenceGenerator(name="TONEGCONDICIONESP_ID_GENERATOR", schema="MIDAS", sequenceName="TONEGCONDICIONESP_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CONDICIONESPECIAL_ID", referencedColumnName="ID", nullable=false)
	@JoinFetch(JoinFetchType.INNER)
	private CondicionEspecial condicionEspecial;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch(JoinFetchType.INNER)
	@JoinColumn(name="NEGOCIO_ID", nullable=false)
	private Negocio negocio;
	
	@Column(name="APLICAEXTERNO")
	private Integer aplicaExterno;
	
	@Column(name="OBLIGATORIA")
	private Integer obligatoria;
	
	@Column(name="PROVOCAEXCEPCION")
	private Integer produceExcepcion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHACREACION")
	private Date fechaCreacion;
	
	@Column(name="CODIGOUSUARIOCREACION")
	private String codigoUsuarioCreacion;
	
	private static final long serialVersionUID = 1L;
	

	public NegocioCondicionEspecial() {

	}

	public CondicionEspecial getCondicionEspecial() {
		return condicionEspecial;
	}

	public void setCondicionEspecial(CondicionEspecial condicionEspecial) {
		this.condicionEspecial = condicionEspecial;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	public void finalize() throws Throwable {

	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {		
		return id;
	}

	@Override
	public String getValue() {
		return id.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getBusinessKey() {
		return id.toString();
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	public Integer getAplicaExterno() {
		return aplicaExterno;
	}

	public void setAplicaExterno(Integer aplicaExterno) {
		this.aplicaExterno = aplicaExterno;
	}

	public Integer getObligatoria() {
		return obligatoria;
	}

	public void setObligatoria(Integer obligatoria) {
		this.obligatoria = obligatoria;
	}

	public Integer getProduceExcepcion() {
		return produceExcepcion;
	}

	public void setProduceExcepcion(Integer produceExcepcion) {
		this.produceExcepcion = produceExcepcion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((aplicaExterno == null) ? 0 : aplicaExterno.hashCode());
		result = prime
				* result
				+ ((codigoUsuarioCreacion == null) ? 0 : codigoUsuarioCreacion
						.hashCode());
		result = prime
				* result
				+ ((condicionEspecial == null) ? 0 : condicionEspecial
						.hashCode());
		result = prime * result
				+ ((fechaCreacion == null) ? 0 : fechaCreacion.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((negocio == null) ? 0 : negocio.hashCode());
		result = prime * result
				+ ((obligatoria == null) ? 0 : obligatoria.hashCode());
		result = prime
				* result
				+ ((produceExcepcion == null) ? 0 : produceExcepcion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NegocioCondicionEspecial other = (NegocioCondicionEspecial) obj;
		if (aplicaExterno == null) {
			if (other.aplicaExterno != null)
				return false;
		} else if (!aplicaExterno.equals(other.aplicaExterno))
			return false;
		if (codigoUsuarioCreacion == null) {
			if (other.codigoUsuarioCreacion != null)
				return false;
		} else if (!codigoUsuarioCreacion.equals(other.codigoUsuarioCreacion))
			return false;
		if (condicionEspecial == null) {
			if (other.condicionEspecial != null)
				return false;
		} else if (!condicionEspecial.equals(other.condicionEspecial))
			return false;
		if (fechaCreacion == null) {
			if (other.fechaCreacion != null)
				return false;
		} else if (!fechaCreacion.equals(other.fechaCreacion))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (negocio == null) {
			if (other.negocio != null)
				return false;
		} else if (!negocio.equals(other.negocio))
			return false;
		if (obligatoria == null) {
			if (other.obligatoria != null)
				return false;
		} else if (!obligatoria.equals(other.obligatoria))
			return false;
		if (produceExcepcion == null) {
			if (other.produceExcepcion != null)
				return false;
		} else if (!produceExcepcion.equals(other.produceExcepcion))
			return false;
		return true;
	}

}