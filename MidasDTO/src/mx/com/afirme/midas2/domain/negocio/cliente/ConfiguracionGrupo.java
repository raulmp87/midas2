package mx.com.afirme.midas2.domain.negocio.cliente;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * ConfiguracionGrupo entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOCONFIGGRUPO", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = "CODIGO"))
public class ConfiguracionGrupo implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private ConfiguracionSeccion configuracionSeccion;
	private String codigo;
	private String descripcion;
	private Short valorDefault;
	private Short tipoPersona;
	private List<ConfiguracionCampo> configuracionCampos = new ArrayList<ConfiguracionCampo>() ;
	private List<ConfiguracionGrupoPorNegocio> configuracionGrupoPorNegocios = new ArrayList<ConfiguracionGrupoPorNegocio>();

	// Constructors

	/** default constructor */
	public ConfiguracionGrupo() {
	}

	/** minimal constructor */
	public ConfiguracionGrupo(Long id,
			ConfiguracionSeccion configuracionSeccion, String codigo,
			String descripcion, Short tipoPersona) {
		this.id = id;
		this.configuracionSeccion = configuracionSeccion;
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.tipoPersona = tipoPersona;
	}

	/** full constructor */
	public ConfiguracionGrupo(Long id,
			ConfiguracionSeccion configuracionSeccion, String codigo,
			String descripcion, Short valorDefault, Short tipoPersona,
			List<ConfiguracionCampo> configuracionCampos,
			List<ConfiguracionGrupoPorNegocio> configuracionGrupoPorNegocios) {
		this.id = id;
		this.configuracionSeccion = configuracionSeccion;
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.valorDefault = valorDefault;
		this.tipoPersona = tipoPersona;
		this.configuracionCampos = configuracionCampos;
		this.configuracionGrupoPorNegocios = configuracionGrupoPorNegocios;
	}

	public boolean esObligatorio(){
		return getValorDefault().equals((short)1);
	}
	
	// Property accessors
	@Id
	@SequenceGenerator(name = "IDCONFGPO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "TOCONFIGGRUPO_IDTOCONFIGGR_SEQ", schema="MIDAS")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDCONFGPO_SEQ_GENERADOR")
	@Column(name = "IDTOCONFIGGRUPO", unique = true, nullable = false, precision = 8, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOCONFIGSECCION", nullable = false)
	public ConfiguracionSeccion getConfiguracionSeccion() {
		return this.configuracionSeccion;
	}

	public void setConfiguracionSeccion(
			ConfiguracionSeccion configuracionSeccion) {
		this.configuracionSeccion = configuracionSeccion;
	}

	@Column(name = "CODIGO", unique = true, nullable = false, length = 50)
	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Column(name = "DESCRIPCION", nullable = false, length = 300)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "VALORDEFAULT", precision = 4, scale = 0)
	public Short getValorDefault() {
		return this.valorDefault;
	}

	public void setValorDefault(Short valorDefault) {
		this.valorDefault = valorDefault;
	}

	@Column(name = "TIPOPERSONA", nullable = false, precision = 2, scale = 0)
	public Short getTipoPersona() {
		return this.tipoPersona;
	}

	public void setTipoPersona(Short tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "configuracionGrupo")
	public List<ConfiguracionCampo> getConfiguracionCampos() {
		return this.configuracionCampos;
	}

	public void setConfiguracionCampos(List<ConfiguracionCampo> configuracionCampos) {
		this.configuracionCampos = configuracionCampos;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "configuracionGrupo")
	public List<ConfiguracionGrupoPorNegocio> getConfiguracionGrupoPorNegocios() {
		return this.configuracionGrupoPorNegocios;
	}

	public void setConfiguracionGrupoPorNegocios(List<ConfiguracionGrupoPorNegocio> configuracionGrupoPorNegocios) {
		this.configuracionGrupoPorNegocios = configuracionGrupoPorNegocios;
	}

}