package mx.com.afirme.midas2.domain.negocio.cliente.grupo;
// default package

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.cliente.GrupoCliente;
import mx.com.afirme.midas2.domain.negocio.Negocio;

/**
 * NegocioGrupoCliente entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TONEGGRUPOCLIENTES", schema = "MIDAS")
public class NegocioGrupoCliente implements java.io.Serializable, Entidad {
	private static final long serialVersionUID = 1L;
	
	private Long idToNegocioGrupoCliente;
	private Negocio negocio;
	private GrupoCliente grupoClientes;

	// Constructors

	/** default constructor */
	public NegocioGrupoCliente() {
	}

	/** full constructor */
	public NegocioGrupoCliente(Long idToNegocioGrupoCliente,
			Negocio negocio, GrupoCliente grupoClientes) {
		this.idToNegocioGrupoCliente = idToNegocioGrupoCliente;
		this.negocio = negocio;
		this.grupoClientes = grupoClientes;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "ID_NEG_GPO_CTE_SEQ", allocationSize = 1, sequenceName = "idToNegGrupoClientes_seq", schema="MIDAS")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_NEG_GPO_CTE_SEQ")
	@Column(name = "IDTONEGGRUPOCLIENTES", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getIdToNegocioGrupoCliente() {
		return this.idToNegocioGrupoCliente;
	}

	public void setIdToNegocioGrupoCliente(Long idToNegocioGrupoCliente) {
		this.idToNegocioGrupoCliente = idToNegocioGrupoCliente;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTONEGOCIO", nullable = false)
	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTCGRUPOCLIENTES", nullable = false)
	public GrupoCliente getGrupoClientes() {
		return this.grupoClientes;
	}

	public void setGrupoClientes(GrupoCliente grupoClientes) {
		this.grupoClientes = grupoClientes;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.idToNegocioGrupoCliente;
	}

	@Override
	public String getValue() {
		return this.grupoClientes.getDescripcion();
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.idToNegocioGrupoCliente;
	}

}