package mx.com.afirme.midas2.domain.negocio.derechos;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.catalogos.EnumBase;
import mx.com.afirme.midas2.domain.MidasAbstracto;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

@Entity
@Table(name="TONEGCONFIGURACIONDERECHO", schema="MIDAS")
public class NegocioConfiguracionDerecho extends MidasAbstracto implements Entidad{

	private static final long serialVersionUID = 937884296799728977L;
	
	@Id
	@Column(name="IDTOCONFIGDERECHO", nullable=false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="NEGCONFIGURACIONDERECHO_GENERATOR")
	@SequenceGenerator(name="NEGCONFIGURACIONDERECHO_GENERATOR", sequenceName="TONEGCONFIGURACIONDERECHO_SEQ", schema="MIDAS", allocationSize=1)
	private BigDecimal idToNegConfigDerecho;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch(JoinFetchType.INNER)
	@JoinColumn(name="IDTONEGOCIO", nullable=false)
	private Negocio negocio;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch(JoinFetchType.INNER)
	@JoinColumn(name="IDTONEGTIPOPOLIZA", nullable=false)
	private NegocioTipoPoliza tipoPoliza;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch(JoinFetchType.INNER)
	@JoinColumn(name="IDTONEGSECCION", nullable=false)
	private NegocioSeccion seccion;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch(JoinFetchType.INNER)
	@JoinColumn(name="IDTONEGPAQUETESECCION", nullable=false)
	private NegocioPaqueteSeccion paquete;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch(JoinFetchType.INNER)
	@JoinColumn(name="IDTCMONEDA", nullable=false)
	private MonedaDTO moneda;
	
	@Column(name="IDTIPODERECHO")
	private String tipoDerecho;
	
	@Column(name="IDTONEGDERECHO")
	private Long idToNegDerecho;
	
	@Column(name="IMPORTEDERECHO")
	private Double importeDerecho;
	
	@Column(name="IMPORTEDEFAULT")
	private boolean importeDefault;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch(JoinFetchType.OUTER)
	@JoinColumn(name="IDTCTIPOUSOVEHICULO", referencedColumnName = "IDTCTIPOUSOVEHICULO", nullable=true)
	private TipoUsoVehiculoDTO tipoUsoVehiculo;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch(JoinFetchType.OUTER)
	@JoinColumn(name="STATE_ID", referencedColumnName="STATE_ID", nullable=true)
	private EstadoDTO estado;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch(JoinFetchType.OUTER)
	@JoinColumn(name="CITY_ID", referencedColumnName="CITY_ID", nullable=true)
	private CiudadDTO municipio;
	
	@Transient
	private Integer totalCount;
	@Transient
	private Integer posStart;
	@Transient
	private Integer count;
	@Transient
	private String orderByAttribute;
	
	public  enum TipoDerecho implements EnumBase<String>{
		POLIZA,
		ENDOSO,
		INCISO;

		@Override
		public String getValue() {
			return this.toString();
		}

		@Override
		public String getLabel() {
			return this.toString();
		}
	}
	
	

	public BigDecimal getIdToNegConfigDerecho() {
		return idToNegConfigDerecho;
	}

	public void setIdToNegConfigDerecho(BigDecimal idToNegConfigDerecho) {
		this.idToNegConfigDerecho = idToNegConfigDerecho;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	public NegocioTipoPoliza getTipoPoliza() {
		return tipoPoliza;
	}

	public void setTipoPoliza(NegocioTipoPoliza tipoPoliza) {
		this.tipoPoliza = tipoPoliza;
	}

	public NegocioSeccion getSeccion() {
		return seccion;
	}

	public void setSeccion(NegocioSeccion seccion) {
		this.seccion = seccion;
	}

	public NegocioPaqueteSeccion getPaquete() {
		return paquete;
	}

	public void setPaquete(NegocioPaqueteSeccion paquete) {
		this.paquete = paquete;
	}

	public MonedaDTO getMoneda() {
		return moneda;
	}

	public void setMoneda(MonedaDTO moneda) {
		this.moneda = moneda;
	}

	public String getTipoDerecho() {
		return tipoDerecho;
	}

	public void setTipoDerecho(String tipoDerecho) {
		this.tipoDerecho = tipoDerecho;
	}

	public Double getImporteDerecho() {
		return importeDerecho;
	}

	public void setImporteDerecho(Double importeDerecho) {
		this.importeDerecho = importeDerecho;
	}

	public boolean isImporteDefault() {
		return importeDefault;
	}

	public void setImporteDefault(boolean importeDefault) {
		this.importeDefault = importeDefault;
	}

	public TipoUsoVehiculoDTO getTipoUsoVehiculo() {
		return tipoUsoVehiculo;
	}

	public void setTipoUsoVehiculo(TipoUsoVehiculoDTO tipoUsoVehiculo) {
		this.tipoUsoVehiculo = tipoUsoVehiculo;
	}

	public EstadoDTO getEstado() {
		return estado;
	}

	public void setEstado(EstadoDTO estado) {
		this.estado = estado;
	}

	public CiudadDTO getMunicipio() {
		return municipio;
	}

	public void setMunicipio(CiudadDTO municipio) {
		this.municipio = municipio;
	}

	
	
	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return idToNegConfigDerecho;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getBusinessKey() {
		return idToNegConfigDerecho;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getPosStart() {
		return posStart;
	}

	public void setPosStart(Integer posStart) {
		this.posStart = posStart;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getOrderByAttribute() {
		return orderByAttribute;
	}

	public void setOrderByAttribute(String orderByAttribute) {
		this.orderByAttribute = orderByAttribute;
	}

	public Long getIdToNegDerecho() {
		return idToNegDerecho;
	}

	public void setIdToNegDerecho(Long idToNegDerecho) {
		this.idToNegDerecho = idToNegDerecho;
	}
	

}
