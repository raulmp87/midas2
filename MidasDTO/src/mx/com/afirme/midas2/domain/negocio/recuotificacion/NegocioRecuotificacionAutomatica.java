package mx.com.afirme.midas2.domain.negocio.recuotificacion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.catalogos.EnumBase;
import mx.com.afirme.midas2.domain.MidasAbstracto;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

@Entity
@Table(name="TONEGRECAUT", schema="MIDAS")
public class NegocioRecuotificacionAutomatica extends MidasAbstracto implements Entidad {

	private static final long serialVersionUID = -7982264877625903930L;
	
	public enum Status{INACTIVO, ACTIVO}; 	
	
	public enum TipoVigencia implements EnumBase<String>{
		SEMESTRAL("SEMESTRAL"), 
		ANUAL("ANUAL");

		private String codigo;

		TipoVigencia(String codigo){
			this.codigo = codigo;
		}			

		@Override
		public String toString(){
			return this.name();			
		}

		@Override
		public String getValue() {
			return codigo;
		}

		@Override
		public String getLabel() {
			return this.toString();
		}

		
	}
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TONEGRECAUT_ID_GENERATOR")
	@SequenceGenerator(name="TONEGRECAUT_ID_GENERATOR", schema="MIDAS", sequenceName="TONEGRECAUT_ID_SEQ",allocationSize=1)	
	@Column(name="ID")
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinFetch(JoinFetchType.INNER)
	@JoinColumn(name="TONEGRECUOTIFICA_ID", nullable=false)
	private NegocioRecuotificacion negRecuotificacion;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "automatica")	
	private List<NegocioRecuotificacionAutProgPago> programas = new ArrayList<NegocioRecuotificacionAutProgPago>();
	
	@Column(name="version")
	private Integer version;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name="claveestatus")
	private Status status;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipoVigencia")
	private TipoVigencia tipoVigencia;
		
	@Transient
	private String nombreUsuario;
	
	public NegocioRecuotificacionAutomatica(String codigoUsuarioCreacion, Date fechaCreacion){
		this();
//		NegocioRecuotificacionAutProgPago programa = new NegocioRecuotificacionAutProgPago(codigoUsuarioCreacion);
//		programa.setAutomatica(this);
//		programa.setNumProgPago(1);
//		programa.setPctDistribucionPrima(100d);
//		programa.setNumProgPago(1);
//		programa.setCodigoUsuarioCreacion(codigoUsuarioCreacion);
//		programa.setFechaCreacion(fechaCreacion);		
//		this.agregarPrograma(programa);
//		this.setCodigoUsuarioCreacion(codigoUsuarioCreacion);
//		this.setFechaCreacion(new Date());
//		this.tipoVigencia  = TipoVigencia.ANUAL;
//		this.status = Status.ACTIVO;
	}
	
	public NegocioRecuotificacionAutomatica(String codigoUsuarioCreacion){
		this(codigoUsuarioCreacion, new Date());
	}
	
	public NegocioRecuotificacionAutomatica() {
		super();		
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {	
		return this.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public NegocioRecuotificacion getNegRecuotificacion() {
		return negRecuotificacion;
	}

	public void setNegRecuotificacion(NegocioRecuotificacion negRecuotificacion) {
		this.negRecuotificacion = negRecuotificacion;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public List<NegocioRecuotificacionAutProgPago> getProgramas() {
		return programas;
	}

	public void setProgramas(List<NegocioRecuotificacionAutProgPago> programas) {
		this.programas = programas;
	}
	
	public void agregarPrograma(NegocioRecuotificacionAutProgPago obj){			
		this.programas.add(obj);		
	}

	public TipoVigencia getTipoVigencia() {
		return tipoVigencia;
	}

	public void setTipoVigencia(TipoVigencia tipoVigencia) {
		this.tipoVigencia = tipoVigencia;
	}
	
	
}
