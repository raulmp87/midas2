package mx.com.afirme.midas2.domain.negocio.bonocomision;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.validator.group.NegocioBonoEspecial;
import mx.com.afirme.midas2.validator.group.NegocioBonoImporte;
import mx.com.afirme.midas2.validator.group.NegocioBonoPorcentaje;
import mx.com.afirme.midas2.validator.group.NegocioSesionDerechos;
import mx.com.afirme.midas2.validator.group.NegocioSobreComision;
import mx.com.afirme.midas2.validator.group.NegocioUDI;

/**
 * The persistent class for the TONEGBONOCOMISION database table.
 * 
 */
@Entity
@Table(name = "TONEGBONOCOMISION", schema="MIDAS")
public class NegocioBonoComision implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TONEGBONOCOMISION_ID_GENERATOR", sequenceName = "MIDAS.IDTONEGBONOCOMISION_SEQ", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TONEGBONOCOMISION_ID_GENERATOR")
	@Column(unique = true, nullable = false)
	private Long id;

	@Column(name = "CLAVEBONO")
	private Boolean claveBono;

	@Column(name = "CLAVEDERECHOS")
	private Boolean claveDerechos;

	@Column(name = "CLAVEINCLUYEIVASOBRECOMISION")
	private Boolean claveIncluyeIVASobreomision;

	@Column(name = "CLAVEPORCENTAJEIMPORTEBONO")
	private Boolean clavePorcentajeImporteBono;

	@Column(name = "CLAVEPORCENTAJEIMPORTEDERECHOS")
	private Boolean clavePorcentajeImporteDerechos;

	@Column(name = "CLAVEPORCENTAJEIMPORTEUDI")
	private Boolean clavePorcentajeImporteUDI;

	@Column(name = "CLAVESOBRECOMISION")
	private Boolean claveSobreComision;

	@Column(name = "CLAVEUDI")
	private Boolean claveUDI;

	@Size(min=1, max=2000,groups=NegocioBonoEspecial.class)
	@NotNull(groups=NegocioBonoEspecial.class)
	@Column(name = "COMENTARIOSBONO",length = 2000)
	private String comentariosBono;

	@Size(min=1, max=2000,groups=NegocioSesionDerechos.class)
	@NotNull(groups=NegocioSesionDerechos.class)
	@Column(name = "COMENTARIOSDERECHOS",length = 2000)
	private String comentariosDerechos;

	@Size(min=1, max=2000,groups=NegocioSobreComision.class)
	@NotNull(groups=NegocioSobreComision.class)
	@Column(name = "COMENTARIOSSOBRECOMISION",length = 2000)
	private String comentariosSobreComision;

	@Size(min=1, max=2000,groups=NegocioUDI.class)
	@NotNull(groups=NegocioUDI.class)
	@Column(name = "COMENTARIOSUDI",length = 2000)
	private String comentariosUDI;

	@Digits(integer=16, fraction=2,groups=NegocioBonoEspecial.class)
	@NotNull(groups=NegocioBonoImporte.class)	
	@Column(name = "IMPORTEBONO")
	private BigDecimal importeBono;

	@Digits(integer=16, fraction=2,groups=NegocioSesionDerechos.class)	
	@Column(name = "IMPORTEDERECHOS")
	private BigDecimal importeDerechos;

	@Digits(integer=16, fraction=2, groups=NegocioUDI.class)	
	@Column(name = "IMPORTEUDI")
	private BigDecimal importeUDI;

	@DecimalMax(value="100.00",groups=NegocioBonoEspecial.class)
	@NotNull(groups=NegocioBonoEspecial.class)		
	@Column(name = "PCTAGENTEBONO")
	private BigDecimal pctAgenteBono;

	@DecimalMax(value="100.00",groups=NegocioSesionDerechos.class)
	@NotNull(groups=NegocioSesionDerechos.class)		
	@Column(name = "PCTAGENTEDERECHOS")
	private BigDecimal pctAgenteDerechos;

	@DecimalMax(value="100.00",groups=NegocioSobreComision.class)
	@NotNull(groups=NegocioSobreComision.class)			
	@Column(name = "PCTAGENTESOBRECOMISION")
	private BigDecimal pctAgenteSobreComision;

	@DecimalMax(value="100.00",groups=NegocioUDI.class)
	@NotNull(groups=NegocioUDI.class)		
	@Column(name = "PCTAGENTEUDI")
	private BigDecimal pctAgenteUDI;

	@DecimalMax(value="100.00",groups=NegocioBonoEspecial.class)
	@NotNull(groups=NegocioBonoPorcentaje.class)		
	@Column(name = "PCTBONO")
	private BigDecimal pctBono;

	@DecimalMax(value="100.00",groups=NegocioSesionDerechos.class)		
	@Column(name = "PCTDERECHOS")
	private BigDecimal pctDerechos;

	@DecimalMax(value="100.00",groups=NegocioBonoEspecial.class)
	@NotNull(groups=NegocioBonoEspecial.class)		
	@Column(name = "PCTPROMOTORBONO")
	private BigDecimal pctPromotorBono;

	@DecimalMax(value="100.00",groups=NegocioSesionDerechos.class)
	@NotNull(groups=NegocioSesionDerechos.class)		
	@Column(name = "PCTPROMOTORDERECHOS")
	private BigDecimal pctPromotorDerechos;

	@DecimalMax(value="100.00",groups=NegocioSobreComision.class)
	@NotNull(groups=NegocioSobreComision.class)		
	@Column(name = "PCTPROMOTORSOBRECOMISION")
	private BigDecimal pctPromotorSobreComision;

	@DecimalMax(value="100.00",groups=NegocioUDI.class)
	@NotNull(groups=NegocioUDI.class)	
	@Column(name = "PCTPROMOTORUDI")
	private BigDecimal pctPromotorUDI;

	@DecimalMax(value="100.00",groups=NegocioSobreComision.class)
	@NotNull(groups=NegocioSobreComision.class)		
	@Column(name = "PCTSOBRECOMISION")
	private BigDecimal pctSobreComision;

	@DecimalMax(value="100.00",groups=NegocioUDI.class)	
	@Column(name = "PCTUDI")
	private BigDecimal pctUDI;

	@DecimalMax(value="100.00",groups=NegocioSesionDerechos.class)		
	@Column(name = "PCTCOMISIONAGTE")
	private BigDecimal pctComisionAgte;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "NEGOCIO_ID", nullable = false, referencedColumnName="IDTONEGOCIO")
	private Negocio negocio;

	public NegocioBonoComision() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getClaveBono() {
		return this.claveBono;
	}

	public void setClaveBono(Boolean claveBono) {
		this.claveBono = claveBono;
	}

	public Boolean getClaveDerechos() {
		return this.claveDerechos;
	}

	public void setClaveDerechos(Boolean claveDerechos) {
		this.claveDerechos = claveDerechos;
	}

	public Boolean getClaveIncluyeIVASobreomision() {
		return this.claveIncluyeIVASobreomision;
	}

	public void setClaveIncluyeIVASobreomision(
			Boolean claveIncluyeIVASobreomision) {
		this.claveIncluyeIVASobreomision = claveIncluyeIVASobreomision;
	}

	public Boolean getClavePorcentajeImporteBono() {
		return this.clavePorcentajeImporteBono;
	}

	public void setClavePorcentajeImporteBono(Boolean clavePorcentajeImporteBono) {
		this.clavePorcentajeImporteBono = clavePorcentajeImporteBono;
	}

	public Boolean getClavePorcentajeImporteDerechos() {
		return this.clavePorcentajeImporteDerechos;
	}

	public void setClavePorcentajeImporteDerechos(
			Boolean clavePorcentajeImporteDerechos) {
		this.clavePorcentajeImporteDerechos = clavePorcentajeImporteDerechos;
	}

	public Boolean getClavePorcentajeImporteUDI() {
		return this.clavePorcentajeImporteUDI;
	}

	public void setClavePorcentajeImporteUDI(Boolean clavePorcentajeImporteUDI) {
		this.clavePorcentajeImporteUDI = clavePorcentajeImporteUDI;
	}

	public Boolean getClaveSobreComision() {
		return this.claveSobreComision;
	}

	public void setClaveSobreComision(Boolean claveSobreComision) {
		this.claveSobreComision = claveSobreComision;
	}

	public Boolean getClaveUDI() {
		return this.claveUDI;
	}

	public void setClaveUDI(Boolean claveUDI) {
		this.claveUDI = claveUDI;
	}

	public String getComentariosBono() {
		return this.comentariosBono;
	}

	public void setComentariosBono(String comentariosBono) {
		this.comentariosBono = comentariosBono;
	}

	public String getComentariosDerechos() {
		return this.comentariosDerechos;
	}

	public void setComentariosDerechos(String comentariosDerechos) {
		this.comentariosDerechos = comentariosDerechos;
	}

	public String getComentariosSobreComision() {
		return this.comentariosSobreComision;
	}

	public void setComentariosSobreComision(String comentariosSobreComision) {
		this.comentariosSobreComision = comentariosSobreComision;
	}

	public String getComentariosUDI() {
		return this.comentariosUDI;
	}

	public void setComentariosUDI(String comentariosUDI) {
		this.comentariosUDI = comentariosUDI;
	}

	public BigDecimal getImporteBono() {
		return this.importeBono;
	}

	public void setImporteBono(BigDecimal importeBono) {
		this.importeBono = importeBono;
	}

	public BigDecimal getImporteDerechos() {
		return this.importeDerechos;
	}

	public void setImporteDerechos(BigDecimal importeDerechos) {
		this.importeDerechos = importeDerechos;
	}

	public BigDecimal getImporteUDI() {
		return this.importeUDI;
	}

	public void setImporteUDI(BigDecimal importeUDI) {
		this.importeUDI = importeUDI;
	}

	public BigDecimal getPctAgenteBono() {
		return this.pctAgenteBono;
	}

	public void setPctAgenteBono(BigDecimal pctAgenteBono) {
		this.pctAgenteBono = pctAgenteBono;
	}

	public BigDecimal getPctAgenteDerechos() {
		return this.pctAgenteDerechos;
	}

	public void setPctAgenteDerechos(BigDecimal pctAgenteDerechos) {
		this.pctAgenteDerechos = pctAgenteDerechos;
	}

	public BigDecimal getPctAgenteSobreComision() {
		return this.pctAgenteSobreComision;
	}

	public void setPctAgenteSobreComision(BigDecimal pctAgenteSobreComision) {
		this.pctAgenteSobreComision = pctAgenteSobreComision;
	}

	public BigDecimal getPctAgenteUDI() {
		return this.pctAgenteUDI;
	}

	public void setPctAgenteUDI(BigDecimal pctAgenteUDI) {
		this.pctAgenteUDI = pctAgenteUDI;
	}

	public BigDecimal getPctBono() {
		return this.pctBono;
	}

	public void setPctBono(BigDecimal pctBono) {
		this.pctBono = pctBono;
	}

	public BigDecimal getPctDerechos() {
		return this.pctDerechos;
	}

	public void setPctDerechos(BigDecimal pctDerechos) {
		this.pctDerechos = pctDerechos;
	}

	public BigDecimal getPctPromotorBono() {
		return this.pctPromotorBono;
	}

	public void setPctPromotorBono(BigDecimal pctPromotorBono) {
		this.pctPromotorBono = pctPromotorBono;
	}

	public BigDecimal getPctPromotorDerechos() {
		return this.pctPromotorDerechos;
	}

	public void setPctPromotorDerechos(BigDecimal pctPromotorDerechos) {
		this.pctPromotorDerechos = pctPromotorDerechos;
	}

	public BigDecimal getPctPromotorSobreComision() {
		return this.pctPromotorSobreComision;
	}

	public void setPctPromotorSobreComision(BigDecimal pctPromotorSobreComision) {
		this.pctPromotorSobreComision = pctPromotorSobreComision;
	}

	public BigDecimal getPctPromotorUDI() {
		return this.pctPromotorUDI;
	}

	public void setPctPromotorUDI(BigDecimal pctPromotorUDI) {
		this.pctPromotorUDI = pctPromotorUDI;
	}

	public BigDecimal getPctSobreComision() {
		return this.pctSobreComision;
	}

	public void setPctSobreComision(BigDecimal pctSobreComision) {
		this.pctSobreComision = pctSobreComision;
	}

	public BigDecimal getPctUDI() {
		return this.pctUDI;
	}

	public void setPctUDI(BigDecimal pctUDI) {
		this.pctUDI = pctUDI;
	}

	public Negocio getNegocio() {
		return this.negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	public BigDecimal getPctComisionAgte() {
		return pctComisionAgte;
	}

	public void setPctComisionAgte(BigDecimal pctComisionAgte) {
		this.pctComisionAgte = pctComisionAgte;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		return this.id.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.id;
	}

}