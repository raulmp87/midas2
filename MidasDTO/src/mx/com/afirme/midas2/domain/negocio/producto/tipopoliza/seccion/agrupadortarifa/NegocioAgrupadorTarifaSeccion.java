package mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.agrupadortarifa;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;

/**
 * NegocioAgrupadorTarifaSeccion entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TONEGAGRUPTARIFASECCION", schema = "MIDAS")
public class NegocioAgrupadorTarifaSeccion implements java.io.Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields

	private Long idToNegAgrupTarifaSeccion;
	private NegocioSeccion negocioSeccion;
	private BigDecimal idMoneda;
	private BigDecimal idToAgrupadorTarifa;
	private BigDecimal idVerAgrupadorTarifa;
	private String codigoUsuarioModificacion;

	// Constructors

	/** default constructor */
	public NegocioAgrupadorTarifaSeccion() {
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "NEG_AGR_TAR_SEC_SEQ", allocationSize = 1, sequenceName = "IDTONEGAGRUPTARIFASECCION_SEQ", schema="MIDAS")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NEG_AGR_TAR_SEC_SEQ")
	@Column(name = "IDTONEGAGRUPTARIFASECCION", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getIdToNegAgrupTarifaSeccion() {
		return this.idToNegAgrupTarifaSeccion;
	}

	public void setIdToNegAgrupTarifaSeccion(
			Long idToNegAgrupTarifaSeccion) {
		this.idToNegAgrupTarifaSeccion = idToNegAgrupTarifaSeccion;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTONEGSECCION", nullable = false)
	public NegocioSeccion getNegocioSeccion() {
		return this.negocioSeccion;
	}

	public void setNegocioSeccion(NegocioSeccion negocioSeccion) {
		this.negocioSeccion = negocioSeccion;
	}

	@Column(name = "IDMONEDA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdMoneda() {
		return this.idMoneda;
	}

	public void setIdMoneda(BigDecimal idMoneda) {
		this.idMoneda = idMoneda;
	}

	@Column(name = "IDTOAGRUPADORTARIFA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToAgrupadorTarifa() {
		return this.idToAgrupadorTarifa;
	}

	public void setIdToAgrupadorTarifa(BigDecimal idToAgrupadorTarifa) {
		this.idToAgrupadorTarifa = idToAgrupadorTarifa;
	}

	@Column(name = "IDVERAGRUPADORTARIFA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdVerAgrupadorTarifa() {
		return this.idVerAgrupadorTarifa;
	}

	public void setIdVerAgrupadorTarifa(BigDecimal idVerAgrupadorTarifa) {
		this.idVerAgrupadorTarifa = idVerAgrupadorTarifa;
	}


	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}

	@Column(name="CODIGOUSUARIOMODIF")
	public String getCodigoUsuarioModificacion() {
		return codigoUsuarioModificacion;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.idToNegAgrupTarifaSeccion;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}


}