package mx.com.afirme.midas2.domain.negocio.renovacion;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * NegocioRenovacionCobertura entity. @author Lizandro Perez
 */
@Entity
@Table(name = "TONEGRENCOBERTURA", schema = "MIDAS")
public class NegocioRenovacionCobertura implements java.io.Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9128088047461771813L;

	private NegocioRenovacionCoberturaId id;
	
	private Short aplicaDefault;
	
	private String nombreComercialCobertura;
	
	public NegocioRenovacionCobertura(){
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public NegocioRenovacionCoberturaId getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setId(NegocioRenovacionCoberturaId id) {
		this.id = id;
	}

    @EmbeddedId
    @AttributeOverrides( {
        @AttributeOverride(name="idToNegocio", column=@Column(name="IDTONEGOCIO", nullable=false, precision = 22, scale = 0) ), 
        @AttributeOverride(name="tipoRiesgo", column=@Column(name="TIPORIESGO", nullable=false,  precision = 4, scale = 0) ),
        @AttributeOverride(name="idToSeccion", column=@Column(name="IDTOSECCION", nullable=false, precision = 22, scale = 0) ),
        @AttributeOverride(name="idToCobertura", column=@Column(name="IDTOCOBERTURA", nullable=false, precision = 22, scale = 0) )} )
	public NegocioRenovacionCoberturaId getId() {
		return id;
	}
	
	public void setAplicaDefault(Short aplicaDefault) {
		this.aplicaDefault = aplicaDefault;
	}

	@Column(name = "APLICADEFAULT", nullable = false, precision = 4, scale = 0)
	public Short getAplicaDefault() {
		return aplicaDefault;
	}

	public void setNombreComercialCobertura(String nombreComercialCobertura) {
		this.nombreComercialCobertura = nombreComercialCobertura;
	}

	@Transient
	public String getNombreComercialCobertura() {
		return nombreComercialCobertura;
	}

}
