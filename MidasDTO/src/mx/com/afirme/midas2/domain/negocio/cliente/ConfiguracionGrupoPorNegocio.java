package mx.com.afirme.midas2.domain.negocio.cliente;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * ConfiguracionGrupoPorNegocio entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TRCONFIGGRUPONEG", schema = "MIDAS")
public class ConfiguracionGrupoPorNegocio implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private ConfiguracionSeccionPorNegocio configuracionSeccionPorNegocio;
	private ConfiguracionGrupo configuracionGrupo;
	private Long idNegocio;
	private Short obligatoriedad;

	// Constructors

	/** default constructor */
	public ConfiguracionGrupoPorNegocio() {
	}

	/** full constructor */
	public ConfiguracionGrupoPorNegocio(Long id,
			ConfiguracionSeccionPorNegocio configuracionSeccionPorNegocio,
			ConfiguracionGrupo configuracionGrupo, Long idNegocio,
			Short obligatoriedad) {
		this.id = id;
		this.configuracionSeccionPorNegocio = configuracionSeccionPorNegocio;
		this.configuracionGrupo = configuracionGrupo;
		this.idNegocio = idNegocio;
		this.obligatoriedad = obligatoriedad;
	}

	public boolean esObligatorio(){
		return getObligatoriedad().equals((short)1);
	}
	
	// Property accessors
	@Id
	@SequenceGenerator(name = "IDCONFGPOSEC_SEQ_GENERADOR", allocationSize = 1, sequenceName = "TRCONFIGGRUPO_IDTRCONFIGCR_SEQ", schema="MIDAS")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDCONFGPOSEC_SEQ_GENERADOR")
	@Column(name = "IDTRCONFIGCRUPONEG", unique = true, nullable = false, precision = 8, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTRCONFIGSECCIONNEG", nullable = false)
	public ConfiguracionSeccionPorNegocio getConfiguracionSeccionPorNegocio() {
		return this.configuracionSeccionPorNegocio;
	}

	public void setConfiguracionSeccionPorNegocio(ConfiguracionSeccionPorNegocio configuracionSeccionPorNegocio) {
		this.configuracionSeccionPorNegocio = configuracionSeccionPorNegocio;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTOCONFIGGRUPO", nullable = false)
	public ConfiguracionGrupo getConfiguracionGrupo() {
		return this.configuracionGrupo;
	}

	public void setConfiguracionGrupo(ConfiguracionGrupo configuracionGrupo) {
		this.configuracionGrupo = configuracionGrupo;
	}

	@Column(name = "IDNEGOCIO", nullable = false, precision = 8, scale = 0)
	public Long getIdNegocio() {
		return this.idNegocio;
	}

	public void setIdNegocio(Long idNegocio) {
		this.idNegocio = idNegocio;
	}

	@Column(name = "OBLIGATORIEDAD", nullable = false, precision = 2, scale = 0)
	public Short getObligatoriedad() {
		return this.obligatoriedad;
	}

	public void setObligatoriedad(Short obligatoriedad) {
		this.obligatoriedad = obligatoriedad;
	}

}