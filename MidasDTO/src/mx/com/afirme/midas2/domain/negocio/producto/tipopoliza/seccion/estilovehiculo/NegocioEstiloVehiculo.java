package mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.estilovehiculo;

// default package

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;

/**
 * NegocioEstiloVehiculo entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TONEGESTILOVEHICULO", schema = "MIDAS")
public class NegocioEstiloVehiculo implements java.io.Serializable,Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields

	private Long idToNegEstiloVehiculo;
	private NegocioSeccion negocioSeccion;
	private EstiloVehiculoDTO estiloVehiculoDTO;

	// Constructors

	/** default constructor */
	public NegocioEstiloVehiculo() {
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTONEGTIPOUSO_SEQ")
	@SequenceGenerator(name="IDTONEGTIPOUSO_SEQ", sequenceName="IDTONEGTIPOUSO_SEQ", allocationSize=1,schema="MIDAS")
	@Column(name = "IDTONEGESTILOVEHICULO", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getIdToNegEstiloVehiculo() {
		return this.idToNegEstiloVehiculo;
	}

	public void setIdToNegEstiloVehiculo(Long idToNegEstiloVehiculo) {
		this.idToNegEstiloVehiculo = idToNegEstiloVehiculo;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTONEGSECCION", nullable = false)
	public NegocioSeccion getNegocioSeccion() {
		return this.negocioSeccion;
	}

	public void setNegocioSeccion(NegocioSeccion negocioSeccion) {
		this.negocioSeccion = negocioSeccion;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumns({
			@JoinColumn(name = "CLAVETIPOBIEN", referencedColumnName = "CLAVETIPOBIEN", nullable = false),
			@JoinColumn(name = "CLAVEESTILO", referencedColumnName = "CLAVEESTILO", nullable = false),
			@JoinColumn(name = "IDVERSIONCARGA", referencedColumnName = "IDVERSIONCARGA", nullable = false) })
	public EstiloVehiculoDTO getEstiloVehiculoDTO() {
		return this.estiloVehiculoDTO;
	}

	public void setEstiloVehiculoDTO(EstiloVehiculoDTO estiloVehiculoDTO) {
		this.estiloVehiculoDTO = estiloVehiculoDTO;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.idToNegEstiloVehiculo;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}