package mx.com.afirme.midas2.domain.negocio.estadodescuento;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

@Entity
@Table(name="toNegEstadoDescuento" , schema="MIDAS")
public class NegocioEstadoDescuento implements Serializable, Entidad{
	private static final long serialVersionUID = 1L;

	private Long id;
	private Negocio negocio;
    private EstadoDTO estadoDTO;
    private Double pctDescuento;
    private Double pctDescuentoDefault;
    private String zona;
    private Short aplicaSeguroObligatorio = 1;
    private String zipCode;
    private String codigoUsuarioModificacion;
	
	public NegocioEstadoDescuento() {
    
	}
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TONEGESTADODESCUENTO_ID_GENERATOR")
	@SequenceGenerator(name="TONEGESTADODESCUENTO_ID_GENERATOR", sequenceName="MIDAS.idToEstadoDescuento_seq", allocationSize=1)
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	
	public Long getId() {
		return this.id;
	}	
	


	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "PCTDESCUENTO", nullable = false, precision = 8, scale = 4)
	@Digits(integer = 8, fraction= 4,groups=EditItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")	
	public Double getPctDescuento() {
		return pctDescuento;
	}
	public void setPctDescuento(Double pctDescuento) {
		if(pctDescuento == null){
			pctDescuento = 0.0;
		}
		this.pctDescuento = pctDescuento;
	}
	
	@Column(name = "PCTDESCUENTODEFAULT", nullable = false, precision = 8, scale = 4)
	@Digits(integer = 8, fraction= 4,groups=EditItemChecks.class,message="{javax.validation.constraints.Digits.message}")
	@NotNull(groups=EditItemChecks.class, message="{com.afirme.midas2.requerido}")	
	public Double getPctDescuentoDefault() {
		return pctDescuentoDefault;
	}
	public void setPctDescuentoDefault(Double pctDescuentoDefault) {
		if(pctDescuentoDefault == null){
			pctDescuentoDefault = 0.0;
		}
		this.pctDescuentoDefault = pctDescuentoDefault;
	}
	
	@Column(name = "ZONA")
	public String getZona() {
		zona = this.zona != null ? this.zona.toUpperCase() : this.zona;
		return zona;
	}
	public void setZona(String zona) {
		this.zona = zona;
	}
	
	@ManyToOne(fetch = FetchType.LAZY )
  	@JoinColumn(name = "IDTONEGOCIO", referencedColumnName="IDTONEGOCIO" )
	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}


	@ManyToOne(fetch = FetchType.LAZY )
  	@JoinColumn(name = "IDESTADO", referencedColumnName="STATE_ID" )
	public EstadoDTO getEstadoDTO() {
		return estadoDTO;
	}
	public void setEstadoDTO(EstadoDTO estadoDTO) {
		this.estadoDTO = estadoDTO;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {

		return null;
	}

	@Override
	public <K> K getBusinessKey() {

		return null;
	}
	
	public void setAplicaSeguroObligatorio(Short aplicaSeguroObligatorio) {
		this.aplicaSeguroObligatorio = aplicaSeguroObligatorio;
	}
	
	@Column(name = "APLICASEGUROOBLIGATORIO")
	public Short getAplicaSeguroObligatorio() {
		return aplicaSeguroObligatorio;
	}
	
	@Column(name = "ZIPCODE")
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}
	
	@Column(name="CODIGOUSUARIOMODIF")
	public String getCodigoUsuarioModificacion() {
		return codigoUsuarioModificacion;
	}
}
