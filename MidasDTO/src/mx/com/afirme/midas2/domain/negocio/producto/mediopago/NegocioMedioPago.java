package mx.com.afirme.midas2.domain.negocio.producto.mediopago;
// default package

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;


/**
 * NegocioMedioPago entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="TONEGMEDIOPAGO"
    ,schema="MIDAS"
)

public class NegocioMedioPago  implements java.io.Serializable,Entidad {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 4571128659668627252L;
	private BigDecimal idToNegMedioPago;
     private NegocioProducto negocioProducto;
     private MedioPagoDTO medioPagoDTO;
     
     private String codigoUsuarioModificacion;


    // Constructors

    /** default constructor */
    public NegocioMedioPago() {
    }

    
    /** full constructor */
    public NegocioMedioPago(BigDecimal idToNegMedioPago, NegocioProducto negocioProducto, MedioPagoDTO medioPagoDTO) {
        this.idToNegMedioPago = idToNegMedioPago;
        this.negocioProducto = negocioProducto;
        this.medioPagoDTO = medioPagoDTO;
    }

   
    // Property accessors
    @Id 
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTONEGMEDIOPAGO_SEQ")
	@SequenceGenerator(name="IDTONEGMEDIOPAGO_SEQ", sequenceName="IDTONEGMEDIOPAGO_SEQ", allocationSize=1,schema="MIDAS")
    @Column(name="idToNegMedioPago", unique=true, nullable=false, precision=22, scale=0)

    public BigDecimal getIdToNegMedioPago() {
        return this.idToNegMedioPago;
    }
    
    public void setIdToNegMedioPago(BigDecimal idToNegMedioPago) {
        this.idToNegMedioPago = idToNegMedioPago;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="idToNegProducto", nullable=false)
    public NegocioProducto getNegocioProducto() {
        return this.negocioProducto;
    }
    
    public void setNegocioProducto(NegocioProducto negocioProducto) {
        this.negocioProducto = negocioProducto;
    }
    
    @ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="idTcMedioPago", nullable=false)
    public MedioPagoDTO getMedioPagoDTO() {
		return medioPagoDTO;
	}
    
    public void setMedioPagoDTO(MedioPagoDTO medioPagoDTO) {
		this.medioPagoDTO = medioPagoDTO;
	}

	@SuppressWarnings({"unchecked"})
	@Override
	public BigDecimal getKey() {
		return this.idToNegMedioPago;
	}


	@Override
	public String getValue() {
		return this.medioPagoDTO.getDescripcion();
	}


	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
   
	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}

	@Column(name="CODIGOUSUARIOMODIF")
	public String getCodigoUsuarioModificacion() {
		return codigoUsuarioModificacion;
	}







}