package mx.com.afirme.midas2.domain.reporteAgentes;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * @author Martin
 * 10/01/2013
 */
@Entity(name="ConfigReporteAgenteDTO")
@Table(name = "toConfigReporteAgente", schema = "MIDAS")
public class ConfigReporteAgenteDTO implements Entidad{

	private static final long serialVersionUID = 1L;
	private Long id;
	private ReporteAgenteDTO reporteAgenteDTO;
	private Long periodicidad;
	private Long activo;
	private Date fechaEjecucion;
	
	@Id
	public Long getId() {
		return id;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idReporte")
	public ReporteAgenteDTO getReporteAgenteDTO() {
		return reporteAgenteDTO;
	}

	public void setReporteAgenteDTO(ReporteAgenteDTO reporteAgenteDTO) {
		this.reporteAgenteDTO = reporteAgenteDTO;
	}

	@Column(name = "periodicidad")
	public Long getPeriodicidad() {
		return periodicidad;
	}
	@Column(name = "activo")
	public Long getActivo() {
		return activo;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public void setPeriodicidad(Long periodicidad) {
		this.periodicidad = periodicidad;
	}

	public void setActivo(Long activo) {
		this.activo = activo;
	}
	
	
	@Transient
	public Date getFechaEjecucion() {
		return fechaEjecucion;
	}


	public void setFechaEjecion(Date fechaEjecucion) {
		this.fechaEjecucion = fechaEjecucion;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}

}
