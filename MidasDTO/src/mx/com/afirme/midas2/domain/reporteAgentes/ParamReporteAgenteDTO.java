package mx.com.afirme.midas2.domain.reporteAgentes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name="ParamReporteAgenteDTO")
@Table(name = "tcParamReporteAgente", schema = "MIDAS")
public class ParamReporteAgenteDTO implements Entidad {
	private static final long serialVersionUID = 1L;
	private Long id;
	private ReporteAgenteDTO reporteAgenteDTO;
	private String nombre;
	private String valor;

	@Id
	public Long getId() {
		return id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idReporte")
	public ReporteAgenteDTO getReporteAgenteDTO() {
		return reporteAgenteDTO;
	}

	public void setReporteAgenteDTO(ReporteAgenteDTO reporteAgenteDTO) {
		this.reporteAgenteDTO = reporteAgenteDTO;
	}

	@Column(name = "nombre")
	public String getNombre() {
		return nombre;
	}

	@Column(name = "valor")
	public String getValor() {
		return valor;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}
