package mx.com.afirme.midas2.domain.reporteAgentes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name="ReporteAgenteDTO")
@Table(name = "tcReporteAgente" , schema = "MIDAS")
public class ReporteAgenteDTO implements Entidad {

	private static final long serialVersionUID = 1L;
	private Long id;
	private String nombre;
	private String spname;
	private String clave;

	@Id
	public Long getId() {
		return id;
	}

	@Column(name = "nombre")
	public String getNombre() {
		return nombre;
	}

	@Column(name = "spname")
	public String getSpname() {
		return spname;
	}

	@Column(name = "clave")
	public String getClave() {
		return clave;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setSpname(String spname) {
		this.spname = spname;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}
