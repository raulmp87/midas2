package mx.com.afirme.midas2.domain.reporteAgentes;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name = "LogReporteAgenteDTO")
@Table(name = "toLogReporteAgente", schema = "MIDAS")
public class LogReporteAgenteDTO implements Entidad {

	private static final long serialVersionUID = 1L;
	private Long id;
	private ReporteAgenteDTO reporteAgenteDTO;
	private String nombre;
	private Date fechaEjecucion;
	private Long exit;
	private String mensajeError;
	
	@Id
	public Long getId() {
		return id;
	}
	@Column(name = "nombre")
	public String getNombre() {
		return nombre;
	}
	
	@Column(name = "fechaEjecucion")
	@Temporal(TemporalType.DATE)
	public Date getFechaEjecucion() {
		return fechaEjecucion;
	}
	
	@Column(name = "exit")
	public Long getExit() {
		return exit;
	}
	
	@Column(name = "mensajeError")
	public String getMensajeError() {
		return mensajeError;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setFechaEjecucion(Date fechaEjecucion) {
		this.fechaEjecucion = fechaEjecucion;
	}

	public void setExit(Long exit) {
		this.exit = exit;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idReporte")
	public ReporteAgenteDTO getReporteAgenteDTO() {
		return reporteAgenteDTO;
	}
	public void setReporteAgenteDTO(ReporteAgenteDTO reporteAgenteDTO) {
		this.reporteAgenteDTO = reporteAgenteDTO;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}
	@Override
	public String getValue() {
		return null;
	}
	@Override
	public <K> K getBusinessKey() {
		return null;
	}
	
	
}
