package mx.com.afirme.midas2.domain.reaseguro.contraparte;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.SequenceGenerator;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Paulo dos Santos
 */
@Entity
@Table(name="CNSF_SIN_AJUSTES_MANUALES", schema="MIDAS")
public class AjustesManuales {

	private static final long serialVersionUID = -7703645329320642555L;

	private Integer idAjuste;
	private Integer siniestro;
	private String reasegurador;
	private String cnsf;
	private String moneda;
	private BigDecimal reserva;
	private String anio;
	private String sistema;
	private BigDecimal tipoCambio;
	private Date fCorte;
		
	/** default constructor */
    public AjustesManuales() {
    }
	
	public AjustesManuales(Date fcorte, BigDecimal tipoCambio) {
		this.fCorte = fcorte;
		this.tipoCambio = tipoCambio;
	}	
	
	@Id
    @SequenceGenerator(name = "IDCARGA_AJUSTE_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.CNSF_SIN_AJUSTES_MANUALES_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDCARGA_AJUSTE_SEQ_GENERADOR")
    @Column(name="IDAJUSTE", unique=true, nullable=false, precision=22, scale=0)
    public Integer getIdidAjuste() {
        return this.idAjuste;
    }
    
    public void setIdidAjuste(Integer idAjuste) {
        this.idAjuste = idAjuste;
    }

    @Column(name="SINIESTRO", nullable=false)
	public Integer getSiniestro() {
		return siniestro;
	}

	public void setSiniestro(Integer siniestro) {
		this.siniestro = siniestro;
	}

	@Column(name="REASEGURADOR", nullable=false, length=40)
	public String getReasegurador() {
		return reasegurador;
	}

	public void setReasegurador(String reasegurador) {
		this.reasegurador = reasegurador;
	}

	@Column(name="CNSF", nullable=false, length=25)
	public String getCnsf() {
		return cnsf;
	}

	public void setCnsf(String cnsf) {
		this.cnsf = cnsf;
	}
	
	@Column(name="MONEDA", nullable=false, length=15)
	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	@Column(name="RESERVA", nullable=false)
	public BigDecimal getReserva() {
		return reserva;
	}

	public void setReserva(BigDecimal reserva) {
		this.reserva = reserva;
	}	
	
	@Column(name="ANIO", nullable=false, length=15)
	public String getAnio() {
		return anio;
	}
	
	public void setAnio(String anio) {
		this.anio = anio;
	}
	
	@Column(name="SISTEMA", nullable=false, length=8)
	public String getSistema() {
		return sistema;
	}
	
	public void setSistema(String sistema) {
		this.sistema = sistema;
	}
		
	@Column(name="TIPOCAMBIO", nullable=false)
	public BigDecimal getTipoCambio() {
		return tipoCambio;
	}

	public void setTipoCambio(BigDecimal tipoCambio) {
		this.tipoCambio = tipoCambio;
	}	

	@Temporal(TemporalType.DATE)
    @Column(name="FECHA_CORTE", nullable=false, length=10)
	public Date getfCorte() {
		return fCorte;
	}

	public void setfCorte(Date fCorte) {
		this.fCorte = fCorte;
	}
	
}
