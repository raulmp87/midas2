package mx.com.afirme.midas2.domain.reaseguro.reporteRR6;


import java.math.BigDecimal;
import java.sql.Date;

public class ReporteRTRS {

	
	private static final long serialVersionUID = -7703645329320642555L;
	
	private Integer consecutivo; 
	private Integer siniestro; 
	private String claveNegocio; 
	private String siniestroReclamacion; 
	private String asegurado; 
	private String fechaReclamacion; 
	private BigDecimal importeReclamacion; 
	private BigDecimal importeRecSiniestro; 
	private String reasInscrito; 
	private String tipoReasNac; 
	private String claveReasNac; 
	private String reasNoInscrito; 
	private BigDecimal importeProporcional; 
	private BigDecimal importeNoProporcional; 
	private BigDecimal importeFacultativo; 
	private String entidades; 
	private String municipio; 
	private String sector; 
	private Date fechaCorte; 
	private String negocio;
	
	public ReporteRTRS(Date fechaCorte) {
		this.fechaCorte =  fechaCorte;
	}

	public Integer getConsecutivo() {
		return consecutivo;
	}

	public void setConsecutivo(Integer consecutivo) {
		this.consecutivo = consecutivo;
	}

	public Integer getSiniestro() {
		return siniestro;
	}

	public void setSiniestro(Integer siniestro) {
		this.siniestro = siniestro;
	}

	public String getClaveNegocio() {
		return claveNegocio;
	}

	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}

	public String getSiniestroReclamacion() {
		return siniestroReclamacion;
	}

	public void setSiniestroReclamacion(String siniestroReclamacion) {
		this.siniestroReclamacion = siniestroReclamacion;
	}

	public String getAsegurado() {
		return asegurado;
	}

	public void setAsegurado(String asegurado) {
		this.asegurado = asegurado;
	}

	public String getFechaReclamacion() {
		return fechaReclamacion;
	}

	public void setFechaReclamacion(String fechaReclamacion) {
		this.fechaReclamacion = fechaReclamacion;
	}

	public BigDecimal getImporteReclamacion() {
		return importeReclamacion;
	}

	public void setImporteReclamacion(BigDecimal importeReclamacion) {
		this.importeReclamacion = importeReclamacion;
	}

	public BigDecimal getImporteRecSiniestro() {
		return importeRecSiniestro;
	}

	public void setImporteRecSiniestro(BigDecimal importeRecSiniestro) {
		this.importeRecSiniestro = importeRecSiniestro;
	}

	public String getReasInscrito() {
		return reasInscrito;
	}

	public void setReasInscrito(String reasInscrito) {
		this.reasInscrito = reasInscrito;
	}

	public String getTipoReasNac() {
		return tipoReasNac;
	}

	public void setTipoReasNac(String tipoReasNac) {
		this.tipoReasNac = tipoReasNac;
	}

	public String getClaveReasNac() {
		return claveReasNac;
	}

	public void setClaveReasNac(String claveReasNac) {
		this.claveReasNac = claveReasNac;
	}

	public String getReasNoInscrito() {
		return reasNoInscrito;
	}

	public void setReasNoInscrito(String reasNoInscrito) {
		this.reasNoInscrito = reasNoInscrito;
	}

	public BigDecimal getImporteProporcional() {
		return importeProporcional;
	}

	public void setImporteProporcional(BigDecimal importeProporcional) {
		this.importeProporcional = importeProporcional;
	}

	public BigDecimal getImporteNoProporcional() {
		return importeNoProporcional;
	}

	public void setImporteNoProporcional(BigDecimal importeNoProporcional) {
		this.importeNoProporcional = importeNoProporcional;
	}

	public BigDecimal getImporteFacultativo() {
		return importeFacultativo;
	}

	public void setImporteFacultativo(BigDecimal importeFacultativo) {
		this.importeFacultativo = importeFacultativo;
	}

	public String getEntidades() {
		return entidades;
	}

	public void setEntidades(String entidades) {
		this.entidades = entidades;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public Date getFechaCorte() {
		return fechaCorte;
	}

	public void setFechaCorte(Date fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	public String getNegocio() {
		return negocio;
	}

	public void setNegocio(String negocio) {
		this.negocio = negocio;
	}
	
}
