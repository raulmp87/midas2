package mx.com.afirme.midas2.domain.reaseguro.reporteRR6;


import java.math.BigDecimal;
import java.sql.Date;

public class ReporteRARN {

	
	private static final long serialVersionUID = -7703645329320642555L;
	
	private String mes; 
	private String concepto; 
	private String ramo; 
	private String rgre; 
	private String tipo; 
	private String reasNacional; 
	private String noInscrito; 
	private BigDecimal primaCedida; 
	private BigDecimal coberturaXL; 
	private BigDecimal sumaAsegurada;
	private BigDecimal sumaAseguradaRetenida; 
	private Date fechaIniCorte; 
	private Date fechaFinCorte; 
	private String negocio;
	
	public ReporteRARN(Date fechaCorte) {
		this.fechaFinCorte =  fechaCorte;
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getRamo() {
		return ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}

	public String getRgre() {
		return rgre;
	}

	public void setRgre(String rgre) {
		this.rgre = rgre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getReasNacional() {
		return reasNacional;
	}

	public void setReasNacional(String reasNacional) {
		this.reasNacional = reasNacional;
	}

	public String getNoInscrito() {
		return noInscrito;
	}

	public void setNoInscrito(String noInscrito) {
		this.noInscrito = noInscrito;
	}

	public BigDecimal getPrimaCedida() {
		return primaCedida;
	}

	public void setPrimaCedida(BigDecimal primaCedida) {
		this.primaCedida = primaCedida;
	}

	public BigDecimal getCoberturaXL() {
		return coberturaXL;
	}

	public void setCoberturaXL(BigDecimal coberturaXL) {
		this.coberturaXL = coberturaXL;
	}

	public BigDecimal getSumaAsegurada() {
		return sumaAsegurada;
	}

	public void setSumaAsegurada(BigDecimal sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}

	public BigDecimal getSumaAseguradaRetenida() {
		return sumaAseguradaRetenida;
	}

	public void setSumaAseguradaRetenida(BigDecimal sumaAseguradaRetenida) {
		this.sumaAseguradaRetenida = sumaAseguradaRetenida;
	}

	public Date getFechaIniCorte() {
		return fechaIniCorte;
	}

	public void setFechaIniCorte(Date fechaIniCorte) {
		this.fechaIniCorte = fechaIniCorte;
	}

	public Date getFechaFinCorte() {
		return fechaFinCorte;
	}

	public void setFechaFinCorte(Date fechaFinCorte) {
		this.fechaFinCorte = fechaFinCorte;
	}

	public String getNegocio() {
		return negocio;
	}

	public void setNegocio(String negocio) {
		this.negocio = negocio;
	}

	
	
}
