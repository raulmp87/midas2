package mx.com.afirme.midas2.domain.reaseguro.cfdi;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.reaseguro.cfdi.estadocuenta.EstadoCuenta;

@Entity(name="Folio")
@Table(name="CFDI_FOLIO",schema="MIDAS")
public class Folio implements Serializable, Entidad {

	private static final long serialVersionUID = -7802837338182052706L;

	private BigDecimal folio;
	
	private EstadoCuenta estadoCuenta;
	
	private int esEgreso;
	
	private boolean esValido = false;

	@Id
	@SequenceGenerator(name="CFDI_FOLIO_SEQGEN", sequenceName="MIDAS.CFDI_FOLIO_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CFDI_FOLIO_SEQGEN")
	@Column(name = "FOLIO", nullable = false, precision = 22, scale = 0)
	public BigDecimal getFolio() {
		return folio;
	}

	public void setFolio(BigDecimal folio) {
		this.folio = folio;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CFDI_EC_REAS_ID")
	public EstadoCuenta getEstadoCuenta() {
		return estadoCuenta;
	}

	public void setEstadoCuenta(EstadoCuenta estadoCuenta) {
		this.estadoCuenta = estadoCuenta;
	}

	@Column(name="ES_EGRESO", nullable=false, precision=1, scale=0)
	public int getEsEgreso() {
		return esEgreso;
	}

	public void setEsEgreso(int esEgreso) {
		this.esEgreso = esEgreso;
	}
	
	@Transient
	public boolean getEsValido() {
		return esValido;
	}

	public void setEsValido(boolean esValido) {
		this.esValido = esValido;
	}

	public Folio() {
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return folio;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getBusinessKey() {
		return folio;
	}
	
	
	
	
}
