package mx.com.afirme.midas2.domain.reaseguro.reporteRR6;


import java.math.BigDecimal;
import java.sql.Date;

public class ReporteRTRI {

	
	private static final long serialVersionUID = -7703645329320642555L;
	
	private String idContrato; 
	private BigDecimal corretajeContratos; 
	private BigDecimal corretajeFacultativos;
	private Date fechaFinCorte;
	
	public ReporteRTRI(Date fechaCorte) {
		this.fechaFinCorte =  fechaCorte;
	}
	
	public String getIdContrato() {
		return idContrato;
	}
	public void setIdContrato(String idContrato) {
		this.idContrato = idContrato;
	}
	public BigDecimal getCorretajeContratos() {
		return corretajeContratos;
	}
	public void setCorretajeContratos(BigDecimal corretajeContratos) {
		this.corretajeContratos = corretajeContratos;
	}
	public BigDecimal getCorretajeFacultativos() {
		return corretajeFacultativos;
	}
	public void setCorretajeFacultativos(BigDecimal corretajeFacultativos) {
		this.corretajeFacultativos = corretajeFacultativos;
	} 
	
	public Date getFechaFinCorte() {
		return fechaFinCorte;
	}

	public void setFechaFinCorte(Date fechaFinCorte) {
		this.fechaFinCorte = fechaFinCorte;
	}
	
}
