package mx.com.afirme.midas2.domain.reaseguro.contraparte;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.SequenceGenerator;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Paulo dos Santos
 */
@Entity
@Table(name="CNSF_SIN_CONTRATOS_AUTOMATICOS", schema="MIDAS")
public class ContratosAutomaticos {

	private static final long serialVersionUID = -7703645329320642555L;

	private Integer idContratos;
	private String contrato;
	private String reaseguradores;
	private String rgre;
	private BigDecimal participacion;
	private Date vigencia_ini;
	private Date vigencia_fin;
	private Date fCorte;
	private String cobertura;
	
	/** default constructor */
    public ContratosAutomaticos() {
    }
	
	public ContratosAutomaticos(Date fcorte) {
		this.fCorte = fcorte;
	}	
	
	@Id
    @SequenceGenerator(name = "IDCARGACONTRATO_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.CNSF_SIN_CONTR_AUTOMATICOS_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDCARGACONTRATO_SEQ_GENERADOR")
    @Column(name="ID", unique=true, nullable=false, precision=22, scale=0)
    public Integer getIdContratos() {
        return this.idContratos;
    }
    
    public void setIdContratos(Integer idContratos) {
        this.idContratos = idContratos;
    }

    @Column(name="CONTRATO", nullable=false, length=20)
	public String getContrato() {
		return contrato;
	}

	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	@Column(name="REASEGURADORES", nullable=false, length=40)
	public String getReaseguradores() {
		return reaseguradores;
	}

	public void setReaseguradores(String reaseguradores) {
		this.reaseguradores = reaseguradores;
	}

	@Column(name="RGRE", nullable=false, length=25)
	public String getRgre() {
		return rgre;
	}

	public void setRgre(String rgre) {
		this.rgre = rgre;
	}

	@Column(name="PARTICIPACION", nullable=false)
	public BigDecimal getParticipacion() {
		return participacion;
	}

	public void setParticipacion(BigDecimal participacion) {
		this.participacion = participacion;
	}

	@Temporal(TemporalType.DATE)
    @Column(name="VIGENCIA_INI", nullable=false, length=10)
	public Date getVigencia_Ini() {
		return vigencia_ini;
	}

	public void setVigencia_Ini(Date vigencia_ini) {
		this.vigencia_ini = vigencia_ini;
	}

	@Temporal(TemporalType.DATE)
    @Column(name="VIGENCIA_FIN", nullable=false, length=10)
	public Date getVigencia_Fin() {
		return vigencia_fin;
	}

	public void setVigencia_Fin(Date vigencia_fin) {
		this.vigencia_fin = vigencia_fin;
	}
	
	@Temporal(TemporalType.DATE)
    @Column(name="FECHA_CORTE", nullable=false, length=10)
	public Date getfCorte() {
		return fCorte;
	}

	public void setfCorte(Date fCorte) {
		this.fCorte = fCorte;
	}
	
	@Column(name="COBERTURA", nullable=false, length=40)
	public String getCobertura() {
		return cobertura;
	}

	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}
}
