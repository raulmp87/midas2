package mx.com.afirme.midas2.domain.reaseguro.cfdi;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

import org.eclipse.persistence.annotations.Customizer;

@Entity(name="ConceptoHistorico")
@Table(name="CFDI_CONCEPTO_HIST",schema="MIDAS")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class ConceptoHistorico implements Serializable, Entidad, Comparable<ConceptoHistorico> {

	private static final long serialVersionUID = 9127335859981541100L;

	private BigDecimal id;
	
	private BigDecimal idOrigen;
	
	private String clave;
	
	private String descripcion;
		
	private Integer secuencia;
	
	private Boolean esTimbre;
	
	private Boolean esHeader;
	
	private Boolean esObligatorio;
	
	private Boolean esBase;
	
	private Boolean esEstandar;
	
	private Integer tipoValidacion;
	
	private Date fechaInicioRegistro;
	
	private Date fechaFinRegistro;
	
	@Id
	@SequenceGenerator(name="CFDI_CONCEPTO_HSEQGEN", sequenceName="MIDAS.CFDI_CONCEPTO_HIST_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CFDI_CONCEPTO_HSEQGEN")
	@Column(name = "ID", nullable = false, precision = 22, scale = 0)
	public BigDecimal getId() {
		return id;
	}



	public void setId(BigDecimal id) {
		this.id = id;
	}

	
	@Column(name = "IDORIGEN", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdOrigen() {
		return idOrigen;
	}

	public void setIdOrigen(BigDecimal idOrigen) {
		this.idOrigen = idOrigen;
	}
	
	

	@Column(name = "CLAVE", nullable = false, length = 5)
	public String getClave() {
		return clave;
	}



	public void setClave(String clave) {
		this.clave = clave;
	}


	@Column(name = "DESCRIPCION", nullable = false, length = 200)
	public String getDescripcion() {
		return descripcion;
	}



	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	
	
	@Column(name = "SECUENCIA", nullable = false, precision = 8, scale = 0)
	public Integer getSecuencia() {
		return secuencia;
	}



	public void setSecuencia(Integer secuencia) {
		this.secuencia = secuencia;
	}


	@Column(name="ES_TIMBRE", nullable=false, precision=1, scale=0)
	public Boolean getEsTimbre() {
		return esTimbre;
	}



	public void setEsTimbre(Boolean esTimbre) {
		this.esTimbre = esTimbre;
	}


	@Column(name="ES_HEADER", nullable=false, precision=1, scale=0)
	public Boolean getEsHeader() {
		return esHeader;
	}



	public void setEsHeader(Boolean esHeader) {
		this.esHeader = esHeader;
	}


	@Column(name="ES_OBLIGATORIO", nullable=false, precision=1, scale=0)
	public Boolean getEsObligatorio() {
		return esObligatorio;
	}



	public void setEsObligatorio(Boolean esObligatorio) {
		this.esObligatorio = esObligatorio;
	}


	@Column(name="ES_BASE", nullable=false, precision=1, scale=0)
	public Boolean getEsBase() {
		return esBase;
	}



	public void setEsBase(Boolean esBase) {
		this.esBase = esBase;
	}
	
	
	@Column(name="ES_ESTANDAR", nullable=false, precision=1, scale=0)
	public Boolean getEsEstandar() {
		return esEstandar;
	}



	public void setEsEstandar(Boolean esEstandar) {
		this.esEstandar = esEstandar;
	}


	@Column(name = "TIPO_VALIDACION", nullable = false, precision = 8, scale = 0)
	public Integer getTipoValidacion() {
		return tipoValidacion;
	}



	public void setTipoValidacion(Integer tipoValidacion) {
		this.tipoValidacion = tipoValidacion;
	}

	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "H_START_DATE", nullable = false, length = 7)
	public Date getFechaInicioRegistro() {
		return fechaInicioRegistro;
	}

	public void setFechaInicioRegistro(Date fechaInicioRegistro) {
		this.fechaInicioRegistro = fechaInicioRegistro;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "H_END_DATE", nullable = false, length = 7)
	public Date getFechaFinRegistro() {
		return fechaFinRegistro;
	}

	public void setFechaFinRegistro(Date fechaFinRegistro) {
		this.fechaFinRegistro = fechaFinRegistro;
	}
	
	
	public ConceptoHistorico() {
		
	}

	@Override
	public int compareTo(ConceptoHistorico o) {
		return this.secuencia - o.secuencia;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return id;
	}



	@Override
	public String getValue() {
		return descripcion;
	}



	@SuppressWarnings("unchecked")
	@Override
	public String getBusinessKey() {
		return clave;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clave == null) ? 0 : clave.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ConceptoHistorico || obj instanceof String))
			return false;
		if (obj instanceof String) {
			String other = (String) obj;
			if (clave == null) {
				if (other != null)
					return false;
			} else if (!clave.equals(other))
				return false;
		} else {
			ConceptoHistorico other = (ConceptoHistorico) obj;
			if (clave == null) {
				if (other.clave != null)
					return false;
			} else if (!clave.equals(other.clave))
				return false;
		}
				
		return true;
	}
	
	
	
}
