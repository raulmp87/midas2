package mx.com.afirme.midas2.domain.reaseguro.cfdi;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name="Concepto")
@Table(name="CFDI_CONCEPTO",schema="MIDAS")
public class Concepto implements Serializable, Entidad, Comparable<Concepto> {
	
	private static final long serialVersionUID = -6447257003394503687L;
	
	public static final String HD_NOMBRE = "HD001";
	
	public static final String HD_RAMO = "HD002";
	
	public static final String HD_MONEDA = "HD003";
	
	public static final String HD_CALLE = "HD004";
	
	public static final String HD_NUMERO_EXT = "HD005";
	
	public static final String HD_NUMERO_INT = "HD006";
	
	public static final String HD_COLONIA = "HD007";
	
	public static final String HD_CIUDAD = "HD008";
	
	public static final String HD_ESTADO = "HD009";
	
	public static final String HD_PAIS = "HD010";
	
	public static final String HD_CODIGO_POSTAL = "HD011";
	
	public static final String HD_FECHA_INICIO_PERIODO = "HD012";
	
	public static final String HD_FECHA_FIN_PERIODO = "HD013";
	
	public static final String HD_DESCRIPCION_EXTRA = "HD014";
	
	public static final String HD_REMESA = "HD015";
	
	public static final String HD_CHEQUE = "HD016";
		
	public static final String BASE_PRIMAS = "P";
	
	public static final String BASE_OTROS = "O";
	
	private BigDecimal id;
	
	private String clave;
	
	private String descripcion;
	
	private Integer secuencia;
	
	private Boolean esTimbre;
	
	private Boolean esHeader;
	
	private Boolean esObligatorio;
	
	private Boolean esBase;
	
	private Boolean esEstandar;
	
	private Integer tipoValidacion;
	
	
	@Id
	@SequenceGenerator(name="CFDI_CONCEPTO_SEQGEN", sequenceName="MIDAS.CFDI_CONCEPTO_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CFDI_CONCEPTO_SEQGEN")
	@Column(name = "ID", nullable = false, precision = 22, scale = 0)
	public BigDecimal getId() {
		return id;
	}



	public void setId(BigDecimal id) {
		this.id = id;
	}


	@Column(name = "CLAVE", nullable = false, length = 5)
	public String getClave() {
		return clave;
	}



	public void setClave(String clave) {
		this.clave = clave;
	}


	@Column(name = "DESCRIPCION", nullable = false, length = 200)
	public String getDescripcion() {
		return descripcion;
	}



	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	@Column(name = "SECUENCIA", nullable = false, precision = 8, scale = 0)
	public Integer getSecuencia() {
		return secuencia;
	}



	public void setSecuencia(Integer secuencia) {
		this.secuencia = secuencia;
	}


	@Column(name="ES_TIMBRE", nullable=false, precision=1, scale=0)
	public Boolean getEsTimbre() {
		return esTimbre;
	}



	public void setEsTimbre(Boolean esTimbre) {
		this.esTimbre = esTimbre;
	}


	@Column(name="ES_HEADER", nullable=false, precision=1, scale=0)
	public Boolean getEsHeader() {
		return esHeader;
	}



	public void setEsHeader(Boolean esHeader) {
		this.esHeader = esHeader;
	}


	@Column(name="ES_OBLIGATORIO", nullable=false, precision=1, scale=0)
	public Boolean getEsObligatorio() {
		return esObligatorio;
	}



	public void setEsObligatorio(Boolean esObligatorio) {
		this.esObligatorio = esObligatorio;
	}


	@Column(name="ES_BASE", nullable=false, precision=1, scale=0)
	public Boolean getEsBase() {
		return esBase;
	}



	public void setEsBase(Boolean esBase) {
		this.esBase = esBase;
	}
	
	
	

	@Column(name="ES_ESTANDAR", nullable=false, precision=1, scale=0)
	public Boolean getEsEstandar() {
		return esEstandar;
	}



	public void setEsEstandar(Boolean esEstandar) {
		this.esEstandar = esEstandar;
	}


	@Column(name = "TIPO_VALIDACION", nullable = false, precision = 8, scale = 0)
	public Integer getTipoValidacion() {
		return tipoValidacion;
	}



	public void setTipoValidacion(Integer tipoValidacion) {
		this.tipoValidacion = tipoValidacion;
	}



	public Concepto() {
		
	}

	@Override
	public int compareTo(Concepto o) {
		return this.secuencia - o.secuencia;
	}



	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return id;
	}



	@Override
	public String getValue() {
		return descripcion;
	}



	@SuppressWarnings("unchecked")
	@Override
	public String getBusinessKey() {
		return clave;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clave == null) ? 0 : clave.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Concepto || obj instanceof String))
			return false;
		if (obj instanceof String) {
			String other = (String) obj;
			if (clave == null) {
				if (other != null)
					return false;
			} else {
				
				Pattern pattern = Pattern.compile(other);
				
				Matcher matcher = pattern.matcher(clave);
				
				return matcher.find();
				
			}
		} else {
			Concepto other = (Concepto) obj;
			if (clave == null) {
				if (other.clave != null)
					return false;
			} else if (!clave.equals(other.clave))
				return false;
		}
		
		return true;
	}

	
	
}
