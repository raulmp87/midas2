package mx.com.afirme.midas2.domain.reaseguro.cfdi.estadocuenta;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.reaseguro.cfdi.ContratoHistorico;
import mx.com.afirme.midas2.domain.reaseguro.cfdi.Folio;
import mx.com.afirme.midas2.domain.reaseguro.cfdi.SubRamo;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;

@Entity(name="EstadoCuenta")
@Table(name="CFDI_EC_REAS",schema="MIDAS")
public class EstadoCuenta implements Serializable, Entidad {

	
	private static final long serialVersionUID = -7703645329320642555L;
	
	private BigDecimal id;
	
	private SubRamo subRamo;
	
	private ContratoHistorico contratoHistorico;
	
	private BigDecimal idReaseguradorCoaseguradorHist;
	
	private Date fechaInicioPeriodo;
	
	private Date fechaFinPeriodo;
	
	private Date fechaInicioPeriodoAnterior;
	
	private Date fechaFinPeriodoAnterior;
	
	private Date fechaCreacion;
	
	private Integer moneda;
	
	private String descripcionExtra;
	
	private List<ConceptoEstadoCuenta> conceptosEstadoCuenta = new ArrayListNullAware<ConceptoEstadoCuenta>();
	
	private List<Folio> folios = new ArrayListNullAware<Folio>();
	
	private List<Folio> foliosD = new ArrayListNullAware<Folio>();
	
	private String nombreReasCorr;
	
	private String entidad;
	
	private Integer identificador;
	
	public static final String SERIE = "EC";
	
	
	@Id
	@SequenceGenerator(name="CFDI_EC_REAS_SEQGEN", sequenceName="MIDAS.CFDI_EC_REAS_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CFDI_EC_REAS_SEQGEN")
	@Column(name = "ID", nullable = false, precision = 22, scale = 0)
	public BigDecimal getId() {
		return id;
	}



	public void setId(BigDecimal id) {
		this.id = id;
	}

	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CFDI_SUBRAMO_ID")
	public SubRamo getSubRamo() {
		return subRamo;
	}



	public void setSubRamo(SubRamo subRamo) {
		this.subRamo = subRamo;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CFDI_CONTRATO_HIST_ID")
	public ContratoHistorico getContratoHistorico() {
		return contratoHistorico;
	}



	public void setContratoHistorico(ContratoHistorico contratoHistorico) {
		this.contratoHistorico = contratoHistorico;
	}


	@Column(name = "REASCORRCOAS", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdReaseguradorCoaseguradorHist() {
		return idReaseguradorCoaseguradorHist;
	}



	public void setIdReaseguradorCoaseguradorHist(
			BigDecimal idReaseguradorCoaseguradorHist) {
		this.idReaseguradorCoaseguradorHist = idReaseguradorCoaseguradorHist;
	}


	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAINI", nullable = false, length = 7)
	public Date getFechaInicioPeriodo() {
		return fechaInicioPeriodo;
	}


	public void setFechaInicioPeriodo(Date fechaInicioPeriodo) {
		this.fechaInicioPeriodo = fechaInicioPeriodo;
	}


	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAFIN", nullable = false, length = 7)
	public Date getFechaFinPeriodo() {
		return fechaFinPeriodo;
	}



	public void setFechaFinPeriodo(Date fechaFinPeriodo) {
		this.fechaFinPeriodo = fechaFinPeriodo;
	}


	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAINI_PERIODO_ANT", nullable = false, length = 7)
	public Date getFechaInicioPeriodoAnterior() {
		return fechaInicioPeriodoAnterior;
	}



	public void setFechaInicioPeriodoAnterior(Date fechaInicioPeriodoAnterior) {
		this.fechaInicioPeriodoAnterior = fechaInicioPeriodoAnterior;
	}


	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAFIN_PERIODO_ANT", nullable = false, length = 7)
	public Date getFechaFinPeriodoAnterior() {
		return fechaFinPeriodoAnterior;
	}



	public void setFechaFinPeriodoAnterior(Date fechaFinPeriodoAnterior) {
		this.fechaFinPeriodoAnterior = fechaFinPeriodoAnterior;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CREACION", nullable = false, length = 7)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}



	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}


	@Column(name = "IDTCMONEDA", nullable = false, precision = 3, scale = 0)
	public Integer getMoneda() {
		return moneda;
	}



	public void setMoneda(Integer moneda) {
		this.moneda = moneda;
	}


	@Column(name = "DESCRIPCION_EXTRA", nullable = false, length = 60)
	public String getDescripcionExtra() {
		return descripcionExtra;
	}



	public void setDescripcionExtra(String descripcionExtra) {
		this.descripcionExtra = descripcionExtra;
	}

	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "estadoCuenta")
	public List<ConceptoEstadoCuenta> getConceptosEstadoCuenta() {
		return conceptosEstadoCuenta;
	}



	public void setConceptosEstadoCuenta(
			List<ConceptoEstadoCuenta> conceptosEstadoCuenta) {
		this.conceptosEstadoCuenta = conceptosEstadoCuenta;
	}
	

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "estadoCuenta")
	public List<Folio> getFolios() {
		return folios;
	}



	public void setFolios(List<Folio> folios) {
		this.folios = folios;
	}
	
	
	@Transient
	public List<Folio> getFoliosD() {
		return foliosD;
	}



	public void setFoliosD(List<Folio> foliosD) {
		this.foliosD = foliosD;
	}



	@Transient
	public String getNombreReasCorr() {
		return nombreReasCorr;
	}



	public void setNombreReasCorr(String nombreReasCorr) {
		this.nombreReasCorr = nombreReasCorr;
	}



	@Transient
	public String getEntidad() {
		return entidad;
	}



	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}


	
	@Transient
	public Integer getIdentificador() {
		return identificador;
	}



	public void setIdentificador(Integer identificador) {
		this.identificador = identificador;
	}



	public EstadoCuenta() {
		
	}



	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return id;
	}



	@Override
	public String getValue() {
		return descripcionExtra;
	}



	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getBusinessKey() {
		return id;
	}

}
