package mx.com.afirme.midas2.domain.reaseguro.cfdi;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;

@Entity(name="Contrato")
@Table(name="CFDI_CONTRATO",schema="MIDAS")
public class Contrato implements Serializable, Entidad {

	private static final long serialVersionUID = -3866784654173299610L;

	public static final String CLAVE_COASEGURO_DANIOS = "D007";
	
	public static final String CLAVE_COASEGURO_VIDA = "V004";
	
	private BigDecimal id;
	
	private String clave;
	
	private String descripcion;
	
	private String claveNegocio;
		
	private Date ultimaRenovacion;
	
	private Boolean esAutomatico;
	
	private List<SubRamo> subRamos = new ArrayListNullAware<SubRamo>();
	
	private List<MonedaDTO> monedas = new ArrayListNullAware<MonedaDTO>();
		
	private List<Concepto> conceptos = new ArrayListNullAware<Concepto>();
	
	@Id
	@SequenceGenerator(name="CFDI_CONTRATO_SEQGEN", sequenceName="MIDAS.CFDI_CONTRATO_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CFDI_CONTRATO_SEQGEN")
	@Column(name = "ID", nullable = false, precision = 22, scale = 0)
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}
	
	@Column(name = "CLAVE", nullable = false, length = 5)
	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	@Column(name = "DESCRIPCION", nullable = false, length = 300)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "CLAVENEGOCIO", nullable = false, length = 1)
	public String getClaveNegocio() {
		return claveNegocio;
	}

	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "ULTIMA_RENOVACION", nullable = false, length = 7)
	public Date getUltimaRenovacion() {
		return ultimaRenovacion;
	}

	public void setUltimaRenovacion(Date ultimaRenovacion) {
		this.ultimaRenovacion = ultimaRenovacion;
	}
	
	@Column(name="ES_AUTOMATICO", nullable=false, precision=1, scale=0)
	public Boolean getEsAutomatico() {
		return esAutomatico;
	}

	public void setEsAutomatico(Boolean esAutomatico) {
		this.esAutomatico = esAutomatico;
	}

	@ManyToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinTable(name = "CFDI_CONTRATO_SUBRAMO_REL", schema = "MIDAS",
			joinColumns = {
				@JoinColumn(name="CFDI_CONTRATO_ID") 
			},
			inverseJoinColumns = {
				@JoinColumn(name="CFDI_SUBRAMO_ID")
			}
	)
	public List<SubRamo> getSubRamos() {
		return subRamos;
	}

	public void setSubRamos(List<SubRamo> subRamos) {
		this.subRamos = subRamos;
	}
	
	
	@ManyToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinTable(name = "CFDI_CONTRATO_MONEDA_REL", schema = "MIDAS",
			joinColumns = {
				@JoinColumn(name="CFDI_CONTRATO_ID") 
			},
			inverseJoinColumns = {
				@JoinColumn(name="CVE_MONEDA")
			}
	)
	public List<MonedaDTO> getMonedas() {
		return monedas;
	}

	public void setMonedas(List<MonedaDTO> monedas) {
		this.monedas = monedas;
	}

	@ManyToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinTable(name = "CFDI_CTO_CPTO_REL", schema = "MIDAS",
			joinColumns = {
				@JoinColumn(name="CFDI_CONTRATO_ID") 
			},
			inverseJoinColumns = {
				@JoinColumn(name="CFDI_CONCEPTO_ID")
			}
	)
	public List<Concepto> getConceptos() {
		return conceptos;
	}

	public void setConceptos(List<Concepto> conceptos) {
		this.conceptos = conceptos;
	}

	public Contrato() {
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getBusinessKey() {
		return clave;
	}
	
	
	
}
