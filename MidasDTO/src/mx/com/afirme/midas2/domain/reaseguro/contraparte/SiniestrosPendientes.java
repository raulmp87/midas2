package mx.com.afirme.midas2.domain.reaseguro.contraparte;


import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Paulo dos Santos
 */
@Entity
@Table(name="CNSF_SIN_PENDIENTES", schema="MIDAS")
public class SiniestrosPendientes {

	
	private static final long serialVersionUID = -7703645329320642555L;
	
	private Integer idPendientes;
	private String poliza;
	private Integer moneda;
	private Integer anio_siniestro;
	private Integer num_siniestro;
	private BigDecimal reserva;
	private BigDecimal costo_siniestro;
	private BigDecimal afirme;
	private BigDecimal hannover_life;
	private BigDecimal mafre;
	private String status;
	private Date fCorte;
	private Date inicio_vigencia;
	private String cobertura;
	
	/** default constructor */
    public SiniestrosPendientes() {
    }
	
	public SiniestrosPendientes(Date fechaCorte) {
		fCorte =  fechaCorte;
	}
	
	@Id
    @SequenceGenerator(name = "IDCARGAPENDIENTE_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.CNSF_SIN_VIDA_PENDIENTE_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDCARGAPENDIENTE_SEQ_GENERADOR")
    @Column(name="ID", unique=true, nullable=false, precision=22, scale=0)
    public Integer getIdpendientes() {
        return this.idPendientes;
    }
    
    public void setIdpendientes(Integer idPendientes) {
        this.idPendientes = idPendientes;
    }
	
    @Column(name="POLIZA", nullable=false, length=20)
	public String getPoliza() {
		return poliza;
	}
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	
	@Column(name="MONEDA", nullable=false)
	public Integer getMoneda() {
		return moneda;
	}
	public void setMoneda(Integer moneda) {
		this.moneda = moneda;
	}
	
	@Column(name="ANIO_SINIESTRO", nullable=false)
	public Integer getAnio_Siniestro() {
		return anio_siniestro;
	}
	public void setAnio_Siniestro(Integer anio_siniestro) {
		this.anio_siniestro = anio_siniestro;
	}
	
	@Column(name="NUM_SINIESTRO", nullable=false)
	public Integer getNum_Siniestro() {
		return num_siniestro;
	}
	public void setNum_Siniestro(Integer num_siniestro) {
		this.num_siniestro = num_siniestro;
	}
	
	@Column(name="RESERVA", nullable=false)
	public BigDecimal getReserva() {
		return reserva;
	}
	public void setReserva(BigDecimal reserva) {
		this.reserva = reserva;
	}
	
	@Column(name="COSTO_SINIESTRO", nullable=false)
	public BigDecimal getCosto_Siniestro() {
		return costo_siniestro;
	}
	public void setCosto_Siniestro(BigDecimal costo_siniestro) {
		this.costo_siniestro = costo_siniestro;
	}
	
	@Column(name="AFIRME", nullable=false)
	public BigDecimal getAfirme() {
		return afirme;
	}
	public void setAfirme(BigDecimal afirme) {
		this.afirme = afirme;
	}
	
	@Column(name="HANNOVER_LIFE", nullable=false)
	public BigDecimal getHannover_Life() {
		return hannover_life;
	}
	public void setHannover_Life(BigDecimal hannover_life) {
		this.hannover_life = hannover_life;
	}
	
	@Column(name="MAFRE_RE", nullable=false)
	public BigDecimal getMafre() {
		return mafre;
	}
	public void setMafre(BigDecimal mafre) {
		this.mafre = mafre;
	}
	
	@Column(name="STATUS", nullable=false)
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CORTE", nullable=false, length=10)
	public Date getfCorte() {
		return fCorte;
	}
	public void setfCorte(Date fCorte) {
		this.fCorte = fCorte;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name="INICIO_VIGENCIA", nullable=false, length=10)
	public Date getInicioVigencia() {
		return inicio_vigencia;
	}
	public void setInicioVigencia(Date inicio_vigencia) {
		this.inicio_vigencia = inicio_vigencia;
	}
	
	@Column(name="COBERTURA", nullable=false, length=40)
	public String getCobertura() {
		return cobertura;
	}

	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}
	
}
