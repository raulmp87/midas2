package mx.com.afirme.midas2.domain.reaseguro.cfdi.estadocuenta;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.domain.reaseguro.cfdi.ConceptoHistorico;

@Entity(name="ConceptoEstadoCuenta")
@Table(name="CFDI_EC_CONCEPTO",schema="MIDAS")
public class ConceptoEstadoCuenta implements Serializable, Comparable<ConceptoEstadoCuenta> {

	private static final long serialVersionUID = -1347963441547960474L;

	private BigDecimal id;
	
	private EstadoCuenta estadoCuenta;
	
	private ConceptoHistorico conceptoHistorico;
	
	private BigDecimal debe;
	
	private BigDecimal haber;
	
	
	@Id
	@SequenceGenerator(name="CFDI_EC_CONCEPTO_SEQGEN", sequenceName="MIDAS.CFDI_EC_CONCEPTO_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CFDI_EC_CONCEPTO_SEQGEN")
	@Column(name = "ID", nullable = false, precision = 22, scale = 0)
	public BigDecimal getId() {
		return id;
	}



	public void setId(BigDecimal id) {
		this.id = id;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CFDI_EC_REAS_ID")
	public EstadoCuenta getEstadoCuenta() {
		return estadoCuenta;
	}



	public void setEstadoCuenta(EstadoCuenta estadoCuenta) {
		this.estadoCuenta = estadoCuenta;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CFDI_CONCEPTO_HIST_ID")
	public ConceptoHistorico getConceptoHistorico() {
		return conceptoHistorico;
	}



	public void setConceptoHistorico(ConceptoHistorico conceptoHistorico) {
		this.conceptoHistorico = conceptoHistorico;
	}


	@Column(name = "DEBE", nullable = false, precision = 16, scale = 2)
	public BigDecimal getDebe() {
		return debe;
	}



	public void setDebe(BigDecimal debe) {
		this.debe = debe;
	}


	@Column(name = "HABER", nullable = false, precision = 16, scale = 2)
	public BigDecimal getHaber() {
		return haber;
	}



	public void setHaber(BigDecimal haber) {
		this.haber = haber;
	}



	public ConceptoEstadoCuenta() {
		
	}



	@Override
	public int compareTo(ConceptoEstadoCuenta o) {
		return this.conceptoHistorico.getSecuencia() - o.conceptoHistorico.getSecuencia();
	}
	
}
