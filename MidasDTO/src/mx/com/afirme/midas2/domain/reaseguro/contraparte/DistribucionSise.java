package mx.com.afirme.midas2.domain.reaseguro.contraparte;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.SequenceGenerator;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Paulo dos Santos
 */
@Entity
@Table(name="CNSF_SIN_DISTRIB_SISE", schema="MIDAS")
public class DistribucionSise {

	private static final long serialVersionUID = -7703645329320642555L;

	private Integer idSise;
	private Integer siniestro;
	private String reasegurador;
	private String rgre;
	private BigDecimal reserva;
	private String moneda;
	private BigDecimal tipoCambio;
	private Date fCorte;
	
	
	
	/** default constructor */
    public DistribucionSise() {
    }
	
	public DistribucionSise(Date fcorte, BigDecimal tipoCambio) {
		this.fCorte = fcorte;
		this.tipoCambio = tipoCambio;
	}	
	
	@Id
    @SequenceGenerator(name = "IDCARGASISE_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.CNSF_SIN_DISTRIB_SISE_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDCARGASISE_SEQ_GENERADOR")
    @Column(name="ID", unique=true, nullable=false, precision=22, scale=0)
    public Integer getIdidSise() {
        return this.idSise;
    }
    
    public void setIdidSise(Integer idSise) {
        this.idSise = idSise;
    }

    @Column(name="SINIESTRO", nullable=false)
	public Integer getSiniestro() {
		return siniestro;
	}

	public void setSiniestro(Integer siniestro) {
		this.siniestro = siniestro;
	}

	@Column(name="REASEGURADOR", nullable=false, length=40)
	public String getReasegurador() {
		return reasegurador;
	}

	public void setReasegurador(String reasegurador) {
		this.reasegurador = reasegurador;
	}

	@Column(name="RGRE", nullable=false, length=25)
	public String getRgre() {
		return rgre;
	}

	public void setRgre(String rgre) {
		this.rgre = rgre;
	}

	@Column(name="RESERVA", nullable=false)
	public BigDecimal getReserva() {
		return reserva;
	}

	public void setReserva(BigDecimal reserva) {
		this.reserva = reserva;
	}
	
	@Column(name="MONEDA", nullable=false)
	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	
	@Column(name="TIPOCAMBIO", nullable=false)
	public BigDecimal getTipoCambio() {
		return tipoCambio;
	}

	public void setTipoCambio(BigDecimal tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	@Temporal(TemporalType.DATE)
    @Column(name="FECHA_CORTE", nullable=false, length=10)
	public Date getfCorte() {
		return fCorte;
	}

	public void setfCorte(Date fCorte) {
		this.fCorte = fCorte;
	}	
	
}
