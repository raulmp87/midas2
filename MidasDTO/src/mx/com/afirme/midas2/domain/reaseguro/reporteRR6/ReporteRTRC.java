package mx.com.afirme.midas2.domain.reaseguro.reporteRR6;


import java.math.BigDecimal;
import java.sql.Date;

public class ReporteRTRC {

	
	private static final long serialVersionUID = -7703645329320642555L;
	
	private String identificador;
	private String clave;
	private String negCubiertos;
	private String moneda;
	private String captura;
	private String modificados;
	private String fechainicial;
	private String fechafinal;
	private Integer tipocontrato;
	private String capas;
	private BigDecimal porcionCedida;
	private BigDecimal retencionPrioridad;
	private String retencionFianzas;
	private BigDecimal capacidadMaxima;
	private String capMaxFianzas;
	private BigDecimal importe;
	private String comision;
	private String utilidades;
	private String reasInscrito;
	private String tipoReas;
	private String reasNacional;
	private String noInscrito;
	private String participacion;
	private String tipoIntermediario;
	private String id_Intermediario; 
	private String intermNoAutorizado; 
	private String suscriptor; 
	private String aclaracion;
	private Date fCorte;
	private String negocio;
	private Integer orden;
	
	public ReporteRTRC(Date fechaCorte) {
		fCorte =  fechaCorte;
	}
	
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getNegCubiertos() {
		return negCubiertos;
	}
	public void setNegCubiertos(String negCubiertos) {
		this.negCubiertos = negCubiertos;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getCaptura() {
		return captura;
	}
	public void setCaptura(String captura) {
		this.captura = captura;
	}
	public String getModificados() {
		return modificados;
	}
	public void setModificados(String modificados) {
		this.modificados = modificados;
	}
	public String getFechainicial() {
		return fechainicial;
	}
	public void setFechainicial(String fechainicial) {
		this.fechainicial = fechainicial;
	}
	public String getFechafinal() {
		return fechafinal;
	}
	public void setFechafinal(String fechafinal) {
		this.fechafinal = fechafinal;
	}
	public Integer getTipocontrato() {
		return tipocontrato;
	}
	public void setTipocontrato(Integer tipocontrato) {
		this.tipocontrato = tipocontrato;
	}
	public String getCapas() {
		return capas;
	}
	public void setCapas(String capas) {
		this.capas = capas;
	}
	public BigDecimal getPorcionCedida() {
		return porcionCedida;
	}
	public void setPorcionCedida(BigDecimal porcionCedida) {
		this.porcionCedida = porcionCedida;
	}
	public BigDecimal getRetencionPrioridad() {
		return retencionPrioridad;
	}
	public void setRetencionPrioridad(BigDecimal retencionPrioridad) {
		this.retencionPrioridad = retencionPrioridad;
	}
	public String getRetencionFianzas() {
		return retencionFianzas;
	}
	public void setRetencionFianzas(String retencionFianzas) {
		this.retencionFianzas = retencionFianzas;
	}
	public BigDecimal getCapacidadMaxima() {
		return capacidadMaxima;
	}
	public void setCapacidadMaxima(BigDecimal capacidadMaxima) {
		this.capacidadMaxima = capacidadMaxima;
	}
	public String getCapMaxFianzas() {
		return capMaxFianzas;
	}
	public void setCapMaxFianzas(String capMaxFianzas) {
		this.capMaxFianzas = capMaxFianzas;
	}
	public BigDecimal getImporte() {
		return importe;
	}
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}
	public String getComision() {
		return comision;
	}
	public void setComision(String comision) {
		this.comision = comision;
	}
	public String getUtilidades() {
		return utilidades;
	}
	public void setUtilidades(String utilidades) {
		this.utilidades = utilidades;
	}
	public String getReasInscrito() {
		return reasInscrito;
	}
	public void setReasInscrito(String reasInscrito) {
		this.reasInscrito = reasInscrito;
	}
	public String getTipoReas() {
		return tipoReas;
	}
	public void setTipoReas(String tipoReas) {
		this.tipoReas = tipoReas;
	}
	public String getReasNacional() {
		return reasNacional;
	}
	public void setReasNacional(String reasNacional) {
		this.reasNacional = reasNacional;
	}
	public String getNoInscrito() {
		return noInscrito;
	}
	public void setNoInscrito(String noInscrito) {
		this.noInscrito = noInscrito;
	}
	public String getParticipacion() {
		return participacion;
	}
	public void setParticipacion(String participacion) {
		this.participacion = participacion;
	}
	public String getTipoIntermediario() {
		return tipoIntermediario;
	}
	public void setTipoIntermediario(String tipoIntermediario) {
		this.tipoIntermediario = tipoIntermediario;
	}
	public String getId_Intermediario() {
		return id_Intermediario;
	}
	public void setId_Intermediario(String id_Intermediario) {
		this.id_Intermediario = id_Intermediario;
	}
	public String getIntermNoAutorizado() {
		return intermNoAutorizado;
	}
	public void setIntermNoAutorizado(String intermNoAutorizado) {
		this.intermNoAutorizado = intermNoAutorizado;
	}
	public String getSuscriptor() {
		return suscriptor;
	}
	public void setSuscriptor(String suscriptor) {
		this.suscriptor = suscriptor;
	}
	public String getAclaracion() {
		return aclaracion;
	}
	public void setAclaracion(String aclaracion) {
		this.aclaracion = aclaracion;
	}
	public Date getfCorte() {
		return fCorte;
	}
	public void setfCorte(Date fCorte) {
		this.fCorte = fCorte;
	}
	public String getNegocio() {
		return negocio;
	}
	public void setNegocio(String negocio) {
		this.negocio = negocio;
	}
	public Integer getOrden() {
		return orden;
	}
	public void setOrden(Integer orden) {
		this.orden = orden;
	}
	
}
