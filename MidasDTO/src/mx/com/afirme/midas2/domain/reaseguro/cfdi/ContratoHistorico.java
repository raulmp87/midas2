package mx.com.afirme.midas2.domain.reaseguro.cfdi;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

import org.eclipse.persistence.annotations.Customizer;

@Entity(name="ContratoHistorico")
@Table(name="CFDI_CONTRATO_HIST",schema="MIDAS")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class ContratoHistorico implements Serializable, Entidad {
	
	private static final long serialVersionUID = 6141405721281974569L;

	private BigDecimal id;
	
	private BigDecimal idOrigen;
	
	private String clave;
	
	private String descripcion;
	
	private String claveNegocio;
	
	private Date ultimaRenovacion;
	
	private Boolean esAutomatico;
	
	private Date fechaInicioRegistro;
	
	private Date fechaFinRegistro;
		
	@Id
	@SequenceGenerator(name="CFDI_CONTRATO_HSEQGEN", sequenceName="MIDAS.CFDI_CONTRATO_HIST_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CFDI_CONTRATO_HSEQGEN")
	@Column(name = "ID", nullable = false, precision = 22, scale = 0)
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}
	
	@Column(name = "IDORIGEN", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdOrigen() {
		return idOrigen;
	}

	public void setIdOrigen(BigDecimal idOrigen) {
		this.idOrigen = idOrigen;
	}
	
	@Column(name = "CLAVE", nullable = false, length = 5)
	public String getClave() {
		return clave;
	}
		
	public void setClave(String clave) {
		this.clave = clave;
	}

	@Column(name = "DESCRIPCION", nullable = false, length = 300)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "CLAVENEGOCIO", nullable = false, length = 1)
	public String getClaveNegocio() {
		return claveNegocio;
	}

	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}
	
	@Column(name="ES_AUTOMATICO", nullable=false, precision=1, scale=0)
	public Boolean getEsAutomatico() {
		return esAutomatico;
	}

	public void setEsAutomatico(Boolean esAutomatico) {
		this.esAutomatico = esAutomatico;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "ULTIMA_RENOVACION", nullable = false, length = 7)
	public Date getUltimaRenovacion() {
		return ultimaRenovacion;
	}

	public void setUltimaRenovacion(Date ultimaRenovacion) {
		this.ultimaRenovacion = ultimaRenovacion;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "H_START_DATE", nullable = false, length = 7)
	public Date getFechaInicioRegistro() {
		return fechaInicioRegistro;
	}

	public void setFechaInicioRegistro(Date fechaInicioRegistro) {
		this.fechaInicioRegistro = fechaInicioRegistro;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "H_END_DATE", nullable = false, length = 7)
	public Date getFechaFinRegistro() {
		return fechaFinRegistro;
	}

	public void setFechaFinRegistro(Date fechaFinRegistro) {
		this.fechaFinRegistro = fechaFinRegistro;
	}
		

	public ContratoHistorico() {
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getBusinessKey() {
		return clave;
	}

	
	
	
}
