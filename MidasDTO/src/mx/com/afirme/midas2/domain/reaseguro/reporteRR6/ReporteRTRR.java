package mx.com.afirme.midas2.domain.reaseguro.reporteRR6;


import java.math.BigDecimal;
import java.sql.Date;

public class ReporteRTRR {

	
	private static final long serialVersionUID = -7703645329320642555L;
	
	private String identificador; 
	private Integer tipoContrato; 
	private String anio; 
	private String clave; 
	private String iniciovigencia; 
	private BigDecimal comisiones; 
	private BigDecimal utilidades; 
	private BigDecimal sinReclamaFac; 
	private BigDecimal sinReclamaNoProp; 
	private BigDecimal ingresos; 
	private BigDecimal otrosIngresos; 
	private BigDecimal montoPrima; 
	private BigDecimal costoCobertura; 
	private BigDecimal partSalvamento; 
	private BigDecimal recursosRetenidos; 
	private BigDecimal reasFinanciero; 
	private BigDecimal castigo; 
	private BigDecimal egresos; 
	private BigDecimal gastosReas; 
	private String aclaraciones; 
	private Date fechaCorte; 
	private String negocio;
	
	public ReporteRTRR(Date fechaCorte) {
		this.fechaCorte =  fechaCorte;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public Integer getTipoContrato() {
		return tipoContrato;
	}

	public void setTipoContrato(Integer tipoContrato) {
		this.tipoContrato = tipoContrato;
	}

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getIniciovigencia() {
		return iniciovigencia;
	}

	public void setIniciovigencia(String iniciovigencia) {
		this.iniciovigencia = iniciovigencia;
	}

	public BigDecimal getComisiones() {
		return comisiones;
	}

	public void setComisiones(BigDecimal comisiones) {
		this.comisiones = comisiones;
	}

	public BigDecimal getUtilidades() {
		return utilidades;
	}

	public void setUtilidades(BigDecimal utilidades) {
		this.utilidades = utilidades;
	}

	public BigDecimal getSinReclamaFac() {
		return sinReclamaFac;
	}

	public void setSinReclamaFac(BigDecimal sinReclamaFac) {
		this.sinReclamaFac = sinReclamaFac;
	}

	public BigDecimal getSinReclamaNoProp() {
		return sinReclamaNoProp;
	}

	public void setSinReclamaNoProp(BigDecimal sinReclamaNoProp) {
		this.sinReclamaNoProp = sinReclamaNoProp;
	}

	public BigDecimal getIngresos() {
		return ingresos;
	}

	public void setIngresos(BigDecimal ingresos) {
		this.ingresos = ingresos;
	}

	public BigDecimal getOtrosIngresos() {
		return otrosIngresos;
	}

	public void setOtrosIngresos(BigDecimal otrosIngresos) {
		this.otrosIngresos = otrosIngresos;
	}

	public BigDecimal getMontoPrima() {
		return montoPrima;
	}

	public void setMontoPrima(BigDecimal montoPrima) {
		this.montoPrima = montoPrima;
	}

	public BigDecimal getCostoCobertura() {
		return costoCobertura;
	}

	public void setCostoCobertura(BigDecimal costoCobertura) {
		this.costoCobertura = costoCobertura;
	}

	public BigDecimal getPartSalvamento() {
		return partSalvamento;
	}

	public void setPartSalvamento(BigDecimal partSalvamento) {
		this.partSalvamento = partSalvamento;
	}

	public BigDecimal getRecursosRetenidos() {
		return recursosRetenidos;
	}

	public void setRecursosRetenidos(BigDecimal recursosRetenidos) {
		this.recursosRetenidos = recursosRetenidos;
	}

	public BigDecimal getReasFinanciero() {
		return reasFinanciero;
	}

	public void setReasFinanciero(BigDecimal reasFinanciero) {
		this.reasFinanciero = reasFinanciero;
	}

	public BigDecimal getCastigo() {
		return castigo;
	}

	public void setCastigo(BigDecimal castigo) {
		this.castigo = castigo;
	}

	public BigDecimal getEgresos() {
		return egresos;
	}

	public void setEgresos(BigDecimal egresos) {
		this.egresos = egresos;
	}

	public BigDecimal getGastosReas() {
		return gastosReas;
	}

	public void setGastosReas(BigDecimal gastosReas) {
		this.gastosReas = gastosReas;
	}

	public String getAclaraciones() {
		return aclaraciones;
	}

	public void setAclaraciones(String aclaraciones) {
		this.aclaraciones = aclaraciones;
	}

	public Date getFechaCorte() {
		return fechaCorte;
	}

	public void setFechaCorte(Date fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	public String getNegocio() {
		return negocio;
	}

	public void setNegocio(String negocio) {
		this.negocio = negocio;
	}

	
	
}
