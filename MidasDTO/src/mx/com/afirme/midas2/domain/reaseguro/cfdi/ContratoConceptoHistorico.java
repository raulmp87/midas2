package mx.com.afirme.midas2.domain.reaseguro.cfdi;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

import org.eclipse.persistence.annotations.Customizer;

@Entity(name="ContratoConceptoHistorico")
@Table(name="CFDI_CTO_CPTO_REL_HIST",schema="MIDAS")
@Customizer(mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HistoryCustomizerOnlyQuerying.class)
public class ContratoConceptoHistorico implements Serializable, Entidad {
		
	private static final long serialVersionUID = -7404562922049379439L;

	private ContratoConceptoHistoricoId id;
	
	private Contrato contrato;
	
	private Concepto concepto;
	
	private Date fechaInicioRegistro;
	
	private Date fechaFinRegistro;
	
	
	
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "contratoId", column = @Column(name = "CFDI_CONTRATO_ID", nullable = false)),
			@AttributeOverride(name = "conceptoId", column = @Column(name = "CFDI_CONCEPTO_ID", nullable = false)) })
	public ContratoConceptoHistoricoId getId() {
		return id;
	}

	public void setId(ContratoConceptoHistoricoId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CFDI_CONTRATO_ID", nullable=false, insertable=false, updatable=false)		
	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CFDI_CONCEPTO_ID", nullable=false, insertable=false, updatable=false)
	public Concepto getConcepto() {
		return concepto;
	}

	public void setConcepto(Concepto concepto) {
		this.concepto = concepto;
	}

	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "H_START_DATE", nullable = false, length = 7)
	public Date getFechaInicioRegistro() {
		return fechaInicioRegistro;
	}

	public void setFechaInicioRegistro(Date fechaInicioRegistro) {
		this.fechaInicioRegistro = fechaInicioRegistro;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "H_END_DATE", nullable = false, length = 7)
	public Date getFechaFinRegistro() {
		return fechaFinRegistro;
	}

	public void setFechaFinRegistro(Date fechaFinRegistro) {
		this.fechaFinRegistro = fechaFinRegistro;
	}
		

	public ContratoConceptoHistorico() {
	}

	@SuppressWarnings("unchecked")
	@Override
	public ContratoConceptoHistoricoId getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getBusinessKey() {
		return null;
	}

	
	
	
}
