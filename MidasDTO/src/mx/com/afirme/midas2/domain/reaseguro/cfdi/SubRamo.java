package mx.com.afirme.midas2.domain.reaseguro.cfdi;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;

@Entity(name="SubRamo")
@Table(name="CFDI_SUBRAMO",schema="MIDAS")
public class SubRamo implements Serializable, Entidad {
	
	private static final long serialVersionUID = -4415593061226775489L;

	private BigDecimal id;
	
	private String clave;
	
	private String descripcion;
	
	private MonedaDTO monedaUnica;
	
	private List<Contrato> contratos = new ArrayListNullAware<Contrato>();
	
	@Id
	@SequenceGenerator(name="CFDI_SUBRAMO_SEQGEN", sequenceName="MIDAS.CFDI_SUBRAMO_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CFDI_SUBRAMO_SEQGEN")
	@Column(name = "ID", nullable = false, precision = 22, scale = 0)
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}
	
	@Column(name = "CLAVE", nullable = false, length = 5)
	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	@Column(name = "DESCRIPCION", nullable = false, length = 200)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@JoinColumn(name = "MONEDA_UNICA", nullable = true)
	public MonedaDTO getMonedaUnica() {
		return monedaUnica;
	}

	public void setMonedaUnica(MonedaDTO monedaUnica) {
		this.monedaUnica = monedaUnica;
	}
	

	@ManyToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinTable(name = "CFDI_CONTRATO_SUBRAMO_REL", schema = "MIDAS",
			joinColumns = {
				@JoinColumn(name="CFDI_SUBRAMO_ID") 
			},
			inverseJoinColumns = {
				@JoinColumn(name="CFDI_CONTRATO_ID")
			}
	)
	public List<Contrato> getContratos() {
		return contratos;
	}

	public void setContratos(List<Contrato> contratos) {
		this.contratos = contratos;
	}

	public SubRamo() {
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getBusinessKey() {
		return clave;
	}
	
}
