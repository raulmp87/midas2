package mx.com.afirme.midas2.domain.reaseguro.contraparte;


import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Paulo dos Santos
 */
@Entity
@Table(name="CNSF_SIN_POL_FACULTADAS", schema="MIDAS")
public class PolizasFacultadas {

	
	private static final long serialVersionUID = -7703645329320642555L;
	
	private Integer idPendientes;
	private String poliza;
	private Integer moneda;
	private Integer aniosiniestro;
	private Integer numsiniestro;
	private BigDecimal reserva;
	private BigDecimal costosiniestro;
	private BigDecimal afirme;
	private BigDecimal montodistribucion;
	private String status;
	private String rgre;
	private String reasegurador;
	private Date fCorte;
	
	/** default constructor */
    public PolizasFacultadas() {
    }
	
	public PolizasFacultadas(Date fechaCorte) {
		fCorte =  fechaCorte;
	}
	
	@Id
    @SequenceGenerator(name = "IDPOLFAC_SEQ_GENERADOR", allocationSize = 1, sequenceName = "MIDAS.CNSF_SIN_POL_FACULTADAS_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDPOLFAC_SEQ_GENERADOR")
    @Column(name="ID", unique=true, nullable=false, precision=22, scale=0)
    public Integer getIdpendientes() {
        return this.idPendientes;
    }
    
    public void setIdpendientes(Integer idPendientes) {
        this.idPendientes = idPendientes;
    }
	
    @Column(name="POLIZA", nullable=false, length=20)
	public String getPoliza() {
		return poliza;
	}
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	
	@Column(name="MONEDA", nullable=false)
	public Integer getMoneda() {
		return moneda;
	}
	public void setMoneda(Integer moneda) {
		this.moneda = moneda;
	}
	
	@Column(name="ANIO_SINIESTRO", nullable=false)
	public Integer getAnioSiniestro() {
		return aniosiniestro;
	}
	public void setAnioSiniestro(Integer aniosiniestro) {
		this.aniosiniestro = aniosiniestro;
	}
	
	@Column(name="NUM_SINIESTRO", nullable=false)
	public Integer getNumSiniestro() {
		return numsiniestro;
	}
	public void setNumSiniestro(Integer numsiniestro) {
		this.numsiniestro = numsiniestro;
	}
	
	@Column(name="RESERVA", nullable=false)
	public BigDecimal getReserva() {
		return reserva;
	}
	public void setReserva(BigDecimal reserva) {
		this.reserva = reserva;
	}
	
	@Column(name="COSTO_SINIESTRO", nullable=false)
	public BigDecimal getCostoSiniestro() {
		return costosiniestro;
	}
	public void setCostoSiniestro(BigDecimal costosiniestro) {
		this.costosiniestro = costosiniestro;
	}
	
	@Column(name="AFIRME", nullable=false)
	public BigDecimal getAfirme() {
		return afirme;
	}
	public void setAfirme(BigDecimal afirme) {
		this.afirme = afirme;
	}
	
	@Column(name="MONTO_DISTRIBUCION", nullable=false)
	public BigDecimal getMontoDistribucion() {
		return montodistribucion;
	}
	public void setMontoDistribucion(BigDecimal montodistribucion) {
		this.montodistribucion = montodistribucion;
	}
	
	@Column(name="STATUS", nullable=false)
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="RGRE", nullable=false, length=25)
	public String getRgre() {
		return rgre;
	}

	public void setRgre(String rgre) {
		this.rgre = rgre;
	}
	
	@Column(name="REASEGURADOR", nullable=false, length=40)
	public String getReasegurador() {
		return reasegurador;
	}

	public void setReasegurador(String reasegurador) {
		this.reasegurador = reasegurador;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CORTE", nullable=false, length=10)
	public Date getfCorte() {
		return fCorte;
	}
	public void setfCorte(Date fCorte) {
		this.fCorte = fCorte;
	}
	
}