package mx.com.afirme.midas2.domain.reaseguro.reporteRR6;


import java.sql.Date;

public class ReporteRTRE {

	
	private static final long serialVersionUID = -7703645329320642555L;
	
	private String clave;
	private String negCubiertos;
	private Integer claveEstrategia;
	private Integer ordenCobertura;
	private String identificador;
	private Date fCorte;
	private String negocio;
	
	public ReporteRTRE(Date fechaCorte) {
		fCorte =  fechaCorte;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getNegCubiertos() {
		return negCubiertos;
	}

	public void setNegCubiertos(String negCubiertos) {
		this.negCubiertos = negCubiertos;
	}

	public Integer getClaveEstrategia() {
		return claveEstrategia;
	}

	public void setClaveEstrategia(Integer claveEstrategia) {
		this.claveEstrategia = claveEstrategia;
	}

	public Integer getOrdenCobertura() {
		return ordenCobertura;
	}

	public void setOrdenCobertura(Integer ordenCobertura) {
		this.ordenCobertura = ordenCobertura;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public Date getfCorte() {
		return fCorte;
	}

	public void setfCorte(Date fCorte) {
		this.fCorte = fCorte;
	}

	public String getNegocio() {
		return negocio;
	}

	public void setNegocio(String negocio) {
		this.negocio = negocio;
	}
	
	
	
}
