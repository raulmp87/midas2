package mx.com.afirme.midas2.domain.reaseguro.reporteRR6;


import java.math.BigDecimal;
import java.sql.Date;

public class ReporteRTRF {

	
	private static final long serialVersionUID = -7703645329320642555L;
	
	private String identificador;
	private String clave;
	private String negCubiertos;
	private String asegurado;
	private String sumaAsegurada;
	private Integer moneda;
	private BigDecimal primaEmitida;
	private BigDecimal primaFacultada;
	private BigDecimal primaCedProporc;
	private BigDecimal primaRetenida;
	private String fechaInicial;
	private String fechaFinal;
	private String tipoContrato;
	private String capas;
	private BigDecimal retPrioridad;
	private String retpriorfian;
	private BigDecimal capacidadMaxReas;
	private String capacidadMaxFian;
	private BigDecimal comision;
	private String partUtilidades;
	private String reasInscrito;
	private String tipoReas;
	private String clvReasNac;
	private String reasNoInscrito;
	private BigDecimal participacionReas;
	private String tipoIntermediario;
	private String clvIntermediario;
	private String intNoAutorizado;
	private String suscriptor;
	private String aclaraciones;
	private String entidades;
	private String municipio;
	private String sector;
	private Date fechaIniCorte;
	private Date fechaFinCorte;
	private String negocio;
	private Integer inciso;
	private Integer idTipoReas;
	private Integer idTipoPolizas;
	
	
	public ReporteRTRF(Date finCorte) {
		fechaFinCorte =  finCorte;
	}
	
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getNegCubiertos() {
		return negCubiertos;
	}
	public void setNegCubiertos(String negCubiertos) {
		this.negCubiertos = negCubiertos;
	}
	public String getAsegurado() {
		return asegurado;
	}
	public void setAsegurado(String asegurado) {
		this.asegurado = asegurado;
	}
	public String getSumaAsegurada() {
		return sumaAsegurada;
	}
	public void setSumaAsegurada(String sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
	public Integer getMoneda() {
		return moneda;
	}
	public void setMoneda(Integer moneda) {
		this.moneda = moneda;
	}
	public BigDecimal getPrimaEmitida() {
		return primaEmitida;
	}
	public void setPrimaEmitida(BigDecimal primaEmitida) {
		this.primaEmitida = primaEmitida;
	}
	public BigDecimal getPrimaFacultada() {
		return primaFacultada;
	}
	public void setPrimaFacultada(BigDecimal primaFacultada) {
		this.primaFacultada = primaFacultada;
	}
	public BigDecimal getPrimaCedProporc() {
		return primaCedProporc;
	}
	public void setPrimaCedProporc(BigDecimal primaCedProporc) {
		this.primaCedProporc = primaCedProporc;
	}
	public BigDecimal getPrimaRetenida() {
		return primaRetenida;
	}
	public void setPrimaRetenida(BigDecimal primaRetenida) {
		this.primaRetenida = primaRetenida;
	}
	public String getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public String getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	public String getTipoContrato() {
		return tipoContrato;
	}
	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}
	public String getCapas() {
		return capas;
	}
	public void setCapas(String capas) {
		this.capas = capas;
	}
	public BigDecimal getRetPrioridad() {
		return retPrioridad;
	}
	public void setRetPrioridad(BigDecimal retPrioridad) {
		this.retPrioridad = retPrioridad;
	}
	public String getRetpriorfian() {
		return retpriorfian;
	}
	public void setRetpriorfian(String retpriorfian) {
		this.retpriorfian = retpriorfian;
	}
	public BigDecimal getCapacidadMaxReas() {
		return capacidadMaxReas;
	}
	public void setCapacidadMaxReas(BigDecimal capacidadMaxReas) {
		this.capacidadMaxReas = capacidadMaxReas;
	}
	public String getCapacidadMaxFian() {
		return capacidadMaxFian;
	}
	public void setCapacidadMaxFian(String capacidadMaxFian) {
		this.capacidadMaxFian = capacidadMaxFian;
	}
	public BigDecimal getComision() {
		return comision;
	}
	public void setComision(BigDecimal comision) {
		this.comision = comision;
	}
	public String getPartUtilidades() {
		return partUtilidades;
	}
	public void setPartUtilidades(String partUtilidades) {
		this.partUtilidades = partUtilidades;
	}
	public String getReasInscrito() {
		return reasInscrito;
	}
	public void setReasInscrito(String reasInscrito) {
		this.reasInscrito = reasInscrito;
	}
	public String getTipoReas() {
		return tipoReas;
	}
	public void setTipoReas(String tipoReas) {
		this.tipoReas = tipoReas;
	}
	public String getClvReasNac() {
		return clvReasNac;
	}
	public void setClvReasNac(String clvReasNac) {
		this.clvReasNac = clvReasNac;
	}
	public String getReasNoInscrito() {
		return reasNoInscrito;
	}
	public void setReasNoInscrito(String reasNoInscrito) {
		this.reasNoInscrito = reasNoInscrito;
	}
	public BigDecimal getParticipacionReas() {
		return participacionReas;
	}
	public void setParticipacionReas(BigDecimal participacionReas) {
		this.participacionReas = participacionReas;
	}
	public String getTipoIntermediario() {
		return tipoIntermediario;
	}
	public void setTipoIntermediario(String tipoIntermediario) {
		this.tipoIntermediario = tipoIntermediario;
	}
	public String getClvIntermediario() {
		return clvIntermediario;
	}
	public void setClvIntermediario(String clvIntermediario) {
		this.clvIntermediario = clvIntermediario;
	}
	public String getIntNoAutorizado() {
		return intNoAutorizado;
	}
	public void setIntNoAutorizado(String intNoAutorizado) {
		this.intNoAutorizado = intNoAutorizado;
	}
	public String getSuscriptor() {
		return suscriptor;
	}
	public void setSuscriptor(String suscriptor) {
		this.suscriptor = suscriptor;
	}
	public String getAclaraciones() {
		return aclaraciones;
	}
	public void setAclaraciones(String aclaraciones) {
		this.aclaraciones = aclaraciones;
	}
	public String getEntidades() {
		return entidades;
	}
	public void setEntidades(String entidades) {
		this.entidades = entidades;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getSector() {
		return sector;
	}
	public void setSector(String sector) {
		this.sector = sector;
	}
	public Date getFechaIniCorte() {
		return fechaIniCorte;
	}
	public void setFechaIniCorte(Date fechaIniCorte) {
		this.fechaIniCorte = fechaIniCorte;
	}
	public Date getFechaFinCorte() {
		return fechaFinCorte;
	}
	public void setFechaFinCorte(Date fechaFinCorte) {
		this.fechaFinCorte = fechaFinCorte;
	}
	public String getNegocio() {
		return negocio;
	}
	public void setNegocio(String negocio) {
		this.negocio = negocio;
	}
	public Integer getInciso() {
		return inciso;
	}
	public void setInciso(Integer inciso) {
		this.inciso = inciso;
	}
	public Integer getIdTipoReas() {
		return idTipoReas;
	}
	public void setIdTipoReas(Integer idTipoReas) {
		this.idTipoReas = idTipoReas;
	}
	public Integer getIdTipoPolizas() {
		return idTipoPolizas;
	}
	public void setIdTipoPolizas(Integer idTipoPolizas) {
		this.idTipoPolizas = idTipoPolizas;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
