package mx.com.afirme.midas2.domain.reaseguro.cfdi;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Embeddable;

@Embeddable
public class ContratoConceptoHistoricoId implements Serializable {

	private static final long serialVersionUID = 6202963552386389147L;

	private BigDecimal contratoId;
	private BigDecimal conceptoId;
	
	public BigDecimal getContratoId() {
		return contratoId;
	}
	public void setContratoId(BigDecimal contratoId) {
		this.contratoId = contratoId;
	}
	public BigDecimal getConceptoId() {
		return conceptoId;
	}
	public void setConceptoId(BigDecimal conceptoId) {
		this.conceptoId = conceptoId;
	}
	
	public ContratoConceptoHistoricoId() {
		
	}
	
}
