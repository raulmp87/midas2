package mx.com.afirme.midas2.domain.migracion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="MigEndososValidos", schema="MIDAS")
public class MigEndososValidos implements Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6935935338629317678L;
	@EmbeddedId
	private MigEndososValidosId id;
	@Column(name="Id_Solicitud")
	private Long idSolicitud;
	@Column(name="IdToPoliza")
	private Long idToPoliza;
	@Column(name="Sit_Endoso")
	private String sitEndoso;
	@Column(name="Fecha_IniVig")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaIniVig;
	@Column(name="UsuarioCreacion")
	private String usuarioCreacion;
	@Column(name="IdToSolicitud")
	private Long idToSolicitud;
	@Column(name="IdCotizacionB")
	private Long idCotizacionB;
	@Column(name="Texto")
	private String texto;
	@Column(name="ClaveProceso")
	private Short claveProceso;

	public static final short CLAVE_PROCESO_NO_PROCESADO = 0;
	public static final short CLAVE_PROCESO_VALIDO = 1;
	public static final short CLAVE_PROCESO_INVALIDO = 2;
	
	
	public MigEndososValidosId getId() {
		return id;
	}
	public void setId(MigEndososValidosId id) {
		this.id = id;
	}
	public Long getIdSolicitud() {
		return idSolicitud;
	}
	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}
	public Long getIdToPoliza() {
		return idToPoliza;
	}
	public void setIdToPoliza(Long idToPoliza) {
		this.idToPoliza = idToPoliza;
	}
	public String getSitEndoso() {
		return sitEndoso;
	}
	public void setSitEndoso(String sitEndoso) {
		this.sitEndoso = sitEndoso;
	}
	public Date getFechaIniVig() {
		return fechaIniVig;
	}
	public void setFechaIniVig(Date fechaIniVig) {
		this.fechaIniVig = fechaIniVig;
	}
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	public Long getIdToSolicitud() {
		return idToSolicitud;
	}
	public void setIdToSolicitud(Long idToSolicitud) {
		this.idToSolicitud = idToSolicitud;
	}
	public Long getIdCotizacionB() {
		return idCotizacionB;
	}
	public void setIdCotizacionB(Long idCotizacionB) {
		this.idCotizacionB = idCotizacionB;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public Short getClaveProceso() {
		return claveProceso;
	}
	public void setClaveProceso(Short claveProceso) {
		this.claveProceso = claveProceso;
	}
	@SuppressWarnings("unchecked")
	@Override
	public MigEndososValidosId getKey() {
		return this.getId();
	}
	@Override
	public String getValue() {
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.getIdSolicitud();
	}	
}
