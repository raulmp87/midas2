package mx.com.afirme.midas2.domain.migracion;

import java.util.Map;

import javax.ejb.Local;

@Local
public interface PkgAutMigracion {

	public Map<String, Object> migraEndoso(MigEndososValidos migEndososValidos);
	public Map<String, Object> migraEndosoEd(MigEndososValidosEd migEndososValidosEd);
	public Map<String, Object> migraPolizasEd();
	public Map<String, Object> ajusteFinalEndoso(Long idCotizacion, Long idVersionPol, Long idSolicitud, Long idToPoliza);
	public Map<String, Object> migraEndososMidas();
}
