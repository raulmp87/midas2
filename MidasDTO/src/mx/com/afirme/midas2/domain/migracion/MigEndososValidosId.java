package mx.com.afirme.midas2.domain.migracion;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MigEndososValidosId  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3447341828809054855L;
	@Column(name="Id_Cotizacion")
	private Long idCotizacion;
	@Column(name="Id_Version_Pol")
	private Long idVersionPol;
	
	public Long getIdCotizacion() {
		return idCotizacion;
	}
	
	public void setIdCotizacion(Long idCotizacion) {
		this.idCotizacion = idCotizacion;
	}
	
	public Long getIdVersionPol() {
		return idVersionPol;
	}
	
	public void setIdVersionPol(Long idVersionPol) {
		this.idVersionPol = idVersionPol;
	}
	
	@Override
	public String toString() {
		return "idCotizacion: " + idCotizacion + " idVersionPol:" + idVersionPol;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idCotizacion == null) ? 0 : idCotizacion.hashCode());
		result = prime * result
				+ ((idVersionPol == null) ? 0 : idVersionPol.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MigEndososValidosId other = (MigEndososValidosId) obj;
		if (idCotizacion == null) {
			if (other.idCotizacion != null)
				return false;
		} else if (!idCotizacion.equals(other.idCotizacion))
			return false;
		if (idVersionPol == null) {
			if (other.idVersionPol != null)
				return false;
		} else if (!idVersionPol.equals(other.idVersionPol))
			return false;
		return true;
	}
}
