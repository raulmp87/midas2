package mx.com.afirme.midas2.domain.contabilidad;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.directwebremoting.annotations.DataTransferObject;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="ING_SOLIC_CHEQUE",schema="SEYCOS")
@DataTransferObject
public class SolicitudCheque implements Serializable, Entidad {

	private static final long serialVersionUID = -2213834432397330361L;

	private BigDecimal id;

	private String status;
	
	private BigDecimal importeNeto;
	
	private Date fechaSolicitud;
	
	private String numeroCheque;

	private Date fechaAplicacion;

	
	@Id
	@Column(name = "ID_SOL_CHEQUE", nullable = false, precision = 22, scale = 0)
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	@Column(name = "SIT_SOL_CHEQUE", nullable = false, length = 1)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "IMP_NETO_CHEQUE", insertable= false, updatable = false, nullable = true, precision = 16, scale = 2)
	public BigDecimal getImporteNeto() {
		return importeNeto;
	}

	public void setImporteNeto(BigDecimal importeNeto) {
		this.importeNeto = importeNeto;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "F_SOLICITUD", insertable= false, updatable = false, nullable = true, length = 7)
	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}

	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	@Column(name = "NUM_CHEQUE", insertable= false, updatable = false, nullable = true, precision = 7, scale = 0)
	public String getNumeroCheque() {
		return numeroCheque;
	}

	public void setNumeroCheque(String numeroCheque) {
		this.numeroCheque = numeroCheque;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "F_APLICACION", insertable= false, updatable = false, nullable = true, length = 7)
	public Date getFechaAplicacion() {
		return fechaAplicacion;
	}

	public void setFechaAplicacion(Date fechaAplicacion) {
		this.fechaAplicacion = fechaAplicacion;
	}

	/**
  																			ID_SOL_CHEQUE        NUMBER(10)               NOT NULL,
  ID_COTIZACION        NUMBER(10)               NOT NULL,
  CVE_TRANS_ORIGEN     CHAR(5 BYTE)             NOT NULL,
  ID_TRANSACCION       NUMBER(12)               NOT NULL,
  ID_LIN_NEGOCIO       NUMBER(5),
  ID_FILIAL            NUMBER(5),
  ID_CATEGORIA         NUMBER(5),
  ID_CERTIFICADO       NUMBER(8),
  CVE_T_TITULO         CHAR(5 BYTE),
  ID_TRANS_CONT        NUMBER(5),
  NUM_POLIZA           NUMBER(10),
  CVEL_CPTO_TRANS      CHAR(5 BYTE),
  																			SIT_SOL_CHEQUE       CHAR(1 BYTE)             NOT NULL,
  																			IMP_NETO_CHEQUE      NUMBER(16,2),
  IMP_ISR              NUMBER(16,2),
  IMP_GASTOS           NUMBER(16,2),
  IMP_INTERESES        NUMBER(16,2),
  IMP_DEDUCIBLE        NUMBER(16,2),
  IMP_COASEGURO        NUMBER(16,2),
  IMP_CHEQUE           NUMBER(16,2),
  																			F_SOLICITUD          DATE,
  ID_USUARIO           CHAR(8 BYTE),
  F_APLICACION         DATE,
  ID_USUARIO_APLIC     CHAR(8 BYTE),
  ID_MONEDA_ORIGEN     NUMBER(3),
  ID_MONEDA            NUMBER(3),
  TIPO_CAMBIO          NUMBER(16,8),
  BENEFICIARIO_CHEQUE  VARCHAR2(100 BYTE),
  																			NUM_CHEQUE           NUMBER(7),
  ID_BANCO             NUMBER(8),
  NUM_CUENTA_CHEQUE    VARCHAR2(20 BYTE),
  TX_CONCEPTO          VARCHAR2(100 BYTE),
  F_ESTIM_PAGO         DATE,
  IMP_IVA              NUMBER(16,2),
  ID_SEC_PAGO          NUMBER(3)                DEFAULT NULL,
  IMP_SALVAMENTO       NUMBER(16,2)
 
	 */
	
	
	
	
	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	
	
}
