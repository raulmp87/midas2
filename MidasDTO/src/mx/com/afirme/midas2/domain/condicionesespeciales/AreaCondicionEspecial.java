package mx.com.afirme.midas2.domain.condicionesespeciales;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * Entidad de Relacion Area- Condicion Especial
 * @author Lizeth De La Garza
 *
 */

@Entity
@Table(name = "TCAREACONDICIONESPECIAL", schema = "MIDAS")
public class AreaCondicionEspecial  implements java.io.Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TCAREACONDICIONESPECIAL_ID_GENERATOR")
	@SequenceGenerator(name="TCAREACONDICIONESPECIAL_ID_GENERATOR",  schema="MIDAS", sequenceName="TCAREACONDICIONESPECIAL_SEQ", allocationSize=1)	
	@Column(name = "ID")
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CONDICIONESPECIAL_ID", referencedColumnName="ID")
	private CondicionEspecial condicionEspecial;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="AREAIMPACTO_ID", referencedColumnName="ID")
	private AreaImpacto areaImpacto;
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="area", orphanRemoval=true)
	private List<FactorAreaCondEsp> factores;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CondicionEspecial getCondicionEspecial() {
		return condicionEspecial;
	}

	public void setCondicionEspecial(CondicionEspecial condicionEspecial) {
		this.condicionEspecial = condicionEspecial;
	}

	public AreaImpacto getAreaImpacto() {
		return areaImpacto;
	}

	public void setAreaImpacto(AreaImpacto areaImpacto) {
		this.areaImpacto = areaImpacto;
	}	

	public List<FactorAreaCondEsp> getFactores() {
		return factores;
	}

	public void setFactores(List<FactorAreaCondEsp> factores) {
		this.factores = factores;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
	
	

}
