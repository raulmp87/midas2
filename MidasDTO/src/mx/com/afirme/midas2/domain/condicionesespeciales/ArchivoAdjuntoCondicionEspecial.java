package mx.com.afirme.midas2.domain.condicionesespeciales;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

@Entity
@Table(name = "TOCONDESPARCHIVOADJUNTO", schema = "MIDAS")
public class ArchivoAdjuntoCondicionEspecial implements Entidad {

	private static final long serialVersionUID = -774341768164057182L;

	@Id	
	@SequenceGenerator(name = "IDTOCONDESPARCHIVOADJUNTO_SEQ_GENERADOR", allocationSize = 1, schema="MIDAS", sequenceName = "IDTOCONDESPARCHIVOADJUNTO_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDTOCONDESPARCHIVOADJUNTO_SEQ_GENERADOR")	  	
	@Column(name="ID")
	private Long id;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinFetch(JoinFetchType.INNER)
	@JoinColumn(name = "IDTOCONTROLARCHIVO", nullable = false, updatable = false)
	private ControlArchivoDTO controlArchivo;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHACREACION")
	private Date fechaCreacion;
	
	@Column(name = "CODIGOUSUARIOCREACION", nullable = false, length = 8)
	private String codigoUsuarioCreacion;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinFetch(JoinFetchType.INNER)
	@JoinColumn(name = "IDCONDICIONESPECIAL", nullable = false, updatable = false)
	private CondicionEspecial condicionEspecial;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ControlArchivoDTO getControlArchivo() {
		return controlArchivo;
	}

	public void setControlArchivo(ControlArchivoDTO controlArchivo) {
		this.controlArchivo = controlArchivo;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
	
	public CondicionEspecial getCondicionEspecial() {
		return condicionEspecial;
	}

	public void setCondicionEspecial(CondicionEspecial condicionEspecial) {
		this.condicionEspecial = condicionEspecial;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {		
		return controlArchivo.getValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return id;
	}

}
