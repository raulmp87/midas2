package mx.com.afirme.midas2.domain.condicionesespeciales;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * Entidad de Factor
 * @author Lizeth De La Garza
 *
 */

@Entity
@Table(name = "TCFACTOR", schema = "MIDAS")
public class Factor  implements java.io.Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TCFACTOR_ID_GENERATOR")
	@SequenceGenerator(name="TCFACTOR_ID_GENERATOR", schema="MIDAS",sequenceName="TCFACTOR_SEQ", allocationSize=1)	
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	private Long id;
	
	@Column(name = "NOMBRE", nullable = false, length = 250)
	private String nombre;
	
	@Column(name = "CODIGO", nullable = false, length = 3)
	private String codigo;
	
	@Column(name = "DESCRIPCION", nullable = false, length = 500)
	private String descripcion;	
	
	@Column(name = "TIPO")
	private Short tipo;	
	
	@Column(name = "TAMANO")
	private Short size;
	
	@Column(name = "JSEVENT", length = 100)
	private String event;
	
	@Column(name = "TIPO_LISTADO", length = 3)
	private String tipoListado;

	@Column(name = "ACTIVO", nullable = false,  precision = 1, scale = 0)
	private Short activo;
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="factor")
	private List<ValorFactor>  valoresFactor = new ArrayList<ValorFactor>();
	
	public enum TIPO_LISTADO {
		VALOR_FACTOR((short) 1, "VAL"),
		CAT_VALOR_FIJO((short) 2, "CAT"),
		TIPO_PRESTADOR((short) 3, "PRE"),;

		private short value;
		
		private String valor;

		TIPO_LISTADO(short value, String valor) {
			this.value = value;
			this.valor = valor;
		}

		public short getValue() {
			return value;
		}

		public void setValue(short value) {
			this.value = value;
		}

		public String getValor() {
			return valor;
		}

		public void setValor(String valor) {
			this.valor = valor;
		}	
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}	

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}	
	
	public Short getTipo() {
		return tipo;
	}

	public void setTipo(Short tipo) {
		this.tipo = tipo;
	}

	public Short getSize() {
		return size;
	}

	public void setSize(Short size) {
		this.size = size;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}	

	public String getTipoListado() {
		return tipoListado;
	}

	public void setTipoListado(String tipoListado) {
		this.tipoListado = tipoListado;
	}

	public Short getActivo() {
		return activo;
	}

	public void setActivo(Short activo) {
		this.activo = activo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}

	public List<ValorFactor> getValoresFactor() {
		return valoresFactor;
	}

	public void setValoresFactor(List<ValorFactor> valoresFactor) {
		this.valoresFactor = valoresFactor;
	}
	

}
