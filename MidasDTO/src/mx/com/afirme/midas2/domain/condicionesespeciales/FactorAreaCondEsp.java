package mx.com.afirme.midas2.domain.condicionesespeciales;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * Entidad de Relacion Area de Condicion Especial- Factor
 * @author Lizeth De La Garza
 *
 */

@Entity
@Table(name = "TCFACTORAREACONDESP", schema = "MIDAS")
public class FactorAreaCondEsp  implements java.io.Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TCFACTORAREACONDESP_ID_GENERATOR")
	@SequenceGenerator(name="TCFACTORAREACONDESP_ID_GENERATOR", schema="MIDAS", sequenceName="TCFACTORAREACONDESP_SEQ", allocationSize=1)	
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "VALOR", length = 50)
	private String valor;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="AREACONDICIONESPECIAL_ID", referencedColumnName="ID")
	private AreaCondicionEspecial area;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="FACTOR_ID", referencedColumnName="ID")
	private Factor factor;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}	

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public AreaCondicionEspecial getArea() {
		return area;
	}

	public void setArea(AreaCondicionEspecial area) {
		this.area = area;
	}

	public Factor getFactor() {
		return factor;
	}

	public void setFactor(Factor factor) {
		this.factor = factor;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
	
	

}
