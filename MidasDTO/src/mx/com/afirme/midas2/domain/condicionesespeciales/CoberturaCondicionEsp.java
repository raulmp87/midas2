package mx.com.afirme.midas2.domain.condicionesespeciales;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * Entidad de Relacion Condicion Especial - Cobertura
 * @author Lizeth De La Garza
 *
 */

@Entity
@Table(name = "TCCOBERTURACONDESP", schema = "MIDAS")
public class CoberturaCondicionEsp  implements java.io.Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TCCOBERTURACONDESP_ID_GENERATOR")
	@SequenceGenerator(name="TCCOBERTURACONDESP_ID_GENERATOR", schema="MIDAS", sequenceName="TCCOBERTURACONDESP_SEQ", allocationSize=5)	
	@Column(name = "ID")
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CONDICIONESPECIAL_ID", referencedColumnName="ID")
	private CondicionEspecial condicionEspecial;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SECCION_ID", referencedColumnName="IDTOSECCION")
	private SeccionDTO seccion;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="COBERTURA_ID", referencedColumnName="IDTOCOBERTURA")
	private CoberturaDTO cobertura;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}	

	public CondicionEspecial getCondicionEspecial() {
		return condicionEspecial;
	}

	public void setCondicionEspecial(CondicionEspecial condicionEspecial) {
		this.condicionEspecial = condicionEspecial;
	}

	public SeccionDTO getSeccion() {
		return seccion;
	}

	public void setSeccion(SeccionDTO seccion) {
		this.seccion = seccion;
	}

	public CoberturaDTO getCobertura() {
		return cobertura;
	}

	public void setCobertura(CoberturaDTO cobertura) {
		this.cobertura = cobertura;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
	
	

}
