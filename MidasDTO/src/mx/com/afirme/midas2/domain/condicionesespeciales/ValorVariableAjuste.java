package mx.com.afirme.midas2.domain.condicionesespeciales;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * Entidad de Relacion de los Valores de una Variable de Ajuste
 * @author Lizeth De La Garza
 *
 */

@Entity
@Table(name = "TCVALORVARIABLEAJUSTE", schema = "MIDAS")
public class ValorVariableAjuste  implements java.io.Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TCVALORVARIABLEAJUSTE_ID_GENERATOR")
	@SequenceGenerator(name="TCVALORVARIABLEAJUSTE_ID_GENERATOR", schema="MIDAS", sequenceName="TCVALORVARIABLEAJUSTE_SEQ", allocationSize=1)	
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "NOMBRE", nullable = false, length = 50)
	private String nombre;
	
	@Column(name = "VALOR", nullable = false, length = 50)
	private String valor;
	
	@Column(name = "ORDEN", nullable = false, precision = 1, scale = 0)
	private Short orden;
	
	@Column(name = "JSEVENT", length = 50)
	private String event;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VARIABLEAJUSTE_ID", referencedColumnName="ID")
	private VariableAjuste variableAjuste;
	
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}	

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public Short getOrden() {
		return orden;
	}

	public void setOrden(Short orden) {
		this.orden = orden;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public VariableAjuste getVariableAjuste() {
		return variableAjuste;
	}

	public void setVariableAjuste(VariableAjuste variableAjuste) {
		this.variableAjuste = variableAjuste;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
	
	

}
