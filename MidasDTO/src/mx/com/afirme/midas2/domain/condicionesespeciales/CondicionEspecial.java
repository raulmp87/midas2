package mx.com.afirme.midas2.domain.condicionesespeciales;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.catalogos.EnumBase;
import mx.com.afirme.midas2.util.EnumUtil;

/**
 * Entidad de Condicion Especial
 * @author Lizeth De La Garza
 *
 */

@Entity
@Table(name = "TCCONDICIONESPECIAL", schema = "MIDAS")
public class CondicionEspecial implements Entidad {

	private static final long serialVersionUID = 1L;
	
	public static enum EstatusCondicion implements EnumBase<Short>{
		PROCESO((short)0, "En proceso"),  ALTA((short)1, "Activo"),  BAJA((short)2, "Inactivo");		
		
		EstatusCondicion(Short estatus, String label) {
			this.estatus = estatus;
			this.label = label;
		}
		private Short estatus;
		
		private String label;
		
		@Override
		public Short getValue() {
			return estatus;
		}

		@Override
		public String getLabel() {
			return label;
		}

	};		
	
	public static enum NivelAplicacion{
		POLIZA((short)0),  INCISO((short)1),  TODAS((short)2);		
		
		NivelAplicacion(Short nivel) {
			this.nivel = nivel;
		}
		private Short nivel;
		
		public Short getNivel() {
			return nivel;
		}
		public void setNivel(Short nivel) {
			this.nivel = nivel;
		}		
	};		
	
	public static enum NivelImportancia{
		BAJA((short)0),  MEDIA((short)1),  ALTA((short)2);		
		
		NivelImportancia(Short nivel) {
			this.nivel = nivel;
		}
		private Short nivel;
		
		public Short getNivel() {
			return nivel;
		}
		public void setNivel(Short nivel) {
			this.nivel = nivel;
		}
		
	};		

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TCCONDICIONESPECIAL_ID_GENERATOR")
	@SequenceGenerator(name="TCCONDICIONESPECIAL_ID_GENERATOR", schema="MIDAS", sequenceName="TCCONDICIONESPECIAL_SEQ", allocationSize=1)	
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "NOMBRE", nullable = false, length = 250)
	private String nombre;

	@Column(name = "CODIGO", nullable = false)
	private Long codigo;
	
	@Column(name = "DESCRIPCION", nullable = false)
	private String descripcion;	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAREGISTRO", nullable = false, length = 7)
	private Date fechaRegistro;	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHAALTA",  length = 7)
	private Date fechaAlta;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHABAJA", length = 7)
	private Date fechaBaja;
	
	@Column(name = "ESTATUS", nullable = false, precision = 1, scale = 0)
	private Short estatus;
	
	@Column(name = "NIVELAPLICACION",  precision = 1, scale = 0)
	private Short nivelAplicacion;
	
	@Column(name = "NIVELIMPORTANCIA", precision = 1, scale = 0)
	private Short nivelImportancia;
	
	@Column(name = "VIP")
	private Boolean vip;
	
	@Column(name = "CODIGOUSUARIO",  length = 50)
	private String codigoUsuario;
	
	@OneToMany( fetch = FetchType.LAZY, mappedBy = "condicionEspecial")
	private List<ArchivoAdjuntoCondicionEspecial> archivos;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "TOCONDICIONESPCOTIZACION", schema = "MIDAS",
			joinColumns = {@JoinColumn(name="CONDICIONESPECIAL_ID", referencedColumnName="ID")},
			inverseJoinColumns = {@JoinColumn(name="COTIZACION_ID", referencedColumnName="IDTOCOTIZACION")}
	)
	private List<CotizacionDTO> cotizaciones;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "TOCONDICIONESPINCISO", schema = "MIDAS",
			joinColumns = {@JoinColumn(name="CONDICIONESPECIAL_ID", referencedColumnName="ID")}, 
			inverseJoinColumns = {@JoinColumn(name="COTIZACION_ID", referencedColumnName="IDTOCOTIZACION"),
			   					  @JoinColumn(name="NUMEROINCISO", referencedColumnName="NUMEROINCISO")}
	)
	private List<IncisoCotizacionDTO> incisos;
	
	@Transient
	private Long ranking;
	
	@Transient
	private String codigoDescripcion;
	
	@Transient
	private String nivelAplicacionStr;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {		
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Date getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public Short getEstatus() {
		return estatus;
	}

	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}

	public Short getNivelAplicacion() {
		return nivelAplicacion;
	}

	public void setNivelAplicacion(Short nivelAplicacion) {
		this.nivelAplicacion = nivelAplicacion;
	}

	public Short getNivelImportancia() {
		return nivelImportancia;
	}

	public void setNivelImportancia(Short nivelImportancia) {
		this.nivelImportancia = nivelImportancia;
	}

	public Boolean getVip() {
		return vip;
	}

	public void setVip(Boolean vip) {
		this.vip = vip;
	}

	public String getCodigoUsuario() {
		return codigoUsuario;
	}

	public void setCodigoUsuario(String codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}

	public Long getRanking() {
		return ranking;
	}

	public void setRanking(Long ranking) {
		this.ranking = ranking;
	}

	public String getCodigoDescripcion() {
		return codigoDescripcion;
	}

	public void setCodigoDescripcion(String codigoDescripcion) {
		this.codigoDescripcion = codigoDescripcion;
	}
	
	@Transient
	public String getEstatusDescripcion() {
		return EnumUtil.getLabel(EstatusCondicion.class, estatus);
	}
	
	public List<ArchivoAdjuntoCondicionEspecial> getArchivos() {
		return archivos;
	}

	public void setArchivos(List<ArchivoAdjuntoCondicionEspecial> archivos) {
		this.archivos = archivos;
	}

	public List<CotizacionDTO> getCotizaciones() {
		return cotizaciones;
	}

	public void setCotizaciones(List<CotizacionDTO> cotizaciones) {
		this.cotizaciones = cotizaciones;
	}

	public List<IncisoCotizacionDTO> getIncisos() {
		return incisos;
	}

	public void setIncisos(List<IncisoCotizacionDTO> incisos) {
		this.incisos = incisos;
	}
	
	/**
	 * @param nivelAplicacionStr the nivelAplicacionStr to set
	 */
	public void setNivelAplicacionStr(String nivelAplicacionStr) {
		this.nivelAplicacionStr = nivelAplicacionStr;
	}

	/**
	 * @return the nivelAplicacionStr
	 */
	public String getNivelAplicacionStr() {
		return nivelAplicacionStr;
	}
	
	@Transient
	public String getCodigoNombre() {
		String nombreCodigo = this.codigo + " - " + this.nombre;
		return nombreCodigo;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CondicionEspecial other = (CondicionEspecial) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
