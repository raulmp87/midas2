package mx.com.afirme.midas2.domain.condicionesespeciales;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.OrderBy;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * Entidad de Relacion Variable de Ajuste - Condicion Especial
 * @author Lizeth De La Garza
 *
 */

@Entity
@Table(name = "TCVARAJUSTECONDESP", schema = "MIDAS")
public class VarAjusteCondicionEsp  implements java.io.Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TCVARAJUSTECONDESP_ID_GENERATOR")
	@SequenceGenerator(name="TCVARAJUSTECONDESP_ID_GENERATOR", schema="MIDAS", sequenceName="TCVARAJUSTECONDESP_SEQ", allocationSize=1)	
	@Column(name = "ID")
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CONDICIONESPECIAL_ID", referencedColumnName="ID")
	private CondicionEspecial condicionEspecial;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VARIABLEAJUSTE_ID", referencedColumnName="ID")
	private VariableAjuste variableAjuste;
	
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="varAjusteCondicion", orphanRemoval=true)
	@OrderBy("orden ASC")
	private List<ValorVarAjusteCondicionEsp> valores;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}	

	public CondicionEspecial getCondicionEspecial() {
		return condicionEspecial;
	}

	public void setCondicionEspecial(CondicionEspecial condicionEspecial) {
		this.condicionEspecial = condicionEspecial;
	}

	public VariableAjuste getVariableAjuste() {
		return variableAjuste;
	}

	public void setVariableAjuste(VariableAjuste variableAjuste) {
		this.variableAjuste = variableAjuste;
	}

	public List<ValorVarAjusteCondicionEsp> getValores() {
		return valores;
	}

	public void setValores(List<ValorVarAjusteCondicionEsp> valores) {
		this.valores = valores;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
	
	

}
