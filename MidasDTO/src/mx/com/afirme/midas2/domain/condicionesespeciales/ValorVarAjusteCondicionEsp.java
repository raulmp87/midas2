package mx.com.afirme.midas2.domain.condicionesespeciales;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * Entidad de Relacion de los Valores de una Variable de Ajuste
 * @author Lizeth De La Garza
 *
 */

@Entity
@Table(name = "TCVALORVARAJUSTECONDESP", schema = "MIDAS")
public class ValorVarAjusteCondicionEsp  implements java.io.Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TCVALORVARAJUSTECONDESP_ID_GENERATOR")
	@SequenceGenerator(name="TCVALORVARAJUSTECONDESP_ID_GENERATOR", schema="MIDAS", sequenceName="TCVALORVARAJUSTECONDESP_SEQ", allocationSize=1)	
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "VALOR", nullable = false, length = 50)
	private String valor;
	
	@Column(name = "ORDEN", nullable = false, precision = 1, scale = 0)
	private Short orden;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VARAJUSTECONDESP_ID", referencedColumnName="ID")
	private VarAjusteCondicionEsp varAjusteCondicion;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VALORVARIABLEAJUSTE_ID", referencedColumnName="ID")
	private ValorVariableAjuste valorVariable;
	
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}	

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public Short getOrden() {
		return orden;
	}

	public void setOrden(Short orden) {
		this.orden = orden;
	}
	

	public VarAjusteCondicionEsp getVarAjusteCondicion() {
		return varAjusteCondicion;
	}

	public void setVarAjusteCondicion(VarAjusteCondicionEsp varAjusteCondicion) {
		this.varAjusteCondicion = varAjusteCondicion;
	}	

	public ValorVariableAjuste getValorVariable() {
		return valorVariable;
	}

	public void setValorVariable(ValorVariableAjuste valorVariable) {
		this.valorVariable = valorVariable;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
	
	

}
