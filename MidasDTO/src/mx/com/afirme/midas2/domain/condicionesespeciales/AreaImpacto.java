package mx.com.afirme.midas2.domain.condicionesespeciales;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * Entidad de Area de Impacto
 * @author Lizeth De La Garza
 *
 */

@Entity
@Table(name = "TCAREAIMPACTO", schema = "MIDAS")
public class AreaImpacto  implements java.io.Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TCAREAIMPACTO_ID_GENERATOR")
	@SequenceGenerator(name="TCAREAIMPACTO_ID_GENERATOR",  schema="MIDAS", sequenceName="TCAREAIMPACTO_SEQ", allocationSize=1)	
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "CODIGO", nullable = false, length = 4)
	private String codigo;
	
	@Column(name = "NOMBRE", nullable = false, length = 250)
	private String nombre;

	
	@Column(name = "DESCRIPCION", nullable = false, length = 500)
	private String descripcion;	
	

	@Column(name = "ACTIVO", nullable = false,  precision = 1, scale = 0)
	private Short activo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}	

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}	
	
	public Short getActivo() {
		return activo;
	}

	public void setActivo(Short activo) {
		this.activo = activo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
	

}
