
package mx.com.afirme.midas2.domain.condicionesespeciales;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * Entidad de Factor
 * @author Lizeth De La Garza
 *
 */

@Entity
@Table(name = "TCVALORFACTOR", schema = "MIDAS")
public class ValorFactor  implements java.io.Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TCVALORFACTOR_ID_GENERATOR")
	@SequenceGenerator(name="TCVALORFACTOR_ID_GENERATOR", schema="MIDAS", sequenceName="TCVALORFACTOR_SEQ", allocationSize=1)	
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	private Long id;
	
	@Column(name = "NOMBRE", nullable = false, length = 50)
	private String nombre;
	
	@Column(name = "ORDEN", precision = 2, scale = 0)
	private Short orden;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="FACTOR_ID", referencedColumnName="ID")
	private Factor factor;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public Short getOrden() {
		return orden;
	}

	public void setOrden(Short orden) {
		this.orden = orden;
	}

	public Factor getFactor() {
		return factor;
	}

	public void setFactor(Factor factor) {
		this.factor = factor;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
	

}
