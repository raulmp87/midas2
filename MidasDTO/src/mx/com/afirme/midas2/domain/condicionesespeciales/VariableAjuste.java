package mx.com.afirme.midas2.domain.condicionesespeciales;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * Entidad de Variable Ajuste
 * @author Lizeth De La Garza
 *
 */

@Entity
@Table(name = "TCVARIABLEAJUSTE", schema = "MIDAS")
public class VariableAjuste  implements java.io.Serializable, Entidad {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TCVARIABLEAJUSTE_ID_GENERATOR")
	@SequenceGenerator(name="TCVARIABLEAJUSTE_ID_GENERATOR", schema="MIDAS", sequenceName="TCVARIABLEAJUSTE_SEQ", allocationSize=1)	
	@Column(name = "ID")
	private Long id;
	
	public enum COMPONENT_TYPE {
		TEXT((short) 1),
		DATE((short) 2),
		DATE_RANGE((short) 3),
		TIME((short) 4),
		TIME_RANGE((short) 5),
		RADIOBUTTON((short) 6),
		COMBOLIST((short) 7),
		CHECKBOX((short) 8),
		NUMERIC((short) 9),
		NUMERIC_RANGE((short) 10);

		private short value;

		COMPONENT_TYPE(short value) {
			this.value = value;
		}

		public short getValue() {
			return value;
		}

		public void setValue(short value) {
			this.value = value;
		}		
	}	
	
	public enum TIPO_LISTADO {
		VALOR_VARIABLE((short) 1, "VAL"),
		ESTADO((short) 2, "EST"),
		CAT_VALOR_FIJO((short) 3, "CAT");

		private short value;
		
		private String valor;

		TIPO_LISTADO(short value, String valor) {
			this.value = value;
			this.valor = valor;
		}

		public short getValue() {
			return value;
		}

		public void setValue(short value) {
			this.value = value;
		}

		public String getValor() {
			return valor;
		}

		public void setValor(String valor) {
			this.valor = valor;
		}	
		
	}
	
	@Column(name = "NOMBRE", nullable = false, length = 50)
	private String nombre;
	
	@Column(name = "CODIGO", nullable = false, length = 20)
	private String codigo;
	
	@Column(name = "DESCRIPCION", nullable = false, length = 100)
	private String descripcion;	
	
	@Column(name = "TIPO", nullable = false)
	private Short tipo;
	
	@Column(name = "PROPERTYNAME", length = 100)
	private String propertyName;	
	
	@Column(name = "TIPO_LISTADO", length = 3)
	private String tipoListado;
	
	@Column(name = "GRUPO_CATALOGO_FIJO", length = 80)
	private String grupoCatalogo;
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="variableAjuste")
	private List<ValorVariableAjuste>  valores = new ArrayList<ValorVariableAjuste>();
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}	
	
	public Short getTipo() {
		return tipo;
	}

	public void setTipo(Short tipo) {
		this.tipo = tipo;
	}	

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}	

	public String getTipoListado() {
		return tipoListado;
	}

	public void setTipoListado(String tipoListado) {
		this.tipoListado = tipoListado;
	}

	public String getGrupoCatalogo() {
		return grupoCatalogo;
	}

	public void setGrupoCatalogo(String grupoCatalogo) {
		this.grupoCatalogo = grupoCatalogo;
	}

	public List<ValorVariableAjuste> getValores() {
		return valores;
	}

	public void setValores(List<ValorVariableAjuste> valores) {
		this.valores = valores;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
	
	

}
