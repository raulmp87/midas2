package mx.com.afirme.midas2.domain.coberturas.kilometros;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name = "SOLCOBERTURAADICIONAL", schema = "MIDAS")
public class SolicitudCoberturaAdicionalDTO implements Serializable, Entidad {

	private static final long serialVersionUID = 1L;
	private Long id;
	private BigDecimal idToSolicitud;
	private BigDecimal idToPoliza;
	private Date fechaHoraInicio;
	private Date fechaHoraFin;
	private BigDecimal valorPrimaCobertura;
	private Short numeroEndoso;
	private BigDecimal distanciaTotal;
	private String observacionesInciso;
	private String estatus;
	
	public enum Estatus {
		PENDIENTE("PENDIENTE"), CONTRATADO("CONTRATADO"), NOCONTRATADO("NOCONTRATADO");
		private String valor;

		private Estatus(String valor) {
			this.valor = valor;
		}

		public String getValor() {
			return this.valor;
		}
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SOLCOBERTURAADICIONAL_SEQ")
	@SequenceGenerator(name = "SOLCOBERTURAADICIONAL_SEQ", schema="MIDAS", sequenceName = "SOLCOBERTURAADICIONAL_SEQ", allocationSize = 1)
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return id;
	}

	public void setId(Long idSolicitudCoberturaAdicional) {
		this.id = idSolicitudCoberturaAdicional;
	}

	@Column(name = "IDTOSOLICITUD", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToSolicitud() {
		return idToSolicitud;
	}

	public void setIdToSolicitud(BigDecimal idToSolicitud) {
		this.idToSolicitud = idToSolicitud;
	}

	@Column(name = "IDTOPOLIZA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}
	
	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAHORAINICIO")
	public Date getFechaHoraInicio() {
		return fechaHoraInicio;
	}

	public void setFechaHoraInicio(Date fechaHoraInicio) {
		this.fechaHoraInicio = fechaHoraInicio;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAHORAFIN")
	public Date getFechaHoraFin() {
		return fechaHoraFin;
	}

	public void setFechaHoraFin(Date fechaHoraFin) {
		this.fechaHoraFin = fechaHoraFin;
	}

	@Column(name = "VALORPRIMACOBERTURA", nullable = false, precision = 22, scale = 0)
	public BigDecimal getValorPrimaCobertura() {
		return valorPrimaCobertura;
	}

	public void setValorPrimaCobertura(BigDecimal valorPrimaCobertura) {
		this.valorPrimaCobertura = valorPrimaCobertura;
	}
	
	@Column(name = "NUMEROENDOSO", nullable = false, precision = 4, scale = 0)
	public Short getNumeroEndoso() {
		return this.numeroEndoso;
	}

	public void setNumeroEndoso(Short numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}
	
	@Column(name = "DISTANCIATOTAL", nullable = false, precision = 22, scale = 0)
	public BigDecimal getDistanciaTotal() {
		return distanciaTotal;
	}

	public void setDistanciaTotal(BigDecimal distanciaTotal) {
		this.distanciaTotal = distanciaTotal;
	}
	
	@Column(name="OBSERVACIONESINCISO")
	public String getObservacionesInciso() {
		return observacionesInciso;
	}

	public void setObservacionesInciso(String observacionesInciso) {
		this.observacionesInciso = observacionesInciso;
	}

	@Column(name = "ESTATUS", nullable = false, length = 50)
	public String getEstatus() {
		return this.estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	
	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long getBusinessKey() {
		return  id;
	}

}