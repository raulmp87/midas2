package mx.com.afirme.midas2.domain.coberturas.kilometros;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class GeneraSolicitudRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String numeroPoliza;
	private BigDecimal distanciaTotal;
	private Date fechaHoraInicio;
	private Date fechaHoraFin;
	private boolean contratar;
	private BigDecimal numeroSolicitud;
	
	
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public BigDecimal getDistanciaTotal() {
		return distanciaTotal;
	}
	public void setDistanciaTotal(BigDecimal distanciaTotal) {
		this.distanciaTotal = distanciaTotal;
	}
	public Date getFechaHoraInicio() {
		return fechaHoraInicio;
	}
	public void setFechaHoraInicio(Date fechaHoraInicio) {
		this.fechaHoraInicio = fechaHoraInicio;
	}
	public Date getFechaHoraFin() {
		return fechaHoraFin;
	}
	public void setFechaHoraFin(Date fechaHoraFin) {
		this.fechaHoraFin = fechaHoraFin;
	}
	public boolean getContratar() {
		return contratar;
	}
	public void setContratar(boolean contratar) {
		this.contratar = contratar;
	}
	public BigDecimal getNumeroSolicitud() {
		return numeroSolicitud;
	}
	public void setNumeroSolicitud(BigDecimal numeroSolicitud) {
		this.numeroSolicitud = numeroSolicitud;
	}
}