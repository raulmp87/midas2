package mx.com.afirme.midas2.domain.coberturas.kilometros;

import java.math.BigDecimal;
import java.io.Serializable;

public class GeneraSolicitudResponse implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal numeroSolicitud;
	private BigDecimal valorPrimaCobertura;
	
	
	public BigDecimal getNumeroSolicitud() {
		return numeroSolicitud;
	}
	public void setNumeroSolicitud(BigDecimal numeroSolicitud) {
		this.numeroSolicitud = numeroSolicitud;
	}
	public BigDecimal getValorPrimaCobertura() {
		return valorPrimaCobertura;
	}
	public void setValorPrimaCobertura(BigDecimal valorPrimaCobertura) {
		this.valorPrimaCobertura = valorPrimaCobertura;
	}
}