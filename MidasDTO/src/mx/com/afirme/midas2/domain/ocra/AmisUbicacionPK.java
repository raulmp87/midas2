package mx.com.afirme.midas2.domain.ocra;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class AmisUbicacionPK implements Serializable {
	
	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;
	
	@Column(name = "ID_PAIS", nullable = false)
	private Long idPais;
	
	@Column(name = "ID_ESTADO", nullable = false)
	private Long idEstado;
	
	@Column(name = "ID_MUNICIPIO", nullable = false)
	private Long idMunicipio;
	
	public AmisUbicacionPK(){}
	
	public AmisUbicacionPK(Long xidPais, Long xidEstado, Long xidMunicipio){
		this.idPais   	 = xidPais;
		this.idEstado 	 = xidEstado;
		this.idMunicipio = xidMunicipio;
	}
	

	public Long getIdPais() {
		return idPais;
	}
	public Long getIdEstado() {
		return idEstado;
	}
	public Long getIdMunicipio() {
		return idMunicipio;
	}
	public void setIdPais(Long idPais) {
		this.idPais = idPais;
	}
	public void setIdEstado(Long idEstado) {
		this.idEstado = idEstado;
	}
	public void setIdMunicipio(Long idMunicipio) {
		this.idMunicipio = idMunicipio;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idEstado == null) ? 0 : idEstado.hashCode());
				
		result = prime * result + ((idPais == null) ? 0 : idPais.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AmisUbicacionPK other = (AmisUbicacionPK) obj;
		if (idEstado == null) {
			if (other.idEstado != null)
				return false;
		} else if (!idEstado.equals(other.idEstado))
			return false;
		if (idPais == null) {
			if (other.idPais != null)
				return false;
		} else if (!idPais.equals(other.idPais))
			return false;
		return true;
	}
	
	

}
