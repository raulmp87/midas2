package mx.com.afirme.midas2.domain.ocra;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;


@Entity
@Table(name = "TOOCRAENVIOLOG", schema = "MIDAS") 
public class EnvioOcraLog implements Entidad {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4968215987404495291L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTCOCRAENVIO_SEQ")
	@SequenceGenerator(name="IDTCOCRAENVIO_SEQ", schema="MIDAS", sequenceName="IDTCOCRAENVIO_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;
	
	@Column(name="MSN_ENVIO")
	private String msnEnvio;
	
	@Column(name="MSN_RESPUESTA")
	private String msnRespuesta;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_ENVIO")
	private Date fechaEnvio;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_RESPUESTA")
	private Date fechaRespuesta;
	
	@Column(name="REPORTE_CABINA_ID")
	private Long reporteCabinaId;
	
	@Column(name="OBSERVACIONES")
	private String observaciones;
	
	
	public EnvioOcraLog(String msnEnvio, String msnRespuesta, Date fechaEnvio,Date fechaRespuesta,Long reporteCabinaId, String observaciones) {
		super();
		this.msnEnvio 		 = msnEnvio;
		this.msnRespuesta 	 = msnRespuesta;
		this.fechaEnvio 	 = fechaEnvio;
		this.fechaRespuesta  = fechaRespuesta;
		this.reporteCabinaId = reporteCabinaId;
		this.observaciones   = observaciones;
	}
	
	public EnvioOcraLog(){}
	
	public Long getId() {
		return id;
	}
	public String getMsnEnvio() {
		return msnEnvio;
	}
	public String getMsnRespuesta() {
		return msnRespuesta;
	}
	public Date getFechaEnvio() {
		return fechaEnvio;
	}
	public Date getFechaRespuesta() {
		return fechaRespuesta;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setMsnEnvio(String msnEnvio) {
		this.msnEnvio = msnEnvio;
	}
	public void setMsnRespuesta(String msnRespuesta) {
		this.msnRespuesta = msnRespuesta;
	}
	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}
	public void setFechaRespuesta(Date fechaRespuesta) {
		this.fechaRespuesta = fechaRespuesta;
	}

	@Override
	public Long getKey() {
		return this.getId();
	}

	@Override
	public String getValue() {
		return this.toString();
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toString() {
		return "EnvioOcraLog [id=" + id + ", msnEnvio=" + msnEnvio
				+ ", msnRespuesta=" + msnRespuesta + ", fechaEnvio="
				+ fechaEnvio + ", fechaRespuesta=" + fechaRespuesta
				+ ", reporteCabinaId=" + reporteCabinaId + ", observaciones="
				+ observaciones + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EnvioOcraLog other = (EnvioOcraLog) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getReporteCabinaId() {
		return reporteCabinaId;
	}

	public void setReporteCabinaId(Long reporteCabinaId) {
		this.reporteCabinaId = reporteCabinaId;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	
	

}
