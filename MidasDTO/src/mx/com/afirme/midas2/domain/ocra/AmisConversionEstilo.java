package mx.com.afirme.midas2.domain.ocra;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name = "SAP_AMIS_CONVERSIONESTILO", schema = "MIDAS")

public class AmisConversionEstilo implements Entidad {
	
	
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	@AttributeOverrides( {
		@AttributeOverride(name = "amisTipoTransporteId", column = @Column(name = "AMISTIPOTRANSPORTE_ID", nullable = false )),
		@AttributeOverride(name = "amisMarcaId",          column = @Column(name = "AMISMARCA_ID",          nullable = false )),
		@AttributeOverride(name = "amisSubMarcaId",       column = @Column(name = "AMISSUBMARCA_ID",       nullable = false )),
		@AttributeOverride(name = "midasMarcaId",         column = @Column(name = "MIDASMARCA_ID",         nullable = false )),
		@AttributeOverride(name = "midasEstiloId",        column = @Column(name = "MIDASESTILO_ID",        nullable = false ))
	} )
	private AmisConversionEstiloPK id;
	
	@Column(name = "MIDASMARCA_ID", nullable=false)
	private Long   midasMarcaId;
	
	@Column(name = "MIDASESTILO_ID", nullable=false)
	private String midasEstiloId;
	
	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public AmisConversionEstiloPK getId() {
		return id;
	}
	public void setId(AmisConversionEstiloPK id) {
		this.id = id;
	}
	public Long getMidasMarcaId() {
		return midasMarcaId;
	}
	public void setMidasMarcaId(Long midasMarcaId) {
		this.midasMarcaId = midasMarcaId;
	}
	public String getMidasEstiloId() {
		return midasEstiloId;
	}
	public void setMidasEstiloId(String midasEstiloId) {
		this.midasEstiloId = midasEstiloId;
	}


	
	
}
