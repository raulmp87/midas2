package mx.com.afirme.midas2.domain.ocra;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name = "SAP_AMIS_UBICACION", schema = "MIDAS")

public class AmisUbicacion implements Entidad {
	

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	@AttributeOverrides( {
		@AttributeOverride(name = "idPais",      column = @Column(name = "ID_PAIS",     nullable = false )),
		@AttributeOverride(name = "idEstado",    column = @Column(name = "ID_ESTADO",   nullable = false )),
		@AttributeOverride(name = "idMunicipio", column = @Column(name = "ID_MUNICIPIO",nullable = false ))
	} )
	private AmisUbicacionPK id;
	
	@Column(name = "DESC_PAIS")
	private String descPais;
	
	@Column(name = "DESC_ESTADO")
	private String descEstado;
	
	@Column(name = "DESC_MUNICIPIO")
	private String descMunicipio;
	
	@Column(name = "CVE_SEYCOS_PAIS")
	private String cveSeycosPais;
	
	@Column(name = "CVE_SEYCOS_ESTADO")
	private String cveSeycosEstado;
	
	@Column(name = "CVE_SEYCOS_MUNICIPIO")
	private String cveSeycosMunicipio;
	
	public AmisUbicacion(){}
	
	public AmisUbicacionPK getId() {
		return this.id;
	}

	public void setId(AmisUbicacionPK id) {
		this.id = id;
	}
	
	
	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	
	public String getDescPais() {
		return descPais;
	}

	public String getDescEstado() {
		return descEstado;
	}

	public String getDescMunicipio() {
		return descMunicipio;
	}

	public String getCveSeycosPais() {
		return cveSeycosPais;
	}

	public String getCveSeycosEstado() {
		return cveSeycosEstado;
	}

	public String getCveSeycosMunicipio() {
		return cveSeycosMunicipio;
	}

	public void setDescPais(String descPais) {
		this.descPais = descPais;
	}

	public void setDescEstado(String descEstado) {
		this.descEstado = descEstado;
	}

	public void setDescMunicipio(String descMunicipio) {
		this.descMunicipio = descMunicipio;
	}

	public void setCveSeycosPais(String cveSeycosPais) {
		this.cveSeycosPais = cveSeycosPais;
	}

	public void setCveSeycosEstado(String cveSeycosEstado) {
		this.cveSeycosEstado = cveSeycosEstado;
	}

	public void setCveSeycosMunicipio(String cveSeycosMunicipio) {
		this.cveSeycosMunicipio = cveSeycosMunicipio;
	}	


}
