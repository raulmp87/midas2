package mx.com.afirme.midas2.domain.ocra;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class AmisConversionEstiloPK implements Serializable {
	
	@Column(name = "AMISTIPOTRANSPORTE_ID", nullable=false)
	private Long amisTipoTransporteId;
	
	@Column(name = "AMISMARCA_ID", nullable=false)
	private Long amisMarcaId;
	
	@Column(name = "AMISSUBMARCA_ID", nullable=false)
	private Long amisSubMarcaId;
	
	@Column(name = "MIDASMARCA_ID", nullable=false)
	private Long midasMarcaId;
	
	@Column(name = "MIDASESTILO_ID", nullable=false)
	private String midasEstiloId;
	
	public AmisConversionEstiloPK(){}
	
	public AmisConversionEstiloPK(Long amisTipoTransporteId, Long amisMarcaId,
			Long amisSubMarcaId, Long midasMarcaId, String midasEstiloId) {
		super();
		this.amisTipoTransporteId = amisTipoTransporteId;
		this.amisMarcaId = amisMarcaId;
		this.amisSubMarcaId = amisSubMarcaId;
		this.midasMarcaId = midasMarcaId;
		this.midasEstiloId = midasEstiloId;
	}
	
	public Long getAmisTipoTransporteId() {
		return amisTipoTransporteId;
	}
	public Long getAmisMarcaId() {
		return amisMarcaId;
	}
	public Long getAmisSubMarcaId() {
		return amisSubMarcaId;
	}
	public Long getMidasMarcaId() {
		return midasMarcaId;
	}
	public String getMidasEstiloId() {
		return midasEstiloId;
	}
	public void setAmisTipoTransporteId(Long amisTipoTransporteId) {
		this.amisTipoTransporteId = amisTipoTransporteId;
	}
	public void setAmisMarcaId(Long amisMarcaId) {
		this.amisMarcaId = amisMarcaId;
	}
	public void setAmisSubMarcaId(Long amisSubMarcaId) {
		this.amisSubMarcaId = amisSubMarcaId;
	}
	public void setMidasMarcaId(Long midasMarcaId) {
		this.midasMarcaId = midasMarcaId;
	}
	public void setMidasEstiloId(String midasEstiloId) {
		this.midasEstiloId = midasEstiloId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((amisMarcaId == null) ? 0 : amisMarcaId.hashCode());
		result = prime * result
				+ ((amisSubMarcaId == null) ? 0 : amisSubMarcaId.hashCode());
		result = prime
				* result
				+ ((amisTipoTransporteId == null) ? 0 : amisTipoTransporteId
						.hashCode());
		result = prime * result
				+ ((midasEstiloId == null) ? 0 : midasEstiloId.hashCode());
		result = prime * result
				+ ((midasMarcaId == null) ? 0 : midasMarcaId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AmisConversionEstiloPK other = (AmisConversionEstiloPK) obj;
		if (amisMarcaId == null) {
			if (other.amisMarcaId != null)
				return false;
		} else if (!amisMarcaId.equals(other.amisMarcaId))
			return false;
		if (amisSubMarcaId == null) {
			if (other.amisSubMarcaId != null)
				return false;
		} else if (!amisSubMarcaId.equals(other.amisSubMarcaId))
			return false;
		if (amisTipoTransporteId == null) {
			if (other.amisTipoTransporteId != null)
				return false;
		} else if (!amisTipoTransporteId.equals(other.amisTipoTransporteId))
			return false;
		if (midasEstiloId == null) {
			if (other.midasEstiloId != null)
				return false;
		} else if (!midasEstiloId.equals(other.midasEstiloId))
			return false;
		if (midasMarcaId == null) {
			if (other.midasMarcaId != null)
				return false;
		} else if (!midasMarcaId.equals(other.midasMarcaId))
			return false;
		return true;
	}

}
