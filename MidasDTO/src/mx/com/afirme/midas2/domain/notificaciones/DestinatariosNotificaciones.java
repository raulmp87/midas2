package mx.com.afirme.midas2.domain.notificaciones;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;

@Entity(name="DestinatariosNotificaciones")
@Table(name="toDestinatariosNotificaciones", schema="MIDAS")
public class DestinatariosNotificaciones implements Entidad, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4463865627286672956L;
	
	private Long id;
	private ConfiguracionNotificaciones idConfigNotificacion;
	private ValorCatalogoAgentes idModoEnvio;
	private ValorCatalogoAgentes idTipoDestinatario;
	private ValorCatalogoAgentes idTipoCorreoEnvio;
	private String puesto;
	private String nombre;
	private String correo;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="idToDestNotif_seq")
	@SequenceGenerator(name="idToDestNotif_seq",sequenceName="MIDAS.idToDestNotif_seq",allocationSize=1)
	@Column(name="ID", nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDCONFIGNOTIFICACION", referencedColumnName="ID")
	public ConfiguracionNotificaciones getIdConfigNotificacion() {
		return idConfigNotificacion;
	}

	public void setIdConfigNotificacion(ConfiguracionNotificaciones idConfigNotificacion) {
		this.idConfigNotificacion = idConfigNotificacion;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDMODOENVIO")
	public ValorCatalogoAgentes getIdModoEnvio() {
		return idModoEnvio;
	}

	public void setIdModoEnvio(ValorCatalogoAgentes idModoEnvio) {
		this.idModoEnvio = idModoEnvio;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDTIPODESTINATARIO")
	public ValorCatalogoAgentes getIdTipoDestinatario() {
		return idTipoDestinatario;
	}

	public void setIdTipoDestinatario(ValorCatalogoAgentes idTipoDestinatario) {
		this.idTipoDestinatario = idTipoDestinatario;
	}

	@Column(name="PUESTO")
	public String getPuesto() {
		return puesto;
	}

	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}

	@Column(name="NOMBRE")
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name="CORREO")
	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDTIPOCORREOENVIO")
	public ValorCatalogoAgentes getIdTipoCorreoEnvio() {
		return idTipoCorreoEnvio;
	}

	public void setIdTipoCorreoEnvio(ValorCatalogoAgentes idTipoCorreoEnvio) {
		this.idTipoCorreoEnvio = idTipoCorreoEnvio;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}
