package mx.com.afirme.midas2.domain.notificaciones;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name="ConfiguracionNotificaciones")
@Table(name="toConfigNotificaciones", schema="MIDAS")
public class ConfiguracionNotificaciones implements Entidad, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1978037787458878092L;

	private Long id;
	private ProcesosAgentes idProceso;
	private MovimientosProcesos idMovimiento;
	private List<DestinatariosNotificaciones>destinatariosList= new ArrayList<DestinatariosNotificaciones>();
	private String usuario;
	private Date fechaCreacion;
	private String fechaCreacionString;
	private String notas;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="idToConfigNotificaciones_seq")
	@SequenceGenerator(name="idToConfigNotificaciones_seq",sequenceName="MIDAS.idToConfigNotificaciones_seq", allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ProcesosAgentes.class)
	@JoinColumn(name="IDPROCESO")
	public ProcesosAgentes getIdProceso() {
		return idProceso;
	}

	public void setIdProceso(ProcesosAgentes idProceso) {
		this.idProceso = idProceso;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=MovimientosProcesos.class)
	@JoinColumn(name="IDMOVIMIENTO")
	public MovimientosProcesos getIdMovimiento() {
		return idMovimiento;
	}

	public void setIdMovimiento(MovimientosProcesos idMovimiento) {
		this.idMovimiento = idMovimiento;
	}

	@OneToMany(cascade=CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="idConfigNotificacion", targetEntity=DestinatariosNotificaciones.class)
	public List<DestinatariosNotificaciones> getDestinatariosList() {
		return destinatariosList;
	}

	public void setDestinatariosList(
			List<DestinatariosNotificaciones> destinatariosList) {
		this.destinatariosList = destinatariosList;
	}

	@Column(name="USUARIO")
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="FECHACREACION")
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");    	
		this.fechaCreacionString=sdf.format(fechaCreacion);
	}	
	
	@Transient
	public String getFechaCreacionString() {
		return fechaCreacionString;
	}

	public void setFechaCreacionString(String fechaCreacionString) {
		this.fechaCreacionString = fechaCreacionString;
	}

	@Column(name="NOTAS")
	public String getNotas() {
		return notas;
	}

	public void setNotas(String notas) {
		this.notas = notas;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}
