package mx.com.afirme.midas2.domain.notificaciones;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name="ProcesosAgentes")
@Table(name="tcProcesosAgentes",schema="MIDAS")
public class ProcesosAgentes implements Entidad, Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4000557440321087998L;
	private Long id;
	private String descripcion;
	
	public ProcesosAgentes() {
		
	}
	
	public ProcesosAgentes(Long idProceso) {
		this.id = idProceso;
	}
	
	@Id
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="DESCRIPCION")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

}
