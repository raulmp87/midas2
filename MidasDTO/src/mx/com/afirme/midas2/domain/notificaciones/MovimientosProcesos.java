package mx.com.afirme.midas2.domain.notificaciones;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name="MovimientosProcesos")
@Table(name="tcMovsProcesosAgentes",schema="MIDAS")
public class MovimientosProcesos implements Entidad, Serializable{	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2375239222704630245L;
	private Long id;
	private Long idProceso;
	private String descripcion;
	
	public MovimientosProcesos() {
		
	}
	
	public MovimientosProcesos(Long idMovimientoProceso) {
		this.id = idMovimientoProceso;
	}
	
	@Id
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="DESCRIPCION")
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Column(name="IDPROCESO")
	public Long getIdProceso() {
		return idProceso;
	}

	public void setIdProceso(Long idProceso) {
		this.idProceso = idProceso;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}
	
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}	
}
