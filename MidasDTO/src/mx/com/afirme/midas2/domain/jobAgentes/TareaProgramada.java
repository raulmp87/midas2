package mx.com.afirme.midas2.domain.jobAgentes;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Clase para el servicio de programacion de tareas para los calculos de comisiones,bonos y cargos
 * @author vmhersil
 *
 */
@Entity
public class TareaProgramada implements Serializable{
	private static final long serialVersionUID = 6915012688819456321L;
	private Long id;
	private	Date fechaEjecucion;
	public TareaProgramada(){}
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaEjecucion() {
		return fechaEjecucion;
	}
	public void setFechaEjecucion(Date fechaEjecucion) {
		this.fechaEjecucion = fechaEjecucion;
	}
}