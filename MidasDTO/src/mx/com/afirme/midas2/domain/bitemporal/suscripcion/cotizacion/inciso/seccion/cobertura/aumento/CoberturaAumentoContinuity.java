package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.aumento;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.catalogos.aumentovario.AumentoVarioDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.CoberturaSeccionContinuity;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.afirme.bitemporal.annotations.BitemporalNotNull;
import com.anasoft.os.daofusion.bitemporal.BitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;
import com.anasoft.os.daofusion.bitemporal.WrappedBitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.WrappedValueAccessor;

@Entity
@Table(name="MCOBERTURAAUMENTOC",schema="MIDAS")
public class CoberturaAumentoContinuity implements Serializable,
		EntidadContinuity<CoberturaAumento, BitemporalCoberturaAumento> {

	public CoberturaAumentoContinuity() {
		if(coberturaSeccionContinuity == null) {
			coberturaSeccionContinuity = new CoberturaSeccionContinuity();
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -7412799468194825752L;

	public static final String PARENT_KEY_NAME = "coberturaSeccionContinuity.id";
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQMCOBERTURAAUMENTOCONTID")
	@SequenceGenerator(name = "SEQMCOBERTURAAUMENTOCONTID", sequenceName = "MIDAS.SEQMCOBERTURAAUMENTOCONTID", allocationSize = 1)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="AUMENTOVARIO_ID" ,referencedColumnName="IDTOAUMENTOVARIO", nullable=false)
	private AumentoVarioDTO aumentoVarioDTO;
	
	@OneToMany(mappedBy="continuity",cascade = CascadeType.ALL, orphanRemoval=true)
	private Collection<BitemporalCoberturaAumento> coberturaAumentos = new LinkedList<BitemporalCoberturaAumento>();
	
	@BitemporalNotNull
	@ManyToOne
	@JoinColumn(name="MCOBERTURASECCIONC_ID", referencedColumnName="id", nullable=false)
	private CoberturaSeccionContinuity coberturaSeccionContinuity = new CoberturaSeccionContinuity();
	

	@Override
	@Transient
	public BitemporalProperty<CoberturaAumento, BitemporalCoberturaAumento> getBitemporalProperty() {
		return getCoberturaAumentos();
	}
	
	@SuppressWarnings("serial")
	public WrappedBitemporalProperty<CoberturaAumento, BitemporalCoberturaAumento, CoberturaAumentoContinuity> getCoberturaAumentos() {
		return new WrappedBitemporalProperty<CoberturaAumento, BitemporalCoberturaAumento, CoberturaAumentoContinuity>(
				coberturaAumentos,
				new WrappedValueAccessor<CoberturaAumento, BitemporalCoberturaAumento, CoberturaAumentoContinuity>() {

					public BitemporalCoberturaAumento wrapValue(CoberturaAumento value,
							IntervalWrapper validityInterval, boolean twoPhaseMode) {
						return new BitemporalCoberturaAumento(value, validityInterval,CoberturaAumentoContinuity.this, null,twoPhaseMode);
					}

				});
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	
	public AumentoVarioDTO getAumentoVarioDTO() {
		return aumentoVarioDTO;
	}
	public void setAumentoVarioDTO(AumentoVarioDTO aumentoVarioDTO) {
		this.aumentoVarioDTO = aumentoVarioDTO;
	}



	public CoberturaSeccionContinuity getCoberturaSeccionContinuity() {
		return coberturaSeccionContinuity;
	}


	public void setCoberturaSeccionContinuity(
			CoberturaSeccionContinuity coberturaSeccionContinuity) {
		this.coberturaSeccionContinuity = coberturaSeccionContinuity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CoberturaAumentoContinuity other = (CoberturaAumentoContinuity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return getId();
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getBusinessKey() {
		return aumentoVarioDTO.getIdAumentoVario();
	}

	@SuppressWarnings("unchecked")
	@Override
	public CoberturaSeccionContinuity getParentContinuity() {
		return getCoberturaSeccionContinuity();
	}

	@SuppressWarnings("rawtypes")
	public void setParentContinuity(EntidadContinuity parentContinuity) {
		setCoberturaSeccionContinuity((CoberturaSeccionContinuity)parentContinuity);
	};

	@Override
	public Class<BitemporalCoberturaAumento> getBitemporalClass() {
		return BitemporalCoberturaAumento.class;
	}
}
