package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.comision;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.anasoft.os.daofusion.bitemporal.BitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;
import com.anasoft.os.daofusion.bitemporal.WrappedBitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.WrappedValueAccessor;

@Entity
@Table(name = "MCOMISIONC", schema = "MIDAS")
public class ComisionContinuity implements Serializable,
EntidadContinuity<Comision, BitemporalComision> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3458085949605750363L;

	public static final String PARENT_KEY_NAME = "cotizacionContinuity.id";
	public static final String PARENT_BUSINESS_KEY_NAME = "cotizacionContinuity.numero";
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQMCOMISIONCONTID")
	@SequenceGenerator(name = "SEQMCOMISIONCONTID", sequenceName = "MIDAS.SEQMCOMISIONCONTID", allocationSize = 1)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	@JoinColumn(name="SUBRAMO_ID", referencedColumnName="IDTCSUBRAMO")
	private SubRamoDTO subRamoDTO;

	@OneToMany(mappedBy = "continuity", cascade = CascadeType.ALL, orphanRemoval=true)
	private Collection<BitemporalComision> comisiones = new LinkedList<BitemporalComision>();

	@ManyToOne
	@JoinColumn(name = "MCOTIZACIONC_ID", referencedColumnName = "id", nullable = false)
	private CotizacionContinuity cotizacionContinuity;


	public ComisionContinuity() {
		if(cotizacionContinuity == null) {
			cotizacionContinuity = new CotizacionContinuity();
		}
	}

	public ComisionContinuity(CotizacionContinuity cotizacionContinuity) {
		this.cotizacionContinuity = cotizacionContinuity;
	}

	@Override
	@Transient
	public BitemporalProperty<Comision, BitemporalComision> getBitemporalProperty() {
		return getComisiones();
	}

	@SuppressWarnings("serial")
	public WrappedBitemporalProperty<Comision, BitemporalComision, ComisionContinuity> getComisiones() {
		return new WrappedBitemporalProperty<Comision, BitemporalComision, ComisionContinuity>(
				comisiones,
				new WrappedValueAccessor<Comision, BitemporalComision, ComisionContinuity>() {

					public BitemporalComision wrapValue(Comision value,
							IntervalWrapper validityInterval,
							boolean twoPhaseMode) {
						return new BitemporalComision(value, validityInterval,
								ComisionContinuity.this, null, twoPhaseMode);
					}

				});
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SubRamoDTO getSubRamoDTO() {
		return subRamoDTO;
	}

	public void setSubRamoDTO(SubRamoDTO subRamoDTO) {
		this.subRamoDTO = subRamoDTO;
	}

	public CotizacionContinuity getCotizacionContinuity() {
		return cotizacionContinuity;
	}

	public void setCotizacionContinuity(
			CotizacionContinuity cotizacionContinuity) {
		this.cotizacionContinuity = cotizacionContinuity;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComisionContinuity other = (ComisionContinuity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.id;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CotizacionContinuity getParentContinuity() {
		return getCotizacionContinuity();
	}
	
	@SuppressWarnings("rawtypes")
	public void setParentContinuity(EntidadContinuity parentContinuity) {
		setCotizacionContinuity((CotizacionContinuity)parentContinuity);
	};
	
	@Override
	public Class<BitemporalComision> getBitemporalClass() {
		return BitemporalComision.class;
	}
}
