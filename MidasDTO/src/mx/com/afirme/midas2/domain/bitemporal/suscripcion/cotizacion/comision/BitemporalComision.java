package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.comision;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.anasoft.os.daofusion.bitemporal.Bitemporal;
import com.anasoft.os.daofusion.bitemporal.BitemporalWrapper;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;

@Entity
@Table(name="MCOMISIONB",schema="MIDAS")
@Access(AccessType.FIELD)
public class BitemporalComision  extends BitemporalWrapper<Comision, ComisionContinuity> implements EntidadBitemporal<Comision, BitemporalComision>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7968257177058150564L;
	@Embedded
	private Comision value;
	@ManyToOne
	@JoinColumn(name = "MCOMISIONC_ID", referencedColumnName="id")
	private ComisionContinuity continuity;

	
	public BitemporalComision(){
		if(continuity == null) {
			continuity = new ComisionContinuity();
		}
		if(value == null) {
			value = new Comision();
		}
	}
	public BitemporalComision(Comision value, IntervalWrapper validityInterval,ComisionContinuity continuity,String valueId, boolean twoPhaseMode) {
		super(value, validityInterval,continuity, valueId, twoPhaseMode);
	}

	public BitemporalComision(Comision value,ComisionContinuity continuity){
		this.value = value;
		this.continuity = continuity;
	}
	@Override
	public Bitemporal copyWith(IntervalWrapper validityInterval, boolean twoPhaseMode) {
		return new BitemporalComision(value, validityInterval, getContinuity(), getValueId(), twoPhaseMode);
	}

	@Override
	public Comision getValue() {
		return value;
	}

	@Override
	public void setValue(Comision value) {
		this.value = value;
	}
	@Override
	public ComisionContinuity getContinuity() {
		return continuity;
	}
	@Override
	protected void setContinuity(ComisionContinuity continuity) {
		this.continuity = continuity;
	}
	
	@Override
	public Comision  getEmbedded() {
		return this.value;
	}
	
	@Override
	public void setEmbedded(Comision value) {
		this.value = value;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ComisionContinuity getEntidadContinuity() {
		return getContinuity();
	}
	
	public void setEntidadContinuity(EntidadContinuity<Comision,BitemporalComision> entidadContinuity){
		this.continuity = (ComisionContinuity) entidadContinuity;
	}

}
