package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducibles.NegocioDeducibleCob;
import mx.com.afirme.midas2.util.UtileriasWeb;

@Embeddable
public class CoberturaSeccion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 966260313521523457L;
	
	@Column(name="SUBRAMO_ID")
	private Long subRamoId;
	
	@Column(name="VALORPRIMANETA")
    private Double valorPrimaNeta;
	
	@Column(name="VALORSUMAASEGURADA")
    private Double valorSumaAsegurada;
	
	@Column(name="VALORCOASEGURO")
    private Double valorCoaseguro;
	
	@Column(name="VALORDEDUCIBLE")
    private Double valorDeducible;
	
	@Column(name="CODIGOUSUARIOAUTREASEGURO")
    private String codigoUsuarioAutReaseguro;
	
	@Column(name="CLAVEOBLIGATORIEDAD")
    private Short claveObligatoriedad;
	
	@Column(name="CLAVECONTRATO")
    private Short claveContrato;
	
	@Column(name="VALORCUOTA")
	private Double valorCuota;
	
	
	@Column(name="PORCENTAJECOASEGURO")
	private Double porcentajeCoaseguro;
	
	@Column(name="CLAVEAUTCOASEGURO")
	private Short claveAutCoaseguro;
	
	@Column(name="CODIGOUSUARIOAUTCOASEGURO")
	private String codigoUsuarioAutCoaseguro;
	
	@Column(name="PORCENTAJEDEDUCIBLE")
	private Double porcentajeDeducible;
	
	@Column(name="CLAVEAUTDEDUCIBLE")
	private Short claveAutDeducible;
	
	@Column(name="CODIGOUSUARIOAUTDEDUCIBLE")
	private String codigoUsuarioAutDeducible;
	
	@Column(name="NUMEROAGRUPACION")
	private Short numeroAgrupacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHASOLAUTCOASEGURO")
	private Date fechaSolAutCoaSeguro;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAAUTCOASEGURO")
	private Date fechaAutCoaSeguro;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHASOLAUTDEDUCIBLE")
	private Date fechaSolAutDeducible;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAAUTDEDUCIBLE")
	private Date fechaAutDeducible;
	
	@Column(name="CLAVETIPODEDUCIBLE")
	private Short claveTipoDeducible;
	
	@Column(name="CLAVETIPOLIMITEDEDUCIBLE")
	private Short claveTipoLimiteDeducible;
	
	@Column(name="VALORMINIMOLIMITEDEDUCIBLE")
	private Double valorMinimoLimiteDeducible;
	
	@Column(name="VALORMAXIMOLIMITEDEDUCIBLE")
	private Double valorMaximoLimiteDeducible;
	
	@Column(name="CLAVEFACULTATIVO")
	private Short claveFacultativo;
	
	@Column(name="AGRUPADORTARIFA_ID")
	private Integer agrupadorTarifaId;
	
	@Column(name="VERAGRUPADORTARIFA_ID")
	private Integer verAgrupadorTarifaId;
	
	@Column(name="VERSIONCARGA_ID")
	private Integer verCargaId;
	
	@Column(name="VERSIONTARIFA_ID")
	private Integer verTarifaId;
	
	@Column(name="TARIFAEXT_ID")
	private Integer tarifaExtId;
	
	@Column(name="VERSIONTARIFAEXT_ID")
	private Integer verTarifaExtId;
	
	@Column(name="VALORPRIMADIARIA")
	private Double valorPrimaDiaria;
	
	@Column(name="DIASSALARIOMINIMO")
	private Integer diasSalarioMinimo;

	@Column(name="CONSULTAASEGURADA")
	private Boolean consultaAsegurada;

	@Transient
	private Double valorCuotaAnterior;
	
	@Transient
	private Boolean claveContratoBoolean;
	
	@Transient
	private CoberturaSeccionDTO coberturaSeccionDTO;
	
	@Transient
    private String descripcionDeducible;
	
	@Transient
    private List<NegocioDeducibleCob> deducibles;
	
	@Transient
    private Double valorSumaAseguradaMin;
	
	@Transient
    private Double valorSumaAseguradaMax;	
	

	public Long getSubRamoId() {
		return subRamoId;
	}
	public void setSubRamoId(Long subRamoId) {
		this.subRamoId = subRamoId;
	}
	

	public Double getValorPrimaNeta() {
		return valorPrimaNeta;
	}
	public void setValorPrimaNeta(Double valorPrimaNeta) {
		this.valorPrimaNeta = valorPrimaNeta;
	}
	
	
	public Double getValorSumaAsegurada() {
		return valorSumaAsegurada;
	}
	public void setValorSumaAsegurada(Double valorSumaAsegurada) {
		this.valorSumaAsegurada = valorSumaAsegurada;
	}
	
	public void setValorSumaAseguradaStr(String valorSumaAsegurada) {
		this.valorSumaAsegurada = UtileriasWeb.eliminaFormatoMoneda(valorSumaAsegurada);
	}
		
	public Double getValorCoaseguro() {
		return valorCoaseguro;
	}
	public void setValorCoaseguro(Double valorCoaseguro) {
		this.valorCoaseguro = valorCoaseguro;
	}
	

	public Double getValorDeducible() {
		return valorDeducible;
	}
	public void setValorDeducible(Double valorDeducible) {
		this.valorDeducible = valorDeducible;
	}
	
	
	public String getCodigoUsuarioAutReaseguro() {
		return codigoUsuarioAutReaseguro;
	}
	public void setCodigoUsuarioAutReaseguro(String codigoUsuarioAutReaseguro) {
		this.codigoUsuarioAutReaseguro = codigoUsuarioAutReaseguro;
	}
	
	
	public Short getClaveObligatoriedad() {
		return claveObligatoriedad;
	}
	public void setClaveObligatoriedad(Short claveObligatoriedad) {
		this.claveObligatoriedad = claveObligatoriedad;
	}
	
	
	public Short getClaveContrato() {
		return claveContrato;
	}
	public void setClaveContrato(Short claveContrato) {
		this.claveContrato = claveContrato;
	}
	

	public Double getValorCuota() {
		return valorCuota;
	}
	public void setValorCuota(Double valorCuota) {
		this.valorCuota = valorCuota;
	}
	

	
	
	public Double getPorcentajeCoaseguro() {
		return porcentajeCoaseguro;
	}
	public void setPorcentajeCoaseguro(Double porcentajeCoaseguro) {
		this.porcentajeCoaseguro = porcentajeCoaseguro;
	}
	
	
	public Short getClaveAutCoaseguro() {
		return claveAutCoaseguro;
	}
	public void setClaveAutCoaseguro(Short claveAutCoaseguro) {
		this.claveAutCoaseguro = claveAutCoaseguro;
	}
	

	public String getCodigoUsuarioAutCoaseguro() {
		return codigoUsuarioAutCoaseguro;
	}
	public void setCodigoUsuarioAutCoaseguro(String codigoUsuarioAutCoaseguro) {
		this.codigoUsuarioAutCoaseguro = codigoUsuarioAutCoaseguro;
	}
	
	
	public Double getPorcentajeDeducible() {
		return porcentajeDeducible;
	}
	public void setPorcentajeDeducible(Double porcentajeDeducible) {
		this.porcentajeDeducible = porcentajeDeducible;
	}
	

	public Short getClaveAutDeducible() {
		return claveAutDeducible;
	}
	public void setClaveAutDeducible(Short claveAutDeducible) {
		this.claveAutDeducible = claveAutDeducible;
	}
	
	
	public String getCodigoUsuarioAutDeducible() {
		return codigoUsuarioAutDeducible;
	}
	public void setCodigoUsuarioAutDeducible(String codigoUsuarioAutDeducible) {
		this.codigoUsuarioAutDeducible = codigoUsuarioAutDeducible;
	}
	
	
	public Short getNumeroAgrupacion() {
		return numeroAgrupacion;
	}
	public void setNumeroAgrupacion(Short numeroAgrupacion) {
		this.numeroAgrupacion = numeroAgrupacion;
	}
	
	
	public Date getFechaSolAutCoaSeguro() {
		return fechaSolAutCoaSeguro;
	}
	public void setFechaSolAutCoaSeguro(Date fechaSolAutCoaSeguro) {
		this.fechaSolAutCoaSeguro = fechaSolAutCoaSeguro;
	}
	

	public Date getFechaAutCoaSeguro() {
		return fechaAutCoaSeguro;
	}
	public void setFechaAutCoaSeguro(Date fechaAutCoaSeguro) {
		this.fechaAutCoaSeguro = fechaAutCoaSeguro;
	}
	

	public Date getFechaSolAutDeducible() {
		return fechaSolAutDeducible;
	}
	public void setFechaSolAutDeducible(Date fechaSolAutDeducible) {
		this.fechaSolAutDeducible = fechaSolAutDeducible;
	}
	

	public Date getFechaAutDeducible() {
		return fechaAutDeducible;
	}
	public void setFechaAutDeducible(Date fechaAutDeducible) {
		this.fechaAutDeducible = fechaAutDeducible;
	}
	
	
	public Short getClaveTipoDeducible() {
		return claveTipoDeducible;
	}
	public void setClaveTipoDeducible(Short claveTipoDeducible) {
		this.claveTipoDeducible = claveTipoDeducible;
	}
	
	
	public Short getClaveTipoLimiteDeducible() {
		return claveTipoLimiteDeducible;
	}
	public void setClaveTipoLimiteDeducible(Short claveTipoLimiteDeducible) {
		this.claveTipoLimiteDeducible = claveTipoLimiteDeducible;
	}
	
	
	public Double getValorMinimoLimiteDeducible() {
		return valorMinimoLimiteDeducible;
	}
	public void setValorMinimoLimiteDeducible(Double valorMinimoLimiteDeducible) {
		this.valorMinimoLimiteDeducible = valorMinimoLimiteDeducible;
	}
	

	public Double getValorMaximoLimiteDeducible() {
		return valorMaximoLimiteDeducible;
	}
	public void setValorMaximoLimiteDeducible(Double valorMaximoLimiteDeducible) {
		this.valorMaximoLimiteDeducible = valorMaximoLimiteDeducible;
	}
	
	
	public Short getClaveFacultativo() {
		return claveFacultativo;
	}
	public void setClaveFacultativo(Short claveFacultativo) {
		this.claveFacultativo = claveFacultativo;
	}


	public Integer getAgrupadorTarifaId() {
		return agrupadorTarifaId;
	}
	public void setAgrupadorTarifaId(Integer agrupadorTarifaId) {
		this.agrupadorTarifaId = agrupadorTarifaId;
	}
	
	
	public Integer getVerAgrupadorTarifaId() {
		return verAgrupadorTarifaId;
	}
	public void setVerAgrupadorTarifaId(Integer verAgrupadorTarifaId) {
		this.verAgrupadorTarifaId = verAgrupadorTarifaId;
	}
	

	public Integer getVerCargaId() {
		return verCargaId;
	}
	public void setVerCargaId(Integer verCargaId) {
		this.verCargaId = verCargaId;
	}
	

	public Integer getVerTarifaId() {
		return verTarifaId;
	}
	public void setVerTarifaId(Integer verTarifaId) {
		this.verTarifaId = verTarifaId;
	}
	
	
	public Integer getTarifaExtId() {
		return tarifaExtId;
	}
	public void setTarifaExtId(Integer tarifaExtId) {
		this.tarifaExtId = tarifaExtId;
	}
	
	
	public Integer getVerTarifaExtId() {
		return verTarifaExtId;
	}
	public void setVerTarifaExtId(Integer verTarifaExtId) {
		this.verTarifaExtId = verTarifaExtId;
	}
	
	
	public Double getValorPrimaDiaria() {
		return valorPrimaDiaria;
	}
	public void setValorPrimaDiaria(Double valorPrimaDiaria) {
		this.valorPrimaDiaria = valorPrimaDiaria;
	}
	
	public Boolean getClaveContratoBoolean() {
		return claveContratoBoolean;
	}

	public void setClaveContratoBoolean(Boolean claveContratoBoolean) {
		this.claveContratoBoolean = claveContratoBoolean;
		if(this.claveContratoBoolean){
			this.claveContrato = 1;
		}else{
			this.claveContrato = 0;
		}
	}
	

	public CoberturaSeccionDTO getCoberturaSeccionDTO() {
		return coberturaSeccionDTO;
	}
	public void setCoberturaSeccionDTO(CoberturaSeccionDTO coberturaSeccionDTO) {
		this.coberturaSeccionDTO = coberturaSeccionDTO;
	}

	public String getDescripcionDeducible() {
		return descripcionDeducible;
	}
	public void setDescripcionDeducible(String descripcionDeducible) {
		this.descripcionDeducible = descripcionDeducible;
	}

	public List<NegocioDeducibleCob>  getDeducibles() {
		return deducibles;
	}
	public void setDeducibles(List<NegocioDeducibleCob>  deducibles) {
		this.deducibles = deducibles;
	}

	public Double getValorSumaAseguradaMin() {
		return valorSumaAseguradaMin;
	}
	public void setValorSumaAseguradaMin(Double valorSumaAseguradaMin) {
		this.valorSumaAseguradaMin = valorSumaAseguradaMin;
	}

	public Double getValorSumaAseguradaMax() {
		return valorSumaAseguradaMax;
	}
	public void setValorSumaAseguradaMax(Double valorSumaAseguradaMax) {
		this.valorSumaAseguradaMax = valorSumaAseguradaMax;
	}
	
	public Double getValorCuotaAnterior() {
		return valorCuotaAnterior;
	}
	public void setValorCuotaAnterior(Double valorCuotaAnterior) {
		this.valorCuotaAnterior = valorCuotaAnterior;
	}
	
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((agrupadorTarifaId == null) ? 0 : agrupadorTarifaId
						.hashCode());
		result = prime
				* result
				+ ((claveAutCoaseguro == null) ? 0 : claveAutCoaseguro
						.hashCode());
		result = prime
				* result
				+ ((claveAutDeducible == null) ? 0 : claveAutDeducible
						.hashCode());
		result = prime * result
				+ ((claveContrato == null) ? 0 : claveContrato.hashCode());
		result = prime
				* result
				+ ((claveFacultativo == null) ? 0 : claveFacultativo.hashCode());
		result = prime
				* result
				+ ((claveObligatoriedad == null) ? 0 : claveObligatoriedad
						.hashCode());
		result = prime
				* result
				+ ((claveTipoDeducible == null) ? 0 : claveTipoDeducible
						.hashCode());
		result = prime
				* result
				+ ((claveTipoLimiteDeducible == null) ? 0
						: claveTipoLimiteDeducible.hashCode());
		result = prime
				* result
				+ ((codigoUsuarioAutCoaseguro == null) ? 0
						: codigoUsuarioAutCoaseguro.hashCode());
		result = prime
				* result
				+ ((codigoUsuarioAutDeducible == null) ? 0
						: codigoUsuarioAutDeducible.hashCode());
		result = prime
				* result
				+ ((codigoUsuarioAutReaseguro == null) ? 0
						: codigoUsuarioAutReaseguro.hashCode());
		result = prime
				* result
				+ ((fechaAutCoaSeguro == null) ? 0 : fechaAutCoaSeguro
						.hashCode());
		result = prime
				* result
				+ ((fechaAutDeducible == null) ? 0 : fechaAutDeducible
						.hashCode());
		result = prime
				* result
				+ ((fechaSolAutCoaSeguro == null) ? 0 : fechaSolAutCoaSeguro
						.hashCode());
		result = prime
				* result
				+ ((fechaSolAutDeducible == null) ? 0 : fechaSolAutDeducible
						.hashCode());
		result = prime
				* result
				+ ((numeroAgrupacion == null) ? 0 : numeroAgrupacion.hashCode());
		result = prime
				* result
				+ ((porcentajeCoaseguro == null) ? 0 : porcentajeCoaseguro
						.hashCode());
		result = prime
				* result
				+ ((porcentajeDeducible == null) ? 0 : porcentajeDeducible
						.hashCode());
		result = prime * result
				+ ((subRamoId == null) ? 0 : subRamoId.hashCode());
		result = prime * result
				+ ((tarifaExtId == null) ? 0 : tarifaExtId.hashCode());
		result = prime * result
				+ ((valorCoaseguro == null) ? 0 : valorCoaseguro.hashCode());
		result = prime * result
				+ ((valorCuota == null) ? 0 : valorCuota.hashCode());
		result = prime * result
				+ ((valorDeducible == null) ? 0 : valorDeducible.hashCode());
		result = prime
				* result
				+ ((valorMaximoLimiteDeducible == null) ? 0
						: valorMaximoLimiteDeducible.hashCode());
		result = prime
				* result
				+ ((valorMinimoLimiteDeducible == null) ? 0
						: valorMinimoLimiteDeducible.hashCode());
		result = prime * result
				+ ((valorPrimaNeta == null) ? 0 : valorPrimaNeta.hashCode());
		result = prime
				* result
				+ ((valorSumaAsegurada == null) ? 0 : valorSumaAsegurada
						.hashCode());
		result = prime
				* result
				+ ((verAgrupadorTarifaId == null) ? 0 : verAgrupadorTarifaId
						.hashCode());
		result = prime * result
				+ ((verCargaId == null) ? 0 : verCargaId.hashCode());
		result = prime * result
				+ ((verTarifaExtId == null) ? 0 : verTarifaExtId.hashCode());
		result = prime * result
				+ ((verTarifaId == null) ? 0 : verTarifaId.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof CoberturaSeccion))
			return false;
		CoberturaSeccion other = (CoberturaSeccion) obj;
//		if (agrupadorTarifaId == null) {
//			if (other.agrupadorTarifaId != null)
//				return false;
//		} else if (!agrupadorTarifaId.equals(other.agrupadorTarifaId))
//			return false;
//		if (claveAutCoaseguro == null) {
//			if (other.claveAutCoaseguro != null)
//				return false;
//		} else if (!claveAutCoaseguro.equals(other.claveAutCoaseguro))
//			return false;
//		if (claveAutDeducible == null) {
//			if (other.claveAutDeducible != null)
//				return false;
//		} else if (!claveAutDeducible.equals(other.claveAutDeducible))
//			return false;
		if (claveContrato == null) {
			if (other.claveContrato != null)
				return false;
		} else if (!claveContrato.equals(other.claveContrato))
			return false;
//		if (claveFacultativo == null) {
//			if (other.claveFacultativo != null)
//				return false;
//		} else if (!claveFacultativo.equals(other.claveFacultativo))
//			return false;
//		if (claveObligatoriedad == null) {
//			if (other.claveObligatoriedad != null)
//				return false;
//		} else if (!claveObligatoriedad.equals(other.claveObligatoriedad))
//			return false;
//		if (claveTipoDeducible == null) {
//			if (other.claveTipoDeducible != null)
//				return false;
//		} else if (!claveTipoDeducible.equals(other.claveTipoDeducible))
//			return false;
//		if (claveTipoLimiteDeducible == null) {
//			if (other.claveTipoLimiteDeducible != null)
//				return false;
//		} else if (!claveTipoLimiteDeducible
//				.equals(other.claveTipoLimiteDeducible))
//			return false;
//		if (codigoUsuarioAutCoaseguro == null) {
//			if (other.codigoUsuarioAutCoaseguro != null)
//				return false;
//		} else if (!codigoUsuarioAutCoaseguro
//				.equals(other.codigoUsuarioAutCoaseguro))
//			return false;
//		if (codigoUsuarioAutDeducible == null) {
//			if (other.codigoUsuarioAutDeducible != null)
//				return false;
//		} else if (!codigoUsuarioAutDeducible
//				.equals(other.codigoUsuarioAutDeducible))
//			return false;
//		if (codigoUsuarioAutReaseguro == null) {
//			if (other.codigoUsuarioAutReaseguro != null)
//				return false;
//		} else if (!codigoUsuarioAutReaseguro
//				.equals(other.codigoUsuarioAutReaseguro))
//			return false;
//		if (fechaAutCoaSeguro == null) {
//			if (other.fechaAutCoaSeguro != null)
//				return false;
//		} else if (!fechaAutCoaSeguro.equals(other.fechaAutCoaSeguro))
//			return false;
//		if (fechaAutDeducible == null) {
//			if (other.fechaAutDeducible != null)
//				return false;
//		} else if (!fechaAutDeducible.equals(other.fechaAutDeducible))
//			return false;
//		if (fechaSolAutCoaSeguro == null) {
//			if (other.fechaSolAutCoaSeguro != null)
//				return false;
//		} else if (!fechaSolAutCoaSeguro.equals(other.fechaSolAutCoaSeguro))
//			return false;
//		if (fechaSolAutDeducible == null) {
//			if (other.fechaSolAutDeducible != null)
//				return false;
//		} else if (!fechaSolAutDeducible.equals(other.fechaSolAutDeducible))
//			return false;
//		if (numeroAgrupacion == null) {
//			if (other.numeroAgrupacion != null)
//				return false;
//		} else if (!numeroAgrupacion.equals(other.numeroAgrupacion))
//			return false;
//		if (porcentajeCoaseguro == null) {
//			if (other.porcentajeCoaseguro != null)
//				return false;
//		} else if (!porcentajeCoaseguro.equals(other.porcentajeCoaseguro))
//			return false;
		if(claveTipoDeducible != null && claveTipoDeducible.intValue() == 1){
			if (porcentajeDeducible == null) {
				if (other.porcentajeDeducible != null)
					return false;
			} else if (!porcentajeDeducible.equals(other.porcentajeDeducible))
				return false;
		}
//		if (subRamoId == null) {
//			if (other.subRamoId != null)
//				return false;
//		} else if (!subRamoId.equals(other.subRamoId))
//			return false;
//		if (tarifaExtId == null) {
//			if (other.tarifaExtId != null)
//				return false;
//		} else if (!tarifaExtId.equals(other.tarifaExtId))
//			return false;
//		if (valorCoaseguro == null) {
//			if (other.valorCoaseguro != null)
//				return false;
//		} else if (!valorCoaseguro.equals(other.valorCoaseguro))
//			return false;
//		if (valorCuota == null) {
//			if (other.valorCuota != null)
//				return false;
//		} else if (!valorCuota.equals(other.valorCuota))
//			return false;
		if(claveTipoDeducible != null && claveTipoDeducible.intValue() != 1){
			if (valorDeducible == null) {
				if (other.valorDeducible != null)
					return false;
			} else if (!valorDeducible.equals(other.valorDeducible))
				return false;
		}
//		if (valorMaximoLimiteDeducible == null) {
//			if (other.valorMaximoLimiteDeducible != null)
//				return false;
//		} else if (!valorMaximoLimiteDeducible
//				.equals(other.valorMaximoLimiteDeducible))
//			return false;
//		if (valorMinimoLimiteDeducible == null) {
//			if (other.valorMinimoLimiteDeducible != null)
//				return false;
//		} else if (!valorMinimoLimiteDeducible
//				.equals(other.valorMinimoLimiteDeducible))
//			return false;
//		if (valorPrimaNeta == null) {
//			if (other.valorPrimaNeta != null)
//				return false;
//		} else if (!valorPrimaNeta.equals(other.valorPrimaNeta))
//			return false;
		if (valorSumaAsegurada == null) {
			if (other.valorSumaAsegurada != null)
				return false;
		} else if (!valorSumaAsegurada.equals(other.valorSumaAsegurada))
			return false;
//		if (verAgrupadorTarifaId == null) {
//			if (other.verAgrupadorTarifaId != null)
//				return false;
//		} else if (!verAgrupadorTarifaId.equals(other.verAgrupadorTarifaId))
//			return false;
//		if (verCargaId == null) {
//			if (other.verCargaId != null)
//				return false;
//		} else if (!verCargaId.equals(other.verCargaId))
//			return false;
//		if (verTarifaExtId == null) {
//			if (other.verTarifaExtId != null)
//				return false;
//		} else if (!verTarifaExtId.equals(other.verTarifaExtId))
//			return false;
//		if (verTarifaId == null) {
//			if (other.verTarifaId != null)
//				return false;
//		} else if (!verTarifaId.equals(other.verTarifaId))
//			return false;
		if (diasSalarioMinimo == null) {
			if (other.diasSalarioMinimo != null)
				return false;
		} else if (!diasSalarioMinimo.equals(other.diasSalarioMinimo))
			return false;
		
		return true;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("subRamoId: " + this.subRamoId + ", ");
		sb.append("coberturaSeccionDTO: " + this.coberturaSeccionDTO);
		sb.append("]");
		
		return sb.toString();
	}
	public void setDiasSalarioMinimo(Integer diasSalarioMinimo) {
		this.diasSalarioMinimo = diasSalarioMinimo;
	}
	public Integer getDiasSalarioMinimo() {
		return diasSalarioMinimo;
	}
	public Boolean getConsultaAsegurada() {
		return consultaAsegurada;
	}
	public void setConsultaAsegurada(Boolean consultaAsegurada) {
		this.consultaAsegurada = consultaAsegurada;
	}
}
