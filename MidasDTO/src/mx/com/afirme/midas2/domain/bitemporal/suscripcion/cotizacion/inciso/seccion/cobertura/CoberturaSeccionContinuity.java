package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.SeccionIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.aumento.BitemporalCoberturaAumento;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.aumento.CoberturaAumento;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.aumento.CoberturaAumentoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.descuento.BitemporalCoberturaDescuento;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.descuento.CoberturaDescuento;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.descuento.CoberturaDescuentoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.detalle.BitemporalCoberturaDetallePrima;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.detalle.CoberturaDetallePrima;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.detalle.CoberturaDetallePrimaContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.recargo.BitemporalCoberturaRecargo;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.recargo.CoberturaRecargo;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.recargo.CoberturaRecargoContinuity;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.afirme.bitemporal.annotations.BitemporalCascadeEnd;
import com.afirme.bitemporal.annotations.BitemporalNotNull;
import com.anasoft.os.daofusion.bitemporal.BitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.ContinuityCollectionWrapper;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;
import com.anasoft.os.daofusion.bitemporal.WrappedBitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.WrappedValueAccessor;

@Entity
@Table(name="MCOBERTURASECCIONC",schema="MIDAS")
public class CoberturaSeccionContinuity implements Serializable,
		EntidadContinuity<CoberturaSeccion, BitemporalCoberturaSeccion> {

	public CoberturaSeccionContinuity() {
		if(seccionIncisoContinuity == null) {
			seccionIncisoContinuity = new SeccionIncisoContinuity();
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 6092924063053684904L;
	
	public static final String PARENT_KEY_NAME = "seccionIncisoContinuity.id";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQMCOBERTURASECCIONCONTID")
	@SequenceGenerator(name = "SEQMCOBERTURASECCIONCONTID", sequenceName = "MIDAS.SEQMCOBERTURASECCIONCONTID", allocationSize = 1)
	@Column(name = "id")
	private Long id;
	
	
	
	@OneToMany(mappedBy="continuity",cascade = CascadeType.ALL, orphanRemoval=true)
	private Collection<BitemporalCoberturaSeccion> coberturaSecciones = new LinkedList<BitemporalCoberturaSeccion>();
	
	@BitemporalNotNull
	@ManyToOne
	@JoinColumn(name="MSECCIONINCISOC_ID", referencedColumnName="id", nullable=false)
	private SeccionIncisoContinuity seccionIncisoContinuity = new SeccionIncisoContinuity();
	
	@BitemporalCascadeEnd
	@OneToMany(mappedBy="coberturaSeccionContinuity", cascade=CascadeType.ALL, orphanRemoval=true)
	private Collection<CoberturaAumentoContinuity> coberturaAumentoContinuities = new LinkedList<CoberturaAumentoContinuity>();
	
	@BitemporalCascadeEnd
	@OneToMany(mappedBy="coberturaSeccionContinuity", cascade=CascadeType.ALL, orphanRemoval=true)
	private Collection<CoberturaDescuentoContinuity> coberturaDescuentoContinuities = new LinkedList<CoberturaDescuentoContinuity>();

	@BitemporalCascadeEnd
	@OneToMany(mappedBy="coberturaSeccionContinuity", cascade=CascadeType.ALL, orphanRemoval=true)
	private Collection<CoberturaDetallePrimaContinuity> coberturaDetallePrimaContinuities = new LinkedList<CoberturaDetallePrimaContinuity>();
	
	@BitemporalCascadeEnd
	@OneToMany(mappedBy="coberturaSeccionContinuity", cascade=CascadeType.ALL, orphanRemoval=true)
	private Collection<CoberturaRecargoContinuity> coberturaRecargoContinuities = new LinkedList<CoberturaRecargoContinuity>();
	
	

	@Override
	@Transient
	public BitemporalProperty<CoberturaSeccion, BitemporalCoberturaSeccion> getBitemporalProperty() {
		return getCoberturaSecciones();
	}
	
	@SuppressWarnings("serial")
	public WrappedBitemporalProperty<CoberturaSeccion, BitemporalCoberturaSeccion, CoberturaSeccionContinuity> getCoberturaSecciones() {
		return new WrappedBitemporalProperty<CoberturaSeccion, BitemporalCoberturaSeccion, CoberturaSeccionContinuity>(
				coberturaSecciones,
				new WrappedValueAccessor<CoberturaSeccion, BitemporalCoberturaSeccion, CoberturaSeccionContinuity>() {

					public BitemporalCoberturaSeccion wrapValue(CoberturaSeccion value,
							IntervalWrapper validityInterval, boolean twoPhaseMode) {
						return new BitemporalCoberturaSeccion(value,validityInterval, CoberturaSeccionContinuity.this, null, twoPhaseMode);
					}

				});
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne
	@JoinColumn(name="COBERTURA_ID", referencedColumnName="IDTOCOBERTURA")
	private CoberturaDTO coberturaDTO;
	
	public CoberturaDTO getCoberturaDTO() {
		return coberturaDTO;
	}
	public void setCoberturaDTO(CoberturaDTO coberturaDTO) {
		this.coberturaDTO = coberturaDTO;
	}

	public SeccionIncisoContinuity getSeccionIncisoContinuity() {
		return seccionIncisoContinuity;
	}

	public void setSeccionIncisoContinuity(
			SeccionIncisoContinuity seccionIncisoContinuity) {
		this.seccionIncisoContinuity = seccionIncisoContinuity;
	}


	public ContinuityCollectionWrapper<CoberturaAumento, BitemporalCoberturaAumento, CoberturaAumentoContinuity> getCoberturaAumentoContinuities() {
		return new ContinuityCollectionWrapper<CoberturaAumento, BitemporalCoberturaAumento, CoberturaAumentoContinuity>(coberturaAumentoContinuities);
	}


	public ContinuityCollectionWrapper<CoberturaDescuento, BitemporalCoberturaDescuento, CoberturaDescuentoContinuity>  getCoberturaDescuentoContinuities() {
		return  new ContinuityCollectionWrapper<CoberturaDescuento, BitemporalCoberturaDescuento, CoberturaDescuentoContinuity> (coberturaDescuentoContinuities);
	}


	public ContinuityCollectionWrapper<CoberturaDetallePrima, BitemporalCoberturaDetallePrima, CoberturaDetallePrimaContinuity> getCoberturaDetallePrimaContinuities() {
		return new ContinuityCollectionWrapper<CoberturaDetallePrima, BitemporalCoberturaDetallePrima, CoberturaDetallePrimaContinuity>(coberturaDetallePrimaContinuities);
	}

	public ContinuityCollectionWrapper<CoberturaRecargo, BitemporalCoberturaRecargo, CoberturaRecargoContinuity> getCoberturaRecargoContinuities() {
		return new ContinuityCollectionWrapper<CoberturaRecargo, BitemporalCoberturaRecargo, CoberturaRecargoContinuity> (coberturaRecargoContinuities);
	}


	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((coberturaDTO == null) ? 0 : coberturaDTO.hashCode());
		result = prime
				* result
				+ ((seccionIncisoContinuity == null) ? 0
						: seccionIncisoContinuity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof CoberturaSeccionContinuity))
			return false;
		CoberturaSeccionContinuity other = (CoberturaSeccionContinuity) obj;
		if (coberturaDTO == null) {
			if (other.coberturaDTO != null)
				return false;
		} else if (!coberturaDTO.equals(other.coberturaDTO))
			return false;
		if (seccionIncisoContinuity == null) {
			if (other.seccionIncisoContinuity != null)
				return false;
		} else if (!seccionIncisoContinuity
				.equals(other.seccionIncisoContinuity))
			return false;
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return getId();
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getBusinessKey() {
		return coberturaDTO.getIdToCobertura();
	}

	@SuppressWarnings("unchecked")
	@Override
	public SeccionIncisoContinuity getParentContinuity() {
		return getSeccionIncisoContinuity();
	}
	
	@SuppressWarnings("rawtypes")
	public void setParentContinuity(EntidadContinuity parentContinuity) {
		setSeccionIncisoContinuity((SeccionIncisoContinuity)parentContinuity);
	};

	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("id: " + this.id + ", ");
		sb.append("coberturaDTO: " + this.coberturaDTO);
		sb.append("]");
		
		return sb.toString();
	}
	
	@Override
	public Class<BitemporalCoberturaSeccion> getBitemporalClass() {
		return BitemporalCoberturaSeccion.class;
	}
}
