package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.direccion.DireccionDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoInciso;

@Embeddable
public class Inciso implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name="NUMEROSECUENCIA")
	private Integer numeroSecuencia;
	
	@ManyToOne
	@JoinColumn(name="DIRECCION_ID", referencedColumnName="IDTODIRECCION")
	private DireccionDTO direccion;
	
	@Column(name="VALORPRIMANETA")
	private Double valorPrimaNeta;
	
	@Column(name="CLAVEESTATUSINSPECCION")
	private Short claveEstatusInspeccion;
	
	@Column(name="CLAVETIPOORIGENINSPECCION")
	private Short claveTipoOrigenInspeccion;
	
	@Column(name="CLAVEMENSAJEINSPECCION")
	private Short claveMensajeInspeccion;
	
	@Column(name="CODIGOUSUARIOESTINSPECCION")
	private String codigoUsuarioEstInspeccion;
	
	@Column(name="CLAVEAUTINSPECCION")
	private Short claveAutInspeccion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAESTATUSINSPECCION")
	private Date fechaEstatusInspeccion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHASOLAUTINSPECCION")
	private Date fechaSolAutInspeccion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAAUTINSPECCION")
	private Date fechaAutInspeccion;
	
	@Column(name="DESCRIPCIONGIROASEGURADO")
	private String descripcionGiroAsegurado;
	
	@Column(name = "IGUALACIONNIVELINCISO")
	private Boolean igualacionNivelInciso = Boolean.FALSE;
	
	@Column(name = "IGUALACIONPRIMAANTERIOR")
	private Double primaTotalAntesDeIgualacion;
	
	@Column(name = "IGUALACIONDESCUENTOGENERADO")
	private Double descuentoIgualacionPrimas;
	
	@Column(name = "IDCLIENTECOB")
	private BigDecimal idClienteCob;
	
	@Column(name = "IDMEDIOPAGO")
	private BigDecimal idMedioPago;
	
	@Column(name = "IDCLIENTECONDUCTOCOBRO")
	private Long idConductoCobroCliente;
	
	@Column(name = "EMAILCONTACTO")
	private String emailContacto;
	
	@Column(name = "CONDUCTOCOBRO")
	private String conductoCobro;
	
	@Column(name = "INSTITUCIONBANCARIA")
	private String institucionBancaria;
	
	@Column(name = "TIPOTARJETA")
	private String tipoTarjeta;
	
	@Column(name = "NUMEROTARJETACLAVE")
	private String numeroTarjetaClave;
	
	@Column(name = "CODIGOSEGURIDAD")
	private String codigoSeguridad;
	
	@Column(name = "FECHAVENCIMIENTO")
	private String fechaVencimiento;
	
	@Transient
	private String numeroTarjetaCobranza;
	
	@Transient
	private String descripcionMedioPago;
	
	//transient
	@Transient
	private AutoInciso autoInciso;
	
	@Transient
	private Boolean checked;
	
	@Transient
	private Double valorPrimaTotal;
	
	@Transient
	private Short estatus;
	
	@Transient
	private Double primaTotal;
	
	@Transient
	private Boolean blocked;
	
	@Transient
	private String siguienteModificacionProgramada;
	
	//Campos auxiliares Endosos Pérdida Total
	@Transient
	private Long siniestroCabinaId;
	
	@Transient
	private String numeroSiniestro;
	
	@Transient
	private String tipoIndemnizacion;
	
	@Transient
	private Date fechaOcurrenciaSiniestro;
	
	@Transient
	private Double valorPrimaIgualacion;	

	// private IncisoAutoCot incisoAutoCot = new IncisoAutoCot();
	// private List<SeccionCotizacionDTO> seccionCotizacionList = new
	// ArrayList<SeccionCotizacionDTO>();
	
	public Integer getNumeroSecuencia() {
		return numeroSecuencia;
	}

	public Boolean getIgualacionNivelInciso() {
		return igualacionNivelInciso;
	}

	public void setIgualacionNivelInciso(Boolean igualacionNivelInciso) {
		this.igualacionNivelInciso = igualacionNivelInciso;
	}

	public Double getPrimaTotalAntesDeIgualacion() {
		return primaTotalAntesDeIgualacion;
	}

	public void setPrimaTotalAntesDeIgualacion(Double primaTotalAntesDeIgualacion) {
		this.primaTotalAntesDeIgualacion = primaTotalAntesDeIgualacion;
	}

	public Double getDescuentoIgualacionPrimas() {
		return descuentoIgualacionPrimas;
	}

	public void setDescuentoIgualacionPrimas(Double descuentoIgualacionPrimas) {
		this.descuentoIgualacionPrimas = descuentoIgualacionPrimas;
	}

	public void setNumeroSecuencia(Integer numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}

	
	public DireccionDTO getDireccion() {
		return direccion;
	}

	public void setDireccion(DireccionDTO direccion) {
		this.direccion = direccion;
	}

	
	public Double getValorPrimaNeta() {
		return valorPrimaNeta;
	}

	public void setValorPrimaNeta(Double valorPrimaNeta) {
		this.valorPrimaNeta = valorPrimaNeta;
	}

	
	public Short getClaveEstatusInspeccion() {
		return claveEstatusInspeccion;
	}

	public void setClaveEstatusInspeccion(Short claveEstatusInspeccion) {
		this.claveEstatusInspeccion = claveEstatusInspeccion;
	}


	public Short getClaveTipoOrigenInspeccion() {
		return claveTipoOrigenInspeccion;
	}

	public void setClaveTipoOrigenInspeccion(Short claveTipoOrigenInspeccion) {
		this.claveTipoOrigenInspeccion = claveTipoOrigenInspeccion;
	}

	
	public Short getClaveMensajeInspeccion() {
		return claveMensajeInspeccion;
	}

	public void setClaveMensajeInspeccion(Short claveMensajeInspeccion) {
		this.claveMensajeInspeccion = claveMensajeInspeccion;
	}


	public String getCodigoUsuarioEstInspeccion() {
		return codigoUsuarioEstInspeccion;
	}

	public void setCodigoUsuarioEstInspeccion(String codigoUsuarioEstInspeccion) {
		this.codigoUsuarioEstInspeccion = codigoUsuarioEstInspeccion;
	}

	
	public Short getClaveAutInspeccion() {
		return claveAutInspeccion;
	}

	public void setClaveAutInspeccion(Short claveAutInspeccion) {
		this.claveAutInspeccion = claveAutInspeccion;
	}

	
	public Date getFechaEstatusInspeccion() {
		return fechaEstatusInspeccion;
	}

	public void setFechaEstatusInspeccion(Date fechaEstatusInspeccion) {
		this.fechaEstatusInspeccion = fechaEstatusInspeccion;
	}

	
	public Date getFechaSolAutInspeccion() {
		return fechaSolAutInspeccion;
	}

	public void setFechaSolAutInspeccion(Date fechaSolAutInspeccion) {
		this.fechaSolAutInspeccion = fechaSolAutInspeccion;
	}

	
	public Date getFechaAutInspeccion() {
		return fechaAutInspeccion;
	}

	public void setFechaAutInspeccion(Date fechaAutInspeccion) {
		this.fechaAutInspeccion = fechaAutInspeccion;
	}

	
	public String getDescripcionGiroAsegurado() {
		return descripcionGiroAsegurado;
	}

	public void setDescripcionGiroAsegurado(String descripcionGiroAsegurado) {
		this.descripcionGiroAsegurado = descripcionGiroAsegurado;
	}
	


	public AutoInciso getAutoInciso() {
		return autoInciso;
	}
	public void setAutoInciso(AutoInciso autoInciso) {
		this.autoInciso = autoInciso;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((claveAutInspeccion == null) ? 0 : claveAutInspeccion
						.hashCode());
		result = prime
				* result
				+ ((claveEstatusInspeccion == null) ? 0
						: claveEstatusInspeccion.hashCode());
		result = prime
				* result
				+ ((claveMensajeInspeccion == null) ? 0
						: claveMensajeInspeccion.hashCode());
		result = prime
				* result
				+ ((claveTipoOrigenInspeccion == null) ? 0
						: claveTipoOrigenInspeccion.hashCode());
		result = prime
				* result
				+ ((codigoUsuarioEstInspeccion == null) ? 0
						: codigoUsuarioEstInspeccion.hashCode());
		result = prime
				* result
				+ ((descripcionGiroAsegurado == null) ? 0
						: descripcionGiroAsegurado.hashCode());
		result = prime * result
				+ ((direccion == null) ? 0 : direccion.hashCode());
		result = prime
				* result
				+ ((fechaAutInspeccion == null) ? 0 : fechaAutInspeccion
						.hashCode());
		result = prime
				* result
				+ ((fechaEstatusInspeccion == null) ? 0
						: fechaEstatusInspeccion.hashCode());
		result = prime
				* result
				+ ((fechaSolAutInspeccion == null) ? 0 : fechaSolAutInspeccion
						.hashCode());
		result = prime * result
				+ ((numeroSecuencia == null) ? 0 : numeroSecuencia.hashCode());
		result = prime * result
				+ ((valorPrimaNeta == null) ? 0 : valorPrimaNeta.hashCode());
		
		result = prime * result
				+ ((conductoCobro == null) ? 0 : conductoCobro.hashCode());
		result = prime * result
				+ ((institucionBancaria == null) ? 0 : institucionBancaria.hashCode());
		result = prime * result
				+ ((tipoTarjeta == null) ? 0 : tipoTarjeta.hashCode());
		result = prime * result
				+ ((numeroTarjetaClave == null) ? 0 : numeroTarjetaClave.hashCode());
		result = prime * result
				+ ((codigoSeguridad == null) ? 0 : codigoSeguridad.hashCode());
		result = prime * result
				+ ((fechaVencimiento == null) ? 0 : fechaVencimiento.hashCode());
		
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Inciso))
			return false;
		Inciso other = (Inciso) obj;
		if (claveAutInspeccion == null) {
			if (other.claveAutInspeccion != null)
				return false;
		} else if (!claveAutInspeccion.equals(other.claveAutInspeccion))
			return false;
		if (claveEstatusInspeccion == null) {
			if (other.claveEstatusInspeccion != null)
				return false;
		} else if (!claveEstatusInspeccion.equals(other.claveEstatusInspeccion))
			return false;
		if (claveMensajeInspeccion == null) {
			if (other.claveMensajeInspeccion != null)
				return false;
		} else if (!claveMensajeInspeccion.equals(other.claveMensajeInspeccion))
			return false;
		if (claveTipoOrigenInspeccion == null) {
			if (other.claveTipoOrigenInspeccion != null)
				return false;
		} else if (!claveTipoOrigenInspeccion
				.equals(other.claveTipoOrigenInspeccion))
			return false;
		if (codigoUsuarioEstInspeccion == null) {
			if (other.codigoUsuarioEstInspeccion != null)
				return false;
		} else if (!codigoUsuarioEstInspeccion
				.equals(other.codigoUsuarioEstInspeccion))
			return false;
		if (descripcionGiroAsegurado == null) {
			if (other.descripcionGiroAsegurado != null)
				return false;
		} else if (!descripcionGiroAsegurado
				.equals(other.descripcionGiroAsegurado))
			return false;
		if (direccion == null) {
			if (other.direccion != null)
				return false;
		} else if (!direccion.equals(other.direccion))
			return false;
		if (fechaAutInspeccion == null) {
			if (other.fechaAutInspeccion != null)
				return false;
		} else if (!fechaAutInspeccion.equals(other.fechaAutInspeccion))
			return false;
		if (fechaEstatusInspeccion == null) {
			if (other.fechaEstatusInspeccion != null)
				return false;
		} else if (!fechaEstatusInspeccion.equals(other.fechaEstatusInspeccion))
			return false;
		if (fechaSolAutInspeccion == null) {
			if (other.fechaSolAutInspeccion != null)
				return false;
		} else if (!fechaSolAutInspeccion.equals(other.fechaSolAutInspeccion))
			return false;
		if (numeroSecuencia == null) {
			if (other.numeroSecuencia != null)
				return false;
		} else if (!numeroSecuencia.equals(other.numeroSecuencia))
			return false;
		if (valorPrimaNeta == null) {
			if (other.valorPrimaNeta != null)
				return false;
		} else if (!valorPrimaNeta.equals(other.valorPrimaNeta))
			return false;
		
		if (conductoCobro == null) {
			if (other.conductoCobro != null)
				return false;
		} else if (!conductoCobro.equals(other.conductoCobro))
			return false;
		if (institucionBancaria == null) {
			if (other.institucionBancaria != null)
				return false;
		} else if (!institucionBancaria.equals(other.institucionBancaria))
			return false;
		
		if (tipoTarjeta == null) {
			if (other.tipoTarjeta != null)
				return false;
		} else if (!tipoTarjeta.equals(other.tipoTarjeta))
			return false;
		
		if (numeroTarjetaClave == null) {
			if (other.numeroTarjetaClave != null)
				return false;
		} else if (!numeroTarjetaClave.equals(other.numeroTarjetaClave))
			return false;
		if (codigoSeguridad == null) {
			if (other.codigoSeguridad != null)
				return false;
		} else if (!codigoSeguridad.equals(other.codigoSeguridad))
			return false;
		if (fechaVencimiento == null) {
			if (other.fechaVencimiento != null)
				return false;
		} else if (!fechaVencimiento.equals(other.fechaVencimiento))
			return false;

		return true;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("numeroSecuencia: " + this.numeroSecuencia + ", ");
		sb.append("valorPrimaNeta: " + this.valorPrimaNeta + ", ");
		sb.append("descripcionGiroAsegurado: " + this.descripcionGiroAsegurado + ", ");
		sb.append("autoInciso: " + this.autoInciso);
		sb.append("]");
		
		return sb.toString();
	}



	public Boolean getChecked() {
		return checked;
	}

	public void setChecked(Boolean checked) {
		this.checked = checked;
	}

	public Double getValorPrimaTotal() {
		return valorPrimaTotal;
	}

	public void setValorPrimaTotal(Double valorPrimaTotal) {
		this.valorPrimaTotal = valorPrimaTotal;
	}

	public Short getEstatus() {
		return estatus;
	}

	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}

	public Double getPrimaTotal() {
		return primaTotal;
	}

	public void setPrimaTotal(Double primaTotal) {
		this.primaTotal = primaTotal;
	}

	public Boolean getBlocked() {
		return blocked;
	}

	public void setBlocked(Boolean blocked) {
		this.blocked = blocked;
	}

	public String getSiguienteModificacionProgramada() {
		return siguienteModificacionProgramada;
	}

	public void setSiguienteModificacionProgramada(
			String siguienteModificacionProgramada) {
		this.siguienteModificacionProgramada = siguienteModificacionProgramada;
	}

	public void setIdClienteCob(BigDecimal idClienteCob) {
		this.idClienteCob = idClienteCob;
	}

	public BigDecimal getIdClienteCob() {
		return idClienteCob;
	}

	public void setIdMedioPago(BigDecimal idMedioPago) {
		this.idMedioPago = idMedioPago;
	}

	public BigDecimal getIdMedioPago() {
		return idMedioPago;
	}

	public void setIdConductoCobroCliente(Long idConductoCobroCliente) {
		this.idConductoCobroCliente = idConductoCobroCliente;
	}

	public Long getIdConductoCobroCliente() {
		return idConductoCobroCliente;
	}

	public void setNumeroTarjetaCobranza(String numeroTarjetaCobranza) {
		this.numeroTarjetaCobranza = numeroTarjetaCobranza;
	}

	public String getNumeroTarjetaCobranza() {
		return numeroTarjetaCobranza;
	}

	public void setDescripcionMedioPago(String descripcionMedioPago) {
		this.descripcionMedioPago = descripcionMedioPago;
	}

	public String getDescripcionMedioPago() {
		return descripcionMedioPago;
	}

	public void setEmailContacto(String emailContacto) {
		this.emailContacto = emailContacto;
	}

	public String getEmailContacto() {
		return emailContacto;
	}

	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}

	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}

	public String getTipoIndemnizacion() {
		return tipoIndemnizacion;
	}

	public void setTipoIndemnizacion(String tipoIndemnizacion) {
		this.tipoIndemnizacion = tipoIndemnizacion;
	}

	public Date getFechaOcurrenciaSiniestro() {
		return fechaOcurrenciaSiniestro;
	}

	public void setFechaOcurrenciaSiniestro(Date fechaOcurrenciaSiniestro) {
		this.fechaOcurrenciaSiniestro = fechaOcurrenciaSiniestro;
	}

	public Long getSiniestroCabinaId() {
		return siniestroCabinaId;
	}

	public void setSiniestroCabinaId(Long siniestroCabinaId) {
		this.siniestroCabinaId = siniestroCabinaId;
	}

	public void setValorPrimaIgualacion(Double valorPrimaIgualacion) {
		this.valorPrimaIgualacion = valorPrimaIgualacion;
	}

	public Double getValorPrimaIgualacion() {
		return valorPrimaIgualacion;
	}

	public String getConductoCobro() {
		return conductoCobro;
	}

	public void setConductoCobro(String conductoCobro) {
		this.conductoCobro = conductoCobro;
	}

	public String getInstitucionBancaria() {
		return institucionBancaria;
	}

	public void setInstitucionBancaria(String institucionBancaria) {
		this.institucionBancaria = institucionBancaria;
	}

	public String getTipoTarjeta() {
		return tipoTarjeta;
	}

	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	public String getNumeroTarjetaClave() {
		return numeroTarjetaClave;
	}

	public void setNumeroTarjetaClave(String numeroTarjetaClave) {
		this.numeroTarjetaClave = numeroTarjetaClave;
	}

	public String getCodigoSeguridad() {
		return codigoSeguridad;
	}

	public void setCodigoSeguridad(String codigoSeguridad) {
		this.codigoSeguridad = codigoSeguridad;
	}

	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
}