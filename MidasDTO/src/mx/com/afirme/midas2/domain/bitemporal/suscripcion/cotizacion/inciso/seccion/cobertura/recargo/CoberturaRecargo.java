package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.recargo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class CoberturaRecargo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5617638996613946593L;
	
	@Column(name="CLAVEAUTORIZACION")
	private Long claveAutorizacion;
	
	@Column(name="CODIGOUSUARIOAUTORIZACION")
	private String codigoUsuarioAutorizacion;
	
	@Column(name="VALORRECARGO")
	private BigDecimal valorRecargo;
	
	@Column(name="CLAVEOBLIGATORIEDAD")
	private Short claveObligatoriedad;
	
	@Column(name="CLAVECONTRATO")
	private Short claveContrato;
	
	@Column(name="CLAVECOMERCIALTECNICO")
	private Short claveComercialTecnico;
	
	@Column(name="CLAVENIVEL")
	private Short claveNivel;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHASOLICITUDAUTORIZACION")
	private Date fechaSolicitudAutorizacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAAUTORIZACION")
	private Date fechaAutorizacion;
	


	public Long getClaveAutorizacion() {
		return claveAutorizacion;
	}
	public void setClaveAutorizacion(Long claveAutorizacion) {
		this.claveAutorizacion = claveAutorizacion;
	}
	
	
	public String getCodigoUsuarioAutorizacion() {
		return codigoUsuarioAutorizacion;
	}
	public void setCodigoUsuarioAutorizacion(String codigoUsuarioAutorizacion) {
		this.codigoUsuarioAutorizacion = codigoUsuarioAutorizacion;
	}
	
	
	public BigDecimal getValorRecargo() {
		return valorRecargo;
	}
	public void setValorRecargo(BigDecimal valorRecargo) {
		this.valorRecargo = valorRecargo;
	}
	
	
	public Short getClaveObligatoriedad() {
		return claveObligatoriedad;
	}
	public void setClaveObligatoriedad(Short claveObligatoriedad) {
		this.claveObligatoriedad = claveObligatoriedad;
	}
	
	
	public Short getClaveContrato() {
		return claveContrato;
	}
	public void setClaveContrato(Short claveContrato) {
		this.claveContrato = claveContrato;
	}
	

	public Short getClaveComercialTecnico() {
		return claveComercialTecnico;
	}
	public void setClaveComercialTecnico(Short claveComercialTecnico) {
		this.claveComercialTecnico = claveComercialTecnico;
	}
	

	public Short getClaveNivel() {
		return claveNivel;
	}
	public void setClaveNivel(Short claveNivel) {
		this.claveNivel = claveNivel;
	}
	

	public Date getFechaSolicitudAutorizacion() {
		return fechaSolicitudAutorizacion;
	}
	public void setFechaSolicitudAutorizacion(Date fechaSolicitudAutorizacion) {
		this.fechaSolicitudAutorizacion = fechaSolicitudAutorizacion;
	}
	

	public Date getFechaAutorizacion() {
		return fechaAutorizacion;
	}
	public void setFechaAutorizacion(Date fechaAutorizacion) {
		this.fechaAutorizacion = fechaAutorizacion;
	}
	
}
