package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.recargo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.anasoft.os.daofusion.bitemporal.Bitemporal;
import com.anasoft.os.daofusion.bitemporal.BitemporalWrapper;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;

@Entity
@Table(name="MCOBERTURARECARGOB",schema="MIDAS")
@Access(AccessType.FIELD)
public class BitemporalCoberturaRecargo extends
		BitemporalWrapper<CoberturaRecargo, CoberturaRecargoContinuity> implements EntidadBitemporal<CoberturaRecargo,BitemporalCoberturaRecargo> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4287034136698047550L;
	@Embedded
	private CoberturaRecargo value;
	@ManyToOne
	@JoinColumn(name="MCOBERTURARECARGOC_ID", referencedColumnName="id")
	private CoberturaRecargoContinuity continuity;


	public BitemporalCoberturaRecargo() {
		if(continuity == null) {
			continuity = new CoberturaRecargoContinuity();
		}
		if(value == null) {
			value = new CoberturaRecargo();
		}
	}

	public BitemporalCoberturaRecargo(CoberturaRecargo value,
			IntervalWrapper validityInterval,CoberturaRecargoContinuity continuity,String valueId, boolean twoPhaseMode) {
		super(value, validityInterval, continuity, valueId, twoPhaseMode);
	}

	@Override
	public Bitemporal copyWith(IntervalWrapper validityInterval, boolean twoPhaseMode) {
		return new BitemporalCoberturaRecargo(value, validityInterval,getContinuity(),getValueId(), twoPhaseMode);
	}

	@Override
	public void setValue(CoberturaRecargo value) {
		this.value=value;

	}
	
	@Override
	public CoberturaRecargo getValue() {
		return value;
	}

	@Override
	public CoberturaRecargoContinuity getContinuity() {
		return continuity;
	}

	@Override
	protected void setContinuity(CoberturaRecargoContinuity continuity) {
		this.continuity = continuity;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public CoberturaRecargoContinuity getEntidadContinuity() {
		return getContinuity();
	}
	
	public void setEntidadContinuity(EntidadContinuity<CoberturaRecargo,BitemporalCoberturaRecargo> entidadContinuity){
		this.continuity = (CoberturaRecargoContinuity) entidadContinuity;
	}

	@Override
	public CoberturaRecargo  getEmbedded() {
		return this.value;
	}
	
	@Override
	public void setEmbedded(CoberturaRecargo value) {
		this.value = value;
	}


}
