/**
 * 
 */
package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author admin
 *
 */
@Embeddable
public class CondicionEspCotizacion implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name="CLAVEAUTORIZACION")
	private Short claveAutorizacion;
	
	
	@Column(name="CODIGOUSUARIOAUTORIZACION")
	private String codigoUsuarioAutorizacion;
	
	
	@Column(name="CODIGOUSUARIOCREACION")
	private String codigoUsuarioCreacion;
	
	@Column(name="NOMBREUSUARIOCREACION")
	private String nombreUsuarioCreacion;

	public Short getClaveAutorizacion() {
		return claveAutorizacion;
	}

	public void setClaveAutorizacion(Short claveAutorizacion) {
		this.claveAutorizacion = claveAutorizacion;
	}

	public String getCodigoUsuarioAutorizacion() {
		return codigoUsuarioAutorizacion;
	}

	public void setCodigoUsuarioAutorizacion(String codigoUsuarioAutorizacion) {
		this.codigoUsuarioAutorizacion = codigoUsuarioAutorizacion;
	}

	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	public String getNombreUsuarioCreacion() {
		return nombreUsuarioCreacion;
	}

	public void setNombreUsuarioCreacion(String nombreUsuarioCreacion) {
		this.nombreUsuarioCreacion = nombreUsuarioCreacion;
	}
	
	
	

}
