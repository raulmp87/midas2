package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.documento;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.anasoft.os.daofusion.bitemporal.Bitemporal;
import com.anasoft.os.daofusion.bitemporal.BitemporalWrapper;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;

@Entity
@Table(name="MDOCANEXOCOTB",schema="MIDAS")
@Access(AccessType.FIELD)
public class BitemporalDocAnexoCot extends BitemporalWrapper<DocAnexoCot, DocAnexoCotContinuity> implements EntidadBitemporal<DocAnexoCot, BitemporalDocAnexoCot> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3835577679950318072L;
	@Embedded
	private DocAnexoCot value;
	@ManyToOne
	@JoinColumn(name = "MDOCANEXOCOTC_ID", referencedColumnName="id")
	private DocAnexoCotContinuity continuity;
	
	
	public BitemporalDocAnexoCot(){
		if (continuity == null) {
			continuity = new DocAnexoCotContinuity();
		}
		if (value == null) {
			value = new DocAnexoCot();
		}
	}
	
	public BitemporalDocAnexoCot(DocAnexoCot value, IntervalWrapper validityInterval, DocAnexoCotContinuity continuity, String valueId, boolean twoPhaseMode) {
		super(value, validityInterval,continuity,valueId, twoPhaseMode);
	}

	public BitemporalDocAnexoCot(DocAnexoCot value, DocAnexoCotContinuity continuity) {
		this.value = value;
		this.continuity = continuity;
	}
	
	@Override
	public Bitemporal copyWith(IntervalWrapper validityInterval, boolean twoPhaseMode) {
		return new BitemporalDocAnexoCot(value, validityInterval, getContinuity(), getValueId(), twoPhaseMode);
	}

	@Override
	public DocAnexoCot getValue() {
		return value;
	}

	@Override
	public void setValue(DocAnexoCot value) {
		this.value = value;
	}
	
	@Override
	public DocAnexoCotContinuity getContinuity() {
		return continuity;
	}
	
	@Override
	protected void setContinuity(DocAnexoCotContinuity continuity) {
		this.continuity = continuity;
	}
	
	@Override
	public DocAnexoCot  getEmbedded() {
		return this.value;
	}
	
	@Override
	public void setEmbedded(DocAnexoCot value) {
		this.value = value;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public DocAnexoCotContinuity getEntidadContinuity() {
		return getContinuity();
	}
	
	public void setEntidadContinuity(EntidadContinuity<DocAnexoCot, BitemporalDocAnexoCot> entidadContinuity) {
		this.continuity = (DocAnexoCotContinuity) entidadContinuity;
	}

}
