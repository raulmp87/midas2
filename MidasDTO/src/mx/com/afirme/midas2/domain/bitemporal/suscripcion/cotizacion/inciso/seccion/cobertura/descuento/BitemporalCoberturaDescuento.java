package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.descuento;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.anasoft.os.daofusion.bitemporal.Bitemporal;
import com.anasoft.os.daofusion.bitemporal.BitemporalWrapper;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;

@Entity
@Table(name="MCOBERTURADESCUENTOB",schema="MIDAS")
@Access(AccessType.FIELD)
public class BitemporalCoberturaDescuento extends
		BitemporalWrapper<CoberturaDescuento, CoberturaDescuentoContinuity> implements EntidadBitemporal<CoberturaDescuento,BitemporalCoberturaDescuento>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7324043922164462850L;
	@Embedded
	private CoberturaDescuento value;
	@ManyToOne
	@JoinColumn(name="MCOBERTURADESCUENTOC_ID", referencedColumnName="id")
	private CoberturaDescuentoContinuity continuity;
	

	public BitemporalCoberturaDescuento() {
		if(continuity == null) {
			continuity = new CoberturaDescuentoContinuity();
		}
		if(value == null) {
			value = new CoberturaDescuento();
		}
	}

	public BitemporalCoberturaDescuento(CoberturaDescuento value,
			IntervalWrapper validityInterval,CoberturaDescuentoContinuity continuity,String valueId, boolean twoPhaseMode) {
		super(value, validityInterval, continuity,valueId, twoPhaseMode);
	}

	@Override
	public Bitemporal copyWith(IntervalWrapper validityInterval, boolean twoPhaseMode) {
		return new BitemporalCoberturaDescuento(value, validityInterval,getContinuity(),getValueId(),  twoPhaseMode);
	}

	@Override
	public void setValue(CoberturaDescuento value) {
		this.value = value;

	}

	@Override
	public CoberturaDescuento getValue() {
		return value;
	}
	
	@Override
	public CoberturaDescuento getEmbedded() {
		return this.value;
	}
	
	@Override
	public void setEmbedded(CoberturaDescuento value) {
		this.value = value;
	}
	
	@Override
	public CoberturaDescuentoContinuity getContinuity() {
		return continuity;
	}

	@Override
	protected void setContinuity(CoberturaDescuentoContinuity continuity) {
		this.continuity = continuity;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public CoberturaDescuentoContinuity getEntidadContinuity() {
		return getContinuity();
	}
	
	public void setEntidadContinuity(EntidadContinuity<CoberturaDescuento,BitemporalCoberturaDescuento> entidadContinuity){
		this.continuity = (CoberturaDescuentoContinuity) entidadContinuity;
	}


}
