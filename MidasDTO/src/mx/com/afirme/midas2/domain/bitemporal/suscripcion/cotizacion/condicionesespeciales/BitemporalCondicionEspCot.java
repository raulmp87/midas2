package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.anasoft.os.daofusion.bitemporal.Bitemporal;
import com.anasoft.os.daofusion.bitemporal.BitemporalWrapper;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;


@Entity
@Table(name="MCONDICIONESPECIALCOTIZACIONB",schema="MIDAS")
@Access(AccessType.FIELD)
public class BitemporalCondicionEspCot extends BitemporalWrapper<CondicionEspCotizacion, CondicionEspCotContinuity> implements EntidadBitemporal<CondicionEspCotizacion, BitemporalCondicionEspCot>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Embedded
	private CondicionEspCotizacion value;
	
	@ManyToOne
	@JoinColumn(name = "MCONDESPCOTIZACIONC_ID", referencedColumnName = "id")
	private CondicionEspCotContinuity continuity;
	

	
	public BitemporalCondicionEspCot(){
		if (continuity == null) {
			continuity = new CondicionEspCotContinuity();
		}
		if (value == null) {
			value = new CondicionEspCotizacion();
		}
	}
	
	
	public BitemporalCondicionEspCot(CondicionEspCotizacion value, IntervalWrapper validityInterval, CondicionEspCotContinuity continuity, String valueId, boolean twoPhaseMode) {
		super(value, validityInterval,continuity,valueId, twoPhaseMode);
	}

	public BitemporalCondicionEspCot(CondicionEspCotizacion value, CondicionEspCotContinuity continuity){
		this.value = value;
		this.continuity = continuity;
	}
	

	@Override
	public Bitemporal copyWith(IntervalWrapper validityInterval,
			boolean twoPhaseMode) {
		return new BitemporalCondicionEspCot(value,validityInterval,getContinuity(),getValueId(),twoPhaseMode);
	}

	@Override
	public CondicionEspCotizacion getEmbedded() {
		return this.value;
	}

	@Override
	public void setEmbedded(CondicionEspCotizacion value) {
		this.value = value;
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public CondicionEspCotContinuity getEntidadContinuity() {
		return getContinuity();
	}

	@Override
	public void setEntidadContinuity(
			EntidadContinuity<CondicionEspCotizacion, BitemporalCondicionEspCot> entidadContinuity) {
		this.continuity = (CondicionEspCotContinuity) entidadContinuity;
		
	}

	@Override
	protected void setValue(CondicionEspCotizacion value) {
		this.value = value;
		
	}

	@Override
	public CondicionEspCotizacion getValue() {
		return value;
	}

	@Override
	public CondicionEspCotContinuity getContinuity() {
		return continuity;
	}

	@Override
	protected void setContinuity(CondicionEspCotContinuity continuity) {
		this.continuity = continuity;
		
	}


}
