package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class SeccionInciso implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6586130965757404963L;
	
	@Column(name="VALORPRIMANETA")
	private Double valorPrimaNeta;
	
	@Column(name="VALORSUMAASEGURADA")
	private Double valorSumaAsegurada;
	
	@Column(name="CLAVEOBLIGATORIEDAD")
	private Short claveObligatoriedad;
	
	@Column(name="CLAVECONTRATO")
	private Short claveContrato;
	
	

	public Double getValorPrimaNeta() {
		return valorPrimaNeta;
	}
	public void setValorPrimaNeta(Double valorPrimaNeta) {
		this.valorPrimaNeta = valorPrimaNeta;
	}
	
	
	public Double getValorSumaAsegurada() {
		return valorSumaAsegurada;
	}
	public void setValorSumaAsegurada(Double valorSumaAsegurada) {
		this.valorSumaAsegurada = valorSumaAsegurada;
	}
	

	public Short getClaveObligatoriedad() {
		return claveObligatoriedad;
	}
	public void setClaveObligatoriedad(Short claveObligatoriedad) {
		this.claveObligatoriedad = claveObligatoriedad;
	}
	
	public Short getClaveContrato() {
		return claveContrato;
	}
	public void setClaveContrato(Short claveContrato) {
		this.claveContrato = claveContrato;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("claveContrato: " + this.claveContrato + ", ");
		sb.append("claveObligatoriedad: " + this.claveObligatoriedad + ", ");
		sb.append("valorPrimaNeta: " + this.valorPrimaNeta + ", ");
		sb.append("valorSumaAsegurada: " + this.valorSumaAsegurada + ", ");
		sb.append("]");
		
		return sb.toString();
	}
}
