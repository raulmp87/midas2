package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.detalle;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CoberturaDetallePrima implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2212135323329836410L;
	
	@Column(name="VALORPRIMANETAB")
    private Double valorPrimaNetaB;
	
	@Column(name="VALORPRIMANETAARDT")
    private Double valorPrimaNetaARDT;
	
	@Column(name="VALORPRIMANETA")
    private Double valorPrimaNeta;
	
	@Column(name="VALORCUOTAB")
    private Double valorCuotaB;
	
	@Column(name="VALORCUOTAARDT")
    private Double valorCuotaARDT;
	
	@Column(name="VALORCUOTAARDV")
    private Double valorCuotaARDV;
	
	@Column(name="VALORCUOTA")
    private Double valorCuota;
	

	public Double getValorPrimaNetaB() {
		return valorPrimaNetaB;
	}
	public void setValorPrimaNetaB(Double valorPrimaNetaB) {
		this.valorPrimaNetaB = valorPrimaNetaB;
	}
	

	public Double getValorPrimaNetaARDT() {
		return valorPrimaNetaARDT;
	}
	public void setValorPrimaNetaARDT(Double valorPrimaNetaARDT) {
		this.valorPrimaNetaARDT = valorPrimaNetaARDT;
	}
	

	public Double getValorPrimaNeta() {
		return valorPrimaNeta;
	}
	public void setValorPrimaNeta(Double valorPrimaNeta) {
		this.valorPrimaNeta = valorPrimaNeta;
	}
	

	public Double getValorCuotaB() {
		return valorCuotaB;
	}
	public void setValorCuotaB(Double valorCuotaB) {
		this.valorCuotaB = valorCuotaB;
	}
	

	public Double getValorCuotaARDT() {
		return valorCuotaARDT;
	}
	public void setValorCuotaARDT(Double valorCuotaARDT) {
		this.valorCuotaARDT = valorCuotaARDT;
	}
	

	public Double getValorCuotaARDV() {
		return valorCuotaARDV;
	}
	public void setValorCuotaARDV(Double valorCuotaARDV) {
		this.valorCuotaARDV = valorCuotaARDV;
	}
	

	public Double getValorCuota() {
		return valorCuota;
	}
	public void setValorCuota(Double valorCuota) {
		this.valorCuota = valorCuota;
	}
  
}
