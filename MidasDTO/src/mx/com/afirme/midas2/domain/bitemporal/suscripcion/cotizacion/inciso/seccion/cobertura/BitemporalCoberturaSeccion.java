package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.MovimientoEndoso;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.anasoft.os.daofusion.bitemporal.Bitemporal;
import com.anasoft.os.daofusion.bitemporal.BitemporalWrapper;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;
@Entity
@Table(name="MCOBERTURASECCIONB",schema="MIDAS")
@Access(AccessType.FIELD)
public class BitemporalCoberturaSeccion extends
		BitemporalWrapper<CoberturaSeccion, CoberturaSeccionContinuity> implements EntidadBitemporal<CoberturaSeccion, BitemporalCoberturaSeccion> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4683661962762401317L;
	@Embedded
	private CoberturaSeccion value;
	@ManyToOne
	@JoinColumn(name = "MCOBERTURASECCIONC_ID", referencedColumnName="id")
	private CoberturaSeccionContinuity continuity;
	

	public BitemporalCoberturaSeccion() {
		if(continuity == null) {
			continuity = new CoberturaSeccionContinuity();
		}
		if(value == null) {
			value = new CoberturaSeccion();
		}
	}
	public BitemporalCoberturaSeccion(CoberturaSeccion value,CoberturaSeccionContinuity continuity){
		this.value = value;
		this.continuity = continuity;
	}

	public BitemporalCoberturaSeccion(CoberturaSeccion value,
			IntervalWrapper validityInterval, CoberturaSeccionContinuity continuity, String valueId,boolean twoPhaseMode) {
		super(value, validityInterval, continuity, valueId,twoPhaseMode);
	}

	@Override
	public Bitemporal copyWith(IntervalWrapper validityInterval, boolean twoPhaseMode) {
		return new BitemporalCoberturaSeccion(value, validityInterval, getContinuity(), getValueId(), twoPhaseMode);
	}

	@Override
	public CoberturaSeccion getValue() {
		return value;
	}

	@Override
	public void setValue(CoberturaSeccion value) {
		this.value = value;
	}
	
	@Override
	public CoberturaSeccion  getEmbedded() {
		return this.value;
	}
	
	@Override
	public void setEmbedded(CoberturaSeccion value) {
		this.value = value;
	}
	
	@Override
	public CoberturaSeccionContinuity getContinuity() {
		return continuity;
	}

	@Override
	protected void setContinuity(CoberturaSeccionContinuity continuity) {
		this.continuity = continuity;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public CoberturaSeccionContinuity getEntidadContinuity() {
		return getContinuity();
	}
	
	public void setEntidadContinuity(EntidadContinuity<CoberturaSeccion,BitemporalCoberturaSeccion> entidadContinuity){
		this.continuity = (CoberturaSeccionContinuity) entidadContinuity;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((continuity == null) ? 0 : continuity.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		result = prime * result + ((super.getId() == null) ? 0 : super.getId().hashCode());
		return result;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		
		if (obj instanceof MovimientoEndoso) {
			MovimientoEndoso movimiento = (MovimientoEndoso) obj;
			
			if (continuity == null) {
				if (movimiento.getBitemporalCoberturaSeccion().continuity != null)
					return false;
			} else if (!continuity.equals(movimiento.getBitemporalCoberturaSeccion().continuity))
				return false;
			return true;
		}
		
		if (obj instanceof NegocioCobPaqSeccion) {
			NegocioCobPaqSeccion coberturaNegocio = (NegocioCobPaqSeccion) obj;
			
			if (continuity == null || continuity.getCoberturaDTO() == null) {
				if (coberturaNegocio.getCoberturaDTO() != null)
					return false;
			} else if (coberturaNegocio.getCoberturaDTO() == null 
					|| !continuity.getCoberturaDTO().equals(coberturaNegocio.getCoberturaDTO()))
				return false;
			return true;
		}
		
		if (!(obj instanceof BitemporalCoberturaSeccion))
			return false;
		BitemporalCoberturaSeccion other = (BitemporalCoberturaSeccion) obj;
		if (continuity == null) {
			if (other.continuity != null)
				return false;
		} else if (!continuity.equals(other.continuity))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		if (super.getId() == null) {
			if (other.getId() != null)
				return false;
		} else if (!super.getId().equals(other.getId()))
			return false;
		if (super.getRecordStatus() == null) {
			if (other.getRecordStatus() != null)
				return false;
		} else if (!super.getRecordStatus().equals(other.getRecordStatus()))
			return false;
		if (super.getValidityInterval() == null) {
			if (other.getValidityInterval() != null)
				return false;
		} else if (!super.getValidityInterval().equals(other.getValidityInterval()))
			return false;
		if (super.getRecordInterval() == null) {
			if (other.getRecordInterval() != null)
				return false;
		} else if (!super.getRecordInterval().equals(other.getRecordInterval()))
			return false;
		
		return true;
	}
	
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("continuity: " + this.continuity + ", ");
		sb.append("value: " + this.value);
		sb.append("]");
		
		return sb.toString();
	}

	
}
