package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.CoberturaSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.CoberturaSeccionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.dato.BitemporalDatoSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.dato.DatoSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.dato.DatoSeccionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.subinciso.BitemporalSubIncisoSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.subinciso.SubIncisoSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.subinciso.SubIncisoSeccionContinuity;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.afirme.bitemporal.annotations.BitemporalCascadeEnd;
import com.afirme.bitemporal.annotations.BitemporalNotNull;
import com.anasoft.os.daofusion.bitemporal.BitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.ContinuityCollectionWrapper;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;
import com.anasoft.os.daofusion.bitemporal.WrappedBitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.WrappedValueAccessor;

@Entity
@Table(name="MSECCIONINCISOC",schema="MIDAS")
public class SeccionIncisoContinuity implements Serializable,
		EntidadContinuity<SeccionInciso, BitemporalSeccionInciso> {

	public SeccionIncisoContinuity() {
		if(incisoContinuity == null) {
			incisoContinuity = new IncisoContinuity();
		}
	}
	
	public SeccionIncisoContinuity(IncisoContinuity incisoContinuity){
		this.incisoContinuity = incisoContinuity;	
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -2702240529457718297L;
	
	public static final String PARENT_KEY_NAME = "incisoContinuity.id";
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQMSECCIONINCISOCONTID")
	@SequenceGenerator(name = "SEQMSECCIONINCISOCONTID", sequenceName = "MIDAS.SEQMSECCIONINCISOCONTID", allocationSize = 1)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="SECCION_ID", referencedColumnName="IDTOSECCION")
	private SeccionDTO seccion;
	
	@OneToMany(mappedBy="continuity",cascade = CascadeType.ALL, orphanRemoval=true)
	private Collection<BitemporalSeccionInciso> incisosecciones = new LinkedList<BitemporalSeccionInciso>();
	
	@BitemporalNotNull
	@ManyToOne
	@JoinColumn(name="MINCISOC_ID",referencedColumnName="id", nullable=false)
	private IncisoContinuity incisoContinuity;
	
	@BitemporalCascadeEnd
	@OneToMany(mappedBy="seccionIncisoContinuity", cascade=CascadeType.ALL, orphanRemoval=true)
	private Collection<SubIncisoSeccionContinuity> subIncisoSeccionContiniuties = new LinkedList<SubIncisoSeccionContinuity>();
	
	@BitemporalCascadeEnd
	@OneToMany(mappedBy="seccionIncisoContinuity", cascade=CascadeType.ALL, orphanRemoval=true)
	private Collection<CoberturaSeccionContinuity> coberturaSeccionContinuities = new LinkedList<CoberturaSeccionContinuity>();
		
	@BitemporalCascadeEnd
	@OneToMany(mappedBy="seccionIncisoContinuity", cascade=CascadeType.ALL, orphanRemoval=true)
	private Collection<DatoSeccionContinuity> datoSeccionContinuities = new LinkedList<DatoSeccionContinuity>();
	
	
	@Override
	@Transient
	public BitemporalProperty<SeccionInciso, BitemporalSeccionInciso> getBitemporalProperty() {
		return getIncisoSecciones();
	}
	
	@SuppressWarnings("serial")
	public WrappedBitemporalProperty<SeccionInciso, BitemporalSeccionInciso, SeccionIncisoContinuity> getIncisoSecciones() {
		return new WrappedBitemporalProperty<SeccionInciso, BitemporalSeccionInciso, SeccionIncisoContinuity>(
				incisosecciones,
				new WrappedValueAccessor<SeccionInciso, BitemporalSeccionInciso, SeccionIncisoContinuity>() {

					public BitemporalSeccionInciso wrapValue(SeccionInciso value,
							IntervalWrapper validityInterval, boolean twoPhaseMode) {
						return new BitemporalSeccionInciso(value, validityInterval, SeccionIncisoContinuity.this,null,twoPhaseMode);
					}

				});
	}


	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public SeccionDTO getSeccion() {
		return seccion;
	}
	public void setSeccion(SeccionDTO seccion) {
		this.seccion = seccion;
	}
	
	public IncisoContinuity getIncisoContinuity() {
		return incisoContinuity;
	}

	public void setIncisoContinuity(IncisoContinuity incisoContinuity) {
		this.incisoContinuity = incisoContinuity;
	}


	public Collection<SubIncisoSeccionContinuity> getSubIncisoSeccionContiniuties() {
		return subIncisoSeccionContiniuties;
	}
	public ContinuityCollectionWrapper<SubIncisoSeccion,BitemporalSubIncisoSeccion, SubIncisoSeccionContinuity> getSubIncisoSeccionContiniuties2() {
		return new ContinuityCollectionWrapper<SubIncisoSeccion,BitemporalSubIncisoSeccion, SubIncisoSeccionContinuity>(subIncisoSeccionContiniuties);
	}

	public void setSubIncisoSeccionContiniuties(
			Collection<SubIncisoSeccionContinuity> subIncisoSeccionContiniuties) {
		this.subIncisoSeccionContiniuties = subIncisoSeccionContiniuties;
		
	}

    public ContinuityCollectionWrapper<CoberturaSeccion,BitemporalCoberturaSeccion, CoberturaSeccionContinuity> getCoberturaSeccionContinuities() {
		return new ContinuityCollectionWrapper<CoberturaSeccion,BitemporalCoberturaSeccion, CoberturaSeccionContinuity>(coberturaSeccionContinuities);
	}
        
    public Collection<DatoSeccionContinuity> getDatoSeccionContinuities() {
		return datoSeccionContinuities;
	}
    
    public ContinuityCollectionWrapper<DatoSeccion,BitemporalDatoSeccion, DatoSeccionContinuity> getDatoSeccionContinuities2() {
		return new ContinuityCollectionWrapper<DatoSeccion,BitemporalDatoSeccion, DatoSeccionContinuity>(datoSeccionContinuities);
	}
	
	
	public void setDatoSeccionContinuities(
			Collection<DatoSeccionContinuity> datoSeccionContinuities) {
		this.datoSeccionContinuities = datoSeccionContinuities;
	}

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SeccionIncisoContinuity other = (SeccionIncisoContinuity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return getId();
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getBusinessKey() {
		return seccion.getBusinessKey();
	}

	@SuppressWarnings("unchecked")
	@Override
	public IncisoContinuity getParentContinuity() {
		return getIncisoContinuity();
	}

	@SuppressWarnings("rawtypes")
	public void setParentContinuity(EntidadContinuity parentContinuity) {
		setIncisoContinuity((IncisoContinuity)parentContinuity);
	};

	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("id: " + this.id + ", ");
		sb.append("seccion: " + this.seccion);
		sb.append("]");
		
		return sb.toString();
	}
	
	@Override
	public Class<BitemporalSeccionInciso> getBitemporalClass() {
		return BitemporalSeccionInciso.class;
	}
}
