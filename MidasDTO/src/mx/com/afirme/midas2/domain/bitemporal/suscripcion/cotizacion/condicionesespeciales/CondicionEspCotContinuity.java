/**
 * 
 */
package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.comision.BitemporalComision;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.afirme.bitemporal.annotations.BitemporalNotNull;
import com.anasoft.os.daofusion.bitemporal.BitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;
import com.anasoft.os.daofusion.bitemporal.WrappedBitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.WrappedValueAccessor;

/**
 * @author admin
 *
 */
@Entity
@Table(name = "MCONDICIONESPECIALCOTIZACIONC", schema = "MIDAS")
public class CondicionEspCotContinuity implements Serializable ,EntidadContinuity<CondicionEspCotizacion, BitemporalCondicionEspCot>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String PARENT_KEY_NAME = "cotizacionContinuity.id";
	public static final String PARENT_BUSINESS_KEY_NAME = "cotizacionContinuity.numero";
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDMCONDESPCOTIZACIONC_SEQ")
	@SequenceGenerator(name = "IDMCONDESPCOTIZACIONC_SEQ",  schema="MIDAS", sequenceName = "IDMCONDESPCOTIZACIONC_SEQ", allocationSize = 1)
	@Column(name = "id")
	private Long id;
	
	@Column(name="CONDICIONESPECIAL_ID")
	private Long condicionEspecialId;
	
	@OneToMany(mappedBy = "continuity", cascade = CascadeType.ALL, orphanRemoval=true)
	private Collection<BitemporalCondicionEspCot> condicionesEspecialesCotizacion = new LinkedList<BitemporalCondicionEspCot>();
	
	
	@BitemporalNotNull
	@ManyToOne
	@JoinColumn(name = "MCOTIZACIONC_ID", referencedColumnName = "id", nullable = false)
	private CotizacionContinuity cotizacionContinuity;

	
	

	public CondicionEspCotContinuity() {
		if(cotizacionContinuity == null) {
			cotizacionContinuity = new CotizacionContinuity();
		}
	}
	
	public CondicionEspCotContinuity(CotizacionContinuity cotizacionContinuity) {
		this.cotizacionContinuity = cotizacionContinuity;
	}


	@Override
	@Transient
	public BitemporalProperty<CondicionEspCotizacion, BitemporalCondicionEspCot> getBitemporalProperty() {
		return getCondicionesEspecialesCotizacion();
	}

	@SuppressWarnings("serial")
	public WrappedBitemporalProperty<CondicionEspCotizacion, BitemporalCondicionEspCot, CondicionEspCotContinuity> getCondicionesEspecialesCotizacion() {
		return new WrappedBitemporalProperty<CondicionEspCotizacion, BitemporalCondicionEspCot, CondicionEspCotContinuity>(
				condicionesEspecialesCotizacion,
				new WrappedValueAccessor<CondicionEspCotizacion, BitemporalCondicionEspCot, CondicionEspCotContinuity>() {

					public BitemporalCondicionEspCot wrapValue(CondicionEspCotizacion value,
							IntervalWrapper validityInterval,
							boolean twoPhaseMode) {
						return new BitemporalCondicionEspCot(value, validityInterval,
								CondicionEspCotContinuity.this,null,twoPhaseMode);
					}

				});
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}


	@Override
	public String getValue() {
		return null;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.id;
	}


	@SuppressWarnings("unchecked")
	@Override
	public CotizacionContinuity getParentContinuity() {
		return getCotizacionContinuity();
	}


	@SuppressWarnings("rawtypes")
	@Override
	public void setParentContinuity(EntidadContinuity parentContinuity) {
		setCotizacionContinuity((CotizacionContinuity) parentContinuity);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CotizacionContinuity getCotizacionContinuity() {
		return cotizacionContinuity;
	}

	public void setCotizacionContinuity(CotizacionContinuity cotizacionContinuity) {
		this.cotizacionContinuity = cotizacionContinuity;
	}	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CondicionEspCotContinuity other = (CondicionEspCotContinuity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getCondicionEspecialId() {
		return condicionEspecialId;
	}

	public void setCondicionEspecialId(Long condicionEspecialId) {
		this.condicionEspecialId = condicionEspecialId;
	}

	@Override
	public Class<BitemporalCondicionEspCot> getBitemporalClass() {
		return BitemporalCondicionEspCot.class;
	}

}
