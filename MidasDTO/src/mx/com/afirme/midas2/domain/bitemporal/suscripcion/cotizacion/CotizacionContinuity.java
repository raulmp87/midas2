package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.LinkedList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.comision.BitemporalComision;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.comision.Comision;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.comision.ComisionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.BitemporalCondicionEspCot;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.CondicionEspCotContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.CondicionEspCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.documento.BitemporalDocAnexoCot;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.documento.DocAnexoCot;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.documento.DocAnexoCotContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.Inciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.texto.BitemporalTexAdicionalCot;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.texto.TexAdicionalCot;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.texto.TexAdicionalCotContinuity;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.afirme.bitemporal.annotations.BitemporalCascadeEnd;
import com.anasoft.os.daofusion.bitemporal.BitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.ContinuityCollectionWrapper;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;
import com.anasoft.os.daofusion.bitemporal.WrappedBitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.WrappedValueAccessor;

@Entity
@Table(name="MCOTIZACIONC",schema="MIDAS")
public class CotizacionContinuity implements Serializable,
		EntidadContinuity<Cotizacion, BitemporalCotizacion>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String BUSINESS_KEY_NAME = "numero";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQMCOTIZACIONCONTID")
	@SequenceGenerator(name = "SEQMCOTIZACIONCONTID", sequenceName = "MIDAS.SEQMCOTIZACIONCONTID", allocationSize = 1)
	private Long id;
	
	@Column(name="NUMERO")
	private Integer numero;
	
	@OneToMany(mappedBy="continuity",cascade = CascadeType.ALL, orphanRemoval=true)
	private Collection<BitemporalCotizacion> cotizaciones = new LinkedList<BitemporalCotizacion>();
	
	@BitemporalCascadeEnd
	@OneToMany(mappedBy="cotizacionContinuity", cascade=CascadeType.ALL, orphanRemoval=true)
	private Collection<IncisoContinuity> incisoContinuities = new LinkedList<IncisoContinuity>();
	
	@BitemporalCascadeEnd
	@OneToMany(mappedBy="cotizacionContinuity", cascade=CascadeType.ALL, orphanRemoval=true)
	private Collection<ComisionContinuity> comisionContinuities = new LinkedList<ComisionContinuity>();
	
	@BitemporalCascadeEnd
	@OneToMany(mappedBy="cotizacionContinuity", cascade=CascadeType.ALL, orphanRemoval=true)
	private Collection<TexAdicionalCotContinuity> texAdicionalCotContinuities = new LinkedList<TexAdicionalCotContinuity>();
	
	@BitemporalCascadeEnd
	@OneToMany(mappedBy="cotizacionContinuity", cascade=CascadeType.ALL, orphanRemoval=true)
	private Collection<DocAnexoCotContinuity> docAnexoContinuities = new LinkedList<DocAnexoCotContinuity>();
	
	@BitemporalCascadeEnd
	@OneToMany(mappedBy="cotizacionContinuity", cascade=CascadeType.ALL, orphanRemoval=true)
	private Collection<CondicionEspCotContinuity> condicionEspecialCotContinuities = new LinkedList<CondicionEspCotContinuity>();
		
	@SuppressWarnings("serial")
	public WrappedBitemporalProperty<Cotizacion, BitemporalCotizacion,CotizacionContinuity> getCotizaciones() {
		return new WrappedBitemporalProperty<Cotizacion, BitemporalCotizacion, CotizacionContinuity>(
				cotizaciones,
				new WrappedValueAccessor<Cotizacion, BitemporalCotizacion, CotizacionContinuity>() {

					public BitemporalCotizacion wrapValue(Cotizacion value,
							IntervalWrapper validityInterval, boolean twoPhaseMode) {
						return new BitemporalCotizacion(value, validityInterval,CotizacionContinuity.this,null,twoPhaseMode);
					}

				});
	}

	@Override
	@Transient
	public BitemporalProperty<Cotizacion, BitemporalCotizacion> getBitemporalProperty() {
		return getCotizaciones();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
	
	public Integer getNumero() {
		return numero;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	
    public ContinuityCollectionWrapper<Inciso,BitemporalInciso, IncisoContinuity> getIncisoContinuities() {
		return new ContinuityCollectionWrapper<Inciso,BitemporalInciso, IncisoContinuity>(incisoContinuities);
	}
    
    
    public ContinuityCollectionWrapper<Comision,BitemporalComision, ComisionContinuity> getComisionContinuities() {
		return new ContinuityCollectionWrapper<Comision,BitemporalComision, ComisionContinuity>(comisionContinuities);
	}
    
 
    public ContinuityCollectionWrapper<DocAnexoCot,BitemporalDocAnexoCot, DocAnexoCotContinuity> getDocAnexoContinuities() {
		return new ContinuityCollectionWrapper<DocAnexoCot,BitemporalDocAnexoCot, DocAnexoCotContinuity>(docAnexoContinuities);
	}
  

    public ContinuityCollectionWrapper<TexAdicionalCot,BitemporalTexAdicionalCot, TexAdicionalCotContinuity> getTexAdicionalCotContinuities() {
		return new ContinuityCollectionWrapper<TexAdicionalCot,BitemporalTexAdicionalCot, TexAdicionalCotContinuity>(texAdicionalCotContinuities);
	}
    
    public ContinuityCollectionWrapper<CondicionEspCotizacion,BitemporalCondicionEspCot, CondicionEspCotContinuity> getCondicionEspecialCotContinuities() {
		return new ContinuityCollectionWrapper<CondicionEspCotizacion,BitemporalCondicionEspCot, CondicionEspCotContinuity>(condicionEspecialCotContinuities);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CotizacionContinuity other = (CotizacionContinuity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Integer getBusinessKey() {
		return this.numero;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CotizacionContinuity getParentContinuity() {
		return this;
	}

	
	@SuppressWarnings("rawtypes")
	public void setParentContinuity(EntidadContinuity parentContinuity) {
		
	};
	
	@Override
	public String getValue() {
		return null;
	}
	
	
	@Transient
	public String getNumeroCotizacion(){
		DecimalFormat f = new DecimalFormat("00000000");
		String prefix = "COT-";
		return prefix + f.format(this.numero);
	}	

	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("id => " + this.id + ",");
		sb.append("numero => " + this.numero);
		sb.append("]");
		
		return sb.toString();
	}
	
	@Override
	public Class<BitemporalCotizacion> getBitemporalClass() {
		return BitemporalCotizacion.class;
	}
}
