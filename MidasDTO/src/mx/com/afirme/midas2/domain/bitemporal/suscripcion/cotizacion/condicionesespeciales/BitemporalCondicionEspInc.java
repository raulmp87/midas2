package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.anasoft.os.daofusion.bitemporal.Bitemporal;
import com.anasoft.os.daofusion.bitemporal.BitemporalWrapper;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;

@Entity
@Table(name="MCONDICIONESPECIALINCISOB",schema="MIDAS")
@Access(AccessType.FIELD)
public class BitemporalCondicionEspInc extends BitemporalWrapper<CondicionEspInciso, CondicionEspIncContinuity> implements EntidadBitemporal<CondicionEspInciso, BitemporalCondicionEspInc>{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 4573485982975604740L;
	@Embedded
	private CondicionEspInciso value;

	@ManyToOne
	@JoinColumn(name = "MCONDESPINCISOC_ID", referencedColumnName = "id")
	private CondicionEspIncContinuity continuity;

	public BitemporalCondicionEspInc() {
		if (continuity == null) {
			continuity = new CondicionEspIncContinuity();
		}
		if (value == null) {
			value = new CondicionEspInciso();
		}
	}
	
	
	public BitemporalCondicionEspInc(CondicionEspInciso value, IntervalWrapper validityInterval, CondicionEspIncContinuity continuity, String valueId, boolean twoPhaseMode) {
		super(value, validityInterval,continuity,valueId, twoPhaseMode);
	}

	public BitemporalCondicionEspInc(CondicionEspInciso value, CondicionEspIncContinuity continuity){
		this.value = value;
		this.continuity = continuity;
	}
	
	

	@Override
	public Bitemporal copyWith(IntervalWrapper validityInterval,boolean twoPhaseMode) {
		return new BitemporalCondicionEspInc(value, validityInterval,getContinuity(), getValueId(), twoPhaseMode);
	}

	@Override
	protected void setValue(CondicionEspInciso value) {
		this.value =  value;
		
	}

	@Override
	public CondicionEspInciso getValue() {
		return value;
	}

	@Override
	public CondicionEspIncContinuity getContinuity() {
		return continuity;
	}

	@Override
	protected void setContinuity(CondicionEspIncContinuity continuity) {
		this.continuity = continuity;
	}


	@Override
	public CondicionEspInciso getEmbedded() {
		return this.value;
	}


	@Override
	public void setEmbedded(CondicionEspInciso value) {
		this.value = value;		
	}


	@SuppressWarnings("unchecked")
	@Override
	public CondicionEspIncContinuity getEntidadContinuity() {
		return this.getContinuity();
	}

	public void setEntidadContinuity(EntidadContinuity<CondicionEspInciso, BitemporalCondicionEspInc> entidadContinuity) {
		this.continuity = (CondicionEspIncContinuity) entidadContinuity;
		
	}

}
