package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.texto;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.anasoft.os.daofusion.bitemporal.Bitemporal;
import com.anasoft.os.daofusion.bitemporal.BitemporalWrapper;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;

@Entity
@Table(name="MTEXADICIONALCOTB",schema="MIDAS")
@Access(AccessType.FIELD)
public class BitemporalTexAdicionalCot extends BitemporalWrapper<TexAdicionalCot, TexAdicionalCotContinuity> implements EntidadBitemporal<TexAdicionalCot, BitemporalTexAdicionalCot>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3835577679950318072L;
	@Embedded
	private TexAdicionalCot value;
	@ManyToOne
	@JoinColumn(name = "MTEXADICIONALCOTC_ID", referencedColumnName = "id")
	private TexAdicionalCotContinuity continuity;
	
	public BitemporalTexAdicionalCot(){
		if (continuity == null) {
			continuity = new TexAdicionalCotContinuity();
		}
		if (value == null) {
			value = new TexAdicionalCot();
		}
	}
	public BitemporalTexAdicionalCot(TexAdicionalCot value, IntervalWrapper validityInterval, TexAdicionalCotContinuity continuity, String valueId, boolean twoPhaseMode) {
		super(value, validityInterval,continuity,valueId, twoPhaseMode);
	}

	public BitemporalTexAdicionalCot(TexAdicionalCot value, TexAdicionalCotContinuity continuity){
		this.value = value;
		this.continuity = continuity;
	}
	@Override
	public Bitemporal copyWith(IntervalWrapper validityInterval, boolean twoPhaseMode) {
		return new BitemporalTexAdicionalCot(value, validityInterval,getContinuity(), getValueId(), twoPhaseMode);
	}

	@Override
	public TexAdicionalCot getValue() {
		return value;
	}

	@Override
	public void setValue(TexAdicionalCot value) {
		this.value = value;
	}
	@Override
	public TexAdicionalCotContinuity getContinuity() {
		return continuity;
	}
	@Override
	protected void setContinuity(TexAdicionalCotContinuity continuity) {
		this.continuity = continuity;
	}
	
	@Override
	public TexAdicionalCot  getEmbedded() {
		return this.value;
	}
	
	@Override
	public void setEmbedded(TexAdicionalCot value) {
		this.value = value;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public TexAdicionalCotContinuity getEntidadContinuity() {
		return getContinuity();
	}
	
	public void setEntidadContinuity(EntidadContinuity<TexAdicionalCot,BitemporalTexAdicionalCot> entidadContinuity){
		this.continuity = (TexAdicionalCotContinuity) entidadContinuity;
	}

}
