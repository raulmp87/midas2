package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.aumento;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.anasoft.os.daofusion.bitemporal.Bitemporal;
import com.anasoft.os.daofusion.bitemporal.BitemporalWrapper;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;

@Entity
@Table(name="MCOBERTURAAUMENTOB",schema="MIDAS")
@Access(AccessType.FIELD)
public class BitemporalCoberturaAumento extends
		BitemporalWrapper<CoberturaAumento, CoberturaAumentoContinuity> implements EntidadBitemporal<CoberturaAumento,BitemporalCoberturaAumento> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4902787961966106585L;
	@Embedded
	private CoberturaAumento value;
	@ManyToOne
	@JoinColumn(name="MCOBERTURAAUMENTOC_ID", referencedColumnName="id")
	private CoberturaAumentoContinuity continuity;

	
	public BitemporalCoberturaAumento() {
		if(continuity == null) {
			continuity = new CoberturaAumentoContinuity();
		}
		if(value == null) {
			value = new CoberturaAumento();
		}
	}

	public BitemporalCoberturaAumento(CoberturaAumento value,
			IntervalWrapper validityInterval, CoberturaAumentoContinuity continuity, String valueId, boolean twoPhaseMode) {
		super(value, validityInterval, continuity, valueId, twoPhaseMode);
	}

	@Override
	public Bitemporal copyWith(IntervalWrapper validityInterval, boolean twoPhaseMode) {
		return new BitemporalCoberturaAumento(value, validityInterval,getContinuity(), getValueId(), twoPhaseMode);
	}

	@Override
	public void setValue(CoberturaAumento value) {
		this.value = value;
	}

	@Override
	public CoberturaAumento getValue() {
		return value;
	}
	
	
	@Override
	public CoberturaAumento  getEmbedded() {
		return this.value;
	}
	
	@Override
	public void setEmbedded(CoberturaAumento value) {
		this.value = value;
	}
	
	@Override
	public CoberturaAumentoContinuity getContinuity() {
		return continuity;
	}

	@Override
	protected void setContinuity(CoberturaAumentoContinuity continuity) {
		this.continuity = continuity;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public CoberturaAumentoContinuity getEntidadContinuity() {
		return getContinuity();
	}
	
	public void setEntidadContinuity(EntidadContinuity<CoberturaAumento,BitemporalCoberturaAumento> entidadContinuity){
		this.continuity = (CoberturaAumentoContinuity) entidadContinuity;
	}


}
