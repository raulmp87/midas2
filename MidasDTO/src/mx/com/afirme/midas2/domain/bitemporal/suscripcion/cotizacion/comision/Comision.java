package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.comision;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class Comision  implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4097819578058650761L;
	
	@Column(name="VALORCOMISIONCOTIZACION")
	private BigDecimal valorComisionCotizacion;
	
	@Column(name="CLAVEAUTCOMISION")
	private Short claveAutComision;
	
	@Column(name="CODIGOUSUARIOAUTCOMISION")
	private String codigoUsuarioAutComision;
	
	@Column(name="CLAVETIPOPORCENTAJECOMISION")
	private Short claveTipoPorcentajeComision;
	
	@Column(name="PORCENTAJECOMISIONDEFAULT")
	private BigDecimal PorcentajeComisionDefault;
	
	@Column(name="PORCENTAJECOMISIONCOTIZACION")
	private BigDecimal PorcentajeComisionCotizacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHASOLAUTCOMISION")
	private Date fechaSolicitudAutComision;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAAUTCOMISION")
	private Date fechaAutComision;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAINICIOVIGENCIA")
	private Date fechaIncioVigencia;
	
	
	public BigDecimal getValorComisionCotizacion() {
		return valorComisionCotizacion;
	}
	public void setValorComisionCotizacion(BigDecimal valorComisionCotizacion) {
		this.valorComisionCotizacion = valorComisionCotizacion;
	}
	
	
	public Short getClaveAutComision() {
		return claveAutComision;
	}
	public void setClaveAutComision(Short claveAutComision) {
		this.claveAutComision = claveAutComision;
	}
	
	
	public String getCodigoUsuarioAutComision() {
		return codigoUsuarioAutComision;
	}
	public void setCodigoUsuarioAutComision(String codigoUsuarioAutComision) {
		this.codigoUsuarioAutComision = codigoUsuarioAutComision;
	}
	
	
	public Short getClaveTipoPorcentajeComision() {
		return claveTipoPorcentajeComision;
	}
	public void setClaveTipoPorcentajeComision(Short claveTipoPorcentajeComision) {
		this.claveTipoPorcentajeComision = claveTipoPorcentajeComision;
	}
	

	public BigDecimal getPorcentajeComisionDefault() {
		return PorcentajeComisionDefault;
	}
	public void setPorcentajeComisionDefault(BigDecimal porcentajeComisionDefault) {
		PorcentajeComisionDefault = porcentajeComisionDefault;
	}
	
	
	public BigDecimal getPorcentajeComisionCotizacion() {
		return PorcentajeComisionCotizacion;
	}
	public void setPorcentajeComisionCotizacion(
			BigDecimal porcentajeComisionCotizacion) {
		PorcentajeComisionCotizacion = porcentajeComisionCotizacion;
	}
	

	public Date getFechaSolicitudAutComision() {
		return fechaSolicitudAutComision;
	}
	public void setFechaSolicitudAutComision(Date fechaSolicitudAutComision) {
		this.fechaSolicitudAutComision = fechaSolicitudAutComision;
	}
	

	public Date getFechaAutComision() {
		return fechaAutComision;
	}
	public void setFechaAutComision(Date fechaAutComision) {
		this.fechaAutComision = fechaAutComision;
	}
	
	
	public Date getFechaIncioVigencia() {
		return fechaIncioVigencia;
	}
	public void setFechaIncioVigencia(Date fechaIncioVigencia) {
		this.fechaIncioVigencia = fechaIncioVigencia;
	}
	

}
