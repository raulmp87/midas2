package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.dato;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.SeccionIncisoContinuity;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.afirme.bitemporal.annotations.BitemporalNotNull;
import com.anasoft.os.daofusion.bitemporal.BitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;
import com.anasoft.os.daofusion.bitemporal.WrappedBitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.WrappedValueAccessor;

@Entity
@Table(name="MDATOSECCIONC",schema="MIDAS")
public class DatoSeccionContinuity implements Serializable,
		EntidadContinuity<DatoSeccion, BitemporalDatoSeccion> {

	public DatoSeccionContinuity() {
		if(seccionIncisoContinuity == null) {
			seccionIncisoContinuity = new SeccionIncisoContinuity();
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -2702240529457718297L;
	
	public static final String PARENT_KEY_NAME = "seccionIncisoContinuity.id";
	

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQMDATOSECCIONCONTID")
	@SequenceGenerator(name = "SEQMDATOSECCIONCONTID", sequenceName = "MIDAS.SEQMDATOSECCIONCONTID", allocationSize = 1)
	@Column(name = "id")
	private Long id;
	
	@Column(name="COBERTURA_ID")
	private Long coberturaId;
	
	@Column(name="RAMO_ID")
	private Long ramoId;
		
	@Column(name="SUBRAMO_ID")
	private Long subRamoId;
		
	@Column(name="CLAVEDETALLE")
	private Long claveDetalle;
	
	@Column(name="DATO_ID")
	private Long datoId;
	
	@OneToMany(mappedBy="continuity",cascade = CascadeType.ALL, orphanRemoval=true)
	private Collection<BitemporalDatoSeccion> datoSecciones = new LinkedList<BitemporalDatoSeccion>();

	@BitemporalNotNull
	@ManyToOne
	@JoinColumn(name="MSECCIONINCISOC_ID", referencedColumnName="id", nullable=false)
	private SeccionIncisoContinuity seccionIncisoContinuity = new SeccionIncisoContinuity();


	@Override
	@Transient
	public BitemporalProperty<DatoSeccion, BitemporalDatoSeccion> getBitemporalProperty() {
		return getDatoSecciones();
	}
	
	@SuppressWarnings("serial")
	public WrappedBitemporalProperty<DatoSeccion, BitemporalDatoSeccion, DatoSeccionContinuity> getDatoSecciones() {
		return new WrappedBitemporalProperty<DatoSeccion, BitemporalDatoSeccion, DatoSeccionContinuity>(
				datoSecciones,
				new WrappedValueAccessor<DatoSeccion, BitemporalDatoSeccion, DatoSeccionContinuity>() {

					public BitemporalDatoSeccion wrapValue(DatoSeccion value,
							IntervalWrapper validityInterval, boolean twoPhaseMode) {
						return new BitemporalDatoSeccion(value, validityInterval,DatoSeccionContinuity.this,null,twoPhaseMode);
					}

				});
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	

	public Long getCoberturaId() {
		return coberturaId;
	}

	public void setCoberturaId(Long coberturaId) {
		this.coberturaId = coberturaId;
	}

	public Long getRamoId() {
		return ramoId;
	}

	public void setRamoId(Long ramoId) {
		this.ramoId = ramoId;
	}

	public Long getSubRamoId() {
		return subRamoId;
	}

	public void setSubRamoId(Long subRamoId) {
		this.subRamoId = subRamoId;
	}

	public Long getClaveDetalle() {
		return claveDetalle;
	}

	public void setClaveDetalle(Long claveDetalle) {
		this.claveDetalle = claveDetalle;
	}

	public Long getDatoId() {
		return datoId;
	}

	public void setDatoId(Long datoId) {
		this.datoId = datoId;
	}

	public SeccionIncisoContinuity getSeccionIncisoContinuity() {
		return seccionIncisoContinuity;
	}

	public void setSeccionIncisoContinuity(
			SeccionIncisoContinuity seccionIncisoContinuity) {
		this.seccionIncisoContinuity = seccionIncisoContinuity;
	}	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((claveDetalle == null) ? 0 : claveDetalle.hashCode());
		result = prime * result
				+ ((coberturaId == null) ? 0 : coberturaId.hashCode());
		result = prime * result + ((datoId == null) ? 0 : datoId.hashCode());
		result = prime * result + ((ramoId == null) ? 0 : ramoId.hashCode());
		result = prime * result
				+ ((subRamoId == null) ? 0 : subRamoId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof DatoSeccionContinuity))
			return false;
		DatoSeccionContinuity other = (DatoSeccionContinuity) obj;
		if (claveDetalle == null) {
			if (other.claveDetalle != null)
				return false;
		} else if (!claveDetalle.equals(other.claveDetalle))
			return false;
		if (coberturaId == null) {
			if (other.coberturaId != null)
				return false;
		} else if (!coberturaId.equals(other.coberturaId))
			return false;
		if (datoId == null) {
			if (other.datoId != null)
				return false;
		} else if (!datoId.equals(other.datoId))
			return false;
		if (ramoId == null) {
			if (other.ramoId != null)
				return false;
		} else if (!ramoId.equals(other.ramoId))
			return false;
		if (subRamoId == null) {
			if (other.subRamoId != null)
				return false;
		} else if (!subRamoId.equals(other.subRamoId))
			return false;
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return getId();
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public SeccionIncisoContinuity getParentContinuity() {
		return getSeccionIncisoContinuity();
	}
	
	@SuppressWarnings("rawtypes")
	public void setParentContinuity(EntidadContinuity parentContinuity) {
		setSeccionIncisoContinuity((SeccionIncisoContinuity)parentContinuity);
	};

	@Override
	public Class<BitemporalDatoSeccion> getBitemporalClass() {
		return BitemporalDatoSeccion.class;
	}
}
