package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.subinciso;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.SeccionIncisoContinuity;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.afirme.bitemporal.annotations.BitemporalNotNull;
import com.anasoft.os.daofusion.bitemporal.BitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;
import com.anasoft.os.daofusion.bitemporal.WrappedBitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.WrappedValueAccessor;

@Entity
@Table(name="MSUBINCISOSECCIONC",schema="MIDAS")
public class SubIncisoSeccionContinuity implements
		EntidadContinuity<SubIncisoSeccion, BitemporalSubIncisoSeccion>, Serializable {

	public SubIncisoSeccionContinuity() {
		if(seccionIncisoContinuity == null) {
			seccionIncisoContinuity = new SeccionIncisoContinuity();
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -7977479134902345754L;
	
	public static final String PARENT_KEY_NAME = "seccionIncisoContinuity.id";
	

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQMSUBINCISOSECCIONCONTID")
	@SequenceGenerator(name = "SEQMSUBINCISOSECCIONCONTID", sequenceName = "MIDAS.SEQMSUBINCISOSECCIONCONTID", allocationSize = 1)
	@Column(name = "id")
	private Long id;
	
	@Column(name="NUMEROSUBINCISO")
	private Long numeroSubInciso;
	
	@OneToMany(mappedBy="continuity",cascade = CascadeType.ALL, orphanRemoval=true)
	private Collection<BitemporalSubIncisoSeccion> subIncisoSecciones = new LinkedList<BitemporalSubIncisoSeccion>();
	
	@BitemporalNotNull
	@ManyToOne
	@JoinColumn(name="MSECCIONINCISOC_ID", referencedColumnName="id", nullable=false)
	private SeccionIncisoContinuity seccionIncisoContinuity = new SeccionIncisoContinuity();

	@Override
	@Transient
	public BitemporalProperty<SubIncisoSeccion, BitemporalSubIncisoSeccion> getBitemporalProperty() {
		return getSubIncisoSecciones();
	}
	
	@SuppressWarnings("serial")
	public WrappedBitemporalProperty<SubIncisoSeccion, BitemporalSubIncisoSeccion, SubIncisoSeccionContinuity> getSubIncisoSecciones() {
		return new WrappedBitemporalProperty<SubIncisoSeccion, BitemporalSubIncisoSeccion, SubIncisoSeccionContinuity>(
				subIncisoSecciones,
				new WrappedValueAccessor<SubIncisoSeccion, BitemporalSubIncisoSeccion, SubIncisoSeccionContinuity>() {

					public BitemporalSubIncisoSeccion wrapValue(SubIncisoSeccion value,
							IntervalWrapper validityInterval, boolean twoPhaseMode) {
						return new BitemporalSubIncisoSeccion(value, validityInterval,SubIncisoSeccionContinuity.this,null, twoPhaseMode);
					}

				});
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	 
	
	public Long getNumeroSubInciso() {
		return numeroSubInciso;
	}
	public void setNumeroSubInciso(Long numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}

	public SeccionIncisoContinuity getSeccionIncisoContinuity() {
		return seccionIncisoContinuity;
	}

	public void setSeccionIncisoContinuity(
			SeccionIncisoContinuity seccionIncisoContinuity) {
		this.seccionIncisoContinuity = seccionIncisoContinuity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubIncisoSeccionContinuity other = (SubIncisoSeccionContinuity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return getId();
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return getNumeroSubInciso();
	}

	@SuppressWarnings("unchecked")
	@Override
	public SeccionIncisoContinuity getParentContinuity() {
		return getSeccionIncisoContinuity();
	}
	
	@SuppressWarnings("rawtypes")
	public void setParentContinuity(EntidadContinuity parentContinuity) {
		setSeccionIncisoContinuity((SeccionIncisoContinuity)parentContinuity);
	};
	
	@Override
	public Class<BitemporalSubIncisoSeccion> getBitemporalClass() {
		return BitemporalSubIncisoSeccion.class;
	}

}
