package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.texto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Embeddable
public class TexAdicionalCot implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final short CLAVEAUTORIZACION_EN_PROCESO = 0;
	public static final short CLAVEAUTORIZACION_AUTORIZADA = 1;
	public static final short CLAVEAUTORIZACION_RECHAZADA = 2;
	public static final short CLAVEAUTORIZACION_CANCELADA = 3;
	
	@Column(name="DESCRIPCIONTEXTO")
	private String descripcionTexto;
	
	@Column(name="CLAVEAUTORIZACION")
	private Short claveAutorizacion;
	
	@Column(name="CODIGOUSUARIOAUTORIZACION")
	private String codigoUsuarioAutorizacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHACREACION")
	private Date fechaCreacion;
	
	@Column(name="CODIGOUSUARIOCREACION")
	private String codigoUsuarioCreacion;
	
	@Column(name="NOMBREUSUARIOCREACION")
	private String nombreUsuarioCreacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAMODIFICACION")
	private Date fechaModificacion;
	
	@Column(name="CODIGOUSUARIOMODIFICACION")
	private String codigoUsuarioModificacion;
	
	@Column(name="NOMBREUSUARIOMODIFICACION")
	private String nombreUsuarioModificacion;
	
	@Column(name="NUMEROSECUENCIA")
	private Integer numeroSecuencia;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHASOLICITUDAUTORIZACION")
	private Date fechaSolicitudAutorizacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAAUTORIZACION")
	private Date fechaAutorizacion;
	
	@Transient
	private String descripcionEstatusAut;


	public String getDescripcionTexto() {
		return descripcionTexto;
	}

	public void setDescripcionTexto(String descripcionTexto) {
		this.descripcionTexto = descripcionTexto;
	}

	
	public Short getClaveAutorizacion() {
		return claveAutorizacion;
	}

	public void setClaveAutorizacion(Short claveAutorizacion) {
		this.claveAutorizacion = claveAutorizacion;
	}

	
	public String getCodigoUsuarioAutorizacion() {
		return codigoUsuarioAutorizacion;
	}

	public void setCodigoUsuarioAutorizacion(String codigoUsuarioAutorizacion) {
		this.codigoUsuarioAutorizacion = codigoUsuarioAutorizacion;
	}

	
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	
	public String getNombreUsuarioCreacion() {
		return nombreUsuarioCreacion;
	}

	public void setNombreUsuarioCreacion(String nombreUsuarioCreacion) {
		this.nombreUsuarioCreacion = nombreUsuarioCreacion;
	}


	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	
	public String getCodigoUsuarioModificacion() {
		return codigoUsuarioModificacion;
	}

	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}

	
	public String getNombreUsuarioModificacion() {
		return nombreUsuarioModificacion;
	}

	public void setNombreUsuarioModificacion(String nombreUsuarioModificacion) {
		this.nombreUsuarioModificacion = nombreUsuarioModificacion;
	}
	

	public Integer getNumeroSecuencia() {
		return numeroSecuencia;
	}

	public void setNumeroSecuencia(Integer numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}


	public Date getFechaSolicitudAutorizacion() {
		return fechaSolicitudAutorizacion;
	}

	public void setFechaSolicitudAutorizacion(Date fechaSolicitudAutorizacion) {
		this.fechaSolicitudAutorizacion = fechaSolicitudAutorizacion;
	}

	
	public Date getFechaAutorizacion() {
		return fechaAutorizacion;
	}

	public void setFechaAutorizacion(Date fechaAutorizacion) {
		this.fechaAutorizacion = fechaAutorizacion;
	}
	
	
	public String getDescripcionEstatusAut(){		
		descripcionEstatusAut = null;
		if (this.claveAutorizacion != null) {
			switch(this.claveAutorizacion){				
				case CLAVEAUTORIZACION_EN_PROCESO:
					descripcionEstatusAut = "EN PROCESO";
					break;
				case CLAVEAUTORIZACION_AUTORIZADA:
					descripcionEstatusAut = "AUTORIZADA";
					break;					
				case CLAVEAUTORIZACION_RECHAZADA:
					descripcionEstatusAut = "RECHAZADA";
					break;
				case CLAVEAUTORIZACION_CANCELADA:
					descripcionEstatusAut = "CANCELADA";
					break;									
				default:
					break;							
			}
		}
		return descripcionEstatusAut;
		
	}
	
	public void setDescripcionEstatusAut(String descripcionEstatusAut){
		this.descripcionEstatusAut = descripcionEstatusAut;
	}

}
