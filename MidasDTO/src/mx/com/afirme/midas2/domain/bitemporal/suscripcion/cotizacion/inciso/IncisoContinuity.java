package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.BitemporalCondicionEspInc;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.CondicionEspIncContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.CondicionEspInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.BitemporalSeccionInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.SeccionInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.SeccionIncisoContinuity;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.afirme.bitemporal.annotations.BitemporalCascadeEnd;
import com.afirme.bitemporal.annotations.BitemporalNotNull;
import com.anasoft.os.daofusion.bitemporal.BitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.ContinuityCollectionWrapper;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;
import com.anasoft.os.daofusion.bitemporal.WrappedBitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.WrappedValueAccessor;

@Entity
@Table(name = "MINCISOC", schema = "MIDAS")
public class IncisoContinuity implements Serializable,
		EntidadContinuity<Inciso, BitemporalInciso> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6714934836248660667L;

	public static final String PARENT_KEY_NAME = "cotizacionContinuity.id";
	public static final String PARENT_BUSINESS_KEY_NAME = "cotizacionContinuity.numero";
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQMINCISOCONTID")
	@SequenceGenerator(name = "SEQMINCISOCONTID", sequenceName = "MIDAS.SEQMINCISOCONTID", allocationSize = 1)
	@Column(name = "id")
	private Long id;

	@Column(name = "NUMERO")
	private Integer numero;

	@OneToMany(mappedBy = "continuity", cascade = CascadeType.ALL, orphanRemoval=true)
	private Collection<BitemporalInciso> incisos = new LinkedList<BitemporalInciso>();

	@BitemporalNotNull
	@ManyToOne
	@JoinColumn(name = "MCOTIZACIONC_ID", referencedColumnName = "id", nullable = false)
	private CotizacionContinuity cotizacionContinuity;

	@BitemporalCascadeEnd
	@OneToOne(mappedBy = "incisoContinuity", cascade = CascadeType.ALL, orphanRemoval = true)
	private AutoIncisoContinuity autoIncisoContinuity;

	@BitemporalCascadeEnd
	@OneToMany(mappedBy = "incisoContinuity", cascade = CascadeType.ALL, orphanRemoval = true)
	private Collection<SeccionIncisoContinuity> seccionIncisoContinuities = new LinkedList<SeccionIncisoContinuity>();
	
	@BitemporalCascadeEnd
	@OneToMany(mappedBy = "incisoContinuity", cascade = CascadeType.ALL, orphanRemoval = true)
	private Collection<CondicionEspIncContinuity> condicionEspIncContinuities = new LinkedList<CondicionEspIncContinuity>();
	

	public IncisoContinuity() {
		if(cotizacionContinuity == null) {
			cotizacionContinuity = new CotizacionContinuity();
		}
	}

	public IncisoContinuity(CotizacionContinuity cotizacionContinuity) {
		this.cotizacionContinuity = cotizacionContinuity;
	}

	@Override
	@Transient
	public BitemporalProperty<Inciso, BitemporalInciso> getBitemporalProperty() {
		return getIncisos();
	}

	@SuppressWarnings("serial")
	public WrappedBitemporalProperty<Inciso, BitemporalInciso, IncisoContinuity> getIncisos() {
		return new WrappedBitemporalProperty<Inciso, BitemporalInciso, IncisoContinuity>(
				incisos,
				new WrappedValueAccessor<Inciso, BitemporalInciso, IncisoContinuity>() {

					public BitemporalInciso wrapValue(Inciso value,
							IntervalWrapper validityInterval,
							boolean twoPhaseMode) {
						return new BitemporalInciso(value, validityInterval,
								IncisoContinuity.this, null, twoPhaseMode);
					}

				});
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public CotizacionContinuity getCotizacionContinuity() {
		return cotizacionContinuity;
	}

	public void setCotizacionContinuity(
			CotizacionContinuity cotizacionContinuity) {
		this.cotizacionContinuity = cotizacionContinuity;
	}
	
	public AutoIncisoContinuity getAutoIncisoContinuity() {
		return autoIncisoContinuity;
	}
	
	public void setAutoIncisoContinuity(
			AutoIncisoContinuity autoIncisoContinuity) {
		this.autoIncisoContinuity = autoIncisoContinuity;
	}

    public ContinuityCollectionWrapper<SeccionInciso,BitemporalSeccionInciso, SeccionIncisoContinuity> getIncisoSeccionContinuities() {
		return new ContinuityCollectionWrapper<SeccionInciso,BitemporalSeccionInciso, SeccionIncisoContinuity>(seccionIncisoContinuities);
	}
    

    public ContinuityCollectionWrapper<CondicionEspInciso,BitemporalCondicionEspInc, CondicionEspIncContinuity> getCondicionEspIncContinuities() {
		return new ContinuityCollectionWrapper<CondicionEspInciso,BitemporalCondicionEspInc, CondicionEspIncContinuity>(condicionEspIncContinuities);
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IncisoContinuity other = (IncisoContinuity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.id;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CotizacionContinuity getParentContinuity() {
		return getCotizacionContinuity();
	}
	
	@SuppressWarnings("rawtypes")
	public void setParentContinuity(EntidadContinuity parentContinuity) {
		setCotizacionContinuity((CotizacionContinuity)parentContinuity);
	};

	public String toString(){
	  StringBuilder sb = new StringBuilder();
	  sb.append("[");
	  sb.append("numero: " + this.numero);
	  //sb.append("incisos: " + this.incisos + ", ");
	  sb.append("]");
	  
	  return sb.toString();
	}
	
	@Override
	public Class<BitemporalInciso> getBitemporalClass() {
		return BitemporalInciso.class;
	}
}
