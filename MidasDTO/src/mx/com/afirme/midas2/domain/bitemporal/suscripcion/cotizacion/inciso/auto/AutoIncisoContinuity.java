package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto;


import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.afirme.bitemporal.annotations.BitemporalNotNull;
import com.anasoft.os.daofusion.bitemporal.BitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;
import com.anasoft.os.daofusion.bitemporal.WrappedBitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.WrappedValueAccessor;

@Entity
@Table(name="MAUTOINCISOC",schema="MIDAS")
public class AutoIncisoContinuity implements Serializable,
	EntidadContinuity<AutoInciso, BitemporalAutoInciso> {

	public AutoIncisoContinuity() {
		if(incisoContinuity == null) {
			incisoContinuity = new IncisoContinuity();
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -2262840654470240137L;

	public static final String PARENT_KEY_NAME = "incisoContinuity.id";
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQMAUTOINCISOCONTID")
	@SequenceGenerator(name = "SEQMAUTOINCISOCONTID", sequenceName = "MIDAS.SEQMAUTOINCISOCONTID", allocationSize = 1)
	@Column(name = "id")
	private Long id;
	
	@OneToMany(mappedBy="continuity",cascade = CascadeType.ALL, orphanRemoval=true)
	private Collection<BitemporalAutoInciso> autoIncisos = new LinkedList<BitemporalAutoInciso>();
	
	@BitemporalNotNull
	@ManyToOne
	@JoinColumn(name="MINCISOC_ID", referencedColumnName="id", nullable=false)
	private IncisoContinuity incisoContinuity = new IncisoContinuity();

	@Override
	@Transient
	public BitemporalProperty<AutoInciso, BitemporalAutoInciso> getBitemporalProperty() {
		return getIncisoAutos();
	}

	@SuppressWarnings("serial")
	public WrappedBitemporalProperty<AutoInciso, BitemporalAutoInciso, AutoIncisoContinuity> getIncisoAutos() {
		return new WrappedBitemporalProperty<AutoInciso, BitemporalAutoInciso, AutoIncisoContinuity>(
				autoIncisos,
				new WrappedValueAccessor<AutoInciso, BitemporalAutoInciso, AutoIncisoContinuity>() {

					public BitemporalAutoInciso wrapValue(AutoInciso value,
							IntervalWrapper validityInterval, boolean twoPhaseMode) {
						return new BitemporalAutoInciso(value, validityInterval,AutoIncisoContinuity.this ,null, twoPhaseMode);
					}

				});
	}


	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}


	public IncisoContinuity getIncisoContinuity() {
		return incisoContinuity;
	}

	public void setIncisoContinuity(IncisoContinuity incisoContinuity) {
		this.incisoContinuity = incisoContinuity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AutoIncisoContinuity other = (AutoIncisoContinuity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return getId();
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Integer getBusinessKey() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public IncisoContinuity getParentContinuity() {
		return getIncisoContinuity();
	}
	
	@SuppressWarnings("rawtypes")
	public void setParentContinuity(EntidadContinuity parentContinuity) {
		setIncisoContinuity((IncisoContinuity)parentContinuity);
	};
	
	@Override
	public Class<BitemporalAutoInciso> getBitemporalClass() {
		return BitemporalAutoInciso.class;
	}	

}
