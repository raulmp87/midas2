package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.detalle;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.CoberturaSeccionContinuity;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.afirme.bitemporal.annotations.BitemporalNotNull;
import com.anasoft.os.daofusion.bitemporal.BitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;
import com.anasoft.os.daofusion.bitemporal.WrappedBitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.WrappedValueAccessor;

@Entity
@Table(name="MCOBERTURADETALLEPRIMAC",schema="MIDAS")
public class CoberturaDetallePrimaContinuity implements Serializable,
		EntidadContinuity<CoberturaDetallePrima, BitemporalCoberturaDetallePrima> {

	public CoberturaDetallePrimaContinuity() {
		if(coberturaSeccionContinuity == null) {
			coberturaSeccionContinuity = new CoberturaSeccionContinuity();
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 4789921055559029940L;

	public static final String PARENT_KEY_NAME = "coberturaSeccionContinuity.id";
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQMCOBERTURADETALLECONTID")
	@SequenceGenerator(name = "SEQMCOBERTURADETALLECONTID", sequenceName = "MIDAS.SEQMCOBERTURADETALLECONTID", allocationSize = 1)
	@Column(name = "id")
	private Long id;
	
	@OneToMany(mappedBy="continuity",cascade = CascadeType.ALL, orphanRemoval=true)
	private Collection<BitemporalCoberturaDetallePrima> coberturaDetalles = new LinkedList<BitemporalCoberturaDetallePrima>();
	
	@BitemporalNotNull
	@ManyToOne
	@JoinColumn(name="MCOBERTURASECCIONC_ID", referencedColumnName="id", nullable=false)
	private CoberturaSeccionContinuity coberturaSeccionContinuity = new CoberturaSeccionContinuity();

	@Override
	@Transient
	public BitemporalProperty<CoberturaDetallePrima, BitemporalCoberturaDetallePrima> getBitemporalProperty() {
		return getCoberturaDetalles();
	}
	
	
	@SuppressWarnings("serial")
	public WrappedBitemporalProperty<CoberturaDetallePrima, BitemporalCoberturaDetallePrima, CoberturaDetallePrimaContinuity> getCoberturaDetalles() {
		return new WrappedBitemporalProperty<CoberturaDetallePrima, BitemporalCoberturaDetallePrima, CoberturaDetallePrimaContinuity>(
				coberturaDetalles,
				new WrappedValueAccessor<CoberturaDetallePrima, BitemporalCoberturaDetallePrima, CoberturaDetallePrimaContinuity>() {

					public BitemporalCoberturaDetallePrima wrapValue(CoberturaDetallePrima value,
							IntervalWrapper validityInterval, boolean twoPhaseMode) {
						return new BitemporalCoberturaDetallePrima(value, validityInterval,CoberturaDetallePrimaContinuity.this,null, twoPhaseMode);
					}

				});
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}



	public CoberturaSeccionContinuity getCoberturaSeccionContinuity() {
		return coberturaSeccionContinuity;
	}


	public void setCoberturaSeccionContinuity(
			CoberturaSeccionContinuity coberturaSeccionContinuity) {
		this.coberturaSeccionContinuity = coberturaSeccionContinuity;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CoberturaDetallePrimaContinuity other = (CoberturaDetallePrimaContinuity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return getId();
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getBusinessKey() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CoberturaSeccionContinuity getParentContinuity() {
		return getCoberturaSeccionContinuity();
	}
	
	@SuppressWarnings("rawtypes")
	public void setParentContinuity(EntidadContinuity parentContinuity) {
		setCoberturaSeccionContinuity((CoberturaSeccionContinuity)parentContinuity);
	};

	@Override
	public Class<BitemporalCoberturaDetallePrima> getBitemporalClass() {
		return BitemporalCoberturaDetallePrima.class;
	}
}
