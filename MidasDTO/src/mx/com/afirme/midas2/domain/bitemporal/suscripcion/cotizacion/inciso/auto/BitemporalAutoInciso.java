package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.anasoft.os.daofusion.bitemporal.Bitemporal;
import com.anasoft.os.daofusion.bitemporal.BitemporalWrapper;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;

@Entity
@Table(name="MAUTOINCISOB",schema="MIDAS")
@Access(AccessType.FIELD)
public class BitemporalAutoInciso extends BitemporalWrapper<AutoInciso, AutoIncisoContinuity> implements EntidadBitemporal<AutoInciso, BitemporalAutoInciso> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -342008970248994984L;
	@Embedded
	private AutoInciso value;
	@ManyToOne
	@JoinColumn(name = "MAUTOINCISOC_ID", referencedColumnName="ID")
	private AutoIncisoContinuity continuity;
	
	
	public BitemporalAutoInciso() {
		if(continuity == null) {
			continuity = new AutoIncisoContinuity();
		}
		if(value == null) {
			value = new AutoInciso();
		}
	}

	public BitemporalAutoInciso(AutoInciso value, IntervalWrapper validityInterval,AutoIncisoContinuity continuity,String valueId, boolean twoPhaseMode) {
		super(value, validityInterval, continuity, valueId,twoPhaseMode);
	}

	@Override
	public Bitemporal copyWith(IntervalWrapper validityInterval, boolean twoPhaseMode) {
		return new BitemporalAutoInciso(value,validityInterval,getContinuity(),getValueId(), twoPhaseMode);
	}

	@Override
	public AutoInciso getValue() {
		return value;
	}

	@Override
	public void setValue(AutoInciso value) {
		this.value = value;
	}
	
	@Override
	public AutoInciso  getEmbedded() {
		return this.value;
	}
	
	@Override
	public void setEmbedded(AutoInciso value) {
		this.value = value;
	}
	
	@Override
	public AutoIncisoContinuity getContinuity() {
		return continuity;
	}

	@Override
	protected void setContinuity(AutoIncisoContinuity continuity) {
		this.continuity = continuity;
		
	}


	@SuppressWarnings("unchecked")
	@Override
	public AutoIncisoContinuity getEntidadContinuity() {
		return getContinuity();
	}

	public void setEntidadContinuity(EntidadContinuity<AutoInciso,BitemporalAutoInciso> entidadContinuity){
		this.continuity = (AutoIncisoContinuity) entidadContinuity;
	}


}
