package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.anasoft.os.daofusion.bitemporal.Bitemporal;
import com.anasoft.os.daofusion.bitemporal.BitemporalWrapper;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;

@Entity
@Table(name="MINCISOB",schema="MIDAS")
@Access(AccessType.FIELD)
public class BitemporalInciso extends BitemporalWrapper<Inciso, IncisoContinuity> implements EntidadBitemporal<Inciso, BitemporalInciso>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3835577679950318072L;
	@Embedded
	private Inciso value;
	@ManyToOne
	@JoinColumn(name = "MINCISOC_ID", referencedColumnName="id")
	private IncisoContinuity continuity;
	
	
	public BitemporalInciso(){
		if(continuity == null) {
			continuity = new IncisoContinuity();
		}
		if(value == null) {
			value = new Inciso();
		}
	}
	public BitemporalInciso(Inciso value, IntervalWrapper validityInterval,IncisoContinuity continuity, String valueId, boolean twoPhaseMode) {
		super(value, validityInterval,continuity, valueId, twoPhaseMode);
	}

	public BitemporalInciso(Inciso value,IncisoContinuity continuity){
		this.value = value;
		this.continuity = continuity;
	}
	@Override
	public Bitemporal copyWith(IntervalWrapper validityInterval, boolean twoPhaseMode) {
		return new BitemporalInciso(value, validityInterval, getContinuity(), getValueId(), twoPhaseMode);
	}

	@Override
	public Inciso getValue() {
		return value;
	}

	@Override
	public void setValue(Inciso value) {
		this.value = value;
	}
	@Override
	public IncisoContinuity getContinuity() {
		return continuity;
	}
	@Override
	protected void setContinuity(IncisoContinuity continuity) {
		this.continuity = continuity;
	}
	
	@Override
	public Inciso  getEmbedded() {
		return this.value;
	}
	
	@Override
	public void setEmbedded(Inciso value) {
		this.value = value;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public IncisoContinuity getEntidadContinuity() {
		return getContinuity();
	}
	
	public void setEntidadContinuity(EntidadContinuity<Inciso,BitemporalInciso> entidadContinuity){
		this.continuity = (IncisoContinuity) entidadContinuity;
	}

	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("continuity: " + this.continuity + ", ");
		sb.append("value: " + this.value);
		sb.append("]");
		
		return sb.toString();
	}
	
}
