package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.documento;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Embeddable
public class DocAnexoCot implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name="FECHACREACION")
	@Temporal(TemporalType.DATE)
	private Date fechaCreacion;
	
	@Column(name="CODIGOUSUARIOCREACION")
	private String codigoUsuarioCreacion;
	
	@Column(name="NOMBREUSUARIOCREACION")	
	private String nombreUsuarioCreacion;
	
	
	@Column(name="FECHAMODIFICACION")
	@Temporal(TemporalType.DATE)
	private Date fechaModificacion;
	
	@Column(name="CODIGOUSUARIOMODIFICACION")
	private String codigoUsuarioModificacion;
	
	@Column(name="NOMBREUSUARIOMODIFICACION")
	private String nombreUsuarioModificacion;
	
	@Column(name="DESCRIPCIONDOCUMENTOANEXO")
	private String descripcionDocumentoAnexo;
	
	@Column(name="CLAVEOBLIGATORIEDAD")
	private Short claveObligatoriedad;
	
	@Column(name="CLAVESELECCION")
	private Short claveSeleccion;
	
	@Column(name="ORDEN")
	private Integer orden;
	
	@Column(name="CLAVETIPO")
	private Integer claveTipo;
	
	@Column(name="COBERTURA_ID")
	private Integer coberturaId;
	
	@Transient
	private String nivel;
	
	public static final String NIVEL_POLIZA = "Póliza";
	public static final String NIVEL_COTIZACION = "Cotización";
	public static final String NIVEL_COBERTURA = "Cobertura";
	
	public static final int CLAVETIPO_POLIZA = 0;
	public static final int CLAVETIPO_COTIZACION = 1;
	public static final int CLAVETIPO_COBERTURA = 2;
	
	
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	
	public String getNombreUsuarioCreacion() {
		return nombreUsuarioCreacion;
	}

	public void setNombreUsuarioCreacion(String nombreUsuarioCreacion) {
		this.nombreUsuarioCreacion = nombreUsuarioCreacion;
	}	

	
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}


	public String getCodigoUsuarioModificacion() {
		return codigoUsuarioModificacion;
	}

	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}


	public String getNombreUsuarioModificacion() {
		return nombreUsuarioModificacion;
	}

	public void setNombreUsuarioModificacion(String nombreUsuarioModificacion) {
		this.nombreUsuarioModificacion = nombreUsuarioModificacion;
	}
	
	
	public String getDescripcionDocumentoAnexo() {
		return descripcionDocumentoAnexo;
	}

	public void setDescripcionDocumentoAnexo(String descripcionDocumentoAnexo) {
		this.descripcionDocumentoAnexo = descripcionDocumentoAnexo;
	}

	
	public Short getClaveObligatoriedad() {
		return claveObligatoriedad;
	}

	public void setClaveObligatoriedad(Short claveObligatoriedad) {
		this.claveObligatoriedad = claveObligatoriedad;
	}


	public Short getClaveSeleccion() {
		return claveSeleccion;
	}

	public void setClaveSeleccion(Short claveSeleccion) {
		this.claveSeleccion = claveSeleccion;
	}

	
	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}


	public Integer getClaveTipo() {
		return claveTipo;
	}

	public void setClaveTipo(Integer claveTipo) {
		this.claveTipo = claveTipo;
	}


	public Integer getCoberturaId() {
		return coberturaId;
	}

	public void setCoberturaId(Integer coberturaId) {
		this.coberturaId = coberturaId;
	}	
	

	public String getNivel() {		
		if (CLAVETIPO_POLIZA == claveTipo) {
			nivel = NIVEL_POLIZA;
		} else if (CLAVETIPO_COTIZACION == claveTipo) {
			nivel = NIVEL_COTIZACION;
		} else if (CLAVETIPO_COBERTURA == claveTipo) {
			nivel = NIVEL_COBERTURA;
		}
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof DocAnexoCot))
			return false;
		
		DocAnexoCot other = (DocAnexoCot) obj;
		if (claveTipo == null) {
			if (other.claveTipo != null)
				return false;
		} else if (!claveTipo.equals(other.claveTipo))
			return false;
		if (descripcionDocumentoAnexo == null) {
			if (other.descripcionDocumentoAnexo != null)
				return false;
		} else if (!descripcionDocumentoAnexo.equals(other.descripcionDocumentoAnexo))
			return false;
		if (claveObligatoriedad == null) {
			if (other.claveObligatoriedad != null)
				return false;
		} else if (!claveObligatoriedad.equals(other.claveObligatoriedad))
			return false;
		if (orden == null) {
			if (other.orden != null)
				return false;
		} else if (!orden.equals(other.orden))
			return false;
				
		return true;
	}	
}
