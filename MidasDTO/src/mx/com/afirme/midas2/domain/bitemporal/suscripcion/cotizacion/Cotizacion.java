package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.tiponegocio.TipoNegocioDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.direccion.DireccionDTO;
import mx.com.afirme.midas.persona.PersonaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;

import org.springframework.beans.BeanUtils;

@Embeddable
public class Cotizacion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@OneToOne
	@JoinColumn(name = "SOLICITUD_ID", referencedColumnName="IDTOSOLICITUD")
	private SolicitudDTO solicitud;
	
	@ManyToOne
	@JoinColumn(name = "TIPOPOLIZA_ID", referencedColumnName="IDTOTIPOPOLIZA")
	private TipoPolizaDTO tipoPoliza;
	
	@ManyToOne
	@JoinColumn(name = "MONEDA_ID", referencedColumnName="IDTCMONEDA")
	private MonedaDTO moneda;
	
	@ManyToOne
	@JoinColumn(name = "FORMAPAGO_ID", referencedColumnName="IDTCFORMAPAGO")
	private FormaPagoDTO formaPago;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAINICIOVIGENCIA")
	private Date fechaInicioVigencia;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAFINVIGENCIA")
	private Date fechaFinVigencia;	
	
	@Column(name="NOMBREEMPRESACONTRATANTE")
	private String nombreEmpresaContratante;
	
	@ManyToOne
	@JoinColumn(name = "DIRECCIONCOBRO_ID", referencedColumnName="IDTODIRECCION")
	private DireccionDTO direccionCobro;
	
	@Column(name="NOMBREEMPRESAASEGURADO")
	private String nombreEmpresaAsegurado;	
	
	@Column(name="CODIGOUSUARIOORDENTRABAJO")
	private String codigoUsuarioOrdenTrabajo;
	
	@Column(name="CODIGOUSUARIOCOTIZACION")
	private String codigoUsuarioCotizacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHACREACION")
	private Date fechaCreacion;
	
	@Column(name="CODIGOUSUARIOCREACION")
	private String codigoUsuarioCreacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAMODIFICACION")
	private Date fechaModificacion;
	
	@Column(name="CODIGOUSUARIOMODIFICACION")
	private String codigoUsuarioModificacion;
	
	@Column(name="PERSONACONTRATANTE_ID")
	private Long personaContratanteId;
	
	@Column(name="PERSONAASEGURADO_ID")
	private Long personaAseguradoId;	
	
	@ManyToOne
	@JoinColumn(name = "MEDIOPAGO_ID", referencedColumnName="IDTCMEDIOPAGO")
	private MedioPagoDTO medioPago;	
	
	@Column(name="CLAVEESTATUS")
	private Short claveEstatus;
	
	@Column(name="PORCENTAJEBONIFCOMISION")
	private Double porcentajebonifcomision = 0D;
	
	@Column(name="FOLIOPOLIZAASOCIADA")
	private String folioPolizaAsociada;
	
	@ManyToOne
	@JoinColumn(name = "TIPONEGOCIO_ID", referencedColumnName="IDTCTIPONEGOCIO")
	private TipoNegocioDTO tipoNegocio;	
	
	@Column(name="DIASGRACIA")
	private Integer diasGracia;
	
	@Column(name="CODIGOUSUARIOEMISION")
	private String codigoUsuarioEmision;
	
	@Column(name="CLAVEAUTRETROACDIFER")
	private Short claveAutRetroacDifer;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHASOLAUTRETROACDIFER")
	private Date fechaSolAutRetroacDifer;	
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAAUTRETROACDIFER")
	private Date fechaAutRetroacDifer;
	
	@Column(name="CODIGOUSUARIOAUTRETROACDIFER")
	private String codigoUsuarioAutRetroacDifer;
	
	@Column(name="CLAVEAUTVIGENCIAMAXMIN")
	private Short claveAutVigenciaMaxMin;	
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHASOLAUTVIGENCIAMAXMIN")
	private Date fechaSolAutVigenciaMaxMin;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAAUTVIGENCIAMAXMIN")
	private Date fechaAutVigenciaMaxMin;
	
	@Column(name="CODIGOUSUARIOAUTVIGENCIAMAXMIN")
	private String codigoUsuarioAutVigenciaMaxMin;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAAUTOMISIONDOCOPERILI")
	private Date fechaSolAutOmisionDocOperIli;
	
	@Column(name="CODIGOUSUARIOAUTOMIDOCOPERILI")
	private String codigoUsuarioAutOmisionDocOperIli;
	
	@Column(name="CLAVEAUTOMISIONDOCOPERILICITAS")
	private Short claveAutoOmisionDocOperIli;
	
	@Column(name="NOMBREASEGURADO")
	private String nombreAsegurado;
	
	@Column(name="NOMBRECONTRATANTE")
	private String nombreContratante;
	
	@Column(name="CLAVEMOTIVOENDOSO")
	private Short claveMotivoEndoso;
	
	@Column(name="TIPOCAMBIO")
	private Double tipoCambio;
	
	@Column(name="PERIODODECLARACION")
	private Integer periodoDeclaracion;
	
	@Column(name="PORCENTAJEIVA")
	private Double porcentajeIva;
	
	@Column(name="ACLARACIONES")
	private String aclaraciones;
	
	@Column(name="CLAVEDERECHOSUSUARIO")
	private Short claveDerechosUsuario;
	
	@Column(name="VALORDERECHOSUSUARIO")
	private Double valorDerechosUsuario;
	
	@Column(name="CLAVERECARGOPAGOFRACCUSUARIO")
	private Short claveRecargoPagoFraccionadoUsuario;
	
	@Column(name="PORCENTAJERECPAGOFRACCUSUARIO")
	private Double porcentajeRecargoPagoFraccionadoUsuario;	
	
	@Column(name="VALORRECARGOPAGOFRACCUSUARIO")
	private Double valorRecargoPagoFraccionadoUsuario;
	
	@Column(name="CLAVEAUTORIZACIONDERECHOS")
	private Short claveAutorizacionDerechos;	
	
	@Column(name="CODIGOUSUARIOAUTDERECHOS")
	private String codigoUsuarioAutorizacionDerechos;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAAUTDERECHOS")
	private Date fechaAutorizacionDerechos;	
	
	@Column(name="CLAVEAUTORIZACIONRPF")
	private Short claveAutorizacionRecargoPagoFraccionado;
	
	@Column(name="CODIGOUSUARIOAUTRPF")
	private String codigoUsuarioAutorizacionRecargoPagoFraccionado;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAAUTRPF")
	private Date fechaAutorizacionRecargoPagoFraccionado;
	
	@Column(name="DESCRIPCIONLICITACION")
	private String descripcionLicitacion;
	
	@Column(name="DESCRIPCIONLICITACIONENDOSO")
	private String descripcionLicitacionEndoso;
	
	@Column(name="CLAVEIMPRESIONSUMAASEGURADA")
	private Short claveImpresionSumaAsegurada;
	
	@Column(name="CODIGOUSUARIOSOLAUTDIASGRACIA")
	private String codigoUsuarioSolicitaAutDiasGracia;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHASOLAUTDIASGRACIA")
	private Date fechaSolicitudAutDiasGracia;
	
	@Column(name="CLAVEAUTDIASGRACIA")
	private Short claveAutorizacionDiasGracia;
	
	@Column(name="CODIGOUSUARIOAUTDIASGRACIA")
	private String codigoUsuarioAutorizaDiasGracia;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAAUTDIASGRACIA")
	private Date fechaAutorizaDiasGracia;	
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHALIBERACION")
	private Date fechaLiberacion;
	
	@ManyToOne
	@JoinColumn(name="PERSONABENEFICIARIO_ID", referencedColumnName="IDTOPERSONA")
	private PersonaDTO beneficiario;
	
	@Column(name="DOMICILIOCONTRATANTE_ID")
	private BigDecimal domicilioContratanteId;
	
	@Column(name="DOMICILIOASEGURADO_ID")
	private BigDecimal doomicilioAseguradoId;
	
	@Column(name="PORCENTAJEDESCUENTOGLOBAL")
	private Double porcentajeDescuentoGlobal;	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "NEGDERECHOPOLIZA_ID", referencedColumnName="idToNegDerechoPoliza")
	private NegocioDerechoPoliza negocioDerechoPoliza;	
	
	@Column(name="NEGDERECHOENDOSO_ID")
	private Long negocioDerechoEndosoId;
	
	@Column(name="VALORPRIMATOTAL")
	private BigDecimal valorPrimaTotal;
	
	@Column(name="VALORPRIMATOTALENDOSO")
	private BigDecimal valorPrimaTotalEndoso;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHASEGUIMIENTO")
	private Date fechaSeguimiento;
	
	@Column(name="VALORTOTALPRIMAS")
	private BigDecimal valorTotalPrimas;
	
	@Column(name="CONFLICTONUMEROSERIE")
	private Boolean conflictoNumeroSerie;	
	
	@Column(name="PCTRECARGOPAGOFRACCIONADO")
	private Double porcentajePagoFraccionado;
	
	@Column(name = "NUMPRIMERRECIBO", nullable = true)
	private String numeroPrimerRecibo;
	
	@Column(name = "DIGVERIFICADORRECIBO", nullable = true)
	private String digitoVerificadorRecibo;
	
	@Column(name = "IDCLIENTECONDUCTOCOBRO", nullable = true,  precision = 16, scale = 2)
	private Long idConductoCobroCliente;
	
	@Column(name = "ESTATUSPAGORECIBO", nullable = true)
	private Boolean primerReciboPagado;
	
	@Column(name = "PRIMERRECIBOUNIFICADO")
	private Boolean primerReciboUnificado = Boolean.FALSE;
	
	@Column(name = "NUMEROAUTORIZACION", nullable = true)
	private String numeroAutorizacion;
	
	@Column(name = "IGUALACIONNIVELCOTIZACION")
	private Boolean igualacionNivelCotizacion = Boolean.FALSE;
	
	@Column(name = "CLAVEAFECTACIONPRIMAS")
	private Short claveAfectacionPrimas;
	
	@Column(name = "IGUALACIONPRIMAANTERIOR")
	private Double primaTotalAntesDeIgualacion;
	
	@Column(name = "IGUALACIONDESCUENTOGENERADO")
	private Double descuentoIgualacionPrimas;
	
	@Column(name = "POLIZAREFERENCIA")
	private Long polizaReferencia;
	
	@Column(name = "TIPOAGRUPACIONRECIBOS")
	private String tipoAgrupacionRecibos;
	
	@Column(name = "TIPOCOTIZACION")
	private String tipoCotizacion;

	@Column(name = "PRIMATARIFA")
	private Double primaTarifa;
	
	
	
	//CONSTANTES ASOCIADAS A LA CLAVE DE AFECTACION DE PRIMAS
	public static final short CVE_AFECTACION_NCR = 1;
	public static final short CVE_AFECTACION_ABONO_POLIZA = 2;
	public static final short CVE_AFECTACION_CHEQUE = 3;
	
	
	

	public Cotizacion(){
		
	}
	public Cotizacion(Cotizacion base){
		if(base == null){
			throw new IllegalArgumentException();
		}		
		BeanUtils.copyProperties(base, this);		
	}
	//Atributos @Transient
	@Transient
	private Double primaNetaAnual;
	
	@Transient
	private Double primaNetaCotizacion;
	
	@Transient
	private Double montoRecargoPagoFraccionado;
	
	@Transient
	private Double derechosPoliza;
	
	@Transient
	private Double factorIVA;
	
	@Transient
	private Double montoIVA;
	
	@Transient
	private Double primaNetaTotal;
	
	@Transient
	private Double montoBonificacionComision;
	
	@Transient
	private Double montoBonificacionComisionPagoFraccionado;
	
	@Transient
	private Double montoComisionPagoFraccionado;
	
	@Transient
	private Double diasPorDevengar;
	
	@Transient
	private Double descuentoVolumen;
	
	@Transient
	private NegocioTipoPoliza negocioTipoPoliza;
	
	@Transient
	private String numeroEndoso;
	
	@Transient
	private String estatusCobranza;
	
	@Transient
	private BigDecimal importeNotaCredito;
	
	
	public Double getPrimaTotalAntesDeIgualacion() {
		return primaTotalAntesDeIgualacion;
	}
	public void setPrimaTotalAntesDeIgualacion(Double primaTotalAntesDeIgualacion) {
		this.primaTotalAntesDeIgualacion = primaTotalAntesDeIgualacion;
	}
	public Double getDescuentoIgualacionPrimas() {
		return descuentoIgualacionPrimas;
	}
	public void setDescuentoIgualacionPrimas(Double descuentoIgualacionPrimas) {
		this.descuentoIgualacionPrimas = descuentoIgualacionPrimas;
	}
	public Long getPolizaReferencia() {
		return polizaReferencia;
	}
	public void setPolizaReferencia(Long polizaReferencia) {
		this.polizaReferencia = polizaReferencia;
	}
	public SolicitudDTO getSolicitud() {
		return solicitud;
	}
	public void setSolicitud(SolicitudDTO solicitud) {
		this.solicitud = solicitud;
	}

	
	
	public TipoPolizaDTO getTipoPoliza() {
		return tipoPoliza;
	}
	public void setTipoPoliza(TipoPolizaDTO tipoPoliza) {
		this.tipoPoliza = tipoPoliza;
	}
	

	public MonedaDTO getMoneda() {
		return moneda;
	}
	public void setMoneda(MonedaDTO moneda) {
		this.moneda = moneda;
	}
	
	
	public FormaPagoDTO getFormaPago() {
		return formaPago;
	}
	public void setFormaPago(FormaPagoDTO formaPago) {
		this.formaPago = formaPago;
	}
	
	
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}
	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	
	
	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}
	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}
	
	
	public String getNombreEmpresaContratante() {
		return nombreEmpresaContratante;
	}
	public void setNombreEmpresaContratante(String nombreEmpresaContratante) {
		this.nombreEmpresaContratante = nombreEmpresaContratante;
	}
	
	
	public DireccionDTO getDireccionCobro() {
		return direccionCobro;
	}
	public void setDireccionCobro(DireccionDTO direccionCobro) {
		this.direccionCobro = direccionCobro;
	}
	
	
	public String getNombreEmpresaAsegurado() {
		return nombreEmpresaAsegurado;
	}
	public void setNombreEmpresaAsegurado(String nombreEmpresaAsegurado) {
		this.nombreEmpresaAsegurado = nombreEmpresaAsegurado;
	}
	
	public String getCodigoUsuarioOrdenTrabajo() {
		return codigoUsuarioOrdenTrabajo;
	}
	public void setCodigoUsuarioOrdenTrabajo(String codigoUsuarioOrdenTrabajo) {
		this.codigoUsuarioOrdenTrabajo = codigoUsuarioOrdenTrabajo;
	}
		
	public String getCodigoUsuarioCotizacion() {
		return codigoUsuarioCotizacion;
	}
	public void setCodigoUsuarioCotizacion(String codigoUsuarioCotizacion) {
		this.codigoUsuarioCotizacion = codigoUsuarioCotizacion;
	}
	
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}
	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
	
	
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	public String getCodigoUsuarioModificacion() {
		return codigoUsuarioModificacion;
	}
	public void setCodigoUsuarioModificacion(String codigoUsuarioModificacion) {
		this.codigoUsuarioModificacion = codigoUsuarioModificacion;
	}
	
	public Long getPersonaContratanteId() {
		return personaContratanteId;
	}
	public void setPersonaContratanteId(Long personaContratanteId) {
		this.personaContratanteId = personaContratanteId;
	}
	
	public Long getPersonaAseguradoId() {
		return personaAseguradoId;
	}
	public void setPersonaAseguradoId(Long personaAseguradoId) {
		this.personaAseguradoId = personaAseguradoId;
	}
	

	public MedioPagoDTO getMedioPago() {
		return medioPago;
	}
	public void setMedioPago(MedioPagoDTO medioPago) {
		this.medioPago = medioPago;
	}
	
	
	public Short getClaveEstatus() {
		return claveEstatus;
	}
	public void setClaveEstatus(Short claveEstatus) {
		this.claveEstatus = claveEstatus;
	}
	
	
	public Double getPorcentajebonifcomision() {
		return porcentajebonifcomision;
	}
	public void setPorcentajebonifcomision(Double porcentajebonifcomision) {
		this.porcentajebonifcomision = porcentajebonifcomision;
	}
	
	
	public String getFolioPolizaAsociada() {
		return folioPolizaAsociada;
	}
	public void setFolioPolizaAsociada(String folioPolizaAsociada) {
		this.folioPolizaAsociada = folioPolizaAsociada;
	}

	public TipoNegocioDTO getTipoNegocio() {
		return tipoNegocio;
	}
	public void setTipoNegocio(TipoNegocioDTO tipoNegocio) {
		this.tipoNegocio = tipoNegocio;
	}
	
	
	public Integer getDiasGracia() {
		return diasGracia;
	}
	public void setDiasGracia(Integer diasGracia) {
		this.diasGracia = diasGracia;
	}
	
	
	public String getCodigoUsuarioEmision() {
		return codigoUsuarioEmision;
	}
	public void setCodigoUsuarioEmision(String codigoUsuarioEmision) {
		this.codigoUsuarioEmision = codigoUsuarioEmision;
	}
	
	
	public Short getClaveAutRetroacDifer() {
		return claveAutRetroacDifer;
	}
	public void setClaveAutRetroacDifer(Short claveAutRetroacDifer) {
		this.claveAutRetroacDifer = claveAutRetroacDifer;
	}
	

	public Date getFechaSolAutRetroacDifer() {
		return fechaSolAutRetroacDifer;
	}
	public void setFechaSolAutRetroacDifer(Date fechaSolAutRetroacDifer) {
		this.fechaSolAutRetroacDifer = fechaSolAutRetroacDifer;
	}
	
	
	public Date getFechaAutRetroacDifer() {
		return fechaAutRetroacDifer;
	}
	public void setFechaAutRetroacDifer(Date fechaAutRetroacDifer) {
		this.fechaAutRetroacDifer = fechaAutRetroacDifer;
	}
	
	
	public String getCodigoUsuarioAutRetroacDifer() {
		return codigoUsuarioAutRetroacDifer;
	}
	public void setCodigoUsuarioAutRetroacDifer(String codigoUsuarioAutRetroacDifer) {
		this.codigoUsuarioAutRetroacDifer = codigoUsuarioAutRetroacDifer;
	}
	
	
	public Short getClaveAutVigenciaMaxMin() {
		return claveAutVigenciaMaxMin;
	}
	public void setClaveAutVigenciaMaxMin(Short claveAutVigenciaMaxMin) {
		this.claveAutVigenciaMaxMin = claveAutVigenciaMaxMin;
	}
	

	public Date getFechaSolAutVigenciaMaxMin() {
		return fechaSolAutVigenciaMaxMin;
	}
	public void setFechaSolAutVigenciaMaxMin(Date fechaSolAutVigenciaMaxMin) {
		this.fechaSolAutVigenciaMaxMin = fechaSolAutVigenciaMaxMin;
	}
	
	
	public Date getFechaAutVigenciaMaxMin() {
		return fechaAutVigenciaMaxMin;
	}
	public void setFechaAutVigenciaMaxMin(Date fechaAutVigenciaMaxMin) {
		this.fechaAutVigenciaMaxMin = fechaAutVigenciaMaxMin;
	}
	
	
	public String getCodigoUsuarioAutVigenciaMaxMin() {
		return codigoUsuarioAutVigenciaMaxMin;
	}
	public void setCodigoUsuarioAutVigenciaMaxMin(
			String codigoUsuarioAutVigenciaMaxMin) {
		this.codigoUsuarioAutVigenciaMaxMin = codigoUsuarioAutVigenciaMaxMin;
	}

	
	
	public Date getFechaSolAutOmisionDocOperIli() {
		return fechaSolAutOmisionDocOperIli;
	}
	public void setFechaSolAutOmisionDocOperIli(Date fechaSolAutOmisionDocOperIli) {
		this.fechaSolAutOmisionDocOperIli = fechaSolAutOmisionDocOperIli;
	}
	
	
	public String getCodigoUsuarioAutOmisionDocOperIli() {
		return codigoUsuarioAutOmisionDocOperIli;
	}
	public void setCodigoUsuarioAutOmisionDocOperIli(
			String codigoUsuarioAutOmisionDocOperIli) {
		this.codigoUsuarioAutOmisionDocOperIli = codigoUsuarioAutOmisionDocOperIli;
	}
	
	public Short getClaveAutoOmisionDocOperIli() {
		return claveAutoOmisionDocOperIli;
	}
	public void setClaveAutoOmisionDocOperIli(Short claveAutoOmisionDocOperIli) {
		this.claveAutoOmisionDocOperIli = claveAutoOmisionDocOperIli;
	}
	
	
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}
	
	
	public String getNombreContratante() {
		return nombreContratante;
	}
	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}
	
	
	public Short getClaveMotivoEndoso() {
		return claveMotivoEndoso;
	}
	public void setClaveMotivoEndoso(Short claveMotivoEndoso) {
		this.claveMotivoEndoso = claveMotivoEndoso;
	}
	
	
	public Double getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	
	public Integer getPeriodoDeclaracion() {
		return periodoDeclaracion;
	}
	public void setPeriodoDeclaracion(Integer periodoDeclaracion) {
		this.periodoDeclaracion = periodoDeclaracion;
	}
	
	
	public Double getPorcentajeIva() {
		return porcentajeIva;
	}
	public void setPorcentajeIva(Double porcentajeIva) {
		this.porcentajeIva = porcentajeIva;
	}
	
	
	public String getAclaraciones() {
		return aclaraciones;
	}
	public void setAclaraciones(String aclaraciones) {
		this.aclaraciones = aclaraciones;
	}
	
	
	public Short getClaveDerechosUsuario() {
		return claveDerechosUsuario;
	}
	public void setClaveDerechosUsuario(Short claveDerechosUsuario) {
		this.claveDerechosUsuario = claveDerechosUsuario;
	}
	
	
	public Double getValorDerechosUsuario() {
		return valorDerechosUsuario;
	}
	public void setValorDerechosUsuario(Double valorDerechosUsuario) {
		this.valorDerechosUsuario = valorDerechosUsuario;
	}
	

	public Short getClaveRecargoPagoFraccionadoUsuario() {
		return claveRecargoPagoFraccionadoUsuario;
	}
	public void setClaveRecargoPagoFraccionadoUsuario(
			Short claveRecargoPagoFraccionadoUsuario) {
		this.claveRecargoPagoFraccionadoUsuario = claveRecargoPagoFraccionadoUsuario;
	}
	
	
	public Double getPorcentajeRecargoPagoFraccionadoUsuario() {
		return porcentajeRecargoPagoFraccionadoUsuario;
	}
	public void setPorcentajeRecargoPagoFraccionadoUsuario(
			Double porcentajeRecargoPagoFraccionadoUsuario) {
		this.porcentajeRecargoPagoFraccionadoUsuario = porcentajeRecargoPagoFraccionadoUsuario;
	}
	
	
	public Double getValorRecargoPagoFraccionadoUsuario() {
		return valorRecargoPagoFraccionadoUsuario;
	}
	public void setValorRecargoPagoFraccionadoUsuario(
			Double valorRecargoPagoFraccionadoUsuario) {
		this.valorRecargoPagoFraccionadoUsuario = valorRecargoPagoFraccionadoUsuario;
	}
	
	
	public Short getClaveAutorizacionDerechos() {
		return claveAutorizacionDerechos;
	}
	public void setClaveAutorizacionDerechos(Short claveAutorizacionDerechos) {
		this.claveAutorizacionDerechos = claveAutorizacionDerechos;
	}
	
	
	public String getCodigoUsuarioAutorizacionDerechos() {
		return codigoUsuarioAutorizacionDerechos;
	}
	public void setCodigoUsuarioAutorizacionDerechos(
			String codigoUsuarioAutorizacionDerechos) {
		this.codigoUsuarioAutorizacionDerechos = codigoUsuarioAutorizacionDerechos;
	}
	
	
	public Date getFechaAutorizacionDerechos() {
		return fechaAutorizacionDerechos;
	}
	public void setFechaAutorizacionDerechos(Date fechaAutorizacionDerechos) {
		this.fechaAutorizacionDerechos = fechaAutorizacionDerechos;
	}
	
	
	public Short getClaveAutorizacionRecargoPagoFraccionado() {
		return claveAutorizacionRecargoPagoFraccionado;
	}
	public void setClaveAutorizacionRecargoPagoFraccionado(
			Short claveAutorizacionRecargoPagoFraccionado) {
		this.claveAutorizacionRecargoPagoFraccionado = claveAutorizacionRecargoPagoFraccionado;
	}
	
	
	public String getCodigoUsuarioAutorizacionRecargoPagoFraccionado() {
		return codigoUsuarioAutorizacionRecargoPagoFraccionado;
	}
	public void setCodigoUsuarioAutorizacionRecargoPagoFraccionado(
			String codigoUsuarioAutorizacionRecargoPagoFraccionado) {
		this.codigoUsuarioAutorizacionRecargoPagoFraccionado = codigoUsuarioAutorizacionRecargoPagoFraccionado;
	}
	
	
	public Date getFechaAutorizacionRecargoPagoFraccionado() {
		return fechaAutorizacionRecargoPagoFraccionado;
	}
	public void setFechaAutorizacionRecargoPagoFraccionado(
			Date fechaAutorizacionRecargoPagoFraccionado) {
		this.fechaAutorizacionRecargoPagoFraccionado = fechaAutorizacionRecargoPagoFraccionado;
	}
	
	
	public String getDescripcionLicitacion() {
		return descripcionLicitacion;
	}
	public void setDescripcionLicitacion(String descripcionLicitacion) {
		this.descripcionLicitacion = descripcionLicitacion;
	}
	

	public String getDescripcionLicitacionEndoso() {
		return descripcionLicitacionEndoso;
	}
	public void setDescripcionLicitacionEndoso(String descripcionLicitacionEndoso) {
		this.descripcionLicitacionEndoso = descripcionLicitacionEndoso;
	}
	

	public Short getClaveImpresionSumaAsegurada() {
		return claveImpresionSumaAsegurada;
	}
	public void setClaveImpresionSumaAsegurada(Short claveImpresionSumaAsegurada) {
		this.claveImpresionSumaAsegurada = claveImpresionSumaAsegurada;
	}
	
	@Column(name="CODIGOUSUARIOSOLAUTDIASGRACIA")
	public String getCodigoUsuarioSolicitaAutDiasGracia() {
		return codigoUsuarioSolicitaAutDiasGracia;
	}
	public void setCodigoUsuarioSolicitaAutDiasGracia(
			String codigoUsuarioSolicitaAutDiasGracia) {
		this.codigoUsuarioSolicitaAutDiasGracia = codigoUsuarioSolicitaAutDiasGracia;
	}
	
	
	public Date getFechaSolicitudAutDiasGracia() {
		return fechaSolicitudAutDiasGracia;
	}
	public void setFechaSolicitudAutDiasGracia(Date fechaSolicitudAutDiasGracia) {
		this.fechaSolicitudAutDiasGracia = fechaSolicitudAutDiasGracia;
	}
	
	
	public Short getClaveAutorizacionDiasGracia() {
		return claveAutorizacionDiasGracia;
	}
	public void setClaveAutorizacionDiasGracia(Short claveAutorizacionDiasGracia) {
		this.claveAutorizacionDiasGracia = claveAutorizacionDiasGracia;
	}
	
	
	public String getCodigoUsuarioAutorizaDiasGracia() {
		return codigoUsuarioAutorizaDiasGracia;
	}
	public void setCodigoUsuarioAutorizaDiasGracia(
			String codigoUsuarioAutorizaDiasGracia) {
		this.codigoUsuarioAutorizaDiasGracia = codigoUsuarioAutorizaDiasGracia;
	}
	
	
	public Date getFechaAutorizaDiasGracia() {
		return fechaAutorizaDiasGracia;
	}
	public void setFechaAutorizaDiasGracia(Date fechaAutorizaDiasGracia) {
		this.fechaAutorizaDiasGracia = fechaAutorizaDiasGracia;
	}
	
	
	public Date getFechaLiberacion() {
		return fechaLiberacion;
	}
	public void setFechaLiberacion(Date fechaLiberacion) {
		this.fechaLiberacion = fechaLiberacion;
	}
	
	
	public PersonaDTO getBeneficiario() {
		return beneficiario;
	}
	public void setBeneficiario(PersonaDTO beneficiario) {
		this.beneficiario = beneficiario;
	}
	
	
	public BigDecimal getDomicilioContratanteId() {
		return domicilioContratanteId;
	}
	public void setDomicilioContratanteId(BigDecimal domicilioContratanteId) {
		this.domicilioContratanteId = domicilioContratanteId;
	}
	
	
	public BigDecimal getDoomicilioAseguradoId() {
		return doomicilioAseguradoId;
	}
	public void setDoomicilioAseguradoId(BigDecimal doomicilioAseguradoId) {
		this.doomicilioAseguradoId = doomicilioAseguradoId;
	}
	
	
	public Double getPorcentajeDescuentoGlobal() {
		return porcentajeDescuentoGlobal;
	}
	public void setPorcentajeDescuentoGlobal(Double porcentajeDescuentoGlobal) {
		this.porcentajeDescuentoGlobal = porcentajeDescuentoGlobal;
	}
	
	
	public NegocioDerechoPoliza getNegocioDerechoPoliza() {
		return negocioDerechoPoliza;
	}
	public void setNegocioDerechoPoliza(NegocioDerechoPoliza negocioDerechoPoliza) {
		this.negocioDerechoPoliza = negocioDerechoPoliza;
	}
		
	
	public Long getNegocioDerechoEndosoId() {
		return negocioDerechoEndosoId;
	}
	public void setNegocioDerechoEndosoId(Long negocioDerechoEndosoId) {
		this.negocioDerechoEndosoId = negocioDerechoEndosoId;
	}
	
	public BigDecimal getValorPrimaTotal() {
		return valorPrimaTotal;
	}
	public void setValorPrimaTotal(BigDecimal valorPrimaTotal) {
		this.valorPrimaTotal = valorPrimaTotal;
	}
		
	
	public BigDecimal getValorPrimaTotalEndoso() {
		return valorPrimaTotalEndoso;
	}
	public void setValorPrimaTotalEndoso(BigDecimal valorPrimaTotalEndoso) {
		this.valorPrimaTotalEndoso = valorPrimaTotalEndoso;
	}
	
	
	public Date getFechaSeguimiento() {
		return fechaSeguimiento;
	}
	public void setFechaSeguimiento(Date fechaSeguimiento) {
		this.fechaSeguimiento = fechaSeguimiento;
	}
	

	public BigDecimal getValorTotalPrimas() {
		return valorTotalPrimas;
	}
	public void setValorTotalPrimas(BigDecimal valorTotalPrimas) {
		this.valorTotalPrimas = valorTotalPrimas;
	}
	

	public Boolean getConflictoNumeroSerie() {
		return conflictoNumeroSerie;
	}
	public void setConflictoNumeroSerie(Boolean conflictoNumeroSerie) {
		this.conflictoNumeroSerie = conflictoNumeroSerie;
	}
	
	public String getNumeroEndoso() {
		return numeroEndoso;
	}
	public void setNumeroEndoso(String numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}
	
	public Double getPrimaNetaAnual() {
		return primaNetaAnual;
	}
	public void setPrimaNetaAnual(Double primaNetaAnual) {
		this.primaNetaAnual = primaNetaAnual;
	}
	
	public Double getPrimaNetaCotizacion() {
		return primaNetaCotizacion;
	}
	public void setPrimaNetaCotizacion(Double primaNetaCotizacion) {
		this.primaNetaCotizacion = primaNetaCotizacion;
	}
	
	public Double getMontoRecargoPagoFraccionado() {
		return montoRecargoPagoFraccionado;
	}
	public void setMontoRecargoPagoFraccionado(Double montoRecargoPagoFraccionado) {
		this.montoRecargoPagoFraccionado = montoRecargoPagoFraccionado;
	}
	
	public Double getDerechosPoliza() {
		return derechosPoliza;
	}
	public void setDerechosPoliza(Double derechosPoliza) {
		this.derechosPoliza = derechosPoliza;
	}
	
	public Double getFactorIVA() {
		return factorIVA;
	}
	public void setFactorIVA(Double factorIVA) {
		this.factorIVA = factorIVA;
	}
	
	public Double getMontoIVA() {
		return montoIVA;
	}
	public void setMontoIVA(Double montoIVA) {
		this.montoIVA = montoIVA;
	}
	
	public Double getPrimaNetaTotal() {
		return primaNetaTotal;
	}
	public void setPrimaNetaTotal(Double primaNetaTotal) {
		this.primaNetaTotal = primaNetaTotal;
	}
	
	public Double getMontoBonificacionComision() {
		return montoBonificacionComision;
	}
	public void setMontoBonificacionComision(Double montoBonificacionComision) {
		this.montoBonificacionComision = montoBonificacionComision;
	}
	
	public Double getMontoBonificacionComisionPagoFraccionado() {
		return montoBonificacionComisionPagoFraccionado;
	}
	public void setMontoBonificacionComisionPagoFraccionado(
			Double montoBonificacionComisionPagoFraccionado) {
		this.montoBonificacionComisionPagoFraccionado = montoBonificacionComisionPagoFraccionado;
	}
	
	public Double getMontoComisionPagoFraccionado() {
		return montoComisionPagoFraccionado;
	}
	public void setMontoComisionPagoFraccionado(Double montoComisionPagoFraccionado) {
		this.montoComisionPagoFraccionado = montoComisionPagoFraccionado;
	}
	
	public Double getPorcentajePagoFraccionado() {
		return porcentajePagoFraccionado;
	}
	public void setPorcentajePagoFraccionado(Double porcentajePagoFraccionado) {
		this.porcentajePagoFraccionado = porcentajePagoFraccionado;
	}
	
	public String getNumeroPrimerRecibo() {
		return numeroPrimerRecibo;
	}
	public void setNumeroPrimerRecibo(String numeroPrimerRecibo) {
		this.numeroPrimerRecibo = numeroPrimerRecibo;
	}
	public String getDigitoVerificadorRecibo() {
		return digitoVerificadorRecibo;
	}
	public void setDigitoVerificadorRecibo(String digitoVerificadorRecibo) {
		this.digitoVerificadorRecibo = digitoVerificadorRecibo;
	}
	public Long getIdConductoCobroCliente() {
		return idConductoCobroCliente;
	}
	public void setIdConductoCobroCliente(Long idConductoCobroCliente) {
		this.idConductoCobroCliente = idConductoCobroCliente;
	}
	public Boolean getPrimerReciboPagado() {
		return primerReciboPagado;
	}
	public void setPrimerReciboPagado(Boolean primerReciboPagado) {
		this.primerReciboPagado = primerReciboPagado;
	}
	public Boolean getPrimerReciboUnificado() {
		return primerReciboUnificado;
	}
	public void setPrimerReciboUnificado(Boolean primerReciboUnificado) {
		this.primerReciboUnificado = primerReciboUnificado;
	}
	public String getNumeroAutorizacion() {
		return numeroAutorizacion;
	}
	public void setNumeroAutorizacion(String numeroAutorizacion) {
		this.numeroAutorizacion = numeroAutorizacion;
	}
	public Boolean getIgualacionNivelCotizacion() {
		return igualacionNivelCotizacion;
	}
	public void setIgualacionNivelCotizacion(Boolean igualacionNivelCotizacion) {
		this.igualacionNivelCotizacion = igualacionNivelCotizacion;
	}
	public Short getClaveAfectacionPrimas() {
		return claveAfectacionPrimas;
	}
	public void setClaveAfectacionPrimas(Short claveAfectacionPrimas) {
		this.claveAfectacionPrimas = claveAfectacionPrimas;
	}
	public Double getDiasPorDevengar() {
		return diasPorDevengar;
	}
	public void setDiasPorDevengar(Double diasPorDevengar) {
		this.diasPorDevengar = diasPorDevengar;
	}
	
	public Double getDescuentoVolumen() {
		return descuentoVolumen;
	}
	public void setDescuentoVolumen(Double descuentoVolumen) {
		this.descuentoVolumen = descuentoVolumen;
	}
	
	public NegocioTipoPoliza getNegocioTipoPoliza() {
		return negocioTipoPoliza;
	}
	public void setNegocioTipoPoliza(NegocioTipoPoliza negocioTipoPoliza) {
		this.negocioTipoPoliza = negocioTipoPoliza;
	}
	
	public String getEstatusCobranza() {
		return estatusCobranza;
	}
	public void setEstatusCobranza(String estatusCobranza) {
		this.estatusCobranza = estatusCobranza;
	}

	public BigDecimal getImporteNotaCredito() {
		return importeNotaCredito;
	}
	public void setImporteNotaCredito(BigDecimal importeNotaCredito) {
		this.importeNotaCredito = importeNotaCredito;
	}
	
	public String getTipoAgrupacionRecibos() {
		return tipoAgrupacionRecibos;
	}
	public void setTipoAgrupacionRecibos(String tipoAgrupacionRecibos) {
		this.tipoAgrupacionRecibos = tipoAgrupacionRecibos;
	}

	public String getTipoCotizacion() {
		return tipoCotizacion;
	}
	
	public void setTipoCotizacion(String tipoCotizacion) {
		this.tipoCotizacion = tipoCotizacion;
	}
	
	public Double getPrimaTarifa() {
		return primaTarifa;
	}
	
	public void setPrimaTarifa(Double primaTarifa) {
		this.primaTarifa = primaTarifa;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("solicitud: " + this.solicitud);
		sb.append("]");
		
		return sb.toString();
	}
}
