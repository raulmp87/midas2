package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.texto;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.afirme.bitemporal.annotations.BitemporalNotNull;
import com.anasoft.os.daofusion.bitemporal.BitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;
import com.anasoft.os.daofusion.bitemporal.WrappedBitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.WrappedValueAccessor;

@Entity
@Table(name = "MTEXADICIONALCOTC", schema = "MIDAS")
public class TexAdicionalCotContinuity implements Serializable, EntidadContinuity<TexAdicionalCot, BitemporalTexAdicionalCot>{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6714934836248660667L;
	
	public static final String PARENT_KEY_NAME = "cotizacionContinuity.id";
	public static final String PARENT_BUSINESS_KEY_NAME = "cotizacionContinuity.numero";
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQMTEXADICIONALCOTCOTCONTID")
	@SequenceGenerator(name = "SEQMTEXADICIONALCOTCOTCONTID", sequenceName = "MIDAS.SEQMTEXADICIONALCOTCOTCONTID", allocationSize = 1)
	@Column(name = "id")
	private Long id;

	@OneToMany(mappedBy = "continuity", cascade = CascadeType.ALL, orphanRemoval=true)
	private Collection<BitemporalTexAdicionalCot> texAdicionales = new LinkedList<BitemporalTexAdicionalCot>();

	@BitemporalNotNull
	@ManyToOne
	@JoinColumn(name = "MCOTIZACIONC_ID", referencedColumnName = "id", nullable = false)
	private CotizacionContinuity cotizacionContinuity;	

	public TexAdicionalCotContinuity() {
		if(cotizacionContinuity == null) {
			cotizacionContinuity = new CotizacionContinuity();
		}
	}

	public TexAdicionalCotContinuity(CotizacionContinuity cotizacionContinuity) {
		this.cotizacionContinuity = cotizacionContinuity;
	}

	@Override
	@Transient
	public BitemporalProperty<TexAdicionalCot, BitemporalTexAdicionalCot> getBitemporalProperty() {
		return getTexAdicionales();
	}

	@SuppressWarnings("serial")
	public WrappedBitemporalProperty<TexAdicionalCot, BitemporalTexAdicionalCot, TexAdicionalCotContinuity> getTexAdicionales() {
		return new WrappedBitemporalProperty<TexAdicionalCot, BitemporalTexAdicionalCot, TexAdicionalCotContinuity>(
				texAdicionales,
				new WrappedValueAccessor<TexAdicionalCot, BitemporalTexAdicionalCot, TexAdicionalCotContinuity>() {

					public BitemporalTexAdicionalCot wrapValue(TexAdicionalCot value,
							IntervalWrapper validityInterval,
							boolean twoPhaseMode) {
						return new BitemporalTexAdicionalCot(value, validityInterval,
								TexAdicionalCotContinuity.this,null,twoPhaseMode);
					}

				});
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CotizacionContinuity getCotizacionContinuity() {
		return cotizacionContinuity;
	}

	public void setCotizacionContinuity(
			CotizacionContinuity cotizacionContinuity) {
		this.cotizacionContinuity = cotizacionContinuity;
	}

    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TexAdicionalCotContinuity other = (TexAdicionalCotContinuity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.id;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CotizacionContinuity getParentContinuity() {
		return getCotizacionContinuity();
	}
	
	@SuppressWarnings("rawtypes")
	public void setParentContinuity(EntidadContinuity parentContinuity) {
		setCotizacionContinuity((CotizacionContinuity)parentContinuity);
	};
	
	@Override
	public Class<BitemporalTexAdicionalCot> getBitemporalClass() {
		return BitemporalTexAdicionalCot.class;
	}
}
