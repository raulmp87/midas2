package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.subinciso;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.anasoft.os.daofusion.bitemporal.Bitemporal;
import com.anasoft.os.daofusion.bitemporal.BitemporalWrapper;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;

@Entity
@Table(name="MSUBINCISOSECCIONB",schema="MIDAS")
@Access(AccessType.FIELD)
public class BitemporalSubIncisoSeccion extends
		BitemporalWrapper<SubIncisoSeccion, SubIncisoSeccionContinuity> implements EntidadBitemporal<SubIncisoSeccion,BitemporalSubIncisoSeccion>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6124687560023644230L;
	@Embedded
	private SubIncisoSeccion value;
	@ManyToOne
	@JoinColumn(name = "MSUBINCISOSECCIONC_ID", referencedColumnName="id")
	private SubIncisoSeccionContinuity continuity;
	
	public BitemporalSubIncisoSeccion() {
		if(continuity == null) {
			continuity = new SubIncisoSeccionContinuity();
		}
		if(value == null) {
			value = new SubIncisoSeccion();
		}
	}

	public BitemporalSubIncisoSeccion(SubIncisoSeccion value,
			IntervalWrapper validityInterval, SubIncisoSeccionContinuity continuity, String valueId, boolean twoPhaseMode) {
		super(value, validityInterval, continuity, valueId, twoPhaseMode);
	}

	@Override
	public Bitemporal copyWith(IntervalWrapper validityInterval, boolean twoPhaseMode) {
		return new BitemporalSubIncisoSeccion(value, validityInterval,getContinuity(), getValueId(), twoPhaseMode);
	}

	@Override
	public SubIncisoSeccion getValue() {
		return value;
	}

	@Override
	public void setValue(SubIncisoSeccion value) {
		this.value = value;
	}
	
	
	
	@Override
	public SubIncisoSeccion  getEmbedded() {
		return this.value;
	}
	
	@Override
	public void setEmbedded(SubIncisoSeccion value) {
		this.value = value;
	}
	
	@Override
	public SubIncisoSeccionContinuity getContinuity() {
		return continuity;
	}

	@Override
	protected void setContinuity(SubIncisoSeccionContinuity continuity) {
		this.continuity = continuity;
		
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public SubIncisoSeccionContinuity getEntidadContinuity() {
		return getContinuity();
	}
	
	public void setEntidadContinuity(EntidadContinuity<SubIncisoSeccion,BitemporalSubIncisoSeccion> entidadContinuity){
		this.continuity = (SubIncisoSeccionContinuity) entidadContinuity;
	}

}
