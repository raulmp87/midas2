package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.anasoft.os.daofusion.bitemporal.Bitemporal;
import com.anasoft.os.daofusion.bitemporal.BitemporalWrapper;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;

@Entity
@Table(name="MSECCIONINCISOB",schema="MIDAS")
@Access(AccessType.FIELD)
public class BitemporalSeccionInciso extends BitemporalWrapper<SeccionInciso, SeccionIncisoContinuity> implements EntidadBitemporal<SeccionInciso, BitemporalSeccionInciso> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4938690464550149096L;
	@Embedded
	private SeccionInciso value;
	@ManyToOne
	@JoinColumn(name = "MSECCIONINCISOC_ID", referencedColumnName="id")
	private SeccionIncisoContinuity continuity;

	
	public BitemporalSeccionInciso() {
		if(continuity == null) {
			continuity = new SeccionIncisoContinuity();
		}
		if(value == null) {
			value = new SeccionInciso();
		}
	}

	public BitemporalSeccionInciso(SeccionInciso value,
			IntervalWrapper validityInterval, SeccionIncisoContinuity continuity,String valueId, boolean twoPhaseMode) {
		super(value, validityInterval,continuity, valueId, twoPhaseMode);
	}
	
	public BitemporalSeccionInciso(SeccionInciso value, SeccionIncisoContinuity continuity){
		this.value = value;
		this.continuity = continuity;
	}

	@Override
	public Bitemporal copyWith(IntervalWrapper validityInterval, boolean twoPhaseMode)  {
		return new BitemporalSeccionInciso(value, validityInterval,getContinuity(),getValueId(), twoPhaseMode);
	}

	@Override
	public SeccionInciso getValue() {
		return value;
	}

	@Override
	public void setValue(SeccionInciso value) {
		this.value = value;

	}
	
	@Override
	public SeccionInciso  getEmbedded() {
		return this.value;
	}
	
	@Override
	public void setEmbedded(SeccionInciso value) {
		this.value = value;
	}
	
	@Override
	public SeccionIncisoContinuity getContinuity() {
		return continuity;
	}

	@Override
	protected void setContinuity(SeccionIncisoContinuity continuity) {
		this.continuity = continuity;
		
	}


	@SuppressWarnings("unchecked")
	@Override
	public SeccionIncisoContinuity getEntidadContinuity() {
		return getContinuity();
	}
	
	public void setEntidadContinuity(EntidadContinuity<SeccionInciso,BitemporalSeccionInciso> entidadContinuity){
		this.continuity = (SeccionIncisoContinuity) entidadContinuity;
	}

	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("value: " + this.value + ", ");
		sb.append("continuity: " + this.continuity);
		sb.append("]");
		
		return sb.toString();
	}
}
