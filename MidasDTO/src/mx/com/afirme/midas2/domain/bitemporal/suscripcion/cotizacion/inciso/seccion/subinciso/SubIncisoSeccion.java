package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.subinciso;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class SubIncisoSeccion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9095221810099844151L;
	
	@Column(name="CLAVEAUTREASEGURO")
	private Short claveAutReaseguro;
	
	@Column(name="CODIGOUSUARIOAUTREASEGURO")
	private String codigoUsuarioAutReaseguro;
	
	@Column(name="DESCRIPCIONSUBINCISO")
	private String descripcionSubInciso;
	
	@Column(name="VALORSUMAASEGURADA")
	private Double valorSumaAsegurada;
	
	@Column(name="CLAVEESTATUSDECLARACION")
	private Short claveEstatusDeclaracion;
	
	
	
	public Short getClaveAutReaseguro() {
		return claveAutReaseguro;
	}
	public void setClaveAutReaseguro(Short claveAutReaseguro) {
		this.claveAutReaseguro = claveAutReaseguro;
	}
	

	public String getCodigoUsuarioAutReaseguro() {
		return codigoUsuarioAutReaseguro;
	}
	public void setCodigoUsuarioAutReaseguro(String codigoUsuarioAutReaseguro) {
		this.codigoUsuarioAutReaseguro = codigoUsuarioAutReaseguro;
	}
	

	public String getDescripcionSubInciso() {
		return descripcionSubInciso;
	}
	public void setDescripcionSubInciso(String descripcionSubInciso) {
		this.descripcionSubInciso = descripcionSubInciso;
	}
	

	public Double getValorSumaAsegurada() {
		return valorSumaAsegurada;
	}
	public void setValorSumaAsegurada(Double valorSumaAsegurada) {
		this.valorSumaAsegurada = valorSumaAsegurada;
	}

	
	public Short getClaveEstatusDeclaracion() {
		return claveEstatusDeclaracion;
	}
	
	public void setClaveEstatusDeclaracion(Short claveEstatusDeclaracion) {
		this.claveEstatusDeclaracion = claveEstatusDeclaracion;
	}

}
