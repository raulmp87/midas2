package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.afirme.bitemporal.annotations.BitemporalNotNull;
import com.anasoft.os.daofusion.bitemporal.BitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;
import com.anasoft.os.daofusion.bitemporal.WrappedBitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.WrappedValueAccessor;


@Entity
@Table(name = "MCONDICIONESPECIALINCISOC", schema = "MIDAS")
public class CondicionEspIncContinuity implements Serializable,EntidadContinuity<CondicionEspInciso,BitemporalCondicionEspInc> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6347509770931234313L;
	public static final String PARENT_KEY_NAME = "incisoContinuity.id";
	public static final String PARENT_BUSINESS_KEY_NAME = "incisoContinuity.numero";
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDMCONDESPINCISOC_SEQ")
	@SequenceGenerator(name = "IDMCONDESPINCISOC_SEQ", schema="MIDAS", sequenceName = "IDMCONDESPINCISOC_SEQ", allocationSize = 1)
	@Column(name = "id")
	private Long id;
	
	@OneToMany(mappedBy = "continuity", cascade = CascadeType.ALL, orphanRemoval=true)
	private Collection<BitemporalCondicionEspInc> condEspInciso = new LinkedList<BitemporalCondicionEspInc>();
	
	@Column(name="CONDICIONESPECIAL_ID")
	private Long condicionEspecialId;

	@BitemporalNotNull
	@ManyToOne
	@JoinColumn(name = "MINCISOC_ID", referencedColumnName = "id", nullable = false)
	private IncisoContinuity incisoContinuity;	
	
	
	public CondicionEspIncContinuity() {
		if(incisoContinuity == null) {
			incisoContinuity = new IncisoContinuity();
		}
	}

	public CondicionEspIncContinuity(IncisoContinuity incisoContinuity) {
		this.incisoContinuity = incisoContinuity;
	}
	
	@Override
	@Transient
	public BitemporalProperty<CondicionEspInciso, BitemporalCondicionEspInc> getBitemporalProperty() {
		return this.getCondEspInciso() ;
	}
	
	@SuppressWarnings("serial")
	public WrappedBitemporalProperty<CondicionEspInciso, BitemporalCondicionEspInc, CondicionEspIncContinuity> getCondEspInciso() {
		return new WrappedBitemporalProperty<CondicionEspInciso, BitemporalCondicionEspInc, CondicionEspIncContinuity>(
				condEspInciso,
				new WrappedValueAccessor<CondicionEspInciso, BitemporalCondicionEspInc, CondicionEspIncContinuity>() {

					@Override
					public BitemporalCondicionEspInc wrapValue(CondicionEspInciso value,IntervalWrapper validityInterval,boolean twoPhaseMode) {
						return new BitemporalCondicionEspInc(value, validityInterval,CondicionEspIncContinuity.this,null,twoPhaseMode);
					}

				});
	}	

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id ;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.id;
	}

	@SuppressWarnings("unchecked")
	@Override
	public IncisoContinuity getParentContinuity() {
		return getIncisoContinuity();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void setParentContinuity(EntidadContinuity parentContinuity) {
		setIncisoContinuity((IncisoContinuity)parentContinuity);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public IncisoContinuity getIncisoContinuity() {
		return incisoContinuity;
	}

	public void setIncisoContinuity(IncisoContinuity incisoContinuity) {
		this.incisoContinuity = incisoContinuity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CondicionEspIncContinuity other = (CondicionEspIncContinuity) obj;
		if (incisoContinuity == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


	public long getCondicionEspecialId() {
		return condicionEspecialId;
	}

	public void setCondicionEspecialId(long condicionEspecialId) {
		this.condicionEspecialId = condicionEspecialId;
	}


	@Override
	public Class<BitemporalCondicionEspInc> getBitemporalClass() {
		return BitemporalCondicionEspInc.class;
	}


}
