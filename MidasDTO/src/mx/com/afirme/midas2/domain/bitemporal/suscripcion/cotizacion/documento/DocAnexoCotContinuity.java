package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.documento;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.afirme.bitemporal.annotations.BitemporalNotNull;
import com.anasoft.os.daofusion.bitemporal.BitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;
import com.anasoft.os.daofusion.bitemporal.WrappedBitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.WrappedValueAccessor;

@Entity
@Table(name = "MDOCANEXOCOTC", schema = "MIDAS")
public class DocAnexoCotContinuity implements Serializable, EntidadContinuity<DocAnexoCot, BitemporalDocAnexoCot> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6714934836248660667L;
	
	public static final String PARENT_KEY_NAME = "cotizacionContinuity.id";
	public static final String PARENT_BUSINESS_KEY_NAME = "cotizacionContinuity.numero";
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQMDOCANEXOCOTCONTID")
	@SequenceGenerator(name = "SEQMDOCANEXOCOTCONTID", sequenceName = "MIDAS.SEQMDOCANEXOCOTCONTID", allocationSize = 1)
	@Column(name = "id")
	private Long id;

	@Column(name = "CONTROLARCHIVO_ID")
	private Integer controlArchivoId;

	@OneToMany(mappedBy = "continuity", cascade = CascadeType.ALL, orphanRemoval=true)
	private Collection<BitemporalDocAnexoCot> docAnexos = new LinkedList<BitemporalDocAnexoCot>();

	@BitemporalNotNull
	@ManyToOne
	@JoinColumn(name = "MCOTIZACIONC_ID", referencedColumnName = "id", nullable = false)
	private CotizacionContinuity cotizacionContinuity;
	
	public DocAnexoCotContinuity() {
		if(cotizacionContinuity == null) {
			cotizacionContinuity = new CotizacionContinuity();
		}
	}

	public DocAnexoCotContinuity(CotizacionContinuity cotizacionContinuity) {
		this.cotizacionContinuity = cotizacionContinuity;
	}

	@Override
	@Transient
	public BitemporalProperty<DocAnexoCot, BitemporalDocAnexoCot> getBitemporalProperty() {
		return getDocAnexos();
	}

	@SuppressWarnings("serial")
	public WrappedBitemporalProperty<DocAnexoCot, BitemporalDocAnexoCot, DocAnexoCotContinuity> getDocAnexos() {
		return new WrappedBitemporalProperty<DocAnexoCot, BitemporalDocAnexoCot, DocAnexoCotContinuity>(
				docAnexos,
				new WrappedValueAccessor<DocAnexoCot, BitemporalDocAnexoCot, DocAnexoCotContinuity>() {

					public BitemporalDocAnexoCot wrapValue(DocAnexoCot value,
							IntervalWrapper validityInterval,
							boolean twoPhaseMode) {
						return new BitemporalDocAnexoCot(value, validityInterval,
								DocAnexoCotContinuity.this,null,twoPhaseMode);
					}

				});
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	public Integer getControlArchivoId() {
		return controlArchivoId;
	}

	public void setControlArchivoId(Integer controlArchivoId) {
		this.controlArchivoId = controlArchivoId;
	}

	public CotizacionContinuity getCotizacionContinuity() {
		return cotizacionContinuity;
	}

	public void setCotizacionContinuity(
			CotizacionContinuity cotizacionContinuity) {
		this.cotizacionContinuity = cotizacionContinuity;
	}  
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocAnexoCotContinuity other = (DocAnexoCotContinuity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return this.id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return this.id;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CotizacionContinuity getParentContinuity() {
		return getCotizacionContinuity();
	}
	
	@SuppressWarnings("rawtypes")
	public void setParentContinuity(EntidadContinuity parentContinuity) {
		setCotizacionContinuity((CotizacionContinuity)parentContinuity);
	};

	@Override
	public Class<BitemporalDocAnexoCot> getBitemporalClass() {
		return BitemporalDocAnexoCot.class;
	}
}
