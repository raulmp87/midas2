package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.dato;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DatoSeccion implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 82549474145179062L;
	
	@Column(name="VALOR")
	private String valor;


	
	public String getValor() {
		return valor;
	}


	public void setValor(String valor) {
		this.valor = valor;
	}
	
	
	
	

}
