package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.anasoft.os.daofusion.bitemporal.Bitemporal;
import com.anasoft.os.daofusion.bitemporal.BitemporalWrapper;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;

@Entity
@Table(name="MCOTIZACIONB",schema="MIDAS")
@Access(AccessType.FIELD)
public class BitemporalCotizacion extends BitemporalWrapper<Cotizacion, CotizacionContinuity> implements EntidadBitemporal<Cotizacion, BitemporalCotizacion> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Embedded
	private Cotizacion value;
	@ManyToOne
	@JoinColumn(name = "MCOTIZACIONC_ID", referencedColumnName="ID")
	private CotizacionContinuity continuity;



	public BitemporalCotizacion() {
		if(continuity == null) {
			continuity = new CotizacionContinuity();
		}
		if(value == null) {
			value = new Cotizacion();
		}
	}

	public BitemporalCotizacion(Cotizacion value, IntervalWrapper validityInterval,CotizacionContinuity continuity, String valueId, boolean twoPhaseMode) {
		super(value, validityInterval,continuity,valueId, twoPhaseMode);
		
	}
	
	


	@Override
	public Cotizacion getValue() {
		return value;
	}

	@Override
	public void setValue(Cotizacion value) {
		this.value = value;
	}

	@Override
	public Bitemporal copyWith(IntervalWrapper validityInterval,boolean twoPhaseMode) {
		return new BitemporalCotizacion(value, validityInterval,getContinuity(), getValueId(), twoPhaseMode);
	}

	@Override
	public Cotizacion getEmbedded() {
		return this.value;
	}
	
	@Override
	public void setEmbedded(Cotizacion value) {
		this.value = value;
	}

	@Override

	public CotizacionContinuity getContinuity() {
		return continuity;
	}

	@Override
	public void setContinuity(CotizacionContinuity continuity) {
		this.continuity = continuity;
		
	}


	@SuppressWarnings("unchecked")
	@Override
	public CotizacionContinuity getEntidadContinuity() {
		return getContinuity();
	}

	public void setEntidadContinuity(EntidadContinuity<Cotizacion,BitemporalCotizacion> entidadContinuity){
		this.continuity = (CotizacionContinuity) entidadContinuity;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("value: " + this.value + ", ");
		sb.append("continuity: " + this.continuity);
		sb.append("]");
		
		return sb.toString();
	}

}
