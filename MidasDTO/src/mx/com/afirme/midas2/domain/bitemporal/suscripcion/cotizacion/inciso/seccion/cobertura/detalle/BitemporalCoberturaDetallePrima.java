package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.detalle;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.anasoft.os.daofusion.bitemporal.Bitemporal;
import com.anasoft.os.daofusion.bitemporal.BitemporalWrapper;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;

@Entity
@Table(name="MCOBERTURADETALLEPRIMAB",schema="MIDAS")
@Access(AccessType.FIELD)
public class BitemporalCoberturaDetallePrima extends
		BitemporalWrapper<CoberturaDetallePrima, CoberturaDetallePrimaContinuity> implements EntidadBitemporal<CoberturaDetallePrima,BitemporalCoberturaDetallePrima>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2103645176255775400L;
	@Embedded
	private CoberturaDetallePrima value;
	@ManyToOne
	@JoinColumn(name = "MCOBERTURADETALLEPRIMAC_ID", referencedColumnName="id")
	private CoberturaDetallePrimaContinuity continuity;
	

	public BitemporalCoberturaDetallePrima() {
		if(continuity == null) {
			continuity = new CoberturaDetallePrimaContinuity();
		}
		if(value == null) {
			value = new CoberturaDetallePrima();
		}
	}

	public BitemporalCoberturaDetallePrima(CoberturaDetallePrima value,
			IntervalWrapper validityInterval,CoberturaDetallePrimaContinuity continuity, String valueId, boolean twoPhaseMode) {
		super(value, validityInterval,continuity, valueId, twoPhaseMode);
	}

	@Override
	public Bitemporal copyWith(IntervalWrapper validityInterval, boolean twoPhaseMode){
		return new BitemporalCoberturaDetallePrima(value,validityInterval,getContinuity(),getValueId(), twoPhaseMode);
	}

	@Override
	public CoberturaDetallePrima getValue() {
		return this.value;
	}

	@Override
	public void setValue(CoberturaDetallePrima value) {
		this.value = value;

	}
	
	@Override
	public CoberturaDetallePrima getEmbedded() {
		return this.value;
	}
	
	@Override
	public void setEmbedded(CoberturaDetallePrima value) {
		this.value = value;
	}
	
	@Override
	public CoberturaDetallePrimaContinuity getContinuity() {
		return continuity;
	}

	@Override
	protected void setContinuity(CoberturaDetallePrimaContinuity continuity) {
		this.continuity = continuity;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public CoberturaDetallePrimaContinuity getEntidadContinuity() {
		return getContinuity();
	}
	
	public void setEntidadContinuity(EntidadContinuity<CoberturaDetallePrima,BitemporalCoberturaDetallePrima> entidadContinuity){
		this.continuity = (CoberturaDetallePrimaContinuity) entidadContinuity;
	}


}
