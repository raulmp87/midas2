package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.descuento;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.afirme.midas.catalogos.descuento.DescuentoDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.CoberturaSeccionContinuity;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.afirme.bitemporal.annotations.BitemporalNotNull;
import com.anasoft.os.daofusion.bitemporal.BitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;
import com.anasoft.os.daofusion.bitemporal.WrappedBitemporalProperty;
import com.anasoft.os.daofusion.bitemporal.WrappedValueAccessor;

@Entity
@Table(name="MCOBERTURADESCUENTOC",schema="MIDAS")
public class CoberturaDescuentoContinuity implements Serializable,
		EntidadContinuity<CoberturaDescuento, BitemporalCoberturaDescuento> {

	public CoberturaDescuentoContinuity() {
		if(coberturaSeccionContinuity == null) {
			coberturaSeccionContinuity = new CoberturaSeccionContinuity();
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 8404102570241527177L;

	public static final String PARENT_KEY_NAME = "coberturaSeccionContinuity.id";
		
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQMCOBERTURADESCUENTOCONTID")
	@SequenceGenerator(name = "SEQMCOBERTURADESCUENTOCONTID", sequenceName = "MIDAS.SEQMCOBERTURADESCUENTOCONTID", allocationSize = 1)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="DESCUENTOVARIO_ID", referencedColumnName="IDTODESCUENTOVARIO", nullable=false)
	private DescuentoDTO descuentoDTO;
	
	@OneToMany(mappedBy="continuity" ,cascade = CascadeType.ALL, orphanRemoval=true)
	private Collection<BitemporalCoberturaDescuento> coberturaDescuentos = new LinkedList<BitemporalCoberturaDescuento>();
	
	@BitemporalNotNull
	@ManyToOne
	@JoinColumn(name="MCOBERTURASECCIONC_ID", referencedColumnName="id", nullable=false)
	private CoberturaSeccionContinuity coberturaSeccionContinuity = new CoberturaSeccionContinuity();
	

	@Override
	@Transient
	public BitemporalProperty<CoberturaDescuento, BitemporalCoberturaDescuento> getBitemporalProperty() {
		return getCoberturaDescuentos();
	}
	
	@SuppressWarnings("serial")
	public WrappedBitemporalProperty<CoberturaDescuento, BitemporalCoberturaDescuento, CoberturaDescuentoContinuity> getCoberturaDescuentos() {
		return new WrappedBitemporalProperty<CoberturaDescuento, BitemporalCoberturaDescuento, CoberturaDescuentoContinuity>(
				coberturaDescuentos,
				new WrappedValueAccessor<CoberturaDescuento, BitemporalCoberturaDescuento, CoberturaDescuentoContinuity>() {

					public BitemporalCoberturaDescuento wrapValue(CoberturaDescuento value,
							IntervalWrapper validityInterval, boolean twoPhaseMode) {
						return new BitemporalCoberturaDescuento(value, validityInterval, CoberturaDescuentoContinuity.this,null,twoPhaseMode);
					}

				});
	}
	
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	
	public DescuentoDTO getDescuentoDTO() {
		return descuentoDTO;
	}
	public void setDescuentoDTO(DescuentoDTO descuentoDTO) {
		this.descuentoDTO = descuentoDTO;
	}


	public CoberturaSeccionContinuity getCoberturaSeccionContinuity() {
		return coberturaSeccionContinuity;
	}


	public void setCoberturaSeccionContinuity(
			CoberturaSeccionContinuity coberturaSeccionContinuity) {
		this.coberturaSeccionContinuity = coberturaSeccionContinuity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CoberturaDescuentoContinuity other = (CoberturaDescuentoContinuity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return getId();
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BigDecimal getBusinessKey() {
		return descuentoDTO.getIdToDescuentoVario();
	}

	@SuppressWarnings("unchecked")
	@Override
	public CoberturaSeccionContinuity getParentContinuity() {
		return getCoberturaSeccionContinuity();
	}
	
	@SuppressWarnings("rawtypes")
	public void setParentContinuity(EntidadContinuity parentContinuity) {
		setCoberturaSeccionContinuity((CoberturaSeccionContinuity)parentContinuity);
	};

	@Override
	public Class<BitemporalCoberturaDescuento> getBitemporalClass() {
		return BitemporalCoberturaDescuento.class;
	}
}
