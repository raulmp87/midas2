package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.dato;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import com.anasoft.os.daofusion.bitemporal.Bitemporal;
import com.anasoft.os.daofusion.bitemporal.BitemporalWrapper;
import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;

@Entity
@Table(name="MDATOSECCIONB",schema="MIDAS")
@Access(AccessType.FIELD)
public class BitemporalDatoSeccion extends BitemporalWrapper<DatoSeccion, DatoSeccionContinuity> implements EntidadBitemporal<DatoSeccion, BitemporalDatoSeccion> {


	/**
	 * 
	 */
	private static final long serialVersionUID = 7774858023710907468L;
	@Embedded
	private DatoSeccion value;
	@ManyToOne
	@JoinColumn(name = "MDATOSECCIONC_ID", referencedColumnName="id")
	private DatoSeccionContinuity continuity;
	
	
	public BitemporalDatoSeccion() {
		if(continuity == null) {
			continuity = new DatoSeccionContinuity();
		}
		if(value == null) {
			value = new DatoSeccion();
		}
	}

	public BitemporalDatoSeccion(DatoSeccion value,
			IntervalWrapper validityInterval, DatoSeccionContinuity continuity, String valueId, boolean twoPhaseMode) {
		super(value, validityInterval,continuity,valueId, twoPhaseMode);
	}

	@Override
	public Bitemporal copyWith(IntervalWrapper validityInterval, boolean twoPhaseMode)  {
		return new BitemporalDatoSeccion(value, validityInterval,getContinuity(),getValueId(), twoPhaseMode);
	}

	@Override
	public DatoSeccion getValue() {
		return value;
	}

	@Override
	public void setValue(DatoSeccion value) {
		this.value = value;

	}
	
	@Override
	public DatoSeccion  getEmbedded() {
		return this.value;
	}
	
	@Override
	public void setEmbedded(DatoSeccion value) {
		this.value = value;
	}
	
	@Override
	public DatoSeccionContinuity getContinuity() {
		return continuity;
	}

	@Override
	protected void setContinuity(DatoSeccionContinuity continuity) {
		this.continuity = continuity;
		
	}


	@SuppressWarnings("unchecked")
	@Override
	public DatoSeccionContinuity getEntidadContinuity() {
		return getContinuity();
	}

	public void setEntidadContinuity(
			EntidadContinuity<DatoSeccion, BitemporalDatoSeccion> entidadContinuity) {
		this.continuity = (DatoSeccionContinuity) entidadContinuity;
		
	}

}
