package mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.annotation.Exportable;
import mx.com.afirme.midas2.domain.catalogos.Paquete;

@Embeddable
public class AutoInciso implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3034052128724927769L;
	
	@Column(name="ESTILO_ID")
	private String estiloId;

	@Column(name="CLAVETIPOBIEN")
	private String claveTipoBien;

	@Column(name="ESTADO_ID")
	private String estadoId;

	@Column(name="MONEDA_ID")
	private Short idMoneda;

	@Column(name="VERSIONCARGA_ID")
	private BigDecimal idVersionCarga;

	@Column(name="MODELOVEHICULO")
	private Short modeloVehiculo;

	@Column(name="MODIFICADORESDESCRIPCION")
	private String modificadoresDescripcion;

	@Column(name="MODIFICADORESPRIMA")
	private String modificadoresPrima;

	@Column(name="MUNICIPIO_ID")
	private String municipioId;
	
	@Column(name="NUMEROMOTOR")
	private String numeroMotor;


	@Column(name="NUMEROSERIE")
	private String numeroSerie;

	@ManyToOne
	@JoinColumn(name="PAQUETE_ID", referencedColumnName="IDPAQUETE")
	private Paquete paquete;
	
	@Column(name="MARCA_ID")
	private BigDecimal marcaId;
	
	@Column(name="DESCRIPCION_FINAL")
	private String descripcionFinal;
	
	@Column(name="PLACA")
	private String placa;
	
	@Column(name="REPUVE")
	private String repuve;
	
	@Column(name="NEGOCIOSECCION_ID")
	private Long negocioSeccionId;
	
	@Column(name="NEGOCIOPAQUETE_ID")
	private Long negocioPaqueteId;
	
	@Column(name="TIPOUSO_ID")
	private Long tipoUsoId;
	
	@Column(name="NUMEROLICENCIA")
	private String numeroLicencia;
	
	@Column(name="NOMBRECONDUCTOR")
	private String nombreConductor;
	
	@Column(name="PATERNOCONDUCTOR")
	private String paternoConductor;
	
	@Column(name="MATERNOCONDUCTOR")
	private String maternoConductor;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHANACCONDUCTOR")
	private Date fechaNacConductor;
	
	@Column(name="OCUPACIONCONDUCTOR")
	private String ocupacionConductor;
	
	@Column(name="ASOCIADA_COTIZACION")
	private Integer asociadaCotizacion;
	
	@Column(name="PERSONAASEGURADO_ID")
	private Long personaAseguradoId;
	
	@Column(name="NOMBREASEGURADO")
	private String nombreAsegurado;
	
	@Column(name="OBSERVACIONESINCISO", nullable = true)
	private String observacionesinciso;
	
	@Column(name="TIPOSERVICIO_ID")
	private Long tipoServicioId;
	
	@Column(name="CLAVESIMILAR")
	private Boolean claveSimilar;
	
	@Column(name="PCTDESCUENTOESTADO")
	private Double pctDescuentoEstado;
	
	@Column(name="CLAVEAMIS")
	private String claveAmis;
	
	@Column(name="CLAVESESA")
	private String claveSesa;
	
	@Column(name="OBSERVACIONESSESA", nullable = true)
	private String observacionesSesa;
	
	@Column(name="RUTACIRCULACION", nullable = true)
	private String rutaCirculacion;	
	
	
	@Column(name = "TIPOAGRUPACIONRECIBOSINCISO")
	private String tipoAgrupacionRecibosInciso;
	
	@Column(name = "IDAGRUPADORPASAJEROS")
	private BigDecimal idAgrupadorPasajeros;
	
	@Column(name = "VINVALIDO")
	private Boolean vinValido;
	 
	@Column(name = "COINCIDEESTILO")
	private Boolean coincideEstilo;	
	
	@Column(name = "CODIGO_POSTAL")
	private Long codigoPostal;	
	
	//Atributos @Transient
	@Transient
	private String nombreEstado;
	
	@Transient
	private String nombreMunicipio;
	
	@Transient
	private String descMarca;
	
	@Transient
	private String descTipoUso;
	
	@Transient
	private String descEstilo;
	
	@Transient
	private String descLineaNegocio;
	
	@Transient
	private String nombreCompletoConductor;
	
	@Transient
	private String nombreAseguradoUpper;
	
	@Transient
	private Integer numeroInciso;
	
	@Transient
	private String idMedioPago;
	
	@Transient
	private String[] conductoCobro;
	
	@Transient
	private String conductoCobroValue;
	
	@Transient
	private String[] institucionBancaria;
	
	@Transient
	private String institucionBancariaValue;
	
	@Transient
	private String[] tipoTarjeta;
	
	@Transient
	private String tipoTarjetaValue;
	
	@Transient
	private String numeroTarjetaClave;
	
	@Transient
	private String codigoSeguridad;
	
	@Transient
	private Date fechaVencimiento;
	
		
	public String getEstiloId() {
		return estiloId;
	}

	public void setEstiloId(String estiloId) {
		this.estiloId = estiloId;
	}


	public String getClaveTipoBien() {
		return claveTipoBien;
	}

	public void setClaveTipoBien(String claveTipoBien) {
		this.claveTipoBien = claveTipoBien;
	}

	
	public String getEstadoId() {
		return estadoId;
	}

	public void setEstadoId(String estadoId) {
		this.estadoId = estadoId;
	}


	public Short getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(Short idMoneda) {
		this.idMoneda = idMoneda;
	}

	
	public BigDecimal getIdVersionCarga() {
		return idVersionCarga;
	}

	public void setIdVersionCarga(BigDecimal idVersionCarga) {
		this.idVersionCarga = idVersionCarga;
	}

	@Exportable(columnName = "MODELO", columnOrder = 2)
	public Short getModeloVehiculo() {
		return modeloVehiculo;
	}

	public void setModeloVehiculo(Short modeloVehiculo) {
		this.modeloVehiculo = modeloVehiculo;
	}

	
	public String getModificadoresDescripcion() {
		return modificadoresDescripcion;
	}

	public void setModificadoresDescripcion(String modificadoresDescripcion) {
		this.modificadoresDescripcion = modificadoresDescripcion;
	}


	public String getModificadoresPrima() {
		return modificadoresPrima;
	}

	public void setModificadoresPrima(String modificadoresPrima) {
		this.modificadoresPrima = modificadoresPrima;
	}

	
	public String getMunicipioId() {
		return municipioId;
	}

	public void setMunicipioId(String municipioId) {
		this.municipioId = municipioId;
	}

	@Exportable(columnName = "NUMERO MOTOR", columnOrder = 10)
	public String getNumeroMotor() {
		return numeroMotor;
	}

	public void setNumeroMotor(String numeroMotor) {
		this.numeroMotor = numeroMotor;
	}

	@Exportable(columnName = "NUMERO SERIE", columnOrder = 11)
	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	
	public Paquete getPaquete() {
		return paquete;
	}

	public void setPaquete(Paquete paquete) {
		this.paquete = paquete;
	}

	
	public BigDecimal getMarcaId() {
		return marcaId;
	}

	public void setMarcaId(BigDecimal marcaId) {
		this.marcaId = marcaId;
	}

	@Exportable(columnName = "DESCRIPCION", columnOrder = 1)
	public String getDescripcionFinal() {
		return descripcionFinal;
	}

	public void setDescripcionFinal(String descripcionFinal) {
		this.descripcionFinal = descripcionFinal;
	}

	@Exportable(columnName = "PLACA", columnOrder = 12)
	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	
	public String getRepuve() {
		return repuve;
	}

	public void setRepuve(String repuve) {
		this.repuve = repuve;
	}

	
	public Long getNegocioSeccionId() {
		return negocioSeccionId;
	}

	public void setNegocioSeccionId(Long negocioSeccionId) {
		this.negocioSeccionId = negocioSeccionId;
	}

	
	public Long getNegocioPaqueteId() {
		return negocioPaqueteId;
	}

	public void setNegocioPaqueteId(Long negocioPaqueteId) {
		this.negocioPaqueteId = negocioPaqueteId;
	}

	
	public Long getTipoUsoId() {
		return tipoUsoId;
	}

	public void setTipoUsoId(Long tipoUsoId) {
		this.tipoUsoId = tipoUsoId;
	}

	@Exportable(columnName = "NUMERO LICENCIA", columnOrder = 7)
	public String getNumeroLicencia() {
		return numeroLicencia;
	}

	public void setNumeroLicencia(String numeroLicencia) {
		this.numeroLicencia = numeroLicencia;
	}

	@Exportable(columnName = "NOMBRE CONDUCTOR", columnOrder = 4)
	public String getNombreConductor() {
		return nombreConductor;
	}

	public void setNombreConductor(String nombreConductor) {
		this.nombreConductor = nombreConductor;
	}

	@Exportable(columnName = "AP PATERNO CONDUCTOR", columnOrder = 5)
	public String getPaternoConductor() {
		return paternoConductor;
	}

	public void setPaternoConductor(String paternoConductor) {
		this.paternoConductor = paternoConductor;
	}

	@Exportable(columnName = "AP MATERNO CONDUCTOR", columnOrder = 6)
	public String getMaternoConductor() {
		return maternoConductor;
	}

	public void setMaternoConductor(String maternoConductor) {
		this.maternoConductor = maternoConductor;
	}

	@Exportable(columnName = "OCUPACION CONDUCTOR", columnOrder = 9)
	public String getOcupacionConductor() {
		return ocupacionConductor;
	}

	public void setOcupacionConductor(String ocupacionConductor) {
		this.ocupacionConductor = ocupacionConductor;
	}


	public Integer getAsociadaCotizacion() {
		return asociadaCotizacion;
	}

	public void setAsociadaCotizacion(Integer asociadaCotizacion) {
		this.asociadaCotizacion = asociadaCotizacion;
	}

	
	public Long getPersonaAseguradoId() {
		return personaAseguradoId;
	}

	public void setPersonaAseguradoId(Long personaAseguradoId) {
		this.personaAseguradoId = personaAseguradoId;
	}

	@Exportable(columnName = "ASEGURADO", columnOrder = 3)
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}

	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}

	@Exportable(columnName = "FECHA NAC CONDUCTOR", columnOrder = 8)
	public Date getFechaNacConductor() {
		return fechaNacConductor;
	}

	public void setFechaNacConductor(Date fechaNacConductor) {
		this.fechaNacConductor = fechaNacConductor;
	}
	
	
	public String getNombreEstado() {
		return nombreEstado;
	}
	public void setNombreEstado(String nombreEstado) {
		this.nombreEstado = nombreEstado;
	}


	public String getNombreMunicipio() {
		return nombreMunicipio;
	}
	public void setNombreMunicipio(String nombreMunicipio) {
		this.nombreMunicipio = nombreMunicipio;
	}
	
	
	public String getDescMarca() {
		return descMarca;
	}
	public void setDescMarca(String descMarca) {
		this.descMarca = descMarca;
	}


	public String getDescTipoUso() {
		return descTipoUso;
	}
	public void setDescTipoUso(String descTipoUso) {
		this.descTipoUso = descTipoUso;
	}


	public String getDescEstilo() {
		return descEstilo;
	}
	public void setDescEstilo(String descEstilo) {
		this.descEstilo = descEstilo;
	}


	public String getDescLineaNegocio() {
		return descLineaNegocio;
	}
	public void setDescLineaNegocio(String descLineaNegocio) {
		this.descLineaNegocio = descLineaNegocio;
	}
	

	public String getNombreCompletoConductor() {
		if (this.nombreConductor != null || this.paternoConductor != null || this.maternoConductor != null) {
			return this.nombreConductor + ' ' + this.paternoConductor + ' ' + this.maternoConductor;
		} else {
			return new String("");
		}
	}
	public void setNombreCompletoConductor(String nombreCompletoConductor) {
		this.nombreCompletoConductor = nombreCompletoConductor;
	}	
	
	public String getNombreAseguradoUpper() {
		nombreAseguradoUpper = this.nombreAsegurado != null ? this.nombreAsegurado.toUpperCase() : this.nombreAsegurado;
		return nombreAseguradoUpper;
	}
		
	@Exportable(columnName = "NUMERO INCISO", columnOrder = 0)
	public Integer getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	@Exportable(columnName = "OBSERVACIONES", columnOrder = 13)
	public String getObservacionesinciso() {
		return observacionesinciso;
	}

	public void setObservacionesinciso(String observacionesinciso) {
		this.observacionesinciso = observacionesinciso;
	}
	
	public String getIdMedioPago() {
		return idMedioPago;
	}

	public void setIdMedioPago(String idMedioPago) {
		this.idMedioPago = idMedioPago;
	}

	@Exportable(columnName = "CONDUCTO COBRO", columnOrder = 14, catalog=true)
	public String[] getConductoCobro() {
		return conductoCobro;
	}

	public void setConductoCobro(String[] conductoCobro) {
		this.conductoCobro = conductoCobro;
	}
	
	public String getConductoCobroValue() {
		return conductoCobroValue;
	}

	public void setConductoCobroValue(String conductoCobroValue) {
		this.conductoCobroValue = conductoCobroValue;
	}
	
	@Exportable(columnName = "INSTITUCION BANCARIA", columnOrder = 15, catalog=true)
	public String[] getInstitucionBancaria() {
		return institucionBancaria;
	}

	public void setInstitucionBancaria(String[] institucionBancaria) {
		this.institucionBancaria = institucionBancaria;
	}
	
	public String getInstitucionBancariaValue() {
		return institucionBancariaValue;
	}

	public void setInstitucionBancariaValue(String institucionBancariaValue) {
		this.institucionBancariaValue = institucionBancariaValue;
	}


	@Exportable(columnName = "TIPO DE TARJETA", columnOrder = 16, catalog=true)
	public String[] getTipoTarjeta() {
		return tipoTarjeta;
	}

	public void setTipoTarjeta(String[] tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	public String getTipoTarjetaValue() {
		return tipoTarjetaValue;
	}

	public void setTipoTarjetaValue(String tipoTarjetaValue) {
		this.tipoTarjetaValue = tipoTarjetaValue;
	}
	
	@Exportable(columnName = "NUMERO DE TARJETA O CLABE", columnOrder = 17)
	public String getNumeroTarjetaClave() {
		return numeroTarjetaClave;
	}

	public void setNumeroTarjetaClave(String numeroTarjetaClave) {
		this.numeroTarjetaClave = numeroTarjetaClave;
	}

	@Exportable(columnName = "CODIGO DE SEGURIDAD", columnOrder = 18)
	public String getCodigoSeguridad() {
		return codigoSeguridad;
	}

	public void setCodigoSeguridad(String codigoSeguridad) {
		this.codigoSeguridad = codigoSeguridad;
	}

	@Exportable(columnName = "FECHA VENCIMIENTO", columnOrder = 19, format="mm/yy")
	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public Long getTipoServicioId() {
		return tipoServicioId;
	}

	public void setTipoServicioId(Long tipoServicioId) {
		this.tipoServicioId = tipoServicioId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((asociadaCotizacion == null) ? 0 : asociadaCotizacion
						.hashCode());
		result = prime * result
				+ ((claveTipoBien == null) ? 0 : claveTipoBien.hashCode());
		result = prime * result
				+ ((descEstilo == null) ? 0 : descEstilo.hashCode());
		result = prime
				* result
				+ ((descLineaNegocio == null) ? 0 : descLineaNegocio.hashCode());
		result = prime * result
				+ ((descMarca == null) ? 0 : descMarca.hashCode());
		result = prime * result
				+ ((descTipoUso == null) ? 0 : descTipoUso.hashCode());
		result = prime
				* result
				+ ((descripcionFinal == null) ? 0 : descripcionFinal.hashCode());
		result = prime * result
				+ ((estadoId == null) ? 0 : estadoId.hashCode());
		result = prime * result
				+ ((estiloId == null) ? 0 : estiloId.hashCode());
		result = prime
				* result
				+ ((fechaNacConductor == null) ? 0 : fechaNacConductor
						.hashCode());
		result = prime * result
				+ ((idMoneda == null) ? 0 : idMoneda.hashCode());
		result = prime * result
				+ ((idVersionCarga == null) ? 0 : idVersionCarga.hashCode());
		result = prime * result + ((marcaId == null) ? 0 : marcaId.hashCode());
		result = prime
				* result
				+ ((maternoConductor == null) ? 0 : maternoConductor.hashCode());
		result = prime * result
				+ ((modeloVehiculo == null) ? 0 : modeloVehiculo.hashCode());
		result = prime
				* result
				+ ((modificadoresDescripcion == null) ? 0
						: modificadoresDescripcion.hashCode());
		result = prime
				* result
				+ ((modificadoresPrima == null) ? 0 : modificadoresPrima
						.hashCode());
		result = prime * result
				+ ((municipioId == null) ? 0 : municipioId.hashCode());
		result = prime
				* result
				+ ((negocioPaqueteId == null) ? 0 : negocioPaqueteId.hashCode());
		result = prime
				* result
				+ ((negocioSeccionId == null) ? 0 : negocioSeccionId.hashCode());
		result = prime * result
				+ ((nombreAsegurado == null) ? 0 : nombreAsegurado.hashCode());
		result = prime
				* result
				+ ((nombreCompletoConductor == null) ? 0
						: nombreCompletoConductor.hashCode());
		result = prime * result
				+ ((nombreConductor == null) ? 0 : nombreConductor.hashCode());
		result = prime * result
				+ ((nombreEstado == null) ? 0 : nombreEstado.hashCode());
		result = prime * result
				+ ((nombreMunicipio == null) ? 0 : nombreMunicipio.hashCode());
		result = prime * result
				+ ((numeroLicencia == null) ? 0 : numeroLicencia.hashCode());
		result = prime * result
				+ ((numeroMotor == null) ? 0 : numeroMotor.hashCode());
		result = prime * result
				+ ((numeroSerie == null) ? 0 : numeroSerie.hashCode());
		result = prime
				* result
				+ ((ocupacionConductor == null) ? 0 : ocupacionConductor
						.hashCode());
		result = prime * result + ((paquete == null) ? 0 : paquete.hashCode());
		result = prime
				* result
				+ ((paternoConductor == null) ? 0 : paternoConductor.hashCode());
		result = prime
				* result
				+ ((personaAseguradoId == null) ? 0 : personaAseguradoId
						.hashCode());
		result = prime * result + ((placa == null) ? 0 : placa.hashCode());
		result = prime * result + ((repuve == null) ? 0 : repuve.hashCode());
		result = prime * result
				+ ((tipoUsoId == null) ? 0 : tipoUsoId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AutoInciso))
			return false;
		AutoInciso other = (AutoInciso) obj;
		if (asociadaCotizacion == null) {
			if (other.asociadaCotizacion != null)
				return false;
		} else if (!asociadaCotizacion.equals(other.asociadaCotizacion))
			return false;
		if (descEstilo == null || descEstilo.isEmpty()) {
			if (other.descEstilo != null && !other.descEstilo.isEmpty())
				return false;
		} else if (!descEstilo.equals(other.descEstilo))
			return false;
		if (descLineaNegocio == null || descLineaNegocio.isEmpty()) {
			if (other.descLineaNegocio != null && !other.descLineaNegocio.isEmpty())
				return false;
		} else if (!descLineaNegocio.equals(other.descLineaNegocio))
			return false;
		if (descMarca == null || descMarca.isEmpty()) {
			if (other.descMarca != null && !other.descMarca.isEmpty())
				return false;
		} else if (!descMarca.equals(other.descMarca))
			return false;
		if (descTipoUso == null || descTipoUso.isEmpty()) {
			if (other.descTipoUso != null && !other.descTipoUso.isEmpty())
				return false;
		} else if (!descTipoUso.equals(other.descTipoUso))
			return false;
		if (descripcionFinal == null || descripcionFinal.isEmpty()) {
			if (other.descripcionFinal != null && !other.descripcionFinal.isEmpty())
				return false;
		} else if (!descripcionFinal.equals(other.descripcionFinal))
			return false;
		if (estadoId == null || estadoId.isEmpty()) {
			if (other.estadoId != null && !other.estadoId.isEmpty())
				return false;
		} else if (!estadoId.equals(other.estadoId))
			return false;
		if (estiloId == null || estiloId.isEmpty()) {
			if (other.estiloId != null && !other.estiloId.isEmpty())
				return false;
		} else if (!estiloId.equals(other.estiloId))
			return false;
		if (fechaNacConductor == null) {
			if (other.fechaNacConductor != null)
				return false;
		} else if (!fechaNacConductor.equals(other.fechaNacConductor))
			return false;
		if (marcaId == null) {
			if (other.marcaId != null)
				return false;
		} else if (!marcaId.equals(other.marcaId))
			return false;
		if (maternoConductor == null || maternoConductor.isEmpty()) {
			if (other.maternoConductor != null && !other.maternoConductor.isEmpty())
				return false;
		} else if (!maternoConductor.equals(other.maternoConductor))
			return false;
		if (modeloVehiculo == null) {
			if (other.modeloVehiculo != null)
				return false;
		} else if (!modeloVehiculo.equals(other.modeloVehiculo))
			return false;
		if (modificadoresDescripcion == null || modificadoresDescripcion.isEmpty()) {
			if (other.modificadoresDescripcion != null && !other.modificadoresDescripcion.isEmpty())
				return false;
		} else if (!modificadoresDescripcion
				.equals(other.modificadoresDescripcion))
			return false;
		if (modificadoresPrima == null || modificadoresPrima.isEmpty()) {
			if (other.modificadoresPrima != null && !other.modificadoresPrima.isEmpty())
				return false;
		} else if (!modificadoresPrima.equals(other.modificadoresPrima))
			return false;
		if (municipioId == null || municipioId.isEmpty()) {
			if (other.municipioId != null && !other.municipioId.isEmpty())
				return false;
		} else if (!municipioId.equals(other.municipioId))
			return false;
		if (negocioPaqueteId == null) {
			if (other.negocioPaqueteId != null)
				return false;
		} else if (!negocioPaqueteId.equals(other.negocioPaqueteId))
			return false;
		if (negocioSeccionId == null) {
			if (other.negocioSeccionId != null)
				return false;
		} else if (!negocioSeccionId.equals(other.negocioSeccionId))
			return false;
		if (nombreAsegurado == null || nombreAsegurado.isEmpty()) {
			if (other.nombreAsegurado != null && !other.nombreAsegurado.isEmpty())
				return false;
		} else if (!nombreAsegurado.equals(other.nombreAsegurado))
			return false;
		if (nombreCompletoConductor == null || nombreCompletoConductor.isEmpty()) {
			if (other.nombreCompletoConductor != null && !other.nombreCompletoConductor.isEmpty())
				return false;
		} else if (!nombreCompletoConductor
				.equals(other.nombreCompletoConductor))
			return false;
		if (nombreConductor == null || nombreConductor.isEmpty()) {
			if (other.nombreConductor != null && !other.nombreConductor.isEmpty())
				return false;
		} else if (!nombreConductor.equals(other.nombreConductor))
			return false;
		if (nombreEstado == null || nombreEstado.isEmpty()) {
			if (other.nombreEstado != null && !other.nombreEstado.isEmpty())
				return false;
		} else if (!nombreEstado.equals(other.nombreEstado))
			return false;
		if (nombreMunicipio == null || nombreMunicipio.isEmpty()) {
			if (other.nombreMunicipio != null && !other.nombreMunicipio.isEmpty())
				return false;
		} else if (!nombreMunicipio.equals(other.nombreMunicipio))
			return false;
		if (numeroLicencia == null || numeroLicencia.isEmpty()) {
			if (other.numeroLicencia != null && !other.numeroLicencia.isEmpty())
				return false;
		} else if (!numeroLicencia.equals(other.numeroLicencia))
			return false;
		if (numeroMotor == null || numeroMotor.isEmpty()) {
			if (other.numeroMotor != null && !other.numeroMotor.isEmpty())
				return false;
		} else if (!numeroMotor.equals(other.numeroMotor))
			return false;
		if (numeroSerie == null || numeroSerie.isEmpty()) {
			if (other.numeroSerie != null && !other.numeroSerie.isEmpty())
				return false;
		} else if (!numeroSerie.equals(other.numeroSerie))
			return false;
		if (ocupacionConductor == null || ocupacionConductor.isEmpty()) {
			if (other.ocupacionConductor != null && !other.ocupacionConductor.isEmpty())
				return false;
		} else if (!ocupacionConductor.equals(other.ocupacionConductor))
			return false;
		if (paquete == null || paquete.getId() == null) {
			if (other.paquete != null && other.paquete.getId() != null)
				return false;
		} else if (!paquete.equals(other.paquete))
			return false;
		if (paternoConductor == null || paternoConductor.isEmpty()) {
			if (other.paternoConductor != null && !other.paternoConductor.isEmpty())
				return false;
		} else if (!paternoConductor.equals(other.paternoConductor))
			return false;
		if (personaAseguradoId == null) {
			if (other.personaAseguradoId != null)
				return false;
		} else if (!personaAseguradoId.equals(other.personaAseguradoId))
			return false;
		if (placa == null || placa.isEmpty()) {
			if (other.placa != null && !other.placa.isEmpty())
				return false;
		} else if (!placa.equals(other.placa))
			return false;
		if (repuve == null || repuve.isEmpty()) {
			if (other.repuve != null && !other.repuve.isEmpty())
				return false;
		} else if (!repuve.equals(other.repuve))
			return false;
		if (tipoUsoId == null) {
			if (other.tipoUsoId != null)
				return false;
		} else if (!tipoUsoId.equals(other.tipoUsoId))
			return false;
		if (pctDescuentoEstado == null) {
			if (other.pctDescuentoEstado != null)
				return false;
		} else if (!pctDescuentoEstado.equals(other.pctDescuentoEstado))
			return false;
		if (rutaCirculacion == null || rutaCirculacion.isEmpty()) {
			if (other.rutaCirculacion != null && !other.rutaCirculacion.isEmpty())
				return false;
		} else if (!rutaCirculacion.equals(other.rutaCirculacion))
			return false;
		if (codigoPostal == null) {
			if (other.codigoPostal != null)
				return false;
		} else if (!codigoPostal.equals(other.codigoPostal))
			return false;
		
		return true;
	}	
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("placa: " + this.placa + ", ");
		sb.append("nombreAsegurado: " + this.nombreAsegurado);
		sb.append("]");
		
		return sb.toString();
	}

	public void setClaveSimilar(Boolean claveSimilar) {
		this.claveSimilar = claveSimilar;
	}

	public Boolean getClaveSimilar() {
		return claveSimilar;
	}

	public Double getPctDescuentoEstado() {
		if(pctDescuentoEstado == null) {
			pctDescuentoEstado = 0.0;
		}
		return pctDescuentoEstado;
	}

	public void setPctDescuentoEstado(Double pctDescuentoEstado) {
		this.pctDescuentoEstado = pctDescuentoEstado;
	}

	public String getClaveAmis() {
		return claveAmis;
	}

	public void setClaveAmis(String claveAmis) {
		this.claveAmis = claveAmis;
	}

	public String getClaveSesa() {
		return claveSesa;
	}

	public void setClaveSesa(String claveSesa) {
		this.claveSesa = claveSesa;
	}

	public String getObservacionesSesa() {
		return observacionesSesa;
	}

	public void setObservacionesSesa(String observacionesSesa) {
		this.observacionesSesa = observacionesSesa;
	}

	public void setRutaCirculacion(String rutaCirculacion) {
		this.rutaCirculacion = rutaCirculacion;
	}

	public String getRutaCirculacion() {
		return rutaCirculacion;
	}

	public String getTipoAgrupacionRecibosInciso() {
		return tipoAgrupacionRecibosInciso;
	}

	public void setTipoAgrupacionRecibosInciso(String tipoAgrupacionRecibosInciso) {
		this.tipoAgrupacionRecibosInciso = tipoAgrupacionRecibosInciso;
	}

	public BigDecimal getIdAgrupadorPasajeros() {
		return idAgrupadorPasajeros;
	}

	public void setIdAgrupadorPasajeros(BigDecimal idAgrupadorPasajeros) {
		this.idAgrupadorPasajeros = idAgrupadorPasajeros;
	}

	public Boolean getVinValido() {
		return vinValido;
	}

	public void setVinValido(Boolean vinValido) {
		this.vinValido = vinValido;
	}

	public Boolean getCoincideEstilo() {
		return coincideEstilo;
	}

	public void setCoincideEstilo(Boolean coincideEstilo) {
		this.coincideEstilo = coincideEstilo;
	}

	public Long getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(Long codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
}
