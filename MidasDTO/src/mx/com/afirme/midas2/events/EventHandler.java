package mx.com.afirme.midas2.events;

import mx.com.afirme.midas2.domain.sistema.Evento;

public interface EventHandler {

	public void execute(Evento event);
	
}
