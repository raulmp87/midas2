package mx.com.afirme.midas2.service.agtSaldoMovto;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.agtSaldoMovto.MovimientosDelSaldo;
import mx.com.afirme.midas2.dto.agtSaldoMovto.MovimientosDelSaldoView;
import mx.com.afirme.midas2.util.MidasException;
@Local
public interface MovimientosDelSaldoService {
	public  List<MovimientosDelSaldo> findByFilters(MovimientosDelSaldo filter) throws MidasException;
	
	public  List<MovimientosDelSaldoView> findByFiltersView(MovimientosDelSaldo filter) throws MidasException;
}
