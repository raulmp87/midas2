package mx.com.afirme.midas2.service.job.lee.ibs.service;

import java.io.Serializable;

import javax.ejb.Local;

/**
 * <p>Clase que contiene definida la logica para 
 * realizar la lectura del archivo de <b>PAGOPROG.txt</b></p> 
 * 
 * @author SEGUROS AFIRME
 * 
 * @since 09022017
 * 
 * @version 1.0
 * 
 */
@Local
public interface LeePagoProgIBSService extends Serializable{
	
	/**
	 * Metodo que inicializa el JOB que realiza 
	 * la lectura del archivo de IBS.
	 */
	public void init();
	
	/**
	 * Metodo que realiza la ejecucion del JOB que
	 * realiza la lectura de IBS.
	 */
	public void jobLeePagoProgIBS();
	
	/**
	 * Metodo que obtiene el archivo del servidor SFTP
	 * para realizar la lectura y posterior guardado de la informacion
	 */
	public void cargaInformacionIBS();
	
	/**
	 * Metodo que realiza la actualizaci\u00f3n
	 * de la informacion de IBS.
	 */
	public void complementarInformacionIBS();
	
	/**
	 * Proceso que se dedica a migrar la informacion de IBS que no se ha procesado.
	 */
	public void migrarInformacionIBS();
}

