/**
 * <p>Paquete que contendr&uacute; la implementaci&oacute;n de los procesos
 * de oracle para el cliente de banca de <b>SELCOR</b></p>
 * 
 * <p>La funci&oacute;n principal de estos paquetes es la de
 * generar un archivo de texto en un servidor de tipo <b>SFTP<b> 
 * el cual define infraestructura, los <b>PATH</b> de la hubicaci&oacute;n
 * de estos archivos ser&aacute; respetada con forme se encontraba definido en los 
 * procesos de <b>ORACLE</b></p>
 */
/**
 * @author SEGUROS AFIRME
 * 
 * @version 1.0
 * 
 * @since 07022017
 * 
 * @category JOB-CRON
 *
 */
package mx.com.afirme.midas2.service.job.escribe.archivo;