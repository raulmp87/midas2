package mx.com.afirme.midas2.service.job.escribe.archivo;

import javax.ejb.Local;

/**
 * <p>Interfaz que contiene definida la l&oacute;gica
 * para realizar la escritura de archivos de texto dentro
 * del servidor <b>SFTP</b> para los procesos que se migraron de 
 * <b>ORACLE.</b></p>
 * 
 * <p>
 * En esta definici&oacute;n se controlan las funciones necesarias y correspondientes a los procesos
 * de escritura de los archivos de: 
 * 		<ul>
 * 			<li>
 * 				<b>ESCRIBE_SELCOR</b>
 * 			</li>
 * 			<li>
 * 				<b>ESCRIBE_CYBER</b>
 * 			</li>
 * 		</ul>
 * </p>
 * 
 * @author SEGROS AFIRME
 * 
 * @since 07032017
 * 
 * @category JOB-CRON
 * 
 * @version 1.0
 */
@Local
public interface EscribeArchivoTextoEnSFTPJobService {
	
	/**
	 * <p>M&eacute;todo que inicializa el cron para realizar la 
	 * escritura del archivo que corresponde a el cliente de banca
	 * de <b>SELCOR</b></p>
	 */
	public void initEscribeSelcor();
	
	/**
	 * <p>M&eacute;todo que inicializa el cron para realizar la escritura
	 * del archivo de texto de CYBER</p>
	 */
	public void initEscribeCyber();
	
	/**
	 * <p>M&eacute;todo que inicializa el proceso que genera el 
	 * archivo de texto de <b>WinstonData</b>.</p>
	 */
	public void initWinstonData();
	
	/**
	 * <p>M&eacute;todo que inicializa la ejecuci&oacute;n
	 * para obtener la informaci&oacute;n para poder escribir un
	 * archivo de texto</p>
	 * 
	 * <p>Para su recepci&oacute;n en banca</p>
	 */
	public void initEscribeCajero();
	
	/**
	 * <p>M&eacute;todo que obtiene la informaci&oacute;n de W.D. de una p&oacute;liza de autos</p>
	 */
	public void initWinstonDataAutos(Long idCotizacion);
	
	/**
	 * <p>M&eacute;todo que obtiene la informaci&oacute;n de W.D. de una p&oacute;liza de vida.</p>
	 */
	public void initWinstonDataVida(Long idCotizacion);
	
	/**
	 * <p>M&eacute;todo que obtiene la informaci&oacute;n de W.D. de una p&oacute;liza de AUTOS.</p>
	 */
	public void initWinstonDataAutosDir(Long idCotizacion);
	
	/**
	 * <p>M&eacute;todo que realiza la exportaci&oacute;n de la informaci&oacute;n 
	 * de WD</p>
	 */
	public void initWinstonDataPrimasDep();
	
	/**
	 * <p>M&eacute;todo que realiza la escritura del archivo de texto que
	 * se genera al terminar las cancelaciones automaticas.</p>
	 */
	public void initResultCancel();
	
	/**
	 * <p>M&eacute;todo que realiza la escritura del archivo IDCLIENTE.txt</p>
	 */
	public void initEscribeIDCliente();
	
	/**
	 * <p>M&eacute;todo que realiza la lectura del archivo RESPE
	 * y con esta informaci&oacute;n actualiza la informacion de la tabla
	 * de <b>SEYCOS.PERSONA_EXT</b></p>
	 */
	public void initLeeCliente();
}
