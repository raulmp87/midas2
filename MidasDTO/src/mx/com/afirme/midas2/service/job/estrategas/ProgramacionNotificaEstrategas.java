package mx.com.afirme.midas2.service.job.estrategas;

import javax.ejb.Local;

/**
 * Clase que realizar\u00e1 la programacion
 * del JOB de notificacion estrategas v\u00eda correo
 * electr\u00f3nico.
 * 
 * @author SEGUROS AFIRME
 * 
 * @version 1.0
 * 
 * @since 20161104
 */
@Local
public interface ProgramacionNotificaEstrategas {
	
	/**
	 * M\u00e9todo que realiza la notificaci\u00f3n a los
	 *  estrategas.
	 */
	public void jobNotificaEstrategas();
	
}
