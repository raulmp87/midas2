package mx.com.afirme.midas2.service.movil.cotizador.vida;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;

import mx.com.afirme.vida.domain.movil.cotizador.QuotationParametersDTO;
import mx.com.afirme.vida.domain.movil.cotizador.ReportServiceDTO;

/**
 *
 */

public interface ImpresionCotizacionMovilVidaService {

	public byte[] getLifeInsuranceApplication(String identifier,
		      String contentTypeReport);
	public ReportServiceDTO getLifeInsuranceApplication(
		      ReportServiceDTO reportDTO);
	public QuotationParametersDTO buscarCotizacionImpresion(Integer quotationId);
	public Object getLifeInsuranceApplicationReport(ReportServiceDTO reportDTO,
		      QuotationParametersDTO quotationDTO );
	public JasperReport getJasperReport(String internalURL)
			throws JRException;

}