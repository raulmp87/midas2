package mx.com.afirme.midas2.service.movil.cotizador.enlaceafirme;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.agentes.VehiculoView;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.domain.tarifa.TarifaServicioPublico;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.EsquemaPagoCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.ControlDinamicoRiesgoDTO;
import mx.com.afirme.midas2.exeption.ApplicationException;


public interface CotizacionMovilEnlaceService {
	
	List<IncisoCotizacionDTO> getListCotizacionSeguroObligatorio(
			String id, String isPoliza) throws ApplicationException;
	
	Map<String, Object> getDatosInicialesCSO( String id ) throws ApplicationException;

	BigDecimal generarCotizacionServicioPublico(CotizacionDTO cotizacion, BigDecimal idAgrupadorPasajeros,
			String claveTarifa, String idEstado) throws ApplicationException;
	
	Map<String,Object> getDatosCobranza(String idToCotizacion, Integer idMedioPago)
		throws ApplicationException;
	
	Map<String,Object> getDatosConducto(String idToCotizacion, BigDecimal idToPersonaContratante, String idConductoCobro)
		throws ApplicationException;
	
	Map<String,Object> getDatosTC(CuentaPagoDTO cuentaPagoDTO, Integer idMedioPago)throws ApplicationException;
	
	Map<String,Object> guardarCobranza(String idToCotizacion, Integer idMedioPago, String tipoTarjeta, Integer idBanco,
			CuentaPagoDTO cuentaPagoDTO)throws ApplicationException;
	
	Map<String, Object> getCatalogo(String idCatalogo, String id);
	
	Map<String, Object> getResumenCosto(String id, String isPoliza, BigDecimal numeroInciso)throws ApplicationException;
	
	Map<BigDecimal, String> getListMarcasByNegSeccion(BigDecimal idNegocioSeccion) throws ApplicationException;
	
	Map<Short, Short> getListModelosByMarca(BigDecimal idNegocioSeccion, BigDecimal idMarca) throws ApplicationException;
	
	MensajeDTO validarListaParaEmitir(String idToCotizacion)throws ApplicationException;
	
	TransporteImpresionDTO getPDFSeguroObligatorio(String id, String isPoliza, Locale locale, String isMovil, String email)throws ApplicationException;
	
	Map<String, Object> saveCotizacion(IncisoCotizacionDTO incisoCotizacion, String forma) throws ApplicationException;
	
	MensajeDTO envioPDFEmail(String id, Locale locale, String isPoliza, String email)throws ApplicationException;
	
	List<ClienteDTO> buscarCliente(ClienteDTO cliente) throws ApplicationException;
	
	Map<String, Object> getTarifasServicioPublico(TarifaServicioPublico tarifaServicioPublico) throws ApplicationException;
	
	BigDecimal guardarCliente(ClienteGenericoDTO cliente) throws ApplicationException;
	
	Map<String, Object> emitir(String id,String email) throws ApplicationException;
	
	ClienteDTO getClienteById(BigDecimal idCliente, String userName) throws ApplicationException;
	
	Map<String, Object> getDatosCotizacion(CotizacionDTO cotizacion) throws ApplicationException;
	
	Map<String, Object> getIncisoAutoCotByIdCotizacion(String id) throws ApplicationException;
	
	String generaPlantillaPoliza(String id, String isPoliza, Locale locale, String email) throws ApplicationException;
	
	Map<String, String> getEstiloPorMarcaNegocioyDescripcion(String idToCotizacion, IncisoAutoCot incisoAutoCot) 
	throws ApplicationException;
	
	Map<String,Object> getConductosCobroCotizacion(String idToCotizacion) throws ApplicationException;
	
	public Map<String, Object> getInicialesCotizacion( String idToNegocio, String idToCotizacion) throws ApplicationException;
	
	Map<String, Object> getDatosLineaNegocio( String lineaNegocio ) throws ApplicationException;
	
	Map<String, Object> showOtherCharacteristics(String estiloSeleccionado, String modificadoresDescripcion) throws ApplicationException;
	
	Map<String, Object> getResumen(CotizacionDTO cotizacion) throws ApplicationException;
	
	List<ControlDinamicoRiesgoDTO> getRiskData( BigDecimal idCotizacion, BigDecimal numInciso, String name, Boolean required, Map<String, String> values ) throws ApplicationException;
	
	List<EsquemaPagoCotizacionDTO> getPaymentScheme( BigDecimal idCotizacion ) throws ApplicationException;
	
	Map<Long, String> getListPaquetesByNegSeccion(BigDecimal idNegocioSeccion) throws ApplicationException;
}
