package mx.com.afirme.midas2.service.movil.cliente;

import java.util.List;
import javax.ejb.Local;

import mx.com.afirme.midas2.domain.movil.cliente.NotificacionClienteMovil;

/**
 * Local interface for TrusrnotificacionFacade.
 * 
 * @author MyEclipse Persistence Tools
 */
@Local
public interface NotificacionClienteMovilService {
	/**
	 * Perform an initial save of a previously unsaved Trusrnotificacion entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            Trusrnotificacion entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(NotificacionClienteMovil entity);

	/**
	 * Delete a persistent Trusrnotificacion entity.
	 * 
	 * @param entity
	 *            Trusrnotificacion entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(NotificacionClienteMovil entity);

	/**
	 * Persist a previously saved Trusrnotificacion entity and return it or a
	 * copy of it to the sender. A copy of the Trusrnotificacion entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            Trusrnotificacion entity to update
	 * @return Trusrnotificacion the persisted Trusrnotificacion entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public NotificacionClienteMovil update(NotificacionClienteMovil entity);

	public NotificacionClienteMovil findById(Long id);

	/**
	 * Find all Trusrnotificacion entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the Trusrnotificacion property to query
	 * @param value
	 *            the property value to match
	 * @return List<Trusrnotificacion> found by query
	 */
	public List<NotificacionClienteMovil> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all Trusrnotificacion entities.
	 * 
	 * @return List<Trusrnotificacion> all Trusrnotificacion entities
	 */
	public List<NotificacionClienteMovil> findAll();
	
	public void notificarArriboAjustador(Long idReporteSiniestro);
	public void notificarComienzoServicioSiniestro(Long idReporteSiniestro);
	public void notificarTerminoServicioSiniestro(Long idReporteSiniestro); 
}