package mx.com.afirme.midas2.service.movil.cliente;

import java.util.List;
import javax.ejb.Local;

import mx.com.afirme.midas2.domain.movil.cliente.PolizaDescargaMovil;

/**
 * Local interface for TrusrpolizadescargaFacade.
 * 
 * @author MyEclipse Persistence Tools
 */
@Local
public interface PolizaDescargaMovilService {
	/**
	 * Perform an initial save of a previously unsaved Trusrpolizadescarga
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            Trusrpolizadescarga entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(PolizaDescargaMovil entity);

	/**
	 * Delete a persistent Trusrpolizadescarga entity.
	 * 
	 * @param entity
	 *            Trusrpolizadescarga entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(PolizaDescargaMovil entity);

	/**
	 * Persist a previously saved Trusrpolizadescarga entity and return it or a
	 * copy of it to the sender. A copy of the Trusrpolizadescarga entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            Trusrpolizadescarga entity to update
	 * @return Trusrpolizadescarga the persisted Trusrpolizadescarga entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public PolizaDescargaMovil update(PolizaDescargaMovil entity);

	public PolizaDescargaMovil findById(Long id);

	/**
	 * Find all Trusrpolizadescarga entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the Trusrpolizadescarga property to query
	 * @param value
	 *            the property value to match
	 * @return List<Trusrpolizadescarga> found by query
	 */
	public List<PolizaDescargaMovil> findByProperty(String propertyName,
			Object value);

	/**
	 * Find all Trusrpolizadescarga entities.
	 * 
	 * @return List<Trusrpolizadescarga> all Trusrpolizadescarga entities
	 */
	public List<PolizaDescargaMovil> findAll();
}