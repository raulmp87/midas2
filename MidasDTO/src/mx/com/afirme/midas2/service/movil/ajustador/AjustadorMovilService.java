package mx.com.afirme.midas2.service.movil.ajustador;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.domain.movil.ajustador.Ajustador;
import mx.com.afirme.midas2.domain.movil.ajustador.AjustadorMovil;
import mx.com.afirme.midas2.domain.movil.ajustador.Catalogo;
import mx.com.afirme.midas2.domain.movil.ajustador.Cobertura;
import mx.com.afirme.midas2.domain.movil.ajustador.CoberturaIncisoParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.DireccionParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.FindPolizaParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.FindPolizaResponse;
import mx.com.afirme.midas2.domain.movil.ajustador.IncisoParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.MarcarContactoParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.MarcarTerminacionParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.Poliza;
import mx.com.afirme.midas2.domain.movil.ajustador.ProcedeSiniestro;
import mx.com.afirme.midas2.domain.movil.ajustador.ReporteSiniestro;
import mx.com.afirme.midas2.domain.movil.ajustador.ReporteSiniestroParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.ReporteSiniestrosAsignados;
import mx.com.afirme.midas2.domain.movil.ajustador.SaveDocumentoSiniestroParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.Tercero;
import mx.com.afirme.midas2.domain.movil.ajustador.TerceroParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.TipoDocumentoSiniestro;
import mx.com.afirme.midas2.exeption.ApplicationException;

public interface AjustadorMovilService {
	
	
	/**
	 * Obtiene todos los tipos de documentos que puede tener un siniestro.
	 * @return
	 */
	public List<TipoDocumentoSiniestro> getTiposDocumentosSiniestro();
	
	public ReporteSiniestro getReporteSiniestro(Long id);
	
	/**
	 * A diferencia de getReporteSiniestro se valida que efectivamente el ajustador tenga asignado el <code>ReporteSiniestro</code>
	 * y se confirma de enterado la asignacion.
	 * @param id el id del reporte
	 * @param tipo null or Id cuando es el id del reporte, N cuando es numero de reporte
	 * @return
	 */
	public ReporteSiniestro getReporteSiniestroAsignado(Long id, String tipo);
		
	/**
	 * Asigna la poliza al reporte
	 * @param parameter
	 * @param usuario
	 */
	public void asignarInciso(IncisoParameter parameter, String usuario);
	
	public void enviarNotificacion(IncisoParameter parameter, String usuario);
	
	public void marcarContacto(MarcarContactoParameter parameter);
		
	public void marcarTerminacion(MarcarTerminacionParameter parameter);
	
	public Poliza getPoliza(String id, Date fechaOcurrido);
	
	public List<FindPolizaResponse> findPoliza(FindPolizaParameter parameter);
	
	public ReporteSiniestrosAsignados getReporteSiniestrosAsignados(String ajustadorId);
	
	/**
	 * Obtiene los reportes de siniestros asignados de un ajustador que no se han confirmado de enterado.
	 * @param ajustadorId
	 * @return
	 */
	public List<ReporteSiniestro> getReporteSiniestrosAsignadosSinConfirmarEnterado(String ajustadorId);
	
	public Ajustador getAjustador(Long id);
	
	public Ajustador getAjustador(Usuario usuario);
	
	public ProcedeSiniestro getProcedeSiniestro(String id, Date fechaOcurrido);
	
	/**
	 * Obtiene el <code>AjustadorMovil</code> utilizando el <code>idSeycos</code>.
	 * @param idSeycos
	 * @return
	 */
	public AjustadorMovil getAjustadorMovil(Long idSeycos);
	
	/**
	 * Obtiene todos los ajustadores moviles que utilizan la aplicación.
	 * @return
	 */
	public List<AjustadorMovil> getAjustadoresMoviles();
	
	public List<String> getRegistrationIds(Long idAjustadorSeycos);
	
	/**
	 * Confirmar que el ajustador está enterado de la asignación.
	 * @param idReporteSiniestro
	 * @return fechaEnteradoAsignacion fecha que registro el sistema de enterado.
	 * @throws ApplicationException si el ajustador no tiene el reporte asignado.
	 */
	public Date confirmarEnteradoAsignacion(Long idReporteSiniestro);
		
	/**
	 * Obtiene el catalogo de tipo de siniestro (causa del siniestro), aplicable de acuerdo al tipo de inciso
	 * @param tipoInciso MOTOS o AUTOS o CAMN etc...
	 * @return catalogo de tipos de siniestro validos
	 * @author ncorrea
	 */
	public List<Catalogo> getTiposSiniestro(String tipoInciso);
	
	/**
	 * Obtiene el catalogo de terminos de ajuste aplicable al tipo de siniestro indicado
	 * @param tipoSiniestro
	 * @param responsabilidad
	 * @return catalogo de terminos de ajuste validos
	 * @author ncorrea
	 */
	public List<Catalogo> getTerminosAjuste(String tipoSiniestro, String responsabilidad);
	
	/**
	 * Obtiene el catalogo de terminos de ajuste aplicable al tipo de siniestro indicado
	 * @param tipoSiniestro
	 * @param terminoAjuste
	 * @return catalogo de terminos de ajuste validos
	 * @author ncorrea
	 */
	public List<Catalogo> getResponsabilidad(String tipoSiniestro, String terminoAjuste);
	
	/**
	 * Guarda la informacion general del reporte
	 * @param parameter
	 */
	public void guardaInfoGralReporte(ReporteSiniestroParameter parameter);
	
	/**
	 * De acuerdo a los datos del reporte, obtiene el listado de coberturas, indicando cuales son afectables
	 * @param reporteSiniestroId
	 * @param id
	 * @param fechaOcurrido
	 * @return listado coberturas del reporte
	 */
	public List<Cobertura> getCoberturasAfecta(IncisoParameter parameter);
	
	/**
	 * Consulta el listado de terceros de una cobertura
	 * @param parameter
	 * @return
	 */
	public List<Tercero> getTerceros(CoberturaIncisoParameter parameter);
	
	/**
	 * Consulta la informacion de un tercero
	 * @param parameter
	 * @return
	 */
	public Object getTercero(TerceroParameter parameter);
	
	/**
	 * Lista de Tipos de Pases Medicos que aplican por cobertura
	 * @param parameter
	 * @return
	 */
	public List<Catalogo> getTiposPases(CoberturaIncisoParameter parameter);
	
	/**
	 * Guarda Estimacion de Reserva
	 * @param tercero
	 * @param claveUsuario
	 */
	public BigDecimal afectaReserva(Object tercero, String claveUsuario);

	/**
	 * Catalogos Simples 
	 * @param tipo
	 * @return
	 */
	public List<Catalogo> getCatalogoSimple(String tipo);
	
	/**
	 * Municipios
	 * @param idEstado
	 * @return
	 */
	public List<Catalogo> getMunicipios(DireccionParameter parameter);
	
	/**
	 * Informacion en Base al CP
	 * @param idEstado
	 * @param idMunicipio
	 * @return
	 */
	public List<Catalogo> getInfoCP(DireccionParameter parameter);
	
	public byte[] getBytesPase(TerceroParameter tercero, String tipoPase, String usuario);	
	
	public void saveDocumentoSiniestro(SaveDocumentoSiniestroParameter parameter);
	
	public String convertirReclamacion(ReporteSiniestroParameter reporteSiniestro, String claveUsuario);
	
}
