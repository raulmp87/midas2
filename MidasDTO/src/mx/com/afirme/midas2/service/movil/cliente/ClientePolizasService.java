package mx.com.afirme.midas2.service.movil.cliente;

import java.util.List;

import mx.com.afirme.midas.poliza.NumeroPolizaCompleto;
import mx.com.afirme.midas2.domain.movil.MensajeValidacion;
import mx.com.afirme.midas2.domain.movil.ajustador.Recibo;
import mx.com.afirme.midas2.domain.movil.cliente.ClientePolizas;
import mx.com.afirme.midas2.domain.movil.cliente.CorreosCosultaMovil;
import mx.com.afirme.midas2.domain.movil.cliente.DatosPolizaSeycos;
import mx.com.afirme.midas2.domain.movil.cliente.EnvioCaratulaParameter;
import mx.com.afirme.midas2.domain.movil.cliente.InfoDescargaPoliza;
import mx.com.afirme.midas2.domain.movil.cliente.InfoPolizaParameter;
import mx.com.afirme.midas2.domain.movil.cliente.NotificacionCliente;
import mx.com.afirme.midas2.domain.movil.cliente.PolizasClienteMovil;
import mx.com.afirme.midas2.domain.movil.cotizador.ValidacionEstatusUsuarioActual;
import mx.com.afirme.midas2.domain.siniestro.TipoSiniestroMovil;
import mx.com.afirme.midas2.dto.fuerzaventa.CentroOperacionView;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;

public interface ClientePolizasService {
	/**
	 * Metodo para buscar polizas relacionadas
	 * 
	 * @param tipo
	 *            tipo de poliza(A=Auto, C=Casa, P=PYME, V=Vida)
	 * @param usuario
	 *            usuario logueado
	 * @return
	 **/
	public List<ClientePolizas> buscarPolizas(InfoPolizaParameter param);

	/**
	 * Guarda Informaci[on en BD
	 * 
	 * @param params
	 *            lista de parametros a asignar
	 **/
	public void guardarPoliza(InfoPolizaParameter params);

	/**
	 * Verifica si existe una poliza en el sistema central MIDAS/SEYCOS
	 * 
	 * @param poliza
	 *            el numero de poliza a verificar
	 * @return
	 **/
	public boolean existePoliza(String polizaNo);
	
	/**
	 * Actualiza los datos almacenados
	 * 
	 * @param params
	 *            datos a reemplazar
	 */
	public MensajeValidacion actualizarPolizasClienteMovil(InfoPolizaParameter param);

	/**
	 * Metodo para el envio de caratula de la poliza Se debe notificar al agente
	 */
	public void enviarCaratula(EnvioCaratulaParameter params);
	
	/**
	 * Metodo para buscar y mostrar los recios que estan relacionados a una
	 * poliza
	 * 
	 * @param id
	 *            el ID de la poliza a consultar
	 * @return
	 */
	public List<Recibo> mostrarRecibos(String polizaID);

	/**
	 * Metodo para eliminar la poliza relacionada al cliente
	 * 
	 * @param id
	 *            ID de la poliza a eliminar
	 */
	public void eliminarPoliza(String id);
	
	/**
	 * Obtiene la Informacion de una poliza especifica
	 * @param id
	 * @return
	 */
	public ClientePolizas infoPoliza(String polizaID);
	
	/**
	 * Método para bloquear al usurio en la sección de consulta
	 * 
	 * @param username
	 *            el nombre del usuario a bloquear
	 */
	public MensajeValidacion bloqueaUsuario(String username);
	
	/**
	 * Método para desbloquear al usurio en la sección de consulta
	 * 
	 * @param username
	 *            el nombre del usuario a desbloquear
	 */
	public MensajeValidacion desbloqueaUsuario(String username);
	
	public void solicitudDesbloqueo(EnvioCaratulaParameter params);
	
	/**
	 * Método para revisar si existe el registro
	 * @param params
	 */
	public boolean existeRegistro(InfoPolizaParameter params);
	
	/**
	 * Método para el envio de recibo XML/PDF
	 * @param params
	 */
	public void envioRecibos(EnvioCaratulaParameter params);
	
	
	/**
	 * Método para el envio de recibo XML/PDF
	 * @param params
	 */
	public void envioPDFyXML(EnvioCaratulaParameter params);
	
	/**
	 * Metodo para obtener los correos para las notificaciones
	 * @return
	 */
	public List<CorreosCosultaMovil> getCorreosNotificacion();
	
	/**
	 * Metodo para guardar los correos a los que se le envia notificacion de
	 * desbloqueo
	 * 
	 * @param pCorreo
	 *            el correo a registrar
	 */
	public void guardarCorreo(String pCorreo, String nombre, String gerenciaID);
	
	/**
	 * Metodo para eliminar correos de notificacion
	 * @param id
	 */
	public void eliminarCorreo(String id);
	
	/**
	 * Obtiene el detalle de los registros de descargas de caratula realizadas
	 * 
	 * @return
	 */
	public List<InfoDescargaPoliza> getRegistroDescargas();
	
	/**
	 * Metodo para obtener el detalle por usuario de las descargas de caratulas
	 * 
	 * @param pUsuario
	 *            el usuario a consultar
	 * @return
	 */
	public List<InfoDescargaPoliza> getDetalleDescargas(String pUsuario);
	
	/**
	 * Metodo para la obtencion de las gerencias de la organizacion
	 * @return
	 */
	public List<CentroOperacionView> getLstGerencias();
	
	/**
	 * Metodos para obtener la informacion para las notificaciones
	 * @return
	 */
	
	//Recibos
	public List<NotificacionCliente> getReciboVencido(String tipoNotificacion);		
	public List<NotificacionCliente> getReciboPagado(String tipoNotificacion);
	public List<NotificacionCliente> getReciboVencimiento(String tipoNotificacion);
	//polizas
	public List<NotificacionCliente> getCancelacionPoliza(String tipoNotificacion);
	
	public List<NotificacionCliente> getNotification();
	
	public  String getTipoPoliza(String strNumeroPolizaCompleto) ;


	public void save(PolizasClienteMovil entity);

	public void delete(PolizasClienteMovil entity);


	public PolizasClienteMovil update(PolizasClienteMovil entity);

	public PolizasClienteMovil findById(Long id);


	public List<PolizasClienteMovil> findByProperty(String propertyName, Object value);

	public List<PolizasClienteMovil> findAll();
	
	public MensajeValidacion validateGuardar(InfoPolizaParameter param);
	
	public ValidacionEstatusUsuarioActual validarEstatusUsuarioActual();
	
	public List<PolizasClienteMovil> findByFilters(PolizasClienteMovil filter);
	
	public List<NotificacionCliente> getEnviarNotificacion();
	
	public void envioNotificacionesCliente();

	public List<ClientePolizas> buscarPolizasPorUsuario();
	
	public List<ClientePolizas> buscarPolizasPorUsuario(String userName);
	
	public List<TipoSiniestroMovil> getSiniestrosByTipoProducto(String clave, Long nivel);
	
	public List<TipoSiniestroMovil> getSiniestrosByPadre(Long idPadre);
	
	public MensajeValidacion validateGuardarPolizaMigrada(InfoPolizaParameter param);
	
	public String getTipoProductoPoliza(NumeroPolizaCompleto numeroPoliza);
	
	public void addPolizasMigradasToUser(String imei, String userName);
	
	/**
	 * M\u00e9todo que evia por correo la caratula de la p\u00f3za desde el
	 * cotizador de agentes.
	 * 
	 * @param transporteImpresionDTO Objeto que contiene la caratula
	 * 	que ser\u00e1 enviada.
	 * 
	 * @param destinatario Correo electr\u00f3nico al cual ser\u00e1 enviada
	 *  la caratula
	 *  
	 * @param params Objeto que contiene el asunto etc.
	 */
	public void enviarCaratulaPolizaPDF(TransporteImpresionDTO transporteImpresionDTO, String destinatario, EnvioCaratulaParameter params);
	/**
	 * M\u00e9todo que obtiene el XML de un recibo con el folio fiscal ya existente.
	 * 
	 * @param llaveFiscal String que contiene el folio fiscal
	 * 
	 */
	public byte[] getReciboFiscalXML(String llaveFiscal);
	
	public void initialize();
	
	public List<String> getUsuariosPorPoliza(Long idPoliza, Long inciso);
		
	public void enviarDocumentosRenovacion(EnvioCaratulaParameter params);
	
	public DatosPolizaSeycos getIncisoPolizaVida(Long  poliza,Long inciso); 

	public void enviarNotificacionCorreoRenovacion(Long idPoliza);
		
	public byte[] generarPdf(PolizasClienteMovil polizasClienteMovil);
}
