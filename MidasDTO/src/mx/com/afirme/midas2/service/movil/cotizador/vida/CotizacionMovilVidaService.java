package mx.com.afirme.midas2.service.movil.cotizador.vida;

import java.util.List;

import mx.com.afirme.midas2.domain.movil.cotizador.CotizacionMovilDTO;
import mx.com.afirme.vida.domain.movil.cotizador.CrearCotizacionVidaParameterDTO;
import mx.com.afirme.vida.domain.movil.cotizador.OccupationDTO;
import mx.com.afirme.vida.domain.movil.cotizador.QuotationVO;
import mx.com.afirme.vida.domain.movil.cotizador.SumaAseguradaDTO;

/**
 *
 */

public interface CotizacionMovilVidaService {

	public Object cotizar(CrearCotizacionVidaParameterDTO param);

	public Object saveCotizacion();

	public Object saveAsegurado(CrearCotizacionVidaParameterDTO param, CotizacionMovilDTO cotizacionMovil);

	public QuotationVO buscaCotizacion(int quotationId);
	
	public List<SumaAseguradaDTO> obtenerSumaAseguradaList();
	
	public OccupationDTO getOcupacionPorId(Long id);
	
	public List<OccupationDTO> getGiroOcupacion(OccupationDTO filter);
	
	public Integer calcularEdad(CrearCotizacionVidaParameterDTO param);

}