package mx.com.afirme.midas2.service.movil.cotizador;
import java.util.List;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.domain.movil.RespuestaCargaMasiva;
import mx.com.afirme.midas2.domain.movil.cotizador.AdministracionUsuarioMovil;
import mx.com.afirme.midas2.domain.movil.cotizador.CatalogoEstatusActualizacion;
import mx.com.afirme.midas2.domain.movil.cotizador.CotizacionMovilDTO;
import mx.com.afirme.midas2.domain.movil.cotizador.DescripcionVehiculo;
import mx.com.afirme.midas2.domain.movil.cotizador.DescuentosAgenteDTO;
import mx.com.afirme.midas2.domain.movil.cotizador.Estado;
import mx.com.afirme.midas2.domain.movil.cotizador.MarcaVehiculo;
import mx.com.afirme.midas2.domain.movil.cotizador.ModeloVehiculo;
import mx.com.afirme.midas2.domain.movil.cotizador.SolicitudCotizacionMovil;
import mx.com.afirme.midas2.domain.movil.cotizador.TarifasDTO;
import mx.com.afirme.midas2.domain.movil.cotizador.TipoVehiculo;
import mx.com.afirme.midas2.domain.movil.cotizador.ValidacionClavePromo;
import mx.com.afirme.midas2.domain.movil.cotizador.ValidacionEstatusUsuarioActual;
import mx.com.afirme.midas2.dto.impresiones.ResumenCotMovilDTO;


/**
 * 
 */
public interface CotizacionMovilService {
	
	public List<MarcaVehiculo> getMarcas();
	
	public List<ModeloVehiculo> getModelos(Long marcaId);
	
	public List<TipoVehiculo> getTipos(Long modeloId);
	
	public List<Estado> getEstados(Long tipoVehiculoId);
		
	public List<DescripcionVehiculo> getDescripcionVehiculo(Long estadoId);
	
	public CotizacionMovilDTO getCotizacionAuto(SolicitudCotizacionMovil solicitud);
	
	public  RespuestaCargaMasiva  setTarifasExcelList(List<TarifasDTO> tarifasExcelList,TarifasDTO tarifasDTO);
	
	public  RespuestaCargaMasiva  saveTarifasExcelList(List<TarifasDTO> tarifasExcelList,TarifasDTO tarifasDTO);
	
	public List<TarifasDTO> findAllTarifas();
	
	public List<ResumenCotMovilDTO> inicializarDatosParaImpresion(
			SolicitudCotizacionMovil solicitud,CotizacionMovilDTO cotizacionMovil);
	
	public List<TarifasDTO> findTarifasByFilters(TarifasDTO filter);
	
	public List<Estado> getALLEstados() ;
	
	public ValidacionClavePromo validarClavePromo(ValidacionClavePromo param);
	
	public List<CotizacionMovilDTO> findCotizacionByFilters(CotizacionMovilDTO filter, String tipoCotizacion);
	
	public List<AdministracionUsuarioMovil> findAdministracionUsuarioByFilters(AdministracionUsuarioMovil filter);
	
	public String actualizar(AdministracionUsuarioMovil filter);
	
	public ValidacionEstatusUsuarioActual validarEstatusUsuarioActual(SolicitudCotizacionMovil solicitud);
	
	public String desactivarTarifasMarcas (CatalogoEstatusActualizacion param);
	
	public String desactivarTarifaEstados (CatalogoEstatusActualizacion param);
	
	public String actualizarVistaMaterializada ();
	
	public List<CatalogoEstatusActualizacion> obtenerCatalogoEstados();
	
	public List<CatalogoEstatusActualizacion> obtenerCatalogoMarcas();
	
	public List<TarifasDTO> existeTarifa(TarifasDTO tarifa);
	
	public boolean existeEstiloMarcaVehiculo(TarifasDTO tarifa);
	
	public String getCantidadMaximaCargaMasivaTarifas ();
	
	public boolean isUserValid(Usuario usuario, boolean esUsuarioPortal);
	
	public EstiloVehiculoDTO getEstiloVehiculo(TarifasDTO tarifa);	
}