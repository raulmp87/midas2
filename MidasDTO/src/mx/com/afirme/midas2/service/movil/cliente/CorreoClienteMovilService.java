package mx.com.afirme.midas2.service.movil.cliente;

import java.util.List;
import javax.ejb.Local;

import mx.com.afirme.midas2.domain.movil.cliente.CorreoClienteMovil;

/**
 * Local interface for TrusrcorreoFacade.
 * 
 * @author MyEclipse Persistence Tools
 */
@Local
public interface CorreoClienteMovilService {
	/**
	 * Perform an initial save of a previously unsaved Trusrcorreo entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            Trusrcorreo entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(CorreoClienteMovil entity);

	/**
	 * Delete a persistent Trusrcorreo entity.
	 * 
	 * @param entity
	 *            Trusrcorreo entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(CorreoClienteMovil entity);

	/**
	 * Persist a previously saved Trusrcorreo entity and return it or a copy of
	 * it to the sender. A copy of the Trusrcorreo entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            Trusrcorreo entity to update
	 * @return Trusrcorreo the persisted Trusrcorreo entity instance, may not be
	 *         the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public CorreoClienteMovil update(CorreoClienteMovil entity);

	public CorreoClienteMovil findById(Long id);

	/**
	 * Find all Trusrcorreo entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the Trusrcorreo property to query
	 * @param value
	 *            the property value to match
	 * @return List<Trusrcorreo> found by query
	 */
	public List<CorreoClienteMovil> findByProperty(String propertyName, Object value);

	/**
	 * Find all Trusrcorreo entities.
	 * 
	 * @return List<Trusrcorreo> all Trusrcorreo entities
	 */
	public List<CorreoClienteMovil> findAll();
}