package mx.com.afirme.midas2.service.reporteAgentes;

import javax.ejb.Local;

import mx.com.afirme.midas2.service.jobAgentes.AgenteTaskManagerService;

@Local
public interface ProgramacionReporteAgenteExecuteService {
	public void initialize();

}