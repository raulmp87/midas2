package mx.com.afirme.midas2.service.reporteAgentes;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.reporteAgentes.ReporteAgenteDTO;
import mx.com.afirme.midas2.util.MidasException;
/**
 * Interface para la ejecucion de cada reporte e insertar en las tablas correspondientes.
 * @author vmhersil
 *
 */
@Local
public interface ProgramacionReporteAgenteDelegate {
	/**
	 * Recibe un reporte, del cual se obtiene el nombre del procedure a ejecutar y 
	 * debe de cargar los parametros de dicho reporte para enviarlos al stored procedure.
	 * Cada procedure debe de realizar los siguientes pasos:
	 * 1.- Insertar en la tabla correspondiente del reporte
	 * 2.- Insertar en el log de reportes que ya fue ejecutado.
	 * 3.- Actualizar el proximo valor de los parametros del reporte.
	 * @param reporte
	 */
	public void cargarDatosInicialesPorReporte(ReporteAgenteDTO reporte) throws MidasException;
	/**
	 * Sobrecarga del metodo cargarDatosInicialesPorReporte con la misma funcionalidad
	 * @param idReporte
	 */
	public void cargarDatosInicialesPorReporte(Long idReporte) throws MidasException;
}
