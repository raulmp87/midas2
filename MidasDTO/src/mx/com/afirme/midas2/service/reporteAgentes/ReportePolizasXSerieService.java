package mx.com.afirme.midas2.service.reporteAgentes;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;

/**
 * Clase que define la l\u00f3 que compondr\u00e1
 * la generacion de reporte de p\u00f3lsas por 
 * n\u00famero de serie
 * 
 * @author AFIRME
 *
 * @since 23112016
 * 
 * @version 1.0
 */
public interface ReportePolizasXSerieService {
	
	/**
	 * Enum que contiene los nombres de los campos permitidos
	 * en el layout de excel.
	 * 
	 * @author SEGUTOS AFIRME
	 * 
	 * @since 28112016
	 * 
	 * @version 1.0
	 *
	 */
	public static enum CAMPOS_EXCEL_CARGA{
		NUMERO_SERIE(0){
			public String obtenerNombreColumna(){
				return "NUMEROSERIE";
			}
		},
		NUMERO_POLIZA_SEYCOS(1){
			public String obtenerNombreColumna(){
				return "NUMEROPOLSEYCOS";
			}
		},
		NUMERO_POLIZA_MIDAS(2){
			public String obtenerNombreColumna(){
				return "NUMEROPOLMIDAS";
			}
		};
		
		int idCampoExcel;
		CAMPOS_EXCEL_CARGA(int idCampoExcel){
			this.idCampoExcel = idCampoExcel;
		}
		
		public abstract String obtenerNombreColumna();
	}
	
	/**
	 * M\u0039todo que realiza la funcion de obtener la informacion del archivo de excel
	 * en caso que sea la version de excel 2010 o superior.
	 * 
	 * @param archivoProceso Archivo que contiene la informacion a obtener.
	 * 
	 * @return Mapa que contiene las llaves y los valores de cada valor de las celdas.
	 * 
	 * @return Mapa que contiene como llave el tipo de lista con la informacion que contiene.
	 */
	public Map<String, List<Object>> procesaArchivoPorXSSF(File archivoProceso);
	
	/**
	 * M\u0039todo que realiza la funcion de obtener la informacion de un archivo de excel
	 * en caso que la version de este sea inferior a la version 2010.
	 * 
	 * @param archivoProceso Archivo del cual se obtendra la informacion que contiene.
	 * 
	 * @return Mapa que contiene como llave el tipo de lista con la informacion que contiene.
	 */
	public Map<String, List<Object>> procesaArchivoPorHSSF(File archivoProceso);
	
	/**
	 * Metodo que realiza la consulta de la informacion de las polizas por
	 * numero de serie, numero de poliza midas o numero de poliza seycos mediante el archivo de excel.
	 * 
	 * @param excelData mapa que contiene la informacion extraida de excel.
	 * 
	 * @return Reporte generado.
	 */
	public TransporteImpresionDTO generarReporte(Map<String, List<Object>> excelData);
	
	/**
	 * Metodo que realiza la consulta de la informacion de las polizas por
	 * numero de serie, numero de poliza midas o numero de poliza seycos de forma individual
	 * 
	 * @param numeroSerie
	 * @param numeroSeycos
	 * @param numeroMidas
	 * @return
	 */
	public TransporteImpresionDTO generarReporte(String numeroSerie, Long numeroSeycos, Long numeroMidas);
	
	/**
	 * Metodo que obtiene el nombre y la estension del archivo que anteriormente se sube 
	 * 
	 * @param numeroSerie
	 * @param numeroSeycos
	 * @param numeroMidas
	 * @return
	 */
	public String obtenerNombreCompleto(BigDecimal idToControlArchivo);
}
