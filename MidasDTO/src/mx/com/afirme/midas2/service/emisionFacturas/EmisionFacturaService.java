package mx.com.afirme.midas2.service.emisionFacturas;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.emisionFacturas.EmisionFactura;
import mx.com.afirme.midas2.domain.emisionFacturas.EmisionFacturaHistorico;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Ingreso;
import mx.com.afirme.midas2.dto.emisionFactura.FacturaResultadoDTO;
import mx.com.afirme.midas2.dto.emisionFactura.FiltroFacturaDTO;
import mx.com.afirme.midas2.dto.emisionFactura.IngresoFacturableDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;


@Local
public interface  EmisionFacturaService {

	public String actualizaDatosFiscales();
	
	public void actualizarDatosEmisionFactura(EmisionFactura emisionFactura);
	
	public EmisionFactura obtenerInformacionFactura(Long emisionFacturaId);
	
	public EmisionFactura obtenerInformacionAsegurado(Long ingresoId);
	
	public EmisionFactura obtenerInformacionProveedor(Long ingresoId);
	
	public List<String> guardarInformacionFactura(EmisionFactura emisionFactura);
	
	public List<IngresoFacturableDTO> obtenerIngresosFacturables(String filtro);
	
	public List<String> validarInfomacionAGuardar(EmisionFactura emisionFactura);
	
	public void agregarDatosExtras();
	
	public Map<String,String> getListaTiposPersona();
	
	public Map<String,String> getListaMetodosPago();
	
	public EmisionFactura obtenerEmisionFacturaByIngresoId(Long ingresoId);
	
	public EmisionFactura obtenerDatosImportesPorIngreso(Long ingresoId);
	
	public EmisionFactura obtenerDatosImportesPorIngreso(Long ingresoId, EmisionFactura emisionFactura);
	
	public Ingreso getIngresoById(Long ingresoId);
	
	/**
	 * ------------------------------------
	 * Se agrega del diseño de busqueda/edicion de facturas
	 * ------------------------------------
	 * 
	 * invocar emisionFacturaDAO.buscarFacturas((filtroFacturaDTO)
	 * 
	 * invocar this.completarResultadosdeBusqueda
	 * 
	 * y regresar el resultado
	 * 
	 * @param filtroFacturaDTO
	 */
	public List<FacturaResultadoDTO> buscarFacturas(FiltroFacturaDTO filtroFacturaDTO);
	
	
	/**
	 * ------------------------------------
	 * Se agrega del diseño de busqueda/edicion de facturas
	 * ------------------------------------
	 * 
	 * utilizar un findById obtener el id del ingreso e invocar
	 * this.generaFacturaElectronica
	 * 
	 * @param idEmisionFactura
	 */
	public void reFacturar(Long idEmisionFactura);
	
	/**
	 * ------------------------------------
	 * Se agrega del diseño de busqueda/edicion de facturas
	 * ------------------------------------
	 * 
	 * Obtener el cliente de wFactura
	 * invocar cliente.cancelDigitalBill
	 * si la cancelación fue correcta
	 * cambiar el estatus de emisionFactura a Cancelada
	 * this.entidadService.save(emisionFactura)
	 * validar si motivoCancelacion != NULL
	 *  	ejecutar el listadoService y del map tomar el valor e invocar
	 *  	this.generaHistorico(emisionFactura , valorDeCancelacion)
	 * en caso contrario
	 * 	  this.generaHistorico(emisionFactura , observacion)
	 * 
	 * @param idEmisionFactura
	 * @param motivoCancelacion
	 * @param observacion
	 */
	public void cancelaFactura(Long idEmisionFactura, String motivoCancelacion, String observacion);
	
	/**
	 * ------------------------------------
	 * Se agrega del diseño de busqueda/edicion de facturas
	 * ------------------------------------
	 * 
	 * Utilizar un <u>findByPropertiesWithOrder </u>
	 * <font color="#0f0f0f">utilizando el id de emision factura y ordenado por fecha
	 * de creación descendente</font>
	 * <font color="#0f0f0f">
	 * </font><font color="#0f0f0f">con la lista resultante iterarla y llenar el valor
	 * de estatusDecripcion</font>
	 * 
	 * @param idEmisionFactura
	 */
	public List<EmisionFacturaHistorico> obtenerHistoricoEmisionFactura(Long idEmisionFactura);
	
	
	/**
	 * ------------------------------------
	 * Se agrega del diseño de busqueda/edicion de facturas
	 * ------------------------------------
	 * 
	 * Genera un objeto de tipo EmisionFacturaHistorico
	 * 
	 * historico.estatus = emision.estatus
	 * historico.observaciones = observaciones
	 * historico.folioFactura = emision.folioFactura
	 * historico.emisionFactura = emisionFactura
	 * 
	 * info de control
	 * 
	 * this.entidadService.save(historico)
	 * 
	 * @param emisionFactura
	 * @param observaciones
	 */
	public void generaHistorico(EmisionFactura emisionFactura, String motivoCancelacion, String observacion);
	
	
	/**
	 * ------------------------------------
	 * Se agrega del diseño de busqueda/edicion de facturas
	 * ------------------------------------
	 * 
	 * this.entidadService.findById(emisionfacturaId)
	 * 
	 * validar si el ingreso está ligado a una recuperación de salvamento, llenar los
	 * siguientes atributos Transient:
	 * marca, tipo, modelo, noSerie, noMotor y color
	 * 
	 * y regresar el resultado
	 * 
	 * @param emisionFacturaId
	 */
	public EmisionFactura obtenerEmisionFactura(Long emisionFacturaId);
	
		
	public List<String> obtenerDetalleError(String folioFactura);
	
	public EmisionFactura generarFactura(EmisionFactura emisionFactura);
	
	public String generarFacturaElectronica(String idsEmisionFactura);
	
	public String invocarProcesoWFactura(EmisionFactura emisionFactura);

	public void enviarNotificacionEmisionFactura(EmisionFactura emisionFactura);
	
	/**
	 * @param emisionFactura
	 * @param exitosa True si lo que se desea notificar es la cancelacion exitosa de la factura
	 * 	,False para notificar que la cancelacion no fue completada exitosamente.
	 */
	public void enviarNotificacionCancelacionFactura(EmisionFactura emisionFactura, Boolean exitosa);
	
	public TransporteImpresionDTO obtenerPdf(Long idEmisionFactura);
	
	public void enviarFactura(Long idEmisionFactura, String destinatariosStr, String observaciones);
	
	public AutoIncisoReporteCabina obtenerDatosVehiculoSalvamentoByIngreso(Long ingresoId);

	public void validaCancelacionesEnWFactura();
	
	public void initialize();

}
