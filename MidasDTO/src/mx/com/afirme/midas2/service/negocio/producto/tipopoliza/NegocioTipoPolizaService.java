package mx.com.afirme.midas2.service.negocio.producto.tipopoliza;
import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.dto.negocio.paquete.tipopoliza.RelacionesNegocioTipoPolizaDTO;


public interface NegocioTipoPolizaService {

    List<TipoPolizaDTO> getListarNegocioTipopoliza();
    RelacionesNegocioTipoPolizaDTO getRelationLists(NegocioProducto negocioProducto);
    void relacionarNegocioTipoPoliza(String accion, NegocioTipoPoliza negocioTipoPoliza);
    NegocioTipoPoliza getPorIdNegocioProductoIdToTipoPoliza(Long idToNegProducto, BigDecimal idToTipoPoliza);
    List<NegocioTipoPoliza> listarNegocioTipoPolizaPorProducto(BigDecimal idProducto);
    NegocioTipoPoliza getPorIdNegocioProductoIdToNegTipoPoliza(Long idToNegProducto, BigDecimal idToNegTipoPoliza);   
}