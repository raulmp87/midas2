package mx.com.afirme.midas2.service.negocio.emailsapamis;

import java.util.List;

import mx.com.afirme.midas2.dto.negocio.emailsapamis.EmailNegocioTipoUsuario;

/************************************************************************************
 *
 * Interfaz del servicio para el manejo de Transacciones del Objeto EmailNegocioTipoUsuario
 * 
 *  @author 			Eduardo Valentin Chavez Oliveros (Eduardosco)
 *	Unidad de Fabrica: 	Avance Solutions Corporation S.A. de C.V.
 *
 ************************************************************************************/
public interface EmailNegocioTipoUsuarioService {
	public List<EmailNegocioTipoUsuario> findAll();	
	public EmailNegocioTipoUsuario findById(Long id);	
	public EmailNegocioTipoUsuario findByDesc(String desc);
	public List<EmailNegocioTipoUsuario> findByStatus(boolean status);
	public EmailNegocioTipoUsuario completeObject(EmailNegocioTipoUsuario object);
	public EmailNegocioTipoUsuario saveObject(EmailNegocioTipoUsuario object);
	public List<EmailNegocioTipoUsuario> findByProperty(String propertyName, Object property);
	public EmailNegocioTipoUsuario findIdByAttributes(EmailNegocioTipoUsuario object);
}