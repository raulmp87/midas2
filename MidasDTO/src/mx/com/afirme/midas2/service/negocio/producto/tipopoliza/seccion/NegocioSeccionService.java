package mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.dto.negocio.seccion.RelacionesNegocioSeccionDTO;


public interface NegocioSeccionService {
	/**
	 * Metodo que sirve para prepara un objeto NegocioSeccion apartir de un id
	 * seccion, busca el tipo de poliza que tiene y obtiene los N agrupadores de
	 * tarifa y los setea en el objeto para posteriormente ser persistido
	 * 
	 * @param IdToSeccion
	 * @return NegocioSeccion objeto
	 */
	public NegocioSeccion prepare(BigDecimal IdToSeccion);
	
	/**
	 * Metodo que sirve para pobla la lista NegocioAgrupadorTarifaSeccions NegocioSeccion,
	 * busca el tipo de poliza que tiene y obtiene los N agrupadores de
	 * tarifa y los setea en el objeto para posteriormente ser persistido
	 * 
	 * @param IdToSeccion
	 * @return NegocioSeccion
	 */
	public NegocioSeccion poblarAgrupTarifaSeccion(NegocioSeccion negocioSeccion);
	
	public RelacionesNegocioSeccionDTO getRelationList(NegocioTipoPoliza negocioTipoPoliza);
	
	public void relacionarNegocioSeccion(String accion, NegocioSeccion negocioSeccion);
	
	public List<NegocioTipoPoliza> getNegTipoPolizaList(NegocioProducto negocioProducto);
	
	public List<NegocioSeccion> getSeccionListByCotizacion(CotizacionDTO cotizacion);
	
	/**
	 * Lista las lineas de negocio de una cotizacion
	 * contenidas en los incisos de la cotizacion
	 * si la cotizacion es null obtiene todas las secciones
	 * de incisos
	 * @param cotizacion
	 * @return
	 */
	public List<NegocioSeccion> getSeccionListByCotizacionInciso(CotizacionDTO cotizacion);
	
	public List<NegocioSeccion> getSeccionListByExpress( BigDecimal idNegocioSeccion);
	
	public NegocioSeccion getByProductoNegocioTipoPolizaSeccionDescripcion(ProductoDTO producto, Negocio negocio, TipoPolizaDTO tipoPoliza, String seccionDescripcion);
	
	public List<SeccionDTO> listarSeccionNegocioByClaveNegocio(String claveNegocio);
	
	public List<NegocioSeccion> getSeccionListByIdNegocioProductoTipoPoliza(BigDecimal idToProducto, Long idToNegocio, BigDecimal idToTipoPoliza);
	
	/**
	 * MSN - Obtener todas las secciones ligadas al negocio que sean distintas 
	 * @param idToNegocio
	 * @return
	 */
	public List<SeccionDTO> listarSeccionNegocioByIdNegocio(BigDecimal idToNegocio);
	
	public NegocioSeccion getPorProductoNegocioTipoPolizaSeccionDescripcion(
			ProductoDTO producto, Negocio negocio, TipoPolizaDTO tipoPoliza, SeccionDTO seccion);
	
	public List<NegocioSeccion> listarNegSeccionByIdNegocio(Long idToNegocio);
	
	public NegocioSeccion getPorIdNegTipoPolizaIdToSeccion(BigDecimal idToNegTipoPoliza, BigDecimal idToSeccion);
	
	List<NegocioSeccion> getNegSeccionesPorIdNegTipoPoliza(BigDecimal idToNegTipoPoliza);
}
