package mx.com.afirme.midas2.service.negocio.derechos;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.negocio.derechos.NegocioConfiguracionDerecho;
import mx.com.afirme.midas2.dto.negocio.derechos.NegocioConfiguracionDerechoDTO;
import mx.com.afirme.midas2.dto.negocio.motordecision.MotorDecisionDTO;

@Local
public interface NegocioConfiguracionDerechoService {
	
	public NegocioConfiguracionDerecho guardarNegocioConfiguracionDerecho(NegocioConfiguracionDerecho configuracionDerecho);
	
	public List<NegocioConfiguracionDerechoDTO> buscarNegocioConfiguracionDerecho(NegocioConfiguracionDerecho filtroConfiguracionDerecho);
	
	public Long conteoBusquedaConfiguracionesDerecho(NegocioConfiguracionDerecho negocioConfiguracionDerecho);
	
	public void eliminarNegocioConfiguracionDerecho(NegocioConfiguracionDerecho configuracionDerecho);
	
	public MotorDecisionDTO obtenerFiltroDerechoParaCotizacion(BigDecimal idToCotizacion, BigDecimal numeroInciso);
	
	public MotorDecisionDTO obtenerFiltroDerechoParaCotizacion(Long idNegocio, BigDecimal idTipoPoliza, Long idSeccion, 
			Long idPaquete, BigDecimal idMoneda,  String idEstado, String idMunicipio, Long idTipoUso);
}
