package mx.com.afirme.midas2.service.negocio.ligaagente;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.negocio.ligaagente.ConfiguracionLigaAgente;
import mx.com.afirme.midas2.dto.negocio.ligaagente.ConfigLigaAgenteDTO;
import mx.com.afirme.midas2.dto.negocio.ligaagente.MonitoreoLigaAgenteDTO;

@Local
public interface ConfiguracionLigaAgenteService {

	/**
	 * busca las Configuraciones de Ligas de Agente que cumplan con las condiciones que se establecen en el filtro que recibe
	 * para la pantalla del listado de configuraciones de ligas de agente
	 * @param cfgLigaFiltro
	 * @return
	 */
	public List<ConfigLigaAgenteDTO> buscarConfiguracionesLigasAgentes(ConfigLigaAgenteDTO cfgLigaFiltro);
	
	/**
	 * busca las Configuraciones de Ligas de Agente que cumplan con las condiciones que se establecen en el filtro que recibe
	 * para la pantalla del listado de monitoreo de ligas de agente
	 * @param cfgLigaFiltro
	 * @return
	 */
	public List<MonitoreoLigaAgenteDTO> buscarMonitoreoLigasAgentes(MonitoreoLigaAgenteDTO monitorLigaFiltro);
	
	/**
	 * busca las Configuraciones de Ligas de Agente que cumplan con las condiciones que se establecen en el filtro que recibe
	 * para la pantalla del listado de monitoreo de ligas de agente. Nuevo metodo para paginar la busqueda.
	 * @param monitorLigaFiltro
	 * @return
	 */
	public List<MonitoreoLigaAgenteDTO> buscarMonitoreoLigasAgentesPaginado(MonitoreoLigaAgenteDTO monitorLigaFiltro);
	
	/**
	 * determina cual es la cantidad de registros que se encuentran para la busqueda del monitoreo de ligas de agentes.
	 * @param monitorLigaFiltro
	 * @return
	 */
	public MonitoreoLigaAgenteDTO contarResultadoMonitoreoLigasAgentes(MonitoreoLigaAgenteDTO monitorLigaFiltro);

	/**
	 * Guarda la configuracion de liga de agentes que se le pasa como parametro
	 * @param configuracion
	 * @return
	 */
	public ConfiguracionLigaAgente guardarConfiguracionLigaAgente(ConfiguracionLigaAgente configuracion);
	
	/**
	 * Genera la liga de una configuracion de liga de agente previamente guardada;
	 * @return
	 */
	public ConfiguracionLigaAgente generarLigaAgente(Long idConfiguracion, String ipAddress);
	
	/**
	 * Envia la liga generada al correo que se establecio en la configuracion de liga de agente.
	 * @param idConfiguracion
	 */
	public void enviarLigaAgente(Long idConfiguracion);
	
	/**
	 * Metodo para determinar si el usuario actual puede monitorear a todos los agentes o solamente 
	 * puede ver la informacion del agente actual
	 * @return
	 */
	public boolean tienePermisoMonitorearTodosLosAgentes();
	
	/**
	 * notifica al agente asignado en la configuracion
	 * de la creacion de una cotizacion a partir de su liga de agente 
	 * @param configuracionId
	 * @param idToCotizacion
	 * @param nombreProspecto
	 * @param correoProspecto
	 * @param telefonoProspecto
	 */
	public void enviarNotificacionCreacionCotizacion(Long configuracionId, BigDecimal idToCotizacion, String nombreProspecto, 
			String correoProspecto, String telefonoProspecto, Locale locale);
	
	/**
	 * envia al cliente el pdf de la cotizacion que realizo mediante las ligas de agente
	 * @param configuracionId
	 * @param idToCotizacion
	 * @param nombreContatante
	 */
	public void enviarCotizacionLigaAgenteAlProspecto(Long configuracionId, BigDecimal idToCotizacion, String nombreContatante, String correoContratante, Locale locale);

}
