package mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.paquete;

import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.dto.negocio.producto.tipopoliza.seccion.paquete.RelacionesNegocioPaqueteDTO;

public interface NegocioPaqueteSeccionService {
	/**
	 * Metodo que sirve para prepara un objeto NegocioPaqueteSeccion apartir de la
	 * seccion y un Paquete, busca la informacion necesaria
	 * y setea las coberturas en el objeto para que pueda ser persistido
	 * 
	 * @param IdToSeccion
	 * @return NegocioSeccion objeto
	 */	
	public RelacionesNegocioPaqueteDTO getRelationLists(NegocioSeccion negocioSeccion);
	public void relacionarNegocioPaquete(String accion, NegocioPaqueteSeccion negocioPaqueteSeccion);
	
	public NegocioPaqueteSeccion findByNegocioSeccionAndPaqueteDescripcion(NegocioSeccion negocioSeccion, String paqueteDescripcion);
	
}
