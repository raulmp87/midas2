package mx.com.afirme.midas2.service.negocio.producto;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;


public interface NegocioProductoService {

	public Long agregarProducto(Long idNegocio, Long idProducto);
	
	public List<ProductoDTO> obtenerProductosDisponiblesRelacionar(Long idNegocio);
	
	public List<NegocioProducto> obtenerNegociosRelacionados(Long idNegocio);
	
	public NegocioProducto getNegocioProductoByCotizacion(CotizacionDTO cotizacion);
	
	public List<ProductoDTO> listarProductosGroupBy(Long idToNegocio);
	public Long deleteProducto(Long idToNegProducto);
	
}
