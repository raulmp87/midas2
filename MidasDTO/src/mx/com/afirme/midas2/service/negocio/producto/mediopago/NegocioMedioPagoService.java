package mx.com.afirme.midas2.service.negocio.producto.mediopago;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.mediopago.NegocioMedioPago;
import mx.com.afirme.midas2.dto.negocio.producto.mediopago.RelacionesNegocioMedioPagoDTO;


public interface NegocioMedioPagoService {
	public RelacionesNegocioMedioPagoDTO getRelationLists(NegocioProducto negocioProducto);
	public void relacionarNegocioMedioPago(String accion, NegocioMedioPago negocioMedioPago);
}
