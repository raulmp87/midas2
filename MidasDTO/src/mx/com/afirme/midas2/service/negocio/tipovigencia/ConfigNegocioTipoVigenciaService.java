/**
 * 
 */
package mx.com.afirme.midas2.service.negocio.tipovigencia;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.negocio.tipovigencia.NegocioTipoVigencia;
import mx.com.afirme.midas2.domain.negocio.tipovigencia.TipoVigencia;

/**
 * @author maguecon
 *
 */
@Local
public interface ConfigNegocioTipoVigenciaService {
	
	/**
	 * Agregar nuevo TipoVigencia
	 * @param tipoVigencia
	 */
	void agregarTipoVigencia(TipoVigencia tipoVigencia);
	
	/**
	 * Eliminar TipoVigencia
	 * @param id
	 */
	Boolean eliminarTipoVigencia(Long id);
	
	/**
	 * Asociar TipoVigencia a un negocio
	 * @param idNegocio
	 * @param idTipoVigencia
	 */
	void asignarANegocio(Long idNegocio, Long idTipoVigencia);
	
	/**
	 * Validar TipoVigencia dentro de los rangos de vigencia de los productos del negocio
	 * @param tipoVigencia
	 * @param idNegocio
	 */
	Boolean validarContraVigenciaProductosNegocio(TipoVigencia tipoVigencia, Long idNegocio);
	
	/**
	 * Validar parámetro fecha fin vigencia fija
	 * @param id
	 */
	Boolean validarParametroFechaFinVigenciaFija(Long idNegocio);
	
	/**
	 * Definir vigencia default para un negocio
	 * @param idTipoVigencia
	 * @param idNegocio
	 */
	void setTipoVigenciaDefaultNegocio(Long idNegocioTipoVigencia, Long idNegocio);
	
	/**
	 * Desasociar TipoVigencia de un negocio
	 * @param idTipoVigencia
	 * @param idNegocio
	 */
	void quitarANegocio(Long idNegocioTipoVigencia, Long idNegocio);
	
	/**
	 * Obtener listado de TipoVigencia disponbles para un negocio
	 * @param idNegocio
	 */
	List<NegocioTipoVigencia> obtenerTiposVigenciaDisponibles(Long idNegocio);
	
	/**
	 * Obtener listado de TipoVigencia asociados a un negocio
	 * @param idNegocio
	 */
	List<NegocioTipoVigencia> obtenerTiposVigenciaAsociados(Long idNegocio);
	
	/**
	 * Obtener listado de TipoVigencia
	 */
	List<TipoVigencia> obtenerTiposVigencia();
	
	/**
	 * Asociar todos las vigencias disponibles a un negocio
	 */
	boolean asociarTodos(Long idNegocio);
	
	/**
	 * Desasociar todos las vigencias asignadas a un negocio
	 */
	void desasociarTodos(Long idNegocio);
	
	/**
	 * Validar si la vigencia que se quiere registrar existe en BD, días debe ser unico 
	 */
	boolean existeRegistroDiasVigencia(Integer dias);
	
	/**
	 * Validar si el negocio tiene una vigencia por default
	 */
	NegocioTipoVigencia existeVigenciaDefaultNegocio(Long idNegocio);
	
	/**
	 * Cambiar vigencia default
	 */
	void cambiarVigenciaDefault(Long idNegocioTipoVigencia, Long idNegocio);
	
	/**
	 * Relacionar vigencia con negocio
	 */
	void relacionarTipoVigencia(String accion, NegocioTipoVigencia negocioTipoVigencia);
}
