package mx.com.afirme.midas2.service.negocio.cliente;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.negocio.cliente.NegocioCliente;


public interface ClienteAsociadoJPAService {
	public List<NegocioCliente> listarClientesAsociados(Long idToNegocio);
}
