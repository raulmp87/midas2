package mx.com.afirme.midas2.service.negocio.tarifa;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccion;
import mx.com.afirme.midas2.dto.negocio.tarifa.RelacionesNegocioTarifaDTO;

public interface NegocioTarifaService {

	public List<RelacionesNegocioTarifaDTO> getNegSeccionList(NegocioTipoPoliza negocioTipoPoliza);
	
	public List<NegocioTipoPoliza> getNegTipoPolizaList(NegocioProducto negocioProducto);
	
	public void relacionarNegocioTarifa(BigDecimal idToSeccion, BigDecimal idTcMoneda, BigDecimal idToAgrupadorTarifa,BigDecimal idVerAgrupadorTarifa);
	/**
	 * Actualiza todos los NegocioAgrupadorTarifaSeccion
	 * que tiene un producto 
	 * @param idToNegSeccion
	 * @param idTcMoneda
	 * @param idToAgrupadorTarifa String con todos los id 
	 * @param idVerAgrupadorTarifa
	 * @autor martin
	 */
	public void relacionarNegocioTarifa(List<String> idToNegSeccion,
			List<String> idTcMoneda,
			List<String> idToAgrupadorTarifa, List<String> idVerAgrupadorTarifa);
	
	public NegocioAgrupadorTarifaSeccion getNegocioAgrupadorTarifaSeccionPorNegocioSeccionMoneda(BigDecimal idToNegSeccion, BigDecimal idMoneda);
}
