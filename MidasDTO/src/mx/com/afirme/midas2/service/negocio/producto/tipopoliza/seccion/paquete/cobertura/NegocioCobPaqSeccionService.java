
package mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.paquete.cobertura;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.sumaasegurada.NegocioCobSumAse;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionCobertura;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.dto.negocio.producto.tipopoliza.seccion.paquete.cobertura.RelacionesNegocioCoberturaDTO;

 
public interface NegocioCobPaqSeccionService {
	public RelacionesNegocioCoberturaDTO getRelationLists(NegocioCobPaqSeccion negocioCobPaqSeccion);	
	public RespuestaGridRelacionDTO relacionarNegocioCobPaqSeccion(String accion, NegocioCobPaqSeccion negocioCobPaqSeccion);
	
	public List<NegocioRenovacionCobertura> getCoberturasPorNegSeccion(Long idToNegocio, Short tipoRiesgo, BigDecimal idToNegSeccion);
	
	public List<NegocioCobPaqSeccion> getCoberturaPorNegSeccion(
			BigDecimal idToCobertura,String stateId,String cityId, Short idTcMoneda,Long idToNegPaqueteSeccion, BigDecimal IdTcTipoUsoVehiculo, Long agenteId, Boolean esRenovacion);
	
	public NegocioCobPaqSeccion getLimitesSumaAseguradaPorCobertura(
			BigDecimal idToCobertura,String stateId,String cityId, BigDecimal idTcMoneda,Long idToNegPaqueteSeccion);
	
	public List<NegocioCobSumAse> obtenerSumasAseguradas(BigDecimal idToNegCobPaqSeccion);
	
	public RespuestaGridRelacionDTO relacionarSumasAseguradasCobertura(String accion, BigDecimal idToNegCobPaqSeccion, Long idSumaAsegurada, Double valorSumaAsegurada, short defaultValor);
}
