package mx.com.afirme.midas2.service.negocio.canalventa;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;

import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.negocio.canalventa.NegocioCanalVenta;

@Local
public interface CanalVentaNegocioService extends Serializable {
		
	public List<NegocioCanalVenta> getCamposPorTipoYNegocio(
			TIPO_CATALOGO tipo, Long Negocio);
	
	public void saveCamposNegocio(Long idCampoEditado);
	
	public void saveAllCamposNegocio(Boolean activo, Long idNegocio);
	
	public String getCamposPorTipoYNegocioJSON(String tipo, Long idNegocio);

	public List<NegocioCanalVenta> getCamposPorTipoYNegocio(TIPO_CATALOGO tipo, String codigoPadre, BigDecimal idNegocio);
	
}
