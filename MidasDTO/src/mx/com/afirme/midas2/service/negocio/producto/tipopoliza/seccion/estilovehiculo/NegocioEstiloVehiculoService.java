package mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.estilovehiculo;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.estilovehiculo.NegocioEstiloVehiculo;
import mx.com.afirme.midas2.dto.negocio.estilovehiculo.RelacionesNegocioEstiloVehiculoDTO;


public interface NegocioEstiloVehiculoService {
	public List<EstiloVehiculoDTO> getListarEstiloVehiculo(
			BigDecimal idTcMarcaVehiculo, BigDecimal tipoVehiculo,
			String claveTipoBien, BigDecimal idToNegSeccion, BigDecimal idMoneda);

	public List<NegocioEstiloVehiculo> getEstiloVehiculoNoAsociados(
			NegocioSeccion negocioSeccion, BigDecimal idTcMarcaVehiculo,
			BigDecimal tipoVehiculo, BigDecimal idMoneda);

	public RelacionesNegocioEstiloVehiculoDTO getRelationList(
			NegocioSeccion negocioSeccion);

	public void relacionarNegocioEstiloVehiculo(String accion,
			NegocioEstiloVehiculo negocioEstiloVehiculo);
	
	public List<NegocioEstiloVehiculo> getEstiloVehiculoAsociados(
			NegocioSeccion negocioSeccion, BigDecimal idTcMarcaVehiculo,
			BigDecimal tipoVehiculo);	

	public List<EstiloVehiculoDTO> getEstilosVehiculo(String claveTipoBien,Integer modelo, String descripcion);	
}
