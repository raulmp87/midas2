package mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.tiposervicio;

import java.io.Serializable;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tiposervicio.NegocioTipoServicio;
import mx.com.afirme.midas2.dto.negocio.seccion.tiposervicio.RelacionesNegocioTipoServicioDTO;


public interface NegocioTipoServicioService extends Serializable {
	public RelacionesNegocioTipoServicioDTO getRelationLists(NegocioSeccion negocioSeccion);
	public void relacionarNegocioTipoServicio(String accion, NegocioTipoServicio negocioTipoServicio);
	/**
	 * Busca el <code>NegocioTipoServicio</code> default para el <code>NegocioSeccion</code> dado.
	 * @param negocioSeccion
	 * @return el <code>NegocioTipoServicio</code> default o null
	 */
	public NegocioTipoServicio buscarDefault(NegocioSeccion negocioSeccion);

}
