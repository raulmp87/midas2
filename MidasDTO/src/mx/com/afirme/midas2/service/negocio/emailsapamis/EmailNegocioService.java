package mx.com.afirme.midas2.service.negocio.emailsapamis;

import java.util.List;

import mx.com.afirme.midas2.dto.negocio.emailsapamis.EmailNegocio;

/************************************************************************************
 *
 * Interfaz del servicio para el manejo de Transacciones del Objeto EmailNegocio
 * 
 *  @author 			Eduardo Valentin Chavez Oliveros (Eduardosco)
 *	Unidad de Fabrica: 	Avance Solutions Corporation S.A. de C.V.
 *
 ************************************************************************************/
public interface EmailNegocioService {
	public List<EmailNegocio> findAll();	
	public EmailNegocio findById(Long id);
	public List<EmailNegocio> findByStatus(boolean status);
	public EmailNegocio completeObject(EmailNegocio object);
	public EmailNegocio saveObject(EmailNegocio object);
	public List<EmailNegocio> findByProperty(String propertyName, Object property);
	public EmailNegocio findIdByAttributes(EmailNegocio object);
}