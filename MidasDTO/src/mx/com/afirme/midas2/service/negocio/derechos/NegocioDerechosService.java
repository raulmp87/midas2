package mx.com.afirme.midas2.service.negocio.derechos;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoEndoso;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;


public interface NegocioDerechosService {

	public List<NegocioDerechoPoliza> obtenerDerechosPoliza(Long idToNegocio);
	
	public List<NegocioDerechoEndoso> obtenerDerechosEndoso(Long idToNegocio, Short esAltaInciso);
	
	/**
	 * Obtiene el <code>NegocioDerechoPoliza</code> default para el negocio dado.
	 * @param idToNegocio
	 * @return el <code>NegocioDerechoPoliza</code> default o null
	 */
	public NegocioDerechoPoliza obtenerDechosPolizaDefault(Long idToNegocio);
	
	public RespuestaGridRelacionDTO relacionarDerechosPoliza(String accion, Long idToNegocio, Long idToNegDerechoPoliza, 
			Double importeDerecho, Short claveDefault);
	
	public RespuestaGridRelacionDTO relacionarDerechosEndoso(String accion, Long idToNegocio, Long idToNegDerechoEndoso,
			Double importeDerecho, Short claveDefault, Short esAltaInciso);
	
	public NegocioDerechoEndoso obtenerDerechosEndosoDefault(SolicitudDTO solicitud);
	
}
