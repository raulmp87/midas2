package mx.com.afirme.midas2.service.negocio.emailsapamis;

import java.util.List;

import mx.com.afirme.midas2.dto.negocio.emailsapamis.EmailNegocioConfig;

/************************************************************************************
 *
 * Interfaz del servicio para el manejo de Transacciones del Objeto EmailNegocioConfig
 * 
 *  @author 			Eduardo Valentin Chavez Oliveros (Eduardosco)
 *	Unidad de Fabrica: 	Avance Solutions Corporation S.A. de C.V.
 *
 ************************************************************************************/
public interface EmailNegocioConfigService {
	public List<EmailNegocioConfig> findAll();	
	public EmailNegocioConfig findById(Long id);
	public List<EmailNegocioConfig> findByStatus(boolean status);
	public EmailNegocioConfig completeObject(EmailNegocioConfig object);
	public EmailNegocioConfig saveObject(EmailNegocioConfig object);
	public List<EmailNegocioConfig> findByProperty(String propertyName, Object property);
	public EmailNegocioConfig findIdByAttributes(EmailNegocioConfig object);
}