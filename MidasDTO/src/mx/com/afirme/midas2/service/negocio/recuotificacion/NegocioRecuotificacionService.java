package mx.com.afirme.midas2.service.negocio.recuotificacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.EnumBase;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacion;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacionAutProgPago;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacionAutRecibo;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacionAutomatica;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacionUsuario;

@Local
public interface NegocioRecuotificacionService extends Serializable {
	
	public enum TipoSaldo implements EnumBase<String>{
		PCTE_PROGPAGO, PCTE_RECIBO_DERECHOS, PCTE_PROGRAMA_DERECHOS, PCTE_VERSION_DERECHOS, PCTE_RECIBOS_PRIMA, DIAS_DURACION;

		@Override
		public String getValue() {			
			return this.toString();
		}

		@Override
		public String getLabel() {
			return this.toString();
		}
	
	};
	
	/**
	 * Validar si el usuario en cuestion tiene permisos de realizar recuotificacion automatica para el negocio dado
	 * @param negocioId
	 * @param cveUsuario
	 * @return
	 */
	public boolean validarPermisoRecuotificacionManual(Long negocioId, Long usuarioId);
	
	/**
	 * Activar recuotificacion automatica
	 * @param id de la recuotificacion
	 * @param activar flag <code>true</code> o <code>false</code>
	 */
	public void activarRecuotificacionAutomatica(Long id, boolean activar);
	
	/**
	 * Activar recuotificacion manual
	 * @param id de la recuotificacion
	 * @param activar flag <code>true</code> o <code>false</code>
	 */
	public void activarRecuotificacionManual(Long id, boolean activar);
	
	/**
	 * Activar recuotificacion prima total
	 * @param id de la recuotificacion
	 * @param activar flag <code>true</code> o <code>false</code>
	 */
	public void activarRecuotificacionPrimaTotal(Long id, boolean activar);
	
	/**
	 * Modificar monto de recuotificacion de prima total
	 * @param id de la recuotificacion
	 * @param activar flag <code>true</code> o <code>false</code>
	 */
	public void modificarRecuotificacionPrimaTotal(Long id, BigDecimal total);
	
	/**
	 * Obtener recuotificacion general para un negocio. En caso de no existir se crea una nueva.
	 * @param idToNegocio id del negocio
	 */
	public NegocioRecuotificacion obtener(Long idToNegocio);
	
	/**
	 * Obtener usuarios disponibles para dar permiso de recuotificacion manual
	 * @param idToNegocio del negocio
	 */
	public List<NegocioRecuotificacionUsuario> obtenerUsuariosDisponibles(Long idToNegocio);
	
	/**
	 * Obtener usuarios asociados para dar permiso de recuotificacion manual
	 * @param idToNegocio del negocio
	 */
	public List<NegocioRecuotificacionUsuario> obtenerUsuariosAsociados(Long idToNegocio);
	
	/**
	 * Asociar o desasociar usuario a la recuotificacion manual
	 * @param accion operacion <code>inserted</code>, <code>deleted</code> (dhtmlx)
	 * @param usuario 
	 */
	public void relacionar(String accion, NegocioRecuotificacionUsuario usuario);
	
	/**
	 * Asociar todos los usuarios disponibles
	 * @param idToNegocio del negocio
	 */
	public void asociarTodos(Long idToNegocio);
	
	/**
	 * Desasociar todos los usuarios de la recuotificacion
	 * @param idToNegocio del negocio
	 */
	public void desasociarTodos(Long idToNegocio);
	
	/**
	 * Activar version
	 * @param id de la version
	 */
	public void activarVersion(Long versionId);
	
	/**
	 * Generar nueva version de recuotificacion automatica
	 * @param negocioId id del negocio donde se creara una nueva version
	 */
	public NegocioRecuotificacionAutomatica nuevaVersion(Long negocioId);
	
	/**
	 * Obtener version activa. En caso de que no exista ninguna, se creara una nueva version y se retornara
	 * @param idToNegocio id del negocio donde se obtendr la version activa 
	 */
	public NegocioRecuotificacionAutomatica obtenerVersionActiva(Long idToNegocio);
	
	/**
	 * Generar nuevo programa de pago
	 * @param recuotificacionId id de la recuotificacion donde se creara una nueva version
	 */
	public NegocioRecuotificacionAutProgPago nuevoProgramaPago(Long versionId);
	
	/**
	 * Obtener informacion de un programa de pago
	 * @param versionId id de la version que almacena el programa de pago
	 * @return listado de registros de los programa de pago
	 */
	public List<NegocioRecuotificacionAutProgPago> obtenerProgramasPago(Long versionId);
	
	
	/**
	 * Modificar datos de un programa de pago	
	 * @param progPago
	 */
	public void modificarProgramaPago(NegocioRecuotificacionAutProgPago progPago);
	
	
	/**
	 * Eliminar programa de pago
	 * @param progPagoId id de la version que almacena el programa de pago
	 */
	public void eliminarProgramaPago(Long progPagoId);
	
	
	/**
	 * Obtener listado de versiones existentes para un negocio
	 * @param idToNegocio
	 * @return
	 */
	public Map<Long, Integer> obtenerListadoVersiones(Long idToNegocio);
	
	/**
	 * Obtener una version en especifico por su identificador
	 * @param version
	 * @return
	 */
	public NegocioRecuotificacionAutomatica obtenerVersion(Long version);
	
	
	/**
	 * Obtener el total para una version por tipo de total (PCTE_PROGPAGO, PCTE_RECIBO_DERECHOS, PCTE_RECIBOS_PRIMA)
	 * @param versionId
	 * @param tipo
	 * @return
	 */
	public BigDecimal obtenerTotal(Long versionId, TipoSaldo tipo);
	
	/**
	 * Obtener el saldo para una version por tipo de saldo (PCTE_PROGPAGO, PCTE_RECIBO_DERECHOS, PCTE_RECIBOS_PRIMA)
	 * @param id
	 * @param tipo
	 * @return
	 */
	public BigDecimal obtenerSaldo(Long versionId, TipoSaldo tipo);
	
	/**
	 * Obtener informacion general de un programa de pago en especial
	 * @param programaId
	 * @return
	 */
	public NegocioRecuotificacionAutProgPago obtenerPrograma(Long programaId);
	
	/**
	 * Obtener listado de recibos para un programa de pago
	 * @param progPagoId
	 * @return
	 */
	public List<NegocioRecuotificacionAutRecibo> obtenerRecibos(Long progPagoId);
	
	/**
	 * Modificar informacion de recibo
	 * @param recibo
	 */
	public void modificarRecibo(NegocioRecuotificacionAutRecibo recibo, TipoSaldo tipo);
	
	
	/**
	 * Eliminar recibo por su id
	 * @param reciboId
	 */
	public void eliminarRecibo(Long reciboId);
	
	
	/**
	 * Generar un recibo nuevo dentro de un programa de pago
	 * @param progPagoId
	 * @return
	 */
	public NegocioRecuotificacionAutRecibo generarRecibo(Long progPagoId);
	

	/**
	 * Obtener negocio recuotificaicon por id negocio. 
	 * @param idToNegocio
	 * @return negocio recuotficacion o <code>null</code> si no existe
	 */
	public NegocioRecuotificacion obtenerNegocioRecuotificacion(Long idToNegocio);
	
		
}
