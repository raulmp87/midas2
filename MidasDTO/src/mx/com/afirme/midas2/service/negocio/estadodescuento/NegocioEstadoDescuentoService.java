package mx.com.afirme.midas2.service.negocio.estadodescuento;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.negocio.estadodescuento.NegocioEstadoDescuento;
import mx.com.afirme.midas2.dto.negocio.estadodescuento.RelacionesNegocioEstadoDescuento;

@Local
public interface NegocioEstadoDescuentoService {
    public RelacionesNegocioEstadoDescuento getRelationLists(Long idToNegocio);
    public Long relacionarNegocio(String accion, NegocioEstadoDescuento negocioEstadoDescuento);
    public Long guardar(String accion, NegocioEstadoDescuento negocioEstadoDescuento);
	public Map<Long, String> getNegocioEstadosPorNegocioId(Long idToNegocio);
	public List<NegocioEstadoDescuento> obtenerEstadosPorNegocioId(Long idToNegocio);
	public List<NegocioEstadoDescuento> getNegocioAndEstadosAsociados(Long idToNegocio);
	public NegocioEstadoDescuento findByNegocioAndEstado(Long idToNegocio, String idToEstado);
	public String obtenerZipCode (Long idToNegocio, String stateId);
}
