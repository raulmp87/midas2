package mx.com.afirme.midas2.service.negocio.emailsapamis;

import java.util.List;

import mx.com.afirme.midas2.dto.negocio.emailsapamis.EmailNegocioAlertas;

/************************************************************************************
 *
 * Interfaz del servicio para el manejo de Transacciones del Objeto AlertasEmailNegocio
 * 
 *  @author 			Eduardo Valentin Chavez Oliveros (Eduardosco)
 *	Unidad de Fabrica: 	Avance Solutions Corporation S.A. de C.V.
 *
 ************************************************************************************/
public interface EmailNegocioAlertasService {
	public List<EmailNegocioAlertas> findAll();	
	public EmailNegocioAlertas findById(Long id);
	public List<EmailNegocioAlertas> findByStatus(boolean status);
	public EmailNegocioAlertas completeObject(EmailNegocioAlertas object);
	public EmailNegocioAlertas saveObject(EmailNegocioAlertas object);
	public List<EmailNegocioAlertas> findByProperty(String propertyName, Object property);
	public EmailNegocioAlertas findIdByAttributes(EmailNegocioAlertas object);
}