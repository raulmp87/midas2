package mx.com.afirme.midas2.service.negocio.motordecision;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.negocio.motordecision.MotorDecisionDTO;
import mx.com.afirme.midas2.dto.negocio.motordecision.ResultadoMotorDecisionAtributosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.CargaMasivaServicioPublicoEstatusDescriDetalleDTO.EstatusDescriDetalle;

@Local
public interface MotorDecisionAtributosService {

	/**
	 * Método de interfaz genérico para la consulta de valores de configuración
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @param {@link MotorDecisionDTO} motorDecisionDto
	 * @return {@link ResultadoMotorDecisionAtributosDTO}
	 */
	public List<ResultadoMotorDecisionAtributosDTO> consultaValoresService(MotorDecisionDTO motorDecisionDto);
	
	/**
	 * Método de interfaz genérico para la validación de sumas aseguradas
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @param {@link MotorDecisionDTO} motorDecisionDtopp
	 * @return int - Rgresa el entero depediendo del tipo de error.  
	 */
	public int validaSumasDeducibles(MotorDecisionDTO motorDecisionDto, Double montoSuma, int tipoValida);
	
	/**
	 * Método de interfaz genérico para la validación de derechos
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @param {@link MotorDecisionDTO} motorDecisionDto
	 * @return EstatusDescriDetalle - Objeto que encapsula el codigo de estatus y la descripción en caso de error 
	 */
	public EstatusDescriDetalle validaDerechos(MotorDecisionDTO motorDecisionDto, Double montoDerecho);
}
