package mx.com.afirme.midas2.service.negocio.antecedentes;

import java.sql.SQLException;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.negocio.antecedentes.NegocioCEAAnexos;
import mx.com.afirme.midas2.domain.negocio.antecedentes.NegocioCEAComentarios;

@Local
public interface NegocioAntecedentesService {
	public void saveAnexo(NegocioCEAAnexos negocioCEAAnexos);
	public Long updateAnexo(String action, NegocioCEAAnexos negocioCEAAnexos);
    public void saveComentario(NegocioCEAComentarios negocioCEAComentarios);
    public List<NegocioCEAAnexos> obtenerAntecedentesAnexos(Long idToNegocio);
    public List<NegocioCEAComentarios> obtenerAntecedentesComentarios(Long idToNegocio);
    public NegocioCEAAnexos findAnexoById(Long id) throws SQLException, Exception;
    public List<NegocioCEAAnexos> findAnexosByFmxDocName(String fmxDocName) throws SQLException, Exception;
	public List<NegocioCEAAnexos> findAnexosByIdToNegocio(Long idToNegocio) throws SQLException, Exception;
}
