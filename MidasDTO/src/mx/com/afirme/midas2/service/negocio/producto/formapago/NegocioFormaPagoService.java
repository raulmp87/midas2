package mx.com.afirme.midas2.service.negocio.producto.formapago;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.formapago.NegocioFormaPago;
import mx.com.afirme.midas2.dto.negocio.producto.formapago.RelacionesNegocioFormaPagoDTO;


public interface NegocioFormaPagoService {
	
	public RelacionesNegocioFormaPagoDTO getRelationLists(NegocioProducto negocioProducto);
	public void relacionarNegocioFormaPago (String accion, NegocioFormaPago negocioFormaPago);
	
}
