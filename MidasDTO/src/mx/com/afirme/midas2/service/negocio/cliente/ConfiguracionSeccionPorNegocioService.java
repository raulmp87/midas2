package mx.com.afirme.midas2.service.negocio.cliente;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.negocio.cliente.ConfiguracionGrupo;
import mx.com.afirme.midas2.domain.negocio.cliente.ConfiguracionSeccionPorNegocio;
import mx.com.afirme.midas2.dto.negocio.cliente.ResultadoActualizacionNodo;


public interface ConfiguracionSeccionPorNegocioService {

	/**
	 * Regresa la configuración de obligatoriedad para un negocio.
	 * Si no existe la configuración, la crea.
	 * @param idNegocio
	 * @return
	 */
	List<ConfiguracionSeccionPorNegocio> getConfiguracionPorNegocio(Long idNegocio);
	
	/**
	 * Modifica la obligatoriedad de un nodo de la estructura de seccion y grupo (campo no es editable).
	 * @param idNodo
	 * @param claveObligatorio	obligatoriedad a establecar al nodo
	 * @return
	 */
	ResultadoActualizacionNodo actualizarNodo(String idNodo,Short claveObligatorio);
	
	List<ConfiguracionGrupo> getCamposRequeridos(Long idNegocio, Short tipoPersona);
	
	List<ConfiguracionGrupo> getCamposRequeridos(Long idNegocio, Short tipoPersona,String codigoSeccion);
}
