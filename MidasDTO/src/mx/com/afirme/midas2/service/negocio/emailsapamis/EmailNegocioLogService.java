package mx.com.afirme.midas2.service.negocio.emailsapamis;

import java.util.List;

import mx.com.afirme.midas2.dto.negocio.emailsapamis.EmailNegocioLog;

/************************************************************************************
 *
 * Interfaz del servicio para el manejo de Transacciones del Objeto EmailLog
 * 
 *  @author 			Eduardo Valentin Chavez Oliveros (Eduardosco)
 *	Unidad de Fabrica: 	Avance Solutions Corporation S.A. de C.V.
 *
 ************************************************************************************/
public interface EmailNegocioLogService {
	public List<EmailNegocioLog> findAll();	
	public EmailNegocioLog findById(Long id);
	public List<EmailNegocioLog> findByStatus(boolean status);
	public EmailNegocioLog saveObject(EmailNegocioLog object);
	public List<EmailNegocioLog> findByProperty(String propertyName, Object property);
}