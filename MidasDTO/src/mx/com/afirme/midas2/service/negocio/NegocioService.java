package mx.com.afirme.midas2.service.negocio;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.agente.NegocioAgente;
import mx.com.afirme.midas2.domain.negocio.cliente.NegocioCliente;


public interface NegocioService{
	

    public Negocio findById(Long id);
    public List<Negocio> findAll();
    public List<Negocio> findByFilters(Negocio filtronegocio);
	public Negocio findByClave(Long clave);	
	public List<NegocioAgente> listarNegocioAgentes(Long id);
	
	public List<String> copiarNegocio(Long idNegocio, String nombreNegocio,boolean copiarEnCascada,Date fechaFinVigencia) ;
	
	/**
	 * Cambia el estatus de un negocio.
	 * El cambio es directo sin validaci�n alguna
	 * 0 - Creado
	 * 1 - Activo
	 * 2 - Inactivo
	 * 3 - Borrado
	 * @param idToNegocio
	 */
	public void cambiarEstatus(Long idToNegocio, Negocio.EstatusNegocio estatus);
	
	/**
	 * Cambia el tipo de persona de un negocio.
	 * El cambio es directo sin validaci�n alguna
	 * 0 - Indefinido
	 * 1 - Persona Física
	 * 2 - Persona Moral
	 * @param idToNegocio
	 * @param tipoPersona
	 */
	public void cambiarTipoPersona(Long idToNegocio, short tipoPersona);
	
	/**
	 * Activa (autoriza) un negocio.
	 * Para poder activar el negocio primero se valida que se cuente con 
	 * lo minimo requerido para la activaci�n, en caso de no ser as� se arroja
	 * una runtimeexception
	 * @param idToNegocio
	 * @return List<String> Retorna una lista con las validaciones que no se 
	 * cumplieron para activar el negocio
	 */
	public List<String> activarNegocio(Long idToNegocio);
	
	/**
	 * Valida si el negocio est� listo para ser activado, es decir, 
	 * cuenta con lo minimo requerido
	 * @param idToNegocio
	 * @return List<String> Contiene los errores de validaci�n encontrados.
	 * Si la lista es vac�a quiere decir que el negocio est� listo para ser 
	 * convertido
	 * 
	 */
	public List<String> validarNegocioParaActivacion(Long idToNegocio);
	
	/**
	 * Listar negocios no borrados por clave negocio
	 * @param cveNegocio
	 * @return
	 */
	public List<Negocio> listarNegociosNoBorrados(String cveNegocio);
	
	/**
	 * Listar negocios no asociados a ningun agente
	 * @param claveNegocio
	 * @return
	 */
	public List<Negocio> listarNegociosSinAgentes(String claveNegocio, Boolean esExterno);
	
	/**
	 * Listar negocios por agente + negocios sin agentes
	 * @param Agente, claveNegocio
	 * @return
	 */
	public List<Negocio> listarNegociosPorAgenteUnionNegociosLibres(Integer idTcAgente, String claveNegocio);
	
	/**
	 * Eliminar negocio de manera logica (cambia estatus a 3). Si el negocio esta activo (autorizador) no se puede borrar. 
	 * @param idToNegocio
	 * @return List<String> contiene los errores de validacion para borrar el negocio.
	 */
	public List<String> eliminarNegocio(Long idToNegocio);
	
	public boolean isDuplicado(Negocio negocio);
	public boolean isDuplicado(String DescripcionNegocio);
	
	public List<Negocio> listarNegociosPorUsuario(String claveNegocio);
	/**
	 * Lista los clientes asociados a un negocio 
	 * que esten en grupos o clientes individuales
	 * @param idToNegocio
	 * @return listado con la union de los dos tipos cientes iindividuales y grupo de clinetes
	 * @author martin
	 */
	public List<NegocioCliente> listarClientesAsociados(Long idToNegocio);
	
	/**
	 * Guarda un negocio y retorna su id 
	 * @param negocio
	 * @return
	 */
	public Long guardar(Negocio negocio, String tipoAccion);
	
	
	/**
	 * Indica si el <code>Agente</code> tiene permitido hacer uso del <code>Negocio</code> dado.
	 * @param agente
	 * @param negocio
	 * @return
	 */
	public boolean isAgenteTienePermitidoNegocio(Agente agente, Negocio negocio);
	
	/**
	 * elimina de la lista los negocios que no apliquen a sucursal
	 * @param negocio
	 * @return lista de negocios que  aplican a sucursal
	 */
	
	public List<Negocio> eliminarNegocioNoAplicaSucursal( List<Negocio> union);
	
}
