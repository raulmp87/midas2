package mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducible;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducibles.NegocioDeducibleCob;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;


public interface NegocioDeduciblesService {

	public List<NegocioDeducibleCob> obtenerDeduciblesCobertura(BigDecimal idToNegCobPaqSeccion);
	
	public NegocioDeducibleCob obtenerDeducibleDefault(BigDecimal idToNegCobPaqSeccion);
	
	public RespuestaGridRelacionDTO relacionarDeduciblesCobertura(
			String accion, BigDecimal idToNegCobPaqSeccion, Long id,
			Double valorDeducible, Short claveDefault);
	
}
