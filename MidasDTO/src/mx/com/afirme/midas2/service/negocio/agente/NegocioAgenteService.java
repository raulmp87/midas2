package mx.com.afirme.midas2.service.negocio.agente;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.agente.NegocioAgente;


public interface NegocioAgenteService {
	
	/**
	 * Persistir NegocioAgente
	 * @param accion
	 * @param negocioAgente
	 */
	public void relacionarNegocioAgente(String accion, NegocioAgente negocioAgente);
	
	/**
	 * Persistir NegocioAgente
	 * @param action
	 * @param idToNegocioAgente
	 * @param idToNegocio
	 * @param idTcAgente
	 */
	public void relacionarNegocioAgente(String action, BigDecimal idToNegocioAgente, Long idToNegocio, Integer idTcAgente);

	/**
	 * Obtener listado de NegocioAgente disponibles para el negocio y filtros seleccionados (oficina, gerencia, promotoria)
	 * @param idToNegocio
	 * @param gerenciaId
	 * @param oficinaId
	 * @param promotoriaId
	 * @return
	 */
	public List<NegocioAgente> listarNegocioAgentesDisponibles(Long idToNegocio, Object gerenciaId, Object oficinaId, Object promotoriaId);
	
	/**
	 * Listar NegocioAgente asociados al negocio
	 * @param idToNegocio
	 * @return
	 */
	public List<NegocioAgente> listarNegocioAgentesAsociados(Long idToNegocio);	
	
	public List<NegocioAgente> listarNegocioAgentesAsociadosLight(Long idToNegocio);	
	
	/**
	 * Elimina todas las relaciones de agentes ligadas al negocio
	 * @param idToNegocio
	 */
	public void eliminarTodasLasRelaciones(Long idToNegocio);
	
	/**
	 * Obtiene los negocios de un agente en particular
	 * @param idToAgente
	 * @return
	 */
	public List<Negocio> listarNegociosPorAgente(Integer idToAgente);
	
	
	/**
	 * Obtiene los negocios de un agente en particular por claveNegocio
	 * @param idToAgente, claveNegocio
	 * @return
	 */	
	public List<Negocio> listarNegociosPorAgenteClaveNegocio(Integer idTcAgente, String claveNegocio);
	
	/**
	 * Lista los negocios por un agente, cve negocio y status en particular
	 * @param idToAgente
	 * @param cveTipoNegocio
	 * @param claveStatus
	 * @return
	 */
	//Comentario integracion
	public List<Negocio> listarNegociosPorAgente(Integer idToAgente, String cveTipoNegocio, Integer claveStatus, Boolean esExterno);
	
	/**
	 * Buscar la relacion negocio-agente
	 * @param Negocio
	 * @param Agente
	 * @return
	 */
	public NegocioAgente getNegocioAgente(Negocio negocio, Agente agente);

}
