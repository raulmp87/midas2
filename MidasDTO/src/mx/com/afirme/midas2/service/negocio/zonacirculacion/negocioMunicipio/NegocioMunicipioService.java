package mx.com.afirme.midas2.service.negocio.zonacirculacion.negocioMunicipio;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioDTO;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioMunicipio;
import mx.com.afirme.midas2.dto.negocio.zonacirculacion.negocioMunicipio.RelacionesNegocioMunicipio;



@Local
public interface NegocioMunicipioService {
	public RelacionesNegocioMunicipio getRelationLists(Long id);
	public Long relacionarNegocio(String accion,NegocioMunicipio negocioMunicipio);
    public String setComboNegocioEstados();
    public Long obtenerMunicipiosPorEstadoId(Long idToEstado);
    public String setFindMunicipios(Long idMunicipio);
    /**
	 * Obtiene los municipios de un estado
	 * (El id no es el id del NegocioEstado)
	 * @param idToEstado
	 * @return
	 * @autor martin
	 */
	public List<NegocioMunicipio> obtenerMunicipiosPorEstadoId(Long idToNegocio,String idToEstado);	
	public NegocioMunicipio findByNegocioAndEstadoAndMunicipio(Long idToNegocio, String stateId, String municipalityId);
	public NegocioMunicipio findByNegocioAndEstadoAndMunicipio(Negocio negocio, EstadoDTO estado, MunicipioDTO municipio);

}
 