package mx.com.afirme.midas2.service.negocio.condicionespecial;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial.NivelAplicacion;
import mx.com.afirme.midas2.domain.negocio.condicionespecial.NegocioCondicionEspecial;

/**
 * Interfaz del servicio para negocio condicion especial
 * @author Softnet01
 * @version 1.0
 * @created 07-abr.-2014 05:40:37 p. m.
 */

@Local
public interface NegocioCondicionEspecialService {



	/**
	 * Método para actualizar las condiciones especiales definiendo si aplican para
	 * externos o no. Obtiene las condiciones del negocio, itera los elementos
	 * modifica su atributo aplicaExterno a través del entidadService.update.
	 * 
	 * @param idToNegocio
	 * @param aplicaExterno
	 */
	public void aplicarExternos(BigDecimal idToNegocio, Integer aplicaExterno);

	/**
	 * Método para verificar si una condición está presente en el negocio vía su
	 * código. El metodo obtiene el listado de negocio condiciones especiales del
	 * metodo obtenerCondicionesNegocio principal usando el id negocio y el codigo de
	 * la condicion. Si la lista es diferente de vacia es que el negocio contiene la
	 * condicion especial.
	 * 
	 * @param idToNegocio
	 * @param codigo
	 */
	public boolean contieneCondicionEspecial(Long idToNegocio, String codigo);

	/**
	 * Revisar si el listado de códigos están dentro del negocio como condicion
	 * especial. Retorna un listado con los códigos que no se encontraron, es decir si
	 * la lista de retorno viene vacía es que todos pertenecen al negocio. Se obtiene
	 * el listado de condiciones por el id negocio. Se itera la lista vs el listado de
	 * codigos agregando a la lista de retorno aquellos que no hayan sido encontrados.
	 * 
	 * @param idToNegocio
	 * @param codigos
	 */
	public List<String> contieneCondicionEspecial(Long idToNegocio, List<String> codigos);

	/**
	 * Metodo para guardar o actualizar un negocio condicion especial y sus atributos.
	 * Persiste un negocio condicion especial a través de EntidadService
	 * 
	 * @param negocioCondicionEspecial
	 */
	public void guardar(NegocioCondicionEspecial negocioCondicionEspecial);

	/**
	 * Obtiene el listado de condiciones especiales obligatorias del negocio por nivel
	 * de aplicacion (Poliza, Inciso, ambos). invoca obtenerCondicionesNegocio
	 * enviando el id del negocio, el nivel aplicacion de los parametros, la
	 * obligatoriedad en true.
	 * 
	 * @param idToNegocio
	 * @param nivelAplicacion
	 */
	public List<NegocioCondicionEspecial> obtenerCondicionesBase(Long idToNegocio, NivelAplicacion nivelAplicacion);

	public List<CondicionEspecial> obtenerCondicionesEspeciales(Long idToNegocio);
	
	/**
	 * Metodo principal par a obtener condiciones ligadas al negocio a través de
	 * negociocondicionespecial que sirve de filtro. En el se puede enviar el nivel de
	 * aplicacion, el id del negocio,  el scope del usuario (interno - externo), el
	 * codigo de la condicion especial, y la obligatoriedad. Se utiliza entidadService.
	 * findByProperties para definir cada atributo siempre que sea diferente de null
	 * obteniendo el listado correspondiente de negocioCondiconEspecial.
	 * 
	 * @param filtro
	 */
	public List<NegocioCondicionEspecial> obtenerCondicionesNegocio(NegocioCondicionEspecial filtro);

	/**
	 * Sobre carga del metodo anterior que  obtiene el listado enviando null en el
	 * atributo externo del principal.
	 * 
	 * @param idToNegocio
	 * @param nivelAplicacion
	 */
	public List<NegocioCondicionEspecial> obtenerCondicionesNegocio(Long idToNegocio, NivelAplicacion nivelAplicacion);

	/**
	 * Metodo sobrecarga para obtener listado de condiciones asociadas al negocio.
	 * Envia null en el atributo externo y el enum TODOS en el atributo de
	 * nivelaplicacion del metodo principal.
	 * 
	 * @param idToNegocio
	 */
	public List<NegocioCondicionEspecial> obtenerCondicionesNegocio(Long idToNegocio);


	/**
	 * Metodo que sirve para guardar la relacion entre una condicion especial y el
	 * negocio. Utiliza executeActionGrid de entidadService que se encarga de
	 * persistir dependiendo de la accion se le envie.
	 * 
	 * @param accion
	 * @param negocioCondicionEspecial
	 */
	public void relacionar(String accion, NegocioCondicionEspecial negocioCondicionEspecial);

	/**
	 * Sobrecargado que itera la lista e invoca el metodo relacionar que recibo un
	 * negocioCondicionEspecial para realizar la persistencia.
	 * 
	 * @param accion
	 * @param condicones
	 */
	public void relacionar(String accion, List<NegocioCondicionEspecial> condiciones);

	/**
	 * Método que  regresa el listado de condiciones especiales que no son válidas
	 * para dar de alta
	 * porque no estan configuradas al negocio.
	 * Usar contieneCondicionEspecial de NegocioCondicionService para verificar que
	 * existe en el negocio.
	 * 
	 * @param cotizacion
	 * @param inciso
	 * @param condiciones
	 */
	public List<String> validaCondicionEspecialAlta(Long idToNegocio,List<String> listCodigocondicions);

	/**
	 * 
	 * Método que  regresa el listado de condiciones especiales que no son válidas
	 * para dar de baja
	 * porque estan marcadas como obligatorias.
	 * Usar contieneCondicionEspecial de NegocioCondicionService para verificar que
	 * existe en el negocio.
	 * 
	 * @param cotizacion
	 * @param inciso
	 * @param condiciones
	 */
	public List<String> validaCondicionEspecialBaja(Long idToNegocio,List<String> listCodigocondicion);
	
	/**
	 * Metodo sobrecarga para obtener condiciones especiales por id de negocio y descripcion de codigo
	 * @param idToNegocio
	 * @param codigoNombre
	 * @return
	 */
	public List<NegocioCondicionEspecial> obtenerCondicionesEspecialesDisponibles(BigDecimal idToNegocio, String codigoNombre);
	
	/**
	 * Metodo sobrecarga para obtener condiciones especiales por id de negocio y id de Seccion
	 * @param idToNegocio
	 * @param idToSeccion
	 * @return
	 */
	public List<NegocioCondicionEspecial> obtenerCondicionesEspecialesDisponibles(BigDecimal idToNegocio, BigDecimal idToSeccion);
	
	/**
	 * Metodo encargado de relacionar todas las condiciones especiales disponibles a un negocio.
	 * @param idToNegocio
	 * @param idToSeccion
	 * @param codNombre
	 */
	public void relacionarTodas( BigDecimal idToNegocio, BigDecimal idToSeccion, String codNombre, Integer aplicaExterno );
	
	/**
	 * Metodo encargado de desligar todas las condiciones especiales asociadas a un negocio.
	 * @param idToNegocio
	 * @param idToSeccion
	 * @param codigoNombre
	 * @param aplicaExterno
	 */
	public void desligarTodas(BigDecimal idToNegocio);
	
	public void saveIds( String ids );
	
	public List<String> validaCondicionEspecialAlta(List<NegocioCondicionEspecial> lNegocioCondicioneEspecial,List<String> codigos);

}