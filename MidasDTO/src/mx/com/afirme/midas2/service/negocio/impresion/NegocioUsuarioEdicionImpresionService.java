package mx.com.afirme.midas2.service.negocio.impresion;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.negocio.impresion.NegocioUsuarioEdicionImpresion;

/**
 * Interfaz del servicio para negocio condicion especial
 * @author user
 *
 */
@Local
public interface NegocioUsuarioEdicionImpresionService {

	public List<NegocioUsuarioEdicionImpresion>	obtenerUsuariosDisponibles(Long idToNegocio);
	
	public List<NegocioUsuarioEdicionImpresion> obtenerUsuariosAsociados(Long idToNegocio);
	
	public void relacionar(String accion, NegocioUsuarioEdicionImpresion negocioUsuarioImpresion);
	
	public void asociarTodas(Long idToNegocio);
	
	public void desasociarTodas(Long idToNegocio);
	
	public boolean tienePermisoImprimir(String nombreUsuario, BigDecimal idToPoliza);
	
	public boolean tienePermisoEditar(String nombreUsuario, BigDecimal idToPoliza);
}
