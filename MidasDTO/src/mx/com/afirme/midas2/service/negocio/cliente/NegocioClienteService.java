package mx.com.afirme.midas2.service.negocio.cliente;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ClienteJPA;
import mx.com.afirme.midas2.domain.negocio.cliente.NegocioCliente;


public interface NegocioClienteService {

	/**
	 * Registra la relación cliente-negocio a través de un objeto "NegocioCliente"
	 * Si la relación ya existe, lanza una excepción.
	 * @param idNegocio
	 * @param idCliente
	 * @return
	 */
	public boolean relacionarClienteNegocio(Long idNegocio,BigDecimal idCliente);
	
	public BigDecimal desasociarClienteNegocio(Long idNegocio,BigDecimal idCliente);
	
	public List<NegocioCliente> listarPorNegocio(Long idNegocio);
	
	public void desasociarClientesNegocio(Long idNegocio);
	
	public Long eliminarAsociacionesClientesYGruposNegocio(Long idNegocio);
	
	public ClienteDTO getClientePorId(BigDecimal idCliente,String nombreUsuario);
	
	public ClienteJPA getClienteJPAPorIdCliente(Long idCliente);
}
