package mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.tipouso;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUso;
import mx.com.afirme.midas2.dto.negocio.seccion.tipouso.RelacionesNegocioTipoUsoDTO;


public interface NegocioTipoUsoService{
	public RelacionesNegocioTipoUsoDTO getRelationLists(NegocioSeccion negocioSeccion);
	public void relacionarNegocioTipoUso(String accion, NegocioTipoUso negocioTipoUso);
	/**
	 * Busca el <code>NegocioTipoUso</code> default para el <code>NegocioSeccion</code> dado.
	 * @param negocioSeccion
	 * @return el <code>NegocioTipoUso</code> default o null
	 */
	public NegocioTipoUso buscarDefault(NegocioSeccion negocioSeccion);

}
