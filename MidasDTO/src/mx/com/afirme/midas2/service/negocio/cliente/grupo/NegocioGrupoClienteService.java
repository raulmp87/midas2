package mx.com.afirme.midas2.service.negocio.cliente.grupo;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.negocio.cliente.grupo.NegocioGrupoCliente;


public interface NegocioGrupoClienteService {

	/**
	 * Registra la relación grupocliente-negocio a través de un objeto "NegocioGrupoCliente"
	 * Si la relación ya existe, lanza una excepción.
	 * @param idGrupo
	 * @param idNegocio
	 * @return
	 */
	public boolean relacionarGrupoClienteNegocio(Long idGrupo,Long idNegocio);
	
	public Long desasociarGrupoClienteNegocio(Long idGrupo,Long idNegocio);
	
	public List<NegocioGrupoCliente> listarPorNegocio(Long idNegocio);
	
	public Long desasociarGruposClienteNegocio(Long idNegocio);
}
