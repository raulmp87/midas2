package mx.com.afirme.midas2.service.negocio.zonacirculacion;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioEstado;
import mx.com.afirme.midas2.dto.negocio.zonacirculacion.RelacionesNegocioZonaCirculacion;



@Local
public interface NegocioEstadoService {
    public RelacionesNegocioZonaCirculacion getRelationLists(Long idToNegocio);
    public Long relacionarNegocio(String accion, NegocioEstado negocioestado);
	public Map<Long, String> getNegocioEstadosPorNegocioId(Long idToNegocio);
	/**
	 * Obtiene un listado de los estados
	 * relacionados con un negocio
	 * @param idToNegocio
	 * @return List<NegocioEstado>
	 * @author martin
	 */
	public List<NegocioEstado> obtenerEstadosPorNegocioId(Long idToNegocio);

}
