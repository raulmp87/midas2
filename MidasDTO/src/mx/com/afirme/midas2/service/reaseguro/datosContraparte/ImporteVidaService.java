package mx.com.afirme.midas2.service.reaseguro.datosContraparte;
import java.math.BigDecimal;

import javax.ejb.Local;

@Local
public interface ImporteVidaService {
	
	String procesarInfo(BigDecimal idToControlArchivo, String tipoArchivo, String fechaCorte, String usuario, String accion, BigDecimal tipoCambio);

}
