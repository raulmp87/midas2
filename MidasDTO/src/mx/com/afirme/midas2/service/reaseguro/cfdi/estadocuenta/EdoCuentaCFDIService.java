package mx.com.afirme.midas2.service.reaseguro.cfdi.estadocuenta;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.reaseguro.cfdi.Contrato;
import mx.com.afirme.midas2.domain.reaseguro.cfdi.estadocuenta.EstadoCuenta;
import mx.com.afirme.midas2.dto.ControlDescripcionArchivoDTO;

@Local
public interface EdoCuentaCFDIService {
	
	public ControlDescripcionArchivoDTO obtenerPlantilla(Integer tipoPlantilla, Contrato contrato);
	
	public ControlDescripcionArchivoDTO procesarInfo(BigDecimal idToControlArchivo, String claveNegocio);
	
	public List<String> findReasCoas();
	
	public List<EstadoCuenta> filtrar(Contrato contrato, EstadoCuenta estadoCuenta, BigDecimal folio);
	
	public ControlDescripcionArchivoDTO imprimir(BigDecimal folio, Integer esPDF);
	
}
