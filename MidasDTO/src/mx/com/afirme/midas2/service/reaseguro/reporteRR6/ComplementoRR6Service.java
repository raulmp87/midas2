package mx.com.afirme.midas2.service.reaseguro.reporteRR6;
import java.math.BigDecimal;

import javax.ejb.Local;

@Local
public interface ComplementoRR6Service{
	
	String procesarInfo(BigDecimal idToControlArchivo, String tipoArchivo, String tipoContrato, String fechaCorte, String usuario, String accion);

}
