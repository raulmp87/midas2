package mx.com.afirme.midas2.service.reaseguro.reportesRR4;

import java.math.BigDecimal;

import javax.ejb.Local;

@Local
public interface CargaArchivosRR4Service {
	String procesarInfo(BigDecimal idToControlArchivo, String tipoArchivo, String fechaCorte, String usuario, String accion);
	
}
