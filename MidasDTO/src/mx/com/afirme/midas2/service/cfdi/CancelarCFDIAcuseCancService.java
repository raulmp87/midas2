package mx.com.afirme.midas2.service.cfdi;

import javax.ejb.Local;

@Local
public interface CancelarCFDIAcuseCancService {
	
	public void cancelarCFDI();
	
	public void initialize();
	
}
