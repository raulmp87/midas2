package mx.com.afirme.midas2.service.cfdi;

import javax.ejb.Local;

@Local
public interface CancelacionesCFDIService {
	
	void cancelDigitalBill();
	
	void initialize();
	
}
