package mx.com.afirme.midas2.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.domain.bonos.ConfigBonoRangoClaveAmis;


public interface ValidacionService {
	/**
	 * Comprueba si un porcentaje de
	 * descuento es valido
	 * @param porcentaje
	 * @param idToCotizacion
	 * @return retorna verdadero si el descuento
	 * global es mayor que el maximo permitido
	 * @author martin
	 */
	public Map<Boolean,Double> validaDescuentoGlobal(Double porcentaje,BigDecimal idToCotizacion);
	
	/**
	 * Comprueba si las fechas de vigencia
	 * son validas
	 * @param fechaInicioVigencia
	 * @param fechaFinVigencia
	 * @param idToCotizacion
	 * @return retorna verdadero si el rango de fechas no es valido
	 * @author martin
	 */
	public Boolean validaFechaCotizacion(String fechaInicioVigencia,String fechaFinVigencia,BigDecimal idToCotizacion);
	
	/**
	 * Comprueba si las fechas de vigencia
	 * son validas
	 * @param fechaInicioVigencia
	 * @param fechaFinVigencia
	 * @param idToCotizacion
	 * @return retorna Map<Boolean,Integer> el true/false y vigencia minima/maxima permitida
	 * @author martin
	 */
	public Map<Integer,Integer> validaFechaCotizacion(Date fechaInicioVigencia,Date fechaFinVigencia,BigDecimal idToCotizacion);
	
	/**
	 * Obtiene las lineas de negocio asociadas a una
	 * linea de negocio
	 * @return true si tiene mas de 1 seccion asociada
	 */
	public Boolean obtenerLineasNegocioAsociadas(BigDecimal idToPoliza);
	
	public boolean numeroAgenteValido(Long numeroAgente);
	
	public boolean validarExisteCrecimiento(Long idConfiguracion);

	/**
	 * comprueba si en una configuracion de bono existe rangos configurados de superacion de meta en el grid de Factores
	 * @param idConfigBono
	 * @return true si existen rangos configurados para la superacion de meta en el grid de factores
	 */
	public Boolean validaSupMetaXConfigBono(Long idConfigBono);
	
	public Boolean existeRangoConfigurado(Long idConfigBono);
	
	public Boolean validaTamanioCampoBonificacion(Long idConfigBono);
	
	/**
	 * Comprueba que un porcentaje de
	 * descuento por estado no exceda al max permitido
	 * @param porcentaje
	 * @param idToNegocio
	 * @param idToEstado
	 * @param idToCotizacion
	 * @param idToNegPaqueteSeccion
	 * @return retorna verdadero si el descuento
	 * por estado es mayor que el descuento por negocio-estado permitido
	 */
	public Map<Boolean, String> validaDescuentoPorEstado(Double porcentaje, Long idToNegocio, String idToEstado, BigDecimal idToCotizacion, Long idToNegPaqueteSeccion);
	/**
	 * valida que no haya rangos incluyentes en el grid de 
	 * amis en la configuracion de bonos, pestaña de exclusiones
	 * @param list
	 * @return retorna verdadero si no hay rangos incluyentes
	 */
	public Boolean validaRangosGridAmis(List<ConfigBonoRangoClaveAmis> list);
	
	
	/**
	 * Valida que una cotizacion de poliza tenga asignado un id de contratante 
	 * @param idToCotizacion
	 * @return retorna verdadero si el atributo idToPersonaContratante cuenta con un valor diferente de nulo
	 */
	public Boolean validarContratanteAsignadoCotizacion(Long idToCotizacion);
}
