package mx.com.afirme.midas2.service.excepcion;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.excepcion.CondicionExcepcion;
import mx.com.afirme.midas2.domain.excepcion.ExcepcionSuscripcion;

@Local
public interface ExcepcionSuscripcionService {
	
	public static enum NivelEvaluacion {
		TERMINAR_COTIZACION((short)0),
		EMITIR ((short)1),
		EMITIR_ENDOSO ((short)2);
		
		private short tipo;
		
		NivelEvaluacion(short tipo){
			this.tipo = tipo;
		}
		
		public short valor(){
			return this.tipo;
		}
     }
	
	public static enum TipoValor {
		VALOR_DIRECTO((short)0),
		RANGO ((short)1),
		MAYOR_IGUAL ((short)2),
		MENOR_IGUAL ((short)3);
		
		private short tipo;
		
		TipoValor(short tipo){
			this.tipo = tipo;
		}
		
		public short valor(){
			return this.tipo;
		}
     }
	
	public static enum TipoExcepcionFija{
		DESCUENTO_GLOBAL((short)1),
		RANGO_VIGENCIA((short)2),
		ANTIGUEDAD__MODELO((short)3),
		NUMERO_SERIE_REPETIDO((short)4),
		ARTICULO_140((short)5),
		IGUALACION_PRIMAS((short)6),
		ENDOSO_INCLUSION_TEXTO((short)7),
		INCLUSION_TEXTO((short)8),
		CONDICION_ESPECIAL((short)9);
		
		private short tipo;
		
		TipoExcepcionFija(short tipo){
			this.tipo = tipo;
		}
		
		public short valor() {
			return this.tipo;
		}
		
	}
	
	public static enum TipoCondicion{
		AGENTE(1),
		USUARIO(2),
		NEGOCIO(3),
		PRODUCTO(4),
		TIPO_POLIZA(5),
		SECCION(6),
		PAQUETE(7),
		COBERTURA(8),
		SUMA_ASEGURADA(9),
		DEDUCIBLE(10),
		AMIS(11),
		DESCRIPCION_ESTILO(12),
		MODELO(13),
		ESTADO(14),
		MUNICIPIO(15),
		NUM_SERIE(16),
		MODELO_ANTIGUEDAD(17),
		RANGO_VIGENCIA(18),
		DESCUENTO_GLOBAL(19),
		ARTICULO_140(20),
		IGUALACION_PRIMA(21),
		INCLUSION_TEXTO(22),
		CONDICION_ESPECIAL_INCISO(23),
		CONDICION_ESPECIAL_COTIZACION(24),
		PCT_DESCUENTO_ESTADO(25),
		EMISION_PERSONA_MORAL(26);
		
		
		private Integer condicion;
		
		TipoCondicion(Integer condicion){
			this.condicion = condicion;
		}
		
		public Integer valor(){
			return this.condicion;
		}
		
		public static TipoCondicion tipoCondicion(Integer i){
			TipoCondicion tipo = null;
			switch(i){
				case 1:
					tipo = AGENTE;
					break;
				case 2:
					tipo = USUARIO;
					break;
				case 3:
					tipo = NEGOCIO;
					break;
				case 4:
					tipo = PRODUCTO;
					break;
				case 5:
					tipo = TIPO_POLIZA;
					break;
				case 6:
					tipo = SECCION;
					break;
				case 7:
					tipo = PAQUETE;
					break;
				case 8:
					tipo = COBERTURA;
					break;
				case 9:
					tipo = SUMA_ASEGURADA;
					break;
				case 10:
					tipo = DEDUCIBLE;
					break;
				case 11:
					tipo = AMIS;
					break;
				case 12:
					tipo = DESCRIPCION_ESTILO;
					break;
				case 13:
					tipo = MODELO;
					break;
				case 14:
					tipo = ESTADO;
					break;
				case 15:
					tipo = MUNICIPIO;
					break;
				case 16:
					tipo = NUM_SERIE;
					break;
				case 17:
					tipo = MODELO_ANTIGUEDAD;
					break;
				case 18:
					tipo = RANGO_VIGENCIA;
					break;	
				case 19:
					tipo = DESCUENTO_GLOBAL;
					break;
				case 20:
					tipo = ARTICULO_140;
					break;
				case 21:
					tipo = IGUALACION_PRIMA;
					break;
				case 22:
					tipo = INCLUSION_TEXTO;
					break;
				case 23:
					tipo = CONDICION_ESPECIAL_INCISO;
					break;
				case 24:
					tipo = CONDICION_ESPECIAL_COTIZACION;
					break;
				case 25:
					tipo = PCT_DESCUENTO_ESTADO;
					break;
				case 26:
					tipo = EMISION_PERSONA_MORAL;
					break;
			}						
			return tipo;
		}
	}

	/**
	 * Regresa todas las excepciones por clave de negocio
	 * @param cveNegocio
	 * @return
	 */
	public List<ExcepcionSuscripcion> listarExcepciones(String cveNegocio);
	
	/**
	 * Regresa todas las excepciones por clave de negocio y nivel de evaluacion (0 - cotizacion, 1 - emision)
	 * @param cveNegocio
	 * @param nivelEvaluacion
	 * @return
	 */
	public List<ExcepcionSuscripcion> listarExcepciones(String cveNegocio, NivelEvaluacion nivelEvaluacion);
	
	/**
	 * Lista excepcion por filtro de negocio y habilitadas. Solo devuelve las excepciones con identificador mayor a 100 que son las configurables.
	 * @param filtro
	 * @return
	 */
	public List<ExcepcionSuscripcion> listarExcepciones(ExcepcionSuscripcion filtro);
	
	public ExcepcionSuscripcion listarExcepcion(Long idToExcepion);
	
	public Long guardarExcepcion(ExcepcionSuscripcion excepcionSuscripcion);
	
	public void removerValoresDeCondicion(Long idToExcepcion, TipoCondicion tipoCondicion);
	
	public void removerExcepcion(Long idToExcepcion);
	
	public void removerCondicion(Long idToExcepcion, TipoCondicion tipoCondicion);
	
	public void removerCondicion(Long idToExcepcion, Long idToCondicion);
	
	public void agregarCondicion(Long idToExcepcion, TipoCondicion tipoCondicion, TipoValor tipoValor, List<String> valores);
	
	public CondicionExcepcion obtenerCondicion(Long idToExcepcion, TipoCondicion tipoCondicion);
	
	/**
	 * Obtener condiciones de una excepcion
	 * @param idToNegocio
	 * @return
	 */
	public List<CondicionExcepcion> obtenerCondiciones(Long idExcepcion);

}
