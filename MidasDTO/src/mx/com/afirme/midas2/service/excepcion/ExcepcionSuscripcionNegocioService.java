package mx.com.afirme.midas2.service.excepcion;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionNegocioDescripcionDTO;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService.NivelEvaluacion;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService.TipoCondicion;


public interface ExcepcionSuscripcionNegocioService {
	
	/**
	 * Obtiene la lista de excepciones guardadas por clave de negocio
	 * @param cveNegocio
	 * @return
	 */
	public List<? extends ExcepcionSuscripcionNegocioDescripcionDTO> listarExcepciones(String cveNegocio);
	
	/**
	 * Obtiene la excepcion por id
	 * @param idToExcepcion
	 * @return
	 */
	public ExcepcionSuscripcionNegocioDescripcionDTO obtenerExcepcion(Long idToExcepcion);
	
	/**
	 * Salva una nueva excepcion
	 * @param excepcion
	 * @return
	 */
	public Long salvarExcepcion(ExcepcionSuscripcionNegocioDescripcionDTO excepcion);
	
	/**
	 * Agrega una condicion a la excepcion con valor simple
	 * @param idToExcepcion
	 * @param propiedad
	 * @param valor
	 */
	public void agregarCondicion(Long idToExcepcion, TipoCondicion tipoCondicion, String valor);
	
	/**
	 * Agrega una serie de valores, dependiendo de <code>tipoValor</code> a la excepcion
	 * @param idToExcepcion
	 * @param propiedad
	 * @param tipoValor
	 * @param valor
	 */
	public void agregarCondicion(Long idToExcepcion, TipoCondicion tipoCondicion, ExcepcionSuscripcionService.TipoValor tipoValor, String...valor);
	
	/**
	 * Obtiene la lista de valores de la propiedad para la excepcion
	 * @param idToExcepcion
	 * @param propiedad
	 * @return
	 */
	public List<String> obtenerCondicionValores(Long idToExcepcion, TipoCondicion tipoCondicion);
	
	/**
	 * Obtiene el primer valor de la propiedad para la excepcion
	 * @param idToExcepcion
	 * @param propiedad
	 * @return
	 */
	public String obtenerCondicionValor(Long idToExcepcion, TipoCondicion tipoCondicion);
	
	/**
	 * Eliminar excepcion con id <code>idToExcepcion</code>
	 * @param idToExcepcion
	 */
	public void eliminarExcepcion(Long idToExcepcion);
	
	/**
	 * Obtiene el tipo de valores de la condicion seleccionada
	 * @param idToExcepcion
	 * @param propiedad
	 * @return
	 */
	public short obtenerTipoCondicionValor(Long idToExcepcion, TipoCondicion tipoCondicion);
	
	
	/**
	 * Evaluar cotizacion contra excepciones. Retorna una lista de reportes con las excepciones encontradas.
	 * @param cotizacion
	 * @return
	 */
	public List<? extends ExcepcionSuscripcionReporteDTO> evaluarCotizacion(BigDecimal idCotizacion);
	
	/**
	 * Evaluar cotizacion contra excepcion. Usa el nivel de evaluacion, 0 - al terminar la cotizacion, 1 - al intentar emitir.
	 * Retorna una lista de reportes con las excepciones encontradas.
	 * @param idToCotizacion
	 * @param nivelEvaluacion
	 * @return
	 */
	public List<? extends ExcepcionSuscripcionReporteDTO> evaluarCotizacion(BigDecimal idToCotizacion, NivelEvaluacion nivelEvaluacion);
	
	public List<? extends ExcepcionSuscripcionReporteDTO> evaluarCotizacion(CotizacionDTO cotizacion, NivelEvaluacion nivelEvaluacion);
	
	/**
	 * Evaluar cotizacion contra excepciones. Retorna una lista de reportes con las excepciones encontradas.
	 * @param cotizacion
	 * @return
	 */
	public List<? extends ExcepcionSuscripcionReporteDTO> evaluarCotizacion(BigDecimal idCotizacion, BigDecimal idLineaNegocio, BigDecimal idInciso);

	public String generarCuerpoMensajeNotificacion(ExcepcionSuscripcionReporteDTO reporte);
	
	public void enviarNotificacion(String recipientes, String titulo, 
			String encabezado, String saludo, String cuerpo );
	
}
