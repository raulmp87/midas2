package mx.com.afirme.midas2.service.parametros;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;

@Local
public interface ParametroGeneralService {
	
	public ParametroGeneralDTO findById(ParametroGeneralId id); 
	public List<ParametroGeneralDTO> listAll();
	public List<ParametroGeneralDTO> listByGroup(String groupName);
	public void save(ParametroGeneralDTO parametroGeneral);
	
	/**
	 * If you defined group name and param name as business keys, use this method to find the parameter needed
	 * @param groupName Name of registered group
	 * @param paramName Name of registered parameter
	 * @return 
	 */
	public ParametroGeneralDTO findByDescCode(String groupName, String paramName);

}
