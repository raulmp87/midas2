package mx.com.afirme.midas2.service.siniestros.recuperacion;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.CoberturaRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Ingreso;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.ReferenciaBancaria;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.ReferenciaBancariaDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.RecuperacionDTO;


/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 19-may-2015 12:37:18 p.m.
 */
@Local
public interface RecuperacionService {

	/**
	 * <b>SE AGREGA EN DISE�O DE LISTAR-BUSCAR-EXPORTAR RECUPERACIONES</b>
	 * 
	 * Invocar al m�todo buscarRecuperaciones de RecuperacionDao y regresar el
	 * resultado.
	 * Iterar el listado y llenar los siguientes atributos:
	 * 
	 * nombreUsuario: si la recuperacion es de origen MANUAL entonces se buscara el
	 * nombre de usuario mediante usuarioService; si la recuperacion es de origen
	 * AUTOMATICO entonces se asignar� el valor del atributo adminRefacciones.
	 * 
	 * @param filtro
	 */
	public List<RecuperacionDTO> buscarRecuperaciones(RecuperacionDTO filtro);

	/**
	 * <b>SE AGREGA EN DISE�O CANCELAR RECUPERACION PROVEEDOR</b>
	 * <b>
	 * </b>M�todo que cancela una Recuperaci�n de
	 * 
	 * Buscar la Recuperacion <b>recuperacion </b>mediante entidadService.findById
	 * 
	 * Cambiar el <b>estatus </b>de la <b>recuperacion </b>a CANCELADO, asignar el
	 * <b>motivoCancelacion </b>y la <b>fechaCancelacion.</b>
	 * <b>
	 * </b>Invocar a BitacoraService.registrar(TIPO_BITACORA.RECUPERACION, EVENTO.
	 * CANCELAR_RECUPERACION, recuperacionId, "Se <font color="#0f0f0f">cancela la
	 * recuperacion:" + recuperacionId, recuperacion, </font><font
	 * color="#0f0f0f">usuarioService.getUsuarioActual<b>()</b>.
	 * getNombreUsuario<b>()</b></font><font color="#0f0f0f">)</font>
	 * 
	 * @param recuperacionId
	 */
	public void cancelarRecuperacion(Long recuperacionId, String motivoCancelacion);

	/**
	 * M�todo que genera un Ingreso en base a una Recuperacion
	 * 
	 * Crear un nuevo objeto Ingreso ingreso, asociar la Recuperacion y el estatus
	 * ser� PENDIENTE, asignar la fecha de creaci�n y el codigo del usuario creaci�n y
	 * hacer un entidadService.save
	 * 
	 * @param recuperacion
	 */
	public Ingreso generarIngreso(Recuperacion recuperacion);

	/**
	 * M�todo que asocia Coberturas a una Recuperaci�n .
	 * 
	 * Buscar la recuperacion por medio de entidadService.findById sobre Recuperacion
	 * y asignarlo a una variablle <b>recuperacion</b>
	 * 
	 * Hacer un split por comas sobre <b>coberturasConcat </b>y asignarlo a un array
	 * String[] <b>coberturas</b>.
	 * 
	 * Iterar <b>coberturas </b>y hacer un split por pipe y asignar el resultado a un
	 * array String[] <b>cobertura</b>.
	 * 
	 * El primer elemento de <b>cobertura[0] </b>ser� el id de CoberturaReporteCabina,
	 * hacer un entidadService.findById y asignarlo a una variable
	 * CoberturaReporteCabina <b>coberturaReporte</b>.
	 * 
	 * El segundo elemento ser� la clave subcalculo, si contiene un 0 hacer caso omiso
	 * de lo contrario asignar a una variable <b>claveSubCalculo</b>.
	 * 
	 * Crear un nuevo objeto CoberturaRecuperacion coberturaRecuperacion y asociar la
	 * <b>recuperacion</b>, la <b>coberturaReporte </b>y  la <b>claveSubcalculo </b>y
	 * hacer un entidadService.save
	 * 
	 * @param recuperacionId
	 * @param coberturasConcat
	 */
	public void guardarCoberturasRecuperacion(Long recuperacionId, String coberturasConcat);
	
	/***
	 * Guardar la cobertura relacionada a la recuperacion
	 * @param recuperacion
	 * @param estimacion
	 */
	public void guardarCoberturasRecuperacion(Recuperacion recuperacion, EstimacionCoberturaReporteCabina estimacion);

	/**
	 * M�todo que asocia Pases de Atenci�n a una Recuperaci�n .
	 * 
	 * Buscar la recuperacion por medio de entidadService.findById sobre Recuperacion
	 * y asignarlo a una variablle <b>recuperacion</b>
	 * 
	 * Hacer un split por comas sobre <b>pasesConcat </b>y asignarlo a un array Long[]
	 * <b>pases</b>.
	 * 
	 * Iterar <b>pases</b> que conteendr� el id de EstimacionCoberturaReporteCabina,
	 * hacer un entidadService.findById y asignarlo a una variable
	 * EstimacionCoberturaReporteCabina <b>pase</b>.
	 * 
	 * Hacer un entidadService.findByProperties sobre CoberturaRecuperacion buscando
	 * por el id de la CoberturaReporteCabina y la claveSubCalculo en caso de aplicar
	 * y asignar el pase a la cobertura correspondiente
	 * 
	 * <font color="#408080">Map<String,String> params = new HashMap<String.
	 * String></font>
	 * <font color="#408080">
	 * </font><font color="#408080">params.put("coberturaReporte.id", pase.
	 * getCoberturaReporteCabina().getId());</font>
	 * <font color="#408080">params.put("recuperacion.id", recuperacionId);</font>
	 * <font color="#408080">
	 * </font><font color="#408080">if (pase.getCveSubCalculo() != null) {</font>
	 * <font color="#408080">	params.put("claveSubCalculo", pase.getCveSubCalculo() );
	 * </font>
	 * <font color="#408080">}</font>
	 * <font color="#408080">
	 * </font><font color="#408080">List<CoberturaRecuperacion> coberturas
	 * =entidadService.findByProperties(CoberturaRecuperacion .class, params);</font>
	 * <font color="#408080">
	 * </font><font color="#408080">if (coberturas != null && !coberturas.isEmpty())
	 * {</font>
	 * <font color="#408080">	CoberturaRecuperacion cobertura = coberturas.get(0);
	 * </font>
	 * <font color="#408080">	</font>
	 * <font color="#408080">
	 * </font><font color="#408080">	cobertura .getPasesAtencion().add(pase);</font>
	 * <font color="#408080">	entidadService.save(cobertura);</font>
	 * <font color="#408080">}</font>
	 * 
	 * @param idRecuperacion
	 * @param pasesConcat
	 */
	public void guardarPasesRecuperacion(Long idRecuperacion, String pasesConcat);

	/**
	 * Guarda una estimacion como pase de atencion
	 * @param idRecueracion
	 * @param estimacion
	 */
	public void guardarPasesRecuperacion(Long idRecueracion, EstimacionCoberturaReporteCabina estimacion);
	
	/**
	 * M�todo que asocia referencias Bancarias a una Recuperaci�n.
	 * 
	 * Buscar la recuperacion mediante entidadService.findById sobre Recuperacion y
	 * ponerlo en la variable <b>recuperacion</b>
	 * 
	 * Iterar el listado de <b>referencias </b>y por cada uno asociar el objeto
	 * <b>recuperacion</b>, y copiar el <b>medio </b>que contenga la
	 * <b>recuperacion</b>
	 * 
	 * hacer un entidadService.save al elemento.
	 * 
	 * @param recuperacionId
	 * @param referencias
	 */
	public void guardarReferenciaBancaria(Long recuperacionId, List<ReferenciaBancaria> referencias);

	/**
	 * M�todo que regresa una cadena de ids concatenados por coma de la Cobertuas
	 * asociadas  a la Recuperaci�n.
	 * 
	 * Declarar una variable String <b>coberturasConcat</b>.
	 * 
	 * Hacer un entidadService.findByProperties sobre CoberturaRecuperacion mediante
	 * el <b>recuperacionId </b>que se recibi� de par�metro y asignarlo a una variable
	 * List<CoberturaRecuperacion> <b>lstCoberturas</b>..
	 * 
	 * Iterar <b>lstCoberturas</b>y concatenar por comas en la variable
	 * <b>coberturasConcat; </b>el id se debe conformar por el di de la
	 * CoberturaReporteCabina un pipe y la clave subcalculo en caso de aplicar,si no
	 * contiene clave subcalculo agregar un 0
	 * Ejem: 150|RCV
	 *          220|0
	 * 
	 * @param recuperacionId
	 */
	public Map<String,String>  obtenerCoberturasRecuperacion(Long recuperacionId);
	
	public List<CoberturaRecuperacion>  getCoberturasRecuperacion(Long recuperacionId);

	/**
	 * M�todo que retorna un mapa con las coberturas afectadas.
	 * 
	 * Inicializar un Map<String,String> <b>coberturas</b>.
	 * 
	 * <font color="#0f0f0f">Invocar al m�todo estimacionService.
	 * obtenerCoberturasAfectacion(reporteId, null, null, null, true) que nos retorne
	 * las coberturas que fueron afectadas en el Reporte y asignarlo a una variable
	 * List<AfectacionCoberturaSiniestroDTO > <b>lstCoberturas</b>.</font>
	 * <font color="#009ce8"><b>
	 * </b></font><font color="#0f0f0f">Iterar  y asignar la cobertura al mapa de
	 * <b>coberturas</b>; el id ser� el id de la CoberturaReporteCabina concatenando
	 * co un pipe y la claveSubcalculo, si esta es nula concatenar un 0. Ejemplo:
	 * </font>
	 * <font color="#408080">
	 * </font><font color="#408080">for (AfectacionCoberturaSiniestroDTO cobertura:
	 * lstCoberturas) {</font>
	 * <font color="#408080">	String id = cobertura.getCoberturaReporteCabina().
	 * getId() + '|' + cobertura.getConfiguracionCalculoCobertura.
	 * getCveSubTipoCalculoCobertura() != null ? cobertura.
	 * getConfiguracionCalculoCobertura.getCveSubTipoCalculoCobertura() : 0</font>
	 * <font color="#408080">
	 * </font><font color="#408080">	coberturas.put(id, cobertura.
	 * getNombreCobertura());</font>
	 * <font color="#408080">}</font>
	 * <font color="#408080">
	 * </font><font color="#408080">150|RCV</font>
	 * <font color="#408080">230|0</font>
	 * 
	 * @param reporteId
	 */
	public Map<String,String> obtenerCoberturasSiniestro(Long reporteId);

	/**
	 * M�todo que obtiene el listado de Pases de un Siniestro en base a un filtro de
	 * Coberturas.
	 * 
	 * Declaran un mapa Map<String,String> <b>pases </b>que ser� el objeto a retornar.
	 * 
	 * 
	 * En un array de <b>coberturas </b>almacenar los ids de las coberturas haciendo
	 * un split sobre el parametro <b>coberturasConcat</b>.
	 * 
	 * Iterar coberturas y  en un array cobertura hacer un split sobre el objeto, el
	 * cual se debe descomponer en un id y una claveSubCalculo en caso de aplicar; si
	 * contiene un 0 no tomar en cuenta.
	 * 
	 * Ejemplo: 151|RCV, 152|0, 153|RCP, 154|0
	 * 
	 * Hacer un entidadService.findByProperties sobre EstimacionCoberturaReporteCabina,
	 * usando como parametros el <b>id </b>de la Cobertura y la <b>claveSubcalculo
	 * </b>si la coneitne
	 * 
	 * Map<String,String> params = new HashMap<String,String>
	 * 
	 * params.put("coberturaReporteCabina.id", cobertura[0]);
	 * 
	 * if (cobertura[1] != null) {
	 * 	params.put("cveSubCalculo", cobertura[1]);
	 * }
	 * 
	 * List<EstimacionCoberturaReporteCabina> lstPases = entidadService.
	 * findByProperties(EstimacionCoberturaReporteCabina, params);
	 * 
	 * 
	 * Si el listado <b>lstPases </b>no esta vac�o, entonces iterar el listado y
	 * agregar al mapa de <b>pases </b>el <b>id </b>y el <b>folio </b>del pase de
	 * atencion
	 * 
	 * @param coberturasConcat
	 */
	public Map<String,String> obtenerPasesCoberturaSiniestro(String coberturasConcat);

	/**
	 * M�todo que regresa una cadena de ids concatenados por coma de los Pases de
	 * Atenci�n asociados a la Recuperaci�n.
	 * 
	 * Declarar una variable String <b>pasesConcat</b>.
	 * 
	 * Hacer un entidadService.findByProperties sobre CoberturaRecuperacion mediante
	 * el <b>recuperacionId </b>que se recibi� de par�metro y asignarlo a una variable
	 * List<CoberturaRecuperacion> <b>lstCoberturas</b>..
	 * 
	 * Iterar el <b>lstCoberturas </b>y por cada uno accesar a su atributo
	 * <b>pasesAtencion </b>y asignarlo a una variable
	 * List<EStimacionCoberturaReporteCabina> <b>lstPases</b>.
	 * 
	 * Iterar <b>lstPases </b>y concatenar por comas en la variable <b>pasesConcat</b>
	 * 
	 * @param recuperacionId
	 */
	public Map<Long, String>  obtenerPasesRecuperacion(Long recuperacionId);
	
	public List<EstimacionCoberturaReporteCabina>  getPasesRecuperacion(Long recuperacionId);
	

	
	

	/**
	 * SE AGREGA EN DISEÑO DE REFERENCIAS BANCARIAS
	 * 
	 * Método que retorna las Cuentas Bancarias asociadas a Ingresos.
	 * 
	 * Invocar el método recuperacionDao.obtenerCuentasBancarias
	 */
	public List<ReferenciaBancariaDTO> obtenerCuentasBancarias();


	/**
	 * SE AGREGA EN DISEÑO DE REFERENCIAS BANCARIAS
	 * 
	 * Método que regresa el listado de Referencias Bancarias asociadas a la
	 * Recuperación.
	 * 
	 * Hacer un entidadService.findById sobre Recuperacion usando el <b>recuperacionId
	 * </b>y asignarlo a una variable <b>recuperacion</b>
	 * 
	 * Hacer un entidadService.findByProperties sobre ReferenciaBancaria usando como
	 * parámetros el <b>recuperacionId </b>y el <b>recuperacion.getMedioRecuperacion
	 * </b>, esto nos traerá la lista de las referencias asociadas a cierta
	 * Recuperación y de un medio en específico.
	 * 
	 * Iterar este listado e ir creando un nuevo objeto dto ReferenciaBancariaDTO,
	 * copiar los atributos de referencia, bancoMidas, clabe, cuentaBancaria,
	 * numeroReferencia
	 * 
	 * @param recuperacionId
	 */
	public List<ReferenciaBancariaDTO> obtenerReferenciasBancaria(Long recuperacionId, String medioRecuperacion);
	
	/**
	 * Obtener una recuperacion que extiende de Recuperacion para el Id dado 
	 * @param id
	 * @return
	 */
	public <R extends Recuperacion>  R obtenerRecuperacion(Class<R> tipo, Long id);
	
	
	public void generarReferenciaBancaria(Long recuperacionId);
	
	public List<ReferenciaBancariaDTO> obtenerReferenciasBancaria(Long recuperacionId);
	
	public void eliminarReferenciasBancariasByRecuperacionId(Long recuperacionId);
	
	public Ingreso obtenerIngresoActivo(Long recuperacionId);
	
	public void invocaProvision(Long idRecuperacion);
	
	/**
	 * Metodo para generar el instructivo de deposito para la notificacion de las recuperaciones
	 * @param idRecuperacion
	 * @return
	 */
	public TransporteImpresionDTO imprimirInstructivoDepositoRecuperacion(Long idRecuperacion);
	
	/**
	 * Lista de Recuperaciones que cuentan con referencias bancarias ligadas a un reporte cabina
	 * @param idReporteCabina
	 * @return
	 */
	public List<RecuperacionDTO> buscarReferenciasBancarias(Long idReporteCabina);
}