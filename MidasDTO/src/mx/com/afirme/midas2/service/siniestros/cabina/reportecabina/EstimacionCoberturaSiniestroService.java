package mx.com.afirme.midas2.service.siniestros.cabina.reportecabina;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ConfiguracionCalculoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.juridico.ReclamacionReservaJuridicoDTO;
import mx.com.afirme.midas2.dto.siniestros.AfectacionCoberturaSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.EstimacionCoberturaSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.PaseAtencionExportacionDTO;
import mx.com.afirme.midas2.dto.siniestros.PaseAtencionImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.PaseAtencionSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.depuracion.CoberturaSeccionSiniestroDTO;

@Local
public interface EstimacionCoberturaSiniestroService {
	
	public static final int	TIPO_DEDUCIBLE_DSMVGDF	= 11;
	public static final int	TIPO_DEDUCIBLE_VALOR	= 2;
	public static final int	TIPO_DEDUCIBLE_PORCENTAJE	= 1;
	
	
	public List<PaseAtencionSiniestroDTO> buscarPasesAtencion(PaseAtencionSiniestroDTO filtro);
	
	/**
	 * 
	 * Método que obtiene el listado de coberturas que corresponden a la configuración seleccionada de Tipo Siniestro, Termino Ajuste y Tipo de Responsabilidad en la pantalla de afectación del inciso.
	* @param reporteCabinaId
	 * @param tipoSiniestro
	 * @param terminoAjuste
	 * @param tipoResponsabilidad
	 * @return
	 */
	public List<AfectacionCoberturaSiniestroDTO> obtenerCoberturasAfectacion(Long reporteCabinaId, String tipoSiniestro,
			String terminoAjuste, String tipoResponsabilidad);
	
	public List<AfectacionCoberturaSiniestroDTO> obtenerCoberturasAfectacion(Long reporteCabinaId, String tipoSiniestro, 
			String terminoAjuste,String tipoResponsabilidad, Boolean soloAfectadas, Boolean soloAfectables);
	
	public List<AfectacionCoberturaSiniestroDTO> obtenerCoberturasAfectacion(Long reporteCabinaId);
	

	/**
	 * 
	 * Método que regresa el listado de coberturas asociadas a un reporte de cabina. Utilizar entidadService.findByProperties y como parámetro enviar el idReporteCabina
	 * @param idReporteCabina
	 * @return
	 */
	public List<CoberturaReporteCabina> obtenerCoberturasReporte(Long reporteCabinaId);
	
	
	
	public EstimacionCoberturaSiniestroDTO obtenerDatosEstimacionCobertura(Long idCoberturaRepCabina, String tipoCalculo, String tipoEstimacion);
	
	/**
	 * Método que obtiene el listado de Pases de Atención de una Cobertura.
	 * @param idCoberturaReporteCabina
	 * @return
	 */
	public List<PaseAtencionSiniestroDTO> obtenerListadoPases (Long idCoberturaReporteCabina,String tipoEstimacion );
	
	
	public EstimacionCoberturaSiniestroDTO obtenerDatosEstimacionCoberturaNueva(Long idCoberturaRepCabina, String tipoEstimacion);

	public EstimacionCoberturaReporteCabina generarFolio(ReporteCabina reporteCabina,EstimacionCoberturaReporteCabina estimacionCobertura);

	public List<String> guardarEstimacionCobertura (EstimacionCoberturaSiniestroDTO estimacionCobertura, Long idCoberturaReporte, String tipoEstimacion);
	
	
	

	
	
	
	
	/**
	 * Método que valida la estimación actual de la cobertura.
	 * @param estimacionCoberturaDTO
	 * @param idCoberturaReporte
	 * @return
	 */
	public List<String> validarEstimacionCobertura(EstimacionCoberturaSiniestroDTO estimacionCoberturaDTO, Long idCoberturaReporte,EstimacionCoberturaReporteCabina estimacion);
	
	public EstimacionCoberturaSiniestroDTO obtenerDatoEstimacionCobertura(EstimacionCoberturaReporteCabina estimacion);
	
	public String descripcionMoneda(BigDecimal idMoneda);

	
	/**
	 * Metodo que obtiene el listado de coberturas que pertenecen a una seccion.
	 * @param idToSeccion id de la Seccion relacionada a las coberturas
	 * @return
	 */
	public List<CoberturaSeccionSiniestroDTO> obtenerCoberturasPorSeccion( Long idToSeccion );
	


	
	public List<PaseAtencionSiniestroDTO> obtenerDesgloseCoberturas(Long idReporteCabina );
	
	
	/**
	 * 
	 * @param idPaseAtencion
	 * @return
	 */
	public TransporteImpresionDTO imprimirPaseAtencion( Long idPaseAtencion );
	
	/**
	 * 
	 * @param estimacion
	 * @return
	 */
	public PaseAtencionSiniestroDTO cargarPaseAtencion( EstimacionCoberturaReporteCabina estimacion );
	
	/**
	 * 
	 * @param idEstimacionCoberturaReporteCabina
	 * @return
	 */
	public EstimacionCoberturaReporteCabina obtenerEstimacionCoberturaReporteCabina( Long idEstimacionCoberturaReporteCabina );
	
	/**
	 * 
	 * @param estimacion
	 * @return
	 */
	public PaseAtencionImpresionDTO cargarPaseAtencionImpresion( EstimacionCoberturaReporteCabina estimacion );
	
	
	/**
	 * 
	 * @param estimaciones
	 * @return
	 */
	public List<PaseAtencionExportacionDTO> obtenerPaseAtencionExportacion( PaseAtencionSiniestroDTO filtro );

	/**
	 * 
	 * @param dto
	 * @param auto
	 */
	public void cargaInformacionAutoInciso(EstimacionCoberturaSiniestroDTO dto, AutoIncisoReporteCabina auto);
	


	public EstimacionCoberturaReporteCabina seleccionaObjetoPorTipoEstimacion( String tipoEstimacion);
	

	/**
	 * Valida si el reporte tiene alguna cobertura afectada
	 * @param reporteCabinaId
	 * @return
	 */
	public Boolean contieneCoberturasAfectadas(Long reporteCabinaId);
	
	/**
	 * Valida si el reporte cuenta con coberturas contratadas o alguna clave tipo calculo
	 * @param reporteCabinaId
	 * @param cveTipoCalculo
	 * @return
	 */
	public Boolean contieneCoberturasContratadas(Long reporteCabinaId, String cveTipoCalculo);
	
	/**
	 * Valida si las coberturas afectadas existentes estan dentro de una nueva configuracion de coberturas
	 * @param reporteCabinaId
	 * @param causaSiniestro
	 * @param tipoResponsabilidad
	 * @param terminoAjuste
	 * @return
	 */
	public List<String> validaCambioEnCoberturasPorCausaTipoTermino(Long reporteCabinaId, 
			String causaSiniestro, String tipoResponsabilidad, String terminoAjuste);
	

	

	public EstimacionCoberturaReporteCabina selectEstimacionCoberturaReporteCabina(EstimacionCoberturaSiniestroDTO dto, String tipoEstimacion);
	

	/**
	 * Valida si la estimacion esta siniestrada.
	 * @param idReporteCabina
	 * @param idEstimacionCobertura
	 * @return Boolean
	 */
	public Boolean esSiniestrada(Long idReporteCabina, Long idEstimacionCobertura);
	
	public ConfiguracionCalculoCoberturaSiniestro obtenerConfiguracionCalcCobertura(String claveTipoCalculo, String claveSubTipoCalculo,
			String tipoEstimacion, String tipoConfiguracion);
	
	public String obtenerNombreCobertura(CoberturaDTO cobertura, String claveTipoCalculo, String claveSubTipoCalculo,
			String tipoEstimacion, String tipoConfiguracion);
	
	@Deprecated
	public List<CatValorFijo> obtenerTipoDePasePorTipoEstimacion(String codigoTipoEstimacion,
			String codigoTipoResponsabilidad);
	
	public List<CatValorFijo> obtenerTipoDePasePorTipoEstimacion(String codigoTipoEstimacion,
			String codigoTipoResponsabilidad, String codigoCausaSiniestro, String codigoTerminoAjuste);
	
	public List<CatValorFijo> obtenerTipoDePaseMenosSoloRegistro();
	
	public void actualizaCampoEsEditableEnLasEstimaciones(Long reporteCabinaId, 
			String causaSiniestro, String tipoResponsabilidad, String terminoAjuste);
	
	public Boolean tieneNumeroDeSerieSiniestrosAnteriores(Long estimacionId, String noSerie);
	
	public void notificaSolicitudSiniestroAntiguo(EstimacionCoberturaSiniestroDTO estimacionCoberturaSiniestro, Long keyReporteCabina);

	public List<ReclamacionReservaJuridicoDTO> obtenerReservaCoberturaJuridico(Long idReporte);
	

	/**
	 * Método que obtiene la suma asegurada para RCViajero dado los días de salario mínimo.
	 * @param diasSalarioMinimo
	 * @return
	 */
	public BigDecimal calculoSumaAseguradaDSMVGDF(Integer diasSalarioMinimo);
	
	
	/**
	 * 
	 * Método que obtiene el deducible dado los días de salario mínimo.
	 * @param deducible
	 * @return
	 */
	public BigDecimal calculoDeducibleDSMVGDF(BigDecimal deducible);
	

	public EstimacionCoberturaReporteCabina guardarEstimacion(EstimacionCoberturaReporteCabina estimacion);
	
	public MovimientoCoberturaSiniestro guardarMovimiento(Long estimacionId, BigDecimal importe, String causaMovimiento);
	
	public Boolean esPerdidaTotal(Long estimacionId);
	
	public Boolean permiteCambiarDeducible(Long estimacionId);
	
	public void guardarCoberturaMontoRechazo(List<AfectacionCoberturaSiniestroDTO> listAfectacionCoberturaSiniestroDTO, Long idReporteCabina);

	public Double obtieneValorSumaAsegurada (CoberturaReporteCabina coberturaReporteCabina, AutoIncisoReporteCabina autoInciso);
	

}