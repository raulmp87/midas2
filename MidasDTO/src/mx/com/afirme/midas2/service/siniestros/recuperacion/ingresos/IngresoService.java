package mx.com.afirme.midas2.service.siniestros.recuperacion.ingresos;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.pagos.OrdenPagoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Ingreso;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza.AplicacionCobranza;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza.AplicacionCobranza.TipoAplicacionCobranza;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza.MovimientoCuentaAcreedor;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.ingresos.IngresoDevolucion;
import mx.com.afirme.midas2.dto.siniestros.ListarIngresoDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos.DepositoBancarioDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos.ImportesIngresosDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos.MovimientoAcreedorDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos.MovimientoManualDTO;
import mx.com.afirme.midas2.util.MidasException;
@Local
public interface IngresoService {

	/**
	 * Invocar al método buscarDepositosBancarios de IngresoDao y regresar el resultado.
	 * @param filtro
	 * @return
	 */
	public List<DepositoBancarioDTO> buscarDepositosBancarios(DepositoBancarioDTO filtro, String depositosConcat, Boolean esConsulta);

	/**
	 * Busca los depositos cuyo id esta incluido en el String concatenado.
	 * @param depositosConcat
	 * @return
	 */
	public List<DepositoBancarioDTO> buscarDepositosBancarios(String depositosConcat, Boolean esConsulta);

	/**
	 * Realiza una busqueda de ingresos utilizando filtros y le agrega los ingresos seleccionados 
	 * @param filtro
	 * @return
	 */
	public List<ListarIngresoDTO> buscarIngresos(ListarIngresoDTO filtro, String ingresosConcat);

	/**
	 * Busca unicamente los ingresos seleccionados
	 * @param ingresosConcat
	 * @return
	 */
	public List<ListarIngresoDTO> buscarIngresos(String ingresosConcat);

	/**
	 * Cancela ingreso pendiente por aplicar
	 * @param idIngreso
	 * @return
	 */
	public String cancelarIngresoPendientePorAplicar(Ingreso ingreso, String motivo);


	public void cancelarIngresoPendienteRecuperacionRegistrada(Long idIngreso,String motivo);


	///////


	/*    private LiquidacionSiniestroService liquidacionSiniestroService;
      public IngresoDao m_IngresoDao;
      public CuentaAcreedoraDTO m_CuentaAcreedoraDTO;
	 */


	/**
	 * <b>SE AGREGA EN DISE?O DE APLICAR INGRESO MANUAL</b>
	 * <b>
	 * </b>
	 * Hacer un entidadService.findById sobre MovimientoCuentaAcreedor con el
	 * <b>idMovto</b>.
	 * 
	 * Invocar al m?todo generarMovimientoAcreedor enviando la <b>aplicacionOriginal
	 * </b>y <b>aplicacionCancelacionId </b>del <b>movimiento</b>, enviar la
	 * <b>aplicacionMovimientoId </b>y el <b>monto </b>a usar.
	 * 
	 * @param idMovto
	 * @param monto
	 * @param aplicacionMovtoId
	 */
	public void cargoMovimientoAcreedor(Long idMovto, BigDecimal monto, Long aplicacionMovtoId);

	public void abonoMovimientoAcreedor(Long idMovto, BigDecimal monto, Long aplicacionMovtoId);

	/**
	 * <b>SE AGREGA EN DISE?O DE APLICAR INGRESO MANUAL</b>
	 * <b>
	 * </b>Invocar al m?todo aplicaRefunic de ingresoDao
	 * 
	 * @param refunic
	 * @param monto
	 * @param descripcion
	 */
	public void aplicaRefunic(String refunic, BigDecimal monto, String descripcion) throws MidasException;

	/**
	 * <b>SE AGREGA EN DISE?O APLICAR INGRESO MANUAL</b>
	 * M?todo que genera un Movimiento sobre MovimientoCuentaAcreedor
	 * Crear un objeto <b>movimiento </b>de tipo MovimientoCuentaAcreedor , asociar la
	 * <b>aplicacionOriginal</b>, <b>aplicacionCancelacion </b>y
	 * <b>aplicacionMovimiento</b>, la <b>cuenta </b>(buscar sobre ConceptoGuias con
	 * entidadService.findById) y el <b>monto</b>
	 * Hacer un entidadService.findByPropertiesWithOrder sobre
	 * MovimientoCuentaAcreedor usando la <b>aplicacionOriginal </b>y ordenar por
	 * fecha, Si existen movimientos se deben cambiar todos a INACTIVO, adem?s se debe
	 * obtener el ?ltimo movimiento para comparar el saldo de este contra el nuevo
	 * monto, realizar la resta para obtener el <b>saldo </b>del nuevo mvimiento.
	 * 
	 * Asignar el <b>saldo </b>al <b>movimiento </b>y hacer un entidadService.save
	 * 
	 * @param aplicacionOriginalId
	 * @param aplicacionCancelacionId
	 * @param aplicacionMovtoId
	 * @param cuentaId
	 * @param monto
	 */
	public MovimientoCuentaAcreedor generarMovimientoAcreedor(Long aplicacionOriginalId, Long aplicacionCancelacionId, Long aplicacionMovtoId, Long cuentaId, BigDecimal monto);


	/**

	 * </b>Metodo que realiza la actualizacion del Ingreso a Aplicado.
	 * 
	 * @param ingresoId
	 * @param fechaAplicacion
	 */
	public void aplicarIngreso(Long ingresoId, Date fechaAplicacion);

	/*
	 *This method proof if RecuperacionCruceroJuridico has a PaseAtencion linked like DanosMateriales or GastosMedicosOcupantes. 
	 *If has, the method returns idEstimacion, otherwise null.
	 */
	public Long comprobarSiRecuperacionTienePase (ReporteCabina reporte);
	
	/**   
	 * @param ingresoId
	 */
	public List<ListarIngresoDTO> obtenerIngresosAplicacion(Long ingresoId);

	public AplicacionCobranza obtenerAplicacionPorIngreso(Long ingresoId);

	/**
	 * @param ingresoId
	 */
	public List<DepositoBancarioDTO> obtenerDepositosAplicacion(Long ingresoId);


	/**
	 * </b>M?todo que realizar? la aplicaci?n del o los Ingresos seleccionados contra
	 * ciertos movimientos.
	 * 

	 * @param ingresosId
	 * @param depositos
	 * @param movAcreedores
	 */
	public String aplicarIngreso(String ingresosId, String depositos, String movAcreedores, String movManuales) throws MidasException;

	public String validaAplicarIngreso(String ingresosId, String depositos, String movAcreedores, String movManuales) throws MidasException;

	/**  
	 * Metodo que Aplicara los Ingresos que se encuentren Pendientes por Aplicar y que
	 * tengan relacionados Referencias Bancarias. Se debe ejecutar con hroa por
	 * definir 
	 */
	public void aplicarIngresoAutomatico();

	/**
	 * Invocar al m?todo buscarIngresos de IngresoDao y regresar el resultado.
	 * 
	 * @param filtro
	 */
	public List<ListarIngresoDTO > buscarIngresos(ListarIngresoDTO filtro);

	/**
	 * Invocar al metodo buscarMovimientosAcreedoresde IngresoDao y regresar el
	 * resultado.
	 * 
	 * @param MovimientoAcreedorDTO
	 * @param String
	 * @param Boolean
	 */
	public List<MovimientoAcreedorDTO> buscarMovimientosAcreedores( MovimientoAcreedorDTO filtro ,String movAcreedoresConcat ,Boolean esConsulta );

	public String cancelacionAcuenta(Long idIngreso, String motivo, String cuentaDeposito, String comentario, int formaAplicar, int tipoCancelacion, int subTipoCancelacion );

	/**
	 * -------------------------------------------------------------------------
	 * Se agrega el cambio para el m?dulo de cancelaci?n
	 * -------------------------------------------------------------------------
	 * 
	 * Se parsea el String de los ingresos seleccionados y se arma una lista de
	 * ingresos
	 * se invoca this.sonIngresosValidosPorAplicar
	 * Si el m?todo regres? mensajes de error retornar la lista
	 * 
	 * Iterar la lista de los Ingresos Pendientes por Aplicar y realizar los
	 * siguientes pasos:
	 * <ul>
	 *    <li>invocar this.aplicarIngreso</li>
	 *    <li>Invocar en el DAO la aplicaci?n de las cuentas contables. PENDIENTE ( El
	 * desarrollador debe preguntar si ya esta listo)</li>
	 *    <li>Obtener la lista de recuperaciones, iterarla y cambiar el estatus de todas
	 * a Recuperadas</li>
	 * </ul>
	 * 
	 * Iterar la lista de los ingresos Aplicados y realizar los siguientes pasos:
	 * <ul>
	 *    <li>invoca ingresoDAO.revierteMovimientosDeIngreso</li>
	 * </ul>
	 * <ul>
	 *    <li>Actualizar los siguientes Campos</li>
	 *    <li>fecha de modificacion </li>
	 *    <li>fecha de cancelacion </li>
	 *    <li>usuario de modificaci?n </li>
	 *    <li>usuario de cancelacion </li>
	 *    <li>cambiar el estatus a "Pendiente por Aplicar"</li>
	 *    <li>Se obtiene la lista  de las recuperaciones y actualizar el estatus a
	 * "Pendiente por Recuperar"</li>
	 * </ul>
	 * 
	 * Si existe una diferencia entre el monto total del ingreso Aplicado - la suma de
	 * todos los montos totales de ingresos Pendientes por Aplicar, invocar
	 * eingresoDAO.depositraACuentaAcreedora
	 * 
	 * @param idIngreso
	 * @param motivo
	 * @param cuentaDeposito
	 * @param comentario
	 * @param ngresosPendientesConcat
	 * @param tipoCancelacion
	 */
	public List<String> cancelacionDeReversa(Long idIngreso, String motivo, String cuentaDeposito, String comentario, String ngresosPendientesConcat,int formaAplicar, int tipoCancelacion);



	/**
	 * -------------------------------------------------------------------------
	 * Se agrega el cambio para el m?dulo de cancelaci?n
	 * -------------------------------------------------------------------------
	 * 
	 * Invocar
	 * ingresosDao.obtenerCuentasAcreedorasCancelacionPorDevolucion
	 */
	/**TODO PENDIENTE CuentaAcreedoraDTO*/
	//public List<CuentaAcreedoraDTO> obtenerCuentasAcreedorasCancelacionPorDevolucion();


	/**
	 * 
	 * Obtener el Ingreso correspondiente mediante un entidadService.findById
	 * 
	 * @param id
	 */
	public Ingreso obtenerIngreso(Long id);



	/**
	 * </b>Obtener el listado de Movimientos Acreedores asociados a la aplicacion del
	 * Ingreso.
	 * 
	 * @param ingresoId
	 */
	public List<MovimientoAcreedorDTO> obtenerMovtoAcreedorAplicacion(Long ingresoId);

	/**
	 * -------------------------------------------------------------------------
	 * Se agrega el cambio para el m?dulo de cancelaci?n
	 * -------------------------------------------------------------------------
	 * 
	 * dependiendo del tipo de recuperaci?n obtener la(s) ordenes de compra,
	 * utilizar el "instance of" para determinar la ruta de ?a prden de pago.
	 * 
	 * @param recuperacion
	 */
	public List<OrdenPagoSiniestro> obtenerOrdenesPago(Recuperacion recuperacion);

	/**
	 * -------------------------------------------------------------------------
	 * Se agrega el cambio para el m?dulo de cancelaci?n
	 * -------------------------------------------------------------------------
	 * Se crear? una lista de mensajes de error
	 * 
	 * Se deber? de validar que de los Ingresos Pendientes por Aplicar (Seleccionados)
	 * se encuentren en estatus de Ingreso(s) Pendiente(s).
	 * Si no es asi agregar a la lista de errores el mensaje "EL ingreso X no se
	 * encuentra en estatus pendiente por aplicar"
	 * 
	 * Se sumar?n los montos totales de los ingresos pendientes y se valida que dicha
	 * suma sea menor o igual que el monto total del ingreso por aplicar
	 * Si no cumple la validaci?n agregar a la lista de errores el mensaje "EL ingreso
	 * seleccionado no tiene saldo suficiente para trapasar a los ingresos
	 * pendientes"
	 * 
	 * retornar la lista de mensajes
	 * 
	 * @param ingresoACancelar
	 * @param ingresosPorAplicar
	 */
	public List<String> sonIngresosValidosPorAplicar(List<Ingreso> ingresoACancelar, List<Ingreso> ingresosPorAplicar);


	public ImportesIngresosDTO calculaImportes (String ingresosId, String depositos, String movAcreedores, String movManuales, Boolean consulta);

	/**
	 * <b>SE AGREGA EN DISEÑO DE APLICACION DE INGRESO MANUAL.</b>
	 * 
	 * Invocar al proceduire MIDAS.PKGSIN_RECUPERACIONES.contabilizaAplicacionIngreso
	 * 
	 * @param aplicacionId
	 */
	public void contabilizaAplicacionIngreso(Long aplicacionId) throws  Exception;

	public AplicacionCobranza ejecutaAplicacionIngreso(String ingresosId, String depositos, String movAcreedores, String movManuales) throws MidasException;

	public List<MovimientoCuentaAcreedor> generarAplicacionCancelacion(AplicacionCobranza aplicacionOrigen, TipoAplicacionCobranza tipoAplicacion,String descripcion, List<Ingreso> listaIngresos, String cuentaDeposito, AplicacionCobranza aplicacionCancelacion );


	public void contabilizaCancelacion(Long aplicacionCancancelacionId);

	public void registrarMovtoManual(Long cuentaId, BigDecimal importe, String causaMovimiento);

	public void eliminarMovtoManual(Long movimientoId);

	public List<MovimientoManualDTO> buscarMovimientosManuales(MovimientoManualDTO filtro, String movimientosConcat, Boolean esConsulta);


	public List<MovimientoManualDTO> obtenerMovtoManualAplicacion(Long ingresoId);


	/***
	 * Obtiene el saldo final de una cuenta acreedora
	 * @param cuentaId
	 */
	public Double obtenerSaldoCuentaAcreedora(Long cuentaId);

	/***
	 * Aplicar monto a cuenta acreedora
	 * @param cuentaId
	 * @param monto
	 */
	public String aplicarMontoCuentaAcreedora(IngresoDevolucion ingresoDevolucion);

	/***
	 * Reversa monto a cuenta acreedora
	 * @param cuentaId
	 * @param monto
	 */
	public String reversaMontoCuentaAcreedora(IngresoDevolucion ingresoDevolucion);
	
	public Ingreso obtenerIngresoActivo(Long recuperacionId);
	
    /**
     * VALIDAD SI LA SOLICITUD DE CANCELACION TIENE UNA RECUPERACION DE DEDUCIBLE, SI ESTA TIENE COMO ORIGEN 'LIQ' 
     * RETORNA TRUE, SI TIENE COMO ORIGEN 'PASE' FALSE
     * @param lIngresos
     * @param tipoCancelacion  CANCELACION_POR_DEVOLUCION = 1 | CANCELACION_CUENTA_ACREEDORA = 2
     * @return TRUE: PUEDE CANCELAR | FALSE: NO PUEDE CANCELAR
     */
	public boolean isOrigenDedudicleEsLiq(List<Ingreso> lIngresos , int tipoCancelacion);
	
	/**
    * REGRESA EL ORIGEN DEL DEDUCIBLE, SI EL INGRESO TIENE UNA RECUPERACION DE DEDUCIBLE
    * @param keyIngreso
    * @return
    */
	public String obtenerOrigenDedudicleRecuperacionIngreso(Long keyIngreso);
	
	public void initialize();
	
}

