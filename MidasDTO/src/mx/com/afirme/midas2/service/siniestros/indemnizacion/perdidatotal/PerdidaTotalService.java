package mx.com.afirme.midas2.service.siniestros.indemnizacion.perdidatotal;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.indemnizacion.IndemnizacionAutorizacion;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.IndemnizacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.CartaPTSNoDocumentadaDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.FiniquitoInformacionSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.ImpresionEntregaDocumentosDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.perdidatotal.DeterminacionInformacionSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.perdidatotal.DeterminacionInformacionSiniestroDTO.InfoRecuperacionDeducible;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.PerdidaTotalFiltro;

@Local
public interface PerdidaTotalService {
	
	/**
	 * Obtiene los tipos de concepto de la orden de compra relacionados con las perdidas totales
	 * @return
	 */
	public Map<String, String> obtenerTiposSiniestroPerdidaTotal();
	
	/**
	 * Interfaz para que al momento de autorizar la orden de compra se cree la indemanizacion con la etapa correspondiente
	 * Para las Ordenes de Compra de Perdida Total, se configura con la etapa Autorizacion PT
	 * Para las Ordenes de Compra de Robo Total y Danios se configura con la etapa Determinacion PT
	 * @param orden
	 */
	public void generarIndemnizacion(OrdenCompra orden);
	
	/**
	 * Metodo que busca una indemnizasionSiniestro de acuerdo al id de orden de compra que se le pasa como parametro
	 * @param idOrdenCompra
	 * @return
	 */
	public IndemnizacionSiniestro buscarIndemnizacionSiniestro(Long idOrdenCompra);
	
	/**
	 * Actualiza la informacion de la indemnizacion para marcarla como autorizada, puede ser autorizacion de perdida total
	 * o autorizacion de indemnizacion dependiendo del tipo de autorizacion que se pase como parametro
	 * @param indemnizacion
	 * @param tipoAutorizacion
	 */
	public void autorizarPerdidaTotal(IndemnizacionSiniestro indemnizacion, String tipoAutorizacion);
	
	/**
	 * Actualiza la informacion de la indemnizacion para marcarla como rechazada, puede ser rechazo de perdida total
	 * o de indemnizacion dependiendo del tipo de autorizacion qeu se pase como parametro
	 * @param indemnizacionCapturada
	 * @param tipoAutorizacion
	 */
	public void rechazarPerdidaTotal(IndemnizacionSiniestro indemnizacionCapturada, String tipoAutorizacion);
	
	/**
	 * Busca un objeto indemnizacion siniestro utilizando la id
	 * @param idIndemnziacion
	 */
	public IndemnizacionSiniestro buscarIndemnizacionSiniestroPorId(Long idIndemnizacion);
	
	/**
	 * Devuelve el numero de puertas del vehiculo a partir del id del estilo del autoinciso
	 * @param estiloId
	 * @return
	 */
	public Short obtenerNumeroPuertas(String estiloId);
	
	/**
	 * Guarda la informacion de la indemnizacion que fue capturada en la pantalla. 
	 * A new feature was added. This is for the beneficiary name. It can be edited from Determinar Perdida Total screen, 
	 * through guardarDeterminacionPT method. added by joksrc. 
	 */
	public void guardarDeterminacionPT(IndemnizacionSiniestro indemnizacion, String tipoSiniestro,DeterminacionInformacionSiniestroDTO informacionSiniestro, String beneficiarioEditado);
	
	/**
	 * Carga la informacion del siniestro para la pantalla de determinacion
	 * @param idIndemnizacion
	 * @return
	 */
	public DeterminacionInformacionSiniestroDTO obtenerInformacionSiniestro(Long idIndemnizacion, String tipoSiniestro, String tipoSiniestroDesc);
	
	/**
	 * Carga la informacion del siniestro y el auto del asegurado para la carta del finiquito 
	 * @param indemnizacion
	 * @return
	 */
	public FiniquitoInformacionSiniestroDTO buscarInformacionAseguradoFiniquito(Long idIndemnizacion);
	
	/**
	 * Carga la informacion del siniestro y el auto del tercero para la carta de finiquito
	 * @param indemnizacion
	 * @return
	 */
	public FiniquitoInformacionSiniestroDTO buscarInformacionTerceroFiniquito(Long idIndemnizacion);

	/**
	 * Carga la informacion del siniestro al dto para la impresion de la carta de entrega de documentos
	 * @param siniestroId
	 * @return
	 */
	public ImpresionEntregaDocumentosDTO buscarInformacionSiniestroEntregaDocumentos(Long siniestroId);
	
	/**
	 * Carta la informacion del siniestro al dto para la impresion de la carta de perdidas no documentadas
	 * @param siniestroId
	 * @return
	 */
	public CartaPTSNoDocumentadaDTO buscarInformacionCartaPTS(Long idIndemnizacion);
	
	/**
	 * valida si existe una indemnizacion automatica activa para la estimacion dada.
	 * @param idEstimacion
	 * @return
	 */
	public Boolean existeIndemnizacionActiva(Long idEstimacion);
	
	/**
	 * valida si existe una indemnizacion automatica finalizada para la estimacion dada
	 * @param idEstimacion
	 * @return
	 */
	public Boolean existeIndemnizacionFinalizada(Long idEstimacion);
	
	/**
	 * valida si existe una indemnizacion con una orden de compra de hgs para la estimacion dada
	 * @param idEstimacion
	 * @return
	 */
	public Boolean existeIndemnizacionHGS(Long idEstimacion);
	
	/**
	 * valida si existe una indemnizacion finalizada con una orde de compra de hgs para la estimacion dada
	 * @param idEstimacion
	 * @return
	 */
	public Boolean existeIndemnizacionFinalizadaHGS(Long idEstimacion);
	
	/**
	 * Cambia el estatus de la indemnizacion y de la orden de compra a cancelado
	 * @param idIndemnizacion
	 * @return
	 */
	public void cancelarIndemnizacion(Long idIndemnizacion, String motivoCancelacion);
	
	/**
	 * valida si existe una una orden de compra final para la indemnizacion con estatus diferente a cancelada, esto es,
	 * ya se autorizo la indemnizacion y se cambio el estatus de la orden de compra dummy a Indemnizacion_Finalizada 
	 * y se genera una orden de compra en tramite que tiene como orden de compra origen la dummy
	 * @param idIndemnizacion
	 * @return
	 */
	public Boolean existeOrdenCompraFinal(Long idIndemnizacion);
	
	/**
	 * Cambia la indemnizacion relacionada con la orden de compra con la id que se pasa como parametro 
	 * a la etapa Orden Compra Generada. 
	 * @param idOrdenCompra
	 */
	public void cambiarEtapaIndemnizacionAOrdenCompraGenerada(Long idOrdenCompraOrigen, Long idOrdenCompraGenerada);
	
	/**
	 * Cambia la indemnizacion relacionada con la orden de compra con la id que se pasa como parametro
	 * a la etapa Autorizacion de indemnizacion. 
	 * @param idOrdenCompraOrigen
	 */
	public void cambiarEtapaIndemnizacionAAutorizacionIndemnizacion(Long idOrdenCompraOrigen);
	
	/**
	 * Obtiene la lista de autorizaciones por indemnizacion y por tipo ( si es AI o AT ) 
	 * @param tipoAutorizacion
	 * @param idIndemnizacion
	 * @return
	 */
	public List<IndemnizacionAutorizacion> listaAutorizaciones(String tipoAutorizacion, Long idIndemnizacion );
	
	/**
	 * Obtiene el puesto del usuario actual para el modulo de indemnizacion. Se realiza la busqueda en la tabla TOROLAUTORIZAINDEMNIZACION
	 * @return
	 */
	public String obtenerPuestoUsuarioActualIndemnizacion();
	
	/**
	 * Obtiene un mapa de los roles de autorizacion de indemnizacion que no tienen autorizacion final activa
	 * @return
	 */
	public Map<String, String> obtenerListaRolesAutorizacionSinAutorizacionFinal();
	
	/**
	 * Guarda el rol suplente para el subdirector de siniestros para la autorizacion de la indemnizacion
	 * @param rolSuplente
	 */
	public void guardarConfiguracionAutorizacionIndemnizacion(String rolSuplente);
	
	/**
	 * Revisa si el usuario actual tiene permiso para realizar la autorizacion final.
	 * @return
	 */
	public boolean tieneRolPermisoDeAutorizacionFinal();
	
	/**
	 * Actualiza la indemnizacion con la informacion de la orden de compra que se genero
	 * @param idOrdenCompraOriginal
	 * @param idOrdenCompraGenerada
	 */
	public void relacionarOrdenCompraGeneradaConIndemnizacion(Long idOrdenCompraOriginal, Long idOrdenCompraGenerada);
	
	/**
	 * Retornar indemnizacion asociada a un reporte de robo para obener los datos de la perdida tota y orden de compra generada
	 * @param roboId
	 * @return
	 */
	public IndemnizacionSiniestro obtenerOrdenCompraRobo(Long roboId);
	
	public String obtenerTipoSiniestro(OrdenCompra orden, Map<String, String> tiposSiniestro);
	
	/**
	 * obtiene la etapa en la que se encuentra la indemnizacion activa de la estimacion proporcionada
	 * @param idEstimacion
	 * @return
	 */
	public String obtenerEtapaIndemnizacionPorEstimacion(Long idEstimacion);
	
	/**
	 * actualiza los valores del deducible y el total a indemnizar en la indemnizacion activa de la estimacion proporcionada
	 * @param idEstimacion
	 */
	public void actualizarDeducibleYTotalIndemnizacion(Long idEstimacion);
	
	/**
	 * busca la indemnizacion que no se encuentra rechazada o cancelada para la estimacion proporcionada
	 * @param idEstimacion
	 * @return
	 */
	public IndemnizacionSiniestro obtenerIndemnizacionActivaPorEstimacion(Long idEstimacion);
	
	/**
	 * Obtiene la prima no devengada para la indemnizacion
	 * @param indemnizacion
	 * @return
	 */
	public BigDecimal obtenerPrimaNoDevengada(IndemnizacionSiniestro indemnizacion);
	
	/**
	 * Devuelve true si la estimacion ya ha pasado por la etapa de recepcion de documentos de la indemnizacion 
	 * @param idIndemnizacion
	 * @return
	 */
	public boolean esPTDocumentada(Long idEstimacion);
	
	/**
	 * Devuleve true si la estimacion ya ha sido liquidada
	 * @param idIndemnizacion
	 * @return
	 */
	public boolean esPTIndemnizada(Long idEstimacion);
	
	/**
	 * Metodo para buscar indemnizaciones usando stored procedure
	 * @param filtroPT
	 * @return
	 */
	public List<PerdidaTotalFiltro> buscarIndemnizaciones(PerdidaTotalFiltro filtroPT);
		
}