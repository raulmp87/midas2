package mx.com.afirme.midas2.service.siniestros.notificaciones;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.DestinatarioConfigNotificacionCabina.EnumModoEnvio;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.depuracion.ConfiguracionDepuracionReserva;

import org.apache.log4j.Logger;
import org.springframework.web.util.HtmlUtils;

@Local
public interface EnvioNotificacionesService {
	
	public static final Logger log = Logger
		.getLogger(EnvioNotificacionesService.class);
	
	/**
	 * Constantes para atributos de mapa comunes 
	 */
	public static final String ATR_REPORTE = "reporte";
	public static final String ATR_AGENTE = "agente"; 
	public static final String ATR_CLIENTE = "cliente"; 
	public static final String ATR_CABINERO = "cabinero"; 
	public static final String ATR_OFICINAID = "oficinaId"; 
	public static final String ATR_INTERESADO = "interesado"; 
	public static final String ATR_AJUSTADOR = "ajustador"; 
	public static final String ATR_EMAILINTERESADO = "emailInteresado"; 
	public static final String ATR_COTIZACION = "cotizacionDTO"; 
	public static final String ATR_POLIZADTO = "polizaDTO";
	public static final String ATR_POLIZA = "poliza"; 
	public static final String ATR_INGENIERO = "ingeniero"; 
	public static final String ATR_ERROR = "error"; 
	public static final String ATR_ERRORCTG = "errorCtg"; 							   
	public static final String ATR_AUTO = "auto"; 
	public static final String ATR_FOLIO = "folio"; 
	public static final String ATR_CONF = "configuracion"; 
	public static final String ATR_INCISO = "inciso";  
	public static final String ATR_NUMREPORTE = "numeroReporte";
	public static final String ATR_OFICINA = "oficinaSiniestro"; 
	public static final String ATR_PERSONAREPORTA = "personaReporta"; 
	public static final String ATR_TELEFONOREPORTA = "telefonoReporta";
	public static final String ATR_UBICASINIE = "lugarSiniestro"; 
	public static final String ATR_HORAREPORTE="horaReporte"; 
	public static final String ATR_CIUDADSINIESTRO = "ciudadSiniestro"; 
	public static final String ATR_ESTADOSINIESTRO="estadoSiniestro";
	public static final String ATR_FECHAHORAASIG ="horaAsignacion"; 
	public static final String ATR_TIPOSINIESTRO="tipoSiniestro"; 
	public static final String ATR_CIUDADESTADO="ciudadEstado"; 
	public static final String ATR_NUMPOLIZA = "numeroPoliza"; 
	public static final String ATR_NUMPOLIZFORMAT = "numPolizaFormateada"; 
	public static final String ATR_REPORTCAB_GMH="reporteCabGmh"; 
	public static final String ATR_OPERACABINA = "operadorCabina";
	   

	/**
	 * 
	 * Enum con los codigos de los movimientos a notificar
	 * 
	 */
	public enum EnumCodigo {
		CABINA_ASIGNACION_AJUSTADOR,                   
		CABINA_ARRIBO_AJUSTADOR,                
		CABINA_TERMINO_SINIESTRO, 
		CARTA_COBERTURA_CON_POLIZA_VALIDA,
		CARTA_COBERTURA_CON_POLIZA_INVALIDA,
		CARTA_COBERTURA_VARIOS_INCISOS,
		CARTA_COBERTURA_CON_POLIZA_INVALIDA_SUPERVISOR,
		CARTA_COBERTURA_VARIOS_INCISOS_SUPERVISOR,
		AFECTACION_DE_INCISO_APLICA_DEDUCIBLE, 
		AFECTACION_DE_INCISO_AUTO_SIGUE_AFIRME, 
		AFECTACION_DE_INCISO_CORREO_CLIENTE, 
		AFECTACION_RESERVA_ROBO_TOTAL,
		RC_PERSONAS_LESION,                    
		RC_PERSONAS_HOMICIDIO,                  
		GASTOS_MEDICOS_LESION, 
		GASTOS_MEDICOS_HOMICIDIO,              
		RC_BIENES_ASIGNACION_INGENIERO,         
		ROBO_TOTAL, 
		ASIGNAR_POLIZA_A_REPORTE_DE_SINIESTRO, 
		ERROR_ENVIO_OCRA,
		CANCELACION_ORDENPAGO,
		NOTIFICA_FALLA_HGS,
		DEPURACION_RESERVA,
		SIN_CONCEPTO_COBERTURA,
		REGISTRO_DEVOLUCION_FACTURA,
		CONDICIONES_ESPECIALES_VIP,
		SINIESTRO_ANTIG_MONTO_RESERVA,
		CONVERTIR_SINIESTRO_HGS_PERDIDA_TOTAL,
		ERROR_VALIDACION_VALUACION,
		PRESTADOR_SERVICIO_HGS_NO_VALIDO,
		NOTIF_CLIENTE_DEDUCIBLE,
		NOTIF_CANCELA_DEDUCIBLE,
		SVM_NOTIFICACION_COMPRADOR,
		SVM_RECORDATORIO_COMPRADOR,
		SVM_SOLICITUD_PRORROGA,
		SVM_ORDEN_SALIDA,
		SVM_RECIBO_CONFORMIDAD,
		SVM_CANC_LIQ_SVM_VENDIDO,
		SVM_NOT_COMPRADOR_PRORROGA,
		SVM_NOT_COMPRADOR_CANC_ADJUDICACION,
		SVM_RECORDATORIO_PAGO_DIARIO_POST_PRORROGA,
		SN_RCIA_EDO_CUENTA,
		SN_RCIA_EDO_CUENTA_SNCORREO,
		NOTIF_CANCELACION_CHEQUE_DEVINGRESO,
		NOTIF_SOLICITUD_CANCELA_REC_CIA,
		SN_RCIA_CARTAS_POR_ELABORAR,
		SN_GENERA_COMPLEMENTO_CIA,
		NOTIF_EMISION_FACTURA,
		NOTIF_REENVIO_FACTURA,
		NOTIF_CANCELA_FACTURA,
		NOTIF_CANCELA_FACTURA_NO_EXITOSA,
		INCENTIVO_NOTIF_AUTORIZADA,
		ORDEN_COMPRA_NO_VALIDA,
		ENVIO_LIGA_AGENTE,
		COTIZACION_LIGA_AGENTE,
		CARATULA_CLIENTE_LIGA_AGENTE,
		CARTA_COBERTURA_CON_POLIZA_INVALIDA_48HRS,
		CARTA_COBERTURA_CON_POLIZA_INVALIDA_72HRS
		;
	}
	
	/**
	 * 
	 * Clase para manejar los coreos a los que se van a enviar, usar funcion
	 * agregar(EnumModoEnvio modo, String correo, String nombre)
	 * 
	 */
	public class EmailDestinaratios {
		private static final int	LONGITUD_MINIMA_EMAIL	= 5;
		private List<String> para = new ArrayList<String>();
		private List<String> cc = new ArrayList<String>();
		private List<String> cco = new ArrayList<String>();

		/**
		 * Agregar un destinatario al objeto. 
		 * @param modo Tipo de destinatario: <code>EnumModoEnvio.PARA.toString()</code>, <code>EnumModoEnvio.CC.toString()</code>, <code>EnumModoEnvio.CCO.toString()</code>
		 * @param correo Email en formato abc@mail.com
		 * @param nombre Nombre del destinatario
		 */
		public void agregar(EnumModoEnvio modo, String correo, String nombre) {
			String rFC822Email = obtenerRFC822Email(nombre, correo);
			if (rFC822Email != null) {
				switch (modo) {
				case PARA:
					para.add(rFC822Email);
					break;
				case CC:
					cc.add(rFC822Email);
					break;
				case CCO:
					cco.add(rFC822Email);
					break;
				default:
					log.error("Modo de envio no valido");
					break;
				}
			}
		}
		
		/**
		 * Permite integrar los destinatarios incluidos en la instancia con los de otra instancia
		 * @param otros
		 */
		public void integrar(EmailDestinaratios otros){
			if(otros != null){
				if(otros.para != null && otros.para.size() > 0){
					para.addAll(otros.para);
				}
				if(otros.cc!= null && otros.cc.size() > 0){
					cc.addAll(otros.cc);
				}
				if(otros.cco!= null && otros.cco.size() > 0){
					cco.addAll(otros.cco);
				}
			}
		}
		
		/**
		 * Devolver el listado de correos de los destinatarios en una sola lista
		 * @return
		 */
		public List<String> obtenerDestinatarios(){
			List<String> todos = new ArrayList<String>();
			if(this.para != null){
				todos.addAll(this.para);
			}
			if(this.cc != null){
				todos.addAll(this.cc);				
			}
			if(this.cco != null){
				todos.addAll(cco);
			}			
			return todos;
		}

		private boolean esEmailValido(String correo) {
			if (correo != null && correo.length() >= LONGITUD_MINIMA_EMAIL) {
				String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
				java.util.regex.Pattern p = java.util.regex.Pattern
						.compile(ePattern);
				java.util.regex.Matcher m = p.matcher(correo);
				return m.matches();
			} else {
				return false;
			}
		}

		private String obtenerRFC822Email(String nombre, String correo) {
			if (esEmailValido(correo)) {
				if (nombre != null && nombre.length() > 0) {
					String nombreEscapado = HtmlUtils.htmlEscape(nombre);
					return nombreEscapado + " <" + correo + ">";
				} else {
					return correo;
				}
			} else {
				log.error("correo no valido: " + correo);
				return null;
			}
		}

		public List<String> getPara() {
			return para;
		}

		public void setPara(List<String> para) {
			this.para = para;
		}

		public List<String> getCc() {
			return cc;
		}

		public void setCc(List<String> cc) {
			this.cc = cc;
		}

		public List<String> getCco() {
			return cco;
		}

		public void setCco(List<String> cco) {
			this.cco = cco;
		}
	}

	/**
	 * Metodo @Asyncrono para envio de notificaciones (email) usando la
	 * configuración creada.
	 * 
	 * @param codigo
	 *            String que indica el Movimiento a notificar
	 *            {@link EnvioNotificacionesService#EnumCodigo}.toString()
	 * @param data
	 *            {@link HashMap}<{@link String}, {@link Serializable}> con
	 *            información relevante a la notificación, entidad(es) donde
	 *            sacar información para el email <br/>
	 * @ejemplos <br/> {@link String} codigo : {@link CABINA_ASIGNACION_AJUSTADOR} <br/>
	 *           HashMap<String, Serializable> dataArg = new HashMap<String,
	 *           Serializable>(); {@link HashMap}< {@link String},
	 *           {@link Serializable}> data = new {@link HashMap}<
	 *           {@link String}, {@link Serializable}>(); data.put("reporte",
	 *           {@link ReporteCabina} instancia}); <br/>
	 */
	public void enviarNotificacion(String codigo,
			Map<String, Serializable> data);
	
	/**
	 * Cargar parametros generales de configuracion de notificaciones basandose en reporte cabina
	 * @param mapa actual, crea uno en caso de ser <code>null</code>
	 * @param reporte
	 * @return
	 */
	public Map<String, Serializable> cargarParametrosGeneralesSiniestros(ReporteCabina reporte, Map<String, Serializable> mapa);
	
	/***
	 * Envio de notificaciones (email) usando la
	 * configuración creada. Se puede enviar destinatarios adicionales no configurados en la notificacion.
	 * 
	 * @param codigo
	 * @param reporte
	 */
	public void enviarNotificacion(String codigo,
			Map<String, Serializable> data, String folio, EmailDestinaratios destinatarios);
	
	/***
	 * Envio de notificaciones (email) usando la
	 * configuración creada. Se puede enviar destinatarios adicionales no configurados en la notificacion.
	 * 
	 * @param codigo
	 * @param reporte
	 */
	public void enviarNotificacion(String codigo,
			Map<String, Serializable> data, String folio, EmailDestinaratios destinatarios, 
			List<ByteArrayAttachment> adjuntos);

	/***
	 * Envio de notificaciones (email) usando la
	 * configuración creada. Se puede enviar destinatarios adicionales no configurados en la notificacion.
	 * 
	 * @param codigo
	 * @param reporte
	 */
	public void enviarNotificacion(String codigo,
			Map<String, Serializable> data, String folio, EmailDestinaratios destinatarios, 
			List<ByteArrayAttachment> adjuntos, Map<String, String> mapInlineImages);
	
	/***
	 * Preparar y enviar notificacion con informacion de un reporte de cabina. propia  que se enviar con el reporte y el codigo de la notificacion.
	 * Se complementa los datos del mapa con informacion "necesaria" obtenida del reporte el cual se espera que venga cargado en el mapa 
	 * 
	 * @param codigo
	 * @param data - dentro de los datos en el mapa se espera un ReporteCabina
	 */
	public void enviarNotificacionSiniestros(String codigo, Map<String, Serializable> data);
	
	

	/***
	 * Notificacion que se debe enviar cuando se crea el reporte, para validar
	 * los tiempos de asigancion
	 * 
	 * @param reporte
	 */
	public void notificacionCabinaCreacionReporte(ReporteCabina reporte);

	/***
	 * Notificacion que se debe enviar cuando se asigna el ajustador al reporte,
	 * para validar tiempos de arribo
	 * 
	 * @param reporte
	 */
	public void notificacionCabinaAsignacionAjustador(ReporteCabina reporte);

	/***
	 * Notificacion que se debe enviar cuando arriba el ajustador, para validar
	 * tiempos de finalizacion
	 * 
	 * @param reporte
	 */
	public void notificacionCabinaArriboAjustador(ReporteCabina reporte);

	/**
	 * Notificacion que se debe enviar cuando se encuentra asignada carta
	 * cobertura con poliza valida pero contiene más de un inciso con el número
	 * de serie
	 * 
	 * @param reporte
	 * @param poliza
	 * @param auto
	 */
	public void notificacionCartaCoberturaVariosIncisos(ReporteCabina reporte,
			PolizaDTO poliza, AutoIncisoReporteCabina auto,String tipo);

	/**
	 * Notificacion que se debe enviar cuando se encuentra asignada carta
	 * cobertura sin poliza valida
	 * 
	 * @param reporte
	 * @param auto
	 */
	public void notificacionCartaCoberturaSinPoliza(ReporteCabina reporte,
			AutoIncisoReporteCabina auto,String tipo);

	/**
	 * Notificacion que se debe enviar cuando se encuentra asignada carta
	 * cobertura con poliza valida
	 * 
	 * @param reporte
	 * @param poliza
	 * @param auto
	 */
	public void notificacionCartaCobertura(ReporteCabina reporte,
			PolizaDTO poliza, AutoIncisoReporteCabina auto);

	/**
	 * Notificacion que se debe enviar cuando se el servicio de OCRA regresa
	 * algun error
	 * 
	 * @param reporte
	 * @param error
	 *            objeto que implemente Serializable, se espera que tenga
	 *            .getMessage() tipo String para incluir los errores en el
	 *            correo (Checar plantilla en DB)
	 */
	public void notificacionErrorOCRA(ReporteCabina reporte, Serializable error);





	/***
	 * Notificar recomendaciones en afectacion de inciso al convertir a siniestro
	 * 
	 * @param reporte
	 */
	public void notificacionAfectacionIncisoCorreoCliente(ReporteCabina reporte);
	
	/**
	 * @param reporte
	 * @param configuracion
	 */
	public void notificacionDepuracionReserva(ReporteCabina reporte, ConfiguracionDepuracionReserva configuracion);
	
	/***
	 * Envia notificacion si proceso HGS no tiene concepto cobertura para la orden de compra
	 */
	public void notificacionSinConceptoAjuste(ReporteCabina reporte);	
	
	public void notificacionConvertirSiniestroHgs(ReporteCabina reporte,String folio);
	
	public void notificarErrorValidacionValuacion(String reporte, String mensaje);
	
	public void notificarErrorValidacionValuacion(String reporte, String id, String nombre);
	
	/**
	 *Método marcado con scheduller a las 12 am y 12 pm diariamente. 
	 *Este cron se encargará de revisar si existen notificaciones 
	 *con recurrencia registradas para realizar nuevamente el 
	 *envío de las mismas. 
	 */
	public void notificarRecurrentes();
	
	/**
	 * Define un adjunto de tipo <code>ByteArrayAttachment</code> que sirve para enviar documentos en correos
	 * @param adjunto Arreglo de bytes que contiene el archivo
	 * @param nombreArchivo Nombre del archivo
	 * @param tipoArchivo Tipo de archivo definido en ByteArrayAttachment.TipoArchivo
	 * @return <code>ByteArrayAttachment</code> conteniendo el archivo a adjuntar en el correo
	 */
	public ByteArrayAttachment generarAdjuntoCorreo(byte[] adjunto, String nombreArchivo, ByteArrayAttachment.TipoArchivo tipoArchivo);

}
