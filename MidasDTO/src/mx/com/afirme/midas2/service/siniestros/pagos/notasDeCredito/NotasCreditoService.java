package mx.com.afirme.midas2.service.siniestros.pagos.notasDeCredito;

import java.io.File;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.EnvioValidacionFactura;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.MensajeValidacionFactura;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionProveedor;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito.BusquedaNotasCreditoDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito.FacturaSiniestroRegistro;
import mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito.NotasCreditoRegistro;
import mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito.OrdenCompraRegistro;

/**
 * @author Israel
 * @version 1.0
 * @created 31-mar.-2015 12:44:16 p. m.
 */
@Local
public interface NotasCreditoService {

	
	/**
	 * 
	 * @param notasCreditoFIltro
	 */
	public List<NotasCreditoRegistro> buscarNotasCeCredito (BusquedaNotasCreditoDTO notasCreditoFIltro);

	/**
	 * 
	 * @param idNotasCreditoConcat
	 */
	public Boolean cancelarNotaDeCredito(String idNotasCreditoConcat);

	/**
	 * 
	 * @param registros
	 */
	public List<NotasCreditoRegistro> convierteANotasCreditoRegistro(List<DocumentoFiscal> registros);

	/**
	 * 
	 * @param ordenes
	 */
	public List<OrdenCompraRegistro> convierteAOrdenesCompraRegistro(List<OrdenCompra> ordenes);

	public List<FacturaSiniestroRegistro> obtenerFacturasParaCrearNotasDeCredito();
	
	/**
	 * 
	 * @param facturaId
	 */
	public List<OrdenCompraRegistro> obtenerOrdenesDeCompraPorFactura(Long facturaId);

	/**
	 * 
	 * @param idProveedor
	 * @param fileNotasDeCredito
	 */
	public List<EnvioValidacionFactura> procesarArchivoNotasDeCredito(Integer idProveedor, File fileNotasDeCredito, String fileName);

	/**
	 * 
	 * @param batchId
	 * @param facturaId
	 * @param ordenesCompraConcat
	 * @param ordenesCompraCanceladasConcat
	 * @param notasCreditoSeleccionadasConcat
	 */
	public List<EnvioValidacionFactura> registrarNotasDeCredito(Long batchId, Long facturaId, String ordenesCompraConcat, String ordenesCompraCanceladasConcat, String notasCreditoSeleccionadasConcat);
	
	public List<EnvioValidacionFactura> obtenerEnvioValidacionFacturaPorConjunto(Long conjuntoId);
	
	public List<OrdenCompraRegistro> obtenerOrdenesDeCompraPorConjunto(Long conjuntoId);
	
	/**
	 * Valida si existen facturas en un estatus determinado entre las facturas que fueron procesadas.
	 * @return
	 */
	public Boolean existenFacturasEstatus(List<EnvioValidacionFactura> facturas, String estatusDocumentoFiscal);
	
	public List<NotasCreditoRegistro> obtenerNotasDeCreditoPorFactura(Long idFactura);
	
	public List<NotasCreditoRegistro> obtenerNotasDeCreditoPorRecuperacion(Long idRecuperacion);
	
	public String cancelarNotaCreditoRecuperacion (Long idNotaCredito );
	
	public List<EnvioValidacionFactura> obtenerNotasCreditoPendientesPorProovedor (Integer idProveedor);
	
	public List<EnvioValidacionFactura> procesarNotasCreditoRecuperacionProveedor (Integer idProveedor,File fileNotasDeCredito,String extension);
	
	public List<EnvioValidacionFactura> registrarNotasParaRecuperaciones (Long idBatch,Long idRecuperacion,String notasCreditoSeleccionadasConcat,Integer idProveedor);
	
	public List<MensajeValidacionFactura> validaSumatoriaNotasRecuperacion (RecuperacionProveedor recuperacion, List<EnvioValidacionFactura> envioValidacion);
	public List<NotasCreditoRegistro> obtenerNotasDeCreditoPorLiquidacion(Long idLiquidacion);
	public List<NotasCreditoRegistro> obtenerRecuperacionesNotasDeCreditoPorLiquidacion(Long idLiquidacion);
	
	
}