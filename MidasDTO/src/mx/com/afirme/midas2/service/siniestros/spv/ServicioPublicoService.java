package mx.com.afirme.midas2.service.siniestros.spv;

import javax.ejb.Local;

import mx.com.afirme.midas2.service.sistema.comm.CommService;

@Local
public interface ServicioPublicoService extends CommService {

	public void notificarReporteServicioPublico(Long keyReporteCabina);
	
	public boolean isPendientesSpv(Long keyReporteCabina);
	
}
