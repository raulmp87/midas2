package mx.com.afirme.midas2.service.siniestros.catalogo.serviciosiniestro;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation.OperationType;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;
import mx.com.afirme.midas2.dto.siniestros.catalogos.ServicioSiniestrosDTO;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService;

@Local
public interface ServicioSiniestroService extends CatalogoSiniestroService{
	
	
	/***
	 * 
	 * @param servicioSiniestroDTO Objeto de transporte con todos los datos del form
	 */
	public void salvarServicioSiniestro(ServicioSiniestrosDTO servicioSiniestroDTO,  Short tipoOperacion);
	
	/***
	 * 
	 * @param servicioSiniestroDTO
	 * @param tipoOperacion
	 */
	public void actualizaServicioSiniestro(ServicioSiniestrosDTO servicioSiniestroDTO,  Short tipoOperacion);
	
	/**
	 * 
	 * @param servicioSiniestroFiltro
	 * @return
	 */
	public List<ServicioSiniestro> buscarServicioSiniestro(ServicioSiniestroFiltro servicioSiniestroFiltro); 
	
	/***
	 * 
	 * @param idSiniestro
	 * @param tipoOperacion
	 * @return
	 */
	public ServicioSiniestrosDTO obtenerDatosSiniestro(Long idSiniestro, Long idPersonaMidas, Short tipoOperacion );
	
	/**
	 * 
	 * @param servicioSiniestroFiltro
	 * @return
	 */
	public List<ServicioSiniestro> buscarAjustadoresDisponibles(ServicioSiniestroFiltro servicioSiniestroFiltro);
	
	public ServicioSiniestro findById(Long id);
	
	public class ServicioSiniestroFiltro extends CatalogoFiltro {
		
		
		private Long    noAbogadoAjustadorId;
		private String  nombreAbogadoAjustador;
		private Long    tipoAbogadoAjustador;
		private Long    noPrestador;
		private String  nombrePrestador;
		private Long    oficinaId; // # AL PERTENECER A UN OBJETO ES NECESARIO MAPEARLA CON EL OFICINA.ID
		private Short   tipoServicio; // # INDICA SI ES AJUSTADOR-1 O ABOGADO-2 . SE SETEA POR MEDIO DE LA LLAVE QUE VIENE DEL MENU EN LA BUSQUEDA
		private String  estado;
		private String  municipio;
		private String  tipoDisponibilidad;
		private String  estatusAsignacion;
		
		@FilterPersistenceAnnotation(persistenceName="id")
		public Long getNoAbogadoAjustadorId() {
			return noAbogadoAjustadorId;
		}
		
		@FilterPersistenceAnnotation(persistenceName="servicioSiniestroId")
		public Short getTipoServicio() {
			return tipoServicio;
		}
		
		@FilterPersistenceAnnotation(persistenceName="oficina.id")
		public Long getOficinaId() {
			return oficinaId;
		}
		
		@FilterPersistenceAnnotation(persistenceName="personaMidas.nombre", operation=OperationType.LIKE  )
		public String getNombreAbogadoAjustador() {
			return nombreAbogadoAjustador;
		}
		
		@FilterPersistenceAnnotation(persistenceName="ambitoPrestadorServicio")
		public Long getTipoAbogadoAjustador() {
			return tipoAbogadoAjustador;
		}
		
		@FilterPersistenceAnnotation(persistenceName="prestadorServicio.id")
		public Long getNoPrestador() {
			return noPrestador;
		}
		
		@FilterPersistenceAnnotation(persistenceName="prestadorServicio.personaMidas.nombre", operation=OperationType.LIKE )
		public String getNombrePrestador() {
			return nombrePrestador;
		}
		
		@FilterPersistenceAnnotation(excluded=true)
		public String getEstado() {
			return estado;
		}

		@FilterPersistenceAnnotation(excluded=true)
		public String getMunicipio() {
			return municipio;
		}

		public void setNoAbogadoAjustadorId(Long noAbogadoAjustadorId) {
			this.noAbogadoAjustadorId = noAbogadoAjustadorId;
		}
		public void setNombreAbogadoAjustador(String nombreAbogadoAjustador) {
			this.nombreAbogadoAjustador = nombreAbogadoAjustador;
		}
		public void setTipoAbogadoAjustador(Long tipoAbogadoAjustador) {
			this.tipoAbogadoAjustador = tipoAbogadoAjustador;
		}
		public void setNoPrestador(Long noPrestador) {
			this.noPrestador = noPrestador;
		}
		public void setNombrePrestador(String nombrePrestador) {
			this.nombrePrestador = nombrePrestador;
		}
		
		public void setEstado(String estado) {
			this.estado = estado;
		}

		public void setMunicipio(String municipio) {
			this.municipio = municipio;
		}

		public void setOficinaId(Long oficinaId) {
			this.oficinaId = oficinaId;
		}

		public void setTipoServicio(Short tipoServicio) {
			this.tipoServicio = tipoServicio;
		}
				
		@FilterPersistenceAnnotation(excluded=true)
		public String getTipoDisponibilidad() {
			return tipoDisponibilidad;
		}

		public void setTipoDisponibilidad(String tipoDisponibilidad) {
			this.tipoDisponibilidad = tipoDisponibilidad;
		}

		@FilterPersistenceAnnotation(excluded=true)
		public String getEstatusAsignacion() {
			return estatusAsignacion;
		}

		public void setEstatusAsignacion(String estatusAsignacion) {
			this.estatusAsignacion = estatusAsignacion;
		}
		

	}

}




