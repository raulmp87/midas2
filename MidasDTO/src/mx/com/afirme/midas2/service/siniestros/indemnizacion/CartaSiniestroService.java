package mx.com.afirme.midas2.service.siniestros.indemnizacion;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.movil.cliente.EnvioCaratulaParameter;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.CartaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.CartaSiniestroEndoso;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.CartaSiniestroFactura;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.InfoContratoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.RecepcionDocumentosSiniestro;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.CartaBajaPlacasDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.FormatoFechasPT;

@Local
public interface CartaSiniestroService {
	
	/**
	 * Metodo para generar el pdf de la carta de baja de placas
	 * @param bajaPlacasDTO
	 * @return
	 */
	public TransporteImpresionDTO imprimirCartaBajaPlacas(CartaBajaPlacasDTO bajaPlacasDTO);
	
	/**
	 * Metodo para generar el pdf de la carta de recepcion de documentos
	 * @param recepcionDoctoSin
	 * @return
	 */
	public TransporteImpresionDTO imprimirRecepcionDocumentosSiniestro(String numeroSiniestro, RecepcionDocumentosSiniestro recepcionDoctoSin, CartaBajaPlacasDTO informacionAuto);
	
	/**
	 * Valida la informacion de la carta de recepcion de documentos y entrega una lista con el nombre de los campos vacios
	 * Si la lista de nombres no tiene elementos, quiere decir que paso la validacion
	 * @param recepcionDoctoSin
	 * @return
	 */
	public List<String>	validarRecepcionDocumentos(RecepcionDocumentosSiniestro recepcionDoctoSin, CartaBajaPlacasDTO informacionAuto, String numeroSiniestro, String tipoCarta);
	
	/**
	 * Obtiene la informacion del auto del siniestro
	 * @param reporteCabina
	 * @return
	 */
	//public IncisoSiniestroDTO buscarInformacionAuto(ReporteCabina reporteCabina);
	
	/**
	 * Obtiene el objeto RecepcionDocumentosSiniestro para mostrar en la ventana.
	 * Primero intenta obtenerlo de la base de datos, en caso de que no exista un registro para el siniestro
	 * se crea un objeto nuevo.
	 * En caso que sea nuevo, obtiene de la indemnizacion si el usuario selecciono que sea indemnizacion de pagos de danios 
	 * @param idSiniestro
	 */
	public RecepcionDocumentosSiniestro mostrarRecepcionDeDocumentos(Long idIndemnizacion);
	
	/**
	 * guarda la recepcion capturada en la pantalla y cambia el estatus de la indemnizacion asociada
	 * @param recepcionCapturada
	 * @return
	 */
	public List<String> guardarRecepcionDocumentos(RecepcionDocumentosSiniestro recepcionCapturada, Long idIndemnizacion);

	/**
	 * Genera la lista de anios, de refrendos y tenencias, que tiene que entregar el cliente para poder
	 * realizar la recepcion de documentos
	 * @param anioAuto
	 * @param anioOcurrido
	 * @return
	 */
	public Map<String, String> obtenerListaRefrendosTenenciasParaRecepcionDoctos(Integer anioModeloAuto, Date fechaOcurrido);
	
	/**
	 * Imprime una carta de notificacion de perdida total para el siniestro dado
	 * @param idSiniestro
	 * @return
	 */
	public TransporteImpresionDTO imprimirCartaNotificacionPerdidaTotal(Long idIndemnizacion, Integer idValuador);
	
	/**
	 * Imprime la carta de Determinacion de Perdida Total con la informacion capturada en la pantalla
	 * @param idIndemnizacion
	 * @param fechaDeterminacion
	 * @param tipoSiniestroDesc
	 * @param numeroPuertas
	 * @return
	 */
	public TransporteImpresionDTO imprimirDeterminacionPT(Long idIndemnizacion, Date fechaDeterminacion, String tipoSiniestro, String tipoSiniestroDesc);
	
	/**
	 * Busca informacion de la carta de finiquito que le correponde a la indemnizacion 
	 * @param idIndemnizacion
	 * @param tipoSiniestro
	 * @return
	 */
	public CartaSiniestro obtenerCartaFiniquito(Long idIndemnizacion);
	
	/**
	 * Imprime la carta de finiquito para la indemnizacion que se le pasa de parametro
	 * @param idIndemnizacion
	 * @param tipoSiniestro
	 * @return
	 */
	public TransporteImpresionDTO imprimirCartaFiniquito(Long idIndemnizacion);
	
	/**
	 * Metodo para saber el tipo de carta de finiquito que se debe generar para la indemnizacion.
	 * Si es true la indemnizacion genera una carta de finiquito de asegurado.
	 * Si es false la indemnizacion genera una carta de finiquito de tercero.
	 * @param idIndemnizacion
	 * @return
	 */
	public Boolean esCoberturaAsegurado(Long idIndemnizacion);
	
	/**
	 * Guarda la informacion del a carta finiquito que se capturo en la pantalla
	 * @param cartaFiniquito
	 */
	public void guardarCartaFiniquito(CartaSiniestro cartaFiniquito, Long idIndemnizacion);
	/**
	 * 
	 * @param idOrdenCompra
	 * @return
	 */
	public FormatoFechasPT obtenerFechasPT( Long idOrdenCompra );
	
	/**
	 * Guarda la informacion capturada en la pantalla de formato fechas PT, en la seccion de primas pendientes
	 * @param formatoFechasPT
	 */
	public void guardarPrimasPendientes( FormatoFechasPT formatoFechasPT );
	
	/**
	 * Consulta las fechas y los datos de la carta para generar la impresion del formato fechas de Perdida Total
	 * @param formatoFechasPT
	 * @return
	 */
	public TransporteImpresionDTO imprimirFormatoFechasPT( Long idOrdenCompra );
	
	/**
	 * Metodo para buscar la carta de entrega de documentos para un siniestro
	 * @param siniestroId
	 * @return
	 */
	public CartaSiniestro obtenerCartaEntregaDocumentos(Long idIndemnizacion);
	
	/**
	 * Metodo para guardar la carta de entrega de documentos capturada
	 * @param cartaSiniestro
	 */
	public void guardarEntregaDocumentos(CartaSiniestro cartaSiniestro, Long idIndemnizacion);
	
	/**
	 * Metodo que genera la impresion de la carta de entrega de documentos dependiendo si es carta tipo robo total o tipo perdida total
	 * @param cartaSiniestro
	 * @param tipoCarta
	 * @return
	 */
	public TransporteImpresionDTO imprimirCartaEntregaDocumentos(CartaSiniestro cartaSiniestro, String tipoCarta, Long idIndemnizacion);
	
	/**
	 * Busca la lista de endosos para una carta de entrega de documentos determinada
	 * @param idCartaSiniestro
	 * @return
	 */
	public List<CartaSiniestroEndoso> listarEndosos(Long idCartaSiniestroFactura);
	
	/**
	 * Elimina el endoso con la id que se pasa como parametro
	 * @param idEndoso
	 */
	public void eliminarEndoso(Long idEndoso);
	
	/**
	 * Guarda el endoso con la informacion que se le pasa como parametro
	 * @param idCartaSiniestro
	 * @param nombreEndoso
	 */
	public void agregarEndosos(Long idCartaSiniestroFactura, String nombreEndoso);
	
	/**
	 * Metodo que genera la impresion de la carta de perdidas totales no documentadas
	 * @param siniestroId
	 * @return
	 */
	public TransporteImpresionDTO imprimirCartaPTSNoDocumentada(Long siniestroId);
	
	/**
	 * Metodo para saber si ya se hizo la recepcion de documentos y si se marco 
	 * la perdida total como pago de danios
	 * @param idIndemnizacion
	 * @return
	 */
	public Boolean getEsRecepcionPagoDanios(Long idIndemnizacion);
	
	/**
	 * Busca una recepcion de documentos usando la indemnizacion
	 * @param idIndemnizacion
	 * @return
	 */
	public RecepcionDocumentosSiniestro buscarRecepcionDeDocumentos(Long idIndemnizacion);
	
	/**
	 * valida si la carta de baja de placas tiene todos los campos requeridos para guardar o imprimir. 
	 * @param bajaPlacasDTO
	 * @param tipoSiniestro
	 * @return
	 */
	public List<String> validarBajaPlacas(CartaBajaPlacasDTO bajaPlacasDTO, String tipoBajaPlacas);

	/**
	 * obtiene la informacion del auto para la recepcion de documentos dependiendo si es indemnizacion
	 * de asegurado o de tercero
	 * @param idIndemnizacion
	 * @return
	 */
	public CartaBajaPlacasDTO obtenerInformacionAutoRecepcionDocumentos(Long idIndemnizacion);
	
	/**
	 * valida que todos los campos de entrega de documentos esten capturados antes de guardar o imprimir
	 * @param informacionCapturada
	 * @param idIndemnizacion
	 * @return
	 */
	public List<String> validarEntregaDocumentos(CartaSiniestro informacionCapturada, String tipoCarta, Long idIndemnizacion);
	
	/**
	 * carga la informacion de la carta siniestro para poder validar los datos.
	 * @param idIndemnizacion
	 * @param tipoCarta
	 * @param informacionCapturada
	 * @return
	 */
	public CartaBajaPlacasDTO obtenerDatosBajaPlacas(Long idIndemnizacion, String tipoCarta, CartaSiniestro informacionCapturada);
	
	/**
	 * Agrega una factura a la carta de entrega de documentos
	 * @param idCartaSiniestro
	 * @param datosCapturados
	 */
	public void agregarFactura(Long idCartaSiniestro, CartaSiniestroFactura datosCapturados);
	
	/**
	 * Elimina una factura de la carta de entrega de documentos
	 * @param idFactura
	 */
	public void eliminarFactura(Long idFactura);
	
	/**
	 * Busca las facturas pertenedientes a una Carta de Entrega de Documentos
	 * @param idCartaSiniestro
	 * @return
	 */
	public List<CartaSiniestroFactura> listarFacturas(Long idCartaSiniestro);
	
	/**
	 * Imprime todas las cartas de finiquito para las determinaciones de la liquidacion 
	 * @param idIndemnizacion
	 * @param infoContrato
	 * @return
	 */
	public TransporteImpresionDTO imprimirCartaFiniquitoLiquidacion(Long idLiquidacion);
	
	/**
	 * guarda la informacion e imprime el contrato relacionado a la indemnizacion
	 * @param idIndemnizacion
	 * @return
	 */
	public TransporteImpresionDTO imprimirContratoIndemnizacion(Long idIndemnizacion, InfoContratoSiniestro infoContrato);
	
	/**
	 * Imprime los contratos relacionados a las indemnizaciones de transferencia bancaria que estan asociadas con una liquidacion de cheque
	 * se recomienda ejecutar el guardado de los contratos de la liquidacion en caso de ser necesario antes de ejecutar este metodo 
	 * 
	 * @param idIndemnizacion
	 * @param infoContrato
	 * @return
	 */
	public TransporteImpresionDTO imprimirContratoLiquidacion(Long idLiquidacion);
	
	/**
	 * guarda la informacion del contrato
	 * @param infoContrato
	 * @return
	 */
	public InfoContratoSiniestro guardarInfoContrato(InfoContratoSiniestro infoContratoCapturado, Long idIndemnizacion);
	
	/**
	 * Busca la informacion de contrato para la indemnizacion dada
	 * @param idIndemnizacion
	 * @return
	 */
	public InfoContratoSiniestro buscarInfoContrato(Long idIndemnizacion);
	
	/**
	 * guarda la informacion de los contratos asociados a la liquidacion
	 * @param infoContrato
	 * @return
	 */
	public InfoContratoSiniestro guardarInfoContratos(InfoContratoSiniestro infoContratoCapturado, Long idLiquidacion);
	
	/**
	 * Busca la informacion de contrato para la liquidacion
	 * @param idIndemnizacion
	 * @return
	 */
	public InfoContratoSiniestro buscarInfoContratos(Long idLiquidacion);
	
	/**
	 * Manda correo para pago de Perdida Total con la carta de notificacion
	 * 
	 * @param envioCaratulaParameter
	 * @param idIndemnizacion
	 * @param idValuador
	 * @return 
	 */
	public String enviarCorreoCartaNotificacionPerdidaTotal(EnvioCaratulaParameter envioCaratulaParameter, Long idIndemnizacion, Integer idValuador);

}