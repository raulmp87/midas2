package mx.com.afirme.midas2.service.siniestros.recuperacion.ingresos;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.EnvioValidacionFactura;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.ingresos.IngresoDevolucion;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos.IngresoDevolucionDTO;


@Local
public interface IngresoDevolucionService {


	public void autorizar(Long ingresoDevolucionId) throws Exception;
	public void rechazar(Long ingresoDevolucionId);
	public List<IngresoDevolucionDTO> buscar(IngresoDevolucionFiltro filtro);
	public EnvioValidacionFactura cargarFactura(File file);
	public IngresoDevolucion generar(IngresoDevolucion ingresoDevolucion, Long cuentaAcreedora);
	public IngresoDevolucion guardar(IngresoDevolucion ingresoDevolucion);
	public Map<String, String> obtenerUsuariosAutorizadores();
	public Map<String, String> obtenerUsuariosSolicitadores();
	public Map<String, String> obtenerCuentasAcreedoras();
	public IngresoDevolucion obtenerSolicitudCheque(Long ingresoDevolucionId);
	public Short cancelar(Long ingresoDevolucionId);
	public IngresoDevolucion enviarSolicitudAutorizacion(IngresoDevolucion ingresoDevolucion);

	public TransporteImpresionDTO obtenerImpresionCheque(Long ingresoDevolucionId);
	public List<EnvioValidacionFactura> obtenerFacturaCargada(Long ingresoDevolucionId);
	
	public class IngresoDevolucionFiltro {

		public enum TipoBusqueda {
		    SOLICITUD("SOL"),
		    AUTORIZACION("AUT");

		    private final String value;

		    private TipoBusqueda(String value) {
		        this.value = value;
		    }

		    public String toString() {
		        return value;
		    }
		}
		
		private String cuentaAcreedora;
		private String siniestroOrigen;
		private Double importeDesde;
		private Double importeHasta;
		private Date fechaSolicitudDesde;
		private Date fechaSolicitudHasta;
		private Date fechaAutorizacionDesde;
		private Date fechaAutorizacionHasta;
		private Date fechaChequeDesde;
		private Date fechaChequeHasta;
		private String motivoCancelacion;
		private String numChequeReferencia;
		private String tipoDevolucion;
		private String estatus;
		private String tipoRecuperacion;
		private String solicitadoPor;
		private String autorizadoPor;
		private String formaPago;
		private String numSolicitud;
		private String tipoBusqueda;

		public IngresoDevolucionFiltro(){

		}

		public String getCuentaAcreedora() {
			return cuentaAcreedora;
		}

		public void setCuentaAcreedora(String cuentaAcreedora) {
			this.cuentaAcreedora = cuentaAcreedora;
		}

		public String getSiniestroOrigen() {
			return siniestroOrigen;
		}

		public void setSiniestroOrigen(String siniestroOrigen) {
			this.siniestroOrigen = siniestroOrigen;
		}

		public Double getImporteDesde() {
			return importeDesde;
		}

		public void setImporteDesde(Double importeDesde) {
			this.importeDesde = importeDesde;
		}

		public Double getImporteHasta() {
			return importeHasta;
		}

		public void setImporteHasta(Double importeHasta) {
			this.importeHasta = importeHasta;
		}

		public Date getFechaSolicitudDesde() {
			return fechaSolicitudDesde;
		}

		public void setFechaSolicitudDesde(Date fechaSolicitudDesde) {
			this.fechaSolicitudDesde = fechaSolicitudDesde;
		}

		public Date getFechaSolicitudHasta() {
			return fechaSolicitudHasta;
		}

		public void setFechaSolicitudHasta(Date fechaSolicitudHasta) {
			this.fechaSolicitudHasta = fechaSolicitudHasta;
		}

		public Date getFechaAutorizacionDesde() {
			return fechaAutorizacionDesde;
		}

		public void setFechaAutorizacionDesde(Date fechaAutorizacionDesde) {
			this.fechaAutorizacionDesde = fechaAutorizacionDesde;
		}

		public Date getFechaAutorizacionHasta() {
			return fechaAutorizacionHasta;
		}

		public void setFechaAutorizacionHasta(Date fechaAutorizacionHasta) {
			this.fechaAutorizacionHasta = fechaAutorizacionHasta;
		}

		public Date getFechaChequeDesde() {
			return fechaChequeDesde;
		}

		public void setFechaChequeDesde(Date fechaChequeDesde) {
			this.fechaChequeDesde = fechaChequeDesde;
		}

		public Date getFechaChequeHasta() {
			return fechaChequeHasta;
		}

		public void setFechaChequeHasta(Date fechaChequeHasta) {
			this.fechaChequeHasta = fechaChequeHasta;
		}

		public String getMotivoCancelacion() {
			return motivoCancelacion;
		}

		public void setMotivoCancelacion(String motivoCancelacion) {
			this.motivoCancelacion = motivoCancelacion;
		}

		public String getNumChequeReferencia() {
			return numChequeReferencia;
		}

		public void setNumChequeReferencia(String numChequeReferencia) {
			this.numChequeReferencia = numChequeReferencia;
		}

		public String getTipoDevolucion() {
			return tipoDevolucion;
		}

		public void setTipoDevolucion(String tipoDevolucion) {
			this.tipoDevolucion = tipoDevolucion;
		}

		public String getEstatus() {
			return estatus;
		}

		public void setEstatus(String estatus) {
			this.estatus = estatus;
		}

		public String getTipoRecuperacion() {
			return tipoRecuperacion;
		}

		public void setTipoRecuperacion(String tipoRecuperacion) {
			this.tipoRecuperacion = tipoRecuperacion;
		}

		public String getSolicitadoPor() {
			return solicitadoPor;
		}

		public void setSolicitadoPor(String solicitadoPor) {
			this.solicitadoPor = solicitadoPor;
		}

		public String getAutorizadoPor() {
			return autorizadoPor;
		}

		public void setAutorizadoPor(String autorizadoPor) {
			this.autorizadoPor = autorizadoPor;
		}

		public String getFormaPago() {
			return formaPago;
		}

		public void setFormaPago(String formaPago) {
			this.formaPago = formaPago;
		}

		public String getNumSolicitud() {
			return numSolicitud;
		}

		public void setNumSolicitud(String numSolicitud) {
			this.numSolicitud = numSolicitud;
		}

		public String getTipoBusqueda() {
			return tipoBusqueda;
		}

		public void setTipoBusqueda(String tipoBusqueda) {
			this.tipoBusqueda = tipoBusqueda;
		}

	}

}