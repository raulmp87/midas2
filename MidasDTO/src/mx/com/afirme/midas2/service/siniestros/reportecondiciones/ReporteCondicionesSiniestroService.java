package mx.com.afirme.midas2.service.siniestros.reportecondiciones;

import java.io.InputStream;
import java.util.Date;

import javax.ejb.Local;

@Local
public interface ReporteCondicionesSiniestroService {

	public InputStream generaReporte(String numeroPoliza, Short tipoPoliza, Long idGerencia, 
									Long idCondicionEspecial, Date fechaSiniestroIni, Date fechaSiniestroFin);
}
