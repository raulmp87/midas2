package mx.com.afirme.midas2.service.siniestros.cabina.monitor;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.dto.siniestros.cabina.monitor.ReporteMonitorDTO;

@Local
public interface MonitorCabinaService {

	public List<ReporteMonitorDTO> getReporteSiniestrosArribo();

	public List<ReporteMonitorDTO> getReporteSiniestrosAsignacion();

	public List<ReporteMonitorDTO> getReporteSiniestrosTermino();

	public List<ReporteMonitorDTO> getReporteSiniestros();
	
	public ReporteMonitorDTO generarReporteMonitorDTO(ReporteCabina reporteCabina);
}
