package mx.com.afirme.midas2.service.siniestros.solicitudcheque;
import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.solicitudcheque.SolicitudChequeSiniestro;
/**
 * Definicion de m�todos del m�dulo de Pagos
 * @author usuario
 * @version 1.0
 * @updated 22-sep-2014 03:59:37 p.m.
 */
@Local
public interface SolicitudChequeService {
	public void  migrarChequeMizar(Long idSolCheque) throws Exception;	
	public SolicitudChequeSiniestro solicitarChequeMizar(
			Long identificador ,SolicitudChequeSiniestro.OrigenSolicitud tipo ) throws Exception;
}