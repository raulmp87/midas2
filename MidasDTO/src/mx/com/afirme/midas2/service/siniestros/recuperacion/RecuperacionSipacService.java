package mx.com.afirme.midas2.service.siniestros.recuperacion;

import java.math.BigDecimal;
import javax.ejb.Local;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionSipac;

@Local
public interface RecuperacionSipacService extends RecuperacionService {
	
	public RecuperacionSipac generar(BigDecimal valorEstimado,
			EstimacionCoberturaReporteCabina estimacion, 
			String codigoUsuario);
		
	public RecuperacionSipac obtener(Long idRecuperacion);
}
