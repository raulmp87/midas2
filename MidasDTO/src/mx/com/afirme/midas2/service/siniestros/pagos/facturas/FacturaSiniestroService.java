/**
 * 
 */
package mx.com.afirme.midas2.service.siniestros.pagos.facturas;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation.OperationType;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.facturas.DatosGralOrdenCompraDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.facturas.DevolucionesFacturasDTO;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService.CatalogoFiltro;

/**
 * @author admin
 *
 */
@Local
public interface FacturaSiniestroService {
	
	public static final String ESTATUS_FACTURA_REGISTRADA	="R";
	public static final String ESTATUS_FACTURA_CANCELADA	="C";
	public static final String ESTATUS_FACTURA_DEVUELTA		="D";
	public static final String ESTATUS_FACTURA_PAGADA		="P";
	
	public static final String CODIGOCORREO_DEVOLUCION 		= "REGISTRO_DEVOLUCION_FACTURA";
	
	public static final String REPORTE_DEVOLUCION_FACTURA = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/ReporteDevolucionFactura.jrxml";
	
	

	/**
	 * busqueda de facturas, se debe poder reutilizar para la pantalla de devolicion
	 * de factura y para la pantalla de listado de facturas devueltas, este método
	 * envía los parámetros de busqueda al dao. <b>FacturaSiniestroDAO.buscarFactura.
	 * </b>
	 * 
	 * @param filtro
	 * @return List<DevolucionesFacturasDTO>
	 */
	public List<DevolucionesFacturasDTO> buscarFacturas(FiltroFactura filtro);

	/**
	 * cambia el estatus de una factura, se utilizará en un futuro para soportar el
	 * cambio a pagado desde el módulo de pagos. invoca el método
	 * <b>FacturaSiniestroDAO.cambiarEstatusFactura</b>
	 * 
	 * @param estatus
	 * @param idFactura
	 */
	public void cambiarEstatusFactura(String estatus, Long idFactura);


	/**
	 * recibe una factura y la guarda con estatus registrada, se debe de actualizar el
	 * numero de factura en la orden de compra, invoca el método
	 * <b>FacturaSiniestroDAO.guardarFactura</b>, pasando como parámetro la factura
	 * recibida
	 * 
	 * @param factura
	 */
	public void registrarFactura(DocumentoFiscal factura, Long idOrdenCompra);

	/**
	 *valida que la factura no se haya registrado anteriormente
	 * 
	 * @param Stirng factura
	 */
	public void validarFacturaRegistrada(DocumentoFiscal facturaSiniestro)throws Exception ;
	
	/**
	 * Obtiene los datos necesarios para presentar en la cabecra de la factura
	 * @param idOrdenCompra
	 * @return DatosGralOrdenCompraDTO
	 */
	public DatosGralOrdenCompraDTO getCabeceraOrdenCompra(Long idOrdenCompra);
	
	/**
	 * Valida si la Orden de compra ya tiene alguna factura asignada
	 * @param idOrdenCompra
	 * @throws Exception
	 */
	public void validarSiYaTieneFacturaRegistrada(Long idOrdenCompra) throws Exception;
	
	/**
	 * obtiene la lista de facturas devueltas para la orden compra 
	 * @param idOrdenCompra
	 * @return List<HistorialDevolucionesFacturasDTO>
	 */
	public List<DevolucionesFacturasDTO> obtenerDevolucionesOrdenCompra(Long idOrdenCompra);

	
	/**
	 * Guarda la factura en estatus Devuelta y envia los correos necesarios.
	 * @param factura
	 */
	public void devolverFactura(DocumentoFiscal factura);
	
	
	/**
	 * edita la informacion de Persona a la que se le entrego y la fecha de entrega, a los ids de  las ediciones masivas proporcionadas 
	 * @param idsEdicionMasivaDevolucion
	 * @param entregadaA
	 * @param fechaEntrega
	 */
	public void realizarEdicionMasivaDevoluciones(String idsEdicionMasivaDevolucion,String entregadaA , Date fechaEntrega);
	
	
	/**
	 * Obtiene la Factura Siniestro del id proporcionado
	 * @param idFacturaSiniestro
	 * @return FacturaSiniestro
	 */
	public DocumentoFiscal getFacturaById(Long idFacturaSiniestro);
	
	/**
	 * Valida que el monto total de la factura coincida con el monto total de la orden de compra 
	 * @param factura
	 * @throws Exception
	 */
	public Boolean validacionMontoTotalOrdenCompraFactura( DocumentoFiscal factura , Long idOrdenCompra);
	
	
	/**
	 * Metodo para generar el PDF de la Devolucion
	 * @param idFacturaSiniestro
	 * @return
	 */
	public TransporteImpresionDTO imprimirDevolucionFactura(Long idFacturaSiniestro);
	
	
	/**
	 * Envia correo cuando se devuelve una factura
	 * @param factura
	 */
	public void envioCorreoDevolucionFactura(DocumentoFiscal factura) ;
	
	
	/**
	 * Envia correo para solicitar la cancelacion de la Orden Compra 
	 * @param factura
	 */
	public void envioCorreosolicitarCancelacionOrdenCompra(DocumentoFiscal factura);
	
	
	/**
	 * Valida si la orden de compra ya tiene una factura registrada antes de devolver una, de ser asi, solicita que la devuelva primero antes de devolver otra
	 * @param idOrdenCompra
	 * @param factura
	 * @throws Exception
	 */
	public void validateOrdenCompraConFacturaRegistradaAntesDevolver(Long idOrdenCompra,String factura) throws Exception;
	
	
	/**
	 * Edita la informacion que se puede editar de la FacturaSiniestro
	 * @param factura
	 */
	public void editarDevolucion(DocumentoFiscal factura);
	
	
	/**
	 * Valida que la factura del proveedor no se haya devuelto previmente
	 * @param factura
	 * @throws Exception
	 */
	public void validateNumeroFacturaYaDevuelta(DocumentoFiscal factura) throws Exception;
	
	
	
	public class FiltroFactura extends CatalogoFiltro{
		
		private Long ordenCompra;
		
		private String factura;
		
		private String proveedor;
		
		private String motivoDevolucion;
		
		private Date fechaDevolucionInicio;
		
		private Date fechaDevolucionFin;
		
		private String devueltaPor;
		
		private Date fechaEntregaInicio;
		
		private Date fechaEntregaFin;
		
		private String entregadaPor;
		
		private String tipoProveedor;
		private Long idBeneficiario;/*id del proveedor*/
		
		
		

		@FilterPersistenceAnnotation(persistenceName="ordenCompra.tipoProveedor")
		public String getTipoProveedor() {
			return tipoProveedor;
		}

		public void setTipoProveedor(String tipoProveedor) {
			this.tipoProveedor = tipoProveedor;
		}
		@FilterPersistenceAnnotation(persistenceName="ordenCompra.idBeneficiario")
		public Long getIdBeneficiario() {
			return idBeneficiario;
		}

		public void setIdBeneficiario(Long idBeneficiario) {
			this.idBeneficiario = idBeneficiario;
		}

		/**
		 * @return the ordenCompra
		 */
		 @FilterPersistenceAnnotation(persistenceName="ordenCompra.id")
		public Long getOrdenCompra() {
			return ordenCompra;
		}

		/**
		 * @param ordenCompra the ordenCompra to set
		 */
		public void setOrdenCompra(Long ordenCompra) {
			this.ordenCompra = ordenCompra;
		}

		/**
		 * @return the factura
		 */
		@FilterPersistenceAnnotation(persistenceName="numeroFactura" ,  operation=OperationType.LIKE)
		public String getFactura() {
			return factura;
		}

		/**
		 * @param factura the factura to set
		 */
		public void setFactura(String factura) {
			this.factura = factura;
		}

		/**
		 * @return the proveedor
		 */
		 @FilterPersistenceAnnotation(persistenceName="ordenCompra.nomBeneficiario" ,  operation=OperationType.LIKE)
		public String getProveedor() {
			return proveedor;
		}

		/**
		 * @param proveedor the proveedor to set
		 */
		public void setProveedor(String proveedor) {
			this.proveedor = proveedor;
		}
		
		/**
		 * @return the mmotivo
		 */
		@FilterPersistenceAnnotation(persistenceName="motivoDevolucion" )
		public String getMotivoDevolucion() {
			return motivoDevolucion;
		}

		/**
		 * @param mmotivo the mmotivo to set
		 */
		public void setMotivoDevolucion(String motivoDevolucion) {
			this.motivoDevolucion = motivoDevolucion;
		}

		/**
		 * @return the fechaDevolucionInicio
		 */
		@FilterPersistenceAnnotation(persistenceName="fechaDevolucion", truncateDate=true , operation=OperationType.GREATERTHANEQUAL, paramKey="fechaDevolucionInicio")
		public Date getFechaDevolucionInicio() {
			return fechaDevolucionInicio;
		}

		/**
		 * @param fechaDevolucionInicio the fechaDevolucionInicio to set
		 */
		public void setFechaDevolucionInicio(Date fechaDevolucionInicio) {
			this.fechaDevolucionInicio = fechaDevolucionInicio;
		}

		/**
		 * @return the fechaDevolucionFin
		 */
		@FilterPersistenceAnnotation(persistenceName="fechaDevolucion", truncateDate=true , operation=OperationType.LESSTHANEQUAL, paramKey="fechaDevolucionFin")
		public Date getFechaDevolucionFin() {
			return fechaDevolucionFin;
		}

		/**
		 * @param fechaDevolucionFin the fechaDevolucionFin to set
		 */
		public void setFechaDevolucionFin(Date fechaDevolucionFin) {
			this.fechaDevolucionFin = fechaDevolucionFin;
		}

		/**
		 * @return the devueltaPor
		 */
		@FilterPersistenceAnnotation(persistenceName="usuarioDevolucion" ,  operation=OperationType.LIKE)
		public String getDevueltaPor() {
			return devueltaPor;
		}

		/**
		 * @param devueltaPor the devueltaPor to set
		 */
		public void setDevueltaPor(String devueltaPor) {
			this.devueltaPor = devueltaPor;
		}

		/**
		 * @return the fechaEntregaInicio
		 */
		@FilterPersistenceAnnotation(persistenceName="fechaEntrega", truncateDate=true , operation=OperationType.GREATERTHANEQUAL, paramKey="fechaEntregaInicio")
		public Date getFechaEntregaInicio() {
			return fechaEntregaInicio;
		}

		/**
		 * @param fechaEntregaInicio the fechaEntregaInicio to set
		 */
		public void setFechaEntregaInicio(Date fechaEntregaInicio) {
			this.fechaEntregaInicio = fechaEntregaInicio;
		}

		/**
		 * @return the fechaEntregaFin
		 */
		@FilterPersistenceAnnotation(persistenceName="fechaEntrega", truncateDate=true , operation=OperationType.LESSTHANEQUAL, paramKey="fechaEntregaFin")
		public Date getFechaEntregaFin() {
			return fechaEntregaFin;
		}

		/**
		 * @param fechaEntregaFin the fechaEntregaFin to set
		 */
		public void setFechaEntregaFin(Date fechaEntregaFin) {
			this.fechaEntregaFin = fechaEntregaFin;
		}

		/**
		 * @return the entregadaPor
		 */
		@FilterPersistenceAnnotation(persistenceName="entregada" ,  operation=OperationType.LIKE)
		public String getEntregadaPor() {
			return entregadaPor;
		}

		/**
		 * @param entregadaPor the entregadaPor to set
		 */
		public void setEntregadaPor(String entregadaPor) {
			this.entregadaPor = entregadaPor;
		}
	}
	
	
}
