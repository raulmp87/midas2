package mx.com.afirme.midas2.service.siniestros.expedientejuridico;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.expedientejuridico.ComentarioJuridico;
import mx.com.afirme.midas2.domain.siniestros.expedientejuridico.ExpedienteJuridico;
import mx.com.afirme.midas2.dto.expedientejuridico.ExpedienteJuridicoDTO;
import mx.com.afirme.midas2.dto.expedientejuridico.ExpedienteJuridicoExportableDTO;

/**
 * invocar expedienteJuridicoDAO.buscarExpedientes
 * y regresr la lista
 * @author Israel
 * @version 1.0
 * @created 06-ago.-2015 10:30:09 a. m.
 */
@Local
public interface ExpedienteJuridicoService {

	/**
	 * Validar si se tiene valor en comentarioProveedor y generar un objeto
	 * comentarioJuridico y realizar la misma operacion para comentarioAfirme.
	 * 
	 * Llenar los campos de control
	 * 
	 * Agregar lo(s) objetos creado(s) a la lista de comentarios de expedienteJuridico
	 * 
	 * @param expedienteJuridico
	 * @param comentarioAfirme
	 * @param comentarioProveedor
	 */
	public void crearComentarios(ExpedienteJuridico expedienteJuridico, String comentarioAfirme, String comentarioProveedor);
	/**
	 * Crear una lista de expotables por cada registro
	 * 
	 * @param expedientes
	 */
	public List<ExpedienteJuridicoExportableDTO> crearExpedientesExportables(List<ExpedienteJuridico> expedientes);

	/**
	 * invocar expedienteJuridicoDAO.buscarExpedientes
	 * y retornar la respuesta
	 * 
	 * @param expedienteJuridicoDTO
	 */
	public List<ExpedienteJuridico> buscarExpedientes(ExpedienteJuridicoDTO expedienteJuridicoDTO);

	/**
	 * acompletar los campos de control
	 * invocarCrearComentarios
	 * y ejecutar el entidadService.save
	 * 
	 * @param expedienteJuridico
	 * @param comentarioAfirme
	 * @param comentarioProveedor
	 */
	public ExpedienteJuridico guardar(ExpedienteJuridico expedienteJuridico, String comentarioAfirme, String comentarioProveedor);
	
	public List<ComentarioJuridico> obtenerComentarios(Long idExpedienteJuridico);

}