package mx.com.afirme.midas2.service.siniestros.catalogo.oficina;

import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation.OperationType;
import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService;

public interface CatalogoSiniestroOficinaService extends
		CatalogoSiniestroService {

	public class OficinaFiltro extends CatalogoFiltro {

		private String claveOficina;
		private Integer estatus;
		private String nombreOficina;
		private Integer id;

		public String getClaveOficina() {
			return claveOficina;
		}

		public void setClaveOficina(String claveOficina) {
			this.claveOficina = claveOficina;
		}

		public Integer getEstatus() {
			return estatus;
		}

		public void setEstatus(Integer estatus) {
			this.estatus = estatus;
		}

		@FilterPersistenceAnnotation(persistenceName="nombreOficina", operation=OperationType.LIKE)
		public String getNombreOficina() {
			return nombreOficina;
		}

		public void setNombreOficina(String nombreOficina) {
			this.nombreOficina = nombreOficina;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

	}

	public List<EstadoMidas> estadosDisponiblesPorPais(String paisId,
			Long oficinaId);

	public Oficina salvarOficina(Oficina oficina);

	public Oficina actualizarDatosOficina(Oficina oficina, Long idEntidad);

	public Oficina relacionarOficina(Long idOficina,
			EstadoMidas estadoTransaccion);

	public Oficina eliminarRelacion(Long idOficina, String idEstadoTrans);

	public Oficina relacionarTodosEstados(Long idOficina,
			List<EstadoMidas> estadosDisponibles);

	public Oficina eliminarRelacionTodosEstados(Long idOficina);
	
	public String validaOficinaConTipoDeServicio(Long oficinaId, String tipoServicio);
	
	public Oficina obtenerWsEnpoint(Oficina oficina); 

}
