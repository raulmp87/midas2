package mx.com.afirme.midas2.service.siniestros.provision;

import java.math.BigDecimal;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.provision.MovimientoProvisionSiniestros;

@Local
public interface SiniestrosProvisionService {
	
	
	public void cancelaProvisionesDeCiaPorReporte(Long reporteId);

	public void habilitaProvisionesDeCiaPorReporte(Long reporteCabinaId);
	
	public MovimientoProvisionSiniestros generaMovimientosAlHabilitar(EstimacionCoberturaReporteCabina estimacion);
	
	public void generaMovimientoDeAjusteReserva(Long estimacionId);
	
	public void actualizaProvision(Long idReferencia,MovimientoProvisionSiniestros.Origen origen);
	
	public void creaProvision(Long idMovimientoProvision);
	
	public void cancelaProvision(Long idMovimientoProvision);
	
	public void generaYGuardaMovimientoDeAplicacionDeIngreso(Long ingresoId);
	
	public void generaYGuardaMovimientoDeExclusionDeCartaCia(Long recuperacionCiaId);
	
	public void revierteProvisionAlCancelarRecuperacionCia(Long recuperacionId);
	
	public void creaMovimientosAlCancelarRecuperacionCia(Long recuperacionId);
	
	public MovimientoProvisionSiniestros creaMovimientosAlAsignarGruasRecuperacionCia(Long recuperacionId,BigDecimal montoGruasInicial);
	
	public void actualizaProvisionAlAsignarGruas(Long recuperacionId,BigDecimal montoGruasInicial);
	
	public  EstimacionCoberturaReporteCabina obtenerEstimacionDeRecuperacionCia(Long recuperacionId);
	
	public BigDecimal obtenerMontoConsiderandoPorcentajeParticipacion(Long estimacionId, BigDecimal montoInicial,Integer porcentajeDeParticipacion );
	
	public BigDecimal obtenerMontoRecuperacionesConsiderandoPorcentajePorEstimacion(Long estimacionId);

	public MovimientoProvisionSiniestros generaMovimientoDeRoboTotal(Long idSalvamento, BigDecimal monto);

	public MovimientoProvisionSiniestros obtenerMovimientoProvisionado(Long idReferencia,MovimientoProvisionSiniestros.Origen origen);

}
