package mx.com.afirme.midas2.service.siniestros.cabina.reportecabina;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.CausaMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.TipoDocumentoMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.TipoMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoSiniestro;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.MovimientoSiniestroDTO;


/**
 * @author Lizeth De La Garza
 *
 */
@Local
public interface MovimientoSiniestroService {
	

	
	/**
	 * Método que obtiene el total del Importe de Movimientos dependiendo de los filtros de búsqueda
	 * @param reporteCabinaId       - Importe de movimientos a nivel Reporte de Cabina (opcional) (id de ReporteCabina)
	 * @param coberturaId           - Importe de movimientos a nivel Cobertura (opcional) (id de CoberturaReporteCabina)
	 * @param estimacionId          - Importe de movimientos a nivel Afectacion/Pase de Atención (opcional) (id de EstimacionCoberturaReporteCabina)
	 * @param claveTipoCalculo      - Importe de movimientos a nivel Coberturas con esta clave de calculo (opcional)
	 * @param claveSubCalculo       - Importe de movimientos a nivel Coberturas con esta clave de sub cobertura (opcional)
	 * @param tipoDocumento         - Importe de movimientos para este tipo de documento (opcional)
	 * @param tipoMovimiento        - Importe de movimientos a nivel para este tipo de movimiento (opcional)
	 * @param causaMovimiento       - Importe de movimientos a nivel para esta causa de movimiento (opcional) 
	 * @param fechaUltimoMovimiento - Importe de movimientos anteriores o iguales a esta fecha (opcional)
	 * @return
	 */
	public BigDecimal obtenerImporteMovimientos(Long reporteCabinaId, Long coberturaId, Long estimacionId, 
			String claveTipoCalculo, String claveSubCalculo, TipoDocumentoMovimiento tipoDocumento, 
			TipoMovimiento tipoMovimiento, CausaMovimiento causaMovimiento, Date fechaUltimoMovimiento);

	/**
	 * Método que obtiene el total del Importe de Movimientos de tipo Estimacion (Ajustes de Reserva)
	 * @param reporteCabinaId       - Importe de movimientos a nivel Reporte de Cabina (opcional) (id de ReporteCabina)
	 * @param coberturaId           - Importe de movimientos a nivel Cobertura (opcional) (id de CoberturaReporteCabina)
	 * @param estimacionId          - Importe de movimientos a nivel Afectacion/Pase de Atención (opcional) (id de EstimacionCoberturaReporteCabina)
	 * @param claveTipoCalculo      - Importe de movimientos a nivel Coberturas con esta clave de calculo (opcional) 
	 * @param claveSubCalculo       - Importe de movimientos a nivel Coberturas con esta clave de sub cobertura (opcional) 
	 * @param fechaUltimoMovimiento - Importe de movimientos anteriores o iguales a esta fecha (opcional) 
	 * @return
	 */
	public BigDecimal obtenerMontoEstimacionActual(Long reporteCabinaId, Long coberturaId, Long estimacionId, 
			String claveTipoCalculo, String claveSubCalculo, Date fechaCreacionUltimoMovimiento);

	
	/**
	 * Método que obtiene el total del Importe de Movimientos de tipo Pagos
	 * @param reporteCabinaId       - Importe de movimientos a nivel Reporte de Cabina (opcional) (id de ReporteCabina)
	 * @param coberturaId           - Importe de movimientos a nivel Cobertura (opcional) (id de CoberturaReporteCabina)
	 * @param estimacionId          - Importe de movimientos a nivel Afectacion/Pase de Atención (opcional) (id de EstimacionCoberturaReporteCabina)
	 * @param claveTipoCalculo      - Importe de movimientos a nivel Coberturas con esta clave de calculo (opcional) 
	 * @param claveSubCalculo       - Importe de movimientos a nivel Coberturas con esta clave de sub cobertura (opcional) 
	 * @param fechaUltimoMovimiento - Importe de movimientos anteriores o iguales a esta fecha (opcional) 
	 * @return
	 */
	public BigDecimal obtenerMontoPagos(Long reporteCabinaId, Long coberturaId, Long estimacionId, 
			String claveTipoCalculo ,String claveSubCalculo,  Date fechaCreacionUltimoMovimiento);

	/**
	 * Método que obtiene el total del Importe de Movimientos de tipo Ingresos
	 * @param reporteCabinaId       - Importe de movimientos a nivel Reporte de Cabina (opcional) (id de ReporteCabina)
	 * @param coberturaId           - Importe de movimientos a nivel Cobertura (opcional) (id de CoberturaReporteCabina)
	 * @param estimacionId          - Importe de movimientos a nivel Afectacion/Pase de Atención (opcional) (id de EstimacionCoberturaReporteCabina)
	 * @param claveTipoCalculo      - Importe de movimientos a nivel Coberturas con esta clave de calculo (opcional) 
	 * @param claveSubCalculo       - Importe de movimientos a nivel Coberturas con esta clave de sub cobertura (opcional) 
	 * @param fechaUltimoMovimiento - Importe de movimientos anteriores o iguales a esta fecha (opcional) 
	 * @return
	 */
	public BigDecimal obtenerMontoIngresos(Long reporteCabinaId, Long coberturaId, Long estimacionId, 
			String claveTipoCalculo, String claveSubCalculo,  Date fechaCreacionUltimoMovimiento);
	
	
	/**
	 * Método que obtiene el total del Importe de Reserva
	 * @param reporteCabinaId       - Reserva a nivel Reporte de Cabina (opcional) (id de ReporteCabina)
	 * @param coberturaId           - Reserva a nivel Cobertura (opcional) (id de CoberturaReporteCabina)
	 * @param estimacionId          - Reserva a nivel Afectacion/Pase de Atención (opcional) (id de EstimacionCoberturaReporteCabina)
	 * @param claveTipoCalculo      - Reserva a nivel Coberturas con esta clave de calculo (opcional)
	 * @param claveSubCalculo       - Reserva a nivel Coberturas con esta clave de sub cobertura (opcional)
	 * @param fechaUltimoMovimiento - Reserva correspondiente a esta fecha (opcional)
	 * @param obtenerDisponible     - Si es true, se consideran las ordenes de compra en tramite o pendientes por autorizar
	 * @return
	 */
	public BigDecimal obtenerReserva(Long reporteCabinaId, Long coberturaId, Long estimacionId, 
			String claveTipoCalculo, String claveSubCalculo,  Date fechaCreacionUltimoMovimiento, Boolean obtenerDisponible);
	
	
	
	/**
	 * Método que obtiene el total del Importe de Movimientos de tipo Gasto de ajuste y Reeembolso de Gastos de Ajuste en base a filtros
	 * @param reporteCabinaId       - Importe de movimientos a nivel Reporte de Cabina (opcional) (id de ReporteCabina)
	 * @param ordenCompraId			- Importe de movimientos a nivel Orden de Compra (opcional) (id de OrdenCompra)
	 * @param tipoDocumento         - Importe de movimientos para este tipo de documento (opcional)
	 * @param tipoMovimiento        - Importe de movimientos a nivel para este tipo de movimiento (opcional)
	 * @param causaMovimiento       - Importe de movimientos a nivel para esta causa de movimiento (opcional)
	 * @param fechaUltimoMovimiento - Importe de movimientos anteriores o iguales a esta fecha (opcional)
	 * @return
	 */
	public BigDecimal obtenerImporteMovimientosGA(Long reporteCabinaId, Long ordenCompraId, 
			TipoDocumentoMovimiento tipoDocumento, TipoMovimiento tipoMovimiento, CausaMovimiento causaMovimiento, Date fechaUltimoMovimiento);
	
	/**
	 * Método que genera un Movimiento de Ingreso, Pago o Afectacion de Estimacion para una Afectacion/Pase de Atencion
	 * @param estimacionId          - ID de Afectacion/Pase de Atencion a la que se generara el movimiento (EstimacionCoberturaReporteCabina)
	 * @param importeMovimiento     - Importe del movimiento
	 * @param tipoDocumento         - Tipo de Documento
	 * @param tipoMovimiento        - Tipo de Movimiento
	 * @param causaMovimiento       - Causa del Movimiento
	 * @param codigoUsuario         - Usuario creacion (opcional)
	 * @param descripcion           - Descripcion del movimiento (opcional)
	 * @return
	 */	
	public MovimientoCoberturaSiniestro  generarMovimiento(Long idEstimacionCobertura , BigDecimal importeMovimiento, 
			TipoDocumentoMovimiento tipoDocumento, TipoMovimiento tipoMovimiento, CausaMovimiento causaMovimiento, 
			String codigoUsuario, String descripcion);
	
	/**
	 * Método que genera un Movimiento para Gasto de Ajuste o Reembolso de Gasto de Ajuste
	 * @param idReporteCabina       - ID del Reporte (ReporteCabina)
	 * @param idOrdenCompra         - ID de la Orden de compra (OrdenCompra)
	 * @param importeMovimiento     - Importe del movimiento
	 * @param tipoDocumento         - Tipo de Documento
	 * @param tipoMovimiento        - Tipo de Movimiento
	 * @param causaMovimiento       - Causa del Movimiento
	 * @param codigoUsuario         - Usuario creacion (opcional)
	 * @param descripcion           - Descripcion del movimiento (opcional)
	 * @return
	 */
	public MovimientoSiniestro  generarMovimientoGA(Long idReporteCabina , Long idOrdenCompra, BigDecimal importeMovimiento, 
			TipoDocumentoMovimiento tipoDocumento, TipoMovimiento tipoMovimiento, CausaMovimiento causaMovimiento, 
			String codigoUsuario, String descripcion);
	
	/**
	 * Método que genera un Movimiento de Ajuste a la Estimacion de una Afectacion/Pase de Atencion en base a la Reserva que se quiere mantener
	 * Internamente se calculara la diferencia entre la Reserva deseada y la Reserva Disponible para generar un ajuste de Aumento o Disminucion
	 * @param estimacionId          - ID de Afectacion/Pase de Atencion a la que se generara el movimiento (EstimacionCoberturaReporteCabina)
	 * @param importeMovimiento     - Importe del movimiento
	 * @param causaMovimiento       - Causa del Movimiento
	 * @param codigoUsuario         - Usuario creacion (opcional)
	 * @return
	 */
	public MovimientoCoberturaSiniestro generarAjusteReserva(Long idEstimacionCobertura , BigDecimal importeReserva,
			CausaMovimiento causaMovimiento, String codigoUsuario);
	
	/**
	 * Método que genera un Movimiento de Ajuste a la Estimacion de una Afectacion/Pase de Atencion en base a la nueva Estimacion que se desea
	 * Internamente se calculara la diferencia entre la Estimacion Nueva y la Estimacion Actual para generar un ajuste de Aumento o Disminucion
	 * @param estimacionId          - ID de Afectacion/Pase de Atencion a la que se generara el movimiento (EstimacionCoberturaReporteCabina)
	 * @param importeMovimiento     - Importe del movimiento
	 * @param causaMovimiento       - Causa del Movimiento
	 * @param codigoUsuario         - Usuario creacion (opcional)
	 * @return
	 */
	public MovimientoCoberturaSiniestro  generarAjusteEstimacion(Long idEstimacionCobertura , BigDecimal importeEstimacion, 
			CausaMovimiento causaMovimiento, String codigoUsuario);
	
	/**
	 * Método que obtiene el total del Importe de Movimientos de tipo Estimacion (Ajustes de Reserva) para un Reporte de Cabina
	 * @param reporteCabinaId       - ID de Reporte de Cabina (ReporteCabina)
	 * @return
	 */
	public BigDecimal obtenerMontoEstimacionReporte(Long reporteCabinaId);
	
	/**
	 * Método que obtiene el total del Importe de Movimientos de tipo Estimacion (Ajustes de Reserva) para una Cobertura o SubCobertura
	 * @param coberturaId           - Id de la cobertura (CoberturaReporteCabina)
	 * @param claveSubCalculo       - clave de la subcobertura (opcional)
	 * @return
	 */
	public BigDecimal obtenerMontoEstimacionCobertura(Long coberturaId, String claveSubCalculo);
	
	/**
	 * Método que obtiene el total del Importe de Movimientos de tipo Estimacion (Ajustes de Reserva) para una Afectacion/Pase de Atencion
	 * @param estimacionId          - Id de la Afectacion/Pase de Atención (EstimacionCoberturaReporteCabina)
	 * @return
	 */
	public BigDecimal obtenerMontoEstimacionAfectacion(Long estimacionId);
	
	/**
	 * Método que obtiene el total del Importe de Movimientos de tipo Estimacion para los Gastos de Ajuste o Reeembolso de Gastos de Ajuste
	 * @param reporteCabinaId       - Id del Reporte de Cabina (ReporteCabina)
	 * @return
	 */
	public BigDecimal obtenerMontoEstimacionGA(Long reporteCabinaId);
	
	/**
	 * Método que obtiene el total del Importe de Movimientos de tipo Pago para un Reporte de Cabina
	 * @param reporteCabinaId       - ID de Reporte de Cabina (ReporteCabina)
	 * @return
	 */
	public BigDecimal obtenerMontoPagosReporte(Long reporteCabinaId);
	
	/**
	 * Método que obtiene el total del Importe de Movimientos de tipo Pago para una Cobertura o SubCobertura
	 * @param coberturaId           - Id de la cobertura (CoberturaReporteCabina)
	 * @param claveSubCalculo       - clave de la subcobertura (opcional)
	 * @return
	 */
	public BigDecimal obtenerMontoPagosCobertura(Long coberturaId, String claveSubCalculo);
	
	/**
	 * Método que obtiene el total del Importe de Movimientos de tipo Pago para una Afectacion/Pase de Atencion
	 * @param estimacionId          - Id de la Afectacion/Pase de Atención (EstimacionCoberturaReporteCabina)
	 * @return
	 */
	public BigDecimal obtenerMontoPagosAfectacion(Long estimacionId);
	
	/**
	 * Método que obtiene el total del Importe de Movimientos de tipo Pago para los Gastos de Ajuste o Reeembolso de Gastos de Ajuste
	 * @param reporteCabinaId       - Id del Reporte de Cabina (ReporteCabina)
	 * @return
	 */
	public BigDecimal obtenerMontoPagosGA(Long reporteCabinaId);
	
	/**
	 * Método que obtiene el total del Importe de Movimientos de tipo Ingreso para un Reporte de Cabina
	 * @param reporteCabinaId       - ID de Reporte de Cabina (ReporteCabina)
	 * @return
	 */
	public BigDecimal obtenerMontoIngresosReporte(Long reporteCabinaId);
	
	/**
	 * Método que obtiene el total del Importe de Movimientos de tipo Ingreso para una Cobertura o SubCobertura
	 * @param coberturaId           - Id de la cobertura (CoberturaReporteCabina)
	 * @param claveSubCalculo       - clave de la subcobertura (opcional)
	 * @return
	 */
	public BigDecimal obtenerMontoIngresosCobertura(Long coberturaId, String claveSubCalculo);
	
	/**
	 * Método que obtiene el total del Importe de Movimientos de tipo Ingreso para una Afectacion/Pase de Atencion
	 * @param estimacionId          - Id de la Afectacion/Pase de Atención (EstimacionCoberturaReporteCabina)
	 * @return
	 */
	public BigDecimal obtenerMontoIngresosAfectacion(Long estimacionId);
	
	/**
	 * Método que obtiene el total del Importe de Movimientos de tipo Ingreso para los Gastos de Ajuste o Reeembolso de Gastos de Ajuste
	 * @param reporteCabinaId       - Id del Reporte de Cabina (ReporteCabina)
	 * @return
	 */
	public BigDecimal obtenerMontoIngresosGA(Long reporteCabinaId);
	
	/**
	 * Método que obtiene el total del Importe de Reserva de un Reporte de Cabina
	 * @param reporteCabinaId       - ID de Reporte de Cabina  (ReporteCabina)

	 * @param obtenerDisponible     - Si es true, se consideran las ordenes de compra en tramite o pendientes por autorizar
	 * @return
	 */
	public BigDecimal obtenerReservaReporte(Long reporteCabinaId,  Boolean obtenerDisponible);
	
	/**
	 * Método que obtiene el total del Importe de Reserva de una Cobertura o Sub Cobertura
	 * @param coberturaId           - ID de la Cobertura (CoberturaReporteCabina)	
	 * @param claveSubCalculo       - clave de la subcobertura (opcional)
	 * @param obtenerDisponible     - Si es true, se consideran las ordenes de compra en tramite o pendientes por autorizar
	 * @return
	 */
	public BigDecimal obtenerReservaCobertura(Long coberturaId, String claveSubCalculo, Boolean obtenerDisponible);
	
	/**
	 * Método que obtiene el total del Importe de Reserva de una Afectacion/Pase de Atención 
	 * @param estimacionId          - ID de la Afectacion/Pase de Atención (EstimacionCoberturaReporteCabina)
	 * @param obtenerDisponible     - Si es true, se consideran las ordenes de compra en tramite o pendientes por autorizar
	 * @return
	 */
	public BigDecimal obtenerReservaAfectacion(Long estimacionId, Boolean obtenerDisponible);
	
	/**
	 * Método que invoca la Interfaz Contable de movimientos de Tipo de Ajuste de Reserva (tipoDocumento = EST)
	 * @param movimiento
	 */
	public void invocarInterfazContable(MovimientoCoberturaSiniestro movimiento);
	
	/**
	 * Obtiene el historico de movimientos dependiendo de los parametros que se le indican en el filtro
	 * @param filtroBusqueda
	 * @return
	 */
	public List<MovimientoSiniestroDTO> obtenerHistoricoMovimientos(MovimientoSiniestroDTO filtroBusqueda);
	
	/**
	 * Obtiene el historico de movimientos de Robo dependiendo de los parametros que se le indican en el filtro
	 * @param filtroBusqueda
	 * @return
	 */
	public List<MovimientoSiniestroDTO> obtenerHistoricoMovimientosRobos(MovimientoSiniestroDTO filtroBusqueda);
	
	/**
	 * Obtiene el historico de movimientos de Gasto de Ajuste y Reembolso de Gasto de Ajuste
	 * dependiendo de los parametros que se le indican en el filtro
	 * @param filtroBusqueda
	 * @return
	 */
	public List<MovimientoSiniestroDTO> obtenerHistoricoMovimientosGA(MovimientoSiniestroDTO filtroBusqueda);
	
	/**
	 * Invoca el metodo obtenerUsuariosMovSiniestros de MovSiniestroDao 
	 * @return
	 */
	public Map<String,String> obtenerUsuariosMovSiniestros();
	
	/**
	 * Obtiene el importe de los movimientos dependiendo de los parametros que reciba en el filtro de busqueda
	 * @param filtroBusqueda
	 * @return
	 */
	public BigDecimal obtenerImporteMovimientos(MovimientoSiniestroDTO filtroBusqueda);
	
	/**
	 * Obtiene el importe de los movimientos de gastos de ajuste dependiendo de los parametros en el filtro de busqueda
	 * @param filtroBusqueda
	 * @return
	 */
	public BigDecimal obtenerImporteMovimientosGA(MovimientoSiniestroDTO filtroBusqueda);
}
