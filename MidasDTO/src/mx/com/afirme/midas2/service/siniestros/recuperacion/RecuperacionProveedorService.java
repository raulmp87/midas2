package mx.com.afirme.midas2.service.siniestros.recuperacion;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.recuperacion.FolioVentaRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.EstatusRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionProveedor;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.ReferenciaBancaria;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.DatosRecuperacionDTO;


/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 19-may-2015 12:37:30 p.m.
 */
@Local
public interface RecuperacionProveedorService extends RecuperacionService {

	
	/**
	 * <b>SE AGREGA EN DISE�O CANCELAR RECUPERACION PROVEEDOR</b>
	 * <b>
	 * </b>M�todo que cancela una Recuperaci�n de Proveedor.
	 * 
	 * Buscar la RecuperacionProveedor <b>recuperacion </b>mediante entidadService.
	 * findById
	 * 
	 * Invocar el m�todo recuperacionService.cancelarRecuperacion para cambiar el
	 * <b>estatus </b>de la <b>recuperacion </b>a CANCELADO, asignar el
	 * <b>motivoCancelacion </b>y la <b>fechaCancelacion.</b>
	 * <b>
	 * </b>Invocar al m�todo recuperacionService.cancelarIngreso para cancelar el
	 * <b>ingreso </b>asociado.
	 * 
	 * Si el <b>medio </b>de la <b>recuperacion </b>es NOTACRED(Nota de Cr�dito),
	 * entonces se deberan eliminar los registros asociados en la tabla. La
	 * recuperacion internamente tendra un listado de notasCredito, hacer un
	 * entidadService.remove
	 * 
	 * @param idRecuperacion
	 * @param motivo
	 */
	public String cancelarRecuperacionProveedor(Long idRecuperacion, String motivo);

	/**
	 * <b>SE AGREGA EN DISE�O EDITAR RECUPERACION PROVEEDOR</b>
	 * <b>
	 * </b>M�todo que genera los folios de Venta para una Recuperaci�n de Proveedor.
	 * 
	 * Se verifica que no existan folios generados previamente.
	 * 
	 * Buscar la RecuperacionProveedor <b>recuperacion </b>por un entidadService.
	 * findById
	 * 
	 * 
	 * Buscar si contiene un listado de folios ya generados, hacer un List<FolioVenta>
	 * folios = entidadService.findByProperty(FolioVenta.class, "recuperacion.id");
	 * 
	 * Si no existen folios, revisar lo siguiente
	 * 
	 * -Si la <b>recuperacion </b>es de <b>origen </b>Manual, se crear� un solio
	 * FolioVenta en base a la <i>RDN de Generaci�n de Folio de Venta</i> con el
	 * <b>conceptoDevolucion </b>de la <b>recuperacion</b>
	 * 
	 * -Si la <b>recuperacion </b>es de <b>origen </b>AUTOMATICA, Hacer un split por
	 * comas del <b>conceptoDevolucion</b>. Iterar el array y por cada uno crear un
	 * objeto de FolioVentaRecuperacion con el nombre de ese concepto. se crear� el
	 * folio en base a la <i>RDN de Generaci�n de Folio de Venta</i>
	 * 
	 * @param recuperacionId
	 */
	public List<FolioVentaRecuperacion> generarFolioVenta(Long recuperacionId);

	/**
	 * M�todo que guarda una Recuperacion de Proveedor.
	 * 
	 * Validar lo siguiente:
	 * 
	 * <font color="#408080"><i>-Que se haya capturado un N�mero de Orden de Compra
	 * con estatus de Pagado.</i></font>
	 * Que el id de la Orden de Compra no sea nulo: recuperacion.getOrdenCompra !=
	 * null.
	 * 
	 * <font color="#408080"><i>- Que se tenga capturada la Descripci�n del Concepto
	 * de Devoluci�n.</i></font>
	 * recuperacion.getConceptoDevolucion != null. && !recuperacion.
	 * getConceptoDevolucion.equals("")
	 * 
	 * <font color="#408080"><i>- Que el Monto del Subtotal sea mayor a cero.
	 * </i></font>
	 * recuperacion.getsubTotal > 0
	 * 
	 * <font color="#408080"><i>- Que el Monto Total Original no sea mayor al Monto
	 * Total de  la Orden de Compra.</i></font>
	 * Buscar la orden de compra pr OrdenCompra ordenCompra = entidadService.
	 * findById(OrdenCompra.class, recuperacion.getOrdenCompra().getId());
	 * 
	 * Invocar al m�todo ordenCompraService.obtenerTotales() enviando ordenCompra.
	 * getReporteCabina().getId(), ordenCompra.getCoberturaReporteCabina().getId(),
	 * ordenCompra.getIdTercero(); Esto nos devolver� el monto total de la Orden de
	 * Compra, asignarlo a una variable <b>montoTotalOC</b>
	 * <font color="#408080"><i>
	 * </i></font>recuperacion.getMontoTotal() <= montoTotalOC
	 * <font color="#408080"><i>
	 * </i></font>
	 * <font color="#408080"><i>- Que se haya generado una Referencia Bancaria.
	 * </i></font>
	 * <font color="#0f0f0f">Iterar el listado de referencias y validar que el campo
	 * referencia no este vac�o</font>
	 * 
	 * <font color="#408080"><i>- Que se haya capturado un correo electr�nico para el
	 * env�o de notificaci�n a Proveedor.</i></font>
	 * recuperacion.getCorreoProveedor!= null. && !recuperacion.getCorreoProveedor.
	 * equals("")
	 * 
	 * 
	 * Si alguna de las condiciones no se cumple, se deber� concatenar en una variable
	 * <b>mensaje </b>los errores ocurridos y retornar el objeto.
	 * 
	 * Si no existen errores proceder con lo siguiente:
	 * 
	 * 
	 * Invocar al m�todo recuperacionService.guardarRecuperacionProveedor enviando el
	 * objeto <b>recuperacion </b>y el retorno asignarlo al mismo objeto.
	 * 
	 * Invocar al m�todo recuperacionService.guardarCoberturasRecuperacion enviando el
	 * id de la recuperacion y el atributo <b>coberturasConcat</b>.
	 * 
	 * Invocar al m�todo recuperacionService.guardarPasesRecuperacion enviando el id
	 * de la recuperacion y el atributo <b>pasesConcat</b>.
	 * 
	 * Invocar al m�todo recuperacionService.guardarReferenciaBancaria enviando el id
	 * de la recuperacion y el atributo <b>coberturasConcat</b>.
	 * 
	 * Por �ltimo se deber+a generar el Ingreso asociado, invocar al m�todo
	 * generarIngreso
	 * 
	 * @param recuperacion
	 * @param coberturasConcat
	 * @param pasesConcat
	 * @param referencias
	 */
	public RecuperacionProveedor guardarRecuperacionProveedor(RecuperacionProveedor recuperacion, String coberturasConcat, String pasesConcat, List<ReferenciaBancaria> referencias)throws Exception;

	/**
	 * M�todo que guarda una Recuperacion de Proveedor.
	 * 
	 * Asignar la fecha de creaci�n y el c�digo de usuario de creaci�n.
	 * 
	 * Hacer un entidadService.save sobre la recuperacion y regresar el objeto
	 * 
	 * 
	 * Invocar a
	 * BitacoraService.registrar(TIPO_BITACORA.RECUPERACION, EVENTO.CREAR_RECUPERACION,
	 * recuperacionId, <font color="#0f0f0f">"Se ha generado una Recuperacion de
	 * Proveedor con id" + recuperacionId, recuperacion, usuarioService.
	 * getUsuarioActual<b>()</b>.getNombreUsuario<b>()</b>).</font>
	 * 
	 * @param recuperacion
	 */
	public RecuperacionProveedor guardarRecuperacionProveedor(RecuperacionProveedor recuperacion);

	/**
	 * <b>SE AGREGA EN DISE�O EDITAR RECUPERACION PROVEEDOR</b>
	 * 
	 * M�todo que guarda la informaci�n de una Venta para una Recuperaci�n de
	 * Proveedor.
	 * 
	 * Buscar la RecuperacionProveedor original mediante un entidadService.findById.
	 * 
	 * 
	 * 
	 * -Si el estatus de la Preventa es EN ESPERA DE VENTA:
	 * <b><i>(RDN Validaci�n para Registrar una Preventa de Refacci�n)</i></b>
	 * +Validar que la <b>recuperacion </b>se encuentre en estatus PENDIENTE POR
	 * RECUPERAR
	 * +Copiar la informaci�n proveniente de la venta a este objeto.
	 * +Hacer un entidadService.save sobre este objeto
	 * +Cambiar el <b>estatus </b>de la <b>recuperacion </b>a REGISTRADO
	 * +Cambiar el <b>medio </b>de la <b>recuperacion </b>a VENTA
	 * +Obtener el Ingreso <b>ingreso </b>asociado a la <b>recuperacion </b>por un
	 * entidadService.findByProperty y cambiar su <b>estatus </b>a CANCELADO
	 * +Enviar notificaci�n de la PREVENTA
	 * Si no cumple con la validaci�n enviar un mensaje de error.
	 * 
	 * -Si el estatus de la Preventa es VENTA
	 * <b><i>(RDN Validaci�n para Registrar una Venta de Refacci�n)</i></b>
	 * +Validar que el <b>estatus </b>de la <b>recuperacion </b>sea REGISTRADO
	 * +Validar que existe un <b>comprador </b>asociado
	 * +Validar que el <b>montoVenta </b>sea mayor a cero
	 * +Validar que existan Referencias Bancarias ligadas a la <b>recuperaci�n</b>.
	 * Buscar en ReferenciaBancaria existan registros de tipo VENTA, usar
	 * entidadService.findByProperties
	 * +Validar que exista el <b>correoComprador </b>capturado
	 * +Copiar la informaci�n proveniente de la venta a este objeto.
	 * +Hacer un entidadService.save sobre este objeto
	 * +Actualizar el <b>estatus </b>de la <b>recuperacion </b>a PENDIENTE
	 * +Generar el <b>ingreso </b>relacionado a la Recuperacion. Invocar al m�todo
	 * generarIngreso.
	 * +Relacionar las <b>referencias</b>. Invocar al m�todo
	 * guardarReferenciaBancaria
	 * 
	 * Si no cumple con la validaci�n enviar un mensaje de error.
	 * 
	 * 
	 * Invocar a
	 * BitacoraService.registrar(TIPO_BITACORA.RECUPERACION, EVENTO.
	 * EDITAR_RECUPERACION, recuperacionId, mensaje<font color="#0f0f0f"> +
	 * recuperacionId, recuperacion, usuarioService.getUsuarioActual<b>()</b>.
	 * getNombreUsuario<b>()</b>).</font>
	 * <font color="#0f0f0f">
	 * </font><font color="#0f0f0f">El mensaje depende de si se cambia a preventa o
	 * venta</font>
	 * 
	 * @param idRecuperacion
	 * @param recuperacion
	 * @param referencias
	 */
	public String guardarVentaRecuperacionProveedor(Long idRecuperacion, RecuperacionProveedor recuperacion, List<ReferenciaBancaria> referencias);

	/**
	 * <b>SE AGREGA EN DISE�O EDITAR RECUPERACION PROVEEDOR</b>
	 * 
	 * M�todo qu7e regresa los Folios de Venta para una Recuperaci�n de Proveedor.
	 * 
	 * Buscar mediante entidadService.findByProperty.
	 * 
	 * Si el listado es vacio, invocar al m�todo generarFolioVenta
	 * 
	 * @param recuperacionId
	 */
	public List<FolioVentaRecuperacion> obtenerFoliosVenta(Long recuperacionId);

	/**
	 * M�todo que regresa el objeto correspondiente a una RecuperacionProveedor
	 * 
	 * hacer un entidadService.findById
	 * 
	 * @param recuperacionId
	 */
	public RecuperacionProveedor obtenerRecuperacionProveedor(Long recuperacionId);
	
	public DatosRecuperacionDTO obtenerDatosRecuperacionProveedor(Long recuperacionId);
	
	public BigDecimal montoTotalRecuperacionProveedor(Long idSiniestroCabina, EstatusRecuperacion estatus ) ; 
	
	public BigDecimal montoTotalRecuperacionProveedorPagadas(Long estimacionId);
	
	public void cancelarEnvioNotificacion(Long recuperacionId);
	

}