/**
 * 
 */
package mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.permisos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.DetalleOrdenCompra;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.permisos.OrdenCompraAutorizacion;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.permisos.OrdenCompraPermiso;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.permisos.AutorizacionesUsuarioDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.permisos.OrdenesCompraAutorizarDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.permisos.UsuarioPermisoDTO;

/**
 * @author admin
 *
 */
@Local
public interface OrdenCompraAutorizacionService {
	
	public static final String TIPO_ORDEN_COMPRA_GASTO_AJUSTE 			= "GA";
	public static final String TIPO_ORDEN_COMPRA_AFECTACION_RESERVA		= "OC";
	
	public static final String ESTATUS_PENDIENTE			= "0";
	public static final String ESTATUS_AUTORIZADO 			= "1";
	public static final String ESTATUS_RECHAZADO			= "2";
	
	/**
	 * This method get all Buy Orders pending for to authorize. No send any parameter.
	 */
	
	public List<OrdenesCompraAutorizarDTO> obtenerListaOrdenesCompraPorAutorizarSP();
	
	
	/**
	 * metodo que crea un nuevo registro en la tabla OrdenCompraAutorizacion para el
	 * concepto/detalleOrdenCompra y usuario
	 * 
	* @param idOrdenCompra
	 */
	public String autorizarOrdenCompra(Long idOrdenCompra);
	
	/**
	 * Metodo que rechaza todos los detalles de una Orden COmpra y la Orden Compra
	 * @param idOrdenCompra
	 */
	public void rechazarOrdenCompra(Long idOrdenCompra) ;

	/**
	 * Valida cada concepto relacionado a la orden de compra contra los permisos del
	 * usuario que manda a utorizar, si tiene el permiso para los conceptos genera un
	 * registro en OrdenCompraAutorizacion para cada concepto con estatus autorizado
	 * de lo contrario el estatus es pendiente y la orden de compra queda en estatus
	 * de Pendiente hasta que todos los conceptos tengan por lo menos un registro en
	 * estatus Autorizado.
	 * 
	 * @param usuario
	 * @param ordenCompra
	 */
	public void autorizarOrdenCompra(String usuario, OrdenCompra ordenCompra);

	/**
	 * Elimina un permiso seleccionado
	 * 
	 * @param idPermiso
	 */
	public void eliminarPermiso(Long idPermiso) throws Exception;


	/**
	 * Valida si un concepto tiene por lo menos un registro autorizado en la tabla
	 * OrdenCompraAutorizacion
	 * 
	 * @param concepto
	 */
	public boolean estaAutorizadoConcepto(DetalleOrdenCompra concepto);

	/**
	 * guarda los datos generales del permiso
	 * 
	 * @param permiso
	 */
	public void guardarPermiso(OrdenCompraPermiso permiso);

	/**
	 * devuelve el nivel de autorización en que se encuantra un concepto.
	 * 
	 * @param concepto
	 */
	public Integer obtenerNivelAutorizacion(DetalleOrdenCompra concepto);


	/**
	 * devuelve una lista de usuarios para ser seleccionos en pantalla y asignar
	 * permisos.
	 */
	public List<UsuarioPermisoDTO> obtenerUsuariosDisponibles( Long ordenCompraPermisoId );
	
	
	/**
	 * Obtiene la lista de Autorizaciones que ya se dieron de alta 
	 * @return
	 */
	public List<AutorizacionesUsuarioDTO> obtenerListaAutorizaciones();
	
	
	/**
	 * Obtiene una OrdenCompraPersmiso por el id proporcionado
	 * @param idPermiso
	 * @return
	 */
	public OrdenCompraPermiso obtenerPermisoById(Long idPermiso);
	
	/**
	 * Guarda la informacion de un Permiso Guarado , debe de tene el Id el permiso que se le proporciona
	 * @param permiso
	 */
	public void editarPermiso(OrdenCompraPermiso permiso);
	
	
	/**
	 * Guarda la relacion del permiso con el usuario proporcionado
	 * @param usuario
	 * @param ordenCompraPermisoId
	 */
	public void guardarRelacionUsuario(UsuarioPermisoDTO usuario);
	
	
	/**
	 * Elimina la OrdenCOmpraPermiso proporcionado
	 * @param ordenCompraPermisoId
	 */
	public void eliminarRelacionUsuario( Long ordenCompraPermisoId);
	
	/**
	 * Obtiene la lista de Usuarios que estan relacionados al permiso
	 * @param ordenCompraPermisoId
	 * @return
	 */
	public List<UsuarioPermisoDTO> obtenerUsuariosAsociados( Long ordenCompraPermisoId );
	
	
	/**
	 * Obtiene la lista de Ordenes de Compra que todavia no estan autorizadas
	 * @return
	 */
	public List<OrdenesCompraAutorizarDTO> obtenerListaOrdenesCompraPorAutorizar();
	
	
	/**
	 * Obtiene el ultimo estatus de un detalle de compra 
	 * @param idOrdenCompra
	 * @param idDetalleCompra
	 * @return
	 */
	public String obtenerEstatusDetalleCompra(Long idOrdenCompra, Long idDetalleCompra);
	
	
	/**
	 * Eliminar todas las autorizaciones realizadas para el detalle
	 * @param idOrdenCompra
	 * @param idDetalleOrdenCompra
	 */
	public void eliminarAutorizacionesDetalle(Long idOrdenCompra, Long idDetalleOrdenCompra);
	
	
	/**
	 * Retornar las autorizaciones para una orden de compra
	 * @param ordenCompraId
	 * @return
	 */
	public List<OrdenCompraAutorizacion> obtenerAutorizacionesOrdenCompra(Long ordenCompraId);
	
	
	/**
	 * Retornar las autorizaciones para las ordenes de compra de una estimacion
	 * @param estimacionId
	 * @return
	 */
	public List<OrdenCompraAutorizacion> obtenerAutorizacionesEstimacion(Long estimacionId);
	
	
	/**
	 * Revisar si una orden de compra cuenta con autorizaciones
	 * @param ordenCompraId
	 * @return
	 */
	public Boolean tieneAutorizacionesOrdenCompra(Long ordenCompraId);
	
	/**
	 * Revisar si una estimacion tiene autorizaciones para cualquiera de sus ordenes de compra
	 * @param estimacionId
	 * @return
	 */
	public Boolean tieneAutorizacionesEstimacion(Long estimacionId);

}
