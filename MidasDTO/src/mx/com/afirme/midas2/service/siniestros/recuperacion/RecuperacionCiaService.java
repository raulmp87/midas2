package mx.com.afirme.midas2.service.siniestros.recuperacion;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.CartaCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.ComplementoCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.OrdenCompraCartaCia;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.EstatusRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.SeguimientoRecuperacion;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.SiniestroCabinaDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.RecuperacionCiaDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.RecuperacionRelacionSiniestroDTO;


/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 10-jul-2015 08:08:17 p.m.
 */
@Local
public interface RecuperacionCiaService extends RecuperacionService {

	
	public RecuperacionRelacionSiniestroDTO obtenerDatosRelacionSiniestro(Long idCoberturaRepCabina,String cveSubTipoCalculo,  Long idEstimacionCoberturaRepCabina);
	
	public RecuperacionRelacionSiniestroDTO obtenerDatosRelacionSiniestro(Long recuperacionId);
	
	public RecuperacionRelacionSiniestroDTO calculaImportes (String ordenesConcat, Integer porcentajeParticipacion, BigDecimal montoGAGrua, Long idEstimacion, Long idCobertura);
	
	public RecuperacionCompania inicializaRecuperacionCompania(RecuperacionCompania recuperacionCompania);
	
	public EstimacionCoberturaReporteCabina obtieneEstimacion (Long idCoberturaRepCabina,Long idEstimacionCoberturaRepCabina );

	/**
	 * <b>SE DEFINE EN DISE�O DE CANCELAR RECUPERACION COMPA�IA</b>
	 * 
	 * M�todo que realiza la Cancelaci�n de una Recuperaci�n de Compa��a
	 * 
	 * Actualizar la <b>recuperacion </b>con <b>estatus </b>CANCELADO y su �ltima
	 * Carta con <b>estatus </b>CANCELADO.
	 * 
	 * 
	 * Invocar al m�todo <b><i>super.cancelarRecuperacion</i></b> e
	 * i<b><i>ngresoService. cancelarIngresoPendientePorAplicar</i></b>
	 * 
	 * @param recuperacionId
	 */
	public void cancelarRecuperacion(Long recuperacionId);

	/**
	 * <b>SE DEFINE EN DISE�O EDITAR RECUPERACION A COMPA�IAS</b>
	 * 
	 * M�todo que actualiza el estatus de la carta a ELABORADA.
	 * 
	 * Obtener la <b>recuperacion </b>por medio de entidadService.findById sobre
	 * RecuperacionCompania y obtener la <b>cartaCompania </b>�por medio de
	 * entidadService.findByProperties con el recuperacionId y estatus PEND_ELAB
	 * 
	 * Validar
	 * <ul>
	 * 	<li><font color="#c0c0c0"><i>Que el monto de la indemnizaci�n sea mayor a
	 * Cero</i></font> : Que se tengas Ordenes de Compra asociadas a la
	 * <b>Recuperacion</b>.</li>
	 * </ul>
	 * 
	 * <ul>
	 * 	<li><font color="#c0c0c0"><i>Que en caso que sea P�rdida Total el Monto de
	 * Salvamento sea mayor a Cero: </i></font></li>
	 * </ul>
	 *        Verificar el pase de atenci�n o cobertura asociada a la <b>recuperacion
	 * </b>e invocar al m�todo <b><i>estimacionCoberturaService.obtenerDatosEstimacion;
	 * </i></b> si el atributo tipoPerdida es Perdida total verificar que el
	 * <b>montoSalvamento </b>de la <b>recuperacion </b>sea mayo a cero
	 * 
	 * <ul>
	 * 	<li><font color="#c0c0c0"><i>En caso de que una Orden de Compra se encuentre
	 * asociada a una Recuperaci�n de Proveedor, �ste debe de estar en estatus
	 * Recuperado:</i></font></li>
	 * </ul>
	 * 
	 * 
	 * <ul>
	 * 	<li><font color="#c0c0c0"><i>Que las �rdenes de Compra no se encuentren
	 * asociados a otra Recuperaci�n de Compa��a:</i></font></li>
	 * </ul>
	 *        Iterar las Ordenes de Compra y buscar que no se encuentren asociadas a
	 * otra entidad de Recuperaciond e Compa�ias
	 * 
	 * 
	 * 
	 * 
	 * Actualizar el <b>estatus </b>a ELABORADA, actualizar la <b>fechaElaboracion
	 * </b>con la fecha actual y el <b>usuarioElaboracion </b>con el usuario actual,
	 * cambiar a null la <b>causaNoElaboracion</b>.
	 * 
	 * @param recuperacionCiaId
	 */
	public String elaborarCarta(Long recuperacionCiaId);

	/**
	 * <b>SE DEFINE EN DISE�O EDITAR RECUPERACION A COMPA�IAS</b>
	 * 
	 * M�todo que actualiza el estatus de la carta a ENTREGADA.
	 * 
	 * Obtener la <b>recuperacion </b>por medio de entidadService.findById sobre
	 * RecuperacionCompania y obtener la <b>cartaCompania </b>�por medio de
	 * entidadService.findByProperties con el recuperacionId y estatus
	 * <b>ELABORADA</b>
	 * 
	 * 
	 * Actualizar el <b>estatus </b>a ENTREGADA, actualizar la <b>fechaEntrega </b>con
	 * la fecha actual y el <b>usuarioEntrega </b>con el usuario actual,  asignar la
	 * <b>fechaAcuse</b>
	 * 
	 * @param recuperacionCiaId
	 * @param fechaAcuse
	 */
	public String entregaCarta(Long recuperacionCiaId, Date fechaAcuse);

	/**
	 * <b>SE AGREGA EN DISE�O REGISTRAR REDOCUMENTACION</b>
	 * 
	 * M�todo que guara el Rechazo o Exclusion de la Carta Actual
	 * 
	 * Invocar al m�todo obtenerCartaActiva y asignar a la variable cartaActual.
	 * 
	 * Cambiar el estatus de la cartaActual a <b>RECHAZADA</b>.
	 * 
	 * Guardar la nueva cartaCompania  con estatus <b>REGISTRADA </b>con el siguiente
	 * <b>folio </b>consecutivo.
	 * 
	 * Actualizar el monto de la recuperacion en caso de ser Exclusion.
	 * 
	 * @param cartaCia
	 * @param recuperacionId
	 */
	public void generarRedocumentacion(CartaCompania cartaCia, Long recuperacionId, String validaExclusionRechazo);

	/**
	 * Asociar y guardar una Carta Compania a una Recuperacion.
	 * 
	 * Obtener el ultimo folio existente de las Cartas Compania asociadas, de no
	 * existir ninguna carta se generara con el folio 0.
	 * 
	 * @param cartaCia
	 * @param recuperacionId
	 */
	public CartaCompania guardarCartaCia(CartaCompania cartaCia, Long recuperacionId);

	/**
	 * M�todo que genera/actualiza una Recuperaci�n de Compa��a Manual asociando el
	 * pase de atenci�n/cobertura y las Ordenes de Compra seleccionadas .
	 * 
	 * i
	 * Invocar al m�todo <b><i>obtenerMontoSalvamento </i></b>y asignaro al
	 * montoSalvamento el objeto recuperacion.
	 * 
	 * Invocar al m�todo <b><i>obtenerOrdenesCompraCia </i></b>enviando el String de
	 * ordenes y soloSeleccionados = true.
	 * 
	 * Sumar los importes de montoARecuperar de las ordenes y asignarlo al atributo
	 * montoIndemnizacion de la recuperacion
	 * 
	 * Validar que los montos  sean correctos en las Ordenes de Compra y Salvamento.
	 * 
	 * Invocar al m�todo <b><i>validarRecuperacionCia</i></b>.
	 * Si no hay errores guardar la recuperacion como medio de Recuperacion REEMBOLSO
	 * y origen MANUAL . Calcular la fecha de Cobro en base a la RDN, sumar 2 a�os
	 * (730 d�as) a la Fecha de Ocurrido del Siniestro
	 * 
	 * 
	 * Invocar al metodo <b><i>guardarCartaCia</i></b>
	 * 
	 * Asociar las Ordenes de Compra obtenidas a la Carta Compania.
	 * 
	 * Invocar al m�todo <b><i>guardarCoberturasRecuperacion </i></b>y
	 * <b><i>guardarPasesRecuperacion  </i></b>(heredados de RecuperacionService)
	 * 
	 * @param recuperacion
	 * @param carta
	 * @param cobertura
	 * @param pase
	 * @param ordenes
	 */
	//public String guardarRecuperacionCiaManual(RecuperacionCompania recuperacion,CartaCompania carta, String cobertura, String pase, String ordenes);
	//public Map<String,Object> guardarRecuperacionCiaManual(RecuperacionCompania recuperacion,CartaCompania carta, String cobertura, String pase, String ordenes);

	/**
	 * <b>SE DEFINE EN DISE�O DE REGISTRAR SEGUIMIENTO DE RECUPERACI�N DE
	 * COMPA��A</b>
	 * M�todo que genera y asocia un seguimiento a una Recuperaci�n de Compa��a
	 * 
	 * Obtener la <b>recupera
	 * 
	 * Crear el objeto de SeguimientoRecuperacion con la <b>fechaCreacion </b>= fecha
	 * actual, <b>usuarioCreacion </b>= usuario actual y el <b>comentario </b>de
	 * par�metro.
	 * 
	 * Asociar el seguimiento a la <b>recuperacion</b>
	 * 
	 * @param recuperacionId
	 * @param comentario
	 */
	public void guardarSeguimiento(Long recuperacionId, String comentario);

	/**
	 * 
	 * <b>SE AGREGA EN DISE�O REGISTRAR REDOCUMENTACION</b>
	 * M�todo que retorna la carta activa.
	 * 
	 * Hacer un entidadService.findByPropertyWithOrder por el recuperacion.id =
	 * recuperacionId y ordenar por folio, retornar la carta con el folio m�s actual
	 * 
	 * @param recuperacionId
	 */
	public CartaCompania obtenerCartaActiva(Long recuperacionId);

	/**
	 * 
	 * @param recuperacionCiaId
	 */
	public CartaCompania obtenerCartaCompania(Long recuperacionCiaId);

	/**
	 * <b>SE AGREGA EN DISE�O REGISTRAR REDOCUMENTACION</b>
	 * 
	 * Invocar al m�todo<b><i> recuperacionCiaDao.obtenerCartasRedoc</i></b>
	 * 
	 * @param recuperacionId
	 */
	public List<CartaCompania> obtenerCartasRedoc(Long recuperacionId);


	/**
	 * Obtener el importe del Salvamento para la Estimaci�n de la Recuperacion de
	 * Compa��as.
	 * 
	 * Hacer un entidadService.findByProperties sobre RecuperacionSalvamento con el
	 * <b>estimacion.id = estimacionId</b> y que el estatus = RECUPERADO. Al obtenerlo
	 * invocar al m�todo del objeto <b><i>recuperacion.obtenerVentaActiva</i></b> y
	 * obtener el atributo <b>subtotal </b>de la <b>venta</b>
	 * 
	 * @param estimacionId
	 */
	public BigDecimal obtenerMontoSalvamento(Long estimacionId);

	/**
	 * Invocar al m�todo <b><i>recuperacionCiaDao.obtenerOCestimacion</i></b>
	 * 
	 * @param estimacionId
	 * @param ordenesConcat
	 */
	public List<OrdenCompraCartaCia> obtenerOCestimacion(Long recuperacionId, Long reporteCabinaId, Long coberturaId, Long estimacionId, String ordenesConcat,
			Integer porcentajeParticipacion, Boolean soloSeleccionados);
	/**
	 * <b>SE AGREGA EN DISE�O EDITAR RECUPERACION COMPA�IA</b>
	 * 
	 * Hacer un entidadService.findByProperties sobre <b><i>OrdenCompraCia
	 * </i></b>usando recuperacion.id = <b>recuperacionCiaId </b>y retornar el listado
	 * 
	 * @param recuperacionCiaId
	 */
	public List<OrdenCompraCartaCia> obtenerOrdenesCompraCia(Long recuperacionCiaId, boolean soloAfectacionReserva);

	/**
	 * Hacer un entidadService.findById sobre RecuperacionCompania y retornar
	 * 
	 * @param recuperacionCiaId
	 */
	public RecuperacionCompania obtenerRecuperacionCia(Long recuperacionCiaId);

	/**
	 * <b>SE DEFINE EN DISE�O DE MOSTRAR RECUPERACIONES CIA POR VENCER</b>
	 * 
	 * Invocar al m�todo <b>recuperacionCiaDao.obtenerRecuperacionesVencer</b>
	 */
	public List<RecuperacionCompania> obtenerRecuperacionesVencer();

	/**
	 * <b>SE DEFINE EN DISE�O DE REGISTRAR SEGUIMIENTO DE RECUPERACI�N DE
	 * COMPA��A</b>
	 * 
	 * M�todo que obtiene el listado de Seguimientos de una Recuperaci�n de Compa��as.
	 * 
	 * 
	 * Hacer un entidadService.findByProperty sobre SeguimientoRecuperacion con el
	 * recuperacion.id = <b>recuperacionId </b>y retornar el resultado.
	 * 
	 * @param recuperacionId
	 */
	public List<SeguimientoRecuperacion> obtenerSeguimientos(Long recuperacionId);

	/**
	 * <b>SE DEFINE EN DISE�O DE CANCELAR RECUPERACION COMPA�IA</b>
	 * 
	 * M�todo que envia una Solicitud de Cancelaci�n al rol definido en RDN.
	 * 
	 * Actualizar la recuperacion con la <b>fechaSolCancelacion</b>, el
	 * <b>motivoCancelacion </b>y el <b>usuarioSolCancelacion. </b>Actualizar el
	 * <b>estatus </b>de la �ltima Carta a<b> PEND_CANC</b>
	 * <b>
	 * </b>Enviar un correo por medio de envioNotificacionService
	 * 
	 * @param recuperacionId
	 */
	public void solicitarCancelacion(Long recuperacionId, String motivoCancelacion);
	
	public void generarEstadoCuenta();

	/**
	 * Validar que la generaci�n de la Recuperaci�n de Compania cumpla con las RDN
	 * establecidas.
	 * 
	 * Validar quese cuenta con un Reporte de Cabina asociado, de caso contrario
	 * mandar un mensaje de error.
	 * 
	 * Validar que se tenga un Pase de Atenci�n o una Cobertura (que no genere Pase de
	 * Atenci�n) seleccionada.
	 * 
	 * Que se cuente con una Compa�ia seleccionada, un num de poliza y num de
	 * Sineistro.
	 * 
	 * Que el porcentaje se encuentre entre 1 y 100%
	 * 
	 * Que el termino de ajuste del Siniestro sea SE RECIBIO ORDEN DE CIA.
	 * Para esto buscar sobre AutoIncisoReporteCabina asociado al Reporte y que cuente
	 * con el terminoAjuste = EOC
	 * 
	 * @param recuperacionCia
	 */
	
	public String validarRecuperacionCia(RecuperacionCompania recuperacionCia,Long idPaseAtencion) ;
	
	/**
	 * Método que asigna una Referencia Bancaria a una o más Recuperaciones de Compañía
	 * 
	 * Validar que la referencia cumple con el formato especificado en la RDN, de lo contrario regresar un mensaje de error.
	 * 
	 * Hacer un split sobre recuperacionesConcat el cual contendrá los ids concatenados por comas de las recuperaciones de compañía.
	 * 
	 * Iterar el array y por cada id buscar la RecuperacionCompania correspondiente mediante un entidadService.findById y asignar a una 
	 * variable recuperacion. Verificar que cada una se encuentre en estatus REGISTRADO, que no contenga una referenciaBancaria y que la 
	 * ultima CartaCompania se encuentre en estatus ENTREGADO de lo contrario ir concatenando el numero de las recuperaciones que no cumplen y
	 * al ultimo devolver el mensaje de error.
	 * 
	 * Si cumplen las validaciones por cada una hay que actualizar su atributo referenciaBancaria con la referencia que entro de parámetro, 
	 * cambiar fechaModificacion y usuarioModificacion  y cambiar el estatus a PENDIENTE POR RECUPERAR. Invocar al método generarIngreso.
	 * 
	 * Enviar la notificación corrrespondiente de acuerdo a la RDN.
	 */
	public String asignarReferenciaBancaria(String recuperacionesConcat, String referencia);
	
	public String eliminarReferenciaBancaria(String recuperacionesConcat);
	
	public List<RecuperacionCiaDTO> buscarRecuperacionesCia(RecuperacionCiaDTO recuperacionCiaFiltro);

	
	public void redocumentarCartas();
	
	public void identificarCartasAElaborar();
	
	/**
	 * <b>SE DEFINE EN DISEÑO REDOCUMENTAR CARTAS COMPAÑÍA</b>
	 * 
	 * Método que obtiene el listado de Cartas de Compañía que necesitan ser
	 * Redocumentadas que cuenten con más de 90 días sin Pago desde la última Fecha de
	 * Acuse
	 * 
	 * Hacer un JPQL sobre CartaCompania todas aquellas que:
	 * 
	 * <ul>
	 * 	<li>El <b>Estatus de la ultima CartaComapnia</b> sea <b>Entregado</b> y </li>
	 * 	<li>el <b>Estatus de la RecuperaciónCompania</b> sea <b>Registrado </b>y</li>
	 * 	<li>la resta entre la <b>Fecha Actual</b> menos la <b>Fecha de Acuse</b>  de
	 * la ultuima CartaCompania sea mayor o igual a 90 días.</li>
	 * </ul>
	 */
	public List<CartaCompania> obtenerCartasARedoc();
	
	public BigDecimal montoTotalRecuperacionCia(Long idSiniestroCabina, EstatusRecuperacion estatus ) ; 	
	
	public String entregarCartas(String recuperacionesConcat, Date fechaAcuse);
	
	public String reasignarCartas(String recuperacionesConcat, Long oficinaReasignadaId);
	
	public String registrarEnvioCarta(String recuperacionesConcat, Long oficinaId);

	public String eliminarEnvioCarta(String recuperacionesConcat);
	
	public String registrarRecepcionCarta(String recuperacionesConcat, String personaRecepcion);
	
	public String eliminarRecepcionCarta(String recuperacionesConcat);

	public List<String> obtenerReferenciasIdentificadas() ;

	
	/**
	 * <b>SE AGREGA EN DISEÑO REGISTRAR COMPLEMENTO</b>
	 * 
	 * Genera una recuperación de tipo Complemento para la recuperacion Actual.
	 * 
	 * Obtener el <b>complemento </b>Actual mediante el método
	 * <b><i>obtenerTieneComplemento</i></b>.
	 * 
	 * Copiar los datos de la <b>recuperacionActual </b>a un nuevo objeto
	 * <b>recuperacion </b>y poner como <b>recuperacionOriginal </b>la
	 * <b>recuperacionActual</b>, pero modificar los siguientes datos.
	 * <b>fechaCreacion</b>
	 * <b>codigoUsuarioCreacion</b>
	 * <b>origen </b>= Manual
	 * <b>estatus </b>= Registrado
	 * <b>importe = complemento.monto</b>
	 * <b>esComplemento </b>= true.
	 * 
	 * Copiar y asociar la <b>cobertura y</b> <b>pase de atencion </b> ligados a la
	 * recuperacionActual.
	 * 
	 * Generar y asociar una <b>cartaCompania </b>a la <b>recuperacion </b>con
	 * <b>estatus </b>REGISTRADO. (Omitir fechas y usuarios de otros estatus que no
	 * sean registro)
	 * 
	 * Actualizar el <b>complemento </b>a <b>activo </b>= false
	 * 
	 * @param recuperacionId
	 */
	public RecuperacionCompania complementarCarta(Long recuperacionId);
	

	/**
	 * <b>SE AGREGA EN DISEÑO REGISTRAR COMPLEMENTO</b>
	 * 
	 * Retorna si se ha realizado un ajuste de aumento al pase/afectación de la
	 * Recuperación.
	 * 
	 * Hacer un entidadService.findByProperties sobre ComplementoCompania y como
	 * parámetro la recuperacion.id = recuperacionId y activo = true.
	 * 
	 * @param recuperacionId
	 */
	public ComplementoCompania obtenerTieneComplemento(Long recuperacionId);
	
	
	/**
	 * <b>SE AGREGA EN DISEÑO REGISTRAR COMPLEMENTO</b>
	 * 
	 * Método que detecta que no se realizará un complemento sobre una Recuperación
	 * existente.
	 * 
	 * Se debe cancelar la provisión PENDIENTE DEFINIR
	 * 
	 * @param recuperacionId
	 */
	public void cancelarComplemento(Long recuperacionId);
	

	/**
	 * <b>SE AGREGA EN DISEÑO REGISTRAR COMPLEMENTO</b>
	 * 
	 * Método que genera una nueva recuperación con el ajuste de aumento del
	 * pase/afectación de la recuperación actual cancelando esta última
	 * 
	 * Obtener el <b>complemento </b>Actual mediante el método
	 * <b><i>obtenerTieneComplemento</i></b>.
	 * 
	 * Obtener la <b>recuperacionActual </b>por medio de entidadService.findById.
	 * 
	 * Copiar los datos de la <b>recuperacionActual </b>a un nuevo objeto
	 * <b>recuperacion</b>, pero modificar los siguientes datos.
	 * <b>fechaCreacion</b>
	 * <b>codigoUsuarioCreacion</b>
	 * <b>origen </b>= Automatico
	 * <b>estatus </b>= Registrado
	 * 
	 * Copiar y asociar la <b>cobertura </b>, <b>pase de atencion y Ordenes de
	 * Compra</b> ligados a la recuperacionActual.
	 * 
	 * Generar y asociar una <b>cartaCompania </b>a la <b>recuperacion </b>con
	 * <b>estatus </b>REGISTRADO.
	 * Invocar al método <b><i>cancelarRecuperacion </i></b>con el motivo de Cancelado
	 * por Complemento (PENDIENTE ENUM).
	 * 
	 * Actualizar el <b>complemento </b>a activo = false
	 * 
	 * @param recuperacionId
	 */
	public RecuperacionCompania regeneraRecuperacion(Long recuperacionId);
	
	public RecuperacionCompania generaRecuperacionCiaAutomatico(EstimacionCoberturaReporteCabina estimacion, BigDecimal importe);
	
	public RecuperacionCompania generaComplementoAutomatico(RecuperacionCompania recuperacionOriginal, BigDecimal montoAjuste);
	
	public TransporteImpresionDTO imprimirCarta(Long cartaCiaId);
	
	public void notificaAjusteRecuperacion(RecuperacionCompania recuperacion, BigDecimal montoAjuste);
	
	public Map<String,Boolean> actualizarInfoCompanias(AutoIncisoReporteCabina autoInciso, Integer recibioOrdenCiaOrigen, String ciaSegurosOrigen, 
										String polizaCiaOrigen, Integer porcentajeOrigen);
	
	public RecuperacionCompania crearYSalvarRecuperacionCia(EstimacionCoberturaReporteCabina estimacion, BigDecimal importe);
	
	public RecuperacionCompania cancelarYGuardarRecuperacionCia(Long recuperacionId);
	
	public RecuperacionCompania creaYGuardaComplemento(RecuperacionCompania recuperacionOriginal, BigDecimal montoAjuste);
	
	public RecuperacionCompania regeneraYGuarda(Long recuperacionId);
	
	public RecuperacionCompania complementarCartaYGuarda(Long recuperacionId);
	
	public RecuperacionCompania creaYGuardaCiaManual(RecuperacionCompania recuperacion,
			CartaCompania carta, String cobertura, String pase, String ordenes);
	
	public RecuperacionCompania guardarRecuperacionCia(RecuperacionCompania recuperacion,
			CartaCompania carta, String cobertura, String pase, String ordenes, BigDecimal montoGruasProvisionado);
	
	public RecuperacionCompania  actualizaPorCambioDePorcentaje(RecuperacionCompania recuperacion,Long estimacionId, Integer companiaId, String polizaCia, String numeroSiniestro, Integer porcentaje);
	
	public RecuperacionCompania elaborarCartaYGuarda(Long recuperacionCiaId);
	
	// INTERFACES PARA PROVISIONAR
	
	public void cancelarRecuperacionesCiaAlDesabilitarCiaTercero(EstimacionCoberturaReporteCabina estimacion);
	public void actualizarInfoCompaniasYProvisiona(SiniestroCabinaDTO siniestroCabina, Integer porcentajeOrigen,Integer recibioOrdenCiaOrigen );
	public void generarRedocumentacionProvision(CartaCompania cartaCia, Long recuperacionId, String validaExclusionRechazo);
	public String guardarRecuperacionCiaManualProvision(RecuperacionCompania recuperacion,CartaCompania carta, String cobertura, String pase, String ordenes);
	public Map<String,BigDecimal> obtenerMontoPiezasSalvamento(Long recuperacionId);
	public  Map<String,Object> validarRecuperacionCiaManual(RecuperacionCompania recuperacion,CartaCompania carta, String cobertura, String pase, String ordenes);
	
	public RecuperacionCompania guardarProvisionarCiaMontos(Long recuperacionId,BigDecimal montoGrua, String ordenes,String pase,String cobertura);
	public RecuperacionCompania guardarProvisionarCiaMontosProcessor(RecuperacionCompania recuperacion);
	public void salvarRecuperacion(RecuperacionCompania recuperacion);
	public String guardarCiaYProvisiona(RecuperacionCompania recuperacion,CartaCompania carta, String cobertura, String pase, String ordenes);
	public String guardarRecuperacionCiaManual(RecuperacionCompania recuperacion,CartaCompania carta, String cobertura, String pase, String ordenes);
}