package mx.com.afirme.midas2.service.siniestros.pagos.facturas;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.persistence.PersistenceException;

import org.apache.xmlbeans.XmlException;

import mx.com.afirme.midas.siniestro.finanzas.pagos.FacturaDTO;
import mx.com.afirme.midas2.domain.envioxml.EnvioFactura;
import mx.com.afirme.midas2.domain.envioxml.EnvioFacturaDet;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.EnvioValidacionFactura;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.MensajeValidacionFactura;
import mx.com.afirme.midas2.dto.siniestros.pagos.facturas.OrdenCompraProveedorDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.facturas.RelacionFacturaOrdenCompraDTO;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos.Retenciones.Retencion;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos.Traslados.Traslado;
@Local
public interface RecepcionFacturaService {



	/**
	 * recibe el idBatch para devolver solamente las facturas procesadas por el
	 * usuario en ese momento.
	 * 
	 * @param idBatch
	 */
	public List<EnvioValidacionFactura> obtenerFacturasProcesadas(Long idBatch);

	/**
	 * Utiliza EntidadService para consultar la entidad MensajeValidacionFacturay
	 * consulta todos los resultados de validación para una factura el último envío.
	 * 
	 * @param idFactura    id de la factura a consultar
	 */
	public List<MensajeValidacionFactura> obtenerResultadosValidacion(Long idEnvioValidacion);



	/**
	 * este método recibe un archivo InputStream que debe ser convertido a un
	 * ZipInputStream para poder procesar el contenido. Se debe invocar el método
	 * procesarFactura por cada archivo xml contenido en el zip, el objeto de que
	 * devuelve se debe almacenar en un List para retornar un solo listado con todas
	 * las facturas procesadas.
	 * 
	 * @param zipFile    Este parámetro tiene el archivo zip cargado por el usuario
	 * que debe contener N archivos xml
	 */
	public List<EnvioValidacionFactura> procesarFacturasZip(Integer proveedorId, File file, String extensionArchivo);

	/**
	 * Realiza las validaciones de montos entre los montos de las órdenes de compra y
	 * los montos de la factura, tambien debe validarse que el número de factura no
	 * esté registrado previamente con el mismo proveedor. Si las validaciones de
	 * negocio pasan se invoca el método envioValidacionFactura, se procesa la
	 * respuesta y se invoca el guardado de los mensajes tanto de negocio (NEG) como
	 * del sat (SAT)
	 * 
	 * Devuelve un listado de errores como resultado de las validaciones que fallaron.
	 * 
	 * @param valores    Mapa que contiene los valores a validar, la interface define
	 * los posibles párametros permitidos
	 * @param facturaSiniestro    Esta es la factura que será validada
	 */
//	public List<> validarFacturaSiniestro(Map valores, FacturaSiniestro facturaSiniestro);

	/**
	 * Valida que la factura que los montos de la factura coincidan con los montos de
	 * la órden de compra. Este método recibe un listado de ordenes de compra ya que
	 * esposible asociar más de una órden de compra a una factura, en este caso el
	 * monto a validar es la sumatoria de los montos de cada órden de compra.
	 * 
	 * En caso que la factura validada no pase la validación todas las órdenes de
	 * compra deberán seguir disponibles para asociarse con otra factura.
	 * 
	 * Las facturas que pasen la validación se cambiarán a estatus de "REGISTRADA" y
	 * se debe actualizar la Orden de compra con el numero de factura relacionada.
	 * Invoca el metodo save de OrdenCompraService
	 * 
	 * @param ordenesCompra    Listado de órdenes de compra con las que serán
	 * comparadas las facturas.
	 * @param factura    entidad factura que se llena con los datos de una factura xml
	 */
	
	
	public List<EnvioValidacionFactura> validarFacturasSiniestro(List<DocumentoFiscal> facturas, Long batchId,Integer idProveedor);
	
	public List<EnvioValidacionFactura> validarFacturasSiniestroAction(List<RelacionFacturaOrdenCompraDTO> listaFacturaOrdenCompra, Long batchId,Integer idProveedor);
	
	public Map<Long,String> obtenerFacturasPorBatchId(Long batchId);
	
	public List<DocumentoFiscal> obtenerFacturasRegistradaByOrdenCompra(Long idOrdenCompra, String estatusFactura);
	
	public EnvioFactura enviaAAxosNET(EnvioFactura envioFactura) throws FileNotFoundException, PersistenceException, Exception;
	
	public List<MensajeValidacionFactura> convierteAMensajesValidacionFactura (List<EnvioFacturaDet> axosNetRespuestas , EnvioValidacionFactura envioValidacionFactura);
	
	public EnvioValidacionFactura creaEnvioValidacion(DocumentoFiscal factura , Long batchId );
	
	public MensajeValidacionFactura  creaMensajeValidacionFactura (EnvioValidacionFactura envioValidacionFactura, Long tipoMensaje, String texto );
	
	public Boolean esValidoRFCPorProveedor (String rfc, Integer proveedorId);
	
	public Comprobante parsearCFD (ByteArrayOutputStream bos) throws XmlException, IOException;
	
	public DocumentoFiscal procesaXML(ByteArrayOutputStream bos,Integer idProveedor,DocumentoFiscal.TipoDocumentoFiscal tipo);
	
	public List<EnvioValidacionFactura> creaEnvioValidacion(List<DocumentoFiscal> facturas);
	
	public void validaProcesamientoDeArchivos(List<EnvioValidacionFactura> envioValidacionList, Integer idProveedor);
	
	public void validaProcesamientoFactura(List<EnvioValidacionFactura> envioValidacionList, Integer idProveedor);
	
	public List<EnvioValidacionFactura> agregaEstatusDescripcion(List<EnvioValidacionFactura> envioValidacionList);
	
	public boolean isValidoXmlSolo();
	
	public DocumentoFiscal guardaAgrupador(Integer idPrestador, String ordenesCompraConcat);
	
	public DocumentoFiscal eliminaAgrupador(Long idAgrupadro);
	
	public List<DocumentoFiscal> buscarAgrupadores(OrdenCompraProveedorDTO filtroAgrupador);
	
	public Boolean validaNombreDelAgrupador(String nombreAgrupador);
	
	public EnvioValidacionFactura validaAsociarLaFacturaLiquidacion(DocumentoFiscal factura,EnvioValidacionFactura envioValidacionFactura);
	
	public String obtenerTipoDeDocumentoFiscal(Comprobante comprobante);
	
	public BigDecimal obtenerImpuestoTraslados(Impuestos impuestos, Traslado.Impuesto.Enum impuesto);
	
	public BigDecimal obtenerImpuestoRetenciones(Impuestos impuestos, Retencion.Impuesto.Enum impuesto);
	
	public List<FacturaDTO> obtenerFacturasPendientesComplementoProveedor(Integer idProveedor,String origenEnvio) throws Exception;	

	public List<FacturaDTO> obtenerFacturasPendientesComplementoAgente(Integer idAgente) throws Exception;	
	
}
