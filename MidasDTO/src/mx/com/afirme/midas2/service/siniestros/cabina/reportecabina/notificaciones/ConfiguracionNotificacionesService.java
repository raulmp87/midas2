package mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.notificaciones;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.AdjuntoConfigNotificacionCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.ConfiguracionNotificacionCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.MovimientoProcesoCabina;
import mx.com.afirme.midas2.dto.siniestros.cabina.notificaciones.AdjuntoConfigNotificacionCabinaDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.notificaciones.ConfiguracionNotificacionDTO;

@Local
public interface ConfiguracionNotificacionesService {
	
	public void agregarAdjunto(ConfiguracionNotificacionCabina configuracion, AdjuntoConfigNotificacionCabina adjunto);
	
	public void borrarAdjunto(ConfiguracionNotificacionCabina configuracion, AdjuntoConfigNotificacionCabina adjunto);
	
	public void eliminarConfiguracion(ConfiguracionNotificacionCabina configuracion);
	
	public void guardarActualizarConfiguracion(ConfiguracionNotificacionCabina configuracion);
	
	public void actualizarEstatusConfiguracion(ConfiguracionNotificacionCabina configuracion);
	
	public ConfiguracionNotificacionCabina obtenerConfiguracion(Long configId);
	
	public List<ConfiguracionNotificacionCabina> obtenerConfiguraciones();
	
	public List<ConfiguracionNotificacionCabina> obtenerConfiguracionesPorFiltros(ConfiguracionNotificacionDTO configuracionDTO);
	
	public Map<String, String> obtenerMovimientos(Long procesoId);
	
	public MovimientoProcesoCabina obtenerMovimiento(String codigo);
	
	public Boolean movimientoSinOficina(String codigo);
	
	public Map<Long, String> obtenerProcesos();
	
	/**
	 * @param configuracion ConfiguracionNotificacionCabina al que pertenecen los adjuntos a buscar
	 * @return Litado de AdjuntoConfigNotificacionCabina encontrados para la configuracion recibida com parametro. 
	 */
	public List<AdjuntoConfigNotificacionCabina> obtenerAdjuntos(ConfiguracionNotificacionCabina configuracion);
	
	/**
	 * @param id identificador de la entidad AdjuntoConfigNotificacionCabina
	 * @return AdjuntoConfigNotificacionCabina como resultado de la busqueda
	 * Realiza una busqueda de un AdjuntoConfigNotificacionCabina por id
	 */
	public AdjuntoConfigNotificacionCabina obtenerAdjunto (Long id);
	
	public List<AdjuntoConfigNotificacionCabinaDTO> obtenerAdjuntosSimpleList(ConfiguracionNotificacionCabina config);
}
