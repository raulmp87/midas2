package mx.com.afirme.midas2.service.siniestros.configuracion.horario.ajustador;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.ajustador.HorarioAjustador;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.ajustador.HorarioAjustador.TipoDisponibilidad;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.ajustador.HorarioAjustadorId;
import mx.com.afirme.midas2.dto.siniestros.configuracion.horario.ajustador.ConfiguracionDeDisponibilidadDTO;
import mx.com.afirme.midas2.dto.siniestros.configuracion.horario.ajustador.ListadoDeDisponibilidadDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService;

@Local
public interface HorarioAjustadorService extends CatalogoSiniestroService {

	/**
	 * Funcion para obtener la Entidad HorarioAjustador por medio de su Id
	 * 
	 * @param id
	 * @return
	 */
	public HorarioAjustador obtener(HorarioAjustadorId id);

	/**
	 * Funcion para obtener el listado usado en el Grid apartir de la
	 * informacion en el filtro
	 * 
	 * @param horarioAjustadorFltro
	 * @return
	 */
	public List<ListadoDeDisponibilidadDTO> listar(
			HorarioAjustadorFiltro horarioAjustadorFiltro);

	/**
	 * Funcion podria usar el renglon (DTO) seleccionado en el Grid de
	 * filtrado/busqueda y sacar el listado a mostrar en el Grid de edicion
	 * 
	 * @param horarioAjustadorDTO
	 * @return
	 */
	public List<ListadoDeDisponibilidadDTO> listar(
			ListadoDeDisponibilidadDTO horarioAjustadorDTO);

	/**
	 * Funcion para guardar la entidad HorarioAjustador
	 * 
	 * @param horarioAjustador
	 * @return
	 */
	public HorarioAjustador guardar(HorarioAjustador horarioAjustador);

	/**
	 * Save settings for HorarioAjustador into MIDAS.TOHORARIOAJUSTADOR on just one connection request. 
	 * It's for avoid many requests connections to database. By joksrc.
	 * 
	 * @param config
	 * @return
	 */
	public List<HorarioAjustador> guardarHorariosAjustadorSP(
			ConfiguracionDeDisponibilidadDTO config);
	
	
	
	/**
	 * Funcion para guardar los HorarioAjustador en la lista de dias de la
	 * configuracion
	 * 
	 * @param config
	 * @return
	 */
	public List<HorarioAjustador> guardarHorariosAjustador(
			ConfiguracionDeDisponibilidadDTO config);

	/**
	 * 
	 * Funcion para ejecutar el envio de correos grupal
	 * 
	 * @param enviarARolesSeleccionados
	 * @param bytes
	 * @param nombreArchivo
	 */
	public void enviarPorCorreoGrupal(String[] enviarARolesSeleccionados,
			List<Long> ajustadoresIds, byte[] bytes);

	/**
	 * 
	 * Funcion para ejecutar el envio de correos a un ajustador
	 * 
	 * @param ajustadorId
	 * @param bytes
	 */
	public void enviarPorCorreoIndividual(Long ajustadorId, byte[] bytes);

	/**
	 * Funcion para borrar horario Ajustador
	 * 
	 * @param horarioAjustador
	 */
	public HorarioAjustador borrar(HorarioAjustador horarioAjustador);

	/**
	 * Funcion que implementa parte de la RDN
	 * {92BC06EF-C4D3-4001-B0DF-E2ECBACE3F89} / Analisis MIDAS II.Requirements
	 * Model.Requerimientos MODULO SINIESTROS AUTOS.CABINA.Configurador de
	 * Horarios de Ajustadores.RDN: Cargar Información para Configurar
	 */
	public Date getFechaFinalLimite();

	/**
	 * Funcion que implementa parte de la RDN
	 * {92BC06EF-C4D3-4001-B0DF-E2ECBACE3F89} / Analisis MIDAS II.Requirements
	 * Model.Requerimientos MODULO SINIESTROS AUTOS.CABINA.Configurador de
	 * Horarios de Ajustadores.RDN: Cargar Información para Configurar
	 */
	public Date getFechaInicioLimite();

	/**
	 * Funcion que valida la RDN {3B578676-B7F8-4fee-8A1F-34D8E1864BF1} /
	 * Analisis MIDAS II.Requirements Model.Requerimientos MODULO SINIESTROS
	 * AUTOS.CABINA.Configurador de Horarios de Ajustadores.RDN: Búsqueda por
	 * varios campos capturados
	 * 
	 * TODO: Cambio requerido en RDN para reflejar esta regla
	 */
	public void validarCamposRequeridosFiltro(HorarioAjustadorFiltro filtro)
			throws NegocioEJBExeption;
	
	/**
	 * Nuevo metodo para eliminar las configuraciones de horario de ajustador al seleccionar varios.
	 * @param horariosIdsSeleccionados
	 * @return
	 */
	public List<HorarioAjustador> borrar(List<String> horariosIdsSeleccionados) throws NegocioEJBExeption;

	/**
	 * Obtener disponibilidad ajustador por ajustador en una fecha y hora dada
	 * @param ajustador
	 * @param fecha
	 * @return
	 */
	public TipoDisponibilidad obtenerDisponibilidadAjustadorEnFechaHora(ServicioSiniestro ajustador, Date fecha);
	
	
	class HorarioAjustadorFiltro extends CatalogoFiltro {

		private Long			oficinaId;

		private List<String>	ajustadoresIds;

		private String			tipoDisponibilidadCode;

		private Date			fechaInicial;

		private Date			fechaFinal;

		private String			horarioInicialCode;

		private String			horarioFinalCode;
		
		private Boolean 		activo = Boolean.TRUE;

		public Long getOficinaId() {
			return oficinaId;
		}

		public void setOficinaId(Long oficinaId) {
			this.oficinaId = oficinaId;
		}

		public List<String> getAjustadoresIds() {
			return ajustadoresIds;
		}

		public void setAjustadoresIds(List<String> ajustadoresIds) {
			this.ajustadoresIds = ajustadoresIds;
		}

		public String getTipoDisponibilidadCode() {
			return tipoDisponibilidadCode;
		}

		public void setTipoDisponibilidadCode(String tipoDisponibilidadCode) {
			this.tipoDisponibilidadCode = tipoDisponibilidadCode;
		}

		public Date getFechaInicial() {
			return fechaInicial;
		}

		public void setFechaInicial(Date fechaInicial) {
			this.fechaInicial = fechaInicial;
		}

		public Date getFechaFinal() {
			return fechaFinal;
		}

		public void setFechaFinal(Date fechaFinal) {
			this.fechaFinal = fechaFinal;
		}

		public String getHorarioInicialCode() {
			return horarioInicialCode;
		}

		public void setHorarioInicialCode(String horarioInicialCode) {
			this.horarioInicialCode = horarioInicialCode;
		}

		public String getHorarioFinalCode() {
			return horarioFinalCode;
		}

		public void setHorarioFinalCode(String horarioFinalCode) {
			this.horarioFinalCode = horarioFinalCode;
		}

		public Boolean validarAjustadoresIds() {
			Boolean val = (ajustadoresIds != null && !ajustadoresIds.isEmpty());
			return val;
		}

		public Boolean validarOficinaId() {
			return oficinaId != null;
		}

		public Boolean validarHorarioInicialCode() {
			return validarStringNoNull(horarioInicialCode);
		}

		public Boolean validarHorarioFinalCode() {
			return validarStringNoNull(horarioFinalCode);
		}

		public Boolean validarTipoDisponibilidadCode() {
			return validarStringNoNull(tipoDisponibilidadCode);
		}

		public Boolean validarFechaInicial() {
			return fechaInicial != null;
		}

		public Boolean validarFechaFinal() {
			return fechaFinal != null;
		}

		private Boolean validarStringNoNull(String propiedad) {
			return propiedad != null && !propiedad.equalsIgnoreCase("");
		}

		public Boolean getActivo() {
			return activo;
		}

		public void setActivo(Boolean activo) {
			this.activo = activo;
		}
		
		

	}

}