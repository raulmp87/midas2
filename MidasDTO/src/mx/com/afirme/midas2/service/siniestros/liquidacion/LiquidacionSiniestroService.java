package mx.com.afirme.midas2.service.siniestros.liquidacion;


import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.liquidacion.LiquidacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.parametros.ParametroAutorizacionLiquidacion;
import mx.com.afirme.midas2.domain.siniestros.pagos.OrdenPagoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionProveedor;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.FacturaLiquidacionDTO;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.LiquidacionSiniestroRegistro;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.ParamAutLiquidacionDTO;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.ParamAutLiquidacionRegistro;
import mx.com.afirme.midas2.dto.siniestros.pagos.DesglosePagoConceptoDTO;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService.CatalogoFiltro;

/**
 * Definicion de m�todos del m�dulo de Pagos
 * @author usuario
 * @version 1.0
 * @updated 22-sep-2014 03:59:37 p.m.
 */
@Local
public interface LiquidacionSiniestroService {
	
	public class LiquidacionSiniestroFiltro extends CatalogoFiltro {
	
		private Long    oficinaId;
		private String	estatusLiquidacion;
		private String  tipoLiquidacion;
		private String  tipoOperacion;
		private Long    noLiquidacion;
		private Long    noProveedor;
		private String  nombreBeneficiario;
		private String  nombreProveedor;
		private String  noSiniestro;
		private String  folioFactura;
		private String  solicitadoPor;
		private String  numeroChequeReferencia;
		private String  autorizadoPor;
		private Integer tipoServicio;
		private Long    noSolicitudCheque;
		private String  origenLiquidacion;
		
		
		private String     condicionMontoTotal;
		private Boolean    esRangoMontoTotal;
		private BigDecimal montoTotalDe;
		private BigDecimal montoTotalHasta;
		
		private String     condicionFechaSolicitud;
		private Boolean    esRangoFechaSolicitud;
		private Date       fechaSolicitudDe;
		private Date       fechaSolicitudHasta;
		
		private String     condicionFechaAutorizacion;
		private Boolean    esRangoFechaAutorizacion;
		private Date       fechaAutorizacionDe;
		private Date       fechaAutorizacionHasta;
		
		private String     condicionFechaElaboracion;
		private Boolean    esRangoFechaElaboracion;
		private Date       fechaElaboracionDe;
		private Date       fechaElaboracionHasta;
		
		
		
		
		public Long getOficinaId() {
			return oficinaId;
		}

		public void setOficinaId(Long oficinaId) {
			this.oficinaId = oficinaId;
		}


		public String getEstatusLiquidacion() {
			return estatusLiquidacion;
		}

		public void setEstatusLiquidacion(String estatusLiquidacion) {
			this.estatusLiquidacion = estatusLiquidacion;
		}


		public String getTipoLiquidacion() {
			return tipoLiquidacion;
		}


		public void setTipoLiquidacion(String tipoLiquidacion) {
			this.tipoLiquidacion = tipoLiquidacion;
		}




		public String getTipoOperacion() {
			return tipoOperacion;
		}




		public void setTipoOperacion(String tipoOperacion) {
			this.tipoOperacion = tipoOperacion;
		}




		public Long getNoLiquidacion() {
			return noLiquidacion;
		}




		public void setNoLiquidacion(Long noLiquidacion) {
			this.noLiquidacion = noLiquidacion;
		}




		public Long getNoProveedor() {
			return noProveedor;
		}




		public void setNoProveedor(Long noProveedor) {
			this.noProveedor = noProveedor;
		}




		public String getNombreProveedor() {
			return nombreProveedor;
		}




		public void setNombreProveedor(String nombreProveedor) {
			this.nombreProveedor = nombreProveedor;
		}




		public String getNoSiniestro() {
			return noSiniestro;
		}




		public void setNoSiniestro(String noSiniestro) {
			this.noSiniestro = noSiniestro;
		}




		public String getFolioFactura() {
			return folioFactura;
		}




		public void setFolioFactura(String folioFactura) {
			this.folioFactura = folioFactura;
		}




		public String getSolicitadoPor() {
			return solicitadoPor;
		}




		public void setSolicitadoPor(String solicitadoPor) {
			this.solicitadoPor = solicitadoPor;
		}




		public String getNumeroChequeReferencia() {
			return numeroChequeReferencia;
		}




		public void setNumeroChequeReferencia(String numeroChequeReferencia) {
			this.numeroChequeReferencia = numeroChequeReferencia;
		}




		public String getAutorizadoPor() {
			return autorizadoPor;
		}




		public void setAutorizadoPor(String autorizadoPor) {
			this.autorizadoPor = autorizadoPor;
		}




		public Integer getTipoServicio() {
			return tipoServicio;
		}




		public void setTipoServicio(Integer tipoServicio) {
			this.tipoServicio = tipoServicio;
		}




		public String getCondicionMontoTotal() {
			return condicionMontoTotal;
		}




		public void setCondicionMontoTotal(String condicionMontoTotal) {
			this.condicionMontoTotal = condicionMontoTotal;
		}




		public Boolean getEsRangoMontoTotal() {
			return esRangoMontoTotal;
		}




		public void setEsRangoMontoTotal(Boolean esRangoMontoTotal) {
			this.esRangoMontoTotal = esRangoMontoTotal;
		}




		public BigDecimal getMontoTotalDe() {
			return montoTotalDe;
		}




		public void setMontoTotalDe(BigDecimal montoTotalDe) {
			this.montoTotalDe = montoTotalDe;
		}




		public BigDecimal getMontoTotalHasta() {
			return montoTotalHasta;
		}




		public void setMontoTotalHasta(BigDecimal montoTotalHasta) {
			this.montoTotalHasta = montoTotalHasta;
		}




		public String getCondicionFechaSolicitud() {
			return condicionFechaSolicitud;
		}




		public void setCondicionFechaSolicitud(String condicionFechaSolicitud) {
			this.condicionFechaSolicitud = condicionFechaSolicitud;
		}




		public Boolean getEsRangoFechaSolicitud() {
			return esRangoFechaSolicitud;
		}




		public void setEsRangoFechaSolicitud(Boolean esRangoFechaSolicitud) {
			this.esRangoFechaSolicitud = esRangoFechaSolicitud;
		}




		public Date getFechaSolicitudDe() {
			return fechaSolicitudDe;
		}




		public void setFechaSolicitudDe(Date fechaSolicitudDe) {
			this.fechaSolicitudDe = fechaSolicitudDe;
		}




		public Date getFechaSolicitudHasta() {
			return fechaSolicitudHasta;
		}




		public void setFechaSolicitudHasta(Date fechaSolicitudHasta) {
			this.fechaSolicitudHasta = fechaSolicitudHasta;
		}




		public String getCondicionFechaAutorizacion() {
			return condicionFechaAutorizacion;
		}




		public void setCondicionFechaAutorizacion(String condicionFechaAutorizacion) {
			this.condicionFechaAutorizacion = condicionFechaAutorizacion;
		}




		public Boolean getEsRangoFechaAutorizacion() {
			return esRangoFechaAutorizacion;
		}




		public void setEsRangoFechaAutorizacion(Boolean esRangoFechaAutorizacion) {
			this.esRangoFechaAutorizacion = esRangoFechaAutorizacion;
		}




		public Date getFechaAutorizacionDe() {
			return fechaAutorizacionDe;
		}




		public void setFechaAutorizacionDe(Date fechaAutorizacionDe) {
			this.fechaAutorizacionDe = fechaAutorizacionDe;
		}




		public Date getFechaAutorizacionHasta() {
			return fechaAutorizacionHasta;
		}




		public void setFechaAutorizacionHasta(Date fechaAutorizacionHasta) {
			this.fechaAutorizacionHasta = fechaAutorizacionHasta;
		}




		public String getCondicionFechaElaboracion() {
			return condicionFechaElaboracion;
		}




		public void setCondicionFechaElaboracion(String condicionFechaElaboracion) {
			this.condicionFechaElaboracion = condicionFechaElaboracion;
		}




		public Boolean getEsRangoFechaElaboracion() {
			return esRangoFechaElaboracion;
		}




		public void setEsRangoFechaElaboracion(Boolean esRangoFechaElaboracion) {
			this.esRangoFechaElaboracion = esRangoFechaElaboracion;
		}




		public Date getFechaElaboracionDe() {
			return fechaElaboracionDe;
		}




		public void setFechaElaboracionDe(Date fechaElaboracionDe) {
			this.fechaElaboracionDe = fechaElaboracionDe;
		}




		public Date getFechaElaboracionHasta() {
			return fechaElaboracionHasta;
		}




		public void setFechaElaboracionHasta(Date fechaElaboracionHasta) {
			this.fechaElaboracionHasta = fechaElaboracionHasta;
		}

		public Long getNoSolicitudCheque() {
			return noSolicitudCheque;
		}

		public void setNoSolicitudCheque(Long noSolicitudCheque) {
			this.noSolicitudCheque = noSolicitudCheque;
		}

		@Override
		public String toString() {
			return "LiquidacionSiniestroFiltro [oficinaId=" + oficinaId
					+ ", estatusLiquidacion=" + estatusLiquidacion
					+ ", tipoLiquidacion=" + tipoLiquidacion
					+ ", tipoOperacion=" + tipoOperacion + ", noLiquidacion="
					+ noLiquidacion + ", noProveedor=" + noProveedor
					+ ", nombreBeneficiario=" + nombreBeneficiario
					+ ", nombreProveedor=" + nombreProveedor + ", noSiniestro="
					+ noSiniestro + ", folioFactura=" + folioFactura
					+ ", solicitadoPor=" + solicitadoPor
					+ ", numeroChequeReferencia=" + numeroChequeReferencia
					+ ", autorizadoPor=" + autorizadoPor + ", tipoServicio="
					+ tipoServicio + ", noSolicitudCheque=" + noSolicitudCheque
					+ ", origenLiquidacion=" + origenLiquidacion
					+ ", condicionMontoTotal=" + condicionMontoTotal
					+ ", esRangoMontoTotal=" + esRangoMontoTotal
					+ ", montoTotalDe=" + montoTotalDe + ", montoTotalHasta="
					+ montoTotalHasta + ", condicionFechaSolicitud="
					+ condicionFechaSolicitud + ", esRangoFechaSolicitud="
					+ esRangoFechaSolicitud + ", fechaSolicitudDe="
					+ fechaSolicitudDe + ", fechaSolicitudHasta="
					+ fechaSolicitudHasta + ", condicionFechaAutorizacion="
					+ condicionFechaAutorizacion
					+ ", esRangoFechaAutorizacion=" + esRangoFechaAutorizacion
					+ ", fechaAutorizacionDe=" + fechaAutorizacionDe
					+ ", fechaAutorizacionHasta=" + fechaAutorizacionHasta
					+ ", condicionFechaElaboracion="
					+ condicionFechaElaboracion + ", esRangoFechaElaboracion="
					+ esRangoFechaElaboracion + ", fechaElaboracionDe="
					+ fechaElaboracionDe + ", fechaElaboracionHasta="
					+ fechaElaboracionHasta + "]";
		}

		public String getNombreBeneficiario() {
			return nombreBeneficiario;
		}

		public void setNombreBeneficiario(String nombreBeneficiario) {
			this.nombreBeneficiario = nombreBeneficiario;
		}

		public String getOrigenLiquidacion() {
			return origenLiquidacion;
		}

		public void setOrigenLiquidacion(String origenLiquidacion) {
			this.origenLiquidacion = origenLiquidacion;
		}


	}
	
	
	/*VARIABLES*/
	public static final String ESTATUS_REGISTADA="1";
	public static final String ESTATUS_PROCESADA="2";
	public static final String ESTATUS_PAGADA="3";
	public static final String ESTATUS_CANCELADA="4";
	
	public static final String CONCEPTO_TRANSACCION="PSIN";
	public static final String TIPO_OPERACION="85";
	
	public static final short ASOCIAR_ORDEN_PAGO = 1; 
	public static final Short DESASOCIAR_ORDEN_PAGO = -1;
	

	/**
	 * Guarda una liquidacion de pago
	 * @param OrdenPagoSiniestro ordenPago
	 */
	public LiquidacionSiniestro guardarLiquidacion(LiquidacionSiniestro liquidacionSiniestro);
	
	/**
	 * crea un nuevo registro detalle de liquidacion .
	 * 
	 * @param detalleOrdenCompra
	 * @param idOrdenCompr
	 */
	public LiquidacionSiniestro asociarFactura(DocumentoFiscal factura, Long idLiquidacion) throws Exception;
	
	public LiquidacionSiniestro desasociarFactura(DocumentoFiscal factura, Long idLiquidacion) throws Exception;
	
	public void recalcularMontosLiquidacion(LiquidacionSiniestro liquidacion) throws IllegalArgumentException;
	
	public List<LiquidacionSiniestroRegistro> buscarLiquidacionDetalle(LiquidacionSiniestroFiltro filtro);
	
	public List<LiquidacionSiniestroRegistro> buscarLiquidacionDetalle(Map<String,String> estatus, String origenLiquidacion);
		
	public void  enviarSolicitudLiquidacion(Long idLiquidacion) throws Exception;

	/**
	 * Metodo para imprimir una orden de expedicion de cheque;
	 * @param idOrdenPago
	 * @return
	 */
	public TransporteImpresionDTO imprimirOrdenExpedicionCheque(Long idLiquidacion);	
	
	public void eliminarLiquidacion(Long idLiquidacion) throws Exception;
	
	public void reexpedir(Long idLiquidacion) throws Exception;
	
	public void solicitarAutorizacion(Long idLiquidacion) throws Exception;
	
	public List<DocumentoFiscal> buscarFacturasLiquidacion(Long idLiquidacion);
	
	public List<FacturaLiquidacionDTO> buscarOrdenesPago(Long idLiquidacion); //TODO revisar si se va a ocupar
	
	public List<Map<String, Object>> buscarProveedoresLiquidacion();
	
	/**
	 * Solicita el cheque a Mizar y se Asigna ala liquidacion.
	 * @param Long idLiquidacion 
	 * @return SolicitudChequeSiniestro
	 */
	public void solicitarChequeLiquidacion(Long idLiquidacion) throws Exception ;
	
	public void rechazarLiquidacion(Long idLiquidacion);
	
	public List<ParamAutLiquidacionRegistro> buscarParametrosAutorizacion  (ParamAutLiquidacionDTO filtro);
	
	public ParametroAutorizacionLiquidacion obtenerParametroAutorizacion  (Long idParametro);
	
	public String guardarParametroAutorizacion (ParametroAutorizacionLiquidacion parametro, String oficinasConcat, String estadosConcat, String proveedoresConcat, String usuariosSolConcat, String usuariosAutConcat);
	
	public void cambiarEstatusParametroAut  (Long idParametro, Integer estatus); 
	/**
	 * Obtiene la clave de negocio del reporte para la impresion de una orden de expedicion de cheque
	 * @param idLiqSiniestro
	 * @return
	 */
	public String obtenerClaveNegocio(Long idLiqSiniestro);

	/**
	 * Valida si la liquidacion se puede mandar a imprimir
	 * @param idLiqSiniestro
	 * @return
	 */
	public Boolean validarImpresionLiquidacion(Long idLiqSiniestro);
	
	public LiquidacionSiniestro procesarFacturaSeleccionada(Long idFactura, Long idLiquidacion, Short tipoOperacion) throws Exception;
	
	public LiquidacionSiniestro guardarLiquidacion(LiquidacionSiniestro liquidacionSiniestro,
			LiquidacionSiniestro.EstatusLiquidacionSiniestro estatus);
	
	public void validarEnvioSolicitudLiquidacion(Long idLiquidacion) throws Exception ;
	
	public Boolean validarUsuarioEsAutorizador(Long idLiquidacion);
	
	public Short cancelarLiquidacion(Long idLiquidacion, Short tipoCancelacion);
	
	public List<OrdenPagoSiniestro> buscarOrdenesPagoIndemnizacion(Long idLiquidacion);
	
	public LiquidacionSiniestro procesarOrdenPagoSeleccionada(Long idOrdenPago, Long idLiquidacion, Short tipoOperacion) throws Exception;
	
	public LiquidacionSiniestro asociarOrdenPago(OrdenPagoSiniestro ordenPago, LiquidacionSiniestro liquidacionSiniestro) throws Exception;
	
	public LiquidacionSiniestro desasociarOrdenPago(OrdenPagoSiniestro ordenPago, LiquidacionSiniestro liquidacionSiniestro) throws Exception;
	
	public BigDecimal obtenerPrimaPendientePago(Long idLiquidacion);
	
	public BigDecimal obtenerPrimaNoDevengada(Long idLiquidacion);	
	
	public Boolean tieneIndemnizacionesPT(OrdenPagoSiniestro ordenPago);
	/**
	 * Utilizar el findById para obtener la liquidación
	 * regresar liquidacion.getRecuperaciones
	 * 
	 * @param idLiquidacion
	 */
	public List<RecuperacionProveedor> obtenerRecuperacionesAsociadas(Long idLiquidacion);

	/**
	 * invocar this.liquidacionSiniestroDAO.obtenerRecuperacionesPendientesPorAsignar
	 * y retornar
	 * 
	 * @param idProveedor
	 */
	public List<RecuperacionProveedor> obtenerRecuperacionesPendientesPorAsignar(Integer idProveedor);
	
	public LiquidacionSiniestro procesarRecuperacionSeleccionada(Long idRecuperacion, Long idLiquidacion, Short tipoOperacion) throws Exception;
	
	public LiquidacionSiniestro asociarRecuperacion(RecuperacionProveedor recuperacion, LiquidacionSiniestro liquidacionSiniestro) throws Exception;
	
	public LiquidacionSiniestro desasociarRecuperacion(RecuperacionProveedor recuperacion, LiquidacionSiniestro liquidacionSiniestro) throws Exception;
	
	/**
	 * retorna verdadero si la liquidacion esta relacionada a indemnizaciones que tienen como forma de pago transferencia bancaria
	 * @param idLiquidacion
	 * @return
	 */
	public Boolean tieneIndemnizaciones(Long idLiquidacion);
	
	/***
	 * retorna verdadero si la orden de pago  esta marcadado como perdida total por cualquier cobertura RCV, DMA, RT
	 */
	public Boolean isIndemnizacionesPT(OrdenPagoSiniestro ordenPago);
	
	/**
	 * 
	 * @param listaFacturasLiquidacion
	 * @return
	 */
	public List<DocumentoFiscal> ordenaFacturasLiquidacion(List<DocumentoFiscal> listaFacturasLiquidacion);
	
	/**
	 * Get process for selected bills to associated liquidation.
	 * @param idFactura
	 * @param idLiquidacion
	 * @param tipoOperacion
	 * @return
	 */
	public LiquidacionSiniestro procesarFacturaSeleccionadaSP(String listaFacturas, Long idLiquidacion, Short tipoOperacion) throws Exception;
	
	public LiquidacionSiniestro procesarFacturasSeleccionadas(String idFacturas, Long idLiquidacion, Short tipoOperacion) throws Exception;
	
	public LiquidacionSiniestro asociaFacturas(String idFacturas, Long idLiquidacion) throws Exception;
	public LiquidacionSiniestro desasociarFacturas(String idFacturas, Long idLiquidacion) throws Exception;
	public List<DesglosePagoConceptoDTO> obtenerPagosPorLiquidacion (Long idLiquidacion);
	public LiquidacionSiniestro procesarRecuperacionesSeleccionadas(String idRecuperacionesStr, Long idLiquidacion, Short tipoOperacion) throws Exception;
	public void recalcularMontosLiquidacionSP(Long liquidacionId);
	
}