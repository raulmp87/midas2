package mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.gastoajuste;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.gastoajuste.GastoAjusteReporte;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.TipoPrestadorServicio;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.ValeTallerDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.gastoajuste.GastoAjusteDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.gastoajuste.PrestadorGastoAjusteDTO;

/**
 * @author Usuario
 *
 */
@Local
public interface GastoAjusteService {
	
	/**
	 * @param idGasto
	 * @return
	 */
	public GastoAjusteReporte buscarGasto(Long idGasto);
	
	/**
	 * @param idReporteCabina
	 * @param gastoAjuste
	 */
	public void guardarGastoAjuste(Long idReporteCabina, GastoAjusteDTO gastoAjuste);
	/**
	 * @param idReporteCabina
	 * @return
	 */
	public List<Map<String, Object>> obtenerGastosAjuste(Long idReporteCabina);
	/**
	 * @return
	 */
	public Map<String, String> consultarPrestadoresGastoAjuste();
	
	/**
	 * @param nombre
	 * @param tipoPrestador
	 * @return
	 */
	public List<PrestadorGastoAjusteDTO> buscarPrestadorPorNombreYTipo(String nombre, String tipoPrestador);
	
	/**
	 * @param nombre
	 * @return
	 */
	public TipoPrestadorServicio buscarTipoPrestadorbyNombre(String nombre);
	
	/**
	 * @param idTipoPrestador
	 * @return
	 */
	public TipoPrestadorServicio buscarTipoPrestador(Long idTipoPrestador);
	
	/**
	 * 
	 * @param valeTaller
	 * @return
	 */
	public TransporteImpresionDTO imprimirValeTaller( ValeTallerDTO valeTaller );
}
