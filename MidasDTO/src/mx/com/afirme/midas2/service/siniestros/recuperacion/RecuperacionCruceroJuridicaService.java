package mx.com.afirme.midas2.service.siniestros.recuperacion;

import java.math.BigDecimal;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.EstatusRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionCruceroJuridica;

@Local
public interface RecuperacionCruceroJuridicaService extends RecuperacionService {
	
	public RecuperacionCruceroJuridica guardar(RecuperacionCruceroJuridica recuperacion);
	public BigDecimal montoTotalRecuperacionCrucero(Long siniestroCabinaId, EstatusRecuperacion estatusRecuperacion);
	
}
