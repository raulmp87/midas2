package mx.com.afirme.midas2.service.siniestros.incentivos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.incentivos.ConfiguracionIncentivos;
import mx.com.afirme.midas2.domain.siniestros.incentivos.ConfiguracionIncentivosNotificacion;
import mx.com.afirme.midas2.domain.siniestros.incentivos.IncentivoAjustador.ConceptoIncentivo;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.incentivos.IncentivoAjustadorDTO;

@Local
public interface ConfiguracionIncentivosService {
	
	public List<ConfiguracionIncentivos> obtenerConfiguracionesParaBandejaAutorizacion();
	
	public void autorizaConfiguracion(Long configuracionId);

	public void rechazaConfiguracion(Long configuracionId);
	
	public List<IncentivoAjustadorDTO> obtenerIncentivoPreliminar(Long configuracionId);
	
	public List<IncentivoAjustadorDTO> obtenerIncentivos(Long configuracionId, ConceptoIncentivo concepto);
	
	public List<ConfiguracionIncentivosNotificacion> obtenerHistoricoNotificaciones(Long idConfiguracion);
	
	public void enviaNotificacion(Long idConfiguracion,String destinatariosStr);
	
	public List<IncentivoAjustadorDTO> obtenerIncentivosAjustador(Long configuracionId);
	
	/**
	 * Metodo para generar la impresion de las percepciones de los ajustadores
	 * @param configIncentivosId
	 * @return
	 */
	public TransporteImpresionDTO imprimirPercepcionAjustadores(Long configIncentivosId);

	/**
	 * obtiene la lista de todas las configuraciones creadas para el listado de percepciones
	 * ordenadas de la más reciente a la mas antigua
	 * @return
	 */
	public List<ConfiguracionIncentivos> obtenerConfiguracionesParaListadoPercepciones();
	
	/**
	 * servicio que persiste la configuracion de incentivos capturada por el usuario
	 * @param configuracion
	 * @return
	 */
	public ConfiguracionIncentivos guardarConfiguracionIncentivos(ConfiguracionIncentivos configuracion);
	
	public ConfiguracionIncentivos guardarConfiguracionIncentivosTransaction(ConfiguracionIncentivos configuracion);

	/**
	 * Cancela las configuraciones 
	 * @param configuracionId
	 */
	public ConfiguracionIncentivos cancelarConfiguracion(Long configuracionId);
	public ConfiguracionIncentivos obtenerConfiguracionIncentivos (Long configuracionId);
	
	public void preRegistroIncentivos(Long configuracionId, String incentivosConcat);
	
	public void actualizarIncentivos(Long configuracionId, String incentivosConcat);
	
	public ConfiguracionIncentivos registrarPercepcion(Long configuracionId);

	public void eliminarConfiguracion(Long configuracionId);

	public void enviaAutorizacion(Long configuracionId);
	
}
