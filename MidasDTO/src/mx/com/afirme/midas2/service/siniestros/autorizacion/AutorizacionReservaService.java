package mx.com.afirme.midas2.service.siniestros.autorizacion;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.autorizacion.ParametroAutorizacionReserva;
import mx.com.afirme.midas2.domain.siniestros.autorizacion.ParametroReservaAntiguo;
import mx.com.afirme.midas2.dto.siniestros.depuracion.CoberturaSeccionSiniestroDTO;



/**
 * @author simavera
 */
@Local
public interface AutorizacionReservaService {

	/**
	 * Metodo que elimina un parametro
	 * @param idParametro id del Parametro de Autorizacion de Reserva
	 */
	public void eliminarParamstatic( Long idParametro );

	/**
	 * <b>SE AGREGA EN CDU AUTORIZACI�N DE AJUSTE DE RESERVA</b>
	 * Metodo que valida que la nueva estimacion de la Cobertura cumpla con los
	 * parametros de Configuracion.
	 * 
	 * @param idEstimacionCob
	 * 
	 */
	public String validarParametros (Long idEstimacionCob, BigDecimal estimacionNueva);

	/**
	 * Metodo que obtiene el listado de coberturas que pertenecen a una seccion 
	 * y no es de Suma Asegurada Amparada
	 * 
	 * @param idToSeccion id de la Seccion relacionada a las coberturas
	 */
	public List<CoberturaSeccionSiniestroDTO> obtenerCoberturasPorSeccion(Long idToSeccion);

	/**
	 * Metodo que obtiene el listado de todos los parametros de autorizacion
	 */
	public List<ParametroAutorizacionReserva> listarParametros();

	/**
	 * Metodo que obtiene el ParametroAutorizacionReserva 
	 * 
	 * @param idParametro id del Parametro de Autorizacion de Reserva
	 */
	public ParametroAutorizacionReserva obtenerParametro(Long idParametro);

	/**
	 * Metodo que  guarda un nuevo o modifica un parametro de autorizacion.
	 * 
	 * @param parametro entidad con los parametros configurados 
	 *                  para la autorizacion de la reserva
	 */
	public void guardarParametro(ParametroAutorizacionReserva parametro);
	
	public Map<String,String> obtenerRolesAutorizacion();
	
	public List<ParametroReservaAntiguo> listarParametrosAntiguedad();
	
	public void inactivarParametroAntiguedad(Long idParametro);
	
	public ParametroReservaAntiguo obtenerParametroAntiguedad(Long idParametro);
	
	public void guardarParametroAntiguedad(ParametroReservaAntiguo parametro);
	
	public Long validarParametrosAntiguedad(Long idEstimacionCob,BigDecimal estimacionNueva);
	
}