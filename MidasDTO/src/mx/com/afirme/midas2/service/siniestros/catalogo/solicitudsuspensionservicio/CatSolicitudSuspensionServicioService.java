/**
 * 
 */
package mx.com.afirme.midas2.service.siniestros.catalogo.solicitudsuspensionservicio;

import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation.OperationType;
import mx.com.afirme.midas2.domain.siniestros.catalogo.solicitudsuspensionservicio.SolicitudSuspensionServicio;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService.CatalogoFiltro;

/**
 * @author simavera
 *
 */
public interface CatSolicitudSuspensionServicioService {

	public List<SolicitudSuspensionServicio> buscar(SolicitudSuspensionServicioFiltro filtro);
	
	public SolicitudSuspensionServicio getById( Integer idSuspensionServicio );
	
	public void save( SolicitudSuspensionServicio solicitudSuspensionServicio );
	
	public String findCurrentUser();
    
    public class SolicitudSuspensionServicioFiltro extends CatalogoFiltro {
          
    	private Date fechaInicio;
    	private Date fechaFin;
    	private Date fechaModEstatus;
    	private String motivo;
    	private String numeroSerie;
    	
    	@Override
    	@FilterPersistenceAnnotation(persistenceName="id")
    	public Long getNumeroCatalogo(){
    		return numeroCatalogo;
    	}
    	
    	@Override
    	@FilterPersistenceAnnotation(persistenceName="oficina.id")
    	public Long getOficinaId() {
			return oficinaId;
		}
		/**
		 * @return the fechaInicio
		 */
    	
    	@FilterPersistenceAnnotation(persistenceName="fechaInicio", truncateDate=true , operation=OperationType.GREATERTHANEQUAL, paramKey="fechaInicio")    
		public Date getFechaInicio() {
			return fechaInicio;
		}
		/**
		 * @param fechaInicio the fechaInicio to set
		 */
		public void setFechaInicio(Date fechaInicio) {
			this.fechaInicio = fechaInicio;
		}
		/**
		 * @return the fechaFin
		 */
		@FilterPersistenceAnnotation(persistenceName="fechaFin", truncateDate=true , operation=OperationType.LESSTHANEQUAL, paramKey="fechaFin")
		public Date getFechaFin() {
			return fechaFin;
		}
		/**
		 * @param fechaFin the fechaFin to set
		 */		   	
		public void setFechaFin(Date fechaFin) {
			this.fechaFin = fechaFin;
		}
		/**
		 * @return the fechaModEstatus
		 */
		@FilterPersistenceAnnotation(persistenceName="fechaModEstatus", truncateDate=true)
		public Date getFechaModEstatus() {
			return fechaModEstatus;
		}
		/**
		 * @param fechaModEstatus the fechaModEstatus to set
		 */
		public void setFechaModEstatus(Date fechaModEstatus) {
			this.fechaModEstatus = fechaModEstatus;
		}
		/**
		 * @return the motivo
		 */
		public String getMotivo() {
			return motivo;
		}
		/**
		 * @param motivo the motivo to set
		 */
		public void setMotivo(String motivo) {
			this.motivo = motivo;
		}
		/**
		 * @return the numeroSerie
		 */
		public String getNumeroSerie() {
			return numeroSerie;
		}
		/**
		 * @param numeroSerie the numeroSerie to set
		 */
		public void setNumeroSerie(String numeroSerie) {
			this.numeroSerie = numeroSerie;
		}

    }
    
    
}
