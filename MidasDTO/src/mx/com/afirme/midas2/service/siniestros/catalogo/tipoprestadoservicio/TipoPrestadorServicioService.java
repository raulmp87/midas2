package mx.com.afirme.midas2.service.siniestros.catalogo.tipoprestadoservicio;

import java.util.List;

import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.TipoPrestadorServicio;

public interface TipoPrestadorServicioService {
	public List<TipoPrestadorServicio> buscarTiposPrestadorSerByFilter(List<Long> lista);
}
