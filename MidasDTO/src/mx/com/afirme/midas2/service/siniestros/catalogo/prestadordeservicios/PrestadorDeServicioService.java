package mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas.sistema.seguridad.UsuarioPrestadorServicio;
import mx.com.afirme.midas2.annotation.Exportable;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation.OperationType;
import mx.com.afirme.midas2.domain.personadireccion.PersonaMidas;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService.CatalogoFiltro;

@Local
public interface PrestadorDeServicioService {

	public class PrestadorServicioFiltro extends CatalogoFiltro {

		private String tipoPrestadorStr;

		private String nombrePersona;
		
		private Long oficinaId;
		
		private Integer estatus;
		
		private String estado;

		@FilterPersistenceAnnotation(excluded = true)
		public String getTipoPrestadorStr() {
			return tipoPrestadorStr;
		}


		public void setTipoPrestadorStr(String tipoPrestadorStr) {
			this.tipoPrestadorStr = tipoPrestadorStr;
		}
		

		@Override
		@FilterPersistenceAnnotation(persistenceName = "id")
		public Long getNumeroPrestador() {
			return this.numeroPrestador;
		}

		
		@FilterPersistenceAnnotation(persistenceName = "personaMidas.nombre", operation=OperationType.LIKE)
		public String getNombrePersona() {
			return nombrePersona;
		}

		public void setNombrePersona(String nombrePersona) {
			this.nombrePersona = nombrePersona;
		}

		@FilterPersistenceAnnotation(persistenceName = "oficina.id")
		public Long getOficinaId() {
			return oficinaId;
		}

		public void setOficinaId(Long oficinaId) {
			this.oficinaId = oficinaId;
		}

		public Integer getEstatus() {
			return estatus;
		}

		public void setEstatus(Integer estatus) {
			this.estatus = estatus;
		}

		@FilterPersistenceAnnotation(excluded=true)
		public String getEstado() {
			return estado;
		}

		public void setEstado(String estado) {
			this.estado = estado;
		}
	}
	
	
	public class PrestadorServicioRegistro implements Serializable{
		
		/**
		 * 
		 */
		private static final long serialVersionUID = -4826781963824666469L;

		private Long id;
		
		private Oficina oficina;
		
		private PersonaMidas personaMidas;
		
		private String tipoPrestador;	
		
		private boolean estatus;
		
		private String nombreOficinaDesc;
		
		private PrestadorServicio prestador;
		
		private String nombrePrestador;
		
		public PrestadorServicioRegistro() {
			super();
		}

		public PrestadorServicioRegistro(Integer id, Oficina oficina, 
				PersonaMidas persona, Object tipoPrestador, Integer estatus){
			this(Long.valueOf(id.toString()), oficina, persona, tipoPrestador != null ? tipoPrestador.toString() : null, estatus.intValue() == 0 ? false : true);
		}
		
		
		public PrestadorServicioRegistro(Long id, Oficina oficina,
				PersonaMidas persona, String tipoPrestador, boolean estatus) {
			this();
			this.id = id;
			this.oficina = oficina;
			this.personaMidas = persona;
			this.tipoPrestador = tipoPrestador;
			this.estatus = estatus;
		}
		
		public PrestadorServicioRegistro(PrestadorServicio prestador) {
			this();
			this.prestador = prestador;
		}
		
		public PrestadorServicioRegistro(Integer id, PersonaMidas persona){
			this.id = Long.valueOf(id.toString());
			this.personaMidas = persona;
		}
		
		public PrestadorServicioRegistro(Integer id, PersonaMidas persona, String nombreOficinaDesc){
			this.id = Long.valueOf(id.toString());
			this.personaMidas = persona;
			this.nombreOficinaDesc = nombreOficinaDesc;
		}

		@Exportable(columnName="No. PRESTADOR", columnOrder=0)
		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		@Exportable(columnName="NOMBRE", columnOrder=1)
		public String getNombrePersona() {
				return personaMidas != null ? personaMidas.getNombre() : null;
		}

		@Exportable(columnName="OFICINA", columnOrder=2)
		public String getNombreOficina(){	
			return oficina != null ? oficina.getNombreOficina() : null;
			//return this.nombreOficinaDesc;
		}
		
		public Oficina getOficina() {
			return oficina;
		}

		public void setOficina(Oficina oficina) {
			this.oficina = oficina;
		}

		public PersonaMidas getPersonaMidas() {
			return personaMidas;
		}

		public void setPersonaMidas(PersonaMidas persona) {
			this.personaMidas = persona;
		}

		@Exportable(columnName="TIPO PRESTADOR", columnOrder=3)
		public String getTipoPrestador() {
			return tipoPrestador;
		}

		public void setTipoPrestador(String tipoPrestador) {
			this.tipoPrestador = tipoPrestador;
		}

		@Exportable(columnName="ESTATUS", columnOrder=4, fixedValues="false=INACTIVO,true=ACTIVO")
		public boolean isEstatus() {
			return estatus;
		}

		public void setEstatus(boolean estatus) {
			this.estatus = estatus;
		}

		public PrestadorServicio getPrestador() {
			return prestador;
		}

		public void setPrestador(PrestadorServicio prestador) {
			this.prestador = prestador;
		}

		public void setNombreOficina(String nombreOficinaDesc) {
			this.nombreOficinaDesc = nombreOficinaDesc;
		}
		
		public void setNombreOficinaDesc(String nombreOficinaDesc) {
			this.nombreOficinaDesc = nombreOficinaDesc;
		}

		public String getNombreOficinaDesc() {
			return this.nombreOficinaDesc;
		}

		public String getNombrePrestador() {
			return nombrePrestador;
		}

		public void setNombrePrestador(String nombrePrestador) {
			this.nombrePrestador = nombrePrestador;
		}
		
	}
	
	List<PrestadorServicioRegistro> buscar(PrestadorServicioFiltro filtro);

	public PrestadorServicio buscarPrestador(Integer idPrestadorServicio);
	
	public List<Map<String, Object>>  obtenerPrestadoresPorUsuario (String codigoUsuario );
	
	/**
	 * Guardar un prestador de servicio. //TODO utilizar este método en lugar de cualquier salvado en el action
	 * @param prestador
	 * @return
	 */
	public PrestadorServicio guardar(PrestadorServicio prestador);
	
	/**
	 * Obtener el usuario de un prestador en particular
	 * @param prestadorId
	 * @return
	 */
	public UsuarioPrestadorServicio obtenerUsuarioPorPrestador(Integer prestadorId);
	
	/**
	 * Actualiza el usuario de un prestador de servicio. 
	 * @param prestadorId
	 * @param claveUsuario
	 */
	public void actualizarUsuarioPrestador(Integer prestadorId, String claveUsuario);
	
	/**
	 * Generar usuario de ASM para el prestador de servicio. En caso de ya contar con un usuario retorna una excepcion de negocio.
	 * @param prestadorId
	 * @return
	 */
	public UsuarioPrestadorServicio generarUsuario(Integer prestadorId);
	
	
}
