package mx.com.afirme.midas2.service.siniestros.depuracion;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.siniestros.depuracion.ConfiguracionDepuracionReserva;
import mx.com.afirme.midas2.domain.siniestros.depuracion.DepuracionReserva;
import mx.com.afirme.midas2.domain.siniestros.depuracion.DepuracionReservaDetalle;
import mx.com.afirme.midas2.dto.siniestros.depuracion.CoberturaSeccionSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.depuracion.DepuracionReservaDTO;
import mx.com.afirme.midas2.dto.siniestros.depuracion.MovimientoPosteriorAjusteDTO;
import mx.com.afirme.midas2.dto.siniestros.depuracion.SeccionSiniestroDTO;

@Local
public interface DepuracionReservaService {
	
	public void asociarCobertura(Long idConfiguracion, BigDecimal idToSeccion, BigDecimal idToCobertura, String claveSubCalculo);
	
	public void asociarSeccion(Long idConfiguracion, BigDecimal idToSeccion);
	
	public void asociarTermino(Long idConfiguracion, String claveTermino);
	
	public void asociarTodasCoberturas(Long idConfiguracion);
	
	public void asociarTodosTerminos(Long idConfiguracion);
	
	public List<ConfiguracionDepuracionReserva> buscarConfiguraciones(DepuracionReservaDTO filtro);
	
	public DepuracionReserva ejecutarDepuracion(String idsDepuracionDetalle);
	
	public DepuracionReserva ejecutarDepuracionSP(String idsDepuracionDetalle);
	
	public void eliminarCobertura(Long idConfiguracion, BigDecimal idToCobertura);
	
	public void eliminarSeccion(Long idConfiguracion, BigDecimal idToSeccion);
	
	public void eliminarTermino(Long idConfiguracion, String claveTermino);
	
	public void eliminarTodasCoberturas(Long idConfiguracion);
	
	public void eliminarTodosTerminos(Long idConfiguracion);
	
	public DepuracionReserva generarDepuracion(Long idConfiguracion, boolean generacionManual);
	
	public void generarInformacionDepuracion();
	
	public List<DepuracionReservaDetalle> generarReservas(Long idConfiguracion, DepuracionReserva depuracion, boolean generacionManual);

	public List<DepuracionReservaDetalle> generarReservasSP(Long idConfiguracion, DepuracionReserva depuracion, boolean generacionManual);
	
	public void guardarConfiguracionDepuracion(ConfiguracionDepuracionReserva configuracion);
	
	public List<SeccionSiniestroDTO> obtenerCoberturasAsociadas(Long idConfiguracion);
	
	public List<SeccionSiniestroDTO> obtenerCoberturasDisponibles(Long idConfiguracion, BigDecimal idToSeccion);
	
	public List<CoberturaSeccionSiniestroDTO> obtenerCoberturasPorSeccion();
	
	public List<DepuracionReserva> obtenerHistoricoConfiguracion(Long idConfiguracion);
	
	public List<Oficina> obtenerOficinasRelacionadas(Long idDepuracion);
	
	public List<DepuracionReservaDetalle> obtenerReservas(Long idDepuracion);
	
	public List<DepuracionReservaDetalle> obtenerReservasOficina(Long idDepuracion, Long idOficina);
	
	public Map<String,String> obtenerTerminosAsociados(Long idConfiguracion);
	
	public Map<String,String> obtenerTerminosDisponibles(Long idConfiguracion);
	
	public ConfiguracionDepuracionReserva cargarConfiguracionDepuracion(Long idConfiguracion);
	
	public List<DepuracionReservaDetalle> obtenerReservasDepuradas(Long idDepuracion);
	
	public List<DepuracionReservaDetalle> obtenerReservasDepuradasById(String listaIdsDepurados);
	
	public MovimientoPosteriorAjusteDTO obtenerDetalleMovimientoPosterior(Long idMovimiento );
	
	public MovimientoPosteriorAjusteDTO transformaMovimiento (MovimientoCoberturaSiniestro movimiento );
	
	public List<MovimientoPosteriorAjusteDTO > buscarMovimientosPosteriores (MovimientoPosteriorAjusteDTO filtro);
	
	public List<ConfiguracionDepuracionReserva> obtenerConfiguracionesActivas();
	
	public DepuracionReserva obtenerDepuracion(Long idDepuracion);
	
	public List<DepuracionReservaDetalle> obtenerReservasExportacion(Long depuracionReservaId);
	
	public  List<DepuracionReservaDetalle> obtenerReservasExportacionSP(Long depuracionReservaId) ;
	
	public List<DepuracionReservaDetalle> exportarSoloDepuradosToExcel(String idsDepurados);
	
	public String getIdsDepuraDetailReservByIdDepuracionReserva (Long idDepuracionReserva, String cadenaIds);
	//changes for commit
	public List<DepuracionReservaDetalle> getListDepuratedReservsByIdDepuracionReserva (Long idDepuracionReserva, String cadenaIds);
	
	public void initialize();
	
}

