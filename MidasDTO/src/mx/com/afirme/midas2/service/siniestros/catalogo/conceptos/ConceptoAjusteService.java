package mx.com.afirme.midas2.service.siniestros.catalogo.conceptos;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import org.apache.commons.collections.map.MultiValueMap;

import mx.com.afirme.midas2.domain.siniestros.catalogo.conceptos.ConceptoAjuste;
import mx.com.afirme.midas2.domain.siniestros.catalogo.conceptos.ConceptoCobertura;
import mx.com.afirme.midas2.domain.siniestros.catalogo.conceptos.ConceptoTipoPrestador;
import mx.com.afirme.midas2.dto.siniestros.catalogos.conceptos.ConceptoCoberturaDTO;
import mx.com.afirme.midas2.dto.siniestros.catalogos.conceptos.SeccionDTOConcepto;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.ConceptoAjusteDTO;

@Local
public interface ConceptoAjusteService {
	public static final  short GASTO_AJUSTE = 2;
	public static final short AFECTACION_RESERVA=1;
	public static final short REEMBOLSO_GASTO_AJUSTE=3;
	
	 
	/**
	 * Este método crea la relación entre una cobertura y un concepto.  Debe recibir
	 * una lista de ids de coberturas, recorrerlas y por cada cobertura crear una
	 * nueva relación. Se debe hacer el borrado de las coberturas previamente
	 * asociadas antes de insertar las nuevas.
	 * 
	 * Por cada cobertura del listado se creará una entidad ConceptoCobertura para ser
	 * persistida.
	 * 
	 * @param concepto    concepto seleccionado al que serán asociadas las coberturas
	 * @param listadoCoberturas    lista de coberturas para ser asociadas
	 */
	public void asociarCoberturas(Long keyConcepto,Long keyLineaDeNegocio, List<String> listadoCoberturas);

	/**
	 * Se debe borrar las coberturas previamente asociadas antes de agregar las nuevas,
	 * las coberturas a borrar deben de estar asociadas a la misma sección
	 * seleccionada.
	 * 
	 * @param seccion    esta debe de ser la seccion seleccionada en el combo de la
	 * pantalla de catálogo de conceptos
	 * @param concepto    concepto seleccionado
	 */
	public void borrarCoberturas(ConceptoAjuste concepto,Long keyLineaDeNegocio);
	
	
	/**
	 * 
	 * @param conceptoAjuste
	 */
	public Long crearCobertura(ConceptoAjuste conceptoAjuste);
	
	public List<ConceptoAjuste> listarConceptos(short tipoConcepto);
	
	public List<ConceptoCoberturaDTO> obtenerCoberturasAsociadasSeccionAjuste(Long keyConceptoAjuste,Long keyLineaDeNegocio);
	
	public MultiValueMap obtenerCobertuasAsociadas(Long keyConceptoAjuste,Long keyLineaDeNegocio);
	
	public List<ConceptoCobertura> buscarConceptoAjusteByCobertura(Long keySeccion, Long keyConertura);

	public String[] obtenerDatoLineaNegocio(String nombreLineaNegocio);
	
	/**
	 * Este método crea la relación entre un tipo prestador y un concepto.  Debe recibir
	 * una lista de ids de tipo prestador, recorrerlas y por cada tipoPrestador crear una
	 * nueva relación. Se debe hacer el borrado de las coberturas previamente
	 * asociadas antes de insertar las nuevas.
	 * 
	 * Por cada cobertura del listado se creará una entidad ConceptoCobertura para ser
	 * persistida.
	 * 
	 * @param concepto    concepto seleccionado al que serán asociadas las coberturas
	 * @param listadoCoberturas    lista de coberturas para ser asociadas
	 */
	public void asociarTipoPrestador(Long keyConcepto,List<Long> listadoCoberturas);
	
	
	public List<ConceptoTipoPrestador> obtenerTipoPrestadorAsociados(Long keyConceptoAjuste);
	
	public void borrarTipoPrestador(ConceptoAjuste concepto) ;
	
	public List<ConceptoAjusteDTO> obtenerCoberturasPorConcepto(Long keyConcepto);
	
	public Map<Long,String> getListarSeccionesVigentesAutosUsablesSiniestros();

}
