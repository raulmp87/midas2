package mx.com.afirme.midas2.service.siniestros.valuacion.hgs;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.valuacion.hgs.ValuacionHgs;
import mx.com.afirme.midas2.domain.siniestros.valuacion.hgs.ValuacionHgsDetalle;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.dto.siniestros.hgs.ConsultaHgsDto;
import mx.com.afirme.midas2.dto.siniestros.hgs.Efile;
import mx.com.afirme.midas2.dto.siniestros.hgs.EfileDevolucion;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ValuacionHGSEditDTO;
import mx.com.afirme.midas2.service.sistema.comm.CommService;

@SuppressWarnings("rawtypes")
@Local
public interface HgsService extends CommService{
	
	public static final String USUARIO_HGS = "SINUSHGS";
	
	/**
	 * Inserta una valuación recibida de HGS en Midas. Se deberán tomar en cuenta las
	 * siguientes reglas y estatus para el procesamiento de la información:
	 * 
	 * @param valuationDTO
	 */
	public String insertarValuacion(Efile efile);

	/**
	 * Método para enviar la suma asegurada del reporte de siniestro. Se deberá
	 * registrar el envío invocando el método registraEnvio.
	 * 
	 * @param idReporteSin
	 */
	public ConsultaHgsDto obtenerSumaAseguradaHGS(Long idReporteSin);
	
	/***
	 * Procesa la información enviada por HGS con los datos del reporte y las refacciones
	 * @param idReporteSin
	 * @param idTercero
	 * @return
	 */
	public void insertarReporte(Long idReporteSin);
	
	/**
	 * Buscar por findByProperties ValuacionHgs.paseatencionId
	 * */
	public List<ValuacionHgs> consultaValuacion(Long keyPaseAtencion);
	
	/**
	 * Insertar valuacion por estimacion
	 * @param keyEstimacion - ID llave de TOESTIMACIONCOBERTURAREPCAB 
	 */
	public String insertarReportePaseAtencion(Long keyEstimacion);
	
	/**
	 * Validar reenvio pase de atencion - busca que el folio tengo error y regresará true para el reenvio del pase
	 * @param folio - folio del pase de atencion
	 */
	public boolean isReenvioPase(String folio);
	
	/***
	 * Valida si hay pases de atención pendientes por enviar
	 * @param keyReporteCabina - ID del reporte cabina
	 */
	public boolean isPendientesHgs(Long keyReporteCabina);
	
	/**
	 * Valida si el pase de atencion fue enviado con exito
	 * @param folio
	 */
	public boolean isExitosoHgs(String folio);
	
	/**
	 * Detalle de devolución de piezas
	 */
	public String generarDevolucionPiezas(EfileDevolucion efileDevolucion);
	
	
	/**
	 * Detalle de devolución de piezas
	 */
	public void notificaEstatusOrdenCompra(Long keyLiquidacion);
	
	/***
	 * Obtener datos de vehiculo por medio del pase de atencion
	 * @param ReporteCabina reporteCabina
	 * @param keyIdTercero
	 */
	public Map<String,Object> obtenerDatosVehiculo ( ReporteCabina reporteCabina, Long keyIdTercero );
	
	public boolean validarEstatusOrdenCompra(OrdenCompra ordenCompra);
	
	public void procesarValuacion(Long keyValuacion,ValuacionHgsDetalle valuacionHgsDetalle, short tipoExpediente);
	
	public void envioReporte(ReporteCabina reporteCabina,EstimacionCoberturaReporteCabina lCor);
	//joksrc * * * * * * *
	public ReporteCabina obtenerReportePorEstimacionId (Long idEstimacionCobertura);
	
	public List<ValuacionHgs> guardarValuacionExistenteHgsDto (Long idEstimacionCoberturaReporteCabina, ValuacionHGSEditDTO informacionValuacion);
	
	public List<ValuacionHgs> guardarValuacionExistenteHgs (Long idEstimacionCoberturaReporteCabina, Short tipoProceso, Short estatusValuacion, 
			Long idValuacionHGS, Date fechaIngresoTaller,Date fechaTerminacion, String idValuador, Date fechaReingresoReparacion, 
			Date ultimoSurtidoRefacciones, String nombreValuador, BigDecimal montoRefacciones, BigDecimal montoManoObra, 
			BigDecimal montoTotalReparacion);
	
	public List<ValuacionHgs> guardarValuacionNuevaManualHgsDto (Long idEstimacionCoberturaReporteCabina, ValuacionHGSEditDTO informacionValuacion);
	
	public List<ValuacionHgs> guardarValuacionNuevaHgs (Long idEstimacionCoberturaReporteCabina, Short tipoProceso, Short estatusValuacion, Long idValuacionHGS, 
			Date fechaIngresoTaller,Date fechaTerminacion, String idValuador, Date fechaReingresoReparacion, Date ultimoSurtidoRefacciones, 
			String nombreValuador, BigDecimal montoRefacciones, BigDecimal montoManoObra, BigDecimal montoTotalReparacion);
	
	public List<String> obtenerValuadoresByNombreOrId(Long idValuador, String nombreValuador);
	
	public List<Map<String, Object>> obtenerListaValuadores(Long idValuador, String nombreValuador);
}
