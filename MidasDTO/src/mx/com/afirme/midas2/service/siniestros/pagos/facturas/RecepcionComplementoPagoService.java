package mx.com.afirme.midas2.service.siniestros.pagos.facturas;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.persistence.PersistenceException;

import org.apache.xmlbeans.XmlException;

import mx.com.afirme.midas.siniestro.finanzas.pagos.FacturaDTO;
import mx.com.afirme.midas.siniestro.finanzas.pagos.FacturaPteComplMonitor;
import mx.com.afirme.midas2.domain.envioxml.EnvioFactura;
import mx.com.afirme.midas2.domain.envioxml.EnvioFacturaDet;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.EnvioValidacionFactura;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.MensajeValidacionFactura;
import mx.com.afirme.midas2.dto.siniestros.pagos.facturas.OrdenCompraProveedorDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.facturas.RelacionFacturaOrdenCompraDTO;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos.Retenciones.Retencion;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos.Traslados.Traslado;
@Local
public interface RecepcionComplementoPagoService {

	public List<EnvioFactura> procesarFacturasZip(Integer proveedorId, File zipFile, String extensionArchivo,String origenEnvio,Long idLote) throws Exception;
	public List<EnvioFactura> procesaXMLComplemento(ByteArrayOutputStream bos,Integer idProveedor)throws Exception;
	public Long obtenerIdLoteCarga() throws Exception;
	public Boolean esValidoRFCPorAgente(String rfc,Integer agenteId)throws Exception;
	public List<FacturaPteComplMonitor> obtenerFacturasPendientesComplementoMonitor(Date fechaPagoInicio, Date fechaPagoFin, String origenEnvio) throws Exception;
	public void notificaRecepcionComplementoPago(String origenEnvio) throws Exception;
}
