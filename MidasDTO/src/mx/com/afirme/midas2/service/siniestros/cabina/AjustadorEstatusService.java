package mx.com.afirme.midas2.service.siniestros.cabina;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation;
import mx.com.afirme.midas2.domain.Coordenadas;
import mx.com.afirme.midas2.domain.siniestros.cabina.AjustadorEstatus;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar.LugarSiniestroMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar.LugarSiniestroMidas.TipoLugar;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.ajustador.HorarioAjustador.TipoDisponibilidad;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.AjustadorEstatusDTO;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService.CatalogoFiltro;
import mx.com.afirme.midas2.util.StringUtil;

import org.directwebremoting.annotations.DataTransferObject;

@Local
public interface AjustadorEstatusService {
	
	
	/**
	 * Guarda un AjustadorEstatus
	 * @param ajustadorEstatus
	 * @return
	 */
	public Long guardarAjustadorEstatus(AjustadorEstatus ajustadorEstatus);
	
	/**
	 * Guardar coordenadas para un ajustador con identificador de Seycos. 
	 * Se mantiene la última coordenada, actualizando el registro en caso de existir.
	 * @param idAjustadorSeycos ID del ajustador en Seycos
	 * @param latitud
	 * @param longitud
	 * @return
	 */
	public void guardarAjustadorEstatusSeycos(Long idAjustadorSeycos, Double latitud, Double longitud);
	
	public void guardarAjustadorEstatusMovil(Long idAjustadorMovil, Double latitud, Double longitud, Boolean midas);
	
	/**
	 * Guarda un AjustadorEstatus. Si ya existe un AjustadorEstatus con esta ID de ajustador
	 * actualiza la informacion de la base de datos
	 * @param idAjustador
	 * @param latitud
	 * @param longitud
	 * @return
	 */
	public Long guardarAjustadorEstatus(Long idAjustador, Double latitud, Double longitud);
	
	
	/**
	 * Busca la lista de AjustadorEstatus para una oficina determinada  
	 * @param oficina
	 * @return
	 */
	public List<AjustadorEstatus> obtenerListaAjustadorEstatus(Long idOficina);
	
	
	/**
	 * Busca la lista de AjustadorEstatus para una oficina determinada  
	 * @param oficina
	 * @return
	 */
	public List<AjustadorEstatusDTO> obtenerListaAjustadorEstatusDTO(Long idOficina);
	
	
	/**
	 * Busca los ajustadores de una oficina y les asigna si se debe de calcular su ruta
	 * @return
	 */
	
	public List<AjustadorEstatusDTO> obtenerAjustadoresParaCalcularRuta(Long idReporte, Long idOficina);

	/**
	 * Genera un String con la direccion del parametro
	 * @param lugar
	 * @return
	 */
	public String obtenerStringDireccion(LugarSiniestroMidas lugar);
	
	/**
	 * Obtener informacion ajustador asignado al reporte que haya hecho contacto con el lugar de atencion (haya registrado sus coordenadas de contacto)
	 * @param reporteId
	 * @param tipoLugar
	 * @return
	 */
	public AjustadorEstatusDTO obtenerAjustadorContactoMovil(Long reporteId, TipoLugar tipoLugar);
	
	public class AjustadorEstatusFiltro extends CatalogoFiltro {
		
		private Long idAjustador;
		private Long estatusAjustador;
		/**
		 * @return the idAjustador
		 */
		@FilterPersistenceAnnotation(persistenceName="ID")
		public Long getIdAjustador() {
			return idAjustador;
		}
		/**
		 * @param idAjustador the idAjustador to set
		 */
		public void setIdAjustador(Long idAjustador) {
			this.idAjustador = idAjustador;
		}
		/**
		 * @return the estatusAjustador
		 */
		@FilterPersistenceAnnotation(persistenceName="ESTATUS")
		public Long getEstatusAjustador() {
			return estatusAjustador;
		}
		/**
		 * @param estatusAjustador the estatusAjustador to set
		 */
		public void setEstatusAjustador(Long estatusAjustador) {
			this.estatusAjustador = estatusAjustador;
		}
	}
	
	/**
	 * 
	 * @param listaAjustadores
	 * @return
	 */
	public List<AjustadorEstatus> obtenerDisponibilidadAjustadorEstatus( Long idOficina , TipoDisponibilidad disponibilidad );
	
	/**
	 * Obtiene los reportes asignados a un ajustador
	 * @param idAjustador
	 * @return
	 */
	//public List<ReporteCabina> obtenerReportesAsignados( Long idAjustador );
	
	/**
	 * Obtiene una lista de ajustadores filtrada por disponibilidad.
	 * @param idOficina
	 * @param disponibilidad
	 * @return
	 */
	public List<ServicioSiniestro> obtenerDisponibilidadAjustador(Long idOficina , TipoDisponibilidad disponibilidad );
	
	/**
	 * Retornar un listado de ajustadores estatus (coordenadas) por oficina. 
	 * El método retornará la lista de las últimas coordenadas reportada por un ajustador activo para la oficina selccionada. 
	 * @param idOficina id de la oficina selccionada
	 * @return
	 */
	public List<AjustadorEstatusDTO> obtenerAjustadoresPorOficina(Long idOficina);
	
	/**
	 * Retornar historico de ubicaciones por ajustador. 
	 * Regresa en orden descendente (fecha creacion) las distintas ubicaciones 
	 * que ha tenido un ajustador desde una fecha dada.
	 * @param idAjustador
	 * @param desde. En caso de ser nula se retornara todo lo del día
	 * @return
	 */	
	public List<AjustadorEstatusDTO> obtenerHistoricoPorAjustador(Long idAjustador, Date desde);
	
	/**
	 * Obtener un listado de reportes que el ajustador tiene asignados. 
	 * Si <code>reporteId</code> es diferente de <code>null</code> se excluira de la busqueda 
	 * @param ajustadorId
	 * @param reporteId
	 * @return
	 */	
	public List<ReporteEstatusDTO> obtenerResumenReportesAsignados(Long idAjustador, Long reporteId); 
			
	/**
	 * Obtener un listado de reportes por oficina que no tengan fecha de cierre/termino
	 * @param idOficina
	 * @return List<ReporteEstatusDTO> Listado informativo de los reportes
	 */
	public List<ReporteEstatusDTO> obtenerResumenReportes(Long idOficina);
	
	/**
	 * Obtener la informacion general de un reporte de cabina
	 * @param reporteId
	 * @return
	 */
	public ReporteEstatusDTO obtenerInformacionReporte(Long reporteId);
	
	
	/**
	 * Eliminar historico de ajustadores de la fecha dada hacia atras. Su invocacion se guarda en la bitacora de sistema.
	 * @param date fecha limite superior del borrado (hasta)
	 * @return mensajeDTO conteniendo informacion acerca de la eliminacion.
	 */
	public MensajeDTO eliminarHistoricoAjustadores(Date date);
	
	public AjustadorEstatus crearAjustadorEstatus(ServicioSiniestro ajustador, Date date);
	
	@DataTransferObject
	public class ReporteEstatusDTO implements Serializable{
		
		private static final long serialVersionUID = 5740365209833062462L;
		
		private Long reporteId;
		private String numeroReporte;
		private Date fechaOcurrido;
		private Date fechaAsignacion;
		private Date fechaContacto;
		private Date fechaTermino;
		private Date fechaReporte;
		private String cliente;
		private String numPoliza;
		private String asignadoA;
		private Coordenadas coordenadas;
		
		
		private static final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
		public ReporteEstatusDTO() {
			super();
		}
	
		public ReporteEstatusDTO(Long reporteId, String numeroReporte,
				Date fechaOcurrido, Date fechaAsignacion, Date fechaContacto,
				Date fechaTermino, Date fechaReporte, String cliente,
				String numPoliza, Coordenadas coordenadas, String asignadoA) {
			this();
			this.reporteId = reporteId;
			this.numeroReporte = numeroReporte;
			this.fechaOcurrido = fechaOcurrido;
			this.fechaAsignacion = fechaAsignacion;
			this.fechaContacto = fechaContacto;
			this.fechaTermino = fechaTermino;
			this.fechaReporte = fechaReporte;
			this.cliente = cliente;
			this.numPoliza = numPoliza;
			this.coordenadas = coordenadas;
			this.asignadoA = asignadoA;
		}
		
		public ReporteEstatusDTO(ReporteCabina reporte){
			this(reporte.getId(), reporte.getClaveOficina(), reporte.getConsecutivoReporte(), reporte.getAnioReporte(), 
					reporte.getFechaOcurrido(), reporte.getFechaHoraAsignacion(), reporte.getFechaHoraContacto(), 
					reporte.getFechaHoraTerminacion(), reporte.getFechaHoraReporte(), reporte.getPersonaReporta(), 
					reporte.getPoliza(), reporte.getLugarAtencion(), reporte.getAjustador());			
		}
		
		public ReporteEstatusDTO(Long reporteId, String claveOficina, String consecutivoReporte, String anioReporte,
				Date fechaOcurrido, Date fechaAsignacion, Date fechaContacto, Date fechaTermino, Date fechaReporte, 
				String nombreReporta, PolizaDTO poliza, LugarSiniestroMidas lugarAtencion, ServicioSiniestro ajustador){
			this(reporteId, claveOficina.concat("-").concat(consecutivoReporte).concat("-").concat(anioReporte), 
					fechaOcurrido, fechaAsignacion, fechaContacto, fechaTermino, fechaReporte, 
					nombreReporta, poliza != null ? poliza.getNumeroPolizaFormateada() : "NA", 
							lugarAtencion != null ? lugarAtencion.getCoordenadas() : null,
									ajustador != null? ajustador.getNombrePersonaAjustador() : null);					
		}



		public String getEstatus(){
			if(fechaAsignacion == null){
				return "Sin asignación";
			}else if(fechaContacto == null){
				return "En Tránsito";
			}else if(fechaTermino == null){
				return "Contacto";
			}else{
				return "Término";
			}			
		}
		
		public Long getReporteId() {
			return reporteId;
		}
		public void setReporteId(Long reporteId) {
			this.reporteId = reporteId;
		}
		public String getNumeroReporte() {
			return numeroReporte;
		}
		public void setNumeroReporte(String numeroReporte) {
			this.numeroReporte = numeroReporte;
		}
		public Date getFechaOcurrido() {
			return fechaOcurrido;
		}
		public void setFechaOcurrido(Date fechaOcurrido) {
			this.fechaOcurrido = fechaOcurrido;
		}
		public Date getFechaAsignacion() {
			return fechaAsignacion;
		}
		public void setFechaAsignacion(Date fechaAsignacion) {
			this.fechaAsignacion = fechaAsignacion;
		}
		public Date getFechaContacto() {
			return fechaContacto;
		}
		public void setFechaContacto(Date fechaContacto) {
			this.fechaContacto = fechaContacto;
		}
		public String getCliente() {
			return cliente;
		}
		public void setCliente(String cliente) {
			this.cliente = cliente;
		}
		public String getNumPoliza() {
			return numPoliza;
		}
		public void setNumPoliza(String numPoliza) {
			this.numPoliza = numPoliza;
		}
		public Date getFechaTermino() {
			return fechaTermino;
		}
		public void setFechaTermino(Date fechaTermino) {
			this.fechaTermino = fechaTermino;
		}
		public Date getFechaReporte() {
			return fechaReporte;
		}
		public void setFechaReporte(Date fechaReporte) {
			this.fechaReporte = fechaReporte;
		}
		public String getFechaOcurridoStr(){
			return fechaOcurrido != null ? dateFormat.format(fechaOcurrido) : "NA";
		}
		public String getFechaAsignacionStr(){
			return fechaAsignacion != null ? dateFormat.format(fechaAsignacion) : "NA";
		}
		public String getFechaContactoStr(){
			return fechaContacto != null ? dateFormat.format(fechaContacto) : "NA";
		}
		public String getFechaTerminoStr(){
			return fechaTermino != null ? dateFormat.format(fechaTermino) : "NA";
		}
		public String getFechaReporteStr(){
			return fechaReporte != null ? dateFormat.format(fechaReporte) : "NA";
		}
		public Coordenadas getCoordenadas() {
			return coordenadas;
		}
		public void setCoordenadas(Coordenadas coordenadas) {
			this.coordenadas = coordenadas;
		}
		public String getAsignadoA() {
			return !StringUtil.isEmpty(asignadoA) ? asignadoA : "Sin ajustador";
		}
		public void setAsignadoA(String asignadoA) {
			this.asignadoA = asignadoA;
		}				
	}
}
