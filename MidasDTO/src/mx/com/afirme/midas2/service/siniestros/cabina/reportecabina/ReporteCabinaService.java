package mx.com.afirme.midas2.service.siniestros.cabina.reportecabina;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.Coordenadas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.BitacoraEventoSiniestro.TipoEvento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.cita.CitaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar.LugarSiniestroMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar.LugarSiniestroMidas.TipoLugar;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.BitacoraEventoSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.ComplementoReporteDTO;
import mx.com.afirme.midas2.dto.siniestros.CreacionReporteDTO;
import mx.com.afirme.midas2.dto.siniestros.DatosReporteSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.ReporteSiniestroRoboDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.ReporteSiniestroDTO;

import org.springframework.stereotype.Component;

@Local
public interface ReporteCabinaService {
	
	@Component
	public class FechaHoraDTO{
		
		private String descripcion;
		
		private String fecha;
		
		private String hora;

		public String getDescripcion() {
			return descripcion;
		}

		public void setDescripcion(String descripcion) {
			this.descripcion = descripcion;
		}

		public String getFecha() {
			return fecha;
		}

		public void setFecha(String fecha) {
			this.fecha = fecha;
		}

		public String getHora() {
			return hora;
		}

		public void setHora(String hora) {
			this.hora = hora;
		}
		
	}
	
	public enum TIPO_FECHA_HORA{REPORTE, ASIGNACION, CONTACTO, TERMINACION, OCURRIDO, ENTERADO};
	
	/**
	 * @param numReporte
	 * @return ReporteCabina
	 */
	public ReporteCabina buscarReporte(Long idReporte);
	
	/**
	 * 
	 * @param numReporte
	 * @return
	 */
	public ReporteCabina buscarReporte(String  nsOficina, String nsConsecutivoR, String nsAnio );
	
	
	public ReporteCabina buscarReportePorSiniestro(String nsOficina, String nsConsecutivoR,String nsAnio);
	/**
	 * @param reporteCabina
	 */
	public void salvarCita(CitaReporteCabina cita, Long numReporte);
	
	/**
	 * Obtiene el lugar de atendido u ocurrido para un reporte en particular
	 * @param idReporte
	 * @param tipoLugar
	 * @return
	 */
	public LugarSiniestroMidas mostrarLugarSiniestro(Long idReporte, TipoLugar tipoLugar);
	
	/**
	 * Salva la informacion minima del reporte junto con el lugar de atendido y devuelve el id reporte generado
	 * @param idReporte
	 * @param nombreReporta
	 * @param lada
	 * @param telefono
	 * @param luar
	 * @return
	 */
	public Long guardarLugarSiniestro(Long idReporte, String nombreReporta, String lada, String telefono, LugarSiniestroMidas lugar, TipoLugar tipo);
	
	/**
	 * Actualiza un lugar de atendido u ocurrido en un reporte dado
	 * @param idReporte
	 * @param lugar
	 */
	public void guardarLugarSiniestro(Long idReporte, LugarSiniestroMidas lugar, TipoLugar tipo);

	
	/**
	 * @param reporteCabina
	 */
	public void salvarReporte(ReporteCabina reporteCabina);
	
	/**
	 * 
	 * @param informacionAdicional
	 */
	public void salvarInformacionAdicional( Long idReporte, String informacionAdicional );
	
	/**
	 * 
	 * @param declaracionSiniestro
	 */
	public void salvarDeclaracionSiniestro( Long idReporte, String declaracionSiniestro );
	
	/**
	 * Contar reportar de la busqueda - paginacion
	 * @param reporteSiniestroDTO
	 * @return
	 */
	public Long contarReporteSiniestro(ReporteSiniestroDTO reporteSiniestroDTO);
	
	/**
	 * 
	 * @param reporteSiniestroDTO
	 * @return
	 */
	public List<ReporteSiniestroDTO> buscarReportes(ReporteSiniestroDTO reporteSiniestroDTO);
	/**
	 * Obtiene Reportes de Siniestrosa de cobertura tipo Robo RT con estimacionCobertura
	 * @param ReporteSiniestroRoboDTO
	 */
	public List<ReporteSiniestroRoboDTO> buscarReportesRobo(ReporteSiniestroRoboDTO reporteSiniestroDTO);	
	
	
	/**
	 * 
	 * @return
	 */
	public String obtenerConsecutivoReporte(ReporteCabina reporteCabina);
	
	/**
	 * 
	 * @return
	 */
	public String construirNumeroReporte( ReporteCabina reporteCabina );
	
	/**
	 * 
	 * @param reporteCabina
	 * @return
	 */
	public Oficina obtenerOficinaReporte(ReporteCabina reporteCabina);
	
	/**
	 * @param bitacoraEventoSiniestroDTO
	 */
	public List<BitacoraEventoSiniestroDTO> buscarEventos(BitacoraEventoSiniestroDTO bitacoraEventoSiniestroDTO, Long idReporte);
	/**
	 * @param bitacoraEventoSiniestro
	 */
	public void guardarEvento(Long idReporte, TipoEvento tipoEvento, String descripcion );
	

	public Long obtenerIncisoReporteCabina(Long id);
	
	/**
	 * Método que regresa los datos generales de un reporte de cabina. Estos datos se utilizan en muchas pantallas.
	 * @param idReporteCabina
	 * @return
	 */
	public DatosReporteSiniestroDTO	obtenerDatosReporte  (Long idReporteCabina );
	
	/**
	 * Método que regresa un objeto de transporte con los datos del reporte.
	 * @param idReporteCabina
	 * @return
	 */
	public TransporteImpresionDTO imprimirReporteCabina( Long idReporteCabina );
	
	
	public boolean validarGuardadoFechaOcurrido(ReporteCabina reporteCabina);
	
	/**
	 * Genera fecha y hora para el reporte dependiendo del tipo seleccionado
	 * @param idReporteCabina
	 * @param tipo
	 * @return
	 */
	public FechaHoraDTO generarFechaHora(Long idReporteCabina, TIPO_FECHA_HORA tipo, String isEditable, Date fecha);
	
	/**
	 * Validar existencia de la fecha y hora en el reporte dependiendo del tipo
	 * @param idReporteCabina
	 * @param tipo
	 * @return
	 */
	public FechaHoraDTO validarExistenciaFechaHora(Long idReporteCabina, TIPO_FECHA_HORA tipo);
	
	
	/**
	 * Validar si el usuario actual tiene permitido editar un reporte convertido a siniestro debido a su rol
	 * @param reporte
	 * @return
	 */
	public Boolean noEsEditableParaElRol(ReporteCabina reporte);
	
		
	public CreacionReporteDTO generarReporte(Long continuityId, String oficinaId, String personaReporte, Date fechaOcurrido, String horaOcurrido,
			String lada, String telefono, String personaConductor, String usuario );
	
	public void depurarReservaReporte(Long reporteId);
	
	/***
	 * Por medio del ID de reporte en el objeto agrega el inciso al reporte y datos del ajustador
	 * Servcio expuesto EN UN ES
	 * @param complementoReporteDto
	 * @return
	 */
	public String sincronizarReporteWS(ComplementoReporteDTO complementoReporteDto);
	
	
	/**
	 * Registrar en bitacora de reporte el contacto con quien reporta el siniestro por parte del ajustador
	 * @param reporteId id del reporte
	 * @param fecha fecha hora de contacto
	 * @param ajustadorId id del ajustador
	 * @param tipo TIPO_FECHA_HORA que esta reportando el ajustador - ENTERADO, CONTACTO
	 */
	public void registrarEventoAjustador(Long reporteId, Date fecha, Long ajustadorId, TIPO_FECHA_HORA tipo);
	
	
	/**
	 * Registrar en bitacora de reporte el contacto con quien reporta el siniestro por parte del ajustador
	 * @param reporteId id del reporte
	 * @param fecha fecha hora de contacto
	 * @param ajustadorId id del ajustador
	 * @param tipo TIPO_FECHA_HORA que esta reportando el ajustador - ENTERADO, CONTACTO
	 */
	public void registrarEventoAjustador(Long reporteId, Date fecha, Long ajustadorId, TIPO_FECHA_HORA tipo, Coordenadas coordenadas);
	
	
	/**
	 * Registrar en bitacora de reporte el contacto con quien reporta el siniestro por parte del ajustador
	 * @param reporteId id del reporte
	 * @param fecha fecha hora de contacto
	 * @param ajustadorId id del ajustador
	 */
	@Deprecated
	public void registrarContacto(Long reporteId, Date fecha, Long ajustadorId);
	
	/**
	 * Registrar en bitacora de reporte el contacto con quien reporta el siniestro por parte del ajustador y su posición al momento del contacto
	 * @param reporteId id del reporte
	 * @param fecha fecha hora de contacto
	 * @param ajustadorId id del ajustador
	 * @param coordendas parametro que contiene latitud y longitud 
	 */
	@Deprecated
	public void registrarContacto(Long reporteId, Date fecha, Long ajustadorId, Coordenadas coordenadas);
	
	/**
	 * Regresa una lista de reportecabina que está en atención por un ajustador pero que aun
	 * no tiene hora de contacto, es decir el ajustador aun no llega al lugar del siniestro
	 * @param idAjustador
	 * @return
	 */
	List<ReporteCabina> buscarReportesCabinaSinContactoPorAjustador(Long idAjustador);

	public String buscarNombreAseguradoAutos(BigDecimal idToPoliza, Integer numeroInciso, Date fechaValidez);
}
