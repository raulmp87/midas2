package mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.DetalleOrdenCompra;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.facturas.OrdenCompraProveedorDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.OrdenCompraRecuperacionDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.BandejaOrdenCompraDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.DetalleOrdenCompraDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.ImportesOrdenCompraDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.OrdenCompraDTO;
/**
 * interface que define la l�gica para el m�dulo de �rdenes de compra
 * @author usuario
 * @version 1.0
 * @created 22-ago-2014 11:37:53 a.m.
 */
@Local
public interface OrdenCompraService {
	
	public static final String ORIGENHGS_PERDIDATOTAL="HGS_IN_PT";
	public static final String ORIGENHGS_AUTOMATICO="HGS_IN_AUT";
	public static final String ORIGENMANUAL="MANUAL";
	public static final String ORIGENHGS_DAÑOS="HGS_IN_DA";
	public static final String ORIGEN_INDEMNIZACION_AUTOMATICA="AUT_IND";
	public static final String ORIGEN_MIGRACION="MIGRA";
	public static final String ORIGENSIPAC_AUTOMATICO="SIPAC";
	
	public static final String ESTATUS_TRAMITE		="Tramite";
	public static final String ESTATUS_AUTORIZADA	="Autorizada";
	public static final String ESTATUS_RECHAZADA	="Rechazada";
	public static final String ESTATUS_CANCELADA	="Cancelado";
	public static final String ESTATUS_PAGADA	="Pagado";	
	public static final String ESTATUS_ASOCIADA	="Asociada";

	public static final String ESTATUS_INDEMNIZACION	="Indemnizacion";
	public static final String ESTATUS_INDEMNIZACION_FINALIZADA	="Indemnizacion_Finalizada";
	public static final String ESTATUS_INDEMNIZACION_CANCELADA	="Indemnizacion_Cancelada";

	
	public static final Integer PRESTADOR_ACTIVO= 1;
	public static final String CVENULA="0";
	public static final short ESTATUSACTIVO=1;
	public static final String SEPARADOR="|";
	
	public static final String TIPO_AFECTACION_RESERVA="OC";
	public static final String TIPO_GASTOAJUSTE="GA";
	public static final String TIPO_REMBOLSO_GASTOAJUSTE="RGA";
	
	public static final String  PAGO_A_BENEFICIARIO="PB";
	public static final String  PAGO_A_PROVEEDOR="PP";
	
	public static final String TIPO_PROVEEDOR_CIA="CIA";
	
	public static final String TIPO_INDEMNIZACION_ROBO="RB";
	public static final String TIPO_INDEMNIZACION_PERDIDATOTAL="PT";
	public static final String TIPO_INDEMNIZACION_PAGO_DANIOS="DA";
	public static final String TIPO_SIPAC="SIP";
	
	public BigDecimal obtenerTotales(Long idEstimacionCobRepCab, Long idReporteCabina, Long idcoberturaReporteCabina, String tipoOrdenCompra);
		
	
	
	/**
	 * recibe un idReporteCabina y busca las �rdenes de compra ligadas al reporte y
	 * devuelve una lista
	 * 
	 * @param idReporteCabina    identificador del reporte de cabina
	 */
	public List<OrdenCompraDTO> obtenerOrdenesCompraSiniestro(Long idReporteCabina, String tipo);
	
	
	/**
	 * busca la lista de conceptos o detalles de orden relacionados
	 * 
	 * @param idOrdenCompra
	 */
	public List <DetalleOrdenCompra> obtenerDetallesOrdenCompra(Long idOrdenCompra, Long idOrdenCompraDetalle);
	

	public String validarCrearOrdenCompra (OrdenCompra ordenCompra);
	
	
	/**
	 * busca la lista de conceptos o detalles de orden relacionados
	 * 
	 * @param idOrdenCompra
	 */
	public ImportesOrdenCompraDTO calcularImportes(Long idOrdenCompra, Long idOrdenCompraDetalle, boolean calcularPorConcepto);
	

	
	
	
	public boolean validaImportesOrdenCompraContraConceptos (Long idOrdenCompra, Long idOrdenCompraDetalle, BigDecimal variacion) throws Exception;

	public List <DetalleOrdenCompra> obtenerDetallesOrdenCompraPorConcepto(Long idOrdenCompra, Long idOrdenCompraDetalle);
	
	/**
	 * busca la lista de conceptos o detalles de orden relacionados
	 * 
	 * @param idOrdenCompra
	 */
	public List <DetalleOrdenCompraDTO> obtenerListaDetallesOrdenDTO(Long idOrdenCompra, Long idOrdenCompraDetalle, boolean calcularPorConcepto);


	/**
	 * consulta una orden de compra en espec�fico
	 * 
	 * @param idOrdenCompra
	 */
	public OrdenCompra obtenerOrdenCompra(Long idOrdenCompra);
	
	/**
	 * Valida la relacion de la orden de compra con la recuperacion de deducible
	 * @param ordenCompra
	 * @return true en caso de que deba mostrar el deducible para la orden de compra 
	 * (en caso de que no haya deducible recuperado o sea la orden de compra con la que se recupero el deducible)
	 * false en caso de que no se deba cargar el monto del deducible en la orden de compra.
	 * (en caso de que ya haya sido recuperado previamente el deducible para la estimacion) 
	 */
	public boolean deducibleYaRecuperado(OrdenCompra ordenCompra);
	

	/**
	 * elimina un concepto asociado a una orden de compra
	 * 
	 * @param idDetalleOrdenCompra
	 */
	public void eliminarConcepto(Long idDetalleOrdenCompra);

	/**
	 * crea un nuevo registro detalle orden de compra, ligado a una orden de compra.
	 * 
	 * @param detalleOrdenCompra
	 * @param idOrdenCompr
	 */
	public void guardarConcepto(DetalleOrdenCompra detalleOrdenCompra, Long idOrdenCompr);

	/**
	 * guarda los datos generales de una orden de compra
	 * 
	 * @param ordenCompra
	 */
	public Long guardarOrdenCompra(OrdenCompra ordenCompra);


	/**
	 * Devuelve un DTO con los datos para la impresion de la orden de compra
	 * 
	 * @param idOrdenCompra
	 */
	public TransporteImpresionDTO imprimirOrdenCompra(Long idOrdenCompra);

		public OrdenCompra generarOrdenCompraIndemnizacion(
			Long idEstimacionCoberturaReporteCabina, String tipoIndemnizacion, String beneficiario)
			throws Exception;
	
	/**Reinicia la orden de compra origen de una OC de tipo indemnizacion*/
	public void reiniciaIndemnizacion (Long idOrdenCompra);
	
	public boolean ordenCompraAplicaBeneficiario( Long idcoberturaReporteCabina,String cveSubTipoCalculo);

	
	
	public boolean validaImportesDetalleOrdenCompraContraConcepto(DetalleOrdenCompra detalleOrdenCompra, BigDecimal variacion) throws Exception;


	public List<OrdenCompraProveedorDTO> obtenerOrdenesCompraProveedor(Integer idProveedor);
	
	
	public void cancelarOrdenPago(Long idOrdenCompra) throws Exception;
	/*CoberturaCompuesta  es idcoberturareporte cabina | clave subcalculo  ej 118|RCB
	 * */
	public List<OrdenCompraRecuperacionDTO> obtenerOrdenCompraRecuperacionProveedor(Long idReporteCabina,String coberturaCompuesta , List<Long>   pasesAtencionId, String tipoOrdenCompra,Long idOrdenCompra);
	
	
	public List<OrdenCompraProveedorDTO> obtenerOrdenesCompraParaAgruparParaCompania(Integer idProveedor,Long oficinaSeleccionada, String tipoAgrupador);
	
	public OrdenCompraProveedorDTO convierteOrdenCompraAOrdenCompraProveedorDTO(OrdenCompra orden);
	
	
	public String validarAplicaDeducible(Long idEstimacion, Long idOrdenCompra ) ;
	
	
	public List<BandejaOrdenCompraDTO> buscarOrdenesCompra(BandejaOrdenCompraDTO filtro ) ;
	
	public Long contarOrdenesCompraBandeja(BandejaOrdenCompraDTO filtro);
	
	public BigDecimal obtenerTotalesOrdenesCompra(Long reporteCabinaId, Long coberturaId, Long estimacionId, 
			String claveTipoCalculo, String claveSubCalculo, Date fechaCreacionUltimoMovimiento, String tipoOrdenCompra, String estatus);
	
	/**
	 * Actualiza orden de compra sipac en caso de que exista
	 * @param valorEstimado
	 * @param estimacion
	 * @return idOrdenCompra
	 */
	public Long actualizarSipac(BigDecimal valorEstimado,
			EstimacionCoberturaReporteCabina estimacion);
	
	/**
	 * Crea la orden de compra SIPAC
	 * @param importe
	 * @param estimacion
	 * @return OrdenCompra
	 */
	public void crearSipac(BigDecimal importe, 
			EstimacionCoberturaReporteCabina estimacion);
	

}