/**
 * 
 */
package mx.com.afirme.midas2.service.siniestros.catalogo.solicitudautorizacion;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation.OperationType;
import mx.com.afirme.midas2.domain.siniestros.catalogo.solicitudautorizacion.SolicitudAutorizacionAjusteAntiguedad;
import mx.com.afirme.midas2.dto.siniestros.catalogo.solicitudautorizacion.SolicitudAutorizacionDTO;
import mx.com.afirme.midas2.dto.siniestros.catalogos.solicitudautorizacion.SolicitudAutorizacionAntiguedadDTO;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService.CatalogoFiltro;

/**
 * @author admin
 *
 */
@Local
public interface CatSolicitudAutorizacionService {

	
	List<SolicitudAutorizacionDTO> buscar(SolicitudAutorizacionFiltro filtro);
	
	void  autorizacion(Long idSolicitudAutorizacion , Integer estatus , String tipoSolicitud );
	
	void save( SolicitudAutorizacionDTO solicitudAutorizacionDTO);
	
	void solicitarAutorizacionReserva(Long idEstimacionCobertura,BigDecimal estimacionNueva, String causaMovimiento );
	
	List<Rol> getRolUsuarioActual();
	
	Map<String,String> getEstatusMap();
	
	Map<Long,String> getOficinasMap();
	
	Map<String,String> getTipoSolicitudMap(); 
	
	public List<SolicitudAutorizacionAjusteAntiguedad> buscarAutorizacionesAntiguedad (SolicitudAutorizacionAntiguedadDTO filtro);
	
	public List<SolicitudAutorizacionAjusteAntiguedad> obtenerAutorizacionAntiguedadPendientes();
	
	public void autorizarSiniestroAntiguo(String solicitudes);
	
	public void rechazarSiniestroAntiguo(String solicitudes);
	
	public void solicitarAutorizacionReservaAntiguedad(Long idEstimacionCobertura,BigDecimal estimacionNueva,Long idParametroReserva,String causaMovimiento);
	
	public void  autorizaRechazaSolicitud(Long idSolicitudAutorizacion , Integer estatus , String tipoSolicitud, String comentario );
	
	
	 public class SolicitudAutorizacionFiltro extends CatalogoFiltro {
		 
		 private Long numeroSolicitud;
		 private String numeroPoliza;
		 private Date fechaSolicitudDe;
		 private Date fechaSolicitudA;
		 private String usuarioSolicitante;
		 private String usuarioAutorizador;
		 private Long oficina;
		 private BigDecimal numeroInciso;
		 private String tipoSolicitud;
		 private String noReporteCabina;
		 private String noFolio;
		 
		 
		 
         @FilterPersistenceAnnotation(persistenceName="solicitudAutorizacionVigencia.id")
		public Long getNumeroSolicitud() {
			return numeroSolicitud;
		}
		public void setNumeroSolicitud(Long numeroSolicitud) {
			this.numeroSolicitud = numeroSolicitud;
		}
		
		@FilterPersistenceAnnotation(persistenceName="solicitudAutorizacionVigencia.poliza.numeroPoliza")
		public String getNumeroPoliza() {
			return numeroPoliza;
		}
		public void setNumeroPoliza(String numeroPoliza) {
			this.numeroPoliza = numeroPoliza;
		}	
		
		@FilterPersistenceAnnotation(persistenceName="fechaCreacion", truncateDate=true , operation=OperationType.GREATERTHANEQUAL, paramKey="fechaCreacionDe")
		public Date getFechaSolicitudDe() {
			return fechaSolicitudDe;
		}
		
		public void setFechaSolicitudDe(Date fechaSolicitudDe) {
			this.fechaSolicitudDe = fechaSolicitudDe;
		}
		
		@FilterPersistenceAnnotation(persistenceName="fechaCreacion", truncateDate=true , operation=OperationType.LESSTHANEQUAL, paramKey="fechaCreacioA")
		public Date getFechaSolicitudA() {
			return fechaSolicitudA;
		}
		public void setFechaSolicitudA(Date fechaSolicitudA) {
			this.fechaSolicitudA = fechaSolicitudA;
		}
		
		@FilterPersistenceAnnotation(persistenceName="codigoUsuarioCreacion", operation=OperationType.LIKE)
		public String getUsuarioSolicitante() {
			return usuarioSolicitante;
		}
		public void setUsuarioSolicitante(String usuarioSolicitante) {
			this.usuarioSolicitante = usuarioSolicitante;
		}
		
		@FilterPersistenceAnnotation(persistenceName="codUsuarioAutorizador")
		public String getUsuarioAutorizador() {
			return usuarioAutorizador;
		}
		public void setUsuarioAutorizador(String usuarioAutorizador) {
			this.usuarioAutorizador = usuarioAutorizador;
		}
		
		@FilterPersistenceAnnotation(persistenceName="solicitudAutorizacionVigencia.oficina.id")
		public Long getOficina() {
			return oficina;
		}
		public void setOficina(Long oficina) {
			this.oficina = oficina;
		}
		
		@FilterPersistenceAnnotation(persistenceName="solicitudAutorizacionVigencia.numeroInciso")
		public BigDecimal getNumeroInciso() {
			return numeroInciso;
		}
		public void setNumeroInciso(BigDecimal numeroInciso) {
			this.numeroInciso = numeroInciso;
		}
		
		public String getTipoSolicitud() {
			return tipoSolicitud;
		}
		public void setTipoSolicitud(String tipoSolicitud) {
			this.tipoSolicitud = tipoSolicitud;
		}
		
		public String getNoReporteCabina() {
			return noReporteCabina;
		}
		public void setNoReporteCabina(String noReporteCabina) {
			this.noReporteCabina = noReporteCabina;
		}
		public String getNoFolio() {
			return noFolio;
		}
		public void setNoFolio(String noFolio) {
			this.noFolio = noFolio;
		}
            

   }
}





