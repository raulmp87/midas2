package mx.com.afirme.midas2.service.siniestros.sipac;


import java.io.InputStream;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.siniestros.sipac.LayoutSipac;
import mx.com.afirme.midas2.domain.siniestros.sipac.LoteLayoutSipac;

@Local
public interface ReporteLayoutSipacService {
	
	/**
	 * Obtiene los registros de un lote determinado
	 * @param idLote
	 * @return
	 */
	public List<LayoutSipac> findLayoutByIdLote(Long idLote);
	
	/**
	 * Genera Archivo TXT para subir la informacion a SIPAC
	 * @param list
	 * @param lote
	 * @return
	 */
	public InputStream generarArchivoTxt(List<LayoutSipac> list, LoteLayoutSipac lote);
	
	/**
	 * Desacarga el Archivo TXT de un Lote
	 * @param idLote
	 * @return
	 */
	public InputStream generarTxtByLote(Long idLote);
	
	/**
	 * Obtiene el listado de Lotes SIPAC
	 * @return
	 */
	public List<LoteLayoutSipac> obtenerLotesSipac();
	
	/**
	 * Obtiene el listado de Lotes Pendientes SIPAC
	 * @return
	 */
	public List<LoteLayoutSipac> obtenerLotesPendientes();
	
	/**
	 * Realiza la busqueda de reportes pendientes de enviar a SIPAC
	 * @param fechaInicial
	 * @param fechaFinal
	 * @return
	 */
	public List<LayoutSipac> buscarReportesPend(Date fechaInicial, Date fechaFinal);
	
	/**
	 * Guarda la informacion del Lote
	 * @param fechaInicial
	 * @param fechaFinal
	 * @return
	 */
	public LoteLayoutSipac saveLotes(Date fechaInicial, Date fechaFinal);
	
	/**
	 * Actualiza Lote
	 * @param idLote
	 * @param estatusLote
	 * @return
	 */
	public LoteLayoutSipac updateLote(Long idLote, String estatusLote);
	
	/**
	 * Guarda la informacion del Acreedor
	 * @param estimacion
	 */
	public void guardarLayoutDM(EstimacionCoberturaReporteCabina estimacion);
	
	/**
	 * Guarda la informacion del Deudor
	 * @param estimacion
	 */
	public void guardarLayoutRCV(EstimacionCoberturaReporteCabina estimacion);
	
	/**
	 * Actualiza Siniestro
	 * @param reporte
	 * @param siniestroCabina
	 */
	public void actualizaSiniestro(ReporteCabina reporte, SiniestroCabina siniestroCabina);
	
	/**
	 * Actualiza Lugar de Ocurrido
	 * @param layout
	 * @param reporte
	 */
	public void actualizaLugarOcurrido(ReporteCabina reporte);
	
	/**
	 * Actualiza los datos que se guardan en la pestaña "Cierre Reporte Siniestro"
	 * @param idReporte
	 * @param autoIncisoReporte
	 */
	public void actualizaDatosAutoInciso(Long idReporte, AutoIncisoReporteCabina autoIncisoReporte);
	
	/**
	 * Actualiza la informacion del LayoutSipac desde la consulta del Lote
	 * Si ya no hay ningun registro enviado (se actualiza el lote como completo)
	 * @param idLote
	 * @param layout
	 */
	public void save(Long idLote, LayoutSipac layout);

}
