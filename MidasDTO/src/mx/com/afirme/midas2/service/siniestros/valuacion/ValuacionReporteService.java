package mx.com.afirme.midas2.service.siniestros.valuacion;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation.OperationType;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ValuacionReporte;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ValuacionReportePieza;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ValuacionReporteDTO;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService.CatalogoFiltro;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService.PrestadorServicioRegistro;

@Local
public interface ValuacionReporteService {

	public static final String NOMBRE_ROL_VALUADORES = "Rol_M2_Coordinador_Valuacion_Siniestros";
	public static final String NOMBRE_ROL_M2_VALUADOR = "Rol_M2_Valuador";
	public static final String NOMBRE_USUARIO_BUSQUEDA = "SISTEMA";
	public static final Integer COLUMNA_ORIGEN = 4;
	public static final Integer COLUMNA_REFACCIONES = 5;
	public static final Integer COLUMNA_PINTURA= 6;
	public static final Integer COLUMNA_HOJALATERIA = 7;
	public static final Integer ESTATUS_EN_PROCESO = 1;
	public static final Integer ESTATUS_TERMINADO = 2;
	public static final Integer ES_CONSULTA_VERDADERO = 1;
	
	
	/**
	 * Agregar una pieza al listado
	 * @param idValuacionReporte
	 * @param idValRepPieza
	 */
	public void agregarPieza(ValuacionReporte reporte, ValuacionReportePieza piezaReporte);
	
	/**
	 * Eliminar una pieza de la lista
	 * @param idValRepPieza
	 */
	public void eliminarPieza(Long idValRepPieza);
	
	/**
	 * Guarda una ValuacionReporte
	 * @param valuacionReporte
	 */
	public void guardar(ValuacionReporte valuacionReporte);
	
	/**
	 * Listar valuaciones de crucero. Se reciben los filtros y se retorna un Listado con las valuaciones correspondientes
	 * @param filtroBusqueda
	 * @return
	 */
	public List<ValuacionReporteDTO> listar(ValuacionReporteFiltro filtroBusqueda);
	
	/**
	 * Metodo que obtiene una Valuacion de Crucero
	 * @param idValuacionReporte
	 * @return
	 */
	public ValuacionReporte obtener(Long idValuacionReporte);
	
	/**
	 * carga las descripciones de las piezas para que se presenten en pantalla
	 * @param piezas
	 * @return
	 */
	public List<ValuacionReportePieza> cargarDescripcionCatalogos(List<ValuacionReportePieza> piezas);
	
	/**
	 * Calcula el nuevo total y devuelve el reporte actualizado
	 * @param idPiezaValuacion
	 * @param tipoColumna
	 * @param nuevoValor
	 * @param origen
	 * @return
	 */
	public ValuacionReporte actualizarTotales(Long idPiezaValuacion, Integer tipoColumna, Double nuevoValor, String origen);
	
	/**
	 * Guarda un nuevo reporte de valuacion y devuelve el id de este nuevo reporte.
	 * @param valuacionReporte
	 * @return
	 */
	public Long guardar(Long idReporteCabina, String codigoValuador, Integer estatusValuacion);
	
	/**
	 * Busca la valuacion correspondiente al reporte con la id que se pasa como parametro
	 * @param idReporteCabina
	 * @return
	 */
	public ValuacionReporte obtenerValuacion(Long idReporteCabina);
	
	/**
	 * Valida que la valuacion tenga al menos una pieza y que todos los campos de costo tengan un valor capturado
	 * @return
	 */
	public List<String> terminarValuacionReporte(ValuacionReporte valuacionReporte);
	
	/**
	 * Genera la impresion para la valuacion en crucero
	 * @param idValuacionReporte
	 * @return
	 */
	public TransporteImpresionDTO imprimirValuacion(Long idValuacionReporte);
	
	/**
	 * Metodo para obtener la lista de valuadores disponibles para guardar la informacion de las valuaciones de hgs
	 * @return
	 */
	public List<PrestadorServicioRegistro> obtenerListadoValuadores();
	
	public class ValuacionReporteFiltro extends CatalogoFiltro{

		private Long ajustador;
		private Integer estatus;
		private Date fechaFinal;
		private Date fechaInicial;
		private String numeroReporte;
		private String claveOficina;
		private String consecutivoReporte;
		private String anioReporte;
		private String numeroSerie;
		private Long numeroValuacion;
		private String valuador;
		
		/**
		 * @return the ajustador
		 */
		@FilterPersistenceAnnotation(persistenceName="ajustador.id")
		public Long getAjustador() {
			return ajustador;
		}
		
		/**
		 * @param ajustador the ajustador to set
		 */
		public void setAjustador(Long ajustador) {
			this.ajustador = ajustador;
		}
		
		/**
		 * @return the estatus
		 */
		@FilterPersistenceAnnotation(persistenceName="estatus")
		public Integer getEstatus() {
			return estatus;
		}
		
		/**
		 * @param estatus the estatus to set
		 */
		public void setEstatus(Integer estatus) {
			this.estatus = estatus;
		}
		
		/**
		 * @return the fechaInicial
		 */
		@FilterPersistenceAnnotation(persistenceName="fechaValuacion", truncateDate=true , operation=OperationType.GREATERTHANEQUAL, paramKey="fechaInicial")
		public Date getFechaInicial() {
			return fechaInicial;
		}
		
		/**
		 * @param fechaInicial the fechaInicial to set
		 */
		public void setFechaInicial(Date fechaInicial) {
			this.fechaInicial = fechaInicial;
		}
		
		/**
		 * @return the fechaFinal
		 */
		@FilterPersistenceAnnotation(persistenceName="fechaValuacion", truncateDate=true , operation=OperationType.LESSTHANEQUAL, paramKey="fechaFinal")
		public Date getFechaFinal() {
			return fechaFinal;
		}
		
		/**
		 * @param fechaFinal the fechaFinal to set
		 */
		public void setFechaFinal(Date fechaFinal) {
			this.fechaFinal = fechaFinal;
		}
		
		/**
		 * @return the numeroReporte
		 */
		@FilterPersistenceAnnotation(persistenceName="reporte.numeroReporte")
		public String getNumeroReporte() {
			return numeroReporte;
		}
		
		/**
		 * @param numeroReporte the numeroReporte to set
		 */
		public void setNumeroReporte(String numeroReporte) {
			this.numeroReporte = numeroReporte;
		}
		
		/**
		 * @return the numeroSerie
		 */
		@FilterPersistenceAnnotation(persistenceName="reporte.seccionReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.numeroSerie")
		public String getNumeroSerie() {
			return numeroSerie;
		}
		
		/**
		 * @param numeroSerie the numeroSerie to set
		 */
		public void setNumeroSerie(String numeroSerie) {
			this.numeroSerie = numeroSerie;
		}
		
		/**
		 * @return the numeroValuacion
		 */
		@FilterPersistenceAnnotation(persistenceName="id")
		public Long getNumeroValuacion() {
			return numeroValuacion;
		}
		
		/**
		 * @param numeroValuacion the numeroValuacion to set
		 */
		public void setNumeroValuacion(Long numeroValuacion) {
			this.numeroValuacion = numeroValuacion;
		}
		
		/**
		 * @return the valuador
		 */
		@FilterPersistenceAnnotation(persistenceName="codigoValuador")
		public String getValuador() {
			return valuador;
		}
		
		/**
		 * @param valuador the valuador to set
		 */
		public void setValuador(String valuador) {
			this.valuador = valuador;
		}

		/**
		 * @return the claveOficina
		 */
		@FilterPersistenceAnnotation(persistenceName="reporte.claveOficina")
		public String getClaveOficina() {
			return claveOficina;
		}

		/**
		 * @param claveOficina the claveOficina to set
		 */
		public void setClaveOficina(String claveOficina) {
			this.claveOficina = claveOficina;
		}

		/**
		 * @return the consecutivoReporte
		 */
		@FilterPersistenceAnnotation(persistenceName="reporte.consecutivoReporte")
		public String getConsecutivoReporte() {
			return consecutivoReporte;
		}

		/**
		 * @param consecutivoReporte the consecutivoReporte to set
		 */
		public void setConsecutivoReporte(String consecutivoReporte) {
			this.consecutivoReporte = consecutivoReporte;
		}

		/**
		 * @return the anioReporte
		 */
		@FilterPersistenceAnnotation(persistenceName="reporte.anioReporte")
		public String getAnioReporte() {
			return anioReporte;
		}

		/**
		 * @param anioReporte the anioReporte to set
		 */
		public void setAnioReporte(String anioReporte) {
			this.anioReporte = anioReporte;
		}
		
	}
	
}
