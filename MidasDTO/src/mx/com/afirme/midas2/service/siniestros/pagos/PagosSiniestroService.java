package mx.com.afirme.midas2.service.siniestros.pagos;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.pagos.OrdenPagoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.DetalleOrdenCompra;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.FacturaLiquidacionDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.DesglosePagoConceptoDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.OrdenPagosDTO;

/**
 * Definicion de m�todos del m�dulo de Pagos
 * @author usuario
 * @version 1.0
 * @updated 22-sep-2014 03:59:37 p.m.
 */
@Local
public interface PagosSiniestroService {
	 
	/*VARIABLES*/
	public static final String ESTATUS_PAGADA="2";
	public static final String ESTATUS_LIBERADA="1";
	public static final String ESTATUS_CANCELADA="3";
	public static final String ESTATUS_ASOCIADA="4";
	public static final String ESTATUS_PREREGISTRO="5";
	
	public static final String TIPOPAGO_TOTAL="1";
	public static final String TIPOPAGO_PARCIAL="2";
	public static final String TIPOPAGO_COMPLEMENTO="3";

	
	public static final String ORDENPAGO_A_TERCERO="2";
	public static final String ORDENPAGO_A_ASEGURADO="1";
	
	/**
	 * Get total details for pay order through stored procedure from PKGSIN_ORDENCOMPRA.
	 * 
	 * @param Long ordenPagoId 
	 * @param Boolean calcularReserva
	 */
	public DesglosePagoConceptoDTO obtenerTotalesSP(Long ordenPagoId, Boolean calcularReserva);
	/**
	 * obtiene las cantidades totales de una orden de pago
	 * @param Long idEstimacionCobRepCab
	 * @param Long idReporteCabina
	 * @param Long idcoberturaReporteCabina
	 */
	public BigDecimal obtenerTotales(Long idEstimacionCobRepCab, Long idReporteCabina, Long idcoberturaReporteCabina);


	/**
	 * Busca ordenes  de pago segun el filtro seleccionado. si solo compra esta habilitado, omite los datos de la orden de pago.
	 * @param  OrdenPagosDTO filtro
	 * @param boolean soloOrdenCompra
	 */
	public List<OrdenPagosDTO>  buscarOrdenesPago( OrdenPagosDTO filtro, boolean soloOrdenCompra);

	/**
	 * Cancela una orden de pago
	 * @param ordePago
	 */
	public void cancelarOrdenPago(Long idOrdenPago) throws Exception;
	
	/**
	 * Envia correo de cancelacion de orden compra
	 * @param Long idOrdenCompra
	 */
	public void solicitarCancelacionOrdenCompra(Long idOrdenCompra);

	/**
	 * Guarda el detalle de una orden de pago 
	 * @param DetalleOrdenPago2 detalle
	 */
	public void guardarDetalleOrdenPago(DetalleOrdenCompra detalle);

	/**
	 * Guarda una orden de pago
	 * @param OrdenPagoSiniestro ordenPago
	 */
	public OrdenPagoSiniestro guardarOrdenPago(OrdenPagoSiniestro ordenPago);
	
	/**
	 * Obtiene una orden de pago por reporte cabina, cobertura , estimacion , tipo pago y estatus del pago.
	 * @param OrdenPagoSiniestro ordenPago
	 */
	public List <OrdenPagoSiniestro>  obtenerOrdenesPago(Long idEstimacionCobRepCab,
			Long idReporteCabina, Long idcoberturaReporteCabina, String tipoPago, String estatusPago);
	

	/**
	 * Obtiene el detalle de una orden de pago 
	 * @param Long idOrdenPago
	 */

	
	public List<DesglosePagoConceptoDTO> getDetalleOrdenPago(Long idOrdenPago, Boolean calcularReserva);

	/**
	 * busca una orden de pago ya sea por id de orden de pago o por el id de orden de
	 * compra y/o orden pago.
	 * 
	 * @param irOrdenCompra
	 * @param idOrdenPago
	 */
	public OrdenPagoSiniestro obtenerOrdenPago(Long idOrdenCompra, Long idOrdenPago);



	/**
	 * Obtiene los totales de una orden de pago especifica.
	 * @param OrdenPagoSiniestro ordenPago
	 */

	
	public DesglosePagoConceptoDTO obtenerTotales(Long ordenPagoId, Boolean calcularReserva);
	
	/**
	 * Obtiene el desglose de una orden de pago
	 * @param OrdenPagoSiniestro ordenPago
	 */
	public  DesglosePagoConceptoDTO obtenerDegloseTotalesPorOrdenPago(OrdenPagoSiniestro ordenPago);
	
	public  DesglosePagoConceptoDTO obtenerTotalesPorDesglose(List<DesglosePagoConceptoDTO> lstdetalleDeglose);
	

	public List<DocumentoFiscal>  buscarFacturasLiquidacion(Long idProveedor);
	
	public List<FacturaLiquidacionDTO> buscarOrdenesPagoFactura(Long idFactura);
	
	public OrdenPagoSiniestro generacionAutomaticaOrdenPago(
			Long idOrdenCompra );
	
	public OrdenPagoSiniestro autorizarOrdenPago(Long idOrdenPago);

	/**Deberá obtener una lista de nombres de beneficiarios de Orden de Compra donde su orden de pago no aparezca en alguna liquidación.
	  *@param String beneficiario
	 * @return  List<String>   
	 * */
	public List<String>  buscarBeneficiario(String beneficiario);
	/**Deberá obtener dado un nombre de beneficiario, sus órdenes de pago que no aparezcan en alguna liquidación.
	 * 
	 * @param beneficiario
	 * @return List<OrdenPagoSiniestro>
	 */
	public List<OrdenPagoSiniestro>  buscarOrdenPagoIndemnizacion(String beneficiario);
	
	public List<OrdenPagoSiniestro> buscarOrdenesPagoIndemnizacion(String nombreBeneficiario);
	
	public List<Map<String, Object>> buscarBeneficiariosLiquidacion();
	
	/**
	 * Retornar la orden de pago para un id de orden de compra. Si la orden de pago no existe se invoca la generacion automatica de la misma.
	 * @param idOrdenCompra
	 * @return
	 */
	public OrdenPagosDTO obtenerOrdenPago(Long idOrdenCompra);
}