package mx.com.afirme.midas2.service.siniestros.notificaciones;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation.OperationType;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.ConfiguracionNotificacionCabina;
import mx.com.afirme.midas2.domain.siniestros.notificaciones.BitacoraDetalleNotificacion;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService.EmailDestinaratios;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService.EnumCodigo;

@Local
public interface BitacoraNotificacionService {
	
	/**
	 * DTO filtro para la bitacora de notificaciones registradas
	 * @author Administrator
	 *
	 */
	public class BitacoraNotificacionFiltroDTO{
		
		private String folio;
		
		private String codigo;
		
		private Boolean reenvio;
		
		private String destinatario;
		
		private String codigoUsuarioEnvio;
		
		private Date fechaEnvioDesde;

		private Date fechaEnvioHasta;
		
		@FilterPersistenceAnnotation(persistenceName="destino.bitacora.folio")
		public String getFolio() {
			return folio;
		}

		public void setFolio(String folio) {
			this.folio = folio;
		}

		@FilterPersistenceAnnotation(persistenceName="destino.bitacora.configuracion.movimientoProceso.codigo")
		public String getCodigo() {
			return codigo;
		}

		public void setCodigo(EnumCodigo codigo) {
			this.codigo = codigo.toString();
		}

		@FilterPersistenceAnnotation(persistenceName="destino.bitacora.recurrente")
		public Boolean getReenvio() {
			return reenvio;
		}

		public void setReenvio(Boolean reenvio) {
			this.reenvio = reenvio;
		}

		@FilterPersistenceAnnotation(persistenceName="destino.destinatario", operation=OperationType.LIKE)
		public String getDestinatario() {
			return destinatario;
		}
		
		public void setDestinatario(String destinatario) {
			this.destinatario = destinatario;
		}

		@FilterPersistenceAnnotation(persistenceName="codigoUsuarioCreacion", operation=OperationType.LIKE)
		public String getCodigoUsuarioEnvio() {
			return codigoUsuarioEnvio;
		}

		public void setCodigoUsuarioEnvio(String codigoUsuarioEnvio) {
			this.codigoUsuarioEnvio = codigoUsuarioEnvio;
		}

		@FilterPersistenceAnnotation(persistenceName="fechaCreacion", truncateDate=true , operation=OperationType.GREATERTHANEQUAL, paramKey="fechaCreacionDesde")
		public Date getFechaEnvioDesde() {
			return fechaEnvioDesde;
		}

		public void setFechaEnvioDesde(Date fechaEnvioDesde) {
			this.fechaEnvioDesde = fechaEnvioDesde;
		}
		@FilterPersistenceAnnotation(persistenceName="fechaCreacion", truncateDate=true , operation=OperationType.LESSTHANEQUAL, paramKey="fechaCreacioHasta")
		public Date getFechaSolicitudHasta() {
			return fechaEnvioHasta;
		}
		public void setFechaSolicitudHasta(Date fechaEnvioHasta) {
			this.fechaEnvioHasta = fechaEnvioHasta;
		}
		
	}
	
	/**
	 * DTO para obtener los detalles de un registra de bitacora una notificacion
	 * @author Administrator
	 *
	 */
	public class BitacoraRegistroDetalleNotificacionDTO{

		private Long idRegistro;
		
		private String folio;
		
		private ConfiguracionNotificacionCabina configuracion;
		
		private Boolean recurrente;
		
		private String destinatario;
		
		private String mapaDatos;
		
		private Integer secuenciaEnvio;
		
		private String codigoUsuarioEnvio;
		
		private Date fechaEnvio;
		
		public BitacoraRegistroDetalleNotificacionDTO(){
			super();
		}

		public BitacoraRegistroDetalleNotificacionDTO(Long idRegistro,
				String folio, ConfiguracionNotificacionCabina configuracion,
				Boolean recurrente, String destinatario, String mapaDatos,
				Integer secuenciaEnvio, String codigoUsuarioEnvio,
				Date fechaEnvio) {
			this();
			this.idRegistro = idRegistro;
			this.folio = folio;
			this.configuracion = configuracion;
			this.recurrente = recurrente;
			this.destinatario = destinatario;
			this.mapaDatos = mapaDatos;
			this.secuenciaEnvio = secuenciaEnvio;
			this.codigoUsuarioEnvio = codigoUsuarioEnvio;
			this.fechaEnvio = fechaEnvio;
		}

		public Long getIdRegistro() {
			return idRegistro;
		}

		public void setIdRegistro(Long idRegistro) {
			this.idRegistro = idRegistro;
		}

		public String getFolio() {
			return folio;
		}

		public void setFolio(String folio) {
			this.folio = folio;
		}

		public ConfiguracionNotificacionCabina getConfiguracion() {
			return configuracion;
		}

		public void setConfiguracion(ConfiguracionNotificacionCabina configuracion) {
			this.configuracion = configuracion;
		}

		public Boolean getRecurrente() {
			return recurrente;
		}

		public void setRecurrente(Boolean recurrente) {
			this.recurrente = recurrente;
		}

		public String getDestinatario() {
			return destinatario;
		}

		public void setDestinatario(String destinatario) {
			this.destinatario = destinatario;
		}
		
		public String getMapaDatos() {
			return mapaDatos;
		}

		public void setMapaDatos(String mapaDatos) {
			this.mapaDatos = mapaDatos;
		}

		public Integer getSecuenciaEnvio() {
			return secuenciaEnvio;
		}

		public void setSecuenciaEnvio(Integer secuenciaEnvio) {
			this.secuenciaEnvio = secuenciaEnvio;
		}

		public String getCodigoUsuarioEnvio() {
			return codigoUsuarioEnvio;
		}

		public void setCodigoUsuarioEnvio(String codigoUsuarioEnvio) {
			this.codigoUsuarioEnvio = codigoUsuarioEnvio;
		}

		public Date getFechaEnvio() {
			return fechaEnvio;
		}

		public void setFechaEnvio(Date fechaEnvio) {
			this.fechaEnvio = fechaEnvio;
		}
		
	}

	/**
	 * Actualizar el estatus de reenvio para una notificacion.
	 * @param folio
	 * @param reenvio
	 */
	public void cambiarReenvio(String folio, Boolean reenvio);
	
	/**
	 * Obtener registros marcados con recurrencia
	 */
	public List<BitacoraRegistroDetalleNotificacionDTO> obtenerRecurrentes();
	
	
	/**
	 * Obtener bitacoras para un folio en especial. 
	 * @param folio
	 * @return
	 */
	public List<BitacoraDetalleNotificacion> obtenerBitacora(String folio);
	
	/**
	 * Obtener bitacoras para un codigo de movimiento
	 * @param codigo
	 * @return
	 */
	public List<BitacoraDetalleNotificacion> obtenerBitacora(EnumCodigo codigo);
	
	/**
	 * Obtener bitacoras por filtro
	 * @param filtro
	 * @return
	 */
	public List<BitacoraDetalleNotificacion> obtenerBitacora(BitacoraNotificacionFiltroDTO filtro);
	
	/**
	 * Salvar una bitacora nueva y su detalle
	 * @param folio
	 * @param recurrente
	 * @param destinatario
	 * @param cuerpo
	 * @param titulo
	 * @return <code>true</code> success, <code>false</code> error
	 */
	public boolean salvarBitacora(String folio, EnumCodigo codigo, EmailDestinaratios destinatarios, Map<String, Serializable> mapaDatos);
	
	/**
	 * Salvar una bitacora nueva y su detalle
	 * @param folio
	 * @param recurrente
	 * @param destinatario
	 * @param cuerpo
	 * @param titulo
	 * @return <code>true</code> success, <code>false</code> error
	 */
	public boolean salvarBitacora(String folio, EnumCodigo codigo, String destinatario, Map<String, Serializable> mapaDatos);
	
	/**
	 * Salvar una bitacora nueva y su detalle
	 * @param folio
	 * @param recurrente
	 * @param destinatario
	 * @param cuerpo
	 * @param titulo
	 * @return <code>true</code> success, <code>false</code> error
	 */
	public boolean salvarBitacora(String folio, String codigo, String destinatario, Map<String, Serializable> mapaDatos);
	
	/**
	 * Salvar una bitacora nueva y su detalle
	 * @param folio
	 * @param recurrente
	 * @param destinatario
	 * @param cuerpo
	 * @param titulo
	 * @return <code>true</code> success, <code>false</code> error
	 */
	public boolean salvarBitacora(String folio, String codigo, EmailDestinaratios destinatarios, Map<String, Serializable> mapaDatos);
	
	/**
	 * Agregar un detalle a un registro de bitacora
	 * @param idBitacora
	 * @param destinatario
	 * @param cuerpo
	 * @param titulo
	 */
	public void agregarDetalle(Long idBitacora, String destinatario, Map<String, Serializable> mapaDatos);
	
	/**
	 * Agregar un detalle a un registro de bitacora
	 * @param folio
	 * @param destinatario
	 * @param cuerpo
	 * @param titulo
	 */
	public void agregarDetalle(String folio, String destinatario, Map<String, Serializable> mapaDatos);
}
