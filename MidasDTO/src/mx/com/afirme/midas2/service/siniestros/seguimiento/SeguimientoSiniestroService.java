package mx.com.afirme.midas2.service.siniestros.seguimiento;
import javax.ejb.Local;
import mx.com.afirme.midas2.dto.siniestros.seguimiento.SeguimientoJuridicoDTO;
import mx.com.afirme.midas2.dto.siniestros.seguimiento.SeguimientoRoboTotalDTO;
import mx.com.afirme.midas2.dto.siniestros.seguimiento.SeguimientoValuacionDTO;

/**
 * Interfaz SeguimientoSiniestroService para encapsular la informaci�n de los
 * diferentes tipos de seguimiento de siniestro.
 * 
 * Los m�todos estan preparados para recibir el Numero de Siniestro, ya que se
 * contempla que esta informaci�n es la que tiene el cliente para consultar por
 * Internet. Internamente se deber� obtener el Id del Reporte de la entidad
 * ReporteCabina para acceder directamente a la informaci�n.
 * @author Arturo
 * @version 1.0
 * @created 13-oct-2014 11:56:19 a.m.
 */
@Local
public interface SeguimientoSiniestroService {

	
	/**
	 * 
	 * @param numeroSiniestro
	 */
	public SeguimientoJuridicoDTO obtenerSeguimientoJuridico(String numeroSiniestro);

	/**
	 * 
	 * @param numeroSiniestro
	 */
	public SeguimientoRoboTotalDTO obtenerSeguimientoRoboTotal(String numeroSiniestro);

	/**
	 * 
	 * @param numeroSiniestro
	 * @param numeroPaseAtencion
	 */
	public SeguimientoValuacionDTO obtenerSeguimientoValuacion(String numeroSiniestro, Long numeroPaseAtencion);

}