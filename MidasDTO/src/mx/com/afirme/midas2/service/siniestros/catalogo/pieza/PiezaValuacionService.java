package mx.com.afirme.midas2.service.siniestros.catalogo.pieza;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation.OperationType;
import mx.com.afirme.midas2.domain.siniestros.catalogo.pieza.PiezaValuacion;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService;

@Local
public interface PiezaValuacionService extends CatalogoSiniestroService {
	
	public void guardar(PiezaValuacion piezaValuacion);
	
	public PiezaValuacion obtener(Long idPiezaValuacion);
	
	public List<PiezaValuacion> listar(PiezaValuacionFiltro piezaValuacionFiltro);
	
	class PiezaValuacionFiltro extends CatalogoFiltro {
		
		private Long numPieza;
		private String seccionAutomovil;
		private String descripcion;
		
		@FilterPersistenceAnnotation(persistenceName="id")
		public Long getNumPieza() {
			return numPieza;
		}
		public String getSeccionAutomovil() {
			return seccionAutomovil;
		}
		
		@FilterPersistenceAnnotation(persistenceName="descripcion", operation=OperationType.LIKE )
		public String getDescripcion() {
			return descripcion;
		}
		public void setNumPieza(Long numPieza) {
			this.numPieza = numPieza;
		}
		public void setSeccionAutomovil(String seccionAutomovil) {
			this.seccionAutomovil = seccionAutomovil;
		}
		public void setDescripcion(String descripcion) {
			this.descripcion = descripcion;
		}
		
		
	}

}
