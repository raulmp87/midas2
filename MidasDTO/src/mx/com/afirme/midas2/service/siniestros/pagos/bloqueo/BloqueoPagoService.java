package mx.com.afirme.midas2.service.siniestros.pagos.bloqueo;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation.OperationType;
import mx.com.afirme.midas2.domain.siniestros.pagos.BloqueoPago;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService.CatalogoFiltro;
import mx.com.afirme.midas2.util.MidasException;

@Local
public interface BloqueoPagoService {

	public List<BloqueoPago> buscarBloqueos(BloqueoPagoFiltro filtroBloqueos);

	public void guardarBloqueo(BloqueoPago bloqueo) throws MidasException;

	public void notificarBloqueoSeycos(BloqueoPago bloqueoPago);

	public BloqueoPago obtenerBloqueoPago(Long idBloquePago);

	public void validarBloqueoPago(BloqueoPago bloqueo) throws MidasException;

	public void validarProveedorBloqueado(Integer idProveedor) throws MidasException;

	public Map<Integer, String> obtenerEstatusBloqueoDescripcion();

	public Map<String, String> obtenerTipoDeBloqueoDescripcion();
	
	public Boolean estaElProovedorBloqueado(Integer proveedorId);
	
	public Boolean estaElPagoBloqueadoPorProovedor(Long pagoId);
	
	public Boolean estaElPagoBloqueadoDirectamente(Long pagoId);

	public class BloqueoPagoFiltro extends CatalogoFiltro {

		private Integer proovedorId;

		private String tipoBloqueo;

		private Integer estatusPago;

		private Date fechaBloqueoDe;

		private Date fechaBloqueoHasta;

		private Long ordenDePago;

		@FilterPersistenceAnnotation(persistenceName = "tipo")
		public String getTipoBloqueo() {
			return tipoBloqueo;
		}

		public void setTipoBloqueo(String tipoBloqueo) {
			this.tipoBloqueo = tipoBloqueo;
		}

		@FilterPersistenceAnnotation(persistenceName = "fechaBloqueo", truncateDate = true, operation = OperationType.GREATERTHANEQUAL, paramKey = "fechaInicial")
		public Date getFechaBloqueoDe() {
			return fechaBloqueoDe;
		}

		@FilterPersistenceAnnotation(persistenceName = "estatus")
		public Boolean getEstatusPagoBoolean() {
			Boolean estatusBoolean = null;
			if(this.estatusPago!=null){
				estatusBoolean = (this.estatusPago==1)?Boolean.TRUE:Boolean.FALSE;
			}
			return estatusBoolean;
		}

		public void setEstatusPago(Integer estatusPago) {
			this.estatusPago = estatusPago;
		}

		@FilterPersistenceAnnotation(persistenceName = "proveedor.id")
		public Integer getProovedorId() {
			return proovedorId;
		}

		public void setProovedorId(Integer proovedorId) {
			this.proovedorId = proovedorId;
		}

		public void setFechaBloqueoDe(Date fechaBloqueoDe) {
			this.fechaBloqueoDe = fechaBloqueoDe;
		}

		@FilterPersistenceAnnotation(persistenceName = "fechaBloqueo", truncateDate = true, operation = OperationType.LESSTHANEQUAL, paramKey = "fechaFinal")
		public Date getFechaBloqueoHasta() {
			return fechaBloqueoHasta;
		}

		public void setFechaBloqueoHasta(Date fechaBloqueoHasta) {
			this.fechaBloqueoHasta = fechaBloqueoHasta;
		}

		@FilterPersistenceAnnotation(persistenceName = "ordenPago.id")
		public Long getOrdenDePago() {
			return ordenDePago;
		}

		public void setOrdenDePago(Long ordenDePago) {
			this.ordenDePago = ordenDePago;
		}

		
	}

}