package mx.com.afirme.midas2.service.siniestros.recuperacion;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.EstatusRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionDeducible;
import mx.com.afirme.midas2.domain.siniestros.valuacion.hgs.ValuacionHgs;

@Local
public interface RecuperacionDeducibleService extends RecuperacionService {
	
	/**
	 * 
	 * @param reporteId
	 * @param estimacionId
	 * @param reservaActual
	 * @param tipoAjuste
	 */
	public void actualizarAjusteReserva(Long estimacionId, BigDecimal reservaActual, String tipoAjuste);

	/**
	 * 
	 * @param reporteId
	 * @param estimacionId
	 * @param aplicaDeducible
	 */
	public void actualizarAplicaDeducible(Long reporteId, Long estimacionId, Boolean aplicaDeducible);
	
	/**
	 * Inactivar recuperacion de deducible siempre y cuando el estatus actual sea <code>Registrado</code> o <code>Pendiente</code>. 
	 * Cancela el ingreso actual en caso de existir
	 * @param estimacionId
	 * @param motivo
	 */
	public void inactivarRecuperacion(Long estimacionId, String motivo);
	
	/**
	 * Activar recuperacion de deducible siempre y cuando el estatus actual sea <code>Inactivo</code>
	 * @param estimacionId
	 */
	public void activarRecuperacion(Long estimacionId, String motivo);
	
	/**
	 * Retorna una recuperacion de deducible para una estimacion.
	 * @param estimacionId
	 * @return
	 */
	public RecuperacionDeducible obtenerRecuperacionPorEstimacionId(Long estimacionId);

	/**
	 * 
	 * @param valuacionHGS
	 */
	public void actualizarHGS(ValuacionHgs valuacionHGS);

	/**
	 * 
	 * @param reporteId
	 * @param estimacionId
	 */
	public void actualizarPendienteRecuperar(Long reporteId, Long estimacionId);

	/**
	 * 
	 * @param recuperacionId
	 * @param motivo
	 */
	public void cancelar(Long recuperacionId, String motivo);

	/**
	 * 
	 * @param reporteId
	 * @param estimacionId
	 */
	public void generar(Long reporteId, Long estimacionId,BigDecimal montoDeducible,BigDecimal montoFinalDeducible);
	/**
	 * 
	 * @param recuperacionDeducible
	 */
	public RecuperacionDeducible guardar(RecuperacionDeducible recuperacionDeducible);

	/**
	 * 
	 * @param recuperacionId
	 */
	public RecuperacionDeducible obtener(Long recuperacionId);
	
	public BigDecimal calcularDeducible(ReporteCabina reporteCabina,String tipoPaseAtencion);
	
	public BigDecimal obtenerPorcentajeDeducible();
	
	public void generar(Long estimacionId,BigDecimal montoFinalDeducible);
	
	public BigDecimal obtenerPorcentajeDeducibleValorComercial();
	
	/**
	 * Obtener el monto total de deducible para todo un siniestro, exceptuando la cobertura de RCV
	 * @param reporteId
	 * @return
	 */
	public BigDecimal obtenerDeducibleSiniestroNoRCV(Long reporteId);

	/**
	 * Obtener el monto total de deducible por estimacion
	 * @param reporteId
	 * @return
	 */
	public BigDecimal obtenerDeduciblePorEstimacion(Long estimacionId);
	
	/**
	 * Obtener recuperaciones de deducible por estimacion
	 * @param reporteId
	 * @return
	 */
	public List<RecuperacionDeducible> obtenerRecuperacionDeduciblePorEstimacion(Long estimacionId);
	
	/**
	 * Obtener el monto total por estatus para un siniestro en particular
	 * @param siniestroId
	 * @param estatus
	 * @return
	 */
	public BigDecimal obtenerMontoRecuperacionDeducible(Long siniestroId, EstatusRecuperacion estatus);
	
	/**
	 * Actualiza el monto final de una recuperacion de deducible. El método guarda en la bitácora de eventos la modificación.
	 * @param recuperacionId
	 * @param monto
	 */
	public void actualizarRecuperacion(Long recuperacionId, BigDecimal monto); 
	
	/**
	 * Obtener monto deducible por cobertura y cve sub calculo (RCV, RCP, RCB)
	 * @param idCoberturaReporte
	 * @param cveSubCalculo
	 * @return
	 */
	public BigDecimal obtenerDeduciblePorCobertura(Long idCoberturaReporte, String cveSubCalculo);
}
