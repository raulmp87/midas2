package mx.com.afirme.midas2.service.siniestros.configuracion.horario.laboral;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.laboral.HorarioLaboral;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService;

@Local
public interface HorarioLaboralService extends CatalogoSiniestroService {
	
	public void guardar(HorarioLaboral horarioLaboral);
	
	public HorarioLaboral obtener(Long id);

	public List<HorarioLaboral> listar(HorarioLaboralFiltro entidadFltro);

	class HorarioLaboralFiltro extends CatalogoFiltro {

		private String horaEntradaCode;

		private String horaSalidaCode;

		private Integer estatus;
		
		private Boolean predeterminado;

		public void setPredeterminado(Boolean predeterminado) {
			this.predeterminado = predeterminado;
		}

		public Boolean getPredeterminado() {
			return predeterminado;
		}

		public Integer getEstatus() {
			return estatus;
		}

		public void setEstatus(Integer estatus) {
			this.estatus = estatus;
		}

		public void setHoraSalidaCode(String horaSalidaCode) {
			this.horaSalidaCode = horaSalidaCode;
		}

		public String getHoraSalidaCode() {
			return horaSalidaCode;
		}

		public void setHoraEntradaCode(String horaEntradaCode) {
			this.horaEntradaCode = horaEntradaCode;
		}

		public String getHoraEntradaCode() {
			return horaEntradaCode;
		}
	}
}
