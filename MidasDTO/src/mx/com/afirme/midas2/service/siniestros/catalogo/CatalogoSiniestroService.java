package mx.com.afirme.midas2.service.siniestros.catalogo;

import java.util.List;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

public interface CatalogoSiniestroService {

	public <K extends CatalogoFiltro, E extends Entidad> List<E> buscar(
			 Class<E> entidad, K filtro);
	
	public <K extends CatalogoFiltro, E extends Entidad, T extends Object> List<E> buscar(
			 Class<E> entidad, K filtro, String ordenPor);

	public <E extends Entidad, K> E obtener(Class<E> entityClass,
			K id);

	public <E extends Entidad> void salvar(E entidad);
	
	public <E extends Entidad> Long salvarObtenerId(E entidad);

	public abstract class CatalogoFiltro {
		
		protected Integer estatus;
		protected String nombre;
		protected Long numeroCatalogo;
		protected Long numeroPrestador;
		protected Long oficinaId;
		public Integer getEstatus() {
			return estatus;
		}
		public void setEstatus(Integer estatus) {
			this.estatus = estatus;
		}
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public Long getNumeroCatalogo() {
			return numeroCatalogo;
		}
		public void setNumeroCatalogo(Long numeroCatalogo) {
			this.numeroCatalogo = numeroCatalogo;
		}
		public Long getNumeroPrestador() {
			return numeroPrestador;
		}
		public void setNumeroPrestador(Long numeroPrestador) {
			this.numeroPrestador = numeroPrestador;
		}
		public Long getOficinaId() {
			return oficinaId;
		}
		public void setOficinaId(Long oficinaId) {
			this.oficinaId = oficinaId;
		}

	}

	
}
