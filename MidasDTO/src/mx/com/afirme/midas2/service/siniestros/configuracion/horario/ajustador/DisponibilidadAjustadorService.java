package mx.com.afirme.midas2.service.siniestros.configuracion.horario.ajustador;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.ajustador.HorarioAjustador.TipoDisponibilidad;

@Local
public interface DisponibilidadAjustadorService {

	/**
	 * Funcion para obtener la fecha y hora inicial correspondiente al dia de
	 * hoy para el ajustador con el tipo de disponibilidad indicado
	 * 
	 * @param ajustador
	 * @param tipoDisponibilidad
	 * @return un Date con fecha y hora
	 */
	public Date obtenerFechaHoraEntrada(ServicioSiniestro ajustador,
			TipoDisponibilidad tipoDisponibilidad);

	/**
	 * 
	 * Funcion para obtener la fecha y hora final correspondiente al dia de hoy
	 * para el ajustador con el tipo de disponibilidad indicado
	 * 
	 * @param ajustador
	 * @param tipoDisponibilidad
	 * @return un Date con fecha y hora
	 */
	public Date obtenerFechaHoraSalida(ServicioSiniestro ajustador,
			TipoDisponibilidad tipoDisponibilidad);

	/**
	 * 
	 * Funcion para obtener la fecha y hora inicial correspondiente al dia
	 * indicado para el ajustador con el tipo de disponibilidad indicado
	 * 
	 * @param ajustador
	 * @param fecha
	 * @param tipoDisponibilidad
	 * @return un Date con fecha y hora
	 */
	public Date obtenerFechaHoraDeEntrada(ServicioSiniestro ajustador,
			Date fecha, TipoDisponibilidad tipoDisponibilidad);

	/**
	 * 
	 * Funcion para obtener la fecha y hora final correspondiente al dia
	 * indicado para el ajustador con el tipo de disponibilidad indicado
	 * 
	 * @param ajustador
	 * @param fechaHora
	 * @param tipoDisponibilidad
	 * @return un Date con fecha y hora
	 */
	public Date obtenerFechaHoraDeSalida(ServicioSiniestro ajustador,
			Date fechaHora, TipoDisponibilidad tipoDisponibilidad);

	/**
	 * Funcion para obtener el tipo de disponibilidad de el ajustador indicado
	 * en la fecha y hora indicada
	 * 
	 * @param fechaHora
	 * @return enum TIPO_DISPONIBILIDAD
	 */
	public TipoDisponibilidad obtenerDisponibilidadAjustadorEnFechaHora(
			ServicioSiniestro ajustador, Date fechaHora);

	/**
	 * Funcion para obtener lista de ajustadores con tipo de disponibilidad
	 * indicada correspondiente a la fecha y hora actual
	 * 
	 * @param tipoDispon
	 * @param oficinaId En caso de ser nulo se buscara en todas las oficinas
	 * @return lista de ServicioSiniestro
	 */
	public List<ServicioSiniestro> obtenerAjustadoresConDisponibilidad(
			TipoDisponibilidad tipoDispon, Long oficinaId);

	/**
	 * 
	 * Funcion para obtener lista de ajustadores con tipo de disponibilidad
	 * indicada correspondiente a la fecha y hora indicada
	 * 
	 * @param tipoDispon
	 * @param fecha
	 * @param oficinaId En caso de ser nulo se buscara en todas las oficinas
	 * @return
	 */
	public List<ServicioSiniestro> obtenerAjustadoresConDisponibilidad(
			TipoDisponibilidad tipoDispon, Date fecha,  Long oficinaId);

	/**
	 * Funcion que implementa la RDN {6E52F64A-E2A2-4eee-834C-AD367995D4FA} /
	 * Analisis MIDAS II.Requirements Model.Requerimientos MODULO SINIESTROS
	 * AUTOS.CABINA.Configurador de Horarios de Ajustadores.RDN: Cambio de
	 * Disponibilidad Automática <br/>
	 * Funcion para saber si el ajustador indicado esta con un Tipo de
	 * Disonibilidad disponible en la fecha y hora actual
	 * 
	 * @param ajustador
	 * @return True si el tipo de disponibilidad de ajustador es de tipo
	 *         disponible, ej: [Descripcion]("[codigo]", true). <br/>
	 *         False si el tipo de disponibilidad de ajustador no es de tipo
	 *         disponible, ej: [Descripcion]("[codigo]", false). <br/>
	 * @see {@link TIPO_DISPONIBILIDAD}
	 */
	public Boolean estaEnHorarioDisponible(ServicioSiniestro ajustador);

	/**
	 * Funcion para saber si el ajustador indicado esta disponible en la fecha y
	 * hora indicada
	 * 
	 * @param ajustador
	 * @param fechaHora
	 * @return True si el tipo de disponibilidad de ajustador es de tipo
	 *         disponible, ej: [Descripcion]("[codigo]", true). <br/>
	 *         False si el tipo de disponibilidad de ajustador no es de tipo
	 *         disponible, ej: [Descripcion]("[codigo]", false). <br/>
	 * @see {@link TIPO_DISPONIBILIDAD}
	 */
	public Boolean estaEnHorarioDisponible(ServicioSiniestro ajustador,
			Date fechaHora);

	/**
	 * Funcion que implementa la RDN {6E52F64A-E2A2-4eee-834C-AD367995D4FA} /
	 * Analisis MIDAS II.Requirements Model.Requerimientos MODULO SINIESTROS
	 * AUTOS.CABINA.Configurador de Horarios de Ajustadores.RDN: Cambio de
	 * Disponibilidad Automática <br/>
	 * Funcion para saber si el ajustador indicado esta disponible en la fecha y
	 * hora actual
	 * 
	 * @param ajustador
	 * @return True y False deacuerdo a la RDN
	 */
	public Boolean estaDisponible(ServicioSiniestro ajustador);

}