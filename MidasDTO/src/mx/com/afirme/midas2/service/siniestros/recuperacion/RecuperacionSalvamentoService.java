package mx.com.afirme.midas2.service.siniestros.recuperacion;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.EstatusRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionSalvamento;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionSalvamento.TipoSalvamento;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.VentaSalvamento;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.IndicadoresVentaDTO;

@Local
public interface RecuperacionSalvamentoService extends RecuperacionService{
	
	/**
	 * Guadar recuperacion de salvamento. En caso de no existir crea un nuevo registro con valores default. En caso de existir actualiza la informacion enviada.
	 * @param estimacionId
	 * @param valorEstimado
	 * @param ubicacionDeterminacion
	 * @param tipoSalvamento
	 * @param abandonoIncosteable
	 * @param direccionSalvamento
	 * @param codigoCiudadMidas
	 */
	public void guardarRecuperacionSalvamento(Long estimacionId, BigDecimal valorEstimado, String ubicacionDeterminacion, TipoSalvamento tipoSalvamento, 
			Boolean abandonoIncosteable, String direccionSalvamento, String codigoCiudadMidas);
	
	/**
	 * Guadar recuperacion de salvamento. En caso de no existir crea un nuevo registro con valores default. En caso de existir actualiza la informacion enviada.
	 * @param estimacionId
	 * @param valorEstimado
	 * @param ubicacionDeterminacion
	 * @param tipoSalvamento
	 * @param abandonoIncosteable
	 * @param direccionSalvamento
	 * @param codigoCiudadMidas
	 * @param motivoIncosteable
	 */
	public void guardarRecuperacionSalvamento(Long estimacionId, BigDecimal valorEstimado, String ubicacionDeterminacion, TipoSalvamento tipoSalvamento, 
			Boolean abandonoIncosteable, String direccionSalvamento, String codigoCiudadMidas, String motivoIncosteable);
	
	/**
	 * Guardar recuperacion de salvemento.
	 * @param salvamento Objeto de tipo Recuperacion Salvamento a guardar o actualizar
	 */
	public void guardarRecuperacionSalvamento(RecuperacionSalvamento salvamento);
	
	/**
	 * Cancelar una recuperacion de salvamento
	 * @param id
	 * @param motivo
	 */
	public void cancelarRecuperacion(Long id, String motivo);
	
	/**
	 * Obtener una recuperacion de salvamento por el id estimacion al que esta relacionada. Para buscar una recuperacion por su ID utilizar el metodo de busqueda de RecuperacionService.obtenerRecuperacion.
	 * @param estimacionId
	 * @return
	 */
	public RecuperacionSalvamento obtenerRecuperacionSalvamento(Long estimacionId);
	
	/**
	 * Inactivar una recuperacion de salvamento por su ID
	 * @param id
	 * @param causaActivacion Puede ser <code>null</code> y sirve para describir el motivo para el cambio de estatus.
	 */
	public void inactivarRecuperacionSalvamento(Long id, String causaInactivacion);
	
	/**
	 * Reactivar una recuparacion de salvamento por su ID
	 * @param id
	 * @param causaActivacion Puede ser <code>null</code> y sirve para describir el motivo para el cambio de estatus.
	 */
	public void reactivarRecuperacionSalvamento(Long id, String causaActivacion);
	
	/**
	 * Retornar una recuperacion de salvamento por su ID. El metodo manda a llamar obtenerRecuperacion de la clase padre y luego complementa el objeto
	 * @param id
	 * @return
	 */
	public RecuperacionSalvamento obtenerRecuperacionPorId(Long id);
	
	
	public Map<Long,String> obtenerCompradores();
	
	
	public void editarVentaSalvamento(Long recuperacionId, VentaSalvamento ventaSalvamento);
	
	/**
	 * Salva la venta de salvamento de una recuperacion 
	 * @param recuperacionId
	 * @param ventaSalvamento
	 */
	public void guardarVentaSalvamento(Long recuperacionId ,VentaSalvamento ventaSalvamento);
	
	public VentaSalvamento obtenerVentaSalvamentoInicial(Long id);

	public List<VentaSalvamento> obtenerVentasPorRecuperacion(Long recuperacionId);
	
	/***
	 * Solicita una prorroga de salvamento al perfil responsable
	 * @param recuperacionId
	 * @param fechaProrroga
	 * @param comentarios
	 */
	public void solicitarProrroga(Long recuperacionId, Date fechaProrroga, String comentarios);
	
	/**
	 * Autorizacion de la prorroga de salvamento 
	 * @param recuperacionId
	 * @param respuestaAutorizacion
	 * @param fechaProrroga
	 * @param comentarios
	 */
	public void autorizarProrroga(Long recuperacionId,String respuestaAutorizacion,Date fechaProrroga, String comentarios);
	
	/***
	 * Valida si es un usuario con permisos validos para autorizar la prorroga
	 * @param usuario
	 * @return
	 */
	public boolean esUsuarioValidoParaAutorizarProrroga(Usuario usuario);
	
	public void confirmarIngreso(Long idRecuperacion,Long bancoId, String cuenta, Date fechaConfirmacion,BigDecimal montoConfirmado);
	
	/**
	 * Obtener indicadores de una venta de salvamento con respecto a la recuperacion, reporte siniestro
	 * @param recuperacionId
	 * @return
	 */
	public IndicadoresVentaDTO obtenerIndicadoresVenta(Long recuperacionId);
	
	/**
	 * Obtener el MONTO total por estatus para el siniestro asociado a la recuperacion de salvamento de la venta
	 * @param siniestroId
	 * @param estatus
	 * @return
	 */
	public BigDecimal obtenerMontoRecuperacionSalvamento(Long siniestroId, EstatusRecuperacion estatus);
	
	/**
	 * Obtener el IVA total por estatus para el siniestro asociado a la recuperacion de salvamento de la venta
	 * @param siniestroId
	 * @param estatus
	 * @return
	 */
	public BigDecimal obtenerIvaRecuperacionSalvamento(Long siniestroId, EstatusRecuperacion estatus);
	
	/***
	 * Obtener venta activa de la recuperacion de salvamento
	 */
	public VentaSalvamento obtenerVentaSalvamentoActiva(Long recuperacionId );
	
	/***
	 * Obtener la recuperacion del salvamento
	 */
	public RecuperacionSalvamento obtenerRecuperacionSalvamentoById(Long recuperacionId);
	
	
	/***
	 * Obtiene los datos del vehiculo apartir de la recuperacion del salvamento
	 */
	public AutoIncisoReporteCabina obtenerDatosAutoByRecuperacionSalvamento(Recuperacion recuperacion);
	
	public void guardaSalvamentoConProvision(Long recuperacionId, RecuperacionSalvamento recuperacionDTO);
	
	public RecuperacionSalvamento salvarSalvamento(RecuperacionSalvamento recuperacionBase, RecuperacionSalvamento recuperacionDTO);
	
	public void invocaProvision(Long idRecuperacion);
	
	public void notificaVentasPorConfirmarIngresoProrroga();
	
	/***
	 * Cancela la venta activa, las referencias bancarias y el ingreso . Si no se requiere cancelar el ingreso cancelarIngreso se deberá enviar como false
	 * @param recuperacionId
	 * @param motivoCancelacion
	 * @param cancelarIngreso
	 */
	public void cancelarAdjudicacion(Long recuperacionId, String motivoCancelacion, boolean cancelarIngreso);
	
	/**
	 * Metodo para actualizar los valores de la recuperacion que son capturados en la determinacion
	 * y en la Recepcion de documentos de la determinacion
	 * @param indemnizacion
	 */
	public void actualizarInformacionRecuperacionSalvamentoEnIndemnizacion(Long idIndemnizacion);
	
	public void initialize();
	
}
