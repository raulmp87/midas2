package mx.com.afirme.midas2.service.siniestros.cabina.reportecabina;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.SiniestroCabinaDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;

@Local
public interface SiniestroCabinaService {
	public static final String TERMINO_SINIESTRO_CRUCERO_EFECTIVO  = "REC";
	public static final String TERMINO_SINIESTRO_JURIDICA_EFECTIVO = "REJ";
	public static final String TERMINO_SINIESTRO_CRUCERO_TDC       = "RTC";
	public static final String TERMINO_SINIESTRO_JURIDICA_TDC      = "RTJ";
	
	public SiniestroCabinaDTO obtenerSiniestroCabina(Long reporteCabinaId);
	public SiniestroCabinaDTO obtenerDetalleSiniestroCabina(SiniestroCabinaDTO siniestroDTO);
	public SiniestroCabina convertirSiniestro(SiniestroCabinaDTO siniestroCabina);
	public void guardar(SiniestroCabinaDTO siniestroCabina) throws Exception;
	public List<CatValorFijo> obtenerTerminosAjuste(String codigoTipoSiniestro, String codigoResponsabilidad);
	public AutoIncisoReporteCabina obtenerAutoIncisoByReporteCabina(Long reporteCabinaId);
	
	/**
	 * Valida si el inciso afectado puede modificar la causa, tipo de responsabilidad y termino de ajuste revisando si existen estimaciones guardadas.
	 * @param autoIncisoReporteId
	 * @param reporteCabinaId
	 * @param causaSiniestro
	 * @param tipoResponsabilidad
	 * @param terminoAjuste
	 * @return
	 */
	public List<String> permiteCambioCausaTipoTermino(Long autoIncisoReporteId, Long reporteCabinaId, 
			String causaSiniestro, String tipoResponsabilidad, String terminoAjuste);
	
	public void generarRecuperacion(SiniestroCabinaDTO siniestroDTO);	

	public String obtenerEstatusValuacion(Long valuacionId);
	
	public ReporteCabina terminarSiniestro(Long reporteCabinaId);
	
	public ReporteCabina rechazarSiniestro(Long reporteCabinaId);
	
	public ReporteCabina cancelarSiniestro(Long reporteCabinaId);
	
	public ReporteCabina reaperturarReporte(Long reporteCabinaId);
	
	public void validarCambioEstatus(Long reporteCabinaId) throws NegocioEJBExeption;
	
	public void guardarYValidaCias(SiniestroCabinaDTO siniestroCabina) throws Exception;
	
	
}
