package mx.com.afirme.midas2.service.envioxml;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.envioxml.EnvioFactura;
import mx.com.afirme.midas2.domain.envioxml.EnvioFacturaDet;


@Local
public interface EnvioFacturaService {
	
	final String CLAVEAGENTE = "AG";
	
	List<EnvioFactura> findByOperation(EnvioFactura envioFactura, Date fechaFinal, Date fechaInicial) throws Exception;
	List<EnvioFactura> ObtenerComplementosPagoFactura(String  uuidFactura);
	List<EnvioFactura> ObtenerComplementosPagoFactura(Long idLote);
	List<EnvioFacturaDet> findByIdEnvio(Long idEnvio)throws Exception;
	EnvioFactura saveEnvio(EnvioFactura envioFactura, String ext) throws Exception;
	EnvioFactura saveEnvio(EnvioFactura envioFactura) throws Exception;
	EnvioFactura saveEnvio(Object response, String origenEnvio, Long idOrigenEnvio, String cuentaAfectada, boolean save) throws Exception;
	long getIncrementedProperty(EnvioFactura envioFactura);
	EnvioFactura saveEnvio(EnvioFactura envioFactura, boolean save) throws Exception;
	void guardaDatosEnvio(EnvioFactura envioFactura);
	List<EnvioFactura> findByProperty(String  nombrePropiedad, Object valorPropiedad);
}
