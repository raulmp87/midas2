package mx.com.afirme.midas2.service.sapamis.sistemas;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisPTT;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;

/************************************************************************************
*
* Interfaz del servicio para el manejo de Transacciones del Objeto SapAmisPTT
* 
*  @author 			Eduardo Valentín Chávez Oliveros (Eduardosco)
*	Unidad de Fabrica: 	Avance Solutions Corporation S.A. de C.V.
*
************************************************************************************/
public interface SapAmisPTTService extends Serializable{
	public List<SapAmisPTT> obtenerPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina);
	public void sendRegSapAmis();
	public void sendRegSapAmis(String[] accesos);
	public void sendRegSapAmis(ParametrosConsulta parametrosConsulta);
	public void sendRegSapAmis(String[] accesos, ParametrosConsulta parametrosConsulta);
}