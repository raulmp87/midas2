package mx.com.afirme.midas2.service.sapamis.catalogos;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.dto.sapamis.catalogos.CatTipoBeneficio;

/************************************************************************************
 *
 * Interfaz del servicio para el manejo de Transacciones del Objeto CatTipoBeneficio
 * 
 *  @author 			Eduardo Valentin Chavez Oliveros (Eduardosco)
 *	Unidad de Fabrica: 	Avance Solutions Corporation S.A. de C.V.
 *
 ************************************************************************************/
public interface CatTipoBeneficioService extends Serializable{
	public List<CatTipoBeneficio> findByStatus(boolean status);
	public CatTipoBeneficio completeObject(CatTipoBeneficio object);
}