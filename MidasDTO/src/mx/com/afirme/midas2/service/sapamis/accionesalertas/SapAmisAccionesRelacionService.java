package mx.com.afirme.midas2.service.sapamis.accionesalertas;

import java.util.List;

import mx.com.afirme.midas2.dto.sapamis.accionesalertas.SapAmisAccionesRelacion;

/************************************************************************************
 *
 * Interfaz del servicio para el manejo de Transacciones del Objeto SapAmisAccionesRelacion
 * 
 *  @author 			Eduardo Valentin Chavez Oliveros (Eduardosco)
 *	Unidad de Fabrica: 	Avance Solutions Corporation S.A. de C.V.
 *
 ************************************************************************************/
public interface SapAmisAccionesRelacionService {
	public List<SapAmisAccionesRelacion> findAll();	
	public SapAmisAccionesRelacion findById(long id);	
	public List<SapAmisAccionesRelacion> findByStatus(boolean status);
	public SapAmisAccionesRelacion saveObject(SapAmisAccionesRelacion object);
	public List<SapAmisAccionesRelacion> findByProperty(String propertyName, Object property);
}