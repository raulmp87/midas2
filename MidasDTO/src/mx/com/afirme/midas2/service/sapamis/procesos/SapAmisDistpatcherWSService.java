package mx.com.afirme.midas2.service.sapamis.procesos;

import java.io.Serializable;

import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisEmision;
import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisPTT;
import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisPrevenciones;
import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisRechazos;
import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisSalvamento;
import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisSiniestros;

/*******************************************************************************
 * Nombre Interface: 	SapAmisDistpatcherWSService.
 * 
 * Descripcion: 		Se utiliza para el manejo de Transacciones entre los 
 * 						SERVICIOS WEB del SAP-AMIS y Midas-2.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
public interface SapAmisDistpatcherWSService extends Serializable{
	public void enviarEmision(SapAmisEmision emision);
	public void enviarEmision(SapAmisEmision emision, String[] accesos);
	public void enviarSiniestro(SapAmisSiniestros siniestro);
	public void enviarSiniestro(SapAmisSiniestros siniestro, String[] accesos);
	public void enviarPrevencion(SapAmisPrevenciones prevencion);
	public void enviarPrevencion(SapAmisPrevenciones prevencion, String[] accesos);
	public void enviarRechazo(SapAmisRechazos rechazo);
	public void enviarRechazo(SapAmisRechazos rechazo, String[] accesos);
	public void enviarPT(SapAmisPTT pt);
	public void enviarPT(SapAmisPTT pt, String[] accesos);
	public void enviarSalvamento(SapAmisSalvamento salvamento);
	public void enviarSalvamento(SapAmisSalvamento salvamento, String[] accesos);
}