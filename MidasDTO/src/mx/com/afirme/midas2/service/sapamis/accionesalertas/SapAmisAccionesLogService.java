package mx.com.afirme.midas2.service.sapamis.accionesalertas;

import java.util.List;

import mx.com.afirme.midas2.dto.sapamis.accionesalertas.SapAmisAccionesLog;

/************************************************************************************
 *
 * Interfaz del servicio para el manejo de Transacciones del Objeto SapAmisAccionesLog
 * 
 *  @author 			Eduardo Valentin Chavez Oliveros (Eduardosco)
 *	Unidad de Fabrica: 	Avance Solutions Corporation S.A. de C.V.
 *
 ************************************************************************************/
public interface SapAmisAccionesLogService {
	public List<SapAmisAccionesLog> findAll();	
	public SapAmisAccionesLog findById(long id);	
	public List<SapAmisAccionesLog> findByStatus(boolean status);
	public SapAmisAccionesLog saveObject(SapAmisAccionesLog sapAmisAccionesLog);
	public List<SapAmisAccionesLog> findByProperty(String propertyName, Object property);
}