package mx.com.afirme.midas2.service.sapamis.sistemas;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisBitacoras;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;

/************************************************************************************
 *
 * Interfaz del servicio para el manejo de Transacciones del Objeto SapAmisBitacoras
 * 
 *  @author 			Eduardo Valentin Chavez Oliveros (Eduardosco)
 *	Unidad de Fabrica: 	Avance Solutions Corporation S.A. de C.V.
 *
 ************************************************************************************/
public interface SapAmisBitacorasService extends Serializable{
	public SapAmisBitacoras saveObject(SapAmisBitacoras sapAmisBitacoras);
	public List<SapAmisBitacoras> obtenerCifras(ParametrosConsulta parametrosConsulta);
}