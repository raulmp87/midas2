package mx.com.afirme.midas2.service.sapamis.catalogos;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.dto.sapamis.catalogos.CatEstatusBitacora;

/************************************************************************************
 *
 * Interfaz del servicio para el manejo de Transacciones del Objeto CatEstatusBitacora
 * 
 *  @author 			Eduardo Valentin Chavez Oliveros (Eduardosco)
 *	Unidad de Fabrica: 	Avance Solutions Corporation S.A. de C.V.
 *
 ************************************************************************************/
public interface CatEstatusBitacoraService extends Serializable{
	public List<CatEstatusBitacora> findByStatusReg(boolean estatusReg);
}
