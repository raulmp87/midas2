package mx.com.afirme.midas2.service.sapamis.otros;

import java.util.Date;
import java.io.Serializable;
/*******************************************************************************
 * Nombre Interface: 	SapAmisUtilsService.
 * 
 * Descripcion: 		Se utiliza para la ejecucion de metodos simples y utiles
 * 						que se ocuparan a lo largo de los Procesos de Envios.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
public interface SapAmisUtilsService extends Serializable{
	public Date convertToDateStr(String fecha);
	public String convertToStrDate(Date fecha);
	public String[] obtenerAccesos();
}