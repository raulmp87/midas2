package mx.com.afirme.midas2.service.sapamis.catalogos;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.dto.sapamis.catalogos.CatCausaBeneficio;

/************************************************************************************
 *
 * Interfaz del servicio para el manejo de Transacciones del Objeto CatCausaBeneficio
 * 
 *  @author 			Eduardo Valentin Chavez Oliveros (Eduardosco)
 *	Unidad de Fabrica: 	Avance Solutions Corporation S.A. de C.V.
 *
 ************************************************************************************/
public interface CatCausaBeneficioService extends Serializable{
	public List<CatCausaBeneficio> findByStatus(boolean status);
	public CatCausaBeneficio completeObject(CatCausaBeneficio catCausaBeneficio);
}