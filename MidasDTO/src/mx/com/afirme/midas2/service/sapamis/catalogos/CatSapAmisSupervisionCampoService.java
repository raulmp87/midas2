package mx.com.afirme.midas2.service.sapamis.catalogos;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatSapAmisSupervisionCampo;

public interface CatSapAmisSupervisionCampoService extends Serializable{
	public boolean existeClaveAmis(Long claveAmis);
	public boolean guardar(CatSapAmisSupervisionCampo catSapAmisSupervisionCampo);
	public CatSapAmisSupervisionCampo obtenerPorId (Long id);
	public Map<String, String> obtenerCatalogo();
	public List<CatSapAmisSupervisionCampo> obtenerPorFiltros(CatSapAmisSupervisionCampo catSapAmisSupervisionCampo);
}