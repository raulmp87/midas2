package mx.com.afirme.midas2.service.sapamis.catalogos;

import java.io.Serializable;

import mx.com.afirme.midas2.dto.sapamis.catalogos.CatAlertasSapAmis;

/************************************************************************************
 *
 * Interfaz del servicio para el manejo de Transacciones del Objeto CatAlertasSapAmis
 * 
 *  @author 			Eduardo Valentin Chavez Oliveros (Eduardosco)
 *	Unidad de Fabrica: 	Avance Solutions Corporation S.A. de C.V.
 *
 ************************************************************************************/
public interface CatAlertasSapAmisService extends Serializable{
	public CatAlertasSapAmis findByDesc(String desc);
	public CatAlertasSapAmis completeObject(CatAlertasSapAmis catAlertasSapAmis);
}