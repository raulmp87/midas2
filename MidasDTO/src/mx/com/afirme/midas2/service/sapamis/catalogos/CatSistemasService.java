package mx.com.afirme.midas2.service.sapamis.catalogos;

import java.io.Serializable;

import mx.com.afirme.midas2.dto.sapamis.catalogos.CatSistemas;

/************************************************************************************
 *
 * Interfaz del servicio para el manejo de Transacciones del Objeto CatSistemas
 * 
 *  @author 			Eduardo Valentin Chavez Oliveros (Eduardosco)
 *	Unidad de Fabrica: 	Avance Solutions Corporation S.A. de C.V.
 *
 ************************************************************************************/
public interface CatSistemasService extends Serializable{ 
	public CatSistemas completeObject(CatSistemas catSistemas);
	public boolean estatusProceso(long idSistema, int tipoProceso);
	public boolean guardarRegistro(CatSistemas catSistemas);
	public Long obtenerIdSistemaPorDescripcion(String descSistema);
}