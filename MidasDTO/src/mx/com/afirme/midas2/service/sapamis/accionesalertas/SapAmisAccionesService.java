package mx.com.afirme.midas2.service.sapamis.accionesalertas;

import java.util.List;

import mx.com.afirme.midas2.dto.sapamis.accionesalertas.SapAmisAcciones;

/************************************************************************************
 *
 * Interfaz del servicio para el manejo de Transacciones del Objeto SapAmisAcciones
 * 
 *  @author 			Eduardo Valentin Chavez Oliveros (Eduardosco)
 *	Unidad de Fabrica: 	Avance Solutions Corporation S.A. de C.V.
 *
 ************************************************************************************/
public interface SapAmisAccionesService {
	public List<SapAmisAcciones> findAll();	
	public SapAmisAcciones findById(long id);	
	public List<SapAmisAcciones> findByStatus(boolean status);
	public SapAmisAcciones saveObject(SapAmisAcciones sapAmisAcciones);
	public List<SapAmisAcciones> findByProperty(String propertyName, Object property);
}