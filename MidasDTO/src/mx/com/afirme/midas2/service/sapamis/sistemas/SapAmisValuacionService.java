package mx.com.afirme.midas2.service.sapamis.sistemas;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisValuacion;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;

/************************************************************************************
*
* Interfaz del servicio para el manejo de Transacciones del Objeto SapAmisValuacion
* 
*  @author 			Eduardo Valentín Chávez Oliveros (Eduardosco)
*	Unidad de Fabrica: 	Avance Solutions Corporation S.A. de C.V.
*
************************************************************************************/
public interface SapAmisValuacionService extends Serializable{
	public List<SapAmisValuacion> obtenerPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina);
}