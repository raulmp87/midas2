package mx.com.afirme.midas2.service.sapamis.catalogos;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.dto.sapamis.catalogos.CatBeneficioRelacion;

/************************************************************************************
 *
 * Interfaz del servicio para el manejo de Transacciones del Objeto CatBeneficioRelacion
 * 
 *  @author 			Eduardo Valentin Chavez Oliveros (Eduardosco)
 *	Unidad de Fabrica: 	Avance Solutions Corporation S.A. de C.V.
 *
 ************************************************************************************/
public interface CatBeneficioRelacionService extends Serializable{
	public List<CatBeneficioRelacion> findByStatus(boolean status);
	public CatBeneficioRelacion completeObject(CatBeneficioRelacion catBeneficioRelacion);
}