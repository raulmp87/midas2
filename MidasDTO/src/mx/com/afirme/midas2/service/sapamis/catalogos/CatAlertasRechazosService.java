package mx.com.afirme.midas2.service.sapamis.catalogos;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.dto.sapamis.catalogos.CatAlertasRechazos;

/************************************************************************************
 *
 * Interfaz del servicio para el manejo de Transacciones del Objeto CatAlertasRechazos
 * 
 *  @author 			Eduardo Valentin Chavez Oliveros (Eduardosco)
 *	Unidad de Fabrica: 	Avance Solutions Corporation S.A. de C.V.
 *
 ************************************************************************************/
public interface CatAlertasRechazosService extends Serializable{
	public List<CatAlertasRechazos> findAll();
	public CatAlertasRechazos findById(long id);
	public List<CatAlertasRechazos> findByStatus(boolean status);
}