package mx.com.afirme.midas2.service.sapamis.procesos;

import java.io.Serializable;

public interface SapAmisEjecucionesJOBSService extends Serializable{
	public void todo();
	public void initialize();
}
