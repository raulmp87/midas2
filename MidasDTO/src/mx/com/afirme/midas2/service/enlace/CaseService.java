package mx.com.afirme.midas2.service.enlace;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dto.enlace.CaseConversationDTO;
import mx.com.afirme.midas2.dto.enlace.CaseDTO;
import mx.com.afirme.midas2.dto.enlace.CaseDeviationDTO;
import mx.com.afirme.midas2.dto.enlace.CaseTypeDTO;

@Local
public interface CaseService {
	/**
	 * Obtiene una lista de los casos activos por usuario, es decir los casos
	 * que tengan estatus de CaseDTO.StatusEnum.NEW o
	 * CaseDTO.StatusEnum.ASSIGNED
	 * 
	 * @param createUser
	 * @return
	 */
	public List<CaseDTO> getActiveCaseByUser(String createUser);

	/**
	 * Obtiene el caso por su identificador
	 * 
	 * @param caseId
	 * @return
	 */
	public CaseDTO findById(Long caseId);

	/**
	 * Crea un nuevo caso
	 * 
	 * @param caseTypeId
	 *            Tipo de caso
	 * @param severity
	 *            Severidad
	 * @param description
	 *            Descripcion
	 * @param createUser
	 *            Usuario
	 * @return
	 */
	public Long newCase(Long caseTypeId, Integer severity, String description,
			String createUser);

	/**
	 * Asigna un caso para su atención dependiendo de su severidad
	 * 
	 * @param caseId
	 */
	public Usuario assign(Long caseId);

	/**
	 * Obtiene el usuario al que se le asignará un caso dependiendo de si tipo
	 * 
	 * @param userType
	 * @param caseTypeId
	 * @return
	 */
	public String getAssigned(Integer userType, Long caseTypeId);

	/**
	 * Indica si el usuario está marcado como VIP
	 * 
	 * @param createUser
	 * @return
	 */
	public boolean getVip(String createUser);

	/**
	 * Cambia el caso de estatus, Nuevo a Asignado, Asignado a Cerrado
	 * 
	 * @param caseId
	 * @param newStatus
	 * @param createUser
	 */
	public void changeStatus(Long caseId, CaseDTO.StatusEnum newStatus,
			String createUser);

	/**
	 * Agrega un comentario al caso
	 * 
	 * @param caseId
	 * @param message
	 * @param createUser
	 */
	public void addMessage(Long caseId, String message, String createUser);

	/**
	 * Obtiene todos los comentarios de un caso
	 * 
	 * @param caseId
	 * @return
	 */
	public List<CaseConversationDTO> getMessages(Long caseId);

	/**
	 * Obtiene los diferentes tipos de casos que se pueden atender
	 * 
	 * @return
	 */
	public List<CaseTypeDTO> getCaseTypes();

	/**
	 * Se usa para desviar la atención de un caso a otra persona
	 * 
	 * @param createUser
	 * @param deviationUser
	 * @param iniDate
	 * @param endDate
	 * @return
	 */
	public CaseDeviationDTO addDeviation(String createUser,
			String deviationUser, Date iniDate, Date endDate);

	/**
	 * Obtiene la lista de personas a las que se le puede desviar la atención de
	 * un caso
	 * 
	 * @param createUser
	 * @return
	 */
	public List<Usuario> getDeviationUserList(String createUser);

	/**
	 * Obtiene la desviación de un caso por ausencia
	 * 
	 * @param createUser
	 * @return
	 */
	public CaseDeviationDTO getDeviation(String createUser);

	/**
	 * Finaliza o cierra un caso
	 * 
	 * @param caseId
	 * @param createUser
	 */
	public void closeCase(Long caseId, String createUser);

	/**
	 * Reasigna el caso a otra persona
	 * 
	 * @param caseId
	 * @param reassignUser
	 */
	public void reassign(Long caseId, String reassignUser);
	
	/**
	 * Indica si el usuario tiene rol de administrador de enlace
	 * @param userName
	 * @return
	 */
	public boolean isManager(String userName);
	
	/**
	 * Envia notificación a dispositivos Android
	 * 
	 * @param message
	 * @param senderUserName
	 * @param receiptUserName
	 * @param evento
	 * @param data
	 * @param title
	 */
	public void advertise(String message, String senderUserName, String receiptUserName, String evento, Map<String, String> data, String title);
	
	/**
	 * Envia notificación a dispositivos iOS
	 * 
	 * @param message
	 * @param senderUserName
	 * @param receiptUserName
	 * @param evento
	 * @param data
	 * @param registrationId
	 * @param title
	 */
	public void advertiseAPN(String message, String senderUserName, String receiptUserName, String evento, 
			Map<String, String> data, String registrationId, String title);
}
