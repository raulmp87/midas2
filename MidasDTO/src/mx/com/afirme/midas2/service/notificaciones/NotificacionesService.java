package mx.com.afirme.midas2.service.notificaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.notificaciones.ConfiguracionNotificaciones;
import mx.com.afirme.midas2.domain.notificaciones.DestinatariosNotificaciones;
import mx.com.afirme.midas2.domain.sistema.Evento;

@Local
public interface NotificacionesService {
	
	public Long saveConfigNotificaciones(ConfiguracionNotificaciones configNotificaciones);
	
	public List<ConfiguracionNotificaciones> findAllConfig();
	
	public ConfiguracionNotificaciones findByIdConfig(Long id);
	
	public List<DestinatariosNotificaciones>findDestinatariosByIdConfig(ConfiguracionNotificaciones idConfiguracion);
	
	public List<ConfiguracionNotificaciones> findByFilters(ConfiguracionNotificaciones configNotificaciones);
	
	public void eliminarNotificacion(ConfiguracionNotificaciones configNotificaciones);
		
	public void notificarNuevoElementoEmision(Evento eventoNuevoElementoEmision);
	

}
