package mx.com.afirme.midas2.service.tarea;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.tarea.EmisionPendiente;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;

@Local
public interface EmisionPendienteService {

	public void procesaCancelacionPolizaDanios();
	public void procesaRehabilitacionPolizaDanios();
	public void procesaCancelacionEndosoDanios();
	public void procesaRehabilitacionEndosoDanios();
	public void procesaBajaIncisoPerdidaTotalAutos();
	public void procesaRehabilitacionIncisoAutos();
	//public void procesaCancelacionPolizaAutos();
	public void procesaCancelacionPolizaAutos(int dias);
	public void procesaRehabilitacionPolizaAutos();
	//public void procesaCancelacionEndosoAutos();
	public void procesaCancelacionEndosoAutos(int dias);
	public void procesaRehabilitacionEndosoAutos();
	public void procesaCancelacionIncisoAutos();
	
	public Long obtenerTotalPaginacionCancelacionesPendientesAutos(EmisionPendiente filtro);
	public List<EmisionPendiente> buscarCancelacionesPendientesAutos(EmisionPendiente filtro);
	public void actualizaBloqueo (Long emisionPendienteId, Short bloqueo);
	public void emitePendiente (Long emisionPendienteId);
	public TransporteImpresionDTO getExcelCancelacionesPendientesAutos(EmisionPendiente filtro);
	public void removeInProcessForEndosoAutomatico(BigDecimal polizaId);
	
	public void procesaEndosoMigracion(Long idCotizacion, String claveTipoEndoso);
	
	public Map<String, Long> monitorearAvanceEmisionPendiente(EmisionPendiente filtro);
	
	public void generarEndosoDesagrupacionRecibosAutomatico(BigDecimal idToPoliza, Date fechaInicioVigencia, IncisoContinuity incisoContinuity);
	
	public void generarEndosoPerdidaTotalAutomatico(BigDecimal idToPoliza, String tipoIndemnizacion, Date fechaInicioVigencia, 
			IncisoContinuity incisoContinuity, Long idSiniestroPT);
	
	public void initialize();
	
}
