package mx.com.afirme.midas2.service.tarea;

import javax.ejb.Local;

@Local
public interface TareaGeneraReporteDxPService {
	
	public void generaArchivoDxP();
	
}
