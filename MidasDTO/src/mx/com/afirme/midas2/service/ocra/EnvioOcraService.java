package mx.com.afirme.midas2.service.ocra;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.ocra.AmisUbicacion;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.IncisoReporteCabina;
import mx.com.afirme.midas2.service.sistema.comm.CommService;

import java.util.Date;
import java.util.Map;

@Local
public interface EnvioOcraService extends CommService{
	
	public String enviarAltaRoboOCRAServicio(Long idReporteCabina);
	
	public String reenvioAltaRoboOCRAServicio(); 
	
	public void   guardaLog(String msnEnvio,String msnRespuesta, Date envio, Date Respueta,Long reporteCabina, String observaciones);  
	
	/***
	 * Valida si el reporte ya fue enviado a Ocra
	 * @param idReporteSin
	 * @return
	 */
	public boolean isPendienteOcra(String idReporteSin);
	
	/**
	 * Obtiene la ubicacion amis
	 * @param pais
	 * @param estado
	 * @param ciudad
	 * @return AmisUbicacion
	 */
	public AmisUbicacion convertirUbicacionAMIS(String pais, String estado, String ciudad);
	
	/**
	 * Convierte los datos de auto Midas a equivalencia Ocra 	
	 * @param incisoReporteCabina
	 * @return
	 */
	public Map<String,String> convertirAutoOcra(IncisoReporteCabina incisoReporteCabina);

}
