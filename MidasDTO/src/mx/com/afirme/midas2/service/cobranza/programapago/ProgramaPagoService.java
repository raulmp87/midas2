package mx.com.afirme.midas2.service.cobranza.programapago;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Transient;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas2.domain.catalogos.DomicilioImpresion;
import mx.com.afirme.midas2.domain.cobranza.programapago.ToDesgloseRecibo;
import mx.com.afirme.midas2.domain.cobranza.programapago.ToProgPago;
import mx.com.afirme.midas2.domain.cobranza.programapago.ToRecibo;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;

public interface ProgramaPagoService {
   public class Saldo{
		private BigDecimal impPrimaNeta          = BigDecimal.ZERO;
		private BigDecimal impBonComPN           = BigDecimal.ZERO;
		private BigDecimal impBonComRPF          = BigDecimal.ZERO;
		private BigDecimal impBonComis           = BigDecimal.ZERO;
		private BigDecimal impDerechos           = BigDecimal.ZERO;
		private BigDecimal impRcgosPagoFR        = BigDecimal.ZERO;
		private BigDecimal impIVA                = BigDecimal.ZERO;
		private BigDecimal impOtrosImptos        = BigDecimal.ZERO;
		private BigDecimal impPrimaTotal         = BigDecimal.ZERO;
		private BigDecimal impComAgtPN           = BigDecimal.ZERO;
		private BigDecimal impComAgtRPF          = BigDecimal.ZERO;
		private BigDecimal impComAgt             = BigDecimal.ZERO;
		private BigDecimal impComSupPN           = BigDecimal.ZERO;
		private BigDecimal impComSupRPF          = BigDecimal.ZERO;
		private BigDecimal impComSup             = BigDecimal.ZERO;
		private BigDecimal impSobreComAgt        = BigDecimal.ZERO;
		private BigDecimal impSobreComProm       = BigDecimal.ZERO;
		private BigDecimal impCesionDerechosAgt  = BigDecimal.ZERO;
		private BigDecimal impCesionDerechosProm = BigDecimal.ZERO;
		private BigDecimal impSobreComUdiAgt     = BigDecimal.ZERO;
		private BigDecimal impSobreComUdiProm    = BigDecimal.ZERO;
		private BigDecimal impBonoAgt            = BigDecimal.ZERO;
		private BigDecimal impBomoProm           = BigDecimal.ZERO;
		private BigDecimal porcentajePagoFraccionado           = BigDecimal.ZERO;
		
		
		
		public BigDecimal getImpPrimaNeta() {
			return impPrimaNeta;
		}
		public void setImpPrimaNeta(BigDecimal impPrimaNeta) {
			this.impPrimaNeta = impPrimaNeta;
		}
		public BigDecimal getImpBonComPN() {
			return impBonComPN;
		}
		public void setImpBonComPN(BigDecimal impBonComPN) {
			this.impBonComPN = impBonComPN;
		}
		public BigDecimal getImpBonComRPF() {
			return impBonComRPF;
		}
		public void setImpBonComRPF(BigDecimal impBonComRPF) {
			this.impBonComRPF = impBonComRPF;
		}
		public BigDecimal getImpBonComis() {
			return impBonComis;
		}
		public void setImpBonComis(BigDecimal impBonComis) {
			this.impBonComis = impBonComis;
		}
		public BigDecimal getImpDerechos() {
			return impDerechos;
		}
		public void setImpDerechos(BigDecimal impDerechos) {
			this.impDerechos = impDerechos;
		}
		public BigDecimal getImpRcgosPagoFR() {
			return impRcgosPagoFR;
		}
		public void setImpRcgosPagoFR(BigDecimal impRcgosPagoFR) {
			this.impRcgosPagoFR = impRcgosPagoFR;
		}
		public BigDecimal getImpIVA() {
			return impIVA;
		}
		public void setImpIVA(BigDecimal impIVA) {
			this.impIVA = impIVA;
		}
		public BigDecimal getImpOtrosImptos() {
			return impOtrosImptos;
		}
		public void setImpOtrosImptos(BigDecimal impOtrosImptos) {
			this.impOtrosImptos = impOtrosImptos;
		}
		public BigDecimal getImpPrimaTotal() {
			return impPrimaTotal;
		}
		public void setImpPrimaTotal(BigDecimal impPrimaTotal) {
			this.impPrimaTotal = impPrimaTotal;
		}
		public BigDecimal getImpComAgtPN() {
			return impComAgtPN;
		}
		public void setImpComAgtPN(BigDecimal impComAgtPN) {
			this.impComAgtPN = impComAgtPN;
		}
		public BigDecimal getImpComAgtRPF() {
			return impComAgtRPF;
		}
		public void setImpComAgtRPF(BigDecimal impComAgtRPF) {
			this.impComAgtRPF = impComAgtRPF;
		}
		public BigDecimal getImpComAgt() {
			return impComAgt;
		}
		public void setImpComAgt(BigDecimal impComAgt) {
			this.impComAgt = impComAgt;
		}
		public BigDecimal getImpComSupPN() {
			return impComSupPN;
		}
		public void setImpComSupPN(BigDecimal impComSupPN) {
			this.impComSupPN = impComSupPN;
		}
		public BigDecimal getImpComSupRPF() {
			return impComSupRPF;
		}
		public void setImpComSupRPF(BigDecimal impComSupRPF) {
			this.impComSupRPF = impComSupRPF;
		}
		public BigDecimal getImpComSup() {
			return impComSup;
		}
		public void setImpComSup(BigDecimal impComSup) {
			this.impComSup = impComSup;
		}
		public BigDecimal getImpSobreComAgt() {
			return impSobreComAgt;
		}
		public void setImpSobreComAgt(BigDecimal impSobreComAgt) {
			this.impSobreComAgt = impSobreComAgt;
		}
		public BigDecimal getImpSobreComProm() {
			return impSobreComProm;
		}
		public void setImpSobreComProm(BigDecimal impSobreComProm) {
			this.impSobreComProm = impSobreComProm;
		}
		public BigDecimal getImpCesionDerechosAgt() {
			return impCesionDerechosAgt;
		}
		public void setImpCesionDerechosAgt(BigDecimal impCesionDerechosAgt) {
			this.impCesionDerechosAgt = impCesionDerechosAgt;
		}
		public BigDecimal getImpCesionDerechosProm() {
			return impCesionDerechosProm;
		}
		public void setImpCesionDerechosProm(BigDecimal impCesionDerechosProm) {
			this.impCesionDerechosProm = impCesionDerechosProm;
		}
		public BigDecimal getImpSobreComUdiAgt() {
			return impSobreComUdiAgt;
		}
		public void setImpSobreComUdiAgt(BigDecimal impSobreComUdiAgt) {
			this.impSobreComUdiAgt = impSobreComUdiAgt;
		}
		public BigDecimal getImpSobreComUdiProm() {
			return impSobreComUdiProm;
		}
		public void setImpSobreComUdiProm(BigDecimal impSobreComUdiProm) {
			this.impSobreComUdiProm = impSobreComUdiProm;
		}
		public BigDecimal getImpBonoAgt() {
			return impBonoAgt;
		}
		public void setImpBonoAgt(BigDecimal impBonoAgt) {
			this.impBonoAgt = impBonoAgt;
		}
		public BigDecimal getImpBomoProm() {
			return impBomoProm;
		}
		public void setImpBomoProm(BigDecimal impBomoProm) {
			this.impBomoProm = impBomoProm;
		}
		public BigDecimal getPorcentajePagoFraccionado() {
			return porcentajePagoFraccionado;
		}
		public void setPorcentajePagoFraccionado(BigDecimal porcentajePagoFraccionado) {
			this.porcentajePagoFraccionado = porcentajePagoFraccionado;
		}
		
		
	}
   
   
   public class ContenedorDatosProgramaPago {
	    private static final long serialVersionUID = 6424503649477306725L;
		private ClienteGenericoDTO cliente;
		private DomicilioImpresion domicilioCliente;
		private Integer numProgPago;
		private Saldo saldo;
		private List<ToRecibo> recibos;
		private Long idContratante;
		private String nombreContratante;
		private BigDecimal idToCotizacion;
		
		public ClienteGenericoDTO getCliente() {
			return cliente;
		}
		public void setCliente(ClienteGenericoDTO cliente) {
			this.cliente = cliente;
		}
		public DomicilioImpresion getDomicilioCliente() {
			return domicilioCliente;
		}
		public void setDomicilioCliente(DomicilioImpresion domicilioCliente) {
			this.domicilioCliente = domicilioCliente;
		}
		public Saldo getSaldo() {
			return saldo;
		}
		public void setSaldo(Saldo saldo) {
			this.saldo = saldo;
		}
		public Integer getNumProgPago() {
			return numProgPago;
		}
		public void setNumProgPago(Integer numProgPago) {
			this.numProgPago = numProgPago;
		}
		public List<ToRecibo> getRecibos() {
			return recibos;
		}
		public void setRecibos(List<ToRecibo> recibos) {
			this.recibos = recibos;
		}
		public Long getIdContratante() {
			return idContratante;
		}
		public void setIdContratante(Long idContratante) {
			this.idContratante = idContratante;
		}
		public String getNombreContratante() {
			return nombreContratante;
		}
		public void setNombreContratante(String nombreContratante) {
			this.nombreContratante = nombreContratante;
		}
		public BigDecimal getIdToCotizacion() {
			return idToCotizacion;
		}
		public void setIdToCotizacion(BigDecimal idToCotizacion) {
			this.idToCotizacion = idToCotizacion;
		}

	}


   
	/**
	 * Obtener programa de pago por ID
	 * @param id
	 * @return
	 */
	public ToProgPago getProgramaPago(Long id);
	
	/**
	 * Obtener programa de pago por llave de negocio (id cotizacion y numero de programa de pago)
	 * @param idToCotizacion
	 * @param numProgPago
	 * @return
	 */
	
	public ToProgPago getProgramaPago(BigDecimal idToCotizacion, Integer numProgPago);
	
	/**
	 * Obtener programas de pago para una cotizacion
	 * @param idToCotizacion
	 * @return
	 */
	public List<ToProgPago> getProgramaPago(BigDecimal idToCotizacion);
	

	/**
	 * Obtener recibo por id
	 * @param id id del recibo
	 * @return
	 */
	public ToRecibo getRecibo(Long id);
	
	/**
	 * Obtener recibo por llave de negocio (id programa pago, numero exhibicion)
	 * @param idProgPago
	 * @param numeroExhibicion
	 * @return
	 */
	public ToRecibo getRecibo(Long idProgPago, Integer numeroExhibicion);
	
	/**
	 * Obtener listado de recibos para la cotizacion
	 * @param idToCotizacion
	 * @return
	 */
	public List<ToRecibo> getRecibo(BigDecimal idToCotizacion);
	
	/**
	 * Obtener listado de recibos para una cotizacion y un programa  de pagos
	 * @param idToCotizacion
	 * @param idProgPago id del programa de pago
	 * @return
	 */
	public List<ToRecibo> getRecibo(BigDecimal idToCotizacion, Long idProgPago);
	
	/**
	 * Obtener listado de recibos para un programa de pago
	 * @param idProgPago id del programa de pagos
	 * @return
	 */
	public List<ToRecibo> getReciboDePrograma(Long idProgPago);
	
	/**
	 * Obtener desglose por id
	 * @param id id del desglose
	 * @return
	 */
	public ToDesgloseRecibo getDesglose(Long id);
	
	/**
	 * Obtener el desglose de un recibo
	 * @param idRecibo id del recibo
	 * @return
	 */
	public List<ToDesgloseRecibo> getDesgloseRecibo(Long idRecibo);
	
	/**
	 * Guardar programa de pago
	 * @param progPago
	 */
	public void guardar(ToProgPago progPago);
	
	/**
	 * Guardar recibo
	 * @param recibo
	 */
	public void guardar(ToRecibo recibo);
	
	/**
	 * Guardar listado de recibos
	 * @param recibos
	 */
	public void guardar(List<ToRecibo> recibos, BigDecimal idToCotizacion);
	
	/**
	 * Eliminar programa de pago
	 * @param idProgPago
	 */
	public void eliminarProgPago(Long idProgPago);
	
	/**
	 * Eliminar recibo
	 * @param idToRecibo
	 */
	public void eliminarRecibo(Long idToRecibo);
	
	/**
	 * Genera el programa de pago default para la cotizacion reemplazando lo que está actualmente guardado
	 * @param idToCotizacion
	 * @return
	 */
	public List<ToProgPago> inicializarProgramaPago(Long idToCotizacion);
	
	/**
	 * Generar recibos default para un programa de pagos reemplazando lo que está actualmente guardado
	 * @param idRecibo
	 * @return
	 */
	public List<ToRecibo> inicializaRecibos(Long idProgPago);
	
	/**
	 * Obtener saldo original de la cotizacion
	 * @param idProgPago
	 * @return
	 */
	public List<Saldo> obtenerSaldoOriginal(Long idCotizacion);
	
	/**
	 * Obtener saldo recuotificable de la cotizacion 
	 * @param idProgPago
	 * @return
	 */
	public List<Saldo> obtenerSaldoProgramaPago(Long idCotizacion);
	
	/**
	 * Obtener saldo para un recibo
	 * @param idRecibo
	 * @return
	 */

	public ToProgPago getPrima(ToProgPago progPago);
	/**
	 * Obtener un programa de pago completo con sus primas
	 * @param progPago
	 * @return
	 */
	
	public List<Saldo> obtenerSaldoRecibos(Long idRecibo);
	
//	public Saldo obtenerSaldoTotal(BigDecimal idToCotizacion);
	
	public Saldo obtenerSaldoOriginal(BigDecimal idToCotizacion);
	
	public Saldo obtenerSaldoPeriodoPago(Long idProgPago);
	
	public Saldo obtenerSaldoActual(BigDecimal idToCotizacion);
//	
	public Saldo obtenerSaldoDiferencia(BigDecimal idToCotizacion);
	
	public ContenedorDatosProgramaPago obtenerDatosCliente(Long idProgPago, BigDecimal idToCotizacion);
	
	public TransporteImpresionDTO imprimirProgramaPagos( BigDecimal idToCotizacion );
	
	public Map<String,String> obtenerPersonasAseguradasCot(BigDecimal idToCotizacion);
	
	public Boolean actualizarAutoIncisoCot(String idAutoIncisoCot, ClienteGenericoDTO idCliente);
	
	public Boolean validaClineteOriginalCot(BigDecimal idToCotizacion, ClienteGenericoDTO cliente);
	
	public Map<String, String> simulaEmitePoliza(CotizacionDTO cotizacionDTO, String canalVenta, Integer recuotificaFlag);
	
	public Saldo obtenerSaldoOriginal(CotizacionDTO cotizacion, BigDecimal idProgPago, Long idReferencia);
	
	public Saldo obtenerSaldoPeriodoPago(BigDecimal idToCotizacion);
	
	public Saldo obtenerSaldoDiferencia(CotizacionDTO cotizacionDTO);
	
	public void guardar(List<ToRecibo> recibos, Long progPago);
	
	public Map<String, String> agregarProgramaPago(ToProgPago dto, BigDecimal formaPago);
	
	public Saldo obtenerSaldoProgoPagoIn(BigDecimal idProgPago);
	
	public Saldo obtenerSaldoRecibosIn(BigDecimal idProgPago);
	
	public Saldo obtenerSaldoDiferenciaInciso(BigDecimal idToprogPago);
	
	Saldo obtenerSaldoOriginalPorInciso(CotizacionDTO cotizacion, BigDecimal idProgPago, Long numeroInciso);
	
	boolean validarPrevioDeRecibos(BigDecimal idToCotizacion);
	
	boolean validaRolRecuotificacion(String userName, String rol);
	
	public Integer actualizarClienteInciso(String identificador, BigDecimal idInciso, BigDecimal idCliente, String nombreCliente);

	public Integer obtenerParametroGeneral(BigDecimal idGrupoParametro, BigDecimal codigoGrupoParametro);
}
