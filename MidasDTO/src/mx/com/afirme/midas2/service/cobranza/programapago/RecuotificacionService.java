package mx.com.afirme.midas2.service.cobranza.programapago;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

import javax.ejb.Local;

@Local
public interface RecuotificacionService extends Serializable {
	
	public enum TipoRecuotificacion {AUTOMATICA, MANUAL}
	
	public enum AccionRecuotificacion {NO_RECUOTIFICAR, GENERAR_AUTOMATICA, RECUOTIFICA_A_ORIGINAL}
	
	public Map<String, Object> generaProgramasPago(BigDecimal idToCotizacion, Integer banderaRecuotifica, String tipoMov, BigDecimal idToSolicitud); 
	
	public Map<String, Object> generaProgramasPagoEndoso(BigDecimal idToSolicitud,String precuotifica);
	
	Map<String, Object> validaTotalesDeCotizacionSalir(BigDecimal idToCotizacion);
	
	Map<String, Object> generaProgramasPagoEndosoAP(BigDecimal idContinuity, BigDecimal tipoFormaPago, String precuotifica, BigDecimal numeroEndoso);
	
	boolean validarPrevioDeRecibos(BigDecimal idToCotizacion);
	 
	Map<String, Object> validaPrimasDeEndoso(BigDecimal idToSolicitud);
	
	BigDecimal obtenerSolicitudDeContinuidad(BigDecimal idContinuity);
	
	public Integer validarEstatusRecuotificacion(BigDecimal idGrupoParametro, BigDecimal codigoParametro);
	
	/****** 20170212 ****/
	
	/**
	 * Marcar una cotizacion como recuotificada
	 * @param idToCotizacion
	 * @param tipo AUTOMATICA o MANUAL
	 */
	public void marcarCotizacionRecuotificada(BigDecimal idToCotizacion, TipoRecuotificacion tipo);
	
	/**
	 * Obtener si una cotizacion esta recuotificada por AUTOMATICA o MANUAL
	 * @param idToCotizacion
	 * @param tipo AUTOMATICA o MANUAL
	 */
	public boolean validarCotizacionRecuotificada(BigDecimal idToCotizacion, TipoRecuotificacion tipo);
	
	/**
	 * Marcar una cotizacion como recuotificada
	 * @param idToCotizacion
	 * @param tipo AUTOMATICA o MANUAL
	 * @param valor <true> or <false>
	 */
	public void marcarCotizacionRecuotificada(BigDecimal idToCotizacion, TipoRecuotificacion tipo, Boolean valor);
	
	public boolean validarAplicaRecuotificacion(BigDecimal idToCotizacion);
	
	public double obtenerMontoPrimerReciboRecuotificado(BigDecimal idToCotizacion);
}
