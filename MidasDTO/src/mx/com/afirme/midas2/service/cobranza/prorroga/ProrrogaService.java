package mx.com.afirme.midas2.service.cobranza.prorroga;

import java.util.List;

import mx.com.afirme.midas2.domain.cobranza.prorroga.ToProrroga;

public interface ProrrogaService {

	public List<ToProrroga> listarPorIdCotizacion(Long idCotizacion, Long inciso);
}
