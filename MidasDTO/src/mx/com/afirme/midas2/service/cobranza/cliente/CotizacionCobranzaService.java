package mx.com.afirme.midas2.service.cobranza.cliente;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas2.domain.cobranza.cliente.CotizacionCobranza;

@Local
public interface CotizacionCobranzaService {
	CotizacionCobranza findFirstByProperty(String propertyName, Object propertyValue);
	List<CotizacionCobranza> findByProperty(String propertyName, Object propertyValue);
	void guardarDatosCotizacionCobranza(CotizacionDTO cotizacionDTO, Integer idConductoCobro, Integer idMedioPago, boolean clienteEsTitular);
}