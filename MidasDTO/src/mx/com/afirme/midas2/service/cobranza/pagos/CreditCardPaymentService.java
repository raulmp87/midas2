package mx.com.afirme.midas2.service.cobranza.pagos;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.domain.cobranza.pagos.PROSAException;
import mx.com.afirme.midas2.domain.cobranza.pagos.PROSATransaction;
import mx.com.afirme.midas2.domain.cobranza.pagos.PROSATransactionResponse;
import mx.com.afirme.midas2.domain.cobranza.pagos.RecibosFoleados;
import mx.com.afirme.midas2.domain.cobranza.pagos.RespuestaPagoDTO;

@Local
public interface CreditCardPaymentService {
	
	static final String URL_PROSA = "https://www.procom.prosa.com.mx/eMerch2/7442762_segurosafirmemonterrey.jsp";
	static final String LOG_USER = "CreditCardPaymentService";
	
	/**
	 * @param insurancePolicy String 
	 * @param subsection String
	 * @param siglasRFC Siglas del RFC (3 personas morales, 4 personas fisicas)
	 * @param fechaRFC Fecha del RFC (en formato ddMMyy)
	 * @param homoClaveRFC Homoclave del RFC (3 digitos alfanumericos)
	 * @return receipts List<ReciboDTO>
	 */	
	 List<ReciboDTO> findReceipts(String insurancePolicy, String subsection, String siglasRFC, String fechaRFC, String homoClaveRFC);
	
	/**
	 * @param transaction PROSATransactionResponse
	 * @param receipts Long []
	 * @return success RespuestaPagoDTO
	 */	
	 RespuestaPagoDTO payReceipts(PROSATransactionResponse transaction, String[] receipts) throws PROSAException, Exception;
	
	/**
	 * @param Insurance Policy String
	 * @return Time remaining for policy to unlock
	 */		
	 PROSATransaction requestTransaction(String insurancePolicy, String subsection, String[] receipts) throws PROSAException, Exception;	
	
	 PolizaDTO findPolicy(String insurancePolicy) throws PROSAException;
	
	 String validReceipts(String receipts);
	
	 String validPoliza(String insurancePolicy, String subsection, String siglasRFC, String fechaRFC);
	
	 RecibosFoleados folearRecibos (ReciboDTO recibo);
	
	 List<ReciboDTO> buscaRecibosById (String poliza, String inciso, String [] recibos);
	
	 void envioPDFyXML(String receiptFiscalKey, List<String> addresses, String poliza);
	 
	 Double obtieneTipoCambioAlDia(Date fecha, String nombreUsuario);
}