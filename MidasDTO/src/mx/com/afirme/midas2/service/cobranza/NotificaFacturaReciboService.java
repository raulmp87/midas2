package mx.com.afirme.midas2.service.cobranza;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;

import net.sf.jasperreports.engine.JRException;

import mx.com.afirme.midas2.domain.cobranza.FiltroRecibos;
import mx.com.afirme.midas2.domain.cobranza.Recibos;

 
public interface NotificaFacturaReciboService {

	public void jobEnviaFacturaRecibo();
	
	public String guardalognotif(String descripcion);
			
	public void procesaEnvioFacturaRecibo(FiltroRecibos filtro);
	
	public void enviaFacturaRecibo (Recibos reciboaenviar);
	
	public void notificarFacturaRecibo(Recibos reciboaenviar)throws JRException;
	
	public String getmensajeFacturaRecibo ();
	
	public String replacemensajeFacturaRecibo(Recibos reciboaenviar , String elmensaje);
	
	public void initialize();
}
