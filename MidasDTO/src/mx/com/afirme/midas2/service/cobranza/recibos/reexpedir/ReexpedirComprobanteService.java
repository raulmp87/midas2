package mx.com.afirme.midas2.service.cobranza.recibos.reexpedir;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas2.domain.cobranza.recibos.reexpedir.FiltroReciboReexpedicionDTO;
import mx.com.afirme.midas2.domain.cobranza.recibos.reexpedir.FormaPagoSAT;

@Local
public interface ReexpedirComprobanteService {
	List<FiltroReciboReexpedicionDTO> listarRecibosFiltrado(FiltroReciboReexpedicionDTO filtro)  throws SystemException;
	
	void reexpedirComprobantes(String idMetodoPago, BigDecimal numeroCuenta, String listaRecibos, FiltroReciboReexpedicionDTO filtro) throws SystemException;
	
	byte[] descargarComprobante(String llaveFiscal, String tipoDocumento) throws SystemException;
	
	List<FormaPagoSAT> getFormasPagoSAT() throws SystemException;
	
}
