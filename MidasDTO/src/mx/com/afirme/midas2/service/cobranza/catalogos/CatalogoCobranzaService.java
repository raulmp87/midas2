package mx.com.afirme.midas2.service.cobranza.catalogos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.cobranza.catalogos.RelacionGrupoAgenteDTO;

@Local
public interface CatalogoCobranzaService {

	public List<RelacionGrupoAgenteDTO> getListaRelacionGrupoAgente( RelacionGrupoAgenteDTO filtro);
	
	public void save(RelacionGrupoAgenteDTO relacionGrupoAgente);
	
	public void baja(RelacionGrupoAgenteDTO relacionGrupoAgente);
}
