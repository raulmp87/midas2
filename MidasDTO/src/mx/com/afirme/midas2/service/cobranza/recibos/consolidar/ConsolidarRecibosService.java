package mx.com.afirme.midas2.service.cobranza.recibos.consolidar;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;
import mx.com.afirme.midas2.domain.cobranza.recibos.consolidar.Consolidado;
import mx.com.afirme.midas2.domain.cobranza.recibos.consolidar.RecibosConsolidar;
import mx.com.afirme.midas2.domain.cobranza.recibos.consolidar.RelRecibosConsolidado;

@Local
public interface ConsolidarRecibosService {

	public class ContenedorDatosAmisDTO {
		private BigDecimal idsession = BigDecimal.ZERO;
		private String tipoProceso;
		private String fechaInicioEjecucionManual;
		private String fechaFinEjecucionManual;
		private String tipomodulo;
		private String fase;

		public BigDecimal getIdsession() {
			return idsession;
		}

		public void setIdsession(BigDecimal idsession) {
			this.idsession = idsession;
		}

		public String getTipoProceso() {
			return tipoProceso;
		}

		public void setTipoProceso(String tipoProceso) {
			this.tipoProceso = tipoProceso;
		}

		public String getFechaInicioEjecucionManual() {
			return fechaInicioEjecucionManual;
		}

		public void setFechaInicioEjecucionManual(
				String fechaInicioEjecucionManual) {
			this.fechaInicioEjecucionManual = fechaInicioEjecucionManual;
		}

		public String getFechaFinEjecucionManual() {
			return fechaFinEjecucionManual;
		}

		public void setFechaFinEjecucionManual(String fechaFinEjecucionManual) {
			this.fechaFinEjecucionManual = fechaFinEjecucionManual;
		}

		public String getTipomodulo() {
			return tipomodulo;
		}

		public void setTipomodulo(String tipomodulo) {
			this.tipomodulo = tipomodulo;
		}

		public String getFase() {
			return fase;
		}

		public void setFase(String fase) {
			this.fase = fase;
		}

	}
	
	public String validarRecibosAConsolidar(String receipts);
	

	/**
	 * Obtener recibo a consolidar.
	 */
	public List<RecibosConsolidar> getlistRecibosAConsolidar(
			RecibosConsolidar reciboConsolidar);

	/**
	 * Obtener recibo a consolidar.
	 */

	public List<Consolidado> getlistRecibosConsolidados(Consolidado consolidado);

	/**
	 * Obtener recibos consolidados.
	 */
	public List<RelRecibosConsolidado> consolidarRecibosSeleccionados(
			RecibosConsolidar reciboConsolidar);

	/**
	 * Obtener recibos consolidados.
	 */
	public List<Consolidado> cancelarRecibosSeleccionados(
			RecibosConsolidar reciboConsolidar);

	public Object correrCFDConsolidado(Consolidado consolidado);

}
