package mx.com.afirme.midas2.service.portal.cotizador.vida;

import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.domain.portal.cotizador.vida.Cobertura;
import mx.com.afirme.vida.domain.movil.cotizador.CrearCotizacionVidaParameterDTO;
import mx.com.afirme.vida.domain.movil.cotizador.OccupationDTO;
import mx.com.afirme.vida.domain.movil.cotizador.SumaAseguradaDTO;

public interface CotizadorPortalVidaService {
	public Object cotizar(CrearCotizacionVidaParameterDTO param, boolean cotizar);

	public List<SumaAseguradaDTO> obtenerSumaAseguradaList();
	
	public List<OccupationDTO> getGiroOcupacion(OccupationDTO filter);
	
	public List<String> getPaquetes();
	
	public Map<String, String> getClaveSexo();
	
	public byte[] imprimirCotizacion(CrearCotizacionVidaParameterDTO param);
	
	public void enviarCorreo(CrearCotizacionVidaParameterDTO param);
	
	public List<String> getFormasPago();
	
	public List<Cobertura> cargarDetalleCoberturas(CrearCotizacionVidaParameterDTO param);
}
