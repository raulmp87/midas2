package mx.com.afirme.midas2.service.portal.cotizador;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.movil.cotizador.CotizacionMovilDTO;
import mx.com.afirme.midas2.domain.portal.cotizador.Cobertura;
import mx.com.afirme.midas2.domain.portal.cotizador.Estado;
import mx.com.afirme.midas2.domain.portal.cotizador.Vehiculo;
import mx.com.afirme.midas2.exeption.ApplicationException;

@Local
public interface CotizadorPortalService {

	List<Vehiculo> getVehiculos(String descripcionVehiculo) throws ApplicationException;
	List<Estado> getEstados(String estado) throws ApplicationException;
	List<String> getPaquetes();
	Long getPaquete(String descripcionPaquete) throws ApplicationException; 
	Long getTarifa(String estado, String descripcionVehiculo) throws ApplicationException;
	CotizacionMovilDTO buscaCotizacion(Map<String,Object> params) throws ApplicationException;
	List<Cobertura> cargarDetalleCoberturas(BigDecimal idToCotizacion, String descripcionPaquete) throws ApplicationException;
	BigDecimal getFormaPago(String descripcionFormaPago) throws ApplicationException;
	List<String> getFormasPago();
}
