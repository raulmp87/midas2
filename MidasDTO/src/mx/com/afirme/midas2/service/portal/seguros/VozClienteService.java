package mx.com.afirme.midas2.service.portal.seguros;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.portal.seguros.Departamento;
import mx.com.afirme.midas2.domain.portal.seguros.Oficina;
import mx.com.afirme.midas2.dto.portal.seguros.Comentario;

@Local
public interface VozClienteService {
	
	boolean enviarComentario(Comentario comentario);
	
	List<Departamento> obtenerDepartamentos();
	
	List<Oficina> obtenerOficinas();
}