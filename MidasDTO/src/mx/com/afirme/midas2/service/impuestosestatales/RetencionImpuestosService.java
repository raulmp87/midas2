package mx.com.afirme.midas2.service.impuestosestatales;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.catalogos.retencionimpuestos.RetencionCedularesView;
import mx.com.afirme.midas2.dto.impuestosestatales.RetencionImpuestosDTO;


@Local
public interface RetencionImpuestosService {

	public void guardarNuevaConfiguracion(RetencionImpuestosDTO filtro);

	public void desactivarConfiguracion(RetencionImpuestosDTO filtro);
	
	public List<RetencionImpuestosDTO> listarHistorico(RetencionImpuestosDTO filtro);
	
	public List<RetencionCedularesView>listarMonitorRetencionImpuestos();
	
	public List<RetencionImpuestosDTO> findByFiltersView(RetencionImpuestosDTO filtro);

	public boolean isCalculoRetencionImpuestosEnProceso();

}
