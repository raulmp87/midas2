package mx.com.afirme.midas2.service.general;

import java.util.List;

import mx.com.afirme.midas2.dto.general.Email;

/************************************************************************************
 *
 * Interfaz del servicio para el manejo de Transacciones del Objeto Email
 * 
 *  @author 			Eduardo Valentin Chavez Oliveros (Eduardosco)
 *	Unidad de Fabrica: 	Avance Solutions Corporation S.A. de C.V.
 *
 ************************************************************************************/
public interface EmailService {
	public List<Email> findAll();
	public Email findById(Long id);
	public Email findByDesc(String desc);
	public List<Email> findByStatus(boolean status);
	public Email saveObject(Email object);
	public List<Email> findByProperty(String propertyName, Object property);
}