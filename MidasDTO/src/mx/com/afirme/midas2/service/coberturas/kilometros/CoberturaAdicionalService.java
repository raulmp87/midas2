package mx.com.afirme.midas2.service.coberturas.kilometros;

import java.math.BigDecimal;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.coberturas.kilometros.GeneraSolicitudRequest;
import mx.com.afirme.midas2.domain.coberturas.kilometros.GeneraSolicitudResponse;

@Local
public interface CoberturaAdicionalService {
	
	public static final BigDecimal GRUPO_PARAMETRO_COBERTURA_ADICIONAL = new BigDecimal("97");
	public static final BigDecimal PARAMETRO_USUARIO_CREACION_MODIFICACION = new BigDecimal("970010");
	public static final BigDecimal PARAMETRO_NOMBRE_USUARIO_CREACION_MODIFICACION = new BigDecimal("970020");
	public static final BigDecimal PARAMETRO_COBERTURAS_DISPONIBLES_INTER_X = new BigDecimal("970030");
	public static final BigDecimal PARAMETRO_FORMA_PAGO_DISPONIBLES_INTER_X = new BigDecimal("970032");
	public static final BigDecimal PARAMETRO_CORREOS_NOTIFICACION_SOLICITUD = new BigDecimal("970040");
	public static final BigDecimal PARAMETRO_TELEFONO_MONTERREY = new BigDecimal("970070");
	public static final BigDecimal PARAMETRO_TELEFONO_MEXICO = new BigDecimal("970080");
	public static final BigDecimal PARAMETRO_TELEFONO_REPUBLICA = new BigDecimal("970090");
	
	public static final Short TIPO_COMENTARIO = 3;
	
	Map<String, Object> generaSolicitud(GeneraSolicitudRequest generaSolicitudRequest);
	
	String obtenerParametrosSolicitud(boolean conEncabezado, GeneraSolicitudRequest generaSolicitudRequest, GeneraSolicitudResponse generaSolicitudResponse);
	
	String contrataSolicitud(BigDecimal numeroSolicitud, boolean contratar);
}