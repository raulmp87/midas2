package mx.com.afirme.midas2.service.recepcionDocumentos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentosAgrupados;
import mx.com.afirme.midas2.domain.recepcionDocumentos.RecepcionDocumentos;
import mx.com.afirme.midas2.dto.fuerzaventa.EntregoDocumentosView;
import mx.com.afirme.midas2.dto.fuerzaventa.GenericaAgentesView;
import mx.com.afirme.midas2.util.MidasException;

@Local
public interface RecepcionDocumentosService {

	public void saveRechazoDocumentos(RecepcionDocumentos recepDoc, Integer mesInicio, Integer mesFin)throws MidasException;
	
	public void actualizarStatusRecibos(DocumentosAgrupados docsAgrupados, Integer mesInicio, Integer mesFin)throws Exception;
	
	public List<EntregoDocumentosView> listaFacturasMesAnio(Long anioMes,Long idAgente) throws Exception;
	
	public void generaFacturaAgenteComis_job() throws Exception;
	
	public void initialize();
}
