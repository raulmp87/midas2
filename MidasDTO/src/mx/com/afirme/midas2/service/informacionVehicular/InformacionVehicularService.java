package mx.com.afirme.midas2.service.informacionVehicular;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.cesvi.vinplus.Respuesta;



@Local
public interface InformacionVehicularService {
	public byte[] getConsultaVin(String vin);
	
	public boolean validacionActiva (BigDecimal codigoParametroGeneral);
	
	public String getModificadoresDescripcion (int grupo, String clave);
	
	public String getMensaje (int respuesta, String vin, String mensajeAdicional);
	
	public List<Respuesta> obtenerVin(String numeroSerie);

	public String validaInfoVehicularByEstiloCotInd(BigDecimal idToCotizacion, BigDecimal polizaId) throws Exception;
	
	public Respuesta obtenerInformacionVehiculo(String numeroSerie) throws Exception; 
}
