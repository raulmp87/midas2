package mx.com.afirme.midas2.service;

import javax.ejb.Local;

import com.anasoft.os.daofusion.bitemporal.CurrentDateProvider;

@Local
public interface OracleCurrentDateService extends CurrentDateProvider {

}
