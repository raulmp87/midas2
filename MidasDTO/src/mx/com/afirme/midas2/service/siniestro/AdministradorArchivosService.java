package mx.com.afirme.midas2.service.siniestro;
import java.util.Map;
import javax.ejb.Local;
import mx.com.afirme.midas2.domain.documentosFortimax.FortimaxDocument;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.ProcesosFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.ProcesosFortimax.PROCESO_FORTIMAX;
/**
 * Esta clase maneja la interacci�n los los diferentes WS expuestos por fortimax.
 * @author Armando Garcia 
 * @version 1.0
 * @created 03-jul-2014 05:12:48 p.m.
 */
@Local
public interface AdministradorArchivosService {
	public static final String PROCESO_TIPO_GENERICO = "G";
	public static final String PROCESO_TIPO_LISTA = "L";
	public static final String ERROR_EXISTE_DOCUMENTO= "FMX-DOC-WS-1050";
	public static final String TERCEROS_PRCB= "PRCB" ;
	public static final String SEPARADOR= "_" ;
	public static final String TERCEROS_PRCV= "PRCV" ;
	/**
	 * Genera Expediente en fortimax
	 * @param FortimaxDocument document
	 * @return boolean
	 */
	
	public boolean  generarExpediente(FortimaxDocument document) throws Exception;
	
	/**
	 * Genera documento en fortimax
	 * @param FortimaxDocument document
	 * @return boolean
	 */
	
	public boolean generarDocumento(FortimaxDocument document)throws Exception;

	/**
	 * Descarga documento en fortimax
	 * @param FortimaxDocument
	 * @return FortimaxDocument
	 */
	
	public FortimaxDocument descargarDocumentoFortimax(FortimaxDocument document ) throws Exception;

	/**
	 * Eliminar Documento en fortimax
	 * @param FortimaxDocument
	 * @return FortimaxDocument
	 */
	public FortimaxDocument eliminarDocumento(FortimaxDocument document)throws Exception;

	/**
	 * Genera Carpeta en fortimax
	 * @param FortimaxDocument
	 * @return FortimaxDocument
	 */
	public FortimaxDocument generarCarpetaFortimax(FortimaxDocument document) throws Exception;

	/**
	 * Genera liga de acceso a fortimax
	 * @param FortimaxDocument
	 * @return FortimaxDocument
	 */
	public FortimaxDocument generarLigaFortimax(FortimaxDocument document,boolean defaultUser)throws Exception;
	/**
	 * Genera liga de acceso a fortimax por proceso
	 * @param proceso
	 * @param idFortimax
	 * @param documento
	 * @param defaultUser
	 * @return String
	 */
	public String generarLigaByProceso(PROCESO_FORTIMAX proceso, Long idFortimax, String documento,boolean defaultUser   )throws Exception;
	/**
	 * Genera documentos en fortimax  por proceso
	 * @param proceso
	 * @param idFortimax
	 * @param folio
	 * @return FortimaxDocument
	 */
	public FortimaxDocument generarDocumentoByProceso (PROCESO_FORTIMAX proceso, Long idFortimax, String folio)throws Exception;;
	/**
	 * Genera login de acceso a fortimax
	 * @param FortimaxDocument
	 * @return FortimaxDocument
	 */
	public FortimaxDocument loginFortimax(FortimaxDocument document) throws Exception;

	/**
	 * Obtiene Documentos anexados en fortimax por Gaveta 
	 * @param FortimaxDocument
	 * @return FortimaxDocument
	 */
	public  FortimaxDocument obtenerDocumentosFortimax(FortimaxDocument document, boolean nodoOnly) throws Exception;

	/**
	 * Obtiene tipo de documemto 
	 * @param FortimaxDocument
	 * @return FortimaxDocument
	 */
	public  FortimaxDocument  obtenerTiposDocumentos(FortimaxDocument document)throws Exception;

	/**
	 * Sube un Archivo en Fortimax
	 * @param FortimaxDocument
	 * @return FortimaxDocument
	 */
	public boolean subirArchivoFortimax(FortimaxDocument document)throws Exception;
	
	/**
	 * Sube un Archivo en Fortimax por carpeta
	 * @param FortimaxDocument
	 * @param carpeta
	 * @return FortimaxDocument
	 */
	public boolean subirArchivoFortimax(FortimaxDocument document, String carpeta)throws Exception;
	
	/**
	 * obtiene documentos por Prefijo 
	 * @param FortimaxDocument
	 * param nombreArchivo
	 * @return FortimaxDocument
	 */
	public FortimaxDocument obtenerDocumentosByPrefijo(FortimaxDocument document,String nombreArchivo)throws Exception;
	
	/**
	 * obtiene documentos por carpeta 
	 * @param FortimaxDocument
	 * @param carpeta
	 * @return FortimaxDocument
	 */
	public FortimaxDocument obtenerDocumentosByCarpeta(FortimaxDocument document,String carpeta)throws Exception;
	
	/**
	 * Genera Codigo secuencial de un documentos por proceso
	 * @param FortimaxDocument
	 * @return String
	 */
	public String generarCodigo(FortimaxDocument document) throws Exception;
	
	/**
	 * retorna la lista de archivos por proceso
	 * @param proceso
	 * @return Map<String, String>
	 */
	public  Map<String, String> getListaArchivosByProceso (PROCESO_FORTIMAX proceso) throws Exception;
	
	/**
	 * Consulta los documentos almacenados en un expediente por proceso
	 * @param proceso
	 * @param fortimaxID
	 * @return Map<String, String>
	 */
	public Map<String, String> getDocumentosAlmacenadosByProceso(
			PROCESO_FORTIMAX proceso, Long fortimaxID) throws Exception ;
	
	/**
	 * Obitene entidad proceso por clave proceso
	 * @param proceso
	 * @return ProcesosFortimax
	 */
	
	public ProcesosFortimax getProceso  (PROCESO_FORTIMAX proceso)  throws Exception;
	
	public String getNodo(Long id,String tituloAplicacion, String nombreElemento, String tipoElemento, boolean nodoOnly )throws Exception;
	


}