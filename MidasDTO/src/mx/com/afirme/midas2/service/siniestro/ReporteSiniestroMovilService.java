package mx.com.afirme.midas2.service.siniestro;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.domain.siniestro.ReporteSiniestroMovil;
import mx.com.afirme.midas2.util.PagingList;

@Local
public interface ReporteSiniestroMovilService {
	
	/**
	 * Reporta un siniestro móvil.
	 * @param reporteSiniestroMovil
	 * @return el <code>reporteSiniestroMovil</code>
	 */
	public ReporteSiniestroMovil reportarSiniestro(ReporteSiniestroMovil reporteSiniestroMovil);

	public ReporteSiniestroMovil findById(Long id);
	
	public ReporteSiniestroMovil findByIdWithCabin(Long id);

	public List<ReporteSiniestroMovil> find();
	
	public PagingList<ReporteSiniestroMovil> findByExamplePage(ReporteSiniestroMovil reporteSiniestroMovil, int pageSize, int page);

	
	/**
	 * Guarda los cambios realizados en <code>reporteSiniestroMovil</code>
	 * @param reporteSiniestroMovil
	 * @return
	 */
	public ReporteSiniestroMovil guardar(ReporteSiniestroMovil reporteSiniestroMovil);
	
	public List<ReporteSiniestroMovil> findSinAsignar();

	/**
	 * Indica si el usuario actual tiene restringido el acceso a los reportes a solo los de su cabina.
	 * @return true si solo puede ver los reportes de su cabina.
	 */
	public boolean isRestringirCabina();
	
	
	/**
	 * Obtiene un <code>ReporteSiniestroMovil</code> que se haya generado recientemenete.
	 * @param numeroCelular
	 * @return reporteSiniestroMovil o null si no se encontro ninguno reciente.
	 */
	public ReporteSiniestroMovil findReporteReciente(String numeroCelular);
	
	/**
	 * Indica si existe el inciso.
	 * @param poliza
	 * @param numeroInciso
	 * @return true si el inciso existe.
	 */
	public boolean existeInciso(PolizaDTO poliza, Integer numeroInciso);
	
	/**
	 * Crea un reporte de cabina a partir del reporte de siniestro movil
	 * @param reporteSiniestroMovil
	 * @return
	 */
	public Long crearReporteCabina(ReporteSiniestroMovil reporteSiniestroMovil);
}
