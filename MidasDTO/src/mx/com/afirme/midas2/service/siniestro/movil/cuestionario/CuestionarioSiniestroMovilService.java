package mx.com.afirme.midas2.service.siniestro.movil.cuestionario;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestro.movil.cuestionario.PreguntaSiniestroMovil;
import mx.com.afirme.midas2.domain.siniestro.movil.cuestionario.RespuestaNumericaSiniestroMovil;

@Local
public interface CuestionarioSiniestroMovilService {
	
	public List<PreguntaSiniestroMovil> obtenerPreguntas();
	public boolean responder(List<RespuestaNumericaSiniestroMovil> respuesta);

}
