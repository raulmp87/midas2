package mx.com.afirme.midas2.service.siniestro;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.siniestros.provision.MovimientoProvisionSiniestros;
import mx.com.afirme.midas2.dto.siniestros.DefinicionSiniestroRoboDTO;
import mx.com.afirme.midas2.dto.siniestros.ReporteSiniestroRoboDTO;

import java.util.List;
import java.util.Map;

/**
 * <font color="#808080">Esta es la definici�n de los m�todos necearios para la
 * b�squeda de sinietros catalogados como robo total.</font>
 * @author usuario
 * @version 1.0
 * @created 28-jul-2014 12:50:44 p.m.
 */
@Local
public interface RoboService {



	/**
	 * Busqueda de siniestros catalogados como robo utilizando el filtro capturado en
	 * pantalla
	 * 
	 * @param Filtro
	 */
	public List<ReporteSiniestroRoboDTO> busquedaRobo(ReporteSiniestroRoboDTO reporteSiniestroDTO);	



	/**
	 * Este m�todo guarda los cambios hechos en la pantalla de seguimiento de robo
	 * 
	 * @param siniestroRoboDTO
	 * @param envioOcra
	 *//*change for commit*/
	public Map<String,MovimientoProvisionSiniestros> guardarRoboTotal(DefinicionSiniestroRoboDTO siniestroRoboDTO, boolean envioOcra) throws Exception;
	
	/**
	 * Este m�todo Realiza el envio a Ocra 
	 * 
	 * @param DefinicionSiniestroRoboDTO
	 */
	public void envioOcra(DefinicionSiniestroRoboDTO siniestroRoboDTO) throws Exception;

	
	/**
	 * Este m�todo consulta la informacion del DTO
	 * @param reporteCabinaId
	 * @param reporteCabinaId
	 */
	public DefinicionSiniestroRoboDTO consultarDefinicionSiniestroRobo(Long reporteCabinaId,Long coberturaCabinaId);
	
	/**
	 * Obtiene un valor de la base de datos de parametroGlobal
	 * @param appMidas ID de la aplicación
	 * @param parametro String para identificar al parámetro
	 * @return
	 */
	public String getValorParametroGlobal(int appMidas, String parametro );
	
	public void guardarYProvisionar(DefinicionSiniestroRoboDTO siniestroRoboDTO,boolean envioOcra)throws Exception;


}