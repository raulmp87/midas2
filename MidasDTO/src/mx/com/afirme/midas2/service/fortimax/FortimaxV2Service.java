package mx.com.afirme.midas2.service.fortimax;


import mx.com.afirme.midas2.domain.documentosGenericosFortimax.ProcesosFortimax.PROCESO_FORTIMAX;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;


public interface FortimaxV2Service {
		

	/**
	 * Genera expediente en la gaveta especificada: tituloAplicacion
	 * @param tituloAplicacion
	 * @param fieldValues
	 * @return String[]
	 */
	public String[] generateExpedient(String tituloAplicacion, String[] fieldValues)throws Exception;

	/**
	 * Genera documento en fortimax en la gaveta especificada: tituloAplicacion
	 * @param id 
	 * @param tituloAplicacion
	 * @param documentName
	 * @param folderName
	 * @return String[]
	 */
	public String[] generateDocument(Long id, String tituloAplicacion, String documentName,String folderName)throws Exception;
	
	/**
	 * Genera liga a Ifimax
	 * @param id 
	 * @param tituloAplicacion
	 * @param documentName
	 * @param user
	 * @param password
	 * @return String Liga
	 */
	public String generateLinkToDocument(Long id, String tituloAplicacion, String nombreDocumento, String user, String password,String nodo)
	throws Exception;
	
	/**
	 * Genera documento en fortimax en base a id expediente y gaveta ; tituloAplicacion
	 * @param id 
	 * @param tituloAplicacion
	 * @return String[]
	 */
	public String[] getDocumentFortimax(Long id,String tituloAplicacion) throws Exception;
	
	/**
	 * Sube documento o imagen a una gaveta en fortimax
	 * @param fileName 
	 * @param transporteImpresionDTO
	 * @param tituloAplicacion
	 * @param expediente
	 * @param carpeta
	 * @return String[]
	 */
	public String[] uploadFile(String fileName,  TransporteImpresionDTO transporteImpresionDTO, String tituloAplicacion, String[] expediente, String carpeta) throws Exception;
	
	/**
	 * Genera Token de acceso a fortimax
	 * @param duration 
	 * @return String
	 */
	public String getToken(long duration) throws Exception;
	
	/**
	 * Obitenen nodo de localizacion de un documento.
	 * @param id 
	 * @param tituloAplicacion 
	 * @param nombreElemento 
	 * @param tipoElemento
	 * @param nodoOnly  
	 * @return String
	 */
	public String getNodo(Long id,String tituloAplicacion, String nombreElemento, String tipoElemento,boolean nodoOnly )throws Exception ;
	
	/**
	 * Descarga un documento de un expediente 
	 * @param fileName 
	 * @param tipoElemento 
	 * @param tituloAplicacion 
	 * @param id
	 * @return byte[] 
	 */
	public byte[] downloadFile(String fileName,String tipoElemento , String tituloAplicacion,Long id ) throws Exception ;
	
	/**
	 * Descarga un documento de un expediente 
	 * @param fileName 
	 * @param tipoElemento 
	 * @param tituloAplicacion 
	 * @param id
	 * @return byte[] 
	 */
	public byte[] downloadFileBP(String fileName, String tipoElemento, String tituloAplicacion, Long id) throws Exception ;
	
	/**
	 * Descarga los documentos por proceso, en base al catalogo de procesos fortimax
	 * @param id 
	 * @param aplicacion 
	 * @param nombreDocumento 
	 * @param defaultUser
	 * @param proceso
	 * @return String 
	 */
	public String generateLinkToDocumentByProceso(Long id, String aplicacion, String nombreDocumento, boolean defaultUser,PROCESO_FORTIMAX proceso, String nodo)	throws Exception;

}
