package mx.com.afirme.midas2.service.fortimax;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentoEntidadFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentosAgrupados;
import mx.com.afirme.midas2.util.MidasException;
@Local
public interface DocumentoEntidadFortimaxService {
	/**
	 * Obtiene los documentos por entidad por medio del id de la entidad y nombre de la aplicacion
	 * @param idRegistro
	 * @param nombreAplicacion
	 * @return
	 * @throws MidasException
	 */
	public List<DocumentoEntidadFortimax> getListaDocumentosGuardadosPorEntidad(Long idRegistro,String nombreAplicacion) throws MidasException;
	
	public List<DocumentoEntidadFortimax> getListaDocumentosGuardadosPorEntidadYCarpeta(Long idRegistro,String nombreAplicacion,String nombreCarpeta) throws MidasException;
	
	public Long save(DocumentoEntidadFortimax documentoEntidad) throws MidasException;
	
	public void delete(Long idDocumentoEntidad) throws MidasException;
	
	public Long delete(DocumentoEntidadFortimax documentoEntidad) throws MidasException;
	/**
	 * Sincroniza Fortimax vs Base de datos consultando el servicio de documentos de fortimax y comparando con los que se tiene en base de datos,
	 * actualizando si existe o no el documento.
	 * @param idRegistro id del agente, o cliente , o entidad en question.
	 * @param nombreAplicacion nombre de la aplicacion segun la entidad, por ejemplo AGENTES, o CLIENTES, revisar tcCatalogoAplicacionFortimax columna "nombreAplicacion", NO checar columna "nombreAplicacionFortimax"
	 * @param nombreCarpeta nombre de la carpeta de la entidad 
	 */
	public void sincronizarDocumentos(Long idRegistro,String nombreAplicacion,String nombreCarpeta) throws MidasException,Exception;
	/**
	 * Metodo que obtiene los documentos pendientes por subir
	 * @param idRegistro
	 * @param nombreAplicacion
	 * @param nombreCarpeta
	 * @return
	 * @throws MidasException
	 */
	public List<DocumentoEntidadFortimax> getListaDocumentosFaltantes(Long idRegistro,String nombreAplicacion,String nombreCarpeta) throws MidasException;
	
	public boolean existeEstructuraPorEntidadAplicacion(Long idRegistro,String nombreAplicacion) throws MidasException;
	
	public boolean existeEstructuraPorEntidadAplicacionYCarpeta(Long idRegistro,String nombreAplicacion,String nombreCarpeta) throws MidasException;
	
	public List<DocumentoEntidadFortimax> findByFilters(DocumentoEntidadFortimax filtro) throws MidasException;
	
	public void saveDocumentosAgrupados(DocumentosAgrupados documentos, Integer numeroRegistros, String nombreDocumento, String nombreAplicacion, String nombreCarpeta)throws MidasException;
	
	public String sincronizarGrupoDocumentos(String nombreDocumento, String nombreAplicacion, String nombreCarpeta, DocumentosAgrupados documentos)throws MidasException;
	
	public void auditarDocumentosEntregadosAnio(Long idRegistro,Long anio,String nombreAplicacion) throws MidasException;
}
