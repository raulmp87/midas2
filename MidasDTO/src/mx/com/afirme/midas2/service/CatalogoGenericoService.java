package mx.com.afirme.midas2.service;

import java.util.List;
import mx.com.afirme.midas2.dto.RegistroDinamicoDTO;

public interface CatalogoGenericoService {
	/**
	 * Metodo que dependiendo de la accion te trae un registro dinamico
	 * @param id Si la accion es editar o borrar, por medio de este podemos obtener el objeto exacto para ser eliminado
	 * @param accion Indica el tipo de accion para saber que hacer con la entidad
	 * @param vista. Indica la vista que se desea consultar del registro dinámico.
	 * @return
	 */
	public RegistroDinamicoDTO verDetalle(String id,String accion,Class<?> vista);
	/**
	 * Obtiene la lista de registros por
	 * @param clave
	 * @return
	 */
	public List<RegistroDinamicoDTO> getRegistrosDinamicos();
	
	/**
	 * Obtiene la lista de registros por filtro
	 * @param filtro
	 * @return
	 */
	public List<RegistroDinamicoDTO> getRegistrosDinamicos(Object filtro);
}
