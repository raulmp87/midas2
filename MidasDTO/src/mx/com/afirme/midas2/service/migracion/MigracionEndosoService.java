package mx.com.afirme.midas2.service.migracion;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.migracion.MigEndososValidos;
import mx.com.afirme.midas2.domain.migracion.MigEndososValidosEd;

@Local
public interface MigracionEndosoService {

	public void migrarEndoso(MigEndososValidos migEndososValidos);
	public void migrarEndosoEmisionDelegada(MigEndososValidosEd migEndososValidosEd);
	
}
