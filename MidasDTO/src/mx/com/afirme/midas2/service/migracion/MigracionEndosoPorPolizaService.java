package mx.com.afirme.midas2.service.migracion;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.migracion.MigEndososValidos;
import mx.com.afirme.midas2.domain.migracion.MigEndososValidosEd;

@Local
public interface MigracionEndosoPorPolizaService {

	public void migrarEndosos(List<MigEndososValidos> migEndososValidosList);
	public void migrarEndososEmisionDelegada(List<MigEndososValidosEd> migEndososValidosEdList);

}
