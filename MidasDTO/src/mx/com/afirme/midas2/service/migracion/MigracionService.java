package mx.com.afirme.midas2.service.migracion;


public interface MigracionService {

	/**
	 * Migra los endosos de Seycos a Midas.
	 */
	public void migrarEndosos();
	
	
	/**
	 * Detiene la migración de endosos. Se debe esperar un tiempo a que los Threads
	 * agendados (Asynchronous) se den cuenta de que la migración fue detenida antes de volver 
	 * a comenzar de nuevo con la migración.
	 */
	public void detenerMigracionEndosos();
	
	/**
	 * Obtiene el estado en el que se encuentra el proceso de la migración de endosos.
	 * @return
	 */
	public EstadoMigracion getEstadoMigracion();


	/**
	 * Migra los endoso de Emisión Delegada a Midas.
	 */
	public void migrarEndososEmisionDelegada();
	
	/**
	 * Detiene la migración de endosos de Emisión Delegada.
	 */
	public void detenerMigracionEndososEmisionDelegada();

	/**
	 * Obtiene el estado en el que se encuentra el proceso de la migración de endosos de Emisión Delegada.
	 * @return
	 */
	public EstadoMigracion getEstadoMigracionEd();
	
	/**
	 * Mueve los archivos del sistema de archivos del servidor hacia el nuevo
	 * repositorio (Fortimax).
	 */	
	public void migrarArchivosFortimax();
	
}
