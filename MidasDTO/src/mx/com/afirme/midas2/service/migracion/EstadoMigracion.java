package mx.com.afirme.midas2.service.migracion;

public enum EstadoMigracion {
	DETENIDA,
	INICIADA,
	APAGADA
}
