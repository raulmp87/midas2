package mx.com.afirme.midas2.service.vehiculo;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.vehiculo.ValorComercialVehiculo;
import mx.com.afirme.midas2.dto.siniestros.DatosEstimacionCoberturaDTO;



import java.util.Date;
import java.util.List;

@Local
public interface ValorComercialVehiculoService {

	/**
	 * Metodo para recorrer, ejecutar la validacion y guardar los elementos de la
	 * lista de valores comerciales de vehiculos provenientes de la carga.
	 * Utilizar el EntidadService para persistir los objetos
	 * 
	 * @param listaRegistros
	 */
	public List<ValorComercialVehiculo> cargarRegistros(List<ValorComercialVehiculo> listaRegistros);

	/**
	 * Método para actualizar el estatus de un registro de valor comercial vehiculo a
	 * desactivado.
	 * Utilizar el EntidadService para actualizar el objeto en BD
	 * 
	 * @param registro
	 */
	public int desactivarRegistro(ValorComercialVehiculo registro);

	/**
	 * Método para realizar la busqueda de los valores comerciales de vehiculos por
	 * los distintos filtros proporcionados. Usar los metodos de EntidadService para
	 * realizar las busquedas de los objetos.
	 * 
	 * @param filtro
	 */
	public List<ValorComercialVehiculo> findByFilters(ValorComercialVehiculo filtro);

	/**
	 * Método para obtener el listado de valores que seran desplegados en el filtro
	 * Año de valor comercial.
	 * Debe filtrar los distintos valores registrados en la columna VALORCOMERCIALANIO
	 * de la tabla MIDAS.TOVALORCOMERCIALVEHICULO ordenarlos de manera descendente y
	 * tomar los valores mas recientes disponibles(hasta un maximo de 5 valores, si
	 * existen mas deberan descartarse).
	 */
	public List<Integer> obtenerAniosValorComercial();

	/**
	 * Método en el que se realizan las validaciones sobre un objeto del tipo
	 * ValorComercialVehiculo para determinar si cumple con las condiciones para ser
	 * registrado en la Base de Datos.
	 * - Tipo 1-si valida mes y año valor comercial no mayor a 2 meses, 2-si valida registro previo
	 * - El mes y año de valor comercial no es mayor a 2 meses (Error)
	 * - Existe algun registro previo con misma clave AMIS, mes y año de valor
	 * comercial (Notificacion)
	 * En caso de no cumplir con las validaciones debe marcar los objetos con un
	 * estatus de error y un mensaje de error/notificacion correspondiente
	 * 
	 * @param registro
	 */
	public String validarRegistro(ValorComercialVehiculo registro, int tipo);
	
	
	public List<Integer> obtenerAnioModelos();
	
	/**
	 * Metodo para obtener los valores comerciales sugeridos de aucerdo al AMIS y Modelo del vehiculo
	 * @param datosEstimacion
	 * @param claveAmis
	 * @param modelo
	 * @param fechaConsulta
	 */
	void getValorComercial(DatosEstimacionCoberturaDTO datosEstimacion,Long claveAmis,short modelo , Date fechaConsulta);
	
	
}
