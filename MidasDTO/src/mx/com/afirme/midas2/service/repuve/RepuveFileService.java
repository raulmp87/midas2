package mx.com.afirme.midas2.service.repuve;

import java.io.File;
import java.io.Serializable;
import java.util.Date;

/************************************************************************************
 *
 * Interfaz del servicio para el manejo de Transacciones del Objeto Repuve
 * 
 *  @author 			Eduardo Valentin Chavez Oliveros (Eduardosco)
 *	Unidad de Fabrica: 	Avance Solutions Corporation S.A. de C.V.
 *
 ************************************************************************************/
public interface RepuveFileService extends Serializable{
	public File generarArchivoRepuve(Date fechaOperaciones, int tipoArchivo); //1 Avisos - 2 Consultas.
	public boolean procesarArchivoRepuve(File file, String fileName);
}