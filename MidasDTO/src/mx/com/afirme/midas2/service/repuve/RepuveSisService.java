package mx.com.afirme.midas2.service.repuve;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.dto.repuve.sistemas.RepuveSisEmision;
import mx.com.afirme.midas2.dto.repuve.sistemas.RepuveSisPTT;
import mx.com.afirme.midas2.dto.repuve.sistemas.RepuveSisRecuperacion;
import mx.com.afirme.midas2.dto.repuve.sistemas.RepuveSisRobo;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;

/************************************************************************************
 *
 * Interfaz del servicio para el manejo de Transacciones del Objeto Repuve
 * 
 *  @author 			Eduardo Valentin Chavez Oliveros (Eduardosco)
 *	Unidad de Fabrica: 	Avance Solutions Corporation S.A. de C.V.
 *
 ************************************************************************************/
public interface RepuveSisService extends Serializable{
	public List<RepuveSisEmision> obtenerEmisionPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina);
	public List<RepuveSisRobo> obtenerRoboPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina);
	public List<RepuveSisRecuperacion> obtenerRecuperacionPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina);
	public List<RepuveSisPTT> obtenerPTTPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina);
}