package mx.com.afirme.midas2.service.datosAmis;

import java.util.Map;

public interface DatosAmisService {

	public Map<Long, String> getListarDatos();
}
