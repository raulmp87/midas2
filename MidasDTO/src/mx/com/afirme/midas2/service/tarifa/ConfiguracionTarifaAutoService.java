package mx.com.afirme.midas2.service.tarifa;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.tarifa.ConfiguracionTarifaAuto;
import mx.com.afirme.midas2.domain.tarifa.TarifaAuto;


public interface ConfiguracionTarifaAutoService {

	/**
	 * Consulta la lista de configuraciones correspondientes a la tarifa recibida.
	 * @param tarifaAuto
	 * @return
	 */
	public List<ConfiguracionTarifaAuto> getConfiguracionTarifaAuto(TarifaAuto tarifaAuto);
}
