package mx.com.afirme.midas2.service.tarifa;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.tarifa.TarifaAutoModifDesc;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoModifDescId;


public interface TarifaAutoModifDescService extends TarifaService<TarifaAutoModifDescId, TarifaAutoModifDesc> {

	public List<TarifaAutoModifDesc> findByFilters(TarifaAutoModifDesc filtro);
	
}
