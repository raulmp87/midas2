package mx.com.afirme.midas2.service.tarifa;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas2.domain.tarifa.TarifaServicioPublico;
import mx.com.afirme.midas2.domain.tarifa.TarifaServicioPublicoDeduciblesAd;
import mx.com.afirme.midas2.domain.tarifa.TarifaServicioPublicoSumaAseguradaAd;
import mx.com.afirme.midas2.dto.RelacionesTarifaServicioPublicoDTO;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.service.ListadoService;

public interface TarifaServicioPublicoService extends ListadoService{
	
	RelacionesTarifaServicioPublicoDTO getRelationList(TarifaServicioPublico tarifaServicioPublico);
	RespuestaGridRelacionDTO guardarMontosCoberturasAdicionales(String accion, TarifaServicioPublico tarifaServicioPublico);
	RespuestaGridRelacionDTO guardarSumasAseguradasAdicionales(String accion, TarifaServicioPublicoSumaAseguradaAd tarifaServicioPublicoAseguradaAd);
	RespuestaGridRelacionDTO guardarDeduciblesAdicionales(String accion, TarifaServicioPublicoDeduciblesAd tarifaServicioPublicoDeduciblesAd);
	RelacionesTarifaServicioPublicoDTO getTarifasServicioPublico(TarifaServicioPublico tarifaServicioPublico);
	List<TarifaServicioPublicoDeduciblesAd> obtenerDeduciblesAdicionalesSerivicioPublico(TarifaServicioPublico tarifaServicioPublicoFiltro);
	boolean esParametroValido(BigDecimal valorParametro, BigDecimal parametroGeneral);
}
