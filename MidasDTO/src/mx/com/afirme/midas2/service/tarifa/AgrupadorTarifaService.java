package mx.com.afirme.midas2.service.tarifa;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifa;


public interface AgrupadorTarifaService {
	public List<AgrupadorTarifa> findByFilters(
			AgrupadorTarifa filtroAgrupadorTarifa);

	/**
	 * Busca una Entidad de tipo AgrupadorTarifa
	 * 
	 * @param id
	 *            corresponde al id de la entidad a buscar
	 *            se sompone de una cadena de 2 valores separada
	 *            por _ (valor_valor)
	 * @return AgrupadorTarifa entidad encontrada
	 */
	public AgrupadorTarifa findById(String id);
	
	public AgrupadorTarifa save(AgrupadorTarifa agrupadorTarifa);
	
	public BigDecimal getNextSequence();
	
	public AgrupadorTarifa createNewVersion(AgrupadorTarifa agrupadorTarifa);
	
	public Map<BigDecimal, String> findByNegocio(String claveNegocio, Long idMoneda);
	
	public AgrupadorTarifa consultaVersionActiva(BigDecimal idToAgrupadorTarifa);
	
	public Map<BigDecimal, String> findByAgrupadorPorMonedaPorSeccion(String claveNegocio, BigDecimal idSeccion, Long idMoneda);
}
