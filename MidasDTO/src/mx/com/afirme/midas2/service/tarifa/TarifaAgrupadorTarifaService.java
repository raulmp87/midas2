package mx.com.afirme.midas2.service.tarifa;

import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccion;
import mx.com.afirme.midas2.domain.tarifa.TarifaAgrupadorTarifa;
import mx.com.afirme.midas2.dto.RelacionesTarifaAgrupadorTarifaDTO;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.service.ListadoService;

public interface TarifaAgrupadorTarifaService extends ListadoService {
	
	public RelacionesTarifaAgrupadorTarifaDTO getRelationList(TarifaAgrupadorTarifa tarifaAgrupadorTarifa, String claveNegocio);
	
	public RespuestaGridRelacionDTO relacionarTarifaAgrupadorTarifa(String accion, TarifaAgrupadorTarifa tarifaAgrupadorTarifa);
	
	public TarifaAgrupadorTarifa getPorNegocioAgrupadorTarifaSeccion(NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion);

}
