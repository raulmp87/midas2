package mx.com.afirme.midas2.service.tarifa;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.tarifa.TarifaAutoProliber;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoProliberId;
import mx.com.afirme.midas2.dto.wrapper.TipoServicioVehiculoComboDTO;
import mx.com.afirme.midas2.dto.wrapper.TipoUsoVehiculoComboDTO;
import mx.com.afirme.midas2.dto.wrapper.TipoVehiculoComboDTO;


public interface TarifaAutoProliberService extends TarifaService<TarifaAutoProliberId, TarifaAutoProliber> {
	
	public List<TarifaAutoProliber> findByFilters(TarifaAutoProliber filter);
	public List<TipoVehiculoComboDTO> getListaTipoVehiculoComboDTO(BigDecimal idToSeccion);
	public List<TipoUsoVehiculoComboDTO> getListaTipoUsoVehiculoComboDTO(BigDecimal idTcTipoVehiculo);
	public List<TipoServicioVehiculoComboDTO> getListaTipoServicioVehiculoComboDTO(BigDecimal idTcTipoVehiculo);

}
