package mx.com.afirme.midas2.service.tarifa;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.tarifa.TarifaVersion;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersionId;


public interface TarifaVersionService {

	public TarifaVersion findById(TarifaVersionId id);
		
	public LinkedHashMap<String, String> cargarVersiones(TarifaVersionId id);
	
	public TarifaVersion cambiarEstatus(TarifaVersionId id, Short claveEstatus);
	
	public TarifaVersion generarNuevaVersion(TarifaVersionId id);
	
	public TarifaVersion cargarPorValorMenu(String valorMenu, Long version, Long idMoneda);
	
	public Map<String, String> cargarMonedas();
	
	public Map<String, String> cargarMonedasByTarifaVersion(TarifaVersionId id);
}
