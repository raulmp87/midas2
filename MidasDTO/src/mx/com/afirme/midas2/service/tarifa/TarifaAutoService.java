package mx.com.afirme.midas2.service.tarifa;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.tarifa.TarifaAuto;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoId;


public interface TarifaAutoService extends TarifaService<TarifaAutoId, TarifaAuto>{

}
