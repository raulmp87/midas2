package mx.com.afirme.midas2.service.tarifa;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifaSeccion;


public interface AgrupadorTarifaSeccionService {
	
	public AgrupadorTarifaSeccion save(AgrupadorTarifaSeccion entity);

	public List<AgrupadorTarifaSeccion> findByFilters(AgrupadorTarifaSeccion filter);
	
	public List<AgrupadorTarifaSeccion> findAll();
	
	public List<AgrupadorTarifaSeccion> findByBusiness(String claveNegocio, AgrupadorTarifaSeccion filter);
	
	public List<AgrupadorTarifaSeccion> consultaAgrupadoresDefault(BigDecimal idToSeccion, String claveNegocio);
	
	public List<AgrupadorTarifaSeccion> consultaAgrupadoresPosibles(NegocioSeccion negocioSeccion, String idMonedas);
	
	public List<AgrupadorTarifaSeccion> consultaAgrupadoresPosiblesConVersiones(NegocioSeccion negocioSeccion, BigDecimal idMoneda, String claveNegocio);
	
}

