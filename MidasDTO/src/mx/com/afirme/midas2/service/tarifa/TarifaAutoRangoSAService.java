package mx.com.afirme.midas2.service.tarifa;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.tarifa.TarifaAutoRangoSA;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoRangoSAId;


public interface TarifaAutoRangoSAService extends TarifaService<TarifaAutoRangoSAId, TarifaAutoRangoSA>{

	public List<TarifaAutoRangoSA> findByFilters(TarifaAutoRangoSA tarifaAutoRangoSA);
}
