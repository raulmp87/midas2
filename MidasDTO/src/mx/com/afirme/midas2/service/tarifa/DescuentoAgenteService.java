package mx.com.afirme.midas2.service.tarifa;
import java.util.List;
import javax.ejb.Local;

import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas2.domain.movil.cotizador.DescuentosAgenteDTO;

/**
 * Local interface for DescuentoAgenteService.
 * 
 * @author 
 */
@Local
public interface DescuentoAgenteService {

	public DescuentosAgenteDTO save(DescuentosAgenteDTO entity);

	public void delete(DescuentosAgenteDTO entity);

	public String update(DescuentosAgenteDTO entity);

	public DescuentosAgenteDTO findById(Long id);

	public List<DescuentosAgenteDTO> findByProperty(String propertyName,
			Object value);

	public List<DescuentosAgenteDTO> findByClaveagente(Object claveagente);

	public List<DescuentosAgenteDTO> findByNombre(Object nombre);

	public List<DescuentosAgenteDTO> findByPorcentaje(Object porcentaje);

	public List<DescuentosAgenteDTO> findAll();
	
	public List<DescuentosAgenteDTO> findByFilters(DescuentosAgenteDTO filter);
	
	public List<String> cargaMasiva(String url) throws Exception;
	
	public boolean setDescuentoAgenteExcelList(List<DescuentosAgenteDTO> descuentosAgenteDTOExcelList);
	
	public boolean existeDescuentoAgente(DescuentosAgenteDTO param);
	
	public List<DescuentosAgenteDTO> findByFiltersDiferentID(DescuentosAgenteDTO filter);
	
	public List<DescuentosAgenteDTO> findByPropertyActivos(String claveNegocio,String propertyName,
			final Object value);
	
	//public List<RamoDTO> obtenerRamos() throws Exception;
	
	public boolean existeClavePromoClaveNegocio(DescuentosAgenteDTO filter);
	
}