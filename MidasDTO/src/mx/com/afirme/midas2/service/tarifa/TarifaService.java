package mx.com.afirme.midas2.service.tarifa;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.tarifa.TarifaVersionId;
import mx.com.afirme.midas2.dto.RegistroDinamicoDTO;


public interface TarifaService<K,E>{
	
	public  void save(E entity);

	public void remove(E entity);
	
	public E update(E entity);

	@SuppressWarnings("hiding")
	public <E,K> E findById(Class<E> entityClass,K id);

	@SuppressWarnings("hiding")
	public <E> List<E> findAll(Class<E> entityClass);

	@SuppressWarnings("hiding")
	public <E,K> E getReference(Class<E> entityClass,K id);
	/**
	 * Metodo que regresar� una lista de objetos de la entidad por medio de tarifa version id
	 * @param tarifaVersionId
	 * @return
	 */
	public List<E> findByTarifaVersionId(TarifaVersionId tarifaVersionId);
	
	/**
	 * Metodo que dependiendo de la accion te trae un registro dinamico
	 * @param tarifaVersionId Si la accion es agregar, entonces se obtiene un objeto vacio de este tipo de catalogo
	 * @param id Si la accion es editar o borrar, por medio de este podemos obtener el objeto exacto para ser eliminado
	 * @param accion Indica el tipo de accion para saber que hacer con la entidad
	 * @param vista Indica la vista de los campos que se deben mostrar. Si no existe una vista definida, se tomara la vista Default 
	 * (javax.validation.groups.Default)
	 * @return
	 */
	public RegistroDinamicoDTO verDetalle(TarifaVersionId tarifaVersionId,String id,String accion, Class<?> vista);
	
	public List<RegistroDinamicoDTO> getRegistrosDinamicosPorTarifaVersionId(TarifaVersionId tarifaVersionId, Class<?> vista);
	
	public E getNewObject();
}
