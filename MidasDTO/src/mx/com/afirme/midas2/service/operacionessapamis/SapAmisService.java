package mx.com.afirme.midas2.service.operacionessapamis;

import java.util.ArrayList;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.dao.amis.alertas.EnvioConsultaAcciones;
import mx.com.afirme.midas2.dao.amis.alertas.EnvioConsultaAlertas;
import mx.com.afirme.midas2.dao.amis.alertas.RespuestaSapAmisAlerta;
import mx.com.afirme.midas2.dto.sapamis.accionesalertas.SapAmisAccionesRelacion;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatAlertasSapAmis;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatSistemas;

public interface SapAmisService {
	CatAlertasSapAmis obtenerObjetoCatAlertasSapAmis(String catAlertasSapAmis);
	CatSistemas obtenerObjetoCatSistemas(String catSistemas);
	SapAmisAccionesRelacion obtenerObjetoSapAmisAccionesRelacion(String vin, CatAlertasSapAmis alerta, String idCotizacion);
	EnvioConsultaAlertas getInfoEnvioConsultaAlertas(String vin);
	ArrayList<RespuestaSapAmisAlerta> getAlertasSapAmisCotizacion(ArrayList<EnvioConsultaAcciones> respuesConsultaLinea);
	ArrayList<RespuestaSapAmisAlerta> getAlertasSapAmisEmision(RespuestaSapAmisAlerta respuestaAlerta,PolizaDTO poliza);
}
