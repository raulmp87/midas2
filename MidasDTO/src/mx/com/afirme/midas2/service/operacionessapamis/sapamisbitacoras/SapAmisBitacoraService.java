package mx.com.afirme.midas2.service.operacionessapamis.sapamisbitacoras;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasCesvi;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasCii;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasEmision;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasOcra;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasPrevencion;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasPt;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasScd;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasSiniestro;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasSipac;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasValuacion;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasistemasEnvio;
import mx.com.afirme.midas2.dto.sapamis.otros.SapAmisBitacoraEmision;
import mx.com.afirme.midas2.dto.sapamis.otros.SapAmisBitacoraSiniestros;
import mx.com.afirme.midas2.dto.sapamis.otros.SapAmisControlEmision;
import mx.com.afirme.midas2.dto.sapamis.otros.SapAmisControlSiniestros;

@Local
public interface SapAmisBitacoraService {
	
	public List<SapAmisBitacoraEmision> obtenerBitacoraEmisionFiltrada(String bitacoraPoliza ,String bitacoraVin ,String bitacoraFechaEnvio,
			String estatusEnvio,String cesvi,String cii,String emision,String ocra,String prevencion,String pt,String csd,
			String siniestro, String sipac , String valuacion);
	
	public SapAlertasistemasEnvio obtenerAlertas(String idEncabezadoAlerta);

	public List<SapAmisBitacoraSiniestros> obtenerBitacoraSiniestrosFiltrada (String bitacoraPoliza ,String bitacoraVin ,String bitacoraFechaEnvio,
			String estatusEnvio,String cesvi,String cii,String emision,String ocra,String prevencion,String pt,String csd,
			String siniestro, String sipac , String valuacion);
	
	public void guardarEmision(SapAlertasistemasEnvio alertasEncabezado,
			ArrayList<SapAlertasCii>listaAlertasCii, ArrayList<SapAlertasEmision>listaAlertasEmision,
			ArrayList<SapAlertasOcra>listaAlertasOcra,ArrayList<SapAlertasPrevencion>listaAlertasPrevencion, 
			ArrayList<SapAlertasPt>listaAlertasPt, ArrayList<SapAlertasScd>listaAlertasScd, 
			ArrayList<SapAlertasSiniestro>listaAlertasSiniestro, 
			ArrayList<SapAlertasSipac>listaAlertasSipac,ArrayList<SapAlertasValuacion>listaAlertasValuacion, 
			ArrayList<SapAlertasCesvi>listaAlertasCesvi,SapAmisBitacoraEmision emisionBitacora, SapAmisControlEmision emision);
	
	public void guardarSiniestro(SapAlertasistemasEnvio alertasEncabezado,
			ArrayList<SapAlertasCii>listaAlertasCii, ArrayList<SapAlertasEmision>listaAlertasEmision,
			ArrayList<SapAlertasOcra>listaAlertasOcra,ArrayList<SapAlertasPrevencion>listaAlertasPrevencion, 
			ArrayList<SapAlertasPt>listaAlertasPt, ArrayList<SapAlertasScd>listaAlertasScd, 
			ArrayList<SapAlertasSiniestro>listaAlertasSiniestro, 
			ArrayList<SapAlertasSipac>listaAlertasSipac,ArrayList<SapAlertasValuacion>listaAlertasValuacion, 
			ArrayList<SapAlertasCesvi>listaAlertasCesvi,SapAmisBitacoraSiniestros siniestroBitacora, SapAmisControlSiniestros siniestro);

}
