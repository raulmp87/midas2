package mx.com.afirme.midas2.service.endoso.cotizacion.auto;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.endoso.cotizacion.auto.CotizacionEndosoDTO;

@Local
public interface CotizacionEndosoService {
		
	/**
	 * Listado de Cotizaciones de Endoso
	 * Desde la Bandeja de Cotizaciones: Autos/Cotizaciones/Endosos
	 * @param cotizacionEndosoDTO objeto con filtros
	 */
	public List<CotizacionEndosoDTO> buscarCotizacionesEndosoFiltrado(CotizacionEndosoDTO cotizacionEndosoDTO);
	
	/**
	 * Total de Cotizaciones de Endoso
	 * Desde la Bandeja de Cotizaciones: Autos/Cotizaciones/Endosos
	 * @param cotizacionEndosoDTO objeto con filtros
	 */
	public Long obtenerTotalPaginacionCotizacionesEndoso(CotizacionEndosoDTO cotizacionEndosoDTO);
	
	
	/**
	 * Listado de Cotizaciones de Endoso de una poliza
	 * Desde Listado de Cotizaciones de Poliza: Autos/Emision/Consulta Por Poliza
	 * @param cotizacionEndosoDTO objeto con filtro Poliza y Cotizacion
	 */
	public List<CotizacionEndosoDTO> buscarCotizacionesEndosoPoliza(CotizacionEndosoDTO cotizacionEndosoDTO);
	
	/**
	 * Total de Cotizaciones de Endoso de una poliza
	 * Desde Listado de Cotizaciones de Poliza: Autos/Emision/Consulta Por Poliza
	 * @param cotizacionEndosoDTO objeto con filtro Poliza y Cotizacion
	 */
	public Long obtenerTotalPaginacionCotizacionesEndosoPoliza(CotizacionEndosoDTO cotizacionEndosoDTO);
	
	/***
	 * Método que obtiene el listado de endosos de un Inciso.
	 */
	public List<CotizacionEndosoDTO> obtenerEndosoInciso(Long incisoConntinuity);
	
	/***
	 * Método que obtiene el listado de endosos de un Inciso filtrado por fecha inicial y final de vigencia del endoso, lista de usuario de creacion del endoso 
	 */
	public List<CotizacionEndosoDTO> obtenerEndosoInciso(Long incisoConntinuity,Date fechaBusquedaInicial,Date fechaBusquedaFinal, List<String> usuarios);

}