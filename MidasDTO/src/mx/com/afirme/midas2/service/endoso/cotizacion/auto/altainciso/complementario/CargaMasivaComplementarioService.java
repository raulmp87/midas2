package mx.com.afirme.midas2.service.endoso.cotizacion.auto.altainciso.complementario;

import java.math.BigDecimal;

import javax.ejb.Local;

import org.joda.time.DateTime;

import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.catalogos.banco.BancoMidasService;

@Local
public interface CargaMasivaComplementarioService {

	public TransporteImpresionDTO procesar(BigDecimal idCotizacion, BigDecimal idToControlArchivo, DateTime fechaIniVigenciaEndoso,BancoMidasService bancoMidasService,IncisoViewService incisoService,ListadoService listadoService,ClienteFacadeRemote clienteFacadeRemote);
	
	public TransporteImpresionDTO obtenerPlantilla(BigDecimal idCotizacion, DateTime fechaIniVigenciaEndoso,ListadoService listadoService);
	
}
