package mx.com.afirme.midas2.service.endoso.cotizacion.auto;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas2.dto.endoso.EndosoAdicionalDTO;
import mx.com.afirme.midas2.dto.endoso.EndosoTransporteDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.ControlDinamicoRiesgoDTO;


public interface EndosoWSMidasAutosService {
	
	public static final BigDecimal GRUPO_PARAMETRO_COBERTURA_ADICIONAL = new BigDecimal("97");
	public static final BigDecimal PAQUETE_COBERTURA_AMPLIA = new BigDecimal("970031");
	
	Map<String, Object> cotizar(EndosoTransporteDTO dto, PolizaDTO poliza) throws SystemException;
	
	Short cotizar(EndosoAdicionalDTO dto, PolizaDTO poliza) throws SystemException;
	
	Map<String, Object> emitir(EndosoTransporteDTO dto) throws SystemException;
	
	byte[] imprimirEndoso(EndosoTransporteDTO dto, PolizaDTO poliza) throws SystemException;
	
	List<ControlDinamicoRiesgoDTO> traerDatosRiesgo(PolizaDTO poliza) throws SystemException;
	
	EndosoDTO getUltimoEndoso (BigDecimal idToPoliza);
}
