/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.service.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaParametros;
import mx.com.afirme.midas2.domain.compensaciones.CaRangos;

/**
 * Local interface for RangoscaFacade.
 * @author MyEclipse Persistence Tools
 */
@Local

public interface CaRangosService {
	/**
	 Perform an initial save of a previously unsaved CaRangos entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaRangos entity to persist
	  @throws RuntimeException when the operation fails
	 */
   public void save(CaRangos entity);
   /**
	 Delete a persistent CaRangos entity.
	  @param entity CaRangos entity to delete
	 @throws RuntimeException when the operation fails
	 */
   public void delete(CaRangos entity);
  /**
	 Persist a previously saved CaRangos entity and return it or a copy of it to the sender. 
	 A copy of the CaRangos entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaRangos entity to update
	 @return CaRangos the persisted CaRangos entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CaRangos update(CaRangos entity);
	public CaRangos findById( Long id);
	 /**
	 * Find all CaRangos entities with a specific property value.  
	 
	  @param propertyName the name of the CaRangos property to query
	  @param value the property value to match
	  	  @return List<CaRangos> found by query
	 */
	public List<CaRangos> findByProperty(String propertyName, Object value
		);
	public List<CaRangos> findByNombre(Object nombre
		);
	public List<CaRangos> findByValorminimo(Object valorminimo
		);
	public List<CaRangos> findByValormaximo(Object valormaximo
		);
	public List<CaRangos> findByUsuario(Object usuario
		);
	public List<CaRangos> findByBorradologico(Object borradologico
		);
	public List<CaRangos> findByValorcompensacion(Object valorcompensacion
		);
	/**
	 * Find all CaRangos entities.
	  	  @return List<CaRangos> all CaRangos entities
	 */
	public List<CaRangos> findAll();
	public List<CaRangos> findAllbyParametrosGralesId(Long parametrosGralesId);	
	public void guardarRangos(CaParametros caParametros, String rangos);	
	public void guardarRangos(CaParametros caParametros, List<CaRangos> rangos);
}