/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */
package mx.com.afirme.midas2.service.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaTipoCompensacion;

/**
 * Local interface for TipoCompensacioncaFacade.
 * @author MyEclipse Persistence Tools
 */
@Local

public interface CaTipoCompensacionService {
		/**
	 Perform an initial save of a previously unsaved CaTipoCompensacion entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaTipoCompensacion entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaTipoCompensacion entity);
    /**
	 Delete a persistent CaTipoCompensacion entity.
	  @param entity CaTipoCompensacion entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaTipoCompensacion entity);
   /**
	 Persist a previously saved CaTipoCompensacion entity and return it or a copy of it to the sender. 
	 A copy of the CaTipoCompensacion entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaTipoCompensacion entity to update
	 @return CaTipoCompensacion the persisted CaTipoCompensacion entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CaTipoCompensacion update(CaTipoCompensacion entity);
	public CaTipoCompensacion findById( Long id);
	 /**
	 * Find all CaTipoCompensacion entities with a specific property value.  
	 
	  @param propertyName the name of the CaTipoCompensacion property to query
	  @param value the property value to match
	  	  @return List<CaTipoCompensacion> found by query
	 */
	public List<CaTipoCompensacion> findByProperty(String propertyName, Object value
		);
	public List<CaTipoCompensacion> findByNombre(Object nombre
		);
	public List<CaTipoCompensacion> findByValor(Object valor
		);
	public List<CaTipoCompensacion> findByUsuario(Object usuario
		);
	public List<CaTipoCompensacion> findByBorradologico(Object borradologico
		);
	/**
	 * Find all CaTipoCompensacion entities.
	  	  @return List<CaTipoCompensacion> all CaTipoCompensacion entities
	 */
	public List<CaTipoCompensacion> findAll(
		);
}