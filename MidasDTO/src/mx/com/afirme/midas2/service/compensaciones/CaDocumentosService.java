/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */
package mx.com.afirme.midas2.service.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaDocumentos;

/**
 * Local interface for DocumentoscaFacade.
 * @author MyEclipse Persistence Tools
 */
@Local

public interface CaDocumentosService {
		/**
	 Perform an initial save of a previously unsaved CaDocumentos entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaDocumentos entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaDocumentos entity);
    /**
	 Delete a persistent CaDocumentos entity.
	  @param entity CaDocumentos entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaDocumentos entity);
   /**
	 Persist a previously saved CaDocumentos entity and return it or a copy of it to the sender. 
	 A copy of the CaDocumentos entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaDocumentos entity to update
	 @return CaDocumentos the persisted CaDocumentos entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CaDocumentos update(CaDocumentos entity);
	public CaDocumentos findById( Long id);
	 /**
	 * Find all CaDocumentos entities with a specific property value.  
	 
	  @param propertyName the name of the CaDocumentos property to query
	  @param value the property value to match
	  	  @return List<CaDocumentos> found by query
	 */
	public List<CaDocumentos> findByProperty(String propertyName, Object value
		);
	public List<CaDocumentos> findByNombre(Object nombre
		);
	public List<CaDocumentos> findByValor(Object valor
		);
	public List<CaDocumentos> findByUsuario(Object usuario
		);
	public List<CaDocumentos> findByBorradologico(Object borradologico
		);
	/**
	 * Find all CaDocumentos entities.
	  	  @return List<CaDocumentos> all CaDocumentos entities
	 */
	public List<CaDocumentos> findAll(
		);	
}