/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Abril 2016
 * @author Mario Dominguez
 * 
 */ 
package mx.com.afirme.midas2.service.compensaciones;

import java.util.List;
import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaBancaGerencia;


@Local

public interface CaBancaGerenciaService {
    public void save(CaBancaGerencia entity);
    public void delete(CaBancaGerencia entity);
	public CaBancaGerencia update(CaBancaGerencia entity);
	public CaBancaGerencia findById( Long id);
	public List<CaBancaGerencia> findByProperty(String propertyName, Object value);
	public List<CaBancaGerencia> findAll();
}