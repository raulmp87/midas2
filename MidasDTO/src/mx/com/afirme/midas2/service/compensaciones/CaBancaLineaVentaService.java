/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Abril 2016
 * @author Mario Dominguez
 * 
 */ 
package mx.com.afirme.midas2.service.compensaciones;

import java.util.List;
import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaBancaLineaVenta;


@Local

public interface CaBancaLineaVentaService {
    public void save(CaBancaLineaVenta entity);
    public void delete(CaBancaLineaVenta entity);
	public CaBancaLineaVenta update(CaBancaLineaVenta entity);
	public CaBancaLineaVenta findById( Long id);
	public List<CaBancaLineaVenta> findByProperty(String propertyName, Object value);
	public List<CaBancaLineaVenta> findAll();
}