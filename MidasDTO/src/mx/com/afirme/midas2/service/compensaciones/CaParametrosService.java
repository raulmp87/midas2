/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */
package mx.com.afirme.midas2.service.compensaciones;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaCompensacion;
import mx.com.afirme.midas2.domain.compensaciones.CaEntidadPersona;
import mx.com.afirme.midas2.domain.compensaciones.CaParametros;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoCompensacion;
import mx.com.afirme.midas2.dto.compensaciones.CompensacionesDTO;

@Local

public interface CaParametrosService {
	/**
	 * 
	 * @param compensAdicioId
	 * @return Una lista de CaParametros que coincidan con la busqueda en base al parametro enviado
	 * @throws RuntimeException when the operation fails
	 */
	public List<CaParametros> findAllbyCompensadicioId(BigDecimal compensAdicioId);
	
		/**
	 Perform an initial save of a previously unsaved CaParametros entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaParametros entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaParametros entity);
    /**
	 Delete a persistent CaParametros entity.
	  @param entity CaParametros entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaParametros entity);
   /**
	 Persist a previously saved CaParametros entity and return it or a copy of it to the sender. 
	 A copy of the CaParametros entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaParametros entity to update
	 @return CaParametros the persisted CaParametros entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CaParametros update(CaParametros entity);
	public CaParametros findById( Long id);
	 /**
	 * Find all CaParametros entities with a specific property value.  
	 
	  @param propertyName the name of the CaParametros property to query
	  @param value the property value to match
	  	  @return List<CaParametros> found by query
	 */
	public List<CaParametros> findByProperty(String propertyName, Object value
		);
	public List<CaParametros> findByMontoutilidades(Object montoutilidades
		);
	public List<CaParametros> findByPorcentajepago(Object porcentajepago
		);
	public List<CaParametros> findByMontopago(Object montopago
		);
	public List<CaParametros> findByPorcentajeagente(Object porcentajeagente
		);
	public List<CaParametros> findByPorcentajepromotor(Object porcentajepromotor
		);
	public List<CaParametros> findByEmisioninterna(Object emisioninterna
		);
	public List<CaParametros> findByEmisionexterna(Object emisionexterna
		);	
	public List<CaParametros> findByTopemaximo(Object topemaximo
			);
	public List<CaParametros> findByUsuario(Object usuario
		);
	public List<CaParametros> findByBorradologico(Object borradologico
		);
	public List<CaParametros> findByParametroactivo(Object parametroactivo
			);
	/**
	 * Find all CaParametros entities.
	  	  @return List<CaParametros> all CaParametros entities
	 */
	public List<CaParametros> findAll(
		);	
	/**
	 * 
	 * @param compensAdicioId
	 * @param tipoCompensId
	 * @return Un objeto del tipo CaParametros que coincida con los parametros de busqueda
	 */
	public CaParametros findByCompensAdicioIdTipoCompensId(Long compensAdicioId,Long tipoCompensId);
	public CaParametros findByTipocompeidCompeIdPersoId(CaTipoCompensacion tipoCompensacion,CaCompensacion compensacion,CaEntidadPersona entidadPersona);
	public List<CaParametros> findAllbyCompensacionEntidadpersona(CaCompensacion compensacion,CaEntidadPersona entidadPersona);
	public void findParametrosForCompensacion(CaCompensacion caCompensacion, CompensacionesDTO compensacionesDTO);
}