package mx.com.afirme.midas2.service.compensaciones.reportes;

import java.util.List;
import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaReportesDTO;

/**
 * Local interface for LiquidacionCompensacionesFacade.
 * @author MyEclipse Persistence Tools
 */
@Local

public interface ReportesCompensacionesService {
	
	public List<CaReportesDTO> filtrarReporteCompensaciones(CaReportesDTO  param);
	
	public List<CaReportesDTO> filtrarReportesPagadasDevengarCa(CaReportesDTO  param);
	
	public List<CaReportesDTO> filtrarReportesDerechoPolizaCa(CaReportesDTO  param);
	

}