/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */package mx.com.afirme.midas2.service.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaTipoProvision;

/**
 * Local interface for TipoProvisioncaFacade.
 * @author MyEclipse Persistence Tools
 */
@Local

public interface CaTipoProvisionService {
		/**
	 Perform an initial save of a previously unsaved CaTipoProvision entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaTipoProvision entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaTipoProvision entity);
    /**
	 Delete a persistent CaTipoProvision entity.
	  @param entity CaTipoProvision entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaTipoProvision entity);
   /**
	 Persist a previously saved CaTipoProvision entity and return it or a copy of it to the sender. 
	 A copy of the CaTipoProvision entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaTipoProvision entity to update
	 @return CaTipoProvision the persisted CaTipoProvision entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CaTipoProvision update(CaTipoProvision entity);
	public CaTipoProvision findById( Long id);
	 /**
	 * Find all CaTipoProvision entities with a specific property value.  
	 
	  @param propertyName the name of the CaTipoProvision property to query
	  @param value the property value to match
	  	  @return List<CaTipoProvision> found by query
	 */
	public List<CaTipoProvision> findByProperty(String propertyName, Object value
		);
	public List<CaTipoProvision> findByNombre(Object nombre
		);
	public List<CaTipoProvision> findByValor(Object valor
		);
	public List<CaTipoProvision> findByUsuario(Object usuario
		);
	public List<CaTipoProvision> findByBorradologico(Object borradologico
		);
	/**
	 * Find all CaTipoProvision entities.
	  	  @return List<CaTipoProvision> all CaTipoProvision entities
	 */
	public List<CaTipoProvision> findAll(
		);	
}