/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.service.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaAutorizacion;
import mx.com.afirme.midas2.domain.compensaciones.CaCompensacion;
import mx.com.afirme.midas2.dto.compensaciones.CompensacionesDTO;

/**
 * Local interface for AutorizacionescaServiceImpl.
 * @author MyEclipse Persistence Tools
 */
@Local

public interface CaAutorizacionService {
		/**
	 Perform an initial save of a previously unsaved CaAutorizacion entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaAutorizacion entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaAutorizacion entity);
    /**
	 Delete a persistent CaAutorizacion entity.
	  @param entity CaAutorizacion entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaAutorizacion entity);
   /**
	 Persist a previously saved CaAutorizacion entity and return it or a copy of it to the sender. 
	 A copy of the CaAutorizacion entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaAutorizacion entity to update
	 @return CaAutorizacion the persisted CaAutorizacion entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CaAutorizacion update(CaAutorizacion entity);
	public CaAutorizacion findById( Long id);
	 /**
	 * Find all CaAutorizacion entities with a specific property value.  
	 
	  @param propertyName the name of the CaAutorizacion property to query
	  @param value the property value to match
	  	  @return List<CaAutorizacion> found by query
	 */
	public List<CaAutorizacion> findByProperty(String propertyName, Object value
		);
	public List<CaAutorizacion> findByCompensacionid(Long idCompensacion
		);
	public List<CaAutorizacion> findByUsuario(Object usuario
		);
	public List<CaAutorizacion> findByBorradologico(Object borradologico
		);
	public List<CaAutorizacion> findByValor(Object valor
	);
	/**
	 * Find all CaAutorizacion entities.
	  	  @return List<CaAutorizacion> all CaAutorizacion entities
	 */
	public List<CaAutorizacion> findAll(
		);
	public List<CaAutorizacion> finByCompensidAndTipoautoid(Long compensId, Long tipoAutoId);
	public List<CaAutorizacion> finByCompensidTiposautosid(Long compensId, Long ...tipos);
	public void cargarAutorizaciones(CaCompensacion caCompensacion, CompensacionesDTO compensacionesDTO);
}