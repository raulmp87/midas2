package mx.com.afirme.midas2.service.compensaciones;

import java.io.File;
import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaBancaPrimPagCP;

@Local
public interface CaBancaPrimPagService {
	public void procesarArchivos(CaBancaPrimPagCP caBancaPrimPagCP,File filePrimasPagadas,File fileSiniestralidadMes,Double mes);
	public void guardarConfiguracionCP(CaBancaPrimPagCP caBancaPrimPagCP);
	public Object[]  procesarFileSiniestralidadMes(File fileUpload,Double mes);
	public Object[] procesarFilePrimasPagadas(File fileUpload,Double mes);
	public void guardarPrimasPagSiniMes(Object[] listPrimPag,Object[] listSiniMes,Long caBancaPrimPagCP);

}
