/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */
package mx.com.afirme.midas2.service.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaTipoRango;

/**
 * Local interface for TipoRangocaFacade.
 * @author MyEclipse Persistence Tools
 */
@Local

public interface CaTipoRangoService {
		/**
	 Perform an initial save of a previously unsaved CaTipoRango entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaTipoRango entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaTipoRango entity);
    /**
	 Delete a persistent CaTipoRango entity.
	  @param entity CaTipoRango entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaTipoRango entity);
   /**
	 Persist a previously saved CaTipoRango entity and return it or a copy of it to the sender. 
	 A copy of the CaTipoRango entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaTipoRango entity to update
	 @return CaTipoRango the persisted CaTipoRango entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CaTipoRango update(CaTipoRango entity);
	public CaTipoRango findById( Long id);
	 /**
	 * Find all CaTipoRango entities with a specific property value.  
	 
	  @param propertyName the name of the CaTipoRango property to query
	  @param value the property value to match
	  	  @return List<CaTipoRango> found by query
	 */
	public List<CaTipoRango> findByProperty(String propertyName, Object value
		);
	public List<CaTipoRango> findByNombre(Object nombre
		);	
	public List<CaTipoRango> findByValor(Object valor
		);	
	public List<CaTipoRango> findByUsuario(Object usuario
		);
	public List<CaTipoRango> findByBorradologico(Object borradologico
		);
	/**
	 * Find all CaTipoRango entities.
	  	  @return List<CaTipoRango> all CaTipoRango entities
	 */
	public List<CaTipoRango> findAll(
		);	
}