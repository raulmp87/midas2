package mx.com.afirme.midas2.service.compensaciones;


import java.util.List;
import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaCompensacion;
import mx.com.afirme.midas2.domain.compensaciones.CaCorreos;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales.RAMO;

/**
 * Local interface for CaCorreosFacade.
 * 
 * @author MyEclipse Persistence Tools
 */
@Local
public interface CaCorreosService {
	public void save(CaCorreos entity);
	
	public void delete(CaCorreos entity);
	
	public CaCorreos update(CaCorreos entity);
	
	public List<CaCorreos> findByProperty(String propertyName, Object value);

	public List<CaCorreos> findByArea(Object area);

	public List<CaCorreos> findByOficina(Object oficina);

	public List<CaCorreos> findByUsername(Object username);
	
	public void notificacionAutorizacionesCorreo(String movimiento, CaCompensacion caCompensacion);
		
	
	/**
	 * Find all CaCorreos entities.
	 * 
	 * @return List<CaCorreos> all CaCorreos entities
	 */
	public List<CaCorreos> findAll();
	
	
}