/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */
package mx.com.afirme.midas2.service.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaEstatus;

/**
 * Local interface for EstatuscaFacade.
 * @author MyEclipse Persistence Tools
 */
@Local

public interface CaEstatusService {
		/**
	 Perform an initial save of a previously unsaved CaEstatus entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaEstatus entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaEstatus entity);
    /**
	 Delete a persistent CaEstatus entity.
	  @param entity CaEstatus entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaEstatus entity);
   /**
	 Persist a previously saved CaEstatus entity and return it or a copy of it to the sender. 
	 A copy of the CaEstatus entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaEstatus entity to update
	 @return CaEstatus the persisted CaEstatus entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CaEstatus update(CaEstatus entity);
	public CaEstatus findById( Long id);
	 /**
	 * Find all CaEstatus entities with a specific property value.  
	 
	  @param propertyName the name of the CaEstatus property to query
	  @param value the property value to match
	  	  @return List<CaEstatus> found by query
	 */
	public List<CaEstatus> findByProperty(String propertyName, Object value
		);
	public List<CaEstatus> findByNombre(Object nombre
		);
	public List<CaEstatus> findByValor(Object valor
		);
	public List<CaEstatus> findByUsuario(Object usuario
		);
	public List<CaEstatus> findByBorradologico(Object borradologico
		);
	/**
	 * Find all CaEstatus entities.
	  	  @return List<CaEstatus> all CaEstatus entities
	 */
	public List<CaEstatus> findAll(
		);	
}