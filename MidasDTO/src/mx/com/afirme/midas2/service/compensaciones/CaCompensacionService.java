/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */
package mx.com.afirme.midas2.service.compensaciones;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaCompensacion;
import mx.com.afirme.midas2.dto.compensaciones.CompensacionesDTO;
import mx.com.afirme.midas2.dto.compensaciones.ProductoVidaDTO;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales.RAMO;

/**
 * Local interface for CompensAdiciocaFacade.
 * @author MyEclipse Persistence Tools
 */
@Local

public interface CaCompensacionService {
		/**
	 Perform an initial save of a previously unsaved CaCompensacion entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaCompensacion entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public CaCompensacion save(CaCompensacion entity);
    /**
	 Delete a persistent CaCompensacion entity.
	  @param entity CaCompensacion entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaCompensacion entity);
   /**
	 Persist a previously saved CaCompensacion entity and return it or a copy of it to the sender. 
	 A copy of the CaCompensacion entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaCompensacion entity to update
	 @return CaCompensacion the persisted CaCompensacion entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CaCompensacion update(CaCompensacion entity);
	public CaCompensacion findById( Long id);
	 /**
	 * Find all CaCompensacion entities with a specific property value.  
	 
	  @param propertyName the name of the CaCompensacion property to query
	  @param value the property value to match
	  	  @return List<CaCompensacion> found by query
	 */
	public List<CaCompensacion> findByProperty(String propertyName, Object value
		);
	public List<CaCompensacion> findByPolizaId(Object polizaId
		);
	public List<CaCompensacion> findByNegocioId(Object negocioId
		);
	public List<CaCompensacion> findByContraprestacion(Object contraprestacion
		);
	public List<CaCompensacion> findByCuadernoconcurso(Object cuadernoconcurso
		);
	public List<CaCompensacion> findByConvenioespecial(Object convenioespecial
		);
	public List<CaCompensacion> findByRetroactivo(Object retroactivo
		);
	public List<CaCompensacion> findByModificable(Object modificable
		);
	public List<CaCompensacion> findByExcepciondocumentos(Object excepciondocumentos
		);
	public List<CaCompensacion> findByCompensacionporsubramos(Object compensacionporsubramos
		);
	public List<CaCompensacion> findByUsuario(Object usuario
		);
	public List<CaCompensacion> findByBorradologico(Object borradologico
		);	
	public BigDecimal findIdCompensAdicioNext();
	/**
	 * Find all CaCompensacion entities.
	  	  @return List<CaCompensacion> all CaCompensacion entities
	 */
	public List<CaCompensacion> findAll();
	
	public void autorizarCompensacion(String ramo, Long compensacionId, Long identificador, Long tipoAutorizacionId, boolean flagContra);
	
	public CaCompensacion findConfiguracion(CompensacionesDTO compensacionDTO, RAMO ramo);
	
	public CaCompensacion findByRamoAndFieldAndContraprestacion(Long ramoId, String field, Object valueFiled, boolean contraprestacion);
	
	public void guardarCompensacion(List<CompensacionesDTO> listCompensacionesDTO);
	
	public int provisionarPorUtilidad(long idToPoliza, long entidadPersonaId, BigDecimal montoPago,BigDecimal montoTotal);
	
	public boolean solicitarPagoPorUtilidad(long idToPoliza, long entidadPersonaId, BigDecimal montoPago);
	
	public List<CaCompensacion> listCompensaciones(Long idRamo,  String field, Object valueField);
	
	public boolean cargarExcel(File fileUpload, Long configuracionBancaId);
	
	public List<CompensacionesDTO> getlistRecibosPendientesPagar(Long idPoliza);
	
	public List<ProductoVidaDTO> getProductosVida();
}