/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.service.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaTipoDocumento;

@Local

public interface CaTipoDocumentoService {
		/**
	 Perform an initial save of a previously unsaved CaTipoDocumento entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaTipoDocumento entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaTipoDocumento entity);
    /**
	 Delete a persistent CaTipoDocumento entity.
	  @param entity CaTipoDocumento entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaTipoDocumento entity);
   /**
	 Persist a previously saved CaTipoDocumento entity and return it or a copy of it to the sender. 
	 A copy of the CaTipoDocumento entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaTipoDocumento entity to update
	 @return CaTipoDocumento the persisted CaTipoDocumento entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CaTipoDocumento update(CaTipoDocumento entity);
	public CaTipoDocumento findById( Long id);
	 /**
	 * Find all CaTipoDocumento entities with a specific property value.  
	 
	  @param propertyName the name of the CaTipoDocumento property to query
	  @param value the property value to match
	  	  @return List<CaTipoDocumento> found by query
	 */
	public List<CaTipoDocumento> findByProperty(String propertyName, Object value
		);
	public List<CaTipoDocumento> findByNombre(Object nombre
		);
	public List<CaTipoDocumento> findByValor(Object valor
		);
	public List<CaTipoDocumento> findByUsuario(Object usuario
		);
	public List<CaTipoDocumento> findByBorradologico(Object borradologico
		);
	/**
	 * Find all CaTipoDocumento entities.
	  	  @return List<CaTipoDocumento> all CaTipoDocumento entities
	 */
	public List<CaTipoDocumento> findAll(
		);	
}