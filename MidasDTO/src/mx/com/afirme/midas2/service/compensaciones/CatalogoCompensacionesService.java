package mx.com.afirme.midas2.service.compensaciones;

import java.util.List;

import javax.ejb.Local;


import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.compensaciones.CaBaseCalculo;
import mx.com.afirme.midas2.domain.compensaciones.CaEntidadPersona;
import mx.com.afirme.midas2.domain.compensaciones.CaRamo;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoCompensacion;
import mx.com.afirme.midas2.domain.compensaciones.Proveedor;
import mx.com.afirme.midas2.domain.negocio.Negocio;

@Local
public interface CatalogoCompensacionesService {

	public List<CaRamo> listarRamos();
	
	public List<CaTipoCompensacion> listarTipoCompensacion();
	
	public List<CaBaseCalculo> listarBaseCalculo();
	
	public List<Proveedor> listarProveedores(String nombreProveedor);
	
	public List<Negocio> listarNegocios(String descripcion);
	
	public List<Proveedor> listarProveedoresId(String id);
	
	public List<CaEntidadPersona> listarAgentesProveedores(String nombre,Long tipo);
	
	public List<Promotoria> listarPromotores(String nombrePromotor);
}
