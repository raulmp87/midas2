package mx.com.afirme.midas2.service.compensaciones;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaPrimPagCP;

@Local
public interface CaBancaPrimPagCPService {

	public void save(CaBancaPrimPagCP entity);
	public BigDecimal findByAnioMes(Double anio, Double mes);
	public CaBancaPrimPagCP findConfiguracionActual(int anio, int mes);

}
