/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.service.compensaciones;

import java.util.List;
import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaBaseCalculo;


@Local

public interface CaBaseCalculoService {
		/**
	 Perform an initial save of a previously unsaved CaBaseCalculo entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaBaseCalculo entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaBaseCalculo entity);
    /**
	 Delete a persistent CaBaseCalculo entity.
	  @param entity CaBaseCalculo entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaBaseCalculo entity);
   /**
	 Persist a previously saved CaBaseCalculo entity and return it or a copy of it to the sender. 
	 A copy of the CaBaseCalculo entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaBaseCalculo entity to update
	 @return CaBaseCalculo the persisted CaBaseCalculo entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CaBaseCalculo update(CaBaseCalculo entity);
	public CaBaseCalculo findById( Long id);
	 /**
	 * Find all CaBaseCalculo entities with a specific property value.  
	 
	  @param propertyName the name of the CaBaseCalculo property to query
	  @param value the property value to match
	  	  @return List<CaBaseCalculo> found by query
	 */
	public List<CaBaseCalculo> findByProperty(String propertyName, Object value
		);
	public List<CaBaseCalculo> findByNombre(Object nombre
		);
	public List<CaBaseCalculo> findByValor(Object valor
		);
	public List<CaBaseCalculo> findByUsuario(Object usuario
		);
	public List<CaBaseCalculo> findByBorradologico(Object borradologico
		);
	/**
	 * Find all CaBaseCalculo entities.
	  	  @return List<CaBaseCalculo> all CaBaseCalculo entities
	 */
	public List<CaBaseCalculo> findAll(
		);
	
	public List<CaBaseCalculo> findByProperties(String propertyName, int ...isValues);
}