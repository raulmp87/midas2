package mx.com.afirme.midas2.service.compensaciones;

import java.io.File;
import javax.ejb.Local;

@Local
public interface CaBancaSiniMesService {
	public boolean cargarExcel(File fileUpload, Long BancaPrimPagId,Double mes);

}
