/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */
package mx.com.afirme.midas2.service.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaParametros;
import mx.com.afirme.midas2.domain.compensaciones.CaSubramosDanios;
import mx.com.afirme.midas2.dto.compensaciones.CompensacionesDTO;
import mx.com.afirme.midas2.dto.compensaciones.SubRamosDaniosView;
import mx.com.afirme.midas2.dto.compensaciones.SubRamosDaniosViewDTO;

/**
 * Local interface for SubramosDanioscaFacade.
 * @author MyEclipse Persistence Tools
 */
@Local

public interface CaSubramosDaniosService {
		/**
	 Perform an initial save of a previously unsaved CaSubramosDanios entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaSubramosDanios entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaSubramosDanios entity);
    /**
	 Delete a persistent CaSubramosDanios entity.
	  @param entity CaSubramosDanios entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaSubramosDanios entity);
   /**
	 Persist a previously saved CaSubramosDanios entity and return it or a copy of it to the sender. 
	 A copy of the CaSubramosDanios entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaSubramosDanios entity to update
	 @return CaSubramosDanios the persisted CaSubramosDanios entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CaSubramosDanios update(CaSubramosDanios entity);
	public CaSubramosDanios findById( Long id);
	 /**
	 * Find all CaSubramosDanios entities with a specific property value.  
	 
	  @param propertyName the name of the CaSubramosDanios property to query
	  @param value the property value to match
	  	  @return List<CaSubramosDanios> found by query
	 */
	public List<CaSubramosDanios> findByProperty(String propertyName, Object value
		);
	public List<CaSubramosDanios> findByPorcentajecompensacion(Object porcentajecompensacion
		);
	public List<CaSubramosDanios> findByUsuario(Object usuario
		);
	public List<CaSubramosDanios> findByBorradologico(Object borradologico
		);
	/**
	 * Find all CaSubramosDanios entities.
	  	  @return List<CaSubramosDanios> all CaSubramosDanios entities
	 */
	public List<CaSubramosDanios> findAll(
		);	
	
	public void findSubRamos(SubRamosDaniosViewDTO subDaniosViewDTO,CompensacionesDTO compensacionesDTO);
	
	public void guardarListadoSubRamosDanios(List<SubRamosDaniosView> listSubRamosDanios, Long cotizacionId, Long cotizacionEndosoId, CaParametros parametros);
	
	public void calcularProvisionDanios(SubRamosDaniosViewDTO subDaniosViewDTO,CompensacionesDTO compensacionesDTO);
	
	public Long findCotizacionForIdCotizacionEndoso(Long idCotizacionEndoso);
	
}