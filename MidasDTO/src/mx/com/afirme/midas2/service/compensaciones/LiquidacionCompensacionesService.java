package mx.com.afirme.midas2.service.compensaciones;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.persistence.PersistenceException;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.EnvioFacturaAgente;
import mx.com.afirme.midas2.domain.compensaciones.DatosDetallePagosPorRecibo;
import mx.com.afirme.midas2.domain.compensaciones.LiquidacionCompensaciones;
import mx.com.afirme.midas2.domain.compensaciones.OrdenPagosDetalle;
import mx.com.afirme.midas2.domain.compensaciones.OrdenesPagoDTO;
import mx.com.afirme.midas2.domain.compensaciones.OrdenesPagoFiltroDTO;
import mx.com.afirme.midas2.domain.compensaciones.ReporteFiltrosDTO;
import mx.com.afirme.midas2.domain.siniestros.solicitudcheque.SolicitudChequeSiniestro;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
/**
 * Local interface for LiquidacionCompensacionesFacade.
 * @author MyEclipse Persistence Tools
 */
@Local

public interface LiquidacionCompensacionesService {
		/**
	 Perform an initial save of a previously unsaved LiquidacionCompensaciones entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity LiquidacionCompensaciones entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(LiquidacionCompensaciones entity);
    /**
	 Delete a persistent LiquidacionCompensaciones entity.
	  @param entity LiquidacionCompensaciones entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(LiquidacionCompensaciones entity);
   /**
	 Persist a previously saved LiquidacionCompensaciones entity and return it or a copy of it to the sender. 
	 A copy of the LiquidacionCompensaciones entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity LiquidacionCompensaciones entity to update
	 @return LiquidacionCompensaciones the persisted LiquidacionCompensaciones entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public LiquidacionCompensaciones update(LiquidacionCompensaciones entity);
	public LiquidacionCompensaciones findById(Long id);
	public List<OrdenPagosDetalle> findByLiquidacionId(Long liquidacionId);
	 /**
	 * Find all LiquidacionCompensaciones entities with a specific property value.  
	 
	  @param propertyName the name of the LiquidacionCompensaciones property to query
	  @param value the property value to match
	  	  @return List<LiquidacionCompensaciones> found by query
	 */
	public List<LiquidacionCompensaciones> findByProperty(String propertyName, Object value);
	public List<LiquidacionCompensaciones> findByEstatus(Object estatus);
	public List<LiquidacionCompensaciones> obtenerLiquidacionesPorFiltro(String estatus, Long idAgente);
	public List<LiquidacionCompensaciones> obtenerLiquidacionesPorFiltro(Long idAgente);
	public List<LiquidacionCompensaciones> findByIdTcmoneda(Object idTcmoneda);
	public List<LiquidacionCompensaciones> findByTipoOperacion(Object tipoOperacion);
	public List<LiquidacionCompensaciones> findByTipoLiquidacion(Object tipoLiquidacion);
	public List<LiquidacionCompensaciones> findByBanco(Object banco);
	public List<LiquidacionCompensaciones> findByCuenta(Object cuenta);
	public List<LiquidacionCompensaciones> findByNoChequeRef(Object noChequeRef);
	public List<LiquidacionCompensaciones> findBySolicitadoPor(Object solicitadoPor);
	public List<LiquidacionCompensaciones> findByAutorizadoPor(Object autorizadoPor);
	public List<LiquidacionCompensaciones> findByRechazadoPor(Object rechazadoPor);
	public List<LiquidacionCompensaciones> findByImportePrima(Object importePrima);
	public List<LiquidacionCompensaciones> findByImporteBs(Object importeBs);
	public List<LiquidacionCompensaciones> findByImporteDerpol(Object importeDerpol);
	public List<LiquidacionCompensaciones> findByImporteCumMeta(Object importeCumMeta);
	public List<LiquidacionCompensaciones> findByImporteUtilidad(Object importeUtilidad);
	public List<LiquidacionCompensaciones> findBySubtotal(Object subtotal);
	public List<LiquidacionCompensaciones> findByImporteTotal(Object importeTotal);
	public List<LiquidacionCompensaciones> findByIva(Object iva);
	public List<LiquidacionCompensaciones> findByIvaRetenido(Object ivaRetenido);
	public List<LiquidacionCompensaciones> findByIsr(Object isr);
	public List<LiquidacionCompensaciones> findByComentarios(Object comentarios);
	public List<LiquidacionCompensaciones> findByCodigoUsuarioCreacion(Object codigoUsuarioCreacion);
	public List<LiquidacionCompensaciones> findByCodigoUsuarioModificacion(Object codigoUsuarioModificacion);
	public List<LiquidacionCompensaciones> findByOrigenLiquidacion(Object origenLiquidacion);
	public List<LiquidacionCompensaciones> findByBeneficiario(Object beneficiario);
	public List<LiquidacionCompensaciones> findByBancoBeneficiario(Object bancoBeneficiario);
	public List<LiquidacionCompensaciones> findByClabeBeneficiario(Object clabeBeneficiario);
	public List<LiquidacionCompensaciones> findByRfcBeneficiario(Object rfcBeneficiario);
	public List<LiquidacionCompensaciones> findByCorreo(Object correo);
	public List<LiquidacionCompensaciones> findByTelefonoLada(Object telefonoLada);
	public List<LiquidacionCompensaciones> findByTelefonoNumero(Object telefonoNumero);
	public List<LiquidacionCompensaciones> findByIdRemesa(Object idRemesa);
	public List<LiquidacionCompensaciones> findByNetoPorPagar(Object netoPorPagar);
	/**
	 * Find all LiquidacionCompensaciones entities.
	  	  @return List<LiquidacionCompensaciones> all LiquidacionCompensaciones entities
	 */
	public List<LiquidacionCompensaciones> findAll();
	
	public List<OrdenesPagoDTO> agruparOrden(OrdenesPagoDTO  param);
	
	public String generarOrdenPagos(String liquidacionesId);
	
	public List<SolicitudChequeSiniestro> contabilizarCheques(String liquidacionesId);
	
	public void excluirOrdenPago(Long idLiquidacion, Long idRecibo);
	
	public void enviarLiquidacionFactura(Long idLiquidacion);
	
	public void eliminarLiquidacion(Long idLiquidacion);
	
	public List<OrdenesPagoDTO> obtenerOrdenPagoProceso(String  usuario);
		
	public List<OrdenesPagoDTO> obtenerOrdenPagoPendXPag(String  usuario);
	
	public void migrarChequeMizarCompensaciones(Long idLiquidacion) throws Exception;
	
	public List<LiquidacionCompensaciones> obtenerOrdenPagosGenerado();
	
	public List<LiquidacionCompensaciones>obtenerOrdenPagoSoliFactura(String estatus);
	
	public EnvioFacturaAgente persistEnvioFacturaCa(Long idAgente, Long idLiquidacion,File archivo, String userName) throws FileNotFoundException, PersistenceException, Exception;
	
	public List<DatosDetallePagosPorRecibo> buscarOrdenesporLiquidacion(Long liquidacionId);
	
	public List<OrdenesPagoDTO> buscarLiquidacionXEstatus(OrdenesPagoDTO  param);
	
	public TransporteImpresionDTO obtenerReciboHonorarios(ReporteFiltrosDTO parametros);
	
	public OrdenesPagoDTO  getReporteAutorizarOrdenPagos(String idLiquidacion);

	public OrdenesPagoFiltroDTO obtenerFiltrosOrdenPagos(Long tipo);
	
	public Map<String,Object> getReporteDetallePrimas(ReporteFiltrosDTO parametros);

	}