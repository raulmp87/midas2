/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */
package mx.com.afirme.midas2.service.compensaciones;


import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaTipoCondicionCalculo;

@Local

public interface CaTipoCondicionCalculoService {
		/**
	 Perform an initial save of a previously unsaved CaTipoCondicionCalculo entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaTipoCondicionCalculo entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaTipoCondicionCalculo entity);
    /**
	 Delete a persistent CaTipoCondicionCalculo entity.
	  @param entity CaTipoCondicionCalculo entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaTipoCondicionCalculo entity);
   /**
	 Persist a previously saved CaTipoCondicionCalculo entity and return it or a copy of it to the sender. 
	 A copy of the CaTipoCondicionCalculo entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaTipoCondicionCalculo entity to update
	 @return CaTipoCondicionCalculo the persisted CaTipoCondicionCalculo entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CaTipoCondicionCalculo update(CaTipoCondicionCalculo entity);
	public CaTipoCondicionCalculo findById( Long id);
	 /**
	 * Find all CaTipoCondicionCalculo entities with a specific property value.  
	 
	  @param propertyName the name of the CaTipoCondicionCalculo property to query
	  @param value the property value to match
	  	  @return List<CaTipoCondicionCalculo> found by query
	 */
	public List<CaTipoCondicionCalculo> findByProperty(String propertyName, Object value
		);
	public List<CaTipoCondicionCalculo> findByNombre(Object nombre
		);
	public List<CaTipoCondicionCalculo> findByValor(Object valor
		);
	public List<CaTipoCondicionCalculo> findByUsuario(Object usuario
		);
	public List<CaTipoCondicionCalculo> findByBorradologico(Object borradologico
		);
	/**
	 * Find all CaTipoCondicionCalculo entities.
	  	  @return List<CaTipoCondicionCalculo> all CaTipoCondicionCalculo entities
	 */
	public List<CaTipoCondicionCalculo> findAll(
		);	
}