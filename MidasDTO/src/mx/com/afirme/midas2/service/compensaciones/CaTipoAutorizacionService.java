/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */
package mx.com.afirme.midas2.service.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaTipoAutorizacion;

/**
 * Local interface for TipoAutorizacioncaServiceImpl.
 * @author MyEclipse Persistence Tools
 */
@Local

public interface CaTipoAutorizacionService {
		/**
	 Perform an initial save of a previously unsaved CaTipoAutorizacion entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaTipoAutorizacion entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaTipoAutorizacion entity);
    /**
	 Delete a persistent CaTipoAutorizacion entity.
	  @param entity CaTipoAutorizacion entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaTipoAutorizacion entity);
   /**
	 Persist a previously saved CaTipoAutorizacion entity and return it or a copy of it to the sender. 
	 A copy of the CaTipoAutorizacion entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaTipoAutorizacion entity to update
	 @return CaTipoAutorizacion the persisted CaTipoAutorizacion entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CaTipoAutorizacion update(CaTipoAutorizacion entity);
	public CaTipoAutorizacion findById( Long id);
	 /**
	 * Find all CaTipoAutorizacion entities with a specific property value.  
	 
	  @param propertyName the name of the CaTipoAutorizacion property to query
	  @param value the property value to match
	  	  @return List<CaTipoAutorizacion> found by query
	 */
	public List<CaTipoAutorizacion> findByProperty(String propertyName, Object value
		);
	public List<CaTipoAutorizacion> findByNombre(Object nombre
		);
	public List<CaTipoAutorizacion> findByValor(Object valor
		);
	public List<CaTipoAutorizacion> findByUsuario(Object usuario
		);
	public List<CaTipoAutorizacion> findByBorradologico(Object borradologico
		);
	/**
	 * Find all CaTipoAutorizacion entities.
	  	  @return List<CaTipoAutorizacion> all CaTipoAutorizacion entities
	 */
	public List<CaTipoAutorizacion> findAll(
		);	
}