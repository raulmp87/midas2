/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */
package mx.com.afirme.midas2.service.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaTipoContrato;

/**
 * Local interface for TipoContratocaFacade.
 * @author MyEclipse Persistence Tools
 */
@Local

public interface CaTipoContratoService {
		/**
	 Perform an initial save of a previously unsaved CaTipoContrato entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaTipoContrato entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaTipoContrato entity);
    /**
	 Delete a persistent CaTipoContrato entity.
	  @param entity CaTipoContrato entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaTipoContrato entity);
   /**
	 Persist a previously saved CaTipoContrato entity and return it or a copy of it to the sender. 
	 A copy of the CaTipoContrato entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaTipoContrato entity to update
	 @return CaTipoContrato the persisted CaTipoContrato entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CaTipoContrato update(CaTipoContrato entity);
	public CaTipoContrato findById( Long id);
	 /**
	 * Find all CaTipoContrato entities with a specific property value.  
	 
	  @param propertyName the name of the CaTipoContrato property to query
	  @param value the property value to match
	  	  @return List<CaTipoContrato> found by query
	 */
	public List<CaTipoContrato> findByProperty(String propertyName, Object value
		);
	public List<CaTipoContrato> findByNombre(Object nombre
		);
	public List<CaTipoContrato> findByValor(Object valor
		);
	public List<CaTipoContrato> findByUsuario(Object usuario
		);
	public List<CaTipoContrato> findByBorradologico(Object borradologico
		);
	/**
	 * Find all CaTipoContrato entities.
	  	  @return List<CaTipoContrato> all CaTipoContrato entities
	 */
	public List<CaTipoContrato> findAll(
		);	
}