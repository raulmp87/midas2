package mx.com.afirme.midas2.service.compensaciones;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaAutorizarConf;



/**
 * Local interface for CaAutorizarConfFacade.
 * 
 * @author MyEclipse Persistence Tools
 */
@Local
public interface CaAutorizarConfService {

	public void save(CaAutorizarConf entity);

	public void delete(CaAutorizarConf entity);

	public CaAutorizarConf  update(CaAutorizarConf entity);

	public CaAutorizarConf findById(Long id);

	public List<CaAutorizarConf> findByProperty(String propertyName,
			Object value);

	public List<CaAutorizarConf> findByEndoso(Object endoso);

	public List<CaAutorizarConf> findByModificable(Object modificable);

	public List<CaAutorizarConf> findAll();
	
	public boolean esModificable(String ramo, Long tipoEndoso);
	
	public Short obtenerTipoEndoso(BigDecimal idToCotizacion,Short numeroEndoso);
    }


