/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.service.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.compensaciones.CaEntidadPersona;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.DatosSeycos;

/**
 * Local interface for EntidadPersonacaFacade.
 * @author MyEclipse Persistence Tools
 */
@Local

public interface CaEntidadPersonaService {
		/**
	 Perform an initial save of a previously unsaved CaEntidadPersona entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaEntidadPersona entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaEntidadPersona entity);
    /**
	 Delete a persistent CaEntidadPersona entity.
	  @param entity CaEntidadPersona entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaEntidadPersona entity);
   /**
	 Persist a previously saved CaEntidadPersona entity and return it or a copy of it to the sender. 
	 A copy of the CaEntidadPersona entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaEntidadPersona entity to update
	 @return CaEntidadPersona the persisted CaEntidadPersona entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CaEntidadPersona update(CaEntidadPersona entity);
	public CaEntidadPersona findById( Long id);
	 /**
	 * Find all CaEntidadPersona entities with a specific property value.  
	 
	  @param propertyName the name of the CaEntidadPersona property to query
	  @param value the property value to match
	  	  @return List<CaEntidadPersona> found by query
	 */
	public List<CaEntidadPersona> findByProperty(String propertyName, Object value
		);
	public CaEntidadPersona findByPropertySingleResult(String propertyName, final Object value);
	public List<CaEntidadPersona> findByCompensAdicioId(Long compensAdicioId);
	public List<CaEntidadPersona> findByUsuario(Object usuario
		);
	public List<CaEntidadPersona> findByNombres(Object nombres
		);
	public List<CaEntidadPersona> findByParametrosId(Object parametrosId
		);
	public List<CaEntidadPersona> findByApaterno(Object apaterno
		);
	public List<CaEntidadPersona> findByAmaterno(Object amaterno
		);
	public List<CaEntidadPersona> findByBorradologico(Object borradologico
		);
	/**
	 * Find all CaEntidadPersona entities.
	  	  @return List<CaEntidadPersona> all CaEntidadPersona entities
	 */
	public List<CaEntidadPersona> findAll(
		);
	
	public CaEntidadPersona findByCompensacionAndTipoEntidadAndClave(Long compensAdicioId, Long clave, Long tipoEntidad);
	public CaEntidadPersona findByCompensacionAndTipoEntidadAndClave(Long compensAdicioId, DatosSeycos datosSeycos, Long tipoEntidad);
	public List<CaEntidadPersona> findByProveedorId(Long proveedorId);
	public List<CaEntidadPersona> findByAgente(Agente agente);
}