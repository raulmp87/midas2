/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.service.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaRamo;

@Local

public interface CaRamoService {
		/**
	 Perform an initial save of a previously unsaved CaRamo entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaRamo entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaRamo entity);
    /**
	 Delete a persistent CaRamo entity.
	  @param entity CaRamo entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaRamo entity);
   /**
	 Persist a previously saved CaRamo entity and return it or a copy of it to the sender. 
	 A copy of the CaRamo entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaRamo entity to update
	 @return CaRamo the persisted CaRamo entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CaRamo update(CaRamo entity);
	public CaRamo findById( Long id);
	 /**
	 * Find all CaRamo entities with a specific property value.  
	 
	  @param propertyName the name of the CaRamo property to query
	  @param value the property value to match
	  	  @return List<CaRamo> found by query
	 */
	public List<CaRamo> findByProperty(String propertyName, Object value
		);
	public List<CaRamo> findByNombre(Object nombre
		);
	public List<CaRamo> findByIdentificador(Object identificador
		);
	public List<CaRamo> findByValor(Object valor
		);
	public List<CaRamo> findByUsuario(Object usuario
		);
	public List<CaRamo> findByBorradologico(Object borradologico
		);
	/**
	 * Find all CaRamo entities.
	  	  @return List<CaRamo> all CaRamo entities
	 */
	public List<CaRamo> findAll(
		);	
}