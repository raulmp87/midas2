/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Abril 2016
 * @author Mario Dominguez
 * 
 */ 
package mx.com.afirme.midas2.service.compensaciones;

import java.util.Date;
import java.util.List;
import javax.ejb.Local;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaBitacora;
import mx.com.afirme.midas2.domain.compensaciones.CaConfiguracionBanca;
import mx.com.afirme.midas2.domain.emision.ppct.GerenciaSeycos;
import mx.com.afirme.midas2.domain.emision.ppct.RamoSeycos;
import mx.com.afirme.midas2.dto.compensaciones.BancaEstadosView;
import mx.com.afirme.midas2.dto.compensaciones.CalculosBancaView;
import mx.com.afirme.midas2.dto.compensaciones.ConfiguracionContraprestacionBanca;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;


@Local

public interface CaConfiguracionBancaService {
    public void save(CaConfiguracionBanca entity);
    public void delete(CaConfiguracionBanca entity);
	public CaConfiguracionBanca update(CaConfiguracionBanca entity);
	public CaConfiguracionBanca findById( Long id);
	public List<CaConfiguracionBanca> findByProperty(String propertyName, Object value);
	public List<CaConfiguracionBanca> findByNombre(Object nombre);	
	public List<CaConfiguracionBanca> findByValor(Object valor);	
	public List<CaConfiguracionBanca> findByDivisor(Object valor);	
	public List<CaConfiguracionBanca> findByUsuario(Object usuario);	
	public List<CaConfiguracionBanca> findByBorradologico(Object borradologico);
	public List<CaConfiguracionBanca> findAll();
	public void cargarConfiguradorContraprestacion(ConfiguracionContraprestacionBanca configuracionContraprestacionBanca);
	public List<RamoSeycos> findRamosSeycos(); 
	public List<GerenciaSeycos> findGerenciasSeycos();
	public List<EstadoDTO> findEstadosDTO();
	public List<BancaEstadosView> cargarEstadosActivos(ConfiguracionContraprestacionBanca configuracionContraprestacionBanca);
	public void guardarConfiguracion(ConfiguracionContraprestacionBanca configuracionContraprestacionBanca);
	public List<CaConfiguracionBanca> filterCalculosCaConfiguracionesBanca(ConfiguracionContraprestacionBanca configuracionContraprestacionBanca);
	public List<CalculosBancaView> ejecutarCalculosBanca(ConfiguracionContraprestacionBanca configuracionContraprestacionBanca, List<CaConfiguracionBanca> listCaConfiguracionBanca, Date fecha);
	public TransporteImpresionDTO exportarExcelCalculos(List<CalculosBancaView> listResultCalculosBanca);
	public List<CaBitacora> getListHistoricoConfiguracion(ConfiguracionContraprestacionBanca configuracionContraprestacionBanca);
	public void procesarListaConfiguracionBanca(List<CalculosBancaView> listCalculosBancaview, Date fecha);
	public Object[] procesarListaCalculosBanca(List<CalculosBancaView> listCalculosBancaview);
	public void guardarCalculosBanca(Object[] listCalculosBancaview,Date fecha);
}