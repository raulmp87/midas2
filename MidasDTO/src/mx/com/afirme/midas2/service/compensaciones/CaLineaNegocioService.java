/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.service.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaLineaNegocio;
import mx.com.afirme.midas2.domain.compensaciones.CaParametros;

@Local

public interface CaLineaNegocioService {
		/**
	 Perform an initial save of a previously unsaved CaLineaNegocio entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaLineaNegocio entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaLineaNegocio entity);
    /**
	 Delete a persistent CaLineaNegocio entity.
	  @param entity CaLineaNegocio entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaLineaNegocio entity);
   /**
	 Persist a previously saved CaLineaNegocio entity and return it or a copy of it to the sender. 
	 A copy of the CaLineaNegocio entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaLineaNegocio entity to update
	 @return CaLineaNegocio the persisted CaLineaNegocio entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CaLineaNegocio update(CaLineaNegocio entity);
	public CaLineaNegocio findById( Long id);
	 /**
	 * Find all CaLineaNegocio entities with a specific property value.  
	 
	  @param propertyName the name of the CaLineaNegocio property to query
	  @param value the property value to match
	  	  @return List<CaLineaNegocio> found by query
	 */
	public List<CaLineaNegocio> findByProperty(String propertyName, Object value
		);
	public List<CaLineaNegocio> findByContraprestacion(Object contraprestacion
		);
	public List<CaLineaNegocio> findByNegocioId(Object negocioId
		);
	public List<CaLineaNegocio> findByValorporcentaje(Object valorporcentaje
		);
	public List<CaLineaNegocio> findByUsuario(Object usuario
		);
	public List<CaLineaNegocio> findByBorradologico(Object borradologico
		);
	public List<CaLineaNegocio> findByValorid(Object valorid
		);
	public List<CaLineaNegocio> findByValordescripcion(Object valordescripcion
		);
	public CaLineaNegocio findByPropertyUniqueResult(String propertyName, Object value
		);
	public CaLineaNegocio findByNegocioIdValorId(Long idNegocio, String idValor
		);
	/**
	 * Find all CaLineaNegocio entities.
	  	  @return List<CaLineaNegocio> all CaLineaNegocio entities
	 */
	public List<CaLineaNegocio> findAll(
		);
	
	public List<CaLineaNegocio> findByNegocioIdandContraprestacion(Long negocioId, boolean contraprestacion);
	
	public void guardarLineasNegocio(CaParametros caParametros, String lineasNegocio);
}