/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */
package mx.com.afirme.midas2.service.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaCalculoBanca;

@Local

public interface CaCalculoBancaService {
		/**
	 Perform an initial save of a previously unsaved CaCalculoBanca entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaCalculoBanca entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaCalculoBanca entity);
    /**
	 Delete a persistent CaCalculoBanca entity.
	  @param entity CaCalculoBanca entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaCalculoBanca entity);
   /**
	 Persist a previously saved CaCalculoBanca entity and return it or a copy of it to the sender. 
	 A copy of the CaCalculoBanca entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaCalculoBanca entity to update
	 @return CaCalculoBanca the persisted CaCalculoBanca entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CaCalculoBanca update(CaCalculoBanca entity);
	public CaCalculoBanca findById( Long id);
	 /**
	 * Find all CaCalculoBanca entities with a specific property value.  
	 
	  @param propertyName the name of the CaCalculoBanca property to query
	  @param value the property value to match
	  	  @return List<CaCalculoBanca> found by query
	 */
	public List<CaCalculoBanca> findByProperty(String propertyName, Object value
		);
	public List<CaCalculoBanca> findByLineanegocioId(Object lineanegocioId
		);
	public List<CaCalculoBanca> findByMonto(Object monto
		);
	public List<CaCalculoBanca> findByPorcentaje(Object porcentaje
		);
	public List<CaCalculoBanca> findByUsuario(Object usuario
		);
	public List<CaCalculoBanca> findByBorradologico(Object borradologico
		);
	/**
	 * Find all CaCalculoBanca entities.
	  	  @return List<CaCalculoBanca> all CaCalculoBanca entities
	 */
	public List<CaCalculoBanca> findAll(
		);	
}