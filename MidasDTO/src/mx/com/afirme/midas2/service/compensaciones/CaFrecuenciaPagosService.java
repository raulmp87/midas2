/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Marzo 2016
 * @author Mario Dominguez
 * 
 */ 
package mx.com.afirme.midas2.service.compensaciones;

import java.util.List;
import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaFrecuenciaPagos;


@Local

public interface CaFrecuenciaPagosService {
    public void save(CaFrecuenciaPagos entity);
    public void delete(CaFrecuenciaPagos entity);
	public CaFrecuenciaPagos update(CaFrecuenciaPagos entity);
	public CaFrecuenciaPagos findById( Long id);
	public List<CaFrecuenciaPagos> findByProperty(String propertyName, Object value);
	public List<CaFrecuenciaPagos> findByNombre(Object nombre);	
	public List<CaFrecuenciaPagos> findByValor(Object valor);	
	public List<CaFrecuenciaPagos> findByDivisor(Object valor);	
	public List<CaFrecuenciaPagos> findByUsuario(Object usuario);	
	public List<CaFrecuenciaPagos> findByBorradologico(Object borradologico);
	public List<CaFrecuenciaPagos> findAll();
}