package mx.com.afirme.midas2.service.compensaciones.negociovida;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaCompensacion;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.DatosSeycos;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.NegocioVida;
import mx.com.afirme.midas2.domain.emision.ppct.AgenteSeycos;
import mx.com.afirme.midas2.domain.emision.ppct.GerenciaSeycos;
import mx.com.afirme.midas2.domain.emision.ppct.LineaNegocioSeycos;
import mx.com.afirme.midas2.domain.emision.ppct.ProductoSeycos;
import mx.com.afirme.midas2.domain.emision.ppct.RamoSeycos;
import mx.com.afirme.midas2.domain.emision.ppct.SubRamoSeycos;
import mx.com.afirme.midas2.dto.compensaciones.CompensacionesDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.GenericaAgentesView;

@Local
public interface NegocioVidaService {
	
	public List<GenericaAgentesView> getProductosVidaView(Long idParam) throws Exception;

	public List<GenericaAgentesView> getRamosPorProductosView(String idProducto)
			throws Exception;

	public List<GenericaAgentesView> getSubramosPorRamoView(String ramos)
			throws Exception;

	public List<GenericaAgentesView> getLineasNegocioPorRamoProductoView(
			String productos, String ramos) throws Exception;

	public void deleteConfiguration(NegocioVida config) throws Exception;

	public List<NegocioVida> findByFilters(NegocioVida config) throws Exception;

	public NegocioVida saveConfiguration(NegocioVida param);
	
	public List<GenericaAgentesView> getGerenciaVidaView() throws Exception;
	
	public List<DatosSeycos> getAgenteVidaView(String agente, String idGerencias)throws Exception;
	
	public List<GerenciaSeycos> getGerenciaVida(Long gerencia);
	
	public List<AgenteSeycos> getAgenteVida(Long agente);
	
	public List<RamoSeycos> getRamoVida(Long ramo);
	
	public List<SubRamoSeycos> getSubRamoVida(Long SubRamo);
	
	public List<ProductoSeycos> getProductoVida(Long producto);
	
	public List<LineaNegocioSeycos> getLineaNegocioVida(Long lineaNegocio);	
	
	public List<DatosSeycos> findAgenteSeycosConPromotor(DatosSeycos datosSeycos);
	
	public List<DatosSeycos> findAgenteSeycos(DatosSeycos datosSeycos);
	
	public List<GenericaAgentesView> getProductoXConfView(Long idconfiguracionVida) throws Exception;
	
	public List<GenericaAgentesView> getRamosXConfView(Long idconfiguracionVida) throws Exception;
	
	public List<GenericaAgentesView> getSubRamosXConfView(Long idconfiguracionVida) throws Exception ;
	
	public List<GenericaAgentesView> getLineasNegXConfView(Long idconfiguracionVida) throws Exception;
	
	public List<GenericaAgentesView> getGerenciaXConfView(Long idconfiguracionVida) throws Exception ;
	
	public List<GenericaAgentesView> getAgenteXConfView(Long idconfiguracionVida) throws Exception;

	public void fechaModificacionVida(List<CaCompensacion> listCompensaciones, CompensacionesDTO compensacionesDTO);
}