/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */
package mx.com.afirme.midas2.service.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaTipoEmisor;

/**
 * Local interface for TipoEmisorcaFacade.
 * @author MyEclipse Persistence Tools
 */
@Local

public interface CaTipoEmisorService {
		/**
	 Perform an initial save of a previously unsaved CaTipoEmisor entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaTipoEmisor entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaTipoEmisor entity);
    /**
	 Delete a persistent CaTipoEmisor entity.
	  @param entity CaTipoEmisor entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaTipoEmisor entity);
   /**
	 Persist a previously saved CaTipoEmisor entity and return it or a copy of it to the sender. 
	 A copy of the CaTipoEmisor entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaTipoEmisor entity to update
	 @return CaTipoEmisor the persisted CaTipoEmisor entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CaTipoEmisor update(CaTipoEmisor entity);
	public CaTipoEmisor findById( Long id);
	 /**
	 * Find all CaTipoEmisor entities with a specific property value.  
	 
	  @param propertyName the name of the CaTipoEmisor property to query
	  @param value the property value to match
	  	  @return List<CaTipoEmisor> found by query
	 */
	public List<CaTipoEmisor> findByProperty(String propertyName, Object value
		);
	public List<CaTipoEmisor> findByNombre(Object nombre
		);
	public List<CaTipoEmisor> findByValor(Object valor
		);
	public List<CaTipoEmisor> findByUsuario(Object usuario
		);
	public List<CaTipoEmisor> findByBorradologico(Object borradologico
		);
	/**
	 * Find all CaTipoEmisor entities.
	  	  @return List<CaTipoEmisor> all CaTipoEmisor entities
	 */
	public List<CaTipoEmisor> findAll(
		);	
}