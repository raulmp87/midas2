/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.service.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaTipoMoneda;

/**
 * Local interface for TipoMonedacaFacade.
 * @author MyEclipse Persistence Tools
 */
@Local

public interface CaTipoMonedaService {
		/**
	 Perform an initial save of a previously unsaved CaTipoMoneda entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaTipoMoneda entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaTipoMoneda entity);
    /**
	 Delete a persistent CaTipoMoneda entity.
	  @param entity CaTipoMoneda entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaTipoMoneda entity);
   /**
	 Persist a previously saved CaTipoMoneda entity and return it or a copy of it to the sender. 
	 A copy of the CaTipoMoneda entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaTipoMoneda entity to update
	 @return CaTipoMoneda the persisted CaTipoMoneda entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CaTipoMoneda update(CaTipoMoneda entity);
	public CaTipoMoneda findById( Long id);
	 /**
	 * Find all CaTipoMoneda entities with a specific property value.  
	 
	  @param propertyName the name of the CaTipoMoneda property to query
	  @param value the property value to match
	  	  @return List<CaTipoMoneda> found by query
	 */
	public List<CaTipoMoneda> findByProperty(String propertyName, Object value
		);
	public List<CaTipoMoneda> findByNombre(Object nombre
		);
	public List<CaTipoMoneda> findByValor(Object valor
		);
	public List<CaTipoMoneda> findByUsuario(Object usuario
		);
	public List<CaTipoMoneda> findByBorradologico(Object borradologico
		);
	/**
	 * Find all CaTipoMoneda entities.
	  	  @return List<CaTipoMoneda> all CaTipoMoneda entities
	 */
	public List<CaTipoMoneda> findAll(
		);	
}