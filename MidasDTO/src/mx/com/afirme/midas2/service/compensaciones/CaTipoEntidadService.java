/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */
package mx.com.afirme.midas2.service.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaTipoEntidad;

@Local

public interface CaTipoEntidadService {
		/**
	 Perform an initial save of a previously unsaved CaTipoEntidad entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaTipoEntidad entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaTipoEntidad entity);
    /**
	 Delete a persistent CaTipoEntidad entity.
	  @param entity CaTipoEntidad entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaTipoEntidad entity);
   /**
	 Persist a previously saved CaTipoEntidad entity and return it or a copy of it to the sender. 
	 A copy of the CaTipoEntidad entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaTipoEntidad entity to update
	 @return CaTipoEntidad the persisted CaTipoEntidad entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CaTipoEntidad update(CaTipoEntidad entity);
	public CaTipoEntidad findById( Long id);
	 /**
	 * Find all CaTipoEntidad entities with a specific property value.  
	 
	  @param propertyName the name of the CaTipoEntidad property to query
	  @param value the property value to match
	  	  @return List<CaTipoEntidad> found by query
	 */
	public List<CaTipoEntidad> findByProperty(String propertyName, Object value
		);
	public List<CaTipoEntidad> findByNombre(Object nombre
		);
	public List<CaTipoEntidad> findByValor(Object valor
		);
	public List<CaTipoEntidad> findByUsuario(Object usuario
		);
	public List<CaTipoEntidad> findByBorradologico(Object borradologico
		);
	/**
	 * Find all CaTipoEntidad entities.
	  	  @return List<CaTipoEntidad> all CaTipoEntidad entities
	 */
	public List<CaTipoEntidad> findAll(
		);	
}