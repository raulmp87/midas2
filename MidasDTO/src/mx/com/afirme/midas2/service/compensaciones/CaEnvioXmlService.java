package mx.com.afirme.midas2.service.compensaciones;

import java.util.List;
import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaEnvioXml;

/**
 * Local interface for CaEnvioXmlFacade.
 * @author MyEclipse Persistence Tools
 */
@Local

public interface CaEnvioXmlService {
		/**
	 Perform an initial save of a previously unsaved CaEnvioXml entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaEnvioXml entity to persist
	  @throws RuntimeException when the operation fails
	 */
	public void save(CaEnvioXml entity);
    /**
	 Delete a persistent CaEnvioXml entity.
	  @param entity CaEnvioXml entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaEnvioXml entity);
   /**
	 Persist a previously saved CaEnvioXml entity and return it or a copy of it to the sender. 
	 A copy of the CaEnvioXml entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaEnvioXml entity to update
	 @return CaEnvioXml the persisted CaEnvioXml entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CaEnvioXml update(CaEnvioXml entity);
	public CaEnvioXml findById(CaEnvioXml id);
	 /**
	 * Find all CaEnvioXml entities with a specific property value.  
	 
	  @param propertyName the name of the CaEnvioXml property to query
	  @param value the property value to match
	  	  @return List<CaEnvioXml> found by query
	 */
	public List<CaEnvioXml> findByProperty(String propertyName, Object value);
	/**
	 * Find all CaEnvioXml entities.
	  	  @return List<CaEnvioXml> all CaEnvioXml entities
	 */
	public List<CaEnvioXml> findAll();	
}