/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.service.compensaciones;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.compensaciones.CaBitacora;

@Local

public interface CaBitacoraService {
		/**
	 Perform an initial save of a previously unsaved CaBitacora entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaBitacora entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaBitacora entity);
    /**
	 Delete a persistent CaBitacora entity.
	  @param entity CaBitacora entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaBitacora entity);
   /**
	 Persist a previously saved CaBitacora entity and return it or a copy of it to the sender. 
	 A copy of the CaBitacora entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaBitacora entity to update
	 @return CaBitacora the persisted CaBitacora entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
	public CaBitacora update(CaBitacora entity);
	public CaBitacora findById( Long id);
	
	 /**
	 * Find all CaBitacora entities with a specific property value.  
	 
	  @param propertyName the name of the CaBitacora property to query
	  @param value the property value to match
	  	  @return List<CaBitacora> found by query
	 */
	public List<CaBitacora> findByProperty(String propertyName, Object value
		);
	public List<CaBitacora> findByUsuario(Object usuario
		);
	public List<CaBitacora> findByHora(Object hora
		);
	public List<CaBitacora> findByMovimiento(Object movimiento
		);
	public List<CaBitacora> findByValorAnterior(Object valorAnterior
		);
	public List<CaBitacora> findByValorPosterior(Object valorPosterior
		);
	public List<CaBitacora> obtenerBitacoraPorCompensacionId( Long compensacionId 
		);
	public List<CaBitacora> obtenerBitacoraPorConfiguracionCalc(Long caBancaConfigCalId
		);
	/**
	 * Find all CaBitacora entities.
	  	  @return List<CaBitacora> all CaBitacora entities
	 */
	public List<CaBitacora> findAll(
		);	
}