package mx.com.afirme.midas2.service.compensaciones;

// default package

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;


import mx.com.afirme.midas2.domain.compensaciones.OrdenPagos;
import mx.com.afirme.midas2.domain.compensaciones.OrdenesPagoDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;

/**
 * Local interface for CaOrdenPagoFacade.
 * 
 * @author MyEclipse Persistence Tools
 */
@Local
public interface OrdenPagosService {
	/**
	 * Perform an initial save of a previously unsaved CaOrdenPago entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            CaOrdenPago entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(OrdenPagos entity);

	/**
	 * Delete a persistent CaOrdenPago entity.
	 * 
	 * @param entity
	 *            CaOrdenPago entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(OrdenPagos entity);

	/**
	 * Persist a previously saved CaOrdenPago entity and return it or a copy of
	 * it to the sender. A copy of the CaOrdenPago entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            CaOrdenPago entity to update
	 * @return CaOrdenPago the persisted CaOrdenPago entity instance, may not be
	 *         the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public OrdenPagos update(OrdenPagos entity);

	public OrdenPagos findById(Long id);

	/**
	 * Find all CaOrdenPago entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the CaOrdenPago property to query
	 * @param value
	 *            the property value to match
	 * @return List<CaOrdenPago> found by query
	 */
	public List<OrdenPagos> findByProperty(String propertyName, Object value);

	public List<OrdenPagos> findByClaveNombre(Object claveNombre);

	public List<OrdenPagos> findByNombreContratante(Object nombreContratante);

	public List<OrdenPagos> findByImportePrima(Object importePrima);

	public List<OrdenPagos> findByImporteBs(Object importeBs);

	public List<OrdenPagos> findByImporteDerpol(Object importeDerpol);

	public List<OrdenPagos> findByImporteCumMeta(Object importeCumMeta);

	public List<OrdenPagos> findByImporteUtilidad(Object importeUtilidad);

	public List<OrdenPagos> findBySubtotal(Object subtotal);

	public List<OrdenPagos> findByIva(Object iva);

	public List<OrdenPagos> findByIvaRetenido(Object ivaRetenido);

	public List<OrdenPagos> findByIsr(Object isr);

	public List<OrdenPagos> findByEstatusFactura(Object estatusFactura);

	public List<OrdenPagos> findByEstatusOrdenpago(Object estatusOrdenpago);

	public List<OrdenPagos> findByImporteTotal(Object importeTotal);

	/**
	 * Find all CaOrdenPago entities.
	 * 
	 * @return List<CaOrdenPago> all CaOrdenPago entities
	 */
	public List<OrdenPagos> findAll();
	
	public List<OrdenPagos> findOrdenPagosByFilters(OrdenesPagoDTO filter);
	
	public  void guardarOrdenesPago(List<OrdenPagos> ordenPagosList);
	
	public  List<OrdenPagos> obtenerOrdenPagosProcesoPorId(Long ordenPagoId);
	
	public  List<OrdenPagos> obtenerOrdenPagosGeneradoPorId(Long ordenPagoId);
	
	public String enviarReportePorCorreo(Long idLiquidacion);	

}

