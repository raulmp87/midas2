package mx.com.afirme.midas2.service.complementopago;

import java.math.BigDecimal;
import java.util.List;
import mx.com.afirme.midas2.domain.cobranza.pagos.complementopago.ComplementoPago;

public interface ComplementoPagoService {

	public static final BigDecimal GRUPO_PARAMETRO_COMPLEMENTO_PAGO= new BigDecimal("21");
	public static final BigDecimal PARAMETRO_SWITCH_COMPLEMENTO_PAGO = new BigDecimal("210060");
	public static final BigDecimal PARAMETRO_SWITCH_COMPLEMENTO_PAGO_DESPAGOS = new BigDecimal("210070");
	public static final BigDecimal PARAMETRO_CORREOS_NOTIFICACION_APLICAR_INGRESO = new BigDecimal("210090");
	public static final BigDecimal PARAMETRO_SWITCH_NOTIFICACION_APLICAR_INGRESO = new BigDecimal("210100");

	/**
	 * Timbra los recibos del tipo complemento Pago.
	 */
	public void timbraComplementoPago();
	public List<ComplementoPago> listarErrores(ComplementoPago complementoPago) throws Exception;
	public List<ComplementoPago> findByFilters(ComplementoPago complementoPago) throws Exception;
	
	public void timbraDespagosComplementoPago();
	
	public void notificaComplementoPago();
	
}