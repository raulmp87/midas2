package mx.com.afirme.midas2.service.movimientosmanuales;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.movimientosmanuales.MovimientosManuales;

@Local
public interface MovimientosManualesService {
	/**
	 * Obtiene el valor de los elementos de un catalogo de acuerdo a los filtros.	
	 * @param filtro
	 * @return
	 * @throws Exception
	 */
	public List<MovimientosManuales> findByFilters(MovimientosManuales filtro) throws Exception;
	
	public int aplicarMovimientosManuales(String[] idsMovimientosManuales, String idUsuarioAplica);

	public int eliminarMovimientosManuales(String[] idsMovimientosManuales);
}
