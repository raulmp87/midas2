package mx.com.afirme.midas2.service.seguridad;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.usuario.UsuarioDTO;

import com.asm.dto.UserDTO;
import com.asm.dto.UserView;


public interface MidasASMTranslator {


	public List<Usuario> obtieneListaUsuariosMidasSinRoles(
			List<UserDTO> listaUsuariosASM) throws ClassCastException;

	public Usuario obtieneUsuarioMidasSinRoles(UserDTO usuarioASM)
			throws ClassCastException;

	public Usuario obtieneUsuarioMidas(UserView usuarioASM, UsuarioDTO usuarioDTO)
			throws ClassCastException;

			
	public Usuario obtieneUsuarioMidas(UserDTO usuarioASM, UsuarioDTO usuarioDTO)
			throws ClassCastException;

}