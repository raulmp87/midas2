package mx.com.afirme.midas2.service.seguridad;

import mx.com.afirme.midas2.exeption.ApplicationException;


public interface EncryptorService {
	
	public String encrypt( String serializedParams ) throws ApplicationException;
	
	public String decrypt( String encryptedParams ) throws ApplicationException;
	
	public void setValuesByParams( Object obj , String params ) throws ApplicationException;
	
	public void setValuesByEncriptedParams( Object obj , String params ) throws ApplicationException;
	
	public String getUrlEncryptedParams( String url, String params ) throws ApplicationException;	
	
}
