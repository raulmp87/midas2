package mx.com.afirme.midas2.service.seguridad;

import java.util.List;

import org.apache.bval.constraints.NotEmpty;

import com.asm.dto.ApplicationDTO;
import com.asm.dto.CreatePasswordUserParameterDTO;
import com.asm.dto.CreatePortalUserParameterDTO;
import com.asm.dto.PageConsentDTO;
import com.asm.dto.ResendConfirmationEmailDTO;
import com.asm.dto.RoleDTO;
import com.asm.dto.SearchUserParametersDTO;
import com.asm.dto.SendResetPasswordTokenDTO;
import com.asm.dto.UserDTO;
import com.asm.dto.UserInboxEntryDTO;
import com.asm.dto.UserUpdateDTO;
import com.js.service.SystemException;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.movil.ajustador.AjustadorMovil;

/**
 * Utilizada para realizar operaciones del <code>Usuario</code>.
 * 
 * @author amosomar
 */

public interface UsuarioService {

	/**
	 * Obtiene el usuario actual.
	 * @return
	 */
	public Usuario getUsuarioActual();
	/**
	 * Anteriormente se llamaba <code>obtieneUsuariosSinRolesPorNombreRol</code>.
	 * @param nombreRol
	 * @param nombreUsuario
	 * @param idSesionUsuario
	 * @return
	 */
	public List<Usuario> buscarUsuariosSinRolesPorNombreRol(String nombreRol, String nombreUsuario, String idSesionUsuario);
	/**
	 * Anteriormente se llamaba <code>obtieneUsuariosSinRolesPorNombreRol</code>.
	 * @param nombreUsuario
	 * @param idSesionUsuario
	 * @param nombresRol
	 * @return
	 */
	public List<Usuario> buscarUsuariosSinRolesPorNombreRol(String nombreUsuario, String idSesionUsuario, String... nombresRol);
	/**
	 * @param nombreUsuario
	 * @param idSesionUsuario
	 * @return
	 */
	public Usuario buscarUsuarioRegistrado(String nombreUsuario, String idSesionUsuario);
	public Usuario buscarUsuarioRegistrado(String token);
	public boolean logOutUsuario(String nombreUsuario, String idSesionUsuario);
	/**
	 * Anteriormente se llamaba <code>obtieneListaConsentimientos</code>.
	 * @param idRole
	 * @return
	 */
	public List<PageConsentDTO> buscarConsentimientosPorRol(int idRole);
	/**
	 * Anteriormente se llamaba <code>obtieneUsuario</code>.
	 * @param idUsuario
	 * @return
	 */
	public Usuario buscarUsuarioPorId(int idUsuario);
	
	/**
	 * Anteriormente se llamaba <code>obtieneUsuario</code>.
	 * @param nombreUsuario
	 * @return
	 */
	public Usuario buscarUsuarioPorNombreUsuario(String nombreUsuario);
	
	public List<Usuario> buscarUsuariosPorNombreRol(String... nombresRol);
	
	public List<Usuario> buscarUsuariosPorNombreRolSimple(String... nombresRol);
	
	/**
	 * Indica si el usuario actual tiene el rol dado.
	 * @param nombreRol
	 * @return
	 */
	public boolean tieneRolUsuarioActual(String nombreRol);

	/**
	 * Indica si el usuario acutal tiene al menos uno de los roles dados.
	 * @param nombreRoles
	 * @return
	 */
	public boolean tieneRolUsuarioActual(String[] nombreRoles);

	
	/**
	 * Indica si el usuario tiene el rol dado.
	 * @param nombreRol
	 * @param usuario
	 * @return
	 */
	public boolean tieneRol(String nombreRol, Usuario usuario);
	
	/**
	 * Indica si el usuario tiene al menos uno de los roles dados.
	 * @param nombreRoles
	 * @param usuario
	 * @return
	 */
	public boolean tieneRol(String[] nombreRoles, Usuario usuario);
	
	/**
	 * Indica si el usuario actual tiene el permiso dado.
	 * @param nombrePermiso
	 * @return
	 */
	public boolean tienePermisoUsuarioActual(String nombrePermiso);
	
	/**
	 * Indica si el usuario actual tiene al menos uno de los permisos dados.
	 * @param nombrePermisos
	 * @return
	 */
	public boolean tienePermisoUsuarioActual(String[] nombrePermisos);
	
	/**
	 * Indica si el usuario tiene el permiso dado.
	 * @param nombrePermiso
	 * @param usuario
	 * @return
	 */
	public boolean tienePermisoUsuario(String nombrePermiso, Usuario usuario);
	
	/**
	 * Indica si el usuario tiene al menos uno de los permisos dados.
	 * @param nombrePermiso
	 * @param usuario
	 * @return
	 */
	public boolean tienePermisoUsuario(String[] nombrePermisos, Usuario usuario);
	
	public void setUsuarioActual(Usuario usuario);
	
	public Agente getAgenteUsuarioActual();
	
	public Agente getAgenteUsuarioActual(Usuario usuario);
	
	public AjustadorMovil getAjustadorMovilUsuarioActual();
	
	public AjustadorMovil getAjustadorMovilUsuario(Usuario usuario);
	
	public AjustadorMovil getAjustadorMovilMidasUsuarioActual();
	
	public AjustadorMovil getAjustadorMovilMidasUsuario(Usuario usuario);
	
	public AjustadorMovil getAjustadorMovilMidasUsuario(Long ajustadorId);
	
	public List<Usuario> buscarUsuarioPorNombreCompleto(String likeCompleteName);
	
	public void deshabilitarToken(String token);
	
	public class LoginParameter {
		private String usuario;
		private String password;
		private Integer applicationId;
		private String ipAddress;
		private String registrationId;
		private String deviceUuid;
		private String deviceApplicationId;
		private boolean portalUser = false;

		
		@NotEmpty
		public String getUsuario() {
			return usuario;
		}
		
		public void setUsuario(String usuario) {
			this.usuario = usuario;
		}
		
		@NotEmpty
		public String getPassword() {
			return password;
		}
		
		public void setPassword(String password) {
			this.password = password;
		}
		
		public Integer getApplicationId() {
			return applicationId;
		}
		
		public void setApplicationId(Integer applicationId) {
			this.applicationId = applicationId;
		}
		
		@NotEmpty
		public String getIpAddress() {
			return ipAddress;
		}
		
		public void setIpAddress(String ipAddress) {
			this.ipAddress = ipAddress;
		}
		
		/**
		 * Google Cloud Messaging Registration Id
		 * @return
		 */
		public String getRegistrationId() {
			return registrationId;
		}
		
		/**
		 * Google Cloud Messaging Registration Id
		 * @param registrationId
		 */
		public void setRegistrationId(String registrationId) {
			this.registrationId = registrationId;
		}
		
		public String getDeviceUuid() {
			return deviceUuid;
		}
		
		public void setDeviceUuid(String deviceUuid) {
			this.deviceUuid = deviceUuid;
		}
		
		public String getDeviceApplicationId() {
			return deviceApplicationId;
		}
		
		public void setDeviceApplicationId(String deviceApplicationId) {
			this.deviceApplicationId = deviceApplicationId;
		}
		
		public boolean isPortalUser() {
			return portalUser;
		}
		
		public void setPortalUser(boolean portalUser) {
			this.portalUser = portalUser;
		}
		
	}
	
	public Usuario login(LoginParameter parameter);
	
	/**
	 * Obtiene los registrationIds para un <code>nombreUsuario</code>
	 * 
	 * @deprecated reemplazado por {@link #getRegistrationIds(String, String)} o
	 *             {@link #getRegistrationId(String, String, String)}.
	 * @param username
	 * @return
	 */
	@Deprecated
	public List<String> getRegistrationIds(String nombreUsuario);

	public List<String> getRegistrationIds(String nombreUsuario, String deviceApplicationId);

	/**
	 * Obtiene el actual registrationId para un <code>nombreUsuario</code>, <code>deviceApplicationId</code> y <code>deviceUuid</code>.
	 * 
	 * @param nombreUsuario
	 * @param deviceApplicationId
	 * @param deviceUuid
	 * @return
	 */
	public String getRegistrationId(String nombreUsuario, String deviceApplicationId, String deviceUuid);
	
	public Usuario createPortalUser(CreatePortalUserParameterDTO parameter);

	public void resendConfirmationEmail(Integer userId);

	public void resendConfirmationEmail(ResendConfirmationEmailDTO resendConfirmationEmail);
	
	public int confirmEmail(String confirmationCode);
	
	public void changePassword(String oldPassword, String newPassword);
	
	public void changePassword2(String newPassword);
	
	public void resetPassword(String token, String newPassword);
	
	public void sendResetPasswordToken(Integer userId);

	public void sendResetPasswordToken(SendResetPasswordTokenDTO sendResetPasswordToken);	
	
	public void updateUser(UserUpdateDTO userUpdate);
	
	public void activateDeactivateUser(Integer userId, Boolean active, Integer administratorUserId);
	
	public List<UserInboxEntryDTO> getUserList(SearchUserParametersDTO searchParameters);
	
	public Usuario createPasswordUser(CreatePasswordUserParameterDTO parameter);
	  
	public Integer findUserIdByUsername(String username);
	
	public Integer findRoleId(Integer applicationId, String roleName);
	
	public boolean isUserNeedsToSetPassword(Integer userId);

	public String getResetPasswordUrl(Integer userId);
	
	public boolean isUserHasAccessToApplication(Integer userId,
			Integer applicationId);

	public boolean isUserPortalUser(Integer userId);

	public List<ApplicationDTO> getUserApplications(Integer userId);
	
	public boolean isTokenActive(String token);
	
	public UserDTO getUserDTOById(Integer userId);
		
	public void validateToken(String token) throws SystemException;
	
	public void updateMidasUser(UserDTO updateUser, UserDTO administrator,
			List<RoleDTO> roleList);
	
	public RoleDTO getRoleDTOById(int id);
	
}
