package mx.com.afirme.midas2.service.custShipmentTracking;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.OrdenRenovacionMasiva;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia.BitacoraGuia;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia.RegistroGuiasProveedorId;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;

@Local
public interface TrackingService {
	
	/**
	 * Consulta el estado actual de una lista de guias
	 * @param guias
	 * @return
	 */
	public void getGuias(List<String>guias);
	
	public void saveCargaMasiva(List<RegistroGuiasProveedorId> id);
	
	public void procesarEstatusPendientes();
	
	public TransporteImpresionDTO getReporteEfectividad(OrdenRenovacionMasiva ordenRenovacion)throws Exception;
	
	public void procesarTareaEstatusPendientes();
	
	public List<BitacoraGuia> getBitacoraByIdToPoliza(BigDecimal idToPoliza);
	
	public String getEstatusBitacora(Integer estatus);
	
	public void initialize();
	
}
