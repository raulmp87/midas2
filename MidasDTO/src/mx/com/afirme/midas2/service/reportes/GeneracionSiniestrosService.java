package mx.com.afirme.midas2.service.reportes;

import java.io.InputStream;
import java.util.Date;

import mx.com.afirme.midas2.util.MidasException;

public interface GeneracionSiniestrosService {
	public InputStream exportarReporte( Date fInicial, Date fFinal  ) throws MidasException;
}
