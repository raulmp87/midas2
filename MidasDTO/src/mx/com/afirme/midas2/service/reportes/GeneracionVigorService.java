package mx.com.afirme.midas2.service.reportes;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.domain.reportes.reporteReservas.GeneracionVigorDTO;
import mx.com.afirme.midas2.util.MidasException;

public interface GeneracionVigorService {

	public int programarTarea( Date fInicial, Date fFinal, String usuario ) throws MidasException;
	
	public int generarReporte( Date fInicial, Date fFinal, String usuario ) throws MidasException;

	public InputStream exportarReporte( Date fInicial, Date fFinal  ) throws MidasException;
	
	public void initTareaProgramada();
	
	public String getFechasReporte(int tipoReporte);
	
	public List<GeneracionVigorDTO> getTareasVigor(BigDecimal tipoTarea) throws MidasException;
	
	public void initialize();
}
