package mx.com.afirme.midas2.service.reportes;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.compensaciones.OrdenPagosDetalle;
import mx.com.afirme.midas2.domain.compensaciones.ReporteFiltrosDTO;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia.ReporteEfectividadEntrega;
import mx.com.afirme.midas2.domain.provisiones.ProvisionImportesRamo;
import mx.com.afirme.midas2.domain.reportes.ReporteEmisionView;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.impresiones.ResumenCotMovilVidaDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgenteEstatusEntregaFactura;
import mx.com.afirme.midas2.dto.reportesAgente.DatosHonorariosAgentes;
import mx.com.afirme.midas2.dto.reportesAgente.RetencionImpuestosReporteView;
import mx.com.afirme.midas2.dto.reportesAgente.SaldosVsContabilidadView;
import mx.com.afirme.midas2.util.MidasException;
import net.sf.jasperreports.engine.JRException;


public interface GenerarPlantillaReporte {

	public static final long TIPO_BENEFICIARIO_PROMOTOR = 838L;
	public static final long TIPO_BENEFICIARIO_AGENTE = 839L;
	
	public static final String TIPO_USUARIO_ADMIN = "ADMIN";
	public static final String TIPO_USUARIO_AGENTE = "AGENTE";
	public static final String TIPO_USUARIO_PROMOTOR = "PROMOTOR";
	/**
	 * Imprimir listado de excepciones.
	 * 
	 * @param cveNegocio
	 *            A - Autos
	 * @param locale
	 *            - opcional
	 * @return
	 */
	TransporteImpresionDTO imprimirListadoExcepciones(String cveNegocio,
			Locale locale);

	/**
	 * Imprimir la cotizacion completa
	 * 
	 * @param cveNegocio
	 *            A - Autos
	 * @param locale
	 *            - opcional
	 * @return
	 */
	TransporteImpresionDTO imprimirCotizacion(BigDecimal idToCotizacion,
			Locale locale, boolean isOutsider);

	/**
	 * Imprimir la poliza completa
	 * 
	 * @param idToPoliza
	 * @param locale
	 * @return
	 */
	TransporteImpresionDTO imprimirPoliza(BigDecimal idToCotizacion,
			BigDecimal idToPoliza, Locale locale);

	/**
	 * Imprimir cotizacion por partes.
	 * <code>todoslosincisos</code>  define si se imprimen todos o ningun inciso
	 * 
	 * @param idToCotizacion
	 * @param locale
	 * @param caratula
	 * @param todosLosIncisos
	 * @param incisoIncial
	 * @param incisoFinal
	 * @return
	 */
	TransporteImpresionDTO imprimirCotizacion(BigDecimal idToCotizacion,
			Locale locale, boolean caratula, boolean todosLosIncisos, boolean isOutsider);
	
	/**
	 * Imprimir cotizacion por partes.
	 * <code>incisoInicial</code> e <code>incisoFinal</code> definen el rango de incisos
	 * 
	 * @param idToCotizacion
	 * @param locale
	 * @param caratula
	 * @param todosLosIncisos
	 * @param incisoIncial
	 * @param incisoFinal
	 * @return
	 */
	TransporteImpresionDTO imprimirCotizacion(BigDecimal idToCotizacion,
			Locale locale, boolean caratula, int incisoIncial, int incisoFinal, boolean isOutsider);

	/**
	 * Imprimir poliza por partes
	 * <code>incisoInicial</code> e <code>incisoFinal</code> definen el rango de 
	 * incisos a imprmir
	 * @param idToCotizacion
	 * @param idToPoliza
	 * @param locale
	 * @param caratula
	 * @param recibo
	 * @param certificadoRC
	 * @param anexosInciso
	 * @param incisoInicial
	 * @param incisoFinal
	 * @return
	 */
	TransporteImpresionDTO imprimirPoliza(BigDecimal idToCotizacion,
			BigDecimal idToPoliza, Locale locale, boolean caratula,
			boolean recibo, boolean certificadoRC,
			boolean anexosInciso, int incisoInicial, int incisoFinal);
	
	/**
	 * Imprmir poliza por partes. <code>incisos</code> determina si se imprimen
	 * todos los incisos o ninguno.
	 * @param idToCotizacion
	 * @param idToPoliza
	 * @param locale
	 * @param caratula
	 * @param recibo
	 * @param todosIncisos
	 * @param certificadoRC
	 * @param anexosInciso
	 * @return
	 */
	TransporteImpresionDTO imprimirPoliza(BigDecimal idToCotizacion,
			BigDecimal idToPoliza, Locale locale, boolean caratula,
			boolean recibo, boolean incisos, boolean certificadoRC,
			boolean anexosInciso);
	/**
	 * Imprime la configuracion de un negocio
	 * @param idToNegocio
	 * @return
	 * @autor martin
	 */
	TransporteImpresionDTO imprimirConfiguracionNegocio(Long idToNegocio, Locale locale);


	
		
	TransporteImpresionDTO imprimirReporteDetPrimasExcel(CentroOperacion centroOperacion, Gerencia gerencia, Ejecutivo ejecutivo,
			Promotoria promotoria, ValorCatalogoAgentes tipoAgentes, String fecha1, String fecha2, Agente agente, String formatoArchivo);

	TransporteImpresionDTO imprimirReportePreviewBonosExcel(Long idCentroOperacion, Long idGerencia, Long idEjecutivo,
			Long idPromotoria, ValorCatalogoAgentes tipoAgentes, String mes, String anio, Agente agente, ConfigBonos bono, Long idCalculo, String formatoArchivo);

	
	TransporteImpresionDTO imprimirReporteAgenteIngresos(String anio, String mes, Long rangoInicio, Long rangoFin,Integer tipoReporte, Locale locale) throws MidasException;
	
	TransporteImpresionDTO imprimirReporteAgenteIngresosToExcel(
			String anio, String mes, Long rangoInicio, Long rangoFin,
			Integer tipoReporte, Locale locale, String formatoArchivo) throws MidasException;
	
	TransporteImpresionDTO imprimirReporteAgentePrimaPagadaPorRamo(
			List<AgenteView> listaAgentes, Date fechaCorteInicio,
			Date fechaCorteFin, Integer tipoReporte, Locale locale);
	
	TransporteImpresionDTO imprimirReporteAgentePrimaPagadaPorRamoToExcel (
			List<Agente> agenteList, Date fechaInicial, Date fechaCorteFin,
		    List<CentroOperacion> centroOperacionesSeleccionados, 
			List<Ejecutivo> ejecutivosSeleccionados,
			List<Gerencia> gerenciasSeleccionadas,
			List<Promotoria> promotoriasSeleccionadas,
			Integer tipoReporte, Locale locale, String formatoArchivo) throws MidasException;
	
	TransporteImpresionDTO imprimirReporteAgenteTopAgente(
			List<AgenteView> listaAgentes,String anio,String mes,Integer tipoReporte,Integer topAgente, Locale locale);
	
	TransporteImpresionDTO imprimirReporteAgenteTopAgenteToExcel(
			List<Agente> listaAgentes,
			List<CentroOperacion> centroOperacionesSeleccionados, List<Ejecutivo> ejecutivosSeleccionados,
			List<Gerencia> gerenciasSeleccionadas,List<Promotoria> promotoriasSeleccionadas,
			String anio,String mes,Integer tipoReporte,Integer topAgente, Locale locale, String formatoArchivo,Date fechaInicio, Date fechaFin);
	
	TransporteImpresionDTO imprimirReporteAgenteDatosAgenteToExcel(
			Date fechaInicial, Date fechaFinal,
			Long idCentroOperacion, Long idGerencia, Long idEjecutivo,
			Long idPromotoria, Long rangoInicio, Long rangoFin, Locale locale,String formatoArchivo);
	
	TransporteImpresionDTO imprimirReporteAgenteBonosGerenciaToExcel(
			String anio, String mes, String mesFin, Long idCentroOperacion, Long idGerencia,
			Long idEjecutivo, Long idPromotoria, Long idAgente, Long idClasificacionAgente, Locale locale, String formatoArchivo);
	/**
	 * imprime el reporte de la contabilidad de midas vs mizar
	 * @param anioInicio
	 * @param mesInicio
	 * @param anioFin
	 * @param anioMes
	 * @param idCentroOperacion
	 * @param idGerencia
	 * @param idEjecutivo
	 * @param idPromotoria
	 * @param idAgente
	 * @param locale
	 * @return TransporteImpresionDTO
	 */
	InputStream imprimirReporteAgenteContabilidadMMToExcel(
			String anioInicio, String mesInicio,String anioFin, String mesFin,
			Long idCentroOperacion, Long idGerencia, Long idEjecutivo,
			Long idPromotoria, Long idAgente, Locale locale);
	
	/**
	 * imprime el reporte del detalle de bonos 
	 * @param idAgente
	 * @param anio
	 * @param mes
	 * @param anioFin
	 * @param mesFin
	 * @return
	 */
	TransporteImpresionDTO imprimirReporteDetalleBonosExcel(Long idAgente, Date fechaInicial, Date fechaFinal, String formatoArchivo);
	
	
	/**
	 * Genera el reporte de Calculo de Bono Mensual para un bono en especifico.
	 * @param idAgente
	 * @param tipoUsuario {@link #TIPO_USUARIO_ADMIN} ó {@link #TIPO_USUARIO_AGENTE} ó {@link #TIPO_USUARIO_PROMOTOR}
	 * @param idCalculoBonos
	 * @return
	 */
	public TransporteImpresionDTO imprimirReporteAgenteCalculoBonoPagado(Long idBeneficiario, String tipoUsuario, 
			Long idCalculoBonos);
	
	/**
	 * Genera el reporte de Calculo de Bono Mensual para un periodo definido por mes y año.
	 * @param idBeneficiario
	 * @param tipoUsuario {@link #TIPO_USUARIO_ADMIN} ó {@link #TIPO_USUARIO_AGENTE} ó {@link #TIPO_USUARIO_PROMOTOR}
	 * @param anio
	 * @param mes
	 * @param tipoBeneficiario  {@link #TIPO_BENEFICIARIO_AGENTE} ó {@link #TIPO_BENEFICIARIO_PROMOTOR} 
	 * @return
	 */
	public TransporteImpresionDTO imprimirReporteAgenteCalculoBonoMensual(Long idBeneficiario, String tipoUsuario, 
			String anio, String mes, Long tipoBeneficiario);	
	
	/**
	 * imprime el reporte de produccion
	 * @param fechaInicio
	 * @param fechaFin
	 * @return
	 */
	InputStream  imprimirReporteProduccion(Date fechaInicio, Date fechaFin);
	/**
	 * imprime el reporte de las primas Pagadas vs las primas emitidas
	 * @author jmendoza
	 * @param anio
	 * @param mes
	 * @param anioFin
	 * @param mesFin
	 * @param centroOperacion
	 * @param gerencia
	 * @param ejecutivo
	 * @param promotoria
	 * @param agente
	 * @return
	 */
	TransporteImpresionDTO imprimirReporePrimaPagadaVsPrimaEmitida(String anio, String mes,
			Long centroOperacion, Long gerencia, Long ejecutivo, Long promotoria, Long agente, String formatoArchivo);
		/**
		 *  imprime el reporte Global de comisiones y el reporte global de bonos
		 * @author jmendoza
		 * @param idAgente
		 * @param fecha
		 * @param idBono
		 * @param bandera
		 * @return
		 */
	TransporteImpresionDTO imprimirRepGlobalComisionYbono(Long idAgente, Date fecha, Long idBono, Boolean bandera);
	
	
	/**
	 * Impre el reporte de estado de cuenta
	 * @param idEmpresa
	 * @param idAgenteInicio
	 * @param idAgenteFin
	 * @param anio
	 * @return
	 * @author martin
	 */
	TransporteImpresionDTO imprimirReporteEstadoDeCuentaAgente(
			Long idEmpresa, Long idAgente, Long anioMes, Boolean guiaLectura, Boolean detalle, Boolean mostrarColAdicionales, Boolean afirmeComunica, String afirmeComunicaText,  Boolean tieneRole);
	
	
	TransporteImpresionDTO imprimirReporteDetalleProvision(String anio,String mes,ProvisionImportesRamo filtro, String formatoArchivo);
	
	
	/**
	 * 
	 * @param anio
	 * @param mes
	 * @param idAgente
	 * @return
	 * @author jmendoza
	 */
	TransporteImpresionDTO imprimirReciboHonorarios(BigDecimal solicitudChequeId);
	/**
	 * 
	 * @param anio
	 * @param mes
	 * @param filtro
	 * @return
	 */
	TransporteImpresionDTO imprimirReporteProvision(String anio,String mes,ProvisionImportesRamo filtro, String formatoArchivo);
	/**
	 * 
	 * @param anio
	 * @param mes
	 * @param agente
	 * @return
	 * @throws JRException
	 */
	TransporteImpresionDTO imprimirReporteDetallePrimas(String anio, String mes, Agente agente, String tipoSalidaArchivo) throws JRException;
	/**
	 * 
	 * @param mesAnio
	 * @return
	 * @throws JRException
	 */
	TransporteImpresionDTO imprimirReporteRecibosProveedores(String mesAnio) throws JRException;
	
	/**
	 * 
	 * @param anioInicio
	 * @param mesInicio
	 * @param anioFin
	 * @param mesFin
	 * @param idCentroOperacion
	 * @param idGerencia
	 * @param idEjecutivo
	 * @param idPromotoria
	 * @param idAgente
	 * @param locale
	 * @return
	 */
	TransporteImpresionDTO imprimirReporteAgenteContabilidadMizar(
			String anioInicio, String mesInicio,String anioFin, String mesFin,
			Long idCentroOperacion, Long idGerencia, Long idEjecutivo,
			Long idPromotoria, Long idAgente, Locale locale);
	
	public static enum TipoSalidaReportes{
		TO_EXCEL("xls"),
		TO_TXT("txt"),
		TO_PDF("pdf"),
		TO_XLSX("xlsx");
		private String value;
		private TipoSalidaReportes(String value){
			this.value=value;
		}
		public String getValue(){
			return value;
		}
	};
	
	TransporteImpresionDTO imprimirReporteSaldosVsContabilidad(List<SaldosVsContabilidadView> lista, String formatoSalida);
	
	
	TransporteImpresionDTO imprimirReporteDetalleAlertasSAP(String idEncabezadoAlertas);
	
	TransporteImpresionDTO imprimirBitacoraEmisionSAP(String bitacoraPoliza ,String bitacoraVin ,String bitacoraFechaEnvio,
			String estatusEnvio,String cesvi,String cii,String emision,String ocra,String prevencion,String pt,String csd,
			String siniestro, String sipac , String valuacion);
	
	TransporteImpresionDTO imprimirBitacoraSiniestrosSAP(String bitacoraPoliza ,String bitacoraVin ,String bitacoraFechaEnvio,
			String estatusEnvio,String cesvi,String cii,String emision,String ocra,String prevencion,String pt,String csd,
			String siniestro, String sipac , String valuacion);
	
	TransporteImpresionDTO imprimirReporteDetalleAlertasLineaSAP(String consultaNuemeroSerie, String usuario, String password);
	
	TransporteImpresionDTO imprimirReporteEfectividadEntrega(List<ReporteEfectividadEntrega> listDataSource, Map<String, Object> params);
	
	byte[] imprimirCotizacionMovilVida(ResumenCotMovilVidaDTO resumenCotMovilVidaDTO);
	
	
	TransporteImpresionDTO imprimirReporteEstatusEntregaFacturaExcel(DatosAgenteEstatusEntregaFactura datos);
	
	/**
	 * imprime el reporte de bonos por gerencia
	 * @param anio
	 * @param mesInicio
	 * @param mesFin
	 * @param crecimiento
	 * @param idCentroOperacion
	 * @param idGerencia
	 * @param idEjecutivo
	 * @param idPromotoria
	 * @param idAgente
	 * @param locale
	 * @param formatoArchivo
	 * @return
	 */
	TransporteImpresionDTO imprimirReporteAgenteVentasProduccion(
			String anio, String mesInicio, String mesFin, Integer crecimiento,  Long idCentroOperacion, Long idGerencia,
			Long idEjecutivo, Long idPromotoria, Long idAgente, Locale locale, String formatoArchivo);

	 public TransporteImpresionDTO imprimirReporteResumenDetalleBonosExcel(Long idAgente);
	 
	 public TransporteImpresionDTO imprimirCotizacion(
			 BigDecimal idToCotizacion,Locale locale, boolean caratula, boolean todosLosIncisos, boolean isMovil, String email, boolean isOutsider);
	 
	 public byte[] imprimirReciboHonorariosCompensaciones(DatosHonorariosAgentes datosHonorariosAgentes);
	 public byte[] imprimirReporteAgenteDetalle(List<OrdenPagosDetalle> listaOrdenPagosDetalle);	 
	 public byte[] imprimirReciboHonorariosComp(DatosHonorariosAgentes datosHonorariosAgentes);
	 public TransporteImpresionDTO imprimirReporteAutorizacionOrdenpago(String idLiquidacion);	 
	 public TransporteImpresionDTO imprimirReporteDetallePrimasComp(ReporteFiltrosDTO reporteFiltrosDTO);
	 public String getValorParameter(BigDecimal idToGrupoParamGen, BigDecimal codigoParma);
	 public void actualizarparametroReporte(String estatus);

	 /**
	 * 
	 * @param anio
	 * @param mes
	 * @param estado
	 * @param idAgente
	 * @param formatoArchivo
	 * @return
	 */
	TransporteImpresionDTO imprimirReporteDetPrimasRetencionImpuestosExcel(RetencionImpuestosReporteView filtro, String formatoArchivo);
}
