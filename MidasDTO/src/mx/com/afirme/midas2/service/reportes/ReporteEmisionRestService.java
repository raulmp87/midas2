package mx.com.afirme.midas2.service.reportes;

import java.io.InputStream;

import mx.com.afirme.midas2.domain.reportes.ReporteEmisionView;

public interface ReporteEmisionRestService {
	
	public InputStream imprimirReporteEmision(ReporteEmisionView filtro);

}
