package mx.com.afirme.midas2.service.reportes.vitro;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import com.afirme.nomina.model.*;

import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.reportes.vitro.ReportesVitroDTO;

/**
 * Clase que contiene los servicios que ser\u00e1n ocupados para
 * la generaci\u00f3n de los reportes del negocio de vitro.
 * 
 * @author AFIRME
 * 
 * @since 20160518
 * 
 * @version 1.0
 *
 */
@Local
public interface ReportesVitroService {
	
	public static final String GRUPO_NOMINA_SEMANAL = "VITRO NOMINA SEMANAL";
	public static final String GRUPO_NOMINA_QUINCENAL = "VITRO NOMINA QUINCENAL";
	
	public static final String NOMBRE_REPORTE_VITRO = "VITROEMPLEADOS";
	
	public static final String LIKE_GRUPOS_VITRO = "%VITRO%";
	
	public static final String NOMINA_SEMANAL = "NOMINA SEMANAL";
	public static final String NOMINA_QUINCENAL = "NOMINA QUINCENAL";
	
	public static final Long DIAS_SEMANA = Long.valueOf("7");
	public static final Long DIAS_QUINCENA = Long.valueOf("15");
	
	public static final Long ID_AGENTE_VENTA_VITRO = Long.valueOf("10527");
	
	/**
	 * Metodo que realiza la consulta de los grupos de vitro
	 * que tienen descuento.
	 * 
	 * @return Lista de grupos de vitro.
	 */
	public List<Dpngrupo> consultarGruposVitro();
	
	/**
	 * Metodo que realiza la consulta del calendario
	 * de los pagos que se tienen que efectuar.
	 * 
	 * @param idGrupo identificador del grupo.
	 * 
	 * @return Lista de fechas.
	 */
	public List<Date> consultarCalendarioCobro(Long idGrupo);
	
	/**
	 * Metodo que realiza la consulta de las polizas de vitro
	 * que son semanales.
	 * 
	 * @param fechaCorte fecha en la que se genera el reporte.
	 * 
	 * @param idNegocio Identificador del Negocio.
	 * 
	 * @param idFormaPago idForma de pago.
	 * 
	 * @return lista de polizas que se van a pagar.
	 */
	public List<ReportesVitroDTO> consultarPolizasVitroSemanal(Date fechaCorte, Long idNegocio, Long idFormaPago);
	
	/**
	 * Metodo que realiza la consulta de las polizas de vitro
	 * que son quincenales.
	 * 
	 * @param fechaCorte fecha en la que se genera el reporte.
	 * 
	 * @param idNegocio Identificador del Negocio.
	 * 
	 * @param idFormaPago idForma de pago.
	 * 
	 * @return lista de polizas que se van a pagar.
	 */
	public List<ReportesVitroDTO> consultarPolizasVitroQuincenal(Date fechaCorte, Long idNegocio, Long idFormaPago);
	
	/**
	 * Metodo que realiza la consulta de las polizas de vitro
	 * que son quincenales.
	 * 
	 * @param fechaCorte fecha en la que se genera el reporte.
	 * 
	 * @param tipoNomina Tipo de nomina de la que se va a generar el reporte.
	 * 
	 * @param idNegocio Tipo denegocio del cual se van a consultar las polizas.
	 * 
	 * @param idFormaPago Tipo de forma de pago de las polizas.
	 * 
	 * @param numeroDivide Numero de dias que componen la quincena o la semana
	 * 
	 * @param nombreGrupo Nombre del grupo al que pertenece la poliza
	 * 
	 * @return lista de polizas que se van a pagar.
	 */
	public List<ReportesVitroDTO> consultarPolizasVitro(Date fechaCorte, String tipoNomina, Long idNegocio, Long idFormaPago, Long numeroDivide, String nombreGrupo);
	
	/**
	 * M\u00e9todo que realiza la consulta del id del cliente de vitro.
	 * 
	 * @param idNegocio Identificador del negocio
	 * 
	 * @return Identificador del cliente;
	 */
	public List<Dpncliente> consultarCliente(String idNegocio);
	
	/**
	 * Metodo que genera el inputStream para la exportaci\u00f3n de las p\u00f3lizas vitro
	 * en excel.
	 * 
	 * @param dataSource Lista de p\u00f3lizas que ser\u00e1n exportadas.
	 * 
	 * @param nombreLibro Nombre del libro que tendr\u00e1 el documento.
	 * 
	 * @return InputStream que contiene el documento exportado.
	 * 
	 * @throws InvalidFormatException Formato no valido
	 * 
	 * @throws IllegalAccessException Acceso ilegal.
	 * 
	 * @throws InvocationTargetException Invocacion erronea
	 * 
	 * @throws IOException 
	 */
	public TransporteImpresionDTO generarReporteVitro(List<ReportesVitroDTO> dataSource, String nombreLibro) throws InvalidFormatException, IllegalAccessException, InvocationTargetException, IOException;
	
	/**
	 * Metodo que consulta los negocios que se encuentran disponibles para el agente
	 * y dentro de las tablas de DPN
	 * 
	 * @return Lista de negocios.
	 */
	public List<Negocio> consultarNegocios();
}
