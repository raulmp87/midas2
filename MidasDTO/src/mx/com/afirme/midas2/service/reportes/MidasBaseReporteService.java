package mx.com.afirme.midas2.service.reportes;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.service.reportes.MidasBaseReporte.MidasBaseReporteExcel;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

@Local
public interface MidasBaseReporteService {
	
	public InputStream generateExcelVeryLong(MidasBaseReporteExcel sheet) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException, FileNotFoundException, InvalidFormatException;
	
	public InputStream generateExcel(MidasBaseReporteExcel sheet) throws IllegalAccessException, IllegalArgumentException,
	InvocationTargetException, IOException, FileNotFoundException,
	InvalidFormatException;

	public InputStream generateExcel(List<MidasBaseReporteExcel> sheets);
	
	public String generateExcel(List<?> list, Workbook wb, Sheet sheet1, int numRows, int rowIndex) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException;
	
	public void populateRows(Row row, Row rowColumnsName, Object obj)
			throws IllegalAccessException, IllegalArgumentException,
			InvocationTargetException;

	public void populateRows(Row row, Row rowColumnsName,
			String[] columnsNames, Object obj) throws IllegalAccessException,
			IllegalArgumentException, InvocationTargetException;

	public void populateRows_setValue(Row row, int x, Object v);
	
	public InputStream margeFiles(List<String> fileNames, String fileName) throws FileNotFoundException, IOException, InvalidFormatException;
	
	public int calculateGap(int dataSourceSize);
	
	public InputStream findFile(String fileName);
	
	public void deleteTemporalsFiles();
	
	public String generateExcelSXSSF(List<?> list) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException;
}