package mx.com.afirme.midas2.service.reportes.historial.cobranza;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.reportesAgente.ReporteHistorialCobranzaDTO;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

@Local
public interface ReporteHistorialCobranzaService {
	public Long ID_AGENTE_MOCK_DEV = new Long(93218);
	
	public List<ReporteHistorialCobranzaDTO> consultarHistCobranzaPorAgente(Long idAgente);
	public TransporteImpresionDTO generarReporteCobranza(List<ReporteHistorialCobranzaDTO> dataSource, String nombreLibro) throws IllegalArgumentException, FileNotFoundException, InvalidFormatException, IllegalAccessException, InvocationTargetException, IOException;
}
