package mx.com.afirme.midas2.service.reportes;

import java.io.InputStream;
import java.util.Date;
import mx.com.afirme.midas2.util.MidasException;

public interface GeneracionReporteAutosNZService {
	public int generarReporte( Date fInicial, Date fFinal, String usuario ) throws MidasException;

	public InputStream exportarReporte( Date fInicial, Date fFinal  ) throws MidasException;
	
	public String getFechasReporte(int tipoReporte);
	
}
