package mx.com.afirme.midas2.service.reportes.MidasBaseReporte;

import java.util.List;

public class MidasBaseReporteExcel {
	private String bookName;
	private String sheetName;
	private List<?> dataSource;
	private String[] columnNames;

	public MidasBaseReporteExcel() {
		this.bookName = "excel.xlsx";
		this.sheetName = "Hoja1";
	}

	public MidasBaseReporteExcel(String bookName, String sheetName) {
		this.bookName = bookName;
		this.sheetName = sheetName;
	}

	public String getBookName() {
		return bookName;
	}

	public String getSheetName() {
		return sheetName;
	}

	public List<?> getDataSource() {
		return dataSource;
	}

	public String[] getColumnNames() {
		return columnNames;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName + ".xlsx";
	}

	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}

	public void setDataSource(List<?> dataSource) {
		this.dataSource = dataSource;
	}

	public void setColumnNames(String[] columnNames) {
		this.columnNames = columnNames;
	}

}
