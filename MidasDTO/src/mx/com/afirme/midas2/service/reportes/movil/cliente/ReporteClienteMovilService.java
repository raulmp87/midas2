package mx.com.afirme.midas2.service.reportes.movil.cliente;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.producto.NegocioSeguros;
import mx.com.afirme.midas2.domain.movil.cliente.ClientePolizas;

@Local
public interface ReporteClienteMovilService {

	public List<ClientePolizas> getReportePolizasClienteMovil(String tipoPoliza);
	public List<NegocioSeguros> getTipoPolizas();
}

