package mx.com.afirme.midas2.service.reportes;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.domain.reportes.reporteSesas.GeneracionSesasDTO;
import mx.com.afirme.midas2.domain.reportes.reporteSesas.ReporteSesasMatrizDTO;
import mx.com.afirme.midas2.util.MidasException;

public interface GeneracionSesasService {
	
	public GeneracionSesasDTO obtenerLog() throws MidasException;
	
	public int iniciarJob( Date fInicial, Date fFinal, String usuario, String claveNeg ) throws MidasException;

	public InputStream generarReporte( String modo, String numArchivos, String claveNeg ) throws MidasException;
	
	public List<GeneracionSesasDTO> getTareasSesas(String claveNeg) throws MidasException;
	
	public void generarSesasAut();
	
	public List<ReporteSesasMatrizDTO> generarMatriz(Integer idGeneracion) throws MidasException;

	public void generarArchivosSesas(String claveNeg);
	
	public void generarSesasVida();

	public InputStream generarReporteVida(int idArchivo) throws Exception;

	public String getNombreReporteVida(int idArchivo, int idGeneracion);
}