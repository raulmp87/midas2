package mx.com.afirme.midas2.service.reportes;

import java.util.Date;

public interface ReporteService {
	
	public Date getUltimaFechaGeneracionDporP();
	
	public Integer getEjecutandoGeneracionDXP();
	
	public void generaDXP(Date fecha);
}
