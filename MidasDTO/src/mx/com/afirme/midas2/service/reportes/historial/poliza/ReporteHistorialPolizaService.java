package mx.com.afirme.midas2.service.reportes.historial.poliza;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.reportesAgente.ReporteHistorialPolizaDTO;

/**
 * Interfaz que contiene la l\u00f3gica para generar el reporte de 
 * historico de polizas por agente.
 * 
 * @author AFIRME
 * 
 * @since 28072016
 * 
 * @version 1.0
 *
 */
@Local
public interface ReporteHistorialPolizaService {
	
	/**
	 * M\u00e0todo que realiza la consulta del historico de las p\u00f3lizas pertenecientes a un agente.
	 * 
	 * @param idAgente identificador de agente.
	 * 
	 * @return Lista de p\u00f3lizas con su historico.
	 */
	public List<ReporteHistorialPolizaDTO> consultarHistorialPolizasPorAgente(Long idAgente);
	
	/**
	 * M\u00e9todo que realiza la consulta del historico de una
	 * p\u00f3liza en particular.
	 * 
	 * @param numeroPoliza N\u00famero de p\u00f3liza de la cual se
	 * va a realizar la b\u00fasqueda.
	 *  
	 * @return Lista de p\u00f3lizas con su historico.
	 */
	public List<ReporteHistorialPolizaDTO> consultarHistorialPorNumeroPoliza(Long numeroPoliza);
	
	/**
	 * Metodo que genera el reporte de historial de p\u00f3liza.
	 * 
	 * @param dataSource Lista de To a partir de la cual se va a generar el reporte.
	 * 
	 * @param nombreLibro Nombre que recibir\u00e1 el archivo.
	 * 
	 * @return Objeto que contiene el archivo.
	 */
	public TransporteImpresionDTO generarReporte(List<ReporteHistorialPolizaDTO> dataSource, String nombreLibro);
}
