package mx.com.afirme.midas2.service.juridico;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.base.PaginadoDTO;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation.OperationType;
import mx.com.afirme.midas2.domain.juridico.CitaJuridico;

@Local
public interface CitaJuridicoService {
	
	
	public class CitaJuridicoFiltro extends PaginadoDTO{
	
				
		/**
		 * 
		 */
		private static final long serialVersionUID = 3844980856993511021L;
		
		private	Long idOficina;
		private	Long numeroCita;
		private String nombreUsuario;
		private Date fechaAltaInicio;
		private Date fechaAltaFin;
		private Date fechaCitaInicio;
		private Date fechaCitaFin;
		private String tipoReclamacion;
		private String claveRamoJuridico;
		private String claveEstatusJuridico;
		
		@FilterPersistenceAnnotation(persistenceName="oficina.id")
		public Long getIdOficina() {
			return idOficina;
		}
		public void setIdOficina(Long idOficina) {
			this.idOficina = idOficina;
		}
		@FilterPersistenceAnnotation(persistenceName="id")
		public Long getNumeroCita() {
			return numeroCita;
		}
		public void setNumeroCita(Long numeroCita) {
			this.numeroCita = numeroCita;
		}
		
		public String getNombreUsuario() {
			return nombreUsuario;
		}
		public void setNombreUsuario(String nombreUsuario) {
			this.nombreUsuario = nombreUsuario;
		}
		@FilterPersistenceAnnotation(persistenceName="fechaCreacion", truncateDate=true, operation=OperationType.GREATERTHANEQUAL, paramKey="fechaAltaInicio")
		public Date getFechaAltaInicio() {
			return fechaAltaInicio;
		}
		public void setFechaAltaInicio(Date fechaAltaInicio) {
			this.fechaAltaInicio = fechaAltaInicio;
		}
		@FilterPersistenceAnnotation(persistenceName="fechaCreacion", truncateDate=true, operation=OperationType.LESSTHANEQUAL, paramKey="fechaAltaFin")
		public Date getFechaAltaFin() {
			return fechaAltaFin;
		}
		public void setFechaAltaFin(Date fechaAltaFin) {
			this.fechaAltaFin = fechaAltaFin;
		}
		@FilterPersistenceAnnotation(persistenceName="fechaInicio", truncateDate=true, operation=OperationType.GREATERTHANEQUAL, paramKey="fechaCitaInicio")
		public Date getFechaCitaInicio() {
			return fechaCitaInicio;
		}
		public void setFechaCitaInicio(Date fechaCitaInicio) {
			this.fechaCitaInicio = fechaCitaInicio;
		}
		@FilterPersistenceAnnotation(persistenceName="fechaInicio", truncateDate=true, operation=OperationType.LESSTHANEQUAL, paramKey="fechaCitaFin")
		public Date getFechaCitaFin() {
			return fechaCitaFin;
		}
		public void setFechaCitaFin(Date fechaCitaFin) {
			this.fechaCitaFin = fechaCitaFin;
		}
		
		public String getTipoReclamacion() {
			return tipoReclamacion;
		}
		public void setTipoReclamacion(String tipoReclamacion) {
			this.tipoReclamacion = tipoReclamacion;
		}
		@FilterPersistenceAnnotation(persistenceName="ramo")
		public String getClaveRamoJuridico() {
			return claveRamoJuridico;
		}
		public void setClaveRamoJuridico(String claveRamoJuridico) {
			this.claveRamoJuridico = claveRamoJuridico;
		}
		@FilterPersistenceAnnotation(persistenceName="estatus")
		public String getClaveEstatusJuridico() {
			return claveEstatusJuridico;
		}
		public void setClaveEstatusJuridico(String claveEstatusJuridico) {
			this.claveEstatusJuridico = claveEstatusJuridico;
		}		
	}
	
	public CitaJuridico obtener(Long id);
	public List<CitaJuridico> filtrar(CitaJuridicoFiltro filtro);
	public Long filtrarPaginado(CitaJuridicoFiltro filtro);
	public CitaJuridico guardar(CitaJuridico cita);
	public void eliminar(Long id);
}
