package mx.com.afirme.midas2.service.juridico;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.juridico.CatalogoJuridico;
import mx.com.afirme.midas2.dto.juridico.CatalogoJuridicoDTO;
import java.util.List;


/**
 * Interface que define los m�todos de negocio requeridos para administrar los
 * cat�logos de jur�dico
 * @author usuario
 * @version 1.0
 * @created 14-ene-2015 10:29:04 a.m.
 */

@Local
public interface CatalogoJuridicoService {

	
	
	public static final String TIPO_CAT_JURIDICO_DELEGACION="DEL";
	public static final String TIPO_CAT_JURIDICO_PROCEDIMIENTO="PROC";
	public static final String TIPO_CAT_JURIDICO_MOTIVOS="MOT";
	
	public static final int ESTATUS_CAT_JURIDICO_ACTIVO=1;
	public static final int ESTATUS_CAT_JURIDICO_INACTIVO=0;
	
	
	/**
	 * Guarda un catalogoJuridico. El flujo es el siguiente:
	 * 
	 * * Si la entidad es nueva (NO EDICION), obtener el siguiente numero/codigo de
	 * cat�logo a trav�s de entidadDao.getIncrementedProperty con el filtro para el
	 * tipo catalogo juridico que se est� manejando.
	 * * Salvar la entidad con entidadservice.save().
	 * 
	 * @param catalogo
	 */
	public void guardar(CatalogoJuridico catalogo);

	/**
	 * Guarda o actualiza una Delegacion.
	 * 
	 * * Setea en el objeto el tipo catalogo para Delegacion.
	 * * Invoca guardar.
	 * 
	 * @param catalogo
	 */
	public void guardarDelegacion(CatalogoJuridicoDTO catalogo);

	/**
	 * Guarda o actualiza un motivo.
	 * 
	 * * Setea en el objeto el tipo catalogo para Motivo.
	 * * Invoca guardar.
	 * 
	 * @param catalogo
	 */
	public void guardarMotivoReclamacion(CatalogoJuridicoDTO catalogo);

	/**
	 * Guarda o actualiza un procedimiento.
	 * 
	 * * Setea en el objeto el tipo catalogo para procedimiento.
	 * * Invoca guardar.
	 * 
	 * @param catalogo
	 */
	public void guardarProcedimiento(CatalogoJuridicoDTO catalogo);

	/**
	 * Obtener un catalogo juridico dependiendo de los filtros utilizados. Invocar
	 * obtenerListado con el filtro recibido y retornar un elemento de la lista.
	 * 
	 * @param filtro
	 */
	public CatalogoJuridico obtener(CatalogoJuridico filtro);

	/**
	 * M�todo de apoyo para obtener el listado de delegaciones.
	 * 
	 * * Setear en el filtro el valor para el tipo de catalogo Delegaciones.
	 * * Invocar obtenerListado con el filtro y retornar los valores.
	 * 
	 * @param filtro
	 */
	public List<CatalogoJuridicoDTO> obtenerDelegaciones(CatalogoJuridico filtro);

	/**
	 * M�todo para retornar los cat�logos dependiendo del filtro utilizado. Invocar
	 * entidadService.findByFilterObject con los valores adecuados y retornar la lista
	 * ordenada por codigo ascendente
	 * 
	 * @param filtro
	 */
	public List<CatalogoJuridico> obtenerListado(CatalogoJuridico filtro);

	/**
	 * M�todo de apoyo para obtener el listado de motivos.
	 * 
	 * * Setear en el filtro el valor para el tipo de catalogo Motivos.
	 * * Invocar obtenerListado con el filtro y retornar los valores.
	 * 
	 * @param filtro
	 */
	public List<CatalogoJuridicoDTO> obtenerMotivosReclamacion(CatalogoJuridico filtro);

	/**
	 * M�todo de apoyo para obtener el listado de Procedimientos.
	 * 
	 * * Setear en el filtro el valor para el tipo de catalogo Procedimientos.
	 * * Invocar obtenerListado con el filtro y retornar los valores.
	 * 
	 * @param filtro
	 */
	public List<CatalogoJuridicoDTO> obtenerProcedimientos(CatalogoJuridico filtro);
	
	
	/**
	 * M�todo de apoyo para obtener el listado de Procedimientos.
	 * 
	 * * en base al objeto DTO crea la entidad CatalogoJuridico
	 * 	 * 
	 * @param CatalogoJuridicoDTO catalogo
	 */
	public CatalogoJuridico getCatalogoJuridico(CatalogoJuridicoDTO catalogo);
	
	/**
	 * M�todo de apoyo para obtener el listado de Procedimientos.
	 * 
	 * * en base ala entidad CatalogoJuridico catalogo crea el objeto DTO, este es usado para mostrar en los JSP y listas de objetos
	 * 	 * 
	 * @param CatalogoJuridico catalogo
	 */
	public CatalogoJuridicoDTO getCatalogoFiltro(CatalogoJuridico catalogo);

}