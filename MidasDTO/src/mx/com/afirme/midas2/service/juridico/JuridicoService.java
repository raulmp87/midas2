package mx.com.afirme.midas2.service.juridico;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation.OperationType;
import mx.com.afirme.midas2.dto.juridico.ReclamacionJuridicoDTO;
import mx.com.afirme.midas2.dto.juridico.ReclamacionOficioJuridicoDTO;
import mx.com.afirme.midas2.dto.juridico.ReclamacionReservaJuridicoDTO;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService.CatalogoFiltro;

@Local
public interface JuridicoService {

	/**
	 * Notifica para bloquear al asegurado y la poliza
	 * @param asegurado
	 */
	public void bloquearAsegurado(String numeroPoliza, String asegurado);
	
	/**
	 * Notifica de la modificacion de la reserva 
	 * @param idCoberturaRepCab
	 */
	public void notificacionReserva(Long idReclamacion, Integer idOficio, Long idSiniestro, String numeroPoliza, String asegurado);
	
	/**
	 * Obtiene los oficios de una reclamacion ordenados por numero de oficio
	 * @param idReclamacion
	 * @return
	 */
	public List<ReclamacionOficioJuridicoDTO> obtenerOficios(Long numeroReclamacion);
	
	/**
	 * Obtiene la informacion un oficio de una reclamacion en particular. Si no se envía oficio 
	 * obtendra la informacion del ultimo oficio registrado
	 * @param idReclamacion
	 * @param idOficio
	 * @return
	 */
	public ReclamacionOficioJuridicoDTO obtenerOficio(Long numeroReclamacion, Integer numeroOficio);
	
	/**
	 * Obtiene una lista de reclamaciones de Juridico utilizando los filtros capturados en pantalla
	 * @param reclamacionFiltro
	 * @return
	 */
	public List<ReclamacionJuridicoDTO> obtenerReclamaciones(ReclamacionJuridicoFiltro reclamacionFiltro);
	
	/**
	 * Obtiene las reservas capturadas por oficio de manera que se puedan consultar indempendientemente utilizando el id de oficio
	 * @param idOficio
	 * @return
	 */
	public List<ReclamacionReservaJuridicoDTO> obtenerReservasOficio(Long numeroReclamacion, Integer numeroOficio, Long idSiniestro);
	
	/**
	 * Guarda una reclamacion, siempre con un nuevo oficio
	 * @param reclamacion
	 */
	public ReclamacionOficioJuridicoDTO salvarReclamacion(ReclamacionOficioJuridicoDTO reclamacionOficio, List<ReclamacionReservaJuridicoDTO> reservas);
	
	/**
	 * Si existe el numero de poliza en el sistema
	 * @param numeroPoliza
	 * @return
	 */
	public Boolean validarPoliza(String numeroPoliza);
	
	/**
	 * Filtro para la busqueda de reclamaciones
	 * @author user
	 *
	 */
	public class ReclamacionJuridicoFiltro extends CatalogoFiltro{
		
		private Long numeroReclamacion;
		private Integer numeroOficio;
		private Long oficinaJuridicaId;
		private Long oficinaSiniestroId;
		private String numeroPoliza;
		private String numeroSiniestro;
		private String estatusReclamo;
		private Date fechaNotificacionDe;
		private Date fechaNotificacionHasta;
		private String asegurado;
		private String reclamante;
		private Long procedimiento;
		private String tipoReclamacion;
		private String ramo;
		private String expediente;
		/**
		 * @return the oficinaJuridicaId
		 */
		@FilterPersistenceAnnotation(persistenceName="reclamacionJuridico.oficinaJuridico.id")
		public Long getOficinaJuridicaId() {
			return oficinaJuridicaId;
		}
		/**
		 * @param oficinaJuridicaId the oficinaJuridicaId to set
		 */
		public void setOficinaJuridicaId(Long oficinaJuridicaId) {
			this.oficinaJuridicaId = oficinaJuridicaId;
		}
		/**
		 * @return the numeroSiniestro
		 */
		@FilterPersistenceAnnotation(persistenceName="reclamacionJuridico.numeroSiniestro",  operation=OperationType.LIKE)
		public String getNumeroSiniestro() {
			return numeroSiniestro;
		}
		/**
		 * @param numeroSiniestro the numeroSiniestro to set
		 */
		
		public void setNumeroSiniestro(String numeroSiniestro) {
			this.numeroSiniestro = numeroSiniestro;
		}
		/**
		 * @return the fechaNotificacionDe
		 */
		@FilterPersistenceAnnotation(persistenceName="reclamacionJuridico.fechaNotificacion", truncateDate=true , operation=OperationType.GREATERTHANEQUAL, paramKey="fechaInicial")
		public Date getFechaNotificacionDe() {
			return fechaNotificacionDe;
		}
		/**
		 * @param fechaNotificacionDe the fechaNotificacionDe to set
		 */
		public void setFechaNotificacionDe(Date fechaNotificacionDe) {
			this.fechaNotificacionDe = fechaNotificacionDe;
		}
		/**
		 * @return the fechaNotificacionHasta
		 */
		@FilterPersistenceAnnotation(persistenceName="reclamacionJuridico.fechaNotificacion", truncateDate=true , operation=OperationType.LESSTHANEQUAL, paramKey="fechaFinal")
		public Date getFechaNotificacionHasta() {
			return fechaNotificacionHasta;
		}
		/**
		 * @param fechaNotificacionHasta the fechaNotificacionHasta to set
		 */
		public void setFechaNotificacionHasta(Date fechaNotificacionHasta) {
			this.fechaNotificacionHasta = fechaNotificacionHasta;
		}
		/**
		 * @return the asegurado
		 */
		@FilterPersistenceAnnotation(persistenceName="asegurado", operation=OperationType.LIKE)
		public String getAsegurado() {
			return asegurado;
		}
		/**
		 * @param asegurado the asegurado to set
		 */
		public void setAsegurado(String asegurado) {
			this.asegurado = asegurado;
		}
		/**
		 * @return the reclamante
		 */
		@FilterPersistenceAnnotation(persistenceName="reclamante", operation=OperationType.LIKE)
		public String getReclamante() {
			return reclamante;
		}
		/**
		 * @param reclamante the reclamante to set
		 */
		public void setReclamante(String reclamante) {
			this.reclamante = reclamante;
		}
		/**
		 * @return the procedimiento
		 */
		@FilterPersistenceAnnotation(persistenceName="procedimiento.id")
		public Long getProcedimiento() {
			return procedimiento;
		}
		/**
		 * @param procedimiento the procedimiento to set
		 */
		public void setProcedimiento(Long procedimiento) {
			this.procedimiento = procedimiento;
		}
		/**
		 * @return the tipoReclamacion
		 */
		@FilterPersistenceAnnotation(persistenceName="reclamacionJuridico.tipoReclamacion")
		public String getTipoReclamacion() {
			return tipoReclamacion;
		}
		/**
		 * @param tipoReclamacion the tipoReclamacion to set
		 */
		public void setTipoReclamacion(String tipoReclamacion) {
			this.tipoReclamacion = tipoReclamacion;
		}
		/**
		 * @return the ramo
		 */
		@FilterPersistenceAnnotation(persistenceName="reclamacionJuridico.ramo")
		public String getRamo() {
			return ramo;
		}
		/**
		 * @param ramo the ramo to set
		 */
		public void setRamo(String ramo) {
			this.ramo = ramo;
		}
		/**
		 * @return the estatusReclamo
		 */
		@FilterPersistenceAnnotation(persistenceName="estatus")
		public String getEstatusReclamo() {
			return estatusReclamo;
		}
		/**
		 * @param estatusReclamo the estatusReclamo to set
		 */
		public void setEstatusReclamo(String estatusReclamo) {
			this.estatusReclamo = estatusReclamo;
		}
		/**
		 * @return the numeroReclamacion
		 */
		@FilterPersistenceAnnotation(persistenceName="reclamacionJuridico.id")
		public Long getNumeroReclamacion() {
			return numeroReclamacion;
		}
		/**
		 * @param numeroReclamacion the numeroReclamacion to set
		 */
		public void setNumeroReclamacion(Long numeroReclamacion) {
			this.numeroReclamacion = numeroReclamacion;
		}
		/**
		 * @return the oficinaSiniestroId
		 */
		@FilterPersistenceAnnotation(persistenceName="reclamacionJuridico.oficinaSiniestro.id")
		public Long getOficinaSiniestroId() {
			return oficinaSiniestroId;
		}
		/**
		 * @param oficinaSiniestroId the oficinaSiniestroId to set
		 */
		public void setOficinaSiniestroId(Long oficinaSiniestroId) {
			this.oficinaSiniestroId = oficinaSiniestroId;
		}
		/**
		 * @return the expediente
		 */
		@FilterPersistenceAnnotation(persistenceName="expediente", operation=OperationType.LIKE)
		public String getExpediente() {
			return expediente;
		}
		/**
		 * @param expediente the expediente to set
		 */
		public void setExpediente(String expediente) {
			this.expediente = expediente;
		}
		/**
		 * @return the numeroPoliza
		 */
		@FilterPersistenceAnnotation(persistenceName="reclamacionJuridico.numeroPoliza", operation=OperationType.LIKE)
		public String getNumeroPoliza() {
			return numeroPoliza;
		}
		/**
		 * @param numeroPoliza the numeroPoliza to set
		 */
		public void setNumeroPoliza(String numeroPoliza) {
			this.numeroPoliza = numeroPoliza;
		}
		/**
		 * @return the numeroOficio
		 */
		@FilterPersistenceAnnotation(persistenceName="id.oficio")
		public Integer getNumeroOficio() {
			return numeroOficio;
		}
		/**
		 * @param numeroOficio the numeroOficio to set
		 */
		public void setNumeroOficio(Integer numeroOficio) {
			this.numeroOficio = numeroOficio;
		}
		
	}

}