package mx.com.afirme.midas2.service.condicionesGenerales;


public interface WSCondicionesGeneralesService {
	
	byte[] consultarCondicionesGenerales(Long idPoliza, String apSolicita) throws Exception;
}
