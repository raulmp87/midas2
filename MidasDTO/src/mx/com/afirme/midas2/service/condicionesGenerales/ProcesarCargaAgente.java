package mx.com.afirme.midas2.service.condicionesGenerales;

import mx.com.afirme.midas2.domain.condicionesGenerales.CgAgente;

public interface ProcesarCargaAgente {
	
	public void guardarCgAgente(  CgAgente cgAgente ) throws Exception;

}
