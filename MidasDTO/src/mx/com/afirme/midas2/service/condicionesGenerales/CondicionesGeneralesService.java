package mx.com.afirme.midas2.service.condicionesGenerales;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.domain.condicionesGenerales.CgAgente;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgAgenteView;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgCentro;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgOrden;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgProveedor;

public interface CondicionesGeneralesService {
	List<CgProveedor> traerListaProveedor( CgProveedor cgProveedor ) throws Exception;	
	void guardarActualizarProveedor( CgProveedor cgProveedor, String usuario ) throws Exception;
	CgProveedor traerProveedor( Long id ) throws Exception;
	void eliminarProveedor( CgProveedor cgProveedor ) throws Exception;
	void eliminarCentroProveedor( CgProveedor cgProveedor, String idToCentro ) throws Exception;
	void guardarCentroProveedor( CgProveedor cgProveedor, String idToCentro ) throws Exception;
	List<CgAgenteView> traerListaAgente( CgAgente cgAgente ) throws Exception;
	CgAgente traerAgente( Long id ) throws Exception;
	CgAgenteView traerCgAgenteView( Long id ) throws Exception;
	void actualizarAgente( CgAgente cgAgente, String usuario ) throws Exception;
	void eliminarAgente( Long id, String usuario ) throws Exception;
	InputStream obtenerLayout()  throws Exception;
	List<String> cargarInformacion( BigDecimal idToControlArchivo, String usuario ) throws Exception;
	List<CgCentro> traerListaCentro( CgCentro cgCentro ) throws Exception;
	CgCentro traerCentro( Long id ) throws Exception;
	List<CgCentro> traeCentrosProveedor( String centrosStr ) throws Exception;
	void actualizarCentro( CgCentro cgCentro, String usuario ) throws Exception;
	void guardarCentro( CgCentro cgCentro, String usuario ) throws Exception;
	void eliminarCentro( Long id, String usuario ) throws Exception;
	void activarCentro( Long id, String usuario ) throws Exception;
	List<CgOrden> traerListaOrden( CgOrden cgOrden, String usuario ) throws Exception;
	CgOrden traerOrden( Long id ) throws Exception;
	void confirmarOrden( Long id, String usuario ) throws Exception;
	void actualizarOrden( CgOrden cgOrden, String usuario ) throws Exception;
	void guardarOrden( CgOrden cgOrden, String usuario, boolean isAgente ) throws Exception;
	InputStream obtenerReporteInventario( String idCentro, String path ) throws Exception;
	Map<Long, String> getMapCentroEmisor() throws Exception;
}
