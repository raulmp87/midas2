package mx.com.afirme.midas2.service.webservice;

import java.rmi.RemoteException;

import javax.ejb.Local;

import com.afirme.eibs.services.payments.vo.CreditCardPromotionVO;
import com.afirme.ws.archivos.AvisoPrivacidad;

@Local
public interface WebServiceDelegate {

	/**
	 * Obtiene el PDF correspondiente del Aviso de Privacidad
	 * @param aviso para indicar que PDF se obtendra (Autos, Vida, Daños)
	 * @return
	 * @throws RemoteException
	 */
	public byte[] obtenerAvisoPrivacidad(AvisoPrivacidad aviso) throws RemoteException;
	
	/**
	 * Obtiene la carta de cargos automáticos de una póliza
	 * @param identifierId
	 * @return
	 * @throws RemoteException
	 */
	public byte[] obtenerCartaDeCargosAutomaticos(String identifierId) throws RemoteException;
	
	/**
	 * Obtiene las promociones disponibles de cierta tarjeta de crédito
	 * @param numTarjeta
	 * @return
	 * @throws RemoteException
	 */
	public CreditCardPromotionVO[] obtenerPromocionesTC(String numTarjeta)throws RemoteException;
	
}
