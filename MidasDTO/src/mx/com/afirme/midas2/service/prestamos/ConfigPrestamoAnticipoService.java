package mx.com.afirme.midas2.service.prestamos;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.prestamos.ConfigPrestamoAnticipo;
import mx.com.afirme.midas2.dto.fuerzaventa.EntregoDocumentosView;
import mx.com.afirme.midas2.dto.prestamos.ConfigPrestamoAnticipoView;
import mx.com.afirme.midas2.dto.prestamos.ReporteIngresosAgente;
import mx.com.afirme.midas2.service.fuerzaventa.GenericMailService;

@Local
public interface ConfigPrestamoAnticipoService extends GenericMailService {

	public ConfigPrestamoAnticipo  loadById(ConfigPrestamoAnticipo obj) throws Exception;
	
	public List<ConfigPrestamoAnticipoView> findByFilters(ConfigPrestamoAnticipo obj) throws Exception;
	
	public ConfigPrestamoAnticipo save(ConfigPrestamoAnticipo obj)throws Exception;
	
	public ConfigPrestamoAnticipo updateEstatus(ConfigPrestamoAnticipo configPrestamoAnticipo, String elementoCatalogo)throws Exception;

	public ReporteIngresosAgente obtenerDatosReporte(ConfigPrestamoAnticipo configPrestamoAnticipo) throws Exception;
	
	public ConfigPrestamoAnticipo autorizarMovimiento(ConfigPrestamoAnticipo config) throws Exception;
	
	public void enviarCorreo(Long id,Map<String,Map<String, List<String>>> mapCorreos,
			String mensaje,String tituloMensaje, int tipoTemplate);
	
	public ConfigPrestamoAnticipo solicitarChequeDelMovimiento(ConfigPrestamoAnticipo config) throws Exception;
	
	public void crearYGenerarDocumentosFortimax(ConfigPrestamoAnticipo config,Long numPagares) throws Exception;
	
	public List<EntregoDocumentosView> consultaEstatusDocumentos(Long idPrestamo,Long idAgente) throws Exception;
	
	public void auditarDocumentosEntregadosPrestamos(Long idPrestamo,Long idAgente,String nombreAplicacion) throws Exception;
}
