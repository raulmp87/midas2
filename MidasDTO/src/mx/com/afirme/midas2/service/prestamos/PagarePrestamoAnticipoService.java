package mx.com.afirme.midas2.service.prestamos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.prestamos.ConfigPrestamoAnticipo;
import mx.com.afirme.midas2.domain.prestamos.PagarePrestamoAnticipo;
import mx.com.afirme.midas2.dto.impresiones.PagarePrestamoAnticipoImpresion;

@Local
public interface PagarePrestamoAnticipoService {
	public PagarePrestamoAnticipo loadById(PagarePrestamoAnticipo pagare) throws Exception;

	public List<PagarePrestamoAnticipo> loadByIdConfigPrestamo(ConfigPrestamoAnticipo obj) throws Exception;
	
	public PagarePrestamoAnticipo save(PagarePrestamoAnticipo obj, Long idConfigPrestamos)throws Exception;
	
	public PagarePrestamoAnticipo save(List<PagarePrestamoAnticipo> list, Long idConfigPrestamos)throws Exception;
	
	public PagarePrestamoAnticipo updateEstatusPagare(PagarePrestamoAnticipo obj, String elementoCatalogo)throws Exception;
	
	public List<PagarePrestamoAnticipo> updateEstatusPagare(List<PagarePrestamoAnticipo> list, String elementoCatalogo) throws Exception ;
	
	public boolean llenarYGuardarListaPagares(String pagaresAgregados, ConfigPrestamoAnticipo obj);
	
	public PagarePrestamoAnticipoImpresion infoImpresionPagare(PagarePrestamoAnticipo pagare) throws Exception;
	
	public PagarePrestamoAnticipoImpresion infoImpresionPagarePrincipal(ConfigPrestamoAnticipo config) throws Exception;
	
	public PagarePrestamoAnticipo aplicaPagare(String pagaresAgregados,ConfigPrestamoAnticipo config,PagarePrestamoAnticipo pagareActivado) throws Exception;
	
	public void activacionAutomaticaDePagares() throws Exception;
	
	public void initialize();

}
