package mx.com.afirme.midas2.service.cargos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.cargos.ConfigCargos;
import mx.com.afirme.midas2.domain.cargos.DetalleCargos;

@Local
public interface DetalleCargosService {
	
	public DetalleCargos loadById(DetalleCargos detalleCargos) throws Exception;
	
	public List<DetalleCargos> loadByIdConfigCargos(ConfigCargos configCargos) throws Exception;
	
	public DetalleCargos save(DetalleCargos detalleCargos, Long idConfigCargos)throws Exception;
	
	public DetalleCargos save(List<DetalleCargos> list, Long idConfigCargos)throws Exception;
	
	public DetalleCargos updateEstatusDetalleCargos(DetalleCargos detalleCargos, String elementoCatalogo)throws Exception;
	
	public List<DetalleCargos> updateEstatusDetalleCargos(List<DetalleCargos> list, String elementoCatalogo) throws Exception ;
	
	public boolean llenarYGuardarListaDetalles(String detalleAgregados, ConfigCargos obj) throws Exception;
	
	public void ejecutarCargo() throws Exception;
	
	public void aplicarCargosEdocta(ConfigCargos config, DetalleCargos objDetalleCargos, String cargoAbono) throws Exception;
	
	public void iterarAplicarMovtoEdocta(List<DetalleCargos> listaDetalleCargos, String tipoMovto) throws Exception;
	
	public List<DetalleCargos> cancelarMovimientoAplicado(ConfigCargos config) throws Exception;
}
