package mx.com.afirme.midas2.service.pago;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.pago.ConfiguracionPagoDTO;
import mx.com.afirme.midas2.dto.pago.ReciboDTO;
import mx.com.afirme.midas2.dto.pago.RespuestaIbsDTO;

@Local
public interface IbsService {
	
	/**
	 * Consultar saldo y aplicar el cobro a una tarjeta de credito. 
	 * Debido al tipo de transaccion bancaria con PROSA esta es la mejor opción, ya
	 * que el solo hecho de consultar el saldo separá el dinero de la tarjeta en 
	 * caso de que los fondos sean suficientes.
	 * @param pago
	 * @param recibo
	 * @return
	 */
	public RespuestaIbsDTO consultarSaldoAplicarCargoTarjetaCredito(ConfiguracionPagoDTO pago, ReciboDTO recibo);
	
	/**
	 * Consultar si el saldo de una tarjeta de credito es suficiente para cubrir el valor
	 * del primer recibo. 
	 * CUIDADO!!! Debido al tipo de transaccion bancaria con PROSA
	 * la simple consulta hará el cobro correspondiente a la tarjeta de credito, pudiendo
	 * provocar un cargo con recibo 0
	 * @param pago
	 * @return
	 */
	public RespuestaIbsDTO consultarSaldoTarjetaCredito(ConfiguracionPagoDTO pago, ReciboDTO recibo);
	
	/**
	 * Confirmar cobro una  vez que se ha consultado el saldo de la tarjeta para un recibo.
	 * Este metodo se invoca posterior a la consulta solo como confirmacion.
	 * CUIDADO!!! Debido al tipo de transaccion bancaria con PROSA aun cuando no se realice
	 * la confirmacion, si el saldo es consultado, se hara el cobro sobre la tarjeta 
	 * @param pago
	 * @return
	 */
	public RespuestaIbsDTO confirmarCargoTarjetaCredito(ConfiguracionPagoDTO pago,  ReciboDTO recibo);
	
	/**
	 * @deprecated
	 * Intentar realizar la cancelacion de un cobro sobre una tarjeta de credito.
	 * CUIDADO!! Debido al tipo de transaccion bancaria con PROSA, casi ningùn banco 
	 * acepta la cancelación del cobro.
	 * @param pago
	 * @return
	 */
	public RespuestaIbsDTO cancelarCargoTarjetaCredito(ConfiguracionPagoDTO pago,  ReciboDTO recibo);
	
	/**
	 * Consultar si el sado de la cuenta afirme es lo suficiente para cubrir el 
	 * primer recibo
	 * @param pago
	 * @return
	 */
	public RespuestaIbsDTO consultarSaldoCuentaAfirme(ConfiguracionPagoDTO pago,  ReciboDTO recibo);
	
	/**
	 * Aplicar cargo sobre un recibo en cuenta afirme. Se debe mandar la referencia 
	 * correcta del recibo al que se le desea aplicar el cobro.
	 * @param pago
	 * @return
	 */
	public RespuestaIbsDTO aplicarCargoCuentaAfirme(ConfiguracionPagoDTO pago,  ReciboDTO recibo);
	
	/**
	 * Programar un cargo en cuenta afirme
	 * @param pago
	 * @return
	 */
	public RespuestaIbsDTO programarCargoCuentaAfirme(ConfiguracionPagoDTO pago,  ReciboDTO recibo);
		
}
