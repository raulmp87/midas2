package mx.com.afirme.midas2.service.pago;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.pago.ConfiguracionPagoDTO;
import mx.com.afirme.midas2.dto.pago.ReciboDTO;
import mx.com.afirme.midas2.dto.pago.RespuestaAmexDTO;

@Local
public interface AmexService {
	
	/**
	 * Consultar saldo y aplicar el cobro a una tarjeta de credito American Exmpress. 
	 * @param pago
	 * @param recibo
	 * @return RespuestaAmexDTO
	 */
	public RespuestaAmexDTO consultarSaldoAplicarCargoTarjetaCredito(ConfiguracionPagoDTO pago, ReciboDTO recibo);
	

	/**
	 * Aplicar un cargo a una tarjeta American Express. devuelve una objeto con los detalles de la transaccion
	 * @param cardNumber
	 * @param securityCode
	 * @param expirationDate
	 * @param reference
	 * @return RespuestaAmexDTO
	 */
	public RespuestaAmexDTO aplicarCargo(String cardNumber, String securityCode, String expirationDate, String reference,String amount, String description);	

}