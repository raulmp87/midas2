package mx.com.afirme.midas2.service;

import javax.ejb.Remote;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;


public interface TransporteService {
	
	@Deprecated
	public AgenteDTO obtenerAgente(Integer idToAgente);
	
	public Agente obtenerDatosAgente(Long id);
	
	public CoberturaDTO obtenerCobertura(Long idToCobertura);
	
	public Agente obtenerDatosAgenteClave(Long idAgente);

}
