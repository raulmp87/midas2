package mx.com.afirme.midas2.service.subordinados;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.subordinados.DerechoEndosoSeccion;
import mx.com.afirme.midas2.domain.subordinados.DerechoPolizaSeccion;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;


public interface SubordinadosSeccionService {

	public List<DerechoPolizaSeccion> obtenerDerechosPoliza(BigDecimal idSeccion);
	
	public List<DerechoEndosoSeccion> obtenerDerechosEndoso(BigDecimal idSeccion);
	
	public RespuestaGridRelacionDTO relacionarDerechosPoliza(String accion, BigDecimal idSeccion, 
			Integer numeroSecuencia, BigDecimal valor, Short claveDefault);
	
	public RespuestaGridRelacionDTO relacionarDerechosEndoso(String accion, BigDecimal idSeccion, 
			Integer numeroSecuencia, BigDecimal valor, Short claveDefault);
	
}
