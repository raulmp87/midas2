package mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto;

import java.math.BigDecimal;
import java.util.Date;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.dto.endoso.cotizacion.auto.CotizacionEndosoDTO;

import org.joda.time.DateTime;

@Local
public interface EndosoPolizaService {
	
	public BitemporalCotizacion getCotizacionEndosoCancelacionPoliza(BigDecimal polizaId,
			Date fechaInicioVigencia,String accionEndoso, Short motivoEndoso) ;
	
	public CotizacionEndosoDTO getCotizacionEndosoRehabilitacionPoliza(BigDecimal polizaId,
			Date fechaInicioVigencia, String accionEndoso, short motivoEndoso);
	
	public CotizacionEndosoDTO getCotizacionEndosoRehabilitacionPoliza(BigDecimal polizaId,
			Date fechaInicioVigencia, String accionEndoso, short motivoEndoso, short migrada);
	
	public BitemporalCotizacion prepareBitemporalEndoso( Long continuityId, 
			 DateTime validoEn);
	
	public void guardaCotizacionEndosoCancelacionPoliza(
			BitemporalCotizacion cotizacion,BigDecimal idTosolicitud, Date fechaInitVigencia, BigDecimal primaTotalIgualar);
	
	public void guardaCotizacionEndosoRehabilitacionPoliza(BigDecimal polizaId, Date fechaInicioVigencia, 
			Integer requiereCorrimientoVigencias, String accionEndoso);
	
	public void guardaCotizacionEndosoRehabilitacionPolizaAutomatica(BigDecimal polizaId, Date fechaInicioVigencia, 
			String accionEndoso, short claveMotivoEndoso, short migrada);
	
	public void guardaCotizacionEndosoCancelacionPolizaPTAutomatico(BitemporalCotizacion cotizacion, String[] continuitiesIds, BigDecimal idToSolicitud, 
			Date fechaInitVigencia);
	
	public void guardaCotizacionEndosoCancelacionPoliza(BitemporalCotizacion cotizacion, BigDecimal idToSolicitud, 
			Date fechaInitVigencia);	
}
