package mx.com.afirme.midas2.service.bitemporal.siniestros;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.ReporteSiniestroVehiculoDTO;

@Local
public interface SiniestroService {
	
	public List<ReporteSiniestroVehiculoDTO> buscarSiniestro( String numeroSerie );
	

}