package mx.com.afirme.midas2.service.bitemporal.endoso.inciso;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;

import org.joda.time.DateTime;

@Local
public interface IncisoSoporteService {

	public BitemporalInciso guardaIncisoBorrador (BitemporalInciso inciso, BitemporalAutoInciso autoInciso, Map<String, String> datosRiesgo,
			List<BitemporalCoberturaSeccion> coberturas, DateTime validoEn);
	
	public BitemporalInciso copiarInciso(Long incisoId, DateTime validoEn);
	
}
