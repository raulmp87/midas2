package mx.com.afirme.midas2.service.bitemporal;

import java.util.Collection;
import java.util.Map;

import javax.ejb.Local;

import org.joda.time.DateTime;

@Local
@SuppressWarnings("rawtypes")
public interface EntidadBitemporalService {

	/**
	 * Guarda un objeto EntidadBitemporal con un estatus 'En Proceso'
	 * @param <B>
	 * @param <C>
	 * @param bitemporalEntity Objeto EntidadBitemporal a guardar
	 * @return el objeto EntidadBitemporal guardado
	 */
	
	public <B extends EntidadBitemporal, C extends EntidadContinuity> B saveInProcess(B bitemporalEntity, DateTime validoEn);
	
	public <B extends EntidadBitemporal, C extends EntidadContinuity> B saveInProcess(B bitemporalEntity, DateTime validoEn,DateTime validoHasta) ;
	
	public <B extends EntidadBitemporal, C extends EntidadContinuity> B saveValid(B bitemporalEntity, DateTime validoEn);

	public <B extends EntidadBitemporal, C extends EntidadContinuity> Object saveInProcessAndGetKey(B bitemporalEntity, DateTime validoEn);
	
	public <B extends EntidadBitemporal, C extends EntidadContinuity> Object saveValidAndGetKey(B bitemporalEntity, DateTime validoEn);

	public <B extends EntidadBitemporal, C extends EntidadContinuity, K> void commit(Class<C> continuityEntityClass, K key);
	
	public <B extends EntidadBitemporal> void remove(B bitemporalEntity, DateTime validoEn);
	
	public <B extends EntidadBitemporal> void remove(B bitemporalEntity, DateTime validoEn, DateTime validoHasta);
	
	public <B extends EntidadBitemporal, C extends EntidadContinuity, K> B getByBusinessKey(Class<C> continuityEntityClass, String businessKeyName, K businessKey);
	
	public <B extends EntidadBitemporal, C extends EntidadContinuity, K> B getByBusinessKey(Class<C> continuityEntityClass, String businessKeyName, K businessKey, DateTime validoEn);
	
	public <B extends EntidadBitemporal, C extends EntidadContinuity, K> B getByKey(Class<C> continuityEntityClass, K key);
	
	public <B extends EntidadBitemporal, C extends EntidadContinuity, K> B getByKey(Class<C> continuityEntityClass, K key, DateTime validoEn);
	
	public <B extends EntidadBitemporal, C extends EntidadContinuity, K> B getNew(Class<B> bitemporalEntityClass, K parentKey);
		
	public <B extends EntidadBitemporal, C extends EntidadContinuity, K> Collection<B> findByParentKey(
			Class<C> continuityEntityClass,String parentKeyName, K parentKey);
	
	public <B extends EntidadBitemporal, C extends EntidadContinuity, K> Collection<B> findByParentKey(
			Class<C> continuityEntityClass,String parentKeyName, K parentKey, DateTime validoEn);

	public <B extends EntidadBitemporal, C extends EntidadContinuity, K> Collection<B> findByParentBusinessKey(
			Class<C> continuityEntityClass, String parentBusinessKeyName, K parentBusinessKey);
	
	public <B extends EntidadBitemporal, C extends EntidadContinuity, K> Collection<B> findByParentBusinessKey(
			Class<C> continuityEntityClass, String parentBusinessKeyName, K parentBusinessKey, DateTime validoEn);
	
	public <B extends EntidadBitemporal> Collection<B> listarFiltrado(Class<B> bitemporalEntityClass, Map<String,Object> params, boolean enProceso, 
			String... orderByAttributes);
	
	/**
	 * Obtiene los bitemporales cuya fecha record.to es igual a la fecha que se recibe como fecha de cancelacion
	 * @param <B>
	 * @param bitemporalEntityClass
	 * @param continuitiId
	 * @param validoEn
	 * @param fechaDeCancelacion
	 * @return
	 */
	public <B extends EntidadBitemporal> Collection<B> obtenerCancelados(Class<B> bitemporalEntityClass, Long continuitiId, DateTime validoEn, DateTime fechaDeCancelacion);
	
	public <B extends EntidadBitemporal> Collection<B> obtenerCancelados(Class<B> bitemporalEntityClass, 
			DateTime validoEn, DateTime fechaDeCancelacion, Map<String, Object> params);

	public <B extends EntidadBitemporal, C extends EntidadContinuity> Collection<B> obtenerCancelados(Collection<C> continuities, Class<B> bitemporalEntityClass, DateTime validoEn, DateTime fechaDeCancelacion);
	
	public <B extends EntidadBitemporal> Collection<B> listarFiltrado(Class<B> bitemporalEntityClass, Map<String,Object> params, 
			DateTime validoEn, boolean enProceso, String... orderByAttributes);
	
	public <B extends EntidadBitemporal> Collection<B> listarFiltrado(
			Class<B> bitemporalEntityClass, Map<String,Object> params, 
			DateTime validoEn, DateTime conocidoEn, boolean enProceso, String... orderByAttributes);
	
	public <B extends EntidadBitemporal, C extends EntidadContinuity, K> B getInProcessByBusinessKey(Class<C> continuityEntityClass, String businessKeyName, K businessKey, DateTime validoEn);
	
	public <B extends EntidadBitemporal, C extends EntidadContinuity, K> B getInProcessByKey(Class<C> continuityEntityClass, K key, DateTime validoEn);
	
	public <B extends EntidadBitemporal, C extends EntidadContinuity, K> Collection<B> findInProcessByParentKey(
			Class<C> continuityEntityClass,String parentKeyName, K parentKey, DateTime validoEn);

	public <B extends EntidadBitemporal, C extends EntidadContinuity, K> Collection<B> findInProcessByParentBusinessKey(
			Class<C> continuityEntityClass, String parentBusinessKeyName, K parentBusinessKey, DateTime validoEn);
	
	public <B extends EntidadBitemporal, K, S> B prepareEndorsementBitemporalEntity(Class<B> bitemporalEntityClass, K entidadContinuityId, 
			S parentContinuityId, DateTime validoEn);
	
	public <B extends EntidadBitemporal> B saveEndorsementBitemporalEntity(B entidadBitemporal, DateTime validoEn);
	
	
	public <B extends EntidadBitemporal, C extends EntidadContinuity> C attachToParentAndSet (B bitemporalEntity,B parentBitemporal, DateTime validoEn);
	
	public <B extends EntidadBitemporal, C extends EntidadContinuity> C attachToParentAndSet (B bitemporalEntity,B parentBitemporal, DateTime validoEn,
			DateTime validoHasta) ;
	
	public <B extends EntidadBitemporal> B set (B bitemporalEntity, DateTime validoEn, Boolean  twoPhaseMode);
	
	public <B extends EntidadBitemporal> B set (B bitemporalEntity, DateTime validoEn, DateTime validoHasta, Boolean  twoPhaseMode);
	
	
	public  void rollBackStatusContinuityCotizacion(Long continuityId , Integer tipo);
	
	public <B extends EntidadBitemporal,C extends EntidadContinuity, K> B getByKeyOld(Class<C> continuityEntityClass, K key, DateTime validoEn);
	
	public void rollBackContinuity(Long continuityId);
}
