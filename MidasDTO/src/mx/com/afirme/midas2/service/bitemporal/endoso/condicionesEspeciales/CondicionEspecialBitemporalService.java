package mx.com.afirme.midas2.service.bitemporal.endoso.condicionesEspeciales;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import org.joda.time.DateTime;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.BitemporalCondicionEspCot;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.BitemporalCondicionEspInc;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.dto.condicionespecial.CondicionEspecialBitemporalDTO;

@Local
public interface CondicionEspecialBitemporalService {

	/**
	 * Método que crea los bitemporales de BitemporalCondicionEspInc que debe contener
	 * el BitemporalInciso que estén marcados como obligatorios en el Negocio.
	 * Utilizar el método obtenerCondicionesNegocio de NegocioCondicionEspecialService,
	 * enviando el id del negocio asociado al Bitemporal de la Cotización
	 * perteneciente al inciso y nivel INCISO
	 * 
	 * @param inciso
	 */
	public void asociarCondicionesDefault (BitemporalInciso inciso);

	/**
	 * Eliminar un registro bitemporal de BitemporalCondicionEspecialCot o
	 * BitemporalCondicionEspecialInc según esté configurado la condición
	 */
	public void eliminarCondicion(Long idCondicionEspecial, Date validoEn);

	/**
	 * Eliminar un registro bitemporal de  BitemporalCondicionEspecialInc. Utilizar el
	 * método remove de EntidadBitemporalService
	 * 
	 * @param condicionInciso
	 * @param validoEn
	 */
	public void eliminarCondicionInciso(BitemporalCondicionEspInc condicionInciso, Date validoEn);
	
	
	/**
	 * Elimina un registro bitemporal de BitemporalCondicionEspCot
	 * 
	 * @param condicionBitemporalDTO
	 * @param validoEn
	 */
	public void eliminarCondicionCotizacion(CondicionEspecialBitemporalDTO condicionBitemporalDTO, Date validoEn);

	/**
	 * Guardar una condición a nivel cotización. Se generá en la entidad
	 * <i>BitemporalCondicionEspCot</i>
	 * 
	 * @param cotizacionContinuityId
	 * @param condicion
	 * @param validoEn
	 */
	public void guardarCondicionCotizacion(Long cotizacionContinuityId, CondicionEspecialBitemporalDTO condicionEspecialBitemporalDTO, Date validoEn);

	/**
	 * 
	 * @param bitemporal
	 * @param cotizacionContinuityId
	 * @param incisos
	 */
	public void guardarCondicionesEspeciales(CondicionEspecialBitemporalDTO bitemporal, Long cotizacionContinuityId, String incisos);

	public void guardarCondicionesObligatoriasInciso();

	/**
	 * Guarda una condición a nivel inciso. Se generá en la entidad
	 * <i>BitemporalCondicionEspInc</i>
	 * 
	 * @param cotizacionContinuityId
	 * @param numeroInciso
	 * @param condicion
	 * @param validoEn
	 */
	public void guardarCondicionInciso(Long cotizacionContinuityId, Long numeroInciso, CondicionEspecial condicion, Date validoEn);

	/**
	 * Guarda una condición a nivel inciso. Se generá en la entidad
	 * <i>BitemporalCondicionEspInc </i>a través del método saveInProcess de
	 * EntidadBitemporalService
	 * 
	 * @param incidoContinuityId
	 * @param condicion
	 * @param validoEn
	 */
	public void guardarCondicionInciso(Long incidoContinuityId, CondicionEspecial condicion, Date validoEn);

	/**
	 * Obtiene los bitemporales de de las condiciones especiales a nivel cotización
	 * dado el Id de continuidad de la cotización asociada
	 * 
	 * @param cotizacionContinuityId
	 */
	public List<BitemporalCondicionEspCot> obtenerCondicionesCotContinuity(Long cotizacionContinuityId);

	/**
	 * Obtener las condiciones que no han sido asociadas a una cotización
	 * 
	 * @param cotizacionContinuityId
	 * @param validoEn
	 * @param condicionesDisponibles
	 */
	public List<CondicionEspecialBitemporalDTO> obtenerCondicionesDisponiblesCotizacion(Long cotizacionContinuityId, Date validoEn);
	
	
	/**
	 * Obtener las Condiciones que ya no estan asociadas a  la Cotizacion
	 * @param cotizacionContinuityId
	 * @param validoEn
	 * @return
	 */
	public List<CondicionEspecialBitemporalDTO> obtenerCondicionesEliminadasCotizacion(Long cotizacionContinuityId, Date validoEn);
	
	/**
	 * Obtener las condiciones que tiene Asociadas una Cotización.
	 * @param cotizacionContinuityId
	 * @param validoEn
	 * @return
	 */
	public List<CondicionEspecialBitemporalDTO> obtenerCondicionesAsociadasCotizacion(Long cotizacionContinuityId, Date validoEn);
	
	/**
	 * Obtener las condiciones que tiene Asociadas una Cotización.
	 * @param cotizacionContinuityId
	 * @param validoEn
	 * @return
	 */
	public List<CondicionEspecialBitemporalDTO> obtenerCondicionesAsociadasCotizacionInProcess(Long cotizacionContinuityId, Date validoEn);
	
	
	/**
	 * Obtener las condiciones Asociadas a una Cotizacion, que tegan el nombre de la Condicion 
	 * @param nombreCondicion
	 * @param cotizacionContinuityId
	 * @param validoEn
	 * @return
	 */
	public List<CondicionEspecialBitemporalDTO> obtenerCondicionesCotizacionNombreCodigo(String nombreCondicion, Long cotizacionContinuityId, Date validoEn) ;

	/**
	 * Obtener las condiciones que aún no han sido asociadas a un inciso
	 * 
	 * @param incisoContinuityId
	 * @param validoEn
	 * @param condicionesAsociadas
	 */
	public List<CondicionEspecialBitemporalDTO> obtenerCondicionesDisponiblesInciso(List<CondicionEspecialBitemporalDTO> lstDisponibles, Long incisoContinuityId, Date validoEn);

	/**
	 * 
	 * @param incisoContinuityId
	 * @param validoEn
	 */
	public List<CondicionEspecialBitemporalDTO> obtenerCondicionesIncisoAsociadas(Long incisoContinuityId, Date validoEn);

	/**
	 * 
	 * @param incisoContinuityId
	 * @param validoEn
	 */
	public List<CondicionEspecialBitemporalDTO> obtenerCondicionesIncisoAsociadasInProcess(Long incisoContinuityId, Date validoEn);

	/**
	 * Obtiene los bitemporales de de las condiciones especiales a nivel inciso dado
	 * el Id de continuidad de la cotización asociada
	 * 
	 * @param cotizacionContinuityId
	 */
	public List<BitemporalCondicionEspInc> obtenerCondicionesIncisoContinuity(Long cotizacionContinuityId);

	/**
	 * Obtener las condiciones asociadas a todos los incisos de una cotizacíón
	 * 
	 * @param cotizacionContinuityId
	 * @param validoEn
	 */
	public List<BitemporalCondicionEspInc> obtenerCondicionesIncisoCotizacion(Long cotizacionContinuityId, Date validoEn);

	/**
	 * Obtener las condiciones asociadas a nivel cotización
	 * 
	 * @param cotizacionContinuityId
	 * @param validoEn
	 */
	public List<BitemporalCondicionEspCot> obtenerCondicionesNivelCotizacion(Long cotizacionContinuityId, Date validoEn);

	/**
	 * Obtener las condiciones asociadas a nivel cotización.
	 * Utilizar el método findByParentKey de EntidadBitemporalService
	 * 
	 * @param cotizacionContinuityId
	 */
	public List<BitemporalCondicionEspCot> obtenerCondicionesNivelCotizacion(Long cotizacionContinuityId);
	
	public void guardarCondicionInciso(Long incisoContinuityId, CondicionEspecialBitemporalDTO condicionBitemporalDTO, Date validoEn);
	
	public void eliminarCondicionInciso(CondicionEspecialBitemporalDTO condicionBitemporalDTO, Date validoEn);
	
	/**
	 * Obtiene
	 * @param cotizacionContinuityId
	 * @return
	 */
	public Collection<BitemporalCondicionEspInc> obtenerCondicionesIncisoContinuity(Long cotizacionContinuityId,DateTime validFromDT, DateTime recordFromDT);
	
	public List<CondicionEspecialBitemporalDTO> obtenerCondicionesIncisoNombreCodigo(String nombreCondicion, Long incisoContinuityId, Date validoEn);
		
	public void asociarCondicionesInciso(Long incisoContinuityId, Date validoEn, String nombreCondicion);
	
	public void eliminarCondicionesInciso(Long incisoContinuityId, Date validoEn);
	
	/**
	 * Obtiene el IdIncisoContinuity , de acuerdo al inciso, la CotizacionContinuity y la fecha en la que es valido
	 * @param incisoABuscar
	 * @param incisoContinuityId
	 * @param validoEn
	 * @return
	 */
	public Long getIdIncisoContinuity(int incisoABuscar , Long cotizacionContinuityId , Date validoEn);
	
	
	/**
	 * Obtiene la lista de CondicionEspecialBitemporalDTO , sin Repetir, de las Condiciones asociadas a la lista de IncisoContinuityId proporcionada.
	 * @param listIncisoContinuityId
	 * @param validoEn
	 * @return
	 */
	public List<CondicionEspecialBitemporalDTO> obtenerCondicionesIncisoAsociadas( List<Long> listIncisoContinuityId, Date validoEn );
	
	public void generarCondicionesBaseInciso(BitemporalCotizacion cotizacion, DateTime validoEn);
	
	/**
	 * Elimina la relacion de una condicion, para la baja de endoso
	 * @param condicionBitemporalDTO
	 * @param validoEn
	 */
	public void eliminarCondicionIncisoBajaEndoso(CondicionEspecialBitemporalDTO condicionBitemporalDTO, Date validoEn) ;
	
	
	/**
	 * Obtiene la Lis de CondicionEspecialBitemporalDTO con solo el ID De la condicionEspCotContinuity ,en proceso, para luego ser eliminadas
	 * @param cotizacionContinuityId
	 * @param validoEn
	 * @return
	 */
	public List<CondicionEspecialBitemporalDTO> obtenerCondicionEspCotInProcess(Long cotizacionContinuityId, Date validoEn);
	
	
	/**
	 * Obtiene la Lis de CondicionEspecialBitemporalDTO con solo el ID De la condicionIncCotContinuity ,en proceso, para luego ser eliminadas
	 * @param cotizacionContinuityId
	 * @param validoEn
	 * @return
	 */
	public List<CondicionEspecialBitemporalDTO> obtenerCondicionEspIncInProcess(Long incisoContinuityId, Date validoEn);
	
	
	/**
	 * Obtener las Condiciones que ya no estan asociadas a  la Cotizacion
	 * 
	 * @param incisoContinuityId
	 * @param validoEn
	 * @return
	 */
	public List<CondicionEspecialBitemporalDTO> obtenerCondicionesEliminadasInciso(Long incisoContinuityId, Date validoEn);
	
	
	/**
	 * Asociada las condiciones que ya estaban en estatus TO_BE_ENDED a nivel poliza
	 * @param condicionBitemporalDTO
	 * @param validoEn
	 */
	public void asociarNuevamenteCondicionCotizacion(CondicionEspecialBitemporalDTO condicionBitemporalDTO, Date validoEn) ;
	
	
	/**
	 * Asociada las condiciones que ya estaban en estatus TO_BE_ENDED a nivel inciso
	 * @param condicionBitemporalDTO
	 * @param validoEn
	 */
	public void asociarNuevamenteCondicionIncisoBajaEndoso(CondicionEspecialBitemporalDTO condicionBitemporalDTO, Date validoEn);
	
	
	/**
	 * Obtener condiciones por poliza
	 * @param bitempCotizacion
	 * @param validOn
	 * @param recordFrom
	 * @return
	 * @throws IOException
	 */
	public List<CondicionEspecial> obtenerCondicionesEspecialesPoliza(
			BitemporalCotizacion bitempCotizacion, 
			DateTime validOn,
			DateTime recordFrom)
			throws IOException;

	/**
	 * Retorna el RESUMEN de las condiciones especiales de todos los incisos SIN REPETIR
	 * @param bitempCotizacion
	 * @param validOnDT
	 * @param recordFromDT
	 * @return
	 * @throws IOException
	 */
	public List<CondicionEspecial> obtenerCondicionesEspecialesIncisos(
				BitemporalCotizacion bitempCotizacion, DateTime validOnDT,
				DateTime recordFromDT) throws IOException;

	/**
	 * Retorna el listado de condiciones en base a un inciso.
	 * @param bitemporalCotizacion
	 * @param numeroInc
	 * @param validOnDT
	 * @param recordFromDT
	 * @return
	 * @throws IOException
	 */
	public List<CondicionEspecial> obtenerCondicionesEspecialesInciso(
			BitemporalCotizacion bitemporalCotizacion,
			String numeroInc, 
			DateTime validOnDT,
			DateTime recordFromDT) throws IOException;
	
	
	/**
	 * Elimina la condicion para el Endoso de Inclusion
	 * @param condicionBitemporalDTO
	 * @param validoEn
	 */
	public void eliminarCondicionIncisoInclusionEndoso(CondicionEspecialBitemporalDTO condicionBitemporalDTO, Date validoEn);
	

}
