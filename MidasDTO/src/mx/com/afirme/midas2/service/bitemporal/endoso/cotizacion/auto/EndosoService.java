package mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.texto.BitemporalTexAdicionalCot;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.domain.tarea.EmisionPendiente.TipoEmision;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService.NivelEvaluacion;

import org.joda.time.DateTime;

@Local
public interface EndosoService {

	
	public BitemporalCotizacion getCotizacionEndosoCFP(BigDecimal polizaId,
			Date fechaInicioVigencia,String accionEndoso);
	
	/**
	 * Obtiene la BitemporalCotizacion para el tipo de ensoso Extensión Vigencia.
	 * @param polizaId
	 * @param accionEndoso
	 * @return
	 */
	public BitemporalCotizacion getCotizacionEndosoEV(BigDecimal polizaId,
			String accionEndoso);
	/**
	 * Método dummy.
	 * @param numeroPoliza
	 * @param fechaInicioVigencia
	 * @return
	 */
	public BitemporalCotizacion getCotizacionEndosoCEAPDummy(BigDecimal numeroPoliza,
			Date fechaInicioVigencia);
	
	/**
	 * Realiza la cancelación de endoso a petición.
	 * @param numeroPoliza
	 * @param fechaInicioVigencia
	 * @param accionEndoso
	 * @return
	 */
	public BitemporalCotizacion getCotizacionEndosoCEAP(BigDecimal numeroPoliza,
			Date fechaInicioVigencia,String accionEndoso);
	
	public BitemporalCotizacion getCotizacionEndosoMovimientos(
			BigDecimal numeroPoliza, Date fechaInicioVigencia,String accionEndoso);

	public BitemporalCotizacion getCotizacionEndosoMovimientos(
			BigDecimal numeroPoliza,String accionEndoso);
	
	public BitemporalCotizacion getCotizacionEndosoIT(BigDecimal polizaId, String accionEndoso);
	
	public BitemporalCotizacion getCotizacionEndosoIT(BigDecimal polizaId, Date fechaInicioVigencia, String accionEndoso);
	
	public BitemporalCotizacion getCotizacionEndosoIA(BigDecimal polizaId, Date fechaInicioVigencia, String accionEndoso);
	
	public BitemporalCotizacion getCotizacionEndosoBajaInciso(BigDecimal polizaId,Date fechaInicioVigencia, String accionEndoso, Short motivoEndoso);
	
	public BitemporalCotizacion getCotizacionEndosoBajaIncisoTrxNew(BigDecimal polizaId,Date fechaInicioVigencia,String accionEndoso, Short motivoEndoso);
	
	public BitemporalCotizacion getCotizacionEndosoBajaInciso(BigDecimal polizaId,String accionEndoso, Short motivoEndoso);
	
    public BitemporalCotizacion getCotizacionEndosoAltaInciso(BigDecimal polizaId,Date fechaInicioVigencia, String accionEndoso);
    
    public BitemporalCotizacion getCotizacionEndosoAltaIncisoTrxNew(BigDecimal polizaId,Date fechaInicioVigencia, String accionEndoso);
	
	public BitemporalCotizacion getCotizacionEndosoAltaInciso(BigDecimal polizaId,String accionEndoso);
	
	public BitemporalCotizacion getCotizacionEndosoCambioDatos(BigDecimal polizaId,Date fechaInicioVigencia, String accionEndoso);
	
	public BitemporalCotizacion getCotizacionEndosoCambioDatos(BigDecimal polizaId, String accionEndoso);
	
	public BitemporalCotizacion getCotizacionEndosoBajaIncisoPerdidaTotal(BigDecimal polizaId, Date fechaInicioVigencia,String accionEndoso);
	
	public BitemporalCotizacion getCotizacionEndosoBajaIncisoPerdidaTotal(BigDecimal polizaId, Date fechaInicioVigencia,String accionEndoso, 
			short claveMotivoEndoso, short migrada);
	
	public void getCotizacionEndosoCancelacionEndoso(BigDecimal polizaId);

	/**
	 * Obtiene la BitemporalCotizacion para el tipo de ensoso Cambio de Agente
	 * 
	 * 
	 * @param polizaId
	 * @param accionEndoso
	 * @return
	 */
	public BitemporalCotizacion getCotizacionEndosoCambioAgente(BigDecimal polizaId, Date fechaInicioVigencia,
			String accionEndoso);
	
    public BitemporalCotizacion getCotizacionEndosoRehabilitacionInciso(BigDecimal polizaId,Date fechaInicioVigencia, String accionEndoso);
    
    public BitemporalCotizacion getCotizacionEndosoRehabilitacionInciso(BigDecimal polizaId,Date fechaInicioVigencia, String accionEndoso, short motivoEndoso);
	
	public BitemporalCotizacion getCotizacionEndosoRehabilitacionInciso(BigDecimal polizaId, String accionEndoso);
	
	public void guardaCotizacionEndoso(BitemporalCotizacion cotizacion);
	
	
	/**
	 * Guarda la BitemporalCotizacion para el tipo Endoso Extensión Vigencia.
	 * @param cotizacion
	 * @param validoHasta
	 */
	public void guardaCotizacionEndosoEV(BitemporalCotizacion cotizacion, Date validoHasta);
	/**
	 * Genera todo el arbol de la bitemporalidad de Cotización a la fecha validoHasta.
	 * En dado caso de que la fecha validoHasta <= a la fecha FinVigencia manda una excepción.
	 * NOTA: solo se realiza la afectación a base de datos.
	 * NegocioEJBExeption 
	 * @param cotizacion
	 * @param validoHasta
	 */
	public void generaCotizacionEndosoExtensionVigencia(BitemporalCotizacion cotizacion,
			  Date validoHasta);
	
	public void guardaCotizacionEndosoMovimientos(BitemporalCotizacion cotizacion);

	public void guardaCotizacionInclusionTexto(String textos, BitemporalCotizacion cotizacion, DateTime validoEn);
	
	public void guardaCotizacionInclusionAnexo(BitemporalCotizacion cotizacion, DateTime validoEn);
	
	public void guardaCotizacionEndosoBajaInciso(String[] continuitiesIds, BitemporalCotizacion cotizacion);
	
	public void guardaCotizacionEndosoCambioAgente(BitemporalCotizacion cotizacion);
	
	public void guardaCotizacionEndosoRehabilitacionInciso(String[] continuitiesIds, List<BitemporalInciso> listaIncisosCancelados, BitemporalCotizacion cotizacion);
	
	public void guardaCotizacionEndosoCambioDatos(BitemporalCotizacion cotizacion);

	@SuppressWarnings("rawtypes")
	public <B extends EntidadBitemporal> void guardarEntidadBitemporalEndoso( B entidadBitemporal, DateTime validoEn);
	//public void guardarEntidadBitemporalEndoso(@SuppressWarnings("rawtypes") EntidadBitemporal entidadBitemporal, DateTime validoEn);

	/**
	 * Persiste un objeto ControlEndosoCot
	 * @param cotizacion
	 * @param claveEstatus,  constantes definidas en CotizacionDTO
	 * @param fechaInicioVigencia
	 */
	public  ControlEndosoCot  guardaControlEndosoCotizacion(BitemporalCotizacion cotizacion, Short claveEstatus,Date fechaInicioVigencia);
	
	public String getNumeroEndoso(BigDecimal polizaId, String format);

	public int getNumeroEndoso(BigDecimal polizaId);

	public BitemporalCotizacion prepareCotizacionEndoso(Long idCotizacion, DateTime validoEn);
	
	public BitemporalCotizacion prepareCotizacionEndosoEV(BigDecimal polizaId,
			String accionEndoso);
	
	@SuppressWarnings("rawtypes")
	public <B extends EntidadBitemporal> B  prepareEntidadBitemporalEndoso(
						Class<B> parentBitemporalEntityClass,
						Long entidadContinuityId, Long parentContinutyId, DateTime validoEn);
	
	//public EntidadBitemporal prepareEntidadBitemporalEndoso(Class<EntidadBitemporal> bitemporalEntityClass, Long entidadContinuityId, 
			//Long parentContinuityId, DateTime validoEn);

	/**
	 * Crea la BitemporalCotizacion
	 * @param polizaId
	 * @param fechaInicioVigencia
	 * @param tipoEndoso
	 * @param accionEndoso
	 * @return
	 */
	public BitemporalCotizacion getInitCotizacionEndoso(BigDecimal polizaId,
			Date fechaInicioVigencia, short tipoEndoso,short motivoEndoso,String accionEndoso);
	
	public BitemporalCotizacion getInitCotizacionEndoso(BigDecimal polizaId,
			Date fechaInicioVigencia, short tipoEndoso,short motivoEndoso,String accionEndoso, short migrada);	
	
	public void actualizaControlEndosoCot(BigDecimal cotizacionId, Long claveEstatusCot, Date validFrom, Date recordForm);
	
	public List<BitemporalTexAdicionalCot> prepareEntidadBitemporalEndosoTexto(String textos, DateTime validoEn);
	
	public Collection<BitemporalTexAdicionalCot> getLstBiTexAdicionalCot(Long cotizacionContinuityId, DateTime validoEn);

	
	public Boolean isBiTextAdicionalCotAutorizado(Long cotizacionContinuityId, DateTime validoEn);
	
	public void deleteCotizacionEndoso(Long cotizacionContinuityId);
	
	public TerminarCotizacionDTO terminarCotizacionEndoso(BitemporalCotizacion cotizacion, DateTime validoEn);
	
	public boolean actualizarDatosContratanteCotizacion(BitemporalCotizacion cotizacion, ClienteGenericoDTO cliente, String nombreUsuario, Date validoEn);
	
	public ClienteGenericoDTO getClienteGenerico(BigDecimal idCliente);
	/**
	 * Lista los endosos por la descripcion
	 * @param propertyName
	 * @param value
	 * @param ascendingOrder
	 * @return
	 */
	public List<EndosoDTO> findByPropertyWithDescriptions(String propertyName,
			final Object value, boolean ascendingOrder);
	
	
	/**
	 * Lista los endosos que son cancelables
	 * @param cotizacionId
	 * @param recordFrom
	 * @return
	 */
	public List<EndosoDTO> findForCancel(Long cotizacionId);
	
	public void guardaCotizacionCancelacionEndoso(BitemporalCotizacion cotizacion, DateTime validoEn, BigDecimal idToPoliza,
													Short numeroEndoso, String accionEndoso, Short motivoEndoso);
	
	
	/**
	 * Obtiene el BitemporalCotizacion en el rango de validez definido y lo Guarda en Proceso.
	 * @param cotizacion
	 * @param validoDesde
	 * @param validoHasta
	 * @return
	 */
	public void setBitemporalCancelacionEndoso(BitemporalCotizacion cotizacion, Date validoDesde,
			  Date validoHasta);
	
	public List<EndosoDTO> findForRehab(Long cotizacionId);
	
	public void guardaCotizacionRehabilitacionEndoso(BitemporalCotizacion cotizacion, BigDecimal idToPoliza, DateTime validoEn, 
																						Short numeroEndoso, String accionEndoso, short motivoEndoso);

	public BitemporalCotizacion getCotizacionEndosoRehabilitacionEndoso(BigDecimal polizaId, Date fechaInicioVigencia, String accionEndoso);
	
	public BitemporalCotizacion prepareCotizacionEndosoCancelacion(PolizaDTO poliza, Short tipoEndoso, 
																		DateTime validoEn, String accionEndoso, Long numeroEndoso, short motivoEndoso);
	
	public BitemporalCotizacion prepareCotizacionEndosoCancelacion(PolizaDTO poliza, Short tipoEndoso, 
			DateTime validoEn, String accionEndoso, Long numeroEndoso, short motivoEndoso, short migrada);
	
	public BitemporalCotizacion obtenerCotizacionCancelacion(PolizaDTO poliza, DateTime validFrom);
	
	public TerminarCotizacionDTO validateTerminarCotizacion(BitemporalCotizacion cotizacion, DateTime validoEn, NivelEvaluacion nivelEvaluacion);
	
	public TerminarCotizacionDTO validarEmisionCotizacion(BitemporalCotizacion cotizacion, DateTime validoEn);
	
	public TerminarCotizacionDTO validarEmisionCotizacionCondicionesGenerales(BitemporalCotizacion cotizacion, DateTime validoEn);
	
	public ControlEndosoCot obtenerControlEndosoCotCotizacion(BigDecimal idToCotizacion);
	
    public Long generarSolicitudAutorizacion(BitemporalCotizacion cotizacion, Long usuarioSolicitante, Long usuarioAutorizador);
	
	public void cancelarSolicitudesAutorizacion(BitemporalCotizacion cotizacion);
		
	public List<EndosoIDTO> validaPagosRealizados (BigDecimal polizaId, Long numeroEndoso, String[] incisoContinuityIds, Date fechaInicioVigencia, 
			TipoEmision tipoEmision);
	
	/**
	 * Valida en Seycos la situacion de los recibos del endoso, poliza o inciso(s) que se desea cancelar a peticion, validando 
	 * si se puede continuar con la cancelación o no, y la fecha limite en la que se debe cancelar en caso de que aplique.
	 * Si la fecha de inicio de vigencia segun la situacion de los recibos no coincide con la fecha de inicio de vigencia
	 * ingresada por el usuario, se manda una excepcion sugiriendo la fecha valida de inicio de vigencia.
	 * @param polizaId Id de la poliza a validar
	 * @param numeroEndoso Numero de endoso a validar (en caso de que aplique)
	 * @param incisoContinuityIds Arreglo con los ids de las continuidades de incisos a validar (en caso de que aplique)
	 * @param fechaInicioVigencia Fecha de inicio de vigencia del endoso a peticion del usuario
	 * @param tipoEmision Tipo de Emision con la cual se va a validar
	 * @param motivoEndoso Motivo del Endoso (A Peticion o Automatica)
	 * @return Lista con los incisos validados de manera individual
	 */
	public List<EndosoIDTO> validaPagosRealizados (BigDecimal polizaId, Long numeroEndoso, String[] incisoContinuityIds, Date fechaInicioVigencia, 
			TipoEmision tipoEmision, Short motivoEndoso);
	
	
	public Object validaPagosRealizadosFront(BigDecimal polizaId, Long numeroEndoso, String[] incisoContinuityIds, Date fechaInicioVigencia, 
			TipoEmision tipoEmision, Short motivoEndoso);
	
	public void guardarCobranzaCotizacion(Long cotizacionContinuityId, Long idToPoliza, DateTime validoEn, Short claveAfectacionPrimas, Short tipoEndoso);
	
	public List<CatalogoValorFijoDTO> listadoMotivoEndoso(Long tipoEndoso, Long polizaId);
	
	public Short convierteMotivoEndosoSeycos(Short motivoEndoso) ;
	
	public void regresarAProcesoCotizacionEndoso(BitemporalCotizacion cotizacion, DateTime validoEn);
	
	public void validaCotizacionConPrima(BitemporalCotizacion cotizacion, String[] continuitiesIds);
	
	public EndosoDTO getUltimoEndoso(Long idToPoliza, Date validFrom);
	
	public List<EndosoDTO> findForAdjust(Long cotizacionId);
	
	public ControlEndosoCot getControlEndosoCotEAP(BigDecimal cotizacionId, Short numeroEndoso);
	 
	public void guardaCotizacionAjustePrima(BigDecimal cotizacionId, BigDecimal idToPoliza, ControlEndosoCot controlEndosoCotEAP, Short numeroEndoso, 
			Integer tipoFormaPago);
	
	public void deleteBitemporalTexAdicionalCot(BitemporalTexAdicionalCot cotizacionContinuityId);
	
	public void validaPrimaNetaCeroEndosoCotizacion(Short claveTipoEndoso, Long idToPoliza, Date validoEn);
	
	public void validaPrimaNetaCeroEndoso(BitemporalInciso bitemporalInciso, Date validoEn, Short claveTipoEndoso);
	
	public BitemporalCotizacion encuentraBitemporalCotizacion(BigDecimal cotizacionId, DateTime dateTimeFechaIniVigencia, 
			short tipoEndoso, String accionEndoso);
	
	public boolean validarRestriccionEndososPolizaFlotillaMigrada(BigDecimal idTopoliza);
	
	public BitemporalCotizacion getCotizacionEndosoExclusionTexto(BigDecimal polizaId, Date fechaInicioVigencia, String accionEndoso);
	
	public void guardaCotizacionEndosoExclusionTexto(String[] continuitiesIds, BitemporalCotizacion cotizacion);
	
	public Collection<BitemporalTexAdicionalCot> getLstBiTexAdicionalCotNotInProcess(Long cotizacionContinuityId, DateTime validoEn);
	
	public Collection<BitemporalTexAdicionalCot> getLstBiTexAdicionalCotProcess(Long cotizacionContinuityId, DateTime validoEn);
	
	public EndosoDTO getUltimoEndosoByValidFrom(Long idToPoliza, Date validFrom);
	
	/**
	 * Obtiene el Bitemporal de la cotización para el endoso indicado.  Debe utilizar
	 * el método initCotizacionEndoso de EndosoService, basarse en métodos similares
	 * para otros endosos.
	 * 
	 * @param polizaId
	 * @param fechaInicioVigencia
	 * @param accionEndoso
	 */
	public BitemporalCotizacion getCotizacionEndosoBajaCondiciones(BigDecimal polizaId, Date fechaInicioVigencia, String accionEndoso);

	/**
	 * Obtiene el Bitemporal de la cotización para el endoso indicado. Debe utilizar
	 * el método initCotizacionEndoso de EndosoService, basarse en métodos similares
	 * para otros endosos.
	 * 
	 * @param polizaId
	 * @param fechaInicioVigencia
	 * @param accionEndoso
	 */
	public BitemporalCotizacion getCotizacionEndosoInclusionCondiciones(BigDecimal polizaId, Date fechaInicioVigencia, String accionEndoso);

	/**
	 * Método que guarda la cotización del Endoso de Baja de Condiciones Especiales.
	 * Debe de guardar el ControlEndosoCot correspondiente y guardar en PROCESO el
	 * bitemporal de la Cotizacion. Tomar como referencia el método
	 * guardaCotizacionInclusionAnexo de EndosoService
	 * 
	 * @param cotizacion
	 * @param validoEn
	 */
	public void guardaCotizacionBajaCondiciones (BitemporalCotizacion cotizacion, DateTime validoEn);

	/**
	 * Método que guarda la cotización del Endoso de Inclusión de Condiciones
	 * Especiales.
	 * Debe de guardar el ControlEndosoCot correspondiente y guardar en PROCESO el
	 * bitemporal de la Cotizacion. Tomar como referencia el método
	 * guardaCotizacionInclusionAnexo de EndosoService
	 * 
	 * @param cotizacion}
	 * @param validoEn
	 */
	public void guardaCotizacionInclusionCondiciones (BitemporalCotizacion cotizacion, DateTime validoEn);
	
	public void validaBajaIncisos(BitemporalCotizacion cotizacion, String[] continuitiesIds);
	  
	public Short getCotizacionEndosoPerdidaTotal(BigDecimal idToPoliza, 
			Date validoEn,  Short tipoEndoso);
	
	public BitemporalCotizacion prepareCotizacionEndosoPerdidaTotal(BigDecimal idToPoliza,
			Date fechaInicioVigencia, Short tipoEndoso,  String tipoIndemnizacion, String accionEndoso, IncisoContinuity incisoContinuity);	
	
	public void getCotizacionEndosoDesagrupacionRecibos(BigDecimal idToPoliza,Date validoEn, Short tipoEndoso);
	
	public BitemporalCotizacion prepareCotizacionEndosoDesagrupacionRecibos(BigDecimal polizaId,Date fechaInicioVigencia, String accionEndoso);
	
	public void guardaCotizacionEndosoDesagrupacionRecibos(PolizaDTO poliza, BitemporalCotizacion cotizacion, IncisoContinuity incisoContinuity, 
			BitemporalAutoInciso bitemporalAutoInciso);
	
	public void guardaCotizacionEndosoDesagrupacionRecibosAutomatica(PolizaDTO poliza, BitemporalCotizacion cotizacion, IncisoContinuity incisoContinuity, 
			BitemporalAutoInciso bitemporalAutoInciso);
	
	public List<EndosoIDTO> validaEndosoPT (BigDecimal idToPoliza, Integer numeroInciso, Date fechaInicioVigencia, Short motivoEndoso);
	
	
	/**
	 * Consulta sobre los recibos en SEYCOS el monto total de las primas pendientes de pago para la poliza-inciso proporcionados
	 * 
	 * @param idToPoliza
	 * @param numeroInciso
	 * @param fechaValidacion  Si es diferente de null entonces valida las primas pendientes de pago hasta la fecha proporcionada
	 * 							en caso contrario valida las primas pendientes de pago por toda la vigencia del inciso.
	 */
	public BigDecimal validarPrimasPendientesPagoInciso(BigDecimal idToPoliza,
			Integer numeroInciso, Date fechaValidacion);
	
	public void actualizaAgrupadoresEndoso(BigDecimal idToPoliza, BigDecimal numeroEndoso);


	
	public boolean validarEndososPorMigrarSeycos(BigDecimal idTopoliza);
	
	public void bloquearMigracionSeycos(String identificador); 
	
	public void igualarEndosoMidas(BigDecimal pnum_poliza, BigDecimal pnum_renov, BigDecimal pnum_endoso, 
			BigDecimal varPrimaNeta, BigDecimal varBonifComis, BigDecimal varRecargos, BigDecimal varBonifComisRPF,
			BigDecimal varDerechos, BigDecimal varIva, BigDecimal varPrimaTotal, BigDecimal varComisPN,
			BigDecimal varComisRPF);
	
	public void migrarEndosoMidas(String identificador);
	
	public void regeneraPrimaRecibo(BigDecimal pId_Recibo,  
			BigDecimal varPrimaNeta, BigDecimal varBonifComis, BigDecimal varRecargos, BigDecimal varBonifComisRPF,
			BigDecimal varDerechos, BigDecimal varIva, BigDecimal varPrimaTotal, BigDecimal varComisPN,
			BigDecimal varComisRPF);
	
	public void regeneraRecibo(BigDecimal pId_Recibo);
	
	public void setLlaveFiscal(BigDecimal pIdToPoliza, BigDecimal pNumeroEndoso, String pllaveFiscal);
	
	public void reprocesaEndosoSeycos(BigDecimal pId_Cotizacion, BigDecimal pId_Solicitud_Canc);
	
	public boolean validaRehabilitacionCAP(BigDecimal idToPoliza);
	
	public void migrarEndosoCFP(String identificador);
	
	public void generaReciboEndoso(BigDecimal pIdCotizacion, BigDecimal pIdSolicitud, BigDecimal pIdVersionPol, BigDecimal pIdVersionPolCanc,
			String pCveTEndoso, String pCveLTFolioCE);

	public void igualarPrimaMovimientosEndoso(ControlEndosoCot controlEndosoCot);
	
	public void ajustaMovimientosEndoso(ControlEndosoCot controlEndosoCot);
	
	public void ajustaCotizacionRehabilitacionEndoso(BitemporalCotizacion cotizacion, BigDecimal idToPoliza, Short numeroEndoso);
	
	public void guardaNoOrderBroker(BigDecimal polizaId, String orderNo);
	
	public String obtenerNoOrderBroker(BigDecimal polizaId);
}
