package mx.com.afirme.midas2.service.bitemporal;

import java.util.Collection;

import javax.ejb.Local;

@Local
@SuppressWarnings("rawtypes")
public interface EntidadContinuityService {

	public <C extends EntidadContinuity, K> C findContinuityByKey(Class<C> continuityEntityClass, K key);

	public <C extends EntidadContinuity, K> C findContinuityByBusinessKey(Class<C> continuityEntityClass, String bussinesKeyName, K businessKey);
	
	public <C extends EntidadContinuity, K> Collection<C> findContinuitiesByParentKey(Class<C> continuityEntityClass,String parentKeyName, K parentKey);

	public <C extends EntidadContinuity, K> Collection<C> findContinuitiesByParentBusinessKey(Class<C> continuityEntityClass, String parentBussinesKeyName, K parentBusinessKey);
}
