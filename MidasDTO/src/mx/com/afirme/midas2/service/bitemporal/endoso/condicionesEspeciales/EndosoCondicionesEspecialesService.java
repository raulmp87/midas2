package mx.com.afirme.midas2.service.bitemporal.endoso.condicionesEspeciales;

import java.util.Date;

import javax.ejb.Local;

import org.joda.time.DateTime;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.BitemporalCondicionEspCot;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.BitemporalCondicionEspInc;
import mx.com.afirme.midas2.dto.condicionespecial.CondicionEspecialBitemporalDTO;
import mx.com.afirme.midas2.dto.emision.consulta.Consulta.Nivel;

@Local
public interface EndosoCondicionesEspecialesService {
	
	
	/**
	 * Método que recibe la representación del Bitemporal de BitemporalCondicionEspCot
	 * y se asocia o elimina dependiendo de la accion recibida y guardar. Determinar
	 * que tipo de bitemporal es y realizar un saveInProcess o un remove dependiendo
	 * de EntidadBitemporalService
	 * 
	 * @param condicion
	 * @param cotizacion
	 * @param validoEn
	 * @param accion
	 */
	public void accionBajaCondicionCotizacion(CondicionEspecialBitemporalDTO condicion, BitemporalCotizacion cotizacion, DateTime validoEn, String accion);

	/**
	 * Método que recibe la representación del Bitemporal de BitemporalCondicionEspInc
	 * y se asocia o elimina dependiendo de la accion recibida y guardarlo  Determinar
	 * los Bitemporales del Inciso con el método obtenerIncisos  y realizar un
	 * saveInProcess o un remove dependiendo de EntidadBitemporalService
	 * 
	 * @param condicion
	 * @param cotizacion
	 * @param validoEn
	 * @param accion
	 * @param incisos
	 */
	public void accionBajaCondicionInciso(CondicionEspecialBitemporalDTO condicion, BitemporalCotizacion cotizacion, DateTime validoEn, String accion, String incisos);

	/**
	 * Método que recibe la representación del Bitemporal de BitemporalCondicionEspCot
	 * y se asocia o elimina dependiendo de la accion recibida y guardar. Determinar
	 * que tipo de bitemporal es y realizar un saveInProcess o un remove dependiendo
	 * de EntidadBitemporalService
	 * 
	 * @param condicion
	 * @param cotizacion
	 * @param validoEn
	 * @param accion
	 */
	public void accionInclusionCondicionCotizacion(CondicionEspecialBitemporalDTO condicion, BitemporalCotizacion cotizacion, DateTime validoEn, String accion);

	/**
	 * Método que recibe la representación del Bitemporal de BitemporalCondicionEspInc
	 * y se asocia o elimina dependiendo de la accion recibida y guardarlo  Determinar
	 * los Bitemporales del Inciso con el método obtenerIncisos  y realizar un
	 * saveInProcess o un remove dependiendo de EntidadBitemporalService
	 * 
	 * @param condicion
	 * @param cotizacion
	 * @param validoEn
	 * @param accion
	 * @param incisos
	 */
	public void accionInclusionCondicionInciso(CondicionEspecialBitemporalDTO condicion, BitemporalCotizacion cotizacion, DateTime validoEn, String accion, String incisos);

	/**
	 * Método que compara dos condiciones para identificar si sus valores son
	 * idénticos
	 * 
	 * @param condicionAgregar
	 * @param condicionCot
	 */
	public boolean compararCondicionCotizacion(CondicionEspecialBitemporalDTO condicionAgregar, BitemporalCondicionEspCot condicionCot);

	/**
	 * Método que compara dos condiciones para identificar si sus valores son
	 * idénticos
	 * 
	 * @param condicionAgregar
	 * @param condicionInc
	 */
	public boolean compararCondicionInciso(CondicionEspecialBitemporalDTO condicionAgregar, BitemporalCondicionEspInc condicionInc);

	/**
	 * Método que obtiene listado de CondicionEspecialBitemporalDTO con las
	 * condiciones asociadas dependiendo del tipo de aplicación que se requiera.
	 * Obtener las condiciones que estan como TO_BE_ENDED
	 * 
	 * @param Long
	 * @param NIVEL_APLICACION
	 */
	public CondicionEspecialBitemporalDTO obtenerCondicionesAsociadasBaja(Long cotizacionContinuityId, Nivel NIVEL_APLICACION);

	/**
	 * Método que obtiene listado de CondicionEspecialBitemporalDTO con las
	 * condiciones asociadas dependiendo del tipo de aplicación que se requiera.
	 * Obtener las condiciones que estan asociadas ya sea con el método
	 * obtenerCondicionesIncisoContinuity o obtenerCondicionesCotContinuity de
	 * CondicionEspecialBitemporalService según sea el caso.
	 * 
	 * @param Long
	 * @param NIVEL_APLICACION
	 */
	public CondicionEspecialBitemporalDTO obtenerCondicionesAsociadasInclusion(Long cotizacionContinuityId , Nivel NIVEL_APLICACION);

	/**
	 * Método que obtiene listado de CondicionEspecialBitemporalDTO con las
	 * condiciones disponibles dependiendo del tipo de aplicación que se requiera.
	 * Obtener las condiciones que estan asociadas ya sea con el método
	 * obtenerCondicionesIncisoContinuity o obtenerCondicionesCotContinuity de
	 * CondicionEspecialBitemporalService según sea el caso.
	 * 
	 * @param Long
	 * @param NIVEL_APLICACION
	 */
	public CondicionEspecialBitemporalDTO obtenerCondicionesDisponiblesBaja(Long cotizacionContinuityId, Nivel NIVEL_APLICACION);

	/**
	 * Método que obtiene listado de CondicionEspecialBitemporalDTO con las
	 * condiciones disponibles dependiendo del tipo de aplicación que se requiera.
	 * Obtener las condiciones que estan asociadas ya sea con el método
	 * obtenerCondicionesIncisoContinuity o obtenerCondicionesCotContinuity de
	 * CondicionEspecialBitemporalService según sea el caso y comparar con el listado
	 * que regrese la invocación del método
	 * 
	 * @param Long
	 * @param NIVEL_APLICACION
	 */
	public CondicionEspecialBitemporalDTO obtenerCondicionesDisponiblesInclusion(Long cotizacionContinuityId , Nivel NIVEL_APLICACION);

	/**
	 * Método que valida los numeros de inciso se encuentren asociados a la cotizacion
	 * y regresa los BitemporalInciso correspondientes. Obtener los incisos validos a
	 * través de del método obtenerIncisosCotizacion de EndosoCondicionesEspecialesDAO.
	 * 
	 * Si un código no existe, mandar excepción
	 * 
	 * @param cotizacion
	 * @param incisos
	 * @param validoEn
	 */
	public String obtenerIncisos(BitemporalCotizacion cotizacion, String incisos, Date validoEn);

	/**
	 * Método que elimina las condiciones especiales de BitemporalCondicionEspCot ó
	 * BitemporalCondicionEsp de un BitemporalCotización dependiendo el nivel .
	 * Obtener los Bitemporales inProcess y realizar un remove
	 * 
	 * @param cotizacion
	 * @param validoEn
	 * @param nivel
	 */
	public void removerInclusiones (BitemporalCotizacion cotizacion, Date validoEn, Nivel NIVEL_APLICACION);

}
