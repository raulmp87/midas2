package mx.com.afirme.midas2.service.bitemporal.excepcion;

import java.util.List;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService.NivelEvaluacion;

import org.joda.time.DateTime;

public interface ExcepcionSuscripNegAutoBitemporalService {
	public List<? extends ExcepcionSuscripcionReporteDTO> evaluarCotizacion(BitemporalCotizacion cotizacion, 
															NivelEvaluacion nivelEvaluacion, DateTime validoEn);
}
