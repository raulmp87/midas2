package mx.com.afirme.midas2.service.bitemporal.poliza.inciso;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.BitemporalSeccionInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;

import org.joda.time.DateTime;

@Local
public interface IncisoViewService {

	public BitemporalInciso getInciso(Long incisoContinuityId, Date validoEn, Date recordFrom);
	
	public BitemporalCotizacion getCotizacion(Long cotizacionId, Date validoEn);
	
	public BitemporalCotizacion getCotizacionByContinuity(Long cotizacionContinuityId, Date validoEn);
	
	public BitemporalCotizacion getCotizacionByContinuity(Long cotizacionContinuityId, Date validoEn, Date recordFrom);
	
	public BitemporalInciso getInciso(Long incisoContinuityId , Date validoEn);
	
	public BitemporalInciso getInciso(Long incisoContinuityId , Date validoEn, Date recordFrom, Short claveTipoEndoso);
	
	public BitemporalAutoInciso getAutoInciso(Long incisoContinuityId, Date validoEn);
	
	public BitemporalAutoInciso getAutoInciso(Long incisoContinuityId, Date validoEn, Date recordFrom, Short claveTipoEndoso);
	
	public Collection<BitemporalInciso> getLstIncisoByCotizacion(Long cotizacionContinuityId, Date validoEn);
	
	public Collection<BitemporalCoberturaSeccion> getLstCoberturasByInciso(Long incisoContinuityId, Date validoEn);
	
	public Collection<BitemporalCoberturaSeccion> getLstCoberturasByInciso(Long incisoContinuityId, Date validoEn, boolean inProcess);
	
	public Collection<BitemporalCoberturaSeccion> getLstCoberturasByInciso(Long incisoContinuityId, DateTime validoEn, 
																								DateTime recordFrom, boolean inProcess);
	
	public List<BitemporalCoberturaSeccion> getLstCoberturasByInciso(Long incisoContinuityId, DateTime validoEn, 
																		DateTime recordFrom, Short claveTipoEndoso, boolean inProcess);
	
	public Boolean infoConductorEsRequerido(Long incisoContinuityId, Date validoEn);
	
	public Boolean infoConductorEsRequerido(Long incisoContinuityId, Date validoEn, Boolean getInProcess);
	
	public Boolean infoConductorEsRequerido(Long incisoContinuityId, DateTime validoEn, DateTime recordFrom, Boolean getInProcess);
	
	public BitemporalCotizacion getCotizacionInProcess(BitemporalInciso biInciso, Date validoEn);

	public BitemporalInciso getIncisoInProcess(Long incisoContinuityId, Date validoEn);
	
	public BitemporalAutoInciso getAutoIncisoInProcess(BitemporalInciso biInciso, Date validoEn);
	
	public Collection<BitemporalSeccionInciso> getLstSeccionIncisoInProcess(BitemporalInciso biInciso, Date validoEn);
	
	public Collection<BitemporalInciso> getLstIncisoCanceladosByCotizacion(IncisoCotizacionDTO filtros, Long cotizacionContinuityId, Date validoEn, Date recordFrom, Boolean esConteo);
	
	public Collection<BitemporalInciso> getLstIncisosCanceladosTotales(IncisoCotizacionDTO filtros, Long cotizacionContinuityId, Date validoEn, Date recordFrom, Boolean esConteo);

	public Long getNumeroIncisos(Long idToCotizacion, DateTime validFrom, DateTime recordFrom);
	
	public Short getNumeroEndoso(Long idToCotizacion, Date recordFrom);
	
	public Long getMaxNumeroSecuencia(Long idToCotizacion, DateTime validFromDT, DateTime recordFromDT);
}
