package mx.com.afirme.midas2.service.bitemporal;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

import com.anasoft.os.daofusion.bitemporal.Bitemporal;
import com.anasoft.os.daofusion.bitemporal.Continuity;

public interface EntidadContinuity <V, T extends Bitemporal> extends Continuity<V, T>, Entidad {
	public <C extends EntidadContinuity<V, T>> C getParentContinuity();
	
	@SuppressWarnings("rawtypes")
	public void setParentContinuity(EntidadContinuity parentContinuity);
	
	public Class<T> getBitemporalClass();
	
}
