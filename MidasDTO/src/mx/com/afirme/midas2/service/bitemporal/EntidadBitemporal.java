package mx.com.afirme.midas2.service.bitemporal;

import com.anasoft.os.daofusion.bitemporal.Bitemporal;

public interface EntidadBitemporal  <V, T extends Bitemporal> {
	public V getEmbedded();
	public void setEmbedded(V value);
	public <C extends EntidadContinuity<V, T>> C getEntidadContinuity();
	public void setEntidadContinuity(EntidadContinuity<V,T> entidadContinuity);
}
