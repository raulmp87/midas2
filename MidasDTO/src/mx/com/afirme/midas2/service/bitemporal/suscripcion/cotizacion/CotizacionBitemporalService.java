package mx.com.afirme.midas2.service.bitemporal.suscripcion.cotizacion;

import java.math.BigDecimal;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.Cotizacion;

import org.joda.time.DateTime;

import com.anasoft.os.daofusion.bitemporal.BitemporalWrapper;

@Local
public interface CotizacionBitemporalService {

	/**
	 * Este metodo provee una instancia de {@link BitemporalCotizacion} que es
	 * una clase A travez de este metodo se puede modificar una entidad que
	 * extienda de{@link BitemporalWrapper}. Es necesario proporcionar el
	 * instante en que se desea consulta, en caso de ser nulo, consultara en
	 * base a la fecha actual
	 * 
	 * @param numeroCotizacion
	 *            atributo de {@link BitemporalCotizacion} por el cual se
	 *            realizara una consulta para determinar el objeto valido que se
	 *            retornara
	 * @param validoEn
	 *            {@link DateTime} la fecha en la cual se desea buscar el
	 *            registro bitempóral
	 * @return {@link Cotizacion}
	 */
	public BitemporalCotizacion getBitemporalCotizacion(BigDecimal numeroCotizacion,
			DateTime validoEn);

	public BitemporalCotizacion getBitemporalCotizacion(BigDecimal numeroCotizacion);

	public Cotizacion getBitemporalCotizacion(Long continuityId,
			DateTime validoEn);
	
	public Double getDescuentoPorVolumenaNivelPoliza(BitemporalCotizacion cotizacion,
			DateTime validoEn);
}
