package mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.tipoEndoso;

import java.util.Collection;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.documento.BitemporalDocAnexoCot;

import org.joda.time.DateTime;

@Local
public interface EndosoInclusionAnexoCotService {

	
	public void accionInclusionAnexo(BitemporalDocAnexoCot docAnexo, BitemporalCotizacion cotizacion, DateTime validoEn, String accion);

	public Collection<BitemporalDocAnexoCot> getLstBiDocAnexosAsociados(Long cotizacionContinuityId, DateTime validoEn);
	
	public Collection<BitemporalDocAnexoCot> getLstBiDocAnexosDisponibles(Long cotizacionContinuityId, DateTime validoEn, 
																			Collection<BitemporalDocAnexoCot> lstBiEndososAsociados);


}
