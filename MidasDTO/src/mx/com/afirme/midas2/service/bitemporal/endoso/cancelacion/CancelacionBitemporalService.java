package mx.com.afirme.midas2.service.bitemporal.endoso.cancelacion;

import java.math.BigDecimal;
import java.util.Date;

import javax.ejb.Local;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;

@Local
public interface CancelacionBitemporalService {

	/**
	 * Cancela la póliza para el endoso dado. En caso de no poder cancelarse, las notificaciones se manejan por la 
	 * excepción NegocioEJBException
	 * @param polizaId
	 * @param endosoId
	 * @param fechaInicioVigencia
	 * @param tipoEndoso
	 */
	public void cancelacionPolizaAutomatico(BigDecimal polizaId ,short endosoId,
			Date fechaInicioVigencia, short tipoEndoso, short motivoEndoso);
	
	/**
	 * Cancela la póliza para el endoso dado. En caso de no poder cancelarse, las notificaciones se manejan por la 
	 * excepción NegocioEJBException
	 * @param polizaId
	 * @param endosoId
	 * @param fechaInicioVigencia
	 * @param tipoEndoso
	 * @return
	 */
	public EndosoDTO cancelacionPoliza(BigDecimal polizaId,short endosoId, 
			Date fechaInicioVigencia, short tipoEndoso, short motivoEndoso);
	
	public BitemporalCotizacion guardaCotizacionCancelacionPolizaAutomatico(BigDecimal polizaId ,short endosoId,
			Date fechaInicioVigencia, short tipoEndoso , short claveMotivoEndoso, short migrada);
	
}
