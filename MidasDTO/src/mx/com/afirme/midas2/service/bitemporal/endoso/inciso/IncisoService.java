package mx.com.afirme.midas2.service.bitemporal.endoso.inciso;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;

import org.joda.time.DateTime;

@Local
public interface IncisoService {
	
	public BitemporalInciso calcularIncisoBorrador(BitemporalInciso inciso, BitemporalAutoInciso autoInciso, Map<String, String> datosRiesgo,
			List<BitemporalCoberturaSeccion> coberturas, DateTime validoEn);
	
	public void bajaInciso(Long incisoId, DateTime validoEn);
	
	public BitemporalInciso asociarIncisoEnCotizacion(BitemporalInciso inciso, DateTime validoEn);
	
	public BitemporalInciso getInciso(Long cotizacionId, Long incisoId, DateTime validoEn);
	
	public BitemporalAutoInciso getAutoInciso(Long autoIncisoId, DateTime validoEn);
	
	public BitemporalInciso multiplicarInciso(Long incisoId, DateTime validoEn);
	
	public BitemporalInciso prepareGuardarIncisoBorrador(BitemporalInciso inciso, BitemporalAutoInciso autoInciso, Map<String, String> datosRiesgo,
			List<BitemporalCoberturaSeccion> coberturas, DateTime validoEn);
	
	public void reasignarSecuencia(Long cotizacionContinuityId, Integer numeroInciso) ;
	
	public EndosoDTO getUltimoEndosoInciso(BigDecimal idToPoliza, Long incisoId);
	
}
