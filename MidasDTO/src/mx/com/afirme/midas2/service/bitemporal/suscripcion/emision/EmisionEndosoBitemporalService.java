package mx.com.afirme.midas2.service.bitemporal.suscripcion.emision;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.MovimientoEndoso;

import org.joda.time.DateTime;

@Local
public interface EmisionEndosoBitemporalService {

	public EndosoDTO emiteEndoso(Long cotizacionContinuityId, DateTime validoEn, short tipoEndoso);
	
	public EndosoDTO emiteEndoso(Long cotizacionContinuityId,DateTime validoEn, short tipoEndoso, boolean emiteRecibos);

	public EndosoDTO emiteEndosoMigracion(Long cotizacionContinuityId, DateTime validoEn, short tipoEndoso);
	
	public EndosoDTO emiteEndosoPerdidaTotal(Long cotizacionContinuityId,
			DateTime validoEn, short tipoEndoso, Long idSiniestroPerdidaTotal); 

	public List<MovimientoEndoso> getMovimientos(
			ControlEndosoCot controlEndosoCot);
	
	public short calculaTipoEndoso(EndosoDTO endoso);
	
	public EndosoDTO emiteEndosoDesagrupacionRecibos(Long cotizacionContinuityId,
			DateTime validoEn, short tipoEndoso);	
}
