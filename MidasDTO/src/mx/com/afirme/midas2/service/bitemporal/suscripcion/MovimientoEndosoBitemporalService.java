package mx.com.afirme.midas2.service.bitemporal.suscripcion;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoEndoso;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;

@Local
public interface MovimientoEndosoBitemporalService {
	public void calcular(ControlEndosoCot controlEndosoCot);
	public void calcular(ControlEndosoCot controlEndosoCot, Boolean esMigrada);
	public void calcular(BitemporalCotizacion cotizacionBitemporal);
	public void borrarMovimientos(ControlEndosoCot controlEndosoCot);
	public Double getPctFormaPago(Integer formaPagoId, Short idMoneda);
	public NegocioDerechoEndoso getNegocioDerechoEndoso (BitemporalCotizacion cotizacion);
	public String generarDescripcionMovimientoGeneral(ControlEndosoCot controlEndosoCot,short tipoOperacion);
}
