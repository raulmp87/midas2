/**
 * 
 */
package mx.com.afirme.midas2.service.bitemporal.siniestros;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.interfaz.cliente.DireccionCliente;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.cobranza.prorroga.ToProrroga;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CondicionEspecialReporte;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.IncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SolicitudReporteCabinaDTO;
import mx.com.afirme.midas2.dto.CondicionEspecialDTO;
import mx.com.afirme.midas2.dto.siniestros.DatosContratanteDTO;
import mx.com.afirme.midas2.dto.siniestros.IncisoPolizaDTO;
import mx.com.afirme.midas2.dto.siniestros.IncisoSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.ProgramaPagoDTO;
import mx.com.afirme.midas2.dto.siniestros.ReciboProgramaPagoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.ControlDinamicoRiesgoDTO;

import org.joda.time.DateTime;

@Local
public interface PolizaSiniestroService {
	public static enum TIPO_VALIDACION_CONDICION {
		VALIDAR_REPORTE(0),
		VALIDAR_PASE_GM (1),
		VALIDAR_SINIESTRO (2);
		
		private int tipo;
		
		TIPO_VALIDACION_CONDICION (int tipo){
			this.tipo = tipo;
		}
		
		public static TIPO_VALIDACION_CONDICION tipoValidacion(Integer i){
			TIPO_VALIDACION_CONDICION tipo = null;
			switch(i){
				case 0:
					tipo = VALIDAR_REPORTE;
					break;
				case 1:
					tipo = VALIDAR_PASE_GM;
					break;
				case 2:
					tipo = VALIDAR_SINIESTRO;
					break;
			}
			return tipo;
		}
		
		public int valor(){
			return this.tipo;
		}
     }

	/**
	 * <b>SE AGREGA EN CDU ASIGNAR CONDICIONES ESPECIALES A REPORTE</b>
	 * Método que guarda las condiciones especiales seleccionadas a un reporte de
	 * cabina.
	 * Convertir el String separado por comas que traerá los id de las condiciones
	 * seleccionadas, validar primero que no se encuentren asociadas al reporte, si no
	 * es así se crean.
	 * 
	 * @param idReporte
	 * @param condicionesEspeciales
	 */
	public void asignarCondicionesEspeciales( Long idReporte, String condicionesEspeciales, TIPO_VALIDACION_CONDICION tipoValidacion );
	
	/**
     * <b>SE AGREGA EN CDU ASIGNAR INCISO</b>
	 * Método que asigna el Inciso a un reporte de Cabina.
	 * Utilizar el método save de entidadService para asignarlo a la Entidad
	 * IncisoReporteCabina.
	 * Utilizar el método eliminarCondiciones para eliminar las condiciones del inciso
	 * anterior en caso de existir.
	 * Llenar el campo de nombreAsegurado con el nombre del Contratante de la póliza,
	 * eso se encuentra en el bitemporalCotizacion
	 * 
     * @param incisoSiniestro
     * @param validOn
     * @param recordFrom
     */
	public void asignarInciso(Long idReporte, Long idToPoliza, Long incisoContinuityId, Date validOn, Date knownOn, Date fechaReporteSiniestro) throws Exception;

	/**
	 * <b>SE AGREGA EN CDU CONSULTAR CONDICIONES ESPECIALES</b>
	 * Método que devuelve el listado de las Condiciones Especiales asociadas a la
	 * póliza y/ó al inciso.
	 * Primero buscar las condiciones especiales de la póliza (bitemporales de las
	 * condiciones especiales a nivel cotización) mediante el método de
	 * CondicionEspecialBitemporalService, despues buscar las condiciones especiales
	 * asociadas al inciso mediante el método de CondicionEspecialBitemporalService.
	 * 
	 * Barrer esa lista y crear un objeto CondicionEspecialDTO por cada elemento, y
	 * buscar mediante el método obtenerCondicionEspReporte y si se encuentra asignada
	 * al reporte marcar el atributo asignadoReporte del objeto CondicionEspecialDTO.
	 * 
	 * @param incisoContinuityId
	 * @param idReporte
	 * @return
	 */
	public List<CondicionEspecialDTO> buscarCondicionesEspeciales ( Long incisoContinuityId, Long idReporteCabina, Date fechaReporteSiniestro, Date validOn, Date knownOn);
	
	
	/**
	 * <b>SE AGREGA EN CDU CONSULTAR DETALLE INCISO</b>
	 * Obtener el detalle del Inciso. Llamar al método buscarInciso de esta clase, y
	 * después llenar los datos faltantes.
	 * Crear el objeto BitemporalCotizacion y de ahí obtener la información adicional.
	 * 
	 * Se enlistan los campos y como s obtienen desde los objetos:
	 * 
	 * ------SECCION ENCABEZADO-----
	 * Poliza: A través del bitemporalCotizacion, buscar la Cotizacion Relacionada y
	 * de ahi obtener la Póliza, concatenar los campos de codigoProducto,
	 * codigoTipoPoliza, numeroPoliza, numeroRenovacion (ver ejemplos en Emisión de
	 * Póliza)
	 * Póliza Anterior:  A través del bitemporalCotizacion, buscar la Cotizacion
	 * Relacionada y de ahi obtener la SolicitudDTO, tiene un campo llamado
	 * PolizaAnterior, repetir la concatenación descrita arriba.
	 * Estatus de Póliza: CANCELADO- NO VIGENTE - VIGENTE : A partir del recordFrom
	 * del bitemporalInciso
	 * Motivo de Estatus: Integrar con Solicitudes de Autorización.
	 * Paquete: Atributo del bitemporalAutoInciso
	 * Vigencia Inicio: ValidFrom de BitemporalInciso
	 * Vigencia Fin: ValidTo de BitemporalInciso
	 * Fecha Emisión: ValidFrom de BitemporalInciso
	 * 
	 * ------SECCION POLIZA-----
	 * Producto: BitemporalCotizacion -> SolicitudDTO -> ´ProductoDTO
	 * Medio Pago: BitemporalCotizacion -> MedioPagoDTO
	 * Linea de Negocio: Se llena en método buscarInciso (autoInciso ->
	 * lineaNegocioDesc)
	 * Moneda: BitemporalCotizacion ->MonedaDTO
	 * Forma de Pago: BitemporalCotizacion -> FormaPagoDTO
	 * 
	 * ---SECCION CONTRATANTE---
	 * Utilizar el método obtenerDatosContratante de esta clase enviando el
	 * personaContratanteId de BitemporalCotizacion
	 * 
	 * --SECCION PERSONALIZACION--
	 * Utilizar el método buscarDatosPersonalizacion de esta clase
	 * 
	 * @param incisoSiniestro
	 * @param validOn
	 * @param recordFrom
	 */
	public IncisoSiniestroDTO buscarDetalleInciso(IncisoSiniestroDTO filtro, Date validOn, Date knownOn, Date fechaHoraSiniestro);

	/**
	 * Si se recibe el parámetro de incisoContinuityId buscar sobre las tablas
	 * bitemporales.
	 * Se puede utilizar el método getInciso de IncisoViewService para obtener el
	 * bitemporal del Inciso y partir de ahí para el llenado del BitemporalAutoInciso
	 * mediante el método getAutoInciso de IncisoViewService.
	 * Se enlistan los campos a utilizar y como obtener su valor:
	 * 
	 * Marca : Se llena en getAutoInciso.
	 * Estilo : Se llena en getAutoInciso.
	 * Tipo Vehículo: Se llena en getAutoInciso.(tipo de Uso)
	 * Version:
	 * Modelo: Utilizar método findById de ModelVehiculoFacadeRemote
	 * Num Motor: Se encuntra dentro del bitemporalAutoInciso
	 * Num Serie: Se encuntra dentro del bitemporalAutoInciso
	 * Conductor Habitual: Es la concatenacion de nombreConductor, paternoConductor y
	 * maternoConductor, se encuentra dentro de bitemporalAutoInciso.
	 * REPUVE:Se encuentra dento del bitemporalAutoInciso
	 * 
	 * @param inciso
	 * @param validOn
	 * @param recordFrom
	 */
	public IncisoSiniestroDTO buscarInciso(IncisoSiniestroDTO filtro, Date validOn, Date knownOn);

	/**
	 * Método que realizará la búsqueda de los incisos de acuerdo a los filtros.
	 * Llamar a método de buscarPolizaFiltro de PolizaSiniestroDAO
	 * 
	 * @param filtro
	 * @param validOn
	 * @param recordFrom
	 */
	public List<IncisoSiniestroDTO> buscarPolizaFiltro(IncisoSiniestroDTO filtro, Date validOn, Date knownOn );

	/**
	 * <b>SE AGREGA EN CDU CONSULTAR COBERTURAS INCISO</b>
	 * Método que regresa el listado de coberturas asociadas al inciso.
	 * Utilizar el método getLstCoberturasByInciso de IncisoViewService
	 * 
	 * @param incisoContinuityId
	 * @param validOn
	 * @param recordFrom
	 * @param biCoberturaSeccionList
	 * @return
	 */
	public List<BitemporalCoberturaSeccion> obtenerCoberturasInciso( Long incisoContinuityId, DateTime validOn, DateTime knownOn, Date fechaReporteSiniestro);
	
/**
	 * Invocar el método getDatosRiesgo de ConfiguracionDatoIncisoBitemporalService,
	 * internamente crear un LinkedHashMap<String, String><b>() vacío y mandar
	 * claveTipoEndoso null y getInProcess false, y accionEndoso = 3 (tipo consulta
	 * para que traiga los controles deshabilitados)</b>
	 * <b>Basarse en el método </b>cargaControles de ComplementarIncisoEndosoAction
	 * 
	 * @param incisoContinuityId
	 * @param idToCotizacion
	 * @param validon
	 * @param recordFrom
	 */
	public List<ControlDinamicoRiesgoDTO> obtenerDatosRiesgo(Long incisoContinuityId,Long idToCotizacion, Date validon, Date knownOn, Date fechaReporteSiniestro);
	
	/**
	 * Metodo que se invoca, solo cuando no se encontraron incisos relacionados , y se tiene que pasar el filtro con el dato de Numero de Serie para que 
	 * realize la busqueda.
	 * @param filtro
	 * @return
	 */
	
	public List<ControlDinamicoRiesgoDTO> obtenerDatosRiesgo(Long incisoContinuityId, Long idToCotizacion, Date validon, 
															 Date knownOn, Date fechaReporteSiniestro, Long coberturaId) ;
	
	
	public List<IncisoSiniestroDTO> buscarSolicitudesPoliza (String numeroSerie, BigDecimal idSolicitud);

	/**
	* Para cargar el cliente contratante, utilizar el método loadById de ClienteFacade, a partir del objeto que nos regrese utilizar los métodos descritos debajo para llenar el domicilioFiscal. Se deben guardar los valores en el atributo DatosContratanteDto dentro del objeto IncisoSiniestroDTO para su facil utilización y mostrar en el JSP.
	*	Nombre Asegurado: Concatena nombreFiscal, apellidoPaternoFiscal y apellidoMaternoFiscal.
	*	RFC: Atributo codigoRfcFiscal
	*	País: Utilizar el servicio findById de PaisFacadeRemote
	*	Estado: Utilizar el servicio findById de EstadoFacadeRemote
	*	Municipio: Utilizar el servicio findById de MunicipioFacadeRemote
	*	Colonia: Utilizar el servicio findById de ColoniaFacadeRemote
	*	Calle y Número: Propiedad nombreCalleFiscal
	*	CP: Propiedad codigoPostalFiscal
    *
	* @param personaContratanteId
	* @return
	*/
	
	public DatosContratanteDTO obtenerDatosContratante(BigDecimal personaContratanteId );
	
	/**
	 * 
	 * @param idReporteCabina
	 * @param idCondicionEspecial
	 * @return
	 */
	public CondicionEspecialReporte obtenerCondicionEspReporte ( Long idReporteCabina, Long idCondicionEspecial );

	/**
	 * Valida si el Inciso se encuentra asociado al reporte
	 * @param incisoContinuityId
	 * @param idToPoliza
	 * @param idReporte
	 * @param fechaReporteSiniestro
	 * @return
	 */
	public Integer incisoAsociadoReporteCabina( Long incisoContinuityId, Long idToPoliza, Long idReporte, Date fechaReporteSiniestro );
	
	/**
	 * Consulta si el inciso se encuentra vigente o con Solicitud de Autorización con estatus Autorizada
	 * @param incisoContinuityId
	 * @param idToPoliza
	 * @param fechaReporteSiniestro
	 * @return
	 */
	public int incisoVigenteAutorizado( Long incisoContinuityId, Long idToPoliza, Date validOn, Date knownOn, 
			Date fechaReporteSiniestro, Short estatusInciso, Long idToReporte);
	
	public IncisoSiniestroDTO buscarSolicitudPolizaById(BigDecimal idToSolicitudDataEnTramite) ;
    /**
     * Obtiene los datos del cliente
     * @param idCliente
     * @return
     */
	public ClienteGenericoDTO buscarDatosCliente( BigDecimal idCliente );

	/**
	 * Envia una solicitud de autorizacion por vigencia. El estatus puede ser para un inciso NO_VIGENTE o para uno CANCELADO
	 * @param idToPoliza
	 * @param numeroInciso
	 * @param reporteId
	 * @param estatusInciso
	 */
    public void enviarSolicitudAutorizacionVigencia(Long idToPoliza, BigDecimal numeroInciso, Long reporteId, Short estatusInciso);
	
	public Integer validaSiTieneAutorizacionEnEsperaYEstatus(Long idToPoliza, String vigenciaInciso, Long idToReporte);
/**
	 * Obtiene los Programas de Pago para el inciso correpondiente
	 * @param idToPoliza
	 * @param numeroInciso
	 * @param fechaReporteSiniestro
	 * @return
	 */
	public List<ProgramaPagoDTO> obtenerProgramasPago(Long idToPoliza, Integer numeroInciso, Date fechaReporteSiniestro);
	
	/**
	 * Obtiene los Recibos del Programa de Pago correspondiente
	 * @param idCotizacionSeycos
	 * @param numeroPrograma
	 * @param fechaReporteSiniestro
	 * @return
	 */
	public List<ReciboProgramaPagoDTO> obtenerRecibosPrograma(Long idCotizacionSeycos, Long numeroPrograma, Date fechaReporteSiniestro);
	
	/**
	 * Obtiene las direcciones del Cliente ya sea personal o fiscal
	 * @param idCliente
	 * @return
	 */
	public List<DireccionCliente>  buscarDireccionesCliente( BigDecimal idCliente );
	
	/**
	 * Consulta los datos de la solicitud del reporte cabina
	 * @param idReporteCabina
	 * @return
	 */
	public SolicitudReporteCabinaDTO buscarCartaCobertura ( BigDecimal idToSolicitud, String serie );
	
	/**
	 * Asigna una solicitud a un reporte
	 * @param idToSolicitud
	 */
	public void asignarCartaCobertura( Long idReporte, String idToSolicitud, String numeroSerie );
	
	/**
	 * Obtiene los datos de la prorroga
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param fechaReporteSiniestro
	 * @return
	 */
	public ToProrroga obtenerProrroga( Long idToCotizacion, Long numeroInciso, Date fechaReporteSiniestro );
	
	
	/**
	 * Obtiene el Inciso que ya esta Asignado a un Reporte
	 * @return
	 */
	public IncisoSiniestroDTO obtenerDatosIncisoAsignadoReporte(Long idReporte);
	
	
	/**
	 * Obtiene una carta Cobertura ya Asignada al Reporte
	 */
	public IncisoSiniestroDTO obtenerCartaCoberturaAsignadoReporte( Long idToReporte , String idToSolicitud );
	
	
	/**
	 * Obtiene el Motivo del estatus de la Cobranza
	 * @param idToPoliza
	 * @param numeroInciso
	 * @param fechaReporteSiniestro
	 * @return
	 */
	public String obtenerMotivoCobranza(Long idToPoliza, BigDecimal idToSeccion, Integer numeroInciso, Date fechaReporteSiniestro);

	/**
	 * Valida si ya fueron afectadas las coberturas del reporte
	 * @param idReporteCabina
	 * @return
	 */
	 
	public boolean validacionTieneCoberturasAfectadas(Long idReporteCabina);
	
	/**
	 * Obtiene los datos de riesgo asociados a una cobertura
	 * @param idReporteCabina
	 * @param idCobertura
	 * @return
	 */
	public List<ControlDinamicoRiesgoDTO> obtenerDatosRiesgo(Long idReporteCabina, Long idCobertura);

	/**
	 * Método que asignará un idtopoliza a un siniestro que no cuente con este dato y que este asociado a una carta cobertura.
	 */
	public void actualizarPolizaSiniestro();
	/**
	 * setea el auto inciso al servicio
	 * @param autoInciso
	 */
	public void setAutoInciso(AutoIncisoReporteCabina autoInciso);
	
	/**
	 * Lista de mensajes de validacion
	 * @return
	 */
	public List<String> getMensajesEvaluacion();
	
	/**
	 * Metodo que valida si se pueden asignar o no condiciones especiales
	 * @param condicionEspecial
	 * @param reporteCabina
	 * @param validacion
	 * @return
	 */
	public List<String> validaCondicionEspecial(CondicionEspecial condicionEspecial, ReporteCabina reporteCabina, TIPO_VALIDACION_CONDICION validacion);
	
	/**
	 * Regresa una bandera indicando si la cobertura de un inciso especifico de una poliza ha sido siniestrada. 
	 * @param idToPoliza
	 * @param numeroInciso
	 * @param idToSeccion
	 * @param idToCobertura
	 * @return 
	 */
	public IncisoReporteCabina obtenerIncisoAsignadoAlReporte(Long idReporte);
	
	public String obtenerSituacionVigenciaInciso(Long idReporte);
	
	public String obtenerSituacionVigenciaInciso(Date validFrom, Date fechaOcurrido, String numeroSerie, BigDecimal idToPoliza);
	
	public List<IncisoPolizaDTO> buscarIncisosPoliza(String numeroPoliza, String nombreAsegurado, 
			String numeroSerie, Integer numeroInciso, Date fechaOcurridoSiniestro, Short tipoPoliza);
	
	
	public String asignarIncisoReporte(Long reporteCabinaId, Long incisoContinuityId, Date fechaHoraOcurrido, String usuario);
	
	public Short estatusIncisoAutorizacion(Long idToPoliza, Long reporteId, Integer numeroInciso, Integer estatus);
	
	public String obtenerDescripcionMotivoCobranza(Long idToPoliza, BigDecimal idToSeccion, Integer numeroInciso, Date fechaReporteSiniestro);
	
	public void initialize();
	
	/**
	 * Método que regresa un Map con el listado de coberturas asociadas al inciso ademá de que indicará si se imprime UMA o DSMGVDF.
	 * 
	 * @param incisoContinuityId
	 * @param validOn
	 * @param recordFrom
	 * @param biCoberturaSeccionList
	 * @return
	 */
	public Map<String, Object> obtenerCoberturasIncisoMap( Long incisoContinuityId, DateTime validOn, DateTime knownOn, Date fechaReporteSiniestro);
}