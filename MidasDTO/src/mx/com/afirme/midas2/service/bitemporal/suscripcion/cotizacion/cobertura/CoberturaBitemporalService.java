package mx.com.afirme.midas2.service.bitemporal.suscripcion.cotizacion.cobertura;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;

import org.joda.time.DateTime;

@Local
public interface CoberturaBitemporalService {

	/**
	 * Metodo que obtiene un listado de {@link BitemporalCoberturaSeccion} en
	 * base a la linea de negocio y el paquete, este metodo se recomienda cuando
	 * se este dando de alta incisos
	 * 
	 * @param idToSeccion
	 * @param idPaquete
	 * @param cotizacionContinuityId
	 * @return
	 */
	public List<BitemporalCoberturaSeccion> getCoberturas(
			BigDecimal lineaNegocioId, Long paqueteId,
			Long cotizacionContinuityId);

	public List<BitemporalCoberturaSeccion> getCoberturas(
			BigDecimal lineaNegocioId, Long paqueteId, String estadoId,
			String municipioId, String claveEstilo, Long modeloVehiculo,
			BigDecimal tipoUsoId, String modificadoresPrima,
			String modificadoresDescripcion, Long cotizacionContinuityId, DateTime validoEn);

	public List<BitemporalCoberturaSeccion> getCoberturas(
			BigDecimal lineaNegocioId, Long paqueteId, String estadoId,
			String municipioId, String claveEstilo, Long modeloVehiculo,
			BigDecimal tipoUsoId, String modificadoresPrima,
			String modificadoresDescripcion, boolean aplicaCalculo,
			Long cotizacionContinuityId, DateTime validoEn);

	public List<BitemporalCoberturaSeccion> getCoberturas(
			BigDecimal lineaNegocioId, Long paqueteId, String estadoId,
			String municipioId, String claveEstilo, Long modeloVehiculo,
			BigDecimal tipoUsoId, String modificadoresPrima,
			String modificadoresDescripcion, boolean tienePlantilla,
			boolean aplicaCalculo, Long plantillaId, Long cotizacionContinuityId, DateTime validoEn);

	public List<BitemporalCoberturaSeccion> getCoberturas(
			Long incisoContinuityId, DateTime validoEn,  boolean soloContratadas);
	
	public List<BitemporalCoberturaSeccion> mostrarCoberturas(
			BitemporalAutoInciso autoInciso, Long cotizacionContinuityId, DateTime validoEn);
	
}
