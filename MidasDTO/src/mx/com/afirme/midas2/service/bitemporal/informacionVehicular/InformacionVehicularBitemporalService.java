package mx.com.afirme.midas2.service.bitemporal.informacionVehicular;

import java.math.BigDecimal;

import javax.ejb.Local;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;

import org.joda.time.DateTime;

@Local
public interface InformacionVehicularBitemporalService {
	
	public void validaInfoVehicularByEstiloPolInd(BitemporalCotizacion cotizacionBitemporal, DateTime validoEn, 
			BigDecimal claveTipoEndoso, PolizaDTO polizaDTO) throws Exception;
}
