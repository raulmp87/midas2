package mx.com.afirme.midas2.service.bitemporal.suscripcion.cotizacion.inciso;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;

import org.joda.time.DateTime;

@Local
public interface IncisoBitemporalService {

	public BitemporalInciso initEdicion(IncisoContinuity continuity,
			DateTime validoEn);

	public BitemporalInciso initEdicion(IncisoContinuity continuity);

	public List<BitemporalInciso> getIncisoList(
			CotizacionContinuity continuity, DateTime validoEn);

	public List<BitemporalInciso> getIncisoList(CotizacionContinuity continuity);
	
	
	public void borrarIncisosNoAsociados(CotizacionContinuity continuity,
			DateTime validoEn);
	
	public BitemporalInciso findBitemporalIncisoByNumero(CotizacionContinuity cotizacionContinuity, Integer numero, DateTime validOn, DateTime recordOn, Boolean inProcess);
	
	public void restaurarPrimaIncisoEndoso(BitemporalInciso inciso, DateTime validoEn, Integer tipoEndoso);

}
