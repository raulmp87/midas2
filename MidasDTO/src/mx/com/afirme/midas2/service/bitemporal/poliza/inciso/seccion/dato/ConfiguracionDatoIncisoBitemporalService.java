package mx.com.afirme.midas2.service.bitemporal.poliza.inciso.seccion.dato;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import org.joda.time.DateTime;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.dato.BitemporalDatoSeccion;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoInciso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoId;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.ControlDinamicoRiesgoDTO;

@Local
public interface ConfiguracionDatoIncisoBitemporalService {
	
	public List<ConfiguracionDatoInciso> getDatosRamoInciso(BigDecimal idTcRamo);
	
	public List<ConfiguracionDatoInciso> getDatosSubRamoInciso(
			BigDecimal idTcRamo, BigDecimal idTcSubRamo);
	
	public List<ConfiguracionDatoInciso> getDatosCoberturaInciso(
			BigDecimal idTcRamo, BigDecimal idTcSubRamo,
			BigDecimal idToCobertura);
	/**
	 * Obtiene los datos riesgo obligatorios hasta la emisi�n
	 * @param valores
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @return
	 */
	
	public List<ControlDinamicoRiesgoDTO> obtenerDatosRiesgo(Long incisoContinuityId, Date validoEn,
			Map<String, String> valores, Short tipoEndoso, String accion, Boolean getInProcess);
	
	public List<ControlDinamicoRiesgoDTO> getDatosRiesgo(Long incisoContinuityId, Date validoEn,
			Map<String, String> valores, Long idToCotizacion, String accion, Boolean getInProcess);
	
	public List<ControlDinamicoRiesgoDTO> getDatosRiesgo(Long incisoContinuityId, Date validoEn, Date recordFrom, 
			Map<String, String> valores, Long idToCotizacion, String accion, Short claveTipoEndoso, Boolean getInProcess);
	
	/**
	 * No obtiene los datos riesgo obligatorios hasta la emisi�n
	 * @param valores
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @return
	 */
	public List<ControlDinamicoRiesgoDTO> getDatosRiesgoCotizacion(Long incisoContinuityId, Date validoEn,
			Map<String, String> valores, String accion, Boolean getInProcess);
	/**
	 * 
	 * @param valores
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param claveFiltroRIesgo
	 * @param incisoCotizacionDTO
	 * @return
	 */
	public List<ControlDinamicoRiesgoDTO> getDatosRiesgo(Long incisoContinuityId, Date validoEn,
			Map<String, String> valores,Short claveFiltroRIesgo, String accion, Boolean getInProcess);
	
	public List<ControlDinamicoRiesgoDTO> getDatosRiesgo(Long incisoContinuityId, Date validoEn, Date recordFrom, 
			Map<String, String> valores, Short claveFiltroRIesgo, String accion, Short claveTipoEndoso, Boolean getInProcess);
	
	
	public List<ControlDinamicoRiesgoDTO> getDatosRiesgo(Long incisoContinuityId, Date validoEn, Date recordFrom, 
			Map<String, String> valores, Short claveFiltroRIesgo, String accion, Short claveTipoEndoso, 
			Boolean getInProcess, Long coberturaId);
	
	
	public ConfiguracionDatoIncisoId getConfiguracionDatoIncisoId(String idDatoRiesgo);
	
	public ControlDinamicoRiesgoDTO findByConfiguracionDatoIncisoId(ConfiguracionDatoIncisoId id);
	
	public boolean isHorizontal(String idDatoRiesgo, String id);
	
	public void guardarDatosAdicionalesPaquete(Map<String, String> datosRiesgo,	Long incisoContinuityId, Date validoEn);
	
	public  MensajeDTO validarListaParaEmitir(BitemporalCotizacion cotizacion, Short claveTipoEndoso, Date validoEn);
	
	public Boolean infoConductorEsRequerido(BitemporalInciso inciso, Date validoEn);
	
	public boolean validaDatosRiesgos(Long incisoContinuityId, Date validoEn, Short tipoEndoso);
	
	public int validaDatosComplementariosIncisoBorrador(BitemporalCotizacion cotizacion, BitemporalInciso inciso, Short claveTipoEndoso, Date validoEn);
	
	public Collection<BitemporalDatoSeccion> getDatosSeccionByInciso(Long incisoContinuityId, Date validoEn, Date recordFrom, Short claveTipoEndoso, Boolean getInProcess);
	
	public List<ControlDinamicoRiesgoDTO> obtieneDescripcionDatosImpresion(Long incisoContinuityId, DateTime validOn, DateTime recordFrom);
}


