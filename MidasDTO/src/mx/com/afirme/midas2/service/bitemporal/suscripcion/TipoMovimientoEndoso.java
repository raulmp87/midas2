package mx.com.afirme.midas2.service.bitemporal.suscripcion;

public enum TipoMovimientoEndoso {
	POLIZA(1), INCISO(2), COBERTURA(3);
	
	private final Integer value;
	
	private TipoMovimientoEndoso(Integer value) {
		this.value = value;
	}
	
	public Integer getValue() {
		return value;
	}
}
