package mx.com.afirme.midas2.service.jobAgentes;

import java.util.List;

import mx.com.afirme.midas2.domain.jobAgentes.TareaProgramada;
/**
 * Interface para manejar las tareas como generacion de preview de calculo programado
 * de bonos, comisiones y cargos.
 * @author vmhersil
 *
 */
public interface AgenteTaskManagerService {
	/**
	 * Este metodo debe de regresar una lista de las tareas programadas, cada tarea debe de 
	 * tener un identificador que permite consultar o generar el calculo de cada tarea, y la 
	 * fecha en que debe de ejecutarse esa tarea y compararse con la fecha actual en que 
	 * se esta ejecutando este metodo.
	 * conceptoEjecucionAutomatica = parametro para identificar si se ejecuto para Provisiones o Bonos
	 * @return
	 */
	public List<TareaProgramada> getTaskToDo(String conceptoEjecucionAutomatica);
	/**
	 * Debe de iterar la lista de tareas, comparar que la fecha actual del sistema sea igual a 
	 * la fecha en que debe de ejecutarse la tarea y llamar a su respectivo servicio que 
	 * generara el calculo o proceso a realizar en esa fecha.
	 * @param tasks
	 */
	public void executeTasks();
}