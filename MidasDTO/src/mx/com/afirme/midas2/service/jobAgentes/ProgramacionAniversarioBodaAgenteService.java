package mx.com.afirme.midas2.service.jobAgentes;

import javax.ejb.Local;

@Local
public interface ProgramacionAniversarioBodaAgenteService extends
		AgenteTaskManagerService {
	public void initialize();

}
