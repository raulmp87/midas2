package mx.com.afirme.midas2.service.jobAgentes;

import javax.ejb.Local;

/**
 * Interface para exponer el EJB para programacion de calculo de bonos
 * Hereda los metodos de su interface padre.
 * @author vmhersil
 *
 */
@Local
public interface ProgramacionCalculoBonosService extends AgenteTaskManagerService{
	public void initialize();

}
