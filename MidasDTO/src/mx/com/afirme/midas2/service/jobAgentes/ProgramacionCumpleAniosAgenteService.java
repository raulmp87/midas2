package mx.com.afirme.midas2.service.jobAgentes;

import javax.ejb.Local;

@Local
public interface ProgramacionCumpleAniosAgenteService extends AgenteTaskManagerService {
	public void initialize();

}
