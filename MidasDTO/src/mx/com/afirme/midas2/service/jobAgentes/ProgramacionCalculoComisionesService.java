package mx.com.afirme.midas2.service.jobAgentes;

import javax.ejb.Local;
/**
 * Interface para exponer el EJB para programacion de calculo de comisiones 
 * Hereda los metodos de su interface padre.
 * @author vmhersil
 *
 */
@Local
public interface ProgramacionCalculoComisionesService extends AgenteTaskManagerService{
	public void initialize();
}
