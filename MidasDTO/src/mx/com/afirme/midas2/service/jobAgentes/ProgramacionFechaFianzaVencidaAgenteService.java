package mx.com.afirme.midas2.service.jobAgentes;

import javax.ejb.Local;

@Local
public interface ProgramacionFechaFianzaVencidaAgenteService extends AgenteTaskManagerService {
	public void initialize();

}
