package mx.com.afirme.midas2.service.fronterizos;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.fronterizos.BitacoraCESVI;

@Local
public interface BitacoraCESVIService {
	public void guardarBitacora(BitacoraCESVI bitacoraCESVI);
	
	public Double obtenerValorComercial(BitacoraCESVI bitacoraCESVI);
}
