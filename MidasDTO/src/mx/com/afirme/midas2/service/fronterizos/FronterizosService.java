package mx.com.afirme.midas2.service.fronterizos;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.fronterizos.LoginKBB;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;

import org.joda.time.DateTime;


@Local
public interface FronterizosService {
	
	public String getLogin() throws Exception;
	public LoginKBB saveKey( LoginKBB loginKBB );
	public LoginKBB findKey();
	public String validateVIN(String VIN) throws Exception;
	public boolean isActive();
	public HashMap<String, Map<String, String>> mostrarInformacionVehicular(String vin, NegocioSeccion negocioSeccion, String idMoneda, String idEstados, 
			BigDecimal idToCotizacion, BigDecimal numeroInciso, CotizacionDTO cotizacion) throws Exception;
	public Double obtieneValorSumaAseguradaCot (NegocioCobPaqSeccion coberturaNegocio, CoberturaSeccionDTO coberturaSeccion, BigDecimal idCotizacion, 
			Long numeroInciso, String numeroSerie, String estadoId);
	public Double obtieneValorSumaAseguradaPol (Date fechaHoraOcurrido, BigDecimal idToCotizacion, 
			Integer numeroInciso, Short tipoValidacionNumSerie, String numeroSerie, String estadoId, Long idReporteCabina, 
			Double valorSumaAseguradaMin, Double valorSumaAseguradaMax);
	public void migrarClavesCotizacion(CotizacionDTO cotizacionDTO) throws Exception;
	public void migrarClavesCotizacionBitemporal(BitemporalCotizacion bitemporalCotizacion,DateTime validoEn) throws Exception;
	public void actualizarClaves(String claveAmis, String claveSesas, String estiloId) throws Exception;
	public void guardarBitacora(BigDecimal idToCotizacion, BigDecimal numeroInciso, Long idReporteCabina,
			String vin, BigDecimal valorEstimadoPesos, BigDecimal valorEstimadoDolares, Short tipoValidacion);
	public void generateKey();
	public void initialize();
}
