package mx.com.afirme.midas2.service.fronterizos;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.fronterizos.BitacoraKBB;

@Local
public interface BitacoraKBBService {
	public void guardarBitacora(BitacoraKBB bitacoraKBB);
	
	public Double obtenerValorComercial(BitacoraKBB bitacoraKBB);
}
