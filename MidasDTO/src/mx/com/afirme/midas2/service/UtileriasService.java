package mx.com.afirme.midas2.service;

import java.math.BigDecimal;
import java.util.Map;

import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.ImportesOrdenCompraDTO;



public interface UtileriasService {
	
	
	public int  diferenciaDiasFechas(String fechaInicial, String fechaFinal);
	
	public String  obtenerRFCbeneficiario( Integer  idPrestador);
	
	public String  obtenerCURPbeneficiario(Integer idPrestador);
	
	public BigDecimal obtenerTotalesOrdenCompra(Long idEstimacionCobRepCab,Long idReporteCabina, Long idcoberturaReporteCabina);
	
	public BigDecimal obtenerReserva(Long idToCobertura, Long idEstimacionCobertura) ;
	
	public BigDecimal obtenerReservaDisponibleOrdenCompra(Long idEstimacionCobRepCab,
			Long idReporteCabina, Long idcoberturaReporteCabina);
	
	
	public boolean ordenCompraAplicaBeneficiario( Long idcoberturaReporteCabina,String cveSubTipoCalculo);
	
	/*
	 * Debera pasarse una cadena de coberturas en el formato  idCobertura|CveSubcalculo ej.   118|RCV,119|0,118|RCB
	 * Si no tiene cve Subcalculo concatener cero ej 119|0
	 * */
	public Map<Long, String> getListasTercerosAfectadorPorCoberturas(String cadenaCoberturas);
	public ImportesOrdenCompraDTO calcularImportes(Long idOrdenCompra, Long idOrdenCompraDetalle, boolean calcularPorConcepto);


}
