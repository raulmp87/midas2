package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.textoadicional;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;

public interface TextoAdicionalService {
	public List<TexAdicionalCotDTO> listarFiltrado(CotizacionDTO cotizacion);
	public void actualizarGrid(String accion,TexAdicionalCotDTO texAdicionalCotDTO,CotizacionDTO cotizacion);
	public void guardarGrid(CotizacionDTO cotizacion, TexAdicionalCotDTO texAdicionalCotDTO);
	public void eliminaRow(TexAdicionalCotDTO texAdicionalCotDTO);
}
