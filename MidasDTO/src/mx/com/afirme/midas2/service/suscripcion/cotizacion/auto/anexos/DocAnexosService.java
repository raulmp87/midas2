package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.anexos;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDTO;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotId;
import mx.com.afirme.midas2.domain.documentosFortimax.DocumentoEntity;
import mx.com.afirme.midas2.domain.documentosFortimax.TipoDocumentoEntity;
import mx.com.afirme.midas2.dto.fuerzaventa.DocumentoFortimax;


public interface DocAnexosService {

	public List<DocAnexoCotDTO> cargaAnexosPorCotizacion(BigDecimal idToCotizacion);
	public DocAnexoCotDTO findById(DocAnexoCotId id);
	public void modificarDocAnexo(DocAnexoCotDTO docAnexoCotDTO);
	public String[] generateExpedientClient(Long id) throws Exception;
	public List<DocumentoFortimax> cargarListaDocsFortimax(Long id, String tipoDocumento) throws Exception;
	public List<String> getDocumentosFaltantes(Long idAgente,Long claveTipoPersona,String tipoDocumento,String grupoDocumentos) throws Exception;
	public List<String> validarDocumentos(Long id, List<String> documentosPorValidar) throws Exception;
	public String[] getAndSaveDocument(Long id,String tipoDocumento,String grupoDocumentos) throws Exception;
	public List<TipoDocumentoEntity> findTypeDocuments(Long idAgente,String tipoDocumento) throws Exception;
	public List<DocumentoEntity> findAllDocumentsEntity(Long id,String grupoDocumentos) throws Exception; 
	public void generateCobDocumentsByCotizacion(BigDecimal idToCotizacion);
}
