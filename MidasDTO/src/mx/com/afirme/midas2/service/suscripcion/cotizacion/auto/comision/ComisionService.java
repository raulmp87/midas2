package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.comision;

import javax.ejb.Remote;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;


public interface ComisionService {
	public void generarComision(CotizacionDTO cotizacion);
}
