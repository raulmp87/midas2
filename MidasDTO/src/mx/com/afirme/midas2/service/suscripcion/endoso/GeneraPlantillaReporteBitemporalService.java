package mx.com.afirme.midas2.service.suscripcion.endoso;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.suscripcion.impresion.EdicionEndoso;
import mx.com.afirme.midas2.domain.suscripcion.impresion.EdicionInciso;
import mx.com.afirme.midas2.domain.suscripcion.impresion.EdicionPoliza;
import mx.com.afirme.midas2.dto.CondicionesEspIncisosDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosCoberturasDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosIncisoDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;

import org.joda.time.DateTime;

/**
 * 
 * @author José Rodrigo Díaz Luján - rdiazafirme@gmail.com
 *
 */
@Local
public interface GeneraPlantillaReporteBitemporalService {

	public enum FoliosPermitidosImpresion{
		TSSO,
		AUP
	}
	
	/**
	 * 
	 * @param idToPoliza
	 * @param locale
	 * @param validOn
	 * @param recordFrom
	 * @param claveTipoEndoso
	 * @return
	 */
	public TransporteImpresionDTO imprimirPoliza(BigDecimal idToPoliza, Locale locale, DateTime validOn, 
			DateTime recordFrom, Short claveTipoEndoso, boolean incluirCondicionesEsp);
	/**
	 * Permite indicar se se incluye o no la carátula de la poliza; también permite indicar
	 * que incisos se imprimiran
	 * @param idToPoliza
	 * @param locale
	 * @param validOn
	 * @param recordFrom
	 * @param incluirCaratula
	 * @param incluirRecibo
	 * @param incluirCertificadoRC
	 * @param incluirReferenciaBancaria
	 * @param incluirAnexosInciso
	 * @param incisoInicial
	 * @param incisoFinal
	 * @param claveTipoEndoso
	 * @param esSituacionActual
	 * @param incluirCondicionesEsp
	 * @return
	 */
	public TransporteImpresionDTO imprimirPoliza(BigDecimal idToPoliza, Locale locale, DateTime validOn,
		DateTime recordFrom, 
	    boolean incluirCaratula, boolean incluirRecibo, 
	    boolean incluirCertificadoRC,boolean  incluirReferenciaBancaria, boolean incluirAnexosInciso,
	    int incisoInicial, int incisoFinal, Short claveTipoEndoso, 
	    Boolean esSituacionActual, boolean incluirCondicionesEsp);
	
	/**
	 * Permite indicar se se incluye o no la carátula de la poliza; también permite indicar
	 * que incisos se imprimiran
	 * @param idToPoliza
	 * @param locale
	 * @param validOn
	 * @param recordFrom
	 * @param incluirCaratula
	 * @param incluirRecibo
	 * @param incluirCertificadoRC
	 * @param incluirReferenciaBancaria
	 * @param incluirAnexosInciso
	 * @param incisoInicial
	 * @param incisoFinal
	 * @param claveTipoEndoso
	 * @param esSituacionActual
	 * @param incluirCondicionesEsp
	 * @param aviso
	 * @param derechos
	 * @return
	 */
	public TransporteImpresionDTO imprimirPoliza(BigDecimal idToPoliza, Locale locale, DateTime validOn,
		DateTime recordFrom, 
	    boolean incluirCaratula, boolean incluirRecibo, 
	    boolean incluirCertificadoRC,boolean  incluirReferenciaBancaria, boolean incluirAnexosInciso,
	    int incisoInicial, int incisoFinal, Short claveTipoEndoso, 
	    Boolean esSituacionActual, boolean incluirCondicionesEsp, boolean aviso, boolean derechos);
	
	/**
	 * Permite indicar se se incluye o no la carátula de la poliza; también permite indicar
	 * que incisos se imprimiran
	 * @param idToPoliza
	 * @param locale
	 * @param validOn
	 * @param recordFrom
	 * @param incluirCaratula
	 * @param incluirRecibo
	 * @param incluirCertificadoRC
	 * @param incluirAnexosInciso
	 * @param incisoInicial
	 * @param incisoFinal
	 * @param claveTipoEndoso
	 * @param esSituacionActual
	 * @param incluirCondicionesEsp
	 * @return
	 */
	public TransporteImpresionDTO imprimirPoliza(BigDecimal idToPoliza, Locale locale, DateTime validOn,
		DateTime recordFrom, 
	    boolean incluirCaratula, boolean incluirRecibo, 
	    boolean incluirCertificadoRC, boolean incluirAnexosInciso,
	    int incisoInicial, int incisoFinal, Short claveTipoEndoso, 
	    Boolean esSituacionActual, boolean incluirCondicionesEsp);
	
	/**
	 * Permite indicar se se incluye o no la carátula de la poliza; también permite indicar
	 * si se incluyen o no los incisos
	 * @param idToPoliza
	 * @param locale
	 * @param validOn
	 * @param recordFrom
	 * @param incluirCaratula
	 * @param incluirRecibo
	 * @param incluirIncisos
	 * @param incluirCertificadoRC
	 * @param incluirAnexosInciso
	 * @param claveTipoEndoso
	 * @return
	 */
	public TransporteImpresionDTO imprimirPoliza(BigDecimal idToPoliza, Locale locale, DateTime validOn,
		DateTime recordFrom, 
		boolean incluirCaratula, boolean incluirRecibo, boolean incluirIncisos,
		boolean incluirCertificadoRC, boolean incluirAnexosInciso, Short claveTipoEndoso,boolean incluirCondicionesEsp);
	
	/**
	 * Permite indicar se se incluye o no la carátula de la poliza; también permite indicar
	 * si se incluyen o no los incisos
	 * @param idToPoliza
	 * @param locale
	 * @param validOn
	 * @param recordFrom
	 * @param incluirCaratula
	 * @param incluirRecibo
	 * @param incluirIncisos
	 * @param incluirCertificadoRC
	 * @param incluirAnexosInciso
	 * @param claveTipoEndoso
	 * @param referenciaBancaria
	 * @return
	 */
	public TransporteImpresionDTO imprimirPoliza(BigDecimal idToPoliza, Locale locale, DateTime validOn,
		DateTime recordFrom, 
		boolean incluirCaratula, boolean incluirRecibo, boolean incluirIncisos,
		boolean incluirCertificadoRC, boolean incluirAnexosInciso, Short claveTipoEndoso,boolean incluirCondicionesEsp
		,boolean  incluirReferenciaBancaria);
	
	/**
	 * Permite indicar se se incluye o no la carátula de la poliza; también permite indicar
	 * si se incluyen o no los incisos
	 * @param idToPoliza
	 * @param locale
	 * @param validOn
	 * @param recordFrom
	 * @param incluirCaratula
	 * @param incluirRecibo
	 * @param incluirIncisos
	 * @param incluirCertificadoRC
	 * @param incluirAnexosInciso
	 * @param claveTipoEndoso
	 * @param referenciaBancaria
	 * @param aviso
	 * @param derechos
	 * @return
	 */
	public TransporteImpresionDTO imprimirPoliza(BigDecimal idToPoliza, Locale locale, DateTime validOn,
		DateTime recordFrom, 
		boolean incluirCaratula, boolean incluirRecibo, boolean incluirIncisos,
		boolean incluirCertificadoRC, boolean incluirAnexosInciso, Short claveTipoEndoso,boolean incluirCondicionesEsp
		,boolean  incluirReferenciaBancaria, boolean aviso, boolean derechos);
	
	
	/**
	 * Permite obtener el documento de condiciones especiales por póliza
	 * @param bitemporalCotizacion
	 * @param validOn
	 * @param knownOn
	 * @return
	 * @throws IOException
	 */
	public byte[] imprimirCondicionesPoliza( 
			List<CondicionEspecial> dataSourceCondicionesEspecialesPoliza,
			List<CondicionEspecial> dataSourceCondicionesEspecialesIncisos,
			String numeroPoliza,
			boolean polizaTipoFlotilla 
	);
	
	/**
	 * Permite indicar se se incluye o no la carátula de la poliza; también permite indicar
	 * si se incluyen o no los incisos
	 * @param idToPoliza
	 * @param locale
	 * @param validOn
	 * @param recordFrom
	 * @param incluirCaratula
	 * @param incluirRecibo
	 * @param incluirIncisos
	 * @param incluirCertificadoRC
	 * @param incluirAnexosInciso
	 * @param claveTipoEndoso
	 * @return
	 */
	public TransporteImpresionDTO imprimirPolizaSO(BigDecimal idToPoliza, Locale locale, DateTime validOn,
		DateTime recordFrom, Short claveTipoEndoso, String e_mail);
	
	/**
	 * Permite obtener el documento PDF de condiciones especiales por inciso con su detalle
	 * @param datosInciso
	 * @param locale
	 * @return
	 */
	public byte[] imprimirCondicionesInciso(DatosIncisoDTO datosInciso,	String titulo,	Locale locale); 


	
	public byte[] imprimirCondicionesEspecialesEndoso(List<CondicionesEspIncisosDTO> datasource,
			String titulo,
			String numeroPoliza);
	
	public String obtenerCadenaIncisoCondicionEspecial(CondicionEspecial condicion, 
			BitemporalCotizacion bCotizacionFiltrado, DateTime dateTimeTemp, DateTime recordFrom );
	
	public List<CondicionEspecial> obtenerCondicionesEspecialesIncisosEndoso(
			BitemporalCotizacion bitempCotizacion, DateTime validOnDT,
			DateTime recordFromDT);
	
	/**
	 * Obtiene la informacion de la caratula de la poliza para presentarla en la pantalla de edicion
	 * @param idToPoliza
	 * @param validOn
	 * @param recordFrom
	 * @param claveTipoEndoso
	 * @param esSituacionActual
	 * @return
	 */
	public EdicionPoliza obtenerEdicionPoliza(BigDecimal idToPoliza, 
			DateTime validOn, DateTime recordFrom, Short claveTipoEndoso, Boolean esSituacionActual);
	
	/**
	 * Guarda la informacion de la caratula de la poliza que fue editada
	 * @param edicionCapturada
	 * @param idToPoliza
	 * @param validOn
	 * @param recordFrom
	 * @param claveTipoEndoso
	 * @param esSituacionActual
	 * @return
	 */
	public EdicionPoliza guardarEdicionPoliza(EdicionPoliza edicionCapturada, BigDecimal idToPoliza, 
			DateTime validOn, DateTime recordFrom, Short claveTipoEndoso, Boolean esSituacionActual);
	
	/**
	 * restaura los valores originales de la poliza en la edicion
	 * @param edicionPoliza
	 * @return
	 */
	public EdicionPoliza restaurarEdicionPoliza(BigDecimal idToPoliza, DateTime validOn, 
			DateTime recordFrom, Short claveTipoEndoso, Boolean esSituacionActual);
	
	/**
	 * genera el PDF de la caratula de la poliza con los datos editados
	 * @param edicionPoliza
	 * @return
	 */
	public TransporteImpresionDTO imprimirEdicionPoliza(BigDecimal idToPoliza, DateTime validOn, 
			DateTime recordFrom, Short claveTipoEndoso, Boolean esSituacionActual, Locale locale);

	/**
	 * Se agrega este metodo para imprimir la edicion de la caratula de la poliza desde el componente de impresion
	 * @param idToPoliza
	 * @param locale
	 * @param validOn
	 * @param recordFrom
	 * @param incluirCaratula
	 * @param incluirRecibo
	 * @param incluirTodosLosIncisos
	 * @param incluirCertificadoRC
	 * @param incluirAnexosIncisos
	 * @param incisoInicial
	 * @param incisoFinal
	 * @param claveTipoEndoso
	 * @param esSituacionActual
	 * @param incluirCondicionesEsp
	 * @return
	 */
	public TransporteImpresionDTO imprimirPoliza(BigDecimal idToPoliza,
			Locale locale, DateTime validOn, DateTime recordFrom,
			boolean incluirCaratula, boolean incluirRecibo,
			boolean incluirTodosLosIncisos, boolean incluirCertificadoRC,
			boolean incluirAnexosIncisos, boolean incluirReferenciasBancarias, int incisoInicial, int incisoFinal,
			Short claveTipoEndoso, Boolean esSituacionActual,
			boolean incluirCondicionesEsp, boolean incluirEdicionPoliza, boolean imprimirEdicionInciso,
			boolean incluirCartaCargo);
	
	/**
	 * Se agrega este metodo para imprimir la edicion de la caratula de la poliza desde el componente de impresion
	 * @param idToPoliza
	 * @param locale
	 * @param validOn
	 * @param recordFrom
	 * @param incluirCaratula
	 * @param incluirRecibo
	 * @param incluirTodosLosIncisos
	 * @param incluirCertificadoRC
	 * @param incluirAnexosIncisos
	 * @param incisoInicial
	 * @param incisoFinal
	 * @param claveTipoEndoso
	 * @param esSituacionActual
	 * @param incluirCondicionesEsp
	 * @param aviso
	 * @param derechos
	 * @return
	 */
	public TransporteImpresionDTO imprimirPoliza(BigDecimal idToPoliza,
			Locale locale, DateTime validOn, DateTime recordFrom,
			boolean incluirCaratula, boolean incluirRecibo,
			boolean incluirTodosLosIncisos, boolean incluirCertificadoRC,
			boolean incluirAnexosIncisos, boolean incluirReferenciasBancarias, int incisoInicial, int incisoFinal,
			Short claveTipoEndoso, Boolean esSituacionActual,
			boolean incluirCondicionesEsp, boolean incluirEdicionPoliza, boolean imprimirEdicionInciso, 
			boolean incluirCartaCargo , boolean aviso, boolean derechos);
	
	/**
	 * Obtiene la informacion de la caratula del inciso para presentarla en la pantalla de edicion
	 * @param idToPoliza
	 * @param validOn
	 * @param recordFrom
	 * @param claveTipoEndoso
	 * @param esSituacionActual
	 * @return
	 */
	public EdicionInciso obtenerEdicionInciso(BigDecimal idToPoliza, 
			DateTime validOn, DateTime recordFrom, Short claveTipoEndoso);
	
	/**
	 * valida si existe una edicion de caratula de poliza
	 * @param idToPoliza
	 * @param validOn
	 * @param recordFrom
	 * @param claveTipoEndoso
	 * @param esSituacionActual
	 * @return
	 */
	public boolean existeEdicionPoliza(BigDecimal idToPoliza, DateTime validOn, 
			DateTime recordFrom, Short claveTipoEndoso, Boolean esSituacionActual);
	
	/**
	 * Se agrega este metodo para incluir o excluir la carta de cargo Automático
	 * @param idToPoliza
	 * @param locale
	 * @param validOn
	 * @param recordFrom
	 * @param incluirCaratula
	 * @param incluirRecibo
	 * @param incluirIncisos
	 * @param incluirCertificadoRC
	 * @param incluirAnexosIncisos
	 * @param claveTipoEndoso
	 * @param incluirCondicionesEsp
	 * @param incluirCartaCargo
	 * @return
	 */
	public TransporteImpresionDTO imprimirPoliza(BigDecimal idToPoliza,
			Locale locale, DateTime validOn, DateTime recordFrom,
			boolean incluirCaratula, boolean incluirRecibo,
			boolean incluirIncisos, boolean incluirCertificadoRC,
			boolean incluirAnexosIncisos, boolean incluirReferenciasBancarias, Short claveTipoEndoso,
			boolean incluirCondicionesEsp,boolean incluirCartaCargo);
	
	/**
	 * valida si existe una edicion de caratula de inciso
	 * @param idToPoliza
	 * @param validOn
	 * @param recordFrom
	 * @param claveTipoEndoso
	 * @return
	 */
	public boolean existeEdicionInciso(BigDecimal idToPoliza, DateTime validOn, 
			DateTime recordFrom, Short claveTipoEndoso);
	
	/**
	 * Guarda la informacion capturada para la edicion del inciso
	 * @param edicionCapturada
	 * @param idToPoliza
	 * @param validOn
	 * @param recordFrom
	 * @param claveTipoEndoso
	 * @param esSituacionActual
	 * @return
	 */
	public EdicionInciso guardarEdicionInciso(EdicionInciso edicionCapturada, BigDecimal idToPoliza, 
			DateTime validOn, DateTime recordFrom, Short claveTipoEndoso, List<DatosCoberturasDTO> coberturasPoliza);
	
	/**
	 * borra la informacion capturada para la edicion del inciso
	 * @param idToPoliza
	 * @param validOn
	 * @param recordFrom
	 * @param claveTipoEndoso
	 * @return
	 */
	public EdicionInciso restaurarEdicionInciso(BigDecimal idToPoliza, DateTime validOn, 
			DateTime recordFrom, Short claveTipoEndoso);
	
	/**
	 * Obtiene la informacion de la coberturas que se utiliza en la edicion de incisos
	 * @param validOn
	 * @param recordFrom
	 * @param idToCotizacion
	 * @param claveTipoEndoso
	 * @return
	 */
	public List<DatosCoberturasDTO> obtenerCoberturasEdicionInciso(DateTime validOn, DateTime recordFrom, 
			BigDecimal idToPoliza, Short claveTipoEndoso);
	
	/**
	 * valida si existe una edicion de endoso
	 * @param idToPoliza
	 * @param validOn
	 * @param recordFrom
	 * @param claveTipoEndoso
	 * @param esSituacionActual
	 * @return
	 */
	public boolean existeEdicionEndoso(BigDecimal idToCotizacion, Date recordFrom, Short claveTipoEndoso);

	/**
	 * Obtiene la informacion de la edicion del endoso
	 * @param idToCotizacion
	 * @param recordFrom
	 * @param claveTipoEndoso
	 * @return
	 */
	public EdicionEndoso obtenerEdicionEndoso(BigDecimal idToCotizacion, Date recordFrom, Short claveTipoEndoso);
	
	/**
	 * Guarda la informacion editada del endoso
	 * @param edicionEndoso
	 * @param idToCotizacion
	 * @param recordFrom
	 * @param claveTipoEndoso
	 * @return
	 */
	public EdicionEndoso guardarEdicionEndoso(EdicionEndoso edicionEndoso, BigDecimal idToCotizacion, Date recordFrom, Short claveTipoEndoso);
	
	/**
	 * Carga la informacion original del ensodo
	 * @param idToCotizacion
	 * @param recordFrom
	 * @param claveTipoEndoso
	 * @return
	 */
	public EdicionEndoso restaurarEdicionEndoso(BigDecimal idToCotizacion, Date recordFrom, Short claveTipoEndoso);
}
