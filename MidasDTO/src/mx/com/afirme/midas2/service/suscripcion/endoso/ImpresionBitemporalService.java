package mx.com.afirme.midas2.service.suscripcion.endoso;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.domain.suscripcion.impresion.EdicionInciso;
import mx.com.afirme.midas2.dto.impresiones.DatosCaratulaInciso;
import mx.com.afirme.midas2.dto.impresiones.DatosCaratulaPoliza;
import mx.com.afirme.midas2.dto.impresiones.DatosCoberturasDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosPolizaDTO;

import org.joda.time.DateTime;

@Local
public interface ImpresionBitemporalService {

	/**
	 * Obtiene la información de la carátula de la poliza, tomando en cuenta la fecha que se 
	 * recibe como parámetro para determinar cuál era la información vigente en ese momento.
	 * @param idToPoliza
	 * @param validOn
	 * @return
	 */
	public DatosCaratulaPoliza llenarPolizaDTO(BigDecimal idToPoliza, DateTime validOn, DateTime recordFrom, 
			Short claveTipoEndoso, Boolean esSituacionActual);
	/**
	 * Obtiene la información de la carátula de la poliza, tomando en cuenta la fecha que se 
	 * recibe como parámetro para determinar cuál era la información vigente en ese momento.
	 * @param idToPoliza
	 * @param validOn
	 * @return
	 */
	
	public DatosCaratulaPoliza llenarReferenciasBancariasPolizaDTO(BigDecimal idToPoliza, DateTime validOn, DateTime recordFrom, 
			Short claveTipoEndoso, Boolean esSituacionActual);
	

	/**
	 * Permite solicitar la información de un todos los incisos asociados a una poliza y una 
	 * fecha de validez
	 * @param bCotizacion
	 * @param numeroPoliza
	 * @param validOn
	 * @param recordFrom
	 * @param polizaDTO
	 * @param datosPolizaDTO
	 * @param numeroEndoso
	 * @param claveTipoEndoso
	 * @return
	 */
	public List<DatosCaratulaInciso> llenarIncisoDTO(BitemporalCotizacion bCotizacion,
			String numeroPoliza, DateTime validOn, DateTime recordFrom, 
			PolizaDTO polizaDTO, DatosPolizaDTO datosPolizaDTO, Short numeroEndoso, Short claveTipoEndoso, boolean incluirCondicionesEsp);
	
	/**
	 * Permite solicitar la información de un rango de incisos asociados a una poliza y una
	 * fecha de validez
	 * @param bCotizacion
	 * @param numeroPoliza
	 * @param validOn
	 * @param recordFrom
	 * @param incisoInicial
	 * @param incisoFinal
	 * @param polizaDTO
	 * @param datosPolizaDTO
	 * @param numeroEndoso
	 * @param claveTipoEndoso
	 * @return
	 */
	public List<DatosCaratulaInciso> llenarIncisoDTO(BitemporalCotizacion bCotizacion,
			String numeroPoliza, DateTime validOn, DateTime recordFrom, int incisoInicial, int incisoFinal, 
			PolizaDTO polizaDTO, DatosPolizaDTO datosPolizaDTO, Short numeroEndoso, Short claveTipoEndoso, boolean incluirCondicionesEsp);
	
	/**
	 * Permite obtener una colleccion de los documentos Anexos correspondientes a una poliza y una 
	 * fecha de validez
	 * @param bitemporalCotizacion
	 * @param validOn
	 * @param knownOn
	 * @return
	 */
	public Collection<byte[]> imprimirAnexosPoliza(
			BitemporalCotizacion bitemporalCotizacion, DateTime validOn, DateTime knownOn) throws IOException;
	
	/**
	 * Permite obtener una colleccion de los documentos Anexos correspondientes a un Endoso y una 
	 * fecha de validez
	 * @param bitemporalCotizacion
	 * @param validOn
	 * @param knownOn
	 * @return
	 */
	public Collection<byte[]> imprimirAnexosEndoso(
			BitemporalCotizacion bitemporalCotizacion, DateTime validOn, DateTime knownOn, ControlEndosoCot controlEndosoCot) throws IOException;
	
	/**
	 * Obtiene el numero de inciso en la poliza de Seycos a partir del inciso de Midas
	 * @param inciso Inciso de Midas
	 * @param clavePolizaSeycos Clave de la poliza de Seycos
	 * @return El numero de inciso de Seycos
	 */
	 public String obtenerNumeroIncisoSeycos(BitemporalInciso inciso, String clavePolizaSeycos);
	
	
	 /**
	 * Obtiene el numero de endoso en la poliza de Seycos a partir del endoso de Midas
	 * @param endoso Endoso de Midas
	 * @param clavePolizaSeycos Clave de la poliza de Seycos
	 * @return El numero de endoso de Seycos
	 */
	public String obtenerNumeroEndosoSeycos(EndosoDTO endoso, String clavePolizaSeycos);
	
	/**
	 * Obtiene el Identificador y los datos del agente para la caratula de la Póliza
	 * @param bitempCotizacion
	 * @return El identificador y los datos del agente
	 */
	public Map<String,String> getIdentificadorCaratulaPoliza(BitemporalCotizacion bitempCotizacion);
	
	/**
	 * Metodo para la nueva funcionalidad de edicion de inciso
	 * @param bCotizacion
	 * @param numeroPoliza
	 * @param validOn
	 * @param recordFrom
	 * @param incisoInicial
	 * @param incisoFinal
	 * @param polizaDTO
	 * @param datosPolizaDTO
	 * @param numeroEndoso
	 * @param claveTipoEndoso
	 * @param incluirCondicionesEsp
	 * @param imprimirEdicionInciso
	 * @return
	 */
	public List<DatosCaratulaInciso> llenarIncisoDTO( BitemporalCotizacion bCotizacion, String numeroPoliza,
			DateTime validOn, DateTime recordFrom, int incisoInicial, int incisoFinal, PolizaDTO polizaDTO,
			DatosPolizaDTO datosPolizaDTO, Short numeroEndoso, Short claveTipoEndoso,
			boolean incluirCondicionesEsp, EdicionInciso edicionInciso);
			
	/**
	 * Metodo para la nueva funcionalidad de edicion de inciso
	 * @param bCotizacion
	 * @param numeroPoliza
	 * @param validOn
	 * @param recordFrom
	 * @param polizaDTO
	 * @param datosPolizaDTO
	 * @param numeroEndoso
	 * @param claveTipoEndoso
	 * @param incluirCondicionesEsp
	 * @param imprimirEdicionInciso
	 * @return
	 */
	public List<DatosCaratulaInciso> llenarIncisoDTO( BitemporalCotizacion bCotizacion, String numeroPoliza, 
			DateTime validOn, DateTime recordFrom, PolizaDTO polizaDTO, DatosPolizaDTO datosPolizaDTO,
			Short numeroEndoso,	Short claveTipoEndoso, boolean incluirCondicionesEsp, EdicionInciso edicionInciso);
	
	/**
	 * Obtiene la lista de coberturas a mostrar en la edicion del inciso
	 * @param bitemporaCotizacion
	 * @param validOn
	 * @param recordFrom
	 * @param claveTipoEndoso
	 * @return
	 */
	public List<DatosCoberturasDTO> obtenerCoberturasEdicionInciso(BitemporalCotizacion bitemporaCotizacion,
			DateTime validOn, DateTime recordFrom, Short claveTipoEndoso);
}
