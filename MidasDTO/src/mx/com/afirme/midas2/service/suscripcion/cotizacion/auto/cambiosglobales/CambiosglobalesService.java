package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cambiosglobales;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas2.domain.catalogos.Paquete;


public interface CambiosglobalesService {
	
	public CoberturaCotizacionDTO saveCoberturtas(CoberturaCotizacionDTO coberturas);
	
	public List<Paquete> getPaquete(BigDecimal idToCotizacion) ;
	
	public void  getCoberturas(List<CoberturaCotizacionDTO> coberturaCotizacionList,Long plantillaId,CoberturaCotizacionDTO coberturaCotizacionDTO);
	
	public List<CoberturaCotizacionDTO> obtenerCoberturasPlantilla(Long idToNegPaqueteSeccion,BigDecimal idToCotizacion);
	
	/**
	 * Metodo que restablece los valores del negocio sobre la cotizaci�n
	 * @param idCotizacion
	 * @param idLinea
	 * @param idPaquete
	 */
	public void reestablecerValoresDelNegocio(BigDecimal idCotizacion, BigDecimal idLinea, Long idPaquete);
}
