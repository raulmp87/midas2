package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoInciso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoId;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.ControlDinamicoRiesgoDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;


public interface ConfiguracionDatoIncisoService extends EntidadService {

	public List<ConfiguracionDatoInciso> getDatosRamoInciso(BigDecimal idTcRamo);

	public List<ConfiguracionDatoInciso> getDatosSubRamoInciso(
			BigDecimal idTcRamo, BigDecimal idTcSubRamo);

	public List<ConfiguracionDatoInciso> getDatosCoberturaInciso(
			BigDecimal idTcRamo, BigDecimal idTcSubRamo,
			BigDecimal idToCobertura);
	/**
	 * Obtiene los datos riesgo obligatorios hasta la emisi�n
	 * @param valores
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @return
	 */
	public List<ControlDinamicoRiesgoDTO> getDatosRiesgo(
			Map<String, String> valores, BigDecimal idToCotizacion,
			BigDecimal numeroInciso);
	/**
	 * No obtiene los datos riesgo obligatorios hasta la emisi�n
	 * @param valores
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @return
	 */
	public List<ControlDinamicoRiesgoDTO> getDatosRiesgoCotizacion(
			Map<String, String> valores,IncisoCotizacionDTO inciso);
	/**
	 * 
	 * @param valores
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param claveFiltroRIesgo
	 * @param incisoCotizacionDTO
	 * @return
	 */
	public List<ControlDinamicoRiesgoDTO> getDatosRiesgo(
			Map<String, String> valores,Short claveFiltroRIesgo,IncisoCotizacionDTO inciso);
	
	public ConfiguracionDatoIncisoId getConfiguracionDatoIncisoId(String idDatoRiesgo);
	
	public ControlDinamicoRiesgoDTO findByConfiguracionDatoIncisoId(ConfiguracionDatoIncisoId id);
	
	public boolean isHorizontal(String idDatoRiesgo, String id);
}
