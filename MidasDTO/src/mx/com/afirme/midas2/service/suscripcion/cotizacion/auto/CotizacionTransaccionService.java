package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;

public interface CotizacionTransaccionService {

	CotizacionDTO copiarCotizacion(CotizacionDTO cotizacion);
}
