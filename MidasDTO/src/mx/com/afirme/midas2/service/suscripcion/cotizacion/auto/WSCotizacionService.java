package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.CotizacionView;
import mx.com.afirme.midas2.domain.ws.autoplazo.Cliente;

public interface WSCotizacionService {

	String validacionDatosCotizacion(String claveUsuario, Long idNegocio, Long idPaquete, String nombreAsegurado,
    		String apellidoPaterno,String apellidoMaterno,	String rfc,String claveSexo, String telefonoCasa,
    		String telefonoOficina,String email)throws SystemException ;
	
	CotizacionDTO creaCotizacion(Integer sucursal,String claveUsuario, Long idNegocio, Double descuento, String folio,Long idPaquete, Integer estilo, String modelo, Integer marca,
										String idEstado,String idMunicipio, Double valorFactura,String nombreAsegurado,String apellidoPaterno,String apellidoMaterno,
										String rfc,String claveSexo,String claveEstadoCivil,String idEstadoNacimiento,Date fechaNacimiento,String codigoPostal,
										String idCiudad,String estado,String colonia,String calleNumero, String telefonoCasa,String telefonoOficina,String email,
										String numeroSerie, String numeroMotor, String codigoPostalCirculacion)throws SystemException ;
	
	PolizaDTO emitir(BigDecimal idCotizacion, String numeroCredito, String numeroSerie, 
			String numeroMotor, BigDecimal idToPoliza, String folio)throws SystemException;
	
	
	List<String> listPaquetesByIdNegocio(Long idNegocio);
	
	byte[] imprimirPoliza(BigDecimal idPoliza);
	
	PolizaDTO emision(BigDecimal idCotizacion, String numeroCredito, String numeroSerie, 
			String numeroMotor, BigDecimal idToPoliza, String folio, Cliente cliente)throws SystemException;
	
	CotizacionDTO creaCotizacion(CotizacionView cotizacionView) throws SystemException;
	
	public Map<String, Object> evalueExcepcion (CotizacionDTO cotizacionDto);//JFGG
}
