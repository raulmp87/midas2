package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo.RamoTipoPolizaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.CotizacionExpress;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoInciso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAuto;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.CotizacionExpressDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.DatosCotizacionExpressDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.EsquemaPagoCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;

public interface CotizacionService {
	
	public void guardarEjecutivoColoca(BigDecimal idToCotizacion, String ejecutivoColoca);
    public List<CotizacionDTO> listarFiltrado(CotizacionDTO cotizacionDTO);
	public List<CotizacionDTO> buscarCotizacion(CotizacionDTO cotizacionDTO, Date fechaInicial, Date fechaFinal, BigDecimal valorPrimaTotalFin,Agente agente,Promotoria promotoria,Double descuento,Double descuentoFin);
	
	public DatosCotizacionExpressDTO obtenerDatosCotizacionExpressDTO(CotizacionExpressDTO cotizacionExpress);
	public CotizacionDTO crearCotizacion(SolicitudDTO solicitud, Short estatus);
	public CotizacionDTO crearCotizacion(CotizacionDTO cotizacion,String claveUsuario);
	public CotizacionDTO complementarCreacionCotizacion(CotizacionDTO cotizacionDTO);
	public Map<Boolean,Double> compararDescuento(CotizacionDTO cotizacion);
	public Map<Integer,Integer> compararVigenciaCotizacionPorProducto(CotizacionDTO cotizacionDTO, Date vigenciaInicial, Date vigenciaFinal);
	public Date getFinVigenciaDefault(ProductoDTO producto, Date vigenciaInicial);
	public CotizacionDTO descuentoGlobalCalculado(CotizacionDTO cotizacion);
	public Double getDescuentoPorVolumenaNivelPoliza(CotizacionDTO cotizacionDTO);
	public Map<BigDecimal, String> getMapContratantes(CotizacionDTO cotizacionDTO);
	public CotizacionDTO getFechasDefaults(CotizacionDTO cotizacion);
	
	public EsquemaPagoCotizacionDTO findEsquemaPagoCotizacion(Date fechaIniBase, Date fechaInicial, Date fechaFinal,
			Integer idFormaPago,  BigDecimal primaNeta, BigDecimal derecho, BigDecimal iva, Integer idMoneda, 
            Double impRecargoFraccionado, boolean obtenerRecargoDefault);
	public Short getTipoPersona(CotizacionDTO cotizacion);
	public String actualizarAsegurado(CotizacionDTO cotizacion,Short tipoAsegurado);
	public Short getDatosAsegurado(CotizacionDTO cotizacion,Short tipoAsegurado);	
	public CotizacionDTO solicitudCotizacionFormal(CotizacionExpress cotizacionExpress); 
	public CotizacionDTO guardarCotizacion(CotizacionDTO cotizacion);
	public CotizacionDTO copiarCotizacion(CotizacionDTO cotizacion);
	public void reasignarSuscriptor(CotizacionDTO cotizacion);
	public List<EsquemaPagoCotizacionDTO> setEsquemaPagoCotizacion(CotizacionDTO cotizacion,Date fechaIniBase, Date fechaInicial, Date fechaFinal,
			                                                       BigDecimal primaNeta, BigDecimal derecho, BigDecimal iva, BigDecimal impPrimaTotal);
	
	public TerminarCotizacionDTO terminarCotizacion(BigDecimal idToCotizacion, boolean esRenovacion);
	
	public MensajeDTO validarListaParaEmitir(BigDecimal idToCotizacion);
	
	public void actualizarEstatusCotizacion(BigDecimal idToCotizacion);
	
	public void actualizarEstatusCotizacionEnProceso(BigDecimal idToCotizacion);
	
	public BigDecimal actualizarDatosContratanteCotizacion(BigDecimal idToCotizacion, BigDecimal idCliente, String nombreUsuario);
	
	public boolean validaIVACotizacion(BigDecimal idToCotizacion, String nombreUsuario);
	
	public boolean validaDatosRiesgo(BigDecimal idToCotizacion, BigDecimal numeroInciso, Short nivelConfiguracionDatosRiesgo);
	
	public boolean requiereDatosRiesgo(BigDecimal idToCotizacion, BigDecimal numeroInciso, Short nivelConfiguracionDatosRiesgo);
	
	public Long obtenerTotalFiltradoCotizacion(CotizacionDTO cotizacionDTO, Date fechaInicial, Date fechaFinal, BigDecimal valorPrimaTotalFin,Agente agente,Promotoria promotoria,Double descuento,Double descuentoFin);
	
	public void actualizarEstatusCotizacionEmitida(BigDecimal idToCotizacion);
	
	public void actualizarTipoImpresionCliente(BigDecimal idToCotizacion,BigDecimal idTipoImpresionCliente);
	
	public Long obtenerTotalFiltradoCotizacion(CotizacionDTO cotizacionDTO);
	
	public void cancelarCotizacion(BigDecimal idToCotizacion);
	
	public void cancelarCotizacionByFolio (BigDecimal idToCotizacion, String folio);
	
	public TerminarCotizacionDTO validarEmisionCotizacion(BigDecimal idToCotizacion);
	
	public int validaFechaOperacion(CotizacionDTO cotizacion);
	
	public String obtenerNumeroPrimerRecibo();
	
	public void calcularTodosLosIncisos(BigDecimal idCotizacion);
		
	public Boolean calcularCambioDescuento(BigDecimal idCotizacion);
	
	public boolean calcularTodosLosIncisosNoIgualacion(BigDecimal idCotizacion);
	
	public CotizacionDTO updateSolicitudCotizacion(CotizacionDTO cotizacion);
	
	public void inicializaIgualacionCotizacion(BigDecimal idToCotizacion);
	
	public TerminarCotizacionDTO validaIgualacionCotizadorAgentes(BigDecimal idToCotizacion);
	
	/**
	 * 20131216 Actualiza el tipo de agrupacion de recibos  
	 * @param idToCotizacion Id de la cotizacion
	 * @param tipoAgrupacionRecibos Valores validos (UB - Ubicacion , FI - Filial , NE - Negocio)
	 */
	public void actualizarTipoAgrupacionRecibos(BigDecimal idToCotizacion, String tipoAgrupacionRecibos);
	
	public CotizacionDTO obtenerCotizacionPorId(BigDecimal idToCotizacion);
	
	public boolean validaConfiguracionDatosRiesgo(List<ConfiguracionDatoInciso> configuracionPrincipal, List<DatoIncisoCotAuto> datoRiesgoCotizacion, 
			SeccionCotizacionDTO seccion, List<RamoTipoPolizaDTO> ramos, List<SubRamoDTO> subRamos);
	
	public EsquemaPagoCotizacionDTO findEsquemaPagoCotizacion(CotizacionDTO cotizacion);
	
	public void eliminarCotizacion(BigDecimal idToCotizacion);
	
	public void validaTerminarCotizacionWS(TerminarCotizacionDTO terminarCotizacionDTO) throws SystemException;
	
	/**
	 * 20170104 Validar si la cotizacion esta recuotificada
	 * @param idToCotizacion
	 * @return
	 */
	public Boolean validarCotizacionRecuotificada(BigDecimal idToCotizacion);
} 