package mx.com.afirme.midas2.service.suscripcion.solicitud.impresiones;

import java.util.Locale;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;

public interface GeneraPlantillaCartaRenovacionService {
	public TransporteImpresionDTO llenarCartaRenovacion(PolizaDTO polizaDTO, Locale locale);
}
