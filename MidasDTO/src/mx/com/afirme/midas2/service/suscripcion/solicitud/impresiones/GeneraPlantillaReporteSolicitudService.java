package mx.com.afirme.midas2.service.suscripcion.solicitud.impresiones;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Locale;

import javax.ejb.Remote;

import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;


public interface GeneraPlantillaReporteSolicitudService {
	public TransporteImpresionDTO llenarSolicitudDTO(BigDecimal idToSolicitud,Locale locale);

	public InputStream impresionDeudorPrima(String nivel, BigDecimal Rango);
	
	public void generaRepDeudorPrima(String nivel, BigDecimal rango);
	
	public String creaDXPD2(String nivel, BigDecimal rango) throws IOException;
}
