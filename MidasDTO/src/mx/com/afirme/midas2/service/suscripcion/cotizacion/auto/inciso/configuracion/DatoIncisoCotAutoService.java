package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAuto;


public interface DatoIncisoCotAutoService {
	public List<DatoIncisoCotAuto> getDatosRamoInciso(BigDecimal idCotizacion,
			BigDecimal numeroInciso, BigDecimal idTcRamo);

	public List<DatoIncisoCotAuto> getDatosSubRamoInciso(
			BigDecimal idCotizacion, BigDecimal numeroInciso,
			BigDecimal idTcRamo, BigDecimal idTcSubRamo);

	public List<DatoIncisoCotAuto> getDatosCoberturaInciso(
			BigDecimal idCotizacion, BigDecimal numeroInciso,
			BigDecimal idTcRamo, BigDecimal idTcSubRamo,
			BigDecimal idToCobertura);
	
	public DatoIncisoCotAuto getDatoIncisoByConfiguracion(
			BigDecimal idCotizacion, BigDecimal idToSeccion, BigDecimal numeroInciso,
			BigDecimal idTcRamo, BigDecimal idTcSubRamo, BigDecimal idToCobertura,
			Short claveDetalle, BigDecimal idDato);

	
}
