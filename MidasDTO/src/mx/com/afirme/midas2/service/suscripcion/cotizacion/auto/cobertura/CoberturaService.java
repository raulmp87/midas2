package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobertura;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.CotizacionCoberturaView;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.CotizacionExpress;

public interface CoberturaService {
	
	/**
	 * Metodo que obtiene el grupo de coberturas que le corresponden a un inciso
	 * de acuerdo al negocio, el tipo de poliza, la linea de negocio o seccion y
	 * el paquete.
	 * 
	 * @param IdNegocio
	 * @param idToProducto
	 * @param idTipoPoliza
	 * @param idToSeccion
	 * @param idPaquete
	 * @return List<CoberturaCotizacionDTO>
	 */
	public List<CoberturaCotizacionDTO> getCoberturas(Long IdNegocio,
			BigDecimal idToProducto, BigDecimal idTipoPoliza,
			BigDecimal idToSeccion, Long idPaquete, Short idMoneda);
	
	/**
	 * Metodo que obtiene el grupo de coberturas que le corresponden a un inciso
	 * de acuerdo al negocio, el tipo de poliza, la linea de negocio o seccion,
	 * paquete y cotizacion.
	 * @param IdNegocio
	 * @param idToProducto
	 * @param idTipoPoliza
	 * @param idToSeccion
	 * @param idPaquete
	 * @param idCotizacion
	 * @param idMoneda
	 * @return
	 */
	public List<CoberturaCotizacionDTO> getCoberturas(Long IdNegocio,
			BigDecimal idToProducto, BigDecimal idTipoPoliza,
			BigDecimal idToSeccion, Long idPaquete, BigDecimal idCotizacion, Short idMoneda);
	
	
	/**
	 * Metodo que obtiene el grupo de coberturas que le corresponden a un inciso
	 * de acuerdo al negocio, el tipo de poliza, la linea de negocio o seccion,
	 * el paquet, estado, municipio, cotizacion, moneda, numero de secuencia
	 * (inciso), clave de estilo vehiculo, modelo vehiculo, tipo de uso
	 * vehiculo, modificadores de prima y modificadores de descripcion. 
	 * @param IdNegocio
	 * @param idToProducto
	 * @param idTipoPoliza
	 * @param idToSeccion
	 * @param idPaquete
	 * @param idEstado
	 * @param idMunicipio
	 * @param idCotizacion
	 * @param idMoneda
	 * @param numeroInciso
	 * @param claveEstilo
	 * @param modeloVehiculo
	 * @param tipoUso
	 * @param modificadoresPrima
	 * @param modificadoresDescripcion
	 * @return
	 */
	public List<CoberturaCotizacionDTO> getCoberturas(Long IdNegocio,BigDecimal idToProducto,
			BigDecimal idTipoPoliza, BigDecimal idToSeccion, Long idPaquete,
			String idEstado, String idMunicipio, BigDecimal idCotizacion, Short idMoneda, Long numeroInciso, String claveEstilo, Long modeloVehiculo,
			BigDecimal tipoUso, String modificadoresPrima, String modificadoresDescripcion);
	
	public List<CoberturaCotizacionDTO> getCoberturas(Long IdNegocio,BigDecimal idToProducto,
			BigDecimal idTipoPoliza, BigDecimal idToSeccion, Long idPaquete,
			String idEstado, String idMunicipio, BigDecimal idCotizacion, Short idMoneda, Long numeroInciso, String claveEstilo, Long modeloVehiculo,
			BigDecimal tipoUso, String modificadoresPrima, String modificadoresDescripcion, String numeroSerie, Boolean vinValido);
	/**
	 *	Se agrega el agente a la configuracion de las coberturas
	 */
	public List<CoberturaCotizacionDTO> getCoberturas(Long IdNegocio,BigDecimal idToProducto,
			BigDecimal idTipoPoliza, BigDecimal idToSeccion, Long idPaquete,
			String idEstado, String idMunicipio, BigDecimal idCotizacion, Short idMoneda, Long numeroInciso, String claveEstilo, Long modeloVehiculo,
			BigDecimal tipoUso, String modificadoresPrima, String modificadoresDescripcion, String numeroSerie, Boolean vinValido, BigDecimal codigoAgente);
	/**
	 * Metodo que obtiene el grupo de coberturas que le corresponden a un inciso
	 * de acuerdo al negocio, el tipo de poliza, la linea de negocio o seccion,
	 * el paquet, estado, municipio, cotizacion, moneda, numero de secuencia
	 * (inciso), clave de estilo vehiculo, modelo vehiculo, tipo de uso
	 * vehiculo, modificadores de prima y modificadores de descripcion. 
	 * @param IdNegocio
	 * @param idToProducto
	 * @param idTipoPoliza
	 * @param idToSeccion
	 * @param idPaquete
	 * @param idEstado
	 * @param idMunicipio
	 * @param idCotizacion
	 * @param idMoneda
	 * @param numeroInciso
	 * @param claveEstilo
	 * @param modeloVehiculo
	 * @param tipoUso
	 * @param modificadoresPrima
	 * @param modificadoresDescripcion
	 * @param aplicaCalculo
	 * @return
	 */
	public List<CoberturaCotizacionDTO> getCoberturas(Long IdNegocio,BigDecimal idToProducto,
			BigDecimal idTipoPoliza, BigDecimal idToSeccion, Long idPaquete,
			String idEstado, String idMunicipio, BigDecimal idCotizacion, Short idMoneda, Long numeroInciso, String claveEstilo, Long modeloVehiculo,
			BigDecimal tipoUso, String modificadoresPrima, String modificadoresDescripcion,boolean aplicaCalculo, String numeroSerie, Boolean vinValido);
	
	/**
	 *	Se agrega el agente a la configuracion de las coberturas
	 */
	public List<CoberturaCotizacionDTO> getCoberturas(Long IdNegocio,BigDecimal idToProducto,
			BigDecimal idTipoPoliza, BigDecimal idToSeccion, Long idPaquete,
			String idEstado, String idMunicipio, BigDecimal idCotizacion, Short idMoneda, Long numeroInciso, String claveEstilo, Long modeloVehiculo,
			BigDecimal tipoUso, String modificadoresPrima, String modificadoresDescripcion, boolean aplicaCalculo, String numeroSerie, Boolean vinValido, BigDecimal codigoAgente);

	/**
	 * Metodo que obtiene el grupo de coberturas que le corresponden a un inciso
	 * de acuerdo al negocio, el tipo de poliza, la linea de negocio o seccion,
	 * el paquet, estado, municipio, cotizacion, moneda, numero de secuencia
	 * (inciso), clave de estilo vehiculo, modelo vehiculo, tipo de uso
	 * vehiculo, modificadores de prima y modificadores de descripcion. 
	 * Si existe una plantilla para la cotizacion, linea y paquete, se busca la 
	 * cobertura correspondiente dentro de la plantilla para obtener los nuevos 
	 * valores
	 * 
	 * @param IdNegocio
	 * @param idToProducto
	 * @param idTipoPoliza
	 * @param idToSeccion
	 * @param idPaquete
	 * @param idEstado
	 * @param idMunicipio
	 * @param idCotizacion
	 * @param idMoneda
	 * @param numeroSecuenciaInciso
	 * @param claveEstilo
	 * @param modeloVehiculo
	 * @param tipoUso
	 * @param modificadoresPrima
	 * @param modificadoresDescripcion
	 * @param tienePlantilla
	 * @param aplicaCalculo
	 * @return List<CoberturaCotizacionDTO>
	 */
	public List<CoberturaCotizacionDTO> getCoberturas(Long IdNegocio,
			BigDecimal idToProducto, BigDecimal idTipoPoliza,
			BigDecimal idToSeccion, Long idPaquete, String idEstado,
			String idMunicipio, BigDecimal idCotizacion, Short idMoneda,
			Long numeroSecuenciaInciso, String claveEstilo,
			Long modeloVehiculo, BigDecimal tipoUso, String modificadoresPrima,
			String modificadoresDescripcion, boolean tienePlantilla, boolean aplicaCalculo,  Long plantillaId, String numeroSerie, Boolean vinValido);
	
	public List<CoberturaCotizacionDTO> getCoberturas(Long IdNegocio,
			BigDecimal idToProducto, BigDecimal idTipoPoliza,
			BigDecimal idToSeccion, Long idPaquete, String idEstado,
			String idMunicipio, BigDecimal idCotizacion, Short idMoneda,
			Long numeroSecuenciaInciso, String claveEstilo,
			Long modeloVehiculo, BigDecimal tipoUso, String modificadoresPrima,
			String modificadoresDescripcion, boolean tienePlantilla, boolean aplicaCalculo,  Long plantillaId, String numeroSerie, Boolean vinValido, BigDecimal codigoAgente);

	public List<CoberturaCotizacionDTO> getCoberturasContratadas(
			IncisoCotizacionDTO inciso);

	public List<CoberturaCotizacionDTO> getCoberturas(IncisoCotizacionDTO inciso);
	
	public List<CoberturaCotizacionDTO> getCoberturas(Long negocioPaqueteId, IncisoCotizacionId id);
	
	public List<CoberturaCotizacionDTO> getCoberturas(IncisoCotizacionDTO inciso, boolean soloContratadas);
	
	public List<CoberturaCotizacionDTO> getCoberturas(CotizacionExpress cotizacionExpress, IncisoCotizacionDTO inciso);

	/**
	 * Obtiene el <code>String</code> correspondiente de la suma asegurada de
	 * acuerdo a la cobertura.
	 * 
	 * @param coberturaSumaAsegurada
	 * @return
	 */
	public String getCoberturaSumaAseguradaStr(CoberturaDTO cobertura,
			BigDecimal sumaAsegurada);
	
	/**
	 * Obtener todas las coberturas de una cotizacion (para fines de impresion)
	 * @param idToCotizacion
	 * @param soloContratadas
	 * @return
	 */
	public List<CoberturaCotizacionDTO> getCoberturas(BigDecimal idToCotizacion, boolean soloContratadas);
	
	/**
	 * Obtener todas las coberturas de una cotizacion omitiendo las repetidas
	 * @param idToCotizacion
	 * @param soloContratadas
	 * @return
	 */
	public List<CoberturaCotizacionDTO> getCoberturasDistinct(BigDecimal idToCotizacion, boolean soloContratadas);
	
	/**
	 * Obtener todas las coberturas de una cotizacion con prima neta 0
	 * @param idToCotizacion
	 * @param soloContratadas
	 * @return
	 */
	public List<CoberturaCotizacionDTO> getCoberturasPrimaNetaCero(BigDecimal idToCotizacion, boolean soloContratadas);
	
	/**
	 * Obtener todas las coberturas sin cotizacion
	 * @param cot
	 * @return
	 */
	public List<CoberturaCotizacionDTO> getCoberturas(CotizacionCoberturaView cot);
	
	/**
	 * Obtener la configuracion de una cobertura a partir de los parametros elegidos en la pantalla tomando en cuenta 
	 * los nuevos parametros de tipo de uso, agente y si la cotizacion es renovacion
	 * @param idToCobertura
	 * @param negocioPaqueteId
	 * @param idEstado
	 * @param idMunicipio
	 * @param idMoneda
	 * @param idTipoUso
	 * @param idAgente
	 * @param renovacion
	 * @return
	 */
	public NegocioCobPaqSeccion getLimitesSumaAseguradaPorCobertura(
			BigDecimal idToCobertura, Long negocioPaqueteId, String idEstado, String idMunicipio, BigDecimal idMoneda,
			BigDecimal idTipoUso, BigDecimal idAgente, boolean renovacion);
	
	/**
	 * Obtener las configuraciones de las coberturas a partir de los parametros elegidos en la pantalla tomando en cuenta
	 * los nuevos parametros de tipo de uso, agente y si la cotizacion es renovacion
	 * @param idNegocio
	 * @param idToProducto
	 * @param idTipoPoliza
	 * @param idToSeccion
	 * @param idPaquete
	 * @param idEstado
	 * @param idMunicipio
	 * @param idMoneda
	 * @param idTipoUso
	 * @param idAgente
	 * @param renovacion
	 * @return
	 */
	public List<NegocioCobPaqSeccion> getCoberturaNegocioConfig(Long idNegocio, BigDecimal idToProducto, BigDecimal idTipoPoliza, 
			BigDecimal idToSeccion, Long idPaquete, String idEstado, String idMunicipio, Short idMoneda, BigDecimal idTipoUso, BigDecimal idAgente, boolean renovacion);

}
