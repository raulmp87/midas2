package mx.com.afirme.midas2.service.suscripcion.cambiosglobales;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas2.domain.suscripcion.cambiosglobales.ConfiguracionPlantillaNeg;
import mx.com.afirme.midas2.domain.suscripcion.cambiosglobales.ConfiguracionPlantillaNegCobertura;

public interface ConfiguracionPlantillaService {

	/**
	 * Crea una plantilla nueva para la cotizacion, seccion y paquete
	 * seleccionada
	 * 
	 * @param idCotizacion
	 * @param idLinea
	 * @param idPaquete
	 * @param comentario
	 * @return retorna la plantilla creada. El listado de cobertura vendra
	 *         vacio.
	 */
	public ConfiguracionPlantillaNeg guardarConfiguracion(
			BigDecimal idCotizacion, BigDecimal idLinea, BigDecimal idPaquete,
			String comentario);
	
	
	/**
	 * Salva una plantilla para la cotizacion, seccion y paquete
	 * seleccionada
	 * 
	 * @param idCotizacion
	 * @param idLinea
	 * @param idPaquete
	 * @param comentario
	 * @return retorna la plantilla creada. El listado de cobertura vendra
	 *         vacio.
	 */
	public ConfiguracionPlantillaNeg guardarConfiguracion(ConfiguracionPlantillaNeg plantilla);

	/**
	 * Agrega una cobertura a una plantilla
	 * 
	 * @param idPlantilla
	 * @param idCobertura
	 * @param contratada
	 * @param sumaAsegurada
	 * @param deducible
	 */
	public void agregarCobertura(Long idPlantilla, BigDecimal idCobertura,
			Boolean contratada, Double sumaAsegurada, Double deducible);

	/**
	 * Obtiene una plantilla en caso de existir para la cotizacion, linea y
	 * paquete
	 * 
	 * @param idCotizacion
	 * @param idLinea
	 * @param idPaquete
	 * @return
	 */
	public ConfiguracionPlantillaNeg obtenerPlantilla(BigDecimal idCotizacion,
			BigDecimal idLinea, BigDecimal idPaquete);

	/**
	 * Busca una cobertura en la configuracion de plantillas para la cotizacion,
	 * linea, paquete y cobertura seleccionada
	 * 
	 * @param idCotizacion
	 * @param idLinea
	 * @param idPaquete
	 * @param idCobertura
	 * @return
	 */
	public ConfiguracionPlantillaNegCobertura obtenerCobertura(
			BigDecimal idCotizacion, BigDecimal idLinea, BigDecimal idPaquete,
			BigDecimal idCobertura);
	
	/**
	 * Busca una cobertura en la configuracion de plantillas para la cotizacion,
	 * linea, paquete (en el filtro) y cobertura seleccionada
	 * 
	 * @param idCotizacion
	 * @param idLinea
	 * @param idPaquete
	 * @param idCobertura
	 * @return
	 */
	public ConfiguracionPlantillaNegCobertura obtenerCobertura(
			ConfiguracionPlantillaNeg filtro, BigDecimal idCobertura);
	

	/**
	 * Busca la cobertura para la plantilla seleccionada
	 * 
	 * @param idPlantilla
	 * @return
	 */
	public ConfiguracionPlantillaNegCobertura obtenerCobertura(
			Long idPlantilla, BigDecimal idCobertura);

	/**
	 * Lista las coberturas dentro de una plantilla
	 * 
	 * @param idPlantilla
	 * @return
	 */
	public List<ConfiguracionPlantillaNegCobertura> obtenerCoberturas(
			Long idPlantilla);
	
	/**
	 * Obtiene el listado de coberturas disponibles para la cotizacion. Este listado regresa el set de coberturas
	 * por paquete junto con las modificaciones de coberturas encontradas dentro de la plantilla si es que existen
	 * @param idCotizacion
	 * @param idLinea
	 * @param idPaquete
	 * @return
	 */
	public List<CoberturaCotizacionDTO> listarCoberturasDisponiblesPlantilla(BigDecimal idCotizacion, BigDecimal idLinea, BigDecimal idPaquete);
	
	/**
	 * Metodo que reestablece las condiciones del negocio a un determinado
	 * paquete,linea y cotizacion
	 * 
	 * @param idCotizacion
	 * @param idLinea
	 * @param idPaquete
	 */
	public void reestablecerValoresDelNegocio(BigDecimal idCotizacion,
			BigDecimal idLinea, Long idPaquete);

	/**
	 * Metodo que aplica plantillas a los incisos de una cotizacion de acuerdo
	 * al paquete que se selleccione, si se envia un 0 en el aplicarAPaquete aplicara
	 * indistintamente a cualquier inciso capturada hasta el momento en la
	 * cotizacion
	 * 
	 * @param idToCotizacion
	 * @param idLinea
	 * @param idPaquete
	 * @param aplicarAPaquete
	 */
	public List<String> aplicarPlantilla(BigDecimal idToCotizacion, BigDecimal idLinea, Long idNegocioPaquete, Long aplicarAPaquete);

	/**
	 * Elimina todas las coberturas de una configuracion determinada
	 * 
	 * @param idCotizacion
	 * @param idLinea
	 * @param idPaquete
	 * @param idCobertura
	 * @return
	 */
	public void eliminarConfiguracionDeCoberturas(BigDecimal idCotizacion, BigDecimal idLinea, Long idPaquete);	
}
