package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.ws;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla.CotizarEndosoFlotillaRequest;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasiva.LogErroresCargaMasivaDTO;

@Local
public interface EndosoFlotillaService {
	
	public static final String ERROR = "ERROR";
	public static final String SUCCESS = "SUCCESS";
	
	public List<LogErroresCargaMasivaDTO> validaCargaMasiva(BitemporalCotizacion bitemporalCotizacion, CotizarEndosoFlotillaRequest request) throws IOException, SystemException;
	public void cancelarCotizacion(BitemporalCotizacion cotizacion);

}
