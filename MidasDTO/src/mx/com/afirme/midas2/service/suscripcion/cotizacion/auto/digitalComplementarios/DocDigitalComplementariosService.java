package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.digitalComplementarios;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.cotizacion.documento.DocumentoDigitalCotizacionDTO;


public interface DocDigitalComplementariosService {
	
	 public List<DocumentoDigitalCotizacionDTO> listarDocDigitalesCotizacion(BigDecimal idToCotizacion);
	 public DocumentoDigitalCotizacionDTO findById(BigDecimal idDocumentoDigitalCotizacion);
	 public void eliminaDocumentoDigitalCotizacionDTO(DocumentoDigitalCotizacionDTO documentoDigitalCotizacionDTO);
}
