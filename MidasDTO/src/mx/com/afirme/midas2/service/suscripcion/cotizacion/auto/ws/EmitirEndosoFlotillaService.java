package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.ws;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla.EmitirEndosoFlotillaRequest;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla.EmitirEndosoFlotillaResponse;

@Local
public interface EmitirEndosoFlotillaService {
	
	public static final String ERROR = "ERROR";
	public static final String SUCCESS = "SUCCESS";
	
	EmitirEndosoFlotillaResponse terminarCotizacion(EmitirEndosoFlotillaRequest request);
	EmitirEndosoFlotillaResponse complementarIncisos(EmitirEndosoFlotillaRequest request);
	EmitirEndosoFlotillaResponse emitir(EmitirEndosoFlotillaRequest request);

}
