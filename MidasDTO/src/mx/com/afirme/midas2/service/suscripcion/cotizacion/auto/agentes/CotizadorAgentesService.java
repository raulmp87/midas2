package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.agentes;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.agentes.VehiculoView;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoDocumentoFortimax;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.CaracteristicasVehiculoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.EsquemaPagoCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;

@Local
public interface CotizadorAgentesService {
	
	List<AgenteView> getAgentesPorDescripcion(String descripcionBusquedaAgente);
	
	Map<String,String > getEstiloPorMarcaNegocioyDescripcion(BigDecimal idTcMarcaVehiculo, 
			Short modeloVehiculo, BigDecimal idToNegSeccion, BigDecimal idMoneda, Long autoIncisoContinuityId, 
			String descripcion, BigDecimal idAgrupadorPasajeros);
	
	Map<String, Map<String, String>> getListasVehiculo(CotizacionDTO cotizacion, String idEstiloVehiculo, 
			Short idModeloVehiculo, BigDecimal idNegocioSeccion, String idToEstado);
	
	Long cambioNegocio(BigDecimal idNegocioSeccion);
	
	IncisoCotizacionDTO saveDatosVehiculo(VehiculoView vehiculo, IncisoCotizacionDTO incisoCotizacion, boolean isSave);
	
	List<CoberturaCotizacionDTO> mostrarCorberturaCotizacion(List<CoberturaCotizacionDTO> coberturaCotizacionBase, IncisoCotizacionDTO incisoCotizacion);
	
	List<EsquemaPagoCotizacionDTO> verEsquemaPagoAgente(CotizacionDTO cotizacion);
	
	/**
	 * @param cotizacion
	 * @return ResumenCostosDTO
	 */
	ResumenCostosDTO mostrarResumenTotalesCotizacionAgente(CotizacionDTO cotizacion);
	/**
	 * JFGG
	 * @param cotizacion
	 * @param statusExpetion
	 * @return ResumenCostosDTO
	 */
	ResumenCostosDTO mostrarResumenTotalesCotizacionAgente(CotizacionDTO cotizacion, boolean statusExpetion);

	List<CaracteristicasVehiculoDTO> mostrarOtrasCaracteristicas(String estiloSeleccionado, 
			String modificadoresDescripcion);
	
	IncisoAutoCot definirOtrasCaracteristicas(List<CaracteristicasVehiculoDTO> caracteristicasVehiculoList,
			String descripcionBase);
	
	List<CatalogoDocumentoFortimax> getListaDocumentosFortimax(Long idToPersonaContratante);

	Map<String, String> getURLFortimax(Long idToPersonaContratante);
	
	IncisoCotizacionDTO saveDatosComplementariosAsegurado(IncisoCotizacionDTO incisoCotizacion, 
			Integer saveAsegurado);
	
	List<CoberturaCotizacionDTO> verDetalleIncisoAgente(IncisoCotizacionDTO incisoCotizacion);
	
	IncisoCotizacionDTO saveDatosComplementarios(List<File> files, List<String> fileNames,
			List<String> contentTypes, IncisoCotizacionDTO incisoCotizacion);
	
	MensajeDTO validarListaParaEmitir(BigDecimal idToCotizacion);
	
	List<AgenteView> getAgentesPorPromotoria(Long idPromotoria);
	
	ClienteGenericoDTO getClienteGenerico(BigDecimal idPersona);
	
	String getValorParameter(BigDecimal idToGrupoParamGen, BigDecimal codigoParma);
	
	Agente getAgente(Long codigoAgente);
	
	String getBienAsegurado(IncisoCotizacionDTO incisoCotizacion);
	
	NegocioTipoPoliza getNegocioTipoPoliza(CotizacionDTO cotizacion);
	
	List<Negocio> getNegociosPorAgente(int codigoAgente, String claveNegocio);
	
	public NegocioDerechoPoliza getNegocioDerechoPoliza(Long idNegocio);
	
	NegocioDerechoPoliza getNegocioDerechoPoliza(Long idNegocio, BigDecimal idToCotizacion, BigDecimal numeroInciso);
	
	/**
	 * M\u00e9todo que realiza la consulta de los agentes apartir de 
	 * un agente tipo filtro.
	 * 
	 * @param filtroAgente Agente que contiene los parametros para su b\u00fasqueda
	 * 
	 * @return Agente que cumple dichos parametros.
	 */
	public Agente getAgente(Agente filtroAgente);
	
	Map<Object, String> obtenerDiasSalario();
	
	Short obtenerNumeroAsientos(IncisoCotizacionDTO incisoCotizacion);
	
	String setNombreAgenteCompleto(CotizacionDTO cotizacion);
	
	EstiloVehiculoDTO getEstiloVehiculo(String estiloId);
	
	Map<String, String> validaCotizacion(CotizacionDTO cotizacion, BigDecimal idTipoPoliza);
	
	CotizacionDTO crearCotizacion(CotizacionDTO cotizacionDto, BigDecimal numeroInciso);
	
	void generaAnexosYCondiciones(BigDecimal idToCotizacion);
}
