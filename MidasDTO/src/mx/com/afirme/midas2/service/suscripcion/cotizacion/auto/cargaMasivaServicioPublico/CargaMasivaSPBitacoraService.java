package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasivaServicioPublico;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.BitacoraCargaMasivaSPFiltradoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.BitacoraCargaMasivaSPResultadoDTO;

@Local
public interface CargaMasivaSPBitacoraService {

	public Long conteoBitacoraCargaMasivaService(
			BitacoraCargaMasivaSPFiltradoDTO filtroBitacora);

	public List<BitacoraCargaMasivaSPResultadoDTO> buscarBitacora(
			BitacoraCargaMasivaSPFiltradoDTO filtroBitacora);

}
