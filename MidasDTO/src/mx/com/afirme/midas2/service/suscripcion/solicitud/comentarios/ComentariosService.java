package mx.com.afirme.midas2.service.suscripcion.solicitud.comentarios;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.comentarios.Comentario;


public interface ComentariosService {
	
	public void guardarComentario(Comentario comentario);
	
	public List<Comentario> getComentarioPorSolicitud(BigDecimal idSolicitud);
	
	public void eliminarComentario(Comentario comentario);
	
	public void guardarComentario(String comentario,SolicitudDTO solicitud);
	
	public void copiarComentarios(BigDecimal idSolicitudBase,
			BigDecimal idSolicitudCopia);
	
	public void actualizaEstatusSolicitudPorComentario(BigDecimal idSolicitud);
	
	public String validaComentarioSolicitudCotizacion(String valor, Short tipoComentario);
	
	public void guardarComentarioCoberturaAdicional(Comentario comentario);
}