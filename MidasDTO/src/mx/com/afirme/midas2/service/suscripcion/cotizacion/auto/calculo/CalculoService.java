package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionDescId;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosGeneralDTO;

import org.joda.time.DateTime;

@Local
public interface CalculoService {

	/**
	 * Metodo que permite calcular  a las condiciones que se encuentran almacenadas
	 * para un grupo de coberturas que le corresponden a un inciso
	 * de acuerdo al negocio, el tipo de poliza, la linea de negocio o seccion,
	 * el paquet, estado, municipio, cotizacion, moneda, numero de secuencia (inciso),
	 * clave de estilo vehiculo, modelo vehiculo, tipo de uso vehiculo, modificadores de prima y
	 * modificadores de descripcion.
	 * 
	 * @param IdNegocio
	 * @param idToProducto
	 * @param idTipoPoliza
	 * @param idToSeccion
	 * @param idPaquete
	 * @param idEstado
	 * @param idMunicipio
	 * @param idCotizacion
	 * @param idMoneda
	 * @param numeroSecuenciaInciso
	 * @param claveEstilo
	 * @param modeloVehiculo
	 * @param tipoUso
	 * @param modificadoresPrima
	 * @param modificadoresDescripcion
	 * @return List<CoberturaCotizacionDTO>
	 */
	public List<CoberturaCotizacionDTO> calculoOnFly(Long IdNegocio,BigDecimal idToProducto,
			BigDecimal idTipoPoliza, BigDecimal idToSeccion, Long idPaquete,
			String idEstado, String idMunicipio, BigDecimal idCotizacion, Short idMoneda, Long numeroInciso, String claveEstilo, Long modeloVehiculo,
			BigDecimal tipoUso, String modificadoresPrima, String modificadoresDescripcion, List<CoberturaCotizacionDTO> coberturas);

	/**
	 * Metodo que permite calcular una cotizacion de acuerdo a las condiciones
	 * que se encuentren almacenadas
	 * 
	 * @param cotizacionDTO
	 * @return CotizacionDTO
	 */
	public CotizacionDTO calcular(CotizacionDTO cotizacionDTO);
	
	public void calcularEndoso(BitemporalCotizacion bitemporalCotizacion, DateTime validoEn, Integer tipoEndoso);
	
	/**
	 * Metodo que permite calcular un inciso de acuerdo a las condiciones que se
	 * encuentren almacenadas
	 * 
	 * @param incisoCotizacionDTO
	 * @return IncisoCotizacionDTO
	 */
	public IncisoCotizacionDTO calcular(IncisoCotizacionDTO incisoCotizacionDTO);
	
	public BitemporalInciso calcular(BitemporalInciso inciso, DateTime validoEn, Integer tipoEndoso, boolean esIgualacion);
	
	public ResumenCostosDTO obtenerResumen(IncisoCotizacionDTO incisoCotizacionDTO);
	/**
	 * JFGG
	 * @param cotizacion
	 * @param esComplementar
	 * @return ResumenCostosDTO
	 */
	public ResumenCostosDTO obtenerResumenCotizacion(CotizacionDTO cotizacion, Boolean esComplementar);
	/**
	 * JFGG
	 * @param cotizacion
	 * @param esComplementar
	 * @param statusExcepcion
	 * @return ResumenCostosDTO
	 */
	public ResumenCostosDTO obtenerResumenCotizacion(CotizacionDTO cotizacion, Boolean esComplementar, boolean statusExcepcion);
	
//	public ResumenCostosDTO obtenerResumenCotizacionEndoso(Integer numeroCotizacion, DateTime validoEn, boolean cotizacionEnProceso);
//	
//	public ResumenCostosDTO obtenerResumenCotizacionEndoso(Integer numeroCotizacion, DateTime validoEn, boolean cotizacionEnProceso, 
//			boolean esEndosoMovimientos);
//	
	public ResumenCostosGeneralDTO obtenerResumenGeneral(CotizacionDTO cotizacion);
	
	/**
	 * Metodo que permite calcular el resumen de costos para la impresion de la cotizacion
	 * @param cotizacion
	 * @param esComplementar
	 * @return ResumenCostosGeneralDTO
	 */
	public ResumenCostosGeneralDTO obtenerResumenGeneral(CotizacionDTO cotizacion, Boolean esComplementar);
	
//	public ResumenCostosGeneralDTO obtenerResumenGeneralEndoso(Integer numeroCotizacion, DateTime validoEn, 
//			boolean cotizacionEnProceso);
//	
//	public ResumenCostosGeneralDTO obtenerResumenGeneralEndoso(Integer numeroCotizacion, DateTime validoEn, 
//			boolean cotizacionEnProceso, boolean esEndosoMovimientos);
//	
//	public ResumenCostosGeneralDTO obtenerResumenGeneralEndoso(Integer numeroCotizacion, DateTime validoEn,
//			DateTime recordFrom, 
//			boolean cotizacionEnProceso, Short claveTipoEndoso);
//	
//	public ResumenCostosGeneralDTO obtenerResumenGeneralEndoso(Integer numeroCotizacion, DateTime validoEn,
//			DateTime recordFrom, 
//			boolean cotizacionEnProceso, boolean esEndosoMovimientos, Short claveTipoEndoso);
//	
//	public ResumenCostosDTO obtenerResumenGeneralEndosoCancelado(Integer numeroCotizacion, DateTime validoEn, DateTime recordFrom);
//	
//	public ResumenCostosDTO obtenerResumenGeneralEndosoCancelado(Integer numeroCotizacion, DateTime validoEn, DateTime recordFrom, boolean esConsulta);
	//////////////////////////////
	
	/**
	 * Metodo que sobrecarga la obtencion de costos emitidos
	 * @param idTopoliza
	 */
	public ResumenCostosDTO resumenCostosEmitidos(BigDecimal idToPoliza, Short numeroendoso, Integer numeroInciso);
	
	/**
	 * Metodo que sobrecarga la obtencion de resumenes de Póliza emitida
	 * @param idTopoliza
	 */
	public ResumenCostosDTO resumenCostosCaratulaPoliza(BigDecimal idToPoliza);
	
	
	/**
	 * Metodo que sobrecarga la obtencion de resumenes de Inciso emitido
	 * @param idTopoliza
	 * @param numeroInciso
	 */
	public ResumenCostosDTO resumenCostosIncisoPoliza(BigDecimal idToPoliza, BigDecimal numeroInciso);
	
	/**
	 * Metodo que sobrecarga la obtencion de resumenes de Inciso emitido
	 * @param idTopoliza
	 * @param numeroInciso
	 * @param numeroEndoso
	 */
	public ResumenCostosDTO resumenCostosIncisoPoliza(BigDecimal idToPoliza, BigDecimal numeroInciso, Short numeroEndoso);
	
	
	/**
	 * Metodo que sobrecarga la obtencion de resumenes de Endoso emitido
	 * @param idTopoliza
	 * @param numeroEndoso
	 */
	public ResumenCostosDTO resumenCostosEndoso(BigDecimal idToPoliza, Short numeroEndoso);
	
	/**
	 * Metodo que sobrecarga la obtencion de resumenes de Endoso emitido
	 * @param idTopoliza
	 * @param numeroEndoso
	 * @param esConsultaPoliza
	 */
	public ResumenCostosDTO resumenCostosEndoso(BigDecimal idToPoliza, Short numeroEndoso, boolean esConsultaPoliza);
	
	
	/**
	 * Metodo que sobrecarga la obtencion de resumenes de cotización endoso
	 * @param numeroCotizacion
	 * @param validoEn
	 */
	public ResumenCostosDTO resumenCotizacionEndoso(Integer numeroCotizacion, DateTime validoEn);
	///////////////////////////////
	
	/**
	 * Igualar prima total de una cotizacion al valor dado
	 * @param idToCotizacion
	 * @param primaTotal
	 * @param restaurarDescuento
	 */
	public void igualarPrima(BigDecimal idToCotizacion, Double primaTotal, Boolean restaurarDescuento);
	
	/**
	 * Igualar prima total de un inciso de la cotizacion
	 * @param idToCotizacion
	 * @param primaTotal
	 */
	public void igualarPrimaInciso(BigDecimal idToCotizacion, BigDecimal numeroInciso, Double primaTotal);
	
	/**
	 * Igualar prima total de una cotizacion al valor dado, metodo creado para endosos bitemporales
	 * @param idToCotizacion
	 * @param primaTotalAIgualar
	 */
	public void igualarPrimaEndoso(Long idToCotizacion, BigDecimal primaTotalAIgualar);
	
	/**
	 * Igualacion de primas a nivel de inciso en un endoso (con bitemporalidad)
	 * @param cotizacionContinuityId
	 * @param numeroInciso
	 * @param primaTotalAIgualar
	 * @param validoEn
	 */
	public void igualarPrimaIncisoEndoso(final Long incisoContinuityId, Double primaTotalAIgualar, DateTime validoEn, Integer tipoEndoso);
	
	/**
	 * Eliminar descuentos para igualacion de primas de una cotizacion y calcularla de nuevo 
	 * @param idToCotizacion
	 */
	public void restaurarPrimaCotizacion(BigDecimal idToCotizacion);
	
	/**
	 * Eliminar descuentos para igualacion de primas de un endoso y calcularla de nuevo 
	 * @param idToCotizacion
	 */
	public void restaurarPrimaEndoso(BigDecimal idToCotizacion, DateTime validoEn, Integer tipoEndoso);
	
	/**
	 * Eliminar descuentos para igualacion de primas de un inciso durante un endoso 
	 * @param BitemporalInciso
	 * @param DateTime
	 */
	public void restaurarPrimaIncisoEndoso(BitemporalInciso inciso, DateTime validoEn, Integer tipoEndoso, boolean esIgualacion);
	
	public Double obtenerPrimaTotalAltaInciso(BitemporalInciso inciso,
			CotizacionContinuity cotizacionContinuity, Date validoEn, short tipoEndoso);
	
	/**
	 * Elimina la estructura de descuento para un inciso
	 * @param idTipoDescuento
	 * @param inciso
	 */
	public void eliminarDescuentoCobertura(Long idTipoDescuento, IncisoCotizacionDTO inciso);
	
	/**
	 * Eliminar todos los descuentos asignados a un inciso
	 * @param idTipoDescuento
	 * @param inciso
	 */
	public void eliminarDescuentos(IncisoCotizacionDTO inciso);
	
	/**
	 * Agrega un descuento para las coberturas propias de un inciso
	 * @param idTipoDescuento
	 * @param inciso
	 * @param descuentoAplicar
	 */
	public void agregarDescuentoCobertura(Long idTipoDescuento, IncisoCotizacionDTO inciso, BigDecimal descuentoAplicar);
	
	public void generarCoberturasInProcessParaIgualacionPrimas(Long incisoContinuityId, DateTime validoEn);
	
	/**
	 * Eliminar todos los recargos asignados a un inciso
	 * @param idTipoDescuento
	 * @param inciso
	 */
	public void eliminarRecargos(IncisoCotizacionDTO inciso);
	
	public void restaurarPrimaInciso(IncisoCotizacionDTO inciso);
	
	/**
	   * @deprecated No usar para desarrollos futuros.
	   */
	@Deprecated
	public void setIdSubRamoC(BigDecimal idSubRamoC);
	
	public void restaurarDescuentoPorEstado(BigDecimal idToCotizacion, Double descuentoPorNoSiniestro, NegocioRenovacionDescId id, boolean renovarEmitir);

	public BigDecimal getPorcentajeRecargoFraccionado(CotizacionDTO cotizacion);
	
	public Double calculaPorcentajeDescuentoGlobal(BigDecimal idToCotizacion);
	
	public void restaurarPrimaEndosoMovimiento(BigDecimal idToCotizacion);
	
	public ControlEndosoCot obtenerControlEndosoCotizacion(Integer numeroCotizacion);
	
	public void ajusteIgualacion(BigDecimal idToCotizacion, BigDecimal numeroInciso, Double primaTotal);
}
