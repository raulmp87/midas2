package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBaseAuto;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.TarifaAutoProliberCombo;

public interface ConfiguracionIncisoGrupoProliberAVService extends
		MidasInterfaceBaseAuto<TarifaAutoProliberCombo> {

}
