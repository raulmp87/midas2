package mx.com.afirme.midas2.service.suscripcion.solicitud;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.solicitud.SolicitudDataEnTramiteDTO;

public interface SolicitudDataEnTramiteService {

	/**
	 * Perform an initial save of a previously unsaved SolicitudDataEnTramiteDTO entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            SolicitudDataEnTramiteDTO entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public SolicitudDataEnTramiteDTO save(SolicitudDataEnTramiteDTO entity);
	
	public void save(BigDecimal idToSolicitud, List<SolicitudDataEnTramiteDTO> entity);

	/**
	 * Delete a persistent SolicitudDataEnTramiteDTO entity.
	 * 
	 * @param entity
	 *            SolicitudDataEnTramiteDTO entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(SolicitudDataEnTramiteDTO entity);

	/**
	 * Persist a previously saved SolicitudDataEnTramiteDTO entity and return it or a copy of
	 * it to the sender. A copy of the SolicitudDataEnTramiteDTO entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            SolicitudDataEnTramiteDTO entity to update
	 * @return SolicitudDataEnTramiteDTO the persisted SolicitudDataEnTramiteDTO entity instance, may not
	 *         be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public SolicitudDataEnTramiteDTO update(SolicitudDataEnTramiteDTO entity);

	public SolicitudDataEnTramiteDTO findById(BigDecimal id);

	/**
	 * Find all SolicitudDataEnTramiteDTO entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SolicitudDataEnTramiteDTO property to query
	 * @param value
	 *            the property value to match
	 * @return List<SolicitudDataEnTramiteDTO> found by query
	 */
	public List<SolicitudDataEnTramiteDTO> findByProperty(String propertyName, Object value);
	
	public void deleteBySolicitud(BigDecimal idToSolicitud);
	
	public List<SolicitudDataEnTramiteDTO> findBySolicitudAndSerie(Long idToSolicitud,String numeroSerie);
	
	public InputStream descargarExcelCartaCobertura(BigDecimal idToSolicitud) throws IOException;
}
