package mx.com.afirme.midas2.service.suscripcion.emision;

import java.math.BigDecimal;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;


public interface EmisionService {
	public Map<String, String> emitir(CotizacionDTO cotizacion, boolean esRenovacion);
	
	public void validacionPreviaRecibos(BigDecimal idToCotizacion) throws RuntimeException;
	
	public void setDatosTarjetaEmision(String fechaVencimiento, String codigoSeguridad);
	
	public String setFunctionPrintPoliza(BigDecimal idToPoliza);
}