package mx.com.afirme.midas2.service.suscripcion.endoso;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import javax.ejb.Local;

import org.joda.time.DateTime;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.dto.impresiones.ImpresionEndosoDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;

@Local
public interface ImpresionEndosoService {
	public ImpresionEndosoDTO imprimirEndosoM2(BigDecimal idToCotizacion, Date recordFrom, Short claveTipoEndoso);
	
	/**
	 * Si la bandera es true, imprime un endoso con la informacion que se modifico en el modulo de edicion de endosos
	 * @param idToCotizacion
	 * @param recordFrom
	 * @param claveTipoEndoso
	 * @param imprimirEdicionEndoso
	 * @return
	 */
	public ImpresionEndosoDTO imprimirEndosoM2(BigDecimal idToCotizacion, Date recordFrom, Short claveTipoEndoso, boolean imprimirEdicionEndoso);
	
	public String obtenerTextoEndoso(ControlEndosoCot controlEndosoCot, PolizaDTO polizaDTO, BitemporalCotizacion bCotizacion,
			 						DateTime validFrom, DateTime recordFrom, BitemporalInciso bitemporalInciso);
	
	public TransporteImpresionDTO imprimirDesgloseCoberturas(BitemporalCotizacion cotizacion, String incisoContinuityId, PolizaDTO poliza, 
			ResumenCostosDTO resumen, Date validoEn);
	
	/**
	 * Recupera la informacion del endoso que se desea imprimir
	 * @param idToCotizacion
	 * @param recordFrom
	 * @param claveTipoEndoso
	 * @return
	 */
	public Map<String,Object> obtenerInformacionImpresionEndoso(BigDecimal idToCotizacion, Date recordFrom, Short claveTipoEndoso);
	
}
