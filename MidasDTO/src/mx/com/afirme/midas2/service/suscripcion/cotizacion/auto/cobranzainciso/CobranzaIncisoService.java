package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobranzainciso;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.complementar.cobranzaInciso.CobranzaIncisoDTO;

public interface CobranzaIncisoService {

	public Long getCountCobranzaIncisos(IncisoCotizacionDTO inciso);
	
	public List<CobranzaIncisoDTO> getCobranzaIncisos(IncisoCotizacionDTO inciso); 
	
	public void aplicarCobranzaIncisos(List<IncisoCotizacionDTO> incisos, 
			BigDecimal idToCotizacion, BigDecimal idMedioPago, Long idConductoCobro, Boolean todos);
	
	public void aplicarCobranzaAseguradoIncisos(List<IncisoCotizacionDTO> incisos);
	
	public Map<BigDecimal, String> getClientesCobro(BigDecimal idContrantante, BigDecimal idAsegurado);
	
	public List<MedioPagoDTO> getMedioPagosView();
	
	public Boolean habilitaCobranzaInciso(BigDecimal cotizacionId);
	
	public void limpiarCobranzaIncisos(BigDecimal idToCotizacion);
	
	public void validarCambioContratanteCobranzaInciso(BigDecimal idToCotizacion);
	
	public void validarCambioAseguradoCobranzaInciso(BigDecimal idToCotizacion, BigDecimal numeroInciso);
	
	public Map<BigDecimal, String> getClientesCobroCotizacion(BigDecimal idCotizacion);
}
