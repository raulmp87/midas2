package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.CaracteristicasVehiculoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.IncisoNumeroSerieDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;

public interface IncisoService {
	public List<CaracteristicasVehiculoDTO> listarCaracteristicas(String modificadoresDescripcion);

	public IncisoCotizacionDTO guardar(IncisoCotizacionDTO incisoCotizacionDTO);

	public IncisoAutoCot definirOtrasCaract(
			List<CaracteristicasVehiculoDTO> caracteristicasVehiculoList, String descripcionBase);

	public void multiplicarInciso(IncisoCotizacionId incisoCotizacionId,
			int numeroCopias);

	public IncisoCotizacionDTO copiarInciso(IncisoCotizacionDTO incisoBase,
			BigDecimal idCotizacionDestino);

	public List<IncisoCotizacionDTO> getIncisos(BigDecimal IdToCotizacion);

	public List<IncisoCotizacionDTO> listarIncisosConFiltro(IncisoCotizacionDTO filtro);

	public Long listarIncisosConFiltroCount(IncisoCotizacionDTO filtro);

	public List<IncisoCotizacionDTO> findByCotizacionId(BigDecimal idToCotizacion);
	
	public List<IncisoCotizacionDTO> findNoAsignadosByCotizacionId(BigDecimal idToCotizacion);

	public Long borrarInciso(IncisoCotizacionDTO incisoCotizacionDTO);

	public Long borrarInciso(BigDecimal idToCotizacion, BigDecimal numeroInciso);
	
	public void actualizaNumeroSecuencia(BigDecimal idToCotizacion,
			Long numeroSecuencia);

	public IncisoCotizacionDTO prepareGuardarInciso(BigDecimal idToCotizacion,
			IncisoAutoCot incisoAutoCot,
			IncisoCotizacionDTO incisoCotizacionDTO,
			List<CoberturaCotizacionDTO> coberturaCotizacionList, Double valorPrimaNeta, 
			Double valorSumaAsegurada, Short claveObligatoriedad, Short claveContrato);
	
	public IncisoCotizacionDTO prepareGuardarIncisoAgente(BigDecimal idToCotizacion,
			IncisoAutoCot incisoAutoCot,
			IncisoCotizacionDTO incisoCotizacionDTO,
			List<CoberturaCotizacionDTO> coberturaCotizacionList, Double valorPrimaNeta, Double valorSumaAsegurada, 
			Short claveObligatoriedad, Short claveContrato);
	
	public IncisoCotizacionDTO prepareGuardarInciso(BigDecimal idToCotizacion,
			IncisoAutoCot incisoAutoCot,
			IncisoCotizacionDTO incisoCotizacionTem,
			List<CoberturaCotizacionDTO> coberturaCotizacionList);

	public void copiarEstructura(IncisoCotizacionDTO incisoNuevo,
			IncisoCotizacionDTO incisoBase);

	/**
	 * Guardar inciso auto cot
	 * 
	 * @param inciso
	 */
	public void guardarIncisoAutoCot(IncisoAutoCot inciso);

	/**
	 * Complementa los datos del inciso (numMotor, numSerie, descripcion final,
	 * repuve, placas)
	 * 
	 * @param inciso
	 */
	public void complementarDatosInciso(IncisoCotizacionDTO inciso);

	/**
	 * Obtiene un IncisoAutoCot
	 * 
	 * @param id
	 * @return IncisoAutoCot
	 * @author martin
	 */
	public IncisoAutoCot obtenerIncisoAutoCotPorId(Long id);

	public List<IncisoCotizacionDTO> getIncisosPorLineaPaquete(
			BigDecimal cotizacionId, BigDecimal lineaId, Long paqueteId);

	public List<IncisoCotizacionDTO> getIncisosPorLinea(
			BigDecimal cotizacionId, BigDecimal lineaId);

	public void cambiarPaquete(BigDecimal idToCotizacion, BigDecimal idInciso, Long idToNegPaqueteSeccion);
	
	public void cambiarPaquete(BigDecimal idToCotizacion, BigDecimal idInciso, Long idToNegPaqueteSeccion, List<CoberturaCotizacionDTO> coberturas);
	
	/*public void cambiarPaqueteActualizarCoberturas(IncisoCotizacionDTO inciso,
			Long idToNegPaqueteSeccion, List<CoberturaCotizacionDTO> coberturas);*/

	public ResumenCostosDTO obtenerResumenInciso(BigDecimal idToCotizacion,
			IncisoAutoCot configAuto, IncisoCotizacionDTO incisoCotizacionDTO,
			List<CoberturaCotizacionDTO> coberturaCotizacionList);

	public void guardarDatosAdicionalesPaquete(Map<String, String> datosRiesgo,
			BigDecimal negocioSeccionId, BigDecimal cotizacionId,
			BigDecimal numeroInciso);

	public Long maxSecuencia(BigDecimal idToCotizacion);

	/**
	 * Valida si esta presente o no la cobertura AUT RES - Responsabilidad Civil
	 * en USA Y CANADA LUC en el inciso
	 * 
	 * @param IncisoCotizacionDTO
	 * @return Boolean
	 * @author eric
	 */
	public Boolean infoConductorEsRequerido(
			IncisoCotizacionDTO incisoCotizacionDTO);
	
	
	public IncisoCotizacionDTO update(IncisoCotizacionDTO incisoCotizacionDTO);
	
	public void borrarIncisoAutoCotNoAsignados(BigDecimal idToCotizacion);
	
	public List<CoberturaCotizacionDTO> obtenerCoberturasParaLaConfiguracion(BigDecimal idToCotizacion, 
			BigDecimal idInciso, Long idToNegPaqueteSeccion);
	
	public List<IncisoNumeroSerieDTO> numerosSerieRepetidos(BigDecimal idToCotizacion, BigDecimal numeroInciso, 
			String numeroSerie, Date fechaIniVigencia, Date fechaFinVigencia);
	
	public boolean existenNumeroSerieRepetidos(BigDecimal idToCotizacion, BigDecimal numeroInciso, 
			String numeroSerie, Date fechaIniVigencia, Date fechaFinVigencia);
	
	public Long borrarIncisoMasivo(IncisoCotizacionDTO incisoCotizacionDTO);
	
	public Double getValorPrimaTotal(IncisoCotizacionDTO incisoCotizacionDTO);
	
	public String getModificadoresDescripcionDefautlt(EstiloVehiculoDTO estilo);
	
	public Boolean observacionesEsRequerido(IncisoCotizacionDTO incisoCotizacionDTO);
	
	public IncisoCotizacionDTO getIncisoCoberturaCotizacionesInicializadas(BigDecimal idToCotizacion, BigDecimal numeroInciso);
	
	public IncisoCotizacionDTO copiarInciso(IncisoCotizacionDTO incisoBase, BigDecimal idCotizacionDestino, Long numero);
	
	public void eliminarDatosRiesgo(IncisoCotizacionDTO inciso);

	public List<IncisoCotizacionDTO> getIncisosRango(BigDecimal idToCotizacion, BigDecimal incisoInicial, BigDecimal incisoFinal);
	
	public void guardarConductoCobroCotizacionAInciso(CotizacionDTO cotizacionDTO);
}
