package mx.com.afirme.midas2.service.suscripcion.pago;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dto.pago.ConfiguracionPagoDTO;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO;
import mx.com.afirme.midas2.dto.pago.ReciboDTO;
import mx.com.afirme.midas2.dto.pago.RespuestaIbsDTO;

import mx.com.afirme.midas2.dto.pago.RespuestaAmexDTO;
import org.joda.time.DateTime;

@Local
public interface PolizaPagoService {
	
	public static final int MONEDA_MXP = 484;
	
	public static final int FORMA_PAGO_ANUAL = 1;
	
	public static final double RECIBO_MINIMO_PAGO_ANUAL = 1000d;
	
	public static final String	TIENE_ROL_EJECUTIVO_SUCURSAL = "Rol_M2_Ejecutivo_Sucursales";//Rol_Subdirector_Rea
	
	public static final String	NO_TIENE_ROL_EJECUTIVO = "NINGUNO"; 
	
	/**
	 * Get only data from current customer's policy through idCliente. By joksrc
	 * @return List<String>
	 */
	public CuentaPagoDTO obtenerDatosTitularSiEsIgualDatosCotizacion(BigDecimal clienteId) throws Exception;
	
	/**
	 * Charge executives list invoking a service. It must be loaded and showed on CotizadorAutos into COMPRAR tab. By joksrc
	 * @return List<String>
	 */
	public Map<String, String> cargarListaEjecutivosPorUsuario(String usuario);
	
	/**
	 * Proof if the current user has an executive role, so then returns current username, otherwise returns the NINGUNO word. By joksrc
	 * @return String
	 */
	public String validarUsuarioActualSiEsEjecutivoOrSucursal();
	
	/**
	 * Aplica el pago para un recibo
	 * @param pagoDTO
	 * @param recibo
	 * @return
	 * @throws Exception
	 */
	RespuestaIbsDTO aplicarPago(ConfiguracionPagoDTO pagoDTO, ReciboDTO recibo) throws Exception;
	
	/**
	 * Aplicar pago para una p�liza nueva
	 * @param idToCotizacion
	 * @return
	 */
	RespuestaIbsDTO aplicarPago(BigDecimal idToCotizacion) throws Exception;


	/**
	 * Aplica el pago para un recibo configurado para pago con conducto de cobro American Express
	 * @param pagoDTO
	 * @param recibo
	 * @return
	 * @throws Exception
	 */
	public RespuestaAmexDTO aplicarPagoAmex(ConfiguracionPagoDTO pagoDTO, ReciboDTO recibo) throws Exception;
	
	/**
	 * Aplicar pago para una p�liza nueva configurado para pago con conducto de cobro American Express
	 * @param idToCotizacion
	 * @param fechaVencimiento
	 * @param codigoSeguridad
	 * @return
	 */
	public RespuestaAmexDTO aplicarPagoAmex(BigDecimal idToCotizacion,String fechaVencimiento,String codigoSeguridad) throws Exception;
	
	/**
	 * Consultar Saldo de una configuracion de pago
	 * @param pagoDTO
	 * @param recibo
	 * @return
	 */
	RespuestaIbsDTO consultarSaldo(BigDecimal idToCotizacion) throws Exception;
	
	/**
	 * Realizar el cambio de conducto de cobro un p�liza existente
	 * @param idToCotizacion
	 * @return
	 */
	RespuestaIbsDTO cambiarConductoCobro(BigDecimal idToCotizacion);
	
	
	/**
	 * Obtener conducto de cobro de la cotizacion. Si no tiene ningun conducto de cobro asociado, devolvera la informacion
	 * del contratante
	 * @param pagoDTO
	 */
	CuentaPagoDTO obtenerConductoCobro(BigDecimal idToCotizacion) throws Exception;
	
	
	/**
	 * Obtener conducto de cobro especifico para un cliente
	 * @param pagoDTO
	 */
	CuentaPagoDTO obtenerConductoCobro(BigDecimal idToCliente, Long idConducto) throws Exception;
	
	/**
	 * Obtiene la informacion del contratante como si fuera un objeto cuenta pago
	 * @param idToCliente
	 * @param idDomicilio
	 * @return
	 * @throws Exception
	 */
	CuentaPagoDTO obtenerConductoCobro(BigDecimal idToCliente, BigDecimal idDomicilio) throws Exception;
	
	/**
	 * Obtener conductos de cobro para un cliente y un tipo en particular
	 * @param pagoDTO
	 */
	List<CuentaPagoDTO> obtenerConductosCobro(BigDecimal idCliente, CuentaPagoDTO.TipoCobro tipoCobro) throws Exception;
	
	
	/**
	 * Guardar el conducto de cobro para una cotizacion
	 * @param pagoDTO
	 */
	void guardarConductoCobro(BigDecimal idToCotizacion, CuentaPagoDTO pagoDTO) throws Exception;
	
	Long guardarConductoCobro(BigDecimal idToCotizacion, CuentaPagoDTO pagoDTO, Usuario usuario, boolean esEndoso) throws Exception;
	
	/**
	 * Obtener un conducto cobro
	 * @param cotizacionContinuityId
	 * @param validoEn
	 * @return
	 * @throws Exception
	 */
	CuentaPagoDTO obtenerConductoCobro(Long cotizacionContinuityId, DateTime validoEn) throws Exception;
	
	/**
	 * Obtiene el monto del primer recibo
	 * @param idToCotizacion
	 * @param idToSeccion
	 * @param idToInciso
	 * @return
	 */
	double obtenerMontoReciboInicial(BigDecimal idToCotizacion, BigDecimal idToSeccion, BigDecimal idToInciso);
	
	RespuestaIbsDTO consultarSaldoIncisos(BigDecimal idToCotizacion) throws Exception;
	
	void guardarConductoCobroCotizacionAInciso(BigDecimal idToCotizacion);
	
	RespuestaIbsDTO aplicarPagoInciso(BigDecimal idToCotizacion) throws Exception;

	public RespuestaAmexDTO aplicarPagoIncisoAmex(BigDecimal idToCotizacion) throws Exception;
	
	Long guardarDatosCobranzaCliente(CuentaPagoDTO cuenta) throws Exception;
	
	CuentaPagoDTO obtenerConductoCobroCliente(BigDecimal idToCliente) throws Exception;
	
	public void setDatosTarjetaEmision(String fechaVencimiento, String codigoSeguridad);
}
