package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.emision;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.cargos.ConfigCargos;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.dto.reportesAgente.RetencionImpuestosReporteView;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesCompDTO.DatosReporteCompParametrosDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesDTO.DatosReporteEstadoCuentaParametrosDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesDerPolDTO.DatosRepDerPolParametrosDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesPagDevengarDTO.DatosReportePagDevDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesPagoCompContra.DatosReportePagoCompContraParametrosDTO;
import mx.com.afirme.midas2.domain.prestamos.ConfigPrestamoAnticipo;
import mx.com.afirme.midas2.domain.prestamos.PagarePrestamoAnticipo;
import mx.com.afirme.midas2.domain.provisiones.ProvisionImportesRamo;
import mx.com.afirme.midas2.dto.fuerzaventa.ReporteDetalleProvisionView;
import mx.com.afirme.midas2.dto.fuerzaventa.ReporteProvisionView;
import mx.com.afirme.midas2.dto.impresiones.ContenedorDatosImpresion;
import mx.com.afirme.midas2.dto.impresiones.DatosBasesEmisionDTO.DatosBasesEmisionParametrosDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosCaratulaCotizacion;
import mx.com.afirme.midas2.dto.impresiones.DatosCaratulaInciso;
import mx.com.afirme.midas2.dto.impresiones.DatosCaratulaPoliza;
import mx.com.afirme.midas2.dto.impresiones.DatosNegocioDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasistemasEnvio;
import mx.com.afirme.midas2.dto.sapamis.otros.SapAmisBitacoraEmision;
import mx.com.afirme.midas2.dto.sapamis.otros.SapAmisBitacoraSiniestros;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgenteBonosGerenciaView;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgenteContabilidadMMView;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgenteDatosView;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgenteEstatusEntregaFactura;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgenteIngresosView;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgentePrimaEmitidaVsPriamaPagadaDTO;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgentePrimaPagadaPorRamoView;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgenteTopAgenteView;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgentesDetalleBonoView;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgentesReporteProduccion;
import mx.com.afirme.midas2.dto.reportesAgente.DatosDetPrimas;
import mx.com.afirme.midas2.dto.reportesAgente.DatosEdoctaAgenteAcumuladoView;
import mx.com.afirme.midas2.dto.reportesAgente.DatosEdoctaAgenteProduccion;
import mx.com.afirme.midas2.dto.reportesAgente.DatosEdoctaAgenteSiniestralidad;
import mx.com.afirme.midas2.dto.reportesAgente.DatosHonorariosAgentes;
import mx.com.afirme.midas2.dto.reportesAgente.DatosReporteCalculoBonoMensualDTO;
import mx.com.afirme.midas2.dto.reportesAgente.DatosReportePreviewBonos;
import mx.com.afirme.midas2.dto.reportesAgente.DatosVentasProduccion;
import mx.com.afirme.midas2.dto.reportesAgente.ReporteGlobalBonoYComisionesDTO;
import mx.com.afirme.midas2.dto.reportesAgente.SaldosVsContabilidadView;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

@Local
public interface ImpresionesService {
	public static enum TipoContrato {
		PROMOTORIA(40L), AGENTE(50L);
		private Long value;

		private TipoContrato(Long value) {
			this.value = value;
		}

		public Long getValue() {
			return value;
		}
	}

	DatosCaratulaPoliza llenarPolizaDTO(BigDecimal idToPoliza,
			int incisoInicial, int incisoFinal);

	List<DatosCaratulaInciso> llenarIncisoDTO(CotizacionDTO cotizacionDTO,
			String numeroPoliza, ContenedorDatosImpresion contenedorImpresion);

	List<DatosCaratulaInciso> llenarIncisoDTO(CotizacionDTO cotizacionDTO,
			String numeroPoliza, int incisoInicial, int incisoFinal,
			ContenedorDatosImpresion contenedorImpresion);

	DatosCaratulaCotizacion llenarCotizacionDTO(BigDecimal idToCotizacion);

	List<DatosAgenteIngresosView> llenarDatosAgenteIngresos(String anio,
			String mes, Long rangoInicio, Long rangoFin, Integer tipoReporte);

	List<DatosAgentePrimaPagadaPorRamoView> llenarDatosAgentePrimaPagadaPorRamo(
			List<Agente> listaAgentes, Date fechaCorteInicio,
			Date fechaCorteFin,
			List<CentroOperacion> centroOperacionesSeleccionados,
			List<Ejecutivo> ejecutivosSeleccionados,
			List<Gerencia> gerenciasSeleccionadas,
			List<Promotoria> promotoriasSeleccionadas, Integer tipoReporte);

	List<DatosAgenteTopAgenteView> llenarDatosAgenteTopAgente(
			List<Agente> listaAgentes,
			List<CentroOperacion> centroOperacionesSeleccionados,
			List<Ejecutivo> ejecutivosSeleccionados,
			List<Gerencia> gerenciasSeleccionadas,
			List<Promotoria> promotoriasSeleccionadas, String anio, String mes,
			Integer tipoReporte, Integer topAgente, Date fechaInicio,
			Date fechaFin);

	DatosNegocioDTO llenarNegocioDTO(Long idToNegocio);

	TransporteImpresionDTO getFileFromDisk(String path);

	TransporteImpresionDTO imprimirCartaBienvenidaAgente();

	TransporteImpresionDTO imprimirContratoAgente(Agente agente,
			TipoContrato tipoContrato, Locale locale);

	TransporteImpresionDTO imprimirBasesEmision(
			DatosBasesEmisionParametrosDTO datosParametros, Locale locales);

	TransporteImpresionDTO imprimirReportesEstadoCuenta(
			DatosReporteEstadoCuentaParametrosDTO datosParametros,
			Locale locales);

	TransporteImpresionDTO imprimirReportesPagoCompContra(
			DatosReportePagoCompContraParametrosDTO datosParametros,
			Locale locales);

	TransporteImpresionDTO imprimirTablaAmortizacion(
			ConfigPrestamoAnticipo configPrestamoAnticipo, Locale locale);

	@SuppressWarnings("rawtypes")
	TransporteImpresionDTO getExcel(List dataSource, String fileNameResource,
			String formatoSalida);

	@SuppressWarnings("rawtypes")
	TransporteImpresionDTO getExcel(List dataSource, String fileNameResource,
			Map<String, Object> params, String formatoSalida);

	@SuppressWarnings("rawtypes")
	TransporteImpresionDTO getExcel(List dataSource,
			List<Map<String, Object>> params, String... fileNameResource);

	@SuppressWarnings("rawtypes")
	InputStream getExcel(List dataSource) throws IllegalArgumentException,
			FileNotFoundException, InvalidFormatException,
			IllegalAccessException, InvocationTargetException, IOException;

	TransporteImpresionDTO imprimirPagarePrestamoAnticipo(
			PagarePrestamoAnticipo pagare, Locale locale);

	TransporteImpresionDTO imprimirPagarePrincipalPrestamoAnticipo(
			ConfigPrestamoAnticipo config, Locale locale);

	TransporteImpresionDTO imprimirComprobanteCargo(ConfigCargos configCargos,
			Locale locle);

	List<DatosReportePreviewBonos> llenarDatosPreviewBonos(
			Long centroOperacion, Long gerencia, Long ejecutivo,
			Long promotoria, ValorCatalogoAgentes tipoAgentes, String anio,
			String mes, Agente agente, ConfigBonos bono, Long idCalcuo);

	List<DatosDetPrimas> llenarDatosDetPrimas(CentroOperacion centroOperacion,
			Gerencia gerencia, Ejecutivo ejecutivo, Promotoria promotoria,
			ValorCatalogoAgentes tipoAgentes, String fecha1, String fecha2,
			Agente agente);

	List<DatosAgenteDatosView> llenarDatosAgenteDatosAgente(Date fechaInicial,
			Date fechaFinal, Long idCentroOperacion, Long idGerencia,
			Long idEjecutivo, Long idPromotoria, Long rangoInicio, Long rangoFin);

	List<DatosAgenteBonosGerenciaView> llenarDatosAgenteBonosGerencia(
			String anio, String mes, String mesFin, Long idCentroOperacion,
			Long idGerencia, Long idEjecutivo, Long idPromotoria,
			Long idAgente, Long clasificacionAgente);

	List<DatosAgenteContabilidadMMView> llenarDatosAgenteContabilidadMM(
			String anioInicio, String mesInicio, String anioFin, String mesFin,
			Long idCentroOperacion, Long idGerencia, Long idEjecutivo,
			Long idPromotoria, Long idAgente);

	List<DatosAgentesDetalleBonoView> llenarDatosDetBonos(Long idAgente,
			String pAnioMesInicial, String pAnioMesFinal);

	public List<DatosReporteCalculoBonoMensualDTO> llenarDatosReporteCalculoBonoMensual(
			Long idBeneficiario, Long idCalculoBonos, String anio, String mes,
			Long tipoBeneficiario);

	List<DatosAgentesReporteProduccion> llenarDatosReporteProd(
			Date fechaInicio, Date fechaFin);

	List<ReporteDetalleProvisionView> reporteDetalleProvisiones(String anio,
			String mes, ProvisionImportesRamo filtro) throws Exception;

	List<ReporteProvisionView> reporteProvisiones(String anio, String mes,
			ProvisionImportesRamo filtro) throws Exception;

	List<DatosAgentePrimaEmitidaVsPriamaPagadaDTO> llenarDatosRepPrimaEmitidaVsPrmaPagada(
			String fechaInicio, String fechaCorteFin, Long centroOperacion,
			Long gerencia, Long ejecutivo, Long promotoria, Long agente);

	List<ReporteGlobalBonoYComisionesDTO> llenarDatosReporteGlobalComisionesY_Bonos(
			Long idAgente, Date fecha, Long idBono, Boolean bandera);

	List<DatosEdoctaAgenteAcumuladoView> llenarDatosEdoctaAgente(
			Long idEmpresa, Long idAgente, Long anioMes);

	DatosEdoctaAgenteProduccion llenarDatosEdoctaAgenteProduccion(
			Long idAgente, String anio, String mes);

	DatosEdoctaAgenteSiniestralidad llenarDatosEdocataAgenteSiniestralidad(
			Long idAgente, String anio, String mes, Long idProduccionSobre,
			String rango, Date fechaInicioAnio, Date fechaFinAnio,
			Date fechaUltimoAnioInicio, Date fechaUltimoAnioFin);

	List<DatosHonorariosAgentes> llenarDatosHonorarios(BigDecimal solicitudChequeId);

	List<DatosDetPrimas> llenarDatosDetallePrimasReporte(String anio,
			String mes, Agente agente);

	List<RetencionImpuestosReporteView> llenarDatosDetallePrimaRetencionImpuestos(
			RetencionImpuestosReporteView filtro);

	int generarDatosReporteRecibosProv(String mesAnio) throws Exception;

	List<DatosAgenteContabilidadMMView> llenarDatosAgenteContabilidadMizar(
			String anioInicio, String mesInicio, String anioFin, String mesFin,
			Long idCentroOperacion, Long idGerencia, Long idEjecutivo,
			Long idPromotoria, Long idAgente);

	List<SaldosVsContabilidadView> llenarDatosReporteSaldosVsContabilidad(
			Date fechaCorte);

	List<SapAlertasistemasEnvio> obtenerDetalleAlertsBitacora(
			String idEncabezadoAlertas);

	List<SapAmisBitacoraEmision> obtenerBitacoraEmision(String bitacoraPoliza,
			String bitacoraVin, String bitacoraFechaEnvio, String estatusEnvio,
			String cesvi, String cii, String emision, String ocra,
			String prevencion, String pt, String csd, String siniestro,
			String sipac, String valuacion);

	List<SapAmisBitacoraSiniestros> obtenerBitacoraSiniestro(
			String bitacoraPoliza, String bitacoraVin,
			String bitacoraFechaEnvio, String estatusEnvio, String cesvi,
			String cii, String emision, String ocra, String prevencion,
			String pt, String csd, String siniestro, String sipac,
			String valuacion);

	@SuppressWarnings("rawtypes")
	TransporteImpresionDTO getReporteEfectividad(List dataSource,
			Map<String, Object> params);

	List<DatosAgenteEstatusEntregaFactura> llenarDatosEstatusEntregaFactura(
			DatosAgenteEstatusEntregaFactura datos);

	List<DatosVentasProduccion> llenarDatosVentasProduccion(String anio,
			String mesInicio, String mesFin, Long idCentroOperacion,
			Long idGerencia, Long idEjecutivo, Long idPromotoria, Long idAgente);

	public TransporteImpresionDTO imprimirReportesCompensacionesAdicionales(
			DatosReporteCompParametrosDTO datosParametros, Locale locales);

	public TransporteImpresionDTO imprimirReportesDerechoPoliza(
			DatosRepDerPolParametrosDTO datosParametros, Locale locales);

	public TransporteImpresionDTO imprimirReportesPagadasDevengar(
			DatosReportePagDevDTO datosParametros, Locale locales);

	public TransporteImpresionDTO imprimirReporteBancaSeguros(
			Map<String, Object> parametros);

	public TransporteImpresionDTO imprimirReportePagosSaldos(
			Map<String, Object> parametros);

	public void actualizarEstatusReporteDetPrimas(String estatus);
}
