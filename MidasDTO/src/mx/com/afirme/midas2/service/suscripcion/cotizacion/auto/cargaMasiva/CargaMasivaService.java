package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasiva;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoEndoso;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.estadodescuento.NegocioEstadoDescuento;
import mx.com.afirme.midas2.domain.negocio.producto.formapago.NegocioFormaPago;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.autoexpedibles.AutoExpediblesAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.autoexpedibles.AutoExpediblesDetalleAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasiva.CargaMasivaAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasiva.DetalleCargaMasivaAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasivacotizacion.CargaMasivaCondicionesEsp;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasivacotizacion.CargaMasivaCondicionesEspDet;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasivaindividual.CargaMasivaIndividualAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasivaindividual.DetalleCargaMasivaIndAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoInciso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAuto;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.RegistroFuerzaDeVentaDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasiva.LogErroresCargaMasivaDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.ControlDinamicoRiesgoDTO;

import org.joda.time.DateTime;

@Local
public interface CargaMasivaService {
	
	public CargaMasivaAutoCot guardaCargaMasiva(BigDecimal idCotizacion, ControlArchivoDTO controlArchivo, String usuario, Short tipoCarga);
	
	public void guardaDetalleCargaMasiva(CargaMasivaAutoCot cargaMasiva, List<DetalleCargaMasivaAutoCot> detalleList);
	
	public NegocioSeccion obtieneNegocioSeccionPorDescripcion(CotizacionDTO cotizacion, String descripcion);
	
	public NegocioTipoUso obtieneNegocioTipoUsoPorDescripcion(NegocioSeccion negocioSeccion, String descripcion);
	
	public NegocioPaqueteSeccion obtieneNegocioPaqueteSeccionPorDescripcion(NegocioSeccion negocioSeccion, String descripcion);
	
	public EstiloVehiculoDTO obtieneEstiloVehiculoDTOPorClaveAMIS(CotizacionDTO cotizacion, NegocioSeccion negocioSeccion, String claveAMIS);
	
	public String obtieneMunicipioIdPorCodigoPostal(String codigoPostal);
	
	public String obtieneEstadoIdPorMunicipio(String municipioId);
	
	public List<CoberturaCotizacionDTO> obtieneCoberturasDelInciso(IncisoCotizacionDTO incisoCotizacion, NegocioPaqueteSeccion negocioPaqueteSeccion, EstiloVehiculoId estiloVehiculoId);

	public IncisoCotizacionDTO guardaIncisoCotizacion(IncisoCotizacionDTO incisoCotizacion, List<CoberturaCotizacionDTO> coberturaCotizacionList);

	public IncisoCotizacionDTO guardaIncisoCotizacion(IncisoCotizacionDTO incisoCotizacion, List<CoberturaCotizacionDTO> coberturaCotizacionList, List<DatoIncisoCotAuto> datoIncisoCotAutos);
	
	public IncisoCotizacionDTO obtieneIncisoCotizacion(CotizacionDTO cotizacion, BigDecimal numeroInciso);
	
	public void guardarComplementarInciso(IncisoCotizacionDTO inciso, List<DatoIncisoCotAuto> datoIncisoCotAutos);
	
	public Boolean datosDelConductorRequeridos(IncisoCotizacionDTO inciso);
	

	/**
	 * Obtiene el primer dato de inciso a nivel cobertura y asigna el valor de la descripcion.
	 */
	public DatoIncisoCotAuto obtieneDatoIncisoCotAuto(CotizacionDTO cotizacion, CoberturaCotizacionDTO cobertura, BigDecimal numeroInciso, String descripcion);
	
	/**
	 * Obtiene el dato de inciso a nivel subramo que sea igual a la descripcion etiqueta y le asigna el valor de la descripcion.
	 */
	public DatoIncisoCotAuto obtieneDatoIncisoCotAutoSubRamo(CotizacionDTO cotizacion,CoberturaCotizacionDTO cobertura, BigDecimal numeroInciso,ConfiguracionDatoInciso.DescripcionEtiqueta descripcionEtiqueta, String descripcion);
	
	/**
	 * Obtiene el dato de inciso a nivel cobertura que sea igual a la descripcion etiqueta y le asigna el valor de la descripcion.
	 */
	public DatoIncisoCotAuto obtieneDatoIncisoCotAutoCobertura(CotizacionDTO cotizacion,CoberturaCotizacionDTO cobertura, BigDecimal numeroInciso,ConfiguracionDatoInciso.DescripcionEtiqueta descripcionEtiqueta, String descripcion);

	
	public List<CoberturaCotizacionDTO> obtieneCoberturasContratadas(IncisoCotizacionDTO inciso);
	
	public List<CargaMasivaAutoCot> obtieneCargasMasivasPendientes();
	
	public CargaMasivaIndividualAutoCot guardaCargaMasivaIndividual(BigDecimal idToNegocio,
			BigDecimal idToNegProducto, BigDecimal idToNegTipoPoliza, 
			ControlArchivoDTO controlArchivo, String usuario, Short tipoCarga, Short claveTipo);
	
	public void guardaDetalleCargaMasivaIndividual(CargaMasivaIndividualAutoCot cargaMasiva,
			List<DetalleCargaMasivaIndAutoCot> detalleList);
	
	public CotizacionDTO crearCotizacion(CotizacionDTO cotizacion);
	
	public FormaPagoDTO obtieneFormaPago(String descripcion);
	
	public NegocioDerechoPoliza obtieneNegocioDerechoPoliza(BigDecimal idToNegocio, Double importe);
	
	public CotizacionDTO guardarCotizacion(CotizacionDTO cotizacion);
	
	public ResumenCostosDTO calculaCotizacion(CotizacionDTO cotizacion);
	
	public TerminarCotizacionDTO terminaCotizacion(CotizacionDTO cotizacion);
	
	public IncisoCotizacionDTO calculoInciso(IncisoCotizacionDTO incisoCotizacion);
	
	public Long solicitudAutorizacion(List<ExcepcionSuscripcionReporteDTO> excepcionesList, BigDecimal idToCotizacion, Long usuarioId);
	
	public Map<String, String> emitirCotizacion(CotizacionDTO cotizacion);
	
	public List<CargaMasivaIndividualAutoCot> obtieneCargasMasivasIndividualesPendientes();
	
	public void eliminaCotizacionError(CotizacionDTO cotizacion);
	
	public List<NegocioSeccion> getSeccionListByCotizacion(CotizacionDTO cotizacion);
	
	public SeccionDTO getSeccionDTOById(NegocioSeccion negocioSeccion);
	
	public List<NegocioTipoUso> getNegocioTipoUsoListByNegSeccion(NegocioSeccion negocioSeccion);
	
	public List<NegocioPaqueteSeccion> getNegocioPaqueteSeccionByNegSeccion(NegocioSeccion negocioSeccion);
	
	public CotizacionDTO obtieneCotizacion(CargaMasivaAutoCot carga);
	
	public void guardaCargaMasivaAutoCot(CargaMasivaAutoCot carga);
	
	public List<NegocioDerechoPoliza> getNegocioDerechoPolizaByNegTipoPoliza(NegocioTipoPoliza negocioTipoPoliza);
	
	public List<NegocioFormaPago> getNegocioFormaPagoByNegTipoPoliza(NegocioTipoPoliza negocioTipoPoliza);
	
	public List<NegocioSeccion> getNegocioSeccionByNegTipoPoliza(NegocioTipoPoliza negocioTipoPoliza);
	
	public List<RegistroFuerzaDeVentaDTO> getCentroEmisores();
	
	public CotizacionDTO getCotizacionByDetalleCargaMasivaIndAutoCot(BigDecimal idToCotizacion);
	
	public PolizaDTO getPolizaByStringId(String idPoliza);
	
	public NegocioTipoPoliza getNegocioTipoPolizaByCarga(BigDecimal idToNegTipoPoliza);
	
	public Usuario getUsuarioByCarga(String codigoUsuarioCreacion);
	
	public void setUsuarioActual(Usuario usuario);
	
	public BigDecimal guardaCargaMasivaIndividualAutoCot(CargaMasivaIndividualAutoCot carga);
	
	public Agente validaAgente(Long claveAgente, String claveNegocio, Long idToNegocio);
	
	public Double obtieneIVAPorCodigoPostalColonia(String codigoPostal, String nombreColonia);
	
	public List<ClienteGenericoDTO> validaClienteContratante(DetalleCargaMasivaIndAutoCot detalle);
	
	public Boolean validaModeloVehiculo(BigDecimal idMoneda, String claveEstilo, BigDecimal idToNegSeccion, Short modeloVehiculo);
	
	public Boolean validaEstadoMunicipio(Long idToNegocio, String estadoId, String municipioId);
	
	public List<DetalleCargaMasivaIndAutoCot> getCargaMasivaDetalleIndividualList(BigDecimal idToCargaMasivaIndAutoCot);
	
	public List<Ejecutivo> getEjecutivos();
	
	public NegocioSeccion getNegocioSeccionById(BigDecimal negocioSeccionId);
	
	public Double getPctePagoFraccionado(Integer idFormaPago, Short idMoneda);
	
	public List<AutoExpediblesDetalleAutoCot> getAutoExpediblesDetalleList(BigDecimal idToAutoExpediblesAutoCot);
	
	public ClienteGenericoDTO validaClienteContratante(AutoExpediblesDetalleAutoCot detalle);
	
	public void guardaDetalleAutoExpedibles(AutoExpediblesAutoCot autoExpedibles, List<AutoExpediblesDetalleAutoCot> detalleList);
	
	public List<AutoExpediblesAutoCot> obtieneAutoExpediblesPendientes();
	
	public BigDecimal guardaAutoExpediblesAutoCot(AutoExpediblesAutoCot autoExpedibles);
	
	public AutoExpediblesAutoCot guardaAutoExpedibles(BigDecimal idToNegocio,
			BigDecimal idToNegProducto, BigDecimal idToNegTipoPoliza, 
			ControlArchivoDTO controlArchivo, String usuario, Short tipoCarga, Short claveTipo);
	
	public void igualarPrima(BigDecimal idToCotizacion, Double primaTotalAIgualar);
	
	public List<IncisoCotizacionDTO> obtieneIncisos(BigDecimal idToCotizacion);
	
	public DatoIncisoCotAuto obtieneDatoIncisoCotAutoDelInciso(CotizacionDTO cotizacion,
			CoberturaCotizacionDTO cobertura, BigDecimal numeroInciso);
	
	public void igualacionInciso(BigDecimal idToCotizacion, BigDecimal numeroInciso, Double primaAIgualar);
	
	public String getUploadFolder();
	
	public String eliminarIncisosCreados(List<IncisoCotizacionDTO> incisos, BigDecimal idCotizacion);
	
	public IncisoAutoCot obtieneIncisoAutoCot(IncisoCotizacionDTO inciso);
	
	public MedioPagoDTO obtieneMedioPago(String descripcion);
	
	public List<CatalogoValorFijoDTO> getTipoCargas();
	
	public CatalogoValorFijoDTO obtieneTipoCargaPorDescripcion(String descripcion);
	
	public boolean validaNoNulo(Object obj, String nombreCampo, BigDecimal numeroInciso,
			String lineaDeNegocio, List<LogErroresCargaMasivaDTO> errores);
		
	public boolean validaNoNulo2(Object obj, String nombreCampo, BigDecimal numeroInciso,
			String lineaDeNegocio, List<LogErroresCargaMasivaDTO> errores);
	
	public boolean validaRangos(Number valorMinimo, Number valorMaximo, Number valor, String nombreCampo, BigDecimal numeroInciso,
			String lineaDeNegocio, List<LogErroresCargaMasivaDTO> errores);
	
	public String generaMensajeError(List<LogErroresCargaMasivaDTO> errores);

	public boolean isCoberturaContratada(List<CoberturaCotizacionDTO> coberturaCotizacionDTOs, String codigoCobertura);
	
	public CoberturaCotizacionDTO getCoberturaCotizacion(List<CoberturaCotizacionDTO> coberturaCotizacionDTOs, String nombreComercial);
	
	public void agregaError(String descripcionError, String nombreCampo, BigDecimal numeroInciso,
			String lineaDeNegocio, List<LogErroresCargaMasivaDTO> errores);
	
	public void calcularTodosLosIncisos(CotizacionDTO cotizacion);
	
	public List<EndosoDTO> getEndosos(BigDecimal idToPoliza);
	
	public TransporteImpresionDTO imprimirPoliza(BigDecimal idToPoliza, Locale locale, DateTime validOnDT, 
			DateTime recordFromDT, Short claveTipoEndoso);
	
	public PolizaDTO getPolizaByCotizacion(BigDecimal idCotizacion);
	
	public void guardaIncisoCotizacion(IncisoCotizacionDTO incisoCotizacion);
	
	public CargaMasivaCondicionesEsp guardaCargaMasivaCondiciones(BigDecimal idSolicitud,ControlArchivoDTO controlArchivoDTO,String usuario,short estatusCarga);
	
	public void guardaDetalleCargaMasivaCondiciones(List<CargaMasivaCondicionesEspDet> lCargaMasivaCondicionesEspDet);
	
	/**
	 * Carga Masiva nueva
	 *
	public void guardaDetalleCargaMasiva(CargaMasivaAutoCot cargaMasiva, ArrayList<Resultado> resultado);*/
	
	public boolean getAplicaDescuentoNegocioPaqueteSeccion(Long idToNegPaqueteSeccion );
	
	public NegocioEstadoDescuento findByNegocioAndEstado(Long idToNegocio, String idToEstado);
	
	public List<BitemporalCoberturaSeccion> mostrarCoberturas(BitemporalAutoInciso bitemporalAutoInciso, Long cotizacionContinuityId, DateTime validoEn);
	
	public List<ControlDinamicoRiesgoDTO> getDatosRiesgo(Long incisoContinuityId, Date validoEn, 
			Map<String, String> valores, Short claveFiltroRIesgo, String accion, Boolean getInProcess);
	
	public void calcularIncisosEndosoAltaInciso(BitemporalCotizacion cotizacion, DateTime validoEn);
	
	public void guardarDatosAdicionalesPaquete(List<ControlDinamicoRiesgoDTO> controles,Long incisoContinuityId, Date validoEn);
	
	public void guardaCotizacionEndosoMovimientos(BitemporalCotizacion cotizacion);
	
	public NegocioDerechoEndoso obtieneNegocioDerechoEndosoAltaInciso(BitemporalCotizacion cotizacion);
}
