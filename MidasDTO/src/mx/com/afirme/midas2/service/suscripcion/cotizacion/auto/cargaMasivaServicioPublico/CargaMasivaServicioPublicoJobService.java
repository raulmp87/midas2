package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasivaServicioPublico;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico.CargaMasivaServicioPublico;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico.CargaMasivaServicioPublicoDetalle;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.CoberturaDetalleDTO;

@Local
public interface CargaMasivaServicioPublicoJobService {

	/**
	 * Método de interfaz de service para la validación de campos de DTO
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @param Lista de {@link CargaMasivaServicioPublicoDetalle} listEmision
	 * @param {@link Usuario} usuario
	 */
	public void validaDatosEmision(List<CargaMasivaServicioPublicoDetalle> listEmision, Usuario usuario);
	
	/**
	 * Método de interfaz de service para la consulta de tarifas configuradas.
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @param Long idToNegocio, BigDecimal idToProducto, BigDecimal idToTipoPoliza, BigDecimal idToSeccion, Long idPaquete
	 * 		  Parametros para consultar {@link NegocioPaqueteSeccion} negocioPaqueteSeccion
	 * @param Long idMoneda, String idTcEstado, String idTcCiudad, Long idVigencia, Long totalPasajeros
	 * 		  Paarametros para la consulta de las tarifas en conjunto con negocioPaqueteSeccion
	 * @param List<{@link CoberturasDetalleDTO}>
	 * 		  Lista de {@link CoberturasDetalleDTO} que almacena el id de cobertura el valor y el tipo de valor (Suma - Deducible)
	 * @return List<Double> Lista de valores de primas netas configuradas por cobertura.
	 */
	public boolean igualarTarifas(Long idToNegocio, BigDecimal idToProducto, BigDecimal idToTipoPoliza, BigDecimal idToSeccion, Long idPaquete,
								  Long idMoneda, String idTcEstado, String idTcCiudad, Long idVigencia, Long totalPasajeros, 
								  List<CoberturaDetalleDTO> listCoberturasDetalleDto, Double montoPrima);
	
	/**
	 * Método de interfaz de service para procesar igualación.
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @param BigDecimal idToCotizacion, Double primaTotalAIgualar, Boolean restaurarDescuento
	 */
	public boolean procesaIgualacion(BigDecimal idToCotizacion, Double primaTotalAIgualar, Boolean restaurarDescuento, Usuario usuario) ;
	
	/**
	 * Metodo para llamar por transaccion a la generacion de la cotizacion
	 * @param archivo
	 * @param usuario
	 */
	public void generarCotizacionesCargaMasivaServicioPublicoTransaction(CargaMasivaServicioPublico archivo, Usuario usuario);
	
	/**
	 * Metodo para llamar por transaccion a la actualización del estatus de archivo
	 * @param archivo
	 */
	public void updateEstatusArchivoDaoTransaction(CargaMasivaServicioPublico archivo);
	
	/**
	 * Metodo para llamar por transaccion a la actualización del estatus del detalle
	 * @param archivo
	 */
	public void updateEstatusDetalleDaoTransaction(CargaMasivaServicioPublicoDetalle item);
}
