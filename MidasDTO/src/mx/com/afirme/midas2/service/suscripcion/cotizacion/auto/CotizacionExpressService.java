package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tiposervicio.NegocioTipoServicio;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.CotizacionExpress;
import mx.com.afirme.midas2.dto.impresiones.ResumenCotExpressDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.CotizacionExpressDTO;


public interface CotizacionExpressService {
	
	/**
	 * Se utiliza para calcular la prima total asi como la prima para cada una de las coberturas de la cotizacion.
	 * @param cotizacionExpressService
	 * @return la cotizacion
	 */
	public CotizacionExpress cotizar(CotizacionExpress cotizacionExpress,  EstiloVehiculoDTO estiloVehiculoDTO,
			NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion, NegocioTipoUso negocioTipoUsoDefault,
			NegocioTipoServicio negocioTipoServicioDefault, String claveCirculacion);


	/**
	 * Cotiza cada una de las combinaciones posibles entre paquete y forma de pago para la <code>CotizacionExpressDTO</code> dada.
	 * @param cotizacionExpressDTO
	 * @return la cotizacionExpressDTO con las cotizaciones.
	 */
	public CotizacionExpressDTO cotizarPaqueteFormaPago(CotizacionExpressDTO cotizacionExpressDTO);
	
	/**
	 * Convierte la <code>cotizacionExpress</code> en una cotizacion formal.
	 * @param cotizacionExpress
	 * @return la <code>cotizacion</code> generada.
	 */
	public CotizacionDTO cotizarFormalmente(CotizacionExpress cotizacionExpress);
	
	/**
	 * Obtiene la version de carga.
	 * @return
	 */
	public BigDecimal getIdVersionCarga();

	/**
	 * Crea una nueva <code>CotizacionExpressDTO</code>.
	 * @return
	 */
	public CotizacionExpressDTO crearNuevaCotizacionExpressDTO();
	
	/**
	 * Indica si la busqueda de agente es permitida en la cotizacion express.
	 * @return
	 */
	public boolean isBusquedaAgenteVisible();
		
	/**
	 * Inicializa listas de paquetes y coberturas para impresion de cotizacion express
	 * @return
	 */
	public List<ResumenCotExpressDTO> inicializarDatosParaImpresion(CotizacionExpressDTO cotizacionExpressDTO);
	
}
