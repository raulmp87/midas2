package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBaseAuto;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;


public interface ConfiguracionIncisoTipoUsoService extends
		MidasInterfaceBaseAuto<TipoUsoVehiculoDTO> {

}
