package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasivaServicioPublico;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico.CargaMasivaServicioPublico;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico.CargaMasivaServicioPublicoDetalle;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAuto;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.CargaMasivaSPDetalleDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.CargaMasivaServicioPublicoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.CargaMasivaServicioPublicoEstatusDescriDetalleDTO;

@Local
public interface CargaMasivaServicioPublicoService {

	/**
	 * Método de interfaz de service para la consulta de archivos de carga masiva almacenados
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @param {@link CargaMasivaServicioPublicoDTO} archivoAdjunto
	 * @return {@link CargaMasivaServicioPublico}
	 */
	public List<CargaMasivaServicioPublico> consultaArchivosCargaMasivaService(CargaMasivaServicioPublicoDTO archivoAdjunto);
	
	/**
	 * Método de interfaz de service para verifiar si un MD5 ya existe en la tabla y en que estatus esta
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @return boolean
	 */
	public boolean getMd5ArchivoService(String md5CheckSum);
	
	/**
	 * Método de interfaz de service para almacenar el archivo en la base de datos
	 * @author SOFTNET - ISCJJBV 
	 * 
	 */
	public CargaMasivaServicioPublico saveArchivoService(CargaMasivaServicioPublico archivoCargaMasiva);
	
	/**
	 * Método de interfaz de service para almacenar el detalle del archivo en la base de datos
	 * @author SOFTNET - ISCJJBV 
	 * 
	 */
	public List<String> saveDetalleArchivoService(CargaMasivaServicioPublico archivoCargaMasiva, File archivo);
	
	/**
	 * Método de interfaz de service para contar el total de registros de la tabla de archivos
	 * @author SOFTNET - ISCJJBV 
	 * 
	 */
	public Long conteoBusquedaArchivosService(CargaMasivaServicioPublicoDTO archivoAdjunto);
	
	/**
	 * Retornar el detalle de un archivo de carga masiva por su id
	 * @param archivoId
	 * @return  SOFTNET -ISCJAM
	 */
	public List<CargaMasivaSPDetalleDTO> obtenerCargaMasivaDetalle(Long archivoId, Integer contador,Integer posStart);

	/**
	 * Retornar el detalle de un archivo de carga masiva por su id
	 * @param archivoId
	 * @return  SOFTNET -ISCJAM
	 */
	public Long contarDetalle(Long archivoId, Integer count,Integer posStart);
	
	/**
	 * Retornar el error detalle de un archivo de carga masiva por su id
	 * @param archivoId
	 * @return  SOFTNET -ISCJAM
	 */
	
	public String mostrarErrorDetalle( Long idDetalle);
	
	
	/**
	 * Crea cotizaciones a partir de la informacion de los detalles exitosos del archivo de carga masiva servicio publico
	 * @param archivo
	 * @return 
	 */
	public void generarCotizacionesCargaMasivaServicioPublico(CargaMasivaServicioPublico archivo, Usuario usuario);
	
	/**
	 * Metodo para ejecutar el guardado del inciso cotizacion, ya que se espera hasta que termina el metodo para persistir 
	 * la informacion.
	 * @param incisoCotizacion
	 * @param coberturaCotizacionList
	 * @param datoIncisoCotAutos
	 * @return
	 */
	public IncisoCotizacionDTO guardarIncisoCotizacion(IncisoCotizacionDTO incisoCotizacion, 
			List<CoberturaCotizacionDTO> coberturaCotizacionList, List<DatoIncisoCotAuto> datoIncisoCotAutos);
	
	
	/**
	 * Metodo para ejecutar el guardado del cotizacion, ya que se espera hasta que termina el metodo para persistir
	 * @param cotizacion
	 * @return
	 */
	public CotizacionDTO guardarCotizacion(CotizacionDTO cotizacion);
	
	/**
	 * Metodo para ejecutar la emision, ya que no soporta transacciones
	 * @param cotizacion
	 * @return
	 */
	public CargaMasivaServicioPublicoEstatusDescriDetalleDTO emitirCotizacion(CotizacionDTO cotizacion, CargaMasivaServicioPublicoEstatusDescriDetalleDTO resultado);
	
	/**
	 * Se hace publico para manejar las transacciones de la emision
	 * @param detalle
	 * @return
	 */
	public CargaMasivaServicioPublicoEstatusDescriDetalleDTO generaCotizacion(CargaMasivaServicioPublicoDetalle detalle, CargaMasivaServicioPublicoEstatusDescriDetalleDTO resultado, Usuario usuario);
	
	/**
	 * Metodo para ejecutar el guardado de la cotizacion
	 * @param cotizacion
	 * @return
	 */
	public ResumenCostosDTO calculaCotizacion(CotizacionDTO cotizacion);
	
	/**
	 * Metodo para ejecutar la igualacion en una nueva transaccion
	 * @param idToCotizacion
	 * @param primaTotal
	 * @param restaurarDescuento
	 * @param usuario
	 * @return
	 */
	public boolean procesaIgualacionTransaction(BigDecimal idToCotizacion, Double primaTotal, Boolean restaurarDescuento, Usuario usuario);
	
	/**
	 * Metodo para ejecutar la eliminacion de la cotizacion en una nueva transaccion
	 * @param cotizacion
	 */
	public void eliminaCotizacionErrorTransaction(CotizacionDTO cotizacion);
	
	/**
	 * Metodo para ejecutar la terminacion de la cotizacion en una nueva transaccion
	 * @param cotizacion
	 * @return
	 */
	public TerminarCotizacionDTO terminaCotizacionTransaction(CotizacionDTO cotizacion);
}