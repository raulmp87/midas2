package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.CoberturaView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.CotizacionCoberturaView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.CotizacionView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.EmisionView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.EstiloView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.MotivosEndosoView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.ParametrosImpresionCotizacionView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.ParametrosImpresionPolizaView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla.CotizarEndosoFlotillaRequest;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla.CotizarEndosoFlotillaResponse;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla.EmitirEndosoFlotillaRequest;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla.EmitirEndosoFlotillaResponse;
import mx.com.afirme.midas2.domain.emision.ppct.ReciboSeycos;
import mx.com.afirme.midas2.dto.endoso.ConductoCobroDTO;
import mx.com.afirme.midas2.dto.endoso.EndosoTransporteDTO;
import mx.com.afirme.midas2.dto.endoso.cotizacion.auto.CotizacionEndosoDTO;
import mx.com.afirme.midas2.dto.endoso.recibos.RecibosEndosoTransporteDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.recibos.MovimientoReciboDTO;

public interface WSCotizacionGenericaService {

	Map<String, Object> creaCotizacion(Long idNegocio, Double descuento, String folio,Long idPaquete, int idFormaPago,
			Integer estilo, Integer modelo, Integer marca, String idEstado,String idMunicipio, String nombreAsegurado,
			String apellidoPaterno,String apellidoMaterno, String rfc,String claveSexo,String claveEstadoCivil,
			String idEstadoNacimiento, Date fechaNacimiento,String codigoPostal, String idCiudad,String estado,
			String colonia,String calleNumero, String telefonoCasa, String telefonoOficina,String email, BigDecimal idProducto, 
			BigDecimal idTipoPoliza, BigDecimal idToNegSeccion, Long idAgente, String observaciones, BigDecimal idCliente,
			List<CoberturaView> coberturaList, Date inicioVigencia, Date finVigencia, String token) throws SystemException;
	
	Long guardarCobranza(BigDecimal idToCotizacion, ConductoCobroDTO conductoCobro, boolean esEndoso, String token) throws Exception;
	
	List<Map<String, Object>> buscarCliente(ClienteGenericoDTO filtro, String token) throws Exception;
	
	Map<String, String> emitir(String numeroSerie, String numeroMotor, String numeroPlaca, int idFormaPago,
    		BigDecimal idCotizacion, Short medioPago, String tipoTarjeta, int idBanco, 
			String fechaVencimientoTarjeta, String codigoSeguridad, String tarjetaHabiente, String cuenta, 
			String correo, String telefono, String clavePais, String claveEstado, String claveCiudad, String idColonia, 
			String nombreColonia, String calleNumero, String codigoPostal, String rfc, String nombreAsegurado, String token) throws SystemException;
	
	List<String> listPaquetesByIdNegocio(Long idNegocio, String token) throws SystemException;
	
	byte[] imprimirPoliza(BigDecimal idPoliza, String token) throws SystemException;
	
	byte[] imprimirPoliza(ParametrosImpresionPolizaView params, String token) throws SystemException;
	
	Map<Long, String> getListAgentes(String token) throws SystemException;
	
	Map<Long, String> getListNegocios(Integer idAgente, String token) throws SystemException;
	
	Map<Long, String> getListNegocioProductos(Long idNegocio, BigDecimal idProducto, String token)  throws SystemException;
	
	Map<BigDecimal, String> getListTiposPolizaByNegProducto(Long idNegProducto, String token)  throws SystemException;
	
	Map<BigDecimal, String> getListLineasNegocio(BigDecimal idToNegTipoPoliza, String token)  throws SystemException;
	
	Map<BigDecimal, String> getListMarcas(BigDecimal idToNegSeccion, String token) throws SystemException;
	
	@Deprecated
	Map<String, String> getListEstilos(BigDecimal idTcMarcaVehiculo, String token) throws SystemException;
	
	Map<String, String> buscarEstilo(EstiloView filtro, String token) throws SystemException;
	
	Map<String, String> getListEstados(Long idNegocio, String token) throws SystemException;
	
	Map<String, String> getListMunicipios(String idEstado, String token) throws SystemException;
	
	Map<Long, String> getListPaquetes(BigDecimal idToNegSeccion, String token) throws SystemException;
	
	Map<Integer, String> getListFormasPago(BigDecimal idToNegTipoPoliza, String token) throws SystemException;
	
	Map<Integer, String> getListBancos( String token ) throws SystemException;
	
	Map<Integer, String> getListMediosPago( String token ) throws SystemException;
	
	Map<String, String> getListTiposTarjeta( String token ) throws SystemException;
	
	Map<String, String> emitirPoliza(String numeroSerie, String numeroMotor, String numeroPlaca, int idFormaPago,
    		BigDecimal idCotizacion, Short medioPago, String tipoTarjeta, int idBanco, 
			String fechaVencimientoTarjeta, String codigoSeguridad, String tarjetaHabiente, String cuenta, 
			String correo, String telefono, String clavePais, String claveEstado, String claveCiudad, String idColonia, 
			String nombreColonia, String calleNumero, String codigoPostal, String rfc, String nombreAsegurado, String token)throws SystemException;
	
	Map<Short, Short> getListModelos(BigDecimal idToNegSeccion, BigDecimal idMoneda, BigDecimal idMarca, String token) throws SystemException;
	
	Map<BigDecimal, String> getListTiposUso(BigDecimal idToNegSeccion, String token) throws SystemException;
	
	Map<String, Object> cotizarEndoso(EndosoTransporteDTO dto) throws SystemException;
	
	Map<String, Object> emitirEndoso(EndosoTransporteDTO dto) throws SystemException;
	
	byte[] imprimirEndoso(EndosoTransporteDTO dto) throws SystemException;
	
	List<CotizacionEndosoDTO> buscarCotizacionesEndosoPoliza(EndosoTransporteDTO dto) throws SystemException;
	
	Map<String, String> getListColoniasByCP(String cp, String token) throws SystemException;
	
	Map<String, Object> creaCotizacion(CotizacionView cotizacionView, String token) throws SystemException;
	
	List<Object> obtenerCoberturas(CotizacionCoberturaView cot, String token) throws SystemException;
	
	Map<Integer, String> getListMotivoEndoso( MotivosEndosoView motivosEndosoView, String token ) throws SystemException;
	
	Map<String, Object> cancelarPoliza(EndosoTransporteDTO dto)  throws SystemException;
	
	Map<BigDecimal, String> getListEndososConRecibosPoliza(RecibosEndosoTransporteDTO dto) throws SystemException;
	
	Map<BigDecimal, String> getListProgramasPagosEndosoPoliza(RecibosEndosoTransporteDTO dto) throws SystemException;
	
	String getIncisosProgPago(RecibosEndosoTransporteDTO dto) throws SystemException;
	
	List<ReciboSeycos> getRecibosPoliza(RecibosEndosoTransporteDTO dto) throws SystemException;
	
	byte[] imprimirRecibo(RecibosEndosoTransporteDTO dto) throws SystemException;
	
	List<MovimientoReciboDTO> getMovimientosRecibosPoliza(RecibosEndosoTransporteDTO dto) throws SystemException;
	
	void validateToken( String token ) throws SystemException;
	
	BigDecimal obtenerIdPoliza(ParametrosImpresionPolizaView params);	
	
	Map<Integer, String> getListTiposEndoso(String token) throws SystemException;
	
	Map<Integer, String> getListFormasPagoDePoliza(EndosoTransporteDTO dto) throws SystemException;
	
	Map<String, String> getMunicipioByCP(String codigoPostal, String token) throws SystemException;
	
	Map<String, String> emitirPoliza(EmisionView emisionView, String token) throws SystemException;
	
	byte[] imprimirCotizacion(ParametrosImpresionCotizacionView params, String token) throws SystemException;
	
	CotizarEndosoFlotillaResponse cotizarEndosoFlotilla(CotizarEndosoFlotillaRequest request, String token) throws SystemException;
	
	EmitirEndosoFlotillaResponse emitirEndosoFlotilla(EmitirEndosoFlotillaRequest request, String token) throws SystemException;
	
	PolizaDTO validatePoliza(String numeroPoliza) throws SystemException;
}
