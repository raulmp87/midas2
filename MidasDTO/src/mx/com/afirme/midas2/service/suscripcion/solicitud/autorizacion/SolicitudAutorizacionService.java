package mx.com.afirme.midas2.service.suscripcion.solicitud.autorizacion;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.autorizacion.SolicitudExcepcionCotizacion;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.autorizacion.SolicitudExcepcionDetalle;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionNegocioDescripcionDTO;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;


public interface SolicitudAutorizacionService {

	public static enum EstatusSolicitud{
		EN_PROCESO((short)0), TERMINADA((short)1), CANCELADA((short)3);
		
		EstatusSolicitud(Short estatus) {
			this.estatus = estatus;
		}
		private Short estatus;
		
		public Short valor(){
			return this.estatus;
		}		
	};	
	
	public static enum EstatusDetalleSolicitud{
		EN_PROCESO((short)0), AUTORIZADA((short)1), RECHAZADA((short)2), CANCELADA((short)3);
		
		EstatusDetalleSolicitud(Short estatus) {
			this.estatus = estatus;
		}
		private Short estatus;
		
		public Short valor(){
			return this.estatus;
		}		
	};		

	/**
	 * Listar solicitudes por los parametros dentro del filtro
	 * @param filtro
	 * @return
	 */
	public List<SolicitudExcepcionCotizacion> listarSolicitudes(SolicitudExcepcionCotizacion filtro);
	
	/**
	 * Guardar solicitud. Si el id es nulo es creada.
	 * @param solicitud
	 */
	public void guardarSolicitud(SolicitudExcepcionCotizacion solicitud);
	
	/**
	 * Obtener solicitud sin detalle por el id
	 * @param id
	 * @return
	 */
	public SolicitudExcepcionCotizacion obtenerSolicitud(Long id);
	
	/**
	 * Obtener solicitud junto con detalle por el id de la solicitud
	 * @param id
	 * @return
	 */
	public SolicitudExcepcionCotizacion obtenerSolicitudConDetalle(Long id);
	
	/**
	 * Obtener solicitud junto con detalle por el id cotizacion
	 * @param id
	 * @return
	 */
	public SolicitudExcepcionCotizacion obtenerSolicitudConDetalle(BigDecimal idCotizacion);
	
	/**
	 * Obtener solicitudes donde este la cotizacion guardada
	 * @param idCotizacion
	 * @return
	 */
	public List<SolicitudExcepcionCotizacion> obtenerSolicitud(BigDecimal idCotizacion);
	
	
	/**
	 * Obtener listado de solicitudes historicas donde haya estado presente la cotizacion
	 */
	public List<SolicitudExcepcionCotizacion> obtenerSolicitudesHistoricas(BigDecimal idCotizacion);
	
	public List<SolicitudExcepcionDetalle> obtenerSolicitudesHistoricasDetalle(BigDecimal idCotizacion, BigDecimal idInciso);
	
	/**
	 * Obtiene la lista de detalle para la solicitud
	 * @param id
	 * @return
	 */
	public List<SolicitudExcepcionDetalle> listarDetalleParaSolicitud(Long id);
	
	/**
	 * Cambia la solicitud a estatus de TERMINADA
	 */
	public void terminarSolicitud(Long id, Long usuarioAutorizador);
	
	/**
	 * Actualiza los estatus y observaciones de los detalles y luego termina la solicitud
	 * @param idSolicitud
	 * @param detalle
	 */
	public void terminarSolicitud(Long idSolicitud, List<SolicitudExcepcionDetalle> detalle, Long usuarioAutorizador);
	
	/**
	 * Cambia la solicitud a estatus de CANCELADA
	 */
	public void cancelarSolicitud(Long id, Long usuarioAutorizador);
	
	/**
	 * Autoriza el detalle de la solicitud
	 * @param idSolicitud
	 * @param idDetalle
	 */
	public void autorizarDetalle(Long idSolicitud, Long idDetalle, String observacion);
	
	/**
	 * Rechaza el detalle de la solicitud
	 * @param idSolicitud
	 * @param idDetalle
	 */
	public void rechazarDetalle(Long idSolicitud, Long idDetalle, String observacion);
	
	
	/**
	 * Cancela el detalle de la solicitud
	 * @param idSolicitud
	 * @param idDetalle
	 */
	public void cancelarDetalle(Long idSolicitud, Long idDetalle, String observacion);
	
	
	/**
	 * Metodo que agrega una excepcion a una solicitud existente para una cotizacion. En
	 * caso de que la solicitud no exista la crea y agrega la excepcion.
	 * @param idCotizacion
	 * @param descripcion
	 * @param observacion
	 * @param usuarioSolicitante
	 * @param usuarioAutorizador
	 */
	public void agregarSolicitud(BigDecimal idCotizacion, String descripcionSolicitud, 
			Long usuarioSolicitante, Long usuarioAutorizador, String cveNegocio, 
			String descripcionDetalle, String observacionDetalle, BigDecimal idSeccion, 
			BigDecimal idInciso, BigDecimal idCobertura, Long idExcepcion, 
			String provocadorExcepcion, String claveProvocadorExcepcion);
	

	/**
	 * Agrega una excepcion a una solicitud existente
	 * @param idSolicitud
	 * @param descripcion
	 * @param observacion
	 */
	public void agregarExcepcionASolicitud(Long idSolicitud, String descripcion, 
			String observacion, BigDecimal idSeccion, BigDecimal idInciso, BigDecimal idCobertura,
			Long idExcepcion, String provocadorExcepcion, String claveProvocadorExcepcion);
	
	/**
	 * Crea o guarda una solicitud de autorizacion con las excepciones falladas
	 * @param idCotizacion
	 * @param reporte
	 * @return
	 */
	public Long guardarSolicitudDeAutorizacion(BigDecimal idCotizacion, 
			String descripcionSolicitud, Long usuarioSolicitante, Long usuarioAutorizador, 
			String cveNegocio, List<ExcepcionSuscripcionReporteDTO> excepciones);
	
	/**
	 * Regresa un listado de solicitudes cuyo estatus sea rechazada o en proceso
	 * @param idCotizacion
	 * @return
	 */
	public List<SolicitudExcepcionCotizacion> listarSolicitudesInvalidas(BigDecimal idCotizacion);
		
	
	public List<SolicitudExcepcionCotizacion> listarSolicitudes(SolicitudExcepcionCotizacion filtro, Date fechaHasta);
	
	/**
	 * Obtiene el estatus para una excepcion dentro de una cotizacion en particular
	 * @param idCotizacion
	 * @param idExcepcion
	 * @return Estaus de la excepcion. <code>null</null> si no se encuentra la excepcion
	 */
	public Short obtenerEstatusParaExcepcion(BigDecimal idCotizacion, BigDecimal idSeccion, 
			BigDecimal idInciso, BigDecimal idCobertura, Long idExcepcion);
	
	
	/**
	 * Obtiene un detalle en particular dentro de la lista de detalles de una cotizacion
	 * @param idCotizacion
	 * @param idExcepcion
	 * @param idSeccion
	 * @param idInciso
	 * @param idCobertura
	 * @return
	 */
	public SolicitudExcepcionDetalle obtenerDetalleParaExcepcion(BigDecimal idCotizacion,
			final Long idExcepcion, final BigDecimal idSeccion, final BigDecimal idInciso, final BigDecimal idCobertura);
			
	public ExcepcionSuscripcionNegocioDescripcionDTO verDetalleExcepcion(Long idExcepcion);
	
	/**
	 * Obtiene una lista de todos los usuarios
	 * 
	 * @return
	 * @author martin
	 */
	public List<Usuario> getListUsuarios(String name);
	
	public Long listarSolicitudesCount(SolicitudExcepcionCotizacion filtro, Date fechaHasta);
	
	public void terminarSolicitudMasiva(List<SolicitudExcepcionCotizacion> solicitudes, Long usuarioAutorizador, Short tipo);
}
