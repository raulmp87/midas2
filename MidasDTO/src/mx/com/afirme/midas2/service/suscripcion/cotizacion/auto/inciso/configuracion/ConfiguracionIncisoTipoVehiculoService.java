package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.MidasInterfaceBaseAuto;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;


public interface ConfiguracionIncisoTipoVehiculoService extends
		MidasInterfaceBaseAuto<TipoVehiculoDTO> {
	

}
