package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.recibos;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.domain.emision.ppct.ReciboSeycos;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.recibos.MovimientoReciboDTO;
import mx.com.afirme.midas2.service.poliza.EnvioEmailPolizaService.EnviarEmailPorIncisoParameters;

import com.afirme.insurance.services.PrintReport.PrintReport;

@Local
public interface ImpresionRecibosService {
	
	Long obtenerTotalPolizasRecibos(PolizaDTO polizaDTO, Boolean filtrar, Boolean isCount);
	
	List<PolizaDTO> listarPolizasRecibos(PolizaDTO polizaDTO, Boolean filtrar, Boolean isCount);

	InputStream imprimirPoliza(List<PolizaDTO> polizaList, Locale locale, 
        	boolean incluirCaratula, boolean incluirRecibo, boolean incluirIncisos, 
        	boolean incluirCertificadoRC, boolean incluirAnexosIncisos,boolean incluirReferenciasBancarias);
	
	String enviaPolizasProveedor(List<PolizaDTO> polizaList, Locale locale, String nombreArchivo);
	
	/**
	 * Obtiene la llave fiscal para el idToPoliza y numeroInciso dado.
	 * @param idToPoliza
	 * @param numero
	 * @return
	 */
	String getLlaveFiscal(BigDecimal idToPoliza, Integer numeroInciso);
	
	String envioMasivoCorreos(List<PolizaDTO> polizaList, EnviarEmailPorIncisoParameters enviarEmailPorIncisoParameters);
	
	List<ReciboSeycos> listarRecibosPoliza(BigDecimal idToPoliza, BigDecimal programaPago);
	
	String getLlaveFiscalRecibo(BigDecimal idToPoliza, BigDecimal idRecibo);
	
	InputStream imprimirLlaveFiscal(String llaveFiscal);
	
	Map<BigDecimal, String> getMapEndososRecibos(BigDecimal idToPoliza);
	
	Map<BigDecimal, String> getMapProgramaPagos(BigDecimal idToPoliza, BigDecimal numeroEndoso);
	
	Map<BigDecimal, String> getMapIncisosEndoso(BigDecimal idToPoliza, BigDecimal idProgPago);

	List<MovimientoReciboDTO> getMovimientosRecibo(BigDecimal idToPoliza, BigDecimal idRecibo);
	
	void regenerarFiscalRecibo(BigDecimal idToPoliza, BigDecimal idRecibo);
	
	PrintReport getServicioImpresionED();
	
	byte[] getReciboFiscal(String llaveFiscal);
	
	byte[] getReciboFiscal(String llaveFiscal,String type);
	
	List<ReciboSeycos> listarRecibosPolizaInciso(BigDecimal idToPoliza, BigDecimal programaPago, BigDecimal idInciso);
	
	List<MovimientoReciboDTO> getMovimientosReciboInciso(BigDecimal idToPoliza, BigDecimal idRecibo, BigDecimal idInciso);
}
