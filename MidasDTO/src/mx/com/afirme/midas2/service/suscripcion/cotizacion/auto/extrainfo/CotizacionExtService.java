package mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.extrainfo;

import java.math.BigDecimal;

import java.util.List;
import java.util.Map;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.extrainfo.CotizacionExtDTO;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;

public interface CotizacionExtService {
	
	public static final String MensajeExcepcion = "Algunos datos de la cotización provocaron excepciones que requieren ser revisadas";
	
	/**
	 * 
	 * @param cotizacion
	 */
	public void crearCotizacionExt(CotizacionExtDTO cotizacion);
	/**
	 * Crea o actualiza registro en BD 
	 * @param cotizacion
	 */
	public void actualizaCotizacionExt(CotizacionExtDTO cotizacion);
	/**
	 * Crea o actualiza registro en BD 
	 * @param idToCotizacion
	 * @return
	 */
	public CotizacionExtDTO getCotizacionExt(BigDecimal idToCotizacion);
	/**
	 * Recupera el objeto de la cotizacion
	 * @param idToCotizacion
	 * @return
	 */
	public CotizacionDTO getCotizacion(BigDecimal idToCotizacion);
	/**
	 * recupera las Excepciones que una cotizacion genero
	 * @param cotizacion
	 * @return
	 */
	public List<? extends ExcepcionSuscripcionReporteDTO> evaluarCotizacion(CotizacionDTO cotizacion);
	/**
	 * Ejecuta la validacion de Exceciones para la cotizacvion
	 * @param cotizacionDto
	 * @return
	 */
	public Map<String, Object> evalueExcepcion (CotizacionDTO cotizacionDto);
	/**
	 * Ejecuta la validacion de Exceciones para la cotizacvion
	 * @param cotizacionDto
	 * @param armaMensaje
	 * @return
	 */
	public Map<String, Object> evalueExcepcion (CotizacionDTO cotizacionDto, boolean armaMensaje);
}
