package mx.com.afirme.midas2.service.suscripcion.solicitud;

import java.util.Date;
import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.comentarios.Comentario;


public interface SolicitudPolizaService {
	
	public BigDecimal agregarSolicitud(SolicitudDTO solicitud);
	
	public SolicitudDTO actualizar(SolicitudDTO solicitud);
	
	public List<SolicitudDTO> buscarSolicitud(SolicitudDTO solicitud, Date fechaInicial, Date fechaFinal,Long idToNegocioProducto);
	
	public SolicitudDTO findById(BigDecimal id);
	
	public void rechazarSolicitud(SolicitudDTO solicitud);
	
	public void asignarCoordinador(SolicitudDTO solicitud);
	public SolicitudDTO reasignarSuscriptor(SolicitudDTO solicitud);
	
	public List<SolicitudDTO> busquedaRapida(SolicitudDTO filtro);

	public SolicitudDTO asignarSuscriptor(SolicitudDTO solicitud);
	
	/**
	 * Crea una cotizacion de la solicitud
	 * @param solicitud
	 * @return
	 */
	public CotizacionDTO crearCotizacionSolicitud(SolicitudDTO solicitud);
	
	public Long obtenerTotalPaginacion(SolicitudDTO filtro, Date fechaFinal);
	
	public Long obtenerTotalPaginacion(SolicitudDTO filtro);
	
	public Comentario comentario(SolicitudDTO solicitud,Comentario comentario, String valor);
	
	/**
	 * Cambia el estatus de una solicitud a cancelada
	 * @param BigDecimal idToSolicitud
	 * @return id de la solicitud afectada
	 * @author martin
	 */
	public BigDecimal cancelarSolicitud(BigDecimal idToSolicitud);
}
