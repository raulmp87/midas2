package mx.com.afirme.midas2.service.avisos;

import java.math.BigDecimal;

import javax.ejb.Local;

@Local
public interface AvisosService {
	
	public void enviarAviso(BigDecimal idToPoliza, int tipoAviso, Short tipoEndoso);

}
