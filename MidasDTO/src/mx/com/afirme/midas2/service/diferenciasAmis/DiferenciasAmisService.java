package mx.com.afirme.midas2.service.diferenciasAmis;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.poliza.diferenciasamis.DiferenciasAmis;
import mx.com.afirme.midas2.util.MidasException;

public interface DiferenciasAmisService {
	public void guardarDiferenciasAmis(DiferenciasAmis diferenciasAmis);
	public List<DiferenciasAmis> traeLista()  throws MidasException;
	public List<DiferenciasAmis> traeListaFiltros( DiferenciasAmis filtro )  throws MidasException;
	public void solicitarCambio( DiferenciasAmis obj ) throws MidasException;
	public DiferenciasAmis traerDiferenciasObject( Long id ) throws MidasException;
	public Agente traerAgenteObject( BigDecimal idToCotizacion ) throws MidasException;
	public void eliminarDiferenciasAmis(BigDecimal idToPoliza);
	
	public void eliminarDiferenciasAmisPorInciso(BigDecimal idToPoliza, BigDecimal numeroInciso);
	
	public void cambiarEstatus(BigDecimal idToPoliza, Short estatus);
	
	public void cambiarEstatusPorInciso(BigDecimal idToPoliza, Short estatus, BigDecimal numeroInciso);
}
