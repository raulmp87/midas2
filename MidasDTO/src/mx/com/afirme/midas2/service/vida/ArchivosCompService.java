package mx.com.afirme.midas2.service.vida;
import java.math.BigDecimal;

import javax.ejb.Local;

@Local
public interface ArchivosCompService {
	
	String procesarInfo(BigDecimal idToControlArchivo, String tipoArchivo, String fechaCorte);

}
