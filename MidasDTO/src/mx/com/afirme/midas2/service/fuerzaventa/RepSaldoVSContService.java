package mx.com.afirme.midas2.service.fuerzaventa;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.reportesAgente.RepMizarSelectMizar;
import mx.com.afirme.midas2.dto.reportesAgente.SaldosVsContabilidadView;

@Local
public interface RepSaldoVSContService {

	public void insertTPSaldosVsContabilidad(List<RepMizarSelectMizar> lista) throws Exception;

	public void deleteTPSaldosVsContabilidad() throws Exception;
	
	public List<SaldosVsContabilidadView> executeSpSaldosVSContabilidad(String pfechaCorte) throws Exception;
	
	public List<RepMizarSelectMizar> consultaMizar(String anioMes, String FechaString) throws Exception;
	
}
