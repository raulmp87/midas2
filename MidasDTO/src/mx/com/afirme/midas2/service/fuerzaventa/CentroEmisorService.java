package mx.com.afirme.midas2.service.fuerzaventa;

import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import mx.com.afirme.midas2.dto.fuerzaventa.RegistroFuerzaDeVentaDTO;


public interface CentroEmisorService extends FuerzaDeVentaService{
	
	public List<RegistroFuerzaDeVentaDTO> listarCentrosEmisores();
	
	public Map<Long, String> listarCentrosEmisoresMap();
}
