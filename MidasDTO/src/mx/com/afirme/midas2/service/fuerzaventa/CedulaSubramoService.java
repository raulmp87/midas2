package mx.com.afirme.midas2.service.fuerzaventa;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CedulaSubramo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;

public interface CedulaSubramoService {
	public List<CedulaSubramo> findByFilters(CedulaSubramo filtroCedulaSubramo);
	
	public CedulaSubramo loadById(CedulaSubramo cedulaSubramo);
	
	public CedulaSubramo saveFull(CedulaSubramo cedulaSubramo) throws Exception;
	
	public List<RamoDTO> obtenerRamos() throws Exception;
	
	public List<SubRamoDTO> obtenerSubRamosPorRamo(RamoDTO ramo) throws Exception;
	
	public List<CedulaSubramo> obtenerSubRamosAsociadosPorCedula(ValorCatalogoAgentes tipoCedula) throws Exception;
	
	public void guardarAsociacion(List<SubRamoDTO> subRamos,ValorCatalogoAgentes tipoCedula) throws Exception;
}
