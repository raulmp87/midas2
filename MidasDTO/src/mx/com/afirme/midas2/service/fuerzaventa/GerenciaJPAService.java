package mx.com.afirme.midas2.service.fuerzaventa;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.dto.fuerzaventa.GerenciaView;

public interface GerenciaJPAService{
	public Gerencia saveFull(Gerencia gerencia) throws Exception;
	
	public void unsubscribe(Gerencia gerencia)throws Exception;
	
	public List<Gerencia> findByFilters(Gerencia filtroGerencia);
	
	public List<Gerencia> findByCentroOperacion(Long idCentroOperacion);
	
	public Gerencia loadById(Gerencia gerencia);
	
	public List<GerenciaView> findByFiltersView(Gerencia filtroGerencia);
	
	public List<GerenciaView> getList(boolean onlyActive);
	
	public List<GerenciaView> findByCentroOperacionLightWeight(Long idParent);
	
	public List<GerenciaView> findGerenciasConCentroOperacionExcluyentes(List<Long> gerencias,List<Long> centrosOperacionExcluyentes);
	
	public Gerencia loadById(Gerencia gerencia, String fechaHistorico);
}