package mx.com.afirme.midas2.service.fuerzaventa;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProductoBancario;
/**
 * 
 * @author vmhersil
 *
 */
@Local
public interface ProductoBancarioService{
	public List<ProductoBancario> findByFilters(ProductoBancario productoBancario);
	
	public List<ProductoBancario> findByBanco(Long idBanco);
}
