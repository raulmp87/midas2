package mx.com.afirme.midas2.service.fuerzaventa;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ClienteJPA;


public interface ClienteJPAService{
	
	public List<ClienteJPA> findByFilters(ClienteJPA clienteJPA);
	
	
	public ClienteJPA loadById(ClienteJPA clienteJPA);
	
	public ClienteJPA loadById(Long idCliente);
}
