package mx.com.afirme.midas2.service.fuerzaventa.mediciones;

import javax.ejb.Local;

import com.asm.dto.UserDTO;

import mx.com.afirme.midas2.dto.fuerzaventa.mediciones.Reporte;

@Local
public interface MedicionesService {
	
	public Reporte obtenerUltimoReporteSemanal(UserDTO usuario);
	
	public Reporte obtenerUltimoReporteMensual(UserDTO usuario);
	
	public Reporte obtenerPenultimoReporteMensual(UserDTO usuario);
	
	public String obtenerUltimoPeriodoMensual();
	
	public String obtenerPenultimoPeriodoMensual();
		
	public Boolean validarUsuario (UserDTO usuario);
	
	public void enviarCorreosReporteSemanal();
		
}
