package mx.com.afirme.midas2.service.fuerzaventa;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.dto.fuerzaventa.EjecutivoView;

public interface EjecutivoService{
	public List<Ejecutivo> findByGerencia(Long idGerencia);
	
	public List<Ejecutivo> findByFilters(Ejecutivo filtroEjecutivo);
	
	public void unsubscribe(Ejecutivo ejecutivo)throws Exception;
	
	public Ejecutivo saveFull(Ejecutivo ejecutivo) throws Exception;
	
	public Ejecutivo loadById(Ejecutivo ejecutivo);
	
	public List<EjecutivoView> findByFiltersView(Ejecutivo filtroEjecutivo);
	
	public List<EjecutivoView> getList(boolean onlyActive);
	
	public List<EjecutivoView> findByGerenciaLightWeight(Long idParent);
	
	public List<EjecutivoView> findEjecutivosConGerenciasExcluyentes(List<Long> ejecutivos,List<Long> gerenciasExcluyentes,List<Long> centrosExcluyentes);
	
	public List<Ejecutivo> findAllEjecutivoResponsable();
	
	public Ejecutivo loadById(Ejecutivo ejecutivo, String fechaHistorico);
}
