package mx.com.afirme.midas2.service.fuerzaventa;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.DomicilioView;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosDTO;

@Local
public interface AgenteMidasService extends GenericMailService{
	
	public List<Agente> findByFilters(Agente filters);
	
	public List<AgenteView> findByFiltersWithAddressSupport(Agente filtroAgente);
	
	public List<AgenteView> findByFiltersWithAddressSupportPaging(Agente filtroAgente,Integer pagingCount,Long pagingStart);
	
	public List<Agente> findAgentesByPromotoria(Long idPromotoria);
		
	public Agente saveDatosGeneralesAgente(Agente agente) throws Exception;
	
	public Agente saveDomiciliosAgente(Agente agente) throws Exception;
	
	public Agente saveDatosFiscalesAgente(Agente agente) throws Exception;
	
	public Agente saveDatosContablesAgente(Agente agente) throws Exception;
	
	public Agente unsuscribe(Agente agente,String accion) throws Exception;
	
	public List<AgenteView> agentesPorAutorizar(Agente filtroAgente) throws Exception;
	
	public Agente loadById(Agente agente);	
	
	public String[] generateExpedientAgent(Long id) throws Exception;
		
	public boolean validarClaveAgente(Agente agente)throws Exception;
				
	public PersonaSeycosDTO llenarPersonaSeycos(Persona persona);
	
	public void llenarYGuardarListaProductosBancarios(String productosAgregados, Agente agenteInternet);
	
	public boolean llenarYGuardarListaHijosAgente(String productosAgregados, Agente agenteInternet);
	
	public boolean llenarYGuardarListaEntretenimientosAgente(String entretenimientosAgregados, Agente agenteInternet);
	
	public void eliminarDocumentosAgente(Long idAgente);
	
	public void eliminarHijosAgente(Long idAgente);
	
	public void eliminarEntretenimientosAgente(Long idAgente);
	
	public Agente findByClaveAgente(Agente agente)throws Exception;
		
	public void disable(Agente agente) throws Exception;
	
	public void saveInSeycos(Agente agente)throws Exception;
	
	public Agente saveAltaPorInternet(Agente agente) throws Exception;
	
	public Long getMaxRowNumFindByFiltersWithAddressSupport(Agente filtroAgente);
	
	public List<AgenteView> findByFilterLightWeight(Agente filtroAgente);
	public List<AgenteView> findByFilterLightWeight(Map<String, Object> filtroAgente);

	public List<String> cargaMasiva(String url) throws Exception;
	
	public Agente loadByClave(Agente agente);
	
	public Agente loadById(Agente agente, String fechaHistorico);
	
	public <E extends Entidad> List<E> findByPropertyWithHistorySupport(Class<E> entityClass, String propertyName,final Object value, String fechaHistorico);
	
	public Agente findByCodigoUsuario(String codigoUsuario);
	
	public AgenteView findByCodigoUsuarioLightWeight(String codigoUsuario);

	public DomicilioView findDomicilioAgente(Agente agente)throws Exception;
	
	public Agente loadByIdImpresiones(Agente agente);
	
	public Agente loadByClaveSolicitudes(Agente agente);
	
	/*servicio para cambiar el estatus a rechazado a los agentes que esten rechazados con emision y sus dias para emitir hayan vencido*/
	public void rechazarAgentesVenceDiasEmision() throws Exception;
	/**
	 * Servicio utilizado para el web service autoplazo , en el que originacion envia la idsucursal y obtenemos el agente correspondiente
	 * 
	 * @param idSucursal
	 * @return Agente
	 */
	public Agente getAgenteByIdSucursal (Integer idSucursal);
	/**
	 * Servicio utilizado para ver si el agente es sucursal envia la idagente
	 * @param idAgente
	 * @return result es o no sucursal
	 */
	public boolean isSucursal (Integer idAgente);
	
	/**
	 * Devuelve un Map con las direcciones de correo electronico de los superiores correspondientes a la estructura a la que pertenece el Agente.
	 * las llaves para acceder a una direccion en especifico son: PROMOTORIA, EJECUTIVO, GERENCIA, CENTRO_OPERACION
	 * 
	 * @see mx.com.afirme.midas2.service.fuerzaventa.PromotoriaService#obtenerDireccionesCorreoSuperioresPromotoria(Promotoria promotoria)
	 * 
	 * @param agente
	 * @return Map<String, String>
	 */
	public Map<String, String> obtenerDireccionesCorreoSuperioresAgente(Agente agente);
	
	public void initialize();
		
	/**
	 * Revisa si el agente ha sido autorizado alguna vez desde que fue creado su registro
	 * @param id Surrogate key del agente (id del sistema MIDAS)
	 * @return true si el agente ha sido autorizado alguna vez desde que fue creado su registro, false en caso contrario
	 */
	public Boolean isAgenteAutorizadoPreviamente(Long id);
	
	/**
	 * Revisa si el agente es agente promotor
	 * @param id Surrogate key del agente (id del sistema MIDAS)
	 * @return true si el agente es agente promotor, false en caso contrario
	 */
	public boolean isAgentePromotor(Long id);
		
}
