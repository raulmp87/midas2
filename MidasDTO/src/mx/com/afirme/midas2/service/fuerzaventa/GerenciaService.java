package mx.com.afirme.midas2.service.fuerzaventa;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.dto.fuerzaventa.RegistroFuerzaDeVentaDTO;


public interface GerenciaService extends FuerzaDeVentaService{
	
	public List<RegistroFuerzaDeVentaDTO> listarGerencias();
	
	public List<RegistroFuerzaDeVentaDTO> listarGerenciasPorCentroEmisor(Object id);
	
	public List<Gerencia> listarGerenciasPorCentroOperacion(Long idCentroOperacion);

}
