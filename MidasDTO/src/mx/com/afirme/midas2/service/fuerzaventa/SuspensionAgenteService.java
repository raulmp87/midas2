package mx.com.afirme.midas2.service.fuerzaventa;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.SuspensionAgente;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentosAgrupados;
import mx.com.afirme.midas2.dto.fuerzaventa.EntregoDocumentosView;
@Local
public interface SuspensionAgenteService{
	public List<SuspensionAgente> findByFilters(SuspensionAgente filtro) throws Exception;
	
	public SuspensionAgente loadById(Long idSolicitudSuspension) throws Exception;
	
	public SuspensionAgente guardarSolicitudDeSuspension(SuspensionAgente solicitudSuspension) throws Exception;
	
	public SuspensionAgente autorizaSolicitudDeSuspension(SuspensionAgente solicitudSuspension) throws Exception;
	
	public SuspensionAgente rechazaSolicitudDeSuspension(SuspensionAgente solicitudSuspension) throws Exception;
	
	public List<SuspensionAgente> findByFiltersView(SuspensionAgente filtro) throws Exception;
	
	public void auditarDocumentosEntregadosSuspension(Long idSuspension, Long idAgente,	String nombreAplicacion) throws Exception;
	
	public List<EntregoDocumentosView> consultaEstatusDocumentosSuspension(Long idSuspension, Long idAgente) throws Exception;
	
	public List<DocumentosAgrupados> documentosFortimaxGuardadosDBSuspension(Long idSuspension, Long idAgente) throws Exception;
	
	public void crearYGenerarDocumentosFortimaxSuspension(SuspensionAgente suspension)throws Exception;
}
