package mx.com.afirme.midas2.service.fuerzaventa;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProductoBancarioPromotoria;

@Local
public interface ProductoBancarioPromotoriaService{
	public List<ProductoBancarioPromotoria> findByFilters(ProductoBancarioPromotoria filtro);
	
	public List<ProductoBancarioPromotoria> findByPromotoria(Long idPromotoria);
	
	public void delete(Long idProducto) throws Exception;
	
	public ProductoBancarioPromotoria saveProducto(ProductoBancarioPromotoria productoBancarioPromotoria)throws Exception;
	
	public void saveListProductoBanc(List<ProductoBancarioPromotoria> lista) throws Exception;
	
	public List<ProductoBancarioPromotoria> findByPromotoria(Long idPromotoria, String fechaHistorico, String tipoAccion);
}
