package mx.com.afirme.midas2.service.fuerzaventa;

import java.util.List;

import mx.com.afirme.midas2.dto.fuerzaventa.RegistroFuerzaDeVentaDTO;


public interface OficinaService extends FuerzaDeVentaService{

	public List<RegistroFuerzaDeVentaDTO> listarOficinas();
	
	public List<RegistroFuerzaDeVentaDTO> listarOficinasPorGerencia(Object id);
}
