package mx.com.afirme.midas2.service.fuerzaventa;

import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.dto.fuerzaventa.RegistroFuerzaDeVentaDTO;


public interface PromotoriaService extends FuerzaDeVentaService {
	
	public List<RegistroFuerzaDeVentaDTO> listarPromotorias();
	
	public List<RegistroFuerzaDeVentaDTO> listarPromotoriasPorOficina(Object id);
	
	/**
	 * Devuelve un Map con las direcciones de correo electronico de los superiores correspondientes a la estructura a la que pertenece la promotoria.
	 * las llaves para acceder a una direccion en especifico son: EJECUTIVO, GERENCIA, CENTRO_OPERACION
	 * 
	 * @see mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService#obtenerDireccionesCorreoSuperioresAgente(Agente agente)
	 * 
	 * @param promotoria
	 * @return Map<String, String>
	 */
	public Map<String, String> obtenerDireccionesCorreoSuperioresPromotoria(Promotoria promotoria);
}
