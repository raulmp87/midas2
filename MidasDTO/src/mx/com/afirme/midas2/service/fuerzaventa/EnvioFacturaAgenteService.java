package mx.com.afirme.midas2.service.fuerzaventa;

import java.io.File;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.EnvioFacturaAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.EnvioFacturaAgenteDet;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HonorariosAgente;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;


@Local
public interface EnvioFacturaAgenteService {
	public List<EnvioFacturaAgente> findByAgente(Long idAgente) throws Exception;
	public List<EnvioFacturaAgenteDet> findByIdEnvio(Long idEnvio)throws Exception;
	public EnvioFacturaAgente persistEnvio(Long idAgente, File facturaXml, String userName)throws Exception;
	public HonorariosAgente findHonorariosByIdAgente(Long idAgente);
	public EnvioFacturaAgente persistEnvio(Long idAgente, File facturaXml)throws Exception;
	public Agente getDatosAgente(String codigoUsuario, Integer idPersona, Long idAgente) throws Exception;
	public void autorizaExcepcion (Long idAgente);
	public PrestadorServicio getDatosPrestadorServicio(Integer idPersona, Long idProveedor) throws Exception;
	public void marcarFacturasAntiguas(Long idAgente);
	public boolean tieneFacturasAntiguasPendientes(Long idAgente);
}
