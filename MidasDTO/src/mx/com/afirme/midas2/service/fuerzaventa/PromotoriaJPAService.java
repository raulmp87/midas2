package mx.com.afirme.midas2.service.fuerzaventa;

import java.util.List;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.dto.fuerzaventa.PromotoriaView;

public interface PromotoriaJPAService{
	
	public Promotoria saveFull(Promotoria promotoria);
	
	public void unsubscribe(Promotoria promotoria);
	
	public List<Promotoria> findByFilters(Promotoria filtroPromotoria);
	
	public List<Promotoria> findByEjecutivo(Long idEjecutivo);
	
	public Promotoria loadById(Promotoria promotoria);
	
	public List<PromotoriaView> findByFiltersView(Promotoria filtroPromotoria);
	
	public List<PromotoriaView> getList(boolean onlyActive);
	
	public List<PromotoriaView> findByEjecutivoLightWeight(Long idParent);
	
	public List<PromotoriaView> findPromotoriaConEjecutivosExcluyentes(List<Long> promotorias,List<Long> ejecutivosExcluyentes,List<Long> gerenciasExcluyentes,List<Long> centrosExcluyentes);
	
	public Promotoria loadById(Promotoria promotoria, String fechaHistorico);
	
	public List<Promotoria> findByFilters(Promotoria filtroPromotoria, String fechaHistorico);	
	
}
