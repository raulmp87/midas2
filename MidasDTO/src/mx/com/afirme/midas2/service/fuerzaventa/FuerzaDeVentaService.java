package mx.com.afirme.midas2.service.fuerzaventa;

import java.util.List;

import mx.com.afirme.midas2.dto.fuerzaventa.RegistroFuerzaDeVentaDTO;


public interface FuerzaDeVentaService {
	
	/**
	 * Obtiene la lista de registro de fuerza de venta dependiendo de los parametros que se envíen. 
	 * Existe un procedure que recibe todos los parámetros y el tipo de búsqueda que se desea hacer.
	 * Por ejemplo si se manda un id de centro emisor y se pone el tipo necesario para agentes,
	 * se retornaría la lista de agentes para ese centro emisor
	 * @param centroOperacionId
	 * @param oficinaId
	 * @param gerenciaId
	 * @param promotoriaId
	 * @param tipo
	 * @return
	 */
	public List<RegistroFuerzaDeVentaDTO> listar(Object centroOperacionId, Object oficinaId, Object gerenciaId, Object promotoriaId, String tipo);
	
	/**
	 * Obtener lista de fueza de venta dependiendo de la combinacion de  parametros.
	 *
	 * Si se manda <code>null</code> en id y tipoPadre se obtendra toda la lista para el tipo de busqueda definido.
	 *  
	 * Si se manda una <code>A</code> en tipoBusqueda obtendra lista de agentes
	 * Si se manda una <code>P</code> en tipoBusqueda obtendra lista de promotorias
	 * Si se manda una <code>O</code> en tipoBusqueda obtendra lista de oficina
	 * Si se manda una <code>G</code> en tipoBusqueda obtendra lista de gerencia
	 * @param id Identificador para el padre de esta lista
	 * @param tipoPadre tipo de padre que se necesita (P - prmotoria, O - oficina , G - gerencia, C - centro operacion) 
	 * @param tipo Tipo de busqueda que se desea realizar
	 * @return
	 */
	public List<RegistroFuerzaDeVentaDTO> listar(Object id, String tipoPadre, String tipoBusqueda);

}
