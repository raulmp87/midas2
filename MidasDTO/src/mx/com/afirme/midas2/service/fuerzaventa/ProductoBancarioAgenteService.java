package mx.com.afirme.midas2.service.fuerzaventa;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProductoBancarioAgente;

@Local
public interface ProductoBancarioAgenteService{
	public List<ProductoBancarioAgente> findByFilters(ProductoBancarioAgente filtro);
	
	public List<ProductoBancarioAgente> findByAgente(Long idAgente);
	
	public void delete(Long idProducto) throws Exception;
	
	public List<ProductoBancarioAgente> findByAgente(Long idAgente, String fechaHistorico);
}
