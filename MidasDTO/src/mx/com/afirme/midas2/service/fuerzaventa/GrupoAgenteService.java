package mx.com.afirme.midas2.service.fuerzaventa;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.GrupoAgente;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;

/**
 * 
 * @author Efren Rodriguez
 *
 */

@Local
public interface GrupoAgenteService {
	
	public GrupoAgente loadById(GrupoAgente grupoAgenteDTO);
	public List<GrupoAgente> findByFilters(GrupoAgente filtro);
	public boolean deleteGrupo(GrupoAgente filtro);
	public List<Object[]> obtenerAgentesAsociadosEnGrupo(GrupoAgente tipoGrupoAgenteDTO);
	public void guardarAgentesAsociadosEnGrupo(Long idGrupo, String idsAgentes);
	public List<Object[]> filtrarAgentesDeGrupo(Long idGrupo, String clave, String nombre, String gerencia);
	public List<AgenteView> filtrarAgente(Agente filtroAgente);
}
