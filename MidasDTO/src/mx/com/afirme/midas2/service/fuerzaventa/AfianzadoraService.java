package mx.com.afirme.midas2.service.fuerzaventa;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Afianzadora;

public interface AfianzadoraService{
	public Afianzadora saveFull(Afianzadora afianzadora) throws Exception;
	
	public void unsubscribe(Afianzadora afianzadora)throws Exception;
	
	public List<Afianzadora> findByFilters(Afianzadora filtroAfianzadora);
	
	public List<Afianzadora> findByFiltersView(Afianzadora filtroAfianzadora);
	
	public Afianzadora loadById(Afianzadora afianzadora);
	
	public Afianzadora loadById(Afianzadora afianzadora, String fechaHistorico);
}
