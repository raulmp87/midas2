package mx.com.afirme.midas2.service.fuerzaventa;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.dto.fuerzaventa.CentroOperacionView;

public interface CentroOperacionService{
	public CentroOperacion saveFull(CentroOperacion centroOperacion) throws Exception;
	
	public void unsubscribe(CentroOperacion centroOperacion)throws Exception;
	
	public List<CentroOperacion> findByFilters(CentroOperacion filtroCentroOperacion);
	
	public List<CentroOperacionView> findByFiltersView(CentroOperacion filtroCentroOperacion);
	
	public CentroOperacion loadById(CentroOperacion centroOperacion);
	
	public CentroOperacion loadById(CentroOperacion centroOperacion, String fechaHistorico);
	
	public List<CentroOperacionView> getList(boolean onlyActive);
}
