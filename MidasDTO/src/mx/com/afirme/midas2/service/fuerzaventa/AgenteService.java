package mx.com.afirme.midas2.service.fuerzaventa;

import java.util.List;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.RegistroFuerzaDeVentaDTO;


public interface AgenteService extends FuerzaDeVentaService {
	
	public List<RegistroFuerzaDeVentaDTO> listarAgentesPorPromotoria(Object id);
	
	public List<RegistroFuerzaDeVentaDTO> listarAgentesPorGerencia(Object id);
	
	public List<RegistroFuerzaDeVentaDTO> listarAgentesPorOficina(Object id);
	
	public List<RegistroFuerzaDeVentaDTO> listarAgentesPorCentroEmisor(Object id);
	
	public AgenteDTO obtenerAgente(Integer idTcAgente);
	
	/**
	 * Obtiene la lista completa de agentes
	 * @return
	 */
	public List<AgenteDTO> listarAgentes();
	
	/**
	 * Lista los agentes de Seycos por los parametros enviados. 
	 * En caso de que todos nulos obtendra el listado completo
	 * @param centroEmisorId
	 * @param gerenciaId
	 * @param oficinaId
	 * @param promotoriaId
	 * @return
	 */
	public List<AgenteDTO> listarAgentes(Object centroOperacionId, Object gerenciaId, Object oficinaId, Object promotoriaId);
	
	/**
	 * Obtiene la lista de agentes que están bajo el mando del usuario dado
	 * @param nombreUsuario
	 * @return
	 */
	public List<AgenteDTO> listarAgentes(String nombreUsuario);
}
