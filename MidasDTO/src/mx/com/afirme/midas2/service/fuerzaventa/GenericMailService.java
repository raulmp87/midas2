package mx.com.afirme.midas2.service.fuerzaventa;

import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
/**
 * Interface que define el comportamiento del envio de correos
 * @author Martin
 * 05/12/2012
 */
public interface GenericMailService {
	public static final String PARA = "Para";
	public static final String COPIA = "C.C.";
	public static final String COPIA_OCULTA = "C.CO.";
	public static final String NOTA = "notas";
	public static int T_GENERAL = -1;
	public static int T_BIENVENIDO = 0;
	public static int T_FELIZ_ANIVERSARIO = 1;
	public static int T_FELIZ_ANIVERSARIO_BODA = 2;
	public static int T_FELIZ_CUMPLEANIOS = 3;
	public static int T_META_CUMPLIDA = 4;
	public static Long P_ALTA_AGENTE  = 1l;
	public static Long P_PAGO_COMISIONES = 2L;
	public static Long P_PAGO_BONOS = 3L;
	public static Long P_PROVISION = 4l;
	public static Long P_CEDULA_VENCIDA = 5l;
	public static Long P_CUMPLEANIOS  = 10l;
	public static Long P_ANIVERSARIO = 11l;
	public static Long P_ANIVERSARIO_BODA = 12l;
	public static Long P_PRESTAMOS_ANTICIPOS = 7l;
	public static Long P_FIANZA_VENCIDA = 6l;
	public static Long M_CUMPLIMIENTO_DEFECHA = 22l;
	public static Long M_CUMPLIMENTO_FECHA_ANIVERSARIO_BODA=24l;
	public static Long M_CUMPLIMENTO_FECHA_ANIVERSARIO_AGENTE=23l;
	public static Long M_ALTA_REGISTRO = 1l;
	public static Long M_AUTORIZACION_DEL_MOVIMIENTO_ALTA_AGENTE = 2l;
	public static Long M_RECHAZO_ALTA_AGENTE = 3l;
	public static Long M_GENERACION_DE_PREVIEW = 4l;
	public static Long M_AUTORIZACION_DEL_MOVIMIENTO_COMISION = 5l;
	public static Long M_AUTORIZACION_DEL_MOVIMIENTO_BONO = 8l;
	public static Long M_RECHAZO_DEL_MOVIMIENTO = 6l;
	public static Long M_DOCUMENTO_POR_VENCER = 13l;
	public static Long M_DOCUMENTO_VENCIDO = 14l;
	public static Long M_FIANZA_DOCUMENTO_POR_VENCER = 15L;
	public static Long M_FIANZA_DOCUMENTO_VENCIDO = 16L;
	public static Long M_PRESTAMOS_ANTICIPOS_ALTA_REGISTO = 17l;
	public static Long M_PRESTAMOS_ANTICIPOS_AUTORIZACION_DEL_MOVIMIENTO = 18l;
	public static Long M_PRESTAMOS_ANTICIPOS_RECHAZO_DEL_MOVIMIENTO = 19l;
	public static Long M_PGO_COMISIONES_AUTORIZACION_DEL_MOVIMIENTO = 5l;
	public static Long M_PROVISION_ALTA_REGISTRO = 4l;
	public static Long M_PROVISION_AUTORIZACION_DEL_MOVIMIENTO = 10l;
	public static Long M_PROSION_RECHAZO_DEL_MOVIMIENTO = 12l;
	
	/**
	 * Obtiene un mapa con los correos que se tienen que enviar
	 * 
	 * @param id
	 * @param proceso
	 * @param movimiento
	 * @return Map<String,String>
	 */
	public Map<String,Map<String, List<String>>> obtenerCorreos(Long id, Long idProceso,
			Long idMovimiento);
	/**
	 * Envia los correos
	 * @param mapCorreos
	 */
	public void enviarCorreo(Map<String,Map<String, List<String>>> mapCorreos,String mensaje,String tituloMensaje,int tipoTemplate,List<ByteArrayAttachment> attachment);	
	/**
	 * Ejecuta el envio de correos en un segundo plano se usa en envios masivos
	 * de correo
	 * 
	 * @param id
	 * @param idProceso
	 * @param idMovimiento
	 * @param mensaje
	 * @param tipoTemplate
	 */
	public void mailThreadMethodSupport(Long id,Long idProceso,Long idMovimiento,String mensaje,int tipoTemplate,String methodToExcecute);
}
