package mx.com.afirme.midas2.service.fuerzaventa.mediciones.conf;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.mediciones.conf.ConfiguracionMedicionesAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.mediciones.conf.ConfiguracionMedicionesPromotoria;

@Local
public interface ConfiguracionMedicionesService {

	
	public void actualizarAgente (Long claveAgente, String correo);
	
	
	public void actualizarPromotoria (Long clavePromotoria, String correo);
	
	
	public void agregarAgente (ConfiguracionMedicionesAgente configAgente);
	
	
	public void agregarCorreosAdicionales(String correosAdicionales);
		
	
	public void agregarPromotoria (ConfiguracionMedicionesPromotoria configPromotoria);
	
	
	public void eliminarAgente (Long claveAgente);
	
	
	public void eliminarPromotoria (Long clavePromotoria);
		
	
	public void habilitarAgente (Long claveAgente);
	
	
	public void habilitarPromotoria (Long clavePromotoria);
	
	
	public String obtenerCorreosAdicionales();
	
	
	public String obtenerRutaBaseReportes();
	
	
	public String obtenerUsuarioRutaBaseReportes();
	
	
	public String obtenerPasswordRutaBaseReportes();
	
	
	public ConfiguracionMedicionesAgente getConfigAgente(Long claveAgente);
	
	
	public ConfiguracionMedicionesPromotoria getConfigPromotoria(Long clavePromotoria);
		
	
}
