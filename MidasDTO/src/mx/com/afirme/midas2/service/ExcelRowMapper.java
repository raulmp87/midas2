package mx.com.afirme.midas2.service;

import java.util.Map;

/**
 * @author amosomar
 * @param <T>
 */
public interface ExcelRowMapper<T> {

	T mapRow(Map<String, String> row, int rowNum); 
}
