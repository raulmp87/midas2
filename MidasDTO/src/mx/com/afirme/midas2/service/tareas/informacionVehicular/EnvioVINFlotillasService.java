package mx.com.afirme.midas2.service.tareas.informacionVehicular;

import javax.ejb.Local;

@Local
public interface EnvioVINFlotillasService {
	
	public void enviarVINFlotillas();
	
	public void initialize();
}