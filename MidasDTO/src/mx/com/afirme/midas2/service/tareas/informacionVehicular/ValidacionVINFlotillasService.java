package mx.com.afirme.midas2.service.tareas.informacionVehicular;

import javax.ejb.Local;

@Local
public interface ValidacionVINFlotillasService {
	
	public void enviarVINFlotillas();
	
	public void validarVINFlotillas();
	
	public void initialize();
}
