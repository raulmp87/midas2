package mx.com.afirme.midas2.service.NotificacionesCierreMes;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.movil.cliente.CorreosCosultaMovil;
import mx.com.afirme.midas2.domain.sistema.parametro.ParametroGlobal;

@Local
public interface NotificacionCierreMesService {

	public List<CorreosCosultaMovil> getCorreosLista();

	public void saveNotificacionMes(ParametroGlobal pGlobal);

	public void initialize();

	public void cancelTimer(String nameTimer);
	
	public String convertFecha(String day);
}
