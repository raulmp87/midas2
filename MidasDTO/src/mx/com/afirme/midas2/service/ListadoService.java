package mx.com.afirme.midas2.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.ServVehiculoLinNegTipoVeh;
import mx.com.afirme.midas2.domain.catalogos.ValorSeccionCobAutos;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProductoBancario;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.DatosSeycos;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.tipovigencia.NegocioTipoVigencia;
import mx.com.afirme.midas2.domain.negocio.tipovigencia.TipoVigencia;
import mx.com.afirme.midas2.domain.personadireccion.ColoniaMidas;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifaId;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.GenericaAgentesView;


public interface ListadoService {
	
	public Map<BigDecimal, String> getMapMonedaPorNegTipoPoliza(BigDecimal idToNegTipoPoliza);
	
	public Map<Long, String> getMapNegocioPaqueteSeccionPorLineaNegocio(BigDecimal idToNegSeccion); 
	
	public Map<BigDecimal, String> getMapNegocioSeccionPorTipoPoliza(BigDecimal idToTipoPoliza);
	
	public Map<Long, String> getMapNegocioPaqueteSeccionPorSeccion(BigDecimal idToSeccion);
	
	public Map<Long, String> getMapPaquetesPorSeccion(BigDecimal idToSeccion);
	
	public Map<BigDecimal, String> getMapCoberturasSeccionPorSeccion(BigDecimal idToSeccion); 
	
	public Map<BigDecimal, String> getMapTipoUsoVehiculoPorSeccion(BigDecimal idToSeccion);
	
	public Map<Short, String> getMapTipoLimiteSumaAseg();
	
	public List<MonedaDTO> getListarMonedas();
	
	public List<ValorSeccionCobAutos> getListarValorSeccionCobAutosPorSeccion(Long idToSeccion);
	
	public List<SeccionDTO> getListarSeccionesVigentesAutos();
	
	public List<SeccionDTO> getListarSeccionesVigentesAutosUsables();
	
	public Map<AgrupadorTarifaId,String> getMapAgrupadorTarifaPorNegocio(String claveNegocio);
																							   
	public Map<BigDecimal, String> getMapLineaNegocioPorAgrupadorTarifa(String idToAgrupadorTarifaFromString);

	public Map<Short, String> getMapMomendaPorAgrupadorTarifaLineaNegocio(String idToAgrupadorTarifaFromString, BigDecimal idToSeccion);
	
	public Map<Short, String> getMapMonedaPorAgrupadorTarifa(String idToAgrupadorTarifaFromString);
	
	public Map<BigDecimal, String> getMapTipoVehiculoPorSeccion(BigDecimal idToSeccion);
	
	public Map<BigDecimal, String> getMapTipoServicioVehiculoPorTipoVehiculo(BigDecimal idTipoVehiculo);
	
	public List<ServVehiculoLinNegTipoVeh> getListarServVehiculoLinNegTipoVehPorSeccion(Long idToSeccion);
	
	public String getClaveTipoDetalleGrupo(Long idGrupo);
	
	public Map<BigDecimal, String> getMapAgrupadoresPorNegocio(String claveNegocio, Long idMoneda);
	
	public List<ProductoDTO> getListarProductos(String claveNegocio,boolean mostrarInactivos);
	
	public Map<BigDecimal, String> getMapProductos(Long idToNegocio, Integer activo);
	
	//public Map<Object, String> getListarProductos(Long idNegocio,Integer activo);
	
	public Map<Object, String> listarCentrosEmisores();
	
	public Map<Object, String> listarGerenciasPorCentroEmisor(String id);
	
	public List<Gerencia> listarGerenciasPorCentroOperacion(String id);

	public List<Ejecutivo> listarEjecutivoPorGerencia(String id);

	public List<Promotoria> listarPromotoriaPorEjecutivo(String id);
	
	public List<ValorCatalogoAgentes> listarTipoPromotoriaByPromotoria(String id);
	
	public Map<Object, String> listarGerencias();
	
	public Map<Object, String> listarOficinasPorGerencia(String id);
	
	public Map<Object, String> listarPromotoriasPorOficina(String id);
	
	public Map<Object, String> listarAgentes();
	
	public Map<Object, String> listarAgentesPorPromotoria(String id);
	
	public Map<Object, String> listarAgentesPorGerencia(String id);
	
	public Map<Object, String> listarAgentesPorOficina(String id);
	
	public Map<Object, String> listarCveTipoBien();
	
	public Map<BigDecimal,String> listarMarcasPorCveTipoBien(String claveTipoBien);
	
	public Map<String,String> listarEstilosPorMarcaCveTipoBien(BigDecimal idTcMarcaVehiculo,String claveTipoBien);
	
	public Map<BigDecimal,String> listarTiposVehiculo(String claveTipoBien);
	
	/**
	 * Obtiene un listado de los estados
	 * relacionados con un negocio
	 * @param idToNegocio
	 * @return List<NegocioEstado>
	 * @author martin
	 */
	public Map<String, String> getEstadosPorNegocioId(Long idToNegocio, Boolean cotizacionExpress);
	
	public List<EstadoDTO> listarEstadosMX();
	
	public Map<String, String> getMapEstadosMX();
	
	public Map<String, String> getMunicipiosPorEstadoId(BigDecimal idToCotizacion,String idToEstado);
	
	public Map<String, String> getMapMunicipiosPorEstado(String stateId);
	
	public Map<Object, String> listarOficinas();
	
	public Map<BigDecimal, String> getMapSeccionPorTipoPoliza(BigDecimal idToTipoPoliza);
	
	public Map<BigDecimal, String> getMapTipoPolizaPorProducto(BigDecimal idToProducto);
	
	/**
	 * Listado de Coberturas de NegCobPaqSeccion con id IdToNegCobPaqSeccion
	 * @param idToNegPaqueteSeccion
	 * @return
	 */
	public Map<BigDecimal, String> getMapNegocioCoberturaPaqueteSeccionPorPaquete(Long idToNegPaqueteSeccion);
	
	/**
	 * Listado de coberturas de NegCobPaqSeccion con id cobertura 
	 * @param idToNegPaqueteSeccion
	 * @return
	 */
	public Map<BigDecimal, String> getMapCoberturaPorNegPaqueteSeccion(Long idToNegPaqueteSeccion);
	
	/**
	 * Litado de coberturas de NegCobPaqSeccion por linea
	 * @param idToNegocio
	 * @return
	 */
	public Map<BigDecimal, String> getMapCoberturaPorNegocioSeccion(Long idToNegocio);
	
	@Deprecated
	public Map<BigDecimal, String> getMapMarcaVehiculoPorTipoUsoNegocioSeccion(BigDecimal idToNegSeccion);
	
	public Map<BigDecimal, String> getMapMarcaVehiculoPorNegocioSeccion(BigDecimal idToNegSeccion);
	
	public Map<String, String> getMapMarcaVehiculoPorNegocioSeccionString(BigDecimal idToNegSeccion); 
 
	public Map<BigDecimal, String> getMapTipoUsoVehiculoPorEstiloVehiculo(String estiloVehiculoId,  BigDecimal idToNegSeccion);
	
	public Map<String, String> getMapTipoUsoVehiculoPorEstiloVehiculoString(String estiloVehiculoId, BigDecimal idToNegSeccion);
 	
	// Metodos para La parte de recuperacion de Mapas de los Combos de Negocio
	
	public Map<Long, String> listarNegociosActivos(String cveNegocio);
	
	public Map<Long, String> getMapNegProductoPorNegocio(Long idToNegocio);
	public Map<String, String> getMapNegProductoPorNegocioString(Long idToNegocio);
	
	public Map<BigDecimal, String> getMapNegTipoPolizaPorNegProducto(Long idToNegProducto);
	
	public Map<BigDecimal, String> getMapNegSeccionPorNegTipoPoliza(BigDecimal idToNegTipoPoliza);
	
	public Map<Long, String> getMapNegPaqueteSeccionPorNegSeccion(BigDecimal idToNegSeccion);
	
	public Map<BigDecimal, String> getMapNegCoberturaPaquetePorNegPaqueteSeccion(Long idToNegPaqueteSeccion);
	
	public Map<Long, String> getMapNegocioPaqueteSeccionPorLineaNegocioCotizacionInciso(BigDecimal idToCotizacion,BigDecimal idToNegSeccion);
	
	@Deprecated
	public Map<Short, Short> getMapModeloVehiculoPorTipoUsoEstilo(BigDecimal idMoneda, String claveEstilo, BigDecimal idToNegSeccion);
	public Map<Short, Short> getMapModeloVehiculoPorMonedaEstiloNegocioSeccion(BigDecimal idMoneda, String claveEstilo, BigDecimal idToNegSeccion);
	public Map<String, String> getMapModeloVehiculoPorMonedaEstiloNegocioSeccionString(BigDecimal idMoneda, String claveEstilo, BigDecimal idToNegSeccion);
	
	public Map<Long, String> listarNegociosPorAgente(Integer idTcAgente);
	
	/**
	 * Busca los negocios asingados a un agente incluyendo los negocios que aplican para todos los agentes.
	 * @param idAgente
	 * @param claveNegocio
	 * @return
	 */
	public Map<Long, String> getNegociosPorAgente(Integer idAgente, String claveNegocio);
	
	/**
	 * Busca los negocios asignados a un agente incluyendo los negocios que aplican para todos los agentes.
	 * Aplica unicamente para los negocios de autos
	 * @param idAgente
	 * @return
	 */
	public Map<Long, String> getNegociosPorAgenteParaAutos(Integer idAgente);
	
	/**
	 * Lista negocios por agente, cvetiponegocio, y estatus. La claveTipo y estatus pueden ser 
	 * <code>null</code>, con lo que la busqueda se hara solo para el idagente
	 * @param idTcAgente
	 * @param cveTipoNegocio
	 * @param status
	 * @return
	 */
	public Map<Long, String> listarNegociosPorAgente(Integer idTcAgente, String cveTipoNegocio, Integer status);
	
	@Deprecated
	public Map<String, String> getMapEstiloVehiculoPorMarcaVehiculo(BigDecimal idTcMarcaVehiculo, BigDecimal idToNegSeccion, BigDecimal idMoneda);
	public Map<String, String> getMapEstiloVehiculoPorMarcaVehiculoNegocioSeccionMoneda(BigDecimal idTcMarcaVehiculo, BigDecimal idToNegSeccion, BigDecimal idMoneda);
	public Map<String, String> getMapEstiloVehiculoPorMarcaVehiculoNegocioSeccionMoneda(BigDecimal idTcMarcaVehiculo, 
			BigDecimal idToNegSeccion, BigDecimal idMoneda, Long autoIncisoContinuityId);
	public  Map<String, String> getMapEstiloVehiculoPorMarcaVehiculoNegocioSeccionMonedaByDescription(Integer modelo, String descripcionVehiculo, BigDecimal idToNegSeccion);
	
	/**
	 * Lista de vehiculos por descripcion de estilos 
	 * @param estilo
	 * @return
	 */
	public List<EstiloVehiculoDTO> listarEstilosPorDescripcion(String estilo);
	
	public List<EstiloVehiculoDTO> listarEstilosPorDescripcion(String tipoBien, String descripcion, BigDecimal idToNegSeccion, BigDecimal idMoneda);
	
	public List<EstiloVehiculoDTO> listarEstilosPorDescripcion(String claveTipoBien,String descripcion, BigDecimal idToNegSeccion);
	
	public Map<Long, String> getMapPaquetePorNegocioSeccion(BigDecimal idToNegSeccion);
	
	public Map<Integer, String> getMapFormasdePago(CotizacionDTO cotizacion);
	
	public Map<Integer, String> getMapFormasdePago(SolicitudDTO solicitudDTO);
	
	@Deprecated
	public List<AgenteDTO> listarAgentesPorDescripcion(String arg0);
	
	public List<Agente> listaAgentesPorDescripcion(String descripcion);
	
	public List<AgenteView> listaAgentesPorDescripcionLightWeight(String descripcion);
	
	public List<AgenteView> listaAgentesPorDescripcionLightWeightAutorizados(String descripcion);
	
	public List<Negocio> listaNegociosPorDescripcion(String descripcion);
		
	public Map<Long, Double> getMapDerechosPoliza(CotizacionDTO cotizacion);
	
	public Map<Long, String> getMapDerechosEndoso(SolicitudDTO solicitud);

	/**
	 * Listados de catalogos por valor fijo (midas 1)
	 * @param catalogId
	 * @return
	 */
	public Map<Object, String> listarCatalogoValorFijo(Integer catalogId);
	
	public Map<Object, String> listarLineasDeNegocioPorCotizacionId(BigDecimal idToCotizacion);
		
	public Set<String> listarIncisosDescripcionByCotId(BigDecimal idToCotizacion);
		
	/**
	 * Listados de catalogos cascadeados de pais, estado, ciudad y colonia
	 */
	public Map<String, String> getMapPaises();
	
	public Map<String, String> getMapEstados(String idPais);
		
	public Map<String, String> getMapColonias(String idMunicipio);
	/**
	 * Obtiene la lista de colonias con un mapa de nombre de colonias como id y valor
	 * @param idMunicipio
	 * @return
	 */
	public Map<String, String> getMapColoniasSameValue(String idMunicipio);
	
	public String getCodigoPostal(String idColonia);
	/**
	 * Obtiene el codigo postal de una colonia por su nombre dependiendo de su municipio
	 * @param colonia
	 * @param idMunicipio
	 * @return
	 */
	public String getCodigoPostalByColonyNameAndCityId(String colonia,String idMunicipio);
	
	/**
	 * Valida si datos de riesgo estan completos para nivel cotizacion
	 *
	 */
	public boolean validaDatosRiesgosCompletosCotizacion(BigDecimal idToCotizacion, BigDecimal numeroInciso);
	
	/**
	 * Valida si el inciso tiene los datos de riesgo completos dependiendo del nivel de configuracion
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param nivelConfiguracionRiesgos
	 * @return
	 */
	public boolean validaDatosRiesgoCompletos(BigDecimal idToCotizacion, BigDecimal numeroInciso, Short nivelConfiguracionRiesgos);
		
	/**
	 * Valida si el inciso requiere datos de riesgo dependiendo del nivel de configuracion
	 * @param idToCotizacion
	 * @param numeroInciso
	 * @param nivelConfiguracionRiesgos
	 * @return
	 */
	public boolean requiereDatosRiesgoInciso(BigDecimal idToCotizacion, BigDecimal numeroInciso, Short nivelConfiguracionRiesgos);
	
	/**
	 * Valida si se requieren datos de riesgo para un inciso al nivel configurado
	 * @author maritn
	 */
	public boolean requiereDatosRiesgoIncisoCotizacion(BigDecimal idToCotizacion, BigDecimal numeroIncisos);
	
	/**
	 * Carga la lista de gerencias por centro de operacino por cascadeo
	 * @param idCentroOperacion
	 * @return
	 */
	public Map<Long,String> getMapGerenciasPorCentroOperacion(Long idCentroOperacion);
	
	/**
	 * Carga la lista de ejecutivos por gerencia por cascadeo
	 * @param idGerencia
	 * @return
	 * @throws Exception
	 */
	public Map<Long,String> getMapEjecutivosPorGerencia(Long idGerencia);
	
	/**
	 * Carga la lista de todos los ejecutivos/oficinas
	 * @param idGerencia
	 * @return
	 * @throws Exception
	 */
	public Map<Long,String> getMapEjecutivos();
	
	
	/**
	 * Carga la lista de promotorias por ejecutivo por cascadeo
	 * @param idEjecutivo
	 * @return
	 */
	public Map<Long,String> getMapPromotoriasPorEjecutivo(Long idEjecutivo);
	
	
	/**
	 * Carga la lista de agente por promotoria por cascadeo
	 * @param idEjecutivo
	 * @return
	 */
	public Map<Long,String> getMapAgentesPorPromotoria(Long idPromotoria);
	
	
	/**
	 * Carga la lista de agente por gerencia, oficina y promotoria por cascadeo
	 * @param idEjecutivo
	 * @return
	 */
	public Map<Long,String> getMapAgentesPorGerenciaOficinaPromotoria(Long gerencia, Long oficina, Long idPromotoria, Short soloAutorizados);
	
	
	
	public Map<Long, String> getMapMonedas();
	
	public Map<BigDecimal, String> getMapAgrupadoresPorMonedaPorSeccion(String claveNegocio, BigDecimal idSeccion, Long idMoneda);
	
	public Map<Long, String> getMapGerencias();
	
	public Map<Long, String> getMapPromotorias();
	
	public List<ProductoBancario> getListaProductosBancarios();
	
	public List<ProductoBancario> getListaProductosBancariosPorBanco(Long idBanco);
	
	public Map<BigDecimal,Integer> getListaVersionesPorCodigoProducto(String codigo);
	
	/**
	 * Obtiene la lista de cuentas/tc/clabes guardadas por cliente
	 * @param idCliente
	 * @param idConductoCobro
	 * @return
	 */
	public Map<Long, String> getConductosCobro(Long idCliente, Integer idTipoConductoCobro);
	
	/**
	 * Busca el <code>NegocioTipoUso</code> default para el
	 * <code>NegocioSeccion</code> dado.
	 * 
	 * @param negocioSeccion
	 * @return el <code>NegocioTipoUso</code> default o null
	 */
	public BigDecimal getTipoUsoDefault(BigDecimal negocioSeccion);

	/**
	 * Regresa una Map con los tipos de uso por <code>NegocioTipoUso</code>
	 * @param idToNegSeccion
	 * @return
	 */
	public Map<BigDecimal, String> getMapTipoUsoVehiculoByNegocio (BigDecimal idToNegSeccion);

	/**
	 * Busca el <code>NegocioTipoServicio</code> default para el
	 * <code>NegocioSeccion</code> dado.
	 * 
	 * @param negocioSeccion
	 * @return el <code>NegocioTipoServicio</code> default o null
	 */
	public BigDecimal getTipoServicioDefault(BigDecimal negocioSeccion);
	
	public Long getCountNegocioEstiloVehiculo(BigDecimal idToNegocioSeccion,BigDecimal idMoneda);
	
	/**
	 * Obtiene el id del estado por medio del codigo postal
	 * @param cp
	 * @return id del estado
	 */
	public String getEstadoIdPorCp(String cp);
	
	/**
	 * Obtiene el porcentaje de fraccionado dependiendo del id forma pago
	 * @param idFormaPago
	 * @return
	 */
	public Double getPctePagoFraccionado(Integer idFormaPago, Short idMoneda);
	
	public Map<Long, String> getMapNegocioPaqueteSeccionPorLineaNegocio(short claveTipoEndoso, Long cotizacionContinuityId, Date validoEn, BigDecimal idToNegSeccion);
	
	public Map<BigDecimal, String> getMapNegTipoPolizaPorNegProductoFlotillaOIndividual(Long idToNegProducto, int aplicaFlotilla);
	public Map<String, String> getMapNegTipoPolizaPorNegProductoFlotillaOIndividualString(Long idToNegProducto, int aplicaFlotilla);
	
	/**
	 * Obtiene todas las versiones de un agrupador
	 * @param idToAgrupadorTarifa
	 * @return
	 */
	public Map<BigDecimal,String> getAgrupadorTarifaById(BigDecimal idToAgrupadorTarifa);
	
	public Map<BigDecimal, String> getMapNegTipoPolizaAutoExpediblePorNegProducto(Long idToNegProducto);
	
	public Long getNegocioProducto(Long idToNegocio, BigDecimal idToProducto);
	
	public Map<String, String> getPromocionesTC(String numTarjeta);
	/**
	 * 
	*/
	
	public List<GenericaAgentesView> listaProducto(String listaId, String idSiguienteLista);
	
	public List<GenericaAgentesView> listaProducto(String listaId);
	
	public List<GenericaAgentesView> listaRamo(String listaId, String idSiguienteLista);

	public List<GenericaAgentesView> listaRamo(String listaId);
	
	public List<GenericaAgentesView> listaSubramo(String listaId, String idSiguienteLista);

	public List<GenericaAgentesView> listaSubramo(String listaId);
	
	public List<GenericaAgentesView> listaLineaNegocio(String listaId, String listaId2, String idSiguienteLista);
	
	public List<GenericaAgentesView> listaLineaNegocio(String listaId, String listaId2);
	
	public List<GenericaAgentesView> listaCoberturas(String listaId, String idSiguienteLista);
	
	public List<GenericaAgentesView> listaCoberturas(String listaId);
	
	public int obtieneNumeroDeRiesgosBitemporal(Long incisoContinuityId, Date validoEn, Short tipoEndoso);
	
	public boolean mostrarDatosConductor(Long incisoContinuityId, Date validoEn);
	
	public Map<Long,String> getMapEjecutivosResponsable();
	
	public Map<Integer, String> listadoMotivoEndoso(Long tipoEndoso, Long polizaId);
	
	public boolean validaDatosComplementariosIncisoBorrador(Long polizaId, Long incisoContinuityId, Short claveTipoEndoso, Date validoEn);
	
	public List<EstiloVehiculoDTO> listarEstilosPorDescripcionClaveTipoBien(String arg0, String cveTipoBien);
	
		public Map<Long, String> getMapMovimientosPorProceso(BigDecimal idProceso);
	/**
	 * Obtiene el listado de elementos de un catalogo de ValorCatalogoAgentes
	 * @param catalogo
	 * @return
	 */
	public List<ValorCatalogoAgentes> getCatalogoAgentes(String catalogo);
	/**
	 * Obtiene el id del elemento catalogo
	 * @param catalogo
	 * @param valor
	 * @return
	 */
	public Long getIdElementoValorCatalogoAgentes(String catalogo,String valor);
	
	/**
	 * Obtiene el mapa de los elementos de un catalogo de ValorCatalogoAgentes
	 * @param nombreCatalogo
	 * @return
	 */
	public Map<Long,String> mapValorCatalogoAgente(String nombreCatalogo);
	
	/**
	 * Retorna un mapa con los meses
	 */
	public Map<Integer,String> getMapMonths();
	
	/**
	 * Regresa un rago +/- de fechas apartir del año actual
	 * Ej. retorna 2010, 2011.., año actual si el rango es 2
	 */
	public Map<Integer,Integer> getMapYears(int range);
	
	/**
	 * 
	 * @param Anio
	 * @return unrango de años empezando del año que se le pase
	 */
	public Map<Integer,Integer> getMapYears(int anio, int rango);
	
	public Map<Integer, String> getMapFormasdePagoByNegTipoPoliza(BigDecimal idToNegTipoPoliza);
	public Map<String, String> getMapFormasdePagoByNegTipoPolizaString(BigDecimal idToNegTipoPoliza);
	
	public Integer getPrimeraFormasdePagoByNegTipoPoliza(Long idToNegProducto);
	
	public Map<Long, Double> getMapDerechosPolizaByNegocio(BigDecimal idNegocio);
	
	public Map<Long, Double> getMapDerechosPolizaByNegocio(BigDecimal idToCotizacion, BigDecimal numeroInciso);
	
	public Map<Long, Double> getMapDerechosPolizaByConfiguracion(Long idNegocio, BigDecimal idTipoPoliza, Long idSeccion, 
			Long idPaquete, BigDecimal idMoneda,  String estado, String municipio, Long idTipoUso);
	
	public boolean mostrarPagoFraccionado(Long idNegocio);
	
	public Map<Short, Short> getMapModeloVehiculoPorMonedaMarcaNegocioSeccion(BigDecimal idMoneda, BigDecimal idMarca, BigDecimal idToNegSeccion);
	
	public Map<String, String> getMapEstiloVehiculoPorMarcaVehiculoModeloNegocioSeccionMoneda(
			BigDecimal idTcMarcaVehiculo, Short modeloVehiculo, BigDecimal idToNegSeccion, BigDecimal idMoneda, 
			Long autoIncisoContinuityId);
	
	public Map<String, String> getMapEstiloVehiculoPorMarcaVehiculoModeloNegocioSeccionMonedaAgente(
			BigDecimal idTcMarcaVehiculo, Short modeloVehiculo, BigDecimal idToNegSeccion, BigDecimal idMoneda, 
			Long autoIncisoContinuityId, String descripcion, BigDecimal idAgrupadorPasajeros);
		
	public List<EstiloVehiculoDTO> getListarEstilo(
			BigDecimal idTcMarcaVehiculo, Short modeloVehiculo, BigDecimal idToNegSeccion, BigDecimal idMoneda, 
			Long autoIncisoContinuityId, String descripcion);
	
	public List<EstiloVehiculoDTO> listarEstilosPorDescripcion(String tipoBien, String descripcion, BigDecimal idToNegSeccion, BigDecimal idMarca,
			Short modeloVehiculo, BigDecimal idMoneda);
	
	public Long getDerechoPolizaDefault(BigDecimal idNegocio);
	
	public Long getDerechoPolizaDefault(BigDecimal idToCotizacion, BigDecimal numeroInciso);
	
	public Long getDerechoPolizaDefaultByConfiguracion(Long idNegocio, BigDecimal idTipoPoliza, Long idSeccion, 
			Long idPaquete, BigDecimal idMoneda,  String estado, String municipio, Long idTipoUso);
	
	
	/**
	 * Obtener catalogo valor fijo para el grupo <code>TIPO_CATALOGO</code> correspondiente
	 * @param tipo Codigo del grupo que se desea obtener
	 * @param orderByDescription <code>true/false</code> para indicar si se ordena por las descripciones del catalogo, o por el ordenador numerico de la entidad
	 * @return Lista ordenada para el grupo correspondiente
	 */
	public Map<String, String> obtenerCatalogoValorFijo(TIPO_CATALOGO tipo, boolean orderByDescription);
	
	/**
	 * Obtener catalogo valor fijo para el grupo <code>TIPO_CATALOGO</code> correspondiente
	 * @param tipo Codigo del grupo que se desea obtener
	 * @return Lista ordenada por el ordenador numerico para el grupo correspondiente
	 */
	public Map<String, String> obtenerCatalogoValorFijo(TIPO_CATALOGO tipo);
	
	/**
	 * Obtener sub conjunto del catalogo valor fijo para el grupo <code>TIPO_CATALOGO</code> correspondiente ligados al padre <code>codigoPadreId</code> 
	 * @param tipo
	 * @param codigoPadreId
	 * @return
	 */
	public Map<String, String> obtenerCatalogoValorFijo(TIPO_CATALOGO tipo, String codigoPadreId);
	
	/**
	 * Obtener sub conjunto del catalogo valor fijo para el grupo <code>TIPO_CATALOGO</code> correspondiente ligados al padre <code>codigoPadreId</code> 
	 * @param tipo Codigo del grupo que se desea obtener
	 * @param codigoPadreId identificador del padre para este valor en el catalogo utilizado generalmente para cascadeo simple
	 * @param orderByDescription <code>true/false</code> para indicar si se ordena por las descripciones del catalogo, o por el ordenador numerico de la entidad
	 * @return Lista ordenada por el ordenador numerico para el grupo correspondiente
	 */
	public Map<String, String> obtenerCatalogoValorFijo(TIPO_CATALOGO tipo, String codigoPadreId, boolean orderByDescription);
	
	/**
	 * Obtener entidad de CatValorFijo para el grupo dado <code>tipo</code> y para el <code>codigo</code> del elemento en cuestion
	 * @param tipo Codigo del grupo que se desea obtener
	 * @param codigo Codigo del elemento que se desea obtener
	 * @return Mapa conteniendo el elemento buscado
	 */
	public Map<String, String> obtenerCatalogoPorCodigo(TIPO_CATALOGO tipo, String codigo);
	
	public Map<Long, String> obtenerOficinasSiniestros();
	
	/**
	 * Busca todas las oficinas dadas de alta en el catalogo de oficina sin importar si estan activas o inactivas
	 * @return
	 */
	public Map<Long, String> obtenerOficinasSiniestrosSinFiltro();
	
	public Map<String, String> getMapTipoPrestador();

	public Map<String, String> getMapColoniasPorCiudadMidas(String idCiudad);
	
	public Map<String, String> getMapMunicipiosPorEstadoMidas(String stateId);
	
	public Map<String, String> getMapEstadosPorPaisMidas(String idPais);
	
	public String getCodigoPostalPorColoniaMidas(String idColonia);
	
	public Map<String, String> getMapPaisesMidas();
	
	public Map<Long, String> getMapCiaDeSeguros();
	
	public List<ColoniaMidas> getColoniasPorCPMidas(String cp);
	
	public Map<String, String> getMapColoniasPorCPMidas(String cp);

	public Map<String, String> getMapCiudadesPorCPMidas(String cp);

	public String getEstadoPorIdColoniaMidas(String idColonia);
	
	public String getEstadoPorCPMidas(String cp);

	public String getCiudadPorCPMidas(String cp);

	public boolean getAplicaDescuentoNegocioPaqueteSeccion(Long idToNegPaqueteSeccion);
	
	public Double getPcteDescuentoDefaultPorEstado(BigDecimal idToCotizacion, String stateId);
	
	public Double getPcteDescuentoMaximoPorEstado(BigDecimal idToCotizacion, Long idToNegocio, String stateId);
	
	public Map<String, String> getMapMovimientosNotificaciones(String idProceso);

	public Double getPcteDescuentoDefaultPorNegocio(Long idToNegocio, String stateId);
	
	public Map<String, String> obtenerCatalogoValorFijoByStr(String tipo, String codigoPadreId);
	
	public Map<String, String> obtenerTerminosAjuste(String codigoTipoSiniestro, String codigoResponsabilidad);
	
	public Map<String, String> obtenerAjustadoresValuacionCrucero();
	
	public Map<String, String> obtenerPiezasPorSeccionAutomovil(String codigoSeccion);
	
	public Map<String, String> obtenerValuadores();
	
	public Map<String, String> obtenerCoberturaPorSeccion( Long idToSeccion );	
	
	public Map<Long, String> getMapAjustadoresPorOficina(Oficina oficina, Boolean soloActivos);

	public Map<String, String> getMapAjustadoresPorOficinaId(Long oficinaId);
	
	public Map<Long, String> getMapHorariosLaborales(Boolean soloActivos);

	public Map<String, String> obtenerTiposPaseAtencion();
	
	public Map<Long, String> listarConceptosPorCobertura(Long idCoberturaCabina,String cveSubTipoCalculoCobertura ,short categoria, String tipoConcepto);
	public Map<Long, String> listarConceptosPorCobertura(Long idCoberturaCabina,String cveSubTipoCalculoCobertura ,short categoria, String tipoConcepto, boolean esValidoHGS);

	
	public Map<String, String> getCoberturasOrdenCompra(Long idReporteCabina, String tipo);
	
	public Map<String, String> getCoberturasReporte(Long reporteCabinaId, String tipoSiniestro, 
			String terminoAjuste,String tipoResponsabilidad, Boolean soloAfectadas, Boolean soloAfectables);

	public Map<Long, String> getMapPrestadorPorTipo(String tipo);
	
	public Map<Long, String> getListasTercerosAfectadorPorCobertura (Long coberturaReporteCabinaId, String cveSubTipoCalculo);
	
	public Map<String, String> obtenerCoberturaPorSeccionConDescripcion( Long idToSeccion );
	/**
	 * Devuelve un map, con las Secciones Vigentes
	 * @return
	 */
	public Map<Long,String> obtenerMapSecciones();
	
	/**
	 * Devuelve un map, com los Conceptos de Afectacion Cobertura, proporcionandole los parametros necesarios.
	 * @param idCoberturaSeccion
	 * @param idSeccion
	 * @param cveSubTipoCalculoCobertura
	 * @param tipoConcepto
	 * @return
	 */
	public Map<Long, String> listarConceptosPorCoberturaSeccion(Long idCoberturaSeccion, Long idSeccion,String cveSubTipoCalculoCobertura, short tipoConcepto);
	
	/**
	 * Devuelve un map, con los Conceptos de Gasto Ajuste
	 * @return
	 */
	public Map<Long, String> obtenerMapConceptosGastoAjuste();
	
	public Map<Long, String> obtenerMapConceptosReembolsoGastoAjuste();
	/**
	 * Devuelve un map, con los Conceptos de Gasto Ajuste
	 * @return
	 */
	public Map<Long, String> getMapConceptosAjuste();
	
	public Map<Long, String> listarConceptosPorCoberturaOrdenCompra(
			Long idCoberturaCabina, String cveSubTipoCalculoCobertura,
			short categoria, String tipoConcepto, boolean aplicaPagoDaños, Long idTipoPrestador);
	
	@Deprecated
	public Map<String, String> obtenerTipoDePasePorTipoEstimacion(String codigoTipoEstimacion,
			String codigoTipoResponsabilidad);
	
	public Map<String, String> obtenerTipoDePasePorTipoEstimacion(String codigoTipoEstimacion,
			String codigoTipoResponsabilidad, String codigoCausaSiniestro, String codigoTerminoAjuste);
	
	public Map<BigDecimal, String> getMapVigencias();
	
	public Map<BigDecimal, String> getMapProgramaPagos(BigDecimal idToPoliza, BigDecimal numeroEndoso);
	
	public String getIncisosProgPago(BigDecimal idToPoliza, BigDecimal idProgPago);
	
	
	
	public Map<Long, String> getMapOficinaJuridico();
	/**
	 * Obtiene el listado de delegaciones en juridico activas, odernadas por numero. 
	 * Utilizar el metodo de catalogoJuridicoService.obtener filtrado de acuerdo a lo que se desea retornar.
	 */
	public Map<Long, String> obtenerJuridicoDelegaciones ();
	/**Obtener el listado de motivos juridico activos ordenado por numero. 
	Utilizar el metodo de catalogoJuridicoService.obtener filtrado de acuerdo a lo que se desea retornar.
	*/ 
	public Map<Long, String> obtenerJuridicoMotivos ();
	/**
	 * Devuelve el listado de procedimientos juridicos activos ordenados por número. 
	 * Utilizar el metodo de catalogoJuridicoService.obtener filtrado de acuerdo a lo que se desea retornar.
	 * */
	public Map<Long, String> obtenerJuridicoProcedimientos ();
	
	public Map<String, String> obtenerUsuariosJuridico();
	
	public Map<Long, String> obtenerTipoPrestadorGastoAjuste() ;
	
	public Map<String, String> getMapTipoPrestadorAfectacionReserva();
	
	/**
	 * @param idGerencia
	 * @param idProvision
	 * @return
	 */
	public Map<Long,String> getMapEjecutivosPorGerencia(Long idGerencia, Long idProvision);
	
	/**
	 * @param idEjecutivo
	 * @param idProvision
	 * @return
	 */
	public Map<Long,String> getMapPromotoriasPorEjecutivo(Long idEjecutivo, Long idProvision);
	/**
	 * @param idLineaVenta
	 * @param idProvision
	 * @return
	 */
	public Map<Long,String> getMapProductoPorLineaVenta(Long idLineaVenta, Long idProvision);
	/**
	 * @param idProducto
	 * @param idProvision
	 * @return
	 */
	public Map<Long,String> getMapLineaNegocioPorProducto(Long idProducto, Long idProvision);
	
	/**
	 * Obtener bancos de las tablas de midas (migracion de seycos)
	 * @return
	 */
	public Map<Long, String> getMapBancosMidas();
	
	/**
	 * Obtener cuentas por banco midas (migracion de seycos)
	 * @param bancoId
	 * @return
	 */
	public Map<Long, String> getMapCuentasBancoMidas(Long bancoId);
	
	
	/**
	 * Obtener listado de cuentas acreedoras para siniestros ingresos
	 * @return mapa con cuentas acreedoras definidas en catálogo
	 */
	public Map<Long, String> getMapCuentasAcreedorasSiniestros();
	
	/**
	 * Obtener listado de cuentas manuales para siniestros ingresos
	 * @return mapa con cuentas manuales definidas en catalogo
	 */
	public Map<Long, String> getMapCuentasManualesSiniestros();
	
	public Map<Long, String> obtenerAbogados();
	
	/***
	 * Filtra los bancos validos para la recuperacion de salvamento
	 * @return
	 */
	public Map<Long,String> getCtgBancosValidosSalvamentos();
	
	public Map<String, String> getMediosPago(Negocio negocio);
	
	public Map<String, String> getMediosPagoSO(Negocio negocio);
	
	public Map<String, String> getBancos();
	
	public List<BancoEmisorDTO> getBancosList();
	/**
	 * Obtiene los datos para la carátula de los incisos de SPV
	 * @param idToNegocio
	 * @return
	 */
	public Map<String, Object> getSpvData(Long idToNegocio);
	
	public BigDecimal getIdToFormaDePagoByNegocioValid(BigDecimal idToNegTipoPoliza, BigDecimal idToNegTipoPolizaCom);
	
	/**
	 * Obtiene los datos de seycos
	 */

	//public List<GenericaAgentesView> listarProductoSeycos(String listaId);
	
	public List<GenericaAgentesView> listaRamoSeycos(String arg0, String arg1);

	public List<GenericaAgentesView> listaSubramoSeycos(String arg0, String arg1);
	
	public List<GenericaAgentesView> listaProductoSeycos(String arg0, String arg1);
	
	public List<GenericaAgentesView> listaLineaNegocioSeycos(String arg0, String arg1, String arg2);
	
	public List<GenericaAgentesView> listaGerenciaSeycos();
	
	public List<DatosSeycos> listarAgenteSeycos(String agente, String idGerencias);	
	
	/**
	 * Obtiene moneda CA
	 */
	public Map<Long, String> getMapCaTipoMoneda();
	
	public  String getDescripionValorFijo(Integer claveTipoDeducible, int key);
	public Map<BigDecimal, String> getMapTipoServicioVehiculo(BigDecimal idToNegSeccion);
	public  Map<String, String> listarNegocioSeccionPorProductoNegocioTipoPoliza(
			BigDecimal idToProducto, Long idToNegocio, BigDecimal idToTipoPoliza);
	
	public Map<Long,Double> listaImportesDeDerechosPorNegocio(Long idToNegocio, String tipoDerecho);
	
	
	public boolean imprimirLeyendaUMA(Date fechaInicioVigencia);
	
	/**
	 * Obtener el listado de versiones existentes para la recuotificacion de un negocio
	 * @param idToNegocio
	 * @return
	 */
	public Map<Long, Integer> obtenerListadoVersionesRecuotificacionNegocio(Long idToNegocio);
	
	public String ObtieneMerchantPortalPagosWeb ();
	
	public String ObtieneUrlBackPortalPagosWeb ();
	
	CiudadDTO getMunicipioPorCp(String cp);
	
	/**
	 * Obtener el listado de Tipos de vigencia asignados a un negocio
	 * @param idToNegocio
	 * @return
	 */
	public Map<Integer, String> obtenerTiposVigenciaNegocio(Long idToNegocio);
	
	/**
	 * Obtener vigencia default
	 * @param idToNegocio
	 * @return
	 */
	Integer obtenerVigenciaDefault(Long idToNegocio);
}
