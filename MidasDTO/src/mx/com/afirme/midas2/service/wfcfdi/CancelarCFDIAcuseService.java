package mx.com.afirme.midas2.service.wfcfdi;

import javax.ejb.Local;

@Local
public interface CancelarCFDIAcuseService {
	
	public void cancelarCFDI();
	
}