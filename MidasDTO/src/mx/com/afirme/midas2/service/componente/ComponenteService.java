package mx.com.afirme.midas2.service.componente;

import java.util.LinkedHashMap;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas2.dto.RegistroDinamicoDTO;


public interface ComponenteService {

	/**
	 * Genera un listado de registros dinamicos a partir de un Id, representado por el campo claveVista.
	 * @param clave Para el modulo de tarifas se recibe como 	T_idLineaNegocio_idMoneda_idRiesgo_idConcepto_version_[camposRestantesDelID]
	 * @param claveVista Clave que corresponde a una vista que se quiere recuperar
	 * @return
	 */
	List<RegistroDinamicoDTO> getListado(String clave, String claveVista);
	
	/**
	 * Consulta un listado de registros dinamicos a partir de un  Id, representado por el campo claveVista y un objeto específico usado como filtro.
	 * @param clave Para el catálogo específico
	 * @param claveVista Clave que corresponde a una vista que se quiere recuperar
	 * @param Object registro que sirve como filtro.
	 * @return
	 */
	List<RegistroDinamicoDTO> getListado(String clave, String claveVista,Object filtro);
	
	/**
	 * En base a la accion recibida, genera un nuevo registro dinamico del tipo especifico
	 * establecido en el parametro "clave"  y lo pobla con los datos de su objeto representado.
	 * @param clave Para el modulo de tarifas se recibe como 	T_idLineaNegocio_idMoneda_idRiesgo_idConcepto_version_[camposRestantesDelID]
	 * @param claveVista Clave que corresponde a una vista que se quiere recuperar
	 * @param accion Especifica la operacion a realizar:
	 * 				"agregar"
	 * 				"editar"
	 * 				"borrar"
	 * @param objeto Objeto cuyos valores se poblaran en el registro dinamico
	 * @return
	 */
	RegistroDinamicoDTO getRegistroParaValidacion(String clave, String claveVista, String accion, Object objeto);
	
	
	
	/**
	 * En base a la accion recibida, genera un nuevo registro dinamico del tipo especifico
	 * establecido en el parametro "clave".
	 * @param clave Para el modulo de tarifas se recibe como 	T_idLineaNegocio_idMoneda_idRiesgo_idConcepto_version_[camposRestantesDelID]
	 * @param claveVista Clave que corresponde a una vista que se quiere recuperar
	 * @param accion Especifica la operacion a realizar:
	 * 				"agregar"
	 * 				"editar"
	 * 				"borrar"
	 * @return
	 */
	RegistroDinamicoDTO getRegistro(String clave, String claveVista, String accion);
	
	/**
	 * 
	 * @param registro
	 * @param id
	 * @param accion
	 * @return
	 */
	void actualizarRegistro(Object registro, String accion);
	
	/**
	 * Consulta el registro en BD de acuerdo a la clave recibida.
	 * Deduce si se intenta agregar un nuevo registro en vase a la consitución de la clave y regresa
	 * ya sea un nuevo objeto o un objeto existente en BD del tipo específico declarado como Object.
	 * @param clave. Clave del catálogo que se está procesando
	 * @return Object objeto del tipo específico de acuerdo a la clave recibida.
	 */
	public Object getRegistroEspecifico(String clave);
	
	/**
	 * Consulta el listado de registros dependientes de un registro cuyo id se recibe en la clave.
	 * @param clave. La clave contiene el tipo de catálogos a consultar y el identificador del registro padre.
	 * @return
	 */
	public LinkedHashMap<String, String> getMapaRegistros(String clave);
	
	/**
	 * Consulta el listado de registros dependientes de un registro cuyo id se recibe en la clave.
	 * @param clave. La clave contiene el tipo de catálogos a consultar y el identificador del registro padre.
	 * @return
	 */
	public List<CacheableDTO> getListadoRegistros(String clave);
}
