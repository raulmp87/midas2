package mx.com.afirme.midas2.service.componente.incisos;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Local;

import org.joda.time.DateTime;

import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.dto.impresiones.DatosLineasDeNegocioDTO;
import mx.com.afirme.midas2.dto.impresiones.ReferenciaBancariaDTO;

@Local
public interface ListadoIncisosDinamicoService {
	
	public List<BitemporalInciso> buscarIncisosFiltrado(IncisoCotizacionDTO filtros, short claveTipoEndoso, Long idToPoliza, Date validoEn, int tipoVista);
	
	public Long obtenerTotalPaginacionIncisos(IncisoCotizacionDTO filtros, short claveTipoEndoso, Long idToPoliza, Date validoEn);
	
	public List<BitemporalInciso> buscarIncisosFiltrado(IncisoCotizacionDTO filtros, short claveTipoEndoso, Long idToPoliza, Date validoEn, boolean buscaValido, int tipoVista);
	
	public Long obtenerTotalPaginacionIncisos(IncisoCotizacionDTO filtros, short claveTipoEndoso, Long idToPoliza, Date validoEn, boolean buscaValido, int tipoVista);
	
	public List<BitemporalInciso> buscarIncisosFiltrado(IncisoCotizacionDTO filtros, short claveTipoEndoso,	Long idToPoliza, Date validoEn, Date recordFrom, boolean buscaValido, int tipoVista);
	
	public List<BitemporalInciso> buscarIncisosFiltrado(IncisoCotizacionDTO filtros, short claveTipoEndoso, Long idToPoliza, Date validoEn, Date recordFrom, boolean buscaValido, boolean esConsultaPoliza, int tipoVista);
	
	public Set<String> listarIncisosDescripcion(short claveTipoEndoso, Long cotizacionContinuityId, Date validoEn);
	
	public List<NegocioSeccion> getSeccionList(short claveTipoEndoso, Long cotizacionContinuityId, Date validoEn);
	
	public Map<Long, String> getMapNegocioPaqueteSeccionPorLineaNegocio(short claveTipoEndoso, Long cotizacionContinuityId, Date validoEn, BigDecimal idToNegSeccion);
	
	public void fillAutoInciso(BitemporalInciso bitemporalInciso, boolean getCancelados);
	
	public void fillAutoInciso(BitemporalInciso bitemporalInciso, boolean getCancelados, DateTime validoEn, DateTime recordFrom);
	
	public List<BitemporalInciso> seleccionarIncisos(List<BitemporalInciso> incisos, String elementosSeleccionados);
	
	public Long getNumeroIncisos(Long cotizacionContinuityId, DateTime validFromDT, DateTime recordFromDT, boolean getCancelados);
	
	/**
	 * Obtiene un resumen de las lineas de negocio con datos como Numero de
	 * incisos y prima neta de una poliza a cierta fecha. Se crea para optimizar
	 * el tiempo de respuesta al momento de imprimir una poliza con muchos
	 * incisos, antes se calculaba todo en java.
	 * 
	 * @param idToPoliza
	 * @param numeroEndoso
	 * @return
	 */
	public List<DatosLineasDeNegocioDTO> getDatosLineaNegocio(BigDecimal idToPoliza, Short numeroEndoso);
	/**
	 * Obtiene las referencias bancarias
	 * @param idToPoliza
	 * @param numeroEndoso
	 * @return
	 */
	public List<ReferenciaBancariaDTO> getReferenciasBancarias(BigDecimal idToPoliza);
}
