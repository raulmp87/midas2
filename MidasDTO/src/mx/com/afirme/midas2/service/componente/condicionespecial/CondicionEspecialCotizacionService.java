package mx.com.afirme.midas2.service.componente.condicionespecial;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial.NivelAplicacion;

@Local
public interface CondicionEspecialCotizacionService {

	/**
	 * Si no le paso el numero de inciso, me guarda la cotizacion a nivel poliza , si se lo proporciono, me lo guarda a nivel inciso
	 */
	public void agregarCondicionEspecial(BigDecimal idToCotizacion, BigDecimal numeroInciso, Long idCondicionEspecial);
	
	public void generaCondicionesBaseCompletas(BigDecimal idToCotizacion);
	
	public void generarCondicionesBaseCotizacion(BigDecimal idToCotizacion);
	
	public void generarCondicionesBaseInciso(BigDecimal idToCotizacion, BigDecimal numeroInciso);
	
	public List<CondicionEspecial> obtenerCondicionEspecial(BigDecimal idToNegocio, BigDecimal idToCotizacion, BigDecimal numeroInciso, 
			CondicionEspecial.NivelAplicacion nivelAplicacion, Long idCondicionEspecial, FiltroCondicionEspecialCotizacion.Asociacion asociacion);
	
	public void removerCondicionEspecial(BigDecimal idToCotizacion, BigDecimal numeroInciso, Long idCondicion);
	
	public List<CondicionEspecial> obtenerCondicionEspecial(FiltroCondicionEspecialCotizacion filtro);
	
	public List<CondicionEspecial> obtenerCondicionEspecialCotizacion(FiltroCondicionEspecialCotizacion filtro);
	
	public List<CondicionEspecial> obtenerCondicionEspecialInciso(FiltroCondicionEspecialCotizacion filtro);
	
	public List<CondicionEspecial> buscarCondicionesEspeciales(BigDecimal idToNegocio, BigDecimal idToCotizacion,
			BigDecimal numeroInciso, NivelAplicacion nivelAplicacion, Long idCondicionEspecial, String token);
	
	public void relacionarTodas(BigDecimal idToNegocio, BigDecimal idToCotizacion, BigDecimal numeroInciso, NivelAplicacion nivelAplicacion, String codigoNombre);
	
	public void quitarTodas(BigDecimal idToCotizacion, BigDecimal numeroInciso, NivelAplicacion nivelAplicacion);
	
	public Long getIdNegocio(BigDecimal idToCotizacion);
	
	public class FiltroCondicionEspecialCotizacion{
		private Long idCondicionEspecial;
		private BigDecimal idToCotizacion;
		private BigDecimal numeroInciso;
		private CondicionEspecial.NivelAplicacion nivelAplicacion;
		private Asociacion asociacion;
		public enum Asociacion{
			ASOCIADA((short)0),  
			DISPONIBLE((short)1),  
			TODAS((short)2);		
			
			Asociacion(Short asociacion) {
				this.asociacion = asociacion;
			}
			private Short asociacion;
			
			public Short getAsociacion() {
				return asociacion;
			}
			public void setAsociacion(Short asociacion) {
				this.asociacion = asociacion;
			}		
		};
		
		public FiltroCondicionEspecialCotizacion(BigDecimal idToCotizacion, Long idCondicionEspecial, BigDecimal numeroInciso, CondicionEspecial.NivelAplicacion nivelAplicacion, Asociacion asociacion){
			this.idToCotizacion = idToCotizacion;
			this.idCondicionEspecial = idCondicionEspecial;
			this.numeroInciso = numeroInciso;
			this.nivelAplicacion = nivelAplicacion == null ? CondicionEspecial.NivelAplicacion.TODAS : nivelAplicacion;
			this.asociacion = asociacion;
		}

		public FiltroCondicionEspecialCotizacion(BigDecimal idToCotizacion, BigDecimal numeroInciso){
			this.idToCotizacion = idToCotizacion;
			this.numeroInciso = numeroInciso;
		}
		
		public Long getIdCondicionEspecial() {
			return idCondicionEspecial;
		}
		public void setIdCondicionEspecial(Long idCondicionEspecial) {
			this.idCondicionEspecial = idCondicionEspecial;
		}
		public BigDecimal getIdToCotizacion() {
			return idToCotizacion;
		}
		public void setIdToCotizacion(BigDecimal idToCotizacion) {
			this.idToCotizacion = idToCotizacion;
		}
		public BigDecimal getNumeroInciso() {
			return numeroInciso;
		}
		public void setNumeroInciso(BigDecimal numeroInciso) {
			this.numeroInciso = numeroInciso;
		}
		public CondicionEspecial.NivelAplicacion getNivelAplicacion() {
			return nivelAplicacion;
		}
		public void setNivelAplicacion(CondicionEspecial.NivelAplicacion nivelAplicacion) {
			this.nivelAplicacion = nivelAplicacion;
		}
		public Asociacion getAsociacion() {
			return asociacion;
		}
		public void setAsociacion(Asociacion asociacion) {
			this.asociacion = asociacion;
		}	
		
	}

}
