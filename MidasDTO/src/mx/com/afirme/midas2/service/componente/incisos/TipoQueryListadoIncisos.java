package mx.com.afirme.midas2.service.componente.incisos;

public enum TipoQueryListadoIncisos {
	INCISOS_PERDIDA_TOTAL,
	INCISOS_RECIBOS_AGRUPADOS
}
