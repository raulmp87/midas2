package mx.com.afirme.midas2.service.sistema;

import java.net.URL;

import javax.xml.namespace.QName;

/**
 * Se utiliza para obtener valores generales del sistema.
 * 
 * @author amosomar
 */

public interface SistemaContext {
	
	public boolean getSeguridadActivada();
	public String getSmtpHostName();
	public boolean getAsmActivo();
	public String getAsmWscContext();
	public String getAsmBaseUrl();
	public String getAsmLogin();
	public String getUsuarioAccesoMidas();
	public String getMidasAppId();
	public String getMidasLogin();
	public String getAccionPaginado();
	public String getListaPaginada();
	public String getAccesoDenegado();
	public String getUrlInicio();
	public String getUrlError();
	public String getUsuarioSistema();
	public String getUrlLoginSinSeguridad();
	public String getUrlCambioPassword();	
	public String getArchivoMenuMidas();
	public String getArchivoRecursos();
	public String getArchivoRecursosFront();
	public String getArchivoRecursosBack();
	public String getUploadFolder();
	//Roles de Daños 
	public String getRolAdminProductos();
	public String getRolAgente();
	public String getRolAgenteExterno();
	public String getRolAgenteEspecial();
	public String getRolAsignadorOt();
	public String getRolAsignadorSol();
	public String getRolCoordinadorEmi();
	public String getRolDirectorTecnico();
	public String getRolEmisor();
	public String getRolMesaControl();
	public String getRolSupervisorSuscriptor();
	public String getRolSuscriptorOt();
	public String getRolSuscriptorCot();
	public String getRolReporteDanios();
	public String getRolConsultaDanios();
	public String getRolSuscriptorExt();
	public String getIdRolAgente();
	public String getIdRolPromotor();
	
	//Roles Siniestros
	public String getRolCabinero();
	public String getRolCoordinadorSiniestros();
	public String getRolCoordinadorSiniestrosFacultativo();
	public String getRolGerenteSiniestros();
	public String getRolGerenteSiniestrosFacultativo();
	public String getRolAnalistaAdministrativo();
	public String getRolAnalistaAdministrativoFacultativo();
	public String getRolDirectorDeOperaciones();
	public String getRolAjustador();
	public String getRolReportesSiniestros();
	public String getRolConsultaSiniestros();
	
	//Roles Reaseguro
	public String getRolOpReaseguroAutomatico();
	public String getRolOpAsistenteSubdirectorReaseguro();
	public String getRolSubdirectorReaseguro();
	public String getRolDirectorReaseguro();
	public String getRolGerenteReaseguro();
	public String getRolOpReaseguroFacultativo();
	public String getRolActuario();
	public String getRolOpPagosCobrosReaseguradores();
	public String getRolCoordinadorAdministrativoReaseguro();
	public String getRolReportesReaseguro();
	public String getRolConsultaReaseguro();
	public String getRolMailReaseguro();
	
	//Roles especiales
	public String getRolEspAdministradorColonias();
	
	//Roles de Autos
	public String getRolAdministradorAutos();
	public String getRolGerenteTecnicoAutos();
	public String getRolCoordinadorAutos();
	public String getRolSuscriptorAutos();
	public String getRolMesaControlAutos();
	public String getRolPromotorAutos();
	public String getRolAgenteAutos();
	public String getRolUsuarioWebAutos();
	public String getRolSucursalEjecutivo();
	
	public String getExitoso();
	public String getNoExitoso();
	public String getNoDisponible();
	public String getAlterno();
	
	public String getAlternoPortal();
	public String getUsuarioAccesoPortal();
	
	//Roles Jurídico
	public String getRolDirectorJuridico();
	public String getRolAnalistaJuridico();
	public String getRolDirectorTecnicoAutos();
	public String getRolGerenteContabilidad();
	public String getRolSubdirectorSiniestrosAuto();
	
	public String getRolGerenteOperacionIndemnizacionesAutos(); 
	public String getRolCoordinadorOperaciones();
	
	
	//Roles de Agentes
	public String getRolAdministradorAgentes();
	public String getRolCoordinadorAgentes();
	
	public String getNotificationChannel();
	
	public String getExpresionCronCancelacionEndososAuto();
	
	public String getEnvioProovedorFTP();
	
	public int getEnvioProovedorPort();
	
	public String getEnvioProovedorUser();
	
	public String getEnvioProovedorPassword();
	
	public String getEnvioProovedorDirectory();
	
	public String getExpedienteFortimaxEndPointPath();
	
	public String getExpedienteFortimaxEndPointPathWSDL();
	
	public String uploadFileFortimaxEndPointPathWSDL();
	
	public String getDocumentoFortimaxEndPointPath();
	
	public String getDocumentoFortimaxEndPointPathWSDL();	
	
	public String getLinkFortimaxEndPointPathWSDL();
	
	public String getTituloApFortimax();
	
	public String getCarpetaRaizFortimax();
	
	public String getCarpetaRaizClientes();
	
	public String getEibsHost();
	public String getEibsPort();
	public String getEibsTimeout();
	
	//Web Services Client
	public String getPrintReportEndpoint();
	public String getRootDirFiles();
	public String getEibsServiceURL();
	//Web Services Client
	
	public String getDirectorioImagenes();
	
	/**
	 * Se obtiene el valor del directorio de imagenes desde la BD.
	 * @return
	 */
	public String getDirectorioImagenesParametroGlobal();
	public String getAmbienteSistema();
	public URL getMultiasistenciaSendAlertPortWsdlUrl();
	public URL getAxosnetValidadorSATWsdlUrl();
	public QName getAxosnetValidadorSATQName();
	public String getAjustadorMovilSenderKey();
	public String getEnlaceMovilSenderKey();
	public String getFortimaxV2BaseEndpointUrl();
	public String getFortimaxV2User();
	public String getFortimaxV2Password();
	
	public String getCesviIndividualEndpoint();
	public URL getCesviFlotillasEndpoint();
	public URL getCesviClavesEndpoint();
	
	public String getTrakingUser();
	public String getTrakingPassword();
	public String getTrakingIdSuscripcion();
	public String getTrackingEndPoint();
	
	public String getPasswordCertificadoAPNs();
	public String getNombreCertificadoAPNs();
	public String getApnsEnvironment();
	public String getLongitudRegistrationIdIOS();
	
	public String getPasswordCertificadoClienteSuite();
	public String getNombreCertificadoClienteSuite();
	public String getClienteSuiteEnvironment();
	public String getRutaCertificadoClienteMovil();
			
	public String getClienteMovilSenderKey();
	String getKBBEndPoint();
	String getKBBUser();
	String getKBBActiva();
	String getMarcaFronterizo();
	String getEstiloFronterizo();
	public String getCondicionFronterizo();
	public String getAjustadorMovilDeviceApplicationId();
	
	public boolean getTimerActivo();
	
	
	public String getEnvioProvAsisVidaFTP();
	public int getEnvioProvAsisVidaPort();
	public String getEnvioProvAsisVidaUser();
	public String getEnvioProvAsisVidaPassword();
	public String getEnvioProvAsisVidaDirectory();
	
}
