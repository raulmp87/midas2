package mx.com.afirme.midas2.service.sistema.comm;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.sistema.comm.CommBitacora;
import mx.com.afirme.midas2.domain.sistema.comm.CommBitacoraDetalle;
import mx.com.afirme.midas2.domain.sistema.comm.CommBitacoraDetalle.STATUS_COMM;
import mx.com.afirme.midas2.domain.sistema.comm.CommProceso;

@Local
public interface CommBitacoraService {
	
	public CommProceso obtenerProceso(CommProceso.TIPO_PROCESO proceso);
	
	public void registrarBitacora(CommBitacora bitacora);
	
	public void registarDetalle(CommBitacoraDetalle detalle);
	
	public List<CommBitacora> obtenerBitacora(CommBitacora filtro);
	
	public List<CommBitacoraDetalle> obtenerBitacoraDetalle(CommBitacoraDetalle filtro);
	
	public CommBitacora obtenerBitacora(String folio);
	
	public CommBitacora obtenerBitacora(String folio,CommProceso.TIPO_PROCESO proceso, CommBitacora.STATUS_BITACORA status);
	
	public CommBitacoraDetalle obtenerUltimoDetalle(String folio);
	
	public CommBitacoraDetalle obtenerUltimoDetalle(String folio, STATUS_COMM status);
	
	public List<CommBitacoraDetalle> obtenerDetallesConStatusErrorComm(String folio);

}
