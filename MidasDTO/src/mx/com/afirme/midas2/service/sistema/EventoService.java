package mx.com.afirme.midas2.service.sistema;

import java.util.List;

import mx.com.afirme.midas2.domain.sistema.Evento;
import mx.com.afirme.midas2.domain.sistema.EventoMidas;

public interface EventoService {

	public List<Evento> obtenerEventos();
	
	public EventoMidas guardarEvento(String nombre, String mensaje);
	
	public void borrarEventoProcesado(Evento evento);
	
}
