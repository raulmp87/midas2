package mx.com.afirme.midas2.service.sistema.parametro;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.sistema.parametro.ParametroGlobal;

@Local
public interface ParametroGlobalService {
	public static final int AP_MIDAS = 5;
	public static final String PARAMETRO_SEGUIMIENTOROBO_CONTACTO="ROBO_CONTACTO_NOMBRE";
	public static final String PARAMETRO_SEGUIMIENTOROBO_TEL="ROBO_CONTACTO_TEL";
	public static final String PARAMETRO_SEGUIMIENTOROBO_LADA="ROBO_CONTACTO_LADA";
	public static final String PARAMETRO_SEGUIMIENTOROBO_MAIL="ROBO_CONTACTO_MAIL";
	public static final String PARAMETRO_SEGUIMIENTOROBO_EXT="ROBO_CONTACTO_EXT";
	public static final String ENTORNO_APLICACION = "ENTORNO_APLICACION"; //  # INDICA SI LA APLICACION ES DESARROLLO O PRODUCCION
	public static final String WS_HGS_REPORTE     = "WS_HGS_REPORTE";     //  # WS A USAR PARA COLOCAR REPORTE EN HGS
	public static final String WS_OCRA            = "WS_OCRA";     //  # WS A USAR PARA OCRA
	public static final String WS_OCRA_INSERTA_REPORTE_PASS = "WS_OCRA_INSERTA_REPORTE_PASS";  //  # WS password ocra insertar reporte
	public static final String WS_OCRA_INSERTA_REPORTE_USER = "WS_OCRA_INSERTA_REPORTE_USER";  //  # WS usuario ocra insertar reporte
	public static final String MAX_REPORTES_AJUSTADOR="MAX_REPORTES_AJUSTADOR";
	// ID DEL TALLER AL CUAL SE ASIGNARÁN PAGO DAÑOS HGS
	public static final String TALLER_PAGO_DANIOS_HGS="TALLER_PAGO_DANIOS_HGS"; 
	// URL DE OCRA PARA INGRESAR AL ADMINISTRADOR
	public static final String WS_URL_OCRA_ADMIN = "WS_URL_OCRA_ADMIN"; 
	public static final String MAX_RUTAS_AJUSTADORES = "MAX_RUTAS_AJUSTADORES";
	// CORREO DEL USUARIO RESPONSABLE DE OCRA
	public static final String WS_CORREO_OCRA_RESPONSABLE = "WS_CORREO_OCRA_RESPONSABLE";
	
	public static final String FACTURA_VALIDAR_PDF = "FACTURA_VALIDAR_PDF";
	public static final String FACTURA_DIFF_IMPUESTO = "FACTURA_DIFF_IMPUESTO";
	public static final String FACTURA_DIFF_TOTAL = "FACTURA_DIFF_TOTAL";
	
	public static final String CONCEPTO_CUENTA_CONTABLE_PREFIJO = "CONCEPTO_CUENTA_CONTABLE_PREFJ";
	public static final String CONCEPTO_CUENTA_CONTABLE_AFECTACION = "CONCEPTO_CUENTA_CONTABLE_1";
	public static final String CONCEPTO_CUENTA_CONTABLE_GASTO = "CONCEPTO_CUENTA_CONTABLE_2";
	public static final String CONCEPTO_CUENTA_CONTABLE_REEMBOLOSO = "CONCEPTO_CUENTA_CONTABLE_3";
	
	public static final String PORCENTAJE_DEDUCIBLE_CRISTALES = "PORCENTAJE_DEDUCIBLE_CRISTALES";
	
	public static final String IVA_VENTA_SALVAMENTO = "IVA_VENTA_SALVAMENTO";
	
	public static final String URL_DIRECTORIO_IMAGENES_IP = "URL_DIRECTORIO_IMAGENES_IP";
	public static final String LIMITE_ENVIO_MANUAL_REPORTE_CALCULOBONO_MENSUAL = "LIMITE_ENVIO_MAN_RCB_MENSUAL";
	
	public static final String PRESTADOR_ROL_ID = "PRESTADOR_ROL_ID";
	
	public static final String SPV_CODIGO_OFICINA = "SPV_CODIGO_OFICINA";
	public static final String CODIGO_USUARIO  = "WS_SPV_USUARIO";
	public static final String CODIGO_PASSWORD = "WS_SPV_PASSWORD";
	
	public static final String NOTIFICACION_CIERRE_MES = "NOTIFICACIONCIERREMES";
	public static final String TIMERTASKNOTIFICACIONCIERREMES = "TIMERTASKNOTIFICACIONCIERREMES";
	public static final String MINTASKNOTIFICACIONCIERREMES="MINTASKNOTIFICACIONCIERREMES";
	public static final String VARIABLEBLOQUESNOTIFICACIONCM = "VARIABLEBLOQUESNOTIFICACIONCM";

	public static final String CORREONOTIRETIMPUESTO="CORREONOTIRETIMPUESTO";
	
	public static final String NOTIFICAR_REP_DETALLE_PRIMAS_GEN_MIDAS = "NOTIFICARREPDETPRIMASGENMIDAS";
	
	public static final String LIGA_AGENTE_URL = "LIGA_AGENTE_URL";
	public static final String LIGA_AGENTE_USUARIO = "LIGA_AGENTE_USUARIO";
	public static final String LIGA_AGENTE_PASSWORD = "LIGA_AGENTE_PASSWORD";
	
	public static final String AGTS_CALC_AUTOR_COMIS_ACTIVO = "AGTS_CALC_AUTOR_COMIS_ACTIVO";
	
	public List<ParametroGlobal> obtenerListadoPorAplicacion(int aplicacion);
	public ParametroGlobal obtenerPorIdParametro(int aplicacion, String id);
	public String obtenerValorPorIdParametro(int aplicacion, String id);
	public String obtenerValorPorIdParametro(int aplicacion, String id, String valorDefault);
	
	public void guardarParametroGlobal(ParametroGlobal parametroGlobal);

}
