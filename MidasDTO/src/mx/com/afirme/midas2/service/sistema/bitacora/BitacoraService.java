package mx.com.afirme.midas2.service.sistema.bitacora;

import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation.OperationType;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.EVENTO;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.TIPO_BITACORA;

public interface BitacoraService {
	
	public class BitacoraFiltro{
		
		private Long id;
		
		private String bitacora;
		
		private String evento;
		
		private String cveUsuario;
		
		private Date fechaCreacionInicial;
		
		private Date fechaCreacionFin;
		
		private String identificador;
		
		@FilterPersistenceAnnotation(persistenceName="tipoBitacora")
		public String getBitacora() {
			return bitacora;
		}

		public void setBitacora(TIPO_BITACORA bitacora) {
			this.bitacora = bitacora != null ? bitacora.toString() : null;
		}

		public String getEvento() {
			return evento;
		}

		public void setEvento(EVENTO evento) {
			this.evento = evento != null ? evento.toString() : null;
		}

		@FilterPersistenceAnnotation(persistenceName="CLAVE_USUARIO")
		public String getCveUsuario() {
			return cveUsuario;
		}

		public void setCveUsuario(String cveUsuario) {
			this.cveUsuario = cveUsuario;
		}
		
		public String getIdentificador() {
			return identificador;
		}

		public void setIdentificador(String identificador) {
			this.identificador = identificador;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public void setBitacora(String bitacora) {
			this.bitacora = bitacora;
		}

		public void setEvento(String evento) {
			this.evento = evento;
		}

		@FilterPersistenceAnnotation(persistenceName="FECHA_CREACION", truncateDate = true, operation=OperationType.GREATERTHANEQUAL, paramKey="fechaInical")
		public Date getFechaCreacionInicial() {
			return fechaCreacionInicial;
		}

		public void setFechaCreacionInicial(Date fechaCreacionInicial) {
			this.fechaCreacionInicial = fechaCreacionInicial;
		}

		@FilterPersistenceAnnotation(persistenceName="FECHA_CREACION", truncateDate = true, operation=OperationType.LESSTHANEQUAL, paramKey="fechaFinal")
		public Date getFechaCreacionFin() {
			return fechaCreacionFin;
		}

		public void setFechaCreacionFin(Date fechaCreacionFin) {
			this.fechaCreacionFin = fechaCreacionFin;
		}
		
	}
	
	public void registrar(Bitacora bitacora);
	
	/**
	 * Registra un evento en la bitacora de sistema
	 * @param <T>
	 * @param bitacora. Tipo de bitacora. Requerido
	 * @param evento.  Accion que se esta registrando. Requerido.
	 * @param identificador. Id de la operacion realizada. Requerido.
	 * @param mensaje. Descripcion de la accion registrada. Requerido.
	 * @param detalle. Detalle de la operacion. Ej. informacion en formato json. Opcional.
	 * @param contenedorDetalle. Nombre canonico de la clase del objeto que se desea serializar. Opcional. 
	 * @param cveUsuario Clave Usuario que realiza la operacion. Requerido 
	 */
	public void registrar(TIPO_BITACORA bitacora, EVENTO evento, String identificador, String mensaje, String detalle, String contenedorDetalle, String cveUsuario);
	
	/**
	 * Registra un evento en la bitacora de sistema
	 * @param <T>
	 * @param bitacora. Tipo de bitacora. Requerido
	 * @param evento.  Accion que se esta registrando. Requerido.
	 * @param identificador. Id de la operacion realizada. Requerido.
	 * @param mensaje. Descripcion de la accion registrada. Requerido.
	 * @param objetoDetalle. Instancia del detalle que se quiere registrar. El objeto sera serializado en json y guardado en la bitacora. Opcional. 
	 * @param cveUsuario Clave Usuario que realiza la operacion. Requerido 
	 */
	public <T extends Object> void registrar(TIPO_BITACORA bitacora, EVENTO evento, String identificador, String mensaje, T objetoDetalle, String cveUsuario);
	
	/**
	 * Registra un evento en la bitacora de sistema
	 * @param <T>
	 * @param bitacora. Tipo de bitacora. Requerido
	 * @param evento.  Accion que se esta registrando. Requerido.
	 * @param identificador. Id de la operacion realizada. Requerido.
	 * @param mensaje. Descripcion de la accion registrada. Requerido.
	 * @param detalle. Detalle de la operacion. Ej. informacion en formato json. Opcional.
	 * @param cveUsuario Clave Usuario que realiza la operacion. Requerido 
	 */
	public void registrar(TIPO_BITACORA bitacora, EVENTO evento, String identificador, String mensaje, String detalle, String cveUsuario);
	
	public List<Bitacora> obtener(TIPO_BITACORA bitacora, EVENTO evento, String identificador);
	
	public List<Bitacora> obtener(BitacoraFiltro filtro);		
 
}
