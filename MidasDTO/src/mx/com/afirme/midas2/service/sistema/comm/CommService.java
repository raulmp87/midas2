package mx.com.afirme.midas2.service.sistema.comm;

import java.io.Serializable;
import java.util.Map;

import mx.com.afirme.midas2.dto.TransporteCommDTO;
import mx.com.afirme.midas2.exeption.CommException;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService;

public interface CommService<T> {
	
	/**
	 * Busca una peticion activa para el folio indicado. Si no existe o se encuentra con estatus de cancelado o error retorna <code>true</code>.
	 * 
	 * @param folio
	 * @return
	 */
	public Boolean permiteEnviar(String folio);
	
	/**
	 * Realizar envios por el canal de comunicacion implementado en <code>enviar</code>.
	 * El metodo crea un <code>TransporteCommDTO</code> via el init de la clase implementadora,
	 * registra la nueva bitacora con su detalle, intenta realizar el envio. En caso de exito
	 * guarda el detalle exitoso. En caso de falla, realiza los reintentos correspondientes y registra
	 * el error.
	 * @param comm
	 * @throws CommException
	 */
	public void realizarEnvio(T e) throws CommException;
	
	/***
	 * Crea la bitacora incial
	 * @param comm
	 */
	public void registrar(TransporteCommDTO<T> comm);
	
	/**
	 * Metodo que debe implementar la clase que herede CommService con la funcionalidad de envio.
	 * Este metodo se invocara dentro de <code>realizarEnvio</code>
	 * @param comm
	 * @return
	 * @throws CommException
	 */
	public TransporteCommDTO<T> enviar(TransporteCommDTO<T> comm) throws CommException;
	
	/**
	 * Revisar si se debe reintentar el envio dependiendo del numero de reintentos permitidos
	 * @param comm
	 * @return
	 */
	public Boolean reintentar( TransporteCommDTO<T> comm);
	
	/**
	 * Registar un detalle
	 * @param comm
	 */
	public void registrarDetalle( TransporteCommDTO<T> comm);
	
	/**
	 * Metodo para crear un <code>TranporteCommDTO</code> inicial que debe implementar quien use la clase.
	 * @param e
	 * @return
	 */
	public TransporteCommDTO<T> init(T e);
	
	/**
	 * Metodo para hacer uso del servicio de notificaciones de siniestros. 
	 * @param codigoNotificacion
	 * @param datosNotificacion
	 */
	public void notificar(EnvioNotificacionesService.EnumCodigo codigoNotificacion, Map<String, Serializable> datosNotificacion);
	
	/***
	 * Metodo para guardar la traza del error en un string
	 * @param Exception
	 */
	public String imprimirTrazaError(Exception e);
	
	/***
	 * Metodo que crea un error directo en log, se recomienda usar en métodos previos al envio del WS donde pueda haber un error (excepción) y se 
	 * necesite registrar este
	 */
	public void registrarErrorPrevioEnvio(TransporteCommDTO comm, String mensajeError) throws CommException;
	
}
