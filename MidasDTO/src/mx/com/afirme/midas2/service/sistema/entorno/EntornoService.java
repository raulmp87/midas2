package mx.com.afirme.midas2.service.sistema.entorno;




public interface EntornoService {

	public String getVariable(String nombreVariableEntorno, String valorDefault);
	
}
