package mx.com.afirme.midas2.service.documentos.anexos;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.poliza.PolizaDTO;

@Local
public interface DocumentoAnexoCoberturaService {
	
	public List<byte[]> getAnexosCobertura( List<BigDecimal> idCoberturaList ) throws Exception;
	public List<byte[]> getAnexosCobertura( PolizaDTO polizaDTO ) throws Exception;

}
