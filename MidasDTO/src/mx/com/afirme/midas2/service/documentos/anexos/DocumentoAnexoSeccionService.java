package mx.com.afirme.midas2.service.documentos.anexos;

import java.sql.SQLException;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.documentos.anexos.DocumentoAnexoSeccion;

@Local
public interface DocumentoAnexoSeccionService {
	
	public void saveDocumentoAnexoSeccion(DocumentoAnexoSeccion documentoAnexoSeccion);
	public Long updateDocumentoAnexoSeccion(String action, DocumentoAnexoSeccion documentoAnexoSeccion) throws Exception;
	public List<DocumentoAnexoSeccion> obtenerDocumentoAnexoSeccion(Long idToSeccion);
    public DocumentoAnexoSeccion findDocumentoAnexoSeccionById(Long id) throws SQLException, Exception;
    public List<DocumentoAnexoSeccion> findDocumentoAnexoSeccionByFmxDocName(String fmxDocName) throws SQLException, Exception;
	public List<DocumentoAnexoSeccion> findDocumentoAnexoSeccionByIdToSeccion(Long idToSeccion) throws SQLException, Exception;
	public List<byte[]> getAnexosSeccion( PolizaDTO polizaDTO ) throws Exception;
	public List<byte[]> getAnexosSeccion( Long idToSeccion ) throws Exception;	
	
}
