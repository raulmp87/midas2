package mx.com.afirme.midas2.service.poliza;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia.BitacoraGuia;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.service.CatalogoGenericoService;


public interface PolizaService extends CatalogoGenericoService {
	List<PolizaDTO> buscarPoliza(PolizaDTO filtro, Date fechaInicial,
			Date fechaFinal);
	
	Long obtenerTotalPaginacion(PolizaDTO filtro, Date fechaIncial,
			Date fechaFinal);
	
	ResumenCostosDTO obtenerResumenCostos(PolizaDTO poliza);
	
	List<PolizaDTO> buscarPolizaRenovacion(PolizaDTO filtro, Date fechaIncial, Date fechaFinal, Boolean isCount, Boolean isFixEndoso,
			Boolean exclusionRenovacion);
	
	Long obtenerTotalPolizaRenovacion(PolizaDTO filtro, Date fechaIncial, Date fechaFinal, Boolean isCount,
			Boolean exclusionRenovacion);
	
	/**
	 * Valida que el nombre del asegurado o contratante de la <code>poliza</code> sea igual al <code>nombre</code>.
	 * Este forma de igualdad tiene cierta tolerancia a errores.
	 * @param poliza
	 * @param nombre
	 * @return
	 */
	boolean equalsPolizaNombre(PolizaDTO poliza, String nombre);
	
	boolean esPolizaAutoplazo(BigDecimal idToPoliza, List<SeccionDTO> seccionList);
	
	BigDecimal obtenerNumeroCotizacionSeycos(BigDecimal idToPoliza);
	
	String obtenerXMLPrimerReciboPoliza(BigDecimal idToPoliza);
	
	PolizaDTO find(String codigoProducto, String codigoTipoPoliza,
			Integer numeroPoliza, Integer numeroRenovacion);
	
	BitacoraGuia getBitacoraEnvioPoliza(BigDecimal idToPoliza);
	
	BigDecimal obtenerIdDoctoVersSeycos(BigDecimal idToPoliza, BigDecimal numeroEndoso);
}
