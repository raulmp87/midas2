package mx.com.afirme.midas2.service.poliza;

import java.math.BigDecimal;
import java.util.Date;

import javax.ejb.Local;
import javax.validation.constraints.NotNull;

import org.apache.bval.constraints.Email;
import org.apache.bval.constraints.NotEmpty;

@Local
public interface EnvioEmailPolizaService {

	public class EnviarEmailPorIncisoParameters {

		private BigDecimal idToPoliza;
		private Integer endoso;
		private String from = "administrador.midas@afirme.com";
		private Boolean incluirRecibo = false;
		private Boolean incluirNu = false;
		private Boolean incluirAnexos = false;
		private Date validOn;
		private Date knownOn;

		@NotNull
		public BigDecimal getIdToPoliza() {
			return idToPoliza;
		}

		public void setIdToPoliza(BigDecimal idToPoliza) {
			this.idToPoliza = idToPoliza;
		}
		
		public Integer getEndoso() {
			return endoso;
		}
		
		public void setEndoso(Integer endoso) {
			this.endoso = endoso;
		}

		@NotEmpty
		@Email
		public String getFrom() {
			return from;
		}

		public void setFrom(String from) {
			this.from = from;
		}

		@NotNull
		public Boolean getIncluirRecibo() {
			return incluirRecibo;
		}
		
		public void setIncluirRecibo(Boolean incluirRecibo) {
			this.incluirRecibo = incluirRecibo;
		}
		
		@NotNull
		public Boolean getIncluirNu() {
			return incluirNu;
		}
		
		public void setIncluirNu(Boolean incluirNu) {
			this.incluirNu = incluirNu;
		}
		
		@NotNull
		public Boolean getIncluirAnexos() {
			return incluirAnexos;
		}
		
		public void setIncluirAnexos(Boolean incluirAnexos) {
			this.incluirAnexos = incluirAnexos;
		}
		
		public Date getValidOn() {
			return validOn;
		}
		
		public void setValidOn(Date validOn) {
			this.validOn = validOn;
		}
		
		public Date getKnownOn() {
			return knownOn;
		}
		
		public void setKnownOn(Date knownOn) {
			this.knownOn = knownOn;
		}
	}

	/**
	 * Envía un email a cada uno de los incisos con su respectios documentos utilizando el <code>emailContacto</code>.
	 * @param parameters
	 */
	public void enviarEmailPorInciso(
			EnviarEmailPorIncisoParameters parameters);

}
