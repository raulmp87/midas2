package mx.com.afirme.midas2.service.poliza.renovacionmasiva;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas.catalogos.codigo.postal.PaisDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoDTO;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoId;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDTO;
import mx.com.afirme.midas.cotizacion.documento.DocumentoAnexoReaseguroCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.DocumentoDigitalCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.catalogos.DomicilioImpresion;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.formapago.NegocioFormaPago;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.estilovehiculo.NegocioEstiloVehiculo;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacion;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionCobertura;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionDesc;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionDescId;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionId;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioEstado;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioMunicipio;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.OrdenRenovacionMasiva;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.OrdenRenovacionMasivaDet;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.OrdenRenovacionMasivaDetId;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia.RegistroGuiasProveedorId;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAuto;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.comentarios.Comentario;
import mx.com.afirme.midas2.dto.ControlDescripcionArchivoDTO;
import mx.com.afirme.midas2.dto.LayoutRenovacion;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.endoso.cotizacion.auto.CotizacionEndosoDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO;
import mx.com.afirme.midas2.dto.poliza.renovacionmasiva.ValidacionRenovacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;

@Local
@SuppressWarnings("rawtypes")
public interface RenovacionMasivaService {
	
	PolizaDTO getPolizaDTOById(BigDecimal idToPoliza);
	
	List<CotizacionDTO> obtieneCotizacionesPorPolizaAnterior(BigDecimal idToPoliza);
	
	BitemporalCotizacion obtieneBitemporalCotizacionByIdCotizacion(BigDecimal idToCotizacion);
	
	NegocioRenovacion obtieneNegocioRenovacionById(NegocioRenovacionId id);
	
	List<MonedaDTO> listarMonedasAsociadasTipoPoliza(BigDecimal idToTipoPoliza);
	
	List<NegocioFormaPago> obtieneNegocioFormaPagoByFormaPagoNegocioProducto(BigDecimal idFormaPago, Long idToNegProducto);
	
	List<NegocioDerechoPoliza> obtieneNegocioDerechoPolizaByNegocioImporte(Long idToNegocio, Double importe);
	
	NegocioSeccion obtieneNegocioSeccionByid(BigDecimal idNegocioSeccion);
	
	List<NegocioEstado> obtenerEstadosPorNegocioId(Long idToNegocio);
	
	List<NegocioMunicipio> obtenerMunicipiosPorEstadoId(Long idToNegocio, String estadoId);
	
	List<MarcaVehiculoDTO> obtenerMarcaVehiculoPorClaveTipoBien(String claveTipoBien);
	
	MarcaVehiculoDTO obtenerMarcaVehiculoPorMarcaId(BigDecimal marcaId);
	
	List<NegocioEstiloVehiculo> obtenerNegocioEstiloVehiculoPorIdNegocioSeccion(BigDecimal idToNegSeccion);
	
	ModeloVehiculoDTO obtenerModeloVehiculoPorId(ModeloVehiculoId modeloVehiculoId);
	
	List<TipoUsoVehiculoDTO> getTipoUsoVehiculoDTOByNegSeccionEstiloVehiculo(BigDecimal idToNegSeccion, BigDecimal idTcTipoVehiculo);
	
	List<NegocioPaqueteSeccion> obtenerNegocioPaqueteSeccionPorNegocioSeccion(BigDecimal idToNegSeccion);
	
	List<NegocioCobPaqSeccion> getCoberturas(Long IdNegocio,
			BigDecimal idToProducto, BigDecimal idTipoPoliza,
			BigDecimal idToSeccion, Long idPaquete, String idEstado,
			String idMunicipio, Short idMoneda);
	
	
	List<EntidadBitemporal> obtenerIncisoCotinuityList(Long continuityId);
	
	List<EntidadBitemporal> obtenerAutoIncisoCotinuityList(Long continuityId);
	
	List<EntidadBitemporal> obtenerSeccionIncisoCotinuityList(Long continuityId);
	
	List<EntidadBitemporal> obtenerCoberturasCotinuityList(Long continuityId);
	
	CotizacionDTO obtenerCotizacionPorId(BigDecimal idToCotizacion);
	
	NegocioProducto getNegocioProductoByCotizacion(CotizacionDTO cotizacionARenovar);
	
	NegocioTipoPoliza getPorIdNegocioProductoIdToTipoPoliza(Long idToNegProducto, BigDecimal idToTipoPoliza);
	
	CotizacionDTO crearCotizacion(CotizacionDTO cotizacionDTO);
	
	void saveComisionCotizacionDTO(ComisionCotizacionDTO item);
	
	void saveDocAnexoCotDTO(DocAnexoCotDTO item);
	
	void saveDocumentoAnexoReaseguroCotizacionDTO(DocumentoAnexoReaseguroCotizacionDTO item);
	
	void saveDocumentoDigitalCotizacionDTO(DocumentoDigitalCotizacionDTO item);
	
	void saveTexAdicionalCotDTO(TexAdicionalCotDTO item);
	
	void saveDocumentoDigitalSolicitudDTO(DocumentoDigitalSolicitudDTO item);
	
	void saveComentario(Comentario item);
	
	CotizacionDTO guardarCotizacion(CotizacionDTO cotizacionDTO);
	
	IncisoCotizacionDTO prepareGuardarInciso(BigDecimal idToCotizacion,
			IncisoAutoCot incisoAutoCot, IncisoCotizacionDTO incisoCotizacionDTO,
			List<CoberturaCotizacionDTO> coberturaCotizacionList, Double valorPrimaNeta,
			Double primaAsegurada, Short claveContrato, Short claveObligatoriedad);
	
	void calcular(IncisoCotizacionDTO nuevoIncisoCotizacion);
		
	ResumenCostosDTO obtenerResumenCotizacion(CotizacionDTO cotizacionDTO);
	
	void igualarPrima(BigDecimal idTpCotizacion, Double primaTotal);
	
	List<NegocioPaqueteSeccion> obtieneNegocioPaqueteSeccionPorNegSeccionPaqueteId(BigDecimal idToNegSeccion, Long paqueteId);
	
	Usuario getUsuarioActual();
	
	BigDecimal saveAndGetIdOrdenRenovacion(OrdenRenovacionMasiva ordenRenovacion);
	
	void saveOrdenRenovacionMasivaDet(OrdenRenovacionMasivaDet item);
	
	TerminarCotizacionDTO terminarCotizacion(BigDecimal idToCotizacion);
	
	void guardarCotizacionSimple(CotizacionDTO item);
	
	void complementarDatosInciso(IncisoCotizacionDTO incisoCotizacion);
	
	Map<String, String> emitir(CotizacionDTO cotizacionDTO);
	
	void actualizarEstatusCotizacionEmitida(BigDecimal idToCotizacion);
		
	List<PolizaDTO> listarPolizasRenovacion(PolizaDTO polizaDTO, Boolean filtrar, Boolean isCount, Boolean isFixEndoso);
	
	List<OrdenRenovacionMasiva> listarOrdenesRenovacion(OrdenRenovacionMasiva orden, Boolean filtrar, String nombreUsuario);
	
	List<OrdenRenovacionMasivaDet> listarOrdenRenovacionDetalleByOrdenRenovacion(BigDecimal idToOrdenRenovacion);
	
	void cancelarOrdenRenovacion(BigDecimal idToOrdenRenovacion);
	
	String terminarOrdenRenovacion(BigDecimal idToOrdenRenovacion);
	
	void cancelarDetalleOrdenRenovacion(BigDecimal idToOrdenRenovacion, List<PolizaDTO> polizaList);
	
	String terminarPolizasDetalleOrdenRenovacion(BigDecimal idToOrdenRenovacion, List<PolizaDTO> polizaList);
	
	OrdenRenovacionMasiva obtieneOrdenRenovacionMasiva(BigDecimal idToOrdenRenovacion);
	
	IncisoAutoCot regresaIncisoAutoCot(BigDecimal idToCotizacion);
	
	String getCamposExcel(IncisoAutoCot incisoAutoCot, int campo, List<EntidadBitemporal> coberturasCotinuityList);
	
	TransporteImpresionDTO obtieneXMLOrdenRenovacion(OrdenRenovacionMasivaDet orden, Locale locale);
	
	List<BigDecimal> cancelaCotizacionesValidacion(List<ValidacionRenovacionDTO> validaciones);
	
	List<EntidadBitemporal> obtenerDatoSeccionContinuityList(Long continuityId);
	
	void saveDatoIncisoCotAuto(DatoIncisoCotAuto datoInciso);
	
	PolizaDTO getCantidadSiniestro(BigDecimal idToPoliza);
	
	NegocioRenovacionDesc obtenerNegocioRenovacionDescPorId(NegocioRenovacionDescId id);
	
	String enviarProveedorOrdenRenovacion(BigDecimal idToOrdenRenovacion, List<ControlDescripcionArchivoDTO> pdf, InputStream excel);
	
	DomicilioImpresion obtieneDomicilioPorId(BigDecimal idDomicilio);
	
	ClienteGenericoDTO obtieneClienteContratantePorId(BigDecimal idCliente);
	
	FormaPagoDTO obtieneFormaPagoPorId(BigDecimal idFormaPago);
	
	MedioPagoDTO obtieneMedioPagoPorId(BigDecimal idMedioPago);
	
	InputStream obtenerOrdenRenovacionProveedor(BigDecimal idToOrdenRenovacion);
	
	int getCantidadSiniestroTotal(BigDecimal idToPoliza);
	
	BigDecimal obtieneValorDescuento(BigDecimal idToCotizacion);
	
	Short obtieneTipoRiesgo(BigDecimal idToCotizacion);
	
	TipoPolizaDTO obtieneTipoPolizaPorId(BigDecimal idTipoPoliza);
	
	void calcularCotizacion(CotizacionDTO cotizacion);
	
	CoberturaDTO getCobertura(BigDecimal idToCobertura);
	
	String getIdAgente(Long id);
	
	CuentaPagoDTO obtenerConductoCobro(BigDecimal idToCotizacion);
	
	Long obtenerTotalPolizasRenovacion(PolizaDTO polizaDTO, Boolean filtrar, Boolean isCount);
	
	Long listarOrdenesRenovacionCount(OrdenRenovacionMasiva orden, Boolean filtrar, String nombreUsuario);
	
	EstiloVehiculoDTO getEstiloVehiculo(BigDecimal idTcMarcaVehiculo, BigDecimal idToNegSeccion, BigDecimal idMoneda, String claveEstilo);
	
	void setNuevoPorcentajeDeducibleDaniosMateriales(CotizacionDTO cotizacion, Double nuevoPorcentajeDeducible);
	
	NegocioDerechoPoliza obtenerDechosPolizaDefault(Long idToNegocio);
	
	List<CoberturaCotizacionDTO> obtenerCoberturasNegocio(NegocioPaqueteSeccion negocioPaqueteSeccion, IncisoCotizacionDTO incisoCotizacion);
	
	NegocioPaqueteSeccion obtenerNegocioPaqueteSeccionPorId(Long id);
	
	String obtenerMontoPrimerRecibo(CotizacionDTO cotizacion);
	
	void eliminaCotizacionFallida(CotizacionDTO cotizacionDTO);
	
	void eliminaCotizacionFallidaPolizaAnt(BigDecimal idToPoliza);
	
	MensajeDTO getValidaNuevoNegocio(BigDecimal idToPoliza, BigDecimal pIdToNegSeccion);
	
	OrdenRenovacionMasivaDet obtieneOrdenRenovacionMasivaDet(OrdenRenovacionMasivaDetId id);
	
	TerminarCotizacionDTO validarEmisionCotizacion(BigDecimal idToCotizacion);
	
	void cancelarCotizacion(BigDecimal idToCotizacion);
	
	MensajeDTO programaPago(BigDecimal idToPoliza);
	
	List<CotizacionEndosoDTO> buscarCotizacionesEndosoFiltrado(CotizacionEndosoDTO filtrosBusqueda);
	
	void deleteCotizacionEndoso(CotizacionEndosoDTO pendiente);
	
	CotizacionContinuity findCotizacionContinuity(BitemporalCotizacion cotizacion);
	
	List<ControlEndosoCot> findControlEndosoCot(Map<String,Object> properties);
	
	void borrarMovimientos(ControlEndosoCot controlEndosoCot);
	
	void removeControlEndosoCot(ControlEndosoCot controlEndosoCot);
	
	void cancelarSolicitudesAutorizacion(BitemporalCotizacion cotizacion);
	
	PaisDTO getPais(String countryId);
	
	List<EntidadBitemporal> obtenerTexAdicionalCotCotinuityList(Long continuityId);
	
	String enviarProveedorPolizasSeleccionadas(String nameFile, InputStream pdfList);
	
	Double getPctePagoFraccionado(Integer idFormaPago, Short idMoneda);
	
	MensajeDTO polizaConIncisosPerdidaTotal (BigDecimal idToPoliza);
	
	ResumenCostosDTO resumenCostosCaratulaPoliza(BigDecimal idToPoliza);
	
	SolicitudDTO obtieneSolicitudRenovacion(BigDecimal idToCotizacion);
	
	void calcularDescuentoPorEstado(CotizacionDTO cotizacion, Double descuentoPorNoSiniestro, NegocioRenovacionDescId id, boolean renovarEmitir);
	
	MensajeDTO getXmlCartaRenovacion(BigDecimal idToPoliza);
	
	void saveCargaMasiva(List<RegistroGuiasProveedorId> rowsValidos);
	
	void procesarEstatusPendientes();
	
	TransporteImpresionDTO getReporteEfectividad(OrdenRenovacionMasiva ordenRenovacion)throws Exception;
	
	void procesarTareaEstatusPendientes();
	
	ResumenCostosDTO resumenCostosEmitidos(BigDecimal idToPoliza);
	
	NegocioRenovacionCobertura obtenerNegocioRenovacionCobertura(NegocioPaqueteSeccion negocioPaqueteSeccion, Short tipoRiesgo, BigDecimal idToCobertura);
	
	MensajeDTO programaPagoEndoso(BigDecimal idToPoliza);
	
	void fixEndosoRenovacion(PolizaDTO poliza);
	
	List<OrdenRenovacionMasivaDet> obtienePolizaEnProcesoRenovacion(BigDecimal idToPoliza);
	
	List<LayoutRenovacion> obtenerListaLayout(String fechaIncio, String fechaFin, Long idToNegocio, Long idToProducto);
}
