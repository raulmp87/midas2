package mx.com.afirme.midas2.service.poliza.seguroobligatorio;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import org.joda.time.DateTime;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;

@Local
public interface SeguroObligatorioService {

	public List<IncisoCotizacionDTO> getIncisosCotizacion(BigDecimal idToCotizacion);
	
	public Negocio getNegocioSeguroObligatorio(BigDecimal idToPoliza);
	
	public NegocioSeccion getNegSeccionSOInciso(Negocio negSeguroOblig, CotizacionDTO cotizacion, IncisoCotizacionDTO inciso);
	
	public NegocioPaqueteSeccion obtieneNegocioPaqueteSeccionPorNegSeccion(BigDecimal idToNegSeccion);
	
	public NegocioDerechoPoliza obtieneNegocioDerechoPolizaSOByNegocio(Long idToNegocio);
	
	public void relacionaPolizaSeguroObligatorio(PolizaDTO original, PolizaDTO obligatoria);
	
	public List<BigDecimal> obtienePolizasCrearSO(String idToPolizaList);
	
	public MensajeDTO validaVehiculoSO(BigDecimal idToPoliza, BigDecimal numeroInciso);
	
	public MensajeDTO validaPagoSO(BigDecimal idToPolizaAnexa);
	
	public MensajeDTO validaPagoBaseSO(BigDecimal idToPoliza);
	
	public void saveCoberturaCotizacion(CoberturaCotizacionDTO cobertura);
	
	public NegocioDerechoPoliza obtenerDerechosPolizaCero(Long idToNegocio);
	
	public Double getPrimaTotalSeguroObligatorio();
	
	public PolizaDTO obtienePolizaAnexa(BigDecimal idToPoliza);
	
	public BitemporalCotizacion obtieneBitemporalCotizacionByIdCotizacion(BigDecimal idToCotizacion, DateTime validOn);
	
	public List<EntidadBitemporal> obtenerIncisoCotinuityList(Long continuityId, DateTime validOn);
	
	public List<EntidadBitemporal> obtenerAutoIncisoCotinuityList(Long continuityId, DateTime validOn);
	
	public List<EntidadBitemporal> obtenerSeccionIncisoCotinuityList(Long continuityId, DateTime validOn);
	
	public List<EntidadBitemporal> obtenerCoberturasCotinuityList(Long continuityId, DateTime validOn);
	
	public List<EntidadBitemporal> obtenerDatoSeccionContinuityList(Long continuityId, DateTime validOn);
	
	public List<BigDecimal> obtieneIncisosCrearSO(BigDecimal idToPoliza, Short numeroEndoso);
	
	public Double getSumaAseguradaSeguroObligatorio(Long negocioPaqueteId);
	
	public MensajeDTO emiteEndosoAltaSO(BigDecimal idToPoliza, Short numeroEndoso);
	
	public MensajeDTO emiteEndosoDatosSO(BigDecimal idToPoliza, Short numeroEndoso);
	
	public Date obtieneFechaValidez();
	
	public Boolean estaHabilitado(BigDecimal tipo);
	
	public Boolean esNegocioSeguroObligatorio(Long idToNegocio);
}
