package mx.com.afirme.midas2.service.calculos;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.calculos.DetalleCalculoComisiones;
import mx.com.afirme.midas2.domain.prestamos.ConfigPrestamoAnticipo;
import mx.com.afirme.midas2.dto.calculos.DetalleSolicitudCheque;
import mx.com.afirme.midas2.util.MidasException;
/**
 * 
 * @author vmhersil
 *
 */
@Local
public interface InterfazMizarService {
	/**
	 * Actualiza el estatus de una solicitud de cheque de MIZAR de un calculo, este cheque representa lo que se le va a pagar en comisiones
	 * a un agente.
	 * @param cheque
	 * @throws MidasException
	 */
	public void actualizarSolitudChequeMizar(DetalleCalculoComisiones cheque) throws MidasException;
	/**
	 * Ejecuta spInsHead en SQL Server el cheque de MIZAR-Cabecera
	 * @param prestamo
	 * @throws MidasException
	 */
	public void actualizarSolitudChequeMizarPrestamo(ConfigPrestamoAnticipo prestamo) throws MidasException; 
	/**
	 * Actualiza en MIZAR los detalles de una solicitud de cheque de un calculo, es decir, una solicitud de cheque puede tener
	 * al menos un o mas detalles de solicitud de cheque, estos se actualizan su estatus por medio de un stored procedure de SQLSERVER
	 * utilizando el datasource de Mizar
	 * @param cheque
	 * @param idCalculo
	 * @throws MidasException
	 */
	public void actualizarDetalleSolitudChequeMizar(DetalleSolicitudCheque cheque,Long idSolicitudCheque,Long idCalculo,Long claveAgente) throws MidasException;
	/**
	 * Deja los cheques como liberados en Mizar
	 * @param idCalculo
	 * @throws MidasException
	 */
	public void importarSolicitudesChequesAMizar(Long idCalculo) throws MidasException;
	

	public void preparaSolicitudChequeMizar(DetalleCalculoComisiones detalleCalculo);
}
