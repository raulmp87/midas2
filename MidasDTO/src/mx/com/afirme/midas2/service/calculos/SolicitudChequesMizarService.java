package mx.com.afirme.midas2.service.calculos;

import java.util.Date;

import javax.ejb.Local;

import mx.com.afirme.midas.interfaz.solicitudcheque.SolicitudChequeDTO;
import mx.com.afirme.midas2.dao.calculos.SolicitudChequesMizarDao.ClavesTransaccionesContables;
import mx.com.afirme.midas2.util.MidasException;
/**
 * 
 * @author vmhersil
 *
 */
@Local
public interface SolicitudChequesMizarService{
	
	public Long iniciarCalculoComisiones(Long idConfiguracionComisiones,Long idCalculoComisiones,Long idCalculoTemporal,String queryAgentes) throws MidasException;	
		
	public Long iniciarCalculoBonos(Long pIdCalculoIn,Long pIdProgramacion,Long pIdConfigBono,Long pIdModoEjec,Date pFechaAlta,Date pFechaEjecucion,String pHora,Long pPeriodoEjecucion,Long pTipoConfiguracion,String pUsuarioAlta,Long pTipoBono,Long pClaveEstatus,Integer pActivo,Integer pEsNegocio) throws MidasException;
	
	public Long iniciarCalculoProvisiones(Long pIdCalculoIn,Long pIdProgramacion,Long pIdConfigBono,Long pIdModoEjec,Date pFechaAlta,Date pFechaEjecucion,String pHora,Long pPeriodoEjecucion,Long pTipoConfiguracion,String pUsuarioAlta,Long pTipoBono,Long pClaveEstatus,Integer pActivo,Integer pEsNegocio) throws MidasException;
	
	/**
	 * Metodo para contabilizar movimientos y afectar cuentas contables de Mizar.
	 * @param identificador Es el identificador para obtener el cursor CONTABLE, en caso de COMISIONES y BONOS, se envia idCalculo|idAgente, en caso del prestamo solo se envia el idPrestamo
	 * @param claveTransaccionContable dependiendo de la clave contable
	 * @throws MidasException
	 */
	public void contabilizarMovimientos(String identificador,ClavesTransaccionesContables claveTransaccionContable) throws MidasException;
	
	/**
	 * Agrega una solicitud de cheque en MIZAR a partir de una solicitud de cheque de SEYCOS
	 * @param solicitudCheque Solicitud de cheque de SEYCOS
	 */
	public void agregarSolicitudChequeMIZAR(SolicitudChequeDTO solicitudCheque) throws Exception;
	
	/**
	 * Procesa en MIZAR las solicitudes de cheque recien importadas desde SEYCOS
	 * @param idSesion Es el identificador de la sesión con la que están relacionadas todas las solicitudes de cheque que serán procesadas
	 */
	public void procesarSolicitudesChequeEnMIZAR(Long idSesion) throws Exception;
	
	
}
