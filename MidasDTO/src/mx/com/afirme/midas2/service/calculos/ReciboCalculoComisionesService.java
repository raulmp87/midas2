package mx.com.afirme.midas2.service.calculos;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.calculos.DetalleCalculoComisiones;
import mx.com.afirme.midas2.domain.calculos.ReciboCalculoComisiones;
import mx.com.afirme.midas2.util.MidasException;
/**
 * Servicio para gestionar los recibos de los detalles del preview del calculo a nivel agente/recibo
 * @author vmhersil
 *
 */
@Local
public interface ReciboCalculoComisionesService {
	/**
	 * Metodo para guardar el recibo del detalle de un calculo para su posterior consulta
	 * @return
	 * @throws MidasException
	 */
	public Long saveReciboCalculoComisiones(ReciboCalculoComisiones detalleCalculo) throws MidasException;
	/**
	 * Carga la informacion del el recibo del detalle de un registro del calculo donde se mostrara el agente y/o promotoria
	 * a quien se les paga la comision
	 * @param idDetalleCalculo Llave primaria del detalle del calculo toDetalleCalculoComisiones.id
	 * @return
	 * @throws MidasException
	 */
	public ReciboCalculoComisiones loadById(Long idReciboCalculo) throws MidasException;
	/**
	 * Carga la informacion del el recibo del detalle de un registro del calculo donde se mostrara el agentes y/o promotoria
	 * a quien se les paga la comision
	 * @param reciboCalculo
	 * @return
	 * @throws MidasException
	 */
	public ReciboCalculoComisiones loadById(ReciboCalculoComisiones reciboCalculo) throws MidasException;
	/**
	 * Carga la informacion de todos los recibos del detalle de un calculo a nivel agente  
	 * @param idDetalleCalculo
	 * @return
	 * @throws MidasException
	 */
	public List<ReciboCalculoComisiones> listarRecibosDeDetalleCalculo(Long idDetalleCalculo) throws MidasException;
	/**
	 * Carga la informacion de todos los recibos del detalle de un calculo a nivel agente  
	 * @param detalleCalculo
	 * @return
	 * @throws MidasException
	 */
	public List<ReciboCalculoComisiones> listarRecibosDeDetalleCalculo(DetalleCalculoComisiones detalleCalculo) throws MidasException;
	/**
	 * Busca los recibos bajo ciertos criterios de busqueda
	 * @param filtro
	 * @return
	 */
	public List<ReciboCalculoComisiones> findByFilters(ReciboCalculoComisiones filtro);
	/**
	 * Elimina logicamente todos los recibos que pertenecen a un detalle del preview del calculo
	 * @param idCalculo
	 * @throws MidasException
	 */
	public void deleteRecibosPorDetalleCalculo(Long idDetalleCalculo) throws MidasException;
	/**
	 * Elimina logicamente todos los recibos que pertenecen a un detalle del preview del calculo
	 * @param calculo
	 * @throws MidasException
	 */
	public void deleteRecibosPorDetalleCalculo(DetalleCalculoComisiones detalleCalculo) throws MidasException;
	/**
	 * Se vuelve a rehabilitar todos los recibos que pertenecen a un detalle del preview del calculo
	 * @param idCalculo
	 * @throws MidasException
	 */
	public void rehabilitarRecibosPorDetalleCalculo(Long idDetalleCalculo) throws MidasException;
	/**
	 * Se vuelve a rehabilitar todos los recibos que pertenecen a un detalle del preview del calculo
	 * @param calculo
	 * @throws MidasException
	 */
	public void rehabilitarRecibosPorDetalleCalculo(DetalleCalculoComisiones calculo) throws MidasException;
	
	public void generarRecibosCalculoComisiones(Long idCalculo) throws MidasException;
}
