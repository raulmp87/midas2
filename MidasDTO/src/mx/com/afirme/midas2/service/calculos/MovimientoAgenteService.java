package mx.com.afirme.midas2.service.calculos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.calculos.MovimientoAgente;
import mx.com.afirme.midas2.dto.calculos.DetalleCalculoComisionesView;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.util.MidasException;
/**
 * Servicio para la consulta d elos movimientos de los agentes
 * @author vmhersil
 *
 */
@Local
public interface MovimientoAgenteService{
	public List<MovimientoAgente> findByFilters(MovimientoAgente filtro);
	
	public List<DetalleCalculoComisionesView> obtenerMovimientosPorAgentes(Long idConfig,List<AgenteView> listadoAgentes) throws MidasException;
}
