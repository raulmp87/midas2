package mx.com.afirme.midas2.service.calculos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.calculos.AgentesCalculoTemporal;
import mx.com.afirme.midas2.util.MidasException;
@Local
/**
 * 
 */
public interface AgentesCalculoTemporalService {
	public Long save(AgentesCalculoTemporal datoTemporal)throws MidasException;
	
	public Long save(Long idRegistro,String catalogo,Long idCalculo) throws MidasException;
	
	public Long getNextIdCalculo();
	
	public void saveAll(List<Long> idRegistros,String catalogo,Long idCalculo) throws MidasException;
	
	public List<AgentesCalculoTemporal> findByCatalogo(String catalogo,Long idCalculo) throws MidasException;
	
	public void delete(Long idCalculo) throws MidasException;
}
