package mx.com.afirme.midas2.service.calculos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.calculos.CalculoComisiones;
import mx.com.afirme.midas2.domain.comisiones.ConfigComisiones;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.CalculoBonoEjecucionesView;
import mx.com.afirme.midas2.dto.fuerzaventa.CalculoComisionesView;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.fuerzaventa.GenericMailService;
import mx.com.afirme.midas2.util.MidasException;
/**
 * Interface para la gestion de calculo de comisiones
 * @author vmhersil
 *
 */
@Local
public interface CalculoComisionesService extends GenericMailService{
	/**
	 * Carga la configuracion del pago de comisiones, obteniendo los agentes como 
	 * resultado de la configuracion, y de el listado de agentes se consulta a la tabla de movimientos donde 
	 * este involucrado cada agente y se obtiene el monto total del importe de comisiones por cada agente. 
	 * Dicho monto total se sumara por todos los agentes para obtener el monto total que se pagara de comisiones
	 * y representara el importe del pago de comision del calculo realizado.
	 * @param idConfigComisiones
	 * @return
	 */
	public Long generarCalculo(Long idConfigComisiones,List<AgenteView> agentesDelCalculo,Long idCalculoTemporal,String queryAgentes) throws MidasException;
	/**
	 * Carga la configuracion del pago de comisiones, obteniendo los agentes como 
	 * resultado de la configuracion, y de el listado de agentes se consulta a la tabla de movimientos donde 
	 * este involucrado cada agente y se obtiene el monto total del importe de comisiones por cada agente. 
	 * Dicho monto total se sumara por todos los agentes para obtener el monto total que se pagara de comisiones
	 * y representara el importe del pago de comision del calculo realizado.
	 * @param calculo
	 * @return
	 * @throws MidasException
	 */
	public Long generarCalculo(ConfigComisiones configuracion,List<AgenteView> agentesDelCalculo,Long idCalculoTemporal,String queryAgentes) throws MidasException;
	
	/**
	 * Metodo para recalcular las comisiones por medio del id de calculo de comisiones
	 * @param idConfigComisiones
	 * @param agentesDelCalculo
	 * @param idCalculoComisiones
	 * @return
	 * @throws MidasException
	 */
	public Long recalcularCalculoComisiones(Long idConfigComisiones,List<AgenteView> agentesDelCalculo,Long idCalculoComisiones,Long idCalculoTemporal,String queryAgentes) throws MidasException;
	
	public List<AgenteView> cargarAgentesPorConfiguracion(Long idConfigComisiones) throws MidasException;
	/**
	 * Permite cargar por medio del id del calculo los datos del calculo de la comision.
	 * @param idCalculo
	 * @return
	 * @throws MidasException
	 */
	public CalculoComisiones loadById(Long idCalculo) throws MidasException;
	/**
	 * Permite cargar los datos del calculo de la comision.
	 * @param calculo
	 * @return
	 * @throws MidasException
	 */
	public CalculoComisiones loadById(CalculoComisiones calculo) throws MidasException;
	/**
	 * Enlista los calculos de comisiones para mostrarlos en el preview de comisiones
	 * @param filtro
	 * @return
	 */
	public List<CalculoComisiones> findByFilters(CalculoComisiones filtro);
	/**
	 * Enlista los calculos de comisiones para mostrarlos en el preview de comisiones
	 * @param filtro
	 * @return
	 */
	public List<CalculoComisionesView> findByFiltersView(CalculoComisiones filtro);
	/**
	 * Inserta o actualiza los datos del calculo, se debe de utilizar para registrar
	 * un nuevo calculo, actualizar el calculo o eliminar de forma logica el calculo cambiando
	 * el estatus del mismo
	 * @param calculo
	 * @return
	 */
	public Long saveCalculoComisiones(CalculoComisiones calculo);
	/**
	 * Elimina logicamente un preview del calculo de comisiones
	 * cambiando el estatus a inactivo
	 * @param idCalculo
	 */
	public void deleteCalculoComisiones(Long idCalculo) throws MidasException;
	/**
	 * Elimina logicamente un preview del calculo de comisiones
	 * cambiando el estatus a inactivo
	 * @param calculo
	 */
	public void deleteCalculoComisiones(CalculoComisiones calculo) throws MidasException;
	
	public List<AgenteView> obtenerAgentes(ConfigComisiones configuracion) throws Exception;
	
	/**
	 * Actualiza el estatus del calculo
	 * @param idCalculo
	 * @param estatus
	 * @throws Exception
	 */
	public void autorizarPreview(CalculoComisiones calculo);
	/**
	 * Se cambia el estatus del calculo a cancelado siempre y cuando no se haya autorizado el calculo, una vez ya autorizado
	 * ya no podra cancelarse.
	 * @param idCalculo Clave del calculo a cancelar
	 * @throws Exception
	 */
	public void cancelarCalculoPreview(CalculoComisiones calculo) throws Exception;
	
	/**
	 * indica si del listado de agentes alguno de ellos ya existe en algun calculo previo pendiente por autorizar, si es asi,
	 * entonces en cada calculo se va a cambiar de estatus a Pendiente-Recalcular, para que al momento de consultar un calculo si
	 * tiene este estatus se pida recalcular. 
	 * @param listaAgenteCalculo
	 * @return
	 * @throws MidasException
	 */
	public void validarAgentesEnCalculosPendientes(Long idCalculoTemporal)throws MidasException;	
	
	/**
	 * Metodo para generar un reporte del calculo de comisiones en excel. 
	 * @param calculoCoisiones
	 * @throws Exception
	 */
	public TransporteImpresionDTO generarReporteComisiones(CalculoComisiones calculoComisiones) throws Exception;
	
	/**
	 * Muestra el listado de calculos que pueden pagarse a MIZAR. Estos deben de ser los unicos cheques 
	 * marcados en MIZAR como transferencias electronicas
	 * @param filtro Permite filtrar por fechas los calculso pendientes por pagar
	 * @return
	 */
	public List<CalculoComisiones> obtenerCalculosPendientesPorPagar(CalculoComisiones filter) throws MidasException;
	/**
	 * Obtiene la lista de configuraciones programadas activas que tienen modo automatico.
	 * @return
	 * @throws MidasException
	 */
	public List<ConfigComisiones> obtenerConfiguracionesAutomaticasActivas() throws MidasException;
	
	public void enviarCorreoAutorizacionMovimiento(Long idCalculo,
			Long idProceso, Long idMovimiento, String mensaje, int tipoTemplate);
	
	public void enviarCorreoGeneracionPreview(Long idCalculo, Long idProceso,
			Long idMovimiento, String mensaje, int tipoTemplate);
	
	public Long getNextIdCalculoTemporal() throws MidasException;
	
	public Long guardarAgentesEnTemporal(List<AgenteView> agentes,Long idCalculoTemporal) throws MidasException;
	
	/**
	 * Obtiene el query para la lista de agentes de acuerdo a la configuracion.
	 * @param idConfigComisiones
	 * @return
	 * @throws MidasException
	 */
	public String cargarAgentesPorConfiguracionQuery(Long idConfigComisiones) throws MidasException;
	/**
	 * @author jmendoza
	 * @param idCalculo
	 * @return
	 * @throws MidasException
	 */
	public Long eliminarComisionesCero(Long idCalculo) throws MidasException;
	
	public List<CalculoBonoEjecucionesView> listaCalculoComisionesMonitorEjecucion() throws MidasException;
	
	public void initialize();
}
