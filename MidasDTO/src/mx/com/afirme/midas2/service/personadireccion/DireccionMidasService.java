package mx.com.afirme.midas2.service.personadireccion;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.personadireccion.CiudadMidas;
import mx.com.afirme.midas2.domain.personadireccion.ColoniaMidas;
import mx.com.afirme.midas2.domain.personadireccion.DireccionMidas;
import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;
import mx.com.afirme.midas2.domain.personadireccion.PaisMidas;

@Local
public interface DireccionMidasService {
	
	public void guardar(DireccionMidas direccion);
	
	public DireccionMidas obtener(Long id);
	
	public List<PaisMidas> obtenerPaises();
	
	public List<EstadoMidas> obtenerEstados(String paisId);
	
	public List<CiudadMidas> obtenerCiudad(String estadoId);
	
	public List<ColoniaMidas> obtenerColoniasPorCiudad(String ciudadId);
	
	public List<ColoniaMidas> obtenerColoniasPorCP(String codigoPostal);
	
	public CiudadMidas obtenerCiudadPorCP(String codigoPostal);	
	
	public CiudadMidas obtenerCiudadPorColonia(String coloniaId);	
	
	public String obtenerCodigoPostalPorColonia(String coloniaId);

}
