package mx.com.afirme.midas2.service.personadireccion;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.personadireccion.PersonaMidas;
import mx.com.afirme.midas2.domain.personadireccion.PersonaMoralMidas;

@Local
public interface PersonaMidasService<P extends PersonaMidas> {
	
	public void guardar(P persona);
	
	public P obtenerPersona(Long id);
	
	public List<PersonaMoralMidas> obtenerPersonas(String sector, String rama, String subRama);
	
	public List<P> buscarPersonas(P filtro);
	
}
