package mx.com.afirme.midas2.service.Excluir;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.catalogos.ExcluirAgentes.Excluir;

@Local
public interface ExcluirService {
	
	public void saveExcluirAgente(Excluir excluir);

	public void eliminarExcluir(Excluir excluir);

	public List<Excluir> getListExcluirAgentes(Excluir excluir);

}
