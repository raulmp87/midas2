package mx.com.afirme.midas2.service;

import java.util.List;

/**
 * @author aavacor
 *
 */
public interface ExcelRowValidationMapper<T> extends ExcelRowMapper<T> {
	
	void validateMapperEntry(T mapperEntry, List<T> mapperEntries); 

}
