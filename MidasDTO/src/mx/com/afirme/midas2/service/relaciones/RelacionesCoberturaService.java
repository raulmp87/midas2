package mx.com.afirme.midas2.service.relaciones;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.catalogos.GrupoVariablesModificacionPrima;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;


public interface RelacionesCoberturaService {

	public List<GrupoVariablesModificacionPrima> obtenerGruposVariablesAsociados(BigDecimal idSeccion, BigDecimal idCobertura);
	
	public List<GrupoVariablesModificacionPrima> obtenerGruposVariablesDisponibles(BigDecimal idSeccion, BigDecimal idCobertura);
		
	public RespuestaGridRelacionDTO relacionarGruposVariables(String accion, BigDecimal idSeccion, BigDecimal idCobertura, Long idGrupoVariables);
	
	
}
