package mx.com.afirme.midas2.service.relaciones;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;


public interface RelacionesSeccionService {

	public List<Paquete> obtenerPaquetesAsociados(BigDecimal idSeccion);
	
	public List<Paquete> obtenerPaquetesDisponibles(BigDecimal idSeccion);
		
	public RespuestaGridRelacionDTO relacionarPaquetes(String accion, BigDecimal idSeccion, Long idPaquete);
	
}
