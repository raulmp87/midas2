package mx.com.afirme.midas2.service.relaciones;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;


public interface RelacionesProductoService {

	public List<FormaPagoDTO> obtenerFormasPagoAsociadas(BigDecimal idProducto);
	
	public List<FormaPagoDTO> obtenerFormasPagoDisponibles(BigDecimal idProducto);
		
	public RespuestaGridRelacionDTO relacionarFormasPago(String accion, BigDecimal idProducto, Integer idFormaPago);
	
	public List<MedioPagoDTO> obtenerMediosPagoAsociadas(BigDecimal idProducto);
	
	public List<MedioPagoDTO> obtenerMediosPagoDisponibles(BigDecimal idProducto);
		
	public RespuestaGridRelacionDTO relacionarMediosPago(String accion, BigDecimal idProducto, Integer idMedioPago);
	
	
}
