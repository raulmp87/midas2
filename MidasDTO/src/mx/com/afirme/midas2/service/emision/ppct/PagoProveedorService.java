package mx.com.afirme.midas2.service.emision.ppct;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.emision.ppct.CoberturaSeycos;
import mx.com.afirme.midas2.domain.emision.ppct.OrdenPago;
import mx.com.afirme.midas2.domain.emision.ppct.Proveedor;
import mx.com.afirme.midas2.domain.emision.ppct.Recibo;
import mx.com.afirme.midas2.domain.emision.ppct.ReciboProveedor;
import mx.com.afirme.midas2.dto.emision.ppct.SaldoDTO;
import mx.com.afirme.midas2.dto.emision.ppct.StatusSolicitudChequeDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;

@Local
public interface PagoProveedorService {

	
	OrdenPago obtenerPreeliminar (OrdenPago ordenPago, String sessionId);
	
	void desasignarRecibosPreeliminares(String sessionId);
	
	BigDecimal seleccionar (BigDecimal reciboId, BigDecimal importePreeliminar);
	
	OrdenPago guardarOrdenPago (OrdenPago ordenPago, String sessionId);
	
	OrdenPago cancelarOrdenPago (OrdenPago ordenPago);
	
	List<Proveedor> obtenerProveedores();
	
	List<CoberturaSeycos> obtenerCoberturasProveedor(Proveedor proveedor, String claveNegocio);
	
	List<StatusSolicitudChequeDTO> obtenerStatus();
	
	List<OrdenPago> obtenerOrdenesPago(OrdenPago filtro);
	
	List<Recibo> obtenerRecibosProveedor(ReciboProveedor filtro, Boolean historicos);
	
	OrdenPago obtenerOrdenPago(OrdenPago filtro);
	
	void guardarOrdenesPagoAutomaticas();
	
	TransporteImpresionDTO obtenerDetalleReporteSaldos(SaldoDTO filtro);
	
	TransporteImpresionDTO obtenerResumenReporteSaldos(SaldoDTO filtro);
	
	void initialize();
	
}
