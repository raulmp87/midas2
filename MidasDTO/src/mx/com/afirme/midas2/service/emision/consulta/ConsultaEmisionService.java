package mx.com.afirme.midas2.service.emision.consulta;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.dto.emision.consulta.Consulta;
import mx.com.afirme.midas2.dto.emision.consulta.ConsultaEmision;

@Local
public interface ConsultaEmisionService {
	
	public ConsultaEmision seleccionar(ConsultaEmision consultaEmision, String seccionStr);
	
	public List<Consulta> navegar(ConsultaEmision consultaEmision, String seccionStr, Integer posStart, Integer count, String orderBy, String direct);
}
