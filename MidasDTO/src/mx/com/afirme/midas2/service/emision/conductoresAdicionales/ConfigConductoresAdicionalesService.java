package mx.com.afirme.midas2.service.emision.conductoresAdicionales;

import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.emision.conductoresAdicionales.PolizaAutoSeycos;
/**
 * @author Adriana Flores
 *
 */
@Local
public interface ConfigConductoresAdicionalesService {
	/**
	 * Agregar conductores
	 * @param polizaAutoSeycos
	 */
	int guardarConductoresAdicionales(PolizaAutoSeycos polizaAutoSeycos);

	/**
	 * Obtener poliza
	 * @param idNegocio
	 */
	PolizaAutoSeycos buscarPoliza(Map<String,Object> filtros);
}
