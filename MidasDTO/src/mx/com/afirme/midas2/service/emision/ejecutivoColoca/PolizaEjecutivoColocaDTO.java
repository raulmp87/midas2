package mx.com.afirme.midas2.service.emision.ejecutivoColoca;

public class PolizaEjecutivoColocaDTO {
	private String idPoliza="";
	private String numeroPolizaMidas="";
	private String numeroPolizaSeycos="";
	private String numeroEndoso="";
	private String tipoMovto="";
	private String sucursal="";
	private String nombreSucursal="";
	private String producto="";
	private String ejecutivo="";
	private String claveNeg="";
	private String idcotizacion="";
	private String identificador="";
	private String esBancaSeguros="";


	public String getNumeroPolizaMidas() {
		return numeroPolizaMidas;
	}
	public void setNumeroPolizaMidas(String numeroPolizaMidas) {
		this.numeroPolizaMidas = numeroPolizaMidas;
	}
	public String getNumeroPolizaSeycos() {
		return numeroPolizaSeycos;
	}
	public void setNumeroPolizaSeycos(String numeroPolizaSeycos) {
		this.numeroPolizaSeycos = numeroPolizaSeycos;
	}
	public String getNumeroEndoso() {
		return numeroEndoso;
	}
	public void setNumeroEndoso(String numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}
	public String getTipoMovto() {
		return tipoMovto;
	}
	public void setTipoMovto(String tipoMovto) {
		this.tipoMovto = tipoMovto;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public String getNombreSucursal() {
		return nombreSucursal;
	}
	public void setNombreSucursal(String nombreSucursal) {
		this.nombreSucursal = nombreSucursal;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getEjecutivo() {
		return ejecutivo;
	}
	public void setEjecutivo(String ejecutivo) {
		this.ejecutivo = ejecutivo;
	}
	public void setIdPoliza(String idPoliza) {
		this.idPoliza = idPoliza;
	}
	public String getIdPoliza() {
		return idPoliza;
	}
	public void setClaveNeg(String claveNeg) {
		this.claveNeg = claveNeg;
	}
	public String getClaveNeg() {
		return claveNeg;
	}
	public void setIdcotizacion(String idcotizacion) {
		this.idcotizacion = idcotizacion;
	}
	public String getIdcotizacion() {
		return idcotizacion;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public String getIdentificador() {
		return identificador;
	}
	public String getEsBancaSeguros() {
		return esBancaSeguros;
	}
	public void setEsBancaSeguros(String esBancaSeguros) {
		this.esBancaSeguros = esBancaSeguros;
	}
	 
}
