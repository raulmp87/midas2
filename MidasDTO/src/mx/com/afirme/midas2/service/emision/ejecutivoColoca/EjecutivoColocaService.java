package mx.com.afirme.midas2.service.emision.ejecutivoColoca;

import java.util.List;

import javax.ejb.Local;

import com.afirme.eibs.services.EibsUserService.UserEibsVO;

@Local
public interface EjecutivoColocaService {

	public List<PolizaEjecutivoColocaDTO> getPolizasEjecutivoColoca(String polizaMidas, String polizaSeycos, 
			String endoso, String fechaInicio, String fechaFinal, String sucursal, String ramo);
	
	public PolizaEjecutivoColocaDTO getPolizaEjecutivoColoca (String polizaMidas, String polizaSeycos, 
			String endoso, String fechaInicio, String fechaFinal, String sucursal, String ramo);
	
	public boolean savePolizaEjecutivoColoca (String numPoliza, String idCotizacion, String idPoliza,String claveNeg, String ejecutivo, String endoso, String identificador);
	
	public boolean getRolUser(String userName);
	
	public List<UserEibsVO> getEjecutivoColocaList(String userName);
}