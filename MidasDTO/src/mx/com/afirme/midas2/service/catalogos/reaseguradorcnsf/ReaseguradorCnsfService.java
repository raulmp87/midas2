package mx.com.afirme.midas2.service.catalogos.reaseguradorcnsf;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.catalogos.calificacionesreas.CalificacionAgenciaDTO;
import mx.com.afirme.midas2.domain.catalogos.reaseguradorcnsf.ReaseguradorCnsfMov;

public interface ReaseguradorCnsfService 
{
	
	public List<ReaseguradorCnsfMov> findByFilters(ReaseguradorCnsfMov filtro);
		
	public Map<BigDecimal, String> getMapAgencias();
	
	public List<CalificacionAgenciaDTO> getListCalificaciones(BigDecimal idAgencia);
	
	public Map<BigDecimal, String> getMapCalificacionesAll();
	
	public Map<BigDecimal, String>  getMapCalificaciones(BigDecimal idAgencia);
	
	public void deleteReaseguradorMovs(BigDecimal idAgencia);
	
	public void saveReaseguradorMovs(ReaseguradorCnsfMov reaseguradorCnsfMov);
	
	public byte[] toArrayByte( List<ReaseguradorCnsfMov> list) throws IOException;
	
	public byte[] encabezadoDetalle() throws IOException;
	
	public Date getFechaUltimoCorteProcesado();
}
