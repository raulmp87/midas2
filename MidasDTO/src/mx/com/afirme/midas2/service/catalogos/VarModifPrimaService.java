package mx.com.afirme.midas2.service.catalogos;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.catalogos.VarModifPrima;
import mx.com.afirme.midas2.dto.wrapper.VariableModifPrimaComboDTO;
import mx.com.afirme.midas2.service.CatalogoGenericoService;


public interface VarModifPrimaService extends CatalogoGenericoService{

	
	public VarModifPrima save(VarModifPrima tcVarModifPrima);
    public void remove(VarModifPrima grupoVariablesModificacionPrima);
    public VarModifPrima findById(Long id);
    public List<VarModifPrima> findAll();
    public List<VarModifPrima> findByFilters(VarModifPrima filtroTcVarModifPrima);
	public VarModifPrima findByClave(Long clave);
	
	public List<VariableModifPrimaComboDTO> getListaVarModifPrimaComboDTO(Long idGrupo);
}
