package mx.com.afirme.midas2.service.catalogos;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.catalogos.Paquete;

public interface PaqueteService{
    public List<Paquete> findByFilters(Paquete filtroPaquete);
    public List<Paquete> listAll();
    public Paquete prepareVerDetalle(Long id);
    public boolean guardar(Paquete paquete);
}
