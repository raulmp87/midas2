package mx.com.afirme.midas2.service.catalogos;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao.TipoOperacionHistorial;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ActualizacionAgente;

/**
 * Servicio para aquellas entidades que requieran guardar en un historial todos los cambios realizados a cada entidad.
 * @author vmhersil
 *
 */
@Local
public interface EntidadHistoricoService{
	
	/**
	 * Guarda en el historial el evento o cambio de un registro de acuerdo al tipo de operacion, especificando el usuario que realiza el cambio
	 * @param operationType
	 * @param idRecord
	 * @param comments
	 * @param userName Usuario que ejecuta el cambio
	 * @return
	 */
	public ActualizacionAgente saveHistory(TipoOperacionHistorial tipoOperacion,Long idRegistro, String comentarios, String usuario,String tipoMovimiento);
	/**
	 * Obtiene el historial de  un registro
	 * @param operationType
	 * @param idRecord
	 * @return
	 * @throws SystemException
	 */
	public List<ActualizacionAgente> getHistory(TipoOperacionHistorial operationType,Long idRecord)throws SystemException;
	/**
	 * Obtiene el ultimo cambio.
	 * @param operationType
	 * @param idRecord
	 * @return
	 * @throws SystemException
	 */
	public ActualizacionAgente getLastUpdate(TipoOperacionHistorial operationType,Long idRecord)throws SystemException;
	
	public ActualizacionAgente getHistoryRecord(TipoOperacionHistorial operationType,Long idRecord, String historyDate)throws SystemException;
	
	public <E extends Entidad>List<E> findByProperties(Class<E> entityClass,Map<String,Object> params);
	
	public <E extends Entidad> List<E> findByPropertiesWithOrder(Class<E> entityClass,Map<String, Object> params,String...orderByAttributes);	
}
