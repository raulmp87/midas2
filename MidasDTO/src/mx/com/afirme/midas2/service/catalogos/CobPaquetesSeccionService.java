package mx.com.afirme.midas2.service.catalogos;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas2.domain.catalogos.CobPaquetesSeccion;
import mx.com.afirme.midas2.dto.RelacionesCobPaquetesSeccionDTO;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;


public interface CobPaquetesSeccionService {
	
	public List<SeccionDTO> getListarSeccionesVigentesAutos();
	
    public 	RelacionesCobPaquetesSeccionDTO getRelationLists(CobPaquetesSeccion cobPaquetesSeccion);
    
    public RespuestaGridRelacionDTO relacionarCoberturaSeccion(String accion, CobPaquetesSeccion cobPaquetesSeccion);
}
