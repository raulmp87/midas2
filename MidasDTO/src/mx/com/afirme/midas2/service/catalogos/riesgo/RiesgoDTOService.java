package mx.com.afirme.midas2.service.catalogos.riesgo;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDTO;


public interface RiesgoDTOService {
	public List<RiesgoDTO> getListarFiltrado(RiesgoDTO riesgoDTO,Boolean mostrarInactivos);
	public List<RiesgoDTO> listarVigentesAutos();
}
