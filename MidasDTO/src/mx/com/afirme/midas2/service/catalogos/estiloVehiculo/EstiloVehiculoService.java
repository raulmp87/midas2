package mx.com.afirme.midas2.service.catalogos.estiloVehiculo;

import javax.ejb.Remote;


public interface EstiloVehiculoService {
	/**
	 * Retorna la descripcion de un estilo (AMIS)
	 * @param claveEstilo (AMIS)
	 * @return String
	 * @author martin
	 */
	public String getDescripcionPorClaveAmis(String claveEstilo);
}
