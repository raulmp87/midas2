package mx.com.afirme.midas2.service.catalogos.cobertura;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;


public interface CoberturaDTOService {
	public List<CoberturaDTO> getListarFiltrado(CoberturaDTO coberturaDTO,Boolean mostrarInactivos);
	public List<CoberturaDTO> listarVigentesAutos();
}
