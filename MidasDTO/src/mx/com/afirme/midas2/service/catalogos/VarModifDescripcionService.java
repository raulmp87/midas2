package mx.com.afirme.midas2.service.catalogos;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas2.domain.catalogos.VarModifDescripcion;
import mx.com.afirme.midas2.dto.wrapper.CatalogoValorFijoComboDTO;

public interface VarModifDescripcionService {

	
	public VarModifDescripcion save(VarModifDescripcion varModifDescripcion);
    public void remove(VarModifDescripcion varModifDescripcion);
    public VarModifDescripcion findById(Long id);
    public List<VarModifDescripcion> findAll();
    public List<VarModifDescripcion> findByFilters(VarModifDescripcion filtroVarModifDescripcion);
	public List<CatalogoValorFijoDTO> listarCombo();
	public List<CatalogoValorFijoComboDTO> listarComboGrupos();
	public List<VarModifDescripcion> findByGroup(Integer group);
}
