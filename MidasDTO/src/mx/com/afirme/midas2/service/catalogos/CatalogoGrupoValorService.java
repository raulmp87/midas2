package mx.com.afirme.midas2.service.catalogos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;

@Local
public interface CatalogoGrupoValorService {

	public List<CatValorFijo> obtenerCatalogoValores(TIPO_CATALOGO tipo, String codigoPadreId);
	
	public List<CatValorFijo> obtenerCatalogoValores(TIPO_CATALOGO tipo, String codigoPadreId, boolean orderByDescription);
	
	public CatGrupoFijo obtenerGrupo(TIPO_CATALOGO tipo);
	
	public CatValorFijo obtenerValorPorCodigo(TIPO_CATALOGO tipo, String codigo);
	
	public String obtenerDescripcionValorPorCodigo(TIPO_CATALOGO tipo, String codigo);
	
}
