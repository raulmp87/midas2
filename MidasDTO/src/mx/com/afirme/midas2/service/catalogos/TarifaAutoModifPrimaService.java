package mx.com.afirme.midas2.service.catalogos;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.tarifa.TarifaAutoModifPrima;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoModifPrimaId;
import mx.com.afirme.midas2.service.tarifa.TarifaService;


public interface TarifaAutoModifPrimaService extends TarifaService<TarifaAutoModifPrimaId,TarifaAutoModifPrima>{
	public List<TarifaAutoModifPrima> findByFilters(TarifaAutoModifPrima filtroTarifaAutoModifPrima);
}
