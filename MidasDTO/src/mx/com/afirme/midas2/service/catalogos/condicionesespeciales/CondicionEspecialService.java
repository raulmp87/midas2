package mx.com.afirme.midas2.service.catalogos.condicionesespeciales;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.condicionesespeciales.AreaImpacto;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas2.domain.condicionesespeciales.ArchivoAdjuntoCondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.CoberturaCondicionEsp;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial.EstatusCondicion;
import mx.com.afirme.midas2.domain.condicionesespeciales.Factor;
import mx.com.afirme.midas2.domain.condicionesespeciales.FactorAreaCondEsp;
import mx.com.afirme.midas2.domain.condicionesespeciales.ValorFactor;
import mx.com.afirme.midas2.domain.condicionesespeciales.ValorVarAjusteCondicionEsp;
import mx.com.afirme.midas2.domain.condicionesespeciales.ValorVariableAjuste;
import mx.com.afirme.midas2.domain.condicionesespeciales.VarAjusteCondicionEsp;
import mx.com.afirme.midas2.domain.condicionesespeciales.VariableAjuste;
import mx.com.afirme.midas2.dto.CondicionEspecialDTO;
import mx.com.afirme.midas2.dto.condicionespecial.AreaCondicionDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.condicionespecial.CoberturasCondicionDTO;
import mx.com.afirme.midas2.dto.condicionespecial.FactorCondicionDTO;
import mx.com.afirme.midas2.dto.condicionespecial.VarAjusteCondicionDTO;

@Local
public interface CondicionEspecialService {

	public List<CondicionEspecial> obtenerCondiciones(CondicionEspecialDTO condicionFiltro) ;
	
	public List<CondicionEspecial> obtenerCondicionesNombreCodigo(String param) ;
	
	public CondicionEspecial obtenerCondicion(Long idCondicionEspecial);
	
	public CondicionEspecial obtenerCondicionCodigo(Long codigo);
	
	public boolean modificarEstatus(Long idCondicionEspecial, Short estatus);
	
	public List<EstatusCondicion> obtenerEstatusDisponible(Long idCondicionEspecial);
	
	public CondicionEspecial guardarCondicionEnProceso(CondicionEspecial condicion);
	
	public void guardarCondicionActivacion(CondicionEspecial condicion);
	
	public CoberturaCondicionEsp obtenerCoberturaCondicion(Long idCoberturaCondEsp);
	
	public List<CoberturasCondicionDTO> getLstCoberturasAsociadas(Long idCondicionEspecial);
	
	public CoberturasCondicionDTO getCoberturaCondicionAsociada(Long idCondicionEspecial, Long idSeccion);
	
	public List<CoberturasCondicionDTO> getLstCoberturasDisponibles(Long idCondicionEspecial, Long idSeccion);
	
	public void asociarCoberturaCondicion(BigDecimal idSeccion, BigDecimal idCobertura, Long idCondicionEspecial);

	public void removerCoberturaCondicion(BigDecimal idSeccion, BigDecimal idCobertura, Long idCondicionEspecial);
	
	public void asociarSeccionCondicion(BigDecimal idSeccion, Long idCondicionEspecial);

	public void removerSeccionCondicion(BigDecimal idSeccion, Long idCondicionEspecial);	
	
	public List<CondicionEspecial>  obtenerCondicionEspecialCodNombre(String token);
	
	public List<CondicionEspecial> obtenerCondicionEspecialPorLinea( BigDecimal idSeccion );
	
	public void guardarArchivoAdjunto(ControlArchivoDTO control, Long idCondicionEspecial, String usuario);
	
	public List<ArchivoAdjuntoCondicionEspecial> listarArchivosAdjuntos(Long idCondicionEspecial);
	
	public void eliminarArchivoAdjunto(BigDecimal idToControlArchivo);
	
	public VariableAjuste obtenerVariableAjuste(Long id);
	
	public VarAjusteCondicionEsp obtenerVariableAjusteCondicion(Long idVariableAjuste, Long idCondicionEspecial);
	
	public List<ValorVariableAjuste> obtenerValoresVariable(Long idVariableAjuste);
	
	public List<ValorVarAjusteCondicionEsp> obtenerValoresVariableCondicion(Long idVariableAjusteCond);
	
	public List<VarAjusteCondicionDTO> getLstVariablesAsociadas(Long idCondicionEspecial);
	
	public List<VarAjusteCondicionDTO> getLstVariablesDisponibles(Long idCondicionEspecial);
	
	public void guardarVariableAjuste(Long idVariableAjuste, Long idCondicionEspecial);
	
	public void eliminarVariableAjuste(Long idVariableAjuste, Long idCondicionEspecial);
	
	public void guardarValorVariable(Long idCondicionEspecial, Long idVariableAjuste, Long idValorVariableAjuste, String valor);
	
	public Long obtenerIdVariablePorValor(Long idValorVariableAjuste);
	
	public List<VarAjusteCondicionDTO> obtenerVariablePorNombre(Long idCondicionEspecial, String nombreVariable);
	
	public void asociarVariablesAjuste(Long idCondicionEspecial, String nombreVariable);
	
	public void eliminarVariablesAjuste(Long idCondicionEspecial);
	
	public List<AreaImpacto> getCatalogoAreas();
	
	public List<Factor> getCatalogoFactor();
	
	public Factor obtenerFactor(Long idFactor);
	
	public List<ValorFactor> obtenerValoresFactor(Long idFactor);
	
	public List<AreaCondicionDTO> obtenerAreaCondicion(Long idCondicionEspecial);
	
	public List<FactorCondicionDTO> obtenerFactorCondicion(Long idCondicionEspecial, Long idAreaImpacto);
	
	public FactorAreaCondEsp obtenerFactorCondicion(Long idCondicionEspecial, Long idAreaImpacto, Long idFactor);
	
	public void guardarAreaImpacto(Long idCondicionEspecial, Long idAreaImpacto);
	
	public void eliminarAreaImpacto(Long idCondicionEspecial, Long idAreaImpacto);
	
	public void guardarFactor(Long idCondicionEspecial, Long idAreaImpacto, Long idFactor);
	
	public void eliminarFactor(Long idCondicionEspecial, Long idAreaImpacto, Long idFactor);
	
	public void guardarValorFactor(String valor, Long idCondicionEspecial, Long idAreaImpacto, Long idFactor);

	public TransporteImpresionDTO imprimirCondicionEspecial( Long idCondicionEspecial );
	
	public List<CoberturaCondicionEsp> getLstCobCondEspAsociadas(Long idCondicionEspecial);
	
	public void guardarNivelImpacto(Long idCondicionEspecial, Short nivelImpacto);
	
	public void guardarVIP(Long idCondicionEspecial, Boolean esVIP);
	
	public List<SeccionDTO> obtenerNegociosLigados( Long idCondicionEspecial , Boolean readOnly );
	
	public CondicionEspecial copiarCondicionEspecial(Long condicionEspecial, String nombre);
}
