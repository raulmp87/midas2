package mx.com.afirme.midas2.service.catalogos;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.catalogos.ColorVehiculo;

public interface ColorVehiculoService extends EntidadService{

    public List<ColorVehiculo> findByFilters(ColorVehiculo filtroColorVehiculo);

    public ColorVehiculo findByClave(Integer clave);
}
