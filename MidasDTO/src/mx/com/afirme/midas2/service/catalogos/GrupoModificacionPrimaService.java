package mx.com.afirme.midas2.service.catalogos;

import java.util.List;

import javax.ejb.Remote;
import mx.com.afirme.midas2.domain.catalogos.GrupoVariablesModificacionPrima;

public interface GrupoModificacionPrimaService {

	
	public GrupoVariablesModificacionPrima save(GrupoVariablesModificacionPrima grupoVariablesModificacionPrima);
    public void remove(GrupoVariablesModificacionPrima grupoVariablesModificacionPrima);
    public GrupoVariablesModificacionPrima findById(Long id);
    public List<GrupoVariablesModificacionPrima> findAll();
    public List<GrupoVariablesModificacionPrima> findByFilters(GrupoVariablesModificacionPrima filtrogrupoVariablesModificacionPrima);
	public GrupoVariablesModificacionPrima findByClave(Integer clave);
	
}
