package mx.com.afirme.midas2.service.catalogos.banco;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorDTO;
import mx.com.afirme.midas2.domain.catalogos.banco.BancoMidas;
import mx.com.afirme.midas2.domain.catalogos.banco.ConfigCuentaMidas;
import mx.com.afirme.midas2.domain.catalogos.banco.CuentaBancoMidas;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasiva.DetalleCargaMasivaAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasivaindividual.DetalleCargaMasivaIndAutoCot;

@Local
public interface BancoMidasService {

	public List<BancoMidas> obtenerBancos();
	
	public List<CuentaBancoMidas> obtenerCuentas();
	
	public List<CuentaBancoMidas> obtenerCuentas(Long bancoId);
	
	public List<ConfigCuentaMidas> obtenerConfiguracionCuentas(Long id);
	
	public ConfigCuentaMidas obtenerConfiguracionCuenta(Long id);
	
	public List<DetalleCargaMasivaAutoCot> encriptarCuentasBancariasCargaMasiva(List<DetalleCargaMasivaAutoCot> detalleCargaMasivaList);
	
	public DetalleCargaMasivaIndAutoCot encriptarCuentasBancariasCargaMasivaInd(DetalleCargaMasivaIndAutoCot detalleCargaMasivaInd);
	
	public String encriptaInformacionBancaria(String valorDesencriptado);

	public String desencriptaInformacionBancaria(String valorDesencriptado);
	
	public List<BancoEmisorDTO> bancosSeycos();
}
