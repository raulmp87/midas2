package mx.com.afirme.midas2.service;

import java.io.File;
import java.io.InputStream;

import javax.ejb.Local;

@Local
public interface PersonaService {
	
	boolean guardarFoto(Long idPersona, File foto);
	InputStream obtenerFoto(Long idPersona);
}
