package mx.com.afirme.midas2.service.cliente.grupo;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.cliente.CalifGrupoCliente;


public interface CalifGrupoClienteService {
	
	public List<CalifGrupoCliente> buscarTodos();

}
