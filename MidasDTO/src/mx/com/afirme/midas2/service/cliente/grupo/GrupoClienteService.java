package mx.com.afirme.midas2.service.cliente.grupo;

import java.util.List;

import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.cliente.GrupoCliente;


public interface GrupoClienteService {
	
	/**
	 * Consulta los registros "GrupoCliente" 
	 * cuya descripción sea similar a la recibida.
	 * @param descripcion
	 * @return
	 */
	public List<GrupoCliente> listarPorDescripcion(String descripcion,String clave);

	public GrupoCliente buscarPorId(Long id);
	
	public GrupoCliente buscarPorClave(String clave);
	
	public GrupoCliente guardar(GrupoCliente grupoCliente);
	
	/**
	 * Obtiene todos los grupos a los que pertenece un cliente.
	 * @param idCliente
	 * @return
	 */
	public List<GrupoCliente> buscarPorCliente(Long idCliente);
	
	/**
	 * Checa si la entidad está duplicada.
	 * @param grupoCliente
	 * @return true si está duplicada.
	 */
	public boolean isDuplicado(GrupoCliente grupoCliente);
	
	public List<GrupoCliente> buscarTodos();
	
	public void eliminar(GrupoCliente grupoCliente);
	
}
