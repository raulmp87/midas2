package mx.com.afirme.midas2.service.folioReexpedibleService;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.folioReexpedible.FiltroFolioReexpedible;
import mx.com.afirme.midas2.domain.folioReexpedible.TcTipoFolio;
import mx.com.afirme.midas2.domain.folioReexpedible.ToFolioReexpedible;

/**
 * Clase que contiene toda la l\u00f3gica de los servicios que ser\u00e1n
 * consumidos por los actios del modulo de emisi\u00f3n de folios reexpedibles.
 * 
 * @since 11022016
 * 
 * @author AFIRME
 *
 */
@Local
public interface FolioReexpedibleService {

	/**
	 * M\u00e9todo que da de alta un objetoFolio dentro de la base de datos.
	 * 
	 * @param folioReexpedible
	 *            endidad a persistir.
	 *            
	 * @param numeroFolios N\u00famero de folios a generar.
	 * 
	 * @return booleano que determina si se pudo realizar la persistencia de
	 *         manera correcta.
	 */
	public boolean guardarFolioReexpedible(ToFolioReexpedible to, String numeroFolios);

	/**
	 * M\u00e9todo que consulta todos los folios reexpedibles que se encuentran
	 * en estado activo.
	 * 
	 * @return Lista de folios activos o habiles.
	 */
	public List<FiltroFolioReexpedible> consultarFoliosReexpedibles();

	/**
	 * M\u00e9todo que realiza la actualizaci\u00f3n de la informaci\u00f3n que
	 * hay dentro de un entity.
	 * 
	 * @param folioReexpedible
	 *            entidad que contiene la informaci\u00f3n a actualizarse.
	 * 
	 * @return boolean que determina si la actualizaci\u00f3n se reali\u00f3
	 *         correcta mente.
	 */
	public boolean actualizarFolioReexpedble(ToFolioReexpedible folioReexpedible);

	/**
	 * M\u00e9todo que realiza la baja del folioReexpedible.
	 * 
	 * @param folioReexpedible
	 *            a eliminar.
	 * 
	 * @return boolean que determina si se elimin\u00f3 de manera correcta.
	 */
	public boolean eliminarFolioReexpedible(ToFolioReexpedible folioReexpedible);

	/**
	 * M\u00e9todo que realiza la consulta de los tipos de folio dados de alta
	 * dentro de la base de datos.
	 * 
	 * @return Lista de tipos de folios.
	 */
	public List<TcTipoFolio> consultarTiposDeFolios();

	/**
	 * M\u00e9todo que realiza la consulta de los folios a
	 * 		apartir de un objeto tipo filtro.
	 * 
	 * @param folioFiltro Filtro que contiene la informaci\u00f3n para
	 * 			consultar dentro de la base de datos.
	 * 
	 * @param usaFiltro Boolean que determina si se usó un filtro para buscar los folios.
	 * 
	 * @return Lista de Folios Consultada.
	 */
	public List<FiltroFolioReexpedible> consultarFolios(FiltroFolioReexpedible folioFiltro, boolean usaFiltro);
	
	/**
	 * M\u00e9todo que realiza la vinculaci\u00f3n de
	 * los folios con un agente, sobre escribiendo el agente facultativo, así como realizar
	 * las validaciones correspondientes de los folios.
	 * 
	 * @param filtro Objeto que contiene el folio inicio y folio fin.
	 * 
	 * @return cadena que contiene los mensajes de error y de exito
	 */
	public String vincularFolios(FiltroFolioReexpedible filtro);
	
	/**
	 * M\u00e9todo que consulta los folios reexpedibles deacuerdo a un filtro especifico.
	 * 
	 * @param folioFiltro Objeto que contiene los elementos del filtro.
	 * 
	 * @return Lista de Folios reexpedibles obtenidos.
	 */
	public List<ToFolioReexpedible> findByProperties(FiltroFolioReexpedible folioFiltro);
	
	/**
	 * M\u00e9todo que obtiene el folio de la tejeta que va a realizar la venta.
	 * 
	 * @param filtro filtro mediante elcual se obtendr\u00e1 el folio.
	 * 
	 * @param mensaje String que contend\u00e1 los mensajes en caso de error.
	 * 
	 * @param to Objetoque será llenado por referencia.
	 */
	public void obtenerFolioVenta(FiltroFolioReexpedible filtro, StringBuilder mensaje, ToFolioReexpedible to);
	
	/**
	 * M\u00e9todo que obtiene el folio de la tarjeta de la cual se va a realizar la venta.
	 * 
	 * @param numeroFolio numero de folio con el que se dió de alta.
	 * 
	 * @return booleano que determina si existe o no.
	 */
	public boolean isFolioVenta(String numeroFolio);
	
	/**
	 * M\u00e9todo que realiza la consulta del n\u00famero de registros que va a devolber la consulta.
	 * 
	 * @param filtro Objeto que contiene los filtros de la consulta.
	 * 
	 * @param usaFiltro Bandera que determina el uso de filtro.
	 * 
	 * @return Cantidad de registros que devolver\u00e1 la consulta.
	 * 
	 */
	public Long contarRegistrosFolios(FiltroFolioReexpedible filtro, Boolean usaFiltro);
}
