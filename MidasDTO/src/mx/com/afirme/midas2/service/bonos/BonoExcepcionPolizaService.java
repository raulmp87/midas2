package mx.com.afirme.midas2.service.bonos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.bonos.BonoExcepcionPoliza;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
/**
 * 
 * @author sams
 *
 */

@Local
public interface BonoExcepcionPolizaService {
	public List<BonoExcepcionPoliza> loadByConfigBono(ConfigBonos configBono) throws Exception;
}
