package mx.com.afirme.midas2.service.bonos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.bonos.ConfigBonoExcepNegocio;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;

@Local
public interface ConfigBonoExcepNegocioService{
	public List<ConfigBonoExcepNegocio> loadByConfigBono(ConfigBonos configBono) throws Exception;
}
