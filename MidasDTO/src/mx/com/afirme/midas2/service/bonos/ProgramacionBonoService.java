package mx.com.afirme.midas2.service.bonos;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.bonos.ProgramacionBono;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.dto.fuerzaventa.CalculoBonoEjecucionesView;
import mx.com.afirme.midas2.dto.fuerzaventa.GenericaAgentesView;
import mx.com.afirme.midas2.util.MidasException;

@Local
public interface ProgramacionBonoService {

	public List<ProgramacionBono> getProgramacionBonos();
	
	public List<GenericaAgentesView> getNegocioEspeciales(Long idTipoBono) throws Exception;
	
	public List<GenericaAgentesView> getConfiguracionBonos(Long idTipoBono) throws Exception;
	
	public void saveProgramacionBono(ProgramacionBono programacionBono, List<Negocio> negociosEspecialesList, List<ConfigBonos> configuracionBonoList,Usuario usuario, Boolean ejecutar) throws MidasException;
	
	public void activaProgramacionBono(String accion, ProgramacionBono programacionBono);
	
	public Map<String, Integer> obtenerProgramados(Long idTipoBono);
	
	public ValorCatalogoAgentes getTipoBonoNegocioEspecial();
	
	public void calcularBono(List<Negocio> negociosEspecialesList, List<ConfigBonos> configuracionBonoList);
	
	public void ejecCalcBono(ProgramacionBono programacionBono, List<Negocio> negociosEspecialesList, List<ConfigBonos> configuracionBonoList,
			Usuario usuario, Boolean isManual) throws MidasException;
	/**
	 * Carga la programacion del bono por id
	 * @param id
	 * @return
	 * @throws MidasException
	 */
	public ProgramacionBono loadById(Long id) throws MidasException;
	/**
	 * Carga la programacion del bono por id
	 * @param id
	 * @return
	 * @throws MidasException
	 */
	public ProgramacionBono loadById(ProgramacionBono programacion) throws MidasException;
	
	public void recalcularBono(Long idCalculo) throws MidasException;
	
	public String eliminarBonosCero(String ids) throws MidasException;
	
	public  List<ConfigBonos> bonoIsVigente(List<ConfigBonos> configuracionBonoList) throws MidasException;
	
	public  List<ConfigBonos> excluirBonoNoVigentes(List<ConfigBonos> listaBonosAExcluir, List<ConfigBonos> configuracionBonoList) throws MidasException;
	
	public List<CalculoBonoEjecucionesView> listaCalculoBonosMonitorEjecucion() throws MidasException;
}
