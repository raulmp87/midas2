package mx.com.afirme.midas2.service.bonos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.bonos.BonoExclusionTipoCedula;
import mx.com.afirme.midas2.util.MidasException;

@Local
public interface BonoExclusionTipoCedulaService {

	/**
	 * 
	 * @param id
	 * @return
	 * @throws MidasException
	 */
	public List<BonoExclusionTipoCedula> findById(Long id) throws MidasException;
	
}
