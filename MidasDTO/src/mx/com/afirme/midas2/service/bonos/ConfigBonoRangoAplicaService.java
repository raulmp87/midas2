package mx.com.afirme.midas2.service.bonos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.bonos.ConfigBonoRangoAplica;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;

@Local
public interface ConfigBonoRangoAplicaService {
	public ConfigBonos saveConfigBonoExcepAgente(ConfigBonos config,List<ConfigBonoRangoAplica> configBonoRangoAplica) throws Exception;
	public List<ConfigBonoRangoAplica> loadByConfigBono(ConfigBonos configBono) throws Exception;
	public void  validarRangofactores(List<ConfigBonoRangoAplica> configBonoRangoAplica) throws Exception;
}
