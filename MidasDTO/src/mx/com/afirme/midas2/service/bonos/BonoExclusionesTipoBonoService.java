package mx.com.afirme.midas2.service.bonos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.bonos.BonoExclusionesTipoBono;
import mx.com.afirme.midas2.util.MidasException;
/**
 * 
 * @author sams
 *
 */
@Local
public interface BonoExclusionesTipoBonoService {

	/**
	 * metodo que regresa una lista de
	 * @param idBono
	 * @return
	 * @throws MidasException
	 */
	public List<BonoExclusionesTipoBono> loadByConfiguration(Long idBono) throws MidasException;
}
