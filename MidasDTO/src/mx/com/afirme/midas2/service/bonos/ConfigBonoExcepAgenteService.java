package mx.com.afirme.midas2.service.bonos;

import java.util.List;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.bonos.ConfigBonoExcepAgente;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;

@Local
public interface ConfigBonoExcepAgenteService{
	public List<ConfigBonoExcepAgente> loadByConfigBono(ConfigBonos configBono) throws Exception;
}
