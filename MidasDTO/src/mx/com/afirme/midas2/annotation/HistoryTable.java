package mx.com.afirme.midas2.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author aavacor
 * <p>Anotacion para definir de manera customizada el nombre de la tabla historica que llevara el HistoryPolicy cuando aplique</p>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({java.lang.annotation.ElementType.TYPE, java.lang.annotation.ElementType.METHOD})
public @interface HistoryTable {

	public String name() default "";
	
	public String schema() default "";
	
}
