package mx.com.afirme.midas2.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @author Lizeth De La Garza
 *
 *<p>Anotación para la exportación a Excel de un listado</p>
 *<p>Se implementa a nivel método. El método no debe contener parámetros y que por lo regular sean métodos get.</p>
 *<p>El atributo <b>columnName</b> es obligatorio, contiene el nombre de la columna en el Excel.</p>
 *<p>El atributo <b>columnOrder</b> es obligatorio, determina el orden que tendrá la columna, empieza en 0.</p>
 *<p>El atributo <b>format</b> es opcional para dar formato a campos tipo número o fecha.</p>
 *<p>El atributo <b>document</b> es opcional, sirve cuando la misma clase sirve para diferentes exportaciones, con este atributo le decimos
 *   a que Documentos(exportaciones) aplican, se pueden separar por comas si es mas de una.</p>
 *<p>El atributo <b>whenValueNull</b> es opcional, permite poner un valor por default cuando el valor es nulo.</p>
 *<p>El atributo <b>fixedValues</b> es opcional, permite interpretar un valor con su correspondiente en casos de catalogos. Ejemplo 1=SI,0=NO;
 *   el valor debe ir despues del signo = y si hay más de un valor deben ser separados por comas</p>
 * <p>El atributo <b>summarize</b> es opcional, permite sumar el total de la columna (solo numéricos), este agrega la fórmula después de la última línea de resultados.</p>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ java.lang.annotation.ElementType.METHOD})
public @interface Exportable {
	
	public String columnName();
	
	public int columnOrder();
	
	public String format() default "";
	
	public String document() default "DEFAULT";
	
	public String whenValueNull() default "";
	
	public String fixedValues() default"";
	
	public boolean summarize() default false;
	
	public boolean catalog() default false;

}
