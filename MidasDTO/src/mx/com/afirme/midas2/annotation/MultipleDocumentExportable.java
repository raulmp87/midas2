package mx.com.afirme.midas2.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author aavacor
 * <p>Anotacion para agrupar descriptores de exportación de multiples documentos, respetando las caracteristicas propias para cada documento</p>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({java.lang.annotation.ElementType.METHOD})
public @interface MultipleDocumentExportable {

	Exportable[] value();
	
}
