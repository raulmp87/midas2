package mx.com.afirme.midas2.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface FilterPersistenceAnnotation {
	
	public static enum OperationType{
		EQUALS,
		GREATERTHAN,
		GREATERTHANEQUAL,
		LESSTHAN,
		LESSTHANEQUAL,
		LIKE
	};

	/**
	 * Nombre del atributo en la persistencia contra el que se hará match
	 * @return
	 */
	public String persistenceName() default "";

	/**
	 * Excluir el campo del filtrado
	 * @return
	 */
	public boolean excluded() default false;
	
	/**
	 * Tipo de comparación a realizar.
	 */
	public OperationType operation() default mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation.OperationType.EQUALS;
	
	
	/**
	 * Si el atributo es de tipo Date, marcar si debe truncarse al hacer la comparacion
	 * @return
	 */
	public boolean truncateDate() default false;
	
	/**
	 * Se utiliza para mapear rangos. Sirve para definir el nombre del parametro que contiene los valores en el mapa que se pasa que entity manager 
	 * @return
	 */
	public String paramKey() default "";
	
}

