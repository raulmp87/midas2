package mx.com.afirme.midas2.dto;

import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;

public class CondicionesEspIncisosDTO {
	
	CondicionEspecial condicionEspecial;
	String incisos;
	
	public CondicionEspecial getCondicionEspecial() {
		return condicionEspecial;
	}
	public void setCondicionEspecial(CondicionEspecial condicionEspecial) {
		this.condicionEspecial = condicionEspecial;
	}
	public String getIncisos() {
		return incisos;
	}
	public void setIncisos(String incisos) {
		this.incisos = incisos;
	}
	
	

}
