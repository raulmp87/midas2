package mx.com.afirme.midas2.dto.calculos;
/**
 * DTO para obtener las solicitudes de cheque
 * @author vmhersil
 *
 */
public class DetalleSolicitudCheque {
	private Long idSolicitudCheque;
	private String rubro;
	private Long idCuentaContable;
	private String concepto;
	private Double importe;
	private String cuentaContable;
	public Long getIdSolicitudCheque() {
		return idSolicitudCheque;
	}
	public void setIdSolicitudCheque(Long idSolicitudCheque) {
		this.idSolicitudCheque = idSolicitudCheque;
	}
	public String getRubro() {
		return rubro;
	}
	public void setRubro(String rubro) {
		this.rubro = rubro;
	}
	public Long getIdCuentaContable() {
		return idCuentaContable;
	}
	public void setIdCuentaContable(Long idCuentaContable) {
		this.idCuentaContable = idCuentaContable;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public Double getImporte() {
		return importe;
	}
	public void setImporte(Double importe) {
		this.importe = importe;
	}
	public String getCuentaContable() {
		return cuentaContable;
	}
	public void setCuentaContable(String cuentaContable) {
		this.cuentaContable = cuentaContable;
	}
}
