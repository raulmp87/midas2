package mx.com.afirme.midas2.dto.calculos;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class DetalleCalculoComisionesView implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7700646363624173985L;
	private Long id;
	private Long idGerencia;
	private String nombreGerencia;
	private Long idPromotoria;
	private String promotoria;
	private Long idAgente;
	private String nombreAgente;
	private Double importeComision;
	private String banco;
	private String numCta;

	public DetalleCalculoComisionesView() {}
	@Id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdGerencia() {
		return idGerencia;
	}

	public void setIdGerencia(Long idGerencia) {
		this.idGerencia = idGerencia;
	}

	public String getNombreGerencia() {
		return nombreGerencia;
	}

	public void setNombreGerencia(String nombreGerencia) {
		this.nombreGerencia = nombreGerencia;
	}

	public Long getIdPromotoria() {
		return idPromotoria;
	}

	public void setIdPromotoria(Long idPromotoria) {
		this.idPromotoria = idPromotoria;
	}

	public String getPromotoria() {
		return promotoria;
	}

	public void setPromotoria(String promotoria) {
		this.promotoria = promotoria;
	}

	public Long getIdAgente() {
		return idAgente;
	}

	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}

	public String getNombreAgente() {
		return nombreAgente;
	}

	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}

	public Double getImporteComision() {
		return importeComision;
	}

	public void setImporteComision(Double importeComision) {
		this.importeComision = importeComision;
	}
	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public String getNumCta() {
		return numCta;
	}
	public void setNumCta(String numCta) {
		this.numCta = numCta;
	}
	
	
}
