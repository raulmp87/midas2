package mx.com.afirme.midas2.dto.endoso;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.DatosAseguradoView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.ParametrosAutoIncisoView;

public class CambioDatosDTO implements Serializable{
	private static final long serialVersionUID = 613624217125156374L;
	
	private ParametrosAutoIncisoView datosComplementariosVehiculo;	
	private Map<String, String> datosRiesgo;
	private DatosAseguradoView datosAsegurado;
	private BigDecimal idContratante;
	
	public ParametrosAutoIncisoView getDatosComplementariosVehiculo() {
		return datosComplementariosVehiculo;
	}
	public void setDatosComplementariosVehiculo(ParametrosAutoIncisoView datosComplementariosVehiculo) {
		this.datosComplementariosVehiculo = datosComplementariosVehiculo;
	}
	public Map<String, String> getDatosRiesgo() {
		return datosRiesgo;
	}
	public void setDatosRiesgo(Map<String, String> datosRiesgo) {
		this.datosRiesgo = datosRiesgo;
	}
	public DatosAseguradoView getDatosAsegurado() {
		return datosAsegurado;
	}
	public void setDatosAsegurado(DatosAseguradoView datosAsegurado) {
		this.datosAsegurado = datosAsegurado;
	}
	public BigDecimal getIdContratante() {
		return idContratante;
	}
	public void setIdContratante(BigDecimal idContratante) {
		this.idContratante = idContratante;
	}
}
