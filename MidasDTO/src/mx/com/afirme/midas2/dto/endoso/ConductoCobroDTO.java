package mx.com.afirme.midas2.dto.endoso;

import java.io.Serializable;

public class ConductoCobroDTO implements Serializable{
	private static final long serialVersionUID = -7357587293281052545L;
	
	private Short idMedioPago;
	private Long idConductoCobroCliente;
	private DatosTitularDTO datosTitular;
	private DatosTarjetaDTO datosTarjeta;
	
	public Short getIdMedioPago() {
		return idMedioPago;
	}
	public void setIdMedioPago(Short idMedioPago) {
		this.idMedioPago = idMedioPago;
	}
	public Long getIdConductoCobroCliente() {
		return idConductoCobroCliente;
	}
	public void setIdConductoCobroCliente(Long idConductoCobroCliente) {
		this.idConductoCobroCliente = idConductoCobroCliente;
	}
	public DatosTitularDTO getDatosTitular() {
		return datosTitular;
	}
	public void setDatosTitular(DatosTitularDTO datosTitular) {
		this.datosTitular = datosTitular;
	}
	public DatosTarjetaDTO getDatosTarjeta() {
		return datosTarjeta;
	}
	public void setDatosTarjeta(DatosTarjetaDTO datosTarjeta) {
		this.datosTarjeta = datosTarjeta;
	}
}
