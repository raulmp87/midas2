package mx.com.afirme.midas2.dto.endoso.cotizacion.auto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import mx.com.afirme.midas.base.PaginadoDTO;

import org.joda.time.DateTime;

public class CotizacionEndosoDTO extends PaginadoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5812229276163304359L;
	private BigDecimal numeroCotizacionEndoso;
	private BigDecimal idPoliza;
	private Integer numeroPoliza;
	private String numeroPolizaFormateado;
	private String nombreAsegurado;
	private String numeroSerieVehiculo;
	private short idTipoEndoso;
	private String descripcionTipoEndoso;
	private Date fechaCotizacionEndoso;	
	private short estatusCotizacionEndoso;
	private String descripcionMovimiento;
	private String numeroInciso;
	private String numeroMovimiento;
	private Date fechaInicioEndoso;
	private Date fechaEmisionEndoso;
	private Date fechaEmisionEndosoMidas;
    private Double primaEndoso;
    private Date fechaCotizacionDesde;
    private Date fechaCotizacionHasta;    
    private Boolean conflictoNumeroSerie;
    private Boolean busquedaEstatusEnProceso;
    private Boolean busquedaEstatusEmitidas;
	private Date fechaInicioVigencia;
	private Long continuityId;
	private BigDecimal solicitudId;
	private Boolean origenCancelacionAutomatica;
	private Boolean aplicaCorrimientoVigencias;
	private Integer requiereCorrimientoVigencias;
	private Date fechaFinPoliza;
	private Date fechaFinPolizaRecorrida;
	private DateTime fechaEmisionCancelacion;
	private DateTime cancelacionValidaDesde;
	private String numeroEndoso;
	private Short claveTipoEndoso;
	private String descripcionMotivoEndoso;
	private String codigoUsuarioCreacion;
	
	public BigDecimal getNumeroCotizacionEndoso() {
		return numeroCotizacionEndoso;
	}
	public void setNumeroCotizacionEndoso(BigDecimal numeroCotizacionEndoso) {
		this.numeroCotizacionEndoso = numeroCotizacionEndoso;
	}
	public String getNumeroPolizaFormateado() {
		return numeroPolizaFormateado;
	}
	public void setNumeroPolizaFormateado(String numeroPolizaFormateado) {
		this.numeroPolizaFormateado = numeroPolizaFormateado;
	}
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}
	
	public short getIdTipoEndoso() {
		return idTipoEndoso;
	}
	public void setIdTipoEndoso(short idTipoEndoso) {
		this.idTipoEndoso = idTipoEndoso;
	}
	public String getDescripcionTipoEndoso() {
		return descripcionTipoEndoso;
	}
	public void setDescripcionTipoEndoso(String descripcionTipoEndoso) {
		this.descripcionTipoEndoso = descripcionTipoEndoso;
	}
	public Date getFechaCotizacionEndoso() {
		return fechaCotizacionEndoso;
	}
	public void setFechaCotizacionEndoso(Date fechaCotizacionEndoso) {
		this.fechaCotizacionEndoso = fechaCotizacionEndoso;
	}
	public short getEstatusCotizacionEndoso() {
		return estatusCotizacionEndoso;
	}
	public void setEstatusCotizacionEndoso(short estatusCotizacionEndoso) {
		this.estatusCotizacionEndoso = estatusCotizacionEndoso;
	}
	public String getDescripcionMovimiento() {
		return descripcionMovimiento;
	}
	public void setDescripcionMovimiento(String descripcionMovimiento) {
		this.descripcionMovimiento = descripcionMovimiento;
	}
	public String getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public String getNumeroMovimiento() {
		return numeroMovimiento;
	}
	public void setNumeroMovimiento(String numeroMovimiento) {
		this.numeroMovimiento = numeroMovimiento;
	}
	public Date getFechaEmisionEndoso() {
		return fechaEmisionEndoso;
	}
	public void setFechaEmisionEndoso(Date fechaEmisionEndoso) {
		this.fechaEmisionEndoso = fechaEmisionEndoso;
	}
	public Date getFechaEmisionEndosoMidas() {
		return fechaEmisionEndosoMidas;
	}
	public void setFechaEmisionEndosoMidas(Date fechaEmisionEndosoMidas) {
		this.fechaEmisionEndosoMidas = fechaEmisionEndosoMidas;
	}
	public Double getPrimaEndoso() {
		return primaEndoso;
	}
	public void setPrimaEndoso(Double primaEndoso) {
		this.primaEndoso = primaEndoso;
	}
	public Boolean getBusquedaEstatusEnProceso() {
		return busquedaEstatusEnProceso;
	}
	public void setBusquedaEstatusEnProceso(Boolean busquedaEstatusEnProceso) {
		this.busquedaEstatusEnProceso = busquedaEstatusEnProceso;
	}
	public Boolean getBusquedaEstatusEmitidas() {
		return busquedaEstatusEmitidas;
	}
	public void setBusquedaEstatusEmitidas(Boolean busquedaEstatusEmitidas) {
		this.busquedaEstatusEmitidas = busquedaEstatusEmitidas;
	}
	public Date getFechaInicioEndoso() {
		return fechaInicioEndoso;
	}
	public void setFechaInicioEndoso(Date fechaInicioEndoso) {
		this.fechaInicioEndoso = fechaInicioEndoso;
	}
	public Date getFechaCotizacionDesde() {
		return fechaCotizacionDesde;
	}
	public void setFechaCotizacionDesde(Date fechaCotizacionDesde) {
		this.fechaCotizacionDesde = fechaCotizacionDesde;
	}
	public Date getFechaCotizacionHasta() {
		return fechaCotizacionHasta;
	}
	public void setFechaCotizacionHasta(Date fechaCotizacionHasta) {
		this.fechaCotizacionHasta = fechaCotizacionHasta;
	}	
	public Boolean getConflictoNumeroSerie() {
		return conflictoNumeroSerie;
	}
	public void setConflictoNumeroSerie(Boolean conflictoNumeroSerie) {
		this.conflictoNumeroSerie = conflictoNumeroSerie;
	}
	public String getNumeroSerieVehiculo() {
		return numeroSerieVehiculo;
	}
	public void setNumeroSerieVehiculo(String numeroSerieVehiculo) {
		this.numeroSerieVehiculo = numeroSerieVehiculo;
	}
	public BigDecimal getIdPoliza() {
		return idPoliza;
	}
	public void setIdPoliza(BigDecimal idPoliza) {
		this.idPoliza = idPoliza;
	}
	public Integer getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(Integer numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}
	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	public Long getContinuityId() {
		return continuityId;
	}
	public void setContinuityId(Long continuityId) {
		this.continuityId = continuityId;
	}
	public BigDecimal getSolicitudId() {
		return solicitudId;
	}
	public void setSolicitudId(BigDecimal solicitudId) {
		this.solicitudId = solicitudId;
	}
	public Boolean getOrigenCancelacionAutomatica() {
		return origenCancelacionAutomatica;
	}
	public void setOrigenCancelacionAutomatica(Boolean origenCancelacionAutomatica) {
		this.origenCancelacionAutomatica = origenCancelacionAutomatica;
	}
	public Boolean getAplicaCorrimientoVigencias() {
		return aplicaCorrimientoVigencias;
	}
	public void setAplicaCorrimientoVigencias(Boolean aplicaCorrimientoVigencias) {
		this.aplicaCorrimientoVigencias = aplicaCorrimientoVigencias;
	}
	public Integer getRequiereCorrimientoVigencias() {
		return requiereCorrimientoVigencias;
	}
	public void setRequiereCorrimientoVigencias(Integer requiereCorrimientoVigencias) {
		this.requiereCorrimientoVigencias = requiereCorrimientoVigencias;
	}
	public Date getFechaFinPoliza() {
		return fechaFinPoliza;
	}
	public void setFechaFinPoliza(Date fechaFinPoliza) {
		this.fechaFinPoliza = fechaFinPoliza;
	}
	public Date getFechaFinPolizaRecorrida() {
		return fechaFinPolizaRecorrida;
	}
	public void setFechaFinPolizaRecorrida(Date fechaFinPolizaRecorrida) {
		this.fechaFinPolizaRecorrida = fechaFinPolizaRecorrida;
	}
	public DateTime getFechaEmisionCancelacion() {
		return fechaEmisionCancelacion;
	}
	public void setFechaEmisionCancelacion(DateTime fechaEmisionCancelacion) {
		this.fechaEmisionCancelacion = fechaEmisionCancelacion;
	}
	public DateTime getCancelacionValidaDesde() {
		return cancelacionValidaDesde;
	}
	public void setCancelacionValidaDesde(DateTime cancelacionValidaDesde) {
		this.cancelacionValidaDesde = cancelacionValidaDesde;
	}
	public String getNumeroEndoso() {
		return numeroEndoso;
	}
	public void setNumeroEndoso(String numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}
	public Short getClaveTipoEndoso() {
		return claveTipoEndoso;
	}
	public void setClaveTipoEndoso(Short claveTipoEndoso) {
		this.claveTipoEndoso = claveTipoEndoso;
	}
	public String getDescripcionMotivoEndoso() {
		return descripcionMotivoEndoso;
	}
	public void setDescripcionMotivoEndoso(String descripcionMotivoEndoso) {
		this.descripcionMotivoEndoso = descripcionMotivoEndoso;
	}
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}
	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}
	
}
