package mx.com.afirme.midas2.dto.endoso;

import java.io.Serializable;

public class DatosTarjetaDTO implements Serializable{
	private static final long serialVersionUID = 2438684288611853577L;
	
	private Long idBanco;
	private String numeroTarjeta;
	private String fechaVencimientoTarjeta;
	private String codigoSeguridadTarjeta;
	private String idTipoTarjeta;
	
	public Long getIdBanco() {
		return idBanco;
	}
	public void setIdBanco(Long idBanco) {
		this.idBanco = idBanco;
	}
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}
	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}
	public String getFechaVencimientoTarjeta() {
		return fechaVencimientoTarjeta;
	}
	public void setFechaVencimientoTarjeta(String fechaVencimientoTarjeta) {
		this.fechaVencimientoTarjeta = fechaVencimientoTarjeta;
	}
	public String getCodigoSeguridadTarjeta() {
		return codigoSeguridadTarjeta;
	}
	public void setCodigoSeguridadTarjeta(String codigoSeguridadTarjeta) {
		this.codigoSeguridadTarjeta = codigoSeguridadTarjeta;
	}
	public String getIdTipoTarjeta() {
		return idTipoTarjeta;
	}
	public void setIdTipoTarjeta(String idTipoTarjeta) {
		this.idTipoTarjeta = idTipoTarjeta;
	}
}
