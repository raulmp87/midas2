package mx.com.afirme.midas2.dto.endoso;

import java.io.Serializable;
import java.math.BigDecimal;

public class EndosoTransporteDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2742486331974846071L;
	
	private String numeroPoliza;
	private Short claveTipoEndoso;
	private BigDecimal idCotizacion;
	private String fechaInicioVigenciaEndoso;
	private DatosEndosoDTO datosEndoso;
	private String token;
	private Short numeroEndoso;
	private int motivoEndoso;
	
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public Short getClaveTipoEndoso() {
		return claveTipoEndoso;
	}
	public void setClaveTipoEndoso(Short claveTipoEndoso) {
		this.claveTipoEndoso = claveTipoEndoso;
	}
	public DatosEndosoDTO getDatosEndoso() {
		return datosEndoso;
	}
	public void setDatosEndoso(DatosEndosoDTO datosEndoso) {
		this.datosEndoso = datosEndoso;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public BigDecimal getIdCotizacion() {
		return idCotizacion;
	}
	public void setIdCotizacion(BigDecimal idCotizacion) {
		this.idCotizacion = idCotizacion;
	}
	public String getFechaInicioVigenciaEndoso() {
		return fechaInicioVigenciaEndoso;
	}
	public void setFechaInicioVigenciaEndoso(String fechaInicioVigenciaEndoso) {
		this.fechaInicioVigenciaEndoso = fechaInicioVigenciaEndoso;
	}
	public Short getNumeroEndoso() {
		return numeroEndoso;
	}
	public void setNumeroEndoso(Short numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}
	public int getMotivoEndoso() {
		return motivoEndoso;
	}
	public void setMotivoEndoso(int motivoEndoso) {
		this.motivoEndoso = motivoEndoso;
	}
}