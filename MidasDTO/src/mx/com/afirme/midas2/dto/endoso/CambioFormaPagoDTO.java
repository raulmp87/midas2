package mx.com.afirme.midas2.dto.endoso;

import java.io.Serializable;

public class CambioFormaPagoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2930299621029926707L;

	private int idFormaPago;
	private double porcentajePagoFraccionado;
	
	public int getIdFormaPago() {
		return idFormaPago;
	}
	public void setIdFormaPago(int idFormaPago) {
		this.idFormaPago = idFormaPago;
	}
	public double getPorcentajePagoFraccionado() {
		return porcentajePagoFraccionado;
	}
	public void setPorcentajePagoFraccionado(double porcentajePagoFraccionado) {
		this.porcentajePagoFraccionado = porcentajePagoFraccionado;
	}
}
