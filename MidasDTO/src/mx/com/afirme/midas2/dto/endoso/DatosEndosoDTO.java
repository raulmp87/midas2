package mx.com.afirme.midas2.dto.endoso;

import java.io.Serializable;

public class DatosEndosoDTO implements Serializable{
	private static final long serialVersionUID = 3496138204703711150L;
	private ConductoCobroDTO conductoCobro;
	private CambioDatosDTO cambioDatos;
	private CambioFormaPagoDTO cambioFormaPago;
	
	public ConductoCobroDTO getConductoCobro() {
		return conductoCobro;
	}
	public void setConductoCobro(ConductoCobroDTO conductoCobro) {
		this.conductoCobro = conductoCobro;
	}
	public CambioDatosDTO getCambioDatos() {
		return cambioDatos;
	}
	public void setCambioDatos(CambioDatosDTO cambioDatos) {
		this.cambioDatos = cambioDatos;
	}
	public CambioFormaPagoDTO getCambioFormaPago() {
		return cambioFormaPago;
	}
	public void setCambioFormaPago(CambioFormaPagoDTO cambioFormaPago) {
		this.cambioFormaPago = cambioFormaPago;
	}
}
