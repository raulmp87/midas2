package mx.com.afirme.midas2.dto.endoso.recibos;

import java.io.Serializable;
import java.math.BigDecimal;

public class RecibosEndosoTransporteDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2528445747911668013L;

	private String numeroPoliza;
	private BigDecimal numeroEndoso;
	private BigDecimal idProgramaPago;
	private BigDecimal numeroInciso;
	private BigDecimal idRecibo;
	private BigDecimal idInciso;
	private String token;
	
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public BigDecimal getNumeroEndoso() {
		return numeroEndoso;
	}
	public void setNumeroEndoso(BigDecimal numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}
	public BigDecimal getIdProgramaPago() {
		return idProgramaPago;
	}
	public void setIdProgramaPago(BigDecimal idProgramaPago) {
		this.idProgramaPago = idProgramaPago;
	}
	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public BigDecimal getIdRecibo() {
		return idRecibo;
	}
	public void setIdRecibo(BigDecimal idRecibo) {
		this.idRecibo = idRecibo;
	}
	public BigDecimal getIdInciso() {
		return idInciso;
	}
	public void setIdInciso(BigDecimal idInciso) {
		this.idInciso = idInciso;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
}
