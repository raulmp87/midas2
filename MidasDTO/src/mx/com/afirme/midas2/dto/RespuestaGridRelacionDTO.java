package mx.com.afirme.midas2.dto;

import java.io.Serializable;

public class RespuestaGridRelacionDTO implements Serializable {

	private static final long serialVersionUID = -8555276161165520519L;

	private String tipoRespuesta;
	private String idOriginal;
	private String idResultado;
	private String tipoMensaje;
	private String operacionExitosa;
	private String mensaje;
	
	public String getTipoRespuesta() {
		return tipoRespuesta;
	}
	public void setTipoRespuesta(String tipoRespuesta) {
		this.tipoRespuesta = tipoRespuesta;
	}
	public String getIdOriginal() {
		return idOriginal;
	}
	public void setIdOriginal(String idOriginal) {
		this.idOriginal = idOriginal;
	}
	public String getIdResultado() {
		return idResultado;
	}
	public void setIdResultado(String idResultado) {
		this.idResultado = idResultado;
	}
	public String getTipoMensaje() {
		return tipoMensaje;
	}
	public void setTipoMensaje(String tipoMensaje) {
		this.tipoMensaje = tipoMensaje;
	}
	public String getOperacionExitosa() {
		return operacionExitosa;
	}
	public void setOperacionExitosa(String operacionExitosa) {
		this.operacionExitosa = operacionExitosa;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	
	
	
}
