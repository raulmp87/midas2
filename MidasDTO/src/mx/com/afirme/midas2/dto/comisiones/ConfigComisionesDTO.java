package mx.com.afirme.midas2.dto.comisiones;

import java.io.Serializable;
import java.util.Date;
//import java.util.List;



import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
//import mx.com.afirme.midas2.domain.comisiones.ConfigComCentroOperacion;
//import mx.com.afirme.midas2.domain.comisiones.ConfigComEjecutivo;
//import mx.com.afirme.midas2.domain.comisiones.ConfigComGerencia;
//import mx.com.afirme.midas2.domain.comisiones.ConfigComPrioridad;
//import mx.com.afirme.midas2.domain.comisiones.ConfigComPromotoria;
//import mx.com.afirme.midas2.domain.comisiones.ConfigComSituacion;
//import mx.com.afirme.midas2.domain.comisiones.ConfigComTipoAgente;
//import mx.com.afirme.midas2.domain.comisiones.ConfigComTipoPromotoria;
import mx.com.afirme.midas2.domain.comisiones.ConfigComisiones;
/**
 * 
 * @author vmhersil
 * Clase para utilizarla como filtro de busqueda en Configuracion de comisiones 
 * para la pantalla de listar configuracion de comisiones existentes.
 *
 */
@Entity
@SqlResultSetMapping(name="ConfigComisionesView",entities={
	@EntityResult(entityClass=ConfigComisionesDTO.class,fields={
		@FieldResult(name="id",column="ID"),
		@FieldResult(name="activo",column="CLAVEACTIVO"),
		@FieldResult(name="pagoSinFactura",column="CLAVEPAGOSINFACTURA"),
		@FieldResult(name="todosLosAgentes",column="CLAVETODOSAGENTES"),
		@FieldResult(name="diaEjecucion",column="DIAEJECUCION"),
		@FieldResult(name="fechaInicioVigencia",column="FECHAINICIOVIGENCIA"),
		@FieldResult(name="horarioEjecucion",column="HORARIOEJECUCION"),
		@FieldResult(name="importeMinimo",column="IMPORTEMINIMO"),
		@FieldResult(name="agenteInicio",column="IDAGENTEINICIO"),
		@FieldResult(name="agenteFin",column="IDAGENTEFIN"),
		@FieldResult(name="agentes",column="AGENTES"),
		@FieldResult(name="modoEjecucion.id",column="IDMODOEJECUCION"),
		@FieldResult(name="periodoEjecucion.id",column="IDPERIODOEJECUCION"),
		@FieldResult(name="idCentroOperacion",column="IDCENTROOPERACION"),
		@FieldResult(name="idGerencia",column="IDGERENCIA"),
		@FieldResult(name="idEjecutivo",column="IDEJECUTIVO"),
		@FieldResult(name="idPromotoria",column="IDPROMOTORIA"),
		@FieldResult(name="idTipoAgente",column="TIPOAGENTE"),
		@FieldResult(name="idTipoPromotoria",column="TIPOPROMOTORIA"),
		@FieldResult(name="idProridad",column="PRIORIDAD"),
		@FieldResult(name="idSituacion",column="SITUACION")
	})
})
public class ConfigComisionesDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3350099082616380743L;
	private Long idCentroOperacion;
	private Long idGerencia;
	private Long idProridad;
	private Long idEjecutivo;
	private Long idPromotoria;
	private Long idTipoPromotoria;
	private Long idTipoAgente;
	private Long idSituacion;
	private Date fechaAltaInicio;
	private Date fechaAltaFin;
	private Long 		id;
	private Boolean 	todosLosAgentesBoolean;
	private Integer 	todosLosAgentes;
	private Long 		agenteInicio;
	private Long 		agenteFin;
	private Double 		importeMinimo;
	private Boolean 	pagoSinFacturaBoolean;
	private Integer 	pagoSinFactura;
	private Date 		fechaInicioVigencia;
	private ValorCatalogoAgentes modoEjecucion;
	private ValorCatalogoAgentes periodoEjecucion;
	private Integer		diaEjecucion;
	private	Date		horarioEjecucion;
	private	Boolean		activoBoolean;
	private	Integer		activo;
	private String 		agentes;
//	private List<ConfigComCentroOperacion> listaCentroOperaciones;
//	private List<ConfigComGerencia> listaGerencias;
//	private List<ConfigComEjecutivo> listaEjecutivos;
//	private List<ConfigComPromotoria> listaPromotorias;
//	private List<ConfigComTipoPromotoria> listaTiposPromotoria;
//	private List<ConfigComPrioridad> listaPrioridades;
//	private List<ConfigComSituacion> listaSituaciones;
//	private List<ConfigComTipoAgente> listaTipoAgentes;

	public ConfigComisionesDTO(){}
	
	public ConfigComisionesDTO(ConfigComisiones config){
		this.activo=config.getActivo();
		this.activoBoolean=config.getActivoBoolean();
		this.agenteFin=config.getAgenteFin();
		this.agenteInicio=config.getAgenteInicio();
		this.agentes=config.getAgentes();
		this.diaEjecucion=config.getDiaEjecucion();
		this.fechaInicioVigencia=config.getFechaInicioVigencia();
		this.horarioEjecucion=config.getHorarioEjecucion();
		this.id=config.getId();
		this.importeMinimo=config.getImporteMinimo();
		this.modoEjecucion=config.getModoEjecucion();
		this.pagoSinFactura=config.getPagoSinFactura();
		this.pagoSinFacturaBoolean=config.getPagoSinFacturaBoolean();
		this.periodoEjecucion=config.getPeriodoEjecucion();
		this.todosLosAgentes=config.getTodosLosAgentes();
		this.todosLosAgentesBoolean=config.getTodosLosAgentesBoolean();
	}
	
	public Long getIdCentroOperacion() {
		return idCentroOperacion;
	}
	public void setIdCentroOperacion(Long idCentroOperacion) {
		this.idCentroOperacion = idCentroOperacion;
	}
	public Long getIdGerencia() {
		return idGerencia;
	}
	public void setIdGerencia(Long idGerencia) {
		this.idGerencia = idGerencia;
	}
	public Long getIdProridad() {
		return idProridad;
	}
	public void setIdProridad(Long idProridad) {
		this.idProridad = idProridad;
	}
	public Long getIdEjecutivo() {
		return idEjecutivo;
	}
	public void setIdEjecutivo(Long idEjecutivo) {
		this.idEjecutivo = idEjecutivo;
	}
	public Long getIdPromotoria() {
		return idPromotoria;
	}
	public void setIdPromotoria(Long idPromotoria) {
		this.idPromotoria = idPromotoria;
	}
	public Long getIdTipoPromotoria() {
		return idTipoPromotoria;
	}
	public void setIdTipoPromotoria(Long idTipoPromotoria) {
		this.idTipoPromotoria = idTipoPromotoria;
	}
	public Long getIdTipoAgente() {
		return idTipoAgente;
	}
	public void setIdTipoAgente(Long idTipoAgente) {
		this.idTipoAgente = idTipoAgente;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaAltaInicio() {
		return fechaAltaInicio;
	}
	public void setFechaAltaInicio(Date fechaAltaInicio) {
		this.fechaAltaInicio = fechaAltaInicio;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaAltaFin() {
		return fechaAltaFin;
	}
	public void setFechaAltaFin(Date fechaAltaFin) {
		this.fechaAltaFin = fechaAltaFin;
	}
	public Long getIdSituacion() {
		return idSituacion;
	}
	public void setIdSituacion(Long idSituacion) {
		this.idSituacion = idSituacion;
	}
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Boolean getTodosLosAgentesBoolean() {
		return todosLosAgentesBoolean;
	}
	public void setTodosLosAgentesBoolean(Boolean todosLosAgentesBoolean) {
		this.todosLosAgentesBoolean = todosLosAgentesBoolean;
	}
	public Integer getTodosLosAgentes() {
		return todosLosAgentes;
	}
	public void setTodosLosAgentes(Integer todosLosAgentes) {
		this.todosLosAgentes = todosLosAgentes;
	}
	public Long getAgenteInicio() {
		return agenteInicio;
	}
	public void setAgenteInicio(Long agenteInicio) {
		this.agenteInicio = agenteInicio;
	}
	public Long getAgenteFin() {
		return agenteFin;
	}
	public void setAgenteFin(Long agenteFin) {
		this.agenteFin = agenteFin;
	}
	public Double getImporteMinimo() {
		return importeMinimo;
	}
	public void setImporteMinimo(Double importeMinimo) {
		this.importeMinimo = importeMinimo;
	}
	public Boolean getPagoSinFacturaBoolean() {
		return pagoSinFacturaBoolean;
	}
	public void setPagoSinFacturaBoolean(Boolean pagoSinFacturaBoolean) {
		this.pagoSinFacturaBoolean = pagoSinFacturaBoolean;
	}
	public Integer getPagoSinFactura() {
		return pagoSinFactura;
	}
	public void setPagoSinFactura(Integer pagoSinFactura) {
		this.pagoSinFactura = pagoSinFactura;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}
	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	public ValorCatalogoAgentes getModoEjecucion() {
		return modoEjecucion;
	}
	public void setModoEjecucion(ValorCatalogoAgentes modoEjecucion) {
		this.modoEjecucion = modoEjecucion;
	}
	public ValorCatalogoAgentes getPeriodoEjecucion() {
		return periodoEjecucion;
	}
	public void setPeriodoEjecucion(ValorCatalogoAgentes periodoEjecucion) {
		this.periodoEjecucion = periodoEjecucion;
	}
	public Integer getDiaEjecucion() {
		return diaEjecucion;
	}
	public void setDiaEjecucion(Integer diaEjecucion) {
		this.diaEjecucion = diaEjecucion;
	}
	@Temporal(TemporalType.TIMESTAMP)
	public Date getHorarioEjecucion() {
		return horarioEjecucion;
	}
	public void setHorarioEjecucion(Date horarioEjecucion) {
		this.horarioEjecucion = horarioEjecucion;
	}
	public Boolean getActivoBoolean() {
		return activoBoolean;
	}
	public void setActivoBoolean(Boolean activoBoolean) {
		this.activoBoolean = activoBoolean;
	}
	
	
	public Integer getActivo() {
		return activo;
	}
	public void setActivo(Integer activo) {
		this.activo = activo;
	}
//	public List<ConfigComCentroOperacion> getListaCentroOperaciones() {
//		return listaCentroOperaciones;
//	}
//	public void setListaCentroOperaciones(
//			List<ConfigComCentroOperacion> listaCentroOperaciones) {
//		this.listaCentroOperaciones = listaCentroOperaciones;
//	}
//	public List<ConfigComGerencia> getListaGerencias() {
//		return listaGerencias;
//	}
//	public void setListaGerencias(List<ConfigComGerencia> listaGerencias) {
//		this.listaGerencias = listaGerencias;
//	}
//	public List<ConfigComEjecutivo> getListaEjecutivos() {
//		return listaEjecutivos;
//	}
//	public void setListaEjecutivos(List<ConfigComEjecutivo> listaEjecutivos) {
//		this.listaEjecutivos = listaEjecutivos;
//	}
//	public List<ConfigComPromotoria> getListaPromotorias() {
//		return listaPromotorias;
//	}
//	public void setListaPromotorias(List<ConfigComPromotoria> listaPromotorias) {
//		this.listaPromotorias = listaPromotorias;
//	}
//	public List<ConfigComTipoPromotoria> getListaTiposPromotoria() {
//		return listaTiposPromotoria;
//	}
//	public void setListaTiposPromotoria(
//			List<ConfigComTipoPromotoria> listaTiposPromotoria) {
//		this.listaTiposPromotoria = listaTiposPromotoria;
//	}
//	public List<ConfigComPrioridad> getListaPrioridades() {
//		return listaPrioridades;
//	}
//	public void setListaPrioridades(List<ConfigComPrioridad> listaPrioridades) {
//		this.listaPrioridades = listaPrioridades;
//	}
//	public List<ConfigComSituacion> getListaSituaciones() {
//		return listaSituaciones;
//	}
//	public void setListaSituaciones(List<ConfigComSituacion> listaSituaciones) {
//		this.listaSituaciones = listaSituaciones;
//	}
//	public List<ConfigComTipoAgente> getListaTipoAgentes() {
//		return listaTipoAgentes;
//	}
//	public void setListaTipoAgentes(List<ConfigComTipoAgente> listaTipoAgentes) {
//		this.listaTipoAgentes = listaTipoAgentes;
//	}

	public String getAgentes() {
		return agentes;
	}

	public void setAgentes(String agentes) {
		this.agentes = agentes;
	}
}
