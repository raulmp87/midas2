/**
 * 
 */
package mx.com.afirme.midas2.dto.vehiculo;

import java.math.BigDecimal;

/**
 * @author admin
 *
 */
public class ValorReferenciaComercialAutoDTO {
	
	
	private Long idProveedor;
	
	private String claveAmis;
	
	private BigDecimal modeloVehiculo;
	
	private BigDecimal valorComercial;
	
	private BigDecimal mes;
	
	private BigDecimal anio;

	/**
	 * @return the idProveedor
	 */
	public Long getIdProveedor() {
		return idProveedor;
	}

	/**
	 * @param idProveedor the idProveedor to set
	 */
	public void setIdProveedor(Long idProveedor) {
		this.idProveedor = idProveedor;
	}

	/**
	 * @return the claveAmis
	 */
	public String getClaveAmis() {
		return claveAmis;
	}

	/**
	 * @param claveAmis the claveAmis to set
	 */
	public void setClaveAmis(String claveAmis) {
		this.claveAmis = claveAmis;
	}

	/**
	 * @return the modeloVehiculo
	 */
	public BigDecimal getModeloVehiculo() {
		return modeloVehiculo;
	}

	/**
	 * @param modeloVehiculo the modeloVehiculo to set
	 */
	public void setModeloVehiculo(BigDecimal modeloVehiculo) {
		this.modeloVehiculo = modeloVehiculo;
	}

	/**
	 * @return the valorComercial
	 */
	public BigDecimal getValorComercial() {
		return valorComercial;
	}

	/**
	 * @param valorComercial the valorComercial to set
	 */
	public void setValorComercial(BigDecimal valorComercial) {
		this.valorComercial = valorComercial;
	}

	/**
	 * @return the mes
	 */
	public BigDecimal getMes() {
		return mes;
	}

	/**
	 * @param mes the mes to set
	 */
	public void setMes(BigDecimal mes) {
		this.mes = mes;
	}

	/**
	 * @return the anio
	 */
	public BigDecimal getAnio() {
		return anio;
	}

	/**
	 * @param anio the anio to set
	 */
	public void setAnio(BigDecimal anio) {
		this.anio = anio;
	}
}
