package mx.com.afirme.midas2.dto;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas2.domain.negocio.cliente.ConfiguracionCampo;
import mx.com.afirme.midas2.dto.ClientBeanValidator.SectionType;
import mx.com.afirme.midas2.util.ReflectionUtils;

public class ClientBeanValidatorProcessor {
	private static Map<String,List<Field>> obtenerCamposMapeados(ClienteGenericoDTO cliente){
		//List<Field> camposConAnotacion=ReflectionUtils.getFieldsWithAnnotation(cliente.getClass(), ClientBeanValidator.class);
		Map<String,List<Field>> camposAgrupadosPorAtributo=ReflectionUtils.getFieldsWithAnnotationGroupByAttribute(cliente.getClass(),ClientBeanValidator.class,"seccion");
		return camposAgrupadosPorAtributo;
	}
	/**
	 * Obtiene la lista de campos mapeados del objeto cliente, es decir todos los atributos de cliente que tengan la anotacion ClientBeanValidator.
	 * @param cliente
	 * @return
	 */
	private static List<Field> getAllMappedFields(ClienteGenericoDTO cliente){
		List<Field> camposConAnotacion=ReflectionUtils.getFieldsWithAnnotation(cliente.getClass(), ClientBeanValidator.class);
		return camposConAnotacion;
	}
	/**
	 * Valida los datos del cliente de acuerdo a la configuracion de sus campos
	 * @param cliente
	 * @param configuracionCampos
	 * @return
	 * @throws Exception
	 */
	public static Map<String,Map<String,String>> validarClienteGenerico(ClienteGenericoDTO cliente,List<ConfiguracionCampo> configuracionCampos)throws Exception{
		Map<String,Map<String,String>> errorList=new HashMap<String, Map<String,String>>();
		Map<String,List<Field>> camposClientePorSeccion=obtenerCamposMapeados(cliente);
		if(camposClientePorSeccion!=null && !camposClientePorSeccion.isEmpty()){
			for(String seccion:camposClientePorSeccion.keySet()){
				List<Field> camposCliente=camposClientePorSeccion.get(seccion);
				//Si hay campos con anotaciones agrupadas por seccion, entonces
				//estos campos se tendran que validar de acuerdo a su configuracion.
				if(camposCliente!=null && !camposCliente.isEmpty()){
					Map<String,String> errores=validarCamposPorSeccion(cliente, camposCliente, configuracionCampos);
					//Si no tiene errores
					/*if(errores!=null && !errores.isEmpty()){
						errorList.put(seccion, errores);
					}else{
						errorList.put(seccion, null);//indica que la seccion no tiene errores, por lo tanto si puede ser guardada.
					}*/
					errorList.put(seccion, errores);//Si la lista de errores es nula, indica que no requiere que la seccion sea validad al guardar
					//Si la lista de errores es vacia, indica que todo lo que era requerido estaba bien
					//si trae algo la lista de errores hay que pintarlos en la forma.
				}else{
					errorList.put(seccion, null);
				}
			}
		}
		return errorList;
	}
	/**
	 * Permite validar seccion por seccion los campos que son requeridos.
	 * @param tipoSeccion Indica si es seccion de datos generales,fiscales,contacto,cobranza u otros.
	 * @param cliente
	 * @param configuracionCampos Lista de campos que son considerados como requeridos.
	 * @return
	 * @throws Exception
	 */
	public static Map<String,String> validarPorSeccion(SectionType tipoSeccion,ClienteGenericoDTO cliente,List<ConfiguracionCampo> configuracionCampos)throws Exception{
		Map<String,String> errores=new HashMap<String, String>();
		Map<String,List<Field>> camposClientePorSeccion=obtenerCamposMapeados(cliente);
		if(camposClientePorSeccion!=null && !camposClientePorSeccion.isEmpty()){
			for(String seccion:camposClientePorSeccion.keySet()){
				if(seccion.equals(tipoSeccion.getValue())){
					List<Field> camposCliente=camposClientePorSeccion.get(seccion);
					//Si hay campos con anotaciones agrupadas por seccion, entonces
					//estos campos se tendran que validar de acuerdo a su configuracion.
					if(camposCliente!=null && !camposCliente.isEmpty()){
						errores=validarCamposPorSeccion(cliente, camposCliente, configuracionCampos);
					}
				}
			}
		}
		return errores;
	}
	/**
	 * Obtiene los errores de los campos del cliente de acuerdo a la configuracion de los campos.
	 * @param cliente
	 * @param camposCliente
	 * @param configuracionCampos
	 * @return
	 */
	private static Map<String, String> validarCamposPorSeccion(ClienteGenericoDTO cliente,List<Field> camposCliente,List<ConfiguracionCampo> configuracionCampos){
		Map<String,String> errorList=new HashMap<String,String>();
		boolean seccionRequiereValidacion=false;
		if(camposCliente!=null && !camposCliente.isEmpty() && configuracionCampos!=null && !configuracionCampos.isEmpty()){
			cicloCamposConAnotacion:
			for(Field campo:camposCliente){
				Object campoMapeado=ReflectionUtils.getAttributeValueFromAnnotation(campo, ClientBeanValidator.class,"mappingLabel");
				Object model=ReflectionUtils.getAttributeValueFromAnnotation(campo, ClientBeanValidator.class,"modelView");
				String modelView=(model!=null)?model.toString()+".":"";
				Object errorMessage=ReflectionUtils.getAttributeValueFromAnnotation(campo, ClientBeanValidator.class,"errorMessage");
				//Si no encuentra el campo a mapear se pasa al siguiente
				if(campoMapeado==null){
					//TODO continuar?
					continue cicloCamposConAnotacion;
				}
				ConfiguracionCampo configuracionDelCampo=null;
				cicloConfiguracionCampos:
				for(ConfiguracionCampo config:configuracionCampos){
					String descripcion=config.getDescripcion();
					//Con que al menos un campo de configuracion se encuentre en esta seccion entonces si requiere ser considerado al guardar.
					if((descripcion!=null && !descripcion.isEmpty()) && descripcion.trim().equals(campoMapeado.toString().trim())){
						seccionRequiereValidacion=true;
						configuracionDelCampo=config;
						break cicloConfiguracionCampos;
					}
				}
				//Si se encontro la configuracion del campo para el atributo que tiene la anotacion.
				if(configuracionDelCampo!=null){//Entonces se checa si su valor es requerido 
					if(configuracionDelCampo.esObligatorio()){
						Object valorCampoMapeado=ReflectionUtils.getValue(cliente, campo);
						if(valorCampoMapeado==null ||(valorCampoMapeado.toString().isEmpty())){
							errorList.put(modelView+campo.getName(),((errorMessage!=null)?errorMessage.toString():""));
						}
					}
				}
			}
			//si la seccion no requiere validacion entonces la lista de errores es nula y si es nula, no se considerará para que se guarde en el facade de cliente
			if(!seccionRequiereValidacion){
				errorList=null;
			}
		}
		return errorList;
	}
	/**
	 * Obtiene la lista de los nombres de los campos que serán marcados como obligatorios, es util para saber que campos deben de ser capturados
	 * y se desplegaran en el modelView del cliente.
	 * @param cliente
	 * @param configuracionCampos
	 * @return
	 */
	public static List<String> obtenerCamposObligatorios(ClienteGenericoDTO cliente,List<ConfiguracionCampo> configuracionCampos){
		List<String> fieldsOfModelView=new ArrayList<String>();
		List<Field> camposCliente=getAllMappedFields(cliente);
		if(camposCliente!=null && !camposCliente.isEmpty() && configuracionCampos!=null && !configuracionCampos.isEmpty()){
			cicloCamposConAnotacion:
			for(Field campo:camposCliente){
				Object campoMapeado=ReflectionUtils.getAttributeValueFromAnnotation(campo, ClientBeanValidator.class,"mappingLabel");
				Object model=ReflectionUtils.getAttributeValueFromAnnotation(campo, ClientBeanValidator.class,"modelView");
				String modelView=(model!=null)?model.toString()+".":"";
				//Si no encuentra el campo a mapear se pasa al siguiente
				if(campoMapeado==null){
					//TODO continuar?
					continue cicloCamposConAnotacion;
				}
				cicloConfiguracionCampos:
				for(ConfiguracionCampo config:configuracionCampos){
					String descripcion=config.getDescripcion();
					//Con que al menos un campo de configuracion se encuentre en esta seccion entonces si requiere ser considerado al guardar.
					if((descripcion!=null && !descripcion.isEmpty()) && descripcion.trim().equals(campoMapeado.toString().trim())){
						//Si se encontro la configuracion del campo para el atributo que tiene la anotacion.
						if(config.esObligatorio()){
							String fieldName=modelView+campo.getName();
							fieldsOfModelView.add(fieldName);
						}
						break cicloConfiguracionCampos;
					}
				}
			}
		}
		return fieldsOfModelView;
	}
	
	public static void main(String... a){
		List<Field> fields;
		ClientBeanValidatorProcessor ins=new ClientBeanValidatorProcessor();
		ClienteGenericoDTO cliente=new ClienteGenericoDTO();
		cliente.setClaveSectorFinanciero("SE");
		//cliente.setIdGiro(new BigDecimal(200));
		//cliente.setApellidoPaterno("Herrera");
		cliente.setApellidoMaterno("Silva");
		cliente.setNombre("Victor Manuel");
		List<ConfiguracionCampo> configuracionCampos=new ArrayList<ConfiguracionCampo>();
		ConfiguracionCampo campo1=new ConfiguracionCampo();
		campo1.setDescripcion("Tipo de persona");
		campo1.setValorDefault((short)1);
		configuracionCampos.add(campo1);
		ConfiguracionCampo campo2=new ConfiguracionCampo();
		campo2.setDescripcion("Apellido Materno");
		campo2.setValorDefault((short)0);
		configuracionCampos.add(campo2);
		ConfiguracionCampo campo3=new ConfiguracionCampo();
		campo3.setDescripcion("Nombre");
		campo3.setValorDefault((short)1);
		configuracionCampos.add(campo3);
		ConfiguracionCampo campo4=new ConfiguracionCampo();
		campo4.setDescripcion("Apellido Paterno");
		campo4.setValorDefault((short)1);
		configuracionCampos.add(campo4);
		ConfiguracionCampo campo5=new ConfiguracionCampo();
		campo1.setDescripcion("Giro");
		campo1.setValorDefault((short)1);
		configuracionCampos.add(campo5);
		fields = ReflectionUtils.getFieldsWithAnnotation(cliente.getClass(),ClientBeanValidator.class);
		if(fields!=null && !fields.isEmpty()){
			System.out.println("Si hay campos, se imprimiran sus valores...");
			for(Field field:fields){
				Object value=ReflectionUtils.getValue(cliente, field);
				//System.out.println("Atributo:"+field.getName()+"|Valor:"+value);
				value=ReflectionUtils.getAttributeValueFromAnnotation(field, ClientBeanValidator.class, "mappingLabel");
				//System.out.println("Atributo de anotacion:mappingLabel|Valor:"+value);
			}
		}
		Map<String,Map<String,String>> errores;
		try {
			errores = ins.validarClienteGenerico(cliente, configuracionCampos);
			if(errores!=null && !errores.isEmpty()){
				System.out.println("Si hay errores:"+errores);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
