package mx.com.afirme.midas2.dto;

import mx.com.afirme.midas.base.CacheableDTO;

public class TipoVehiculoComboDTO extends CacheableDTO {
	private static final long serialVersionUID = 1L;

	private String codigoTipoVehiculo;
	private String descripcionTipoVehiculo;
	
	public String getCodigoTipoVehiculo() {
		return codigoTipoVehiculo;
	}

	public void setCodigoTipoVehiculo(String codigoTipoVehiculo) {
		this.codigoTipoVehiculo = codigoTipoVehiculo;
	}

	public String getDescripcionTipoVehiculo() {
		return descripcionTipoVehiculo;
	}

	public void setDescripcionTipoVehiculo(String descripcionTipoVehiculo) {
		this.descripcionTipoVehiculo = descripcionTipoVehiculo;
	}

	@Override
	public Object getId() {
		return getCodigoTipoVehiculo();
	}

	@Override
	public String getDescription() {
		return getDescripcionTipoVehiculo();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((codigoTipoVehiculo == null) ? 0 : codigoTipoVehiculo
						.hashCode());
		result = prime
				* result
				+ ((descripcionTipoVehiculo == null) ? 0
						: descripcionTipoVehiculo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoVehiculoComboDTO other = (TipoVehiculoComboDTO) obj;
		if (codigoTipoVehiculo == null) {
			if (other.codigoTipoVehiculo != null)
				return false;
		} else if (!codigoTipoVehiculo.equals(other.codigoTipoVehiculo))
			return false;
		if (descripcionTipoVehiculo == null) {
			if (other.descripcionTipoVehiculo != null)
				return false;
		} else if (!descripcionTipoVehiculo
				.equals(other.descripcionTipoVehiculo))
			return false;
		return true;
	}

//	@Override
//	public boolean equals(Object object) {
//		if(object != null && ! (object instanceof TipoVehiculoComboDTO))
//			return false;
//		TipoVehiculoCombo
//		if (((TipoVehiculoComboDTO)object).getCodigoTipoVehiculo() != null){
//			return getCodigoTipoVehiculo().equals(((ClaveGrupoComboDTO)object).getIdGrupo());
//		}
//		return false;
//	}

}
