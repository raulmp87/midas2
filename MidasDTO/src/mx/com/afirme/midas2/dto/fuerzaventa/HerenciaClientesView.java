package mx.com.afirme.midas2.dto.fuerzaventa;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


@Entity
public class HerenciaClientesView implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2050359975668617977L;
	private Long idHerencia;
	private String nombreCompletoAgente;
	private String descripcionTipoCedula;
	private String nombrePromotoria;
	private String descripcionMotivoCesion;
	private Date fechaCesion;
	private String fechaString;
	
	@Id
	public Long getIdHerencia() {
		return idHerencia;
	}
	public void setIdHerencia(Long idHerencia) {
		this.idHerencia = idHerencia;
	}
	public String getNombreCompletoAgente() {
		return nombreCompletoAgente;
	}
	public void setNombreCompletoAgente(String nombreCompletoAgente) {
		this.nombreCompletoAgente = nombreCompletoAgente;
	}
	public String getDescripcionTipoCedula() {
		return descripcionTipoCedula;
	}
	public void setDescripcionTipoCedula(String descripcionTipoCedula) {
		this.descripcionTipoCedula = descripcionTipoCedula;
	}
	public String getNombrePromotoria() {
		return nombrePromotoria;
	}
	public void setNombrePromotoria(String nombrePromotoria) {
		this.nombrePromotoria = nombrePromotoria;
	}
	
	public String getDescripcionMotivoCesion() {
		return descripcionMotivoCesion;
	}
	public void setDescripcionMotivoCesion(String descripcionMotivoCesion) {
		this.descripcionMotivoCesion = descripcionMotivoCesion;
	}
	
	@Temporal(TemporalType.DATE)
	public Date getFechaCesion() {
		return fechaCesion;
	}
	public void setFechaCesion(Date fechaCesion) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");    	
		this.fechaString=sdf.format(fechaCesion);
	}
	
	@Transient
	public String getFechaString() {
		return fechaString;
	}

	public void setFechaString(String fechaString) {
		this.fechaString = fechaString;
	}
	


}
