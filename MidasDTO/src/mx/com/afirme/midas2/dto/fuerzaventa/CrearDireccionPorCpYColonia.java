package mx.com.afirme.midas2.dto.fuerzaventa;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CrearDireccionPorCpYColonia implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2195784394222296658L;
	private String idPais;
	private String idEstado;
	private String idCiudad;
	private String idcolonia;
	private String cp;
	
	@Id
	public String getIdPais() {
		return idPais;
	}
	public void setIdPais(String idPais) {
		this.idPais = idPais;
	}
	public String getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}
	public String getIdCiudad() {
		return idCiudad;
	}
	public void setIdCiudad(String idCiudad) {
		this.idCiudad = idCiudad;
	}
	public String getIdcolonia() {
		return idcolonia;
	}
	public void setIdcolonia(String idcolonia) {
		this.idcolonia = idcolonia;
	}
	public String getCp() {
		return cp;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}
	
}
