package mx.com.afirme.midas2.dto.fuerzaventa;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.ReadOnly;

/**
 * <b>Nota:</b> Revisar y aplicar mismo workaround empleado en la clase mx.com.afirme.midas2.dto.impresiones.DatosLineasDeNegocioDTO, 
 * en concreto si se usa el @SqlResultSetMapping productoXLineaVentaView para mapear un nativeQuery no olvidar incluir el HINT mencionado 
 * @see mx.com.afirme.midas2.dto.impresiones.DatosLineasDeNegocioDTO
 *
 */
@Entity
@ReadOnly
@Cacheable(false)
@SqlResultSetMapping(name="productoXLineaVentaView",entities={
		@EntityResult(entityClass=GenericaAgentesView.class,fields={
			@FieldResult(name="id",column="id"),
			@FieldResult(name="valor",column="descripcion")
		})
	}
)
public class GenericaAgentesView implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7774333223851101138L;
	private Long id;
	private String valor;
	private Integer checado;
	
	public GenericaAgentesView(){}	
	@Id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
	@Transient
	public Integer getChecado() {
		return checado;
	}
	public void setChecado(Integer checado) {
		this.checado = checado;
	}
}
