package mx.com.afirme.midas2.dto.fuerzaventa.mediciones;

import java.io.InputStream;
import java.io.Serializable;

public class Reporte implements Serializable {

	private static final long serialVersionUID = 4163423501540937846L;
	
	public enum Tipo {
		ULTIMO_REPORTE_SEMANAL(1), 
		ULTIMO_REPORTE_MENSUAL(2), 
		PENULTIMO_REPORTE_MENSUAL(3);
		
		private final Integer value;
		
		private Tipo(Integer value) {
			
			this.value = value;
			
		}
		
		public Integer value() {
			return this.value;
		}
		
	}
	
	
	private String fileName;
	private String contentType;
	private String corte; //representa la fecha de corte en un formato YYYYMMDD
	private InputStream inputStream;
		
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getCorte() {
		return corte;
	}
	public void setCorte(String corte) {
		this.corte = corte;
	}
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	
	public InputStream getInputStream() {
		return inputStream;
	}
		
	
}

