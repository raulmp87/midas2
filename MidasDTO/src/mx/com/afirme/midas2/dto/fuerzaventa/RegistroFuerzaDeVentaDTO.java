package mx.com.afirme.midas2.dto.fuerzaventa;

import java.io.Serializable;

public class RegistroFuerzaDeVentaDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5133766386741025767L;

	private Object id;
	
	private String description;

	public Object getId() {
		return id;
	}

	public void setId(Object id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}	

}
