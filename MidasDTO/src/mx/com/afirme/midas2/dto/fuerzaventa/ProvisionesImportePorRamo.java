package mx.com.afirme.midas2.dto.fuerzaventa;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class ProvisionesImportePorRamo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Long idRamo;
	private String descripcion;
	private Long idProvision;
	private Long idBono;
	private Double importe;
	
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getIdRamo() {
		return idRamo;
	}
	public void setIdRamo(Long idRamo) {
		this.idRamo = idRamo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Double getImporte() {
		return importe;
	}
	public void setImporte(Double importe) {
		this.importe = importe;
	}
	public Long getIdProvision() {
		return idProvision;
	}
	public void setIdProvision(Long idProvision) {
		this.idProvision = idProvision;
	}
	public Long getIdBono() {
		return idBono;
	}
	public void setIdBono(Long idBono) {
		this.idBono = idBono;
	}
	
	
}
