package mx.com.afirme.midas2.dto.fuerzaventa;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
/**
 * Interface para replicar los datos de Centro de Operacion de Midas a Seycos
 * @author vmhersil
 *
 */
@Local
public interface CentroOperacionSeycosFacadeRemote {
	/**
	 * Metodo para replicar el centro de operacion a Seycos
	 * @param centroOperacion
	 * @return
	 */
	public Long save(CentroOperacion centroOperacion) throws Exception;
	
	public void unsubscribe(CentroOperacion centroOperacion) throws Exception;
}
