package mx.com.afirme.midas2.dto.fuerzaventa;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
/**
 * Clase que representa una vista para presentar los datos del agente en el grid de agentes por autorizar
 * @author vmhersil
 *
 */
@Entity
@SqlResultSetMapping(name="agenteView",entities={
		@EntityResult(entityClass=AgenteView.class,fields={
			@FieldResult(name="id",column="id"),
			@FieldResult(name="idAgente",column="idAgente"),
			@FieldResult(name="diasTranscurridos",column="diasTranscurridos"),
			@FieldResult(name="conductoAlta",column="conductoAlta"),
			@FieldResult(name="perfil",column="perfil"),
			@FieldResult(name="promotoria",column="promotoria"),
			@FieldResult(name="nombreCompleto",column="nombreCompleto"),
			@FieldResult(name="tipoCedulaAgente",column="tipoCedulaAgente"),
			@FieldResult(name="ejecutivo",column="ejecutivo"),
			@FieldResult(name="tipoSituacion",column="tipoSituacion"),
			@FieldResult(name="codigoRfc",column="codigoRfc"),
			@FieldResult(name="codigoCurp",column="codigoCurp"),
			@FieldResult(name="gerencia",column="gerencia"),
			@FieldResult(name="prioridad",column="prioridad"),
			@FieldResult(name="codigousuario",column="codigousuario")			
		})
	}
)
public class AgenteView implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3732789591057174423L;
	private String conductoAlta;
	private String perfil;
	private BigDecimal diasTranscurridos;
	private Long id;
	private Long idAgente;
	private String nombreCompleto;
	private String promotoria;
	private String tipoCedulaAgente;
	private String ejecutivo;
	private String tipoSituacion;
	private String codigoRfc;
	private String codigoCurp;
	private String gerencia;
	private String prioridad;
	private String codigoUsuario;
	
	public AgenteView(){}
	
	public String getConductoAlta() {
		return conductoAlta;
	}
	public void setConductoAlta(String conductoAlta) {
		this.conductoAlta = conductoAlta;
	}
	public String getPerfil() {
		return perfil;
	}
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	public BigDecimal getDiasTranscurridos() {
		return diasTranscurridos;
	}
	public void setDiasTranscurridos(BigDecimal diasTranscurridos) {
		this.diasTranscurridos = diasTranscurridos;
	}
	@Id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public String getPromotoria() {
		return promotoria;
	}

	public void setPromotoria(String promotoria) {
		this.promotoria = promotoria;
	}
	
	public String getTipoCedulaAgente() {
		return tipoCedulaAgente;
	}

	public void setTipoCedulaAgente(String tipoCedulaAgente) {
		this.tipoCedulaAgente =tipoCedulaAgente;
	}

	public String getEjecutivo() {
		return ejecutivo;
	}

	public void setEjecutivo(String ejecutivo) {
		this.ejecutivo = ejecutivo;
	}

	public Long getIdAgente() {
		return idAgente;
	}

	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}

	public String getTipoSituacion() {
		return tipoSituacion;
	}

	public void setTipoSituacion(String tipoSituacion) {
		this.tipoSituacion = tipoSituacion;
	}

	public String getCodigoRfc() {
		return codigoRfc;
	}

	public void setCodigoRfc(String codigoRfc) {
		this.codigoRfc = codigoRfc;
	}

	public String getCodigoCurp() {
		return codigoCurp;
	}

	public void setCodigoCurp(String codigoCurp) {
		this.codigoCurp = codigoCurp;
	}

	public String getGerencia() {
		return gerencia;
	}

	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}

	public String getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}
	
	public String getCodigoUsuario() {
		return codigoUsuario;
	}
	
	public void setCodigoUsuario(String codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}
	
}
