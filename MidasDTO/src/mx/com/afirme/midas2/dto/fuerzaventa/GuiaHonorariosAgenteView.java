package mx.com.afirme.midas2.dto.fuerzaventa;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@SqlResultSetMapping(name="guiaHonorariosAgenteView",entities={
		@EntityResult(entityClass=GuiaHonorariosAgenteView.class,fields={
			@FieldResult(name="id",column="id"),
			@FieldResult(name="clave",column="clave"),
			@FieldResult(name="nombreCompleto",column="nombreCompleto"),
			@FieldResult(name="nombrePromotoria",column="nombrePromotoria"),
			@FieldResult(name="numeroCheque",column="numeroCheque"),
			@FieldResult(name="fechaAplicacion",column="fechaAplicacion"),
			@FieldResult(name="solicitudChequeId",column="solicitudChequeId")
		})
	}
)
public class GuiaHonorariosAgenteView  implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long idAgente;
	private Long clave;
	private String nombreCompleto;
	private String nombrePromotoria;
	private BigDecimal solicitudChequeId;
	private String numeroCheque;
	private Date fechaAplicacion;
	
public GuiaHonorariosAgenteView() { }
	
	public GuiaHonorariosAgenteView(GuiaHonorariosAgenteView guia) {
		this.id = guia.id;
		this.idAgente = guia.idAgente;
		this.clave = guia.clave;
		this.nombreCompleto = guia.nombreCompleto;
		this.nombrePromotoria = guia.nombrePromotoria;
		this.solicitudChequeId = guia.solicitudChequeId;
		this.numeroCheque = guia.numeroCheque;
		this.fechaAplicacion = guia.fechaAplicacion;
	}
	
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdAgente() {
		return idAgente;
	}

	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
	public Long getClave() {
		return clave;
	}
	public void setClave(Long clave) {
		this.clave = clave;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	public String getNombrePromotoria() {
		return nombrePromotoria;
	}
	public void setNombrePromotoria(String nombrePromotoria) {
		this.nombrePromotoria = nombrePromotoria;
	}
	public BigDecimal getSolicitudChequeId() {
		return solicitudChequeId;
	}
	public void setSolicitudChequeId(BigDecimal solicitudChequeId) {
		this.solicitudChequeId = solicitudChequeId;
	}
	public String getNumeroCheque() {
		return numeroCheque;
	}
	public void setNumeroCheque(String numeroCheque) {
		this.numeroCheque = numeroCheque;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaAplicacion() {
		return fechaAplicacion;
	}
	public void setFechaAplicacion(Date fechaAplicacion) {
		this.fechaAplicacion = fechaAplicacion;
	}
}
