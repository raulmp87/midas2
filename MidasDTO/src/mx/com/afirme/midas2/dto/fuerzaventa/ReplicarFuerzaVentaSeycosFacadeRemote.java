package mx.com.afirme.midas2.dto.fuerzaventa;

import javax.ejb.Local;
/**
 * Interface que permite guardar una fuerza de venta(centro operacion,gerencia,ejecutivo,promotoria)
 * y replicarlo en el esquema de SEYCOS segun sea el objeto de fuerza de venta.
 * @author vmhersil
 *
 */
@Local
public interface ReplicarFuerzaVentaSeycosFacadeRemote {
	public static enum TipoAccionFuerzaVenta{
		GUARDAR,INACTIVAR
	}
	/**
	 * Metodo que registra una fuerza de venta en el esquema de SEYCOS. 
	 * Regresa la primary key de la entidad guardada.
	 * @param fuerzaVenta
	 * @return
	 */
	public Long replicarFuerzaVenta(Object fuerzaVenta,TipoAccionFuerzaVenta tipoAccion)throws Exception;
}
