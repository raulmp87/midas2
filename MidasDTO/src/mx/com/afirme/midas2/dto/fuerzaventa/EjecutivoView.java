package mx.com.afirme.midas2.dto.fuerzaventa;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class EjecutivoView implements Serializable{
	
	private static final long serialVersionUID = 1L;	
	
	private Long	  id;
	private Long      idEjecutivo;
	private Long  	  gerencia_id;
	private String 	  descripcion;
	private Long      idPersona;
	private String    nombreCompleto;
	private Long 	  idTipoEjecutivo;
	private String 	  tipoEjecutivo;
	private Long	  claveEstatus;
	private Integer   checado;
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}	
	public Long getIdEjecutivo() {
		return idEjecutivo;
	}
	
	public void setIdEjecutivo(Long idEjecutivo) {
		this.idEjecutivo = idEjecutivo;
	}
	public Long getGerencia_id() {
		return gerencia_id;
	}
	public void setGerencia_id(Long gerencia_id) {
		this.gerencia_id = gerencia_id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Long getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	public Long getIdTipoEjecutivo() {
		return idTipoEjecutivo;
	}
	public void setIdTipoEjecutivo(Long idTipoEjecutivo) {
		this.idTipoEjecutivo = idTipoEjecutivo;
	}
	public String getTipoEjecutivo() {
		return tipoEjecutivo;
	}
	public void setTipoEjecutivo(String tipoEjecutivo) {
		this.tipoEjecutivo = tipoEjecutivo;
	}
	public Long getClaveEstatus() {
		return claveEstatus;
	}
	public void setClaveEstatus(Long claveEstatus) {
		this.claveEstatus = claveEstatus;
	}
	@Transient
	public Integer getChecado() {
		return checado;
	}
	public void setChecado(Integer checado) {
		this.checado = checado;
	}
	
	
}
