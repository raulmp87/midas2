package mx.com.afirme.midas2.dto.fuerzaventa;

import javax.ejb.Local;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
/**
 * Interface para replicar los datos de promotoria en Seycos. 
 * @author vmhersil
 *
 */
@Local
public interface PromotoriaSeycosFacadeRemote {
	public Long save(Promotoria promotoria) throws Exception;
	
	public void unsubscribe(Promotoria promotoria) throws Exception;
}
