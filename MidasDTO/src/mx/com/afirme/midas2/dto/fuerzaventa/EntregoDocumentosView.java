package mx.com.afirme.midas2.dto.fuerzaventa;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class EntregoDocumentosView implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
		private Long id;
		private String valor;
		private Integer checado;
		
	
		@Id
		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getValor() {
			return valor;
		}

		public void setValor(String valor) {
			this.valor = valor;
		}		
		
		public Integer getChecado() {
			return checado;
		}
		public void setChecado(Integer checado) {
			this.checado = checado;
		}

}
