package mx.com.afirme.midas2.dto.fuerzaventa;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ClienteAgenteView implements Serializable{

	private static final long serialVersionUID = 1L;
	private Long id;
	private Long idAgente;
	private Long idCliente;
	private String claveEstatus;
	private Long idHerencia;
	private String nombreCompleto;
	
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getIdAgente() {
		return idAgente;
	}
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
	public Long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	public String getClaveEstatus() {
		return claveEstatus;
	}
	public void setClaveEstatus(String claveEstatus) {
		this.claveEstatus = claveEstatus;
	}
	public Long getIdHerencia() {
		return idHerencia;
	}
	public void setIdHerencia(Long idHerencia) {
		this.idHerencia = idHerencia;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	
	
}
