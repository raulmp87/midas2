package mx.com.afirme.midas2.dto.fuerzaventa;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
/**
 * Interface de replica de gerencias de Midas-Seycos
 * @author vmhersil
 *
 */
@Local
public interface GerenciaSeycosFacadeRemote {
	/**
	 * Metodo para replicar la gerencia de Midas a Seycos
	 * @param gerencia
	 * @return
	 */
	public Long save(Gerencia gerencia)throws Exception;
	
	public void unsubscribe(Gerencia gerencia) throws Exception;
}
