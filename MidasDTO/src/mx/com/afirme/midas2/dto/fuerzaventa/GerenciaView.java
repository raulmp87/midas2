package mx.com.afirme.midas2.dto.fuerzaventa;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class GerenciaView implements Serializable{

	private static final long serialVersionUID = -8543156754435869948L;
	private Long 	id;
	private Long 	idGerencia;
	private String 	descripcion;
	private Long 	idPersona;
	private String 	nombreGerente;
	private Long 	claveEstatus;
	private String  descripcionCentroOperacion;
	private Long	idCentroOperacion;
	private Integer checado;
	
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdGerencia() {
		return idGerencia;
	}
	public void setIdGerencia(Long idGerencia) {
		this.idGerencia = idGerencia;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Long getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}
	public String getNombreGerente() {
		return nombreGerente;
	}
	public void setNombreGerente(String nombreGerente) {
		this.nombreGerente = nombreGerente;
	}
	public Long getClaveEstatus() {
		return claveEstatus;
	}
	public void setClaveEstatus(Long claveEstatus) {
		this.claveEstatus = claveEstatus;
	}
	public String getDescripcionCentroOperacion() {
		return descripcionCentroOperacion;
	}
	public void setDescripcionCentroOperacion(String descripcionCentroOperacion) {
		this.descripcionCentroOperacion = descripcionCentroOperacion;
	}
	public Long getIdCentroOperacion() {
		return idCentroOperacion;
	}
	public void setIdCentroOperacion(Long idCentroOperacion) {
		this.idCentroOperacion = idCentroOperacion;
	}
	@Transient
	public Integer getChecado() {
		return checado;
	}
	public void setChecado(Integer checado) {
		this.checado = checado;
	}	

}
