package mx.com.afirme.midas2.dto.fuerzaventa;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
@Entity
@SqlResultSetMapping(name="promotoriaView",entities={
		@EntityResult(entityClass=PromotoriaView.class,fields={
			@FieldResult(name="id",column="id"),
			@FieldResult(name="idPromotoria",column="idPromotoria"),
			@FieldResult(name="descripcion",column="descripcion"),
			@FieldResult(name="idTipoPromotoria",column="idTipoPromotoria"),
			@FieldResult(name="tipoPromotoria",column="nombreTipoPromotoria"),
			@FieldResult(name="idAgrupadorPromotoria",column="idAgrupadorPromotoria"),
			@FieldResult(name="agrupadorPromotoria",column="nombreAgrupador"),
			@FieldResult(name="idEjecutivo",column="ejecutivo_id"),
			@FieldResult(name="nombreEjecutivo",column="nombreEjecutivo"),
			@FieldResult(name="idAgentePromotor",column="agentePromotor_id"),
			@FieldResult(name="nombreAgentePromotor",column="nombreAgente"),
			@FieldResult(name="fechaAlta",column="fechaHoraActualizacion"),
			@FieldResult(name="claveEstatus",column="claveEstatus")
		})
	}
)
public class PromotoriaView implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4454364615338132730L;
	private Long id;
	private Long idPromotoria;
	private String descripcion;
	private Long idTipoPromotoria;
	private String tipoPromotoria;
	private Long idAgrupadorPromotoria;
	private String agrupadorPromotoria;
	private Long idEjecutivo;
	private String nombreEjecutivo;
	private Long idAgentePromotor;
	private String nombreAgentePromotor;
	private Date fechaAlta;
	private String fechaAltaString;
	private Integer claveEstatus;
	private String correoElectronico;
	private Date fechaInicio;
	private Date fechaFin;
	private String personaResponsable;
	private Integer checado;
	
	public PromotoriaView() {}
	@Id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdPromotoria() {
		return idPromotoria;
	}
	public void setIdPromotoria(Long idPromotoria) {
		this.idPromotoria = idPromotoria;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Long getIdTipoPromotoria() {
		return idTipoPromotoria;
	}
	public void setIdTipoPromotoria(Long idTipoPromotoria) {
		this.idTipoPromotoria = idTipoPromotoria;
	}
	public String getTipoPromotoria() {
		return tipoPromotoria;
	}
	public void setTipoPromotoria(String tipoPromotoria) {
		this.tipoPromotoria = tipoPromotoria;
	}
	public Long getIdAgrupadorPromotoria() {
		return idAgrupadorPromotoria;
	}
	public void setIdAgrupadorPromotoria(Long idAgrupadorPromotoria) {
		this.idAgrupadorPromotoria = idAgrupadorPromotoria;
	}
	public String getAgrupadorPromotoria() {
		return agrupadorPromotoria;
	}
	public void setAgrupadorPromotoria(String agrupadorPromotoria) {
		this.agrupadorPromotoria = agrupadorPromotoria;
	}
	public Long getIdEjecutivo() {
		return idEjecutivo;
	}
	public void setIdEjecutivo(Long idEjecutivo) {
		this.idEjecutivo = idEjecutivo;
	}
	public String getNombreEjecutivo() {
		return nombreEjecutivo;
	}
	public void setNombreEjecutivo(String nombreEjecutivo) {
		this.nombreEjecutivo = nombreEjecutivo;
	}
	public Long getIdAgentePromotor() {
		return idAgentePromotor;
	}
	public void setIdAgentePromotor(Long idAgentePromotor) {
		this.idAgentePromotor = idAgentePromotor;
	}
	public String getNombreAgentePromotor() {
		return nombreAgentePromotor;
	}
	public void setNombreAgentePromotor(String nombreAgentePromotor) {
		this.nombreAgentePromotor = nombreAgentePromotor;
	}
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(Date fechaAlta) {
		DateFormat format=new SimpleDateFormat("dd/MM/yyyy");
		this.fechaAltaString=(fechaAlta!=null)?format.format(fechaAlta):null;
		this.fechaAlta = fechaAlta;
	}
	public Integer getClaveEstatus() {
		return claveEstatus;
	}
	public void setClaveEstatus(Integer claveEstatus) {
		this.claveEstatus = claveEstatus;
	}
	public String getCorreoElectronico() {
		return correoElectronico;
	}
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getPersonaResponsable() {
		return personaResponsable;
	}
	public void setPersonaResponsable(String personaResponsable) {
		this.personaResponsable = personaResponsable;
	}
	@Transient
	public String getFechaAltaString() {
		return fechaAltaString;
	}
	public void setFechaAltaString(String fechaAltaString) {
		this.fechaAltaString = fechaAltaString;
	}
	@Transient
	public Integer getChecado() {
		return checado;
	}
	public void setChecado(Integer checado) {
		this.checado = checado;
	}
	
}
