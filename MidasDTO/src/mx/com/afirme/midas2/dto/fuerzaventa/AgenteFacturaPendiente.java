package mx.com.afirme.midas2.dto.fuerzaventa;

import java.io.Serializable;
import java.math.BigDecimal;

public class AgenteFacturaPendiente implements Serializable {
	
	
	private static final long serialVersionUID = 1805372656569219420L;

	
	private Long idAgente;
	private Long anioMes;
	private BigDecimal solicitudChequeId;
	
	public Long getIdAgente() {
		return idAgente;
	}
	
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
	
	public Long getAnioMes() {
		return anioMes;
	}
	
	public void setAnioMes(Long anioMes) {
		this.anioMes = anioMes;
	}

	public BigDecimal getSolicitudChequeId() {
		return solicitudChequeId;
	}

	public void setSolicitudChequeId(BigDecimal solicitudChequeId) {
		this.solicitudChequeId = solicitudChequeId;
	}
	
}

