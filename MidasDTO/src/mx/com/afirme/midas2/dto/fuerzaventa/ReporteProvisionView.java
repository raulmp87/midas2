package mx.com.afirme.midas2.dto.fuerzaventa;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class ReporteProvisionView implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Long idGerencia;
	private String nombreGerencia;
	private String nombreGrupo;
	private Long claveAgente;
	private String nombreAgente;
	private String nombreEjecutivo;
	private String tipoAgente;
	private Long sec;
	private String nombreRamo;
	private Double primaEmitida;
	private Double primaPagada;
	private Double primaDevengada;
	private Double reclama;
	private Double gastos;
	private Double salv_recup;
	private Double costoSiniestro;
	private Double pcteSiniestro;
	private Double monto;
	private Double montoAProvisionar;
	
	public static final String PLANTILLA_NAME = "midas.agente.reporte.agente.reporteProvision.archivo.nombre";
		
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdGerencia() {
		return idGerencia;
	}
	public void setIdGerencia(Long idGerencia) {
		this.idGerencia = idGerencia;
	}
	public String getNombreGerencia() {
		return nombreGerencia;
	}
	public void setNombreGerencia(String nombreGerencia) {
		this.nombreGerencia = nombreGerencia;
	}
	public String getNombreGrupo() {
		return nombreGrupo;
	}
	public void setNombreGrupo(String nombreGrupo) {
		this.nombreGrupo = nombreGrupo;
	}
	public Long getClaveAgente() {
		return claveAgente;
	}
	public void setClaveAgente(Long claveAgente) {
		this.claveAgente = claveAgente;
	}
	public String getNombreAgente() {
		return nombreAgente;
	}
	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}
	public String getNombreEjecutivo() {
		return nombreEjecutivo;
	}
	public void setNombreEjecutivo(String nombreEjecutivo) {
		this.nombreEjecutivo = nombreEjecutivo;
	}
	public String getTipoAgente() {
		return tipoAgente;
	}
	public void setTipoAgente(String tipoAgente) {
		this.tipoAgente = tipoAgente;
	}
	public Long getSec() {
		return sec;
	}
	public void setSec(Long sec) {
		this.sec = sec;
	}
	public String getNombreRamo() {
		return nombreRamo;
	}
	public void setNombreRamo(String nombreRamo) {
		this.nombreRamo = nombreRamo;
	}
	public Double getPrimaEmitida() {
		return primaEmitida;
	}
	public void setPrimaEmitida(Double primaEmitida) {
		this.primaEmitida = primaEmitida;
	}
	public Double getPrimaPagada() {
		return primaPagada;
	}
	public void setPrimaPagada(Double primaPagada) {
		this.primaPagada = primaPagada;
	}
	public Double getPrimaDevengada() {
		return primaDevengada;
	}
	public void setPrimaDevengada(Double primaDevengada) {
		this.primaDevengada = primaDevengada;
	}
	public Double getReclama() {
		return reclama;
	}
	public void setReclama(Double reclama) {
		this.reclama = reclama;
	}
	public Double getGastos() {
		return gastos;
	}
	public void setGastos(Double gastos) {
		this.gastos = gastos;
	}
	public Double getSalv_recup() {
		return salv_recup;
	}
	public void setSalv_recup(Double salv_recup) {
		this.salv_recup = salv_recup;
	}
	public Double getCostoSiniestro() {
		return costoSiniestro;
	}
	public void setCostoSiniestro(Double costoSiniestro) {
		this.costoSiniestro = costoSiniestro;
	}
	public Double getPcteSiniestro() {
		return pcteSiniestro;
	}
	public void setPcteSiniestro(Double pcteSiniestro) {
		this.pcteSiniestro = pcteSiniestro;
	}
	public Double getMonto() {
		return monto;
	}
	public void setMonto(Double monto) {
		this.monto = monto;
	}
	public Double getMontoAProvisionar() {
		return montoAProvisionar;
	}
	public void setMontoAProvisionar(Double montoAProvisionar) {
		this.montoAProvisionar = montoAProvisionar;
	}
	
	
}

