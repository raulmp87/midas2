package mx.com.afirme.midas2.dto.fuerzaventa;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class CentroOperacionView implements Serializable {

	private static final long serialVersionUID = -6295990373029559993L;
	private Long id;
	private String descripcion;
	private Long idPersonaResponsable;
	private Long claveEstatus;
	private String nombreCompleto;
	private Integer checado;
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Long getIdPersonaResponsable() {
		return idPersonaResponsable;
	}
	public void setIdPersonaResponsable(Long idPersonaResponsable) {
		this.idPersonaResponsable = idPersonaResponsable;
	}
	public Long getClaveEstatus() {
		return claveEstatus;
	}
	public void setClaveEstatus(Long claveEstatus) {
		this.claveEstatus = claveEstatus;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	@Transient
	public Integer getChecado() {
		return checado;
	}
	public void setChecado(Integer checado) {
		this.checado = checado;
	}
	

}