package mx.com.afirme.midas2.dto.fuerzaventa;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
@Entity
public class ProvisionesBonoAgentesView implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String nombreProvision;
	private Date fechaProvision;
	private Double importeProvision;
	private String modoEjecucion;
	private String estatusProvision;
	private String fechaProvisionString;
	
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombreProvision() {
		return nombreProvision;
	}
	public void setNombreProvision(String nombreProvision) {
		this.nombreProvision = nombreProvision;
	}
	
	@Temporal(TemporalType.DATE)
	public Date getFechaProvision() {
		return fechaProvision;
	}
	public void setFechaProvision(Date fechaProvision) {
		this.fechaProvision = fechaProvision;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
		this.fechaProvisionString = sdf.format(fechaProvision);
	}
	public Double getImporteProvision() {
		return importeProvision;
	}
	public void setImporteProvision(Double importeProvision) {
		this.importeProvision = importeProvision;
	}
	public String getModoEjecucion() {
		return modoEjecucion;
	}
	public void setModoEjecucion(String modoEjecucion) {
		this.modoEjecucion = modoEjecucion;
	}
	public String getEstatusProvision() {
		return estatusProvision;
	}
	public void setEstatusProvision(String estatusProvision) {
		this.estatusProvision = estatusProvision;
	}
	
	@Transient
	public String getFechaProvisionString() {
		return fechaProvisionString;
	}
	public void setFechaProvisionString(String fechaProvisionString) {
		this.fechaProvisionString = fechaProvisionString;
	}
	
	
}
