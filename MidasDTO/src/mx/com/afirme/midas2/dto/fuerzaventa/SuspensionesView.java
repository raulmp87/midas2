package mx.com.afirme.midas2.dto.fuerzaventa;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
public class SuspensionesView implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Long idAgente;
	private Date fechaSolicitud;
	private String fechaSolicitudString;
	private String nombreAgente;
	private String nombreSolicitante;
	private String estatusMovimiento;
	private String estatusAgente;
	
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdAgente() {
		return idAgente;
	}
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
	
	public String getNombreAgente() {
		return nombreAgente;
	}
	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}
	public String getNombreSolicitante() {
		return nombreSolicitante;
	}
	public void setNombreSolicitante(String nombreSolicitante) {
		this.nombreSolicitante = nombreSolicitante;
	}
	public String getEstatusMovimiento() {
		return estatusMovimiento;
	}
	public void setEstatusMovimiento(String estatusMovimiento) {
		this.estatusMovimiento = estatusMovimiento;
	}
	public String getEstatusAgente() {
		return estatusAgente;
	}
	public void setEstatusAgente(String estatusAgente) {
		this.estatusAgente = estatusAgente;
	}
	
	@Temporal(TemporalType.DATE)
	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}
	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
		this.fechaSolicitudString = sdf.format(fechaSolicitud);
	}
	
	@Transient
	public String getFechaSolicitudString() {
		return fechaSolicitudString;
	}

	public void setFechaSolicitudString(String fechaSolicitudString) {
		this.fechaSolicitudString = fechaSolicitudString;
	}
	

}
