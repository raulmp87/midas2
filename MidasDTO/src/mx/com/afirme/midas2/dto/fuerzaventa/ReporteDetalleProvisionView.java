package mx.com.afirme.midas2.dto.fuerzaventa;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class ReporteDetalleProvisionView implements Serializable{

	private static final long serialVersionUID = 1L;
	private Long id;
	private Long idAgente;
	private String nombreAgente;
	private Double incendio_puro;
	private Double maritimo_y_transportes;
	private Double diversos_miscelaneos_y_tecnico;
	private Double automoviles;
	private Double camiones_residentes;
	private Double motocicletas_residentes;
	private Double resp_civil_general;
	private Double resp_civil_de_avion;
	private Double resp_civil_viajero;
	private Double huracan_y_granizo;
	private Double inundacion;
	private Double vida_individual;
	private Double vida_grupo_y_colectivo;
	private Double iva;
	private Double isr;
	private Double ivaRet;
	private Double total;
	
	public static final String PLANTILLA_NAME = "midas.agente.reporte.agente.detalleProvision.archivo.nombre"; 
		
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getIdAgente() {
		return idAgente;
	}
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
	public String getNombreAgente() {
		return nombreAgente;
	}
	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}
	public Double getIncendio_puro() {
		return incendio_puro;
	}
	public void setIncendio_puro(Double incendio_puro) {
		this.incendio_puro = incendio_puro;
	}
	public Double getMaritimo_y_transportes() {
		return maritimo_y_transportes;
	}
	public void setMaritimo_y_transportes(Double maritimo_y_transportes) {
		this.maritimo_y_transportes = maritimo_y_transportes;
	}
	public Double getDiversos_miscelaneos_y_tecnico() {
		return diversos_miscelaneos_y_tecnico;
	}
	public void setDiversos_miscelaneos_y_tecnico(
			Double diversos_miscelaneos_y_tecnico) {
		this.diversos_miscelaneos_y_tecnico = diversos_miscelaneos_y_tecnico;
	}
	public Double getAutomoviles() {
		return automoviles;
	}
	public void setAutomoviles(Double automoviles) {
		this.automoviles = automoviles;
	}
	public Double getCamiones_residentes() {
		return camiones_residentes;
	}
	public void setCamiones_residentes(Double camiones_residentes) {
		this.camiones_residentes = camiones_residentes;
	}
	public Double getMotocicletas_residentes() {
		return motocicletas_residentes;
	}
	public void setMotocicletas_residentes(Double motocicletas_residentes) {
		this.motocicletas_residentes = motocicletas_residentes;
	}
	public Double getResp_civil_general() {
		return resp_civil_general;
	}
	public void setResp_civil_general(Double resp_civil_general) {
		this.resp_civil_general = resp_civil_general;
	}
	public Double getResp_civil_de_avion() {
		return resp_civil_de_avion;
	}
	public void setResp_civil_de_avion(Double resp_civil_de_avion) {
		this.resp_civil_de_avion = resp_civil_de_avion;
	}
	public Double getResp_civil_viajero() {
		return resp_civil_viajero;
	}
	public void setResp_civil_viajero(Double resp_civil_viajero) {
		this.resp_civil_viajero = resp_civil_viajero;
	}
	public Double getHuracan_y_granizo() {
		return huracan_y_granizo;
	}
	public void setHuracan_y_granizo(Double huracan_y_granizo) {
		this.huracan_y_granizo = huracan_y_granizo;
	}
	public Double getInundacion() {
		return inundacion;
	}
	public void setInundacion(Double inundacion) {
		this.inundacion = inundacion;
	}
	public Double getVida_individual() {
		return vida_individual;
	}
	public void setVida_individual(Double vida_individual) {
		this.vida_individual = vida_individual;
	}
	public Double getVida_grupo_y_colectivo() {
		return vida_grupo_y_colectivo;
	}
	public void setVida_grupo_y_colectivo(Double vida_grupo_y_colectivo) {
		this.vida_grupo_y_colectivo = vida_grupo_y_colectivo;
	}
	public Double getIva() {
		return iva;
	}
	public void setIva(Double iva) {
		this.iva = iva;
	}
	public Double getIvaRet() {
		return ivaRet;
	}
	public void setIvaRet(Double ivaRet) {
		this.ivaRet = ivaRet;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public Double getIsr() {
		return isr;
	}
	public void setIsr(Double isr) {
		this.isr = isr;
	}
	
	
}
