package mx.com.afirme.midas2.dto.fuerzaventa;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;



public class ConfiguracionAgenteDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private List<CentroOperacion> centrosOperacion;
	private List<Ejecutivo> ejecutivos;
	private List<Gerencia> gerencias;
	private List<Promotoria> promotorias;
	private List<ValorCatalogoAgentes> situaciones;
	private List<ValorCatalogoAgentes> tiposAgente;
	private List<ValorCatalogoAgentes> tiposPromotoria;
	private List<Agente> agentesEspecificos;
	
	
	public List<CentroOperacion> getCentrosOperacion() {
		return centrosOperacion;
	}
	public void setCentrosOperacion(List<CentroOperacion> centrosOperacion) {
		this.centrosOperacion = centrosOperacion;
	}
	public List<Ejecutivo> getEjecutivos() {
		return ejecutivos;
	}
	public void setEjecutivos(List<Ejecutivo> ejecutivos) {
		this.ejecutivos = ejecutivos;
	}
	public List<Gerencia> getGerencias() {
		return gerencias;
	}
	public void setGerencias(List<Gerencia> gerencias) {
		this.gerencias = gerencias;
	}
	public List<Promotoria> getPromotorias() {
		return promotorias;
	}
	public void setPromotorias(List<Promotoria> promotorias) {
		this.promotorias = promotorias;
	}
	public List<ValorCatalogoAgentes> getSituaciones() {
		return situaciones;
	}
	public void setSituaciones(List<ValorCatalogoAgentes> situaciones) {
		this.situaciones = situaciones;
	}
	public List<ValorCatalogoAgentes> getTiposAgente() {
		return tiposAgente;
	}
	public void setTiposAgente(List<ValorCatalogoAgentes> tiposAgente) {
		this.tiposAgente = tiposAgente;
	}
	public List<ValorCatalogoAgentes> getTiposPromotoria() {
		return tiposPromotoria;
	}
	public void setTiposPromotoria(List<ValorCatalogoAgentes> tiposPromotoria) {
		this.tiposPromotoria = tiposPromotoria;
	}
	public List<Agente> getAgentesEspecificos() {
		return agentesEspecificos;
	}
	public void setAgentesEspecificos(List<Agente> agentesEspecificos) {
		this.agentesEspecificos = agentesEspecificos;
	}


}
