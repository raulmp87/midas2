package mx.com.afirme.midas2.dto.fuerzaventa;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
@Entity
public class PreviewCalculoBonoView implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String descripcionBono;
	private String tipoBono;
	private Date fechaCorte;
	private Long totalBeneficiarios;
	private BigDecimal importeTotal;
	private String modoEjecucion;
	private String fechaCorteString;
	private String descripcionEstatus;
	
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescripcionBono() {
		return descripcionBono;
	}
	public void setDescripcionBono(String descripcionBono) {
		this.descripcionBono = descripcionBono;
	}
	public String getTipoBono() {
		return tipoBono;
	}
	public void setTipoBono(String tipoBono) {
		this.tipoBono = tipoBono;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaCorte() {
		return fechaCorte;
	}
	public void setFechaCorte(Date fechaCorte) {
		this.fechaCorte = fechaCorte;
		if(fechaCorte!=null){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			this.fechaCorteString=sdf.format(fechaCorte);
		}
	}
	public Long getTotalBeneficiarios() {
		return totalBeneficiarios;
	}
	public void setTotalBeneficiarios(Long totalBeneficiarios) {
		this.totalBeneficiarios = totalBeneficiarios;
	}
	public BigDecimal getImporteTotal() {
		return importeTotal;
	}
	public void setImporteTotal(BigDecimal importeTotal) {
		this.importeTotal = importeTotal;
	}
	public String getModoEjecucion() {
		return modoEjecucion;
	}
	public void setModoEjecucion(String modoEjecucion) {
		this.modoEjecucion = modoEjecucion;
	}
	@Transient
	public String getFechaCorteString() {
		return fechaCorteString;
	}
	public void setFechaCorteString(String fechaCorteString) {
		this.fechaCorteString = fechaCorteString;
	}
	public String getDescripcionEstatus() {
		return descripcionEstatus;
	}
	public void setDescripcionEstatus(String descripcionEstatus) {
		this.descripcionEstatus = descripcionEstatus;
	}
	
		

}
