package mx.com.afirme.midas2.dto.fuerzaventa;

import javax.ejb.Local;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
/**
 * Interface para replicar los datos de ejecutivo a Seycos.
 * @author vmhersil
 *
 */
@Local
public interface EjecutivoSeycosFacadeRemote {
	public Long save(Ejecutivo ejecutivo) throws Exception;
	
	public void unsubscribe(Ejecutivo ejecutivo) throws Exception;
}
