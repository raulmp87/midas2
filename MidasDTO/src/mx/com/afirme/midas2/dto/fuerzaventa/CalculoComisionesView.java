package mx.com.afirme.midas2.dto.fuerzaventa;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
@Entity
public class CalculoComisionesView implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 691306494438553519L;
	private Long id;
	private Date fechaCorte;
	private Integer numComisionesProcesadas;
	private Double importeTotalComisiones;
	private String estatusCalculo;
	private String fechaCorteString;
	
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getNumComisionesProcesadas() {
		return numComisionesProcesadas;
	}
	public void setNumComisionesProcesadas(Integer numComisionesProcesadas) {
		this.numComisionesProcesadas = numComisionesProcesadas;
	}
	public Double getImporteTotalComisiones() {
		return importeTotalComisiones;
	}
	public void setImporteTotalComisiones(Double importeTotalComisiones) {
		this.importeTotalComisiones = importeTotalComisiones;
	}
	public String getEstatusCalculo() {
		return estatusCalculo;
	}
	public void setEstatusCalculo(String estatusCalculo) {
		this.estatusCalculo = estatusCalculo;
	}
	
	@Temporal(TemporalType.DATE)
	public Date getFechaCorte() {
		return fechaCorte;
	}
	public void setFechaCorte(Date fechaCorte) {
		this.fechaCorte = fechaCorte;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
		this.fechaCorteString = sdf.format(fechaCorte);
	}
	
	@Transient
	public String getFechaCorteString() {
		return fechaCorteString;
	}
	public void setFechaCorteString(String fechaCorteString) {
		this.fechaCorteString = fechaCorteString;
	}
	
	
	
	
}
