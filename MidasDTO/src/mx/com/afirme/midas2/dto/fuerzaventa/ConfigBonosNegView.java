package mx.com.afirme.midas2.dto.fuerzaventa;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ConfigBonosNegView implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7348464178484965374L;
	private Long idNegocio;
	private Long claveEstatus;
	private String descripcionNegocio;
	private Double pcteDescuento;
	
	@Id
	public Long getIdNegocio() {
		return idNegocio;
	}
	public void setIdNegocio(Long idNegocio) {
		this.idNegocio = idNegocio;
	}
	public Long getClaveEstatus() {
		return claveEstatus;
	}
	public void setClaveEstatus(Long claveEstatus) {
		this.claveEstatus = claveEstatus;
	}
	public String getDescripcionNegocio() {
		return descripcionNegocio;
	}
	public void setDescripcionNegocio(String descripcionNegocio) {
		this.descripcionNegocio = descripcionNegocio;
	}
	public Double getPcteDescuento() {
		return pcteDescuento;
	}
	public void setPcteDescuento(Double pcteDescuento) {
		this.pcteDescuento = pcteDescuento;
	}
	
}
