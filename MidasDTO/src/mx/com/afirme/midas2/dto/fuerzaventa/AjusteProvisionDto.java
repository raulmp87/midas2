package mx.com.afirme.midas2.dto.fuerzaventa;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class AjusteProvisionDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1630675013081349376L;
	public Long id;
	public Double incendioPuro;
	public Double maritimoTransportes;
	public Double diversos;
	public Double automoviles;
	public Double responsabilidadCivilGeneral;
	public Double catastroficos;
	public Double vidaGrupoColectivo;
	public Double vidaInd;
	
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Double getIncendioPuro() {
		return incendioPuro;
	}
	public void setIncendioPuro(Double incendioPuro) {
		this.incendioPuro = incendioPuro;
	}
	public Double getMaritimoTransportes() {
		return maritimoTransportes;
	}
	public void setMaritimoTransportes(Double maritimoTransportes) {
		this.maritimoTransportes = maritimoTransportes;
	}
	public Double getDiversos() {
		return diversos;
	}
	public void setDiversos(Double diversos) {
		this.diversos = diversos;
	}
	public Double getAutomoviles() {
		return automoviles;
	}
	public void setAutomoviles(Double automoviles) {
		this.automoviles = automoviles;
	}
	public Double getResponsabilidadCivilGeneral() {
		return responsabilidadCivilGeneral;
	}
	public void setResponsabilidadCivilGeneral(Double responsabilidadCivilGeneral) {
		this.responsabilidadCivilGeneral = responsabilidadCivilGeneral;
	}
	public Double getCatastroficos() {
		return catastroficos;
	}
	public void setCatastroficos(Double catastroficos) {
		this.catastroficos = catastroficos;
	}
	public Double getVidaGrupoColectivo() {
		return vidaGrupoColectivo;
	}
	public void setVidaGrupoColectivo(Double vidaGrupoColectivo) {
		this.vidaGrupoColectivo = vidaGrupoColectivo;
	}
	public Double getVidaInd() {
		return vidaInd;
	}
	public void setVidaInd(Double vidaInd) {
		this.vidaInd = vidaInd;
	}

}
