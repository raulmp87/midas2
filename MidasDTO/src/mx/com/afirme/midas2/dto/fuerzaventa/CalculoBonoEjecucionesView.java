package mx.com.afirme.midas2.dto.fuerzaventa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
public class CalculoBonoEjecucionesView implements Serializable{

	private static final long serialVersionUID = -139825996688432423L;
	 
	
	private Long id;
	private String nombreConfiguracionBono;
	private String modoEjecucion;
	private String estatusEjecucion;
	private Long idPreviewCalculo;
	private Date fechaEjecucion;
	private String fechaEjecucionString;
	private String usuario;
	private Date fechaFinalizaBono;
	private String fechaFinalizaBonoString;
	private String avance;
	
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombreConfiguracionBono() {
		return nombreConfiguracionBono;
	}
	public void setNombreConfiguracionBono(String nombreConfiguracionBono) {
		this.nombreConfiguracionBono = nombreConfiguracionBono;
	}
	public String getModoEjecucion() {
		return modoEjecucion;
	}
	public void setModoEjecucion(String modoEjecucion) {
		this.modoEjecucion = modoEjecucion;
	}
	public String getEstatusEjecucion() {
		return estatusEjecucion;
	}
	public void setEstatusEjecucion(String estatusEjecucion) {
		this.estatusEjecucion = estatusEjecucion;
	}
	public Long getIdPreviewCalculo() {
		return idPreviewCalculo;
	}
	public void setIdPreviewCalculo(Long idPreviewCalculo) {
		this.idPreviewCalculo = idPreviewCalculo;
	}
	
	@Temporal(TemporalType.DATE)
	public Date getFechaEjecucion() {
		return fechaEjecucion;
	}
	public void setFechaEjecucion(Date fechaEjecucion) {
		this.fechaEjecucion = fechaEjecucion;
	}
	
	@Transient
	public String getFechaEjecucionString() {
		return fechaEjecucionString;
	}
	public void setFechaEjecucionString(String fechaEjecucionString) {
		this.fechaEjecucionString = fechaEjecucionString;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	@Temporal(TemporalType.DATE)
	public Date getFechaFinalizaBono() {
		return fechaFinalizaBono;
	}
	public void setFechaFinalizaBono(Date fechaFinalizaBono) {
		this.fechaFinalizaBono = fechaFinalizaBono;
	}
	
	@Transient
	public String getFechaFinalizaBonoString() {
		return fechaFinalizaBonoString;
	}
	public void setFechaFinalizaBonoString(String fechaFinalizaBonoString) {
		this.fechaFinalizaBonoString = fechaFinalizaBonoString;
	}
	public String getAvance() {
		return avance;
	}
	public void setAvance(String avance) {
		this.avance = avance;
	}
	
	
}
