package mx.com.afirme.midas2.dto.fuerzaventa;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import mx.com.afirme.midas2.domain.bonos.ConfigBonos;

@Entity
public class ExcepcionesPolizaView implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8535252920566592722L;
	private Long idPoliza;
	private Long numeroPoliza;
	private Long numeroPolizaSeycos;
	private String nombreCompleto; 
	private String descripcionPromotoria;
	private String stringNumeroPoliza;
	private String numeroPolizaMidasString;
	private String numeroPolizaSeycosString;
	//excepciones 
	private ConfigBonos idConfig;
	private BigDecimal idCotizacion;
	private Double valorMontoPcteAgente;
	private Double valorMontoPctePromotoria;
	private Integer noAplicaDescuentoExcepcion;
	private Double descuentoPoliza;
	private Long idExcepcionPoliza;
	
	@Id
	public Long getIdPoliza() {
		return idPoliza;
	}
	public void setIdPoliza(Long idPoliza) {
		this.idPoliza = idPoliza;
	}
	public Long getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(Long numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	public String getDescripcionPromotoria() {
		return descripcionPromotoria;
	}
	public void setDescripcionPromotoria(String descripcionPromotoria) {
		this.descripcionPromotoria = descripcionPromotoria;
	}
	@Transient
	public String getStringNumeroPoliza() {
		return stringNumeroPoliza;
	}
	public void setStringNumeroPoliza(String stringNumeroPoliza) {
		this.stringNumeroPoliza = stringNumeroPoliza;
	}
	public Long getNumeroPolizaSeycos() {
		return numeroPolizaSeycos;
	}
	public void setNumeroPolizaSeycos(Long numeroPolizaSeycos) {
		this.numeroPolizaSeycos = numeroPolizaSeycos;
	}
	public String getNumeroPolizaMidasString() {
		return numeroPolizaMidasString;
	}
	public void setNumeroPolizaMidasString(String numeroPolizaMidasString) {
		this.numeroPolizaMidasString = numeroPolizaMidasString;
	}
	public String getNumeroPolizaSeycosString() {
		return numeroPolizaSeycosString;
	}
	public void setNumeroPolizaSeycosString(String numeroPolizaSeycosString) {
		this.numeroPolizaSeycosString = numeroPolizaSeycosString;
	}
	public ConfigBonos getIdConfig() {
		return idConfig;
	}
	public void setIdConfig(ConfigBonos idConfig) {
		this.idConfig = idConfig;
	}
	public BigDecimal getIdCotizacion() {
		return idCotizacion;
	}
	public void setIdCotizacion(BigDecimal idCotizacion) {
		this.idCotizacion = idCotizacion;
	}
	public Double getValorMontoPcteAgente() {
		return valorMontoPcteAgente;
	}
	public void setValorMontoPcteAgente(Double valorMontoPcteAgente) {
		this.valorMontoPcteAgente = valorMontoPcteAgente;
	}
	public Double getValorMontoPctePromotoria() {
		return valorMontoPctePromotoria;
	}
	public void setValorMontoPctePromotoria(Double valorMontoPctePromotoria) {
		this.valorMontoPctePromotoria = valorMontoPctePromotoria;
	}
	public Integer getNoAplicaDescuentoExcepcion() {
		return noAplicaDescuentoExcepcion;
	}
	public void setNoAplicaDescuentoExcepcion(Integer noAplicaDescuentoExcepcion) {
		this.noAplicaDescuentoExcepcion = noAplicaDescuentoExcepcion;
	}
	public Double getDescuentoPoliza() {
		return descuentoPoliza;
	}
	public void setDescuentoPoliza(Double descuentoPoliza) {
		this.descuentoPoliza = descuentoPoliza;
	}
	public Long getIdExcepcionPoliza() {
		return idExcepcionPoliza;
	}
	public void setIdExcepcionPoliza(Long idExcepcionPoliza) {
		this.idExcepcionPoliza = idExcepcionPoliza;
	}
	
	
}
