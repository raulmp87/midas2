package mx.com.afirme.midas2.dto.siniestros;



import org.springframework.stereotype.Component;
@Component
public class ArchivosDigitalizadosSiniestroDTO {

	private String nombre;
	private String tipo;
	private String nodo;
	private String digitalizado;
	private String descripcion;
	private String idExpediente;	
	private String cveProceso;
	private String aplicacion;
	

	public String getIdExpediente() {
		return idExpediente;
	} 

	public void setIdExpediente(String idExpediente) {
		this.idExpediente = idExpediente; 
	}

	public String getCveProceso() {
		return cveProceso;
	}

	public void setCveProceso(String cveProceso) {
		this.cveProceso = cveProceso;
	}

	public String getAplicacion() {
		return aplicacion;
	}

	public void setAplicacion(String aplicacion) {
		this.aplicacion = aplicacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getNodo() {
		return nodo;
	}

	public void setNodo(String nodo) {
		this.nodo = nodo;
	}

	public String getDigitalizado() {
		return digitalizado;
	}

	public void setDigitalizado(String digitalizado) {
		this.digitalizado = digitalizado;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}	
	
	

}