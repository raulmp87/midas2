package mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.gastoajuste;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity(name = "PrestadorGastoAjusteDTO")
@Table(name = "VW_PRESTADOR_GASTO_AJUSTE", schema = "MIDAS")
public class PrestadorGastoAjusteDTO implements Entidad{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1823808204621736440L;

	@Id
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "NOMBRE")
	private String nombre;
	
	@Column(name = "TIPO")
	private String tipo;
	
	@Column(name = "PRESTADOR_ORIGEN")
	private String prestadorOrigen;
	
	@Column(name = "ORIGEN")
	private Short origen;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getPrestadorOrigen() {
		return prestadorOrigen;
	}

	public void setPrestadorOrigen(String prestadorOrigen) {
		this.prestadorOrigen = prestadorOrigen;
	}

	public Short getOrigen() {
		return origen;
	}

	public void setOrigen(Short origen) {
		this.origen = origen;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object getKey() {
		// TODO Auto-generated method stub
		return this.getId();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return getNombre();
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
