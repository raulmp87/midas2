package mx.com.afirme.midas2.dto.siniestros.incentivos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class IncentivoAjustadorDTO implements Serializable {
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -8037083030052457391L;
	private Long incentivoId;
	private Long folio;
	private Boolean seleccionado = Boolean.FALSE;
	private String tipoConceptoPago;
	private String conceptoPagoDesc;
	private Long numeroAjustador;
	private String nombreAjustador;
	private String oficinaAjustador;
	private String numeroReporte;
	private String oficinaReporte;
	private Date fechaAsignacionAjustador;
	private String horaAsignacionAjustador;
	private BigDecimal montoRecuperacion;
	
	/**
	 * Datos globales por ajustador
	 */
	
	private Long numeroSiniestros;
	private BigDecimal costo;
	private BigDecimal importe;
	private BigDecimal montoRecuperacionEfectivo;
	private BigDecimal montoCalculadoEfectivo;
	private BigDecimal montoExcedenteEfectivo;
	private BigDecimal montoCalculadoExcedente;	
	
	private BigDecimal montoTotalInhabiles;
	private BigDecimal montoTotalEfectivo;
	private BigDecimal montoTotalCompanias;
	private BigDecimal montoTotalSIPAC;
	private BigDecimal montoTotalRechazos;
	private BigDecimal montoTotal;
	
	
	
	public IncentivoAjustadorDTO() {
		super();
	}
	
	
	
	
	public IncentivoAjustadorDTO(Long incentivoId, Boolean seleccionado,
			String conceptoPagoDesc, String nombreAjustador,
			String oficinaAjustador, String numeroReporte,
			String oficinaReporte, Date fechaAsignacionAjustador, BigDecimal montoRecuperacion) {
		super();
		this.incentivoId = incentivoId;
		this.seleccionado = seleccionado;
		this.conceptoPagoDesc = conceptoPagoDesc;
		this.nombreAjustador = nombreAjustador;
		this.oficinaAjustador = oficinaAjustador;
		this.numeroReporte = numeroReporte;
		this.oficinaReporte = oficinaReporte;
		this.fechaAsignacionAjustador = fechaAsignacionAjustador;
		this.montoRecuperacion = montoRecuperacion;
	}
	
	public IncentivoAjustadorDTO(Long incentivoId, Boolean seleccionado,
			String tipoConceptoPago, String conceptoPagoDesc, Long numeroAjustador, String nombreAjustador,
			String oficinaAjustador, String numeroReporte,
			String oficinaReporte, Date fechaAsignacionAjustador, BigDecimal montoRecuperacion) {
		super();
		this.incentivoId = incentivoId;
		this.seleccionado = seleccionado;
		this.tipoConceptoPago = tipoConceptoPago;
		this.conceptoPagoDesc = conceptoPagoDesc;
		this.numeroAjustador = numeroAjustador;
		this.nombreAjustador = nombreAjustador;
		this.oficinaAjustador = oficinaAjustador;
		this.numeroReporte = numeroReporte;
		this.oficinaReporte = oficinaReporte;
		this.fechaAsignacionAjustador = fechaAsignacionAjustador;
		this.montoRecuperacion = montoRecuperacion;
	}
	
	public IncentivoAjustadorDTO(Long numeroAjustador, String nombreAjustador, Long numeroSiniestros,
								BigDecimal costo,BigDecimal importe) {
		super();
		this.numeroAjustador = numeroAjustador;
		this.nombreAjustador = nombreAjustador;
		this.nombreAjustador = nombreAjustador;
		this.numeroSiniestros = numeroSiniestros;
		this.costo = costo;
		this.importe = importe;
	}
	
	public IncentivoAjustadorDTO(Long numeroAjustador, String nombreAjustador, Long numeroSiniestros,
			BigDecimal montoRecuperacionEfectivo, BigDecimal montoCalculadoEfectivo, BigDecimal montoExcedenteEfectivo, 
			BigDecimal montoCalculadoExcedente, BigDecimal importe) {
		super();
		this.numeroAjustador = numeroAjustador;
		this.nombreAjustador = nombreAjustador;
		this.nombreAjustador = nombreAjustador;
		this.numeroSiniestros = numeroSiniestros;
		this.montoRecuperacionEfectivo = montoRecuperacionEfectivo;
		this.montoCalculadoEfectivo = montoCalculadoEfectivo;
		this.montoExcedenteEfectivo = montoExcedenteEfectivo;
		this.montoCalculadoExcedente = montoCalculadoExcedente;
		this.importe = importe;
	}




	public Long getIncentivoId() {
		return incentivoId;
	}
	public void setIncentivoId(Long incentivoId) {
		this.incentivoId = incentivoId;
	}
	public Long getFolio() {
		return folio;
	}
	public void setFolio(Long folio) {
		this.folio = folio;
	}
	public Boolean getSeleccionado() {
		return seleccionado;
	}
	public void setSeleccionado(Boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	public String getTipoConceptoPago() {
		return tipoConceptoPago;
	}
	public void setTipoConceptoPago(String tipoConceptoPago) {
		this.tipoConceptoPago = tipoConceptoPago;
	}
	public String getConceptoPagoDesc() {
		return conceptoPagoDesc;
	}
	public void setConceptoPagoDesc(String conceptoPagoDesc) {
		this.conceptoPagoDesc = conceptoPagoDesc;
	}
	public Long getNumeroAjustador() {
		return numeroAjustador;
	}
	public void setNumeroAjustador(Long numeroAjustador) {
		this.numeroAjustador = numeroAjustador;
	}
	public String getNombreAjustador() {
		return nombreAjustador;
	}
	public void setNombreAjustador(String nombreAjustador) {
		this.nombreAjustador = nombreAjustador;
	}
	public String getOficinaAjustador() {
		return oficinaAjustador;
	}
	public void setOficinaAjustador(String oficinaAjustador) {
		this.oficinaAjustador = oficinaAjustador;
	}
	public String getNumeroReporte() {
		return numeroReporte;
	}
	public void setNumeroReporte(String numeroReporte) {
		this.numeroReporte = numeroReporte;
	}
	public String getOficinaReporte() {
		return oficinaReporte;
	}
	public void setOficinaReporte(String oficinaReporte) {
		this.oficinaReporte = oficinaReporte;
	}
	public Date getFechaAsignacionAjustador() {
		return fechaAsignacionAjustador;
	}
	public void setFechaAsignacionAjustador(Date fechaAsignacionAjustador) {
		this.fechaAsignacionAjustador = fechaAsignacionAjustador;
	}
	public String getHoraAsignacionAjustador() {
		return horaAsignacionAjustador;
	}
	public void setHoraAsignacionAjustador(String horaAsignacionAjustador) {
		this.horaAsignacionAjustador = horaAsignacionAjustador;
	}
	public BigDecimal getMontoRecuperacion() {
		return montoRecuperacion;
	}
	public void setMontoRecuperacion(BigDecimal montoRecuperacion) {
		this.montoRecuperacion = montoRecuperacion;
	}
	public Long getNumeroSiniestros() {
		return numeroSiniestros;
	}
	public void setNumeroSiniestros(Long numeroSiniestros) {
		this.numeroSiniestros = numeroSiniestros;
	}
	public BigDecimal getCosto() {
		return costo;
	}
	public void setCosto(BigDecimal costo) {
		this.costo = costo;
	}
	public BigDecimal getImporte() {
		return importe;
	}
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}
	public BigDecimal getMontoRecuperacionEfectivo() {
		return montoRecuperacionEfectivo;
	}
	public void setMontoRecuperacionEfectivo(BigDecimal montoRecuperacionEfectivo) {
		this.montoRecuperacionEfectivo = montoRecuperacionEfectivo;
	}
	public BigDecimal getMontoCalculadoEfectivo() {
		return montoCalculadoEfectivo;
	}
	public void setMontoCalculadoEfectivo(BigDecimal montoCalculadoEfectivo) {
		this.montoCalculadoEfectivo = montoCalculadoEfectivo;
	}
	public BigDecimal getMontoExcedenteEfectivo() {
		return montoExcedenteEfectivo;
	}
	public void setMontoExcedenteEfectivo(BigDecimal montoExcedenteEfectivo) {
		this.montoExcedenteEfectivo = montoExcedenteEfectivo;
	}
	public BigDecimal getMontoCalculadoExcedente() {
		return montoCalculadoExcedente;
	}
	public void setMontoCalculadoExcedente(BigDecimal montoCalculadoExcedente) {
		this.montoCalculadoExcedente = montoCalculadoExcedente;
	}
	public BigDecimal getMontoTotalInhabiles() {
		return montoTotalInhabiles;
	}
	public void setMontoTotalInhabiles(BigDecimal montoTotalInhabiles) {
		this.montoTotalInhabiles = montoTotalInhabiles;
	}
	public BigDecimal getMontoTotalEfectivo() {
		return montoTotalEfectivo;
	}
	public void setMontoTotalEfectivo(BigDecimal montoTotalEfectivo) {
		this.montoTotalEfectivo = montoTotalEfectivo;
	}
	public BigDecimal getMontoTotalCompanias() {
		return montoTotalCompanias;
	}
	public void setMontoTotalCompanias(BigDecimal montoTotalCompanias) {
		this.montoTotalCompanias = montoTotalCompanias;
	}
	public BigDecimal getMontoTotalSIPAC() {
		return montoTotalSIPAC;
	}
	public void setMontoTotalSIPAC(BigDecimal montoTotalSIPAC) {
		this.montoTotalSIPAC = montoTotalSIPAC;
	}
	public BigDecimal getMontoTotalRechazos() {
		return montoTotalRechazos;
	}
	public void setMontoTotalRechazos(BigDecimal montoTotalRechazos) {
		this.montoTotalRechazos = montoTotalRechazos;
	}
	public BigDecimal getMontoTotal() {
		return montoTotal;
	}
	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}
	
	
	
	
}
