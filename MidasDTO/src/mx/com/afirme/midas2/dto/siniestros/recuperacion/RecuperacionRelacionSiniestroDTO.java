
package mx.com.afirme.midas2.dto.siniestros.recuperacion;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class RecuperacionRelacionSiniestroDTO  implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1151804639497558136L;

	private Long idRecuperacion;
	private Long idCoberturaRepCabina;
	private Long idEstimacionCoberturaRepCabina;	
	private String numeroPoliza;
	private String estadoUbicacion;
	private String ciudadUbicacion;
	private String lugarSiniestro;
	private Date   fechaSiniestro;  
	private String   fechaSiniestroStr;  
	private String tipoPerdida;
	private String vehiculoAsegurado;
	private String vehiculoTercero;
	private String personaLesionada;
	private String mensaje;
	private String conceptoCarta;
	
	//Datos Adicional
	private Long companiaId;
	private String compania;
	private String numPolizaCia;
	private String noSiniestrosCia;
	private String paseAtnCia;
	private Integer porcParticipacion;
	private boolean cartaComplemento;
	private String recuperacionAnterior;
	private String nombreCompania;
	
	
	//MONTOS TOTALES 
	private BigDecimal montoIndemniza ; 
	private BigDecimal montoSalvamento;
	private BigDecimal montoGrua;
	private BigDecimal montoTotal;
	private BigDecimal montoInicial;
	private String termioAjuste;
	private BigDecimal montoPiezas;
	
	
	
	public BigDecimal getMontoPiezas() {
		return montoPiezas;
	}
	public void setMontoPiezas(BigDecimal montoPiezas) {
		this.montoPiezas = montoPiezas;
	}
	public Long getIdRecuperacion() {
		return idRecuperacion;
	}
	public void setIdRecuperacion(Long idRecuperacion) {
		this.idRecuperacion = idRecuperacion;
	}
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public String getEstadoUbicacion() {
		return estadoUbicacion;
	}
	public void setEstadoUbicacion(String estadoUbicacion) {
		this.estadoUbicacion = estadoUbicacion;
	}
	public String getCiudadUbicacion() {
		return ciudadUbicacion;
	}
	public void setCiudadUbicacion(String ciudadUbicacion) {
		this.ciudadUbicacion = ciudadUbicacion;
	}
	public String getLugarSiniestro() {
		return lugarSiniestro;
	}
	public void setLugarSiniestro(String lugarSiniestro) {
		this.lugarSiniestro = lugarSiniestro;
	}
	public Date getFechaSiniestro() {
		return fechaSiniestro;
	}
	public void setFechaSiniestro(Date fechaSiniestro) {
		this.fechaSiniestro = fechaSiniestro;
	}
	public String getTipoPerdida() {
		return tipoPerdida;
	}
	public void setTipoPerdida(String tipoPerdida) {
		this.tipoPerdida = tipoPerdida;
	}
	public String getVehiculoAsegurado() {
		return vehiculoAsegurado;
	}
	public void setVehiculoAsegurado(String vehiculoAsegurado) {
		this.vehiculoAsegurado = vehiculoAsegurado;
	}
	public String getVehiculoTercero() {
		return vehiculoTercero;
	}
	public void setVehiculoTercero(String vehiculoTercero) {
		this.vehiculoTercero = vehiculoTercero;
	}
	public String getPersonaLesionada() {
		return personaLesionada;
	}
	public void setPersonaLesionada(String personaLesionada) {
		this.personaLesionada = personaLesionada;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getCompania() {
		return compania;
	}
	public void setCompania(String compania) {
		this.compania = compania;
	}
	public String getNumPolizaCia() {
		return numPolizaCia;
	}
	public void setNumPolizaCia(String numPolizaCia) {
		this.numPolizaCia = numPolizaCia;
	}
	public String getNoSiniestrosCia() {
		return noSiniestrosCia;
	}
	public void setNoSiniestrosCia(String noSiniestrosCia) {
		this.noSiniestrosCia = noSiniestrosCia;
	}
	public String getPaseAtnCia() {
		return paseAtnCia;
	}
	public void setPaseAtnCia(String paseAtnCia) {
		this.paseAtnCia = paseAtnCia;
	}
	
	public Integer getPorcParticipacion() {
		return porcParticipacion;
	}
	public void setPorcParticipacion(Integer porcParticipacion) {
		this.porcParticipacion = porcParticipacion;
	}
	public boolean isCartaComplemento() {
		return cartaComplemento;
	}
	public void setCartaComplemento(boolean cartaComplemento) {
		this.cartaComplemento = cartaComplemento;
	}
	public String getRecuperacionAnterior() {
		return recuperacionAnterior;
	}
	public void setRecuperacionAnterior(String recuperacionAnterior) {
		this.recuperacionAnterior = recuperacionAnterior;
	}
	public Long getIdCoberturaRepCabina() {
		return idCoberturaRepCabina;
	}
	public void setIdCoberturaRepCabina(Long idCoberturaRepCabina) {
		this.idCoberturaRepCabina = idCoberturaRepCabina;
	}
	public Long getIdEstimacionCoberturaRepCabina() {
		return idEstimacionCoberturaRepCabina;
	}
	public void setIdEstimacionCoberturaRepCabina(
			Long idEstimacionCoberturaRepCabina) {
		this.idEstimacionCoberturaRepCabina = idEstimacionCoberturaRepCabina;
	}
	public String getConceptoCarta() {
		return conceptoCarta;
	}
	public void setConceptoCarta(String conceptoCarta) {
		this.conceptoCarta = conceptoCarta;
	}
	public Long getCompaniaId() {
		return companiaId;
	}
	public void setCompaniaId(Long companiaId) {
		this.companiaId = companiaId;
	}
	public BigDecimal getMontoIndemniza() {
		return montoIndemniza;
	}
	public void setMontoIndemniza(BigDecimal montoIndemniza) {
		this.montoIndemniza = montoIndemniza;
	}
	public BigDecimal getMontoSalvamento() {
		return montoSalvamento;
	}
	public void setMontoSalvamento(BigDecimal montoSalvamento) {
		this.montoSalvamento = montoSalvamento;
	}
	public BigDecimal getMontoGrua() {
		return montoGrua;
	}
	public void setMontoGrua(BigDecimal montoGrua) {
		this.montoGrua = montoGrua;
	}
	public BigDecimal getMontoTotal() {
		return montoTotal;
	}
	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}
	public String getFechaSiniestroStr() {
		return fechaSiniestroStr;
	}
	public void setFechaSiniestroStr(String fechaSiniestroStr) {
		this.fechaSiniestroStr = fechaSiniestroStr;
	}
	public String getTermioAjuste() {
		return termioAjuste;
	}
	public void setTermioAjuste(String termioAjuste) {
		this.termioAjuste = termioAjuste;
	}
	public String getNombreCompania() {
		return nombreCompania;
	}
	public void setNombreCompania(String nombreCompania) {
		this.nombreCompania = nombreCompania;
	}
	public BigDecimal getMontoInicial() {
		return montoInicial;
	}
	public void setMontoInicial(BigDecimal montoInicial) {
		this.montoInicial = montoInicial;
	}


}
