package mx.com.afirme.midas2.dto.siniestros;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;

import org.springframework.stereotype.Component;

@Component
@XmlAccessorType(XmlAccessType.FIELD)
public class ComplementoReporteDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idReporte;
	private Long incisoContinuityId;
	private Long ajustadorId;
	private String usuario;
	private Date fechaHoraAsignacion;
	
	
	
	public Long getIdReporte() {
		return idReporte;
	}
	public void setIdReporte(Long idReporte) {
		this.idReporte = idReporte;
	}
	public Long getIncisoContinuityId() {
		return incisoContinuityId;
	}
	public void setIncisoContinuityId(Long incisoContinuityId) {
		this.incisoContinuityId = incisoContinuityId;
	}
	public Long getAjustadorId() {
		return ajustadorId;
	}
	public void setAjustadorId(Long ajustadorId) {
		this.ajustadorId = ajustadorId;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public Date getFechaHoraAsignacion() {
		return fechaHoraAsignacion;
	}
	public void setFechaHoraAsignacion(Date fechaHoraAsignacion) {
		this.fechaHoraAsignacion = fechaHoraAsignacion;
	}
	
	
}
