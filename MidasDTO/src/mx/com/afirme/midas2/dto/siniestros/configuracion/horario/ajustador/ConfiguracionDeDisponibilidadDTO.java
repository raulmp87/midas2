package mx.com.afirme.midas2.dto.siniestros.configuracion.horario.ajustador;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class ConfiguracionDeDisponibilidadDTO implements Serializable {

	private static final long	serialVersionUID	= -901965023802345339L;

	private String				oficinaId;
	private List<String>		ajustadoresIds;
	private Long				horarioLaboralId;
	// ----- Long getHorarioLaboralDefault()
	private String				tipoDisponibilidadCode;

	private Date				fechaInicial;
	private Date				fechaFinal;

	private String				comentarios;
	private String				diasSeleccion;

	public String getOficinaId() {
		return oficinaId;
	}

	public void setOficinaId(String oficinaId) {
		this.oficinaId = oficinaId;
	}

	public List<String> getAjustadoresIds() {
		return ajustadoresIds;
	}

	public void setAjustadoresIds(List<String> ajustadoresIds) {
		this.ajustadoresIds = ajustadoresIds;
	}

	public Long getHorarioLaboralId() {
		return horarioLaboralId;
	}

	public void setHorarioLaboralId(Long horarioLaboralId) {
		this.horarioLaboralId = horarioLaboralId;
	}

	public String getTipoDisponibilidadCode() {
		return tipoDisponibilidadCode;
	}

	public void setTipoDisponibilidadCode(String tipoDisponibilidadCode) {
		this.tipoDisponibilidadCode = tipoDisponibilidadCode;
	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public String getDiasSeleccion() {
		return diasSeleccion;
	}

	public void setDiasSeleccion(String diasSeleccion) {
		this.diasSeleccion = diasSeleccion;
	}

}
