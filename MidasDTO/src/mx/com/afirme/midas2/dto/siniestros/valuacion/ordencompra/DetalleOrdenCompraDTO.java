package mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra;

import java.io.Serializable;
import java.math.BigDecimal;

import mx.com.afirme.midas2.domain.siniestros.catalogo.conceptos.ConceptoAjuste;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;

import org.springframework.stereotype.Component;

@Component
public class DetalleOrdenCompraDTO implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -2643227125513163757L;
	private Long id;
	private ConceptoAjuste conceptoAjuste;
	
	private BigDecimal costoUnitario;
	
	private String estatus;
	
	private BigDecimal importe;

	private BigDecimal importePagado;
	private BigDecimal iva;
	
	
	
	private String observaciones;
	
	private OrdenCompra ordenCompra;
	
	private BigDecimal porcIva;
	
	private BigDecimal porcIvaRetenido;

	private BigDecimal porcIsr;
	
	private Long idOrdenCompra;
	private BigDecimal cantidad;
	
	private String coberturaDescripcion;
	
	private String conceptoPago;
	
	private BigDecimal pendiente;
	
	private String tipoOrden ;
	
	private BigDecimal ivaRetenido;
	
	private BigDecimal isr;
	
	
	
	
	
	
	
	
	
	



	public BigDecimal getPorcIvaRetenido() {
		return porcIvaRetenido;
	}

	public void setPorcIvaRetenido(BigDecimal porcIvaRetenido) {
		this.porcIvaRetenido = porcIvaRetenido;
	}

	public BigDecimal getPorcIsr() {
		return porcIsr;
	}

	public void setPorcIsr(BigDecimal porcIsr) {
		this.porcIsr = porcIsr;
	}

	public BigDecimal getIvaRetenido() {
		return ivaRetenido;
	}

	public void setIvaRetenido(BigDecimal ivaRetenido) {
		this.ivaRetenido = ivaRetenido;
	}

	public BigDecimal getIsr() {
		return isr;
	}

	public void setIsr(BigDecimal isr) {
		this.isr = isr;
	}

	public String getTipoOrden() {
		return tipoOrden;
	}

	public void setTipoOrden(String tipoOrden) {
		this.tipoOrden = tipoOrden;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getCostoUnitario() {
		return costoUnitario;
	}

	public void setCostoUnitario(BigDecimal costoUnitario) {
		this.costoUnitario = costoUnitario;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public BigDecimal getImporte() {
		return importe;
	}

	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	public BigDecimal getCantidad() {
		return cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public String getCoberturaDescripcion() {
		return coberturaDescripcion;
	}

	public void setCoberturaDescripcion(String coberturaDescripcion) {
		this.coberturaDescripcion = coberturaDescripcion;
	}

	public String getConceptoPago() {
		return conceptoPago;
	}

	public void setConceptoPago(String conceptoPago) {
		this.conceptoPago = conceptoPago;
	}

	public BigDecimal getPendiente() {
		return pendiente;
	}

	public void setPendiente(BigDecimal pendiente) {
		this.pendiente = pendiente;
	}

	public BigDecimal getImportePagado() {
		return importePagado;
	}

	public void setImportePagado(BigDecimal importePagado) {
		this.importePagado = importePagado;
	}

	public BigDecimal getIva() {
		return iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public OrdenCompra getOrdenCompra() {
		return ordenCompra;
	}

	public void setOrdenCompra(OrdenCompra ordenCompra) {
		this.ordenCompra = ordenCompra;
	}

	public BigDecimal getPorcIva() {
		return porcIva;
	}

	public void setPorcIva(BigDecimal porcIva) {
		this.porcIva = porcIva;
	}

	public ConceptoAjuste getConceptoAjuste() {
		return conceptoAjuste;
	}

	public void setConceptoAjuste(ConceptoAjuste conceptoAjuste) {
		this.conceptoAjuste = conceptoAjuste;
	}

	public Long getIdOrdenCompra() {
		return idOrdenCompra;
	}

	public void setIdOrdenCompra(Long idOrdenCompra) {
		this.idOrdenCompra = idOrdenCompra;
	}

	

	
	

}
