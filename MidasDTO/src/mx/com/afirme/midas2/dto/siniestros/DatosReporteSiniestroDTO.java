/**
 * 
 */
package mx.com.afirme.midas2.dto.siniestros;

import java.io.Serializable;

import org.springframework.stereotype.Component;

/**
 * @author simavera
 *
 */
@Component
public class DatosReporteSiniestroDTO implements Serializable{

	private String numeroReporte;
	private String numeroSiniestro;
	private String numeroPoliza;
	private Integer numeroInciso;
	private Long reporteCabinaId;
	
	/**
	 * @return the numeroReporte
	 */
	public String getNumeroReporte() {
		return numeroReporte;
	}
	/**
	 * @param numeroReporte the numeroReporte to set
	 */
	public void setNumeroReporte(String numeroReporte) {
		this.numeroReporte = numeroReporte;
	}
	/**
	 * @return the numeroSiniestro
	 */
	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}
	/**
	 * @param numeroSiniestro the numeroSiniestro to set
	 */
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}
	/**
	 * @return the numeroPoliza
	 */
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	/**
	 * @param numeroPoliza the numeroPoliza to set
	 */
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	/**
	 * @return the numeroInciso
	 */
	public Integer getNumeroInciso() {
		return numeroInciso;
	}
	/**
	 * @param numeroInciso the numeroInciso to set
	 */
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	/**
	 * @return id reporteCabina
	 */
	public Long getReporteCabinaId() {
		return reporteCabinaId;
	}
	/**
	 * @return reporteCabinaId
	 */
	public void setReporteCabinaId(Long reporteCabinaId) {
		this.reporteCabinaId = reporteCabinaId;
	}
	
	
}
