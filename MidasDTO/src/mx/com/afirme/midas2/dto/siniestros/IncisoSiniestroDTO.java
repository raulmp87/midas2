package mx.com.afirme.midas2.dto.siniestros;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.Cotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoInciso;

import org.springframework.stereotype.Component;

@Component
public class IncisoSiniestroDTO implements Serializable {

	private static final long serialVersionUID = -3390764027252034664L;

	public enum EstatusIncisoSiniestro {
		VIGENTE, NO_VIGENTE, CANCELADO
	};

	private Long incisoContinuityId;
	private Cotizacion cotizacion;
	private AutoInciso autoInciso;
	private Short estatus;
	private String descEstatus;
	private Short claveEstatus;
	private Boolean servicioPublico;
	private Boolean servicioParticular;
	private String numeroPoliza;
	private Integer cvePolizaMidas;
	private String numPolizaSeycos;
	private Date fechaIniVigencia;
	private Date fechaFinVigencia;
	private Integer numeroInciso;
	private String numeroCotizacion;
	private String motivo;
	private BigDecimal idToCotizacion;
	private Date fechaReporteSiniestro;
	private DatosContratanteDTO datosContratante;
	private DatosPersonalizacionDTO datosPersonalizacion;
	private Date fechaEmision;
	private String numeroPolizaAnterior;
	private String producto;
	private String medioPago;
	private String moneda;
	private String formaPago;
	private BigDecimal idToPoliza;
	private Date validOn;
	private Date validTo;
	private Date recordFrom;
	private BigDecimal idToSolicitud;
	private BigDecimal idToSolicitudDataEnTramite;
	private String modelo;
	private Boolean tieneProrroga;
	private String nombreLinea;
	private String agente;
	private Long validOnMillis;
	private Long recordFromMillis;
	private String nombreContratante;
	private Boolean noAplicaDepuracion;
	private String numeroSerie;
	private String marca;
	private String tipoVehiculo;
	private String placa;
	private String numeroMotor;
	private Short modeloVehiculo;
	private Boolean provieneSeguroObligatorio;
	private BigDecimal idToSeccion;
	private Boolean busquedaPolizaSeycos;
	private Short esSeguroObligatorio;
	private String numeroFolio;
	private Date fechaVigenciaIniRealInciso;
	private Date fechaRealEmisionPoliza;
	private Integer numeroSecuencia; 
	
	

	

	public IncisoSiniestroDTO() {
	}
	
	public IncisoSiniestroDTO(Long incisoContinuityId, String numeroPoliza, Integer cvePolizaMidas, String numPolizaSeycos,BigDecimal idToPoliza, Short claveEstatus, Date fechaIniVigencia, Date fechaFinVigencia, Integer numeroInciso, Date validOn,
			Date recordFrom, String nombreContratante, String numeroSerie, String marca,
			String tipoVehiculo, String placa, String numeroMotor, Short modeloVehiculo, Object estatus, 
			Boolean provieneSeguroObligatorio){
		
		this(incisoContinuityId, numeroPoliza, cvePolizaMidas, numPolizaSeycos, idToPoliza, claveEstatus, fechaIniVigencia, fechaFinVigencia, numeroInciso, validOn,
				recordFrom, nombreContratante, numeroSerie, marca,
				tipoVehiculo, placa, numeroMotor, modeloVehiculo, estatus, 
				provieneSeguroObligatorio, null);
	}
	
	public IncisoSiniestroDTO(Long incisoContinuityId, String numeroPoliza, Integer cvePolizaMidas, String numPolizaSeycos,BigDecimal idToPoliza, Short claveEstatus, Date fechaIniVigencia, Date fechaFinVigencia, Integer numeroInciso, Date validOn,
			Date recordFrom, String nombreContratante, String numeroSerie, String marca,
			String tipoVehiculo, String placa, String numeroMotor, Short modeloVehiculo, Object estatus, 
			Boolean provieneSeguroObligatorio, String numeroFolio) {
		super();
		this.incisoContinuityId = incisoContinuityId;
		this.numeroPoliza = numeroPoliza;
		this.cvePolizaMidas= cvePolizaMidas;
		this.numPolizaSeycos= numPolizaSeycos;
		this.idToPoliza = idToPoliza;
		this.claveEstatus = claveEstatus;
		this.fechaIniVigencia = fechaIniVigencia;
		this.fechaFinVigencia = fechaFinVigencia;
		this.numeroInciso = numeroInciso;
		this.validOn = validOn;
		this.recordFrom = recordFrom;
		this.nombreContratante = nombreContratante;
		this.numeroSerie = numeroSerie;
		this.marca = marca;
		this.tipoVehiculo = tipoVehiculo;
		this.placa = placa;
		this.numeroMotor = numeroMotor;
		this.modeloVehiculo = modeloVehiculo;
		if (estatus != null) {
			this.estatus = new Short(String.valueOf(estatus));
		}
		
		if (this.validOnMillis == null && this.validOn != null) {
			this.validOnMillis = this.validOn.getTime();
		}
		
		if (this.recordFromMillis == null && this.recordFrom != null) {
			this.recordFromMillis = this.recordFrom.getTime();
		}
		this.provieneSeguroObligatorio = provieneSeguroObligatorio;
		this.numeroFolio = numeroFolio;
	}
	


	public Long getIncisoContinuityId() {
		return incisoContinuityId;
	}

	public void setIncisoContinuityId(Long incisoContinuityId) {
		this.incisoContinuityId = incisoContinuityId;
	}

	public Cotizacion getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(Cotizacion cotizacion) {
		this.cotizacion = cotizacion;
	}

	public AutoInciso getAutoInciso() {
		return autoInciso;
	}

	public void setAutoInciso(AutoInciso autoInciso) {
		this.autoInciso = autoInciso;
	}

	public String getDescEstatus() {
		return descEstatus;
	}

	public void setDescEstatus(String descEstatus) {
		this.descEstatus = descEstatus;
	}

	public Boolean getServicioPublico() {
		return servicioPublico;
	}

	public void setServicioPublico(Boolean servicioPublico) {
		this.servicioPublico = servicioPublico;
	}

	public Boolean getServicioParticular() {
		return servicioParticular;
	}

	public void setServicioParticular(Boolean servicioParticular) {
		this.servicioParticular = servicioParticular;
	}

	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	public Date getFechaIniVigencia() {
		return fechaIniVigencia;
	}

	public void setFechaIniVigencia(Date fechaIniVigencia) {
		this.fechaIniVigencia = fechaIniVigencia;
	}

	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}

	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	public Integer getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public String getNumeroCotizacion() {
		return numeroCotizacion;
	}

	public void setNumeroCotizacion(String numeroCotizacion) {
		this.numeroCotizacion = numeroCotizacion;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public Date getFechaReporteSiniestro() {
		return fechaReporteSiniestro;
	}

	public void setFechaReporteSiniestro(Date fechaReporteSiniestro) {
		this.fechaReporteSiniestro = fechaReporteSiniestro;
	}

	public DatosContratanteDTO getDatosContratante() {
		return datosContratante;
	}

	public void setDatosContratante(DatosContratanteDTO datosContratante) {
		this.datosContratante = datosContratante;
	}

	public DatosPersonalizacionDTO getDatosPersonalizacion() {
		return datosPersonalizacion;
	}

	public void setDatosPersonalizacion(
			DatosPersonalizacionDTO datosPersonalizacion) {
		this.datosPersonalizacion = datosPersonalizacion;
	}

	public Date getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public String getNumeroPolizaAnterior() {
		return numeroPolizaAnterior;
	}

	public void setNumeroPolizaAnterior(String numeroPolizaAnterior) {
		this.numeroPolizaAnterior = numeroPolizaAnterior;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getMedioPago() {
		return medioPago;
	}

	public void setMedioPago(String medioPago) {
		this.medioPago = medioPago;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}

	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	public Date getValidOn() {
		return validOn;
	}

	public void setValidOn(Date validOn) {
		this.validOn = validOn;
	}

	public Date getRecordFrom() {
		return recordFrom;
	}

	public void setRecordFrom(Date recordFrom) {
		this.recordFrom = recordFrom;
	}

	public BigDecimal getIdToSolicitud() {
		return idToSolicitud;
	}

	public void setIdToSolicitud(BigDecimal idToSolicitud) {
		this.idToSolicitud = idToSolicitud;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public BigDecimal getIdToSolicitudDataEnTramite() {
		return idToSolicitudDataEnTramite;
	}

	public void setIdToSolicitudDataEnTramite(
			BigDecimal idToSolicitudDataEnTramite) {
		this.idToSolicitudDataEnTramite = idToSolicitudDataEnTramite;
	}

	/**
	 * @param tieneProrroga
	 *            the tieneProrroga to set
	 */
	public void setTieneProrroga(Boolean tieneProrroga) {
		this.tieneProrroga = tieneProrroga;
	}

	/**
	 * @return the tieneProrroga
	 */
	public Boolean getTieneProrroga() {
		return tieneProrroga;
	}

	public String getNombreLinea() {
		return nombreLinea;
	}

	public void setNombreLinea(String nombreLinea) {
		this.nombreLinea = nombreLinea;
	}

	public String getAgente() {
		return agente;
	}

	public void setAgente(String agente) {
		this.agente = agente;
	}

	public Short getEstatus() {
		return estatus;
	}

	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}

	public Long getValidOnMillis() {
		return validOnMillis;
	}

	public void setValidOnMillis(Long validOnMillis) {
		this.validOnMillis = validOnMillis;
	}

	public Long getRecordFromMillis() {
		return recordFromMillis;
	}

	public void setRecordFromMillis(Long recordFromMillis) {
		this.recordFromMillis = recordFromMillis;
	}

	public String getNombreContratante() {
		return nombreContratante;
	}

	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}

	public Boolean getNoAplicaDepuracion() {
		return noAplicaDepuracion;
	}

	public void setNoAplicaDepuracion(Boolean noAplicaDepuracion) {
		this.noAplicaDepuracion = noAplicaDepuracion;
	}

	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getTipoVehiculo() {
		return tipoVehiculo;
	}

	public void setTipoVehiculo(String tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getNumeroMotor() {
		return numeroMotor;
	}

	public void setNumeroMotor(String numeroMotor) {
		this.numeroMotor = numeroMotor;
	}

	public Short getModeloVehiculo() {
		return modeloVehiculo;
	}

	public void setModeloVehiculo(Short modeloVehiculo) {
		this.modeloVehiculo = modeloVehiculo;
	}

	public Boolean getProvieneSeguroObligatorio() {
		return provieneSeguroObligatorio;
	}

	public void setProvieneSeguroObligatorio(Boolean provieneSeguroObligatorio) {
		this.provieneSeguroObligatorio = provieneSeguroObligatorio;
	}
	
	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	public String getNumPolizaSeycos() {
		return numPolizaSeycos;
	}

	public void setNumPolizaSeycos(String numPolizaSeycos) {
		this.numPolizaSeycos = numPolizaSeycos;
	}
	public Integer getCvePolizaMidas() {
		return cvePolizaMidas;
	}

	public void setCvePolizaMidas(Integer cvePolizaMidas) {
		this.cvePolizaMidas = cvePolizaMidas;
	}

	public Boolean getBusquedaPolizaSeycos() {
		return busquedaPolizaSeycos;
	}

	public void setBusquedaPolizaSeycos(Boolean busquedaPolizaSeycos) {
		this.busquedaPolizaSeycos = busquedaPolizaSeycos;
	}
	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
	public Short getClaveEstatus() {
		return this.claveEstatus;
	}

	public void setClaveEstatus(Short claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	public Short getEsSeguroObligatorio() {
		return esSeguroObligatorio;
	}

	public void setEsSeguroObligatorio(Short esSeguroObligatorio) {
		this.esSeguroObligatorio = esSeguroObligatorio;
	}

	public String getNumeroFolio() {
		return numeroFolio;
	}

	public void setNumeroFolio(String numeroFolio) {
		this.numeroFolio = numeroFolio;
	}
	
	public Date getFechaVigenciaIniRealInciso() {
		return fechaVigenciaIniRealInciso;
	}

	public void setFechaVigenciaIniRealInciso(Date fechaVigenciaIniRealInciso) {
		this.fechaVigenciaIniRealInciso = fechaVigenciaIniRealInciso;
	}
	
	public Date getFechaRealEmisionPoliza() {
		return fechaRealEmisionPoliza;
	}

	public void setFechaRealEmisionPoliza(Date fechaRealEmisionPoliza) {
		this.fechaRealEmisionPoliza = fechaRealEmisionPoliza;
	}

	public Integer getNumeroSecuencia() {
		return numeroSecuencia;
	}

	public void setNumeroSecuencia(Integer numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}
	
	
	
}
