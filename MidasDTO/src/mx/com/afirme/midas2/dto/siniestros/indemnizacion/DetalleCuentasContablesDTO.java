package mx.com.afirme.midas2.dto.siniestros.indemnizacion;

import java.io.Serializable;
import java.math.BigDecimal;

public class DetalleCuentasContablesDTO implements Serializable{

	private static final long serialVersionUID = 3314802520301822569L;

	private String cuenta;
	private String moneda;
	private String concepto;
	private BigDecimal cargo;
	private BigDecimal abono;
	/**
	 * @return the cuenta
	 */
	public String getCuenta() {
		return cuenta;
	}
	/**
	 * @param cuenta the cuenta to set
	 */
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	/**
	 * @return the moneda
	 */
	public String getMoneda() {
		return moneda;
	}
	/**
	 * @param moneda the moneda to set
	 */
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	/**
	 * @return the concepto
	 */
	public String getConcepto() {
		return concepto;
	}
	/**
	 * @param concepto the concepto to set
	 */
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	/**
	 * @return the cargo
	 */
	public BigDecimal getCargo() {
		return cargo;
	}
	/**
	 * @param cargo the cargo to set
	 */
	public void setCargo(BigDecimal cargo) {
		this.cargo = cargo;
	}
	/**
	 * @return the abono
	 */
	public BigDecimal getAbono() {
		return abono;
	}
	/**
	 * @param abono the abono to set
	 */
	public void setAbono(BigDecimal abono) {
		this.abono = abono;
	}

}
