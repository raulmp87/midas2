package mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.permisos;


import java.io.Serializable;

import mx.com.afirme.midas.base.CacheableDTO;

public class ASMUserDTO  implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long userId;
	
	private String username;

	private String fullname;
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

}
