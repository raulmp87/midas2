package mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos;

import java.math.BigDecimal;
import java.util.Date;

import mx.com.afirme.midas2.util.StringUtil;

public class DepositoBancarioDTO {

	private String referencia;
	private Long bancoId;
	private Long cuentaId;
	private String numeroCuenta;
	private String refunic;
	private String descripcionMovimiento;
	private Date fechaDepositoIni;
	private Date fechaDepositoFin;
	private Date fechaDeposito;
	private BigDecimal montoIni;
	private BigDecimal montoFin;
	private BigDecimal monto;
	private String banco;
	private String depositosConcat;
	private Boolean seleccionado;
	private String identificadorGuia;
	/**
	 * @return the referencia
	 */
	public String getReferencia() {
		return referencia;
	}
	/**
	 * @param referencia the referencia to set
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	/**
	 * @return the bancoId
	 */
	public Long getBancoId() {
		return bancoId;
	}
	/**
	 * @param bancoId the bancoId to set
	 */
	public void setBancoId(Long bancoId) {
		this.bancoId = bancoId;
	}
	/**
	 * @return the numeroCuenta
	 */
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	/**
	 * @param numeroCuenta the numeroCuenta to set
	 */
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	/**
	 * @return the refunic
	 */
	public String getRefunic() {
		return refunic;
	}
	/**
	 * @param refunic the refunic to set
	 */
	public void setRefunic(String refunic) {
		this.refunic = refunic;
	}
	/**
	 * @return the descripcionMovimiento
	 */
	public String getDescripcionMovimiento() {
		return descripcionMovimiento;
	}
	/**
	 * @param descripcionMovimiento the descripcionMovimiento to set
	 */
	public void setDescripcionMovimiento(String descripcionMovimiento) {
		this.descripcionMovimiento = descripcionMovimiento;
	}
	/**
	 * @return the fechaDepositoIni
	 */
	public Date getFechaDepositoIni() {
		return fechaDepositoIni;
	}
	/**
	 * @param fechaDepositoIni the fechaDepositoIni to set
	 */
	public void setFechaDepositoIni(Date fechaDepositoIni) {
		this.fechaDepositoIni = fechaDepositoIni;
	}
	/**
	 * @return the fechaDepositoFin
	 */
	public Date getFechaDepositoFin() {
		return fechaDepositoFin;
	}
	/**
	 * @param fechaDepositoFin the fechaDepositoFin to set
	 */
	public void setFechaDepositoFin(Date fechaDepositoFin) {
		this.fechaDepositoFin = fechaDepositoFin;
	}
	/**
	 * @return the fechaDeposito
	 */
	public Date getFechaDeposito() {
		return fechaDeposito;
	}
	/**
	 * @param fechaDeposito the fechaDeposito to set
	 */
	public void setFechaDeposito(Date fechaDeposito) {
		this.fechaDeposito = fechaDeposito;
	}
	/**
	 * @return the montoIni
	 */
	public BigDecimal getMontoIni() {
		return montoIni;
	}
	/**
	 * @param montoIni the montoIni to set
	 */
	public void setMontoIni(BigDecimal montoIni) {
		this.montoIni = montoIni;
	}
	/**
	 * @return the montoFin
	 */
	public BigDecimal getMontoFin() {
		return montoFin;
	}
	/**
	 * @param montoFin the montoFin to set
	 */
	public void setMontoFin(BigDecimal montoFin) {
		this.montoFin = montoFin;
	}
	/**
	 * @return the monto
	 */
	public BigDecimal getMonto() {
		return monto;
	}
	/**
	 * @param monto the monto to set
	 */
	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}
	/**
	 * @return the banco
	 */
	public String getBanco() {
		return banco;
	}
	/**
	 * @param banco the banco to set
	 */
	public void setBanco(String banco) {
		this.banco = banco;
	}
	/**
	 * @return the depositosConcat
	 */
	public String getDepositosConcat() {
		return depositosConcat;
	}
	/**
	 * @param depositosConcat the depositosConcat to set
	 */
	public void setDepositosConcat(String depositosConcat) {
		if(!StringUtil.isEmpty(depositosConcat)){
			this.depositosConcat = depositosConcat;
		}
	}
	/**
	 * @return the seleccionado
	 */
	public Boolean getSeleccionado() {
		return seleccionado;
	}
	/**
	 * @param seleccionado the seleccionado to set
	 */
	public void setSeleccionado(Boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	public String getIdentificadorGuia() {
		return identificadorGuia;
	}
	public void setIdentificadorGuia(String identificadorGuia) {
		this.identificadorGuia = identificadorGuia;
	}
	public Long getCuentaId() {
		return cuentaId;
	}
	public void setCuentaId(Long cuentaId) {
		this.cuentaId = cuentaId;
	}
	
	
}