package mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra;

import java.io.Serializable;
import java.math.BigDecimal;

import org.springframework.stereotype.Component;

@Component
public class OrdenCompraDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1965145573745891614L;
	private String noOrdenCompra;
	private String beneficiario;
	private BigDecimal impPresupuesto;
	private BigDecimal impPagado;
	private String refacciones;
	private String estatus;
	private String acciones;
	private Long reporteCabinaId;
	private Long ordenCompraId;
	private String cveOrigen;
	private String origen;
	private boolean contieneFirma;
	private String tipoOrdenCompra ; 
	
	public String getNoOrdenCompra() {
		return noOrdenCompra;
	}
	public void setNoOrdenCompra(String noOrdenCompra) {
		this.noOrdenCompra = noOrdenCompra;
	}
	public String getBeneficiario() {
		return beneficiario;
	}
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}
	public BigDecimal getImpPresupuesto() {
		return impPresupuesto;
	}
	public void setImpPresupuesto(BigDecimal impPresupuesto) {
		this.impPresupuesto = impPresupuesto;
	}
	public BigDecimal getImpPagado() {
		return impPagado;
	}
	public void setImpPagado(BigDecimal impPagado) {
		this.impPagado = impPagado;
	}
	public String getRefacciones() {
		return refacciones;
	}
	public void setRefacciones(String refacciones) {
		this.refacciones = refacciones;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getAcciones() {
		return acciones;
	}
	public void setAcciones(String acciones) {
		this.acciones = acciones;
	}
	public Long getReporteCabinaId() {
		return reporteCabinaId;
	}
	public void setReporteCabinaId(Long reporteCabinaId) {
		this.reporteCabinaId = reporteCabinaId;
	}
	public Long getOrdenCompraId() {
		return ordenCompraId;
	}
	public void setOrdenCompraId(Long ordenCompraId) {
		this.ordenCompraId = ordenCompraId;
	}
	public String getCveOrigen() {
		return cveOrigen;
	}
	public void setCveOrigen(String cveOrigen) {
		this.cveOrigen = cveOrigen;
	}
	public String getOrigen() {
		return origen;
	}
	public void setOrigen(String origen) {
		this.origen = origen;
	}
	public boolean isContieneFirma() {
		return contieneFirma;
	}
	public void setContieneFirma(boolean contieneFirma) {
		this.contieneFirma = contieneFirma;
	}
	public String getTipoOrdenCompra() {
		return tipoOrdenCompra;
	}
	public void setTipoOrdenCompra(String tipoOrdenCompra) {
		this.tipoOrdenCompra = tipoOrdenCompra;
	}
	
	

}
