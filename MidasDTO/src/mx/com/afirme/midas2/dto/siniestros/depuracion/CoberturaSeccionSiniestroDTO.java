package mx.com.afirme.midas2.dto.siniestros.depuracion;

import java.io.Serializable;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;

import org.springframework.stereotype.Component;

@Component
public class CoberturaSeccionSiniestroDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6408792282375674979L;
	
	private String claveSubCalculo;
	private CoberturaSeccionDTO coberturaSeccionDTO;
	private String nombreCobertura;
	
	public CoberturaSeccionSiniestroDTO(){
		super();
	}
	
	public CoberturaSeccionSiniestroDTO(String claveSubCalculo,
			CoberturaSeccionDTO coberturaSeccionDTO, String nombreCobertura) {
		super();
		this.claveSubCalculo = claveSubCalculo != null ? claveSubCalculo : "";
		this.coberturaSeccionDTO = coberturaSeccionDTO;
		this.nombreCobertura = nombreCobertura;
	}
	
	public String getClaveSubCalculo() {
		return claveSubCalculo;
	}
	public void setClaveSubCalculo(String claveSubCalculo) {
		this.claveSubCalculo = claveSubCalculo;
	}
	public String getNombreCobertura() {
		return nombreCobertura;
	}
	public void setNombreCobertura(String nombreCobertura) {
		this.nombreCobertura = nombreCobertura;
	}
	public CoberturaSeccionDTO getCoberturaSeccionDTO() {
		return coberturaSeccionDTO;
	}
	public void setCoberturaSeccionDTO(CoberturaSeccionDTO coberturaSeccionDTO) {
		this.coberturaSeccionDTO = coberturaSeccionDTO;
	}
}
