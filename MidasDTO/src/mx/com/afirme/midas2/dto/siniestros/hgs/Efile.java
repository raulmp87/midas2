package mx.com.afirme.midas2.dto.siniestros.hgs;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eFile", propOrder = {
	"numFolio",
	"IDReporteSin",
    "IDAseguradoTercero",
    "IDInciso",
    "IDCobertura",
    "IDCategoria",
    "IDLineaNegocio",
    "tipoProceso",
    "estatusExpediente",
    "montoManoObra",
    "claveValuador",
    "nombreValuador",
    "fechaRefaccionesEntregadas",
    "montoRefacciones",
    "fechaIngreso",
    "fechaValuacion",
    "fechaReingreso",
    "fechaTermino",
    "montoTotalValuacion",
    "montoSubTotalValuacion",
    "montoIvaTotalValuacion",
    "valesRefacciones"
})

public class Efile implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8580791065405004170L;
	
	@XmlElement(name="id_asegurado_tercero",required=true)
	private Long IDAseguradoTercero;
	
	@XmlElement(name="monto_mano_obra",required=true)
    private double montoManoObra;
	
	@XmlElement(name="id_inciso",required=true)
    private Long IDInciso;
	
	@XmlElement(name="id_cobertura",required=true)
    private Long IDCobertura;
	
	@XmlElement(name="tipo_proceso",required=true)
    private String tipoProceso;
	
	@XmlElement(name="nombre_valuador",required=false)
    private String nombreValuador;
	
	@XmlElement(name="fecha_refacciones_entregadas",required=false)
    private Date fechaRefaccionesEntregadas;
	
	@XmlElement(name="clave_valuador",required=false)
    private String claveValuador;
	
	@XmlElement(name="monto_refacciones",required=false)
    private double montoRefacciones;
	
	@XmlElement(name="id_categoria",required=true)
    private Long IDCategoria;
	
	@XmlElement(name="fecha_reingreso",required=false)
    private Date fechaReingreso;
	
	@XmlElement(name="fecha_termino",required=false)
    private Date fechaTermino;
	
	@XmlElement(name="monto_total_valuacion",required=false)
    private double montoTotalValuacion;
	
	@XmlElement(name="valesRefacciones",required=false)
    private ValeRefaccion[] valesRefacciones;
	
	@XmlElement(name="num_folio",required=true)
    private String numFolio;
	
	@XmlElement(name="fecha_ingreso",required=true)
    private Date fechaIngreso;
	
	@XmlElement(name="fecha_valuacion",required=false)
    private Date fechaValuacion;
	
	@XmlElement(name="id_linea_negocio",required=true)
    private Long IDLineaNegocio;
	
	@XmlElement(name="id_reporte_sin",required=true)
    private Long IDReporteSin;
	
	@XmlElement(name="estatus_expediente",required=true)
    private String estatusExpediente;
	
	@XmlElement(name="monto_subtotal_valuacion",required=true)
	private double montoSubTotalValuacion;
	
	@XmlElement(name="monto_iva_total_valuacion",required=true)
	private double montoIvaTotalValuacion;
    
    
	public Long getIDAseguradoTercero() {
		return IDAseguradoTercero;
	}
	public double getMontoManoObra() {
		return montoManoObra;
	}
	public Long getIDInciso() {
		return IDInciso;
	}
	public Long getIDCobertura() {
		return IDCobertura;
	}
	public String getTipoProceso() {
		return tipoProceso;
	}
	public String getNombreValuador() {
		return nombreValuador;
	}
	public Date getFechaRefaccionesEntregadas() {
		return fechaRefaccionesEntregadas;
	}
	public String getClaveValuador() {
		return claveValuador;
	}
	public double getMontoRefacciones() {
		return montoRefacciones;
	}
	public Long getIDCategoria() {
		return IDCategoria;
	}
	public Date getFechaReingreso() {
		return fechaReingreso;
	}
	public Date getFechaTermino() {
		return fechaTermino;
	}
	public double getMontoTotalValuacion() {
		return montoTotalValuacion;
	}
	public ValeRefaccion[] getValesRefacciones() {
		return valesRefacciones;
	}
	public String getNumFolio() {
		return numFolio;
	}
	public Date getFechaIngreso() {
		return fechaIngreso;
	}
	public Date getFechaValuacion() {
		return fechaValuacion;
	}
	public Long getIDLineaNegocio() {
		return IDLineaNegocio;
	}
	public Long getIDReporteSin() {
		return IDReporteSin;
	}
	public String getEstatusExpediente() {
		return estatusExpediente;
	}
	public void setIDAseguradoTercero(Long iDAseguradoTercero) {
		IDAseguradoTercero = iDAseguradoTercero;
	}
	public void setMontoManoObra(double montoManoObra) {
		this.montoManoObra = montoManoObra;
	}
	public void setIDInciso(Long iDInciso) {
		IDInciso = iDInciso;
	}
	public void setIDCobertura(Long iDCobertura) {
		IDCobertura = iDCobertura;
	}
	public void setTipoProceso(String tipoProceso) {
		this.tipoProceso = tipoProceso;
	}
	public void setNombreValuador(String nombreValuador) {
		this.nombreValuador = nombreValuador;
	}
	public void setFechaRefaccionesEntregadas(Date fechaRefaccionesEntregadas) {
		this.fechaRefaccionesEntregadas = fechaRefaccionesEntregadas;
	}
	public void setClaveValuador(String claveValuador) {
		this.claveValuador = claveValuador;
	}
	public void setMontoRefacciones(double montoRefacciones) {
		this.montoRefacciones = montoRefacciones;
	}
	public void setIDCategoria(Long iDCategoria) {
		IDCategoria = iDCategoria;
	}
	public void setFechaReingreso(Date fechaReingreso) {
		this.fechaReingreso = fechaReingreso;
	}
	public void setFechaTermino(Date fechaTermino) {
		this.fechaTermino = fechaTermino;
	}
	public void setMontoTotalValuacion(double montoTotalValuacion) {
		this.montoTotalValuacion = montoTotalValuacion;
	}
	public void setValesRefacciones(ValeRefaccion[] valesRefacciones) {
		this.valesRefacciones = valesRefacciones;
	}
	public void setNumFolio(String numFolio) {
		this.numFolio = numFolio;
	}
	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	public void setFechaValuacion(Date fechaValuacion) {
		this.fechaValuacion = fechaValuacion;
	}
	public void setIDLineaNegocio(Long iDLineaNegocio) {
		IDLineaNegocio = iDLineaNegocio;
	}
	public void setIDReporteSin(Long iDReporteSin) {
		IDReporteSin = iDReporteSin;
	}
	public void setEstatusExpediente(String estatusExpediente) {
		this.estatusExpediente = estatusExpediente;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((IDAseguradoTercero == null) ? 0 : IDAseguradoTercero
						.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Efile other = (Efile) obj;
		if (IDAseguradoTercero == null) {
			if (other.IDAseguradoTercero != null)
				return false;
		} else if (!IDAseguradoTercero.equals(other.IDAseguradoTercero))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Efile [IDAseguradoTercero=" + IDAseguradoTercero
				+ ", montoManoObra=" + montoManoObra + ", IDInciso=" + IDInciso
				+ ", IDCobertura=" + IDCobertura + ", tipoProceso="
				+ tipoProceso + ", nombreValuador=" + nombreValuador
				+ ", fechaRefaccionesEntregadas=" + fechaRefaccionesEntregadas
				+ ", claveValuador=" + claveValuador + ", montoRefacciones="
				+ montoRefacciones + ", IDCategoria=" + IDCategoria
				+ ", fechaReingreso=" + fechaReingreso + ", fechaTermino="
				+ fechaTermino + ", montoTotalValuacion=" + montoTotalValuacion
				+ ", valesRefacciones=" + Arrays.toString(valesRefacciones)
				+ ", numFolio=" + numFolio + ", fechaIngreso=" + fechaIngreso
				+ ", fechaValuacion=" + fechaValuacion + ", IDLineaNegocio="
				+ IDLineaNegocio + ", IDReporteSin=" + IDReporteSin
				+ ", estatusExpediente=" + estatusExpediente
				+ ", montoSubTotalValuacion=" + montoSubTotalValuacion
				+ ", montoIvaTotalValuacion=" + montoIvaTotalValuacion + "]";
	}
	public double getMontoSubTotalValuacion() {
		return montoSubTotalValuacion;
	}
	public void setMontoSubTotalValuacion(double montoSubTotalValuacion) {
		this.montoSubTotalValuacion = montoSubTotalValuacion;
	}
	public double getMontoIvaTotalValuacion() {
		return montoIvaTotalValuacion;
	}
	public void setMontoIvaTotalValuacion(double montoIvaTotalValuacion) {
		this.montoIvaTotalValuacion = montoIvaTotalValuacion;
	}
    
    

}
