package mx.com.afirme.midas2.dto.siniestros.seguimiento;

import java.io.Serializable;


/**
 * Objeto para transportar Datos del Veh�culo.
 * @author Arturo
 * @version 1.0
 * @created 13-oct-2014 11:31:00 a.m.
 */
public class SeguimientoDatosVehiculo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4944013095989581705L;
	private String color;
	private String marca;
	private int modelo;
	private String numeroSerie;
	private String placas;
	private String tipo;

	public SeguimientoDatosVehiculo(){

	}

	public void finalize() throws Throwable {

	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public int getModelo() {
		return modelo;
	}

	public void setModelo(int modelo) {
		this.modelo = modelo;
	}

	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public String getPlacas() {
		return placas;
	}

	public void setPlacas(String placas) {
		this.placas = placas;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	

}