package mx.com.afirme.midas2.dto.siniestros.cabina;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.service.siniestros.cabina.AjustadorEstatusService;
import mx.com.afirme.midas2.service.siniestros.cabina.AjustadorEstatusService.ReporteEstatusDTO;

public class AjustadorEstatusDTO implements Comparator<AjustadorEstatusDTO>{
	
	private Long id;
	private String latitud;
	private String longitud;
	private Long idAjustador;
	private String nombreAjustador;
	private Boolean calcularRuta;
	private Double distanciaSiniestroKM;
	private Boolean estaAsignado;
	private String disponibilidad;
	private Date fechaReporteCoordenadas;
	private Boolean estatus;
	private Integer numeroReportesAsignados;
	private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyy HH:mm");
	private List<ReporteEstatusDTO> reportesAsignados = new ArrayList<AjustadorEstatusService.ReporteEstatusDTO>();
	

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the latitud
	 */
	public String getLatitud() {
		return latitud;
	}
	/**
	 * @param latitud the latitud to set
	 */
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	/**
	 * @return the longitud
	 */
	public String getLongitud() {
		return longitud;
	}
	/**
	 * @param longitud the longitud to set
	 */
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	/**
	 * @return the nombreAjustador
	 */
	public String getNombreAjustador() {
		return nombreAjustador;
	}
	/**
	 * @param nombreAjustador the nombreAjustador to set
	 */
	public void setNombreAjustador(String nombreAjustador) {
		this.nombreAjustador = nombreAjustador;
	}
	/**
	 * @return the calcularRuta
	 */
	public Boolean getCalcularRuta() {
		return calcularRuta;
	}
	/**
	 * @param calcularRuta the calcularRuta to set
	 */
	public void setCalcularRuta(Boolean calcularRuta) {
		this.calcularRuta = calcularRuta;
	}
	/**
	 * @return the distanciaSiniestroKM
	 */
	public Double getDistanciaSiniestroKM() {
		return distanciaSiniestroKM;
	}
	/**
	 * @param distanciaSiniestroKM the distanciaSiniestroKM to set
	 */
	public void setDistanciaSiniestroKM(Double distanciaSiniestroKM) {
		this.distanciaSiniestroKM = distanciaSiniestroKM;
	}
	
	
	public Integer getNumeroReportesAsignados() {
		return numeroReportesAsignados;
	}
	public void setNumeroReportesAsignados(Integer numeroReportesAsignados) {
		this.numeroReportesAsignados = numeroReportesAsignados;
	}
	/**
	 * @return the estaAsignado
	 */
	public Boolean getEstaAsignado() {
		return estaAsignado;
	}
	/**
	 * @param estaAsignado the estaAsignado to set
	 */
	public void setEstaAsignado(Boolean estaAsignado) {
		this.estaAsignado = estaAsignado;
	}
	@Override
	public int compare(AjustadorEstatusDTO ajustador0, AjustadorEstatusDTO ajustador1) {
		return (ajustador0.distanciaSiniestroKM < ajustador1.distanciaSiniestroKM? -1 : (ajustador0.distanciaSiniestroKM == ajustador1.distanciaSiniestroKM ? 0 : 1));
	}
	public String getDisponibilidad() {
		return disponibilidad;
	}
	public void setDisponibilidad(String disponibilidad) {
		this.disponibilidad = disponibilidad;
	}
	public Long getIdAjustador() {
		return idAjustador;
	}
	public void setIdAjustador(Long idAjustador) {
		this.idAjustador = idAjustador;
	}
	public Date getFechaReporteCoordenadas() {
		return fechaReporteCoordenadas;
	}
	public void setFechaReporteCoordenadas(Date fechaReporteCoordenadas) {
		this.fechaReporteCoordenadas = fechaReporteCoordenadas;
	}
	public String getFechaCoordenadas(){
		return fechaReporteCoordenadas != null ? dateFormat.format(fechaReporteCoordenadas) : "Desconocida";
	}
	public Boolean getEstatus() {
		return estatus;
	}
	public void setEstatus(Boolean estatus) {
		this.estatus = estatus;
	}
	public List<ReporteEstatusDTO> getReportesAsignados() {
		return reportesAsignados;
	}
	public void setReportesAsignados(List<ReporteEstatusDTO> reportesAsignados) {
		this.reportesAsignados = reportesAsignados;
	}	
	
}