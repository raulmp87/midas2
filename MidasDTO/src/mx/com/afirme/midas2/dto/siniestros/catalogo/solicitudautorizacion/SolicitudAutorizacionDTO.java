package mx.com.afirme.midas2.dto.siniestros.catalogo.solicitudautorizacion;

import java.math.BigDecimal;
import java.util.Date;

import mx.com.afirme.midas2.annotation.Exportable;



public class SolicitudAutorizacionDTO {
	
	private String comentarios;
	private Long idSolicitud;
	
	private Long idTipoSolicitud;
	
	private Long polizaId;
	
	private Long numeroInciso;
	
	private String codUsuarioCreacion;

	private String codUsuarioAutorizador;
	
	private Date fechaCreacion;
	
	private Integer estatusSolicitud;
	
	private Integer estatusTipoSolicitud;	

	private String oficina;
	
	private String tipoSolicitud;
	
	private String numeroPoliza;
	
	private BigDecimal estimacionNueva;
	
	private BigDecimal estimacionActual;
	
	private BigDecimal reserva;
	
	private String nombreCobertura ;
	
	private String nombreTipoSolicitud;
	
	private String numReporteCabina;
	
	private String noPaseAtencion;

	public static final Integer ESTATUS_RECHAZADO_ID 	= 2 ;
	public static final Integer ESTATUS_APROBADO_ID 	= 1 ;
	public static final Integer ESTATUS_PENDIENTE_ID 	= 0 ;
	
	public static final String ESTATUS_RECHAZADO_STR 	= "Rechazado" ;
	public static final String ESTATUS_APROBADO_STR 	= "Aprobado" ;
	public static final String ESTATUS_PENDIENTE_STR 	= "Pendiente" ;
	


	public Long getIdSolicitud() {
		return idSolicitud;
	}

	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	@Exportable(columnName="NUMERO SOLICITUD", columnOrder=0)
	public Long getIdTipoSolicitud() {
		return idTipoSolicitud;
	}

	public void setIdTipoSolicitud(Long idTipoSolicitud) {
		this.idTipoSolicitud = idTipoSolicitud;
	}

	public Long getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(Long polizaId) {
		this.polizaId = polizaId;
	}

	@Exportable(columnName="NUMERO INCISO", columnOrder=2)
	public Long getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(Long numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	@Exportable(columnName="USUARIO SOLICITANTE", columnOrder=3)
	public String getCodUsuarioCreacion() {
		return codUsuarioCreacion;
	}

	public void setCodUsuarioCreacion(String codUsuarioCreacion) {
		this.codUsuarioCreacion = codUsuarioCreacion;
	}

	@Exportable(columnName="USUARIO AUTORIZADOR", columnOrder=4)
	public String getCodUsuarioAutorizador() {
		return codUsuarioAutorizador;
	}

	public void setCodUsuarioAutorizador(String codUsuarioAutorizador) {
		this.codUsuarioAutorizador = codUsuarioAutorizador;
	}

	@Exportable(columnName="FECHA DE SOLICITUD",format="dd/MM/yyyy", columnOrder=5)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Integer getEstatusSolicitud() {
		return estatusSolicitud;
	}

	public void setEstatusSolicitud(Integer estatusSolicitud) {
		this.estatusSolicitud = estatusSolicitud;
	}

	public Integer getEstatusTipoSolicitud() {
		return estatusTipoSolicitud;
	}

	public void setEstatusTipoSolicitud(Integer estatusTipoSolicitud) {
		this.estatusTipoSolicitud = estatusTipoSolicitud;
	}
	
	@Exportable(columnName="AUTORIZADO", columnOrder=6)
	public String getEstatusStr(){
		String resultado = "";
		
		if( ESTATUS_RECHAZADO_ID.equals( estatusTipoSolicitud) ){
			resultado = ESTATUS_RECHAZADO_STR;
		}else if (ESTATUS_APROBADO_ID.equals(estatusTipoSolicitud)){
			resultado = ESTATUS_APROBADO_STR;
		}else if (ESTATUS_PENDIENTE_ID.equals(estatusTipoSolicitud )){
			resultado = ESTATUS_PENDIENTE_STR;
		}
		return resultado;
	}

	@Exportable(columnName="OFICINA", columnOrder=7)
	public String getOficina() {
		return oficina;
	}

	public void setOficina(String oficina) {
		this.oficina = oficina;
	}

	@Exportable(columnName="TIPO SOLICITUD", columnOrder=8)
	public String getTipoSolicitud() {
		return tipoSolicitud;
	}

	public void setTipoSolicitud(String tipoSolicitud) {
		this.tipoSolicitud = tipoSolicitud;
	}

	@Exportable(columnName="NUMERO POLIZA", columnOrder=1)
	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	@Exportable(columnName="ESTIMACION NUEVA", columnOrder=10, format="$#,##0.00")
	public BigDecimal getEstimacionNueva() {
		return estimacionNueva;
	}

	public void setEstimacionNueva(BigDecimal estimacionNueva) {
		this.estimacionNueva = estimacionNueva;
	}

	@Exportable(columnName="ESTIMACION ACTUAL", columnOrder=9, format="$#,##0.00")
	public BigDecimal getEstimacionActual() {
		return estimacionActual;
	}

	public void setEstimacionActual(BigDecimal estimacionActual) {
		this.estimacionActual = estimacionActual;
	}

	@Exportable(columnName="RESERVA", columnOrder=11, format="$#,##0.00")
	public BigDecimal getReserva() {
		return reserva;
	}

	public void setReserva(BigDecimal reserva) {
		this.reserva = reserva;
	}

	public String getNombreCobertura() {
		return nombreCobertura;
	}

	public void setNombreCobertura(String nombreCobertura) {
		this.nombreCobertura = nombreCobertura;
	}
	
	public String getNombreTipoSolicitud() {
		return nombreTipoSolicitud;
	}

	public void setNombreTipoSolicitud(String nombreTipoSolicitud) {
		this.nombreTipoSolicitud = nombreTipoSolicitud;
	}
	@Exportable(columnName="COMENTARIOS", columnOrder=12)
	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	@Exportable(columnName="REPORTE CABINA", columnOrder=13)
	public String getNumReporteCabina() {
		return numReporteCabina;
	}

	public void setNumReporteCabina(String numReporteCabina) {
		this.numReporteCabina = numReporteCabina;
	}

	@Exportable(columnName="FOLIO", columnOrder=14)
	public String getNoPaseAtencion() {
		return noPaseAtencion;
	}

	public void setNoPaseAtencion(String noPaseAtencion) {
		this.noPaseAtencion = noPaseAtencion;
	}
	
}
