package mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra;


import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.pagos.OrdenPagoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.util.DateUtils;

import org.apache.commons.lang.StringUtils;
public class BandejaOrdenCompraDTO implements Serializable {

	private static final long serialVersionUID = -6776689259245320337L;
	private String claveOficinaSiniestro;
	private String consecutivoReporteSiniestro;
	private String anioReporteSiniestro;
	
	
	private String claveReporte;
	private String conseReporte;
	private String anioReporte;
	private String numReporte;
	
	private Long idReporte;
	private Long oficinaId;
	private String oficina;
	private String numSiniestro;
	
	private String  conceptoPago;
	private Long  conceptoAjusteId;
	
	private String terminoAjuste;
	
	private Long numOrdenCompra;
	private String tipoPagoOrdenCompra;
	
	private String beneficiario;
	private String tipoPersona;
	private Integer idBeneficiario;
	private String tipoProveedor;
	private String ordenPagoSeycos;
	private String proveedor;

	private BigDecimal totalOrdenCompra;
	
	private Long reporteCabinaId;
	
	private Boolean servPublico;
	private Boolean servParticular;
	
	private Long id;

	private String idOrdenPagoSeycos;

	private String estatus;
	
	private String coberturaAfectada;
	private String pago;
	private String estatusPago;
	
	private String aplicaDeducible;
	private String idEstatus;
	
	/**
	 * TOTAL, PARCIAL
	 */
	private String tipoPago;
	
	private String tipoOrdenPago;
	
	private String tipoPagoDescripcion;
	
	private String cveTipoOrdenPago;
	private String usuarioSistema;
	
	private Date fechaPago;
	private String origenPago;
	
	/*total por pagar orden de compra */
	private BigDecimal  totalesPorPagar;
	private Date fechaOrdenPago;
	private Date fechaOrdenCompraCreacion;
	
	boolean seleccionada;
	
	private Long idDetalleLiquidacion;
	private Long idLiquidacion;
	
	private boolean pagoBloqueado;
	
	
	
	private String curp;
	private String rfc;
	private String bancoReceptor;
	private String clabe;
	private String correo;
	private String telefono;

	
	//Salvamento
	private BigDecimal salvamento = BigDecimal.ZERO;
	private BigDecimal ivaSalvamento = BigDecimal.ZERO;
	private BigDecimal totalSalvamento = BigDecimal.ZERO;
	
	//paginacion
	private Integer totalCount;
	private Integer posStart;
	private Integer count;
	private String orderByAttribute;
	
	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	public String getMotivoCancelacion() {
		return motivoCancelacion;
	}

	public void setMotivoCancelacion(String motivoCancelacion) {
		this.motivoCancelacion = motivoCancelacion;
	}

	public String getFactura() {
		return factura;
	}

	public void setFactura(String factura) {
		this.factura = factura;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public String getCancealadaPor() {
		return cancealadaPor;
	}

	public void setCancealadaPor(String cancealadaPor) {
		this.cancealadaPor = cancealadaPor;
	}



	public Date getFechaCacelacion() {
		return fechaCacelacion;
	}

	public void setFechaCacelacion(Date fechaCacelacion) {
		this.fechaCacelacion = fechaCacelacion;
	}

	public boolean isSolicitarCancelarOrdenCompra() {
		return solicitarCancelarOrdenCompra;
	}

	public void setSolicitarCancelarOrdenCompra(boolean solicitarCancelarOrdenCompra) {
		this.solicitarCancelarOrdenCompra = solicitarCancelarOrdenCompra;
	}

	/**
	 * fecha en que se sincronizo la informaci�n con seycos, esto se har� en una
	 * segudan fase para obtener la informaci�n de seycos de la orden de pago
	 */
	private Date fechaActualizacionSeycos;
	
	/**
	 * estatus de la orden en seycos, se traer� en una segunda fase mediante una
	 * inferface
	 */
	private String estatusSeycos;
	
	/**
	 * relacion con orden de compra, debe ser uno a uno
	 */
	private OrdenCompra ordenCompra;
	
	private SiniestroCabina siniestroCabina;
	
	private int dias;
	
	
	/*Cancelacion*/
	private String motivoCancelacion;
	private String factura;
	private String comentarios	;
	private String cancealadaPor 	;
	private Date fechaCacelacion 	;
	private boolean solicitarCancelarOrdenCompra 	;

	/*Acciones*/
	private boolean registrarFactura; //- Cuando no Exista Orden de Pago

	private boolean devolverFactura; //- Cuando no Exista una Factura registrada a la orden de compra.

	private boolean registrarNuevaOrdenPago; //- Cuando no exista un ID de la orden de pago relacionada a la orden de compra.

	private boolean editarOrdenPago; //- Cuando Existe el registro de una orden de pago 

	private boolean consultarOrdenPago; //- Cuando Existe el registro de una orden de pago 
	
	private boolean cancelarOrdenPago;// -   - Cuando Existe el registro de una orden de pago 
	
	private boolean bloquearPago;// -   - Cuando se pude b loquear el pago
	
	

	/*Detalle Factura*/
	private String numFactura;
	private Date fechaFactura;
	private String monedaPago;
	private Date fechaRecepcionMatriz;
	private String estatusfactura;
	private Date fechaRegistroFactura;
	private BigDecimal montoFactura;
	
	
	public BandejaOrdenCompraDTO(){
		super();
	}
	
	
	public BandejaOrdenCompraDTO(OrdenCompra ordenCompra){
		this();
		if(ordenCompra.getReporteCabina() != null){
			this.oficinaId = ordenCompra.getReporteCabina().getOficina().getId();
			this.oficina = ordenCompra.getReporteCabina().getOficina().getNombreOficina();
			this.claveOficinaSiniestro = ordenCompra.getReporteCabina().getOficina().getClaveOficina();
			this.reporteCabinaId = ordenCompra.getReporteCabina().getId();
			if(ordenCompra.getReporteCabina().getSiniestroCabina() != null){
				this.anioReporteSiniestro = ordenCompra.getReporteCabina().getSiniestroCabina().getAnioReporte();
				this.consecutivoReporteSiniestro = ordenCompra.getReporteCabina().getSiniestroCabina().getConsecutivoReporte();
				this.numSiniestro = ordenCompra.getReporteCabina().getSiniestroCabina().getNumeroSiniestro();
				this.numReporte = ordenCompra.getReporteCabina().getNumeroReporte();
			}
		}		
		
		this.idBeneficiario = ordenCompra.getIdBeneficiario();
		this.beneficiario = ordenCompra.getNomBeneficiario();		
		this.correo = ordenCompra.getCorreo();
		this.clabe = ordenCompra.getClabe();
		this.curp = ordenCompra.getCurp();		
		this.ordenCompra = ordenCompra;		
		this.numOrdenCompra = ordenCompra.getId();								
		this.cveTipoOrdenPago = ordenCompra.getTipo();
		this.conceptoPago = ordenCompra.getDetalleConceptos();
		String tel ="";
		if(!StringUtils.isEmpty(ordenCompra.getLada())){
			tel = tel.concat(ordenCompra.getLada());
		}
		if(!StringUtils.isEmpty(ordenCompra.getTelefono())){
			tel= tel.concat(ordenCompra.getTelefono());
		}
		//this.telefono(tel);
		this.coberturaAfectada = ordenCompra.getCoberturaReporteCabina() != null ? 
				ordenCompra.getCoberturaReporteCabina().getCoberturaDTO().getDescripcion() : null;	    		
	}
	
	public BandejaOrdenCompraDTO(OrdenCompra ordenCompra, OrdenPagoSiniestro ordenPago, PrestadorServicio proveedor, String estatusOrdenPago, String tipoPersona, 
			String tipoOrdenPago, String termAjuste, String tipoPagoOrdenCompra, String tipoOrdenCompra){
		this(ordenCompra);		
		this.tipoPersona = tipoPersona;  //Catalogo
		this.terminoAjuste = termAjuste; //Catalogo
		this.tipoPagoDescripcion = tipoPagoOrdenCompra; //Catalogo			
		this.tipoPago = tipoOrdenPago; //Catalogo
		this.pago =  tipoOrdenPago; //ordenPago.getTipoPago() Catalogo
		this.estatus = estatusOrdenPago; //catalogo		
		if(this.idBeneficiario != null && proveedor != null){
			this.proveedor = proveedor.getNombrePersona();
		}					
		if(ordenPago != null){		
			this.fechaOrdenPago = ordenPago.getFechaCreacion();
			this.fechaCacelacion = ordenPago.getFechaCancelacion();
			this.comentarios = ordenPago.getComentarios();
			this.estatus = ordenPago.getEstatus();
			this.motivoCancelacion = ordenPago.getMotivoCancelacion();
			this.factura = ordenPago.getFactura();
			this.fechaActualizacionSeycos = ordenPago.getFechaActualizacionSeycos();
			this.id = ordenPago.getId();
			this.origenPago = ordenPago.getOrigenPago();
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			if(ordenCompra!=null && ordenCompra.getFechaCreacion()!=null ){
				String fechaInicial = dateFormat.format(ordenCompra.getFechaCreacion());
				String fechaFinal = dateFormat.format(new Date());										
				this.dias = DateUtils.diferenciaDiasFechas(fechaInicial, fechaFinal);	
			}
									
		}
	}
	
	

	public BandejaOrdenCompraDTO(String claveOficinaSiniestro,
			String consecutivoReporteSiniestro, String anioReporteSiniestro,
			Long oficinaId, String oficina, String numSiniestro,
			String conceptoPago, String terminoAjuste,
			Long numOrdenCompra, String tipoPagoOrdenCompra,
			String beneficiario, String tipoPersona, Integer idBeneficiario,
			String tipoProveedor, String proveedor,
			Long reporteCabinaId, Long id, String estatus, String coberturaAfectada,
			String pago, String aplicaDeducible,
			String tipoPago, String tipoOrdenPago, String tipoPagoDescripcion,
			String cveTipoOrdenPago,  Date fechaPago,
			String origenPago, Date fechaOrdenPago,
			Date fechaOrdenCompraCreacion, String curp, String rfc,
			String bancoReceptor, String clabe, String correo, String telefono,
			Date fechaActualizacionSeycos, 
			OrdenCompra ordenCompra, SiniestroCabina siniestroCabina, 
			String motivoCancelacion, String factura, String comentarios,
			Date fechaCacelacion, String codigoUsuarioCancela, String numReporte, BigDecimal totalOrdenCompra) {
		this();
		this.claveOficinaSiniestro = claveOficinaSiniestro;
		this.consecutivoReporteSiniestro = consecutivoReporteSiniestro;
		this.anioReporteSiniestro = anioReporteSiniestro;
		this.oficinaId = oficinaId;
		this.oficina = oficina;
		this.numSiniestro = numSiniestro;
		this.numReporte=numReporte;
		this.totalOrdenCompra= totalOrdenCompra;
		this.conceptoPago = conceptoPago;
		this.terminoAjuste = terminoAjuste;
		this.numOrdenCompra = numOrdenCompra;
		this.tipoPagoOrdenCompra = tipoPagoOrdenCompra;
		this.beneficiario = beneficiario;
		this.tipoPersona = tipoPersona;
		this.idBeneficiario = idBeneficiario;
		this.tipoProveedor = tipoProveedor;
		this.proveedor = proveedor;
		this.reporteCabinaId = reporteCabinaId;	
		this.id = id;
		this.estatus = estatus;
		this.coberturaAfectada = coberturaAfectada;
		this.pago = pago;
		this.aplicaDeducible = aplicaDeducible;
		this.tipoPago = tipoPago;
		this.tipoOrdenPago = tipoOrdenPago;
		this.tipoPagoDescripcion = tipoPagoDescripcion;
		this.cveTipoOrdenPago = cveTipoOrdenPago;
		this.fechaPago = fechaPago;
		this.origenPago = origenPago;
		this.fechaOrdenPago = fechaOrdenPago;
		this.fechaOrdenCompraCreacion = fechaOrdenCompraCreacion;		
		this.curp = curp;
		this.rfc = rfc;
		this.bancoReceptor = bancoReceptor;
		this.clabe = clabe;
		this.correo = correo;
		//this.telefono = telefono;
		this.fechaActualizacionSeycos = fechaActualizacionSeycos;		
		this.ordenCompra = ordenCompra;
		this.siniestroCabina = siniestroCabina;		
		this.motivoCancelacion = motivoCancelacion;
		this.factura = factura;
		this.comentarios = comentarios;	
		this.fechaCacelacion = fechaCacelacion;				
		this.cancealadaPor = codigoUsuarioCancela;
		this.conceptoPago = ordenCompra.getDetalleConceptos();
		String tel ="";
		if(!StringUtils.isEmpty(ordenCompra.getLada())){
			tel = tel.concat(ordenCompra.getLada());
		}
		if(!StringUtils.isEmpty(ordenCompra.getTelefono())){
			tel= tel.concat(ordenCompra.getTelefono());
		}
		this.telefono = tel;
		
		if(this.fechaOrdenCompraCreacion != null){
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			String fechaInicial = dateFormat.format(this.fechaOrdenCompraCreacion);
			String fechaFinal = dateFormat.format(new Date());										
			this.dias = DateUtils.diferenciaDiasFechas(fechaInicial, fechaFinal);	
		}
		
	}

	public String getClaveOficinaSiniestro() {
		return claveOficinaSiniestro;
	}

	public void setClaveOficinaSiniestro(String claveOficinaSiniestro) {
		this.claveOficinaSiniestro = claveOficinaSiniestro;
	}

	public String getConsecutivoReporteSiniestro() {
		return consecutivoReporteSiniestro;
	}

	public void setConsecutivoReporteSiniestro(String consecutivoReporteSiniestro) {
		this.consecutivoReporteSiniestro = consecutivoReporteSiniestro;
	}

	public String getAnioReporteSiniestro() {
		return anioReporteSiniestro;
	}

	public void setAnioReporteSiniestro(String anioReporteSiniestro) {
		this.anioReporteSiniestro = anioReporteSiniestro;
	}

	public Long getOficinaId() {
		return oficinaId;
	}

	public void setOficinaId(Long oficinaId) {
		this.oficinaId = oficinaId;
	}

	public String getOficina() {
		return oficina;
	}

	public void setOficina(String oficina) {
		this.oficina = oficina;
	}



	public String getConceptoPago() {
		return conceptoPago;
	}

	public void setConceptoPago(String conceptoPago) {
		this.conceptoPago = conceptoPago;
	}

	public String getTerminoAjuste() {
		return terminoAjuste;
	}

	public void setTerminoAjuste(String terminoAjuste) {
		this.terminoAjuste = terminoAjuste;
	}

	public Long getNumOrdenCompra() {
		return numOrdenCompra;
	}

	public void setNumOrdenCompra(Long numOrdenCompra) {
		this.numOrdenCompra = numOrdenCompra;
	}

	public String getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	public Integer getIdBeneficiario() {
		return idBeneficiario;
	}

	public void setIdBeneficiario(Integer idBeneficiario) {
		this.idBeneficiario = idBeneficiario;
	}

	public String getTipoProveedor() {
		return tipoProveedor;
	}

	public void setTipoProveedor(String tipoProveedor) {
		this.tipoProveedor = tipoProveedor;
	}

	public String getOrdenPagoSeycos() {
		return ordenPagoSeycos;
	}

	public void setOrdenPagoSeycos(String ordenPagoSeycos) {
		this.ordenPagoSeycos = ordenPagoSeycos;
	}

	public Boolean getServPublico() {
		return servPublico;
	}

	public void setServPublico(Boolean servPublico) {
		this.servPublico = servPublico;
	}

	public Boolean getServParticular() {
		return servParticular;
	}

	public void setServParticular(Boolean servParticular) {
		this.servParticular = servParticular;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdOrdenPagoSeycos() {
		return idOrdenPagoSeycos;
	}

	public void setIdOrdenPagoSeycos(String idOrdenPagoSeycos) {
		this.idOrdenPagoSeycos = idOrdenPagoSeycos;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}

	public Date getFechaActualizacionSeycos() {
		return fechaActualizacionSeycos;
	}

	public void setFechaActualizacionSeycos(Date fechaActualizacionSeycos) {
		this.fechaActualizacionSeycos = fechaActualizacionSeycos;
	}

	public String getEstatusSeycos() {
		return estatusSeycos;
	}

	public void setEstatusSeycos(String estatusSeycos) {
		this.estatusSeycos = estatusSeycos;
	}

	public OrdenCompra getOrdenCompra() {
		return ordenCompra;
	}

	public void setOrdenCompra(OrdenCompra ordenCompra) {
		this.ordenCompra = ordenCompra;
	}

	public SiniestroCabina getSiniestroCabina() {
		return siniestroCabina;
	}

	public void setSiniestroCabina(SiniestroCabina siniestroCabina) {
		this.siniestroCabina = siniestroCabina;
	}

	public BigDecimal getTotalOrdenCompra() {
		return totalOrdenCompra;
	}

	public void setTotalOrdenCompra(BigDecimal totalOrdenCompra) {
		this.totalOrdenCompra = totalOrdenCompra;
	}

	public Long getReporteCabinaId() {
		return reporteCabinaId;
	}

	public void setReporteCabinaId(Long reporteCabinaId) {
		this.reporteCabinaId = reporteCabinaId;
	}

	public Long getConceptoAjusteId() {
		return conceptoAjusteId;
	}

	public void setConceptoAjusteId(Long conceptoAjusteId) {
		this.conceptoAjusteId = conceptoAjusteId;
	}

	public String getNumSiniestro() {
		return numSiniestro;
	}

	public void setNumSiniestro(String numSiniestro) {
		this.numSiniestro = numSiniestro;
	}

	public int getDias() {
		return dias;
	}

	public void setDias(int dias) {
		this.dias = dias;
	}

	public boolean isRegistrarFactura() {
		return registrarFactura;
	}

	public void setRegistrarFactura(boolean registrarFactura) {
		this.registrarFactura = registrarFactura;
	}

	public boolean isDevolverFactura() {
		return devolverFactura;
	}

	public void setDevolverFactura(boolean devolverFactura) {
		this.devolverFactura = devolverFactura;
	}

	public boolean isRegistrarNuevaOrdenPago() {
		return registrarNuevaOrdenPago;
	}

	public void setRegistrarNuevaOrdenPago(boolean registrarNuevaOrdenPago) {
		this.registrarNuevaOrdenPago = registrarNuevaOrdenPago;
	}

	public boolean isEditarOrdenPago() {
		return editarOrdenPago;
	}

	public void setEditarOrdenPago(boolean editarOrdenPago) {
		this.editarOrdenPago = editarOrdenPago;
	}

	public boolean isConsultarOrdenPago() {
		return consultarOrdenPago;
	}

	public void setConsultarOrdenPago(boolean consultarOrdenPago) {
		this.consultarOrdenPago = consultarOrdenPago;
	}

	public boolean isCancelarOrdenPago() {
		return cancelarOrdenPago;
	}

	public void setCancelarOrdenPago(boolean cancelarOrdenPago) {
		this.cancelarOrdenPago = cancelarOrdenPago;
	}

	public String getCoberturaAfectada() {
		return coberturaAfectada;
	}

	public void setCoberturaAfectada(String coberturaAfectada) {
		this.coberturaAfectada = coberturaAfectada;
	}

	public String getTipoOrdenPago() {
		return tipoOrdenPago;
	}

	public void setTipoOrdenPago(String tipoOrdenPago) {
		this.tipoOrdenPago = tipoOrdenPago;
	}

	public String getNumFactura() {
		return numFactura;
	}

	public void setNumFactura(String numFactura) {
		this.numFactura = numFactura;
	}

	public Date getFechaFactura() {
		return fechaFactura;
	}

	public void setFechaFactura(Date fechaFactura) {
		this.fechaFactura = fechaFactura;
	}

	public String getMonedaPago() {
		return monedaPago;
	}

	public void setMonedaPago(String monedaPago) {
		this.monedaPago = monedaPago;
	}

	public Date getFechaRecepcionMatriz() {
		return fechaRecepcionMatriz;
	}

	public void setFechaRecepcionMatriz(Date fechaRecepcionMatriz) {
		this.fechaRecepcionMatriz = fechaRecepcionMatriz;
	}

	public String getEstatusfactura() {
		return estatusfactura;
	}

	public void setEstatusfactura(String estatusfactura) {
		this.estatusfactura = estatusfactura;
	}

	public Date getFechaRegistroFactura() {
		return fechaRegistroFactura;
	}

	public void setFechaRegistroFactura(Date fechaRegistroFactura) {
		this.fechaRegistroFactura = fechaRegistroFactura;
	}

	public Date getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	public String getCveTipoOrdenPago() {
		return cveTipoOrdenPago;
	}

	public void setCveTipoOrdenPago(String cveTipoOrdenPago) {
		this.cveTipoOrdenPago = cveTipoOrdenPago;
	}

	public String getTipoPagoDescripcion() {
		return tipoPagoDescripcion;
	}

	public void setTipoPagoDescripcion(String tipoPagoDescripcion) {
		this.tipoPagoDescripcion = tipoPagoDescripcion;
	}

	public BigDecimal getMontoFactura() {
		return montoFactura;
	}

	public void setMontoFactura(BigDecimal montoFactura) {
		this.montoFactura = montoFactura;
	}

	public String getPago() {
		return pago;
	}

	public void setPago(String pago) {
		this.pago = pago;
	}

	public String getEstatusPago() {
		return estatusPago;
	}

	public void setEstatusPago(String estatusPago) {
		this.estatusPago = estatusPago;
	}

	public String getAplicaDeducible() {
		return aplicaDeducible;
	}

	public void setAplicaDeducible(String aplicaDeducible) {
		this.aplicaDeducible = aplicaDeducible;
	}

	public String getUsuarioSistema() {
		return usuarioSistema;
	}

	public void setUsuarioSistema(String usuarioSistema) {
		this.usuarioSistema = usuarioSistema;
	}

	public String getOrigenPago() {
		return origenPago;
	}

	public void setOrigenPago(String origenPago) {
		this.origenPago = origenPago;
	}

	public boolean isBloquearPago() {
		return bloquearPago;
	}

	public void setBloquearPago(boolean bloquearPago) {
		this.bloquearPago = bloquearPago;
	}

	public BigDecimal getTotalesPorPagar() {
		return totalesPorPagar;
	}

	public void setTotalesPorPagar(BigDecimal totalesPorPagar) {
		this.totalesPorPagar = totalesPorPagar;
	}

	public Date getFechaOrdenPago() {
		return fechaOrdenPago;
	}

	public void setFechaOrdenPago(Date fechaOrdenPago) {
		this.fechaOrdenPago = fechaOrdenPago;
	}

	public String getTipoPagoOrdenCompra() {
		return tipoPagoOrdenCompra;
	}

	public void setTipoPagoOrdenCompra(String tipoPagoOrdenCompra) {
		this.tipoPagoOrdenCompra = tipoPagoOrdenCompra;
	}

	public boolean isSeleccionada() {
		return seleccionada;
	}

	public void setSeleccionada(boolean seleccionada) {
		this.seleccionada = seleccionada;
	}

	public Long getIdDetalleLiquidacion() {
		return idDetalleLiquidacion;
	}

	public void setIdDetalleLiquidacion(Long idDetalleLiquidacion) {
		this.idDetalleLiquidacion = idDetalleLiquidacion;
	}

	public Long getIdLiquidacion() {
		return idLiquidacion;
	}

	public void setIdLiquidacion(Long idLiquidacion) {
		this.idLiquidacion = idLiquidacion;
	}

	public boolean isPagoBloqueado() {
		return pagoBloqueado;
	}

	public void setPagoBloqueado(boolean pagoBloqueado) {
		this.pagoBloqueado = pagoBloqueado;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getBancoReceptor() {
		return bancoReceptor;
	}

	public void setBancoReceptor(String bancoReceptor) {
		this.bancoReceptor = bancoReceptor;
	}

	public String getClabe() {
		return clabe;
	}

	public void setClabe(String clabe) {
		this.clabe = clabe;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Date getFechaOrdenCompraCreacion() {
		return fechaOrdenCompraCreacion;
	}

	public void setFechaOrdenCompraCreacion(Date fechaOrdenCompraCreacion) {
		this.fechaOrdenCompraCreacion = fechaOrdenCompraCreacion;
	}

	public String getIdEstatus() {
		return idEstatus;
	}

	public void setIdEstatus(String idEstatus) {
		this.idEstatus = idEstatus;
	}

	public BigDecimal getSalvamento() {
		return salvamento;
	}

	public void setSalvamento(BigDecimal salvamento) {
		this.salvamento = salvamento;
	}

	public BigDecimal getIvaSalvamento() {
		return ivaSalvamento;
	}

	public void setIvaSalvamento(BigDecimal ivaSalvamento) {
		this.ivaSalvamento = ivaSalvamento;
	}

	public BigDecimal getTotalSalvamento() {
		return totalSalvamento;
	}

	public void setTotalSalvamento(BigDecimal totalSalvamento) {
		this.totalSalvamento = totalSalvamento;
	}

	public String getClaveReporte() {
		return claveReporte;
	}

	public void setClaveReporte(String claveReporte) {
		this.claveReporte = claveReporte;
	}

	public String getConseReporte() {
		return conseReporte;
	}

	public void setConseReporte(String conseReporte) {
		this.conseReporte = conseReporte;
	}

	public String getAnioReporte() {
		return anioReporte;
	}

	public void setAnioReporte(String anioReporte) {
		this.anioReporte = anioReporte;
	}

	public String getNumReporte() {
		return numReporte;
	}

	public void setNumReporte(String numReporte) {
		this.numReporte = numReporte;
	}

	public Long getIdReporte() {
		return idReporte;
	}

	public void setIdReporte(Long idReporte) {
		this.idReporte = idReporte;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getPosStart() {
		return posStart;
	}

	public void setPosStart(Integer posStart) {
		this.posStart = posStart;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getOrderByAttribute() {
		return orderByAttribute;
	}

	public void setOrderByAttribute(String orderByAttribute) {
		this.orderByAttribute = orderByAttribute;
	}


	
}