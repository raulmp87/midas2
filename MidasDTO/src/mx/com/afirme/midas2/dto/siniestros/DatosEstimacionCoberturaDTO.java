/**
 * 
 */
package mx.com.afirme.midas2.dto.siniestros;

import java.io.Serializable;
import java.math.BigDecimal;

import org.springframework.stereotype.Component;

/**
 * @author simavera
 *
 */
@Component
public class DatosEstimacionCoberturaDTO implements Serializable{

	private BigDecimal estimacionActual;
	private BigDecimal reserva;
	private String causaMovimiento;
	private BigDecimal importePagado;
	private BigDecimal estimacionNueva;
	private BigDecimal sumaAseguradaAmparada;
	private Long idEstimacionCobertura;
	private BigDecimal sumaAseguradaProporcionadaOpcion1;
	private BigDecimal sumaAseguradaProporcionadaOpcion2;
	public BigDecimal getEstimacionActual() {
		return estimacionActual;
	}
	public void setEstimacionActual(BigDecimal estimacionActual) {
		this.estimacionActual = estimacionActual;
	}
	public BigDecimal getReserva() {
		return reserva;
	}
	public void setReserva(BigDecimal reserva) {
		this.reserva = reserva;
	}
	public String getCausaMovimiento() {
		return causaMovimiento;
	}
	public void setCausaMovimiento(String causaMovimiento) {
		this.causaMovimiento = causaMovimiento;
	}
	public BigDecimal getImportePagado() {
		return importePagado;
	}
	public void setImportePagado(BigDecimal importePagado) {
		this.importePagado = importePagado;
	}
	public BigDecimal getEstimacionNueva() {
		return estimacionNueva;
	}
	public void setEstimacionNueva(BigDecimal estimacionNueva) {
		this.estimacionNueva = estimacionNueva;
	}
	public BigDecimal getSumaAseguradaAmparada() {
		return sumaAseguradaAmparada;
	}
	public void setSumaAseguradaAmparada(BigDecimal sumaAseguradaAmparada) {
		this.sumaAseguradaAmparada = sumaAseguradaAmparada;
	}
	public Long getIdEstimacionCobertura() {
		return idEstimacionCobertura;
	}
	public void setIdEstimacionCobertura(Long idEstimacionCobertura) {
		this.idEstimacionCobertura = idEstimacionCobertura;
	}
	/**
	 * @return the sumaAseguradaProporcionadaOpcion1
	 */
	public BigDecimal getSumaAseguradaProporcionadaOpcion1() {
		return sumaAseguradaProporcionadaOpcion1;
	}
	/**
	 * @param sumaAseguradaProporcionadaOpcion1 the sumaAseguradaProporcionadaOpcion1 to set
	 */
	public void setSumaAseguradaProporcionadaOpcion1(
			BigDecimal sumaAseguradaProporcionadaOpcion1) {
		this.sumaAseguradaProporcionadaOpcion1 = sumaAseguradaProporcionadaOpcion1;
	}
	/**
	 * @return the sumaAseguradaProporcionadaOpcion2
	 */
	public BigDecimal getSumaAseguradaProporcionadaOpcion2() {
		return sumaAseguradaProporcionadaOpcion2;
	}
	/**
	 * @param sumaAseguradaProporcionadaOpcion2 the sumaAseguradaProporcionadaOpcion2 to set
	 */
	public void setSumaAseguradaProporcionadaOpcion2(
			BigDecimal sumaAseguradaProporcionadaOpcion2) {
		this.sumaAseguradaProporcionadaOpcion2 = sumaAseguradaProporcionadaOpcion2;
	}
	
	
}
