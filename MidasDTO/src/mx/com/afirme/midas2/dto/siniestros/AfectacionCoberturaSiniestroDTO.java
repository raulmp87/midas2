/**
 * 
 */
package mx.com.afirme.midas2.dto.siniestros;

import java.io.Serializable;
import java.math.BigDecimal;

import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ConfiguracionCalculoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ConfiguracionCoberturaSiniestro;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.util.StringUtil;

import org.springframework.stereotype.Component;

/**
 * @author simavera
 *
 */
@Component
public class AfectacionCoberturaSiniestroDTO implements Serializable{

	private static final long serialVersionUID = -3906520686251102836L;
	
	private CoberturaReporteCabina coberturaReporteCabina;
	private ConfiguracionCoberturaSiniestro configuracionCoberturaSiniestro;
	private String nombreCobertura;
	private ConfiguracionCalculoCoberturaSiniestro configuracionCalculoCobertura;
	private BigDecimal montoSumaAsegurada;
	private BigDecimal sumaAseguradaEstimada;
	private BigDecimal montoEstimado;
	private BigDecimal montoDeducible;
	private BigDecimal montoRechazo;
	private Short tieneDatosRiesgo;
	private Boolean esSiniestrable;
	private Boolean siniestrado;
	
	public String getCoberturaCompuesta() {
			
		String cveSubTipCal=OrdenCompraService.CVENULA;
		
		if (configuracionCalculoCobertura != null
				&& !StringUtil.isEmpty(configuracionCalculoCobertura.getCveSubTipoCalculoCobertura())){
			cveSubTipCal = configuracionCalculoCobertura.getCveSubTipoCalculoCobertura() ;
		}
		
		String idCobertura=OrdenCompraService.CVENULA;
		if (coberturaReporteCabina != null){
			idCobertura= coberturaReporteCabina.getId().toString();
		}
		return idCobertura+ OrdenCompraService.SEPARADOR +cveSubTipCal;
	}
	
	/**
	 * @return the coberturaReporteCabina
	 */
	public CoberturaReporteCabina getCoberturaReporteCabina() {
		return coberturaReporteCabina;
	}
	/**
	 * @param coberturaReporteCabina the coberturaReporteCabina to set
	 */
	public void setCoberturaReporteCabina(
			CoberturaReporteCabina coberturaReporteCabina) {
		this.coberturaReporteCabina = coberturaReporteCabina;
	}
	/**
	 * @return the configuracionCoberturaSiniestro
	 */
	public ConfiguracionCoberturaSiniestro getConfiguracionCoberturaSiniestro() {
		return configuracionCoberturaSiniestro;
	}
	/**
	 * @param configuracionCoberturaSiniestro the configuracionCoberturaSiniestro to set
	 */
	public void setConfiguracionCoberturaSiniestro(
			ConfiguracionCoberturaSiniestro configuracionCoberturaSiniestro) {
		this.configuracionCoberturaSiniestro = configuracionCoberturaSiniestro;
	}
	/**
	 * @return the nombreCobertura
	 */
	public String getNombreCobertura() {
		return nombreCobertura;
	}
	/**
	 * @param nombreCobertura the nombreCobertura to set
	 */
	public void setNombreCobertura(String nombreCobertura) {
		this.nombreCobertura = nombreCobertura;
	}
	/**
	 * @return the configuracionCalculoCobertura
	 */
	public ConfiguracionCalculoCoberturaSiniestro getConfiguracionCalculoCobertura() {
		return configuracionCalculoCobertura;
	}
	/**
	 * @param configuracionCalculoCobertura the configuracionCalculoCobertura to set
	 */
	public void setConfiguracionCalculoCobertura(
			ConfiguracionCalculoCoberturaSiniestro configuracionCalculoCobertura) {
		this.configuracionCalculoCobertura = configuracionCalculoCobertura;
	}
	public BigDecimal getMontoSumaAsegurada() {
		return montoSumaAsegurada;
	}
	public void setMontoSumaAsegurada(BigDecimal montoSumaAsegurada) {
		this.montoSumaAsegurada = montoSumaAsegurada;
	}
	public BigDecimal getSumaAseguradaEstimada() {
		return sumaAseguradaEstimada;
	}
	public void setSumaAseguradaEstimada(BigDecimal sumaAseguradaEstimada) {
		this.sumaAseguradaEstimada = sumaAseguradaEstimada;
	}
	
	public BigDecimal getMontoEstimado() {
		return montoEstimado;
	}
	public void setMontoEstimado(BigDecimal montoEstimado) {
		this.montoEstimado = montoEstimado;
	}
	
	public String getIdCoberturaTipoAfectacion(){
		return coberturaReporteCabina.getId() + "-" + configuracionCalculoCobertura.getTipoEstimacion(); 
	}
	public Short getTieneDatosRiesgo() {
		return tieneDatosRiesgo;
	}
	public void setTieneDatosRiesgo(Short tieneDatosRiesgo) {
		this.tieneDatosRiesgo = tieneDatosRiesgo;
	}
	public Boolean getEsSiniestrable() {
		return esSiniestrable;
	}
	public void setEsSiniestrable(Boolean esSiniestrable) {
		this.esSiniestrable = esSiniestrable;
	}
	
	
	public Boolean getSiniestrado() {
		return siniestrado;
	}
	public void setSiniestrado(Boolean siniestrado) {
		this.siniestrado = siniestrado;
	}
	
	public BigDecimal getMontoDeducible() {
		return montoDeducible;
	}
	public void setMontoDeducible(BigDecimal montoDeducible) {
		this.montoDeducible = montoDeducible;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AfectacionCoberturaSiniestroDTO)) {
			return false;
		}
		AfectacionCoberturaSiniestroDTO other = (AfectacionCoberturaSiniestroDTO) obj;
		if (coberturaReporteCabina == null) {
			if (other.coberturaReporteCabina != null) {
				return false;
			}
		} else if (!coberturaReporteCabina.getCoberturaDTO().getIdToCobertura().equals(
				other.coberturaReporteCabina.getCoberturaDTO().getIdToCobertura())){
			return false;
		}
		return true;
	}

	public BigDecimal getMontoRechazo() {
		return this.montoRechazo;
	}

	public void setMontoRechazo(BigDecimal montoRechazo) {
		this.montoRechazo = montoRechazo;
	}

}