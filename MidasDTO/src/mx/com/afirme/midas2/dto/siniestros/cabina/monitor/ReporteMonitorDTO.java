package mx.com.afirme.midas2.dto.siniestros.cabina.monitor;

import java.util.Date;

import mx.com.afirme.midas2.annotation.Exportable;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;

public class ReporteMonitorDTO {

	public enum EnumSeccion {
		ASIGNACION, ARRIBO, TERMINO, TODOS
	}

	private Long		id;
	private Oficina		oficina;
	private String		consecutivo;
	private String		anioReporte;
	private EnumSeccion	seccion;
	private String		numeroReporte;
	private Date		fechaHoraReporte;
	private Date		tiempoTranscurido;
	private Date		tiempoParaAlarma;
	private String		tipoAlarma;
	private String		personaReporta;
	private String		telefono;
	private String		ciudad;
	private String		ubicacion;
	private String		ajustador;
	private String		citaCruzero;
	private Date		fechaHoraCita;
	private Date		fechaHoraContacto;
	private String		asegurado;
	private String		agente;
	private String		estatus;
	private String		tiempoExcel;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public Oficina getOficina() {
		return oficina;
	}

	public void setOficina(Oficina oficina) {
		this.oficina = oficina;
	}

	public String getConsecutivo() {
		return consecutivo;
	}

	public void setConsecutivo(String consecutivo) {
		this.consecutivo = consecutivo;
	}

	public String getAnioReporte() {
		return anioReporte;
	}

	public void setAnioReporte(String anioReporte) {
		this.anioReporte = anioReporte;
	}

	public void setSeccion(EnumSeccion seccion) {
		this.seccion = seccion;
	}

	public EnumSeccion getSeccion() {
		return seccion;
	}

	@Exportable(columnName = "Sección", columnOrder = 0)
	public String getSeccionString() {
		return seccion.toString();
	}

	@Exportable(columnName = "No. Reporte", columnOrder = 1)
	public String getNumeroReporte() {
		return numeroReporte;
	}

	public void setNumeroReporte(String numeroReporte) {
		this.numeroReporte = numeroReporte;
	}

	@Exportable(columnName = "Fecha de Reporte", format = "dd/MM/yyyy hh:mm a", columnOrder = 2)
	public Date getFechaHoraReporte() {
		return fechaHoraReporte;
	}

	public void setFechaHoraReporte(Date fechaHoraReporte) {
		this.fechaHoraReporte = fechaHoraReporte;
	}

	public Date getTiempoTranscurido() {
		return tiempoTranscurido;
	}

	@Exportable(columnName = "Tiempo", columnOrder = 3)
	public String getTiempoExcel() {
		return tiempoExcel;
	}	

	public void setTiempoExcel(String tiempoExcel) {
		this.tiempoExcel = tiempoExcel;
	}

	public void setTiempoTranscurido(Date tiempoTranscurido) {
		this.tiempoTranscurido = tiempoTranscurido;
	}

	public void setTiempoParaAlarma(Date tiempoParaAlarma) {
		this.tiempoParaAlarma = tiempoParaAlarma;
	}

	public void setTipoAlarma(String tipoAlarma) {
		this.tipoAlarma = tipoAlarma;
	}

	public String getTipoAlarma() {
		return tipoAlarma;
	}

	public Date getTiempoParaAlarma() {
		return tiempoParaAlarma;
	}

	@Exportable(columnName = "Reporta", columnOrder = 4)
	public String getPersonaReporta() {
		return personaReporta;
	}

	public void setPersonaReporta(String personaReporta) {
		this.personaReporta = personaReporta;
	}

	@Exportable(columnName = "Teléfono", columnOrder = 5)
	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	@Exportable(columnName = "Ciudad", columnOrder = 6)
	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	@Exportable(columnName = "Ubicación", columnOrder = 7)
	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	@Exportable(columnName = "Ajustador", columnOrder = 8)
	public String getAjustador() {
		return ajustador;
	}

	public void setAjustador(String ajustador) {
		this.ajustador = ajustador;
	}

	@Exportable(columnName = "Cita/Crucero", columnOrder = 9)
	public String getCitaCruzero() {
		return citaCruzero;
	}

	public void setCitaCruzero(String citaCruzero) {
		this.citaCruzero = citaCruzero;
	}

	@Exportable(columnName = "Hora Cita", format = "dd/MM/yyyy hh:mm a", columnOrder = 10)
	public Date getFechaHoraCita() {
		return fechaHoraCita;
	}

	public void setFechaHoraCita(Date fechaHoraCita) {
		this.fechaHoraCita = fechaHoraCita;
	}

	@Exportable(columnName = "Hora Contacto", format = "dd/MM/yyyy hh:mm a", columnOrder = 11)
	public Date getFechaHoraContacto() {
		return fechaHoraContacto;
	}

	public void setFechaHoraContacto(Date fechaHoraContacto) {
		this.fechaHoraContacto = fechaHoraContacto;
	}

	@Exportable(columnName = "Asegurado", columnOrder = 12)
	public String getAsegurado() {
		return asegurado;
	}

	public void setAsegurado(String asegurado) {
		this.asegurado = asegurado;
	}

	@Exportable(columnName = "Agente", columnOrder = 13)
	public String getAgente() {
		return agente;
	}

	public void setAgente(String agente) {
		this.agente = agente;
	}

	@Exportable(columnName = "Estatus", columnOrder = 14)
	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
}
