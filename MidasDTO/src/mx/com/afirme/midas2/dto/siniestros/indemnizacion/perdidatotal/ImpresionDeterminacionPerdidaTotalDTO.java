package mx.com.afirme.midas2.dto.siniestros.indemnizacion.perdidatotal;

import java.io.Serializable;
import java.util.Date;

import mx.com.afirme.midas2.domain.siniestros.indemnizacion.IndemnizacionSiniestro;

import org.springframework.stereotype.Component;

@Component
public class ImpresionDeterminacionPerdidaTotalDTO implements Serializable{

	private static final long serialVersionUID = -1279015444484542199L;

	private DeterminacionInformacionSiniestroDTO informacionSiniestro;
	private IndemnizacionSiniestro indemnizacion;
	private Date fechaDeterminacion;
	/**
	 * @return the indemnizacion
	 */
	public IndemnizacionSiniestro getIndemnizacion() {
		return indemnizacion;
	}
	/**
	 * @param indemnizacion the indemnizacion to set
	 */
	public void setIndemnizacion(IndemnizacionSiniestro indemnizacion) {
		this.indemnizacion = indemnizacion;
	}
	/**
	 * @return the fechaDeterminacion
	 */
	public Date getFechaDeterminacion() {
		return fechaDeterminacion;
	}
	/**
	 * @param fechaDeterminacion the fechaDeterminacion to set
	 */
	public void setFechaDeterminacion(Date fechaDeterminacion) {
		this.fechaDeterminacion = fechaDeterminacion;
	}
	/**
	 * @return the informacionSiniestro
	 */
	public DeterminacionInformacionSiniestroDTO getInformacionSiniestro() {
		return informacionSiniestro;
	}
	/**
	 * @param informacionSiniestro the informacionSiniestro to set
	 */
	public void setInformacionSiniestro(
			DeterminacionInformacionSiniestroDTO informacionSiniestro) {
		this.informacionSiniestro = informacionSiniestro;
	}

}