package mx.com.afirme.midas2.dto.siniestros.catalogos;

import java.io.File;
import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;



public class ServicioSiniestrosDTO implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// # ALTA SERVICIO
	private Long    id;
	private Long    idDatoContacto;
	private Long    idDatosDireccion;
	private Long    noServicio;   
	private Long    prestadorId;   
	private Date    fechaCreacion;  
	private Date    fechaActivo;
	private Date    fechaInactivo;
	private String  nombreRazon = "";
	@NotNull
	private String  nombrePersona = "";
	@NotNull
	private String  apellidoPaterno = "";
	@NotNull
	private String  apellidoMaterno = "";  
	private Short   ambitoPrestadorServicio;
	@NotNull
	private Long    oficinaId; 
	private Short   estatus;
	@NotNull
	private String  idPaisName = "";
	@NotNull
	private String  idEstadoName = "";
	@NotNull
	private String  idCiudadName = "";
	private String  idColoniaName = "";  
	private String  calleName ="";
	private String  numeroName = "";
	private String  numeroIntName = "";
	private String  cpName = "";
	private Boolean idColoniaCheckName;
	private String  nuevaColoniaName="";
	@NotNull
	@Min(3)
	private String  telCasaLada = "";
	@NotNull
	private String  telCasa = "";
	private String  telCasa2 = "";
	private String  telCasaLada2 = "";
	private String  telOtro1 = "";
	private String  telOtro2 = "";
	private String  telOtro1Lada = "";
	private String  telOtro2Lada = "";
	private String  telCelularLada = "";
	private String  telCelular = "";
	private String  correoPrincipal = "";
	private String  colonia = "";
	private Short   tipoOperacion;
	private Short   certificacion;
	private Short   estatusBase;
	private Long    idPersonaMidas;
	private String  referencia = "";
	private String tipoPersona = "";
	private String nombreDeLaEmpresa = "";
	private String nombreComercial = "";
	private String administrador = "";
	private String rfc = "";
	private String claveUsuario;
	
	private String codigoUnidad;
	private String placas;
	private String descripcionUnidad;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getNoServicio() {
		return noServicio;
	}
	public void setNoServicio(Long noServicio) {
		this.noServicio = noServicio;
	}
	public Long getPrestadorId() {
		return prestadorId;
	}
	public void setPrestadorId(Long prestadorId) {
		this.prestadorId = prestadorId;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getNombrePersona() {
		return nombrePersona;
	}
	public void setNombrePersona(String nombrePersona) {
		this.nombrePersona = nombrePersona;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public Short getAmbitoPrestadorServicio() {
		return ambitoPrestadorServicio;
	}
	public void setAmbitoPrestadorServicio(Short ambitoPrestadorServicio) {
		this.ambitoPrestadorServicio = ambitoPrestadorServicio;
	}
	public Short getEstatus() {
		return estatus;
	}
	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}
	public String getTelCasaLada() {
		return telCasaLada;
	}
	public void setTelCasaLada(String telCasaLada) {
		this.telCasaLada = telCasaLada;
	}
	public String getTelCasa() {
		return telCasa;
	}
	public void setTelCasa(String telCasa) {
		this.telCasa = telCasa;
	}
	public String getTelOtro1Lada() {
		return telOtro1Lada;
	}
	public void setTelOtro1Lada(String telOtro1Lada) {
		this.telOtro1Lada = telOtro1Lada;
	}
	public String getTelOtro2Lada() {
		return telOtro2Lada;
	}
	public void setTelOtro2Lada(String telOtro2Lada) {
		this.telOtro2Lada = telOtro2Lada;
	}
	public String getTelCelular() {
		return telCelular;
	}
	public void setTelCelular(String telCelular) {
		this.telCelular = telCelular;
	}
	public String getCorreoPrincipal() {
		return correoPrincipal;
	}
	public void setCorreoPrincipal(String correoPrincipal) {
		this.correoPrincipal = correoPrincipal;
	}
	public Long getOficinaId() {
		return oficinaId;
	}
	public void setOficinaId(Long oficinaId) {
		this.oficinaId = oficinaId;
	}
	public String getTelOtro1() {
		return telOtro1;
	}
	public void setTelOtro1(String telOtro1) {
		this.telOtro1 = telOtro1;
	}
	public String getTelOtro2() {
		return telOtro2;
	}
	public void setTelOtro2(String telOtro2) {
		this.telOtro2 = telOtro2;
	}
	public String getColonia() {
		return colonia;
	}
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	public Short getTipoOperacion() {
		return tipoOperacion;
	}
	public void setTipoOperacion(Short tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	public String getNombreRazon() {
		return nombreRazon;
	}
	public void setNombreRazon(String nombreRazon) {
		this.nombreRazon = nombreRazon;
	}
	public Short getCertificacion() {
		return certificacion;
	}
	public void setCertificacion(Short certificacion) {
		this.certificacion = certificacion;
	}
	public Date getFechaActivo() {
		return fechaActivo;
	}
	public void setFechaActivo(Date fechaActivo) {
		this.fechaActivo = fechaActivo;
	}
	public Date getFechaInactivo() {
		return fechaInactivo;
	}
	public void setFechaInactivo(Date fechaInactivo) {
		this.fechaInactivo = fechaInactivo;
	}
	public String getTelCelularLada() {
		return telCelularLada;
	}
	public void setTelCelularLada(String telCelularLada) {
		this.telCelularLada = telCelularLada;
	}

	public Short getEstatusBase() {
		return estatusBase;
	}
	public void setEstatusBase(Short estatusBase) {
		this.estatusBase = estatusBase;
	}
	public String getIdPaisName() {
		return idPaisName;
	}
	public void setIdPaisName(String idPaisName) {
		this.idPaisName = idPaisName;
	}
	public String getIdEstadoName() {
		return idEstadoName;
	}
	public void setIdEstadoName(String idEstadoName) {
		this.idEstadoName = idEstadoName;
	}
	public String getIdCiudadName() {
		return idCiudadName;
	}
	public void setIdCiudadName(String idCiudadName) {
		this.idCiudadName = idCiudadName;
	}
	public String getIdColoniaName() {
		return idColoniaName;
	}
	public void setIdColoniaName(String idColoniaName) {
		this.idColoniaName = idColoniaName;
	}
	public String getCpName() {
		return cpName;
	}
	public void setCpName(String cpName) {
		this.cpName = cpName;
	}
	public String getNuevaColoniaName() {
		return nuevaColoniaName;
	}
	public void setNuevaColoniaName(String nuevaColoniaName) {
		this.nuevaColoniaName = nuevaColoniaName;
	}
	public Long getIdPersonaMidas() {
		return idPersonaMidas;
	}
	public void setIdPersonaMidas(Long idPersonaMidas) {
		this.idPersonaMidas = idPersonaMidas;
	}
	public Long getIdDatoContacto() {
		return idDatoContacto;
	}
	public Long getIdDatosDireccion() {
		return idDatosDireccion;
	}
	public void setIdDatoContacto(Long idDatoContacto) {
		this.idDatoContacto = idDatoContacto;
	}
	public void setIdDatosDireccion(Long idDatosDireccion) {
		this.idDatosDireccion = idDatosDireccion;
	}
	public String getCalleName() {
		return calleName;
	}
	public String getNumeroName() {
		return numeroName;
	}
	public String getNumeroIntName() {
		return numeroIntName;
	}
	public void setCalleName(String calleName) {
		this.calleName = calleName;
	}
	public void setNumeroName(String numeroName) {
		this.numeroName = numeroName;
	}
	public void setNumeroIntName(String numeroIntName) {
		this.numeroIntName = numeroIntName;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public Boolean getIdColoniaCheckName() {
		return idColoniaCheckName;
	}
	public void setIdColoniaCheckName(Boolean idColoniaCheckName) {
		this.idColoniaCheckName = idColoniaCheckName;
	}
	public String getTelCasa2() {
		return telCasa2;
	}
	public String getTelCasaLada2() {
		return telCasaLada2;
	}
	public void setTelCasa2(String telCasa2) {
		this.telCasa2 = telCasa2;
	}
	public void setTelCasaLada2(String telCasaLada2) {
		this.telCasaLada2 = telCasaLada2;
	}
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	public String getTipoPersona() {
		return tipoPersona;
	}
	public void setNombreDeLaEmpresa(String nombreDeLaEmpresa) {
		this.nombreDeLaEmpresa = nombreDeLaEmpresa;
	}
	public String getNombreDeLaEmpresa() {
		return nombreDeLaEmpresa;
	}
	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}
	public String getNombreComercial() {
		return nombreComercial;
	}
	public void setAdministrador(String administrador) {
		this.administrador = administrador;
	}
	public String getAdministrador() {
		return administrador;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getRfc() {
		return rfc;
	}
	public String getClaveUsuario() {
		return claveUsuario;
	}
	public void setClaveUsuario(String claveUsuario) {
		this.claveUsuario = claveUsuario;
	}
	public String getCodigoUnidad() {
		return codigoUnidad;
	}
	public void setCodigoUnidad(String codigoUnidad) {
		this.codigoUnidad = codigoUnidad;
	}
	public String getPlacas() {
		return placas;
	}
	public void setPlacas(String placas) {
		this.placas = placas;
	}
	public String getDescripcionUnidad() {
		return descripcionUnidad;
	}
	public void setDescripcionUnidad(String descripcionUnidad) {
		this.descripcionUnidad = descripcionUnidad;
	}
}
