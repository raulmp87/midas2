package mx.com.afirme.midas2.dto.siniestros.reportecondiciones;

import java.math.BigDecimal;

public class ReporteCondicionesSiniestroDTO {
	
	private String numeroPoliza;
	private String codigoCondicion;
	private String nombreCondicion;
	private BigDecimal siniestralidad;
	private BigDecimal costoSiniestros;
	private BigDecimal porcPartCosto;//Porcentaje de Participacion en Costo
	private Long numeroSiniestros;
	private BigDecimal porcPartNumSiniestros;//Porcentaje de Participacion en Numero de Siniestros
	private BigDecimal primaDevengada;
	private Long numeroPolizas;
	
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public String getCodigoCondicion() {
		return codigoCondicion;
	}
	public void setCodigoCondicion(String codigoCondicion) {
		this.codigoCondicion = codigoCondicion;
	}
	public String getNombreCondicion() {
		return nombreCondicion;
	}
	public void setNombreCondicion(String nombreCondicion) {
		this.nombreCondicion = nombreCondicion;
	}
	public BigDecimal getSiniestralidad() {
		return siniestralidad;
	}
	public void setSiniestralidad(BigDecimal siniestralidad) {
		this.siniestralidad = siniestralidad;
	}
	public BigDecimal getCostoSiniestros() {
		return costoSiniestros;
	}
	public void setCostoSiniestros(BigDecimal costoSiniestros) {
		this.costoSiniestros = costoSiniestros;
	}
	public BigDecimal getPorcPartCosto() {
		return porcPartCosto;
	}
	public void setPorcPartCosto(BigDecimal porcPartCosto) {
		this.porcPartCosto = porcPartCosto;
	}
	public Long getNumeroSiniestros() {
		return numeroSiniestros;
	}
	public void setNumeroSiniestros(Long numeroSiniestros) {
		this.numeroSiniestros = numeroSiniestros;
	}
	public BigDecimal getPorcPartNumSiniestros() {
		return porcPartNumSiniestros;
	}
	public void setPorcPartNumSiniestros(BigDecimal porcPartNumSiniestros) {
		this.porcPartNumSiniestros = porcPartNumSiniestros;
	}
	public BigDecimal getPrimaDevengada() {
		return primaDevengada;
	}
	public void setPrimaDevengada(BigDecimal primaDevengada) {
		this.primaDevengada = primaDevengada;
	}
	public Long getNumeroPolizas() {
		return numeroPolizas;
	}
	public void setNumeroPolizas(Long numeroPolizas) {
		this.numeroPolizas = numeroPolizas;
	}	

}
