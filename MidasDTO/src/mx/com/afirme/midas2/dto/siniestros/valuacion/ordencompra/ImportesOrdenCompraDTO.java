package mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra;

import java.io.Serializable;
import java.math.BigDecimal;

import org.springframework.stereotype.Component;

@Component
public class ImportesOrdenCompraDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7327869778305648342L;
	//Cantidades
	BigDecimal subtotal;
	BigDecimal iva;
	BigDecimal isr;
	BigDecimal ivaRetenido;
	BigDecimal totalSinDescuentos;//subtotal+iva-isr-ivaRetenido
	BigDecimal deducible;
	BigDecimal descuento;
	BigDecimal totalPagado;	
	BigDecimal totalPendiente;
	BigDecimal total; // totalSinDescuentos-deducible-descuento
	
	public BigDecimal getDescuento() {
		return descuento;
	}
	public void setDescuento(BigDecimal descuento) {
		this.descuento = descuento;
	}
	public BigDecimal getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}
	public BigDecimal getIva() {
		return iva;
	}
	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}
	public BigDecimal getDeducible() {
		return deducible;
	}
	public void setDeducible(BigDecimal deducible) {
		this.deducible = deducible;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public BigDecimal getTotalPagado() {
		return totalPagado;
	}
	public void setTotalPagado(BigDecimal totalPagado) {
		this.totalPagado = totalPagado;
	}
	public BigDecimal getTotalPendiente() {
		return totalPendiente;
	}
	public void setTotalPendiente(BigDecimal totalPendiente) {
		this.totalPendiente = totalPendiente;
	}
	public BigDecimal getIsr() {
		return isr;
	}
	public void setIsr(BigDecimal isr) {
		this.isr = isr;
	}
	public BigDecimal getIvaRetenido() {
		return ivaRetenido;
	}
	public void setIvaRetenido(BigDecimal ivaRetenido) {
		this.ivaRetenido = ivaRetenido;
	}
	public BigDecimal getTotalSinDescuentos() {
		return totalSinDescuentos;
	}
	public void setTotalSinDescuentos(BigDecimal totalSinDescuentos) {
		this.totalSinDescuentos = totalSinDescuentos;
	}
	
	


}
