package mx.com.afirme.midas2.dto.siniestros;

import java.math.BigDecimal;
import java.util.Date;

import mx.com.afirme.midas2.annotation.Exportable;

public class ListarIngresoDTO {

	private Long 	claveDeudor;
	private String 	estatus;
	private String 	estatusDesc;
	private Date 	fechaCancelacion;
	private Date 	fechaFinAplicacion;
	private Date 	fechaFinCancelacion;
	private Date	fechaFinPendiente;
	private Date 	fechaIngreso;
	private Date 	fechaIniAplicacion;
	private Date 	fechaIniCancelacion;
	private Date 	fechaIniPendiente;
	private Date 	fechaRegistro;
	private Long 	ingresoId;
	private String 	ingresosConcat;
	private String 	medioRecuperacion;
	private String 	medioRecuperacionDesc;
	private BigDecimal 	montoFinalRecuperado;
	private BigDecimal montoFinIngreso;
	private BigDecimal montoFinRecuperacion;
	private BigDecimal montoIniIngreso;
	private BigDecimal montoIniRecuperacion;
	private String 	nombreDeudor;
	private Long 	numeroIngreso;
	private Long	numeroRecuperacion;
	private String 	numeroReporte;
	private String 	numeroSiniestro;
	private String 	oficinaDesc;
	private Long 	oficinaId;
	private String 	referencia;
	private String 	referenciaIdentificada;
	private Boolean seleccionado;
	private Short	servicio;
	private Short 	servicioParticular;
	private Short 	servicioPublico;
	private String 	tipoRecuperacion;
	private String 	tipoRecuperacionDesc;
	private Integer banderaBusquedaConFiltro;
	private String 	pantallaOrigen;
	private String 	estatusFacturado;
	private Integer permitirFacturacion;
	/**
	 * @return the claveDeudor
	 */
	@Exportable(columnName="NUMERO DE DEUDOR", columnOrder=4)
	public Long getClaveDeudor() {
		return claveDeudor;
	}
	/**
	 * @param claveDeudor the claveDeudor to set
	 */
	public void setClaveDeudor(Long claveDeudor) {
		this.claveDeudor = claveDeudor;
	}
	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	/**
	 * @return the estatusDesc
	 */
	@Exportable(columnName="ESTATUS", columnOrder=13)
	public String getEstatusDesc() {
		return estatusDesc;
	}
	/**
	 * @param estatusDesc the estatusDesc to set
	 */
	public void setEstatusDesc(String estatusDesc) {
		this.estatusDesc = estatusDesc;
	}
	/**
	 * @return the fechaCancelacion
	 */
	@Exportable(columnName="FECHA DE CANCELACION", columnOrder=12)
	public Date getFechaCancelacion() {
		return fechaCancelacion;
	}
	/**
	 * @param fechaCancelacion the fechaCancelacion to set
	 */
	public void setFechaCancelacion(Date fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}
	/**
	 * @return the fechaFinAplicacion
	 */
	public Date getFechaFinAplicacion() {
		return fechaFinAplicacion;
	}
	/**
	 * @param fechaFinAplicacion the fechaFinAplicacion to set
	 */
	public void setFechaFinAplicacion(Date fechaFinAplicacion) {
		this.fechaFinAplicacion = fechaFinAplicacion;
	}
	/**
	 * @return the fechaFinCancelacion
	 */
	public Date getFechaFinCancelacion() {
		return fechaFinCancelacion;
	}
	/**
	 * @param fechaFinCancelacion the fechaFinCancelacion to set
	 */
	public void setFechaFinCancelacion(Date fechaFinCancelacion) {
		this.fechaFinCancelacion = fechaFinCancelacion;
	}
	/**
	 * @return the fechaFinPendiente
	 */
	public Date getFechaFinPendiente() {
		return fechaFinPendiente;
	}
	/**
	 * @param fechaFinPendiente the fechaFinPendiente to set
	 */
	public void setFechaFinPendiente(Date fechaFinPendiente) {
		this.fechaFinPendiente = fechaFinPendiente;
	}
	/**
	 * @return the fechaIngreso
	 */
	@Exportable(columnName="FECHA DE APLICACION DEL INGRESO", columnOrder=11)
	public Date getFechaIngreso() {
		return fechaIngreso;
	}
	/**
	 * @param fechaIngreso the fechaIngreso to set
	 */
	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	/**
	 * @return the fechaIniAplicacion
	 */
	public Date getFechaIniAplicacion() {
		return fechaIniAplicacion;
	}
	/**
	 * @param fechaIniAplicacion the fechaIniAplicacion to set
	 */
	public void setFechaIniAplicacion(Date fechaIniAplicacion) {
		this.fechaIniAplicacion = fechaIniAplicacion;
	}
	/**
	 * @return the fechaIniCancelacion
	 */
	public Date getFechaIniCancelacion() {
		return fechaIniCancelacion;
	}
	/**
	 * @param fechaIniCancelacion the fechaIniCancelacion to set
	 */
	public void setFechaIniCancelacion(Date fechaIniCancelacion) {
		this.fechaIniCancelacion = fechaIniCancelacion;
	}
	/**
	 * @return the fechaRegistro
	 */
	@Exportable(columnName="FECHA DE REGISTRO INGRESO PENDIENTE", columnOrder=10)
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	/**
	 * @param fechaRegistro the fechaRegistro to set
	 */
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	/**
	 * @return the ingresoId
	 */
	public Long getIngresoId() {
		return ingresoId;
	}
	/**
	 * @param ingresoId the ingresoId to set
	 */
	public void setIngresoId(Long ingresoId) {
		this.ingresoId = ingresoId;
	}
	/**
	 * @return the ingresosConcat
	 */
	public String getIngresosConcat() {
		return ingresosConcat;
	}
	/**
	 * @param ingresosConcat the ingresosConcat to set
	 */
	public void setIngresosConcat(String ingresosConcat) {
		this.ingresosConcat = ingresosConcat;
	}
	/**
	 * @return the medioRecuperacion
	 */
	public String getMedioRecuperacion() {
		return medioRecuperacion;
	}
	/**
	 * @param medioRecuperacion the medioRecuperacion to set
	 */
	public void setMedioRecuperacion(String medioRecuperacion) {
		this.medioRecuperacion = medioRecuperacion;
	}
	/**
	 * @return the medioRecuperacionDesc
	 */
	@Exportable(columnName="MEDIO DE RECUPERACION", columnOrder=3)
	public String getMedioRecuperacionDesc() {
		return medioRecuperacionDesc;
	}
	/**
	 * @param medioRecuperacionDesc the medioRecuperacionDesc to set
	 */
	public void setMedioRecuperacionDesc(String medioRecuperacionDesc) {
		this.medioRecuperacionDesc = medioRecuperacionDesc;
	}
	/**
	 * @return the montoFinalRecuperado
	 */
	@Exportable(columnName="MONTO FINAL RECUPERADO", columnOrder=9)
	public BigDecimal getMontoFinalRecuperado() {
		return montoFinalRecuperado;
	}
	/**
	 * @param montoFinalRecuperado the montoFinalRecuperado to set
	 */
	public void setMontoFinalRecuperado(BigDecimal montoFinalRecuperado) {
		this.montoFinalRecuperado = montoFinalRecuperado;
	}
	/**
	 * @return the montoFinIngreso
	 */
	public BigDecimal getMontoFinIngreso() {
		return montoFinIngreso;
	}
	/**
	 * @param montoFinIngreso the montoFinIngreso to set
	 */
	public void setMontoFinIngreso(BigDecimal montoFinIngreso) {
		this.montoFinIngreso = montoFinIngreso;
	}
	/**
	 * @return the montoFinRecuperacion
	 */
	public BigDecimal getMontoFinRecuperacion() {
		return montoFinRecuperacion;
	}
	/**
	 * @param montoFinRecuperacion the montoFinRecuperacion to set
	 */
	public void setMontoFinRecuperacion(BigDecimal montoFinRecuperacion) {
		this.montoFinRecuperacion = montoFinRecuperacion;
	}
	/**
	 * @return the montoIniIngreso
	 */
	public BigDecimal getMontoIniIngreso() {
		return montoIniIngreso;
	}
	/**
	 * @param montoIniIngreso the montoIniIngreso to set
	 */
	public void setMontoIniIngreso(BigDecimal montoIniIngreso) {
		this.montoIniIngreso = montoIniIngreso;
	}
	/**
	 * @return the montoIniRecuperacion
	 */
	public BigDecimal getMontoIniRecuperacion() {
		return montoIniRecuperacion;
	}
	/**
	 * @param montoIniRecuperacion the montoIniRecuperacion to set
	 */
	public void setMontoIniRecuperacion(BigDecimal montoIniRecuperacion) {
		this.montoIniRecuperacion = montoIniRecuperacion;
	}
	/**
	 * @return the nombreDeudor
	 */
	@Exportable(columnName="NOMBRE DE DEUDOR", columnOrder=5)
	public String getNombreDeudor() {
		return nombreDeudor;
	}
	/**
	 * @param nombreDeudor the nombreDeudor to set
	 */
	public void setNombreDeudor(String nombreDeudor) {
		this.nombreDeudor = nombreDeudor;
	}
	/**
	 * @return the numeroIngreso
	 */
	@Exportable(columnName="NUMERO DE INGRESO", columnOrder=0)
	public Long getNumeroIngreso() {
		return numeroIngreso;
	}
	/**
	 * @param numeroIngreso the numeroIngreso to set
	 */
	public void setNumeroIngreso(Long numeroIngreso) {
		this.numeroIngreso = numeroIngreso;
	}
	/**
	 * @return the numeroReporte
	 */
	@Exportable(columnName="NUMERO DE REPORTE DE SINIESTRO", columnOrder=7)
	public String getNumeroReporte() {
		return numeroReporte;
	}
	/**
	 * @param numeroReporte the numeroReporte to set
	 */
	public void setNumeroReporte(String numeroReporte) {
		this.numeroReporte = numeroReporte;
	}
	/**
	 * @return the numeroSiniestro
	 */
	@Exportable(columnName="NUMERO DE SINIESTRO", columnOrder=6)
	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}
	/**
	 * @param numeroSiniestro the numeroSiniestro to set
	 */
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}
	/**
	 * @return the oficinaDesc
	 */
	@Exportable(columnName="OFICINA", columnOrder=8)
	public String getOficinaDesc() {
		return oficinaDesc;
	}
	/**
	 * @param oficinaDesc the oficinaDesc to set
	 */
	public void setOficinaDesc(String oficinaDesc) {
		this.oficinaDesc = oficinaDesc;
	}
	/**
	 * @return the oficinaId
	 */
	public Long getOficinaId() {
		return oficinaId;
	}
	/**
	 * @param oficinaId the oficinaId to set
	 */
	public void setOficinaId(Long oficinaId) {
		this.oficinaId = oficinaId;
	}
	/**
	 * @return the referencia
	 */
	public String getReferencia() {
		return referencia;
	}
	/**
	 * @param referencia the referencia to set
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	/**
	 * @return the seleccionado
	 */
	public Boolean getSeleccionado() {
		return seleccionado;
	}
	/**
	 * @param seleccionado the seleccionado to set
	 */
	public void setSeleccionado(Boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	/**
	 * @return the servicioParticular
	 */
	public Short getServicioParticular() {
		return servicioParticular;
	}
	/**
	 * @param servicioParticular the servicioParticular to set
	 */
	public void setServicioParticular(Short servicioParticular) {
		this.servicioParticular = servicioParticular;
	}
	/**
	 * @return the servicioPublico
	 */
	public Short getServicioPublico() {
		return servicioPublico;
	}
	/**
	 * @param servicioPublico the servicioPublico to set
	 */
	public void setServicioPublico(Short servicioPublico) {
		this.servicioPublico = servicioPublico;
	}
	/**
	 * @return the tipoRecuperacion
	 */
	public String getTipoRecuperacion() {
		return tipoRecuperacion;
	}
	/**
	 * @param tipoRecuperacion the tipoRecuperacion to set
	 */
	public void setTipoRecuperacion(String tipoRecuperacion) {
		this.tipoRecuperacion = tipoRecuperacion;
	}
	/**
	 * @return the tipoRecuperacionDesc
	 */
	@Exportable(columnName="TIPO DE RECUPERACION", columnOrder=2)
	public String getTipoRecuperacionDesc() {
		return tipoRecuperacionDesc;
	}
	/**
	 * @param tipoRecuperacionDesc the tipoRecuperacionDesc to set
	 */
	public void setTipoRecuperacionDesc(String tipoRecuperacionDesc) {
		this.tipoRecuperacionDesc = tipoRecuperacionDesc;
	}
	/**
	 * @return the fechaIniPendiente
	 */
	public Date getFechaIniPendiente() {
		return fechaIniPendiente;
	}
	/**
	 * @param fechaIniPendiente the fechaIniPendiente to set
	 */
	public void setFechaIniPendiente(Date fechaIniPendiente) {
		this.fechaIniPendiente = fechaIniPendiente;
	}
	/**
	 * @return the servicio
	 */
	public Short getServicio() {
		return servicio;
	}
	/**
	 * @param servicio the servicio to set
	 */
	public void setServicio(Short servicio) {
		this.servicio = servicio;
	}
	/**
	 * @return the numeroRecuperacion
	 */
	@Exportable(columnName="NUMERO DE RECUPERACION", columnOrder=1)
	public Long getNumeroRecuperacion() {
		return numeroRecuperacion;
	}
	/**
	 * @param numeroRecuperacion the numeroRecuperacion to set
	 */
	public void setNumeroRecuperacion(Long numeroRecuperacion) {
		this.numeroRecuperacion = numeroRecuperacion;
	}
	/**
	 * @return the banderaBusquedaConFiltro
	 */
	public Integer getBanderaBusquedaConFiltro() {
		return banderaBusquedaConFiltro;
	}
	/**
	 * @param banderaBusquedaConFiltro the banderaBusquedaConFiltro to set
	 */
	public void setBanderaBusquedaConFiltro(Integer banderaBusquedaConFiltro) {
		this.banderaBusquedaConFiltro = banderaBusquedaConFiltro;
	}
	/**
	 * @return the pantallaOrigen
	 */
	public String getPantallaOrigen() {
		return pantallaOrigen;
	}
	/**
	 * @param pantallaOrigen the pantallaOrigen to set
	 */
	public void setPantallaOrigen(String pantallaOrigen) {
		this.pantallaOrigen = pantallaOrigen;
	}
	public void setReferenciaIdentificada(String referenciaIdentificada) {
		this.referenciaIdentificada = referenciaIdentificada;
	}
	public String getReferenciaIdentificada() {
		return referenciaIdentificada;
	}
	/**
	 * @param estatusFacturado the estatusFacturado to set
	 */
	public void setEstatusFacturado(String estatusFacturado) {
		this.estatusFacturado = estatusFacturado;
	}
	/**
	 * @return the estatusFacturado
	 */
	public String getEstatusFacturado() {
		return estatusFacturado;
	}
	/**
	 * @param permitirFacturacion the permitirFacturacion to set
	 */
	public void setPermitirFacturacion(Integer permitirFacturacion) {
		this.permitirFacturacion = permitirFacturacion;
	}
	/**
	 * @return the permitirFacturacion
	 */
	public Integer getPermitirFacturacion() {
		return permitirFacturacion;
	}
}