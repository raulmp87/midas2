package mx.com.afirme.midas2.dto.siniestros.configuracion.horario.ajustador;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import mx.com.afirme.midas2.annotation.Exportable;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.ajustador.HorarioAjustador;

public class ListadoDeDisponibilidadDTO implements Serializable {

	private static final long	serialVersionUID	= -7349925882323439958L;

	private String				id;

	private String				ajustador;

	private Date				fecha;

	private HorarioAjustador	dispNormal			= new HorarioAjustador();

	private HorarioAjustador	dispGuardia			= new HorarioAjustador();

	private HorarioAjustador	dispApoyo			= new HorarioAjustador();

	private HorarioAjustador	dispDescanso		= new HorarioAjustador();

	private HorarioAjustador	dispIncapacidad		= new HorarioAjustador();

	private HorarioAjustador	dispPermiso			= new HorarioAjustador();

	private HorarioAjustador	dispVacaciones		= new HorarioAjustador();

	public HorarioAjustador[]	dispList			= { dispNormal,
			dispGuardia, dispApoyo, dispDescanso, dispDescanso,
			dispIncapacidad, dispPermiso, dispVacaciones };

	@Exportable(columnName = "Ajustador", columnOrder = 0)
	public String getAjustador() {
		return ajustador;
	}

	public void setAjustador(String ajustador) {
		this.ajustador = ajustador;
	}

	@Exportable(columnName = "Día", columnOrder = 1)
	public String getDiaSemana() {
		return new SimpleDateFormat("EEEE", new Locale("es")).format(fecha);
	}

	@Exportable(columnName = "Fecha", format = "dd/MM/yyyy", columnOrder = 2)
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public HorarioAjustador getDispNormal() {
		return dispNormal;
	}

	public void setDispNormal(HorarioAjustador dispNormal) {
		this.dispNormal = dispNormal;
	}

	public HorarioAjustador getDispGuardia() {
		return dispGuardia;
	}

	public void setDispGuardia(HorarioAjustador dispGuardia) {
		this.dispGuardia = dispGuardia;
	}

	public HorarioAjustador getDispApoyo() {
		return dispApoyo;
	}

	public void setDispApoyo(HorarioAjustador dispApoyo) {
		this.dispApoyo = dispApoyo;
	}

	public HorarioAjustador getDispDescanso() {
		return dispDescanso;
	}

	public void setDispDescanso(HorarioAjustador dispDescanso) {
		this.dispDescanso = dispDescanso;
	}

	public HorarioAjustador getDispIncapacidad() {
		return dispIncapacidad;
	}

	public void setDispIncapacidad(HorarioAjustador dispIncapacidad) {
		this.dispIncapacidad = dispIncapacidad;
	}

	public HorarioAjustador getDispPermiso() {
		return dispPermiso;
	}

	public void setDispPermiso(HorarioAjustador dispPermiso) {
		this.dispPermiso = dispPermiso;
	}

	public HorarioAjustador getDispVacaciones() {
		return dispVacaciones;
	}

	public void setDispVacaciones(HorarioAjustador dispVacaciones) {
		this.dispVacaciones = dispVacaciones;
	}

	@Exportable(columnName = "Normal", columnOrder = 3)
	public String getDispNormalHoras() {
		return dispNormal.getHorarioLaboral().getDescripcion();
	}

	@Exportable(columnName = "Guardia", columnOrder = 4)
	public String getDispGuardiaHoras() {
		return dispGuardia.getHorarioLaboral().getDescripcion();
	}

	@Exportable(columnName = "Apoyo", columnOrder = 5)
	public String getDispApoyoHoras() {
		return dispApoyo.getHorarioLaboral().getDescripcion();
	}

	@Exportable(columnName = "Descanso", columnOrder = 6)
	public String getDispDescansoHoras() {
		return dispDescanso.getHorarioLaboral().getDescripcion();
	}

	@Exportable(columnName = "Incapacidad", columnOrder = 7)
	public String getDispIncapacidadHoras() {
		return dispIncapacidad.getHorarioLaboral().getDescripcion();
	}

	@Exportable(columnName = "Permiso", columnOrder = 8)
	public String getDispPermisoHoras() {
		return dispPermiso.getHorarioLaboral().getDescripcion();
	}

	@Exportable(columnName = "Vacaciones", columnOrder = 9)
	public String getDispVacacionesHoras() {
		return dispVacaciones.getHorarioLaboral().getDescripcion();
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}
	
	public Boolean getValidarFechaHoyOMayor(){
		Calendar cal = new GregorianCalendar();
        cal.setTime(new Date());
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND,0);
        Calendar fechaCal = new GregorianCalendar();
        fechaCal.setTime(fecha);
        return cal.compareTo(fechaCal) <= 0;
	}
}
