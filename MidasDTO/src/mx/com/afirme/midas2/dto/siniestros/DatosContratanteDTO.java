/**
 * 
 */
package mx.com.afirme.midas2.dto.siniestros;

import org.springframework.stereotype.Component;

/**
 * @author admin
 *
 */
@Component
public class DatosContratanteDTO {
	
	private String calleNumeroFiscal;
	
	private String coloniaFiscal;
	
	private String cpFiscal;
	
	private String estadoFiscal;
	
	private String municipioFiscal;
	
	private String nombreContratante;
	
	private String paisFiscal;
	
	private String rfcFiscal;
	
	private Long idPersona;
	
	private Long idPersonaContratante;

	public String getCalleNumeroFiscal() {
		return calleNumeroFiscal;
	}

	public void setCalleNumeroFiscal(String calleNumeroFiscal) {
		this.calleNumeroFiscal = calleNumeroFiscal;
	}

	public String getColoniaFiscal() {
		return coloniaFiscal;
	}

	public void setColoniaFiscal(String coloniaFiscal) {
		this.coloniaFiscal = coloniaFiscal;
	}

	public String getCpFiscal() {
		return cpFiscal;
	}

	public void setCpFiscal(String cpFiscal) {
		this.cpFiscal = cpFiscal;
	}

	public String getEstadoFiscal() {
		return estadoFiscal;
	}

	public void setEstadoFiscal(String estadoFiscal) {
		this.estadoFiscal = estadoFiscal;
	}

	public String getMunicipioFiscal() {
		return municipioFiscal;
	}

	public void setMunicipioFiscal(String municipioFiscal) {
		this.municipioFiscal = municipioFiscal;
	}

	public String getNombreContratante() {
		return nombreContratante;
	}

	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}

	public String getPaisFiscal() {
		return paisFiscal;
	}

	public void setPaisFiscal(String paisFiscal) {
		this.paisFiscal = paisFiscal;
	}

	public String getRfcFiscal() {
		return rfcFiscal;
	}

	public void setRfcFiscal(String rfcFiscal) {
		this.rfcFiscal = rfcFiscal;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public Long getIdPersonaContratante() {
		return idPersonaContratante;
	}

	public void setIdPersonaContratante(Long idPersonaContratante) {
		this.idPersonaContratante = idPersonaContratante;
	}

	
	
	

}
