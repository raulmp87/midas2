package mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos;

import java.io.Serializable;
import java.math.BigDecimal;

import org.springframework.stereotype.Component;

@Component
public class ImportesIngresosDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//Cantidades
	BigDecimal  totalesIngresosAplicar;//Totales de Ingresos a Aplicar = Es la sumatoria de todos los Ingresos Pendientes por Aplicar que se encuentren seleccionados en su listado correspondiente.
	BigDecimal  totalesDepositosBancarios;//Totales de Depósitos Bancarios = Es la sumatoria de todos los Depósitos Bancarios que se encuentren seleccionados en su listado correspondiente.
	BigDecimal  totalesMovimientosAcreedores;//Totales de Movimientos Acreedores = Es la sumatoria de todos los Movimientos Acreedores que se encuentren seleccionados en su listado correspondiente.
	BigDecimal  totalesMovimientosManuales;//Totales de Movimientos Manuales = Es la sumatoria de todos los Movimientos Manuales que se encuentren seleccionados en su listado correspondiente.
	BigDecimal  diferencia;//Diferencia = Totales de Ingresos Pendientes por Aplicar - (Totales de Depósitos Bancarios + Totales de Movimientos Acreedores + Totales de Movimientos Manuales) 
	public BigDecimal getTotalesIngresosAplicar() {
		return totalesIngresosAplicar;
	}
	public void setTotalesIngresosAplicar(BigDecimal totalesIngresosAplicar) {
		this.totalesIngresosAplicar = totalesIngresosAplicar;
	}
	
	public BigDecimal getTotalesDepositosBancarios() {
		return totalesDepositosBancarios;
	}
	public void setTotalesDepositosBancarios(BigDecimal totalesDepositosBancarios) {
		this.totalesDepositosBancarios = totalesDepositosBancarios;
	}
	public BigDecimal getTotalesMovimientosAcreedores() {
		return totalesMovimientosAcreedores;
	}
	public void setTotalesMovimientosAcreedores(
			BigDecimal totalesMovimientosAcreedores) {
		this.totalesMovimientosAcreedores = totalesMovimientosAcreedores;
	}
	public BigDecimal getTotalesMovimientosManuales() {
		return totalesMovimientosManuales;
	}
	public void setTotalesMovimientosManuales(BigDecimal totalesMovimientosManuales) {
		this.totalesMovimientosManuales = totalesMovimientosManuales;
	}
	public BigDecimal getDiferencia() {
		return diferencia;
	}
	public void setDiferencia(BigDecimal diferencia) {
		this.diferencia = diferencia;
	}
	



}
