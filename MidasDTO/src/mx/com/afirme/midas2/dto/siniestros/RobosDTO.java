package mx.com.afirme.midas2.dto.siniestros;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.Cotizacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoInciso;

import org.springframework.stereotype.Component;

@Component
public class RobosDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7958491355211510884L;
	private Long incisoContinuityId;
	private Cotizacion cotizacion;
	private AutoInciso autoInciso;
	private String descEstatus;
	private Boolean servicioPublico;
	private Boolean servicioParticular;
	private String numeroPoliza;
	private Date fechaIniVigencia;
	private Date fechaFinVigencia;
	private Integer numeroInciso;
	private String numeroCotizacion;
	private String motivo;
	private BigDecimal idToCotizacion;
	private Date fechaReporteSiniestro;
	private DatosContratanteDTO datosContratante;
	private DatosPersonalizacionDTO datosPersonalizacion;
	private Date fechaEmision;
	private String numeroPolizaAnterior;
	private String producto;
	private String medioPago;
	private String moneda;
	private String formaPago;
	private BigDecimal idToPoliza;
	private Date validOn;
	private Date recordFrom;
	private BigDecimal idToSolicitud;
	private BigDecimal idToSolicitudDataEnTramite;
	private String modelo;
	private Boolean tieneProrroga; 
	private String nombreLinea;
	private String agente;
	private boolean vigente;
	private Long validOnMillis;
	private Long recordFromMillis;
	private String nombreContratante;
	
	
	public Long getIncisoContinuityId() {
		return incisoContinuityId;
	}
	public void setIncisoContinuityId(Long incisoContinuityId) {
		this.incisoContinuityId = incisoContinuityId;
	}
	public Cotizacion getCotizacion() {
		return cotizacion;
	}
	public void setCotizacion(Cotizacion cotizacion) {
		this.cotizacion = cotizacion;
	}
	public AutoInciso getAutoInciso() {
		return autoInciso;
	}
	public void setAutoInciso(AutoInciso autoInciso) {
		this.autoInciso = autoInciso;
	}
	public String getDescEstatus() {
		return descEstatus;
	}
	public void setDescEstatus(String descEstatus) {
		this.descEstatus = descEstatus;
	}
	public Boolean getServicioPublico() {
		return servicioPublico;
	}
	public void setServicioPublico(Boolean servicioPublico) {
		this.servicioPublico = servicioPublico;
	}
	public Boolean getServicioParticular() {
		return servicioParticular;
	}
	public void setServicioParticular(Boolean servicioParticular) {
		this.servicioParticular = servicioParticular;
	}
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public Date getFechaIniVigencia() {
		return fechaIniVigencia;
	}
	public void setFechaIniVigencia(Date fechaIniVigencia) {
		this.fechaIniVigencia = fechaIniVigencia;
	}
	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}
	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}
	public Integer getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public String getNumeroCotizacion() {
		return numeroCotizacion;
	}
	public void setNumeroCotizacion(String numeroCotizacion) {
		this.numeroCotizacion = numeroCotizacion;
	}
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}
	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	public Date getFechaReporteSiniestro() {
		return fechaReporteSiniestro;
	}
	public void setFechaReporteSiniestro(Date fechaReporteSiniestro) {
		this.fechaReporteSiniestro = fechaReporteSiniestro;
	}
	public DatosContratanteDTO getDatosContratante() {
		return datosContratante;
	}
	public void setDatosContratante(DatosContratanteDTO datosContratante) {
		this.datosContratante = datosContratante;
	}
	public DatosPersonalizacionDTO getDatosPersonalizacion() {
		return datosPersonalizacion;
	}
	public void setDatosPersonalizacion(DatosPersonalizacionDTO datosPersonalizacion) {
		this.datosPersonalizacion = datosPersonalizacion;
	}
	public Date getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public String getNumeroPolizaAnterior() {
		return numeroPolizaAnterior;
	}
	public void setNumeroPolizaAnterior(String numeroPolizaAnterior) {
		this.numeroPolizaAnterior = numeroPolizaAnterior;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getMedioPago() {
		return medioPago;
	}
	public void setMedioPago(String medioPago) {
		this.medioPago = medioPago;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getFormaPago() {
		return formaPago;
	}
	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}
	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}
	public Date getValidOn() {
		return validOn;
	}
	public void setValidOn(Date validOn) {
		this.validOn = validOn;
	}
	public Date getRecordFrom() {
		return recordFrom;
	}
	public void setRecordFrom(Date recordFrom) {
		this.recordFrom = recordFrom;
	}
	public BigDecimal getIdToSolicitud() {
		return idToSolicitud;
	}
	public void setIdToSolicitud(BigDecimal idToSolicitud) {
		this.idToSolicitud = idToSolicitud;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public BigDecimal getIdToSolicitudDataEnTramite() {
		return idToSolicitudDataEnTramite;
	}
	public void setIdToSolicitudDataEnTramite(BigDecimal idToSolicitudDataEnTramite) {
		this.idToSolicitudDataEnTramite = idToSolicitudDataEnTramite;
	}
	
	
	
	/**
	 * @param tieneProrroga the tieneProrroga to set
	 */
	public void setTieneProrroga(Boolean tieneProrroga) {
		this.tieneProrroga = tieneProrroga;
	}
	
	/**
	 * @return the tieneProrroga
	 */
	public Boolean getTieneProrroga() {
		return tieneProrroga;
	}
	public String getNombreLinea() {
		return nombreLinea;
	}
	public void setNombreLinea(String nombreLinea) {
		this.nombreLinea = nombreLinea;
	}
	public String getAgente() {
		return agente;
	}
	public void setAgente(String agente) {
		this.agente = agente;
	}

	public boolean isVigente() {
		return vigente;
	}
	public void setVigente(boolean vigente) {
		this.vigente = vigente;
	}
	public Long getValidOnMillis() {
		return validOnMillis;
	}
	public void setValidOnMillis(Long validOnMillis) {
		this.validOnMillis = validOnMillis;
	}
	public Long getRecordFromMillis() {
		return recordFromMillis;
	}
	public void setRecordFromMillis(Long recordFromMillis) {
		this.recordFromMillis = recordFromMillis;
	}
	public String getNombreContratante() {
		return nombreContratante;
	}
	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}
	
	

}
