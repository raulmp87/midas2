/**
 * 
 */
package mx.com.afirme.midas2.dto.siniestros;
import java.io.Serializable;
import java.math.BigDecimal;

import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ConfiguracionCalculoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionGenerica;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroDanosMateriales;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroGastosMedicos;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroGastosMedicosConductor;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCBienes;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCPersonas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCVehiculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCViajero;

import org.springframework.stereotype.Component;

/**
 * @author simavera
 *
 */
@Component
public class EstimacionCoberturaSiniestroDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private EstimacionCoberturaReporteCabina estimacionCoberturaReporte;
	private Long estatus;
	private DatosReporteSiniestroDTO datosReporte;
	private ConfiguracionCalculoCoberturaSiniestro configuracionCalculoCoberturaSiniestro;
	private String moneda;
	private String nombreCobertura;
	private CoberturaDTO cobertura;
	private String marca;
	private String estilo;
	private DatosEstimacionCoberturaDTO datosEstimacion;
	
	private TerceroRCBienes estimacionBienes;
	private EstimacionGenerica estimacionGenerica;
	private TerceroGastosMedicos estimacionGastosMedicos;
	private TerceroRCPersonas estimacionPersonas;
	private TerceroRCViajero estimacionViajero;
	private TerceroGastosMedicosConductor estimacionGastosMedicosConductor;
	private TerceroRCVehiculo estimacionVehiculos;
	private TerceroDanosMateriales estimacionDanosMateriales;

	private String transmision;
	private TipoUsoVehiculoDTO tipoVehiculoDTO;
	private ReporteCabina reporteCabina;
	private BigDecimal montoDeducible;
	private BigDecimal montoDeducibleCalculado;
	private String     motivoNoAplicaDeducible;
	private String     tipoCoberturaAutoInciso;
	private Boolean    aplicaDeducible = Boolean.FALSE;
	private Integer    recibidaOrdenCia = 0;
	private Integer    companiaOrdenId;
	private String     nombreCompania;
	private String 	   numeroSiniestroCia;
	private String 	   responsabilidad;
	private BigDecimal montoRecuperacionSipac;


	public static final int	TIPO_DEDUCIBLE_DSMVGDF	= 11;
	public static final int	TIPO_DEDUCIBLE_VALOR	= 2;
	public static final int	TIPO_DEDUCIBLE_PORCENTAJE	= 1;
	public static final int	TIPO_DEDUCIBLE_CARATULA	= 10;
	
	/**
	 * @return the estimacionCoberturaReporte
	 */
	public EstimacionCoberturaReporteCabina getEstimacionCoberturaReporte() {
		return estimacionCoberturaReporte;
	}
	/**
	 * @param estimacionCoberturaReporte the estimacionCoberturaReporte to set
	 */
	public void setEstimacionCoberturaReporte(
			EstimacionCoberturaReporteCabina estimacionCoberturaReporte) {
		this.estimacionCoberturaReporte = estimacionCoberturaReporte;
	}
	/**
	 * @return the estatus
	 */
	public Long getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(Long estatus) {
		this.estatus = estatus;
	}
	/**
	 * @return the datosReporte
	 */
	public DatosReporteSiniestroDTO getDatosReporte() {
		return datosReporte;
	}
	/**
	 * @param datosReporte the datosReporte to set
	 */
	public void setDatosReporte(DatosReporteSiniestroDTO datosReporte) {
		this.datosReporte = datosReporte;
	}
	/**
	 * @return the configuracionCalculoCoberturaSiniestro
	 */
	public ConfiguracionCalculoCoberturaSiniestro getConfiguracionCalculoCoberturaSiniestro() {
		return configuracionCalculoCoberturaSiniestro;
	}
	/**
	 * @param configuracionCalculoCoberturaSiniestro the configuracionCalculoCoberturaSiniestro to set
	 */
	public void setConfiguracionCalculoCoberturaSiniestro(
			ConfiguracionCalculoCoberturaSiniestro configuracionCalculoCoberturaSiniestro) {
		this.configuracionCalculoCoberturaSiniestro = configuracionCalculoCoberturaSiniestro;
	}
	/**
	 * @return the moneda
	 */
	public String getMoneda() {
		return moneda;
	}
	/**
	 * @param moneda the moneda to set
	 */
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	/**
	 * @return the nombreCobertura
	 */
	public String getNombreCobertura() {
		return nombreCobertura;
	}
	/**
	 * @param nombreCobertura the nombreCobertura to set
	 */
	public void setNombreCobertura(String nombreCobertura) {
		this.nombreCobertura = nombreCobertura;
	}
	
	public CoberturaDTO getCobertura() {
		return cobertura;
	}
	public void setCobertura(CoberturaDTO cobertura) {
		this.cobertura = cobertura;
	}
	/**
	 * @return the datosEstimacion
	 */
	public DatosEstimacionCoberturaDTO getDatosEstimacion() {
		return datosEstimacion;
	}
	/**
	 * @param datosEstimacion the datosEstimacion to set
	 */
	public void setDatosEstimacion(DatosEstimacionCoberturaDTO datosEstimacion) {
		this.datosEstimacion = datosEstimacion;
	}
	public TerceroRCBienes getEstimacionBienes() {
		return estimacionBienes;
	}
	public void setEstimacionBienes(TerceroRCBienes estimacionBienes) {
		this.estimacionBienes = estimacionBienes;
	}
	public EstimacionGenerica getEstimacionGenerica() {
		return estimacionGenerica;
	}
	public void setEstimacionGenerica(EstimacionGenerica estimacionGenerica) {
		this.estimacionGenerica = estimacionGenerica;
	}
	public TerceroGastosMedicos getEstimacionGastosMedicos() {
		return estimacionGastosMedicos;
	}
	public void setEstimacionGastosMedicos(
			TerceroGastosMedicos estimacionGastosMedicos) {
		this.estimacionGastosMedicos = estimacionGastosMedicos;
	}
	public TerceroRCPersonas getEstimacionPersonas() {
		return estimacionPersonas;
	}
	public void setEstimacionPersonas(TerceroRCPersonas estimacionPersonas) {
		this.estimacionPersonas = estimacionPersonas;
	}
	public TerceroRCViajero getEstimacionViajero() {
		return estimacionViajero;
	}
	public void setEstimacionViajero(TerceroRCViajero estimacionViajero) {
		this.estimacionViajero = estimacionViajero;
	}
	public TerceroGastosMedicosConductor getEstimacionGastosMedicosConductor() {
		return estimacionGastosMedicosConductor;
	}
	public void setEstimacionGastosMedicosConductor(
			TerceroGastosMedicosConductor estimacionGastosMedicosConductor) {
		this.estimacionGastosMedicosConductor = estimacionGastosMedicosConductor;
	}
	public TerceroRCVehiculo getEstimacionVehiculos() {
		return estimacionVehiculos;
	}
	public void setEstimacionVehiculos(TerceroRCVehiculo estimacionVehiculos) {
		this.estimacionVehiculos = estimacionVehiculos;
	}
	public TerceroDanosMateriales getEstimacionDanosMateriales() {
		return estimacionDanosMateriales;
	}
	public void setEstimacionDanosMateriales(
			TerceroDanosMateriales estimacionDanosMateriales) {
		this.estimacionDanosMateriales = estimacionDanosMateriales;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getEstilo() {
		return estilo;
	}
	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}
	
	public String getTransmision() {
		return transmision;
	}
	public void setTransmision(String transmision) {
		this.transmision = transmision;
	}
	public TipoUsoVehiculoDTO getTipoVehiculoDTO() {
		return tipoVehiculoDTO;
	}
	public void setTipoVehiculoDTO(TipoUsoVehiculoDTO tipoVehiculoDTO) {
		this.tipoVehiculoDTO = tipoVehiculoDTO;
	}
	public ReporteCabina getReporteCabina() {
		return reporteCabina;
	}
	public void setReporteCabina(ReporteCabina reporteCabina) {
		this.reporteCabina = reporteCabina;
	}
	public BigDecimal getMontoDeducible() {
		return montoDeducible;
	}
	public void setMontoDeducible(BigDecimal montoDeducible) {
		this.montoDeducible = montoDeducible;
	}
	public BigDecimal getMontoDeducibleCalculado() {
		return montoDeducibleCalculado;
	}
	public void setMontoDeducibleCalculado(BigDecimal montoDeducibleCalculado) {
		this.montoDeducibleCalculado = montoDeducibleCalculado;
	}
	public String getMotivoNoAplicaDeducible() {
		return motivoNoAplicaDeducible;
	}
	public void setMotivoNoAplicaDeducible(String motivoNoAplicaDeducible) {
		this.motivoNoAplicaDeducible = motivoNoAplicaDeducible;
	}
	public String getTipoCoberturaAutoInciso() {
		return tipoCoberturaAutoInciso;
	}
	public void setTipoCoberturaAutoInciso(String tipoCoberturaAutoInciso) {
		this.tipoCoberturaAutoInciso = tipoCoberturaAutoInciso;
	}
	public Boolean getAplicaDeducible() {
		return aplicaDeducible;
	}
	public void setAplicaDeducible(Boolean aplicaDeducible) {
		this.aplicaDeducible = aplicaDeducible;
	}
	public Integer getRecibidaOrdenCia() {
		return recibidaOrdenCia;
	}
	public void setRecibidaOrdenCia(Integer recibidaOrdenCia) {
		this.recibidaOrdenCia = recibidaOrdenCia;
	}
	public Integer getCompaniaOrdenId() {
		return companiaOrdenId;
	}
	public void setCompaniaOrdenId(Integer companiaOrdenId) {
		this.companiaOrdenId = companiaOrdenId;
	}
	public String getNombreCompania() {
		return nombreCompania;
	}
	public void setNombreCompania(String nombreCompania) {
		this.nombreCompania = nombreCompania;
	}
	public String getNumeroSiniestroCia() {
		return numeroSiniestroCia;
	}
	public void setNumeroSiniestroCia(String numeroSiniestroCia) {
		this.numeroSiniestroCia = numeroSiniestroCia;
	}
	public String getResponsabilidad() {
		return responsabilidad;
	}
	public void setResponsabilidad(String responsabilidad) {
		this.responsabilidad = responsabilidad;
	}
	public BigDecimal getMontoRecuperacionSipac() {
		return montoRecuperacionSipac;
	}
	public void setMontoRecuperacionSipac(BigDecimal montoRecuperacionSipac) {
		this.montoRecuperacionSipac = montoRecuperacionSipac;
	}	
}
