package mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;



/**
 * @author Israel
 * @version 1.0
 * @created 31-mar.-2015 12:43:15 p. m.
 */
public class BusquedaNotasCreditoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String estatus;
	private Date fechaBajaNotaCreditoFin;
	private Date fechaBajaNotaCreditoIni;
	private Date fechaRegNotaCreditoFin;
	private Date fechaRegNotaCreditoIni;
	private String folioNotaCredito;
	private BigDecimal montoNotaCreditoFin;
	private BigDecimal montoNotaCreditoIni;
	private String noFactura;
	private String nombreProveedor;
	private Long noProveedor;
	private String siniestro;

	public BusquedaNotasCreditoDTO(){

	}

	public void finalize() throws Throwable {

	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public Date getFechaBajaNotaCreditoFin() {
		return fechaBajaNotaCreditoFin;
	}

	public void setFechaBajaNotaCreditoFin(Date fechaBajaNotaCreditoFin) {
		this.fechaBajaNotaCreditoFin = fechaBajaNotaCreditoFin;
	}

	public Date getFechaBajaNotaCreditoIni() {
		return fechaBajaNotaCreditoIni;
	}

	public void setFechaBajaNotaCreditoIni(Date fechaBajaNotaCreditoIni) {
		this.fechaBajaNotaCreditoIni = fechaBajaNotaCreditoIni;
	}

	public Date getFechaRegNotaCreditoFin() {
		return fechaRegNotaCreditoFin;
	}

	public void setFechaRegNotaCreditoFin(Date fechaRegNotaCreditoFin) {
		this.fechaRegNotaCreditoFin = fechaRegNotaCreditoFin;
	}

	public Date getFechaRegNotaCreditoIni() {
		return fechaRegNotaCreditoIni;
	}

	public void setFechaRegNotaCreditoIni(Date fechaRegNotaCreditoIni) {
		this.fechaRegNotaCreditoIni = fechaRegNotaCreditoIni;
	}

	public String getFolioNotaCredito() {
		return folioNotaCredito;
	}

	public void setFolioNotaCredito(String folioNotaCredito) {
		this.folioNotaCredito = folioNotaCredito;
	}

	public BigDecimal getMontoNotaCreditoFin() {
		return montoNotaCreditoFin;
	}

	public void setMontoNotaCreditoFin(BigDecimal montoNotaCreditoFin) {
		this.montoNotaCreditoFin = montoNotaCreditoFin;
	}

	public BigDecimal getMontoNotaCreditoIni() {
		return montoNotaCreditoIni;
	}

	public void setMontoNotaCreditoIni(BigDecimal montoNotaCreditoIni) {
		this.montoNotaCreditoIni = montoNotaCreditoIni;
	}

	public String getNoFactura() {
		return noFactura;
	}

	public void setNoFactura(String noFactura) {
		this.noFactura = noFactura;
	}

	public String getNombreProveedor() {
		return nombreProveedor;
	}

	public void setNombreProveedor(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}

	public Long getNoProveedor() {
		return noProveedor;
	}

	public void setNoProveedor(Long noProveedor) {
		this.noProveedor = noProveedor;
	}

	public String getSiniestro() {
		return siniestro;
	}

	public void setSiniestro(String siniestro) {
		this.siniestro = siniestro;
	}
}