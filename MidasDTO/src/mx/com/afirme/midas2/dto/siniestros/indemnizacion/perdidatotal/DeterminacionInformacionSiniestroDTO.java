package mx.com.afirme.midas2.dto.siniestros.indemnizacion.perdidatotal;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.util.CollectionUtils;

import org.springframework.stereotype.Component;

@Component
public class DeterminacionInformacionSiniestroDTO implements Serializable{

	private static final long serialVersionUID = 4232546149523504808L;
	
//	INFORMACION SINIESTRO
	private String numPoliza;
	private Integer inciso;
	private String numReporte;
	private String agente;
	private String numSiniestro;
	private Date   fechaSiniestro;
	private String tipoSiniestro;
	private String lugarSiniestro;
	private String asegurado;
	private BigDecimal primaTotal;
	private String beneficiario;
	private String terminoAjuste;
	private String terminoSiniestro;
	private String esTercero;
	
//	DESCRIPCION AUTO
	private String marcaTipo;
	private String color;
	private String numSerie;
	private String motor;
	private Short modelo;
	private Short puertas;
	private String marca;
	private String tipo;
	private String placa;

//	INFORMACION ADICIONAL
	private String tipoBajaPlacas;
	private Long idReporteCabina;
	private Long idSiniestro;
	private Long idValuador;
	private Boolean esCoberturaAsegurado;
	
	//Lista de recuperaciones de deducibles asociadas
	private List<InfoRecuperacionDeducible> deducibles = 
		new ArrayList<DeterminacionInformacionSiniestroDTO.InfoRecuperacionDeducible>();
	
	public BigDecimal montoTotalDeducible = BigDecimal.ZERO;
	

	public static class InfoRecuperacionDeducible{
		
		private Long idRecuperacion;
		private String descripcion;
		private BigDecimal monto;	
		private Boolean ingresoGenerado;
		
		public InfoRecuperacionDeducible(){
			super();
		}
		
		/**
		 * Crear objeto de transporte para recuperacion de deducible en indemnizacion
		 * @param cobertura nombre de la cobertura que genero la recuperacion
		 * @param monto monto final del deducible 
		 * @param idEstimacion estimacion que genero la recuperacion
		 */
		public InfoRecuperacionDeducible(String cobertura, BigDecimal monto, Long idRecuperacion, Boolean ingresoGenerado){
			this();
			this.idRecuperacion = idRecuperacion;
			this.monto = monto;
			this.descripcion = cobertura;
			this.ingresoGenerado = ingresoGenerado;
		}
		
		public Long getIdRecuperacion() {
			return idRecuperacion;
		}

		public void setIdRecuperacion(Long idRecuperacion) {
			this.idRecuperacion = idRecuperacion;
		}

		public String getDescripcion() {
			return descripcion;
		}
		public void setDescripcion(String descripcion) {
			this.descripcion = descripcion;
		}
		public BigDecimal getMonto() {
			return monto;
		}
		public void setMonto(BigDecimal monto) {
			this.monto = monto;
		}
		public Boolean getIngresoGenerado() {
			return ingresoGenerado;
		}
		public void setIngresoGenerado(Boolean ingresoGenerado) {
			this.ingresoGenerado = ingresoGenerado;
		}
		
	}
	
	/**
	 * @return the numPoliza
	 */
	public String getNumPoliza() {
		return numPoliza;
	}
	/**
	 * @param numPoliza the numPoliza to set
	 */
	public void setNumPoliza(String numPoliza) {
		this.numPoliza = numPoliza;
	}
	/**
	 * @return the inciso
	 */
	public Integer getInciso() {
		return inciso;
	}
	/**
	 * @param inciso the inciso to set
	 */
	public void setInciso(Integer inciso) {
		this.inciso = inciso;
	}
	/**
	 * @return the numReporte
	 */
	public String getNumReporte() {
		return numReporte;
	}
	/**
	 * @param numReporte the numReporte to set
	 */
	public void setNumReporte(String numReporte) {
		this.numReporte = numReporte;
	}
	/**
	 * @return the agente
	 */
	public String getAgente() {
		return agente;
	}
	/**
	 * @param agente the agente to set
	 */
	public void setAgente(String agente) {
		this.agente = agente;
	}
	/**
	 * @return the numSiniestro
	 */
	public String getNumSiniestro() {
		return numSiniestro;
	}
	/**
	 * @param numSiniestro the numSiniestro to set
	 */
	public void setNumSiniestro(String numSiniestro) {
		this.numSiniestro = numSiniestro;
	}
	/**
	 * @return the fechaSiniestro
	 */
	public Date getFechaSiniestro() {
		return fechaSiniestro;
	}
	/**
	 * @param fechaSiniestro the fechaSiniestro to set
	 */
	public void setFechaSiniestro(Date fechaSiniestro) {
		this.fechaSiniestro = fechaSiniestro;
	}
	/**
	 * @return the tipoSiniestro
	 */
	public String getTipoSiniestro() {
		return tipoSiniestro;
	}
	/**
	 * @param tipoSiniestro the tipoSiniestro to set
	 */
	public void setTipoSiniestro(String tipoSiniestro) {
		this.tipoSiniestro = tipoSiniestro;
	}
	/**
	 * @return the lugarSiniestro
	 */
	public String getLugarSiniestro() {
		return lugarSiniestro;
	}
	/**
	 * @param lugarSiniestro the lugarSiniestro to set
	 */
	public void setLugarSiniestro(String lugarSiniestro) {
		this.lugarSiniestro = lugarSiniestro;
	}
	/**
	 * @return the asegurado
	 */
	public String getAsegurado() {
		return asegurado;
	}
	/**
	 * @param asegurado the asegurado to set
	 */
	public void setAsegurado(String asegurado) {
		this.asegurado = asegurado;
	}
	/**
	 * @return the primaTotal
	 */
	public BigDecimal getPrimaTotal() {
		return primaTotal;
	}
	/**
	 * @param primaTotal the primaTotal to set
	 */
	public void setPrimaTotal(BigDecimal primaTotal) {
		this.primaTotal = primaTotal;
	}
	/**
	 * @return the beneficiario
	 */
	public String getBeneficiario() {
		return beneficiario;
	}
	/**
	 * @param beneficiario the beneficiario to set
	 */
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}
	/**
	 * @return the marcaTipo
	 */
	public String getMarcaTipo() {
		return marcaTipo;
	}
	/**
	 * @param marcaTipo the marcaTipo to set
	 */
	public void setMarcaTipo(String marcaTipo) {
		this.marcaTipo = marcaTipo;
	}
	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}
	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}
	/**
	 * @return the numSerie
	 */
	public String getNumSerie() {
		return numSerie;
	}
	/**
	 * @param numSerie the numSerie to set
	 */
	public void setNumSerie(String numSerie) {
		this.numSerie = numSerie;
	}
	/**
	 * @return the motor
	 */
	public String getMotor() {
		return motor;
	}
	/**
	 * @param motor the motor to set
	 */
	public void setMotor(String motor) {
		this.motor = motor;
	}
	/**
	 * @return the modelo
	 */
	public Short getModelo() {
		return modelo;
	}
	/**
	 * @param modelo the modelo to set
	 */
	public void setModelo(Short modelo) {
		this.modelo = modelo;
	}
	/**
	 * @return the puertas
	 */
	public Short getPuertas() {
		return puertas;
	}
	/**
	 * @param puertas the puertas to set
	 */
	public void setPuertas(Short puertas) {
		this.puertas = puertas;
	}
	/**
	 * @return the tipoBajaPlacas
	 */
	public String getTipoBajaPlacas() {
		return tipoBajaPlacas;
	}
	/**
	 * @param tipoBajaPlacas the tipoBajaPlacas to set
	 */
	public void setTipoBajaPlacas(String tipoBajaPlacas) {
		this.tipoBajaPlacas = tipoBajaPlacas;
	}
	/**
	 * @return the placa
	 */
	public String getPlaca() {
		return placa;
	}
	/**
	 * @param placa the placa to set
	 */
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	
	
	
	/**
	 * @return the marca
	 */
	public String getMarca() {
		return marca;
	}
	/**
	 * @param marca the marca to set
	 */
	public void setMarca(String marca) {
		this.marca = marca;
	}
	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}
	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	/**
	 * @return the idReporteCabina
	 */
	public Long getIdReporteCabina() {
		return idReporteCabina;
	}
	/**
	 * @param idReporteCabina the idReporteCabina to set
	 */
	public void setIdReporteCabina(Long idReporteCabina) {
		this.idReporteCabina = idReporteCabina;
	}
	/**
	 * @return the idValuador
	 */
	public Long getIdValuador() {
		return idValuador;
	}
	/**
	 * @param idValuador the idValuador to set
	 */
	public void setIdValuador(Long idValuador) {
		this.idValuador = idValuador;
	}
	/**
	 * @return the idSiniestro
	 */
	public Long getIdSiniestro() {
		return idSiniestro;
	}
	/**
	 * @param idSiniestro the idSiniestro to set
	 */
	public void setIdSiniestro(Long idSiniestro) {
		this.idSiniestro = idSiniestro;
	}
	/**
	 * @return the esCoberturaAsegurado
	 */
	public Boolean getEsCoberturaAsegurado() {
		return esCoberturaAsegurado;
	}
	/**
	 * @param esCoberturaAsegurado the esCoberturaAsegurado to set
	 */
	public void setEsCoberturaAsegurado(Boolean esCoberturaAsegurado) {
		this.esCoberturaAsegurado = esCoberturaAsegurado;
	}
	/**
	 * @return the terminoAjuste
	 */
	public String getTerminoAjuste() {
		return terminoAjuste;
	}
	/**
	 * @param terminoAjuste the terminoAjuste to set
	 */
	public void setTerminoAjuste(String terminoAjuste) {
		this.terminoAjuste = terminoAjuste;
	}
	/**
	 * @return the terminoSiniestro
	 */
	public String getTerminoSiniestro() {
		return terminoSiniestro;
	}
	
	/**
	 * @return the esTercero
	 */
	public String getEsTercero() {
		return esTercero;
	}
	/**
	 * @param esTercero the esTercero to set
	 */
	public void setEsTercero(String esTercero) {
		this.esTercero = esTercero;
	}
	/**
	 * @param terminoSiniestro the terminoSiniestro to set
	 */
	public void setTerminoSiniestro(String terminoSiniestro) {
		this.terminoSiniestro = terminoSiniestro;
	}
	
	public List<InfoRecuperacionDeducible> getDeducibles() {
		return deducibles;
	}
	
	public void setDeducibles(List<InfoRecuperacionDeducible> deducibles) {
		this.deducibles = deducibles;
	}
	public BigDecimal getMontoTotalDeducible() {	
		montoTotalDeducible = BigDecimal.ZERO;
		for(InfoRecuperacionDeducible deducible : CollectionUtils.emptyIfNull(deducibles)){
			montoTotalDeducible = montoTotalDeducible.add(deducible.getMonto());
		}		
		return montoTotalDeducible;
	}
	
}