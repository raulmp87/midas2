package mx.com.afirme.midas2.dto.siniestros.indemnizacion;

import java.io.Serializable;

public class CartaPTSNoDocumentadaDTO implements Serializable{

	private static final long serialVersionUID = 6718353697778179296L;
	
	private String	nombreCliente;
	private String	numeroSiniestro;
	private String	periodoInicio;
	private String 	periodoFin;
	private String	numeroPoliza;
	private String	marca;
	private String	tipo;
	private String	modelo;
	private String 	numeroSerie;
	private String	nombreUsuario;
	/**
	 * @return the nombreCliente
	 */
	public String getNombreCliente() {
		return (nombreCliente == null) ? "" : nombreCliente;
	}
	/**
	 * @param nombreCliente the nombreCliente to set
	 */
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	/**
	 * @return the numeroSiniestro
	 */
	public String getNumeroSiniestro() {
		return (numeroSiniestro == null)? "" : numeroSiniestro;
	}
	/**
	 * @param numeroSiniestro the numeroSiniestro to set
	 */
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}
	/**
	 * @return the periodoInicio
	 */
	public String getPeriodoInicio() {
		return (periodoInicio == null)? "" : periodoInicio;
	}
	/**
	 * @param periodoInicio the periodoInicio to set
	 */
	public void setPeriodoInicio(String periodoInicio) {
		this.periodoInicio = periodoInicio;
	}
	/**
	 * @return the periodoFin
	 */
	public String getPeriodoFin() {
		return (periodoFin == null)? "" : periodoFin;
	}
	/**
	 * @param periodoFin the periodoFin to set
	 */
	public void setPeriodoFin(String periodoFin) {
		this.periodoFin = periodoFin;
	}
	/**
	 * @return the numeroPoliza
	 */
	public String getNumeroPoliza() {
		return (numeroPoliza == null)? "" : numeroPoliza;
	}
	/**
	 * @param numeroPoliza the numeroPoliza to set
	 */
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	/**
	 * @return the marca
	 */
	public String getMarca() {
		return (marca == null)? "" : marca;
	}
	/**
	 * @param marca the marca to set
	 */
	public void setMarca(String marca) {
		this.marca = marca;
	}
	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return (tipo == null)? "" : tipo;
	}
	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	/**
	 * @return the modelo
	 */
	public String getModelo() {
		return (modelo == null)? "" : modelo;
	}
	/**
	 * @param modelo the modelo to set
	 */
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	/**
	 * @return the numeroSerie
	 */
	public String getNumeroSerie() {
		return (numeroSerie == null)? "" : numeroSerie;
	}
	/**
	 * @param numeroSerie the numeroSerie to set
	 */
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	/**
	 * @return the nombreUsuario
	 */
	public String getNombreUsuario() {
		return (nombreUsuario == null)? "" : nombreUsuario;
	}
	/**
	 * @param nombreUsuario the nombreUsuario to set
	 */
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	
}