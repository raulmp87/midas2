package mx.com.afirme.midas2.dto.siniestros.liquidacion;

import java.io.Serializable;
import java.math.BigDecimal;

public class LiquidacionElementoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String detalle;
	private Long id;
	private BigDecimal montoTotal;
	private String numero;
	private String tieneNotasCredito;
	private String tipo;
	private String tipoDescripcion;

	public LiquidacionElementoDTO(){

	}
	
	public LiquidacionElementoDTO(Long id, String numero, BigDecimal montoTotal, String tieneNotasCredito, String tipo, String tipoDescripcion)
	{
		this.id = id;
		this.numero = numero;
		this.montoTotal = montoTotal;
		this.tieneNotasCredito = tieneNotasCredito;
		this.tipo = tipo;
		this.tipoDescripcion = tipoDescripcion;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}

	public String getTieneNotasCredito() {
		return tieneNotasCredito;
	}

	public void setTieneNotasCredito(String tieneNotasCredito) {
		this.tieneNotasCredito = tieneNotasCredito;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getTipoDescripcion() {
		return tipoDescripcion;
	}

	public void setTipoDescripcion(String tipoDescripcion) {
		this.tipoDescripcion = tipoDescripcion;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}
}