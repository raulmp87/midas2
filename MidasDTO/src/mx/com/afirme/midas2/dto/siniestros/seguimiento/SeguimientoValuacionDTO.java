package mx.com.afirme.midas2.dto.siniestros.seguimiento;

import java.io.Serializable;
import java.util.List;


/**
 * Objeto para transportar el Seguimiento de Valuaci�n.
 * @author Arturo
 * @version 1.0
 * @created 13-oct-2014 11:44:00 a.m.
 */
public class SeguimientoValuacionDTO extends SeguimientoSiniestroDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2129749237169645932L;
	private int diasEnReparacion;
	private String fechaAdmision;
	private String fechaPromesa;
	private List<SeguimientoValuacionRefaccion> refacciones;
	private List<SeguimientoValuacionTrabajo> trabajosPendientes;
	private List<SeguimientoValuacionTrabajo> trabajosRealizados;
	public int getDiasEnReparacion() {
		return diasEnReparacion;
	}
	public void setDiasEnReparacion(int diasEnReparacion) {
		this.diasEnReparacion = diasEnReparacion;
	}
	public String getFechaAdmision() {
		return fechaAdmision;
	}
	public void setFechaAdmision(String fechaAdmision) {
		this.fechaAdmision = fechaAdmision;
	}
	public String getFechaPromesa() {
		return fechaPromesa;
	}
	public void setFechaPromesa(String fechaPromesa) {
		this.fechaPromesa = fechaPromesa;
	}
	public List<SeguimientoValuacionRefaccion> getRefacciones() {
		return refacciones;
	}
	public void setRefacciones(List<SeguimientoValuacionRefaccion> refacciones) {
		this.refacciones = refacciones;
	}
	public List<SeguimientoValuacionTrabajo> getTrabajosPendientes() {
		return trabajosPendientes;
	}
	public void setTrabajosPendientes(
			List<SeguimientoValuacionTrabajo> trabajosPendientes) {
		this.trabajosPendientes = trabajosPendientes;
	}
	public List<SeguimientoValuacionTrabajo> getTrabajosRealizados() {
		return trabajosRealizados;
	}
	public void setTrabajosRealizados(
			List<SeguimientoValuacionTrabajo> trabajosRealizados) {
		this.trabajosRealizados = trabajosRealizados;
	}


}