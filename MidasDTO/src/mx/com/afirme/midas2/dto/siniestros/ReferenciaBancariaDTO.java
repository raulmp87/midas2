package mx.com.afirme.midas2.dto.siniestros;

import mx.com.afirme.midas2.domain.catalogos.banco.BancoMidas;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.ReferenciaBancaria;

import org.springframework.stereotype.Component;



/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 26-may-2015 02:07:55 p.m.
 */
@Component
public class ReferenciaBancariaDTO {

	private BancoMidas bancoMidas;
	private String clabe;
	private String cuentaBancaria;
	private Long id;
	private String medioRecuperacion;
	private String numeroReferencia;
	private ReferenciaBancaria referencia;
	
	
	public ReferenciaBancariaDTO (	){}
	
	
	public ReferenciaBancariaDTO ( BancoMidas bancoMidas,
	 String clabe,
	 String cuentaBancaria,
	 Long id,
	 String medioRecuperacion,
	 String numeroReferencia,
	 ReferenciaBancaria referencia
	){
		this.bancoMidas = bancoMidas;
		this.clabe=clabe;
		this.cuentaBancaria=cuentaBancaria;
		this.id=id;
		this.medioRecuperacion=medioRecuperacion;
		this.numeroReferencia=numeroReferencia;
		this.referencia=referencia;
		
	}
	
	
	
	
	
	
	
	public BancoMidas getBancoMidas() {
		return bancoMidas;
	}
	public void setBancoMidas(BancoMidas bancoMidas) {
		this.bancoMidas = bancoMidas;
	}
	public String getClabe() {
		return clabe;
	}
	public void setClabe(String clabe) {
		this.clabe = clabe;
	}
	public String getCuentaBancaria() {
		return cuentaBancaria;
	}
	public void setCuentaBancaria(String cuentaBancaria) {
		this.cuentaBancaria = cuentaBancaria;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMedioRecuperacion() {
		return medioRecuperacion;
	}
	public void setMedioRecuperacion(String medioRecuperacion) {
		this.medioRecuperacion = medioRecuperacion;
	}
	public String getNumeroReferencia() {
		return numeroReferencia;
	}
	public void setNumeroReferencia(String numeroReferencia) {
		this.numeroReferencia = numeroReferencia;
	}
	public ReferenciaBancaria getReferencia() {
		return referencia;
	}
	public void setReferencia(ReferenciaBancaria referencia) {
		this.referencia = referencia;
	}

	

}