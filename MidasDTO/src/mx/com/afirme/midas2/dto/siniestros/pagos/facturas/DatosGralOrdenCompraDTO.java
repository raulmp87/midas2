/**
 * 
 */
package mx.com.afirme.midas2.dto.siniestros.pagos.facturas;

import java.io.Serializable;

import org.springframework.stereotype.Component;

/**
 * @author admin
 *
 */
@Component
public class DatosGralOrdenCompraDTO  implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2248287624867651201L;

	private Long numeroOrdenCompra;
	
	private String proveedor;
	
	private String tipoPago;
	
	private String conceptoDePago;
	
	private String terminoDeAjuste;
	
	private String coberturaAfectada;
	
	/**
	 * Atributos para el envio de correos
	 */
	private String usuario;
	
	private String comentarios;

	/**
	 * @return the numeroOrdenCompra
	 */
	public Long getNumeroOrdenCompra() {
		return numeroOrdenCompra;
	}

	/**
	 * @param numeroOrdenCompra the numeroOrdenCompra to set
	 */
	public void setNumeroOrdenCompra(Long numeroOrdenCompra) {
		this.numeroOrdenCompra = numeroOrdenCompra;
	}

	/**
	 * @return the proveedor
	 */
	public String getProveedor() {
		return proveedor;
	}

	/**
	 * @param proveedor the proveedor to set
	 */
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	/**
	 * @return the tipoPago
	 */
	public String getTipoPago() {
		return tipoPago;
	}

	/**
	 * @param tipoPago the tipoPago to set
	 */
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}

	/**
	 * @return the conceptoDePago
	 */
	public String getConceptoDePago() {
		return conceptoDePago;
	}

	/**
	 * @param conceptoDePago the conceptoDePago to set
	 */
	public void setConceptoDePago(String conceptoDePago) {
		this.conceptoDePago = conceptoDePago;
	}

	/**
	 * @return the terminoDeAjuste
	 */
	public String getTerminoDeAjuste() {
		return terminoDeAjuste;
	}

	/**
	 * @param terminoDeAjuste the terminoDeAjuste to set
	 */
	public void setTerminoDeAjuste(String terminoDeAjuste) {
		this.terminoDeAjuste = terminoDeAjuste;
	}

	/**
	 * @return the coberturaAfectada
	 */
	public String getCoberturaAfectada() {
		return coberturaAfectada;
	}

	/**
	 * @param coberturaAfectada the coberturaAfectada to set
	 */
	public void setCoberturaAfectada(String coberturaAfectada) {
		this.coberturaAfectada = coberturaAfectada;
	}

	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the comentarios
	 */
	public String getComentarios() {
		return comentarios;
	}

	/**
	 * @param comentarios the comentarios to set
	 */
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}
	

	
	
}
