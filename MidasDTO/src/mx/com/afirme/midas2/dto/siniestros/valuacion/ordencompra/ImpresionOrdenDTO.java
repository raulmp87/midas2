package mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class ImpresionOrdenDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1223669940936274048L;
	private ImpresionOrdenCompraDTO impresionDto;
	private List<DetalleOrdenCompraDTO> detalleOrdenCompraDTO ;
	public ImpresionOrdenCompraDTO getImpresionDto() {
		return impresionDto;
	}
	public void setImpresionDto(ImpresionOrdenCompraDTO impresionDto) {
		this.impresionDto = impresionDto;
	}
	public List<DetalleOrdenCompraDTO> getDetalleOrdenCompraDTO() {
		return detalleOrdenCompraDTO;
	}
	public void setDetalleOrdenCompraDTO(
			List<DetalleOrdenCompraDTO> detalleOrdenCompraDTO) {
		this.detalleOrdenCompraDTO = detalleOrdenCompraDTO;
	}
	






	
	
	
	}
