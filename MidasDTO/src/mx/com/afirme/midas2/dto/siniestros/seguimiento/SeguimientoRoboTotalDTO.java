package mx.com.afirme.midas2.dto.siniestros.seguimiento;

import java.io.Serializable;


/**
 * Objeto para transportar el Seguimiento de Robo Total.
 * @author Arturo
 * @version 1.0
 * @created 13-oct-2014 11:45:55 a.m.
 */
public class SeguimientoRoboTotalDTO extends SeguimientoSiniestroDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4358182319036324796L;
	private String calleNumero;
	private String codigoPostal;
	private String colonia;
	private String estado;
	private String estatus;
	private String fechaAveriguacion;
	private String fechaLocalizacion;
	private String fechaRecuperacion;
	private String fechaReporte;
	private String kilometro;
	private String municipio;
	private String nombreCarretera;
	private String numAveriguacion;
	private String pais;
	private String referencia;
	private String tipoCarretera;
	private String tipoRobo;
	private String ubicacion;
	public String getCalleNumero() {
		return calleNumero;
	}
	public void setCalleNumero(String calleNumero) {
		this.calleNumero = calleNumero;
	}
	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public String getColonia() {
		return colonia;
	}
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getFechaAveriguacion() {
		return fechaAveriguacion;
	}
	public void setFechaAveriguacion(String fechaAveriguacion) {
		this.fechaAveriguacion = fechaAveriguacion;
	}
	public String getFechaLocalizacion() {
		return fechaLocalizacion;
	}
	public void setFechaLocalizacion(String fechaLocalizacion) {
		this.fechaLocalizacion = fechaLocalizacion;
	}
	public String getFechaRecuperacion() {
		return fechaRecuperacion;
	}
	public void setFechaRecuperacion(String fechaRecuperacion) {
		this.fechaRecuperacion = fechaRecuperacion;
	}
	public String getFechaReporte() {
		return fechaReporte;
	}
	public void setFechaReporte(String fechaReporte) {
		this.fechaReporte = fechaReporte;
	}
	
	public String getKilometro() {
		return kilometro;
	}
	public void setKilometro(String kilometro) {
		this.kilometro = kilometro;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getNombreCarretera() {
		return nombreCarretera;
	}
	public void setNombreCarretera(String nombreCarretera) {
		this.nombreCarretera = nombreCarretera;
	}
	public String getNumAveriguacion() {
		return numAveriguacion;
	}
	public void setNumAveriguacion(String numAveriguacion) {
		this.numAveriguacion = numAveriguacion;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getTipoCarretera() {
		return tipoCarretera;
	}
	public void setTipoCarretera(String tipoCarretera) {
		this.tipoCarretera = tipoCarretera;
	}
	public String getTipoRobo() {
		return tipoRobo;
	}
	public void setTipoRobo(String tipoRobo) {
		this.tipoRobo = tipoRobo;
	}
	public String getUbicacion() {
		return ubicacion;
	}
	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}


	

}