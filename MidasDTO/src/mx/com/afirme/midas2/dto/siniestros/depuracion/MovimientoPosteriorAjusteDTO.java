package mx.com.afirme.midas2.dto.siniestros.depuracion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import mx.com.afirme.midas2.annotation.Exportable;

import org.springframework.stereotype.Component;

@Component
public class MovimientoPosteriorAjusteDTO implements Serializable{

	private static final long serialVersionUID = 5953798640760815407L;
	
	private Long movimientoId;
	private String causaAjuste;
	private String claveSubCalculo;
	private Long conceptoId;
	private Date fechaAjuste;
	private Date fechaFin;
	private Date fechaInicio;
	private BigDecimal idToCobertura;
	private BigDecimal montoAjuste;
	private String nombreCobertura;
	private String nombreConcepto;
	private String nombreOficina;
	private String nombreUsuario;
	private Integer numeroInciso;
	private String numeroPoliza;
	private String numeroReporte;
	private String numeroSiniestro;
	private Long oficinaId;
	private BigDecimal reservaActual;
	private String terminoSiniestro;
	private String terminoSiniestroDesc;
	private String tipoEstimacion;
	private String tipoServicio;
	private String oficinaDesc;
	private String contratante;
	private String idCoberturaClaveSubCalculo;
	
	
	public String getCausaAjuste() {
		return causaAjuste;
	}
	public void setCausaAjuste(String causaAjuste) {
		this.causaAjuste = causaAjuste;
	}
	public String getClaveSubCalculo() {
		return claveSubCalculo;
	}
	public void setClaveSubCalculo(String claveSubCalculo) {
		this.claveSubCalculo = claveSubCalculo;
	}
	public Long getConceptoId() {
		return conceptoId;
	}
	public void setConceptoId(Long conceptoId) {
		this.conceptoId = conceptoId;
	}
	@Exportable(columnName="Fecha de Ajuste", columnOrder=9)
	public Date getFechaAjuste() {
		return fechaAjuste;
	}
	public void setFechaAjuste(Date fechaAjuste) {
		this.fechaAjuste = fechaAjuste;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public BigDecimal getIdToCobertura() {
		return idToCobertura;
	}
	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}
	@Exportable(columnName="Monto de Ajuste", columnOrder=8, format="$* #,##0.00")
	public BigDecimal getMontoAjuste() {
		return montoAjuste;
	}
	public void setMontoAjuste(BigDecimal montoAjuste) {
		this.montoAjuste = montoAjuste;
	}
	@Exportable(columnName="Cobertura", columnOrder=4)
	public String getNombreCobertura() {
		return nombreCobertura;
	}
	public void setNombreCobertura(String nombreCobertura) {
		this.nombreCobertura = nombreCobertura;
	}
	@Exportable(columnName="Concepto de Depuracion", columnOrder=6)
	public String getNombreConcepto() {
		return nombreConcepto;
	}
	public void setNombreConcepto(String nombreConcepto) {
		this.nombreConcepto = nombreConcepto;
	}
	@Exportable(columnName="Oficina", columnOrder=0)
	public String getNombreOficina() {
		return nombreOficina;
	}
	public void setNombreOficina(String nombreOficina) {
		this.nombreOficina = nombreOficina;
	}
	@Exportable(columnName="Usuario", columnOrder=7)
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public Integer getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	@Exportable(columnName="No. de Poliza", columnOrder=1)
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	@Exportable(columnName="No de Reporte", columnOrder=3)
	public String getNumeroReporte() {
		return numeroReporte;
	}
	public void setNumeroReporte(String numeroReporte) {
		this.numeroReporte = numeroReporte;
	}
	@Exportable(columnName="No de Siniestro", columnOrder=2)
	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}
	public Long getOficinaId() {
		return oficinaId;
	}
	public void setOficinaId(Long oficinaId) {
		this.oficinaId = oficinaId;
	}
	public BigDecimal getReservaActual() {
		return reservaActual;
	}
	public void setReservaActual(BigDecimal reservaActual) {
		this.reservaActual = reservaActual;
	}
	public String getTerminoSiniestro() {
		return terminoSiniestro;
	}
	public void setTerminoSiniestro(String terminoSiniestro) {
		this.terminoSiniestro = terminoSiniestro;
	}
	@Exportable(columnName="Termino de Siniestro", columnOrder=5)
	public String getTerminoSiniestroDesc() {
		return terminoSiniestroDesc;
	}
	public void setTerminoSiniestroDesc(String terminoSiniestroDesc) {
		this.terminoSiniestroDesc = terminoSiniestroDesc;
	}
	public String getTipoEstimacion() {
		return tipoEstimacion;
	}
	public void setTipoEstimacion(String tipoEstimacion) {
		this.tipoEstimacion = tipoEstimacion;
	}
	public String getTipoServicio() {
		return tipoServicio;
	}
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	public Long getMovimientoId() {
		return movimientoId;
	}
	public void setMovimientoId(Long movimientoId) {
		this.movimientoId = movimientoId;
	}
	public String getOficinaDesc() {
		return oficinaDesc;
	}
	public void setOficinaDesc(String oficinaDesc) {
		this.oficinaDesc = oficinaDesc;
	}
	public String getContratante() {
		return contratante;
	}
	public void setContratante(String contratante) {
		this.contratante = contratante;
	}
	public String getIdCoberturaClaveSubCalculo() {
		return idCoberturaClaveSubCalculo;
	}
	public void setIdCoberturaClaveSubCalculo(String idCoberturaClaveSubCalculo) {
		this.idCoberturaClaveSubCalculo = idCoberturaClaveSubCalculo;
	}
	
}