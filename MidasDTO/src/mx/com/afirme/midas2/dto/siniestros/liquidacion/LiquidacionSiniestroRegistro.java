package mx.com.afirme.midas2.dto.siniestros.liquidacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import mx.com.afirme.midas2.annotation.Exportable;

public class LiquidacionSiniestroRegistro implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idLiquidacion;
	private String oficina;
	private String estatus;
	private String tipoLiquidacion;
	private String tipoOperacion;
	private Long noProveedor;
	private String proveedor;
	private String siniestro;
	private String factura;
	private BigDecimal totalAPagar;
	private String solicitadaPor;
	private Date fechaSolicitud;
	private String autorizadoPor;
	private Date fechaDeAutorizacion;
	private Date fechaDeElaboracion;
	private String noCheque;
	private boolean isAutorizable;
	private Long   noDetalle;
	private Long  noSolCheque;
	private String estatusId;
	private Short tipoCancelacion;
	private String nombreBeneficiario;
	private String origenLiquidacion;
	private Long noLiquidacion;
	
	
	public LiquidacionSiniestroRegistro() {
		super();
	}
	
	
	public LiquidacionSiniestroRegistro(Long idLiquidacion, String oficina,
			String estatus, String tipoLiquidacion, String tipoOperacion,
			Long noProveedor, String proveedor, String siniestro,
			String factura, BigDecimal totalAPagar, String solicitadaPor,
			Date fechaSolicitud, String autorizadoPor,
			Date fechaDeAutorizacion, Date fechaDeElaboracion,
			String noCheque) {
		super();
		this.idLiquidacion = idLiquidacion;
		this.oficina = oficina;
		this.estatus = estatus;
		this.tipoLiquidacion = tipoLiquidacion;
		this.tipoOperacion = tipoOperacion;
		this.noProveedor = noProveedor;
		this.proveedor = proveedor;
		this.siniestro = siniestro;
		this.factura = factura;
		this.totalAPagar = totalAPagar;
		this.solicitadaPor = solicitadaPor;
		this.fechaSolicitud = fechaSolicitud;
		this.autorizadoPor = autorizadoPor;
		this.fechaDeAutorizacion = fechaDeAutorizacion;
		this.fechaDeElaboracion = fechaDeElaboracion;
		this.noCheque = noCheque;
	}

	public void setIdLiquidacion(Long idLiquidacion) {
		this.idLiquidacion = idLiquidacion;
	}

	public void setOficina(String oficina) {
		this.oficina = oficina;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public void setTipoLiquidacion(String tipoLiquidacion) {
		this.tipoLiquidacion = tipoLiquidacion;
	}

	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public void setNoProveedor(Long noProveedor) {
		this.noProveedor = noProveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	public void setSiniestro(String siniestro) {
		this.siniestro = siniestro;
	}

	public void setFactura(String factura) {
		this.factura = factura;
	}

	public void setTotalAPagar(BigDecimal totalAPagar) {
		this.totalAPagar = totalAPagar;
	}

	public void setSolicitadaPor(String solicitadaPor) {
		this.solicitadaPor = solicitadaPor;
	}

	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	public void setAutorizadoPor(String autorizadoPor) {
		this.autorizadoPor = autorizadoPor;
	}
	
	public void setFechaDeAutorizacion(Date fechaDeAutorizacion) {
		this.fechaDeAutorizacion = fechaDeAutorizacion;
	}

	public void setFechaDeElaboracion(Date fechaDeElaboracion) {
		this.fechaDeElaboracion = fechaDeElaboracion;
	}

	public void setNoCheque(String noCheque) {
		this.noCheque = noCheque;
	}
	@Override
	public String toString() {
		return "LiquidacionSiniestroRegistro [idLiquidacion=" + idLiquidacion
				+ ", oficina=" + oficina + ", estatus=" + estatus
				+ ", tipoLiquidacion=" + tipoLiquidacion + ", tipoOperacion="
				+ tipoOperacion + ", noProveedor=" + noProveedor
				+ ", proveedor=" + proveedor + ", siniestro=" + siniestro
				+ ", factura=" + factura + ", totalAPagar=" + totalAPagar
				+ ", solicitadaPor=" + solicitadaPor + ", fechaSolicitud="
				+ fechaSolicitud + ", autorizadoPor=" + autorizadoPor
				+ ", fechaDeAutorizacion=" + fechaDeAutorizacion
				+ ", fechaDeElaboracion=" + fechaDeElaboracion + ", noCheque="
				+ noCheque + ", isAutorizable=" + isAutorizable
				+ ", noDetalle=" + noDetalle + ", noSolCheque=" + noSolCheque
				+ ", estatusId=" + estatusId + ", tipoCancelacion="
				+ tipoCancelacion + ", nombreBeneficiario="
				+ nombreBeneficiario + ", origenLiquidacion="
				+ origenLiquidacion + "]";
	}


	public boolean isAutorizable() {
		return isAutorizable;
	}


	public void setAutorizable(boolean isAutorizable) {
		this.isAutorizable = isAutorizable;
	}


	public Long getNoDetalle() {
		return noDetalle;
	}


	public void setNoDetalle(Long noDetalle) {
		this.noDetalle = noDetalle;
	}

	public void setNoSolCheque(Long noSolCheque) {
		this.noSolCheque = noSolCheque;
	}


	public String getEstatusId() {
		return estatusId;
	}


	public void setEstatusId(String estatusId) {
		this.estatusId = estatusId;
	}


	public Short getTipoCancelacion() {
		return tipoCancelacion;
	}


	public void setTipoCancelacion(Short tipoCancelacion) {
		this.tipoCancelacion = tipoCancelacion;
	}

	public void setNombreBeneficiario(String nombreBeneficiario) {
		this.nombreBeneficiario = nombreBeneficiario;
	}


	public String getOrigenLiquidacion() {
		return origenLiquidacion;
	}


	public void setOrigenLiquidacion(String origenLiquidacion) {
		this.origenLiquidacion = origenLiquidacion;
	}
	
	public void setNoLiquidacion(Long noLiquidacion) {
		this.noLiquidacion = noLiquidacion;
	}
	
	public Long getIdLiquidacion() {
		return idLiquidacion;
	}
	

	@Exportable(columnName="Oficina", columnOrder=0, document="PROVEEDOR,BENEFICIARIO" )
	public String getOficina() {
		return oficina;
	}
	@Exportable(columnName="No. Liquidacion", columnOrder=1,  document="PROVEEDOR,BENEFICIARIO")
	public Long getNoLiquidacion() {
		return noLiquidacion;
	}
	
	@Exportable(columnName="Estatus", columnOrder=2,   document="PROVEEDOR,BENEFICIARIO")
	public String getEstatus() {
		return estatus;
	}
	@Exportable(columnName="Tipo de Liquidacion", columnOrder=3,   document="PROVEEDOR,BENEFICIARIO")
	public String getTipoLiquidacion() {
		return tipoLiquidacion;
	}
	@Exportable(columnName="Tipo de Operacion", columnOrder=4,   document="PROVEEDOR,BENEFICIARIO")
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	@Exportable(columnName="No. Siniestro", columnOrder=5,   document="PROVEEDOR,BENEFICIARIO")
	public String getSiniestro() {
		return siniestro;
	}
	@Exportable(columnName="Solicitado Por", columnOrder=6,   document="PROVEEDOR,BENEFICIARIO")
	public String getSolicitadaPor() {
		return solicitadaPor;
	}
	@Exportable(columnName="Fecha de Solicitud", columnOrder=7,   document="PROVEEDOR,BENEFICIARIO")
	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}
	@Exportable(columnName="Numero Solicitud de Cheque", columnOrder=8,   document="PROVEEDOR,BENEFICIARIO")
	public Long getNoSolCheque() {
		return noSolCheque;
	}
	@Exportable(columnName="Autorizado Por", columnOrder=9,   document="PROVEEDOR,BENEFICIARIO")
	public String getAutorizadoPor() {
		return autorizadoPor;
	}
	@Exportable(columnName="Fecha de Autorizacion De", columnOrder=10,   document="PROVEEDOR,BENEFICIARIO")
	public Date getFechaDeAutorizacion() {
		return fechaDeAutorizacion;
	}
	@Exportable(columnName="Fecha de Elaboración Cheque/Transferencia", columnOrder=11,   document="PROVEEDOR,BENEFICIARIO")
	public Date getFechaDeElaboracion() {
		return fechaDeElaboracion;
	}
	@Exportable(columnName="N° Cheque/Referencia", columnOrder=12,   document="PROVEEDOR,BENEFICIARIO")
	public String getNoCheque() {
		return noCheque;
	}
	@Exportable(columnName="Neto a Pagar", columnOrder=13, summarize=true,   document="PROVEEDOR,BENEFICIARIO")
	public BigDecimal getTotalAPagar() {
		return totalAPagar;
	}
	@Exportable(columnName="Beneficiario", columnOrder=14, document="BENEFICIARIO" )
	public String getNombreBeneficiario() {
		return nombreBeneficiario;
	}
	@Exportable(columnName="No. Proveedor", columnOrder=15, document="PROVEEDOR")
	public Long getNoProveedor() {
		return noProveedor;
	}
	@Exportable(columnName="Proveedor", columnOrder=16, document="PROVEEDOR")
	public String getProveedor() {
		return proveedor;
	}
	@Exportable(columnName="No. Factura", columnOrder=17, document="PROVEEDOR")
	public String getFactura() {
		return factura;
	}



	
	
	
}
