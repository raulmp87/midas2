package mx.com.afirme.midas2.dto.siniestros.hgs;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "valesRefacciones", propOrder = {
    "monto",
    "porcentajeIva",
    "fechaActualizacion",
    "refaccion",
    "montoIva",
    "folio",
    "estatusVale",
    "IDVale",
    "claveProveedor",
    "nombreProveedor",
    "concepto",
    "fechaExpedicion"
})

public class ValeRefaccion implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -4277697943671280673L;
	
	@XmlElement(name="monto",required=true)
	private double monto;
	
	@XmlElement(name="porcentaje_iva",required=true)
    private double porcentajeIva;
	
	@XmlElement(name="fecha_actualizacion",required=false)
    private Date fechaActualizacion;
	
	@XmlElement(name="refaccion",required=true)
    private boolean refaccion;
	
	@XmlElement(name="monto_iva",required=false)
    private double montoIva;
	
	@XmlElement(name="folio",required=true)
    private String folio;
	
	@XmlElement(name="estatus_vale",required=true)
    private String estatusVale;
	
	@XmlElement(name="id_vale",required=true)
    private Long IDVale;
	
	@XmlElement(name="clave_proveedor",required=false)
    private String claveProveedor;
	
	@XmlElement(name="nombre_proveedor",required=false)
    private String nombreProveedor;
	
	@XmlElement(name="concepto",required=false)
    private String concepto;
	
	@XmlElement(name="fecha_expedicion",required=false)
    private Date fechaExpedicion;
    
    
	public double getMonto() {
		return monto;
	}
	public double getPorcentajeIva() {
		return porcentajeIva;
	}
	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}
	public boolean isRefaccion() {
		return refaccion;
	}
	public double getMontoIva() {
		return montoIva;
	}
	public String getFolio() {
		return folio;
	}
	public String getEstatusVale() {
		return estatusVale;
	}
	public Long getIDVale() {
		return IDVale;
	}
	public String getClaveProveedor() {
		return claveProveedor;
	}
	public String getNombreProveedor() {
		return nombreProveedor;
	}
	public String getConcepto() {
		return concepto;
	}
	public Date getFechaExpedicion() {
		return fechaExpedicion;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	public void setPorcentajeIva(double porcentajeIva) {
		this.porcentajeIva = porcentajeIva;
	}
	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}
	public void setRefaccion(boolean refaccion) {
		this.refaccion = refaccion;
	}
	public void setMontoIva(double montoIva) {
		this.montoIva = montoIva;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public void setEstatusVale(String estatusVale) {
		this.estatusVale = estatusVale;
	}
	public void setIDVale(Long iDVale) {
		IDVale = iDVale;
	}
	public void setClaveProveedor(String claveProveedor) {
		this.claveProveedor = claveProveedor;
	}
	public void setNombreProveedor(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public void setFechaExpedicion(Date fechaExpedicion) {
		this.fechaExpedicion = fechaExpedicion;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((IDVale == null) ? 0 : IDVale.hashCode());
		result = prime * result + ((folio == null) ? 0 : folio.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValeRefaccion other = (ValeRefaccion) obj;
		if (IDVale == null) {
			if (other.IDVale != null)
				return false;
		} else if (!IDVale.equals(other.IDVale))
			return false;
		if (folio == null) {
			if (other.folio != null)
				return false;
		} else if (!folio.equals(other.folio))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "ValeRefaccion [monto=" + monto + ", porcentajeIva="
				+ porcentajeIva + ", fechaActualizacion=" + fechaActualizacion
				+ ", refaccion=" + refaccion + ", montoIva=" + montoIva
				+ ", folio=" + folio + ", estatusVale=" + estatusVale
				+ ", IDVale=" + IDVale + ", claveProveedor=" + claveProveedor
				+ ", nombreProveedor=" + nombreProveedor + ", concepto="
				+ concepto + ", fechaExpedicion=" + fechaExpedicion + "]";
	}
    
    
}
