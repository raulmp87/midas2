package mx.com.afirme.midas2.dto.siniestros.recuperacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class IndicadoresVentaDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal eficienciaIndeminizacion;
	private BigDecimal eficienciaProvision;
	private Date fechaAplicacionIngreso;
	private Date fechaCierreDeSubasta;
	private Date fechaConfirmacionIngreso;
	private Date fechaIndeminizacion;
	private Date fechaProvision;
	private Date fechaSiniestro;
	private Date fechaInicioSubasta;
	private Date fechaRecepcionPapeleo;
	private Date fechaDeDeterminacion;
	private Date fechaAsignacion;
	private int tiempoAplicacionIngreso;
	private int tiempoConfirmacionIngreso;
	private int tiempoDeAsignacion;
	private int tiempoDeDocumentacion;
	private int tiempoDeIndeminizacion;
	private int tiempoDeSubasta;
	private int tiempoPagoDesdeDocumentacion;
	private int tiempoPagoDesdeDeterminacion;
	private int tiempoPagoDesdeIndeminizacion;
	private int tiempoPromedioTotalVenta;
	private int tiempoPromedioVenta;
	private int tiempoAutorizacionPagoOrdenCompra;
	private BigDecimal totalDeVenta;
	private BigDecimal valorEstimadoEnDeterminacion;
	private BigDecimal valorProvisionado;
	
	
	
	
	
	public BigDecimal getEficienciaIndeminizacion() {
		return eficienciaIndeminizacion;
	}
	public void setEficienciaIndeminizacion(BigDecimal eficienciaIndeminizacion) {
		this.eficienciaIndeminizacion = eficienciaIndeminizacion;
	}
	public BigDecimal getEficienciaProvision() {
		return eficienciaProvision;
	}
	public void setEficienciaProvision(BigDecimal eficienciaProvision) {
		this.eficienciaProvision = eficienciaProvision;
	}
	public Date getFechaAplicacionIngreso() {
		return fechaAplicacionIngreso;
	}
	public void setFechaAplicacionIngreso(Date fechaAplicacionIngreso) {
		this.fechaAplicacionIngreso = fechaAplicacionIngreso;
	}
	public Date getFechaCierreDeSubasta() {
		return fechaCierreDeSubasta;
	}
	public void setFechaCierreDeSubasta(Date fechaCierreDeSubasta) {
		this.fechaCierreDeSubasta = fechaCierreDeSubasta;
	}
	public Date getFechaConfirmacionIngreso() {
		return fechaConfirmacionIngreso;
	}
	public void setFechaConfirmacionIngreso(Date fechaConfirmacionIngreso) {
		this.fechaConfirmacionIngreso = fechaConfirmacionIngreso;
	}
	public Date getFechaIndeminizacion() {
		return fechaIndeminizacion;
	}
	public void setFechaIndeminizacion(Date fechaIndeminizacion) {
		this.fechaIndeminizacion = fechaIndeminizacion;
	}
	public Date getFechaProvision() {
		return fechaProvision;
	}
	public void setFechaProvision(Date fechaProvision) {
		this.fechaProvision = fechaProvision;
	}
	public Date getFechaSiniestro() {
		return fechaSiniestro;
	}
	public void setFechaSiniestro(Date fechaSiniestro) {
		this.fechaSiniestro = fechaSiniestro;
	}
	public int getTiempoAplicacionIngreso() {
		return tiempoAplicacionIngreso;
	}
	public void setTiempoAplicacionIngreso(int tiempoAplicacionIngreso) {
		this.tiempoAplicacionIngreso = tiempoAplicacionIngreso;
	}
	public int getTiempoConfirmacionIngreso() {
		return tiempoConfirmacionIngreso;
	}
	public void setTiempoConfirmacionIngreso(int tiempoConfirmacionIngreso) {
		this.tiempoConfirmacionIngreso = tiempoConfirmacionIngreso;
	}
	public int getTiempoDeAsignacion() {
		return tiempoDeAsignacion;
	}
	public void setTiempoDeAsignacion(int tiempoDeAsignacion) {
		this.tiempoDeAsignacion = tiempoDeAsignacion;
	}
	public int getTiempoDeDocumentacion() {
		return tiempoDeDocumentacion;
	}
	public void setTiempoDeDocumentacion(int tiempoDeDocumentacion) {
		this.tiempoDeDocumentacion = tiempoDeDocumentacion;
	}
	public int getTiempoDeIndeminizacion() {
		return tiempoDeIndeminizacion;
	}
	public void setTiempoDeIndeminizacion(int tiempoDeIndeminizacion) {
		this.tiempoDeIndeminizacion = tiempoDeIndeminizacion;
	}
	public int getTiempoDeSubasta() {
		return tiempoDeSubasta;
	}
	public void setTiempoDeSubasta(int tiempoDeSubasta) {
		this.tiempoDeSubasta = tiempoDeSubasta;
	}
	public int getTiempoPagoDesdeDocumentacion() {
		return tiempoPagoDesdeDocumentacion;
	}
	public void setTiempoPagoDesdeDocumentacion(
			int tiempoPagoDesdeDocumentacion) {
		this.tiempoPagoDesdeDocumentacion = tiempoPagoDesdeDocumentacion;
	}
	public int getTiempoPagoDesdeIndeminizacion() {
		return tiempoPagoDesdeIndeminizacion;
	}
	public void setTiempoPagoDesdeIndeminizacion(
			int tiempoPagoDesdeIndeminizacion) {
		this.tiempoPagoDesdeIndeminizacion = tiempoPagoDesdeIndeminizacion;
	}
	public int getTiempoPromedioTotalVenta() {
		return tiempoPromedioTotalVenta;
	}
	public void setTiempoPromedioTotalVenta(int tiempoPromedioTotalVenta) {
		this.tiempoPromedioTotalVenta = tiempoPromedioTotalVenta;
	}
	public int getTiempoPromedioVenta() {
		return tiempoPromedioVenta;
	}
	public void setTiempoPromedioVenta(int tiempoPromedioVenta) {
		this.tiempoPromedioVenta = tiempoPromedioVenta;
	}
	public BigDecimal getTotalDeVenta() {
		return totalDeVenta;
	}
	public void setTotalDeVenta(BigDecimal totalDeVenta) {
		this.totalDeVenta = totalDeVenta;
	}
	public BigDecimal getValorEstimadoEnDeterminacion() {
		return valorEstimadoEnDeterminacion;
	}
	public void setValorEstimadoEnDeterminacion(
			BigDecimal valorEstimadoEnDeterminacion) {
		this.valorEstimadoEnDeterminacion = valorEstimadoEnDeterminacion;
	}
	public BigDecimal getValorProvisionado() {
		return valorProvisionado;
	}
	public void setValorProvisionado(BigDecimal valorProvisionado) {
		this.valorProvisionado = valorProvisionado;
	}
	public Date getFechaInicioSubasta() {
		return fechaInicioSubasta;
	}
	public void setFechaInicioSubasta(Date fechaInicioSubasta) {
		this.fechaInicioSubasta = fechaInicioSubasta;
	}
	public Date getFechaRecepcionPapeleo() {
		return fechaRecepcionPapeleo;
	}
	public void setFechaRecepcionPapeleo(Date fechaRecepcionPapeleo) {
		this.fechaRecepcionPapeleo = fechaRecepcionPapeleo;
	}
	public Date getFechaDeDeterminacion() {
		return fechaDeDeterminacion;
	}
	public void setFechaDeDeterminacion(Date fechaDeDeterminacion) {
		this.fechaDeDeterminacion = fechaDeDeterminacion;
	}
	public int getTiempoPagoDesdeDeterminacion() {
		return tiempoPagoDesdeDeterminacion;
	}
	public void setTiempoPagoDesdeDeterminacion(int tiempoPagoDesdeDeterminacion) {
		this.tiempoPagoDesdeDeterminacion = tiempoPagoDesdeDeterminacion;
	}
	public int getTiempoAutorizacionPagoOrdenCompra() {
		return tiempoAutorizacionPagoOrdenCompra;
	}
	public void setTiempoAutorizacionPagoOrdenCompra(
			int tiempoAutorizacionPagoOrdenCompra) {
		this.tiempoAutorizacionPagoOrdenCompra = tiempoAutorizacionPagoOrdenCompra;
	}
	public Date getFechaAsignacion() {
		return fechaAsignacion;
	}
	public void setFechaAsignacion(Date fechaAsignacion) {
		this.fechaAsignacion = fechaAsignacion;
	}

}
