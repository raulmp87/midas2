package mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos;


import java.math.BigDecimal;
import java.util.Date;

import mx.com.afirme.midas2.util.StringUtil;

import org.springframework.stereotype.Component;


/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 22-jun-2015 02:04:49 p.m.
 */
@Component
public class MovimientoAcreedorDTO {

	private String cuentaContable;
	private Long idCuentaContable;
	private Date fechaTraspaso;
	private Date fechaTraspasoFin;
	private Date fechaTraspasoIni;
	private BigDecimal importe;
	private BigDecimal importeFin;
	private BigDecimal importeIni;
	private String motivoCancelacion;
	private String motivoCancelacionDesc;
	private Long movimientoId;
	private String refunic;
	private String siniestroOrigen;
	private String tipoRecuperacion;
	private String tipoRecuperacionDesc;
	private String movAcreedoresConcat;

	public MovimientoAcreedorDTO(){

	}

	public String getCuentaContable() {
		return cuentaContable;
	}

	public void setCuentaContable(String cuentaContable) {
		this.cuentaContable = cuentaContable;
	}

	public Date getFechaTraspaso() {
		return fechaTraspaso;
	}

	public void setFechaTraspaso(Date fechaTraspaso) {
		this.fechaTraspaso = fechaTraspaso;
	}

	public Date getFechaTraspasoFin() {
		return fechaTraspasoFin;
	}

	public void setFechaTraspasoFin(Date fechaTraspasoFin) {
		this.fechaTraspasoFin = fechaTraspasoFin;
	}

	public Date getFechaTraspasoIni() {
		return fechaTraspasoIni;
	}

	public void setFechaTraspasoIni(Date fechaTraspasoIni) {
		this.fechaTraspasoIni = fechaTraspasoIni;
	}

	public BigDecimal getImporte() {
		return importe;
	}

	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	public BigDecimal getImporteFin() {
		return importeFin;
	}

	public void setImporteFin(BigDecimal importeFin) {
		this.importeFin = importeFin;
	}

	public BigDecimal getImporteIni() {
		return importeIni;
	}

	public void setImporteIni(BigDecimal importeIni) {
		this.importeIni = importeIni;
	}

	public String getMotivoCancelacion() {
		return motivoCancelacion;
	}

	public void setMotivoCancelacion(String motivoCancelacion) {
		this.motivoCancelacion = motivoCancelacion;
	}

	public String getMotivoCancelacionDesc() {
		return motivoCancelacionDesc;
	}

	public void setMotivoCancelacionDesc(String motivoCancelacionDesc) {
		this.motivoCancelacionDesc = motivoCancelacionDesc;
	}

	public Long getMovimientoId() {
		return movimientoId;
	}

	public void setMovimientoId(Long movimientoId) {
		this.movimientoId = movimientoId;
	}

	public String getRefunic() {
		return refunic;
	}

	public void setRefunic(String refunic) {
		this.refunic = refunic;
	}

	public String getSiniestroOrigen() {
		return siniestroOrigen;
	}

	public void setSiniestroOrigen(String siniestroOrigen) {
		this.siniestroOrigen = siniestroOrigen;
	}

	public String getTipoRecuperacion() {
		return tipoRecuperacion;
	}

	public void setTipoRecuperacion(String tipoRecuperacion) {
		this.tipoRecuperacion = tipoRecuperacion;
	}

	public String getTipoRecuperacionDesc() {
		return tipoRecuperacionDesc;
	}

	public void setTipoRecuperacionDesc(String tipoRecuperacionDesc) {
		this.tipoRecuperacionDesc = tipoRecuperacionDesc;
	}

	/**
	 * @return the movAcreedoresConcat
	 */
	public String getMovAcreedoresConcat() {
		return movAcreedoresConcat;
	}

	/**
	 * @param movAcreedoresConcat the movAcreedoresConcat to set
	 */
	public void setMovAcreedoresConcat(String movAcreedoresConcat) {
		if(!StringUtil.isEmpty(movAcreedoresConcat)){
			this.movAcreedoresConcat = movAcreedoresConcat;
		}
	}

	/**
	 * @return the idCuentaContable
	 */
	public Long getIdCuentaContable() {
		return idCuentaContable;
	}

	/**
	 * @param idCuentaContable the idCuentaContable to set
	 */
	public void setIdCuentaContable(Long idCuentaContable) {
		this.idCuentaContable = idCuentaContable;
	}
	
}