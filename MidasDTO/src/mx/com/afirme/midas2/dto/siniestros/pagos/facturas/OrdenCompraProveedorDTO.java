package mx.com.afirme.midas2.dto.siniestros.pagos.facturas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class OrdenCompraProveedorDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long idOrdenCompra;
	private Long idFactura;	
	private Date fechaRegistro;
	private Long idSiniestro;
	private String numeroSiniestro;
	private BigDecimal subtotal;
	private BigDecimal iva;
	private BigDecimal ivaRetenido;
	private BigDecimal isr;
	private BigDecimal total;
	private String afectado;
	private String oficina;
	private Long idOficina;
	private String nombreAgrupador;
	private String tipoAgrupador;
	private Integer compania;
	private BigDecimal totalDe;
	private BigDecimal totalHasta;
	private Date fechaRegistroDe;
	private Date fechaRegistroHasta;
	private String siniestroTercero;
	private Date  fechaCarta;
	private Date fechaRecepcionCarta;
	private String folioPaseAtencion;
	private BigDecimal reservaDisponible;
	
	public OrdenCompraProveedorDTO()
	{
		
	}
	
	public OrdenCompraProveedorDTO(Long idOrdenCompra, Long idFactura,
			Date fechaRegistro, Long idSiniestro, String numeroSiniestro,
			BigDecimal subtotal, BigDecimal iva, BigDecimal ivaRetenido, BigDecimal isr,
			BigDecimal total) {
		super();
		this.idOrdenCompra = idOrdenCompra;
		this.idFactura = idFactura;
		this.fechaRegistro = fechaRegistro;
		this.idSiniestro = idSiniestro;
		this.numeroSiniestro = numeroSiniestro;
		this.subtotal = subtotal;
		this.iva = iva;
		this.ivaRetenido = ivaRetenido;
		this.isr = isr;
		this.total = total;
	}
	public Long getIdOrdenCompra() {
		return idOrdenCompra;
	}
	public void setIdOrdenCompra(Long idOrdenCompra) {
		this.idOrdenCompra = idOrdenCompra;
	}
	public Long getIdFactura() {
		return idFactura;
	}
	public void setIdFactura(Long idFactura) {
		this.idFactura = idFactura;
	}
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public Long getIdSiniestro() {
		return idSiniestro;
	}
	public void setIdSiniestro(Long idSiniestro) {
		this.idSiniestro = idSiniestro;
	}
	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}

	public BigDecimal getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public BigDecimal getIva() {
		return iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public BigDecimal getIvaRetenido() {
		return ivaRetenido;
	}

	public void setIvaRetenido(BigDecimal ivaRetenido) {
		this.ivaRetenido = ivaRetenido;
	}

	public BigDecimal getIsr() {
		return isr;
	}

	public void setIsr(BigDecimal isr) {
		this.isr = isr;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public String getAfectado() {
		return afectado;
	}

	public void setAfectado(String afectado) {
		this.afectado = afectado;
	}

	public String getOficina() {
		return oficina;
	}

	public void setOficina(String oficina) {
		this.oficina = oficina;
	}

	public Long getIdOficina() {
		return idOficina;
	}

	public void setIdOficina(Long idOficina) {
		this.idOficina = idOficina;
	}

	public String getNombreAgrupador() {
		return nombreAgrupador;
	}

	public void setNombreAgrupador(String nombreAgrupador) {
		this.nombreAgrupador = nombreAgrupador;
	}

	public Integer getCompania() {
		return compania;
	}

	public void setCompania(Integer compania) {
		this.compania = compania;
	}

	public BigDecimal getTotalDe() {
		return totalDe;
	}

	public void setTotalDe(BigDecimal totalDe) {
		this.totalDe = totalDe;
	}

	public BigDecimal getTotalHasta() {
		return totalHasta;
	}

	public void setTotalHasta(BigDecimal totalHasta) {
		this.totalHasta = totalHasta;
	}

	public Date getFechaRegistroDe() {
		return fechaRegistroDe;
	}

	public void setFechaRegistroDe(Date fechaRegistroDe) {
		this.fechaRegistroDe = fechaRegistroDe;
	}

	public Date getFechaRegistroHasta() {
		return fechaRegistroHasta;
	}

	public void setFechaRegistroHasta(Date fechaRegistroHasta) {
		this.fechaRegistroHasta = fechaRegistroHasta;
	}

	public String getSiniestroTercero() {
		return siniestroTercero;
	}

	public void setSiniestroTercero(String siniestroTercero) {
		this.siniestroTercero = siniestroTercero;
	}

	public Date getFechaCarta() {
		return fechaCarta;
	}

	public void setFechaCarta(Date fechaCarta) {
		this.fechaCarta = fechaCarta;
	}

	public Date getFechaRecepcionCarta() {
		return fechaRecepcionCarta;
	}

	public void setFechaRecepcionCarta(Date fechaRecepcionCarta) {
		this.fechaRecepcionCarta = fechaRecepcionCarta;
	}

	public String getFolioPaseAtencion() {
		return folioPaseAtencion;
	}

	public void setFolioPaseAtencion(String folioPaseAtencion) {
		this.folioPaseAtencion = folioPaseAtencion;
	}

	public BigDecimal getReservaDisponible() {
		return reservaDisponible;
	}

	public void setReservaDisponible(BigDecimal reservaDisponible) {
		this.reservaDisponible = reservaDisponible;
	}

	public String getTipoAgrupador() {
		return tipoAgrupador;
	}

	public void setTipoAgrupador(String tipoAgrupador) {
		this.tipoAgrupador = tipoAgrupador;
	}
}
