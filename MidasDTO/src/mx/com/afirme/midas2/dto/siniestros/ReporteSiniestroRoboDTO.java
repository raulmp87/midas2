package mx.com.afirme.midas2.dto.siniestros;

import java.io.Serializable;
import java.util.Date;
import mx.com.afirme.midas2.annotation.Exportable;
public class ReporteSiniestroRoboDTO implements Serializable{

	private static final long serialVersionUID = 1914515880899142008L;
	private String numSiniestro;
	private String numReporte;
	private String numPoliza;
	private String numSerie;
	private String numActa;
	private String tipoRobo;
	private String estatus;	
	private Long oficinaId;
	private String oficina;

	private Long estadoId;
	private String estado;
	private String municipio;
	private Long municipioId;
	private Date fechaIniReporte;
	private Date fechaFinReporte;
	private Date fechaIniOcurrido;
	private Date fechaFinOcurrido;
	private Boolean servPublico;
	private Boolean servParticular;
	
	private String tipoVehiculo;

	
	private String piblico;
	private String particular;
	
	
	private String nombrePersona;
	private Long reporteCabinaId;	
	private String nombreAsegurado;
	private Date fechaSiniestro;
	private String tipoSiniestro;
	private String terminoSiniestro;
	
	private String ajustador;
	private String razonSocial;
	private Long idReporteCabina;
	private Long idCobertura;
	private Long reporteRoboId;

	
	private String fechaReporte;
	private String fechaOcurrido;
	
	
	private String claveOficina;
	private String consecutivoReporte;
	private String anioReporte;
	
	private String claveOficinaSiniestro;
	private String consecutivoReporteSiniestro;
	private String anioReporteSiniestro 
	;

	
	
	
	@Exportable(columnName="Número de Siniestro", columnOrder=0)
	public String getNumSiniestro() {
		return numSiniestro;
	}
	
	@Exportable(columnName="Número de Reporte", columnOrder=1)
	public String getNumReporte() {
		return numReporte;
	}
	
	@Exportable(columnName="Número de Poliza", columnOrder=2)
	public String getNumPoliza() {
		return numPoliza;
	}
	@Exportable(columnName="Número de Serie", columnOrder=3)
	public String getNumSerie() {
		return numSerie;
	}

	@Exportable(columnName="Tipo Vehiculo", columnOrder=4)
	public String getTipoVehiculo() {
		return tipoVehiculo;
	}
	
	
	@Exportable(columnName="Número de Acta", columnOrder=5)
	public String getNumActa() {
		return numActa;
	}
	
	@Exportable(columnName="Tipo Robo", columnOrder=6)
	public String getTipoRobo() {
		return tipoRobo;
	}
	@Exportable(columnName="Estatus", columnOrder=7)
	public String getEstatus() {
		return estatus;
	}
	@Exportable(columnName="Oficina", columnOrder=8)
	public String getOficina() {
		return oficina;
	}
	
	@Exportable(columnName="Estado", columnOrder=9)
	public String getEstado() {
		return estado;
	}
	
	@Exportable(columnName="Municipio", columnOrder=10)
	public String getMunicipio() {
		return municipio;
	}
	
	@Exportable(columnName="Fecha Reporte", columnOrder=11)
	public String getFechaReporte() {
		return fechaReporte;
	}
	@Exportable(columnName="Fecha Ocurrido", columnOrder=12)
	public String getFechaOcurrido() {
		return fechaOcurrido;
	}
	
	
	
	public void setFechaReporte(String fechaReporte) {
		this.fechaReporte = fechaReporte;
	}
	
	public void setFechaOcurrido(String fechaOcurrido) {
		this.fechaOcurrido = fechaOcurrido;
	}
	public Long getIdCobertura() {
		return idCobertura;
	}
	public void setIdCobertura(Long idCobertura) {
		this.idCobertura = idCobertura;
	}
	public Long getIdReporteCabina() {
		return idReporteCabina;
	}
	public void setIdReporteCabina(Long idReporteCabina) {
		this.idReporteCabina = idReporteCabina;
	}
	
	public void setNumActa(String numActa) {
		this.numActa = numActa;
	}
	
	public void setNumPoliza(String numPoliza) {
		this.numPoliza = numPoliza;
	}
	
	public void setNumSerie(String numSerie) {
		this.numSerie = numSerie;
	}
	
	public void setNumSiniestro(String numSiniestro) {
		this.numSiniestro = numSiniestro;
	}
	
	
	
	public void setNumReporte(String numReporte) {
		this.numReporte = numReporte;
	}
	/**
	 * @return the oficinaId
	 */
	public Long getOficinaId() {
		return oficinaId;
	}
	/**
	 * @param oficinaId the oficinaId to set
	 */
	public void setOficinaId(Long oficinaId) {
		this.oficinaId = oficinaId;
	}
	/**
	 * @return the fechaIniReporte
	 */

	public Date getFechaIniReporte() {
		return fechaIniReporte;
	}
	/**
	 * @param fechaIniReporte the fechaIniReporte to set
	 */
	public void setFechaIniReporte(Date fechaIniReporte) {
		this.fechaIniReporte = fechaIniReporte;
	}
	/**
	 * @return the fechaFinReporte
	 */
	public Date getFechaFinReporte() {
		return fechaFinReporte;
	}
	/**
	 * @param fechaFinReporte the fechaFinReporte to set
	 */
	public void setFechaFinReporte(Date fechaFinReporte) {
		this.fechaFinReporte = fechaFinReporte;
	}
	/**
	 * @return the nombrePersona
	 */
	public String getNombrePersona() {
		return nombrePersona;
	}
	/**
	 * @param nombrePersona the nombrePersona to set
	 */
	public void setNombrePersona(String nombrePersona) {
		this.nombrePersona = nombrePersona;
	}
	/**
	 * @return the reporteCabinaId
	 */
	public Long getReporteCabinaId() {
		return reporteCabinaId;
	}
	/**
	 * @param reporteCabinaId the reporteCabinaId to set
	 */
	public void setReporteCabinaId(Long reporteCabinaId) {
		this.reporteCabinaId = reporteCabinaId;
	}
	

	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}	
	
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}
	
	public String getTipoSiniestro() {
		return tipoSiniestro;
	}
	public void setTipoSiniestro(String tipoSiniestro) {
		this.tipoSiniestro = tipoSiniestro;
	}
	
	public String getTerminoSiniestro() {
		return terminoSiniestro;
	}
	public void setTerminoSiniestro(String terminoSiniestro) {
		this.terminoSiniestro = terminoSiniestro;
	}
	
	public String getAjustador() {
		return ajustador;
	}
	public void setAjustador(String ajustador) {
		this.ajustador = ajustador;
	}

	
	public Date getFechaSiniestro() {
		return fechaSiniestro;
	}
	public void setFechaSiniestro(Date fechaSiniestro) {
		this.fechaSiniestro = fechaSiniestro;
	}
	

	
	
	public void setTipoRobo(String tipoRobo) {
		this.tipoRobo = tipoRobo;
	}
	
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public Long getEstadoId() {
		return estadoId;
	}
	public void setEstadoId(Long estadoId) {
		this.estadoId = estadoId;
	}
	
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public Long getMunicipioId() {
		return municipioId;
	}
	public void setMunicipioId(Long municipioId) {
		this.municipioId = municipioId;
	}
	public Date getFechaIniOcurrido() {
		return fechaIniOcurrido;
	}
	public void setFechaIniOcurrido(Date fechaIniOcurrido) {
		this.fechaIniOcurrido = fechaIniOcurrido;
	}
	public Date getFechaFinOcurrido() {
		return fechaFinOcurrido;
	}
	public void setFechaFinOcurrido(Date fechaFinOcurrido) {
		this.fechaFinOcurrido = fechaFinOcurrido;
	}
	
	public void setTipoVehiculo(String tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}
	public Boolean getServPublico() {
		return servPublico;
	}
	public void setServPublico(Boolean servPublico) {
		this.servPublico = servPublico;
	}
	public Boolean getServParticular() {
		return servParticular;
	}
	public void setServParticular(Boolean servParticular) {
		this.servParticular = servParticular;
	}
	public String getPiblico() {
		return piblico;
	}
	public void setPiblico(String piblico) {
		this.piblico = piblico;
	}
	public String getClaveOficina() {
		return claveOficina;
	}
	public void setClaveOficina(String claveOficina) {
		this.claveOficina = claveOficina;
	}
	public String getConsecutivoReporte() {
		return consecutivoReporte;
	}
	public void setConsecutivoReporte(String consecutivoReporte) {
		this.consecutivoReporte = consecutivoReporte;
	}
	public String getAnioReporte() {
		return anioReporte;
	}
	public void setAnioReporte(String anioReporte) {
		this.anioReporte = anioReporte;
	}
	public String getParticular() {
		return particular;
	}
	public void setParticular(String particular) {
		this.particular = particular;
	}
	
	public void setOficina(String oficina) {
		this.oficina = oficina;
	}
	public Long getReporteRoboId() {
		return reporteRoboId;
	}
	public void setReporteRoboId(Long reporteRoboId) {
		this.reporteRoboId = reporteRoboId;
	}

	public String getClaveOficinaSiniestro() {
		return claveOficinaSiniestro;
	}

	public void setClaveOficinaSiniestro(String claveOficinaSiniestro) {
		this.claveOficinaSiniestro = claveOficinaSiniestro;
	}

	public String getConsecutivoReporteSiniestro() {
		return consecutivoReporteSiniestro;
	}

	public void setConsecutivoReporteSiniestro(String consecutivoReporteSiniestro) {
		this.consecutivoReporteSiniestro = consecutivoReporteSiniestro;
	}

	public String getAnioReporteSiniestro() {
		return anioReporteSiniestro;
	}

	public void setAnioReporteSiniestro(String anioReporteSiniestro) {
		this.anioReporteSiniestro = anioReporteSiniestro;
	}
	
	
}
