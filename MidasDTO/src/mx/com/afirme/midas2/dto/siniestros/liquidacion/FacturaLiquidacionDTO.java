package mx.com.afirme.midas2.dto.siniestros.liquidacion;

import java.io.Serializable;
import java.math.BigDecimal;

import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.siniestros.pagos.OrdenPagoSiniestro;
import mx.com.afirme.midas2.dto.siniestros.pagos.DesglosePagoConceptoDTO;

public class FacturaLiquidacionDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idLiquidacionSiniestro;
	private Long idFactura;
	private String numeroFactura;
	private String numeroOrdenPago;
	private OrdenPagoSiniestro ordenPago;
	private DesglosePagoConceptoDTO desgloseConceptos;
	private SiniestroCabina siniestro;
	
	public FacturaLiquidacionDTO(Long idFactura, String numeroFactura, SiniestroCabina siniestro, OrdenPagoSiniestro ordenPago)
	{
		this.idFactura = idFactura;
		this.numeroFactura = numeroFactura;
		this.ordenPago = ordenPago;
		this.siniestro = siniestro;
	}
	
	public Long getIdLiquidacionSiniestro() {
		return idLiquidacionSiniestro;
	}
	public void setIdLiquidacionSiniestro(Long idLiquidacionSiniestro) {
		this.idLiquidacionSiniestro = idLiquidacionSiniestro;
	}
	public Long getIdFactura() {
		return idFactura;
	}
	public void setIdFactura(Long idFactura) {
		this.idFactura = idFactura;
	}
	public String getNumeroFactura() {
		return numeroFactura;
	}
	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}
	
	public String getNumeroOrdenPago() {
		return numeroOrdenPago;
	}
	public void setNumeroOrdenPago(String numeroOrdenPago) {
		this.numeroOrdenPago = numeroOrdenPago;
	}
	public DesglosePagoConceptoDTO getDesgloseConceptos() {
		return desgloseConceptos;
	}
	public void setDesgloseConceptos(DesglosePagoConceptoDTO desgloseConceptos) {
		this.desgloseConceptos = desgloseConceptos;
	}
	public OrdenPagoSiniestro getOrdenPago() {
		return ordenPago;
	}
	public void setOrdenPago(OrdenPagoSiniestro ordenPago) {
		this.ordenPago = ordenPago;
	}

	public SiniestroCabina getSiniestro() {
		return siniestro;
	}

	public void setSiniestro(SiniestroCabina siniestro) {
		this.siniestro = siniestro;
	}	
}
