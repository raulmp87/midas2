package mx.com.afirme.midas2.dto.siniestros.catalogos.conceptos;

import java.math.BigDecimal;

public class SeccionDTOConcepto {
	
	private BigDecimal idSeccionConcepto;
	private String nombreSeccionConcepto;
	
	public SeccionDTOConcepto() {}
	
	public SeccionDTOConcepto(BigDecimal idSeccionConcepto, String nombreSeccionConcepto) {
		this.idSeccionConcepto = idSeccionConcepto;
		this.nombreSeccionConcepto = nombreSeccionConcepto;
	}
	
	
	public String getNombreSeccionConcepto() {
		return nombreSeccionConcepto;
	}
	public void setNombreSeccionConcepto(String nombreSeccionConcepto) {
		this.nombreSeccionConcepto = nombreSeccionConcepto;
	}
	
	public BigDecimal getIdSeccionConcepto() {
		return idSeccionConcepto;
	}
	public void setIdSeccionConcepto(BigDecimal idSeccionConcepto) {
		this.idSeccionConcepto = idSeccionConcepto;
	}


}
