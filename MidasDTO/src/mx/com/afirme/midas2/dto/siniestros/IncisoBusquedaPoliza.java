package mx.com.afirme.midas2.dto.siniestros;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;


@XmlAccessorType(XmlAccessType.FIELD)
public class IncisoBusquedaPoliza implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<IncisoPolizaDTO> lIncisosPolizaDTO;
	private String  mensajesError;
	
	public String getMensajesError() {
		return mensajesError;
	}
	public void setMensajesError(String mensajesError) {
		this.mensajesError = mensajesError;
	}
	public List<IncisoPolizaDTO> getlIncisosPolizaDTO() {
		return lIncisosPolizaDTO;
	}
	public void setlIncisosPolizaDTO(List<IncisoPolizaDTO> lIncisosPolizaDTO) {
		this.lIncisosPolizaDTO = lIncisosPolizaDTO;
	}
	

}
