package mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Israel
 * @version 1.0
 * @created 31-mar.-2015 12:43:24 p. m.
 */
public class NotasCreditoRegistro implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int estatus;
	private String estatusDesc;
	private Date fechaBaja;
	private Date fechaNotaCredito;
	private Date fechaRegistro;
	private String folioNotaCredito;
	private BigDecimal montoNotaCredito;
	private String noFactura;
	private String nombreProveedor;
	private Long noProveedor;
	private Long notaCreditoId;
	private Long idFactura;
	private String noSiniestro;
	private Long idConjunto;
	
	private BigDecimal iva;
	private BigDecimal ivaRetenido;
	private BigDecimal isr;
	private BigDecimal subTotal;
	private BigDecimal montoTotal;
	
	
	private String tipo;
	

	public NotasCreditoRegistro(){

	}
	
	public NotasCreditoRegistro(Long idConjunto,Long notaCreditoId,Long idFactura, String folioNotaCredito, 
			BigDecimal monto, Long noProveedor, String nombreProveedor,String folioFactura, String noSiniestro, 
			Date fechaRegistro, Date fechaBaja, String estatusDesc){

		this.idConjunto = idConjunto;
		this.notaCreditoId = notaCreditoId;
		this.idFactura = idFactura;
		this.folioNotaCredito = folioNotaCredito;
		this.montoNotaCredito = monto;
		this.noProveedor = noProveedor;
		this.nombreProveedor = nombreProveedor;
		this.noFactura= folioFactura;
		this.noSiniestro = noSiniestro;
		this.fechaRegistro = fechaRegistro;
		this.fechaBaja = fechaBaja;
		this.estatusDesc = estatusDesc;
	}
	
	

	public NotasCreditoRegistro(Long notaCreditoId, Long idFactura, String noFactura,
			BigDecimal iva, BigDecimal ivaRetenido, BigDecimal isr,
			BigDecimal subTotal, BigDecimal montoTotal) {
		super();
		this.notaCreditoId = notaCreditoId;
		this.idFactura = idFactura;
		this.noFactura = noFactura;//No. Nota Credito
		this.iva = iva;
		this.ivaRetenido = ivaRetenido;
		this.isr = isr;
		this.subTotal = subTotal;
		this.montoTotal = montoTotal;
	}
	
	public NotasCreditoRegistro(Long notaCreditoId, Long idFactura, String noFactura,
			BigDecimal iva, BigDecimal ivaRetenido, BigDecimal isr,
			BigDecimal subTotal, BigDecimal montoTotal, String tipo) {
		super();
		this.notaCreditoId = notaCreditoId;
		this.idFactura = idFactura;
		this.noFactura = noFactura;//No. Nota Credito
		this.iva = iva;
		this.ivaRetenido = ivaRetenido;
		this.isr = isr;
		this.subTotal = subTotal;
		this.montoTotal = montoTotal;
		this.tipo = tipo;
		
	}

	public int getEstatus() {
		return estatus;
	}

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}

	public String getEstatusDesc() {
		return estatusDesc;
	}

	public void setEstatusDesc(String estatusDesc) {
		this.estatusDesc = estatusDesc;
	}

	public Date getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public Date getFechaNotaCredito() {
		return fechaNotaCredito;
	}

	public void setFechaNotaCredito(Date fechaNotaCredito) {
		this.fechaNotaCredito = fechaNotaCredito;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getFolioNotaCredito() {
		return folioNotaCredito;
	}

	public void setFolioNotaCredito(String folioNotaCredito) {
		this.folioNotaCredito = folioNotaCredito;
	}

	public BigDecimal getMontoNotaCredito() {
		return montoNotaCredito;
	}

	public void setMontoNotaCredito(BigDecimal montoNotaCredito) {
		this.montoNotaCredito = montoNotaCredito;
	}

	public String getNoFactura() {
		return noFactura;
	}

	public void setNoFactura(String noFactura) {
		this.noFactura = noFactura;
	}

	public String getNombreProveedor() {
		return nombreProveedor;
	}

	public void setNombreProveedor(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}

	public Long getNoProveedor() {
		return noProveedor;
	}

	public void setNoProveedor(Long noProveedor) {
		this.noProveedor = noProveedor;
	}

	public Long getNotaCreditoId() {
		return notaCreditoId;
	}

	public void setNotaCreditoId(Long notaCreditoId) {
		this.notaCreditoId = notaCreditoId;
	}

	public Long getIdFactura() {
		return idFactura;
	}

	public void setIdFactura(Long idFactura) {
		this.idFactura = idFactura;
	}

	public String getNoSiniestro() {
		return noSiniestro;
	}

	public void setNoSiniestro(String noSiniestro) {
		this.noSiniestro = noSiniestro;
	}

	public Long getIdConjunto() {
		return idConjunto;
	}

	public void setIdConjunto(Long idConjunto) {
		this.idConjunto = idConjunto;
	}

	public BigDecimal getIva() {
		return iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public BigDecimal getIvaRetenido() {
		return ivaRetenido;
	}

	public void setIvaRetenido(BigDecimal ivaRetenido) {
		this.ivaRetenido = ivaRetenido;
	}

	public BigDecimal getIsr() {
		return isr;
	}

	public void setIsr(BigDecimal isr) {
		this.isr = isr;
	}

	public BigDecimal getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}

	public BigDecimal getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
}