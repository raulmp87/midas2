package mx.com.afirme.midas2.dto.siniestros.seguimiento;

import java.io.Serializable;


/**
 * Objeto para transportar los datos de indentificaci�n del siniestro.
 * @author Arturo
 * @version 1.0
 * @created 13-oct-2014 11:30:31 a.m.
 */
public class SeguimientoSiniestroId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9004497985917015227L;
	private Integer numeroInciso;
	private String numeroReporte;
	private String numeroSiniestro;
	private String poliza;

	public SeguimientoSiniestroId(Integer numeroInciso,String numeroReporte,String numeroSiniestro,String poliza){
		this.numeroInciso=numeroInciso;
		this.numeroReporte=numeroReporte;
		this.numeroSiniestro=numeroSiniestro;
		this.poliza=poliza;
	}

	public SeguimientoSiniestroId(){

	}
	
	public void finalize() throws Throwable {

	}

	public Integer getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public String getNumeroReporte() {
		return numeroReporte;
	}

	public void setNumeroReporte(String numeroReporte) {
		this.numeroReporte = numeroReporte;
	}

	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}

	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}

	public String getPoliza() {
		return poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	

}