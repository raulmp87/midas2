package mx.com.afirme.midas2.dto.siniestros.seguimiento;

import java.io.Serializable;


/**
 * Objeto para transportar el Seguimiento Jur�dico.
 * @author Arturo
 * @version 1.0
 * @created 13-oct-2014 11:45:41 a.m.
 */
public class SeguimientoJuridicoDTO extends SeguimientoSiniestroDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5117532415982594681L;
	private String fechaAdmision;
	private String fechaConclusion;
	private String motivoTurno;
	private String noExpediente;
	private String nombreAbogado;
	private String situacion;
	private int vehiculoDetenido;
	public String getFechaAdmision() {
		return fechaAdmision;
	}
	public void setFechaAdmision(String fechaAdmision) {
		this.fechaAdmision = fechaAdmision;
	}
	public String getFechaConclusion() {
		return fechaConclusion;
	}
	public void setFechaConclusion(String fechaConclusion) {
		this.fechaConclusion = fechaConclusion;
	}
	public String getMotivoTurno() {
		return motivoTurno;
	}
	public void setMotivoTurno(String motivoTurno) {
		this.motivoTurno = motivoTurno;
	}
	public String getNoExpediente() {
		return noExpediente;
	}
	public void setNoExpediente(String noExpediente) {
		this.noExpediente = noExpediente;
	}
	public String getNombreAbogado() {
		return nombreAbogado;
	}
	public void setNombreAbogado(String nombreAbogado) {
		this.nombreAbogado = nombreAbogado;
	}
	public String getSituacion() {
		return situacion;
	}
	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}
	public int getVehiculoDetenido() {
		return vehiculoDetenido;
	}
	public void setVehiculoDetenido(int vehiculoDetenido) {
		this.vehiculoDetenido = vehiculoDetenido;
	}


}