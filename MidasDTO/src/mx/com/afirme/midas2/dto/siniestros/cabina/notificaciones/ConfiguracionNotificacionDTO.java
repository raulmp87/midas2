package mx.com.afirme.midas2.dto.siniestros.cabina.notificaciones;

import java.util.ArrayList;
import java.util.List;

public class ConfiguracionNotificacionDTO {
	private Long id;
	private List<String> oficinasId = new ArrayList<String>();
	private Long oficinaId;
	private Long procesoCabinaId;
	private Long movimientoId;
	private String notas;
	private String modoEnvioId;
	private String tipoDestinatarioId;
	private String correoEnvio;
	private String puesto;
	private String nombre;
	private String correo;
	private String codigo;
	private Integer agenteId;
	private String numPoliza;
	private String numInciso;
	private Boolean esBusquedaPorPantalla;
	

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getOficinaId() {
		return oficinaId;
	}
	public void setOficinaId(Long oficinaId) {
		this.oficinaId = oficinaId;
	}
	public Long getProcesoCabinaId() {
		return procesoCabinaId;
	}
	public void setProcesoCabinaId(Long procesoCabinaId) {
		this.procesoCabinaId = procesoCabinaId;
	}
	public Long getMovimientoId() {
		return movimientoId;
	}
	public void setMovimientoId(Long movimientoId) {
		this.movimientoId = movimientoId;
	}
	public String getNotas() {
		return notas;
	}
	public void setNotas(String notas) {
		this.notas = notas;
	}
	public String getModoEnvioId() {
		return modoEnvioId;
	}
	public void setModoEnvioId(String modoEnvioId) {
		this.modoEnvioId = modoEnvioId;
	}
	public String getTipoDestinatarioId() {
		return tipoDestinatarioId;
	}
	public void setTipoDestinatarioId(String tipoDestinatarioId) {
		this.tipoDestinatarioId = tipoDestinatarioId;
	}
	public String getCorreoEnvio() {
		return correoEnvio;
	}
	public void setCorreoEnvio(String correoEnvio) {
		this.correoEnvio = correoEnvio;
	}
	public String getPuesto() {
		return puesto;
	}
	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public Integer getAgenteId() {
		return agenteId;
	}
	public void setAgenteId(Integer agenteId) {
		this.agenteId = agenteId;
	}
	public String getNumPoliza() {
		return numPoliza;
	}
	public void setNumPoliza(String numPoliza) {
		this.numPoliza = numPoliza;
	}
	public String getNumInciso() {
		return numInciso;
	}
	public void setNumInciso(String numInciso) {
		this.numInciso = numInciso;
	}
	public List<String> getOficinasId() {
		return oficinasId;
	}
	public void setOficinasId(List<String> oficinasId) {
		this.oficinasId = oficinasId;
	}	
	public Boolean getEsBusquedaPorPantalla() {
		return esBusquedaPorPantalla;
	}
	public void setEsBusquedaPorPantalla(Boolean esBusquedaPorPantalla) {
		this.esBusquedaPorPantalla = esBusquedaPorPantalla;
	}
}
