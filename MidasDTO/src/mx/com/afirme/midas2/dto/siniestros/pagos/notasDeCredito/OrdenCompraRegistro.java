package mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Israel
 * @version 1.0
 * @created 31-mar.-2015 12:43:44 p. m.
 */
public class OrdenCompraRegistro implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1262887491141914098L;
	private String concepto;
	private Date fechaRegistro;
	private BigDecimal isr;
	private BigDecimal iva;
	private BigDecimal ivaRetenido;
	private Long ordenCompraId;
	private Long ordenPago;
	private String siniestro;
	private BigDecimal subTotal;
	private BigDecimal total;
	private String afectado;

	public OrdenCompraRegistro(){

	}

	public void finalize() throws Throwable {

	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public BigDecimal getIsr() {
		return isr;
	}

	public void setIsr(BigDecimal isr) {
		this.isr = isr;
	}

	public BigDecimal getIva() {
		return iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public BigDecimal getIvaRetenido() {
		return ivaRetenido;
	}

	public void setIvaRetenido(BigDecimal ivaRetenido) {
		this.ivaRetenido = ivaRetenido;
	}

	public Long getOrdenCompraId() {
		return ordenCompraId;
	}

	public void setOrdenCompraId(Long ordenCompraId) {
		this.ordenCompraId = ordenCompraId;
	}

	public Long getOrdenPago() {
		return ordenPago;
	}

	public void setOrdenPago(Long ordenPago) {
		this.ordenPago = ordenPago;
	}

	public String getSiniestro() {
		return siniestro;
	}

	public void setSiniestro(String siniestro) {
		this.siniestro = siniestro;
	}

	public BigDecimal getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public String getAfectado() {
		return afectado;
	}

	public void setAfectado(String afectado) {
		this.afectado = afectado;
	}

}