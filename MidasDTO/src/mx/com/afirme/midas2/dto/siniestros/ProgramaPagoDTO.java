package mx.com.afirme.midas2.dto.siniestros;

public class ProgramaPagoDTO {
	
	private String documentoOrigen;
	private Long idCotizacionSeycos;
	private Long numeroPrograma;
	private Integer numeroRecibos;
	private String tipoEndoso;
	
	/**
	 * @return the documentoOrigen
	 */
	public String getDocumentoOrigen() {
		return documentoOrigen;
	}
	/**
	 * @param documentoOrigen the documentoOrigen to set
	 */
	public void setDocumentoOrigen(String documentoOrigen) {
		this.documentoOrigen = documentoOrigen;
	}
	/**
	 * @return the idCotizacionSeycos
	 */
	public Long getIdCotizacionSeycos() {
		return idCotizacionSeycos;
	}
	/**
	 * @param idCotizacionSeycos the idCotizacionSeycos to set
	 */
	public void setIdCotizacionSeycos(Long idCotizacionSeycos) {
		this.idCotizacionSeycos = idCotizacionSeycos;
	}
	/**
	 * @return the numeroPrograma
	 */
	public Long getNumeroPrograma() {
		return numeroPrograma;
	}
	/**
	 * @param numeroPrograma the numeroPrograma to set
	 */
	public void setNumeroPrograma(Long numeroPrograma) {
		this.numeroPrograma = numeroPrograma;
	}
	/**
	 * @return the numeroRecibos
	 */
	public Integer getNumeroRecibos() {
		return numeroRecibos;
	}
	/**
	 * @param numeroRecibos the numeroRecibos to set
	 */
	public void setNumeroRecibos(Integer numeroRecibos) {
		this.numeroRecibos = numeroRecibos;
	}
	/**
	 * @return the tipoEndoso
	 */
	public String getTipoEndoso() {
		return tipoEndoso;
	}
	/**
	 * @param tipoEndoso the tipoEndoso to set
	 */
	public void setTipoEndoso(String tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}
	
}
