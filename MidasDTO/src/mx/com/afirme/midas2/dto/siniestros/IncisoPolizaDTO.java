package mx.com.afirme.midas2.dto.siniestros;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;


@XmlSeeAlso({IncisoPolizaDTO.class})
@XmlAccessorType(XmlAccessType.FIELD)

public class IncisoPolizaDTO implements Serializable{
	
	private Long continuityId;
	private String numeroPoliza;
	private String numPolizaSeycos;
	private Integer numeroInciso;
	private Date validoDesde; 
	private Date validoHasta; 
	private String nombreContratante;
	private String numeroSerie;
	private String marca;
	private String tipoVehiculo;
	private String placa;
	private Short modeloVehiculo;
	private Short estatusInciso; //0 Vigente, 1 No Vigente, 2 Cancelado
	

	public IncisoPolizaDTO(){}
	/**
	 * 
	 */
	private static final long serialVersionUID = 4205350012340632733L;

	public IncisoPolizaDTO(Long id, String numeroPoliza, String numPolizaSeycos, Integer numeroInciso, Date validoDesde, Date validoHasta, 
			String nombreContratante, String numeroSerie, String marca, String tipoVehiculo, String placa, Short modeloVehiculo, Object estatusInciso ) {
		super();
		this.continuityId = id;
		this.numeroPoliza = numeroPoliza;
		this.numPolizaSeycos= numPolizaSeycos;
		this.numeroInciso = numeroInciso;
		this.validoDesde = validoDesde;
		this.validoHasta = validoHasta;
		this.nombreContratante = nombreContratante;
		this.numeroSerie = numeroSerie;
		this.marca = marca;
		this.tipoVehiculo = tipoVehiculo;
		this.placa = placa;
		this.modeloVehiculo = modeloVehiculo;	
		this.estatusInciso = estatusInciso != null ? Short.valueOf(String.valueOf(estatusInciso)) : null;
		
	}

	public Long getContinuityId() {
		return continuityId;
	}

	public void setContinuityId(Long continuityId) {
		this.continuityId = continuityId;
	}

	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	public String getNumPolizaSeycos() {
		return numPolizaSeycos;
	}

	public void setNumPolizaSeycos(String numPolizaSeycos) {
		this.numPolizaSeycos = numPolizaSeycos;
	}

	public Integer getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public Date getValidoDesde() {
		return validoDesde;
	}

	public void setValidoDesde(Date validoDesde) {
		this.validoDesde = validoDesde;
	}

	public Date getValidoHasta() {
		return validoHasta;
	}

	public void setValidoHasta(Date validoHasta) {
		this.validoHasta = validoHasta;
	}

	public String getNombreContratante() {
		return nombreContratante;
	}

	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}

	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getTipoVehiculo() {
		return tipoVehiculo;
	}

	public void setTipoVehiculo(String tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Short getModeloVehiculo() {
		return modeloVehiculo;
	}

	public void setModeloVehiculo(Short modeloVehiculo) {
		this.modeloVehiculo = modeloVehiculo;
	}

	public Short getEstatusInciso() {
		return estatusInciso;
	}

	public void setEstatusInciso(Short estatusInciso) {
		this.estatusInciso = estatusInciso;
	}

}
