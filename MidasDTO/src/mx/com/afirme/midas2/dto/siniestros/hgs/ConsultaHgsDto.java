package mx.com.afirme.midas2.dto.siniestros.hgs;

import java.io.Serializable;

public class ConsultaHgsDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long idReporteSin;
	private String numFolio;
	private Double sumaAsegurada;
	private String aplicaDeducible;
	private Double porcentajeDeducible;

	public Long getIdReporteSin() {
		return idReporteSin;
	}

	public void setIdReporteSin(Long idReporteSin) {
		this.idReporteSin = idReporteSin;
	}

	public String getNumFolio() {
		return numFolio;
	}

	public void setNumFolio(String numFolio) {
		this.numFolio = numFolio;
	}

	public Double getSumaAsegurada() {
		return sumaAsegurada;
	}

	public void setSumaAsegurada(Double sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}

	public String getAplicaDeducible() {
		return aplicaDeducible;
	}

	public void setAplicaDeducible(String aplicaDeducible) {
		this.aplicaDeducible = aplicaDeducible;
	}

	public Double getPorcentajeDeducible() {
		return porcentajeDeducible;
	}

	public void setPorcentajeDeducible(Double porcentajeDeducible) {
		this.porcentajeDeducible = porcentajeDeducible;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idReporteSin == null) ? 0 : idReporteSin.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		return false;
	}

	@Override
	public String toString() {
		return "ConsultaHgsDto [idReporteSin=" + idReporteSin + ", numFolio="
				+ numFolio + ", sumaAsegurada=" + sumaAsegurada
				+ ", aplicaDeducible=" + aplicaDeducible
				+ ", porcentajeDeducible=" + porcentajeDeducible + "]";
	}
	
}
