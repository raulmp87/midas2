package mx.com.afirme.midas2.dto.siniestros;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DefinicionSiniestroRoboDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -158541178843484789L;
	
	
	//Datos Vehiculo	
	private String	numSerie	;
	private String	numSerieOcra	;
	private String	numMotor	;
	private String	marca	;
	private String	tipoVehiculo	;
	private String	modelo	;
	private String	placas	;
	private String	color	;
		//Asegurado	;
	private String	nombreAsegurado	;
	private String	ladaAsegurado	;
	private String	telASegurado	;
	private String	nombreContacto	;
	private String	ladaContacto	;
	private String	mailContacto	;
	private String	extContacto	;
	private String	telContacto	;
		//Fechas	;
	private Date	vigenciaPolizaIni	;
	private Date	vigenciaPolizaFin	;
	private Date	fechaRobo	;
	private Date	fechaReporte	;
	private Date	fechaCapOCRA	;
	private Date	fechaAveriguacion	;
	private Date	fechaLocalizacion	;
	private Date	fechaRecuperacion	;
	private int	 diasLocalizacion	;
	private int	 diasRecuperacion	;
		//Datos Ajustador	
	private String	ajustador	;
	private String	agente	;
	
	//Localidad	
	private String	pais	;
	private String	estado	;
	private String	municipio	;
	private String	colonia	;
	private String	calle	;
	private String	referencia	;
	private String	cp	;
	private String kilometro;
	private String tipoCarretera;
	private String nombreCarretera;
	
	
	private String	tipoRobo	;
	private String	averiguacionLocal	;
	private String	averiguacionDen	;
	private Boolean	isTurnarInvetigacion	;
	private String	turnarInvetigacion	;
		//Seguimiento Robo	
	private String	estatusVehiculo	;
	private String	localizador	;
	private String	recuperador	;
	private String	proveedor	;
	private String	numActa	;
	private String	ubicacionVehiculo	;
	private String	causaMovimiento	;
	private String	observaciones	;
		//Montos Recuperacion	
	private BigDecimal	valorComercialMomento	;
	private BigDecimal	presupuestoDanos	;
	private String	faltantes	;
	private BigDecimal	montoProvicion	;
	private BigDecimal	importePagado	;
	
	private Long reporteCabinaid ;
	private Long coberturaId ;
	private Long reporteRoboId;
	private Long estimacionId;
	
	//coberturas
	private String coberturaDes;
	private BigDecimal sumaAsegurada;
	private BigDecimal valorComercial;
	private BigDecimal porcentajeDeducible;
	private BigDecimal montoDeducible;
	private Boolean	aplicaDeducible = Boolean.FALSE;

	private BigDecimal reserva;

	
	//datos condicion especiales
	private Date fechaOcurrido;
	private long fechaOcurridoMillis;
	private Long incisoContinuityId;
	private BigDecimal idToPoliza;
	
	
	private String noSiniestro;
	private String noPoliza;
	private String noReporte;
	private Integer numeroInciso;
	private boolean envioOcra;
	
	private String nombreAgenteMinisterioPublico;



	
	

	public String getNombreAgenteMinisterioPublico() {
		return nombreAgenteMinisterioPublico;
	}
	public void setNombreAgenteMinisterioPublico(
			String nombreAgenteMinisterioPublico) {
		this.nombreAgenteMinisterioPublico = nombreAgenteMinisterioPublico;
	}
	public String getNumSerieOcra() {
		return numSerieOcra;
	}
	public void setNumSerieOcra(String numSerieOcra) {
		this.numSerieOcra = numSerieOcra;
	}
	public String getNoSiniestro() {
		return noSiniestro;
	}
	public void setNoSiniestro(String noSiniestro) {
		this.noSiniestro = noSiniestro;
	}
	public String getNoPoliza() {
		return noPoliza;
	}
	public void setNoPoliza(String noPoliza) {
		this.noPoliza = noPoliza;
	}
	public String getNoReporte() {
		return noReporte;
	}
	public void setNoReporte(String noReporte) {
		this.noReporte = noReporte;
	}
	public boolean getEnvioOcra() {
		return envioOcra;
	}
	public void setEnvioOcra(boolean envioOcra) {
		this.envioOcra = envioOcra;
	}

	private List<AfectacionCoberturaSiniestroDTO> listadoAfectacionCoberturas= new ArrayList<AfectacionCoberturaSiniestroDTO>();	
	

	
	public List<AfectacionCoberturaSiniestroDTO> getListadoAfectacionCoberturas() {
		return listadoAfectacionCoberturas;
	}
	public void setListadoAfectacionCoberturas(
			List<AfectacionCoberturaSiniestroDTO> listadoAfectacionCoberturas) {
		this.listadoAfectacionCoberturas = listadoAfectacionCoberturas;
	}
	public Long getReporteCabinaid() {
		return reporteCabinaid;
	}
	public void setReporteCabinaid(Long reporteCabinaid) {
		this.reporteCabinaid = reporteCabinaid;
	}
	public Long getCoberturaId() {
		return coberturaId;
	}
	public void setCoberturaId(Long coberturaId) {
		this.coberturaId = coberturaId;
	}
	public String getNumSerie() {
		return numSerie;
	}
	public void setNumSerie(String numSerie) {
		this.numSerie = numSerie;
	}
	public String getNumMotor() {
		return numMotor;
	}
	public void setNumMotor(String numMotor) {
		this.numMotor = numMotor;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getTipoVehiculo() {
		return tipoVehiculo;
	}
	public void setTipoVehiculo(String tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getPlacas() {
		return placas;
	}
	public void setPlacas(String placas) {
		this.placas = placas;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}
	public String getLadaAsegurado() {
		return ladaAsegurado;
	}
	public void setLadaAsegurado(String ladaAsegurado) {
		this.ladaAsegurado = ladaAsegurado;
	}
	public String getTelASegurado() {
		return telASegurado;
	}
	public void setTelASegurado(String telASegurado) {
		this.telASegurado = telASegurado;
	}
	public String getNombreContacto() {
		return nombreContacto;
	}
	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}
	public String getLadaContacto() {
		return ladaContacto;
	}
	public void setLadaContacto(String ladaContacto) {
		this.ladaContacto = ladaContacto;
	}
	public String getTelContacto() {
		return telContacto;
	}
	public void setTelContacto(String telContacto) {
		this.telContacto = telContacto;
	}
	public Date getVigenciaPolizaIni() {
		return vigenciaPolizaIni;
	}
	public void setVigenciaPolizaIni(Date vigenciaPolizaIni) {
		this.vigenciaPolizaIni = vigenciaPolizaIni;
	}
	public Date getVigenciaPolizaFin() {
		return vigenciaPolizaFin;
	}
	public void setVigenciaPolizaFin(Date vigenciaPolizaFin) {
		this.vigenciaPolizaFin = vigenciaPolizaFin;
	}
	public Date getFechaRobo() {
		return fechaRobo;
	}
	public void setFechaRobo(Date fechaRobo) {
		this.fechaRobo = fechaRobo;
	}
	public Date getFechaReporte() {
		return fechaReporte;
	}
	public void setFechaReporte(Date fechaReporte) {
		this.fechaReporte = fechaReporte;
	}
	public Date getFechaCapOCRA() {
		return fechaCapOCRA;
	}
	public void setFechaCapOCRA(Date fechaCapOCRA) {
		this.fechaCapOCRA = fechaCapOCRA;
	}
	public Date getFechaAveriguacion() {
		return fechaAveriguacion;
	}
	public void setFechaAveriguacion(Date fechaAveriguacion) {
		this.fechaAveriguacion = fechaAveriguacion;
	}
	public Date getFechaLocalizacion() {
		return fechaLocalizacion;
	}
	public void setFechaLocalizacion(Date fechaLocalizacion) {
		this.fechaLocalizacion = fechaLocalizacion;
	}
	public Date getFechaRecuperacion() {
		return fechaRecuperacion;
	}
	public void setFechaRecuperacion(Date fechaRecuperacion) {
		this.fechaRecuperacion = fechaRecuperacion;
	}
	public int getDiasLocalizacion() {
		return diasLocalizacion;
	}
	public void setDiasLocalizacion(int diasLocalizacion) {
		this.diasLocalizacion = diasLocalizacion;
	}
	public int getDiasRecuperacion() {
		return diasRecuperacion;
	}
	public void setDiasRecuperacion(int diasRecuperacion) {
		this.diasRecuperacion = diasRecuperacion;
	}
	public String getAjustador() {
		return ajustador;
	}
	public void setAjustador(String ajustador) {
		this.ajustador = ajustador;
	}
	public String getAgente() {
		return agente;
	}
	public void setAgente(String agente) {
		this.agente = agente;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getColonia() {
		return colonia;
	}
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getCp() {
		return cp;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}
	public String getTipoRobo() {
		return tipoRobo;
	}
	public void setTipoRobo(String tipoRobo) {
		this.tipoRobo = tipoRobo;
	}
	public String getAveriguacionLocal() {
		return averiguacionLocal;
	}
	public void setAveriguacionLocal(String averiguacionLocal) {
		this.averiguacionLocal = averiguacionLocal;
	}
	public String getAveriguacionDen() {
		return averiguacionDen;
	}
	public void setAveriguacionDen(String averiguacionDen) {
		this.averiguacionDen = averiguacionDen;
	}
	public Boolean getIsTurnarInvetigacion() {
		return isTurnarInvetigacion;
	}
	public void setIsTurnarInvetigacion(Boolean isTurnarInvetigacion) {
		this.isTurnarInvetigacion = isTurnarInvetigacion;
	}
	public String getTurnarInvetigacion() {
		return turnarInvetigacion;
	}
	public void setTurnarInvetigacion(String turnarInvetigacion) {
		this.turnarInvetigacion = turnarInvetigacion;
	}
	public String getEstatusVehiculo() {
		return estatusVehiculo;
	}
	public void setEstatusVehiculo(String estatusVehiculo) {
		this.estatusVehiculo = estatusVehiculo;
	}
	public String getLocalizador() {
		return localizador;
	}
	public void setLocalizador(String localizador) {
		this.localizador = localizador;
	}
	public String getRecuperador() {
		return recuperador;
	}
	public void setRecuperador(String recuperador) {
		this.recuperador = recuperador;
	}
	public String getProveedor() {
		return proveedor;
	}
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}
	public String getNumActa() {
		return numActa;
	}
	public void setNumActa(String numActa) {
		this.numActa = numActa;
	}
	public String getUbicacionVehiculo() {
		return ubicacionVehiculo;
	}
	public void setUbicacionVehiculo(String ubicacionVehiculo) {
		this.ubicacionVehiculo = ubicacionVehiculo;
	}
	public String getCausaMovimiento() {
		return causaMovimiento;
	}
	public void setCausaMovimiento(String causaMovimiento) {
		this.causaMovimiento = causaMovimiento;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	
	public String getCoberturaDes() {
		return coberturaDes;
	}
	public void setCoberturaDes(String coberturaDes) {
		this.coberturaDes = coberturaDes;
	}
	
	public Date getFechaOcurrido() {
		return fechaOcurrido;
	}
	public void setFechaOcurrido(Date fechaOcurrido) {
		this.fechaOcurrido = fechaOcurrido;
	}

	public Long getIncisoContinuityId() {
		return incisoContinuityId;
	}
	public void setIncisoContinuityId(Long incisoContinuityId) {
		this.incisoContinuityId = incisoContinuityId;
	}
	
	public long getFechaOcurridoMillis() {
		return fechaOcurridoMillis;
	}
	public void setFechaOcurridoMillis(long fechaOcurridoMillis) {
		this.fechaOcurridoMillis = fechaOcurridoMillis;
	}
	
	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}
	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}
	public Long getReporteRoboId() {
		return reporteRoboId;
	}
	public void setReporteRoboId(Long reporteRoboId) {
		this.reporteRoboId = reporteRoboId;
	}
	public String getFaltantes() {
		return faltantes;
	}
	public void setFaltantes(String faltantes) {
		this.faltantes = faltantes;
	}
	public BigDecimal getValorComercialMomento() {
		return valorComercialMomento;
	}
	public void setValorComercialMomento(BigDecimal valorComercialMomento) {
		this.valorComercialMomento = valorComercialMomento;
	}
	public BigDecimal getPresupuestoDanos() {
		return presupuestoDanos;
	}
	public void setPresupuestoDanos(BigDecimal presupuestoDanos) {
		this.presupuestoDanos = presupuestoDanos;
	}
	public BigDecimal getMontoProvicion() {
		return montoProvicion;
	}
	public void setMontoProvicion(BigDecimal montoProvicion) {
		this.montoProvicion = montoProvicion;
	}
	public BigDecimal getImportePagado() {
		return importePagado;
	}
	public void setImportePagado(BigDecimal importePagado) {
		this.importePagado = importePagado;
	}
	public BigDecimal getSumaAsegurada() {
		return sumaAsegurada;
	}
	public void setSumaAsegurada(BigDecimal sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
	public BigDecimal getValorComercial() {
		return valorComercial;
	}
	public void setValorComercial(BigDecimal valorComercial) {
		this.valorComercial = valorComercial;
	}
	public BigDecimal getPorcentajeDeducible() {
		return porcentajeDeducible;
	}
	public void setPorcentajeDeducible(BigDecimal porcentajeDeducible) {
		this.porcentajeDeducible = porcentajeDeducible;
	}
	public BigDecimal getMontoDeducible() {
		return montoDeducible;
	}
	public void setMontoDeducible(BigDecimal montoDeducible) {
		this.montoDeducible = montoDeducible;
	}
	public BigDecimal getReserva() {
		return reserva;
	}
	public void setReserva(BigDecimal reserva) {
		this.reserva = reserva;
	}
	public String getMailContacto() {
		return mailContacto;
	}
	public void setMailContacto(String mailContacto) {
		this.mailContacto = mailContacto;
	}
	public String getExtContacto() {
		return extContacto;
	}
	public void setExtContacto(String extContacto) {
		this.extContacto = extContacto;
	}
	public String getKilometro() {
		return kilometro;
	}
	public void setKilometro(String kilometro) {
		this.kilometro = kilometro;
	}
	public String getTipoCarretera() {
		return tipoCarretera;
	}
	public void setTipoCarretera(String tipoCarretera) {
		this.tipoCarretera = tipoCarretera;
	}
	public String getNombreCarretera() {
		return nombreCarretera;
	}
	public void setNombreCarretera(String nombreCarretera) {
		this.nombreCarretera = nombreCarretera;
	}
	public Integer getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public Long getEstimacionId() {
		return estimacionId;
	}
	public void setEstimacionId(Long estimacionId) {
		this.estimacionId = estimacionId;
	}
	public Boolean getAplicaDeducible() {
		return aplicaDeducible;
	}
	public void setAplicaDeducible(Boolean aplicaDeducible) {
		this.aplicaDeducible = aplicaDeducible;
	}
	
	
	
	

}