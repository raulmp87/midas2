package mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

@Component
public class SiniestroCabinaDTO {

	private Long 	id;
	private String 	causaSiniestro;
	@NotNull(message="{com.afirme.midas2.requerido}")
	private String 	celular;
	private String 	ciaSeguros;
	private String 	color;
	private String 	condMismoAsegurado;
	@NotNull(message="{com.afirme.midas2.requerido}")
	private String 	conductor;
	private String 	conductorHabitual;
	private String 	curp;
	private String 	descripcion;
	@NotNull(message="{com.afirme.midas2.requerido}")
	private Integer edad;
	private String 	estatus;
	private Date	finVigencia;
	private String	fugoTerceroResponsable;
	private String 	genero;
	private BigDecimal	importeEstimado;
	private BigDecimal	importeReserva;
	private Date 	iniVigencia;
	@NotNull(message="{com.afirme.midas2.requerido}")
	private String	ladaCelular;
	@NotNull(message="{com.afirme.midas2.requerido}")
	private String	ladaTelefono;
	private String	lineaNegocio;
	private String	motivoSituacion;
	private String 	numeroInciso;
	private String	numeroReporte;
	private String	numeroSiniestro;
	private Integer	numOcupantes;
	private Long numValuacion;
	private String	placas;
	private String 	poliza;
	private BigDecimal 	idToPoliza;
	private String	recibidaOrdenCia;
	private String 	rfc;
	private String	solValuacion;
	@NotNull(message="{com.afirme.midas2.requerido}")
	private String 	telefono;
	private String	terminoAjuste;
	private String	terminoSiniestro;
	private String 	tipoLicencia;
	private String 	tipoPerdida;
	private String 	tipoResponsabilidad;
	private String	unidadEquipoPesado;
	private String	verificado;
	private Boolean	esSiniestro;
	private Long	reporteCabinaId;
	private Boolean	cargoDetalle = false;
	private Date	fechaOcurrido;
	private Long	incisoContinuity;
	private Long	fechaOcurridoMillis;
	private Boolean noAplicaDepuracion;
	private Boolean enviosPendientesHGSOCRA;
	private String tipoCotizacion;
	private String rfcAsegurado;
	private String curpAsegurado;
	private String estatusValuacion;
	private BigDecimal montoValuacion;
	private BigDecimal montoRecuperado;
	private Integer banco;
	private String numAprobacion;
	private String numDeposito;
	private Date fechaDeposito;
	private Long recuperacionId;
	private String tipoRecuperacion;
	private String polizaCia;
	private String siniestroCia;
	private Integer porcentajeParticipacion;
	private Integer estatusReporte;
	private String motivoRechazo;
	private BigDecimal montoDanos;
	private String rechazoDesistimiento;
	private String observacionRechazo;
	private Boolean tieneCondicionesEspeciales = Boolean.FALSE;
	private Date fechaVigIniRealIncSiniestro;
	private String origen;
	private String incisoCia;
	private Date fechaExpedicion;
	private Date fechaDictamen;
	private Date fechaJuicio;
	private Long claveAmisSupervisionCampo;

	public Date getFechaVigIniRealIncSiniestro() {
		return fechaVigIniRealIncSiniestro;
	}
	public void setFechaVigIniRealIncSiniestro(Date fechaVigIniRealIncSiniestro) {
		this.fechaVigIniRealIncSiniestro = fechaVigIniRealIncSiniestro;
	}
	public String getCausaSiniestro() {
		return causaSiniestro;
	}
	public void setCausaSiniestro(String causaSiniestro) {
		this.causaSiniestro = causaSiniestro;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getCiaSeguros() {
		return ciaSeguros;
	}
	public void setCiaSeguros(String ciaSeguros) {
		this.ciaSeguros = ciaSeguros;
	}
	public String getCondMismoAsegurado() {
		return condMismoAsegurado;
	}
	public void setCondMismoAsegurado(String condMismoAsegurado) {
		this.condMismoAsegurado = condMismoAsegurado;
	}
	public String getConductor() {
		return conductor;
	}
	public void setConductor(String conductor) {
		this.conductor = conductor;
	}
	public String getConductorHabitual() {
		return conductorHabitual;
	}
	public void setConductorHabitual(String conductorHabitual) {
		this.conductorHabitual = conductorHabitual;
	}
	public String getCurp() {
		return curp;
	}
	public void setCurp(String curp) {
		this.curp = curp;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Integer getEdad() {
		return edad;
	}
	public void setEdad(Integer edad) {
		this.edad = edad;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public Date getFinVigencia() {
		return finVigencia;
	}
	public void setFinVigencia(Date finVigencia) {
		this.finVigencia = finVigencia;
	}
	public String getFugoTerceroResponsable() {
		return fugoTerceroResponsable;
	}
	public void setFugoTerceroResponsable(String fugoTerceroResponsable) {
		this.fugoTerceroResponsable = fugoTerceroResponsable;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public BigDecimal getImporteEstimado() {
		return importeEstimado;
	}
	public void setImporteEstimado(BigDecimal importeEstimado) {
		this.importeEstimado = importeEstimado;
	}
	public BigDecimal getImporteReserva() {
		return importeReserva;
	}
	public void setImporteReserva(BigDecimal importeReserva) {
		this.importeReserva = importeReserva;
	}
	public Date getIniVigencia() {
		return iniVigencia;
	}
	public void setIniVigencia(Date iniVigencia) {
		this.iniVigencia = iniVigencia;
	}
	public String getLadaCelular() {
		return ladaCelular;
	}
	public void setLadaCelular(String ladaCelular) {
		this.ladaCelular = ladaCelular;
	}
	public String getLadaTelefono() {
		return ladaTelefono;
	}
	public void setLadaTelefono(String ladaTelefono) {
		this.ladaTelefono = ladaTelefono;
	}
	public String getLineaNegocio() {
		return lineaNegocio;
	}
	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}
	public String getMotivoSituacion() {
		return motivoSituacion;
	}
	public void setMotivoSituacion(String motivoSituacion) {
		this.motivoSituacion = motivoSituacion;
	}
	public String getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public String getNumeroReporte() {
		return numeroReporte;
	}
	public void setNumeroReporte(String numeroReporte) {
		this.numeroReporte = numeroReporte;
	}
	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}
	public Integer getNumOcupantes() {
		return numOcupantes;
	}
	public void setNumOcupantes(Integer numOcupantes) {
		this.numOcupantes = numOcupantes;
	}
	public Long getNumValuacion() {
		return numValuacion;
	}
	public void setNumValuacion(Long numValuacion) {
		this.numValuacion = numValuacion;
	}
	public String getPlacas() {
		return placas;
	}
	public void setPlacas(String placas) {
		this.placas = placas;
	}
	public String getPoliza() {
		return poliza;
	}
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	public String getRecibidaOrdenCia() {
		return recibidaOrdenCia;
	}
	public void setRecibidaOrdenCia(String recibidaOrdenCia) {
		this.recibidaOrdenCia = recibidaOrdenCia;
	}
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getSolValuacion() {
		return solValuacion;
	}
	public void setSolValuacion(String solValuacion) {
		this.solValuacion = solValuacion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getTerminoAjuste() {
		return terminoAjuste;
	}
	public void setTerminoAjuste(String terminoAjuste) {
		this.terminoAjuste = terminoAjuste;
	}
	public String getTerminoSiniestro() {
		return terminoSiniestro;
	}
	public void setTerminoSiniestro(String terminoSiniestro) {
		this.terminoSiniestro = terminoSiniestro;
	}
	public String getTipoLicencia() {
		return tipoLicencia;
	}
	public void setTipoLicencia(String tipoLicencia) {
		this.tipoLicencia = tipoLicencia;
	}
	public String getTipoPerdida() {
		return tipoPerdida;
	}
	public void setTipoPerdida(String tipoPerdida) {
		this.tipoPerdida = tipoPerdida;
	}
	public String getTipoResponsabilidad() {
		return tipoResponsabilidad;
	}
	public void setTipoResponsabilidad(String tipoResponsabilidad) {
		this.tipoResponsabilidad = tipoResponsabilidad;
	}
	public String getUnidadEquipoPesado() {
		return unidadEquipoPesado;
	}
	public void setUnidadEquipoPesado(String unidadEquipoPesado) {
		this.unidadEquipoPesado = unidadEquipoPesado;
	}
	public String getVerificado() {
		return verificado;
	}
	public void setVerificado(String verificado) {
		this.verificado = verificado;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Boolean getEsSiniestro() {
		return esSiniestro;
	}
	public void setEsSiniestro(Boolean esSiniestro) {
		this.esSiniestro = esSiniestro;
	}
	public Long getReporteCabinaId() {
		return reporteCabinaId;
	}
	public void setReporteCabinaId(Long reporteCabinaId) {
		this.reporteCabinaId = reporteCabinaId;
	}
	public Boolean getCargoDetalle() {
		return cargoDetalle;
	}
	public void setCargoDetalle(Boolean cargoDetalle) {
		this.cargoDetalle = cargoDetalle;
	}
	public Date getFechaOcurrido() {
		return fechaOcurrido;
	}
	public void setFechaOcurrido(Date fechaOcurrido) {
		this.fechaOcurrido = fechaOcurrido;
	}
	public Long getIncisoContinuity() {
		return incisoContinuity;
	}
	public void setIncisoContinuity(Long incisoContinuity) {
		this.incisoContinuity = incisoContinuity;
	}
	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}
	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}
	public Long getFechaOcurridoMillis() {
		return fechaOcurridoMillis;
	}
	public void setFechaOcurridoMillis(Long fechaOcurridoMillis) {
		this.fechaOcurridoMillis = fechaOcurridoMillis;
	}
	public Boolean getNoAplicaDepuracion() {
		return noAplicaDepuracion;
	}
	public void setNoAplicaDepuracion(Boolean noAplicaDepuracion) {
		this.noAplicaDepuracion = noAplicaDepuracion;
	}
	public Boolean getEnviosPendientesHGSOCRA() {
		return enviosPendientesHGSOCRA;
	}
	public void setEnviosPendientesHGSOCRA(Boolean enviosPendientesHGSOCRA) {
		this.enviosPendientesHGSOCRA = enviosPendientesHGSOCRA;
	}
	public String getTipoCotizacion() {
		return tipoCotizacion;
	}
	public void setTipoCotizacion(String tipoCotizacion) {
		this.tipoCotizacion = tipoCotizacion;
	}
	
	public String getRfcAsegurado() {
		return rfcAsegurado;
	}
	public void setRfcAsegurado(String rfcAsegurado) {
		this.rfcAsegurado = rfcAsegurado;
	}
	public String getCurpAsegurado() {
		return curpAsegurado;
	}
	public void setCurpAsegurado(String curpAsegurado) {
		this.curpAsegurado = curpAsegurado;
	}
	public String getEstatusValuacion() {
		return estatusValuacion;
	}
	public void setEstatusValuacion(String estatusValuacion) {
		this.estatusValuacion = estatusValuacion;
	}
	public BigDecimal getMontoValuacion() {
		return montoValuacion;
	}
	public void setMontoValuacion(BigDecimal montoValuacion) {
		this.montoValuacion = montoValuacion;
	}
	public BigDecimal getMontoRecuperado() {
		return montoRecuperado;
	}
	public void setMontoRecuperado(BigDecimal montoRecuperado) {
		this.montoRecuperado = montoRecuperado;
	}
	public Integer getBanco() {
		return banco;
	}
	public void setBanco(Integer banco) {
		this.banco = banco;
	}
	public String getNumAprobacion() {
		return numAprobacion;
	}
	public void setNumAprobacion(String numAprobacion) {
		this.numAprobacion = numAprobacion;
	}
	public String getNumDeposito() {
		return numDeposito;
	}
	public void setNumDeposito(String numDeposito) {
		this.numDeposito = numDeposito;
	}
	public Date getFechaDeposito() {
		return fechaDeposito;
	}
	public void setFechaDeposito(Date fechaDeposito) {
		this.fechaDeposito = fechaDeposito;
	}
	public Long getRecuperacionId() {
		return recuperacionId;
	}
	public void setRecuperacionId(Long recuperacionId) {
		this.recuperacionId = recuperacionId;
	}
	public String getTipoRecuperacion() {
		return tipoRecuperacion;
	}
	public void setTipoRecuperacion(String tipoRecuperacion) {
		this.tipoRecuperacion = tipoRecuperacion;
	}
	public String getPolizaCia() {
		return polizaCia;
	}
	public void setPolizaCia(String polizaCia) {
		this.polizaCia = polizaCia;
	}
	public String getSiniestroCia() {
		return siniestroCia;
	}
	public void setSiniestroCia(String siniestroCia) {
		this.siniestroCia = siniestroCia;
	}
	public Integer getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}
	public void setPorcentajeParticipacion(Integer porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}
	public Integer getEstatusReporte() {
		return estatusReporte;
	}
	public void setEstatusReporte(Integer estatusReporte) {
		this.estatusReporte = estatusReporte;
	}
	public String getMotivoRechazo() {
		return motivoRechazo;
	}
	public void setMotivoRechazo(String motivoRechazo) {
		this.motivoRechazo = motivoRechazo;
	}
	public BigDecimal getMontoDanos() {
		return montoDanos;
	}
	public void setMontoDanos(BigDecimal montoDanos) {
		this.montoDanos = montoDanos;
	}
	public String getRechazoDesistimiento() {
		return rechazoDesistimiento;
	}
	public void setRechazoDesistimiento(String rechazoDesistimiento) {
		this.rechazoDesistimiento = rechazoDesistimiento;
	}
	public String getObservacionRechazo() {
		return observacionRechazo;
	}
	public void setObservacionRechazo(String observacionRechazo) {
		this.observacionRechazo = observacionRechazo;
	}
	public Boolean getTieneCondicionesEspeciales() {
		return tieneCondicionesEspeciales;
	}
	public void setTieneCondicionesEspeciales(Boolean tieneCondicionesEspeciales) {
		this.tieneCondicionesEspeciales = tieneCondicionesEspeciales;
	}	
	public String getOrigen() {
		return origen;
	}
	public void setOrigen(String origen) {
		this.origen = origen;
	}
	public String getIncisoCia() {
		return incisoCia;
	}
	public void setIncisoCia(String incisoCia) {
		this.incisoCia = incisoCia;
	}
	public Date getFechaExpedicion() {
		return fechaExpedicion;
	}
	public void setFechaExpedicion(Date fechaExpedicion) {
		this.fechaExpedicion = fechaExpedicion;
	}
	public Date getFechaDictamen() {
		return fechaDictamen;
	}
	public void setFechaDictamen(Date fechaDictamen) {
		this.fechaDictamen = fechaDictamen;
	}
	public Date getFechaJuicio() {
		return fechaJuicio;
	}
	public void setFechaJuicio(Date fechaJuicio) {
		this.fechaJuicio = fechaJuicio;
	}

	public Long getClaveAmisSupervisionCampo() {
		return claveAmisSupervisionCampo;
	}
	public void setClaveAmisSupervisionCampo(Long claveAmisSupervisionCampo) {
		this.claveAmisSupervisionCampo = claveAmisSupervisionCampo;
	}
}