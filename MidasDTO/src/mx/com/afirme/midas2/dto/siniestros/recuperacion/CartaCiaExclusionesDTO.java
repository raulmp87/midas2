package mx.com.afirme.midas2.dto.siniestros.recuperacion;

import java.math.BigDecimal;

public class CartaCiaExclusionesDTO {

	private String nombre;
	private String descripcion;
	private BigDecimal monto;
	
	public CartaCiaExclusionesDTO() {
		super();
	}
	public CartaCiaExclusionesDTO(String nombre, String descripcion,
			BigDecimal monto) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.monto = monto;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @return the monto
	 */
	public BigDecimal getMonto() {
		return monto;
	}
	/**
	 * @param monto the monto to set
	 */
	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}
	
}
