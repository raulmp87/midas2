package mx.com.afirme.midas2.dto.siniestros.seguimiento;

import java.io.Serializable;


/**
 * Objeto para transportar Datos de Persona para Asegurados, Valuadores,
 * Coordinadores, etc.
 * @author Arturo
 * @version 1.0
 * @created 13-oct-2014 11:34:20 a.m.
 */
public class SeguimientoDatosPersona implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5582972771557522641L;
	private String email;
	private String extension;
	private String nombre;
	private String telefono;

	public SeguimientoDatosPersona(){

	}

	public void finalize() throws Throwable {

	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

}