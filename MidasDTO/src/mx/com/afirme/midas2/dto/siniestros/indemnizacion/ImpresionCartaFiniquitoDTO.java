package mx.com.afirme.midas2.dto.siniestros.indemnizacion;

import java.io.Serializable;
import java.math.BigDecimal;

import mx.com.afirme.midas2.domain.siniestros.indemnizacion.CartaSiniestro;

public class ImpresionCartaFiniquitoDTO implements Serializable{

	private static final long serialVersionUID = 5220789027282832270L;

	private CartaSiniestro informacionFiniquito;
	private FiniquitoInformacionSiniestroDTO informacionAsegurado;
	private FiniquitoInformacionSiniestroDTO informacionTercero;
	private BigDecimal primasPendientesPago;
	private BigDecimal primasNoDevengadas;
	
	/**
	 * @return the informacionFiniquito
	 */
	public CartaSiniestro getInformacionFiniquito() {
		return informacionFiniquito;
	}
	/**
	 * @param informacionFiniquito the informacionFiniquito to set
	 */
	public void setInformacionFiniquito(CartaSiniestro informacionFiniquito) {
		this.informacionFiniquito = informacionFiniquito;
	}
	/**
	 * @return the informacionAsegurado
	 */
	public FiniquitoInformacionSiniestroDTO getInformacionAsegurado() {
		return informacionAsegurado;
	}
	/**
	 * @param informacionAsegurado the informacionAsegurado to set
	 */
	public void setInformacionAsegurado(
			FiniquitoInformacionSiniestroDTO informacionAsegurado) {
		this.informacionAsegurado = informacionAsegurado;
	}
	/**
	 * @return the informacionTercero
	 */
	public FiniquitoInformacionSiniestroDTO getInformacionTercero() {
		return informacionTercero;
	}
	/**
	 * @param informacionTercero the informacionTercero to set
	 */
	public void setInformacionTercero(
			FiniquitoInformacionSiniestroDTO informacionTercero) {
		this.informacionTercero = informacionTercero;
	}
	/**
	 * @return the primasPendientesPago
	 */
	public BigDecimal getPrimasPendientesPago() {
		return primasPendientesPago;
	}
	/**
	 * @param primasPendientesPago the primasPendientesPago to set
	 */
	public void setPrimasPendientesPago(BigDecimal primasPendientesPago) {
		this.primasPendientesPago = primasPendientesPago;
	}
	/**
	 * @return the primasNoDevengadas
	 */
	public BigDecimal getPrimasNoDevengadas() {
		return primasNoDevengadas;
	}
	/**
	 * @param primasNoDevengadas the primasNoDevengadas to set
	 */
	public void setPrimasNoDevengadas(BigDecimal primasNoDevengadas) {
		this.primasNoDevengadas = primasNoDevengadas;
	}
	
}