package mx.com.afirme.midas2.dto.siniestros.indemnizacion;

import java.io.Serializable;

public class FiniquitoInformacionSiniestroDTO implements Serializable{

	private static final long serialVersionUID = -183117135241800993L;
	
	private String 	numeroSiniestro;
	private String 	numeroPoliza;
	private Integer inciso;
	private String	marcaTipo;
	private Short 	modelo;
	private String 	color;
	private String 	numeroPlacas;
	private String 	numeroSerie;
	private String 	nombrePropietario;
	private String 	direccion;
	/**
	 * @return the numeroPoliza
	 */
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	/**
	 * @param numeroPoliza the numeroPoliza to set
	 */
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	/**
	 * @return the marcaTipo
	 */
	public String getMarcaTipo() {
		return marcaTipo;
	}
	/**
	 * @param marcaTipo the marcaTipo to set
	 */
	public void setMarcaTipo(String marcaTipo) {
		this.marcaTipo = marcaTipo;
	}
	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}
	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}
	/**
	 * @return the numeroPlacas
	 */
	public String getNumeroPlacas() {
		return numeroPlacas;
	}
	/**
	 * @param numeroPlacas the numeroPlacas to set
	 */
	public void setNumeroPlacas(String numeroPlacas) {
		this.numeroPlacas = numeroPlacas;
	}
	/**
	 * @return the numeroSerie
	 */
	public String getNumeroSerie() {
		return numeroSerie;
	}
	/**
	 * @param numeroSerie the numeroSerie to set
	 */
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	/**
	 * @return the nombrePropietario
	 */
	public String getNombrePropietario() {
		return nombrePropietario;
	}
	/**
	 * @param nombrePropietario the nombrePropietario to set
	 */
	public void setNombrePropietario(String nombrePropietario) {
		this.nombrePropietario = nombrePropietario;
	}
	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}
	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	/**
	 * @return the numeroSiniestro
	 */
	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}
	/**
	 * @param numeroSiniestro the numeroSiniestro to set
	 */
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}
	/**
	 * @return the inciso
	 */
	public Integer getInciso() {
		return inciso;
	}
	/**
	 * @param inciso the inciso to set
	 */
	public void setInciso(Integer inciso) {
		this.inciso = inciso;
	}
	/**
	 * @return the modelo
	 */
	public Short getModelo() {
		return modelo;
	}
	/**
	 * @param modelo the modelo to set
	 */
	public void setModelo(Short modelo) {
		this.modelo = modelo;
	}

}
