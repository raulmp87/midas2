package mx.com.afirme.midas2.dto.siniestros.valuacion;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

public class ValuacionReporteDTO implements Comparator<ValuacionReporteDTO>, Serializable{
	
	private static final long serialVersionUID = 2491112416785294308L;
	
	private Integer estatus;
	private String estatusDesc;
	private Date fechaValuacion;
	private String nombreAjustador;
	private String nombreValuador;
	private String numeroReporte;
	private String numeroSerie;
	private Long numeroValuacion;
	private Double totalReparacion;
	private Date fechaCreacion;

	
	/**
	 * @return the estatus
	 */
	public Integer getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}
	/**
	 * @return the estatusDesc
	 */
	public String getEstatusDesc() {
		return estatusDesc;
	}
	/**
	 * @param estatusDesc the estatusDesc to set
	 */
	public void setEstatusDesc(String estatusDesc) {
		this.estatusDesc = estatusDesc;
	}
	/**
	 * @return the fechaValuacion
	 */
	public Date getFechaValuacion() {
		return fechaValuacion;
	}
	/**
	 * @param fechaValuacion the fechaValuacion to set
	 */
	public void setFechaValuacion(Date fechaValuacion) {
		this.fechaValuacion = fechaValuacion;
	}
	/**
	 * @return the nombreAjustador
	 */
	public String getNombreAjustador() {
		return nombreAjustador;
	}
	/**
	 * @param nombreAjustador the nombreAjustador to set
	 */
	public void setNombreAjustador(String nombreAjustador) {
		this.nombreAjustador = nombreAjustador;
	}
	/**
	 * @return the nombreValuador
	 */
	public String getNombreValuador() {
		return nombreValuador;
	}
	/**
	 * @param nombreValuador the nombreValuador to set
	 */
	public void setNombreValuador(String nombreValuador) {
		this.nombreValuador = nombreValuador;
	}
	/**
	 * @return the numeroReporte
	 */
	public String getNumeroReporte() {
		return numeroReporte;
	}
	/**
	 * @param numeroReporte the numeroReporte to set
	 */
	public void setNumeroReporte(String numeroReporte) {
		this.numeroReporte = numeroReporte;
	}
	/**
	 * @return the numeroSerie
	 */
	public String getNumeroSerie() {
		return numeroSerie;
	}
	/**
	 * @param numeroSerie the numeroSerie to set
	 */
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	/**
	 * @return the numeroValuacion
	 */
	public Long getNumeroValuacion() {
		return numeroValuacion;
	}
	/**
	 * @param numeroValuacion the numeroValuacion to set
	 */
	public void setNumeroValuacion(Long numeroValuacion) {
		this.numeroValuacion = numeroValuacion;
	}	

	/**
	 * @return the totalReparacion
	 */
	public Double getTotalReparacion() {
		return totalReparacion;
	}
	
	/**
	 * @param totalReparacion the totalReparacion to set
	 */
	public void setTotalReparacion(Double totalReparacion) {
		this.totalReparacion = totalReparacion;
	}
	
	/**
	 * @return the fechaCreacion
	 */
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	/**
	 * @param fechaCreacion the fechaCreacion to set
	 */
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	@Override
	public int compare(ValuacionReporteDTO valuacion0, ValuacionReporteDTO valuacion1) {
		return ((valuacion0.getFechaValuacion() == null)? -1: 
				((valuacion1.getFechaValuacion() == null)? 1:
					(valuacion0.getFechaValuacion().before(valuacion1.getFechaValuacion())? 1 : 
						(valuacion0.getFechaValuacion().after(valuacion1.getFechaValuacion())) ? -1 : 0)));
	}
	
}
