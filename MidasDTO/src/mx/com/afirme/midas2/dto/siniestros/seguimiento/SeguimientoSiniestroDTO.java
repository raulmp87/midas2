package mx.com.afirme.midas2.dto.siniestros.seguimiento;
import java.io.Serializable;

/**
 * Objeto Abstracto con informaci�n com�n de los seguimientos de siniestro.
 * @author Arturo
 * @version 1.0
 * @created 13-oct-2014 11:31:16 a.m.
 */
public abstract class SeguimientoSiniestroDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6903360515262663341L;
	private String comentarios;
	private SeguimientoDatosPersona datosAsegurado;
	private SeguimientoDatosPersona datosContacto;
	private SeguimientoDatosVehiculo datosVehiculo;
	private SeguimientoSiniestroId siniestroId;

	public SeguimientoSiniestroDTO(){

	}

	public void finalize() throws Throwable {

	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public SeguimientoDatosPersona getDatosAsegurado() {
		return datosAsegurado;
	}

	public void setDatosAsegurado(SeguimientoDatosPersona datosAsegurado) {
		this.datosAsegurado = datosAsegurado;
	}

	public SeguimientoDatosPersona getDatosContacto() {
		return datosContacto;
	}

	public void setDatosContacto(SeguimientoDatosPersona datosContacto) {
		this.datosContacto = datosContacto;
	}

	public SeguimientoDatosVehiculo getDatosVehiculo() {
		return datosVehiculo;
	}

	public void setDatosVehiculo(SeguimientoDatosVehiculo datosVehiculo) {
		this.datosVehiculo = datosVehiculo;
	}

	public SeguimientoSiniestroId getSiniestroId() {
		return siniestroId;
	}

	public void setSiniestroId(SeguimientoSiniestroId siniestroId) {
		this.siniestroId = siniestroId;
	}
	

}