package mx.com.afirme.midas2.dto.siniestros.hgs;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)

public class EfileDevolucionRefacciones implements Serializable {
	
	
	@XmlElement(name="nombre",required=true)
	private String nombre;
	@XmlElement(name="sub_total",required=true)
	private BigDecimal subtotal;
	@XmlElement(name="iva",required=true)
	private BigDecimal iva;
	@XmlElement(name="monto_total",required=true)
	private BigDecimal montoTotal;
	
	
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -3631960767698425139L;

	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public BigDecimal getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public BigDecimal getIva() {
		return iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public BigDecimal getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}

	@Override
	public String toString() {
		return "EfileDevolucionRefacciones [nombre=" + nombre + ", subtotal="
				+ subtotal + ", iva=" + iva + ", montoTotal=" + montoTotal
				+ "]";
	}

}
