package mx.com.afirme.midas2.dto.siniestros.valuacion;

import java.math.BigDecimal;
import java.util.Date;

public class ValuacionHGSEditDTO {
	
	private static final long serialVersionUID = 2491112416785294308L;
	
	
	
	private Short tipoProceso;
	private Short estatusValuacion;
	private Long idValuacionHGS;
	private Date fechaIngresoTaller;
	private Date fechaTerminacion;
	private Long idValuador;
	private Date fechaReingresoReparacion;
	private Date ultimoSurtidoRefacciones;
	private String nombreValuador;
	private BigDecimal montoRefacciones;
	private BigDecimal montoManoObra;
	private BigDecimal montoTotalReparacion;

	public Short getTipoProceso() {
		return tipoProceso;
	}
	public void setTipoProceso(Short tipoProceso) {
		this.tipoProceso = tipoProceso;
	}
	public Short getEstatusValuacion() {
		return estatusValuacion;
	}
	public void setEstatusValuacion(Short estatusValuacion) {
		this.estatusValuacion = estatusValuacion;
	}
	public Long getIdValuacionHGS() {
		return idValuacionHGS;
	}
	public void setIdValuacionHGS(Long idValuacionHGS) {
		this.idValuacionHGS = idValuacionHGS;
	}
	public Date getFechaIngresoTaller() {
		return fechaIngresoTaller;
	}
	public void setFechaIngresoTaller(Date fechaIngresoTaller) {
		this.fechaIngresoTaller = fechaIngresoTaller;
	}
	public Date getFechaTerminacion() {
		return fechaTerminacion;
	}
	public void setFechaTerminacion(Date fechaTerminacion) {
		this.fechaTerminacion = fechaTerminacion;
	}
	public Long getIdValuador() {
		return idValuador;
	}
	public void setIdValuador(Long idValuador) {
		this.idValuador = idValuador;
	}
	public Date getFechaReingresoReparacion() {
		return fechaReingresoReparacion;
	}
	public void setFechaReingresoReparacion(Date fechaReingresoReparacion) {
		this.fechaReingresoReparacion = fechaReingresoReparacion;
	}
	public Date getUltimoSurtidoRefacciones() {
		return ultimoSurtidoRefacciones;
	}
	public void setUltimoSurtidoRefacciones(Date ultimoSurtidoRefacciones) {
		this.ultimoSurtidoRefacciones = ultimoSurtidoRefacciones;
	}
	public String getNombreValuador() {
		return nombreValuador;
	}
	public void setNombreValuador(String nombreValuador) {
		this.nombreValuador = nombreValuador;
	}
	public BigDecimal getMontoRefacciones() {
		return montoRefacciones;
	}
	public void setMontoRefacciones(BigDecimal montoRefacciones) {
		this.montoRefacciones = montoRefacciones;
	}
	public BigDecimal getMontoManoObra() {
		return montoManoObra;
	}
	public void setMontoManoObra(BigDecimal montoManoObra) {
		this.montoManoObra = montoManoObra;
	}
	public BigDecimal getMontoTotalReparacion() {
		return montoTotalReparacion;
	}
	public void setMontoTotalReparacion(BigDecimal montoTotalReparacion) {
		this.montoTotalReparacion = montoTotalReparacion;
	}
	
}
