package mx.com.afirme.midas2.dto.siniestros.indemnizacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class ImpresionOrdenExpedicionDTO implements Serializable{

	private static final long serialVersionUID = 8621937907867862544L;

	private String ramo;//
	
	private String aFavor;
	private String porConcepto;
	private Date   fechaSolicitud;
	private String localidad;
	private String usuarioSolicita;
	private String liquidacionEgreso;
	private Long   solicitudChequeMizar;//
	private Date   fechaElaboracionCheque;//
	private String numeroChequeReferencia;//
	private String tipo;//
	private String cuenta;//
	private String banco;//
	private String tipoOperacion;
	private Integer numeroPagos;//
	private String telefono;
	private String correo;
	private String situacion;
	private String cheque;
	private String nombreCompletoUsuarioSolicita;
	
	
	private String observaciones;
	private String elaborado;
	private String aprobado;
	
	//Totales DetalleOrdenExpedicion
	private BigDecimal total;
	private BigDecimal primasPendientesPorCobrar;
	
	//Totales DetalleCuentasContables
	private BigDecimal cargoTotal;
	private BigDecimal abonoTotal;
	
	//Total Notas Credito
	private BigDecimal totalNotasCredito;
	
	//Importes de la Liquidacion
	private DetalleOrdenExpedicionDTO importesLiquidacion;
	
	private List<DetalleOrdenExpedicionDTO>  detallesOrden;
	private List<DetalleCuentasContablesDTO> detalleCuentas;
	private List<DetalleOrdenExpedicionDTO>  detallesNotasCredito;
	
	//Fares for pdf's print just for liquidation provider.
	private BigDecimal subtotal;
	private BigDecimal deducible;
	private BigDecimal descuento;
	private BigDecimal iva;
	private BigDecimal ivaAcred;
	private BigDecimal ivaRet;
	private BigDecimal isr;
	private BigDecimal isrRet;
	private BigDecimal primasPendientesPago;
	private BigDecimal primasADevolver;
	private BigDecimal aPagar;
	private BigDecimal primasPendientes;
	
	
	
	/**
	 * @return the aFavor
	 */
	public String getaFavor() {
		return aFavor;
	}
	/**
	 * @param aFavor the aFavor to set
	 */
	public void setaFavor(String aFavor) {
		this.aFavor = aFavor;
	}
	/**
	 * @return the porConcepto
	 */
	public String getPorConcepto() {
		return porConcepto;
	}
	/**
	 * @param porConcepto the porConcepto to set
	 */
	public void setPorConcepto(String porConcepto) {
		this.porConcepto = porConcepto;
	}
	/**
	 * @return the fechaSolicitud
	 */
	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}
	/**
	 * @param fechaSolicitud the fechaSolicitud to set
	 */
	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}
	/**
	 * @return the localidad
	 */
	public String getLocalidad() {
		return localidad;
	}
	/**
	 * @param localidad the localidad to set
	 */
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	/**
	 * @return the usuarioSolicita
	 */
	public String getUsuarioSolicita() {
		return usuarioSolicita;
	}
	/**
	 * @param usuarioSolicita the usuarioSolicita to set
	 */
	public void setUsuarioSolicita(String usuarioSolicita) {
		this.usuarioSolicita = usuarioSolicita;
	}
	/**
	 * @return the liquidacionEgreso
	 */
	public String getLiquidacionEgreso() {
		return liquidacionEgreso;
	}
	/**
	 * @param liquidacionEgreso the liquidacionEgreso to set
	 */
	public void setLiquidacionEgreso(String liquidacionEgreso) {
		this.liquidacionEgreso = liquidacionEgreso;
	}
	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}
	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	/**
	 * @return the tipoOperacion
	 */
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	/**
	 * @param tipoOperacion the tipoOperacion to set
	 */
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	/**
	 * @return the total
	 */
	public BigDecimal getTotal() {
		return total;
	}
	/**
	 * @param total the total to set
	 */
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	/**
	 * @return the primasPendientesPorCobrar
	 */
	public BigDecimal getPrimasPendientesPorCobrar() {
		return primasPendientesPorCobrar;
	}
	/**
	 * @param primasPendientesPorCobrar the primasPendientesPorCobrar to set
	 */
	public void setPrimasPendientesPorCobrar(BigDecimal primasPendientesPorCobrar) {
		this.primasPendientesPorCobrar = primasPendientesPorCobrar;
	}
	/**
	 * @return the detallesOrden
	 */
	public List<DetalleOrdenExpedicionDTO> getDetallesOrden() {
		return detallesOrden;
	}
	/**
	 * @param detallesOrden the detallesOrden to set
	 */
	public void setDetallesOrden(List<DetalleOrdenExpedicionDTO> detallesOrden) {
		this.detallesOrden = detallesOrden;
	}
	/**
	 * @return the detalleCuentas
	 */
	public List<DetalleCuentasContablesDTO> getDetalleCuentas() {
		return detalleCuentas;
	}
	/**
	 * @param detalleCuentas the detalleCuentas to set
	 */
	public void setDetalleCuentas(List<DetalleCuentasContablesDTO> detalleCuentas) {
		this.detalleCuentas = detalleCuentas;
	}
	/**
	 * @return the cargoTotal
	 */
	public BigDecimal getCargoTotal() {
		return cargoTotal;
	}
	/**
	 * @param cargoTotal the cargoTotal to set
	 */
	public void setCargoTotal(BigDecimal cargoTotal) {
		this.cargoTotal = cargoTotal;
	}
	/**
	 * @return the abonoTotal
	 */
	public BigDecimal getAbonoTotal() {
		return abonoTotal;
	}
	/**
	 * @param abonoTotal the abonoTotal to set
	 */
	public void setAbonoTotal(BigDecimal abonoTotal) {
		this.abonoTotal = abonoTotal;
	}
	/**
	 * @return the ramo
	 */
	public String getRamo() {
		return ramo;
	}
	/**
	 * @param ramo the ramo to set
	 */
	public void setRamo(String ramo) {
		this.ramo = ramo;
	}
	/**
	 * @return the solicitudChequeMizar
	 */
	public Long getSolicitudChequeMizar() {
		return solicitudChequeMizar;
	}
	/**
	 * @param solicitudChequeMizar the solicitudChequeMizar to set
	 */
	public void setSolicitudChequeMizar(Long solicitudChequeMizar) {
		this.solicitudChequeMizar = solicitudChequeMizar;
	}
	/**
	 * @return the fechaElaboracionCheque
	 */
	public Date getFechaElaboracionCheque() {
		return fechaElaboracionCheque;
	}
	/**
	 * @param fechaElaboracionCheque the fechaElaboracionCheque to set
	 */
	public void setFechaElaboracionCheque(Date fechaElaboracionCheque) {
		this.fechaElaboracionCheque = fechaElaboracionCheque;
	}
	/**
	 * @return the numeroChequeReferencia
	 */
	public String getNumeroChequeReferencia() {
		return numeroChequeReferencia;
	}
	/**
	 * @param numeroChequeReferencia the numeroChequeReferencia to set
	 */
	public void setNumeroChequeReferencia(String numeroChequeReferencia) {
		this.numeroChequeReferencia = numeroChequeReferencia;
	}
	/**
	 * @return the cuenta
	 */
	public String getCuenta() {
		return cuenta;
	}
	/**
	 * @param cuenta the cuenta to set
	 */
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	/**
	 * @return the banco
	 */
	public String getBanco() {
		return banco;
	}
	/**
	 * @param banco the banco to set
	 */
	public void setBanco(String banco) {
		this.banco = banco;
	}
	/**
	 * @return the numeroPagos
	 */
	public Integer getNumeroPagos() {
		return numeroPagos;
	}
	/**
	 * @param numeroPagos the numeroPagos to set
	 */
	public void setNumeroPagos(Integer numeroPagos) {
		this.numeroPagos = numeroPagos;
	}
	/**
	 * @return the observaciones
	 */
	public String getObservaciones() {
		return observaciones;
	}
	/**
	 * @param observaciones the observaciones to set
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	/**
	 * @return the elaborado
	 */
	public String getElaborado() {
		return elaborado;
	}
	/**
	 * @param elaborado the elaborado to set
	 */
	public void setElaborado(String elaborado) {
		this.elaborado = elaborado;
	}
	/**
	 * @return the aprobado
	 */
	public String getAprobado() {
		return aprobado;
	}
	/**
	 * @param aprobado the aprobado to set
	 */
	public void setAprobado(String aprobado) {
		this.aprobado = aprobado;
	}
	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}
	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	/**
	 * @return the correo
	 */
	public String getCorreo() {
		return correo;
	}
	/**
	 * @param correo the correo to set
	 */
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	/**
	 * @return the situacion
	 */
	public String getSituacion() {
		return situacion;
	}
	/**
	 * @param situacion the situacion to set
	 */
	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}
	/**
	 * @return the cheque
	 */
	public String getCheque() {
		return cheque;
	}
	/**
	 * @param cheque the cheque to set
	 */
	public void setCheque(String cheque) {
		this.cheque = cheque;
	}
	/**
	 * @return the importesLiquidacion
	 */
	public DetalleOrdenExpedicionDTO getImportesLiquidacion() {
		return importesLiquidacion;
	}
	/**
	 * @param importesLiquidacion the importesLiquidacion to set
	 */
	public void setImportesLiquidacion(DetalleOrdenExpedicionDTO importesLiquidacion) {
		this.importesLiquidacion = importesLiquidacion;
	}
	/**
	 * @return the detallesNotasCredito
	 */
	public List<DetalleOrdenExpedicionDTO> getDetallesNotasCredito() {
		return detallesNotasCredito;
	}
	/**
	 * @param detallesNotasCredito the detallesNotasCredito to set
	 */
	public void setDetallesNotasCredito(
			List<DetalleOrdenExpedicionDTO> detallesNotasCredito) {
		this.detallesNotasCredito = detallesNotasCredito;
	}
	/**
	 * @return the totalNotasCredito
	 */
	public BigDecimal getTotalNotasCredito() {
		return totalNotasCredito;
	}
	/**
	 * @param totalNotasCredito the totalNotasCredito to set
	 */
	public void setTotalNotasCredito(BigDecimal totalNotasCredito) {
		this.totalNotasCredito = totalNotasCredito;
	}
	//Getters and setters for fares pdf's print
	public BigDecimal getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}
	public BigDecimal getDeducible() {
		return deducible;
	}
	public void setDeducible(BigDecimal deducible) {
		this.deducible = deducible;
	}
	public BigDecimal getDescuento() {
		return descuento;
	}
	public void setDescuento(BigDecimal descuento) {
		this.descuento = descuento;
	}
	public BigDecimal getIva() {
		return iva;
	}
	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}
	public BigDecimal getIvaAcred() {
		return ivaAcred;
	}
	public void setIvaAcred(BigDecimal ivaAcred) {
		this.ivaAcred = ivaAcred;
	}
	public BigDecimal getIvaRet() {
		return ivaRet;
	}
	public void setIvaRet(BigDecimal ivaRet) {
		this.ivaRet = ivaRet;
	}
	public BigDecimal getIsr() {
		return isr;
	}
	public void setIsr(BigDecimal isr) {
		this.isr = isr;
	}
	public BigDecimal getIsrRet() {
		return isrRet;
	}
	public void setIsrRet(BigDecimal isrRet) {
		this.isrRet = isrRet;
	}
	public BigDecimal getPrimasPendientesPago() {
		return primasPendientesPago;
	}
	public void setPrimasPendientesPago(BigDecimal primasPendientesPago) {
		this.primasPendientesPago = primasPendientesPago;
	}
	public BigDecimal getPrimasADevolver() {
		return primasADevolver;
	}
	public void setPrimasADevolver(BigDecimal primasADevolver) {
		this.primasADevolver = primasADevolver;
	}
	public BigDecimal getaPagar() {
		return aPagar;
	}
	public void setaPagar(BigDecimal aPagar) {
		this.aPagar = aPagar;
	}
	public BigDecimal getPrimasPendientes() {
		return primasPendientes;
	}
	public void setPrimasPendientes(BigDecimal primasPendientes) {
		this.primasPendientes = primasPendientes;
	}

	public String getNombreCompletoUsuarioSolicita() {
		return nombreCompletoUsuarioSolicita;
	}
	
	public void setNombreCompletoUsuarioSolicita(
			String nombreCompletoUsuarioSolicita) {
		this.nombreCompletoUsuarioSolicita = nombreCompletoUsuarioSolicita;
	}
}