package mx.com.afirme.midas2.dto.siniestros.indemnizacion;

import java.io.Serializable;

import mx.com.afirme.midas2.domain.siniestros.indemnizacion.CartaSiniestro;

public class ImpresionEntregaDocumentosDTO implements Serializable{

	private static final long serialVersionUID = -1747305337981986263L;
	
	private CartaSiniestro informacionEntrega;
	private String numeroSiniestro;
	private String marcaTipo;
	private String modelo;
	private String numeroSerie;
	private String endosos;

	/**
	 * @return the informacionEntrega
	 */
	public CartaSiniestro getInformacionEntrega() {
		return informacionEntrega;
	}
	/**
	 * @param informacionEntrega the informacionEntrega to set
	 */
	public void setInformacionEntrega(CartaSiniestro informacionEntrega) {
		this.informacionEntrega = informacionEntrega;
	}
	/**
	 * @return the numeroSiniestro
	 */
	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}
	/**
	 * @param numeroSiniestro the numeroSiniestro to set
	 */
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}
	/**
	 * @return the marcaTipo
	 */
	public String getMarcaTipo() {
		return marcaTipo;
	}
	/**
	 * @param marcaTipo the marcaTipo to set
	 */
	public void setMarcaTipo(String marcaTipo) {
		this.marcaTipo = marcaTipo;
	}
	/**
	 * @return the modelo
	 */
	public String getModelo() {
		return modelo;
	}
	/**
	 * @param modelo the modelo to set
	 */
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	/**
	 * @return the numeroSerie
	 */
	public String getNumeroSerie() {
		return numeroSerie;
	}
	/**
	 * @param numeroSerie the numeroSerie to set
	 */
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	/**
	 * @return the endosos
	 */
	public String getEndosos() {
		return endosos;
	}
	/**
	 * @param endosos the endosos to set
	 */
	public void setEndosos(String endosos) {
		this.endosos = endosos;
	}

}