package mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos;

import java.io.Serializable;
import java.util.Date;

import mx.com.afirme.midas2.annotation.Exportable;

public class IngresoDevolucionDTO implements Serializable{

	private static final long	serialVersionUID	= 7941073887640515507L;
	
	private Long   id;
	private Long   cuentaAcreedora;
	private String cuentaAcreedoraStr;
	private String tipoRecuperacion;
	private String tipoDevolucion;
	private String siniestroOrigen;
	private String motivoCancelacion;
	private String formaPago;
	private Long   numSolicitud;
	private String solicitadoPor;
	private String autorizadoPor;
	private String numChequeRef;
	private Date fechaSolicitud;
	private Date fechaAutorizacion;
	private Date fechaCheque;
	private Double importe;
	private String estatus;
	private Date fechaIngreso;
	private Long cuentaAcreedoraRestante;
	private String cuentaAcreedoraRestanteStr;

	public IngresoDevolucionDTO (){

	}

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	public Long getCuentaAcreedora() {
		return cuentaAcreedora;
	}

	public void setCuentaAcreedora(Long cuentaAcreedora) {
		this.cuentaAcreedora = cuentaAcreedora;
	}

	@Exportable(columnName = "Tipo de Recuperación", columnOrder = 3)
	public String getTipoRecuperacion() {
		return tipoRecuperacion;
	}

	public void setTipoRecuperacion(String tipoRecuperacion) {
		this.tipoRecuperacion = tipoRecuperacion;
	}

	@Exportable(columnName = "Tipo de Devolución", columnOrder = 0)
	public String getTipoDevolucion() {
		return tipoDevolucion;
	}

	public void setTipoDevolucion(String tipoDevolucion) {
		this.tipoDevolucion = tipoDevolucion;
	}

	@Exportable(columnName = "Siniestro Origen", columnOrder = 2)
	public String getSiniestroOrigen() {
		return siniestroOrigen;
	}

	public void setSiniestroOrigen(String siniestroOrigen) {
		this.siniestroOrigen = siniestroOrigen;
	}

	@Exportable(columnName = "Motivo de Cancelación", columnOrder = 9)
	public String getMotivoCancelacion() {
		return motivoCancelacion;
	}

	public void setMotivoCancelacion(String motivoCancelacion) {
		this.motivoCancelacion = motivoCancelacion;
	}

	@Exportable(columnName = "Forma de Pago", columnOrder = 6)
	public String getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}

	@Exportable(columnName = "Número Solicitud de Pago", columnOrder = 7)
	public Long getNumSolicitud() {
		return numSolicitud;
	}

	public void setNumSolicitud(Long numSolicitud) {
		this.numSolicitud = numSolicitud;
	}

	@Exportable(columnName = "Solicitado por", columnOrder = 4)
	public String getSolicitadoPor() {
		return solicitadoPor;
	}

	public void setSolicitadoPor(String solicitadoPor) {
		this.solicitadoPor = solicitadoPor;
	}

	@Exportable(columnName = "Autorizado por", columnOrder = 5)
	public String getAutorizadoPor() {
		return autorizadoPor;
	}

	public void setAutorizadoPor(String autorizadoPor) {
		this.autorizadoPor = autorizadoPor;
	}

	@Exportable(columnName = "Número Cheque/Referencia", columnOrder = 8)
	public String getNumChequeRef() {
		return numChequeRef;
	}

	public void setNumChequeRef(String numChequeRef) {
		this.numChequeRef = numChequeRef;
	}

	@Exportable(columnName = "Fecha de Solicitud", columnOrder = 12)
	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}

	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	@Exportable(columnName = "Fecha de Autorización de Solicitud", columnOrder = 13)
	public Date getFechaAutorizacion() {
		return fechaAutorizacion;
	}

	public void setFechaAutorizacion(Date fechaAutorizacion) {
		this.fechaAutorizacion = fechaAutorizacion;
	}

	@Exportable(columnName = "Fecha de Elaboración de Cheque", columnOrder = 14)
	public Date getFechaCheque() {
		return fechaCheque;
	}

	public void setFechaCheque(Date fechaCheque) {
		this.fechaCheque = fechaCheque;
	}

	@Exportable(columnName = "Importe", columnOrder = 10)
	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	@Exportable(columnName = "Estatus", columnOrder = 15)
	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	@Exportable(columnName = "Fecha Registro de Ingreso", columnOrder = 11)
	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public Long getCuentaAcreedoraRestante() {
		return cuentaAcreedoraRestante;
	}

	public void setCuentaAcreedoraRestante(Long cuentaAcreedoraRestante) {
		this.cuentaAcreedoraRestante = cuentaAcreedoraRestante;
	}

	@Exportable(columnName = "Cuenta Acreedora", columnOrder = 1)
	public String getCuentaAcreedoraStr() {
		return cuentaAcreedoraStr;
	}


	public void setCuentaAcreedoraStr(String cuentaAcreedoraStr) {
		this.cuentaAcreedoraStr = cuentaAcreedoraStr;
	}


	public String getCuentaAcreedoraRestanteStr() {
		return cuentaAcreedoraRestanteStr;
	}

	public void setCuentaAcreedoraRestanteStr(String cuentaAcreedoraRestanteStr) {
		this.cuentaAcreedoraRestanteStr = cuentaAcreedoraRestanteStr;
	}

}