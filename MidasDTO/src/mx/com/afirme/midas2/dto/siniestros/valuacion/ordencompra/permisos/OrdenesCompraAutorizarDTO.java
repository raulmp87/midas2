/**
 * 
 */
package mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.permisos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.springframework.stereotype.Component;

/**
 * @author admin
 *
 */
@Component
public class OrdenesCompraAutorizarDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5845458573034972182L;

	private String noOrdenCompra;
	
	private String noSiniestro;
	
	private String cobertura;
	
	private String conceptoPago;
	
	private Date fechaElaboracion;
	
	private int cantidad;
	
	private BigDecimal subtotal;
	
	private BigDecimal iva;
	
	private BigDecimal importe;
	
	private BigDecimal totalPagado;
	
	private BigDecimal totalPendiente;
	
	private String estatus;
	
	private String tipoOrdenCompra;

	/**
	 * @return the noOrdenCompra
	 */
	public String getNoOrdenCompra() {
		return noOrdenCompra;
	}

	/**
	 * @param noOrdenCompra the noOrdenCompra to set
	 */
	public void setNoOrdenCompra(String noOrdenCompra) {
		this.noOrdenCompra = noOrdenCompra;
	}

	/**
	 * @return the noSiniestro
	 */
	public String getNoSiniestro() {
		return noSiniestro;
	}

	/**
	 * @param noSiniestro the noSiniestro to set
	 */
	public void setNoSiniestro(String noSiniestro) {
		this.noSiniestro = noSiniestro;
	}

	/**
	 * @return the cobertura
	 */
	public String getCobertura() {
		return cobertura;
	}

	/**
	 * @param cobertura the cobertura to set
	 */
	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}

	/**
	 * @return the conceptoPago
	 */
	public String getConceptoPago() {
		return conceptoPago;
	}

	/**
	 * @param conceptoPago the conceptoPago to set
	 */
	public void setConceptoPago(String conceptoPago) {
		this.conceptoPago = conceptoPago;
	}

	/**
	 * @return the fechaElaboracion
	 */
	public Date getFechaElaboracion() {
		return fechaElaboracion;
	}

	/**
	 * @param fechaElaboracion the fechaElaboracion to set
	 */
	public void setFechaElaboracion(Date fechaElaboracion) {
		this.fechaElaboracion = fechaElaboracion;
	}

	/**
	 * @return the cantidad
	 */
	public int getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	/**
	 * @return the subtotal
	 */
	public BigDecimal getSubtotal() {
		return subtotal;
	}

	/**
	 * @param subtotal the subtotal to set
	 */
	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	/**
	 * @return the iva
	 */
	public BigDecimal getIva() {
		return iva;
	}

	/**
	 * @param iva the iva to set
	 */
	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	/**
	 * @return the importe
	 */
	public BigDecimal getImporte() {
		return importe;
	}

	/**
	 * @param importe the importe to set
	 */
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	/**
	 * @return the totalPagado
	 */
	public BigDecimal getTotalPagado() {
		return totalPagado;
	}

	/**
	 * @param totalPagado the totalPagado to set
	 */
	public void setTotalPagado(BigDecimal totalPagado) {
		this.totalPagado = totalPagado;
	}

	/**
	 * @return the totalPendiente
	 */
	public BigDecimal getTotalPendiente() {
		return totalPendiente;
	}

	/**
	 * @param totalPendiente the totalPendiente to set
	 */
	public void setTotalPendiente(BigDecimal totalPendiente) {
		this.totalPendiente = totalPendiente;
	}

	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getTipoOrdenCompra() {
		return tipoOrdenCompra;
	}

	public void setTipoOrdenCompra(String tipoOrdenCompra) {
		this.tipoOrdenCompra = tipoOrdenCompra;
	}
	
	
	

}
