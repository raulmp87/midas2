/**
 * 
 */
package mx.com.afirme.midas2.dto.siniestros.pagos.facturas;

import java.io.Serializable;
import java.util.Date;

/**
 * @author admin
 *
 */
public class DevolucionesFacturasDTO  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6321016548139515770L;

	private Long idFactura;
	
	private Date fechaDevolcion;
	
	private String numeroFactura;
	
	private String motivo;
	
	private String devueltaPor;
	
	private Date fechaEntrega;
	
	private String entregadaA;
	
	private String ordenCompra;
	
	private String proveedor;
	

	/**
	 * @return the fechaDevolcion
	 */
	public Date getFechaDevolcion() {
		return fechaDevolcion;
	}

	/**
	 * @param fechaDevolcion the fechaDevolcion to set
	 */
	public void setFechaDevolcion(Date fechaDevolcion) {
		this.fechaDevolcion = fechaDevolcion;
	}

	/**
	 * @return the numeroFactura
	 */
	public String getNumeroFactura() {
		return numeroFactura;
	}

	/**
	 * @param numeroFactura the numeroFactura to set
	 */
	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	/**
	 * @return the motivo
	 */
	public String getMotivo() {
		return motivo;
	}

	/**
	 * @param motivo the motivo to set
	 */
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	/**
	 * @return the devueltaPor
	 */
	public String getDevueltaPor() {
		return devueltaPor;
	}

	/**
	 * @param devueltaPor the devueltaPor to set
	 */
	public void setDevueltaPor(String devueltaPor) {
		this.devueltaPor = devueltaPor;
	}

	/**
	 * @return the fechaEntrega
	 */
	public Date getFechaEntrega() {
		return fechaEntrega;
	}

	/**
	 * @param fechaEntrega the fechaEntrega to set
	 */
	public void setFechaEntrega(Date fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}

	/**
	 * @return the entregadaA
	 */
	public String getEntregadaA() {
		return entregadaA;
	}

	/**
	 * @param entregadaA the entregadaA to set
	 */
	public void setEntregadaA(String entregadaA) {
		this.entregadaA = entregadaA;
	}

	/**
	 * @return the ordenCompra
	 */
	public String getOrdenCompra() {
		return ordenCompra;
	}

	/**
	 * @param ordenCompra the ordenCompra to set
	 */
	public void setOrdenCompra(String ordenCompra) {
		this.ordenCompra = ordenCompra;
	}

	/**
	 * @return the proveedor
	 */
	public String getProveedor() {
		return proveedor;
	}

	/**
	 * @param proveedor the proveedor to set
	 */
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	/**
	 * @return the idFactura
	 */
	public Long getIdFactura() {
		return idFactura;
	}

	/**
	 * @param idFactura the idFactura to set
	 */
	public void setIdFactura(Long idFactura) {
		this.idFactura = idFactura;
	}
	
	
}
