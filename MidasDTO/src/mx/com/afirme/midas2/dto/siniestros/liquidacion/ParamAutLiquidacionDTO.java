package mx.com.afirme.midas2.dto.siniestros.liquidacion;

import java.io.Serializable;
import java.util.Date;

public class ParamAutLiquidacionDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String criterioFechaActivo;
	private String criterioFechaInactivo;
	private Short estatus;
	private Date fechaActivoFin;
	private Date fechaActivoIni;
	private Date fechaInactivoFin;
	private Date fechaInactivoIni;
	private String liquidacion;
	private String nombreConfiguracion;
	private String nombreUsuarioConfigurador;
	private String usuario;
	private Long numero;
	private String tipoLiquidacion;
	private String tipoPago;
	private Boolean rangoFechaActivo;
	private Boolean rangoFechaInactivo;
	

	public ParamAutLiquidacionDTO(){

	}
	
	public void finalize() throws Throwable {

	}

	public String getCriterioFechaActivo() {
		return criterioFechaActivo;
	}

	public void setCriterioFechaActivo(String criterioFechaActivo) {
		this.criterioFechaActivo = criterioFechaActivo;
	}

	public String getCriterioFechaInactivo() {
		return criterioFechaInactivo;
	}

	public void setCriterioFechaInactivo(String criterioFechaInactivo) {
		this.criterioFechaInactivo = criterioFechaInactivo;
	}

	public Short getEstatus() {
		return estatus;
	}

	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}

	public Date getFechaActivoFin() {
		return fechaActivoFin;
	}

	public void setFechaActivoFin(Date fechaActivoFin) {
		this.fechaActivoFin = fechaActivoFin;
	}

	public Date getFechaActivoIni() {
		return fechaActivoIni;
	}

	public void setFechaActivoIni(Date fechaActivoIni) {
		this.fechaActivoIni = fechaActivoIni;
	}

	public Date getFechaInactivoFin() {
		return fechaInactivoFin;
	}

	public void setFechaInactivoFin(Date fechaInactivoFin) {
		this.fechaInactivoFin = fechaInactivoFin;
	}

	public Date getFechaInactivoIni() {
		return fechaInactivoIni;
	}

	public void setFechaInactivoIni(Date fechaInactivoIni) {
		this.fechaInactivoIni = fechaInactivoIni;
	}

	public String getLiquidacion() {
		return liquidacion;
	}

	public void setLiquidacion(String liquidacion) {
		this.liquidacion = liquidacion;
	}

	public String getNombreConfiguracion() {
		return nombreConfiguracion;
	}

	public void setNombreConfiguracion(String nombreConfiguracion) {
		this.nombreConfiguracion = nombreConfiguracion;
	}

	public String getNombreUsuarioConfigurador() {
		return nombreUsuarioConfigurador;
	}

	public void setNombreUsuarioConfigurador(String nombreUsuarioConfigurador) {
		this.nombreUsuarioConfigurador = nombreUsuarioConfigurador;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public String getTipoLiquidacion() {
		return tipoLiquidacion;
	}

	public void setTipoLiquidacion(String tipoLiquidacion) {
		this.tipoLiquidacion = tipoLiquidacion;
	}

	public String getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}

	public Boolean getRangoFechaActivo() {
		return rangoFechaActivo;
	}

	public void setRangoFechaActivo(Boolean rangoFechaActivo) {
		this.rangoFechaActivo = rangoFechaActivo;
	}

	public Boolean getRangoFechaInactivo() {
		return rangoFechaInactivo;
	}

	public void setRangoFechaInactivo(Boolean rangoFechaInactivo) {
		this.rangoFechaInactivo = rangoFechaInactivo;
	}
	
	
}
