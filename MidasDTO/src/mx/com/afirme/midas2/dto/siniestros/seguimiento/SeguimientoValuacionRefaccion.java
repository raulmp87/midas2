package mx.com.afirme.midas2.dto.siniestros.seguimiento;

import java.io.Serializable;


/**
 * Objeto para transportar la informaci�n de las refacciones de una valuaci�n.
 * @author Arturo
 * @version 1.0
 * @created 13-oct-2014 11:36:49 a.m.
 */
public class SeguimientoValuacionRefaccion implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1215371909132521389L;
	private String descripcion;
	private String estatus;
	private String fechaPromesa;
	private String fechaRecepcion;

	public SeguimientoValuacionRefaccion(){

	}

	public void finalize() throws Throwable {

	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getFechaPromesa() {
		return fechaPromesa;
	}

	public void setFechaPromesa(String fechaPromesa) {
		this.fechaPromesa = fechaPromesa;
	}

	public String getFechaRecepcion() {
		return fechaRecepcion;
	}

	public void setFechaRecepcion(String fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}
	

}