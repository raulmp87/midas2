/**
 * 
 */
package mx.com.afirme.midas2.dto.siniestros;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;

import org.springframework.stereotype.Component;

/**
 * @author simavera
 *
 */
@Component
public class PaseAtencionSiniestroDTO implements Serializable{

	private static final long serialVersionUID = 7011915477488428732L;
	private EstimacionCoberturaReporteCabina estimacionCoberturaReporte;
	private String nombreCobertura;
	private BigDecimal montoReserva;
	private String tipoPaseAtencionDescripcion;
	private String numeroReporte;
	private String numeroSiniestro;
	private Long idReporteCabina;
	private String descripcionPaseAtencion;
	private String descripcionVehiculo;
	private BigDecimal importeDeducible;
	private String estatus;
	private Date fechaInicial;
	private Date fechaFinal;
	private String limiteMontoPaciente;
	private String estimacionTipoPaseDeAtencion;
	private Boolean puedeImprimir;
	private Boolean fueEnviadoAHGS;
	private Boolean aplicaHGS;
	
	public PaseAtencionSiniestroDTO () {
		super();
	}
	
	public PaseAtencionSiniestroDTO (EstimacionCoberturaReporteCabina estimacion, Long idReporteCabina, String numeroReporte,
									String numeroSiniestro, String tipoPaseAtencionDescripcion, String nombreCobertura, 
									Date fecha, Integer estatus, Boolean aplicaHGS, Boolean fueEnviadoAHGS, Boolean puedeImprimir) {
		super();
		this.estimacionCoberturaReporte = estimacion;
		this.idReporteCabina = idReporteCabina;
		this.numeroReporte = numeroReporte;
		this.numeroSiniestro = numeroSiniestro;
		this.tipoPaseAtencionDescripcion = tipoPaseAtencionDescripcion;
		this.nombreCobertura = nombreCobertura;
		this.fechaInicial = fecha;
		
		if (estatus != null) {
			this.estatus = estatus.toString();
		}
		
		this.aplicaHGS = aplicaHGS;
		this.fueEnviadoAHGS = fueEnviadoAHGS;
		this.puedeImprimir = puedeImprimir;
		
	}
	
	
	
	/**
	 * @return the estimacionCoberturaReporte
	 */
	public EstimacionCoberturaReporteCabina getEstimacionCoberturaReporte() {
		return estimacionCoberturaReporte;
	}
	/**
	 * @param estimacionCoberturaReporte the estimacionCoberturaReporte to set
	 */
	public void setEstimacionCoberturaReporte(
			EstimacionCoberturaReporteCabina estimacionCoberturaReporte) {
		this.estimacionCoberturaReporte = estimacionCoberturaReporte;
	}
	/**
	 * @return the nombreCobertura
	 */
	public String getNombreCobertura() {
		return nombreCobertura;
	}
	/**
	 * @param nombreCobertura the nombreCobertura to set
	 */
	public void setNombreCobertura(String nombreCobertura) {
		this.nombreCobertura = nombreCobertura;
	}

	
	/**
	 * @return the tipoPaseAtencionDescripcion
	 */
	public String getTipoPaseAtencionDescripcion() {
		return tipoPaseAtencionDescripcion;
	}
	/**
	 * @param tipoPaseAtencionDescripcion the tipoPaseAtencionDescripcion to set
	 */
	public void setTipoPaseAtencionDescripcion(
			String tipoPaseAtencionDescripcion) {
		this.tipoPaseAtencionDescripcion = tipoPaseAtencionDescripcion;
	}
	/**
	 * @return the numeroReporte
	 */
	public String getNumeroReporte() {
		return numeroReporte;
	}
	/**
	 * @param numeroReporte the numeroReporte to set
	 */
	public void setNumeroReporte(String numeroReporte) {
		this.numeroReporte = numeroReporte;
	}
	/**
	 * @return the numeroSiniestro
	 */
	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}
	/**
	 * @param numeroSiniestro the numeroSiniestro to set
	 */
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}
	/**
	 * @return the idReporteCabina
	 */
	public Long getIdReporteCabina() {
		return idReporteCabina;
	}
	/**
	 * @param idReporteCabina the idReporteCabina to set
	 */
	public void setIdReporteCabina(Long idReporteCabina) {
		this.idReporteCabina = idReporteCabina;
	}
	/**
	 * @return the descripcionPaseAtencion
	 */
	public String getDescripcionPaseAtencion() {
		return descripcionPaseAtencion;
	}
	/**
	 * @param descripcionPaseAtencion the descripcionPaseAtencion to set
	 */
	public void setDescripcionPaseAtencion(String descripcionPaseAtencion) {
		this.descripcionPaseAtencion = descripcionPaseAtencion;
	}
	/**
	 * @return the descripcionVehiculo
	 */
	public String getDescripcionVehiculo() {
		return descripcionVehiculo;
	}
	/**
	 * @param descripcionVehiculo the descripcionVehiculo to set
	 */
	public void setDescripcionVehiculo(String descripcionVehiculo) {
		this.descripcionVehiculo = descripcionVehiculo;
	}
	public BigDecimal getMontoReserva() {
		return montoReserva;
	}
	public void setMontoReserva(BigDecimal montoReserva) {
		this.montoReserva = montoReserva;
	}
	public BigDecimal getImporteDeducible() {
		return importeDeducible;
	}
	public void setImporteDeducible(BigDecimal importeDeducible) {
		this.importeDeducible = importeDeducible;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public Date getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public String getLimiteMontoPaciente() {
		return limiteMontoPaciente;
	}
	public void setLimiteMontoPaciente(String limiteMontoPaciente) {
		this.limiteMontoPaciente = limiteMontoPaciente;
	}
	public String getEstimacionTipoPaseDeAtencion() {
		return estimacionTipoPaseDeAtencion;
	}
	public void setEstimacionTipoPaseDeAtencion(String estimacionTipoPaseDeAtencion) {
		this.estimacionTipoPaseDeAtencion = estimacionTipoPaseDeAtencion;
	}
	public Date getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	public Boolean isEmpty(){
		Boolean isEmpty = false;
		
		if( ( estimacionCoberturaReporte.getFolio() == null || estimacionCoberturaReporte.getFolio().equals("") ) &&
			( estimacionCoberturaReporte.getTipoPaseAtencion() == null || estimacionCoberturaReporte.getTipoPaseAtencion().equals("") ) &&
			( nombreCobertura == null || nombreCobertura.equals("") ) &&
			( montoReserva == null || montoReserva.equals("") ) &&
			( tipoPaseAtencionDescripcion == null || tipoPaseAtencionDescripcion.equals("") ) &&
			( numeroReporte == null || numeroReporte.equals("") ) &&
			( numeroSiniestro == null || numeroSiniestro.equals("") ) &&
			( idReporteCabina == null || idReporteCabina.equals("") ) &&
			( descripcionPaseAtencion == null || descripcionPaseAtencion.equals("") ) &&
			( descripcionVehiculo == null || descripcionVehiculo.equals("") ) &&
			( importeDeducible == null || importeDeducible.equals("") ) &&
			( estatus == null || estatus.equals("") ) &&
			( fechaInicial == null || fechaInicial.equals("") ) &&
			( fechaFinal == null || fechaFinal.equals("") ) &&
			( limiteMontoPaciente == null || limiteMontoPaciente.equals("") )) {
			isEmpty = true;
		}
		
		return isEmpty;
	}
	public Boolean getPuedeImprimir() {
		return puedeImprimir;
	}
	public void setPuedeImprimir(Boolean puedeImprimir) {
		this.puedeImprimir = puedeImprimir;
	}
	public Boolean getFueEnviadoAHGS() {
		return fueEnviadoAHGS;
	}
	public void setFueEnviadoAHGS(Boolean fueEnviadoAHGS) {
		this.fueEnviadoAHGS = fueEnviadoAHGS;
	}
	public Boolean getAplicaHGS() {
		return aplicaHGS;
	}
	public void setAplicaHGS(Boolean aplicaHGS) {
		this.aplicaHGS = aplicaHGS;
	}
	
}
