package mx.com.afirme.midas2.dto.siniestros.depuracion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;

import org.springframework.stereotype.Component;

@Component
public class SeccionSiniestroDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4721792771848523563L;
	
	private List<CoberturaSeccionSiniestroDTO> coberturas = new ArrayList<CoberturaSeccionSiniestroDTO>();
	private SeccionDTO seccion;
	
	public List<CoberturaSeccionSiniestroDTO> getCoberturas() {
		return coberturas;
	}
	public void setCoberturas(List<CoberturaSeccionSiniestroDTO> coberturas) {
		this.coberturas = coberturas;
	}
	public SeccionDTO getSeccion() {
		return seccion;
	}
	public void setSeccion(SeccionDTO seccion) {
		this.seccion = seccion;
	}
}
