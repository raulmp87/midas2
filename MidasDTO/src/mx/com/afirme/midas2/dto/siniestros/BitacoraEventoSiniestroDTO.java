package mx.com.afirme.midas2.dto.siniestros;

import java.util.Date;

import mx.com.afirme.midas2.annotation.Exportable;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation.OperationType;
import mx.com.afirme.midas2.util.DateUtils;

import org.springframework.stereotype.Component;
@Component
public class BitacoraEventoSiniestroDTO {
	
	private static final String	FORMATO_FECHA	= "dd/MM/yyyy HH:mm";

	private String evento;
	private Date fechaFin;
	private Date fechaInicio;
	private String modulo;
	private String nombreUsuario;
	private String observaciones;
	private String fechaHora;

	
	@Exportable(columnName="Evento", columnOrder=1)
	@FilterPersistenceAnnotation(persistenceName="idEvento")
	public String getEvento() {
		return evento;
	}

	public void setEvento(String evento) {
		this.evento = evento;
	}
	
	
	@FilterPersistenceAnnotation(persistenceName="fechaCreacion", truncateDate=true , operation=OperationType.LESSTHANEQUAL, paramKey="fechaCreacionA")
	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	
	@FilterPersistenceAnnotation(persistenceName="fechaCreacion", truncateDate=true , operation=OperationType.GREATERTHANEQUAL, paramKey="fechaCreacionDe")
	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	
	@Exportable(columnName="Modulo", columnOrder=2)
	@FilterPersistenceAnnotation(excluded=true)
	public String getModulo() {
		return modulo;
	}

	public void setModulo(String modulo) {
		this.modulo = modulo;
	}
	
	@Exportable(columnName="Evento Generado por", columnOrder=3)
	@FilterPersistenceAnnotation(excluded=true)
	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	@Exportable(columnName="Observaciones", columnOrder=4)
	@FilterPersistenceAnnotation(excluded=true)	
	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	@Exportable(columnName="Fecha/Hora", columnOrder=0)
	@FilterPersistenceAnnotation(excluded=true)	
	public String getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(String fechaHora) {
		this.fechaHora = fechaHora;
	}
	
	public BitacoraEventoSiniestroDTO(){
		super();
	}
	
	
	
	public BitacoraEventoSiniestroDTO(String evento, Date fechaFin,
			Date fechaInicio, String modulo, String nombreUsuario,
			String observaciones, Date fechaHora) {
		this();
		this.evento = evento;
		this.fechaFin = fechaFin;
		this.fechaInicio = fechaInicio;
		this.modulo = modulo;
		this.nombreUsuario = nombreUsuario;
		this.observaciones = observaciones;
		this.fechaHora = DateUtils.toString(fechaHora,FORMATO_FECHA);
	}

	public BitacoraEventoSiniestroDTO(String evento, String modulo, String nombreUsuario,
			String observaciones, Date fechaHora) {
		this(evento, null, null, modulo, nombreUsuario, observaciones, fechaHora );		
	}
	
	
	public void finalize() throws Throwable {

	}
}