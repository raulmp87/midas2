package mx.com.afirme.midas2.dto.siniestros.catalogos.conceptos;

import java.io.Serializable;
import java.math.BigDecimal;

import org.springframework.stereotype.Component;

@Component
public class ConceptoCoberturaDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String     claveSubCobertura;
	private BigDecimal idToCobertura;
	
	public ConceptoCoberturaDTO(){
		
	}
	
	public ConceptoCoberturaDTO(String xClaveSubCobertura, BigDecimal xIdToCobertura ){
		this.claveSubCobertura = xClaveSubCobertura;
		this.idToCobertura     = xIdToCobertura;
	}

	public String getClaveSubCobertura() {
		return claveSubCobertura;
	}

	public void setClaveSubCobertura(String claveSubCobertura) {
		this.claveSubCobertura = claveSubCobertura;
	}

	public BigDecimal getIdToCobertura() {
		return idToCobertura;
	}

	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

}
