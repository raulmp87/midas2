package mx.com.afirme.midas2.dto.siniestros.indemnizacion;

import java.io.Serializable;
import java.util.Date;

public class CartaBajaPlacasDTO implements Serializable{

	private static final long serialVersionUID = -2155521138349236672L;
	
	private String	marcaDesc;
	private String	descTipoUso;
	private String	modelo;
	private String	placa;
	private String	numeroSerie;
	private Date	fechaSiniestro;
	private String	estado;
	private String 	numeroAveriguacionPrevia;
	private String	agenteMinisterio;
	private String	nombreUsuario;
	private String	puestoUsuario;
	private String 	tipoCarta;
	private String 	nombreTipoCarta;
	private String	descripcionSol;
	private String	descripcionSolComp;
	private String	personaAQuienSeDirige;
	/**
	 * @return the marcaDesc
	 */
	public String getMarcaDesc() {
		return marcaDesc;
	}
	/**
	 * @param marcaDesc the marcaDesc to set
	 */
	public void setMarcaDesc(String marcaDesc) {
		this.marcaDesc = marcaDesc;
	}
	/**
	 * @return the descTipoUso
	 */
	public String getDescTipoUso() {
		return descTipoUso;
	}
	/**
	 * @param descTipoUso the descTipoUso to set
	 */
	public void setDescTipoUso(String descTipoUso) {
		this.descTipoUso = descTipoUso;
	}
	/**
	 * @return the modelo
	 */
	public String getModelo() {
		return modelo;
	}
	/**
	 * @param modelo the modelo to set
	 */
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	/**
	 * @return the placa
	 */
	public String getPlaca() {
		return placa;
	}
	/**
	 * @param placa the placa to set
	 */
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	/**
	 * @return the numeroSerie
	 */
	public String getNumeroSerie() {
		return numeroSerie;
	}
	/**
	 * @param numeroSerie the numeroSerie to set
	 */
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	/**
	 * @return the fechaSiniestro
	 */
	public Date getFechaSiniestro() {
		return fechaSiniestro;
	}
	/**
	 * @param fechaSiniestro the fechaSiniestro to set
	 */
	public void setFechaSiniestro(Date fechaSiniestro) {
		this.fechaSiniestro = fechaSiniestro;
	}
	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/**
	 * @return the agenteMinisterio
	 */
	public String getAgenteMinisterio() {
		return agenteMinisterio;
	}
	/**
	 * @param agenteMinisterio the agenteMinisterio to set
	 */
	public void setAgenteMinisterio(String agenteMinisterio) {
		this.agenteMinisterio = agenteMinisterio;
	}
	/**
	 * @return the nombreUsuario
	 */
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	/**
	 * @param nombreUsuario the nombreUsuario to set
	 */
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	/**
	 * @return the puestoUsuario
	 */
	public String getPuestoUsuario() {
		return puestoUsuario;
	}
	/**
	 * @param puestoUsuario the puestoUsuario to set
	 */
	public void setPuestoUsuario(String puestoUsuario) {
		this.puestoUsuario = puestoUsuario;
	}
	/**
	 * @return the numeroAveriguacionPrevia
	 */
	public String getNumeroAveriguacionPrevia() {
		return numeroAveriguacionPrevia;
	}
	/**
	 * @param numeroAveriguacionPrevia the numeroAveriguacionPrevia to set
	 */
	public void setNumeroAveriguacionPrevia(String numeroAveriguacionPrevia) {
		this.numeroAveriguacionPrevia = numeroAveriguacionPrevia;
	}
	/**
	 * @return the tipoCarta
	 */
	public String getTipoCarta() {
		return tipoCarta;
	}
	/**
	 * @param tipoCarta the tipoCarta to set
	 */
	public void setTipoCarta(String tipoCarta) {
		this.tipoCarta = tipoCarta;
	}
	/**
	 * @return the nombreTipoCarta
	 */
	public String getNombreTipoCarta() {
		return nombreTipoCarta;
	}
	/**
	 * @param nombreTipoCarta the nombreTipoCarta to set
	 */
	public void setNombreTipoCarta(String nombreTipoCarta) {
		this.nombreTipoCarta = nombreTipoCarta;
	}
	/**
	 * @return the descripcionSol
	 */
	public String getDescripcionSol() {
		return descripcionSol;
	}
	/**
	 * @param descripcionSol the descripcionSol to set
	 */
	public void setDescripcionSol(String descripcionSol) {
		this.descripcionSol = descripcionSol;
	}
	/**
	 * @return the descripcionSolComp
	 */
	public String getDescripcionSolComp() {
		return descripcionSolComp;
	}
	/**
	 * @param descripcionSolComp the descripcionSolComp to set
	 */
	public void setDescripcionSolComp(String descripcionSolComp) {
		this.descripcionSolComp = descripcionSolComp;
	}
	/**
	 * @return the personaAQuienSeDirige
	 */
	public String getPersonaAQuienSeDirige() {
		return personaAQuienSeDirige;
	}
	/**
	 * @param personaAQuienSeDirige the personaAQuienSeDirige to set
	 */
	public void setPersonaAQuienSeDirige(String personaAQuienSeDirige) {
		this.personaAQuienSeDirige = personaAQuienSeDirige;
	}

}
