package mx.com.afirme.midas2.dto.siniestros;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class CreacionReporteDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3460061528674088140L;
	
	
	private Short estatus;
	private String descripcionError;
	private Long reporteId;
	private String noReporteSiniestro;
	
	public Short getEstatus() {
		return estatus;
	}

	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}

	public String getDescripcionError() {
		return descripcionError;
	}

	public void setDescripcionError(String descripcionError) {
		this.descripcionError = descripcionError;
	}

	public Long getReporteId() {
		return reporteId;
	}

	public void setReporteId(Long reporteId) {
		this.reporteId = reporteId;
	}

	public String getNoReporteSiniestro() {
		return noReporteSiniestro;
	}

	public void setNoReporteSiniestro(String noReporteSiniestro) {
		this.noReporteSiniestro = noReporteSiniestro;
	}
	
}
