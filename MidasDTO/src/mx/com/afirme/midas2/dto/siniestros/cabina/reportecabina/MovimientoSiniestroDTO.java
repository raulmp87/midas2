package mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class MovimientoSiniestroDTO implements Serializable{

	private static final long serialVersionUID = 5813198174235361399L;
	
	/**Filtros Busqueda**/
	private String usuario;
	private String descripcionMovimiento;
	private String numeroSiniestro;
	private Long idCoberturaReporte;
	private String claveTipoCalculo;
	private String claveSubCalculo;
	private Date fechaFin;
	private Date fechaIni;
	private String tipoMovimiento;
	private String tipoDocumento;
	private String tipoCausaMovimiento;
	private String tipoEstimacion;
	private String descripcion;
	

	private Long idToReporte;
	private Long idMovimiento;
	private Long idEstimacion;
	private Long idOrdenCompra;

	private String folio;
	private String nombreCobertura;
	private String tipoDocumentoDesc;
	private String tipoMovimientoDesc;
	private String causaMovimientoDesc;
	private BigDecimal reserva;
	private BigDecimal importe;
	private Date fechaMovimiento;
	
	
	public MovimientoSiniestroDTO() {
		super();
	}
	
	
	public MovimientoSiniestroDTO(Long idMovimiento, String usuario, String numeroSiniestro,
			String nombreCobertura, String folio, String tipoDocumentoDesc, String tipoMovimientoDesc, String causaMovimientoDesc,
			BigDecimal reserva, BigDecimal importe, Date fechaMovimiento, String descripcion) {
		super();
		this.usuario             = usuario;
		this.numeroSiniestro     = numeroSiniestro;
		this.idMovimiento        = idMovimiento;
		this.nombreCobertura     = nombreCobertura;
		this.folio               = folio;
		this.tipoDocumentoDesc   = tipoDocumentoDesc;
		this.tipoMovimientoDesc  = tipoMovimientoDesc;
		this.causaMovimientoDesc = causaMovimientoDesc;
		this.reserva             = reserva;
		this.importe             = importe;
		this.fechaMovimiento     = fechaMovimiento;
		this.descripcion 		 = descripcion;
	}
	
	public MovimientoSiniestroDTO(Long idMovimiento, String usuario, String numeroSiniestro,
			String tipoDocumentoDesc, String tipoMovimientoDesc, String causaMovimientoDesc,
			BigDecimal importe, Date fechaMovimiento, String descripcion) {
		super();
		this.usuario             = usuario;
		this.numeroSiniestro     = numeroSiniestro;
		this.idMovimiento        = idMovimiento;
		this.tipoDocumentoDesc   = tipoDocumentoDesc;
		this.tipoMovimientoDesc  = tipoMovimientoDesc;
		this.causaMovimientoDesc = causaMovimientoDesc;
		this.importe             = importe;
		this.fechaMovimiento     = fechaMovimiento;
		this.descripcion 		 = descripcion;
	}


	/**
	 * @return the fechaFin
	 */
	public Date getFechaFin() {
		return fechaFin;
	}
	/**
	 * @param fechaFin the fechaFin to set
	 */
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	/**
	 * @return the fechaIni
	 */
	public Date getFechaIni() {
		return fechaIni;
	}
	/**
	 * @param fechaIni the fechaIni to set
	 */
	public void setFechaIni(Date fechaIni) {
		this.fechaIni = fechaIni;
	}
	/**
	 * @return the idCoberturaReporte
	 */
	public Long getIdCoberturaReporte() {
		return idCoberturaReporte;
	}
	/**
	 * @param idCoberturaReporte the idCoberturaReporte to set
	 */
	public void setIdCoberturaReporte(Long idCoberturaReporte) {
		this.idCoberturaReporte = idCoberturaReporte;
	}
	
	/**
	 * @return the tipoEstimacion
	 */
	public String getTipoEstimacion() {
		return tipoEstimacion;
	}
	/**
	 * @param tipoEstimacion the tipoEstimacion to set
	 */
	public void setTipoEstimacion(String tipoEstimacion) {
		this.tipoEstimacion = tipoEstimacion;
	}
	/**
	 * @return the tipoMovimiento
	 */
	public String getTipoMovimiento() {
		return tipoMovimiento;
	}
	/**
	 * @param tipoMovimiento the tipoMovimiento to set
	 */
	public void setTipoMovimiento(String tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}
	/**
	 * @return the tipoMovimientoDesc
	 */
	public String getTipoMovimientoDesc() {
		return tipoMovimientoDesc;
	}
	/**
	 * @param tipoMovimientoDesc the tipoMovimientoDesc to set
	 */
	public void setTipoMovimientoDesc(String tipoMovimientoDesc) {
		this.tipoMovimientoDesc = tipoMovimientoDesc;
	}
	/**
	 * @return the nombreCobertura
	 */
	public String getNombreCobertura() {
		return nombreCobertura;
	}
	/**
	 * @param nombreCobertura the nombreCobertura to set
	 */
	public void setNombreCobertura(String nombreCobertura) {
		this.nombreCobertura = nombreCobertura;
	}

	/**
	 * @return the idToReporte
	 */
	public Long getIdToReporte() {
		return idToReporte;
	}
	/**
	 * @param idToReporte the idToReporte to set
	 */
	public void setIdToReporte(Long idToReporte) {
		this.idToReporte = idToReporte;
	}
	/**
	 * @return the reserva
	 */
	public BigDecimal getReserva() {
		return reserva;
	}
	/**
	 * @param reserva the reserva to set
	 */
	public void setReserva(BigDecimal reserva) {
		this.reserva = reserva;
	}
	/**
	 * @return the idCoberturaTipoEstimacion
	 */
	public String getIdCoberturaTipoEstimacion() {
		return idCoberturaReporte + "-" +tipoEstimacion;
	}
	/**
	 * @param idCoberturaTipoEstimacion the idCoberturaTipoEStimacion to set
	 */
	public void setIdCoberturaTipoEstimacion(String idCoberturaTipoEstimacion) {
		if(idCoberturaTipoEstimacion != null && !idCoberturaTipoEstimacion.isEmpty()){
			String[] params = idCoberturaTipoEstimacion.split("-");
			idCoberturaReporte = Long.parseLong(params[0]);
			tipoEstimacion = params[1];
		}
	}
	
	public String getCausaMovimientoDesc() {
		return causaMovimientoDesc;
	}
	public void setCausaMovimientoDesc(String causaMovimientoDesc) {
		this.causaMovimientoDesc = causaMovimientoDesc;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getDescripcionMovimiento() {
		return descripcionMovimiento;
	}
	public void setDescripcionMovimiento(String descripcionMovimiento) {
		this.descripcionMovimiento = descripcionMovimiento;
	}
	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}
	public String getClaveTipoCalculo() {
		return claveTipoCalculo;
	}
	public void setClaveTipoCalculo(String claveTipoCalculo) {
		this.claveTipoCalculo = claveTipoCalculo;
	}
	public String getClaveSubCalculo() {
		return claveSubCalculo;
	}
	public void setClaveSubCalculo(String claveSubCalculo) {
		this.claveSubCalculo = claveSubCalculo;
	}
	public Long getIdMovimiento() {
		return idMovimiento;
	}
	public void setIdMovimiento(Long idMovimiento) {
		this.idMovimiento = idMovimiento;
	}
	public BigDecimal getImporte() {
		return importe;
	}
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}
	public Date getFechaMovimiento() {
		return fechaMovimiento;
	}
	public void setFechaMovimiento(Date fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public Long getIdEstimacion() {
		return idEstimacion;
	}
	public void setIdEstimacion(Long idEstimacion) {
		this.idEstimacion = idEstimacion;
	}
	public Long getIdOrdenCompra() {
		return idOrdenCompra;
	}
	public void setIdOrdenCompra(Long idOrdenCompra) {
		this.idOrdenCompra = idOrdenCompra;
	}
	public String getTipoCausaMovimiento() {
		return tipoCausaMovimiento;
	}
	public void setTipoCausaMovimiento(String tipoCausaMovimiento) {
		this.tipoCausaMovimiento = tipoCausaMovimiento;
	}
	public String getTipoDocumentoDesc() {
		return tipoDocumentoDesc;
	}
	public void setTipoDocumentoDesc(String tipoDocumentoDesc) {
		this.tipoDocumentoDesc = tipoDocumentoDesc;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}	
	
	
}
