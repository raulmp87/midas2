

/**
 * Objeto de transporte nuevo para mostrar la información proveniente de HGS en la
 * recuperación.
 * 
 * <font color="#0000ff"><b>numOrdenCompra </b></font><b>- La seleccionada en el
 * pop-up de búsqueda</b>
 * <font color="#0000ff"><b>fechaOrdenCompra</b></font><b>- La fecha de creación
 * de la entidad de órden de compra</b>
 * <font color="#0000ff"><b>montoOrdenCompra </b></font><b>- De la sumatoria de
 * los conceptos de la órden de compra. Se obtiene con el método
 * OrdenCompraService.obtenerTotales</b>
 * <font color="#0000ff"><b>fechaOrdenPago - </b></font><b>Fecha de Creación de la
 * Orden de Pago relacionada a la Orden de Compra</b>
 * <font color="#0000ff"><b>montoOrdenPago </b></font><b> De la sumatoria de los
 * conceptos de la órden de pago. Se obtiene con el método PagoSiniestroService.
 * obtenerTotales</b>
 * <font color="#0000ff"><b>factura </b></font><b>- Se obtiene de la Orden de
 * Compra</b>
 * <font color="#0000ff"><b>nombreProveedor </b></font><b>- Se obtiene de la Orden
 * de Compra</b>
 * <font color="#0000ff"><b>telefonoProveedor</b></font><b> - Se obtiene del
 * catálogo de Proveedores (teléfono de contacto)</b>
 * <font color="#0000ff"><b>correoProveedor </b></font><b>- Se obtiene del
 * catálogo de Proveedores.</b>
 * 
 * <font color="#0000ff"><b>numValuacion </b></font><font color="#800080"><b>-
 * </b></font><b>Número de valuación de HGS.</b>
 * <font color="#0000ff"><b>nombreValuador </b></font><font color="#800080"><b>-
 * </b></font><b>Nombre del valuador de HGS.</b>
 * <font color="#0000ff"><b>nombreTaller - </b></font><b> Nombre Proveedor del
 * vale de refacción de HGS.</b>
 * <font color="#0000ff"><b>claveTaller - </b></font><b> Clave Proveedor del vale
 * de refaccion de HGS, que corresponde a un prestador de servicio.</b>
 * 
 * <b>
 * </b><font color="#0000ff"><b>marcaVehiculo </b></font><font color="#800080"><b>-
 * </b></font><b>Se obtiene del Pase de Atención por Daños Materiales o de RC
 * Vehículos.</b>
 * <font color="#0000ff"><b>tipoVehiculo</b></font><font color="#800080"><b> -
 * </b></font><b>Se obtiene del Pase de Atención por Daños Materiales o de RC
 * Vehículos.</b>
 * <font color="#0000ff"><b>modeloVehiculo </b></font><font color="#800080"><b>-
 * </b></font><b>Se obtiene del Pase de Atención por Daños Materiales o de RC
 * Vehículos.</b>
 * <font color="#0000ff"><b>adminRefacciones - </b></font><b>Se obtiene de HGS de
 * los nuevos servicios.</b>
 * <font color="#0000ff"><b>adminRefaccionesId - </b></font><b>Se obtiene de HGS
 * de los nuevos servicios.</b>
 * @author Arturo
 * @version 1.0
 * @created 11-may-2015 05:57:55 p.m.
 */

package mx.com.afirme.midas2.dto.siniestros.recuperacion;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import mx.com.afirme.midas2.annotation.Exportable;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.annotation.MultipleDocumentExportable;

import org.springframework.stereotype.Component;

@Component
public class RecuperacionDTO  implements Serializable {

	private static final long	serialVersionUID	= 3896290770336652162L;
	public static final String EXPORTABLE_LISTADO_REC_CIA_PROGRAMAR = "ListadoRecuperacionCompaniaPrgRefBancaria";
	public static final String EXPORTABLE_LISTADO_REC_CIA_ENTREGACARTAS = "ListadoRecuperacionCompaniaRegEntCartas";
	public static final String EXPORTABLE_LISTADO_REC_CIA_REELABCARTAS = "ListadoRecuperacionCompaniaReElabCartas";
	public static final String EXPORTABLE_LISTADO_REC_CIA_ENVRECCARTAS = "ListadoRecuperacionCompaniaEnvRecCartas";
	
	private Long idRecuperacion;
	private Long numeroRecuperacion;
	private String tipoRecuperacionDesc;
	private String origenRegistroDesc;
	private String nombreOficina;
	private Date fechaRegistro;
	private Long numeroPrestador;
	private String nombrePrestador;
	private String numeroSiniestro;
	private String numeroReporte;
	private Date fechaSiniestro;
	private String medioRecuperacionDesc;
	private BigDecimal monto;
	private Date fechaCancelacion;
	private Date fechaAplicacion;
	private String estatusDesc;
	private String usuario;
	private Long numeroSubasta;
	private String estatusPreventa;
	private Date  fechaAplicacionDe;
	private Date  fechaAplicacionHasta;
	private String estatusCartaCia;
	
	private String adminRefacciones;
	private String codigoUsuarioCreacion;
	private String estatus;
	private String medioRecuperacion;
	private Long oficinaId;
	private String origenRegistro;
	private String ptDocumentada;
	private Recuperacion recuperacion;
	private String tipoRecuperacion;
	private String tipoServicioPoliza;


	private String enSubasta;
	private String esIndemnizado;
	private String estatusCarta;
	private String estatusIngreso;
	private String complemento;
	private String paseAtencion;
	private String folioCarta;
	private String subasta;
	private String folio;
	private String conceptoRecuperacionRefacciones;
	
	private Date fechaIniRegistro;
	private Date fechaFinRegistro;
	
	private Date fechaIniSiniestro;
	private Date fechaFinSiniestro;
	
	private BigDecimal montoFinal;
	private BigDecimal montoInicial;
	
	private Date fechaFinCancelacion;
	private Date fechaIniCancelacion;
	
	private BigDecimal montoProvision;
	private String dua;
	private String motivoCancelacion;
	
	private BigDecimal costoEstimadoCobertura;

	public RecuperacionDTO(){

	}

	public void finalize() throws Throwable {

	}

	public RecuperacionDTO(String adminRefacciones,
			String codigoUsuarioCreacion, String estatus, String estatusDesc,
			Date fechaAplicacion, Date fechaCancelacion,
			Date fechaFinCancelacion, Date fechaFinRegistro,
			Date fechaFinSiniestro, Date fechaIniCancelacion,
			Date fechaIniRegistro, Date fechaIniSiniestro, Date fechaRegistro,
			Date fechaSiniestro, String medioRecuperacion,
			String medioRecuperacionDesc, BigDecimal monto,
			BigDecimal montoFinal, BigDecimal montoInicial,
			String nombreOficina, String nombrePrestador, Long numeroPrestador,
			Long numeroRecuperacion, String numeroReporte,
			String numeroSiniestro, Long numeroSubasta, Long oficinaId,
			String origenRegistro, String origenRegistroDesc,
			String ptDocumentada, Recuperacion recuperacion,
			String tipoRecuperacion, String tipoRecuperacionDesc,
			String tipoServicioPoliza, String usuario) {
		super();
		this.adminRefacciones = adminRefacciones;
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
		this.estatus = estatus;
		this.estatusDesc = estatusDesc;
		this.fechaAplicacion = fechaAplicacion;
		this.fechaCancelacion = fechaCancelacion;
		this.fechaFinCancelacion = fechaFinCancelacion;
		this.fechaFinRegistro = fechaFinRegistro;
		this.fechaFinSiniestro = fechaFinSiniestro;
		this.fechaIniCancelacion = fechaIniCancelacion;
		this.fechaIniRegistro = fechaIniRegistro;
		this.fechaIniSiniestro = fechaIniSiniestro;
		this.fechaRegistro = fechaRegistro;
		this.fechaSiniestro = fechaSiniestro;
		this.medioRecuperacion = medioRecuperacion;
		this.medioRecuperacionDesc = medioRecuperacionDesc;
		this.monto = monto;
		this.montoFinal = montoFinal;
		this.montoInicial = montoInicial;
		this.nombreOficina = nombreOficina;
		this.nombrePrestador = nombrePrestador;
		this.numeroPrestador = numeroPrestador;
		this.numeroRecuperacion = numeroRecuperacion;
		this.numeroReporte = numeroReporte;
		this.numeroSiniestro = numeroSiniestro;
		this.numeroSubasta = numeroSubasta;
		this.oficinaId = oficinaId;
		this.origenRegistro = origenRegistro;
		this.origenRegistroDesc = origenRegistroDesc;
		this.ptDocumentada = ptDocumentada;
		this.recuperacion = recuperacion;
		this.tipoRecuperacion = tipoRecuperacion;
		this.tipoRecuperacionDesc = tipoRecuperacionDesc;
		this.tipoServicioPoliza = tipoServicioPoliza;
		this.usuario = usuario;
	}

	
	public String getAdminRefacciones() {
		return adminRefacciones;
	}

	public void setAdminRefacciones(String adminRefacciones) {
		this.adminRefacciones = adminRefacciones;
	}

	
	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	@MultipleDocumentExportable(value = {
		@Exportable(columnName="Estatus", columnOrder=15),
		@Exportable(columnName = "Estatus", columnOrder = 10, document= EXPORTABLE_LISTADO_REC_CIA_PROGRAMAR)
		}
	)
	public String getEstatusDesc() {
		return estatusDesc;
	}

	
	public void setEstatusDesc(String estatusDesc) {
		this.estatusDesc = estatusDesc;
	}

	@Exportable(columnName="Fecha Aplicación", columnOrder=14)
	public Date getFechaAplicacion() {
		return fechaAplicacion;
	}

	public void setFechaAplicacion(Date fechaAplicacion) {
		this.fechaAplicacion = fechaAplicacion;
	}

	@Exportable(columnName="Fecha Cancelación", columnOrder=13)
	public Date getFechaCancelacion() {
		return fechaCancelacion;
	}

	public void setFechaCancelacion(Date fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}

	public Date getFechaFinCancelacion() {
		return fechaFinCancelacion;
	}

	public void setFechaFinCancelacion(Date fechaFinCancelacion) {
		this.fechaFinCancelacion = fechaFinCancelacion;
	}

	public Date getFechaFinRegistro() {
		return fechaFinRegistro;
	}

	public void setFechaFinRegistro(Date fechaFinRegistro) {
		this.fechaFinRegistro = fechaFinRegistro;
	}

	public Date getFechaFinSiniestro() {
		return fechaFinSiniestro;
	}

	public void setFechaFinSiniestro(Date fechaFinSiniestro) {
		this.fechaFinSiniestro = fechaFinSiniestro;
	}

	public Date getFechaIniCancelacion() {
		return fechaIniCancelacion;
	}

	public void setFechaIniCancelacion(Date fechaIniCancelacion) {
		this.fechaIniCancelacion = fechaIniCancelacion;
	}

	public Date getFechaIniRegistro() {
		return fechaIniRegistro;
	}

	public void setFechaIniRegistro(Date fechaIniRegistro) {
		this.fechaIniRegistro = fechaIniRegistro;
	}

	public Date getFechaIniSiniestro() {
		return fechaIniSiniestro;
	}

	public void setFechaIniSiniestro(Date fechaIniSiniestro) {
		this.fechaIniSiniestro = fechaIniSiniestro;
	}

	@MultipleDocumentExportable(value = {
			@Exportable(columnName="Fecha Registro", columnOrder=5),
			@Exportable(columnName = "Fecha Registro", columnOrder = 2, format="dd/MM/yyyy", document= EXPORTABLE_LISTADO_REC_CIA_PROGRAMAR),
			@Exportable(columnName = "Fecha Registro", columnOrder = 2, format="dd/MM/yyyy", document= EXPORTABLE_LISTADO_REC_CIA_ENTREGACARTAS),
			@Exportable(columnName = "Fecha Registro", columnOrder = 2, format="dd/MM/yyyy", document= EXPORTABLE_LISTADO_REC_CIA_REELABCARTAS),
			@Exportable(columnName = "Fecha Registro", columnOrder = 2, format="dd/MM/yyyy", document= EXPORTABLE_LISTADO_REC_CIA_ENVRECCARTAS)
			}
		)
	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	@Exportable(columnName="Fecha Siniestro", columnOrder=10)
	public Date getFechaSiniestro() {
		return fechaSiniestro;
	}

	public void setFechaSiniestro(Date fechaSiniestro) {
		this.fechaSiniestro = fechaSiniestro;
	}

	public String getMedioRecuperacion() {
		return medioRecuperacion;
	}

	public void setMedioRecuperacion(String medioRecuperacion) {
		this.medioRecuperacion = medioRecuperacion;
	}

	@Exportable(columnName="Medio", columnOrder=11)
	public String getMedioRecuperacionDesc() {
		return medioRecuperacionDesc;
	}

	public void setMedioRecuperacionDesc(String medioRecuperacionDesc) {
		this.medioRecuperacionDesc = medioRecuperacionDesc;
	}

	@MultipleDocumentExportable(value = {
			@Exportable(columnName="Monto", format="$#,##0.00", columnOrder=12),
			@Exportable(columnName = "Monto", columnOrder = 11, format="$#,##0.00", document= EXPORTABLE_LISTADO_REC_CIA_PROGRAMAR),
			@Exportable(columnName = "Monto", columnOrder = 9, format="$#,##0.00", document= EXPORTABLE_LISTADO_REC_CIA_ENTREGACARTAS),
			@Exportable(columnName = "Monto", columnOrder = 10, format="$#,##0.00", document= EXPORTABLE_LISTADO_REC_CIA_REELABCARTAS),
			@Exportable(columnName = "Monto", columnOrder = 10, format="$#,##0.00", document= EXPORTABLE_LISTADO_REC_CIA_ENVRECCARTAS)
				}
			)
	public BigDecimal getMonto() {
		return monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	public BigDecimal getMontoFinal() {
		return montoFinal;
	}

	public void setMontoFinal(BigDecimal montoFinal) {
		this.montoFinal = montoFinal;
	}

	public BigDecimal getMontoInicial() {
		return montoInicial;
	}

	public void setMontoInicial(BigDecimal montoInicial) {
		this.montoInicial = montoInicial;
	}

	@MultipleDocumentExportable(value = {
			@Exportable(columnName="Oficina", columnOrder=4),
			@Exportable(columnName = "Oficina", columnOrder = 3, document= EXPORTABLE_LISTADO_REC_CIA_PROGRAMAR),
			@Exportable(columnName = "Oficina", columnOrder = 3, document= EXPORTABLE_LISTADO_REC_CIA_ENTREGACARTAS),
			@Exportable(columnName = "Oficina", columnOrder = 3, document= EXPORTABLE_LISTADO_REC_CIA_REELABCARTAS),
			@Exportable(columnName = "Oficina Siniestro", columnOrder = 4, document= EXPORTABLE_LISTADO_REC_CIA_ENVRECCARTAS)			
				}
			)
	public String getNombreOficina() {
		return nombreOficina;
	}

	public void setNombreOficina(String nombreOficina) {
		this.nombreOficina = nombreOficina;
	}

	@Exportable(columnName="Proveedor/Comprador/Compania Aseguradora", columnOrder=7)
	public String getNombrePrestador() {
		return nombrePrestador;
	}

	public void setNombrePrestador(String nombrePrestador) {
		this.nombrePrestador = nombrePrestador;
	}

	@Exportable(columnName="No. Proveedor", columnOrder=6)
	public Long getNumeroPrestador() {
		return numeroPrestador;
	}

	public void setNumeroPrestador(Long numeroPrestador) {
		this.numeroPrestador = numeroPrestador;
	}

	@MultipleDocumentExportable(value = {
			@Exportable(columnName="Número Recuperación", columnOrder=1),
			@Exportable(columnName = "Número Recuperación", columnOrder = 0, document= EXPORTABLE_LISTADO_REC_CIA_PROGRAMAR),
			@Exportable(columnName = "Número Recuperación", columnOrder = 0, document= EXPORTABLE_LISTADO_REC_CIA_ENTREGACARTAS),
			@Exportable(columnName = "Número Recuperación", columnOrder = 0, document= EXPORTABLE_LISTADO_REC_CIA_REELABCARTAS),
			@Exportable(columnName = "Número Recuperación", columnOrder = 0, document= EXPORTABLE_LISTADO_REC_CIA_ENVRECCARTAS)
				}
			)
	public Long getNumeroRecuperacion() {
		return numeroRecuperacion;
	}

	public void setNumeroRecuperacion(Long numeroRecuperacion) {
		this.numeroRecuperacion = numeroRecuperacion;
	}

	@Exportable(columnName="No. Reporte", columnOrder=9)
	public String getNumeroReporte() {
		return numeroReporte;
	}

	
	public void setNumeroReporte(String numeroReporte) {
		this.numeroReporte = numeroReporte;
	}

	@MultipleDocumentExportable(value = {
			@Exportable(columnName="No. Siniestro", columnOrder=8),
			@Exportable(columnName = "No. Siniestro", columnOrder = 7, document= EXPORTABLE_LISTADO_REC_CIA_PROGRAMAR),
			@Exportable(columnName = "No. Siniestro", columnOrder = 7, document= EXPORTABLE_LISTADO_REC_CIA_ENTREGACARTAS),
			@Exportable(columnName = "No. Siniestro", columnOrder = 7, document= EXPORTABLE_LISTADO_REC_CIA_REELABCARTAS),
			@Exportable(columnName = "No. Siniestro", columnOrder = 8, document= EXPORTABLE_LISTADO_REC_CIA_ENVRECCARTAS)
				}
			)
	
	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}

	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}

	public Long getNumeroSubasta() {
		return numeroSubasta;
	}

	public void setNumeroSubasta(Long numeroSubasta) {
		this.numeroSubasta = numeroSubasta;
	}

	public Long getOficinaId() {
		return oficinaId;
	}

	public void setOficinaId(Long oficinaId) {
		this.oficinaId = oficinaId;
	}

	public String getOrigenRegistro() {
		return origenRegistro;
	}

	public void setOrigenRegistro(String origenRegistro) {
		this.origenRegistro = origenRegistro;
	}

	@Exportable(columnName="Origen", columnOrder=3)
	public String getOrigenRegistroDesc() {
		return origenRegistroDesc;
	}

	public void setOrigenRegistroDesc(String origenRegistroDesc) {
		this.origenRegistroDesc = origenRegistroDesc;
	}

	@Exportable(columnName="PT Documentada", columnOrder=17)
	public String getPtDocumentada() {
		return ptDocumentada;
	}

	public void setPtDocumentada(String ptDocumentada) {
		this.ptDocumentada = ptDocumentada;
	}

	public Recuperacion getRecuperacion() {
		return recuperacion;
	}

	public void setRecuperacion(Recuperacion recuperacion) {
		this.recuperacion = recuperacion;
	}

	public String getTipoRecuperacion() {
		return tipoRecuperacion;
	}

	public void setTipoRecuperacion(String tipoRecuperacion) {
		this.tipoRecuperacion = tipoRecuperacion;
	}

	@Exportable(columnName="Tipo", columnOrder=2)
	public String getTipoRecuperacionDesc() {
		return tipoRecuperacionDesc;
	}

	public void setTipoRecuperacionDesc(String tipoRecuperacionDesc) {
		this.tipoRecuperacionDesc = tipoRecuperacionDesc;
	}

	@MultipleDocumentExportable(value = {
			@Exportable(columnName = "Tipo de Servicio", columnOrder= 24 ),
			@Exportable(columnName = "Tipo de Servicio", columnOrder = 4, document= EXPORTABLE_LISTADO_REC_CIA_PROGRAMAR),
			@Exportable(columnName = "Tipo de Servicio", columnOrder = 4, document= EXPORTABLE_LISTADO_REC_CIA_ENTREGACARTAS),
			@Exportable(columnName = "Tipo de Servicio", columnOrder = 4, document= EXPORTABLE_LISTADO_REC_CIA_REELABCARTAS),
			@Exportable(columnName = "Tipo de Servicio", columnOrder = 5, document= EXPORTABLE_LISTADO_REC_CIA_ENVRECCARTAS)
				}
			)
	public String getTipoServicioPoliza() {
		return tipoServicioPoliza;
	}

	public void setTipoServicioPoliza(String tipoServicioPoliza) {
		this.tipoServicioPoliza = tipoServicioPoliza;
	}

	@MultipleDocumentExportable(value = {
			@Exportable(columnName="Usuario", columnOrder=16),
			@Exportable(columnName = "Usuario", columnOrder = 8, document= EXPORTABLE_LISTADO_REC_CIA_PROGRAMAR),
			@Exportable(columnName = "Usuario", columnOrder = 8, document= EXPORTABLE_LISTADO_REC_CIA_ENTREGACARTAS),
			@Exportable(columnName = "Usuario", columnOrder = 8, document= EXPORTABLE_LISTADO_REC_CIA_REELABCARTAS),
			@Exportable(columnName = "Usuario", columnOrder = 9, document= EXPORTABLE_LISTADO_REC_CIA_ENVRECCARTAS)
				}
			)
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Long getIdRecuperacion() {
		return idRecuperacion;
	}

	public void setIdRecuperacion(Long idRecuperacion) {
		this.idRecuperacion = idRecuperacion;
	}

	
	public String getEstatusPreventa() {
		return estatusPreventa;
	}

	public void setEstatusPreventa(String estatusPreventa) {
		this.estatusPreventa = estatusPreventa;
	}

	public RecuperacionDTO(Long idRecuperacion, Long numeroRecuperacion,
			String tipoRecuperacionDesc, String origenRegistroDesc,
			String nombreOficina, Date fechaRegistro, Long numeroPrestador,
			String nombrePrestador, String numeroSiniestro,
			String numeroReporte, Date fechaSiniestro,
			String medioRecuperacionDesc, BigDecimal monto,
			Date fechaCancelacion, Date fechaAplicacion, String estatusDesc,
			String usuario, Long numeroSubasta, String adminRefacciones,
			String codigoUsuarioCreacion, String estatus,
			String medioRecuperacion, Long oficinaId, String origenRegistro,
			String ptDocumentada, Recuperacion recuperacion,
			String tipoRecuperacion, String tipoServicioPoliza,
			Date fechaIniRegistro, Date fechaFinRegistro,
			Date fechaIniSiniestro, Date fechaFinSiniestro,
			BigDecimal montoFinal, BigDecimal montoInicial,
			Date fechaFinCancelacion, Date fechaIniCancelacion) {
		super();
		this.idRecuperacion = idRecuperacion;
		this.numeroRecuperacion = numeroRecuperacion;
		this.tipoRecuperacionDesc = tipoRecuperacionDesc;
		this.origenRegistroDesc = origenRegistroDesc;
		this.nombreOficina = nombreOficina;
		this.fechaRegistro = fechaRegistro;
		this.numeroPrestador = numeroPrestador;
		this.nombrePrestador = nombrePrestador;
		this.numeroSiniestro = numeroSiniestro;
		this.numeroReporte = numeroReporte;
		this.fechaSiniestro = fechaSiniestro;
		this.medioRecuperacionDesc = medioRecuperacionDesc;
		this.monto = monto;
		this.fechaCancelacion = fechaCancelacion;
		this.fechaAplicacion = fechaAplicacion;
		this.estatusDesc = estatusDesc;
		this.usuario = usuario;
		this.numeroSubasta = numeroSubasta;
		this.adminRefacciones = adminRefacciones;
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
		this.estatus = estatus;
		this.medioRecuperacion = medioRecuperacion;
		this.oficinaId = oficinaId;
		this.origenRegistro = origenRegistro;
		this.ptDocumentada = ptDocumentada;
		this.recuperacion = recuperacion;
		this.tipoRecuperacion = tipoRecuperacion;
		this.tipoServicioPoliza = tipoServicioPoliza;
		this.fechaIniRegistro = fechaIniRegistro;
		this.fechaFinRegistro = fechaFinRegistro;
		this.fechaIniSiniestro = fechaIniSiniestro;
		this.fechaFinSiniestro = fechaFinSiniestro;
		this.montoFinal = montoFinal;
		this.montoInicial = montoInicial;
		this.fechaFinCancelacion = fechaFinCancelacion;
		this.fechaIniCancelacion = fechaIniCancelacion;
	}


	public String getEstatusCarta() {
		return estatusCarta;
	}

	public void setEstatusCarta(String estatusCarta) {
		this.estatusCarta = estatusCarta;
	}

	public String getEstatusIngreso() {
		return estatusIngreso;
	}

	public void setEstatusIngreso(String estatusIngreso) {
		this.estatusIngreso = estatusIngreso;
	}

	@Exportable(columnName="Complemento Pendiente", columnOrder=25)
	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	@Exportable(columnName="Pase de Atención", columnOrder=22)
	public String getPaseAtencion() {
		return paseAtencion;
	}

	public void setPaseAtencion(String paseAtencion) {
		this.paseAtencion = paseAtencion;
	}

	@Exportable(columnName="En Subasta", columnOrder=26)
	public String getEnSubasta() {
		return enSubasta;
	}

	public void setEnSubasta(String enSubasta) {
		this.enSubasta = enSubasta;
	}

	@Exportable(columnName="Indemnizada", columnOrder=20)
	public String getEsIndemnizado() {
		return esIndemnizado;
	}

	public void setEsIndemnizado(String esIndemnizado) {
		this.esIndemnizado = esIndemnizado;
	}

	public Date getFechaAplicacionDe() {
		return fechaAplicacionDe;
	}

	public void setFechaAplicacionDe(Date fechaAplicacionDe) {
		this.fechaAplicacionDe = fechaAplicacionDe;
	}

	public Date getFechaAplicacionHasta() {
		return fechaAplicacionHasta;
	}

	public void setFechaAplicacionHasta(Date fechaAplicacionHasta) {
		this.fechaAplicacionHasta = fechaAplicacionHasta;
	}

	@Exportable(columnName="Estatus de la Carta", columnOrder=19)
	public String getEstatusCartaCia() {
		return estatusCartaCia;
	}

	public void setEstatusCartaCia(String estatusCartaCia) {
		this.estatusCartaCia = estatusCartaCia;
	}
	
	@Exportable(columnName="Folios Venta", columnOrder=18)
	public String getFolioCarta() {
		return folioCarta;
	}

	public void setFolioCarta(String folioCarta) {
		this.folioCarta = folioCarta;
	}

	@Exportable(columnName="No. Subasta", columnOrder=21)
	public String getSubasta() {
		return subasta;
	}

	public void setSubasta(String subasta) {
		this.subasta = subasta;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	@Exportable(columnName="Concepto de Recuperación", columnOrder=23)
	public String getConceptoRecuperacionRefacciones() {
		return conceptoRecuperacionRefacciones;
	}

	public void setConceptoRecuperacionRefacciones(
			String conceptoRecuperacionRefacciones) {
		this.conceptoRecuperacionRefacciones = conceptoRecuperacionRefacciones;
	}
	
	@Exportable(columnName="Monto de provisión", columnOrder=27, format="$#,##0.00")
	public BigDecimal getMontoProvision() {
		return montoProvision;
	}
	
	public void setMontoProvision(BigDecimal montoProvision) {
		this.montoProvision = montoProvision;
	}
	
	@Exportable(columnName="DUA", columnOrder=28)
	public String getdua() {
		return dua;
	}
	
	public void setdua(String dua) {
		this.dua = dua;
	}
	
	@Exportable(columnName="Motivo de cancelación", columnOrder=30)
	public String getmotivoCancelacion() {
		return motivoCancelacion;
	}
	
	public void setmotivoCancelacion(String motivoCancelacion) {
		this.motivoCancelacion = motivoCancelacion;
	}

	@Exportable(columnName="Costo estimado de cobertura", columnOrder=29)
	public BigDecimal getCostoEstimadoCobertura() {
		return costoEstimadoCobertura;
	}

	public void setCostoEstimadoCobertura(BigDecimal costoEstimadoCobertura) {
		this.costoEstimadoCobertura = costoEstimadoCobertura;
	}
	
	
	

}
