package mx.com.afirme.midas2.dto.siniestros.solicitudcheque;

import java.io.Serializable;

public class GeneraChequeDTO implements Serializable{ /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long idsolcheque;
	public Long getIdsolcheque() {
		return idsolcheque;
	}
	public void setIdsolcheque(Long idsolcheque) {
		this.idsolcheque = idsolcheque;
	}
	
	
}
