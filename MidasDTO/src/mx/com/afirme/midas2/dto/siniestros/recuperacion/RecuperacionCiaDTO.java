package mx.com.afirme.midas2.dto.siniestros.recuperacion;

import java.util.Date;

import mx.com.afirme.midas2.annotation.Exportable;
import mx.com.afirme.midas2.annotation.MultipleDocumentExportable;

import org.springframework.stereotype.Component;

@Component
public class RecuperacionCiaDTO extends RecuperacionDTO{


	private static final long	serialVersionUID	= 7625293368653834656L;
	private String estatusCarta;
	private String referenciaBancaria;
	private String folioCarta;
	private String numeroCia;
	private String compania;
	private Boolean seleccionado;
	private Long oficinaReasignacionId;
	private String oficinaReasignacion;
	private Long oficinaRecepcionId;
	private String oficinaRecepcion;
	private Date fechaEnvio;
	private Date fechaRecepcion;
	private String usuarioEnvio;
	private String usuarioRecepcion;
	private Date fechaImpresion;
	
	private Date fechaSiniestro;
	private String edoOcurrencia;
	private String tipoSiniestro;
	private String cobertura;
	private String nombreLesionado;
	private String vehiculoTercero;
	private String observaciones;
	private Date fechaAcuse;
	private Date fechaUltimoAcuse;
	
	private Long reporteCabinaId;
	
	private Short tipoModulo;
	
	public String getEstatusCarta() {
		return estatusCarta;
	}
	public void setEstatusCarta(String estatusCarta) {
		this.estatusCarta = estatusCarta;
	}
	
	@MultipleDocumentExportable(value = {
			@Exportable(columnName = "Referencia Bancaria", columnOrder = 9, document= EXPORTABLE_LISTADO_REC_CIA_PROGRAMAR)
				}
			)
	public String getReferenciaBancaria() {
		return referenciaBancaria;
	}
	public void setReferenciaBancaria(String referenciaBancaria) {
		this.referenciaBancaria = referenciaBancaria;
	}
	
	@MultipleDocumentExportable(value = {
				@Exportable(columnName = "Folio Carta", columnOrder = 1, document= EXPORTABLE_LISTADO_REC_CIA_PROGRAMAR),
				@Exportable(columnName = "Folio Carta", columnOrder = 1, document= EXPORTABLE_LISTADO_REC_CIA_ENTREGACARTAS),
				@Exportable(columnName = "Folio Carta", columnOrder = 1, document= EXPORTABLE_LISTADO_REC_CIA_REELABCARTAS),
				@Exportable(columnName = "Folio Carta", columnOrder = 1, document= EXPORTABLE_LISTADO_REC_CIA_ENVRECCARTAS)
				}
			)
	public String getFolioCarta() {
		return folioCarta;
	}
	public void setFolioCarta(String folioCarta) {
		this.folioCarta = folioCarta;
	}
	
	@MultipleDocumentExportable(value = {
			@Exportable(columnName = "No. Compañía", columnOrder = 5, document= EXPORTABLE_LISTADO_REC_CIA_PROGRAMAR),
			@Exportable(columnName = "No. Compañía", columnOrder = 5, document= EXPORTABLE_LISTADO_REC_CIA_ENTREGACARTAS),
			@Exportable(columnName = "No. Compañía", columnOrder = 5, document= EXPORTABLE_LISTADO_REC_CIA_REELABCARTAS),
			@Exportable(columnName = "No. Compañía", columnOrder = 6, document= EXPORTABLE_LISTADO_REC_CIA_ENVRECCARTAS)
				}
			)
	public String getNumeroCia() {
		return numeroCia;
	}
	public void setNumeroCia(String numeroCia) {
		this.numeroCia = numeroCia;
	}
	
	@MultipleDocumentExportable(value = {
			@Exportable(columnName = "Compañía", columnOrder = 6, document= EXPORTABLE_LISTADO_REC_CIA_PROGRAMAR),
			@Exportable(columnName = "Compañía", columnOrder = 6, document= EXPORTABLE_LISTADO_REC_CIA_ENTREGACARTAS),
			@Exportable(columnName = "Compañía", columnOrder = 6, document= EXPORTABLE_LISTADO_REC_CIA_REELABCARTAS),
			@Exportable(columnName = "Compañía", columnOrder = 7, document= EXPORTABLE_LISTADO_REC_CIA_ENVRECCARTAS)
				}
			)
	public String getCompania() {
		return compania;
	}
	public void setCompania(String compania) {
		this.compania = compania;
	}
	public Boolean getSeleccionado() {
		return seleccionado;
	}
	public void setSeleccionado(Boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	public Long getOficinaReasignacionId() {
		return oficinaReasignacionId;
	}
	public void setOficinaReasignacionId(Long oficinaReasignacionId) {
		this.oficinaReasignacionId = oficinaReasignacionId;
	}
	
	@Exportable(columnName = "Oficina Reasignación", columnOrder = 9, document= EXPORTABLE_LISTADO_REC_CIA_REELABCARTAS)
	public String getOficinaReasignacion() {
		return oficinaReasignacion;
	}
	public void setOficinaReasignacion(String oficinaReasignacion) {
		this.oficinaReasignacion = oficinaReasignacion;
	}
	public Long getOficinaRecepcionId() {
		return oficinaRecepcionId;
	}
	public void setOficinaRecepcionId(Long oficinaRecepcionId) {
		this.oficinaRecepcionId = oficinaRecepcionId;
	}
	
	@MultipleDocumentExportable(value = {
			@Exportable(columnName = "Oficina Destino", columnOrder = 12, document= EXPORTABLE_LISTADO_REC_CIA_ENVRECCARTAS)
				}
			)
	public String getOficinaRecepcion() {
		return oficinaRecepcion;
	}
	public void setOficinaRecepcion(String oficinaRecepcion) {
		this.oficinaRecepcion = oficinaRecepcion;
	}

	@MultipleDocumentExportable(value = {
			@Exportable(columnName = "Fecha Envío", columnOrder = 11, document= EXPORTABLE_LISTADO_REC_CIA_ENVRECCARTAS)
				}
			)
	public Date getFechaEnvio() {
		return fechaEnvio;
	}
	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}
	
	@MultipleDocumentExportable(value = {
			@Exportable(columnName = "Fecha Recepción", columnOrder = 13, document= EXPORTABLE_LISTADO_REC_CIA_ENVRECCARTAS)
				}
			)
	public Date getFechaRecepcion() {
		return fechaRecepcion;
	}
	public void setFechaRecepcion(Date fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}
	public String getUsuarioEnvio() {
		return usuarioEnvio;
	}
	public void setUsuarioEnvio(String usuarioEnvio) {
		this.usuarioEnvio = usuarioEnvio;
	}
	public String getUsuarioRecepcion() {
		return usuarioRecepcion;
	}
	public void setUsuarioRecepcion(String usuarioRecepcion) {
		this.usuarioRecepcion = usuarioRecepcion;
	}
	
	@MultipleDocumentExportable(value = {
			@Exportable(columnName = "Fecha Impresión", columnOrder = 3, document= EXPORTABLE_LISTADO_REC_CIA_ENVRECCARTAS)
				}
			)
	public Date getFechaImpresion() {
		return fechaImpresion;
	}
	public void setFechaImpresion(Date fechaImpresion) {
		this.fechaImpresion = fechaImpresion;
	}
		
	
	@MultipleDocumentExportable(value = {
			@Exportable(columnName = "Estado Ocurrencia", columnOrder = 11, document= EXPORTABLE_LISTADO_REC_CIA_ENTREGACARTAS)
				}
			)
	public String getEdoOcurrencia() {
		return edoOcurrencia;
	}
	public void setEdoOcurrencia(String edoOcurrencia) {
		this.edoOcurrencia = edoOcurrencia;
	}
	
	@MultipleDocumentExportable(value = {
			@Exportable(columnName = "Tipo Siniestro", columnOrder = 12, document= EXPORTABLE_LISTADO_REC_CIA_ENTREGACARTAS)
				}
			)
	public String getTipoSiniestro() {
		return tipoSiniestro;
	}
	
	public void setTipoSiniestro(String tipoSiniestro) {
		this.tipoSiniestro = tipoSiniestro;
	}
	
	@MultipleDocumentExportable(value = {
			@Exportable(columnName = "Cobertura", columnOrder = 13, document= EXPORTABLE_LISTADO_REC_CIA_ENTREGACARTAS)
				}
			)
	public String getCobertura() {
		return cobertura;
	}
	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}
	
	@MultipleDocumentExportable(value = {
			@Exportable(columnName = "Nombre Lesionado", columnOrder = 14, document= EXPORTABLE_LISTADO_REC_CIA_ENTREGACARTAS)
				}
			)
	public String getNombreLesionado() {
		return nombreLesionado;
	}
	public void setNombreLesionado(String nombreLesionado) {
		this.nombreLesionado = nombreLesionado;
	}
	
	@MultipleDocumentExportable(value = {
			@Exportable(columnName = "Vehiculo Tercero", columnOrder = 15, document= EXPORTABLE_LISTADO_REC_CIA_ENTREGACARTAS)
				}
			)
	public String getVehiculoTercero() {
		return vehiculoTercero;
	}
	
	public void setVehiculoTercero(String vehiculoTercero) {
		this.vehiculoTercero = vehiculoTercero;
	}
	
	@MultipleDocumentExportable(value = {
			@Exportable(columnName = "Observaciones", columnOrder = 16, document= EXPORTABLE_LISTADO_REC_CIA_ENTREGACARTAS)
				}
			)
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	@MultipleDocumentExportable(value = {
			@Exportable(columnName = "Fecha Acuse", columnOrder = 18, document= EXPORTABLE_LISTADO_REC_CIA_ENTREGACARTAS)
				}
			)
	public Date getFechaAcuse() {
		return fechaAcuse;
	}
	public void setFechaAcuse(Date fechaAcuse) {
		this.fechaAcuse = fechaAcuse;
	}
	
	@MultipleDocumentExportable(value = {
			@Exportable(columnName = "Fecha Ult Acuse", columnOrder = 17, document= EXPORTABLE_LISTADO_REC_CIA_ENTREGACARTAS)
				}
			)
	public Date getFechaUltimoAcuse() {
		return fechaUltimoAcuse;
	}
	public void setFechaUltimoAcuse(Date fechaUltimoAcuse) {
		this.fechaUltimoAcuse = fechaUltimoAcuse;
	}
	
	@MultipleDocumentExportable(value = {
			@Exportable(columnName = "Fecha Siniestro", columnOrder = 10, document= EXPORTABLE_LISTADO_REC_CIA_ENTREGACARTAS)
				}
			)
	public Date getFechaSiniestro() {
		return fechaSiniestro;
	}
	public void setFechaSiniestro(Date fechaSiniestro) {
		this.fechaSiniestro = fechaSiniestro;
	}
	public Long getReporteCabinaId() {
		return reporteCabinaId;
	}
	public void setReporteCabinaId(Long reporteCabinaId) {
		this.reporteCabinaId = reporteCabinaId;
	}
	public Short getTipoModulo() {
		return tipoModulo;
	}
	public void setTipoModulo(Short tipoModulo) {
		this.tipoModulo = tipoModulo;
	}
		
}
