package mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Israel
 * @version 1.0
 * @created 31-mar.-2015 12:43:35 p. m.
 */
public class FacturaSiniestroRegistro implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1335316432653157350L;
	
	private Date fechaFactura;
	private Long idFactura;
	private Long idProveedor;
	private String noFactura;
	private String nombreProveedor;

	public FacturaSiniestroRegistro(){

	}
	
	

	public FacturaSiniestroRegistro(Long idFactura, String noFactura, Date fechaFactura,
			Long idProveedor, String nombreProveedor) {
		
		this.fechaFactura = fechaFactura;
		this.idFactura = idFactura;
		this.idProveedor = idProveedor;
		this.noFactura = noFactura;
		this.nombreProveedor = nombreProveedor;
	}



	public Date getFechaFactura() {
		return fechaFactura;
	}

	public void setFechaFactura(Date fechaFactura) {
		this.fechaFactura = fechaFactura;
	}

	public Long getIdFactura() {
		return idFactura;
	}

	public void setIdFactura(Long idFactura) {
		this.idFactura = idFactura;
	}

	public Long getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(Long idProveedor) {
		this.idProveedor = idProveedor;
	}

	public String getNoFactura() {
		return noFactura;
	}

	public void setNoFactura(String noFactura) {
		this.noFactura = noFactura;
	}

	public String getNombreProveedor() {
		return nombreProveedor;
	}

	public void setNombreProveedor(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}

}