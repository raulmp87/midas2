/**
 * 
 */
package mx.com.afirme.midas2.dto.siniestros;

import org.springframework.stereotype.Component;

/**
 * @author admin
 *
 */
@Component
public class DatosPersonalizacionDTO {
	
	private String cilindros;
	
	private int puertas;
	
	private String tipoAlarma;
	
	private String tipoVehiculo;
	
	private String transmision;
	
	private String descripcionEstilo;

	public String getCilindros() {
		return cilindros;
	}

	public void setCilindros(String cilindros) {
		this.cilindros = cilindros;
	}

	public int getPuertas() {
		return puertas;
	}

	public void setPuertas(int puertas) {
		this.puertas = puertas;
	}

	public String getTipoAlarma() {
		return tipoAlarma;
	}

	public void setTipoAlarma(String tipoAlarma) {
		this.tipoAlarma = tipoAlarma;
	}

	public String getTipoVehiculo() {
		return tipoVehiculo;
	}

	public void setTipoVehiculo(String tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}

	public String getTransmision() {
		return transmision;
	}

	public void setTransmision(String transmision) {
		this.transmision = transmision;
	}
	
	
	

}
