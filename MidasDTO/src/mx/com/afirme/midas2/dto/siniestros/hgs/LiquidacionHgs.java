package mx.com.afirme.midas2.dto.siniestros.hgs;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class LiquidacionHgs implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@XmlElement(name="idLiquidacion",required=true)
	private Long idLiquidacion;


	public Long getIdLiquidacion() {
		return idLiquidacion;
	}


	public void setIdLiquidacion(Long idLiquidacion) {
		this.idLiquidacion = idLiquidacion;
	}


	@Override
	public String toString() {
		return "Liquidacion [idLiquidacion=" + idLiquidacion + "]";
	}
	
	

}
