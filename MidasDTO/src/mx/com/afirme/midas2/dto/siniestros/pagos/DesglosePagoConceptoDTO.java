package mx.com.afirme.midas2.dto.siniestros.pagos;


import java.io.Serializable;
import java.math.BigDecimal;

import mx.com.afirme.midas2.domain.siniestros.catalogo.conceptos.ConceptoAjuste;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
public class DesglosePagoConceptoDTO implements Serializable {	
	private static final long serialVersionUID = 8920501488479157659L;
	private Long idDetalleOrdenPago;
	private Long idDetalleOrdenCompra;
	private ConceptoAjuste conceptoAjuste;			
	private String estatus;	
	private BigDecimal importe;
	private BigDecimal importePagado;	
	private String observaciones;	
	private OrdenCompra ordenCompra;	
	private Long idOrdenCompra;
	private BigDecimal cantidad;	
	private String coberturaDescripcion;	
	private String conceptoPago;	
	private BigDecimal pendiente;	
	private String tipoOrden ;	
	
	//Cantidades por detalle.
	private BigDecimal iva;
	private BigDecimal porcIva;	
	private BigDecimal subtotal;//Por detalle
	private BigDecimal costoUnitario;//Por detalle
	private BigDecimal total;//Por detalle
	private BigDecimal totalPagado;//Por detalle
	private BigDecimal totalPendiente;//Por detalle
	private String estatusPago;//Por detalle
	private String pago;//Por detalle
	private BigDecimal porIva;//Por detalle
	private BigDecimal ivaRetenido;//Por detalle
	private BigDecimal porIvaRetenido;//Por detalle
	private BigDecimal isr;//Por detalle
	private BigDecimal porIsr;	//Por detalle
	private BigDecimal totalPagar;//Por detalle
	private BigDecimal salvamento;//Por detalle
	private BigDecimal totalSalvamento;//Por detalle
	private BigDecimal salvamentoPorIva;//Por detalle
	
	
//	SecciÃ³n Desglose Total a Pagar
	private String aplicaDeducible;
	private BigDecimal pagos;
	private String estatusPagos;
	
	//TOTALES POR ORDEN DE PAGO
	private BigDecimal deduciblesTotal;
	private BigDecimal descuentosTotal;
	private BigDecimal reserva;
	private BigDecimal reservaDisponible;
	private BigDecimal  totalesPagados;
	private BigDecimal  subTotalPorPagar;
	private BigDecimal  totalSinDescuentosPorPagar;//subTotalPorPagar+ivaTotalPorPagar-isrTotalPorPagar-ivaRetenidoTotalPorPagar
	private BigDecimal  totalesPorPagar;//totalPacialPorPagar-deduciblesTotal-descuentosTotal
	private BigDecimal  ivaTotalPorPagar;
	private BigDecimal  isrTotalPorPagar;
	private BigDecimal  ivaRetenidoTotalPorPagar;
	private BigDecimal  salvamentoSubtotalPorPagar;
	private BigDecimal  salvamentoTotalPorPagar;
	
	public String getPago() {
		return pago;
	}
	public void setPago(String pago) {
		this.pago = pago;
	}
	public BigDecimal getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}
	public BigDecimal getIva() {
		return iva;
	}
	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}
	
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public BigDecimal getTotalPagado() {
		return totalPagado;
	}
	public void setTotalPagado(BigDecimal totalPagado) {
		this.totalPagado = totalPagado;
	}
	public BigDecimal getTotalPendiente() {
		return totalPendiente;
	}
	public void setTotalPendiente(BigDecimal totalPendiente) {
		this.totalPendiente = totalPendiente;
	}
	
	
	
	
	
	
	
	
	
	
	
	public BigDecimal getPorIva() {
		return porIva;
	}
	public void setPorIva(BigDecimal porIva) {
		this.porIva = porIva;
	}
	public BigDecimal getIvaRetenido() {
		return ivaRetenido;
	}
	public void setIvaRetenido(BigDecimal ivaRetenido) {
		this.ivaRetenido = ivaRetenido;
	}
	public BigDecimal getPorIvaRetenido() {
		return porIvaRetenido;
	}
	public void setPorIvaRetenido(BigDecimal porIvaRetenido) {
		this.porIvaRetenido = porIvaRetenido;
	}
	public BigDecimal getIsr() {
		return isr;
	}
	public void setIsr(BigDecimal isr) {
		this.isr = isr;
	}
	public BigDecimal getPorIsr() {
		return porIsr;
	}
	public void setPorIsr(BigDecimal porIsr) {
		this.porIsr = porIsr;
	}
	
	public BigDecimal getTotalPagar() {
		return totalPagar;
	}
	public void setTotalPagar(BigDecimal totalPagar) {
		this.totalPagar = totalPagar;
	}
	public BigDecimal getSalvamento() {
		return salvamento;
	}
	public void setSalvamento(BigDecimal salvamento) {
		this.salvamento = salvamento;
	}
	public BigDecimal getSalvamentoPorIva() {
		return salvamentoPorIva;
	}
	public void setSalvamentoPorIva(BigDecimal salvamentoPorIva) {
		this.salvamentoPorIva = salvamentoPorIva;
	}
	public String getAplicaDeducible() {
		return aplicaDeducible;
	}
	public void setAplicaDeducible(String aplicaDeducible) {
		this.aplicaDeducible = aplicaDeducible;
	}
	public BigDecimal getDeduciblesTotal() {
		return deduciblesTotal;
	}
	public void setDeduciblesTotal(BigDecimal deduciblesTotal) {
		this.deduciblesTotal = deduciblesTotal;
	}
	public BigDecimal getTotalesPorPagar() {
		return totalesPorPagar;
	}
	public void setTotalesPorPagar(BigDecimal totalesPorPagar) {
		this.totalesPorPagar = totalesPorPagar;
	}
	public BigDecimal getTotalesPagados() {
		return totalesPagados;
	}
	public void setTotalesPagados(BigDecimal totalesPagados) {
		this.totalesPagados = totalesPagados;
	}
	public BigDecimal getPagos() {
		return pagos;
	}
	public void setPagos(BigDecimal pagos) {
		this.pagos = pagos;
	}
	public BigDecimal getReserva() {
		return reserva;
	}
	public void setReserva(BigDecimal reserva) {
		this.reserva = reserva;
	}
	
	public Long getIdDetalleOrdenPago() {
		return idDetalleOrdenPago;
	}
	public void setIdDetalleOrdenPago(Long idDetalleOrdenPago) {
		this.idDetalleOrdenPago = idDetalleOrdenPago;
	}
	public Long getIdDetalleOrdenCompra() {
		return idDetalleOrdenCompra;
	}
	public void setIdDetalleOrdenCompra(Long idDetalleOrdenCompra) {
		this.idDetalleOrdenCompra = idDetalleOrdenCompra;
	}
	public ConceptoAjuste getConceptoAjuste() {
		return conceptoAjuste;
	}
	public void setConceptoAjuste(ConceptoAjuste conceptoAjuste) {
		this.conceptoAjuste = conceptoAjuste;
	}
	public BigDecimal getCostoUnitario() {
		return costoUnitario;
	}
	public void setCostoUnitario(BigDecimal costoUnitario) {
		this.costoUnitario = costoUnitario;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public BigDecimal getImporte() {
		return importe;
	}
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}
	public BigDecimal getImportePagado() {
		return importePagado;
	}
	public void setImportePagado(BigDecimal importePagado) {
		this.importePagado = importePagado;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public OrdenCompra getOrdenCompra() {
		return ordenCompra;
	}
	public void setOrdenCompra(OrdenCompra ordenCompra) {
		this.ordenCompra = ordenCompra;
	}
	public BigDecimal getPorcIva() {
		return porcIva;
	}
	public void setPorcIva(BigDecimal porcIva) {
		this.porcIva = porcIva;
	}
	public Long getIdOrdenCompra() {
		return idOrdenCompra;
	}
	public void setIdOrdenCompra(Long idOrdenCompra) {
		this.idOrdenCompra = idOrdenCompra;
	}
	public BigDecimal getCantidad() {
		return cantidad;
	}
	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}
	public String getCoberturaDescripcion() {
		return coberturaDescripcion;
	}
	public void setCoberturaDescripcion(String coberturaDescripcion) {
		this.coberturaDescripcion = coberturaDescripcion;
	}
	public String getConceptoPago() {
		return conceptoPago;
	}
	public void setConceptoPago(String conceptoPago) {
		this.conceptoPago = conceptoPago;
	}
	public BigDecimal getPendiente() {
		return pendiente;
	}
	public void setPendiente(BigDecimal pendiente) {
		this.pendiente = pendiente;
	}
	public String getTipoOrden() {
		return tipoOrden;
	}
	public void setTipoOrden(String tipoOrden) {
		this.tipoOrden = tipoOrden;
	}
	public BigDecimal getTotalSalvamento() {
		return totalSalvamento;
	}
	public void setTotalSalvamento(BigDecimal totalSalvamento) {
		this.totalSalvamento = totalSalvamento;
	}
	public String getEstatusPago() {
		return estatusPago;
	}
	public void setEstatusPago(String estatusPago) {
		this.estatusPago = estatusPago;
	}
	public String getEstatusPagos() {
		return estatusPagos;
	}
	public void setEstatusPagos(String estatusPagos) {
		this.estatusPagos = estatusPagos;
	}
	public BigDecimal getReservaDisponible() {
		return reservaDisponible;
	}
	public void setReservaDisponible(BigDecimal reservaDisponible) {
		this.reservaDisponible = reservaDisponible;
	}
	public BigDecimal getDescuentosTotal() {
		return descuentosTotal;
	}
	public void setDescuentosTotal(BigDecimal descuentosTotal) {
		this.descuentosTotal = descuentosTotal;
	}
	public BigDecimal getIvaTotalPorPagar() {
		return ivaTotalPorPagar;
	}
	public void setIvaTotalPorPagar(BigDecimal ivaTotalPorPagar) {
		this.ivaTotalPorPagar = ivaTotalPorPagar;
	}
	public BigDecimal getIsrTotalPorPagar() {
		return isrTotalPorPagar;
	}
	public void setIsrTotalPorPagar(BigDecimal isrTotalPorPagar) {
		this.isrTotalPorPagar = isrTotalPorPagar;
	}
	public BigDecimal getIvaRetenidoTotalPorPagar() {
		return ivaRetenidoTotalPorPagar;
	}
	public void setIvaRetenidoTotalPorPagar(BigDecimal ivaRetenidoTotalPorPagar) {
		this.ivaRetenidoTotalPorPagar = ivaRetenidoTotalPorPagar;
	}
	public BigDecimal getSalvamentoTotalPorPagar() {
		return salvamentoTotalPorPagar;
	}
	public void setSalvamentoTotalPorPagar(BigDecimal salvamentoTotalPorPagar) {
		this.salvamentoTotalPorPagar = salvamentoTotalPorPagar;
	}
	public BigDecimal getSubTotalPorPagar() {
		return subTotalPorPagar;
	}
	public void setSubTotalPorPagar(BigDecimal subTotalPorPagar) {
		this.subTotalPorPagar = subTotalPorPagar;
	}
	public BigDecimal getSalvamentoSubtotalPorPagar() {
		return salvamentoSubtotalPorPagar;
	}
	public void setSalvamentoSubtotalPorPagar(BigDecimal salvamentoSubtotalPorPagar) {
		this.salvamentoSubtotalPorPagar = salvamentoSubtotalPorPagar;
	}
	public BigDecimal getTotalSinDescuentosPorPagar() {
		return totalSinDescuentosPorPagar;
	}
	public void setTotalSinDescuentosPorPagar(BigDecimal totalSinDescuentosPorPagar) {
		this.totalSinDescuentosPorPagar = totalSinDescuentosPorPagar;
	}
	

	


}