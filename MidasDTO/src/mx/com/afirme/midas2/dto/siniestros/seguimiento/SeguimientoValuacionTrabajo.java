package mx.com.afirme.midas2.dto.siniestros.seguimiento;

import java.io.Serializable;


/**
 * Objeto para transportar los trabajos pendientes y realizados registrados en HGS.
 * 
 * @author Arturo
 * @version 1.0
 * @created 13-oct-2014 11:41:43 a.m.
 */
public class SeguimientoValuacionTrabajo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2298627206400220065L;
	private String concepto;
	private String descripcion;
	private String fechaAvance;
	private int realizado;

	public SeguimientoValuacionTrabajo(){

	}

	public void finalize() throws Throwable {

	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getFechaAvance() {
		return fechaAvance;
	}

	public void setFechaAvance(String fechaAvance) {
		this.fechaAvance = fechaAvance;
	}

	public int getRealizado() {
		return realizado;
	}

	public void setRealizado(int realizado) {
		this.realizado = realizado;
	}

	
}