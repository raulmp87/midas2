package mx.com.afirme.midas2.dto.siniestros;

import java.util.Date;

public class ReciboProgramaPagoDTO {
	
	private Date fechaFinCobertura;
	private Date fechaFinProrroga;
	private Date fechaInicioCobertura;
	private Date fechaVencimiento;
	private Long folio;
	private Integer numeroRecibo;
	private String situacion;
	
	/**
	 * @return the fechaFinCobertura
	 */
	public Date getFechaFinCobertura() {
		return fechaFinCobertura;
	}
	/**
	 * @param fechaFinCobertura the fechaFinCobertura to set
	 */
	public void setFechaFinCobertura(Date fechaFinCobertura) {
		this.fechaFinCobertura = fechaFinCobertura;
	}
	/**
	 * @return the fechaFinProrroga
	 */
	public Date getFechaFinProrroga() {
		return fechaFinProrroga;
	}
	/**
	 * @param fechaFinProrroga the fechaFinProrroga to set
	 */
	public void setFechaFinProrroga(Date fechaFinProrroga) {
		this.fechaFinProrroga = fechaFinProrroga;
	}
	/**
	 * @return the fechaInicioCobertura
	 */
	public Date getFechaInicioCobertura() {
		return fechaInicioCobertura;
	}
	/**
	 * @param fechaInicioCobertura the fechaInicioCobertura to set
	 */
	public void setFechaInicioCobertura(Date fechaInicioCobertura) {
		this.fechaInicioCobertura = fechaInicioCobertura;
	}
	/**
	 * @return the fechaVencimiento
	 */
	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}
	/**
	 * @param fechaVencimiento the fechaVencimiento to set
	 */
	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	/**
	 * @return the folio
	 */
	public Long getFolio() {
		return folio;
	}
	/**
	 * @param folio the folio to set
	 */
	public void setFolio(Long folio) {
		this.folio = folio;
	}
	/**
	 * @return the numeroRecibo
	 */
	public Integer getNumeroRecibo() {
		return numeroRecibo;
	}
	/**
	 * @param numeroRecibo the numeroRecibo to set
	 */
	public void setNumeroRecibo(Integer numeroRecibo) {
		this.numeroRecibo = numeroRecibo;
	}
	/**
	 * @return the situacion
	 */
	public String getSituacion() {
		return situacion;
	}
	/**
	 * @param situacion the situacion to set
	 */
	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}
	
}
