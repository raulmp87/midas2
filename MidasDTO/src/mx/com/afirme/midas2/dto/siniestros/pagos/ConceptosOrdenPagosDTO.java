package mx.com.afirme.midas2.dto.siniestros.pagos;


import java.io.Serializable;
import java.math.BigDecimal;
public class ConceptosOrdenPagosDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6642498757205469418L;
	private String tipoPago;//	 (Solo Lectura)
	private String conceptoPago;
	private BigDecimal subtotal;
	private BigDecimal iva;
	private BigDecimal porIva;
	private BigDecimal total;
	public String getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	public String getConceptoPago() {
		return conceptoPago;
	}
	public void setConceptoPago(String conceptoPago) {
		this.conceptoPago = conceptoPago;
	}
	public BigDecimal getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}
	public BigDecimal getIva() {
		return iva;
	}
	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}
	public BigDecimal getPorIva() {
		return porIva;
	}
	public void setPorIva(BigDecimal porIva) {
		this.porIva = porIva;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}

}