package mx.com.afirme.midas2.dto.siniestros.recuperacion;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class InstructivoDepositoListadoBancosDTO implements Serializable{
	
	private static final long serialVersionUID = -5579133621749978143L;

	private String banco;
	private String numeroCuenta;
	private String claveInterbancaria;
	/**
	 * @return the banco
	 */
	public String getBanco() {
		return banco;
	}
	/**
	 * @param banco the banco to set
	 */
	public void setBanco(String banco) {
		this.banco = banco;
	}
	/**
	 * @return the numeroCuenta
	 */
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	/**
	 * @param numeroCuenta the numeroCuenta to set
	 */
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	/**
	 * @return the claveInterbancaria
	 */
	public String getClaveInterbancaria() {
		return claveInterbancaria;
	}
	/**
	 * @param claveInterbancaria the claveInterbancaria to set
	 */
	public void setClaveInterbancaria(String claveInterbancaria) {
		this.claveInterbancaria = claveInterbancaria;
	}
	
}
