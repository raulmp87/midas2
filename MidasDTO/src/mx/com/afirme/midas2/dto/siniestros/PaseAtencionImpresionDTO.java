package mx.com.afirme.midas2.dto.siniestros;

import java.io.Serializable;
import java.util.Date;

public class PaseAtencionImpresionDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String numeroPoliza;
	private String numeroSiniestro;
	private String numeroReporte;
	private String folio;
	private String claveAjustador;
	private String nombreAjustador;
	private String nombrePrestador;
	private String domicilioPrestador;
	private String telefonoPrestador;
	private Boolean tieneCondicionesEsp;
	private String nombreAfectado;
	private String domicilioAfectado;
	private String telefonoAfectado;
	private String celularAfectado;
	private String observaciones;
	private String daniosPreexistentes;
	private Date fechaExpedicion;
	private Integer numeroInciso;
	private String nombreCobertura;
	private Integer numeroLesionados;
	private Integer totalLesionados;
	private String nombreContratante;
	private Double limiteMontoPaciente;
	private Integer edad;
	private String consecuenciaAccidente;
	private Date fechaOcurrido;
	private String fechaOcurridoStr;
	private String lugarExpedicion;
	private String tipoPrestador;
	private String marcaVehiculo;
	private String tipoVehiculo;
	private String transmision;
	private String placas;
	private String color;
	private String numeroSerie;
	private Boolean aplicaDeducible;
	private String tipoCalculo;
	private String porcentajeDeducible;
	private Double valorComercial;
	private Boolean tieneEquipoEspecial;
	private String porcentajeDeducibleEE;
	private String valorComercialEE;
	private String daniosEncubiertos;
	private String tipoRiesgo;
	private short tipoImpresion;
	private String daniosMecanicos;
	private String daniosInteriores;
	private String bastiadores;
	private String tituloImpresion;
	private String tituloPaseAtencion;
	private String modeloVehiculo;
	private String tieneCondicionesEspStr;
	private String aplicaDeducibleStr;
	private String asegurado;
	private String tieneEquipoEspecialStr;
	private String marcaImpresion;
	private String clausulasAplicables;
	private String tipoMovimiento;
	
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}
	public String getNumeroReporte() {
		return numeroReporte;
	}
	public void setNumeroReporte(String numeroReporte) {
		this.numeroReporte = numeroReporte;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public String getClaveAjustador() {
		return claveAjustador;
	}
	public void setClaveAjustador(String claveAjustador) {
		this.claveAjustador = claveAjustador;
	}
	public String getNombreAjustador() {
		return nombreAjustador;
	}
	public void setNombreAjustador(String nombreAjustador) {
		this.nombreAjustador = nombreAjustador;
	}
	public String getNombrePrestador() {
		return nombrePrestador;
	}
	public void setNombrePrestador(String nombrePrestador) {
		this.nombrePrestador = nombrePrestador;
	}
	public String getDomicilioPrestador() {
		return domicilioPrestador;
	}
	public void setDomicilioPrestador(String domicilioPrestador) {
		this.domicilioPrestador = domicilioPrestador;
	}
	public String getTelefonoPrestador() {
		return telefonoPrestador;
	}
	public void setTelefonoPrestador(String telefonoPrestador) {
		this.telefonoPrestador = telefonoPrestador;
	}
	public Boolean getTieneCondicionesEsp() {
		return tieneCondicionesEsp;
	}
	public void setTieneCondicionesEsp(Boolean tieneCondicionesEsp) {
		this.tieneCondicionesEsp = tieneCondicionesEsp;
	}
	public String getNombreAfectado() {
		return nombreAfectado;
	}
	public void setNombreAfectado(String nombreAfectado) {
		this.nombreAfectado = nombreAfectado;
	}
	public String getDomicilioAfectado() {
		return domicilioAfectado;
	}
	public void setDomicilioAfectado(String domicilioAfectado) {
		this.domicilioAfectado = domicilioAfectado;
	}
	public String getTelefonoAfectado() {
		return telefonoAfectado;
	}
	public void setTelefonoAfectado(String telefonoAfectado) {
		this.telefonoAfectado = telefonoAfectado;
	}
	public String getCelularAfectado() {
		return celularAfectado;
	}
	public void setCelularAfectado(String celularAfectado) {
		this.celularAfectado = celularAfectado;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public String getDaniosPreexistentes() {
		return daniosPreexistentes;
	}
	public void setDaniosPreexistentes(String daniosPreexistentes) {
		this.daniosPreexistentes = daniosPreexistentes;
	}
	public Date getFechaExpedicion() {
		return fechaExpedicion;
	}
	public void setFechaExpedicion(Date fechaExpedicion) {
		this.fechaExpedicion = fechaExpedicion;
	}
	public Integer getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public String getNombreCobertura() {
		return nombreCobertura;
	}
	public void setNombreCobertura(String nombreCobertura) {
		this.nombreCobertura = nombreCobertura;
	}
	public Integer getNumeroLesionados() {
		return numeroLesionados;
	}
	public void setNumeroLesionados(Integer numeroLesionados) {
		this.numeroLesionados = numeroLesionados;
	}
	public Integer getTotalLesionados() {
		return totalLesionados;
	}
	public void setTotalLesionados(Integer totalLesionados) {
		this.totalLesionados = totalLesionados;
	}
	public String getNombreContratante() {
		return nombreContratante;
	}
	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}
	public Double getLimiteMontoPaciente() {
		return limiteMontoPaciente;
	}
	public void setLimiteMontoPaciente(Double limiteMontoPaciente) {
		this.limiteMontoPaciente = limiteMontoPaciente;
	}
	public Integer getEdad() {
		return edad;
	}
	public void setEdad(Integer edad) {
		this.edad = edad;
	}
	public String getConsecuenciaAccidente() {
		return consecuenciaAccidente;
	}
	public void setConsecuenciaAccidente(String consecuenciaAccidente) {
		this.consecuenciaAccidente = consecuenciaAccidente;
	}
	public Date getFechaOcurrido() {
		return fechaOcurrido;
	}
	public void setFechaOcurrido(Date fechaOcurrido) {
		this.fechaOcurrido = fechaOcurrido;
	}
	/**
	 * @param fechaOcurridoStr the fechaOcurridoStr to set
	 */
	public void setFechaOcurridoStr(String fechaOcurridoStr) {
		this.fechaOcurridoStr = fechaOcurridoStr;
	}
	/**
	 * @return the fechaOcurridoStr
	 */
	public String getFechaOcurridoStr() {
		return fechaOcurridoStr;
	}
	public String getLugarExpedicion() {
		return lugarExpedicion;
	}
	public void setLugarExpedicion(String lugarExpedicion) {
		this.lugarExpedicion = lugarExpedicion;
	}
	public String getTipoPrestador() {
		return tipoPrestador;
	}
	public void setTipoPrestador(String tipoPrestador) {
		this.tipoPrestador = tipoPrestador;
	}
	public String getMarcaVehiculo() {
		return marcaVehiculo;
	}
	public void setMarcaVehiculo(String marcaVehiculo) {
		this.marcaVehiculo = marcaVehiculo;
	}
	public String getTipoVehiculo() {
		return tipoVehiculo;
	}
	public void setTipoVehiculo(String tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}
	public String getTransmision() {
		return transmision;
	}
	public void setTransmision(String transmision) {
		this.transmision = transmision;
	}
	public String getPlacas() {
		return placas;
	}
	public void setPlacas(String placas) {
		this.placas = placas;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getNumeroSerie() {
		return numeroSerie;
	}
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	public Boolean getAplicaDeducible() {
		return aplicaDeducible;
	}
	public void setAplicaDeducible(Boolean aplicaDeducible) {
		this.aplicaDeducible = aplicaDeducible;
	}
	public String getTipoCalculo() {
		return tipoCalculo;
	}
	public void setTipoCalculo(String tipoCalculo) {
		this.tipoCalculo = tipoCalculo;
	}
	public String getPorcentajeDeducible() {
		return porcentajeDeducible;
	}
	public void setPorcentajeDeducible(String porcentajeDeducible) {
		this.porcentajeDeducible = porcentajeDeducible;
	}
	public Double getValorComercial() {
		return valorComercial;
	}
	public void setValorComercial(Double valorComercial) {
		this.valorComercial = valorComercial;
	}
	public Boolean getTieneEquipoEspecial() {
		return tieneEquipoEspecial;
	}
	public void setTieneEquipoEspecial(Boolean tieneEquipoEspecial) {
		this.tieneEquipoEspecial = tieneEquipoEspecial;
	}
	public String getPorcentajeDeducibleEE() {
		return porcentajeDeducibleEE;
	}
	public void setPorcentajeDeducibleEE(String porcentajeDeducibleEE) {
		this.porcentajeDeducibleEE = porcentajeDeducibleEE;
	}
	public String getValorComercialEE() {
		return valorComercialEE;
	}
	public void setValorComercialEE(String valorComercialEE) {
		this.valorComercialEE = valorComercialEE;
	}
	public String getDaniosEncubiertos() {
		return daniosEncubiertos;
	}
	public void setDaniosEncubiertos(String daniosEncubiertos) {
		this.daniosEncubiertos = daniosEncubiertos;
	}
	public String getTipoRiesgo() {
		return tipoRiesgo;
	}
	public void setTipoRiesgo(String tipoRiesgo) {
		this.tipoRiesgo = tipoRiesgo;
	}
	public short getTipoImpresion() {
		return tipoImpresion;
	}
	public void setTipoImpresion(short tipoImpresion) {
		this.tipoImpresion = tipoImpresion;
	}
	public String getDaniosMecanicos() {
		return daniosMecanicos;
	}
	public void setDaniosMecanicos(String daniosMecanicos) {
		this.daniosMecanicos = daniosMecanicos;
	}
	public String getDaniosInteriores() {
		return daniosInteriores;
	}
	public void setDaniosInteriores(String daniosInteriores) {
		this.daniosInteriores = daniosInteriores;
	}
	public String getBastiadores() {
		return bastiadores;
	}
	public void setBastiadores(String bastiadores) {
		this.bastiadores = bastiadores;
	}
	public String getTituloImpresion() {
		return tituloImpresion;
	}
	public void setTituloImpresion(String tituloImpresion) {
		this.tituloImpresion = tituloImpresion;
	}
	public String getModeloVehiculo() {
		return modeloVehiculo;
	}
	public void setModeloVehiculo(String modeloVehiculo) {
		this.modeloVehiculo = modeloVehiculo;
	}
	public String getTieneCondicionesEspStr() {
		return tieneCondicionesEspStr;
	}
	public void setTieneCondicionesEspStr(String tieneCondicionesEspStr) {
		this.tieneCondicionesEspStr = tieneCondicionesEspStr;
	}
	public String getAplicaDeducibleStr() {
		return aplicaDeducibleStr;
	}
	public void setAplicaDeducibleStr(String aplicaDeducibleStr) {
		this.aplicaDeducibleStr = aplicaDeducibleStr;
	}
	/**
	 * @return the asegurado
	 */
	public String getAsegurado() {
		return asegurado;
	}
	/**
	 * @param asegurado the asegurado to set
	 */
	public void setAsegurado(String asegurado) {
		this.asegurado = asegurado;
	}
	public String getTieneEquipoEspecialStr() {
		return tieneEquipoEspecialStr;
	}
	public void setTieneEquipoEspecialStr(String tieneEquipoEspecialStr) {
		this.tieneEquipoEspecialStr = tieneEquipoEspecialStr;
	}
	public String getTituloPaseAtencion() {
		return tituloPaseAtencion;
	}
	public void setTituloPaseAtencion(String tituloPaseAtencion) {
		this.tituloPaseAtencion = tituloPaseAtencion;
	}
	public String getMarcaImpresion() {
		return marcaImpresion;
	}
	public void setMarcaImpresion(String marcaImpresion) {
		this.marcaImpresion = marcaImpresion;
	}
	public String getClausulasAplicables() {
		return clausulasAplicables;
	}
	public void setClausulasAplicables(String clausulasAplicables) {
		this.clausulasAplicables = clausulasAplicables;
	}
	/**
	 * @return the tipoMovimiento
	 */
	public String getTipoMovimiento() {
		return tipoMovimiento;
	}
	/**
	 * @param tipoMovimiento the tipoMovimiento to set
	 */
	public void setTipoMovimiento(String tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

}