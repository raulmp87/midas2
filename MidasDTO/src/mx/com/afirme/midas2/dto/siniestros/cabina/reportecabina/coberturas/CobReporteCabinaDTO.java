package mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.coberturas;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="VCOBREPORTECABINA", schema = "MIDAS")
public class CobReporteCabinaDTO implements mx.com.afirme.midas2.dao.catalogos.Entidad {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID")
	private long id;
	
	@Column(name="REPORTECABINA_ID")
	private BigDecimal idReporteCabina;
	
	
	@Column(name="COBERTURA_ID")
	private BigDecimal idCobertura;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public BigDecimal getIdReporteCabina() {
		return idReporteCabina;
	}

	public void setIdReporteCabina(BigDecimal idReporteCabina) {
		this.idReporteCabina = idReporteCabina;
	}

	public BigDecimal getIdCobertura() {
		return idCobertura;
	}

	public void setIdCobertura(BigDecimal idCobertura) {
		this.idCobertura = idCobertura;
	}

	@Override
	public <K> K getKey() {
		return null;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}

}
