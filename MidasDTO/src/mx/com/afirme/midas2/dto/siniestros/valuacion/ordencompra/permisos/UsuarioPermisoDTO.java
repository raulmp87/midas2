package mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.permisos;

import java.io.Serializable;

public class UsuarioPermisoDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long idUsuarioPermiso;
	
	private Long idOrdenCompraPermiso;
	
	private Long idUsuario;
	
	private String nombreUsuario;
	
	private String nombre;

	/**
	 * @return the idUsuarioPermiso
	 */
	public Long getIdUsuarioPermiso() {
		return idUsuarioPermiso;
	}

	/**
	 * @param idUsuarioPermiso the idUsuarioPermiso to set
	 */
	public void setIdUsuarioPermiso(Long idUsuarioPermiso) {
		this.idUsuarioPermiso = idUsuarioPermiso;
	}

	/**
	 * @return the idUsuario
	 */
	public Long getIdUsuario() {
		return idUsuario;
	}

	/**
	 * @param idUsuario the idUsuario to set
	 */
	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	/**
	 * @return the nombreUsuario
	 */
	public String getNombreUsuario() {
		return nombreUsuario;
	}

	/**
	 * @param nombreUsuario the nombreUsuario to set
	 */
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the idOrdenCompraPermiso
	 */
	public Long getIdOrdenCompraPermiso() {
		return idOrdenCompraPermiso;
	}

	/**
	 * @param idOrdenCompraPermiso the idOrdenCompraPermiso to set
	 */
	public void setIdOrdenCompraPermiso(Long idOrdenCompraPermiso) {
		this.idOrdenCompraPermiso = idOrdenCompraPermiso;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsuarioPermisoDTO other = (UsuarioPermisoDTO) obj;
		if (nombreUsuario == null) {
			if (other.nombreUsuario != null)
				return false;
		} else if (!nombreUsuario.equals(other.nombreUsuario))
			return false;
		return true;
	}
	
	
	

}
