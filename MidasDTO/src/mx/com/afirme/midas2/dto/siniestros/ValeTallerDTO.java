package mx.com.afirme.midas2.dto.siniestros;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.springframework.stereotype.Component;



/**
 * @author simavera
 */
@Component
public class ValeTallerDTO implements Serializable{

	private static final long serialVersionUID = 6428610778851475851L;
	private Long idTercero;
	private String dia;
	private String mes;
	private String anio;
	private String hora;
	private String amPm;
	private String ubicacion;
	private Short tipoVehiculo;
	private String numeroPlacas;
	private String tipoCarroceria;
	private String tipoServicio;
	private String recogerEn;
	private String entregarEn;
	private String con;
	private BigDecimal importe;
	private String numeroPoliza;
	private Integer numeroInciso;
	private String numeroSiniestro;
	private String nombreAsegurado;
	private String marcaVehiculo;
	private Integer modelo;
	private String tipoVehiculoDesc;
	private String color;
	private String nombreGrua;
	private String nombreAjustador;
	private String claveAjustador;
	

	public ValeTallerDTO(){

	}

	/**
	 * @return the idTercero
	 */
	public Long getIdTercero() {
		return idTercero;
	}

	/**
	 * @param idTercero the idTercero to set
	 */
	public void setIdTercero(Long idTercero) {
		this.idTercero = idTercero;
	}

	/**
	 * @return the dia
	 */
	public String getDia() {
		return dia;
	}

	/**
	 * @param dia the dia to set
	 */
	public void setDia(String dia) {
		this.dia = dia;
	}

	/**
	 * @return the mes
	 */
	public String getMes() {
		return mes;
	}

	/**
	 * @param mes the mes to set
	 */
	public void setMes(String mes) {
		this.mes = mes;
	}

	/**
	 * @return the anio
	 */
	public String getAnio() {
		return anio;
	}

	/**
	 * @param anio the anio to set
	 */
	public void setAnio(String anio) {
		this.anio = anio;
	}

	/**
	 * @return the hora
	 */
	public String getHora() {
		return hora;
	}

	/**
	 * @param hora the hora to set
	 */
	public void setHora(String hora) {
		this.hora = hora;
	}

	/**
	 * @return the amPm
	 */
	public String getAmPm() {
		return amPm;
	}

	/**
	 * @param amPm the amPm to set
	 */
	public void setAmPm(String amPm) {
		this.amPm = amPm;
	}

	/**
	 * @return the ubicacion
	 */
	public String getUbicacion() {
		return ubicacion;
	}

	/**
	 * @param ubicacion the ubicacion to set
	 */
	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	/**
	 * @return the tipoVehiculo
	 */
	public Short getTipoVehiculo() {
		return tipoVehiculo;
	}

	/**
	 * @param tipoVehiculo the tipoVehiculo to set
	 */
	public void setTipoVehiculo(Short tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}

	/**
	 * @return the numeroPlacas
	 */
	public String getNumeroPlacas() {
		return numeroPlacas;
	}

	/**
	 * @param numeroPlacas the numeroPlacas to set
	 */
	public void setNumeroPlacas(String numeroPlacas) {
		this.numeroPlacas = numeroPlacas;
	}

	/**
	 * @return the tipoCarroceria
	 */
	public String getTipoCarroceria() {
		return tipoCarroceria;
	}

	/**
	 * @param tipoCarroceria the tipoCarroceria to set
	 */
	public void setTipoCarroceria(String tipoCarroceria) {
		this.tipoCarroceria = tipoCarroceria;
	}

	/**
	 * @return the tipoServicio
	 */
	public String getTipoServicio() {
		return tipoServicio;
	}

	/**
	 * @param tipoServicio the tipoServicio to set
	 */
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

	/**
	 * @return the recogerEn
	 */
	public String getRecogerEn() {
		return recogerEn;
	}

	/**
	 * @param recogerEn the recogerEn to set
	 */
	public void setRecogerEn(String recogerEn) {
		this.recogerEn = recogerEn;
	}

	/**
	 * @return the entregarEn
	 */
	public String getEntregarEn() {
		return entregarEn;
	}

	/**
	 * @param entregarEn the entregarEn to set
	 */
	public void setEntregarEn(String entregarEn) {
		this.entregarEn = entregarEn;
	}

	/**
	 * @return the con
	 */
	public String getCon() {
		return con;
	}

	/**
	 * @param con the con to set
	 */
	public void setCon(String con) {
		this.con = con;
	}

	/**
	 * @return the importe
	 */
	public BigDecimal getImporte() {
		return importe;
	}

	/**
	 * @param importe the importe to set
	 */
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	/**
	 * @return the numeroPoliza
	 */
	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	/**
	 * @param numeroPoliza the numeroPoliza to set
	 */
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	/**
	 * @return the numeroInciso
	 */
	public Integer getNumeroInciso() {
		return numeroInciso;
	}

	/**
	 * @param numeroInciso the numeroInciso to set
	 */
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	/**
	 * @return the numeroSiniestro
	 */
	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}

	/**
	 * @param numeroSiniestro the numeroSiniestro to set
	 */
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}

	/**
	 * @return the nombreAsegurado
	 */
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}

	/**
	 * @param nombreAsegurado the nombreAsegurado to set
	 */
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}

	/**
	 * @return the marcaVehiculo
	 */
	public String getMarcaVehiculo() {
		return marcaVehiculo;
	}

	/**
	 * @param marcaVehiculo the marcaVehiculo to set
	 */
	public void setMarcaVehiculo(String marcaVehiculo) {
		this.marcaVehiculo = marcaVehiculo;
	}

	/**
	 * @return the modelo
	 */
	public Integer getModelo() {
		return modelo;
	}

	/**
	 * @param modelo the modelo to set
	 */
	public void setModelo(Integer modelo) {
		this.modelo = modelo;
	}

	/**
	 * @return the tipoVehiculoDesc
	 */
	public String getTipoVehiculoDesc() {
		return tipoVehiculoDesc;
	}

	/**
	 * @param tipoVehiculoDesc the tipoVehiculoDesc to set
	 */
	public void setTipoVehiculoDesc(String tipoVehiculoDesc) {
		this.tipoVehiculoDesc = tipoVehiculoDesc;
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @return the nombreGrua
	 */
	public String getNombreGrua() {
		return nombreGrua;
	}

	/**
	 * @param nombreGrua the nombreGrua to set
	 */
	public void setNombreGrua(String nombreGrua) {
		this.nombreGrua = nombreGrua;
	}

	/**
	 * @return the nombreAjustador
	 */
	public String getNombreAjustador() {
		return nombreAjustador;
	}

	/**
	 * @param nombreAjustador the nombreAjustador to set
	 */
	public void setNombreAjustador(String nombreAjustador) {
		this.nombreAjustador = nombreAjustador;
	}

	/**
	 * @return the claveAjustador
	 */
	public String getClaveAjustador() {
		return claveAjustador;
	}

	/**
	 * @param claveAjustador the claveAjustador to set
	 */
	public void setClaveAjustador(String claveAjustador) {
		this.claveAjustador = claveAjustador;
	}

}