package mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class ConceptoAjusteDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private Long id;
	

	private String nombre;


	private String tipoConcepto;
	

	private Date fechaCambioEstatus;
	

	private short estatus;
	

	private Short categoria;	
	
	

	private BigDecimal porcIva;
	
	

	private BigDecimal porcIsr;
	
	

	private BigDecimal porcIvaRetenido;
	
	
	
	private Boolean aplicaIva ;
    

	private Boolean aplicaIvaRetenido;
	
	private Boolean aplicaIsr ;
	
	
	private Boolean aplicaPorcentajes ;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipoConcepto() {
		return tipoConcepto;
	}

	public void setTipoConcepto(String tipoConcepto) {
		this.tipoConcepto = tipoConcepto;
	}

	public Date getFechaCambioEstatus() {
		return fechaCambioEstatus;
	}

	public void setFechaCambioEstatus(Date fechaCambioEstatus) {
		this.fechaCambioEstatus = fechaCambioEstatus;
	}

	public short getEstatus() {
		return estatus;
	}

	public void setEstatus(short estatus) {
		this.estatus = estatus;
	}

	public Short getCategoria() {
		return categoria;
	}

	public void setCategoria(Short categoria) {
		this.categoria = categoria;
	}

	public BigDecimal getPorcIva() {
		return porcIva;
	}

	public void setPorcIva(BigDecimal porcIva) {
		this.porcIva = porcIva;
	}

	public BigDecimal getPorcIsr() {
		return porcIsr;
	}

	public void setPorcIsr(BigDecimal porcIsr) {
		this.porcIsr = porcIsr;
	}

	public BigDecimal getPorcIvaRetenido() {
		return porcIvaRetenido;
	}

	public void setPorcIvaRetenido(BigDecimal porcIvaRetenido) {
		this.porcIvaRetenido = porcIvaRetenido;
	}

	public Boolean getAplicaIva() {
		return aplicaIva;
	}

	public void setAplicaIva(Boolean aplicaIva) {
		this.aplicaIva = aplicaIva;
	}

	public Boolean getAplicaIvaRetenido() {
		return aplicaIvaRetenido;
	}

	public void setAplicaIvaRetenido(Boolean aplicaIvaRetenido) {
		this.aplicaIvaRetenido = aplicaIvaRetenido;
	}

	public Boolean getAplicaIsr() {
		return aplicaIsr;
	}

	public void setAplicaIsr(Boolean aplicaIsr) {
		this.aplicaIsr = aplicaIsr;
	}

	public Boolean getAplicaPorcentajes() {
		if (this.aplicaIsr || this.aplicaIva || this.aplicaIvaRetenido){
			return true;
		}else{
			return false;
		}
	}

	public void setAplicaPorcentajes(Boolean aplicaPorcentajes) {
		this.aplicaPorcentajes = aplicaPorcentajes;
	}
	
	
	
	

}
