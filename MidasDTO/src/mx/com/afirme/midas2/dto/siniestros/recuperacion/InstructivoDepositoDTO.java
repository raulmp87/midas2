package mx.com.afirme.midas2.dto.siniestros.recuperacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class InstructivoDepositoDTO implements Serializable{

	private static final long serialVersionUID = 5820349063340842823L;

	private BigDecimal importe;
	private String referencia;
	private String codigoBarras;
	private String numeroReporte;
	private String numeroSiniestro;
	private String tipoRecuperacion;
	private String medioRecuperacion;
	
	private List<InstructivoDepositoListadoBancosDTO> listadoBancos;

	/**
	 * @return the importe
	 */
	public BigDecimal getImporte() {
		return importe;
	}

	/**
	 * @param importe the importe to set
	 */
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	/**
	 * @return the referencia
	 */
	public String getReferencia() {
		return referencia;
	}

	/**
	 * @param referencia the referencia to set
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	/**
	 * @return the codigoBarras
	 */
	public String getCodigoBarras() {
		return codigoBarras;
	}

	/**
	 * @param codigoBarras the codigoBarras to set
	 */
	public void setCodigoBarras(String codigoBarras) {
		this.codigoBarras = codigoBarras;
	}

	/**
	 * @return the numeroReporte
	 */
	public String getNumeroReporte() {
		return numeroReporte;
	}

	/**
	 * @param numeroReporte the numeroReporte to set
	 */
	public void setNumeroReporte(String numeroReporte) {
		this.numeroReporte = numeroReporte;
	}

	/**
	 * @return the numeroSiniestro
	 */
	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}

	/**
	 * @param numeroSiniestro the numeroSiniestro to set
	 */
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}

	/**
	 * @return the tipoRecuperacion
	 */
	public String getTipoRecuperacion() {
		return tipoRecuperacion;
	}

	/**
	 * @param tipoRecuperacion the tipoRecuperacion to set
	 */
	public void setTipoRecuperacion(String tipoRecuperacion) {
		this.tipoRecuperacion = tipoRecuperacion;
	}

	/**
	 * @return the medioRecuperacion
	 */
	public String getMedioRecuperacion() {
		return medioRecuperacion;
	}

	/**
	 * @param medioRecuperacion the medioRecuperacion to set
	 */
	public void setMedioRecuperacion(String medioRecuperacion) {
		this.medioRecuperacion = medioRecuperacion;
	}

	/**
	 * @return the listadoBancos
	 */
	public List<InstructivoDepositoListadoBancosDTO> getListadoBancos() {
		return listadoBancos;
	}

	/**
	 * @param listadoBancos the listadoBancos to set
	 */
	public void setListadoBancos(
			List<InstructivoDepositoListadoBancosDTO> listadoBancos) {
		this.listadoBancos = listadoBancos;
	}
	
}
