

/**
 * Objeto de transporte nuevo para mostrar la información proveniente de HGS en la
 * recuperación.
 * 
 * <font color="#0000ff"><b>numOrdenCompra </b></font><b>- La seleccionada en el
 * pop-up de búsqueda</b>
 * <font color="#0000ff"><b>fechaOrdenCompra</b></font><b>- La fecha de creación
 * de la entidad de órden de compra</b>
 * <font color="#0000ff"><b>montoOrdenCompra </b></font><b>- De la sumatoria de
 * los conceptos de la órden de compra. Se obtiene con el método
 * OrdenCompraService.obtenerTotales</b>
 * <font color="#0000ff"><b>fechaOrdenPago - </b></font><b>Fecha de Creación de la
 * Orden de Pago relacionada a la Orden de Compra</b>
 * <font color="#0000ff"><b>montoOrdenPago </b></font><b> De la sumatoria de los
 * conceptos de la órden de pago. Se obtiene con el método PagoSiniestroService.
 * obtenerTotales</b>
 * <font color="#0000ff"><b>factura </b></font><b>- Se obtiene de la Orden de
 * Compra</b>
 * <font color="#0000ff"><b>nombreProveedor </b></font><b>- Se obtiene de la Orden
 * de Compra</b>
 * <font color="#0000ff"><b>telefonoProveedor</b></font><b> - Se obtiene del
 * catálogo de Proveedores (teléfono de contacto)</b>
 * <font color="#0000ff"><b>correoProveedor </b></font><b>- Se obtiene del
 * catálogo de Proveedores.</b>
 * 
 * <font color="#0000ff"><b>numValuacion </b></font><font color="#800080"><b>-
 * </b></font><b>Número de valuación de HGS.</b>
 * <font color="#0000ff"><b>nombreValuador </b></font><font color="#800080"><b>-
 * </b></font><b>Nombre del valuador de HGS.</b>
 * <font color="#0000ff"><b>nombreTaller - </b></font><b> Nombre Proveedor del
 * vale de refacción de HGS.</b>
 * <font color="#0000ff"><b>claveTaller - </b></font><b> Clave Proveedor del vale
 * de refaccion de HGS, que corresponde a un prestador de servicio.</b>
 * 
 * <b>
 * </b><font color="#0000ff"><b>marcaVehiculo </b></font><font color="#800080"><b>-
 * </b></font><b>Se obtiene del Pase de Atención por Daños Materiales o de RC
 * Vehículos.</b>
 * <font color="#0000ff"><b>tipoVehiculo</b></font><font color="#800080"><b> -
 * </b></font><b>Se obtiene del Pase de Atención por Daños Materiales o de RC
 * Vehículos.</b>
 * <font color="#0000ff"><b>modeloVehiculo </b></font><font color="#800080"><b>-
 * </b></font><b>Se obtiene del Pase de Atención por Daños Materiales o de RC
 * Vehículos.</b>
 * <font color="#0000ff"><b>adminRefacciones - </b></font><b>Se obtiene de HGS de
 * los nuevos servicios.</b>
 * <font color="#0000ff"><b>adminRefaccionesId - </b></font><b>Se obtiene de HGS
 * de los nuevos servicios.</b>
 * @author Arturo
 * @version 1.0
 * @created 11-may-2015 05:57:55 p.m.
 */

package mx.com.afirme.midas2.dto.siniestros.recuperacion;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class OrdenCompraRecuperacionDTO  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5337812737886584510L;
	private String adminRefacciones;
	private Long adminRefaccionesId;
	private String correoProveedor;
	private Boolean esRefaccion;
	private String factura;
	private Date fechaOrdenCompra;
	private Date fechaOrdenPago;
	private String marcaVehiculo;
	private String modeloVehiculo;
	private BigDecimal montoOrdenCompra;
	private BigDecimal montoOrdenPago;
	private String nombreProveedor;
	private String nombreTaller;
	private String nombreValuador;
	private Long numOrdenCompra;
	private Long numValuacion;
	private String telefonoProveedor;
	private String tipoVehiculo;
	private boolean esResponsabilidadCivil;
	private boolean esDanosMateriale;
	
	
	private String noProveedor;
	
	
	public String getAdminRefacciones() {
		return adminRefacciones;
	}
	public void setAdminRefacciones(String adminRefacciones) {
		this.adminRefacciones = adminRefacciones;
	}
	public Long getAdminRefaccionesId() {
		return adminRefaccionesId;
	}
	public void setAdminRefaccionesId(Long adminRefaccionesId) {
		this.adminRefaccionesId = adminRefaccionesId;
	}
	public String getCorreoProveedor() {
		return correoProveedor;
	}
	public void setCorreoProveedor(String correoProveedor) {
		this.correoProveedor = correoProveedor;
	}
	public Boolean getEsRefaccion() {
		return esRefaccion;
	}
	public void setEsRefaccion(Boolean esRefaccion) {
		this.esRefaccion = esRefaccion;
	}
	public String getFactura() {
		return factura;
	}
	public void setFactura(String factura) {
		this.factura = factura;
	}
	public Date getFechaOrdenCompra() {
		return fechaOrdenCompra;
	}
	public void setFechaOrdenCompra(Date fechaOrdenCompra) {
		this.fechaOrdenCompra = fechaOrdenCompra;
	}
	public Date getFechaOrdenPago() {
		return fechaOrdenPago;
	}
	public void setFechaOrdenPago(Date fechaOrdenPago) {
		this.fechaOrdenPago = fechaOrdenPago;
	}
	public String getMarcaVehiculo() {
		return marcaVehiculo;
	}
	public void setMarcaVehiculo(String marcaVehiculo) {
		this.marcaVehiculo = marcaVehiculo;
	}
	public String getModeloVehiculo() {
		return modeloVehiculo;
	}
	public void setModeloVehiculo(String modeloVehiculo) {
		this.modeloVehiculo = modeloVehiculo;
	}
	


	
	
	public String getNombreProveedor() {
		return nombreProveedor;
	}
	public void setNombreProveedor(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}
	public String getNombreTaller() {
		return nombreTaller;
	}
	public void setNombreTaller(String nombreTaller) {
		this.nombreTaller = nombreTaller;
	}
	public String getNombreValuador() {
		return nombreValuador;
	}
	public void setNombreValuador(String nombreValuador) {
		this.nombreValuador = nombreValuador;
	}
	public Long getNumOrdenCompra() {
		return numOrdenCompra;
	}
	public void setNumOrdenCompra(Long numOrdenCompra) {
		this.numOrdenCompra = numOrdenCompra;
	}
	public Long getNumValuacion() {
		return numValuacion;
	}
	public void setNumValuacion(Long numValuacion) {
		this.numValuacion = numValuacion;
	}
	public String getTelefonoProveedor() {
		return telefonoProveedor;
	}
	public void setTelefonoProveedor(String telefonoProveedor) {
		this.telefonoProveedor = telefonoProveedor;
	}
	public String getTipoVehiculo() {
		return tipoVehiculo;
	}
	public void setTipoVehiculo(String tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getNoProveedor() {
		return noProveedor;
	}
	public void setNoProveedor(String noProveedor) {
		this.noProveedor = noProveedor;
	}
	public BigDecimal getMontoOrdenCompra() {
		return montoOrdenCompra;
	}
	public void setMontoOrdenCompra(BigDecimal montoOrdenCompra) {
		this.montoOrdenCompra = montoOrdenCompra;
	}
	public BigDecimal getMontoOrdenPago() {
		return montoOrdenPago;
	}
	public void setMontoOrdenPago(BigDecimal montoOrdenPago) {
		this.montoOrdenPago = montoOrdenPago;
	}
	public boolean isEsResponsabilidadCivil() {
		return esResponsabilidadCivil;
	}
	public void setEsResponsabilidadCivil(boolean esResponsabilidadCivil) {
		this.esResponsabilidadCivil = esResponsabilidadCivil;
	}
	public boolean isEsDanosMateriale() {
		return esDanosMateriale;
	}
	public void setEsDanosMateriale(boolean esDanosMateriale) {
		this.esDanosMateriale = esDanosMateriale;
	}

	
	
	

}
