package mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Transient;

import mx.com.afirme.midas2.annotation.Exportable;

public class ReporteSiniestroDTO implements Serializable{

	private static final long serialVersionUID = 1914515880899142008L;
	private Long oficinaId;
	private String numeroSiniestro;
	private String numeroReporte;
	private Date fechaIniReporte;
	private Date fechaFinReporte;
	private String nombrePersona;
	private Long reporteCabinaId;	
	private String nombreAsegurado;
	private String nombreConductor;
	private Date fechaSiniestro;
	private String tipoSiniestro;
	private String terminoSiniestro;
	private String numeroSerie;
	private String ajustador;
	private String razonSocial;
	private String numeroPoliza;
	private Integer numeroInciso;
	private String nombreOficina;
	private String claveOficina;
	private String consecutivoReporte;
	private String anioReporte;
	private Integer estatus;
	private String estatusDesc;
	private Long   siniestroCabinaId;
	private Short tipoServicio;
	private boolean isSiniestrado;
	private Integer totalCount;
	private Integer posStart;
	private Integer count;
	private String orderByAttribute;
	
	public ReporteSiniestroDTO() {
		
	}
	
	public ReporteSiniestroDTO(Long reporteCabinaId, String nombreOficina, Long oficinaId, Date fechaSiniestro, String numeroReporte,
			String claveOficina, String consecutivoReporte, String anioReporte, String nombrePersona, String nombreConductor,
			Integer estatus, String estatusDesc, String numeroPoliza, String nombreAsegurado, Integer numeroInciso, String numeroSerie,
			String numeroSiniestro,  Long siniestroCabinaId,Short tipoServicio) {
		
		this.reporteCabinaId = reporteCabinaId;
		this.nombreOficina = nombreOficina;
		this.oficinaId = oficinaId;
		this.fechaSiniestro = fechaSiniestro;
		this.numeroReporte = numeroReporte;
		this.claveOficina = claveOficina;
		this.consecutivoReporte = consecutivoReporte;
		this.anioReporte = anioReporte;
		this.nombrePersona = nombrePersona;
		this.nombreConductor = nombreConductor;
		this.estatus = estatus;
		this.estatusDesc = estatusDesc;
		this.numeroPoliza = numeroPoliza;
		this.nombreAsegurado = nombreAsegurado;
		this.numeroInciso = numeroInciso;
		this.numeroSerie = numeroSerie;
		this.numeroSiniestro = numeroSiniestro;
		this.siniestroCabinaId = siniestroCabinaId;
		this.tipoServicio      = tipoServicio;
		
	}

	public ReporteSiniestroDTO(Long reporteCabinaId, String nombreOficina, Long oficinaId, Date fechaSiniestro, String numeroReporte,
			String claveOficina, String consecutivoReporte, String anioReporte, String nombrePersona, String nombreConductor,
			Integer estatus, String estatusDesc, String numeroPoliza, String nombreAsegurado, Integer numeroInciso, String numeroSerie,
			String numeroSiniestro,  Long siniestroCabinaId,Short tipoServicio, String nombreAjustador) {
		
		this.reporteCabinaId = reporteCabinaId;
		this.nombreOficina = nombreOficina;
		this.oficinaId = oficinaId;
		this.fechaSiniestro = fechaSiniestro;
		this.numeroReporte = numeroReporte;
		this.claveOficina = claveOficina;
		this.consecutivoReporte = consecutivoReporte;
		this.anioReporte = anioReporte;
		this.nombrePersona = nombrePersona;
		this.nombreConductor = nombreConductor;
		this.estatus = estatus;
		this.estatusDesc = estatusDesc;
		this.numeroPoliza = numeroPoliza;
		this.nombreAsegurado = nombreAsegurado;
		this.numeroInciso = numeroInciso;
		this.numeroSerie = numeroSerie;
		this.numeroSiniestro = numeroSiniestro;
		this.siniestroCabinaId = siniestroCabinaId;
		this.tipoServicio      = tipoServicio;
		this.ajustador = nombreAjustador;
		
	}
	
	public ReporteSiniestroDTO(Long reporteCabinaId, String numeroSiniestro, String numeroReporte, String nombreAsegurado, String numeroPoliza,
			String tipoSiniestro, String terminoSiniestro, String nombreAjustador, Date fechaHoraReporte, String numeroSerie) {
		
		this.reporteCabinaId = reporteCabinaId;
		this.numeroSiniestro = numeroSiniestro;
		this.numeroReporte = numeroReporte;
		this.nombreAsegurado = nombreAsegurado;
		this.numeroPoliza = numeroPoliza;
		this.tipoSiniestro = tipoSiniestro;
		this.terminoSiniestro = terminoSiniestro;
		this.ajustador = nombreAjustador;
		this.fechaSiniestro = fechaHoraReporte;
		this.numeroSerie = numeroSerie;		
		
	}

	/**
	 * @return the oficinaId
	 */
	public Long getOficinaId() {
		return oficinaId;
	}
	/**
	 * @param oficinaId the oficinaId to set
	 */
	public void setOficinaId(Long oficinaId) {
		this.oficinaId = oficinaId;
	}
	/**
	 * @return the fechaIniReporte
	 */
	public Date getFechaIniReporte() {
		return fechaIniReporte;
	}
	/**
	 * @param fechaIniReporte the fechaIniReporte to set
	 */
	public void setFechaIniReporte(Date fechaIniReporte) {
		this.fechaIniReporte = fechaIniReporte;
	}
	/**
	 * @return the fechaFinReporte
	 */
	public Date getFechaFinReporte() {
		return fechaFinReporte;
	}
	/**
	 * @param fechaFinReporte the fechaFinReporte to set
	 */
	public void setFechaFinReporte(Date fechaFinReporte) {
		this.fechaFinReporte = fechaFinReporte;
	}
	/**
	 * @return the nombrePersona
	 */
	@Exportable(columnName="Nombre quien Reporta", columnOrder=8)
	public String getNombrePersona() {
		return nombrePersona;
	}
	/**
	 * @param nombrePersona the nombrePersona to set
	 */
	public void setNombrePersona(String nombrePersona) {
		this.nombrePersona = nombrePersona;
	}
	/**
	 * @return the reporteCabinaId
	 */
	public Long getReporteCabinaId() {
		return reporteCabinaId;
	}
	/**
	 * @param reporteCabinaId the reporteCabinaId to set
	 */
	public void setReporteCabinaId(Long reporteCabinaId) {
		this.reporteCabinaId = reporteCabinaId;
	}
	
	@Exportable(columnName="Número de Siniestro", columnOrder=2)
	public String getNumeroSiniestro(){
		return numeroSiniestro;
	}
	
	/**
	 * @param numeroSiniestro the numeroSiniestro to set
	 */
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}
	
	@Exportable(columnName="Número de Reporte", columnOrder=3)
	@Transient
	public String getNumeroReporte(){
		return numeroReporte;
	}
	
	/**
	 * @param numeroReporte the numeroReporte to set
	 */
	public void setNumeroReporte(String numeroReporte) {
		this.numeroReporte = numeroReporte;
	}
	
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}	
	
	@Exportable(columnName="Contratante (s)/ Razon social", columnOrder=7)
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}
	
	@Exportable(columnName="Nombre del Conductor", columnOrder=9)
	public String getNombreConductor() {
		return nombreConductor;
	}
	public void setNombreConductor(String nombreConductor) {
		this.nombreConductor = nombreConductor;
	}
	public String getTipoSiniestro() {
		return tipoSiniestro;
	}
	public void setTipoSiniestro(String tipoSiniestro) {
		this.tipoSiniestro = tipoSiniestro;
	}
	
	public String getTerminoSiniestro() {
		return terminoSiniestro;
	}
	public void setTerminoSiniestro(String terminoSiniestro) {
		this.terminoSiniestro = terminoSiniestro;
	}
	
	@Exportable(columnName="Nombre de Ajustador", columnOrder=4)
	public String getAjustador() {
		return ajustador;
	}
	public void setAjustador(String ajustador) {
		this.ajustador = ajustador;
	}
	
	@Exportable(columnName="Número de serie", columnOrder=10)
	public String getNumeroSerie() {
		return numeroSerie;
	}
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	
	@Exportable(columnName="Fecha de Reporte", format="dd/MM/yyyy", columnOrder=1)
	public Date getFechaSiniestro() {
		return fechaSiniestro;
	}
	public void setFechaSiniestro(Date fechaSiniestro) {
		this.fechaSiniestro = fechaSiniestro;
	}
	
	@Exportable(columnName="Número de Póliza", columnOrder=5)
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public Integer getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	
	@Exportable(columnName="Oficina", columnOrder=0	)
	public String getNombreOficina() {
		return nombreOficina;
	}
	public void setNombreOficina(String nombreOficina) {
		this.nombreOficina = nombreOficina;
	}
	public String getClaveOficina() {
		return claveOficina;
	}
	public void setClaveOficina(String claveOficina) {
		this.claveOficina = claveOficina;
	}
	public String getConsecutivoReporte() {
		return consecutivoReporte;
	}
	public void setConsecutivoReporte(String consecutivoReporte) {
		this.consecutivoReporte = consecutivoReporte;
	}
	public String getAnioReporte() {
		return anioReporte;
	}
	public void setAnioReporte(String anioReporte) {
		this.anioReporte = anioReporte;
	}
	public Integer getEstatus() {
		return estatus;
	}
	
	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}
	
	@Exportable(columnName="Estatus", columnOrder=6	)
	public String getEstatusDesc() {
		return estatusDesc;
	}
	public void setEstatusDesc(String estatusDesc) {
		this.estatusDesc = estatusDesc;
	}
	public Long getSiniestroCabinaId() {
		return siniestroCabinaId;
	}
	public void setSiniestroCabinaId(Long siniestroCabinaId) {
		this.siniestroCabinaId = siniestroCabinaId;
	}

	public Short getTipoServicio() {
		return tipoServicio;
	}

	public void setTipoServicio(Short tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

	public boolean isSiniestrado() {
		return isSiniestrado;
	}

	public void setSiniestrado(boolean isSiniestrado) {
		this.isSiniestrado = isSiniestrado;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
	
	public Integer getPosStart() {
		return posStart;
	}

	public void setPosStart(Integer posStart) {
		this.posStart = posStart;
	}

	public String getOrderByAttribute() {
		return orderByAttribute;
	}

	public void setOrderByAttribute(String orderByAttribute) {
		this.orderByAttribute = orderByAttribute;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

}
