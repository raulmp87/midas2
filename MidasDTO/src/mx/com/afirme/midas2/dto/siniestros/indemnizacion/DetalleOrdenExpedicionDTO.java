package mx.com.afirme.midas2.dto.siniestros.indemnizacion;

import java.io.Serializable;
import java.math.BigDecimal;

public class DetalleOrdenExpedicionDTO implements Serializable{

	private static final long serialVersionUID = 8098507513063195526L;
	
	private String siniestro;
	private String ordenPago;
	private String concepto;
	private String terminoAjuste;
	private String factura;
	private String numeroRecuperacion;
	private String tipoRecuperacion;
	private String referencia;
	private BigDecimal subtotal;
	private BigDecimal deducible;
	private BigDecimal descuento;
	private BigDecimal iva;
	private BigDecimal ivaAcred;
	private BigDecimal ivaRet;
	private BigDecimal isr;
	private BigDecimal isrRet;
	private BigDecimal primasPendientesPago;
	private BigDecimal primasADevolver;
	private BigDecimal aPagar;
	private BigDecimal primasPendientes;
	
	/**
	 * @return the concepto
	 */
	public String getConcepto() {
		return concepto;
	}
	/**
	 * @param concepto the concepto to set
	 */
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	/**
	 * @return the factura
	 */
	public String getFactura() {
		return factura;
	}
	/**
	 * @param factura the factura to set
	 */
	public void setFactura(String factura) {
		this.factura = factura;
	}
	/**
	 * @return the subtotal1
	 */
	public BigDecimal getSubtotal() {
		return subtotal;
	}
	/**
	 * @param subtotal1 the subtotal1 to set
	 */
	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}
	/**
	 * @return the ivaAcred
	 */
	public BigDecimal getIvaAcred() {
		return ivaAcred;
	}
	/**
	 * @param ivaAcred the ivaAcred to set
	 */
	public void setIvaAcred(BigDecimal ivaAcred) {
		this.ivaAcred = ivaAcred;
	}
	/**
	 * @return the ivaRet
	 */
	public BigDecimal getIvaRet() {
		return ivaRet;
	}
	/**
	 * @param ivaRet the ivaRet to set
	 */
	public void setIvaRet(BigDecimal ivaRet) {
		this.ivaRet = ivaRet;
	}
	/**
	 * @return the isrRet
	 */
	public BigDecimal getIsrRet() {
		return isrRet;
	}
	/**
	 * @param isrRet the isrRet to set
	 */
	public void setIsrRet(BigDecimal isrRet) {
		this.isrRet = isrRet;
	}
	/**
	 * @return the aPagar
	 */
	public BigDecimal getaPagar() {
		return aPagar;
	}
	/**
	 * @param aPagar the aPagar to set
	 */
	public void setaPagar(BigDecimal aPagar) {
		this.aPagar = aPagar;
	}
	/**
	 * @return the primasPendientes
	 */
	public BigDecimal getPrimasPendientes() {
		return primasPendientes;
	}
	/**
	 * @param primasPendientes the primasPendientes to set
	 */
	public void setPrimasPendientes(BigDecimal primasPendientes) {
		this.primasPendientes = primasPendientes;
	}
	/**
	 * @return the siniestro
	 */
	public String getSiniestro() {
		return siniestro;
	}
	/**
	 * @param siniestro the siniestro to set
	 */
	public void setSiniestro(String siniestro) {
		this.siniestro = siniestro;
	}
	/**
	 * @return the ordenPago
	 */
	public String getOrdenPago() {
		return ordenPago;
	}
	/**
	 * @param ordenPago the ordenPago to set
	 */
	public void setOrdenPago(String ordenPago) {
		this.ordenPago = ordenPago;
	}
	/**
	 * @return the terminoAjuste
	 */
	public String getTerminoAjuste() {
		return terminoAjuste;
	}
	/**
	 * @param terminoAjuste the terminoAjuste to set
	 */
	public void setTerminoAjuste(String terminoAjuste) {
		this.terminoAjuste = terminoAjuste;
	}
	/**
	 * @return the descuento
	 */
	public BigDecimal getDescuento() {
		return descuento;
	}
	/**
	 * @param descuento the descuento to set
	 */
	public void setDescuento(BigDecimal descuento) {
		this.descuento = descuento;
	}
	/**
	 * @return the primasPendientesPago
	 */
	public BigDecimal getPrimasPendientesPago() {
		return primasPendientesPago;
	}
	/**
	 * @param primasPendientesPago the primasPendientesPago to set
	 */
	public void setPrimasPendientesPago(BigDecimal primasPendientesPago) {
		this.primasPendientesPago = primasPendientesPago;
	}
	/**
	 * @return the primasADevolver
	 */
	public BigDecimal getPrimasADevolver() {
		return primasADevolver;
	}
	/**
	 * @param primasADevolver the primasADevolver to set
	 */
	public void setPrimasADevolver(BigDecimal primasADevolver) {
		this.primasADevolver = primasADevolver;
	}
	/**
	 * @return the deducible
	 */
	public BigDecimal getDeducible() {
		return deducible;
	}
	/**
	 * @param deducible the deducible to set
	 */
	public void setDeducible(BigDecimal deducible) {
		this.deducible = deducible;
	}
	/**
	 * @return the numeroRecuperacion
	 */
	public String getNumeroRecuperacion() {
		return numeroRecuperacion;
	}
	/**
	 * @param numeroRecuperacion the numeroRecuperacion to set
	 */
	public void setNumeroRecuperacion(String numeroRecuperacion) {
		this.numeroRecuperacion = numeroRecuperacion;
	}
	/**
	 * @return the tipoRecuperacion
	 */
	public String getTipoRecuperacion() {
		return tipoRecuperacion;
	}
	/**
	 * @param tipoRecuperacion the tipoRecuperacion to set
	 */
	public void setTipoRecuperacion(String tipoRecuperacion) {
		this.tipoRecuperacion = tipoRecuperacion;
	}
	/**
	 * @return the referencia
	 */
	public String getReferencia() {
		return referencia;
	}
	/**
	 * @param referencia the referencia to set
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	
	public BigDecimal getIsr() {
		return isr;
	}
	public void setIsr(BigDecimal isr) {
		this.isr = isr;
	}
	
	public BigDecimal getIva() {
		return iva;
	}
	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

}