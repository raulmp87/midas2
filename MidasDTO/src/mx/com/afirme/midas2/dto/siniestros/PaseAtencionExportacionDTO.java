package mx.com.afirme.midas2.dto.siniestros;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import mx.com.afirme.midas2.annotation.Exportable;

public class PaseAtencionExportacionDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String numeroSiniestro;
	private Integer anioSiniestro;
	private String numeroReporte;
	private String nombreAjustador;
	private String tallerAsignado;
	private String marca;
	private String desVehiculo;
	private Integer modelo;
	private String numeroSerie;
	private String placas;
	private String color;
	private String nombreContratante;
	private String nombreAfectado;
	private String telefonoUno;
	private String telefonoDos;
	private Date fechaOcurrido;
	private Integer mesReporte;
	private Date fechaReportado;
	private String horaReportada;
	private String nombreCobertura;
	private String tipoPase;
	private String folio;
	private String numeroPoliza;
	private Integer numeroInciso;
	private String moneda;
	private Date inicioVigencia;
	private String responsabilidad;
	private String causaSiniestro;
	private String terminoAjuste;
	private String hospital;
	private String ingenieroAsignado;
	private Date fechaPaseAtencion;
	private String estado;
	private String ciudad;
	private String estatusPase;
	private String operacion;
	private String nombreProducto;
	private String companiaTercero;
	private String folioTercero;
	private String siniestroTercero;
	private BigDecimal estimadoInicial;
	private BigDecimal ajusteMas;
	private BigDecimal ajusteMenos;
	private BigDecimal estimacionFinal;
	private BigDecimal pagos;
	private BigDecimal reservaAbierta;
	private BigDecimal salvamentos;
	private BigDecimal recuperaciones;
	private BigDecimal recuperacionesDeducibles;
	private BigDecimal recuperacionesPendientes;
	private String reciboOrdenCia;
	private String nombreGerencia;
	private Integer codigoOficina;
	private String nombreAgente;
	private Long centroEmisor;
	private String proceso;
	private String valuacion;
	private Date fechaIngresoTaller;
	private Date fechaValuacion;
	private Date fechaReingresoReparacion;
	private Date fechaUltimoSurtido;
	private Date fechaTerminacion;
	private BigDecimal importeRefacciones;
	private BigDecimal importeManoObra;
	private BigDecimal importeTotalValuacion;
	private String nombreValuador;
	private BigDecimal sumaAsegurada;
	private BigDecimal importeDaniosMateriales;
	private BigDecimal importeResponsabilidadCivil;
	private BigDecimal importeGastosMedicos;	
	


	@Exportable(columnName="NUMERO SINIESTRO", columnOrder=0, whenValueNull="N/A")
	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}
	
	@Exportable(columnName="AÑO SINIESTRO", columnOrder=1, whenValueNull="N/A")
	public Integer getAnioSiniestro() {
		return anioSiniestro;
	}
	public void setAnioSiniestro(Integer anioSiniestro) {
		this.anioSiniestro = anioSiniestro;
	}
	
	@Exportable(columnName="REPORTE CABINA", columnOrder=2)
	public String getNumeroReporte() {
		return numeroReporte;
	}
	public void setNumeroReporte(String numeroReporte) {
		this.numeroReporte = numeroReporte;
	}
	
	@Exportable(columnName="AJUSTADOR", columnOrder=3) 
	public String getNombreAjustador() {

			return nombreAjustador;
		}
	public void setNombreAjustador(String nombreAjustador) {
		this.nombreAjustador = nombreAjustador;
	}

	@Exportable(columnName="TALLER ASIGNADO", columnOrder=4, whenValueNull="N/A")
	public String getTallerAsignado() {
		return tallerAsignado;
	}
	public void setTallerAsignado(String tallerAsignado) {
		this.tallerAsignado = tallerAsignado;
	}

	@Exportable(columnName="MARCA", columnOrder=5, whenValueNull="N/A")
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}

	@Exportable(columnName="DESCRIPCION VEHICULO", columnOrder=6, whenValueNull="N/A")
	public String getDesVehiculo() {
		return desVehiculo;
	}
	public void setDesVehiculo(String desVehiculo) {
		this.desVehiculo = desVehiculo;
	}

	@Exportable(columnName="MODELO", columnOrder=7, whenValueNull="N/A")
	public Integer getModelo() {
		return modelo;
	}
	public void setModelo(Integer modelo) {
		this.modelo = modelo;
	}

	@Exportable(columnName="SERIE", columnOrder=8, whenValueNull="N/A")
	public String getNumeroSerie() {
		return numeroSerie;
	}
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	@Exportable(columnName="PLACA", columnOrder=9, whenValueNull="N/A")
	public String getPlacas() {
		return placas;
	}
	public void setPlacas(String placas) {
		this.placas = placas;
	}

	@Exportable(columnName="COLOR", columnOrder=10, whenValueNull="N/A")
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}

	@Exportable(columnName="CONTRATANTE", columnOrder=11)
	public String getNombreContratante() {
		return nombreContratante;
	}
	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}

	@Exportable(columnName="AFECTADO", columnOrder=12, whenValueNull="N/A")
	public String getNombreAfectado() {
		return nombreAfectado;
	}
	public void setNombreAfectado(String nombreAfectado) {
		this.nombreAfectado = nombreAfectado;
	}

	@Exportable(columnName="TELEFONO 1", columnOrder=13, whenValueNull="N/A")
	public String getTelefonoUno() {
		return telefonoUno;
	}
	public void setTelefonoUno(String telefonoUno) {
		this.telefonoUno = telefonoUno;
	}

	@Exportable(columnName="TELEFONO 2", columnOrder=14, whenValueNull="N/A")
	public String getTelefonoDos() {
		return telefonoDos;
	}
	public void setTelefonoDos(String telefonoDos) {
		this.telefonoDos = telefonoDos;
	}

	@Exportable(columnName="FECHA OCURRIDO", columnOrder=15)
	public Date getFechaOcurrido() {
		return fechaOcurrido;
	}
	public void setFechaOcurrido(Date fechaOcurrido) {
		this.fechaOcurrido = fechaOcurrido;
	}

	@Exportable(columnName="MES REPORTE", columnOrder=16)
	public Integer getMesReporte() {
		return mesReporte;
	}
	public void setMesReporte(Integer mesReporte) {
		this.mesReporte = mesReporte;
	}

	@Exportable(columnName="FECHA REPORTADO", columnOrder=17)
	public Date getFechaReportado() {
		return fechaReportado;
	}
	public void setFechaReportado(Date fechaReportado) {
		this.fechaReportado = fechaReportado;
	}

	@Exportable(columnName="HORA REPORTADO", columnOrder=18)
	public String getHoraReportada() {
		return horaReportada;
	}
	public void setHoraReportada(String horaReportada) {
		this.horaReportada = horaReportada;
	}

	@Exportable(columnName="NOM_COBERTURA", columnOrder=19)
	public String getNombreCobertura() {
		return nombreCobertura;
	}
	public void setNombreCobertura(String nombreCobertura) {
		this.nombreCobertura = nombreCobertura;
	}

	@Exportable(columnName="TIPO PASE DE ATENCION", columnOrder=20)
	public String getTipoPase() {
		return tipoPase;
	}
	public void setTipoPase(String tipoPase) {
		this.tipoPase = tipoPase;
	}

	@Exportable(columnName="FOLIO AFIRME", columnOrder=21)
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}

	@Exportable(columnName="POLIZA", columnOrder=22)
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	@Exportable(columnName="INCISO", columnOrder=23)
	public Integer getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	@Exportable(columnName="MONEDA", columnOrder=24, whenValueNull="N/A")
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	@Exportable(columnName="INICIO DE VIGENCIA", columnOrder=25)
	public Date getInicioVigencia() {
		return inicioVigencia;
	}
	public void setInicioVigencia(Date inicioVigencia) {
		this.inicioVigencia = inicioVigencia;
	}

	@Exportable(columnName="RESPONSABILIDAD", columnOrder=26)
	public String getResponsabilidad() {
		return responsabilidad;
	}
	public void setResponsabilidad(String responsabilidad) {
		this.responsabilidad = responsabilidad;
	}

	@Exportable(columnName="CAUSA SINIESTRO", columnOrder=27)
	public String getCausaSiniestro() {
		return causaSiniestro;
	}
	public void setCausaSiniestro(String causaSiniestro) {
		this.causaSiniestro = causaSiniestro;
	}

	@Exportable(columnName="TERMINO DE AJUSTE", columnOrder=28)
	public String getTerminoAjuste() {
		return terminoAjuste;
	}
	public void setTerminoAjuste(String terminoAjuste) {
		this.terminoAjuste = terminoAjuste;
	}

	@Exportable(columnName="HOSPITAL", columnOrder=29, whenValueNull="N/A")
	public String getHospital() {
		return hospital;
	}
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}

	@Exportable(columnName="INGENIERO ASIGNADO", columnOrder=30, whenValueNull="N/A")
	public String getIngenieroAsignado() {
		return ingenieroAsignado;
	}
	public void setIngenieroAsignado(String ingenieroAsignado) {
		this.ingenieroAsignado = ingenieroAsignado;
	}

	@Exportable(columnName="FECHA DEL PASE DE ATENCION", columnOrder=31)
	public Date getFechaPaseAtencion() {
		return fechaPaseAtencion;
	}
	public void setFechaPaseAtencion(Date fechaPaseAtencion) {
		this.fechaPaseAtencion = fechaPaseAtencion;
	}

	@Exportable(columnName="ESTADO", columnOrder=32, whenValueNull="N/A")
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Exportable(columnName="CIUDAD", columnOrder=33, whenValueNull="N/A")
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	@Exportable(columnName="ESTATUS PASE", columnOrder=34)
	public String getEstatusPase() {
		return estatusPase;
	}
	public void setEstatusPase(String estatusPase) {
		this.estatusPase = estatusPase;
	}

	@Exportable(columnName="OPERACION", columnOrder=35)
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	@Exportable(columnName="NOMBRE PRODUCTO", columnOrder=36)
	public String getNombreProducto() {
		return nombreProducto;
	}
	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	@Exportable(columnName="COMPAÑIA ASEG. TERCERO", columnOrder=37, whenValueNull="N/A")
	public String getCompaniaTercero() {
		return companiaTercero;
	}
	public void setCompaniaTercero(String companiaTercero) {
		this.companiaTercero = companiaTercero;
	}

	@Exportable(columnName="FOLIO ASEG.TERCERO", columnOrder=38, whenValueNull="N/A")
	public String getFolioTercero() {
		return folioTercero;
	}
	public void setFolioTercero(String folioTercero) {
		this.folioTercero = folioTercero;
	}

	@Exportable(columnName="SINIESTRO TERCERO", columnOrder=39, whenValueNull="N/A")
	public String getSiniestroTercero() {
		return siniestroTercero;
	}
	public void setSiniestroTercero(String siniestroTercero) {
		this.siniestroTercero = siniestroTercero;
	}

	@Exportable(columnName="ESTIMADO INICIAL", columnOrder=40, format="#,##0.00", whenValueNull="N/A")
	public BigDecimal getEstimadoInicial() {
		return estimadoInicial;
	}
	public void setEstimadoInicial(BigDecimal estimadoInicial) {
		this.estimadoInicial = estimadoInicial;
	}

	@Exportable(columnName="AJUSTE DE MAS", columnOrder=41, format="#,##0.00", whenValueNull="N/A")
	public BigDecimal getAjusteMas() {
		return ajusteMas;
	}
	public void setAjusteMas(BigDecimal ajusteMas) {
		this.ajusteMas = ajusteMas;
	}

	@Exportable(columnName="AJUSTE DE MENOS", columnOrder=42, format="#,##0.00", whenValueNull="N/A")
	public BigDecimal getAjusteMenos() {
		return ajusteMenos;
	}
	public void setAjusteMenos(BigDecimal ajusteMenos) {
		this.ajusteMenos = ajusteMenos;
	}

	@Exportable(columnName="ESTIMACION FINAL", columnOrder=43, format="#,##0.00", whenValueNull="N/A")
	public BigDecimal getEstimacionFinal() {
		return estimacionFinal;
	}
	public void setEstimacionFinal(BigDecimal estimacionFinal) {
		this.estimacionFinal = estimacionFinal;
	}

	@Exportable(columnName="PAGOS", columnOrder=44, format="#,##0.00", whenValueNull="N/A")
	public BigDecimal getPagos() {
		return pagos;
	}
	public void setPagos(BigDecimal pagos) {
		this.pagos = pagos;
	}

	@Exportable(columnName="RESERVA ABIERTA", columnOrder=45, format="#,##0.00", whenValueNull="N/A")
	public BigDecimal getReservaAbierta() {
		return reservaAbierta;
	}
	public void setReservaAbierta(BigDecimal reservaAbierta) {
		this.reservaAbierta = reservaAbierta;
	}

	@Exportable(columnName="SALVAMENTOS", columnOrder=46, format="#,##0.00", whenValueNull="N/A")
	public BigDecimal getSalvamentos() {
		return salvamentos;
	}
	public void setSalvamentos(BigDecimal salvamentos) {
		this.salvamentos = salvamentos;
	}

	@Exportable(columnName="RECUPERACIONES", columnOrder=47, format="#,##0.00", whenValueNull="N/A")
	public BigDecimal getRecuperaciones() {
		return recuperaciones;
	}
	public void setRecuperaciones(BigDecimal recuperaciones) {
		this.recuperaciones = recuperaciones;
	}

	@Exportable(columnName="RECUPERACIONES DEDUCIBLES", columnOrder=48, format="#,##0.00", whenValueNull="N/A")
	public BigDecimal getRecuperacionesDeducibles() {
		return recuperacionesDeducibles;
	}
	public void setRecuperacionesDeducibles(BigDecimal recuperacionesDeducibles) {
		this.recuperacionesDeducibles = recuperacionesDeducibles;
	}

	@Exportable(columnName="RECUPERACIONES PENDIENTES", columnOrder=49, format="#,##0.00", whenValueNull="N/A")
	public BigDecimal getRecuperacionesPendientes() {
		return recuperacionesPendientes;
	}
	public void setRecuperacionesPendientes(BigDecimal recuperacionesPendientes) {
		this.recuperacionesPendientes = recuperacionesPendientes;
	}

	@Exportable(columnName="RECIBIMOS ORDEN DE COMPAÑIA", columnOrder=50, whenValueNull="N/A")
	public String getReciboOrdenCia() {
		return reciboOrdenCia;
	}
	public void setReciboOrdenCia(String reciboOrdenCia) {
		this.reciboOrdenCia = reciboOrdenCia;
	}

	@Exportable(columnName="NOMBRE GERENCIA", columnOrder=51, whenValueNull="N/A")
	public String getNombreGerencia() {
		return nombreGerencia;
	}
	public void setNombreGerencia(String nombreGerencia) {
		this.nombreGerencia = nombreGerencia;
	}

	@Exportable(columnName="CODIGO OFICINA", columnOrder=52, whenValueNull="N/A")
	public Integer getCodigoOficina() {
		return codigoOficina;
	}
	public void setCodigoOficina(Integer codigoOficina) {
		this.codigoOficina = codigoOficina;
	}

	@Exportable(columnName="AGENTE", columnOrder=53, whenValueNull="N/A")
	public String getNombreAgente() {
		return nombreAgente;
	}
	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}

	@Exportable(columnName="CENTRO EMISOR", columnOrder=54, whenValueNull="N/A")
	public Long getCentroEmisor() {
		return centroEmisor;
	}
	public void setCentroEmisor(Long centroEmisor) {
		this.centroEmisor = centroEmisor;
	}

	@Exportable(columnName="PROCESO", columnOrder=55, whenValueNull="N/A")
	public String getProceso() {
		return proceso;
	}
	public void setProceso(String proceso) {
		this.proceso = proceso;
	}

	@Exportable(columnName="VALUACION", columnOrder=56, whenValueNull="N/A")
	public String getValuacion() {
		return valuacion;
	}
	public void setValuacion(String valuacion) {
		this.valuacion = valuacion;
	}

	@Exportable(columnName="FECHA INGRESO A TALLER", columnOrder=57, whenValueNull="N/A")
	public Date getFechaIngresoTaller() {
		return fechaIngresoTaller;
	}
	public void setFechaIngresoTaller(Date fechaIngresoTaller) {
		this.fechaIngresoTaller = fechaIngresoTaller;
	}

	@Exportable(columnName="FECHA DE VALUACION", columnOrder=58, whenValueNull="N/A")
	public Date getFechaValuacion() {
		return fechaValuacion;
	}
	public void setFechaValuacion(Date fechaValuacion) {
		this.fechaValuacion = fechaValuacion;
	}

	@Exportable(columnName="FECHA REINGRESO A REPARACION", columnOrder=59, whenValueNull="N/A")
	public Date getFechaReingresoReparacion() {
		return fechaReingresoReparacion;
	}
	public void setFechaReingresoReparacion(Date fechaReingresoReparacion) {
		this.fechaReingresoReparacion = fechaReingresoReparacion;
	}

	@Exportable(columnName="FECHA ULTIMO SURTIDO", columnOrder=60, whenValueNull="N/A")
	public Date getFechaUltimoSurtido() {
		return fechaUltimoSurtido;
	}
	public void setFechaUltimoSurtido(Date fechaUltimoSurtido) {
		this.fechaUltimoSurtido = fechaUltimoSurtido;
	}

	@Exportable(columnName="FECHA DE TERMINACION", columnOrder=61, whenValueNull="N/A")
	public Date getFechaTerminacion() {
		return fechaTerminacion;
	}
	public void setFechaTerminacion(Date fechaTerminacion) {
		this.fechaTerminacion = fechaTerminacion;
	}

	@Exportable(columnName="IMPORTE DE REFACCIONES", columnOrder=62, format="#,##0.00", whenValueNull="N/A")
	public BigDecimal getImporteRefacciones() {
		return importeRefacciones;
	}
	public void setImporteRefacciones(BigDecimal importeRefacciones) {
		this.importeRefacciones = importeRefacciones;
	}

	@Exportable(columnName="IMPORTE MANO DE OBRA", columnOrder=63, format="#,##0.00", whenValueNull="N/A")
	public BigDecimal getImporteManoObra() {
		return importeManoObra;
	}
	public void setImporteManoObra(BigDecimal importeManoObra) {
		this.importeManoObra = importeManoObra;
	}

	@Exportable(columnName="IMPORTE TOTAL DE VALUACION", columnOrder=64, format="#,##0.00", whenValueNull="N/A")
	public BigDecimal getImporteTotalValuacion() {
		return importeTotalValuacion;
	}
	public void setImporteTotalValuacion(BigDecimal importeTotalValuacion) {
		this.importeTotalValuacion = importeTotalValuacion;
	}

	@Exportable(columnName="VALUADOR", columnOrder=65, whenValueNull="N/A")
	public String getNombreValuador() {
		return nombreValuador;
	}
	public void setNombreValuador(String nombreValuador) {
		this.nombreValuador = nombreValuador;
	}

	@Exportable(columnName="SUMA ASEGURADA", columnOrder=66, format="#,##0.00")
	public BigDecimal getSumaAsegurada() {
		return sumaAsegurada;
	}
	public void setSumaAsegurada(BigDecimal sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
	public BigDecimal getImporteDaniosMateriales() {
		return importeDaniosMateriales;
	}
	public void setImporteDaniosMateriales(BigDecimal importeDaniosMateriales) {
		this.importeDaniosMateriales = importeDaniosMateriales;
	}
	public BigDecimal getImporteResponsabilidadCivil() {
		return importeResponsabilidadCivil;
	}
	public void setImporteResponsabilidadCivil(BigDecimal importeResponsabilidadCivil) {
		this.importeResponsabilidadCivil = importeResponsabilidadCivil;
	}
	public BigDecimal getImporteGastosMedicos() {
		return importeGastosMedicos;
	}
	public void setImporteGastosMedicos(BigDecimal importeGastosMedicos) {
		this.importeGastosMedicos = importeGastosMedicos;
	}
}