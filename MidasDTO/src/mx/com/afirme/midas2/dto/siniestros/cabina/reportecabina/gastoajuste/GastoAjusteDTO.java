package mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.gastoajuste;

public class GastoAjusteDTO {
	private Long id;
	private Long personaId;
	private Integer prestadorAjusteId;
	private Integer tallerAsignadoId;
	private String tipoGrua;
	private String rfc;
	private String curp;
	private String motivoSituacion;
	private Boolean mostrarSeccionGrua;
	private String numeroReporteExterno;
	private String numeroFactura;
	private Short origen;
	private String tipoPrestadorId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getPersonaId() {
		return personaId;
	}
	public void setPersonaId(Long personaId) {
		this.personaId = personaId;
	}
	public Integer getTallerAsignadoId() {
		return tallerAsignadoId;
	}
	public void setTallerAsignadoId(Integer tallerAsignadoId) {
		this.tallerAsignadoId = tallerAsignadoId;
	}
	public String getTipoGrua() {
		return tipoGrua;
	}
	public void setTipoGrua(String tipoGrua) {
		this.tipoGrua = tipoGrua;
	}
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getCurp() {
		return curp;
	}
	public void setCurp(String curp) {
		this.curp = curp;
	}
	public String getMotivoSituacion() {
		return motivoSituacion;
	}
	public void setMotivoSituacion(String motivoSituacion) {
		this.motivoSituacion = motivoSituacion;
	}
	public Boolean getMostrarSeccionGrua() {
		return mostrarSeccionGrua;
	}
	public void setMostrarSeccionGrua(Boolean mostrarSeccionGrua) {
		this.mostrarSeccionGrua = mostrarSeccionGrua;
	}
	public String getNumeroReporteExterno() {
		return numeroReporteExterno;
	}
	public void setNumeroReporteExterno(String numeroReporteExterno) {
		this.numeroReporteExterno = numeroReporteExterno;
	}
	public String getNumeroFactura() {
		return numeroFactura;
	}
	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}
	public Short getOrigen() {
		return origen;
	}
	public void setOrigen(Short origen) {
		this.origen = origen;
	}
	public Integer getPrestadorAjusteId() {
		return prestadorAjusteId;
	}
	public void setPrestadorAjusteId(Integer prestadorAjusteId) {
		this.prestadorAjusteId = prestadorAjusteId;
	}
	public String getTipoPrestadorId() {
		return tipoPrestadorId;
	}
	public void setTipoPrestadorId(String tipoPrestadorId) {
		this.tipoPrestadorId = tipoPrestadorId;
	}
}
