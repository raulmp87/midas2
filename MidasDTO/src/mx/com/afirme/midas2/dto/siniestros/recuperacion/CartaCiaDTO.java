package mx.com.afirme.midas2.dto.siniestros.recuperacion;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.annotation.Exportable;
import mx.com.afirme.midas2.annotation.MultipleDocumentExportable;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.OrdenCompraCartaCia;



/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 11-Aug-2015 7:29:51 PM
 */
public class CartaCiaDTO {

	private Long cartaId;
	private String nombreCia;
	private String siniestro;
	private String siniestroCia;
	private String polizaCia;
	private String vehiculo;
	private String vehiculoTercero;
	private Date fechaSiniestro;
	private String tipoPase;
	private String usuario;
	private Float porcentajePart;
	private BigDecimal importe;
	private String folio;
	private String folioAnterior;
	private Date fechaAcuse;
	private BigDecimal montoIndemnizacion;
	private BigDecimal montoSalvamento;
	private BigDecimal montoPiezas;
	private BigDecimal montoGAGrua ;
	private Boolean esRedocumentada;
	private Boolean esComplemento;
	private Boolean esPerdidaTotal;
	private String lugar;
	private Date fecha;
	private Date fechaUltimoAcuse;
	private Date fechaElaboraCarta;
	private Integer diasAtraso;
	private String observaciones;
	private String numeroRecuperacion;
	private String oficinaDesc;
	private String nombreCobertura;
	private String folioPase;
	private String lesionado;
	private String tipoCarta;
	private List<CartaCiaExclusionesDTO> exclusiones;
	private List<OrdenCompraCartaCia> ordenesCompra;
	public static final String DOCUMENTO_EDOCUENTA = "ESTADO_CUENTA";
	public static final String DOCUMENTO_CARTA_ELABORAR = "CARTAS_POR_ELABORAR";
	
	public static enum TipoCarta{
		DANOS_MATERIALES_RESP_CIVIL("DMRC"), GASTOS_MEDICOS("GM"), ROBO_TOTAL("RT"), PERDIDA_TOTAL("PT");
		public final String codigo;
		TipoCarta(String codigo){
			this.codigo = codigo;
		}
	}
	
	
	
	public Long getCartaId() {
		return cartaId;
	}

	public void setCartaId(Long cartaId) {
		this.cartaId = cartaId;
	}

	@MultipleDocumentExportable (value = {
			@Exportable(columnName="NOMBRE COMPAÑIA", columnOrder=0, document=DOCUMENTO_EDOCUENTA),
			@Exportable(columnName="NOMBRE COMPAÑIA", columnOrder=6, document=DOCUMENTO_CARTA_ELABORAR)
		})
	
	public String getNombreCia() {
		return nombreCia;
	}

	public void setNombreCia(String nombreCia) {
		this.nombreCia = nombreCia;
	}

	@MultipleDocumentExportable(value={
			@Exportable(columnName="NUMERO DE SINIESTRO AFIRME", columnOrder=1, document=DOCUMENTO_EDOCUENTA),
			@Exportable(columnName="NUMERO DE SINIESTRO AFIRME", columnOrder=3, document=DOCUMENTO_CARTA_ELABORAR)
	})
	
	public String getSiniestro() {
		return siniestro;
	}

	public void setSiniestro(String siniestro) {
		this.siniestro = siniestro;
	}

	@Exportable(columnName="SINIESTRO DE COMPAÑIA", columnOrder=6, document=DOCUMENTO_EDOCUENTA)
	public String getSiniestroCia() {
		return siniestroCia;
	}

	public void setSiniestroCia(String siniestroCia) {
		this.siniestroCia = siniestroCia;
	}

	@Exportable(columnName="POLIZA DE COMPAÑIA", columnOrder=7, document=DOCUMENTO_EDOCUENTA)
	public String getPolizaCia() {
		return polizaCia;
	}

	public void setPolizaCia(String polizaCia) {
		this.polizaCia = polizaCia;
	}

	public String getVehiculo() {
		return vehiculo;
	}

	public void setVehiculo(String vehiculo) {
		this.vehiculo = vehiculo;
	}

	@Exportable(columnName="DATOS DEL VEHICULO TERCERO", columnOrder=8, document=DOCUMENTO_EDOCUENTA)
	public String getVehiculoTercero() {
		return vehiculoTercero;
	}

	public void setVehiculoTercero(String vehiculoTercero) {
		this.vehiculoTercero = vehiculoTercero;
	}

	@MultipleDocumentExportable(value={
			@Exportable(columnName="FECHA DE SINIESTRO", columnOrder=2, format= "dd/MM/yyyy", document=DOCUMENTO_EDOCUENTA),
			@Exportable(columnName="FECHA DE SINIESTRO", columnOrder=2, format= "dd/MM/yyyy", document=DOCUMENTO_CARTA_ELABORAR)
	})	
	public Date getFechaSiniestro() {
		return fechaSiniestro;
	}

	public void setFechaSiniestro(Date fechaSiniestro) {
		this.fechaSiniestro = fechaSiniestro;
	}

	public String getTipoPase() {
		return tipoPase;
	}

	public void setTipoPase(String tipoPase) {
		this.tipoPase = tipoPase;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Float getPorcentajePart() {
		return porcentajePart;
	}

	public void setPorcentajePart(Float porcentajePart) {
		this.porcentajePart = porcentajePart;
	}

	@MultipleDocumentExportable(value= {
			@Exportable(columnName="IMPORTE A RECUPERAR", columnOrder=5, format="$* #,##0.00", document=DOCUMENTO_EDOCUENTA),
			@Exportable(columnName="IMPORTE", columnOrder=7, format="$* #,##0.00", document=DOCUMENTO_CARTA_ELABORAR)
	})
	
	public BigDecimal getImporte() {
		return importe;
	}

	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	@Exportable(columnName="FOLIO DE CARTA", columnOrder=3, document=DOCUMENTO_EDOCUENTA)
	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getFolioAnterior() {
		return folioAnterior;
	}

	public void setFolioAnterior(String folioAnterior) {
		this.folioAnterior = folioAnterior;
	}

	@Exportable(columnName="FECHA DE ACUSE", columnOrder=11, format="dd/MM/yyyy", document=DOCUMENTO_EDOCUENTA)
	public Date getFechaAcuse() {
		return fechaAcuse;
	}

	public void setFechaAcuse(Date fechaAcuse) {
		this.fechaAcuse = fechaAcuse;
	}

	public BigDecimal getMontoIndemnizacion() {
		return montoIndemnizacion;
	}

	public void setMontoIndemnizacion(BigDecimal montoIndemnizacion) {
		this.montoIndemnizacion = montoIndemnizacion;
	}

	public BigDecimal getMontoSalvamento() {
		return montoSalvamento;
	}

	public void setMontoSalvamento(BigDecimal montoSalvamento) {
		this.montoSalvamento = montoSalvamento;
	}

	public BigDecimal getMontoGAGrua() {
		return montoGAGrua;
	}

	public void setMontoGAGrua(BigDecimal montoGAGrua) {
		this.montoGAGrua = montoGAGrua;
	}

	public Boolean getEsRedocumentada() {
		return esRedocumentada;
	}

	public void setEsRedocumentada(Boolean esRedocumentada) {
		this.esRedocumentada = esRedocumentada;
	}

	public Boolean getEsComplemento() {
		return esComplemento;
	}

	public void setEsComplemento(Boolean esComplemento) {
		this.esComplemento = esComplemento;
	}

	@Exportable(columnName="PT (PERDIDA TOTAL)", columnOrder=9, fixedValues="true=SI,false=NO", document=DOCUMENTO_EDOCUENTA)
	public Boolean getEsPerdidaTotal() {
		return esPerdidaTotal;
	}

	public void setEsPerdidaTotal(Boolean esPerdidaTotal) {
		this.esPerdidaTotal = esPerdidaTotal;
	}

	public String getLugar() {
		return lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}
	
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@Exportable(columnName="ULTIMO ACUSE", columnOrder=12, format="dd/MM/yyyy", document=DOCUMENTO_EDOCUENTA)
	public Date getFechaUltimoAcuse() {
		return fechaUltimoAcuse;
	}

	public void setFechaUltimoAcuse(Date fechaUltimoAcuse) {
		this.fechaUltimoAcuse = fechaUltimoAcuse;
	}

	@Exportable(columnName="FECHA DE CARTA", columnOrder=4, format="dd/MM/yyyy", document=DOCUMENTO_EDOCUENTA)
	public Date getFechaElaboraCarta() {
		return fechaElaboraCarta;
	}

	public void setFechaElaboraCarta(Date fechaElaboraCarta) {
		this.fechaElaboraCarta = fechaElaboraCarta;
	}

	@Exportable(columnName="DIAS DE ATRASO", columnOrder=13, document=DOCUMENTO_EDOCUENTA)
	public Integer getDiasAtraso() {
		return diasAtraso;
	}

	public void setDiasAtraso(Integer diasAtraso) {
		this.diasAtraso = diasAtraso;
	}

	@Exportable(columnName="OBSERVACIONES", columnOrder=14, document=DOCUMENTO_EDOCUENTA)
	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	@Exportable(columnName="NUMERO RECUPERACION", columnOrder=1, document=DOCUMENTO_CARTA_ELABORAR)
	public String getNumeroRecuperacion() {
		return numeroRecuperacion;
	}

	public void setNumeroRecuperacion(String numeroRecuperacion) {
		this.numeroRecuperacion = numeroRecuperacion;
	}

	@Exportable(columnName="OFICINA", columnOrder=0, document=DOCUMENTO_CARTA_ELABORAR)
	public String getOficinaDesc() {
		return oficinaDesc;
	}

	public void setOficinaDesc(String oficinaDesc) {
		this.oficinaDesc = oficinaDesc;
	}

	@Exportable(columnName="COBERTURA", columnOrder=4, document=DOCUMENTO_CARTA_ELABORAR)
	public String getNombreCobertura() {
		return nombreCobertura;
	}

	public void setNombreCobertura(String nombreCobertura) {
		this.nombreCobertura = nombreCobertura;
	}

	@Exportable(columnName="PASE DE ATENCION", columnOrder=5, document=DOCUMENTO_CARTA_ELABORAR)
	public String getFolioPase() {
		return folioPase;
	}

	public void setFolioPase(String folioPase) {
		this.folioPase = folioPase;
	}
	
	@Exportable(columnName="LESIONADO", columnOrder=10, document=DOCUMENTO_EDOCUENTA)
	public String getLesionado() {
		return lesionado;
	}

	public void setLesionado(String lesionado) {
		this.lesionado = lesionado;
	}

	public CartaCiaDTO(){

	}

	/**
	 * @return the tipoCarta
	 */
	public String getTipoCarta() {
		return tipoCarta;
	}

	/**
	 * @param tipoCarta the tipoCarta to set
	 */
	public void setTipoCarta(String tipoCarta) {
		this.tipoCarta = tipoCarta;
	}

	/**
	 * @return the exclusiones
	 */
	public List<CartaCiaExclusionesDTO> getExclusiones() {
		return exclusiones;
	}

	/**
	 * @param exclusiones the exclusiones to set
	 */
	public void setExclusiones(List<CartaCiaExclusionesDTO> exclusiones) {
		this.exclusiones = exclusiones;
	}

	/**
	 * @return the ordenesCompra
	 */
	public List<OrdenCompraCartaCia> getOrdenesCompra() {
		return ordenesCompra;
	}

	/**
	 * @param ordenesCompra the ordenesCompra to set
	 */
	public void setOrdenesCompra(List<OrdenCompraCartaCia> ordenesCompra) {
		this.ordenesCompra = ordenesCompra;
	}

	public BigDecimal getMontoPiezas() {
		return montoPiezas;
	}

	public void setMontoPiezas(BigDecimal montoPiezas) {
		this.montoPiezas = montoPiezas;
	}
	
}