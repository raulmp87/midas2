/**
 * 
 */
package mx.com.afirme.midas2.dto.siniestros;

import java.util.Date;

/**
 * @author admin
 *
 */
public class EstatusCobranzaDTO {
	
	private Date fechaConsulta;
	
	private Long idToPoliza;
	
	private Integer numInciso;
	
	private String situacion;

	public Date getFechaConsulta() {
		return fechaConsulta;
	}

	public void setFechaConsulta(Date fechaConsulta) {
		this.fechaConsulta = fechaConsulta;
	}

	public Long getIdToPoliza() {
		return idToPoliza;
	}

	public void setIdToPoliza(Long idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	public Integer getNumInciso() {
		return numInciso;
	}

	public void setNumInciso(Integer numInciso) {
		this.numInciso = numInciso;
	}

	public String getSituacion() {
		return situacion;
	}

	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}
	
	
	
	

}
