package mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra;

import java.io.Serializable;
import java.math.BigDecimal;

import org.springframework.stereotype.Component;

@Component
public class ImpresionOrdenCompraDTO implements Serializable {
	public static final String JRXML_GMO_RCP = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/impresionOrdenCompraGMO.jrxml";
	public static final String JRXML_AYC_DMA_RCV = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/impresionOrdenCompraVehiculo.jrxml";
	public static final String JRXML_RCB = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/impresionOrdenCompraRCB.jrxml";
	public static final String JRXML_ROBO = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/impresionOrdenCompraRobo.jrxml";
	public static final String JRXML_GRAL = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/impresionOrdenCompra.jrxml";
	
	
	public static final Integer AUTORIZA_ELABORO =1;
	public static final Integer AUTORIZA_SUPERVISOR  =2;
	public static final Integer AUTORIZA_GERENCIA 	=3;
	public static final Integer AUTORIZA_DIRECCION  =4;
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1755096644644915875L;

	//DATOS REPORTE 
	//Orden de Compra Adaptaciones y Conversiones 
	//Reporte de Orden de Compra Daños Materiales 
	//	//Reporte de Orden de Compra R.C. Vehículo 
	private Long idOrdenCompra;
	
	private String vehiculo;
	private String modelo;
	private String placas;
	private String serie;	
	//Reporte de Orden de Compra Gastos Médicos. 
	//Reporte de Orden de Compra R.C. Personas 

	private String hospital;
	private String medico;
	private String estado;
	private String tipoAtencion;
	//Reporte de Orden de Compra R.C Inmuebles.
	//Reporte de Orden de Compra Robo.  
	private String tipoBien;
	
	private String dano;
	
	
	//---Detalle Todos los reportes
	private String aseguradoOcupanteTercero;
	private String aseguradoTercero;
	
	private String siniestro;
	private String oficina;
	private String folioPresupuesto;
	private String beneficiario;
	private String numProveedor;
	private String tipoSiniestro;
	private String terminoAjuste;
	private String terminaSiniestro;
	private String responsabilidad;
	private String cobertura;
	private String fechaSiniestro;
	private String fechaElaboracion;
	
	//firmas 
	private int  firmaElaboro=0;
	private int  firmaSupervisor =0;
	private int  firmaGerencia =0;
	private int  firmaDireccion =0;
	
	private String  nombreElaboro;
	private String  nombreSupervisor ;
	private String  nombreGerencia ;
	private String  nombreDireccion ;
	
	private String  fechaElaboro;//dd/mm/yy
	private String  fechaSupervisor ;//dd/mm/yy
	private String  fechaGerencia ;//dd/mm/yy
	private String  fechaDireccion ;//dd/mm/yy
	
	
	
	private BigDecimal subtotal;
	private BigDecimal iva;
	private BigDecimal isr;
	private BigDecimal ivaRetenido;
	
	private BigDecimal deducible;
	private BigDecimal total;
	private BigDecimal totalPagado;
	private BigDecimal totalPendiente;
	
	private String reporte;

	
	
	public String getReporte() {
		return reporte;
	}
	public void setReporte(String reporte) {
		this.reporte = reporte;
	}
	public Long getIdOrdenCompra() {
		return idOrdenCompra;
	}
	public void setIdOrdenCompra(Long idOrdenCompra) {
		this.idOrdenCompra = idOrdenCompra;
	}
	public String getVehiculo() {
		return vehiculo;
	}
	public void setVehiculo(String vehiculo) {
		this.vehiculo = vehiculo;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getPlacas() {
		return placas;
	}
	public void setPlacas(String placas) {
		this.placas = placas;
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public String getHospital() {
		return hospital;
	}
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}
	public String getMedico() {
		return medico;
	}
	public void setMedico(String medico) {
		this.medico = medico;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getTipoAtencion() {
		return tipoAtencion;
	}
	public void setTipoAtencion(String tipoAtencion) {
		this.tipoAtencion = tipoAtencion;
	}
	public String getTipoBien() {
		return tipoBien;
	}
	public void setTipoBien(String tipoBien) {
		this.tipoBien = tipoBien;
	}
	public String getDano() {
		return dano;
	}
	public void setDano(String dano) {
		this.dano = dano;
	}
	public String getAseguradoOcupanteTercero() {
		return aseguradoOcupanteTercero;
	}
	public void setAseguradoOcupanteTercero(String aseguradoOcupanteTercero) {
		this.aseguradoOcupanteTercero = aseguradoOcupanteTercero;
	}
	public String getAseguradoTercero() {
		return aseguradoTercero;
	}
	public void setAseguradoTercero(String aseguradoTercero) {
		this.aseguradoTercero = aseguradoTercero;
	}
	public String getSiniestro() {
		return siniestro;
	}
	public void setSiniestro(String siniestro) {
		this.siniestro = siniestro;
	}
	public String getOficina() {
		return oficina;
	}
	public void setOficina(String oficina) {
		this.oficina = oficina;
	}
	public String getFolioPresupuesto() {
		return folioPresupuesto;
	}
	public void setFolioPresupuesto(String folioPresupuesto) {
		this.folioPresupuesto = folioPresupuesto;
	}
	public String getBeneficiario() {
		return beneficiario;
	}
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}
	public String getNumProveedor() {
		return numProveedor;
	}
	public void setNumProveedor(String numProveedor) {
		this.numProveedor = numProveedor;
	}
	public String getTipoSiniestro() {
		return tipoSiniestro;
	}
	public void setTipoSiniestro(String tipoSiniestro) {
		this.tipoSiniestro = tipoSiniestro;
	}
	public String getTerminoAjuste() {
		return terminoAjuste;
	}
	public void setTerminoAjuste(String terminoAjuste) {
		this.terminoAjuste = terminoAjuste;
	}
	public String getTerminaSiniestro() {
		return terminaSiniestro;
	}
	public void setTerminaSiniestro(String terminaSiniestro) {
		this.terminaSiniestro = terminaSiniestro;
	}
	public String getResponsabilidad() {
		return responsabilidad;
	}
	public void setResponsabilidad(String responsabilidad) {
		this.responsabilidad = responsabilidad;
	}
	public String getCobertura() {
		return cobertura;
	}
	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}
	public String getFechaSiniestro() {
		return fechaSiniestro;
	}
	public void setFechaSiniestro(String fechaSiniestro) {
		this.fechaSiniestro = fechaSiniestro;
	}

	public String getFechaElaboracion() {
		return fechaElaboracion;
	}
	public void setFechaElaboracion(String fechaElaboracion) {
		this.fechaElaboracion = fechaElaboracion;
	}
	public int getFirmaElaboro() {
		return firmaElaboro;
	}
	public void setFirmaElaboro(int firmaElaboro) {
		this.firmaElaboro = firmaElaboro;
	}
	public int getFirmaSupervisor() {
		return firmaSupervisor;
	}
	public void setFirmaSupervisor(int firmaSupervisor) {
		this.firmaSupervisor = firmaSupervisor;
	}
	public int getFirmaGerencia() {
		return firmaGerencia;
	}
	public void setFirmaGerencia(int firmaGerencia) {
		this.firmaGerencia = firmaGerencia;
	}
	public int getFirmaDireccion() {
		return firmaDireccion;
	}
	public void setFirmaDireccion(int firmaDireccion) {
		this.firmaDireccion = firmaDireccion;
	}
	public String getNombreElaboro() {
		return nombreElaboro;
	}
	public void setNombreElaboro(String nombreElaboro) {
		this.nombreElaboro = nombreElaboro;
	}
	public String getNombreSupervisor() {
		return nombreSupervisor;
	}
	public void setNombreSupervisor(String nombreSupervisor) {
		this.nombreSupervisor = nombreSupervisor;
	}
	public String getNombreGerencia() {
		return nombreGerencia;
	}
	public void setNombreGerencia(String nombreGerencia) {
		this.nombreGerencia = nombreGerencia;
	}
	public String getNombreDireccion() {
		return nombreDireccion;
	}
	public void setNombreDireccion(String nombreDireccion) {
		this.nombreDireccion = nombreDireccion;
	}
	public String getFechaElaboro() {
		return fechaElaboro;
	}
	public void setFechaElaboro(String fechaElaboro) {
		this.fechaElaboro = fechaElaboro;
	}
	public String getFechaSupervisor() {
		return fechaSupervisor;
	}
	public void setFechaSupervisor(String fechaSupervisor) {
		this.fechaSupervisor = fechaSupervisor;
	}
	public String getFechaGerencia() {
		return fechaGerencia;
	}
	public void setFechaGerencia(String fechaGerencia) {
		this.fechaGerencia = fechaGerencia;
	}
	public String getFechaDireccion() {
		return fechaDireccion;
	}
	public void setFechaDireccion(String fechaDireccion) {
		this.fechaDireccion = fechaDireccion;
	}
	public BigDecimal getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}
	public BigDecimal getIva() {
		return iva;
	}
	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}
	public BigDecimal getDeducible() {
		return deducible;
	}
	public void setDeducible(BigDecimal deducible) {
		this.deducible = deducible;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public BigDecimal getTotalPagado() {
		return totalPagado;
	}
	public void setTotalPagado(BigDecimal totalPagado) {
		this.totalPagado = totalPagado;
	}
	public BigDecimal getTotalPendiente() {
		return totalPendiente;
	}
	public void setTotalPendiente(BigDecimal totalPendiente) {
		this.totalPendiente = totalPendiente;
	}
	public BigDecimal getIsr() {
		return isr;
	}
	public void setIsr(BigDecimal isr) {
		this.isr = isr;
	}
	public BigDecimal getIvaRetenido() {
		return ivaRetenido;
	}
	public void setIvaRetenido(BigDecimal ivaRetenido) {
		this.ivaRetenido = ivaRetenido;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	


	






	
	
	
	}
