package mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class MovimientoManualDTO {
	
	private Long idMovimiento;
	private Long idCuentaContable;
	private String codigoCuentaContable;
	private String cuentaContable;
	private String causaMovimiento;
	private String usuario;
	private Date fechaTraspasoCuenta;
	private Date fechaTraspasoCuentaDesde;
	private Date fechaTraspasoCuentaHasta;
	private BigDecimal importe;
	private BigDecimal importeDesde;
	private BigDecimal importeHasta;
	private String movManualConcat;
	private String fechaRegistro;
	/**
	 * @return the idMovimiento
	 */
	public Long getIdMovimiento() {
		return idMovimiento;
	}
	/**
	 * @param idMovimiento the idMovimiento to set
	 */
	public void setIdMovimiento(Long idMovimiento) {
		this.idMovimiento = idMovimiento;
	}
	/**
	 * @return the idCuentaContable
	 */
	public Long getIdCuentaContable() {
		return idCuentaContable;
	}
	/**
	 * @param idCuentaContable the idCuentaContable to set
	 */
	public void setIdCuentaContable(Long idCuentaContable) {
		this.idCuentaContable = idCuentaContable;
	}
	/**
	 * @return the cuentaContable
	 */
	public String getCuentaContable() {
		return cuentaContable;
	}
	/**
	 * @param cuentaContable the cuentaContable to set
	 */
	public void setCuentaContable(String cuentaContable) {
		this.cuentaContable = cuentaContable;
	}
	/**
	 * @return the causaMovimiento
	 */
	public String getCausaMovimiento() {
		return causaMovimiento;
	}
	/**
	 * @param causaMovimiento the causaMovimiento to set
	 */
	public void setCausaMovimiento(String causaMovimiento) {
		this.causaMovimiento = causaMovimiento;
	}
	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}
	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	/**
	 * @return the fechaTraspasoCuenta
	 */
	public Date getFechaTraspasoCuenta() {
		return fechaTraspasoCuenta;
	}
	/**
	 * @param fechaTraspasoCuenta the fechaTraspasoCuenta to set
	 */
	public void setFechaTraspasoCuenta(Date fechaTraspasoCuenta) {
		this.fechaTraspasoCuenta = fechaTraspasoCuenta;
	}
	/**
	 * @return the fechaTraspasoCuentaDesde
	 */
	public Date getFechaTraspasoCuentaDesde() {
		return fechaTraspasoCuentaDesde;
	}
	/**
	 * @param fechaTraspasoCuentaDesde the fechaTraspasoCuentaDesde to set
	 */
	public void setFechaTraspasoCuentaDesde(Date fechaTraspasoCuentaDesde) {
		this.fechaTraspasoCuentaDesde = fechaTraspasoCuentaDesde;
	}
	/**
	 * @return the fechaTraspasoCuentaHasta
	 */
	public Date getFechaTraspasoCuentaHasta() {
		return fechaTraspasoCuentaHasta;
	}
	/**
	 * @param fechaTraspasoCuentaHasta the fechaTraspasoCuentaHasta to set
	 */
	public void setFechaTraspasoCuentaHasta(Date fechaTraspasoCuentaHasta) {
		this.fechaTraspasoCuentaHasta = fechaTraspasoCuentaHasta;
	}
	/**
	 * @return the importe
	 */
	public BigDecimal getImporte() {
		return importe;
	}
	/**
	 * @param importe the importe to set
	 */
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}
	/**
	 * @return the importeDesde
	 */
	public BigDecimal getImporteDesde() {
		return importeDesde;
	}
	/**
	 * @param importeDesde the importeDesde to set
	 */
	public void setImporteDesde(BigDecimal importeDesde) {
		this.importeDesde = importeDesde;
	}
	/**
	 * @return the importeHasta
	 */
	public BigDecimal getImporteHasta() {
		return importeHasta;
	}
	/**
	 * @param importeHasta the importeHasta to set
	 */
	public void setImporteHasta(BigDecimal importeHasta) {
		this.importeHasta = importeHasta;
	}
	/**
	 * @return the movManualConcat
	 */
	public String getMovManualConcat() {
		return movManualConcat;
	}
	/**
	 * @param movManualConcat the movManualConcat to set
	 */
	public void setMovManualConcat(String movManualConcat) {
		this.movManualConcat = movManualConcat;
	}
	/**
	 * @return the fechaRegistro
	 */
	public String getFechaRegistro() {
		return fechaRegistro;
	}
	/**
	 * @param fechaRegistro the fechaRegistro to set
	 */
	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public String getCodigoCuentaContable() {
		return codigoCuentaContable;
	}
	public void setCodigoCuentaContable(String codigoCuentaContable) {
		this.codigoCuentaContable = codigoCuentaContable;
	}
	
	
}
