package mx.com.afirme.midas2.dto.siniestros.pagos.facturas;

import java.io.Serializable;

public class RelacionFacturaOrdenCompraDTO implements Serializable{ 

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;  
	
	private String idFactura;
	private String idOrdenCompra;
	
	
	public String getIdFactura() {
		return idFactura;
	}
	public void setIdFactura(String idFactura) {
		this.idFactura = idFactura;
	}
	public String getIdOrdenCompra() {
		return idOrdenCompra;
	}
	public void setIdOrdenCompra(String idOrdenCompra) {
		this.idOrdenCompra = idOrdenCompra;
	}

}
