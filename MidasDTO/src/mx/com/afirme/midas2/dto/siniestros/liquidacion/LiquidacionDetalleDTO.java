package mx.com.afirme.midas2.dto.siniestros.liquidacion;

import java.io.Serializable;
import java.math.BigDecimal;

public class LiquidacionDetalleDTO implements Serializable{ /**
	 * 
	 */
	private static final long serialVersionUID = 1185022779781955675L;
	private String noSiniestro;
	private Long numOrdenPago;
	private Long numOrdenCompra;
	private String tipoPago;
	private String conceptoPago;
	private String proveedor ;
	private String estatus; 
	private BigDecimal totales;
	private Long idDetalleLiquidacion;
	private Long idLiquidacion;
	private String numFactura; 
	private Long oficinaId;
	private String oficina;
	private String numSiniestro;
	private String beneficiario;
	private Long idProveedor;
	private String tipoProveedor;
	private String tipo;
	private String claveOficinaSiniestro;
	private String consecutivoReporteSiniestro;
	private String anioReporteSiniestro;
	
	
	public String getNoSiniestro() {
		return noSiniestro;
	}
	public void setNoSiniestro(String noSiniestro) {
		this.noSiniestro = noSiniestro;
	}
	public Long getNumOrdenPago() {
		return numOrdenPago;
	}
	public void setNumOrdenPago(Long numOrdenPago) {
		this.numOrdenPago = numOrdenPago;
	}
	public Long getNumOrdenCompra() {
		return numOrdenCompra;
	}
	public void setNumOrdenCompra(Long numOrdenCompra) {
		this.numOrdenCompra = numOrdenCompra;
	}
	public String getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	public String getConceptoPago() {
		return conceptoPago;
	}
	public void setConceptoPago(String conceptoPago) {
		this.conceptoPago = conceptoPago;
	}
	
	public String getProveedor() {
		return proveedor;
	}
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}
	
	public String getBeneficiario() {
		return beneficiario;
	}
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public BigDecimal getTotales() {
		return totales;
	}
	public void setTotales(BigDecimal totales) {
		this.totales = totales;
	}
	public Long getIdDetalleLiquidacion() {
		return idDetalleLiquidacion;
	}
	public void setIdDetalleLiquidacion(Long idDetalleLiquidacion) {
		this.idDetalleLiquidacion = idDetalleLiquidacion;
	}
	public Long getIdLiquidacion() {
		return idLiquidacion;
	}
	public void setIdLiquidacion(Long idLiquidacion) {
		this.idLiquidacion = idLiquidacion;
	}
	public String getNumFactura() {
		return numFactura;
	}
	public void setNumFactura(String numFactura) {
		this.numFactura = numFactura;
	}
	public String getAnioReporteSiniestro() {
		return anioReporteSiniestro;
	}
	public void setAnioReporteSiniestro(String anioReporteSiniestro) {
		this.anioReporteSiniestro = anioReporteSiniestro;
	}
	public Long getOficinaId() {
		return oficinaId;
	}
	public void setOficinaId(Long oficinaId) {
		this.oficinaId = oficinaId;
	}
	public String getOficina() {
		return oficina;
	}
	public void setOficina(String oficina) {
		this.oficina = oficina;
	}
	public String getNumSiniestro() {
		return numSiniestro;
	}
	public void setNumSiniestro(String numSiniestro) {
		this.numSiniestro = numSiniestro;
	}
	
	public Long getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(Long idProveedor) {
		this.idProveedor = idProveedor;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getClaveOficinaSiniestro() {
		return claveOficinaSiniestro;
	}
	public void setClaveOficinaSiniestro(String claveOficinaSiniestro) {
		this.claveOficinaSiniestro = claveOficinaSiniestro;
	}
	public String getConsecutivoReporteSiniestro() {
		return consecutivoReporteSiniestro;
	}
	public void setConsecutivoReporteSiniestro(String consecutivoReporteSiniestro) {
		this.consecutivoReporteSiniestro = consecutivoReporteSiniestro;
	}
	public String getTipoProveedor() {
		return tipoProveedor;
	}
	public void setTipoProveedor(String tipoProveedor) {
		this.tipoProveedor = tipoProveedor;
	}



}
