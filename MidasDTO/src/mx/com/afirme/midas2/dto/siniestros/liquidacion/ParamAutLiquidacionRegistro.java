package mx.com.afirme.midas2.dto.siniestros.liquidacion;

import java.io.Serializable;
import java.util.Date;

public class ParamAutLiquidacionRegistro implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String tipoLiquidacion;
	private String tipoLiquidacionDesc;
	private Boolean estatus;
	private String estatusDesc;
	private Date fechaActivo;
	private Date fechaInactivo; 
	private String liquidacion;
	private String liquidacionDesc;
	private String nombreConfiguracion; 
	private String nombreUsuarioConfigurador;
	private Long numero;
	private String tipoPago;
	private String tipoPagoDesc;
	private Short puedeAutorizar;
	
	public ParamAutLiquidacionRegistro() {
		
	}
	
	
	public ParamAutLiquidacionRegistro(Long id, String tipoLiquidacion,
			Boolean estatus, Date fechaActivo, Date fechaInactivo,
			String liquidacion, String nombreConfiguracion,
			String nombreUsuarioConfigurador, Long numero, String tipoPago) {
		super();
		this.id = id;
		this.tipoLiquidacion = tipoLiquidacion;
		this.estatus = estatus;
		this.fechaActivo = fechaActivo;
		this.fechaInactivo = fechaInactivo;
		this.liquidacion = liquidacion;
		this.nombreConfiguracion = nombreConfiguracion;
		this.nombreUsuarioConfigurador = nombreUsuarioConfigurador;
		this.numero = numero;
		this.tipoPago = tipoPago;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTipoLiquidacion() {
		return tipoLiquidacion;
	}
	public void setTipoLiquidacion(String tipoLiquidacion) {
		this.tipoLiquidacion = tipoLiquidacion;
	}
	public String getTipoLiquidacionDesc() {
		return tipoLiquidacionDesc;
	}
	public void setTipoLiquidacionDesc(String tipoLiquidacionDesc) {
		this.tipoLiquidacionDesc = tipoLiquidacionDesc;
	}
	public Boolean getEstatus() {
		return estatus;
	}
	public void setEstatus(Boolean estatus) {
		this.estatus = estatus;
	}
	public String getEstatusDesc() {
		return estatusDesc;
	}
	public void setEstatusDesc(String estatusDesc) {
		this.estatusDesc = estatusDesc;
	}
	public Date getFechaActivo() {
		return fechaActivo;
	}
	public void setFechaActivo(Date fechaActivo) {
		this.fechaActivo = fechaActivo;
	}
	public Date getFechaInactivo() {
		return fechaInactivo;
	}
	public void setFechaInactivo(Date fechaInactivo) {
		this.fechaInactivo = fechaInactivo;
	}
	public String getLiquidacion() {
		return liquidacion;
	}
	public void setLiquidacion(String liquidacion) {
		this.liquidacion = liquidacion;
	}
	public String getLiquidacionDesc() {
		return liquidacionDesc;
	}
	public void setLiquidacionDesc(String liquidacionDesc) {
		this.liquidacionDesc = liquidacionDesc;
	}
	public String getNombreConfiguracion() {
		return nombreConfiguracion;
	}
	public void setNombreConfiguracion(String nombreConfiguracion) {
		this.nombreConfiguracion = nombreConfiguracion;
	}
	public String getNombreUsuarioConfigurador() {
		return nombreUsuarioConfigurador;
	}
	public void setNombreUsuarioConfigurador(String nombreUsuarioConfigurador) {
		this.nombreUsuarioConfigurador = nombreUsuarioConfigurador;
	}
	public Long getNumero() {
		return numero;
	}
	public void setNumero(Long numero) {
		this.numero = numero;
	}
	public String getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	public String getTipoPagoDesc() {
		return tipoPagoDesc;
	}
	public void setTipoPagoDesc(String tipoPagoDesc) {
		this.tipoPagoDesc = tipoPagoDesc;
	}
	public Short getPuedeAutorizar() {
		return puedeAutorizar;
	}
	public void setPuedeAutorizar(Short puedeAutorizar) {
		this.puedeAutorizar = puedeAutorizar;
	}
	
	

}
