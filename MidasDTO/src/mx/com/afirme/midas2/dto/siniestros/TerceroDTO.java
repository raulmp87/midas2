package mx.com.afirme.midas2.dto.siniestros;

import java.io.Serializable;
import java.math.BigDecimal;

import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;

public class TerceroDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Boolean esDMA = false;
	private Boolean esRCV = false;
	private Boolean esGME = false;
	private Boolean esRCP = false;
	private Boolean esRCJ = false;
	private Boolean esRCB = false;
	private Boolean esGMC = false;
	
//	Danios Materiales 
	private String nombreTercero;
	private String marca;
	private String estilo;
	private String modelo;
	private String placas;
	private String color;

//	Gastos Medicos
	private String claveHospital;
	private String nombreHospital;
	private String claveMedico;
	private String nombreMedico;

//	Responsabilidad Civil
	private String tipoBien;
	private String pais;
	private String estado;
	private String municipio;
	private String colonia;
	private String domicilio;
	
	
	// Generales
	private String observacionesTercero;
	private String daniosPreexistentes;
	private PrestadorServicio taller = new PrestadorServicio();
	private PrestadorServicio ciaSeguros = new PrestadorServicio();
	private String nombrePrestador;
	private String tipoPrestador;
	private String domicilioPrestador;
	private String telefonoPrestador;
	private String numeroSerie;
	
	private String nombreCobertura;
	private BigDecimal estimacionActual;
	
	public Boolean getEsDMA() {
		return esDMA;
	}
	public void setEsDMA(Boolean esDMA) {
		this.esDMA = esDMA;
	}
	public Boolean getEsRCV() {
		return esRCV;
	}
	public void setEsRCV(Boolean esRCV) {
		this.esRCV = esRCV;
	}
	public Boolean getEsGME() {
		return esGME;
	}
	public void setEsGME(Boolean esGME) {
		this.esGME = esGME;
	}
	public Boolean getEsRCP() {
		return esRCP;
	}
	public void setEsRCP(Boolean esRCP) {
		this.esRCP = esRCP;
	}
	public Boolean getEsRCJ() {
		return esRCJ;
	}
	public void setEsRCJ(Boolean esRCJ) {
		this.esRCJ = esRCJ;
	}
	public Boolean getEsRCB() {
		return esRCB;
	}
	public void setEsRCB(Boolean esRCB) {
		this.esRCB = esRCB;
	}
	public Boolean getEsGMC() {
		return esGMC;
	}
	public void setEsGMC(Boolean esGMC) {
		this.esGMC = esGMC;
	}
	public String getNombreTercero() {
		return nombreTercero;
	}
	public void setNombreTercero(String nombreTercero) {
		this.nombreTercero = nombreTercero;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getEstilo() {
		return estilo;
	}
	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getPlacas() {
		return placas;
	}
	public void setPlacas(String placas) {
		this.placas = placas;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getClaveHospital() {
		return claveHospital;
	}
	public void setClaveHospital(String claveHospital) {
		this.claveHospital = claveHospital;
	}
	public String getNombreHospital() {
		return nombreHospital;
	}
	public void setNombreHospital(String nombreHospital) {
		this.nombreHospital = nombreHospital;
	}
	public String getClaveMedico() {
		return claveMedico;
	}
	public void setClaveMedico(String claveMedico) {
		this.claveMedico = claveMedico;
	}
	public String getNombreMedico() {
		return nombreMedico;
	}
	public void setNombreMedico(String nombreMedico) {
		this.nombreMedico = nombreMedico;
	}
	public String getTipoBien() {
		return tipoBien;
	}
	public void setTipoBien(String tipoBien) {
		this.tipoBien = tipoBien;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getColonia() {
		return colonia;
	}
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	public String getDomicilio() {
		return domicilio;
	}
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	
	public Boolean isEmpty(){
		if(
		( this.nombreTercero  == null || this.nombreTercero.isEmpty()) ||
		( this.marca          == null || this.marca.isEmpty()) ||
		( this.estilo         == null || this.estilo.isEmpty()) ||
		( this.modelo         == null || this.modelo.isEmpty()) ||
		( this.placas         == null || this.placas.isEmpty()) ||
		( this.color          == null || this.color.isEmpty()) ||
		( this.claveHospital  == null || this.claveHospital.isEmpty()) ||
		( this.nombreHospital == null || this.nombreHospital.isEmpty()) ||
		( this.claveMedico    == null || this.claveMedico.isEmpty()) ||
		( this.nombreMedico   == null || this.nombreMedico.isEmpty()) ||
		( this.tipoBien       == null || this.tipoBien.isEmpty()) ||
		( this.pais           == null || this.pais.isEmpty()) ||
		( this.estado         == null || this.estado.isEmpty()) ||
		( this.municipio      == null || this.municipio.isEmpty()) ||
		( this.colonia        == null || this.colonia.isEmpty()) ||
		( this.domicilio      == null || this.domicilio.isEmpty())
		){
			return true;
		}
		return false;
	}
	public String getObservacionesTercero() {
		return observacionesTercero;
	}
	public void setObservacionesTercero(String observacionesTercero) {
		this.observacionesTercero = observacionesTercero;
	}
	public String getDaniosPreexistentes() {
		return daniosPreexistentes;
	}
	public void setDaniosPreexistentes(String daniosPreexistentes) {
		this.daniosPreexistentes = daniosPreexistentes;
	}
	public PrestadorServicio getTaller() {
		return taller;
	}
	public void setTaller(PrestadorServicio taller) {
		this.taller = taller;
	}
	public PrestadorServicio getCiaSeguros() {
		return ciaSeguros;
	}
	public void setCiaSeguros(PrestadorServicio ciaSeguros) {
		this.ciaSeguros = ciaSeguros;
	}
	public String getNombrePrestador() {
		return nombrePrestador;
	}
	public void setNombrePrestador(String nombrePrestador) {
		this.nombrePrestador = nombrePrestador;
	}
	public String getTipoPrestador() {
		return tipoPrestador;
	}
	public void setTipoPrestador(String tipoPrestador) {
		this.tipoPrestador = tipoPrestador;
	}
	public String getDomicilioPrestador() {
		return domicilioPrestador;
	}
	public void setDomicilioPrestador(String domicilioPrestador) {
		this.domicilioPrestador = domicilioPrestador;
	}
	public String getTelefonoPrestador() {
		return telefonoPrestador;
	}
	public void setTelefonoPrestador(String telefonoPrestador) {
		this.telefonoPrestador = telefonoPrestador;
	}
	public String getNumeroSerie() {
		return numeroSerie;
	}
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	public String getNombreCobertura() {
		return nombreCobertura;
	}
	public void setNombreCobertura(String nombreCobertura) {
		this.nombreCobertura = nombreCobertura;
	}
	public BigDecimal getEstimacionActual() {
		return estimacionActual;
	}
	public void setEstimacionActual(BigDecimal estimacionActual) {
		this.estimacionActual = estimacionActual;
	}
	
	
	
}
