package mx.com.afirme.midas2.dto.siniestros.catalogos.solicitudautorizacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class SolicitudAutorizacionAntiguedadDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String claveSubCalculo;
	private String coberturaId;
	private String condicionAntiguedad;
	private String condicionMonto;
	private String estatus;
	private Date fechaFin;
	private Date fechaIni;
	private Integer meses;
	private BigDecimal monto;
	private String numeroSiniestro;
	private Long oficinaId;
	private String tipoAjuste;
	private String tipoEstimacion;
	private String tipoSiniestro;
	public String getClaveSubCalculo() {
		return claveSubCalculo;
	}
	public void setClaveSubCalculo(String claveSubCalculo) {
		this.claveSubCalculo = claveSubCalculo;
	}
	public String getCoberturaId() {
		return coberturaId;
	}
	public void setCoberturaId(String coberturaId) {
		this.coberturaId = coberturaId;
	}
	public String getCondicionAntiguedad() {
		return condicionAntiguedad;
	}
	public void setCondicionAntiguedad(String condicionAntiguedad) {
		this.condicionAntiguedad = condicionAntiguedad;
	}
	public String getCondicionMonto() {
		return condicionMonto;
	}
	public void setCondicionMonto(String condicionMonto) {
		this.condicionMonto = condicionMonto;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	public Date getFechaIni() {
		return fechaIni;
	}
	public void setFechaIni(Date fechaIni) {
		this.fechaIni = fechaIni;
	}
	public Integer getMeses() {
		return meses;
	}
	public void setMeses(Integer meses) {
		this.meses = meses;
	}
	public BigDecimal getMonto() {
		return monto;
	}
	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}
	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}
	public Long getOficinaId() {
		return oficinaId;
	}
	public void setOficinaId(Long oficinaId) {
		this.oficinaId = oficinaId;
	}
	public String getTipoAjuste() {
		return tipoAjuste;
	}
	public void setTipoAjuste(String tipoAjuste) {
		this.tipoAjuste = tipoAjuste;
	}
	public String getTipoEstimacion() {
		return tipoEstimacion;
	}
	public void setTipoEstimacion(String tipoEstimacion) {
		this.tipoEstimacion = tipoEstimacion;
	}
	public String getTipoSiniestro() {
		return tipoSiniestro;
	}
	public void setTipoSiniestro(String tipoSiniestro) {
		this.tipoSiniestro = tipoSiniestro;
	}
	
	

}
