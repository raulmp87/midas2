package mx.com.afirme.midas2.dto.siniestros.hgs;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)

public class EfileDevolucion implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6448280792231029833L;
	
	@XmlElement(name="orden_compra_id",required=true)
	private Long ordenCompraId;
	
	@XmlElement(name="valuacion_id",required=true)
	private Long valuacionId;
	
	@XmlElement(name="nombre_admin_refacciones",required=true)
	private String nombreAdminRefacciones;
	
	@XmlElement(name="nombre_persona_devolucion",required=true)
	private String nomprePersonaDevolucion;
	
	@XmlElement(name="motivo",required=true)
	private String motivo;
	
	@XmlElement(name="cantidad_refacciones",required=true)
	private int cantidadRefacciones;
	
	@XmlElement(name="subtotal",required=true)
	private BigDecimal subtotal;
	
	@XmlElement(name="iva",required=true)
	private BigDecimal iva;
	
	@XmlElement(name="monto_total",required=true)
	private BigDecimal montoTotal;
	
	@XmlElement(name="devolucionRefacciones",required=false)
	private EfileDevolucionRefacciones[] efileDevolucionRefacciones;
	
	

	public Long getOrdenCompraId() {
		return ordenCompraId;
	}
	public void setOrdenCompraId(Long ordenCompraId) {
		this.ordenCompraId = ordenCompraId;
	}
	public Long getValuacionId() {
		return valuacionId;
	}
	public void setValuacionId(Long valuacionId) {
		this.valuacionId = valuacionId;
	}
	public String getNombreAdminRefacciones() {
		return nombreAdminRefacciones;
	}
	public void setNombreAdminRefacciones(String nombreAdminRefacciones) {
		this.nombreAdminRefacciones = nombreAdminRefacciones;
	}
	public String getNomprePersonaDevolucion() {
		return nomprePersonaDevolucion;
	}
	public void setNomprePersonaDevolucion(String nomprePersonaDevolucion) {
		this.nomprePersonaDevolucion = nomprePersonaDevolucion;
	}
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public int getCantidadRefacciones() {
		return cantidadRefacciones;
	}
	public void setCantidadRefacciones(int cantidadRefacciones) {
		this.cantidadRefacciones = cantidadRefacciones;
	}
	public BigDecimal getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}
	public BigDecimal getIva() {
		return iva;
	}
	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}
	public BigDecimal getMontoTotal() {
		return montoTotal;
	}
	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}
	@Override
	public String toString() {
		return "EfileDevolucion [ordenCompraId=" + ordenCompraId
				+ ", valuacionId=" + valuacionId + ", nombreAdminRefacciones="
				+ nombreAdminRefacciones + ", nomprePersonaDevolucion="
				+ nomprePersonaDevolucion + ", motivo=" + motivo
				+ ", cantidadRefacciones=" + cantidadRefacciones
				+ ", subtotal=" + subtotal + ", iva=" + iva + ", montoTotal="
				+ montoTotal + ", efileDevolucionRefacciones="
				+ Arrays.toString(efileDevolucionRefacciones) + "]";
	}
	public EfileDevolucionRefacciones[] getEfileDevolucionRefacciones() {
		return efileDevolucionRefacciones;
	}
	public void setEfileDevolucionRefacciones(
			EfileDevolucionRefacciones[] efileDevolucionRefacciones) {
		this.efileDevolucionRefacciones = efileDevolucionRefacciones;
	}
	
	

}
