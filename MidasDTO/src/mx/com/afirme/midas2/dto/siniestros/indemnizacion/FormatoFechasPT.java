package mx.com.afirme.midas2.dto.siniestros.indemnizacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.IndemnizacionSiniestro;

/**
 * DTO para mostrar los datos de solo lectura en la pantalla de formato fechas PT.
 * @author simavera
 */
public class FormatoFechasPT implements Serializable{

	private static final long serialVersionUID = 2215610688702161597L;
	private ReporteCabina reporteCabina;
	private Date fechaRecepcionDoc;
	private Date fechaAutorizacionSubSiniestro;
	private Date fechaRecepOperaciones;
	private Date fechaLiquidacionEgresos;
	private Date fechaAutorizacionOperaciones;
	private Date fechaRecepcionFinanzas;
	private Date fechaRecepcionCheque;
	private Date fechaEntregado;
	
	private BigDecimal importePrimaPend; 
	private Integer investigacion; 
	private String usuarioFinanzas;
	private String tipoPerdida; 
	private String comentarios;
	private String tipo;
	private Long idCartaSiniestro;
	private IndemnizacionSiniestro indemnizacion;
	
	public FormatoFechasPT(){

	}

	/**
	 * @return the reporteCabina
	 */
	public ReporteCabina getReporteCabina() {
		return reporteCabina;
	}

	/**
	 * @param reporteCabina the reporteCabina to set
	 */
	public void setReporteCabina(ReporteCabina reporteCabina) {
		this.reporteCabina = reporteCabina;
	}

	/**
	 * @return the fechaRecepcionDoc
	 */
	public Date getFechaRecepcionDoc() {
		return fechaRecepcionDoc;
	}

	/**
	 * @param fechaRecepcionDoc the fechaRecepcionDoc to set
	 */
	public void setFechaRecepcionDoc(Date fechaRecepcionDoc) {
		this.fechaRecepcionDoc = fechaRecepcionDoc;
	}

	/**
	 * @return the fechaAutorizacionSubSiniestro
	 */
	public Date getFechaAutorizacionSubSiniestro() {
		return fechaAutorizacionSubSiniestro;
	}

	/**
	 * @param fechaAutorizacionSubSiniestro the fechaAutorizacionSubSiniestro to set
	 */
	public void setFechaAutorizacionSubSiniestro(Date fechaAutorizacionSubSiniestro) {
		this.fechaAutorizacionSubSiniestro = fechaAutorizacionSubSiniestro;
	}

	/**
	 * @return the fechaRecepOperaciones
	 */
	public Date getFechaRecepOperaciones() {
		return fechaRecepOperaciones;
	}

	/**
	 * @param fechaRecepOperaciones the fechaRecepOperaciones to set
	 */
	public void setFechaRecepOperaciones(Date fechaRecepOperaciones) {
		this.fechaRecepOperaciones = fechaRecepOperaciones;
	}

	/**
	 * @return the fechaLiquidacionEgresos
	 */
	public Date getFechaLiquidacionEgresos() {
		return fechaLiquidacionEgresos;
	}

	/**
	 * @param fechaLiquidacionEgresos the fechaLiquidacionEgresos to set
	 */
	public void setFechaLiquidacionEgresos(Date fechaLiquidacionEgresos) {
		this.fechaLiquidacionEgresos = fechaLiquidacionEgresos;
	}

	/**
	 * @return the fechaAutorizacionOperaciones
	 */
	public Date getFechaAutorizacionOperaciones() {
		return fechaAutorizacionOperaciones;
	}

	/**
	 * @param fechaAutorizacionOperaciones the fechaAutorizacionOperaciones to set
	 */
	public void setFechaAutorizacionOperaciones(Date fechaAutorizacionOperaciones) {
		this.fechaAutorizacionOperaciones = fechaAutorizacionOperaciones;
	}

	/**
	 * @return the fechaRecepcionFinanzas
	 */
	public Date getFechaRecepcionFinanzas() {
		return fechaRecepcionFinanzas;
	}

	/**
	 * @param fechaRecepcionFinanzas the fechaRecepcionFinanzas to set
	 */
	public void setFechaRecepcionFinanzas(Date fechaRecepcionFinanzas) {
		this.fechaRecepcionFinanzas = fechaRecepcionFinanzas;
	}

	/**
	 * @return the fechaRecepcionCheque
	 */
	public Date getFechaRecepcionCheque() {
		return fechaRecepcionCheque;
	}

	/**
	 * @param fechaRecepcionCheque the fechaRecepcionCheque to set
	 */
	public void setFechaRecepcionCheque(Date fechaRecepcionCheque) {
		this.fechaRecepcionCheque = fechaRecepcionCheque;
	}

	/**
	 * @return the fechaEntregado
	 */
	public Date getFechaEntregado() {
		return fechaEntregado;
	}

	/**
	 * @param fechaEntregado the fechaEntregado to set
	 */
	public void setFechaEntregado(Date fechaEntregado) {
		this.fechaEntregado = fechaEntregado;
	}

	/**
	 * @return the importePrimaPend
	 */
	public BigDecimal getImportePrimaPend() {
		return importePrimaPend;
	}

	/**
	 * @param importePrimaPend the importePrimaPend to set
	 */
	public void setImportePrimaPend(BigDecimal importePrimaPend) {
		this.importePrimaPend = importePrimaPend;
	}

	/**
	 * @return the investigacion
	 */
	public Integer getInvestigacion() {
		return investigacion;
	}

	/**
	 * @param investigacion the investigacion to set
	 */
	public void setInvestigacion(Integer investigacion) {
		this.investigacion = investigacion;
	}

	/**
	 * @return the usuarioFinanzas
	 */
	public String getUsuarioFinanzas() {
		return usuarioFinanzas;
	}

	/**
	 * @param usuarioFinanzas the usuarioFinanzas to set
	 */
	public void setUsuarioFinanzas(String usuarioFinanzas) {
		this.usuarioFinanzas = usuarioFinanzas;
	}

	/**
	 * @return the tipoPerdida
	 */
	public String getTipoPerdida() {
		return tipoPerdida;
	}

	/**
	 * @param tipoPerdida the tipoPerdida to set
	 */
	public void setTipoPerdida(String tipoPerdida) {
		this.tipoPerdida = tipoPerdida;
	}

	/**
	 * @return the comentarios
	 */
	public String getComentarios() {
		return comentarios;
	}

	/**
	 * @param comentarios the comentarios to set
	 */
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the idCartaSiniestro
	 */
	public Long getIdCartaSiniestro() {
		return idCartaSiniestro;
	}

	/**
	 * @param idCartaSiniestro the idCartaSiniestro to set
	 */
	public void setIdCartaSiniestro(Long idCartaSiniestro) {
		this.idCartaSiniestro = idCartaSiniestro;
	}

	/**
	 * @return the indemnizacion
	 */
	public IndemnizacionSiniestro getIndemnizacion() {
		return indemnizacion;
	}

	/**
	 * @param indemnizacion the indemnizacion to set
	 */
	public void setIndemnizacion(IndemnizacionSiniestro indemnizacion) {
		this.indemnizacion = indemnizacion;
	}

}