/**
 * 
 */
package mx.com.afirme.midas2.dto.siniestros.pagos.facturas;

import java.io.Serializable;

import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;

/**
 * @author admin
 *
 */
public class ReporteFacturaDevolucionDTO  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6516852128254582883L;

	private DatosGralOrdenCompraDTO datosGralOrdenCompraDTO;
	
	private DocumentoFiscal facturaSiniestro;

	/**
	 * @return the datosGralOrdenCompraDTO
	 */
	public DatosGralOrdenCompraDTO getDatosGralOrdenCompraDTO() {
		return datosGralOrdenCompraDTO;
	}

	/**
	 * @param datosGralOrdenCompraDTO the datosGralOrdenCompraDTO to set
	 */
	public void setDatosGralOrdenCompraDTO(
			DatosGralOrdenCompraDTO datosGralOrdenCompraDTO) {
		this.datosGralOrdenCompraDTO = datosGralOrdenCompraDTO;
	}

	/**
	 * @return the facturaSiniestro
	 */
	public DocumentoFiscal getFacturaSiniestro() {
		return facturaSiniestro;
	}

	/**
	 * @param facturaSiniestro the facturaSiniestro to set
	 */
	public void setFacturaSiniestro(DocumentoFiscal facturaSiniestro) {
		this.facturaSiniestro = facturaSiniestro;
	}
	
	

}
