package mx.com.afirme.midas2.dto.siniestros.depuracion;

import java.io.Serializable;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class DepuracionReservaDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7601363225584825009L;
	
	private String claveSubCalculo;
	private Long coberturaId;
	private String contratante;
	private String estatus;
	private Date fechaFinActivo;
	private Date fechaFinInactivo;
	private Date fechaIniActivo;
	private Date fechaIniInactivo;
	private Long negocioId;
	private String nombreConfiguracion;
	private Integer numeroInciso;
	private String numeroPoliza;
	private String numeroSiniestro;
	private Long oficinaId;
	private Long seccionId;
	private String tipoServicio;
	
	public String getClaveSubCalculo() {
		return claveSubCalculo;
	}
	public void setClaveSubCalculo(String claveSubCalculo) {
		this.claveSubCalculo = claveSubCalculo;
	}
	public Long getCoberturaId() {
		return coberturaId;
	}
	public void setCoberturaId(Long coberturaId) {
		this.coberturaId = coberturaId;
	}
	public String getContratante() {
		return contratante;
	}
	public void setContratante(String contratante) {
		this.contratante = contratante;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public Date getFechaFinActivo() {
		return fechaFinActivo;
	}
	public void setFechaFinActivo(Date fechaFinActivo) {
		this.fechaFinActivo = fechaFinActivo;
	}
	public Date getFechaFinInactivo() {
		return fechaFinInactivo;
	}
	public void setFechaFinInactivo(Date fechaFinInactivo) {
		this.fechaFinInactivo = fechaFinInactivo;
	}
	public Date getFechaIniActivo() {
		return fechaIniActivo;
	}
	public void setFechaIniActivo(Date fechaIniActivo) {
		this.fechaIniActivo = fechaIniActivo;
	}
	public Date getFechaIniInactivo() {
		return fechaIniInactivo;
	}
	public void setFechaIniInactivo(Date fechaIniInactivo) {
		this.fechaIniInactivo = fechaIniInactivo;
	}
	public Long getNegocioId() {
		return negocioId;
	}
	public void setNegocioId(Long negocioId) {
		this.negocioId = negocioId;
	}
	public String getNombreConfiguracion() {
		return nombreConfiguracion;
	}
	public void setNombreConfiguracion(String nombreConfiguracion) {
		this.nombreConfiguracion = nombreConfiguracion;
	}
	public Integer getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}
	public Long getOficinaId() {
		return oficinaId;
	}
	public void setOficinaId(Long oficinaId) {
		this.oficinaId = oficinaId;
	}
	public Long getSeccionId() {
		return seccionId;
	}
	public void setSeccionId(Long seccionId) {
		this.seccionId = seccionId;
	}
	public String getTipoServicio() {
		return tipoServicio;
	}
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
}
