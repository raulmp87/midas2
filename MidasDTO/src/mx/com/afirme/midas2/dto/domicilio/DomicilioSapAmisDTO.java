package mx.com.afirme.midas2.dto.domicilio;

import java.io.Serializable;

import javax.persistence.*;


@Entity
@Table(name="Sap_Amis_Ubicacion", schema = "MIDAS")
public class DomicilioSapAmisDTO implements mx.com.afirme.midas2.dao.catalogos.Entidad {


	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idPais", column = @Column(name = "ID_PAIS", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idEstado", column = @Column(name = "ID_ESTADO", nullable = false, precision = 22, scale = 0)),
			@AttributeOverride(name = "idMunicipio", column = @Column(name = "ID_MUNICIPIO", nullable = false, precision = 22, scale = 0)) })
	private IdDomicilioSapAmisDTO id;
	
	// Property accessors

	@Column(name="DESC_PAIS")
	private String descPais;
	
	@Column(name="DESC_ESTADO")
	private String descEstado;
	
	@Column(name="DESC_MUNICIPIO")
	private String descMunicipio;
	
	@Column(name="CVE_SEYCOS_PAIS")
	private String idPaisSeycos;
	
	@Column(name="CVE_SEYCOS_ESTADO")
	private String idEstadoSeycos;
	
	@Column(name="CVE_SEYCOS_MUNICIPIO")
	private String idMunicipioSeycos;

	public String getIdPaisSeycos() {
		return idPaisSeycos;
	}

	public void setIdPaisSeycos(String idPaisSeycos) {
		this.idPaisSeycos = idPaisSeycos;
	}

	public String getIdEstadoSeycos() {
		return idEstadoSeycos;
	}

	public void setIdEstadoSeycos(String idEstadoSeycos) {
		this.idEstadoSeycos = idEstadoSeycos;
	}

	public String getIdMunicipioSeycos() {
		return idMunicipioSeycos;
	}

	public void setIdMunicipioSeycos(String idMunicipioSeycos) {
		this.idMunicipioSeycos = idMunicipioSeycos;
	}
	
	

	public String getDescPais() {
		return descPais;
	}

	public void setDescPais(String descPais) {
		this.descPais = descPais;
	}

	public String getDescEstado() {
		return descEstado;
	}

	public void setDescEstado(String descEstado) {
		this.descEstado = descEstado;
	}

	public String getDescMunicipio() {
		return descMunicipio;
	}

	public void setDescMunicipio(String descMunicipio) {
		this.descMunicipio = descMunicipio;
	}
	
	public IdDomicilioSapAmisDTO getId() {
		return this.id;
	}
	
	public void setId(IdDomicilioSapAmisDTO id) {
		this.id = id;
	}

	@SuppressWarnings("unchecked")
	@Override
	public IdDomicilioSapAmisDTO getKey() {
		// TODO Auto-generated method stub
		return this.id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
