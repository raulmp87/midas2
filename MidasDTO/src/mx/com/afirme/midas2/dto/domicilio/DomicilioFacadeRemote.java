package mx.com.afirme.midas2.dto.domicilio;

import java.sql.SQLException;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Remote;

import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.DomicilioImpresion;


public interface DomicilioFacadeRemote {
	public static enum TipoDomicilio{
		PERSONAL("PERS"),FISCAL("FISC"),OFICINA("OFIC"),CENTRO_OPERACION("CE_OP"),GERENCIA("GERE"),EJECUTIVO("EJEC"),PROMOTORIA("PROM"),AFIANZADORA("AFIA");
		private String value;
		private TipoDomicilio(String value){
			this.value=value;
		}
		public String getValue(){
			return value;
		}
	}
	public Long save(Domicilio domicilio,String nombreUsuario) throws SQLException,Exception;
	
	public Long update(Domicilio domicilio,String nombreUsuario) throws SQLException,Exception;
	
	public Domicilio findById(Long idDomicilio) throws SQLException,Exception;
	
	public Domicilio findById(Long idDomicilio,Long idPersona) throws SQLException,Exception;
	
	public List<Domicilio> findByPerson(Long idPersona) throws Exception;
	
	public List<Domicilio> findByFilters(Domicilio domicilio);
	
	public void remove(Long idDomicilio) throws Exception;
	
	public TipoDomicilio getTipoDomicilioPorClave(String id);
	
	public List<Domicilio> findDomiciliosActualesPorPersona(Long idPersona,String tipoDomicilio,String fechaConsulta);
	
	public DomicilioImpresion findDomicilioImpresionById(Long idDomicilio) throws SQLException,Exception;;
	
	public DomicilioImpresion findDomicilioImpresionById(Long idDomicilio, Long idPersona) throws SQLException,Exception;;
	
	public List<DomicilioImpresion> findDomicilioImpresionByFilters(DomicilioImpresion domicilio);
}
