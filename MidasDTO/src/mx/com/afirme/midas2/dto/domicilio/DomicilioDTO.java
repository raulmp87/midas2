package mx.com.afirme.midas2.dto.domicilio;

import mx.com.afirme.midas2.dto.persona.PersonaSeycosDTO;

/**
 * DTO Para guardar domicilio en seycos
 * @author vmhersil
 *
 */
public class DomicilioDTO {
	private Long idDomicilio;
	private String codigoPostal;
	private Long idColonia;
	private String nombreColonia;
	private String nombreCalle;
	private PersonaSeycosDTO persona;
	public Long getIdDomicilio() {
		return idDomicilio;
	}
	public void setIdDomicilio(Long idDomicilio) {
		this.idDomicilio = idDomicilio;
	}
	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public Long getIdColonia() {
		return idColonia;
	}
	public void setIdColonia(Long idColonia) {
		this.idColonia = idColonia;
	}
	public String getNombreColonia() {
		return nombreColonia;
	}
	public void setNombreColonia(String nombreColonia) {
		this.nombreColonia = nombreColonia;
	}
	public String getNombreCalle() {
		return nombreCalle;
	}
	public void setNombreCalle(String nombreCalle) {
		this.nombreCalle = nombreCalle;
	}
	public PersonaSeycosDTO getPersona() {
		return persona;
	}
	public void setPersona(PersonaSeycosDTO persona) {
		this.persona = persona;
	}
}
