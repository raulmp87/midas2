package mx.com.afirme.midas2.dto.domicilio;

import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * IdDomicilioSapAmisDTO entity. @author Luis Ibarra
 */
@Embeddable
public class IdDomicilioSapAmisDTO implements java.io.Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name="ID_PAIS")
	private int idPais;
	
	@Column(name="ID_ESTADO")
	private int idEstado;
	
	@Column(name="ID_MUNICIPIO")
	private int idMunicipio;
	
	public int getIdPais() {
		return idPais;
	}

	public void setIdPais(int idPais) {
		this.idPais = idPais;
	}

	public int getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(int idEstado) {
		this.idEstado = idEstado;
	}

	public int getIdMunicipio() {
		return idMunicipio;
	}

	public void setIdMunicipio(int idMunicipio) {
		this.idMunicipio = idMunicipio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idEstado;
		result = prime * result + idMunicipio;
		result = prime * result + idPais;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IdDomicilioSapAmisDTO other = (IdDomicilioSapAmisDTO) obj;
		if (idEstado != other.idEstado)
			return false;
		if (idMunicipio != other.idMunicipio)
			return false;
		if (idPais != other.idPais)
			return false;
		return true;
	}
}
