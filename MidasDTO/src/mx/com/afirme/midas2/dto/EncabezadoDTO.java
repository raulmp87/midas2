package mx.com.afirme.midas2.dto;

import java.io.Serializable;

public class EncabezadoDTO implements Serializable, Comparable<EncabezadoDTO> {

	private static final long serialVersionUID = -5622772819047010977L;
	
	private Integer secuencia;
	private String etiqueta;
	
	
	public Integer getSecuencia() {
		return secuencia;
	}
	
	public void setSecuencia(Integer secuencia) {
		this.secuencia = secuencia;
	}
	public String getEtiqueta() {
		return etiqueta;
	}
	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((etiqueta == null) ? 0 : etiqueta.hashCode());
		result = prime * result
				+ ((secuencia == null) ? 0 : secuencia.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EncabezadoDTO other = (EncabezadoDTO) obj;
		if (etiqueta == null) {
			if (other.etiqueta != null)
				return false;
		} else if (!etiqueta.equals(other.etiqueta))
			return false;
		if (secuencia == null) {
			if (other.secuencia != null)
				return false;
		} else if (!secuencia.equals(other.secuencia))
			return false;
		return true;
	}

	@Override
	public int compareTo(EncabezadoDTO o) {
		return this.secuencia.compareTo(o.secuencia);
	}
	
	
}
