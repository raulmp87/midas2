package mx.com.afirme.midas2.dto.juridico;

import java.math.BigDecimal;
import java.util.Date;

import mx.com.afirme.midas2.annotation.Exportable;

public class ReclamacionOficioJuridicoAaDTO extends ReclamacionOficioJuridicoDTO{

	private static final long serialVersionUID = 6477955908375640792L;

	public ReclamacionOficioJuridicoAaDTO(){
	}
	
	public ReclamacionOficioJuridicoAaDTO(ReclamacionOficioJuridicoDTO reclamacionOficio){
		super.setActor(reclamacionOficio.getActor());
		super.setAsegurado(reclamacionOficio.getAsegurado());
		super.setAsignado(reclamacionOficio.getAsignado());
		super.setClasificacionReclamacion(reclamacionOficio.getClasificacionReclamacion());
		super.setClasificacionReclamacionDesc(getClasificacionReclamacionDesc());
		super.setDelegacion(reclamacionOficio.getDelegacion());
		super.setDelegacionDesc(reclamacionOficio.getDelegacionDesc());
		super.setDemandado(reclamacionOficio.getDemandado());
		super.setEntidadFederativa(reclamacionOficio.getEntidadFederativa());
		super.setEstado(reclamacionOficio.getEstado());
		super.setEstadoDesc(reclamacionOficio.getEstadoDesc());
		super.setEstatus(reclamacionOficio.getEstatus());
		super.setEstatusDesc(reclamacionOficio.getEstatusDesc());
		super.setEsUltimoOficio(reclamacionOficio.getEsUltimoOficio());
		super.setEtapaJuicio(reclamacionOficio.getEtapaJuicio());
		super.setEtapaJuicioDesc(reclamacionOficio.getEtapaJuicioDesc());
		super.setExpedienteJuridico(reclamacionOficio.getExpedienteJuridico());
		super.setFechaNotificacion(reclamacionOficio.getFechaNotificacion());
		super.setIdtopoliza(reclamacionOficio.getIdtopoliza());
		super.setJustificacion(reclamacionOficio.getJustificacion());
		super.setMontoReclamado(reclamacionOficio.getMontoReclamado());
		super.setMotivoReclamacion(reclamacionOficio.getMotivoReclamacion());
		super.setMotivoReclamacionDesc(reclamacionOficio.getMotivoReclamacionDesc());
		super.setMunicipio(reclamacionOficio.getMunicipio());
		super.setMunicipioDesc(reclamacionOficio.getMunicipioDesc());
		super.setNumeroOficio(reclamacionOficio.getNumeroOficio());
		super.setNumeroPoliza(reclamacionOficio.getNumeroPoliza());
		super.setNumeroReclamacion(reclamacionOficio.getNumeroReclamacion());
		super.setNumeroSiniestro(reclamacionOficio.getNumeroSiniestro());
		super.setOficinaJuridica(reclamacionOficio.getOficinaJuridica());
		super.setOficinaJuridicaDesc(reclamacionOficio.getOficinaJuridicaDesc());
		super.setOficinaSiniestro(reclamacionOficio.getOficinaSiniestro());
		super.setOficinaSiniestroDesc(reclamacionOficio.getOficinaSiniestroDesc());
		super.setParteEnJuicio(reclamacionOficio.getParteEnJuicio());
		super.setProbabilidadExito(reclamacionOficio.getProbabilidadExito());
		super.setProcedimiento(reclamacionOficio.getProcedimiento());
		super.setProcedimientoDesc(reclamacionOficio.getProcedimientoDesc());
		super.setRamo(reclamacionOficio.getRamo());
		super.setRamoDesc(reclamacionOficio.getRamoDesc());
		super.setReclamante(reclamacionOficio.getReclamante());
		super.setSiniestroId(reclamacionOficio.getSiniestroId());
		super.setTipoQueja(reclamacionOficio.getTipoQueja());
		super.setTipoQuejaDesc(reclamacionOficio.getTipoQuejaDesc());
		super.setTipoReclamacion(reclamacionOficio.getTipoReclamacion());
		super.setTipoReclamacionDesc(reclamacionOficio.getTipoReclamacionDesc());
		super.setTotalOficios(reclamacionOficio.getTotalOficios());
		super.setUbicacion(reclamacionOficio.getUbicacion());
	}

	/**
	 * @return the actor
	 */
	public String getActor() {
		return super.getActor();
	}
	/**
	 * @param actor the actor to set
	 */
	public void setActor(String actor) {
		super.setActor(actor);
	}
	/**
	 * @return the asegurado
	 */
	@Exportable(columnName="ASEGURADO", columnOrder=12)
	public String getAsegurado() {
		return super.getAsegurado();
	}
	/**
	 * @param asegurado the asegurado to set
	 */
	public void setAsegurado(String asegurado) {
		super.setAsegurado(asegurado);
	}
	/**
	 * @return the asignado
	 */
	@Exportable(columnName="ASIGNADO A", columnOrder=13)
	public String getAsignado() {
		return super.getAsignado();
	}
	/**
	 * @param asignado the asignado to set
	 */
	public void setAsignado(String asignado) {
		super.setAsignado(asignado);
	}
	/**
	 * @return the clasificacionReclamacion
	 */
	public String getClasificacionReclamacion() {
		return super.getClasificacionReclamacion();
	}
	/**
	 * @param clasificacionReclamacion the clasificacionReclamacion to set
	 */
	public void setClasificacionReclamacion(String clasificacionReclamacion) {
		super.setClasificacionReclamacion(clasificacionReclamacion);
	}
	/**
	 * @return the delegacion
	 */
	public Long getDelegacion() {
		return super.getDelegacion();
	}
	/**
	 * @param delegacion the delegacion to set
	 */
	public void setDelegacion(Long delegacion) {
		super.setDelegacion(delegacion);
	}
	/**
	 * @return the demandado
	 */
	public String getDemandado() {
		return super.getDemandado();
	}
	/**
	 * @param demandado the demandado to set
	 */
	public void setDemandado(String demandado) {
		super.setDemandado(demandado);
	}
	/**
	 * @return the entidadFederativa
	 */
	public String getEntidadFederativa() {
		return super.getEntidadFederativa();
	}
	/**
	 * @param entidadFederativa the entidadFederativa to set
	 */
	public void setEntidadFederativa(String entidadFederativa) {
		super.setEntidadFederativa(entidadFederativa);
	}
	/**
	 * @return the estado
	 */
	public String getEstado() {
		return super.getEstado();
	}
	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		super.setEstado(estado);
	}
	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return super.getEstatus();
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		super.setEstatus(estatus);
	}
	/**
	 * @return the etapaJuicio
	 */
	public String getEtapaJuicio() {
		return super.getEtapaJuicio();
	}
	/**
	 * @param etapaJuicio the etapaJuicio to set
	 */
	public void setEtapaJuicio(String etapaJuicio) {
		super.setEtapaJuicio(etapaJuicio);
	}
	/**
	 * @return the expedienteJuridico
	 */
	@Exportable(columnName="EXPEDIENTE JURIDICO", columnOrder=15)
	public String getExpedienteJuridico() {
		return super.getExpedienteJuridico();
	}
	/**
	 * @param expedienteJuridico the expedienteJuridico to set
	 */
	public void setExpedienteJuridico(String expedienteJuridico) {
		super.setExpedienteJuridico(expedienteJuridico);
	}
	/**
	 * @return the fechaNotificacion
	 */
	@Exportable(columnName="FECHA DE NOTIFICACION", columnOrder=4)
	public Date getFechaNotificacion() {
		return super.getFechaNotificacion();
	}
	/**
	 * @param fechaNotificacion the fechaNotificacion to set
	 */
	public void setFechaNotificacion(Date fechaNotificacion) {
		super.setFechaNotificacion(fechaNotificacion);
	}
	/**
	 * @return the justificacion
	 */
	@Exportable(columnName="DESCRIPCION", columnOrder=20)
	public String getJustificacion() {
		return super.getJustificacion();
	}
	/**
	 * @param justificacion the justificacion to set
	 */
	public void setJustificacion(String justificacion) {
		super.setJustificacion(justificacion);
	}
	/**
	 * @return the montoReclamado
	 */
	@Exportable(columnName="MONTO RECLAMADO", columnOrder=11)
	public BigDecimal getMontoReclamado() {
		return super.getMontoReclamado();
	}
	/**
	 * @param montoReclamado the montoReclamado to set
	 */
	public void setMontoReclamado(BigDecimal montoReclamado) {
		super.setMontoReclamado(montoReclamado);
	}
	/**
	 * @return the motivoReclamacion
	 */
	public Long getMotivoReclamacion() {
		return super.getMotivoReclamacion();
	}
	/**
	 * @param motivoReclamacion the motivoReclamacion to set
	 */
	public void setMotivoReclamacion(Long motivoReclamacion) {
		super.setMotivoReclamacion(motivoReclamacion);
	}
	/**
	 * @return the municipio
	 */
	public String getMunicipio() {
		return super.getMunicipio();
	}
	/**
	 * @param municipio the municipio to set
	 */
	public void setMunicipio(String municipio) {
		super.setMunicipio(municipio);
	}
	/**
	 * @return the numeroOficio
	 */
	@Exportable(columnName="NUMERO DE OFICIO", columnOrder=2)
	public Integer getNumeroOficio() {
		return super.getNumeroOficio();
	}
	/**
	 * @param numeroOficio the numeroOficio to set
	 */
	public void setNumeroOficio(Integer numeroOficio) {
		super.setNumeroOficio(numeroOficio);
	}
	/**
	 * @return the numeroPoliza
	 */
	@Exportable(columnName="NUMERO DE POLIZA", columnOrder=7)
	public String getNumeroPoliza() {
		return super.getNumeroPoliza();
	}
	/**
	 * @param numeroPoliza the numeroPoliza to set
	 */
	public void setNumeroPoliza(String numeroPoliza) {
		super.setNumeroPoliza(numeroPoliza);
	}
	/**
	 * @return the numeroReclamacion
	 */
	@Exportable(columnName="NUMERO DE RECLAMACION", columnOrder=0)
	public Long getNumeroReclamacion() {
		return super.getNumeroReclamacion();
	}
	/**
	 * @param numeroReclamacion the numeroReclamacion to set
	 */
	public void setNumeroReclamacion(Long numeroReclamacion) {
		super.setNumeroReclamacion(numeroReclamacion);
	}
	/**
	 * @return the numeroSiniestro
	 */
	@Exportable(columnName="NUMERO DE SINIESTRO", columnOrder=8)
	public String getNumeroSiniestro() {
		return super.getNumeroSiniestro();
	}
	/**
	 * @param numeroSiniestro the numeroSiniestro to set
	 */
	public void setNumeroSiniestro(String numeroSiniestro) {
		super.setNumeroSiniestro(numeroSiniestro);
	}
	/**
	 * @return the oficinaJuridica
	 */
	public String getOficinaJuridica() {
		return super.getOficinaJuridica();
	}
	/**
	 * @param oficinaJuridica the oficinaJuridica to set
	 */
	public void setOficinaJuridica(String oficinaJuridica) {
		super.setOficinaJuridica(oficinaJuridica);
	}
	/**
	 * @return the oficinaSiniestro
	 */
	public String getOficinaSiniestro() {
		return super.getOficinaSiniestro();
	}
	/**
	 * @param oficinaSiniestro the oficinaSiniestro to set
	 */
	public void setOficinaSiniestro(String oficinaSiniestro) {
		super.setOficinaSiniestro(oficinaSiniestro);
	}
	/**
	 * @return the procedimiento
	 */
	public Long getProcedimiento() {
		return super.getProcedimiento();
	}
	/**
	 * @param procedimiento the procedimiento to set
	 */
	public void setProcedimiento(Long procedimiento) {
		super.setProcedimiento(procedimiento);
	}
	/**
	 * @return the ramo
	 */
	public String getRamo() {
		return super.getRamo();
	}
	/**
	 * @param ramo the ramo to set
	 */
	public void setRamo(String ramo) {
		super.setRamo(ramo);
	}
	/**
	 * @return the reclamanate
	 */
	@Exportable(columnName="AUTORIDAD", columnOrder=5)
	public String getReclamante() {
		return super.getReclamante();
	}
	/**
	 * @param reclamanate the reclamanate to set
	 */
	public void setReclamante(String reclamante) {
		super.setReclamante(reclamante);
	}
	/**
	 * @return the tipoQueja
	 */
	public String getTipoQueja() {
		return super.getTipoQueja();
	}
	/**
	 * @param tipoQueja the tipoQueja to set
	 */
	public void setTipoQueja(String tipoQueja) {
		super.setTipoQueja(tipoQueja);
	}
	/**
	 * @return the tipoReclamacion
	 */
	public String getTipoReclamacion() {
		return super.getTipoReclamacion();
	}
	/**
	 * @param tipoReclamacion the tipoReclamacion to set
	 */
	public void setTipoReclamacion(String tipoReclamacion) {
		super.setTipoReclamacion(tipoReclamacion);
	}
	/**
	 * @return the ubicacion
	 */
	public String getUbicacion() {
		return super.getUbicacion();
	}
	/**
	 * @param ubicacion the ubicacion to set
	 */
	public void setUbicacion(String ubicacion) {
		super.setUbicacion(ubicacion);
	}
	/**
	 * @return the totalOficios
	 */
	public Integer getTotalOficios() {
		return super.getTotalOficios();
	}
	/**
	 * @param totalOficios the totalOficios to set
	 */
	public void setTotalOficios(Integer totalOficios) {
		super.setTotalOficios(totalOficios);
	}
	/**
	 * @return the parteEnJuicio
	 */
	public String getParteEnJuicio() {
		return super.getParteEnJuicio();
	}
	/**
	 * @param parteEnJuicio the parteEnJuicio to set
	 */
	public void setParteEnJuicio(String parteEnJuicio) {
		super.setParteEnJuicio(parteEnJuicio);
	}
	/**
	 * @return the probabilidadExito
	 */
	public String getProbabilidadExito() {
		return super.getProbabilidadExito();
	}
	/**
	 * @param probabilidadExito the probabilidadExito to set
	 */
	public void setProbabilidadExito(String probabilidadExito) {
		super.setProbabilidadExito(probabilidadExito);
	}
	/**
	 * @return the siniestroId
	 */
	public Long getSiniestroId() {
		return super.getSiniestroId();
	}
	/**
	 * @param siniestroId the siniestroId to set
	 */
	public void setSiniestroId(Long siniestroId) {
		super.setSiniestroId(siniestroId);
	}
	/**
	 * @return the idtopoliza
	 */
	public Long getIdtopoliza() {
		return super.getIdtopoliza();
	}
	/**
	 * @param idtopoliza the idtopoliza to set
	 */
	public void setIdtopoliza(Long idtopoliza) {
		super.setIdtopoliza(idtopoliza);
	}
	/**
	 * @return the etapaJuicioDesc
	 */
	public String getEtapaJuicioDesc() {
		return super.getEtapaJuicioDesc();
	}
	/**
	 * @param etapaJuicioDesc the etapaJuicioDesc to set
	 */
	public void setEtapaJuicioDesc(String etapaJuicioDesc) {
		super.setEtapaJuicioDesc(etapaJuicioDesc);
	}
	/**
	 * @return the procedimientoDesc
	 */
	@Exportable(columnName="PROCEDIMIENTO", columnOrder=14)
	public String getProcedimientoDesc() {
		return super.getProcedimientoDesc();
	}
	/**
	 * @param procedimientoDesc the procedimientoDesc to set
	 */
	public void setProcedimientoDesc(String procedimientoDesc) {
		super.setProcedimientoDesc(procedimientoDesc);
	}
	/**
	 * @return the oficinaSiniestroDesc
	 */
	@Exportable(columnName="OFICINA DE SINIESTRO", columnOrder=9)
	public String getOficinaSiniestroDesc() {
		return super.getOficinaSiniestroDesc();
	}
	/**
	 * @param oficinaSiniestroDesc the oficinaSiniestroDesc to set
	 */
	public void setOficinaSiniestroDesc(String oficinaSiniestroDesc) {
		super.setOficinaSiniestroDesc(oficinaSiniestroDesc);
	}
	/**
	 * @return the ramoDesc
	 */
	@Exportable(columnName="RAMO", columnOrder=6)
	public String getRamoDesc() {
		return super.getRamoDesc();
	}
	/**
	 * @param ramoDesc the ramoDesc to set
	 */
	public void setRamoDesc(String ramoDesc) {
		super.setRamoDesc(ramoDesc);
	}
	/**
	 * @return the tipoReclamacionDesc
	 */
	@Exportable(columnName="TIPO DE RECLAMACION", columnOrder=1)
	public String getTipoReclamacionDesc() {
		return super.getTipoReclamacionDesc();
	}
	/**
	 * @param tipoReclamacionDesc the tipoReclamacionDesc to set
	 */
	public void setTipoReclamacionDesc(String tipoReclamacionDesc) {
		super.setTipoReclamacionDesc(tipoReclamacionDesc);
	}
	/**
	 * @return the esUltimoOficio
	 */
	public Boolean getEsUltimoOficio() {
		return super.getEsUltimoOficio();
	}
	/**
	 * @param esUltimoOficio the esUltimoOficio to set
	 */
	public void setEsUltimoOficio(Boolean esUltimoOficio) {
		super.setEsUltimoOficio(esUltimoOficio);
	}
	/**
	 * @return the oficinaJuridicaDesc
	 */
	@Exportable(columnName="OFICINA JURIDICA", columnOrder=3)
	public String getOficinaJuridicaDesc() {
		return super.getOficinaJuridicaDesc();
	}
	/**
	 * @param oficinaJuridicaDesc the oficinaJuridicaDesc to set
	 */
	public void setOficinaJuridicaDesc(String oficinaJuridicaDesc) {
		super.setOficinaJuridicaDesc(oficinaJuridicaDesc);
	}
	/**
	 * @return the delegacionDesc
	 */
	@Exportable(columnName="DELEGACION", columnOrder=18)
	public String getDelegacionDesc() {
		return super.getDelegacionDesc();
	}
	/**
	 * @param delegacionDesc the delegacionDesc to set
	 */
	public void setDelegacionDesc(String delegacionDesc) {
		super.setDelegacionDesc(delegacionDesc);
	}
	/**
	 * @return the estadoDesc
	 */
	@Exportable(columnName="ESTADO", columnOrder=16)
	public String getEstadoDesc() {
		return super.getEstadoDesc();
	}
	/**
	 * @param estadoDesc the estadoDesc to set
	 */
	public void setEstadoDesc(String estadoDesc) {
		super.setEstadoDesc(estadoDesc);
	}
	/**
	 * @return the municipioDesc
	 */
	@Exportable(columnName="MUNICIPIO", columnOrder=17)
	public String getMunicipioDesc() {
		return super.getMunicipioDesc();
	}
	/**
	 * @param municipioDesc the municipioDesc to set
	 */
	public void setMunicipioDesc(String municipioDesc) {
		super.setMunicipioDesc(municipioDesc);
	}
	/**
	 * @return the estatusDesc
	 */
	@Exportable(columnName="ESTATUS", columnOrder=19)
	public String getEstatusDesc() {
		return super.getEstatusDesc();
	}
	/**
	 * @param estatusDesc the estatusDesc to set
	 */
	public void setEstatusDesc(String estatusDesc) {
		super.setEstatusDesc(estatusDesc);
	}
	/**
	 * @return the clasificacionReclamacionDesc
	 */
	public String getClasificacionReclamacionDesc() {
		return super.getClasificacionReclamacionDesc();
	}
	/**
	 * @param clasificacionReclamacionDesc the clasificacionReclamacionDesc to set
	 */
	public void setClasificacionReclamacionDesc(String clasificacionReclamacionDesc) {
		super.setClasificacionReclamacionDesc(clasificacionReclamacionDesc);
	}
	/**
	 * @return the tipoQuejaDesc
	 */
	public String getTipoQuejaDesc() {
		return super.getTipoQuejaDesc();
	}
	/**
	 * @param tipoQuejaDesc the tipoQuejaDesc to set
	 */
	public void setTipoQuejaDesc(String tipoQuejaDesc) {
		super.setTipoQuejaDesc(tipoQuejaDesc);
	}
	/**
	 * @return the motivoReclamacionDesc
	 */
	@Exportable(columnName="MOTIVO DE RECLAMACION", columnOrder=10)
	public String getMotivoReclamacionDesc() {
		return super.getMotivoReclamacionDesc();
	}
	/**
	 * @param motivoReclamacionDesc the motivoReclamacionDesc to set
	 */
	public void setMotivoReclamacionDesc(String motivoReclamacionDesc) {
		super.setMotivoReclamacionDesc(motivoReclamacionDesc);
	}
}
