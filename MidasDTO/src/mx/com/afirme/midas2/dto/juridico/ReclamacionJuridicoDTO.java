package mx.com.afirme.midas2.dto.juridico;

import java.io.Serializable;
import java.util.Date;

import mx.com.afirme.midas2.annotation.Exportable;

public class ReclamacionJuridicoDTO implements Serializable{

	private static final long serialVersionUID = 2248137313588028450L;
	
	private String actor;
	private String asegurado;
	private String asignado;
	private String clasificacionReclamacion;
	private String delegacion;
	private String demandado;
	private String entidadFederativa;
	private String estado;
	private String estatus;
	private String etapaJuicio;
	private String expedienteJuridico;
	private Date fechaNotificacion;
	private String justificacion;
	private Double montoReclamado;
	private String motivoReclamacion;
	private String municipio;
	private Integer numeroOficio;
	private String numeroPoliza;
	private Long numeroReclamacion;
	private String numeroSiniestro;
	private String oficinaJuridica;
	private String oficinaSiniestro;
	private String parteEnJuicio;
	private String procedimiento;
	private String ramo;
	private String reclamante;
	private String tipoQueja;
	private String tipoReclamacion;
	private String ubicacion;
	private Integer totalOficios;
	private String estatusFinal;
	/**
	 * @return the actor
	 */
	public String getActor() {
		return actor;
	}
	/**
	 * @param actor the actor to set
	 */
	public void setActor(String actor) {
		this.actor = actor;
	}
	/**
	 * @return the asegurado
	 */
	@Exportable(columnName="ASEGURADO", columnOrder=5)
	public String getAsegurado() {
		return asegurado;
	}
	/**
	 * @param asegurado the asegurado to set
	 */
	public void setAsegurado(String asegurado) {
		this.asegurado = asegurado;
	}
	/**
	 * @return the asignado
	 */
	@Exportable(columnName="ASIGNADO A", columnOrder=12)
	public String getAsignado() {
		return asignado;
	}
	/**
	 * @param asignado the asignado to set
	 */
	public void setAsignado(String asignado) {
		this.asignado = asignado;
	}
	/**
	 * @return the clasificacionReclamacion
	 */
	public String getClasificacionReclamacion() {
		return clasificacionReclamacion;
	}
	/**
	 * @param clasificacionReclamacion the clasificacionReclamacion to set
	 */
	public void setClasificacionReclamacion(String clasificacionReclamacion) {
		this.clasificacionReclamacion = clasificacionReclamacion;
	}
	/**
	 * @return the delegacion
	 */
	public String getDelegacion() {
		return delegacion;
	}
	/**
	 * @param delegacion the delegacion to set
	 */
	public void setDelegacion(String delegacion) {
		this.delegacion = delegacion;
	}
	/**
	 * @return the demandado
	 */
	@Exportable(columnName="DEMANDADO", columnOrder=10)
	public String getDemandado() {
		return demandado;
	}
	/**
	 * @param demandado the demandado to set
	 */
	public void setDemandado(String demandado) {
		this.demandado = demandado;
	}
	/**
	 * @return the entidadFederativa
	 */
	public String getEntidadFederativa() {
		return entidadFederativa;
	}
	/**
	 * @param entidadFederativa the entidadFederativa to set
	 */
	public void setEntidadFederativa(String entidadFederativa) {
		this.entidadFederativa = entidadFederativa;
	}
	/**
	 * @return the estado
	 */
	@Exportable(columnName="ESTADO", columnOrder=15)
	public String getEstado() {
		return estado;
	}
	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/**
	 * @return the estatus
	 */
	@Exportable(columnName="ESTATUS", columnOrder=16)
	public String getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	/**
	 * @return the etapaJuicio
	 */
	public String getEtapaJuicio() {
		return etapaJuicio;
	}
	/**
	 * @param etapaJuicio the etapaJuicio to set
	 */
	public void setEtapaJuicio(String etapaJuicio) {
		this.etapaJuicio = etapaJuicio;
	}
	/**
	 * @return the expedienteJuridico
	 */
	@Exportable(columnName="EXPEDIENTE DE JURIDICO", columnOrder=8)
	public String getExpedienteJuridico() {
		return expedienteJuridico;
	}
	/**
	 * @param expedienteJuridico the expedienteJuridico to set
	 */
	public void setExpedienteJuridico(String expedienteJuridico) {
		this.expedienteJuridico = expedienteJuridico;
	}
	/**
	 * @return the fechaNotificacion
	 */
	@Exportable(columnName="FECHA DE ALTA", columnOrder=9)
	public Date getFechaNotificacion() {
		return fechaNotificacion;
	}
	/**
	 * @param fechaNotificacion the fechaNotificacion to set
	 */
	public void setFechaNotificacion(Date fechaNotificacion) {
		this.fechaNotificacion = fechaNotificacion;
	}
	/**
	 * @return the justificacion
	 */
	public String getJustificacion() {
		return justificacion;
	}
	/**
	 * @param justificacion the justificacion to set
	 */
	public void setJustificacion(String justificacion) {
		this.justificacion = justificacion;
	}
	/**
	 * @return the montoReclamado
	 */
	public Double getMontoReclamado() {
		return montoReclamado;
	}
	/**
	 * @param montoReclamado the montoReclamado to set
	 */
	public void setMontoReclamado(Double montoReclamado) {
		this.montoReclamado = montoReclamado;
	}
	/**
	 * @return the motivoReclamacion
	 */
	public String getMotivoReclamacion() {
		return motivoReclamacion;
	}
	/**
	 * @param motivoReclamacion the motivoReclamacion to set
	 */
	public void setMotivoReclamacion(String motivoReclamacion) {
		this.motivoReclamacion = motivoReclamacion;
	}
	/**
	 * @return the municipio
	 */
	public String getMunicipio() {
		return municipio;
	}
	/**
	 * @param municipio the municipio to set
	 */
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	/**
	 * @return the numeroOficio
	 */
	public Integer getNumeroOficio() {
		return numeroOficio;
	}
	/**
	 * @param numeroOficio the numeroOficio to set
	 */
	public void setNumeroOficio(Integer numeroOficio) {
		this.numeroOficio = numeroOficio;
	}
	/**
	 * @return the numeroPoliza
	 */
	@Exportable(columnName="NUMERO DE POLIZA", columnOrder=4)
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	/**
	 * @param numeroPoliza the numeroPoliza to set
	 */
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	/**
	 * @return the numeroReclamacion
	 */
	@Exportable(columnName="NUMERO DE RECLAMACION", columnOrder=0)
	public Long getNumeroReclamacion() {
		return numeroReclamacion;
	}
	/**
	 * @param numeroReclamacion the numeroReclamacion to set
	 */
	public void setNumeroReclamacion(Long numeroReclamacion) {
		this.numeroReclamacion = numeroReclamacion;
	}
	/**
	 * @return the numeroSiniestro
	 */
	@Exportable(columnName="NUMERO DE SINIESTRO", columnOrder=6)
	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}
	/**
	 * @param numeroSiniestro the numeroSiniestro to set
	 */
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}
	/**
	 * @return the oficinaJuridica
	 */
	@Exportable(columnName="OFICINA JURIDICA", columnOrder=3)
	public String getOficinaJuridica() {
		return oficinaJuridica;
	}
	/**
	 * @param oficinaJuridica the oficinaJuridica to set
	 */
	public void setOficinaJuridica(String oficinaJuridica) {
		this.oficinaJuridica = oficinaJuridica;
	}
	/**
	 * @return the oficinaSiniestro
	 */
	@Exportable(columnName="OFICINA DE SINIESTRO", columnOrder=7)
	public String getOficinaSiniestro() {
		return oficinaSiniestro;
	}
	/**
	 * @param oficinaSiniestro the oficinaSiniestro to set
	 */
	public void setOficinaSiniestro(String oficinaSiniestro) {
		this.oficinaSiniestro = oficinaSiniestro;
	}
	/**
	 * @return the procedimiento
	 */
	@Exportable(columnName="PROCEDIMIENTO", columnOrder=13)
	public String getProcedimiento() {
		return procedimiento;
	}
	/**
	 * @param procedimiento the procedimiento to set
	 */
	public void setProcedimiento(String procedimiento) {
		this.procedimiento = procedimiento;
	}
	/**
	 * @return the ramo
	 */
	@Exportable(columnName="RAMO", columnOrder=14)
	public String getRamo() {
		return ramo;
	}
	/**
	 * @param ramo the ramo to set
	 */
	public void setRamo(String ramo) {
		this.ramo = ramo;
	}
	/**
	 * @return the tipoQueja
	 */
	public String getTipoQueja() {
		return tipoQueja;
	}
	/**
	 * @param tipoQueja the tipoQueja to set
	 */
	public void setTipoQueja(String tipoQueja) {
		this.tipoQueja = tipoQueja;
	}
	/**
	 * @return the tipoReclamacion
	 */
	@Exportable(columnName="TIPO DE RECLAMACION", columnOrder=1)
	public String getTipoReclamacion() {
		return tipoReclamacion;
	}
	/**
	 * @param tipoReclamacion the tipoReclamacion to set
	 */
	public void setTipoReclamacion(String tipoReclamacion) {
		this.tipoReclamacion = tipoReclamacion;
	}
	/**
	 * @return the ubicacion
	 */
	public String getUbicacion() {
		return ubicacion;
	}
	/**
	 * @param ubicacion the ubicacion to set
	 */
	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}
	/**
	 * @return the totalOficios
	 */
	@Exportable(columnName="TOTAL DE OFICIOS", columnOrder=2)
	public Integer getTotalOficios() {
		return totalOficios;
	}
	/**
	 * @param totalOficios the totalOficios to set
	 */
	public void setTotalOficios(Integer totalOficios) {
		this.totalOficios = totalOficios;
	}
	/**
	 * @return the parteEnJuicio
	 */
	public String getParteEnJuicio() {
		return parteEnJuicio;
	}
	/**
	 * @param parteEnJuicio the parteEnJuicio to set
	 */
	public void setParteEnJuicio(String parteEnJuicio) {
		this.parteEnJuicio = parteEnJuicio;
	}
	/**
	 * @return the estatusFinal
	 */
	public String getEstatusFinal() {
		return estatusFinal;
	}
	/**
	 * @param estatusFinal the estatusFinal to set
	 */
	public void setEstatusFinal(String estatusFinal) {
		this.estatusFinal = estatusFinal;
	}
	/**
	 * @return the reclamante
	 */
	@Exportable(columnName="RECLAMANTE", columnOrder=11)
	public String getReclamante() {
		return reclamante;
	}
	/**
	 * @param reclamante the reclamante to set
	 */
	public void setReclamante(String reclamante) {
		this.reclamante = reclamante;
	}

}