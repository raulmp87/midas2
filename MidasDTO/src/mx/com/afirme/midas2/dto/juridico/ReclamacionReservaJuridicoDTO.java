package mx.com.afirme.midas2.dto.juridico;

import java.io.Serializable;
import java.math.BigDecimal;

public class ReclamacionReservaJuridicoDTO implements Serializable{

	private static final long serialVersionUID = 8341695747929900306L;
	
	private String claveSubCobertura;
	private Long coberturaId;
	private String nombreCobertura;
	private Integer oficio;
	private Long reclamacionId;
	private BigDecimal reservaMontoAutoridad;
	private BigDecimal reservaSiniestro;
	/**
	 * @return the claveSubCobertura
	 */
	public String getClaveSubCobertura() {
		return claveSubCobertura;
	}
	/**
	 * @param claveSubCobertura the claveSubCobertura to set
	 */
	public void setClaveSubCobertura(String claveSubCobertura) {
		this.claveSubCobertura = claveSubCobertura;
	}
	/**
	 * @return the coberturaId
	 */
	public Long getCoberturaId() {
		return coberturaId;
	}
	/**
	 * @param coberturaId the coberturaId to set
	 */
	public void setCoberturaId(Long coberturaId) {
		this.coberturaId = coberturaId;
	}
	/**
	 * @return the nombreCobertura
	 */
	public String getNombreCobertura() {
		return nombreCobertura;
	}
	/**
	 * @param nombreCobertura the nombreCobertura to set
	 */
	public void setNombreCobertura(String nombreCobertura) {
		this.nombreCobertura = nombreCobertura;
	}
	/**
	 * @return the oficio
	 */
	public Integer getOficio() {
		return oficio;
	}
	/**
	 * @param oficio the oficio to set
	 */
	public void setOficio(Integer oficio) {
		this.oficio = oficio;
	}
	/**
	 * @return the reclamacionId
	 */
	public Long getReclamacionId() {
		return reclamacionId;
	}
	/**
	 * @param reclamacionId the reclamacionId to set
	 */
	public void setReclamacionId(Long reclamacionId) {
		this.reclamacionId = reclamacionId;
	}
	/**
	 * @return the reservaMontoAutoridad
	 */
	public BigDecimal getReservaMontoAutoridad() {
		return reservaMontoAutoridad;
	}
	/**
	 * @param reservaMontoAutoridad the reservaMontoAutoridad to set
	 */
	public void setReservaMontoAutoridad(BigDecimal reservaMontoAutoridad) {
		this.reservaMontoAutoridad = reservaMontoAutoridad;
	}
	/**
	 * @return the reservaSiniestro
	 */
	public BigDecimal getReservaSiniestro() {
		return reservaSiniestro;
	}
	/**
	 * @param reservaSiniestro the reservaSiniestro to set
	 */
	public void setReservaSiniestro(BigDecimal reservaSiniestro) {
		this.reservaSiniestro = reservaSiniestro;
	}

}