package mx.com.afirme.midas2.dto.juridico;

import java.io.Serializable;
import java.util.Date;

import org.springframework.stereotype.Component;
@Component
public class CatalogoJuridicoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1965145573745891614L;
	private Long id; 
	private Long numero;
	private String nombre;
	private String estatus;
	private String estatusDesc;
	private Long idOficina;
	private String tipoCatalogo;

	
	
	private String nombreOficina;
	private Date fechaActivo;
	private Date fechaInactivo;
	
	private String fechaActivoStr;
	private String fechaInactivoStr;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getNumero() {
		return numero;
	}
	public void setNumero(Long numero) {
		this.numero = numero;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	
	public String getNombreOficina() {
		return nombreOficina;
	}
	public void setNombreOficina(String nombreOficina) {
		this.nombreOficina = nombreOficina;
	}
	public Date getFechaActivo() {
		return fechaActivo;
	}
	public void setFechaActivo(Date fechaActivo) {
		this.fechaActivo = fechaActivo;
	}
	public Date getFechaInactivo() {
		return fechaInactivo;
	}
	public void setFechaInactivo(Date fechaInactivo) {
		this.fechaInactivo = fechaInactivo;
	}
	public String getFechaActivoStr() {
		return fechaActivoStr;
	}
	public void setFechaActivoStr(String fechaActivoStr) {
		this.fechaActivoStr = fechaActivoStr;
	}
	public String getFechaInactivoStr() {
		return fechaInactivoStr;
	}
	public void setFechaInactivoStr(String fechaInactivoStr) {
		this.fechaInactivoStr = fechaInactivoStr;
	}
	public String getTipoCatalogo() {
		return tipoCatalogo;
	}
	public void setTipoCatalogo(String tipoCatalogo) {
		this.tipoCatalogo = tipoCatalogo;
	}
	public Long getIdOficina() {
		return idOficina;
	}
	public void setIdOficina(Long idOficina) {
		this.idOficina = idOficina;
	}
	public String getEstatusDesc() {
		return estatusDesc;
	}
	public void setEstatusDesc(String estatusDesc) {
		this.estatusDesc = estatusDesc;
	}
	
	
	
	
}
