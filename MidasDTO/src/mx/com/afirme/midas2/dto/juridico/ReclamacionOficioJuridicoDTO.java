package mx.com.afirme.midas2.dto.juridico;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import mx.com.afirme.midas2.annotation.Exportable;

public class ReclamacionOficioJuridicoDTO implements Serializable{

	private static final long serialVersionUID = 9447659493759743L;

	private String actor;
	private String asegurado;
	private String asignado;
	private String clasificacionReclamacion;
	private String clasificacionReclamacionDesc;
	private Long delegacion;
	private String delegacionDesc;
	private String demandado;
	private String entidadFederativa;
	private String estado;
	private String estadoDesc;
	private String estatus;
	private String estatusDesc;
	private String etapaJuicio;
	private String etapaJuicioDesc;
	private String expedienteJuridico;
	private Date fechaNotificacion;
	private String justificacion;
	private BigDecimal montoReclamado;
	private Long motivoReclamacion;
	private String motivoReclamacionDesc;
	private String municipio;
	private String municipioDesc;
	private Integer numeroOficio;
	private Long idtopoliza;
	private String numeroPoliza;
	private Long numeroReclamacion;
	private String numeroSiniestro;
	private String oficinaJuridica;
	private String oficinaJuridicaDesc;
	private String oficinaSiniestro;
	private String oficinaSiniestroDesc;
	private String parteEnJuicio;
	private Long procedimiento;
	private String procedimientoDesc;
	private String probabilidadExito;
	private String ramo;
	private String ramoDesc;
	private String reclamante;
	private Long siniestroId;
	private String tipoQueja;
	private String tipoQuejaDesc;
	private String tipoReclamacion;
	private String tipoReclamacionDesc;
	private String ubicacion;
	private Integer totalOficios;
	private Boolean esUltimoOficio;
	/**
	 * @return the actor
	 */
	@Exportable(columnName="ACTOR", columnOrder=5)
	public String getActor() {
		return actor;
	}
	/**
	 * @param actor the actor to set
	 */
	public void setActor(String actor) {
		this.actor = actor;
	}
	/**
	 * @return the asegurado
	 */
	@Exportable(columnName="ASEGURADO", columnOrder=7)
	public String getAsegurado() {
		return asegurado;
	}
	/**
	 * @param asegurado the asegurado to set
	 */
	public void setAsegurado(String asegurado) {
		this.asegurado = asegurado;
	}
	/**
	 * @return the asignado
	 */
	@Exportable(columnName="ASIGNADO A", columnOrder=13)
	public String getAsignado() {
		return asignado;
	}
	/**
	 * @param asignado the asignado to set
	 */
	public void setAsignado(String asignado) {
		this.asignado = asignado;
	}
	/**
	 * @return the clasificacionReclamacion
	 */
	public String getClasificacionReclamacion() {
		return clasificacionReclamacion;
	}
	/**
	 * @param clasificacionReclamacion the clasificacionReclamacion to set
	 */
	public void setClasificacionReclamacion(String clasificacionReclamacion) {
		this.clasificacionReclamacion = clasificacionReclamacion;
	}
	/**
	 * @return the delegacion
	 */
	public Long getDelegacion() {
		return delegacion;
	}
	/**
	 * @param delegacion the delegacion to set
	 */
	public void setDelegacion(Long delegacion) {
		this.delegacion = delegacion;
	}
	/**
	 * @return the demandado
	 */
	@Exportable(columnName="DEMANDADO", columnOrder=6)
	public String getDemandado() {
		return demandado;
	}
	/**
	 * @param demandado the demandado to set
	 */
	public void setDemandado(String demandado) {
		this.demandado = demandado;
	}
	/**
	 * @return the entidadFederativa
	 */
	public String getEntidadFederativa() {
		return entidadFederativa;
	}
	/**
	 * @param entidadFederativa the entidadFederativa to set
	 */
	public void setEntidadFederativa(String entidadFederativa) {
		this.entidadFederativa = entidadFederativa;
	}
	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	/**
	 * @return the etapaJuicio
	 */
	public String getEtapaJuicio() {
		return etapaJuicio;
	}
	/**
	 * @param etapaJuicio the etapaJuicio to set
	 */
	public void setEtapaJuicio(String etapaJuicio) {
		this.etapaJuicio = etapaJuicio;
	}
	/**
	 * @return the expedienteJuridico
	 */
	@Exportable(columnName="EXPEDIENTE JURIDICO", columnOrder=15)
	public String getExpedienteJuridico() {
		return expedienteJuridico;
	}
	/**
	 * @param expedienteJuridico the expedienteJuridico to set
	 */
	public void setExpedienteJuridico(String expedienteJuridico) {
		this.expedienteJuridico = expedienteJuridico;
	}
	/**
	 * @return the fechaNotificacion
	 */
	@Exportable(columnName="FECHA DE NOTIFICACION", columnOrder=4)
	public Date getFechaNotificacion() {
		return fechaNotificacion;
	}
	/**
	 * @param fechaNotificacion the fechaNotificacion to set
	 */
	public void setFechaNotificacion(Date fechaNotificacion) {
		this.fechaNotificacion = fechaNotificacion;
	}
	/**
	 * @return the justificacion
	 */
	@Exportable(columnName="COMENTARIOS", columnOrder=17)
	public String getJustificacion() {
		return justificacion;
	}
	/**
	 * @param justificacion the justificacion to set
	 */
	public void setJustificacion(String justificacion) {
		this.justificacion = justificacion;
	}
	/**
	 * @return the montoReclamado
	 */
	@Exportable(columnName="MONTO RECLAMADO", columnOrder=12)
	public BigDecimal getMontoReclamado() {
		return montoReclamado;
	}
	/**
	 * @param montoReclamado the montoReclamado to set
	 */
	public void setMontoReclamado(BigDecimal montoReclamado) {
		this.montoReclamado = montoReclamado;
	}
	/**
	 * @return the motivoReclamacion
	 */
	public Long getMotivoReclamacion() {
		return motivoReclamacion;
	}
	/**
	 * @param motivoReclamacion the motivoReclamacion to set
	 */
	public void setMotivoReclamacion(Long motivoReclamacion) {
		this.motivoReclamacion = motivoReclamacion;
	}
	/**
	 * @return the municipio
	 */
	public String getMunicipio() {
		return municipio;
	}
	/**
	 * @param municipio the municipio to set
	 */
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	/**
	 * @return the numeroOficio
	 */
	@Exportable(columnName="NUMERO DE OFICIO", columnOrder=2)
	public Integer getNumeroOficio() {
		return numeroOficio;
	}
	/**
	 * @param numeroOficio the numeroOficio to set
	 */
	public void setNumeroOficio(Integer numeroOficio) {
		this.numeroOficio = numeroOficio;
	}
	/**
	 * @return the numeroPoliza
	 */
	@Exportable(columnName="NUMERO DE POLIZA", columnOrder=9)
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	/**
	 * @param numeroPoliza the numeroPoliza to set
	 */
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	/**
	 * @return the numeroReclamacion
	 */
	@Exportable(columnName="NUMERO DE RECLAMACION", columnOrder=0)
	public Long getNumeroReclamacion() {
		return numeroReclamacion;
	}
	/**
	 * @param numeroReclamacion the numeroReclamacion to set
	 */
	public void setNumeroReclamacion(Long numeroReclamacion) {
		this.numeroReclamacion = numeroReclamacion;
	}
	/**
	 * @return the numeroSiniestro
	 */
	@Exportable(columnName="NUMERO DE SINIESTRO", columnOrder=10)
	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}
	/**
	 * @param numeroSiniestro the numeroSiniestro to set
	 */
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}
	/**
	 * @return the oficinaJuridica
	 */
	public String getOficinaJuridica() {
		return oficinaJuridica;
	}
	/**
	 * @param oficinaJuridica the oficinaJuridica to set
	 */
	public void setOficinaJuridica(String oficinaJuridica) {
		this.oficinaJuridica = oficinaJuridica;
	}
	/**
	 * @return the oficinaSiniestro
	 */
	public String getOficinaSiniestro() {
		return oficinaSiniestro;
	}
	/**
	 * @param oficinaSiniestro the oficinaSiniestro to set
	 */
	public void setOficinaSiniestro(String oficinaSiniestro) {
		this.oficinaSiniestro = oficinaSiniestro;
	}
	/**
	 * @return the procedimiento
	 */
	public Long getProcedimiento() {
		return procedimiento;
	}
	/**
	 * @param procedimiento the procedimiento to set
	 */
	public void setProcedimiento(Long procedimiento) {
		this.procedimiento = procedimiento;
	}
	/**
	 * @return the ramo
	 */
	public String getRamo() {
		return ramo;
	}
	/**
	 * @param ramo the ramo to set
	 */
	public void setRamo(String ramo) {
		this.ramo = ramo;
	}
	/**
	 * @return the reclamanate
	 */
	public String getReclamante() {
		return reclamante;
	}
	/**
	 * @param reclamanate the reclamanate to set
	 */
	public void setReclamante(String reclamante) {
		this.reclamante = reclamante;
	}
	/**
	 * @return the tipoQueja
	 */
	public String getTipoQueja() {
		return tipoQueja;
	}
	/**
	 * @param tipoQueja the tipoQueja to set
	 */
	public void setTipoQueja(String tipoQueja) {
		this.tipoQueja = tipoQueja;
	}
	/**
	 * @return the tipoReclamacion
	 */
	public String getTipoReclamacion() {
		return tipoReclamacion;
	}
	/**
	 * @param tipoReclamacion the tipoReclamacion to set
	 */
	public void setTipoReclamacion(String tipoReclamacion) {
		this.tipoReclamacion = tipoReclamacion;
	}
	/**
	 * @return the ubicacion
	 */
	@Exportable(columnName="UBICACION DEL LITIGIO", columnOrder=3)
	public String getUbicacion() {
		return ubicacion;
	}
	/**
	 * @param ubicacion the ubicacion to set
	 */
	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}
	/**
	 * @return the totalOficios
	 */
	public Integer getTotalOficios() {
		return totalOficios;
	}
	/**
	 * @param totalOficios the totalOficios to set
	 */
	public void setTotalOficios(Integer totalOficios) {
		this.totalOficios = totalOficios;
	}
	/**
	 * @return the parteEnJuicio
	 */
	public String getParteEnJuicio() {
		return parteEnJuicio;
	}
	/**
	 * @param parteEnJuicio the parteEnJuicio to set
	 */
	public void setParteEnJuicio(String parteEnJuicio) {
		this.parteEnJuicio = parteEnJuicio;
	}
	/**
	 * @return the probabilidadExito
	 */
	public String getProbabilidadExito() {
		return probabilidadExito;
	}
	/**
	 * @param probabilidadExito the probabilidadExito to set
	 */
	public void setProbabilidadExito(String probabilidadExito) {
		this.probabilidadExito = probabilidadExito;
	}
	/**
	 * @return the siniestroId
	 */
	public Long getSiniestroId() {
		return siniestroId;
	}
	/**
	 * @param siniestroId the siniestroId to set
	 */
	public void setSiniestroId(Long siniestroId) {
		this.siniestroId = siniestroId;
	}
	/**
	 * @return the idtopoliza
	 */
	public Long getIdtopoliza() {
		return idtopoliza;
	}
	/**
	 * @param idtopoliza the idtopoliza to set
	 */
	public void setIdtopoliza(Long idtopoliza) {
		this.idtopoliza = idtopoliza;
	}
	/**
	 * @return the etapaJuicioDesc
	 */
	@Exportable(columnName="ETAPA DE JUICIO", columnOrder=16)
	public String getEtapaJuicioDesc() {
		return etapaJuicioDesc;
	}
	/**
	 * @param etapaJuicioDesc the etapaJuicioDesc to set
	 */
	public void setEtapaJuicioDesc(String etapaJuicioDesc) {
		this.etapaJuicioDesc = etapaJuicioDesc;
	}
	/**
	 * @return the procedimientoDesc
	 */
	@Exportable(columnName="PROCEDIMIENTO", columnOrder=14)
	public String getProcedimientoDesc() {
		return procedimientoDesc;
	}
	/**
	 * @param procedimientoDesc the procedimientoDesc to set
	 */
	public void setProcedimientoDesc(String procedimientoDesc) {
		this.procedimientoDesc = procedimientoDesc;
	}
	/**
	 * @return the oficinaSiniestroDesc
	 */
	@Exportable(columnName="OFICINA SINEISTRO", columnOrder=11)
	public String getOficinaSiniestroDesc() {
		return oficinaSiniestroDesc;
	}
	/**
	 * @param oficinaSiniestroDesc the oficinaSiniestroDesc to set
	 */
	public void setOficinaSiniestroDesc(String oficinaSiniestroDesc) {
		this.oficinaSiniestroDesc = oficinaSiniestroDesc;
	}
	/**
	 * @return the ramoDesc
	 */
	@Exportable(columnName="RAMO", columnOrder=8)
	public String getRamoDesc() {
		return ramoDesc;
	}
	/**
	 * @param ramoDesc the ramoDesc to set
	 */
	public void setRamoDesc(String ramoDesc) {
		this.ramoDesc = ramoDesc;
	}
	/**
	 * @return the tipoReclamacionDesc
	 */
	@Exportable(columnName="TIPO DE RECLAMACION", columnOrder=1)
	public String getTipoReclamacionDesc() {
		return tipoReclamacionDesc;
	}
	/**
	 * @param tipoReclamacionDesc the tipoReclamacionDesc to set
	 */
	public void setTipoReclamacionDesc(String tipoReclamacionDesc) {
		this.tipoReclamacionDesc = tipoReclamacionDesc;
	}
	/**
	 * @return the esUltimoOficio
	 */
	public Boolean getEsUltimoOficio() {
		return esUltimoOficio;
	}
	/**
	 * @param esUltimoOficio the esUltimoOficio to set
	 */
	public void setEsUltimoOficio(Boolean esUltimoOficio) {
		this.esUltimoOficio = esUltimoOficio;
	}
	/**
	 * @return the oficinaJuridicaDesc
	 */
	public String getOficinaJuridicaDesc() {
		return oficinaJuridicaDesc;
	}
	/**
	 * @param oficinaJuridicaDesc the oficinaJuridicaDesc to set
	 */
	public void setOficinaJuridicaDesc(String oficinaJuridicaDesc) {
		this.oficinaJuridicaDesc = oficinaJuridicaDesc;
	}
	/**
	 * @return the delegacionDesc
	 */
	public String getDelegacionDesc() {
		return delegacionDesc;
	}
	/**
	 * @param delegacionDesc the delegacionDesc to set
	 */
	public void setDelegacionDesc(String delegacionDesc) {
		this.delegacionDesc = delegacionDesc;
	}
	/**
	 * @return the estadoDesc
	 */
	public String getEstadoDesc() {
		return estadoDesc;
	}
	/**
	 * @param estadoDesc the estadoDesc to set
	 */
	public void setEstadoDesc(String estadoDesc) {
		this.estadoDesc = estadoDesc;
	}
	/**
	 * @return the municipioDesc
	 */
	public String getMunicipioDesc() {
		return municipioDesc;
	}
	/**
	 * @param municipioDesc the municipioDesc to set
	 */
	public void setMunicipioDesc(String municipioDesc) {
		this.municipioDesc = municipioDesc;
	}
	/**
	 * @return the estatusDesc
	 */
	public String getEstatusDesc() {
		return estatusDesc;
	}
	/**
	 * @param estatusDesc the estatusDesc to set
	 */
	public void setEstatusDesc(String estatusDesc) {
		this.estatusDesc = estatusDesc;
	}
	/**
	 * @return the clasificacionReclamacionDesc
	 */
	public String getClasificacionReclamacionDesc() {
		return clasificacionReclamacionDesc;
	}
	/**
	 * @param clasificacionReclamacionDesc the clasificacionReclamacionDesc to set
	 */
	public void setClasificacionReclamacionDesc(String clasificacionReclamacionDesc) {
		this.clasificacionReclamacionDesc = clasificacionReclamacionDesc;
	}
	/**
	 * @return the tipoQuejaDesc
	 */
	public String getTipoQuejaDesc() {
		return tipoQuejaDesc;
	}
	/**
	 * @param tipoQuejaDesc the tipoQuejaDesc to set
	 */
	public void setTipoQuejaDesc(String tipoQuejaDesc) {
		this.tipoQuejaDesc = tipoQuejaDesc;
	}
	/**
	 * @return the motivoReclamacionDesc
	 */
	public String getMotivoReclamacionDesc() {
		return motivoReclamacionDesc;
	}
	/**
	 * @param motivoReclamacionDesc the motivoReclamacionDesc to set
	 */
	public void setMotivoReclamacionDesc(String motivoReclamacionDesc) {
		this.motivoReclamacionDesc = motivoReclamacionDesc;
	}
	
}