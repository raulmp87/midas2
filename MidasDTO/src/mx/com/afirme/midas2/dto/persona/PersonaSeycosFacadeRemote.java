package mx.com.afirme.midas2.dto.persona;

import java.util.List;

import javax.ejb.Remote;


public interface PersonaSeycosFacadeRemote {
	public enum TipoDomicilio{
		PERSONAL("PERS"),FISCAL("FISC"),OFICINA("OFIC"),COBRANZA("COBR");
		private String value;
		private TipoDomicilio(String value){
			this.value=value;
		}
		public String getValue(){
			return value;
		}
	}
	public PersonaSeycosDTO findById(Long idPersona) throws Exception;
	
	public PersonaSeycosDTO save(PersonaSeycosDTO persona) throws Exception;
	
	public PersonaSeycosDTO saveOficeData(PersonaSeycosDTO persona) throws Exception;
	
	public PersonaSeycosDTO saveContactData(PersonaSeycosDTO persona) throws Exception;
	
	public List<PersonaSeycosDTO> findAll() throws Exception;
	
	public List<PersonaSeycosDTO> findByFilters(PersonaSeycosDTO filtroPersona,boolean withAddress)throws Exception;
	
	public PersonaSeycosDTO unsubscribe(PersonaSeycosDTO persona) throws Exception;
	
	public PersonaSeycosDTO saveWithoutAddress(PersonaSeycosDTO persona) throws Exception;
	
	public List<PersonaSeycosDTO> findByFilters(PersonaSeycosDTO filtroPersona,boolean withAddress, String fechaHistorico)throws Exception;
	
	public PersonaSeycosDTO findById(Long idPersona, String fechaHistorico) throws Exception;
}
