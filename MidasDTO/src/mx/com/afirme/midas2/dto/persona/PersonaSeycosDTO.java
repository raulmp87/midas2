package mx.com.afirme.midas2.dto.persona;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Afianzadora;

public class PersonaSeycosDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long idPersona;
	private	Short claveTipoPersona;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String sexo;
	private String codigoRFC;
	private String email;
	private String codigoCURP;
	private String telefono;//telefono casa
	private String estadoCivil;
	private Date fechaNacimiento;
	private String clavePaisNacimiento;
	private String claveEstadoNacimiento;
	private String claveCiudadNacimiento;
	private Short tipoSituacion;
	private String situacionPersona;
	private String claveSectorFinanciero;
	private String idGiro;
	private List<Domicilio> domicilios;
	private Long idDomicilio;
	private String razonSocial;
	private Date fechaConstitucion;
	private String telCasa;
	private String telOficina;
	private String telFax;
	//Datos de persona EXT
	private String telCelular;
	private String nombreContacto;
	private String puestoContacto;
	private String faceBook;
	private String twitter;
	private String paginaWeb;
	private String telefonosAdicionales;
	private String correosAdicionales;
	private Long idRepresentante;
	private String extension;
	private Date fechaAlta;
	private Date fechaBaja;
	private String nombreCompleto;
	private String nombreRepresentanteLegal;

	public PersonaSeycosDTO(){}
	
	public String getNombreRepresentanteLegal() {
		return nombreRepresentanteLegal;
	}

	public void setNombreRepresentanteLegal(String nombreRepresentanteLegal) {
		this.nombreRepresentanteLegal = nombreRepresentanteLegal;
	}

	public Long getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}
	public Short getClaveTipoPersona() {//  getClaveTipoPersona()
		return claveTipoPersona;
	}
	public void setClaveTipoPersona(Short claveTipoPersona) { //Short claveTipoPersona
		this.claveTipoPersona = claveTipoPersona;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getCodigoRFC() {
		return codigoRFC;
	}
	public void setCodigoRFC(String codigoRFC) {
		this.codigoRFC = codigoRFC;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCodigoCURP() {
		return codigoCURP;
	}
	public void setCodigoCURP(String codigoCURP) {
		this.codigoCURP = codigoCURP;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}	
	public String getClavePaisNacimiento() {
		return clavePaisNacimiento;
	}
	public void setClavePaisNacimiento(String clavePaisNacimiento) {
		this.clavePaisNacimiento = clavePaisNacimiento;
	}
	public String getClaveEstadoNacimiento() {
		return claveEstadoNacimiento;
	}
	public void setClaveEstadoNacimiento(String claveEstadoNacimiento) {
		this.claveEstadoNacimiento = claveEstadoNacimiento;
	}
	public String getClaveCiudadNacimiento() {
		return claveCiudadNacimiento;
	}
	public void setClaveCiudadNacimiento(String claveCiudadNacimiento) {
		this.claveCiudadNacimiento = claveCiudadNacimiento;
	}
	public Short getTipoSituacion() {
		return tipoSituacion;
	}
	public void setTipoSituacion(Short tipoSituacion) {
		this.tipoSituacion = tipoSituacion;
	}
	public String getClaveSectorFinanciero() {
		return claveSectorFinanciero;
	}
	public void setClaveSectorFinanciero(String claveSectorFinanciero) {
		this.claveSectorFinanciero = claveSectorFinanciero;
	}
	public String getIdGiro() {
		return idGiro;
	}
	public void setIdGiro(String idGiro) {
		this.idGiro = idGiro;
	}
	public List<Domicilio> getDomicilios() {
		return domicilios;
	}
	public void setDomicilios(List<Domicilio> domicilios) {
		this.domicilios = domicilios;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public Date getFechaConstitucion() {
		return fechaConstitucion;
	}
	public void setFechaConstitucion(Date fechaConstitucion) {
		this.fechaConstitucion = fechaConstitucion;
	}
	public String getTelCasa() {
		return telCasa;
	}
	public void setTelCasa(String telCasa) {
		this.telCasa = telCasa;
	}
	public String getTelOficina() {
		return telOficina;
	}
	public void setTelOficina(String telOficina) {
		this.telOficina = telOficina;
	}
	public String getTelFax() {
		return telFax;
	}
	public void setTelFax(String telFax) {
		this.telFax = telFax;
	}
	public String getNombreContacto() {
		return nombreContacto;
	}
	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}
	public String getPuestoContacto() {
		return puestoContacto;
	}
	public void setPuestoContacto(String puestoContacto) {
		this.puestoContacto = puestoContacto;
	}
	public String getFaceBook() {
		return faceBook;
	}
	public void setFaceBook(String faceBook) {
		this.faceBook = faceBook;
	}
	public String getTwitter() {
		return twitter;
	}
	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}
	public String getPaginaWeb() {
		return paginaWeb;
	}
	public void setPaginaWeb(String paginaWeb) {
		this.paginaWeb = paginaWeb;
	}
	public String getTelefonosAdicionales() {
		return telefonosAdicionales;
	}
	public void setTelefonosAdicionales(String telefonosAdicionales) {
		this.telefonosAdicionales = telefonosAdicionales;
	}
	public String getCorreosAdicionales() {
		return correosAdicionales;
	}
	public void setCorreosAdicionales(String correosAdicionales) {
		this.correosAdicionales = correosAdicionales;
	}
	public String getTelCelular() {
		return telCelular;
	}
	public void setTelCelular(String telCelular) {
		this.telCelular = telCelular;
	}
	public Long getIdRepresentante() {
		return idRepresentante;
	}
	public void setIdRepresentante(Long idRepresentante) {
		this.idRepresentante = idRepresentante;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	
	public Long getIdDomicilio() {
		return idDomicilio;
	}

	public void setIdDomicilio(Long idDomicilio) {
		this.idDomicilio = idDomicilio;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Date getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public String getSituacionPersona() {
		return situacionPersona;
	}

	public void setSituacionPersona(String situacionPersona) {
		this.situacionPersona = situacionPersona;
	}

	public PersonaSeycosDTO(Afianzadora afianzadora){
		List<Domicilio> domicilios=new ArrayList<Domicilio>();
		domicilios.add(afianzadora.getDomicilio());
		this.domicilios=domicilios;
		this.email=afianzadora.getCorreoElectronico();
		this.tipoSituacion=(afianzadora.getClaveEstatus()!=null)?afianzadora.getClaveEstatus().shortValue():null;
		this.extension=(afianzadora.getExtensionOficina()!=null)?afianzadora.getExtensionOficina().toString():null;
		this.claveTipoPersona= 2;//afianzadora siempre sera moral
		this.telFax=afianzadora.getFaxOficina();
		this.fechaConstitucion=afianzadora.getFechaAlta();
		String codigoRFC=afianzadora.getSiglasRfc()+afianzadora.getFechaRfc()+afianzadora.getHomoclaveRfc();
		this.codigoRFC=codigoRFC;
//		this.claveSectorFinanciero=(afianzadora.getSector()!=null && afianzadora.getSector().getIdSector()!=null)?"0"+afianzadora.getSector().getIdSector().toString():null;
		this.paginaWeb=afianzadora.getPaginaWeb();
		this.razonSocial=afianzadora.getRazonSocial();
		this.telCelular=afianzadora.getTelefonoCelular();
		this.telOficina=afianzadora.getTelefonoOficina();
		this.telCasa=afianzadora.getTelefonoParticular();
		this.telefono=afianzadora.getTelefonoParticular();
	}
		
}
