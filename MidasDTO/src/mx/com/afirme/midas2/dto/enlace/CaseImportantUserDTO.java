package mx.com.afirme.midas2.dto.enlace;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * CaseImportantUserDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOCASEIMPORTANTUSER", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = "USERNAME"))
public class CaseImportantUserDTO implements java.io.Serializable, Entidad {

	/** serialVersionUID	 **/
	private static final long serialVersionUID = -5670989698196460951L;
	// Fields

	private Long id;
	private String userName;
	private Integer isVip;

	// Constructors

	/** default constructor */
	public CaseImportantUserDTO() {
	}

	/** full constructor */
	public CaseImportantUserDTO(Long id, String userName, Integer isVip) {
		this.id = id;
		this.userName = userName;
		this.isVip = isVip;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "TOCASEIMPORTANTUSER_SEQ", allocationSize = 1, sequenceName = "MIDAS.TOCASEIMPORTANTUSER_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TOCASEIMPORTANTUSER_SEQ")
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "USERNAME", unique = true, nullable = false, length = 20)
	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "ISVIP", nullable = false, precision = 1, scale = 0)
	public Integer getIsVip() {
		return this.isVip;
	}

	public void setIsVip(Integer isVip) {
		this.isVip = isVip;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Long getKey() {
		return getId();
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}

}