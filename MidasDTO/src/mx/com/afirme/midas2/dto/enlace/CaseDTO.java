package mx.com.afirme.midas2.dto.enlace;


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * CaseDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOCASE", schema = "MIDAS")
public class CaseDTO implements java.io.Serializable, Entidad {

	public enum StatusEnum {
		NEW(1, "Nuevo"), ASSIGNED(2, "Asignado"), CLOSED(3, "Cerrado");

		private int id;
		private String name;

		StatusEnum(int id, String name) {
			this.id = id;
			this.name = name;
		}

		public int getId() {
			return this.id;
		}
		
		public String getName() {
			return this.name;
		}
	}
	
	public enum Severity {
		HIGHT(1, "Alta"), NORMAL(2, "Normal"), LOW(3, "Baja");

		private int id;
		private String name;

		Severity(int id, String name) {
			this.id = id;
			this.name = name;
		}

		public int getId() {
			return this.id;
		}
		
		public String getName() {
			return this.name;
		}
	}
	
	public enum UserAssignedType {
		DIRECTOR(1), MANAGER(2), EMPLOYEE(3);

		private int id;

		UserAssignedType(int id) {
			this.id = id;
		}

		public int getId() {
			return this.id;
		}
		
	}

	// Fields

	/** serialVersionUID **/
	private static final long serialVersionUID = 5715863223620917541L;
	private Long id;
	private CaseTypeDTO caseTypeDTO;
	private Long caseTypeId;
	private Integer status;
	private Integer caseSeverity;
	private String description;
	private String createUser;
	private Date createDate;
	private String assignedUser;
	private Set<CaseConversationDTO> caseConversationDTOs = new HashSet<CaseConversationDTO>(0);

	// Transient
	private String severityName;
	private String statusName;
	private String createUserName;
	private String assignedUserName;

	// Constructors

	/** default constructor */
	public CaseDTO() {
	}

	/** minimal constructor */
	public CaseDTO(Long id, CaseTypeDTO caseTypeDTO, Integer status,
			Integer caseSeverity, String description, String createUser,
			Date createDate, String assignedUser) {
		this.id = id;
		this.caseTypeDTO = caseTypeDTO;
		this.status = status;
		this.caseSeverity = caseSeverity;
		this.description = description;
		this.createUser = createUser;
		this.createDate = createDate;
		this.assignedUser = assignedUser;
	}

	@PostLoad
	public void onLoad() {
		if (caseSeverity != null) {
			switch (caseSeverity) {
			case 1:
				severityName = Severity.HIGHT.getName();
				break;
			case 2:
				severityName = Severity.NORMAL.getName();
				break;
			case 3:
				severityName = Severity.LOW.getName();
				break;
			default:
				severityName = "?";
				break;
			}
		}
		if (status != null) {
			switch (status) {
			case 1:
				statusName = StatusEnum.NEW.getName();
				break;
			case 2:
				statusName = StatusEnum.ASSIGNED.getName();
				break;
			case 3:
				statusName = StatusEnum.CLOSED.getName();
				break;
			default:
				statusName = "?";
				break;
			}
		}
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "TOCASE_SEQ", allocationSize = 1, sequenceName = "MIDAS.TOCASE_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TOCASE_SEQ")
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDCASETYPE", nullable = false)
	public CaseTypeDTO getCaseTypeDTO() {
		return this.caseTypeDTO;
	}

	public void setCaseTypeDTO(CaseTypeDTO caseTypeDTO) {
		this.caseTypeDTO = caseTypeDTO;
	}

	@Column(name = "STATUS", nullable = false, precision = 22, scale = 0)
	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Column(name = "CASESEVERITY", nullable = false, precision = 22, scale = 0)
	public Integer getCaseSeverity() {
		return this.caseSeverity;
	}

	public void setCaseSeverity(Integer caseSeverity) {
		this.caseSeverity = caseSeverity;
	}

	@Column(name = "DESCRIPTION", nullable = false, length = 400)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "CREATEUSER", nullable = false, length = 20)
	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "CREATEDATE", nullable = false, length = 7)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column(name = "ASSIGNEDUSER", nullable = true, length = 20)
	public String getAssignedUser() {
		return this.assignedUser;
	}

	public void setAssignedUser(String assignedUser) {
		this.assignedUser = assignedUser;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "caseDTO")
	public Set<CaseConversationDTO> getCaseConversationDTOs() {
		return this.caseConversationDTOs;
	}

	public void setCaseConversationDTOs(
			Set<CaseConversationDTO> caseConversationDTOs) {
		this.caseConversationDTOs = caseConversationDTOs;
	}

	@Transient
	public String getSeverityName() {
		return severityName;
	}
	
	public void setSeverityName(String severityName) {
		this.severityName = severityName;
	}

	@Transient
	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	
	@Column(name = "IDCASETYPE", insertable = false, updatable = false)
	public Long getCaseTypeId() {
		return caseTypeId;
	}

	public void setCaseTypeId(Long caseTypeId) {
		this.caseTypeId = caseTypeId;
	}
	
	@Transient
	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	
	@Transient
	public String getAssignedUserName() {
		return assignedUserName;
	}

	public void setAssignedUserName(String assignedUserName) {
		this.assignedUserName = assignedUserName;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return getId();
	}

	@Override
	public String getValue() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getBusinessKey() {
		return getId();
	}

}