package mx.com.afirme.midas2.dto.enlace;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * CaseHistDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOCASEHIST", schema = "MIDAS")
public class CaseHistDTO implements java.io.Serializable, Entidad {

	/** serialVersionUID **/
	private static final long serialVersionUID = -7306571021429354415L;
	// Fields

	private Long id;
	private CaseDTO caseDTO;
	private Integer iniStatus;
	private Integer endStatus;
	private String createUser;
	private Date createDate;

	// Constructors

	/** default constructor */
	public CaseHistDTO() {
	}

	/** full constructor */
	public CaseHistDTO(Long id, CaseDTO caseDTO, Integer iniStatus,
			Integer endStatus, String createUser, Date createDate) {
		this.id = id;
		this.caseDTO = caseDTO;
		this.iniStatus = iniStatus;
		this.endStatus = endStatus;
		this.createUser = createUser;
		this.createDate = createDate;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "TOCASEHIST_SEQ", allocationSize = 1, sequenceName = "MIDAS.TOCASEHIST_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TOCASEHIST_SEQ")
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDCASE", nullable = false)
	public CaseDTO getCaseDTO() {
		return this.caseDTO;
	}

	public void setCaseDTO(CaseDTO caseDTO) {
		this.caseDTO = caseDTO;
	}

	@Column(name = "INISTATUS", nullable = false, length = 20)
	public Integer getIniStatus() {
		return this.iniStatus;
	}

	public void setIniStatus(Integer iniStatus) {
		this.iniStatus = iniStatus;
	}

	@Column(name = "ENDSTATUS", nullable = false, length = 20)
	public Integer getEndStatus() {
		return this.endStatus;
	}

	public void setEndStatus(Integer endStatus) {
		this.endStatus = endStatus;
	}

	@Column(name = "CREATEUSER", nullable = false, length = 20)
	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "CREATEDATE", nullable = false, length = 7)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Long getKey() {
		return getId();
	}

	@Override
	public String getValue() {
		return null;
	}

}