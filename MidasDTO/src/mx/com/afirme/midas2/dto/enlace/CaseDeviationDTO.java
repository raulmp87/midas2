package mx.com.afirme.midas2.dto.enlace;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * CaseDeviationDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOCASEDEVIATION", schema = "MIDAS")
public class CaseDeviationDTO implements java.io.Serializable, Entidad {

	// Fields

	/** serialVersionUID **/
	private static final long serialVersionUID = 7783482863701296808L;
	private Long id;
	private Date iniDate;
	private Date endDate;
	private String createUser;
	private Date createDate;
	private String deviationUser;

	// Constructors

	/** default constructor */
	public CaseDeviationDTO() {
	}

	/** full constructor */
	public CaseDeviationDTO(Long id, Date iniDate, Date endDate,
			String createUser, Date createDate, String deviationUser) {
		this.id = id;
		this.iniDate = iniDate;
		this.endDate = endDate;
		this.createUser = createUser;
		this.createDate = createDate;
		this.deviationUser = deviationUser;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "TOCASEDEVIATION_SEQ", allocationSize = 1, sequenceName = "MIDAS.TOCASEDEVIATION_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TOCASEDEVIATION_SEQ")
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "INIDATE", nullable = false, length = 7)
	public Date getIniDate() {
		return this.iniDate;
	}

	public void setIniDate(Date iniDate) {
		this.iniDate = iniDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "ENDDATE", nullable = false, length = 7)
	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Column(name = "CREATEUSER", nullable = false, length = 20)
	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "CREATEDATE", nullable = false, length = 7)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column(name = "DEVIATIONUSER", nullable = false, length = 20)
	public String getDeviationUser() {
		return this.deviationUser;
	}

	public void setDeviationUser(String deviationUser) {
		this.deviationUser = deviationUser;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Long getKey() {
		return getId();
	}

	@Override
	public String getValue() {
		return null;
	}

}