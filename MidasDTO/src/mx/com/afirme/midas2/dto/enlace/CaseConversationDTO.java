package mx.com.afirme.midas2.dto.enlace;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * CaseConversationDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TOCASECONVERSATION", schema = "MIDAS")
public class CaseConversationDTO implements java.io.Serializable, Entidad {

	// Fields

	/** serialVersionUID **/
	private static final long serialVersionUID = -8975837276952152786L;
	private Long id;
	private CaseDTO caseDTO;
	private String message;
	private String createUser;
	private Date createDate;
	
	// Transient
	private String createUserName;

	// Constructors

	/** default constructor */
	public CaseConversationDTO() {
	}

	/** full constructor */
	public CaseConversationDTO(Long id, CaseDTO caseDTO, String message,
			String createUser, Date createDate) {
		this.id = id;
		this.caseDTO = caseDTO;
		this.message = message;
		this.createUser = createUser;
		this.createDate = createDate;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "TOCASECONVERSATION_SEQ", allocationSize = 1, sequenceName = "MIDAS.TOCASECONVERSATION_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TOCASECONVERSATION_SEQ")
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDCASE", nullable = false)
	public CaseDTO getCaseDTO() {
		return this.caseDTO;
	}

	public void setCaseDTO(CaseDTO caseDTO) {
		this.caseDTO = caseDTO;
	}

	@Column(name = "MESSAGE", nullable = false, length = 800)
	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Column(name = "CREATEUSER", nullable = false, length = 20)
	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATEDATE", nullable = false, length = 11)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Transient
	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Long getKey() {
		return getId();
	}

	@Override
	public String getValue() {
		return null;
	}

}