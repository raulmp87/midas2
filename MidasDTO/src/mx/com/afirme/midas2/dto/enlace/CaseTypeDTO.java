package mx.com.afirme.midas2.dto.enlace;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * CaseTypeDTO entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TCCASETYPE", schema = "MIDAS")
public class CaseTypeDTO implements java.io.Serializable, Entidad {

	/**serialVersionUID	 **/
	private static final long serialVersionUID = -6572819365882401836L;
	// Fields

	private Long id;
	private String name;

	// Constructors

	/** default constructor */
	public CaseTypeDTO() {
	}

	/** minimal constructor */
	public CaseTypeDTO(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	// Property accessors
	@Id
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "NAME", nullable = false, length = 200)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Long getKey() {
		return getId();
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}

}