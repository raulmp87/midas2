package mx.com.afirme.midas2.dto.enlace;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/**
 * CaseManagerUser entity. @author MyEclipse Persistence Tools
 */
@Entity

@Table(name = "TOCASEMANAGERUSER", schema = "MIDAS", uniqueConstraints = @UniqueConstraint(columnNames = "USERNAME"))
public class CaseManagerUserDTO implements java.io.Serializable, Entidad {

	// Fields

	/** serialVersionUID **/
	private static final long serialVersionUID = 2267574199459583852L;
	private Long id;
	private CaseTypeDTO caseTypeDTO;
	private String userName;
	private Integer userType;
	private String dependsOn;
	private String createUser;
	private Date createDate;

	// Constructors

	/** default constructor */
	public CaseManagerUserDTO() {
	}

	/** minimal constructor */
	public CaseManagerUserDTO(Long id, CaseTypeDTO caseTypeDTO,
			String userName, Integer userType, String createUser,
			Date createDate) {
		this.id = id;
		this.caseTypeDTO = caseTypeDTO;
		this.userName = userName;
		this.userType = userType;
		this.createUser = createUser;
		this.createDate = createDate;
	}

	/** full constructor */
	public CaseManagerUserDTO(Long id, CaseTypeDTO caseTypeDTO,
			String userName, Integer userType, String dependsOn,
			String createUser, Date createDate) {
		this.id = id;
		this.caseTypeDTO = caseTypeDTO;
		this.userName = userName;
		this.userType = userType;
		this.dependsOn = dependsOn;
		this.createUser = createUser;
		this.createDate = createDate;
	}

	// Property accessors
	@Id
	@SequenceGenerator(name = "TOCASEMANAGERUSER_SEQ", allocationSize = 1, sequenceName = "MIDAS.TOCASEMANAGERUSER_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TOCASEMANAGERUSER_SEQ")
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CASETYPEID", nullable = false)
	public CaseTypeDTO getCaseTypeDTO() {
		return this.caseTypeDTO;
	}

	public void setCaseTypeDTO(CaseTypeDTO caseTypeDTO) {
		this.caseTypeDTO = caseTypeDTO;
	}

	@Column(name = "USERNAME", unique = true, nullable = false, length = 20)
	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "USERTYPE", nullable = false, precision = 22, scale = 0)
	public Integer getUserType() {
		return this.userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	@Column(name = "DEPENDSON", length = 20)
	public String getDependsOn() {
		return this.dependsOn;
	}

	public void setDependsOn(String dependsOn) {
		this.dependsOn = dependsOn;
	}

	@Column(name = "CREATEUSER", nullable = false, length = 20)
	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "CREATEDATE", nullable = false, length = 7)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Long getKey() {
		return getId();
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}

}