package mx.com.afirme.midas2.dto.Cargos;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class DetalleCargosView implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6424783604895592619L;

 	 private Long id;
	 private String tipoCargo;
	 private Date fechaAltaCargo;
	 private Long idAgente;
	 private String nombreCompleto;
	 private double importe;
	 private int numCargos;
	 private String estatus;
	 private String fechaString;
	 private Long idConfigCargos;
	 
	 @Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTipoCargo() {
		return tipoCargo;
	}
	public void setTipoCargo(String tipoCargo) {
		this.tipoCargo = tipoCargo;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaAltaCargo() {
		return fechaAltaCargo;
	}
	public void setFechaAltaCargo(Date fechaAltaCargo) {
		this.fechaAltaCargo = fechaAltaCargo;
		if (fechaAltaCargo!= null){
			 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");    	
			this.fechaString = sdf.format(fechaAltaCargo);
		}
	}
	public Long getIdAgente() {
		return idAgente;
	}
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	public double getImporte() {
		return importe;
	}
	public void setImporte(double importe) {
		this.importe = importe;
	}
	public int getNumCargos() {
		return numCargos;
	}
	public void setNumCargos(int numCargos) {
		this.numCargos = numCargos;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getFechaString() {
		return fechaString;
	}
	public void setFechaString(String fechaString) {
		this.fechaString = fechaString;
	}
	public Long getIdConfigCargos() {
		return idConfigCargos;
	}
	public void setIdConfigCargos(Long idConfigCargos) {
		this.idConfigCargos = idConfigCargos;
	}

}
