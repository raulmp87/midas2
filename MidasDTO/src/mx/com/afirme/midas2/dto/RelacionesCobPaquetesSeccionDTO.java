package mx.com.afirme.midas2.dto;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;

public class RelacionesCobPaquetesSeccionDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1469211129613956375L;

	private List<CoberturaSeccionDTO> disponibles;
	
	private List<CoberturaSeccionDTO> asociadas;

	public List<CoberturaSeccionDTO> getDisponibles() {
		return disponibles;
	}

	public void setDisponibles(List<CoberturaSeccionDTO> disponibles) {
		this.disponibles = disponibles;
	}

	public List<CoberturaSeccionDTO> getAsociadas() {
		return asociadas;
	}

	public void setAsociadas(List<CoberturaSeccionDTO> asociadas) {
		this.asociadas = asociadas;
	}
	
}
