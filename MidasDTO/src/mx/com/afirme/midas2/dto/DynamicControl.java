package mx.com.afirme.midas2.dto;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
/**
 * 
 * @author vmhersil
 *
 */
@Retention(value=java.lang.annotation.RetentionPolicy.RUNTIME)
@Target(value={ElementType.METHOD, ElementType.FIELD})
public @interface DynamicControl {
	public static enum TipoControl{
		TEXTBOX(1),SELECT(2),CHECKBOX(3),RADIOBUTTON(4),HIDDEN(5);
		private Integer value;
		private TipoControl(Integer value){
			this.value=value;
		}
		public Integer getValue(){
			return value;
		}
	}
	
	public static enum TipoValidacion{
		NINGUNA(1),NUM_ENTERO(2),NUM_DECIMAL(3),ALFANUMERICO(4);
		private Integer value;
		private TipoValidacion(Integer value){
			this.value=value;
		}
		public Integer getValue(){
			return value;
		}
	}
	
	public String secuencia() default "";
	public String atributoMapeo();
	public boolean esComponenteId() default false;
	public String etiqueta() default "";
	public TipoControl tipoControl();
	//private List<CacheableDTO> elementosCombo;
	
	public boolean esNumerico() default false;
	public boolean permiteNulo() default false;
	public boolean editable() default false;
	public String valor() default "";
	public boolean esValido() default false;
	public String descripcionValidacion() default "";
	
	public String idControlPadre() default "";
	public String idControlHijo() default "";
	public String javaScriptOnChange() default "";
	public String javaScriptOnKeyPress() default "";
	public String javaScriptOnFocus() default "";
	public String className() default "";
	public Class<?>[] vistas() default {};
	public String claveCascada() default "";
	public TipoValidacion tipoValidacion() default mx.com.afirme.midas2.dto.DynamicControl.TipoValidacion.NINGUNA;
	public String longitudMaxima() default "50";
	
}
