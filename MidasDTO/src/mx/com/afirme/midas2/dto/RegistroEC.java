package mx.com.afirme.midas2.dto;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;

import org.apache.poi.ss.util.CellRangeAddress;

public class RegistroEC implements Serializable, Comparable<RegistroEC> {

	private static final long serialVersionUID = 6732815994874901879L;

	private CellRangeAddress rango;
	
	private Integer secuencia;
	
	private Boolean procesado;
	
	private String erroresGenerales;
	
	private List<ControlDescripcionArchivoDTO> representacionesImpresas = new ArrayListNullAware<ControlDescripcionArchivoDTO>();
	
	private List<ControlDescripcionArchivoDTO> cfdiGenerados = new ArrayListNullAware<ControlDescripcionArchivoDTO>();
	

	public CellRangeAddress getRango() {
		return rango;
	}

	public void setRango(CellRangeAddress rango) {
		this.rango = rango;
	}

	public Integer getSecuencia() {
		return secuencia;
	}

	public void setSecuencia(Integer secuencia) {
		this.secuencia = secuencia;
	}

	public Boolean getProcesado() {
		return procesado;
	}

	public void setProcesado(Boolean procesado) {
		this.procesado = procesado;
	}
		
	public String getErroresGenerales() {
		return erroresGenerales;
	}

	public void setErroresGenerales(String erroresGenerales) {
		this.erroresGenerales = erroresGenerales;
	}

	public List<ControlDescripcionArchivoDTO> getRepresentacionesImpresas() {
		return representacionesImpresas;
	}

	public void setRepresentacionesImpresas(
			List<ControlDescripcionArchivoDTO> representacionesImpresas) {
		this.representacionesImpresas = representacionesImpresas;
	}

	public List<ControlDescripcionArchivoDTO> getCfdiGenerados() {
		return cfdiGenerados;
	}

	public void setCfdiGenerados(List<ControlDescripcionArchivoDTO> cfdiGenerados) {
		this.cfdiGenerados = cfdiGenerados;
	}

	public RegistroEC() {
	}

	@Override
	public int compareTo(RegistroEC o) {
		
		if (this.rango != null && o != null && o.getRango() != null) {
			return this.rango.getFirstRow() - o.getRango().getFirstRow();
		}
		
		return 0;
	}
	
	
}
