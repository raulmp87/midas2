package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class DatosCoberturasDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2793739072502054198L;
	private Integer riesgo;
	private String nombreCobertura;
	private String sumaAsegurada;
	private String deducible;
	private Double primaCobertura;
	private Date fechaRegistro;
    private String numeroRegistro;
    private BigDecimal idToCobertura; 
	
	
	public Integer getRiesgo() {
		return riesgo;
	}
	
	public void setRiesgo(Integer riesgo) {
		this.riesgo = riesgo;
	}
	
	public String getNombreCobertura() {
		return nombreCobertura;
	}
	
	public void setNombreCobertura(String nombreCobertura) {
		this.nombreCobertura = nombreCobertura;
	}
	
	public String getSumaAsegurada() {
		return sumaAsegurada;
	}
	
	public void setSumaAsegurada(String sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
	
	public String getDeducible() {
		return deducible;
	}
	
	public void setDeducible(String deducible) {
		this.deducible = deducible;
	}
	
	public Double getPrimaCobertura() {
		return primaCobertura;
	}
	
	public void setPrimaCobertura(Double primaCobertura) {
		this.primaCobertura = primaCobertura;
	}

	/**
	 * @return el fechaRegistro
	 */
	@Temporal(TemporalType.DATE)
	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	/**
	 * @param fechaRegistro el fechaRegistro a establecer
	 */
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	/**
	 * @return el numeroRegistro
	 */
	public String getNumeroRegistro() {
		return numeroRegistro;
	}

	/**
	 * @param numeroRegistro el numeroRegistro a establecer
	 */
	public void setNumeroRegistro(String numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
	}

	public BigDecimal getIdToCobertura() {
		return idToCobertura;
	}

	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}
	
}
