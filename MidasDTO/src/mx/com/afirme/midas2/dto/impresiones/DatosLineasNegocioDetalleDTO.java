package mx.com.afirme.midas2.dto.impresiones;
import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.estilovehiculo.NegocioEstiloVehiculo;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tiposervicio.NegocioTipoServicio;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUso;

public class DatosLineasNegocioDetalleDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String tipoPolizaDesc;
	private String seccionDesc;
	private List<NegocioTipoUso> negocioTipoUso;
	private List<NegocioTipoServicio> negocioTipoServicio;
	private List<NegocioEstiloVehiculo> negocioEstiloVehiculo;
	
	
	/**
	 * @return the tipoPolizaDesc
	 */
	public String getTipoPolizaDesc() {
		return tipoPolizaDesc;
	}
	/**
	 * @param tipoPolizaDesc the tipoPolizaDesc to set
	 */
	public void setTipoPolizaDesc(String tipoPolizaDesc) {
		this.tipoPolizaDesc = tipoPolizaDesc;
	}
	/**
	 * @return the seccionDesc
	 */
	public String getSeccionDesc() {
		return seccionDesc;
	}
	/**
	 * @param seccionDesc the seccionDesc to set
	 */
	public void setSeccionDesc(String seccionDesc) {
		this.seccionDesc = seccionDesc;
	}
	/**
	 * @return the negocioTipo
	 */
	public List<NegocioTipoUso> getNegocioTipoUso() {
		return negocioTipoUso;
	}
	/**
	 * @param negocioTipo the negocioTipo to set
	 */
	public void setNegocioTipoUso(List<NegocioTipoUso> negocioTipoUso) {
		this.negocioTipoUso = negocioTipoUso;
	}
	/**
	 * @return the negocioTipoServicio
	 */
	public List<NegocioTipoServicio> getNegocioTipoServicio() {
		return negocioTipoServicio;
	}
	/**
	 * @param negocioTipoServicio the negocioTipoServicio to set
	 */
	public void setNegocioTipoServicio(List<NegocioTipoServicio> negocioTipoServicio) {
		this.negocioTipoServicio = negocioTipoServicio;
	}
	/**
	 * @return the negocioEstiloVehiculo
	 */
	public List<NegocioEstiloVehiculo> getNegocioEstiloVehiculo() {
		return negocioEstiloVehiculo;
	}
	/**
	 * @param negocioEstiloVehiculo the negocioEstiloVehiculo to set
	 */
	public void setNegocioEstiloVehiculo(
			List<NegocioEstiloVehiculo> negocioEstiloVehiculo) {
		this.negocioEstiloVehiculo = negocioEstiloVehiculo;
	}
	
	
	
}
