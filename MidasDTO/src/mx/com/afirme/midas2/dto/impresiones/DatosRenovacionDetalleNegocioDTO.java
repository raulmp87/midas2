package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacion;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionDesc;

public class DatosRenovacionDetalleNegocioDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private NegocioRenovacion negocioRenovacion;
	private List<NegocioRenovacionDesc> descuentosList;
	private List<NegocioRenovacionDesc> deduciblesList;

	/**
	 * @return the negocioRenovacion
	 */
	public NegocioRenovacion getNegocioRenovacion() {
		return negocioRenovacion;
	}

	/**
	 * @param negocioRenovacion the negocioRenovacion to set
	 */
	public void setNegocioRenovacion(NegocioRenovacion negocioRenovacion) {
		this.negocioRenovacion = negocioRenovacion;
	}

	/**
	 * @return the descuentosList
	 */
	public List<NegocioRenovacionDesc> getDescuentosList() {
		return descuentosList;
	}

	/**
	 * @param descuentosList
	 *            the descuentosList to set
	 */
	public void setDescuentosList(List<NegocioRenovacionDesc> descuentosList) {
		this.descuentosList = descuentosList;
	}

	/**
	 * @return the deduciblesList
	 */
	public List<NegocioRenovacionDesc> getDeduciblesList() {
		return deduciblesList;
	}

	/**
	 * @param deduciblesList
	 *            the deduciblesList to set
	 */
	public void setDeduciblesList(List<NegocioRenovacionDesc> deduciblesList) {
		this.deduciblesList = deduciblesList;
	}

}
