package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;
import java.util.Date;

public class DatosCotizacionExpressM2DTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3129663264053070040L;
	private String nombreCliente;
	private String marca;
	private String descripcion;
	private String estado;
	private Date fecha;
	private String modelo;
	private String idCotizacion;
	
	public String getNombreCliente() {
		return nombreCliente;
	}
	
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	
	public String getMarca() {
		return marca;
	}
	
	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public String getEstado() {
		return estado;
	}
	
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public Date getFecha() {
		return fecha;
	}
	
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	public String getModelo() {
		return modelo;
	}
	
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	
	public String getIdCotizacion() {
		return idCotizacion;
	}
	
	public void setIdCotizacion(String idCotizacion) {
		this.idCotizacion = idCotizacion;
	}	
	
}
