package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;

public class CoberturaImporteDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nombreCobertura;
	private Double importeCobertura;
	
	public String getNombreCobertura() {
		return nombreCobertura;
	}
	public void setNombreCobertura(String nombreCobertura) {
		this.nombreCobertura = nombreCobertura;
	}
	public Double getImporteCobertura() {
		return importeCobertura;
	}
	public void setImporteCobertura(Double importeCobertura) {
		this.importeCobertura = importeCobertura;
	}
}
