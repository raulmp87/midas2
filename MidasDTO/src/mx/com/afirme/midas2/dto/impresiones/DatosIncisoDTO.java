package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;

public class DatosIncisoDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8896585178796176029L;
	private String numeroPoliza;
	private String idInciso;
	private String numeroCotizacion;
	private Date fechaInicioVigencia;
	private Date fechaFinVigencia;
	private String nombreContratante;
	private String nombreContratanteInciso;
	private String calleYNumero;
	private String colonia;
	private String ciudad;
	private String estado;
	private String codigoPostal;
	private String rfcContratante;
	private BigDecimal idToPersonaContratante;
	private Double primaNetaCotizacion;
	private Double montoRecargoPagoFraccionado;
	private Double descuentoComisionCedida=0.0d;
	private Double derechosPoliza;
	private Double montoIVA;
	private Double primaNetaTotal;
	private String descripcionMoneda;
	private String descripcionFormaPago;	
	private String observaciones;
	private String marca;
	private String idClase;
	private String descripcionClase;
	private String modelo;
	private String numeroPlacas;
	private String numeroMotor;
	private String numeroSerie;
	private String numeroAsientos;
	private String tipoUso;
	private String tipoServicio;
	private String tipoCarga;
	private String textoCopia;
	private String numeroCopia;
	private String textoAgregado;
	private String titulo;
	private String tipoPoliza;
	private String subTitulo;
	private String tipoInciso;
	private String idSeccion;
	private String identificador;
	private Double totalPrimas;
	private Collection<DatosCoberturasDTO> coberturas;
	private boolean nationalUnity = false;
	private Double descuento;
	private String clavePolizaSeycos;
	private String numeroIncisoSeycos;
	private List<CondicionEspecial> condicionesEspeciales;
	
	private  String numeroSecuenciaInciso;
	private String numeroEndoso;
	private String nombreProducto;
	private String edoCirculacion;
	private String codigoPostalCirculacion;
	
	
	/**
	 * @return the descuento
	 */
	public Double getDescuento() {
		return descuento;
	}

	/**
	 * @param descuento the descuento to set
	 */
	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}

	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	public String getIdInciso() {
		return idInciso;
	}

	public void setIdInciso(String idInciso) {
		this.idInciso = idInciso;
	}

	public String getNumeroCotizacion() {
		return numeroCotizacion;
	}
	
	public void setNumeroCotizacion(String numeroCotizacion) {
		this.numeroCotizacion = numeroCotizacion;
	}
	
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}
	
	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	
	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}
	
	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}
	
	public String getNombreContratante() {
		return nombreContratante;
	}
	
	public String getNombreContratanteInciso() {
		return nombreContratanteInciso;
	}

	public void setNombreContratanteInciso(String nombreContratanteInciso) {
		this.nombreContratanteInciso = nombreContratanteInciso;
	}

	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}
	
	public String getCalleYNumero() {
		return calleYNumero;
	}
	
	public void setCalleYNumero(String calleYNumero) {
		this.calleYNumero = calleYNumero;
	}
	
	public String getColonia() {
		return colonia;
	}
	
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	
	public String getCiudad() {
		return ciudad;
	}
	
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	
	public String getEstado() {
		return estado;
	}
	
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public String getCodigoPostal() {
		return codigoPostal;
	}
	
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	
	public String getRfcContratante() {
		return rfcContratante;
	}
	
	public void setRfcContratante(String rfcContratante) {
		this.rfcContratante = rfcContratante;
	}
	
	public BigDecimal getIdToPersonaContratante() {
		return idToPersonaContratante;
	}
	
	public void setIdToPersonaContratante(BigDecimal idToPersonaContratante) {
		this.idToPersonaContratante = idToPersonaContratante;
	}
	
	public Double getPrimaNetaCotizacion() {
		return primaNetaCotizacion;
	}
	
	public void setPrimaNetaCotizacion(Double primaNetaCotizacion) {
		this.primaNetaCotizacion = primaNetaCotizacion;
	}
	
	public Double getMontoRecargoPagoFraccionado() {
		return montoRecargoPagoFraccionado;
	}
	
	public void setMontoRecargoPagoFraccionado(Double montoRecargoPagoFraccionado) {
		this.montoRecargoPagoFraccionado = montoRecargoPagoFraccionado;
	}
	
	public Double getDerechosPoliza() {
		return derechosPoliza;
	}
	
	public void setDerechosPoliza(Double derechosPoliza) {
		this.derechosPoliza = derechosPoliza;
	}
	
	public Double getMontoIVA() {
		return montoIVA;
	}
	
	public void setMontoIVA(Double montoIVA) {
		this.montoIVA = montoIVA;
	}
	
	public Double getPrimaNetaTotal() {
		return primaNetaTotal;
	}
	
	public void setPrimaNetaTotal(Double primaNetaTotal) {
		this.primaNetaTotal = primaNetaTotal;
	}
	
	public String getDescripcionMoneda() {
		return descripcionMoneda;
	}
	
	public void setDescripcionMoneda(String descripcionMoneda) {
		this.descripcionMoneda = descripcionMoneda;
	}
	
	public String getDescripcionFormaPago() {
		return descripcionFormaPago;
	}
	
	public void setDescripcionFormaPago(String descripcionFormaPago) {
		this.descripcionFormaPago = descripcionFormaPago;
	}
	
	public String getObservaciones() {
		return observaciones;
	}
	
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	public String getMarca() {
		return marca;
	}
	
	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	public String getIdClase() {
		return idClase;
	}
	
	public void setIdClase(String idClase) {
		this.idClase = idClase;
	}
	
	public String getDescripcionClase() {
		return descripcionClase;
	}
	
	public void setDescripcionClase(String descripcionClase) {
		this.descripcionClase = descripcionClase;
	}
	
	public String getModelo() {
		return modelo;
	}
	
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	
	public String getNumeroPlacas() {
		return numeroPlacas;
	}
	
	public void setNumeroPlacas(String numeroPlacas) {
		this.numeroPlacas = numeroPlacas;
	}
	
	public String getNumeroMotor() {
		return numeroMotor;
	}
	
	public void setNumeroMotor(String numeroMotor) {
		this.numeroMotor = numeroMotor;
	}
	
	public String getNumeroSerie() {
		return numeroSerie;
	}
	
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	
	public String getNumeroAsientos() {
		return numeroAsientos;
	}
	
	public void setNumeroAsientos(String numeroAsientos) {
		this.numeroAsientos = numeroAsientos;
	}
	
	public String getTipoUso() {
		return tipoUso;
	}
	
	public void setTipoUso(String tipoUso) {
		this.tipoUso = tipoUso;
	}
	
	public String getTipoServicio() {
		return tipoServicio;
	}
	
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	
	public String getTipoCarga() {
		return tipoCarga;
	}
	
	public void setTipoCarga(String tipoCarga) {
		this.tipoCarga = tipoCarga;
	}
	
	public String getTextoCopia() {
		return textoCopia;
	}
	
	public void setTextoCopia(String textoCopia) {
		this.textoCopia = textoCopia;
	}
	
	public String getNumeroCopia() {
		return numeroCopia;
	}
	
	public void setNumeroCopia(String numeroCopia) {
		this.numeroCopia = numeroCopia;
	}
	
	public String getTextoAgregado() {
		return textoAgregado;
	}
	
	public void setTextoAgregado(String textoAgregado) {
		this.textoAgregado = textoAgregado;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}	

	public String getTipoPoliza() {
		return tipoPoliza;
	}

	public void setTipoPoliza(String tipoPoliza) {
		this.tipoPoliza = tipoPoliza;
	}

	public void setCoberturas(Collection<DatosCoberturasDTO> coberturas) {
		this.coberturas = coberturas;
	}

	public Collection<DatosCoberturasDTO> getCoberturas() {
		return coberturas;
	}

	public void setSubTitulo(String subTitulo) {
		this.subTitulo = subTitulo;
	}

	public String getSubTitulo() {
		return subTitulo;
	}

	public void setTipoInciso(String tipoInciso) {
		this.tipoInciso = tipoInciso;
	}

	public String getTipoInciso() {
		return tipoInciso;
	}

	public String getIdSeccion() {
		return idSeccion;
	}

	public void setIdSeccion(String idSeccion) {
		this.idSeccion = idSeccion;
	}

	public boolean isNationalUnity() {
		return nationalUnity;
	}

	public void setNationalUnity(boolean nationalUnity) {
		this.nationalUnity = nationalUnity;
	}

	public Double getDescuentoComisionCedida() {
		return descuentoComisionCedida;
	}

	public void setDescuentoComisionCedida(Double descuentoComisionCedida) {
		this.descuentoComisionCedida = descuentoComisionCedida;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	/**
	 * @return the totalPrimas
	 */
	public Double getTotalPrimas() {
		return totalPrimas;
	}

	/**
	 * @param totalPrimas the totalPrimas to set
	 */
	public void setTotalPrimas(Double totalPrimas) {
		this.totalPrimas = totalPrimas;
	}

	public String getClavePolizaSeycos() {
		return clavePolizaSeycos;
	}

	public void setClavePolizaSeycos(String clavePolizaSeycos) {
		this.clavePolizaSeycos = clavePolizaSeycos;
	}

	public String getNumeroIncisoSeycos() {
		return numeroIncisoSeycos;
	}

	public void setNumeroIncisoSeycos(String numeroIncisoSeycos) {
		this.numeroIncisoSeycos = numeroIncisoSeycos;
	}

	public  String getNumeroSecuenciaInciso() {
		 return numeroSecuenciaInciso;
	 }

	public  void setNumeroSecuenciaInciso(String numeroSecuenciaInciso) {
		 this.numeroSecuenciaInciso = numeroSecuenciaInciso;
	 }

	public List<CondicionEspecial> getCondicionesEspeciales() {
		return condicionesEspeciales;
	}

	public void setCondicionesEspeciales(List<CondicionEspecial> condicionesEspeciales) {
		this.condicionesEspeciales = condicionesEspeciales;
	}

	public String getNumeroEndoso() {
		return numeroEndoso;
	}

	public void setNumeroEndoso(String numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}

	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public String getEdoCirculacion() {
		return edoCirculacion;
	}

	public void setEdoCirculacion(String edoCirculacion) {
		this.edoCirculacion = edoCirculacion;
	}

	public String getCodigoPostalCirculacion() {
		return codigoPostalCirculacion;
	}

	public void setCodigoPostalCirculacion(String codigoPostalCirculacion) {
		this.codigoPostalCirculacion = codigoPostalCirculacion;
	}
}
