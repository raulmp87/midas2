package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;
import java.text.DateFormatSymbols;
import java.util.Date;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name = "TT_REP_RECIBOS_PROVEEDORES", schema = "MIDAS")
public class DatosRecibosProveedores implements Entidad, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2144611575923450819L;
	
	private Long id;
	private Long idRecibo;
	private Long numeroPoliza;
	private String numeroPolizaForm;
	private Long idCotizacion;
	private Long numeroFolioRecibo;
	private Integer idMoneda;
	private String descripcionMoneda;
	private String idRamo;
	private String descripcionRamo;
	private String idSubramo;
	private String descripcionSubramo;
	private Long numeroEndoso;
	private Long idAgente;
	private String nombreAgente;
	private Long idSupervisoria;
	private String descripcionSupervisoria;
	private String idOficina;
	private String descripcionOficina;
	private String idGerencia;
	private String descripcionGerencia;
	private Date fechaDesde;
	private Date fechaHasta;
	private String formaPago;
	private String nombreSolicitante;
	private Date fechaCubreDesdeRecibo;
	private Date fechaCubreHastaRecibo;
	private Double emitidoAsistTerc;
	private Double emitidoSustTerc;
	private Double emitidoNationalUnityTerc;
	private Double pagadoAsistTerc;
	private Double pagadoSustTerc;
	private Double pagadoNationalUnityTerc;
	private String periodo;
	private Date fechaProcesado;	
	private String periodoForm;
	
	private Double saldoIniDevAsistTerc;
	private Double saldoIniDevSustTerc;
	private Double saldoIniDevNationalUnityTerc; 
	private Double saldoFinDevAsistTerc;
	private Double saldoFinDevSustTerc;
	private Double saldoFinDevNationalUnityTerc; 
	
	public static final String PLANTILLA_NAME = "midas.auto.reportes.reporteRecibosProveedores.archivo.nombre";
	
	@Id
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "ID_RECIBO", nullable = false, precision = 8)
	public Long getIdRecibo() {
		return idRecibo;
	}

	public void setIdRecibo(Long idRecibo) {
		this.idRecibo = idRecibo;
	}
	
	@Column(name = "NUM_POLIZA", precision = 10)
	public Long getNumeroPoliza() {
		return numeroPoliza;
	}
	
	public void setNumeroPoliza(Long numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	@Column(name = "NUM_POLIZA_FORM")
	public String getNumeroPolizaForm() {
		return numeroPolizaForm;
	}

	public void setNumeroPolizaForm(String numeroPolizaForm) {
		this.numeroPolizaForm = numeroPolizaForm;
	}

	@Column(name = "ID_COTIZACION", precision = 10)
	public Long getIdCotizacion() {
		return idCotizacion;
	}

	public void setIdCotizacion(Long idCotizacion) {
		this.idCotizacion = idCotizacion;
	}

	@Column(name = "NUM_FOLIO_RECIBO", precision = 10)
	public Long getNumeroFolioRecibo() {
		return numeroFolioRecibo;
	}

	public void setNumeroFolioRecibo(Long numeroFolioRecibo) {
		this.numeroFolioRecibo = numeroFolioRecibo;
	}

	@Column(name = "ID_MONEDA", precision = 3)
	public Integer getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(Integer idMoneda) {
		this.idMoneda = idMoneda;
	}

	@Column(name = "DESC_MONEDA")
	public String getDescripcionMoneda() {
		return descripcionMoneda;
	}

	public void setDescripcionMoneda(String descripcionMoneda) {
		this.descripcionMoneda = descripcionMoneda;
	}

	@Column(name = "ID_RAMO")
	public String getIdRamo() {
		return idRamo;
	}

	public void setIdRamo(String idRamo) {
		this.idRamo = idRamo;
	}

	@Column(name = "DESC_RAMO")
	public String getDescripcionRamo() {
		return descripcionRamo;
	}

	public void setDescripcionRamo(String descripcionRamo) {
		this.descripcionRamo = descripcionRamo;
	}

	@Column(name = "ID_SUBRAMO")
	public String getIdSubramo() {
		return idSubramo;
	}

	public void setIdSubramo(String idSubramo) {
		this.idSubramo = idSubramo;
	}

	@Column(name = "DESC_SUBRAMO", nullable = false, precision = 8)
	public String getDescripcionSubramo() {
		return descripcionSubramo;
	}

	public void setDescripcionSubramo(String descripcionSubramo) {
		this.descripcionSubramo = descripcionSubramo;
	}

	@Column(name = "NUM_ENDOSO", precision = 10)
	public Long getNumeroEndoso() {
		return numeroEndoso;
	}

	public void setNumeroEndoso(Long numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}

	@Column(name = "ID_AGENTE", precision = 8)
	public Long getIdAgente() {
		return idAgente;
	}

	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}

	@Column(name = "NOMBRE_AGENTE")
	public String getNombreAgente() {
		return nombreAgente;
	}

	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}

	@Column(name = "ID_SUPERVISORIA")
	public Long getIdSupervisoria() {
		return idSupervisoria;
	}

	public void setIdSupervisoria(Long idSupervisoria) {
		this.idSupervisoria = idSupervisoria;
	}

	@Column(name = "DESC_SUPERVISORIA")
	public String getDescripcionSupervisoria() {
		return descripcionSupervisoria;
	}

	public void setDescripcionSupervisoria(String descripcionSupervisoria) {
		this.descripcionSupervisoria = descripcionSupervisoria;
	}

	@Column(name = "ID_OFICINA")
	public String getIdOficina() {
		return idOficina;
	}

	public void setIdOficina(String idOficina) {
		this.idOficina = idOficina;
	}

	@Column(name = "DESC_OFICINA")
	public String getDescripcionOficina() {
		return descripcionOficina;
	}

	public void setDescripcionOficina(String descripcionOficina) {
		this.descripcionOficina = descripcionOficina;
	}

	@Column(name = "ID_GERENCIA")
	public String getIdGerencia() {
		return idGerencia;
	}

	public void setIdGerencia(String idGerencia) {
		this.idGerencia = idGerencia;
	}

	@Column(name = "DESC_GERENCIA")	
	public String getDescripcionGerencia() {
		return descripcionGerencia;
	}

	public void setDescripcionGerencia(String descripcionGerencia) {
		this.descripcionGerencia = descripcionGerencia;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FH_DESDE")
	public Date getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FH_HASTA")
	public Date getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	@Column(name = "FORMA_PAGO")
	public String getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}

	@Column(name = "NOMBRE_SOLICITANTE")
	public String getNombreSolicitante() {
		return nombreSolicitante;
	}

	public void setNombreSolicitante(String nombreSolicitante) {
		this.nombreSolicitante = nombreSolicitante;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "F_CUBRE_DESDE_RECIBO")
	public Date getFechaCubreDesdeRecibo() {
		return fechaCubreDesdeRecibo;
	}

	public void setFechaCubreDesdeRecibo(Date fechaCubreDesdeRecibo) {
		this.fechaCubreDesdeRecibo = fechaCubreDesdeRecibo;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "F_CUBRE_HASTA_RECIBO")
	public Date getFechaCubreHastaRecibo() {
		return fechaCubreHastaRecibo;
	}

	public void setFechaCubreHastaRecibo(Date fechaCubreHastaRecibo) {
		this.fechaCubreHastaRecibo = fechaCubreHastaRecibo;
	}

	@Column(name = "EMITIDO_ASIST_TERC")
	public Double getEmitidoAsistTerc() {
		return emitidoAsistTerc;
	}

	public void setEmitidoAsistTerc(Double emitidoAsistTerc) {
		this.emitidoAsistTerc = emitidoAsistTerc;
	}

	@Column(name = "EMITIDO_SUST_TERC")
	public Double getEmitidoSustTerc() {
		return emitidoSustTerc;
	}

	public void setEmitidoSustTerc(Double emitidoSustTerc) {
		this.emitidoSustTerc = emitidoSustTerc;
	}

	@Column(name = "EMITIDO_NU_TERC")
	public Double getEmitidoNationalUnityTerc() {
		return emitidoNationalUnityTerc;
	}

	public void setEmitidoNationalUnityTerc(Double emitidoNationalUnityTerc) {
		this.emitidoNationalUnityTerc = emitidoNationalUnityTerc;
	}

	@Column(name = "PAGADO_ASIST_TERC")
	public Double getPagadoAsistTerc() {
		return pagadoAsistTerc;
	}

	public void setPagadoAsistTerc(Double pagadoAsistTerc) {
		this.pagadoAsistTerc = pagadoAsistTerc;
	}

	@Column(name = "PAGADO_SUST_TERC")
	public Double getPagadoSustTerc() {
		return pagadoSustTerc;
	}

	public void setPagadoSustTerc(Double pagadoSustTerc) {
		this.pagadoSustTerc = pagadoSustTerc;
	}

	@Column(name = "PAGADO_NU_TERC")
	public Double getPagadoNationalUnityTerc() {
		return pagadoNationalUnityTerc;
	}
	
	public void setPagadoNationalUnityTerc(Double pagadoNationalUnityTerc) {
		this.pagadoNationalUnityTerc = pagadoNationalUnityTerc;
	}

	@Column(name = "PERIODO")
	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_PROCESADO")
	public Date getFechaProcesado() {
		return fechaProcesado;
	}

	public void setFechaProcesado(Date fechaProcesado) {
		this.fechaProcesado = fechaProcesado;
	}

	@Override
	public Long getKey() {
		// TODO Auto-generated method stub
		return this.id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long getBusinessKey() {
		// TODO Auto-generated method stub
		return this.id;
	}

	@Transient
	public String getPeriodoForm() {
		
		if(periodo != null && !periodo.isEmpty())
		{
			String mes = periodo.substring(0, 2);
			String anio = periodo.substring(2);	
			String[] monthsArray = new DateFormatSymbols(new Locale("es","MX")).getMonths();
			
			periodoForm = monthsArray[Integer.valueOf(mes)-1] + " " + anio;			
		}
		else
		{
			periodoForm = periodo;
		}
		
		return periodoForm;
	}

	@Column(name = "SALDO_INI_DEV_ASIST_TERC")
	public Double getSaldoIniDevAsistTerc() {
		return saldoIniDevAsistTerc;
	}

	public void setSaldoIniDevAsistTerc(Double saldoIniDevAsistTerc) {
		this.saldoIniDevAsistTerc = saldoIniDevAsistTerc;
	}
	
	@Column(name = "SALDO_INI_DEV_SUST_TERC")
	public Double getSaldoIniDevSustTerc() {
		return saldoIniDevSustTerc;
	}

	public void setSaldoIniDevSustTerc(Double saldoIniDevSustTerc) {
		this.saldoIniDevSustTerc = saldoIniDevSustTerc;
	}
	
	@Column(name = "SALDO_INI_DEV_NU_TERC")
	public Double getSaldoIniDevNationalUnityTerc() {
		return saldoIniDevNationalUnityTerc;
	}

	public void setSaldoIniDevNationalUnityTerc(Double saldoIniDevNationalUnityTerc) {
		this.saldoIniDevNationalUnityTerc = saldoIniDevNationalUnityTerc;
	}

	@Column(name = "SALDO_FIN_DEV_ASIST_TERC")
	public Double getSaldoFinDevAsistTerc() {
		return saldoFinDevAsistTerc;
	}

	public void setSaldoFinDevAsistTerc(Double saldoFinDevAsistTerc) {
		this.saldoFinDevAsistTerc = saldoFinDevAsistTerc;
	}	

	@Column(name = "SALDO_FIN_DEV_SUST_TERC")
	public Double getSaldoFinDevSustTerc() {
		return saldoFinDevSustTerc;
	}

	public void setSaldoFinDevSustTerc(Double saldoFinDevSustTerc) {
		this.saldoFinDevSustTerc = saldoFinDevSustTerc;
	}

	@Column(name = "SALDO_FIN_DEV_NU_TERC")
	public Double getSaldoFinDevNationalUnityTerc() {
		return saldoFinDevNationalUnityTerc;
	}

	public void setSaldoFinDevNationalUnityTerc(Double saldoFinDevNationalUnityTerc) {
		this.saldoFinDevNationalUnityTerc = saldoFinDevNationalUnityTerc;
	}
}
