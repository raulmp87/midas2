package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;

public class DatosPrimaPaquetesDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2962404300363135608L;
	private Double primaNeta;

	public Double getPrimaNeta() {
		return primaNeta;
	}

	public void setPrimaNeta(Double primaNeta) {
		this.primaNeta = primaNeta;
	}
	
}
