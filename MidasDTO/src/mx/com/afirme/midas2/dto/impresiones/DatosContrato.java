package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.PaisDTO;
import mx.com.afirme.midas.catalogos.estadocivil.EstadoCivilDTO;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;

public class DatosContrato implements Serializable {

	private static final long serialVersionUID = 2657088149138217993L;
	private String fechaConstString;
	private String ciudad;
	private String tipoAutorizacion;
	private String vigenciaAutorizacion;
	private String domicilioParticular;
	private String intermediacionSeguros;
	private String montoFianzaNumero;
	private String montoFianzaLetra;
	private String vigenciaFianza;
	private String acargoPorCuenta;
	private String acargoAfirme;
	private String responsableVista;
	private String responsableCompensacion;
	private String repLegal;
	private Agente agente;
	private Domicilio domicilio;
	private PaisDTO pais;
	private EstadoCivilDTO estadoCivil;
	private EstadoDTO estadoDTO;
	private MunicipioDTO municipioDTO;
	
	public String getFechaConstString() {
		return fechaConstString;
	}

	public void setFechaConstString(String fechaConstString) {
		this.fechaConstString = fechaConstString;
	}

	public String getRepLegal() {
		return repLegal;
	}

	public void setRepLegal(String repLegal) {
		this.repLegal = repLegal;
	}

	public String getCiudad() {
		return ciudad;
	}
	
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getTipoAutorizacion() {
		return tipoAutorizacion;
	}
	
	public void setTipoAutorizacion(String tipoAutorizacion) {
		this.tipoAutorizacion = tipoAutorizacion;
	}	
		
	public String getVigenciaAutorizacion() {
		return vigenciaAutorizacion;
	}

	public void setVigenciaAutorizacion(String vigenciaAutorizacion) {
		this.vigenciaAutorizacion = vigenciaAutorizacion;
	}

	public String getDomicilioParticular() {
		return domicilioParticular;
	}
	
	public void setDomicilioParticular(String domicilioParticular) {
		this.domicilioParticular = domicilioParticular;
	}
	
	public String getIntermediacionSeguros() {
		return intermediacionSeguros;
	}
	
	public void setIntermediacionSeguros(String intermediacionSeguros) {
		this.intermediacionSeguros = intermediacionSeguros;
	}
	
	public String getMontoFianzaNumero() {
		return montoFianzaNumero;
	}
	
	public void setMontoFianzaNumero(String montoFianzaNumero) {
		this.montoFianzaNumero = montoFianzaNumero;
	}
	
	public String getMontoFianzaLetra() {
		return montoFianzaLetra;
	}
	
	public void setMontoFianzaLetra(String montoFianzaLetra) {
		this.montoFianzaLetra = montoFianzaLetra;
	}
	
	public String getVigenciaFianza() {
		return vigenciaFianza;
	}
	
	public void setVigenciaFianza(String vigenciaFianza) {
		this.vigenciaFianza = vigenciaFianza;
	}
	
	public String getAcargoPorCuenta() {
		return acargoPorCuenta;
	}
	
	public void setAcargoPorCuenta(String acargoPorCuenta) {
		this.acargoPorCuenta = acargoPorCuenta;
	}
	
	public String getAcargoAfirme() {
		return acargoAfirme;
	}
	
	public void setAcargoAfirme(String acargoAfirme) {
		this.acargoAfirme = acargoAfirme;
	}
	
	public String getResponsableVista() {
		return responsableVista;
	}
	
	public void setResponsableVista(String responsableVista) {
		this.responsableVista = responsableVista;
	}
	
	public String getResponsableCompensacion() {
		return responsableCompensacion;
	}
	
	public void setResponsableCompensacion(String responsableCompensacion) {
		this.responsableCompensacion = responsableCompensacion;
	}

	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	public Domicilio getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(Domicilio domicilio) {
		this.domicilio = domicilio;
	}

	public PaisDTO getPais() {
		return pais;
	}

	public void setPais(PaisDTO pais) {
		this.pais = pais;
	}

	public EstadoCivilDTO getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadoCivilDTO estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public EstadoDTO getEstadoDTO() {
		return estadoDTO;
	}

	public void setEstadoDTO(EstadoDTO estadoDTO) {
		this.estadoDTO = estadoDTO;
	}

	public MunicipioDTO getMunicipioDTO() {
		return municipioDTO;
	}

	public void setMunicipioDTO(MunicipioDTO municipioDTO) {
		this.municipioDTO = municipioDTO;
	}	
	
}
