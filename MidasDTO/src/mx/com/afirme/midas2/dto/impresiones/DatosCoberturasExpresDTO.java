package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;

public class DatosCoberturasExpresDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8404270239180810064L;
	private String cobertura;
	private String sumaAseguradaAmplia;	
	private String sumaAseguradaLimitada;
	private String sumaAseguradaBasica;
	private String deducibleAmplia;
	private String deducibleLimitada;
	private String deducibleBasica;
	
	public String getCobertura() {
		return cobertura;
	}
	
	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}
	
	public String getSumaAseguradaAmplia() {
		return sumaAseguradaAmplia;
	}
	
	public void setSumaAseguradaAmplia(String sumaAseguradaAmplia) {
		this.sumaAseguradaAmplia = sumaAseguradaAmplia;
	}
	
	public String getSumaAseguradaLimitada() {
		return sumaAseguradaLimitada;
	}
	
	public void setSumaAseguradaLimitada(String sumaAseguradaLimitada) {
		this.sumaAseguradaLimitada = sumaAseguradaLimitada;
	}
	
	public String getSumaAseguradaBasica() {
		return sumaAseguradaBasica;
	}
	
	public void setSumaAseguradaBasica(String sumaAseguradaBasica) {
		this.sumaAseguradaBasica = sumaAseguradaBasica;
	}
	
	public String getDeducibleAmplia() {
		return deducibleAmplia;
	}
	
	public void setDeducibleAmplia(String deducibleAmplia) {
		this.deducibleAmplia = deducibleAmplia;
	}
	
	public String getDeducibleLimitada() {
		return deducibleLimitada;
	}
	
	public void setDeducibleLimitada(String deducibleLimitada) {
		this.deducibleLimitada = deducibleLimitada;
	}
	
	public String getDeducibleBasica() {
		return deducibleBasica;
	}
	
	public void setDeducibleBasica(String deducibleBasica) {
		this.deducibleBasica = deducibleBasica;
	}
		
}
