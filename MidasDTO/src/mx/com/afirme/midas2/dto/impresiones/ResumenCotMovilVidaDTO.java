package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;

import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.domain.movil.cotizador.CotizacionMovilDTO;

public class ResumenCotMovilVidaDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2855059608902210280L;
	private CotizacionMovilDTO cotizacionMovil;
	
	private String mensualBasico;
	private String trimestralBasico;
	private String semestralBasico;
	
	private String mensualPlatino;
	private String trimestralPlatino;
	private String semestralPlatino;
	
	private String edad;
	private String derechoPoiza;
	
	public ResumenCotMovilVidaDTO() {
	}
	
	public ResumenCotMovilVidaDTO(Paquete paquete, FormaPagoDTO formaPago, CotizacionMovilDTO cotizacionMovil) {
		this.setCotizacionMovil(cotizacionMovil);
	}

	public void setCotizacionMovil(CotizacionMovilDTO cotizacionMovil) {
		this.cotizacionMovil = cotizacionMovil;
	}

	public CotizacionMovilDTO getCotizacionMovil() {
		return cotizacionMovil;
	}

	public String getMensualBasico() {
		return mensualBasico;
	}

	public void setMensualBasico(String mensualBasico) {
		this.mensualBasico = mensualBasico;
	}

	public String getTrimestralBasico() {
		return trimestralBasico;
	}

	public void setTrimestralBasico(String trimestralBasico) {
		this.trimestralBasico = trimestralBasico;
	}

	public String getSemestralBasico() {
		return semestralBasico;
	}

	public void setSemestralBasico(String semestralBasico) {
		this.semestralBasico = semestralBasico;
	}

	public String getMensualPlatino() {
		return mensualPlatino;
	}

	public void setMensualPlatino(String mensualPlatino) {
		this.mensualPlatino = mensualPlatino;
	}

	public String getTrimestralPlatino() {
		return trimestralPlatino;
	}

	public void setTrimestralPlatino(String trimestralPlatino) {
		this.trimestralPlatino = trimestralPlatino;
	}

	public String getSemestralPlatino() {
		return semestralPlatino;
	}

	public void setSemestralPlatino(String semestralPlatino) {
		this.semestralPlatino = semestralPlatino;
	}

	public String getEdad() {
		return edad;
	}

	public void setEdad(String edad) {
		this.edad = edad;
	}

	public String getDerechoPoiza() {
		return derechoPoiza;
	}

	public void setDerechoPoiza(String derechoPoiza) {
		this.derechoPoiza = derechoPoiza;
	}
	
}
