package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas2.dto.CondicionEspecialDTO;

public class DatosCaratulaInciso extends GeneralesDatosImpresion implements Serializable {

	private static final long serialVersionUID = 2818164512269314511L;
	private List<DatosCoberturasDTO> dataSourceCoberturas;
	private DatosIncisoDTO dataSourceIncisos;
	private List<IncisoCotizacionDTO> listadoIncisos;
	private List<CondicionEspecialDTO> listadoCondicionesEspeciales;
	
	public List<DatosCoberturasDTO> getDataSourceCoberturas() {
		return dataSourceCoberturas;
	}
	
	public void setDataSourceCoberturas(
			List<DatosCoberturasDTO> dataSourceCoberturas) {
		this.dataSourceCoberturas = dataSourceCoberturas;
	}
	
	public DatosIncisoDTO getDataSourceIncisos() {
		return dataSourceIncisos;
	}
	
	public void setDataSourceIncisos(DatosIncisoDTO dataSourceIncisos) {
		this.dataSourceIncisos = dataSourceIncisos;
	}

	public List<IncisoCotizacionDTO> getListadoIncisos() {
		return listadoIncisos;
	}

	public void setListadoIncisos(List<IncisoCotizacionDTO> listadoIncisos) {
		this.listadoIncisos = listadoIncisos;
	}

	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("dataSourceCoberturas: " + this.dataSourceCoberturas + ", ");
		sb.append("dataSourceIncisos: " + this.dataSourceIncisos + ", ");
		sb.append("listadoIncisos: " + this.listadoIncisos);
		sb.append("]");
		
		return sb.toString();
	}

	public List<CondicionEspecialDTO> getListadoCondicionesEspeciales() {
		return listadoCondicionesEspeciales;
	}

	public void setListadoCondicionesEspeciales(
			List<CondicionEspecialDTO> listadoCondicionesEspeciales) {
		this.listadoCondicionesEspeciales = listadoCondicionesEspeciales;
	}
}
