package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
@Entity
public class PagarePrestamoAnticipoImpresion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3601543615829484369L;
	
	private Long id;
	private Double capitalInicial;
	private String capitalInicialLetra;
	private Double interesOrdinario;
	private String interesOrdinarioLetra;
	private String plazoInteresOrdinario;
	private String lugarSuscripcion;
	private Date fechaSuscripcion;
	private Date fechaVencimiento;
	private String nombreCompletoSuscriptor;
	private String calleYNumeroSuscriptor;
	private String coloniaSuscriptor;
	private String municipioSuscriptor;
	private Long codigoPostalSuscriptor;
	private String estadoSuscriptor;
	private String telefonoSuscriptor;
	private String nombreCompletoAval;
	private String calleYNumeroAval;
	private String coloniaAval;
	private String municipioAval;
	private Integer codigoPostalAval;
	private String estadoAval;
	private String telefonoAval;
	
	
	
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Double getCapitalInicial() {
		return capitalInicial;
	}
	public void setCapitalInicial(Double capitalInicial) {
		this.capitalInicial = capitalInicial;
	}
	
	@Transient
	public String getCapitalInicialLetra() {
		return capitalInicialLetra;
	}
	public void setCapitalInicialLetra(String capitalInicialLetra) {
		this.capitalInicialLetra = capitalInicialLetra;
	}
	public Double getInteresOrdinario() {
		return interesOrdinario;
	}
	public void setInteresOrdinario(Double interesOrdinario) {
		this.interesOrdinario = interesOrdinario;
	}	

	@Transient
	public String getInteresOrdinarioLetra() {
		return interesOrdinarioLetra;
	}	
	public void setInteresOrdinarioLetra(String interesOrdinarioLetra) {
		this.interesOrdinarioLetra = interesOrdinarioLetra;
	}
	
	public String getPlazoInteresOrdinario() {
		return plazoInteresOrdinario;
	}
	public void setPlazoInteresOrdinario(String plazoInteresOrdinario) {
		this.plazoInteresOrdinario = plazoInteresOrdinario;
	}
	

	@Transient
	public String getLugarSuscripcion() {
		return lugarSuscripcion;
	}	
	public void setLugarSuscripcion(String lugarSuscripcion) {
		this.lugarSuscripcion = lugarSuscripcion;
	}
	
	@Temporal(TemporalType.DATE)
	public Date getFechaSuscripcion() {
		return fechaSuscripcion;
	}
	public void setFechaSuscripcion(Date fechaSuscripcion) {
		this.fechaSuscripcion = fechaSuscripcion;
	}
	
	@Temporal(TemporalType.DATE)
	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public String getNombreCompletoSuscriptor() {
		return nombreCompletoSuscriptor;
	}
	public void setNombreCompletoSuscriptor(String nombreCompletoSuscriptor) {
		this.nombreCompletoSuscriptor = nombreCompletoSuscriptor;
	}
	public String getCalleYNumeroSuscriptor() {
		return calleYNumeroSuscriptor;
	}
	public void setCalleYNumeroSuscriptor(String calleYNumeroSuscriptor) {
		this.calleYNumeroSuscriptor = calleYNumeroSuscriptor;
	}
	public String getColoniaSuscriptor() {
		return coloniaSuscriptor;
	}
	public void setColoniaSuscriptor(String coloniaSuscriptor) {
		this.coloniaSuscriptor = coloniaSuscriptor;
	}
	public String getMunicipioSuscriptor() {
		return municipioSuscriptor;
	}
	public void setMunicipioSuscriptor(String municipioSuscriptor) {
		this.municipioSuscriptor = municipioSuscriptor;
	}
	public Long getCodigoPostalSuscriptor() {
		return codigoPostalSuscriptor;
	}
	public void setCodigoPostalSuscriptor(Long codigoPostalSuscriptor) {
		this.codigoPostalSuscriptor = codigoPostalSuscriptor;
	}
	public String getEstadoSuscriptor() {
		return estadoSuscriptor;
	}
	public void setEstadoSuscriptor(String estadoSuscriptor) {
		this.estadoSuscriptor = estadoSuscriptor;
	}
	public String getTelefonoSuscriptor() {
		return telefonoSuscriptor;
	}
	public void setTelefonoSuscriptor(String telefonoSuscriptor) {
		this.telefonoSuscriptor = telefonoSuscriptor;
	}
	public String getNombreCompletoAval() {
		return nombreCompletoAval;
	}
	public void setNombreCompletoAval(String nombreCompletoAval) {
		this.nombreCompletoAval = nombreCompletoAval;
	}
	public String getCalleYNumeroAval() {
		return calleYNumeroAval;
	}
	public void setCalleYNumeroAval(String calleYNumeroAval) {
		this.calleYNumeroAval = calleYNumeroAval;
	}
	public String getColoniaAval() {
		return coloniaAval;
	}
	public void setColoniaAval(String coloniaAval) {
		this.coloniaAval = coloniaAval;
	}
	public String getMunicipioAval() {
		return municipioAval;
	}
	public void setMunicipioAval(String municipioAval) {
		this.municipioAval = municipioAval;
	}
	public Integer getCodigoPostalAval() {
		return codigoPostalAval;
	}
	public void setCodigoPostalAval(Integer codigoPostalAval) {
		this.codigoPostalAval = codigoPostalAval;
	}
	public String getEstadoAval() {
		return estadoAval;
	}
	public void setEstadoAval(String estadoAval) {
		this.estadoAval = estadoAval;
	}
	public String getTelefonoAval() {
		return telefonoAval;
	}
	public void setTelefonoAval(String telefonoAval) {
		this.telefonoAval = telefonoAval;
	}
	
	
}
