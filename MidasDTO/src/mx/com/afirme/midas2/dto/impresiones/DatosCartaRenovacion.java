package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;

public class DatosCartaRenovacion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6259220295489699179L;

	private String nombreContratante;
	private String nombreAsegurado;
	private String fechaDDMMMMYYYY;
	private String fechaMMMMYYYY;
	private String sucursal;
	private String bancos;
	
	
	
	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}
	public String getNombreContratante() {
		return nombreContratante;
	}
	public void setFechaDDMMMMYYYY(String fechaDDMMMMYYYY) {
		this.fechaDDMMMMYYYY = fechaDDMMMMYYYY;
	}
	public String getFechaDDMMMMYYYY() {
		return fechaDDMMMMYYYY;
	}
	public void setFechaMMMMYYYY(String fechaMMMMYYYY) {
		this.fechaMMMMYYYY = fechaMMMMYYYY;
	}
	public String getFechaMMMMYYYY() {
		return fechaMMMMYYYY;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}
	public void setBancos(String bancos) {
		this.bancos = bancos;
	}
	public String getBancos() {
		return bancos;
	}
	
	
}
