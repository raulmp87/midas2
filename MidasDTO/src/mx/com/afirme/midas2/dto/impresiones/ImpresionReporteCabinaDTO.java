package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.dto.siniestros.EstimacionCoberturaSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.IncisoSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.PaseAtencionSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.TerceroDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.SiniestroCabinaDTO;

public class ImpresionReporteCabinaDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ReporteCabina reporteCabina;
	private Usuario usuario;
	private String horaDeImpresion;
	private IncisoSiniestroDTO incisoSiniestro;
	private SiniestroCabinaDTO siniestroCabina;
	private String estilo;
	private String marca;
	private EstimacionCoberturaReporteCabina estimacionCoberturaReporte;
	private TerceroDTO datosTerceroVehiculo;
	private AutoIncisoReporteCabina autoIncisoReporteCabina = new AutoIncisoReporteCabina();
	private List<PaseAtencionSiniestroDTO> paseAtencionSiniestroDTO = new ArrayList<PaseAtencionSiniestroDTO>();
	private List<CoberturaReporteCabina> coberturasReporteCabina = new ArrayList<CoberturaReporteCabina>();
	private List<TerceroDTO> pasesVehiculos = new ArrayList<TerceroDTO>();
	private List<TerceroDTO> pasesMedicos = new ArrayList<TerceroDTO>();
	private List<TerceroDTO> pasesBienes = new ArrayList<TerceroDTO>();
	private List<CoberturaImporteDTO> afectaciones = new ArrayList<CoberturaImporteDTO>();
	private EstimacionCoberturaSiniestroDTO estimacionCoberturaSiniestroDTO = new EstimacionCoberturaSiniestroDTO();
	private Double estimacionDaniosMateriales = 0.00;
	private Double estimacionGastosMedicos = 0.00;
	private Double estimacionResponsabilidadCivil = 0.00;
	private String estatusReporte;
	private String causaSiniestro;
	
	
	
	public ReporteCabina getReporteCabina() {
		return reporteCabina;
	}
	public void setReporteCabina(ReporteCabina reporteCabina) {
		this.reporteCabina = reporteCabina;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public String getHoraDeImpresion() {
		return horaDeImpresion;
	}
	public void setHoraDeImpresion(String horaDeImpresion) {
		this.horaDeImpresion = horaDeImpresion;
	}
	public SiniestroCabinaDTO getSiniestroCabina() {
		return siniestroCabina;
	}
	public void setSiniestroCabina(SiniestroCabinaDTO siniestroCabina) {
		this.siniestroCabina = siniestroCabina;
	}
	public IncisoSiniestroDTO getIncisoSiniestro() {
		return incisoSiniestro;
	}
	public void setIncisoSiniestro(IncisoSiniestroDTO incisoSiniestro) {
		this.incisoSiniestro = incisoSiniestro;
	}
	public EstimacionCoberturaReporteCabina getEstimacionCoberturaReporte() {
		return estimacionCoberturaReporte;
	}
	public void setEstimacionCoberturaReporte(
			EstimacionCoberturaReporteCabina estimacionCoberturaReporte) {
		this.estimacionCoberturaReporte = estimacionCoberturaReporte;
	}
	public List<PaseAtencionSiniestroDTO> getPaseAtencionSiniestroDTO() {
		return paseAtencionSiniestroDTO;
	}
	public void setPaseAtencionSiniestroDTO(
			List<PaseAtencionSiniestroDTO> paseAtencionSiniestroDTO) {
		this.paseAtencionSiniestroDTO = paseAtencionSiniestroDTO;
	}
	public List<CoberturaReporteCabina> getCoberturasReporteCabina() {
		return coberturasReporteCabina;
	}
	public void setCoberturasReporteCabina(
			List<CoberturaReporteCabina> coberturasReporteCabina) {
		this.coberturasReporteCabina = coberturasReporteCabina;
	}
	public AutoIncisoReporteCabina getAutoIncisoReporteCabina() {
		return autoIncisoReporteCabina;
	}
	public void setAutoIncisoReporteCabina(
			AutoIncisoReporteCabina autoIncisoReporteCabina) {
		this.autoIncisoReporteCabina = autoIncisoReporteCabina;
	}
	public String getEstilo() {
		return estilo;
	}
	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}	
	public List<TerceroDTO> getPasesVehiculos() {
		return pasesVehiculos;
	}
	public void setPasesVehiculos(List<TerceroDTO> pasesVehiculos) {
		this.pasesVehiculos = pasesVehiculos;
	}
	public List<TerceroDTO> getPasesMedicos() {
		return pasesMedicos;
	}
	public void setPasesMedicos(List<TerceroDTO> pasesMedicos) {
		this.pasesMedicos = pasesMedicos;
	}
	public List<TerceroDTO> getPasesBienes() {
		return pasesBienes;
	}
	public void setPasesBienes(List<TerceroDTO> pasesBienes) {
		this.pasesBienes = pasesBienes;
	}	
	public List<CoberturaImporteDTO> getAfectaciones() {
		return afectaciones;
	}
	public void setAfectaciones(List<CoberturaImporteDTO> afectaciones) {
		this.afectaciones = afectaciones;
	}
	public EstimacionCoberturaSiniestroDTO getEstimacionCoberturaSiniestroDTO() {
		return estimacionCoberturaSiniestroDTO;
	}
	public void setEstimacionCoberturaSiniestroDTO(
			EstimacionCoberturaSiniestroDTO estimacionCoberturaSiniestroDTO) {
		this.estimacionCoberturaSiniestroDTO = estimacionCoberturaSiniestroDTO;
	}
	public Double getEstimacionDaniosMateriales() {
		return estimacionDaniosMateriales;
	}
	public void setEstimacionDaniosMateriales(Double estimacionDaniosMateriales) {
		this.estimacionDaniosMateriales= estimacionDaniosMateriales;
	}
	public void plusEstimacionDaniosMateriales(Double estimacionDaniosMateriales) {
		this.estimacionDaniosMateriales+=estimacionDaniosMateriales;
	}
	public void minusEstimacionDaniosMateriales(Double estimacionDaniosMateriales) {
		this.estimacionDaniosMateriales-=estimacionDaniosMateriales;
	}
	public Double getEstimacionGastosMedicos() {
		return estimacionGastosMedicos;
	}
	public void setEstimacionGastosMedicos(Double estimacionGastosMedicos) {
		this.estimacionGastosMedicos = estimacionGastosMedicos;
	}
	public void plusEstimacionGastosMedicos( Double estimacionGastosMedicos ) {
		this.estimacionGastosMedicos+= estimacionGastosMedicos;
	}
	public void minusEstimacionGastosMedicos(Double estimacionGastosMedicos) {
		this.estimacionGastosMedicos-= estimacionGastosMedicos;
	}
	public Double getEstimacionResponsabilidadCivil() {
		return estimacionResponsabilidadCivil;
	}
	public void setEstimacionResponsabilidadCivil(Double estimacionResponsabilidadCivil) {
		this.estimacionResponsabilidadCivil = estimacionResponsabilidadCivil;
	}
	public void plusEstimacionResponsabilidadCivil(Double estimacionResponsabilidadCivil) {
		this.estimacionResponsabilidadCivil+= estimacionResponsabilidadCivil;
	}
	public void minusEstimacionResponsabilidadCivil(Double estimacionResponsabilidadCivil) {
		this.estimacionResponsabilidadCivil-= estimacionResponsabilidadCivil;
	}
	public TerceroDTO getDatosTerceroVehiculo() {
		return datosTerceroVehiculo;
	}
	public void setDatosTerceroVehiculo(TerceroDTO datosTerceroVehiculo) {
		this.datosTerceroVehiculo = datosTerceroVehiculo;
	}
	public String getEstatusReporte() {
		return estatusReporte;
	}
	public void setEstatusReporte(String estatusReporte) {
		this.estatusReporte = estatusReporte;
	}
	public String getCausaSiniestro() {
		return causaSiniestro;
	}
	public void setCausaSiniestro(String causaSiniestro) {
		this.causaSiniestro = causaSiniestro;
	}
	
}
