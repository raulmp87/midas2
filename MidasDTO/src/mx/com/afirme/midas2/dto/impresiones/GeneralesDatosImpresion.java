package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;

public class GeneralesDatosImpresion implements Serializable {
	
	private static final long serialVersionUID = -167987228285015460L;
	
	private ContenedorDatosImpresion contenedorDatosImpresion;

	public ContenedorDatosImpresion getContenedorDatosImpresion() {
		return contenedorDatosImpresion;
	}

	public void setContenedorDatosImpresion(
			ContenedorDatosImpresion contenedorDatosImpresion) {
		this.contenedorDatosImpresion = contenedorDatosImpresion;
	}
	
}
