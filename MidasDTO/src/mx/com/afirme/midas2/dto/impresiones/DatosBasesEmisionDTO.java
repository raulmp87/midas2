package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

public class DatosBasesEmisionDTO implements Serializable {
		
	private	Integer	oficinaEmision;
	private	String	descOficinaEmision;
	private	String	descRamo;
	private	String	ctaContableRamo;
	private	String	descSubramo;
	private	String	ctaContableSubramo;
	private	String	numeroPoliza;
	private	String	numeroRenovacion;
	private	String	monedaPoliza;
	private	String	codigoProducto;
	private	String	descProducto;
	private	String	codigoTipoNegocio;
	private	String	descTipoNegocio;
	private	String	codigoUsuarioAsegurado;
	private	String	nombreUsuarioAsegurado;
	private	Integer	claveAgente;
	private	String	nombreAgente;
	private	String	formaPago;
	private	String	estatusPoliza;
	private	String	giroAsociadoPoliza;
	private	Integer	numeroEndoso;
	private	String	descTipoEndoso;
	private	String	descMotivoEndoso;
	private	Date	fechaEmision;
	private	Date	fechaInicioVigencia;
	private	Date	fechaFinVigencia;
	private	BigDecimal	tipoCambio;
	private	Integer	idPromotoria;
	private	Integer	idGerencia;
	private	Integer	idContratante;
	private	String	codigoUsuarioEmision;
	private	Integer	tipoUsoVehiculo;
	private	Integer	numeroInciso;
	private	String	descSeccion;
	private	String	descCobertura;
	private	BigDecimal	importePrimaNeta;
	private	BigDecimal	importeDerechos;
	private	BigDecimal	importeComisiones;
	private	BigDecimal	importeBonifCom;
	private	BigDecimal	importeComisionesRPF;
	private	BigDecimal	importeBonifcomRPF;
	private	BigDecimal	importeRecargos;
	private	BigDecimal	importeIva;
	private	BigDecimal	bonificacion;
	private	String	promDescripcion;
	private	String	gerenciaNombre;
	private	String	contratanteNombre;
	private	String	contratanteTipoPersona;
	private	Integer	idMedioPago;
	private	String	medioPago;
	private	String	agenteTipoPersona;
	private	BigDecimal	comisionPromotoriaFisica;
	private	BigDecimal	comisionPromotoriaMoral;
	private	BigDecimal	comisionAgenteFisica;
	private	BigDecimal	comisionAgenteMoral;
	private	BigDecimal	comRecargoAgenteFisica;
	private	BigDecimal	comRecargoAgenteMoral;
	private	BigDecimal	importeTotal;
	private String coberturaRcUSA;

	private String codigoTipoUsoVehiculo;
	private String descripcionTipoUsoVehiculo;
	
	private String codigoTipoServicioVehiculo;
	private String descripcionTipoServVehiculo;

	private BigDecimal porcentajeDescuento;
	
	private String parte;
	
	
	
	public String getParte() {
		return parte;
	}


	public void setParte(String parte) {
		this.parte = parte;
	}


	public BigDecimal getPorcentajeDescuento() {
		return porcentajeDescuento;
	}


	public void setPorcentajeDescuento(BigDecimal porcentajeDescuento) {
		this.porcentajeDescuento = porcentajeDescuento;
	}


	public String getCodigoTipoServicioVehiculo() {
		return codigoTipoServicioVehiculo;
	}


	public void setCodigoTipoServicioVehiculo(String codigoTipoServicioVehiculo) {
		this.codigoTipoServicioVehiculo = codigoTipoServicioVehiculo;
	}


	public String getDescripcionTipoServVehiculo() {
		return descripcionTipoServVehiculo;
	}


	public void setDescripcionTipoServVehiculo(String descripcionTipoServVehiculo) {
		this.descripcionTipoServVehiculo = descripcionTipoServVehiculo;
	}


	public String getCodigoTipoUsoVehiculo() {
		return codigoTipoUsoVehiculo;
	}


	public void setCodigoTipoUsoVehiculo(String codigoTipoUsoVehiculo) {
		this.codigoTipoUsoVehiculo = codigoTipoUsoVehiculo;
	}


	public String getDescripcionTipoUsoVehiculo() {
		return descripcionTipoUsoVehiculo;
	}


	public void setDescripcionTipoUsoVehiculo(String descripcionTipoUsoVehiculo) {
		this.descripcionTipoUsoVehiculo = descripcionTipoUsoVehiculo;
	}


	public String getCoberturaRcUSA() {
		return coberturaRcUSA;
	}


	public void setCoberturaRcUSA(String coberturaRcUSA) {
		this.coberturaRcUSA = coberturaRcUSA;
	}


	public BigDecimal getImporteTotal() {
		return importeTotal;
	}


	public void setImporteTotal(BigDecimal importeTotal) {
		this.importeTotal = importeTotal;
	}


	public Integer getOficinaEmision() {
		return oficinaEmision;
	}


	public void setOficinaEmision(Integer oficinaEmision) {
		this.oficinaEmision = oficinaEmision;
	}


	public String getDescOficinaEmision() {
		return descOficinaEmision;
	}


	public void setDescOficinaEmision(String descOficinaEmision) {
		this.descOficinaEmision = descOficinaEmision;
	}


	public String getDescRamo() {
		return descRamo;
	}


	public void setDescRamo(String descRamo) {
		this.descRamo = descRamo;
	}


	public String getCtaContableRamo() {
		return ctaContableRamo;
	}


	public void setCtaContableRamo(String ctaContableRamo) {
		this.ctaContableRamo = ctaContableRamo;
	}


	public String getDescSubramo() {
		return descSubramo;
	}


	public void setDescSubramo(String descSubramo) {
		this.descSubramo = descSubramo;
	}


	public String getCtaContableSubramo() {
		return ctaContableSubramo;
	}


	public void setCtaContableSubramo(String ctaContableSubramo) {
		this.ctaContableSubramo = ctaContableSubramo;
	}


	public String getNumeroPoliza() {
		return numeroPoliza;
	}


	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}


	public String getNumeroRenovacion() {
		return numeroRenovacion;
	}


	public void setNumeroRenovacion(String numeroRenovacion) {
		this.numeroRenovacion = numeroRenovacion;
	}


	public String getMonedaPoliza() {
		return monedaPoliza;
	}


	public void setMonedaPoliza(String monedaPoliza) {
		this.monedaPoliza = monedaPoliza;
	}


	public String getCodigoProducto() {
		return codigoProducto;
	}


	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}


	public String getDescProducto() {
		return descProducto;
	}


	public void setDescProducto(String descProducto) {
		this.descProducto = descProducto;
	}


	public String getCodigoTipoNegocio() {
		return codigoTipoNegocio;
	}


	public void setCodigoTipoNegocio(String codigoTipoNegocio) {
		this.codigoTipoNegocio = codigoTipoNegocio;
	}


	public String getDescTipoNegocio() {
		return descTipoNegocio;
	}


	public void setDescTipoNegocio(String descTipoNegocio) {
		this.descTipoNegocio = descTipoNegocio;
	}


	public String getCodigoUsuarioAsegurado() {
		return codigoUsuarioAsegurado;
	}


	public void setCodigoUsuarioAsegurado(String codigoUsuarioAsegurado) {
		this.codigoUsuarioAsegurado = codigoUsuarioAsegurado;
	}


	public String getNombreUsuarioAsegurado() {
		return nombreUsuarioAsegurado;
	}


	public void setNombreUsuarioAsegurado(String nombreUsuarioAsegurado) {
		this.nombreUsuarioAsegurado = nombreUsuarioAsegurado;
	}


	public Integer getClaveAgente() {
		return claveAgente;
	}


	public void setClaveAgente(Integer claveAgente) {
		this.claveAgente = claveAgente;
	}


	public String getNombreAgente() {
		return nombreAgente;
	}


	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}


	public String getFormaPago() {
		return formaPago;
	}


	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}


	public String getEstatusPoliza() {
		return estatusPoliza;
	}


	public void setEstatusPoliza(String estatusPoliza) {
		this.estatusPoliza = estatusPoliza;
	}


	public String getGiroAsociadoPoliza() {
		return giroAsociadoPoliza;
	}


	public void setGiroAsociadoPoliza(String giroAsociadoPoliza) {
		this.giroAsociadoPoliza = giroAsociadoPoliza;
	}


	public Integer getNumeroEndoso() {
		return numeroEndoso;
	}


	public void setNumeroEndoso(Integer numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}


	public String getDescTipoEndoso() {
		return descTipoEndoso;
	}


	public void setDescTipoEndoso(String descTipoEndoso) {
		this.descTipoEndoso = descTipoEndoso;
	}


	public String getDescMotivoEndoso() {
		return descMotivoEndoso;
	}


	public void setDescMotivoEndoso(String descMotivoEndoso) {
		this.descMotivoEndoso = descMotivoEndoso;
	}


	public Date getFechaEmision() {
		return fechaEmision;
	}


	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}


	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}


	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}


	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}


	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}


	public BigDecimal getTipoCambio() {
		return tipoCambio;
	}


	public void setTipoCambio(BigDecimal tipoCambio) {
		this.tipoCambio = tipoCambio;
	}


	public Integer getIdPromotoria() {
		return idPromotoria;
	}


	public void setIdPromotoria(Integer idPromotoria) {
		this.idPromotoria = idPromotoria;
	}


	public Integer getIdGerencia() {
		return idGerencia;
	}


	public void setIdGerencia(Integer idGerencia) {
		this.idGerencia = idGerencia;
	}


	public Integer getIdContratante() {
		return idContratante;
	}


	public void setIdContratante(Integer idContratante) {
		this.idContratante = idContratante;
	}


	public String getCodigoUsuarioEmision() {
		return codigoUsuarioEmision;
	}


	public void setCodigoUsuarioEmision(String codigoUsuarioEmision) {
		this.codigoUsuarioEmision = codigoUsuarioEmision;
	}


	public Integer getTipoUsoVehiculo() {
		return tipoUsoVehiculo;
	}


	public void setTipoUsoVehiculo(Integer tipoUsoVehiculo) {
		this.tipoUsoVehiculo = tipoUsoVehiculo;
	}


	public Integer getNumeroInciso() {
		return numeroInciso;
	}


	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}


	public String getDescSeccion() {
		return descSeccion;
	}


	public void setDescSeccion(String descSeccion) {
		this.descSeccion = descSeccion;
	}


	public String getDescCobertura() {
		return descCobertura;
	}


	public void setDescCobertura(String descCobertura) {
		this.descCobertura = descCobertura;
	}


	public BigDecimal getImportePrimaNeta() {
		return importePrimaNeta;
	}


	public void setImportePrimaNeta(BigDecimal importePrimaNeta) {
		this.importePrimaNeta = importePrimaNeta;
	}


	public BigDecimal getImporteDerechos() {
		return importeDerechos;
	}


	public void setImporteDerechos(BigDecimal importeDerechos) {
		this.importeDerechos = importeDerechos;
	}


	public BigDecimal getImporteComisiones() {
		return importeComisiones;
	}


	public void setImporteComisiones(BigDecimal importeComisiones) {
		this.importeComisiones = importeComisiones;
	}


	public BigDecimal getImporteBonifCom() {
		return importeBonifCom;
	}


	public void setImporteBonifCom(BigDecimal importeBonifCom) {
		this.importeBonifCom = importeBonifCom;
	}


	public BigDecimal getImporteComisionesRPF() {
		return importeComisionesRPF;
	}


	public void setImporteComisionesRPF(BigDecimal importeComisionesRPF) {
		this.importeComisionesRPF = importeComisionesRPF;
	}


	public BigDecimal getImporteBonifcomRPF() {
		return importeBonifcomRPF;
	}


	public void setImporteBonifcomRPF(BigDecimal importeBonifcomRPF) {
		this.importeBonifcomRPF = importeBonifcomRPF;
	}


	public BigDecimal getImporteRecargos() {
		return importeRecargos;
	}


	public void setImporteRecargos(BigDecimal importeRecargos) {
		this.importeRecargos = importeRecargos;
	}


	public BigDecimal getImporteIva() {
		return importeIva;
	}


	public void setImporteIva(BigDecimal importeIva) {
		this.importeIva = importeIva;
	}


	public BigDecimal getBonificacion() {
		return bonificacion;
	}


	public void setBonificacion(BigDecimal bonificacion) {
		this.bonificacion = bonificacion;
	}


	public String getPromDescripcion() {
		return promDescripcion;
	}


	public void setPromDescripcion(String promDescripcion) {
		this.promDescripcion = promDescripcion;
	}


	public String getGerenciaNombre() {
		return gerenciaNombre;
	}


	public void setGerenciaNombre(String gerenciaNombre) {
		this.gerenciaNombre = gerenciaNombre;
	}


	public String getContratanteNombre() {
		return contratanteNombre;
	}


	public void setContratanteNombre(String contratanteNombre) {
		this.contratanteNombre = contratanteNombre;
	}


	public String getContratanteTipoPersona() {
		return contratanteTipoPersona;
	}


	public void setContratanteTipoPersona(String contratanteTipoPersona) {
		this.contratanteTipoPersona = contratanteTipoPersona;
	}


	public Integer getIdMedioPago() {
		return idMedioPago;
	}


	public void setIdMedioPago(Integer idMedioPago) {
		this.idMedioPago = idMedioPago;
	}


	public String getMedioPago() {
		return medioPago;
	}


	public void setMedioPago(String medioPago) {
		this.medioPago = medioPago;
	}


	public String getAgenteTipoPersona() {
		return agenteTipoPersona;
	}


	public void setAgenteTipoPersona(String agenteTipoPersona) {
		this.agenteTipoPersona = agenteTipoPersona;
	}


	public BigDecimal getComisionPromotoriaFisica() {
		return comisionPromotoriaFisica;
	}


	public void setComisionPromotoriaFisica(BigDecimal comisionPromotoriaFisica) {
		this.comisionPromotoriaFisica = comisionPromotoriaFisica;
	}


	public BigDecimal getComisionPromotoriaMoral() {
		return comisionPromotoriaMoral;
	}


	public void setComisionPromotoriaMoral(BigDecimal comisionPromotoriaMoral) {
		this.comisionPromotoriaMoral = comisionPromotoriaMoral;
	}


	public BigDecimal getComisionAgenteFisica() {
		return comisionAgenteFisica;
	}


	public void setComisionAgenteFisica(BigDecimal comisionAgenteFisica) {
		this.comisionAgenteFisica = comisionAgenteFisica;
	}


	public BigDecimal getComisionAgenteMoral() {
		return comisionAgenteMoral;
	}


	public void setComisionAgenteMoral(BigDecimal comisionAgenteMoral) {
		this.comisionAgenteMoral = comisionAgenteMoral;
	}


	public BigDecimal getComRecargoAgenteFisica() {
		return comRecargoAgenteFisica;
	}


	public void setComRecargoAgenteFisica(BigDecimal comRecargoAgenteFisica) {
		this.comRecargoAgenteFisica = comRecargoAgenteFisica;
	}


	public BigDecimal getComRecargoAgenteMoral() {
		return comRecargoAgenteMoral;
	}


	public void setComRecargoAgenteMoral(BigDecimal comRecargoAgenteMoral) {
		this.comRecargoAgenteMoral = comRecargoAgenteMoral;
	}


	public static class DatosBasesEmisionParametrosDTO implements Serializable {
		private Date fechaInicio;
		private Date fechaFin;
		private Integer idRamo;
		private Integer idSubRamo;
		private Integer idNivelContratoReaseguro;
		private Short nivelAgrupamiento;
		
		
		public DatosBasesEmisionParametrosDTO(Date fechaInicio, Date fechaFin,
				Integer idRamo, Integer idSubRamo,
				Integer idNivelContratoReaseguro, Short nivelAgrupamiento) {
			super();
			this.fechaInicio = fechaInicio;
			this.fechaFin = fechaFin;
			this.idRamo = idRamo;
			this.idSubRamo = idSubRamo;
			this.idNivelContratoReaseguro = idNivelContratoReaseguro;
			this.nivelAgrupamiento = nivelAgrupamiento;
		}

		public Date getFechaInicio() {
			return fechaInicio;
		}

		public void setFechaInicio(Date fechaInicio) {
			this.fechaInicio = fechaInicio;
		}

		public Date getFechaFin() {
			return fechaFin;
		}

		public void setFechaFin(Date fechaFin) {
			this.fechaFin = fechaFin;
		}

		public Integer getIdRamo() {
			return idRamo;
		}

		public void setIdRamo(Integer idRamo) {
			this.idRamo = idRamo;
		}

		public Integer getIdSubRamo() {
			return idSubRamo;
		}

		public void setIdSubRamo(Integer idSubRamo) {
			this.idSubRamo = idSubRamo;
		}

		public Integer getIdNivelContratoReaseguro() {
			return idNivelContratoReaseguro;
		}

		public void setIdNivelContratoReaseguro(Integer idNivelContratoReaseguro) {
			this.idNivelContratoReaseguro = idNivelContratoReaseguro;
		}

		public Short getNivelAgrupamiento() {
			return nivelAgrupamiento;
		}

		public void setNivelAgrupamiento(Short nivelAgrupamiento) {
			this.nivelAgrupamiento = nivelAgrupamiento;
		}
		
		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("fechaInicio:").append(fechaInicio).append(",");
			sb.append("fechaFin:").append(fechaFin).append(",");
			sb.append("idRamo:").append(this.idRamo).append(",");
			sb.append("idSubRamo:").append(this.idSubRamo).append(",");
			sb.append("idNivelContratoReaseguro:").append(this.idNivelContratoReaseguro).append(",");
			sb.append("nivelAgrupamiento:").append(this.nivelAgrupamiento).append(",");
			return sb.toString();
		}
	}
	
	
	
	
	@StaticMetamodel(DatosBasesEmisionDTO.class)
	public static class DatosBasesEmisionDTO_ {
		
		public static volatile SingularAttribute<DatosBasesEmisionDTO,Integer> oficinaEmision;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> descOficinaEmision;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> descRamo;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> ctaContableRamo;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> descSubramo;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> ctaContableSubramo;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> numeroPoliza;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> numeroRenovacion;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> monedaPoliza;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> codigoProducto;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> descProducto;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> codigoTipoNegocio;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> descTipoNegocio;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> codigoUsuarioAsegurado;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> nombreUsuarioAsegurado;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,Integer> claveAgente;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> nombreAgente;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> formaPago;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> estatusPoliza;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> giroAsociadoPoliza;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,Integer> numeroEndoso;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> descTipoEndoso;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> descMotivoEndoso;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,Date> fechaEmision;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,Date> fechaInicioVigencia;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,Date> fechaFinVigencia;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,BigDecimal> tipoCambio;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,Integer> idPromotoria;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,Integer> idGerencia;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,Integer> idContratante;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> codigoUsuarioEmision;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,Integer> tipoUsoVehiculo;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,Integer> numeroInciso;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> descSeccion;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> descCobertura;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,BigDecimal> importePrimaNeta;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,BigDecimal> importeDerechos;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,BigDecimal> importeComisiones;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,BigDecimal> importeBonifCom;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,BigDecimal> importeComisionesRPF;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,BigDecimal> importeBonifcomRPF;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,BigDecimal> importeRecargos;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,BigDecimal> importeIva;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,BigDecimal> bonificacion;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> promDescripcion;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> gerenciaNombre;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> contratanteNombre;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> contratanteTipoPersona;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,Integer> idMedioPago;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> medioPago;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> agenteTipoPersona;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,BigDecimal> comisionPromotoriaFisica;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,BigDecimal> comisionPromotoriaMoral;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,BigDecimal> comisionAgenteFisica;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,BigDecimal> comisionAgenteMoral;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,BigDecimal> comRecargoAgenteFisica;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,BigDecimal> comRecargoAgenteMoral;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,BigDecimal> importeTotal;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> coberturaRcUSA;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> codigoTipoUsoVehiculo;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> descripcionTipoUsoVehiculo;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> codigoTipoServicioVehiculo;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> descripcionTipoServVehiculo;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,BigDecimal> porcentajeDescuento;
		public static volatile SingularAttribute<DatosBasesEmisionDTO,String> parte;


	}
}
