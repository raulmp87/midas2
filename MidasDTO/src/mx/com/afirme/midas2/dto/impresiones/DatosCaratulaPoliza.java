package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas.poliza.PolizaDTO;

public class DatosCaratulaPoliza extends GeneralesDatosImpresion implements Serializable {

	private static final long serialVersionUID = -8398837380092836007L;
	private DatosPolizaDTO datosPolizaDTO;
	private List<DatosLineasDeNegocioDTO> datasourceLineasNegocio;
	private List<ReferenciaBancariaDTO> datasourceReferenciasBancarias;
	private PolizaDTO polizaDTO;
	
	public DatosPolizaDTO getDatosPolizaDTO() {
		return datosPolizaDTO;
	}
	
	public void setDatosPolizaDTO(DatosPolizaDTO datosPolizaDTO) {
		this.datosPolizaDTO = datosPolizaDTO;
	}
	
	public List<DatosLineasDeNegocioDTO> getDatasourceLineasNegocio() {
		return datasourceLineasNegocio;
	}
	
	public void setDatasourceLineasNegocio(
			List<DatosLineasDeNegocioDTO> datasourceLineasNegocio) {
		this.datasourceLineasNegocio = datasourceLineasNegocio;
	}

	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	/**
	 * @return the datasourceReferenciasBancarias
	 */
	public List<ReferenciaBancariaDTO> getDatasourceReferenciasBancarias() {
		return datasourceReferenciasBancarias;
	}

	/**
	 * @param datasourceReferenciasBancarias the datasourceReferenciasBancarias to set
	 */
	public void setDatasourceReferenciasBancarias(
			List<ReferenciaBancariaDTO> datasourceReferenciasBancarias) {
		this.datasourceReferenciasBancarias = datasourceReferenciasBancarias;
	}


	
}
