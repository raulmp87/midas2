package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.DomicilioImpresion;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosGeneralDTO;

public class ContenedorDatosImpresion implements Serializable {

	private static final long serialVersionUID = 6424503649477306725L;
	private ClienteGenericoDTO cliente;
	private DomicilioImpresion domicilioCliente;
	private MonedaDTO moneda;
	private FormaPagoDTO formaPago;
	
	private List<IncisoCotizacionDTO> incisosCotizacionList = new ArrayList<IncisoCotizacionDTO>();
	private List<CoberturaCotizacionDTO> coberturasCotizacionList = null;
	private List<CatalogoValorFijoDTO> posiblesDeducibles = null;	
	private ResumenCostosGeneralDTO resumenCostosDTO = null;	
	
	public ClienteGenericoDTO getCliente() {
		return cliente;
	}
	public void setCliente(ClienteGenericoDTO cliente) {
		this.cliente = cliente;
	}
	public DomicilioImpresion getDomicilioCliente() {
		return domicilioCliente;
	}
	public void setDomicilioCliente(DomicilioImpresion domicilioCliente) {
		this.domicilioCliente = domicilioCliente;
	}
	public MonedaDTO getMoneda() {
		return moneda;
	}
	public void setMoneda(MonedaDTO moneda) {
		this.moneda = moneda;
	}
	public FormaPagoDTO getFormaPago() {
		return formaPago;
	}
	public void setFormaPago(FormaPagoDTO formaPago) {
		this.formaPago = formaPago;
	}
	public List<IncisoCotizacionDTO> getIncisosCotizacionList() {
		return incisosCotizacionList;
	}
	public void setIncisosCotizacionList(
			List<IncisoCotizacionDTO> incisosCotizacionList) {
		this.incisosCotizacionList = incisosCotizacionList;
	}
	public List<CoberturaCotizacionDTO> getCoberturasCotizacionList() {
		return coberturasCotizacionList;
	}
	public void setCoberturasCotizacionList(
			List<CoberturaCotizacionDTO> coberturasCotizacionList) {
		this.coberturasCotizacionList = coberturasCotizacionList;
	}
	public List<CatalogoValorFijoDTO> getPosiblesDeducibles() {
		return posiblesDeducibles;
	}
	public void setPosiblesDeducibles(List<CatalogoValorFijoDTO> posiblesDeducibles) {
		this.posiblesDeducibles = posiblesDeducibles;
	}
	public ResumenCostosGeneralDTO getResumenCostosDTO() {
		return resumenCostosDTO;
	}
	public void setResumenCostosDTO(ResumenCostosGeneralDTO resumenCostosDTO) {
		this.resumenCostosDTO = resumenCostosDTO;
	}			
}
