package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class DatosCoberturasDeNegocioDetalleDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	private String tipoPolizaDesc;
	private String seccionDesc;
	private List<DatosCobPaqueteNegocioDetalleDTO> datosCobPaqueteNegocioDetalleDTO = new LinkedList<DatosCobPaqueteNegocioDetalleDTO>();
	
	/**
	 * @return the tipoPolizaDesc
	 */
	public String getTipoPolizaDesc() {
		return tipoPolizaDesc;
	}
	/**
	 * @param tipoPolizaDesc the tipoPolizaDesc to set
	 */
	public void setTipoPolizaDesc(String tipoPolizaDesc) {
		this.tipoPolizaDesc = tipoPolizaDesc;
	}
	/**
	 * @return the seccionDesc
	 */
	public String getSeccionDesc() {
		return seccionDesc;
	}
	/**
	 * @param seccionDesc the seccionDesc to set
	 */
	public void setSeccionDesc(String seccionDesc) {
		this.seccionDesc = seccionDesc;
	}

	/**
	 * @return the datosCobPaqueteNegocioDetalleDTO
	 */
	public List<DatosCobPaqueteNegocioDetalleDTO> getDatosCobPaqueteNegocioDetalleDTO() {
		return datosCobPaqueteNegocioDetalleDTO;
	}
	/**
	 * @param datosCobPaqueteNegocioDetalleDTO the datosCobPaqueteNegocioDetalleDTO to set
	 */
	public void setDatosCobPaqueteNegocioDetalleDTO(
			List<DatosCobPaqueteNegocioDetalleDTO> datosCobPaqueteNegocioDetalleDTO) {
		this.datosCobPaqueteNegocioDetalleDTO = datosCobPaqueteNegocioDetalleDTO;
	}
	
}
