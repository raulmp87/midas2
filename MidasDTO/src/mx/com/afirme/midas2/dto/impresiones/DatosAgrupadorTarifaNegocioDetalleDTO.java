package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifaSeccion;

/**
 * Almacena los datos de los agurpadores de tarifa que esten asociados a una
 * seccion relacionada con el negocio uff
 * 
 * @author Martin 07/08/2012
 */
public class DatosAgrupadorTarifaNegocioDetalleDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String tipoPolizaDesc;
	private String seccionDesc;
	private List<AgrupadorTarifaSeccion> agrupadorTarifaSeccion = new LinkedList<AgrupadorTarifaSeccion>();
	
	/**
	 * @return the tipoPolizaDesc
	 */
	public String getTipoPolizaDesc() {
		return tipoPolizaDesc;
	}
	/**
	 * @param tipoPolizaDesc the tipoPolizaDesc to set
	 */
	public void setTipoPolizaDesc(String tipoPolizaDesc) {
		this.tipoPolizaDesc = tipoPolizaDesc;
	}
	/**
	 * @return the agrupadorTarifaSeccion
	 */
	public List<AgrupadorTarifaSeccion> getAgrupadorTarifaSeccion() {
		return agrupadorTarifaSeccion;
	}
	/**
	 * @param agrupadorTarifaSeccion the agrupadorTarifaSeccion to set
	 */
	public void setAgrupadorTarifaSeccion(
			List<AgrupadorTarifaSeccion> agrupadorTarifaSeccion) {
		this.agrupadorTarifaSeccion = agrupadorTarifaSeccion;
	}
	/**
	 * @return the seccionDesc
	 */
	public String getSeccionDesc() {
		return seccionDesc;
	}
	/**
	 * @param seccionDesc the seccionDesc to set
	 */
	public void setSeccionDesc(String seccionDesc) {
		this.seccionDesc = seccionDesc;
	}
	
}
