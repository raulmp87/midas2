package mx.com.afirme.midas2.dto.impresiones;

import java.math.BigDecimal;

public class DatosCrossTabDTO {
	private String header;     
	private String row;
	private String row2;
	private String value;
	private String value2;
	private BigDecimal idHeader;
	private BigDecimal idRow;

	public DatosCrossTabDTO(String header, String row, String value, String value2) {
		this.setRow(row);
		this.setHeader(header);
		this.setValue(value);
		this.setValue2(value2);
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getHeader() {
		return header;
	}

	public void setRow(String row) {
		this.row = row;
	}

	public String getRow() {
		return row;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setIdHeader(BigDecimal idHeader) {
		this.idHeader = idHeader;
	}

	public BigDecimal getIdHeader() {
		return idHeader;
	}

	public void setIdRow(BigDecimal idRow) {
		this.idRow = idRow;
	}

	public BigDecimal getIdRow() {
		return idRow;
	}

	public void setValue2(String value2) {
		this.value2 = value2;
	}

	public String getValue2() {
		return value2;
	}

	public void setRow2(String row2) {
		this.row2 = row2;
	}

	public String getRow2() {
		return row2;
	}
	
	
}