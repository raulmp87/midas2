package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;

public class DatosLinNegPorCotDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6240518409652692814L;
	private Double primaNeta; 
	private String numIncisos;
	private Integer unidadesRiesgo;
	
	public Double getPrimaNeta() {
		return primaNeta;
	}
	
	public void setPrimaNeta(Double primaNeta) {
		this.primaNeta = primaNeta;
	}
	
	public String getNumIncisos() {
		return numIncisos;
	}
	
	public void setNumIncisos(String numIncisos) {
		this.numIncisos = numIncisos;
	}
	
	public Integer getUnidadesRiesgo() {
		return unidadesRiesgo;
	}
	
	public void setUnidadesRiesgo(Integer unidadesRiesgo) {
		this.unidadesRiesgo = unidadesRiesgo;
	}
	
}
