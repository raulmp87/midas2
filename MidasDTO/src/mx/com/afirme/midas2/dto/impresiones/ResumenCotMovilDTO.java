package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoIDTO;
import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.domain.movil.cotizador.CotizacionMovilDTO;
import mx.com.afirme.midas2.domain.movil.cotizador.DatosDesglosePagosMovil;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.CotizacionExpress;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.EsquemaPagoCotizacionDTO;

public class ResumenCotMovilDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2855059608902210280L;
	private Paquete paquete;
	private FormaPagoDTO formaPago;
	private FormaPagoIDTO formaPagoIDTO;
	private CotizacionMovilDTO cotizacionMovil;
	private Map<String,EsquemaPagoCotizacionDTO>esquemaPagoMovil = new LinkedHashMap<String, EsquemaPagoCotizacionDTO>();
	DatosDesglosePagosMovil datosDesglosePagosMovil;
	public ResumenCotMovilDTO() {
	}
	
	public ResumenCotMovilDTO(Paquete paquete, FormaPagoDTO formaPago, CotizacionMovilDTO cotizacionMovil) {
		this.paquete = paquete;
		this.formaPago = formaPago;
		this.setCotizacionMovil(cotizacionMovil);
	}
	
	public void setPaquete(Paquete paquete) {
		this.paquete = paquete;
	}
	
	public void setFormaPago(FormaPagoDTO formaPago) {
		this.formaPago = formaPago;
	}
	
	public Paquete getPaquete() {
		return paquete;
	}

	public FormaPagoDTO getFormaPago() {
		return formaPago;
	}



	public void setCotizacionMovil(CotizacionMovilDTO cotizacionMovil) {
		this.cotizacionMovil = cotizacionMovil;
	}

	public CotizacionMovilDTO getCotizacionMovil() {
		return cotizacionMovil;
	}

	public void setEsquemaPagoMovil(Map<String,EsquemaPagoCotizacionDTO> esquemaPagoMovil) {
		this.esquemaPagoMovil = esquemaPagoMovil;
	}

	public Map<String,EsquemaPagoCotizacionDTO> getEsquemaPagoMovil() {
		return esquemaPagoMovil;
	}

	public void setFormaPagoIDTO(FormaPagoIDTO formaPagoIDTO) {
		this.formaPagoIDTO = formaPagoIDTO;
	}

	public FormaPagoIDTO getFormaPagoIDTO() {
		return formaPagoIDTO;
	}

	public DatosDesglosePagosMovil getDatosDesglosePagosMovil() {
		return datosDesglosePagosMovil;
	}

	public void setDatosDesglosePagosMovil(
			DatosDesglosePagosMovil datosDesglosePagosMovil) {
		this.datosDesglosePagosMovil = datosDesglosePagosMovil;
	}
	
	
}
