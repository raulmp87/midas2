package mx.com.afirme.midas2.dto.impresiones;

import java.io.InputStream;
import java.io.Serializable;

public class TransporteImpresionDTO implements Serializable{
	
	public static final String DOC_ESTADO_CUENTA = "ESTADO DE CUENTA";
	public static final String DOC_DETALLE_PRIMAS = "DETALLE DE PRIMAS";
	public static final String DOC_RECIBO_HONORARIOS = "RECIBO DE HONORARIOS";

	private static final long serialVersionUID = 18844320916957235L;
	

	private byte[] byteArray = null;
	private InputStream genericInputStream;
	private String contentType;
	private String fileName;

	public byte[] getByteArray() {
		return byteArray;
	}

	public void setByteArray(byte[] byteArray) {
		this.byteArray = byteArray;
	}

	public InputStream getGenericInputStream() {
		return genericInputStream;
	}

	public void setGenericInputStream(InputStream genericInputStream) {
		this.genericInputStream = genericInputStream;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
