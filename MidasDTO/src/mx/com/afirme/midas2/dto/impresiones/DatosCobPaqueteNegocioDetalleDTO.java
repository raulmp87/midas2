package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;
import java.math.BigDecimal;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas2.domain.catalogos.Paquete;

public class DatosCobPaqueteNegocioDetalleDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String tipoPolizaDesc;
	private String seccionDesc;
	private BigDecimal idToNegCobPaqSeccion;
	private Paquete paquete;
	private MonedaDTO monedaDTO;
	private EstadoDTO estadoDTO;
	private CiudadDTO ciudadDTO;
	private String coberturaDesc;
	private String coberturaDeducibles;
	private Double valorSumaAseguradaMin;
	private Double valorSumaAseguradaMax;
	private Double valorSumaAseguradaDefault;
	
	/**
	 * @return the idToNegCobPaqSeccion
	 */
	public BigDecimal getIdToNegCobPaqSeccion() {
		return idToNegCobPaqSeccion;
	}
	/**
	 * @param idToNegCobPaqSeccion the idToNegCobPaqSeccion to set
	 */
	public void setIdToNegCobPaqSeccion(BigDecimal idToNegCobPaqSeccion) {
		this.idToNegCobPaqSeccion = idToNegCobPaqSeccion;
	}
	/**
	 * @return the paquete
	 */
	public Paquete getPaquete() {
		return paquete;
	}
	/**
	 * @param paquete the paquete to set
	 */
	public void setPaquete(Paquete paquete) {
		this.paquete = paquete;
	}
	/**
	 * @return the monedaDTO
	 */
	public MonedaDTO getMonedaDTO() {
		return monedaDTO;
	}
	/**
	 * @param monedaDTO the monedaDTO to set
	 */
	public void setMonedaDTO(MonedaDTO monedaDTO) {
		this.monedaDTO = monedaDTO;
	}
	/**
	 * @return the estadoDTO
	 */
	public EstadoDTO getEstadoDTO() {
		return estadoDTO;
	}
	/**
	 * @param estadoDTO the estadoDTO to set
	 */
	public void setEstadoDTO(EstadoDTO estadoDTO) {
		this.estadoDTO = estadoDTO;
	}
	/**
	 * @return the ciudadDTO
	 */
	public CiudadDTO getCiudadDTO() {
		return ciudadDTO;
	}
	/**
	 * @param ciudadDTO the ciudadDTO to set
	 */
	public void setCiudadDTO(CiudadDTO ciudadDTO) {
		this.ciudadDTO = ciudadDTO;
	}
	/**
	 * @return the coberturaDesc
	 */
	public String getCoberturaDesc() {
		return coberturaDesc;
	}
	/**
	 * @param coberturaDesc the coberturaDesc to set
	 */
	public void setCoberturaDesc(String coberturaDesc) {
		this.coberturaDesc = coberturaDesc;
	}
	/**
	 * @return the coberturaDeducibles
	 */
	public String getCoberturaDeducibles() {
		return coberturaDeducibles;
	}
	/**
	 * @param coberturaDeducibles the coberturaDeducibles to set
	 */
	public void setCoberturaDeducibles(String coberturaDeducibles) {
		this.coberturaDeducibles = coberturaDeducibles;
	}

	/**
	 * @return the valorSumaAseguradaMin
	 */
	public Double getValorSumaAseguradaMin() {
		return valorSumaAseguradaMin;
	}
	/**
	 * @param valorSumaAseguradaMin the valorSumaAseguradaMin to set
	 */
	public void setValorSumaAseguradaMin(Double valorSumaAseguradaMin) {
		this.valorSumaAseguradaMin = valorSumaAseguradaMin;
	}
	/**
	 * @return the valorSumaAseguradaMax
	 */
	public Double getValorSumaAseguradaMax() {
		return valorSumaAseguradaMax;
	}
	/**
	 * @param valorSumaAseguradaMax the valorSumaAseguradaMax to set
	 */
	public void setValorSumaAseguradaMax(Double valorSumaAseguradaMax) {
		this.valorSumaAseguradaMax = valorSumaAseguradaMax;
	}
	/**
	 * @return the valorSumaAseguradaDefault
	 */
	public Double getValorSumaAseguradaDefault() {
		return valorSumaAseguradaDefault;
	}
	/**
	 * @param valorSumaAseguradaDefault the valorSumaAseguradaDefault to set
	 */
	public void setValorSumaAseguradaDefault(Double valorSumaAseguradaDefault) {
		this.valorSumaAseguradaDefault = valorSumaAseguradaDefault;
	}
	/**
	 * @return the tipoPolizaDesc
	 */
	public String getTipoPolizaDesc() {
		return tipoPolizaDesc;
	}
	/**
	 * @param tipoPolizaDesc the tipoPolizaDesc to set
	 */
	public void setTipoPolizaDesc(String tipoPolizaDesc) {
		this.tipoPolizaDesc = tipoPolizaDesc;
	}
	/**
	 * @return the seccionDesc
	 */
	public String getSeccionDesc() {
		return seccionDesc;
	}
	/**
	 * @param seccionDesc the seccionDesc to set
	 */
	public void setSeccionDesc(String seccionDesc) {
		this.seccionDesc = seccionDesc;
	}
	
	
}
