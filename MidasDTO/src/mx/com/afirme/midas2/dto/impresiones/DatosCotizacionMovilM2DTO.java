package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;
import java.util.Date;

public class DatosCotizacionMovilM2DTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3129663264053070040L;
	private String nombreCliente;
	private String marca;
	private String descripcion;
	private String estado;
	private Date fecha;
	private String modelo;
	private String idCotizacion;
	private String numeroTelefonoCliente;
	private String numeroTelefonoAgente;
	private String emailAgente;
	private String emailCliente;
	private String claveAgente;
	private String clavePromo;
	public String getNombreCliente() {
		return nombreCliente;
	}
	
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	
	public String getMarca() {
		return marca;
	}
	
	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public String getEstado() {
		return estado;
	}
	
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public Date getFecha() {
		return fecha;
	}
	
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	public String getModelo() {
		return modelo;
	}
	
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	
	public String getIdCotizacion() {
		return idCotizacion;
	}
	
	public void setIdCotizacion(String idCotizacion) {
		this.idCotizacion = idCotizacion;
	}

	public String getNumeroTelefonoAgente() {
		return numeroTelefonoAgente;
	}

	public void setNumeroTelefonoAgente(String numeroTelefonoAgente) {
		this.numeroTelefonoAgente = numeroTelefonoAgente;
	}

	public void setNumeroTelefonoCliente(String numeroTelefonoCliente) {
		this.numeroTelefonoCliente = numeroTelefonoCliente;
	}

	public String getNumeroTelefonoCliente() {
		return numeroTelefonoCliente;
	}

	public String getEmailAgente() {
		return emailAgente;
	}

	public void setEmailAgente(String emailAgente) {
		this.emailAgente = emailAgente;
	}

	public String getEmailCliente() {
		return emailCliente;
	}

	public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente;
	}

	public String getClaveAgente() {
		return claveAgente;
	}

	public void setClaveAgente(String claveAgente) {
		this.claveAgente = claveAgente;
	}

	public String getClavePromo() {
		return clavePromo;
	}

	public void setClavePromo(String clavePromo) {
		this.clavePromo = clavePromo;
	}	
	
}
