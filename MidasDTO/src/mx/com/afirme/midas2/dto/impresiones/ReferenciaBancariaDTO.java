package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


public class ReferenciaBancariaDTO implements Serializable {
	
	private static final long serialVersionUID = -2310275007625321646L;
	private Long idReferenciaBancariaDTO;
	private Date fechaVencimiento;
	private Long poliza;
	private Long recibo;
	private String estatus;
	private String referenciaBancaria;
	private String descripcionCobro;
	private Double monto;
	private String afirmeTitulo;
	private String afirmeCuenta;
	private String afirmeReferencia;
	private String banorteTitulo;
	private String banorteCuenta;
	private String banorteReferencia;
	private String santanderTitulo;
	private String santanderCuenta;
	private String santanderReferencia;
	private String bancomerTitulo;
	private String bancomerCuenta;
	private String bancomerReferencia;
	private String oxxoTitulo;
	private String oxxoCuenta;
	private String oxxoReferencia;
	private Date fechaDesde;
	private Date fechaHasta;
	/**
	 * @return the idReferenciaBancariaDTO
	 */
	public Long getIdReferenciaBancariaDTO() {
		return idReferenciaBancariaDTO;
	}
	/**
	 * @param idReferenciaBancariaDTO the idReferenciaBancariaDTO to set
	 */
	public void setIdReferenciaBancariaDTO(Long idReferenciaBancariaDTO) {
		this.idReferenciaBancariaDTO = idReferenciaBancariaDTO;
	}
	
	
	/**
	 * @return the fechaVencimiento
	 */
	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}
	/**
	 * @param fechaVencimiento the fechaVencimiento to set
	 */
	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	/**
	 * @return the poliza
	 */
	public Long getPoliza() {
		return poliza;
	}
	/**
	 * @param poliza the poliza to set
	 */
	public void setPoliza(Long poliza) {
		this.poliza = poliza;
	}
	/**
	 * @return the recibo
	 */
	public Long getRecibo() {
		return recibo;
	}
	/**
	 * @param recibo the recibo to set
	 */
	public void setRecibo(Long recibo) {
		this.recibo = recibo;
	}
	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	/**
	 * @return the referenciaBancaria
	 */
	public String getReferenciaBancaria() {
		return referenciaBancaria;
	}
	/**
	 * @param referenciaBancaria the referenciaBancaria to set
	 */
	public void setReferenciaBancaria(String referenciaBancaria) {
		this.referenciaBancaria = referenciaBancaria;
	
		setReferencias(this.referenciaBancaria);
	
	}
	/**
	 * @return the afirmeTitulo
	 */
	public String getAfirmeTitulo() {
		return afirmeTitulo;
	}
	/**
	 * @param afirmeTitulo the afirmeTitulo to set
	 */
	public void setAfirmeTitulo(String afirmeTitulo) {
		this.afirmeTitulo = afirmeTitulo;
	}
	/**
	 * @return the afirmeCuenta
	 */
	public String getAfirmeCuenta() {
		return afirmeCuenta;
	}
	/**
	 * @param afirmeCuenta the afirmeCuenta to set
	 */
	public void setAfirmeCuenta(String afirmeCuenta) {
		this.afirmeCuenta = afirmeCuenta;
	}
	/**
	 * @return the afirmeReferencia
	 */
	public String getAfirmeReferencia() {
		return afirmeReferencia;
	}
	/**
	 * @param afirmeReferencia the afirmeReferencia to set
	 */
	public void setAfirmeReferencia(String afirmeReferencia) {
		this.afirmeReferencia = afirmeReferencia;
	}
	/**
	 * @return the banorteTitulo
	 */
	public String getBanorteTitulo() {
		return banorteTitulo;
	}
	/**
	 * @param banorteTitulo the banorteTitulo to set
	 */
	public void setBanorteTitulo(String banorteTitulo) {
		this.banorteTitulo = banorteTitulo;
	}
	/**
	 * @return the banorteCuenta
	 */
	public String getBanorteCuenta() {
		return banorteCuenta;
	}
	/**
	 * @param banorteCuenta the banorteCuenta to set
	 */
	public void setBanorteCuenta(String banorteCuenta) {
		this.banorteCuenta = banorteCuenta;
	}
	/**
	 * @return the banorteReferencia
	 */
	public String getBanorteReferencia() {
		return banorteReferencia;
	}
	/**
	 * @param banorteReferencia the banorteReferencia to set
	 */
	public void setBanorteReferencia(String banorteReferencia) {
		this.banorteReferencia = banorteReferencia;
	}
	/**
	 * @return the santanderTitulo
	 */
	public String getSantanderTitulo() {
		return santanderTitulo;
	}
	/**
	 * @param santanderTitulo the santanderTitulo to set
	 */
	public void setSantanderTitulo(String santanderTitulo) {
		this.santanderTitulo = santanderTitulo;
	}
	/**
	 * @return the santanderCuenta
	 */
	public String getSantanderCuenta() {
		return santanderCuenta;
	}
	/**
	 * @param santanderCuenta the santanderCuenta to set
	 */
	public void setSantanderCuenta(String santanderCuenta) {
		this.santanderCuenta = santanderCuenta;
	}
	/**
	 * @return the santanderReferencia
	 */
	public String getSantanderReferencia() {
		return santanderReferencia;
	}
	/**
	 * @param santanderReferencia the santanderReferencia to set
	 */
	public void setSantanderReferencia(String santanderReferencia) {
		this.santanderReferencia = santanderReferencia;
	}
	
	/**
	 * @return the bancomerTitulo
	 */
	public String getBancomerTitulo() {
		return bancomerTitulo;
	}
	/**
	 * @param bancomerTitulo the bancomerTitulo to set
	 */
	public void setBancomerTitulo(String bancomerTitulo) {
		this.bancomerTitulo = bancomerTitulo;
	}
	/**
	 * @return the bancomerCuenta
	 */
	public String getBancomerCuenta() {
		return bancomerCuenta;
	}
	/**
	 * @param bancomerCuenta the bancomerCuenta to set
	 */
	public void setBancomerCuenta(String bancomerCuenta) {
		this.bancomerCuenta = bancomerCuenta;
	}
	/**
	 * @return the bancomerReferencia
	 */
	public String getBancomerReferencia() {
		return bancomerReferencia;
	}
	/**
	 * @param bancomerReferencia the bancomerReferencia to set
	 */
	public void setBancomerReferencia(String bancomerReferencia) {
		this.bancomerReferencia = bancomerReferencia;
	}
	/**
	 * @return the oxxoTitulo
	 */
	public String getOxxoTitulo() {
		return oxxoTitulo;
	}
	/**
	 * @param oxxoTitulo the oxxoTitulo to set
	 */
	public void setOxxoTitulo(String oxxoTitulo) {
		this.oxxoTitulo = oxxoTitulo;
	}
	/**
	 * @return the oxxoCuenta
	 */
	public String getOxxoCuenta() {
		return oxxoCuenta;
	}
	/**
	 * @param oxxoCuenta the oxxoCuenta to set
	 */
	public void setOxxoCuenta(String oxxoCuenta) {
		this.oxxoCuenta = oxxoCuenta;
	}
	/**
	 * @return the oxxoReferencia
	 */
	public String getOxxoReferencia() {
		return oxxoReferencia;
	}
	/**
	 * @param oxxoReferencia the oxxoReferencia to set
	 */
	public void setOxxoReferencia(String oxxoReferencia) {
		this.oxxoReferencia = oxxoReferencia;
	}
	
	/**
	 * @return the fechaDesde
	 */
	public Date getFechaDesde() {
		return fechaDesde;
	}
	/**
	 * @param fechaDesde the fechaDesde to set
	 */
	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	/**
	 * @return the fechaHasta
	 */
	public Date getFechaHasta() {
		return fechaHasta;
	}
	/**
	 * @param fechaHasta the fechaHasta to set
	 */
	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	
	/**
	 * @return the descripcionCobro
	 */
	public String getDescripcionCobro() {
		return descripcionCobro;
	}
	/**
	 * @param descripcionCobro the descripcionCobro to set
	 */
	public void setDescripcionCobro(String descripcionCobro) {
		this.descripcionCobro = descripcionCobro;
	}
	/**
	 * @return the monto
	 */
	public Double getMonto() {
		return monto;
	}
	/**
	 * @param monto the monto to set
	 */
	public void setMonto(Double monto) {
		this.monto = monto;
	}
	/**
	 * 
	 */
	public void setReferencias(String referenciaBancaria) {
		try {
			List<String> items = Arrays.asList(referenciaBancaria.split("##"));	
			
			 this.setAfirmeTitulo(items.get(0));
			 this.setAfirmeCuenta(items.get(1));
			 this.setAfirmeReferencia(items.get(2));
			 this.setBanorteTitulo(items.get(3));
			 this.setBanorteCuenta(items.get(4));
			 this.setBanorteReferencia(items.get(5));
			 this.setSantanderTitulo(items.get(6));
			 this.setSantanderCuenta(items.get(7));
			 this.setSantanderReferencia(items.get(8));
			 this.setBancomerTitulo(items.get(9));
			 this.setBancomerCuenta(items.get(10));
			 this.setBancomerReferencia(items.get(11));
			 this.setOxxoTitulo(items.get(12));
			 this.setOxxoCuenta(items.get(13));
			 this.setOxxoReferencia(items.get(14));
			 this.setDescripcionCobro(items.get(15));
			 try {
				 this.setMonto( Double.parseDouble(items.get(16)));
			} catch (Exception e) {
			e.printStackTrace();
			}
			 
		
			 
			 
	
		} catch (Exception e) {
			e.printStackTrace();	
		}
		
		
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ReferenciaBancariaDTO [idReferenciaBancariaDTO="
				+ idReferenciaBancariaDTO + ", fechaVencimiento="
				+ fechaVencimiento + ", poliza=" + poliza + ", recibo="
				+ recibo + ", estatus=" + estatus + ", referenciaBancaria="
				+ referenciaBancaria + ", descripcionCobro=" + descripcionCobro
				+ ", monto=" + monto + ", afirmeTitulo=" + afirmeTitulo
				+ ", afirmeCuenta=" + afirmeCuenta + ", afirmeReferencia="
				+ afirmeReferencia + ", banorteTitulo=" + banorteTitulo
				+ ", banorteCuenta=" + banorteCuenta + ", banorteReferencia="
				+ banorteReferencia + ", santanderTitulo=" + santanderTitulo
				+ ", santanderCuenta=" + santanderCuenta
				+ ", santanderReferencia=" + santanderReferencia
				+ ", bancomerTitulo=" + bancomerTitulo + ", bancomerCuenta="
				+ bancomerCuenta + ", bancomerReferencia=" + bancomerReferencia
				+ ", oxxoTitulo=" + oxxoTitulo + ", oxxoCuenta=" + oxxoCuenta
				+ ", oxxoReferencia=" + oxxoReferencia + ", fechaDesde="
				+ fechaDesde + ", fechaHasta=" + fechaHasta + "]";
	}




	

}
