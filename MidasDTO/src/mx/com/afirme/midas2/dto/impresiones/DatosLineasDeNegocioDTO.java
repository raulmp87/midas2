package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.ReadOnly;

/**
 * Esta clase se utiliza solamente para como objeto de transporte para mapear el resultado
 * de una nativeQuery. Se marca como @Entity sin embargo no es para que sea utilizado como tal.
 * Debido a que solamente en JPA 2.1 existe manera de definir @SqlResultSetMapping para
 * clases te tuvo que recurrir a esto. Sin embargo además de incluir @ReadOnly y @Cacheable(false)
 * es necesario que los native queries contengan el QueryHints.MAINT_CACHE como FALSE para evitar
 * que se obtenga un valor incorrecto del cache del primer o segundo nivel.
 * Ejemplo:
 * query.setHint(QueryHints.MAINTAIN_CACHE, HintValues.FALSE); 
 */
@Entity
@ReadOnly
@Cacheable(false)
public class DatosLineasDeNegocioDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2310275007625321646L;
	@Id
	private Integer ROWNUM;
	private Integer numIncisos;
	private Double impPrimaNeta;
	private Integer unidadesRiesgo;
	private String lineaNegocio;
	private String numeroRegistro;
	@Temporal(TemporalType.DATE)
	private Date fechaRegistro;
	
	public Integer getROWNUM() {
		return ROWNUM;
	}
	
	public void setROWNUM(Integer rOWNUM) {
		ROWNUM = rOWNUM;
	}
	
	public Integer getNumIncisos() {
		return numIncisos;
	}
	
	public void setNumIncisos(Integer numIncisos) {
		this.numIncisos = numIncisos;
	}
	
	public Double getImpPrimaNeta() {
		return impPrimaNeta;
	}
	
	public void setImpPrimaNeta(Double impPrimaNeta) {
		this.impPrimaNeta = impPrimaNeta;
	}
	
	public Integer getUnidadesRiesgo() {
		return unidadesRiesgo;
	}
	
	public void setUnidadesRiesgo(Integer unidadesRiesgo) {
		this.unidadesRiesgo = unidadesRiesgo;
	}
	
	public String getLineaNegocio() {
		return lineaNegocio;
	}
	
	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}

	/**
	 * @return el numeroRegistro
	 */
	public String getNumeroRegistro() {
		return numeroRegistro;
	}

	/**
	 * @param numeroRegistro el numeroRegistro a establecer
	 */
	public void setNumeroRegistro(String numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
	}

	/**
	 * @return el fechaRegistro
	 */
	@Temporal(TemporalType.DATE)
	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	/**
	 * @param fechaRegistro el fechaRegistro a establecer
	 */
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	
}
