package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.formapago.NegocioFormaPago;
import mx.com.afirme.midas2.domain.negocio.producto.mediopago.NegocioMedioPago;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;

public class DatosProductosDeNegocioDetalleDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private List<NegocioTipoPoliza> negocioTipoPolizas;
	private List<DatosLineasNegocioDetalleDTO> datosLineasNegocioDetalleDTOs;
	private List<DatosPaquetesNegocioDetalleDTO> datosPaquetesNegocioDetalleDTOs;
	private List<DatosCoberturasDeNegocioDetalleDTO> datosCoberturasDeNegocioDetalleDTOs;
	private List<DatosAgrupadorTarifaNegocioDetalleDTO> datosAgrupadorTarifaNegocioDetalleDTOs;
	private NegocioProducto negocioProducto;
	private String tipoPolizaDesc;
	private String seccionDesc;
	private List<NegocioFormaPago> formaPagos;
	private List<NegocioMedioPago> medioPagos;

	/**
	 * @return the negocioProductos
	 */
	public NegocioProducto getNegocioProducto() {
		return negocioProducto;
	}

	/**
	 * @param negocioProductos the negocioProductos to set
	 */
	public void setNegocioProducto(NegocioProducto negocioProducto) {
		this.negocioProducto = negocioProducto;
	}

	/**
	 * @return the negocioTipoPolizas
	 */
	public List<NegocioTipoPoliza> getNegocioTipoPolizas() {
		return negocioTipoPolizas;
	}

	/**
	 * @param negocioTipoPolizas the negocioTipoPolizas to set
	 */
	public void setNegocioTipoPolizas(List<NegocioTipoPoliza> negocioTipoPolizas) {
		this.negocioTipoPolizas = negocioTipoPolizas;
	}

	/**
	 * @return the datosLineasNegocioDetalleDTOs
	 */
	public List<DatosLineasNegocioDetalleDTO> getDatosLineasNegocioDetalleDTOs() {
		return datosLineasNegocioDetalleDTOs;
	}

	/**
	 * @param datosLineasNegocioDetalleDTOs
	 *            the datosLineasNegocioDetalleDTOs to set
	 */
	public void setDatosLineasNegocioDetalleDTOs(
			List<DatosLineasNegocioDetalleDTO> datosLineasNegocioDetalleDTOs) {
		this.datosLineasNegocioDetalleDTOs = datosLineasNegocioDetalleDTOs;
	}

	/**
	 * @return the datosPaquetesNegocioDetalleDTOs
	 */
	public List<DatosPaquetesNegocioDetalleDTO> getDatosPaquetesNegocioDetalleDTOs() {
		return datosPaquetesNegocioDetalleDTOs;
	}

	/**
	 * @param datosPaquetesNegocioDetalleDTOs
	 *            the datosPaquetesNegocioDetalleDTOs to set
	 */
	public void setDatosPaquetesNegocioDetalleDTOs(
			List<DatosPaquetesNegocioDetalleDTO> datosPaquetesNegocioDetalleDTOs) {
		this.datosPaquetesNegocioDetalleDTOs = datosPaquetesNegocioDetalleDTOs;
	}

	/**
	 * @return the datosCoberturasDeNegocioDetalleDTOs
	 */
	public List<DatosCoberturasDeNegocioDetalleDTO> getDatosCoberturasDeNegocioDetalleDTOs() {
		return datosCoberturasDeNegocioDetalleDTOs;
	}

	/**
	 * @param datosCoberturasDeNegocioDetalleDTOs
	 *            the datosCoberturasDeNegocioDetalleDTOs to set
	 */
	public void setDatosCoberturasDeNegocioDetalleDTOs(
			List<DatosCoberturasDeNegocioDetalleDTO> datosCoberturasDeNegocioDetalleDTOs) {
		this.datosCoberturasDeNegocioDetalleDTOs = datosCoberturasDeNegocioDetalleDTOs;
	}

	/**
	 * @return the datosAgrupadorTarifaNegocioDetalleDTOs
	 */
	public List<DatosAgrupadorTarifaNegocioDetalleDTO> getDatosAgrupadorTarifaNegocioDetalleDTOs() {
		return datosAgrupadorTarifaNegocioDetalleDTOs;
	}

	/**
	 * @param datosAgrupadorTarifaNegocioDetalleDTOs
	 *            the datosAgrupadorTarifaNegocioDetalleDTOs to set
	 */
	public void setDatosAgrupadorTarifaNegocioDetalleDTOs(
			List<DatosAgrupadorTarifaNegocioDetalleDTO> datosAgrupadorTarifaNegocioDetalleDTOs) {
		this.datosAgrupadorTarifaNegocioDetalleDTOs = datosAgrupadorTarifaNegocioDetalleDTOs;
	}

	/**
	 * @return the tipoPolizaDesc
	 */
	public String getTipoPolizaDesc() {
		return tipoPolizaDesc;
	}

	/**
	 * @param tipoPolizaDesc the tipoPolizaDesc to set
	 */
	public void setTipoPolizaDesc(String tipoPolizaDesc) {
		this.tipoPolizaDesc = tipoPolizaDesc;
	}

	/**
	 * @return the seccionDesc
	 */
	public String getSeccionDesc() {
		return seccionDesc;
	}

	/**
	 * @param seccionDesc the seccionDesc to set
	 */
	public void setSeccionDesc(String seccionDesc) {
		this.seccionDesc = seccionDesc;
	}

	/**
	 * @return the formaPagos
	 */
	public List<NegocioFormaPago> getFormaPagos() {
		return formaPagos;
	}

	/**
	 * @param formaPagos the formaPagos to set
	 */
	public void setFormaPagos(List<NegocioFormaPago> formaPagos) {
		this.formaPagos = formaPagos;
	}

	/**
	 * @return the medioPagos
	 */
	public List<NegocioMedioPago> getMedioPagos() {
		return medioPagos;
	}

	/**
	 * @param medioPagos the medioPagos to set
	 */
	public void setMedioPagos(List<NegocioMedioPago> medioPagos) {
		this.medioPagos = medioPagos;
	}

}
