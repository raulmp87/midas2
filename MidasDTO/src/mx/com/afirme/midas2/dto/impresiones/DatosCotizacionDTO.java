package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class DatosCotizacionDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1912356115582198380L;
	private String numeroCotizacion;
	private Date fechaInicioVigencia;
	private Date fechaFinVigencia;
	private String nombreContratante;
	private String calleYNumero;
	private String colonia;
	private String ciudad;
	private String estado;
	private String codigoPostal;
	private String rfcContratante;
	private BigDecimal idToPersonaContratante;
	private Double primaNeta;
	private Double derechos;
	private String observaciones;
	private String titulo;
	private String subTitulo;
	private String identificador;
	
	public String getNumeroCotizacion() {
		return numeroCotizacion;
	}
	public void setNumeroCotizacion(String numeroCotizacion) {
		this.numeroCotizacion = numeroCotizacion;
	}
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}
	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}
	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}
	public String getNombreContratante() {
		return nombreContratante;
	}
	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}
	public String getCalleYNumero() {
		return calleYNumero;
	}
	public void setCalleYNumero(String calleYNumero) {
		this.calleYNumero = calleYNumero;
	}
	public String getColonia() {
		return colonia;
	}
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public String getRfcContratante() {
		return rfcContratante;
	}
	public void setRfcContratante(String rfcContratante) {
		this.rfcContratante = rfcContratante;
	}
	public BigDecimal getIdToPersonaContratante() {
		return idToPersonaContratante;
	}
	public void setIdToPersonaContratante(BigDecimal idToPersonaContratante) {
		this.idToPersonaContratante = idToPersonaContratante;
	}
	public Double getPrimaNeta() {
		return primaNeta;
	}
	public void setPrimaNeta(Double primaNeta) {
		this.primaNeta = primaNeta;
	}
	public Double getDerechos() {
		return derechos;
	}
	public void setDerechos(Double derechos) {
		this.derechos = derechos;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getSubTitulo() {
		return subTitulo;
	}
	public void setSubTitulo(String subTitulo) {
		this.subTitulo = subTitulo;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public String getIdentificador() {
		return identificador;
	}
	
		
}
