package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.domain.compensaciones.CaParametros;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.agente.NegocioAgente;
import mx.com.afirme.midas2.domain.negocio.bonocomision.NegocioBonoComision;
import mx.com.afirme.midas2.domain.negocio.cliente.NegocioCliente;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoEndoso;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacion;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacion;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionDesc;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioEstado;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioMunicipio;

public class DatosNegocioDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long idToNegocio;
	private List<DatosProductosDeNegocioDetalleDTO> datosProductosDeNegocioDetalleDTOs;
	private List<NegocioCliente> negocioClientes; 
	private List<NegocioAgente> negocioAgentes;
	private Negocio negocio;
	private List<NegocioDerechoEndoso> negocioDerechoEndosos;
	private List<NegocioDerechoPoliza> negocioDerechoPolizas;
	private List<NegocioEstado> estados;
	private List<NegocioMunicipio> municipios;
	private List<NegocioRenovacion> negocioRenovacions;
	private List<NegocioRenovacionDesc> renovacionDescuentosList;
	private List<DatosRenovacionDetalleNegocioDTO> datosRenovacionDetalleNegocioDTOs;
	private List<NegocioBonoComision> negocioBonoComisions;
	private String usuarioCreador;
	private List<CaParametros> caParametros;
	private List<ReporteNegCompensacionesDTO> reportePrima_poliza;
	private List<ReporteNegCompensacionesDTO> reporteBs;
	private NegocioRecuotificacion negocioRecuotificacion;
		
	public NegocioRecuotificacion getNegocioRecuotificacion() {
		return negocioRecuotificacion;
	}

	public void setNegocioRecuotificacion(
			NegocioRecuotificacion negocioRecuotificacion) {
		this.negocioRecuotificacion = negocioRecuotificacion;
	}

	/**
	 * @return the renovacionDescuentosList
	 */
	public List<NegocioRenovacionDesc> getRenovacionDescuentosList() {
		return renovacionDescuentosList;
	}

	/**
	 * @param renovacionDescuentosList the renovacionDescuentosList to set
	 */
	public void setRenovacionDescuentosList(
			List<NegocioRenovacionDesc> renovacionDescuentosList) {
		this.renovacionDescuentosList = renovacionDescuentosList;
	}

	/**
	 * @return the negocioRenovacions
	 */
	public List<NegocioRenovacion> getNegocioRenovacions() {
		return negocioRenovacions;
	}

	/**
	 * @param negocioRenovacions the negocioRenovacions to set
	 */
	public void setNegocioRenovacions(List<NegocioRenovacion> negocioRenovacions) {
		this.negocioRenovacions = negocioRenovacions;
	}

	/**
	 * @return the estados
	 */
	public List<NegocioEstado> getEstados() {
		return estados;
	}

	/**
	 * @param estados the estados to set
	 */
	public void setEstados(List<NegocioEstado> estados) {
		this.estados = estados;
	}

	/**
	 * @return the municipios
	 */
	public List<NegocioMunicipio> getMunicipios() {
		return municipios;
	}

	/**
	 * @param municipios the municipios to set
	 */
	public void setMunicipios(List<NegocioMunicipio> municipios) {
		this.municipios = municipios;
	}

	/**
	 * @return the negocioClientes
	 */
	public List<NegocioCliente> getNegocioClientes() {
		return negocioClientes;
	}

	/**
	 * @param negocioClientes the negocioClientes to set
	 */
	public void setNegocioClientes(List<NegocioCliente> negocioClientes) {
		this.negocioClientes = negocioClientes;
	}

	/**
	 * @return the idToNegocio
	 */
	public Long getIdToNegocio() {
		return idToNegocio;
	}

	/**
	 * @param idToNegocio
	 *            the idToNegocio to set
	 */
	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}

	/**
	 * @return the datosProductosDeNegocioDetalleDTOs
	 */
	public List<DatosProductosDeNegocioDetalleDTO> getDatosProductosDeNegocioDetalleDTOs() {
		return datosProductosDeNegocioDetalleDTOs;
	}

	/**
	 * @param datosProductosDeNegocioDetalleDTOs the datosProductosDeNegocioDetalleDTOs to set
	 */
	public void setDatosProductosDeNegocioDetalleDTOs(
			List<DatosProductosDeNegocioDetalleDTO> datosProductosDeNegocioDetalleDTOs) {
		this.datosProductosDeNegocioDetalleDTOs = datosProductosDeNegocioDetalleDTOs;
	}

	/**
	 * @return the negocioAgentes
	 */
	public List<NegocioAgente> getNegocioAgentes() {
		return negocioAgentes;
	}

	/**
	 * @param negocioAgentes the negocioAgentes to set
	 */
	public void setNegocioAgentes(List<NegocioAgente> negocioAgentes) {
		this.negocioAgentes = negocioAgentes;
	}

	/**
	 * @return the negocio
	 */
	public Negocio getNegocio() {
		return negocio;
	}

	/**
	 * @param negocio the negocio to set
	 */
	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	/**
	 * @return the negocioDerechoEndosos
	 */
	public List<NegocioDerechoEndoso> getNegocioDerechoEndosos() {
		return negocioDerechoEndosos;
	}

	/**
	 * @param negocioDerechoEndosos the negocioDerechoEndosos to set
	 */
	public void setNegocioDerechoEndosos(
			List<NegocioDerechoEndoso> negocioDerechoEndosos) {
		this.negocioDerechoEndosos = negocioDerechoEndosos;
	}

	/**
	 * @return the negocioDerechoPolizas
	 */
	public List<NegocioDerechoPoliza> getNegocioDerechoPolizas() {
		return negocioDerechoPolizas;
	}

	/**
	 * @param negocioDerechoPolizas the negocioDerechoPolizas to set
	 */
	public void setNegocioDerechoPolizas(
			List<NegocioDerechoPoliza> negocioDerechoPolizas) {
		this.negocioDerechoPolizas = negocioDerechoPolizas;
	}

	/**
	 * @return the datosRenovacionDetalleNegocioDTOs
	 */
	public List<DatosRenovacionDetalleNegocioDTO> getDatosRenovacionDetalleNegocioDTOs() {
		return datosRenovacionDetalleNegocioDTOs;
	}

	/**
	 * @param datosRenovacionDetalleNegocioDTOs the datosRenovacionDetalleNegocioDTOs to set
	 */
	public void setDatosRenovacionDetalleNegocioDTOs(
			List<DatosRenovacionDetalleNegocioDTO> datosRenovacionDetalleNegocioDTOs) {
		this.datosRenovacionDetalleNegocioDTOs = datosRenovacionDetalleNegocioDTOs;
	}

	/**
	 * @return the negocioBonoComisions
	 */
	public List<NegocioBonoComision> getNegocioBonoComisions() {
		return negocioBonoComisions;
	}

	/**
	 * @param negocioBonoComisions the negocioBonoComisions to set
	 */
	public void setNegocioBonoComisions(
			List<NegocioBonoComision> negocioBonoComisions) {
		this.negocioBonoComisions = negocioBonoComisions;
	}

	/**
	 * @return the usuarioCreador
	 */
	public String getUsuarioCreador() {
		return usuarioCreador;
	}

	/**
	 * @param usuarioCreador the usuarioCreador to set
	 */
	public void setUsuarioCreador(String usuarioCreador) {
		this.usuarioCreador = usuarioCreador;
	}

	public List<CaParametros> getCaParametros() {
		return caParametros;
	}

	public void setCaParametros(List<CaParametros> caParametros) {
		this.caParametros = caParametros;
	}

	public List<ReporteNegCompensacionesDTO> getReportePrima_poliza() {
		return reportePrima_poliza;
	}

	public void setReportePrima_poliza(
			List<ReporteNegCompensacionesDTO> reportePrima_poliza) {
		this.reportePrima_poliza = reportePrima_poliza;
	}

	public List<ReporteNegCompensacionesDTO> getReporteBs() {
		return reporteBs;
	}

	public void setReporteBs(List<ReporteNegCompensacionesDTO> reporteBs) {
		this.reporteBs = reporteBs;
	}
	
}
