package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosGeneralDTO;

public class DatosCaratulaCotizacion extends GeneralesDatosImpresion implements Serializable {
	
	
	private static final long serialVersionUID = 1947414171419903941L;
	
	private DatosCotizacionDTO datosCotizacionDTO;
	private List<DatosLineasDeNegocioDTO> datasourceLineasNegocio;
	private List<DatosDesglosePagosDTO> datasourceDesglosePagos;
	private CotizacionDTO cotizacionDTO;
	private ResumenCostosGeneralDTO resumenCostosGeneralDTO;
	
	public ResumenCostosGeneralDTO getResumenCostosGeneralDTO() {
		return resumenCostosGeneralDTO;
	}

	public void setResumenCostosGeneralDTO(
			ResumenCostosGeneralDTO resumenCostosGeneralDTO) {
		this.resumenCostosGeneralDTO = resumenCostosGeneralDTO;
	}


	public DatosCotizacionDTO getDatosCotizacionDTO() {
		return datosCotizacionDTO;
	}

	public void setDatosCotizacionDTO(DatosCotizacionDTO datosCotizacionDTO) {
		this.datosCotizacionDTO = datosCotizacionDTO;
	}
	
	public List<DatosLineasDeNegocioDTO> getDatasourceLineasNegocio() {
		return datasourceLineasNegocio;
	}

	public void setDatasourceLineasNegocio(
			List<DatosLineasDeNegocioDTO> datasourceLineasNegocio) {
		this.datasourceLineasNegocio = datasourceLineasNegocio;
	}

	public List<DatosDesglosePagosDTO> getDatasourceDesglosePagos() {
		return datasourceDesglosePagos;
	}
	
	public void setDatasourceDesglosePagos(
			List<DatosDesglosePagosDTO> datasourceDesglosePagos) {
		this.datasourceDesglosePagos = datasourceDesglosePagos;
	}

	public CotizacionDTO getCotizacionDTO() {
		return cotizacionDTO;
	}

	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}
	
}
