package mx.com.afirme.midas2.dto.impresiones;

import java.math.BigDecimal;
import java.io.Serializable;


public class ReporteNegCompensacionesDTO implements Serializable {	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String tipoCompensacion;
	private String tipoBeneficiario;
    private BigDecimal porcentajePago;
	private BigDecimal montoPago;
	private BigDecimal valorMinimo;
	private BigDecimal valorMaximo;
	private BigDecimal valorCompensacion;
	
	
 
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}	
	public String getTipoCompensacion() {
		return tipoCompensacion;
	}
	public void setTipoCompensacion(String tipoCompensacion) {
		this.tipoCompensacion = tipoCompensacion;
	}
	public String getTipoBeneficiario() {
		return tipoBeneficiario;
	}
	public void setTipoBeneficiario(String tipoBeneficiario) {
		this.tipoBeneficiario = tipoBeneficiario;
	}
	public BigDecimal getPorcentajePago() {
		return porcentajePago;
	}
	public void setPorcentajePago(BigDecimal porcentajePago) {
		this.porcentajePago = porcentajePago;
	}
	public BigDecimal getMontoPago() {
		return montoPago;
	}
	public void setMontoPago(BigDecimal montoPago) {
		this.montoPago = montoPago;
	}
	public BigDecimal getValorMinimo() {
		return valorMinimo;
	}
	public void setValorMinimo(BigDecimal valorMinimo) {
		this.valorMinimo = valorMinimo;
	}
	public BigDecimal getValorMaximo() {
		return valorMaximo;
	}
	public void setValorMaximo(BigDecimal valorMaximo) {
		this.valorMaximo = valorMaximo;
	}
	public BigDecimal getValorCompensacion() {
		return valorCompensacion;
	}
	public void setValorCompensacion(BigDecimal valorCompensacion) {
		this.valorCompensacion = valorCompensacion;
	}
	
	
	
}
