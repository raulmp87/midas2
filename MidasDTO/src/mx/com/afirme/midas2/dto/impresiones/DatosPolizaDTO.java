package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DatosPolizaDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7407183267906674744L;
	private String numeroPoliza;
	private Date fechaInicioVigencia;
	private Date fechaFinVigencia;
	private String nombreContratante;
	private String txDomicilio;
	private String calleYNumero;
	private String colonia;
	private String ciudad;
	private String estado;
	private String codigoPostal;
	private String rfcContratante;
	private BigDecimal idToPersonaContratante;
	private Double primaNetaCotizacion;
	private Double montoRecargoPagoFraccionado;
	private Double derechosPoliza;
	private Double montoIVA;
	private Double descuento;
	private Double primaNetaTotal;
	private Double totalPrimas;
	private String descripcionMoneda;
	private String descripcionFormaPago;	
	private String observaciones;
	private String titulo;
	private String subTitulo;
	private String identificador;
	private Integer idCentroEmisor;
	private String clavePolizaSeycos;
	
	private String numeroFolio;
	private String codigoPostalCirculacion;
	
	/**
	 * @return the idCentroEmisor
	 */
	public Integer getIdCentroEmisor() {
		return idCentroEmisor;
	}

	/**
	 * @param idCentroEmisor the idCentroEmisor to set
	 */
	public void setIdCentroEmisor(Integer idCentroEmisor) {
		this.idCentroEmisor = idCentroEmisor;
	}

	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}
	
	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	
	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}
	
	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}
	
	public String getNombreContratante() {
		return nombreContratante;
	}
	
	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}
	
	public BigDecimal getIdToPersonaContratante() {
		return idToPersonaContratante;
	}
	
	public void setIdToPersonaContratante(BigDecimal idToPersonaContratante) {
		this.idToPersonaContratante = idToPersonaContratante;
	}
	
	public Double getPrimaNetaCotizacion() {
		return primaNetaCotizacion;
	}
	
	public void setPrimaNetaCotizacion(Double primaNetaCotizacion) {
		this.primaNetaCotizacion = primaNetaCotizacion;
	}
	
	public Double getMontoRecargoPagoFraccionado() {
		return montoRecargoPagoFraccionado;
	}
	
	public void setMontoRecargoPagoFraccionado(Double montoRecargoPagoFraccionado) {
		this.montoRecargoPagoFraccionado = montoRecargoPagoFraccionado;
	}
	
	public Double getDerechosPoliza() {
		return derechosPoliza;
	}
	
	public void setDerechosPoliza(Double derechosPoliza) {
		this.derechosPoliza = derechosPoliza;
	}
	
	public Double getMontoIVA() {
		return montoIVA;
	}
	
	public void setMontoIVA(Double montoIVA) {
		this.montoIVA = montoIVA;
	}
	
	public Double getPrimaNetaTotal() {
		return primaNetaTotal;
	}
	
	public void setPrimaNetaTotal(Double primaNetaTotal) {
		this.primaNetaTotal = primaNetaTotal;
	}

	public String getDescripcionMoneda() {
		return descripcionMoneda;
	}
	
	public void setDescripcionMoneda(String descripcionMoneda) {
		this.descripcionMoneda = descripcionMoneda;
	}
	
	public String getDescripcionFormaPago() {
		return descripcionFormaPago;
	}
	
	public void setDescripcionFormaPago(String descripcionFormaPago) {
		this.descripcionFormaPago = descripcionFormaPago;
	}

	public String getTxDomicilio() {
		return txDomicilio;
	}

	public void setTxDomicilio(String txDomicilio) {
		this.txDomicilio = txDomicilio;
	}

	public String getCalleYNumero() {
		return calleYNumero;
	}

	public void setCalleYNumero(String calleYNumero) {
		this.calleYNumero = calleYNumero;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getRfcContratante() {
		return rfcContratante;
	}

	public void setRfcContratante(String rfcContratante) {
		this.rfcContratante = rfcContratante;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getSubTitulo() {
		return subTitulo;
	}

	public void setSubTitulo(String subTitulo) {
		this.subTitulo = subTitulo;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public String getIdentificador() {
		return identificador;
	}

	public String getFechaInicioVigenciaStr(){
		String fechaInicioVigenciaStr = null;
		SimpleDateFormat sdf =  new SimpleDateFormat("dd/MM/yyyy");
		fechaInicioVigenciaStr =  sdf.format(getFechaInicioVigencia());
		
		return fechaInicioVigenciaStr;
	}
	
	public String getFechaFinVigenciaStr(){
		String fechaFinVigenciaStr = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		fechaFinVigenciaStr = sdf.format(getFechaFinVigencia());
		
		return fechaFinVigenciaStr;
	}
	
	public void setFechaFinVigenciaStr(String ffv){
		
	}
	
	public void setFechaInicioVigenciaStr(String fiv){
		
	}

	/**
	 * @return the totalPrimas
	 */
	public Double getTotalPrimas() {
		return totalPrimas;
	}

	/**
	 * @param totalPrimas the totalPrimas to set
	 */
	public void setTotalPrimas(Double totalPrimas) {
		this.totalPrimas = totalPrimas;
	}

	/**
	 * @return the descuento
	 */
	public Double getDescuento() {
		return descuento;
	}

	/**
	 * @param descuento the descuento to set
	 */
	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}

	public String getClavePolizaSeycos() {
		return clavePolizaSeycos;
	}

	public void setClavePolizaSeycos(String clavePolizaSeycos) {
		this.clavePolizaSeycos = clavePolizaSeycos;
	}

	public String getNumeroFolio() {
		return numeroFolio;
	}

	public void setNumeroFolio(String numeroFolio) {
		this.numeroFolio = numeroFolio;
	}

	public String getCodigoPostalCirculacion() {
		return codigoPostalCirculacion;
	}

	public void setCodigoPostalCirculacion(String codigoPostalCirculacion) {
		this.codigoPostalCirculacion = codigoPostalCirculacion;
	}
	
	
	
}
