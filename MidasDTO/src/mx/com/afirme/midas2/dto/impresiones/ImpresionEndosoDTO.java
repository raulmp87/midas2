package mx.com.afirme.midas2.dto.impresiones;

import java.util.Map;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;

public class ImpresionEndosoDTO {
	
	private byte[] impresionEndoso;
	private EndosoDTO endoso;
	private PolizaDTO poliza;
	private ControlEndosoCot controlEndosoCot;
	private BitemporalCotizacion bitemporalCotizacion;
	private Map<String,Object> parametrosImpresionEndoso;
	private CotizacionContinuity cotizacionContinuity;
	
	
	public byte[] getImpresionEndoso() {
		return impresionEndoso;
	}
	public void setImpresionEndoso(byte[] impresionEndoso) {
		this.impresionEndoso = impresionEndoso;
	}
	public EndosoDTO getEndoso() {
		return endoso;
	}
	public void setEndoso(EndosoDTO endoso) {
		this.endoso = endoso;
	}
	public PolizaDTO getPoliza() {
		return poliza;
	}
	public void setPoliza(PolizaDTO poliza) {
		this.poliza = poliza;
	}
	public ControlEndosoCot getControlEndosoCot() {
		return controlEndosoCot;
	}
	public void setControlEndosoCot(ControlEndosoCot controlEndosoCot) {
		this.controlEndosoCot = controlEndosoCot;
	}
	public BitemporalCotizacion getBitemporalCotizacion() {
		return bitemporalCotizacion;
	}
	public void setBitemporalCotizacion(BitemporalCotizacion bitemporalCotizacion) {
		this.bitemporalCotizacion = bitemporalCotizacion;
	}
	public Map<String, Object> getParametrosImpresionEndoso() {
		return parametrosImpresionEndoso;
	}
	public void setParametrosImpresionEndoso(
			Map<String, Object> parametrosImpresionEndoso) {
		this.parametrosImpresionEndoso = parametrosImpresionEndoso;
	}
	public CotizacionContinuity getCotizacionContinuity() {
		return cotizacionContinuity;
	}
	public void setCotizacionContinuity(CotizacionContinuity cotizacionContinuity) {
		this.cotizacionContinuity = cotizacionContinuity;
	}
	
}