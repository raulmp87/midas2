package mx.com.afirme.midas2.dto.impresiones;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.CotizacionExpress;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.EsquemaPagoCotizacionDTO;

public class ResumenCotExpressDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2855059608902210280L;
	private Paquete paquete;
	private FormaPagoDTO formaPago;
	private CotizacionExpress cotizacionExpress;
	private Map<String,EsquemaPagoCotizacionDTO>esquemaPagoExpress = new LinkedHashMap<String, EsquemaPagoCotizacionDTO>();
	
	public ResumenCotExpressDTO() {
	}
	
	public ResumenCotExpressDTO(Paquete paquete, FormaPagoDTO formaPago, CotizacionExpress cotizacionExpress) {
		this.paquete = paquete;
		this.formaPago = formaPago;
		this.cotizacionExpress = cotizacionExpress;
	}
	
	public void setPaquete(Paquete paquete) {
		this.paquete = paquete;
	}
	
	public void setFormaPago(FormaPagoDTO formaPago) {
		this.formaPago = formaPago;
	}
	
	public Paquete getPaquete() {
		return paquete;
	}

	public FormaPagoDTO getFormaPago() {
		return formaPago;
	}

	public CotizacionExpress getCotizacionExpress() {
		return cotizacionExpress;
	}

	public void setCotizacionExpress(CotizacionExpress cotizacionExpress) {
		this.cotizacionExpress = cotizacionExpress;
	}

	public Map<String, EsquemaPagoCotizacionDTO> getEsquemaPagoExpress() {
		return esquemaPagoExpress;
	}

	public void setEsquemaPagoExpress(
			Map<String, EsquemaPagoCotizacionDTO> esquemaPagoExpress) {
		this.esquemaPagoExpress = esquemaPagoExpress;
	}	
	
}
