package mx.com.afirme.midas2.dto.impresiones;
import java.io.Serializable;
import java.util.List;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;

public class DatosPaquetesNegocioDetalleDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	private String tipoPolizaDesc;
	private String seccionDesc;
	private List<NegocioPaqueteSeccion> paqueteSeccion;
	/**
	 * @return the tipoPolizaDesc
	 */
	public String getTipoPolizaDesc() {
		return tipoPolizaDesc;
	}
	/**
	 * @param tipoPolizaDesc the tipoPolizaDesc to set
	 */
	public void setTipoPolizaDesc(String tipoPolizaDesc) {
		this.tipoPolizaDesc = tipoPolizaDesc;
	}
	/**
	 * @return the seccionDesc
	 */
	public String getSeccionDesc() {
		return seccionDesc;
	}
	/**
	 * @param seccionDesc the seccionDesc to set
	 */
	public void setSeccionDesc(String seccionDesc) {
		this.seccionDesc = seccionDesc;
	}
	/**
	 * @return the paqueteSeccion
	 */
	public List<NegocioPaqueteSeccion> getPaqueteSeccion() {
		return paqueteSeccion;
	}
	/**
	 * @param paqueteSeccion the paqueteSeccion to set
	 */
	public void setPaqueteSeccion(List<NegocioPaqueteSeccion> paqueteSeccion) {
		this.paqueteSeccion = paqueteSeccion;
	}
	
}
