package mx.com.afirme.midas2.dto;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(value=java.lang.annotation.RetentionPolicy.RUNTIME)
@Target(value={ElementType.METHOD, ElementType.FIELD})
public @interface CatalogoAyuda {

	public Class<?>[] vistas() default {};
	
	public String atributoMapeo(); 
	
	public Class<?> nombre();
	
}
