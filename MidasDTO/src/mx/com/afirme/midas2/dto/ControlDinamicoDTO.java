package mx.com.afirme.midas2.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.base.CacheableDTO;

public class ControlDinamicoDTO implements Serializable, Comparable<ControlDinamicoDTO> {

	private static final long serialVersionUID = 142817165909952937L;
	
	public static final int TEXTBOX = 1;
	
	public static final int SELECT = 2;
	
	public static final int CHECKBOX = 3;
	
	public static final int RADIOBUTTON = 4;
	
	public static final int HIDDEN = 5;
	
	
	private Integer secuencia;
	private String atributoMapeo;
	@Deprecated
	private Boolean esComponenteId;
	private String etiqueta;
	private Integer tipoControl;
	private List<CacheableDTO> elementosCombo = new ArrayList<CacheableDTO>(1);
	
	@Deprecated
	private Boolean esNumerico;
	@Deprecated
	private Boolean permiteNulo;
	@Deprecated
	private Boolean esValido;
	@Deprecated
	private String descripcionValidacion;
	
	private Boolean editable;
	private String valor;
	
	
	private String idControlPadre;
	private String idControlHijo;
	private String javaScriptOnChange;
	private String javaScriptOnBlur;
	private String javaScriptOnKeyPress;
	private String javaScriptOnFocus;
	private String longitudMaxima;
	
	
	public String getValorInterpretado(){
		String valorInterpretado = "";
		switch(tipoControl){
			case SELECT:
				if(elementosCombo != null){
					//se dificulta hacer la búsqueda usando los métodos del List la manejar CacheableDTO
					for(CacheableDTO cacheable : elementosCombo){
						if(cacheable.getId().toString().equals(valor)){
							valorInterpretado = cacheable.getDescription();
							break;
						}
					}
				}
			break;
			case TEXTBOX:
			case CHECKBOX:
			case RADIOBUTTON:
			case HIDDEN:
			default:
				valorInterpretado = valor;
			break;
		}
		return valorInterpretado;
	}
	
	/**
	 * Secuencia del orden del control respecto a otros
	 * @return
	 */
	public Integer getSecuencia() {
		return secuencia;
	}




	public void setSecuencia(Integer secuencia) {
		this.secuencia = secuencia;
	}



	/**
	 * Nombre de la propiedad a la que representa el control
	 * @return
	 */
	public String getAtributoMapeo() {
		return atributoMapeo;
	}




	public void setAtributoMapeo(String atributoMapeo) {
		this.atributoMapeo = atributoMapeo;
	}



	/**
	 * Distingue si la propiedad a la que representa el control es parte del id del objeto al que pertenece
	 * @return
	 */
	public Boolean getEsComponenteId() {
		return esComponenteId;
	}




	public void setEsComponenteId(Boolean esComponenteId) {
		this.esComponenteId = esComponenteId;
	}



	/**
	 * Etiqueta que se mostrara en la vista
	 * Debe coincidir con un key del resource bundle
	 * @return
	 */
	public String getEtiqueta() {
		return etiqueta;
	}




	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}



	/**
	 * Tipo de control que representa este objeto
	 * @return
	 */
	public Integer getTipoControl() {
		return tipoControl;
	}




	public void setTipoControl(Integer tipoControl) {
		this.tipoControl = tipoControl;
	}



	/**
	 * Lista de elementos a mostrar en caso de que el control sea de tipo SELECT
	 * @return
	 */
	public List<CacheableDTO> getElementosCombo() {
		return elementosCombo;
	}




	public void setElementosCombo(List<CacheableDTO> elementosCombo) {
		this.elementosCombo = elementosCombo;
	}



	/**
	 * Define si se validara que el contenido del control sea numerico
	 * @return
	 */
	public Boolean getEsNumerico() {
		return esNumerico;
	}




	public void setEsNumerico(Boolean esNumerico) {
		this.esNumerico = esNumerico;
	}



	/**
	 * Define si se validara que el contenido del control no sea nulo
	 * @return
	 */
	public Boolean getPermiteNulo() {
		return permiteNulo;
	}




	public void setPermiteNulo(Boolean permiteNulo) {
		this.permiteNulo = permiteNulo;
	}



	/**
	 * Define si se permitira editar sobre el control
	 * @return
	 */
	public Boolean getEditable() {
		return editable;
	}




	public void setEditable(Boolean editable) {
		this.editable = editable;
	}



	/**
	 * Valor del control
	 * @return
	 */
	public String getValor() {
		return valor;
	}




	public void setValor(String valor) {
		this.valor = valor;
	}



	/**
	 * Define si el control paso la validacion
	 * @return
	 */
	public Boolean getEsValido() {
		return esValido;
	}




	public void setEsValido(Boolean esValido) {
		this.esValido = esValido;
	}



	/**
	 * La descripcion sobre el resultado de la validacion
	 * @return
	 */
	public String getDescripcionValidacion() {
		return descripcionValidacion;
	}




	public void setDescripcionValidacion(String descripcionValidacion) {
		this.descripcionValidacion = descripcionValidacion;
	}



	/**
	 * id que representa al objeto padre de la entidad representada en este control
	 * @return
	 */
	public String getIdControlPadre() {
		return idControlPadre;
	}




	public void setIdControlPadre(String idControlPadre) {
		this.idControlPadre = idControlPadre;
	}



	/**
	 * id que representa al objeto hijo de la entidad representada en este control
	 * @return
	 */
	public String getIdControlHijo() {
		return idControlHijo;
	}




	public void setIdControlHijo(String idControlHijo) {
		this.idControlHijo = idControlHijo;
	}



	/**
	 * codigo javascript a ejecutar en el evento onChange (por el momento solo aplica para controles tipo SELECT)
	 * @return
	 */
	public String getJavaScriptOnChange() {
		return javaScriptOnChange;
	}
	
	public void setJavaScriptOnChange(String javaScriptOnChange) {
		this.javaScriptOnChange = javaScriptOnChange;
	}
	
	/**
	 * codigo javascript a ejecutar en el evento onBlur
	 * @return
	 */
	public String getJavaScriptOnBlur() {
		return javaScriptOnBlur;
	}

	public void setJavaScriptOnBlur(String javaScriptOnBlur) {
		this.javaScriptOnBlur = javaScriptOnBlur;
	}
	
	
	/**
	 * codigo javascript a ejecutar en el evento onKeyPress
	 * @return
	 */
	public String getJavaScriptOnKeyPress() {
		return javaScriptOnKeyPress;
	}

	public void setJavaScriptOnKeyPress(String javaScriptOnKeyPress) {
		this.javaScriptOnKeyPress = javaScriptOnKeyPress;
	}
	
	
	/**
	 * dfine la longitud maxima del contenido del control (para controles tipo TEXTBOX)
	 * @return
	 */
	public String getLongitudMaxima() {
		return longitudMaxima;
	}

	public void setLongitudMaxima(String longitudMaxima) {
		this.longitudMaxima = longitudMaxima;
	}

	public String getJavaScriptOnFocus() {
		return javaScriptOnFocus;
	}

	public void setJavaScriptOnFocus(String javaScriptOnFocus) {
		this.javaScriptOnFocus = javaScriptOnFocus;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((atributoMapeo == null) ? 0 : atributoMapeo.hashCode());
		result = prime * result
				+ ((secuencia == null) ? 0 : secuencia.hashCode());
		return result;
	}




	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ControlDinamicoDTO other = (ControlDinamicoDTO) obj;
		if (atributoMapeo == null) {
			if (other.atributoMapeo != null)
				return false;
		} else if (!atributoMapeo.equals(other.atributoMapeo))
			return false;
		if (secuencia == null) {
			if (other.secuencia != null)
				return false;
		} else if (!secuencia.equals(other.secuencia))
			return false;
		return true;
	}




	@Override
	public int compareTo(ControlDinamicoDTO o) {
		return this.secuencia.compareTo(o.secuencia);
	}
	
	
}
