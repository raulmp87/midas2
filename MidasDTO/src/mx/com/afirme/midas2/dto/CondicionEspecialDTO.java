package mx.com.afirme.midas2.dto;

import java.util.Date;

import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;

import org.springframework.stereotype.Component;

/**
 * 
 * @author Lizeth De La Garza
 *
 */


@Component
public class CondicionEspecialDTO {


	private CondicionEspecial condicion;
	
	private Date fechaIniRegistro;
	
	private Date fechaFinRegistro;
	
	private Date fechaIniAlta;
	
	private Date fechaFinAlta;
	
	private Date fechaIniBaja;
	
	private Date fechaFinBaja;	
	
	private Integer asignadoReporte;
	
	private Boolean claveContrato;

	
	public CondicionEspecial getCondicion() {
		return condicion;
	}

	public void setCondicion(CondicionEspecial condicion) {
		this.condicion = condicion;
	}

	public Date getFechaIniRegistro() {
		return fechaIniRegistro;
	}

	public void setFechaIniRegistro(Date fechaIniRegistro) {
		this.fechaIniRegistro = fechaIniRegistro;
	}

	public Date getFechaFinRegistro() {
		return fechaFinRegistro;
	}

	public void setFechaFinRegistro(Date fechaFinRegistro) {
		this.fechaFinRegistro = fechaFinRegistro;
	}

	public Date getFechaIniAlta() {
		return fechaIniAlta;
	}

	public void setFechaIniAlta(Date fechaIniAlta) {
		this.fechaIniAlta = fechaIniAlta;
	}

	public Date getFechaFinAlta() {
		return fechaFinAlta;
	}

	public void setFechaFinAlta(Date fechaFinAlta) {
		this.fechaFinAlta = fechaFinAlta;
	}

	public Date getFechaIniBaja() {
		return fechaIniBaja;
	}

	public void setFechaIniBaja(Date fechaIniBaja) {
		this.fechaIniBaja = fechaIniBaja;
	}

	public Date getFechaFinBaja() {
		return fechaFinBaja;
	}

	public void setFechaFinBaja(Date fechaFinBaja) {
		this.fechaFinBaja = fechaFinBaja;
	}

	/**
	 * @param asignadoReporte the asignadoReporte to set
	 */
	public void setAsignadoReporte(Integer asignadoReporte) {
		this.asignadoReporte = asignadoReporte;
	}

	/**
	 * @return the asignadoReporte
	 */
	public Integer getAsignadoReporte() {
		return asignadoReporte;
	}

	public Boolean isClaveContrato() {
		return claveContrato;
	}

	public void setClaveContrato(Boolean claveContrato) {
		this.claveContrato = claveContrato;
	}
	
}
