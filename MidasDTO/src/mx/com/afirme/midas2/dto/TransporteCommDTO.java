package mx.com.afirme.midas2.dto;

import java.io.Serializable;
import java.util.Map;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.domain.sistema.comm.CommBitacora.STATUS_BITACORA;
import mx.com.afirme.midas2.domain.sistema.comm.CommBitacoraDetalle.STATUS_COMM;
import mx.com.afirme.midas2.domain.sistema.comm.CommProceso.TIPO_COMM;
import mx.com.afirme.midas2.domain.sistema.comm.CommProceso.TIPO_PROCESO;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService;

public class TransporteCommDTO<T>{
	
	private TIPO_PROCESO tipoProceso;
	
	private T objetoASerializar;
	
	private String objetoSerializado;
	
	private Usuario usuario;
	
	private TIPO_COMM tipoComm;
	
	private String folio;
	
	private STATUS_COMM statusComm;
	
	private STATUS_BITACORA statusTramite;
	
	private String statusDescripcion;
	
	private EnvioNotificacionesService.EnumCodigo codigoNotificacion;
	
	private Map<String, Serializable> datosNotificacion;

	public TIPO_PROCESO getTipoProceso() {
		return tipoProceso;
	}

	public void setTipoProceso(TIPO_PROCESO tipoProceso) {
		this.tipoProceso = tipoProceso;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public TIPO_COMM getTipoComm() {
		return tipoComm;
	}

	public void setTipoComm(TIPO_COMM tipoComm) {
		this.tipoComm = tipoComm;
	}

	public T getObjetoASerializar() {
		return objetoASerializar;
	}

	public void setObjetoASerializar(T objetoASerializar) {
		this.objetoASerializar = objetoASerializar;
	}

	public String getObjetoSerializado() {
		return objetoSerializado;
	}

	public void setObjetoSerializado(String objetoSerializado) {
		this.objetoSerializado = objetoSerializado;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public STATUS_COMM getStatusComm() {
		return statusComm;
	}

	public void setStatusComm(STATUS_COMM statusComm) {
		this.statusComm = statusComm;
	}

	public STATUS_BITACORA getStatusTramite() {
		return statusTramite;
	}

	public void setStatusTramite(STATUS_BITACORA statusTramite) {
		this.statusTramite = statusTramite;
	}

	public String getStatusDescripcion() {
		return statusDescripcion;
	}

	public void setStatusDescripcion(String statusDescripcion) {
		this.statusDescripcion = statusDescripcion;
	}

	public EnvioNotificacionesService.EnumCodigo getCodigoNotificacion() {
		return codigoNotificacion;
	}

	public void setCodigoNotificacion(
			EnvioNotificacionesService.EnumCodigo codigoNotificacion) {
		this.codigoNotificacion = codigoNotificacion;
	}

	public Map<String, Serializable> getDatosNotificacion() {
		return datosNotificacion;
	}

	public void setDatosNotificacion(Map<String, Serializable> datosNotificacion) {
		this.datosNotificacion = datosNotificacion;
	}
	
}
