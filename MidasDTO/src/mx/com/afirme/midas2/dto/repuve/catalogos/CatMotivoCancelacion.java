package mx.com.afirme.midas2.dto.repuve.catalogos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/******************************************************************************
 * 	Entidad para el manejo de los datos de Bitacoras dentro de los Procesos
 *  de envio del REPUVE.
 * 
 * 		Table:		REPCATMOTCANCPOLIZA
 * 		Schema:		MIDAS
 * 		Sequence:	REPCATMOTCANCPOLIZA_SEQ
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 * 		
 ******************************************************************************/
@Entity
@Table(name="REPCATMOTCANCPOLIZA", schema = "MIDAS")
public class CatMotivoCancelacion implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="REPCATMOTCANCPOLIZA_SEQ")
	@SequenceGenerator(name="REPCATMOTCANCPOLIZA_SEQ", schema = "MIDAS", sequenceName="REPCATMOTCANCPOLIZA_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;

	@Column(name="DESCRIPCION")
	private String descripcion;

	@Column(name="ACTIVO")
	private long estatus;
	
	/** Getters and Setters **/

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public long getEstatus() {
		return estatus;
	}

	public void setEstatus(long estatus) {
		this.estatus = estatus;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}