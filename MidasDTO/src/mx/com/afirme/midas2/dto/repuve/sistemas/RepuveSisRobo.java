package mx.com.afirme.midas2.dto.repuve.sistemas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatUbicacionMunicipio;
import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisBitacoras;

/******************************************************************************
 * 	Entidad para el manejo de los datos de Bitacoras dentro de los Procesos
 *  de envio del REPUVE.
 * 
 * 		Table:		REPSISROBO
 * 		Schema:		MIDAS
 * 		Sequence:	REPSISROBO_SEQ
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 * 		
 ******************************************************************************/
@Entity
@Table(name="REPSISROBO", schema = "MIDAS")
public class RepuveSisRobo implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="REPSISROBO_SEQ")
	@SequenceGenerator(name="REPSISROBO_SEQ", schema = "MIDAS", sequenceName="REPSISROBO_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;

	@OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(updatable=false,insertable=false, name="SAPAMISBITACORAS_ID", referencedColumnName="IDSAPAMISBITACORAS")
	private SapAmisBitacoras sapAmisBitacoras;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="REPNIVNCI_ID")
	private RepuveNivNci repuveNivNci;

	@Column(name="NOPOLIZA")
	private String numeroPoliza;

	@Column(name="NUMEROSINIESTRO")
	private String numeroSiniestro;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHAROBO")
	private Date fechaRobo;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SAPAMISCATUBICMUNICIPIO_ID")
	private CatUbicacionMunicipio catUbicacionMunicipio;

	@Column(name="NUMEROAVERIGUACIONPREVIA")
	private String numeroAveriguacionPrevia;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHAAVERIGUACIONPREVIA")
	private Date fechaAveriguacionPrevia;
	
	/** Getters and Setters **/

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RepuveNivNci getRepuveNivNci() {
		return repuveNivNci;
	}

	public void setRepuveNivNci(RepuveNivNci repuveNivNci) {
		this.repuveNivNci = repuveNivNci;
	}

	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}

	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}

	public Date getFechaRobo() {
		return fechaRobo;
	}

	public void setFechaRobo(Date fechaRobo) {
		this.fechaRobo = fechaRobo;
	}

	public CatUbicacionMunicipio getCatUbicacionMunicipio() {
		return catUbicacionMunicipio;
	}

	public void setCatUbicacionMunicipio(
			CatUbicacionMunicipio catUbicacionMunicipio) {
		this.catUbicacionMunicipio = catUbicacionMunicipio;
	}

	public String getNumeroAveriguacionPrevia() {
		return numeroAveriguacionPrevia;
	}

	public void setNumeroAveriguacionPrevia(String numeroAveriguacionPrevia) {
		this.numeroAveriguacionPrevia = numeroAveriguacionPrevia;
	}

	public Date getFechaAveriguacionPrevia() {
		return fechaAveriguacionPrevia;
	}

	public void setFechaAveriguacionPrevia(Date fechaAveriguacionPrevia) {
		this.fechaAveriguacionPrevia = fechaAveriguacionPrevia;
	}

	public SapAmisBitacoras getSapAmisBitacoras() {
		return sapAmisBitacoras;
	}

	public void setSapAmisBitacoras(SapAmisBitacoras sapAmisBitacoras) {
		this.sapAmisBitacoras = sapAmisBitacoras;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}