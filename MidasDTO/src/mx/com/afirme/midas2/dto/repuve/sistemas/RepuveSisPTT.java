package mx.com.afirme.midas2.dto.repuve.sistemas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatUbicacionMunicipio;
import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisBitacoras;
import mx.com.afirme.midas2.dto.repuve.catalogos.CatMotivoPTT;

/******************************************************************************
 * 	Entidad para el manejo de los datos de Bitacoras dentro de los Procesos
 *  de envio del REPUVE.
 * 
 * 		Table:		REPSISPTT
 * 		Schema:		MIDAS
 * 		Sequence:	REPSISPTT_SEQ
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 * 		
 ******************************************************************************/
@Entity
@Table(name="REPSISPTT", schema="MIDAS")
public class RepuveSisPTT implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="REPSISPTT_SEQ")
	@SequenceGenerator(name="REPSISPTT_SEQ", schema="MIDAS", sequenceName="REPSISPTT_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;

	@OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(updatable=false,insertable=false, name="SAPAMISBITACORAS_ID", referencedColumnName="IDSAPAMISBITACORAS")
	private SapAmisBitacoras sapAmisBitacoras;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="REPNIVNCI_ID")
	private RepuveNivNci repuveNivNci;

	@Column(name="NOPOLIZA")
	private String numeroPoliza;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SAPAMISCATUBICMUNICIPIO_ID")
	private CatUbicacionMunicipio catUbicacionMunicipio;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHAPTT")
	private Date fechaPTT;

	@Column(name="NUMEROAVERIGUACIONPREVIA")
	private String numeroAveriguacionPrevia;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHAAVERIGUACIONPREVIA")
	private Date fechaAveriguacionPrevia;

	@Column(name="PORCENTAJEPERDIDA")
	private long porcentajePerdida;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="REPCATMOTPTT_ID")
	private CatMotivoPTT catMotivoPTT;

	@Column(name="NUMEROSINIESTRO")
	private String numeroSiniestro;
	
	/** Getters and Setters **/

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RepuveNivNci getRepuveNivNci() {
		return repuveNivNci;
	}

	public void setRepuveNivNci(RepuveNivNci repuveNivNci) {
		this.repuveNivNci = repuveNivNci;
	}

	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	public CatUbicacionMunicipio getCatUbicacionMunicipio() {
		return catUbicacionMunicipio;
	}

	public void setCatUbicacionMunicipio(
			CatUbicacionMunicipio catUbicacionMunicipio) {
		this.catUbicacionMunicipio = catUbicacionMunicipio;
	}

	public Date getFechaPTT() {
		return fechaPTT;
	}

	public void setFechaPTT(Date fechaPTT) {
		this.fechaPTT = fechaPTT;
	}

	public String getNumeroAveriguacionPrevia() {
		return numeroAveriguacionPrevia;
	}

	public void setNumeroAveriguacionPrevia(String numeroAveriguacionPrevia) {
		this.numeroAveriguacionPrevia = numeroAveriguacionPrevia;
	}

	public Date getFechaAveriguacionPrevia() {
		return fechaAveriguacionPrevia;
	}

	public void setFechaAveriguacionPrevia(Date fechaAveriguacionPrevia) {
		this.fechaAveriguacionPrevia = fechaAveriguacionPrevia;
	}

	public long getPorcentajePerdida() {
		return porcentajePerdida;
	}

	public void setPorcentajePerdida(long porcentajePerdida) {
		this.porcentajePerdida = porcentajePerdida;
	}

	public CatMotivoPTT getCatMotivoPTT() {
		return catMotivoPTT;
	}

	public void setCatMotivoPTT(CatMotivoPTT catMotivoPTT) {
		this.catMotivoPTT = catMotivoPTT;
	}

	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}

	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}

	public SapAmisBitacoras getSapAmisBitacoras() {
		return sapAmisBitacoras;
	}

	public void setSapAmisBitacoras(SapAmisBitacoras sapAmisBitacoras) {
		this.sapAmisBitacoras = sapAmisBitacoras;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}