package mx.com.afirme.midas2.dto.repuve.sistemas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/******************************************************************************
 * 	Entidad para el manejo de los datos de Bitacoras dentro de los Procesos
 *  de envio del REPUVE.
 * 
 * 		Table:		REPNIVNCI
 * 		Schema:		MIDAS
 * 		Sequence:	REPNIVNCI_SEQ
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 * 		
 ******************************************************************************/
@Entity
@Table(name="REPNIVNCI", schema = "MIDAS")
public class RepuveNivNci implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="REPNIVNCI_SEQ")
	@SequenceGenerator(name="REPNIVNCI_SEQ", schema = "MIDAS", sequenceName="REPNIVNCI_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;

	@Column(name="NIV")
	private String niv;

	@Column(name="NCI")
	private String nci;
	
	/** Getters and Setters **/

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNiv() {
		return niv;
	}

	public void setNiv(String niv) {
		this.niv = niv;
	}

	public String getNci() {
		return nci;
	}

	public void setNci(String nci) {
		this.nci = nci;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}