package mx.com.afirme.midas2.dto.repuve.sistemas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatUbicacionMunicipio;
import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisBitacoras;

/******************************************************************************
 * 	Entidad para el manejo de los datos de Bitacoras dentro de los Procesos
 *  de envio del REPUVE.
 * 
 * 		Table:		REPSISRECUPERACION
 * 		Schema:		MIDAS
 * 		Sequence:	REPSISRECUPERACION_SEQ
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 * 		
 ******************************************************************************/
@Entity
@Table(name="REPSISRECUPERACION", schema = "MIDAS")
public class RepuveSisRecuperacion implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="REPSISRECUPERACION_SEQ")
	@SequenceGenerator(name="REPSISRECUPERACION_SEQ", schema = "MIDAS", sequenceName="REPSISRECUPERACION_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;

	@OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(updatable=false,insertable=false, name="SAPAMISBITACORAS_ID", referencedColumnName="IDSAPAMISBITACORAS")
	private SapAmisBitacoras sapAmisBitacoras;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="REPSISROBO_ID")
	private RepuveSisRobo repuveSisRobo;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SAPAMISCATUBICMUNICIPIO_ID")
	private CatUbicacionMunicipio catUbicacionMunicipio;

    @Temporal(TemporalType.DATE)
	@Column(name="FECHARECUPERACION")
	private Date fechaRecuperacion;

	@Column(name="NUMEROAVERIGUACIONPREVIA")
	private String numeroAveriguacionPrevia;

    @Temporal(TemporalType.DATE)
	@Column(name="FECHAAVERIGUACIONPREVIA")
	private Date fechaAveriguacionPrevia;
	
	
	/** Getters and Setters **/

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RepuveSisRobo getRepuveSisRobo() {
		return repuveSisRobo;
	}

	public void setRepuveSisRobo(RepuveSisRobo repuveSisRobo) {
		this.repuveSisRobo = repuveSisRobo;
	}

	public CatUbicacionMunicipio getCatUbicacionMunicipio() {
		return catUbicacionMunicipio;
	}

	public void setCatUbicacionMunicipio(
			CatUbicacionMunicipio catUbicacionMunicipio) {
		this.catUbicacionMunicipio = catUbicacionMunicipio;
	}

	public Date getFechaRecuperacion() {
		return fechaRecuperacion;
	}

	public void setFechaRecuperacion(Date fechaRecuperacion) {
		this.fechaRecuperacion = fechaRecuperacion;
	}

	public SapAmisBitacoras getSapAmisBitacoras() {
		return sapAmisBitacoras;
	}

	public void setSapAmisBitacoras(SapAmisBitacoras sapAmisBitacoras) {
		this.sapAmisBitacoras = sapAmisBitacoras;
	}
    public String getNumeroAveriguacionPrevia() {
		return numeroAveriguacionPrevia;
	}

	public void setNumeroAveriguacionPrevia(String numeroAveriguacionPrevia) {
		this.numeroAveriguacionPrevia = numeroAveriguacionPrevia;
	}

	public Date getFechaAveriguacionPrevia() {
		return fechaAveriguacionPrevia;
	}

	public void setFechaAveriguacionPrevia(Date fechaAveriguacionPrevia) {
		this.fechaAveriguacionPrevia = fechaAveriguacionPrevia;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}