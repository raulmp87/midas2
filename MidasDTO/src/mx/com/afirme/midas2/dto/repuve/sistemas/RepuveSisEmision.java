package mx.com.afirme.midas2.dto.repuve.sistemas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatUbicacionMunicipio;
import mx.com.afirme.midas2.dto.repuve.catalogos.CatMotivoCancelacion;
import mx.com.afirme.midas2.dto.repuve.catalogos.CatTerminosCobertura;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatTipoPersona;
import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisBitacoras;

/******************************************************************************
 * 	Entidad para el manejo de los datos de Bitacoras dentro de los Procesos
 *  de envio del REPUVE.
 * 
 * 		Table:		REPSISEMISIONES
 * 		Schema:		MIDAS
 * 		Sequence:	REPSISEMISIONES_SEQ
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 * 		
 ******************************************************************************/
@Entity
@Table(name="REPSISEMISIONES", schema = "MIDAS")
public class RepuveSisEmision implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="REPSISEMISIONES_SEQ")
	@SequenceGenerator(name="REPSISEMISIONES_SEQ", schema = "MIDAS", sequenceName="REPSISEMISIONES_SEQ",allocationSize=1)
	@Column(name="ID")
	private Long id;

	@OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(updatable=false,insertable=false, name="SAPAMISBITACORAS_ID", referencedColumnName="IDSAPAMISBITACORAS")
	private SapAmisBitacoras sapAmisBitacoras;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="REPNIVNCI_ID")
	private RepuveNivNci repuveNivNci;

    @Temporal(TemporalType.DATE)
	@Column(name="FECHAPAGO")
	private Date fechaPago;

	@Column(name="NOPOLIZA")
	private String numeroPoliza;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="REPCATTERMINOSCOBERTURA_ID")
	private CatTerminosCobertura catTerminosCobertura;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SAPAMISCATUBICMUNICIPIO_ID")
	private CatUbicacionMunicipio catUbicacionMunicipio;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SAPAMISCATTIPOPERSONA_ID")
	private CatTipoPersona catTipoPersona;

	@Column(name="TITULAR")
	private String titular;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHAVIGENCIAINICIO")
	private Date fechaVigenciaInicio;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHAVIGENCIAFIN")
	private Date fechaVigenciaFin;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHACANCELACION")
	private Date fechaCancelacion;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="REPCATMOTCANCPOLIZA_ID")
	private CatMotivoCancelacion catMotivoCancelacion;
	
	/** Getters and Setters **/

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RepuveNivNci getRepuveNivNci() {
		return repuveNivNci;
	}

	public void setRepuveNivNci(RepuveNivNci repuveNivNci) {
		this.repuveNivNci = repuveNivNci;
	}

	public Date getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	public CatTerminosCobertura getCatTerminosCobertura() {
		return catTerminosCobertura;
	}

	public void setCatTerminosCobertura(CatTerminosCobertura catTerminosCobertura) {
		this.catTerminosCobertura = catTerminosCobertura;
	}

	public CatUbicacionMunicipio getCatUbicacionMunicipio() {
		return catUbicacionMunicipio;
	}

	public void setCatUbicacionMunicipio(CatUbicacionMunicipio catUbicacionMunicipio) {
		this.catUbicacionMunicipio = catUbicacionMunicipio;
	}

	public CatTipoPersona getCatTipoPersona() {
		return catTipoPersona;
	}

	public void setCatTipoPersona(CatTipoPersona catTipoPersona) {
		this.catTipoPersona = catTipoPersona;
	}

	public String getTitular() {
		return titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public Date getFechaVigenciaInicio() {
		return fechaVigenciaInicio;
	}

	public void setFechaVigenciaInicio(Date fechaVigenciaInicio) {
		this.fechaVigenciaInicio = fechaVigenciaInicio;
	}

	public Date getFechaVigenciaFin() {
		return fechaVigenciaFin;
	}

	public void setFechaVigenciaFin(Date fechaVigenciaFin) {
		this.fechaVigenciaFin = fechaVigenciaFin;
	}

	public Date getFechaCancelacion() {
		return fechaCancelacion;
	}

	public void setFechaCancelacion(Date fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}

	public CatMotivoCancelacion getCatMotivoCancelacion() {
		return catMotivoCancelacion;
	}

	public void setCatMotivoCancelacion(CatMotivoCancelacion catMotivoCancelacion) {
		this.catMotivoCancelacion = catMotivoCancelacion;
	}

	public SapAmisBitacoras getSapAmisBitacoras() {
		return sapAmisBitacoras;
	}

	public void setSapAmisBitacoras(SapAmisBitacoras sapAmisBitacoras) {
		this.sapAmisBitacoras = sapAmisBitacoras;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}