package mx.com.afirme.midas2.dto.impuestosestatales;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import oracle.sql.TIMESTAMP;

import org.apache.commons.lang.StringUtils;

public class RetencionImpuestosDTO implements Serializable {
	private static final long serialVersionUID = 5957195062222058307L;
	
	private Long id;
	private EstadoDTO estado;
	private String estadoId;
	private Long porcentaje;
	private String concepto;
	private ValorCatalogoAgentes tipoComision;
	private Long tipoComisionId;
	private ValorCatalogoAgentes personalidadJuridica;
	private Long personalidadJuridicaId;
	private Date fechaInicioVig;
	private Date fechaFinVig;
	private String estatus;
	private String usuario;
	private TIMESTAMP fechaModificacionTimestamp;
	private String fechaModificacionString;
	
	public RetencionImpuestosDTO() {}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public TIMESTAMP getFechaModificacionTimestamp() {
		return fechaModificacionTimestamp;
	}
	public void setFechaModificacionTimestamp(TIMESTAMP fechaModificacionTimestamp) {
		this.fechaModificacionTimestamp = fechaModificacionTimestamp;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public EstadoDTO getEstado() {
		return estado;
	}
	public void setEstado(EstadoDTO estado) {
		this.estado = estado;
	}
	public Long getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(Long porcentaje) {
		this.porcentaje = porcentaje;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public ValorCatalogoAgentes getTipoComision() {
		return tipoComision;
	}
	public void setTipoComision(ValorCatalogoAgentes tipoComision) {
		this.tipoComision = tipoComision;
	}
	public ValorCatalogoAgentes getPersonalidadJuridica() {
		return personalidadJuridica;
	}
	public void setPersonalidadJuridica(ValorCatalogoAgentes personalidadJuridica) {
		this.personalidadJuridica = personalidadJuridica;
	}
	public Date getFechaInicioVig() {
		return fechaInicioVig;
	}
	public void setFechaInicioVig(Date fechaInicioVig) {
		this.fechaInicioVig = fechaInicioVig;
	}
	public Date getFechaFinVig() {
		return fechaFinVig;
	}
	public void setFechaFinVig(Date fechaFinVig) {
		this.fechaFinVig = fechaFinVig;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getFechaModificacionString() {
		return fechaModificacionString;
	}
	public void setFechaModificacionString(String fechaModificacionString) {
		this.fechaModificacionString = fechaModificacionString;
	}
	public Long getTipoComisionId() {
		return tipoComisionId;
	}
	public void setTipoComisionId(Long tipoComisionId) {
		this.tipoComisionId = tipoComisionId;
	}
	public Long getPersonalidadJuridicaId() {
		return personalidadJuridicaId;
	}
	public void setPersonalidadJuridicaId(Long personalidadJuridicaId) {
		this.personalidadJuridicaId = personalidadJuridicaId;
	}
	public String getEstadoId() {
		return estadoId;
	}
	public void setEstadoId(String estadoId) {
		this.estadoId = estadoId;
	}
	
	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		SimpleDateFormat format = new SimpleDateFormat("dd/mm/yyyy");
		result.append("Estatus: " + (StringUtils.isNotEmpty(this.getEstatus()) ? this.getEstatus() : " "));
		result.append(", ID: " + (this.getId() != null ? this.getId() : " "));
		result.append(", Estado: " + (this.getEstado() != null ? this.getEstado() : " "));
		result.append(", Porcentaje: " + (this.getPorcentaje() != null ? this.getPorcentaje() : " "));
		result.append(", Concepto" + (StringUtils.isNotEmpty(this.getConcepto()) ? this.getConcepto() : " "));
		result.append(", Tipo Comision: " + (this.getTipoComision() != null && this.getTipoComision().getValor() != null ? this.getTipoComision().getValor() : " "));
		result.append(", Tipo Persona Juridica: " + (this.getPersonalidadJuridica() != null && this.getPersonalidadJuridica().getValor() != null ? this.getPersonalidadJuridica().getValor() : " "));
		result.append(", Fecha Inicio Vigencia: " + (this.getFechaInicioVig() != null ? format.format(this.getFechaInicioVig()) : " "));
		result.append(", Fecha Fin Vigencia: " + (this.getFechaFinVig() != null ? format.format(this.getFechaFinVig()) : " "));
		result.append(", Usuario Autor: " + (StringUtils.isNotEmpty(this.getUsuario()) ? this.getUsuario() : " "));
		result.append(", Fecha Modificacion: " + (this.getFechaModificacionTimestamp()) != null ? this.getFechaModificacionTimestamp().stringValue() : " ");
		return result.toString();
	}

}
