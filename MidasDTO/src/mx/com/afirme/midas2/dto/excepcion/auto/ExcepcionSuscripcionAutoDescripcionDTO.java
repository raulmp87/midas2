package mx.com.afirme.midas2.dto.excepcion.auto;

import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionNegocioDescripcionDTO;

public class ExcepcionSuscripcionAutoDescripcionDTO extends ExcepcionSuscripcionNegocioDescripcionDTO{
	
	private static final long serialVersionUID = -6361804439291448535L;

	private String rangoModelos;
	
	private String amisDescripcionVehiculo;
	
	public String getRangoModelos() {
		return rangoModelos;
	}

	public void setRangoModelos(String rangoModelos) {
		this.rangoModelos = rangoModelos;
	}

	public String getAmisDescripcionVehiculo() {
		return amisDescripcionVehiculo;
	}

	public void setAmisDescripcionVehiculo(String amisDescripcionVehiculo) {
		this.amisDescripcionVehiculo = amisDescripcionVehiculo;
	}
	
}
