package mx.com.afirme.midas2.dto.excepcion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class ExcepcionSuscripcionReporteDTO implements Serializable {
	
	
	private static final long serialVersionUID = -6084853594918725475L;
	
	private BigDecimal idToCotizacion = null;
	
	private BigDecimal idLineaNegocio = null;
	
	private String lineaNegocio = null;
	
	private BigDecimal idInciso = null;
	
	private Long sequenciaInciso = null;
	
	private BigDecimal idCobertura = null;
	
	private String cobertura = null;
	
	private Long idExcepcion = null;
	
	private Long numExcepcion = null;
	
	private String descripcionExcepcion = null;
	
	private String descripcionRegistroConExcepcion = null;
	
	private String descripcionSeccion = null;
	
	private String descripcionCobertura = null;
	
	private String descripcionInciso = null;

	private String observacion = null;
	
	public String getDescripcionRegistroConExcepcion() {
		return descripcionRegistroConExcepcion;
	}

	public void setDescripcionRegistroConExcepcion(
			String descripcionRegistroConExcepcion) {
		this.descripcionRegistroConExcepcion = descripcionRegistroConExcepcion;
	}

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public BigDecimal getIdLineaNegocio() {
		return idLineaNegocio;
	}

	public void setIdLineaNegocio(BigDecimal idLineaNegocio) {
		this.idLineaNegocio = idLineaNegocio;
	}

	public BigDecimal getIdInciso() {
		return idInciso;
	}

	public void setIdInciso(BigDecimal idInciso) {
		this.idInciso = idInciso;
	}

	public BigDecimal getIdCobertura() {
		return idCobertura;
	}

	public void setIdCobertura(BigDecimal idCobertura) {
		this.idCobertura = idCobertura;
	}

	public String getLineaNegocio() {
		return lineaNegocio;
	}

	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}

	public Long getSequenciaInciso() {
		return sequenciaInciso;
	}

	public void setSequenciaInciso(Long sequenciaInciso) {
		this.sequenciaInciso = sequenciaInciso;
	}

	public String getCobertura() {
		return cobertura;
	}

	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}

	public Long getIdExcepcion() {
		return idExcepcion;
	}

	public void setIdExcepcion(Long idExcepcion) {
		this.idExcepcion = idExcepcion;
	}

	public String getDescripcionExcepcion() {
		return descripcionExcepcion;
	}
	
	public String getDescripcionExcepcionPlana() {
		String descripcionExcepcionPlana = descripcionExcepcion.replaceAll("<ul>", "");
		descripcionExcepcionPlana = descripcionExcepcionPlana.replaceAll("</ul>", "");
		descripcionExcepcionPlana = descripcionExcepcionPlana.replaceAll("<li>", "");
		descripcionExcepcionPlana = descripcionExcepcionPlana.replaceAll("</li>", "");
		
		return descripcionExcepcionPlana;
	}

	public void setDescripcionExcepcion(String descripcionExcepcion) {
		this.descripcionExcepcion = descripcionExcepcion;
	}

	public void setDescripcionSeccion(String descripcionSeccion) {
		this.descripcionSeccion = descripcionSeccion;
	}

	public String getDescripcionSeccion() {
		return descripcionSeccion;
	}

	public void setDescripcionCobertura(String descripcionCobertura) {
		this.descripcionCobertura = descripcionCobertura;
	}

	public String getDescripcionCobertura() {
		return descripcionCobertura;
	}

	public void setDescripcionInciso(String descripcionInciso) {
		this.descripcionInciso = descripcionInciso;
	}

	public String getDescripcionInciso() {
		return descripcionInciso;
	}

	public Long getNumExcepcion() {
		return numExcepcion;
	}

	public void setNumExcepcion(Long numExcepcion) {
		this.numExcepcion = numExcepcion;
	}
	
	public List<String> getListaProvocadorExcepcion() {		
		if(descripcionRegistroConExcepcion == null){
			return new ArrayList<String>();	
		}else{
			return Arrays.asList(descripcionRegistroConExcepcion.split(","));
		}
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	
}
