package mx.com.afirme.midas2.dto.excepcion.auto;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionCotizacionEvaluacionDTO;

public class ExcepcionCotizacionEvaluacionAutoDTO extends ExcepcionCotizacionEvaluacionDTO {

	private static final long serialVersionUID = -1447309570990241362L;

	private Long paquete;

	private String estado;

	private String municipio;

	private String amis;

	private String descripcionEstilo;

	private Integer modelo;
	
	private String numSerie;
	
	private Boolean igualacionPrimasInciso;
	
	private Double descuentoIgualacionInciso;
	
	private Double pctDescuentoEstado;
	
	private List<CondicionEspecial> condicionesCotizacion = new ArrayList<CondicionEspecial>();
	
	private List<CondicionEspecial> condicionesInciso = new ArrayList<CondicionEspecial>();
	
	private Boolean aplicaValidacionPersonaMoral;

	public Double getDescuentoIgualacionInciso() {
		return descuentoIgualacionInciso;
	}

	public void setDescuentoIgualacionInciso(Double descuentoIgualacionInciso) {
		this.descuentoIgualacionInciso = descuentoIgualacionInciso;
	}

	public String getNumSerie() {
		return numSerie;
	}

	public void setNumSerie(String numSerie) {
		this.numSerie = numSerie;
	}

	public Long getPaquete() {
		return paquete;
	}

	public void setPaquete(Long paquete) {
		this.paquete = paquete;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getAmis() {
		return amis;
	}

	public void setAmis(String amis) {
		this.amis = amis;
	}

	public String getDescripcionEstilo() {
		return descripcionEstilo;
	}

	public void setDescripcionEstilo(String descripcionEstilo) {
		this.descripcionEstilo = descripcionEstilo;
	}

	public Integer getModelo() {
		return modelo;
	}

	public void setModelo(Integer modelo) {
		this.modelo = modelo;
	}
	
	public Boolean getIgualacionPrimasInciso() {
		return igualacionPrimasInciso;
	}

	public void setIgualacionPrimasInciso(Boolean igualacionPrimasInciso) {
		this.igualacionPrimasInciso = igualacionPrimasInciso;
	}

	public Double getPctDescuentoEstado() {
		return pctDescuentoEstado;
	}

	public void setPctDescuentoEstado(Double pctDescuentoEstado) {
		this.pctDescuentoEstado = pctDescuentoEstado;
	}

	public List<CondicionEspecial> getCondicionesCotizacion() {
		return condicionesCotizacion;
	}

	public void setCondicionesCotizacion(
			List<CondicionEspecial> condicionesCotizacion) {
		this.condicionesCotizacion = condicionesCotizacion;
	}

	public List<CondicionEspecial> getCondicionesInciso() {
		return condicionesInciso;
	}

	public void setCondicionesInciso(List<CondicionEspecial> condicionesInciso) {
		this.condicionesInciso = condicionesInciso;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((amis == null) ? 0 : amis.hashCode());
		result = prime
				* result
				+ ((descripcionEstilo == null) ? 0 : descripcionEstilo
						.hashCode());
		result = prime * result + ((estado == null) ? 0 : estado.hashCode());
		result = prime * result + ((modelo == null) ? 0 : modelo.hashCode());
		result = prime * result
				+ ((municipio == null) ? 0 : municipio.hashCode());
		result = prime * result + ((paquete == null) ? 0 : paquete.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof ExcepcionCotizacionEvaluacionAutoDTO)) {
			return false;
		}
		ExcepcionCotizacionEvaluacionAutoDTO other = (ExcepcionCotizacionEvaluacionAutoDTO) obj;
		if (amis == null) {
			if (other.amis != null) {
				return false;
			}
		} else if (!amis.equals(other.amis)) {
			return false;
		}
		if (descripcionEstilo == null) {
			if (other.descripcionEstilo != null) {
				return false;
			}
		} else if (!descripcionEstilo.equals(other.descripcionEstilo)) {
			return false;
		}
		if (estado == null) {
			if (other.estado != null) {
				return false;
			}
		} else if (!estado.equals(other.estado)) {
			return false;
		}
		if (modelo == null) {
			if (other.modelo != null) {
				return false;
			}
		} else if (!modelo.equals(other.modelo)) {
			return false;
		}
		if (municipio == null) {
			if (other.municipio != null) {
				return false;
			}
		} else if (!municipio.equals(other.municipio)) {
			return false;
		}
		if (paquete == null) {
			if (other.paquete != null) {
				return false;
			}
		} else if (!paquete.equals(other.paquete)) {
			return false;
		}
		return true;
	}

	public Boolean getAplicaValidacionPersonaMoral() {
		return aplicaValidacionPersonaMoral;
	}

	public void setAplicaValidacionPersonaMoral(Boolean aplicaValidacionPersonaMoral) {
		this.aplicaValidacionPersonaMoral = aplicaValidacionPersonaMoral;
	}
	
	
	
	
}
