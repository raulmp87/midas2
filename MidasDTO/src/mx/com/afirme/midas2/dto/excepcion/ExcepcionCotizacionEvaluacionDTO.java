package mx.com.afirme.midas2.dto.excepcion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.sistema.cargacombo.ComboBean;

public class ExcepcionCotizacionEvaluacionDTO implements Serializable {
	
	private static final long serialVersionUID = 2626625030176955781L;

	private BigDecimal cotizacion;

	private Integer agente;

	private String usuario;

	private Long negocio;

	private BigDecimal producto;

	private BigDecimal tipoPoliza;
	
	private BigDecimal linea;
	
	private BigDecimal inciso;

	private Long secuenciaInciso;
	
	private BigDecimal cobertura;

	private Double sumaAsegurada;

	private Double deducible;
	
	private Double descuentoGlobal;
	
	private Date vigenciaIncial;
	
	private Date vigenciaFinal;
	
	private BigDecimal idCliente;
	
	private Short cveTipoPersona;
	
	private Double primaTotal;
	
	private Boolean igualacionPrimasCotizacion;
	
	private Double descuentoIgualacionCotizacion;
	
	private List<ComboBean> texAdicionalMap;

	
	public Double getDescuentoIgualacionCotizacion() {
		return descuentoIgualacionCotizacion;
	}

	public void setDescuentoIgualacionCotizacion(
			Double descuentoIgualacionCotizacion) {
		this.descuentoIgualacionCotizacion = descuentoIgualacionCotizacion;
	}

	public Date getVigenciaIncial() {
		return vigenciaIncial;
	}

	public void setVigenciaIncial(Date vigenciaIncial) {
		this.vigenciaIncial = vigenciaIncial;
	}

	public Date getVigenciaFinal() {
		return vigenciaFinal;
	}

	public void setVigenciaFinal(Date vigenciaFinal) {
		this.vigenciaFinal = vigenciaFinal;
	}

	public Double getDescuentoGlobal() {
		return descuentoGlobal;
	}

	public void setDescuentoGlobal(Double descuentoGlobal) {
		this.descuentoGlobal = descuentoGlobal;
	}

	public BigDecimal getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(BigDecimal cotizacion) {
		this.cotizacion = cotizacion;
	}

	public Integer getAgente() {
		return agente;
	}

	public void setAgente(Integer agente) {
		this.agente = agente;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Long getNegocio() {
		return negocio;
	}

	public void setNegocio(Long negocio) {
		this.negocio = negocio;
	}

	public BigDecimal getProducto() {
		return producto;
	}

	public void setProducto(BigDecimal producto) {
		this.producto = producto;
	}

	public BigDecimal getTipoPoliza() {
		return tipoPoliza;
	}

	public void setTipoPoliza(BigDecimal tipoPoliza) {
		this.tipoPoliza = tipoPoliza;
	}

	public BigDecimal getLinea() {
		return linea;
	}

	public void setLinea(BigDecimal linea) {
		this.linea = linea;
	}

	public BigDecimal getInciso() {
		return inciso;
	}

	public void setInciso(BigDecimal inciso) {
		this.inciso = inciso;
	}

	public Long getSecuenciaInciso() {
		return secuenciaInciso;
	}

	public void setSecuenciaInciso(Long secuenciaInciso) {
		this.secuenciaInciso = secuenciaInciso;
	}	

	public BigDecimal getCobertura() {
		return cobertura;
	}

	public void setCobertura(BigDecimal cobertura) {
		this.cobertura = cobertura;
	}

	public Double getSumaAsegurada() {
		return sumaAsegurada;
	}

	public void setSumaAsegurada(Double sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}

	public Double getDeducible() {
		return deducible;
	}

	public void setDeducible(Double deducible) {
		this.deducible = deducible;
	}
	
	public BigDecimal getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(BigDecimal idCliente) {
		this.idCliente = idCliente;
	}

	public Short getCveTipoPersona() {
		return cveTipoPersona;
	}

	public void setCveTipoPersona(Short cveTipoPersona) {
		this.cveTipoPersona = cveTipoPersona;
	}
	
	public Double getPrimaTotal() {
		return primaTotal;
	}

	public void setPrimaTotal(Double primaTotal) {
		this.primaTotal = primaTotal;
	}
	
	public Boolean getIgualacionPrimasCotizacion() {
		return igualacionPrimasCotizacion;
	}

	public void setIgualacionPrimasCotizacion(Boolean igualacionPrimasCotizacion) {
		this.igualacionPrimasCotizacion = igualacionPrimasCotizacion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((agente == null) ? 0 : agente.hashCode());
		result = prime * result
				+ ((cobertura == null) ? 0 : cobertura.hashCode());
		result = prime * result
				+ ((cotizacion == null) ? 0 : cotizacion.hashCode());
		result = prime * result
				+ ((deducible == null) ? 0 : deducible.hashCode());
		result = prime * result + ((inciso == null) ? 0 : inciso.hashCode());
		result = prime * result + ((linea == null) ? 0 : linea.hashCode());
		result = prime * result + ((negocio == null) ? 0 : negocio.hashCode());
		result = prime * result
				+ ((producto == null) ? 0 : producto.hashCode());
		result = prime * result
				+ ((secuenciaInciso == null) ? 0 : secuenciaInciso.hashCode());
		result = prime * result
				+ ((sumaAsegurada == null) ? 0 : sumaAsegurada.hashCode());
		result = prime * result
				+ ((tipoPoliza == null) ? 0 : tipoPoliza.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ExcepcionCotizacionEvaluacionDTO)) {
			return false;
		}
		ExcepcionCotizacionEvaluacionDTO other = (ExcepcionCotizacionEvaluacionDTO) obj;
		if (agente == null) {
			if (other.agente != null) {
				return false;
			}
		} else if (!agente.equals(other.agente)) {
			return false;
		}
		if (cobertura == null) {
			if (other.cobertura != null) {
				return false;
			}
		} else if (!cobertura.equals(other.cobertura)) {
			return false;
		}
		if (cotizacion == null) {
			if (other.cotizacion != null) {
				return false;
			}
		} else if (!cotizacion.equals(other.cotizacion)) {
			return false;
		}
		if (deducible == null) {
			if (other.deducible != null) {
				return false;
			}
		} else if (!deducible.equals(other.deducible)) {
			return false;
		}
		if (inciso == null) {
			if (other.inciso != null) {
				return false;
			}
		} else if (!inciso.equals(other.inciso)) {
			return false;
		}
		if (linea == null) {
			if (other.linea != null) {
				return false;
			}
		} else if (!linea.equals(other.linea)) {
			return false;
		}
		if (negocio == null) {
			if (other.negocio != null) {
				return false;
			}
		} else if (!negocio.equals(other.negocio)) {
			return false;
		}
		if (producto == null) {
			if (other.producto != null) {
				return false;
			}
		} else if (!producto.equals(other.producto)) {
			return false;
		}
		if (secuenciaInciso == null) {
			if (other.secuenciaInciso != null) {
				return false;
			}
		} else if (!secuenciaInciso.equals(other.secuenciaInciso)) {
			return false;
		}
		if (sumaAsegurada == null) {
			if (other.sumaAsegurada != null) {
				return false;
			}
		} else if (!sumaAsegurada.equals(other.sumaAsegurada)) {
			return false;
		}
		if (tipoPoliza == null) {
			if (other.tipoPoliza != null) {
				return false;
			}
		} else if (!tipoPoliza.equals(other.tipoPoliza)) {
			return false;
		}
		if (usuario == null) {
			if (other.usuario != null) {
				return false;
			}
		} else if (!usuario.equals(other.usuario)) {
			return false;
		}
		return true;
	}

	public void setTexAdicionalMap(List<ComboBean> texAdicionalMap) {
		this.texAdicionalMap = texAdicionalMap;
	}

	public List<ComboBean> getTexAdicionalMap() {
		return texAdicionalMap;
	}
	
}
