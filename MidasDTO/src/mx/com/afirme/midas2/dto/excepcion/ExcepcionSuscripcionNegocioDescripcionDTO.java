package mx.com.afirme.midas2.dto.excepcion;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.validator.group.NewItemChecks;

public class ExcepcionSuscripcionNegocioDescripcionDTO implements Serializable{
	
	
	private static final long serialVersionUID = -4671557485875854371L;

	private Long idExcepcion;
	
	private Long numExcepcion;
	
	private String descripcion;
	
	private String cveNegocio;
	
	private String usuario;
	
	private String agente;
	
	private String negocio;
	
	private String producto;
	
	private String tipoPoliza;
	
	private String lineaNegocio;
	
	private String paquete;
	
	private String cobertura;
	
	private String deducible;
	
	private String sumaAsegurada;
	
	private String zona;
	
	private boolean activo;
	
	private boolean habilitado;
	
	private boolean autorizacion;
	
	private boolean notificacion;
	
	private String correo;

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public Long getIdExcepcion() {
		return idExcepcion;
	}

	public void setIdExcepcion(Long idExcepcion) {
		this.idExcepcion = idExcepcion;
	}

	public Long getNumExcepcion() {
		return numExcepcion;
	}

	public void setNumExcepcion(Long numExcepcion) {
		this.numExcepcion = numExcepcion;
	}

	public String getAgente() {
		return agente;
	}

	public void setAgente(String agente) {
		this.agente = agente;
	}

	public String getNegocio() {
		return negocio;
	}

	public void setNegocio(String negocio) {
		this.negocio = negocio;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getTipoPoliza() {
		return tipoPoliza;
	}

	public void setTipoPoliza(String tipoPoliza) {
		this.tipoPoliza = tipoPoliza;
	}

	public String getLineaNegocio() {
		return lineaNegocio;
	}

	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}

	public String getPaquete() {
		return paquete;
	}

	public void setPaquete(String paquete) {
		this.paquete = paquete;
	}

	public String getCobertura() {
		return cobertura;
	}

	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}

	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public boolean isHabilitado() {
		return habilitado;
	}

	public void setHabilitado(boolean habilitado) {
		this.habilitado = habilitado;
	}

	public boolean isAutorizacion() {
		return autorizacion;
	}

	public void setAutorizacion(boolean autorizacion) {
		this.autorizacion = autorizacion;
	}

	public boolean isNotificacion() {
		return notificacion;
	}

	public void setNotificacion(boolean notificacion) {
		this.notificacion = notificacion;
	}

	public String getCveNegocio() {
		return cveNegocio;
	}

	public void setCveNegocio(String cveNegocio) {
		this.cveNegocio = cveNegocio;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	@NotNull(groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	@Size(min=1, max=50, groups=NewItemChecks.class, message="{javax.validation.constraints.Size.message}")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDeducible() {
		return deducible;
	}

	public void setDeducible(String deducible) {
		this.deducible = deducible;
	}

	public String getSumaAsegurada() {
		return sumaAsegurada;
	}

	public void setSumaAsegurada(String sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}

}
