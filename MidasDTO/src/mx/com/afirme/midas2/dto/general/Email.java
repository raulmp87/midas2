package mx.com.afirme.midas2.dto.general;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/*************************************************************************************
 * Nombre de la Clase: 	Email
 * Descripción:			Entidad para el manejo de Correos Electronicos
 * 
 * @author Eduardo.Chavez
 * 
 * 		Table:		EMAIL
 * 		Schema:		MIDAS
 * 		Sequence:	SEQEMAIL
 * 
 *********************************************************************************/
@Entity
@Table(name="TCEMAIL", schema = "MIDAS")
public class Email implements Serializable, mx.com.afirme.midas2.dao.catalogos.Entidad{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQTCEMAIL")
	@SequenceGenerator(name="SEQTCEMAIL", schema = "MIDAS", sequenceName="SEQTCEMAIL",allocationSize=1)
	@Column(name="IDTCEMAIL")
	private long idEmail;
	
	@Column(name="TCEMAIL")
	private String email;
	
	/*****************************
	 * Inician Getters y Setters *
	 *****************************/

	public long getIdEmail() {
		return idEmail;
	}

	public void setIdEmail(long idEmail) {
		this.idEmail = idEmail;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	/************************************************
	 * Mètodos implementados de la interfaz Entidad *
	 * Para el uso generico de los DAO y Servicios  *
	 ************************************************/

	@SuppressWarnings("unchecked")
	@Override
	public Object getKey() {
		return getIdEmail()==0?null:getIdEmail();
	}

	@Override
	public String getValue() {
		return "{ email: " + getEmail() + " }";
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
}
