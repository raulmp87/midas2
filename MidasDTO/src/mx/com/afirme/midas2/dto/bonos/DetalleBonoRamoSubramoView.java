package mx.com.afirme.midas2.dto.bonos;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class DetalleBonoRamoSubramoView implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Long idBeneficiario;
	private String nombreBeneficiario;
	private String descripcionRamo;
	private String descripcionSubRamo;
	private Double primaNeta;
	private Double pcteBono;
	private Double bonoPagar;
	private Double primaNetaBono;
	private BigDecimal importeIsr;
	private BigDecimal importeIva;
	private BigDecimal importeIvaRet;
	private Double pctsiniestralidad;
	private Double pctcrecimiento;
	private Double primaNetaPerodoCoparacion;
	
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdBeneficiario() {
		return idBeneficiario;
	}
	public void setIdBeneficiario(Long idBeneficiario) {
		this.idBeneficiario = idBeneficiario;
	}
	public String getNombreBeneficiario() {
		return nombreBeneficiario;
	}
	public void setNombreBeneficiario(String nombreBeneficiario) {
		this.nombreBeneficiario = nombreBeneficiario;
	}
	public String getDescripcionRamo() {
		return descripcionRamo;
	}
	public void setDescripcionRamo(String descripcionRamo) {
		this.descripcionRamo = descripcionRamo;
	}
	public String getDescripcionSubRamo() {
		return descripcionSubRamo;
	}
	public void setDescripcionSubRamo(String descripcionSubRamo) {
		this.descripcionSubRamo = descripcionSubRamo;
	}
	public Double getPrimaNeta() {
		return primaNeta;
	}
	public void setPrimaNeta(Double primaNeta) {
		this.primaNeta = primaNeta;
	}
	public Double getPcteBono() {
		return pcteBono;
	}
	public void setPcteBono(Double pcteBono) {
		this.pcteBono = pcteBono;
	}
	public Double getBonoPagar() {
		return bonoPagar;
	}
	public void setBonoPagar(Double bonoPagar) {
		this.bonoPagar = bonoPagar;
	}
	public Double getPrimaNetaBono() {
		return primaNetaBono;
	}
	public void setPrimaNetaBono(Double primaNetaBono) {
		this.primaNetaBono = primaNetaBono;
	}
	public BigDecimal getImporteIsr() {
		return importeIsr;
	}
	public void setImporteIsr(BigDecimal importeIsr) {
		this.importeIsr = importeIsr;
	}
	public BigDecimal getImporteIva() {
		return importeIva;
	}
	public void setImporteIva(BigDecimal importeIva) {
		this.importeIva = importeIva;
	}
	public BigDecimal getImporteIvaRet() {
		return importeIvaRet;
	}
	public void setImporteIvaRet(BigDecimal importeIvaRet) {
		this.importeIvaRet = importeIvaRet;
	}
	public Double getPctsiniestralidad() {
		return pctsiniestralidad;
	}
	public void setPctsiniestralidad(Double pctsiniestralidad) {
		this.pctsiniestralidad = pctsiniestralidad;
	}
	public Double getPctcrecimiento() {
		return pctcrecimiento;
	}
	public void setPctcrecimiento(Double pctcrecimiento) {
		this.pctcrecimiento = pctcrecimiento;
	}
	public Double getPrimaNetaPerodoCoparacion() {
		return primaNetaPerodoCoparacion;
	}
	public void setPrimaNetaPerodoCoparacion(Double primaNetaPerodoCoparacion) {
		this.primaNetaPerodoCoparacion = primaNetaPerodoCoparacion;
	}
	
}
