package mx.com.afirme.midas2.dto.bonos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
/**
 * 
 * @author vmhersil
 * Clase para utilizarla como filtro de busqueda en Configuracion de comisiones 
 * para la pantalla de listar configuracion de comisiones existentes.
 *
 */
@Entity
@SqlResultSetMapping(name="configBonosView",entities={
	@EntityResult(entityClass=ConfigBonosDTO.class,fields={
		@FieldResult(name="descripcion",column="descripcion"),
		@FieldResult(name="id",column="ID"),
		@FieldResult(name="activo",column="activo"),
		@FieldResult(name="pagoSinFactura",column="pagoSinFactura"),
		@FieldResult(name="todosLosAgentes",column="todosAgentes"),
		@FieldResult(name="fechaInicioVigencia",column="fechaInicioVigencia"),
		@FieldResult(name="fechaFinVigencia",column="fechaFinVigencia"),
		@FieldResult(name="importeMinimo",column="IMPORTEMINIMO"),
		@FieldResult(name="idProducto",column="idProducto"),
		@FieldResult(name="idRamo",column="idRamo"),
		@FieldResult(name="idSubRamo",column="idSubRamo"),
		@FieldResult(name="idSeccion",column="idSeccion"),
		@FieldResult(name="idCobertura",column="idCobertura"),
		@FieldResult(name="idMoneda",column="idMoneda"),
		@FieldResult(name="ajusteDeOficina",column="ajusteDeOficina"),
		@FieldResult(name="periodoAjuste",column="periodoAjuste"),
		@FieldResult(name="idBonoAjustar",column="idBonoAjustar"),
		@FieldResult(name="produccionMinima",column="produccionMinima"),
		@FieldResult(name="porcentajeSiniestralidadMaxima",column="porcentajeSiniestralidadMaxima"),
		@FieldResult(name="global",column="global"),
		@FieldResult(name="periodoInicial",column="periodoInicial"),
		@FieldResult(name="periodoFinal",column="periodoFinal"),
		@FieldResult(name="porcentajeImporteDirecto",column="porcentajeImporteDirecto"),
		@FieldResult(name="porcentajeAplicacionDirecto",column="porcentajeAplicacionDirecto"),
		@FieldResult(name="importeAplicacionDirecto",column="importeAplicacionDirecto"),
		@FieldResult(name="porcentajeImporteRangos",column="porcentajeImporteRangos"),
		@FieldResult(name="tipoBeneficiario",column="tipoBeneficiario"),
		@FieldResult(name="todasPromotorias",column="todasPromotorias"),
		@FieldResult(name="todosAgentes",column="todosAgentes"),
		@FieldResult(name="tipoBono.id",column="tipoBono"),
		@FieldResult(name="periodoComparacion.id",column="periodoComparacion"),
		@FieldResult(name="siniestralidadSobre.id",column="siniestralidadSobre"),
		@FieldResult(name="produccionSobre.id",column="produccionSobre"),
		@FieldResult(name="modoAplicacion.id",column="modoAplicacion"),
		@FieldResult(name="periodoAjuste.id",column="periodoAjuste"),
		@FieldResult(name="idCentroOperacion",column="IDCENTROOPERACION"),
		@FieldResult(name="idGerencia",column="IDGERENCIA"),
		@FieldResult(name="idEjecutivo",column="IDEJECUTIVO"),
		@FieldResult(name="idPromotoria",column="IDPROMOTORIA"),
		@FieldResult(name="idTipoAgente",column="TIPOAGENTE"),
		@FieldResult(name="idTipoPromotoria",column="TIPOPROMOTORIA"),
		@FieldResult(name="idProridad",column="PRIORIDAD"),
		@FieldResult(name="idSituacion",column="SITUACION"),
		@FieldResult(name ="idPoliza", column="IDPOLIZA"),
		@FieldResult(name="descripcionTipoBono", column="descripcionTipoBono"),
		@FieldResult(name="descripcionProducto", column="descripcionProducto"),
		@FieldResult(name="descripLineaNeg", column="descripLineaNeg")
	})
})
public class ConfigBonosDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -9151339331807670544L;
	private Long idCentroOperacion;
	private Long idGerencia;
	private Long idProridad;
	private Long idEjecutivo;
	private Long idPromotoria;
	private Long idTipoPromotoria;
	private Long idTipoAgente;
	private Long idSituacion;
	private Date fechaAltaInicio;
	private Date fechaAltaFin;
	private String nombreAgente;//Solo para consulta por filtro
	private Long idProducto;
	private	Long idRamo;
	private	Long idSubramo;
	private Long idSeccion;
	private	Long idCobertura;
	private Long	id;
	private ValorCatalogoAgentes tipoBono;
	private	String	descripcion;
	private Boolean pagoSinFacturaBoolean;
	private Integer	pagoSinFactura;
	private Date	fechaInicioVigencia;
	private	Date 	fechaFinVigencia;
	private	Long	idMoneda;
	private	Boolean	ajusteDeOficinaBoolean;
	private Integer	ajusteDeOficina;
	private ValorCatalogoAgentes periodoAjuste;
	private Long	idBonoAjustar;
	private ValorCatalogoAgentes produccionSobre;
	private	ValorCatalogoAgentes siniestralidadSobre;
	private BigDecimal	produccionMinima;
	private BigDecimal 	porcentajeSiniestralidadMaxima;
	private Boolean globaBoolean;
	private Integer	global;
	private Date 	periodoInicial;
	private Date	periodoFinal;
	private ValorCatalogoAgentes modoAplicacion;
	private Boolean	porcentajeImporteDirectoBoolean;
	private	BigDecimal	porcentajeImporteDirecto;
	private Double 	porcentajeAplicacionDirecto;
	private Double	importeAplicacionDirecto;
	private Boolean porcentajeImporteRangosBoolean;
	private Integer porcentajeImporteRangos;
	private ValorCatalogoAgentes periodoComparacion;
	private Boolean tipoBeneficiarioBoolean;
	private Long	tipoBeneficiario;
	private Boolean todasPromotoriasBoolean;
	private Integer	todasPromotorias;
	private Boolean todosAgentesBoolean;
	private Integer	todosAgentes;
	private Boolean activoBoolean;
	private Integer	activo;
	private Long idPoliza;
	private String descripcionTipoBono;
	private String descripcionProducto;
	private String descripLineaNeg;
	
	public String getDescripLineaNeg() {
		return descripLineaNeg;
	}
	public void setDescripLineaNeg(String descripLineaNeg) {
		this.descripLineaNeg = descripLineaNeg;
	}
	public String getDescripcionProducto() {
		return descripcionProducto;
	}
	public void setDescripcionProducto(String descripcionProducto) {
		this.descripcionProducto = descripcionProducto;
	}
	public String getDescripcionTipoBono() {
		return descripcionTipoBono;
	}
	public void setDescripcionTipoBono(String descripcionTipoBono) {
		this.descripcionTipoBono = descripcionTipoBono;
	}
	public Long getIdPoliza() {
		return idPoliza;
	}
	public void setIdPoliza(Long idPoliza) {
		this.idPoliza = idPoliza;
	}
	public Long getIdCentroOperacion() {
		return idCentroOperacion;
	}
	public void setIdCentroOperacion(Long idCentroOperacion) {
		this.idCentroOperacion = idCentroOperacion;
	}
	public Long getIdGerencia() {
		return idGerencia;
	}
	public void setIdGerencia(Long idGerencia) {
		this.idGerencia = idGerencia;
	}
	public Long getIdProridad() {
		return idProridad;
	}
	public void setIdProridad(Long idProridad) {
		this.idProridad = idProridad;
	}
	public Long getIdEjecutivo() {
		return idEjecutivo;
	}
	public void setIdEjecutivo(Long idEjecutivo) {
		this.idEjecutivo = idEjecutivo;
	}
	public Long getIdPromotoria() {
		return idPromotoria;
	}
	public void setIdPromotoria(Long idPromotoria) {
		this.idPromotoria = idPromotoria;
	}
	public Long getIdTipoPromotoria() {
		return idTipoPromotoria;
	}
	public void setIdTipoPromotoria(Long idTipoPromotoria) {
		this.idTipoPromotoria = idTipoPromotoria;
	}
	public Long getIdTipoAgente() {
		return idTipoAgente;
	}
	public void setIdTipoAgente(Long idTipoAgente) {
		this.idTipoAgente = idTipoAgente;
	}
	public Long getIdSituacion() {
		return idSituacion;
	}
	public void setIdSituacion(Long idSituacion) {
		this.idSituacion = idSituacion;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaAltaInicio() {
		return fechaAltaInicio;
	}
	public void setFechaAltaInicio(Date fechaAltaInicio) {
		this.fechaAltaInicio = fechaAltaInicio;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaAltaFin() {
		return fechaAltaFin;
	}
	public void setFechaAltaFin(Date fechaAltaFin) {
		this.fechaAltaFin = fechaAltaFin;
	}
	public String getNombreAgente() {
		return nombreAgente;
	}
	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}
	public Long getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}
	public Long getIdRamo() {
		return idRamo;
	}
	public void setIdRamo(Long idRamo) {
		this.idRamo = idRamo;
	}
	public Long getIdSubramo() {
		return idSubramo;
	}
	public void setIdSubramo(Long idSubramo) {
		this.idSubramo = idSubramo;
	}
	public Long getIdSeccion() {
		return idSeccion;
	}
	public void setIdSeccion(Long idSeccion) {
		this.idSeccion = idSeccion;
	}
	public Long getIdCobertura() {
		return idCobertura;
	}
	public void setIdCobertura(Long idCobertura) {
		this.idCobertura = idCobertura;
	}
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public ValorCatalogoAgentes getTipoBono() {
		return tipoBono;
	}
	public void setTipoBono(ValorCatalogoAgentes tipoBono) {
		this.tipoBono = tipoBono;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Boolean getPagoSinFacturaBoolean() {
		return pagoSinFacturaBoolean;
	}
	public void setPagoSinFacturaBoolean(Boolean pagoSinFacturaBoolean) {
		this.pagoSinFacturaBoolean = pagoSinFacturaBoolean;
	}
	public Integer getPagoSinFactura() {
		return pagoSinFactura;
	}
	public void setPagoSinFactura(Integer pagoSinFactura) {
		this.pagoSinFactura = pagoSinFactura;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}
	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}
	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}
	public Long getIdMoneda() {
		return idMoneda;
	}
	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}
	public Boolean getAjusteDeOficinaBoolean() {
		return ajusteDeOficinaBoolean;
	}
	public void setAjusteDeOficinaBoolean(Boolean ajusteDeOficinaBoolean) {
		this.ajusteDeOficinaBoolean = ajusteDeOficinaBoolean;
	}
	public Integer getAjusteDeOficina() {
		return ajusteDeOficina;
	}
	public void setAjusteDeOficina(Integer ajusteDeOficina) {
		this.ajusteDeOficina = ajusteDeOficina;
	}
	public ValorCatalogoAgentes getPeriodoAjuste() {
		return periodoAjuste;
	}
	public void setPeriodoAjuste(ValorCatalogoAgentes periodoAjuste) {
		this.periodoAjuste = periodoAjuste;
	}
	public Long getIdBonoAjustar() {
		return idBonoAjustar;
	}
	public void setIdBonoAjustar(Long idBonoAjustar) {
		this.idBonoAjustar = idBonoAjustar;
	}
	public ValorCatalogoAgentes getProduccionSobre() {
		return produccionSobre;
	}
	public void setProduccionSobre(ValorCatalogoAgentes produccionSobre) {
		this.produccionSobre = produccionSobre;
	}
	public ValorCatalogoAgentes getSiniestralidadSobre() {
		return siniestralidadSobre;
	}
	public void setSiniestralidadSobre(ValorCatalogoAgentes siniestralidadSobre) {
		this.siniestralidadSobre = siniestralidadSobre;
	}
	public BigDecimal getProduccionMinima() {
		return produccionMinima;
	}
	public void setProduccionMinima(BigDecimal produccionMinima) {
		this.produccionMinima = produccionMinima;
	}
	public BigDecimal getPorcentajeSiniestralidadMaxima() {
		return porcentajeSiniestralidadMaxima;
	}
	public void setPorcentajeSiniestralidadMaxima(
			BigDecimal porcentajeSiniestralidadMaxima) {
		this.porcentajeSiniestralidadMaxima = porcentajeSiniestralidadMaxima;
	}
	public Boolean getGlobaBoolean() {
		return globaBoolean;
	}
	public void setGlobaBoolean(Boolean globaBoolean) {
		this.globaBoolean = globaBoolean;
	}
	public Integer getGlobal() {
		return global;
	}
	public void setGlobal(Integer global) {
		this.global = global;
	}
	@Temporal(TemporalType.DATE)
	public Date getPeriodoInicial() {
		return periodoInicial;
	}
	public void setPeriodoInicial(Date periodoInicial) {
		this.periodoInicial = periodoInicial;
	}
	@Temporal(TemporalType.DATE)
	public Date getPeriodoFinal() {
		return periodoFinal;
	}
	public void setPeriodoFinal(Date periodoFinal) {
		this.periodoFinal = periodoFinal;
	}
	public ValorCatalogoAgentes getModoAplicacion() {
		return modoAplicacion;
	}
	public void setModoAplicacion(ValorCatalogoAgentes modoAplicacion) {
		this.modoAplicacion = modoAplicacion;
	}
	public Boolean getPorcentajeImporteDirectoBoolean() {
		return porcentajeImporteDirectoBoolean;
	}
	public void setPorcentajeImporteDirectoBoolean(
			Boolean porcentajeImporteDirectoBoolean) {
		this.porcentajeImporteDirectoBoolean = porcentajeImporteDirectoBoolean;
	}
	public BigDecimal getPorcentajeImporteDirecto() {
		return porcentajeImporteDirecto;
	}
	public void setPorcentajeImporteDirecto(BigDecimal porcentajeImporteDirecto) {
		this.porcentajeImporteDirecto = porcentajeImporteDirecto;
	}
	public Double getPorcentajeAplicacionDirecto() {
		return porcentajeAplicacionDirecto;
	}
	public void setPorcentajeAplicacionDirecto(Double porcentajeAplicacionDirecto) {
		this.porcentajeAplicacionDirecto = porcentajeAplicacionDirecto;
	}
	public Double getImporteAplicacionDirecto() {
		return importeAplicacionDirecto;
	}
	public void setImporteAplicacionDirecto(Double importeAplicacionDirecto) {
		this.importeAplicacionDirecto = importeAplicacionDirecto;
	}
	public Boolean getPorcentajeImporteRangosBoolean() {
		return porcentajeImporteRangosBoolean;
	}
	public void setPorcentajeImporteRangosBoolean(
			Boolean porcentajeImporteRangosBoolean) {
		this.porcentajeImporteRangosBoolean = porcentajeImporteRangosBoolean;
	}
	public Integer getPorcentajeImporteRangos() {
		return porcentajeImporteRangos;
	}
	public void setPorcentajeImporteRangos(Integer porcentajeImporteRangos) {
		this.porcentajeImporteRangos = porcentajeImporteRangos;
	}
	public ValorCatalogoAgentes getPeriodoComparacion() {
		return periodoComparacion;
	}
	public void setPeriodoComparacion(ValorCatalogoAgentes periodoComparacion) {
		this.periodoComparacion = periodoComparacion;
	}
	public Boolean getTipoBeneficiarioBoolean() {
		return tipoBeneficiarioBoolean;
	}
	public void setTipoBeneficiarioBoolean(Boolean tipoBeneficiarioBoolean) {
		this.tipoBeneficiarioBoolean = tipoBeneficiarioBoolean;
	}
	public Long getTipoBeneficiario() {
		return tipoBeneficiario;
	}
	public void setTipoBeneficiario(Long tipoBeneficiario) {
		this.tipoBeneficiario = tipoBeneficiario;
	}
	public Boolean getTodasPromotoriasBoolean() {
		return todasPromotoriasBoolean;
	}
	public void setTodasPromotoriasBoolean(Boolean todasPromotoriasBoolean) {
		this.todasPromotoriasBoolean = todasPromotoriasBoolean;
	}
	public Integer getTodasPromotorias() {
		return todasPromotorias;
	}
	public void setTodasPromotorias(Integer todasPromotorias) {
		this.todasPromotorias = todasPromotorias;
	}
	public Boolean getTodosAgentesBoolean() {
		return todosAgentesBoolean;
	}
	public void setTodosAgentesBoolean(Boolean todosAgentesBoolean) {
		this.todosAgentesBoolean = todosAgentesBoolean;
	}
	public Integer getTodosAgentes() {
		return todosAgentes;
	}
	public void setTodosAgentes(Integer todosAgentes) {
		this.todosAgentes = todosAgentes;
	}
	public Boolean getActivoBoolean() {
		return activoBoolean;
	}
	public void setActivoBoolean(Boolean activoBoolean) {
		this.activoBoolean = activoBoolean;
	}
	public Integer getActivo() {
		return activo;
	}
	public void setActivo(Integer activo) {
		this.activo = activo;
	}
}
