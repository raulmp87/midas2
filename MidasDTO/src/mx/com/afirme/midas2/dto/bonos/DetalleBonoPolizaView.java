package mx.com.afirme.midas2.dto.bonos;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class DetalleBonoPolizaView implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1350113736334478032L;
	
	private Long idCalculoBono;
	private Long idBeneficiario;
	private String nombreBeneficiario;
	private String numeroPoliza;
	private BigDecimal importeBono;
	private BigDecimal importePrima;
	private String descripcionRamo;
	private String descripcionSubRamo;
	private Double porcentajeSiniestralidad;
	private Long numRecibo;
	private Long idRecibo;
	private String cobertura;
	private Long idCobertura;
	private Double porcentajeCrecimiento;
	private Double porcentajeSuperacionMeta;
	private BigDecimal importeIsr;
	private BigDecimal importeIva;
	private BigDecimal importeIvaRet;
	private Double primaNetaBono;
	private BigDecimal primaNetaPerodoCoparacion;
	private String exclusion;
	private String idestilo;
	private Double pcteBono;
	private String tipoRecibo;
	
	@Id
	public Long getIdCalculoBono() {
		return idCalculoBono;
	}
	public void setIdCalculoBono(Long idCalculoBono) {
		this.idCalculoBono = idCalculoBono;
	}
	public Long getIdBeneficiario() {
		return idBeneficiario;
	}
	public void setIdBeneficiario(Long idBeneficiario) {
		this.idBeneficiario = idBeneficiario;
	}
	public String getNombreBeneficiario() {
		return nombreBeneficiario;
	}
	public void setNombreBeneficiario(String nombreBeneficiario) {
		this.nombreBeneficiario = nombreBeneficiario;
	}
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public BigDecimal getImporteBono() {
		return importeBono;
	}
	public void setImporteBono(BigDecimal importeBono) {
		this.importeBono = importeBono;
	}
	public BigDecimal getImportePrima() {
		return importePrima;
	}
	public void setImportePrima(BigDecimal importePrima) {
		this.importePrima = importePrima;
	}
	public String getDescripcionRamo() {
		return descripcionRamo;
	}
	public void setDescripcionRamo(String descripcionRamo) {
		this.descripcionRamo = descripcionRamo;
	}
	public String getDescripcionSubRamo() {
		return descripcionSubRamo;
	}
	public void setDescripcionSubRamo(String descripcionSubRamo) {
		this.descripcionSubRamo = descripcionSubRamo;
	}
	public Double getPorcentajeSiniestralidad() {
		return porcentajeSiniestralidad;
	}
	public void setPorcentajeSiniestralidad(Double porcentajeSiniestralidad) {
		this.porcentajeSiniestralidad = porcentajeSiniestralidad;
	}
	public Long getNumRecibo() {
		return numRecibo;
	}
	public void setNumRecibo(Long numRecibo) {
		this.numRecibo = numRecibo;
	}
	public Long getIdRecibo() {
		return idRecibo;
	}
	public void setIdRecibo(Long idRecibo) {
		this.idRecibo = idRecibo;
	}
	public String getCobertura() {
		return cobertura;
	}
	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}
	public Long getIdCobertura() {
		return idCobertura;
	}
	public void setIdCobertura(Long idCobertura) {
		this.idCobertura = idCobertura;
	}
	public Double getPorcentajeCrecimiento() {
		return porcentajeCrecimiento;
	}
	public void setPorcentajeCrecimiento(Double porcentajeCrecimiento) {
		this.porcentajeCrecimiento = porcentajeCrecimiento;
	}
	public Double getPorcentajeSuperacionMeta() {
		return porcentajeSuperacionMeta;
	}
	public void setPorcentajeSuperacionMeta(Double porcentajeSuperacionMeta) {
		this.porcentajeSuperacionMeta = porcentajeSuperacionMeta;
	}
	public BigDecimal getImporteIsr() {
		return importeIsr;
	}
	public void setImporteIsr(BigDecimal importeIsr) {
		this.importeIsr = importeIsr;
	}
	public BigDecimal getImporteIva() {
		return importeIva;
	}
	public void setImporteIva(BigDecimal importeIva) {
		this.importeIva = importeIva;
	}
	public BigDecimal getImporteIvaRet() {
		return importeIvaRet;
	}
	public void setImporteIvaRet(BigDecimal importeIvaRet) {
		this.importeIvaRet = importeIvaRet;
	}
	public Double getPrimaNetaBono() {
		return primaNetaBono;
	}
	public void setPrimaNetaBono(Double primaNetaBono) {
		this.primaNetaBono = primaNetaBono;
	}
	public BigDecimal getPrimaNetaPerodoCoparacion() {
		return primaNetaPerodoCoparacion;
	}
	public void setPrimaNetaPerodoCoparacion(BigDecimal primaNetaPerodoCoparacion) {
		this.primaNetaPerodoCoparacion = primaNetaPerodoCoparacion;
	}
	public String getExclusion() {
		return exclusion;
	}
	public void setExclusion(String exclusion) {
		this.exclusion = exclusion;
	}
	public String getIdestilo() {
		return idestilo;
	}
	public void setIdestilo(String idestilo) {
		this.idestilo = idestilo;
	}
	public Double getPcteBono() {
		return pcteBono;
	}
	public void setPcteBono(Double pcteBono) {
		this.pcteBono = pcteBono;
	}
	public String getTipoRecibo() {
		return tipoRecibo;
	}
	public void setTipoRecibo(String tipoRecibo) {
		this.tipoRecibo = tipoRecibo;
	}
}
