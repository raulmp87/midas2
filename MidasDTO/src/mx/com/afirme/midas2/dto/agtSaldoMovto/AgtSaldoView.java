package mx.com.afirme.midas2.dto.agtSaldoMovto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
public class AgtSaldoView implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4851258324038451967L;
	private Long id;
	private String mes;
	private Date fechaInicioPeriodo;
	private String fechaInicioPeriodoString;
	private Date fechaFinPeriodo;
	private String fechaFinPeriodoString;
	private Double saldoInicial;
	private Double saldoFinal;
	private String situacionSaldo;
	private Date fechaSituacion;
	private String fechaSituacionString;
	private Long idAgente;
	private Long anioMes;
	private Long idMoneda;
	private String nombreCompleto;
	private String situacionAgente;
	private String motivoEstatus;
	
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaInicioPeriodo() {
		return fechaInicioPeriodo;
	}
	public void setFechaInicioPeriodo(Date fechaInicioPeriodo) {
		this.fechaInicioPeriodo = fechaInicioPeriodo;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
		this.fechaInicioPeriodoString = sdf.format(fechaInicioPeriodo);
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaFinPeriodo() {
		return fechaFinPeriodo;
	}
	public void setFechaFinPeriodo(Date fechaFinPeriodo) {
		this.fechaFinPeriodo = fechaFinPeriodo;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
		this.fechaFinPeriodoString = sdf.format(fechaFinPeriodo);
	}
	public Double getSaldoInicial() {
		return saldoInicial;
	}
	public void setSaldoInicial(Double saldoInicial) {
		this.saldoInicial = saldoInicial;
	}
	public Double getSaldoFinal() {
		return saldoFinal;
	}
	public void setSaldoFinal(Double saldoFinal) {
		this.saldoFinal = saldoFinal;
	}
	public String getSituacionSaldo() {
		return situacionSaldo;
	}
	public void setSituacionSaldo(String situacionSaldo) {
		this.situacionSaldo = situacionSaldo;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaSituacion() {
		return fechaSituacion;
	}
	public void setFechaSituacion(Date fechaSituacion) {
		this.fechaSituacion = fechaSituacion;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
		this.fechaSituacionString = sdf.format(fechaSituacion);
	}
	public Long getIdAgente() {
		return idAgente;
	}
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
	public Long getAnioMes() {
		return anioMes;
	}
	public void setAnioMes(Long anioMes) {
		this.anioMes = anioMes;
	}
	public Long getIdMoneda() {
		return idMoneda;
	}
	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	public String getSituacionAgente() {
		return situacionAgente;
	}
	public void setSituacionAgente(String situacionAgente) {
		this.situacionAgente = situacionAgente;
	}
	public String getMotivoEstatus() {
		return motivoEstatus;
	}
	public void setMotivoEstatus(String motivoEstatus) {
		this.motivoEstatus = motivoEstatus;
	}
	
	@Transient
	public String getFechaInicioPeriodoString() {
		return fechaInicioPeriodoString;
	}
	public void setFechaInicioPeriodoString(String fechaInicioPeriodoString) {
		this.fechaInicioPeriodoString = fechaInicioPeriodoString;
	}
	@Transient
	public String getFechaFinPeriodoString() {
		return fechaFinPeriodoString;
	}
	public void setFechaFinPeriodoString(String fechaFinPeriodoString) {
		this.fechaFinPeriodoString = fechaFinPeriodoString;
	}
	@Transient
	public String getFechaSituacionString() {
		return fechaSituacionString;
	}
	public void setFechaSituacionString(String fechaSituacionString) {
		this.fechaSituacionString = fechaSituacionString;
	}
	
	
	
}
