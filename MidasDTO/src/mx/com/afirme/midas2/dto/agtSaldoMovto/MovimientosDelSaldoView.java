package mx.com.afirme.midas2.dto.agtSaldoMovto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class MovimientosDelSaldoView implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8438074439212911605L;
	private	Long	id;
	private	Long	idEmpresa;
	private	String	nombreAgente;
	private	Date	fehcaCorteEdocta;
	private	String	fehcaCorteEdoctaString;
	private	Long	idConsecMovto;
	private	String	anioMes;
	private	Long	idConcepto;
	private	String moneda;
	private	String	monedaOrigen;
	private	Long	tipoCambio;
	private	String	descripcionMovto;
	private	String	idRamoContable;
	private	String	idSubramoContable;
	private	Long	idLineaNegocio;
	private	Date	fechaMovimiento;
	private	String	fechaMovimientoString;
	private	Long	idCentroEmisor;
	private	Long	numPoliza;
	private	Long	numRenovacionPoliza;
	private	Long	idCotizacion;
	private	Long	idVersionPol;
	private	Long	idCentroEmisore;
	private	Long	tipoEndoso;
	private	Long	numeroEndoso;
	private	Long	idSolicitud;
	private	Long	idVersionEndoso;
	private	String	referencia;
	private	String	centroOperacion;
	private	Long	idRemesa;
	private	Long	idConsecMovtoR;
	private	String	claveOrigenRemesa;
	private	String	serieFolioRbo;
	private	Long	numFolioRbo;
	private	Long	idRecibo;
	private	Long	idVersionRbo;
	private	Date	fechavencimientorec	;
	private	String	fechavencimientorecString;
	private	Double	porcentajePartAgente;
	private	Double	importePrimaNeta;
	private	Double	impRcgossPagoFr;
	private	Double	importeComisionAgente;
	private	String	claveOrigenAplic;
	private	String	claveOrigenMovto;
	private	Date	fechaCortePagoCom;
	private	String	fechaCortePagoComString;
	private	String	estatusMovimiento;
	private	String	idUsuarioIntegeg;
	private	Date	fhIntegracion;
	private	String	fhIntegracionString;
	private	Date	fhAplicacion;
	private	String	fhAplicacionString;
	private	String	naturalezaConcepto;
	private	String	tipoMovtoCobranza;
	private	Date	fTransfDePv;
	private	String	fTransfDePvString;
	private	Long	idTransacDePv;
	private	Long	idTreansaccOrig;
	private	String	agenteOrigen;
	private	Long	idCobertura;
	private	String	esFacultativo;
	private	Long	anioVigenciaPoliza;
	private	String	claveSistAdmon;
	private	String	claveExpCalcDiv;
	private	Long	importePrimaDcp;
	private	Long	importePrimaTotal;
	private	String	cvetCptoAco;
	private	Long	idConceptoO;
	private	String	esMasivo;
	private	Double	importeBaseCalculo;
	private	Double	porcentajeComisionAgente;
	private	Long	numMovtoManAgente;
	private	Double	importePagoAntImptos;
	private	Double	porcentajeIva;
	private	Double	porcentageIvaRetenido;
	private	Double	porcentajeIsrRet;
	private	Double	importeIva;
	private	Double	importeIvaRet;
	private	Double	importeIsr;
	private	String	cveSdoImpto;
	private	String	esImpuesto;
	private	Long	anioMesPago;
	private	Long	idCalculo;
	private String descripcionConcepto;
	
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	public String getNombreAgente() {
		return nombreAgente;
	}
	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}
	@Temporal(TemporalType.DATE)
	public Date getFehcaCorteEdocta() {
		return fehcaCorteEdocta;
	}
	public void setFehcaCorteEdocta(Date fehcaCorteEdocta) {
		this.fehcaCorteEdocta = fehcaCorteEdocta;
//		if(fehcaCorteEdocta!=null){
//			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
//			this.fehcaCorteEdoctaString = sdf.format(fehcaCorteEdocta);
//		}
	}
	public Long getIdConsecMovto() {
		return idConsecMovto;
	}
	public void setIdConsecMovto(Long idConsecMovto) {
		this.idConsecMovto = idConsecMovto;
	}
	public String getAnioMes() {
		return anioMes;
	}
	public void setAnioMes(String anioMes) {
		this.anioMes = anioMes;
	}
	public Long getIdConcepto() {
		return idConcepto;
	}
	public void setIdConcepto(Long idConcepto) {
		this.idConcepto = idConcepto;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getMonedaOrigen() {
		return monedaOrigen;
	}
	public void setMonedaOrigen(String monedaOrigen) {
		this.monedaOrigen = monedaOrigen;
	}
	public Long getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(Long tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public String getDescripcionMovto() {
		return descripcionMovto;
	}
	public void setDescripcionMovto(String descripcionMovto) {
		this.descripcionMovto = descripcionMovto;
	}
	public String getIdRamoContable() {
		return idRamoContable;
	}
	public void setIdRamoContable(String idRamoContable) {
		this.idRamoContable = idRamoContable;
	}
	public String getIdSubramoContable() {
		return idSubramoContable;
	}
	public void setIdSubramoContable(String idSubramoContable) {
		this.idSubramoContable = idSubramoContable;
	}
	public Long getIdLineaNegocio() {
		return idLineaNegocio;
	}
	public void setIdLineaNegocio(Long idLineaNegocio) {
		this.idLineaNegocio = idLineaNegocio;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaMovimiento() {
		return fechaMovimiento;
	}
	public void setFechaMovimiento(Date fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
//		if(fechaMovimiento!=null){
//			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
//			this.fechaMovimientoString = sdf.format(fechaMovimiento);
//		}
	}
	public Long getIdCentroEmisor() {
		return idCentroEmisor;
	}
	public void setIdCentroEmisor(Long idCentroEmisor) {
		this.idCentroEmisor = idCentroEmisor;
	}
	public Long getNumPoliza() {
		return numPoliza;
	}
	public void setNumPoliza(Long numPoliza) {
		this.numPoliza = numPoliza;
	}
	public Long getNumRenovacionPoliza() {
		return numRenovacionPoliza;
	}
	public void setNumRenovacionPoliza(Long numRenovacionPoliza) {
		this.numRenovacionPoliza = numRenovacionPoliza;
	}
	public Long getIdCotizacion() {
		return idCotizacion;
	}
	public void setIdCotizacion(Long idCotizacion) {
		this.idCotizacion = idCotizacion;
	}
	public Long getIdVersionPol() {
		return idVersionPol;
	}
	public void setIdVersionPol(Long idVersionPol) {
		this.idVersionPol = idVersionPol;
	}
	public Long getIdCentroEmisore() {
		return idCentroEmisore;
	}
	public void setIdCentroEmisore(Long idCentroEmisore) {
		this.idCentroEmisore = idCentroEmisore;
	}
	public Long getTipoEndoso() {
		return tipoEndoso;
	}
	public void setTipoEndoso(Long tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}
	public Long getNumeroEndoso() {
		return numeroEndoso;
	}
	public void setNumeroEndoso(Long numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}
	public Long getIdSolicitud() {
		return idSolicitud;
	}
	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}
	public Long getIdVersionEndoso() {
		return idVersionEndoso;
	}
	public void setIdVersionEndoso(Long idVersionEndoso) {
		this.idVersionEndoso = idVersionEndoso;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getCentroOperacion() {
		return centroOperacion;
	}
	public void setCentroOperacion(String centroOperacion) {
		this.centroOperacion = centroOperacion;
	}
	public Long getIdRemesa() {
		return idRemesa;
	}
	public void setIdRemesa(Long idRemesa) {
		this.idRemesa = idRemesa;
	}
	public Long getIdConsecMovtoR() {
		return idConsecMovtoR;
	}
	public void setIdConsecMovtoR(Long idConsecMovtoR) {
		this.idConsecMovtoR = idConsecMovtoR;
	}
	public String getClaveOrigenRemesa() {
		return claveOrigenRemesa;
	}
	public void setClaveOrigenRemesa(String claveOrigenRemesa) {
		this.claveOrigenRemesa = claveOrigenRemesa;
	}
	public String getSerieFolioRbo() {
		return serieFolioRbo;
	}
	public void setSerieFolioRbo(String serieFolioRbo) {
		this.serieFolioRbo = serieFolioRbo;
	}
	public Long getNumFolioRbo() {
		return numFolioRbo;
	}
	public void setNumFolioRbo(Long numFolioRbo) {
		this.numFolioRbo = numFolioRbo;
	}
	public Long getIdRecibo() {
		return idRecibo;
	}
	public void setIdRecibo(Long idRecibo) {
		this.idRecibo = idRecibo;
	}
	public Long getIdVersionRbo() {
		return idVersionRbo;
	}
	public void setIdVersionRbo(Long idVersionRbo) {
		this.idVersionRbo = idVersionRbo;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechavencimientorec() {
		return fechavencimientorec;
	}
	public void setFechavencimientorec(Date fechavencimientorec) {
		this.fechavencimientorec = fechavencimientorec;
//		if(fechavencimientorec!=null){
//			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
//			this.fechavencimientorecString = sdf.format(fechavencimientorec);
//		}
	}
	public Double getPorcentajePartAgente() {
		return porcentajePartAgente;
	}
	public void setPorcentajePartAgente(Double porcentajePartAgente) {
		this.porcentajePartAgente = porcentajePartAgente;
	}
	public Double getImportePrimaNeta() {
		return importePrimaNeta;
	}
	public void setImportePrimaNeta(Double importePrimaNeta) {
		this.importePrimaNeta = importePrimaNeta;
	}
	public Double getImpRcgossPagoFr() {
		return impRcgossPagoFr;
	}
	public void setImpRcgossPagoFr(Double impRcgossPagoFr) {
		this.impRcgossPagoFr = impRcgossPagoFr;
	}
	public Double getImporteComisionAgente() {
		return importeComisionAgente;
	}
	public void setImporteComisionAgente(Double importeComisionAgente) {
		this.importeComisionAgente = importeComisionAgente;
	}
	public String getClaveOrigenAplic() {
		return claveOrigenAplic;
	}
	public void setClaveOrigenAplic(String claveOrigenAplic) {
		this.claveOrigenAplic = claveOrigenAplic;
	}
	public String getClaveOrigenMovto() {
		return claveOrigenMovto;
	}
	public void setClaveOrigenMovto(String claveOrigenMovto) {
		this.claveOrigenMovto = claveOrigenMovto;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaCortePagoCom() {
		return fechaCortePagoCom;
	}
	public void setFechaCortePagoCom(Date fechaCortePagoCom) {
		this.fechaCortePagoCom = fechaCortePagoCom;
//		if(fechaCortePagoCom!=null){
//			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
//			this.fechaCortePagoComString = sdf.format(fechaCortePagoCom);
//		}else{
//			this.fechaCortePagoComString = "";
//		}
	}
	public String getEstatusMovimiento() {
		return estatusMovimiento;
	}
	public void setEstatusMovimiento(String estatusMovimiento) {
		this.estatusMovimiento = estatusMovimiento;
	}
	public String getIdUsuarioIntegeg() {
		return idUsuarioIntegeg;
	}
	public void setIdUsuarioIntegeg(String idUsuarioIntegeg) {
		this.idUsuarioIntegeg = idUsuarioIntegeg;
	}
	@Temporal(TemporalType.DATE)
	public Date getFhIntegracion() {
		return fhIntegracion;
	}
	public void setFhIntegracion(Date fhIntegracion) {
		this.fhIntegracion = fhIntegracion;
//		if(fhIntegracion!=null){
//			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
//			this.fhIntegracionString = sdf.format(fhIntegracion);
//		}
	}
	@Temporal(TemporalType.DATE)
	public Date getFhAplicacion() {
		return fhAplicacion;
	}
	public void setFhAplicacion(Date fhAplicacion) {
		this.fhAplicacion = fhAplicacion;
//		if(fhAplicacion!=null){
//			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
//			this.fhAplicacionString = sdf.format(fhAplicacion);
//		}
	}
	public String getNaturalezaConcepto() {
		return naturalezaConcepto;
	}
	public void setNaturalezaConcepto(String naturalezaConcepto) {
		this.naturalezaConcepto = naturalezaConcepto;
	}
	public String getTipoMovtoCobranza() {
		return tipoMovtoCobranza;
	}
	public void setTipoMovtoCobranza(String tipoMovtoCobranza) {
		this.tipoMovtoCobranza = tipoMovtoCobranza;
	}
	@Temporal(TemporalType.DATE)
	public Date getfTransfDePv() {
		return fTransfDePv;
	}
	public void setfTransfDePv(Date fTransfDePv) {
		this.fTransfDePv = fTransfDePv;
//		if(fTransfDePv!=null){
//			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
//			this.fTransfDePvString = sdf.format(fTransfDePv);
//		}
	}
	public Long getIdTransacDePv() {
		return idTransacDePv;
	}
	public void setIdTransacDePv(Long idTransacDePv) {
		this.idTransacDePv = idTransacDePv;
	}
	public Long getIdTreansaccOrig() {
		return idTreansaccOrig;
	}
	public void setIdTreansaccOrig(Long idTreansaccOrig) {
		this.idTreansaccOrig = idTreansaccOrig;
	}
	public String getAgenteOrigen() {
		return agenteOrigen;
	}
	public void setAgenteOrigen(String agenteOrigen) {
		this.agenteOrigen = agenteOrigen;
	}
	public Long getIdCobertura() {
		return idCobertura;
	}
	public void setIdCobertura(Long idCobertura) {
		this.idCobertura = idCobertura;
	}
	public String getEsFacultativo() {
		return esFacultativo;
	}
	public void setEsFacultativo(String esFacultativo) {
		this.esFacultativo = esFacultativo;
	}
	public Long getAnioVigenciaPoliza() {
		return anioVigenciaPoliza;
	}
	public void setAnioVigenciaPoliza(Long anioVigenciaPoliza) {
		this.anioVigenciaPoliza = anioVigenciaPoliza;
	}
	public String getClaveSistAdmon() {
		return claveSistAdmon;
	}
	public void setClaveSistAdmon(String claveSistAdmon) {
		this.claveSistAdmon = claveSistAdmon;
	}
	public String getClaveExpCalcDiv() {
		return claveExpCalcDiv;
	}
	public void setClaveExpCalcDiv(String claveExpCalcDiv) {
		this.claveExpCalcDiv = claveExpCalcDiv;
	}
	public Long getImportePrimaDcp() {
		return importePrimaDcp;
	}
	public void setImportePrimaDcp(Long importePrimaDcp) {
		this.importePrimaDcp = importePrimaDcp;
	}
	public Long getImportePrimaTotal() {
		return importePrimaTotal;
	}
	public void setImportePrimaTotal(Long importePrimaTotal) {
		this.importePrimaTotal = importePrimaTotal;
	}
	public String getCvetCptoAco() {
		return cvetCptoAco;
	}
	public void setCvetCptoAco(String cvetCptoAco) {
		this.cvetCptoAco = cvetCptoAco;
	}
	public Long getIdConceptoO() {
		return idConceptoO;
	}
	public void setIdConceptoO(Long idConceptoO) {
		this.idConceptoO = idConceptoO;
	}
	public String getEsMasivo() {
		return esMasivo;
	}
	public void setEsMasivo(String esMasivo) {
		this.esMasivo = esMasivo;
	}
	public Double getImporteBaseCalculo() {
		return importeBaseCalculo;
	}
	public void setImporteBaseCalculo(Double importeBaseCalculo) {
		this.importeBaseCalculo = importeBaseCalculo;
	}
	public Double getPorcentajeComisionAgente() {
		return porcentajeComisionAgente;
	}
	public void setPorcentajeComisionAgente(Double porcentajeComisionAgente) {
		this.porcentajeComisionAgente = porcentajeComisionAgente;
	}
	public Long getNumMovtoManAgente() {
		return numMovtoManAgente;
	}
	public void setNumMovtoManAgente(Long numMovtoManAgente) {
		this.numMovtoManAgente = numMovtoManAgente;
	}
	public Double getImportePagoAntImptos() {
		return importePagoAntImptos;
	}
	public void setImportePagoAntImptos(Double importePagoAntImptos) {
		this.importePagoAntImptos = importePagoAntImptos;
	}
	public Double getPorcentajeIva() {
		return porcentajeIva;
	}
	public void setPorcentajeIva(Double porcentajeIva) {
		this.porcentajeIva = porcentajeIva;
	}
	public Double getPorcentageIvaRetenido() {
		return porcentageIvaRetenido;
	}
	public void setPorcentageIvaRetenido(Double porcentageIvaRetenido) {
		this.porcentageIvaRetenido = porcentageIvaRetenido;
	}
	public Double getPorcentajeIsrRet() {
		return porcentajeIsrRet;
	}
	public void setPorcentajeIsrRet(Double porcentajeIsrRet) {
		this.porcentajeIsrRet = porcentajeIsrRet;
	}
	public Double getImporteIva() {
		return importeIva;
	}
	public void setImporteIva(Double importeIva) {
		this.importeIva = importeIva;
	}
	public Double getImporteIvaRet() {
		return importeIvaRet;
	}
	public void setImporteIvaRet(Double importeIvaRet) {
		this.importeIvaRet = importeIvaRet;
	}
	public Double getImporteIsr() {
		return importeIsr;
	}
	public void setImporteIsr(Double importeIsr) {
		this.importeIsr = importeIsr;
	}
	public String getCveSdoImpto() {
		return cveSdoImpto;
	}
	public void setCveSdoImpto(String cveSdoImpto) {
		this.cveSdoImpto = cveSdoImpto;
	}
	public String getEsImpuesto() {
		return esImpuesto;
	}
	public void setEsImpuesto(String esImpuesto) {
		this.esImpuesto = esImpuesto;
	}
	public Long getAnioMesPago() {
		return anioMesPago;
	}
	public void setAnioMesPago(Long anioMesPago) {
		this.anioMesPago = anioMesPago;
	}
	public Long getIdCalculo() {
		return idCalculo;
	}
	public void setIdCalculo(Long idCalculo) {
		this.idCalculo = idCalculo;
	}
	
//	@Transient
	public String getFehcaCorteEdoctaString() {
		return fehcaCorteEdoctaString;
	}
	public void setFehcaCorteEdoctaString(String fehcaCorteEdoctaString) {
		this.fehcaCorteEdoctaString = fehcaCorteEdoctaString;
	}
//	@Transient
	public String getFechaMovimientoString() {
		return fechaMovimientoString;
	}
	public void setFechaMovimientoString(String fechaMovimientoString) {
		this.fechaMovimientoString = fechaMovimientoString;
	}
//	@Transient
	public String getFechavencimientorecString() {
		return fechavencimientorecString;
	}
	public void setFechavencimientorecString(String fechavencimientorecString) {
		this.fechavencimientorecString = fechavencimientorecString;
	}
//	@Transient
	public String getFechaCortePagoComString() {
		return fechaCortePagoComString;
	}
	public void setFechaCortePagoComString(String fechaCortePagoComString) {
		this.fechaCortePagoComString = fechaCortePagoComString;
	}
//	@Transient
	public String getFhIntegracionString() {
		return fhIntegracionString;
	}
	public void setFhIntegracionString(String fhIntegracionString) {
		this.fhIntegracionString = fhIntegracionString;
	}
//	@Transient
	public String getFhAplicacionString() {
		return fhAplicacionString;
	}
	public void setFhAplicacionString(String fhAplicacionString) {
		this.fhAplicacionString = fhAplicacionString;
	}
//	@Transient
	public String getfTransfDePvString() {
		return fTransfDePvString;
	}
	public void setfTransfDePvString(String fTransfDePvString) {
		this.fTransfDePvString = fTransfDePvString;
	}
	public String getDescripcionConcepto() {
		return descripcionConcepto;
	}
	public void setDescripcionConcepto(String descripcionConcepto) {
		this.descripcionConcepto = descripcionConcepto;
	}	
	
}
