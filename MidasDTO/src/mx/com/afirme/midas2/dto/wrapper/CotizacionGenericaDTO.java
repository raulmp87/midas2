package mx.com.afirme.midas2.dto.wrapper;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;

/**
 * Se diseño como un Wrapper para hacer "compatibles" las clases
 * CotizacionDTO y Cotizacion que son no-bitemporal y bitemporal respectivamente.
 * @author José Rodrigo Díaz Luján
 *
 */
public interface CotizacionGenericaDTO {
	public BigDecimal getIdFormaPago();
	public BigDecimal getIdMoneda();
	public List<IncisoCotizacionDTO> getIncisoCotizacionDTOs();
	public BigDecimal getIdToCotizacion();
}
