/**
 * 
 */
package mx.com.afirme.midas2.dto.wrapper;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.Cotizacion;

/**
 * @author admin
 *
 */
public class CotizacionDTOWrapper extends Cotizacion implements CotizacionGenericaDTO {

	private static final long serialVersionUID = 2402719537001693448L;
	private BitemporalCotizacion bitemporalCotizacion;
	private Cotizacion cotizacion;
	
	public CotizacionDTOWrapper(BitemporalCotizacion bc) {
		this.bitemporalCotizacion = bc;
		cotizacion = this.bitemporalCotizacion.getEmbedded();
	}
	
	/* (non-Javadoc)
	 * @see mx.com.afirme.midas2.dto.wrapper.CotizacionGenericaDTO#getIdFormaPago()
	 */
	public BigDecimal getIdFormaPago() {
		BigDecimal idFormaPago = null;
		FormaPagoDTO formaPagoDTO = cotizacion.getFormaPago();
		Integer formaPagoIdInt = formaPagoDTO.getIdFormaPago();
		idFormaPago = new BigDecimal(formaPagoIdInt);
		
		return idFormaPago;
	}

	/* (non-Javadoc)
	 * @see mx.com.afirme.midas2.dto.wrapper.CotizacionGenericaDTO#getIdMoneda()
	 */
	public BigDecimal getIdMoneda() {
		BigDecimal idMoneda = null;
		MonedaDTO monedaDTO = cotizacion.getMoneda();
		monedaDTO.getIdTcMoneda();
		return idMoneda;
	}

	/* (non-Javadoc)
	 * @see mx.com.afirme.midas2.dto.wrapper.CotizacionGenericaDTO#getIncisoCotizacionDTOs()
	 */
	public List<IncisoCotizacionDTO> getIncisoCotizacionDTOs() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.afirme.midas2.dto.wrapper.CotizacionGenericaDTO#getIdToCotizacion()
	 */
	public BigDecimal getIdToCotizacion() {
		// TODO Auto-generated method stub
		return null;
	}

}
