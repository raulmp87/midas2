package mx.com.afirme.midas2.dto.wrapper;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas2.domain.catalogos.VarModifPrima;

public class VariableModifPrimaComboDTO extends CacheableDTO {
	private static final long serialVersionUID = 1L;

	private VarModifPrima varModifPrima;

	public VariableModifPrimaComboDTO() {
	}
	
	public VariableModifPrimaComboDTO(VarModifPrima varModifPrima) {
		this.varModifPrima = varModifPrima;
	}

	@Override
	public Object getId() {
		if(varModifPrima != null && varModifPrima.getId() != null)
			return varModifPrima.getId().toString();
		return null;
	}

	@Override
	public String getDescription() {
		if(varModifPrima != null && varModifPrima.getGrupo() != null) {
			if (varModifPrima.getGrupo().getClaveTipoDetalle().intValue() == 0) { //Tipo Valor
				return varModifPrima.getValor();
			} else { //Tipo Rango
				return varModifPrima.getRangoMinimo() + " - " + varModifPrima.getRangoMaximo();
			}
		}
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((varModifPrima == null) ? 0 : varModifPrima.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VariableModifPrimaComboDTO other = (VariableModifPrimaComboDTO) obj;
		if (varModifPrima == null) {
			if (other.varModifPrima != null)
				return false;
		} else if (!varModifPrima.equals(other.varModifPrima))
			return false;
		return true;
	}

	public VarModifPrima getVarModifPrima() {
		return varModifPrima;
	}

	public void setVarModifPrima(VarModifPrima varModifPrima) {
		this.varModifPrima = varModifPrima;
	}

}
