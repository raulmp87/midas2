package mx.com.afirme.midas2.dto.wrapper;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.catalogos.tiposerviciovehiculo.TipoServicioVehiculoDTO;

public class TipoServicioVehiculoComboDTO extends CacheableDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5512227824319260667L;
	
	private TipoServicioVehiculoDTO tipoServicioVehiculoDTO; 

	@Override
	public Object getId() {
		if(tipoServicioVehiculoDTO!=null)
				return tipoServicioVehiculoDTO.getCodigoTipoServicioVehiculo();
		return null;
	}

	@Override
	public String getDescription() {
		if(tipoServicioVehiculoDTO!=null)
			return tipoServicioVehiculoDTO.getDescripcionTipoServVehiculo();
		return null;
	}

	@Override
	public boolean equals(Object object) {
		if(object instanceof TipoServicioVehiculoComboDTO && ((TipoServicioVehiculoComboDTO)object).getTipoServicioVehiculoDTO() != null &&
				getTipoServicioVehiculoDTO() != null){
			return getTipoServicioVehiculoDTO().equals(((TipoServicioVehiculoComboDTO)object).getTipoServicioVehiculoDTO());
		}
		return false;
	}

	public TipoServicioVehiculoDTO getTipoServicioVehiculoDTO() {
		return tipoServicioVehiculoDTO;
	}

	public void setTipoServicioVehiculoDTO(
			TipoServicioVehiculoDTO tipoServicioVehiculoDTO) {
		this.tipoServicioVehiculoDTO = tipoServicioVehiculoDTO;
	}
	
	public TipoServicioVehiculoComboDTO(){}
	
	public TipoServicioVehiculoComboDTO(TipoServicioVehiculoDTO tipoServicioVehiculoDTO){
		this.tipoServicioVehiculoDTO = tipoServicioVehiculoDTO;
	}

}
