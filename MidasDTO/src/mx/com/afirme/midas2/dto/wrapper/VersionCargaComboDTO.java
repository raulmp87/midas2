package mx.com.afirme.midas2.dto.wrapper;

import java.math.BigDecimal;

import mx.com.afirme.midas.base.CacheableDTO;

public class VersionCargaComboDTO extends CacheableDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7938151615685463640L;
	
	private BigDecimal idVersionCarga;

	@Override
	public Object getId() {
		if(idVersionCarga != null)
				return idVersionCarga;
		return null;
	}

	@Override
	public String getDescription() {
		if(idVersionCarga != null)
				return idVersionCarga.toString();
		return null;
	}

	@Override
	public boolean equals(Object object) {
		if(object instanceof VersionCargaComboDTO && ((VersionCargaComboDTO)object).getId() != null &&
				getId() != null){
			return getId().equals(((VersionCargaComboDTO)object).getId());
		}
		return false;
	}
	
	public BigDecimal getIdVersionCarga() {
		return idVersionCarga;
	}

	public void setIdVersionCarga(BigDecimal idVersionCarga) {
		this.idVersionCarga = idVersionCarga;
	}

	public VersionCargaComboDTO(BigDecimal idVersionCarga){
		
		this.idVersionCarga = idVersionCarga;
	}
	
	public VersionCargaComboDTO(){
		
	}

}
