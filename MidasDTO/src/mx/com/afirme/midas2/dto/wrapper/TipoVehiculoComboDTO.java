package mx.com.afirme.midas2.dto.wrapper;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;

public class TipoVehiculoComboDTO extends CacheableDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7938151615685463640L;
	
	private TipoVehiculoDTO tipoVehiculoDTO;

	@Override
	public Object getId() {
		if(tipoVehiculoDTO != null)
				return tipoVehiculoDTO.getId();
		return null;
	}

	@Override
	public String getDescription() {
		if(tipoVehiculoDTO != null)
				return tipoVehiculoDTO.getDescripcionTipoVehiculo();
		return null;
	}

	@Override
	public boolean equals(Object object) {
		if(object instanceof TipoVehiculoComboDTO && ((TipoVehiculoComboDTO)object).getTipoVehiculoDTO() != null &&
				getTipoVehiculoDTO() != null){
			return getTipoVehiculoDTO().equals(((TipoVehiculoComboDTO)object).getTipoVehiculoDTO());
		}
		return false;
	}

	public void setTipoVehiculoDTO(TipoVehiculoDTO tipoVehiculoDTO) {
		this.tipoVehiculoDTO = tipoVehiculoDTO;
	}

	public TipoVehiculoDTO getTipoVehiculoDTO() {
		return tipoVehiculoDTO;
	}
	
	public TipoVehiculoComboDTO(TipoVehiculoDTO tipoVehiculoDTO){
		
		this.tipoVehiculoDTO = tipoVehiculoDTO;
	}
	public TipoVehiculoComboDTO(){
		
	}

}
