package mx.com.afirme.midas2.dto.wrapper;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;

public class TipoUsoVehiculoComboDTO extends CacheableDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -426109532819952551L;
	
	private TipoUsoVehiculoDTO tipoUsoVehiculoDTO;

	@Override
	public Object getId() {
		if(tipoUsoVehiculoDTO!=null)
			return tipoUsoVehiculoDTO.getCodigoTipoUsoVehiculo();
		return null;
	}

	@Override
	public String getDescription() {
		if(tipoUsoVehiculoDTO!=null)
				return tipoUsoVehiculoDTO.getDescripcionTipoUsoVehiculo();
		return null;
	}

	@Override
	public boolean equals(Object object) {
		if(object instanceof TipoUsoVehiculoComboDTO && ((TipoUsoVehiculoComboDTO)object).getTipoUsoVehiculoDTO() != null &&
				getTipoUsoVehiculoDTO() != null){
			return getTipoUsoVehiculoDTO().equals(((TipoUsoVehiculoComboDTO)object).getTipoUsoVehiculoDTO());
		}
		return false;
	}

	public void setTipoUsoVehiculoDTO(TipoUsoVehiculoDTO tipoUsoVehiculoDTO) {
		this.tipoUsoVehiculoDTO = tipoUsoVehiculoDTO;
	}

	public TipoUsoVehiculoDTO getTipoUsoVehiculoDTO() {
		return tipoUsoVehiculoDTO;
	}
	
	public TipoUsoVehiculoComboDTO(){}
	
	public TipoUsoVehiculoComboDTO(TipoUsoVehiculoDTO tipoUsoVehiculoDTO){
		this.tipoUsoVehiculoDTO = tipoUsoVehiculoDTO;	
	}

}
