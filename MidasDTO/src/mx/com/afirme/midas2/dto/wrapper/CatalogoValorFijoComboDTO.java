package mx.com.afirme.midas2.dto.wrapper;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;

public class CatalogoValorFijoComboDTO extends CacheableDTO {
	private static final long serialVersionUID = 1L;

	private CatalogoValorFijoDTO catalogoValorFijoDTO;
	
	@Override
	public Object getId() {
		if(catalogoValorFijoDTO != null)
			return catalogoValorFijoDTO.getId().getIdDato();
		return null;
	}

	@Override
	public String getDescription() {
		if(catalogoValorFijoDTO != null)
			return catalogoValorFijoDTO.getDescripcion();
		return null;
	}

	@Override
	public boolean equals(Object object) {
		if(object instanceof CatalogoValorFijoComboDTO && ((CatalogoValorFijoComboDTO)object).getCatalogoValorFijoDTO() != null &&
				getCatalogoValorFijoDTO() != null){
			return getCatalogoValorFijoDTO().equals(((CatalogoValorFijoComboDTO)object).getCatalogoValorFijoDTO());
		}
		return false;
	}

	public CatalogoValorFijoDTO getCatalogoValorFijoDTO() {
		return catalogoValorFijoDTO;
	}

	public void setCatalogoValorFijoDTO(CatalogoValorFijoDTO catalogoValorFijoDTO) {
		this.catalogoValorFijoDTO = catalogoValorFijoDTO;
	}

	public CatalogoValorFijoComboDTO(CatalogoValorFijoDTO catalogoValorFijoDTO) {
		this.catalogoValorFijoDTO = catalogoValorFijoDTO;
	}
	
	public CatalogoValorFijoComboDTO() {
	}
	

}
