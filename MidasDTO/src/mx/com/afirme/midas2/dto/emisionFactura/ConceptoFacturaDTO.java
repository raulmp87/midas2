package mx.com.afirme.midas2.dto.emisionFactura;

import java.io.Serializable;
import java.math.BigDecimal;

public class ConceptoFacturaDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8442646187126942305L;
	
	private String clave;
	private Integer cantidad;
	private String unidadMedida;
	private String descripcion;
	private BigDecimal precioUnitario;
	private BigDecimal importe;
	
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public String getUnidadMedida() {
		return unidadMedida;
	}
	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public BigDecimal getPrecioUnitario() {
		return precioUnitario;
	}
	public void setPrecioUnitario(BigDecimal precioUnitario) {
		this.precioUnitario = precioUnitario;
	}
	public BigDecimal getImporte() {
		return importe;
	}
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}
}
