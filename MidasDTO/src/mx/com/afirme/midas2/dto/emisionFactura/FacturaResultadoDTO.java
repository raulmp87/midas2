package mx.com.afirme.midas2.dto.emisionFactura;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import mx.com.afirme.midas2.annotation.Exportable;

public class FacturaResultadoDTO implements Serializable {

	
	private Long emisionId;
	private String estatusFactura;
	private String estatusFacturaDesc;
	private String estatusIngresoDesc;
	private String tipoRecuperacionDesc;
	private String noSiniestro;
	private String oficinaDesc;
	private BigDecimal totalFacturado;
	private String noFactura;
	private String tipoRecuperacion;
	private Date fechaCancelacion;
	private Date fechaFacturacion;
	private Long antiguedadFactura;
	private String nombreRazonSocial;
	private String facturadaPor;
	
	//////////////////////////////////////////
	private String estatusIngreso;
	private String oficina;
	private String estatusFacturaCodigo;
	
	

	public FacturaResultadoDTO(){

	}

	public FacturaResultadoDTO(
			Long emisionId,
			String estatusFactura,
			String estatusFacturaDesc,
			String estatusIngresoDesc,
			String tipoRecuperacionDesc,
			String noSiniestro,
			String oficinaDesc,
			BigDecimal totalFacturado, 
			String noFactura, 
			String tipoRecuperacion, 
			Date fechaCancelacion, 
			Date fechaFacturacion,
			Long antiguedadFactura, 
			String nombreRazonSocial, 
			String facturadaPor
		) {
		super();
		this.emisionId = emisionId;
		this.estatusFactura = estatusFactura;
		this.estatusFacturaDesc = estatusFacturaDesc;
		this.estatusIngresoDesc = estatusIngresoDesc;
		this.tipoRecuperacionDesc = tipoRecuperacionDesc;
		this.noSiniestro = noSiniestro;
		this.oficinaDesc = oficinaDesc;
		this.totalFacturado = totalFacturado;
		this.noFactura = noFactura;
		this.tipoRecuperacion = tipoRecuperacion;
		this.fechaCancelacion = fechaCancelacion;
		this.fechaFacturacion = fechaFacturacion;
		this.antiguedadFactura = antiguedadFactura;
		
		
		
		this.facturadaPor = facturadaPor;
		
		this.nombreRazonSocial = nombreRazonSocial;
		this.oficina    = oficina;
	}



	/**
	 * @param antiguedadFactura the antiguedadFactura to set
	 */
	public void setAntiguedadFactura(Long antiguedadFactura) {
		this.antiguedadFactura = antiguedadFactura;
	}

	/**
	 * @return the antiguedadFactura
	 */
	public Long getAntiguedadFactura() {
		return antiguedadFactura;
	}

	/**
	 * @param estatusFactura the estatusFactura to set
	 */
	public void setEstatusFactura(String estatusFactura) {
		this.estatusFactura = estatusFactura;
	}

	/**
	 * @return the estatusFactura
	 */
	public String getEstatusFactura() {
		return estatusFactura;
	}

	/**
	 * @param estatusFacturaDesc the estatusFacturaDesc to set
	 */
	public void setEstatusFacturaDesc(String estatusFacturaDesc) {
		this.estatusFacturaDesc = estatusFacturaDesc;
	}

	/**
	 * @return the estatusFacturaDesc
	 */
	@Exportable(columnName="Estatus Factura", columnOrder=1 )
	public String getEstatusFacturaDesc() {
		return estatusFacturaDesc;
	}

	/**
	 * @param estatusIngreso the estatusIngreso to set
	 */
	public void setEstatusIngreso(String estatusIngreso) {
		this.estatusIngreso = estatusIngreso;
	}

	/**
	 * @return the estatusIngreso
	 */
	public String getEstatusIngreso() {
		return estatusIngreso;
	}

	/**
	 * @param facturadaPor the facturadaPor to set
	 */
	public void setFacturadaPor(String facturadaPor) {
		this.facturadaPor = facturadaPor;
	}

	/**
	 * @return the facturadaPor
	 */
	@Exportable(columnName="Facturada Por", columnOrder=6 )
	public String getFacturadaPor() {
		return facturadaPor;
	}

	/**
	 * @param fechaCancelacion the fechaCancelacion to set
	 */
	public void setFechaCancelacion(Date fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}

	/**
	 * @return the fechaCancelacion
	 */
	@Exportable(columnName="Fecha Cancelación", columnOrder=10 )
	public Date getFechaCancelacion() {
		return fechaCancelacion;
	}

	/**
	 * @param fechaFacturacion the fechaFacturacion to set
	 */
	public void setFechaFacturacion(Date fechaFacturacion) {
		this.fechaFacturacion = fechaFacturacion;
	}

	/**
	 * @return the fechaFacturacion
	 */
	@Exportable(columnName="Fecha Facturación", columnOrder=9 )
	public Date getFechaFacturacion() {
		return fechaFacturacion;
	}

	/**
	 * @param noFactura the noFactura to set
	 */
	public void setNoFactura(String noFactura) {
		this.noFactura = noFactura;
	}

	/**
	 * @return the noFactura
	 */
	@Exportable(columnName="No. Factura", columnOrder=0 )
	public String getNoFactura() {
		return noFactura;
	}

	/**
	 * @param nombreRazonSocial the nombreRazonSocial to set
	 */
	public void setNombreRazonSocial(String nombreRazonSocial) {
		this.nombreRazonSocial = nombreRazonSocial;
	}

	/**
	 * @return the nombreRazonSocial
	 */
	@Exportable(columnName="Nombre/Razón Social", columnOrder=5 )
	public String getNombreRazonSocial() {
		return nombreRazonSocial;
	}

	/**
	 * @param noSiniestro the noSiniestro to set
	 */
	public void setNoSiniestro(String noSiniestro) {
		this.noSiniestro = noSiniestro;
	}

	/**
	 * @return the noSiniestro
	 */
	@Exportable(columnName="No. Siniestro", columnOrder=4 )
	public String getNoSiniestro() {
		return noSiniestro;
	}

	/**
	 * @param tipoRecuperacion the tipoRecuperacion to set
	 */
	public void setTipoRecuperacion(String tipoRecuperacion) {
		this.tipoRecuperacion = tipoRecuperacion;
	}

	/**
	 * @return the tipoRecuperacion
	 */
	public String getTipoRecuperacion() {
		return tipoRecuperacion;
	}

	/**
	 * @param totalFacturado the totalFacturado to set
	 */
	public void setTotalFacturado(BigDecimal totalFacturado) {
		this.totalFacturado = totalFacturado;
	}

	/**
	 * @return the totalFacturado
	 */
	@Exportable(columnName="Total Facturado", columnOrder=7, format="$#,##0.00")
	public BigDecimal getTotalFacturado() {
		return totalFacturado;
	}

	public String getOficina() {
		return oficina;
	}

	public void setOficina(String oficina) {
		this.oficina = oficina;
	}

	public String getEstatusFacturaCodigo() {
		return estatusFacturaCodigo;
	}

	public void setEstatusFacturaCodigo(String estatusFacturaCodigo) {
		this.estatusFacturaCodigo = estatusFacturaCodigo;
	}

	@Exportable(columnName="Tipo de Recuperación", columnOrder=2 )
	public String getTipoRecuperacionDesc() {
		return tipoRecuperacionDesc;
	}

	
	public void setTipoRecuperacionDesc(String tipoRecuperacionDesc) {
		this.tipoRecuperacionDesc = tipoRecuperacionDesc;
	}

	@Exportable(columnName="Oficina", columnOrder=3 )
	public String getOficinaDesc() {
		return oficinaDesc;
	}

	
	public void setOficinaDesc(String oficinaDesc) {
		this.oficinaDesc = oficinaDesc;
	}

	@Exportable(columnName="Estatus Ingreso", columnOrder=8 )
	public String getEstatusIngresoDesc() {
		return estatusIngresoDesc;
	}

	
	public void setEstatusIngresoDesc(String estatusIngresoDesc) {
		this.estatusIngresoDesc = estatusIngresoDesc;
	}

	public Long getEmisionId() {
		return emisionId;
	}

	public void setEmisionId(Long emisionId) {
		this.emisionId = emisionId;
	}
}