package mx.com.afirme.midas2.dto.emisionFactura;

import java.io.Serializable;
import java.util.Date;

public class FiltroFacturaDTO implements Serializable {

	private String concepto;
	private String estatusFactura;
	private String estatusIngreso;
	private String estatusRecuperacion;
	private Date fechaCancelacionDe;
	private Date fechaCancelacionHasta;
	private Date fechaFacturacionDe;
	private Date fechaFacturacionHasta;
	private String noFactura;
	private String nombreRazonSocial;
	private Long oficina;
	private String facturadoPor;
	private String tipoRecuperacionDesc;

	public FiltroFacturaDTO(){

	}

	/**
	 * @param concepto the concepto to set
	 */
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	/**
	 * @return the concepto
	 */
	public String getConcepto() {
		return concepto;
	}

	/**
	 * @param estatusFactura the estatusFactura to set
	 */
	public void setEstatusFactura(String estatusFactura) {
		this.estatusFactura = estatusFactura;
	}

	/**
	 * @return the estatusFactura
	 */
	public String getEstatusFactura() {
		return estatusFactura;
	}

	/**
	 * @param estatusIngreso the estatusIngreso to set
	 */
	public void setEstatusIngreso(String estatusIngreso) {
		this.estatusIngreso = estatusIngreso;
	}

	/**
	 * @return the estatusIngreso
	 */
	public String getEstatusIngreso() {
		return estatusIngreso;
	}


	/**
	 * @param fechaCancelacionDe the fechaCancelacionDe to set
	 */
	public void setFechaCancelacionDe(Date fechaCancelacionDe) {
		this.fechaCancelacionDe = fechaCancelacionDe;
	}

	/**
	 * @return the fechaCancelacionDe
	 */
	public Date getFechaCancelacionDe() {
		return fechaCancelacionDe;
	}

	/**
	 * @param fechaCancelacionHasta the fechaCancelacionHasta to set
	 */
	public void setFechaCancelacionHasta(Date fechaCancelacionHasta) {
		this.fechaCancelacionHasta = fechaCancelacionHasta;
	}

	/**
	 * @return the fechaCancelacionHasta
	 */
	public Date getFechaCancelacionHasta() {
		return fechaCancelacionHasta;
	}

	/**
	 * @param fechaFacturacionDe the fechaFacturacionDe to set
	 */
	public void setFechaFacturacionDe(Date fechaFacturacionDe) {
		this.fechaFacturacionDe = fechaFacturacionDe;
	}

	/**
	 * @return the fechaFacturacionDe
	 */
	public Date getFechaFacturacionDe() {
		return fechaFacturacionDe;
	}

	/**
	 * @param fechaFacturacionHasta the fechaFacturacionHasta to set
	 */
	public void setFechaFacturacionHasta(Date fechaFacturacionHasta) {
		this.fechaFacturacionHasta = fechaFacturacionHasta;
	}

	/**
	 * @return the fechaFacturacionHasta
	 */
	public Date getFechaFacturacionHasta() {
		return fechaFacturacionHasta;
	}

	/**
	 * @param noFactura the noFactura to set
	 */
	public void setNoFactura(String noFactura) {
		this.noFactura = noFactura;
	}

	/**
	 * @return the noFactura
	 */
	public String getNoFactura() {
		return noFactura;
	}

	/**
	 * @param nombreRazonSocial the nombreRazonSocial to set
	 */
	public void setNombreRazonSocial(String nombreRazonSocial) {
		this.nombreRazonSocial = nombreRazonSocial;
	}

	/**
	 * @return the nombreRazonSocial
	 */
	public String getNombreRazonSocial() {
		return nombreRazonSocial;
	}

	/**
	 * @param oficina the oficina to set
	 */
	public void setOficina(Long oficina) {
		this.oficina = oficina;
	}

	/**
	 * @return the oficina
	 */
	public Long getOficina() {
		return oficina;
	}

	public String getEstatusRecuperacion() {
		return estatusRecuperacion;
	}

	public void setEstatusRecuperacion(String estatusRecuperacion) {
		this.estatusRecuperacion = estatusRecuperacion;
	}

	public String getFacturadoPor() {
		return facturadoPor;
	}

	public void setFacturadoPor(String facturadoPor) {
		this.facturadoPor = facturadoPor;
	}

	public String getTipoRecuperacionDesc() {
		return tipoRecuperacionDesc;
	}

	public void setTipoRecuperacionDesc(String tipoRecuperacionDesc) {
		this.tipoRecuperacionDesc = tipoRecuperacionDesc;
	}
}