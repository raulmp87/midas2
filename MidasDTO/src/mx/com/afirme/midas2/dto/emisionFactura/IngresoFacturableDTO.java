package mx.com.afirme.midas2.dto.emisionFactura;

import mx.com.afirme.midas2.annotation.Exportable;

public class IngresoFacturableDTO {

	private Long 	ingresoId;
	private Long 	numeroIngreso;
	private String 	tipoRecuperacion;
	private String 	tipoRecuperacionDesc;
	private String 	estatusIngreso;
	private String 	estatusIngresoDesc;
	private Boolean facturar = true;
	private String 	estatusFactura;
	private String 	estatusFacturaDesc;
	private String  detalleRechazo;
	private Long	emisionFacturaId;
	private String folioFactura;
	
	public void setNumeroIngreso(Long numeroIngreso) {
		this.numeroIngreso = numeroIngreso;
	}
	public Long getNumeroIngreso() {
		return numeroIngreso;
	}
	public void setTipoRecuperacion(String tipoRecuperacion) {
		this.tipoRecuperacion = tipoRecuperacion;
	}
	public String getTipoRecuperacion() {
		return tipoRecuperacion;
	}
	public void setTipoRecuperacionDesc(String tipoRecuperacionDesc) {
		this.tipoRecuperacionDesc = tipoRecuperacionDesc;
	}
	public String getTipoRecuperacionDesc() {
		return tipoRecuperacionDesc;
	}
	public void setEstatusIngreso(String estatusIngreso) {
		this.estatusIngreso = estatusIngreso;
	}
	public String getEstatusIngreso() {
		return estatusIngreso;
	}
	public void setEstatusIngresoDesc(String estatusIngresoDesc) {
		this.estatusIngresoDesc = estatusIngresoDesc;
	}
	public String getEstatusIngresoDesc() {
		return estatusIngresoDesc;
	}
	public void setFacturar(Boolean facturar) {
		this.facturar = facturar;
	}
	public Boolean getFacturar() {
		return facturar;
	}
	public void setEstatusFactura(String estatusFactura) {
		this.estatusFactura = estatusFactura;
	}
	public String getEstatusFactura() {
		return estatusFactura;
	}
	public void setEstatusFacturaDesc(String estatusFacturaDesc) {
		this.estatusFacturaDesc = estatusFacturaDesc;
	}
	public String getEstatusFacturaDesc() {
		return estatusFacturaDesc;
	}
	public void setDetalleRechazo(String detalleRechazo) {
		this.detalleRechazo = detalleRechazo;
	}
	public String getDetalleRechazo() {
		return detalleRechazo;
	}
	public void setIngresoId(Long ingresoId) {
		this.ingresoId = ingresoId;
	}
	public Long getIngresoId() {
		return ingresoId;
	}
	/**
	 * @param emisionFacturaId the emisionFacturaId to set
	 */
	public void setEmisionFacturaId(Long emisionFacturaId) {
		this.emisionFacturaId = emisionFacturaId;
	}
	/**
	 * @return the emisionFacturaId
	 */
	public Long getEmisionFacturaId() {
		return emisionFacturaId;
	}
	public String getFolioFactura() {
		return folioFactura;
	}
	public void setFolioFactura(String folioFactura) {
		this.folioFactura = folioFactura;
	}

}