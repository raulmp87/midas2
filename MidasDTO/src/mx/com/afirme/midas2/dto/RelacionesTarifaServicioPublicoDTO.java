package mx.com.afirme.midas2.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.domain.tarifa.TarifaServicioPublico;
import mx.com.afirme.midas2.domain.tarifa.TarifaServicioPublicoDeduciblesAd;
import mx.com.afirme.midas2.domain.tarifa.TarifaServicioPublicoSumaAseguradaAd;

public class RelacionesTarifaServicioPublicoDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2502718601684777597L;
	
	private List<TarifaServicioPublico> obligatorias = new ArrayList<TarifaServicioPublico>(); 
	private List<TarifaServicioPublico> adicionales = new ArrayList<TarifaServicioPublico>(); 
	private List<TarifaServicioPublicoSumaAseguradaAd> sumasAseguradasAdicionales = new ArrayList<TarifaServicioPublicoSumaAseguradaAd>();
	private List<TarifaServicioPublicoDeduciblesAd> deduciblesAdicionales = new ArrayList<TarifaServicioPublicoDeduciblesAd>();
	
	public List<TarifaServicioPublico> getObligatorias() {
		return obligatorias;
	}
	public void setObligatorias(List<TarifaServicioPublico> obligatorias) {
		this.obligatorias = obligatorias;
	}
	public List<TarifaServicioPublico> getAdicionales() {
		return adicionales;
	}
	public void setAdicionales(List<TarifaServicioPublico> adicionales) {
		this.adicionales = adicionales;
	}
	public List<TarifaServicioPublicoSumaAseguradaAd> getSumasAseguradasAdicionales() {
		return sumasAseguradasAdicionales;
	}
	public void setSumasAseguradasAdicionales(
			List<TarifaServicioPublicoSumaAseguradaAd> sumasAseguradasAdicionales) {
		this.sumasAseguradasAdicionales = sumasAseguradasAdicionales;
	}
	public List<TarifaServicioPublicoDeduciblesAd> getDeduciblesAdicionales() {
		return deduciblesAdicionales;
	}
	public void setDeduciblesAdicionales(
			List<TarifaServicioPublicoDeduciblesAd> deduciblesAdicionales) {
		this.deduciblesAdicionales = deduciblesAdicionales;
	}
}
