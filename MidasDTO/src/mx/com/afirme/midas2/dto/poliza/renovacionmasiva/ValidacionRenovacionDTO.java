package mx.com.afirme.midas2.dto.poliza.renovacionmasiva;

import java.io.Serializable;
import java.math.BigDecimal;

public class ValidacionRenovacionDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3202383572709416278L;

	private BigDecimal idToPoliza;
	private String numeroPolizaFormateada;
	private String mensajeError;
	
	
	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}
	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}
	public void setNumeroPolizaFormateada(String numeroPolizaFormateada) {
		this.numeroPolizaFormateada = numeroPolizaFormateada;
	}
	public String getNumeroPolizaFormateada() {
		return numeroPolizaFormateada;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	public String getMensajeError() {
		return mensajeError;
	}
}
