package mx.com.afirme.midas2.dto.pago;

import java.io.Serializable;
import java.math.BigDecimal;

public class ReciboDTO implements Serializable {

	private static final long serialVersionUID = -6005542986640983867L;
	
	private Long idRecibo;
	
	private String referencia;
	
	private Integer numConsecutivo;
	
	private Double cantidad;
	
	private String fechaVencimiento;
	
	private BigDecimal idCotizacion;
	
	private FolioReciboDTO folioRecibo;
	
	private String ramo;
	
	private BigDecimal idToCotizacion;
	
	private BigDecimal idToSeccion;
	
	private BigDecimal idInciso;

	public Long getIdRecibo() {
		return idRecibo;
	}

	public void setIdRecibo(Long idRecibo) {
		this.idRecibo = idRecibo;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public Integer getNumConsecutivo() {
		return numConsecutivo;
	}

	public void setNumConsecutivo(Integer numConsecutivo) {
		this.numConsecutivo = numConsecutivo;
	}

	public Double getCantidad() {
		return cantidad;
	}

	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}

	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public BigDecimal getIdCotizacion() {
		return idCotizacion;
	}

	public void setIdCotizacion(BigDecimal idCotizacion) {
		this.idCotizacion = idCotizacion;
	}

	public String getRamo() {
		return ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}	
	
	public FolioReciboDTO getFolioRecibo() {
		return folioRecibo;
	}

	public void setFolioRecibo(FolioReciboDTO folioRecibo) {
		this.folioRecibo = folioRecibo;
	}

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	public BigDecimal getIdInciso() {
		return idInciso;
	}

	public void setIdInciso(BigDecimal idInciso) {
		this.idInciso = idInciso;
	}
	
}
