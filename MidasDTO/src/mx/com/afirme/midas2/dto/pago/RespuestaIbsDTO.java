package mx.com.afirme.midas2.dto.pago;

import java.io.Serializable;

public class RespuestaIbsDTO implements Serializable {
	
	private static final long serialVersionUID = 5448184028307850367L;

	private String ibsNumAutorizacion;
	
	private String codigoErrorIBS;
	
	private String descripcionErrorIBS;
	
	public RespuestaIbsDTO(){
		this.codigoErrorIBS = "0";
	}
	
	public RespuestaIbsDTO(String codigoErrorIBS, String descripcionErrorIBS){
		this.codigoErrorIBS = codigoErrorIBS;
		this.descripcionErrorIBS = descripcionErrorIBS;
	} 

	public String getIbsNumAutorizacion() {
		return ibsNumAutorizacion;
	}

	public void setIbsNumAutorizacion(String ibsNumAutorizacion) {
		this.ibsNumAutorizacion = ibsNumAutorizacion;
	}

	public String getCodigoErrorIBS() {
		return codigoErrorIBS;
	}

	public void setCodigoErrorIBS(String codigoErrorIBS) {
		this.codigoErrorIBS = codigoErrorIBS;
	}

	public String getDescripcionErrorIBS() {
		return descripcionErrorIBS;
	}

	public void setDescripcionErrorIBS(String descripcionErrorIBS) {
		this.descripcionErrorIBS = descripcionErrorIBS;
	}	

	public int getCodigoRespuesta(){
		return Integer.valueOf(this.codigoErrorIBS);
	}
	
	public boolean esValido(){
		if(getCodigoRespuesta() == 0){
			return true;
		}
		return false;
	}
}
