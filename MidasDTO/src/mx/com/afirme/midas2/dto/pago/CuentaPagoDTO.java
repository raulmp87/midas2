package mx.com.afirme.midas2.dto.pago;

import java.io.Serializable;
import java.math.BigDecimal;

import mx.com.afirme.midas2.domain.catalogos.Domicilio;

public class CuentaPagoDTO implements Serializable {
	
	private static final long serialVersionUID = 3750257907095254064L;
	public static final int DOMICILIACION = 8;
	public static final int TARJETA_CREDITO = 4;
	public static final int CUENTA_AFIRME = 1386;
	public static final int AMERICAN_EXPRESS = 6;
	
	public static enum TipoCobro{
		EFECTIVO((short)15),
		DOMICILIACION((short)8),
		TARJETA_CREDITO((short)4),
		CUENTA_AFIRME((short)1386),
		AMERICAN_EXPRESS((short)6);
		
		private short valor;
		
		TipoCobro(short id){
			valor = id;
		}
		
		public short valor(){
			return valor;
		}		
	}

	public static enum TipoCuenta{
		DEBITO((short)1),
		TARJETA_CREDITO((short)2),
		CLABE((short)3),
		CUENTA((short)4);
		
		private short valor;
		
		TipoCuenta(short id){
			valor = id;
		}
		
		public short valor(){
			return valor;
		}
	}
	
	public static enum TipoTarjeta{
		VISA("VS"),
		MASTERCARD("MC"),
		AMERICAN_EXPRESS("AMEX"),
		CREDIT_CARD("TARJETA DE CRED");

		private String tipo;
		
		TipoTarjeta(String tipo){
			this.tipo = tipo;
		}
		
		public String valor(){
			return tipo;
		}
	}
	
	private TipoCobro tipoConductoCobro;
	
	private BigDecimal  idCliente;
	
	private Long idConductoCliente;
	
	private Long idBanco;
	
	private String cuenta;
	
	private String tarjetaHabiente;
	
	private Domicilio domicilio;
	
	private TipoCuenta tipoCuenta;
	
	private String tipoPersona;
	
	private String telefono;
	
	private String correo;
	
	private String comentarios;
	
	private String rfc;
	
	private String tipoPromocion;
	
	private String codigoSeguridad;
	
	private String fechaVencimiento;
	
	private Integer diaPago;
	
	private TipoTarjeta tipoTarjeta;
	
	private String cuentaNoEncriptada;
	
	public CuentaPagoDTO(){
		new CuentaPagoDTO(TipoCobro.EFECTIVO);		
	}
	
	public CuentaPagoDTO(TipoCobro tipoCobro){
		if(tipoCobro != null){
			this.tipoConductoCobro = tipoCobro;
		}else{
			this.tipoConductoCobro = TipoCobro.EFECTIVO;
		}
	}
	

	public TipoTarjeta getTipoTarjeta() {
		return tipoTarjeta;
	}

	public void setTipoTarjeta(TipoTarjeta tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	public Long getIdBanco() {
		return idBanco;
	}

	public void setIdBanco(Long idBanco) {
		this.idBanco = idBanco;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public String getTarjetaHabiente() {
		return tarjetaHabiente;
	}

	public void setTarjetaHabiente(String tarjetaHabiente) {
		this.tarjetaHabiente = tarjetaHabiente;
	}

	public Domicilio getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(Domicilio domicilio) {
		this.domicilio = domicilio;
	}

	public TipoCuenta getTipoCuenta() {
		return tipoCuenta;
	}

	public void setTipoCuenta(TipoCuenta tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getTipoPromocion() {
		return tipoPromocion;
	}

	public void setTipoPromocion(String tipoPromocion) {
		this.tipoPromocion = tipoPromocion;
	}

	public BigDecimal getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(BigDecimal idCliente) {
		this.idCliente = idCliente;
	}

	public String getCodigoSeguridad() {
		return codigoSeguridad;
	}

	public void setCodigoSeguridad(String codigoSeguridad) {
		this.codigoSeguridad = codigoSeguridad;
	}

	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public Long getIdConductoCliente() {
		return idConductoCliente;
	}

	public void setIdConductoCliente(Long idConductoCliente) {
		this.idConductoCliente = idConductoCliente;
	}

	public Integer getDiaPago() {
		return diaPago;
	}

	public void setDiaPago(Integer diaPago) {
		this.diaPago = diaPago;
	}


	public TipoCobro getTipoConductoCobro() {
		return tipoConductoCobro;
	}


	public void setTipoConductoCobro(TipoCobro tipoConductoCobro) {
		this.tipoConductoCobro = tipoConductoCobro;
	}

	public String getCuentaNoEncriptada() {
		return cuentaNoEncriptada;
	}

	public void setCuentaNoEncriptada(String cuentaNoEncriptada) {
		this.cuentaNoEncriptada = cuentaNoEncriptada;
	}
	
}
