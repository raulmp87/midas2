package mx.com.afirme.midas2.dto.pago;

public class CuentaPagoTarjetaCreditoDTO extends CuentaPagoDTO {

	public CuentaPagoTarjetaCreditoDTO(TipoCobro tipoCobro) {
		super(tipoCobro);
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 214512541533054870L;
	
	public static enum TipoTarjeta{
		VISA("VS"),
		MASTERCARD("MC");
		
		private String tipo;
		
		TipoTarjeta(String tipo){
			this.tipo = tipo;
		}
		
		public String valor(){
			return tipo;
		}
	}

	private String codigoSeguridad;
	
	private String fechaVencimiento;
	
	private TipoTarjeta tipoTarjeta;

	public String getCodigoSeguridad() {
		return codigoSeguridad;
	}

	public void setCodigoSeguridad(String codigoSeguridad) {
		this.codigoSeguridad = codigoSeguridad;
	}
/*
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public TipoTarjeta getTipoTarjeta() {
		return tipoTarjeta;
	}*/

	public void setTipoTarjeta(TipoTarjeta tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}
		
}
