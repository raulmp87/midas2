package mx.com.afirme.midas2.dto.pago;

import mx.com.afirme.midas.base.StringUtil;

public class FolioReciboDTO{
	
	private String numRecibo;
	
	private String digitoVerificador;
	
	public FolioReciboDTO(){}
	
	public FolioReciboDTO(String numRecibo, String digitoVerificador){
		this.numRecibo = numRecibo;
		this.digitoVerificador = digitoVerificador;
	}

	public String getNumRecibo() {
		return numRecibo;
	}

	public void setNumRecibo(String numRecibo) {
		this.numRecibo = numRecibo;
	}

	public String getDigitoVerificador() {
		return digitoVerificador;
	}

	public void setDigitoVerificador(String digitoVerificador) {
		this.digitoVerificador = digitoVerificador;
	}
	
	public String getFolioConDigito(){
		return !StringUtil.isEmpty(this.numRecibo.toString()) && !StringUtil.isEmpty(this.digitoVerificador) ? this.numRecibo +  this.digitoVerificador : "";
	}
	
}