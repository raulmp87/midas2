package mx.com.afirme.midas2.dto.pago;

import mx.com.afirme.midas2.util.ResourceUtils;

import com.afirme.commons.CommonDefinitions;

public class PagosAfirmeDefinicion extends CommonDefinitions {
	
	public static final String SMTP_HOST_KEY = "com.afirme.payment.mail.smtp";

	public static final String MAILER_KEY = "com.afirme.payment.mail.mailer";

	public static final String MAIL_SENDER_ADDRESS_KEY = "com.afirme.payment.mail.sender";

	public static final String BUNDLE_NAME = "com.bsd.seycos.resources.payment";

	public static final String USER_NAME_KEY = "com.afirme.payment.userName";

	public static final String PASSWORD_KEY = "com.afirme.payment.password";

	public static final String URL_KEY = "com.afirme.payment.url";

	public static final String DRIVER_KEY = "com.afirme.payment.driver";

	public static final String USER_KEY = "com.afirme.payment.eibs.user";

	public static final String INSURANCE_ADMIN_MAIL = "com.afirme.payment.mail.admin";

	public static final String INSURANCE_HEADER_MAIL = "com.afirme.payment.mail.header";

	public static final String USER_NAME = ResourceUtils.getMessage(
			"mx.com.afirme.midas2.service.impl.pago.resources.payment",
			"com.afirme.payment.userName");

	public static final String PASSWORD = ResourceUtils.getMessage(
			"mx.com.afirme.midas2.service.impl.pago.resources.payment",
			"com.afirme.payment.password");

	public static final String URL = ResourceUtils.getMessage(
			"mx.com.afirme.midas2.service.impl.pago.resources.payment",
			"com.afirme.payment.url");

	public static final String DRIVER = ResourceUtils.getMessage(
			"mx.com.afirme.midas2.service.impl.pago.resources.payment",
			"com.afirme.payment.driver");

	public static final String USER = ResourceUtils.getMessage(
			"mx.com.afirme.midas2.service.impl.pago.resources.payment",
			"com.afirme.payment.eibs.user");

	public static final String SMTP_HOST = ResourceUtils.getMessage(
			"mx.com.afirme.midas2.service.impl.pago.resources.payment",
			"com.afirme.payment.mail.smtp");

	public static final String MAILER = ResourceUtils.getMessage(
			"mx.com.afirme.midas2.service.impl.pago.resources.payment",
			"com.afirme.payment.mail.mailer");

	public static final String MAIL_SENDER_ADDRESS = ResourceUtils.getMessage(
			"mx.com.afirme.midas2.service.impl.pago.resources.payment",
			"com.afirme.payment.mail.sender");

	public static final String TAB = "\t";

}
