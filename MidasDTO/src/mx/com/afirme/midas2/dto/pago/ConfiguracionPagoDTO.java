package mx.com.afirme.midas2.dto.pago;

import java.io.Serializable;

public class ConfiguracionPagoDTO implements Serializable {
	
	private static final long serialVersionUID = 4928788743054200561L;
	
	
	public static enum OrigenCobro{
		AUTO((short)1),
		VIDA_FAMILIAR((short)2),
		VIDA_MODULO((short)3),
		DANIOS((short)4),
		OTROS((short)5),
		CASA((short)6);
		
		private short origen;
		
		OrigenCobro(short origen){
			this.origen = origen;
		}
		
		public short valor(){
			return this.origen;
		}
	}
	
	private CuentaPagoDTO informacionCuenta;	
	
	private Integer diaDePago;
	
	private Integer numIntentoPago;
	
	private OrigenCobro origenCobro;
	
	private String descripcionPago;
	
	private String referenciaPago;
	

	public Integer getDiaDePago() {
		return diaDePago;
	}

	public void setDiaDePago(Integer diaDePago) {
		this.diaDePago = diaDePago;
	}

	public Integer getNumIntentoPago() {
		return numIntentoPago;
	}

	public void setNumIntentoPago(Integer numIntentoPago) {
		this.numIntentoPago = numIntentoPago;
	}

	public CuentaPagoDTO getInformacionCuenta() {
		return informacionCuenta;
	}

	public void setInformacionCuenta(CuentaPagoDTO informacionCuenta) {
		this.informacionCuenta = informacionCuenta;
	}

	public OrigenCobro getOrigenCobro() {
		return origenCobro;
	}

	public void setOrigenCobro(OrigenCobro origenCobro) {
		this.origenCobro = origenCobro;
	}

	public String getDescripcionPago() {
		return descripcionPago;
	}

	public void setDescripcionPago(String descripcionPago) {
		this.descripcionPago = descripcionPago;
	}

	public String getReferenciaPago() {
		return referenciaPago;
	}

	public void setReferenciaPago(String referenciaPago) {
		this.referenciaPago = referenciaPago;
	}
	
}
