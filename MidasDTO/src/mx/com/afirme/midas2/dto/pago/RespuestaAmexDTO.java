package mx.com.afirme.midas2.dto.pago;

import java.io.Serializable;

public class RespuestaAmexDTO implements Serializable {
	
	private static final long serialVersionUID = 5448184028307850367L;
	
	private String numAfiliacion;

	private String amexNumAutorizacion;
	
	private String codigoErrorAmex;
	
	private String referencia;
	
	private String descripcionErrorAmex;
	
	private String monto;
	
	private String numTarjeta;
	
	public RespuestaAmexDTO(){
		this.codigoErrorAmex = "0";
	}

	public String getNumAfiliacion() {
		return numAfiliacion;
	}

	public void setNumAfiliacion(String numAfiliacion) {
		this.numAfiliacion = numAfiliacion;
	}

	public String getAmexNumAutorizacion() {
		return amexNumAutorizacion;
	}

	public void setAmexNumAutorizacion(String amexNumAutorizacion) {
		this.amexNumAutorizacion = amexNumAutorizacion;
	}

	public String getCodigoErrorAmex() {
		return codigoErrorAmex;
	}

	public void setCodigoErrorAmex(String codigoErrorAmex) {
		this.codigoErrorAmex = codigoErrorAmex;
	}

	public String getDescripcionErrorAmex() {
		return descripcionErrorAmex;
	}

	public void setDescripcionErrorAmex(String descripcionErrorAmex) {
		this.descripcionErrorAmex = descripcionErrorAmex;
	}	

	public int getCodigoRespuesta(){
		return Integer.valueOf(this.codigoErrorAmex);
	}
	
	public boolean esValido(){
		if(getCodigoRespuesta() == 0){
			return true;
		}
		return false;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getNumTarjeta() {
		return numTarjeta;
	}

	public void setNumTarjeta(String numTarjeta) {
		this.numTarjeta = numTarjeta;
	}
	
	
}