/**
 * 
4 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */
package mx.com.afirme.midas2.dto.compensaciones;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.domain.compensaciones.CaBaseCalculo;
import mx.com.afirme.midas2.domain.compensaciones.CaEntidadPersona;
import mx.com.afirme.midas2.domain.compensaciones.CaRamo;
import mx.com.afirme.midas2.domain.compensaciones.CaRangos;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoAutorizacion;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoCondicionCalculo;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoContrato;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoMoneda;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoProvision;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.DatosSeycos;

public class CompensacionesDTO {
	
	/// Encabezado de la Compensacion
	private Long compensacionId;
	private CaRamo ramo;
	private Long idNegocio;
	private String nombreNegocio;
	private Long cotizacionId;
	private BigDecimal polizaId;
	private Long gerenciaId;
	private String gerenciaNombre;
	private Long idNegocioVida;
	private Long idConfiguracionBanca;
	private int idRecibo;
	private int descripcionRecibo;
	private BigDecimal topeMaximo;
	private String formaPagoBanca;
	
	
	/// Configuracion General
	private String claveAgenteGral;
	private String nombreAgenteGral;
	private String clavePromotorGral;
	private String nombrePromotorGral;
	private int inclusionesGral;	
	private CaTipoAutorizacion caTipoAutorizacion;
	private List<CaTipoAutorizacion> listTipoAutorizacionca;
	private List<Long> listTipoAutorizacionId;
	private List<CaEntidadPersona> listEntidadPersonaca;
	private CaEntidadPersona caEntidadPersona;
	private DatosSeycos datosSeycos;
	private List<SubRamosDaniosView> listSubRamosDaniosView;
	private BigDecimal importeTotalSubRamosDanios;
	
	/// Por Prima
	private boolean porPrima;
	private BigDecimal porcePrima;
	private BigDecimal montoPrima;
	private BigDecimal porceAgentePrima;
	private BigDecimal porcePromotorPrima;
	private boolean comisiNormalPrima;
	private CaBaseCalculo baseCalculocaPrima;
	private CaTipoMoneda tipoMonedacaPrima;
	private CaTipoProvision tipoProvisioncaPrima;
	private CaTipoCondicionCalculo condicionesCalcPrima;
	private List<CaRamo> listRamos;
	private String lineasNegocio;
	private int comisionAgente;
	private Long frecuenciaPagosId;
	private BigDecimal bonoFijoVida;
	
	/// Por Baja Siniestralidad
	private boolean porSiniestralidad;
	private BigDecimal porceSiniestralidad;
	private BigDecimal montoSiniestralidad;
	private BigDecimal porceAgenteSiniestralidad;
	private BigDecimal porcePromotorSiniestralidad;	
	private CaBaseCalculo baseCalculocaSiniestralidad;
	private CaTipoCondicionCalculo condicionesCalcSiniestralidad;
	private String nivelesRangos;
	private List<CaRangos> listCaRangosSiniestralidad;
	
	
	/// Por Derecho de Poliza
	private boolean porPoliza;
	private String claveAgentePoliza;
	private String nombreAgentePoliza;
	private BigDecimal porcePoliza;
	private BigDecimal montoPoliza;
	private BigDecimal porceAgentePoliza;
	private BigDecimal porcePromotorPoliza;
	private BigDecimal polizaPoliza;
	private BigDecimal incisoPoliza;
	private BigDecimal endosoPoliza;	
	private Boolean emisionInterna;
    private Boolean emisionExterna;
	
	/// Por Utilidad
	private boolean porUtilidad;
	private String claveAgenteUtilidad;
	private String nombreAgenteUtilidad;
	private BigDecimal porcenUtilidadesUtilidad;
	private BigDecimal montoUtilidadesUtilidad;
	private BigDecimal montoDeLasUtilidades;
	private BigDecimal montoPagoUtilidad;
	/// Contraprestacion
	private boolean contraprestacion;
	private String claveProveedorContra;
	private String nombreProveedorContra;
	private CaTipoContrato caTipoContrato;
	private List<CaEntidadPersona> listEntidadPersonacaContra;
	
	/// Contraprestacion Por Prima
	private boolean porPrimaContra;
	private BigDecimal porcePrimaContra;
	private BigDecimal montoPrimaContra;
	private BigDecimal porceProveedorPrimaContra;
	private BigDecimal porcePromotorPrimaContra;
	private boolean comisiNormalPrimaContra;
	private CaBaseCalculo baseCalculocaPrimaContra;
	private CaTipoMoneda tipoMonedacaPrimaContra;
	private CaTipoCondicionCalculo condicionesCalcPrimaContra;
	
	
	/// Contraprestacion Por Baja Siniestralidad
	private boolean porSiniestralidadContra;
	private BigDecimal porceSiniestralidadContra;
	private BigDecimal montoSiniestralidadContra;
	private BigDecimal porceProveedorSiniestralidadContra;
	private BigDecimal porcePromotorSiniestralidadContra;
	private CaBaseCalculo baseCalculocaSiniestralidadContra;
	private CaTipoMoneda tipoMonedacaSiniestralidadContra;
	
	/// Contraprestacion Por Utilidad
	private boolean porUtilidadContra;
	private String claveAgenteUtilidadContra;
	private String nombreAgenteUtilidadContra;
	private BigDecimal porcenUtilidadesUtilidadContra;
	private BigDecimal montoUtilidadesUtilidadContra;
	private BigDecimal montoDeLasUtilidadesContra;
	private BigDecimal parcialidadUtilidad;
	
	private boolean porCumplimientoDeMeta;
	private CaBaseCalculo baseCalculoCumplimientoMeta;
	private CaTipoMoneda caTipoMonedaCumplimientoMeta;
	private String nivelesRangosCumplimientoMeta;
	private List<CaRangos> listCaRangosCumplimientoMeta;
	private String presupuestoAnual;
	
	//Usuario
	private boolean fnTecnico;
	private boolean fnAgente;	
	private boolean fnJuridico;
	
	private Date ultimaModificacion;
	private String usuarioModificacion;
	
	private int bloquearConfiguracion;
	
	private int estatusCompensacionId;	
	private int estatusContraprestacionId;
	
	private Date fechaModificacionVida;
	private Date fechaModificacionVidaMax;
	private int aplicaComision;	
	private int ivaPrima;
	private int ivaSiniestralidad;
	
	private String vidaIndiv;
	private Map<Long, String> listRecibos;
	
	public Long getCompensacionId() {
		return compensacionId;
	}
	public void setCompensacionId(Long compensacionId) {
		this.compensacionId = compensacionId;
	}	
	public String getNombreNegocio() {
		return nombreNegocio;
	}
	public void setNombreNegocio(String nombreNegocio) {
		this.nombreNegocio = nombreNegocio;
	}
	public BigDecimal getPolizaId() {
		return polizaId;
	}
	public void setPolizaId(BigDecimal polizaId) {
		this.polizaId = polizaId;
	}
	public Long getGerenciaId() {
		return gerenciaId;
	}
	public void setGerenciaId(Long gerenciaId) {
		this.gerenciaId = gerenciaId;
	}
	public String getClaveAgenteGral() {
		return claveAgenteGral;
	}
	public void setClaveAgenteGral(String claveAgenteGral) {
		this.claveAgenteGral = claveAgenteGral;
	}
	public String getNombreAgenteGral() {
		return nombreAgenteGral;
	}
	public void setNombreAgenteGral(String nombreAgenteGral) {
		this.nombreAgenteGral = nombreAgenteGral;
	}
	public boolean isPorPrima() {
		return porPrima;
	}
	public void setPorPrima(boolean porPrima) {
		this.porPrima = porPrima;
	}
	public BigDecimal getPorcePrima() {
		return porcePrima;
	}
	public void setPorcePrima(BigDecimal porcePrima) {
		this.porcePrima = porcePrima;
	}
	public BigDecimal getMontoPrima() {
		return montoPrima;
	}
	public void setMontoPrima(BigDecimal montoPrima) {
		this.montoPrima = montoPrima;
	}
	public BigDecimal getPorceAgentePrima() {
		return porceAgentePrima;
	}
	public void setPorceAgentePrima(BigDecimal porceAgentePrima) {
		this.porceAgentePrima = porceAgentePrima;
	}
	public BigDecimal getPorcePromotorPrima() {
		return porcePromotorPrima;
	}
	public void setPorcePromotorPrima(BigDecimal porcePromotorPrima) {
		this.porcePromotorPrima = porcePromotorPrima;
	}
	public boolean isComisiNormalPrima() {
		return comisiNormalPrima;
	}
	public void setComisiNormalPrima(boolean comisiNormalPrima) {
		this.comisiNormalPrima = comisiNormalPrima;
	}
	public CaBaseCalculo getBaseCalculocaPrima() {
		return baseCalculocaPrima;
	}
	public void setBaseCalculocaPrima(CaBaseCalculo baseCalculocaPrima) {
		this.baseCalculocaPrima = baseCalculocaPrima;
	}
	public CaTipoMoneda getTipoMonedacaPrima() {
		return tipoMonedacaPrima;
	}
	public void setTipoMonedacaPrima(CaTipoMoneda tipoMonedacaPrima) {
		this.tipoMonedacaPrima = tipoMonedacaPrima;
	}
	public boolean isPorSiniestralidad() {
		return porSiniestralidad;
	}
	public void setPorSiniestralidad(boolean porSiniestralidad) {
		this.porSiniestralidad = porSiniestralidad;
	}
	public BigDecimal getPorceSiniestralidad() {
		return porceSiniestralidad;
	}
	public void setPorceSiniestralidad(BigDecimal porceSiniestralidad) {
		this.porceSiniestralidad = porceSiniestralidad;
	}
	public BigDecimal getMontoSiniestralidad() {
		return montoSiniestralidad;
	}
	public void setMontoSiniestralidad(BigDecimal montoSiniestralidad) {
		this.montoSiniestralidad = montoSiniestralidad;
	}
	public BigDecimal getPorceAgenteSiniestralidad() {
		return porceAgenteSiniestralidad;
	}
	public void setPorceAgenteSiniestralidad(BigDecimal porceAgenteSiniestralidad) {
		this.porceAgenteSiniestralidad = porceAgenteSiniestralidad;
	}
	public BigDecimal getPorcePromotorSiniestralidad() {
		return porcePromotorSiniestralidad;
	}
	public void setPorcePromotorSiniestralidad(BigDecimal porcePromotorSiniestralidad) {
		this.porcePromotorSiniestralidad = porcePromotorSiniestralidad;
	}
	public CaBaseCalculo getBaseCalculocaSiniestralidad() {
		return baseCalculocaSiniestralidad;
	}
	public void setBaseCalculocaSiniestralidad(
			CaBaseCalculo baseCalculocaSiniestralidad) {
		this.baseCalculocaSiniestralidad = baseCalculocaSiniestralidad;
	}
	public String getClaveAgentePoliza() {
		return claveAgentePoliza;
	}
	public void setClaveAgentePoliza(String claveAgentePoliza) {
		this.claveAgentePoliza = claveAgentePoliza;
	}
	public String getNombreAgentePoliza() {
		return nombreAgentePoliza;
	}
	public void setNombreAgentePoliza(String nombreAgentePoliza) {
		this.nombreAgentePoliza = nombreAgentePoliza;
	}
	public boolean isPorPoliza() {
		return porPoliza;
	}
	public void setPorPoliza(boolean porPoliza) {
		this.porPoliza = porPoliza;
	}
	public BigDecimal getPorcePoliza() {
		return porcePoliza;
	}
	public void setPorcePoliza(BigDecimal porcePoliza) {
		this.porcePoliza = porcePoliza;
	}
	public BigDecimal getMontoPoliza() {
		return montoPoliza;
	}
	public void setMontoPoliza(BigDecimal montoPoliza) {
		this.montoPoliza = montoPoliza;
	}
	public BigDecimal getPorceAgentePoliza() {
		return porceAgentePoliza;
	}
	public void setPorceAgentePoliza(BigDecimal porceAgentePoliza) {
		this.porceAgentePoliza = porceAgentePoliza;
	}
	public BigDecimal getPorcePromotorPoliza() {
		return porcePromotorPoliza;
	}
	public void setPorcePromotorPoliza(BigDecimal porcePromotorPoliza) {
		this.porcePromotorPoliza = porcePromotorPoliza;
	}
	public BigDecimal getPolizaPoliza() {
		return polizaPoliza;
	}
	public void setPolizaPoliza(BigDecimal polizaPoliza) {
		this.polizaPoliza = polizaPoliza;
	}
	public BigDecimal getIncisoPoliza() {
		return incisoPoliza;
	}
	public void setIncisoPoliza(BigDecimal incisoPoliza) {
		this.incisoPoliza = incisoPoliza;
	}
	public BigDecimal getEndosoPoliza() {
		return endosoPoliza;
	}
	public void setEndosoPoliza(BigDecimal endosoPoliza) {
		this.endosoPoliza = endosoPoliza;
	}
	public String getClaveAgenteUtilidad() {
		return claveAgenteUtilidad;
	}
	public void setClaveAgenteUtilidad(String claveAgenteUtilidad) {
		this.claveAgenteUtilidad = claveAgenteUtilidad;
	}
	public String getNombreAgenteUtilidad() {
		return nombreAgenteUtilidad;
	}
	public void setNombreAgenteUtilidad(String nombreAgenteUtilidad) {
		this.nombreAgenteUtilidad = nombreAgenteUtilidad;
	}
	public BigDecimal getPorcenUtilidadesUtilidad() {
		return porcenUtilidadesUtilidad;
	}
	public void setPorcenUtilidadesUtilidad(BigDecimal porcenUtilidadesUtilidad) {
		this.porcenUtilidadesUtilidad = porcenUtilidadesUtilidad;
	}
	public BigDecimal getMontoUtilidadesUtilidad() {
		return montoUtilidadesUtilidad;
	}
	public void setMontoUtilidadesUtilidad(BigDecimal montoUtilidadesUtilidad) {
		this.montoUtilidadesUtilidad = montoUtilidadesUtilidad;
	}
	public BigDecimal getMontoDeLasUtilidades() {
		return montoDeLasUtilidades;
	}
	public void setMontoDeLasUtilidades(BigDecimal montoDeLasUtilidades) {
		this.montoDeLasUtilidades = montoDeLasUtilidades;
	}
	public boolean isContraprestacion() {
		return contraprestacion;
	}
	public void setContraprestacion(boolean contraprestacion) {
		this.contraprestacion = contraprestacion;
	}
	public String getClaveProveedorContra() {
		return claveProveedorContra;
	}
	public void setClaveProveedorContra(String claveProveedorContra) {
		this.claveProveedorContra = claveProveedorContra;
	}
	public String getNombreProveedorContra() {
		return nombreProveedorContra;
	}
	public void setNombreProveedorContra(String nombreProveedorContra) {
		this.nombreProveedorContra = nombreProveedorContra;
	}
	public CaTipoContrato getCaTipoContrato() {
		return caTipoContrato;
	}
	public void setCaTipoContrato(CaTipoContrato caTipoContrato) {
		this.caTipoContrato = caTipoContrato;
	}
	public boolean isPorPrimaContra() {
		return porPrimaContra;
	}
	public void setPorPrimaContra(boolean porPrimaContra) {
		this.porPrimaContra = porPrimaContra;
	}
	public BigDecimal getPorcePrimaContra() {
		return porcePrimaContra;
	}
	public void setPorcePrimaContra(BigDecimal porcePrimaContra) {
		this.porcePrimaContra = porcePrimaContra;
	}
	public BigDecimal getMontoPrimaContra() {
		return montoPrimaContra;
	}
	public void setMontoPrimaContra(BigDecimal montoPrimaContra) {
		this.montoPrimaContra = montoPrimaContra;
	}
	public BigDecimal getPorceProveedorPrimaContra() {
		return porceProveedorPrimaContra;
	}
	public void setPorceProveedorPrimaContra(BigDecimal porceProveedorPrimaContra) {
		this.porceProveedorPrimaContra = porceProveedorPrimaContra;
	}
	public BigDecimal getPorcePromotorPrimaContra() {
		return porcePromotorPrimaContra;
	}
	public void setPorcePromotorPrimaContra(BigDecimal porcePromotorPrimaContra) {
		this.porcePromotorPrimaContra = porcePromotorPrimaContra;
	}
	public boolean isComisiNormalPrimaContra() {
		return comisiNormalPrimaContra;
	}
	public void setComisiNormalPrimaContra(boolean comisiNormalPrimaContra) {
		this.comisiNormalPrimaContra = comisiNormalPrimaContra;
	}
	public CaBaseCalculo getBaseCalculocaPrimaContra() {
		return baseCalculocaPrimaContra;
	}
	public void setBaseCalculocaPrimaContra(CaBaseCalculo baseCalculocaPrimaContra) {
		this.baseCalculocaPrimaContra = baseCalculocaPrimaContra;
	}
	public CaTipoMoneda getTipoMonedacaPrimaContra() {
		return tipoMonedacaPrimaContra;
	}
	public void setTipoMonedacaPrimaContra(CaTipoMoneda tipoMonedacaPrimaContra) {
		this.tipoMonedacaPrimaContra = tipoMonedacaPrimaContra;
	}
	public boolean isPorSiniestralidadContra() {
		return porSiniestralidadContra;
	}
	public void setPorSiniestralidadContra(boolean porSiniestralidadContra) {
		this.porSiniestralidadContra = porSiniestralidadContra;
	}
	public BigDecimal getPorceSiniestralidadContra() {
		return porceSiniestralidadContra;
	}
	public void setPorceSiniestralidadContra(BigDecimal porceSiniestralidadContra) {
		this.porceSiniestralidadContra = porceSiniestralidadContra;
	}
	public BigDecimal getMontoSiniestralidadContra() {
		return montoSiniestralidadContra;
	}
	public void setMontoSiniestralidadContra(BigDecimal montoSiniestralidadContra) {
		this.montoSiniestralidadContra = montoSiniestralidadContra;
	}
	public BigDecimal getPorceProveedorSiniestralidadContra() {
		return porceProveedorSiniestralidadContra;
	}
	public void setPorceProveedorSiniestralidadContra(
			BigDecimal porceProveedorSiniestralidadContra) {
		this.porceProveedorSiniestralidadContra = porceProveedorSiniestralidadContra;
	}
	public BigDecimal getPorcePromotorSiniestralidadContra() {
		return porcePromotorSiniestralidadContra;
	}
	public void setPorcePromotorSiniestralidadContra(
			BigDecimal porcePromotorSiniestralidadContra) {
		this.porcePromotorSiniestralidadContra = porcePromotorSiniestralidadContra;
	}
	public CaBaseCalculo getBaseCalculocaSiniestralidadContra() {
		return baseCalculocaSiniestralidadContra;
	}
	public void setBaseCalculocaSiniestralidadContra(
			CaBaseCalculo baseCalculocaSiniestralidadContra) {
		this.baseCalculocaSiniestralidadContra = baseCalculocaSiniestralidadContra;
	}
	public String getClaveAgenteUtilidadContra() {
		return claveAgenteUtilidadContra;
	}
	public void setClaveAgenteUtilidadContra(String claveAgenteUtilidadContra) {
		this.claveAgenteUtilidadContra = claveAgenteUtilidadContra;
	}
	public String getNombreAgenteUtilidadContra() {
		return nombreAgenteUtilidadContra;
	}
	public void setNombreAgenteUtilidadContra(String nombreAgenteUtilidadContra) {
		this.nombreAgenteUtilidadContra = nombreAgenteUtilidadContra;
	}
	public BigDecimal getPorcenUtilidadesUtilidadContra() {
		return porcenUtilidadesUtilidadContra;
	}
	public void setPorcenUtilidadesUtilidadContra(BigDecimal porcenUtilidadesUtilidadContra) {
		this.porcenUtilidadesUtilidadContra = porcenUtilidadesUtilidadContra;
	}
	public BigDecimal getMontoUtilidadesUtilidadContra() {
		return montoUtilidadesUtilidadContra;
	}
	public void setMontoUtilidadesUtilidadContra(
			BigDecimal montoUtilidadesUtilidadContra) {
		this.montoUtilidadesUtilidadContra = montoUtilidadesUtilidadContra;
	}
	public BigDecimal getMontoDeLasUtilidadesContra() {
		return montoDeLasUtilidadesContra;
	}
	public void setMontoDeLasUtilidadesContra(BigDecimal montoDeLasUtilidadesContra) {
		this.montoDeLasUtilidadesContra = montoDeLasUtilidadesContra;
	}
	public boolean isPorUtilidad() {
		return porUtilidad;
	}
	public void setPorUtilidad(boolean porUtilidad) {
		this.porUtilidad = porUtilidad;
	}
	public boolean isPorUtilidadContra() {
		return porUtilidadContra;
	}
	public void setPorUtilidadContra(boolean porUtilidadContra) {
		this.porUtilidadContra = porUtilidadContra;
	}
	public CaTipoCondicionCalculo getCondicionesCalcPrima() {
		return condicionesCalcPrima;
	}
	public void setCondicionesCalcPrima(CaTipoCondicionCalculo condicionesCalcPrima) {
		this.condicionesCalcPrima = condicionesCalcPrima;
	}
	public int getInclusionesGral() {
		return inclusionesGral;
	}
	public void setInclusionesGral(int inclusionesGral) {
		this.inclusionesGral = inclusionesGral;
	}
	public CaTipoProvision getTipoProvisioncaPrima() {
		return tipoProvisioncaPrima;
	}
	public void setTipoProvisioncaPrima(CaTipoProvision tipoProvisioncaPrima) {
		this.tipoProvisioncaPrima = tipoProvisioncaPrima;
	}
	public String getGerenciaNombre() {
		return gerenciaNombre;
	}
	public void setRamo(CaRamo ramo) {
		this.ramo = ramo;
	}
	public CaRamo getRamo(){
		return this.ramo;
	}
	public void setGerenciaNombre(String gerenciaNombre) {
		this.gerenciaNombre = gerenciaNombre;
	}
	public Boolean getEmisionInterna() {
		return emisionInterna;
	}
	public Boolean getEmisionExterna() {
		return emisionExterna;
	}
	public void setEmisionInterna(Boolean emisionInterna) {
		this.emisionInterna = emisionInterna;
	}
	public void setEmisionExterna(Boolean emisionExterna) {
		this.emisionExterna = emisionExterna;
	}
	public CaTipoCondicionCalculo getCondicionesCalcSiniestralidad() {
		return condicionesCalcSiniestralidad;
	}
	public CaTipoMoneda getTipoMonedacaSiniestralidadContra() {
		return tipoMonedacaSiniestralidadContra;
	}
	public void setCondicionesCalcSiniestralidad(
			CaTipoCondicionCalculo condicionesCalcSiniestralidad) {
		this.condicionesCalcSiniestralidad = condicionesCalcSiniestralidad;
	}
	public void setTipoMonedacaSiniestralidadContra(
			CaTipoMoneda tipoMonedacaSiniestralidadContra) {
		this.tipoMonedacaSiniestralidadContra = tipoMonedacaSiniestralidadContra;
	}
	public CaTipoAutorizacion getTipoAutorizacionca() {
		return caTipoAutorizacion;
	}
	public void setTipoAutorizacionca(CaTipoAutorizacion caTipoAutorizacion) {
		this.caTipoAutorizacion = caTipoAutorizacion;
	}
	public List<CaTipoAutorizacion> getListTipoAutorizacionca() {
		return listTipoAutorizacionca;
	}
	public void setListTipoAutorizacionca(
			List<CaTipoAutorizacion> listTipoAutorizacionca) {
		this.listTipoAutorizacionca = listTipoAutorizacionca;
	}	
	public List<CaEntidadPersona> getListEntidadPersonaca() {
		return listEntidadPersonaca;
	}
	public void setListEntidadPersonaca(List<CaEntidadPersona> listEntidadPersonaca) {
		this.listEntidadPersonaca = listEntidadPersonaca;
	}
	public Long getIdNegocio() {
		return idNegocio;
	}
	public void setIdNegocio(Long idNegocio) {
		this.idNegocio = idNegocio;
	}
	public String getClavePromotorGral() {
		return clavePromotorGral;
	}
	public String getNombrePromotorGral() {
		return nombrePromotorGral;
	}
	public void setClavePromotorGral(String clavePromotorGral) {
		this.clavePromotorGral = clavePromotorGral;
	}
	public void setNombrePromotorGral(String nombrePromotorGral) {
		this.nombrePromotorGral = nombrePromotorGral;
	}
	public List<CaRamo> getListRamos() {
		return listRamos;
	}
	public void setListRamos(List<CaRamo> listRamos) {
		this.listRamos = listRamos;
	}
	public List<CaEntidadPersona> getListEntidadPersonacaContra() {
		return listEntidadPersonacaContra;
	}
	public void setListEntidadPersonacaContra(
			List<CaEntidadPersona> listEntidadPersonacaContra) {
		this.listEntidadPersonacaContra = listEntidadPersonacaContra;
	}
	public CaTipoCondicionCalculo getCondicionesCalcPrimaContra() {
		return condicionesCalcPrimaContra;
	}
	public void setCondicionesCalcPrimaContra(
			CaTipoCondicionCalculo condicionesCalcPrimaContra) {
		this.condicionesCalcPrimaContra = condicionesCalcPrimaContra;
	}
	public CaEntidadPersona getCaEntidadPersona() {
		return caEntidadPersona;
	}
	public void setCaEntidadPersona(CaEntidadPersona caEntidadPersona) {
		this.caEntidadPersona = caEntidadPersona;
	}
	public void setCotizacionId(Long cotizacionId) {
		this.cotizacionId = cotizacionId;
	}
	public Long getCotizacionId() {
		return cotizacionId;
	}
	public void setFnTecnico(boolean fnTecnico) {
		this.fnTecnico = fnTecnico;
	}
	public boolean isFnTecnico() {
		return fnTecnico;
	}
	public void setFnAgente(boolean fnAgente) {
		this.fnAgente = fnAgente;
	}
	public boolean isFnAgente() {
		return fnAgente;
	}
	public void setFnJuridico(boolean fnJuridico) {
		this.fnJuridico = fnJuridico;
	}
	public boolean isFnJuridico() {
		return fnJuridico;
	}
	public void setNivelesRangos(String nivelesRangos) {
		this.nivelesRangos = nivelesRangos;
	}
	public String getNivelesRangos() {
		return nivelesRangos;
	}
	public void setLineasNegocio(String lineasNegocio) {
		this.lineasNegocio = lineasNegocio;
	}
	public String getLineasNegocio() {
		return lineasNegocio;
	}
	public void setParcialidadUtilidad(BigDecimal parcialidadUtilidad) {
		this.parcialidadUtilidad = parcialidadUtilidad;
	}
	public BigDecimal getParcialidadUtilidad() {
		return parcialidadUtilidad;
	}
	
	public void setListSubRamosDaniosView(List<SubRamosDaniosView> listSubRamosDaniosView) {
		this.listSubRamosDaniosView = listSubRamosDaniosView;
	}

	public List<SubRamosDaniosView> getListSubRamosDaniosView() {
		return listSubRamosDaniosView;
	}
	public void setMontoPagoUtilidad(BigDecimal montoPagoUtilidad) {
		this.montoPagoUtilidad = montoPagoUtilidad;
	}
	public BigDecimal getMontoPagoUtilidad() {
		return montoPagoUtilidad;
	}
	public void setComisionAgente(int comisionAgente) {
		this.comisionAgente = comisionAgente;
	}
	public int getComisionAgente() {
		return comisionAgente;
	}
	public void setFrecuenciaPagosId(Long frecuenciaPagosId) {
		this.frecuenciaPagosId = frecuenciaPagosId;
	}
	public Long getFrecuenciaPagosId() {
		return frecuenciaPagosId;
	}
	public void setIdNegocioVida(Long idNegocioVida) {
		this.idNegocioVida = idNegocioVida;
	}
	public Long getIdNegocioVida() {
		return idNegocioVida;
	}
	public void setUltimaModificacion(Date ultimaModificacion) {
		this.ultimaModificacion = ultimaModificacion;
	}
	public Date getUltimaModificacion() {
		return ultimaModificacion;
	}
	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}
	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}
	public void setBloquearConfiguracion(int bloquearConfiguracion) {
		this.bloquearConfiguracion = bloquearConfiguracion;
	}
	public int getBloquearConfiguracion() {
		return bloquearConfiguracion;
	}
	public void setListTipoAutorizacionId(List<Long> listTipoAutorizacionId) {
		this.listTipoAutorizacionId = listTipoAutorizacionId;
	}
	public List<Long> getListTipoAutorizacionId() {
		return listTipoAutorizacionId;
	}
	public void setIdConfiguracionBanca(Long idConfiguracionBanca) {
		this.idConfiguracionBanca = idConfiguracionBanca;
	}
	public Long getIdConfiguracionBanca() {
		return idConfiguracionBanca;
	}
	public void setBonoFijoVida(BigDecimal bonoFijoVida) {
		this.bonoFijoVida = bonoFijoVida;
	}
	public BigDecimal getBonoFijoVida() {
		return bonoFijoVida;
	}
	public void setPorCumplimientoDeMeta(boolean porCumplimientoDeMeta) {
		this.porCumplimientoDeMeta = porCumplimientoDeMeta;
	}
	public boolean isPorCumplimientoDeMeta() {
		return porCumplimientoDeMeta;
	}
	public void setBaseCalculoCumplimientoMeta(CaBaseCalculo baseCalculoCumplimientoMeta) {
		this.baseCalculoCumplimientoMeta = baseCalculoCumplimientoMeta;
	}
	public CaBaseCalculo getBaseCalculoCumplimientoMeta() {
		return baseCalculoCumplimientoMeta;
	}
	public void setNivelesRangosCumplimientoMeta(String nivelesRangosCumplimientoMeta) {
		this.nivelesRangosCumplimientoMeta = nivelesRangosCumplimientoMeta;
	}
	public String getNivelesRangosCumplimientoMeta() {
		return nivelesRangosCumplimientoMeta;
	}
	public void setCaTipoMonedaCumplimientoMeta(CaTipoMoneda caTipoMonedaCumplimientoMeta) {
		this.caTipoMonedaCumplimientoMeta = caTipoMonedaCumplimientoMeta;
	}
	public CaTipoMoneda getCaTipoMonedaCumplimientoMeta() {
		return caTipoMonedaCumplimientoMeta;
	}
	public void setDatosSeycos(DatosSeycos datosSeycos) {
		this.datosSeycos = datosSeycos;
	}
	public DatosSeycos getDatosSeycos() {
		return datosSeycos;
	}
	public void setListCaRangosSiniestralidad(List<CaRangos> listCaRangosSiniestralidad) {
		this.listCaRangosSiniestralidad = listCaRangosSiniestralidad;
	}
	public List<CaRangos> getListCaRangosSiniestralidad() {
		return listCaRangosSiniestralidad;
	}
	public void setListCaRangosCumplimientoMeta(List<CaRangos> listCaRangosCumplimientoMeta) {
		this.listCaRangosCumplimientoMeta = listCaRangosCumplimientoMeta;
	}
	public List<CaRangos> getListCaRangosCumplimientoMeta() {
		return listCaRangosCumplimientoMeta;
	}
	public void setPresupuestoAnual(String presupuestoAnual) {
		this.presupuestoAnual = presupuestoAnual;
	}
	public String getPresupuestoAnual() {
		return presupuestoAnual;
	}
	public void setImporteTotalSubRamosDanios(BigDecimal importeTotalSubRamosDanios) {
		this.importeTotalSubRamosDanios = importeTotalSubRamosDanios;
	}
	public BigDecimal getImporteTotalSubRamosDanios() {
		return importeTotalSubRamosDanios;
	}
	public void setListRecibos(Map<Long, String> listRecibos) {
		this.listRecibos = listRecibos;
	}
	public Map<Long, String> getListRecibos() {
		return listRecibos;
	}
	public String getVidaIndiv() {
		return vidaIndiv;
	}
	public void setVidaIndiv(String vidaIndiv) {
		this.vidaIndiv = vidaIndiv;
	}
	public void setEstatusCompensacionId(int estatusCompensacionId) {
		this.estatusCompensacionId = estatusCompensacionId;
	}
	public int getEstatusCompensacionId() {
		return estatusCompensacionId;
	}
	public void setEstatusContraprestacionId(int estatusContraprestacionId) {
		this.estatusContraprestacionId = estatusContraprestacionId;
	}
	public int getEstatusContraprestacionId() {
		return estatusContraprestacionId;
	}
	public void setAplicaComision(int aplicaComision) {
		this.aplicaComision = aplicaComision;
	}
	public int getAplicaComision() {
		return aplicaComision;
	}
	public void setFechaModificacionVida(Date fechaModificacionVida) {
		this.fechaModificacionVida = fechaModificacionVida;
	}
	public void setFechaModificacionVida(String fechaModificacionVida) {
		try {
			this.fechaModificacionVida = new SimpleDateFormat("dd/MM/yyyy").parse(fechaModificacionVida);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	public Date getFechaModificacionVida() {
		return fechaModificacionVida;
	}
	public void setFechaModificacionVidaMax(Date fechaModificacionVidaMax) {
		this.fechaModificacionVidaMax = fechaModificacionVidaMax;
	}
	public Date getFechaModificacionVidaMax() {
		return fechaModificacionVidaMax;
	}
	public void setIvaPrima(int ivaPrima) {
		this.ivaPrima = ivaPrima;
	}
	public int getIvaPrima() {
		return ivaPrima;
	}
	public void setIvaSiniestralidad(int ivaSiniestralidad) {
		this.ivaSiniestralidad = ivaSiniestralidad;
	}
	public int getIvaSiniestralidad() {
		return ivaSiniestralidad;
	}
	public int getIdRecibo() {
		return idRecibo;
	}
	public void setIdRecibo(int idRecibo) {
		this.idRecibo = idRecibo;
	}
	public int getDescripcionRecibo() {
		return descripcionRecibo;
	}
	public void setDescripcionRecibo(int descripcionRecibo) {
		this.descripcionRecibo = descripcionRecibo;
	}
	public BigDecimal getTopeMaximo() {
		return topeMaximo;
	}
	public void setTopeMaximo(BigDecimal topeMaximo) {
		this.topeMaximo = topeMaximo;
	}
	public String getFormaPagoBanca() {
		return formaPagoBanca;
	}
	public void setFormaPagoBanca(String formaPagoBanca) {
		this.formaPagoBanca = formaPagoBanca;
	}
	
}
