package mx.com.afirme.midas2.dto.compensaciones;

import java.io.Serializable;
import java.math.BigDecimal;

public class SubRamosDaniosViewDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private BigDecimal id;
	private BigDecimal IdToCotizacion;
	private BigDecimal IdToPoliza;
	private BigDecimal IdEntidadPer;
	private BigDecimal IdCompensacion;
	private BigDecimal idCotizacionEndoso;
	private int idRecibo;
	
	public BigDecimal getIdCotizacionEndoso() {
		return idCotizacionEndoso;
	}
	public void setIdCotizacionEndoso(BigDecimal idCotizacionEndoso) {
		this.idCotizacionEndoso = idCotizacionEndoso;
	}
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public BigDecimal getIdToCotizacion() {
		return IdToCotizacion;
	}
	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		IdToCotizacion = idToCotizacion;
	}
	public BigDecimal getIdToPoliza() {
		return IdToPoliza;
	}
	public void setIdToPoliza(BigDecimal idToPoliza) {
		IdToPoliza = idToPoliza;
	}
	public BigDecimal getIdEntidadPer() {
		return IdEntidadPer;
	}
	public void setIdEntidadPer(BigDecimal idEntidadPer) {
		IdEntidadPer = idEntidadPer;
	}
	public BigDecimal getIdCompensacion() {
		return IdCompensacion;
	}
	public void setIdCompensacion(BigDecimal idCompensacion) {
		IdCompensacion = idCompensacion;
	}
	public int getIdRecibo() {
		return idRecibo;
	}
	public void setIdRecibo(int idRecibo) {
		this.idRecibo = idRecibo;
	}
	
	
}
