package mx.com.afirme.midas2.dto.compensaciones;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.domain.compensaciones.CaConfiguracionBanca;
import mx.com.afirme.midas2.domain.emision.ppct.GerenciaSeycosId;

public class ConfiguracionContraprestacionBanca implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private CaConfiguracionBanca caConfiguracionBanca;
	private Long lineaVentaId;
	private List<Long> listLineasVenta;
	private List<String> listRamosId;
	private GerenciaSeycosId gerenciaSeycosId;
	private List<GerenciaSeycosId> listGerenciasSeycosId;
	private List<BancaEstadosView> listBancaEstadosActivos;
	
	public void setCaConfiguracionBanca(CaConfiguracionBanca caConfiguracionBanca) {
		this.caConfiguracionBanca = caConfiguracionBanca;
	}
	public CaConfiguracionBanca getCaConfiguracionBanca() {
		return caConfiguracionBanca;
	}
	public void setLineaVentaId(Long lineaVentaId) {
		this.lineaVentaId = lineaVentaId;
	}
	public Long getLineaVentaId() {
		return lineaVentaId;
	}
	public void setListRamosId(List<String> listRamosId) {
		this.listRamosId = listRamosId;
	}
	public List<String> getListRamosId() {
		return listRamosId;
	}
	public void setListGerenciasSeycosId(List<GerenciaSeycosId> listGerenciasSeycosId) {
		this.listGerenciasSeycosId = listGerenciasSeycosId;
	}
	public List<GerenciaSeycosId> getListGerenciasSeycosId() {
		return listGerenciasSeycosId;
	}
	public void setListBancaEstadosActivos(List<BancaEstadosView> listBancaEstadosActivos) {
		this.listBancaEstadosActivos = listBancaEstadosActivos;
	}
	public List<BancaEstadosView> getListBancaEstadosActivos() {
		return listBancaEstadosActivos;
	}
	public void setListLineasVenta(List<Long> listLineasVenta) {
		this.listLineasVenta = listLineasVenta;
	}
	public List<Long> getListLineasVenta() {
		return listLineasVenta;
	}
	public void setGerenciaSeycosId(GerenciaSeycosId gerenciaSeycosId) {
		this.gerenciaSeycosId = gerenciaSeycosId;
	}
	public GerenciaSeycosId getGerenciaSeycosId() {
		return gerenciaSeycosId;
	}

}
