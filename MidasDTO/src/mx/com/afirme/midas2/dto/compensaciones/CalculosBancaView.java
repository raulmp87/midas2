package mx.com.afirme.midas2.dto.compensaciones;

import java.math.BigDecimal;

public class CalculosBancaView {
	
	//private BigDecimal id;
	private String ramo;
	private String codigoRamo;
	private BigDecimal primaNetaEmitida;
	private BigDecimal primaNetaPagada;
	private BigDecimal porcentajeContraprestacionProducto;
	private BigDecimal montoContraprestacionProducto;
	private BigDecimal porcentajeContraprestacionCumplimientoMeta;
	private BigDecimal montoContraprestacionCumplimientoMeta;
	private BigDecimal porcentajeCalidadCartera;
	private BigDecimal montoCalidadCartera;
	private BigDecimal bonoFijo;
	private BigDecimal descuento;
	private BigDecimal total;
	private String comentarios;
	
	public CalculosBancaView (){
		
	}
	/*
	public void setId(BigDecimal id){
		this.id = id;
	}
	public BigDecimal getId(){
		return this.id;
	}*/
	public void setRamo(String ramo) {
		this.ramo = ramo;
	}
	public String getRamo() {
		return ramo;
	}
	public String getCodigoRamo() {
		return codigoRamo;
	}

	public void setCodigoRamo(String codigoRamo) {
		this.codigoRamo = codigoRamo;
	}

	public void setPrimaNetaEmitida(BigDecimal primaNetaEmitida) {
		this.primaNetaEmitida = primaNetaEmitida;
	}
	public BigDecimal getPrimaNetaEmitida() {
		return primaNetaEmitida;
	}
	public void setPrimaNetaPagada(BigDecimal primaNetaPagada) {
		this.primaNetaPagada = primaNetaPagada;
	}
	public BigDecimal getPrimaNetaPagada() {
		return primaNetaPagada;
	}
	public void setPorcentajeContraprestacionProducto(
			BigDecimal porcentajeContraprestacionProducto) {
		this.porcentajeContraprestacionProducto = porcentajeContraprestacionProducto;
	}
	public BigDecimal getPorcentajeContraprestacionProducto() {
		return porcentajeContraprestacionProducto;
	}
	public void setMontoContraprestacionProducto(
			BigDecimal montoContraprestacionProducto) {
		this.montoContraprestacionProducto = montoContraprestacionProducto;
	}
	public BigDecimal getMontoContraprestacionProducto() {
		return montoContraprestacionProducto;
	}
	public void setPorcentajeContraprestacionCumplimientoMeta(
			BigDecimal porcentajeContraprestacionCumplimientoMeta) {
		this.porcentajeContraprestacionCumplimientoMeta = porcentajeContraprestacionCumplimientoMeta;
	}
	public BigDecimal getPorcentajeContraprestacionCumplimientoMeta() {
		return porcentajeContraprestacionCumplimientoMeta;
	}
	public void setMontoContraprestacionCumplimientoMeta(
			BigDecimal montoContraprestacionCumplimientoMeta) {
		this.montoContraprestacionCumplimientoMeta = montoContraprestacionCumplimientoMeta;
	}
	public BigDecimal getMontoContraprestacionCumplimientoMeta() {
		return montoContraprestacionCumplimientoMeta;
	}
	public void setMontoCalidadCartera(BigDecimal montoCalidadCartera) {
		this.montoCalidadCartera = montoCalidadCartera;
	}
	public BigDecimal getMontoCalidadCartera() {
		return montoCalidadCartera;
	}
	public void setPorcentajeCalidadCartera(BigDecimal porcentajeCalidadCartera) {
		this.porcentajeCalidadCartera = porcentajeCalidadCartera;
	}
	public BigDecimal getPorcentajeCalidadCartera() {
		return porcentajeCalidadCartera;
	}
	public void setBonoFijo(BigDecimal bonoFijo) {
		this.bonoFijo = bonoFijo;
	}
	public BigDecimal getBonoFijo() {
		return bonoFijo;
	}
	public void setDescuento(BigDecimal descuento) {
		this.descuento = descuento;
	}
	public BigDecimal getDescuento() {
		return descuento;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}
	public String getComentarios() {
		return comentarios;
	}
	
	
}
