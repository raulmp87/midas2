package mx.com.afirme.midas2.dto.compensaciones;

import java.math.BigDecimal;

public class FiltroCompensacion {

	private Long idCompensacion;
	private BigDecimal idCotizacion;
	private BigDecimal idPoliza;
	private Long idGerencia;
	private Long idRamo;
	private Long idEstatus;
	private Long idNegocio;
	private String nombreNegocio;
	private Long idAgente;
	private String nombreAgente;
	private Long idProveedor;
	private String nombreProveedor;
	private Long idPromotor;
	private String nombrePromotor;
	
	private int pageSize;
	private int pageNumber;
	
	public Long getIdCompensacion() {
		return idCompensacion;
	}
	public void setIdCompensacion(Long idCompensacion) {
		this.idCompensacion = idCompensacion;
	}
	public BigDecimal getIdCotizacion() {
		return idCotizacion;
	}
	public void setIdCotizacion(BigDecimal idCotizacion) {
		this.idCotizacion = idCotizacion;
	}
	public BigDecimal getIdPoliza() {
		return idPoliza;
	}
	public void setIdPoliza(BigDecimal idPoliza) {
		this.idPoliza = idPoliza;
	}
	public Long getIdGerencia() {
		return idGerencia;
	}
	public void setIdGerencia(Long idGerencia) {
		this.idGerencia = idGerencia;
	}
	public Long getIdRamo() {
		return idRamo;
	}
	public void setIdRamo(Long idRamo) {
		this.idRamo = idRamo;
	}
	public Long getIdEstatus() {
		return idEstatus;
	}
	public void setIdEstatus(Long idEstatus) {
		this.idEstatus = idEstatus;
	}
	
	public Long getIdNegocio() {
		return idNegocio;
	}
	public void setIdNegocio(Long idNegocio) {
		this.idNegocio = idNegocio;
	}
	
	public String getNombreNegocio() {
		return nombreNegocio;
	}
	public void setNombreNegocio(String nombreNegocio) {
		this.nombreNegocio = nombreNegocio;
	}
	
	public Long getIdAgente() {
		return idAgente;
	}
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
	public String getNombreAgente() {
		return nombreAgente;
	}
	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}
	
	public Long getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(Long idProveedor) {
		this.idProveedor = idProveedor;
	}
	public String getNombreProveedor() {
		return nombreProveedor;
	}
	public void setNombreProveedor(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	public void setIdPromotor(Long idPromotor) {
		this.idPromotor = idPromotor;
	}
	public Long getIdPromotor() {
		return idPromotor;
	}
	public void setNombrePromotor(String nombrePromotor) {
		this.nombrePromotor = nombrePromotor;
	}
	public String getNombrePromotor() {
		return nombrePromotor;
	}
	
	
}
