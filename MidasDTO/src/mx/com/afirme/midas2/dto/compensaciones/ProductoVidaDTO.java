package mx.com.afirme.midas2.dto.compensaciones;

import java.math.BigDecimal;

public class ProductoVidaDTO {
	private String nombreProducto;
	private String nombreLinea;
	private BigDecimal comision;
	
	public String getNombreProducto() {
		return nombreProducto;
	}
	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}
	public String getNombreLinea() {
		return nombreLinea;
	}
	public void setNombreLinea(String nombreLinea) {
		this.nombreLinea = nombreLinea;
	}
	public BigDecimal getComision() {
		return comision;
	}
	public void setComision(BigDecimal comision) {
		this.comision = comision;
	}
}
