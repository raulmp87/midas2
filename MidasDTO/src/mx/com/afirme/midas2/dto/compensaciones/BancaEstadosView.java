package mx.com.afirme.midas2.dto.compensaciones;

import java.io.Serializable;
import java.math.BigDecimal;

public class BancaEstadosView implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private BigDecimal id;
	private BigDecimal configuracionBancaId;
	private String estadoId;
	private String estadoNombre;
	
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public BigDecimal getId() {
		return id;
	}
	
	public void setConfiguracionBancaId(BigDecimal configuracionBancaId) {
		this.configuracionBancaId = configuracionBancaId;
	}
	public BigDecimal getConfiguracionBancaId() {
		return configuracionBancaId;
	}
	
	public void setEstadoId(String estadoId) {
		this.estadoId = estadoId;
	}
	public String getEstadoId() {
		return estadoId;
	}
	
	public void setEstadoNombre(String estadoNombre) {
		this.estadoNombre = estadoNombre;
	}
	public String getEstadoNombre() {
		return estadoNombre;
	}
	
	
	
}
