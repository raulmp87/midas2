package mx.com.afirme.midas2.dto.compensaciones;

import java.io.Serializable;


public class CalculoCompensacionesDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long idToPoliza;
	private Long idEntidadPersona;
	private Long idCompensacion;
	private Double importe;
	private Long respuestaCodigo;
	private Long respuestaDescripcion;
	
	public Long getIdToPoliza() {
		return idToPoliza;
	}
	public void setIdToPoliza(Long idToPoliza) {
		this.idToPoliza = idToPoliza;
	}
	public Long getIdEntidadPersona() {
		return idEntidadPersona;
	}
	public void setIdEntidadPersona(Long idEntidadPersona) {
		this.idEntidadPersona = idEntidadPersona;
	}
	public Long getIdCompensacion() {
		return idCompensacion;
	}
	public void setIdCompensacion(Long idCompensacion) {
		this.idCompensacion = idCompensacion;
	}
	public Double getImporte() {
		return importe;
	}
	public void setImporte(Double importe) {
		this.importe = importe;
	}
	public Long getRespuestaCodigo() {
		return respuestaCodigo;
	}
	public void setRespuestaCodigo(Long respuestaCodigo) {
		this.respuestaCodigo = respuestaCodigo;
	}
	public Long getRespuestaDescripcion() {
		return respuestaDescripcion;
	}
	public void setRespuestaDescripcion(Long respuestaDescripcion) {
		this.respuestaDescripcion = respuestaDescripcion;
	}
}
