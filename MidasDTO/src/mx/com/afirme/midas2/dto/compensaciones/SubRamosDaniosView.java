package mx.com.afirme.midas2.dto.compensaciones;

import java.io.Serializable;
import java.math.BigDecimal;

public class SubRamosDaniosView implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private BigDecimal id;
	private BigDecimal idTcRamo;
	private String descripcionRamo;
	private BigDecimal idTcSubRamo;
	private String subRamo;
	private BigDecimal primaBase;
	private BigDecimal porcentajeCompensacion;
	private BigDecimal porcentajeCompensacionImporte;
	private String existe;
	
	
	public SubRamosDaniosView(){
		
	}

	public BigDecimal getIdTcRamo() {
		return idTcRamo;
	}

	public void setIdTcRamo(BigDecimal idTcRamo) {
		this.idTcRamo = idTcRamo;
	}

	public void setDescripcionRamo(String descripcionRamo) {
		this.descripcionRamo = descripcionRamo;
	}
	public String getDescripcionRamo() {
		return descripcionRamo;
	}
	public void setIdTcSubRamo(BigDecimal idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}
	public BigDecimal getIdTcSubRamo() {
		return idTcSubRamo;
	}
	public void setSubRamo(String subRamo) {
		this.subRamo = subRamo;
	}
	public String getSubRamo() {
		return subRamo;
	}
	public void setPorcentajeCompensacion(BigDecimal porcentajeCompensacion) {
		this.porcentajeCompensacion = porcentajeCompensacion;
	}
	public BigDecimal getPorcentajeCompensacion() {
		return porcentajeCompensacion;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public BigDecimal getId() {
		return id;
	}	
	
	public BigDecimal getPrimaBase() {
		return primaBase;
	}

	public void setPrimaBase(BigDecimal primaBase) {
		this.primaBase = primaBase;
	}

	public BigDecimal getPorcentajeCompensacionImporte() {
		return porcentajeCompensacionImporte;
	}

	public void setPorcentajeCompensacionImporte(
			BigDecimal porcentajeCompensacionImporte) {
		this.porcentajeCompensacionImporte = porcentajeCompensacionImporte;
	}

	public void setExiste(String existe) {
		this.existe = existe;
	}

	public String getExiste() {
		return existe;
	}
	
	
	
}
