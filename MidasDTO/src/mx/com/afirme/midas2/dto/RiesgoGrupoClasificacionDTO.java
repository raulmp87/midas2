package mx.com.afirme.midas2.dto;

import java.io.Serializable;


public class RiesgoGrupoClasificacionDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long idToriesgo;
	private Long idConcepto;
	
	public RiesgoGrupoClasificacionDTO(Long idToriesgo, Long idConcepto) {
		super();
		this.idToriesgo = idToriesgo;
		this.idConcepto = idConcepto;
	}
	public Long getIdToriesgo() {
		return idToriesgo;
	}
	public void setIdToriesgo(Long idToriesgo) {
		this.idToriesgo = idToriesgo;
	}
	public Long getIdConcepto() {
		return idConcepto;
	}
	public void setIdConcepto(Long idConcepto) {
		this.idConcepto = idConcepto;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idConcepto == null) ? 0 : idConcepto.hashCode());
		result = prime * result
				+ ((idToriesgo == null) ? 0 : idToriesgo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RiesgoGrupoClasificacionDTO other = (RiesgoGrupoClasificacionDTO) obj;
		if (idConcepto == null) {
			if (other.idConcepto != null)
				return false;
		} else if (!idConcepto.equals(other.idConcepto))
			return false;
		if (idToriesgo == null) {
			if (other.idToriesgo != null)
				return false;
		} else if (!idToriesgo.equals(other.idToriesgo))
			return false;
		return true;
	}
}