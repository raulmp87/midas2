package mx.com.afirme.midas2.dto;

import java.util.Date;

import mx.com.afirme.midas2.annotation.Exportable;


public class LayoutRenovacion{

	private String estatus;
	private Long idAgente;
	private String moneda;
	private String centroEmisor;
	private String ejecutivo;
	private Long idCotizacion;
	private String numPoliza;
	private String numLiquidacion;
	private String autProsa;
	private Date fechaIniVig;
	private Date fechaEmision;
	private Date fechaFinVig;
	private String lineaNegocio;
	private String paquete;
	private String idUsuario;
	private String tipoPersona;
	private String rfc;
	private String clienteVip;
	private String nombreCliente;
	private String paternoCliente;
	private String maternoCliente;
	private String cp;
	private String colonia;
	private String telefono;
	private String clvAmis;
	private Long modelo;
	private String repuve;
	private String clvUso;
	private String placas;
	private String formaPago;
	private String numMotor;
	private String numSerie;
	private String valorComer;
	private String tPasajeros;
	private String descVehiculo;
	private Double primaTotal;
	private Long deducMateriales;
	private Long deducRoboTotal;
	private Double limiteRCTerc;
	private Long deducRCTerc;
	private Double limiteGMedicos;
	private Double limiteMuerte;
	private Double limiteRCViaj;
	private String aJuridica;
	private Double limiteAdapt;
	private Double limiteEquipo;
	private Long deducEquipo;
	private String igualacion;
	private Double derechos;
	private String sAutorizacion;
	private String cAutorizacion;
	private String descEquipo;
	private String descAdaptacion;
	private String nombreConduc;
	private String paternoConduc;
	private String maternoConduc;
	private String licencia;
	private Date fechaNacim;
	private String ocupacion;
	private Long idCliente;
	private Long remolques;
	private String tipoCarga;
	private String observaciones;
	private String nombreAseg;
	private String limiteAcciden;
	private Long recargoPago;
	private Double sumaDanios;
	private Double sumaRobo;
	private String correo;
	private String altaCondicion;
	private String bajaCondicion;
	
	public LayoutRenovacion() {

	}
	public LayoutRenovacion(String estatus, Long idAgente, String moneda,
			String centroEmisor, String ejecutivo, Long idCotizacion,
			String numPoliza, String numLiquidacion, String autProsa,
			Date fechaIniVig, Date fechaEmision, Date fechaFinVig,
			String lineaNegocio, String paquete, String idUsuario,
			String tipoPersona, String rfc, String clienteVip,
			String nombreCliente, String paternoCliente, String maternoCliente,
			String cp, String colonia, String telefono, String clvAmis,
			Long modelo, String repuve, String clvUso, String placas,
			String formaPago, String numMotor, String numSerie,
			String valorComer, String tPasajeros, String descVehiculo,
			Double primaTotal, Long deducMateriales, Long deducRoboTotal,
			Double limiteRCTerc, Long deducRCTerc, Double limiteGMedicos,
			Double limiteMuerte, Double limiteRCViaj, String aJuridica,
			Double limiteAdapt, Double limiteEquipo, Long deducEquipo,
			String igualacion, Double derechos, String sAutorizacion,
			String cAutorizacion, String descEquipo, String descAdaptacion,
			String nombreConduc, String paternoConduc, String maternoConduc,
			String licencia, Date fechaNacim, String ocupacion, Long idCliente,
			Long remolques, String tipoCarga, String observaciones,
			String nombreAseg, String limiteAcciden, Long recargoPago,
			Double sumaDanios, Double sumaRobo, String correo,
			String altaCondicion, String bajaCondicion) {
		this.estatus = estatus;
		this.idAgente = idAgente;
		this.moneda = moneda;
		this.centroEmisor = centroEmisor;
		this.ejecutivo = ejecutivo;
		this.idCotizacion = idCotizacion;
		this.numPoliza = numPoliza;
		this.numLiquidacion = numLiquidacion;
		this.autProsa = autProsa;
		this.fechaIniVig = fechaIniVig;
		this.fechaEmision = fechaEmision;
		this.fechaFinVig = fechaFinVig;
		this.lineaNegocio = lineaNegocio;
		this.paquete = paquete;
		this.idUsuario = idUsuario;
		this.tipoPersona = tipoPersona;
		this.rfc = rfc;
		this.clienteVip = clienteVip;
		this.nombreCliente = nombreCliente;
		this.paternoCliente = paternoCliente;
		this.maternoCliente = maternoCliente;
		this.cp = cp;
		this.colonia = colonia;
		this.telefono = telefono;
		this.clvAmis = clvAmis;
		this.modelo = modelo;
		this.repuve = repuve;
		this.clvUso = clvUso;
		this.placas = placas;
		this.formaPago = formaPago;
		this.numMotor = numMotor;
		this.numSerie = numSerie;
		this.valorComer = valorComer;
		this.tPasajeros = tPasajeros;
		this.descVehiculo = descVehiculo;
		this.primaTotal = primaTotal;
		this.deducMateriales = deducMateriales;
		this.deducRoboTotal = deducRoboTotal;
		this.limiteRCTerc = limiteRCTerc;
		this.deducRCTerc = deducRCTerc;
		this.limiteGMedicos = limiteGMedicos;
		this.limiteMuerte = limiteMuerte;
		this.limiteRCViaj = limiteRCViaj;
		this.aJuridica = aJuridica;
		this.limiteAdapt = limiteAdapt;
		this.limiteEquipo = limiteEquipo;
		this.deducEquipo = deducEquipo;
		this.igualacion = igualacion;
		this.derechos = derechos;
		this.sAutorizacion = sAutorizacion;
		this.cAutorizacion = cAutorizacion;
		this.descEquipo = descEquipo;
		this.descAdaptacion = descAdaptacion;
		this.nombreConduc = nombreConduc;
		this.paternoConduc = paternoConduc;
		this.maternoConduc = maternoConduc;
		this.licencia = licencia;
		this.fechaNacim = fechaNacim;
		this.ocupacion = ocupacion;
		this.idCliente = idCliente;
		this.remolques = remolques;
		this.tipoCarga = tipoCarga;
		this.observaciones = observaciones;
		this.nombreAseg = nombreAseg;
		this.limiteAcciden = limiteAcciden;
		this.recargoPago = recargoPago;
		this.sumaDanios = sumaDanios;
		this.sumaRobo = sumaRobo;
		this.correo = correo;
		this.altaCondicion = altaCondicion;
		this.bajaCondicion = bajaCondicion;
	}
	@Exportable(columnName="Estatus", columnOrder=0)
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	@Exportable(columnName="Agente", columnOrder=1)
	public Long getIdAgente() {
		return idAgente;
	}
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
	@Exportable(columnName="Moneda", columnOrder=2)
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	@Exportable(columnName="Centro Emisor", columnOrder=3)
	public String getCentroEmisor() {
		return centroEmisor;
	}
	public void setCentroEmisor(String centroEmisor) {
		this.centroEmisor = centroEmisor;
	}
	@Exportable(columnName="Ejecutivo", columnOrder=4)
	public String getEjecutivo() {
		return ejecutivo;
	}
	public void setEjecutivo(String ejecutivo) {
		this.ejecutivo = ejecutivo;
	}
	@Exportable(columnName="Num de Cotizacion", columnOrder=5)
	public Long getIdCotizacion() {
		return idCotizacion;
	}
	public void setIdCotizacion(Long idCotizacion) {
		this.idCotizacion = idCotizacion;
	}
	@Exportable(columnName="Num de Poliza", columnOrder=6)
	public String getNumPoliza() {
		return numPoliza;
	}
	public void setNumPoliza(String numPoliza) {
		this.numPoliza = numPoliza;
	}
	@Exportable(columnName="Num de Liquidacion", columnOrder=7)
	public String getNumLiquidacion() {
		return numLiquidacion;
	}
	public void setNumLiquidacion(String numLiquidacion) {
		this.numLiquidacion = numLiquidacion;
	}
	@Exportable(columnName="Autorizacion de Prosa", columnOrder=8)
	public String getAutProsa() {
		return autProsa;
	}
	public void setAutProsa(String autProsa) {
		this.autProsa = autProsa;
	}
	@Exportable(columnName="Fecha Vigencia Inicio", columnOrder=9)
	public Date getFechaIniVig() {
		return fechaIniVig;
	}
	public void setFechaIniVig(Date fechaIniVig) {
		this.fechaIniVig = fechaIniVig;
	}
	@Exportable(columnName="Fecha Emision", columnOrder=10)
	public Date getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	@Exportable(columnName="Fecha Fin Vigencia", columnOrder=11)
	public Date getFechaFinVig() {
		return fechaFinVig;
	}
	public void setFechaFinVig(Date fechaFinVig) {
		this.fechaFinVig = fechaFinVig;
	}
	@Exportable(columnName="Linea de Negocio", columnOrder=12)
	public String getLineaNegocio() {
		return lineaNegocio;
	}
	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}
	@Exportable(columnName="Paquete", columnOrder=13)
	public String getPaquete() {
		return paquete;
	}
	public void setPaquete(String paquete) {
		this.paquete = paquete;
	}
	@Exportable(columnName="Numero de Empleado", columnOrder=14)
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	@Exportable(columnName="Tipo Persona Cliente", columnOrder=15)
	public String getTipoPersona() {
		return tipoPersona;
	}
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	@Exportable(columnName="RFC Cliente", columnOrder=16)
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	@Exportable(columnName="Cliente VIP", columnOrder=17)
	public String getClienteVip() {
		return clienteVip;
	}
	public void setClienteVip(String clienteVip) {
		this.clienteVip = clienteVip;
	}
	@Exportable(columnName="Nombre o Razon Social", columnOrder=18)
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	@Exportable(columnName="Apellido Paterno Cliente", columnOrder=19)
	public String getPaternoCliente() {
		return paternoCliente;
	}
	public void setPaternoCliente(String paternoCliente) {
		this.paternoCliente = paternoCliente;
	}
	@Exportable(columnName="Apellido Materno Cliente", columnOrder=20)
	public String getMaternoCliente() {
		return maternoCliente;
	}
	public void setMaternoCliente(String maternoCliente) {
		this.maternoCliente = maternoCliente;
	}
	@Exportable(columnName="Codigo Postal Cliente", columnOrder=21)
	public String getCp() {
		return cp;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}
	@Exportable(columnName="Colonia Cliente", columnOrder=22)
	public String getColonia() {
		return colonia;
	}
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	@Exportable(columnName="Telefono Cliente", columnOrder=23)
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	@Exportable(columnName="Clave AMIS", columnOrder=24)
	public String getClvAmis() {
		return clvAmis;
	}
	public void setClvAmis(String clvAmis) {
		this.clvAmis = clvAmis;
	}
	@Exportable(columnName="Modelo", columnOrder=25)
	public Long getModelo() {
		return modelo;
	}
	public void setModelo(Long modelo) {
		this.modelo = modelo;
	}
	@Exportable(columnName="Repuve", columnOrder=26)
	public String getRepuve() {
		return repuve;
	}
	public void setRepuve(String repuve) {
		this.repuve = repuve;
	}
	@Exportable(columnName="Clave Uso", columnOrder=27)
	public String getClvUso() {
		return clvUso;
	}
	public void setClvUso(String clvUso) {
		this.clvUso = clvUso;
	}
	@Exportable(columnName="Placas", columnOrder=28)
	public String getPlacas() {
		return placas;
	}
	public void setPlacas(String placas) {
		this.placas = placas;
	}
	@Exportable(columnName="Forma de Pago", columnOrder=29)
	public String getFormaPago() {
		return formaPago;
	}
	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	@Exportable(columnName="Numero de Motor", columnOrder=30)
	public String getNumMotor() {
		return numMotor;
	}
	public void setNumMotor(String numMotor) {
		this.numMotor = numMotor;
	}
	@Exportable(columnName="Numero de Serie", columnOrder=31)
	public String getNumSerie() {
		return numSerie;
	}
	public void setNumSerie(String numSerie) {
		this.numSerie = numSerie;
	}
	@Exportable(columnName="Valor Comercial", columnOrder=32)
	public String getValorComer() {
		return valorComer;
	}
	public void setValorComer(String valorComer) {
		this.valorComer = valorComer;
	}
	@Exportable(columnName="Total Pasajeros", columnOrder=33)
	public String gettPasajeros() {
		return tPasajeros;
	}
	public void settPasajeros(String tPasajeros) {
		this.tPasajeros = tPasajeros;
	}
	@Exportable(columnName="Descripcion del vehiculo", columnOrder=34)
	public String getDescVehiculo() {
		return descVehiculo;
	}
	public void setDescVehiculo(String descVehiculo) {
		this.descVehiculo = descVehiculo;
	}
	@Exportable(columnName="Prima Total", columnOrder=35)
	public Double getPrimaTotal() {
		return primaTotal;
	}
	public void setPrimaTotal(Double primaTotal) {
		this.primaTotal = primaTotal;
	}
	@Exportable(columnName="Deducible Daños Materiales", columnOrder=36)
	public Long getDeducMateriales() {
		return deducMateriales;
	}
	public void setDeducMateriales(Long deducMateriales) {
		this.deducMateriales = deducMateriales;
	}
	@Exportable(columnName="Deducible Robo Total", columnOrder=37)
	public Long getDeducRoboTotal() {
		return deducRoboTotal;
	}
	public void setDeducRoboTotal(Long deducRoboTotal) {
		this.deducRoboTotal = deducRoboTotal;
	}
	@Exportable(columnName="Limite RC Terceros", columnOrder=38)
	public Double getLimiteRCTerc() {
		return limiteRCTerc;
	}
	public void setLimiteRCTerc(Double limiteRCTerc) {
		this.limiteRCTerc = limiteRCTerc;
	}
	@Exportable(columnName="Deducible RC Terceros", columnOrder=39)
	public Long getDeducRCTerc() {
		return deducRCTerc;
	}
	public void setDeducRCTerc(Long deducRCTerc) {
		this.deducRCTerc = deducRCTerc;
	}
	@Exportable(columnName="Limite Gastos Medicos", columnOrder=40)
	public Double getLimiteGMedicos() {
		return limiteGMedicos;
	}
	public void setLimiteGMedicos(Double limiteGMedicos) {
		this.limiteGMedicos = limiteGMedicos;
	}
	@Exportable(columnName="Limite Muerte", columnOrder=41)
	public Double getLimiteMuerte() {
		return limiteMuerte;
	}
	public void setLimiteMuerte(Double limiteMuerte) {
		this.limiteMuerte = limiteMuerte;
	}
	@Exportable(columnName="Limite RC Viajero", columnOrder=42)
	public Double getLimiteRCViaj() {
		return limiteRCViaj;
	}
	public void setLimiteRCViaj(Double limiteRCViaj) {
		this.limiteRCViaj = limiteRCViaj;
	}
	@Exportable(columnName="Asistencia Juridica", columnOrder=43)
	public String getaJuridica() {
		return aJuridica;
	}
	public void setaJuridica(String aJuridica) {
		this.aJuridica = aJuridica;
	}
	@Exportable(columnName="Limite Adapt y Conversiones", columnOrder=44)
	public Double getLimiteAdapt() {
		return limiteAdapt;
	}
	public void setLimiteAdapt(Double limiteAdapt) {
		this.limiteAdapt = limiteAdapt;
	}
	@Exportable(columnName="Limite Equipo Especial", columnOrder=45)
	public Double getLimiteEquipo() {
		return limiteEquipo;
	}
	public void setLimiteEquipo(Double limiteEquipo) {
		this.limiteEquipo = limiteEquipo;
	}
	@Exportable(columnName="Deducible Equipo Especial", columnOrder=46)
	public Long getDeducEquipo() {
		return deducEquipo;
	}
	public void setDeducEquipo(Long deducEquipo) {
		this.deducEquipo = deducEquipo;
	}
	@Exportable(columnName="Igualacion", columnOrder=47)
	public String getIgualacion() {
		return igualacion;
	}
	public void setIgualacion(String igualacion) {
		this.igualacion = igualacion;
	}
	@Exportable(columnName="Derechos", columnOrder=48)
	public Double getDerechos() {
		return derechos;
	}
	public void setDerechos(Double derechos) {
		this.derechos = derechos;
	}
	@Exportable(columnName="Solicitar Autorizacion", columnOrder=49)
	public String getsAutorizacion() {
		return sAutorizacion;
	}
	public void setsAutorizacion(String sAutorizacion) {
		this.sAutorizacion = sAutorizacion;
	}
	@Exportable(columnName="Causa Autorizacion", columnOrder=50)
	public String getcAutorizacion() {
		return cAutorizacion;
	}
	public void setcAutorizacion(String cAutorizacion) {
		this.cAutorizacion = cAutorizacion;
	}
	@Exportable(columnName="Descripcion Equipo Especial", columnOrder=51)
	public String getDescEquipo() {
		return descEquipo;
	}
	public void setDescEquipo(String descEquipo) {
		this.descEquipo = descEquipo;
	}
	@Exportable(columnName="Descripcion Adapt y Conversion", columnOrder=52)
	public String getDescAdaptacion() {
		return descAdaptacion;
	}
	public void setDescAdaptacion(String descAdaptacion) {
		this.descAdaptacion = descAdaptacion;
	}
	@Exportable(columnName="Nombre Conductor", columnOrder=53)
	public String getNombreConduc() {
		return nombreConduc;
	}
	public void setNombreConduc(String nombreConduc) {
		this.nombreConduc = nombreConduc;
	}
	@Exportable(columnName="Apellido Paterno", columnOrder=54)
	public String getPaternoConduc() {
		return paternoConduc;
	}
	public void setPaternoConduc(String paternoConduc) {
		this.paternoConduc = paternoConduc;
	}
	@Exportable(columnName="Apellido Materno", columnOrder=55)
	public String getMaternoConduc() {
		return maternoConduc;
	}
	public void setMaternoConduc(String maternoConduc) {
		this.maternoConduc = maternoConduc;
	}
	@Exportable(columnName="Numero Licencia", columnOrder=56)
	public String getLicencia() {
		return licencia;
	}
	public void setLicencia(String licencia) {
		this.licencia = licencia;
	}
	@Exportable(columnName="Fecha de Nacimiento", columnOrder=57)
	public Date getFechaNacim() {
		return fechaNacim;
	}
	public void setFechaNacim(Date fechaNacim) {
		this.fechaNacim = fechaNacim;
	}
	@Exportable(columnName="Ocupacion", columnOrder=58)
	public String getOcupacion() {
		return ocupacion;
	}
	public void setOcupacion(String ocupacion) {
		this.ocupacion = ocupacion;
	}
	@Exportable(columnName="ID Cliente", columnOrder=59)
	public Long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	@Exportable(columnName="Numero Remolques", columnOrder=60)
	public Long getRemolques() {
		return remolques;
	}
	public void setRemolques(Long remolques) {
		this.remolques = remolques;
	}
	@Exportable(columnName="Tipo Carga", columnOrder=61)
	public String getTipoCarga() {
		return tipoCarga;
	}
	public void setTipoCarga(String tipoCarga) {
		this.tipoCarga = tipoCarga;
	}
	@Exportable(columnName="Observaciones", columnOrder=62)
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	@Exportable(columnName="Nombre Asegurado", columnOrder=63)
	public String getNombreAseg() {
		return nombreAseg;
	}
	public void setNombreAseg(String nombreAseg) {
		this.nombreAseg = nombreAseg;
	}
	@Exportable(columnName="Limite Accidente Conductor", columnOrder=64)
	public String getLimiteAcciden() {
		return limiteAcciden;
	}
	public void setLimiteAcciden(String limiteAcciden) {
		this.limiteAcciden = limiteAcciden;
	}
	@Exportable(columnName="% Recargo Pago Fraccionado", columnOrder=65)
	public Long getRecargoPago() {
		return recargoPago;
	}
	public void setRecargoPago(Long recargoPago) {
		this.recargoPago = recargoPago;
	}
	@Exportable(columnName="Suma Aseg Daños Materiales", columnOrder=66)
	public Double getSumaDanios() {
		return sumaDanios;
	}
	public void setSumaDanios(Double sumaDanios) {
		this.sumaDanios = sumaDanios;
	}
	@Exportable(columnName="Suma Asegurada Robo Total", columnOrder=67)
	public Double getSumaRobo() {
		return sumaRobo;
	}
	public void setSumaRobo(Double sumaRobo) {
		this.sumaRobo = sumaRobo;
	}
	@Exportable(columnName="Correo Electronico", columnOrder=68)
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	@Exportable(columnName="Alta de Condicion Especial", columnOrder=69)
	public String getAltaCondicion() {
		return altaCondicion;
	}
	public void setAltaCondicion(String altaCondicion) {
		this.altaCondicion = altaCondicion;
	}
	@Exportable(columnName="Baja de Condicion Especial", columnOrder=70)
	public String getBajaCondicion() {
		return bajaCondicion;
	}
	public void setBajaCondicion(String bajaCondicion) {
		this.bajaCondicion = bajaCondicion;
	}
	@Override
	public String toString() {
		return "LayoutRenovacion [estatus=" + estatus + ", idAgente="
				+ idAgente + ", moneda=" + moneda + ", centroEmisor="
				+ centroEmisor + ", ejecutivo=" + ejecutivo + ", idCotizacion="
				+ idCotizacion + ", numPoliza=" + numPoliza
				+ ", numLiquidacion=" + numLiquidacion + ", autProsa="
				+ autProsa + ", fechaIniVig=" + fechaIniVig + ", fechaEmision="
				+ fechaEmision + ", fechaFinVig=" + fechaFinVig
				+ ", lineaNegocio=" + lineaNegocio + ", paquete=" + paquete
				+ ", idUsuario=" + idUsuario + ", tipoPersona=" + tipoPersona
				+ ", rfc=" + rfc + ", clienteVip=" + clienteVip
				+ ", nombreCliente=" + nombreCliente + ", paternoCliente="
				+ paternoCliente + ", maternoCliente=" + maternoCliente
				+ ", cp=" + cp + ", colonia=" + colonia + ", telefono="
				+ telefono + ", clvAmis=" + clvAmis + ", modelo=" + modelo
				+ ", repuve=" + repuve + ", clvUso=" + clvUso + ", placas="
				+ placas + ", formaPago=" + formaPago + ", numMotor="
				+ numMotor + ", numSerie=" + numSerie + ", valorComer="
				+ valorComer + ", tPasajeros=" + tPasajeros + ", descVehiculo="
				+ descVehiculo + ", primaTotal=" + primaTotal
				+ ", deducMateriales=" + deducMateriales + ", deducRoboTotal="
				+ deducRoboTotal + ", limiteRCTerc=" + limiteRCTerc
				+ ", deducRCTerc=" + deducRCTerc + ", limiteGMedicos="
				+ limiteGMedicos + ", limiteMuerte=" + limiteMuerte
				+ ", limiteRCViaj=" + limiteRCViaj + ", aJuridica=" + aJuridica
				+ ", limiteAdapt=" + limiteAdapt + ", limiteEquipo="
				+ limiteEquipo + ", deducEquipo=" + deducEquipo
				+ ", igualacion=" + igualacion + ", derechos=" + derechos
				+ ", sAutorizacion=" + sAutorizacion + ", cAutorizacion="
				+ cAutorizacion + ", descEquipo=" + descEquipo
				+ ", descAdaptacion=" + descAdaptacion + ", nombreConduc="
				+ nombreConduc + ", paternoConduc=" + paternoConduc
				+ ", maternoConduc=" + maternoConduc + ", licencia=" + licencia
				+ ", fechaNacim=" + fechaNacim + ", ocupacion=" + ocupacion
				+ ", idCliente=" + idCliente + ", remolques=" + remolques
				+ ", tipoCarga=" + tipoCarga + ", observaciones="
				+ observaciones + ", nombreAseg=" + nombreAseg
				+ ", limiteAcciden=" + limiteAcciden + ", recargoPago="
				+ recargoPago + ", sumaDanios=" + sumaDanios + ", sumaRobo="
				+ sumaRobo + ", correo=" + correo + ", altaCondicion="
				+ altaCondicion + ", bajaCondicion=" + bajaCondicion + "]";
	}	
}
