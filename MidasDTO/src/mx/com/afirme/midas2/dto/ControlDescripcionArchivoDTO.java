package mx.com.afirme.midas2.dto;

import java.io.InputStream;
import java.io.Serializable;

public class ControlDescripcionArchivoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4231528647941632629L;

	private String nombreArchivo;
	private String extension;
	private InputStream inputStream;
	
	
	
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public String getExtension() {
		return extension;
	}
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	public InputStream getInputStream() {
		return inputStream;
	}
}
