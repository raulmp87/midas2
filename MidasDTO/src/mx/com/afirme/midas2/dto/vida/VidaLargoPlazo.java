package mx.com.afirme.midas2.dto.vida;

import java.io.Serializable;
import java.math.BigDecimal;

public class VidaLargoPlazo implements Serializable{
	private static final long serialVersionUID = 4437552743464818768L;
	Integer edad;
	Integer antiguedad;
	Integer id_moneda;
	Integer mod_plan;
	Integer tipoCaducidad;
	Integer tipoMortalidad;
	String tipoFlujo;
	Integer vigencia_restante;
	BigDecimal montoBase;
	String basico;
	public Integer getEdad() {
		return edad;
	}
	public void setEdad(Integer edad) {
		this.edad = edad;
	}
	public Integer getAntiguedad() {
		return antiguedad;
	}
	public void setAntiguedad(Integer antiguedad) {
		this.antiguedad = antiguedad;
	}
	public Integer getId_moneda() {
		return id_moneda;
	}
	public void setId_moneda(Integer id_moneda) {
		this.id_moneda = id_moneda;
	}
	public Integer getMod_plan() {
		return mod_plan;
	}
	public void setMod_plan(Integer mod_plan) {
		this.mod_plan = mod_plan;
	}
	public Integer getTipoCaducidad() {
		return tipoCaducidad;
	}
	public void setTipoCaducidad(Integer tipoCaducidad) {
		this.tipoCaducidad = tipoCaducidad;
	}
	public String getTipoFlujo() {
		return tipoFlujo;
	}
	public void setTipoFlujo(String tipoFlujo) {
		this.tipoFlujo = tipoFlujo;
	}
	public Integer getVigencia_restante() {
		return vigencia_restante;
	}
	public void setVigencia_restante(Integer vigencia_restante) {
		this.vigencia_restante = vigencia_restante;
	}
	public BigDecimal getMontoBase() {
		return montoBase;
	}
	public void setMontoBase(BigDecimal montoBase) {
		this.montoBase = montoBase;
	}
	public String getBasico() {
		return basico;
	}
	public void setBasico(String basico) {
		this.basico = basico;
	}
	public Integer getTipoMortalidad() {
		return tipoMortalidad;
	}
	public void setTipoMortalidad(Integer tipoMortalidad) {
		this.tipoMortalidad = tipoMortalidad;
	}
	
}
