package mx.com.afirme.midas2.dto;

import java.io.Serializable;
import java.util.List;

public class RegistroDinamicoDTO implements Serializable {

	private static final long serialVersionUID = 1138180459937192554L;

	private List<ControlDinamicoDTO> controles;
	private Integer numControlesVista;
	public static final String TARIFA = "T";
	public static final String VARIABLES_MODIFICADORAS_PRIMA = "VMP";
	public static final String POLIZA_DANIO = "POLDAN";
	public static final String POLIZA_AUTO = "POLAUT";
	
	/**
	 * Contiene la clave del tipo de registro (tarifa por descripcion, por prima, etc.)
	 * Así como los atributos que conforman el ID específico.
	 */
	private String id;
	
	private String javascriptOnLoad;

	public List<ControlDinamicoDTO> getControles() {
		return controles;
	}

	public void setControles(List<ControlDinamicoDTO> controles) {
		this.controles = controles;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getJavascriptOnLoad() {
		return javascriptOnLoad;
	}

	public void setJavascriptOnLoad(String javascriptOnLoad) {
		this.javascriptOnLoad = javascriptOnLoad;
	}

	public void setNumControlesVista(Integer numControlesVista) {
		this.numControlesVista = numControlesVista;
	}

	public Integer getNumControlesVista() {
		return numControlesVista;
	}
	
}
