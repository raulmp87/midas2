package mx.com.afirme.midas2.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class DeudorPorPrimaDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2325015441153753725L;
	
	private String rango;
	private String desc_moneda;
	private String uen;
	private String id_ramo;
	private String nom_ramo;
	private String num_poliza;
	private String nom_solicitante;
	private String cve_t_recibo;
	private Date f_vencimiento;
	private BigDecimal num_folio;
	private String nom_producto;
	private String nom_linea_neg;
	private String endoso;
	private BigDecimal prima_neta;
	private BigDecimal bonif_comis;
	private BigDecimal recargos;
	private BigDecimal derechos;
	private BigDecimal iva;
	private BigDecimal prima_total;
	private BigDecimal com_pn_pf;
	private BigDecimal com_pn_pm;
	private BigDecimal com_rpf_pf;
	private BigDecimal com_rpf_pm;
	private BigDecimal id_agente;
	private String nombre;
	private String nom_supervisoria;
	private String nom_gerencia;
	private String nom_oficina;
	private BigDecimal id_cotizacion;
	private BigDecimal id_recibo;
	private Date f_prorroga;
	private Integer dias_vigencia;
	private String sit_poliza;
	private Date f_ini_vigencia;
	private Date f_fin_vigencia;	
	
	
	public void setRango(String rango) {
		this.rango = rango;
	}
	public String getRango() {
		return rango;
	}
	public void setDesc_moneda(String desc_moneda) {
		this.desc_moneda = desc_moneda;
	}
	public String getDesc_moneda() {
		return desc_moneda;
	}
	public void setUen(String uen) {
		this.uen = uen;
	}
	public String getUen() {
		return uen;
	}
	public void setId_ramo(String id_ramo) {
		this.id_ramo = id_ramo;
	}
	public String getId_ramo() {
		return id_ramo;
	}
	public void setNom_ramo(String nom_ramo) {
		this.nom_ramo = nom_ramo;
	}
	public String getNom_ramo() {
		return nom_ramo;
	}
	public void setNum_poliza(String num_poliza) {
		this.num_poliza = num_poliza;
	}
	public String getNum_poliza() {
		return num_poliza;
	}
	public void setNom_solicitante(String nom_solicitante) {
		this.nom_solicitante = nom_solicitante;
	}
	public String getNom_solicitante() {
		return nom_solicitante;
	}
	public void setCve_t_recibo(String cve_t_recibo) {
		this.cve_t_recibo = cve_t_recibo;
	}
	public String getCve_t_recibo() {
		return cve_t_recibo;
	}
	public void setF_vencimiento(Date f_vencimiento) {
		this.f_vencimiento = f_vencimiento;
	}
	public Date getF_vencimiento() {
		return f_vencimiento;
	}
	public void setNum_folio(BigDecimal num_folio) {
		this.num_folio = num_folio;
	}
	public BigDecimal getNum_folio() {
		return num_folio;
	}
	public void setNom_producto(String nom_producto) {
		this.nom_producto = nom_producto;
	}
	public String getNom_producto() {
		return nom_producto;
	}
	public void setNom_linea_neg(String nom_linea_neg) {
		this.nom_linea_neg = nom_linea_neg;
	}
	public String getNom_linea_neg() {
		return nom_linea_neg;
	}
	public void setEndoso(String endoso) {
		this.endoso = endoso;
	}
	public String getEndoso() {
		return endoso;
	}
	public void setPrima_neta(BigDecimal prima_neta) {
		this.prima_neta = prima_neta;
	}
	public BigDecimal getPrima_neta() {
		return prima_neta;
	}
	public void setBonif_comis(BigDecimal bonif_comis) {
		this.bonif_comis = bonif_comis;
	}
	public BigDecimal getBonif_comis() {
		return bonif_comis;
	}
	public void setRecargos(BigDecimal recargos) {
		this.recargos = recargos;
	}
	public BigDecimal getRecargos() {
		return recargos;
	}
	public void setDerechos(BigDecimal derechos) {
		this.derechos = derechos;
	}
	public BigDecimal getDerechos() {
		return derechos;
	}
	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}
	public BigDecimal getIva() {
		return iva;
	}
	public void setPrima_total(BigDecimal prima_total) {
		this.prima_total = prima_total;
	}
	public BigDecimal getPrima_total() {
		return prima_total;
	}
	public void setCom_pn_pf(BigDecimal com_pn_pf) {
		this.com_pn_pf = com_pn_pf;
	}
	public BigDecimal getCom_pn_pf() {
		return com_pn_pf;
	}
	public void setCom_pn_pm(BigDecimal com_pn_pm) {
		this.com_pn_pm = com_pn_pm;
	}
	public BigDecimal getCom_pn_pm() {
		return com_pn_pm;
	}
	public void setCom_rpf_pf(BigDecimal com_rpf_pf) {
		this.com_rpf_pf = com_rpf_pf;
	}
	public BigDecimal getCom_rpf_pf() {
		return com_rpf_pf;
	}
	public void setCom_rpf_pm(BigDecimal com_rpf_pm) {
		this.com_rpf_pm = com_rpf_pm;
	}
	public BigDecimal getCom_rpf_pm() {
		return com_rpf_pm;
	}
	public void setId_agente(BigDecimal id_agente) {
		this.id_agente = id_agente;
	}
	public BigDecimal getId_agente() {
		return id_agente;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNom_supervisoria(String nom_supervisoria) {
		this.nom_supervisoria = nom_supervisoria;
	}
	public String getNom_supervisoria() {
		return nom_supervisoria;
	}
	public void setNom_gerencia(String nom_gerencia) {
		this.nom_gerencia = nom_gerencia;
	}
	public String getNom_gerencia() {
		return nom_gerencia;
	}
	public void setNom_oficina(String nom_oficina) {
		this.nom_oficina = nom_oficina;
	}
	public String getNom_oficina() {
		return nom_oficina;
	}
	public void setId_cotizacion(BigDecimal id_cotizacion) {
		this.id_cotizacion = id_cotizacion;
	}
	public BigDecimal getId_cotizacion() {
		return id_cotizacion;
	}
	public void setId_recibo(BigDecimal id_recibo) {
		this.id_recibo = id_recibo;
	}
	public BigDecimal getId_recibo() {
		return id_recibo;
	}
	public Date getF_prorroga() {
		return f_prorroga;
	}
	public void setF_prorroga(Date f_prorroga) {
		this.f_prorroga = f_prorroga;
	}		
	public Integer getDias_vigencia() {
		return dias_vigencia;
	}
	public void setDias_vigencia(Integer dias_vigencia) {
		this.dias_vigencia = dias_vigencia;
	}
	public String getSit_poliza() {
		return sit_poliza;
	}
	public void setSit_poliza(String sit_poliza) {
		this.sit_poliza = sit_poliza;
	}
	public Date getF_ini_vigencia() {
		return f_ini_vigencia;
	}
	public void setF_ini_vigencia(Date f_ini_vigencia) {
		this.f_ini_vigencia = f_ini_vigencia;
	}
	public Date getF_fin_vigencia() {
		return f_fin_vigencia;
	}
	public void setF_fin_vigencia(Date f_fin_vigencia) {
		this.f_fin_vigencia = f_fin_vigencia;
	}	
	
}
