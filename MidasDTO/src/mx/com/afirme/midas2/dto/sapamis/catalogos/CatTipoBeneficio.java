package mx.com.afirme.midas2.dto.sapamis.catalogos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="SAPAMISCATTIPOBENEFICIO", schema = "MIDAS")
public class CatTipoBeneficio implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="IDSAPAMISCATTIPOBENEFICIO")
	private Long id;

	@Column(name="SAPAMISCATTIPOBENEFICIO")
	private String descCatTipoBeneficio;
    
    @Column(name="ESTATUS")
    private long estatus;
	
	/**
	 * GETTERS AND SETTERS
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescCatTipoBeneficio() {
		return descCatTipoBeneficio;
	}

	public void setDescCatTipoBeneficio(String descCatTipoBeneficio) {
		this.descCatTipoBeneficio = descCatTipoBeneficio;
	}
    
    public long getEstatus() {
        return estatus;
    }

    public void setEstatus(long estatus) {
        this.estatus = estatus;
    }

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}