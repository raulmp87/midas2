package mx.com.afirme.midas2.dto.sapamis.otros;

import java.io.Serializable;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the SAP_AMIS_CONTROLEMISION database table.
 * 
 */
@Entity
@Table(name="SAP_AMIS_CONTROLEMISION", schema = "MIDAS")
public class SapAmisControlEmision implements Entidad, Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQSAP_AMIS_CONTROLEMISIONID")
	@SequenceGenerator(name="SEQSAP_AMIS_CONTROLEMISIONID", schema = "MIDAS", sequenceName="SEQSAP_AMIS_CONTROLEMISIONID",allocationSize=1)
	@Column(name="IDSAP_AMIS_CONTROLEMISION")	
	private long idSapAmisControlEmision;

	@Column(name="POLIZA")
	private String poliza;

	@Column(name="INCISO")
	private String inciso;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHA_INICIO_VIGENCIA")
	private Date fechaInicioVigencia;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHA_FIN_VIGENCIA")
	private Date fechaFinVigencia;

	@Column(name="SERIE")
	private String serie;

	@Column(name="MARCA")
	private long marca;

	@Column(name="TIPO_SUBMARCA")
	private long tipoSubmarca;

	@Column(name="MODELO")
	private long modelo;

	@Column(name="TIPO_TRANSPORTE")
	private String tipoTransporte;

	@Column(name="TIPO_PERSONA")
	private long tipoPersona;

	@Column(name="TIPO_SERVICIO")
	private String tipoServicio;

	@Column(name="CLIENTE1_PATERNO")
	private String cliente1ApPaterno;

	@Column(name="CLIENTE1_MATERNO")
	private String cliente1ApMaterno;

	@Column(name="CLIENTE1_NOMBRE_RAZON_SOCIAL")
	private String cliente1NombreRazonSocial;

	@Column(name="CLIENTE1_RFC")
	private String cliente1RFC;

	@Column(name="CLIENTE1_CURP")
	private String cliente1CURP;

	@Column(name="CLIENTE2_PATERNO")
	private String cliente2ApPaterno;

	@Column(name="CLIENTE2_MATERNO")
	private String cliente2ApMaterno;

	@Column(name="CLIENTE2_NOMBRE")
	private String cliente2Nombre;

	@Column(name="BENEFICIARIO_AP")
	private String beneficiarioApPaterno;

	@Column(name="BENEFICIARIO_AM")
	private String beneficiarioApMaterno;

	@Column(name="BENEFICIARIO_NOMBRE")
	private String beneficiarioNombre;

	@Column(name="BENEFICIARIO_RFC")
	private String beneficiarioRFC;

	@Column(name="BENEFICIARIO_CURP")
	private String beneficiarioCURP;

	@Column(name="CANAL_VENTA")
	private long canalVenta;

	@Column(name="AGENTE")
	private String agente;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHA_CANCELACION")
	private Date fechaCancelacion;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHA_REHABILITACION")
	private Date fechaRehabilitacion;

	@Column(name="BANDERA_OPERACION")
	private String banderaOperacion;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="RECORDFROM")
	private Date recordFrom;
    
    @Column(name="SESSION_ID")
	private long sessionid;
    
    @Column(name="PROCESADO")
	private String procesado;
    
    @Column(name="PROCESAR")
	private String procesar;

    @Column(name="IDMARCA")
	private long idMarca;
    
    @Column(name="IDESTILO")
	private String idEstilo;
    
    @Temporal( TemporalType.DATE)
	@Column(name="VALIDFROM")
	private Date validFrom;
    
    @Column(name="IDTOCOTIZACION")
	private long idToCotizacion;
    
    @Column(name="IDSOLICITUD")
	private long idSolicitud;
    
    @Column(name="ENDOSO")
	private String endoso;
    
    @Column(name="CVETBIEN")
	private String cvetbien;
    
    @Column(name="OBSERVACIONES")
	private String observaciones;
    
    @Column(name="CVE_PAIS_ASEGURADO")
	private String cvePaisAsegurado;
    
    @Column(name="CVE_ESTADO_ASEGURADO")
	private String cveEstadoAsegurado;
    
    @Column(name="CVE_CIUDAD_ASEGURADO")
	private String cveCiudadAsegurado;
    
    @Column(name="CVE_PAIS_EMISION")
	private String cvePaisEmision;
    
    @Column(name="CVE_ESTADO_EMISION")
	private String cveEstadoEmision;
    
    @Column(name="CVE_CIUDAD_EMISION")
	private String cveCiudadEmision;
    
    @Column(name="ESTATUSMIGRACION")
	private long estatusMigracion;
    
    /**
     * Metodos Getters y Setters
     */

    public String getAgente() {
		return this.agente;
	}

	public long getEstatusMigracion() {
		return estatusMigracion;
	}

	public void setEstatusMigracion(long estatusMigracion) {
		this.estatusMigracion = estatusMigracion;
	}

	public void setAgente(String agente) {
		this.agente = agente;
	}

	public String getBanderaOperacion() {
		return this.banderaOperacion;
	}

	public void setBanderaOperacion(String banderaOperacion) {
		this.banderaOperacion = banderaOperacion;
	}

	public String getBeneficiarioNombre() {
		return this.beneficiarioNombre;
	}

	public void setBeneficiarioNombre(String beneficiarioNombre) {
		this.beneficiarioNombre = beneficiarioNombre;
	}

	public long getCanalVenta() {
		return this.canalVenta;
	}

	public void setCanalVenta(long canalVenta) {
		this.canalVenta = canalVenta;
	}

	public String getCliente1NombreRazonSocial() {
		return this.cliente1NombreRazonSocial;
	}

	public void setCliente1NombreRazonSocial(String cliente1NombreRazonSocial) {
		this.cliente1NombreRazonSocial = cliente1NombreRazonSocial;
	}

	public String getCliente2Nombre() {
		return this.cliente2Nombre;
	}

	public void setCliente2Nombre(String cliente2Nombre) {
		this.cliente2Nombre = cliente2Nombre;
	}

	public Date getFechaCancelacion() {
		return this.fechaCancelacion;
	}

	public void setFechaCancelacion(Date fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}

	public Date getFechaFinVigencia() {
		return this.fechaFinVigencia;
	}

	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	public Date getFechaInicioVigencia() {
		return this.fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	public Date getFechaRehabilitacion() {
		return this.fechaRehabilitacion;
	}

	public void setFechaRehabilitacion(Date fechaRehabilitacion) {
		this.fechaRehabilitacion = fechaRehabilitacion;
	}

	public String getInciso() {
		return this.inciso;
	}

	public void setInciso(String inciso) {
		this.inciso = inciso;
	}

	public long getMarca() {
		return this.marca;
	}

	public void setMarca(long marca) {
		this.marca = marca;
	}

	public long getModelo() {
		return this.modelo;
	}

	public void setModelo(long modelo) {
		this.modelo = modelo;
	}

	public String getPoliza() {
		return this.poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	public Date getRecordFrom() {
		return this.recordFrom;
	}

	public void setRecordFrom(Date recordFrom) {
		this.recordFrom = recordFrom;
	}

	public String getSerie() {
		return this.serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public long getTipoPersona() {
		return this.tipoPersona;
	}

	public void setTipoPersona(long tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getTipoServicio() {
		return this.tipoServicio;
	}

	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

	public long getTipoSubmarca() {
		return this.tipoSubmarca;
	}

	public void setTipoSubmarca(long tipoSubmarca) {
		this.tipoSubmarca = tipoSubmarca;
	}

	public String getTipoTransporte() {
		return this.tipoTransporte;
	}

	public void setTipoTransporte(String tipoTransporte) {
		this.tipoTransporte = tipoTransporte;
	}

	public long getSessionid() {
		return sessionid;
	}

	public void setSessionid(long sessionid) {
		this.sessionid = sessionid;
	}

	public String getProcesado() {
		return procesado;
	}

	public void setProcesado(String procesado) {
		this.procesado = procesado;
	}

	public String getProcesar() {
		return procesar;
	}

	public void setProcesar(String procesar) {
		this.procesar = procesar;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getEndoso() {
		return endoso;
	}

	public void setEndoso(String endoso) {
		this.endoso = endoso;
	}

	public String getCvetbien() {
		return cvetbien;
	}

	public void setCvetbien(String cvetbien) {
		this.cvetbien = cvetbien;
	}

	public long getIdSapAmisControlEmision() {
		return idSapAmisControlEmision;
	}

	public void setIdSapAmisControlEmision(long idSapAmisControlEmision) {
		this.idSapAmisControlEmision = idSapAmisControlEmision;
	}

	public String getCliente1ApPaterno() {
		return cliente1ApPaterno;
	}

	public void setCliente1ApPaterno(String cliente1ApPaterno) {
		this.cliente1ApPaterno = cliente1ApPaterno;
	}

	public String getCliente1ApMaterno() {
		return cliente1ApMaterno;
	}

	public void setCliente1ApMaterno(String cliente1ApMaterno) {
		this.cliente1ApMaterno = cliente1ApMaterno;
	}

	public String getCliente1RFC() {
		return cliente1RFC;
	}

	public void setCliente1RFC(String cliente1rfc) {
		cliente1RFC = cliente1rfc;
	}

	public String getCliente1CURP() {
		return cliente1CURP;
	}

	public void setCliente1CURP(String cliente1curp) {
		cliente1CURP = cliente1curp;
	}

	public String getCliente2ApPaterno() {
		return cliente2ApPaterno;
	}

	public void setCliente2ApPaterno(String cliente2ApPaterno) {
		this.cliente2ApPaterno = cliente2ApPaterno;
	}

	public String getCliente2ApMaterno() {
		return cliente2ApMaterno;
	}

	public void setCliente2ApMaterno(String cliente2ApMaterno) {
		this.cliente2ApMaterno = cliente2ApMaterno;
	}

	public String getBeneficiarioApPaterno() {
		return beneficiarioApPaterno;
	}

	public void setBeneficiarioApPaterno(String beneficiarioApPaterno) {
		this.beneficiarioApPaterno = beneficiarioApPaterno;
	}

	public String getBeneficiarioApMaterno() {
		return beneficiarioApMaterno;
	}

	public void setBeneficiarioApMaterno(String beneficiarioApMaterno) {
		this.beneficiarioApMaterno = beneficiarioApMaterno;
	}

	public String getBeneficiarioRFC() {
		return beneficiarioRFC;
	}

	public void setBeneficiarioRFC(String beneficiarioRFC) {
		this.beneficiarioRFC = beneficiarioRFC;
	}

	public String getBeneficiarioCURP() {
		return beneficiarioCURP;
	}

	public void setBeneficiarioCURP(String beneficiarioCURP) {
		this.beneficiarioCURP = beneficiarioCURP;
	}

	public long getIdMarca() {
		return idMarca;
	}

	public void setIdMarca(long idMarca) {
		this.idMarca = idMarca;
	}

	public String getIdEstilo() {
		return idEstilo;
	}

	public void setIdEstilo(String idEstilo) {
		this.idEstilo = idEstilo;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public long getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(long idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public long getIdSolicitud() {
		return idSolicitud;
	}

	public void setIdSolicitud(long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	public String getCveEstadoAsegurado() {
		return cveEstadoAsegurado;
	}

	public void setCveEstadoAsegurado(String cveEstadoAsegurado) {
		this.cveEstadoAsegurado = cveEstadoAsegurado;
	}

	public String getCveCiudadAsegurado() {
		return cveCiudadAsegurado;
	}

	public void setCveCiudadAsegurado(String cveCiudadAsegurado) {
		this.cveCiudadAsegurado = cveCiudadAsegurado;
	}

	public String getCveEstadoEmision() {
		return cveEstadoEmision;
	}

	public void setCveEstadoEmision(String cveEstadoEmision) {
		this.cveEstadoEmision = cveEstadoEmision;
	}

	public String getCveCiudadEmision() {
		return cveCiudadEmision;
	}

	public void setCveCiudadEmision(String cveCiudadEmision) {
		this.cveCiudadEmision = cveCiudadEmision;
	}

	public String getCvePaisAsegurado() {
		return cvePaisAsegurado;
	}

	public void setCvePaisAsegurado(String cvePaisAsegurado) {
		this.cvePaisAsegurado = cvePaisAsegurado;
	}

	public String getCvePaisEmision() {
		return cvePaisEmision;
	}

	public void setCvePaisEmision(String cvePaisEmision) {
		this.cvePaisEmision = cvePaisEmision;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Object getKey() {
		// TODO Auto-generated method stub
		return getIdSapAmisControlEmision()==0?null:getIdSapAmisControlEmision();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
}

