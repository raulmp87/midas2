package mx.com.afirme.midas2.dto.sapamis.catalogos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/******************************************************************************
 * Entidad para el manejo del catalogo Sistemas (Emision, Siniestros, etc...)
 * 
 * @author Eduardo.Chavez
 * 
 * 		Table:		SAPAMISCATSISTEMAS
 * 		Schema:		MIDAS
 * 		Sequence:	SEQSAPAMISCATSISTEMAS
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 * 		
 ******************************************************************************/
@Entity
@Table(name="SAPAMISCATSISTEMAS", schema = "MIDAS")
public class CatSistemas implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQSAPAMISCATSISTEMAS")
	@SequenceGenerator(name="SEQSAPAMISCATSISTEMAS", schema = "MIDAS", sequenceName="SEQSAPAMISCATSISTEMAS",allocationSize=1)
	@Column(name="IDSAPAMISCATSISTEMAS")
	private Long id;
	
	@Column(name="SAPAMISCATSISTEMAS")
	private String descCatSistemas;
    
    @Column(name="ESTATUSPROCESOEXTRACCION")
    private long estatusProcesoExtraccion;
    
    @Column(name="ESTATUSPROCESOENVIO")
    private long estatusProcesoEnvio;
    
    @Column(name="ESTATUS")
    private long estatus;
	
	/*****************************
	 * Inician Getters y Setters *
	 *****************************/
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescCatSistemas() {
		return descCatSistemas;
	}

	public void setDescCatSistemas(String descCatSistemas) {
		this.descCatSistemas = descCatSistemas;
	}
    
    public long getEstatus() {
        return estatus;
    }

    public void setEstatus(long estatus) {
        this.estatus = estatus;
    }
	
	public long getEstatusProcesoExtraccion() {
		return estatusProcesoExtraccion;
	}

	public void setEstatusProcesoExtraccion(long estatusProcesoExtraccion) {
		this.estatusProcesoExtraccion = estatusProcesoExtraccion;
	}

	public long getEstatusProcesoEnvio() {
		return estatusProcesoEnvio;
	}

	public void setEstatusProcesoEnvio(long estatusProcesoEnvio) {
		this.estatusProcesoEnvio = estatusProcesoEnvio;
	}

	/************************************************
	 * Mètodos implementados de la interfaz Entidad *
	 * Para el uso generico de los DAO y Servicios  *
	 ************************************************/

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}
