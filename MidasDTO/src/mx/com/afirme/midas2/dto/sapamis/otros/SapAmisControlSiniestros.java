package mx.com.afirme.midas2.dto.sapamis.otros;

import java.io.Serializable;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the SAP_AMIS_CONTROLSINIESTROS database table.
 * 
 */
@Entity
@Table(name="SAP_AMIS_CONTROLSINIESTROS", schema = "MIDAS")
public class SapAmisControlSiniestros implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQSAP_AMIS_CONTROLSINIESTROS")
	@SequenceGenerator(name="SEQSAP_AMIS_CONTROLSINIESTROS", schema = "MIDAS", sequenceName="SEQSAP_AMIS_CONTROLSINIESTROS",allocationSize=1)
	@Column(name="IDSAP_AMIS_CONTROLSINIESTROS")
	private long idSapAmisControlSiniestros;

	@Column(name="NUMEROSINIESTRO")
	private String numeroSiniestro;

	@Column(name="NUMEROPOLIZA")
	private String numeroPoliza;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHASINIESTRO")
	private Date fechaSiniestro;

	@Column(name="NUMEROSERIE")
	private String numeroSerie;

	@Column(name="CAUSASINIESTRO")
	private long causaSiniestro;

	@Column(name="MONTOOCURRIDOSINIESTRO")
	private Double montoOcurridoSiniestro;

	@Column(name="UBICACIONDELSINIESTRO_PAIS")
	private long ubicacionSiniestroPais;

	@Column(name="UBICACIONDELSINIESTRO_ESTADO")
	private long ubicacionSiniestroEstado;

	@Column(name="UBICACIONDELSINIESTRO_MUNI")
	private long ubicacionSiniestroMunicipio;
	
	@Column(name="AFECTADO")
	private long afectado;

	@Column(name="AFECTADOPATERNO")
	private String afectadoApPaterno;

	@Column(name="AFECTADOMATERNO")
	private String afectadoApMaterno;

	@Column(name="AFECTADONOMBRE")
	private String afectadoNombre;

	@Column(name="AFECTADORFC")
	private String afectadoRFC;

	@Column(name="AFECTADOCURP")
	private String afectadoCURP;

	@Column(name="CONDUCTORPATERNO")
	private String conductorApPaterno;

	@Column(name="CONDUCTORMATERNO")
	private String conductorApMaterno;

	@Column(name="CONDUCTORNOMBRE")
	private String conductorNombre;

	@Column(name="CONDUCTORRFC")
	private String conductorRFC;

	@Column(name="CONDUCTORCURP")
	private String conductorCURP;

    @Temporal(TemporalType.DATE)
	@Column(name="FECHACANCELACION")
	private Date fechaCancelacion;

	@Column(name="DESISTIMIENTO")
	private String desistimiento;

	@Column(name="MONTO_RECHAZO")
	private Double montoRechazo;

	@Column(name="DETECTADO_SAP")
	private String detectadoSap;

	@Column(name="OBSERVACIONES")
	private String observaciones;

	@Column(name="BANDERA_OPERACION")
	private String banderaOperacion;

	@Column(name="INCISO")
	private String inciso;

	@Column(name="SESSION_ID")
	private long sessionId;

	@Column(name="PROCESADO")
	private String procesado;

	@Column(name="PROCESAR")
	private String procesar;

	@Column(name="ID_REPORTE_SIN")
	private long idReporteSiniestro;

	@Column(name="CVEPAIS_SEYCOS")
	private String cvePaisSeycos;

	@Column(name="CVEESTADO_SEYCOS")
	private String cveEstadoSeycos;

	@Column(name="CVECIUDAD_SEYCOS")
	private String cveCiudadSeycos;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHAREGISTRO")
	private Date fechaRegistro;

	@Column(name="CONDUCTOR_TELEFONO")
	private String conductorTelefono;
    
	@Column(name="ESTATUSMIGRACION")
	private long estatusMigracion;
    
	
	
	
	public long getEstatusMigracion() {
		return estatusMigracion;
	}

	public void setEstatusMigracion(long estatusMigracion) {
		this.estatusMigracion = estatusMigracion;
	}

	public long getIdSapAmisControlSiniestros() {
		return idSapAmisControlSiniestros;
	}

	public void setIdSapAmisControlSiniestros(long idSapAmisControlSiniestros) {
		this.idSapAmisControlSiniestros = idSapAmisControlSiniestros;
	}

	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}

	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}

	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	public Date getFechaSiniestro() {
		return fechaSiniestro;
	}

	public void setFechaSiniestro(Date fechaSiniestro) {
		this.fechaSiniestro = fechaSiniestro;
	}

	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public long getCausaSiniestro() {
		return causaSiniestro;
	}

	public void setCausaSiniestro(long causaSiniestro) {
		this.causaSiniestro = causaSiniestro;
	}

	public Double getMontoOcurridoSiniestro() {
		return montoOcurridoSiniestro;
	}

	public void setMontoOcurridoSiniestro(Double montoOcurridoSiniestro) {
		this.montoOcurridoSiniestro = montoOcurridoSiniestro;
	}

	public long getUbicacionSiniestroPais() {
		return ubicacionSiniestroPais;
	}

	public void setUbicacionSiniestroPais(long ubicacionSiniestroPais) {
		this.ubicacionSiniestroPais = ubicacionSiniestroPais;
	}

	public long getUbicacionSiniestroEstado() {
		return ubicacionSiniestroEstado;
	}

	public void setUbicacionSiniestroEstado(long ubicacionSiniestroEstado) {
		this.ubicacionSiniestroEstado = ubicacionSiniestroEstado;
	}

	public long getUbicacionSiniestroMunicipio() {
		return ubicacionSiniestroMunicipio;
	}

	public void setUbicacionSiniestroMunicipio(
			long ubicacionSiniestroMunicipio) {
		this.ubicacionSiniestroMunicipio = ubicacionSiniestroMunicipio;
	}

	public long getAfectado() {
		return afectado;
	}

	public void setAfectado(long afectado) {
		this.afectado = afectado;
	}

	public String getAfectadoApPaterno() {
		return afectadoApPaterno;
	}

	public void setAfectadoApPaterno(String afectadoApPaterno) {
		this.afectadoApPaterno = afectadoApPaterno;
	}

	public String getAfectadoApMaterno() {
		return afectadoApMaterno;
	}

	public void setAfectadoApMaterno(String afectadoApMaterno) {
		this.afectadoApMaterno = afectadoApMaterno;
	}

	public String getAfectadoNombre() {
		return afectadoNombre;
	}

	public void setAfectadoNombre(String afectadoNombre) {
		this.afectadoNombre = afectadoNombre;
	}

	public String getAfectadoRFC() {
		return afectadoRFC;
	}

	public void setAfectadoRFC(String afectadoRFC) {
		this.afectadoRFC = afectadoRFC;
	}

	public String getAfectadoCURP() {
		return afectadoCURP;
	}

	public void setAfectadoCURP(String afectadoCURP) {
		this.afectadoCURP = afectadoCURP;
	}

	public String getConductorApPaterno() {
		return conductorApPaterno;
	}

	public void setConductorApPaterno(String conductorApPaterno) {
		this.conductorApPaterno = conductorApPaterno;
	}

	public String getConductorApMaterno() {
		return conductorApMaterno;
	}

	public void setConductorApMaterno(String conductorApMaterno) {
		this.conductorApMaterno = conductorApMaterno;
	}

	public String getConductorNombre() {
		return conductorNombre;
	}

	public void setConductorNombre(String conductorNombre) {
		this.conductorNombre = conductorNombre;
	}

	public String getConductorRFC() {
		return conductorRFC;
	}

	public void setConductorRFC(String conductorRFC) {
		this.conductorRFC = conductorRFC;
	}

	public String getConductorCURP() {
		return conductorCURP;
	}

	public void setConductorCURP(String conductorCURP) {
		this.conductorCURP = conductorCURP;
	}

	public Date getFechaCancelacion() {
		return fechaCancelacion;
	}

	public void setFechaCancelacion(Date fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}

	public String getDesistimiento() {
		return desistimiento;
	}

	public void setDesistimiento(String desistimiento) {
		this.desistimiento = desistimiento;
	}

	public Double getMontoRechazo() {
		return montoRechazo;
	}

	public void setMontoRechazo(Double montoRechazo) {
		this.montoRechazo = montoRechazo;
	}

	public String getDetectadoSap() {
		return detectadoSap;
	}

	public void setDetectadoSap(String detectadoSap) {
		this.detectadoSap = detectadoSap;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getBanderaOperacion() {
		return banderaOperacion;
	}

	public void setBanderaOperacion(String banderaOperacion) {
		this.banderaOperacion = banderaOperacion;
	}

	public String getInciso() {
		return inciso;
	}

	public void setInciso(String inciso) {
		this.inciso = inciso;
	}

	public long getSessionId() {
		return sessionId;
	}

	public void setSessionId(long sessionId) {
		this.sessionId = sessionId;
	}

	public String getProcesado() {
		return procesado;
	}

	public void setProcesado(String procesado) {
		this.procesado = procesado;
	}

	public String getProcesar() {
		return procesar;
	}

	public void setProcesar(String procesar) {
		this.procesar = procesar;
	}

	public long getIdReporteSiniestro() {
		return idReporteSiniestro;
	}

	public void setIdReporteSiniestro(long idReporteSiniestro) {
		this.idReporteSiniestro = idReporteSiniestro;
	}

	public String getCvePaisSeycos() {
		return cvePaisSeycos;
	}

	public void setCvePaisSeycos(String cvePaisSeycos) {
		this.cvePaisSeycos = cvePaisSeycos;
	}

	public String getCveEstadoSeycos() {
		return cveEstadoSeycos;
	}

	public void setCveEstadoSeycos(String cveEstadoSeycos) {
		this.cveEstadoSeycos = cveEstadoSeycos;
	}

	public String getCveCiudadSeycos() {
		return cveCiudadSeycos;
	}

	public void setCveCiudadSeycos(String cveCiudadSeycos) {
		this.cveCiudadSeycos = cveCiudadSeycos;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getConductorTelefono() {
		return conductorTelefono;
	}

	public void setConductorTelefono(String conductorTelefono) {
		this.conductorTelefono = conductorTelefono;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object getKey() {
		return getIdSapAmisControlSiniestros()==0?null:getIdSapAmisControlSiniestros();
	}

	@Override
	public String getValue() {
		String retorno = "IdSapAmisControlSiniestros: " + getIdSapAmisControlSiniestros()+
						 " | NumeroSiniestro: " + getNumeroSiniestro()+
						 " | NumeroPoliza: " + getNumeroPoliza()+
						 " | FechaSiniestro: " + getFechaSiniestro()+
						 " | NumeroSerie: " + getNumeroSerie()+
						 " | CausaSiniestro: " + getCausaSiniestro()+
						 " | MontoOcurridoSiniestro: " + getMontoOcurridoSiniestro()+
						 " | UbicacionSiniestroPais: " + getUbicacionSiniestroPais()+
						 " | UbicacionSiniestroEstado: " + getUbicacionSiniestroEstado()+
						 " | UbicacionSiniestroMunicipio: " + getUbicacionSiniestroMunicipio()+
						 " | Afectado: " + getAfectado()+
						 " | AfectadoApPaterno: " + getAfectadoApPaterno()+
						 " | AfectadoApMaterno: " + getAfectadoApMaterno()+
						 " | AfectadoNombre: " + getAfectadoNombre()+
						 " | AfectadoRFC: " + getAfectadoRFC()+
						 " | AfectadoCURP: " + getAfectadoCURP()+
						 " | ConductorApPaterno: " + getConductorApPaterno()+
						 " | ConductorApMaterno: " + getConductorApMaterno()+
						 " | ConductorNombre: " + getConductorNombre()+
						 " | ConductorRFC: " + getConductorRFC()+
						 " | ConductorCURP: " + getConductorCURP()+
						 " | FechaCancelacion: " + getFechaCancelacion()+
						 " | Desistimiento: " + getDesistimiento()+
						 " | MontoRechazo: " + getMontoRechazo()+
						 " | DetectadoSap: " + getDetectadoSap()+
						 " | Observaciones: " + getObservaciones()+
						 " | BanderaOperacion: " + getBanderaOperacion()+
						 " | Inciso: " + getInciso()+
						 " | SessionId: " + getSessionId()+
						 " | Procesado: " + getProcesado()+
						 " | Procesar: " + getProcesar()+
						 " | IdReporteSiniestro: " + getIdReporteSiniestro()+
						 " | CvePaisSeycos: " + getCvePaisSeycos()+
						 " | CveEstadoSeycos: " + getCveEstadoSeycos()+
						 " | CveCiudadSeycos: " + getCveCiudadSeycos()+
						 " | FechaRegistro: " + getFechaRegistro()+
						 " | ConductorTelefono: " + getConductorTelefono();
		return retorno;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
}