package mx.com.afirme.midas2.dto.sapamis.catalogos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id; 
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatSistemas;
/******************************************************************************
 * Entidad para el manejo de las Alertas SAP-AMIS
 * 
 * @author Eduardo.Chavez
 * 
 * 		Table:		SAP_AMIS_ALERTAS
 * 		Schema:		MIDAS
 * 		Sequence:	SEQSAP_AMIS_ALERTAS
 * 		
 ******************************************************************************/
@Entity
@Table(name="SAPAMISCATALERTAS", schema = "MIDAS")
public class CatAlertasSapAmis implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQSAPAMISCATALERTAS")
	@SequenceGenerator(name="SEQSAPAMISCATALERTAS", schema = "MIDAS", sequenceName="SEQSAPAMISCATALERTAS",allocationSize=1)
	@Column(name="IDSAPAMISCATALERTAS")
	private Long id;

	@Column(name="SAPAMISCATALERTAS")
	private String descCatAlertasSapAmis;

	@ManyToOne
	@JoinColumn(name="IDSAPAMISCATSISTEMAS")
	private CatSistemas catSistemas;
	
	@Column(name="ESTATUS")
	private long estatus;
	
	/*****************************
	 * Inician Getters y Setters *
	 *****************************/
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescCatAlertasSapAmis() {
		return descCatAlertasSapAmis;
	}

	public void setDescCatAlertasSapAmis(String descCatAlertasSapAmis) {
		this.descCatAlertasSapAmis = descCatAlertasSapAmis;
	}
	
	public CatSistemas getCatSistemas() {
		return catSistemas;
	}

	public void setCatSistemas(CatSistemas catSistemas) {
		this.catSistemas = catSistemas;
	}
	
	public long getEstatus() {
		return estatus;
	}

	public void setEstatus(long estatus) {
		this.estatus = estatus;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}
