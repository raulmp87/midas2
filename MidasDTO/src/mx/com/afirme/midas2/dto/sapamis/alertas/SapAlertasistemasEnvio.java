package mx.com.afirme.midas2.dto.sapamis.alertas;

import java.io.Serializable;

import javax.persistence.*;

import mx.com.afirme.midas2.dto.sapamis.otros.SapAmisBitacoraEmision;
import mx.com.afirme.midas2.dto.sapamis.otros.SapAmisBitacoraSiniestros;

import java.util.Set;


/**
 * The persistent class for the SAP_ALERTASISTEMAS_ENVIO database table.
 * 
 */
@Entity
@Table(name="SAP_ALERTASISTEMAS_ENVIO", schema = "MIDAS")
public class SapAlertasistemasEnvio implements Serializable, mx.com.afirme.midas2.dao.catalogos.Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQSAP_ALERTASISTEMAS_ENVIOID")
	@SequenceGenerator(name="SEQSAP_ALERTASISTEMAS_ENVIOID", schema = "MIDAS" , sequenceName="SEQSAP_ALERTASISTEMAS_ENVIOID",allocationSize=1)
	@Column(name="SAP_IDALERTASISTEMAS_ENVIO")
	private long sapIdalertasistemasEnvio;

	private String error;

	//bi-directional many-to-one association to SapAlertasCesvi
	@OneToMany(mappedBy="sapAlertasistemasEnvio", fetch=FetchType.EAGER)
	private Set<SapAlertasCesvi> sapAlertasCesvis;

	//bi-directional many-to-one association to SapAlertasCii
	@OneToMany(mappedBy="sapAlertasistemasEnvio", fetch=FetchType.EAGER)
	private Set<SapAlertasCii> sapAlertasCiis;

	//bi-directional many-to-one association to SapAlertasEmision
	@OneToMany(mappedBy="sapAlertasistemasEnvio", fetch=FetchType.EAGER)
	private Set<SapAlertasEmision> sapAlertasEmisions;

	//bi-directional many-to-one association to SapAlertasOcra
	@OneToMany(mappedBy="sapAlertasistemasEnvio", fetch=FetchType.EAGER)
	private Set<SapAlertasOcra> sapAlertasOcras;

	//bi-directional many-to-one association to SapAlertasPrevencion
	@OneToMany(mappedBy="sapAlertasistemasEnvio", fetch=FetchType.EAGER)
	private Set<SapAlertasPrevencion> sapAlertasPrevencions;

	//bi-directional many-to-one association to SapAlertasPt
	@OneToMany(mappedBy="sapAlertasistemasEnvio", fetch=FetchType.EAGER)
	private Set<SapAlertasPt> sapAlertasPts;

	//bi-directional many-to-one association to SapAlertasScd
	@OneToMany(mappedBy="sapAlertasistemasEnvio", fetch=FetchType.EAGER)
	private Set<SapAlertasScd> sapAlertasScds;

	//bi-directional many-to-one association to SapAlertasSiniestro
	@OneToMany(mappedBy="sapAlertasistemasEnvio", fetch=FetchType.EAGER)
	private Set<SapAlertasSiniestro> sapAlertasSiniestros;

	//bi-directional many-to-one association to SapAlertasSipac
	@OneToMany(mappedBy="sapAlertasistemasEnvio", fetch=FetchType.EAGER)
	private Set<SapAlertasSipac> sapAlertasSipacs;

	//bi-directional many-to-one association to SapAlertasValuacion
	@OneToMany(mappedBy="sapAlertasistemasEnvio", fetch=FetchType.EAGER)
	private Set<SapAlertasValuacion> sapAlertasValuacions;

	//bi-directional many-to-one association to SapAmisBitacoraEmision
	@OneToMany(mappedBy="sapAlertasistemasEnvio", fetch=FetchType.EAGER)
	private Set<SapAmisBitacoraEmision> sapAmisBitacoraEmisions;

	//bi-directional many-to-one association to SapAmisBitacoraSiniestro
	@OneToMany(mappedBy="sapAlertasistemasEnvio", fetch=FetchType.EAGER)
	private Set<SapAmisBitacoraSiniestros> sapAmisBitacoraSiniestros;

    public SapAlertasistemasEnvio() {
    }

	public long getSapIdalertasistemasEnvio() {
		return this.sapIdalertasistemasEnvio;
	}

	public void setSapIdalertasistemasEnvio(long sapIdalertasistemasEnvio) {
		this.sapIdalertasistemasEnvio = sapIdalertasistemasEnvio;
	}

	public String getError() {
		return this.error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Set<SapAlertasCesvi> getSapAlertasCesvis() {
		return this.sapAlertasCesvis;
	}

	public void setSapAlertasCesvis(Set<SapAlertasCesvi> sapAlertasCesvis) {
		this.sapAlertasCesvis = sapAlertasCesvis;
	}
	
	public Set<SapAlertasCii> getSapAlertasCiis() {
		return this.sapAlertasCiis;
	}

	public void setSapAlertasCiis(Set<SapAlertasCii> sapAlertasCiis) {
		this.sapAlertasCiis = sapAlertasCiis;
	}
	
	public Set<SapAlertasEmision> getSapAlertasEmisions() {
		return this.sapAlertasEmisions;
	}

	public void setSapAlertasEmisions(Set<SapAlertasEmision> sapAlertasEmisions) {
		this.sapAlertasEmisions = sapAlertasEmisions;
	}
	
	public Set<SapAlertasOcra> getSapAlertasOcras() {
		return this.sapAlertasOcras;
	}

	public void setSapAlertasOcras(Set<SapAlertasOcra> sapAlertasOcras) {
		this.sapAlertasOcras = sapAlertasOcras;
	}
	
	public Set<SapAlertasPrevencion> getSapAlertasPrevencions() {
		return this.sapAlertasPrevencions;
	}

	public void setSapAlertasPrevencions(Set<SapAlertasPrevencion> sapAlertasPrevencions) {
		this.sapAlertasPrevencions = sapAlertasPrevencions;
	}
	
	public Set<SapAlertasPt> getSapAlertasPts() {
		return this.sapAlertasPts;
	}

	public void setSapAlertasPts(Set<SapAlertasPt> sapAlertasPts) {
		this.sapAlertasPts = sapAlertasPts;
	}
	
	public Set<SapAlertasScd> getSapAlertasScds() {
		return this.sapAlertasScds;
	}

	public void setSapAlertasScds(Set<SapAlertasScd> sapAlertasScds) {
		this.sapAlertasScds = sapAlertasScds;
	}
	
	public Set<SapAlertasSiniestro> getSapAlertasSiniestros() {
		return this.sapAlertasSiniestros;
	}

	public void setSapAlertasSiniestros(Set<SapAlertasSiniestro> sapAlertasSiniestros) {
		this.sapAlertasSiniestros = sapAlertasSiniestros;
	}
	
	public Set<SapAlertasSipac> getSapAlertasSipacs() {
		return this.sapAlertasSipacs;
	}

	public void setSapAlertasSipacs(Set<SapAlertasSipac> sapAlertasSipacs) {
		this.sapAlertasSipacs = sapAlertasSipacs;
	}
	
	public Set<SapAlertasValuacion> getSapAlertasValuacions() {
		return this.sapAlertasValuacions;
	}

	public void setSapAlertasValuacions(Set<SapAlertasValuacion> sapAlertasValuacions) {
		this.sapAlertasValuacions = sapAlertasValuacions;
	}
	
	public Set<SapAmisBitacoraEmision> getSapAmisBitacoraEmisions() {
		return this.sapAmisBitacoraEmisions;
	}

	public void setSapAmisBitacoraEmisions(Set<SapAmisBitacoraEmision> sapAmisBitacoraEmisions) {
		this.sapAmisBitacoraEmisions = sapAmisBitacoraEmisions;
	}
	
	public Set<SapAmisBitacoraSiniestros> getSapAmisBitacoraSiniestros() {
		return this.sapAmisBitacoraSiniestros;
	}

	public void setSapAmisBitacoraSiniestros(Set<SapAmisBitacoraSiniestros> sapAmisBitacoraSiniestros) {
		this.sapAmisBitacoraSiniestros = sapAmisBitacoraSiniestros;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		// TODO Auto-generated method stub
		return sapIdalertasistemasEnvio;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
}