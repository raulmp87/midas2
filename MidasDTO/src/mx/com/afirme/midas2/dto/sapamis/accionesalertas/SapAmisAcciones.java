package mx.com.afirme.midas2.dto.sapamis.accionesalertas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/******************************************************************************
 * Entidad para el manejo de las Acciones sobre las Alertas del SAP-AMIS
 * 
 * @author Eduardo.Chavez
 * 
 * 		Table:		SAP_AMIS_ACIONES
 * 		Schema:		MIDAS
 * 		Sequence:	SEQSAP_AMIS_ACIONES
 * 		
 ******************************************************************************/
@Entity
@Table(name="SAPAMISACCIONES", schema = "MIDAS")
public class SapAmisAcciones implements Serializable, mx.com.afirme.midas2.dao.catalogos.Entidad {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQSAPAMISACCIONES")
	@SequenceGenerator(name="SEQSAPAMISACCIONES", schema = "MIDAS", sequenceName="SEQSAPAMISACCIONES",allocationSize=1)
	
	@Column(name="IDSAPAMISACCIONES")
	private long idSapAmisAcciones;
	
	@Column(name="SAPAMISACCIONES")
	private String descripcionAccion;
	
	@ManyToOne
	@JoinColumn(name="IDSAPAMISACCIONESRELACION")
	private SapAmisAccionesRelacion sapAmisAccionesRelacion;
	
	// 0:Activo, 1:Eliminado - Se maneja para cuando se eliminan que quede el registro
	@Column(name="ESTATUS")
	private long estatus; 
	
	/*****************************
	 * Inician Getters y Setters *
	 *****************************/

	public long getIdSapAmisAcciones() {
		return idSapAmisAcciones;
	}
	public void setIdSapAmisAcciones(long idSapAmisAcciones) {
		this.idSapAmisAcciones = idSapAmisAcciones;
	}
	public String getDescripcionAccion() {
		return descripcionAccion;
	}
	public void setDescripcionAccion(String descripcionAccion) {
		this.descripcionAccion = descripcionAccion;
	}
	public SapAmisAccionesRelacion getSapAmisAccionesRelacion() {
		return sapAmisAccionesRelacion;
	}
	public void setSapAmisAccionesRelacion(
			SapAmisAccionesRelacion sapAmisAccionesRelacion) {
		this.sapAmisAccionesRelacion = sapAmisAccionesRelacion;
	}
	public long getEstatus() {
		return estatus;
	}
	public void setEstatus(long estatus) {
		this.estatus = estatus;
	}
	
	/************************************************
	 * Mètodos implementados de la interfaz Entidad *
	 * Para el uso generico de los DAO y Servicios  *
	 ************************************************/
	
	@SuppressWarnings("unchecked")
	@Override
	public Object getKey() {
		return getIdSapAmisAcciones()==0?null:getIdSapAmisAcciones();
	}
	
	@Override
	public String getValue() {
		return "{ "
					+ "descripcionAccion: " + getDescripcionAccion() + " | "
					+ "sapAmisAccionesRelacion: " + getSapAmisAccionesRelacion() +
			   " }";
	}
	
	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}
