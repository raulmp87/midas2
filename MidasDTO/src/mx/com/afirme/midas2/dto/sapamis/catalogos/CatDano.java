package mx.com.afirme.midas2.dto.sapamis.catalogos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="SAPAMISCATDANO", schema="MIDAS")
public class CatDano implements Serializable, Entidad {
	private static final long serialVersionUID=1L;
	
	@Id
	@Column(name="IDSAPAMISCATDANO")
	private Long id;
	
	@Column(name="SAPAMISCATDANO")
	private String descCatDano;
	
	@Column(name="ESTATUS")
	private long estatus;
		
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescCatDano() {
		return descCatDano;
	}

	public void setDescCatDano(String descCatDano) {
		this.descCatDano = descCatDano;
	}

	public long getEstatus() {
		return estatus;
	}

	public void setEstatus(long estatus) {
		this.estatus = estatus;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}