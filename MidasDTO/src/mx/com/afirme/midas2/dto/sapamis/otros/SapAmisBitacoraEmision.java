package mx.com.afirme.midas2.dto.sapamis.otros;

import java.io.Serializable;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasistemasEnvio;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the SAP_AMIS_BITACORA_EMISION database table.
 * 
 */
@Entity
@Table(name="SAP_AMIS_BITACORA_EMISION", schema = "MIDAS")
public class SapAmisBitacoraEmision implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQSAP_AMIS_BITACORA_EMISIONID")
	@SequenceGenerator(name="SEQSAP_AMIS_BITACORA_EMISIONID", schema = "MIDAS", sequenceName="SEQSAP_AMIS_BITACORA_EMISIONID",allocationSize=1)
	@Column(name="IDSAP_AMIS_BITACORA_EMISION")
	private long idSapAmisBitacoraEmision;

	@Column(name="USUARIO_SAP")
	private String usuarioSap;

	@Column(name="PASSWORD_SAP")
	private String passwordSap;

	@Column(name="POLIZA")
	private String poliza;

    @Column(name="INCISO")
	private String inciso;

    @Temporal(TemporalType.DATE)
	@Column(name="FECHA_INICIO_VIGENCIA")
	private Date fechaInicioVigencia;

    @Temporal(TemporalType.DATE)
	@Column(name="FECHA_FIN_VIGENCIA")
	private Date fechaFinVigencia;

    @Column(name="SERIE")
	private String serie;

    @Column(name="MARCA")
	private long marca;

	@Column(name="TIPO_SUBMARCA")
	private long tipoSubmarca;

	@Column(name="MODELO")
	private long modelo;

	@Column(name="TIPO_TRANSPORTE")
	private String tipoTransporte;

	@Column(name="TIPO_PERSONA")
	private long tipoPersona;

	@Column(name="TIPO_SERVICIO")
	private String tipoServicio;

	@Column(name="CLIENTE1_PATERNO")
	private String cliente1Paterno;

	@Column(name="CLIENTE1_MATERNO")
	private String cliente1Materno;

	@Column(name="CLIENTE1_NOMBRE_RAZON_SOCIAL")
	private String cliente1NombreRazonSocial;

	@Column(name="CLIENTE1_RFC")
	private String cliente1Rfc;

	@Column(name="CLIENTE1_CURP")
	private String cliente1Curp;

	@Column(name="CLIENTE2_PATERNO")
	private String cliente2Paterno;

	@Column(name="CLIENTE2_MATERNO")
	private String cliente2Materno;

	@Column(name="CLIENTE2_NOMBRE")
	private String cliente2Nombre;

	@Column(name="BENEFICIARIO_AP")
	private String beneficiarioAp;

	@Column(name="BENEFICIARIO_AM")
	private String beneficiarioAm;

	@Column(name="BENEFICIARIO_NOMBRE")
	private String beneficiarioNombre;

	@Column(name="BENEFICIARIO_RFC")
	private String beneficiarioRfc;

	@Column(name="BENEFICIARIO_CURP")
	private String beneficiarioCurp;

	@Column(name="CANAL_VENTA")
	private long canalVenta;
	
	@Column(name="AGENTE")
	private String agente;
	
    @Temporal(TemporalType.DATE)
	@Column(name="FECHA_CANCELACION")
	private Date fechaCancelacion;

    @Temporal(TemporalType.DATE)
	@Column(name="FECHA_REHABILITACION")
	private Date fechaRehavilitacion;

	@Column(name="BANDERA_OPERACION")
	private String banderaOperacion;

    @Temporal(TemporalType.DATE)
	@Column(name="FECHA_ENVIO_SAP")
	private Date fechaEnvioSap;

	@Column(name="ESTATUS_RESP_OPERACION")
	private String estatusRespOperacion;

    @Temporal(TemporalType.DATE)
	@Column(name="FECHAFIN_RESP_SAP")
	private Date fechafinRespSap;

	@Column(name="ESTATUS_RESP_VALIDACION")
	private String estatusRespValidacion;

	@Column(name="ESTATUS_RESP_ALERTAS")
	private String estatusRespAlertas;

	@Column(name="MENSAJE_VALIDACION")
	private String mensageValidacion;

	//bi-directional many-to-one association to SapAlertasistemasEnvio
    @ManyToOne
	@JoinColumn(name="IDSAP_ALERTASISTEMAS_ENVIO")
	private SapAlertasistemasEnvio sapAlertasistemasEnvio;

    @Temporal(TemporalType.TIMESTAMP)
	private Date recordfrom;
	
	@Column(name="SESSION_ID")
	private long sessionId;
	
	@Column(name="LOTE_ID")
	private long loteId;

	
	
	
	

    public SapAmisBitacoraEmision() {
    }

	public long getIdSapAmisBitacoraEmision() {
		return this.idSapAmisBitacoraEmision;
	}

	public void setIdSapAmisBitacoraEmision(long idSapAmisBitacoraEmision) {
		this.idSapAmisBitacoraEmision = idSapAmisBitacoraEmision;
	}

	public String getAgente() {
		return this.agente;
	}

	public void setAgente(String agente) {
		this.agente = agente;
	}

	public String getBanderaOperacion() {
		
		String operacion = new String();
		switch (Integer.valueOf(this.banderaOperacion)) {
            case 1:  
            	operacion = "Emision";
                break;
            case 2:  
            	operacion = "Modificacion";
                break;
            case 3:  
            	operacion = "Baja";
                break;
            case 4:  
            	operacion = "Rehabilitacion";
            	break;         
            default: 
            	System.out.println("Bandera no reconocida");
                break;
		}
		
		return operacion;
	}

	public void setBanderaOperacion(String banderaOperacion) {
		this.banderaOperacion = banderaOperacion;
	}

	public String getBeneficiarioAm() {
		return this.beneficiarioAm;
	}

	public void setBeneficiarioAm(String beneficiarioAm) {
		this.beneficiarioAm = beneficiarioAm;
	}

	public String getBeneficiarioAp() {
		return this.beneficiarioAp;
	}

	public void setBeneficiarioAp(String beneficiarioAp) {
		this.beneficiarioAp = beneficiarioAp;
	}

	public String getBeneficiarioCurp() {
		return this.beneficiarioCurp;
	}

	public void setBeneficiarioCurp(String beneficiarioCurp) {
		this.beneficiarioCurp = beneficiarioCurp;
	}

	public String getBeneficiarioNombre() {
		return this.beneficiarioNombre;
	}

	public void setBeneficiarioNombre(String beneficiarioNombre) {
		this.beneficiarioNombre = beneficiarioNombre;
	}

	public String getBeneficiarioRfc() {
		return this.beneficiarioRfc;
	}

	public void setBeneficiarioRfc(String beneficiarioRfc) {
		this.beneficiarioRfc = beneficiarioRfc;
	}

	public long getCanalVenta() {
		return this.canalVenta;
	}

	public void setCanalVenta(long canalVenta) {
		this.canalVenta = canalVenta;
	}

	public String getCliente1Curp() {
		return this.cliente1Curp;
	}

	public void setCliente1Curp(String cliente1Curp) {
		this.cliente1Curp = cliente1Curp;
	}

	public String getCliente1Materno() {
		return this.cliente1Materno;
	}

	public void setCliente1Materno(String cliente1Materno) {
		this.cliente1Materno = cliente1Materno;
	}

	public String getCliente1NombreRazonSocial() {
		return this.cliente1NombreRazonSocial;
	}

	public void setCliente1NombreRazonSocial(String cliente1NombreRazonSocial) {
		this.cliente1NombreRazonSocial = cliente1NombreRazonSocial;
	}

	public String getCliente1Paterno() {
		return this.cliente1Paterno;
	}

	public void setCliente1Paterno(String cliente1Paterno) {
		this.cliente1Paterno = cliente1Paterno;
	}

	public String getCliente1Rfc() {
		return this.cliente1Rfc;
	}

	public void setCliente1Rfc(String cliente1Rfc) {
		this.cliente1Rfc = cliente1Rfc;
	}

	public String getCliente2Materno() {
		return this.cliente2Materno;
	}

	public void setCliente2Materno(String cliente2Materno) {
		this.cliente2Materno = cliente2Materno;
	}

	public String getCliente2Nombre() {
		return this.cliente2Nombre;
	}

	public void setCliente2Nombre(String cliente2Nombre) {
		this.cliente2Nombre = cliente2Nombre;
	}

	public String getCliente2Paterno() {
		return this.cliente2Paterno;
	}

	public void setCliente2Paterno(String cliente2Paterno) {
		this.cliente2Paterno = cliente2Paterno;
	}

	public String getEstatusRespAlertas() {
		return this.estatusRespAlertas;
	}

	public void setEstatusRespAlertas(String estatusRespAlertas) {
		this.estatusRespAlertas = estatusRespAlertas;
	}

	public String getEstatusRespOperacion() {
		
		if(this.estatusRespOperacion.equals("0")){
			return "Operacion exitosa";
		}else if(this.estatusRespOperacion.equals("1")){
			return "Operacion exitosa";
		}
		return this.estatusRespOperacion;
	}

	public void setEstatusRespOperacion(String estatusRespOperacion) {
		this.estatusRespOperacion = estatusRespOperacion;
	}

	public String getEstatusRespValidacion() {
		return this.estatusRespValidacion;
	}

	public void setEstatusRespValidacion(String estatusRespValidacion) {
		this.estatusRespValidacion = estatusRespValidacion;
	}

	public Date getFechaCancelacion() {
		return this.fechaCancelacion;
	}

	public void setFechaCancelacion(Date fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}

	public Date getFechaEnvioSap() {
		return this.fechaEnvioSap;
	}

	public void setFechaEnvioSap(Date fechaEnvioSap) {
		this.fechaEnvioSap = fechaEnvioSap;
	}

	public Date getFechaFinVigencia() {
		return this.fechaFinVigencia;
	}

	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	public Date getFechaInicioVigencia() {
		return this.fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	public Date getFechaRehavilitacion() {
		return this.fechaRehavilitacion;
	}

	public void setFechaRehavilitacion(Date fechaRehavilitacion) {
		this.fechaRehavilitacion = fechaRehavilitacion;
	}

	public Date getFechafinRespSap() {
		return this.fechafinRespSap;
	}

	public void setFechafinRespSap(Date fechafinRespSap) {
		this.fechafinRespSap = fechafinRespSap;
	}

	public String getInciso() {
		return this.inciso;
	}

	public void setInciso(String inciso) {
		this.inciso = inciso;
	}

	public long getMarca() {
		return this.marca;
	}

	public void setMarca(long marca) {
		this.marca = marca;
	}

	public String getMensageValidacion() {
		return this.mensageValidacion;
	}

	public void setMensageValidacion(String mensageValidacion) {
		this.mensageValidacion = mensageValidacion;
	}

	public long getModelo() {
		return this.modelo;
	}

	public void setModelo(long modelo) {
		this.modelo = modelo;
	}

	public String getPasswordSap() {
		return this.passwordSap;
	}

	public void setPasswordSap(String passwordSap) {
		this.passwordSap = passwordSap;
	}

	public String getPoliza() {
		return this.poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	public Date getRecordfrom() {
		return this.recordfrom;
	}

	public void setRecordfrom(Date recordfrom) {
		this.recordfrom = recordfrom;
	}

	public String getSerie() {
		return this.serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public long getTipoPersona() {
		return this.tipoPersona;
	}

	public void setTipoPersona(long tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getTipoServicio() {
		return this.tipoServicio;
	}

	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

	public long getTipoSubmarca() {
		return this.tipoSubmarca;
	}

	public void setTipoSubmarca(long tipoSubmarca) {
		this.tipoSubmarca = tipoSubmarca;
	}

	public String getTipoTransporte() {
		return this.tipoTransporte;
	}

	public void setTipoTransporte(String tipoTransporte) {
		this.tipoTransporte = tipoTransporte;
	}

	public String getUsuarioSap() {
		return this.usuarioSap;
	}

	public void setUsuarioSap(String usuarioSap) {
		this.usuarioSap = usuarioSap;
	}

	public SapAlertasistemasEnvio getSapAlertasistemasEnvio() {
		return this.sapAlertasistemasEnvio;
	}

	public void setSapAlertasistemasEnvio(SapAlertasistemasEnvio sapAlertasistemasEnvio) {
		this.sapAlertasistemasEnvio = sapAlertasistemasEnvio;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object getKey() {
		// TODO Auto-generated method stub
		return getIdSapAmisBitacoraEmision()==0?null:getIdSapAmisBitacoraEmision();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	public long getSessionId() {
		return sessionId;
	}

	public void setSessionId(long sessionId) {
		this.sessionId = sessionId;
	}

	public long getLoteId() {
		return loteId;
	}

	public void setLoteId(long loteId) {
		this.loteId = loteId;
	}
	
}