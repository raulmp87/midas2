package mx.com.afirme.midas2.dto.sapamis.sistemas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatEstVehPago;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatMarcaVehiculo;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatSubMarcaVehiculo;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatTipoAfectado;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatTipoPersona;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatTipoTransporte;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatTipoVenta;
/******************************************************************************
 * 	Entidad para el manejo de los datos de Salvamento dentro de los Procesos
 *  de envio del SAP-AMIS.
 * 
 * 	@author Eduardo.Chavez
 * 
 * 		Table:		SAPAMISSALVAMENTO
 * 		Schema:		MIDAS
 * 		Sequence:	SEQSAPAMISSALVAMENTO
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 * 		
 ******************************************************************************/
@Entity
@Table(name="SAPAMISSALVAMENTO",schema="MIDAS")
public class SapAmisSalvamento implements Serializable, Entidad {
	private static final long serialVersionUID=1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQSAPAMISSALVAMENTO")
	@SequenceGenerator(name="SEQSAPAMISSALVAMENTO", schema="MIDAS", sequenceName="SEQSAPAMISSALVAMENTO", allocationSize=1)
	@Column(name="IDSAPAMISSALVAMENTO")
	private Long id;
	
	@Column(name="NOSERIE")
	private String noSerie;
	
	@Column(name="NOSINIESTRO")
	private String noSiniestro;
	
	@ManyToOne
	@JoinColumn(name="IDSAPAMISCATTIPOAFECTADO")
	private CatTipoAfectado catTipoAfectado;
	
	@ManyToOne
	@JoinColumn(name="IDSAPAMISCATTIPOVENTA")
	private CatTipoVenta catTipoVenta;
	
	@Column(name="IMPORTEVENTA")
	private Double importeVenta;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FECHAVENTA")
	private Date fechaVenta;
	
	@Column(name="NOFACTURA")
	private String noFactura;
	
	@Column(name="RFCFACTURA")
	private String rfcFactura;
	
	@ManyToOne
	@JoinColumn(name="IDSAPAMISCATTIPOPERSONA")
	private CatTipoPersona catTipoPersona;
	
	@ManyToOne
	@JoinColumn(name="IDSAPAMISCATESTVEHPAGO")
	private CatEstVehPago catEstVehPago;
	
	@Column(name="REMARCADO")
	private String remarcado;
	
	@ManyToOne
	@JoinColumn(name="IDSAPAMISCATTIPOTRANSPORTE")
	private CatTipoTransporte catTipoTransporte;
	
	@ManyToOne
	@JoinColumn(name="IDSAPAMISCATMARCAVEH")
	private CatMarcaVehiculo catMarcaVehiculo;
	
	@ManyToOne
	@JoinColumn(name="IDSAPAMISCATSUBMARCAVEH")
	private CatSubMarcaVehiculo catSubMarcaVehiculo;
	
	@Column(name="ANOMODELO")
	private long anoModelo;

	@OneToOne
    @JoinColumn(updatable=false,insertable=false, name="IDSAPAMISBITACORAS", referencedColumnName="IDSAPAMISBITACORAS")
	private SapAmisBitacoras sapAmisBitacoras;
		
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNoSerie() {
		return noSerie;
	}

	public void setNoSerie(String noSerie) {
		this.noSerie = noSerie;
	}

	public String getNoSiniestro() {
		return noSiniestro;
	}

	public void setNoSiniestro(String noSiniestro) {
		this.noSiniestro = noSiniestro;
	}

	public CatTipoAfectado getCatTipoAfectado() {
		return catTipoAfectado;
	}

	public void setCatTipoAfectado(CatTipoAfectado catTipoAfectado) {
		this.catTipoAfectado = catTipoAfectado;
	}

	public CatTipoVenta getCatTipoVenta() {
		return catTipoVenta;
	}

	public void setCatTipoVenta(CatTipoVenta catTipoVenta) {
		this.catTipoVenta = catTipoVenta;
	}
	
	public Double getImporteVenta() {
		return importeVenta;
	}

	public void setImporteVenta(Double importeVenta) {
		this.importeVenta = importeVenta;
	}

	public Date getFechaVenta() {
		return fechaVenta;
	}

	public void setFechaVenta(Date fechaVenta) {
		this.fechaVenta = fechaVenta;
	}

	public String getNoFactura() {
		return noFactura;
	}

	public void setNoFactura(String noFactura) {
		this.noFactura = noFactura;
	}

	public String getRfcFactura() {
		return rfcFactura;
	}

	public void setRfcFactura(String rfcFactura) {
		this.rfcFactura = rfcFactura;
	}

	public CatTipoPersona getCatTipoPersona() {
		return catTipoPersona;
	}

	public void setCatTipoPersona(CatTipoPersona catTipoPersona) {
		this.catTipoPersona = catTipoPersona;
	}

	public CatEstVehPago getCatEstVehPago() {
		return catEstVehPago;
	}

	public void setCatEstVehPago(CatEstVehPago catEstVehPago) {
		this.catEstVehPago = catEstVehPago;
	}

	public String getRemarcado() {
		return remarcado;
	}

	public void setRemarcado(String remarcado) {
		this.remarcado = remarcado;
	}

	public CatTipoTransporte getCatTipoTransporte() {
		return catTipoTransporte;
	}

	public void setCatTipoTransporte(CatTipoTransporte catTipoTransporte) {
		this.catTipoTransporte = catTipoTransporte;
	}

	public CatMarcaVehiculo getCatMarcaVehiculo() {
		return catMarcaVehiculo;
	}

	public void setCatMarcaVehiculo(CatMarcaVehiculo catMarcaVehiculo) {
		this.catMarcaVehiculo = catMarcaVehiculo;
	}

	public CatSubMarcaVehiculo getCatSubMarcaVehiculo() {
		return catSubMarcaVehiculo;
	}

	public void setCatSubMarcaVehiculo(CatSubMarcaVehiculo catSubMarcaVehiculo) {
		this.catSubMarcaVehiculo = catSubMarcaVehiculo;
	}

	public long getAnoModelo() {
		return anoModelo;
	}

	public void setAnoModelo(long anoModelo) {
		this.anoModelo = anoModelo;
	}

	public SapAmisBitacoras getSapAmisBitacoras() {
		return sapAmisBitacoras;
	}

	public void setSapAmisBitacoras(SapAmisBitacoras sapAmisBitacoras) {
		this.sapAmisBitacoras = sapAmisBitacoras;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}