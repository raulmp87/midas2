package mx.com.afirme.midas2.dto.sapamis.alertas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the SAP_ALERTAS_EMISION database table.
 * 
 */
@Entity
@Table(name="SAP_ALERTAS_EMISION", schema = "MIDAS")
public class SapAlertasEmision implements Serializable, mx.com.afirme.midas2.dao.catalogos.Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQSAP_ALERTAS_EMISIONID")
	@SequenceGenerator(name="SEQSAP_ALERTAS_EMISIONID", schema = "MIDAS" , sequenceName="SEQSAP_ALERTAS_EMISIONID",allocationSize=1)
	@Column(name="IDSAP_ALERTAS_EMISION")
	private long idsapAlertasEmision;

	private String agente;

	private String canalventa;

	private String cia;

	@Column(name="CLIENTE1_MATERNO")
	private String cliente1Materno;

	@Column(name="CLIENTE1_NOMBRE")
	private String cliente1Nombre;

	@Column(name="CLIENTE1_PATERNO")
	private String cliente1Paterno;

    @Temporal( TemporalType.DATE)
	@Column(name="FIN_VIGENCIA")
	private Date finVigencia;

	private String inciso;

    @Temporal( TemporalType.DATE)
	@Column(name="INICIO_VIGENCIA")
	private Date inicioVigencia;

	@Column(name="MARCA_DESC")
	private String marcaDesc;

	private String modelo;

	@Column(name="PERSONA_C1")
	private String personaC1;

	private String poliza;

	@Column(name="TIPO_DESC")
	private String tipoDesc;

	@Column(name="TIPO_SERVICIO")
	private String tipoServicio;

	@Column(name="TTRANS_DESC")
	private String ttransDesc;

	//bi-directional many-to-one association to SapAlertasistemasEnvio
    @ManyToOne
	@JoinColumn(name="SAP_IDALERTASISTEMAS_ENVIO")
	private SapAlertasistemasEnvio sapAlertasistemasEnvio;

    public SapAlertasEmision() {
    }

	public long getIdsapAlertasEmision() {
		return this.idsapAlertasEmision;
	}

	public void setIdsapAlertasEmision(long idsapAlertasEmision) {
		this.idsapAlertasEmision = idsapAlertasEmision;
	}

	public String getAgente() {
		return this.agente;
	}

	public void setAgente(String agente) {
		this.agente = agente;
	}

	public String getCanalventa() {
		return this.canalventa;
	}

	public void setCanalventa(String canalventa) {
		this.canalventa = canalventa;
	}

	public String getCia() {
		return this.cia;
	}

	public void setCia(String cia) {
		this.cia = cia;
	}

	public String getCliente1Materno() {
		return this.cliente1Materno;
	}

	public void setCliente1Materno(String cliente1Materno) {
		this.cliente1Materno = cliente1Materno;
	}

	public String getCliente1Nombre() {
		return this.cliente1Nombre;
	}

	public void setCliente1Nombre(String cliente1Nombre) {
		this.cliente1Nombre = cliente1Nombre;
	}

	public String getCliente1Paterno() {
		return this.cliente1Paterno;
	}

	public void setCliente1Paterno(String cliente1Paterno) {
		this.cliente1Paterno = cliente1Paterno;
	}

	public Date getFinVigencia() {
		return this.finVigencia;
	}

	public void setFinVigencia(Date finVigencia) {
		this.finVigencia = finVigencia;
	}

	public String getInciso() {
		return this.inciso;
	}

	public void setInciso(String inciso) {
		this.inciso = inciso;
	}

	public Date getInicioVigencia() {
		return this.inicioVigencia;
	}

	public void setInicioVigencia(Date inicioVigencia) {
		this.inicioVigencia = inicioVigencia;
	}

	public String getMarcaDesc() {
		return this.marcaDesc;
	}

	public void setMarcaDesc(String marcaDesc) {
		this.marcaDesc = marcaDesc;
	}

	public String getModelo() {
		return this.modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getPersonaC1() {
		return this.personaC1;
	}

	public void setPersonaC1(String personaC1) {
		this.personaC1 = personaC1;
	}

	public String getPoliza() {
		return this.poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	public String getTipoDesc() {
		return this.tipoDesc;
	}

	public void setTipoDesc(String tipoDesc) {
		this.tipoDesc = tipoDesc;
	}

	public String getTipoServicio() {
		return this.tipoServicio;
	}

	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

	public String getTtransDesc() {
		return this.ttransDesc;
	}

	public void setTtransDesc(String ttransDesc) {
		this.ttransDesc = ttransDesc;
	}

	public SapAlertasistemasEnvio getSapAlertasistemasEnvio() {
		return this.sapAlertasistemasEnvio;
	}

	public void setSapAlertasistemasEnvio(SapAlertasistemasEnvio sapAlertasistemasEnvio) {
		this.sapAlertasistemasEnvio = sapAlertasistemasEnvio;
	}

	@SuppressWarnings("unchecked")
	public Long getKey() {
		// TODO Auto-generated method stub
		return idsapAlertasEmision;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
}