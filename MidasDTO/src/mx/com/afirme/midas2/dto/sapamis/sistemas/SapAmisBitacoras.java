package mx.com.afirme.midas2.dto.sapamis.sistemas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatEstatusBitacora;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatOperaciones;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatSistemas;

/******************************************************************************
 * 	Entidad para el manejo de los datos de Bitacoras dentro de los Procesos
 *  de envio del SAP-AMIS.
 * 
 * 		Table:		SAPAMISBITACORAS
 * 		Schema:		MIDAS
 * 		Sequence:	SEQSAPAMISBITACORAS
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 * 		
 ******************************************************************************/
@Entity
@Table(name="SAPAMISBITACORAS", schema = "MIDAS")
public class SapAmisBitacoras implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQSAPAMISBITACORAS")
	@SequenceGenerator(name="SEQSAPAMISBITACORAS", schema = "MIDAS", sequenceName="SEQSAPAMISBITACORAS",allocationSize=1)
	@Column(name="IDSAPAMISBITACORAS")
	private Long id;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="IDSAPAMISCATOPERACIONES")
	private CatOperaciones catOperaciones;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="IDSAPAMISCATSISTEMAS")
	private CatSistemas catSistemas;

	//Tambien se usa para contar registros para consultas de Estadisticas (En BD equivale a COUNT(*))
	@Column(name="IDSAPAMISREGISTRO")
	private long idSapAmisRegistro;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHAOPERACION")
	private Date fechaOperacion;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHAREGISTRO")
	private Date fechaRegistro;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHAENVIO")
	private Date fechaEnvio;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ESTATUS")
	private CatEstatusBitacora catEstatusBitacora;

	@Column(name="OBSERVACIONES")
	private String observaciones;

	@Column(name="MODULO")
	private long modulo;
	
	/**
	 * GETTERS AND SETTERS
	 */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CatOperaciones getCatOperaciones() {
		return catOperaciones;
	}

	public void setCatOperaciones(CatOperaciones catOperaciones) {
		this.catOperaciones = catOperaciones;
	}

	public CatSistemas getCatSistemas() {
		return catSistemas;
	}

	public void setCatSistemas(CatSistemas catSistemas) {
		this.catSistemas = catSistemas;
	}

	public long getIdSapAmisRegistro() {
		return idSapAmisRegistro;
	}

	public void setIdSapAmisRegistro(long idSapAmisRegistro) {
		this.idSapAmisRegistro = idSapAmisRegistro;
	}

	public Date getFechaOperacion() {
		return fechaOperacion;
	}

	public void setFechaOperacion(Date fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}
	
	public CatEstatusBitacora getCatEstatusBitacora() {
		return catEstatusBitacora;
	}

	public void setCatEstatusBitacora(CatEstatusBitacora catEstatusBitacora) {
		this.catEstatusBitacora = catEstatusBitacora;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Date getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	public long getModulo() {
		return modulo;
	}

	public void setModulo(long modulo) {
		this.modulo = modulo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}
