package mx.com.afirme.midas2.dto.sapamis.catalogos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="TCSAPAMISSUPERVISIONCAMPO",schema="MIDAS")
public class CatSapAmisSupervisionCampo implements Serializable, Entidad {	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TCSAPAMISSUPERVISIONCAMPO_SEQ")
	@SequenceGenerator(name="TCSAPAMISSUPERVISIONCAMPO_SEQ", schema = "MIDAS", sequenceName="TCSAPAMISSUPERVISIONCAMPO_SEQ",allocationSize=1)
	@Column (name="ID")
	private Long id;
	
	@Column(name="CVEAMIS")
	private Long claveAmis;
	
	@Column(name="SUPERVISIONCAMPO")
	private String supervisionCampo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getClaveAmis() {
		return claveAmis;
	}

	public void setClaveAmis(Long claveAmis) {
		this.claveAmis = claveAmis;
	}

	public String getSupervisionCampo() {
		return supervisionCampo;
	}

	public void setSupervisionCampo(String supervisionCampo) {
		this.supervisionCampo = supervisionCampo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}
	

	@Override
	public String getValue() {
		return claveAmis + supervisionCampo;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}