package mx.com.afirme.midas2.dto.sapamis.alertas;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
/**
 * The persistent class for the SAP_ALERTAS_CESVI database table.
 * 
 */
@Entity
@Table(name="SAP_ALERTAS_CESVI", schema = "MIDAS")
public class SapAlertasCesvi implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQSAP_ALERTAS_CESVIID")
	@SequenceGenerator(name="SEQSAP_ALERTAS_CESVIID", schema = "MIDAS" ,  sequenceName="SEQSAP_ALERTAS_CESVIID",allocationSize=1)
	@Column(name="IDSAP_ALERTAS_CESVI")
	private long idsapAlertasCesvi;

	private String anio;

	private String complemento;

	private String estatus;

	private BigDecimal idmensaje;

	private String linea;

	private String marca;

	private String mensaje;

	@Column(name="MENSAJE_ESTATUS")
	private String mensajeEstatus;

	private String motor;

	private String submarca;

	@Column(name="TIPO_VEHICULO")
	private String tipoVehiculo;

	private String vin;

	//bi-directional many-to-one association to SapAlertasistemasEnvio
    @ManyToOne
	@JoinColumn(name="SAP_IDALERTASISTEMAS_ENVIO")
	private SapAlertasistemasEnvio sapAlertasistemasEnvio;

    public SapAlertasCesvi() {
    }

	public long getIdsapAlertasCesvi() {
		return this.idsapAlertasCesvi;
	}

	public void setIdsapAlertasCesvi(long idsapAlertasCesvi) {
		this.idsapAlertasCesvi = idsapAlertasCesvi;
	}

	public String getAnio() {
		return this.anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public String getComplemento() {
		return this.complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getEstatus() {
		return this.estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public BigDecimal getIdmensaje() {
		return this.idmensaje;
	}

	public void setIdmensaje(BigDecimal idmensaje) {
		this.idmensaje = idmensaje;
	}

	public String getLinea() {
		return this.linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public String getMarca() {
		return this.marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getMensaje() {
		return this.mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getMensajeEstatus() {
		return this.mensajeEstatus;
	}

	public void setMensajeEstatus(String mensajeEstatus) {
		this.mensajeEstatus = mensajeEstatus;
	}

	public String getMotor() {
		return this.motor;
	}

	public void setMotor(String motor) {
		this.motor = motor;
	}

	public String getSubmarca() {
		return this.submarca;
	}

	public void setSubmarca(String submarca) {
		this.submarca = submarca;
	}

	public String getTipoVehiculo() {
		return this.tipoVehiculo;
	}

	public void setTipoVehiculo(String tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}

	public String getVin() {
		return this.vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public SapAlertasistemasEnvio getSapAlertasistemasEnvio() {
		return this.sapAlertasistemasEnvio;
	}

	public void setSapAlertasistemasEnvio(SapAlertasistemasEnvio sapAlertasistemasEnvio) {
		this.sapAlertasistemasEnvio = sapAlertasistemasEnvio;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		// TODO Auto-generated method stub
		return idsapAlertasCesvi;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
