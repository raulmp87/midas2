package mx.com.afirme.midas2.dto.sapamis.catalogos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/******************************************************************************
* 	Entidad para el manejo del catalogo Operaciones.
* 
* 		Table:		SAPAMISCATOPERACIONES
* 		Schema:		MIDAS
* 		Sequence:	SEQSAPAMISCATOPERACIONES
* 
* 	Unidad de Fabrica:	Avance Solution Corporation.
* 
* 	Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
* 		
******************************************************************************/
@Entity
@Table(name="SAPAMISCATOPERACIONES", schema = "MIDAS")
public class CatOperaciones implements Serializable, Entidad{
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="IDSAPAMISCATOPERACIONES")
	private Long id;

	@Column(name="SAPAMISCATOPERACIONES")
	private String descCatOperaciones;
    
    @Column(name="ESTATUS")
    private long estatus;
	
	/**
	 * GETTERS AND SETTERS
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescCatOperaciones() {
		return descCatOperaciones;
	}

	public void setDescCatOperaciones(String descCatOperaciones) {
		this.descCatOperaciones = descCatOperaciones;
	}
    
    public long getEstatus() {
        return estatus;
    }

    public void setEstatus(long estatus) {
        this.estatus = estatus;
    }

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}