package mx.com.afirme.midas2.dto.sapamis.alertas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



/**
 * The persistent class for the SAP_ALERTAS_SINIESTRO database table.
 * 
 */
@Entity
@Table(name="SAP_ALERTAS_SINIESTRO", schema = "MIDAS")
public class SapAlertasSiniestro implements Serializable, mx.com.afirme.midas2.dao.catalogos.Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQSAP_ALERTAS_SINIESTROS")
	@SequenceGenerator(name="SEQSAP_ALERTAS_SINIESTROS", schema = "MIDAS" , sequenceName="SEQSAP_ALERTAS_SINIESTROS",allocationSize=1)
	@Column(name="IDSAP_ALERTAS_SINIESTRO")
	private long idsapAlertasSiniestro;

	@Column(name="CAUSA_SINIESTRO")
	private String causaSiniestro;

	private String cia;

	private String estatus;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHA_SINIESTRO")
	private Date fechaSiniestro;

	private String inciso;

	@Column(name="MONTO_SINIESTRO")
	private String montoSiniestro;

	@Column(name="NUM_SINIESTRO")
	private String numSiniestro;

	private String poliza;

	//bi-directional many-to-one association to SapAlertasistemasEnvio
    @ManyToOne
	@JoinColumn(name="SAP_IDALERTASISTEMAS_ENVIO")
	private SapAlertasistemasEnvio sapAlertasistemasEnvio;

    public SapAlertasSiniestro() {
    }

	public long getIdsapAlertasSiniestro() {
		return this.idsapAlertasSiniestro;
	}

	public void setIdsapAlertasSiniestro(long idsapAlertasSiniestro) {
		this.idsapAlertasSiniestro = idsapAlertasSiniestro;
	}

	public String getCausaSiniestro() {
		return this.causaSiniestro;
	}

	public void setCausaSiniestro(String causaSiniestro) {
		this.causaSiniestro = causaSiniestro;
	}

	public String getCia() {
		return this.cia;
	}

	public void setCia(String cia) {
		this.cia = cia;
	}

	public String getEstatus() {
		return this.estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public Date getFechaSiniestro() {
		return this.fechaSiniestro;
	}

	public void setFechaSiniestro(Date fechaSiniestro) {
		this.fechaSiniestro = fechaSiniestro;
	}

	public String getInciso() {
		return this.inciso;
	}

	public void setInciso(String inciso) {
		this.inciso = inciso;
	}

	public String getMontoSiniestro() {
		return this.montoSiniestro;
	}

	public void setMontoSiniestro(String montoSiniestro) {
		this.montoSiniestro = montoSiniestro;
	}

	public String getNumSiniestro() {
		return this.numSiniestro;
	}

	public void setNumSiniestro(String numSiniestro) {
		this.numSiniestro = numSiniestro;
	}

	public String getPoliza() {
		return this.poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	public SapAlertasistemasEnvio getSapAlertasistemasEnvio() {
		return this.sapAlertasistemasEnvio;
	}

	public void setSapAlertasistemasEnvio(SapAlertasistemasEnvio sapAlertasistemasEnvio) {
		this.sapAlertasistemasEnvio = sapAlertasistemasEnvio;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		// TODO Auto-generated method stub
		return idsapAlertasSiniestro;
	}
	
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
}