package mx.com.afirme.midas2.dto.sapamis.sistemas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatAlertasRechazos;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatBeneficioRelacion;

/******************************************************************************
 * 	Entidad para el manejo de los datos de Rechazos dentro de los Procesos
 *  	de envio del SAP-AMIS.
 * 
 * 		Table:		SAPAMISRECHAZOS
 * 		Schema:		MIDAS
 * 		Sequence:	SEQSAPAMISRECHAZOS
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 * 		
 ******************************************************************************/
@Entity
@Table(name="SAPAMISRECHAZOS", schema = "MIDAS")
public class SapAmisRechazos implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQSAPAMISRECHAZOS")
	@SequenceGenerator(name="SEQSAPAMISRECHAZOS", schema = "MIDAS", sequenceName="SEQSAPAMISRECHAZOS",allocationSize=1)
	@Column(name="IDSAPAMISRECHAZOS")
	private Long id;

	@Column(name="NUMEROSINIESTRO")
	private String numeroSiniestro;

	@ManyToOne
	@JoinColumn(name="IDSAPAMISCATBENEFICIORELACION")
	private CatBeneficioRelacion catBeneficioRelacion;

	@Column(name="DESISTIMIENTO")
	private String desistimiento;

	@Column(name="MONTORECHAZO")
	private Double montoRechazo;

	@Column(name="DETECTADOSAP")
	private String detectadoSAP;

	@Column(name="OBSERVACIONES")
	private String observaciones;

	@ManyToOne
	@JoinColumn(name="IDSAPAMISCATALERTASRECHAZOS")
	private CatAlertasRechazos catAlertasRechazos;

    @Temporal(TemporalType.DATE)
	@Column(name="FECHARECHAZO")
	private Date fecha;

	@OneToOne
    @JoinColumn(updatable=false,insertable=false, name="IDSAPAMISBITACORAS", referencedColumnName="IDSAPAMISBITACORAS")
	private SapAmisBitacoras sapAmisBitacoras;

	/**
	 * GETTERS AND SETTERS
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}

	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}

	public CatBeneficioRelacion getCatBeneficioRelacion() {
		return catBeneficioRelacion;
	}

	public void setCatBeneficioRelacion(CatBeneficioRelacion catBeneficioRelacion) {
		this.catBeneficioRelacion = catBeneficioRelacion;
	}

	public String getDesistimiento() {
		return desistimiento;
	}

	public void setDesistimiento(String desistimiento) {
		this.desistimiento = desistimiento;
	}

	public Double getMontoRechazo() {
		return montoRechazo;
	}

	public void setMontoRechazo(Double montoRechazo) {
		this.montoRechazo = montoRechazo;
	}

	public String getDetectadoSAP() {
		return detectadoSAP;
	}

	public void setDetectadoSAP(String detectadoSAP) {
		this.detectadoSAP = detectadoSAP;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public CatAlertasRechazos getCatAlertasRechazos() {
		return catAlertasRechazos;
	}

	public void setCatAlertasRechazos(CatAlertasRechazos catAlertasRechazos) {
		this.catAlertasRechazos = catAlertasRechazos;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public SapAmisBitacoras getSapAmisBitacoras() {
		return sapAmisBitacoras;
	}

	public void setSapAmisBitacoras(SapAmisBitacoras sapAmisBitacoras) {
		this.sapAmisBitacoras = sapAmisBitacoras;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}