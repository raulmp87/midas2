package mx.com.afirme.midas2.dto.sapamis.alertas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the SAP_ALERTAS_PREVENCION database table.
 * 
 */
@Entity
@Table(name="SAP_ALERTAS_PREVENCION" , schema = "MIDAS")
public class SapAlertasPrevencion implements Serializable, mx.com.afirme.midas2.dao.catalogos.Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQSAP_ALERTAS_PREVENCION")
	@SequenceGenerator(name="SEQSAP_ALERTAS_PREVENCION", schema = "MIDAS" , sequenceName="SEQSAP_ALERTAS_PREVENCION",allocationSize=1)
	@Column(name="IDSAP_ALERTAS_PREVENCION")
	private long idsapAlertasPrevencion;

	private String causaprevencion;

	private String cia;

	private String monto;

	private String noserie;

	private String observaciones;

	private String placa;

	//bi-directional many-to-one association to SapAlertasistemasEnvio
    @ManyToOne
	@JoinColumn(name="SAP_IDALERTASISTEMAS_ENVIO")
	private SapAlertasistemasEnvio sapAlertasistemasEnvio;

    public SapAlertasPrevencion() {
    }

	public long getIdsapAlertasPrevencion() {
		return this.idsapAlertasPrevencion;
	}

	public void setIdsapAlertasPrevencion(long idsapAlertasPrevencion) {
		this.idsapAlertasPrevencion = idsapAlertasPrevencion;
	}

	public String getCausaprevencion() {
		return this.causaprevencion;
	}

	public void setCausaprevencion(String causaprevencion) {
		this.causaprevencion = causaprevencion;
	}

	public String getCia() {
		return this.cia;
	}

	public void setCia(String cia) {
		this.cia = cia;
	}

	public String getMonto() {
		return this.monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getNoserie() {
		return this.noserie;
	}

	public void setNoserie(String noserie) {
		this.noserie = noserie;
	}

	public String getObservaciones() {
		return this.observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getPlaca() {
		return this.placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public SapAlertasistemasEnvio getSapAlertasistemasEnvio() {
		return this.sapAlertasistemasEnvio;
	}

	public void setSapAlertasistemasEnvio(SapAlertasistemasEnvio sapAlertasistemasEnvio) {
		this.sapAlertasistemasEnvio = sapAlertasistemasEnvio;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		// TODO Auto-generated method stub
		return idsapAlertasPrevencion;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
}