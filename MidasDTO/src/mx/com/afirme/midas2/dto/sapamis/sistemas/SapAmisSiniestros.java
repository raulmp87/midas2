package mx.com.afirme.midas2.dto.sapamis.sistemas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatCausaSiniestro;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatTipoAfectado;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatUbicacionMunicipio;

/******************************************************************************
 * 	Entidad para el manejo de los datos de Siniestros dentro de los Procesos
 *  de envio del SAP-AMIS.
 * 
 * 	@author Eduardo.Chavez
 * 
 * 		Table:		SAPAMISSINIESTROS
 * 		Schema:		MIDAS
 * 		Sequence:	SEQSAPAMISSINIESTROS
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 * 		
 ******************************************************************************/
@Entity
@Table(name = "SAPAMISSINIESTROS", schema = "MIDAS")
public class SapAmisSiniestros implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQSAPAMISSINIESTROS")
	@SequenceGenerator(name = "SEQSAPAMISSINIESTROS", schema = "MIDAS", sequenceName = "SEQSAPAMISSINIESTROS", allocationSize = 1)
	@Column(name = "IDSAPAMISSINIESTROS")
	private Long id;

	@Column(name = "NOSINIESTRO")
	private String numSiniestro;

	@Column(name = "POLIZA")
	private String poliza;

	@Column(name = "INCISO")
	private String inciso;

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHASINIESTRO")
	private Date fechaSiniestro;

	@Column(name = "NOSERIE")
	private String serie;

	@ManyToOne
	@JoinColumn(name = "CAUSASINIESTRO")
	private CatCausaSiniestro causaSiniestro;

	@Column(name = "MONTO")
	private Double monto;

	@ManyToOne
	@JoinColumn(name = "UBICACIONSINIESTRO")
	private CatUbicacionMunicipio ubicacionSiniestro;

	@Column(name = "CONDUTORAPPATERNO")
	private String conductorApPaterno;

	@Column(name = "CONDUTORAPMATERNO")
	private String conductorApMaterno;

	@Column(name = "CONDUTORNOMBRE")
	private String conductorNombre;

	@Column(name = "CONDUTORRFC")
	private String conductorRfc;

	@Column(name = "CONDUTORCURP")
	private String conductorCurp;

	@Column(name = "CONDUTORTELEFONO")
	private String conductorTelefono;

	@ManyToOne
	@JoinColumn(name = "INVOLUCRADOSINIESTRO")
	private CatTipoAfectado involucradoSiniestro;

	@OneToOne
    @JoinColumn(updatable=false,insertable=false, name="IDSAPAMISBITACORAS", referencedColumnName="IDSAPAMISBITACORAS")
	private SapAmisBitacoras sapAmisBitacoras;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumSiniestro() {
		return numSiniestro;
	}

	public void setNumSiniestro(String numSiniestro) {
		this.numSiniestro = numSiniestro;
	}

	public String getPoliza() {
		return poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	public String getInciso() {
		return inciso;
	}

	public void setInciso(String inciso) {
		this.inciso = inciso;
	}

	public Date getFechaSiniestro() {
		return fechaSiniestro;
	}

	public void setFechaSiniestro(Date fechaSiniestro) {
		this.fechaSiniestro = fechaSiniestro;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public CatCausaSiniestro getCausaSiniestro() {
		return causaSiniestro;
	}

	public void setCausaSiniestro(CatCausaSiniestro causaSiniestro) {
		this.causaSiniestro = causaSiniestro;
	}

	public Double getMonto() {
		return monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}

	public String getConductorApPaterno() {
		return conductorApPaterno;
	}

	public void setConductorApPaterno(String conductorApPaterno) {
		this.conductorApPaterno = conductorApPaterno;
	}

	public String getConductorApMaterno() {
		return conductorApMaterno;
	}

	public void setConductorApMaterno(String conductorApMaterno) {
		this.conductorApMaterno = conductorApMaterno;
	}

	public String getConductorNombre() {
		return conductorNombre;
	}

	public void setConductorNombre(String conductorNombre) {
		this.conductorNombre = conductorNombre;
	}

	public String getConductorRfc() {
		return conductorRfc;
	}

	public void setConductorRfc(String conductorRfc) {
		this.conductorRfc = conductorRfc;
	}

	public String getConductorCurp() {
		return conductorCurp;
	}

	public void setConductorCurp(String conductorCurp) {
		this.conductorCurp = conductorCurp;
	}

	public String getConductorTelefono() {
		return conductorTelefono;
	}

	public void setConductorTelefono(String conductorTelefono) {
		this.conductorTelefono = conductorTelefono;
	}

	public CatTipoAfectado getInvolucradoSiniestro() {
		return involucradoSiniestro;
	}

	public void setInvolucradoSiniestro(CatTipoAfectado involucradoSiniestro) {
		this.involucradoSiniestro = involucradoSiniestro;
	}

	public CatUbicacionMunicipio getUbicacionSiniestro() {
		return ubicacionSiniestro;
	}

	public void setUbicacionSiniestro(CatUbicacionMunicipio ubicacionSiniestro) {
		this.ubicacionSiniestro = ubicacionSiniestro;
	}

	public SapAmisBitacoras getSapAmisBitacoras() {
		return sapAmisBitacoras;
	}

	public void setSapAmisBitacoras(SapAmisBitacoras sapAmisBitacoras) {
		this.sapAmisBitacoras = sapAmisBitacoras;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}