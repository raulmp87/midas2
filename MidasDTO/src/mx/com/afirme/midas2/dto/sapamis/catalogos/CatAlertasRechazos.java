package mx.com.afirme.midas2.dto.sapamis.catalogos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="SAPAMISCATALERTASRECHAZOS", schema = "MIDAS")
public class CatAlertasRechazos implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="IDSAPAMISCATALERTASRECHAZOS")
	private Long id;

	@Column(name="SAPAMISCATALERTASRECHAZOS")
	private String descCatAlertasRechazos;

	@Column(name="ALERTACLAVESAPAMIS")
	private String claveAmis;
	
	@Column(name="ESTATUS")
	private long estatus;

	/**
	 * GETTERS AND SETTERS
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescCatAlertasRechazos() {
		return descCatAlertasRechazos;
	}

	public void setDescCatAlertasRechazos(String descCatAlertasRechazos) {
		this.descCatAlertasRechazos = descCatAlertasRechazos;
	}

	public String getClaveAmis() {
		return claveAmis;
	}

	public void setClaveAmis(String claveAmis) {
		this.claveAmis = claveAmis;
	}
	
	public long getEstatus() {
		return estatus;
	}

	public void setEstatus(long estatus) {
		this.estatus = estatus;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}
