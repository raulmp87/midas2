package mx.com.afirme.midas2.dto.sapamis.catalogos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="SAPAMISCATCAUSABENEFICIO", schema = "MIDAS")
public class CatCausaBeneficio implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="IDSAPAMISCATCAUSABENEFICIO")
	private Long id;

	@Column(name="SAPAMISCATCAUSABENEFICIO")
	private String descCatCausaBeneficio;

	@Column(name="ESTATUS")
	private long estatus;
	
	/**
	 * GETTERS AND SETTERS
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescCatCausaBeneficio() {
		return descCatCausaBeneficio;
	}

	public void setDescCatCausaBeneficio(String descCatCausaBeneficio) {
		this.descCatCausaBeneficio = descCatCausaBeneficio;
	}

	public long getEstatus() {
		return estatus;
	}

	public void setEstatus(long estatus) {
		this.estatus = estatus;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}
