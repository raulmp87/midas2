package mx.com.afirme.midas2.dto.sapamis.otros;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasistemasEnvio;


/**
 * The persistent class for the SAP_AMIS_BITACORA_SINIESTROS database table.
 * 
 */
@Entity
@Table(name="SAP_AMIS_BITACORA_SINIESTROS", schema = "MIDAS")
public class SapAmisBitacoraSiniestros implements Serializable, mx.com.afirme.midas2.dao.catalogos.Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQSAP_AMIS_BITACORA_SINIES")
	@SequenceGenerator(name="SEQSAP_AMIS_BITACORA_SINIES", schema = "MIDAS", sequenceName="SEQSAP_AMIS_BITACORA_SINIES",allocationSize=1)
	@Column(name="IDSAP_AMIS_BITACORA_SINIESTROS")
	private long idSapAmisBitacoraSiniestros;
	
	@Column(name="SESSION_ID")
	private long sessionId;

	@Column(name="USUARIO_SAP")
	private String usuarioSap;

	@Column(name="PASSWORD_SAP")
	private String passwordSap;

	@Column(name="NUMEROPOLIZA")
	private String numeroPoliza;

	@Column(name="INCISO")
	private String inciso;

    @Temporal(TemporalType.DATE)
	@Column(name="FECHASINIESTRO")
	private Date fechaSiniestro;

	@Column(name="NUMEROSERIE")
	private String numeroSerie;

	@Column(name="CAUSASINIESTRO")
	private long causaSiniestro;
	
	@Column(name="MONTOOCURRIDOSINIESTRO")
	private long montoOcurridoSiniestro;

	@Column(name="UBICACIONDELSINIESTRO_PAIS")
	private long ubicacionSiniestroPais;

	@Column(name="UBICACIONDELSINIESTRO_ESTADO")
	private long ubicacionSiniestroEstado;

	@Column(name="UBICACIONDELSINIESTRO_MUNI")
	private long ubicacionSiniestroMunicipio;
	
	@Column(name="AFECTADO")
	private long afectado;
	
	@Column(name="AFECTADOPATERNO")
	private String afectadoApPaterno;

	@Column(name="AFECTADOMATERNO")
	private String afectadoApMaterno;

	@Column(name="AFECTADONOMBRE")
	private String afectadoNombre;

	@Column(name="AFECTADORFC")
	private String afectadoRFC;

	@Column(name="AFECTADOCURP")
	private String afectadoCURP;

	@Column(name="CONDUCTORPATERNO")
	
	private String conductorApPaterno;

	@Column(name="CONDUCTORNOMBRE")
	private String conductorNombre;

	@Column(name="CONDUCTORRFC")
	private String conductorRFC;

	@Column(name="CONDUCTORCURP")
	private String conductorCURP;

    @Temporal(TemporalType.DATE)
	@Column(name="FECHACANCELACION")
	private Date fechaCancelacion;

	@Column(name="DESISTIMIENTO")
	private String desistimiento;

	@Column(name="MONTO_RECHAZO")
	private long montoRechazo;

	@Column(name="DETECTADO_SAP")
	private String detectadoSap;

	@Column(name="OBSERVACIONES")
	private String observaciones;

	@Column(name="BANDERA_OPERACION")
	private String banderaOperacion;

    @Temporal(TemporalType.DATE)
	@Column(name="FECHA_ENVIO_SAP")
	private Date fechaEnvioSap;

	@Column(name="ESTATUS_RESP_OPERACION")
	private String estatusRespOperacion;

    @Temporal(TemporalType.DATE)
	@Column(name="FECHAFIN_RESP_SAP")
	private Date fechaFinRespSap;

	@Column(name="ESTATUS_RESP_VALIDACION")
	private String estatusRespValidacion;

	@Column(name="ESTATUS_RESP_ALERTAS")
	private String estatusRespAlertas;

	@Column(name="MENSAGE_VALIDACION")
	private String mensajeValidacion;

	//bi-directional many-to-one association to SapAlertasistemasEnvio
	@ManyToOne
	@JoinColumn(name="IDSAP_ALERTASISTEMAS_ENVIO")
	private SapAlertasistemasEnvio sapAlertasistemasEnvio;

	@Column(name="NUMEROSINIESTRO")
	private String numeroSiniestro;

	@Column(name="CONDUCTORMATERNO")
	private String conductorApMaterno;
	
	@Column(name="LOTE_ID")
	private long loteId;	
	
	@Column(name="ID_REPORTE_SIN")
	private long idReporteSiniestro;
	
	/*******************************************
	 * 										   *
	 * Seccion de Métodos Getters and Setters. *
	 * 										   *
	 *******************************************/
	public long getIdSapAmisBitacoraSiniestros() {
		return this.idSapAmisBitacoraSiniestros;
	}

	public void setIdSapAmisBitacoraSiniestros(long idSapAmisBitacoraSiniestros) {
		this.idSapAmisBitacoraSiniestros = idSapAmisBitacoraSiniestros;
	}

	public long getAfectado() {
		return this.afectado;
	}

	public void setAfectado(long afectado) {
		this.afectado = afectado;
	}

	public String getAfectadoCURP() {
		return this.afectadoCURP;
	}

	public void setAfectadoCURP(String afectadoCURP) {
		this.afectadoCURP = afectadoCURP;
	}

	public String getAfectadoRFC() {
		return this.afectadoRFC;
	}

	public void setAfectadoRFC(String afectadoRFC) {
		this.afectadoRFC = afectadoRFC;
	}

	public void setBanderaOperacion(String banderaOperacion) {
		this.banderaOperacion = banderaOperacion;
	}

	public String getConductorCURP() {
		return this.conductorCURP;
	}

	public void setConductorCURP(String conductorCURP) {
		this.conductorCURP = conductorCURP;
	}

	public String getConductorRFC() {
		return this.conductorRFC;
	}

	public void setConductorRFC(String conductorRFC) {
		this.conductorRFC = conductorRFC;
	}

	public String getDesistimiento() {
		return this.desistimiento;
	}

	public void setDesistimiento(String desistimiento) {
		this.desistimiento = desistimiento;
	}

	public String getDetectadoSap() {
		return this.detectadoSap;
	}

	public void setDetectadoSap(String detectadoSap) {
		this.detectadoSap = detectadoSap;
	}

	public String getEstatusRespAlertas() {
		return this.estatusRespAlertas;
	}

	public void setEstatusRespAlertas(String estatusRespAlertas) {
		this.estatusRespAlertas = estatusRespAlertas;
	}

	public String getEstatusRespOperacion() {
		return estatusRespOperacion;
	}

	public void setEstatusRespOperacion(String estatusRespOperacion) {
		this.estatusRespOperacion = estatusRespOperacion;
	}

	public String getEstatusRespValidacion() {
		return this.estatusRespValidacion;
	}

	public void setEstatusRespValidacion(String estatusRespValidacion) {
		this.estatusRespValidacion = estatusRespValidacion;
	}

	public Date getFechaEnvioSap() {
		return this.fechaEnvioSap;
	}

	public void setFechaEnvioSap(Date fechaEnvioSap) {
		this.fechaEnvioSap = fechaEnvioSap;
	}

	public String getInciso() {
		return this.inciso;
	}

	public void setInciso(String inciso) {
		this.inciso = inciso;
	}

	public long getMontoRechazo() {
		return this.montoRechazo;
	}

	public void setMontoRechazo(long montoRechazo) {
		this.montoRechazo = montoRechazo;
	}

	public String getObservaciones() {
		return this.observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getPasswordSap() {
		return this.passwordSap;
	}

	public void setPasswordSap(String passwordSap) {
		this.passwordSap = passwordSap;
	}

	public String getUsuarioSap() {
		return this.usuarioSap;
	}

	public void setUsuarioSap(String usuarioSap) {
		this.usuarioSap = usuarioSap;
	}

	public SapAlertasistemasEnvio getSapAlertasistemasEnvio() {
		return this.sapAlertasistemasEnvio;
	}

	public void setSapAlertasistemasEnvio(SapAlertasistemasEnvio sapAlertasistemasEnvio) {
		this.sapAlertasistemasEnvio = sapAlertasistemasEnvio;
	}

	public long getSessionId() {
		return sessionId;
	}

	public void setSessionId(long sessionId) {
		this.sessionId = sessionId;
	}

	public long getLoteId() {
		return loteId;
	}

	public void setLoteId(long loteId) {
		this.loteId = loteId;
	}

	public long getIdReporteSiniestro() {
		return idReporteSiniestro;
	}

	public void setIdReporteSiniestro(long idReporteSiniestro) {
		this.idReporteSiniestro = idReporteSiniestro;
	}

	public String getConductorApMaterno() {
		return conductorApMaterno;
	}

	public void setConductorApMaterno(String conductorApMaterno) {
		this.conductorApMaterno = conductorApMaterno;
	}

	public String getAfectadoApMaterno() {
		return afectadoApMaterno;
	}

	public void setAfectadoApMaterno(String afectadoApMaterno) {
		this.afectadoApMaterno = afectadoApMaterno;
	}

	public String getAfectadoNombre() {
		return afectadoNombre;
	}

	public void setAfectadoNombre(String afectadoNombre) {
		this.afectadoNombre = afectadoNombre;
	}

	public String getAfectadoApPaterno() {
		return afectadoApPaterno;
	}

	public void setAfectadoApPaterno(String afectadoApPaterno) {
		this.afectadoApPaterno = afectadoApPaterno;
	}

	public long getCausaSiniestro() {
		return causaSiniestro;
	}

	public void setCausaSiniestro(long causaSiniestro) {
		this.causaSiniestro = causaSiniestro;
	}

	public String getConductorNombre() {
		return conductorNombre;
	}

	public void setConductorNombre(String conductorNombre) {
		this.conductorNombre = conductorNombre;
	}

	public String getConductorApPaterno() {
		return conductorApPaterno;
	}

	public void setConductorApPaterno(String conductorApPaterno) {
		this.conductorApPaterno = conductorApPaterno;
	}

	public Date getFechaCancelacion() {
		return fechaCancelacion;
	}

	public void setFechaCancelacion(Date fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}

	public Date getFechaFinRespSap() {
		return fechaFinRespSap;
	}

	public void setFechaFinRespSap(Date fechaFinRespSap) {
		this.fechaFinRespSap = fechaFinRespSap;
	}

	public Date getFechaSiniestro() {
		return fechaSiniestro;
	}

	public void setFechaSiniestro(Date fechaSiniestro) {
		this.fechaSiniestro = fechaSiniestro;
	}

	public String getMensajeValidacion() {
		return mensajeValidacion;
	}

	public void setMensajeValidacion(String mensajeValidacion) {
		this.mensajeValidacion = mensajeValidacion;
	}

	public long getMontoOcurridoSiniestro() {
		return montoOcurridoSiniestro;
	}

	public void setMontoOcurridoSiniestro(long montoOcurridoSiniestro) {
		this.montoOcurridoSiniestro = montoOcurridoSiniestro;
	}

	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}

	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}

	public long getUbicacionSiniestroEstado() {
		return ubicacionSiniestroEstado;
	}

	public void setUbicacionSiniestroEstado(long ubicacionSiniestroEstado) {
		this.ubicacionSiniestroEstado = ubicacionSiniestroEstado;
	}

	public long getUbicacionSiniestroMunicipio() {
		return ubicacionSiniestroMunicipio;
	}

	public void setUbicacionSiniestroMunicipio(long ubicacionSiniestroMunicipio) {
		this.ubicacionSiniestroMunicipio = ubicacionSiniestroMunicipio;
	}

	public long getUbicacionSiniestroPais() {
		return ubicacionSiniestroPais;
	}

	public void setUbicacionSiniestroPais(long ubicacionSiniestroPais) {
		this.ubicacionSiniestroPais = ubicacionSiniestroPais;
	}

	public String getBanderaOperacion() {
		return banderaOperacion;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Object getKey() {
		// TODO Auto-generated method stub
		return getIdSapAmisBitacoraSiniestros()==0?null:getIdSapAmisBitacoraSiniestros();
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
}
