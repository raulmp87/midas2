package mx.com.afirme.midas2.dto.sapamis.catalogos;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/******************************************************************************
 * Entidad para el manejo de la Relacion entre los objetos
 * 
 * 	CatSistemas y CatOperacion
 * 
 * @author Eduardo.Chavez
 * 
 * 		Table:		SAPAMISCATRELSISOPER
 * 		Schema:		MIDAS
 * 		Sequence:	SEQSAPAMISCATRELSISOPER
 * 		
 ******************************************************************************/
@Entity
@Table(name="SAPAMISCATRELSISOPER", schema = "MIDAS")
public class RelSistemaOperacion implements Serializable, Entidad{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQSAPAMISCATRELSISOPER")
	@SequenceGenerator(name = "SEQSAPAMISCATRELSISOPER", schema = "MIDAS", sequenceName = "SEQSAPAMISCATRELSISOPER", allocationSize = 1)
	@Column(name = "IDSAPAMISCATRELSISOPER")
	private Long id;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="IDSAPAMISCATOPERACIONES")
	private CatOperaciones catOperaciones;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="IDSAPAMISCATSISTEMAS")
	private CatSistemas catSistemas;
    
    @Column(name="ESTATUS")
    private long estatus;
    
    @Column(name="MODULO")
    private long modulo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CatOperaciones getCatOperaciones() {
		return catOperaciones;
	}

	public void setCatOperaciones(CatOperaciones catOperaciones) {
		this.catOperaciones = catOperaciones;
	}

	public CatSistemas getCatSistemas() {
		return catSistemas;
	}

	public void setCatSistemas(CatSistemas catSistemas) {
		this.catSistemas = catSistemas;
	}

	public long getEstatus() {
		return estatus;
	}

	public void setEstatus(long estatus) {
		this.estatus = estatus;
	}

	public long getModulo() {
		return modulo;
	}

	public void setModulo(long modulo) {
		this.modulo = modulo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}
