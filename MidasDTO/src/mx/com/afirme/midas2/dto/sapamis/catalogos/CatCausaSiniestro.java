package mx.com.afirme.midas2.dto.sapamis.catalogos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="SAPAMISCATCAUSASINIESTRO", schema = "MIDAS")
public class CatCausaSiniestro implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="IDSAPAMISCATCAUSASINIESTRO")
	private Long id;

	@Column(name="SAPAMISCATCAUSASINIESTRO")
	private String descCatCausaSiniestro;
    
    @Column(name="ESTATUS")
    private long estatus;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescCatCausaSiniestro() {
		return descCatCausaSiniestro;
	}

	public void setDescCatCausaSiniestro(String descCatCausaSiniestro) {
		this.descCatCausaSiniestro = descCatCausaSiniestro;
	}
    
    public long getEstatus() {
        return estatus;
    }

    public void setEstatus(long estatus) {
        this.estatus = estatus;
    }

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}