package mx.com.afirme.midas2.dto.sapamis.alertas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



/**
 * The persistent class for the SAP_ALERTAS_PT database table.
 * 
 */
@Entity
@Table(name="SAP_ALERTAS_PT" , schema = "MIDAS")
public class SapAlertasPt implements Serializable, mx.com.afirme.midas2.dao.catalogos.Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQSAP_ALERTAS_PT")
	@SequenceGenerator(name="SEQSAP_ALERTAS_PT", schema = "MIDAS" , sequenceName="SEQSAP_ALERTAS_PT",allocationSize=1)
	@Column(name="IDSAP_ALERTAS_PT")
	private long idsapAlertasPt;

	private String cia;

	@Column(name="ESTATUS_VEH_PAGO")
	private String estatusVehPago;

	@Column(name="ESTATUS_VEH_VTA")
	private String estatusVehVta;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHA_FACTURA")
	private Date fechaFactura;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHA_FINIQUITO")
	private Date fechaFiniquito;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHA_PAGO")
	private Date fechaPago;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHA_SINIESTRO")
	private Date fechaSiniestro;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHA_VENTA")
	private Date fechaVenta;

	private String importeventa;

	@Column(name="MARCA_DESC")
	private String marcaDesc;

	private String modelo;

	@Column(name="MONTO_PAGO")
	private String montoPago;

	@Column(name="NUM_SINIESTRO")
	private String numSiniestro;

	private String placa;

	@Column(name="PORCENTAJE_PERDIDA")
	private String porcentajePerdida;

	@Column(name="TIPO_DANO")
	private String tipoDano;

	@Column(name="TIPO_DESC")
	private String tipoDesc;

	@Column(name="TIPO_PAGO")
	private String tipoPago;

	@Column(name="TIPO_VENTA")
	private String tipoVenta;

	@Column(name="TTRANS_DESC")
	private String ttransDesc;

	private String uso;

	//bi-directional many-to-one association to SapAlertasistemasEnvio
    @ManyToOne
	@JoinColumn(name="SAP_IDALERTASISTEMAS_ENVIO")
	private SapAlertasistemasEnvio sapAlertasistemasEnvio;

    public SapAlertasPt() {
    }

	public long getIdsapAlertasPt() {
		return this.idsapAlertasPt;
	}

	public void setIdsapAlertasPt(long idsapAlertasPt) {
		this.idsapAlertasPt = idsapAlertasPt;
	}

	public String getCia() {
		return this.cia;
	}

	public void setCia(String cia) {
		this.cia = cia;
	}

	public String getEstatusVehPago() {
		return this.estatusVehPago;
	}

	public void setEstatusVehPago(String estatusVehPago) {
		this.estatusVehPago = estatusVehPago;
	}

	public String getEstatusVehVta() {
		return this.estatusVehVta;
	}

	public void setEstatusVehVta(String estatusVehVta) {
		this.estatusVehVta = estatusVehVta;
	}

	public Date getFechaFactura() {
		return this.fechaFactura;
	}

	public void setFechaFactura(Date fechaFactura) {
		this.fechaFactura = fechaFactura;
	}

	public Date getFechaFiniquito() {
		return this.fechaFiniquito;
	}

	public void setFechaFiniquito(Date fechaFiniquito) {
		this.fechaFiniquito = fechaFiniquito;
	}

	public Date getFechaPago() {
		return this.fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	public Date getFechaSiniestro() {
		return this.fechaSiniestro;
	}

	public void setFechaSiniestro(Date fechaSiniestro) {
		this.fechaSiniestro = fechaSiniestro;
	}

	public Date getFechaVenta() {
		return this.fechaVenta;
	}

	public void setFechaVenta(Date fechaVenta) {
		this.fechaVenta = fechaVenta;
	}

	public String getImporteventa() {
		return this.importeventa;
	}

	public void setImporteventa(String importeventa) {
		this.importeventa = importeventa;
	}

	public String getMarcaDesc() {
		return this.marcaDesc;
	}

	public void setMarcaDesc(String marcaDesc) {
		this.marcaDesc = marcaDesc;
	}

	public String getModelo() {
		return this.modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getMontoPago() {
		return this.montoPago;
	}

	public void setMontoPago(String montoPago) {
		this.montoPago = montoPago;
	}

	public String getNumSiniestro() {
		return this.numSiniestro;
	}

	public void setNumSiniestro(String numSiniestro) {
		this.numSiniestro = numSiniestro;
	}

	public String getPlaca() {
		return this.placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getPorcentajePerdida() {
		return this.porcentajePerdida;
	}

	public void setPorcentajePerdida(String porcentajePerdida) {
		this.porcentajePerdida = porcentajePerdida;
	}

	public String getTipoDano() {
		return this.tipoDano;
	}

	public void setTipoDano(String tipoDano) {
		this.tipoDano = tipoDano;
	}

	public String getTipoDesc() {
		return this.tipoDesc;
	}

	public void setTipoDesc(String tipoDesc) {
		this.tipoDesc = tipoDesc;
	}

	public String getTipoPago() {
		return this.tipoPago;
	}

	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}

	public String getTipoVenta() {
		return this.tipoVenta;
	}

	public void setTipoVenta(String tipoVenta) {
		this.tipoVenta = tipoVenta;
	}

	public String getTtransDesc() {
		return this.ttransDesc;
	}

	public void setTtransDesc(String ttransDesc) {
		this.ttransDesc = ttransDesc;
	}

	public String getUso() {
		return this.uso;
	}

	public void setUso(String uso) {
		this.uso = uso;
	}

	public SapAlertasistemasEnvio getSapAlertasistemasEnvio() {
		return this.sapAlertasistemasEnvio;
	}

	public void setSapAlertasistemasEnvio(SapAlertasistemasEnvio sapAlertasistemasEnvio) {
		this.sapAlertasistemasEnvio = sapAlertasistemasEnvio;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return idsapAlertasPt;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
}