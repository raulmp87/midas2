package mx.com.afirme.midas2.dto.sapamis.accionesalertas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
/******************************************************************************
 * Entidad para el manejo del log de las Acciones sobre las Alertas del 
 * SAP-AMIS
 * 
 * @author Eduardo.Chavez
 * 
 * 		Table:		SAP_AMIS_ACIONES_LOG
 * 		Schema:		MIDAS
 * 		Sequence:	SEQSAP_AMIS_ACIONES_LOG
 * 		
 ******************************************************************************/
@Entity
@Table(name="SAPAMISACCIONESLOG", schema = "MIDAS")
public class SapAmisAccionesLog implements Serializable, mx.com.afirme.midas2.dao.catalogos.Entidad {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQSAPAMISACCIONESLOG")
	@SequenceGenerator(name="SEQSAPAMISACCIONESLOG", schema = "MIDAS", sequenceName="SEQSAPAMISACCIONESLOG",allocationSize=1)
	@Column(name="IDSAPAMISACCIONESLOG")
	private long idSapAmisAccionesLog;
	
    @Temporal( TemporalType.DATE)
	@Column(name="FECHA")
	private Date fecha;
    
	@Column(name="TIPOMODIFICACION")
	private int tipoModificacion;
	
	@Column(name="USUARIO")
	private String usuario;

	@ManyToOne
	@JoinColumn(name="IDSAPAMISACCIONES")
	private SapAmisAcciones sapAmisAcciones;
	
	/*****************************
	 * Inician Getters y Setters *
	 *****************************/
	
	public long getIdSapAmisAccionesLog() {
		return idSapAmisAccionesLog;
	}
	public void setIdSapAmisAccionesLog(long idSapAmisAccionesLog) {
		this.idSapAmisAccionesLog = idSapAmisAccionesLog;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public int getTipoModificacion() {
		return tipoModificacion;
	}
	public void setTipoModificacion(int tipoModificacion) {
		this.tipoModificacion = tipoModificacion;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public SapAmisAcciones getSapAmisAcciones() {
		return sapAmisAcciones;
	}
	public void setSapAmisAcciones(SapAmisAcciones sapAmisAcciones) {
		this.sapAmisAcciones = sapAmisAcciones;
	}
	
	/************************************************
	 * Mètodos implementados de la interfaz Entidad *
	 * Para el uso generico de los DAO y Servicios  *
	 ************************************************/
	
	@SuppressWarnings("unchecked")
	@Override
	public Object getKey() {
		return getIdSapAmisAccionesLog()==0?null:getIdSapAmisAccionesLog();
	}
	@Override
	public String getValue() {
		return usuario;
	}
	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
}
