package mx.com.afirme.midas2.dto.sapamis.alertas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;




/**
 * The persistent class for the SAP_ALERTAS_CII database table.
 * 
 */
@Entity
@Table(name="SAP_ALERTAS_CII", schema = "MIDAS")
public class SapAlertasCii implements Serializable, mx.com.afirme.midas2.dao.catalogos.Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQSAP_ALERTAS_CIIID")
	@SequenceGenerator(name="SEQSAP_ALERTAS_CIIID", schema = "MIDAS" , sequenceName="SEQSAP_ALERTAS_CIIID",allocationSize=1)
	@Column(name="IDSAP_ALERTAS_CII")
	private long idsapAlertasCii;

	private String cia;

    @Temporal( TemporalType.DATE)
	private Date fechadeteccion;

    @Temporal( TemporalType.DATE)
	private Date fechasiniestro;

	private String importe;

	@Column(name="NUM_POLIZA")
	private String numPoliza;

	@Column(name="NUM_SINIESTRO")
	private String numSiniestro;

	@Column(name="TIPO_ATENCION")
	private String tipoAtencion;

	@Column(name="TIPO_HECHO")
	private String tipoHecho;

	@Column(name="TIPO_SINIESTRO")
	private String tipoSiniestro;

	//bi-directional many-to-one association to SapAlertasistemasEnvio
    @ManyToOne
	@JoinColumn(name="SAP_IDALERTASISTEMAS_ENVIO")
	private SapAlertasistemasEnvio sapAlertasistemasEnvio;

    public SapAlertasCii() {
    }

	public long getIdsapAlertasCii() {
		return this.idsapAlertasCii;
	}

	public void setIdsapAlertasCii(long idsapAlertasCii) {
		this.idsapAlertasCii = idsapAlertasCii;
	}

	public String getCia() {
		return this.cia;
	}

	public void setCia(String cia) {
		this.cia = cia;
	}

	public Date getFechadeteccion() {
		return this.fechadeteccion;
	}

	public void setFechadeteccion(Date fechadeteccion) {
		this.fechadeteccion = fechadeteccion;
	}

	public Date getFechasiniestro() {
		return this.fechasiniestro;
	}

	public void setFechasiniestro(Date fechasiniestro) {
		this.fechasiniestro = fechasiniestro;
	}

	public String getImporte() {
		return this.importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getNumPoliza() {
		return this.numPoliza;
	}

	public void setNumPoliza(String numPoliza) {
		this.numPoliza = numPoliza;
	}

	public String getNumSiniestro() {
		return this.numSiniestro;
	}

	public void setNumSiniestro(String numSiniestro) {
		this.numSiniestro = numSiniestro;
	}

	public String getTipoAtencion() {
		return this.tipoAtencion;
	}

	public void setTipoAtencion(String tipoAtencion) {
		this.tipoAtencion = tipoAtencion;
	}

	public String getTipoHecho() {
		return this.tipoHecho;
	}

	public void setTipoHecho(String tipoHecho) {
		this.tipoHecho = tipoHecho;
	}

	public String getTipoSiniestro() {
		return this.tipoSiniestro;
	}

	public void setTipoSiniestro(String tipoSiniestro) {
		this.tipoSiniestro = tipoSiniestro;
	}

	public SapAlertasistemasEnvio getSapAlertasistemasEnvio() {
		return this.sapAlertasistemasEnvio;
	}

	public void setSapAlertasistemasEnvio(SapAlertasistemasEnvio sapAlertasistemasEnvio) {
		this.sapAlertasistemasEnvio = sapAlertasistemasEnvio;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		// TODO Auto-generated method stub
		return idsapAlertasCii;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
}