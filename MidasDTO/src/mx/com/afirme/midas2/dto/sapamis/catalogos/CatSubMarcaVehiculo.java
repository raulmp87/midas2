package mx.com.afirme.midas2.dto.sapamis.catalogos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="SAPAMISCATSUBMARCAVEH", schema = "MIDAS")
public class CatSubMarcaVehiculo implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="IDSAPAMISCATSUBMARCAVEH")
	private Long id;

	@Column(name="SAPAMISCATSUBMARCAVEH")
	private String descCatSubMarcaVehiculo;

	@ManyToOne
	@JoinColumn(name="IDSAPAMISCATMARCAVEH")
	private CatMarcaVehiculo catMarcaVehiculo;

	@ManyToOne
	@JoinColumn(name="IDSAPAMISCATTIPOTRANSPORTE")
	private CatTipoTransporte catTipoTransporte;
    
    @Column(name="ESTATUS")
    private long estatus;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescCatSubMarcaVehiculo() {
		return descCatSubMarcaVehiculo;
	}

	public void setDescCatSubMarcaVehiculo(String descCatSubMarcaVehiculo) {
		this.descCatSubMarcaVehiculo = descCatSubMarcaVehiculo;
	}

	public CatMarcaVehiculo getCatMarcaVehiculo() {
		return catMarcaVehiculo;
	}

	public void setCatMarcaVehiculo(CatMarcaVehiculo catMarcaVehiculo) {
		this.catMarcaVehiculo = catMarcaVehiculo;
	}
	
	public CatTipoTransporte getCatTipoTransporte() {
		return catTipoTransporte;
	}

	public void setCatTipoTransporte(CatTipoTransporte catTipoTransporte) {
		this.catTipoTransporte = catTipoTransporte;
	}
    
    public long getEstatus() {
        return estatus;
    }

    public void setEstatus(long estatus) {
        this.estatus = estatus;
    }

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}