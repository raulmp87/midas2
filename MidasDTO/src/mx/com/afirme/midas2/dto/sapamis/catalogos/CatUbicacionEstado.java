package mx.com.afirme.midas2.dto.sapamis.catalogos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="SAPAMISCATUBICESTADO", schema = "MIDAS")
public class CatUbicacionEstado implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="IDSAPAMISCATUBICESTADO")
	private Long id;

	@Column(name="CVESEYCOS")
	private String claveSeycos;

	@Column(name="SAPAMISCATUBICESTADO")
	private String descCatUbicacionEstado;

	@ManyToOne
	@JoinColumn(name="IDSAPAMISCATUBICPAIS")
	private CatUbicacionPais catUbicacionPais;
    
    @Column(name="ESTATUS")
    private long estatus;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getClaveSeycos() {
		return claveSeycos;
	}

	public void setClaveSeycos(String claveSeycos) {
		this.claveSeycos = claveSeycos;
	}

	public String getDescCatUbicacionEstado() {
		return descCatUbicacionEstado;
	}

	public void setDescCatUbicacionEstado(String descCatUbicacionEstado) {
		this.descCatUbicacionEstado = descCatUbicacionEstado;
	}

	public CatUbicacionPais getCatUbicacionPais() {
		return catUbicacionPais;
	}

	public void setCatUbicacionPais(CatUbicacionPais catUbicacionPais) {
		this.catUbicacionPais = catUbicacionPais;
	}
    
    public long getEstatus() {
        return estatus;
    }

    public void setEstatus(long estatus) {
        this.estatus = estatus;
    }

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}