package mx.com.afirme.midas2.dto.sapamis.sistemas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatCausaCancelacion;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatCausaSiniestro;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatDano;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatEstVehPago;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatMarcaVehiculo;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatSubMarcaVehiculo;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatTipoAfectado;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatTipoMonto;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatTipoPago;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatTipoServicio;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatTipoTransporte;

/******************************************************************************
 * 	Entidad para el manejo de los datos de Perdidas Totales dentro de los Procesos
 *  de envio del SAP-AMIS.
 * 
 * 		Table:		SAPAMISPTT
 * 		Schema:		MIDAS
 * 		Sequence:	SEQSAPAMISPTT
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 * 		
 ******************************************************************************/
@Entity
@Table(name = "SAPAMISPTT", schema = "MIDAS")
public class SapAmisPTT implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQSAPAMISPTT")
	@SequenceGenerator(name = "SEQSAPAMISPTT", schema = "MIDAS", sequenceName = "SEQSAPAMISPTT", allocationSize = 1)
	@Column(name = "IDSAPAMISPTT")
	private Long id;

	@Column(name = "NOSERIE")
	private String noSerie;

	@Column(name = "NOSINIESTRO")
	private String noSiniestro;

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHASINIESTRO")
	private Date fechaSiniestro;

	@Column(name = "PORCENTAJEPERDIDA")
	private long porcentajePerdida;

	@ManyToOne
	@JoinColumn(name = "IDSAPAMISCATTIPOAFECTADO")
	private CatTipoAfectado catTipoAfectado;
	
	@ManyToOne
	@JoinColumn(name = "IDSAPAMISCATCAUSASINIESTRO")
	private CatCausaSiniestro catCausaSiniestro;

	@Column(name = "NOMOTOR")
	private String noMotor;

	@ManyToOne
	@JoinColumn(name = "IDSAPAMISCATTIPOSERVICIO")
	private CatTipoServicio catTipoServicio;

	@ManyToOne
	@JoinColumn(name = "IDSAPAMISCATDANO")
	private CatDano catDano;

	@ManyToOne
	@JoinColumn(name = "IDSAPAMISCATTIPOMONTO")
	private CatTipoMonto catTipoMonto;

	@Column(name = "MONTOPAGADO")
	private long montoPagado;

	@ManyToOne
	@JoinColumn(name = "IDSAPAMISCATTIPOPAGO")
	private CatTipoPago catTipoPago;

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHAPAGO")
	private Date fechaPago;

	@ManyToOne
	@JoinColumn(name = "IDSAPAMISCATESTVEHPAGO")
	private CatEstVehPago catEstVehPago;

	@ManyToOne
	@JoinColumn(name = "IDSAPAMISCATTIPOTRANSPORTE")
	private CatTipoTransporte catTipoTransporte;

	@ManyToOne
	@JoinColumn(name = "IDSAPAMISCATMARCAVEH")
	private CatMarcaVehiculo catMarcaVehiculo;

	@ManyToOne
	@JoinColumn(name = "IDSAPAMISCATSUBMARCAVEH")
	private CatSubMarcaVehiculo catSubMarcaVehiculo;

	@Column(name = "ANOMODELO")
	private long anoModelo;

	@ManyToOne
	@JoinColumn(name = "IDSAPAMISCATCAUSACANCELACION")
	private CatCausaCancelacion catCausaCancelacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "FECHACANCELACION")
	private Date fechaCancelacion;

	@OneToOne
    @JoinColumn(updatable=false,insertable=false, name="IDSAPAMISBITACORAS", referencedColumnName="IDSAPAMISBITACORAS")
	private SapAmisBitacoras sapAmisBitacoras;

	public Date getFechaCancelacion() {
		return fechaCancelacion;
	}

	public void setFechaCancelacion(Date fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNoSerie() {
		return noSerie;
	}

	public void setNoSerie(String noSerie) {
		this.noSerie = noSerie;
	}

	public String getNoSiniestro() {
		return noSiniestro;
	}

	public void setNoSiniestro(String noSiniestro) {
		this.noSiniestro = noSiniestro;
	}

	public Date getFechaSiniestro() {
		return fechaSiniestro;
	}

	public void setFechaSiniestro(Date fechaSiniestro) {
		this.fechaSiniestro = fechaSiniestro;
	}

	public long getPorcentajePerdida() {
		return porcentajePerdida;
	}

	public void setPorcentajePerdida(long porcentajePerdida) {
		this.porcentajePerdida = porcentajePerdida;
	}

	public CatTipoAfectado getCatTipoAfectado() {
		return catTipoAfectado;
	}

	public void setCatTipoAfectado(CatTipoAfectado catTipoAfectado) {
		this.catTipoAfectado = catTipoAfectado;
	}

	public CatCausaSiniestro getCatCausaSiniestro() {
		return catCausaSiniestro;
	}

	public void setCatCausaSiniestro(CatCausaSiniestro catCausaSiniestro) {
		this.catCausaSiniestro = catCausaSiniestro;
	}

	public String getNoMotor() {
		return noMotor;
	}

	public void setNoMotor(String noMotor) {
		this.noMotor = noMotor;
	}

	public CatTipoServicio getCatTipoServicio() {
		return catTipoServicio;
	}

	public void setCatTipoServicio(CatTipoServicio catTipoServicio) {
		this.catTipoServicio = catTipoServicio;
	}

	public CatDano getCatDano() {
		return catDano;
	}

	public void setCatDano(CatDano catDano) {
		this.catDano = catDano;
	}

	public CatTipoMonto getCatTipoMonto() {
		return catTipoMonto;
	}

	public void setCatTipoMonto(CatTipoMonto catTipoMonto) {
		this.catTipoMonto = catTipoMonto;
	}

	public long getMontoPagado() {
		return montoPagado;
	}

	public void setMontoPagado(long montoPagado) {
		this.montoPagado = montoPagado;
	}

	public CatTipoPago getCatTipoPago() {
		return catTipoPago;
	}

	public void setCatTipoPago(CatTipoPago catTipoPago) {
		this.catTipoPago = catTipoPago;
	}

	public Date getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	public CatEstVehPago getCatEstVehPago() {
		return catEstVehPago;
	}

	public void setCatEstVehPago(CatEstVehPago catEstVehPago) {
		this.catEstVehPago = catEstVehPago;
	}

	public CatTipoTransporte getCatTipoTransporte() {
		return catTipoTransporte;
	}

	public void setCatTipoTransporte(CatTipoTransporte catTipoTransporte) {
		this.catTipoTransporte = catTipoTransporte;
	}

	public CatMarcaVehiculo getCatMarcaVehiculo() {
		return catMarcaVehiculo;
	}

	public void setCatMarcaVehiculo(CatMarcaVehiculo catMarcaVehiculo) {
		this.catMarcaVehiculo = catMarcaVehiculo;
	}

	public CatSubMarcaVehiculo getCatSubMarcaVehiculo() {
		return catSubMarcaVehiculo;
	}

	public void setCatSubMarcaVehiculo(CatSubMarcaVehiculo catSubMarcaVehiculo) {
		this.catSubMarcaVehiculo = catSubMarcaVehiculo;
	}

	public long getAnoModelo() {
		return anoModelo;
	}

	public void setAnoModelo(long anoModelo) {
		this.anoModelo = anoModelo;
	}

	public CatCausaCancelacion getCatCausaCancelacion() {
		return catCausaCancelacion;
	}

	public void setCatCausaCancelacion(CatCausaCancelacion catCausaCancelacion) {
		this.catCausaCancelacion = catCausaCancelacion;
	}

	public SapAmisBitacoras getSapAmisBitacoras() {
		return sapAmisBitacoras;
	}

	public void setSapAmisBitacoras(SapAmisBitacoras sapAmisBitacoras) {
		this.sapAmisBitacoras = sapAmisBitacoras;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}