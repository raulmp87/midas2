package mx.com.afirme.midas2.dto.sapamis.utiles;

import java.io.Serializable;
import java.util.Date;

public class ParametrosConsulta implements Serializable{
	private static final long serialVersionUID = 1L;

	private Date fechaOperacionIni;
	private Date fechaOperacionFin;
	private Date fechaRegistroIni;
	private Date fechaRegistroFin;
	private Date fechaEnvioIni;
	private Date fechaEnvioFin;
	private Long sistema;
	private Long operacion;
	private Long estatus;
	private String observaciones;
	private Long modulo;
	
	public Date getFechaOperacionIni() {
		return fechaOperacionIni;
	}
	public void setFechaOperacionIni(Date fechaOperacionIni) {
		this.fechaOperacionIni = fechaOperacionIni;
	}
	public Date getFechaOperacionFin() {
		return fechaOperacionFin;
	}
	public void setFechaOperacionFin(Date fechaOperacionFin) {
		this.fechaOperacionFin = fechaOperacionFin;
	}
	public Date getFechaRegistroIni() {
		return fechaRegistroIni;
	}
	public void setFechaRegistroIni(Date fechaRegistroIni) {
		this.fechaRegistroIni = fechaRegistroIni;
	}
	public Date getFechaRegistroFin() {
		return fechaRegistroFin;
	}
	public void setFechaRegistroFin(Date fechaRegistroFin) {
		this.fechaRegistroFin = fechaRegistroFin;
	}
	public Date getFechaEnvioIni() {
		return fechaEnvioIni;
	}
	public void setFechaEnvioIni(Date fechaEnvioIni) {
		this.fechaEnvioIni = fechaEnvioIni;
	}
	public Date getFechaEnvioFin() {
		return fechaEnvioFin;
	}
	public void setFechaEnvioFin(Date fechaEnvioFin) {
		this.fechaEnvioFin = fechaEnvioFin;
	}
	public Long getSistema() {
		return sistema;
	}
	public void setSistema(Long sistema) {
		this.sistema = sistema;
	}
	public Long getOperacion() {
		return operacion;
	}
	public void setOperacion(Long operacion) {
		this.operacion = operacion;
	}
	public Long getEstatus() {
		return estatus;
	}
	public void setEstatus(Long estatus) {
		this.estatus = estatus;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public Long getModulo() {
		return modulo;
	}
	public void setModulo(Long modulo) {
		this.modulo = modulo;
	}
}
