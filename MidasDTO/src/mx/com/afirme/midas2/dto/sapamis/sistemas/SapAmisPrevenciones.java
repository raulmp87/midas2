package mx.com.afirme.midas2.dto.sapamis.sistemas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatCausaPrevencion;

/******************************************************************************
 * 	Entidad para el manejo de los datos de Prevenciones dentro de los Procesos
 *  de envio del SAP-AMIS.
 * 
 * 		Table:		SAPAMISPREVENCIONES
 * 		Schema:		MIDAS
 * 		Sequence:	SEQSAPAMISPREVENCIONES
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 * 		
 ******************************************************************************/
@Entity
@Table(name="SAPAMISPREVENCIONES", schema = "MIDAS")
public class SapAmisPrevenciones implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQSAPAMISPREVENCIONES")
	@SequenceGenerator(name="SEQSAPAMISPREVENCIONES", schema = "MIDAS", sequenceName="SEQSAPAMISPREVENCIONES",allocationSize=1)
	@Column(name="IDSAPAMISPREVENCIONES")
	private Long id;

	@Column(name="NUMEROSERIE")
	private String numSerie;

	@ManyToOne
	@JoinColumn(name="CAUSAPREVENCION")
	private CatCausaPrevencion causaPrevencion;

	@Column(name="PLACA")
	private String placa;

	@Column(name="OBSERVACIONES")
	private String observaciones;

	@OneToOne
    @JoinColumn(updatable=false,insertable=false, name="IDSAPAMISBITACORAS", referencedColumnName="IDSAPAMISBITACORAS")
	private SapAmisBitacoras sapAmisBitacoras;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumSerie() {
		return numSerie;
	}

	public void setNumSerie(String numSerie) {
		this.numSerie = numSerie;
	}

	public CatCausaPrevencion getCausaPrevencion() {
		return causaPrevencion;
	}

	public void setCausaPrevencion(CatCausaPrevencion causaPrevencion) {
		this.causaPrevencion = causaPrevencion;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public SapAmisBitacoras getSapAmisBitacoras() {
		return sapAmisBitacoras;
	}

	public void setSapAmisBitacoras(SapAmisBitacoras sapAmisBitacoras) {
		this.sapAmisBitacoras = sapAmisBitacoras;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}