package mx.com.afirme.midas2.dto.sapamis.catalogos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="SAPAMISCATTIPOMONTO", schema = "MIDAS")
public class CatTipoMonto implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column (name="IDSAPAMISCATTIPOMONTO")
	private Long id;
	
	@Column(name="SAPAMISCATTIPOMONTO")
	private String descCatTipoMonto;
	
	@Column(name="ESTATUS")
	private long estatus;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescCatTipoMonto() {
		return descCatTipoMonto;
	}

	public void setDescCatTipoMonto(String descCatTipoMonto) {
		this.descCatTipoMonto = descCatTipoMonto;
	}

	public long getEstatus() {
		return estatus;
	}

	public void setEstatus(long estatus) {
		this.estatus = estatus;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}