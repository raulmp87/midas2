package mx.com.afirme.midas2.dto.sapamis.sistemas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatCanalVentas;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatSubMarcaVehiculo;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatTipoPersona;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatTipoServicio;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatUbicacionMunicipio;

/******************************************************************************
 * 	Entidad para el manejo de los datos de Emision dentro de los Procesos
 *  de envio del SAP-AMIS.
 * 
 * 		Table:		SAPAMISEMISION
 * 		Schema:		MIDAS
 * 		Sequence:	SEQSAPAMISEMISION
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 * 		
 ******************************************************************************/
@Entity
@Table(name="SAPAMISEMISION", schema = "MIDAS")
public class SapAmisEmision implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQSAPAMISEMISION")
	@SequenceGenerator(name="SEQSAPAMISEMISION", schema = "MIDAS", sequenceName="SEQSAPAMISEMISION",allocationSize=1)
	@Column(name="IDSAPAMISEMISION")
	private Long id;

	@Column(name="NOPOLIZA")
	private String noPoliza;

	@Column(name="INCISO")
	private String inciso;

    @Temporal(TemporalType.DATE)
	@Column(name="FECHAVIGENCIAINICIO")
	private Date fechaVigenciaInicio;

    @Temporal(TemporalType.DATE)
	@Column(name="FECHAVIGENCIAFIN")
	private Date fechaVigenciaFin;

	@Column(name="NOSERIE")
	private String noSerie;

	@ManyToOne
	@JoinColumn(name="VEHICULO")
	private CatSubMarcaVehiculo vehiculo;

	@Column(name="MODELO")
	private int modelo;

	@ManyToOne
	@JoinColumn(name="IDTIPOPERSONA")
	private CatTipoPersona tipoPersona;

	@ManyToOne
	@JoinColumn(name="IDTIPOSERVICIO")
	private CatTipoServicio catTipoServicio;

	@Column(name="CLIENTE1APPATERNO")
	private String cliente1ApPaterno;

	@Column(name="CLIENTE1APMATERNO")
	private String cliente1ApMaterno;

	@Column(name="CLIENTE1NOMBRE")
	private String cliente1Nombre;

	@Column(name="CLIENTE1RFC")
	private String cliente1RFC;

	@Column(name="CLIENTE1CURP")
	private String cliente1CURP;

	@Column(name="CLIENTE2APPATERNO")
	private String cliente2ApPaterno;

	@Column(name="CLIENTE2APMATERNO")
	private String cliente2ApMaterno;

	@Column(name="CLIENTE2NOMBRE")
	private String cliente2Nombre;

	@ManyToOne
	@JoinColumn(name="BENEFICIARIOIDTIPOPERSONA")
	private CatTipoPersona beneficiarioTipoPersona;

	@Column(name="BENEFICIARIOAPPATERNO")
	private String beneficiarioApPaterno;

	@Column(name="BENEFICIARIOAPMATERNO")
	private String beneficiarioApMaterno;

	@Column(name="BENEFICIARIONOMBRE")
	private String beneficiarioNombre;

	@Column(name="BENEFICIARIORFC")
	private String beneficiarioRFC;

	@Column(name="BENEFICIARIOCURP")
	private String beneficiarioCURP;

	@ManyToOne
	@JoinColumn(name="CANALVENTAS")
	private CatCanalVentas canalVentas;

	@Column(name="AGENTE")
	private String agente;

	@Column(name="OBLIGATORIO")
	private int obligatorio;

    @Temporal(TemporalType.DATE)
	@Column(name="FECHAEMISION")
	private Date fechaEmision;

	@ManyToOne
	@JoinColumn(name="UBICACIONASEGURADO")
	private CatUbicacionMunicipio ubicacionAsegurado;

	@ManyToOne
	@JoinColumn(name="UBICACIONEMISION")
	private CatUbicacionMunicipio ubicacionEmision;

	@OneToOne
    @JoinColumn(updatable=false,insertable=false, name="IDSAPAMISBITACORAS", referencedColumnName="IDSAPAMISBITACORAS")
	private SapAmisBitacoras sapAmisBitacoras;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNoPoliza() {
		return noPoliza;
	}

	public void setNoPoliza(String noPoliza) {
		this.noPoliza = noPoliza;
	}

	public String getInciso() {
		return inciso;
	}

	public void setInciso(String inciso) {
		this.inciso = inciso;
	}

	public Date getFechaVigenciaInicio() {
		return fechaVigenciaInicio;
	}

	public void setFechaVigenciaInicio(Date fechaVigenciaInicio) {
		this.fechaVigenciaInicio = fechaVigenciaInicio;
	}

	public Date getFechaVigenciaFin() {
		return fechaVigenciaFin;
	}

	public void setFechaVigenciaFin(Date fechaVigenciaFin) {
		this.fechaVigenciaFin = fechaVigenciaFin;
	}

	public String getNoSerie() {
		return noSerie;
	}

	public void setNoSerie(String noSerie) {
		this.noSerie = noSerie;
	}

	public CatSubMarcaVehiculo getVehiculo() {
		return vehiculo;
	}

	public void setVehiculo(CatSubMarcaVehiculo vehiculo) {
		this.vehiculo = vehiculo;
	}

	public int getModelo() {
		return modelo;
	}

	public void setModelo(int modelo) {
		this.modelo = modelo;
	}

	public CatTipoPersona getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(CatTipoPersona tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public CatTipoServicio getCatTipoServicio() {
		return catTipoServicio;
	}

	public void setCatTipoServicio(CatTipoServicio catTipoServicio) {
		this.catTipoServicio = catTipoServicio;
	}

	public String getCliente1ApPaterno() {
		return cliente1ApPaterno;
	}

	public void setCliente1ApPaterno(String cliente1ApPaterno) {
		this.cliente1ApPaterno = cliente1ApPaterno;
	}

	public String getCliente1ApMaterno() {
		return cliente1ApMaterno;
	}

	public void setCliente1ApMaterno(String cliente1ApMaterno) {
		this.cliente1ApMaterno = cliente1ApMaterno;
	}

	public String getCliente1Nombre() {
		return cliente1Nombre;
	}

	public void setCliente1Nombre(String cliente1Nombre) {
		this.cliente1Nombre = cliente1Nombre;
	}

	public String getCliente1RFC() {
		return cliente1RFC;
	}

	public void setCliente1RFC(String cliente1rfc) {
		cliente1RFC = cliente1rfc;
	}

	public String getCliente1CURP() {
		return cliente1CURP;
	}

	public void setCliente1CURP(String cliente1curp) {
		cliente1CURP = cliente1curp;
	}

	public String getCliente2ApPaterno() {
		return cliente2ApPaterno;
	}

	public void setCliente2ApPaterno(String cliente2ApPaterno) {
		this.cliente2ApPaterno = cliente2ApPaterno;
	}

	public String getCliente2ApMaterno() {
		return cliente2ApMaterno;
	}

	public void setCliente2ApMaterno(String cliente2ApMaterno) {
		this.cliente2ApMaterno = cliente2ApMaterno;
	}

	public String getCliente2Nombre() {
		return cliente2Nombre;
	}

	public void setCliente2Nombre(String cliente2Nombre) {
		this.cliente2Nombre = cliente2Nombre;
	}

	public CatTipoPersona getBeneficiarioTipoPersona() {
		return beneficiarioTipoPersona;
	}

	public void setBeneficiarioTipoPersona(CatTipoPersona beneficiarioTipoPersona) {
		this.beneficiarioTipoPersona = beneficiarioTipoPersona;
	}

	public String getBeneficiarioApPaterno() {
		return beneficiarioApPaterno;
	}

	public void setBeneficiarioApPaterno(String beneficiarioApPaterno) {
		this.beneficiarioApPaterno = beneficiarioApPaterno;
	}

	public String getBeneficiarioApMaterno() {
		return beneficiarioApMaterno;
	}

	public void setBeneficiarioApMaterno(String beneficiarioApMaterno) {
		this.beneficiarioApMaterno = beneficiarioApMaterno;
	}

	public String getBeneficiarioNombre() {
		return beneficiarioNombre;
	}

	public void setBeneficiarioNombre(String beneficiarioNombre) {
		this.beneficiarioNombre = beneficiarioNombre;
	}

	public String getBeneficiarioRFC() {
		return beneficiarioRFC;
	}

	public void setBeneficiarioRFC(String beneficiarioRFC) {
		this.beneficiarioRFC = beneficiarioRFC;
	}

	public String getBeneficiarioCURP() {
		return beneficiarioCURP;
	}

	public void setBeneficiarioCURP(String beneficiarioCURP) {
		this.beneficiarioCURP = beneficiarioCURP;
	}

	public CatCanalVentas getCanalVentas() {
		return canalVentas;
	}

	public void setCanalVentas(CatCanalVentas canalVentas) {
		this.canalVentas = canalVentas;
	}

	public String getAgente() {
		return agente;
	}

	public void setAgente(String agente) {
		this.agente = agente;
	}

	public int getObligatorio() {
		return obligatorio;
	}

	public void setObligatorio(int obligatorio) {
		this.obligatorio = obligatorio;
	}

	public Date getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public CatUbicacionMunicipio getUbicacionAsegurado() {
		return ubicacionAsegurado;
	}

	public void setUbicacionAsegurado(CatUbicacionMunicipio ubicacionAsegurado) {
		this.ubicacionAsegurado = ubicacionAsegurado;
	}

	public CatUbicacionMunicipio getUbicacionEmision() {
		return ubicacionEmision;
	}

	public void setUbicacionEmision(CatUbicacionMunicipio ubicacionEmision) {
		this.ubicacionEmision = ubicacionEmision;
	}

	public SapAmisBitacoras getSapAmisBitacoras() {
		return sapAmisBitacoras;
	}

	public void setSapAmisBitacoras(SapAmisBitacoras sapAmisBitacoras) {
		this.sapAmisBitacoras = sapAmisBitacoras;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}