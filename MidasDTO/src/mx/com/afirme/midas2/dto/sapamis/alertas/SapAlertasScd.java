package mx.com.afirme.midas2.dto.sapamis.alertas;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the SAP_ALERTAS_SCD database table.
 * 
 */
@Entity
@Table(name="SAP_ALERTAS_SCD", schema = "MIDAS")
public class SapAlertasScd implements Serializable, mx.com.afirme.midas2.dao.catalogos.Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQSAP_ALERTAS_SCD")
	@SequenceGenerator(name="SEQSAP_ALERTAS_SCD", schema = "MIDAS" , sequenceName="SEQSAP_ALERTAS_SCD",allocationSize=1)
	@Column(name="IDSAP_ALERTAS_SCD")
	private long idsapAlertasScd;

	private String anniomodelo;

	@Column(name="AVE_PREVIA")
	private String avePrevia;

	private String compania;

	private String deposito;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHA_EGRESO")
	private Date fechaEgreso;

    @Temporal( TemporalType.DATE)
	private Date fechaegreso;

    @Temporal( TemporalType.DATE)
	private Date fechaingreso;

	@Column(name="MARCA_DESC")
	private String marcaDesc;

	@Column(name="NUM_ECONOMICO")
	private String numEconomico;

	private String placas;

	private String siniestro;

	@Column(name="TIPO_DESC")
	private String tipoDesc;

	@Column(name="TTRANS_DESC")
	private String ttransDesc;

	//bi-directional many-to-one association to SapAlertasistemasEnvio
    @ManyToOne
	@JoinColumn(name="SAP_IDALERTASISTEMAS_ENVIO")
	private SapAlertasistemasEnvio sapAlertasistemasEnvio;

    public SapAlertasScd() {
    }

	public long getIdsapAlertasScd() {
		return this.idsapAlertasScd;
	}

	public void setIdsapAlertasScd(long idsapAlertasScd) {
		this.idsapAlertasScd = idsapAlertasScd;
	}

	public String getAnniomodelo() {
		return this.anniomodelo;
	}

	public void setAnniomodelo(String anniomodelo) {
		this.anniomodelo = anniomodelo;
	}

	public String getAvePrevia() {
		return this.avePrevia;
	}

	public void setAvePrevia(String avePrevia) {
		this.avePrevia = avePrevia;
	}

	public String getCompania() {
		return this.compania;
	}

	public void setCompania(String compania) {
		this.compania = compania;
	}

	public String getDeposito() {
		return this.deposito;
	}

	public void setDeposito(String deposito) {
		this.deposito = deposito;
	}

	public Date getFechaEgreso() {
		return this.fechaEgreso;
	}

	public void setFechaEgreso(Date fechaEgreso) {
		this.fechaEgreso = fechaEgreso;
	}

	public Date getFechaegreso() {
		return this.fechaegreso;
	}

	public void setFechaegreso(Date fechaegreso) {
		this.fechaegreso = fechaegreso;
	}

	public Date getFechaingreso() {
		return this.fechaingreso;
	}

	public void setFechaingreso(Date fechaingreso) {
		this.fechaingreso = fechaingreso;
	}

	public String getMarcaDesc() {
		return this.marcaDesc;
	}

	public void setMarcaDesc(String marcaDesc) {
		this.marcaDesc = marcaDesc;
	}

	public String getNumEconomico() {
		return this.numEconomico;
	}

	public void setNumEconomico(String numEconomico) {
		this.numEconomico = numEconomico;
	}

	public String getPlacas() {
		return this.placas;
	}

	public void setPlacas(String placas) {
		this.placas = placas;
	}

	public String getSiniestro() {
		return this.siniestro;
	}

	public void setSiniestro(String siniestro) {
		this.siniestro = siniestro;
	}

	public String getTipoDesc() {
		return this.tipoDesc;
	}

	public void setTipoDesc(String tipoDesc) {
		this.tipoDesc = tipoDesc;
	}

	public String getTtransDesc() {
		return this.ttransDesc;
	}

	public void setTtransDesc(String ttransDesc) {
		this.ttransDesc = ttransDesc;
	}

	public SapAlertasistemasEnvio getSapAlertasistemasEnvio() {
		return this.sapAlertasistemasEnvio;
	}

	public void setSapAlertasistemasEnvio(SapAlertasistemasEnvio sapAlertasistemasEnvio) {
		this.sapAlertasistemasEnvio = sapAlertasistemasEnvio;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		// TODO Auto-generated method stub
		return idsapAlertasScd;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
}