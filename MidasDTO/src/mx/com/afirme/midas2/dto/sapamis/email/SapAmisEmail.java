package mx.com.afirme.midas2.dto.sapamis.email;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dto.general.Email;

@Entity
@Table(name="SAPAMISEMAIL", schema = "MIDAS")
public class SapAmisEmail implements Serializable, mx.com.afirme.midas2.dao.catalogos.Entidad {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQSAPAMISEMAIL")
	@SequenceGenerator(name="SEQSAPAMISEMAIL", schema = "MIDAS", sequenceName="SEQSAPAMISEMAIL",allocationSize=1)
	@Column(name="IDSAPAMISEMAIL")
	private Long idSapAmisEmail;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="IDTCEMAIL")
	private Email email;
    
    @Column(name="ESTATUS")
    private long estatus;
    
	public Long getIdSapAmisEmail() {
		return idSapAmisEmail;
	}

	public void setIdSapAmisEmail(Long idSapAmisEmail) {
		this.idSapAmisEmail = idSapAmisEmail;
	}

	public Email getEmail() {
		return email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}

	public long getEstatus() {
		return estatus;
	}

	public void setEstatus(long estatus) {
		this.estatus = estatus;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object getKey() {
		return idSapAmisEmail==0?null:idSapAmisEmail;
	}

	@Override
	public String getValue() {
		return email.getValue();
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}

}
