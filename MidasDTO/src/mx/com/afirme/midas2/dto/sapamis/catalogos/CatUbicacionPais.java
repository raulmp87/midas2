package mx.com.afirme.midas2.dto.sapamis.catalogos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="SAPAMISCATUBICPAIS", schema = "MIDAS")
public class CatUbicacionPais implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="IDSAPAMISCATUBICPAIS")
	private Long id;

	@Column(name="CVESEYCOS")
	private String claveSeycos;

	@Column(name="SAPAMISCATUBICPAIS")
	private String descCatUbicacionPais;
    
    @Column(name="ESTATUS")
    private long estatus;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getClaveSeycos() {
		return claveSeycos;
	}

	public void setClaveSeycos(String claveSeycos) {
		this.claveSeycos = claveSeycos;
	}

	public String getDescCatUbicacionPais() {
		return descCatUbicacionPais;
	}

	public void setDescCatUbicacionPais(String descCatUbicacionPais) {
		this.descCatUbicacionPais = descCatUbicacionPais;
	}
    
    public long getEstatus() {
        return estatus;
    }

    public void setEstatus(long estatus) {
        this.estatus = estatus;
    }

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}