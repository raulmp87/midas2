package mx.com.afirme.midas2.dto.sapamis.alertas;


import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the SAP_ALERTAS_VALUACION database table.
 * 
 */
@Entity
@Table(name="SAP_ALERTAS_VALUACION", schema = "MIDAS")
public class SapAlertasValuacion implements Serializable, mx.com.afirme.midas2.dao.catalogos.Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQSAP_ALERTAS_VALUACIONID")
	@SequenceGenerator(name="SEQSAP_ALERTAS_VALUACIONID", schema = "MIDAS",  sequenceName="SEQSAP_ALERTAS_VALUACIONID",allocationSize=1)
	@Column(name="IDSAP_ALERTAS_VALUACION")
	private long idsapAlertasValuacion;

	private String cia;

    @Temporal( TemporalType.DATE)
	private Date fecha;

	private String monto;

	private String noserie;

	private String numsiniestro;

	private String numvaluacion;

	//bi-directional many-to-one association to SapAlertasistemasEnvio
    @ManyToOne
	@JoinColumn(name="SAP_IDALERTASISTEMAS_ENVIO")
	private SapAlertasistemasEnvio sapAlertasistemasEnvio;

    public SapAlertasValuacion() {
    }

	public long getIdsapAlertasValuacion() {
		return this.idsapAlertasValuacion;
	}

	public void setIdsapAlertasValuacion(long idsapAlertasValuacion) {
		this.idsapAlertasValuacion = idsapAlertasValuacion;
	}

	public String getCia() {
		return this.cia;
	}

	public void setCia(String cia) {
		this.cia = cia;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getMonto() {
		return this.monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getNoserie() {
		return this.noserie;
	}

	public void setNoserie(String noserie) {
		this.noserie = noserie;
	}

	public String getNumsiniestro() {
		return this.numsiniestro;
	}

	public void setNumsiniestro(String numsiniestro) {
		this.numsiniestro = numsiniestro;
	}

	public String getNumvaluacion() {
		return this.numvaluacion;
	}

	public void setNumvaluacion(String numvaluacion) {
		this.numvaluacion = numvaluacion;
	}

	public SapAlertasistemasEnvio getSapAlertasistemasEnvio() {
		return this.sapAlertasistemasEnvio;
	}

	public void setSapAlertasistemasEnvio(SapAlertasistemasEnvio sapAlertasistemasEnvio) {
		this.sapAlertasistemasEnvio = sapAlertasistemasEnvio;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		
		return idsapAlertasValuacion;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
}