package mx.com.afirme.midas2.dto.sapamis.alertas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;





/**
 * The persistent class for the SAP_ALERTAS_SIPAC database table.
 * 
 */
@Entity
@Table(name="SAP_ALERTAS_SIPAC", schema = "MIDAS")
public class SapAlertasSipac implements Serializable, mx.com.afirme.midas2.dao.catalogos.Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQSAP_ALERTAS_SIPACID")
	@SequenceGenerator(name="SEQSAP_ALERTAS_SIPACID", schema = "MIDAS",  sequenceName="SEQSAP_ALERTAS_SIPACID",allocationSize=1)
	@Column(name="IDSAP_ALERTAS_SIPAC")
	private long idsapAlertasSipac;

	@Column(name="CIA_ACREEDORA")
	private String ciaAcreedora;

	@Column(name="CIA_DUDORA")
	private String ciaDudora;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHA_ESTATUS")
	private Date fechaEstatus;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHA_EXP")
	private Date fechaExp;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHA_SINIESTRO")
	private Date fechaSiniestro;

	@Column(name="MARCA_DESC")
	private String marcaDesc;

	private String modelo;

	private String placa;

	private String responsabilidad;

	@Column(name="SINIESTRO_DEUDOR")
	private String siniestroDeudor;

	@Column(name="TIPO_DESC")
	private String tipoDesc;

	@Column(name="TIPO_ORDEN")
	private String tipoOrden;

	@Column(name="TTRANS_DESC")
	private String ttransDesc;

	//bi-directional many-to-one association to SapAlertasistemasEnvio
    @ManyToOne
	@JoinColumn(name="SAP_IDALERTASISTEMAS_ENVIO")
	private SapAlertasistemasEnvio sapAlertasistemasEnvio;

    public SapAlertasSipac() {
    }

	public long getIdsapAlertasSipac() {
		return this.idsapAlertasSipac;
	}

	public void setIdsapAlertasSipac(long idsapAlertasSipac) {
		this.idsapAlertasSipac = idsapAlertasSipac;
	}

	public String getCiaAcreedora() {
		return this.ciaAcreedora;
	}

	public void setCiaAcreedora(String ciaAcreedora) {
		this.ciaAcreedora = ciaAcreedora;
	}

	public String getCiaDudora() {
		return this.ciaDudora;
	}

	public void setCiaDudora(String ciaDudora) {
		this.ciaDudora = ciaDudora;
	}

	public Date getFechaEstatus() {
		return this.fechaEstatus;
	}

	public void setFechaEstatus(Date fechaEstatus) {
		this.fechaEstatus = fechaEstatus;
	}

	public Date getFechaExp() {
		return this.fechaExp;
	}

	public void setFechaExp(Date fechaExp) {
		this.fechaExp = fechaExp;
	}

	public Date getFechaSiniestro() {
		return this.fechaSiniestro;
	}

	public void setFechaSiniestro(Date fechaSiniestro) {
		this.fechaSiniestro = fechaSiniestro;
	}

	public String getMarcaDesc() {
		return this.marcaDesc;
	}

	public void setMarcaDesc(String marcaDesc) {
		this.marcaDesc = marcaDesc;
	}

	public String getModelo() {
		return this.modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getPlaca() {
		return this.placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getResponsabilidad() {
		return this.responsabilidad;
	}

	public void setResponsabilidad(String responsabilidad) {
		this.responsabilidad = responsabilidad;
	}

	public String getSiniestroDeudor() {
		return this.siniestroDeudor;
	}

	public void setSiniestroDeudor(String siniestroDeudor) {
		this.siniestroDeudor = siniestroDeudor;
	}

	public String getTipoDesc() {
		return this.tipoDesc;
	}

	public void setTipoDesc(String tipoDesc) {
		this.tipoDesc = tipoDesc;
	}

	public String getTipoOrden() {
		return this.tipoOrden;
	}

	public void setTipoOrden(String tipoOrden) {
		this.tipoOrden = tipoOrden;
	}

	public String getTtransDesc() {
		return this.ttransDesc;
	}

	public void setTtransDesc(String ttransDesc) {
		this.ttransDesc = ttransDesc;
	}

	public SapAlertasistemasEnvio getSapAlertasistemasEnvio() {
		return this.sapAlertasistemasEnvio;
	}

	public void setSapAlertasistemasEnvio(SapAlertasistemasEnvio sapAlertasistemasEnvio) {
		this.sapAlertasistemasEnvio = sapAlertasistemasEnvio;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		// TODO Auto-generated method stub
		return idsapAlertasSipac;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
}