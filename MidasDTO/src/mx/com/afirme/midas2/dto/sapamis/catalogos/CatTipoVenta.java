package mx.com.afirme.midas2.dto.sapamis.catalogos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="SAPAMISCATTIPOVENTA",schema="MIDAS")
public class CatTipoVenta implements Serializable, Entidad {	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="IDSAPAMISCATTIPOVENTA")
	private Long id;
	
	@Column(name="SAPAMISCATTIPOVENTA")
	private String descCatTipoVenta;
	
	@Column(name="ESTATUS")
	private long estatus;
		
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescCatTipoVenta() {
		return descCatTipoVenta;
	}

	public void setDescCatTipoVenta(String descCatTipoVenta) {
		this.descCatTipoVenta = descCatTipoVenta;
	}

	public long getEstatus() {
		return estatus;
	}

	public void setEstatus(long estatus) {
		this.estatus = estatus;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}