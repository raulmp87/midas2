package mx.com.afirme.midas2.dto.sapamis.catalogos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="SAPAMISCATBENEFICIORELACION", schema = "MIDAS")
public class CatBeneficioRelacion implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="IDSAPAMISCATBENEFICIORELACION")
	private Long id;

	@ManyToOne
	@JoinColumn(name="IDSAPAMISCATTIPOBENEFICIO")
	private CatTipoBeneficio catTipoBeneficio;

	@ManyToOne
	@JoinColumn(name="IDSAPAMISCATCAUSABENEFICIO")
	private CatCausaBeneficio catCausaBeneficio;
    
    @Column(name="ESTATUS")
    private long estatus;
	
	/**
	 * GETTERS AND SETTERS
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CatTipoBeneficio getCatTipoBeneficio() {
		return catTipoBeneficio;
	}

	public void setCatTipoBeneficio(CatTipoBeneficio catTipoBeneficio) {
		this.catTipoBeneficio = catTipoBeneficio;
	}

	public CatCausaBeneficio getCatCausaBeneficio() {
		return catCausaBeneficio;
	}

	public void setCatCausaBeneficio(CatCausaBeneficio catCausaBeneficio) {
		this.catCausaBeneficio = catCausaBeneficio;
	}
    
    public long getEstatus() {
        return estatus;
    }

    public void setEstatus(long estatus) {
        this.estatus = estatus;
    }

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}
