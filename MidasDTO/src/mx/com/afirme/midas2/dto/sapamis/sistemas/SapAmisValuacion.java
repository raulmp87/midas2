package mx.com.afirme.midas2.dto.sapamis.sistemas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dto.repuve.sistemas.RepuveNivNci;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatTipoAfectado;

/******************************************************************************
 * 	Entidad para el manejo de los datos de Siniestros dentro de los Procesos
 *  de envio del SAP-AMIS.
 * 
 * 	@author Eduardo.Chavez
 * 
 * 		Table:		SAPAMISVALUACION
 * 		Schema:		MIDAS
 * 		Sequence:	SAPAMISVALUACION_SEQ
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 * 		
 ******************************************************************************/
@Entity
@Table(name = "SAPAMISVALUACION", schema = "MIDAS")
public class SapAmisValuacion implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SAPAMISVALUACION_SEQ")
	@SequenceGenerator(name = "SAPAMISVALUACION_SEQ", schema = "MIDAS", sequenceName = "SAPAMISVALUACION_SEQ", allocationSize = 1)
	@Column(name = "ID")
	private Long id;

	@OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(updatable=false,insertable=false, name="SAPAMISBITACORAS_ID", referencedColumnName="IDSAPAMISBITACORAS")
	private SapAmisBitacoras sapAmisBitacoras;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="REPNIVNCI_ID")
	private RepuveNivNci repuveNivNci;

	@Column(name = "NUMEROSINIESTRO")
	private String numeroSiniestro;

	@Column(name = "NOVALUACION")
	private String numeroValuacion;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHAVALUACION")
	private Date fechaValuacion;

	@Column(name = "MONTOVALUACION")
	private Double montoValuacion;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "SAPAMISCATTIPOAFECTADO_ID")
	private CatTipoAfectado catTipoAfectado;
	
	/** Getters and Setters **/

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SapAmisBitacoras getSapAmisBitacoras() {
		return sapAmisBitacoras;
	}

	public void setSapAmisBitacoras(SapAmisBitacoras sapAmisBitacoras) {
		this.sapAmisBitacoras = sapAmisBitacoras;
	}

	public RepuveNivNci getRepuveNivNci() {
		return repuveNivNci;
	}

	public void setRepuveNivNci(RepuveNivNci repuveNivNci) {
		this.repuveNivNci = repuveNivNci;
	}

	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}

	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}

	public String getNumeroValuacion() {
		return numeroValuacion;
	}

	public void setNumeroValuacion(String numeroValuacion) {
		this.numeroValuacion = numeroValuacion;
	}

	public Date getFechaValuacion() {
		return fechaValuacion;
	}

	public void setFechaValuacion(Date fechaValuacion) {
		this.fechaValuacion = fechaValuacion;
	}

	public Double getMontoValuacion() {
		return montoValuacion;
	}

	public void setMontoValuacion(Double montoValuacion) {
		this.montoValuacion = montoValuacion;
	}

	public CatTipoAfectado getCatTipoAfectado() {
		return catTipoAfectado;
	}

	public void setCatTipoAfectado(CatTipoAfectado catTipoAfectado) {
		this.catTipoAfectado = catTipoAfectado;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}