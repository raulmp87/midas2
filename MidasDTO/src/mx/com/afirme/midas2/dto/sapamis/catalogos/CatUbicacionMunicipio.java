package mx.com.afirme.midas2.dto.sapamis.catalogos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="SAPAMISCATUBICMUNICIPIO", schema = "MIDAS")
public class CatUbicacionMunicipio implements Serializable, Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="IDSAPAMISCATUBICMUNICIPIO")
	private Long id;

	@Column(name="CVESEYCOS")
	private String claveSeycos;

	@Column(name="CVEINEGIMUNICIPIO")
	private long cveInegiMunicipio;

	@Column(name="SAPAMISCATUBICMUNICIPIO")
	private String descCatUbicacionMunicipio;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="IDSAPAMISCATUBICESTADO")
	private CatUbicacionEstado catUbicacionEstado;
    
    @Column(name="ESTATUS")
    private long estatus;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getClaveSeycos() {
		return claveSeycos;
	}

	public long getCveInegiMunicipio() {
		return cveInegiMunicipio;
	}

	public void setCveInegiMunicipio(long cveInegiMunicipio) {
		this.cveInegiMunicipio = cveInegiMunicipio;
	}

	public void setClaveSeycos(String claveSeycos) {
		this.claveSeycos = claveSeycos;
	}

	public String getDescCatUbicacionMunicipio() {
		return descCatUbicacionMunicipio;
	}

	public void setDescCatUbicacionMunicipio(String descCatUbicacionMunicipio) {
		this.descCatUbicacionMunicipio = descCatUbicacionMunicipio;
	}

	public CatUbicacionEstado getCatUbicacionEstado() {
		return catUbicacionEstado;
	}

	public void setCatUbicacionEstado(CatUbicacionEstado catUbicacionEstado) {
		this.catUbicacionEstado = catUbicacionEstado;
	}
    
    public long getEstatus() {
        return estatus;
    }

    public void setEstatus(long estatus) {
        this.estatus = estatus;
    }

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}