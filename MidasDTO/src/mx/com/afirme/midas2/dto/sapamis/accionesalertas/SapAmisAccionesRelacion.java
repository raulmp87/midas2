package mx.com.afirme.midas2.dto.sapamis.accionesalertas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.afirme.midas2.dto.sapamis.catalogos.CatAlertasSapAmis;

/******************************************************************************
 * Entidad para el manejo del log de las Acciones sobre las Alertas del 
 * SAP-AMIS
 * 
 * @author Eduardo.Chavez
 * 
 * 		Table:		SAP_AMIS_ACIONES_RELACION
 * 		Schema:		MIDAS
 * 		Sequence:	SEQSAP_AMIS_ACIONES_RELACION
 * 		
 ******************************************************************************/
@Entity
@Table(name="SAPAMISACCIONESRELACION", schema = "MIDAS")
public class SapAmisAccionesRelacion implements Serializable, mx.com.afirme.midas2.dao.catalogos.Entidad {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQSAPAMISACCIONESRELACION")
	@SequenceGenerator(name="SEQSAPAMISACCIONESRELACION", schema = "MIDAS", sequenceName="SEQSAPAMISACCIONESRELACION",allocationSize=1)
	@Column(name="IDSAPAMISACCIONESRELACION")
	private long idSapAmisAccionesRelacion;

	@Column(name="IDCOTIZACION")
	private long idCotizacion;

	@Column(name="NUMEROSERIE")
	private String vin;
	
	@ManyToOne
	@JoinColumn(name="IDSAPAMISALERTAS")
	private CatAlertasSapAmis catAlertasSapAmis;
	
	@Column(name="TIPO")
	private int tipo;
	
	/*****************************
	 * Inician Getters y Setters *
	 *****************************/

	public long getIdSapAmisAccionesRelacion() {
		return idSapAmisAccionesRelacion;
	}

	public void setIdSapAmisAccionesRelacion(long idSapAmisAccionesRelacion) {
		this.idSapAmisAccionesRelacion = idSapAmisAccionesRelacion;
	}

	public long getIdCotizacion() {
		return idCotizacion;
	}

	public void setIdCotizacion(long idCotizacion) {
		this.idCotizacion = idCotizacion;
	}

	public CatAlertasSapAmis getCatAlertasSapAmis() {
		return catAlertasSapAmis;
	}

	public void setCatAlertasSapAmis(CatAlertasSapAmis catAlertasSapAmis) {
		this.catAlertasSapAmis = catAlertasSapAmis;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}
	
	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	/************************************************
	 * Mètodos implementados de la interfaz Entidad *
	 * Para el uso generico de los DAO y Servicios  *
	 ************************************************/
	
	@SuppressWarnings("unchecked")
	@Override
	public Object getKey() {
		return getIdSapAmisAccionesRelacion()==0?null:getIdSapAmisAccionesRelacion();
	}

	@Override
	public String getValue() {
		return "{"
					+ "idCotizacion: " + getIdCotizacion() + " | " 
				    + "vin: " + getVin() + " | " 
				    + "catAlertasSapAmis: " + getCatAlertasSapAmis().getValue() + " | " 
					+ "tipo: " + getTipo() + 
				" }";
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
}
