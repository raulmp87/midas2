package mx.com.afirme.midas2.dto.sapamis.alertas;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the SAP_ALERTAS_OCRA database table.
 * 
 */
@Entity
@Table(name="SAP_ALERTAS_OCRA" , schema = "MIDAS")
public class SapAlertasOcra implements Serializable, mx.com.afirme.midas2.dao.catalogos.Entidad {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQSAP_ALERTAS_OCRA")
	@SequenceGenerator(name="SEQSAP_ALERTAS_OCRA", schema = "MIDAS" , sequenceName="SEQSAP_ALERTAS_OCRA",allocationSize=1)
	@Column(name="IDSAP_ALERTAS_OCRA")
	private long idsapAlertasOcra;

	private String cia;

	private String estatus;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHA_LOCALIZACION")
	private Date fechaLocalizacion;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHA_RECUPERACION")
	private Date fechaRecuperacion;

    @Temporal( TemporalType.DATE)
	@Column(name="FECHA_ROBO")
	private Date fechaRobo;

	@Column(name="MARCA_DESC")
	private String marcaDesc;

	private String modelo;

	@Column(name="NUM_SINIESTRO")
	private String numSiniestro;

	private String placa;

	private String remarcado;

	@Column(name="TIPO_DESC")
	private String tipoDesc;

	@Column(name="TIPO_ROBO")
	private String tipoRobo;

	@Column(name="TTRANS_DESC")
	private String ttransDesc;

	//bi-directional many-to-one association to SapAlertasistemasEnvio
    @ManyToOne
	@JoinColumn(name="SAP_IDALERTASISTEMAS_ENVIO")
	private SapAlertasistemasEnvio sapAlertasistemasEnvio;

    public SapAlertasOcra() {
    }

	public long getIdsapAlertasOcra() {
		return this.idsapAlertasOcra;
	}

	public void setIdsapAlertasOcra(long idsapAlertasOcra) {
		this.idsapAlertasOcra = idsapAlertasOcra;
	}

	public String getCia() {
		return this.cia;
	}

	public void setCia(String cia) {
		this.cia = cia;
	}

	public String getEstatus() {
		return this.estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public Date getFechaLocalizacion() {
		return this.fechaLocalizacion;
	}

	public void setFechaLocalizacion(Date fechaLocalizacion) {
		this.fechaLocalizacion = fechaLocalizacion;
	}

	public Date getFechaRecuperacion() {
		return this.fechaRecuperacion;
	}

	public void setFechaRecuperacion(Date fechaRecuperacion) {
		this.fechaRecuperacion = fechaRecuperacion;
	}

	public Date getFechaRobo() {
		return this.fechaRobo;
	}

	public void setFechaRobo(Date fechaRobo) {
		this.fechaRobo = fechaRobo;
	}

	public String getMarcaDesc() {
		return this.marcaDesc;
	}

	public void setMarcaDesc(String marcaDesc) {
		this.marcaDesc = marcaDesc;
	}

	public String getModelo() {
		return this.modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getNumSiniestro() {
		return this.numSiniestro;
	}

	public void setNumSiniestro(String numSiniestro) {
		this.numSiniestro = numSiniestro;
	}

	public String getPlaca() {
		return this.placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getRemarcado() {
		return this.remarcado;
	}

	public void setRemarcado(String remarcado) {
		this.remarcado = remarcado;
	}

	public String getTipoDesc() {
		return this.tipoDesc;
	}

	public void setTipoDesc(String tipoDesc) {
		this.tipoDesc = tipoDesc;
	}

	public String getTipoRobo() {
		return this.tipoRobo;
	}

	public void setTipoRobo(String tipoRobo) {
		this.tipoRobo = tipoRobo;
	}

	public String getTtransDesc() {
		return this.ttransDesc;
	}

	public void setTtransDesc(String ttransDesc) {
		this.ttransDesc = ttransDesc;
	}

	public SapAlertasistemasEnvio getSapAlertasistemasEnvio() {
		return this.sapAlertasistemasEnvio;
	}

	public void setSapAlertasistemasEnvio(SapAlertasistemasEnvio sapAlertasistemasEnvio) {
		this.sapAlertasistemasEnvio = sapAlertasistemasEnvio;
	}

	@SuppressWarnings("unchecked")	
	@Override
	public Long getKey() {
		// TODO Auto-generated method stub
		return idsapAlertasOcra;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
}