package mx.com.afirme.midas2.dto.sapamis.catalogos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

/******************************************************************************
 * 	Entidad para el manejo del catalogo Estatus de Bitacoras: Indica el estatus
 *	en el que se encuentra un registro dentro del proceso de Envios SAP-AMIS.
 * 
 * 	@author Eduardo.Chavez
 * 
 * 		Table:		SAPAMISCATESTATUSBITACORA
 * 		Schema:		MIDAS
 * 		Sequence:	N.A.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 * 		
 ******************************************************************************/
@Entity
@Table(name="SAPAMISCATESTATUSBITACORA", schema = "MIDAS")
public class CatEstatusBitacora implements Serializable, Entidad{
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "IDSAPAMISCATESTATUSBITACORA")
	private Long id;
    
    @Column(name="SAPAMISCATESTATUSBITACORA")
    private String descCatEstatusBitacora;
    
    @Column(name="ESTATUSREGISTRO")//Estatus del Registro del catalogo (Activo / Inactivo)
    private long estatusRegistro;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescCatEstatusBitacora() {
		return descCatEstatusBitacora;
	}

	public void setDescCatEstatusBitacora(String descCatEstatusBitacora) {
		this.descCatEstatusBitacora = descCatEstatusBitacora;
	}

	public long getEstatusRegistro() {
		return estatusRegistro;
	}

	public void setEstatusRegistro(long estatusRegistro) {
		this.estatusRegistro = estatusRegistro;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		return id;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		return null;
	}
}
