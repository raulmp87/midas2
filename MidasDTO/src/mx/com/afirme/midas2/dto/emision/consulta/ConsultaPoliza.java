package mx.com.afirme.midas2.dto.emision.consulta;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="VW_CONSULTA_POLIZA",schema="MIDAS")
public class ConsultaPoliza extends Consulta implements Serializable, Entidad {
	
	private static final long serialVersionUID = -8130708797799818481L;

	@Id
	@Column(name="ID")
	private Long id;
	
	@Column(name="ID_AGENTE")
	private String agenteId;
	
	@Column(name="ID_CLIENTE")
	private Long clienteId;
	
	@Column(name="NUM_POLIZA")
	private String numeroPoliza;
	
	@Column(name="NUM_POLIZA_SEYCOS")
	private String claveSeycos;
	
	@Column(name="FECHA_CREACION")
	private String fechaCreacion;
	
	@Column(name="ESTATUS")
	private String estatus;
	
	@Column(name="FECHA_INICIO_VIGENCIA")
	private String fechaInicioVigencia;
	
	@Column(name="FECHA_FIN_VIGENCIA")
	private String fechaFinVigencia;
	
	@Column(name="TIPO_FLOTILLA")
	private String tipo; //Individual o Flotilla
	
	@Column(name="NUM_COTIZACION")
	private String numeroCotizacion;

	@Column(name="NOMBRECONTRATANTE")
	private String contratante;
	
	@Column(name="FOLIO_REEXPEDIBLE")
	private String folio;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAgenteId() {
		return agenteId;
	}

	public void setAgenteId(String agenteId) {
		this.agenteId = agenteId;
	}

	public Long getClienteId() {
		return clienteId;
	}

	public void setClienteId(Long clienteId) {
		this.clienteId = clienteId;
	}

	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	public String getClaveSeycos() {
		return claveSeycos;
	}

	public void setClaveSeycos(String claveSeycos) {
		this.claveSeycos = claveSeycos;
	}

	public String getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(String fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	public String getFechaFinVigencia() {
		return fechaFinVigencia;
	}

	public void setFechaFinVigencia(String fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getNumeroCotizacion() {
		return numeroCotizacion;
	}

	public void setNumeroCotizacion(String numeroCotizacion) {
		this.numeroCotizacion = numeroCotizacion;
	}

	public String getContratante() {
		return contratante;
	}

	public void setContratante(String contratante) {
		this.contratante = contratante;
	}

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Nivel getNivel() {
		return Nivel.DOS;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}
	
}
