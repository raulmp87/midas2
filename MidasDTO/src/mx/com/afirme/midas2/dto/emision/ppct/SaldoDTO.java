package mx.com.afirme.midas2.dto.emision.ppct;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import mx.com.afirme.midas2.annotation.Exportable;
import mx.com.afirme.midas2.annotation.MultipleDocumentExportable;

public class SaldoDTO implements Serializable {

	
	private static final long serialVersionUID = -890113661812066828L;
	
	public static final String DETALLE = "DETALLE";
	public static final String RESUMEN = "RESUMEN";
	
	private BigDecimal periodo;
	private String proveedor;
	private String poliza;
	private Integer endoso;
	private String inciso;
	private String cobertura;
	private String recibo;
	private Date fechaInicioVigencia;
	private Date fechaFinVigencia;
	private Date fechaCorte;
	private BigDecimal saldo;
	private Integer moneda;
	private BigDecimal referenciaContable;
	private String claveNegocio;
	
	@MultipleDocumentExportable (value = {
		@Exportable(columnName = "PERIODO", columnOrder = 0, document= SaldoDTO.DETALLE),
		@Exportable(columnName = "PERIODO", columnOrder = 0, document= SaldoDTO.RESUMEN)
	})
	public BigDecimal getPeriodo() {
		return periodo;
	}
	public void setPeriodo(BigDecimal periodo) {
		this.periodo = periodo;
	}
	
	@MultipleDocumentExportable (value = {
			@Exportable(columnName = "PROVEEDOR", columnOrder = 1, document= SaldoDTO.DETALLE),
			@Exportable(columnName = "PROVEEDOR", columnOrder = 1, document= SaldoDTO.RESUMEN)
		})
	public String getProveedor() {
		return proveedor;
	}
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}
	
	@Exportable(columnName = "POLIZA", columnOrder = 2, document= SaldoDTO.DETALLE)
	public String getPoliza() {
		return poliza;
	}
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	
	@Exportable(columnName = "ENDOSO", columnOrder = 3, document= SaldoDTO.DETALLE)
	public Integer getEndoso() {
		return endoso;
	}
	public void setEndoso(Integer endoso) {
		this.endoso = endoso;
	}
	
	@Exportable(columnName = "INCISO", columnOrder = 4, document= SaldoDTO.DETALLE)
	public String getInciso() {
		return inciso;
	}
	public void setInciso(String inciso) {
		this.inciso = inciso;
	}
	
	@MultipleDocumentExportable (value = {
			@Exportable(columnName = "COBERTURA", columnOrder = 5, document= SaldoDTO.DETALLE),
			@Exportable(columnName = "COBERTURA", columnOrder = 2, document= SaldoDTO.RESUMEN)
		})
	public String getCobertura() {
		return cobertura;
	}
	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}
	
	@Exportable(columnName = "RECIBO", columnOrder = 6, document= SaldoDTO.DETALLE)
	public String getRecibo() {
		return recibo;
	}
	public void setRecibo(String recibo) {
		this.recibo = recibo;
	}
	
	@Exportable(columnName = "FECHA INICIO VIGENCIA", columnOrder = 7, document= SaldoDTO.DETALLE)
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}
	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	
	@Exportable(columnName = "FECHA FIN VIGENCIA", columnOrder = 8, document= SaldoDTO.DETALLE)
	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}
	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}
	
	@Exportable(columnName = "FECHA DE CORTE", columnOrder = 9, document= SaldoDTO.DETALLE)
	public Date getFechaCorte() {
		return fechaCorte;
	}
	public void setFechaCorte(Date fechaCorte) {
		this.fechaCorte = fechaCorte;
	}
	
	@MultipleDocumentExportable (value = {
			@Exportable(columnName = "SALDO", columnOrder = 10, document= SaldoDTO.DETALLE),
			@Exportable(columnName = "SALDO", columnOrder = 3, document= SaldoDTO.RESUMEN)
		})
	public BigDecimal getSaldo() {
		return saldo;
	}
	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}
	
	@MultipleDocumentExportable (value = {
			@Exportable(columnName = "MONEDA", columnOrder = 11, document= SaldoDTO.DETALLE, fixedValues="484=NACIONAL,840=USD"),
			@Exportable(columnName = "MONEDA", columnOrder = 4, document= SaldoDTO.RESUMEN, fixedValues="484=NACIONAL,840=USD")
		})
	public Integer getMoneda() {
		return moneda;
	}
	public void setMoneda(Integer moneda) {
		this.moneda = moneda;
	}
	
	@Exportable(columnName = "REFERENCIA CONTABLE", columnOrder = 12, document= SaldoDTO.DETALLE)
	public BigDecimal getReferenciaContable() {
		return referenciaContable;
	}
	public void setReferenciaContable(BigDecimal referenciaContable) {
		this.referenciaContable = referenciaContable;
	}
	
	public String getClaveNegocio() {
		return claveNegocio;
	}
	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}
	
	
	
}
