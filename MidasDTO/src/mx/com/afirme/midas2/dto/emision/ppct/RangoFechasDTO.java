package mx.com.afirme.midas2.dto.emision.ppct;

import java.io.Serializable;
import java.util.Date;

public class RangoFechasDTO implements Serializable {

	private static final long serialVersionUID = -473580027622814762L;

	private Date fechaInicio;
	
	private Date fechaFin;

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	
	
	
}
