package mx.com.afirme.midas2.dto.emision.consulta;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="VW_CONSULTA_AGENTE",schema="MIDAS")
public class ConsultaAgente extends Consulta implements Serializable, Entidad {

	private static final long serialVersionUID = 8077508338933594212L;

	@Id
	@Column(name="ID")
	private String id;
	
	@Column(name="NOMBRE")
	private String nombre;
	
	@Column(name="OFICINA")
	private String oficina;
	
	@Column(name="GERENCIA")
	private String gerencia;
	
	@Column(name="SUPERVISION")
	private String supervision;
	
	@Column(name="ESTATUS")
	private String estatus;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getOficina() {
		return oficina;
	}

	public void setOficina(String oficina) {
		this.oficina = oficina;
	}

	public String getGerencia() {
		return gerencia;
	}

	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}

	public String getSupervision() {
		return supervision;
	}

	public void setSupervision(String supervision) {
		this.supervision = supervision;
	}
	
	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Nivel getNivel() {
		return Nivel.UNO;
	}
	
	
}
