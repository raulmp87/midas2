package mx.com.afirme.midas2.dto.emision.consulta;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ConsultaCobertura extends Consulta implements Serializable {

	private static final long serialVersionUID = 995773107550552664L;

	@Id
	private Long coberturaId;
	
	private Integer numeroInciso;
	
	private Long polizaId;
	
	private String nombre;
	
	private String 	deducible;
	
	private String sumaAsegurada;
	
	private String primaNeta;

	
	public Long getCoberturaId() {
		return coberturaId;
	}

	public void setCoberturaId(Long coberturaId) {
		this.coberturaId = coberturaId;
	}

	public Integer getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public Long getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(Long polizaId) {
		this.polizaId = polizaId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDeducible() {
		return deducible;
	}

	public void setDeducible(String deducible) {
		this.deducible = deducible;
	}

	public String getSumaAsegurada() {
		return sumaAsegurada;
	}

	public void setSumaAsegurada(String sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}

	public String getPrimaNeta() {
		return primaNeta;
	}

	public void setPrimaNeta(String primaNeta) {
		this.primaNeta = primaNeta;
	}

	@Override
	public Nivel getNivel() {
		return Nivel.CUATRO;
	}
	
}
