package mx.com.afirme.midas2.dto.emision.consulta;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ConsultaCobranza extends Consulta {
	
	private static final long serialVersionUID = 1958849248212859808L;
	
	@Id
	private String indice;
	
	private Long polizaId;
	
	private Integer numeroEndoso;
	
	private Integer numeroInciso;
	
	private String serie;	
		
	private String numeroRecibo;	
	
	private String monto;	
	
	private String fechaInicioVigencia;
	
	private String fechaFinVigencia;
	
	private String estatus;
	
	
	public String getIndice() {
		return indice;
	}

	public void setIndice(String indice) {
		this.indice = indice;
	}

	public Long getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(Long polizaId) {
		this.polizaId = polizaId;
	}

	public Integer getNumeroEndoso() {
		return numeroEndoso;
	}

	public void setNumeroEndoso(Integer numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}

	public Integer getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getNumeroRecibo() {
		return numeroRecibo;
	}

	public void setNumeroRecibo(String numeroRecibo) {
		this.numeroRecibo = numeroRecibo;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(String fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	public String getFechaFinVigencia() {
		return fechaFinVigencia;
	}

	public void setFechaFinVigencia(String fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	@Override
	public Nivel getNivel() {
		return Nivel.CUATRO;
	}
	
	
}
