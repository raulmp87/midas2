package mx.com.afirme.midas2.dto.emision.consulta;

public class SeccionConsulta {

	public static final String AGENTE = "AGENTE";
	public static final String CLIENTE = "CLIENTE";
	public static final String POLIZA = "POLIZA";
	public static final String VEHICULO = "VEHICULO";
	public static final String ENDOSO = "ENDOSO";
	public static final String ANEXO = "ANEXO";
	public static final String SINIESTRO = "SINIESTRO";
	public static final String COBERTURA = "COBERTURA";
	public static final String COBRANZA = "COBRANZA";
}