package mx.com.afirme.midas2.dto.emision.consulta;

import mx.com.afirme.midas.base.PaginadoDTO;

public abstract class Consulta extends PaginadoDTO {

	
	private static final long serialVersionUID = -450885592970861073L;


	public static enum Nivel {
		
		CUATRO(1),
		TRES(2),
		DOS(3),
		UNO(4);
		
		private final Integer value;
		
		Nivel(Integer value) {
			this.value = value;
		}
		
		public Integer getValue() {
			return value;
		}
	};
	
	//Propiedad que cada que se seleccione un detalle se va a limpiar en todos los niveles de ConsultaEmision y se va a marcar
	//la seccion seleccionada
	protected Boolean ultimaSeleccion = false;
	
	//Extendiendo a PaginadoDTO, el total de registros a devolver en una consulta
	protected Long totalRegistros;
	
	protected String orderBy;
	
	protected String direct;
	
	public Boolean getUltimaSeleccion() {
		return ultimaSeleccion;
	}

	public void setUltimaSeleccion(Boolean ultimaSeleccion) {
		this.ultimaSeleccion = ultimaSeleccion;
	}
	
	public Long getTotalRegistros() {
		return totalRegistros;
	}

	public void setTotalRegistros(Long totalRegistros) {
		this.totalRegistros = totalRegistros;
	}
	
	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getDirect() {
		return direct;
	}

	public void setDirect(String direct) {
		this.direct = direct;
	}

	public abstract Nivel getNivel();
		
}
