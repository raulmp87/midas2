package mx.com.afirme.midas2.dto.emision.consulta;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="VW_CONSULTA_SINIESTRO",schema="MIDAS")
public class ConsultaSiniestro extends Consulta implements Serializable, Entidad {

	private static final long serialVersionUID = -4942474690050915954L;
	
	@Id
	@Column(name="NUMERO_SINIESTRO")
	private String numeroSiniestro;
	
	@Column(name="CAUSA")
	private String causa;

	@Column(name="TIPO")
	private String tipo;

	@Column(name="FECHA_SINIESTRO")
	private String fecha;

	@Column(name="MONTO_ESTIMADO_INICIAL")
	private String montoEstimadoInicial;

	@Column(name="PAGOS")
	private String pagos;

	@Column(name="RECUPERACION")
	private String recuperacion;

	@Column(name="TERCERO_INVOLUCRADO")
	private String terceroInvolucrado;

	@Column(name="TIPO_RESPONSABILIDAD")
	private Integer tipoResponsabilidad;  // Afectado = 1, Responsable = 2

	@Column(name="COBERTURAS_AFECTADAS")
	private String coberturasAfectadas;

	@Column(name="DEDUCIBLE")
	private String deducible;

	@Column(name="COMPANIA_INVOLUCRADA")
	private String companiaInvolucrada;

	@Column(name="NUMERO_INCISO")
	private Integer numeroInciso;

	@Column(name="ID_POLIZA")
	private Long polizaId;
	
	@Column(name="ID_AGENTE")
	private String agenteId;
	
	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}

	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}

	public String getCausa() {
		return causa;
	}

	public void setCausa(String causa) {
		this.causa = causa;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getMontoEstimadoInicial() {
		return montoEstimadoInicial;
	}

	public void setMontoEstimadoInicial(String montoEstimadoInicial) {
		this.montoEstimadoInicial = montoEstimadoInicial;
	}

	public String getPagos() {
		return pagos;
	}

	public void setPagos(String pagos) {
		this.pagos = pagos;
	}

	public String getRecuperacion() {
		return recuperacion;
	}

	public void setRecuperacion(String recuperacion) {
		this.recuperacion = recuperacion;
	}

	public String getTerceroInvolucrado() {
		return terceroInvolucrado;
	}

	public void setTerceroInvolucrado(String terceroInvolucrado) {
		this.terceroInvolucrado = terceroInvolucrado;
	}

	public Integer getTipoResponsabilidad() {
		return tipoResponsabilidad;
	}

	public void setTipoResponsabilidad(Integer tipoResponsabilidad) {
		this.tipoResponsabilidad = tipoResponsabilidad;
	}

	public String getCoberturasAfectadas() {
		return coberturasAfectadas;
	}

	public void setCoberturasAfectadas(String coberturasAfectadas) {
		this.coberturasAfectadas = coberturasAfectadas;
	}

	public String getDeducible() {
		return deducible;
	}

	public void setDeducible(String deducible) {
		this.deducible = deducible;
	}

	public String getCompaniaInvolucrada() {
		return companiaInvolucrada;
	}

	public void setCompaniaInvolucrada(String companiaInvolucrada) {
		this.companiaInvolucrada = companiaInvolucrada;
	}

	public Integer getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public Long getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(Long polizaId) {
		this.polizaId = polizaId;
	}
	
	public String getAgenteId() {
		return agenteId;
	}

	public void setAgenteId(String agenteId) {
		this.agenteId = agenteId;
	}

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Nivel getNivel() {
		return Nivel.CUATRO;
	}

}
