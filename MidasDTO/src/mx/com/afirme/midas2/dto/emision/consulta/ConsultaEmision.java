package mx.com.afirme.midas2.dto.emision.consulta;


public class ConsultaEmision {
	
	private ConsultaAgente detalleAgente;
	
	private ConsultaCliente detalleCliente;
		
	private ConsultaPoliza detallePoliza;
	
	private ConsultaVehiculo detalleVehiculo;
	
	private ConsultaEndoso detalleEndoso;
	
	private ConsultaAnexo detalleAnexo;
	
	private ConsultaSiniestro detalleSiniestro;
	
	private ConsultaCobertura detalleCobertura;
	
	private ConsultaCobranza detalleCobranza;
	
	
	
	public ConsultaAgente getDetalleAgente() {
		return detalleAgente;
	}



	public void setDetalleAgente(ConsultaAgente detalleAgente) {
		this.detalleAgente = detalleAgente;
	}



	public ConsultaCliente getDetalleCliente() {
		return detalleCliente;
	}



	public void setDetalleCliente(ConsultaCliente detalleCliente) {
		this.detalleCliente = detalleCliente;
	}



	public ConsultaPoliza getDetallePoliza() {
		return detallePoliza;
	}



	public void setDetallePoliza(ConsultaPoliza detallePoliza) {
		this.detallePoliza = detallePoliza;
	}



	public ConsultaVehiculo getDetalleVehiculo() {
		return detalleVehiculo;
	}



	public void setDetalleVehiculo(ConsultaVehiculo detalleVehiculo) {
		this.detalleVehiculo = detalleVehiculo;
	}



	public ConsultaEndoso getDetalleEndoso() {
		return detalleEndoso;
	}



	public void setDetalleEndoso(ConsultaEndoso detalleEndoso) {
		this.detalleEndoso = detalleEndoso;
	}



	public ConsultaAnexo getDetalleAnexo() {
		return detalleAnexo;
	}



	public void setDetalleAnexo(ConsultaAnexo detalleAnexo) {
		this.detalleAnexo = detalleAnexo;
	}



	public ConsultaSiniestro getDetalleSiniestro() {
		return detalleSiniestro;
	}



	public void setDetalleSiniestro(ConsultaSiniestro detalleSiniestro) {
		this.detalleSiniestro = detalleSiniestro;
	}



	public ConsultaCobertura getDetalleCobertura() {
		return detalleCobertura;
	}



	public void setDetalleCobertura(ConsultaCobertura detalleCobertura) {
		this.detalleCobertura = detalleCobertura;
	}



	public ConsultaCobranza getDetalleCobranza() {
		return detalleCobranza;
	}



	public void setDetalleCobranza(ConsultaCobranza detalleCobranza) {
		this.detalleCobranza = detalleCobranza;
	}

	public ConsultaEmision() {
		this.detalleAgente = new ConsultaAgente();
		this.detalleCliente = new ConsultaCliente();
		this.detallePoliza = new ConsultaPoliza();
		this.detalleVehiculo = new ConsultaVehiculo();
		this.detalleEndoso = new ConsultaEndoso();
		this.detalleAnexo = new ConsultaAnexo();
		this.detalleSiniestro = new ConsultaSiniestro();
		this.detalleCobertura = new ConsultaCobertura();
		this.detalleCobranza = new ConsultaCobranza();
	}

	
	
	
	
}
