package mx.com.afirme.midas2.dto.emision.consulta;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ConsultaAnexo extends Consulta implements Serializable {
	
	private static final long serialVersionUID = -7200781142947970896L;

	@Id
	private Long id;
	
	private Long polizaId;
	
	private String tipo; 
	
	private String descripcion;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(Long polizaId) {
		this.polizaId = polizaId;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Override
	public Nivel getNivel() {
		return Nivel.TRES;
	}

	
}
