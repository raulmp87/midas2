package mx.com.afirme.midas2.dto.emision.consulta;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ConsultaEndoso extends Consulta implements Serializable {

	private static final long serialVersionUID = -8559503016223073292L;

	@Id
	private Integer numeroEndoso;
	
	private Long polizaId;

	private String tipoEndoso;

	private String motivo;

	private String estatus;

	private String fechaEmision;

	private String fechaInicioVigencia;

	private String fechaFinVigencia;

	private String primaNeta;

	private String derechos;

	private String iva;

	private String recargo;

	private String primaTotal;

	public Integer getNumeroEndoso() {
		return numeroEndoso;
	}

	public void setNumeroEndoso(Integer numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}

	public Long getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(Long polizaId) {
		this.polizaId = polizaId;
	}

	public String getTipoEndoso() {
		return tipoEndoso;
	}

	public void setTipoEndoso(String tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public String getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(String fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	public String getFechaFinVigencia() {
		return fechaFinVigencia;
	}

	public void setFechaFinVigencia(String fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	public String getPrimaNeta() {
		return primaNeta;
	}

	public void setPrimaNeta(String primaNeta) {
		this.primaNeta = primaNeta;
	}

	public String getDerechos() {
		return derechos;
	}

	public void setDerechos(String derechos) {
		this.derechos = derechos;
	}

	public String getIva() {
		return iva;
	}

	public void setIva(String iva) {
		this.iva = iva;
	}

	public String getRecargo() {
		return recargo;
	}

	public void setRecargo(String recargo) {
		this.recargo = recargo;
	}

	public String getPrimaTotal() {
		return primaTotal;
	}

	public void setPrimaTotal(String primaTotal) {
		this.primaTotal = primaTotal;
	}

	@Override
	public Nivel getNivel() {
		return Nivel.TRES;
	}
	
}
