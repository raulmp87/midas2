package mx.com.afirme.midas2.dto.emision.ppct;

import java.io.Serializable;

public class StatusSolicitudChequeDTO implements Serializable {

		
	private static final long serialVersionUID = 4843285139069413583L;

	public static final String PENDIENTE = "P";
	public static final String SOLICITADO = "S";
	public static final String TERMINADO = "T";
	public static final String CANCELADO = "C";
	
	
	private String clave;
	
	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getDescripcion() {
		
		if (clave != null) {
			if (clave.equals(PENDIENTE)) {
				return "PENDIENTE";
			} else if (clave.equals(SOLICITADO)) {
				return "SOLICITADO";
			} else if (clave.equals(TERMINADO)) {
				return "TERMINADO";
			} else if (clave.equals(CANCELADO)) {
				return "CANCELADO";
			}
		}
		
		return "";
		
	}

	public StatusSolicitudChequeDTO(String clave) {
		this.clave = clave;
	}
	
	
}
