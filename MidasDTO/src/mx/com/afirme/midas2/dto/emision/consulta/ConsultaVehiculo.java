package mx.com.afirme.midas2.dto.emision.consulta;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import mx.com.afirme.midas2.dao.catalogos.Entidad;

@Entity
@Table(name="VW_CONSULTA_VEHICULO",schema="MIDAS")
public class ConsultaVehiculo extends Consulta implements Serializable, Entidad {

	private static final long serialVersionUID = 9026553831912828528L;

	@Id
	@Column(name="INDICE")
	private String indice;
	
	@Column(name="NUMERO_INCISO")
	private Integer numeroInciso;
	
	@Column(name="ID_POLIZA")
	private Long polizaId;
	
	@Column(name="ID_AGENTE")
	private String agenteId;
	
	@Column(name="ID_CLIENTE")
	private Long clienteId;
	
	@Column(name="MARCA")
	private String marca;
	
	@Column(name="TIPO")
	private String tipo;
	
	@Column(name="MODELO")
	private String modelo;
	
	@Column(name="MOTOR")
	private String motor;
	
	@Column(name="SERIE")
	private String serie;
	
	@Column(name="PLACA")
	private String placa;
	
	@Column(name="PAQUETE")
	private String paquete;
	
	@Column(name="ESTATUS")
	private String estatus;
	
	@Column(name="FECHA_INICIO_VIGENCIA")
	private String fechaInicioVigencia;
	
	@Column(name="FECHA_FIN_VIGENCIA")
	private String fechaFinVigencia;
	
	@Column(name="PRIMANETA")
	private String primaNeta;
	
	@Column(name="NOMBREASEGURADO")
	private String nombreAsegurado;

	
	public String getIndice() {
		return indice;
	}

	public void setIndice(String indice) {
		this.indice = indice;
	}

	public Integer getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public Long getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(Long polizaId) {
		this.polizaId = polizaId;
	}
	
	public String getAgenteId() {
		return agenteId;
	}

	public void setAgenteId(String agenteId) {
		this.agenteId = agenteId;
	}

	public Long getClienteId() {
		return clienteId;
	}

	public void setClienteId(Long clienteId) {
		this.clienteId = clienteId;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getMotor() {
		return motor;
	}

	public void setMotor(String motor) {
		this.motor = motor;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getPaquete() {
		return paquete;
	}

	public void setPaquete(String paquete) {
		this.paquete = paquete;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(String fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	public String getFechaFinVigencia() {
		return fechaFinVigencia;
	}

	public void setFechaFinVigencia(String fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	public String getPrimaNeta() {
		return primaNeta;
	}

	public void setPrimaNeta(String primaNeta) {
		this.primaNeta = primaNeta;
	}

	public String getNombreAsegurado() {
		return nombreAsegurado;
	}

	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}

	@Override
	public <K> K getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <K> K getBusinessKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Nivel getNivel() {
		return Nivel.TRES;
	}
	
}
