package mx.com.afirme.midas2.dto.condicionespecial;

import java.util.List;

import mx.com.afirme.midas2.domain.condicionesespeciales.AreaImpacto;
import mx.com.afirme.midas2.domain.condicionesespeciales.FactorAreaCondEsp;

public class AreaCondicionDTO {

	private Long idCondicionEspecial;
	
	private AreaImpacto area;
	
	private List<FactorCondicionDTO> factores;
	
	private boolean selected;

	public Long getIdCondicionEspecial() {
		return idCondicionEspecial;
	}

	public void setIdCondicionEspecial(Long idCondicionEspecial) {
		this.idCondicionEspecial = idCondicionEspecial;
	}

	public AreaImpacto getArea() {
		return area;
	}

	public void setArea(AreaImpacto area) {
		this.area = area;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public List<FactorCondicionDTO> getFactores() {
		return factores;
	}

	public void setFactores(List<FactorCondicionDTO> factores) {
		this.factores = factores;
	}

	
}
