package mx.com.afirme.midas2.dto.condicionespecial;

import mx.com.afirme.midas2.domain.condicionesespeciales.VariableAjuste;

/**
 * 
 * @author Lizeth De La Garza
 *
 */
public class VarAjusteCondicionDTO {
	
	private Long idCondicionEspecial;
	
	private VariableAjuste variableAjuste;

	public Long getIdCondicionEspecial() {
		return idCondicionEspecial;
	}

	public void setIdCondicionEspecial(Long idCondicionEspecial) {
		this.idCondicionEspecial = idCondicionEspecial;
	}

	public VariableAjuste getVariableAjuste() {
		return variableAjuste;
	}

	public void setVariableAjuste(VariableAjuste variableAjuste) {
		this.variableAjuste = variableAjuste;
	}
	
}
