package mx.com.afirme.midas2.dto.condicionespecial;

import java.util.List;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;

/**
 * 
 * @author Lizeth De La Garza
 *
 */
public class CoberturasCondicionDTO {
	
	private Long idCondicionEspecial;
	
	private SeccionDTO seccion;
	
	private List<CoberturaDTO> listCoberturas;
	
	public CoberturasCondicionDTO() {
		
	}
	
	public CoberturasCondicionDTO(Long idCondicionEspecial, SeccionDTO seccion, List<CoberturaDTO> listCoberturas) {
		this.idCondicionEspecial = idCondicionEspecial;
		this.seccion = seccion;
		this.listCoberturas = listCoberturas;
	}
	
	
	public Long getIdCondicionEspecial() {
		return idCondicionEspecial;
	}

	public void setIdCondicionEspecial(Long idCondicionEspecial) {
		this.idCondicionEspecial = idCondicionEspecial;
	}

	public SeccionDTO getSeccion() {
		return seccion;
	}

	public void setSeccion(SeccionDTO seccion) {
		this.seccion = seccion;
	}

	public List<CoberturaDTO> getListCoberturas() {
		return listCoberturas;
	}

	public void setListCoberturas(List<CoberturaDTO> listCoberturas) {
		this.listCoberturas = listCoberturas;
	}

}
