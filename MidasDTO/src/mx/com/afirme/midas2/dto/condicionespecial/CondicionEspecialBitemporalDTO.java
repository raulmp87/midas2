package mx.com.afirme.midas2.dto.condicionespecial;

import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial.NivelAplicacion;

import org.springframework.stereotype.Component;

@Component
public class CondicionEspecialBitemporalDTO {
	
	private CondicionEspecial condicionEspecial;
	private Long idCondicionBitemporal;
	private Long idContinuity;
	private Short tipoCondicion;
	private Integer obligatoria;

	
	public CondicionEspecial getCondicionEspecial() {
		return condicionEspecial;
	}
	public void setCondicionEspecial(CondicionEspecial condicionEspecial) {
		this.condicionEspecial = condicionEspecial;
	}
	 
	public Long getIdCondicionBitemporal() {
		return idCondicionBitemporal;
	}
	public void setIdCondicionBitemporal(Long idCondicionBitemporal) {
		this.idCondicionBitemporal = idCondicionBitemporal;
	}	
	
	public Long getIdContinuity() {
		return idContinuity;
	}
	public void setIdContinuity(Long idContinuity) {
		this.idContinuity = idContinuity;
	}
	public Short getTipoCondicion() {
		return tipoCondicion;
	}
	public void setTipoCondicion(Short tipoCondicion) {
		this.tipoCondicion = tipoCondicion;
	}
	public Integer getObligatoria() {
		return obligatoria;
	}
	public void setObligatoria(Integer obligatoria) {
		this.obligatoria = obligatoria;
	}
	
	public String getTipoCondicionStr(){
	 String result = "";
		if(this.tipoCondicion == 0){
			result = "Poliza";
		}else if (this.tipoCondicion == 1){
			result = "Inciso";
		}
	  return result;
	}
	
	public void setTipoCondicionStr(String tipoCondicionStr){
		if("Inciso".equals(tipoCondicionStr)){
			this.tipoCondicion = 1;
		}else if ("Poliza".equals(tipoCondicionStr)){
			this.tipoCondicion = 0;
		}
	}
}
