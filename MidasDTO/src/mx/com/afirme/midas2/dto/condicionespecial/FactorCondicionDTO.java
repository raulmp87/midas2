package mx.com.afirme.midas2.dto.condicionespecial;

import mx.com.afirme.midas2.domain.condicionesespeciales.Factor;

public class FactorCondicionDTO {

	private Long idCondicionEspecial;
	
	private Long idAreaImpacto;
	
	private Factor factor;
	
	private boolean selected;

	public Long getIdCondicionEspecial() {
		return idCondicionEspecial;
	}

	public void setIdCondicionEspecial(Long idCondicionEspecial) {
		this.idCondicionEspecial = idCondicionEspecial;
	}

	public Long getIdAreaImpacto() {
		return idAreaImpacto;
	}

	public void setIdAreaImpacto(Long idAreaImpacto) {
		this.idAreaImpacto = idAreaImpacto;
	}

	public Factor getFactor() {
		return factor;
	}

	public void setFactor(Factor factor) {
		this.factor = factor;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	
}
