package mx.com.afirme.midas2.dto;

import java.io.Serializable;

public class MensajeDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6173313053353677440L;
	public static final String TIPO_MENSAJE_EXITO = "30";
	public static final String TIPO_MENSAJE_ERROR = "10";
	public static final String TIPO_MENSAJE_INFORMACION = "20";
	public static final String MENSAJE_EXITO_GENERICO = "La operación fue realizada con éxito";
	public static final String MENSAJE_ERROR_GENERICO = "Ocurrió un error al realizar la operación";
	public static final String MENSAJE_VIGENCIA_MINIMO = "El rango de fecha es menor al minimo de vigencia permitido ";
	public static final String MENSAJE_VIGENCIA_MAXIMO = "El rango de fecha es mayor al minimo de vigencia permitido ";
	public static final String MENSAJE_COPIAR_COTIZACION ="Se ha copiado exitosamente la cotizaci\u00F3n: ";
	public static final String MENSAJE_ERROR_COPIAR_COTIZACION="Error al copiar la cotizaci\u00F3n: ";
	public static final String MENSAJE_COPIAR_COND_ESP ="Se ha creado correctamente la nueva condici\u00F3n especial: ";
	public static final String MENSAJE_ERROR_COND_ESP="Error al copiar la condici\u00F3n especial: ";
	public static final String MENSAJE_ASIGNAR_INCISO ="Se ha asignado correctamente el inciso al reporte. ";
	public static final String MENSAJE_ERROR_ASIGNAR_INCISO ="Ocurri\u00F3 un error al asignar el inciso al reporte. ";
	public static final String MENSAJE_ASIGNAR_CARTA_COBERTURA ="Se ha asignado correctamente la solicitud al reporte. ";
	public static final String MENSAJE_ERROR_ASIGNAR_CARTA_COBERTURA ="Ocurri\u00F3 un error al asignar la solicitud al reporte. ";
	public static final String MENSAJE_GUARDAR_ESTIMACION = "Se ha guardado correctamente la estimaci\u00F3n.";
	public static final String MENSAJE_ERROR_GUARDAR_ESTIMACION = "Ocurri\u00F3 un error al guardar la estimaci\u00F3n.";
	public static final String MENSAJE_ELIMINAR_SOLICITUD_SUSPENSION = "Se ha eliminado correctamente la solicitud de suspensi\u00F3n de servicio.";
	public static final String MENSAJE_ERROR_ELIMINAR_SOLICITUD_SUSPENSION = "Ocurri\u00F3 un error al guardar la solicitud de suspensi\u00F3n de servicio.";
	public static final String MENSAJE_GUARDAR_CONFIGURACION_AUT_RESERVA = "La configuracion se ha guardado exitosamente";
	public static final String MENSAJE_REQUIERE_AUT_RESERVA = "Se requiere autorizaci\u00F3n por antig\u00fcedad del Siniestro";
	public static final String MENSAJE_ERROR_COBERTURAS_AFECTADAS = "La P\u00F3liza actual del Siniestro cuenta con coberturas afectadas. No es posible asignar una nueva P\u00F3liza";
	public static final String MENSAJE_VIGENCIA_INCISO_SINIESTRO = "La P\u00F3liza se encuentra dentro de sus primeros 30 d\u00edas de Vigencia sin renovaci\u00F3n anterior";
	public static final String MENSAJE_PERIODO_DESCUBIERTO_INCISO_SINIESTRO = "El N\u00famero de Serie cuenta con más de 24 horas sin cobertura";
	
	
	private String tipoMensaje;
	private String mensaje;
	private boolean visible;
	
	public String getTipoMensaje() {
		return tipoMensaje;
	}
	public void setTipoMensaje(String tipoMensaje) {
		this.tipoMensaje = tipoMensaje;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public boolean isVisible() {
		return visible;
	}
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

}
