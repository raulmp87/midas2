package mx.com.afirme.midas2.dto.expedientejuridico;

import java.io.Serializable;
import java.util.Date;

import mx.com.afirme.midas2.annotation.Exportable;

public class ExpedienteJuridicoExportableDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6145136649331769450L;
	
	
	private String noSinietro;
	private String anioSiniestro;
	private String reporteCabina;
	private String poliza;
	private String inciso;
	private String contratante;
	private String telContratante;
	private String conductor;
	private Date fechaOcurrido;
	private Date fechaReportado;
	private Date fechaTurnoJuridico;
	private String tVehicAseg;
	private String modelo;
	private String serie;
	private String placas;
	private String causaSin;
	private String responsabilidad;
	private String descripcionSiniestro;
	private String estatusJuridico;
	private String comentariosJuridico;
	private String estFinalDM;
	private String estFinalRCVehiculos;
	private String estFinalRCPersonas;
	private String estFinalRCBienes;
	private String estFinalGM;
	private String estFinalRT;
	private String estFinalRCViajero;
	private String estFinalAAC;
	private String estFinalAYC;
	private String estFinalEE;
	private String pagosDM;
	private String pagosRCVehiculo;
	private String pagosRCPersonas;
	private String pagosRCBienes;
	private String pagosGM;
	private String pagosRT;
	private String pagosRCViajero;
	private String pagosAAC;
	private String pagosAYC;
	private String pagosEE;
	private String recuperacionesProveedor;
	private String recuperacionesDeducible;
	private String recuperacionesSalvamento;
	private String recuperacionesCompanias;
	private String recuperacionesCruceroYVial;
	private String recuperacionesPendCompanias;;
	private String tVehiculoTercero;
	private String terminoAjuste;
	private String companiaTerceroResp;
	private String ciudad;
	private String estado;
	private String ajustador;
	private String motivoTurno;
	private String numExpedienteExterno;
	private String tipoAbogado;
	private String cveAbogado;
	private Date fechaConclusionLegal;
	private Date fechaConclusionAfirme;
	private String tipoConclusion;
	private String bVehicDetenido;
	private String estatusVehiculo;
	private Date fechaLiberacionVehic;
	private String impIngSalvam;
	
	public ExpedienteJuridicoExportableDTO() {
		super();
	}
	
	@Exportable(columnName="NUM_SINIESTRO", columnOrder=1)
	public String getNoSinietro() {
		return noSinietro;
	}
	public void setNoSinietro(String noSinietro) {
		this.noSinietro = noSinietro;
	}
	@Exportable(columnName="ANIO_SINIESTRO", columnOrder=2)
	public String getAnioSiniestro() {
		return anioSiniestro;
	}
	public void setAnioSiniestro(String anioSiniestro) {
		this.anioSiniestro = anioSiniestro;
	}
	@Exportable(columnName="REPORTE_CABINA", columnOrder=3)
	public String getReporteCabina() {
		return reporteCabina;
	}
	public void setReporteCabina(String reporteCabina) {
		this.reporteCabina = reporteCabina;
	}
	@Exportable(columnName="POLIZA", columnOrder=4)
	public String getPoliza() {
		return poliza;
	}
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	@Exportable(columnName="INCISO", columnOrder=5)
	public String getInciso() {
		return inciso;
	}
	public void setInciso(String inciso) {
		this.inciso = inciso;
	}
	@Exportable(columnName="CONTRATANTE", columnOrder=6)
	public String getContratante() {
		return contratante;
	}
	public void setContratante(String contratante) {
		this.contratante = contratante;
	}
	@Exportable(columnName="TEL_CONTRATANTE", columnOrder=7)
	public String getTelContratante() {
		return telContratante;
	}
	public void setTelContratante(String telContratante) {
		this.telContratante = telContratante;
	}
	@Exportable(columnName="CONDUCTOR", columnOrder=8)
	public String getConductor() {
		return conductor;
	}
	public void setConductor(String conductor) {
		this.conductor = conductor;
	}
	@Exportable(columnName="FH_OCURRIDO", columnOrder=9)
	public Date getFechaOcurrido() {
		return fechaOcurrido;
	}
	public void setFechaOcurrido(Date fechaOcurrido) {
		this.fechaOcurrido = fechaOcurrido;
	}
	@Exportable(columnName="FH_REPORTADO", columnOrder=10)
	public Date getFechaReportado() {
		return fechaReportado;
	}
	public void setFechaReportado(Date fechaReportado) {
		this.fechaReportado = fechaReportado;
	}
	@Exportable(columnName="FH_TURNO_JUR", columnOrder=11)
	public Date getFechaTurnoJuridico() {
		return fechaTurnoJuridico;
	}
	public void setFechaTurnoJuridico(Date fechaTurnoJuridico) {
		this.fechaTurnoJuridico = fechaTurnoJuridico;
	}
	@Exportable(columnName="T_VEHIC_ASEG", columnOrder=12)
	public String gettVehicAseg() {
		return tVehicAseg;
	}
	public void settVehicAseg(String tVehicAseg) {
		this.tVehicAseg = tVehicAseg;
	}
	@Exportable(columnName="MODELO", columnOrder=13)
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	@Exportable(columnName="SERIE", columnOrder=14)
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	@Exportable(columnName="PLACAS", columnOrder=15)
	public String getPlacas() {
		return placas;
	}
	public void setPlacas(String placas) {
		this.placas = placas;
	}
	@Exportable(columnName="CAUSA_SIN", columnOrder=16)
	public String getCausaSin() {
		return causaSin;
	}
	public void setCausaSin(String causaSin) {
		this.causaSin = causaSin;
	}
	@Exportable(columnName="RESPONSABILIDAD", columnOrder=17)
	public String getResponsabilidad() {
		return responsabilidad;
	}
	public void setResponsabilidad(String responsabilidad) {
		this.responsabilidad = responsabilidad;
	}
	@Exportable(columnName="DESCRIPCION_SINIESTRO", columnOrder=18)
	public String getDescripcionSiniestro() {
		return descripcionSiniestro;
	}
	public void setDescripcionSiniestro(String descripcionSiniestro) {
		this.descripcionSiniestro = descripcionSiniestro;
	}
	@Exportable(columnName="STATUS_JURIDICO", columnOrder=19)
	public String getEstatusJuridico() {
		return estatusJuridico;
	}
	public void setEstatusJuridico(String estatusJuridico) {
		this.estatusJuridico = estatusJuridico;
	}
	@Exportable(columnName="COMENTARIOS_JURIDICO", columnOrder=20)
	public String getComentariosJuridico() {
		return comentariosJuridico;
	}
	public void setComentariosJuridico(String comentariosJuridico) {
		this.comentariosJuridico = comentariosJuridico;
	}
	@Exportable(columnName="EST_FINAL_DM", columnOrder=21)
	public String getEstFinalDM() {
		return estFinalDM;
	}
	public void setEstFinalDM(String estFinalDM) {
		this.estFinalDM = estFinalDM;
	}
	@Exportable(columnName="EST_FINAL_RC VEHÍCULOS", columnOrder=22)
	public String getEstFinalRCVehiculos() {
		return estFinalRCVehiculos;
	}
	public void setEstFinalRCVehiculos(String estFinalRCVehiculos) {
		this.estFinalRCVehiculos = estFinalRCVehiculos;
	}
	@Exportable(columnName="EST_FINAL_RC PERSONAS", columnOrder=23)
	public String getEstFinalRCPersonas() {
		return estFinalRCPersonas;
	}
	public void setEstFinalRCPersonas(String estFinalRCPersonas) {
		this.estFinalRCPersonas = estFinalRCPersonas;
	}
	@Exportable(columnName="EST_FINAL_RC BIENES", columnOrder=24)
	public String getEstFinalRCBienes() {
		return estFinalRCBienes;
	}
	public void setEstFinalRCBienes(String estFinalRCBienes) {
		this.estFinalRCBienes = estFinalRCBienes;
	}
	@Exportable(columnName="EST_FINAL_GM", columnOrder=25)
	public String getEstFinalGM() {
		return estFinalGM;
	}
	public void setEstFinalGM(String estFinalGM) {
		this.estFinalGM = estFinalGM;
	}
	@Exportable(columnName="EST_FINAL_RT", columnOrder=26)
	public String getEstFinalRT() {
		return estFinalRT;
	}
	public void setEstFinalRT(String estFinalRT) {
		this.estFinalRT = estFinalRT;
	}
	@Exportable(columnName="EST_FINAL_RC VIAJERO", columnOrder=27)
	public String getEstFinalRCViajero() {
		return estFinalRCViajero;
	}
	public void setEstFinalRCViajero(String estFinalRCViajero) {
		this.estFinalRCViajero = estFinalRCViajero;
	}
	@Exportable(columnName="EST_FINAL_AAC", columnOrder=28)
	public String getEstFinalAAC() {
		return estFinalAAC;
	}
	public void setEstFinalAAC(String estFinalAAC) {
		this.estFinalAAC = estFinalAAC;
	}
	@Exportable(columnName="EST_FINAL_AYC", columnOrder=29)
	public String getEstFinalAYC() {
		return estFinalAYC;
	}
	public void setEstFinalAYC(String estFinalAYC) {
		this.estFinalAYC = estFinalAYC;
	}
	@Exportable(columnName="EST_FINAL_EE", columnOrder=30)
	public String getEstFinalEE() {
		return estFinalEE;
	}
	public void setEstFinalEE(String estFinalEE) {
		this.estFinalEE = estFinalEE;
	}
	@Exportable(columnName="PAGOS_DM", columnOrder=31)
	public String getPagosDM() {
		return pagosDM;
	}
	public void setPagosDM(String pagosDM) {
		this.pagosDM = pagosDM;
	}
	@Exportable(columnName="PAGOS_RC VEHÍCULOS", columnOrder=32)
	public String getPagosRCVehiculo() {
		return pagosRCVehiculo;
	}
	public void setPagosRCVehiculo(String pagosRCVehiculo) {
		this.pagosRCVehiculo = pagosRCVehiculo;
	}
	@Exportable(columnName="PAGOS_RC PERSONAS", columnOrder=33)
	public String getPagosRCPersonas() {
		return pagosRCPersonas;
	}
	public void setPagosRCPersonas(String pagosRCPersonas) {
		this.pagosRCPersonas = pagosRCPersonas;
	}
	@Exportable(columnName="PAGOS_RC BINES", columnOrder=34)
	public String getPagosRCBienes() {
		return pagosRCBienes;
	}
	public void setPagosRCBienes(String pagosRCBienes) {
		this.pagosRCBienes = pagosRCBienes;
	}
	@Exportable(columnName="PAGOS_GM", columnOrder=35)
	public String getPagosGM() {
		return pagosGM;
	}
	public void setPagosGM(String pagosGM) {
		this.pagosGM = pagosGM;
	}
	@Exportable(columnName="PAGOS_RT", columnOrder=36)
	public String getPagosRT() {
		return pagosRT;
	}
	public void setPagosRT(String pagosRT) {
		this.pagosRT = pagosRT;
	}
	@Exportable(columnName="PAGOS_RC VIAJERO", columnOrder=37)
	public String getPagosRCViajero() {
		return pagosRCViajero;
	}
	public void setPagosRCViajero(String pagosRCViajero) {
		this.pagosRCViajero = pagosRCViajero;
	}
	@Exportable(columnName="PAGOS_AAC", columnOrder=38)
	public String getPagosAAC() {
		return pagosAAC;
	}
	public void setPagosAAC(String pagosAAC) {
		this.pagosAAC = pagosAAC;
	}
	@Exportable(columnName="PAGOS_AYC", columnOrder=39)
	public String getPagosAYC() {
		return pagosAYC;
	}
	public void setPagosAYC(String pagosAYC) {
		this.pagosAYC = pagosAYC;
	}
	@Exportable(columnName="PAGOS_EE", columnOrder=40)
	public String getPagosEE() {
		return pagosEE;
	}
	public void setPagosEE(String pagosEE) {
		this.pagosEE = pagosEE;
	}
	@Exportable(columnName="RECUPERACIONES_SALVAMENTO", columnOrder=41)
	public String getRecuperacionesSalvamento() {
		return recuperacionesSalvamento;
	}

	public void setRecuperacionesSalvamento(String recuperacionesSalvamento) {
		this.recuperacionesSalvamento = recuperacionesSalvamento;
	}
	
	@Exportable(columnName="RECUPERACIONES_DEDUCIBLES", columnOrder=42)
	public String getRecuperacionesDeducible() {
		return recuperacionesDeducible;
	}
	public void setRecuperacionesDeducible(String recuperacionesDeducible) {
		this.recuperacionesDeducible = recuperacionesDeducible;
	}
	@Exportable(columnName="RECUPERACIONES_PROVEEDOR", columnOrder=43)
	public String getRecuperacionesProveedor() {
		return recuperacionesProveedor;
	}
	public void setRecuperacionesProveedor(String recuperacionesProveedor) {
		this.recuperacionesProveedor = recuperacionesProveedor;
	}
	@Exportable(columnName="RECUPERACIONES_COMPANIAS", columnOrder=44)
	public String getRecuperacionesCompanias() {
		return recuperacionesCompanias;
	}
	public void setRecuperacionesCompanias(String recuperacionesCompañias) {
		this.recuperacionesCompanias = recuperacionesCompañias;
	}
	@Exportable(columnName="RECUPERACIONES_CRUCEROYVIAL", columnOrder=45)
	public String getRecuperacionesCruceroYVial() {
		return recuperacionesCruceroYVial;
	}
	public void setRecuperacionesCruceroYVial(String recuperacionesCruceroYVial) {
		this.recuperacionesCruceroYVial = recuperacionesCruceroYVial;
	}
	@Exportable(columnName="RECUP_PEND_COMPANIAS", columnOrder=46)
	public String getRecuperacionesPendCompanias() {
		return recuperacionesPendCompanias;
	}
	public void setRecuperacionesPendCompanias(String recuperacionesPendCompanias) {
		this.recuperacionesPendCompanias = recuperacionesPendCompanias;
	}
	@Exportable(columnName="T_VEHIC_TERCERO", columnOrder=47)
	public String gettVehiculoTercero() {
		return tVehiculoTercero;
	}
	public void settVehiculoTercero(String tVehiculoTercero) {
		this.tVehiculoTercero = tVehiculoTercero;
	}
	@Exportable(columnName="TERMINO_AJUSTE", columnOrder=48)
	public String getTerminoAjuste() {
		return terminoAjuste;
	}
	public void setTerminoAjuste(String terminoAjuste) {
		this.terminoAjuste = terminoAjuste;
	}
	@Exportable(columnName="COMPANIA_TERC_RESP", columnOrder=49)
	public String getCompaniaTerceroResp() {
		return companiaTerceroResp;
	}
	public void setCompaniaTerceroResp(String companiaTerceroResp) {
		this.companiaTerceroResp = companiaTerceroResp;
	}
	@Exportable(columnName="CIUDAD", columnOrder=50)
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	@Exportable(columnName="ESTADO", columnOrder=51)
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	@Exportable(columnName="AJUSTADOR", columnOrder=52)
	public String getAjustador() {
		return ajustador;
	}
	public void setAjustador(String ajustador) {
		this.ajustador = ajustador;
	}
	@Exportable(columnName="MOTIVO_TURNO", columnOrder=53)
	public String getMotivoTurno() {
		return motivoTurno;
	}
	public void setMotivoTurno(String motivoTurno) {
		this.motivoTurno = motivoTurno;
	}
	@Exportable(columnName="NUM_EXPEDIENTE_EXTERNO", columnOrder=54)
	public String getNumExpedienteExterno() {
		return numExpedienteExterno;
	}
	public void setNumExpedienteExterno(String numExpedienteExterno) {
		this.numExpedienteExterno = numExpedienteExterno;
	}
	@Exportable(columnName="TIPO_ABOGADO", columnOrder=55)
	public String getTipoAbogado() {
		return tipoAbogado;
	}
	public void setTipoAbogado(String tipoAbogado) {
		this.tipoAbogado = tipoAbogado;
	}
	@Exportable(columnName="CVE_ABOGADO", columnOrder=56)
	public String getCveAbogado() {
		return cveAbogado;
	}
	public void setCveAbogado(String cveAbogado) {
		this.cveAbogado = cveAbogado;
	}
	@Exportable(columnName="F_CONCLUSION_LEGAL", columnOrder=57)
	public Date getFechaConclusionLegal() {
		return fechaConclusionLegal;
	}
	public void setFechaConclusionLegal(Date fechaConclusionLegal) {
		this.fechaConclusionLegal = fechaConclusionLegal;
	}
	@Exportable(columnName="F_CONCLUSION_AFIRME", columnOrder=58)
	public Date getFechaConclusionAfirme() {
		return fechaConclusionAfirme;
	}
	public void setFechaConclusionAfirme(Date fechaConclusionAfirme) {
		this.fechaConclusionAfirme = fechaConclusionAfirme;
	}
	@Exportable(columnName="TIPO_CONCLUSION", columnOrder=59)
	public String getTipoConclusion() {
		return tipoConclusion;
	}
	public void setTipoConclusion(String tipoConclusion) {
		this.tipoConclusion = tipoConclusion;
	}
	@Exportable(columnName="B_VEHIC_DETENIDO", columnOrder=60)
	public String getbVehicDetenido() {
		return bVehicDetenido;
	}
	public void setbVehicDetenido(String bVehicDetenido) {
		this.bVehicDetenido = bVehicDetenido;
	}
	@Exportable(columnName="ESTATUS_VEHICULO", columnOrder=61)
	public String getEstatusVehiculo() {
		return estatusVehiculo;
	}
	public void setEstatusVehiculo(String estatusVehiculo) {
		this.estatusVehiculo = estatusVehiculo;
	}
	@Exportable(columnName="F_LIBERACION_VEHIC", columnOrder=62)
	public Date getFechaLiberacionVehic() {
		return fechaLiberacionVehic;
	}
	public void setFechaLiberacionVehic(Date fechaLiberacionVehic) {
		this.fechaLiberacionVehic = fechaLiberacionVehic;
	}
	@Exportable(columnName="IMP_ING_SALVAM", columnOrder=63)
	public String getImpIngSalvam() {
		return impIngSalvam;
	}
	public void setImpIngSalvam(String impIngSalvam) {
		this.impIngSalvam = impIngSalvam;
	}
}
