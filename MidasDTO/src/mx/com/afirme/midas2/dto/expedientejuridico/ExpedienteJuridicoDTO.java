package mx.com.afirme.midas2.dto.expedientejuridico;

import java.io.Serializable;
import java.util.Date;

import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;

/**
 * @author Israel
 * @version 1.0
 * @created 06-ago.-2015 10:29:50 a. m.
 */
public class ExpedienteJuridicoDTO implements Serializable {

    /**
    * 
     */
    private static final long serialVersionUID = 1L;
    private String numeroSiniestro;
    private String motivoTurno;
    private String estatusJuridico;
    private String expedienteProveedor;
    private Short vehiculoDetenido;
    private String estatusVehiculo;
    private ServicioSiniestro abogado;
    private String tipoConclusion;
    private Date fechaTurnoDe;
    private Date fechaTurnoHasta;
    private Date fechaConclusionLegalDe;
    private Date fechaConclusionLegalHasta;
    private Date fechaConclusionAfirmeDe;
    private Date fechaConclusionAfirmeHasta;

    public ExpedienteJuridicoDTO(){

    }

    public String getNumeroSiniestro() {
          return numeroSiniestro;
    }

    public void setNumeroSiniestro(String numeroSiniestro) {
          this.numeroSiniestro = numeroSiniestro;
    }

    public String getMotivoTurno() {
          return motivoTurno;
    }

    public void setMotivoTurno(String motivoTurno) {
          this.motivoTurno = motivoTurno;
    }

    public String getEstatusJuridico() {
          return estatusJuridico;
    }

    public void setEstatusJuridico(String estatusJuridico) {
          this.estatusJuridico = estatusJuridico;
    }

    public String getExpedienteProveedor() {
          return expedienteProveedor;
    }

    public void setExpedienteProveedor(String expedienteProveedor) {
          this.expedienteProveedor = expedienteProveedor;
    }

    public Short getVehiculoDetenido() {
          return vehiculoDetenido;
    }

    public void setVehiculoDetenido(Short vehiculoDetenido) {
          this.vehiculoDetenido = vehiculoDetenido;
    }

    public String getEstatusVehiculo() {
          return estatusVehiculo;
    }

    public void setEstatusVehiculo(String estatusVehiculo) {
          this.estatusVehiculo = estatusVehiculo;
    }

    public ServicioSiniestro getAbogado() {
          return abogado;
    }

    public void setAbogado(ServicioSiniestro abogado) {
          this.abogado = abogado;
    }

    public String getTipoConclusion() {
          return tipoConclusion;
    }

    public void setTipoConclusion(String tipoConclusion) {
          this.tipoConclusion = tipoConclusion;
    }

    public Date getFechaTurnoDe() {
          return fechaTurnoDe;
    }

    public void setFechaTurnoDe(Date fechaTurnoDe) {
          this.fechaTurnoDe = fechaTurnoDe;
    }

    public Date getFechaTurnoHasta() {
          return fechaTurnoHasta;
    }

    public void setFechaTurnoHasta(Date fechaTurnoHasta) {
          this.fechaTurnoHasta = fechaTurnoHasta;
    }

    public Date getFechaConclusionLegalDe() {
          return fechaConclusionLegalDe;
    }

    public void setFechaConclusionLegalDe(Date fechaConclusionLegalDe) {
          this.fechaConclusionLegalDe = fechaConclusionLegalDe;
    }

    public Date getFechaConclusionLegalHasta() {
          return fechaConclusionLegalHasta;
    }

    public void setFechaConclusionLegalHasta(Date fechaConclusionLegalHasta) {
          this.fechaConclusionLegalHasta = fechaConclusionLegalHasta;
    }

    public Date getFechaConclusionAfirmeDe() {
          return fechaConclusionAfirmeDe;
    }

    public void setFechaConclusionAfirmeDe(Date fechaConclusionAfirmeDe) {
          this.fechaConclusionAfirmeDe = fechaConclusionAfirmeDe;
    }

    public Date getFechaConclusionAfirmeHasta() {
          return fechaConclusionAfirmeHasta;
    }

    public void setFechaConclusionAfirmeHasta(Date fechaConclusionAfirmeHasta) {
          this.fechaConclusionAfirmeHasta = fechaConclusionAfirmeHasta;
    }

    @Override
    public String toString() {
          return "ExpedienteJuridicoDTO [numeroSiniestro=" + numeroSiniestro
                      + ", motivoTurno=" + motivoTurno + ", estatusJuridico="
                      + estatusJuridico + ", expedienteProveedor="
                      + expedienteProveedor + ", vehiculoDetenido="
                      + vehiculoDetenido + ", estatusVehiculo=" + estatusVehiculo
                      + ", abogado=" + abogado + ", tipoConclusion=" + tipoConclusion
                      + ", fechaTurnoDe=" + fechaTurnoDe + ", fechaTurnoHasta="
                      + fechaTurnoHasta + ", fechaConclusionLegalDe="
                      + fechaConclusionLegalDe + ", fechaConclusionLegalHasta="
                      + fechaConclusionLegalHasta + ", fechaConclusionAfirmeDe="
                      + fechaConclusionAfirmeDe + ", fechaConclusionAfirmeHasta="
                      + fechaConclusionAfirmeHasta + "]";
    }

    
}
