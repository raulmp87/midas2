package mx.com.afirme.midas2.dto.portal.seguros;

import java.io.Serializable;
import mx.com.afirme.midas2.domain.portal.seguros.Departamento;

public class Comentario implements Serializable {
	
	private static final long serialVersionUID = -1996828590669627031L;
	
	private String nombre;
	private String telefono;
	private String correo;
	private Departamento departamento;
	private String texto;
	private String asunto;
	private String estado;
	
	public Comentario() {
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
	public Departamento getDepartamento() {
		return departamento;
	}
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	
	public String getAsunto() {
		return asunto;
	}
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
}