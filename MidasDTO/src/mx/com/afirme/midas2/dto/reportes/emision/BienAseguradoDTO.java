package mx.com.afirme.midas2.dto.reportes.emision;

import mx.com.afirme.midas2.annotation.Exportable;

public class BienAseguradoDTO {

	private String poliza;
	private String inciso;
	private String endoso;
	private String clave;
	private String marca;
	private String modelo;
	private String descripcion;
	private String serie;
	private String motor;
	private String placa;
	private String tipouso;
	private String tiposervicio;
	private String ocupantes;
	private String conductor;
	private String cobertura;
	private String foliocorredor;
	
	@Exportable(columnName = "POLIZA", columnOrder = 0)
	public String getPoliza() {
		return poliza;
	}
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	@Exportable(columnName = "INCISO", columnOrder = 1)
	public String getInciso() {
		return inciso;
	}
	public void setInciso(String inciso) {
		this.inciso = inciso;
	}
	@Exportable(columnName = "ENDOSO", columnOrder = 2)
	public String getEndoso() {
		return endoso;
	}
	public void setEndoso(String endoso) {
		this.endoso = endoso;
	}
	@Exportable(columnName = "CLAVE", columnOrder = 3)
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	@Exportable(columnName = "MARCA", columnOrder = 4)
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	@Exportable(columnName = "MODELO", columnOrder = 5)
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	@Exportable(columnName = "DESCRIPCION", columnOrder = 6)
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	@Exportable(columnName = "SERIE", columnOrder = 7)
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	@Exportable(columnName = "MOTOR", columnOrder = 8)
	public String getMotor() {
		return motor;
	}
	public void setMotor(String motor) {
		this.motor = motor;
	}
	@Exportable(columnName = "PLACAS", columnOrder = 9)
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	@Exportable(columnName = "USO", columnOrder = 10)
	public String getTipouso() {
		return tipouso;
	}
	public void setTipouso(String tipouso) {
		this.tipouso = tipouso;
	}
	@Exportable(columnName = "SERVICIO", columnOrder = 11)
	public String getTiposervicio() {
		return tiposervicio;
	}
	public void setTiposervicio(String tiposervicio) {
		this.tiposervicio = tiposervicio;
	}
	@Exportable(columnName = "OCUPANTES", columnOrder = 12)
	public String getOcupantes() {
		return ocupantes;
	}
	public void setOcupantes(String ocupantes) {
		this.ocupantes = ocupantes;
	}
	@Exportable(columnName = "CONDUCTOR", columnOrder = 13)
	public String getConductor() {
		return conductor;
	}
	public void setConductor(String conductor) {
		this.conductor = conductor;
	}
	@Exportable(columnName = "COBERTURA", columnOrder = 14)
	public String getCobertura() {
		return cobertura;
	}
	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}
	@Exportable(columnName = "FOLIO CORREDOR", columnOrder = 15)
	public String getFoliocorredor() {
		return foliocorredor;
	}
	public void setFoliocorredor(String foliocorredor) {
		this.foliocorredor = foliocorredor;
	} 

}
