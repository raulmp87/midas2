package mx.com.afirme.midas2.dto.reportes.emision;

import mx.com.afirme.midas2.annotation.Exportable;

public class DocumentoDTO {

	private String ot;
	private String endoso;
	private String poliza;
	private String inciso;
	private String ramo;
	private String subramo;
	private String tipodocumento;
	private String iniciovigencia;
	private String finvigencia;
	private String pneta;
	private String recargos;
	private String descuento;
	private String derpoliza;
	private String iva;
	private String primatotal;
	private String formapago;
	private String moneda;
	private String claveagente;
	private String pctcomision;
	private String comision;
	private String apppaterno;
	private String appmaterno;
	private String nombre;
	private String rfc;
	private String estado;
	private String municipio;
	private String colonia;
	private String cp;
	private String calle;
	private String numext;
	private String numint;
	private String foliocorredor;

	@Exportable(columnName = "OT", columnOrder = 0)
	public String getOt() {
		return ot;
	}

	public void setOt(String ot) {
		this.ot = ot;
	}
	
	@Exportable(columnName = "POLIZA", columnOrder = 1)
	public String getPoliza() {
		return poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	
	@Exportable(columnName = "INCISO", columnOrder = 2)
	public String getInciso() {
		return inciso;
	}

	public void setInciso(String inciso) {
		this.inciso = inciso;
	}

	@Exportable(columnName = "ENDOSO", columnOrder = 3)
	public String getEndoso() {
		return endoso;
	}

	public void setEndoso(String endoso) {
		this.endoso = endoso;
	}

	
	@Exportable(columnName = "RAMO", columnOrder = 4)
	public String getRamo() {
		return ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}

	@Exportable(columnName = "SUBRAMO", columnOrder = 5)
	public String getSubramo() {
		return subramo;
	}

	public void setSubramo(String subramo) {
		this.subramo = subramo;
	}

	@Exportable(columnName = "TIPO DOCUMENTO", columnOrder = 6)
	public String getTipodocumento() {
		return tipodocumento;
	}

	public void setTipodocumento(String tipodocumento) {
		this.tipodocumento = tipodocumento;
	}

	@Exportable(columnName = "INICIO VIGENCIA", columnOrder = 7)
	public String getIniciovigencia() {
		return iniciovigencia;
	}

	public void setIniciovigencia(String iniciovigencia) {
		this.iniciovigencia = iniciovigencia;
	}

	@Exportable(columnName = "FIN VIGENCIA", columnOrder = 8)
	public String getFinvigencia() {
		return finvigencia;
	}

	public void setFinvigencia(String finvigencia) {
		this.finvigencia = finvigencia;
	}

	@Exportable(columnName = "P. NETA", columnOrder = 9)
	public String getPneta() {
		return pneta;
	}

	public void setPneta(String pneta) {
		this.pneta = pneta;
	}

	@Exportable(columnName = "RECARGOS", columnOrder = 10)
	public String getRecargos() {
		return recargos;
	}

	public void setRecargos(String recargos) {
		this.recargos = recargos;
	}
	
	@Exportable(columnName = "DESCUENTO", columnOrder = 11)
	public String getDescuento() {
		return descuento;
	}

	public void setDescuento(String descuento) {
		this.descuento = descuento;
	}
	
	@Exportable(columnName = "DER. POLIZA", columnOrder = 12)
	public String getDerpoliza() {
		return derpoliza;
	}

	public void setDerpoliza(String derpoliza) {
		this.derpoliza = derpoliza;
	}


	@Exportable(columnName = "IVA", columnOrder = 13)
	public String getIva() {
		return iva;
	}

	public void setIva(String iva) {
		this.iva = iva;
	}

	@Exportable(columnName = "P. TOTAL", columnOrder = 14)
	public String getPrimatotal() {
		return primatotal;
	}

	public void setPrimatotal(String primatotal) {
		this.primatotal = primatotal;
	}

	@Exportable(columnName = "FORMA PAGO", columnOrder = 15)
	public String getFormapago() {
		return formapago;
	}

	public void setFormapago(String formapago) {
		this.formapago = formapago;
	}

	@Exportable(columnName = "MONEDA", columnOrder = 16)
	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	@Exportable(columnName = "CLAVE AGENTE", columnOrder = 17)
	public String getClaveagente() {
		return claveagente;
	}

	public void setClaveagente(String claveagente) {
		this.claveagente = claveagente;
	}

	@Exportable(columnName = "% COMISION", columnOrder = 18)
	public String getPctcomision() {
		return pctcomision;
	}

	public void setPctcomision(String pctcomision) {
		this.pctcomision = pctcomision;
	}

	@Exportable(columnName = "COMISION", columnOrder = 19)
	public String getComision() {
		return comision;
	}

	public void setComision(String comision) {
		this.comision = comision;
	}

	@Exportable(columnName = "AP. PATERNO", columnOrder = 20)
	public String getApppaterno() {
		return apppaterno;
	}

	public void setApppaterno(String apppaterno) {
		this.apppaterno = apppaterno;
	}

	@Exportable(columnName = "AP. MATERNO", columnOrder = 21)
	public String getAppmaterno() {
		return appmaterno;
	}

	public void setAppmaterno(String appmaterno) {
		this.appmaterno = appmaterno;
	}

	@Exportable(columnName = "NOMBRE", columnOrder = 22)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Exportable(columnName = "RFC", columnOrder = 23)
	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	@Exportable(columnName = "ESTADO", columnOrder = 24)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Exportable(columnName = "MUNICIPIO", columnOrder = 25)
	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	@Exportable(columnName = "COLONIA", columnOrder = 26)
	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	@Exportable(columnName = "CP", columnOrder = 27)
	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	@Exportable(columnName = "CALLE", columnOrder = 28)
	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	@Exportable(columnName = "NUMERO EXT", columnOrder = 29)
	public String getNumext() {
		return numext;
	}

	public void setNumext(String numext) {
		this.numext = numext;
	}

	@Exportable(columnName = "NUMERO INT", columnOrder = 30)
	public String getNumint() {
		return numint;
	}

	public void setNumint(String numint) {
		this.numint = numint;
	}

	@Exportable(columnName = "FOLIO CORREDOR", columnOrder = 31)
	public String getFoliocorredor() {
		return foliocorredor;
	}

	public void setFoliocorredor(String foliocorredor) {
		this.foliocorredor = foliocorredor;
	}
}
