package mx.com.afirme.midas2.dto.reportes.emision;

import mx.com.afirme.midas2.annotation.Exportable;

public class ReciboDTO {

	private String poliza;
	private String inciso;
	private String endoso;
	private String recibo;
	private String moneda;
	private String iniciovigencia;
	private String finvigencia;
	private String primaneta;
	private String recargos;
	private String descuento;
	private String derpoliza;
	private String iva;
	private String primatotal;
	private String pctcomision;
	private String comision;
	
	@Exportable(columnName = "POLIZA", columnOrder = 0)
	public String getPoliza() {
		return poliza;
	}
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	@Exportable(columnName = "INCISO", columnOrder = 1)
	public String getInciso() {
		return inciso;
	}
	public void setInciso(String inciso) {
		this.inciso = inciso;
	}
	@Exportable(columnName = "ENDOSO", columnOrder = 2)
	public String getEndoso() {
		return endoso;
	}
	public void setEndoso(String endoso) {
		this.endoso = endoso;
	}
	@Exportable(columnName = "RECIBO", columnOrder = 3)
	public String getRecibo() {
		return recibo;
	}
	public void setRecibo(String recibo) {
		this.recibo = recibo;
	}
	@Exportable(columnName = "MONEDA", columnOrder = 4)
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	@Exportable(columnName = "INICIO VIGENCIA", columnOrder = 5)
	public String getIniciovigencia() {
		return iniciovigencia;
	}
	public void setIniciovigencia(String iniciovigencia) {
		this.iniciovigencia = iniciovigencia;
	}
	@Exportable(columnName = "FIN VIGENCIA", columnOrder = 6)
	public String getFinvigencia() {
		return finvigencia;
	}
	public void setFinvigencia(String finvigencia) {
		this.finvigencia = finvigencia;
	}
	@Exportable(columnName = "P. NETA", columnOrder = 7)
	public String getPrimaneta() {
		return primaneta;
	}
	public void setPrimaneta(String primaneta) {
		this.primaneta = primaneta;
	}
	@Exportable(columnName = "RECARGOS", columnOrder = 8)
	public String getRecargos() {
		return recargos;
	}
	public void setRecargos(String recargos) {
		this.recargos = recargos;
	}
	@Exportable(columnName = "DESCUENTO", columnOrder = 9)
	public String getDescuento() {
		return descuento;
	}
	public void setDescuento(String descuento) {
		this.descuento = descuento;
	}
	@Exportable(columnName = "DER. POLIZA", columnOrder = 10)
	public String getDerpoliza() {
		return derpoliza;
	}
	public void setDerpoliza(String derpoliza) {
		this.derpoliza = derpoliza;
	}
	@Exportable(columnName = "IVA", columnOrder = 11)
	public String getIva() {
		return iva;
	}
	public void setIva(String iva) {
		this.iva = iva;
	}
	@Exportable(columnName = "P. TOTAL", columnOrder = 12)
	public String getPrimatotal() {
		return primatotal;
	}
	public void setPrimatotal(String primatotal) {
		this.primatotal = primatotal;
	}
	@Exportable(columnName = "% COMISION", columnOrder = 13)
	public String getPctcomision() {
		return pctcomision;
	}
	public void setPctcomision(String pctcomision) {
		this.pctcomision = pctcomision;
	}

	@Exportable(columnName = "COMISION", columnOrder = 14)
	public String getComision() {
		return comision;
	}
	public void setComision(String comision) {
		this.comision = comision;
	}
	
	
}
