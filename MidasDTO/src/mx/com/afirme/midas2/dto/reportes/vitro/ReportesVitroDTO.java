package mx.com.afirme.midas2.dto.reportes.vitro;

import java.io.Serializable;

import javax.persistence.Transient;

import mx.com.afirme.midas2.annotation.Exportable;

/**
 * Clase que contend\u00e1 la informaci\u00f3n de 
 * un registro de los asegurados del negocio vitro.
 * 
 * @author AFIRME
 * 
 * @since 20160523
 * 
 * @version 1.0
 *
 */
public class ReportesVitroDTO implements Serializable{

	private static final long serialVersionUID = -255091309962394198L;
	
	private String numeroPoliza;
	private String nombreAsegurado;
	private String numeroEmpleado;
	private Double montoTotal;
	private Double montoApagar;
	private Long idRecibo;
	private Long numeroFolioRecibo;
	private Long idCotizacionSeycos;
	
	public ReportesVitroDTO() {
	}

	@Exportable(columnName = "NUMPOLIZASEYCOS", columnOrder = 0)
	@Transient
	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	@Exportable(columnName = "NOMBREASEGURADO", columnOrder = 1)
	@Transient
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}

	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}

	@Exportable(columnName = "NUMERO_EMPLEADO", columnOrder = 2)
	@Transient
	public String getNumeroEmpleado() {
		return numeroEmpleado;
	}

	public void setNumeroEmpleado(String numeroEmpleado) {
		this.numeroEmpleado = numeroEmpleado;
	}

	@Exportable(columnName = "MONTO_TOTAL", columnOrder = 3, format="$#,##0.00")
	@Transient
	public Double getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(Double montoTotal) {
		this.montoTotal = montoTotal;
	}

	@Exportable(columnName = "MONTO_A_PAGO", columnOrder = 4, format="$#,##0.00")
	@Transient
	public Double getMontoApagar() {
		return montoApagar;
	}

	public void setMontoApagar(Double montoApagar) {
		this.montoApagar = montoApagar;
	}

	@Exportable(columnName = "ID_RECIBO", columnOrder = 5)
	@Transient
	public Long getIdRecibo() {
		return idRecibo;
	}

	public void setIdRecibo(Long idReciho) {
		this.idRecibo = idReciho;
	}

	@Exportable(columnName = "NUM_FOLIO_RBO", columnOrder = 6)
	@Transient
	public Long getNumeroFolioRecibo() {
		return numeroFolioRecibo;
	}

	public void setNumeroFolioRecibo(Long numeroFolioRecibo) {
		this.numeroFolioRecibo = numeroFolioRecibo;
	}

	@Exportable(columnName = "ID_COTIZACION", columnOrder = 7)
	@Transient
	public Long getIdCotizacionSeycos() {
		return idCotizacionSeycos;
	}

	public void setIdCotizacionSeycos(Long idCotizacionSeycos) {
		this.idCotizacionSeycos = idCotizacionSeycos;
	}
}
