package mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico;

import java.io.Serializable;

public class CargaMasivaSPDetalleDTO implements Serializable {
	
	private static final long serialVersionUID = -8399663574742388174L;
	
	private Long idArchivo;
	private Long poliza;
	private String cliente;
	private String numSerie;
	private String estilo;
	private Long modelo;
	private Double primaTotal;
	private String estatus;
	private String codigoError;
	private Long idDetalle;
	

	public Long getIdDetalle() {
		return idDetalle;
	}

	public void setIdDetalle(Long idDetalle) {
		this.idDetalle = idDetalle;
	}

	private Integer posStart;
	private Integer count;
	private String orderByAtribute;
	
	
	public CargaMasivaSPDetalleDTO(){
		super();
	}
	
	public CargaMasivaSPDetalleDTO(Long idArchivo, Long poliza, String cliente,
			String numSerie, String estilo, Long modelo,
			Double primaTotal, String estatus, String codigoError, Long idDetalle) {
		super();
		this.idArchivo=idArchivo;
		this.poliza = poliza;
		this.cliente = cliente;
		this.numSerie = numSerie;
		this.estilo = estilo;
		this.modelo = modelo;
		this.primaTotal = primaTotal;
		this.estatus = estatus;
		this.codigoError = codigoError;
		this.idDetalle =idDetalle;
	}

	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public String getNumSerie() {
		return numSerie;
	}
	public void setNumSerie(String numSerie) {
		this.numSerie = numSerie;
	}
	public String getEstilo() {
		return estilo;
	}
	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}
	
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getCodigoError() {
		return codigoError;
	}
	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}

	
	public Integer getPosStart() {
		return posStart;
	}

	public void setPosStart(Integer posStart) {
		this.posStart = posStart;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getOrderByAtribute() {
		return orderByAtribute;
	}

	public void setOrderByAtribute(String orderByAtribute) {
		this.orderByAtribute = orderByAtribute;
	}

	public void setIdArchivo(Long idArchivo) {
		this.idArchivo = idArchivo;
	}

	public Long getIdArchivo() {
		return idArchivo;
	}

	public Long getPoliza() {
		return poliza;
	}

	public void setPoliza(Long poliza) {
		this.poliza = poliza;
	}

	public Long getModelo() {
		return modelo;
	}

	public void setModelo(Long modelo) {
		this.modelo = modelo;
	}

	public Double getPrimaTotal() {
		return primaTotal;
	}

	public void setPrimaTotal(Double primaTotal) {
		this.primaTotal = primaTotal;
	}


}
