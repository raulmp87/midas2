package mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;

public class ResumenCostosGeneralDTO implements Serializable {
	
	private static final long serialVersionUID = 9175665309993414014L;

	private ResumenCostosDTO caratula;
	
	private CotizacionDTO cotizacionDTO;
	
	private Map<BigDecimal, ResumenCostosDTO> incisos;

	public ResumenCostosDTO getCaratula() {
		return caratula;
	}

	public void setCaratula(ResumenCostosDTO caratula) {
		this.caratula = caratula;
	}

	public Map<BigDecimal, ResumenCostosDTO> getIncisos() {
		return incisos;
	}

	public void setIncisos(Map<BigDecimal, ResumenCostosDTO> incisos) {
		this.incisos = incisos;
	}

	public CotizacionDTO getCotizacionDTO() {
		return cotizacionDTO;
	}

	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}

	
}
