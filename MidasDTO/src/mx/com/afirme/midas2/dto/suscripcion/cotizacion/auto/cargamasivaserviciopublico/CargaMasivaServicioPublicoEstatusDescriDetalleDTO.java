package mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico;

import java.io.Serializable;
import java.util.List;

public class CargaMasivaServicioPublicoEstatusDescriDetalleDTO implements Serializable{

	private static final long serialVersionUID = -1118012110461695346L;
	
	private Long numCotizacion;
	private Long numPoliza;
	private Long idDetalle;
	private List<EstatusDescriDetalle> listEstatusDescri;
	
	
	
	public Long getNumCotizacion() {
		return numCotizacion;
	}
	public void setNumCotizacion(Long numCotizacion) {
		this.numCotizacion = numCotizacion;
	}
	public Long getNumPoliza() {
		return numPoliza;
	}
	public void setNumPoliza(Long numPoliza) {
		this.numPoliza = numPoliza;
	}
	
	public Long getIdDetalle() {
		return idDetalle;
	}
	public void setIdDetalle(Long idDetalle) {
		this.idDetalle = idDetalle;
	}
	
	public List<EstatusDescriDetalle> getListEstatusDescri() {
		return listEstatusDescri;
	}
	public void setListEstatusDescri(List<EstatusDescriDetalle> listEstatusDescri) {
		this.listEstatusDescri = listEstatusDescri;
	}

	public class EstatusDescriDetalle{
		private String estatusDetalle;
		private String descriEstatus;
		
		public EstatusDescriDetalle() {
		}
		
		public EstatusDescriDetalle(String estatusDetalle, String descriEstatus) {
			this.estatusDetalle = estatusDetalle;
			this.descriEstatus = descriEstatus;
		}
		
		public String getEstatusDetalle() {
			return estatusDetalle;
		}
		public void setEstatusDetalle(String estatusDetalle) {
			this.estatusDetalle = estatusDetalle;
		}
		
		public String getDescriEstatus() {
			return descriEstatus;
		}
		public void setDescriEstatus(String descriEstatus) {
			this.descriEstatus = descriEstatus;
		}
	}
}
