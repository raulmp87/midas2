package mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas2.domain.negocio.producto.formapago.NegocioFormaPago;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;


/**
 * @author amosomar
 *
 */
public class DatosCotizacionExpressDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<NegocioFormaPago> encabezados;
	private List<NegocioPaqueteSeccion> fila;
	private long totalEncabezados;
	private long totalFilas;
	
	public void setEncabezados(List<NegocioFormaPago> encabezados) {
		this.encabezados = encabezados;
	}
	public List<NegocioFormaPago> getEncabezados() {
		return encabezados;
	}
	public void setFila(List<NegocioPaqueteSeccion> fila) {
		this.fila = fila;
	}
	public List<NegocioPaqueteSeccion> getFila() {
		return fila;
	}
	public void setTotalEncabezados(long totalEncabezados) {
		this.totalEncabezados = totalEncabezados;
	}
	public long getTotalEncabezados() {
		return totalEncabezados;
	}
	public void setTotalFilas(long totalFilas) {
		this.totalFilas = totalFilas;
	}
	public long getTotalFilas() {
		return totalFilas;
	}
}
