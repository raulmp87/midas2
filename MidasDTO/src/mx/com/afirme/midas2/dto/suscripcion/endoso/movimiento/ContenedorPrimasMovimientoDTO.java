package mx.com.afirme.midas2.dto.suscripcion.endoso.movimiento;

import java.math.BigDecimal;

public 	class ContenedorPrimasMovimientoDTO {
	
	private BigDecimal valorMovimientoPropias = new BigDecimal(0);
	private BigDecimal valorMovimientoExternas = new BigDecimal(0);
	
	private BigDecimal valorRPFPropias = new BigDecimal(0);
	private BigDecimal valorRPFExternas = new BigDecimal(0);
	
	private BigDecimal valorBonificacionComisionPropias = new BigDecimal(0);
	private BigDecimal valorBonificacionComisionExternas = new BigDecimal(0);
	
	private BigDecimal valorBonificacionRPFPropias = new BigDecimal(0);
	private BigDecimal valorBonificacionRPFExternas = new BigDecimal(0);
	
	private BigDecimal valorComisionPropias = new BigDecimal(0);
	private BigDecimal valorComisionExternas = new BigDecimal(0);
	
	private BigDecimal valorComisionRPFPropias = new BigDecimal(0);
	private BigDecimal valorComisionRPFExternas = new BigDecimal(0);
	
	private BigDecimal valorDerechosPropias = new BigDecimal(0);
	private BigDecimal valorDerechosExternas = new BigDecimal(0);
	
	private BigDecimal valorIvaPropias = new BigDecimal(0);
	private BigDecimal valorIvaExternas = new BigDecimal(0);
	
	private BigDecimal valorSobreComisionAgentePropias = new BigDecimal(0);
	private BigDecimal valorSobreComisionAgenteExternas = new BigDecimal(0);

	private BigDecimal valorSobreComisionPromPropias = new BigDecimal(0);
	private BigDecimal valorSobreComisionPromExternas = new BigDecimal(0);
	
	private BigDecimal valorBonoAgentePropias = new BigDecimal(0);
	private BigDecimal valorBonoAgenteExternas = new BigDecimal(0);
	
	private BigDecimal valorBonoPromPropias = new BigDecimal(0);
	private BigDecimal valorBonoPromExternas = new BigDecimal(0);
	
	private BigDecimal valorCesionDerechosAgentePropias = new BigDecimal(0);
	private BigDecimal valorCesionDerechosAgenteExternas = new BigDecimal(0);
	
	private BigDecimal valorCesionDerechosPromPropias = new BigDecimal(0);
	private BigDecimal valorCesionDerechosPromExternas = new BigDecimal(0);
	
	private BigDecimal valorSobreComisionUDIAgentePropias = new BigDecimal(0);
	private BigDecimal valorSobreComisionUDIAgenteExternas = new BigDecimal(0);
	
	private BigDecimal valorSobreComisionUDIPromPropias = new BigDecimal(0);
	private BigDecimal valorSobreComisionUDIPromExternas = new BigDecimal(0);
	
	public void setValorMovimientoPropias(BigDecimal valorMovimientoPropias) {
		this.valorMovimientoPropias = valorMovimientoPropias;
	}
	public BigDecimal getValorMovimientoPropias() {
		return valorMovimientoPropias;
	}
	public void setValorMovimientoExternas(BigDecimal valorMovimientoExternas) {
		this.valorMovimientoExternas = valorMovimientoExternas;
	}
	public BigDecimal getValorMovimientoExternas() {
		return valorMovimientoExternas;
	}
	public void setValorRPFPropias(BigDecimal valorRPFPropias) {
		this.valorRPFPropias = valorRPFPropias;
	}
	public BigDecimal getValorRPFPropias() {
		return valorRPFPropias;
	}
	public void setValorRPFExternas(BigDecimal valorRPFExternas) {
		this.valorRPFExternas = valorRPFExternas;
	}
	public BigDecimal getValorRPFExternas() {
		return valorRPFExternas;
	}
	public void setValorBonificacionComisionPropias(
			BigDecimal valorBonificacionComisionPropias) {
		this.valorBonificacionComisionPropias = valorBonificacionComisionPropias;
	}
	public BigDecimal getValorBonificacionComisionPropias() {
		return valorBonificacionComisionPropias;
	}
	public void setValorBonificacionComisionExternas(
			BigDecimal valorBonificacionComisionExternas) {
		this.valorBonificacionComisionExternas = valorBonificacionComisionExternas;
	}
	public BigDecimal getValorBonificacionComisionExternas() {
		return valorBonificacionComisionExternas;
	}
	public void setValorBonificacionRPFPropias(
			BigDecimal valorBonificacionRPFPropias) {
		this.valorBonificacionRPFPropias = valorBonificacionRPFPropias;
	}
	public BigDecimal getValorBonificacionRPFPropias() {
		return valorBonificacionRPFPropias;
	}
	public void setValorBonificacionRPFExternas(
			BigDecimal valorBonificacionRPFExternas) {
		this.valorBonificacionRPFExternas = valorBonificacionRPFExternas;
	}
	public BigDecimal getValorBonificacionRPFExternas() {
		return valorBonificacionRPFExternas;
	}
	public void setValorComisionPropias(BigDecimal valorComisionPropias) {
		this.valorComisionPropias = valorComisionPropias;
	}
	public BigDecimal getValorComisionPropias() {
		return valorComisionPropias;
	}
	public void setValorComisionExternas(BigDecimal valorComisionExternas) {
		this.valorComisionExternas = valorComisionExternas;
	}
	public BigDecimal getValorComisionExternas() {
		return valorComisionExternas;
	}
	public void setValorComisionRPFPropias(BigDecimal valorComisionRPFPropias) {
		this.valorComisionRPFPropias = valorComisionRPFPropias;
	}
	public BigDecimal getValorComisionRPFPropias() {
		return valorComisionRPFPropias;
	}
	public void setValorComisionRPFExternas(BigDecimal valorComisionRPFExternas) {
		this.valorComisionRPFExternas = valorComisionRPFExternas;
	}
	public BigDecimal getValorComisionRPFExternas() {
		return valorComisionRPFExternas;
	}
	public void setValorDerechosPropias(BigDecimal valorDerechosPropias) {
		this.valorDerechosPropias = valorDerechosPropias;
	}
	public BigDecimal getValorDerechosPropias() {
		return valorDerechosPropias;
	}
	public void setValorDerechosExternas(BigDecimal valorDerechosExternas) {
		this.valorDerechosExternas = valorDerechosExternas;
	}
	public BigDecimal getValorDerechosExternas() {
		return valorDerechosExternas;
	}
	public void setValorIvaPropias(BigDecimal valorIvaPropias) {
		this.valorIvaPropias = valorIvaPropias;
	}
	public BigDecimal getValorIvaPropias() {
		return valorIvaPropias;
	}
	public void setValorIvaExternas(BigDecimal valorIvaExternas) {
		this.valorIvaExternas = valorIvaExternas;
	}
	public BigDecimal getValorIvaExternas() {
		return valorIvaExternas;
	}
	public void setValorSobreComisionAgentePropias(
			BigDecimal valorSobreComisionAgentePropias) {
		this.valorSobreComisionAgentePropias = valorSobreComisionAgentePropias;
	}
	public BigDecimal getValorSobreComisionAgentePropias() {
		return valorSobreComisionAgentePropias;
	}
	public void setValorSobreComisionAgenteExternas(
			BigDecimal valorSobreComisionAgenteExternas) {
		this.valorSobreComisionAgenteExternas = valorSobreComisionAgenteExternas;
	}
	public BigDecimal getValorSobreComisionAgenteExternas() {
		return valorSobreComisionAgenteExternas;
	}
	public void setValorSobreComisionPromPropias(
			BigDecimal valorSobreComisionPromPropias) {
		this.valorSobreComisionPromPropias = valorSobreComisionPromPropias;
	}
	public BigDecimal getValorSobreComisionPromPropias() {
		return valorSobreComisionPromPropias;
	}
	public void setValorSobreComisionPromExternas(
			BigDecimal valorSobreComisionPromExternas) {
		this.valorSobreComisionPromExternas = valorSobreComisionPromExternas;
	}
	public BigDecimal getValorSobreComisionPromExternas() {
		return valorSobreComisionPromExternas;
	}
	public void setValorBonoAgentePropias(BigDecimal valorBonoAgentePropias) {
		this.valorBonoAgentePropias = valorBonoAgentePropias;
	}
	public BigDecimal getValorBonoAgentePropias() {
		return valorBonoAgentePropias;
	}
	public void setValorBonoAgenteExternas(BigDecimal valorBonoAgenteExternas) {
		this.valorBonoAgenteExternas = valorBonoAgenteExternas;
	}
	public BigDecimal getValorBonoAgenteExternas() {
		return valorBonoAgenteExternas;
	}
	public void setValorBonoPromPropias(BigDecimal valorBonoPromPropias) {
		this.valorBonoPromPropias = valorBonoPromPropias;
	}
	public BigDecimal getValorBonoPromPropias() {
		return valorBonoPromPropias;
	}
	public void setValorBonoPromExternas(BigDecimal valorBonoPromExternas) {
		this.valorBonoPromExternas = valorBonoPromExternas;
	}
	public BigDecimal getValorBonoPromExternas() {
		return valorBonoPromExternas;
	}
	public void setValorCesionDerechosAgentePropias(
			BigDecimal valorCesionDerechosAgentePropias) {
		this.valorCesionDerechosAgentePropias = valorCesionDerechosAgentePropias;
	}
	public BigDecimal getValorCesionDerechosAgentePropias() {
		return valorCesionDerechosAgentePropias;
	}
	public void setValorCesionDerechosAgenteExternas(
			BigDecimal valorCesionDerechosAgenteExternas) {
		this.valorCesionDerechosAgenteExternas = valorCesionDerechosAgenteExternas;
	}
	public BigDecimal getValorCesionDerechosAgenteExternas() {
		return valorCesionDerechosAgenteExternas;
	}
	public void setValorCesionDerechosPromPropias(
			BigDecimal valorCesionDerechosPromPropias) {
		this.valorCesionDerechosPromPropias = valorCesionDerechosPromPropias;
	}
	public BigDecimal getValorCesionDerechosPromPropias() {
		return valorCesionDerechosPromPropias;
	}
	public void setValorCesionDerechosPromExternas(
			BigDecimal valorCesionDerechosPromExternas) {
		this.valorCesionDerechosPromExternas = valorCesionDerechosPromExternas;
	}
	public BigDecimal getValorCesionDerechosPromExternas() {
		return valorCesionDerechosPromExternas;
	}
	public void setValorSobreComisionUDIAgentePropias(
			BigDecimal valorSobreComisionUDIAgentePropias) {
		this.valorSobreComisionUDIAgentePropias = valorSobreComisionUDIAgentePropias;
	}
	public BigDecimal getValorSobreComisionUDIAgentePropias() {
		return valorSobreComisionUDIAgentePropias;
	}
	public void setValorSobreComisionUDIAgenteExternas(
			BigDecimal valorSobreComisionUDIAgenteExternas) {
		this.valorSobreComisionUDIAgenteExternas = valorSobreComisionUDIAgenteExternas;
	}
	public BigDecimal getValorSobreComisionUDIAgenteExternas() {
		return valorSobreComisionUDIAgenteExternas;
	}
	public void setValorSobreComisionUDIPromPropias(
			BigDecimal valorSobreComisionUDIPromPropias) {
		this.valorSobreComisionUDIPromPropias = valorSobreComisionUDIPromPropias;
	}
	public BigDecimal getValorSobreComisionUDIPromPropias() {
		return valorSobreComisionUDIPromPropias;
	}
	public void setValorSobreComisionUDIPromExternas(
			BigDecimal valorSobreComisionUDIPromExternas) {
		this.valorSobreComisionUDIPromExternas = valorSobreComisionUDIPromExternas;
	}
	public BigDecimal getValorSobreComisionUDIPromExternas() {
		return valorSobreComisionUDIPromExternas;
	}
}
