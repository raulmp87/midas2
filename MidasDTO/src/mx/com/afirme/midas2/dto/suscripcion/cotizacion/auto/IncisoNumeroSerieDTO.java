package mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto;

import java.io.Serializable;
import java.util.Date;

public class IncisoNumeroSerieDTO implements Serializable{

	private static final long serialVersionUID = -2438614200264650664L;

	private Long idCotizacion;
	
	private Long idSeccion;
	
	private Long numeroInciso;
	
	private String numeroSerie;
	
	private Date fechaIniVigencia;
	
	private Date fechaFinVigencia;
	
	private Long incisoContinuityId;

	public Long getIdCotizacion() {
		return idCotizacion;
	}

	public void setIdCotizacion(Long idCotizacion) {
		this.idCotizacion = idCotizacion;
	}

	public Long getIdSeccion() {
		return idSeccion;
	}

	public void setIdSeccion(Long idSeccion) {
		this.idSeccion = idSeccion;
	}

	public Long getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(Long numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public Date getFechaIniVigencia() {
		return fechaIniVigencia;
	}

	public void setFechaIniVigencia(Date fechaIniVigencia) {
		this.fechaIniVigencia = fechaIniVigencia;
	}

	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}

	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	public Long getIncisoContinuityId() {
		return incisoContinuityId;
	}

	public void setIncisoContinuityId(Long incisoContinuityId) {
		this.incisoContinuityId = incisoContinuityId;
	}
		
}
