package mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ResumenCostosDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/*
	 * 
	 * Valores para mostrar en jsp
	 */ 
	private Double primaNetaCoberturas = 0.0d; /*1 suma de las primas netas de todas la coberturas */ 
	//setTotalPrimas(totalPrimasNetas)
	private Double totalPrimas=0.0d; /*4 total de primas = prima neta de coberturas - descuento*/
	private Double descuentoComisionCedida=0.0d; /*Desc /comisi�n cedida = Total Primas * porcentaje de comisi�n cedida (cotizaciondto.porcentajebonifcomision, regla de tres con ComisionCotizacionDTO.PorcentajeComisionDefault)*/ 
	private Double recargo=0.0d; /*recargo = prima neta coberturas * % de recargo de la forma de pago (FormaPagoDN.getPorId)*/
	private Double derechos = 0.0d; /*derechos = monto de derechos seleccionado por el usuario (CotizacionDTO.getNegocioDerechoPoliza) / Total de coberturas propias*/
	private List<Integer> ivaList = new ArrayList<Integer>(); /*Listado de montos de iva validos*/
	private Integer valorIvaSeleccionado; /*valor correspondinte al porcentaje de iva para la cotizacion (cotizaciondto.getPorcentajeIva).*/
	private Double iva = 0.0d;
	private Double primaTotal = 0.0d; /*Prima total = prima neta de coberturas + recargos + derechos + iva (cotizacionDTO.porcentajeIva)*/


	/*
	 * Valores adicionales para calculos que no se muestran en jsp
	 */
	private Double primaNetaConberturasPropias= 0.0d; /*2 suma de primas netas de las coberturas sean propias*/
	private Double descuento = 0.0d; /*3 prima neta de coberturas propias * % descto global (cotizacion.porcentajeDescuentoGlobal)*/
	
	private Double sobreComision = 0.0d;/*
	 									*si aplica sobre comisi�n en el negocio , c�lcular la sobrecomisi�n decuerdo a las siguientes condici�nes (negocio.CLAVEMANEJAUDI IS '0:No, 1:Si'):
	 									*	Si aplica sobre porcentaje: sobrecomisi�n = prima neta de cotizaci�n* % de sobrecomisi�n del negocio (negocio.CLAVEPORCENTAJEMONTOUDI IS '1:Porcentaje)
	 									*	Si aplica sobre comisi�n en monto: sobrecomisi�n = monto de sobrecomisi�n del negocio (negocio.CLAVEPORCENTAJEMONTOUDI IS �2:Monto';)
	 									*/
	private Double comision= 0.0d; /*comisi�n = (Prima Neta de Coberturas - sobrecomisi�n ) * % de comisi�n (ComisionCotizacionDTO.PorcentajeComisionDefault)*/
	private Double comisionSobreRecargo = 0.0d; /*comisi�n sobre recargo = comisi�n * % de recargo de la forma de pago (FormaPagoDN.getPorId)*/
	
	//Se agregan dos propiedad para mostrar el valor del porcentaje de iva de la cotizacion durante los endosos
	//y a la vez controlar la manera en la que se muestran
	private Double porcentajeIva = 16d;
	private boolean readOnly = true;
	
	private boolean statusExcepcion = false;//JFGG
	private boolean showExcepcion = false;//JFGG
	private String mensajeExcepcion = null;//JFGG
	
	
	public ResumenCostosDTO(){
		ivaList.add(16);
		//ivaList.add(11); //Ajuste de IVA 20140101
	}



	public Double getPrimaNetaCoberturas() {
		return primaNetaCoberturas;
	}



	public void setPrimaNetaCoberturas(Double primaNetaCoberturas) {
		this.primaNetaCoberturas = primaNetaCoberturas;
	}



	public Double getTotalPrimas() {
		return totalPrimas;
	}



	public void setTotalPrimas(Double totalPrimas) {
		this.totalPrimas = totalPrimas;
	}



	public Double getDescuentoComisionCedida() {
		return descuentoComisionCedida;
	}



	public void setDescuentoComisionCedida(Double descuentoComisionCedidad) {
		this.descuentoComisionCedida = descuentoComisionCedidad;
	}



	public Double getRecargo() {
		return recargo;
	}



	public void setRecargo(Double recargo) {
		this.recargo = recargo;
	}



	public Double getDerechos() {
		return derechos;
	}



	public void setDerechos(Double derechos) {
		this.derechos = derechos;
	}



	public Integer getValorIvaSeleccionado() {
		return valorIvaSeleccionado;
	}



	public void setValorIvaSeleccionado(Integer valorIvaSeleccionado) {
		this.valorIvaSeleccionado = valorIvaSeleccionado;
	}



	public Double getPrimaTotal() {
		return primaTotal;
	}



	public void setPrimaTotal(Double primaTotal) {
		this.primaTotal = primaTotal;
	}



	public Double getPrimaNetaConberturasPropias() {
		return primaNetaConberturasPropias;
	}



	public void setPrimaNetaConberturasPropias(Double primaNetaConberturasPropias) {
		this.primaNetaConberturasPropias = primaNetaConberturasPropias;
	}



	public Double getDescuento() {
		return descuento;
	}



	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}



	public Double getSobreComision() {
		return sobreComision;
	}



	public void setSobreComision(Double sobreComision) {
		this.sobreComision = sobreComision;
	}



	public Double getComision() {
		return comision;
	}



	public void setComision(Double comision) {
		this.comision = comision;
	}



	public Double getComisionSobreRecargo() {
		return comisionSobreRecargo;
	}



	public void setComisionSobreRecargo(Double comisionSobreRecargo) {
		this.comisionSobreRecargo = comisionSobreRecargo;
	}



	public Double getPorcentajeIva() {
		return porcentajeIva;
	}



	public void setPorcentajeIva(Double porcentajeIva) {
		this.porcentajeIva = porcentajeIva;
	}



	public boolean getReadOnly() {
		return readOnly;
	}



	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}



	public List<Integer> getIvaList() {
		return ivaList;
	}
	
	public Double getIva() {
		return iva;
	}

	public void setIva(Double iva) {
		this.iva = iva;
	}
	/**
	 * JFGG
	 * Estatus de resultado de la evaluacio de Excepciones
	 * @return boolean
	 */
	public boolean isStatusExcepcion() {
		return statusExcepcion;
	}
	/**
	 * JFGG
	 * Estatus de resultado de la evaluacio de Excepciones
	 * @param statusExcepcion
	 */
	public void setStatusExcepcion(boolean statusExcepcion) {
		this.statusExcepcion = statusExcepcion;
	}
	/**
	 * JFGG
	 * Peticion mostrara resultado de Excepcion
	 * @return boolean
	 */
	public boolean isShowExcepcion() {
		return showExcepcion;
	}
	/**
	 * JFGG
	 * Peticion mostrara resultado de Excepcion
	 * @param showExcepcion
	 */
	public void setShowExcepcion(boolean showExcepcion) {
		this.showExcepcion = showExcepcion;
	}
	/**
	 * JFGG
	 * Mensjae de Excepcion a mostrar 
	 * @return String
	 */
	public String getMensajeExcepcion() {
		return mensajeExcepcion;
	}
	/**
	 * JFGG
	 * Mensjae de Excepcion a mostrar 
	 * @param mensajeExcepcion
	 */
	public void setMensajeExcepcion(String mensajeExcepcion) {
		this.mensajeExcepcion = mensajeExcepcion;
	}
}
