package mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import mx.com.afirme.midas2.util.StringUtil;

import org.apache.log4j.Logger;


public class BitacoraCargaMasivaSPResultadoDTO implements Serializable {

	
	private static final long serialVersionUID = -512569944205310217L;

	public static final Logger LOG = Logger.getLogger(BitacoraCargaMasivaSPResultadoDTO.class);
	
	private Long id;
	private String poliza;
	private String numSerie;
	private String estatus;
	private String usuarioCarga;
	private Date fechaCarga;
	private Date fechaCargaFinal;
	private String descripcionError;
	
	public BitacoraCargaMasivaSPResultadoDTO(){
		super();
	}
	

	public BitacoraCargaMasivaSPResultadoDTO(Long id, String poliza,
			String numSerie,  Date fechaCarga,
			String usuarioCarga, String estatus, String descripcionError) {
		super();
		this.id = id;
		this.poliza = poliza;
		this.numSerie = numSerie;
		this.estatus=estatus;
		this.usuarioCarga= usuarioCarga;
		this.fechaCarga=fechaCarga;
		this.descripcionError=descripcionError;
		
	}
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPoliza() {
		return poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	public String getNumSerie() {
		return numSerie;
	}

	public void setNumSerie(String numSerie) {
		this.numSerie = numSerie;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getUsuarioCarga() {
		return usuarioCarga;
	}

	public void setUsuarioCarga(String usuarioCarga) {
		this.usuarioCarga = usuarioCarga;
	}

	public Date getFechaCarga() {
		return fechaCarga;
	}

	public void setFechaCarga(Date fechaCarga) {
		this.fechaCarga = fechaCarga;
	}
	
	public void setFechaCarga(String fechaCarga) {
		try{
			if(!StringUtil.isEmpty(fechaCarga)){
				DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
				this.fechaCarga = format.parse(fechaCarga);
			}
		}catch(Exception e){
			LOG.error(e);
			this.fechaCarga = null;
		}		
	}


	public String getDescripcionError() {
		return descripcionError;
	}

	public void setDescripcionError(String descripcionError) {
		this.descripcionError = descripcionError;
	}

	public void setFechaCargaFinal(Date fechaCargaFinal) {
		this.fechaCargaFinal = fechaCargaFinal;
	}
	
	public void setFechaCargaFinal(String fechaCargaFinal) {
		try{
			if(!StringUtil.isEmpty(fechaCargaFinal)){
				DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
				this.fechaCargaFinal = format.parse(fechaCargaFinal);
			}
		}catch(Exception e){
			LOG.error(e);
			this.fechaCargaFinal = null;
		}		
	}

	public Date getFechaCargaFinal() {
		return fechaCargaFinal;
	}


	



}
