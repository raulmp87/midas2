package mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import mx.com.afirme.midas2.validator.group.NewItemChecks;

public class CaracteristicasVehiculoDTO implements Serializable {

	private static final long serialVersionUID = -1834418075379105734L;

	String descripcion;
	String valorSeleccionado;
	Map <String, String> listadoVariablesModificadoras = new LinkedHashMap<String, String>();
	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}	
	
	@Size(min=1, max=100, groups=NewItemChecks.class, message="{com.afirme.midas2.requerido}")
	public String getValorSeleccionado() {
		return valorSeleccionado;
	}
	public void setValorSeleccionado(String valorSeleccionado) {
		this.valorSeleccionado = valorSeleccionado.trim();
	}
	public Map<String, String> getListadoVariablesModificadoras() {
		return listadoVariablesModificadoras;
	}
	public void setListadoVariablesModificadoras(
			Map<String, String> listadoVariablesModificadoras) {
		this.listadoVariablesModificadoras = listadoVariablesModificadoras;
	}
	
}
