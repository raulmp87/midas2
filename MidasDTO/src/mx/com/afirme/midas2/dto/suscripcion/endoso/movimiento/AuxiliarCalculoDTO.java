package mx.com.afirme.midas2.dto.suscripcion.endoso.movimiento;

import java.math.BigDecimal;
import java.util.Collection;

import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.comision.BitemporalComision;

public class AuxiliarCalculoDTO {

	private Collection<BitemporalComision> comisiones;
	private Double porcentajeComision;
	private Double porcentajeFormaPago;
	private Double porcentajeFormaPagoAnterior;
	private Double porcentajeBonificacionComision;
	private Double porcentajeIVA;
	private BigDecimal valorDerechosEndoso;
	private BigDecimal valorDerechosPoliza;
	private Boolean aplicaDerechosPoliza;
	private Boolean aplicaDerechosEndoso;
	private Integer diasVigencia;
	private BigDecimal porcentajeUDI;
	private BigDecimal porcentajeSobreComision;
	private BigDecimal porcentajeSobreComisionAgente;
	private BigDecimal porcentajeSobreComisionProm;
	private BigDecimal porcentajeBono;
	private BigDecimal porcentajeBonoAgente;
	private BigDecimal porcentajeBonoProm;
	private BigDecimal porcentajeCesionDerechos;
	private BigDecimal porcentajeCesionDerechosAgente;
	private BigDecimal porcentajeCesionDerechosProm;
	private BigDecimal porcentajeSobreComisionUDI;
	private BigDecimal porcentajeSobreComisionUDIAgente;
	private BigDecimal porcentajeSobreComisionUDIProm;
	private BigDecimal importeCesionDerechos;
	
	public Collection<BitemporalComision> getComisiones() {
		return comisiones;
	}
	public void setComisiones(Collection<BitemporalComision> comisiones) {
		this.comisiones = comisiones;
	}
	public Double getPorcentajeComision() {
		return porcentajeComision;
	}
	public void setPorcentajeComision(Double porcentajeComision) {
		this.porcentajeComision = porcentajeComision;
	}
	public Double getPorcentajeFormaPago() {
		return porcentajeFormaPago;
	}
	public void setPorcentajeFormaPago(Double porcentajeFormaPago) {
		this.porcentajeFormaPago = porcentajeFormaPago;
	}
	public Double getPorcentajeFormaPagoAnterior() {
		return porcentajeFormaPagoAnterior;
	}
	public void setPorcentajeFormaPagoAnterior(Double porcentajeFormaPagoAnterior) {
		this.porcentajeFormaPagoAnterior = porcentajeFormaPagoAnterior;
	}
	public Double getPorcentajeBonificacionComision() {
		return porcentajeBonificacionComision;
	}
	public void setPorcentajeBonificacionComision(
			Double porcentajeBonificacionComision) {
		this.porcentajeBonificacionComision = porcentajeBonificacionComision;
	}
	public Double getPorcentajeIVA() {
		return porcentajeIVA;
	}
	public void setPorcentajeIVA(Double porcentajeIVA) {
		this.porcentajeIVA = porcentajeIVA;
	}
	public BigDecimal getValorDerechosEndoso() {
		return valorDerechosEndoso;
	}
	public void setValorDerechosEndoso(BigDecimal valorDerechosEndoso) {
		this.valorDerechosEndoso = valorDerechosEndoso;
	}
	public BigDecimal getValorDerechosPoliza() {
		return valorDerechosPoliza;
	}
	public void setValorDerechosPoliza(BigDecimal valorDerechosPoliza) {
		this.valorDerechosPoliza = valorDerechosPoliza;
	}
	public Boolean getAplicaDerechosPoliza() {
		return aplicaDerechosPoliza;
	}
	public void setAplicaDerechosPoliza(Boolean aplicaDerechosPoliza) {
		this.aplicaDerechosPoliza = aplicaDerechosPoliza;
	}
	public Boolean getAplicaDerechosEndoso() {
		return aplicaDerechosEndoso;
	}
	public void setAplicaDerechosEndoso(Boolean aplicaDerechosEndoso) {
		this.aplicaDerechosEndoso = aplicaDerechosEndoso;
	}
	public Integer getDiasVigencia() {
		return diasVigencia;
	}
	public void setDiasVigencia(Integer diasVigencia) {
		this.diasVigencia = diasVigencia;
	}
	public BigDecimal getPorcentajeUDI() {
		return porcentajeUDI;
	}
	public void setPorcentajeUDI(BigDecimal porcentajeUDI) {
		this.porcentajeUDI = porcentajeUDI;
	}
	public BigDecimal getPorcentajeSobreComision() {
		return porcentajeSobreComision;
	}
	public void setPorcentajeSobreComision(BigDecimal porcentajeSobreComision) {
		this.porcentajeSobreComision = porcentajeSobreComision;
	}
	public BigDecimal getPorcentajeSobreComisionAgente() {
		return porcentajeSobreComisionAgente;
	}
	public void setPorcentajeSobreComisionAgente(
			BigDecimal porcentajeSobreComisionAgente) {
		this.porcentajeSobreComisionAgente = porcentajeSobreComisionAgente;
	}
	public BigDecimal getPorcentajeSobreComisionProm() {
		return porcentajeSobreComisionProm;
	}
	public void setPorcentajeSobreComisionProm(
			BigDecimal porcentajeSobreComisionProm) {
		this.porcentajeSobreComisionProm = porcentajeSobreComisionProm;
	}
	public BigDecimal getPorcentajeBono() {
		return porcentajeBono;
	}
	public void setPorcentajeBono(BigDecimal porcentajeBono) {
		this.porcentajeBono = porcentajeBono;
	}
	public BigDecimal getPorcentajeBonoAgente() {
		return porcentajeBonoAgente;
	}
	public void setPorcentajeBonoAgente(BigDecimal porcentajeBonoAgente) {
		this.porcentajeBonoAgente = porcentajeBonoAgente;
	}
	public BigDecimal getPorcentajeBonoProm() {
		return porcentajeBonoProm;
	}
	public void setPorcentajeBonoProm(BigDecimal porcentajeBonoProm) {
		this.porcentajeBonoProm = porcentajeBonoProm;
	}
	public BigDecimal getPorcentajeCesionDerechos() {
		return porcentajeCesionDerechos;
	}
	public void setPorcentajeCesionDerechos(BigDecimal porcentajeCesionDerechos) {
		this.porcentajeCesionDerechos = porcentajeCesionDerechos;
	}
	public BigDecimal getPorcentajeCesionDerechosAgente() {
		return porcentajeCesionDerechosAgente;
	}
	public void setPorcentajeCesionDerechosAgente(
			BigDecimal porcentajeCesionDerechosAgente) {
		this.porcentajeCesionDerechosAgente = porcentajeCesionDerechosAgente;
	}
	public BigDecimal getPorcentajeCesionDerechosProm() {
		return porcentajeCesionDerechosProm;
	}
	public void setPorcentajeCesionDerechosProm(
			BigDecimal porcentajeCesionDerechosProm) {
		this.porcentajeCesionDerechosProm = porcentajeCesionDerechosProm;
	}
	public BigDecimal getPorcentajeSobreComisionUDI() {
		return porcentajeSobreComisionUDI;
	}
	public void setPorcentajeSobreComisionUDI(BigDecimal porcentajeSobreComisionUDI) {
		this.porcentajeSobreComisionUDI = porcentajeSobreComisionUDI;
	}
	public BigDecimal getPorcentajeSobreComisionUDIAgente() {
		return porcentajeSobreComisionUDIAgente;
	}
	public void setPorcentajeSobreComisionUDIAgente(
			BigDecimal porcentajeSobreComisionUDIAgente) {
		this.porcentajeSobreComisionUDIAgente = porcentajeSobreComisionUDIAgente;
	}
	public BigDecimal getPorcentajeSobreComisionUDIProm() {
		return porcentajeSobreComisionUDIProm;
	}
	public void setPorcentajeSobreComisionUDIProm(
			BigDecimal porcentajeSobreComisionUDIProm) {
		this.porcentajeSobreComisionUDIProm = porcentajeSobreComisionUDIProm;
	}
	public BigDecimal getImporteCesionDerechos() {
		return importeCesionDerechos;
	}
	public void setImporteCesionDerechos(BigDecimal importeCesionDerechos) {
		this.importeCesionDerechos = importeCesionDerechos;
	}
	
}
