package mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.complementar.cobranzaInciso;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;

public class CobranzaIncisoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6329733376617925058L;

	private IncisoCotizacionDTO inciso;
	
	private List<MedioPagoDTO> mediosPago;
	
	private Map<Long, String> conductosCobro;
	
	private Map<BigDecimal, String> clientesCobro;
	
	public void setInciso(IncisoCotizacionDTO inciso) {
		this.inciso = inciso;
	}
	public IncisoCotizacionDTO getInciso() {
		return inciso;
	}
	public void setMediosPago(List<MedioPagoDTO> mediosPago) {
		this.mediosPago = mediosPago;
	}
	public List<MedioPagoDTO> getMediosPago() {
		return mediosPago;
	}
	public void setConductosCobro(Map<Long, String> conductosCobro) {
		this.conductosCobro = conductosCobro;
	}
	public Map<Long, String> getConductosCobro() {
		return conductosCobro;
	}
	public void setClientesCobro(Map<BigDecimal, String> clientesCobro) {
		this.clientesCobro = clientesCobro;
	}
	public Map<BigDecimal, String> getClientesCobro() {
		return clientesCobro;
	}
	
}
