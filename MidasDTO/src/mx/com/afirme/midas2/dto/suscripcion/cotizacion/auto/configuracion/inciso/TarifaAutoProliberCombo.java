package mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso;

import java.io.Serializable;

import mx.com.afirme.midas.base.CacheableDTO;

public class TarifaAutoProliberCombo extends CacheableDTO implements
		Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2752780595757444532L;
	private String claveGrupoProliber;

	public String getClaveGrupoProliber() {
		return claveGrupoProliber;
	}

	public void setClaveGrupoProliber(String claveGrupoProliber) {
		this.claveGrupoProliber = claveGrupoProliber;
	}

	@Override
	public Object getId() {
		return this.claveGrupoProliber;
	}

	@Override
	public String getDescription() {
		return this.claveGrupoProliber;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TarifaAutoProliberCombo other = (TarifaAutoProliberCombo) obj;
		if (claveGrupoProliber == null) {
			if (other.claveGrupoProliber != null)
				return false;
		} else if (!claveGrupoProliber.equals(other.claveGrupoProliber))
			return false;
		return true;
	}

}
