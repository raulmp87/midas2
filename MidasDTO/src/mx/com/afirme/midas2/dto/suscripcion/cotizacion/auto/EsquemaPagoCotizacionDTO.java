package mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto;

import java.io.Serializable;

import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;

public class EsquemaPagoCotizacionDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private FormaPagoDTO formaPagoDTO;
	private ImporteCotizacionDTO pagoInicial;
	private ImporteCotizacionDTO pagoSubsecuente;
	private Integer cantidad;
	
	public void setPagoInicial(ImporteCotizacionDTO pagoInicial) {
		this.pagoInicial = pagoInicial;
	}
	public ImporteCotizacionDTO getPagoInicial() {
		return pagoInicial;
	}
	public void setPagoSubsecuente(ImporteCotizacionDTO pagoSubsecuente) {
		this.pagoSubsecuente = pagoSubsecuente;
	}
	public ImporteCotizacionDTO getPagoSubsecuente() {
		return pagoSubsecuente;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setFormaPagoDTO(FormaPagoDTO formaPagoDTO) {
		this.formaPagoDTO = formaPagoDTO;
	}
	public FormaPagoDTO getFormaPagoDTO() {
		return formaPagoDTO;
	}
	

}
