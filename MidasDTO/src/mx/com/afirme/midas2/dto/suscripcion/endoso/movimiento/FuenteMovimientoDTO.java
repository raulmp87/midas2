package mx.com.afirme.midas2.dto.suscripcion.endoso.movimiento;

import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.TipoMovimientoEndoso;

public class FuenteMovimientoDTO {

	private TipoMovimientoEndoso tipoMovimientoEndoso;
	
	private Object source;
	
	private Object target;
	
	private EndosoIDTO validacionInciso;
	
	public FuenteMovimientoDTO() {
	}

	public FuenteMovimientoDTO(TipoMovimientoEndoso tipoMovimientoEndoso,
			Object source, Object target) {
		this.tipoMovimientoEndoso = tipoMovimientoEndoso;
		this.source = source;
		this.target = target;
	}

	public TipoMovimientoEndoso getTipoMovimientoEndoso() {
		return tipoMovimientoEndoso;
	}

	public void setTipoMovimientoEndoso(TipoMovimientoEndoso tipoMovimientoEndoso) {
		this.tipoMovimientoEndoso = tipoMovimientoEndoso;
	}

	public Object getSource() {
		return source;
	}

	public void setSource(Object source) {
		this.source = source;
	}

	public Object getTarget() {
		return target;
	}

	public void setTarget(Object target) {
		this.target = target;
	}

	public EndosoIDTO getValidacionInciso() {
		return validacionInciso;
	}

	public void setValidacionInciso(EndosoIDTO validacionInciso) {
		this.validacionInciso = validacionInciso;
	}
	
	
	
}
