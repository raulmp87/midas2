package mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico;

import java.io.Serializable;

public class CoberturaDetalleDTO implements Serializable{

	private static final long serialVersionUID = -259747960435678997L;

	private Long idToCobertura;
	private Double valor;
	private int tipoValor;
			/*
			 * 1 - Deducible
			 * 2 - Suma asegurada (Límite)
			 */
	
	public CoberturaDetalleDTO() {
		super();
	}
	
	public CoberturaDetalleDTO(Long idToCobertura, Double valor,
			int tipoValor) {
		super();
		this.idToCobertura = idToCobertura;
		this.valor = valor;
		this.tipoValor = tipoValor;
	}

	public Long getIdToCobertura() {
		return idToCobertura;
	}
	public void setIdToCobertura(Long idToCobertura) {
		this.idToCobertura = idToCobertura;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public int getTipoValor() {
		return tipoValor;
	}
	public void setTipoValor(int tipoValor) {
		this.tipoValor = tipoValor;
	}
}
