package mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;

public class TerminarCotizacionDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static enum Estatus{
		OK((short)0), ERROR_AGENTE((short)1), ERROR_MONTO((short)2), 
		ERROR_EXCEPCIONES((short)3), ERROR_SOLICITUDES((short)4), 
		ERROR_INCISOS_INSUFICIENTES((short)5), ERROR_COBRANZA((short)5), 
		ERROR_DATOS_COBRANZA((short)6), ERROR_AGRUPACION_RECIBOS((short)7),
		ERROR_MAX_DESCUENTO_POR_ESTADO((short)8);		
		Estatus(Short estatus) {
			this.estatus = estatus;
		}
		private Short estatus;
		public Short getEstatus(){
			return this.estatus;
		}
	};	
	
	private short estatus;
	private List<ExcepcionSuscripcionReporteDTO> excepcionesList;
	private String mensajeError;
	public void setEstatus(short estatus) {
		this.estatus = estatus;
	}
	public short getEstatus() {
		return estatus;
	}
	public void setExcepcionesList(List<ExcepcionSuscripcionReporteDTO> excepcionesList) {
		this.excepcionesList = excepcionesList;
	}
	public List<ExcepcionSuscripcionReporteDTO> getExcepcionesList() {
		return excepcionesList;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	
	public String getMensajeErrorPlano() {
		String mensajeErroPlano = mensajeError.replaceAll("<ul>", "");
		mensajeErroPlano = mensajeErroPlano.replaceAll("</ul>", "");
		mensajeErroPlano = mensajeErroPlano.replaceAll("<li>", "");
		mensajeErroPlano = mensajeErroPlano.replaceAll("</li>", "");
		
		return mensajeErroPlano;
	}

}
