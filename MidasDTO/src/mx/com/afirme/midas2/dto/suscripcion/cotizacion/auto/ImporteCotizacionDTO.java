package mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class ImporteCotizacionDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal numExhibicion;
	private BigDecimal importe;
	private String importeFormat;
	
	public void setNumExhibicion(BigDecimal numExhibicion) {
		this.numExhibicion = numExhibicion;
	}
	public BigDecimal getNumExhibicion() {
		return numExhibicion;
	}
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}
	public BigDecimal getImporte() {
		return importe;
	}

	public String getImporteFormat() {
		NumberFormat formatter = new DecimalFormat("$###,###,###.00");
		return formatter.format(importe);
	}

}
