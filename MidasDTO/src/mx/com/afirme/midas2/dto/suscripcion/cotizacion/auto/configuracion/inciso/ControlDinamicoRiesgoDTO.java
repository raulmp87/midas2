package mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso;

import java.io.Serializable;
import java.util.Map;

public class ControlDinamicoRiesgoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String etiqueta;
	private Integer tipoControl;
	private Integer tipoValidador;
	private Boolean visible;
	private Map<String, String> lista;
	private String valor;
	private String dependencia;
	private Integer tipoDependencia;
	private String idNivel;
	private String descripcionNivel;
	private Boolean deshabilitado;
	private Short idGrupoValores;

	// Tipo Control
	public static final int TIPO_CONTROL_SELECT_CATALOGO_PROPIO = 1;
	public static final int TIPO_CONTROL_SELECT_VALORES_FIJOS = 2;
	public static final int TIPO_CONTROL_TEXTFIELD = 3;
	public static final int TIPO_CONTROL_TEXTAREA = 4;
	public static final int TIPO_CONTROL_CHECKBOX = 5;
	
	// Tipo Validador
	public static final int TIPO_VALIDADOR_DECIMAL_OCHO_CUATRO = 1;
	public static final int TIPO_VALIDADOR_DECIMAL_OCHO_DOS = 2;
	public static final int TIPO_VALIDADOR_DECIMAL_DIECISEIS_DOS = 3;
	public static final int TIPO_VALIDADOR_DECIMAL_DIECISEIS_CUATRO = 4;
	public static final int TIPO_VALIDADOR_DECIMAL_OCHO = 5;

	// Tipo dependencia
	public static final int TIPO_DEPENDENCIA_ES_HORIZONTAL = 1;

	public ControlDinamicoRiesgoDTO() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	public Integer getTipoControl() {
		return tipoControl;
	}

	public void setTipoControl(Integer tipoControl) {
		this.tipoControl = tipoControl;
	}

	public Integer getTipoValidador() {
		return tipoValidador;
	}

	public void setTipoValidador(Integer tipoValidador) {
		this.tipoValidador = tipoValidador;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	public Map<String, String> getLista() {
		return lista;
	}

	public void setLista(Map<String, String> lista) {
		this.lista = lista;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public void setDependencia(String dependencia) {
		this.dependencia = dependencia;
	}

	public String getDependencia() {
		return dependencia;
	}

	public Integer getTipoDependencia() {
		return tipoDependencia;
	}

	public void setTipoDependencia(Integer tipoDependencia) {
		this.tipoDependencia = tipoDependencia;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ControlDinamicoRiesgoDTO other = (ControlDinamicoRiesgoDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public void setDescripcionNivel(String descripcionNivel) {
		this.descripcionNivel = descripcionNivel;
	}

	public String getDescripcionNivel() {
		return descripcionNivel;
	}

	public void setIdNivel(String idNivel) {
		this.idNivel = idNivel;
	}

	public String getIdNivel() {
		return idNivel;
	}

	public Boolean getDeshabilitado() {
		return deshabilitado;
	}

	public void setDeshabilitado(Boolean deshabilitado) {
		this.deshabilitado = deshabilitado;
	}

	public void setIdGrupoValores(Short idGrupoValores) {
		this.idGrupoValores = idGrupoValores;
	}

	public Short getIdGrupoValores() {
		return idGrupoValores;
	}

	
	

}
