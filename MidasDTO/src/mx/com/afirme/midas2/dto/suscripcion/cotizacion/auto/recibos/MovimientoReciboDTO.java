package mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.recibos;

import java.io.Serializable;
import java.math.BigDecimal;

public class MovimientoReciboDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5071447774049745521L;
	
	private BigDecimal idRecibo;
	private BigDecimal verRecibo;
	private BigDecimal folioRecibo;
	private String sitRecibo;
	private String cveTDoctoVerr;
	private BigDecimal idDoctovers;
	private String cveMovtoEnd;
	private BigDecimal numEndoso;
	
	public void setIdRecibo(BigDecimal idRecibo) {
		this.idRecibo = idRecibo;
	}
	public BigDecimal getIdRecibo() {
		return idRecibo;
	}
	public void setVerRecibo(BigDecimal verRecibo) {
		this.verRecibo = verRecibo;
	}
	public BigDecimal getVerRecibo() {
		return verRecibo;
	}
	public void setFolioRecibo(BigDecimal folioRecibo) {
		this.folioRecibo = folioRecibo;
	}
	public BigDecimal getFolioRecibo() {
		return folioRecibo;
	}
	public void setSitRecibo(String sitRecibo) {
		this.sitRecibo = sitRecibo;
	}
	public String getSitRecibo() {
		return sitRecibo;
	}
	public void setCveTDoctoVerr(String cveTDoctoVerr) {
		this.cveTDoctoVerr = cveTDoctoVerr;
	}
	public String getCveTDoctoVerr() {
		return cveTDoctoVerr;
	}
	public void setIdDoctovers(BigDecimal idDoctovers) {
		this.idDoctovers = idDoctovers;
	}
	public BigDecimal getIdDoctovers() {
		return idDoctovers;
	}
	public void setCveMovtoEnd(String cveMovtoEnd) {
		this.cveMovtoEnd = cveMovtoEnd;
	}
	public String getCveMovtoEnd() {
		return cveMovtoEnd;
	}
	public void setNumEndoso(BigDecimal numEndoso) {
		this.numEndoso = numEndoso;
	}
	public BigDecimal getNumEndoso() {
		return numEndoso;
	}
	
	

}
