package mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasiva;

import java.io.Serializable;
import java.math.BigDecimal;

public class LogErroresCargaMasivaDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6392656392209085029L;
	
	private BigDecimal numeroInciso;
	private String nombreSeccion;
	private String nombreCampoError;
	private String recomendaciones;
	
	
	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}
	public void setNombreSeccion(String nombreSeccion) {
		this.nombreSeccion = nombreSeccion;
	}
	public String getNombreSeccion() {
		return nombreSeccion;
	}
	public void setNombreCampoError(String nombreCampoError) {
		this.nombreCampoError = nombreCampoError;
	}
	public String getNombreCampoError() {
		return nombreCampoError;
	}
	public void setRecomendaciones(String recomendaciones) {
		this.recomendaciones = recomendaciones;
	}
	public String getRecomendaciones() {
		return recomendaciones;
	}
	
	public String getMensajeError() {
		return "Campo: " + nombreCampoError + "- " + recomendaciones;
	}

}
