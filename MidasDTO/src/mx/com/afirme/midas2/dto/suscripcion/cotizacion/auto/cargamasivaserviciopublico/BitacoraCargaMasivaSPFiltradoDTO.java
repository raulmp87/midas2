package mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Transient;

public class BitacoraCargaMasivaSPFiltradoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3179116241406244571L;
	
	
	private Long id;
	private String poliza;
	private String numSerie;
	private String usuarioCarga;
	private Date fechaInicioCarga;
	private Date fechaFinCarga;
	private String estatus;
	private String codigoError;
	
	private Integer totalCount;
	private Integer posStart;
	private Integer count;
	private String orderByAttribute;
	
	
	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getPosStart() {
		return posStart;
	}

	public void setPosStart(Integer posStart) {
		this.posStart = posStart;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getOrderByAttribute() {
		return orderByAttribute;
	}

	public void setOrderByAttribute(String orderByAttribute) {
		this.orderByAttribute = orderByAttribute;
	}

//	public BitacoraCargaMasivaSPFiltradoDTO(){
//		super();
//	}
//	
//	public BitacoraCargaMasivaSPFiltradoDTO(Long id, String poliza,
//			String numSerie,  Date fechaInicioCarga,Date fechaFinCarga,
//			String usuarioCarga, String estatus, String codigoError) {
//		super();
//		this.id = id;
//		this.poliza = poliza;
//		this.numSerie = numSerie;
//		this.estatus=estatus;
//		this.usuarioCarga= usuarioCarga;
//		this.fechaInicioCarga=fechaInicioCarga;
//		this.fechaFinCarga=fechaFinCarga;
//		this.codigoError=codigoError;
//		
//	}
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPoliza() {
		return poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	public String getNumSerie() {
		return numSerie;
	}

	public void setNumSerie(String numSerie) {
		this.numSerie = numSerie;
	}

	public String getUsuarioCarga() {
		return usuarioCarga;
	}

	public void setUsuarioCarga(String usuarioCarga) {
		this.usuarioCarga = usuarioCarga;
	}

	public Date getFechaInicioCarga() {
		return fechaInicioCarga;
	}

	public void setFechaInicioCarga(Date fechaInicioCarga) {
		this.fechaInicioCarga = fechaInicioCarga;
	}

	public Date getFechaFinCarga() {
		return fechaFinCarga;
	}

	public void setFechaFinCarga(Date fechaFinCarga) {
		this.fechaFinCarga = fechaFinCarga;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getCodigoError() {
		return codigoError;
	}

	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}
}
