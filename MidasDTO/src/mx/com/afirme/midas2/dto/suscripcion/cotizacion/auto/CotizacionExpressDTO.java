package mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioDTO;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.CotizacionExpress;
import mx.com.afirme.midas2.validator.group.CotizacionExpressPaso1Checks;
import mx.com.afirme.midas2.validator.group.CotizacionExpressPaso2Checks;

import org.apache.bval.constraints.NotEmpty;


public class CotizacionExpressDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private ModeloVehiculoDTO modeloVehiculo;
	private EstadoDTO estado;
	private MunicipioDTO municipio;
	private String variableModificadoraDescripcion;
	private Agente agente;
	private FormaPagoDTO formaPago;
	private Paquete paquete;
	private NegocioSeccion negocioSeccion;
	private Short idMoneda;
	private Boolean frontera;
	private Long paso;
	private List<CotizacionExpressDTO.PaqueteFormaPago> paqueteFormaPagos = new ArrayList<CotizacionExpressDTO.PaqueteFormaPago>();
	
	public List<PaqueteFormaPago> getPaqueteFormaPagos() {
		return paqueteFormaPagos;
	}

	public void setPaqueteFormaPagos(List<PaqueteFormaPago> paqueteFormaPagos) {
		this.paqueteFormaPagos = paqueteFormaPagos;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Valid
	@NotNull(groups=CotizacionExpressPaso1Checks.class)
	public ModeloVehiculoDTO getModeloVehiculo() {
		return modeloVehiculo;
	}
	
	public void setModeloVehiculo(ModeloVehiculoDTO modeloVehiculo) {
		this.modeloVehiculo = modeloVehiculo;
	}
		
	
	@Valid
	@NotNull(groups=CotizacionExpressPaso1Checks.class)
	public EstadoDTO getEstado() {
		return estado;
	}
	
	public void setEstado(EstadoDTO estado) {
		this.estado = estado;
	}

	@Valid
	@NotNull(groups=CotizacionExpressPaso1Checks.class)
	public MunicipioDTO getMunicipio() {
		return municipio;
	}
	
	public void setMunicipio(MunicipioDTO municipio) {
		this.municipio = municipio;
	}
	
	@NotEmpty(groups=CotizacionExpressPaso1Checks.class, message="{com.afirme.midas2.requerido}")
	public String getVariableModificadoraDescripcion() {
		return variableModificadoraDescripcion;
	}
	
	public void setVariableModificadoraDescripcion(
			String variableModificadoraDescripcion) {
		this.variableModificadoraDescripcion = variableModificadoraDescripcion;
	}
	
	public Agente getAgente() {
		return agente;
	}
	
	public void setAgente(Agente agente) {
		this.agente = agente;
	}
	
	@NotNull(groups=CotizacionExpressPaso2Checks.class)
	@Valid
	public FormaPagoDTO getFormaPago() {
		return formaPago;
	}
	
	public void setFormaPago(FormaPagoDTO formaPago) {
		this.formaPago = formaPago;
	}
	
	@NotNull(groups=CotizacionExpressPaso2Checks.class)
	@Valid
	public Paquete getPaquete() {
		return paquete;
	}
	
	public void setPaquete(Paquete paquete) {
		this.paquete = paquete;
	}
	
	@NotNull(groups=CotizacionExpressPaso1Checks.class)
	@Valid
	public NegocioSeccion getNegocioSeccion() {
		return negocioSeccion;
	}
	
	public void setNegocioSeccion(NegocioSeccion negocioSeccion) {
		this.negocioSeccion = negocioSeccion;
	}
	
	@NotNull(groups=CotizacionExpressPaso1Checks.class)
	public Short getIdMoneda() {
		return idMoneda;
	}
	
	public void setIdMoneda(Short idMoneda) {
		this.idMoneda = idMoneda;
	}
	
	@NotNull(groups=CotizacionExpressPaso1Checks.class)
	public Long getPaso() {
		return paso;
	}
	
	public void setPaso(Long paso) {
		this.paso = paso;
	}
	
	@NotNull(groups=CotizacionExpressPaso1Checks.class, message="{com.afirme.midas2.requerido}")
	public Boolean getFrontera() {
		return frontera;
	}
	
	public void setFrontera(Boolean frontera) {
		this.frontera = frontera;
	}
	
	public CotizacionExpress getCotizacionExpress(Paquete paquete, FormaPagoDTO formaPago) {
		try{
			PaqueteFormaPago paqueteFormaPago = new PaqueteFormaPago();
			paqueteFormaPago.setPaquete(paquete);
			paqueteFormaPago.setFormaPago(formaPago);
			int i = 0;
			if ((i = paqueteFormaPagos.indexOf(paqueteFormaPago)) != -1) {
				return paqueteFormaPagos.get(i).getCotizacionExpress();
			}
		}catch(Exception ex){
			ex.printStackTrace();
			throw new RuntimeException(ex);
		}
		return null;
		
	}
	
	public void addCotizacionExpress(CotizacionExpress cotizacionExpress) {
		try{
			PaqueteFormaPago paqueteFormaPago = new PaqueteFormaPago(cotizacionExpress.getPaquete(), cotizacionExpress.getFormaPago(), cotizacionExpress);
			if (!paqueteFormaPagos.contains(paqueteFormaPago)) {
				paqueteFormaPagos.add(paqueteFormaPago);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public void resetPaqueteFormaPago(){
		paqueteFormaPagos = new ArrayList<CotizacionExpressDTO.PaqueteFormaPago>();
	}
	
	/**
	 * Obtiene la cotizacion actual para el paquete y forma de pago seleccionado.
	 * @return
	 */
	public CotizacionExpress getCotizacionActual() {
		return getCotizacionExpress(paquete, formaPago);
	}

	private class PaqueteFormaPago implements Serializable{
		
		private static final long serialVersionUID = -8747023122182686262L;
		private Paquete paquete;
		private FormaPagoDTO formaPago;
		private CotizacionExpress cotizacionExpress;
		
		public PaqueteFormaPago() {
		}
		
		public PaqueteFormaPago(Paquete paquete, FormaPagoDTO formaPago, CotizacionExpress cotizacionExpress) {
			this.paquete = paquete;
			this.formaPago = formaPago;
			this.cotizacionExpress = cotizacionExpress;
		}
		
		public void setPaquete(Paquete paquete) {
			this.paquete = paquete;
		}
		
		public void setFormaPago(FormaPagoDTO formaPago) {
			this.formaPago = formaPago;
		}
		
		public Paquete getPaquete() {
			return paquete;
		}

		public FormaPagoDTO getFormaPago() {
			return formaPago;
		}

		public CotizacionExpress getCotizacionExpress() {
			return cotizacionExpress;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result
					+ ((formaPago == null) ? 0 : formaPago.hashCode());
			result = prime * result
					+ ((paquete == null) ? 0 : paquete.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			PaqueteFormaPago other = (PaqueteFormaPago) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (formaPago == null) {
				if (other.formaPago != null)
					return false;
			} else if (!formaPago.equals(other.formaPago))
				return false;
			if (paquete == null) {
				if (other.paquete != null)
					return false;
			} else if (!paquete.equals(other.paquete))
				return false;
			return true;
		}

		private CotizacionExpressDTO getOuterType() {
			return CotizacionExpressDTO.this;
		}
		
	}
}
