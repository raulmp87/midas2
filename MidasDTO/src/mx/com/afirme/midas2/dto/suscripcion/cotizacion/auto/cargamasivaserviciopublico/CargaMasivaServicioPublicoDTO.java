package mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico;

import java.io.Serializable;

public class CargaMasivaServicioPublicoDTO implements Serializable{

	private static final long serialVersionUID = 520162044616771073L;

	private Integer posStar;
	private Integer count;
	private String orderByAtribute;
	private String usuarioActual;
	
	public Integer getPosStar() {
		return posStar;
	}
	public void setPosStar(Integer posStar) {
		this.posStar = posStar;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public String getOrderByAtribute() {
		return orderByAtribute;
	}
	public void setOrderByAtribute(String orderByAtribute) {
		this.orderByAtribute = orderByAtribute;
	}
	public String getUsuarioActual() {
		return usuarioActual;
	}
	public void setUsuarioActual(String usuarioActual) {
		this.usuarioActual = usuarioActual;
	}
}
