package mx.com.afirme.midas2.dto.notificacion.estratega;

import java.util.Date;

/**
 * Clase que representa un objeto que contiene la informaci\u00f3n
 * para la noficicaci\u00f3n de estrategas.
 * 
 * @author AFIRME
 *
 * @since 20161104
 */
public class NotificacionEstrategaDTO {
	
	private String poliza;
	private String idSistema;
	private String idOficina;
	private String subGrupo;
	private String idRamo;
	private String idSubrContable;
	private Long modulo;
	private String idBanco;
	private Long idMoneda;
	private String idTipoCuenta;
	private String numeroCuenta;
	private String numRecibo;
	private Date fecCubreDesde;
	private Date fecCubreHasta;
	private String impPrimaTotal;
	private String idEmpresa;
	private String idLineaNeg;
	private String idAgente;
	private String idFuncionario;
	private String idSucursal;
	private Long tipoMov;
	private Long reciboPagado;
	private String numOperacion;
	private String formaPago;
	private String respCobro;
	private String ordenId;
	private Long origen;
	private String serieRecibo;
	private Date fechaIntento;
	private Long numIntento;
	private String descRespCobro;
	
	public String getPoliza() {
		return poliza;
	}
	
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	
	public String getIdSistema() {
		return idSistema;
	}
	
	public void setIdSistema(String idSistema) {
		this.idSistema = idSistema;
	}
	
	public String getIdOficina() {
		return idOficina;
	}
	
	public void setIdOficina(String idOficina) {
		this.idOficina = idOficina;
	}
	
	public String getSubGrupo() {
		return subGrupo;
	}
	
	public void setSubGrupo(String subGrupo) {
		this.subGrupo = subGrupo;
	}
	
	public String getIdRamo() {
		return idRamo;
	}
	
	public void setIdRamo(String idRamo) {
		this.idRamo = idRamo;
	}
	
	public String getIdSubrContable() {
		return idSubrContable;
	}
	
	public void setIdSubrContable(String idSubrContable) {
		this.idSubrContable = idSubrContable;
	}
	
	public Long getModulo() {
		return modulo;
	}
	
	public void setModulo(Long modulo) {
		this.modulo = modulo;
	}
	
	public String getIdBanco() {
		return idBanco;
	}
	
	public void setIdBanco(String idBanco) {
		this.idBanco = idBanco;
	}
	
	public Long getIdMoneda() {
		return idMoneda;
	}
	
	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}
	
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	
	public String getNumRecibo() {
		return numRecibo;
	}
	
	public void setNumRecibo(String numRecibo) {
		this.numRecibo = numRecibo;
	}
	
	public Date getFecCubreDesde() {
		return fecCubreDesde;
	}
	
	public void setFecCubreDesde(Date fecCubreDesde) {
		this.fecCubreDesde = fecCubreDesde;
	}
	
	public Date getFecCubreHasta() {
		return fecCubreHasta;
	}
	
	public void setFecCubreHasta(Date fecCubreHasta) {
		this.fecCubreHasta = fecCubreHasta;
	}
	
	public String getImpPrimaTotal() {
		return impPrimaTotal;
	}
	
	public void setImpPrimaTotal(String impPrimaTotal) {
		this.impPrimaTotal = impPrimaTotal;
	}
	
	public String getIdEmpresa() {
		return idEmpresa;
	}
	
	public void setIdEmpresa(String idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	
	public String getIdLineaNeg() {
		return idLineaNeg;
	}
	
	public void setIdLineaNeg(String idLineaNeg) {
		this.idLineaNeg = idLineaNeg;
	}
	
	public String getIdAgente() {
		return idAgente;
	}
	
	public void setIdAgente(String idAgente) {
		this.idAgente = idAgente;
	}
	
	public String getIdFuncionario() {
		return idFuncionario;
	}
	
	public void setIdFuncionario(String idFuncionario) {
		this.idFuncionario = idFuncionario;
	}
	
	public String getIdSucursal() {
		return idSucursal;
	}
	
	public void setIdSucursal(String idSucursal) {
		this.idSucursal = idSucursal;
	}
	
	public Long getTipoMov() {
		return tipoMov;
	}
	
	public void setTipoMov(Long tipoMov) {
		this.tipoMov = tipoMov;
	}
	
	public Long getReciboPagado() {
		return reciboPagado;
	}
	
	public void setReciboPagado(Long reciboPagado) {
		this.reciboPagado = reciboPagado;
	}
	
	public String getNumOperacion() {
		return numOperacion;
	}
	
	public void setNumOperacion(String numOperacion) {
		this.numOperacion = numOperacion;
	}
	
	public String getFormaPago() {
		return formaPago;
	}
	
	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	
	public String getRespCobro() {
		return respCobro;
	}
	
	public void setRespCobro(String respCobro) {
		this.respCobro = respCobro;
	}
	
	public String getOrdenId() {
		return ordenId;
	}
	
	public void setOrdenId(String ordenId) {
		this.ordenId = ordenId;
	}
	
	public Long getOrigen() {
		return origen;
	}
	
	public void setOrigen(Long origen) {
		this.origen = origen;
	}
	
	public String getSerieRecibo() {
		return serieRecibo;
	}
	
	public void setSerieRecibo(String serieRecibo) {
		this.serieRecibo = serieRecibo;
	}
	
	public Date getFechaIntento() {
		return fechaIntento;
	}
	
	public void setFechaIntento(Date fechaIntento) {
		this.fechaIntento = fechaIntento;
	}
	
	public Long getNumIntento() {
		return numIntento;
	}
	
	public void setNumIntento(Long numIntento) {
		this.numIntento = numIntento;
	}
	
	public String getDescRespCobro() {
		return descRespCobro;
	}
	
	public void setDescRespCobro(String descRespCobro) {
		this.descRespCobro = descRespCobro;
	}

	public String getIdTipoCuenta() {
		return idTipoCuenta;
	}

	public void setIdTipoCuenta(String idTipoCuenta) {
		this.idTipoCuenta = idTipoCuenta;
	}	
}
