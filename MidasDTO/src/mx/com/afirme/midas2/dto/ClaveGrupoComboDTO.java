package mx.com.afirme.midas2.dto;

import mx.com.afirme.midas.base.CacheableDTO;

public class ClaveGrupoComboDTO extends CacheableDTO {
	private static final long serialVersionUID = 1L;
	
	private Long idGrupo;

	public Long getIdGrupo() {
		return idGrupo;
	}

	public void setIdGrupo(Long idGrupo) {
		this.idGrupo = idGrupo;
	}

	@Override
	public Object getId() {
		return getIdGrupo();
	}

	@Override
	public String getDescription() {
		return getIdGrupo().toString();
	}

	@Override
	public boolean equals(Object object) {
		if(object != null && ! (object instanceof ClaveGrupoComboDTO))
			return false;
		else if (((ClaveGrupoComboDTO)object).getIdGrupo() != null){
			return getIdGrupo().equals(((ClaveGrupoComboDTO)object).getIdGrupo());
		}
		return false;
	}

}
