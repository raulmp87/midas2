package mx.com.afirme.midas2.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.domain.tarifa.TarifaVersion;

public class RelacionesTarifaAgrupadorTarifaDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3037899582242127320L;
	
	private List<TarifaVersion> asociadas = new ArrayList<TarifaVersion>();
	private List<TarifaVersion> disponibles = new ArrayList<TarifaVersion>();
	
	public List<TarifaVersion> getAsociadas() {
		return asociadas;
	}
	public void setAsociadas(List<TarifaVersion> asociadas) {
		this.asociadas = asociadas;
	}
	public List<TarifaVersion> getDisponibles() {
		return disponibles;
	}
	public void setDisponibles(List<TarifaVersion> disponibles) {
		this.disponibles = disponibles;
	}

}
