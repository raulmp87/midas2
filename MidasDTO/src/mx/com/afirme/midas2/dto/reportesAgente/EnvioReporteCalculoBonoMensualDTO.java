package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.bonos.CalculoBono;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;

import org.directwebremoting.annotations.DataTransferObject;

@Entity(name="EnvioReporteCalculoBonoMensualDTO")
@Table(name="TOENVIOREPCBMENSUAL",schema="MIDAS")
@DataTransferObject
public class EnvioReporteCalculoBonoMensualDTO implements Serializable,Entidad  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long idBeneficiario;
	private ValorCatalogoAgentes tipoBeneficiario;
	private CalculoBono calculoBono;
	private String periodo;
	private ValorCatalogoAgentes tipoEnvio;
	private ValorCatalogoAgentes estatus;
	private String detalleEstatus;
	private Date fechaCreacion;
	private Date fechaEnvio;	
	
	public static enum EstatusEnvioRepCBMensual{
		REGISTRADO("Envio Registrado"),
		ERROR("Envio con Error"),	
		ENVIADO("Envio Exitoso");
		
		private String value;
		
		private EstatusEnvioRepCBMensual(String value){
			this.value=value;
		}
		
		public String getValue(){
			return value;
		}
	}
	
	public static enum TipoEnvioRepCBMensual{
		AUTOMATICO("Automatico"),	
		MANUAL("Manual");
		
		private String value;
		
		private TipoEnvioRepCBMensual(String value){
			this.value=value;
		}
		
		public String getValue(){
			return value;
		}
	}
	
	public static enum TipoBeneficiarioCBMensual{
		AGENTE("Agente"),	
		PROMOTOR("Promotoria");
		
		private String value;
		
		private TipoBeneficiarioCBMensual(String value){
			this.value=value;
		}
		
		public String getValue(){
			return value;
		}
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public Long getKey() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long getBusinessKey() {
		// TODO Auto-generated method stub
		return id;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOENVIOREPCBMENSUAL_SEQ")
	@SequenceGenerator(name="TOENVIOREPCBMENSUAL_SEQ", sequenceName="MIDAS.TOENVIOREPCBMENSUAL_SEQ",allocationSize=1)
	@Column(name="ID",nullable=false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="IDBENEFICIARIO",nullable=false)
	public Long getIdBeneficiario() {
		return idBeneficiario;
	}

	public void setIdBeneficiario(Long idBeneficiario) {
		this.idBeneficiario = idBeneficiario;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDTIPOBENEFICIARIO")	
	public ValorCatalogoAgentes getTipoBeneficiario() {
		return tipoBeneficiario;
	}

	public void setTipoBeneficiario(ValorCatalogoAgentes tipoBeneficiario) {
		this.tipoBeneficiario = tipoBeneficiario;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=CalculoBono.class,optional=true)
	@JoinColumn(name="IDCALCULOBONO")
	public CalculoBono getCalculoBono() {
		return calculoBono;
	}

	public void setCalculoBono(CalculoBono calculoBono) {
		this.calculoBono = calculoBono;
	}

	@Column(name="PERIODO")
	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	@ManyToOne(fetch=FetchType.LAZY,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDTIPOENVIO")
	public ValorCatalogoAgentes getTipoEnvio() {
		return tipoEnvio;
	}

	public void setTipoEnvio(ValorCatalogoAgentes tipoEnvio) {
		this.tipoEnvio = tipoEnvio;
	}

	@ManyToOne(fetch=FetchType.EAGER,targetEntity=ValorCatalogoAgentes.class)
	@JoinColumn(name="IDESTATUS")
	public ValorCatalogoAgentes getEstatus() {
		return estatus;
	}

	public void setEstatus(ValorCatalogoAgentes estatus) {
		this.estatus = estatus;
	}

	@Column(name="DETALLEESTATUS")
	public String getDetalleEstatus() {
		return detalleEstatus;
	}

	public void setDetalleEstatus(String detalleEstatus) {
		this.detalleEstatus = detalleEstatus;
	}

	@Column(name="FECHACREACION")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Column(name="FECHAENVIO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

}
