package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@SqlResultSetMapping(name="datosAgenteView",entities={
		@EntityResult(entityClass=DatosAgenteView.class,fields={
			@FieldResult(name="id",column="id"),
			@FieldResult(name="idAgente",column="idAgente"),
			@FieldResult(name="nombreCompleto",column="nombreCompleto"),
			@FieldResult(name="promotoria",column="promotoria"),
			@FieldResult(name="tipoCedulaAgente",column="tipoCedulaAgente"),
			@FieldResult(name="ejecutivo",column="ejecutivo"),
			@FieldResult(name="tipoSituacion",column="tipoSituacion"),
			@FieldResult(name="gerencia",column="gerencia"),
			@FieldResult(name="prioridad",column="prioridad"),
			@FieldResult(name="centroOperacion",column="centroOperacion"),
			@FieldResult(name="fechaAlta",column="fechaAlta"),
			@FieldResult(name="fechaActivacion",column="fechaActivacion"),
			@FieldResult(name="tipoAgente",column="tipoAgente"),
			@FieldResult(name="tipoPromotoria",column="tipoPromotoria")
		})
	}
)
public class DatosAgenteView implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long id;
	private Long idAgente;
	private String nombreCompleto;
	private String promotoria;
	private String tipoCedulaAgente;
	private String ejecutivo;
	private String tipoSituacion;
	private String gerencia;
	private String prioridad;
	private String centroOperacion;
	private Date fechaAlta;
	private Date fechaActivacion;
	private String tipoAgente;
	private String tipoPromotoria;

	@Id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdAgente() {
		return idAgente;
	}

	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public String getPromotoria() {
		return promotoria;
	}

	public void setPromotoria(String promotoria) {
		this.promotoria = promotoria;
	}

	public String getTipoCedulaAgente() {
		return tipoCedulaAgente;
	}

	public void setTipoCedulaAgente(String tipoCedulaAgente) {
		this.tipoCedulaAgente = tipoCedulaAgente;
	}

	public String getEjecutivo() {
		return ejecutivo;
	}

	public void setEjecutivo(String ejecutivo) {
		this.ejecutivo = ejecutivo;
	}

	public String getTipoSituacion() {
		return tipoSituacion;
	}

	public void setTipoSituacion(String tipoSituacion) {
		this.tipoSituacion = tipoSituacion;
	}

	public String getGerencia() {
		return gerencia;
	}

	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}

	public String getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}

	public String getCentroOperacion() {
		return centroOperacion;
	}

	public void setCentroOperacion(String centroOperacion) {
		this.centroOperacion = centroOperacion;
	}
	
	@Temporal(TemporalType.DATE)
	public Date getFechaAlta() {
		return fechaAlta;
	}
	
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaActivacion() {
		return fechaActivacion;
	}

	public void setFechaActivacion(Date fechaActivacion) {
		this.fechaActivacion = fechaActivacion;
	}

	public String getTipoAgente() {
		return tipoAgente;
	}

	public void setTipoAgente(String tipoAgente) {
		this.tipoAgente = tipoAgente;
	}
	public String getTipoPromotoria() {
		return tipoPromotoria;
	}

	public void setTipoPromotoria(String tipoPromotoria) {
		this.tipoPromotoria = tipoPromotoria;
	}
}
