package mx.com.afirme.midas2.dto.reportesAgente;

import java.util.Date;

import javax.persistence.Transient;

import mx.com.afirme.midas2.annotation.Exportable;

public class ReporteHistorialCobranzaDTO {

	private Long id_cotizacion;
	private String poliza;
	private String nom_cond_cobro;
	private String tipo_cuenta;
	private String tipo_cobro;
	private String num_cuenta;
	private String nom_t_tarjeta;
	private String nom_solicitante;
	private Long id_agente;
	private String nom_agente;
	private Date f_contab;
	private String sit_poliza;
	private Date f_ini_vigencia;
	private Date f_fin_vigencia;
	private Long id_lin_negocio;
	private Long id_inciso;
	private String desc_vehic;
	private String cve_amis;
	private Long id_modelo_auto;
	private String num_serie;
	private String placa;
	private String num_motor;
	private Double imp_prima_neta;
	private Double imp_derechos;
	private Double imp_rcgos_pagofr;
	private Double imp_iva;
	private Double imp_prima_total;
	private String nom_paquete;
	private String nom_titular;
	private Long num_folio_rbo;
	private Long num_exhibicion;
	private Date f_cubre_desde;
	private Date f_cubre_hasta;
	private String sit_recibo;
	private Double imp_prima_total_rec;
	private String id_usuario_creac;
	private String nom_usuario;
	
	
	/**
	 * @return el id_cotizacion
	 */
	@Exportable(columnName = "NUM. COTIZACION", columnOrder = 1)
	@Transient	
	public Long getId_cotizacion() {
		return id_cotizacion;
	}
	/**
	 * @param id_cotizacion el id_cotizacion a establecer
	 */
	public void setId_cotizacion(Long id_cotizacion) {
		this.id_cotizacion = id_cotizacion;
	}
	/**
	 * @return el poliza
	 */
	@Exportable(columnName = "POLIZA", columnOrder = 2)
	@Transient	
	public String getPoliza() {
		return poliza;
	}
	/**
	 * @param poliza el poliza a establecer
	 */
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	/**
	 * @return el nom_cond_cobro
	 */
	@Exportable(columnName = "NOMBRE COND. COBRO", columnOrder = 3)
	@Transient	
	public String getNom_cond_cobro() {
		return nom_cond_cobro;
	}
	/**
	 * @param nom_cond_cobro el nom_cond_cobro a establecer
	 */
	public void setNom_cond_cobro(String nom_cond_cobro) {
		this.nom_cond_cobro = nom_cond_cobro;
	}
	/**
	 * @return el tipo_cuenta
	 */
	@Exportable(columnName = "TIPO CUENTA", columnOrder = 4)
	@Transient	
	public String getTipo_cuenta() {
		return tipo_cuenta;
	}
	/**
	 * @param tipo_cuenta el tipo_cuenta a establecer
	 */
	public void setTipo_cuenta(String tipo_cuenta) {
		this.tipo_cuenta = tipo_cuenta;
	}
	/**
	 * @return el tipo_cobro
	 */
	@Exportable(columnName = "TIPO COBRO", columnOrder = 5)
	@Transient	
	public String getTipo_cobro() {
		return tipo_cobro;
	}
	/**
	 * @param tipo_cobro el tipo_cobro a establecer
	 */
	public void setTipo_cobro(String tipo_cobro) {
		this.tipo_cobro = tipo_cobro;
	}
	/**
	 * @return el num_cuenta
	 */
	@Exportable(columnName = "NUM. CUENTA", columnOrder = 6)
	@Transient	
	public String getNum_cuenta() {
		return num_cuenta;
	}
	/**
	 * @param num_cuenta el num_cuenta a establecer
	 */
	public void setNum_cuenta(String num_cuenta) {
		this.num_cuenta = num_cuenta;
	}
	/**
	 * @return el nom_t_tarjeta
	 */
	@Exportable(columnName = "NOMBRE TITULAR TARJETA", columnOrder = 7)
	@Transient	
	public String getNom_t_tarjeta() {
		return nom_t_tarjeta;
	}
	/**
	 * @param nom_t_tarjeta el nom_t_tarjeta a establecer
	 */
	public void setNom_t_tarjeta(String nom_t_tarjeta) {
		this.nom_t_tarjeta = nom_t_tarjeta;
	}
	/**
	 * @return el nom_solicitante
	 */
	@Exportable(columnName = "NOMBRE SOLICITANTE", columnOrder = 8)
	@Transient	
	public String getNom_solicitante() {
		return nom_solicitante;
	}
	/**
	 * @param nom_solicitante el nom_solicitante a establecer
	 */
	public void setNom_solicitante(String nom_solicitante) {
		this.nom_solicitante = nom_solicitante;
	}
	/**
	 * @return el id_agente
	 */
	@Exportable(columnName = "ID AGENTE", columnOrder = 9)
	@Transient	
	public Long getId_agente() {
		return id_agente;
	}
	/**
	 * @param id_agente el id_agente a establecer
	 */
	public void setId_agente(Long id_agente) {
		this.id_agente = id_agente;
	}
	/**
	 * @return el nom_agente
	 */
	@Exportable(columnName = "NOMBRE AGENTE", columnOrder = 10)
	@Transient	
	public String getNom_agente() {
		return nom_agente;
	}
	/**
	 * @param nom_agente el nom_agente a establecer
	 */
	public void setNom_agente(String nom_agente) {
		this.nom_agente = nom_agente;
	}
	/**
	 * @return el f_contab
	 */
	@Exportable(columnName = "FECHA CONTABLE", columnOrder = 11)
	@Transient	
	public Date getF_contab() {
		return f_contab;
	}
	/**
	 * @param f_contab el f_contab a establecer
	 */
	public void setF_contab(Date f_contab) {
		this.f_contab = f_contab;
	}
	/**
	 * @return el sit_poliza
	 */
	@Exportable(columnName = "SITUACION POLIZA", columnOrder = 12)
	@Transient	
	public String getSit_poliza() {
		return sit_poliza;
	}
	/**
	 * @param sit_poliza el sit_poliza a establecer
	 */
	public void setSit_poliza(String sit_poliza) {
		this.sit_poliza = sit_poliza;
	}
	/**
	 * @return el f_ini_vigencia
	 */
	@Exportable(columnName = "FECHA INI. VIGENCIA", columnOrder = 13)
	@Transient	
	public Date getF_ini_vigencia() {
		return f_ini_vigencia;
	}
	/**
	 * @param f_ini_vigencia el f_ini_vigencia a establecer
	 */
	public void setF_ini_vigencia(Date f_ini_vigencia) {
		this.f_ini_vigencia = f_ini_vigencia;
	}
	/**
	 * @return el f_fin_vigencia
	 */
	@Exportable(columnName = "FECHA FIN VIGENCIA", columnOrder = 14)
	@Transient	
	public Date getF_fin_vigencia() {
		return f_fin_vigencia;
	}
	/**
	 * @param f_fin_vigencia el f_fin_vigencia a establecer
	 */
	public void setF_fin_vigencia(Date f_fin_vigencia) {
		this.f_fin_vigencia = f_fin_vigencia;
	}
	/**
	 * @return el id_lin_negocio
	 */
	@Exportable(columnName = "ID LINEA NEGOCIO", columnOrder = 15)
	@Transient	
	public Long getId_lin_negocio() {
		return id_lin_negocio;
	}
	/**
	 * @param id_lin_negocio el id_lin_negocio a establecer
	 */
	public void setId_lin_negocio(Long id_lin_negocio) {
		this.id_lin_negocio = id_lin_negocio;
	}
	/**
	 * @return el id_inciso
	 */
	@Exportable(columnName = "INCISO", columnOrder = 16)
	@Transient	
	public Long getId_inciso() {
		return id_inciso;
	}
	/**
	 * @param id_inciso el id_inciso a establecer
	 */
	public void setId_inciso(Long id_inciso) {
		this.id_inciso = id_inciso;
	}
	/**
	 * @return el desc_vehic
	 */
	@Exportable(columnName = "DESCRIPCION VEHICULO", columnOrder = 17)
	@Transient	
	public String getDesc_vehic() {
		return desc_vehic;
	}
	/**
	 * @param desc_vehic el desc_vehic a establecer
	 */
	public void setDesc_vehic(String desc_vehic) {
		this.desc_vehic = desc_vehic;
	}
	/**
	 * @return el cve_amis
	 */
	@Exportable(columnName = "CLAVE AMIS", columnOrder = 18)
	@Transient	
	public String getCve_amis() {
		return cve_amis;
	}
	/**
	 * @param cve_amis el cve_amis a establecer
	 */
	public void setCve_amis(String cve_amis) {
		this.cve_amis = cve_amis;
	}
	/**
	 * @return el id_modelo_auto
	 */
	@Exportable(columnName = "ID MODELO AUTO", columnOrder = 19)
	@Transient	
	public Long getId_modelo_auto() {
		return id_modelo_auto;
	}
	/**
	 * @param id_modelo_auto el id_modelo_auto a establecer
	 */
	public void setId_modelo_auto(Long id_modelo_auto) {
		this.id_modelo_auto = id_modelo_auto;
	}
	/**
	 * @return el num_serie
	 */
	@Exportable(columnName = "SERIE", columnOrder = 20)
	@Transient	
	public String getNum_serie() {
		return num_serie;
	}
	/**
	 * @param num_serie el num_serie a establecer
	 */
	public void setNum_serie(String num_serie) {
		this.num_serie = num_serie;
	}
	/**
	 * @return el placa
	 */
	@Exportable(columnName = "PLACA", columnOrder = 21)
	@Transient	
	public String getPlaca() {
		return placa;
	}
	/**
	 * @param placa el placa a establecer
	 */
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	/**
	 * @return el num_motor
	 */
	@Exportable(columnName = "NUMERO MOTOR", columnOrder = 22)
	@Transient	
	public String getNum_motor() {
		return num_motor;
	}
	/**
	 * @param num_motor el num_motor a establecer
	 */
	public void setNum_motor(String num_motor) {
		this.num_motor = num_motor;
	}
	/**
	 * @return el imp_prima_neta
	 */
	@Exportable(columnName = "IMPUESTO PRIMA NETA", columnOrder = 23)
	@Transient	
	public Double getImp_prima_neta() {
		return imp_prima_neta;
	}
	/**
	 * @param imp_prima_neta el imp_prima_neta a establecer
	 */
	public void setImp_prima_neta(Double imp_prima_neta) {
		this.imp_prima_neta = imp_prima_neta;
	}
	/**
	 * @return el imp_derechos
	 */
	@Exportable(columnName = "IMPUESTO DERECHOS", columnOrder = 24)
	@Transient	
	public Double getImp_derechos() {
		return imp_derechos;
	}
	/**
	 * @param imp_derechos el imp_derechos a establecer
	 */
	public void setImp_derechos(Double imp_derechos) {
		this.imp_derechos = imp_derechos;
	}
	/**
	 * @return el imp_rcgos_pagofr
	 */
	@Exportable(columnName = "IMPUESTO RECARGOS PAGOFR", columnOrder = 25)
	@Transient	
	public Double getImp_rcgos_pagofr() {
		return imp_rcgos_pagofr;
	}
	/**
	 * @param imp_rcgos_pagofr el imp_rcgos_pagofr a establecer
	 */
	public void setImp_rcgos_pagofr(Double imp_rcgos_pagofr) {
		this.imp_rcgos_pagofr = imp_rcgos_pagofr;
	}
	/**
	 * @return el imp_iva
	 */
	@Exportable(columnName = "IVA", columnOrder = 26)
	@Transient	
	public Double getImp_iva() {
		return imp_iva;
	}
	/**
	 * @param imp_iva el imp_iva a establecer
	 */
	public void setImp_iva(Double imp_iva) {
		this.imp_iva = imp_iva;
	}
	/**
	 * @return el imp_prima_total
	 */
	@Exportable(columnName = "IMPUESTO PRIMA TOTAL", columnOrder = 27)
	@Transient	
	public Double getImp_prima_total() {
		return imp_prima_total;
	}
	/**
	 * @param imp_prima_total el imp_prima_total a establecer
	 */
	public void setImp_prima_total(Double imp_prima_total) {
		this.imp_prima_total = imp_prima_total;
	}
	/**
	 * @return el nom_paquete
	 */
	@Exportable(columnName = "NOMBRE PAQUETE", columnOrder = 28)
	@Transient	
	public String getNom_paquete() {
		return nom_paquete;
	}
	/**
	 * @param nom_paquete el nom_paquete a establecer
	 */
	public void setNom_paquete(String nom_paquete) {
		this.nom_paquete = nom_paquete;
	}
	/**
	 * @return el nom_titular
	 */
	@Exportable(columnName = "NOMBRE TITULAR", columnOrder = 29)
	@Transient	
	public String getNom_titular() {
		return nom_titular;
	}
	/**
	 * @param nom_titular el nom_titular a establecer
	 */
	public void setNom_titular(String nom_titular) {
		this.nom_titular = nom_titular;
	}
	/**
	 * @return el num_folio_rbo
	 */
	@Exportable(columnName = "NUMERO FOLIO RECIBO", columnOrder = 30)
	@Transient	
	public Long getNum_folio_rbo() {
		return num_folio_rbo;
	}
	/**
	 * @param num_folio_rbo el num_folio_rbo a establecer
	 */
	public void setNum_folio_rbo(Long num_folio_rbo) {
		this.num_folio_rbo = num_folio_rbo;
	}
	/**
	 * @return el num_exhibicion
	 */
	@Exportable(columnName = "NUMERO EXHIBICION", columnOrder = 31)
	@Transient	
	public Long getNum_exhibicion() {
		return num_exhibicion;
	}
	/**
	 * @param num_exhibicion el num_exhibicion a establecer
	 */
	public void setNum_exhibicion(Long num_exhibicion) {
		this.num_exhibicion = num_exhibicion;
	}
	/**
	 * @return el f_cubre_desde
	 */
	@Exportable(columnName = "FECHA CUBRE DESDE", columnOrder = 32)
	@Transient	
	public Date getF_cubre_desde() {
		return f_cubre_desde;
	}
	/**
	 * @param f_cubre_desde el f_cubre_desde a establecer
	 */
	public void setF_cubre_desde(Date f_cubre_desde) {
		this.f_cubre_desde = f_cubre_desde;
	}
	/**
	 * @return el f_cubre_hasta
	 */
	@Exportable(columnName = "FECHA CUBRE HASTA", columnOrder = 33)
	@Transient	
	public Date getF_cubre_hasta() {
		return f_cubre_hasta;
	}
	/**
	 * @param f_cubre_hasta el f_cubre_hasta a establecer
	 */
	public void setF_cubre_hasta(Date f_cubre_hasta) {
		this.f_cubre_hasta = f_cubre_hasta;
	}
	/**
	 * @return el sit_recibo
	 */
	@Exportable(columnName = "SITUACION RECIBO", columnOrder = 34)
	@Transient	
	public String getSit_recibo() {
		return sit_recibo;
	}
	/**
	 * @param sit_recibo el sit_recibo a establecer
	 */
	public void setSit_recibo(String sit_recibo) {
		this.sit_recibo = sit_recibo;
	}
	/**
	 * @return el imp_prima_total_rec
	 */
	@Exportable(columnName = "IMPUESTO PRIMA TOTAL REC", columnOrder = 35)
	@Transient	
	public Double getImp_prima_total_rec() {
		return imp_prima_total_rec;
	}
	/**
	 * @param imp_prima_total_rec el imp_prima_total_rec a establecer
	 */
	public void setImp_prima_total_rec(Double imp_prima_total_rec) {
		this.imp_prima_total_rec = imp_prima_total_rec;
	}
	/**
	 * @return el id_usuario_creac
	 */
	@Exportable(columnName = "ID USUARIO CREACION", columnOrder = 36)
	@Transient	
	public String getId_usuario_creac() {
		return id_usuario_creac;
	}
	/**
	 * @param id_usuario_creac el id_usuario_creac a establecer
	 */
	public void setId_usuario_creac(String id_usuario_creac) {
		this.id_usuario_creac = id_usuario_creac;
	}
	/**
	 * @return el nom_usuario
	 */
	@Exportable(columnName = "NOMBRE USUARIO", columnOrder = 37)
	@Transient	
	public String getNom_usuario() {
		return nom_usuario;
	}
	/**
	 * @param nom_usuario el nom_usuario a establecer
	 */
	public void setNom_usuario(String nom_usuario) {
		this.nom_usuario = nom_usuario;
	}
}
