package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
public class DatosAgentesReporteProduccion implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private Long id_moneda;
	private String  desc_moneda;
	private String  id_ramo_contable;
	private String  nom_ramo;
	private String  id_subr_contable;
	private String  nom_subramo;
	private Long  id_centro_emis;
	private Long  num_poliza;
	private Long  num_renov_pol;
	private String  num_polizax;
	private String  endoso;
	private String  f_contab;
	private Long  id_empresa;
	private Long  id_agente;
	private String d_nombre;
	private Long i_supervisoria;
	private String n_supervisoria;
	private Long i_oficina;
	private String n_oficina;
	private Long i_gerencia;
	private String n_gerencia;//********
	private String  sit_recibo;
	private String  f_cubre_desde;
	private String  f_cubre_hasta;
	private String  nom_solicitante;
	private Double  prima_neta;
	private Double  bonif_comis;
	private Double  derechos;
	private Double  recargos;
	private Double  iva;
	private Double  primatotal;
	private Double  com_agt_pf;
	private Double  com_agt_pm;
	private Double  com_sup_pf;
	private Double  com_sup_pm;
	private Double  com_rec_pf;
	private Double  com_rec_pm;
	private Double compesacionesagtpf;
	private Double compesacionesagtpm;
	private Double compesacionesProm;
	private Long  id_contratante;
	private Long  id_remesa;
	private Date  f_cubre_desde_recibo;
	private Date  f_cubre_hasta_recibo;
	private String  tipo_mov;
	private String  num_cuenta;
	private String  tar_cre;
	private String  lin_negocio;
	private Long numero_vendedor;
	private String nombre_vendedor;
	private String  usu_emi;
	private Long  num_folio_rbo;
	private String  recibo_pagado;
	private String  paquete;
	private String  medio_pago;
	private String  sistema_emision;
	private String  pol_rc_ext;
	private String  duso;
	private String  dservicio;
	private Double  fpct_descto_glob;
	private String  tipo_endoso;
	private String b_parte;
	private String casa;
	private Double tipo_cambio;
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getId_moneda() {
		return id_moneda;
	}
	public void setId_moneda(Long id_moneda) {
		this.id_moneda = id_moneda;
	}
	public String getDesc_moneda() {
		return desc_moneda;
	}
	public void setDesc_moneda(String desc_moneda) {
		this.desc_moneda = desc_moneda;
	}
	public String getId_ramo_contable() {
		return id_ramo_contable;
	}
	public void setId_ramo_contable(String id_ramo_contable) {
		this.id_ramo_contable = id_ramo_contable;
	}
	public String getNom_ramo() {
		return nom_ramo;
	}
	public void setNom_ramo(String nom_ramo) {
		this.nom_ramo = nom_ramo;
	}
	public String getId_subr_contable() {
		return id_subr_contable;
	}
	public void setId_subr_contable(String id_subr_contable) {
		this.id_subr_contable = id_subr_contable;
	}
	public String getNom_subramo() {
		return nom_subramo;
	}
	public void setNom_subramo(String nom_subramo) {
		this.nom_subramo = nom_subramo;
	}
	public Long getId_centro_emis() {
		return id_centro_emis;
	}
	public void setId_centro_emis(Long id_centro_emis) {
		this.id_centro_emis = id_centro_emis;
	}
	public Long getNum_poliza() {
		return num_poliza;
	}
	public void setNum_poliza(Long num_poliza) {
		this.num_poliza = num_poliza;
	}
	public Long getNum_renov_pol() {
		return num_renov_pol;
	}
	public void setNum_renov_pol(Long num_renov_pol) {
		this.num_renov_pol = num_renov_pol;
	}
	public String getNum_polizax() {
		return num_polizax;
	}
	public void setNum_polizax(String num_polizax) {
		this.num_polizax = num_polizax;
	}
	public String getEndoso() {
		return endoso;
	}
	public void setEndoso(String endoso) {
		this.endoso = endoso;
	}
	public String getF_contab() {
		return f_contab;
	}
	public void setF_contab(String f_contab) {
		this.f_contab = f_contab;
	}
	public Long getId_empresa() {
		return id_empresa;
	}
	public void setId_empresa(Long id_empresa) {
		this.id_empresa = id_empresa;
	}
	public Long getId_agente() {
		return id_agente;
	}
	public void setId_agente(Long id_agente) {
		this.id_agente = id_agente;
	}
	public String getD_nombre() {
		return d_nombre;
	}
	public void setD_nombre(String d_nombre) {
		this.d_nombre = d_nombre;
	}
	public Long getI_supervisoria() {
		return i_supervisoria;
	}
	public void setI_supervisoria(Long i_supervisoria) {
		this.i_supervisoria = i_supervisoria;
	}
	public String getN_supervisoria() {
		return n_supervisoria;
	}
	public void setN_supervisoria(String n_supervisoria) {
		this.n_supervisoria = n_supervisoria;
	}
	public Long getI_oficina() {
		return i_oficina;
	}
	public void setI_oficina(Long i_oficina) {
		this.i_oficina = i_oficina;
	}
	public String getN_oficina() {
		return n_oficina;
	}
	public void setN_oficina(String n_oficina) {
		this.n_oficina = n_oficina;
	}
	public Long getI_gerencia() {
		return i_gerencia;
	}
	public void setI_gerencia(Long i_gerencia) {
		this.i_gerencia = i_gerencia;
	}
	public String getN_gerencia() {
		return n_gerencia;
	}
	public void setN_gerencia(String n_gerencia) {
		this.n_gerencia = n_gerencia;
	}
	public String getSit_recibo() {
		return sit_recibo;
	}
	public void setSit_recibo(String sit_recibo) {
		this.sit_recibo = sit_recibo;
	}
	public String getF_cubre_desde() {
		return f_cubre_desde;
	}
	public void setF_cubre_desde(String f_cubre_desde) {
		this.f_cubre_desde = f_cubre_desde;
	}
	public String getF_cubre_hasta() {
		return f_cubre_hasta;
	}
	public void setF_cubre_hasta(String f_cubre_hasta) {
		this.f_cubre_hasta = f_cubre_hasta;
	}
	public String getNom_solicitante() {
		return nom_solicitante;
	}
	public void setNom_solicitante(String nom_solicitante) {
		this.nom_solicitante = nom_solicitante;
	}
	public Double getPrima_neta() {
		return prima_neta;
	}
	public void setPrima_neta(Double prima_neta) {
		this.prima_neta = prima_neta;
	}
	public Double getBonif_comis() {
		return bonif_comis;
	}
	public void setBonif_comis(Double bonif_comis) {
		this.bonif_comis = bonif_comis;
	}
	public Double getDerechos() {
		return derechos;
	}
	public void setDerechos(Double derechos) {
		this.derechos = derechos;
	}
	public Double getRecargos() {
		return recargos;
	}
	public void setRecargos(Double recargos) {
		this.recargos = recargos;
	}
	public Double getIva() {
		return iva;
	}
	public void setIva(Double iva) {
		this.iva = iva;
	}
	public Double getPrimatotal() {
		return primatotal;
	}
	public void setPrimatotal(Double primatotal) {
		this.primatotal = primatotal;
	}
	public Double getCom_agt_pf() {
		return com_agt_pf;
	}
	public void setCom_agt_pf(Double com_agt_pf) {
		this.com_agt_pf = com_agt_pf;
	}
	public Double getCom_agt_pm() {
		return com_agt_pm;
	}
	public void setCom_agt_pm(Double com_agt_pm) {
		this.com_agt_pm = com_agt_pm;
	}
	public Double getCom_sup_pf() {
		return com_sup_pf;
	}
	public void setCom_sup_pf(Double com_sup_pf) {
		this.com_sup_pf = com_sup_pf;
	}
	public Double getCom_sup_pm() {
		return com_sup_pm;
	}
	public void setCom_sup_pm(Double com_sup_pm) {
		this.com_sup_pm = com_sup_pm;
	}
	public Double getCom_rec_pf() {
		return com_rec_pf;
	}
	public void setCom_rec_pf(Double com_rec_pf) {
		this.com_rec_pf = com_rec_pf;
	}
	public Double getCom_rec_pm() {
		return com_rec_pm;
	}
	public void setCom_rec_pm(Double com_rec_pm) {
		this.com_rec_pm = com_rec_pm;
	}
	public Long getId_contratante() {
		return id_contratante;
	}
	public void setId_contratante(Long id_contratante) {
		this.id_contratante = id_contratante;
	}
	public Long getId_remesa() {
		return id_remesa;
	}
	public void setId_remesa(Long id_remesa) {
		this.id_remesa = id_remesa;
	}
	@Temporal(TemporalType.DATE)
	public Date getF_cubre_desde_recibo() {
		return f_cubre_desde_recibo;
	}
	public void setF_cubre_desde_recibo(Date f_cubre_desde_recibo) {
		this.f_cubre_desde_recibo = f_cubre_desde_recibo;
	}
	@Temporal(TemporalType.DATE)
	public Date getF_cubre_hasta_recibo() {
		return f_cubre_hasta_recibo;
	}
	public void setF_cubre_hasta_recibo(Date f_cubre_hasta_recibo) {
		this.f_cubre_hasta_recibo = f_cubre_hasta_recibo;
	}
	public String getTipo_mov() {
		return tipo_mov;
	}
	public void setTipo_mov(String tipo_mov) {
		this.tipo_mov = tipo_mov;
	}
	public String getNum_cuenta() {
		return num_cuenta;
	}
	public void setNum_cuenta(String num_cuenta) {
		this.num_cuenta = num_cuenta;
	}
	public String getTar_cre() {
		return tar_cre;
	}
	public void setTar_cre(String tar_cre) {
		this.tar_cre = tar_cre;
	}
	public String getLin_negocio() {
		return lin_negocio;
	}
	public void setLin_negocio(String lin_negocio) {
		this.lin_negocio = lin_negocio;
	}
	public Long getNumero_vendedor() {
		return numero_vendedor;
	}
	public void setNumero_vendedor(Long numero_vendedor) {
		this.numero_vendedor = numero_vendedor;
	}
	public String getNombre_vendedor() {
		return nombre_vendedor;
	}
	public void setNombre_vendedor(String nombre_vendedor) {
		this.nombre_vendedor = nombre_vendedor;
	}
	public String getUsu_emi() {
		return usu_emi;
	}
	public void setUsu_emi(String usu_emi) {
		this.usu_emi = usu_emi;
	}
	public Long getNum_folio_rbo() {
		return num_folio_rbo;
	}
	public void setNum_folio_rbo(Long num_folio_rbo) {
		this.num_folio_rbo = num_folio_rbo;
	}
	public String getRecibo_pagado() {
		return recibo_pagado;
	}
	public void setRecibo_pagado(String recibo_pagado) {
		this.recibo_pagado = recibo_pagado;
	}
	public String getPaquete() {
		return paquete;
	}
	public void setPaquete(String paquete) {
		this.paquete = paquete;
	}
	public String getMedio_pago() {
		return medio_pago;
	}
	public void setMedio_pago(String medio_pago) {
		this.medio_pago = medio_pago;
	}
	public String getSistema_emision() {
		return sistema_emision;
	}
	public void setSistema_emision(String sistema_emision) {
		this.sistema_emision = sistema_emision;
	}
	public String getPol_rc_ext() {
		return pol_rc_ext;
	}
	public void setPol_rc_ext(String pol_rc_ext) {
		this.pol_rc_ext = pol_rc_ext;
	}
	public String getDuso() {
		return duso;
	}
	public void setDuso(String duso) {
		this.duso = duso;
	}
	public String getDservicio() {
		return dservicio;
	}
	public void setDservicio(String dservicio) {
		this.dservicio = dservicio;
	}
	public Double getFpct_descto_glob() {
		return fpct_descto_glob;
	}
	public void setFpct_descto_glob(Double fpct_descto_glob) {
		this.fpct_descto_glob = fpct_descto_glob;
	}
	public String getTipo_endoso() {
		return tipo_endoso;
	}
	public void setTipo_endoso(String tipo_endoso) {
		this.tipo_endoso = tipo_endoso;
	}
	public String getB_parte() {
		return b_parte;
	}
	public void setB_parte(String b_parte) {
		this.b_parte = b_parte;
	}
	public String getCasa() {
		return casa;
	}
	public void setCasa(String casa) {
		this.casa = casa;
	}
	public Double getTipo_cambio() {
		return tipo_cambio;
	}
	public void setTipo_cambio(Double tipo_cambio) {
		this.tipo_cambio = tipo_cambio;
	}
	public Double getCompesacionesProm() {
		return compesacionesProm;
	}
	public void setCompesacionesProm(Double compesacionesProm) {
		this.compesacionesProm = compesacionesProm;
	}
	public Double getCompesacionesagtpf() {
		return compesacionesagtpf;
	}
	public void setCompesacionesagtpf(Double compesacionesagtpf) {
		this.compesacionesagtpf = compesacionesagtpf;
	}
	public Double getCompesacionesagtpm() {
		return compesacionesagtpm;
	}
	public void setCompesacionesagtpm(Double compesacionesagtpm) {
		this.compesacionesagtpm = compesacionesagtpm;
	}
}
