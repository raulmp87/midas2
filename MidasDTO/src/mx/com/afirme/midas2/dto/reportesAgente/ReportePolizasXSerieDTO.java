package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;

import javax.persistence.Transient;

import mx.com.afirme.midas2.annotation.Exportable;

/**
 * Clase que contiene la información que es generada mediante el reporte
 * de p\u00f3lizas por n\u00fcmero de serie.
 * 
 * @author AFIRME
 * 
 * @since 23112016
 * 
 * @version 1.0
 *
 */
public class ReportePolizasXSerieDTO implements Serializable {

	private static final long serialVersionUID = -2706855100456109650L;
	
	private String centroEmis;
	private String numPolizaMidas;
	private String numPolizaSeycos;
	private String numRenov;
	private Long numCotizaMidas;
	private Long numCotizaSeycos;
	private String fechIniVigencia;
	private String fechFinVigencia;
	private String sitPoliza;
	private Long linNegocio;
	private Long inciso;
	private String estilo;
	private Long modeloAuto;
	private String descVehic;
	private String numSerie;
	private String numMotor;

	@Exportable(columnName = "CENTRO_EMIS", columnOrder = 0)
	@Transient
	public String getCentroEmis() {
		return centroEmis;
	}

	public void setCentroEmis(String centroEmis) {
		this.centroEmis = centroEmis;
	}
	
	public String getNumPolizaMidas() {
		return numPolizaMidas;
	}

	public void setNumPolizaMidas(String numPolizaMidas) {
		this.numPolizaMidas = numPolizaMidas;
	}
	
	@Exportable(columnName = "NUM_POLIZA", columnOrder = 1)
	@Transient
	public String getNumPolizaSeycos() {
		return numPolizaSeycos;
	}

	public void setNumPolizaSeycos(String numPolizaSeycos) {
		this.numPolizaSeycos = numPolizaSeycos;
	}
	
	@Exportable(columnName = "NUM_RENOV_POL", columnOrder = 2)
	@Transient
	public String getNumRenov() {
		return numRenov;
	}

	public void setNumRenov(String numRenov_pol) {
		this.numRenov = numRenov_pol;
	}
	
	public Long getNumCotizaMidas() {
		return numCotizaMidas;
	}

	public void setNumCotizaMidas(Long numCotizaMidas) {
		this.numCotizaMidas = numCotizaMidas;
	}
	
	@Exportable(columnName = "NUM_COTIZA", columnOrder = 3)
	@Transient
	public Long getNumCotizaSeycos() {
		return numCotizaSeycos;
	}

	public void setNumCotizaSeycos(Long numCotizaSeycos) {
		this.numCotizaSeycos = numCotizaSeycos;
	}

	@Exportable(columnName = "F_INI_VIGENCIA", columnOrder = 4)
	@Transient
	public String getFechIniVigencia() {
		return fechIniVigencia;
	}

	public void setFechIniVigencia(String fechIniVigencia) {
		this.fechIniVigencia = fechIniVigencia;
	}

	@Exportable(columnName = "F_FIN_VIGENCIA", columnOrder = 5)
	@Transient
	public String getFechFinVigencia() {
		return fechFinVigencia;
	}

	public void setFechFinVigencia(String fechFinVigencia) {
		this.fechFinVigencia = fechFinVigencia;
	}

	@Exportable(columnName = "SIT_POLIZA", columnOrder = 6)
	@Transient
	public String getSitPoliza() {
		return sitPoliza;
	}

	public void setSitPoliza(String sitPoliza) {
		this.sitPoliza = sitPoliza;
	}

	@Exportable(columnName = "LIN_NEGOCIO", columnOrder = 7)
	@Transient
	public Long getLinNegocio() {
		return linNegocio;
	}

	public void setLinNegocio(Long linNegocio) {
		this.linNegocio = linNegocio;
	}

	@Exportable(columnName = "INCISO", columnOrder = 8)
	@Transient
	public Long getInciso() {
		return inciso;
	}

	public void setInciso(Long inciso) {
		this.inciso = inciso;
	}

	@Exportable(columnName = "ESTILO", columnOrder = 9)
	@Transient
	public String getEstilo() {
		return estilo;
	}

	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}

	@Exportable(columnName = "MODELO_AUTO", columnOrder = 10)
	@Transient
	public Long getModeloAuto() {
		return modeloAuto;
	}

	public void setModeloAuto(Long modeloAuto) {
		this.modeloAuto = modeloAuto;
	}

	@Exportable(columnName = "DESC_VEHIC", columnOrder = 11)
	@Transient
	public String getDescVehic() {
		return descVehic;
	}

	public void setDescVehic(String descVehic) {
		this.descVehic = descVehic;
	}

	@Exportable(columnName = "NUM_SERIE", columnOrder = 12)
	@Transient
	public String getNumSerie() {
		return numSerie;
	}

	public void setNumSerie(String numSerie) {
		this.numSerie = numSerie;
	}

	@Exportable(columnName = "NUM_MOTOR", columnOrder = 13)
	@Transient
	public String getNumMotor() {
		return numMotor;
	}

	public void setNumMotor(String numMotor) {
		this.numMotor = numMotor;
	}
}
