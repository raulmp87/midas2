package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;

public class ReporteGlobalBonoYComisionesDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String f_corte_pagocom;
	private String nom_gerencia;
	private String ejecutivo;
	private Long id_agente;
	private String nombre;
	private Double imp_comis_agte;
	private String banco;
	private String descripcionbono;

	public static final String PLANTILLA_NAME = "midas.agente.reporte.agente.repGlobalBono.archivo.nombre";
	
	public String getDescripcionbono() {
		return descripcionbono;
	}
	public void setDescripcionbono(String descripcionbono) {
		this.descripcionbono = descripcionbono;
	}
	public String getF_corte_pagocom() {
		return f_corte_pagocom;
	}
	public void setF_corte_pagocom(String f_corte_pagocom) {
		this.f_corte_pagocom = f_corte_pagocom;
	}
	public String getNom_gerencia() {
		return nom_gerencia;
	}
	public void setNom_gerencia(String nom_gerencia) {
		this.nom_gerencia = nom_gerencia;
	}
	public String getEjecutivo() {
		return ejecutivo;
	}
	public void setEjecutivo(String ejecutivo) {
		this.ejecutivo = ejecutivo;
	}
	public Long getId_agente() {
		return id_agente;
	}
	public void setId_agente(Long id_agente) {
		this.id_agente = id_agente;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Double getImp_comis_agte() {
		return imp_comis_agte;
	}
	public void setImp_comis_agte(Double imp_comis_agte) {
		this.imp_comis_agte = imp_comis_agte;
	}
	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	
}
