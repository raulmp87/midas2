package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class DatosAgentePrestamoView implements Serializable {
	private static final long serialVersionUID = 1L;
	// Datos Agente
	private Long id;
	private Long idAgente;
	private String nombreCompleto;
	private String promotoria;
	private String tipoCedulaAgente;
	private String ejecutivo;
	private String tipoSituacion;
	private String gerencia;
	private String prioridad;
	private String centroOperacion;
	private Date fechaAlta;
	private Date fechaActivacion;
	private String tipoAgente;
	private String tipoPromotoria;
	// Datos Prestamos
	private Long idPrestamo;
	private Date fechaAltaMovimiento;
	private Double importeOtorgado;
	private String estatus;
	private Double importePagado;
	private String plazo;
	private String tipoMoviemiento;

	@Id
	public Long getIdPrestamo() {
		return idPrestamo;
	}

	public void setIdPrestamo(Long idPrestamo) {
		this.idPrestamo = idPrestamo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdAgente() {
		return idAgente;
	}

	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public String getPromotoria() {
		return promotoria;
	}

	public void setPromotoria(String promotoria) {
		this.promotoria = promotoria;
	}

	public String getTipoCedulaAgente() {
		return tipoCedulaAgente;
	}

	public void setTipoCedulaAgente(String tipoCedulaAgente) {
		this.tipoCedulaAgente = tipoCedulaAgente;
	}

	public String getEjecutivo() {
		return ejecutivo;
	}

	public void setEjecutivo(String ejecutivo) {
		this.ejecutivo = ejecutivo;
	}

	public String getTipoSituacion() {
		return tipoSituacion;
	}

	public void setTipoSituacion(String tipoSituacion) {
		this.tipoSituacion = tipoSituacion;
	}

	public String getGerencia() {
		return gerencia;
	}

	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}

	public String getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}

	public String getCentroOperacion() {
		return centroOperacion;
	}

	public void setCentroOperacion(String centroOperacion) {
		this.centroOperacion = centroOperacion;
	}

	@Temporal(TemporalType.DATE)
	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	@Temporal(TemporalType.DATE)
	public Date getFechaActivacion() {
		return fechaActivacion;
	}

	public void setFechaActivacion(Date fechaActivacion) {
		this.fechaActivacion = fechaActivacion;
	}

	public String getTipoAgente() {
		return tipoAgente;
	}

	public void setTipoAgente(String tipoAgente) {
		this.tipoAgente = tipoAgente;
	}

	public String getTipoPromotoria() {
		return tipoPromotoria;
	}

	public void setTipoPromotoria(String tipoPromotoria) {
		this.tipoPromotoria = tipoPromotoria;
	}

	@Temporal(TemporalType.DATE)
	public Date getFechaAltaMovimiento() {
		return fechaAltaMovimiento;
	}

	public void setFechaAltaMovimiento(Date fechaAltaMovimiento) {
		this.fechaAltaMovimiento = fechaAltaMovimiento;
	}

	public Double getImporteOtorgado() {
		return importeOtorgado;
	}

	public void setImporteOtorgado(Double importeOtorgado) {
		this.importeOtorgado = importeOtorgado;
	}


	public Double getImportePagado() {
		return importePagado;
	}

	public void setImportePagado(Double importePagado) {
		this.importePagado = importePagado;
	}

	public String getPlazo() {
		return plazo;
	}

	public void setPlazo(String plazo) {
		this.plazo = plazo;
	}

	public String getTipoMoviemiento() {
		return tipoMoviemiento;
	}

	public void setTipoMoviemiento(String tipoMoviemiento) {
		this.tipoMoviemiento = tipoMoviemiento;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

}
