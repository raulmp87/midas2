package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class DatosAgenteIngresosView implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5846344179515565620L;
	// Datos Agente
	private Long id;
	private Long idAgente;
	private Long claveAgente;
	private Long idPromotoria;
	private Long idEjecutivo;
	private Long idGerencia;
	private String nombreCompleto;
	private String promotoria;
	private String tipoCedulaAgente;
	private String ejecutivo;
	private String tipoSituacion;
	private String gerencia;
	private String prioridad;
	private String centroOperacion;
	private Date fechaAlta;
	private Date fechaActivacion;
	private String tipoAgente;
	private String tipoPromotoria;
	private String email;
	// Datos Movimiento
	private Long idMovimiento;
	private Long anioMes;
	private String fechaMovimiento;
	private Double importeBaseCalculo;
	private Double importeIva;
	private Double importeIvaRetenido;
	private Double importeISR;
	private BigDecimal importeTotal;
	private String origen;
	private String tr;
	private Double importeEstatalRetenido;
	
	public static final String PLANTILLA_NAME = "midas.agente.reporte.agente.ingresos.plantilla.archivo.nombre";
	
	@Id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getClaveAgente() {
		return claveAgente;
	}

	public void setClaveAgente(Long claveAgente) {
		this.claveAgente = claveAgente;
	}

	public Long getIdAgente() {
		return idAgente;
	}

	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public String getPromotoria() {
		return promotoria;
	}

	public void setPromotoria(String promotoria) {
		this.promotoria = promotoria;
	}

	public String getTipoCedulaAgente() {
		return tipoCedulaAgente;
	}

	public void setTipoCedulaAgente(String tipoCedulaAgente) {
		this.tipoCedulaAgente = tipoCedulaAgente;
	}

	public String getEjecutivo() {
		return ejecutivo;
	}

	public void setEjecutivo(String ejecutivo) {
		this.ejecutivo = ejecutivo;
	}

	public String getTipoSituacion() {
		return tipoSituacion;
	}

	public void setTipoSituacion(String tipoSituacion) {
		this.tipoSituacion = tipoSituacion;
	}

	public String getGerencia() {
		return gerencia;
	}

	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}

	public String getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}

	public String getCentroOperacion() {
		return centroOperacion;
	}

	public void setCentroOperacion(String centroOperacion) {
		this.centroOperacion = centroOperacion;
	}

	@Temporal(TemporalType.DATE)
	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	@Temporal(TemporalType.DATE)
	public Date getFechaActivacion() {
		return fechaActivacion;
	}

	public void setFechaActivacion(Date fechaActivacion) {
		this.fechaActivacion = fechaActivacion;
	}

	public String getTipoAgente() {
		return tipoAgente;
	}

	public void setTipoAgente(String tipoAgente) {
		this.tipoAgente = tipoAgente;
	}

	public String getTipoPromotoria() {
		return tipoPromotoria;
	}

	public void setTipoPromotoria(String tipoPromotoria) {
		this.tipoPromotoria = tipoPromotoria;
	}
	
	public Long getIdMovimiento() {
		return idMovimiento;
	}

	public void setIdMovimiento(Long idMovimiento) {
		this.idMovimiento = idMovimiento;
	}

	public Long getAnioMes() {
		return anioMes;
	}

	public void setAnioMes(Long anioMes) {
		this.anioMes = anioMes;
	}

//	@Temporal(TemporalType.DATE)
	public String getFechaMovimiento() {
		return fechaMovimiento;
	}

	public void setFechaMovimiento(String fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
	}

	public Double getImporteBaseCalculo() {
		return importeBaseCalculo;
	}

	public void setImporteBaseCalculo(Double importeBaseCalculo) {
		this.importeBaseCalculo = importeBaseCalculo;
	}

	public Double getImporteIva() {
		return importeIva;
	}

	public void setImporteIva(Double importeIva) {
		this.importeIva = importeIva;
	}

	public Double getImporteIvaRetenido() {
		return importeIvaRetenido;
	}

	public void setImporteIvaRetenido(Double importeIvaRetenido) {
		this.importeIvaRetenido = importeIvaRetenido;
	}

	public Double getImporteISR() {
		return importeISR;
	}

	public void setImporteISR(Double importeISR) {
		this.importeISR = importeISR;
	}
	public Long getIdPromotoria() {
		return idPromotoria;
	}

	public void setIdPromotoria(Long idPromotoria) {
		this.idPromotoria = idPromotoria;
	}

	public Long getIdEjecutivo() {
		return idEjecutivo;
	}

	public void setIdEjecutivo(Long idEjecutivo) {
		this.idEjecutivo = idEjecutivo;
	}

	public Long getIdGerencia() {
		return idGerencia;
	}

	public void setIdGerencia(Long idGerencia) {
		this.idGerencia = idGerencia;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public BigDecimal getImporteTotal() {
		return importeTotal;
	}

	public void setImporteTotal(BigDecimal importeTotal) {
		this.importeTotal = importeTotal;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getTr() {
		return tr;
	}

	public void setTr(String tr) {
		this.tr = tr;
	}

	public Double getImporteEstatalRetenido() {
		return importeEstatalRetenido;
	}

	public void setImporteEstatalRetenido(Double importeEstatalRetenido) {
		this.importeEstatalRetenido = importeEstatalRetenido;
	}

}
