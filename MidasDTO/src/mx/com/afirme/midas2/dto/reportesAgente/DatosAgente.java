package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.calculos.CalculoComisiones;
import mx.com.afirme.midas2.domain.calculos.DetalleCalculoComisiones;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.prestamos.ConfigPrestamoAnticipo;

@Entity
public class DatosAgente implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3578372857758400182L;
	private Long id;
	private Agente agente;
	private CentroOperacion centroOperacion;
	private Gerencia gerencia;
	private Ejecutivo ejecutivo;
	private String tipoAgente;
	private String tipoCedula;
	private String tipoReporte;
	private String tipoPromotoria;
	private String estatusAgente;
	private String prioridad;
	private CalculoComisiones calculoComisiones;
	private DetalleCalculoComisiones detalleCalculoComisiones;
	private Date fechaCorte;
	private String lineaVenta;
	private String producto;
	private String ramo;
	private String subRamo;
	private String lineaNegocio;
	private String cobertura;
	private ConfigPrestamoAnticipo configPrestamoAnticipo;
	private Date fechaInicioVigencia;
	private Date fechaFinVigencia;
	private ConfigBonos configBonos;
	
	@Id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	public CentroOperacion getCentroOperacion() {
		return centroOperacion;
	}

	public void setCentroOperacion(CentroOperacion centroOperacion) {
		this.centroOperacion = centroOperacion;
	}

	public Gerencia getGerencia() {
		return gerencia;
	}

	public void setGerencia(Gerencia gerencia) {
		this.gerencia = gerencia;
	}

	public String getTipoAgente() {
		return tipoAgente;
	}

	public void setTipoAgente(String tipoAgente) {
		this.tipoAgente = tipoAgente;
	}

	public String getTipoCedula() {
		return tipoCedula;
	}

	public void setTipoCedula(String tipoCedula) {
		this.tipoCedula = tipoCedula;
	}

	public String getTipoReporte() {
		return tipoReporte;
	}

	public void setTipoReporte(String tipoReporte) {
		this.tipoReporte = tipoReporte;
	}

	public String getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}

	public Ejecutivo getEjecutivo() {
		return ejecutivo;
	}

	public void setEjecutivo(Ejecutivo ejecutivo) {
		this.ejecutivo = ejecutivo;
	}

	public String getTipoPromotoria() {
		return tipoPromotoria;
	}

	public void setTipoPromotoria(String tipoPromotoria) {
		this.tipoPromotoria = tipoPromotoria;
	}

	public String getEstatusAgente() {
		return estatusAgente;
	}

	public void setEstatusAgente(String estatusAgente) {
		this.estatusAgente = estatusAgente;
	}

	public CalculoComisiones getCalculoComisiones() {
		return calculoComisiones;
	}

	public void setCalculoComisiones(CalculoComisiones calculoComisiones) {
		this.calculoComisiones = calculoComisiones;
	}

	public DetalleCalculoComisiones getDetalleCalculoComisiones() {
		return detalleCalculoComisiones;
	}

	@Temporal(TemporalType.DATE)
	public Date getFechaCorte() {
		return fechaCorte;
	}

	public void setFechaCorte(Date fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	public void setDetalleCalculoComisiones(
			DetalleCalculoComisiones detalleCalculoComisiones) {
		this.detalleCalculoComisiones = detalleCalculoComisiones;
	}

	public String getLineaVenta() {
		return lineaVenta;
	}

	public void setLineaVenta(String lineaVenta) {
		this.lineaVenta = lineaVenta;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getRamo() {
		return ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}

	public String getSubRamo() {
		return subRamo;
	}

	public void setSubRamo(String subRamo) {
		this.subRamo = subRamo;
	}

	public String getLineaNegocio() {
		return lineaNegocio;
	}

	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}

	public String getCobertura() {
		return cobertura;
	}

	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}

	public ConfigPrestamoAnticipo getConfigPrestamoAnticipo() {
		return configPrestamoAnticipo;
	}

	public void setConfigPrestamoAnticipo(
			ConfigPrestamoAnticipo configPrestamoAnticipo) {
		this.configPrestamoAnticipo = configPrestamoAnticipo;
	}

	@Temporal(TemporalType.DATE)
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	@Temporal(TemporalType.DATE)
	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}

	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	public ConfigBonos getConfigBonos() {
		return configBonos;
	}

	public void setConfigBonos(ConfigBonos configBonos) {
		this.configBonos = configBonos;
	}

}
