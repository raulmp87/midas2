package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class DatosEdoctaAgenteSiniestralidad implements Serializable{

	private static final long serialVersionUID = 1L;
	private Long id;
	private Double anual;
	private Double mensual;
	private Double actual;
	private String anualstr;
	private String mensualstr;
	private String actualstr;
	
	@Id
	public Long getId() {
		return id;
	}
	public Double getAnual() {
		return anual;
	}
	public Double getMensual() {
		return mensual;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setAnual(Double anual) {
		this.anual = anual;
	}
	public void setMensual(Double mensual) {
		this.mensual = mensual;
	}
	public Double getActual() {
		return actual;
	}
	public void setActual(Double actual) {
		this.actual = actual;
	}
	public String getAnualstr() {
		return anualstr;
	}
	public void setAnualstr(String anualstr) {
		this.anualstr = anualstr;
	}
	public String getMensualstr() {
		return mensualstr;
	}
	public void setMensualstr(String mensualstr) {
		this.mensualstr = mensualstr;
	}
	public String getActualstr() {
		return actualstr;
	}
	public void setActualstr(String actualstr) {
		this.actualstr = actualstr;
	}
	
}
