package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;
import java.math.BigDecimal;

public class DatosVentasProduccion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3450734454546892534L;
	
	public static final String PLANTILLA_NAME = "midas.agente.reporte.agente.ventasProduccion.archivo.nombre";
	
	//PeriodoActual
	private	String		producto;
	private	BigDecimal	primaPagadaEne;
	private	BigDecimal	exclusionesEne;
	private	BigDecimal	primaPagadaBonoEne;
	private	BigDecimal	bonoProduccionEne;
	private	BigDecimal	bonoSiniestralidadEne;
	private	BigDecimal	bonoCrecimientoEne;
	private	BigDecimal	primaPagadaFeb;
	private	BigDecimal	exclusionesFeb;
	private	BigDecimal	primaPagadaBonoFeb;
	private	BigDecimal	bonoProduccionFeb;
	private	BigDecimal	bonoSiniestralidadFeb;
	private	BigDecimal	bonoCrecimientoFeb;
	private	BigDecimal	primaPagadaMar;
	private	BigDecimal	exclusionesMar;
	private	BigDecimal	primaPagadaBonoMar;
	private	BigDecimal	bonoProduccionMar;
	private	BigDecimal	bonoSiniestralidadMar;
	private	BigDecimal	bonoCrecimientoMar;
	private	BigDecimal	primaPagadaAbr;
	private	BigDecimal	exclusionesAbr;
	private	BigDecimal	primaPagadaBonoAbr;
	private	BigDecimal	bonoProduccionAbr;
	private	BigDecimal	bonoSiniestralidadAbr;
	private	BigDecimal	bonoCrecimientoAbr;
	private	BigDecimal	primaPagadaMay;
	private	BigDecimal	exclusionesMay;
	private	BigDecimal	primaPagadaBonoMay;
	private	BigDecimal	bonoProduccionMay;
	private	BigDecimal	bonoSiniestralidadMay;
	private	BigDecimal	bonoCrecimientoMay;
	private	BigDecimal	primaPagadaJun;
	private	BigDecimal	exclusionesJun;
	private	BigDecimal	primaPagadaBonoJun;
	private	BigDecimal	bonoProduccionJun;
	private	BigDecimal	bonoSiniestralidadJun;
	private	BigDecimal	bonoCrecimientoJun;
	private	BigDecimal	primaPagadaJul;
	private	BigDecimal	exclusionesJul;
	private	BigDecimal	primaPagadaBonoJul;
	private	BigDecimal	bonoProduccionJul;
	private	BigDecimal	bonoSiniestralidadJul;
	private	BigDecimal	bonoCrecimientoJul;
	private	BigDecimal	primaPagadaAgo;
	private	BigDecimal	exclusionesAgo;
	private	BigDecimal	primaPagadaBonoAgo;
	private	BigDecimal	bonoProduccionAgo;
	private	BigDecimal	bonoSiniestralidadAgo;
	private	BigDecimal	bonoCrecimientoAgo;
	private	BigDecimal	primaPagadaSep;
	private	BigDecimal	exclusionesSep;
	private	BigDecimal	primaPagadaBonoSep;
	private	BigDecimal	bonoProduccionSep;
	private	BigDecimal	bonoSiniestralidadSep;
	private	BigDecimal	bonoCrecimientoSep;
	private	BigDecimal	primaPagadaOct;
	private	BigDecimal	exclusionesOct;
	private	BigDecimal	primaPagadaBonoOct;
	private	BigDecimal	bonoProduccionOct;
	private	BigDecimal	bonoSiniestralidadOct;
	private	BigDecimal	bonoCrecimientoOct;
	private	BigDecimal	primaPagadaNov;
	private	BigDecimal	exclusionesNov;
	private	BigDecimal	primaPagadaBonoNov;
	private	BigDecimal	bonoProduccionNov;
	private	BigDecimal	bonoSiniestralidadNov;
	private	BigDecimal	bonoCrecimientoNov;
	private	BigDecimal	primaPagadaDic;
	private	BigDecimal	exclusionesDic;
	private	BigDecimal	primaPagadaBonoDic;
	private	BigDecimal	bonoProduccionDic;
	private	BigDecimal	bonoSiniestralidadDic;
	private	BigDecimal	bonoCrecimientoDic;
	private Long periodoActual;
	private Long periodoAnterior;
	
	/*periodo de comparacion*/
	private	BigDecimal	paPrimaPagadaEne;
	private	BigDecimal	paExclusionesEne;
	private	BigDecimal	paPrimaPagadaBonoEne;
	private	BigDecimal	paBonoProduccionEne;
	private	BigDecimal	paBonoSiniestralidadEne;
	private	BigDecimal	paBonoCrecimientoEne;
	private	BigDecimal	paPrimaPagadaFeb;
	private	BigDecimal	paExclusionesFeb;
	private	BigDecimal	paPrimaPagadaBonoFeb;
	private	BigDecimal	paBonoProduccionFeb;
	private	BigDecimal	paBonoSiniestralidadFeb;
	private	BigDecimal	paBonoCrecimientoFeb;
	private	BigDecimal	paPrimaPagadaMar;
	private	BigDecimal	paExclusionesMar;
	private	BigDecimal	paPrimaPagadaBonoMar;
	private	BigDecimal	paBonoProduccionMar;
	private	BigDecimal	paBonoSiniestralidadMar;
	private	BigDecimal	paBonoCrecimientoMar;
	private	BigDecimal	paPrimaPagadaAbr;
	private	BigDecimal	paExclusionesAbr;
	private	BigDecimal	paPrimaPagadaBonoAbr;
	private	BigDecimal	paBonoProduccionAbr;
	private	BigDecimal	paBonoSiniestralidadAbr;
	private	BigDecimal	paBonoCrecimientoAbr;
	private	BigDecimal	paPrimaPagadaMay;
	private	BigDecimal	paExclusionesMay;
	private	BigDecimal	paPrimaPagadaBonoMay;
	private	BigDecimal	paBonoProduccionMay;
	private	BigDecimal	paBonoSiniestralidadMay;
	private	BigDecimal	paBonoCrecimientoMay;
	private	BigDecimal	paPrimaPagadaJun;
	private	BigDecimal	paExclusionesJun;
	private	BigDecimal	paPrimaPagadaBonoJun;
	private	BigDecimal	paBonoProduccionJun;
	private	BigDecimal	paBonoSiniestralidadJun;
	private	BigDecimal	paBonoCrecimientoJun;
	private	BigDecimal	paPrimaPagadaJul;
	private	BigDecimal	paExclusionesJul;
	private	BigDecimal	paPrimaPagadaBonoJul;
	private	BigDecimal	paBonoProduccionJul;
	private	BigDecimal	paBonoSiniestralidadJul;
	private	BigDecimal	paBonoCrecimientoJul;
	private	BigDecimal	paPrimaPagadaAgo;
	private	BigDecimal	paExclusionesAgo;
	private	BigDecimal	paPrimaPagadaBonoAgo;
	private	BigDecimal	paBonoProduccionAgo;
	private	BigDecimal	paBonoSiniestralidadAgo;
	private	BigDecimal	paBonoCrecimientoAgo;
	private	BigDecimal	paPrimaPagadaSep;
	private	BigDecimal	paExclusionesSep;
	private	BigDecimal	paPrimaPagadaBonoSep;
	private	BigDecimal	paBonoProduccionSep;
	private	BigDecimal	paBonoSiniestralidadSep;
	private	BigDecimal	paBonoCrecimientoSep;
	private	BigDecimal	paPrimaPagadaOct;
	private	BigDecimal	paExclusionesOct;
	private	BigDecimal	paPrimaPagadaBonoOct;
	private	BigDecimal	paBonoProduccionOct;
	private	BigDecimal	paBonoSiniestralidadOct;
	private	BigDecimal	paBonoCrecimientoOct;
	private	BigDecimal	paPrimaPagadaNov;
	private	BigDecimal	paExclusionesNov;
	private	BigDecimal	paPrimaPagadaBonoNov;
	private	BigDecimal	paBonoProduccionNov;
	private	BigDecimal	paBonoSiniestralidadNov;
	private	BigDecimal	paBonoCrecimientoNov;
	private	BigDecimal	paPrimaPagadaDic;
	private	BigDecimal	paExclusionesDic;
	private	BigDecimal	paPrimaPagadaBonoDic;
	private	BigDecimal	paBonoProduccionDic;
	private	BigDecimal	paBonoSiniestralidadDic;
	private	BigDecimal	paBonoCrecimientoDic;
		
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public BigDecimal getPrimaPagadaEne() {
		return primaPagadaEne;
	}
	public void setPrimaPagadaEne(BigDecimal primaPagadaEne) {
		this.primaPagadaEne = primaPagadaEne;
	}
	public BigDecimal getExclusionesEne() {
		return exclusionesEne;
	}
	public void setExclusionesEne(BigDecimal exclusionesEne) {
		this.exclusionesEne = exclusionesEne;
	}
	public BigDecimal getPrimaPagadaBonoEne() {
		return primaPagadaBonoEne;
	}
	public void setPrimaPagadaBonoEne(BigDecimal primaPagadaBonoEne) {
		this.primaPagadaBonoEne = primaPagadaBonoEne;
	}
	public BigDecimal getBonoProduccionEne() {
		return bonoProduccionEne;
	}
	public void setBonoProduccionEne(BigDecimal bonoProduccionEne) {
		this.bonoProduccionEne = bonoProduccionEne;
	}
	public BigDecimal getBonoSiniestralidadEne() {
		return bonoSiniestralidadEne;
	}
	public void setBonoSiniestralidadEne(BigDecimal bonoSiniestralidadEne) {
		this.bonoSiniestralidadEne = bonoSiniestralidadEne;
	}
	public BigDecimal getBonoCrecimientoEne() {
		return bonoCrecimientoEne;
	}
	public void setBonoCrecimientoEne(BigDecimal bonoCrecimientoEne) {
		this.bonoCrecimientoEne = bonoCrecimientoEne;
	}
	public BigDecimal getPrimaPagadaFeb() {
		return primaPagadaFeb;
	}
	public void setPrimaPagadaFeb(BigDecimal primaPagadaFeb) {
		this.primaPagadaFeb = primaPagadaFeb;
	}
	public BigDecimal getExclusionesFeb() {
		return exclusionesFeb;
	}
	public void setExclusionesFeb(BigDecimal exclusionesFeb) {
		this.exclusionesFeb = exclusionesFeb;
	}
	public BigDecimal getPrimaPagadaBonoFeb() {
		return primaPagadaBonoFeb;
	}
	public void setPrimaPagadaBonoFeb(BigDecimal primaPagadaBonoFeb) {
		this.primaPagadaBonoFeb = primaPagadaBonoFeb;
	}
	public BigDecimal getBonoProduccionFeb() {
		return bonoProduccionFeb;
	}
	public void setBonoProduccionFeb(BigDecimal bonoProduccionFeb) {
		this.bonoProduccionFeb = bonoProduccionFeb;
	}
	public BigDecimal getBonoSiniestralidadFeb() {
		return bonoSiniestralidadFeb;
	}
	public void setBonoSiniestralidadFeb(BigDecimal bonoSiniestralidadFeb) {
		this.bonoSiniestralidadFeb = bonoSiniestralidadFeb;
	}
	public BigDecimal getBonoCrecimientoFeb() {
		return bonoCrecimientoFeb;
	}
	public void setBonoCrecimientoFeb(BigDecimal bonoCrecimientoFeb) {
		this.bonoCrecimientoFeb = bonoCrecimientoFeb;
	}
	public BigDecimal getPrimaPagadaMar() {
		return primaPagadaMar;
	}
	public void setPrimaPagadaMar(BigDecimal primaPagadaMar) {
		this.primaPagadaMar = primaPagadaMar;
	}
	public BigDecimal getExclusionesMar() {
		return exclusionesMar;
	}
	public void setExclusionesMar(BigDecimal exclusionesMar) {
		this.exclusionesMar = exclusionesMar;
	}
	public BigDecimal getPrimaPagadaBonoMar() {
		return primaPagadaBonoMar;
	}
	public void setPrimaPagadaBonoMar(BigDecimal primaPagadaBonoMar) {
		this.primaPagadaBonoMar = primaPagadaBonoMar;
	}
	public BigDecimal getBonoProduccionMar() {
		return bonoProduccionMar;
	}
	public void setBonoProduccionMar(BigDecimal bonoProduccionMar) {
		this.bonoProduccionMar = bonoProduccionMar;
	}
	public BigDecimal getBonoSiniestralidadMar() {
		return bonoSiniestralidadMar;
	}
	public void setBonoSiniestralidadMar(BigDecimal bonoSiniestralidadMar) {
		this.bonoSiniestralidadMar = bonoSiniestralidadMar;
	}
	public BigDecimal getBonoCrecimientoMar() {
		return bonoCrecimientoMar;
	}
	public void setBonoCrecimientoMar(BigDecimal bonoCrecimientoMar) {
		this.bonoCrecimientoMar = bonoCrecimientoMar;
	}
	public BigDecimal getPrimaPagadaAbr() {
		return primaPagadaAbr;
	}
	public void setPrimaPagadaAbr(BigDecimal primaPagadaAbr) {
		this.primaPagadaAbr = primaPagadaAbr;
	}
	public BigDecimal getExclusionesAbr() {
		return exclusionesAbr;
	}
	public void setExclusionesAbr(BigDecimal exclusionesAbr) {
		this.exclusionesAbr = exclusionesAbr;
	}
	public BigDecimal getPrimaPagadaBonoAbr() {
		return primaPagadaBonoAbr;
	}
	public void setPrimaPagadaBonoAbr(BigDecimal primaPagadaBonoAbr) {
		this.primaPagadaBonoAbr = primaPagadaBonoAbr;
	}
	public BigDecimal getBonoProduccionAbr() {
		return bonoProduccionAbr;
	}
	public void setBonoProduccionAbr(BigDecimal bonoProduccionAbr) {
		this.bonoProduccionAbr = bonoProduccionAbr;
	}
	public BigDecimal getBonoSiniestralidadAbr() {
		return bonoSiniestralidadAbr;
	}
	public void setBonoSiniestralidadAbr(BigDecimal bonoSiniestralidadAbr) {
		this.bonoSiniestralidadAbr = bonoSiniestralidadAbr;
	}
	public BigDecimal getBonoCrecimientoAbr() {
		return bonoCrecimientoAbr;
	}
	public void setBonoCrecimientoAbr(BigDecimal bonoCrecimientoAbr) {
		this.bonoCrecimientoAbr = bonoCrecimientoAbr;
	}
	public BigDecimal getPrimaPagadaMay() {
		return primaPagadaMay;
	}
	public void setPrimaPagadaMay(BigDecimal primaPagadaMay) {
		this.primaPagadaMay = primaPagadaMay;
	}
	public BigDecimal getExclusionesMay() {
		return exclusionesMay;
	}
	public void setExclusionesMay(BigDecimal exclusionesMay) {
		this.exclusionesMay = exclusionesMay;
	}
	public BigDecimal getPrimaPagadaBonoMay() {
		return primaPagadaBonoMay;
	}
	public void setPrimaPagadaBonoMay(BigDecimal primaPagadaBonoMay) {
		this.primaPagadaBonoMay = primaPagadaBonoMay;
	}
	public BigDecimal getBonoProduccionMay() {
		return bonoProduccionMay;
	}
	public void setBonoProduccionMay(BigDecimal bonoProduccionMay) {
		this.bonoProduccionMay = bonoProduccionMay;
	}
	public BigDecimal getBonoSiniestralidadMay() {
		return bonoSiniestralidadMay;
	}
	public void setBonoSiniestralidadMay(BigDecimal bonoSiniestralidadMay) {
		this.bonoSiniestralidadMay = bonoSiniestralidadMay;
	}
	public BigDecimal getBonoCrecimientoMay() {
		return bonoCrecimientoMay;
	}
	public void setBonoCrecimientoMay(BigDecimal bonoCrecimientoMay) {
		this.bonoCrecimientoMay = bonoCrecimientoMay;
	}
	public BigDecimal getPrimaPagadaJun() {
		return primaPagadaJun;
	}
	public void setPrimaPagadaJun(BigDecimal primaPagadaJun) {
		this.primaPagadaJun = primaPagadaJun;
	}
	public BigDecimal getExclusionesJun() {
		return exclusionesJun;
	}
	public void setExclusionesJun(BigDecimal exclusionesJun) {
		this.exclusionesJun = exclusionesJun;
	}
	public BigDecimal getPrimaPagadaBonoJun() {
		return primaPagadaBonoJun;
	}
	public void setPrimaPagadaBonoJun(BigDecimal primaPagadaBonoJun) {
		this.primaPagadaBonoJun = primaPagadaBonoJun;
	}
	public BigDecimal getBonoProduccionJun() {
		return bonoProduccionJun;
	}
	public void setBonoProduccionJun(BigDecimal bonoProduccionJun) {
		this.bonoProduccionJun = bonoProduccionJun;
	}
	public BigDecimal getBonoSiniestralidadJun() {
		return bonoSiniestralidadJun;
	}
	public void setBonoSiniestralidadJun(BigDecimal bonoSiniestralidadJun) {
		this.bonoSiniestralidadJun = bonoSiniestralidadJun;
	}
	public BigDecimal getBonoCrecimientoJun() {
		return bonoCrecimientoJun;
	}
	public void setBonoCrecimientoJun(BigDecimal bonoCrecimientoJun) {
		this.bonoCrecimientoJun = bonoCrecimientoJun;
	}
	public BigDecimal getPrimaPagadaJul() {
		return primaPagadaJul;
	}
	public void setPrimaPagadaJul(BigDecimal primaPagadaJul) {
		this.primaPagadaJul = primaPagadaJul;
	}
	public BigDecimal getExclusionesJul() {
		return exclusionesJul;
	}
	public void setExclusionesJul(BigDecimal exclusionesJul) {
		this.exclusionesJul = exclusionesJul;
	}
	public BigDecimal getPrimaPagadaBonoJul() {
		return primaPagadaBonoJul;
	}
	public void setPrimaPagadaBonoJul(BigDecimal primaPagadaBonoJul) {
		this.primaPagadaBonoJul = primaPagadaBonoJul;
	}
	public BigDecimal getBonoProduccionJul() {
		return bonoProduccionJul;
	}
	public void setBonoProduccionJul(BigDecimal bonoProduccionJul) {
		this.bonoProduccionJul = bonoProduccionJul;
	}
	public BigDecimal getBonoSiniestralidadJul() {
		return bonoSiniestralidadJul;
	}
	public void setBonoSiniestralidadJul(BigDecimal bonoSiniestralidadJul) {
		this.bonoSiniestralidadJul = bonoSiniestralidadJul;
	}
	public BigDecimal getBonoCrecimientoJul() {
		return bonoCrecimientoJul;
	}
	public void setBonoCrecimientoJul(BigDecimal bonoCrecimientoJul) {
		this.bonoCrecimientoJul = bonoCrecimientoJul;
	}
	public BigDecimal getPrimaPagadaAgo() {
		return primaPagadaAgo;
	}
	public void setPrimaPagadaAgo(BigDecimal primaPagadaAgo) {
		this.primaPagadaAgo = primaPagadaAgo;
	}
	public BigDecimal getExclusionesAgo() {
		return exclusionesAgo;
	}
	public void setExclusionesAgo(BigDecimal exclusionesAgo) {
		this.exclusionesAgo = exclusionesAgo;
	}
	public BigDecimal getPrimaPagadaBonoAgo() {
		return primaPagadaBonoAgo;
	}
	public void setPrimaPagadaBonoAgo(BigDecimal primaPagadaBonoAgo) {
		this.primaPagadaBonoAgo = primaPagadaBonoAgo;
	}
	public BigDecimal getBonoProduccionAgo() {
		return bonoProduccionAgo;
	}
	public void setBonoProduccionAgo(BigDecimal bonoProduccionAgo) {
		this.bonoProduccionAgo = bonoProduccionAgo;
	}
	public BigDecimal getBonoSiniestralidadAgo() {
		return bonoSiniestralidadAgo;
	}
	public void setBonoSiniestralidadAgo(BigDecimal bonoSiniestralidadAgo) {
		this.bonoSiniestralidadAgo = bonoSiniestralidadAgo;
	}
	public BigDecimal getBonoCrecimientoAgo() {
		return bonoCrecimientoAgo;
	}
	public void setBonoCrecimientoAgo(BigDecimal bonoCrecimientoAgo) {
		this.bonoCrecimientoAgo = bonoCrecimientoAgo;
	}
	public BigDecimal getPrimaPagadaSep() {
		return primaPagadaSep;
	}
	public void setPrimaPagadaSep(BigDecimal primaPagadaSep) {
		this.primaPagadaSep = primaPagadaSep;
	}
	public BigDecimal getExclusionesSep() {
		return exclusionesSep;
	}
	public void setExclusionesSep(BigDecimal exclusionesSep) {
		this.exclusionesSep = exclusionesSep;
	}
	public BigDecimal getPrimaPagadaBonoSep() {
		return primaPagadaBonoSep;
	}
	public void setPrimaPagadaBonoSep(BigDecimal primaPagadaBonoSep) {
		this.primaPagadaBonoSep = primaPagadaBonoSep;
	}
	public BigDecimal getBonoProduccionSep() {
		return bonoProduccionSep;
	}
	public void setBonoProduccionSep(BigDecimal bonoProduccionSep) {
		this.bonoProduccionSep = bonoProduccionSep;
	}
	public BigDecimal getBonoSiniestralidadSep() {
		return bonoSiniestralidadSep;
	}
	public void setBonoSiniestralidadSep(BigDecimal bonoSiniestralidadSep) {
		this.bonoSiniestralidadSep = bonoSiniestralidadSep;
	}
	public BigDecimal getBonoCrecimientoSep() {
		return bonoCrecimientoSep;
	}
	public void setBonoCrecimientoSep(BigDecimal bonoCrecimientoSep) {
		this.bonoCrecimientoSep = bonoCrecimientoSep;
	}
	public BigDecimal getPrimaPagadaOct() {
		return primaPagadaOct;
	}
	public void setPrimaPagadaOct(BigDecimal primaPagadaOct) {
		this.primaPagadaOct = primaPagadaOct;
	}
	public BigDecimal getExclusionesOct() {
		return exclusionesOct;
	}
	public void setExclusionesOct(BigDecimal exclusionesOct) {
		this.exclusionesOct = exclusionesOct;
	}
	public BigDecimal getPrimaPagadaBonoOct() {
		return primaPagadaBonoOct;
	}
	public void setPrimaPagadaBonoOct(BigDecimal primaPagadaBonoOct) {
		this.primaPagadaBonoOct = primaPagadaBonoOct;
	}
	public BigDecimal getBonoProduccionOct() {
		return bonoProduccionOct;
	}
	public void setBonoProduccionOct(BigDecimal bonoProduccionOct) {
		this.bonoProduccionOct = bonoProduccionOct;
	}
	public BigDecimal getBonoSiniestralidadOct() {
		return bonoSiniestralidadOct;
	}
	public void setBonoSiniestralidadOct(BigDecimal bonoSiniestralidadOct) {
		this.bonoSiniestralidadOct = bonoSiniestralidadOct;
	}
	public BigDecimal getBonoCrecimientoOct() {
		return bonoCrecimientoOct;
	}
	public void setBonoCrecimientoOct(BigDecimal bonoCrecimientoOct) {
		this.bonoCrecimientoOct = bonoCrecimientoOct;
	}
	public BigDecimal getPrimaPagadaNov() {
		return primaPagadaNov;
	}
	public void setPrimaPagadaNov(BigDecimal primaPagadaNov) {
		this.primaPagadaNov = primaPagadaNov;
	}
	public BigDecimal getExclusionesNov() {
		return exclusionesNov;
	}
	public void setExclusionesNov(BigDecimal exclusionesNov) {
		this.exclusionesNov = exclusionesNov;
	}
	public BigDecimal getPrimaPagadaBonoNov() {
		return primaPagadaBonoNov;
	}
	public void setPrimaPagadaBonoNov(BigDecimal primaPagadaBonoNov) {
		this.primaPagadaBonoNov = primaPagadaBonoNov;
	}
	public BigDecimal getBonoProduccionNov() {
		return bonoProduccionNov;
	}
	public void setBonoProduccionNov(BigDecimal bonoProduccionNov) {
		this.bonoProduccionNov = bonoProduccionNov;
	}
	public BigDecimal getBonoSiniestralidadNov() {
		return bonoSiniestralidadNov;
	}
	public void setBonoSiniestralidadNov(BigDecimal bonoSiniestralidadNov) {
		this.bonoSiniestralidadNov = bonoSiniestralidadNov;
	}
	public BigDecimal getBonoCrecimientoNov() {
		return bonoCrecimientoNov;
	}
	public void setBonoCrecimientoNov(BigDecimal bonoCrecimientoNov) {
		this.bonoCrecimientoNov = bonoCrecimientoNov;
	}
	public BigDecimal getPrimaPagadaDic() {
		return primaPagadaDic;
	}
	public void setPrimaPagadaDic(BigDecimal primaPagadaDic) {
		this.primaPagadaDic = primaPagadaDic;
	}
	public BigDecimal getExclusionesDic() {
		return exclusionesDic;
	}
	public void setExclusionesDic(BigDecimal exclusionesDic) {
		this.exclusionesDic = exclusionesDic;
	}
	public BigDecimal getPrimaPagadaBonoDic() {
		return primaPagadaBonoDic;
	}
	public void setPrimaPagadaBonoDic(BigDecimal primaPagadaBonoDic) {
		this.primaPagadaBonoDic = primaPagadaBonoDic;
	}
	public BigDecimal getBonoProduccionDic() {
		return bonoProduccionDic;
	}
	public void setBonoProduccionDic(BigDecimal bonoProduccionDic) {
		this.bonoProduccionDic = bonoProduccionDic;
	}
	public BigDecimal getBonoSiniestralidadDic() {
		return bonoSiniestralidadDic;
	}
	public void setBonoSiniestralidadDic(BigDecimal bonoSiniestralidadDic) {
		this.bonoSiniestralidadDic = bonoSiniestralidadDic;
	}
	public BigDecimal getBonoCrecimientoDic() {
		return bonoCrecimientoDic;
	}
	public void setBonoCrecimientoDic(BigDecimal bonoCrecimientoDic) {
		this.bonoCrecimientoDic = bonoCrecimientoDic;
	}
	public Long getPeriodoActual() {
		return periodoActual;
	}
	public void setPeriodoActual(Long periodoActual) {
		this.periodoActual = periodoActual;
	}
	public Long getPeriodoAnterior() {
		return periodoAnterior;
	}
	public void setPeriodoAnterior(Long periodoAnterior) {
		this.periodoAnterior = periodoAnterior;
	}
	public BigDecimal getPaPrimaPagadaEne() {
		return paPrimaPagadaEne;
	}
	public void setPaPrimaPagadaEne(BigDecimal paPrimaPagadaEne) {
		this.paPrimaPagadaEne = paPrimaPagadaEne;
	}
	public BigDecimal getPaExclusionesEne() {
		return paExclusionesEne;
	}
	public void setPaExclusionesEne(BigDecimal paExclusionesEne) {
		this.paExclusionesEne = paExclusionesEne;
	}
	public BigDecimal getPaPrimaPagadaBonoEne() {
		return paPrimaPagadaBonoEne;
	}
	public void setPaPrimaPagadaBonoEne(BigDecimal paPrimaPagadaBonoEne) {
		this.paPrimaPagadaBonoEne = paPrimaPagadaBonoEne;
	}
	public BigDecimal getPaBonoProduccionEne() {
		return paBonoProduccionEne;
	}
	public void setPaBonoProduccionEne(BigDecimal paBonoProduccionEne) {
		this.paBonoProduccionEne = paBonoProduccionEne;
	}
	public BigDecimal getPaBonoSiniestralidadEne() {
		return paBonoSiniestralidadEne;
	}
	public void setPaBonoSiniestralidadEne(BigDecimal paBonoSiniestralidadEne) {
		this.paBonoSiniestralidadEne = paBonoSiniestralidadEne;
	}
	public BigDecimal getPaBonoCrecimientoEne() {
		return paBonoCrecimientoEne;
	}
	public void setPaBonoCrecimientoEne(BigDecimal paBonoCrecimientoEne) {
		this.paBonoCrecimientoEne = paBonoCrecimientoEne;
	}
	public BigDecimal getPaPrimaPagadaFeb() {
		return paPrimaPagadaFeb;
	}
	public void setPaPrimaPagadaFeb(BigDecimal paPrimaPagadaFeb) {
		this.paPrimaPagadaFeb = paPrimaPagadaFeb;
	}
	public BigDecimal getPaExclusionesFeb() {
		return paExclusionesFeb;
	}
	public void setPaExclusionesFeb(BigDecimal paExclusionesFeb) {
		this.paExclusionesFeb = paExclusionesFeb;
	}
	public BigDecimal getPaPrimaPagadaBonoFeb() {
		return paPrimaPagadaBonoFeb;
	}
	public void setPaPrimaPagadaBonoFeb(BigDecimal paPrimaPagadaBonoFeb) {
		this.paPrimaPagadaBonoFeb = paPrimaPagadaBonoFeb;
	}
	public BigDecimal getPaBonoProduccionFeb() {
		return paBonoProduccionFeb;
	}
	public void setPaBonoProduccionFeb(BigDecimal paBonoProduccionFeb) {
		this.paBonoProduccionFeb = paBonoProduccionFeb;
	}
	public BigDecimal getPaBonoSiniestralidadFeb() {
		return paBonoSiniestralidadFeb;
	}
	public void setPaBonoSiniestralidadFeb(BigDecimal paBonoSiniestralidadFeb) {
		this.paBonoSiniestralidadFeb = paBonoSiniestralidadFeb;
	}
	public BigDecimal getPaBonoCrecimientoFeb() {
		return paBonoCrecimientoFeb;
	}
	public void setPaBonoCrecimientoFeb(BigDecimal paBonoCrecimientoFeb) {
		this.paBonoCrecimientoFeb = paBonoCrecimientoFeb;
	}
	public BigDecimal getPaPrimaPagadaMar() {
		return paPrimaPagadaMar;
	}
	public void setPaPrimaPagadaMar(BigDecimal paPrimaPagadaMar) {
		this.paPrimaPagadaMar = paPrimaPagadaMar;
	}
	public BigDecimal getPaExclusionesMar() {
		return paExclusionesMar;
	}
	public void setPaExclusionesMar(BigDecimal paExclusionesMar) {
		this.paExclusionesMar = paExclusionesMar;
	}
	public BigDecimal getPaPrimaPagadaBonoMar() {
		return paPrimaPagadaBonoMar;
	}
	public void setPaPrimaPagadaBonoMar(BigDecimal paPrimaPagadaBonoMar) {
		this.paPrimaPagadaBonoMar = paPrimaPagadaBonoMar;
	}
	public BigDecimal getPaBonoProduccionMar() {
		return paBonoProduccionMar;
	}
	public void setPaBonoProduccionMar(BigDecimal paBonoProduccionMar) {
		this.paBonoProduccionMar = paBonoProduccionMar;
	}
	public BigDecimal getPaBonoSiniestralidadMar() {
		return paBonoSiniestralidadMar;
	}
	public void setPaBonoSiniestralidadMar(BigDecimal paBonoSiniestralidadMar) {
		this.paBonoSiniestralidadMar = paBonoSiniestralidadMar;
	}
	public BigDecimal getPaBonoCrecimientoMar() {
		return paBonoCrecimientoMar;
	}
	public void setPaBonoCrecimientoMar(BigDecimal paBonoCrecimientoMar) {
		this.paBonoCrecimientoMar = paBonoCrecimientoMar;
	}
	public BigDecimal getPaPrimaPagadaAbr() {
		return paPrimaPagadaAbr;
	}
	public void setPaPrimaPagadaAbr(BigDecimal paPrimaPagadaAbr) {
		this.paPrimaPagadaAbr = paPrimaPagadaAbr;
	}
	public BigDecimal getPaExclusionesAbr() {
		return paExclusionesAbr;
	}
	public void setPaExclusionesAbr(BigDecimal paExclusionesAbr) {
		this.paExclusionesAbr = paExclusionesAbr;
	}
	public BigDecimal getPaPrimaPagadaBonoAbr() {
		return paPrimaPagadaBonoAbr;
	}
	public void setPaPrimaPagadaBonoAbr(BigDecimal paPrimaPagadaBonoAbr) {
		this.paPrimaPagadaBonoAbr = paPrimaPagadaBonoAbr;
	}
	public BigDecimal getPaBonoProduccionAbr() {
		return paBonoProduccionAbr;
	}
	public void setPaBonoProduccionAbr(BigDecimal paBonoProduccionAbr) {
		this.paBonoProduccionAbr = paBonoProduccionAbr;
	}
	public BigDecimal getPaBonoSiniestralidadAbr() {
		return paBonoSiniestralidadAbr;
	}
	public void setPaBonoSiniestralidadAbr(BigDecimal paBonoSiniestralidadAbr) {
		this.paBonoSiniestralidadAbr = paBonoSiniestralidadAbr;
	}
	public BigDecimal getPaBonoCrecimientoAbr() {
		return paBonoCrecimientoAbr;
	}
	public void setPaBonoCrecimientoAbr(BigDecimal paBonoCrecimientoAbr) {
		this.paBonoCrecimientoAbr = paBonoCrecimientoAbr;
	}
	public BigDecimal getPaPrimaPagadaMay() {
		return paPrimaPagadaMay;
	}
	public void setPaPrimaPagadaMay(BigDecimal paPrimaPagadaMay) {
		this.paPrimaPagadaMay = paPrimaPagadaMay;
	}
	public BigDecimal getPaExclusionesMay() {
		return paExclusionesMay;
	}
	public void setPaExclusionesMay(BigDecimal paExclusionesMay) {
		this.paExclusionesMay = paExclusionesMay;
	}
	public BigDecimal getPaPrimaPagadaBonoMay() {
		return paPrimaPagadaBonoMay;
	}
	public void setPaPrimaPagadaBonoMay(BigDecimal paPrimaPagadaBonoMay) {
		this.paPrimaPagadaBonoMay = paPrimaPagadaBonoMay;
	}
	public BigDecimal getPaBonoProduccionMay() {
		return paBonoProduccionMay;
	}
	public void setPaBonoProduccionMay(BigDecimal paBonoProduccionMay) {
		this.paBonoProduccionMay = paBonoProduccionMay;
	}
	public BigDecimal getPaBonoSiniestralidadMay() {
		return paBonoSiniestralidadMay;
	}
	public void setPaBonoSiniestralidadMay(BigDecimal paBonoSiniestralidadMay) {
		this.paBonoSiniestralidadMay = paBonoSiniestralidadMay;
	}
	public BigDecimal getPaBonoCrecimientoMay() {
		return paBonoCrecimientoMay;
	}
	public void setPaBonoCrecimientoMay(BigDecimal paBonoCrecimientoMay) {
		this.paBonoCrecimientoMay = paBonoCrecimientoMay;
	}
	public BigDecimal getPaPrimaPagadaJun() {
		return paPrimaPagadaJun;
	}
	public void setPaPrimaPagadaJun(BigDecimal paPrimaPagadaJun) {
		this.paPrimaPagadaJun = paPrimaPagadaJun;
	}
	public BigDecimal getPaExclusionesJun() {
		return paExclusionesJun;
	}
	public void setPaExclusionesJun(BigDecimal paExclusionesJun) {
		this.paExclusionesJun = paExclusionesJun;
	}
	public BigDecimal getPaPrimaPagadaBonoJun() {
		return paPrimaPagadaBonoJun;
	}
	public void setPaPrimaPagadaBonoJun(BigDecimal paPrimaPagadaBonoJun) {
		this.paPrimaPagadaBonoJun = paPrimaPagadaBonoJun;
	}
	public BigDecimal getPaBonoProduccionJun() {
		return paBonoProduccionJun;
	}
	public void setPaBonoProduccionJun(BigDecimal paBonoProduccionJun) {
		this.paBonoProduccionJun = paBonoProduccionJun;
	}
	public BigDecimal getPaBonoSiniestralidadJun() {
		return paBonoSiniestralidadJun;
	}
	public void setPaBonoSiniestralidadJun(BigDecimal paBonoSiniestralidadJun) {
		this.paBonoSiniestralidadJun = paBonoSiniestralidadJun;
	}
	public BigDecimal getPaBonoCrecimientoJun() {
		return paBonoCrecimientoJun;
	}
	public void setPaBonoCrecimientoJun(BigDecimal paBonoCrecimientoJun) {
		this.paBonoCrecimientoJun = paBonoCrecimientoJun;
	}
	public BigDecimal getPaPrimaPagadaJul() {
		return paPrimaPagadaJul;
	}
	public void setPaPrimaPagadaJul(BigDecimal paPrimaPagadaJul) {
		this.paPrimaPagadaJul = paPrimaPagadaJul;
	}
	public BigDecimal getPaExclusionesJul() {
		return paExclusionesJul;
	}
	public void setPaExclusionesJul(BigDecimal paExclusionesJul) {
		this.paExclusionesJul = paExclusionesJul;
	}
	public BigDecimal getPaPrimaPagadaBonoJul() {
		return paPrimaPagadaBonoJul;
	}
	public void setPaPrimaPagadaBonoJul(BigDecimal paPrimaPagadaBonoJul) {
		this.paPrimaPagadaBonoJul = paPrimaPagadaBonoJul;
	}
	public BigDecimal getPaBonoProduccionJul() {
		return paBonoProduccionJul;
	}
	public void setPaBonoProduccionJul(BigDecimal paBonoProduccionJul) {
		this.paBonoProduccionJul = paBonoProduccionJul;
	}
	public BigDecimal getPaBonoSiniestralidadJul() {
		return paBonoSiniestralidadJul;
	}
	public void setPaBonoSiniestralidadJul(BigDecimal paBonoSiniestralidadJul) {
		this.paBonoSiniestralidadJul = paBonoSiniestralidadJul;
	}
	public BigDecimal getPaBonoCrecimientoJul() {
		return paBonoCrecimientoJul;
	}
	public void setPaBonoCrecimientoJul(BigDecimal paBonoCrecimientoJul) {
		this.paBonoCrecimientoJul = paBonoCrecimientoJul;
	}
	public BigDecimal getPaPrimaPagadaAgo() {
		return paPrimaPagadaAgo;
	}
	public void setPaPrimaPagadaAgo(BigDecimal paPrimaPagadaAgo) {
		this.paPrimaPagadaAgo = paPrimaPagadaAgo;
	}
	public BigDecimal getPaExclusionesAgo() {
		return paExclusionesAgo;
	}
	public void setPaExclusionesAgo(BigDecimal paExclusionesAgo) {
		this.paExclusionesAgo = paExclusionesAgo;
	}
	public BigDecimal getPaPrimaPagadaBonoAgo() {
		return paPrimaPagadaBonoAgo;
	}
	public void setPaPrimaPagadaBonoAgo(BigDecimal paPrimaPagadaBonoAgo) {
		this.paPrimaPagadaBonoAgo = paPrimaPagadaBonoAgo;
	}
	public BigDecimal getPaBonoProduccionAgo() {
		return paBonoProduccionAgo;
	}
	public void setPaBonoProduccionAgo(BigDecimal paBonoProduccionAgo) {
		this.paBonoProduccionAgo = paBonoProduccionAgo;
	}
	public BigDecimal getPaBonoSiniestralidadAgo() {
		return paBonoSiniestralidadAgo;
	}
	public void setPaBonoSiniestralidadAgo(BigDecimal paBonoSiniestralidadAgo) {
		this.paBonoSiniestralidadAgo = paBonoSiniestralidadAgo;
	}
	public BigDecimal getPaBonoCrecimientoAgo() {
		return paBonoCrecimientoAgo;
	}
	public void setPaBonoCrecimientoAgo(BigDecimal paBonoCrecimientoAgo) {
		this.paBonoCrecimientoAgo = paBonoCrecimientoAgo;
	}
	public BigDecimal getPaPrimaPagadaSep() {
		return paPrimaPagadaSep;
	}
	public void setPaPrimaPagadaSep(BigDecimal paPrimaPagadaSep) {
		this.paPrimaPagadaSep = paPrimaPagadaSep;
	}
	public BigDecimal getPaExclusionesSep() {
		return paExclusionesSep;
	}
	public void setPaExclusionesSep(BigDecimal paExclusionesSep) {
		this.paExclusionesSep = paExclusionesSep;
	}
	public BigDecimal getPaPrimaPagadaBonoSep() {
		return paPrimaPagadaBonoSep;
	}
	public void setPaPrimaPagadaBonoSep(BigDecimal paPrimaPagadaBonoSep) {
		this.paPrimaPagadaBonoSep = paPrimaPagadaBonoSep;
	}
	public BigDecimal getPaBonoProduccionSep() {
		return paBonoProduccionSep;
	}
	public void setPaBonoProduccionSep(BigDecimal paBonoProduccionSep) {
		this.paBonoProduccionSep = paBonoProduccionSep;
	}
	public BigDecimal getPaBonoSiniestralidadSep() {
		return paBonoSiniestralidadSep;
	}
	public void setPaBonoSiniestralidadSep(BigDecimal paBonoSiniestralidadSep) {
		this.paBonoSiniestralidadSep = paBonoSiniestralidadSep;
	}
	public BigDecimal getPaBonoCrecimientoSep() {
		return paBonoCrecimientoSep;
	}
	public void setPaBonoCrecimientoSep(BigDecimal paBonoCrecimientoSep) {
		this.paBonoCrecimientoSep = paBonoCrecimientoSep;
	}
	public BigDecimal getPaPrimaPagadaOct() {
		return paPrimaPagadaOct;
	}
	public void setPaPrimaPagadaOct(BigDecimal paPrimaPagadaOct) {
		this.paPrimaPagadaOct = paPrimaPagadaOct;
	}
	public BigDecimal getPaExclusionesOct() {
		return paExclusionesOct;
	}
	public void setPaExclusionesOct(BigDecimal paExclusionesOct) {
		this.paExclusionesOct = paExclusionesOct;
	}
	public BigDecimal getPaPrimaPagadaBonoOct() {
		return paPrimaPagadaBonoOct;
	}
	public void setPaPrimaPagadaBonoOct(BigDecimal paPrimaPagadaBonoOct) {
		this.paPrimaPagadaBonoOct = paPrimaPagadaBonoOct;
	}
	public BigDecimal getPaBonoProduccionOct() {
		return paBonoProduccionOct;
	}
	public void setPaBonoProduccionOct(BigDecimal paBonoProduccionOct) {
		this.paBonoProduccionOct = paBonoProduccionOct;
	}
	public BigDecimal getPaBonoSiniestralidadOct() {
		return paBonoSiniestralidadOct;
	}
	public void setPaBonoSiniestralidadOct(BigDecimal paBonoSiniestralidadOct) {
		this.paBonoSiniestralidadOct = paBonoSiniestralidadOct;
	}
	public BigDecimal getPaBonoCrecimientoOct() {
		return paBonoCrecimientoOct;
	}
	public void setPaBonoCrecimientoOct(BigDecimal paBonoCrecimientoOct) {
		this.paBonoCrecimientoOct = paBonoCrecimientoOct;
	}
	public BigDecimal getPaPrimaPagadaNov() {
		return paPrimaPagadaNov;
	}
	public void setPaPrimaPagadaNov(BigDecimal paPrimaPagadaNov) {
		this.paPrimaPagadaNov = paPrimaPagadaNov;
	}
	public BigDecimal getPaExclusionesNov() {
		return paExclusionesNov;
	}
	public void setPaExclusionesNov(BigDecimal paExclusionesNov) {
		this.paExclusionesNov = paExclusionesNov;
	}
	public BigDecimal getPaPrimaPagadaBonoNov() {
		return paPrimaPagadaBonoNov;
	}
	public void setPaPrimaPagadaBonoNov(BigDecimal paPrimaPagadaBonoNov) {
		this.paPrimaPagadaBonoNov = paPrimaPagadaBonoNov;
	}
	public BigDecimal getPaBonoProduccionNov() {
		return paBonoProduccionNov;
	}
	public void setPaBonoProduccionNov(BigDecimal paBonoProduccionNov) {
		this.paBonoProduccionNov = paBonoProduccionNov;
	}
	public BigDecimal getPaBonoSiniestralidadNov() {
		return paBonoSiniestralidadNov;
	}
	public void setPaBonoSiniestralidadNov(BigDecimal paBonoSiniestralidadNov) {
		this.paBonoSiniestralidadNov = paBonoSiniestralidadNov;
	}
	public BigDecimal getPaBonoCrecimientoNov() {
		return paBonoCrecimientoNov;
	}
	public void setPaBonoCrecimientoNov(BigDecimal paBonoCrecimientoNov) {
		this.paBonoCrecimientoNov = paBonoCrecimientoNov;
	}
	public BigDecimal getPaPrimaPagadaDic() {
		return paPrimaPagadaDic;
	}
	public void setPaPrimaPagadaDic(BigDecimal paPrimaPagadaDic) {
		this.paPrimaPagadaDic = paPrimaPagadaDic;
	}
	public BigDecimal getPaExclusionesDic() {
		return paExclusionesDic;
	}
	public void setPaExclusionesDic(BigDecimal paExclusionesDic) {
		this.paExclusionesDic = paExclusionesDic;
	}
	public BigDecimal getPaPrimaPagadaBonoDic() {
		return paPrimaPagadaBonoDic;
	}
	public void setPaPrimaPagadaBonoDic(BigDecimal paPrimaPagadaBonoDic) {
		this.paPrimaPagadaBonoDic = paPrimaPagadaBonoDic;
	}
	public BigDecimal getPaBonoProduccionDic() {
		return paBonoProduccionDic;
	}
	public void setPaBonoProduccionDic(BigDecimal paBonoProduccionDic) {
		this.paBonoProduccionDic = paBonoProduccionDic;
	}
	public BigDecimal getPaBonoSiniestralidadDic() {
		return paBonoSiniestralidadDic;
	}
	public void setPaBonoSiniestralidadDic(BigDecimal paBonoSiniestralidadDic) {
		this.paBonoSiniestralidadDic = paBonoSiniestralidadDic;
	}
	public BigDecimal getPaBonoCrecimientoDic() {
		return paBonoCrecimientoDic;
	}
	public void setPaBonoCrecimientoDic(BigDecimal paBonoCrecimientoDic) {
		this.paBonoCrecimientoDic = paBonoCrecimientoDic;
	}
	
	
}
