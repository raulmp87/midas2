package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class DatosAgenteDatosView implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Long id;
	private Long idCentroOperacion;
	private String nom_centroOperacion;
	private Long idGenrencia;
	private String nomGenrencia;
	private Long idEjecutivo;
	private String nomEjecutivo;
	private Long idPromotoria;
	private String nomPromotoria;
	private Long idAgentePromotoria;
	private String nomAgentePromotoria;
	private Long idAgente;
	private String nombre;
	private String fechaNacimiento;
	private String calleAgente;
	private String coloniaAgente;
	private String nuevaColoniaAgente;
	private String ciudadAgente;
	private String estadoAgente;
	private String codigoPostalAgente;
	private String e_mail;
	private String f_ultima_actualizacion;
	private String id_usuario;
	private Long id_Persona;
	private String personalidadJuridica;
	private String cve_t_agente;
	private String rfc;
	private String f_autorizacion;
	private String f_vencto_autor;
	private String cedula;
	private BigDecimal pct_dividendos;
	private String f_alta;
	private String f_baja;
	private String sit_agente;
	private String mot_sit_agte;
	private String banco;
	private String tipoProdBancario;
	private String numCuenta;
	private String numCuentaClave;
	private String b_genera_cheque;
	private String b_contabiliza_comision;
	private String b_comision;
	private String b_impr_edocta;
	private Long id_agte_reclutad;
	private String nom_agte_reclutad;
	private String curp;
	private String entretenimiento;
	
	public static final String PLANTILLA_NAME = "midas.agente.reporte.agente.datos.plantilla.archivo.nombre";

	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdCentroOperacion() {
		return idCentroOperacion;
	}
	public void setIdCentroOperacion(Long idCentroOperacion) {
		this.idCentroOperacion = idCentroOperacion;
	}
	public String getNom_centroOperacion() {
		return nom_centroOperacion;
	}
	public void setNom_centroOperacion(String nom_centroOperacion) {
		this.nom_centroOperacion = nom_centroOperacion;
	}
	public Long getIdGenrencia() {
		return idGenrencia;
	}
	public void setIdGenrencia(Long idGenrencia) {
		this.idGenrencia = idGenrencia;
	}
	public String getNomGenrencia() {
		return nomGenrencia;
	}
	public void setNomGenrencia(String nomGenrencia) {
		this.nomGenrencia = nomGenrencia;
	}
	public Long getIdEjecutivo() {
		return idEjecutivo;
	}
	public void setIdEjecutivo(Long idEjecutivo) {
		this.idEjecutivo = idEjecutivo;
	}
	public String getNomEjecutivo() {
		return nomEjecutivo;
	}
	public void setNomEjecutivo(String nomEjecutivo) {
		this.nomEjecutivo = nomEjecutivo;
	}
	public Long getIdPromotoria() {
		return idPromotoria;
	}
	public void setIdPromotoria(Long idPromotoria) {
		this.idPromotoria = idPromotoria;
	}
	public String getNomPromotoria() {
		return nomPromotoria;
	}
	public void setNomPromotoria(String nomPromotoria) {
		this.nomPromotoria = nomPromotoria;
	}
	
	public Long getIdAgentePromotoria() {
		return idAgentePromotoria;
	}
	public void setIdAgentePromotoria(Long idAgentePromotoria) {
		this.idAgentePromotoria = idAgentePromotoria;
	}
	public String getNomAgentePromotoria() {
		return nomAgentePromotoria;
	}
	public void setNomAgentePromotoria(String nomAgentePromotoria) {
		this.nomAgentePromotoria = nomAgentePromotoria;
	}
	public Long getIdAgente() {
		return idAgente;
	}
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getCalleAgente() {
		return calleAgente;
	}
	public void setCalleAgente(String calleAgente) {
		this.calleAgente = calleAgente;
	}
	public String getColoniaAgente() {
		return coloniaAgente;
	}
	public void setColoniaAgente(String coloniaAgente) {
		this.coloniaAgente = coloniaAgente;
	}
	public String getNuevaColoniaAgente() {
		return nuevaColoniaAgente;
	}
	public void setNuevaColoniaAgente(String nuevaColoniaAgente) {
		this.nuevaColoniaAgente = nuevaColoniaAgente;
	}
	public String getCiudadAgente() {
		return ciudadAgente;
	}
	public void setCiudadAgente(String ciudadAgente) {
		this.ciudadAgente = ciudadAgente;
	}
	public String getEstadoAgente() {
		return estadoAgente;
	}
	public void setEstadoAgente(String estadoAgente) {
		this.estadoAgente = estadoAgente;
	}
	public String getCodigoPostalAgente() {
		return codigoPostalAgente;
	}
	public void setCodigoPostalAgente(String codigoPostalAgente) {
		this.codigoPostalAgente = codigoPostalAgente;
	}
	public String getE_mail() {
		return e_mail;
	}
	public void setE_mail(String e_mail) {
		this.e_mail = e_mail;
	}
	public String getF_ultima_actualizacion() {
		return f_ultima_actualizacion;
	}
	public void setF_ultima_actualizacion(String f_ultima_actualizacion) {
		this.f_ultima_actualizacion = f_ultima_actualizacion;
	}
	public String getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(String id_usuario) {
		this.id_usuario = id_usuario;
	}
	public Long getId_Persona() {
		return id_Persona;
	}
	public void setId_Persona(Long id_Persona) {
		this.id_Persona = id_Persona;
	}
	public String getPersonalidadJuridica() {
		return personalidadJuridica;
	}
	public void setPersonalidadJuridica(String personalidadJuridica) {
		this.personalidadJuridica = personalidadJuridica;
	}
	public String getCve_t_agente() {
		return cve_t_agente;
	}
	public void setCve_t_agente(String cve_t_agente) {
		this.cve_t_agente = cve_t_agente;
	}
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getF_autorizacion() {
		return f_autorizacion;
	}
	public void setF_autorizacion(String f_autorizacion) {
		this.f_autorizacion = f_autorizacion;
	}
	public String getF_vencto_autor() {
		return f_vencto_autor;
	}
	public void setF_vencto_autor(String f_vencto_autor) {
		this.f_vencto_autor = f_vencto_autor;
	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public BigDecimal getPct_dividendos() {
		return pct_dividendos;
	}
	public void setPct_dividendos(BigDecimal pct_dividendos) {
		this.pct_dividendos = pct_dividendos;
	}
	public String getF_alta() {
		return f_alta;
	}
	public void setF_alta(String f_alta) {
		this.f_alta = f_alta;
	}
	public String getF_baja() {
		return f_baja;
	}
	public void setF_baja(String f_baja) {
		this.f_baja = f_baja;
	}
	public String getSit_agente() {
		return sit_agente;
	}
	public void setSit_agente(String sit_agente) {
		this.sit_agente = sit_agente;
	}
	public String getMot_sit_agte() {
		return mot_sit_agte;
	}
	public void setMot_sit_agte(String mot_sit_agte) {
		this.mot_sit_agte = mot_sit_agte;
	}
	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public String getTipoProdBancario() {
		return tipoProdBancario;
	}
	public void setTipoProdBancario(String tipoProdBancario) {
		this.tipoProdBancario = tipoProdBancario;
	}
	public String getNumCuenta() {
		return numCuenta;
	}
	public void setNumCuenta(String numCuenta) {
		this.numCuenta = numCuenta;
	}
	public String getNumCuentaClave() {
		return numCuentaClave;
	}
	public void setNumCuentaClave(String numCuentaClave) {
		this.numCuentaClave = numCuentaClave;
	}
	public String getB_genera_cheque() {
		return b_genera_cheque;
	}
	public void setB_genera_cheque(String b_genera_cheque) {
		this.b_genera_cheque = b_genera_cheque;
	}
	public String getB_contabiliza_comision() {
		return b_contabiliza_comision;
	}
	public void setB_contabiliza_comision(String b_contabiliza_comision) {
		this.b_contabiliza_comision = b_contabiliza_comision;
	}
	public String getB_comision() {
		return b_comision;
	}
	public void setB_comision(String b_comision) {
		this.b_comision = b_comision;
	}
	public String getB_impr_edocta() {
		return b_impr_edocta;
	}
	public void setB_impr_edocta(String b_impr_edocta) {
		this.b_impr_edocta = b_impr_edocta;
	}
	public Long getId_agte_reclutad() {
		return id_agte_reclutad;
	}
	public void setId_agte_reclutad(Long id_agte_reclutad) {
		this.id_agte_reclutad = id_agte_reclutad;
	}
	public String getNom_agte_reclutad() {
		return nom_agte_reclutad;
	}
	public void setNom_agte_reclutad(String nom_agte_reclutad) {
		this.nom_agte_reclutad = nom_agte_reclutad;
	}
	public String getCurp() {
		return curp;
	}
	public void setCurp(String curp) {
		this.curp = curp;
	}
	public String getEntretenimiento() {
		return entretenimiento;
	}
	public void setEntretenimiento(String entretenimiento) {
		this.entretenimiento = entretenimiento;
	}
	
}
