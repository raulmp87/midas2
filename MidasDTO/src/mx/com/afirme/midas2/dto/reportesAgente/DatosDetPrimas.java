package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.annotation.Exportable;


@Entity
public class DatosDetPrimas implements Serializable {
	private static final long serialVersionUID = 5727747230559014746L;
	private Long id;
	private Long idAgente;
	private String idRamo;
	private String idSubramo;
	private Long idSupervisoria;
	private String numeroPoliza;
	private Long numeroEndoso;
	private Long numeroFolioRubro;
	private String contratante;
	private String fechaMovimiento;
	private String descripcionMovimiento;
	private String formaPago;
	private String moneda;
	private String nombreSupervisoria;
	private String nombreAgente;
	private String porcentajeComisionAgente;
	private BigDecimal importeSaldoInicialPeriodo;
	private BigDecimal importePrimaNeta;
	private BigDecimal importeRecargosPagoFraccionado;
	private BigDecimal importePrimaTotal;
	private	BigDecimal importeComisionAgente;
	private BigDecimal importeIvaAcreditado;
	private BigDecimal importeIvaRetenido;
	private BigDecimal importeEstatalRetenido;
	private BigDecimal importeIsr;

	/******campos adicionales Reporte Detalle Primas (mas detalle)******/
	private Long idGerencia;
	private Long idOficina;
	private Long idPromotoria;
	private Long idMovimientoConsecutivo;
	private Long idEmpresa;
	private Long idPersona;
	private Long idRecibo;
	private Long idConcepto;
	private String numeroCedula;
	private Long secuencia;
	private String ramo;
	private String numeroFianza;
	private String tipoConceptoOrigen;
	private String naturalezaConcepto;
	private String claseAgente;
	private String situacionAgente;
	private String dia;
	private String diaCorte;
	private String anioMes;
	private Date fecha;
	private Date fechaVencimientoCedula;
	private String fechaCorte;
	private Date fechaFinFianza;
	private Date fechaInicioPeriodo;
	private BigDecimal importeTipoCambio;
	private BigDecimal importeTotalPorPoliza;
	private BigDecimal importeComisionAcumuladaDiaCorte;
	private BigDecimal importeTotalPorMovimiento;
	private BigDecimal importeInicialIva;
	private BigDecimal importeInicialIvaRetenido;
	private BigDecimal importeInicialIsr;
	private BigDecimal importeInicialEstatalRetenido;
	
	public static final String PLANTILLA_NAME = "midas.agente.reporte.agente.detallePrimasAgente.archivo.nombre";
	
	@Id
	public Long getId() {
		return id;
	}
	@Exportable(columnName="FECHAMOVIMIENTO", columnOrder=0)
	public String getFechaMovimiento() {
		return fechaMovimiento;
	}
	@Exportable(columnName="ID_SUPERVISORIA", columnOrder=1)
	public Long getIdSupervisoria() {
		return idSupervisoria;
	}
	@Exportable(columnName="NOM_SUPERVISORIA", columnOrder=2)
	public String getNombreSupervisoria() {
		return nombreSupervisoria;
	}
	@Exportable(columnName="ID_AGENTE", columnOrder=3)
	public Long getIdAgente() {
		return idAgente;
	}
	@Exportable(columnName="NOM_AGENTE", columnOrder=4)
	public String getNombreAgente() {
		return nombreAgente;
	}
	@Exportable(columnName="NUM_POLIZA", columnOrder=5)
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	@Exportable(columnName="NUM_ENDOSO", columnOrder=6)
	public Long getNumeroEndoso() {
		return numeroEndoso;
	}
	@Exportable(columnName="NUM_FOLIO_RBO", columnOrder=7)
	public Long getNumeroFolioRubro() {
		return numeroFolioRubro;
	}
	@Exportable(columnName="FORMA_PAGO", columnOrder=8)
	public String getFormaPago() {
		return formaPago;
	}
	@Exportable(columnName="RAMO_CONTABLE", columnOrder=9)
	public String getIdRamo() {
		return idRamo;
	}
	@Exportable(columnName="ID_SUBR_CONTABLE", columnOrder=10)
	public String getIdSubramo() {
		return idSubramo;
	}
	@Exportable(columnName="DESC_MONEDA", columnOrder=11)
	public String getMoneda() {
		return moneda;
	}
	@Exportable(columnName="CONTRATANTE", columnOrder=12)
	public String getContratante() {
		return contratante;
	}
	@Exportable(columnName="IMP_PRIMA_NETA", columnOrder=13)
	public BigDecimal getImportePrimaNeta() {
		return importePrimaNeta;
	}
	@Exportable(columnName="IMP_RCGOS_PAGOFR", columnOrder=14)
	public BigDecimal getImporteRecargosPagoFraccionado() {
		return importeRecargosPagoFraccionado;
	}
	@Exportable(columnName="IMP_PRIMA_TOTAL", columnOrder=15)
	public BigDecimal getImportePrimaTotal() {
		return importePrimaTotal;
	}
	@Exportable(columnName="PCT_COMIS_AGTE", columnOrder=16)
	public String getPorcentajeComisionAgente() {
		return porcentajeComisionAgente;
	}
	@Exportable(columnName="SDO_INICIAL", columnOrder=17)
	public BigDecimal getImporteSaldoInicialPeriodo() {
		return importeSaldoInicialPeriodo;
	}
	@Exportable(columnName="IMP_COMIS_AGTE", columnOrder=18)
	public BigDecimal getImporteComisionAgente() {
		return importeComisionAgente;
	}
	@Exportable(columnName="IVA", columnOrder=19)
	public BigDecimal getImporteIvaAcreditado() {
		return importeIvaAcreditado;
	}
	@Exportable(columnName="IMP_IVA_RE", columnOrder=20)
	public BigDecimal getImporteIvaRetenido() {
		return importeIvaRetenido;
	}
	@Exportable(columnName="IMP_ISR", columnOrder=21)
	public BigDecimal getImporteIsr() {
		return importeIsr;
	}
	@Exportable(columnName="IMP_RET_EST", columnOrder=22)
	public BigDecimal getImporteEstatalRetenido() {
		return importeEstatalRetenido;
	}
	@Exportable(columnName="DESC_MOVTO", columnOrder=23)
	public String getDescripcionMovimiento() {
		return descripcionMovimiento;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setContratante(String contratante) {
		this.contratante = contratante;
	}
	public void setFechaMovimiento(String fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
	}
	public void setDescripcionMovimiento(String descripcionMovimiento) {
		this.descripcionMovimiento = descripcionMovimiento;
	}
	public void setPorcentajeComisionAgente(String porcentajeComisionAgente) {
		this.porcentajeComisionAgente = porcentajeComisionAgente;
	}
	public void setImporteSaldoInicialPeriodo(BigDecimal importeSaldoInicialPeriodo) {
		this.importeSaldoInicialPeriodo = importeSaldoInicialPeriodo;
	}
	public void setImportePrimaNeta(BigDecimal importePrimaNeta) {
		this.importePrimaNeta = importePrimaNeta;
	}
	public void setImporteRecargosPagoFraccionado(
			BigDecimal importeRecargosPagoFraccionado) {
		this.importeRecargosPagoFraccionado = importeRecargosPagoFraccionado;
	}
	public void setImportePrimaTotal(BigDecimal importePrimaTotal) {
		this.importePrimaTotal = importePrimaTotal;
	}
	public void setImporteComisionAgente(BigDecimal importeComisionAgente) {
		this.importeComisionAgente = importeComisionAgente;
	}
	public void setImporteIvaAcreditado(BigDecimal importeIvaAcreditado) {
		this.importeIvaAcreditado = importeIvaAcreditado;
	}
	public void setImporteIvaRetenido(BigDecimal importeIvaRetenido) {
		this.importeIvaRetenido = importeIvaRetenido;
	}
	public void setImporteEstatalRetenido(BigDecimal importeEstatalRetenido) {
		this.importeEstatalRetenido = importeEstatalRetenido;
	}
	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public void setNombreSupervisoria(String nombreSupervisoria) {
		this.nombreSupervisoria = nombreSupervisoria;
	}
	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}
	public void setImporteIsr(BigDecimal importeIsr) {
		this.importeIsr = importeIsr;
	}
	public Long getIdGerencia() {
		return idGerencia;
	}
	public void setIdGerencia(Long idGerencia) {
		this.idGerencia = idGerencia;
	}
	public Long getIdOficina() {
		return idOficina;
	}
	public void setIdOficina(Long idOficina) {
		this.idOficina = idOficina;
	}
	public Long getIdPromotoria() {
		return idPromotoria;
	}
	public void setIdPromotoria(Long idPromotoria) {
		this.idPromotoria = idPromotoria;
	}
	public Long getIdMovimientoConsecutivo() {
		return idMovimientoConsecutivo;
	}
	public void setIdMovimientoConsecutivo(Long idMovimientoConsecutivo) {
		this.idMovimientoConsecutivo = idMovimientoConsecutivo;
	}
	public Long getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	public Long getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}
	public Long getIdRecibo() {
		return idRecibo;
	}
	public void setIdRecibo(Long idRecibo) {
		this.idRecibo = idRecibo;
	}
	public Long getIdConcepto() {
		return idConcepto;
	}
	public void setIdConcepto(Long idConcepto) {
		this.idConcepto = idConcepto;
	}
	public String getNumeroCedula() {
		return numeroCedula;
	}
	public void setNumeroCedula(String numeroCedula) {
		this.numeroCedula = numeroCedula;
	}
	public Long getSecuencia() {
		return secuencia;
	}
	public void setSecuencia(Long secuencia) {
		this.secuencia = secuencia;
	}
	public String getRamo() {
		return ramo;
	}
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
	public void setIdRamo(String idRamo) {
		this.idRamo = idRamo;
	}
	public void setIdSubramo(String idSubramo) {
		this.idSubramo = idSubramo;
	}
	public void setIdSupervisoria(Long idSupervisoria) {
		this.idSupervisoria = idSupervisoria;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public void setNumeroEndoso(Long numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}
	public void setNumeroFolioRubro(Long numeroFolioRubro) {
		this.numeroFolioRubro = numeroFolioRubro;
	}
	public void setRamo(String ramo) {
		this.ramo = ramo;
	}
	public String getNumeroFianza() {
		return numeroFianza;
	}
	public void setNumeroFianza(String numeroFianza) {
		this.numeroFianza = numeroFianza;
	}
	public String getTipoConceptoOrigen() {
		return tipoConceptoOrigen;
	}
	public void setTipoConceptoOrigen(String tipoConceptoOrigen) {
		this.tipoConceptoOrigen = tipoConceptoOrigen;
	}
	public String getNaturalezaConcepto() {
		return naturalezaConcepto;
	}
	public void setNaturalezaConcepto(String naturalezaConcepto) {
		this.naturalezaConcepto = naturalezaConcepto;
	}
	public String getClaseAgente() {
		return claseAgente;
	}
	public void setClaseAgente(String claseAgente) {
		this.claseAgente = claseAgente;
	}
	public String getSituacionAgente() {
		return situacionAgente;
	}
	public void setSituacionAgente(String situacionAgente) {
		this.situacionAgente = situacionAgente;
	}
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	public String getDiaCorte() {
		return diaCorte;
	}
	public void setDiaCorte(String diaCorte) {
		this.diaCorte = diaCorte;
	}
	public String getAnioMes() {
		return anioMes;
	}
	public void setAnioMes(String anioMes) {
		this.anioMes = anioMes;
	}
	@Temporal(TemporalType.DATE)
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaVencimientoCedula() {
		return fechaVencimientoCedula;
	}
	public void setFechaVencimientoCedula(Date fechaVencimientoCedula) {
		this.fechaVencimientoCedula = fechaVencimientoCedula;
	}
	public String getFechaCorte() {
		return fechaCorte;
	}
	public void setFechaCorte(String fechaCorte) {
		this.fechaCorte = fechaCorte;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaFinFianza() {
		return fechaFinFianza;
	}
	public void setFechaFinFianza(Date fechaFinFianza) {
		this.fechaFinFianza = fechaFinFianza;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaInicioPeriodo() {
		return fechaInicioPeriodo;
	}
	public void setFechaInicioPeriodo(Date fechaInicioPeriodo) {
		this.fechaInicioPeriodo = fechaInicioPeriodo;
	}
	public BigDecimal getImporteTipoCambio() {
		return importeTipoCambio;
	}
	public void setImporteTipoCambio(BigDecimal importeTipoCambio) {
		this.importeTipoCambio = importeTipoCambio;
	}
	public BigDecimal getImporteTotalPorPoliza() {
		return importeTotalPorPoliza;
	}
	public void setImporteTotalPorPoliza(BigDecimal importeTotalPorPoliza) {
		this.importeTotalPorPoliza = importeTotalPorPoliza;
	}
	public BigDecimal getImporteComisionAcumuladaDiaCorte() {
		return importeComisionAcumuladaDiaCorte;
	}
	public void setImporteComisionAcumuladaDiaCorte(
			BigDecimal importeComisionAcumuladaDiaCorte) {
		this.importeComisionAcumuladaDiaCorte = importeComisionAcumuladaDiaCorte;
	}
	public BigDecimal getImporteTotalPorMovimiento() {
		return importeTotalPorMovimiento;
	}
	public void setImporteTotalPorMovimiento(BigDecimal importeTotalPorMovimiento) {
		this.importeTotalPorMovimiento = importeTotalPorMovimiento;
	}
	public BigDecimal getImporteInicialIva() {
		return importeInicialIva;
	}
	public void setImporteInicialIva(BigDecimal importeInicialIva) {
		this.importeInicialIva = importeInicialIva;
	}
	public BigDecimal getImporteInicialIvaRetenido() {
		return importeInicialIvaRetenido;
	}
	public void setImporteInicialIvaRetenido(BigDecimal importeInicialIvaRetenido) {
		this.importeInicialIvaRetenido = importeInicialIvaRetenido;
	}
	public BigDecimal getImporteInicialIsr() {
		return importeInicialIsr;
	}
	public void setImporteInicialIsr(BigDecimal importeInicialIsr) {
		this.importeInicialIsr = importeInicialIsr;
	}
	public BigDecimal getImporteInicialEstatalRetenido() {
		return importeInicialEstatalRetenido;
	}
	public void setImporteInicialEstatalRetenido(BigDecimal importeInicialEstatalRetenido) {
		this.importeInicialEstatalRetenido = importeInicialEstatalRetenido;
	}

}