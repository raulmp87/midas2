package mx.com.afirme.midas2.dto.reportesAgente;

import java.math.BigDecimal;

import mx.com.afirme.midas2.annotation.Exportable;

public class RetencionImpuestosReporteView {

	private Long idGerencia;
	private String nombreGerencia;
	private Long idEjecutivo;
	private String nombreEjecutivo;
	private String oficina;
	private Long anio;
	private Long mes;
	private Long idPromotores;
	private String nombrePromotor;
	private Long idAgente;
	private String nombreAgente;
	private String estado;
	private BigDecimal impBase;
	private BigDecimal impIva;
	private BigDecimal impRetIva;
	private BigDecimal impRetIsr;
	private BigDecimal impEstRet;
	private String tr;
	
	public static final String PLANTILLA_NAME = "midas.agente.reporte.agente.detallePrimasAgenteRetencionImpuestos.archivo.nombre";

	@Exportable(columnName = "ID_GERENCIA", columnOrder = 0)
	public Long getIdGerencia() {
		return idGerencia;
	}

	@Exportable(columnName = "NOM_GERENCIA", columnOrder = 1)
	public String getNombreGerencia() {
		return nombreGerencia;
	}

	@Exportable(columnName = "ID_EJECUTIVO", columnOrder = 2)
	public Long getIdEjecutivo() {
		return idEjecutivo;
	}

	@Exportable(columnName = "NOM_EJECUTIVO", columnOrder = 3)
	public String getNombreEjecutivo() {
		return nombreEjecutivo;
	}
	
	@Exportable(columnName = "OFICINA", columnOrder = 4)
	public String getOficina() {
		return oficina;
	}

	@Exportable(columnName = "MES", columnOrder = 5)
	public Long getMes() {
		return mes;
	}

	@Exportable(columnName = "ID_PROMOTORES", columnOrder = 6)
	public Long getIdPromotores() {
		return idPromotores;
	}

	@Exportable(columnName = "NOMBRE_PROMOTORIA", columnOrder = 7)
	public String getNombrePromotor() {
		return nombrePromotor;
	}

	@Exportable(columnName = "ID_AGENTE", columnOrder = 8)
	public Long getIdAgente() {
		return idAgente;
	}

	@Exportable(columnName = "NOMBRE", columnOrder = 9)
	public String getNombreAgente() {
		return nombreAgente;
	}

	@Exportable(columnName = "ESTADO", columnOrder = 10)
	public String getEstado() {
		return estado;
	}

	@Exportable(columnName = "IMP_BASE", columnOrder = 11)
	public BigDecimal getImpBase() {
		return impBase;
	}

	@Exportable(columnName = "IMP_IVA", columnOrder = 12)
	public BigDecimal getImpIva() {
		return impIva;
	}

	@Exportable(columnName = "IMP_RET_IVA", columnOrder = 13)
	public BigDecimal getImpRetIva() {
		return impRetIva;
	}

	@Exportable(columnName = "IMP_RET_ISR", columnOrder = 14)
	public BigDecimal getImpRetIsr() {
		return impRetIsr;
	}

	@Exportable(columnName = "IMP_EST_RET", columnOrder = 15)
	public BigDecimal getImpEstRet() {
		return impEstRet;
	}

	@Exportable(columnName = "TR", columnOrder = 16)
	public String getTr() {
		return tr;
	}
	
	public void setIdGerencia(Long idGerencia) {
		this.idGerencia = idGerencia;
	}

	public void setNombreGerencia(String nombreGerencia) {
		this.nombreGerencia = nombreGerencia;
	}

	public void setIdEjecutivo(Long idEjecutivo) {
		this.idEjecutivo = idEjecutivo;
	}

	public void setNombreEjecutivo(String nombreEjecutivo) {
		this.nombreEjecutivo = nombreEjecutivo;
	}

	public void setMes(Long mes) {
		this.mes = mes;
	}

	public void setIdPromotores(Long idPromotores) {
		this.idPromotores = idPromotores;
	}

	public void setNombrePromotor(String nombrePromotor) {
		this.nombrePromotor = nombrePromotor;
	}

	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}

	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public void setImpBase(BigDecimal impBase) {
		this.impBase = impBase;
	}

	public void setImpIva(BigDecimal impIva) {
		this.impIva = impIva;
	}

	public void setImpRetIva(BigDecimal impRetIva) {
		this.impRetIva = impRetIva;
	}

	public void setImpRetIsr(BigDecimal impRetIsr) {
		this.impRetIsr = impRetIsr;
	}

	public void setImpEstRet(BigDecimal impEstRet) {
		this.impEstRet = impEstRet;
	}

	public void setTr(String tr) {
		this.tr = tr;
	}
	
	public Long getAnio() {
		return anio;
	}
	
	public void setAnio(Long anio) {
		this.anio = anio;
	}
	
	public void setOficina(String oficina) {
		this.oficina = oficina;
	}
	
}
