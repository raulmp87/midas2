package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class DatosAgentePrimaPagadaPorRamoView implements Serializable {
	
	public static final String PLANTILLA_NAME = "midas.agente.reporte.agente.primaPagadaPorRamo.plantilla.archivo.nombre";
	
	private static final long serialVersionUID = 1L;
	
	private Long idAgente;
	private String nombreCompleto;
	private Long idPromotoria;
	private String promotoria;
	private Long idGerencia;
	private String gerencia;
	//id centro operacion
	private Long idCop;
	//nombre centro operacion
	private String cop;
	private Long idEjecutivo;
	private String ejecutivo;
	private Double primaPagadaTotal;
	private Double acumuladodanios;
	private Double acumuladoautos;
	private Double acumuladovida;
	private String clavenegociodanios;
	private String clavenegocioautos;
	private Long sin_total;
	
	@Id
	public Long getIdAgente() {
		return idAgente;
	}
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	public Long getIdPromotoria() {
		return idPromotoria;
	}
	public void setIdPromotoria(Long idPromotoria) {
		this.idPromotoria = idPromotoria;
	}
	public String getPromotoria() {
		return promotoria;
	}
	public void setPromotoria(String promotoria) {
		this.promotoria = promotoria;
	}
	public Long getIdGerencia() {
		return idGerencia;
	}
	public void setIdGerencia(Long idGerencia) {
		this.idGerencia = idGerencia;
	}
	public String getGerencia() {
		return gerencia;
	}
	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}
	public Long getIdCop() {
		return idCop;
	}
	public void setIdCop(Long idCop) {
		this.idCop = idCop;
	}
	public String getCop() {
		return cop;
	}
	public void setCop(String cop) {
		this.cop = cop;
	}
	public Long getIdEjecutivo() {
		return idEjecutivo;
	}
	public void setIdEjecutivo(Long idEjecutivo) {
		this.idEjecutivo = idEjecutivo;
	}
	public String getEjecutivo() {
		return ejecutivo;
	}
	public void setEjecutivo(String ejecutivo) {
		this.ejecutivo = ejecutivo;
	}
	public Double getPrimaPagadaTotal() {
		return primaPagadaTotal;
	}
	public void setPrimaPagadaTotal(Double primaPagadaTotal) {
		this.primaPagadaTotal = primaPagadaTotal;
	}
	public Double getAcumuladodanios() {
		return acumuladodanios;
	}
	public void setAcumuladodanios(Double acumuladodanios) {
		this.acumuladodanios = acumuladodanios;
	}
	public Double getAcumuladoautos() {
		return acumuladoautos;
	}
	public void setAcumuladoautos(Double acumuladoautos) {
		this.acumuladoautos = acumuladoautos;
	}
	public Double getAcumuladovida() {
		return acumuladovida;
	}
	public void setAcumuladovida(Double acumuladovida) {
		this.acumuladovida = acumuladovida;
	}
	public String getClavenegociodanios() {
		return clavenegociodanios;
	}
	public void setClavenegociodanios(String clavenegociodanios) {
		this.clavenegociodanios = clavenegociodanios;
	}
	public String getClavenegocioautos() {
		return clavenegocioautos;
	}
	public void setClavenegocioautos(String clavenegocioautos) {
		this.clavenegocioautos = clavenegocioautos;
	}
	public Long getSin_total() {
		return sin_total;
	}
	public void setSin_total(Long sin_total) {
		this.sin_total = sin_total;
	}
}