package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class DatosAgentePrimaEmitidaVsPriamaPagadaDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String ejecutivo;
	private String tipoAgente;	
	private Long anio;
	private Long claveAgente;
	private Double primaEmitida;
	private Double primaPagada;
	private String agente;
//	private String nombreBono; 
	private Long id;
	private BigDecimal siniestralidad_mes;
	private BigDecimal siniestralidad_ejercicio;
	private BigDecimal siniestralidad_ult12Meses;
	private String estatusAgente;

	public static final String PLANTILLA_NAME = "midas.agente.reporte.agente.primaPAgVSprimaEmit.archivo.nombre";
	
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
//	public String getNombreBono() {
//		return nombreBono;
//	}
//	public void setNombreBono(String nombreBono) {
//		this.nombreBono = nombreBono;
//	}
	public String getEjecutivo() {
		return ejecutivo;
	}
	public void setEjecutivo(String ejecutivo) {
		this.ejecutivo = ejecutivo;
	}
	
	public String getTipoAgente() {
		return tipoAgente;
	}
	public void setTipoAgente(String tipoAgente) {
		this.tipoAgente = tipoAgente;
	}
	
	public Long getAnio() {
		return anio;
	}
	public void setAnio(Long anio) {
		this.anio = anio;
	}
	
	public Double getPrimaEmitida() {
		return primaEmitida;
	}
	public void setPrimaEmitida(Double primaEmitida) {
		this.primaEmitida = primaEmitida;
	}
	
	public Double getPrimaPagada() {
		return primaPagada;
	}
	
	public void setPrimaPagada(Double primaPagada) {
		this.primaPagada = primaPagada;
	}
	
	public Long getClaveAgente() {
		return claveAgente;
	}
	
	public void setClaveAgente(Long claveAgente) {
		this.claveAgente = claveAgente;
	}
	
	public String getAgente() {
		return agente;
	}
	public void setAgente(String agente) {
		this.agente = agente;
	}
	
	public BigDecimal getSiniestralidad_mes() {
		return siniestralidad_mes;
	}
	public void setSiniestralidad_mes(BigDecimal siniestralidad_mes) {
		this.siniestralidad_mes = siniestralidad_mes;
	}
	
	public BigDecimal getSiniestralidad_ejercicio() {
		return siniestralidad_ejercicio;
	}
	public void setSiniestralidad_ejercicio(BigDecimal siniestralidad_ejercicio) {
		this.siniestralidad_ejercicio = siniestralidad_ejercicio;
	}
	
	public BigDecimal getSiniestralidad_ult12Meses() {
		return siniestralidad_ult12Meses;
	}
	public void setSiniestralidad_ult12Meses(BigDecimal siniestralidad_ult12Meses) {
		this.siniestralidad_ult12Meses = siniestralidad_ult12Meses;
	}
	public String getEstatusAgente() {
		return estatusAgente;
	}
	public void setEstatusAgente(String estatusAgente) {
		this.estatusAgente = estatusAgente;
	}

}
