package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;
import java.math.BigDecimal;

public class RepMizarSelectMizar implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 169325439655386121L;
	private Long agente;
	private BigDecimal cob_importe;
	private BigDecimal agt_importe;
	private BigDecimal gg_importe;
	
	public Long getAgente() {
		return agente;
	}
	public void setAgente(Long agente) {
		this.agente = agente;
	}
	public BigDecimal getCob_importe() {
		return cob_importe;
	}
	public void setCob_importe(BigDecimal cob_importe) {
		this.cob_importe = cob_importe;
	}
	public BigDecimal getAgt_importe() {
		return agt_importe;
	}
	public void setAgt_importe(BigDecimal agt_importe) {
		this.agt_importe = agt_importe;
	}
	public BigDecimal getGg_importe() {
		return gg_importe;
	}
	public void setGg_importe(BigDecimal gg_importe) {
		this.gg_importe = gg_importe;
	}
}
