package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class DatosEdoctaAgenteProduccion implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Long idAgente;
	private Double mensual;
	private Double anual;
	
	@Id
	public Long getIdAgente() {
		return idAgente;
	}
	public Double getMensual() {
		return mensual;
	}
	public Double getAnual() {
		return anual;
	}
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
	public void setMensual(Double mensual) {
		this.mensual = mensual;
	}
	public void setAnual(Double anual) {
		this.anual = anual;
	}
	
}
