package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author admin
 *
 */
public class DatosVentasProduccionAux implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1436631699051971713L;
	
	private String producto;
	private BigDecimal importeEne;
	private BigDecimal porcentajeEne;
	private BigDecimal importeFeb;
	private BigDecimal porcentajeFeb;
	private BigDecimal importeMar;
	private BigDecimal porcentajeMar;
	private BigDecimal importeAbr;
	private BigDecimal porcentajeAbr;
	private BigDecimal importeMay;
	private BigDecimal porcentajeMay;
	private BigDecimal importeJun;
	private BigDecimal porcentajeJun;
	private BigDecimal importeJul;
	private BigDecimal porcentajeJul;
	private BigDecimal importeAgo;
	private BigDecimal porcentajeAgo;
	private BigDecimal importeSep;
	private BigDecimal porcentajeSep;
	private BigDecimal importeOct;
	private BigDecimal porcentajeOct;
	private BigDecimal importeNov;
	private BigDecimal porcentajeNov;
	private BigDecimal importeDic;
	private BigDecimal porcentajeDic;
	private BigDecimal importeGlobal;
	private BigDecimal porcentajeGlobal;
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public BigDecimal getImporteEne() {
		return importeEne;
	}
	public void setImporteEne(BigDecimal importeEne) {
		this.importeEne = importeEne;
	}
	public BigDecimal getPorcentajeEne() {
		return porcentajeEne;
	}
	public void setPorcentajeEne(BigDecimal porcentajeEne) {
		this.porcentajeEne = porcentajeEne;
	}
	public BigDecimal getImporteFeb() {
		return importeFeb;
	}
	public void setImporteFeb(BigDecimal importeFeb) {
		this.importeFeb = importeFeb;
	}
	public BigDecimal getPorcentajeFeb() {
		return porcentajeFeb;
	}
	public void setPorcentajeFeb(BigDecimal porcentajeFeb) {
		this.porcentajeFeb = porcentajeFeb;
	}
	public BigDecimal getImporteMar() {
		return importeMar;
	}
	public void setImporteMar(BigDecimal importeMar) {
		this.importeMar = importeMar;
	}
	public BigDecimal getPorcentajeMar() {
		return porcentajeMar;
	}
	public void setPorcentajeMar(BigDecimal porcentajeMar) {
		this.porcentajeMar = porcentajeMar;
	}
	public BigDecimal getImporteAbr() {
		return importeAbr;
	}
	public void setImporteAbr(BigDecimal importeAbr) {
		this.importeAbr = importeAbr;
	}
	public BigDecimal getPorcentajeAbr() {
		return porcentajeAbr;
	}
	public void setPorcentajeAbr(BigDecimal porcentajeAbr) {
		this.porcentajeAbr = porcentajeAbr;
	}
	public BigDecimal getImporteMay() {
		return importeMay;
	}
	public void setImporteMay(BigDecimal importeMay) {
		this.importeMay = importeMay;
	}
	public BigDecimal getPorcentajeMay() {
		return porcentajeMay;
	}
	public void setPorcentajeMay(BigDecimal porcentajeMay) {
		this.porcentajeMay = porcentajeMay;
	}
	public BigDecimal getImporteJun() {
		return importeJun;
	}
	public void setImporteJun(BigDecimal importeJun) {
		this.importeJun = importeJun;
	}
	public BigDecimal getPorcentajeJun() {
		return porcentajeJun;
	}
	public void setPorcentajeJun(BigDecimal porcentajeJun) {
		this.porcentajeJun = porcentajeJun;
	}
	public BigDecimal getImporteJul() {
		return importeJul;
	}
	public void setImporteJul(BigDecimal importeJul) {
		this.importeJul = importeJul;
	}
	public BigDecimal getPorcentajeJul() {
		return porcentajeJul;
	}
	public void setPorcentajeJul(BigDecimal porcentajeJul) {
		this.porcentajeJul = porcentajeJul;
	}
	public BigDecimal getImporteAgo() {
		return importeAgo;
	}
	public void setImporteAgo(BigDecimal importeAgo) {
		this.importeAgo = importeAgo;
	}
	public BigDecimal getPorcentajeAgo() {
		return porcentajeAgo;
	}
	public void setPorcentajeAgo(BigDecimal porcentajeAgo) {
		this.porcentajeAgo = porcentajeAgo;
	}
	public BigDecimal getImporteSep() {
		return importeSep;
	}
	public void setImporteSep(BigDecimal importeSep) {
		this.importeSep = importeSep;
	}
	public BigDecimal getPorcentajeSep() {
		return porcentajeSep;
	}
	public void setPorcentajeSep(BigDecimal porcentajeSep) {
		this.porcentajeSep = porcentajeSep;
	}
	public BigDecimal getImporteOct() {
		return importeOct;
	}
	public void setImporteOct(BigDecimal importeOct) {
		this.importeOct = importeOct;
	}
	public BigDecimal getPorcentajeOct() {
		return porcentajeOct;
	}
	public void setPorcentajeOct(BigDecimal porcentajeOct) {
		this.porcentajeOct = porcentajeOct;
	}
	public BigDecimal getImporteNov() {
		return importeNov;
	}
	public void setImporteNov(BigDecimal importeNov) {
		this.importeNov = importeNov;
	}
	public BigDecimal getPorcentajeNov() {
		return porcentajeNov;
	}
	public void setPorcentajeNov(BigDecimal porcentajeNov) {
		this.porcentajeNov = porcentajeNov;
	}
	public BigDecimal getImporteDic() {
		return importeDic;
	}
	public void setImporteDic(BigDecimal importeDic) {
		this.importeDic = importeDic;
	}
	public BigDecimal getPorcentajeDic() {
		return porcentajeDic;
	}
	public void setPorcentajeDic(BigDecimal porcentajeDic) {
		this.porcentajeDic = porcentajeDic;
	}
	public BigDecimal getImporteGlobal() {
		return importeGlobal;
	}
	public void setImporteGlobal(BigDecimal importeGlobal) {
		this.importeGlobal = importeGlobal;
	}
	public BigDecimal getPorcentajeGlobal() {
		return porcentajeGlobal;
	}
	public void setPorcentajeGlobal(BigDecimal porcentajeGlobal) {
		this.porcentajeGlobal = porcentajeGlobal;
	}
	
	
}
