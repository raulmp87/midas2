package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;
import java.math.BigDecimal;

public class DatosReporteCalculoBonoMensualDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idBeneficiario;
	private String nombreBeneficiario;
	private String periodoFechaPago;
	private Long idCalculoBono;//preview
	private String clasificacionBono;
	private BigDecimal primaPagada;
	private BigDecimal primaPagadaBono;
	private BigDecimal bonoPagar;
	private BigDecimal iva;
	private BigDecimal ivaRetenido;
	private BigDecimal isr;
	private BigDecimal porcentajeBono;
	private BigDecimal porcentajeSiniestralidad;
	private String periodoProduccion;
	private String estatus;
	
	public static final String PLANTILLA_NAME = "midas.agente.reporte.agente.calculoBonoMensual.archivo.nombre";
	
	public Long getIdBeneficiario() {
		return idBeneficiario;
	}
	public void setIdBeneficiario(Long idBeneficiario) {
		this.idBeneficiario = idBeneficiario;
	}
	public String getNombreBeneficiario() {
		return nombreBeneficiario;
	}
	public void setNombreBeneficiario(String nombreBeneficiario) {
		this.nombreBeneficiario = nombreBeneficiario;
	}
	public String getPeriodoFechaPago() {
		return periodoFechaPago;
	}
	public void setPeriodoFechaPago(String periodoFechaPago) {
		this.periodoFechaPago = periodoFechaPago;
	}
	public Long getIdCalculoBono() {
		return idCalculoBono;
	}
	public void setIdCalculoBono(Long idCalculoBono) {
		this.idCalculoBono = idCalculoBono;
	}
	public String getClasificacionBono() {
		return clasificacionBono;
	}
	public void setClasificacionBono(String clasificacionBono) {
		this.clasificacionBono = clasificacionBono;
	}
	public BigDecimal getPrimaPagadaBono() {
		return primaPagadaBono;
	}
	public void setPrimaPagadaBono(BigDecimal primaPagadaBono) {
		this.primaPagadaBono = primaPagadaBono;
	}
	public BigDecimal getBonoPagar() {
		return bonoPagar;
	}
	public void setBonoPagar(BigDecimal bonoPagar) {
		this.bonoPagar = bonoPagar;
	}
	public BigDecimal getIva() {
		return iva;
	}
	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}
	public BigDecimal getIvaRetenido() {
		return ivaRetenido;
	}
	public void setIvaRetenido(BigDecimal ivaRetenido) {
		this.ivaRetenido = ivaRetenido;
	}
	public BigDecimal getIsr() {
		return isr;
	}
	public void setIsr(BigDecimal isr) {
		this.isr = isr;
	}
	public BigDecimal getPorcentajeBono() {
		return porcentajeBono;
	}
	public void setPorcentajeBono(BigDecimal porcentajeBono) {
		this.porcentajeBono = porcentajeBono;
	}
	public BigDecimal getPorcentajeSiniestralidad() {
		return porcentajeSiniestralidad;
	}
	public void setPorcentajeSiniestralidad(BigDecimal porcentajeSiniestralidad) {
		this.porcentajeSiniestralidad = porcentajeSiniestralidad;
	}
	public String getPeriodoProduccion() {
		return periodoProduccion;
	}
	public void setPeriodoProduccion(String periodoProduccion) {
		this.periodoProduccion = periodoProduccion;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}	
	public BigDecimal getPrimaPagada() {
		return primaPagada;
	}
	public void setPrimaPagada(BigDecimal primaPagada) {
		this.primaPagada = primaPagada;
	}
	@Override
	public String toString() {
		return "DatosReporteCalculoBonoMensualDTO [idBeneficiario="
				+ idBeneficiario + ", nombreBeneficiario=" + nombreBeneficiario
				+ ", periodoFechaPago=" + periodoFechaPago + ", idCalculoBono="
				+ idCalculoBono + ", clasificacionBono=" + clasificacionBono
				+ ", primaPagada=" + primaPagada + ", primaPagadaBono="
				+ primaPagadaBono + ", bonoPagar=" + bonoPagar + ", iva=" + iva
				+ ", ivaRetenido=" + ivaRetenido + ", isr=" + isr
				+ ", porcentajeBono=" + porcentajeBono
				+ ", porcentajeSiniestralidad=" + porcentajeSiniestralidad
				+ ", periodoProduccion=" + periodoProduccion + ", estatus="
				+ estatus + "]";
	}	
}
