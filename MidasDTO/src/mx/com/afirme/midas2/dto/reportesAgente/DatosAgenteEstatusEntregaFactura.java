package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

public class DatosAgenteEstatusEntregaFactura implements Serializable{

	private static final long serialVersionUID = 3229636994846247611L;
	
	public static enum estatusFactura{
		ENTREGADA("ENTREGADA"),
		PENDIENTE("PENDIENTE"),
		NO_TIENE_PAGOS("NO TIENE PAGOS");
		
		private String valor;
		
		estatusFactura(String id){
			valor = id;
		}
		
		public String valor(){
			return valor;
		}		
	}
	
	private Integer fechaInicio;
	private Integer fechaFin;
	private BigDecimal centroOperacionId;
	private BigDecimal gerenciaId;
	private String gerenciaNombre;
	private BigDecimal promotoriaId;
	private String promotoriaNombre;
	private BigDecimal idAgente;
	private String nombreAgente;
	private BigDecimal ejecutivoId;
	private String periodo;
	private String estatus;
	private Map<String, Boolean> listEstatus;
	
	public static final String PLANTILLA_NAME = "midas.agente.reporte.agente.estatusEntregaFactura.archivo.nombre";
	
	public Integer getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Integer fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Integer getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Integer fechaFin) {
		this.fechaFin = fechaFin;
	}
	public BigDecimal getCentroOperacionId() {
		return centroOperacionId;
	}
	public void setCentroOperacionId(BigDecimal centroOperacionId) {
		this.centroOperacionId = centroOperacionId;
	}
	public BigDecimal getGerenciaId() {
		return gerenciaId;
	}
	public void setGerenciaId(BigDecimal gerenciaId) {
		this.gerenciaId = gerenciaId;
	}
	public String getGerenciaNombre() {
		return gerenciaNombre;
	}
	public void setGerenciaNombre(String gerenciaNombre) {
		this.gerenciaNombre = gerenciaNombre;
	}
	public BigDecimal getPromotoriaId() {
		return promotoriaId;
	}
	public void setPromotoriaId(BigDecimal promotoriaId) {
		this.promotoriaId = promotoriaId;
	}
	public String getPromotoriaNombre() {
		return promotoriaNombre;
	}
	public void setPromotoriaNombre(String promotoriaNombre) {
		this.promotoriaNombre = promotoriaNombre;
	}
	public BigDecimal getIdAgente() {
		return idAgente;
	}
	public void setIdAgente(BigDecimal idAgente) {
		this.idAgente = idAgente;
	}
	public String getNombreAgente() {
		return nombreAgente;
	}
	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}
	public BigDecimal getEjecutivoId() {
		return ejecutivoId;
	}
	public void setEjecutivoId(BigDecimal ejecutivoId) {
		this.ejecutivoId = ejecutivoId;
	}
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public Map<String, Boolean> getListEstatus() {
		return listEstatus;
	}
	public void setListEstatus(Map<String, Boolean> listEstatus) {
		this.listEstatus = listEstatus;
	}	
}
