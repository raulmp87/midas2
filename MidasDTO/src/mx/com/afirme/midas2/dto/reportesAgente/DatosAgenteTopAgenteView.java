package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class DatosAgenteTopAgenteView implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long id;
	private Long idAgente;
	private String gerencia;
	private String nombreCompleto;
	private String idTipoAgente;
	private BigDecimal acumuladothisYear;
	private BigDecimal acumuladoBeforeYear;
	private Double acumuladoThisMonthThisYear;
	private Double acumuladoThisMonthLastYear;
	private Date fechaMovimiento;
	private BigDecimal totalCiaMesAct;
	private BigDecimal totalCiaAcumAnioAct;
	private BigDecimal totalCiaMesAnt;
	private BigDecimal totalCiaAcumAnioAnt;
	private Double pctCiaAcumMesAct;
	private Double pctCiaAcumAnioAct;
	private Double totalAcumAgentes;
	private BigDecimal totalAcumProm;
	private Double pctPromAcumAnioActual;
	public static final String PLANTILLA_NAME = "midas.agente.reporte.agente.topAgente.plantilla.archivo.nombre";
	
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdAgente() {
		return idAgente;
	}
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
	public String getGerencia() {
		return gerencia;
	}
	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	public String getIdTipoAgente() {
		return idTipoAgente;
	}
	public void setIdTipoAgente(String idTipoAgente) {
		this.idTipoAgente = idTipoAgente;
	}
	
	
	public BigDecimal getAcumuladothisYear() {
		return acumuladothisYear;
	}
	public void setAcumuladothisYear(BigDecimal acumuladothisYear) {
		this.acumuladothisYear = acumuladothisYear;
	}
	public BigDecimal getAcumuladoBeforeYear() {
		return acumuladoBeforeYear;
	}
	public void setAcumuladoBeforeYear(BigDecimal acumuladoBeforeYear) {
		this.acumuladoBeforeYear = acumuladoBeforeYear;
	}
	public Double getAcumuladoThisMonthThisYear() {
		return acumuladoThisMonthThisYear;
	}
	public void setAcumuladoThisMonthThisYear(Double acumuladoThisMonthThisYear) {
		this.acumuladoThisMonthThisYear = acumuladoThisMonthThisYear;
	}
	public Double getAcumuladoThisMonthLastYear() {
		return acumuladoThisMonthLastYear;
	}
	public void setAcumuladoThisMonthLastYear(Double acumuladoThisMonthLastYear) {
		this.acumuladoThisMonthLastYear = acumuladoThisMonthLastYear;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaMovimiento() {
		return fechaMovimiento;
	}
	public void setFechaMovimiento(Date fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
	}
	public BigDecimal getTotalCiaMesAct() {
		return totalCiaMesAct;
	}
	public void setTotalCiaMesAct(BigDecimal totalCiaMesAct) {
		this.totalCiaMesAct = totalCiaMesAct;
	}
	public BigDecimal getTotalCiaAcumAnioAct() {
		return totalCiaAcumAnioAct;
	}
	public void setTotalCiaAcumAnioAct(BigDecimal totalCiaAcumAnioAct) {
		this.totalCiaAcumAnioAct = totalCiaAcumAnioAct;
	}
	public BigDecimal getTotalCiaMesAnt() {
		return totalCiaMesAnt;
	}
	public void setTotalCiaMesAnt(BigDecimal totalCiaMesAnt) {
		this.totalCiaMesAnt = totalCiaMesAnt;
	}
	public Double getPctCiaAcumMesAct() {
		return pctCiaAcumMesAct;
	}
	public void setPctCiaAcumMesAct(Double pctCiaAcumMesAct) {
		this.pctCiaAcumMesAct = pctCiaAcumMesAct;
	}
	public Double getPctCiaAcumAnioAct() {
		return pctCiaAcumAnioAct;
	}
	public void setPctCiaAcumAnioAct(Double pctCiaAcumAnioAct) {
		this.pctCiaAcumAnioAct = pctCiaAcumAnioAct;
	}
	public BigDecimal getTotalCiaAcumAnioAnt() {
		return totalCiaAcumAnioAnt;
	}
	public void setTotalCiaAcumAnioAnt(BigDecimal totalCiaAcumAnioAnt) {
		this.totalCiaAcumAnioAnt = totalCiaAcumAnioAnt;
	}
	public Double getTotalAcumAgentes() {
		return totalAcumAgentes;
	}
	public void setTotalAcumAgentes(Double totalAcumAgentes) {
		this.totalAcumAgentes = totalAcumAgentes;
	}
	public BigDecimal getTotalAcumProm() {
		return totalAcumProm;
	}
	public void setTotalAcumProm(BigDecimal totalAcumProm) {
		this.totalAcumProm = totalAcumProm;
	}
	public Double getPctPromAcumAnioActual() {
		return pctPromAcumAnioActual;
	}
	public void setPctPromAcumAnioActual(Double pctPromAcumAnioActual) {
		this.pctPromAcumAnioActual = pctPromAcumAnioActual;
	}
	
}
