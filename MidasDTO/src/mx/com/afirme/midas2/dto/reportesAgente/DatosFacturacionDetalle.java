package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;

public class DatosFacturacionDetalle implements Serializable {

	private static final long serialVersionUID = 1784878947634134280L;
	
	private Long numero_poliza;
	private String contratante;
	private Long endoso;
	private String prima_base;
	private Double importe_prima_base;
	private Double porcentaje_comp_prima;
	private Double importe_prima;
	private Double comp_derecho_poliza;
	private Double comp_baja_siniestralidad;
	private Double total_compensacion;
	private Double recargo;
	private Double comision_Sistema;
	private Double comision_Ajuste;
	
	public Long getNumero_poliza() {
		return numero_poliza;
	}
	public void setNumero_poliza(Long numero_poliza) {
		this.numero_poliza = numero_poliza;
	}
	public String getContratante() {
		return contratante;
	}
	public void setContratante(String contratante) {
		this.contratante = contratante;
	}
	public Long getEndoso() {
		return endoso;
	}
	public void setEndoso(Long endoso) {
		this.endoso = endoso;
	}
	public String getPrima_base() {
		return prima_base;
	}
	public void setPrima_base(String prima_base) {
		this.prima_base = prima_base;
	}
	public Double getImporte_prima_base() {
		return importe_prima_base;
	}
	public void setImporte_prima_base(Double importe_prima_base) {
		this.importe_prima_base = importe_prima_base;
	}
	public Double getPorcentaje_comp_prima() {
		return porcentaje_comp_prima;
	}
	public void setPorcentaje_comp_prima(Double porcentaje_comp_prima) {
		this.porcentaje_comp_prima = porcentaje_comp_prima;
	}
	public Double getImporte_prima() {
		return importe_prima;
	}
	public void setImporte_prima(Double importe_prima) {
		this.importe_prima = importe_prima;
	}
	
	public Double getComp_derecho_poliza() {
		return comp_derecho_poliza;
	}
	public void setComp_derecho_poliza(Double comp_derecho_poliza) {
		this.comp_derecho_poliza = comp_derecho_poliza;
	}
	public Double getComp_baja_siniestralidad() {
		return comp_baja_siniestralidad;
	}
	public void setComp_baja_siniestralidad(Double comp_baja_siniestralidad) {
		this.comp_baja_siniestralidad = comp_baja_siniestralidad;
	}
	public Double getTotal_compensacion() {
		return total_compensacion;
	}
	public void setTotal_compensacion(Double total_compensacion) {
		this.total_compensacion = total_compensacion;
	}
	public Double getRecargo() {
		return recargo;
	}
	public void setRecargo(Double recargo) {
		this.recargo = recargo;
	}
	public Double getComision_Sistema() {
		return comision_Sistema;
	}
	public void setComision_Sistema(Double comision_Sistema) {
		this.comision_Sistema = comision_Sistema;
	}
	public Double getComision_Ajuste() {
		return comision_Ajuste;
	}
	public void setComision_Ajuste(Double comision_Ajuste) {
		this.comision_Ajuste = comision_Ajuste;
	}

	
}