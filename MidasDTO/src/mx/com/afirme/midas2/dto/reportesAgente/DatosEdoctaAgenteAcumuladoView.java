package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class DatosEdoctaAgenteAcumuladoView implements Serializable {

	private static final long serialVersionUID = 5034101133989113760L;
	
	private Long idAgente;
	private String nombreAgente;
	private String domicilio;
	private String colonia;
	private String ciudad;
	private String cp;
	private String rfc;
	private String cedula;
	private String correo;
	private String correoCc;
	private String promotor;
	private String ejecutivo;
	private String centroOperacion;
	private String promotoria;
	private String gerencia;
	private BigDecimal saldoInicial;
	private BigDecimal importeComisionGravadaIvaA;
	private BigDecimal importeComisionExentaIvaA;
	private BigDecimal importeLiquidado;
	private BigDecimal importeCargos;
	private BigDecimal importeBonoVida;
	private BigDecimal importeBonoDanos;
	private BigDecimal importeIvaAcreditado;
	private BigDecimal importeIvaRetenido;
	private BigDecimal importeIsr;
	private BigDecimal importeRetenidoEstatal;
	private BigDecimal saldoFinal;
	private BigDecimal acumuladoSaldoInicial;
	private BigDecimal acumuladoComisionIva;
	private BigDecimal acumuladoComisionExenta;
	private BigDecimal acumuladoLiquidado;
	private BigDecimal acumuladoCargos;
	private BigDecimal acumuladoBonoVida;
	private BigDecimal acumuladoBonoDanos;
	private BigDecimal acumuladoIvaAcreditado;
	private BigDecimal acumuladoIvaRetenido;
	private BigDecimal acumuladoIsr;
	private BigDecimal acumuladoEstatalRetenido;
	private BigDecimal acumuladoSaldoFinal;
	private BigDecimal nuevoSaldoInicial;
	private BigDecimal nuevoComisionIva;
	private BigDecimal nuevoComisionExenta;
	private BigDecimal nuevoLiquidado;
	private BigDecimal nuevoCargos;
	private BigDecimal nuevoBonoVida;
	private BigDecimal nuevoBonoDanos;
	private BigDecimal nuevoIvaAcreditado;
	private BigDecimal nuevoIvaRetenido;
	private BigDecimal nuevoIsr;
	private BigDecimal nuevoRetenidoEstatal;
	private BigDecimal nuevoSaldoFinal;
	private BigDecimal importeOtrosAbonos;
	private BigDecimal acumuladoOtrosAbonos;
	private BigDecimal nuevoOtrosAbonos;
	private List<DatosEdoctaAgenteMovimientosView> movimientos;
	
	@Id
	public Long getIdAgente() {
		return idAgente;
	}
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
	public String getNombreAgente() {
		return nombreAgente;
	}
	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}
	public String getDomicilio() {
		return domicilio;
	}
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	public String getColonia() {
		return colonia;
	}
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getCp() {
		return cp;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getCorreoCc() {
		return correoCc;
	}
	public void setCorreoCc(String correoCc) {
		this.correoCc = correoCc;
	}
	public String getPromotor() {
		return promotor;
	}
	public void setPromotor(String promotor) {
		this.promotor = promotor;
	}
	public String getEjecutivo() {
		return ejecutivo;
	}
	public void setEjecutivo(String ejecutivo) {
		this.ejecutivo = ejecutivo;
	}
	public String getCentroOperacion() {
		return centroOperacion;
	}
	public void setCentroOperacion(String centroOperacion) {
		this.centroOperacion = centroOperacion;
	}
	public String getPromotoria() {
		return promotoria;
	}
	public void setPromotoria(String promotoria) {
		this.promotoria = promotoria;
	}
	public String getGerencia() {
		return gerencia;
	}
	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}
	public BigDecimal getSaldoInicial() {
		return saldoInicial;
	}
	public void setSaldoInicial(BigDecimal saldoInicial) {
		this.saldoInicial = saldoInicial;
	}
	public BigDecimal getImporteComisionGravadaIvaA() {
		return importeComisionGravadaIvaA;
	}
	public void setImporteComisionGravadaIvaA(BigDecimal importeComisionGravadaIvaA) {
		this.importeComisionGravadaIvaA = importeComisionGravadaIvaA;
	}
	public BigDecimal getImporteComisionExentaIvaA() {
		return importeComisionExentaIvaA;
	}
	public void setImporteComisionExentaIvaA(BigDecimal importeComisionExentaIvaA) {
		this.importeComisionExentaIvaA = importeComisionExentaIvaA;
	}
	public BigDecimal getImporteLiquidado() {
		return importeLiquidado;
	}
	public void setImporteLiquidado(BigDecimal importeLiquidado) {
		this.importeLiquidado = importeLiquidado;
	}
	public BigDecimal getImporteCargos() {
		return importeCargos;
	}
	public void setImporteCargos(BigDecimal importeCargos) {
		this.importeCargos = importeCargos;
	}
	public BigDecimal getImporteBonoVida() {
		return importeBonoVida;
	}
	public void setImporteBonoVida(BigDecimal importeBonoVida) {
		this.importeBonoVida = importeBonoVida;
	}
	public BigDecimal getImporteBonoDanos() {
		return importeBonoDanos;
	}
	public void setImporteBonoDanos(BigDecimal importeBonoDanos) {
		this.importeBonoDanos = importeBonoDanos;
	}
	public BigDecimal getImporteIvaAcreditado() {
		return importeIvaAcreditado;
	}
	public void setImporteIvaAcreditado(BigDecimal importeIvaAcreditado) {
		this.importeIvaAcreditado = importeIvaAcreditado;
	}
	public BigDecimal getImporteIvaRetenido() {
		return importeIvaRetenido;
	}
	public void setImporteIvaRetenido(BigDecimal importeIvaRetenido) {
		this.importeIvaRetenido = importeIvaRetenido;
	}
	public BigDecimal getImporteIsr() {
		return importeIsr;
	}
	public void setImporteIsr(BigDecimal importeIsr) {
		this.importeIsr = importeIsr;
	}
	public BigDecimal getImporteRetenidoEstatal() {
		return importeRetenidoEstatal;
	}
	public void setImporteRetenidoEstatal(BigDecimal importeRetenidoEstatal) {
		this.importeRetenidoEstatal = importeRetenidoEstatal;
	}
	public BigDecimal getSaldoFinal() {
		return saldoFinal;
	}
	public void setSaldoFinal(BigDecimal saldoFinal) {
		this.saldoFinal = saldoFinal;
	}
	public BigDecimal getAcumuladoSaldoInicial() {
		return acumuladoSaldoInicial;
	}
	public void setAcumuladoSaldoInicial(BigDecimal acumuladoSaldoInicial) {
		this.acumuladoSaldoInicial = acumuladoSaldoInicial;
	}
	public BigDecimal getAcumuladoComisionIva() {
		return acumuladoComisionIva;
	}
	public void setAcumuladoComisionIva(BigDecimal acumuladoComisionIva) {
		this.acumuladoComisionIva = acumuladoComisionIva;
	}
	public BigDecimal getAcumuladoComisionExenta() {
		return acumuladoComisionExenta;
	}
	public void setAcumuladoComisionExenta(BigDecimal acumuladoComisionExenta) {
		this.acumuladoComisionExenta = acumuladoComisionExenta;
	}
	public BigDecimal getAcumuladoLiquidado() {
		return acumuladoLiquidado;
	}
	public void setAcumuladoLiquidado(BigDecimal acumuladoLiquidado) {
		this.acumuladoLiquidado = acumuladoLiquidado;
	}
	public BigDecimal getAcumuladoCargos() {
		return acumuladoCargos;
	}
	public void setAcumuladoCargos(BigDecimal acumuladoCargos) {
		this.acumuladoCargos = acumuladoCargos;
	}
	public BigDecimal getAcumuladoBonoVida() {
		return acumuladoBonoVida;
	}
	public void setAcumuladoBonoVida(BigDecimal acumuladoBonoVida) {
		this.acumuladoBonoVida = acumuladoBonoVida;
	}
	public BigDecimal getAcumuladoBonoDanos() {
		return acumuladoBonoDanos;
	}
	public void setAcumuladoBonoDanos(BigDecimal acumuladoBonoDanos) {
		this.acumuladoBonoDanos = acumuladoBonoDanos;
	}
	public BigDecimal getAcumuladoIvaAcreditado() {
		return acumuladoIvaAcreditado;
	}
	public void setAcumuladoIvaAcreditado(BigDecimal acumuladoIvaAcreditado) {
		this.acumuladoIvaAcreditado = acumuladoIvaAcreditado;
	}
	public BigDecimal getAcumuladoIvaRetenido() {
		return acumuladoIvaRetenido;
	}
	public void setAcumuladoIvaRetenido(BigDecimal acumuladoIvaRetenido) {
		this.acumuladoIvaRetenido = acumuladoIvaRetenido;
	}
	public BigDecimal getAcumuladoIsr() {
		return acumuladoIsr;
	}
	public void setAcumuladoIsr(BigDecimal acumuladoIsr) {
		this.acumuladoIsr = acumuladoIsr;
	}
	public BigDecimal getAcumuladoEstatalRetenido() {
		return acumuladoEstatalRetenido;
	}
	public void setAcumuladoEstatalRetenido(BigDecimal acumuladoEstatalRetenido) {
		this.acumuladoEstatalRetenido = acumuladoEstatalRetenido;
	}
	public BigDecimal getAcumuladoSaldoFinal() {
		return acumuladoSaldoFinal;
	}
	public void setAcumuladoSaldoFinal(BigDecimal acumuladoSaldoFinal) {
		this.acumuladoSaldoFinal = acumuladoSaldoFinal;
	}
	public BigDecimal getNuevoSaldoInicial() {
		return nuevoSaldoInicial;
	}
	public void setNuevoSaldoInicial(BigDecimal nuevoSaldoInicial) {
		this.nuevoSaldoInicial = nuevoSaldoInicial;
	}
	public BigDecimal getNuevoComisionIva() {
		return nuevoComisionIva;
	}
	public void setNuevoComisionIva(BigDecimal nuevoComisionIva) {
		this.nuevoComisionIva = nuevoComisionIva;
	}
	public BigDecimal getNuevoComisionExenta() {
		return nuevoComisionExenta;
	}
	public void setNuevoComisionExenta(BigDecimal nuevoComisionExenta) {
		this.nuevoComisionExenta = nuevoComisionExenta;
	}
	public BigDecimal getNuevoLiquidado() {
		return nuevoLiquidado;
	}
	public void setNuevoLiquidado(BigDecimal nuevoLiquidado) {
		this.nuevoLiquidado = nuevoLiquidado;
	}
	public BigDecimal getNuevoCargos() {
		return nuevoCargos;
	}
	public void setNuevoCargos(BigDecimal nuevoCargos) {
		this.nuevoCargos = nuevoCargos;
	}
	public BigDecimal getNuevoBonoVida() {
		return nuevoBonoVida;
	}
	public void setNuevoBonoVida(BigDecimal nuevoBonoVida) {
		this.nuevoBonoVida = nuevoBonoVida;
	}
	public BigDecimal getNuevoBonoDanos() {
		return nuevoBonoDanos;
	}
	public void setNuevoBonoDanos(BigDecimal nuevoBonoDanos) {
		this.nuevoBonoDanos = nuevoBonoDanos;
	}
	public BigDecimal getNuevoIvaAcreditado() {
		return nuevoIvaAcreditado;
	}
	public void setNuevoIvaAcreditado(BigDecimal nuevoIvaAcreditado) {
		this.nuevoIvaAcreditado = nuevoIvaAcreditado;
	}
	public BigDecimal getNuevoIvaRetenido() {
		return nuevoIvaRetenido;
	}
	public void setNuevoIvaRetenido(BigDecimal nuevoIvaRetenido) {
		this.nuevoIvaRetenido = nuevoIvaRetenido;
	}
	public BigDecimal getNuevoIsr() {
		return nuevoIsr;
	}
	public void setNuevoIsr(BigDecimal nuevoIsr) {
		this.nuevoIsr = nuevoIsr;
	}
	public BigDecimal getNuevoRetenidoEstatal() {
		return nuevoRetenidoEstatal;
	}
	public void setNuevoRetenidoEstatal(BigDecimal nuevoRetenidoEstatal) {
		this.nuevoRetenidoEstatal = nuevoRetenidoEstatal;
	}
	public BigDecimal getNuevoSaldoFinal() {
		return nuevoSaldoFinal;
	}
	public void setNuevoSaldoFinal(BigDecimal nuevoSaldoFinal) {
		this.nuevoSaldoFinal = nuevoSaldoFinal;
	}
	public BigDecimal getImporteOtrosAbonos() {
		return importeOtrosAbonos;
	}
	public void setImporteOtrosAbonos(BigDecimal importeOtrosAbonos) {
		this.importeOtrosAbonos = importeOtrosAbonos;
	}
	public BigDecimal getAcumuladoOtrosAbonos() {
		return acumuladoOtrosAbonos;
	}
	public void setAcumuladoOtrosAbonos(BigDecimal acumuladoOtrosAbonos) {
		this.acumuladoOtrosAbonos = acumuladoOtrosAbonos;
	}
	public BigDecimal getNuevoOtrosAbonos() {
		return nuevoOtrosAbonos;
	}
	public void setNuevoOtrosAbonos(BigDecimal nuevoOtrosAbonos) {
		this.nuevoOtrosAbonos = nuevoOtrosAbonos;
	}
	@Transient
	public List<DatosEdoctaAgenteMovimientosView> getMovimientos() {
		return movimientos;
	}
	public void setMovimientos(List<DatosEdoctaAgenteMovimientosView> movimientos) {
		this.movimientos = movimientos;
	}

}
