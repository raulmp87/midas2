package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class DatosReportePreviewBonos implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8297484961349189649L;
	private Long id;
	private String promotor;
	private String clasificacion;//Tipo de Agente
	private Double acumulada ;//Prima neta Acumilada
	private Double mes; //Prima acumulada del mes anterior
	private Double mesActual; //prima del mes
	
	public static final String PLANTILLA_NAME = "midas.agente.reporte.agente.previewBonos.archivo.nombre";
	
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPromotor() {
		return promotor;
	}
	public void setPromotor(String promotor) {
		this.promotor = promotor;
	}
	public String getClasificacion() {
		return clasificacion;
	}
	public void setClasificacion(String clasificacion) {
		this.clasificacion = clasificacion;
	}
	public Double getAcumulada() {
		return acumulada;
	}
	public void setAcumulada(Double acumulada) {
		this.acumulada = acumulada;
	}
	public Double getMes() {
		return mes;
	}
	public void setMes(Double mes) {
		this.mes = mes;
	}
	public Double getMesActual() {
		return mesActual;
	}
	public void setMesActual(Double mesActual) {
		this.mesActual = mesActual;
	}
}
