package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;
import java.math.BigDecimal;


public class DetPrimasResumen implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1919851821482299666L;	
	private String concepto;
	private BigDecimal cargo;
	private BigDecimal abono;	
	
	public DetPrimasResumen()
	{
		cargo = new BigDecimal(0);
		abono = new BigDecimal(0);		
	}
	
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public BigDecimal getCargo() {
		return cargo;
	}
	public void setCargo(BigDecimal cargo) {
		this.cargo = cargo;
	}
	public BigDecimal getAbono() {
		return abono;
	}
	public void setAbono(BigDecimal abono) {
		this.abono = abono;
	}	
	
	public void agregarMovimiento(Double monto)
	{
		if(monto.doubleValue() >= 0)
		{
			this.abono = this.abono.add(new BigDecimal(monto.doubleValue()));
			
		}else
		{
			this.cargo = this.cargo.add(new BigDecimal(monto.doubleValue()));
		}
	}
}
