package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class DatosEdoctaAgenteMovimientosView implements Serializable{

	private static final long serialVersionUID = 1L;
	private Long idAgente;
//	private String periodo;
	private Long periodo;
	private String dia;
	private String referencia;
	private String concepto;
	private Double cargo;
	private Double abono;
	private Long id;
	private String subramo;
	private String ramo;
	private Double totalDia;
	private Double saldos;
	private String lineaVenta;
	private Double cierremes;
	
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdAgente() {
		return idAgente;
	}
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
//	public String getPeriodo() {
//		return periodo;
//	}
//	public void setPeriodo(String periodo) {
//		this.periodo = periodo;
//	}
	public Long getPeriodo() {
		return periodo;
	}
	public void setPeriodo(Long periodo) {
		this.periodo = periodo;
	}
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public Double getCargo() {
		return cargo;
	}
	public void setCargo(Double cargo) {
		this.cargo = cargo;
	}
	public Double getAbono() {
		return abono;
	}
	public void setAbono(Double abono) {
		this.abono = abono;
	}
	public String getSubramo() {
		return subramo;
	}
	public void setSubramo(String subramo) {
		this.subramo = subramo;
	}
	public String getRamo() {
		return ramo;
	}
	public void setRamo(String ramo) {
		this.ramo = ramo;
	}
	public Double getTotalDia() {
		return totalDia;
	}
	public void setTotalDia(Double totalDia) {
		this.totalDia = totalDia;
	}
	public Double getSaldos() {
		return saldos;
	}
	public void setSaldos(Double saldos) {
		this.saldos = saldos;
	}
	public String getLineaVenta() {
		return lineaVenta;
	}
	public void setLineaVenta(String lineaVenta) {
		this.lineaVenta = lineaVenta;
	}
	public Double getCierremes() {
		return cierremes;
	}
	public void setCierremes(Double cierremes) {
		this.cierremes = cierremes;
	}
}
