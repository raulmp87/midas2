package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class DatosAgenteBonosGerenciaView implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long claveAgente;
	private String nombreAgente;
	private BigDecimal primaPagadaTotal;
	private BigDecimal primaNetaPagSP;
	private BigDecimal primaNetaPagBonoSP;
	private Double bonoSPAL;
	private BigDecimal bonoPagadoSp;
	private BigDecimal crecimientoSp;
	private BigDecimal bonoPagadoCrecimientoSP;
	private BigDecimal primaNetaPagAutos;
	private BigDecimal primaNetaPagBonoAutos;
	private Double bonosAutosAl;
	private BigDecimal bonosPagAutos;
	private BigDecimal primaNetaPagDanios;
	private BigDecimal primaNetaPagBonoDanios;
	private Double bonoDaniosAl;
	private BigDecimal bonoPagadoDanios;
	private BigDecimal primaNetaPagTC;
	private BigDecimal primaNetaPagBonoTC;
	private Double bonoDaniosAlTC;
	private BigDecimal bonoPagadoTC;
	private BigDecimal primaNetaPagCasa;
	private BigDecimal primaNetaPagBonoCasa;
	private Double bonoDaniosALCasa;
	private BigDecimal bonoPagadoCasa;
	private BigDecimal primaNetaPagVida;
	private BigDecimal primaNetaPagBonoVida;
	private Double bonoVidaAL;
	private BigDecimal bonoPagadoVida;
	
	public static final String PLANTILLA_NAME = "midas.agente.reporte.agente.bonosgerencia.archivo.nombre";
	
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getClaveAgente() {
		return claveAgente;
	}
	public void setClaveAgente(Long claveAgente) {
		this.claveAgente = claveAgente;
	}
	public String getNombreAgente() {
		return nombreAgente;
	}
	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}
	public BigDecimal getPrimaPagadaTotal() {
		return primaPagadaTotal;
	}
	public void setPrimaPagadaTotal(BigDecimal primaPagadaTotal) {
		this.primaPagadaTotal = primaPagadaTotal;
	}
	public BigDecimal getPrimaNetaPagSP() {
		return primaNetaPagSP;
	}
	public void setPrimaNetaPagSP(BigDecimal primaNetaPagSP) {
		this.primaNetaPagSP = primaNetaPagSP;
	}
	public BigDecimal getPrimaNetaPagBonoSP() {
		return primaNetaPagBonoSP;
	}
	public void setPrimaNetaPagBonoSP(BigDecimal primaNetaPagBonoSP) {
		this.primaNetaPagBonoSP = primaNetaPagBonoSP;
	}
	public Double getBonoSPAL() {
		return bonoSPAL;
	}
	public void setBonoSPAL(Double bonoSPAL) {
		this.bonoSPAL = bonoSPAL;
	}
	public BigDecimal getBonoPagadoSp() {
		return bonoPagadoSp;
	}
	public void setBonoPagadoSp(BigDecimal bonoPagadoSp) {
		this.bonoPagadoSp = bonoPagadoSp;
	}
	public BigDecimal getCrecimientoSp() {
		return crecimientoSp;
	}
	public void setCrecimientoSp(BigDecimal crecimientoSp) {
		this.crecimientoSp = crecimientoSp;
	}
	public BigDecimal getBonoPagadoCrecimientoSP() {
		return bonoPagadoCrecimientoSP;
	}
	public void setBonoPagadoCrecimientoSP(BigDecimal bonoPagadoCrecimientoSP) {
		this.bonoPagadoCrecimientoSP = bonoPagadoCrecimientoSP;
	}
	public BigDecimal getPrimaNetaPagAutos() {
		return primaNetaPagAutos;
	}
	public void setPrimaNetaPagAutos(BigDecimal primaNetaPagAutos) {
		this.primaNetaPagAutos = primaNetaPagAutos;
	}
	public BigDecimal getPrimaNetaPagBonoAutos() {
		return primaNetaPagBonoAutos;
	}
	public void setPrimaNetaPagBonoAutos(BigDecimal primaNetaPagBonoAutos) {
		this.primaNetaPagBonoAutos = primaNetaPagBonoAutos;
	}
	public Double getBonosAutosAl() {
		return bonosAutosAl;
	}
	public void setBonosAutosAl(Double bonosAutosAl) {
		this.bonosAutosAl = bonosAutosAl;
	}
	public BigDecimal getBonosPagAutos() {
		return bonosPagAutos;
	}
	public void setBonosPagAutos(BigDecimal bonosPagAutos) {
		this.bonosPagAutos = bonosPagAutos;
	}
	public BigDecimal getPrimaNetaPagDanios() {
		return primaNetaPagDanios;
	}
	public void setPrimaNetaPagDanios(BigDecimal primaNetaPagDanios) {
		this.primaNetaPagDanios = primaNetaPagDanios;
	}
	public BigDecimal getPrimaNetaPagBonoDanios() {
		return primaNetaPagBonoDanios;
	}
	public void setPrimaNetaPagBonoDanios(BigDecimal primaNetaPagBonoDanios) {
		this.primaNetaPagBonoDanios = primaNetaPagBonoDanios;
	}
	public Double getBonoDaniosAl() {
		return bonoDaniosAl;
	}
	public void setBonoDaniosAl(Double bonoDaniosAl) {
		this.bonoDaniosAl = bonoDaniosAl;
	}
	public BigDecimal getBonoPagadoDanios() {
		return bonoPagadoDanios;
	}
	public void setBonoPagadoDanios(BigDecimal bonoPagadoDanios) {
		this.bonoPagadoDanios = bonoPagadoDanios;
	}
	public BigDecimal getPrimaNetaPagTC() {
		return primaNetaPagTC;
	}
	public void setPrimaNetaPagTC(BigDecimal primaNetaPagTC) {
		this.primaNetaPagTC = primaNetaPagTC;
	}
	public BigDecimal getPrimaNetaPagBonoTC() {
		return primaNetaPagBonoTC;
	}
	public void setPrimaNetaPagBonoTC(BigDecimal primaNetaPagBonoTC) {
		this.primaNetaPagBonoTC = primaNetaPagBonoTC;
	}
	public Double getBonoDaniosAlTC() {
		return bonoDaniosAlTC;
	}
	public void setBonoDaniosAlTC(Double bonoDaniosAlTC) {
		this.bonoDaniosAlTC = bonoDaniosAlTC;
	}
	public BigDecimal getBonoPagadoTC() {
		return bonoPagadoTC;
	}
	public void setBonoPagadoTC(BigDecimal bonoPagadoTC) {
		this.bonoPagadoTC = bonoPagadoTC;
	}
	public BigDecimal getPrimaNetaPagCasa() {
		return primaNetaPagCasa;
	}
	public void setPrimaNetaPagCasa(BigDecimal primaNetaPagCasa) {
		this.primaNetaPagCasa = primaNetaPagCasa;
	}
	public BigDecimal getPrimaNetaPagBonoCasa() {
		return primaNetaPagBonoCasa;
	}
	public void setPrimaNetaPagBonoCasa(BigDecimal primaNetaPagBonoCasa) {
		this.primaNetaPagBonoCasa = primaNetaPagBonoCasa;
	}
	public Double getBonoDaniosALCasa() {
		return bonoDaniosALCasa;
	}
	public void setBonoDaniosALCasa(Double bonoDaniosALCasa) {
		this.bonoDaniosALCasa = bonoDaniosALCasa;
	}
	public BigDecimal getBonoPagadoCasa() {
		return bonoPagadoCasa;
	}
	public void setBonoPagadoCasa(BigDecimal bonoPagadoCasa) {
		this.bonoPagadoCasa = bonoPagadoCasa;
	}
	public BigDecimal getPrimaNetaPagVida() {
		return primaNetaPagVida;
	}
	public void setPrimaNetaPagVida(BigDecimal primaNetaPagVida) {
		this.primaNetaPagVida = primaNetaPagVida;
	}
	public BigDecimal getPrimaNetaPagBonoVida() {
		return primaNetaPagBonoVida;
	}
	public void setPrimaNetaPagBonoVida(BigDecimal primaNetaPagBonoVida) {
		this.primaNetaPagBonoVida = primaNetaPagBonoVida;
	}
	public Double getBonoVidaAL() {
		return bonoVidaAL;
	}
	public void setBonoVidaAL(Double bonoVidaAL) {
		this.bonoVidaAL = bonoVidaAL;
	}
	public BigDecimal getBonoPagadoVida() {
		return bonoPagadoVida;
	}
	public void setBonoPagadoVida(BigDecimal bonoPagadoVida) {
		this.bonoPagadoVida = bonoPagadoVida;
	}
	
	
}
