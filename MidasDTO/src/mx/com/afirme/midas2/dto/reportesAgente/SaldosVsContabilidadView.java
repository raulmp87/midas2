package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class SaldosVsContabilidadView implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5221524186278221762L;
	
	public static final String PLANTILLA_NAME = "midas.agente.reporte.agente.saldoVScontabilidad.archivo.nombre";
	
	Long id;
	Long id_agente;
	String nombre;
	String b_contab_comis;
	Double saldo_inicial;
	Double cobranza;
	Double manuales;
	Double pagos;
	Double saldo_final;
	Double cob_mizar;
	Double agt_mizar;
	Double gg_mizar;
	Double dif_cob;
	Double dif_man;
	Double dif_pag;
	
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getId_agente() {
		return id_agente;
	}
	public void setId_agente(Long id_agente) {
		this.id_agente = id_agente;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getB_contab_comis() {
		return b_contab_comis;
	}
	public void setB_contab_comis(String b_contab_comis) {
		this.b_contab_comis = b_contab_comis;
	}
	public Double getSaldo_inicial() {
		return saldo_inicial;
	}
	public void setSaldo_inicial(Double saldo_inicial) {
		this.saldo_inicial = saldo_inicial;
	}
	public Double getCobranza() {
		return cobranza;
	}
	public void setCobranza(Double cobranza) {
		this.cobranza = cobranza;
	}
	public Double getManuales() {
		return manuales;
	}
	public void setManuales(Double manuales) {
		this.manuales = manuales;
	}
	public Double getPagos() {
		return pagos;
	}
	public void setPagos(Double pagos) {
		this.pagos = pagos;
	}
	public Double getSaldo_final() {
		return saldo_final;
	}
	public void setSaldo_final(Double saldo_final) {
		this.saldo_final = saldo_final;
	}
	public Double getCob_mizar() {
		return cob_mizar;
	}
	public void setCob_mizar(Double cob_mizar) {
		this.cob_mizar = cob_mizar;
	}
	public Double getGg_mizar() {
		return gg_mizar;
	}
	public void setGg_mizar(Double gg_mizar) {
		this.gg_mizar = gg_mizar;
	}
	public Double getAgt_mizar() {
		return agt_mizar;
	}
	public void setAgt_mizar(Double agt_mizar) {
		this.agt_mizar = agt_mizar;
	}
	public Double getDif_cob() {
		return dif_cob;
	}
	public void setDif_cob(Double dif_cob) {
		this.dif_cob = dif_cob;
	}
	public Double getDif_man() {
		return dif_man;
	}
	public void setDif_man(Double dif_man) {
		this.dif_man = dif_man;
	}
	public Double getDif_pag() {
		return dif_pag;
	}
	public void setDif_pag(Double dif_pag) {
		this.dif_pag = dif_pag;
	}

}
