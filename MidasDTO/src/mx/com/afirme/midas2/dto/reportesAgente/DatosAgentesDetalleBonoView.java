package mx.com.afirme.midas2.dto.reportesAgente;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class DatosAgentesDetalleBonoView implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id ;
	private Date fechaMovimiento;// FECHAMOVIMIENTO	
	private String gerencia;       // GERENCIA	
	private String tipoAgente;     // TIPO DE AGENTE	
	private String promotoria;     // PROMOTORIA	
	private Long claveAgente;      // CLAVE AGENTE	
	private String nom_agente;     // NOM_AGENTE	
	private String num_poliza;     // NUM_POLIZA	
	private Long num_endoso; 	   // NUM_ENDOSO	
	private Long num_folio_rbo; // NUM_FOLIO_RECIBO	
	private String forma_pago;     // FORMA_PAGO	
	private String id_ramo_contable; // ID_RAMO_CONTABLE	
	private String id_subr_contable; // ID_SUBR_CONTABLE	
	private String ramo;           // RAMO	**
	private String ramo1;          // RAMO 1	**
	private String desc_moneda;    // DESC_MONEDA	
	private String contratante;    // CONTRATANTE	 
	private Double imp_prima_neta;   // IMP_PRIMA_NETA 	
	private Double imp_rcgos_pagofr; // IMP_RCGOS_PAGOFR	 
	private Double imp_prima_total;  // IMP_PRIMA_TOTAL 	
	private String pct_comis_agte;   // PCT_COMIS_AGTE	
	private Double sdo_inicial;      // SDO_INICIAL	 
	private Double imp_comis_agte;   // IMP_COMIS_AGTE 	 
	private Double imp_iva_acred;    // IMP_IVA_ACRED 	 
	private Double imp_iva_ret;      // IMP_IVA_RET 	 
	private Double imp_isr;          // IMP_ISR 	
	private String desc_movto;     // DESC_MOVTO
	
	public static final String PLANTILLA_NAME = "midas.agente.reporte.agente.detalleBonos.archivo.nombre";
	
	@Id	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Temporal(TemporalType.DATE)
	public Date getFechaMovimiento() {
		return fechaMovimiento;
	}
	public void setFechaMovimiento(Date fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
	}
	public String getGerencia() {
		return gerencia;
	}
	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}
	public String getTipoAgente() {
		return tipoAgente;
	}
	public void setTipoAgente(String tipoAgente) {
		this.tipoAgente = tipoAgente;
	}
	public String getPromotoria() {
		return promotoria;
	}
	public void setPromotoria(String promotoria) {
		this.promotoria = promotoria;
	}
	public Long getClaveAgente() {
		return claveAgente;
	}
	public void setClaveAgente(Long claveAgente) {
		this.claveAgente = claveAgente;
	}
	public String getNom_agente() {
		return nom_agente;
	}
	public void setNom_agente(String nom_agente) {
		this.nom_agente = nom_agente;
	}
	public String getNum_poliza() {
		return num_poliza;
	}
	public void setNum_poliza(String num_poliza) {
		this.num_poliza = num_poliza;
	}
	public Long getNum_endoso() {
		return num_endoso;
	}
	public void setNum_endoso(Long num_endoso) {
		this.num_endoso = num_endoso;
	}
	
	public Long getNum_folio_rbo() {
		return num_folio_rbo;
	}
	public void setNum_folio_rbo(Long num_folio_rbo) {
		this.num_folio_rbo = num_folio_rbo;
	}
	public String getForma_pago() {
		return forma_pago;
	}
	public void setForma_pago(String forma_pago) {
		this.forma_pago = forma_pago;
	}
	public String getId_ramo_contable() {
		return id_ramo_contable;
	}
	public void setId_ramo_contable(String id_ramo_contable) {
		this.id_ramo_contable = id_ramo_contable;
	}
	public String getId_subr_contable() {
		return id_subr_contable;
	}
	public void setId_subr_contable(String id_subr_contable) {
		this.id_subr_contable = id_subr_contable;
	}
	public String getRamo() {
		return ramo;
	}
	public void setRamo(String ramo) {
		this.ramo = ramo;
	}
	public String getRamo1() {
		return ramo1;
	}
	public void setRamo1(String ramo1) {
		this.ramo1 = ramo1;
	}
	public String getDesc_moneda() {
		return desc_moneda;
	}
	public void setDesc_moneda(String desc_moneda) {
		this.desc_moneda = desc_moneda;
	}
	public String getContratante() {
		return contratante;
	}
	public void setContratante(String contratante) {
		this.contratante = contratante;
	}
	
	public Double getImp_prima_neta() {
		return imp_prima_neta;
	}
	public void setImp_prima_neta(Double imp_prima_neta) {
		this.imp_prima_neta = imp_prima_neta;
	}
	public Double getImp_rcgos_pagofr() {
		return imp_rcgos_pagofr;
	}
	public void setImp_rcgos_pagofr(Double imp_rcgos_pagofr) {
		this.imp_rcgos_pagofr = imp_rcgos_pagofr;
	}
	public Double getImp_prima_total() {
		return imp_prima_total;
	}
	public void setImp_prima_total(Double imp_prima_total) {
		this.imp_prima_total = imp_prima_total;
	}
	public String getPct_comis_agte() {
		return pct_comis_agte;
	}
	public void setPct_comis_agte(String pct_comis_agte) {
		this.pct_comis_agte = pct_comis_agte;
	}
	public Double getSdo_inicial() {
		return sdo_inicial;
	}
	public void setSdo_inicial(Double sdo_inicial) {
		this.sdo_inicial = sdo_inicial;
	}
	public Double getImp_comis_agte() {
		return imp_comis_agte;
	}
	public void setImp_comis_agte(Double imp_comis_agte) {
		this.imp_comis_agte = imp_comis_agte;
	}
	public Double getImp_iva_acred() {
		return imp_iva_acred;
	}
	public void setImp_iva_acred(Double imp_iva_acred) {
		this.imp_iva_acred = imp_iva_acred;
	}
	public Double getImp_iva_ret() {
		return imp_iva_ret;
	}
	public void setImp_iva_ret(Double imp_iva_ret) {
		this.imp_iva_ret = imp_iva_ret;
	}
	public Double getImp_isr() {
		return imp_isr;
	}
	public void setImp_isr(Double imp_isr) {
		this.imp_isr = imp_isr;
	}
	public String getDesc_movto() {
		return desc_movto;
	}
	public void setDesc_movto(String desc_movto) {
		this.desc_movto = desc_movto;
	}

}
